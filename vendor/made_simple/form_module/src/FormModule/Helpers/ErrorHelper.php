<?php

namespace FormModule\Helpers;

use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;

class ErrorHelper
{
    /**
     * @param FormInterface $form
     * @return int
     */
    public static function getCount(FormInterface $form)
    {
        $errors = $form->getErrors(TRUE, FALSE);
        return $errors->count();
    }

    /**
     * @param FormInterface $formInterface
     * @return null|string
     */
    public static function getFirstErrorElementName(FormInterface $formInterface)
    {
        $errors = $formInterface->getErrors(TRUE, FALSE);

        if (!($errors->count() > 0)) {
            return NULL;
        }
        if ($errors->hasChildren()) {
            return $errors[0]->getForm()->getConfig()->getName() . '_' . $errors->getChildren()[0]->getForm()->getConfig()->getName();
        }
        $formError = NULL;
        if ($errors[0] instanceof FormError) {
            $formError = $errors[0]->getOrigin();
        } else {
            $formError = $errors[0]->getForm();
        }

        return $formError->getConfig()->getName();
    }

    /**
     * @param FormInterface $form
     * @return string|bool
     */
    public static function getFirstError(FormInterface $form)
    {
        $errors = self::toArray($form);
        return reset($errors);
    }

    /**
     * @param FormInterface $form
     * @return array
     */
    public static function toArray(FormInterface $form)
    {
        $errors = [];
        if ($form->count() > 0) {
            foreach ($form->all() as $child) {
                if (!$child->isValid()) {
                    $errors[$child->getName()] = self::toArray($child);
                }
            }
        } else {
            foreach ($form->getErrors() as $key => $error) {
                $errors[] = $error->getMessage();
            }
        }

        return $errors;
    }
}