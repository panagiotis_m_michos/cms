<?php

namespace FormModule\Types;

use Symfony\Component\Form\AbstractType;

class PaymentType extends AbstractType
{
    /**
     * @return string
     */
    public function getParent()
    {
        return 'choice';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'paymentType';
    }

}