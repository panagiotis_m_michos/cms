<?php

namespace FormModule\Types;

use Symfony\Component\Form\AbstractType;

class TelType extends AbstractType
{

    /**
     * @return string
     */
    public function getName()
    {
        return 'tel';
    }

    /**
     * @return string
     */
    public function getParent()
    {
        return 'text';
    }

}
