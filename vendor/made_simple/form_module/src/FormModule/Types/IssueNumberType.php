<?php

namespace FormModule\Types;

use Symfony\Component\Form\AbstractType;

class IssueNumberType extends AbstractType
{
    /**
     * @return string
     */
    public function getParent()
    {
        return new TelType();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'issueNumber';
    }

}