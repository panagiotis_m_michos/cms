<?php

namespace FormModule\Types;

use Symfony\Component\Form\AbstractType;

class CardHolderType extends AbstractType
{
    /**
     * @return string
     */
    public function getParent()
    {
        return 'text';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cardholder';
    }

}