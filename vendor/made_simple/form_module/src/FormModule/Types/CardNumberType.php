<?php

namespace FormModule\Types;

use Symfony\Component\Form\AbstractType;

class CardNumberType extends AbstractType
{
    /**
     * @return string
     */
    public function getParent()
    {
        return new TelType();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cardNumber';
    }

}