<?php

namespace FormModule\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Jaik Dean <jaik@fluoresce.co>
 */
class EmbedValid extends Constraint
{
    /**
     * @var array Embedded groups
     */
    public $groups = [Constraint::DEFAULT_GROUP];
}