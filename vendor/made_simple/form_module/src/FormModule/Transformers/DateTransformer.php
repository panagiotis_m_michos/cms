<?php

namespace FormModule\Transformers;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Utils\Date;

class DateTransformer implements DataTransformerInterface
{
    /**
     * @var string
     */
    private $format;

    /**
     * @param string $format
     */
    public function __construct($format)
    {
        $this->format = $format;
    }

    /**
     * @param mixed $date
     * @return string
     */
    public function transform($date)
    {
        if (NULL === $date) {
            return '';
        }

        if (!$date instanceof Date) {
            throw new TransformationFailedException('Expected a Date.');
        }
        return $date->format($this->format);
    }

    /**
     * @param mixed $value
     * @return Date|NULL
     */
    public function reverseTransform($value)
    {
        if (empty($value)) {
            return NULL;
        }

        if (!is_string($value)) {
            throw new TransformationFailedException('Expected a string.');
        }
        $date = Date::createFromFormat($this->format, $value);
        if (!($date instanceof Date)) {
            throw new TransformationFailedException(sprintf('Invalid date %s provided. Expected format %s!', $value, $this->format));
        }
        return $date;
    }
}