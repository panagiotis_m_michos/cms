<?php

namespace FormModule\Transformers;

use Symfony\Component\Form\DataTransformerInterface;

class IncompleteDateTransformer implements DataTransformerInterface
{
    /**
     * Do nothing when transforming from norm -> view
     */
    public function transform($object)
    {
        return $object;
    }

    /**
     * If some components of the date is missing we'll add those.
     * This reverse transform will work when day is missing
     */
    public function reverseTransform($date)
    {
        if (!is_array($date)) {
            return $date;
        }

        if (empty($date['year'])) {
            return $date;
        }

        if (empty($date['month'])) {
            return $date;
        }
        $date['day'] = 1;
        return $date;
    }
}