# SagePay Reporting library

Library for accessing SagePay Reporting & Admin API

All possible commands can be found in [Reporting & Admin API Integration Guideline](https://trello-attachments.s3.amazonaws.com/5317360aac3125fd6b3df09c/537a5b53e4a34711d33d2bc5/30018480ea805ee89450a90226a92660/Reporting_and_Admin_API_Integration_Guideline_31012014.pdf)

For every command you have to implement Request and Response classes with all required fields and command name according to API document. Instance of Request class should be then passed to method executing specific command.

Usage (executing getTokenDetails command):

~~~
// packagesDir is path to vendor libraries (usually VENDOR_DIR)
$sagepay = SagePayFactory::createGateway($this->packagesDir);
// response is instance of object according to command executed (TokenDetails in this case)
// TokenDetails is Request object used for "getTokenDetails" command and requires token identifier
$response = $sagepay->getTokenDetails(new TokenDetails($token->identifier));
~~~
