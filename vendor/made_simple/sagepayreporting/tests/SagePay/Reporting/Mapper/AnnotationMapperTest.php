<?php

namespace SagePay\Reporting\Mapper;

use Doctrine\Common\Annotations\AnnotationReader;
use SagePay\Reporting\Annotations\SpField;
use SagePay\Reporting\Request\TokenDetails as TokenDetailsRequest;
use SagePay\Reporting\Response\TokenDetails as TokenDetailsResponse;
use SagePay\Reporting\SagePayFactory;

class AnnotationMapperTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var AnnotationMapper
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        SagePayFactory::register(VENDOR_DIR);

        $annotationName = get_class(new SpField());
        $this->object = new AnnotationMapper(new AnnotationReader(), $annotationName);
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        
    }

    /**
     * @covers SagePay\Reporting\Mapper\AnnotationMapper::objectToArray
     */
    public function testObjectToArray()
    {
        $request = new TokenDetailsRequest('abcdef');
        $requestArray = $this->object->objectToArray($request);

        $this->assertNotEmpty($requestArray);
        $this->assertEquals($requestArray['command'], $request->getCommand());
        $this->assertEquals($requestArray['token'], $request->getToken());
    }

    /**
     * @covers SagePay\Reporting\Mapper\AnnotationMapper::arrayToObject
     */
    public function testArrayToObject()
    {
        $responseArray = array(
            'errorcode' => '0000',
            'token' => 'abcdef'
        );
        $response = new TokenDetailsResponse();
        $this->object->arrayToObject($responseArray, $response);

        $this->assertEquals($responseArray['errorcode'], $response->getErrorCode());
        $this->assertEquals($responseArray['token'], $response->getToken());
    }

}
