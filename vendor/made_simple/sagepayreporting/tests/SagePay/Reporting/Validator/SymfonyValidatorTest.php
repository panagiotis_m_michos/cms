<?php

namespace SagePay\Reporting\Validator;

use PHPUnit_Framework_TestCase;
use SagePay\Reporting\Identity;
use SagePay\Reporting\Request\TokenDetails;
use SagePay\Reporting\SagePayFactory;
use Symfony\Component\Validator\Validation;

class SymfonyValidatorTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var SymfonyValidator
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        SagePayFactory::register(VENDOR_DIR);

        $validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();
        $this->object = new SymfonyValidator($validator);
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        
    }

    /**
     * @covers SagePay\Reporting\Validator\SymfonyValidator::validate
     */
    public function testValidateIdentity()
    {
        $identity = new Identity('testVendor', 'testUser', 'testPassword');
        $this->assertTrue($this->object->validate($identity));
    }

    /**
     * @covers SagePay\Reporting\Validator\SymfonyValidator::validate
     * @expectedException \SagePay\Reporting\Exception\ValidationError
     */
    public function testValidateIdentityEmptyVendor()
    {
        $identity = new Identity('', 'testUser', 'testPassword');
        $this->assertTrue($this->object->validate($identity));
    }

    /**
     * @covers SagePay\Reporting\Validator\SymfonyValidator::validate
     */
    public function testValidateTokenDetails()
    {
        $request = new TokenDetails('1234567890123456789012345678901234567890');
        $this->assertTrue($this->object->validate($request));
    }

    /**
     * @covers SagePay\Reporting\Validator\SymfonyValidator::validate
     * @expectedException \SagePay\Reporting\Exception\ValidationError
     */
    public function testValidateTokenDetailsLongToken()
    {
        $request = new TokenDetails('12345678901234567890123456789012345678901');
        $this->assertTrue($this->object->validate($request));
    }

    /**
     * @covers SagePay\Reporting\Validator\SymfonyValidator::validate
     * @expectedException \SagePay\Reporting\Exception\ValidationError
     */
    public function testValidateTokenDetailsEmptyToken()
    {
        $request = new TokenDetails('');
        $this->assertTrue($this->object->validate($request));
    }
}
