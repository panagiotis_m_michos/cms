<?php

namespace SagePay\Reporting;

use Doctrine\Common\Annotations\AnnotationReader;
use PHPUnit_Framework_TestCase;
use SagePay\Reporting\Mapper\AnnotationMapper;
use SagePay\Reporting\Request\TokenDetails as TokenDetailsRequest;
use SagePay\Reporting\Response\TokenDetails as TokenDetailsResponse;
use Symfony\Component\Validator\Validation;

class CommunicatorTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Communicator
     */
    protected $object;

    /**
     * @var Identity
     */
    private $identity;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        SagePayFactory::register(VENDOR_DIR);

        $xmlString = '<vspaccess><errorcode>0000</errorcode><token>abcdef</token><last4digits>1234</last4digits></vspaccess>';
        $transfer = $this->getMock('SagePay\Reporting\Transfer', array('makeRequest'));
        $transfer->expects($this->once())->method('makeRequest')->will($this->returnValue($xmlString));
        $annotationName = get_class(new Annotations\SpField());
        $annotationReader = new AnnotationReader();
        $mapper = new AnnotationMapper($annotationReader, $annotationName);
        $validator = Validation::createValidatorBuilder()->enableAnnotationMapping($annotationReader)->getValidator();
        $symfonyValidator = new Validator\SymfonyValidator($validator);
        $this->identity = new Identity('testVendor', 'testUser', 'testPassword');
        $this->object = new Communicator($transfer, $mapper, $symfonyValidator);
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {

    }

    /**
     * @covers SagePay\Reporting\Communicator::submit
     */
    public function testSubmitTokenDetails()
    {
        $request = new TokenDetailsRequest('abcdef');
        /** @var $response TokenDetailsResponse */
        $response = $this->object->submit($request, $this->identity);

        $this->assertInstanceOf('SagePay\Reporting\Response\TokenDetails', $response);
        $this->assertEquals($response->getToken(), 'abcdef');
        $this->assertEquals($response->getLastDigits(), '1234');
    }
}
