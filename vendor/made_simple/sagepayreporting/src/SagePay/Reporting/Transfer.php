<?php

namespace SagePay\Reporting;

use BadFunctionCallException;
use Nette\Object;

class Transfer extends Object
{
    /**
     * @param string $url
     * @param array $requestData
     * @return mixed
     * @throws ConnectionException
     */
    public function makeRequest($url, array $requestData)
    {
        if (!function_exists('curl_setopt')) {
            throw new BadFunctionCallException("Curl is not installed in your server.");
        }

        $curlSession = curl_init();
        $sendData = http_build_query($requestData);

        // Set curl options
        curl_setopt($curlSession, CURLOPT_URL, $url);
        curl_setopt($curlSession, CURLOPT_HEADER, 0);
        curl_setopt($curlSession, CURLOPT_POST, 1);
        curl_setopt($curlSession, CURLOPT_POSTFIELDS, $sendData);
        curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlSession, CURLOPT_TIMEOUT, 30);
        curl_setopt($curlSession, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curlSession, CURLOPT_SSL_VERIFYHOST, 2);

        // Execute request
        $response = curl_exec($curlSession);
        $responseCode = curl_getinfo($curlSession, CURLINFO_HTTP_CODE);

        // Verify that the request executed successfully.
        if (curl_errno($curlSession)) {
            $message = curl_error($curlSession);
            curl_close($curlSession);
            throw new ConnectionException("A problem occured when posting data to Sage Pay. Message:`$message`");
        } elseif ($responseCode !== 200) {
            throw new ConnectionException("A problem occured when posting data to Sage Pay. Message:`Wrong response code `{$responseCode}` returned`");
        } else {
            curl_close($curlSession);
        }

        return $response;
    }
}
