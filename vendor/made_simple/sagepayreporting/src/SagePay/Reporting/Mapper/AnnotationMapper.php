<?php

namespace SagePay\Reporting\Mapper;

use Doctrine\Common\Annotations\Reader;
use Nette\Object;
use ReflectionClass;
use ReflectionProperty;
use SagePay\Reporting\Identity;
use SagePay\Reporting\Request\Request_Abstract;
use SagePay\Reporting\Response\Response_Abstract;

class AnnotationMapper extends Object
{

    /**
     * @var Reader
     */
    private $reader;
    
    /**
     * @var string
     */
    private $annotationName;

    /**
     * @param Reader $reader
     * @param string $annotationName
     */
    public function __construct(Reader $reader, $annotationName)
    {
        $this->reader = $reader;
        $this->annotationName = $annotationName;
    }

    /**
     * assumes that setters and getters exists for object properties
     * @param Request_Abstract|Response_Abstract $object
     * @param Identity $identity
     * @return array
     */
    public function objectToArray($object, Identity $identity = NULL)
    {
        $arr = $this->propertiesToArray($object);
        if (!empty($identity)) {
            $identityArr = $this->propertiesToArray($identity);
            return array_merge($arr, $identityArr);
        }
        return $arr;
    }

    /**
     * assumes that setters and getters exists for object properties
     * @param array $arr
     * @param Response_Abstract|Request_Abstract $object
     * @return Response_Abstract|Request_Abstract
     */
    public function arrayToObject(array $arr, $object)
    {
        $reflClass = new ReflectionClass($object);
        foreach ($reflClass->getProperties() as $property) {
            $propertyAnnotation = $this->reader->getPropertyAnnotation($property, $this->annotationName);
            if ($propertyAnnotation) {
                if (isset($arr[$propertyAnnotation->name])) {
                    $propertyName = $property->getName();
                    $camelCaseMethodName = 'set' . ucfirst($propertyName);
                    if ($reflClass->hasMethod($camelCaseMethodName)) {
                        $object->$camelCaseMethodName($arr[$propertyAnnotation->name]);
                    }
                }
            }
        }
        return $object;
    }

    /**
     * @param mixed $object
     * @return array
     */
    private function propertiesToArray($object)
    {
        $reflClass = new ReflectionClass($object);
        $propertyArray = array();
        foreach ($reflClass->getProperties(ReflectionProperty::IS_PRIVATE | ReflectionProperty::IS_PROTECTED) as $property) {
            if ($property->isStatic()) {
                continue;
            }
            $propertyName = $property->getName();
            $camelCaseMethodName = 'get' . ucfirst($propertyName);
            if ($reflClass->hasMethod($camelCaseMethodName)) {
                if ($object->$camelCaseMethodName() !== NULL) {
                    $propertyAnnotation = $this->reader->getPropertyAnnotation($property, $this->annotationName);
                    if ($propertyAnnotation) {
                        $propertyArray[$propertyAnnotation->name] = $object->$camelCaseMethodName();
                    }
                }
            }
        }
        return $propertyArray;
    }
}
