<?php

namespace SagePay\Reporting;

use Nette\Object;
use SagePay\Reporting\Annotations\SpField;
use Symfony\Component\Validator\Constraints as Assert;

class Identity extends Object
{
    /**
     * Vendor Login Name
     * @var string
     * @Assert\NotBlank(message="Vendor must not be empty")
     * @Assert\Length(max=15, maxMessage="Vendor must not be more than {{ limit }} characters")
     * @SpField(name="vendor")
     */
    private $vendor;

    /**
     * User login
     * @var string
     * @Assert\NotBlank(message="User must not be empty")
     * @Assert\Length(max=20, maxMessage="User must not be more than {{ limit }} characters")
     * @SpField(name="user")
     */
    private $user;

    /**
     * User password
     * @var string
     * @Assert\NotBlank(message="Password must not be empty")
     * @SpField(name="password")
     */
    private $password;

    /**
     * @param string $vendor
     * @param string $user
     * @param string $password
     */
    public function __construct($vendor, $user, $password)
    {
        $this->vendor = $vendor;
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getVendor()
    {
        return $this->vendor;
    }

    /**
     * @param string $vendor
     */
    public function setVendor($vendor)
    {
        $this->vendor = $vendor;
    }

    /**
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param string $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }
}
