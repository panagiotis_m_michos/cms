<?php

namespace SagePay\Reporting\Validator;

interface IValidator
{
    public function validate($object);
}
