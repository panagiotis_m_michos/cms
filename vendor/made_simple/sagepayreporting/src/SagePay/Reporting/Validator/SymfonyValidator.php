<?php

namespace SagePay\Reporting\Validator;

use SagePay\Reporting\ValidationException;
use Symfony\Component\Validator\ValidatorInterface;

class SymfonyValidator implements IValidator
{
    /**
     * @var ValidatorInterface 
     */
    private $validator;

    /**
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param mixed $object
     * @throws ValidationException
     * @return boolean
     */
    public function validate($object)
    {
        $violations = $this->validator->validate($object);
        if ($violations->count() === 0) {
            return TRUE;
        }
        foreach ($violations as $violation) {
            throw new ValidationException($violation->getMessage());
        }
    }

}
