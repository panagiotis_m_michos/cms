<?php

namespace SagePay\Reporting;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\Common\Annotations\CachedReader;
use Doctrine\Common\Cache\ArrayCache;
use FApplication;
use Nette\Object;
use SagePay\Reporting\Mapper\AnnotationMapper;
use SagePay\Reporting\Validator\SymfonyValidator;
use Symfony\Component\Validator\Validation;

class SagePayFactory extends Object
{
    /**
     * @var AnnotationReader annotation reader
     */
    private static $annotationReader;

    /**
     * @param string $packagesDir
     */
    public static function register($packagesDir)
    {
        AnnotationRegistry::registerAutoloadNamespace('Symfony\Component\Validator\Constraints', $packagesDir . 'symfony/validator');
        AnnotationRegistry::registerAutoloadNamespace('SagePay\Reporting\Annotations', $packagesDir . 'made_simple/sagepayreporting/src');
    }

    /**
     * @param string $packagesDir
     * @return SagePay
     */
    public static function createGateway($packagesDir)
    {
        self::register($packagesDir);

        $config = FApplication::$config['sage'];
        $identity = new Identity($config['vendor'], $config['reportingUser'], $config['reportingPassword']);
        $transfer = new Transfer();
        $symfonyValidator = self::createValidator();
        $mapper = self::createAnnotationMapper();
        $communicator = new Communicator($transfer, $mapper, $symfonyValidator, $config['server']);
        $sagePay = new SagePay($communicator, $identity);
        return $sagePay;
    }

    /**
     * @return AnnotationMapper
     */
    public static function createAnnotationMapper()
    {
        $annotationName = get_class(new Annotations\SpField());
        $annotationReader = self::getAnnotationReader();
        $mapper = new AnnotationMapper($annotationReader, $annotationName);
        return $mapper;
    }

    /**
     * @return AnnotationReader|CachedReader
     */
    public static function getAnnotationReader()
    {
        return self::$annotationReader ? self::$annotationReader : new CachedReader(new AnnotationReader(), new ArrayCache());
    }

    /**
     * @return Validator\SymfonyValidator
     */
    public static function createValidator()
    {
        $annotationReader = self::getAnnotationReader();
        $validator = Validation::createValidatorBuilder()->enableAnnotationMapping($annotationReader)->getValidator();
        return new SymfonyValidator($validator);
    }
}
