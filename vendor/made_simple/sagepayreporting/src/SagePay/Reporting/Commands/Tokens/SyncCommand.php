<?php

namespace SagePay\Reporting\Commands\Tokens;

use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use SagePay\Reporting\Interfaces\ISagePay;
use SagePay\Reporting\Interfaces\IToken;
use SagePay\Reporting\Interfaces\ITokenService;
use SagePay\Reporting\ResponseException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SyncCommand extends Command
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ITokenService
     */
    private $tokenService;

    /**
     * @var ISagePay
     */
    private $sagePay;

    /**
     * @param EntityManagerInterface $entityManager
     * @param LoggerInterface $logger
     * @param ITokenService $tokenService
     * @param ISagePay $sagePay
     * @param string|NULL $name
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        LoggerInterface $logger,
        ITokenService $tokenService,
        ISagePay $sagePay,
        $name = NULL
    )
    {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
        $this->tokenService = $tokenService;
        $this->sagePay = $sagePay;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('sage:tokensSync')
            ->setDescription('Remove expired tokens from our DB')
            ->addOption(
                'dry-run',
                NULL,
                InputOption::VALUE_NONE,
                'Simulate execution of this command'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return NULL
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->logMessage($output, "Execution started: {$this->getName()}");

        $isDryRun = $input->getOption('dry-run');
        if ($isDryRun) {
            $this->logMessage($output, 'Running in dry mode, no changes will be made.');
        }

        $counter = 0;
        $activeTokens = $this->tokenService->getAllTokens();
        foreach ($activeTokens as $localTokenData) {
            $counter++;
            if ($counter % 100 == 0) {
                $this->entityManager->flush();
                $this->entityManager->clear();
            }

            /** @var IToken $localToken */
            $localToken = $localTokenData[0];
            if ($localToken->isActive()) {
                $isActiveOnSagePay = $this->sagePay->isTokenActive($localToken->getIdentifier());

                if (!$isActiveOnSagePay) {
                    $this->logMessage(
                        $output,
                        "Token {$localToken->getIdentifier()} is set as ACTIVE, but does not exist in Sage and will be removed!"
                    );

                    if (!$isDryRun) {
                        $this->tokenService->removeToken($localToken, GEDMO_LOGGABLE_USERNAME);
                    }
                }
            } else {
                $this->logMessage(
                    $output,
                    "Token {$localToken->getIdentifier()} is set as DELETED, will be removed!"
                );

                if (!$isDryRun) {
                    $this->tokenService->removeToken($localToken, GEDMO_LOGGABLE_USERNAME);
                }
            }
        }
        $this->logMessage(
            $output,
            "Execution finished: {$this->getName()}"
        );
    }

    /**
     * @param OutputInterface $output
     * @param string $message
     * @param string $level
     */
    private function logMessage(OutputInterface $output, $message, $level = LogLevel::DEBUG)
    {
        $output->writeln($message);
        $this->logger->log($level, strip_tags($message));
    }
}
