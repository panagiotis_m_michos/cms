<?php

namespace SagePay\Reporting\Response;

use Nette\Object;
use SagePay\Reporting\Annotations\SpField;

abstract class Response_Abstract extends Object implements IResponse
{
    /**
     * @var string
     * @SpField(name="errorcode")
     */
    protected $errorCode;

    /**
     * @var string
     * @SpField(name="error")
     */
    protected $error;

    /**
     * @var string
     * @SpField(name="timestamp")
     */
    protected $timestamp;

    /**
     * contains raw response from sage pay
     * @var string
     */
    protected $rawResponse;

    /**
     * @return string
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param string $errorcode
     */
    public function setErrorCode($errorcode)
    {
        $this->errorCode = $errorcode;
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param string $error
     */
    public function setError($error)
    {
        $this->error = $error;
    }

    /**
     * @return string
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param string $timestamp
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }

    /**
     * @return string
     */
    public function getRawResponse()
    {
        return $this->rawResponse;
    }

    /**
     * @param $rawResponse
     */
    public function setRawResponse($rawResponse)
    {
        $this->rawResponse = $rawResponse;
    }

    /**
     * @return bool
     */
    public function isSuccess()
    {
        return $this->getErrorCode() == '0000';
    }
}
