<?php

namespace SagePay\Reporting\Response;

use SagePay\Reporting\Annotations\SpField;
use SagePay\Reporting\Annotations\SpBoolean;

class TokenDetails extends Response_Abstract
{
    /**
     * @var string
     * @SpField(name="token")
     */
    private $token;

    /**
     * @var string
     * @SpField(name="cardholder")
     */
    private $cardHolder;

    /**
     * @var string
     * @SpField(name="last4digits")
     */
    private $lastDigits;

    /**
     * @var string
     * @SpField(name="paymentsystem")
     */
    private $paymentSystem;

    /**
     * @var string
     * @SpField(name="corporatecard")
     */
    private $corporateCard;

    /**
     * @var string
     * @SpField(name="iscredit")
     */
    private $isCredit;

    /**
     * @var string
     * @SpField(name="expirydate")
     */
    private $expiryDate;

    /**
     * @var string
     * @SpField(name="uses")
     */
    private $uses;

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getCardHolder()
    {
        return $this->cardHolder;
    }

    /**
     * @param string $cardHolder
     */
    public function setCardHolder($cardHolder)
    {
        $this->cardHolder = $cardHolder;
    }

    /**
     * @return string
     */
    public function getCorporateCard()
    {
        return $this->corporateCard;
    }

    /**
     * @param string $corporateCard
     */
    public function setCorporateCard($corporateCard)
    {
        $this->corporateCard = $corporateCard;
    }

    /**
     * @return string
     */
    public function getExpiryDate()
    {
        return $this->expiryDate;
    }

    /**
     * @param string $expiryDate
     */
    public function setExpiryDate($expiryDate)
    {
        $this->expiryDate = $expiryDate;
    }

    /**
     * @return string
     */
    public function getIsCredit()
    {
        return $this->isCredit;
    }

    /**
     * @param string $isCredit
     */
    public function setIsCredit($isCredit)
    {
        $this->isCredit = $isCredit;
    }

    /**
     * @return string
     */
    public function getLastDigits()
    {
        return $this->lastDigits;
    }

    /**
     * @param string $lastDigits
     */
    public function setLastDigits($lastDigits)
    {
        $this->lastDigits = $lastDigits;
    }

    /**
     * @return string
     */
    public function getPaymentSystem()
    {
        return $this->paymentSystem;
    }

    /**
     * @param string $paymentSystem
     */
    public function setPaymentSystem($paymentSystem)
    {
        $this->paymentSystem = $paymentSystem;
    }

    /**
     * @return string
     */
    public function getUses()
    {
        return $this->uses;
    }

    /**
     * @param string $uses
     */
    public function setUses($uses)
    {
        $this->uses = $uses;
    }
}
