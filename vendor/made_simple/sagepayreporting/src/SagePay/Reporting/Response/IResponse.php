<?php

namespace SagePay\Reporting\Response;

interface IResponse
{
    /**
     * @param $rawResponse
     */
    public function setRawResponse($rawResponse);

    /**
     * @return bool
     */
    public function isSuccess();

    /**
     * @return string
     */
    public function getErrorCode();

    /**
     * @return string
     */
    public function getError();
}
