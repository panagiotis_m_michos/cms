<?php

namespace SagePay\Reporting\Response;

use SagePay\Reporting\Annotations\SpField;

class TokenCount extends Response_Abstract
{
    /**
     * @var string
     * @SpField(name="totalnumber")
     */
    private $totalNumber;

    /**
     * @return string
     */
    public function getTotalNumber()
    {
        return $this->totalNumber;
    }

    /**
     * @param string $token
     */
    public function setTotalNumber($token)
    {
        $this->totalNumber = $token;
    }


}
