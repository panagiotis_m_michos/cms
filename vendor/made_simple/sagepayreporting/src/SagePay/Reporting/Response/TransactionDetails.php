<?php

namespace SagePay\Reporting\Response;

use SagePay\Reporting\Annotations\SpField;
use SagePay\Reporting\Annotations\SpBoolean;

class TransactionDetails extends Response_Abstract
{
    /**
     * @var string
     * @SpField(name="errorcode")
     */
    protected $errorCode;

    /**
     * @var string
     * @SpField(name="error")
     */
    protected $error;

    /**
     * @var string
     * @SpField(name="vpstxid")
     */
    private $vpsTxId;

    /**
     * @var string
     * @SpField(name="vendortxcode")
     */
    private $vendorTxCode;

    /**
     * @var string
     * @SpField(name="txstateid")
     */
    private $txStateId;

    /**
     * @var string
     * @SpField(name="status")
     */
    private $status;

    /**
     * @var string
     * @SpField(name="paymentsystem")
     */
    private $paymentSystem;

    /**
     * @var string
     * @SpField(name="expirydate")
     */
    private $expiryDate;

    /**
     * @var string
     * @SpField(name="last4digits")
     */
    private $lastDigits;

    /**
     * @var string
     * @SpField(name="billingfirstnames")
     */
    private $billingFirstNames;

    /**
     * @var string
     * @SpField(name="billingsurname")
     */
    private $billingSurname;

    /**
     * @var string
     * @SpField(name="billingaddress")
     */
    private $billingAddress;

    /**
     * @var string
     * @SpField(name="billingaddress2")
     */
    private $billingAddress2;

    /**
     * @var string
     * @SpField(name="billingcity")
     */
    private $billingCity;

    /**
     * @var string
     * @SpField(name="billingstate")
     */
    private $billingState;

    /**
     * @var string
     * @SpField(name="billingpostcode")
     */
    private $billingPostCode;

    /**
     * @var string
     * @SpField(name="billingcountry")
     */
    private $billingCountry;

    /**
     * @var string
     * @SpField(name="cardholder")
     */
    private $cardHolder;

    /**
     * @param string $billingAddress
     */
    public function setBillingAddress($billingAddress)
    {
        $this->billingAddress = $billingAddress;
    }

    /**
     * @return string
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }

    /**
     * @param string $billingAddress2
     */
    public function setBillingAddress2($billingAddress2)
    {
        $this->billingAddress2 = $billingAddress2;
    }

    /**
     * @return string
     */
    public function getBillingAddress2()
    {
        return $this->billingAddress2;
    }

    /**
     * @param string $billingCity
     */
    public function setBillingCity($billingCity)
    {
        $this->billingCity = $billingCity;
    }

    /**
     * @return string
     */
    public function getBillingCity()
    {
        return $this->billingCity;
    }

    /**
     * @param string $billingCountry
     */
    public function setBillingCountry($billingCountry)
    {
        $this->billingCountry = $billingCountry;
    }

    /**
     * @return string
     */
    public function getBillingCountry()
    {
        return $this->billingCountry;
    }

    /**
     * @param string $billingFirstNames
     */
    public function setBillingFirstNames($billingFirstNames)
    {
        $this->billingFirstNames = $billingFirstNames;
    }

    /**
     * @return string
     */
    public function getBillingFirstNames()
    {
        return $this->billingFirstNames;
    }

    /**
     * @param string $billingPostCode
     */
    public function setBillingPostCode($billingPostCode)
    {
        $this->billingPostCode = $billingPostCode;
    }

    /**
     * @return string
     */
    public function getBillingPostCode()
    {
        return $this->billingPostCode;
    }

    /**
     * @param string $billingState
     */
    public function setBillingState($billingState)
    {
        $this->billingState = $billingState;
    }

    /**
     * @return string
     */
    public function getBillingState()
    {
        return $this->billingState;
    }

    /**
     * @param string $billingSurname
     */
    public function setBillingSurname($billingSurname)
    {
        $this->billingSurname = $billingSurname;
    }

    /**
     * @return string
     */
    public function getBillingSurname()
    {
        return $this->billingSurname;
    }

    /**
     * @param string $cardHolder
     */
    public function setCardHolder($cardHolder)
    {
        $this->cardHolder = $cardHolder;
    }

    /**
     * @return string
     */
    public function getCardHolder()
    {
        return $this->cardHolder;
    }

    /**
     * @param string $error
     */
    public function setError($error)
    {
        $this->error = $error;
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param string $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return string
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param string $expiryDate
     */
    public function setExpiryDate($expiryDate)
    {
        $this->expiryDate = $expiryDate;
    }

    /**
     * @return string
     */
    public function getExpiryDate()
    {
        return $this->expiryDate;
    }

    /**
     * @param string $lastDigits
     */
    public function setLastDigits($lastDigits)
    {
        $this->lastDigits = $lastDigits;
    }

    /**
     * @return string
     */
    public function getLastDigits()
    {
        return $this->lastDigits;
    }

    /**
     * @param string $paymentSystem
     */
    public function setPaymentSystem($paymentSystem)
    {
        $this->paymentSystem = $paymentSystem;
    }

    /**
     * @return string
     */
    public function getPaymentSystem()
    {
        return $this->paymentSystem;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $txStateId
     */
    public function setTxStateId($txStateId)
    {
        $this->txStateId = $txStateId;
    }

    /**
     * @return string
     */
    public function getTxStateId()
    {
        return $this->txStateId;
    }

    /**
     * @param string $vendorTxCode
     */
    public function setVendorTxCode($vendorTxCode)
    {
        $this->vendorTxCode = $vendorTxCode;
    }

    /**
     * @return string
     */
    public function getVendorTxCode()
    {
        return $this->vendorTxCode;
    }

    /**
     * @param string $vpsTxId
     */
    public function setVpsTxId($vpsTxId)
    {
        $this->vpsTxId = $vpsTxId;
    }

    /**
     * @return string
     */
    public function getVpsTxId()
    {
        return $this->vpsTxId;
    }


}
