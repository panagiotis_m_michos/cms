<?php

namespace SagePay\Reporting;

use Nette\Object;
use SagePay\Reporting\Mapper\AnnotationMapper;
use SagePay\Reporting\Request\IRequest;
use SagePay\Reporting\Response\IResponse;
use SagePay\Reporting\Validator\IValidator;
use SimpleXMLElement;

class Communicator extends Object
{
    const ENVIRONMENT_LIVE = 'LIVE';
    const ENVIRONMENT_TEST = 'TEST';

    /**
     * @var Transfer
     */
    protected $transfer;

    /**
     * @var AnnotationMapper
     */
    protected $mapper;

    /**
     * @var IValidator
     */
    protected $validator;

    /**
     * @var bool
     */
    protected $environment;

    /**
     * @param Transfer $transfer
     * @param AnnotationMapper $mapper
     * @param IValidator $validator
     * @param string $environment
     */
    public function __construct(Transfer $transfer, AnnotationMapper $mapper, IValidator $validator, $environment = self::ENVIRONMENT_TEST)
    {
        $this->transfer = $transfer;
        $this->mapper = $mapper;
        $this->validator = $validator;
        $this->environment = $environment;
    }

    /**
     * @param IRequest $request
     * @param Identity $identity
     * @return IResponse
     * @throws ValidationException
     * @throws ConnectionException
     */
    public function submit(IRequest $request, Identity $identity)
    {
        $this->validator->validate($request);
        $this->validator->validate($identity);
        $url = $request->getUrl($this->environment);
        $requestArray = $this->mapper->objectToArray($request, $identity);
        $requestData = $this->buildData($requestArray);
        $responseString = $this->transfer->makeRequest($url, $requestData);
        $responseArray = $this->buildArray($responseString);
        $response = $request->createResponse();
        $this->mapper->arrayToObject($responseArray, $response);
        $response->setRawResponse($responseString);
        return $response;
    }

    /**
     * @param string $xmlString
     * @return array
     */
    private function buildArray($xmlString)
    {
        $array = new SimpleXMLElement($xmlString);
        return (array) $array;
    }

    /**
     * @param array $requestArray
     * @return array
     */
    private function buildData(array $requestArray)
    {
        $commandArray = $requestArray;
        unset($commandArray['password']);

        $commandString = $this->convertArrayToXmlString($commandArray);
        $signatureString = md5($this->convertArrayToXmlString($requestArray));
        $requestString = '<?xml version="1.0"?><vspaccess>' . $commandString . '<signature>' . $signatureString . '</signature></vspaccess>';

        return array('XML' => $requestString);
    }

    /**
     * @param array $array
     * @return string
     */
    private function convertArrayToXmlString(array $array)
    {
        $string = '';
        foreach ($array as $key => $value) {
            $string .= "<{$key}>{$value}</{$key}>";
        }
        return $string;
    }
}
