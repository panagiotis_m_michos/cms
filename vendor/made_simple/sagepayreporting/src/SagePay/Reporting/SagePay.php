<?php

namespace SagePay\Reporting;

use Nette\Object;
use SagePay\Reporting\Interfaces\ISagePay;
use SagePay\Reporting\Request\IRequest;
use SagePay\Reporting\Request\TokenDetails as TokenDetailsRequest;
use SagePay\Reporting\Request\TokenCount as TokenCountRequest;
use SagePay\Reporting\Request\TransactionDetails as TransactionDetailsRequest;
use SagePay\Reporting\Response\TransactionDetails as TransactionDetailsResponse;
use SagePay\Reporting\Response\IResponse;
use SagePay\Reporting\Response\TokenDetails as TokenDetailsResponse;
use SagePay\Reporting\Response\TokenCount as TokenCountResponse;

class SagePay extends Object implements ISagePay
{
    const EXCEPTION_CODE_COMPANY_NOT_FOUND = 2236;

    /**
     * @var Communicator
     */
    private $communicator;

    /**
     * identity must persist between request since token registration and issue can be used separately
     * @var Identity
     */
    protected $identity;

    /**
     * @param Communicator $communicator
     * @param Identity $identity
     */
    public function __construct(Communicator $communicator, Identity $identity)
    {
        $this->communicator = $communicator;
        $this->identity = $identity;
    }

    /**
     * @param Communicator $communicator
     */
    public function setCommunicator(Communicator $communicator)
    {
        $this->communicator = $communicator;
    }

    /**
     * @param Identity $identity
     */
    public function setIdentity(Identity $identity)
    {
        $this->identity = $identity;
    }

    /**
     * @param string $token
     * @throws ResponseException
     * @return bool
     */
    public function isTokenActive($token)
    {
        try {
            $this->getTokenDetails(new TokenDetailsRequest($token));
        } catch (ResponseException $e) {
            if ($e->getCode() == self::EXCEPTION_CODE_COMPANY_NOT_FOUND) {
                return FALSE;
            } else {
                throw $e;
            }
        }

        return TRUE;
    }

    /**
     * @param TokenDetailsRequest $request
     * @return TokenDetailsResponse
     */
    public function getTokenDetails(TokenDetailsRequest $request)
    {
        return $this->makeRequest($request);
    }

    /**
     * @param TokenCountRequest $request
     * @return TokenCountResponse
     */
    public function getTokenCount(TokenCountRequest $request)
    {
        return $this->makeRequest($request);
    }

    /**
     * @param TransactionDetailsRequest $request
     * @return TransactionDetailsResponse
     */
    public function getTransactionDetails(TransactionDetailsRequest $request)
    {
        return $this->makeRequest($request);
    }

    /**
     * @param IRequest $request
     * @throws ResponseException
     * @return IResponse
     */
    private function makeRequest(IRequest $request)
    {
        $response = $this->communicator->submit($request, $this->identity);
        if ($response->isSuccess()) {
            return $response;
        }
        throw new ResponseException($response->getError(), $response->getErrorCode());
    }
}
