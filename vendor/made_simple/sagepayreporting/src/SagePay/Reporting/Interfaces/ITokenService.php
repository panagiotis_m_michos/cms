<?php

namespace SagePay\Reporting\Interfaces;

interface ITokenService
{
    /**
     * @return IToken[]
     */
    public function getAllTokens();

    /**
     * @param IToken $token
     * @param string $actionBy
     * @return NULL
     */
    public function removeToken(IToken $token, $actionBy);
} 