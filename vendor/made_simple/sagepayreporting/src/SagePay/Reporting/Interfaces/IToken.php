<?php

namespace SagePay\Reporting\Interfaces;

interface IToken
{
    /**
     * @return bool
     */
    public function isActive();

    /**
     * @return string
     */
    public function getIdentifier();
} 