<?php

namespace SagePay\Reporting\Interfaces;

use SagePay\Reporting\Communicator;
use SagePay\Reporting\Identity;
use SagePay\Reporting\Request\TokenDetails as TokenDetailsRequest;
use SagePay\Reporting\Request\TokenCount as TokenCountRequest;
use SagePay\Reporting\Request\TransactionDetails as TransactionDetailsRequest;
use SagePay\Reporting\Response\TransactionDetails as TransactionDetailsResponse;
use SagePay\Reporting\Response\TokenDetails as TokenDetailsResponse;
use SagePay\Reporting\Response\TokenCount as TokenCountResponse;

interface ISagePay
{
    /**
     * @param string $token
     * @return bool
     */
    public function isTokenActive($token);
}