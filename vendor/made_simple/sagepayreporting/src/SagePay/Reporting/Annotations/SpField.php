<?php

namespace SagePay\Reporting\Annotations;

/**
 * @Annotation
 * @Target({"PROPERTY"})
 */
class SpField
{
    /**
     * @var string
     */
    public $name;
}
