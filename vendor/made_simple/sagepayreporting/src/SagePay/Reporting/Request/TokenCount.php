<?php

namespace SagePay\Reporting\Request;

use SagePay\Reporting\Response\TokenCount as ResponseTokenCount;
use SagePay\Reporting\Annotations\SpField;
use Symfony\Component\Validator\Constraints as Assert;

class TokenCount extends Request_Abstract
{
    /**
     * @var string
     * @Assert\NotBlank(message="Command must not be empty")
     * @SpField(name="command")
     */
    protected $command = 'getTokenCount';

    public function createResponse()
    {
        return new ResponseTokenCount();
    }
}
