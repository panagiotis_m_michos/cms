<?php

namespace SagePay\Reporting\Request;

use Nette\Object;
use SagePay\Reporting\Annotations\SpField;
use SagePay\Reporting\Communicator;
use Symfony\Component\Validator\Constraints as Assert;

abstract class Request_Abstract extends Object implements IRequest
{
    /**
     * live request url
     * @var string
     */
    const LIVE_REQUEST_URL = 'https://live.sagepay.com/access/access.htm';

    /**
     * test request url
     * @var string
     */
    const TEST_REQUEST_URL = 'https://test.sagepay.com/access/access.htm';

    /**
     * @var string
     * @Assert\NotBlank(message="Command must not be empty")
     * @SpField(name="command")
     */
    protected $command;

    /**
     * @return string
     */
    public function getCommand()
    {
        return $this->command;
    }

    /**
     * @param string $environment
     * @return string
     */
    public function getUrl($environment)
    {
        if ($environment == Communicator::ENVIRONMENT_LIVE) {
            return self::LIVE_REQUEST_URL;
        }
        return self::TEST_REQUEST_URL;
    }
}
