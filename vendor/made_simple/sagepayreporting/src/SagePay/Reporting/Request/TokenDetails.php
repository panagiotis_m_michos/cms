<?php

namespace SagePay\Reporting\Request;

use SagePay\Reporting\Annotations\SpField;
use Symfony\Component\Validator\Constraints as Assert;
use SagePay\Reporting\Response\TokenDetails as TokenDetailsResponse;

class TokenDetails extends Request_Abstract
{
    /**
     * @var string
     * @Assert\NotBlank(message="Command must not be empty")
     * @SpField(name="command")
     */
    protected $command = 'getTokenDetails';

    /**
     * @var string
     * @Assert\NotBlank(message="Token must not be empty")
     * @Assert\Length(max=40, maxMessage="Token must not be more than {{ limit }} characters")
     * @SpField(name="token")
     */
    private $token;

    /**
     * @param string $token
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * @return TokenDetailsResponse
     */
    public function createResponse()
    {
        return new TokenDetailsResponse();
    }

    /**
     * @return string
     */
    public function getCommand()
    {
        return $this->command;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

}
