<?php

namespace SagePay\Reporting\Request;

use SagePay\Reporting\Annotations\SpField;
use Symfony\Component\Validator\Constraints as Assert;
use SagePay\Reporting\Response\TransactionDetails as TransactionDetailsResponse;

class TransactionDetails extends Request_Abstract
{
    /**
     * @var string
     * @Assert\NotBlank(message="Command must not be empty")
     * @SpField(name="command")
     */
    protected $command = 'getTransactionDetail';

    /**
     * @var string
     * @Assert\NotBlank(message="Transaction must not be empty")
     * @Assert\Length(max=40, maxMessage="Transaction code must not be more than {{ limit }} characters")
     * @SpField(name="vendortxcode")
     */
    private $transactionCode;

    /**
     * @param $transactionCode
     */
    public function __construct($transactionCode)
    {
        $this->transactionCode = $transactionCode;
    }

    /**
     * @return TransactionDetailsResponse
     */
    public function createResponse()
    {
        return new TransactionDetailsResponse();
    }

    /**
     * @return string
     */
    public function getCommand()
    {
        return $this->command;
    }

    /**
     * @return string
     */
    public function getTransactionCode()
    {
        return $this->transactionCode;
    }

    /**
     * @param string $transactionCode
     */
    public function setTransactionCode($transactionCode)
    {
        $this->transactionCode = $transactionCode;
    }

}
