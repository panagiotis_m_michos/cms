<?php

namespace SagePay\Reporting\Request;

use SagePay\Reporting\Response\IResponse;

interface IRequest
{
    /**
     * @param string $environment
     * @return string
     */
    public function getUrl($environment);

    /**
     * @return IResponse
     */
    public function createResponse();
}
