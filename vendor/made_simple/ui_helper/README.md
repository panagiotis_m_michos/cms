Use:

``` php

$parameters = array(
    'login_link' => 'login',
    'image_link' => IMAGES_DIR,
    'menu_data' => FALSE
    //'menu_data' => array($menu, 'getMenu')
);
$ymlLoader = new YamlLoader(PROJECT_DIR . '/libs/UiHelper/config', new Parser(), $parameters);
$mustache = new Mustache_Engine(
    array(
        'cache' => CACHE_DIR . '/mustache',
        'loader' => new Mustache_Loader_FilesystemLoader(COMPONENT_DIR . '/ui_server/templates'),
        //'partials_loader' => new Mustache_Loader_FilesystemLoader(COMPONENT_DIR . '/ui_server/templates/blocks'),
        'strict_callables' => TRUE,
    )
);
//use cache only for production
$cache = $environment === Environment::PRODUCTION ? new FilesystemCache(CACHE_DIR . '/ui') : NULL;
$mustacheViewHelper = new ViewHelper($mustache, $ymlLoader, $cache);
$this->engine->registerPlugin('function', 'ui', array($mustacheViewHelper, 'render'));
return $mustacheViewHelper;

```

Smarty implementation:

``` php

$viewHelper = new ViewHelper($mustache, $ymlLoader, $cache);
$this->engine->registerPlugin('function', 'ui', array($viewHelper, 'render'));

//used in templates
{ui name="header" people="arst" menu_img=$urlImgs menu=$htmlMenu cache=60}
{ui name="header" data=$header}

```

Latte implementation:

``` php

UiLatteHelper::$uiHelper = new ViewHelper($mustache, $ymlLoader, $cache);

//simple static method which contains object ViewHelper
LatteMacros::$defaultMacros['ui'] = '<?php echo UiLatteHelper::$uiHelper->render(%%); ?>';

//used in templates
{ui array(name => "header", people => arst, menu_img => $urlImgs, "menu" => $htmlMenu, cache=60)}
{ui array(template => "header", data => $header)}
{ui array(template => "header", content => "header")}
```
