<?php

namespace UiHelper;

use Doctrine\Common\Cache\Cache;
use InvalidArgumentException;
use Mustache_Engine;

class ViewHelper
{
    /**
     * @var Mustache_Engine
     */
    private $mustacheEngine;

    /**
     * Cache|NULL
     */
    private $cache;

    /**
     * @var array
     */
    private $defaultParameters;

    /**
     * @param Mustache_Engine $mustacheEngine
     * @param Cache $cache
     * @param array $defaultParameters
     */
    public function __construct(Mustache_Engine $mustacheEngine, Cache $cache = NULL, array $defaultParameters = [])
    {
        $this->mustacheEngine = $mustacheEngine;
        $this->cache = $cache;
        $this->defaultParameters = $defaultParameters;
    }

    /**
     * @TODO how caching should work with dynamic arguments
     * @param mixed $args
     * @return mixed|string
     * @throws InvalidArgumentException
     */
    public function render($args)
    {
        $data = $this->defaultParameters;

        if (!isset($args['name'])) {
            throw new InvalidArgumentException('Template name must be provided. e.g. template=header');
        }

        $templateName = $args['name'];
        unset($args['name']);

        $cacheTime = NULL;
        //we want to cache
        if (isset($args['cache'])) {
            $cacheTime = (int) $args['cache'];
            unset($args['cache']);
        }
        //no caching with additional arguments
        if ($cacheTime && !empty($args)) {
            $cacheTime = NULL;
        }

        if (isset($args['data'])) {
            $data = array_replace_recursive($data, $args['data']);
            unset($args['data']);
        }

        $data = array_replace_recursive($data, $args);

        $cacheKey = $templateName;
        if ($this->cache && $cacheTime && $this->cache->contains($cacheKey)) {
            return $this->cache->fetch($cacheKey);
        }

        $html = $this->mustacheEngine->render($templateName, $data);

        if ($this->cache && $cacheTime) {
            $this->cache->save($cacheKey, $html, $cacheTime);
        }

        return $html;
    }

}
