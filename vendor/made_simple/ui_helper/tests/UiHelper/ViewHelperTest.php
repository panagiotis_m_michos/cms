<?php

namespace UiHelper;
use Doctrine\Common\Cache\Cache;
use InvalidArgumentException;
use Mustache_Engine;
use PHPUnit_Framework_MockObject_MockBuilder;

class ViewHelperTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ViewHelper
     */
    protected $object;

    /**
     * @var Cache|PHPUnit_Framework_MockObject_MockBuilder
     */
    private $cacheMock;

    /**
     * @var Mustache_Engine|PHPUnit_Framework_MockObject_MockBuilder
     */
    private $mustacheEngineMock;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->cacheMock = $this->getMockBuilder('Doctrine\Common\Cache\Cache')
            ->disableOriginalConstructor()->getMock();
        $this->mustacheEngineMock = $this->getMockBuilder('Mustache_Engine')
            ->disableOriginalConstructor()->getMock();
        $this->object = new ViewHelper(
            $this->mustacheEngineMock,
            $this->cacheMock,
            ['phone' => 1]
        );
    }

    /**
     * @covers UiHelper\ViewHelper::render
     */
    public function testRender()
    {
        $this->mustacheEngineMock->expects($this->once())
            ->method('render')
            ->with(
                'test',
                [
                    'phone' => 1,
                    'option' => ['option1', 'option2'],
                ]
            )->will($this->returnValue('html'));

        //cache not called
        $this->cacheMock->expects($this->exactly(0))
            ->method('save');

        $this->object->render(
            [
                'name' => 'test',
                'option' => ['option1', 'option2'],
                'cache' => 2,
            ]
        );
    }

    /**
     * @covers UiHelper\ViewHelper::render
     */
    public function testRenderWithCache()
    {
        $templateName = 'test';

        $this->mustacheEngineMock->expects($this->once())
            ->method('render')
            ->with(
                $templateName,
                [
                    'phone' => 1,
                ]
            )->will($this->returnValue('html'));

        // save in cache
        $this->cacheMock->expects($this->once())
            ->method('save')
            ->with($templateName, 'html', 20);

        // get from cache
        $containsMock = $this->cacheMock->expects($this->exactly(2))
            ->method('contains')
            ->with($templateName);
        $containsMock->will($this->returnValue(FALSE));
        $this->cacheMock->expects($this->once())
            ->method('fetch')
            ->with($templateName);

        $this->object->render(
            array(
                'name' => $templateName,
                'cache' => 20
            )
        );
        $containsMock->will($this->returnValue(TRUE));
        $this->object->render(
            array(
                'name' => $templateName,
                'cache' => 20
            )
        );
    }

    /**
     * @covers UiHelper\ViewHelper::render
     * @expectedException InvalidArgumentException
     */
    public function testRenderNoTemplateName()
    {
        $this->object->render(
            [
                'option' => ['option1', 'option2'],
                'cache' => 2,
            ]
        );
    }
}
