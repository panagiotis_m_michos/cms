# Content Module

## What it is
This repository contains a module concerned with content management.

## Description
At the moment this repository contains only the variable loaders that can be composed together to load content variables from yaml files, parse them using our [placeholder parser](https://bitbucket.org/made_simple/utils/src/66d185ebc19503448db5a5c9df2a5df5a154e15b/src/Utils/Text/Parser/?at=master) and cache the result

### Variable Providers
Variable providers load, process and provide variables. They have to implement IVariableProvider. If possible they should be proxies to one another.

#### YamlVariableProvider
Loads data from a yaml file, depends only on yaml parser.

#### CachedVariableProvider
A proxy to any other Variable Provider. Caches the results in any doctrine cache provided in constructor.

#### MarkdownParsedVariableProvider
A proxy to any other Variable Provider. Parses any data identified by |markdown modifier in it's key as markdown. During parsing this modifier is removed, so title|markdown becomes just title.
  
#### TextParsed Variable Provider
A proxy to any other Variable Provider. Parses any placeholder used by our [placeholder parser](https://bitbucket.org/made_simple/utils/src/66d185ebc19503448db5a5c9df2a5df5a154e15b/src/Utils/Text/Parser/?at=master). No modifier needed. 
