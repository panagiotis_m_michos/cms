<?php

namespace spec\ContentModule\VariableProviders;

use ContentModule\VariableProviders\IVariableProvider;
use ContentModule\VariableProviders\TextParsedVariableProvider;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Utils\Text\Parser\Parser;

/**
 * @mixin TextParsedVariableProvider
 */
class TextParsedVariableProviderSpec extends ObjectBehavior
{
    function it_can_get_variables_by_key(IVariableProvider $variableProvider, Parser $parser)
    {
        $this->beConstructedWith($variableProvider, $parser);

        $variableProvider->getVariablesByKey('key')->willReturn(['1', '2']);
        $parser->parse('1')->willReturn('3');
        $parser->parse('2')->willReturn('4');

        $this->getVariablesByKey('key')->shouldReturn(['3', '4']);
    }
}
