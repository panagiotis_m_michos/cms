<?php

namespace spec\ContentModule\VariableProviders;

use ContentModule\Exceptions\UnresolvableReference;
use ContentModule\VariableProviders\IVariableProvider;
use ContentModule\VariableProviders\ReferenceResolvedVariableProvider;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin ReferenceResolvedVariableProvider
 */
class ReferenceResolvedVariableProviderSpec extends ObjectBehavior
{
    /**
     * @var IVariableProvider
     */
    private $variableProvider;

    function let(IVariableProvider $variableProvider)
    {
        $this->variableProvider = $variableProvider;
        $this->beConstructedWith($this->variableProvider);
    }

    function it_can_be_instantiated()
    {
        $this->beAnInstanceOf(IVariableProvider::class);
    }

    function it_can_resolve_references()
    {
        $this->variableProvider->getVariablesByKey('asd')->willReturn([
            'var' => 'qwe',
            'ref' => '%var%'
        ]);

        $this->getVariablesByKey('asd')->shouldReturn(['var' => 'qwe', 'ref' => 'qwe']);
    }

    function it_can_resolve_nested_references()
    {
        $this->variableProvider->getVariablesByKey('asd')->willReturn([
            'var' => ['asd' => 'qwe'],
            'ref' => '%var.asd%'
        ]);

        $this->getVariablesByKey('asd')->shouldReturn(['var' => ['asd' => 'qwe'], 'ref' => 'qwe']);
    }

    function it_will_throw_on_unresolvable_references()
    {
        $this->variableProvider->getVariablesByKey('asd')->willReturn([
            'var' => 'qwe',
            'ref' => '%varasd%'
        ]);

        $this->shouldThrow(UnresolvableReference::class)->duringGetVariablesByKey('asd');
    }
}
