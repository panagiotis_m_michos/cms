<?php

namespace spec\ContentModule\VariableProviders;

use ContentModule\VariableProviders\YamlVariableProvider;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\Yaml\Parser;

/**
 * @mixin YamlVariableProvider
 */
class YamlVariableProviderSpec extends ObjectBehavior
{
    function it_can_get_variables_by_key(Parser $parser)
    {
        $this->beConstructedWith(sys_get_temp_dir(), $parser);

        $filepath = sys_get_temp_dir() . '/yamlvarprovidertest.yml';
        file_put_contents($filepath, '123');

        $parser->parse('123')->willReturn([1, 2, 3]);

        $this->getVariablesByKey('yamlvarprovidertest')->shouldReturn([1, 2, 3]);

        unlink($filepath);
    }
}
