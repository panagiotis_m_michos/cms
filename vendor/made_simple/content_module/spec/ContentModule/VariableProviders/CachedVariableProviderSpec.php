<?php

namespace spec\ContentModule\VariableProviders;

use ContentModule\VariableProviders\CachedVariableProvider;
use ContentModule\VariableProviders\IVariableProvider;
use Doctrine\Common\Cache\Cache;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin CachedVariableProvider
 */
class CachedVariableProviderSpec extends ObjectBehavior
{
    /**
     * @var IVariableProvider
     */
    private $variableProvider;

    /**
     * @var Cache
     */
    private $cache;

    function let(IVariableProvider $variableProvider, Cache $cache)
    {
        $this->variableProvider = $variableProvider;
        $this->cache = $cache;
        $this->beConstructedWith($this->variableProvider, $this->cache);
    }

    function it_can_get_variables_by_key()
    {
        $key = 'key';
        $cacheKey = "variables-$key";
        $value = 'value';

        $this->cache->contains($cacheKey)->willReturn(FALSE);
        $this->variableProvider->getVariablesByKey($key)->willReturn($value);
        $this->cache->save($cacheKey, $value)->shouldBeCalled();

        $this->getVariablesByKey($key)->shouldReturn($value);
    }

    function it_can_get_chached_variables()
    {
        $key = 'key';
        $cacheKey = "variables-$key";
        $value = 'value';

        $this->cache->contains($cacheKey)->willReturn(TRUE);
        $this->cache->fetch($cacheKey)->willReturn($value);

        $this->getVariablesByKey($key)->shouldReturn($value);
    }
}
