<?php

namespace spec\ContentModule\VariableProviders;

use ContentModule\VariableProviders\IVariableProvider;
use ContentModule\VariableProviders\MarkdownParsedVariableProvider;
use League\CommonMark\CommonMarkConverter;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin MarkdownParsedVariableProvider
 */
class MarkdownParsedVariableProviderSpec extends ObjectBehavior
{
    function it_can_get_variables_by_key(IVariableProvider $variableProvider, CommonMarkConverter $converter)
    {
        $this->beConstructedWith($variableProvider, $converter);
        $variableProvider->getVariablesByKey('key')->willReturn(['a' => 1, 'b|markdown' => 'mdstring']);
        $converter->convertToHtml('mdstring')->willReturn('converted');

        $this->getVariablesByKey('key')->shouldReturn(['a' => 1, 'b' => 'converted']);
    }
}
