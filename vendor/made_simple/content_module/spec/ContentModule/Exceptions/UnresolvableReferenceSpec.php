<?php

namespace spec\ContentModule\Exceptions;

use Exception;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class UnresolvableReferenceSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Exception::class);
    }
}
