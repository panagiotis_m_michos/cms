<?php

namespace ContentModule\VariableProviders;

use ContentModule\Exceptions\UnresolvableReference;

class ReferenceResolvedVariableProvider implements IVariableProvider
{
    /**
     * @var IVariableProvider
     */
    private $variableProvider;

    /**
     * ReferenceResolvedVariableProvider constructor.
     * @param IVariableProvider $variableProvider
     */
    public function __construct(IVariableProvider $variableProvider)
    {
        $this->variableProvider = $variableProvider;
    }

    /**
     * @param string $key
     * @return array
     */
    public function getVariablesByKey($key)
    {
        return $this->resolveReferences($this->variableProvider->getVariablesByKey($key));
    }

    /**
     * @param array $data
     * @return array
     */
    private function resolveReferences(array $data)
    {
        $maxDepth = 30;
        $depth = 0;

        do {
            $depth++;
            $referencesFound = FALSE;

            array_walk_recursive(
                $data,
                function (&$value) use (&$referencesFound, $data) {
                    if (preg_match('/^%(.*)%$/', $value, $matches) === 1) {
                        $value = $this->extractVariable($data, $matches[1]);
                    }
                }
            );
        } while ($referencesFound && $depth < $maxDepth);

        return $data;
    }

    /**
     * @param mixed $data
     * @param mixed $keyPath
     * @return mixed
     * @throws UnresolvableReference
     */
    private function extractVariable($data, $keyPath)
    {
        $keyPathParts = explode('.', $keyPath);
        $currentKey = array_shift($keyPathParts);

        if (!isset($data[$currentKey])) {
            throw new UnresolvableReference;
        }

        $currentData = $data[$currentKey];

        if (empty($keyPathParts)) {
            return $currentData;
        } else {
            return $this->extractVariable($currentData, implode('.', $keyPathParts));
        }
    }
}
