<?php

namespace ContentModule\VariableProviders;

use Utils\Text\Parser\Parser;

class TextParsedVariableProvider implements IVariableProvider
{
    /**
     * @var IVariableProvider
     */
    private $variableLoader;

    /**
     * @var Parser
     */
    private $parser;

    /**
     * @param IVariableProvider $variableLoader
     * @param Parser $parser
     */
    public function __construct(IVariableProvider $variableLoader, Parser $parser)
    {
        $this->variableLoader = $variableLoader;
        $this->parser = $parser;
    }

    /**
     * @param string $key
     * @return array
     */
    public function getVariablesByKey($key)
    {
        $variables = $this->variableLoader->getVariablesByKey($key);

        array_walk_recursive(
            $variables,
            function (&$item) {
                if (is_string($item)) {
                    $item = $this->parser->parse($item);
                }
            }
        );

        return $variables;
    }
}
