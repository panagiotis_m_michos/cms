<?php

namespace ContentModule\VariableProviders;

use DOMDocument;
use DOMXPath;
use League\CommonMark\CommonMarkConverter;

class MarkdownParsedVariableProvider implements IVariableProvider
{
    /**
     * @var IVariableProvider
     */
    private $variableLoader;

    /**
     * @var CommonMarkConverter
     */
    private $converter;

    /**
     * @param IVariableProvider $variableLoader
     * @param CommonMarkConverter $converter
     */
    public function __construct(IVariableProvider $variableLoader, CommonMarkConverter $converter)
    {
        $this->variableLoader = $variableLoader;
        $this->converter = $converter;
    }

    /**
     * @param string $key
     * @return array
     */
    public function getVariablesByKey($key)
    {
        $variables = $this->variableLoader->getVariablesByKey($key);
        return $this->convert($variables);
    }

    /**
     * @param array $inputs
     * @return array
     */
    public function convert(array $inputs)
    {
        $converted = [];
        
        foreach ($inputs as $key => $input) {
            if (is_array($input)) {
                $input = $this->convert($input);
            } else {
                if (strstr($key, '|markdown') !== FALSE) {
                    $key = str_replace('|markdown', '', $key);
                    $input = $this->converter->convertToHtml($input);

                    if (strstr($key, '|allow_paragraph') === FALSE) {
                        $domDocument = new DOMDocument;
                        $domDocument->loadHTML("<body>$input</body>");
                        $xPath = new DOMXPath($domDocument);

                        if ($xPath->query('//body/p')->length === 1) {
                            $input = preg_replace('#<p>(.*)</p>#s', '\1', $input);
                        }
                    } else {
                        $key = str_replace('|allow_paragraph', '', $key);
                    }
                }
            }

            $converted[$key] = $input;
        }

        return $converted;
    }
}
