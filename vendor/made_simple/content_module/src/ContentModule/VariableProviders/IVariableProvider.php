<?php
namespace ContentModule\VariableProviders;

interface IVariableProvider
{
    /**
     * @param string $key
     * @return array
     */
    public function getVariablesByKey($key);
}
