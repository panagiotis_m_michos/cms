<?php

namespace ContentModule\VariableProviders;

use Symfony\Component\Yaml\Parser;

class YamlVariableProvider implements IVariableProvider
{
    /**
     * @var string
     */
    private $rootPath;

    /**
     * @var Parser
     */
    private $parser;

    /**
     * @param string $rootPath
     * @param Parser $parser
     */
    public function __construct($rootPath, Parser $parser)
    {
        $this->rootPath = $rootPath;
        $this->parser = $parser;
    }

    /**
     * @param string $key
     * @return array
     */
    public function getVariablesByKey($key)
    {
        $key = str_replace(['\\', '.'], '/', $key);
        $yamlPath = "$this->rootPath/$key.yml";

        return is_file($yamlPath) ? $this->loadYaml($yamlPath) : [];
    }

    /**
     * @param string $path
     * @return array
     */
    private function loadYaml($path)
    {
        $data = $this->parser->parse(file_get_contents($path));

        if (isset($data['@imports'])) {
            $currentDir = dirname($path);
            $imports = $data['@imports'];
            unset($data['@imports']);

            $importedData = [];
            foreach ($imports as $importAs => $importPath) {
                $realImportPath = strpos($importPath, './') === 0
                    ? $currentDir . DIRECTORY_SEPARATOR . $importPath
                    : $this->rootPath . DIRECTORY_SEPARATOR . $importPath;

                $currentImportedData = is_numeric($importAs)
                    ? $this->loadYaml($realImportPath)
                    : [$importAs => $this->loadYaml($realImportPath)];

                $importedData = array_replace_recursive(
                    $importedData,
                    $currentImportedData
                );
            }

            $data = array_replace_recursive($importedData, $data);
        }

        return $data;
    }
}
