<?php

namespace ContentModule\VariableProviders;

use Doctrine\Common\Cache\Cache;

class CachedVariableProvider implements IVariableProvider
{
    /**
     * @var IVariableProvider
     */
    private $variableLoader;

    /**
     * @var Cache
     */
    private $cache;

    /**
     * @var int
     */
    private $cacheLifetime;


    /**
     * @param IVariableProvider $variableLoader
     * @param Cache $cache
     * @param int $cacheLifetime
     */
    public function __construct(IVariableProvider $variableLoader, Cache $cache, $cacheLifetime = 3600)
    {
        $this->variableLoader = $variableLoader;
        $this->cache = $cache;
        $this->cacheLifetime = $cacheLifetime;
    }

    /**
     * @param string $key
     * @return array
     */
    public function getVariablesByKey($key)
    {
        $cacheKey = "variables-$key";

        if ($this->cache->contains($cacheKey)) {
            return $this->cache->fetch($cacheKey);
        }

        $variables = $this->variableLoader->getVariablesByKey($key);
        $this->cache->save($cacheKey, $variables);

        return $variables;
    }
}
