<?php

namespace ContentModule\Exceptions;

use Exception;

class UnresolvableReference extends Exception
{
}
