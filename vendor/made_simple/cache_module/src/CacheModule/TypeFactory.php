<?php

namespace CacheModule;

use Context;
use Doctrine\Common\Cache\ApcCache;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\Common\Cache\FilesystemCache;
use Doctrine\Common\Cache\RedisCache;
use FileJournal;
use FileStorage;
use InvalidArgumentException;
use Redis;
use RedisException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use CacheModule\DummyCache\FileStorage as FileStorageDummy;
use Utils\Directory;

class TypeFactory
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $type
     * @param mixed $context
     * @param string $key (service id)
     * @return FileStorageDummy|ApcCache|ArrayCache|FilesystemCache|FileStorage
     */
    public function create($type, $context, $key = NULL)
    {
        if (!$this->container->getParameter('cache.enabled')) {
            return $this->createDummy($type);
        }
        switch ($type) {
            case 'nette.filesystem_dummy':
                return new FileStorageDummy();
            case 'array':
                return new ArrayCache();
            case 'filesystem':
                return new FilesystemCache($context);
            case 'apc':
                $apc = new ApcCache();
                $apc->setNamespace($context);
                return $apc;
            case 'redis':
                return $this->createRedis($this->container->get($key), $context);
            case 'nette.filesystem':
                $directory = Directory::createWritable($context);
                return new FileStorage($directory->getPath());
            case 'nette.filesystem_context':
                $directory = Directory::createWritable($context);
                $cacheContext = new Context();
                $cacheContext->addService('Nette\Caching\ICacheJournal', new FileJournal($directory->getPath()));
                return new FileStorage($directory->getPath(), $cacheContext);
            default:
                throw new InvalidArgumentException(sprintf('Type %s does not exist', $type));
        }

    }

    /**
     * @param string $type
     * @return FileStorageDummy|ArrayCache
     * @throws InvalidArgumentException
     */
    public function createDummy($type)
    {
        switch ($type) {
            case 'nette.filesystem':
            case 'nette.filesystem_dummy':
            case 'nette.filesystem_context':
                return new FileStorageDummy();
            case 'apc':
            case 'filesystem':
            case 'redis':
            case 'array':
                return new ArrayCache();
            default:
                throw new InvalidArgumentException(sprintf('Type %s does not exist', $type));
        }
    }

    /**
     * @param Redis $redis
     * @param string $namespace
     * @return ArrayCache|RedisCache
     */
    public function createRedis(Redis $redis, $namespace)
    {
        try {
            $redis->ping();
            $redisCache = new RedisCache();
            $redisCache->setRedis($redis);
            $redisCache->setNamespace($namespace);
            return $redisCache;
        } catch (RedisException $e) {
            return new ArrayCache();
        }
    }
}