<?php

namespace CacheModule\Commands;

use CacheModule\BootstrapExtension\CacheExt;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ClearAllCaches extends Command
{
    /**
     * @var string
     */
    private $host;

    /**
     * @param string $host
     */
    public function __construct($host)
    {
        parent::__construct(NULL);
        $this->host = $host;
    }

    protected function configure()
    {
        $this->setName('cache:clearAll')
            ->setDescription('Clear caches');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $url = $this->host . CacheExt::CLEARING_URL;
        $response = $this->getResponse($url);

        if ($response == CacheExt::RESPONSE_OK) {
            $output->writeln('All caches cleared');
        } else {
            $output->writeln('Can\'t clear caches. Response: ' . $response);
        }
    }

    /**
     * @param string $url
     * @return string
     */
    private function getResponse($url)
    {
        $ch = curl_init();

        $optArray = [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => TRUE
        ];

        curl_setopt_array($ch, $optArray);

        return curl_exec($ch);
    }
}
