<?php

namespace CacheModule\DummyCache;

use Doctrine\Common\Cache\ArrayCache;
use ICacheStorage;

class FileStorage extends ArrayCache implements ICacheStorage
{
    /**
     * @inheritdoc
     */
    function read($key)
    {
        $data = $this->fetch($key);
        return $data === FALSE ? NULL : $data;
    }

    /**
     * @inheritdoc
     */
    function write($key, $data, array $dependencies)
    {
        return $this->save($key, $data);
    }

    /**
     * @inheritdoc
     */
    function remove($key)
    {
        return $this->delete($key);
    }

    /**
     * @inheritdoc
     */
    function clean(array $conds)
    {
        return $this->deleteAll();
    }

}