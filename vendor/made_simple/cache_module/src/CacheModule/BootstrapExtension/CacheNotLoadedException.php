<?php

namespace CacheModule\BootstrapExtension;

use Exception;

class CacheNotLoadedException extends Exception
{
    public static function clearingNonLoadedCache($cacheName)
    {
        return new self("Trying to clear non-loaded cache: {$cacheName}");
    }
}
