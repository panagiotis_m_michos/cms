<?php

namespace CacheModule\BootstrapExtension;

use BootstrapModule\Ext\IExtension;
use Exception;
use Symfony\Component\DependencyInjection\Container;

class CacheExt implements IExtension
{
    const CLEARING_URL = '/cache.php?cache=clearAll';
    const RESPONSE_OK = "OK";

    const TYPE_FACTORY = 'cache_module.type_factory';

    /**
     * @param Container $container
     */
    public function load(Container $container)
    {
        $currentUrl = array_key_exists('REQUEST_URI', $_SERVER) ? $_SERVER['REQUEST_URI'] : NULL;

        if ($currentUrl == self::CLEARING_URL) {
            $this->clearCaches($container);
            exit;
        }
    }

    /**
     * @param Container $container
     */
    private function clearCaches(Container $container)
    {
        try {
            $this->clearLoadedCaches($container);
            $this->notifySuccess();
        } catch (Exception $e) {
            $this->notifyFailure($e);
        }

    }

    /**
     * @param Container $container
     */
    private function clearLoadedCaches(Container $container)
    {
        $caches = $this->getLoadedCaches($container);

        foreach ($caches as $cache) {
            if ($cache->isLoaded()) {
                $cache->clear();
            }
        }
    }

    /**
     * @param Container $container
     * @return ICache[]
     */
    private function getLoadedCaches(Container $container)
    {
        $loader = new CacheLoader($container);

        return $loader->getLoadedCaches();
    }

    private function notifySuccess()
    {
        echo self::RESPONSE_OK;
    }

    /**
     * @param Exception $e
     */
    private function notifyFailure(Exception $e)
    {
        echo "Cache clearing failed: {$e->getMessage()}";
    }
}
