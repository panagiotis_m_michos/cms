<?php

namespace CacheModule\BootstrapExtension;

use BootstrapModule\Ext\DoctrineExt;
use Doctrine\Common\Cache\CacheProvider;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;

class DoctrineCache implements ICache
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @throws CacheNotLoadedException
     */
    public function clear()
    {
        if ($this->isLoaded()) {
            /** @var EntityManager $em */
            $em = $this->container->get(DoctrineExt::ENTITY_MANAGER);
            $configuration = $em->getConfiguration();
            /** @var CacheProvider $cache */
            $cache = $configuration->getMetadataCacheImpl();
            $cache->deleteAll();
        } else {
            throw CacheNotLoadedException::clearingNonLoadedCache(__CLASS__);
        }
    }

    /**
     * @return bool
     */
    public function isLoaded()
    {
        return $this->container->has(DoctrineExt::ENTITY_MANAGER);
    }
}
