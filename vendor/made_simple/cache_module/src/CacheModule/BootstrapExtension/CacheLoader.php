<?php

namespace CacheModule\BootstrapExtension;

use Symfony\Component\DependencyInjection\Container;

class CacheLoader
{
    /**
     * @var ICache[]
     */
    private $caches = [];

    /**
     * @param Container $containerBuilder
     */
    public function __construct(Container $containerBuilder)
    {
        $this->caches = [new DoctrineCache($containerBuilder),];
    }

    /**
     * @return ICache[]
     */
    public function getCaches()
    {
        return $this->caches;
    }

    /**
     * @return ICache[]
     */
    public function getLoadedCaches()
    {
        $caches = [];

        foreach ($this->caches as $cache) {
            if ($cache->isLoaded()) {
                $caches[] = $cache;
            }
        }

        return $caches;
    }
}
