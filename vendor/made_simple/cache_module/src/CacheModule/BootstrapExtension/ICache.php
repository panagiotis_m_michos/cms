<?php
namespace CacheModule\BootstrapExtension;

interface ICache
{
    public function clear();

    /**
     * @return bool
     */
    public function isLoaded();
}
