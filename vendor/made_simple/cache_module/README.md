#CacheModule#
CacheModule is in charge of clearing caches.
 
## CacheExt ##
It works with https://bitbucket.org/made_simple/git-hooks to clear the caches on each deploy (using post-merge git hook).

### Usage ###
Upon visiting `/cache.php?cache=clearAll` all caches registered to CacheExt's CacheLoader will be cleared.
This is the only use case for CacheExt, separate cache clearing for each cache may be implemented in future.

### Adding a cache ###
If you want to add cache to CacheExt so it will be cleared, you follow these 2 steps:

* create a class representing your cache that implements `BootstrapModule\Ext\Cache\ICache`

* register this class to CacheExt in `BootstrapModule\Ext\Cache\CacheLoader`'s constructor

## Command ##
You can register the `CacheModule\Commands\ClearAllCaches` Symfony command and use it in your application to clear caches. It calls CacheExt internally.

### Usage ###
Command will be registered as `cache:clearAll`
