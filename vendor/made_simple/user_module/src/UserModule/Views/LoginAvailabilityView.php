<?php

namespace UserModule\Views;

use FormModule\Helpers\ErrorHelper;
use JsonSerializable;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use UserModule\Contracts\ICustomer;
use UserModule\Contracts\ILoginAvailability;

class LoginAvailabilityView implements ILoginAvailability, JsonSerializable
{
    /**
     * @var FormInterface
     */
    private $form;

    /**
     * @var NULL|ICustomer
     */
    private $customer;

    /**
     * @var bool|null
     */
    private $emailIsTaken;

    /**
     * @param FormInterface $form
     * @param ICustomer|NULL $customer
     * @param bool|null $emailIsTaken
     * @return LoginAvailabilityView
     */
    public static function withDetails(FormInterface $form, ICustomer $customer = NULL, $emailIsTaken = NULL)
    {
        $self = new self();
        $self->form = $form;
        $self->customer = $customer;
        $self->emailIsTaken = $emailIsTaken;
        return $self;
    }

    /**
     * @return bool
     */
    public function isEmailConfirmed()
    {
        return $this->customer && $this->customer->getEmail();
    }

    /**
     * @return bool
     */
    public function isRequiredToLogIn()
    {
        return $this->emailIsTaken === TRUE;
    }

    /**
     * @return FormView
     */
    public function getFormView()
    {
        return $this->form->createView();
    }

    /**
     * @return bool
     */
    public function isLoggedIn()
    {
        return $this->customer && $this->customer->getId() > 0;
    }

    /**
     * @return ICustomer|null
     */
    public function getLoggedInCustomer()
    {
        return $this->isLoggedIn() ? $this->customer : NULL;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'emailConfirmed' => $this->isEmailConfirmed(),
            'requiresToLogin' => $this->isRequiredToLogIn(),
            'isLoggedIn' => $this->isLoggedIn(),
            'errors' => ErrorHelper::toArray($this->form)
        ];
    }
}