<?php

namespace UserModule\Domain;

class Credentials
{
    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @param string $username
     * @param string $password
     * @return Credentials
     */
    public static function withLogin($username, $password)
    {
        $self = new self();
        $self->username = $username;
        $self->password = $password;
        return $self;
    }

    /**
     * @param string $username
     * @return Credentials
     */
    public static function withUsername($username)
    {
        $self = new self();
        $self->username = $username;
        return $self;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }


}