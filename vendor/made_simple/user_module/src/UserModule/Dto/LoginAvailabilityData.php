<?php

namespace UserModule\Dto;

use UserModule\Domain\Credentials;

class LoginAvailabilityData
{
    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $password;

    /**
     * @param string $email
     * @return LoginAvailabilityData
     */
    public static function withEmail($email)
    {
        $self = new self();
        $self->email = $email;
        return $self;
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
        if (!$this->password) {
            return Credentials::withUsername($this->email);
        }
        return Credentials::withLogin($this->email, $this->password);
    }

    /**
     * @return bool
     */
    public function isLogginAttempt()
    {
        return !empty($this->password);
    }
}