<?php

namespace UserModule\Listeners;

use UserModule\Services\CustomerDetailsService;
use PaymentModule\Config\EventLocator;
use PaymentModule\Events\PaymentEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CustomerDetailsListener implements EventSubscriberInterface
{
    /**
     * @var CustomerDetailsService
     */
    private $customerDetailsService;

    /**
     * @param CustomerDetailsService $customerDetailsService
     */
    public function __construct(CustomerDetailsService $customerDetailsService)
    {
        $this->customerDetailsService = $customerDetailsService;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            EventLocator::PAYMENT_SUCCEEDED => 'onPaymentSuccess'
        ];
    }

    /**
     * @param PaymentEvent $event
     */
    public function onPaymentSuccess(PaymentEvent $event)
    {
        $this->customerDetailsService->processPaymentDetails($event->getCustomer(), $event->getPaymentResponse());
    }

}