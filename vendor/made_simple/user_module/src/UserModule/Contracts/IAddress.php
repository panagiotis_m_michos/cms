<?php

namespace UserModule\Contracts;

interface IAddress
{
    /**
     * @param string $address1
     */
    public function setAddress1($address1);

    /**
     * @param string $address2
     */
    public function setAddress2($address2);

    /**
     * @param string $city
     */
    public function setCity($city);

    /**
     * @param string $postCode
     */
    public function setPostCode($postCode);

    /**
     * @param string $country
     */
    public function setCountryIso($country);


    /**
     * @return string
     */
    public function getAddress1();

    /**
     * @return string
     */
    public function getAddress2();

    /**
     * @return string
     */
    public function getCity();

    /**
     * @@return string
     */
    public function getPostCode();

    /**
     * @return string
     */
    public function getCountryIso();
    
}
