<?php

namespace UserModule\Contracts;

interface ICustomer
{
    /**
     * @return bool
     */
    public function hasCompletedRegistration();

    /**
     * @param string $title
     */
    public function setTitleId($title);

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName);

    /**
     * @param string $lastName
     */
    public function setLastName($lastName);

    /**
     * @return IAddress
     */
    public function getPrimaryAddress();

    /**
     * @return string
     */
    public function getFirstName();

    /**
     * @return string
     */
    public function getLastName();

    /**
     * @return string
     */
    public function getFullName();

    /**
     * @return string
     */
    public function getEmail();

    /**
     * @param float $credit
     * @return bool
     */
    public function hasEnoughCredit($credit);

    /**
     * @param float $credit
     */
    public function subtractCredit($credit);

    /**
     * @return bool
     */
    public function hasCredit();

    /**
     * @return float
     */
    public function getCredit();

    /**
     * @return int
     */
    public function getId();

    /**
     * @param string $password
     */
    public function setTemporaryPassword($password);

    /**
     * @return string
     */
    public function getTemporaryPassword();
}
