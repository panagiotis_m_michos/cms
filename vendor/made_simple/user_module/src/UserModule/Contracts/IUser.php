<?php

namespace UserModule\Contracts;

interface IUser
{
    /**
     * @return int
     */
    public function getId();
}