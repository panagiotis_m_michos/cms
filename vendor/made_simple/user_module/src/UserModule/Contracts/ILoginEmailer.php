<?php

namespace UserModule\Contracts;

interface ILoginEmailer
{
    /**
     * @param ICustomer $customer
     * @param string $returnUrl
     * @return bool
     */
    public function sendForgottenPasswordEmail(ICustomer $customer, $returnUrl);
}