<?php

namespace UserModule\Contracts;

use Symfony\Component\Form\FormView;

interface ILoginAvailability
{
    /**
     * @return bool
     */
    public function isEmailConfirmed();

    /**
     * @return bool
     */
    public function isRequiredToLogIn();

    /**
     * @return FormView
     */
    public function getFormView();

    /**
     * @return bool
     */
    public function isLoggedIn();

    /**
     * @return ICustomer|null
     */
    public function getLoggedInCustomer();
}