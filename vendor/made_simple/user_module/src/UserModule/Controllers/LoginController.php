<?php

namespace UserModule\Controllers;

use RouterModule\Helpers\ControllerHelper;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use UserModule\Contracts\ILoginAvailability;
use UserModule\Dto\LoginAvailabilityData;
use UserModule\Exceptions\CustomerAvailabilityException;
use UserModule\Exceptions\LoginException;
use UserModule\Forms\LoginAvailabilityForm;
use UserModule\Services\IAuthenticator;
use UserModule\Services\ICustomerAvailability;
use UserModule\Views\LoginAvailabilityView;

class LoginController
{
    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    /**
     * @var IAuthenticator
     */
    private $authenticator;

    /**
     * @var ICustomerAvailability
     */
    private $customerAvailability;

    /**
     * @param ControllerHelper $controllerHelper
     * @param IAuthenticator $authenticator
     * @param ICustomerAvailability $customerAvailability
     */
    public function __construct(ControllerHelper $controllerHelper, IAuthenticator $authenticator, ICustomerAvailability $customerAvailability)
    {
        $this->controllerHelper = $controllerHelper;
        $this->authenticator = $authenticator;
        $this->customerAvailability = $customerAvailability;
    }

    /**
     * @return ILoginAvailability
     */
    public function handleLoginAttempt()
    {
        $emailTaken = $loginError = NULL;
        $customer = $this->customerAvailability->optionalTemporaryCustomer();
        $loginAvailabilityData = $customer ? LoginAvailabilityData::withEmail($customer->getEmail()) : NULL;
        $form = $this->controllerHelper->buildForm(new LoginAvailabilityForm(), $loginAvailabilityData);
        if ($form->isValid()) {
            $data = $form->getData();
            try {
                $customer = $this->authenticator->useCredentials($data->getCredentials());
            } catch (CustomerAvailabilityException $e) {
                return LoginAvailabilityView::withDetails($form, NULL, TRUE);
            } catch (LoginException $e) {
                $form->get('password')->addError(new FormError($e->getMessage()));
                return LoginAvailabilityView::withDetails($form, NULL, TRUE);
            }
        }
        return LoginAvailabilityView::withDetails($form, $customer, $emailTaken);
    }

    /**
     * @return JsonResponse
     */
    public function loginAvailable()
    {
        $loginAvailability = $this->handleLoginAttempt();
        return JsonResponse::create(['loginDetails' => $loginAvailability]);
    }
}