<?php

namespace UserModule\Controllers;

use Exceptions\Technical\EmailException;
use FormModule\Helpers\ErrorHelper;
use LoginControler;
use RouterModule\Generators\IUrlGenerator;
use RouterModule\Helpers\ControllerHelper;
use Symfony\Component\HttpFoundation\JsonResponse;
use TranslationModule\Config\MsgId;
use UserModule\Contracts\ILoginEmailer;
use UserModule\Exceptions\CustomerAvailabilityException;
use UserModule\Forms\ForgottenPasswordForm;
use UserModule\Services\ILoginService;

class ForgottenLoginController
{

    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    /**
     * @var ILoginService
     */
    private $loginService;

    /**
     * @var ILoginEmailer
     */
    private $loginEmailer;

    /**
     * @param ILoginService $loginService
     * @param ILoginEmailer $loginEmailer
     * @param ControllerHelper $controllerHelper
     */
    public function __construct(ILoginService $loginService, ILoginEmailer $loginEmailer, ControllerHelper $controllerHelper)
    {
        $this->controllerHelper = $controllerHelper;
        $this->loginService = $loginService;
        $this->loginEmailer = $loginEmailer;
    }

    /**
     * @param string|null $resourceType
     * @param string|null $returnType
     * @return JsonResponse
     */
    public function forgottenPassword($resourceType = NULL, $returnType = NULL)
    {
        try {
            $form = $this->controllerHelper->buildForm(new ForgottenPasswordForm($resourceType), NULL, ['allow_extra_fields' => TRUE]);
            if ($form->isValid()) {
                $email = $form->getData()->email;
                $returnUrl = $this->getUrlByReturnType($returnType);
                $customer = $this->loginService->resetCustomerPassword($email);
                $this->loginEmailer->sendForgottenPasswordEmail($customer, $returnUrl);
                return JsonResponse::create(['success' => TRUE]);
            }
            return JsonResponse::create(['error' => ErrorHelper::getFirstError($form) ? : s_(MsgId::GENERIC_RESPONSE_ERROR), 'full' => ErrorHelper::toArray($form)], JsonResponse::HTTP_SERVICE_UNAVAILABLE);
        } catch (CustomerAvailabilityException $e) {
            return JsonResponse::create(['error' => $e->getMessage()], JsonResponse::HTTP_SERVICE_UNAVAILABLE);
        } catch (EmailException $e) {
            return JsonResponse::create(['error' => s_(MsgId::GENERIC_RESPONSE_ERROR)], JsonResponse::HTTP_SERVICE_UNAVAILABLE);
        }
    }

    /**
     * @param string $returnType
     * @return string
     */
    private function getUrlByReturnType($returnType)
    {
        $returnUrl = $this->controllerHelper->getLink(LoginControler::LOGIN_PAGE, [], IUrlGenerator::SECURE);
        switch ($returnType) {
            case 'payment':
                $returnUrl = $this->controllerHelper->getUrl('payment', [], IUrlGenerator::SECURE);
        }
        return $returnUrl;
    }
}