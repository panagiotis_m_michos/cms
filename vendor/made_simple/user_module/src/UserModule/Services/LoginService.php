<?php

namespace UserModule\Services;

use FTools;
use OrmModule\Contracts\IRepository;
use UserModule\Contracts\ICustomer;
use UserModule\Exceptions\CustomerAvailabilityException;

class LoginService implements ILoginService
{
    /**
     * @var IRepository
     */
    private $customerRepository;

    /**
     * @param IRepository $customerRepository
     */
    public function __construct(IRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param string $email
     * @return ICustomer
     * @throws CustomerAvailabilityException
     */
    public function resetCustomerPassword($email)
    {
        /** @var ICustomer $customer */
        $customer = $this->customerRepository->findOneBy(['email' => $email]);
        if (!$customer) {
            throw CustomerAvailabilityException::emailNotFound($email);
        }
        $password = FTools::generPwd(6);
        $customer->setTemporaryPassword($password);
        return $this->customerRepository->saveEntity($customer);
    }
}