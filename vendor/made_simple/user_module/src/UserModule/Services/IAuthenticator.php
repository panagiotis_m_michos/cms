<?php

namespace UserModule\Services;

use UserModule\Contracts\ICustomer;
use UserModule\Domain\Credentials;

interface IAuthenticator
{
    const TEMPORARY_CUSTOMER_KEY = 'payment.temporaryCustomer';
    const CUSTOMER_KEY = 'customer.customerId';
    
    /**
     * login customer or use email to save temporary customer
     * 
     * @param Credentials $credentials
     * @return ICustomer
     */
    public function useCredentials(Credentials $credentials);
}