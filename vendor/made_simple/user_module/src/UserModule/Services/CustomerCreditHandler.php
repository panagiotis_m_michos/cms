<?php

namespace UserModule\Services;

use OrmModule\Contracts\IRepository;
use UserModule\Contracts\ICustomer;
use UserModule\Exceptions\CreditException;

class CustomerCreditHandler implements ICreditProvider
{
    /**
     * @var IRepository
     */
    private $customerRepository;

    /**
     * @param IRepository $customerRepository
     */
    public function __construct(IRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param ICustomer $customer
     * @param float $credit
     * @return ICustomer
     * @throws CreditException
     */
    public function subtractCredit(ICustomer $customer, $credit)
    {
        if (!$customer->hasEnoughCredit($credit)) {
            throw CreditException::notEnough($customer->getCredit(), $credit);
        }
        $customer->subtractCredit($credit);
        return $this->customerRepository->saveEntity($customer);
    }
}