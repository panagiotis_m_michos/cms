<?php

namespace UserModule\Services;

use Entities\Customer;

interface ILoginService
{
    /**
     * @param string $email
     * @return Customer
     */
    public function resetCustomerPassword($email);
}