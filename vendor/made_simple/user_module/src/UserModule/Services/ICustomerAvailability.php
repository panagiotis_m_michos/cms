<?php

namespace UserModule\Services;

use UserModule\Contracts\ICustomer;

interface ICustomerAvailability
{
    /**
     * returns logged in or temporary customer
     * @return ICustomer
     */
    public function requireCustomer();
    
    /**
     * @return ICustomer
     */
    public function requireTemporaryCustomer();
    
    /**
     * @return ICustomer
     */
    public function optionalTemporaryCustomer();

    /**
     * @return ICustomer
     */
    public function requireLoggedInCustomer();

    /**
     * @return ICustomer|NULL
     */
    public function optionalLoggedInCustomer();
}