<?php

namespace UserModule\Services;

use OrmModule\Contracts\IRepository;
use PaymentModule\Contracts\IPaymentResponse;
use PayPalNVP\Response\GetExpressCheckoutDetailsResponse;
use Repositories\CustomerRepository;
use SagePay\Token\Request\Details;
use UserModule\Contracts\ICustomer;

class CustomerDetailsService
{
    /**
     * @var CustomerRepository
     */
    private $customerRepository;

    /**
     * @param IRepository $customerRepository
     */
    public function __construct(IRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param ICustomer $customer
     * @param IPaymentResponse $paymentResponse
     * @return bool|ICustomer
     */
    public function processPaymentDetails(ICustomer $customer, IPaymentResponse $paymentResponse)
    {
        if ($customer->hasCompletedRegistration()) {
            return FALSE;
        }
        if ($paymentResponse->isSagePay() && $paymentResponse->getSagePaymentInfo()) {
            return $this->saveCustomerDetailsFromSagePay($customer, $paymentResponse->getSagePaymentInfo());
        } elseif ($paymentResponse->isPaypal() && $paymentResponse->getPaypalPaymentInfo()) {
            return $this->saveCustomerDetailsFromPaypal($customer, $paymentResponse->getPaypalPaymentInfo());
        }
        return FALSE;
    }

    /**
     * @param ICustomer $customer
     * @param Details $details
     * @return ICustomer
     */
    public function saveCustomerDetailsFromSagePay(ICustomer $customer, Details $details)
    {
        $customer->setFirstName($details->getBillingFirstnames());
        $customer->setLastName($details->getBillingSurname());
        $address = $customer->getPrimaryAddress();
        $address->setAddress1($details->getBillingAddress1());
        if ($details->getBillingAddress2()) {
            $address->setAddress2($details->getBillingAddress2());
        }
        $address->setCity($details->getBillingCity());
        $address->setPostCode($details->getBillingPostCode());
        $address->setCountryIso($details->getBillingCountry());
        return $this->customerRepository->saveEntity($customer);
    }

    /**
     * @param ICustomer $customer
     * @param GetExpressCheckoutDetailsResponse $details
     * @return ICustomer
     */
    public function saveCustomerDetailsFromPaypal(ICustomer $customer, GetExpressCheckoutDetailsResponse $details)
    {
        $customerInfo = $details->getPayerName();
        if ($customerInfo) {
            $customer->setTitleId($customerInfo->getSuffix());
            $customer->setFirstName($customerInfo->getFirstName());
            $customer->setLastName($customerInfo->getLastName());
        }
        // it seems providing REQBILLINGADDRESS=1 would return billing address. library has no implementation of this. http://stackoverflow.com/questions/17369587/paypal-express-checkout-billing-addresses-in-sandbox
        if ($shippingAddress = $details->getShippingAddress()) {
            $address = $customer->getPrimaryAddress();
            if ($shippingAddress->getStreet()) {
                $address->setAddress1($shippingAddress->getStreet());
            }
            if ($shippingAddress->getStreet2()) {
                $address->setAddress2($shippingAddress->getStreet2());
            }
            if ($shippingAddress->getCity()) {
                $address->setCity($shippingAddress->getCity());
            }
            if ($shippingAddress->getZip()) {
                $address->setPostCode($shippingAddress->getZip());
            }
            if ($shippingAddress->getCountry()) {
                $address->setCountryIso($shippingAddress->getCountry());
            }
        }
        return $this->customerRepository->saveEntity($customer);
    }
}