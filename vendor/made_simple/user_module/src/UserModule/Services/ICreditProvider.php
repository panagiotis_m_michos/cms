<?php

namespace UserModule\Services;

use UserModule\Contracts\ICustomer;

interface ICreditProvider
{
    /**
     * @param ICustomer $customer
     * @param float $credit
     */
    public function subtractCredit(ICustomer $customer, $credit);
}