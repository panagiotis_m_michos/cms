<?php

namespace UserModule\Services;

use OrmModule\Contracts\IRepository;
use SessionModule\ISession;
use UserModule\Contracts\ICustomer;
use UserModule\Exceptions\CustomerRequiredException;

final class CustomerAvailability implements ICustomerAvailability
{
    /**
     * @var ISession
     */
    private $session;

    /**
     * @var IRepository
     */
    private $repository;

    /**
     * @var string
     */
    private $loggedInKey;

    /**
     * @var string
     */
    private $temporaryKey;

    /**
     * @param IRepository $repository
     * @param ISession $session
     * @param string $loggedInKey
     * @param $temporaryKey
     */
    public function __construct(IRepository $repository, ISession $session, $loggedInKey, $temporaryKey)
    {
        $this->session = $session;
        $this->repository = $repository;
        $this->loggedInKey = $loggedInKey;
        $this->temporaryKey = $temporaryKey;
    }

    /**
     * @return ICustomer
     * @throws CustomerRequiredException
     */
    public function requireTemporaryCustomer()
    {
        $customer = $this->optionalTemporaryCustomer();
        if (!$customer) {
            throw CustomerRequiredException::noTemporary();
        }
        return $customer;
    }

    /**
     * @return ICustomer
     * @throws CustomerRequiredException
     */
    public function requireCustomer()
    {
        $customer = $this->optionalLoggedInCustomer();
        if (!$customer) {
            $customer = $this->requireTemporaryCustomer();
        }
        return $customer;
    }

    /**
     * @return ICustomer
     * @throws CustomerRequiredException
     */
    public function requireLoggedInCustomer()
    {
        $customer = $this->optionalLoggedInCustomer();
        if (!$customer) {
            throw CustomerRequiredException::noCustomer();
        }
        return $customer;
    }

    /**
     * @return ICustomer|NULL
     */
    public function optionalLoggedInCustomer()
    {
        $customerId = $this->session->get($this->loggedInKey);
        if (!$customerId) {
            return NULL;
        }
        return $this->repository->find($customerId);
    }

    /**
     * @return ICustomer|NULL
     */
    public function optionalTemporaryCustomer()
    {
        return $this->session->get($this->temporaryKey);
    }
}