<?php

namespace UserModule\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use UserModule\Dto\LoginAvailabilityData;

class ForgottenPasswordForm extends AbstractType
{
    /**
     * @var string
     */
    private $name;

    /**
     * ForgottenPasswordForm constructor.
     * @param string $name
     */
    public function __construct($name = ForgottenPasswordForm::class)
    {
        $this->name = $name;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, ['label' => 'Email:*', 'attr' => ['rv-disabled' => 'emailConfirmed']])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => LoginAvailabilityData::class,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}