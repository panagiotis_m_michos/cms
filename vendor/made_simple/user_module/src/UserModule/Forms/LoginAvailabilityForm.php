<?php

namespace UserModule\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use UserModule\Dto\LoginAvailabilityData;

class LoginAvailabilityForm  extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, ['label' => 'Email:*', 'attr' => ['rv-disabled' => 'isEmailLocked < emailConfirmed requiresToLogin']])
            ->add('password', PasswordType::class, ['label' => 'Password:', 'required' => FALSE, 'attr' => ['rv-value' => 'password']])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => LoginAvailabilityData::class,
            'validation_groups' => function(FormInterface $form) {
                $groups = ['Default'];
                $data = $form->getData();
                if ($data->isLogginAttempt()) {
                    $groups[] = 'Password';
                }
                return $groups;
            }
        ));
    }
}