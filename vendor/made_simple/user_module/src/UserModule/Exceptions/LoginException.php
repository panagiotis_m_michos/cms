<?php

namespace UserModule\Exceptions;

use Exception;
use LogicException;

class LoginException extends LogicException
{
    /**
     * @param Exception $e
     * @return LoginException
     */
    public static function fromNumeric(Exception $e)
    {
        $message = 'Email or password does not match';
        switch ($e->getMessage()) {
            case 3:
                $message = 'Your account haven\'t been validated!';
                break;
            case 4:
                $message = 'Your account is blocked. Please contact our customer supoort line on 0207 608 5500';
                break;

        }
        return new self($message, $e->getCode(), $e);
    }
}