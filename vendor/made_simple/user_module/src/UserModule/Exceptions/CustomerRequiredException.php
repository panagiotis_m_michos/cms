<?php

namespace UserModule\Exceptions;

use LogicException;
use TranslationModule\Config\MsgId;

class CustomerRequiredException extends LogicException
{
    /**
     * @return CustomerRequiredException
     */
    public static function noCustomer()
    {
        return new self(s_(MsgId::CUSTOMER_NOT_LOGGED_IN));
    }

    /**
     * @return CustomerRequiredException
     */
    public static function noTemporary()
    {
        return new self(s_(MsgId::CUSTOMER_NOT_TEMPORARY));
    }
}