<?php

namespace UserModule\Exceptions;

use LogicException;
use TranslationModule\Config\MsgId;

class CreditException extends LogicException
{
    /**
     * @param float $existingCredit
     * @param float $requestedCredit
     * @return CreditException
     */
    public static function notEnough($existingCredit, $requestedCredit)
    {
        return new self(s_(MsgId::NOT_ENOUGH_CREDIT, $existingCredit, $requestedCredit));
    }
}