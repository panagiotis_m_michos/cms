<?php

namespace UserModule\Exceptions;

use LogicException;
use TranslationModule\Config\MsgId;

class CustomerAvailabilityException extends LogicException
{
    /**
     * @param string $email
     * @return CustomerAvailabilityException
     */
    public static function emailTaken($email)
    {
        return new self(s_(MsgId::CUSTOMER_EMAIL_TAKEN, $email));
    }

    /**
     * @param string $email
     * @return CustomerAvailabilityException
     */
    public static function emailNotFound($email)
    {
        return new self(s_(MsgId::CUSTOMER_EMAIL_NOT_FOUND, $email));
    }
}