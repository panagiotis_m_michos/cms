<?php

namespace spec\UserModule\Views;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\Form\FormInterface;
use UserModule\Contracts\ICustomer;
use UserModule\Contracts\ILoginAvailability;
use UserModule\Views\LoginAvailabilityView;

/**
 * @mixin LoginAvailabilityView
 */
class LoginAvailabilityViewSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('UserModule\Views\LoginAvailabilityView');
        $this->shouldHaveType(ILoginAvailability::class);
    }

    function it_should_be_providing_email_confirmed(FormInterface $form, ICustomer $customer)
    {
        $customer->getEmail()->willReturn('test@madesimplegroup.com');
        $this->beConstructedThrough([LoginAvailabilityView::class, 'withDetails'], [$form, $customer]);
        $this->isEmailConfirmed()->shouldBe(TRUE);
    }

    function it_should_be_providing_email_confirmed_when_there_is_no_customer(FormInterface $form)
    {
        $this->beConstructedThrough([LoginAvailabilityView::class, 'withDetails'], [$form]);
        $this->isEmailConfirmed()->shouldBe(FALSE);
    }

    function it_should_be_providing_email_confirmed_when_email_is_taken(FormInterface $form)
    {
        $this->beConstructedThrough([LoginAvailabilityView::class, 'withDetails'], [$form, NULL, TRUE]);
        $this->isEmailConfirmed()->shouldBe(FALSE);
    }

    function it_should_be_providing_login_requirement(FormInterface $form)
    {
        $this->beConstructedThrough([LoginAvailabilityView::class, 'withDetails'], [$form, NULL, TRUE]);
        $this->isRequiredToLogIn()->shouldBe(TRUE);
    }

    function it_should_provide_if_customer_is_logged_in(FormInterface $form, ICustomer $customer)
    {
        $customer->getEmail()->willReturn('test@madesimplegroup.com');
        $customer->getId()->willReturn(1);
        $this->beConstructedThrough([LoginAvailabilityView::class, 'withDetails'], [$form, $customer]);
        $this->isLoggedIn()->shouldBe(TRUE);
    }

    function it_should_be_providing_logged_in_customer(FormInterface $form, ICustomer $customer)
    {
        $customer->getEmail()->willReturn('test@madesimplegroup.com');
        $customer->getId()->willReturn(1);
        $this->beConstructedThrough([LoginAvailabilityView::class, 'withDetails'], [$form, $customer]);
        $this->getLoggedInCustomer()->shouldBe($customer);
    }
}
