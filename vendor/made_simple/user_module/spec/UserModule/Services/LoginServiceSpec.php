<?php

namespace spec\UserModule\Services;

use OrmModule\Contracts\IRepository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use UserModule\Contracts\ICustomer;
use UserModule\Exceptions\CustomerAvailabilityException;
use UserModule\Services\LoginService;

/**
 * @mixin LoginService
 */
class LoginServiceSpec extends ObjectBehavior
{
    /**
     * @var IRepository
     */
    private $customerRepository;

    function let(IRepository $customerRepository)
    {
        $this->beConstructedWith($customerRepository);
        $this->customerRepository = $customerRepository;
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('UserModule\Services\LoginService');
        $this->shouldHaveType('UserModule\Services\ILoginService');
    }

    function it_should_reset_customer_password(ICustomer $customer)
    {
        $customer->setTemporaryPassword(Argument::that(function($value) {
            return strlen($value) === 6;
        }))->shouldBeCalled();
        $this->customerRepository->findOneBy(['email' => 'temp'])->willReturn($customer);
        $this->customerRepository->saveEntity($customer)->shouldBeCalled();
        $this->resetCustomerPassword('temp');
    }

    function it_should_fail_if_customer_is_not_found(ICustomer $customer)
    {
        $this->customerRepository->findOneBy(['email' => 'temp'])->willReturn(NULL);
        $this->shouldThrow(CustomerAvailabilityException::class)->during('resetCustomerPassword', ['temp']);
    }
}
