<?php

namespace spec\UserModule\Services;

use OrmModule\Contracts\IRepository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use SessionModule\ISession;
use UserModule\Contracts\ICustomer;
use UserModule\Exceptions\CustomerRequiredException;
use UserModule\Services\CustomerAvailability;

/**
 * @mixin CustomerAvailability
 */
class CustomerAvailabilitySpec extends ObjectBehavior
{
    /**
     * @var ISession
     */
    private $session;

    /**
     * @var IRepository
     */
    private $repository;

    function let(ISession $session, IRepository $repository)
    {
        $this->beConstructedWith($repository, $session, 'customerId', 'temporaryCustomer');
        $this->session = $session;
        $this->repository = $repository;
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('UserModule\Services\CustomerAvailability');
        $this->shouldHaveType('UserModule\Services\ICustomerAvailability');
    }

    function it_should_be_empty_when_there_is_no_logged_in_customer()
    {
        $this->optionalLoggedInCustomer()->shouldBe(NULL);
    }

    function it_should_return_logged_in_customer(ICustomer $customer)
    {
        $this->session->get('customerId')->willReturn(1);
        $this->repository->find(1)->willReturn($customer);
        $this->optionalLoggedInCustomer()->shouldBe($customer);
        $this->requireLoggedInCustomer()->shouldBe($customer);
        $this->requireCustomer()->shouldBe($customer);
    }

    function it_should_fail_when_there_is_no_logged_in_customer()
    {
        $this->shouldThrow(CustomerRequiredException::class)->during('requireLoggedInCustomer');
    }

    function it_should_be_empty_when_there_is_no_temporary_customer()
    {
        $this->optionalTemporaryCustomer()->shouldBe(NULL);
    }

    function it_should_return_temporary_customer(ICustomer $customer)
    {
        $this->session->get('customerId')->willReturn(NULL);
        $this->session->get('temporaryCustomer')->willReturn($customer);
        $this->optionalTemporaryCustomer()->shouldBe($customer);
        $this->requireTemporaryCustomer()->shouldBe($customer);
        $this->requireCustomer()->shouldBe($customer);
    }

    function it_should_fail_when_there_is_no_temporary_customer()
    {
        $this->shouldThrow(CustomerRequiredException::class)->during('requireTemporaryCustomer');
    }

    function it_should_fail_when_no_logged_in_or_temporary_customer()
    {
        $this->shouldThrow(CustomerRequiredException::class)->during('requireCustomer');
    }
}
