<?php

namespace spec\UserModule\Services;

use PaymentModule\Contracts\IPaymentResponse;
use PayPalNVP\Environment;
use PayPalNVP\Fields\PayerName;
use PayPalNVP\Fields\ShippingAddress;
use PayPalNVP\Response\GetExpressCheckoutDetailsResponse;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use OrmModule\Contracts\IRepository;
use SagePay\Token\Request\Details;
use UserModule\Contracts\IAddress;
use UserModule\Contracts\ICustomer;

/**
 * @TODO very questionable behaviour tests. monitor how simple changes to behaviour brakes it
 */
class CustomerDetailsServiceSpec extends ObjectBehavior
{
    /**
     * @var IRepository
     */
    private $repository;

    function let(IRepository $repository)
    {
        $this->beConstructedWith($repository);
        $this->repository = $repository;
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('UserModule\Services\CustomerDetailsService');
    }

    function it_should_map_sage_details(ICustomer $customer, IAddress $address)
    {
        $details = $this->create_details();
        $this->sage_expectations($customer, $address, $details);
        $this->saveCustomerDetailsFromSagePay($customer, $details)->shouldBe($customer);
    }

    function it_should_map_paypal_details(ICustomer $customer, IAddress $address)
    {
        $details = $this->create_paypal_response();
        $shipping = $details->getPayments()[0]->getShippingAddress();
        $this->paypal_expectations($customer, $address, $details->getPayerName(), $shipping);
        $this->saveCustomerDetailsFromPaypal($customer, $details)->shouldBe($customer);
    }

    function it_should_not_process_payment_details_when_registration_is_completed(ICustomer $customer, IPaymentResponse $paymentResponse)
    {
        $customer->hasCompletedRegistration()->willReturn(TRUE);
        $this->processPaymentDetails($customer, $paymentResponse)->shouldBe(FALSE);
    }

    function it_should_process_payment_details(ICustomer $customer, IAddress $address, IPaymentResponse $paymentResponse)
    {
        $details = $this->create_paypal_response();
        $shipping = $details->getPayments()[0]->getShippingAddress();
        $this->paypal_expectations($customer, $address, $details->getPayerName(), $shipping);
        $customer->hasCompletedRegistration()->willReturn(FALSE);
        $paymentResponse->isSagePay()->willReturn(FALSE);
        $paymentResponse->isPaypal()->willReturn(TRUE);
        $paymentResponse->getPaypalPaymentInfo()->willReturn($details);
        $this->processPaymentDetails($customer, $paymentResponse)->shouldBe($customer);

    }

    private function sage_expectations(ICustomer $customer, IAddress $address, Details $details)
    {
        $customer->getPrimaryAddress()->willReturn($address);
        $customer->setFirstName($details->getBillingFirstnames())->shouldBeCalled();
        $customer->setLastName($details->getBillingSurname())->shouldBeCalled();
        $address->setAddress1($details->getBillingAddress1())->shouldBeCalled();
        //$address->setAddress2($details->getBillingAddress2())->shouldBeCalled();
        $address->setPostCode($details->getBillingPostCode())->shouldBeCalled();
        $address->setCity($details->getBillingCity())->shouldBeCalled();
        $address->setCountryIso($details->getBillingCountry())->shouldBeCalled();
        $this->repository->saveEntity($customer)->willReturn($customer);
    }

    private function paypal_expectations(ICustomer $customer, IAddress $address, PayerName $name, ShippingAddress $shippingAddress)
    {
        $customer->getPrimaryAddress()->willReturn($address);
        $customer->setTitleId($name->getSuffix())->shouldBeCalled();
        $customer->setFirstName($name->getFirstName())->shouldBeCalled();
        $customer->setLastName($name->getLastName())->shouldBeCalled();
        $address->setAddress1($shippingAddress->getStreet())->shouldBeCalled();
        //$address->setAddress2($shippingAddress->getStreet2())->shouldBeCalled();
        $address->setPostCode($shippingAddress->getZip())->shouldBeCalled();
        $address->setCity($shippingAddress->getCity())->shouldBeCalled();
        $address->setCountryIso($shippingAddress->getCountry())->shouldBeCalled();
        $this->repository->saveEntity($customer)->willReturn($customer);
    }

    /**
     * @return Details
     */
    private function create_details()
    {
        $det = new Details();
        $det->amount = 12.23;
        $det->currency = 'GBP';
        $det->description = 'description';
        $det->billingSurname = 'Tester';
        $det->billingFirstnames = 'Johny';
        $det->billingAddress1 = '88';
        $det->billingCity = 'London';
        $det->billingPostCode = '412';
        $det->billingCountry = 'GB';
        $det->deliverySurname = 'OtherTester';
        $det->deliveryFirstnames = 'OtherJohny';
        $det->deliveryAddress1 = '13 St. John Street';
        $det->deliveryCity = 'London';
        $det->deliveryPostCode = 'ec1 4r1';
        $det->deliveryCountry = 'GB';
        $det->token = '492900000000649290000000060000';
        return $det;
    }

    /**
     * @return GetExpressCheckoutDetailsResponse
     */
    private function create_paypal_response()
    {
        $response = 'TOKEN=EC%2d1D381042K2799650F&PHONENUM=%2b44%202076085500&BILLINGAGREEMENTACCEPTEDSTATUS=0&CHECKOUTSTATUS=PaymentActionNotInitiated&TIMESTAMP=2015%2d12%2d02T09%3a11%3a47Z&CORRELATIONID=65324ce0e3c66&ACK=Success&VERSION=89%2e0&BUILD=18308778&EMAIL=tomasj%40madesimplegroup%2ecom&PAYERID=FDJNWNWPV67TJ&PAYERSTATUS=unverified&FIRSTNAME=John&LASTNAME=Boy&COUNTRYCODE=GB&SHIPTONAME=John%20Boy&SHIPTOSTREET=Farm%20in%20Essex&SHIPTOCITY=Farmhouse&SHIPTOSTATE=Essex&SHIPTOZIP=EC1V%204PY&SHIPTOCOUNTRYCODE=GB&SHIPTOCOUNTRYNAME=United%20Kingdom&ADDRESSSTATUS=Confirmed&CURRENCYCODE=GBP&AMT=5%2e94&ITEMAMT=5%2e94&SHIPPINGAMT=0%2e00&HANDLINGAMT=0%2e00&TAXAMT=0%2e00&CUSTOM=COMPANYSEARCHESMADESIMPLE&INSURANCEAMT=0%2e00&SHIPDISCAMT=0%2e00&INSURANCEOPTIONOFFERED=false&L_QTY0=1&L_TAXAMT0=0%2e00&L_AMT0=5%2e94&PAYMENTREQUEST_0_CURRENCYCODE=GBP&PAYMENTREQUEST_0_AMT=5%2e94&PAYMENTREQUEST_0_ITEMAMT=5%2e94&PAYMENTREQUEST_0_SHIPPINGAMT=0%2e00&PAYMENTREQUEST_0_HANDLINGAMT=0%2e00&PAYMENTREQUEST_0_TAXAMT=0%2e00&PAYMENTREQUEST_0_CUSTOM=COMPANYSEARCHESMADESIMPLE&PAYMENTREQUEST_0_INSURANCEAMT=0%2e00&PAYMENTREQUEST_0_SHIPDISCAMT=0%2e00&PAYMENTREQUEST_0_SELLERPAYPALACCOUNTID=diviak_1225732123_biz%40gmail%2ecom&PAYMENTREQUEST_0_INSURANCEOPTIONOFFERED=false&PAYMENTREQUEST_0_SHIPTONAME=John%20Boy&PAYMENTREQUEST_0_SHIPTOSTREET=Farm%20in%20Essex&PAYMENTREQUEST_0_SHIPTOCITY=Farmhouse&PAYMENTREQUEST_0_SHIPTOSTATE=Essex&PAYMENTREQUEST_0_SHIPTOZIP=EC1V%204PY&PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE=GB&PAYMENTREQUEST_0_SHIPTOCOUNTRYNAME=United%20Kingdom&PAYMENTREQUEST_0_ADDRESSSTATUS=Confirmed&L_PAYMENTREQUEST_0_QTY0=1&L_PAYMENTREQUEST_0_TAXAMT0=0%2e00&L_PAYMENTREQUEST_0_AMT0=5%2e94&PAYMENTREQUESTINFO_0_ERRORCODE=0';
        return new GetExpressCheckoutDetailsResponse($response, Environment::SANDBOX());
    }
}
