<?php

namespace spec\UserModule\Services;

use OrmModule\Contracts\IRepository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use UserModule\Contracts\ICustomer;
use UserModule\Exceptions\CreditException;

class CustomerCreditHandlerSpec extends ObjectBehavior
{
    /**
     * @var IRepository
     */
    private $repository;

    function let(IRepository $repository)
    {
        $this->beConstructedWith($repository);
        $this->repository = $repository;
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('UserModule\Services\CustomerCreditHandler');
        $this->shouldHaveType('UserModule\Services\ICreditProvider');
    }

    // how to test it properly ?
    function it_should_subtract_credit(ICustomer $customerWithCredits)
    {
        $customerWithCredits->hasEnoughCredit(30)->willReturn(TRUE);
        $customerWithCredits->subtractCredit(30)->shouldBeCalled();
        $this->subtractCredit($customerWithCredits, 30);
    }

    // how to test it properly ?
    function it_should_fail_if_customer_does_not_have_enough_credit(ICustomer $customerWithCredits)
    {
        $customerWithCredits->hasEnoughCredit(30)->willReturn(FALSE);
        $customerWithCredits->getCredit()->willReturn(10);
        $this->shouldThrow(CreditException::class)->during('subtractCredit', [$customerWithCredits, 30]);
    }
}
