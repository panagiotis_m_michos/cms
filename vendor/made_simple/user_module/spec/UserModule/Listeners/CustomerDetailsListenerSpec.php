<?php

namespace spec\UserModule\Listeners;

use BasketModule\Contracts\IBasket;
use PaymentModule\Contracts\IPaymentResponse;
use PaymentModule\Events\PaymentEvent;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use UserModule\Contracts\ICustomer;
use UserModule\Services\CustomerDetailsService;

class CustomerDetailsListenerSpec extends ObjectBehavior
{
    /**
     * @var CustomerDetailsService
     */
    private $customerDetailsService;

    function let(CustomerDetailsService $customerDetailsService)
    {
        $this->beConstructedWith($customerDetailsService);
        $this->customerDetailsService = $customerDetailsService;
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('UserModule\Listeners\CustomerDetailsListener');
        $this->shouldHaveType('Symfony\Component\EventDispatcher\EventSubscriberInterface');
    }

    function it_should_call_service(ICustomer $customer, IPaymentResponse $paymentResponse, IBasket $basket)
    {
        $paymentEvent = new PaymentEvent($customer->getWrappedObject(), $basket->getWrappedObject(), $paymentResponse->getWrappedObject());
        $this->customerDetailsService->processPaymentDetails($customer, $paymentResponse)->shouldBeCalled();
        $this->onPaymentSuccess($paymentEvent);
    }
}
