<?php

namespace RouterModule;

use FeatureModule\FeatureToggle;
use RouterModule\Exceptions\RouteException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Router;
use RouterModule\Exceptions\InvalidArgumentException as InvalidArgumentRouteException;

class RouterProcessor
{
    /**
     * @var Router
     */
    private $router;

    /**
     * @var Dispatcher
     */
    private $dispatcher;

    /**
     * @var ResponseHandler
     */
    private $responseHandler;

    /**
     * @var FeatureToggle
     */
    private $featureToggle;

    /**
     * @var RouteParameter
     */
    private $routeParameter;


    /**
     * @param Router $router
     * @param Dispatcher $dispatcher
     * @param ResponseHandler $responseHandler
     * @param FeatureToggle $featureToggle
     */
    public function __construct(Router $router, Dispatcher $dispatcher, ResponseHandler $responseHandler, FeatureToggle $featureToggle = NULL)
    {
        $this->router = $router;
        $this->dispatcher = $dispatcher;
        $this->responseHandler = $responseHandler;
        $this->featureToggle = $featureToggle;
    }

    /**
     * @param Request $request
     * @throws RouteException
     */
    public function handle(Request $request)
    {
        try {
            //$route = self::isAdmin() && array_key_exists('REQUEST_URI', $_SERVER) ? $this->newRouter->match(strtok($_SERVER["REQUEST_URI"], '?')) :
            $route = $this->router->matchRequest($request);
            $this->routeParameter = RouteParameter::fromArray($route);
            if ($this->routeParameter->getFeature()) {
                if (!$this->featureToggle || !$this->featureToggle->isEnabled($this->routeParameter->getFeature())) {
                    throw RouteException::disabledFeature($this->routeParameter);
                }
            }
            $response = $this->dispatcher->dispatch($this->routeParameter, $request);
            $this->responseHandler->handle($this->routeParameter, $response);
        } catch (ResourceNotFoundException $e) {
            throw RouteException::processingFailed($e);
        } catch (MethodNotAllowedException $e) {
            throw RouteException::processingFailed($e);
        } catch (InvalidArgumentRouteException $e) {
            throw RouteException::processingFailed($e);
        }
    }

    /**
     * @return RouteParameter
     */
    public function getCurrentRouteParameters()
    {
        return $this->routeParameter;
    }

    /**
     * @param FeatureToggle $featureToggle
     */
    public function setFeatureToggle(FeatureToggle $featureToggle)
    {
        $this->featureToggle = $featureToggle;
    }
}
