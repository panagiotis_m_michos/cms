<?php

namespace RouterModule;

use RouterModule\Exceptions\TemplateException;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ResponseHandler
{

    /**
     * @var Container
     */
    private $containerBuilder;

    /**
     * @var TemplateRenderer
     */
    private $templateRenderer;

    /**
     * @var string
     */
    private $templateFolderName;

    /**
     * @param Container $containerBuilder
     * @param TemplateRenderer $templateRenderer
     * @param string $templateFolderName
     */
    public function __construct(Container $containerBuilder, TemplateRenderer $templateRenderer, $templateFolderName = 'Templates')
    {
        $this->containerBuilder = $containerBuilder;
        $this->templateRenderer = $templateRenderer;
        $this->templateFolderName = $templateFolderName;
    }

    /**
     * @param RouteParameter $parameter
     * @param $rawResponse
     * @return Response
     * @throws TemplateException
     */
    public function handle(RouteParameter $parameter, $rawResponse)
    {
        if ($rawResponse instanceof Response) {
            $response = $rawResponse;
        } elseif (is_string($rawResponse)) {
            $response = new Response($rawResponse);
        } elseif (is_array($rawResponse)) {
            $response = new JsonResponse($rawResponse);
        } else {
            $pathToTemplate = $this->getTemplatePath($parameter);
            $response = new Response($this->templateRenderer->getHtml($pathToTemplate));
        }
        return $response->send();
    }

    /**
     * RoutingModule/Controllers/TestController:renderList => RoutingModule/Templates/TestController/renderList
     * @param RouteParameter $parameter
     * @return string
     * @throws TemplateException
     */
    public function getTemplatePath(RouteParameter $parameter)
    {
        $controller = $this->containerBuilder->get($parameter->getController());
        $namespace = get_class($controller);
        $namespacePath = explode('\\', $namespace);
        //replace second part with Templates
        if (!isset($namespacePath[1])) {
            throw TemplateException::wrongNamespace($namespace);
        }
        $namespacePath[1] = $this->templateFolderName;
        return implode(DIRECTORY_SEPARATOR, $namespacePath) . DIRECTORY_SEPARATOR . $parameter->getAction();
    }
}
