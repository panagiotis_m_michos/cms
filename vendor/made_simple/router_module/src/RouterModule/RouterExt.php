<?php

namespace RouterModule;

use BootstrapModule\Ext\IExtension;
use RouterModule\Locators\DynamicFileLocator;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Router;

class RouterExt implements IExtension
{
    const ROUTER = 'symfony.router';
    const REQUEST = 'symfony.request';
    const ROUTE_PROCESSOR = 'router_module.route_processor';

    /**
     * @param Container $container
     */
    public function load(Container $container)
    {
        if ($container->hasParameter('router.generated_file')) {
            $locator = new DynamicFileLocator(
                new FileLocator(),
                $container->getParameter('routes'),
                $container->getParameter('debug_mode')
            );
            $mainRouteFile = $container->getParameter('router.generated_file');
        } else {
            $locator = new FileLocator($container->getParameter('router.paths'));
            $mainRouteFile = $container->getParameter('router.file');
        }
        $requestContext = new RequestContext();
        $request = Request::createFromGlobals();
        $requestContext->fromRequest($request);

        $router = new Router(
            new YamlFileLoader($locator),
            $mainRouteFile,
            array(
                'cache_dir' => $container->getParameter('router.cache_dir'),
                'debug' => $container->getParameter('debug_mode')
            ),
            $requestContext
        );

        if (!$requestContext->getHost()) {
            $requestContext->setScheme('http');
            $requestContext->setHttpPort(80);
            $requestContext->setHttpsPort(443);

            if ($container->hasParameter('host')) {
                $requestContext->setHost($container->getParameter('host'));
            }

            if ($container->hasParameter('scheme')) {
                $requestContext->setScheme($container->getParameter('scheme'));
            }

            if ($container->hasParameter('http_port')) {
                $requestContext->setHttpPort($container->getParameter('http_port'));
            }

            if ($container->hasParameter('https_port')) {
                $requestContext->setHttpsPort($container->getParameter('https_port'));
            }
        }

        $container->set(self::ROUTER, $router);
        $container->set(self::REQUEST, $request);
    }
}
