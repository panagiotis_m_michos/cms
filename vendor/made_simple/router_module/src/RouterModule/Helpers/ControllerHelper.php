<?php

namespace RouterModule\Helpers;

use Exception;
use InvalidArgumentException;
use RouterModule\Generators\IUrlGenerator;
use SessionModule\ISession;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class ControllerHelper
{
    const SESSION_FLASH = 'flash_messages';
    const SESSION_BACKLINK = 'backlink';
    const SESSION_DEFAULT = 'controller_helper';
    const SESSION_ONCE = 'session_temporary_flash_keys';

    const MESSAGE_INFO = 'info';
    const MESSAGE_SUCCESS = 'success';
    const MESSAGE_ERROR = 'error';

    const FLASH_DO_NOT_ESC = FALSE;

    /**
     * @var ISession
     */
    private $session;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var IUrlGenerator
     */
    private $urlGenerator;

    /**
     * @param ISession $session
     * @param IUrlGenerator $urlGenerator
     * @param FormFactory $formFactory
     * @param Request $request
     */
    public function __construct(ISession $session, IUrlGenerator $urlGenerator,
                                FormFactory $formFactory, Request $request)
    {
        $this->session = $session;
        $this->formFactory = $formFactory;
        $this->request = $request;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * @param string $name
     * @param array $params
     * @param int $options
     * @return string
     * @throws InvalidArgumentException
     */
    public function getUrl($name, $params = [], $options = IUrlGenerator::RELATIVE)
    {
        return $this->urlGenerator->url($name, $params, $options);
    }

    /**
     * @param string $name
     * @param array $params
     * @return string
     * @throws InvalidArgumentException
     */
    public function getSecureUrl($name, $params = [])
    {
        return $this->urlGenerator->url($name, $params, IUrlGenerator::SECURE);
    }

    /**
     * @param string $code
     * @param array $params
     * @param int $options
     * @return string
     */
    public function getLink($code, $params = [], $options = IUrlGenerator::OLD_LINK)
    {
        return $this->urlGenerator->url($code, is_array($params) ? $params : [], $options !== NULL ? $options | IUrlGenerator::OLD_LINK : NULL);
    }

    /**
     * @param string $code
     * @param array $params
     * @return string
     * @throws InvalidArgumentException
     */
    public function getSecureLink($code, $params = [])
    {
        return $this->getLink($code, $params, IUrlGenerator::SECURE);
    }

    /**
     * @param string $message
     * @param string $type
     * @param bool|null $escapeOutput (false will not escape output)
     * @param bool $isDismissable
     */
    public function setFlashMessage($message, $type = self::MESSAGE_INFO, $escapeOutput = NULL, $isDismissable = TRUE)
    {
        $flashes = $this->session->get( static::SESSION_FLASH . '.messages', []);
        $flashes[] = ['text' => $message, 'type' => $type, 'escapeOutput' => $escapeOutput, 'isDismissable' => $isDismissable];
        $this->session->set(static::SESSION_FLASH . '.messages', $flashes);
    }

    /**
     * @return array
     */
    public function readFlashMessages()
    {
        $flashes = $this->session->get(static::SESSION_FLASH . '.messages', []);
        $this->session->remove(static::SESSION_FLASH . '.messages');
        return $flashes;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getSessionValue($key)
    {
        return $this->session->get(static::SESSION_DEFAULT . '.' . $key);
    }

    /**
     * @param string $key
     * @param string $value
     */
    public function setSessionValue($key, $value)
    {
        $this->session->set(static::SESSION_DEFAULT . '.' . $key, $value);
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getFlashSessionValue($key)
    {
        $return = $this->session->get(static::SESSION_ONCE . '.'. $key);
        $this->session->remove(static::SESSION_ONCE . '.' . $key);
        return $return;
    }

    /**
     * @param string $key
     * @param string $value
     */
    public function setFlashSessionValue($key, $value)
    {
        $this->session->set(static::SESSION_ONCE . '.' . $key, $value);
    }

    /**
     * @param mixed $type
     * @param mixed $data
     * @param array $options
     * @return FormInterface
     */
    public function buildForm($type, $data = NULL, $options = array())
    {
        $form = $this->formFactory->create($type, $data, $options);
        $form->handleRequest($this->request);
        return $form;
    }

    /**
     * if method is post, but we want data from get
     * @param mixed $type
     * @param mixed $data
     * @param array $options
     * @return FormInterface
     */
    public function buildFormForGet($type, $data = NULL, $options = array())
    {
        $options['method'] = 'GET';
        $form = $this->formFactory->create($type, $data, $options);
        $request = clone $this->request;
        $request->setMethod('GET');
        $form->handleRequest($request);
        return $form;
    }

    /**
     * left for backward compatibility
     *
     * @param string|Exception $message
     * @param string $url
     * @return JsonResponse|RedirectResponse
     */
    public function getAjaxResponse($message, $url)
    {
        if ($this->request->isXmlHttpRequest()) {
            $response = $message instanceof Exception
                ? new JsonResponse(array('error' => $message->getMessage()), JsonResponse::HTTP_SERVICE_UNAVAILABLE)
                : new JsonResponse(array('success' => TRUE, 'message' => $message));
        } else {
            $this->setFlashMessage($message instanceof Exception ? $message->getMessage() : $message);
            return new RedirectResponse($url);
        }
        return $response;
    }

    /**
     * @param string|Exception|array $data
     * @param string $url
     * @param string $flashType
     * @param bool|null $escapeOutput
     * @return JsonResponse|RedirectResponse
     */
    public function getFlexibleResponse($data, $url, $flashType = self::MESSAGE_SUCCESS, $escapeOutput = NULL)
    {
        $responseData = is_array($data) ? $data : [];
        if (!is_array($data)) {
            if ($data instanceof Exception) {
                $responseData['error'] = $data->getMessage();
            } else {
                $responseData['success'] = TRUE;
                $responseData['message'] = $data;
            }
        }
        if (empty($responseData['success']) && empty($responseData['error'])) {
            $responseData['success'] = TRUE;
        }
        if (empty($responseData['message'])) {
            if (!empty($responseData['error'])) {
                $responseData['message'] = $responseData['error'];
            } else {
                $responseData['message'] = 'Successfully updated!';
            }
        }
        if ($this->request->isXmlHttpRequest()) {
            return new JsonResponse($responseData, empty($responseData['error']) ?
                JsonResponse::HTTP_OK : JsonResponse::HTTP_SERVICE_UNAVAILABLE);
        } else {
            $this->setFlashMessage($responseData['message'], empty($responseData['error']) ? $flashType : self::MESSAGE_ERROR, $escapeOutput);
            return new RedirectResponse($url);
        }
    }

    /**
     * Returns back link or false if is not set
     * @return mixed
     */
    public function getBacklink()
    {
        return $this->session->get(static::SESSION_BACKLINK . '.backlink', FALSE);
    }

    /**
     * @param string $url
     */
    public function setBacklink($url)
    {
        $this->session->set(static::SESSION_BACKLINK . '.backlink', $url);
    }

    /**
     * @return bool
     */
    public function isAjax()
    {
        return $this->request->isXmlHttpRequest();
    }

    /**
     * @return array
     */
    public function getQueryParams()
    {
        return $this->request->query->all();
    }

    /**
     * @return bool
     */
    public function isRefererGoogle()
    {
        $referer = $this->request->headers->get('referer');
        if ($referer) {
            $host = parse_url($referer, PHP_URL_HOST);
            if (preg_match("/google/i", $host)) {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * @param string $url
     * @param array $params
     * @return RedirectResponse
     */
    public function redirectionTo($url, array $params = [])
    {
        return new RedirectResponse($this->getUrl($url, $params));
    }

    /**
     * @return bool
     */
    public function isPostRequest()
    {
        return $this->request->isMethod('POST');
    }
}
