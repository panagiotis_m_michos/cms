<?php

namespace RouterModule;

interface IRouterParameter
{
    /**
     * @return string
     */
    public function getController();

    /**
     * @return string
     */
    public function getAction();

    /**
     * @return int
     */
    public function getPage();

    /**
     * @return string
     */
    public function getRequiredController();

    /**
     * @return array|NULL
     */
    public function getRequirements();

    /**
     * @param string $name
     * @return mixed
     */
    public function getRequirement($name);

    /**
     * @return string
     */
    public function getFeature();
}