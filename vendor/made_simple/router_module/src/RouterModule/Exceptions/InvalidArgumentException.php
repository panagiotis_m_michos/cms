<?php

namespace RouterModule\Exceptions;

use InvalidArgumentException as PhpInvalidArgumentException;

class InvalidArgumentException extends PhpInvalidArgumentException
{

}