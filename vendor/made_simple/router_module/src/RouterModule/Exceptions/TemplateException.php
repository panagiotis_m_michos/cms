<?php

namespace RouterModule\Exceptions;

use RuntimeException;

class TemplateException extends RuntimeException
{
    /**
     * @param string $path
     * @return TemplateException
     */
    public static function notFound($path)
    {
        return new self(sprintf('Template does not exist %s', $path));
    }

    /**
     * @param string $namespace
     * @return TemplateException
     */
    public static function wrongNamespace($namespace)
    {
        return new self(sprintf('Unsupported %s namespace for template found!', $namespace));
    }
}