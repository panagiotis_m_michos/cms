<?php

namespace RouterModule\Exceptions;

use Exception;
use RouterModule\RouteParameter;
use RuntimeException;

class RouteException extends RuntimeException
{
    /**
     * @param Exception $exception
     * @return RouteException
     */
    public static function processingFailed(Exception $exception)
    {
        return new self('Failed to process route to controller', $exception->getCode(), $exception);
    }

    /**
     * @param RouteParameter $routeParameter
     * @return RouteException
     */
    public static function disabledFeature(RouteParameter $routeParameter)
    {
        return new self(sprintf('Feature %s is disabled', $routeParameter->getFeature()));
    }
}