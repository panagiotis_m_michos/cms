<?php

namespace RouterModule\Exceptions;

use RuntimeException;

class ConverterException extends RuntimeException
{
    /**
     * @param string $name
     * @return ConverterException
     */
    public static function notFound($name)
    {
        return new self(sprintf('Converter %s does not exists', $name));
    }
}