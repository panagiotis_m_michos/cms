<?php

namespace RouterModule;

use FTemplate;
use RouterModule\Exceptions\TemplateException;

class TemplateRenderer
{
    /**
     * @var FTemplate
     */
    private $template;

    /**
     * @var string
     */
    private $rootDir;

    /**
     * @var string
     */
    private $ext;

    /**
     * @param FTemplate $template
     * @param string $rootDir
     * @param string $ext
     */
    public function __construct(FTemplate $template, $rootDir, $ext = 'tpl')
    {
        $this->template = $template;
        $this->rootDir = $rootDir;
        $this->ext = $ext;
    }

    /**
     * @param string $path
     * @return string
     * @throws TemplateException
     */
    public function getHtml($path)
    {
        $fullPath = $this->rootDir . DIRECTORY_SEPARATOR . $path . '.' . $this->ext;
        if (!file_exists($fullPath)) {
            throw TemplateException::notFound($fullPath);
        }
        return $this->template->getHtml($fullPath);
    }
}
