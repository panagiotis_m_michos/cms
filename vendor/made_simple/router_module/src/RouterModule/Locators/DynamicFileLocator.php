<?php

namespace RouterModule\Locators;

use Symfony\Component\Config\FileLocatorInterface;
use Symfony\Component\Yaml\Yaml;
use Utils\Directory;
use Utils\File;

class DynamicFileLocator implements FileLocatorInterface
{
    /**
     * @var string
     */
    private $routes;

    /**
     * @var bool
     */
    private $debug;

    /**
     * @var bool
     */
    private $generated;

    /**
     * @var FileLocatorInterface
     */
    private $fileLocator;

    /**
     * @param FileLocatorInterface $fileLocator
     * @param array $routes
     * @param bool $debug
     */
    public function __construct(FileLocatorInterface $fileLocator, array $routes, $debug)
    {
        $this->routes = $routes;
        $this->debug = $debug;
        $this->fileLocator = $fileLocator;
    }

    /**
     * Returns a full path for a given file name.
     *
     * @param string $name The file name to locate
     * @param string|null $currentPath The current path
     * @param bool $first Whether to return the first occurrence or an array of filenames
     *
     * @return string|array The full path to the file or an array of file paths
     *
     * @throws \InvalidArgumentException When file is not found
     */
    public function locate($name, $currentPath = NULL, $first = TRUE)
    {
        if (!$this->generated) {
            $filePath = $this->produceMainFileFromRoutes(new File($name), $this->routes, $this->debug);
            $this->generated = TRUE;
            return $filePath;
        }
        return $this->fileLocator->locate($name, $currentPath, $first);
    }

    /**
     * @param File $cacheFilePath
     * @param array $routes
     * @param bool $debug
     * @return string
     */
    private function produceMainFileFromRoutes(File $cacheFilePath, array $routes, $debug)
    {
        if (!$debug && $cacheFilePath->isReadable()) {
            return $cacheFilePath->getPath();
        } else {
            $fileContents = $cacheFilePath->isReadable() ? $cacheFilePath->getContent() : FALSE;
            $expectedContents = $this->produceFileContent($routes);
            if ($expectedContents !== $fileContents) {
                if (!$cacheFilePath->isReadable()) {
                    Directory::createWritable(dirname($cacheFilePath->getPathname()));
                }
                $file = $cacheFilePath->openFile('w+');
                $file->fwrite($expectedContents);
            }
            return $cacheFilePath->getPath();
        }
    }

    /**
     * @param array $routes
     * @return string
     */
    private function produceFileContent(array $routes)
    {
        $content = [];
        foreach ($routes as $route) {
            if (is_array($route)) {
                $content[] = $route;
            } else {
                $content[] = ['resource' => $route];
            }
        }
        return Yaml::dump($content);
    }
}