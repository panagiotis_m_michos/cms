<?php

namespace RouterModule\Tests;

use Entities\Customer;
use FTemplate;
use Symfony\Component\HttpFoundation\Response;
use Utils\Date;

class TestingController1
{
    /**
     * @var FTemplate
     */
    private $template;

    /**
     * @param FTemplate $template
     */
    public function __construct(FTemplate $template)
    {
        $this->template = $template;
    }

    /**
     * @param Customer $customer
     * @return Response
     */
    public function testTemplateCustomer(Customer $customer)
    {
        $this->template->test = 'testTemplate';
        return new Response(
            $this->template->getHtml(DOCUMENT_ROOT . '/project/FrontModule/templates/Home.default.tpl')
        );
    }

    /**
     * @param Date $date
     * @param string $name
     * @return Response
     */
    public function testTemplateDate(Date $date, $name)
    {
        $this->template->test = 'testTemplateDate';
        $this->template->date = $date;
        $this->template->name = $name;
        return new Response(
            $this->template->getHtml(DOCUMENT_ROOT . '/project/FrontModule/templates/Home.default.tpl')
        );
    }
}