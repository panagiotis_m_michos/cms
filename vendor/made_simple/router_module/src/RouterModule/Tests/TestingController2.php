<?php

namespace RouterModule\Tests;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Utils\Date;

class TestingController2
{
    /**
     * @param Request $request
     * @return Response
     */
    public function defaultMethod(Request $request)
    {
        return new Response(
            'Me and My Friend Irene'
        );
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function testJson(Request $request)
    {
        return new JsonResponse(array(
            'get' => 1
        ));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function testOnlyPost(Request $request)
    {
        return new JsonResponse(array(
            'post' => 1
        ));
    }

    /**
     * @param Date $date
     * @param int $test
     * @param string $anotherTest
     */
    public function specTest(Date $date, $test, $anotherTest)
    {
        return 'methodValue';
    }
}