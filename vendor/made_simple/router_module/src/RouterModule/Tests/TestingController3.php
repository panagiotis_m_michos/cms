<?php

namespace RouterModule\Tests;

use Entities\Customer;
use FTemplate;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TestingController3
{
    /**
     * @var FTemplate
     */
    private $template;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @param FTemplate $template
     * @param Request $request
     * @param Customer $customer
     */
    public function __construct(FTemplate $template, Request $request, Customer $customer)
    {
        $this->template = $template;
        $this->request = $request;
        $this->customer = $customer;
    }

    /**
     * @return Response
     */
    public function testTemplate()
    {
        $this->template->test = 'testTemplate';
        return new Response(
            $this->template->getHtml(DOCUMENT_ROOT . '/project/FrontModule/templates/Home.default.tpl')
        );
    }

    public function testTemplateWithoutResponse()
    {
        $this->template->test = 'testTemplateWithoutResponse';
    }
}