<?php

namespace RouterModule;

use Nette\Reflection\Method;
use ReflectionClass;
use ReflectionParameter;
use RouterModule\Exceptions\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Request;

class ObjectMapper
{
    /**
     * @var ConverterManager
     */
    private $converterManager;

    /**
     * @param ConverterManager $converterManager
     */
    public function __construct(ConverterManager $converterManager)
    {
        $this->converterManager = $converterManager;
    }

    /**
     * @param mixed $class
     * @param RouteParameter $parameter
     * @param Request $request
     * @return array
     * @throws InvalidArgumentException
     */
    public function convertArguments($class, RouteParameter $parameter, Request $request)
    {
        $reflectionClass = new ReflectionClass($class);
        $reflectionMethod = $reflectionClass->getMethod($parameter->getAction());
        $arguments = $reflectionMethod->getParameters();
        $annotationTypes = $this->getArgumentAnnotations($class, $parameter->getAction());
        $methodArguments = array();
        foreach ($arguments as $argument) {
            $annotationType = isset($annotationTypes[$argument->getName()]) ? $annotationTypes[$argument->getName()] : NULL;
            $methodArguments[] = $this->convert($argument, $request, $parameter, $annotationType);
        }
        return $methodArguments;
    }

    /**
     * @param ReflectionParameter $reflectionParameter
     * @param Request $request
     * @param RouteParameter $parameter
     * @param string $annotationType
     * @return mixed
     */
    public function convert(ReflectionParameter $reflectionParameter, Request $request, RouteParameter $parameter, $annotationType = NULL)
    {
        $parameterClass = $reflectionParameter->getClass();
        $value = $parameter->getValue($reflectionParameter->getName());
        if ($value === NULL) {
            $value = $request->get($reflectionParameter->getName());
        }
        if ($value === NULL && !$reflectionParameter->isOptional()) {
            throw new InvalidArgumentException(sprintf('No value provided for parameter %s', $reflectionParameter->getName()));
        }
        if ($parameterClass === NULL) {
            if ($annotationType !== NULL) {
                return $this->converterManager->convert($value, $annotationType, $parameter->getRequirement($reflectionParameter->getName()));
            }
            return $value;
        }
        $type = $parameterClass->getName();
        return $this->converterManager->convert(
            $value,
            $type,
            $parameter->getRequirement($reflectionParameter->getName())
        );
    }

    /**
     * @param string $class
     * @param string $method
     * @return array
     */
    public function getArgumentAnnotations($class, $method)
    {
        $methodReflection = Method::from($class, $method);
        $annotations = $methodReflection->getAnnotations();
        $simpleAnnotationTypes = array();
        if (isset($annotations['param'])) {
            foreach ($annotations['param'] as $annotation) {
                $parts = explode(' ', $annotation, 3);
                $parts = array_filter($parts);
                if (!empty($parts[0]) && !empty($parts[1])) {
                    //handle @param NULL|string or @param NULL | string properly
                    if (strpos($parts[0], '|') === FALSE && !empty($parts[1][0]) && $parts[1][0] === '$') {
                        $name = substr($parts[1], 1);
                        $simpleAnnotationTypes[$name] = $parts[0];
                    }
                }
            }
        }
        return $simpleAnnotationTypes;
    }
}