<?php

namespace RouterModule;

use RouterModule\Converters\IConverter;
use RouterModule\Exceptions\ConverterException;

class ConverterManager
{
    /**
     * @var array
     */
    private $converters = array();

    /**
     * @param mixed $value
     * @param string $type
     * @param mixed $configuration
     * @return mixed
     * @throws ConverterException
     */
    public function convert($value, $type, $configuration)
    {
        $name = !empty($configuration['converter']) ? $configuration['converter'] : $type;
        return $this->getConverter($name)->convert($value, $type, $configuration);
    }

    /**
     * @param string $name
     * @throws ConverterException
     */
    public function getConverter($name)
    {
        if (!isset($this->converters[$name])) {
            throw ConverterException::notFound($name);
        }
        return $this->converters[$name];
    }

    /**
     * @param array $types
     * @param IConverter $converter
     */
    public function addConverter(array $types, IConverter $converter)
    {
        foreach ($types as $typeName) {
            $this->converters[$typeName] = $converter;
        }
    }
}