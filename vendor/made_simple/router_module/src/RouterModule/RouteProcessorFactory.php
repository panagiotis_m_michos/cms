<?php

namespace RouterModule;

use BootstrapModule\Ext\DoctrineExt;
use FTemplate;
use RouterModule\Converters\DateTimeConverter;
use RouterModule\Converters\EntityConverter;
use RouterModule\Converters\SimpleTypeConverter;
use Symfony\Component\DependencyInjection\Container;


/**
 * Default factory for all projects
 * Class RouteProcessorFactory
 * @package RouterModule
 */
class RouteProcessorFactory
{
    /**
     * @param Container $container
     * @param FTemplate $template
     * @return RouterProcessor
     */
    public static function create(Container $container, FTemplate $template)
    {
        $converterManager = new ConverterManager();
        $converterManager->addConverter(
            ['doctrine'],
            new EntityConverter($container->get(DoctrineExt::ENTITY_MANAGER))
        );
        $converterManager->addConverter(['DateTime', 'Utils\Date'], new DateTimeConverter());
        $converterManager->addConverter(['float', 'int', 'integer', 'bool', 'boolean', 'string', 'array'], new SimpleTypeConverter());

        $dispatcher = new Dispatcher($container, new ObjectMapper($converterManager));
        $responseHandler = new ResponseHandler(
            $container,
            new TemplateRenderer(
                $template,
                $container->getParameter('router.template_path'),
                $container->getParameter('router.template_ext')
            ),
            $container->getParameter('router.template_folder_name')

        );

        return new RouterProcessor($container->get(RouterExt::ROUTER), $dispatcher, $responseHandler);
    }
}
