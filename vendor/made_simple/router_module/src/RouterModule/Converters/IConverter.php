<?php

namespace RouterModule\Converters;

use RouterModule\Exceptions\InvalidArgumentException;

interface IConverter
{
    /**
     * @param mixed $value
     * @param string $type
     * @param mixed $configuration
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function convert($value, $type, $configuration = NULL);

}