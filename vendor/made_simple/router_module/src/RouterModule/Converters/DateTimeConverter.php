<?php

namespace RouterModule\Converters;

use RouterModule\Exceptions\InvalidArgumentException;

class DateTimeConverter implements IConverter
{
    const DEFAULT_FORMAT = 'Y-m-d H:i:s';

    /**
     * @param mixed $value
     * @param string $type
     * @param mixed $configuration
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function convert($value, $type = 'DateTime', $configuration = NULL)
    {
        $format = static::DEFAULT_FORMAT;
        if (isset($configuration['format'])) {
            $format = $configuration['format'];
        }
        $method = 'createFromFormat';
        if (!is_callable(array($type, $method))) {
            throw new InvalidArgumentException(sprintf('Unable to create %s since it does not have method %s', $type, $method));
        }
        $dateTime = call_user_func_array(array($type, $method), array($format, $value));
        if (!$dateTime) {
            throw new InvalidArgumentException(sprintf('Unable to create %s with format %s from %s value provided', $type, $format, $value));
        }
        return $dateTime;
    }

}