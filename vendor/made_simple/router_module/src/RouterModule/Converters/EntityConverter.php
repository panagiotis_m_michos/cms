<?php

namespace RouterModule\Converters;

use Doctrine\Common\Persistence\ObjectManager;
use RouterModule\Exceptions\InvalidArgumentException;

class EntityConverter implements IConverter
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param mixed $value
     * @param string $type
     * @param mixed $configuration
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function convert($value, $type, $configuration = NULL)
    {
        $entity = $this->objectManager->getRepository($type)->find($value);
        if (!$entity) {
            throw new InvalidArgumentException(sprintf('Unable to find %s from %s value provided', $type, $value));
        }
        return $entity;
    }

}