<?php

namespace RouterModule\Converters;

class SimpleTypeConverter implements IConverter
{
    /**
     * @param mixed $value
     * @param string $type
     * @param mixed $configuration
     * @return mixed
     */
    public function convert($value, $type, $configuration = NULL)
    {
        switch ($type) {
            case 'int':
            case 'integer':
                return (int) $value;
            case 'float':
            case 'double':
                return (float) $value;
            case 'bool':
            case 'boolean':
                return (bool) $value;
            case 'string':
                return (string) $value;
            case 'array':
                return (array) $value;
        }
        return $value;
    }
}