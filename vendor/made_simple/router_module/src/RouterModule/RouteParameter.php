<?php

namespace RouterModule;

final class RouteParameter implements IRouterParameter
{
    /**
     * @var string
     */
    private $controller;

    /**
     * @var string
     */
    private $action;

    /**
     * @var int
     */
    private $page;

    /**
     * @var string
     */
    private $requiredController;

    /**
     * @var array
     */
    private $requirements;

    /**
     * @var array
     */
    private $values;

    /**
     * @var string
     */
    private $feature;

    private function __construct()
    {
    }

    /**
     * @param array $route
     * @return RouteParameter
     */
    public static function fromArray(array $route)
    {
        $self = new self();
        $self->controller = isset($route['controller']) ? $route['controller'] : NULL;
        $self->action = isset($route['action']) ? $route['action'] : NULL;
        $self->page = isset($route['page']) ? $route['page'] : NULL;
        $self->requirements = isset($route['requirements']) ? $route['requirements'] : NULL;
        $self->requiredController = isset($route['requiredController']) ? $route['requiredController'] : NULL;
        $self->feature = isset($route['feature']) ? $route['feature'] : NULL;
        $self->values = $route;
        return $self;
    }

    /**
     * @return string
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @return string
     */
    public function getRequiredController()
    {
        return $this->requiredController;
    }

    /**
     * @return array|NULL
     */
    public function getRequirements()
    {
        return $this->requirements;
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function getValue($name)
    {
        return isset($this->values[$name]) ? $this->values[$name] : NULL;
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function getRequirement($name)
    {
        return isset($this->requirements[$name]) ? $this->requirements[$name] : NULL;
    }

    /**
     * @return string
     */
    public function getFeature()
    {
        return $this->feature;
    }
}