<?php

namespace RouterModule\Generators;

interface ILegacyUrlGenerator
{

    /**
     * @param string|int|null $code
     * @param array|null $params
     * @return mixed
     */
    public function secureLink($code = NULL, $params = NULL);

    /**
     * @param string|int|null $code
     * @param array|null $params
     * @return mixed
     */
    public function absoluteFrontLink($code = NULL, $params = NULL);

    /**
     * @param string|int|null $code
     * @param array|null $params
     * @return mixed
     */
    public function link($code = NULL, $params = NULL);

    /**
     * @param string|int|null $code
     * @param array|null $params
     * @return mixed
     */
    public function adminLink($code = NULL, $params = NULL);

    /**
     * @param string|int|null $code
     * @param array|null $params
     * @return mixed
     */
    public function absoluteAdminLink($code = NULL, $params = NULL);
}