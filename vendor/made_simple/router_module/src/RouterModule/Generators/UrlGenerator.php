<?php

namespace RouterModule\Generators;

use FRouter;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Router;

class UrlGenerator implements IUrlGenerator
{
    /**
     * @var Router
     */
    private $symfonyGenerator;

    /**
     * @var FRouter
     */
    private $frouter;

    /**
     * @param UrlGeneratorInterface $symfonyGenerator
     * @param ILegacyUrlGenerator $frouter
     */
    public function __construct(UrlGeneratorInterface $symfonyGenerator, ILegacyUrlGenerator $frouter)
    {
        $this->symfonyGenerator = $symfonyGenerator;
        $this->frouter = $frouter;
    }

    /**
     * @param string $routeName
     * @param array $parameters
     * @param int $options
     * @return string
     */
    public function url($routeName, array $parameters = [], $options = self::RELATIVE)
    {
        if ($options & self::FORMAT_WEBALIZE) {
            $parameters = array_map(['Nette\Utils\Strings', 'webalize'], $parameters);
        }
        if ($options & self::OLD_LINK) {
            if ($options & self::SECURE) {
                return $this->frouter->secureLink($routeName, $parameters);
            } elseif ($options & self::ABSOLUTE) {
                return $this->frouter->absoluteFrontLink($routeName, $parameters);
            } else {
                return $this->frouter->link($routeName, $parameters);
            }
        } else {
            if ($options & self::SECURE || $options & self::ABSOLUTE) {
                $url = $this->symfonyGenerator->generate($routeName, $parameters, Router::ABSOLUTE_URL);
                if ($options & self::SECURE) {
                    if (substr($url, 0, 6) !== 'https:') {
                        return 'https:' . mb_substr($url, 5);
                    }
                    return $url;
                } else {
                    return $url;
                }
            }
            return $this->symfonyGenerator->generate($routeName, $parameters, Router::ABSOLUTE_PATH);
        }
    }
}
