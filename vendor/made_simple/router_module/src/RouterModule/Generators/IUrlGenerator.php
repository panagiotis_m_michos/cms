<?php

namespace RouterModule\Generators;

interface IUrlGenerator
{
    const RELATIVE = 1;
    const SECURE = 2;
    const ABSOLUTE = 4;
    const OLD_LINK = 8;
    const FORMAT_WEBALIZE = 16;

    /**
     * @param string $routeName
     * @param array $parameters
     * @param int $options
     * @return string
     */
    public function url($routeName, array $parameters = [], $options = self::RELATIVE);
}