<?php

namespace RouterModule;

use BadMethodCallException;
use FApplication;
use RouterModule\Exceptions\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Request;

class Dispatcher
{
    /**
     * @var Container
     */
    private $containerBuilder;
    /**
     * @var ObjectMapper
     */
    private $objectMapper;

    /**
     * @param Container $containerBuilder
     * @param ObjectMapper $objectMapper
     */
    public function __construct(Container $containerBuilder, ObjectMapper $objectMapper)
    {
        $this->containerBuilder = $containerBuilder;
        $this->objectMapper = $objectMapper;
    }

    /**
     * @param RouteParameter $parameter
     * @param Request $request
     * @return mixed
     * @throws BadMethodCallException
     * @throws InvalidArgumentException
     */
    public function dispatch(RouteParameter $parameter, Request $request)
    {
        $this->initializeLegacyDependencies($parameter);
        $controller = $this->containerBuilder->get($parameter->getController(), Container::NULL_ON_INVALID_REFERENCE);
        if (!method_exists($controller, $parameter->getAction())) {
            throw new BadMethodCallException(
                sprintf('Method %s or controller %s does not exist', $parameter->getAction(), $parameter->getController())
            );
        }
        $methodsArguments = $this->objectMapper->convertArguments($controller, $parameter, $request);
        return call_user_func_array(array($controller, $parameter->getAction()), $methodsArguments);
    }

    /**
     * @param RouteParameter $parameter
     */
    public function initializeLegacyDependencies(RouteParameter $parameter)
    {
        //legacy code support
        if ($requiredControllerName = $parameter->getRequiredController()) {
            FApplication::$nodeId = (int) ($parameter->getPage() ? $parameter->getPage() : FApplication::$config['home_node']);
            FApplication::$lang = FApplication::$config['default_lng'];
            $requiredController = new $requiredControllerName;
            FApplication::$controler = $requiredController;
            $requiredController->startup();
            $requiredController->beforePrepare();
            if (method_exists($requiredController, 'beforeHandle')) {
                $requiredController->beforeHandle();
            }
            $requiredController->beforeRender();
        }
    }
}