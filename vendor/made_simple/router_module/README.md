# Route module  #

Router module helps to implement symfony router, request parameter mapping and controller creation

### How to use it ###

* create a contoller
* add contoller to dependency injection container
* create route specifications in (routes.yml)
* visit the url specified in "path" key

### routes.yml ###

All syntax remains the same as in [symfony router](http://symfony.com/doc/current/components/routing/introduction.html), except "defaults" key.

```
route_tester2: #name of the route
  path: /route-tester/{type}/ #path
  defaults:
    controller: controllers.route_tester #service id for di container
    action: test2 #method name
    page: 3435 #page to load for required controller
    requiredController: DefaultControler #controller which loads dependencies such as for template or check if user is logged in
    feature: report_upgrade #load only if feature report_upgrade is enabled
  requirements: #requirements for arguments
    type: \d+ #argument can only be digit
```

```
testing1customer:
  path: /testing1customer/{customer}/{date}/ #dynamic path
  defaults:
    controller: controllers.testing1
    action: testTemplateCustomer
    requirements: #custom requirements not implemented by symfony
      customer: #argument name as in controller and path
        converter: doctrine #converter to use (other types does not require specifying it only doctrine entities do)
      date:
        dateFormat: d-m-Y #format to use for Utils\Date object (type specified in controller method)
```
You can import routes from different files. This allows to reduce amount of routes in routes.yml by separating files in different components as well as inheriting route parameters.

```
loggedin:
  resource: loggedin.yml #file to import
  defaults:
    requiredController: LoggedInController #inherited options for all included routes from loggedin.yml
```

Url generation:

```
$url = $router->generate('testing1customer', array(
    'customer' => 1,
    'date' => '23-23-3021',
    'additional' => 'anything'
));
```

Url generation in smarty:

```
{url route="testing1customer" customer=1 date='23-23-3021' additional="anything"}
```

Url generation in latte:

```
{url 'limited_company', ['companyNumber => 'CompanyNumber, 'companyName' => 'Company Name']}

For more info refer to src/RoutingModule/Tests/
