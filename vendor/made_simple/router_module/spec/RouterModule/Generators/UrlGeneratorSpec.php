<?php

namespace spec\RouterModule\Generators;

use FRouter;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use RouterModule\Generators\IUrlGenerator;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class UrlGeneratorSpec extends ObjectBehavior
{
    /**
     * @var FRouter
     */
    private $frouter;

    /**
     * @var UrlGeneratorInterface
     */
    private $generator;

    function let(UrlGeneratorInterface $generator, FRouter $frouter)
    {
        $this->beConstructedWith($generator, $frouter);
        $this->generator = $generator;
        $this->frouter = $frouter;
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('RouterModule\Generators\UrlGenerator');
        $this->shouldHaveType('RouterModule\Generators\IUrlGenerator');
    }

    function it_should_generate_relavite_old_link()
    {
        $this->frouter->link('test', ['param' => 1])->shouldBeCalled();
        $this->url('test', ['param' => 1], IUrlGenerator::OLD_LINK);
    }

    function it_should_generate_absolute_old_link()
    {
        $this->frouter->absoluteFrontLink('test', ['param' => 1])->shouldBeCalled();
        $this->url('test', ['param' => 1], IUrlGenerator::OLD_LINK | IUrlGenerator::ABSOLUTE);
    }

    function it_should_generate_secure_old_link()
    {
        $this->frouter->secureLink('test', ['param' => 1])->shouldBeCalled();
        $this->url('test', ['param' => 1], IUrlGenerator::SECURE | IUrlGenerator::OLD_LINK);
    }

    function it_should_generate_secure_link()
    {
        $this->generator->generate('test', ['param' => 1], UrlGeneratorInterface::ABSOLUTE_URL)->willReturn('http://domain/test/1');
        $this->url('test', ['param' => 1], IUrlGenerator::SECURE)->shouldBe('https://domain/test/1');
    }

    function it_should_generate_absolute_link()
    {
        $this->generator->generate('test', ['param' => 1], UrlGeneratorInterface::ABSOLUTE_URL)->shouldBeCalled();
        $this->url('test', ['param' => 1], IUrlGenerator::ABSOLUTE);
    }

    function it_should_generate_relative_link()
    {
        $this->generator->generate('test', ['param' => 1], UrlGeneratorInterface::ABSOLUTE_PATH)->shouldBeCalled();
        $this->url('test', ['param' => 1], IUrlGenerator::RELATIVE);
    }
}
