<?php

namespace spec\RouterModule;

use FTemplate;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

if (!class_exists('FTemplate')) {
    class FTemplate
    {
        public function getHtml($path)
        {
            return file_get_contents($path);
        }
    }
}

class TemplateRendererSpec extends ObjectBehavior
{
    /**
     * @var vfsStreamDirectory
     */
    private $root;

    function let(FTemplate $template)
    {
        $this->beConstructedWith($template, vfsStream::url('tmp'), 'xml');
        $this->root = vfsStream::setup('tmp');
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('RouterModule\TemplateRenderer');
    }

    function it_should_throw_exception_if_template_does_not_exist(FTemplate $template)
    {
        $path = 'test/me';
        $this->shouldThrow('RouterModule\Exceptions\TemplateException')->duringGetHtml($path);
    }

    function it_should_return_html(FTemplate $template)
    {
        vfsStream::create(array('test' => array('me.xml' => 'content')), $this->root);
        $path = 'test/me';
        $template->getHtml(vfsStream::url('tmp/' . $path . '.xml'))->willReturn('content');
        $this->getHtml($path)->shouldBe('content');
    }
}
