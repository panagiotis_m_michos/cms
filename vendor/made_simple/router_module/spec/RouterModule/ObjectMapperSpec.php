<?php

namespace spec\RouterModule;

use Doctrine\Common\Annotations\AnnotationReader;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use RouterModule\ConverterManager;
use RouterModule\Converters\DateTimeConverter;
use RouterModule\RouteParameter;
use Symfony\Component\HttpFoundation\Request;
use Utils\Date;

class ObjectMapperSpec extends ObjectBehavior
{
    function let(ConverterManager $converterManager)
    {
        $this->beConstructedWith($converterManager);
        $converterManager->convert('2012-12-12', 'Utils\Date', array('format' => 'Y-m-d'))->willReturn(new Date('2012-12-12'));
        $converterManager->convert("1", 'int', NULL)->willReturn(1);
        $converterManager->convert("empty", 'string', NULL)->willReturn("empty");
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('RouterModule\ObjectMapper');
    }

    function it_should_convert_method_arguments(Request $request)
    {
        $request->get('anotherTest')->willReturn('empty');
        $parameters = RouteParameter::fromArray(array(
            'action' => 'specTest',
            'date' => '2012-12-12',
            'test' => "1",
            'requirements' => array('date' => array('format' => 'Y-m-d'))
        ));
        $this->convertArguments('RouterModule\Tests\TestingController2', $parameters, $request)
            ->shouldBeLike(array(
                new Date('2012-12-12'),
                1,
                'empty'
            ));
    }
}
