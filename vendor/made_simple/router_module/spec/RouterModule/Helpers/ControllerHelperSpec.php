<?php

namespace spec\RouterModule\Helpers;

use FRouter;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use RouterModule\Generators\IUrlGenerator;
use Session;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;
use UnexpectedValueException;

class ControllerHelperSpec extends ObjectBehavior
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var Session
     */
    private $session;

    function let(IUrlGenerator $generator,
                 FormFactory $formFactory, Request $request)
    {
        $this->session = new Session();
        $this->beConstructedWith($this->session, $generator, $formFactory, $request);
        $this->request = $request;
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('RouterModule\Helpers\ControllerHelper');
    }

    function it_should_return_redirect_response_from_exception()
    {
        $response = $this->getFlexibleResponse(new UnexpectedValueException('Test'), '/a/b/');
        $response->shouldBeAnInstanceOf('Symfony\Component\HttpFoundation\RedirectResponse');
        $response->getTargetUrl()->shouldBe('/a/b/');
        //how would you test this ? code smell ?
        //$this->session->getNamespace(ControllerHelper::SESSION_FLASH)->willReturn($namespace);
    }

    function it_should_return_redirect_response_from_string()
    {
        $response = $this->getFlexibleResponse('Test', '/a/b/');
        $response->shouldBeAnInstanceOf('Symfony\Component\HttpFoundation\RedirectResponse');
        $response->getTargetUrl()->shouldBe('/a/b/');
    }

    function it_should_return_redirect_response_from_array()
    {
        $response = $this->getFlexibleResponse(['data' => ['a', 'b']], '/a/b/');
        $response->shouldBeAnInstanceOf('Symfony\Component\HttpFoundation\RedirectResponse');
        $response->getTargetUrl()->shouldBe('/a/b/');
    }

    function it_should_return_json_response_with_success_and_append_the_message()
    {
        $data = ['data' => ['a', 'b']];
        $expectedResponse = new JsonResponse(array_merge($data, ['success' => TRUE, 'message' => 'Successfully updated!']));
        $this->request->isXmlHttpRequest()->willReturn(TRUE);
        $response = $this->getFlexibleResponse($data, '/a/b/');
        $response->shouldBeAnInstanceOf('Symfony\Component\HttpFoundation\JsonResponse');
        $response->shouldBeLike($expectedResponse);
    }

    function it_should_return_json_response_with_success_and_use_the_message()
    {
        $data = ['message' => 'Test', 'data' => ['a', 'b']];
        $expectedResponse = new JsonResponse(array_merge($data, ['success' => TRUE]));
        $this->request->isXmlHttpRequest()->willReturn(TRUE);
        $response = $this->getFlexibleResponse($data, '/a/b/');
        $response->shouldBeAnInstanceOf('Symfony\Component\HttpFoundation\JsonResponse');
        $response->shouldBeLike($expectedResponse);
    }

    function it_should_return_json_response_with_error()
    {
        $data = ['error' => 'Error message', 'data' => ['a', 'b']];
        $expectedResponse = new JsonResponse(array_merge($data, ['message' => 'Error message']), JsonResponse::HTTP_SERVICE_UNAVAILABLE);
        $this->request->isXmlHttpRequest()->willReturn(TRUE);
        $response = $this->getFlexibleResponse($data, '/a/b/');
        $response->shouldBeAnInstanceOf('Symfony\Component\HttpFoundation\JsonResponse');
        $response->shouldBeLike($expectedResponse);
    }

    function it_should_set_and_get_flash_session()
    {
        $this->setFlashSessionValue('test', 2);
        $this->getFlashSessionValue('test')->shouldBe(2);
    }

    function it_should_remove_key_once_its_retrieved()
    {
        $this->setFlashSessionValue('test', 2);
        $this->getFlashSessionValue('test')->shouldBe(2);
        $this->getFlashSessionValue('test')->shouldBe(NULL);
    }
}
