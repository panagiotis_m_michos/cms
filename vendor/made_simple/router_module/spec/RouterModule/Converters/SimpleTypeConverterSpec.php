<?php

namespace spec\RouterModule\Converters;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class SimpleTypeConverterSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('RouterModule\Converters\SimpleTypeConverter');
    }

    function it_should_convert_simple_types()
    {
        $this->convert(1, 'string')->shouldBe("1");
        $this->convert("2", 'int')->shouldBe(2);
        $this->convert("3.21", 'float')->shouldBe(3.21);
        $this->convert("34", 'array')->shouldBe(array("34"));
        $this->convert("34454", 'bool')->shouldBe(TRUE);
        $this->convert(2323, 'unknow')->shouldBe(2323);
    }
}
