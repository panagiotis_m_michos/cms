<?php

namespace spec\RouterModule\Converters;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityRepository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use stdClass;

class EntityConverterSpec extends ObjectBehavior
{
    function let(ObjectManager $objectManager, EntityRepository $entityRepository)
    {
        $this->beConstructedWith($objectManager);
        $objectManager->getRepository('stdClass')->willReturn($entityRepository);
        $entityRepository->find(1)->willReturn(new stdClass());
        $entityRepository->find(2)->willReturn(NULL);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('RouterModule\Converters\EntityConverter');
    }

    function it_should_return_an_entity()
    {
        $this->convert(1, 'stdClass')->shouldBeLike(new stdClass());
    }

    function it_should_throw_exception_if_object_is_not_found()
    {
        $this->shouldThrow('RouterModule\Exceptions\InvalidArgumentException')->duringConvert(2, 'stdClass');
    }
}
