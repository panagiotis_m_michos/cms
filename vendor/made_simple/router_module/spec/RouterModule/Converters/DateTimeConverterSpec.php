<?php

namespace spec\RouterModule\Converters;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use DateTime;
use Utils\Date;

class DateTimeConverterSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('RouterModule\Converters\DateTimeConverter');
    }

    function it_should_convert_string_to_datetime()
    {
        $dateTime = new DateTime('3023-12-02 12:00:01');
        $this->convert('3023-12-02 12:00:01', 'DateTime')->shouldBeLike($dateTime);
    }

    function it_should_convert_string_to_date()
    {
        $dateTime = new Date('2201-01-01');
        $this->convert('2201-01-01', 'Utils\Date', array('format' => 'Y-m-d'))->shouldBeLike($dateTime);
    }

    function it_should_throw_an_exception_when_wrong_format_is_used()
    {
        $this->shouldThrow('RouterModule\Exceptions\InvalidArgumentException')
            ->duringConvert('2201-01-01', 'Utils\Date', array('format' => 'd-m-Y'));
    }

    function it_should_throw_an_exception_when_wrong_type_is_used()
    {
        $this->shouldThrow('RouterModule\Exceptions\InvalidArgumentException')
            ->duringConvert('2201-01-01', 'stdClass', array('format' => 'Y-m-d'));
    }
}
