<?php

namespace spec\RouterModule;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use RouterModule\ObjectMapper;
use RouterModule\RouteParameter;
use RouterModule\Tests\TestingController2;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Request;
use Utils\Date;

class DispatcherSpec extends ObjectBehavior
{
    function let(Container $containerBuilder, ObjectMapper $objectMapper)
    {
        $this->beConstructedWith($containerBuilder, $objectMapper);
        $containerBuilder->get('test', Container::NULL_ON_INVALID_REFERENCE)->willReturn(new TestingController2());
        $objectMapper->convertArguments(Argument::any(), Argument::any(), Argument::any())->willReturn(array(new Date(), 1, 1));
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('RouterModule\Dispatcher');
    }

    function it_should_dispatch_route_parameters(Request $request)
    {
        $parameters = RouteParameter::fromArray(array(
            'controller' => 'test',
            'action' => 'specTest',
            'date' => '2012-12-12',
            'test' => 1,
            'requirements' => array('date' => array('format' => 'Y-m-d'))
        ));
        $this->dispatch($parameters, $request)->shouldBe('methodValue');
    }

    function it_should_throw_exception_if_method_does_not_exist(Request $request)
    {
        $parameters = RouteParameter::fromArray(array('controller' => 'test', 'action' => 'nonExistingMethod'));
        $this->shouldThrow('BadMethodCallException')->duringDispatch($parameters, $request);
    }
}
