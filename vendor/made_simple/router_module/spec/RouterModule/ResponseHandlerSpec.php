<?php

namespace spec\RouterModule;

use FTemplate;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use RouterModule\RouteParameter;
use RouterModule\TemplateRenderer;
use RouterModule\Tests\TestingController2;
use Symfony\Component\DependencyInjection\Container;

class ResponseHandlerSpec extends ObjectBehavior
{
    function let(Container $containerBuilder, TemplateRenderer $template)
    {
        $this->beConstructedWith($containerBuilder, $template);
        $containerBuilder->get('test')->willReturn(new TestingController2());
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('RouterModule\ResponseHandler');
    }

    function it_should_handle_string_response()
    {
        $routerParams = RouteParameter::fromArray(array('controller' => 'test', 'action' => 'test'));
        $response = 'test_string';
        $this->handle($routerParams, $response)->shouldBeAnInstanceOf('Symfony\Component\HttpFoundation\Response');
    }

    function it_should_handle_array_response()
    {
        $routerParams = RouteParameter::fromArray(array('controller' => 'test', 'action' => 'test'));
        $response = array('test');
        $this->handle($routerParams, $response)->shouldBeAnInstanceOf('Symfony\Component\HttpFoundation\JsonResponse');
    }

    function it_should_return_template_path()
    {
        $routerParams = RouteParameter::fromArray(array('controller' => 'test', 'action' => 'listCustomers'));
        $this->getTemplatePath($routerParams)->shouldBe('RouterModule/Templates/TestingController2/listCustomers');
    }

    function it_should_handle_null_response(TemplateRenderer $template)
    {
        $template->getHtml('RouterModule/Templates/TestingController2/test')->willReturn('template_content')->shouldBeCalled();
        $routerParams = RouteParameter::fromArray(array('controller' => 'test', 'action' => 'test'));
        $response = NULL;
        $this->handle($routerParams, $response)->shouldBeAnInstanceOf('Symfony\Component\HttpFoundation\Response');
    }

}
