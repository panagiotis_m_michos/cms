<?php

namespace spec\RouterModule;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use RouterModule\Converters\IConverter;

class ConverterManagerSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('RouterModule\ConverterManager');
    }

    function it_should_have_conterters(IConverter $converter)
    {
        $this->addConverter(array('test'), $converter);
        $this->getConverter('test')->shouldReturn($converter);
    }

    function it_should_throw_exception_if_converter_does_not_exist()
    {
        $this->shouldThrow('RouterModule\Exceptions\ConverterException')->duringGetConverter('test');
    }
}
