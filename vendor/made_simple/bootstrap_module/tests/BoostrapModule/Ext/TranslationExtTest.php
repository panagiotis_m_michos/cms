<?php

namespace BootstrapModule\Ext;

use FApplication;
use PHPUnit_Framework_TestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;

class Animal { function __toString() { return 'Zebra'; }}
class Number { function __toString() { return '1034343434343'; }}

class TranslationExtTest extends PHPUnit_Framework_TestCase
{
    /**
     * @after
     */
    public function restoreLocale()
    {
        $ext = new TranslationExt();
        //@TODO have same singleton in all projects
        if (FApplication::$container->hasParameter('translation.default_locale')) {
            $ext->load(FApplication::$container);
        }
    }

    public function load($locale)
    {
        $config = [
            'translation.text_domain' => 'test',
            'translation.file_directory' => dirname(dirname(__DIR__)) . '/fixtures/locale',
            'translation.default_locale' => $locale
        ];
        $container = new ContainerBuilder(new ParameterBag($config));
        $ext = new TranslationExt();
        $ext->load($container);
    }

    public function testGb()
    {
        $this->load('en_GB.UTF8');
        $this->assertEquals('Hello',s_('Hello'));
        $this->assertEquals('Long very very long text, with complicated attributes',s_('Complicate attributes'));
        $this->assertEquals('Tree', n_('Tree', 'Trees', 1));
        $this->assertEquals('Trees', n_('Tree', 'Trees', 2));
        $this->assertEquals('Enabled', c_('subscription', 'ACTIVE'));
        $this->assertEquals('Current', c_('token', 'ACTIVE'));
        $this->assertEquals('Enabled for 1 customer with 10 services', sprintf(cn_('subscription', 'Enabled for %d customer with %d services', 'Enabled for %d customers with %d services', 1), 1, 10));
        $this->assertEquals('Enabled for 3 customers with 10 services', sprintf(cn_('subscription', 'Enabled for %d customer with %d services', 'Enabled for %d customers with %d services', 3), 3, 10));
        $this->assertEquals('Saved for 1 customer with 3 services', sprintf(cn_('token', 'Enabled for %d customer with %d services', 'Enabled for %d customers with %d services', 1), 1, 3));
        $this->assertEquals('Saved for 3 customers with 3 services', sprintf(cn_('token', 'Enabled for %d customer with %d services', 'Enabled for %d customers with %d services', 3), 3, 3));
    }

    public function testDe()
    {
        $this->load('de_DE.UTF8');
        $this->assertEquals('Hallo',s_('Hello'));
        $this->assertEquals('Lange sehr sehr langen Text, mit komplizierten Attribute',s_('Complicate attributes'));
        $this->assertEquals('Baum', n_('Tree', 'Trees', 1));
        $this->assertEquals('Bäume', n_('Tree', 'Trees', 2));
        $this->assertEquals('Freigegeben', c_('subscription', 'ACTIVE'));
        $this->assertEquals('Jetzig', c_('token', 'ACTIVE'));
        $this->assertEquals('Für 1 Kunden mit 10 Dienste aktiviert', sprintf(cn_('subscription', 'Enabled for %d customer with %d services', 'Enabled for %d customers with %d services', 1), 1, 10));
        $this->assertEquals('Für 3 Kundschaft mit 10 Dienste aktiviert', sprintf(cn_('subscription', 'Enabled for %d customer with %d services', 'Enabled for %d customers with %d services', 3), 3, 10));
        $this->assertEquals('Gespeichert für 1 Kunden mit 3 Dienste', sprintf(cn_('token', 'Enabled for %d customer with %d services', 'Enabled for %d customers with %d services', 1), 1, 3));
        $this->assertEquals('Gespeichert für 3 Kundschaft mit 3 Dienste', sprintf(cn_('token', 'Enabled for %d customer with %d services', 'Enabled for %d customers with %d services', 3), 3, 3));

    }

    public function testNonExisting()
    {
        $this->load('C');
        $this->assertEquals('Hello',s_('Hello'));
        $this->assertEquals('Complicate attributes',s_('Complicate attributes'));
        $this->assertEquals('Tree', n_('Tree', 'Trees', 1));
        $this->assertEquals('Trees', n_('Tree', 'Trees', 2));
        $this->assertEquals('ACTIVE', c_('subscription', 'ACTIVE'));
        $this->assertEquals('ACTIVE', c_('token', 'ACTIVE'));
        $this->assertEquals('Enabled for 1 customer with 10 services', sprintf(cn_('subscription', 'Enabled for %d customer with %d services', 'Enabled for %d customers with %d services', 1), 1, 10));
        $this->assertEquals('Enabled for 3 customers with 10 services', sprintf(cn_('subscription', 'Enabled for %d customer with %d services', 'Enabled for %d customers with %d services', 3), 3, 10));
        $this->assertEquals('Enabled for 1 customer with 3 services', sprintf(cn_('token', 'Enabled for %d customer with %d services', 'Enabled for %d customers with %d services', 1), 1, 3));
        $this->assertEquals('Enabled for 3 customers with 3 services', sprintf(cn_('token', 'Enabled for %d customer with %d services', 'Enabled for %d customers with %d services', 3), 3, 3));

    }

    public function testPlaceholders()
    {
        $this->load('non_existing');
        $this->assertEquals('Hello Jonas',s_('Hello %s', 'Jonas'));
        $this->assertEquals('Long very very long Zebra, with complicated number sequence 1034343434343',s_('Long very very long %s, with complicated number sequence %s', new Animal(), new Number()));
        $this->assertEquals('One Lamp', n_('%s Lamp', 'Lamps', 1, 'One'));
        // weird error
        //$this->assertEquals('Trees', n_('Tree', '%d Trees', 5));
        //$this->assertEquals('5 Trees', n_('Tree', '%d Trees', 5, 5));
        $this->assertEquals('%d Lamps', n_('Lamp', '%d Lamps', 5));
        $this->assertEquals('5 Lamps', n_('Lamp', '%d Lamps', 5, 5));
        $this->assertEquals('ACTIVE for me', c_('subscription', 'ACTIVE for %s', 'me'));
        $this->assertEquals('ACTIVE for 10', c_('token', 'ACTIVE for %d', 10));
        $this->assertEquals('Enabled for 1 customer with 10 services', cn_('subscription', 'Enabled for %d customer with %d services', 'Enabled for %d customers with %d services', 1, 1, 10));
        $this->assertEquals('Enabled for 4 customers with 10 services', cn_('subscription', 'Enabled for %d customer with %d services', 'Enabled for %d customers with %d services', 3, 4, 10));

    }

}