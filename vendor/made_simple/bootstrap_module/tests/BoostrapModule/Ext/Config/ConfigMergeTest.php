<?php

namespace BootstrapModule\Ext\Config;

use PHPUnit_Framework_TestCase;

class ConfigMergeTest extends PHPUnit_Framework_TestCase
{
    public function test_config_merge_with_numeric_keys_intact_function()
    {
        $main = [
            'routes' => ['a.yml', 'b.yml'],
            'just text',
            'services' => ['main_di.xml', 'other' => 'main_di.xml'],
            'deep' => ['db' => ['username' => 'real']],
            'just_main_key' => ['key']

        ];
        $included = [
            'routes' => ['c.yml', 'd.yml'],
            'another text',
            'services' => ['included.xml', 'other' => 'other_di.xml'],
            'deep' => ['db' => ['username' => 'temp']],
            'just_included_key' => ['key']
        ];

        $expected = [
            'routes' => ['c.yml', 'd.yml', 'a.yml', 'b.yml'],
            'another text',
            'just text',
            'services' => ['included.xml', 'main_di.xml', 'other' => 'main_di.xml'],
            'deep' => ['db' => ['username' => 'real']],
            'just_included_key' => ['key'],
            'just_main_key' => ['key']
        ];

        $result = array_merge_replace_recursive($included, $main);
        $this->assertEquals($expected, $result);
    }

    public function test_config_bug_route_merging()
    {
        $main = [
            'routes' => [['resource' => 'b.yml', 'defaults' => ['requiredController' => 'testController']]]
        ];
        $included = [
            'routes' => ['c.yml']
        ];
        $expected = [
            'routes' => ['c.yml', ['resource' => 'b.yml', 'defaults' => ['requiredController' => 'testController']]]
        ];
        $result = array_merge_replace_recursive($included, $main);
        $this->assertEquals($expected, $result);
    }
}
