<?php

namespace BootstrapModule\Config;

use Utils\Directory;

class UiConfig implements IUiConfig
{

    /**
     * @var Directory
     */
    private $yamlConfigDir;

    /**
     * @var Directory
     */
    private $mustacheTemplateDir;

    /**
     * @var Directory
     */
    private $mustacheCacheDir;

    /**
     * @var string
     */
    private $uiUrl;

    /**
     * @param Directory $yamlConfigDir
     * @param Directory $mustacheTemplateDir
     * @param Directory $mustacheCacheDir
     * @param string $uiUrl
     */
    public function __construct(Directory $yamlConfigDir, Directory $mustacheTemplateDir, Directory $mustacheCacheDir, $uiUrl)
    {
        $this->yamlConfigDir = $yamlConfigDir;
        $this->mustacheTemplateDir = $mustacheTemplateDir;
        $this->mustacheCacheDir = $mustacheCacheDir;
        $this->uiUrl = $uiUrl;
    }

    /**
     * @return string
     */
    public function getMustacheCacheDir()
    {
        return $this->mustacheCacheDir->getPath();
    }

    /**
     * @return string
     */
    public function getMustacheTemplateDir()
    {
        return $this->mustacheTemplateDir->getPath();
    }

    /**
     * @return string
     */
    public function getYamlConfigDir()
    {
        return $this->yamlConfigDir->getPath();
    }

    /**
     * @return string
     */
    public function getUiUrl()
    {
        return $this->uiUrl;
    }


} 