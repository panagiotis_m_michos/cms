<?php

namespace BootstrapModule\Config;

class DatabaseConfig
{

    /**
     * @var string
     */
    private $host;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $database;

    /**
     * @var string
     */
    private $driver;

    /**
     * @param string $driver
     * @param string $host
     * @param string $username
     * @param string $password
     * @param string $database
     */
    public function __construct($driver, $host, $username, $password, $database)
    {
        $this->host = $host;
        $this->username = $username;
        $this->password = $password;
        $this->database = $database;
        $this->driver = $driver;
    }

    /**
     * @return array
     */
    public function toDoctrine()
    {
        return array(
            'driver' => $this->driver,
            'host' => $this->host,
            'dbname' => $this->database,
            'user' => $this->username,
            'password' => $this->password,
            'charset' => 'utf8'
        );
    }

}