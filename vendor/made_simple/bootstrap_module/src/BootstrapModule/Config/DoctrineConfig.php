<?php

namespace BootstrapModule\Config;

use Utils\Directory;

class DoctrineConfig implements IDoctrineConfig
{
    /**
     * @var string
     */
    private $cacheNamespace;

    /**
     * @var array
     */
    private $entityPaths;

    /**
     * @var string
     */
    private $entityNamespace;

    /**
     * @var Directory
     */
    private $proxyDirectory;

    /**
     * @var DatabaseConfig
     */
    private $databaseConfig;

    /**
     * @param string $cacheNamespace
     * @param array $entityPaths
     * @param string $entityNamespace
     * @param Directory $proxyDirectory
     * @param DatabaseConfig $databaseConfig
     */
    public function __construct($cacheNamespace, array $entityPaths, $entityNamespace, Directory $proxyDirectory, DatabaseConfig $databaseConfig)
    {

        $this->cacheNamespace = $cacheNamespace;
        $this->entityPaths = $entityPaths;
        $this->entityNamespace = $entityNamespace;
        $this->proxyDirectory = $proxyDirectory;
        $this->databaseConfig = $databaseConfig;
    }

    /**
     * @param array $config
     * @param Directory $proxyDirectory
     * @param array $database
     * @return DoctrineConfig
     */
    public static function fromConfig(array $config, Directory $proxyDirectory, array $database)
    {
        return new self($config['cacheNamespace'], $config['entityPaths'], $config['entityNamespace'], $proxyDirectory, $database);
    }

    /**
     * @return string
     */
    public function getCacheNamespace()
    {
        return $this->cacheNamespace;
    }

    /**
     * @return array
     */
    public function getDatabaseConfig()
    {
        return $this->databaseConfig->toDoctrine();
    }

    /**
     * @return string
     */
    public function getEntityNamespace()
    {
        return $this->entityNamespace;
    }

    /**
     * @return array
     */
    public function getEntityPaths()
    {
        return $this->entityPaths;
    }

    /**
     * @return Directory
     */
    public function getEntityProxyDir()
    {
        return $this->proxyDirectory->getPath();
    }
}