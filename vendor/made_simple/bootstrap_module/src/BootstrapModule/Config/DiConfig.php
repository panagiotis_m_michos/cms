<?php

namespace BootstrapModule\Config;

use Utils\Directory;

class DiConfig implements IDiConfig
{

    /**
     * @var array
     */
    private $configPaths = array();

    /**
     * @var array
     */
    private $fileNames;

    /**
     * @var Directory
     */
    private $cacheDir;


    /**
     * @param $paths
     * @param array $filesNames
     * @param Directory $cacheDir
     * @return DiConfig
     */
    public static function fromConfig($paths, array $filesNames, Directory $cacheDir)
    {
        $config = new self();
        $config->configPaths = $paths;
        $config->fileNames = $filesNames;
        $config->cacheDir = $cacheDir;
        return $config;
    }

    /**
     * @return array
     */
    public function getFiles()
    {
        return $this->fileNames;
    }

    /**
     * @return array
     */
    public function getPaths()
    {
        return $this->configPaths;
    }

    /**
     * @return string
     */
    public function getCacheDir()
    {
        return $this->cacheDir->getPath();
    }
}