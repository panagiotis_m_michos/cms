<?php

namespace BootstrapModule\Config;

interface IUiConfig
{
    /**
     * @return string
     */
    public function getYamlConfigDir();

    /**
     * @return string
     */
    public function getMustacheCacheDir();

    /**
     * @return string
     */
    public function getMustacheTemplateDir();

    /**
     * @return string
     */
    public function getUiUrl();
}