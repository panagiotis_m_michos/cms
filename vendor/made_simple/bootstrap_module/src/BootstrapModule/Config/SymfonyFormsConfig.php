<?php

namespace BootstrapModule\Config;

use Utils\Directory;

class SymfonyFormsConfig implements ISymfonyFormsConfig
{
    /**
     * @var Directory
     */
    private $templateDirs;

    /**
     * @var string
     */
    private $csrfSecret;

    /**
     * @var array
     */
    private $defaultThemes;

    /**
     * @var array
     */
    private $validationFiles;

    /**
     * @var Directory
     */
    private $validationDirs;

    /**
     * @param array $templateDirs
     * @param string $csrfSecret
     * @param array $defaultThemes
     * @param array $validationDirs
     * @param array $validationFiles
     */
    public function __construct(array $templateDirs, $csrfSecret, array $defaultThemes, array $validationDirs = array(), array $validationFiles = array())
    {
        $this->templateDirs = $templateDirs;
        $this->csrfSecret = $csrfSecret;
        $this->defaultThemes = $defaultThemes;
        $this->validationFiles = $validationFiles;
        $this->validationDirs = $validationDirs;
    }

    /**
     * @param array $config
     * @return $this
     */
    public static function fromConfig(array $config)
    {
        return new self(
            $config['templates_dirs'],
            $config['csrf_secret'],
            $config['default_themes'],
            isset($config['validation_dirs']) ? $config['validation_dirs'] : array(),
            isset($config['validation_files']) ? $config['validation_files'] : array()
        );
    }

    /**
     * @return Directory
     */
    public function getTemplateDirs()
    {
        return $this->templateDirs;
    }

    /**
     * @return string
     */
    public function getCsrfSecret()
    {
        return $this->csrfSecret;
    }

    /**
     * @return array
     */
    public function getDefaultThemes()
    {
        return $this->defaultThemes;
    }

    /**
     * @param string $extension
     * @return array
     */
    public function getValidationFiles($extension)
    {
        $yamlFiles = array();
        foreach ($this->validationFiles as $filename) {
            $fileExt = substr(strrchr($filename, "."), 1);
            if ($fileExt === $extension) {
                if (file_exists($filename)) {
                    $yamlFiles[] = $filename;
                } else {
                    foreach ($this->validationDirs as $validationDir) {
                        $filePath = $validationDir . DIRECTORY_SEPARATOR . $filename;
                        if (file_exists($filePath)) {
                            $yamlFiles[] = $filePath;
                        }
                    }
                }

            }
        }
        return $yamlFiles;
    }
}
