<?php

namespace BootstrapModule\Config;

interface IDiConfig
{
    /**
     * @return array
     */
    public function getFiles();

    /**
     * @return array
     */
    public function getPaths();

    /**
     * @return string
     */
    public function getCacheDir();
}