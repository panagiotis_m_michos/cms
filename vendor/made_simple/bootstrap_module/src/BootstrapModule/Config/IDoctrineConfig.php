<?php

namespace BootstrapModule\Config;

interface IDoctrineConfig
{
    /**
     * @return string
     */
    public function getCacheNamespace();

    /**
     * @return array
     */
    public function getEntityPaths();

    /**
     * @return string
     */
    public function getEntityNamespace();

    /**
     * @return string
     */
    public function getEntityProxyDir();

    /**
     * @return array
     */
    public function getDatabaseConfig();
}