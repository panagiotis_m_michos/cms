<?php

namespace BootstrapModule\Config;

use Utils\Directory;

interface ISymfonyFormsConfig
{
    /**
     * @return array
     */
    public function getTemplateDirs();

    /**
     * @return string
     */
    public function getCsrfSecret();

    /**
     * @return array
     */
    public function getDefaultThemes();

    /**
     * @param string $extension
     * @return array
     */
    public function getValidationFiles($extension);
}
