<?php

namespace BootstrapModule;

use Symfony\Component\Config\ConfigCache;
use Symfony\Component\Config\Resource\ResourceInterface;

class ConstantCreator
{
    /**
     * @param $className
     * @param array $constants
     * @param ResourceInterface[] $configurationFiles
     * @param string $cacheFile
     * @param bool $debug
     */
    public static function load($className, array $constants, array $configurationFiles, $cacheFile, $debug = FALSE)
    {
        $configCache = new ConfigCache($cacheFile, $debug);
        if (!$configCache->isFresh()) {
            $configCache->write(self::produceCode($className, $constants), $configurationFiles);
        }
        require $cacheFile;
    }

    /**
     * @param string $className
     * @param array $constants
     * @return string
     */
    private static function produceCode($className, array $constants)
    {
        $classTemplate = "<?php

class $className
{
%s
}
";
        $constantString = '';
        foreach ($constants as $key => $value) {
            $name = str_replace('.', '_', $key);
            $constantName = mb_strtoupper($name);
            $constantString .= sprintf('    const %s = \'%s\';', $constantName, $value) . PHP_EOL;
        }
        return sprintf($classTemplate, $constantString);
    }

}