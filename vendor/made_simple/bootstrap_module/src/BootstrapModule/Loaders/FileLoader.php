<?php

namespace BootstrapModule\Loaders;

use Nette\Loaders\RobotLoader;
use Nette\Caching\Storages\FileStorage;
use Utils\Directory;

class FileLoader
{
    /**
     * @param array $dirsToLoad
     * @param Directory $cacheDirectory
     * @param array $dirsToIgnore
     * @return RobotLoader
     */
    public function load(array $dirsToLoad, Directory $cacheDirectory, array $dirsToIgnore)
    {
        $loader = new RobotLoader();
        $loader->setCacheStorage(new FileStorage($cacheDirectory->getPath()));
        $loader->addDirectory($dirsToLoad);
        if ($dirsToIgnore) {
            $dirPath = implode(', ', $dirsToIgnore);
            $loader->ignoreDirs .= ', ' . $dirPath;
        }
        $loader->register();
        return $loader;
    }

}
