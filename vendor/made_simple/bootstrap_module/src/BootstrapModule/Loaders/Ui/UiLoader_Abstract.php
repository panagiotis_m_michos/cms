<?php

namespace BootstrapModule\Loaders\Ui;

use BootstrapModule\Config\IUiConfig;
use Doctrine\Common\Cache\Cache;
use FApplication;
use Mustache_Engine;
use Mustache_Loader_FilesystemLoader;
use UiHelper\MenuHelper_Abstract;
use UiHelper\ViewHelper;
use UiHelper\YamlLoader;
use Symfony\Component\Yaml\Parser;

abstract class UiLoader_Abstract
{

    /**
     * @param IUiConfig $uiConfig
     * @param MenuHelper_Abstract $menu
     * @return ViewHelper
     */
    public function load(IUiConfig $uiConfig, MenuHelper_Abstract $menu, Cache $cache = NULL)
    {
        $templateParameters = array(
            'UI_SERVER_URL' => $uiConfig->getUiUrl()
        );
        $yamlParameters = array(
            'UI_SERVER_URL' => $uiConfig->getUiUrl(),
            'megaMenuItems' => array($menu, 'getMegaMenu'),
            'bottomMenu' => array($menu, 'getBottomMenu'),
            'accountMenu' => array($menu, 'getAccountMenu'),
        );
        $ymlLoader = new YamlLoader($uiConfig->getYamlConfigDir(), new Parser(), $yamlParameters);
        //@TODO remove FApplication
        $ymlLoader->addHelper('link', array(FApplication::$router, 'link'));
        $mustache = new Mustache_Engine(
            array(
                'cache' => $uiConfig->getMustacheCacheDir(),
                'loader' => new Mustache_Loader_FilesystemLoader($uiConfig->getMustacheTemplateDir()),
                'strict_callables' => TRUE,
            )
        );
        $mustacheViewHelper = new ViewHelper($mustache, $ymlLoader, $cache, $templateParameters);
        $this->loadEngine($mustacheViewHelper);
        return $mustacheViewHelper;
    }

    /**
     * @param ViewHelper $mustacheViewHelper
     */
    abstract public function loadEngine(ViewHelper $mustacheViewHelper);
}