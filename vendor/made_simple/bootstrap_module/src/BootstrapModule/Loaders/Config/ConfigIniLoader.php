<?php

namespace BootstrapModule\Loaders\Config;

use ConfigAdapterIni;
use Exception;
use Utils\File;

final class ConfigIniLoader extends ConfigLoader_Abstract
{
    /**
     * @param File $file
     * @return array
     * @throws Exception
     */
    public function loadExt(File $file)
    {
        return ConfigAdapterIni::load($file->getPath());
    }
}
