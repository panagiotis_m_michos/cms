<?php

namespace BootstrapModule\Loaders\Config;

use ConfigAdapterNeon;
use Exception;
use InvalidArgumentException;
use Utils\File;

final class ConfigNeonLoader extends ConfigLoader_Abstract
{
    /**
     * @param File $file
     * @return array
     * @throws Exception
     */
    public function loadExt(File $file)
    {
        return ConfigAdapterNeon::load($file->getPath());
    }
}