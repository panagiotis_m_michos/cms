<?php

namespace BootstrapModule\Loaders\Config;

use InvalidArgumentException;
use Utils\Directory;
use Utils\File;

abstract class ConfigLoader_Abstract
{
    /**
     * @var Directory
     */
    private $directory;

    /**
     * @param Directory $directory
     */
    public function __construct(Directory $directory)
    {
        $this->directory = $directory;
    }

    /**
     * @param File $file
     * @return array
     */
    abstract public function loadExt(File $file);

    /**
     * @param array $filesToLoad
     * @return array
     */
    public function load(array $filesToLoad)
    {
        if (empty($filesToLoad)) {
            throw new InvalidArgumentException('Config files were not provided');
        }
        $config = array();
        foreach ($filesToLoad as $fileToLoad) {
            $config = array_replace_recursive($config, $this->loadExt(File::fromExisting($this->directory, $fileToLoad)));
        }
        return $config;
    }
}
