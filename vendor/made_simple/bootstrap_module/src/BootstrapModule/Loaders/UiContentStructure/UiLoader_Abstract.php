<?php

namespace BootstrapModule\Loaders\UiContentStructure;

use BootstrapModule\Config\IUiConfig;
use Doctrine\Common\Cache\Cache;
use FApplication;
use Mustache_Engine;
use Mustache_Loader_FilesystemLoader;
use UiHelper\MenuHelper_Abstract;
use UiHelper\ViewHelper;
use UiHelper\YamlLoader;
use Symfony\Component\Yaml\Parser;

abstract class UiLoader_Abstract
{

    /**
     * @param IUiConfig $uiConfig
     * @param Cache $cache
     * @return ViewHelper
     */
    public function load(IUiConfig $uiConfig, Cache $cache = NULL)
    {
        $templateParameters = [
            'UI_SERVER_URL' => $uiConfig->getUiUrl()
        ];

        $mustache = new Mustache_Engine(
            [
                'cache' => $uiConfig->getMustacheCacheDir(),
                'loader' => new Mustache_Loader_FilesystemLoader($uiConfig->getMustacheTemplateDir()),
                'strict_callables' => TRUE,
            ]
        );

        return new ViewHelper($mustache, $cache, $templateParameters);
    }

    /**
     * @param ViewHelper $mustacheViewHelper
     */
    abstract public function loadEngine(ViewHelper $mustacheViewHelper);
}

