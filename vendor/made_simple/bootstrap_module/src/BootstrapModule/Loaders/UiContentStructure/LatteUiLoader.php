<?php

namespace BootstrapModule\Loaders\UiContentStructure;

use LatteMacros;
use LatteUiHelper;
use UiHelper\ViewHelper;

class LatteUiLoader extends UiLoader_Abstract
{
    /**
     * @param ViewHelper $mustacheViewHelper
     */
    public function loadEngine(ViewHelper $mustacheViewHelper)
    {
        LatteUiHelper::$uiHelper = $mustacheViewHelper;
        LatteMacros::$defaultMacros['ui'] = '<?php echo LatteUiHelper::$uiHelper->render(%%); ?>';
    }
}
