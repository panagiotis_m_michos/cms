<?php

namespace BootstrapModule\Loaders\UiContentStructure;

use Smarty;
use UiHelper\ViewHelper;

final class SmartyUiLoader extends UiLoader_Abstract
{
    /**
     * @var Smarty
     */
    private $engine;

    /**
     * @param Smarty $engine
     */
    public function __construct(Smarty $engine)
    {
        $this->engine = $engine;
    }

    /**
     * @param ViewHelper $mustacheViewHelper
     */
    public function loadEngine(ViewHelper $mustacheViewHelper)
    {
        $this->engine->registerPlugin('function', 'ui', array($mustacheViewHelper, 'render'));
    }
}
