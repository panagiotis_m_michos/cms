<?php


/**
 * replace array keys with the subsequent array, do not overwrite numeric keys
 *
 * @return array|mixed
 */
function array_merge_replace_recursive()
{
    // Holds all the arrays passed
    $params = func_get_args();

    // First array is used as the base, everything else overwrites on it
    $return = array_shift($params);

    // Merge all arrays on the first array
    foreach ($params as $array) {
        foreach ($array as $key => $value) {
            // Numeric keyed values are added (unless already there)
            if (is_numeric($key) && (!in_array($value, $return))) {
                if (is_array($value) && is_array($return[$key])) {
                    $return[] = array_merge_replace_recursive($return[$key], $value);
                } else {
                    $return[] = $value;
                }

                // String keyed values are replaced
            } else {
                if (isset($return[$key]) && is_array($value) && is_array($return[$key])) {
                    $return[$key] = array_merge_replace_recursive($return[$key], $value);
                } else {
                    $return[$key] = $value;
                }
            }
        }
    }

    return $return;
}

if (!function_exists('pgettext')) {
    /**
     * Context-aware gettext wrapper; use when messages in different contexts
     * won't be distinguished from the English source but need different translations.
     * The context string will appear as msgctxt in the .po files.
     *
     * Not currently exposed in PHP's gettext module; implemented to be compat
     * with gettext.h's macros.
     *
     * @param string $context context identifier, should be some key like "menu|file"
     * @param string $msgid English source text
     * @return string original or translated message
     */
    function pgettext($context, $msg)
    {
        $msgid = $context . "\004" . $msg;
        $out = dcgettext(textdomain(NULL), $msgid, LC_MESSAGES);
        if ($out == $msgid) {
            return $msg;
        } else {
            return $out;
        }
    }
}

if (!function_exists('npgettext')) {
    /**
     * Context-aware ngettext wrapper; use when messages in different contexts
     * won't be distinguished from the English source but need different translations.
     * The context string will appear as msgctxt in the .po files.
     *
     * Not currently exposed in PHP's gettext module; implemented to be compat
     * with gettext.h's macros.
     *
     * @param string $context context identifier, should be some key like "menu|file"
     * @param string $msg singular English source text
     * @param string $plural plural English source text
     * @param int $n number of items to control plural selection
     * @return string original or translated message
     */
    function npgettext($context, $msg, $plural, $n)
    {
        $msgid = $context . "\004" . $msg;
        $out = dcngettext(textdomain(NULL), $msgid, $plural, $n, LC_MESSAGES);
        if ($out == $msgid) {
            return $msg;
        } else {
            return $out;
        }
    }
}

/**
 * translate string
 *
 * @param string $text
 * @param mixed ...$placeholders
 * @return string
 */
function s_($text, ...$placeholders)
{
    $translation = gettext($text);
    if ($placeholders) {
        return sprintf($translation, ...$placeholders);
    }
    return $translation;
}

/**
 * translate pluralize string with context
 *
 * @param string $singularText
 * @param string $pluralText
 * @param int $number
 * @param mixed ...$placeholders
 * @return string
 */
function n_($singularText, $pluralText, $number, ...$placeholders)
{
    $translation = ngettext($singularText, $pluralText, $number);
    if ($placeholders) {
        return sprintf($translation, ...$placeholders);
    }
    return $translation;
}

/**
 * translate string with context
 *
 * @param string $context
 * @param string $text
 * @param ...$placeholders
 * @return string
 */
function c_($context, $text, ...$placeholders)
{
    $translation = pgettext($context, $text);
    if ($placeholders) {
        return sprintf($translation, ...$placeholders);
    }
    return $translation;
}

/**
 * translate pluralize string with context
 *
 * @param string $context
 * @param string $singularText
 * @param string $pluralText
 * @param int $number
 * @param mixed ...$placeholders
 * @return string
 */
function cn_($context, $singularText, $pluralText, $number, ...$placeholders)
{
    $translation = npgettext($context, $singularText, $pluralText, $number);
    if ($placeholders) {
        return sprintf($translation, ...$placeholders);
    }
    return $translation;

}