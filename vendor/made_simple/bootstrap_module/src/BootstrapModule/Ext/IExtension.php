<?php

namespace BootstrapModule\Ext;

use Symfony\Component\DependencyInjection\Container;

interface IExtension
{
    /**
     * @param Container $container
     */
    public function load(Container $container);
}