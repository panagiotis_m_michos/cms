<?php

namespace BootstrapModule\Ext;

use Doctrine\Common\EventManager;
use Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Environment;
use Gedmo\DoctrineExtensions;
use Gedmo\Timestampable\TimestampableListener;
use Symfony\Component\DependencyInjection\Container;

final class DoctrineExt implements IExtension
{
    const ENTITY_MANAGER = 'doctrine.orm.entity_manager';
    const EVENT_MANAGER = 'doctrine.event_manager';
    const ANNOTATION_READER = 'doctrine.annotation_reader';
    const CONFIG = 'bootstrap.config.doctrine';
    const CONNECTION = 'doctrine.connection';
    const CACHE = 'doctrine.cache';

    /**
     * @param Container $container
     */
    public function load(Container $container)
    {
        $entityConfig = $container->get(self::CONFIG);
        $environment = $container->getParameter('environment');
        $cache = $container->get(self::CACHE);
        if ($environment == Environment::PRODUCTION) {
            $autogenerateProxy = FALSE;
        } else {
            $autogenerateProxy = TRUE;
        }
        // ---- Doctrine ----
        $configuration = new Configuration;
        $metadata = $configuration->newDefaultAnnotationDriver(
            $entityConfig->getEntityPaths(),
            FALSE
        );
        $configuration->setMetadataDriverImpl($metadata);
        $configuration->setProxyNamespace($entityConfig->getEntityNamespace());
        $configuration->setProxyDir($entityConfig->getEntityProxyDir());
        $configuration->setAutoGenerateProxyClasses($autogenerateProxy);
        //caching
        $configuration->setQueryCacheImpl($cache);
        $configuration->setResultCacheImpl($cache);
        $configuration->setMetadataCacheImpl($cache);

        // create a driver chain for metadata reading
        $reader = $metadata->getReader();
        $driverChain = new MappingDriverChain();
        DoctrineExtensions::registerAbstractMappingIntoDriverChainORM(
            $driverChain, // our metadata driver chain, to hook into
            $reader // our cached annotation reader
        );

        $timestampableListener = new TimestampableListener;
        $timestampableListener->setAnnotationReader($reader);

        $evm = new EventManager;
        $evm->addEventSubscriber($timestampableListener);

        // MySQL extensions
        $configuration->addCustomStringFunction('DATEDIFF', 'CustomDoctrine\Extensions\Query\MySql\DateDiff');
        $configuration->addCustomStringFunction('DATE_FORMAT', 'CustomDoctrine\Extensions\Query\MySql\DateFormat');
        $configuration->addCustomDatetimeFunction('DATE', 'Luxifer\DQL\Datetime\Date');

        $em = EntityManager::create($entityConfig->getDatabaseConfig(), $configuration, $evm);
        //additional types
        $conn = $em->getConnection();
        $conn->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
        Type::overrideType('date', 'CustomDoctrine\Types\DateType');
        $conn->getDatabasePlatform()->registerDoctrineTypeMapping('Date', 'date');

        $container->set(self::ENTITY_MANAGER, $em);
        $container->set(self::EVENT_MANAGER, $evm);
        $container->set(self::ANNOTATION_READER, $reader);
        $container->set(self::CONNECTION, $conn);
    }
}
