<?php

namespace BootstrapModule\Ext\UiContentStructure;

use BootstrapModule\Ext\IExtension;
use Environment;
use Symfony\Component\DependencyInjection\Container;

final class SmartyExt extends UiExt_Abstract implements IExtension
{
    const SMARTY_LOADER = 'bootstrap.ui_content_structure.smarty';

    /**
     * @param Container $container
     */
    public function load(Container $container)
    {
        $smartyLoader = $container->get(self::SMARTY_LOADER);
        $viewHelper = $smartyLoader->load(
            $container->get(self::CONFIG),
            $container->get(self::CACHE_UI)
        );

        $container->set(UiExt_Abstract::UI_VIEW_HELPER, $viewHelper);
        $smartyLoader->loadEngine($viewHelper);
    }
}
