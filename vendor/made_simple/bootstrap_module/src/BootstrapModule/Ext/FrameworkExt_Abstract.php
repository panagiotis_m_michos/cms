<?php

namespace BootstrapModule\Ext;

abstract class FrameworkExt_Abstract
{
    const DIBI = 'dibi';
    const APPLICATION = 'application';
    const ROUTER = 'application.router';
    const TEMPLATE_ENGINE = 'application.template_engine';
    const LATTE_ENGINE = 'latte.engine';
    const URL_GENERATOR = 'url_generator';

}
