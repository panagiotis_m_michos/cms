<?php

namespace BootstrapModule\Ext\Config;

use InvalidArgumentException;
use Symfony\Component\Config\Resource\FileResource;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\Config\EnvParametersResource;
use Symfony\Component\Yaml\Yaml;
use Utils\File;
use MsgCachedContainer;

class ConfigEnvExt extends ConfigExt_Abstract
{
    /**
     * @param Container $container
     * @throws InvalidArgumentException
     */
    public function load(Container $container)
    {
        //we are using cached container
        if ($container instanceof MsgCachedContainer) {
            return;
        }

        $parameterBag = $container->getParameterBag();

        $config = $this->getEnvParameters($container->getParameter('env_variables_prefix'));
        $this->loadBag($parameterBag, $config);
        $currentConfig = $container->getParameter('config');

        $mergedConfig = array_replace_recursive($currentConfig, $config);
        $this->loadBag($parameterBag, $mergedConfig);
        $this->loadDefaults($parameterBag, $mergedConfig);

        $parameterBag->resolve();
    }

    /**
     * @param string $prefix
     * @return array
     */
    private function getEnvParameters($prefix)
    {
        $parameters = [];
        foreach ($_ENV as $key => $value) {
            if (strpos($key, $prefix) === 0) {
                $parameters[strtolower(str_replace('__', '.', substr($key, strlen($prefix))))] = $value;
            }
        }

        return $this->parseToArray($parameters);
    }

    /**
     * @param array $parameters
     * @return array
     */
    private function parseToArray(array $parameters)
    {
        $config = [];
        foreach ($parameters as $path => $parameter) {
            $currentElement = &$config;
            foreach (explode('.', $path) as $key) {
                $currentElement = &$currentElement[$key];
            }
            $currentElement = $parameter;
        }

        return $config;
    }
}
