<?php

namespace BootstrapModule\Ext\Config;

use InvalidArgumentException;
use MsgCachedContainer;
use Symfony\Component\Config\Resource\FileResource;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Yaml\Yaml;
use Utils\File;

class ConfigYmlExt extends ConfigExt_Abstract
{
    const CACHE_FILE = 'config_yml_cache';

    /**
     * @param Container $container
     * @throws InvalidArgumentException
     */
    public function load(Container $container)
    {
        //we are using cached container
        if ($container instanceof MsgCachedContainer) {
            return;
        }
        $parameterBag = $container->getParameterBag();
        $file = new File($parameterBag->get('config_path'));
        $resources = [];
        $config = $this->import($parameterBag, $file, $resources);
        if ($container instanceof ContainerBuilder) {
            foreach ($resources as $resource) {
                $container->addResource($resource);
            }
        }
        $this->loadBag($parameterBag, $config);
        $this->loadDefaults($parameterBag, $config);
        $parameterBag->resolve();
    }

    /**
     * @param ParameterBagInterface $parameterBag
     * @param File $configFile
     * @return array
     * @throws InvalidArgumentException
     */
    public function import(ParameterBagInterface $parameterBag, File $configFile, array &$resources)
    {
        $content = str_replace('%module_dir%', dirname(dirname($configFile->getPath())), $configFile->getContent());
        $config = Yaml::parse($content);
        $resources[] = new FileResource($configFile->getPath());
        if (isset($config['imports'])) {
            foreach ($config['imports'] as $fileConfig) {
                if (!isset($fileConfig['resource'])) {
                    throw new InvalidArgumentException(sprintf('File %s contains imports, but resource key is missing', $configFile->getPath()));
                }
                $filePath = $parameterBag->resolveValue($fileConfig['resource']);
                $fileObject = new File($filePath);
                if (!$fileObject->isReadable()) {
                    $fileObject = new File(dirname($configFile->getPath()) . DIRECTORY_SEPARATOR . $filePath);
                }
                $importConfig = $this->import($parameterBag, $fileObject, $resources);
                $config = $parameterBag->has('use_new_config_loading')
                    ? array_merge_replace_recursive($importConfig, $config)
                    : array_replace_recursive($importConfig, $config);
                ;
            }
        }
        return $config;
    }
}