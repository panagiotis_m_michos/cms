<?php

namespace BootstrapModule\Ext\Config;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

abstract class ConfigExt_Abstract
{
    /**
     * @param ParameterBagInterface $bagInterface
     * @param array $config
     * @param string | NULL $prependName
     */
    protected function loadBag(ParameterBagInterface $bagInterface, array $config, $prependName = NULL)
    {
        foreach ($config as $key => $value) {
            $name = $prependName ? $prependName . '.' . $key : $key;
            $bagInterface->set($name, $value);
            if ($this->isAssoc($value)) {
                $this->loadBag($bagInterface, $value, $name);
            }
        }
    }

    /**
     * @param ParameterBagInterface $bagInterface
     * @param array $config
     */
    protected function loadDefaults(ParameterBagInterface $bagInterface, array $config)
    {
        $bagInterface->set('config', $config);
    }

    /**
     * @param $array
     * @return bool
     */
    protected function isAssoc($array)
    {
        return is_array($array) && (bool) count(array_filter(array_keys($array), 'is_string'));
    }
}