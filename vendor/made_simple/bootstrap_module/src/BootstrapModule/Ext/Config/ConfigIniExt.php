<?php

namespace BootstrapModule\Ext\Config;

use BootstrapModule\Ext\IExtension;
use BootstrapModule\Loaders\Config\ConfigIniLoader;
use Symfony\Component\DependencyInjection\Container;
use Utils\Directory;

final class ConfigIniExt extends ConfigExt_Abstract implements IExtension
{
    /**
     * @param Container $container
     */
    public function load(Container $container)
    {
        $configIni = new ConfigIniLoader(Directory::fromPath($container->getParameter('config_dir')));
        $config = $configIni->load($container->getParameter('config_files'));
        $parameterBag = $container->getParameterBag();
        $this->loadBag($parameterBag, $config);
        $this->loadDefaults($parameterBag, $config);
        $parameterBag->resolve();
    }
}