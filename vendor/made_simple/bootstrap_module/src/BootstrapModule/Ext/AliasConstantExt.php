<?php

namespace BootstrapModule\Ext;

use BootstrapModule\ConstantCreator;
use FeatureModule\FeatureExt;
use RouterModule\RouterExt;
use Symfony\Component\Config\Resource\FileResource;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Routing\RouteCollection;

class AliasConstantExt implements IExtension
{
    /**
     * @param Container $container
     */
    public function load(Container $container)
    {
        if ($container->hasParameter(FeatureExt::CONFIG_KEY)) {
            $featureKeys = array_keys($container->getParameter(FeatureExt::CONFIG_KEY));
            ConstantCreator::load(
                'Feature',
                array_combine($featureKeys, $featureKeys),
                [new FileResource(dirname(dirname($container->getParameter('config_path'))) . '/features.yml')],
                $container->getParameter('alias.cache_dir') . '/FeatureAlias.php',
                $container->getParameter('debug_mode')
            );
        }

        /** @var RouteCollection $routeCollection */
        $routeCollection = class_exists('RouterModule\RouterExt') && $container->has(RouterExt::ROUTER)
            ? $container->get(RouterExt::ROUTER)->getRouteCollection()
            : NULL;
        if ($routeCollection) {
            $routes = array_keys($routeCollection->all());
            $resources = $routeCollection->getResources();
            ConstantCreator::load(
                'Url',
                array_combine($routes, $routes),
                $resources,
                $container->getParameter('alias.cache_dir') . '/UrlAlias.php',
                $container->getParameter('debug_mode')
            );
        }

        if ($container instanceof ContainerBuilder) {
            $serviceIds = $container->getServiceIds();
            ConstantCreator::load(
                'Di',
                array_combine($serviceIds, $serviceIds),
                $container->getResources(),
                $container->getParameter('alias.cache_dir') . '/ServiceIdAlias.php',
                $container->getParameter('debug_mode')
            );
        } else {
            ConstantCreator::load(
                'Di',
                [],
                [],
                $container->getParameter('alias.cache_dir') . '/ServiceIdAlias.php',
                $container->getParameter('debug_mode')
            );
        }
    }

}