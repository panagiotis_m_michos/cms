<?php

namespace BootstrapModule\Ext;

use Symfony\Component\DependencyInjection\Container;

/**
 * requires config values:
 *
 * translation:
 *   default_locale: en_GB.utf8
 *   file_directory: %root%/storage/locale
 *   text_domain: msg
 */
final class TranslationExt implements IExtension
{
    /**
     * @param Container $container
     */
    public function load(Container $container)
    {
        putenv("LC_ALL={$container->getParameter('translation.default_locale')}");
        setlocale(LC_ALL, $container->getParameter('translation.default_locale'));
        bindtextdomain($container->getParameter('translation.text_domain'), $container->getParameter('translation.file_directory'));
        textdomain($container->getParameter('translation.text_domain'));

        // should we add helpers here ? do we need helpers in the templates ?
        //$engine = $container->has(FrameworkExt_Abstract::LATTE_ENGINE)
        //    ? $container->get(FrameworkExt_Abstract::LATTE_ENGINE)
        //    : NULL;
        //if ($engine) {
        //    $engine->registerHelper('trans', 's_');
            //$engine->registerHelper('trans_plural', 'n_');
            //$engine->registerHelper('trans_context', 'c_');
            //$engine->registerHelper('trans_context_plural', 'cn_');
        //}
    }
}