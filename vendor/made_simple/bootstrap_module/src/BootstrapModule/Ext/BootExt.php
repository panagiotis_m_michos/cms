<?php

namespace BootstrapModule\Ext;

use Symfony\Component\DependencyInjection\Container;
use Tracy\Debugger;



class BootExt implements IExtension
{
    public function load(Container $container)
    {
        if ($container->getParameter('show_errors')) {
            ini_set('display_errors', 'On');
            error_reporting(E_ALL);
        } else {
            register_shutdown_function(
                function () {
                    //a little hack to handle out of memory exceptions
                    $currentMemoryLimit = ini_get('memory_limit');
                    if ($currentMemoryLimit != -1) {
                        $memoryLimitInBytes = BootExt::returnBytes($currentMemoryLimit);
                        //increase by 1mb
                        $increasedLimit = ($memoryLimitInBytes + 1048576) / 1048576;
                        ini_set('memory_limit', $increasedLimit . 'M');
                    }

                    if (class_exists('Tracy\Debugger')) {
                        if (!Debugger::isEnabled()) {
                            Debugger::enable(Debugger::PRODUCTION, DOCUMENT_ROOT . '/logs/php', 'weberror@madesimplegroup.com');
                            Debugger::_shutdownHandler();
                        }
                    }
                }
            );
        }
    }

    /**
     * @param string $val
     * @return int
     */
    public static function returnBytes($val)
    {
        if (empty($val))
            return 0;

        $val = trim($val);

        preg_match('#([0-9]+)[\s]*([a-z]+)#i', $val, $matches);

        $last = '';
        if (isset($matches[2])) {
            $last = $matches[2];
        }

        if (isset($matches[1])) {
            $val = (int) $matches[1];
        }

        switch (strtolower($last)) {
            case 'g':
            case 'gb':
                $val *= 1024;
            case 'm':
            case 'mb':
                $val *= 1024;
            case 'k':
            case 'kb':
                $val *= 1024;
        }

        return (int) $val;
    }

}