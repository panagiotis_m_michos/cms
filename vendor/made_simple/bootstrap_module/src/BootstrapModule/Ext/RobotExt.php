<?php

namespace BootstrapModule\Ext;

use BootstrapModule\Loaders\FileLoader;
use Symfony\Component\DependencyInjection\Container;
use Utils\Directory;

final class RobotExt implements IExtension
{
    const ROBOT_LOADER = 'autoloader.robot';

    /**
     * @param Container $container
     */
    public function load(Container $container)
    {
        $fileLoader = new FileLoader();
        $robotLoader = $fileLoader->load(
            $container->getParameter('dirs_to_load'),
            Directory::createWritable($container->getParameter('cache_dir')),
            $container->getParameter('dirs_to_ignore')
        );
        $container->set(self::ROBOT_LOADER, $robotLoader);
    }
}