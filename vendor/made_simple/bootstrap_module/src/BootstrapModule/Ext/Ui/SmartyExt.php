<?php

namespace BootstrapModule\Ext\Ui;

use BootstrapModule\Ext\IExtension;
use Environment;
use Symfony\Component\DependencyInjection\Container;

final class SmartyExt extends UiExt_Abstract implements IExtension
{
    const SMARTY_LOADER = 'bootstrap.ui.smarty';

    /**
     * @param Container $container
     */
    public function load(Container $container)
    {
        $smartyLoader = $container->get(self::SMARTY_LOADER);
        $viewHelper = $smartyLoader->load(
            $container->get(self::CONFIG),
            $container->get(self::MENU),
            $container->get(self::CACHE_UI)
        );
        $container->set(UiExt_Abstract::UI_VIEW_HELPER, $viewHelper);
    }
}