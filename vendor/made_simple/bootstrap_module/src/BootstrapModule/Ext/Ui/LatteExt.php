<?php

namespace BootstrapModule\Ext\Ui;

use BootstrapModule\Ext\IExtension;
use BootstrapModule\Loaders\Ui\LatteUiLoader;
use Symfony\Component\DependencyInjection\Container;

final class LatteExt extends UiExt_Abstract implements IExtension
{
    /**
     * @param Container $container
     */
    public function load(Container $container)
    {
        $latteLoader = new LatteUiLoader();
        $latteLoader->load(
            $container->get(self::CONFIG),
            $container->get(self::MENU),
            $container->get(self::CACHE_UI)
        );
    }
}