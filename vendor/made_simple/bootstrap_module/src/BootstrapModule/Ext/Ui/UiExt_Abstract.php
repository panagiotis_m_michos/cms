<?php

namespace BootstrapModule\Ext\Ui;

abstract class UiExt_Abstract
{
    const CONFIG = 'bootstrap.config.ui';
    const MENU = 'ui.menu';
    const UI_VIEW_HELPER = 'ui_helper.view_helper';
    const CACHE_UI = 'ui.cache';
}