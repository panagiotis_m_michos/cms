<?php

namespace BootstrapModule\Ext\Di;

use BootstrapModule\Ext\IExtension;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use MsgCachedContainer;

final class SymfonyDiExt extends DiExt_Abstract implements IExtension
{
    /**
     * @param Container $container
     */
    public function load(Container $container)
    {
        if ($container instanceof MsgCachedContainer) {
            return;
        }
        $diConfig = $container->get(self::CONFIG);
        $xmlLoader = new XmlFileLoader($container, new FileLocator($diConfig->getPaths()));
        $ymlLoader = new YamlFileLoader($container, new FileLocator($diConfig->getPaths()));
        $allFiles = array_merge(
            $diConfig->getFiles(),
            $container->hasParameter('services') ? $container->getParameter('services') : []
        );
        foreach ($allFiles as $fileName) {
            switch(pathinfo($fileName, PATHINFO_EXTENSION)) {
                case 'xml':
                    $xmlLoader->load($fileName);
                    break;
                case 'yml':
                    $ymlLoader->load($fileName);
                    break;
            }
        }
        $container->set(self::CONTAINER, $container);
    }
}