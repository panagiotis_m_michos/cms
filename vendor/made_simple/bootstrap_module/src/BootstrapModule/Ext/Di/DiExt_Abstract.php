<?php

namespace BootstrapModule\Ext\Di;

abstract class DiExt_Abstract
{
    const CONFIG = 'bootstrap.config.di';
    const CONTAINER = 'di_container';
}