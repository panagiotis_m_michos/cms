<?php

namespace BootstrapModule\Ext\Di;

use BootstrapModule\Ext\IExtension;
use Nette\Configurator;
use Symfony\Component\DependencyInjection\Container;

final class NetteDiExt extends DiExt_Abstract implements IExtension
{
    /**
     * @param Container $container
     */
    public function load(Container $container)
    {
        $diConfig = $container->get(self::CONFIG);
        $configurator = new Configurator();

        $parameterBag = $container->getParameterBag();
        $params = array_merge(['documentRoot' => $container->getParameter('root')], $parameterBag->all());
        $configurator->addParameters($params);

        $configurator->setTempDirectory($diConfig->getCacheDir());
        foreach ($diConfig->getPaths() as $path) {
            foreach ($diConfig->getFiles() as $file) {
                switch(pathinfo($file, PATHINFO_EXTENSION)) {
                    case 'php':
                    case 'neon':
                    case 'ini':
                        $filePath = $path . DIRECTORY_SEPARATOR . $file;
                        if (file_exists($filePath)) {
                            $configurator->addConfig($filePath);
                        }
                        break;
                }
            }
        }
        $container->set(self::CONTAINER, $configurator->createContainer());
    }
}
