<?php

namespace BootstrapModule\Ext\Doctrine;

use BootstrapModule\Ext\DoctrineExt;
use BootstrapModule\Ext\IExtension;
use CustomDoctrine\Extensions\TablePrefix;
use Doctrine\ORM\Events;
use Symfony\Component\DependencyInjection\Container;

final class TablePrefixExt implements IExtension
{
    /**
     * @param Container $container
     */
    public function load(Container $container)
    {
        $evm = $container->get(DoctrineExt::EVENT_MANAGER);
        $evm->addEventListener(Events::loadClassMetadata, new TablePrefix($container->getParameter('doctrine.table_prefix')));
    }
}