<?php

namespace BootstrapModule\Ext;

use FeatureModule\FeatureExt;
use ITemplate;
use LatteMacros;
use Symfony\Component\DependencyInjection\Container;

class LatteMacroExt implements IExtension
{
    /**
     * @param Container $container
     */
    public function load(Container $container)
    {
        $template = $container->has(FrameworkExt_Abstract::LATTE_ENGINE)
            ? $container->get(FrameworkExt_Abstract::LATTE_ENGINE)
            : NULL;
        if (!$template) {
            return;
        }
        $urlGenerator = $container->has(FrameworkExt_Abstract::URL_GENERATOR)
            ? $container->get(FrameworkExt_Abstract::URL_GENERATOR)
            : NULL;
        if ($urlGenerator) {
            $template->urlGenerator = $urlGenerator;
            LatteMacros::$defaultMacros['url'] = '<?php echo %:escape%($urlGenerator->url(%%)); ?>';
        }
        $feature = class_exists('FeatureModule\FeatureExt') && $container->has(FeatureExt::FEATURE)
            ? $container->get(FeatureExt::FEATURE)
            : NULL;
        if ($feature) {
            $template->featureToggle = $feature;
            LatteMacros::$defaultMacros['featureDisabled'] = '<?php if (!$featureToggle->isEnabled(%%)): ?>';
            LatteMacros::$defaultMacros['/featureDisabled'] = '<?php endif ?>';
            LatteMacros::$defaultMacros['feature'] = '<?php if ($featureToggle->isEnabled(%%)): ?>';
            LatteMacros::$defaultMacros['/feature'] = '<?php endif ?>';
        }
    }
}
