<?php

namespace BootstrapModule\Ext\SymfonyForms;

use Symfony\Component\Templating\TemplateNameParserInterface;
use Symfony\Component\Templating\TemplateReference;
use Symfony\Component\Templating\TemplateReferenceInterface;

/**
 * Needed to load the templates used for rendering form items.
 */
class CustomTemplateNameParser implements TemplateNameParserInterface
{
    /**
     * @var string
     */
    private $root;

    /**
     * @param string $root
     */
    public function __construct($root)
    {
        $this->root = $root;
    }

    /**
     * @param string|TemplateReferenceInterface $name
     * @return TemplateReference
     */
    public function parse($name)
    {
        list(, $template) = explode(':', $name);
        $path = $this->root . DIRECTORY_SEPARATOR . $template;
        return new TemplateReference($path, 'php');
    }
}
