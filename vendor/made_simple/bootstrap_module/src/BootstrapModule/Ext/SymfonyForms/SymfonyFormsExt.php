<?php

namespace BootstrapModule\Ext\SymfonyForms;

use BootstrapModule\Config\ISymfonyFormsConfig;
use BootstrapModule\Ext\IExtension;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Symfony\Bridge\Doctrine\Form\DoctrineOrmExtension;
use Symfony\Bridge\Twig\Extension\FormExtension;
use Symfony\Bridge\Twig\Extension\TranslationExtension;
use Symfony\Bridge\Twig\Form\TwigRenderer;
use Symfony\Bridge\Twig\Form\TwigRendererEngine;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Bundle\FrameworkBundle\Templating\Helper\FormHelper;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Form\Extension\Core\CoreExtension;
use Symfony\Component\Form\Extension\Csrf\CsrfExtension;
use Symfony\Component\Form\Extension\Csrf\CsrfProvider\DefaultCsrfProvider;
use Symfony\Component\Form\Extension\HttpFoundation\HttpFoundationExtension;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\Forms;
use Symfony\Component\Translation\MessageSelector;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\Validation;
use Twig_Environment;
use Twig_Loader_Filesystem;

final class SymfonyFormsExt implements IExtension
{
    const FORM_FACTORY = 'symfony.factories.form';
    const TEMPLATING_PHP = 'symfony.templating.php';
    const FORM_HELPER = 'symfony.forms.form_helper';
    const CONFIG = 'bootstrap.config.symfony_forms';
    const FORM_VALIDATOR_FACTORY = 'symfony.bundle.framework_bundle.validator.validator_factory';

    /**
     * @param Container $container
     */
    public function load(Container $container)
    {
        /** @var ISymfonyFormsConfig $entityConfig */
        $entityConfig = $container->get(self::CONFIG);
        // Set up Twig
        $twig = new Twig_Environment(
            new Twig_Loader_Filesystem($entityConfig->getTemplateDirs()),
            [
                'cache' => $container->getParameter('twig.cache_dir'),
                'auto_reload' => TRUE
            ]
        );
        $formEngine = new TwigRendererEngine($entityConfig->getDefaultThemes());
        $formEngine->setEnvironment($twig);

        AnnotationRegistry::registerLoader('class_exists');
        $validatorBuilder = Validation::createValidatorBuilder();
        $validatorBuilder->addYamlMappings($entityConfig->getValidationFiles('yml'));
        $validatorBuilder->addXmlMappings($entityConfig->getValidationFiles('xml'));
        $validatorBuilder->enableAnnotationMapping(new AnnotationReader());

        if ($container->has(self::FORM_VALIDATOR_FACTORY)) {
            if ($container instanceof ContainerBuilder) {
                $validators = [];
                foreach ($container->findTaggedServiceIds(
                    'validator.constraint_validator'
                ) as $id => $attributes) {
                    if (isset($attributes[0]['alias'])) {
                        $validators[$attributes[0]['alias']] = $id;
                    }
                }

                $container->getDefinition(self::FORM_VALIDATOR_FACTORY)->replaceArgument(1, $validators);
            }

            $constraintValidatorFactory = $container->get(self::FORM_VALIDATOR_FACTORY);
            $validatorBuilder->setConstraintValidatorFactory($constraintValidatorFactory);
        }

        $validator = $validatorBuilder->getValidator();
        $csrfProvider = new DefaultCsrfProvider($entityConfig->getCsrfSecret());
        $renderer = new TwigRenderer($formEngine, $csrfProvider);
        $twig->addExtension(new TranslationExtension(new Translator('en', new MessageSelector())));
        $twig->addExtension(new FormExtension($renderer));

        $formFactory = Forms::createFormFactoryBuilder()
            ->addExtension(new CsrfExtension($csrfProvider))
            ->addExtension(new CoreExtension())
            ->addExtension(new ValidatorExtension($validator))
            ->addExtension(new DoctrineOrmExtension(new ManagerRegistry($container)))
            ->addExtension(new HttpFoundationExtension())
            //->addExtension(new TemplatingExtension($templating, $csrfProvider, $entityConfig->getDefaultThemes()))
            ->getFormFactory();

        $formHelper = new FormHelper($renderer, $csrfProvider);
        $container->set(self::FORM_FACTORY, $formFactory);
        //@TODO remove once all projects are updated
        $container->set(self::TEMPLATING_PHP, array('form' => $formHelper));
        $container->set(self::FORM_HELPER, $formHelper);
    }
}
