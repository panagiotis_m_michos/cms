<?php

namespace BootstrapModule\Ext\SymfonyForms;

use BadMethodCallException;
use BootstrapModule\Ext\DoctrineExt;
use Doctrine\Common\Persistence\AbstractManagerRegistry;
use Symfony\Component\DependencyInjection\Container;

class ManagerRegistry extends AbstractManagerRegistry
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * Constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        parent::__construct(NULL, array(), array(DoctrineExt::ENTITY_MANAGER), NULL, DoctrineExt::ENTITY_MANAGER, 'Doctrine\ORM\Proxy\Proxy');
        $this->container = $container;
    }

    /**
     * @param string $name
     * @return object
     */
    protected function getService($name)
    {
        return $this->container->get($name);
    }

    /**
     * @param string $name
     */
    protected function resetService($name)
    {
        $this->container->set($name, NULL);
    }

    /**
     * @param string $alias
     */
    public function getAliasNamespace($alias)
    {
        throw new BadMethodCallException('Namespace aliases not supported.');
    }
}
