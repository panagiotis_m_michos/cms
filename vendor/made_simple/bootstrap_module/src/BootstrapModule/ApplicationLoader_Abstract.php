<?php

namespace BootstrapModule;

use BootstrapModule\Ext\IExtension;
use Symfony\Component\Config\ConfigCache;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Dumper\PhpDumper;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Utils\Directory;
use MsgCachedContainer;

require_once 'functions.php';

abstract class ApplicationLoader_Abstract
{
    const CACHE_FILE = 'container_cache';
    const PRODUCTION = 'production';

    /**
     * @param array $config
     * @return Container
     */
    public function load(array $config)
    {
        //@TODO remove environments. use debug_mode
        if (!isset($config['environment'])) {
            $config['environment'] = self::PRODUCTION;
        }
        $config['debug_mode'] = $config['environment'] !== self::PRODUCTION;
        // @TODO remove once removed from all projects
        $config['isProduction'] = $config['is_production'] = $config['environment'] === self::PRODUCTION;
        $parameterBag = new ParameterBag($config);
        $cacheFilePath = $this->getCacheFile($parameterBag);
        $configCache = new ConfigCache($cacheFilePath, $parameterBag->get('debug_mode'));
        $this->define(Directory::fromPath($parameterBag->get('root')));
        if (!$configCache->isFresh()) {
            $container = new ContainerBuilder($parameterBag);
            $this->loadConfig($container);
        } else {
            require_once $cacheFilePath;
            $container = new MsgCachedContainer();
        }

        foreach ($this->getExtensions($container) as $ext) {
            $ext->load($container);
        }

        if ($container instanceof ContainerBuilder) {
            Directory::createWritable(dirname($cacheFilePath));
            $container->compile();
            $dumper = new PhpDumper($container);
            $configCache->write(
                $dumper->dump(array('class' => 'MsgCachedContainer')),
                $container->getResources()
            );
        }
        return $container;
    }

    /**
     * @TODO remove defines once we do not use it
     * @param Directory $root
     */
    abstract public function define(Directory $root);

    /**
     * @param Container $container
     * @return IExtension[]
     */
    abstract public function getExtensions(Container $container);

    /**
     * @param Container $container
     * @return YamlFileLoader
     */
    protected function loadConfig(Container $container)
    {
        $yamlFileLoader = new YamlFileLoader($container, new FileLocator(__DIR__));
        $yamlFileLoader->load('default.yml');
        return $yamlFileLoader;
    }

    /**
     * @param ParameterBagInterface $parameterBag
     * @return string
     */
    protected function getCacheFile(ParameterBagInterface $parameterBag)
    {
        $configCacheDir = $parameterBag->get('cache_dir') . DIRECTORY_SEPARATOR . 'config';
        $dynamicHash = md5(json_encode($parameterBag->all()));
        $cacheSuffix = '_' . $parameterBag->get('environment') . '_' . $dynamicHash . '.php';
        $cacheFilePath = $configCacheDir . DIRECTORY_SEPARATOR . self::CACHE_FILE . $cacheSuffix;
        return $cacheFilePath;
    }
}
