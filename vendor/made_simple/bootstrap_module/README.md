# How to use #

Implement ApplicationLoader_Abstract loadExtensions() method specifying which extensions should be loaded for project

# Config Loading #

Config can import other config files using:

imports:
    - { resource: otherconfig.yml }
    
To not override numeric keys in imported files
Add 'use_new_config_loading' => TRUE to bootstrap/default.php $config

Special keys in configuration files:

services:
 - %module_dir%/Config/di.xml
 
will load the file in di container

routes:
 - %module_dir%/Config/routes.yml
 
will import the route resource in the main route file

# Todo #

* remove all external dependencies to projects
* use yml to load dependencies where possible
* compile containers
* use 1 caching strategy for doctrine and the rest (moved to bootstrap module repo)
