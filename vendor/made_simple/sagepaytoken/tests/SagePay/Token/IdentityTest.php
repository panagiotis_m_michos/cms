<?php

namespace SagePay\Token;

/**
 * Generated by PHPUnit_SkeletonGenerator 1.2.0 on 2013-02-04 at 13:55:04.
 */
class IdentityTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var Identity
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->object = new Identity('testVendor', 'testVendorCode');
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        
    }

    /**
     * @covers SagePay\Token\Identity::getVpsProtocol
     * @todo   Implement testGetVpsProtocol().
     */
    public function testGetVpsProtocol()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers SagePay\Token\Identity::setVpsProtocol
     * @todo   Implement testSetVpsProtocol().
     */
    public function testSetVpsProtocol()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers SagePay\Token\Identity::getVendor
     * @todo   Implement testGetVendor().
     */
    public function testGetVendor()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers SagePay\Token\Identity::setVendor
     * @todo   Implement testSetVendor().
     */
    public function testSetVendor()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers SagePay\Token\Identity::getVendorTxCode
     * @todo   Implement testGetVendorTxCode().
     */
    public function testGetVendorTxCode()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers SagePay\Token\Identity::setVendorTxCode
     * @todo   Implement testSetVendorTxCode().
     */
    public function testSetVendorTxCode()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers SagePay\Token\Identity::generateVendorTxCode
     * @todo   Implement testGenerateVendorTxCode().
     */
    public function testGenerateVendorTxCode()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers SagePay\Token\Identity::lock
     */
    public function testLock()
    {
        $this->assertFalse($this->object->isLocked());
        $this->object->lock();
        $this->assertTrue($this->object->isLocked());
    }

    /**
     * @covers SagePay\Token\Identity::clear
     * @depends testLock
     */
    public function testClear()
    {
        $this->assertNotEmpty($this->object->getVendorTxCode());
        $this->object->lock();
        $this->object->clear();
        $this->assertFalse($this->object->isLocked());
        $this->assertEmpty($this->object->getVendorTxCode());
    }

}
