<?php

class PaymentHelper
{

    /**
     * Generate a random string
     *
     * @param int $length
     * @param int $mode 1 = Alpha, 2 = Alpha-numeric, 3 = Alpha-numeric with symbols
     * @param boolian $char_set Set true for Upper and Lower case letters
     * @return string
     */
    public static function randomString($length = 16, $mode = 1, $char_set = false)
    {
        $string = '';
        $possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        if ($char_set) {

            $possible .= strtolower($possible);
        }

        switch ($mode) {

            case 3:

                $possible .= '`~!@#$%^&*()_-+=|}]{[":;<,>.?/';

            case 2:

                $possible .= '0123456789';
                break;
        }

        for ($i = 1; $i <= $length; $i++) {
            $char = substr($possible, mt_rand(0, strlen($possible) - 1), 1);
            $string .= $char;
        }

        return $string;
    }

    public static function createSageDetails()
    {

        $det = new \SagePay\Token\Request\Details();
        $det->amount = 12.23;
        $det->currency = 'GBP';
        $det->description = 'description';
        $det->billingSurname = 'Tester';
        $det->billingFirstnames = 'Johny';
        $det->billingAddress1 = '88';
        $det->billingCity = 'London';
        $det->billingPostCode = '412';
        $det->billingCountry = 'GB';
        $det->deliverySurname = 'OtherTester';
        $det->deliveryFirstnames = 'OtherJohny';
        $det->deliveryAddress1 = '13 St. John Street';
        $det->deliveryCity = 'London';
        $det->deliveryPostCode = 'ec1 4r1';
        $det->deliveryCountry = 'GB';
        $det->token = '492900000000649290000000060000';
        return $det;
    }

    public static function createSageRegistration()
    {

        $req = new \SagePay\Token\Request\Registration();
        $req->cardHolder = 'Big Daddy';
        $req->cardNumber = '4929000000006';
        $req->expiryDate = '1218';
        $req->cardType = 'VISA';
        $req->cv2 = '123';
        $req->currency = 'GBP';
        return $req;
    }

    public static function createPaymentResponse($customer, $typeId)
    {
        $response = new PaymentResponse($customer, $typeId);
        return $response;
    }

}

