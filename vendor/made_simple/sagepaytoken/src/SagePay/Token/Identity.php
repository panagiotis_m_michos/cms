<?php

namespace SagePay\Token;

use SagePay\Token\Annotations\SpField;
use Symfony\Component\Validator\Constraints as Assert;

class Identity extends \Object
{

    /**
     * default protocol
     * @var string
     * @Assert\NotBlank(message="VpsProtocol must not be empty")
     * @SpField(name="VPSProtocol")
     */
    private $vpsProtocol = '2.23';

    /**
     * Vendor Login Name
     * @var string
     * @Assert\NotBlank(message="Vendor must not be empty")
     * @SpField(name="Vendor")
     */
    private $vendor;

    /**
     * reference code to the transaction
     * @var string (40 chars)
     * @Assert\NotBlank(message="VendorTxCode must not be empty")
     * @Assert\Length(max=50, maxMessage="|VendorTxCode must not be more than {{ limit }} characters")
     * @SpField(name="VendorTxCode")
     */
    private $vendorTxCode;

    /**
     * identity is locked when the payment is made
     * @var bool
     */
    private $locked = false;

    public function __construct($vendor, $vendorTxCode = null)
    {
        $this->vendor = $vendor;
        $this->vendorTxCode = $vendorTxCode;
    }

    public function getVpsProtocol()
    {
        return $this->vpsProtocol;
    }

    public function setVpsProtocol($vpsProtocol)
    {
        $this->vpsProtocol = $vpsProtocol;
    }

    public function getVendor()
    {
        return $this->vendor;
    }

    public function setVendor($vendor)
    {
        $this->vendor = $vendor;
    }

    public function getVendorTxCode()
    {
        return $this->vendorTxCode;
    }

    public function setVendorTxCode($vendorTxCode)
    {
        $this->vendorTxCode = $vendorTxCode;
    }

    /**
     * generate vendor tx code
     */
    public function generateVendorTxCode()
    {
        $this->vendorTxCode = uniqid('REF', true);
    }

    public function lock()
    {
        $this->locked = true;
    }

    public function isLocked()
    {
        return $this->locked;
    }

    public function clear()
    {
        $this->locked = false;
        $this->vendorTxCode = null;
    }

}