<?php

namespace SagePay\Token;

use SagePay\Token\Exception\AuthenticationRequired;
use SagePay\Token\Exception\ConnectionError;
use SagePay\Token\Exception\DataMismatch;
use SagePay\Token\Exception\FailedResponse;
use SagePay\Token\Exception\Transaction;
use SagePay\Token\Exception\ValidationError;
use SagePay\Token\Request\Authentication as RequestAuthentication;
use SagePay\Token\Request\Details as RequestDetails;
use SagePay\Token\Request\Registration as RequestRegistration;
use SagePay\Token\Request\Remove as RequestRemove;
use SagePay\Token\Request\RequestAbstract;
use SagePay\Token\Response\Details as ResponseDetails;
use SagePay\Token\Response\Authentication as ResponseAuthentication;
use SagePay\Token\Response\ResponseAbstract;
use SagePay\Token\Response\Summary as ResponseSummary;
use SagePay\Token\Response\Registration as ResponseRegistration;

class SagePay
{

    /**
     * @var Communicator
     */
    private $communicator;

    /**
     * @var Storage
     */
    private $storage;

    /**
     * identity must persist between request since token registration and issue can be used separately
     * @var Identity
     */
    protected $identity;

    /**
     * @param Communicator $communicator
     * @param Storage $storage
     * @param Identity $identity
     */
    public function __construct(Communicator $communicator, Storage $storage, Identity $identity)
    {
        $this->communicator = $communicator;
        $this->storage = $storage;
        $this->identity = $identity;
    }

    public function setCommunicator(Communicator $communicator)
    {
        $this->communicator = $communicator;
    }

    public function setStorage(Storage $storage)
    {
        $this->storage = $storage;
    }

    public function setIdentity(Identity $identity)
    {
        $this->identity = $identity;
    }

    /**
     * @param RequestRegistration $requestRegistration
     * @return ResponseRegistration
     * @throws FailedResponse
     * @throws ConnectionError
     * @throws ValidationError
     */
    public function registerToken(RequestRegistration $requestRegistration)
    {
        $response = $this->makeRequest($requestRegistration);
        $this->storage->saveCardHolder($requestRegistration->getCardHolder());
        $this->storage->saveCardNumber($requestRegistration->getCardNumber());
        $this->storage->saveCardType($requestRegistration->getCardType());
        $this->storage->saveCardExpiryDate($requestRegistration->getExpiryDate());
        return $response;
    }

    /**
     * @param RequestRemove $requestRemove
     * @return ResponseAbstract
     * @throws FailedResponse
     * @throws ConnectionError
     * @throws ValidationError
     */
    public function removeToken(RequestRemove $requestRemove)
    {
        return $this->makeRequest($requestRemove);
    }

    /**
     *
     * @param RequestDetails $details
     * @param string $token
     * @param string $backUrl
     * @return ResponseSummary|string (form for request)
     * @throws AuthenticationRequired
     * @throws FailedResponse
     * @throws ConnectionError
     * @throws ValidationError
     * @throws Transaction
     */
    public function makePayment(RequestDetails $details, $token, $backUrl = null)
    {
        //prevent users from resubmiting the same form without finishing the transaction
        if ($this->identity->isLocked()) {
            throw new Transaction("Vendor code `{$this->identity->getVendorTxCode()}` for the transaction was used before");
        }
        $details->setToken($token);
        $this->storage->saveToken($token);
        $this->identity->lock();
        $response = $this->submitRequest($details);
        if ($response->isAuthenticationRequired() && $response->isRequiredToRedirect()) {
            $this->storage->savePaymentAmount($details->getAmount());
            $this->storage->saveResponseDetails($response);
            $this->storage->saveIdentity($this->identity);
            if ($backUrl) {
                return FormBuilder::get3dAuthenticationForm($response->getAcsUrl(), $response->getPaReq(), $response->getMd(), $backUrl);
            } else {
                throw new AuthenticationRequired($response->getStatusDetail());
            }
        } else if ($response->isSuccess()) {
            return $this->finishTransaction($response);
        }

        $errorCode = $this->parseErrorCodeFromStatusDetail($response->getStatusDetail());
        throw new FailedResponse($response->getStatusDetail(), $errorCode);
    }

    /**
     * confirm payment after 3d authorization
     * @param string $md
     * @param string $paReq
     * @return ResponseSummary
     * @throws FailedResponse
     * @throws ConnectionError
     * @throws ValidationError
     */
    public function confirmPayment($md, $paReq)
    {
        $authenticationRequest = new RequestAuthentication();
        $authenticationRequest->setMd($md);
        $authenticationRequest->setPaRes($paReq);
        $response = $this->submitRequest($authenticationRequest);
        if ($response->isSuccess()) {
            return $this->finishTransaction($response);
        }
        $errorCode = $this->parseErrorCodeFromStatusDetail($response->getStatusDetail());
        throw new FailedResponse($response->getStatusDetail(), $errorCode);
    }

    /**
     * @param RequestAbstract $request
     * @return ResponseAbstract
     * @throws ConnectionError
     * @throws ValidationError
     */
    public function submitRequest(RequestAbstract $request)
    {
        //generate vendor code if its not existing
        if ($this->identity->getVendorTxCode() === null) {
            $this->identity->generateVendorTxCode();
        }
        return $this->communicator->submit($request, $this->identity);
    }

    /**
     * match authentication array with details provided
     * match price sent with current price
     * and send authorization request to sagepay
     * @param array $sagePayResponse
     * @param float $amount
     * @return ResponseSummary
     * @throws ConnectionError
     * @throws DataMismatch
     * @throws FailedResponse
     * @throws ValidationError
     */
    public function authorizePayment($sagePayResponse, $amount)
    {
        $savedResponse = $this->storage->getResponseDetails();
        $amountRequested = $this->storage->getPaymentAmount();
        if (!isset($sagePayResponse['MD'])) {
            throw new ValidationError('No authentication data provided!');
        }
        if (!($savedResponse instanceof ResponseDetails) || !$savedResponse->getMd()) {
            throw new ValidationError('No saved authentication information found!');
        }

        if (!($amountRequested > 0)) {
            throw new ValidationError('No previous payment amount sent found!');
        }
        if ($amountRequested != $amount) {
            throw new DataMismatch('Payment amount sent `' . $amountRequested . '` does not match with the amount `' . $amount . '` requested.');
        }
        if ($sagePayResponse['MD'] === $savedResponse->getMd()) {
            if (isset($sagePayResponse['PaRes'])) {
                return $this->confirmPayment($savedResponse->getMd(), $sagePayResponse['PaRes']);
            }
        }
        throw new ValidationError('Bad authentication information found!');
    }

    /**
     * finalize the transaction by clearing everything
     * @param ResponseAbstract $response
     * @return ResponseSummary | null
     */
    public function finishTransaction(ResponseAbstract $response = null)
    {
        $responseSummary = $response ? $this->createSummary($response) : null;
        $this->identity->clear();
        $this->storage->clear();
        return $responseSummary;
    }

    /**
     * @param string $backUrl
     * @return string
     * @throws ValidationError
     */
    public function getAuthenticationForm($backUrl)
    {
        $savedResponse = $this->storage->getResponseDetails();
        if (!($savedResponse instanceof ResponseDetails) || !$savedResponse->getMd()) {
            throw new ValidationError('No saved authentication information found!');
        }
        return FormBuilder::get3dAuthenticationForm($savedResponse->getAcsUrl(), $savedResponse->getPaReq(), $savedResponse->getMd(), $backUrl);
    }

    /**
     * @param RequestAbstract $request
     * @return ResponseAbstract
     * @throws FailedResponse
     * @throws ConnectionError
     * @throws ValidationError
     */
    private function makeRequest(RequestAbstract $request)
    {
        $response = $this->submitRequest($request);
        if ($response->isSuccess()) {
            $this->finishTransaction();
            return $response;
        }
        throw new FailedResponse($response->getStatusDetail());
    }

    /**
     * @return ResponseSummary
     */
    private function createSummary(ResponseAuthentication $response)
    {
        //create payment summary
        $responseSummary = new ResponseSummary();
        $responseSummary->setSecurityKey($response->getSecurityKey());
        $responseSummary->setVpsTxId($response->getVpsTxId());
        $responseSummary->setTxAuthNo($response->getTxAuthNo());
        $responseSummary->setVendorTxCode($this->identity->getVendorTxCode());
        $responseSummary->setToken($this->storage->getToken());
        $responseSummary->setCardHolder($this->storage->getCardHolder());
        $responseSummary->setCardNumber($this->storage->getCardNumber());
        $responseSummary->setCardType($this->storage->getCardType());
        $responseSummary->setCardExpiryDate($this->storage->getCardExpiryDate());
        return $responseSummary;
    }

    /**
     * @param string $statusDetail
     * @return int
     */
    private function parseErrorCodeFromStatusDetail($statusDetail)
    {
        return (int) trim(strstr($statusDetail, ':', TRUE));
    }
}
