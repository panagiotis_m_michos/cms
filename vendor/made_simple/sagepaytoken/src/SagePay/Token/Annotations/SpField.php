<?php

namespace SagePay\Token\Annotations;

/**
* @Annotation
* @Target({"PROPERTY"})
*/
class SpField
{
    /**
     * @var string
     */
    public $name;
}