<?php

namespace SagePay\Token;

class FormBuilder
{

    /**
     * get3DForm
     *
     * gets form for 3D Secure payment
     *
     * @param string $ascURL;
     * @param string $pareq;
     * @param string $md;
     * @param string $yourURL;
     * @return string
     */
    public static function get3dAuthenticationForm($ascUrl, $paReq, $md, $yourUrl)
    {

        $form = '<html><head><title>3D Secure Verification</title></head>
			<body OnLoad="OnLoadEvent();">
			<form name="issuerForm" method="POST" action="' . htmlspecialchars(trim($ascUrl), ENT_QUOTES) . '" >
			<input type="hidden" name="PaReq" value="' . htmlspecialchars(trim($paReq), ENT_QUOTES) . '" />
			<input type="hidden" name="MD" value="' . htmlspecialchars(trim($md), ENT_QUOTES) . '" />
			<input type="hidden" name="TermUrl" value="' . htmlspecialchars(trim($yourUrl), ENT_QUOTES) . '" />
			<noscript>
			This page should forward you to your own card issuer for identification.
			If your browser does not start loading the page, press the button you see.
			<br/>
			After you successfully identify yourself you will be sent back to the site
			where the payment process will continue as if nothing had happened.
			<br/>
			<input type="submit" name="Authenticate your card" />
			</noscript>
			</form>
			<script language="Javascript">
			<!--
			function OnLoadEvent() {
				document.issuerForm.submit(); }
			//-->
			</script>
			</body>
			</html>';
        return $form;
    }

}