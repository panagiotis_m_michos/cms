<?php

namespace SagePay\Token\Validator;

use SagePay\Token\Exception\ValidationError;

interface IValidator
{
    /**
     * @param mixed $object
     * @return bool
     * @throws ValidationError
     */
    public function validate($object);
}