<?php

namespace SagePay\Token\Validator;

use SagePay\Token\Exception\ValidationError;
use Symfony\Component\Validator\ValidatorInterface;

class SymfonyValidator implements IValidator
{
    /**
     * @var ValidatorInterface 
     */
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * validate the object
     * @param mixed $object
     * @return boolean
     * @throws ValidationError
     */
    public function validate($object)
    {
        $violations = $this->validator->validate($object);
        if ($violations->count() === 0) {
            return true;
        }
        foreach ($violations as $violation) {
            throw new ValidationError($violation->getMessage());
        }
    }

}