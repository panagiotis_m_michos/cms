<?php

namespace SagePay\Token;

use SagePay\Token\Identity;
use SagePay\Token\Response\Details as ResponseDetails;

class Storage
{
    //key to save in session

    const RESPONSE_KEY = 'detailsResponse';
    //amount requested for payment
    const REQUEST_AMOUNT = 'requestAmount';
    //identity object
    const IDENTITY = 'identity';
    //token registration
    const TOKEN = 'token';
    const CARDNUMBER = 'cardnumber';
    const CARDHOLDER = 'cardholder';
    const CARD_TYPE = 'cardtype';
    const CARD_EXPIRY_DATE = 'cardExpiryDate';

    /**
     * authentication information will be stored here
     * @var array
     */
    private $storage;

    public function __construct(&$var)
    {
        $this->storage = &$var;
    }

    public function clear()
    {
        $this->storage = array();
    }

    /**
     * get value from key
     * @param string $key
     * @return mixed
     */
    public function get($key)
    {
        return isset($this->storage[$key]) ? unserialize($this->storage[$key]) : false;
    }

    /**
     * use persistent array like $_SESION['transaction']
     * storage is required for 3d authentication to keep track of transaction
     * identity
     * @param array $var
     */
    public function setStorage(&$var)
    {
        $this->storage = &$var;
    }

    /**
     * save value inside storage
     * @param string $key
     * @param mixed $value
     */
    public function save($key, $value)
    {
        $this->storage[$key] = serialize($value);
    }

    public function getPaymentAmount()
    {
        return $this->get(self::REQUEST_AMOUNT);
    }

    public function savePaymentAmount($amount)
    {
        $this->save(self::REQUEST_AMOUNT, $amount);
    }

    /**
     * @return ResponseDetails|false
     */
    public function getResponseDetails()
    {
        return $this->get(self::RESPONSE_KEY);
    }

    public function saveResponseDetails(ResponseDetails $response)
    {
        $this->save(self::RESPONSE_KEY, $response);
    }

    /**
     * @param string $token
     */
    public function saveToken($token)
    {
        $this->save(self::TOKEN, $token);
    }

    /**
     * @return string | bool
     */
    public function getToken()
    {
        return $this->get(self::TOKEN);
    }

    /**
     * @param Identity $identity
     */
    public function saveIdentity(Identity $identity)
    {
        $this->save(self::IDENTITY, $identity);
    }

    /**
     * @param array $config
     * @return Identity
     */
    public function retrieveIdentity($vendor)
    {
        return $this->get(self::IDENTITY) ? $this->get(self::IDENTITY) : new Identity($vendor);
    }

    /**
     * @param string $cardNumber
     */
    public function saveCardNumber($cardNumber)
    {
        $number = str_replace(' ', '', $cardNumber);
        $this->save(self::CARDNUMBER, mb_substr($number, -4, 4));
    }

    /**
     * @return string | bool
     */
    public function getCardNumber()
    {
        return $this->get(self::CARDNUMBER);
    }

    /**
     * @param string $cardType
     */
    public function saveCardType($cardType)
    {
        $this->save(self::CARD_TYPE, $cardType);
    }

    /**
     * @return string | bool
     */
    public function getCardType()
    {
        return $this->get(self::CARD_TYPE);
    }

    /**
     * @param string $cardHolder
     */
    public function saveCardHolder($cardHolder)
    {
        $this->save(self::CARDHOLDER, $cardHolder);
    }

    /**
     * @return string | bool
     */
    public function getCardHolder()
    {
        return $this->get(self::CARDHOLDER);
    }

    /**
     * @param string $expiryDate
     */
    public function saveCardExpiryDate($expiryDate)
    {
        $this->save(self::CARD_EXPIRY_DATE, $expiryDate);
    }

    /**
     * @return string | bool
     */
    public function getCardExpiryDate()
    {
        return $this->get(self::CARD_EXPIRY_DATE);
    }
}
