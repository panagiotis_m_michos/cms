<?php

namespace SagePay\Token;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\Common\Annotations\CachedReader;
use Doctrine\Common\Cache\ArrayCache;
use FApplication;
use SagePay\Token\Mapper\AnnotationMapper;
use SagePay\Token\Storage;
use Symfony\Component\Validator\Validation;

/**
 * @TODO refactor and move this class to the project or refactor and pass external dependencies
 * Class SageFactory
 * @package SagePay\Token
 */
class SageFactory
{

    /**
     * @var reuse annotation reader
     */
    private static $annotationReader;

    public static function register($packageDir)
    {
        AnnotationRegistry::registerAutoloadNamespace("Symfony\Component\Validator\Constraints", $packageDir . 'symfony/validator');
        AnnotationRegistry::registerAutoloadNamespace("SagePay\Token\Annotations", $packageDir . 'made_simple/sagepaytoken/src');
    }

    /**
     *
     * @param Storage $storage
     * @return SagePay
     */
    public static function createGateway(Storage $storage = null)
    {
        $config = FApplication::$config['sage'];
        $storage = $storage ? $storage : self::createStorage();
        $identity = $storage->retrieveIdentity($config['vendor']);
        $transfer = new Transfer();
        $symfonyValidator = self::createValidator();
        $mapper = self::createAnnotationMapper();
        $communicator = new Communicator($transfer, $mapper, $symfonyValidator, $config['system']);
        $sagePayment = new SagePay($communicator, $storage, $identity);
        return $sagePayment;
    }

    /**
     * @return Storage
     */
    public static function createStorage()
    {
        //@TODO remove FApplication from this class
        if (FApplication::$session instanceof \Nette\Http\Session) {
            $namespace = FApplication::$session->getSection('orderconfirmation');
        } else {
            $namespace = FApplication::$session->getNamespace('orderconfirmation');
        }
        if (empty($namespace->transaction))
            $namespace->transaction = array();
        $storage = new Storage($namespace->transaction);
        return $storage;
    }

    /**
     * @return AnnotationMapper
     */
    public static function createAnnotationMapper()
    {
        $annotationName = get_class(new Annotations\SpField());
        $annotationReader = self::getAnnotationReader();
        $mapper = new AnnotationMapper($annotationReader, $annotationName);
        return $mapper;
    }

    /**
     * @return CachedReader|reuse
     */
    public static function getAnnotationReader()
    {
        return self::$annotationReader ? self::$annotationReader : new CachedReader(new AnnotationReader(), new ArrayCache());
    }

    /**
     * @return Validator\SymfonyValidator
     */
    public static function createValidator()
    {
        $annotationReader = self::getAnnotationReader();
        $validator = Validation::createValidatorBuilder()->enableAnnotationMapping($annotationReader)->getValidator();
        return new Validator\SymfonyValidator($validator);
    }

}

/*
// 1. Register token and make a payment (3d authentication form is displayed in the same page if applicable)
$cardDetails = new Request\Registration();
$customerDetails = new Request\Details();

$backUrl = 'http://localhost/3dSuccess';

$sagePay = SageFactory::createGateway();
$responseCardDetails = $sagePay->registerToken($cardDetails);
//save token somewhere
... database code
$responsePayment = $sagePay->makePayment($customerDetails, $responseCardDetails->getToken(), $backUrl);

//after authorization completes in http://localhost/3dSuccess
$sagePay = SageFactory::createGateway();
$sagePay->authorizePayment($_POST);

// 2. Make a payment with previously registered token (make sure you disable 3d and csv checks)
$token = // code to retrieve token
$sagePay = SageFactory::createGateway();
$sagePay->makePayment($customerDetails, $token);

// 3. Register token and make a payment (manually redirecting to 3d authentication)
try {
    $cardDetails = new Request\Registration();

    $customerDetails = new Request\Details();

    $sagePay = SageFactory::createGateway();
    $responseCardDetails = $sagePay->registerToken($cardDetails);
    //no back url provided exception will be thrown
    $responsePayment = $sagePay->makePayment($customerDetails, $responseCardDetails->getToken());
} catch (AuthenticationRequired $e) {
    //redirect a customer
    redirect('http://localhost/3dAuthentication');
}

//in http://localhost/3dAuthentication show this form
$sagePay = SageFactory::createGateway();
$yourUrl = 'http://localhost/3dSuccess';
$sagePay->displayAuthenticationForm($yourUrl);

//once the 3dauthentication completes your are redirected back to http://localhost/3dSuccess
$sagePay = SageFactory::createGateway();
$yourUrl = 'http://localhost/3dSuccess';
$sagePay->authorizePayment($_POST);
 */
