<?php

namespace SagePay\Token\Request;

use SagePay\Token\Response\GeneralResponse;
use SagePay\Token\Annotations\SpField;
use Symfony\Component\Validator\Constraints as Assert;

class Remove extends RequestAbstract
{

    protected static $REQUEST_URL = 'https://live.sagepay.com/gateway/service/removetoken.vsp';
    protected static $TEST_REQUEST_URL = 'https://test.sagepay.com/gateway/service/removetoken.vsp';

    /**
     * ( max 15 )
     * @var string
     * @Assert\NotBlank(message="Transaction type must not be empty")
     * @SpField(name="TxType")
     */
    protected $txType = self::TYPE_REMOVE;

    /**
     * token
     * @var string
     * @SpField(name="Token")
     * @Assert\NotBlank(message="Token must not be empty")
     * @Assert\Length(min=38, max=38, exactMessage="|Token must be {{ limit }} characters")
     */
    protected $token;

    /**
     * @return GeneralResponse
     */
    public function createResponse()
    {
        return new GeneralResponse();
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

}