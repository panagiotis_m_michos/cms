<?php

namespace SagePay\Token\Request;

use SagePay\Token\Response\GeneralResponse;
use SagePay\Token\Annotations\SpField;
use Symfony\Component\Validator\Constraints as Assert;

class Cancel extends RequestAbstract
{
    protected static $REQUEST_URL = 'https://live.sagepay.com/gateway/service/cancel.vsp';
    protected static $TEST_REQUEST_URL = 'https://test.sagepay.com/gateway/service/cancel.vsp';

    /**
     * ( max 15 )
     * @var string
     * @Assert\NotBlank(message="Transaction type must not be empty")
     * @SpField(name="TxType")
     */
    protected $txType = self::TYPE_CANCEL;

    /**
     * unique transaction identifier
     * @var string
     * @Assert\NotBlank(message="MD must not be empty")
     * @SpField(name="MD")
     */
    private $md;

    /**
     * encoded string to identified transaction
     * @var string
     * @Assert\NotBlank(message="PaRes must not be empty")
     * @SpField(name="PAReq")
     */
    private $paReq;

    /**
     * @return GeneralResponse
     */
    public function createResponse()
    {
        return new GeneralResponse();
    }

    /**
     * @return string
     */
    public function getTxType()
    {
        return $this->txType;
    }

    /**
     * @param string $txType
     */
    public function setTxType($txType)
    {
        $this->txType = $txType;
    }

    /**
     * @return string
     */
    public function getMd()
    {
        return $this->md;
    }

    /**
     * @param string $md
     */
    public function setMd($md)
    {
        $this->md = $md;
    }

    /**
     * @return string
     */
    public function getPaReq()
    {
        return $this->paReq;
    }

    /**
     * @param string $paReq
     */
    public function setPaReq($paReq)
    {
        $this->paReq = $paReq;
    }

}