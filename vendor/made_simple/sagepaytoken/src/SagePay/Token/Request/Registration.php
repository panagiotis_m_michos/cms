<?php

namespace SagePay\Token\Request;

use SagePay\Token\Response\Registration as ResponseRegistration;
use SagePay\Token\Annotations\SpField;
use Symfony\Component\Validator\Constraints as Assert;

class Registration extends RequestAbstract
{

    protected static $REQUEST_URL = 'https://live.sagepay.com/gateway/service/directtoken.vsp';
    protected static $TEST_REQUEST_URL = 'https://test.sagepay.com/gateway/service/directtoken.vsp';

    /**
     * ( max 15 )
     * @var string
     * @Assert\NotBlank(message="Transaction type must not be empty")
     * @SpField(name="TxType")
     */
    protected $txType = self::TYPE_SAVE;

    /**
     * card holders name
     * @var string max 50
     * @Assert\NotBlank(message="Card holder must not be empty")
     * @Assert\Length(max=50, maxMessage="|Card Holder must not be more than {{ limit }} characters")
     * @SpField(name="CardHolder")
     */
    private $cardHolder;

    /**
     * @var string max 20
     * @SpField(name="CardNumber")
     * @Assert\NotBlank(message="Card number must not be empty")
     * @Assert\Length(max=20, maxMessage="|Card Number must not be more than {{ limit }} digits")
     */
    private $cardNumber;

    /**
     * @var string ( format mmyyy )
     * @Assert\Length(min=4, max=4, minMessage="|Start date must be in this format 'mmyyy'", maxMessage="|Start date must be in this format 'mmyyy'")
     */
    private $startDate;

    /**
     * @var string ( format mmyyy )
     * @SpField(name="ExpiryDate")
     * @Assert\NotBlank(message="Expiry date must not be empty")
     * @Assert\Length(min=4, max=4, minMessage="|Expiry date must be in this format 'mmyyy'", maxMessage="|Expiry date must be in this format 'mmyyy'")
     */
    private $expiryDate;

    /**
     * The card Issue Number (some Maestro and Solo cards only)
     * @var string
     * @SpField(name="IssueNumber")
     * @Assert\Length(max=2, maxMessage="|Issue Number must not be more than {{ limit }} digits")
     */
    private $issueNumber;

    /**
     * The extra security 3 digits on the signature strip of the card,
     * or the extra 4 digits on the front for American
     * @var string ( max 4 )
     * @SpField(name="CV2")
     * @Assert\NotBlank(message="CV2 number must not be empty")
     * @Assert\Length(min=3, max=4, minMessage="|CV2 number must be at least {{ limit }} characters", maxMessage="|CV2 number must not be more than {{ limit }} characters")
     */
    private $cv2;

    /**
     * VISA, MC, MAESTRO ..
     * @var string
     * @SpField(name="CardType")
     * @Assert\NotBlank(message="Card number must not be empty")
     * @Assert\Length(max=15, maxMessage="|Card type must not be more than {{ limit }} characters, {{ value }} provided")
     */
    private $cardType;

    /**
     * @var string max 3
     * @SpField(name="Currency")
     * @Assert\Length(max=3, maxMessage="|Currency must not be more than {{ limit }} characters, {{ value }} provided")
     */
    private $currency;

    /**
     * @return ResponseRegistration
     */
    public function createResponse()
    {
        return new ResponseRegistration();
    }

    /**
     * @return string
     */
    public function getCardHolder()
    {
        return $this->cardHolder;
    }

    /**
     * @param string $cardHolder
     */
    public function setCardHolder($cardHolder)
    {
        $this->cardHolder = $cardHolder;
    }

    /**
     * @return string
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * @param string $cardNumber
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;
    }

    /**
     * @return string
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param string $startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @return string
     */
    public function getExpiryDate()
    {
        return $this->expiryDate;
    }

    /**
     * @param string $expiryDate
     */
    public function setExpiryDate($expiryDate)
    {
        $this->expiryDate = $expiryDate;
    }

    /**
     * @return string
     */
    public function getIssueNumber()
    {
        return $this->issueNumber;
    }

    /**
     * @param string $issueNumber
     */
    public function setIssueNumber($issueNumber)
    {
        $this->issueNumber = $issueNumber;
    }

    /**
     * @return string
     */
    public function getCv2()
    {
        return $this->cv2;
    }

    /**
     * @param string $cv2
     */
    public function setCv2($cv2)
    {
        $this->cv2 = $cv2;
    }

    /**
     * @return string
     */
    public function getCardType()
    {
        return $this->cardType;
    }

    /**
     * @param string $cardType
     */
    public function setCardType($cardType)
    {
        $this->cardType = $cardType;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

}