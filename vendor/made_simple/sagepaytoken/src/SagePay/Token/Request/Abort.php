<?php

namespace SagePay\Token\Request;

use SagePay\Token\Response\GeneralResponse;
use SagePay\Token\Annotations\SpField;
use Symfony\Component\Validator\Constraints as Assert;

class Abort extends RequestAbstract {

	protected static $REQUEST_URL = 'https://live.sagepay.com/gateway/service/abort.vsp';
	protected static $TEST_REQUEST_URL = 'https://test.sagepay.com/gateway/service/abort.vsp';

    /**
     * ( max 15 )
     * @var string
     * @Assert\NotBlank(message="Transaction type must not be empty")
     * @SpField(name="TxType")
     */
    protected $txType = self::TYPE_ABORT;

    /**
     * unique transaction reference
     * @var string
     * @Assert\NotBlank(message="Transaction id must not be empty")
     * @SpField(name="VPSTxId")
     */
    private $vpsTxId;

    /**
     * 10-digit alphanumeric code that is used in digitally signing the transaction
     * @var string
     * @Assert\NotBlank(message="Security key must not be empty")
     * @SpField(name="SecurityKey")
     */
    private $securityKey;

    /**
     * the bank will use this value to refer to the transaction
     * @var string
     * @Assert\NotBlank(message="Transaction authorization number must not be empty")
     * @SpField(name="TxAuthNo")
     */
    private $txAuthNo;

    /**
     * @return GeneralResponse
     */
    public function createResponse()
    {
		return new GeneralResponse();
	}

    /**
     * @return string
     */
    public function getTxType()
    {
        return $this->txType;
    }

    /**
     * @param string $txType
     */
    public function setTxType($txType)
    {
		$this->txType = $txType;
	}

    /**
     * @return string
     */
    public function getVpsTxId()
    {
		return $this->vpsTxId;
	}

    /**
     * @param string $vpsTxId
     */
    public function setVpsTxId($vpsTxId)
    {
		$this->vpsTxId = $vpsTxId;
	}

    /**
     * @return string
     */
    public function getSecurityKey()
    {
		return $this->securityKey;
	}

    /**
     * @param string $securityKey
     */
    public function setSecurityKey($securityKey)
    {
		$this->securityKey = $securityKey;
	}

    /**
     * @return string
     */
    public function getTxAuthNo()
    {
		return $this->txAuthNo;
	}

    /**
     * @param string $txAuthNo
     */
    public function setTxAuthNo($txAuthNo)
    {
		$this->txAuthNo = $txAuthNo;
	}
}