<?php

namespace SagePay\Token\Request;

use SagePay\Token\Response\Details as ResponseDetails;
use SagePay\Token\Annotations\SpField;
use Symfony\Component\Validator\Constraints as Assert;

class Details extends RequestAbstract
{

    const SECURE3D_DEFAULT = 0;
    const SECURE3D_FORCE = 1;
    const SECURE3D_OFF = 2;
    //force 3d security checks , disregard account options
    const SECURE3D_FORCE_NOACCOUNT = 3;
    const CV2_OFF = 2;
    const CONTINUOUS_AUTHORITY = 'C';

    protected static $REQUEST_URL = 'https://live.sagepay.com/gateway/service/vspdirect-register.vsp';
    protected static $TEST_REQUEST_URL = 'https://test.sagepay.com/gateway/service/vspdirect-register.vsp';


    /**
     * ( max 15 )
     * @var string
     * @Assert\NotBlank(message="Transaction type must not be empty")
     * @SpField(name="TxType")
     */
    protected $txType = self::TYPE_PAYMENT;

    /**
     * token
     * @var string
     * @SpField(name="Token")
     * @Assert\NotBlank(message="Token must not be empty")
     * @Assert\Length(min=38, max=38, exactMessage="|Token must be {{ limit }} characters")
     * @SpField(name="Token")
     */
    protected $token;

    /**
     * 1.00 - 100,000.00 ( positive, numeric, two decimal places )
     * @var double
     * @Assert\Type(type="numeric", message="Amount {{ value }} is not valid")
     * @SpField(name="Amount")
     */
    private $amount;

    /**
     * @var string max 3
     * @SpField(name="Currency")
     * @Assert\Length(max=3, maxMessage="|Currency must not be more than {{ limit }} characters")
     * @SpField(name="Currency")
     */
    private $currency;
    
    /**
     * @var string
     * @SpField(name="Description")
     */
    private $description;

    /**
     * whether to store token
     * @var int
     * @Assert\Choice(choices = {0, 1}, message="Store token value must be 0 or 1")
     * @SpField(name="StoreToken")
     */
    private $storeToken = 0;

    /**
     * The extra security 3 digits on the signature strip of the card,
     * or the extra 4 digits on the front for American
     * @var string ( max 4 ) optional
     * @SpField(name="CV2")
     * @Assert\Length(min=3, max=4, minMessage="|CV2 number must be at least {{ limit }} characters", maxMessage="|CV2 number must not be more than {{ limit }} characters")
     * @SpField(name="CV2")
     */
    private $cv2;

    /** 
     * @var string max 20  
     * @Assert\NotBlank(message="Billing surname must not be empty")
     * @Assert\Length(max=20, maxMessage="|Billing surname must not be more than {{ limit }} characters, {{ value }} provided")
     * @SpField(name="BillingSurname")
     */
    private $billingSurname;

    /** 
     * @var string max 20  
     * @Assert\NotBlank(message="Billing first name must not be empty")
     * @Assert\Length(max=20, maxMessage="|Billing first name must not be more than {{ limit }} characters, {{ value }} provided")
     * @SpField(name="BillingFirstnames")
     */
    private $billingFirstnames;

    /** 
     * @var string max 100  
     * @Assert\NotBlank(message="Billing address1 must not be empty")
     * @Assert\Length(max=100, maxMessage="|Billing address1 must not be more than {{ limit }} characters, {{ value }} provided")
     * @SpField(name="BillingAddress1")
     */
    private $billingAddress1;

    /** 
     * @var string max 40 optional
     * @Assert\Length(max=40, maxMessage="|Billing address2 must not be more than {{ limit }} characters, {{ value }} provided")
     * @SpField(name="BillingAddress2")
     */
    private $billingAddress2;

    /** 
     * @var string max 40  
     * @Assert\NotBlank(message="Billing city must not be empty")
     * @Assert\Length(max=40, maxMessage="|Billing city must not be more than {{ limit }} characters, {{ value }} provided")
     * @SpField(name="BillingCity")
     */
    private $billingCity;

    /** 
     * @var string max 40  
     * @Assert\NotBlank(message="Billing postcode must not be empty")
     * @Assert\Length(max=40, maxMessage="|Billing postcode must not be more than {{ limit }} characters, {{ value }} provided")
     * @SpField(name="BillingPostCode")
     */
    private $billingPostCode;

    /** 
     * @var string max 2  
     * @Assert\NotBlank(message="Billing country must not be empty")
     * @Assert\Length(max=2, maxMessage="|Billing country must not be more than {{ limit }} characters, {{ value }} provided")
     * @SpField(name="BillingCountry")
     */
    private $billingCountry;

    /** 
     * @var string max 2 optional
     * @Assert\Length(max=2, maxMessage="|Billing state must not be more than {{ limit }} characters, {{ value }} provided")
     * @SpField(name="BillingState")
     */
    private $billingState;

    /** 
     * @var string max 20 optional
     * @Assert\Length(max=20, maxMessage="|Billing phone must not be more than {{ limit }} characters, {{ value }} provided")
     * @SpField(name="BillingPhone")
     */
    private $billingPhone;

    /** 
     * @var string max 20  
     * @Assert\NotBlank(message="Delivery surname must not be empty")
     * @Assert\Length(max=20, maxMessage="|Delivery surname must not be more than {{ limit }} characters, {{ value }} provided")
     * @SpField(name="DeliverySurname")
     */
    private $deliverySurname;

    /** 
     * @var string max 20  
     * @Assert\NotBlank(message="Delivery first name must not be empty")
     * @Assert\Length(max=20, maxMessage="|Delivery first name must not be more than {{ limit }} characters, {{ value }} provided")
     * @SpField(name="DeliveryFirstnames")
     */
    private $deliveryFirstnames;

    /** 
     * @var string max 100  
     * @Assert\NotBlank(message="Delivery address1 must not be empty")
     * @Assert\Length(max=100, maxMessage="|Delivery address1 must not be more than {{ limit }} characters, {{ value }} provided")
     * @SpField(name="DeliveryAddress1")
     */
    private $deliveryAddress1;

    /** 
     * @var string max 100  optional
     * @Assert\Length(max=100, maxMessage="|Delivery address2 must not be more than {{ limit }} characters, {{ value }} provided")
     * @SpField(name="DeliveryAddress2")
     */
    private $deliveryAddress2;

    /** 
     * @var string max 40  
     * @Assert\NotBlank(message="Delivery city must not be empty")
     * @Assert\Length(max=40, maxMessage="|Delivery city must not be more than {{ limit }} characters, {{ value }} provided")
     * @SpField(name="DeliveryCity")
     */
    private $deliveryCity;

    /** 
     * @var string max 10  
     * @Assert\NotBlank(message="Delivery postcode must not be empty")
     * @Assert\Length(max=10, maxMessage="|Delivery postcode must not be more than {{ limit }} characters, {{ value }} provided")
     * @SpField(name="DeliveryPostCode")
     */
    private $deliveryPostCode;

    /** 
     * @var string max 2  
     * @Assert\NotBlank(message="Delivery country must not be empty")
     * @Assert\Length(max=2, maxMessage="|Delivery country must not be more than {{ limit }} characters, {{ value }} provided")
     * @SpField(name="DeliveryCountry")
     */
    private $deliveryCountry;

    /** 
     * @var string max 2 optional
     * @Assert\Length(max=2, maxMessage="|Delivery state must not be more than {{ limit }} characters, {{ value }} provided")
     * @SpField(name="DeliveryState")
     */
    private $deliveryState;

    /** 
     * @var string max 20 optional
     * @Assert\Length(max=20, maxMessage="|Delivery phone must not be more than {{ limit }} characters, {{ value }} provided")
     * @SpField(name="DeliveryPhone")
     */
    private $deliveryPhone;

    /** 
     * @var string max 255 optional
     * @Assert\Length(max=255, maxMessage="|Customer email must not be more than {{ limit }} characters, {{ value }} provided")
     * @SpField(name="CustomerEmail")
     */
    private $customerEmail;

    /** 
     * @var string max 7500 optional
     * @Assert\Length(max=7500, maxMessage="|Basket must not be more than {{ limit }} characters")
     * @SpField(name="Basket")
     */
    private $basket;

    /** 
     * @var int flag optional
     * @Assert\Choice(choices = {0, 1}, message="Gift aid payment value must be 0 or 1. Value supplied {{ value }}")
     * @SpField(name="GiftAidPayment")
     */
    private $giftAidPayment = 0;

    /** 
     * @var int fine tune AVS/CV2 flag optional
     * @Assert\Choice(choices = {0, 1, 2, 3}, message="Apply 3d secure value must be 0, 1, 2, 3 . Value supplied {{ value }}")
     * @SpField(name="ApplyAVSCV2")
     */
    private $applyAvsCv2 = 0;

    /** 
     * @var int fine tune 3D secure checks
     * @Assert\NotBlank(message="Apply 3d secure value must not be empty")
     * @Assert\Choice(choices = {0, 1, 2, 3}, message="Apply 3d secure value must be 0, 1, 2, 3 . Value supplied {{ value }}")
     * @SpField(name="Apply3DSecure")
     */
    private $apply3dSecure = 0;

    /** 
     * @var string max 15 optional
     * @Assert\Ip(message="Client ip address is not valid!")
     * @SpField(name="ClientIPAddress")
     */
    private $clientIpAddress;

    /** 
     * @var string max 1 optional
     * @Assert\Choice(choices = {"E", "C", "M"}, message="Account type value must be E, C, M . Value supplied {{ value }}")
     * @SpField(name="AccountType")
     */
    private $accountType;

    /**
     * @return ResponseDetails
     */
    public function createResponse()
    {
        return new ResponseDetails();
    }

    /**
     * @return string
     */
    public function getTxType()
    {
        return $this->txType;
    }

    /**
     * @param string $txType
     */
    public function setTxType($txType)
    {
        $this->txType = $txType;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getStoreToken()
    {
        return $this->storeToken;
    }

    /**
     * @param int $storeToken
     */
    public function setStoreToken($storeToken)
    {
        $this->storeToken = $storeToken;
    }

    /**
     * @return string
     */
    public function getCv2()
    {
        return $this->cv2;
    }

    /**
     * @param string $cv2
     */
    public function setCv2($cv2)
    {
        $this->cv2 = $cv2;
    }

    /**
     * @return string
     */
    public function getBillingSurname()
    {
        return $this->billingSurname;
    }

    /**
     * @param string $billingSurname
     */
    public function setBillingSurname($billingSurname)
    {
        $this->billingSurname = $billingSurname;
    }

    /**
     * @return string
     */
    public function getBillingFirstnames()
    {
        return $this->billingFirstnames;
    }

    /**
     * @param string $billingFirstnames
     */
    public function setBillingFirstnames($billingFirstnames)
    {
        $this->billingFirstnames = $billingFirstnames;
    }

    /**
     * @return string
     */
    public function getBillingAddress1()
    {
        return $this->billingAddress1;
    }

    /**
     * @param string $billingAddress1
     */
    public function setBillingAddress1($billingAddress1)
    {
        $this->billingAddress1 = $billingAddress1;
    }

    /**
     * @return string
     */
    public function getBillingAddress2()
    {
        return $this->billingAddress2;
    }

    /**
     * @param string $billingAddress2
     */
    public function setBillingAddress2($billingAddress2)
    {
        $this->billingAddress2 = $billingAddress2;
    }

    /**
     * @return string
     */
    public function getBillingCity()
    {
        return $this->billingCity;
    }

    /**
     * @param string $billingCity
     */
    public function setBillingCity($billingCity)
    {
        $this->billingCity = $billingCity;
    }

    /**
     * @return string
     */
    public function getBillingPostCode()
    {
        return $this->billingPostCode;
    }

    /**
     * @param string $billingPostCode
     */
    public function setBillingPostCode($billingPostCode)
    {
        $this->billingPostCode = $billingPostCode;
    }

    /**
     * @return string
     */
    public function getBillingCountry()
    {
        return $this->billingCountry;
    }

    /**
     * @param string $billingCountry
     */
    public function setBillingCountry($billingCountry)
    {
        $this->billingCountry = $billingCountry;
    }

    /**
     * @return string
     */
    public function getBillingState()
    {
        return $this->billingState;
    }

    /**
     * @param string $billingState
     */
    public function setBillingState($billingState)
    {
        $this->billingState = $billingState;
    }

    /**
     * @return string
     */
    public function getBillingPhone()
    {
        return $this->billingPhone;
    }

    /**
     * @param string $billingPhone
     */
    public function setBillingPhone($billingPhone)
    {
        $this->billingPhone = $billingPhone;
    }

    /**
     * @return string
     */
    public function getDeliverySurname()
    {
        return $this->deliverySurname;
    }

    /**
     * @param string $deliverySurname
     */
    public function setDeliverySurname($deliverySurname)
    {
        $this->deliverySurname = $deliverySurname;
    }

    /**
     * @return string
     */
    public function getDeliveryFirstnames()
    {
        return $this->deliveryFirstnames;
    }

    /**
     * @param string $deliveryFirstnames
     */
    public function setDeliveryFirstnames($deliveryFirstnames)
    {
        $this->deliveryFirstnames = $deliveryFirstnames;
    }

    /**
     * @return string
     */
    public function getDeliveryAddress1()
    {
        return $this->deliveryAddress1;
    }

    /**
     * @param string $deliveryAddress1
     */
    public function setDeliveryAddress1($deliveryAddress1)
    {
        $this->deliveryAddress1 = $deliveryAddress1;
    }

    /**
     * @return string
     */
    public function getDeliveryAddress2()
    {
        return $this->deliveryAddress2;
    }

    /**
     * @param string $deliveryAddress2
     */
    public function setDeliveryAddress2($deliveryAddress2)
    {
        $this->deliveryAddress2 = $deliveryAddress2;
    }

    /**
     * @return string
     */
    public function getDeliveryCity()
    {
        return $this->deliveryCity;
    }

    /**
     * @param string $deliveryCity
     */
    public function setDeliveryCity($deliveryCity)
    {
        $this->deliveryCity = $deliveryCity;
    }

    /**
     * @return string
     */
    public function getDeliveryPostCode()
    {
        return $this->deliveryPostCode;
    }

    /**
     * @param string $deliveryPostCode
     */
    public function setDeliveryPostCode($deliveryPostCode)
    {
        $this->deliveryPostCode = $deliveryPostCode;
    }

    /**
     * @return string
     */
    public function getDeliveryCountry()
    {
        return $this->deliveryCountry;
    }

    /**
     * @param string $deliveryCountry
     */
    public function setDeliveryCountry($deliveryCountry)
    {
        $this->deliveryCountry = $deliveryCountry;
    }

    /**
     * @return string
     */
    public function getDeliveryState()
    {
        return $this->deliveryState;
    }

    /**
     * @param string $deliveryState
     */
    public function setDeliveryState($deliveryState)
    {
        $this->deliveryState = $deliveryState;
    }

    /**
     * @return string
     */
    public function getDeliveryPhone()
    {
        return $this->deliveryPhone;
    }

    /**
     * @param string $deliveryPhone
     */
    public function setDeliveryPhone($deliveryPhone)
    {
        $this->deliveryPhone = $deliveryPhone;
    }

    /**
     * @return string
     */
    public function getCustomerEmail()
    {
        return $this->customerEmail;
    }

    /**
     * @param string $customerEmail
     */
    public function setCustomerEmail($customerEmail)
    {
        $this->customerEmail = $customerEmail;
    }

    /**
     * @return string
     */
    public function getBasket()
    {
        return $this->basket;
    }

    /**
     * @param string $basket
     */
    public function setBasket($basket)
    {
        $this->basket = $basket;
    }

    /**
     * @return int
     */
    public function getGiftAidPayment()
    {
        return $this->giftAidPayment;
    }

    /**
     * @param int $giftAidPayment
     */
    public function setGiftAidPayment($giftAidPayment)
    {
        $this->giftAidPayment = $giftAidPayment;
    }

    /**
     * @return int
     */
    public function getApplyAvsCv2()
    {
        return $this->applyAvsCv2;
    }

    /**
     * @param int $applyAvsCv2
     */
    public function setApplyAvsCv2($applyAvsCv2)
    {
        $this->applyAvsCv2 = $applyAvsCv2;
    }

    /**
     * @return int
     */
    public function getApply3dSecure()
    {
        return $this->apply3dSecure;
    }

    /**
     * @param int $apply3dSecure
     */
    public function setApply3dSecure($apply3dSecure)
    {
        $this->apply3dSecure = $apply3dSecure;
    }

    /**
     * @return string
     */
    public function getClientIpAddress()
    {
        return $this->clientIpAddress;
    }

    /**
     * @param string $clientIpAddress
     */
    public function setClientIpAddress($clientIpAddress)
    {
        $this->clientIpAddress = $clientIpAddress;
    }

    /**
     * @return string
     */
    public function getAccountType()
    {
        return $this->accountType;
    }

    /**
     * @param string $accountType
     */
    public function setAccountType($accountType)
    {
        $this->accountType = $accountType;
    }

}