<?php

namespace SagePay\Token\Request;

use SagePay\Token\Annotations\SpField;
use Symfony\Component\Validator\Constraints as Assert;

abstract class RequestAbstract extends \Object implements IRequest
{
    /**
     * available request types
     */
    const TYPE_PAYMENT = 'PAYMENT';
    const TYPE_DEFERRED = 'DEFERRED';
    const TYPE_AUTHENTICATE = 'AUTHENTICATE';
    const TYPE_SAVE = 'TOKEN';
    const TYPE_REMOVE = 'REMOVETOKEN';
    const TYPE_RELEASE = 'RELEASE';
    const TYPE_ABORT = 'ABORT';
    const TYPE_CANCEL = 'CANCEL';

    /**
     * live request url
     * @var string
     */
    protected static $REQUEST_URL;

    /**
     * test request url
     * @var string
     */
    protected static $TEST_REQUEST_URL;
    
    /**
     * ( max 15 )
     * @var string
     * @Assert\NotBlank
     * @SpField(name="TxType")
     */
    protected $txType = self::TYPE_AUTHENTICATE;

    /**
     * each request knows about the response it should receive
     * @return ResponseAbstract
     */
    public abstract function createResponse();


    /**
     * @return string
     */
    public function getTxType()
    {
        return $this->txType;
    }

    /**
     * @param string $txType
     */
    public function setTxType($txType)
    {
        $this->txType = $txType;
    }

    /**
     * @inheritdoc
     */
    public function getUrl($production)
    {
        if ($production) {
            return static::$REQUEST_URL;
        }
        return static::$TEST_REQUEST_URL;
    }

    /**
     * @return bool
     */
    public function isDeferred()
    {
        return $this->txType === self::TYPE_DEFERRED;
    }

}