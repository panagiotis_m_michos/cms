<?php

namespace SagePay\Token\Request;

interface IRequest {

    /**
     * @param bool $production
     * @return string
     */
    public function getUrl($production);

}