<?php

namespace SagePay\Token\Request;

use SagePay\Token\Response\Authentication as ResponseAuthentication;
use SagePay\Token\Annotations\SpField;
use Symfony\Component\Validator\Constraints as Assert;

class Authentication extends RequestAbstract
{

    protected static $REQUEST_URL = 'https://live.sagepay.com/gateway/service/direct3dcallback.vsp';
    protected static $TEST_REQUEST_URL = 'https://test.sagepay.com/gateway/service/direct3dcallback.vsp';
    
    /**
     * ( max 15 )
     * @var string
     * @Assert\NotBlank(message="Transaction type must not be empty")
     * @SpField(name="TxType")
     */
    protected $txType = self::TYPE_AUTHENTICATE;

    /**
     * unique transaction identifier
     * @var string
     * @Assert\NotBlank(message="MD must not be empty")
     * @SpField(name="MD")
     */
    private $md;

    /**
     * encoded string to identified transaction
     * @var string
     * @Assert\NotBlank(message="PaRes must not be empty")
     * @SpField(name="PARes")
     */
    private $paRes;

    /**
     * @return ResponseAuthentication
     */
    public function createResponse()
    {
        return new ResponseAuthentication();
    }

    /**
     * @return string
     */
    public function getTxType()
    {
        return $this->txType;
    }

    /**
     * @param string $txType
     */
    public function setTxType($txType)
    {
        $this->txType = $txType;
    }

    /**
     * @return string
     */
    public function getMd()
    {
        return $this->md;
    }

    /**
     * @param string $md
     */
    public function setMd($md)
    {
        $this->md = $md;
    }

    /**
     * @return string
     */
    public function getPaRes()
    {
        return $this->paRes;
    }

    /**
     * @param string $paRes
     */
    public function setPaRes($paRes)
    {
        $this->paRes = $paRes;
    }
}