<?php

namespace SagePay\Token\Mapper;

use Doctrine\Common\Annotations\Reader;
use ReflectionClass;
use ReflectionProperty;
use SagePay\Token\Identity;
use SagePay\Token\Request\RequestAbstract;
use SagePay\Token\Response\ResponseAbstract;

class AnnotationMapper
{

    /**
     * @var Reader
     */
    private $reader;
    
    /**
     * @var string
     */
    private $annotationName;

    public function __construct(Reader $reader, $annotationName)
    {
        $this->reader = $reader;
        $this->annotationName = $annotationName;
    }

    /**
     * 
     * @param mixed $object
     * @return array
     */
    private function propertiesToArray($object)
    {
        $reflClass = new ReflectionClass($object);
        $propertyArray = array();
        foreach ($reflClass->getProperties(ReflectionProperty::IS_PRIVATE | ReflectionProperty::IS_PROTECTED) as $property) {
            if ($property->isStatic()) {
                continue;
            }
            $propertyName = $property->getName();
            $camelCaseMethodName = 'get' . ucfirst($propertyName);
            if ($reflClass->hasMethod($camelCaseMethodName)) {
                if ($object->$camelCaseMethodName() !== null) {
                    $propertyAnnotation = $this->reader->getPropertyAnnotation($property, $this->annotationName);
                    if ($propertyAnnotation) {
                        $propertyArray[$propertyAnnotation->name] = $object->$camelCaseMethodName();
                    }
                }
            }
        }
        return $propertyArray;
    }

    /**
     * assumes that setters and getters exists for object properties
     * @param RequestAbstract|ResponseAbstract $object
     * @param Identity $identity
     * @return array
     */
    public function objectToArray($object, Identity $identity = null)
    {
        $arr = $this->propertiesToArray($object);
        if (!empty($identity)) {
            $identityArr = $this->propertiesToArray($identity);
            return array_merge($arr, $identityArr);
        }
        return $arr;
    }

    /**
     * assumes that setters and getters exists for object properties
     * @param array $arr
     * @param ResponseAbstract|RequestAbstract $object
     * @return ResponseAbstract|RequestAbstract
     */
    public function arrayToObject(array $arr, $object)
    {
        $reflClass = new ReflectionClass($object);
        foreach ($reflClass->getProperties() as $property) {
            $propertyAnnotation = $this->reader->getPropertyAnnotation($property, $this->annotationName);
            if ($propertyAnnotation) {
                if (isset($arr[$propertyAnnotation->name])) {
                    $propertyName = $property->getName();
                    $camelCaseMethodName = 'set' . ucfirst($propertyName);
                    if ($reflClass->hasMethod($camelCaseMethodName)) {
                        $object->$camelCaseMethodName($arr[$propertyAnnotation->name]);
                    }
                }
            }
        }
        return $object;
    }

}