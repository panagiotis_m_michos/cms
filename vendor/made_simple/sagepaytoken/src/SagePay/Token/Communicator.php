<?php

namespace SagePay\Token;

use SagePay\Token\Exception\ConnectionError;
use SagePay\Token\Exception\ValidationError;
use SagePay\Token\Mapper\AnnotationMapper;
use SagePay\Token\Request\RequestAbstract;
use SagePay\Token\Validator\IValidator;

class Communicator
{

    const ENVIRONMENT_PRODUCTION = 'LIVE';
    const ENVIRONMENT_TEST = 'TEST';

    /**
     * @var Transfer
     */
    protected $transfer;

    /**
     * @var AnnotationMapper
     */
    protected $mapper;

    /**
     * @var IValidator
     */
    protected $validator;

    /**
     * whether its a production environment
     * @var bool
     */
    protected $environment;

    /**
     * @param Transfer $transfer
     * @param AnnotationMapper $mapper
     * @param IValidator $validator
     * @param string $environment
     */
    public function __construct(Transfer $transfer, AnnotationMapper $mapper, IValidator $validator, $environment = self::ENVIRONMENT_TEST)
    {
        $this->transfer = $transfer;
        $this->mapper = $mapper;
        $this->validator = $validator;
        $this->environment = $environment;
    }

    /**
     * submit request to sage pay and return response
     * @param RequestAbstract $request
     * @param Identity $identity
     * @return ResponseAbstract
     * @throws ConnectionError
     * @throws ValidationError
     */
    public function submit(RequestAbstract $request, Identity $identity)
    {
        $this->validator->validate($request);
        $this->validator->validate($identity);
        //which enviroment should we use
        $productionEnvironment = $this->environment == self::ENVIRONMENT_PRODUCTION;
        $url = $request->getUrl($productionEnvironment);
        $requestData = $this->mapper->objectToArray($request, $identity);
        $responseString = $this->transfer->makeRequest($url, $requestData);
        $response = $request->createResponse();
        $responseArray = $this->buildArray($responseString);
        $this->mapper->arrayToObject($responseArray, $response);
        $response->setRawResponse($responseString);
        return $response;
    }

    /**
     * assign string values to properties
     * @param string $text
     * @return array
     */
    private function buildArray($text)
    {
        $array = array();
        $lines = preg_split('/(\n|\r\n|\r)/', $text);
        foreach ($lines as $line) {
            if (strpos($line, '=') !== false) {
                list( $key, $value ) = explode('=', $line, 2);
                $array[trim($key)] = trim($value);
            }
        }
        return $array;
    }

}