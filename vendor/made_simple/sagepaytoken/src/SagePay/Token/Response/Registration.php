<?php

namespace SagePay\Token\Response;

use SagePay\Token\Annotations\SpField;

class Registration extends ResponseAbstract
{

    /**
     * 38 alphanumeric characters
     * @var string
     * @SpField(name="Token")
     */
    protected $token;

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

}