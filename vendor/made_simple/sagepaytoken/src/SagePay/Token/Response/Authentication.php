<?php

namespace SagePay\Token\Response;

use SagePay\Token\Annotations\SpField;

class Authentication extends ResponseAbstract
{

    /**
     * unique transaction reference
     * @var string
     * @SpField(name="VPSTxId")
     */
    protected $vpsTxId;

    /**
     * 10-digit alphanumeric code that is used in digitally signing the transaction
     * @var string
     * @SpField(name="SecurityKey")
     */
    protected $securityKey;

    /**
     * the bank will use this value to refer to the transaction
     * @var string
     * @SpField(name="TxAuthNo")
     */
    protected $txAuthNo;

    /**
     * @var string
     * @SpField(name="AVSCV2")
     */
    protected $avsCv2;

    /**
     * @var string
     * @SpField(name="AddressResult")
     */
    protected $addressResult;

    /**
     * @var string
     * @SpField(name="PostCodeResult")
     */
    protected $postCodeResult;

    /**
     * @var string
     * @SpField(name="CV2Result")
     */
    protected $cv2Result;

    /**
     * @var string
     * @SpField(name="3DSecureStatus")
     */
    protected $secureStatus;

    /**
     * @return string
     */
    public function getVpsTxId()
    {
        return $this->vpsTxId;
    }

    /**
     * @param string $vpsTxId
     */
    public function setVpsTxId($vpsTxId)
    {
        $this->vpsTxId = $vpsTxId;
    }

    /**
     * @return string
     */
    public function getSecurityKey()
    {
        return $this->securityKey;
    }

    /**
     * @param string $securityKey
     */
    public function setSecurityKey($securityKey)
    {
        $this->securityKey = $securityKey;
    }

    /**
     * @return string
     */
    public function getTxAuthNo()
    {
        return $this->txAuthNo;
    }

    /**
     * @param string $txAuthNo
     */
    public function setTxAuthNo($txAuthNo)
    {
        $this->txAuthNo = $txAuthNo;
    }

    /**
     * @return string
     */
    public function getAvsCv2()
    {
        return $this->avsCv2;
    }

    /**
     * @param string $avsCv2
     */
    public function setAvsCv2($avsCv2)
    {
        $this->avsCv2 = $avsCv2;
    }

    /**
     * @return string
     */
    public function getAddressResult()
    {
        return $this->addressResult;
    }

    /**
     * @param string $addressResult
     */
    public function setAddressResult($addressResult)
    {
        $this->addressResult = $addressResult;
    }

    /**
     * @return string
     */
    public function getPostCodeResult()
    {
        return $this->postCodeResult;
    }

    /**
     * @param string $postCodeResult
     */
    public function setPostCodeResult($postCodeResult)
    {
        $this->postCodeResult = $postCodeResult;
    }

    /**
     * @return string
     */
    public function getCv2Result()
    {
        return $this->cv2Result;
    }

    /**
     * @param string $cv2Result
     */
    public function setCv2Result($cv2Result)
    {
        $this->cv2Result = $cv2Result;
    }

    /**
     * @return string
     */
    public function getSecureStatus()
    {
        return $this->secureStatus;
    }

    /**
     * @param string $secureStatus
     */
    public function setSecureStatus($secureStatus)
    {
        $this->secureStatus = $secureStatus;
    }

    /**
     * @return bool
     */
    public function isSuccess()
    {
        return $this->getStatus() === 'OK' || $this->getStatus() === 'AUTHENTICATED' || $this->getStatus() === 'REGISTERED';
    }
}