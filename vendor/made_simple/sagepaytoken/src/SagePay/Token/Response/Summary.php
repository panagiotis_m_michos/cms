<?php

namespace SagePay\Token\Response;

class Summary extends \Object
{
    /*
     * unique transaction reference
     * @var string
     */
    private $vpsTxId;

    /**
     * 10-digit alphanumeric code that is used in digitally signing the transaction
     * @var string
     */
    private $securityKey;

    /**
     * the bank will use this value to refer to the transaction
     * @var string
     */
    private $txAuthNo;

    /**
     * reference code to the transaction
     * @var string (40 chars)
     */
    private $vendorTxCode;

    /**
     * @var string
     */
    private $token;

    /**
     * @var string
     */
    private $cardHolder;

    /**
     * @var string
     */
    private $cardNumber;

    /**
     * @var string
     */
    private $cardType;

    /**
     * @var string
     */
    private $cardExpiryDate;


    /**
     * @return string
     */
    public function getVpsTxId()
    {
        return $this->vpsTxId;
    }

    /**
     * @param string $vpsTxId
     */
    public function setVpsTxId($vpsTxId)
    {
        $this->vpsTxId = $vpsTxId;
    }

    /**
     * @return string
     */
    public function getSecurityKey()
    {
        return $this->securityKey;
    }

    /**
     * @param string $securityKey
     */
    public function setSecurityKey($securityKey)
    {
        $this->securityKey = $securityKey;
    }

    /**
     * @return string
     */
    public function getTxAuthNo()
    {
        return $this->txAuthNo;
    }

    /**
     * @param string $txAuthNo
     */
    public function setTxAuthNo($txAuthNo)
    {
        $this->txAuthNo = $txAuthNo;
    }

    /**
     * @return string
     */
    public function getVendorTxCode()
    {
        return $this->vendorTxCode;
    }

    /**
     * @param string $vendorTxCode
     */
    public function setVendorTxCode($vendorTxCode)
    {
        $this->vendorTxCode = $vendorTxCode;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getCardHolder()
    {
        return $this->cardHolder;
    }

    /**
     * @param string $cardHolder
     */
    public function setCardHolder($cardHolder)
    {
        $this->cardHolder = $cardHolder;
    }

    /**
     * @return string
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * @param string $cardNumber
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;
    }

    /**
     * @return string
     */
    public function getCardType()
    {
        return $this->cardType;
    }

    /**
     * @param string $cardType
     */
    public function setCardType($cardType)
    {
        $this->cardType = $cardType;
    }

    /**
     * @return string
     */
    public function getCardExpiryDate()
    {
        return $this->cardExpiryDate;
    }

    /**
     * @param string $cardExpiryDate
     */
    public function setCardExpiryDate($cardExpiryDate)
    {
        $this->cardExpiryDate = $cardExpiryDate;
    }
}
