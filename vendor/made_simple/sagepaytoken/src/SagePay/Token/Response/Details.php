<?php

namespace SagePay\Token\Response;

use SagePay\Token\Annotations\SpField;

/**
 * Can contain authentication information if there is no 3d authentication
 */
class Details extends Authentication
{

    /**
     * unique identifier
     * @var string
     * @SpField(name="MD")
     */
    private $md;

    /**
     * encoded string sent to the bank
     * @var string
     * @SpField(name="PAReq")
     */
    private $paReq;

    /**
     * url to redirect 
     * @var string
     * @SpField(name="ACSURL")
     */
    private $acsUrl;


    /**
     * @return string
     */
    public function getMd()
    {
        return $this->md;
    }

    /**
     * @param string $md
     */
    public function setMd($md)
    {
        $this->md = $md;
    }

    /**
     * @return string
     */
    public function getPaReq()
    {
        return $this->paReq;
    }

    /**
     * @param string $paReq
     */
    public function setPaReq($paReq)
    {
        $this->paReq = $paReq;
    }

    /**
     * @return string
     */
    public function getAcsUrl()
    {
        return $this->acsUrl;
    }

    /**
     * @param string $acsUrl
     */
    public function setAcsUrl($acsUrl)
    {
        $this->acsUrl = $acsUrl;
    }

    /**
     * @return bool
     */
    public function isRequiredToRedirect()
    {
        return strlen($this->getAcsUrl()) > 0;
    }

}