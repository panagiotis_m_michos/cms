<?php

namespace SagePay\Token\Response;

use SagePay\Token\Annotations\SpField;

abstract class ResponseAbstract extends \Object
{

    /**
     * OK | MALFORMED | INVALID | 3DAUTH
     * @var string
     * @SpField(name="Status")
     */
    protected $status;

    /**
     * extra details for response
     * @var string
     * @SpField(name="StatusDetail")
     */
    protected $statusDetail;

    /**
     * contains raw response from sage pay
     * @var string
     */
    protected $rawResponse;

    /**
     * @return mixed
     */
    public function getVpsProtocol()
    {
        return $this->vpsProtocol;
    }

    /**
     * @param string $vpsProtocol
     */
    public function setVpsProtocol($vpsProtocol)
    {
        $this->vpsProtocol = $vpsProtocol;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getStatusDetail()
    {
        return $this->statusDetail;
    }

    /**
     * @param string $statusDetail
     */
    public function setStatusDetail($statusDetail)
    {
        $this->statusDetail = $statusDetail;
    }

    /**
     * @return string
     */
    public function getRawResponse()
    {
        return $this->rawResponse;
    }

    /**
     * @param string $rawResponse
     */
    public function setRawResponse($rawResponse)
    {
        $this->rawResponse = $rawResponse;
    }

    /**
     * whether request was successfull
     * @return bool
     */
    public function isSuccess()
    {
        return $this->getStatus() == 'OK';
    }

    /**
     * whether authentication is required
     * @return bool
     */
    public function isAuthenticationRequired()
    {
        return $this->getStatus() == '3DAUTH';
    }

}