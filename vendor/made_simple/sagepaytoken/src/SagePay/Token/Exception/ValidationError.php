<?php

namespace SagePay\Token\Exception;

use UnexpectedValueException;

class ValidationError extends UnexpectedValueException {
	
}