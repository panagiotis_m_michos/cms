<?php

namespace SagePay\Token\Exception;

use UnexpectedValueException;

class FailedResponse extends UnexpectedValueException
{
    const AUTHORISATION_DECLINED_BY_BANK = 2000;
}
