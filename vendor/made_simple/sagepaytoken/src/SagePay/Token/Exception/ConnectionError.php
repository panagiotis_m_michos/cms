<?php

namespace SagePay\Token\Exception;

use RuntimeException;

class ConnectionError extends RuntimeException {
	
}