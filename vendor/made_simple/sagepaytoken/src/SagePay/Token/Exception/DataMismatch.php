<?php

namespace SagePay\Token\Exception;

use UnexpectedValueException;

class DataMismatch extends UnexpectedValueException {
	
}