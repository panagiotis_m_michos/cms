#Git

##Todo
This package is not covered by unit tests. They should be implemented in near future.

##About
This library serves as object layer above the URIs service provided by Companies House at http://data.companieshouse.gov.uk/

##Requirements
This package requires Guzzle as its dependency

##Usage
~~~
use CompaniesHouse\Data\Data;

$companyNumber = 124785;

$data = new Data();
$companyDetails = $data->getCompanyDetails($companyNumber); //will return object of CompanyDetailsResponse
echo $companyDetails->getCompanyName();
~~~
