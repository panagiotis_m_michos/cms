<?php

namespace CompaniesHouse\Data\Entities;

use DateTime;
use Nette\Object;

class Accounts extends Object
{
    /**
     * @var int
     */
    private $accountRefDay;

    /**
     * @var int
     */
    private $accountRefMonth;

    /**
     * @var DateTime
     */
    private $nextDueDate;

    /**
     * @var DateTime
     */
    private $lastMadeUpDate;

    /**
     * @var string
     */
    private $accountCategory;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        foreach ($data as $property => $value) {
            $property = lcfirst($property);

            if ($this->getReflection()->hasProperty($property)) {
                if (in_array($property, array('nextDueDate', 'lastMadeUpDate')) && $value != NULL) {
                    $this->$property = DateTime::createFromFormat('d/m/Y', $value);
                } else {
                    $this->$property = $value;
                }
            }
        }
    }

    /**
     * @return string
     */
    public function getAccountCategory()
    {
        return $this->accountCategory;
    }

    /**
     * @return int
     */
    public function getAccountRefDay()
    {
        return $this->accountRefDay;
    }

    /**
     * @return int
     */
    public function getAccountRefMonth()
    {
        return $this->accountRefMonth;
    }

    /**
     * @return DateTime
     */
    public function getLastMadeUpDate()
    {
        return $this->lastMadeUpDate;
    }

    /**
     * @return DateTime
     */
    public function getNextDueDate()
    {
        return $this->nextDueDate;
    }
}