<?php

namespace CompaniesHouse\Data\Entities;

use ArrayObject;

class PreviousNames extends ArrayObject
{
    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        parent::__construct(array(), 0, "ArrayIterator");

        foreach ($data as $previousNameData) {
            $this->append(new PreviousNameItem($previousNameData));
        }
    }
} 