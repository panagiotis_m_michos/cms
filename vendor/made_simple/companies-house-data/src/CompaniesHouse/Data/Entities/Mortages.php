<?php

namespace CompaniesHouse\Data\Entities;

use Nette\Object;

class Mortgages extends Object
{
    /**
     * @var int
     */
    private $numMortCharges;

    /**
     * @var int
     */
    private $numMortOutstanding;

    /**
     * @var int
     */
    private $numMortPartSatisfied;

    /**
     * @var int
     */
    private $numMortSatisfied;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        foreach ($data as $property => $value) {
            $property = lcfirst($property);

            if ($this->getReflection()->hasProperty($property)) {
                $this->$property = $value;
            }
        }
    }

    /**
     * @return int
     */
    public function getNumMortCharges()
    {
        return $this->numMortCharges;
    }

    /**
     * @return int
     */
    public function getNumMortOutstanding()
    {
        return $this->numMortOutstanding;
    }

    /**
     * @return int
     */
    public function getNumMortPartSatisfied()
    {
        return $this->numMortPartSatisfied;
    }

    /**
     * @return int
     */
    public function getNumMortSatisfied()
    {
        return $this->numMortSatisfied;
    }
}