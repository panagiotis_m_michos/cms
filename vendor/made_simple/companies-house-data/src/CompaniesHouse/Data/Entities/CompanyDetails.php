<?php

namespace CompaniesHouse\Data\Entities;

use DateTime;

class CompanyDetails
{
    /**
     * @var string
     */
    private $companyName;

    /**
     * @var string
     */
    private $companyNumber;

    /**
     * @var RegisteredAddress
     */
    private $regAddress;

    /**
     * @var string
     */
    private $companyCategory;

    /**
     * @var string
     */
    private $companyStatus;

    /**
     * @var string
     */
    private $countryOfOrigin;

    /**
     * @var DateTime
     */
    private $incorporationDate;

    /**
     * @var PreviousNames
     */
    private $previousNames;

    /**
     * @var Accounts
     */
    private $accounts;

    /**
     * @var Returns
     */
    private $returns;

    /**
     * @var Mortgages
     */
    private $mortgages;

    /**
     * @var SicCodes
     */
    private $sicCodes;

    /**
     * @var DateTime
     */
    private $registrationDate;

    /**
     * @var DateTime
     */
    private $dissolutionDate;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        if (!empty($data['CompanyName'])) {
            $this->companyName = $data['CompanyName'];
        }

        if (!empty($data['CompanyNumber'])) {
            $this->companyNumber = $data['CompanyNumber'];
        }

        if (!empty($data['CompanyCategory'])) {
            $this->companyCategory = $data['CompanyCategory'];
        }

        if (!empty($data['CompanyStatus'])) {
            $this->companyStatus = $data['CompanyStatus'];
        }

        if (!empty($data['CountryOfOrigin'])) {
            $this->countryOfOrigin = $data['CountryOfOrigin'];
        }

        if (!empty($data['RegAddress'])) {
            $this->regAddress = new RegisteredAddress($data['RegAddress']);
        }

        if (!empty($data['Accounts'])) {
            $this->accounts = new Accounts($data['Accounts']);
        }

        if (!empty($data['Returns'])) {
            $this->returns = new Returns($data['Returns']);
        }

        if (!empty($data['SICCodes'])) {
            $this->sicCodes = new SicCodes($data['SICCodes']);
        }

        if (!empty($data['PreviousNames'])) {
            $this->previousNames = new PreviousNames($data['PreviousNames']);
        }

        if (!empty($data['IncorporationDate'])) {
            $this->incorporationDate = DateTime::createFromFormat('d/m/Y', $data['IncorporationDate']);
        }

        if (!empty($data['RegistrationDate'])) {
            $this->registrationDate = DateTime::createFromFormat('d/m/Y', $data['RegistrationDate']);
        }

        if (!empty($data['DissolutionDate'])) {
            $this->dissolutionDate = DateTime::createFromFormat('d/m/Y', $data['DissolutionDate']);
        }

        if (!empty($data['Mortgages'])) {
            $this->mortgages = new Mortgages($data['Mortgages']);
        }
    }

    /**
     * @return DateTime
     */
    public function getDissolutionDate()
    {
        return $this->dissolutionDate;
    }

    /**
     * @return DateTime
     */
    public function getRegistrationDate()
    {
        return $this->registrationDate;
    }

    /**
     * @return Accounts
     */
    public function getAccounts()
    {
        return $this->accounts;
    }

    /**
     * @return string
     */
    public function getCompanyCategory()
    {
        return $this->companyCategory;
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @return string
     */
    public function getCompanyNumber()
    {
        return $this->companyNumber;
    }

    /**
     * @return string
     */
    public function getCompanyStatus()
    {
        return $this->companyStatus;
    }

    /**
     * @return string
     */
    public function getCountryOfOrigin()
    {
        return $this->countryOfOrigin;
    }

    /**
     * @return DateTime
     */
    public function getIncorporationDate()
    {
        return $this->incorporationDate;
    }

    /**
     * @return Mortgages
     */
    public function getMortgages()
    {
        return $this->mortgages;
    }

    /**
     * @return PreviousNames
     */
    public function getPreviousNames()
    {
        return $this->previousNames;
    }

    /**
     * @return RegisteredAddress
     */
    public function getRegAddress()
    {
        return $this->regAddress;
    }

    /**
     * @return Returns
     */
    public function getReturns()
    {
        return $this->returns;
    }

    /**
     * @return SicCodes
     */
    public function getSicCodes()
    {
        return $this->sicCodes;
    }
}
