<?php

namespace CompaniesHouse\Data\Entities;

use Nette\Object;

class SicCodes extends Object
{
    const NONE_SUPPLIED = 'None Supplied';

    /**
     * @var array
     */
    private $sicText;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        foreach ($data as $property => $value) {
            $property = lcfirst($property);

            if ($this->getReflection()->hasProperty($property)) {
                $this->$property = $value;
            }
        }
    }

    /**
     * @return array
     */
    public function getSicText()
    {
        return $this->sicText;
    }
} 
