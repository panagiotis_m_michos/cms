<?php

namespace CompaniesHouse\Data\Entities;

use Nette\Object;

class RegisteredAddress extends Object
{
    /**
     * @var string
     */
    private $addressLine1;

    /**
     * @var string
     */
    private $addressLine2;

    /**
     * @var string
     */
    private $postTown;

    /**
     * @var string
     */
    private $county;

    /**
     * @var string
     */
    private $postcode;

    /**
     * @var string
     */
    private $careOf;

    /**
     * @var string
     */
    private $pobox;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        foreach ($data as $property => $value) {
            $property = lcfirst($property);

            if ($this->getReflection()->hasProperty($property)) {
                $this->$property = $value;
            }
        }

        if (isset($data['POBox'])) {
            $this->pobox = $data['POBox'];
        }
    }

    /**
     * @return string
     */
    public function getCareOf()
    {
        return $this->careOf;
    }

    /**
     * @return string
     */
    public function getPobox()
    {
        return $this->pobox;
    }

    /**
     * @return string
     */
    public function getAddressLine1()
    {
        return $this->addressLine1;
    }

    /**
     * @return string
     */
    public function getAddressLine2()
    {
        return $this->addressLine2;
    }

    /**
     * @return string
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * @return string
     */
    public function getPostTown()
    {
        return $this->postTown;
    }

    /**
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }
} 