<?php

namespace CompaniesHouse\Data\Entities;

use DateTime;
use Nette\Object;

class PreviousNameItem extends Object
{
    /**
     * @var DateTime
     */
    private $conDate;

    /**
     * @var string
     */
    private $companyName;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        if (isset($data['CONDate'])) {
            $this->conDate = DateTime::createFromFormat('d/m/Y', $data['CONDate']);
        }

        if (isset($data['CompanyName'])) {
            $this->companyName = $data['CompanyName'];
        }
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @return DateTime
     */
    public function getConDate()
    {
        return $this->conDate;
    }
} 