<?php

namespace CompaniesHouse\Data\Entities;

use DateTime;
use Nette\Object;

class Returns extends Object
{
    /**
     * @var DateTime
     */
    private $nextDueDate;

    /**
     * @var DateTime
     */
    private $lastMadeUpDate;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        foreach ($data as $property => $value) {
            $property = lcfirst($property);

            if ($this->getReflection()->hasProperty($property)) {
                $this->$property = DateTime::createFromFormat('d/m/Y', $value);

                if (is_bool($this->$property)) {
                    $this->$property = $value;
                }
            }
        }
    }

    /**
     * @return DateTime
     */
    public function getLastMadeUpDate()
    {
        return $this->lastMadeUpDate;
    }

    /**
     * @return DateTime
     */
    public function getNextDueDate()
    {
        return $this->nextDueDate;
    }
} 