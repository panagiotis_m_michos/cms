<?php

namespace CompaniesHouse\Data\Requests;

use CompaniesHouse\Data;
use CompaniesHouse\Data\Entities\CompanyDetails;
use CompaniesHouse\Data\Exceptions\CompanyNotFoundException;
use CompaniesHouse\Data\Exceptions\InvalidArgumentException;
use CompaniesHouse\Data\Exceptions\ResponseException;
use HttpClient\HttpMethod;
use HttpClient\HttpStatusCode;
use HttpClient\Interfaces\IHttpRequest;
use HttpClient\Interfaces\IHttpResponse;

class CompanyDetailsRequest implements IHttpRequest
{
    /**
     * @var string
     */
    private $companyNumber;

    /**
     * @param string $companyNumber
     */
    public function __construct($companyNumber)
    {
        $this->companyNumber = $companyNumber;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        $baseUri = 'http://data.companieshouse.gov.uk/doc/company/{companyNumber}.json';
        return str_replace('{companyNumber}', $this->companyNumber, $baseUri);
    }

    /**
     * @return HttpMethod
     */
    public function getMethod()
    {
        return new HttpMethod(HttpMethod::GET);
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return '';
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return array();
    }

    /**
     * @param IHttpResponse $response
     * @return CompanyDetails
     * @throws CompanyNotFoundException
     * @throws InvalidArgumentException
     * @throws ResponseException
     */
    public function buildResponse(IHttpResponse $response)
    {
        $statusCode = $response->getStatusCode();

        if ($statusCode->is(HttpStatusCode::NOT_FOUND)) {
            throw CompanyNotFoundException::common($this->companyNumber);
        } elseif (!$statusCode->is(HttpStatusCode::OK)) {
            throw ResponseException::common($statusCode);
        }

        $jsonData = json_decode($response->getBody(), TRUE);

        if (!isset($jsonData['primaryTopic'])) {
            throw InvalidArgumentException::wrongJsonSupplied('primaryTopic');
        }

        return new CompanyDetails($jsonData['primaryTopic'], $statusCode);
    }
}
