<?php

namespace CompaniesHouse\Data;

use CompaniesHouse\Data\Entities\CompanyDetails;
use HttpClient\HttpClient;

class Data extends HttpClient
{
    /**
     * Fetches company details from Companies House URIs service
     *
     * @param string $companyNumber
     * @return CompanyDetails
     */
    public function getCompanyDetails($companyNumber)
    {
        $request = new Requests\CompanyDetailsRequest($companyNumber);
        return $this->sendRequest($request);
    }
} 