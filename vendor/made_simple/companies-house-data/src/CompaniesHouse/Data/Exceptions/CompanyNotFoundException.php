<?php

namespace CompaniesHouse\Data\Exceptions;

use Exception;

class CompanyNotFoundException extends DataException
{
    /**
     * @param string $companyNumber
     * @param Exception $prev
     * @return CompanyNotFoundException
     */
    public static function common($companyNumber, Exception $prev = NULL)
    {
        return new self("Company number {$companyNumber} was not found in Companies House!", 0 , $prev);
    }
}