<?php

namespace CompaniesHouse\Data\Exceptions;

use Exception;

class InvalidArgumentException extends DataException
{
    /**
     * @param array|string $missingFields
     * @param Exception $prev
     * @return InvalidArgumentException
     */
    public static function wrongArraySupplied($missingFields = NULL, Exception $prev = NULL)
    {
        $text = "Wrong array supplied.";

        if (is_array($missingFields)) {
            $text .= ' Missing fields:' . join(',', $missingFields);
        } elseif (is_string($missingFields)) {
            $text .= ' Missing field:' . $missingFields;
        }

        return new self ($text, 0, $prev);
    }

    /**
     * @param array|string $missingFields
     * @param Exception $prev
     * @return InvalidArgumentException
     */
    public static function wrongJsonSupplied($missingFields = NULL, Exception $prev = NULL)
    {
        $text = "Wrong json object supplied.";

        if (is_array($missingFields)) {
            $text .= ' Missing fields:' . join(',', $missingFields);
        } elseif (is_string($missingFields)) {
            $text .= ' Missing field:' . $missingFields;
        }

        return new self ($text, 0, $prev);
    }
}
