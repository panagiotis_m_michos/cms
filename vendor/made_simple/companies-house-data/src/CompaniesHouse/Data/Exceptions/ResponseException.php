<?php

namespace CompaniesHouse\Data\Exceptions;

use Exception;
use HttpClient\HttpStatusCode;

class ResponseException extends DataException
{
    /**
     * @param HttpStatusCode $statusCode
     * @param Exception $prev
     * @return ResponseException
     */
    public static function common(HttpStatusCode $statusCode, Exception $prev = NULL)
    {
        return new self("Companies house server responded with status code {$statusCode->getValue()}!", 0 , $prev);
    }
}
