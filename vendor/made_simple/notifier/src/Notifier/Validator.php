<?php

namespace Notifier;

use Notifier\Exceptions\ValidationError;
use Symfony\Component\Validator\ValidatorInterface;


class Validator implements IValidator
{

    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function validate($object)
    {
        $violations = $this->validator->validate($object);
        if ($violations->count() === 0) {
            return true;
        }
        foreach ($violations as $violation) {
            throw new ValidationError($violation->getMessage());
        }
    }
}
