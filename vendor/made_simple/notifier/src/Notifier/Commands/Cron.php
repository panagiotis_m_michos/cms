<?php

namespace Notifier\Commands;

use Notifier\Models\Task;
use Notifier\Services\EventService;
use Notifier\Config;
use DateTime;
use UnexpectedValueException;
use InvalidArgumentException;
use LogicException;
use RuntimeException;

class Cron
{

    private $eventService;

    public function __construct(EventService $eventService)
    {
        $this->eventService = $eventService;
    }

    /**
     * execute
     *
     * @param Task $task
     */
    public function execute(Task $task)
    {
        $info = $task->getInfo();
        $task->setDtc(new DateTime());
        try {
            if (!isset($info['when'], $info['event'])) {
                throw new UnexpectedValueException(
                    sprintf('Info object missing for task. It must contain keys: when, event. Current keys `%s`', implode(', ', array_keys($info)))
                );
            }
            $shouldBeExecuted = new DateTime($info['when']);
            $event = $this->eventService->getLastEvent($info['event']);
            $lastExecuted = $event->getDtc();
            if ($lastExecuted < $shouldBeExecuted) {
                throw new UnexpectedValueException(
                    sprintf('`%s` should have been executed later than `%s`. Last execution found `%s`', 
                        $task->getTitle(), $shouldBeExecuted->format(Config::DATE_UK), 
                        $lastExecuted->format(Config::DATE_UK))
                );
            }
            if ($event->isFailure()) {
                throw new LogicException($event->getMessage());
            }
            $task->setStatus(true);
            $task->setMessage($event->getMessage());
        }
        catch(RuntimeException $e) {
            $task->setStatus(false);
            $task->setMessage($e->getMessage());
        }
        catch (LogicException $e) {
            $task->setStatus(false);
            $task->setMessage($e->getMessage());
        }
    }
}
