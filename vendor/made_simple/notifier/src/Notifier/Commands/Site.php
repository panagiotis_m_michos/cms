<?php

namespace Notifier\Commands;

use Notifier\Models\Task;
use Notifier\Config;
use DateTime;
use UnexpectedValueException;

class Site
{

    public function execute(Task $task)
    {
        $info = $task->getInfo();
        $now = new DateTime();
        $task->setDtc($now);
        try {
            if (!isset($info['url'])) {
                throw new UnexpectedValueException(
                    sprintf('Url parameter not set for task `%d`', $task->getTaskId())
                );
            }
            if (!filter_var($info['url'], FILTER_VALIDATE_URL)) {
                throw new UnexpectedValueException(
                    sprintf('Url parameter is not valid `%s`', $info['url'])
                );
            }
            $this->getHead($info['url']);
            $task->setStatus(true);
            $task->setMessage(sprintf('Site is live. Last checked `%s`', $now->format(Config::DATE_UK)));
        }
        catch (UnexpectedValueException $e) {
            $task->setStatus(false);
            $task->setMessage($e->getMessage());
        }
    }

    public function getHead($url)
    {
        $ctx = stream_context_create(array(
            'http' => array(
                'timeout' => 5
                )
            )
        );
        $output = @file_get_contents($url, 0, $ctx);
        if (empty($output)) {
            throw new UnexpectedValueException("Unable to retrieve website from `$url`");
        }
        return $output;
    }
}
