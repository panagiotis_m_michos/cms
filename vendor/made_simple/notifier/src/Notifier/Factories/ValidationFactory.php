<?php

namespace Notifier\Factories;

use Notifier\Validator;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Symfony\Component\Validator\Validation;
use ReflectionClass;

class ValidationFactory
{
    private static $registered = false;

    public static function register($path)
    {
        self::$registered = true;
        AnnotationRegistry::registerAutoloadNamespace("Symfony\Component\Validator\Constraints", $path);
    }
    /**
     * @return ValidationInferface
     */
    public static function create()
    {
        self::checkAnnotationRegister();
        $validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping()
            ->getValidator();
        return new Validator($validator);
    }

    private static function checkAnnotationRegister()
    {
        if (!self::$registered) {
            $className = 'Symfony\Component\Validator\Constraints\All';
            //try to find symfony
            if (class_exists($className)) {
                $reflection = new ReflectionClass($className);
                $constrainPath = dirname(dirname(dirname(dirname(dirname($reflection->getFileName())))));
                self::register($constrainPath);
            } else {
                throw new LogicException('You need to register symfony validation annotations in order to use this library. ValidationFactory::register($pathToSymfony)');
            }

        }
    }
}
