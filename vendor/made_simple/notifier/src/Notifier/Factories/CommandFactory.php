<?php

namespace Notifier\Factories;

use Notifier\Models\Task;
use Notifier\Commands\Site;
use Notifier\Commands\Cron;
use Notifier\Services\EventService;
use InvalidArgumentException;

class CommandFactory
{

    /**
     * @var EventService
     */
    private $eventService;

    /**
     * __construct
     *
     * @param EventService $eventService
     */
    public function __construct(EventService $eventService)
    {
        $this->eventService = $eventService;
    }

    /**
     * create command
     *
     * @param Task $task
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function create(Task $task)
    {
        switch ($task->getCommandName()) {
        case "site" : return new Site();
        case "cron" : return new Cron($this->eventService);
        default: throw new InvalidArgumentException("Command `{$task->getCommandName()}` not found!");
        }
    }
}
