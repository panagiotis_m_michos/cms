<?php

namespace Notifier\Factories;

use DibiConnection as Connection;
use Notifier\Repositories\TaskRepository;
use Notifier\Repositories\ProjectRepository;
use Notifier\Repositories\EventRepository;
use Notifier\Hydrators\TaskHydrator;
use Notifier\Hydrators\ProjectHydrator;
use Notifier\Hydrators\EventHydrator;
use InvalidArgumentException;

class RepositoryFactory
{


    /**
     * @var Connection
     */
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function create($repositoryName)
    {
        switch ($repositoryName) {
        case 'task' : return new TaskRepository($this->connection, new TaskHydrator(), new ProjectHydrator());
        case 'project' : return new ProjectRepository($this->connection, new ProjectHydrator(), new TaskHydrator());
        case 'event' : return new EventRepository($this->connection, new EventHydrator());
        default: throw new InvalidArgumentException("Repository `$repositoryName` does not exists!");
        }
    }

}
