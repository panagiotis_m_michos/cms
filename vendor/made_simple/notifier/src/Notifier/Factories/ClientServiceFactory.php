<?php

namespace Notifier\Factories;

use Notifier\Services\EventService;
use Notifier\Hydrators\EventHydrator;
use Notifier\Repositories\ClientEventRepository;
use Guzzle\Http\Client;
use Guzzle\Plugin\Oauth\OauthPlugin;
use InvalidArgumentException;
use Notifier\Plugins\CronNotifier;
use Notifier\IValidator;

class ClientServiceFactory
{
    /**
     * @var IValidator
     */
    private $validator;

    public function __construct(IValidator $validator)
    {
        $this->validator = $validator;
    }
    
    /**
     * create client object for communicating with webservices
     *
     * @param string $url
     * @param array $oauthConfig
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function create($serviceName, $url, $defaultParameters = array(), $oauthConfig = array())
    {
        $client = new Client($url, ['curl.options' => ['CURLOPT_SSLVERSION' => 6]]);
        if (!empty($defaultParameters)) {
            $client->setDefaultOption('query', $defaultParameters);
        }
        if (!empty($oauthConfig)) {
            $client->addSubscriber(new OauthPlugin($oauthConfig));
        }
        switch ($serviceName) {
            case "event": return new EventService(new ClientEventRepository(new EventHydrator(), $client), $this->validator);
            case "notifier": return new CronNotifier(new ClientEventRepository(new EventHydrator(), $client), $this->validator);
            default: throw new InvalidArgumentException("Service $serviceName does not exist!");
        }
    }
}
