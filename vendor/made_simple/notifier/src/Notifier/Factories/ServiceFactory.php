<?php

namespace Notifier\Factories;

use Notifier\Services\TaskService;
use Notifier\Services\ProjectService;
use Notifier\Services\EventService;
use Notifier\IValidator;


class ServiceFactory
{
    /**
     * @var RepositoryFactory
     */
    private $repositoryFactory;

    private $validator;

    private $commandFactory;

    public function __construct(RepositoryFactory $repositoryFactory, IValidator $validator, CommandFactory $commandFactory = null)
    {
        $this->repositoryFactory = $repositoryFactory;
        $this->validator = $validator;
        $this->commandFactory = $commandFactory;
    }

    public function create($serviceName)
    {
        switch ($serviceName) {
            case 'task' : return new TaskService($this->repositoryFactory->create('task'), $this->validator, $this->commandFactory);
            case 'project' : return new ProjectService($this->repositoryFactory->create('project'));
            case 'event' : return new EventService($this->repositoryFactory->create('event'), $this->validator);
            default: throw new InvalidArgumentException("Service `$serviceName` does not exists!");
        }
    }
}
