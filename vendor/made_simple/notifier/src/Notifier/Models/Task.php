<?php

namespace Notifier\Models;

use Symfony\Component\Validator\Constraints as Assert;
use DateTime;

class Task
{

    private $taskId;

    /**
     * @Assert\NotBlank(message="title should not be empty")
     */
    private $title;

    private $description;

    /**
     * @var bool
     **/
    private $status;

    /**
     * @Assert\NotBlank(message="commandName should not be empty")
     */
    private $commandName;

    /**
     * @var array
     **/
    private $info;

    private $message;

    /**
     * @var Project
     */
    private $project;

    /**
     * @Assert\NotBlank(message="projectId should not be empty")
     * @var int
     */
    private $projectId;

    /**
     * @var DateTime
     **/
    private $dtc;


    public function __construct($title, $commandName, $projectId)
    {
        $this->title = $title;
        $this->commandName = $commandName;
        $this->projectId = $projectId;
    }


    public function getId()
    {
        return $this->getTaskId();
    }

    public function getTaskId()
    {
        return $this->taskId;
    }

    public function setTaskId($taskId)
    {
        $this->taskId = $taskId;
    }

    /**
     * Get title.
     *
     * @return title.
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title.
     *
     * @param title the value to set.
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get description.
     *
     * @return description.
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description.
     *
     * @param description the value to set.
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get status.
     *
     * @return status.
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status.
     *
     * @param status the value to set.
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get commandName.
     *
     * @return commandName.
     */
    public function getCommandName()
    {
        return $this->commandName;
    }

    /**
     * Set commandName.
     *
     * @param commandName the value to set.
     */
    public function setCommandName($commandName)
    {
        $this->commandName = $commandName;
    }

    /**
     * Get info.
     *
     * @return info.
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Set info.
     *
     * @param info the value to set.
     */
    public function setInfo($info)
    {
        $this->info = $info;
    }

    /**
     * Get message.
     *
     * @return message.
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set message.
     *
     * @param message the value to set.
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * Get project.
     *
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set project.
     *
     * @param Project $project
     */
    public function setProject($project)
    {
        $this->project = $project;
    }

    /**
     * Get dtc.
     *
     * @return dtc.
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * Set dtc.
     *
     * @param dtc the value to set.
     */
    public function setDtc($dtc)
    {
        $this->dtc = $dtc;
    }

    /**
     * Get projectId.
     *
     * @return projectId.
     */
    public function getProjectId()
    {
        return $this->projectId;
    }

    /**
     * Set projectId.
     *
     * @param projectId the value to set.
     */
    public function setProjectId($projectId)
    {
        $this->projectId = $projectId;
    }
}
