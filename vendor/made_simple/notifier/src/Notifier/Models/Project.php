<?php

namespace Notifier\Models;

use Symfony\Component\Validator\Constraints as Assert;

class Project
{

    /**
     * @var int
     **/
    private $projectId;

    /**
     * @var string
     **/
    private $title;

    /**
     * @var array<Task>
     **/
    private $tasks = array();

    public function __construct($title)
    {
        $this->title = $title;
    }

    public function getId()
    {
        return $this->getProjectId();
    }

    /**
     * Get projectId.
     *
     * @return projectId.
     */
    public function getProjectId()
    {
        return $this->projectId;
    }

    /**
     * Set projectId.
     *
     * @param projectId the value to set.
     */
    public function setProjectId($projectId)
    {
        $this->projectId = $projectId;
    }

    /**
     * Get title.
     *
     * @return title.
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title.
     *
     * @param title the value to set.
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get tasks.
     *
     * @return array<Task>
     */
    public function getTasks()
    {
        return $this->tasks;
    }

    /**
     * Set tasks.
     *
     * @param array<Task>
     */
    public function setTasks($tasks)
    {
        $this->tasks = $tasks;
    }

    public function addTask(Task $task)
    {
        $this->tasks[] = $task;
    }
}
