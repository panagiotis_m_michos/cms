<?php

namespace Notifier\Models;

use Symfony\Component\Validator\Constraints as Assert;
use DateTime;

class Event
{
    const SUCCESS = 'SUCCESS';
    const WARNING = 'WARNING';
    const FAILURE = 'FAILURE';

    private $eventId;


    /**
     * @Assert\NotBlank(message="eventKey should not be empty")
     */
    private $eventKey;

    private $statusId;

    /**
     * @Assert\NotBlank(message="objectId should not be empty")
     */
    private $objectId;

    private $message;

    /**
     * @Assert\NotBlank(message="dtc should not be empty")
     * @Assert\DateTime(message="dtc should be a datetime")
     * @var DateTime
     **/
    private $dtc;

    public function __construct($eventKey, $objectId, DateTime $dtc)
    {
        $this->eventKey = $eventKey;
        $this->objectId = $objectId;
        $this->dtc = $dtc;
    }


    public function getId()
    {
        return $this->getEventId();
    }

    /**
     * Get id.
     *
     * @return id.
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    /**
     * Set id.
     *
     * @param id the value to set.
     */
    public function setEventId($eventId)
    {
        $this->eventId = $eventId;
    }

    /**
     * Get eventKey.
     *
     * @return eventKey.
     */
    public function getEventKey()
    {
        return $this->eventKey;
    }

    /**
     * Set eventKey.
     *
     * @param eventKey the value to set.
     */
    public function setEventKey($eventKey)
    {
        $this->eventKey = $eventKey;
    }

    /**
     * Get statusId.
     *
     * @return statusId.
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * Set statusId.
     *
     * @param statusId the value to set.
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;
    }

    public function isFailure()
    {
        return $this->statusId === self::FAILURE;
    }

    /**
     * Get objectId.
     *
     * @return objectId.
     */
    public function getObjectId()
    {
        return $this->objectId;
    }

    /**
     * Set objectId.
     *
     * @param objectId the value to set.
     */
    public function setObjectId($objectId)
    {
        $this->objectId = $objectId;
    }

    /**
     * Get message.
     *
     * @return message.
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set message.
     *
     * @param message the value to set.
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * Get dtc.
     *
     * @return dtc.
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * Set dtc.
     *
     * @param dtc the value to set.
     */
    public function setDtc($dtc)
    {
        $this->dtc = $dtc;
    }
}
