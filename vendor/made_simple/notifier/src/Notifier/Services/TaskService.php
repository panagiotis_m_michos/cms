<?php

namespace Notifier\Services;

use Notifier\Repositories\IRepository;
use Notifier\Factories\CommandFactory;
use Notifier\Models\Task;
use Notifier\IValidator;
use Notifier\Hydrators\TaskHydrator;

class TaskService
{

    protected $repository;

    protected $valitor;

    private $commandFactory;


    /**
     * __construct
     *
     * @param IRepository $repository
     * @param CommandFactory $commandFactory
     */
    public function __construct(IRepository $repository, IValidator $validator, CommandFactory $commandFactory)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->commandFactory = $commandFactory;
    }

    /**
     * create task object from array
     *
     * @param array $fields
     */
    public function create(array $fields)
    {
        $taskHydrator = new TaskHydrator();
        $task = $taskHydrator->fromArray($fields);
        $this->validator->validate($task);
        return $task;
    }


    public function getTasks()
    {
        return $this->repository->findAll();
    }

    public function getTask($taskId)
    {
        return $this->repository->find($taskId);
    }

    public function execute($tasks)
    {
        $tasks = is_array($tasks) ? $tasks : array($tasks);
        foreach ($tasks as $task) {
            $command = $this->commandFactory->create($task);
            $command->execute($task);
        }
    }

    public function save(Task $task)
    {
        return $this->repository->save($task);
    }

    public function remove(Task $task)
    {
        return $this->repository->remove($task);
    }

    public function toArray($tasks)
    {
        $taskHydrator = new TaskHydrator();
        if (is_array($tasks)) {
            $data = array();
            foreach ($tasks as $task) {
                $data[] = $taskHydrator->toArray($task);
            }
            return array('tasks' => $data);
        } else {
            return array('task' => $taskHydrator->toArray($tasks));
        }
    }
}
