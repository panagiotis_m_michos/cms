<?php

namespace Notifier\Services;

use Notifier\Models\Event;
use Notifier\Repositories\IRepository;
use DateTime;
use DateInterval;
use InvalidArgumentException;
use Notifier\IValidator;
use Notifier\Hydrators\EventHydrator;

class EventService
{

    /**
     * @var IRepository
     **/
    protected $repository;

    protected $validator;


    public function __construct(IRepository $repository, IValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * create event object from array
     *
     * @param array $fields
     */
    public function create(array $fields)
    {
        $eventHydrator = new EventHydrator();
        $event = $eventHydrator->fromArray($fields);
        $this->validator->validate($event);
        return $event;
    }

    public function getEvents()
    {
        return $this->repository->findAll();
    }

    public function getEventsBy($params)
    {
        return $this->repository->findBy($params);
    }

    public function save(Event $event)
    {
        $this->repository->save($event);
    }

    /**
     * @param string $eventKey
     * @param string $objectId
     */
    public function getEvent($eventKey, $objectId)
    {
        try {
            return $this->repository->findOneBy(array('eventKey' => $eventKey,
                'objectId' => $objectId));
        } catch (InvalidArgumentException $e) {
            return null;
        }
    }

    public function getLastEvent($eventKey)
    {
        return $this->repository->findOneBy(array('eventKey' => $eventKey));
    }

    /**
     * @param string $eventKey
     * @param int $objectId
     */
    public function notify($eventKey, $objectId, $message = null)
    {
        $event = new Event($eventKey, $objectId, new DateTime());
        $event->setMessage($message);
        $this->validator->validate($event);
        $this->repository->save($event);
    }

    /**
     * did particular event occured
     *
     * @param string $eventKey
     * @param int $objectId
     * @return bool
     */
    public function eventOccurred($eventKey, $objectId)
    {
        $event = $this->getEvent($eventKey, $objectId);
        return $event ? true : false;
    }

    /**
     * did particular event occurred in a specified date interval
     *
     * @param string $eventKey
     * @param int $objectId
     * @param \DateInterval $dateInterval
     * @return boolean
     */
    public function eventOccurredIn($eventKey, $objectId, DateInterval $dateInterval)
    {
        $event = $this->getEvent($eventKey, $objectId);
        if (!$event) {
            return false;
        }
        $now = new \DateTime();
        $now->setTime(0, 0 , 0);
        $dateOccurred = clone $event->getDtc();
        $dateOccurred->setTime(0, 0 , 0);
        return $now->sub($dateInterval) <= $dateOccurred;
    }

    public function toArray($events)
    {
        $eventHydrator = new EventHydrator();
        if (is_array($events)) {
            $data = array();
            foreach ($events as $event) {
                $data[] = $eventHydrator->toArray($event);
            }
            return array('events' => $data);
        } else {
            return array('event' => $eventHydrator->toArray($events));
        }
    }
}
