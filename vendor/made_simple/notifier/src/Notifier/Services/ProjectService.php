<?php

namespace Notifier\Services;

use Notifier\Repositories\IRepository;
use Notifier\Factories\CommandFactory;
use Notifier\Hydrators\ProjectHydrator;

class ProjectService
{

    protected $repository;


    /**
     * __construct
     *
     * @param IRepository $repository
     */
    public function __construct(IRepository $repository)
    {
        $this->repository = $repository;
    }


    public function getProjects()
    {
        return $this->repository->findAll();
    }

    public function getProject($projectId)
    {
        return $this->repository->find($projectId);
    }

    public function toArray($projects)
    {
        $projectHydrator = new ProjectHydrator();
        $data = array();
        $tasks = is_array($projects) ? $projects : array($projects);
        foreach ($projects as $project) {
            $data[] = $projectHydrator->toArray($project);
        }
        return array('projects' => $data);
    }
}
