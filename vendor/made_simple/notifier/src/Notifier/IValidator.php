<?php

namespace Notifier;

interface IValidator {
    public function validate($object);
}
