<?php

namespace Notifier\Repositories;

interface IRepository
{

    /**
     * find object by id
     *
     * @param int $id
     * @return mixed
     */
    public function find($id);

    /**
     * find ojects by params provided
     *
     * @param array $params
     * @return array<mixed>
     */
    public function findBy($params);

    /**
     * find one object by params provided
     *
     * @param array $params
     * @return mixed
     */
    public function findOneBy($params);

    /**
     * find all object
     *
     * @return array<mixed>
     */
    public function findAll();

    /**
     * save model
     *
     * @param mixed $model
     */
    public function save($model);

    /**
     * remove model
     *
     * @param mixed $model
     */
    public function remove($model);

}
