<?php

namespace Notifier\Repositories;

use Notifier\Hydrators\IHydrator;
use DibiConnection as Connection;
use InvalidArgumentException;

abstract class RepositoryAbstract implements IRepository
{

    protected $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function findAll()
    {
        $objects = array();
        $rows = $this->findAllTable(static::TABLE, static::PRIMARY_KEY);
        foreach ($rows as $row) {
            $objects[] = $this->mapToObject($row);
        }
        return $objects;
    }

    public function findBy($params)
    {
        $objects = array();
        $rows = $this->findByTable(static::TABLE, $params, static::PRIMARY_KEY);
        foreach ($rows as $row) {
             $objects[] = $this->mapToObject($row);
        }
        return $objects;
    }

    /**
     * findOneBy
     *
     * @param array $params
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function findOneBy($params)
    {
        $row = $this->findOneByTable(static::TABLE, $params, static::PRIMARY_KEY);
        if (!$row) {
            throw new InvalidArgumentException('Row with ' . json_encode($params) . ' not found!');
        }
        return $this->mapToObject($row);
    }


    /**
     * find
     *
     * @param int $id
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function find($id)
    {
        $row = $this->findTable(static::TABLE, static::PRIMARY_KEY, $id);
        if (!$row) {
            throw new InvalidArgumentException('Row with id ' . $id . ' not found!');
        }
        return $this->mapToObject($row);
    }


    /**
     * remove
     *
     * @param mixed $model
     */
    public function remove($model)
    {
        return $this->connection->delete(static::TABLE)->where(static::PRIMARY_KEY . ' = %i', $model->getId())->execute();
    }


    protected function findTable($table, $keyName, $id)
    {
        return $this->connection->select('*')->from($table)->where($keyName .' = %i', $id)->fetch();
    }

    protected function findByTable($table, $params, $orderBy)
    {
        return $this->connection->select('*')->from($table)->where($params)->orderBy($orderBy)->desc();
    }

    protected function findOneByTable($table, $params, $orderBy)
    {
        return $this->connection->select('*')->from($table)->where($params)->orderBy($orderBy)->desc()->fetch();
    }

    protected function findAllTable($table, $orderBy)
    {
        return $this->connection->select('*')->from($table)->orderBy($orderBy)->desc();
    }

    /**
     * map row to object
     *
     * @param array $row
     */
    protected abstract function mapToObject($row);

}
