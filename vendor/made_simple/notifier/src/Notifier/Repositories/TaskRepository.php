<?php

namespace Notifier\Repositories;

use Notifier\Hydrators\TaskHydrator;
use Notifier\Hydrators\ProjectHydrator;
use DibiConnection as Connection;

class TaskRepository extends RepositoryAbstract
{

    const TABLE = 'tasks';

    const PRIMARY_KEY = 'taskId';

    private $taskHydrator;

    private $projectHydrator;


    public function __construct(Connection $connection, TaskHydrator $taskHydrator, ProjectHydrator $projectHydrator)
    {
        parent::__construct($connection);
        $this->taskHydrator = $taskHydrator;
        $this->projectHydrator = $projectHydrator;
    }

    public function mapToObject($row)
    {
        $task = $this->taskHydrator->fromArray((array)$row);
        $projectRow = $this->findTable(ProjectRepository::TABLE, ProjectRepository::PRIMARY_KEY, $row['projectId']);
        $project = $this->projectHydrator->fromArray((array)$projectRow);
        $task->setProject($project);
        return $task;
    }

    public function save($model)
    {
        $arr = $this->taskHydrator->toDbArray($model);
        if ($model->getId()) {
            $this->connection->query('UPDATE ' . static::TABLE . ' SET ', $arr, ' WHERE taskId=%i', $model->getId());
        } else {
            $this->connection->query('INSERT INTO ' . static::TABLE, $arr);
            $model->setTaskId($this->connection->getInsertId());
        }
    }
}
