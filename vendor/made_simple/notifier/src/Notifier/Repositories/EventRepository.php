<?php

namespace Notifier\Repositories;

use DibiConnection as Connection;
use Notifier\Hydrators\EventHydrator;

class EventRepository extends RepositoryAbstract
{

    const TABLE = 'events';

    const PRIMARY_KEY = 'eventId';


    private $eventHydrator;


    public function __construct(Connection $connection, EventHydrator $eventHydrator)
    {
        parent::__construct($connection);
        $this->eventHydrator = $eventHydrator;
    }

    public function mapToObject($row)
    {
        $event = $this->eventHydrator->fromArray((array)$row);
        return $event;
    }

    public function save($model)
    {
        $arr = $this->eventHydrator->toDbArray($model);
        if ($model->getId()) {
            $this->connection->query('UPDATE ' . static::TABLE . ' SET ', $arr, ' WHERE eventId=%i', $model->getId());
        } else {
            $this->connection->query('INSERT INTO ' . static::TABLE, $arr);
            $model->setEventId($this->connection->getInsertId());
        }
    }


}
