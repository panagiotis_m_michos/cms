<?php

namespace Notifier\Repositories;

use Notifier\Models\Event;
use Guzzle\Http\Client;
use Notifier\Hydrators\EventHydrator;
use BadMethodCallException;
use UnexpectedValueException;

class ClientEventRepository implements IRepository
{

    /**
     * @var IHydrator
     */
    private $hydrator;

    /**
     * @var Client
     */
    private $client;

    /**
     * __construct
     *
     * @param EventHydrator $hydrator
     * @param Client $client
     */
    public function __construct(EventHydrator $hydrator, Client $client)
    {
        $this->hydrator = $hydrator;
        $this->client = $client;
    }

    /**
     * @param int $id
     * @return Event
     */
    public function find($id)
    {
        $response = $this->client->get('events/' . $id)->send()->json();
        $event = $this->hydrator->fromArray($response);
        return $event;
    }

    public function findAll()
    {
        throw new BadMethodCallException("findAll method not implemented!");
    }

    /**
     * find one event by params
     *
     * @param array $params
     * return Event
     */
    public function findOneBy($params)
    {
        $response = $this->client->get('events/', null, array('query' => $params))->send()->json();
        if (empty($response['events'])) {
            throw new UnexpectedValueException('Event with `' . json_encode($params) . '` not found!');
        }
        $eventArr = reset($response['events']);
        $event = $this->hydrator->fromArray($eventArr);
        return $event;
    }

    public function findBy($params)
    {
        throw new BadMethodCallException("findBy method not implemented!");
    }

    /**
     * @param Event $event
     */
    public function save($event)
    {
        $arr = $this->hydrator->toArray($event);
        $request = $this->client->post('events/', array(), $arr);
        $request->send();
    }

    public function remove($model)
    {
        throw new BadMethodCallException("remove method not implemented!");
    }
}
