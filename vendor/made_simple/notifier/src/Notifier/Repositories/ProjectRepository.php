<?php

namespace Notifier\Repositories;

use Notifier\Hydrators\ProjectHydrator;
use Notifier\Hydrators\TaskHydrator;
use DibiConnection as Connection;

class ProjectRepository extends RepositoryAbstract
{

    const TABLE = 'projects';

    const PRIMARY_KEY = 'projectId';

    private $projectHydrator;

    private $taskHydrator;


    public function __construct(Connection $connection, ProjectHydrator $projectHydrator, TaskHydrator $taskHydrator)
    {
        parent::__construct($connection);
        $this->projectHydrator = $projectHydrator;
        $this->taskHydrator = $taskHydrator;
    }

    public function mapToObject($row)
    {
        $project = $this->projectHydrator->fromArray((array)$row);
        $taskRows = $this->findByTable(TaskRepository::TABLE, array(self::PRIMARY_KEY => $project->getProjectId()), TaskRepository::PRIMARY_KEY);
        foreach ($taskRows as $taskRow) {
            $task = $this->taskHydrator->fromArray((array)$taskRow);
            $task->setProject($project);
            $project->addTask($task);
        }
        return $project;
    }

    public function save($project)
    {
    }

}
