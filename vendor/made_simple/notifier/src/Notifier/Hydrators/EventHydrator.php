<?php

namespace Notifier\Hydrators;

use Notifier\Models\Event;
use DateTime;

class EventHydrator implements IHydrator
{

    /**
     * @param array $arr
     **/
    public function fromArray(array $arr)
    {
        $event = new Event(
            isset($arr['eventKey']) ? $arr['eventKey'] : null,
            isset($arr['objectId']) ? $arr['objectId'] : null,
            isset($arr['dtc']) ? new DateTime($arr['dtc']) : new DateTime()
        );
        $event->setEventId(isset($arr['eventId']) ? $arr['eventId'] : null);
        $event->setStatusId(isset($arr['statusId']) ? $arr['statusId'] : null);
        $event->setMessage(isset($arr['message']) ? $arr['message'] : null);
        return $event;
    }

    public function toDbArray($event)
    {
        $data = array();
        $data['eventKey'] = $event->getEventKey();
        $data['statusId'] = $event->getStatusId();
        $data['objectId'] = $event->getObjectId();
        $data['message'] = $event->getMessage();
        $data['dtc'] = $event->getDtc()->format(DATE_W3C);
        return $data;
    }

    /**
     * @params Event $event
     * return array
     **/
    public function toArray($event)
    {
        $data = array();
        $data['id'] = $data['eventId'] = $event->getEventId();
        $data['eventKey'] = $event->getEventKey();
        $data['statusId'] = $event->getStatusId();
        $data['objectId'] = $event->getObjectId();
        $data['message'] = $event->getMessage();
        $data['dtc'] = $event->getDtc()->format(DATE_W3C);
        return $data;
    }
}
