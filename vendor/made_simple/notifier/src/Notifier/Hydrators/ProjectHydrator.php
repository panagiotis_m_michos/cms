<?php

namespace Notifier\Hydrators;

use Notifier\Models\Project;

class ProjectHydrator implements IHydrator
{
    /**
     * @param array $arr
     **/
    public function fromArray(array $arr)
    {
        $project = new Project(isset($arr['title']) ? $arr['title'] : null);
        $project->setProjectId(isset($arr['projectId']) ? $arr['projectId'] : null);
        return $project;
    }

    public function toDbArray($project)
    {
        $data = array();
        $data['title'] = $project->getTitle();
        return $data;
    }

    /**
     * @params Project $project
     * return array
     **/
    public function toArray($project)
    {
        $taskHydrator = new TaskHydrator();
        $data = array();
        $data['projectId'] = $data['id'] = $project->getProjectId();
        $data['title'] = $project->getTitle();
        $tasks = array();
        foreach ($project->getTasks() as $task) {
            $tasks[] = $task->getId();
        }
        $data['tasks'] = $tasks;
        return $data;
    }
}
