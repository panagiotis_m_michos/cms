<?php

namespace Notifier\Hydrators;

interface IHydrator
{

    /**
     * create object from array
     *
     * @param array $fromArray
     * @return mixed
     */
    public function fromArray(array $fromArray);

    /**
     * convert object to array
     *
     * @param mixed $model
     * @return array
     */
    public function toArray($model);

    /**
     * convert object to database representation
     *
     * @param mixed $model
     * @return array
     */
    public function toDbArray($model);
}
