<?php

namespace Notifier\Hydrators;

use Notifier\Models\Task;

class TaskHydrator implements IHydrator
{

    /**
     * @param array $arr
     **/
    public function fromArray(array $taskArr)
    {
        $task = new Task(
            isset($taskArr['title']) ? $taskArr['title'] : null,
            isset($taskArr['commandName']) ? $taskArr['commandName'] : null,
            isset($taskArr['projectId']) ? $taskArr['projectId']
                : (isset($taskArr['project']) ? $taskArr['project'] : null)
        );
        $task->setTaskId(isset($taskArr['taskId']) ? $taskArr['taskId'] : null);
        $task->setDescription(isset($taskArr['description']) ? $taskArr['description'] : null);
        $task->setInfo(isset($taskArr['info']) ? json_decode($taskArr['info'], true) : null);
        return $task;
    }

    /**
     * @params Task $task
     * return array
     **/
    public function toArray($task)
    {
        $data = array();
        $data['taskId'] = $data['id'] = $task->getTaskId();
        $data['title'] = $task->getTitle();
        $data['description'] = $task->getDescription();
        $data['commandName'] = $task->getCommandName();
        $data['status'] = $task->getStatus();
        $data['message'] = $task->getMessage();
        $data['info'] = $task->getInfo();
        $project = $task->getProject();
        $data['project'] = $project ? $project->getProjectId() : null;
        $data['dtc'] = $task->getDtc()->format(DATE_W3C);
        return $data;
    }

    /**
     * @params Task $task
     * return array
     **/
    public function toDbArray($task)
    {
        $data = array();
        $data['title'] = $task->getTitle();
        $data['description'] = $task->getDescription();
        $data['commandName'] = $task->getCommandName();
        $data['info'] = json_encode($task->getInfo());
        $data['projectId'] = $task->getProjectId() > 0 ? $task->getProjectId() : null;
        return $data;
    }
}
