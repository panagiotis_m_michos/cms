<?php

namespace Notifier\Plugins;

use Notifier\Services\EventService;
use DateTime;
use Cron\INotifier;
use Notifier\Models\Event;

class CronNotifier extends EventService implements INotifier
{
    private $defaultNamespace;

    public function triggerSuccess($eventKey, $objectId, $message)
    {
        $this->trigger(Event::SUCCESS, $eventKey, $objectId, $message);
    }

    public function triggerWarning($eventKey, $objectId, $message)
    {
        $this->trigger(Event::WARNING, $eventKey, $objectId, $message);
    }

    public function triggerFailure($eventKey, $objectId, $message)
    {
        $this->trigger(Event::FAILURE, $eventKey, $objectId, $message);
    }


    private function trigger($status, $eventKey, $objectId, $message)
    {
        $eventKey = $this->defaultNamespace ? $this->defaultNamespace . $eventKey : $eventKey;
        $event = new Event($eventKey, $objectId, new DateTime());
        $event->setStatusId($status);
        $event->setMessage($message);
        $this->validator->validate($event);
        $this->repository->save($event);
    }

    /**
     * Set defaultNamespace.
     *
     * @param defaultNamespace the value to set.
     */
    public function setDefaultNamespace($defaultNamespace)
    {
        $this->defaultNamespace = $defaultNamespace;
    }
}
