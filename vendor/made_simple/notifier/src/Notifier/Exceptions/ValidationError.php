<?php

namespace Notifier\Exceptions;

use LogicException;

class ValidationError extends LogicException
{
}
