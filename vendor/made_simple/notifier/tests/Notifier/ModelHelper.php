<?php

namespace Notifier;

use Notifier\Models\Task;
use Notifier\Models\Event;
use DateTime;

class ModelHelper
{

    public static function createTask()
    {
        $task = new Task('test title', 'site', new DateTime());
        $task->setDescription('description');
        return $task;
    }

    public static function createEvent()
    {
        $event = new Event('testName', 1, new DateTime());
        $event->setMessage('temp message');
        return $event;
    }

}
