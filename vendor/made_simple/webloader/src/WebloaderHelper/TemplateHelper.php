<?php

namespace WebloaderHelper;

use LatteMacros;
use Nette\Object;

class TemplateHelper extends Object
{
    /**
     * @var WebloaderHelper
     */
    private $helper;

    /**
     * @param WebloaderHelper $helper
     */
    public function __construct(WebloaderHelper $helper)
    {
        $this->helper = $helper;
    }

    /**
     * @param Object $template
     */
    public function registerSmartyPlugin($template)
    {
        $template->registerPlugin('function', 'styles', array($this->helper, 'loadStyles'));
        $template->registerPlugin('function', 'scripts', array($this->helper, 'loadScripts'));
    }

    /**
     * @param Object $template
     */
    public function registerLatteTemplate($template)
    {
        $template->webloader = $this->helper;
        LatteMacros::$defaultMacros['styles'] = '<?php echo $webloader->loadStyles(%%); ?>';
        LatteMacros::$defaultMacros['scripts'] = '<?php echo $webloader->loadScripts(%%); ?>';
    }

}