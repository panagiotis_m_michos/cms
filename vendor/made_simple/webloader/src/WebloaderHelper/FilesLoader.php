<?php

namespace WebloaderHelper;

use ConfigAdapterNeon;
use Nette\Object;
use WebloaderHelper\Exceptions\InvalidArgumentException;
use WebloaderHelper\Interfaces\IConfig;
use WebloaderHelper\Interfaces\IFilesLoader;

class FilesLoader extends Object implements IFilesLoader
{
    const TYPE_STYLES = 'styles';
    const TYPE_SCRIPTS = 'scripts';

    /**
     * @var IConfig
     */
    private $config;

    /**
     * @param IConfig $config
     */
    public function __construct(IConfig $config)
    {
        $this->config = $config;
    }

    /**
     * @param string $file
     * @param string $type
     * @param string $section
     * @throws InvalidArgumentException
     * @return array
     */
    public function getFiles($file, $type, $section)
    {
        $configPath = $this->config->getConfigDir() . DIRECTORY_SEPARATOR . $file;
        if (!is_file($configPath)) {
            throw new InvalidArgumentException(
                sprintf("Webloader config file `$configPath` doesn't exist")
            );
        }

        $config = ConfigAdapterNeon::load($configPath);
        if (!isset($config[$type][$section])) {
            throw new InvalidArgumentException(
                sprintf("Webloader config section `%s[%s]` doesn't exist", $type, $section)
            );
        }

        return $config[$type][$section];
    }
}