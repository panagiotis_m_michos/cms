<?php

namespace WebloaderHelper\Interfaces;

interface IConfig
{
    /**
     * @return string
     */
    public function getBasePath();

    /**
     * @return string
     */
    public function getConfigDir();

    /**
     * @return string
     */
    public function getFilesRoot();

    /**
     * @return string
     */
    public function getOutputDir();

    /**
     * @return string
     */
    public function getTempPath();
}