<?php

namespace WebloaderHelper\Interfaces;

use WebLoader\Nette\WebLoader;

interface ILoaderFactory
{
    /**
     * @param array $filesConfig
     * @return WebLoader
     */
    public function getLoader(array $filesConfig);
}