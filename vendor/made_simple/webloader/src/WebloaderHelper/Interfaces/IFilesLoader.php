<?php

namespace WebloaderHelper\Interfaces;

interface IFilesLoader
{
    /**
     * @param string $file
     * @param string $type
     * @param string $section
     * @return mixed
     */
    public function getFiles($file, $type, $section);
}