<?php

namespace WebloaderHelper;

use Nette\Object;
use WebloaderHelper\Exceptions\InvalidArgumentException;
use WebloaderHelper\Interfaces\IFilesLoader;
use WebloaderHelper\Interfaces\ILoaderFactory;

class WebloaderHelper extends Object
{
    /**
     * @var IFilesLoader
     */
    private $configLoader;

    /**
     * @var ILoaderFactory
     */
    private $stylesLoader;

    /**
     * @var ILoaderFactory
     */
    private $scriptsLoader;

    /**
     * @param IFilesLoader $configLoader
     * @param ILoaderFactory $stylesLoader
     * @param ILoaderFactory $scriptsLoader
     */
    public function __construct(IFilesLoader $configLoader, ILoaderFactory $stylesLoader, ILoaderFactory $scriptsLoader)
    {
        $this->stylesLoader = $stylesLoader;
        $this->scriptsLoader = $scriptsLoader;
        $this->configLoader = $configLoader;
    }

    /**
     * @param array $params
     * @throws InvalidArgumentException
     */
    public function loadStyles(array $params)
    {
        $loader = $this->stylesLoader->getLoader(
            $this->configLoader->getFiles(
                self::getParam($params, 'file'),
                FilesLoader::TYPE_STYLES,
                self::getParam($params, 'section')
            )
        );
        $loader->render();
    }

    /**
     * @param array $params
     * @throws InvalidArgumentException
     */
    public function loadScripts(array $params)
    {
        $loader = $this->scriptsLoader->getLoader(
            $this->configLoader->getFiles(
                self::getParam($params, 'file'),
                FilesLoader::TYPE_SCRIPTS,
                self::getParam($params, 'section')
            )
        );
        $loader->render();
    }

    /**
     * @param array $params
     * @param string $key
     * @return mixed
     * @throws InvalidArgumentException
     */
    private static function getParam(array $params, $key)
    {
        if (empty($params[$key])) {
            throw new InvalidArgumentException("Webloader helper argument `$key` is missing");
        }
        return $params[$key];
    }
}