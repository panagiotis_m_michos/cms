<?php

namespace WebloaderHelper;

use Nette\Object;
use WebloaderHelper\Interfaces\IConfig;

class Config extends Object implements IConfig
{
    /**
     * @var string
     */
    private $configDir;
    
    /**
     * @var string
     */
    private $filesRoot;
    
    /**
     * @var string
     */
    private $outputDir;

    /**
     * @var string
     */
    private $basePath;

    /**
     * @var string
     */
    private $tempPath;

    /**
     * @param string $configDir  Path to directory where config neon files are located
     * @param string $filesRoot  FileCollection root for asset files
     * @param string $outputDir  Output dir for webloader generated files
     * @param string $basePath   CssUrlsFilter base path (rewriting images urls)
     * @param string $tempPath   CssLoader relative path to generated files
     */
    public function __construct($configDir, $filesRoot, $outputDir, $basePath, $tempPath)
    {
        $this->configDir = $configDir;
        $this->filesRoot = $filesRoot;
        $this->outputDir = $outputDir;
        $this->basePath = $basePath;
        $this->tempPath = $tempPath;
    }

    /**
     * @return string
     */
    public function getBasePath()
    {
        return $this->basePath;
    }

    /**
     * @return string
     */
    public function getConfigDir()
    {
        return $this->configDir;
    }

    /**
     * @return string
     */
    public function getFilesRoot()
    {
        return $this->filesRoot;
    }

    /**
     * @return string
     */
    public function getOutputDir()
    {
        return $this->outputDir;
    }

    /**
     * @return string
     */
    public function getTempPath()
    {
        return $this->tempPath;
    }
}