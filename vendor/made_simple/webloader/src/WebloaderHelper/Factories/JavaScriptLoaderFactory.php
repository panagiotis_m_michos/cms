<?php

namespace WebloaderHelper\Factories;

use JsMin\Minify;
use Nette\Object;
use WebLoader\Compiler;
use WebLoader\FileCollection;
use WebloaderHelper\Interfaces\IConfig;
use WebloaderHelper\Interfaces\ILoaderFactory;
use WebLoader\Nette\JavaScriptLoader;

class JavaScriptLoaderFactory extends Object implements ILoaderFactory
{
    /**
     * @var boolean
     */
    private $isProductionMode;

    /**
     * @var IConfig
     */
    private $config;

    /**
     * @param IConfig $config
     * @param boolean $isProductionMode
     */
    public function __construct(IConfig $config, $isProductionMode)
    {
        $this->config = $config;
        $this->isProductionMode = (bool) $isProductionMode;
    }

    /**
     * @param array $filesConfig
     * @return JavaScriptLoader
     */
    public function getLoader(array $filesConfig)
    {
        $config = $this->config;

        $files = new FileCollection($config->getFilesRoot());

        if (isset($filesConfig['files'])) {
            $files->addFiles($filesConfig['files']);
        }
        if (isset($filesConfig['remote'])) {
            $files->addRemoteFiles($filesConfig['remote']);
        }

        $compiler = Compiler::createJsCompiler($files, $config->getOutputDir());
        $compiler->setJoinFiles($this->isProductionMode);

        if ($this->isProductionMode) {
            $compiler->addFilter(
                function ($code) {
                    return Minify::minify($code);
                }
            );
        }

        return new JavaScriptLoader($compiler, $config->getTempPath());
    }

} 