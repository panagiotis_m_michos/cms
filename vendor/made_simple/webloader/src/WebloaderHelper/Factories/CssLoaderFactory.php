<?php

namespace WebloaderHelper\Factories;

use CssMin;
use Nette\Object;
use WebLoader\Compiler;
use WebLoader\FileCollection;
use WebloaderHelper\Filters\CssUrlsFilter;
use WebloaderHelper\Interfaces\IConfig;
use WebloaderHelper\Interfaces\ILoaderFactory;
use WebLoader\Nette\CssLoader;

class CssLoaderFactory extends Object implements ILoaderFactory
{
    /**
     * @var boolean
     */
    private $isProductionMode;

    /**
     * @var IConfig
     */
    private $config;

    /**
     * @param IConfig $config
     * @param boolean $isProductionMode
     */
    public function __construct(IConfig $config, $isProductionMode)
    {
        $this->config = $config;
        $this->isProductionMode = (bool) $isProductionMode;
    }

    /**
     * @param array $filesConfig
     * @return CssLoader
     */
    public function getLoader(array $filesConfig)
    {
        $config = $this->config;

        $files = new FileCollection($config->getFilesRoot());
        $files->addFiles($filesConfig);

        $compiler = Compiler::createCssCompiler($files, $config->getOutputDir());
        $compiler->setJoinFiles($this->isProductionMode);

        $compiler->addFileFilter(new CssUrlsFilter($config->getBasePath()));

        if ($this->isProductionMode) {
            $compiler->addFilter(
                function ($code) {
                    return CssMin::minify($code);
                }
            );
        }

        $loader = new CssLoader($compiler, $config->getTempPath());
        $loader->setMedia('screen');
        return $loader;
    }
}
