<?php

namespace WebloaderHelper\Factories;

use FileTemplate;
use Smarty;
use Nette\Object;
use WebloaderHelper\Exceptions\InvalidArgumentException;
use WebloaderHelper\FilesLoader;
use WebloaderHelper\Interfaces\IConfig;
use WebloaderHelper\TemplateHelper;
use WebloaderHelper\WebloaderHelper;

class TemplateHelperFactory extends Object 
{
    /**
     * @param IConfig $config
     * @param mixed $template
     * @param bool $isProduction
     * @throws InvalidArgumentException
     */
    public static function registerMacro(IConfig $config, $template, $isProduction)
    {
        $webloaderHelper = new WebloaderHelper(
            new FilesLoader($config),
            new CssLoaderFactory($config, $isProduction),
            new JavaScriptLoaderFactory($config, $isProduction)
        );

        $templateHelper = new TemplateHelper($webloaderHelper);

        if ($template instanceof FileTemplate) {
            $templateHelper->registerLatteTemplate($template);

        } elseif ($template instanceof Smarty) {
            $templateHelper->registerSmartyPlugin($template);

        } else {
            throw new InvalidArgumentException("Template engine is not supported");
        }
    }
}