### Description

We currently use the [webloader](http://addons.nette.org/janmarek/webloader) addon to prepare the JS and CSS of our sites.

The factory files (the file that instruct webloader on how to treat the js and css) for all projects are the same, but they need to carry different site specific configuration for each project.

This webloader helper will abstract the need of any configuration inside the webloader factories. You can pass the configs on the implementation code of each project and the factory files that are shared among all projects will have no site specific configs.



### Install

By installing this WebloaderHelper you will install the webloader addon and all required dependencies. (all dependencies are specified in [composer.json](https://bitbucket.org/made_simple/webloader/src/6f191ba7735f502cc7c2989970f8e9d69d85b8b6/composer.json?at=master))

To install the helper add this to your project composer.json:
~~~
# composer.json
{
    "repositories": [
    {
        "type": "git",
        "url": "git@bitbucket.com:made_simple/webloader.git"
    }
    ],
    "require": {
        "made_simple/webloader": "~1.0"
    }
}
~~~

And run:
~~~
$ composer update made_simple/webloader
~~~

This will install everything you need.



### Implementation

After creating the webloader configuration files (check this [CMS example](https://bitbucket.org/made_simple/cms/src/c92933e5f6a967ddc11415e99c282b2d3754d09e/project/config/webloader/webloader_admin.neon?at=master) - read the [webloader project page](http://addons.nette.org/janmarek/webloader) for less information) you need to create a webtemp directory in web root folder (777 permissions):
~~~
www/webtemp
~~~

To implement the webloader you will need to pass all site specific configurations ([CMS example](https://bitbucket.org/made_simple/cms/src/58f4ea91aebdeb9828ca5148cc4ce12e2192fe94/libs/Framework/FTemplate.php?at=webloader#cl-45))

~~~
// FTemplate.php
$config = new WebloaderConfig(
    APP_DIR . '/config/webloader/',
    WWW_DIR,
    WWW_DIR . '/webtemp/',
    WWW_DIR . '/../',
    URL_ROOT . 'www/webtemp/'
);
TemplateHelperFactory::registerMacro($config, $this->engine, FApplication::isProduction());
~~~

[Check what configs the WebloaderConfig requires.](https://bitbucket.org/made_simple/webloader/src/1a8e54e5c52a90d9fa2015afe007893f18941ed8/src/WebloaderHelper/Config.php?at=master#cl-36)


### Template (e.g. layout.latte)

On your template you need to use the script calls to insert the JS and CSS files.

For JS:
~~~
// css styles
{styles array('file' => 'front.neon', 'section' => 'common')}
~~~

Will output:
~~~
<script type="text/javascript" src="/components/jquery/jquery.js"></script>
<script type="text/javascript" src="/components/jquery-ui/ui/jquery-ui.js"></script>
~~~

For CSS:
~~~
// javascripts
{scripts array('file' => 'front.neon', 'section' => 'common')}
~~~

Will output:
~~~
<link rel="stylesheet" href="/css/reset.css" type="text/css">
<link rel="stylesheet" href="/css/960_12_col.css" type="text/css">
~~~
