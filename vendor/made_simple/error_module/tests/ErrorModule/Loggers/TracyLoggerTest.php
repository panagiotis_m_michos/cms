<?php

namespace ErrorModule\Loggers;

use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;
use Psr\Log\LogLevel;
use Tracy\Debugger;

class TracyLoggerTest extends PHPUnit_Framework_TestCase
{

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    private $logger;

    /**
     * @var TracyLogger
     */
    private $object;

    public function setUp()
    {
        $this->logger = $this->getMock('Psr\Log\LoggerInterface');
        $this->object = new TracyLogger($this->logger);
    }

    /**
     * @covers ErrorModule\Loggers\TracyLogger::log
     */
    public function testCorrectNullype()
    {
        $this->logger->expects($this->once())->method('log')->with(LogLevel::DEBUG, 'test');
        $this->object->log('test', NULL);
    }

    /**
     * @covers ErrorModule\Loggers\TracyLogger::log
     */
    public function testCorrectTracytoMonologType()
    {
        $this->logger->expects($this->once())->method('log')->with(LogLevel::CRITICAL, 'test');
        $this->object->log('test', Debugger::EXCEPTION);
    }

    /**
     * @covers ErrorModule\Loggers\TracyLogger::log
     */
    public function testCorrectSameType()
    {
        $this->logger->expects($this->once())->method('log')->with(LogLevel::INFO, 'test');
        $this->object->log('test', Debugger::INFO);
    }
}
