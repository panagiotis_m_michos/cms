<?php

namespace ErrorModule\Ext;

use BootstrapModule\Ext\DoctrineExt;
use BootstrapModule\Ext\FrameworkExt_Abstract;
use BootstrapModule\Ext\IExtension;
use ErrorModule\Panels\FrameworkDebugPanel;
use Symfony\Component\DependencyInjection\Container;
use Tracy\Debugger;
use Kdyby\Doctrine\Diagnostics\Panel as DoctrinePanel;
use Dibi\Bridges\Tracy\Panel as DibiPanel;

final class PanelExt implements IExtension
{
    /**
     * @var bool
     */
    public static $showDebugBar = FALSE;

    /**
     * @TODO find a way to disable debug panels completely
     * @param Container $container
     */
    public function load(Container $container)
    {
        if (!$container->hasParameter('debug_panel') || php_sapi_name() === 'cli') {
            return;
        }
        $showQueries = $container->getParameter('debug_panel.show_queries');
        if ($showQueries) {
            $dibiPanel = new DibiPanel();
            $dibiPanel->register($container->get(FrameworkExt_Abstract::DIBI));
            $doctrinePanel = new DoctrinePanel();
            $doctrinePanel->setConnection($container->get(DoctrineExt::CONNECTION));
            Debugger::getBar()->addPanel($doctrinePanel);
        }
        $showAdmin = $container->getParameter('debug_panel.show_admin');
        if ($showAdmin) {
            FrameworkDebugPanel::register('cms');
        }

        if ($showQueries || $showAdmin) {
            //render debugbar when enabled
            register_shutdown_function(array($this, 'showDebugBar'));
        }
    }

    public function showDebugBar()
    {
        if (self::$showDebugBar && $this->isHtmlMode()) {
            Debugger::getBar()->render();
        }
    }

    /**
     * @return bool
     */
    private function isHtmlMode()
    {
        return empty($_SERVER['HTTP_X_REQUESTED_WITH'])
        && !preg_match('#^Content-Type: (?!text/html)#im', implode("\n", headers_list()));
    }
}
