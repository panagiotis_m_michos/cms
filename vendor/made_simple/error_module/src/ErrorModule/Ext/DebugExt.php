<?php

namespace ErrorModule\Ext;

use BootstrapModule\Ext\IExtension;
use Environment;
use ErrorModule\Config\IDebugConfig;
use ErrorModule\Controllers\DevErrorController;
use ErrorModule\Controllers\ErrorController;
use ErrorModule\Handlers\EmergencyHandler;
use ErrorModule\Handlers\TracyMailHandler;
use ErrorModule\Loggers\TracyLogger;
use FRouter;
use Monolog\Handler\FilterHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Registry;
use Symfony\Component\DependencyInjection\Container;
use Tracy\Debugger;

final class DebugExt implements IExtension
{
    const ERROR_CONTROLLER = 'error.controller';
    const CONFIG = 'error.config.debug';
    const LOGGER = 'error.loggers.monolog';
    const LOGGER_HANDLERS_TRACY_MAIL_HANDLER = 'error.loggers.handlers.tracy_mail_handler';
    const LOGGER_CONTROLLER = 'error.controllers.logger';

    /**
     * @param Container $container
     */
    public function load(Container $container)
    {
        $environment = $container->getParameter('environment');
        $debugConfig = $container->get(self::CONFIG);
        $monolog = new Logger('application');
        $tracyMailHandler = new TracyMailHandler(
            $debugConfig->getEmailTo(),
            $debugConfig->getEmailFrom(),
            $debugConfig->getErrorLogDir(),
            Logger::ERROR,
            new FRouter()
        );

        if (in_array($environment, [Environment::DEVELOPMENT, Environment::CONSOLE])) {
            $errorController = new DevErrorController();
            if (php_sapi_name() !== 'cli') {
                Debugger::enable(Debugger::DEVELOPMENT);
            }
        }
        else {
            $errorController = new ErrorController($debugConfig->getErrorPagesDir());
            //all debug and info messages go to a separate file
            $monolog->pushHandler(new FilterHandler(new StreamHandler($debugConfig->getInfoLogPath()), Logger::DEBUG, Logger::INFO));
            //order is important here
            if (php_sapi_name() != 'cli') {
                $monolog->pushHandler(new EmergencyHandler($errorController, Logger::CRITICAL));
            }
            $monolog->pushHandler($tracyMailHandler);
            $monolog->pushHandler(new StreamHandler($debugConfig->getErrorLogPath(), Logger::NOTICE));
            //unfortunately tracy and monolog are useless on memory limit exceptions in cli and BootExt memory overriding does not help
            if (php_sapi_name() === 'cli') {
                register_shutdown_function(function(IDebugConfig $debugConfig) {
                    $error = error_get_last();
                    //@TODO find a better way to check if its fatal memory exception
                    if ($error && $error['type'] == E_ERROR && strpos($error['message'], 'Allowed memory size') !== FALSE) {
                        $message = date('[Y-m-d H:i:s]') . ' PHP Fatal error: ' . var_export($error, TRUE) . PHP_EOL;
                        file_put_contents($debugConfig->getErrorLogPath(), $message, FILE_APPEND);
                        $headers = "From: {$debugConfig->getEmailFrom()} \r\n";
                        $headers .= 'Content-type: text/plain; charset=utf-8' . "\r\n";
                        mail($debugConfig->getEmailTo(), 'application.CRITICAL: ' . substr($error['message'], 0, 200), $message, $headers);
                        echo $message;
                        //stop other shutdown_handlers (causes segmentation fault if not stopped)
                        exit(1);
                    }
                }, $debugConfig);
            }
            //email probably wont be used here
            Debugger::enable(Debugger::PRODUCTION, $debugConfig->getErrorLogDir(), $debugConfig->getEmailTo());
            //bridge for tracy and monolog
            $tracyLogger = new TracyLogger($monolog);
            Debugger::setLogger($tracyLogger);
        }

        //for old debug class
        Registry::addLogger($monolog);
        $container->set(self::LOGGER, $monolog);
        $container->set(self::LOGGER_HANDLERS_TRACY_MAIL_HANDLER, $tracyMailHandler);
        $container->set(self::ERROR_CONTROLLER, $errorController);
    }
}
