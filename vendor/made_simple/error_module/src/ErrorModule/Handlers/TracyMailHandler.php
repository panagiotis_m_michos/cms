<?php

namespace ErrorModule\Handlers;

use FRouter;
use LoggerAdminControler;
use Monolog\Handler\MailHandler;
use Monolog\Logger;

class TracyMailHandler extends MailHandler
{
    /**
     * The email addresses to which the message will be sent
     * @var array
     */
    protected $to;

    /**
     * Optional headers for the message
     * @var array
     */
    protected $headers = [];

    /**
     * The wordwrap length for the message
     * @var integer
     */
    protected $maxColumnWidth;

    /**
     * The Content-type for the message
     * @var string
     */
    protected $contentType = 'text/plain';

    /**
     * The encoding for the message
     * @var string
     */
    protected $encoding = 'utf-8';

    /**
     * @var string
     */
    private $logDir;

    /**
     * @var FRouter
     */
    private $router;

    /**
     * @param string|array $to The receiver of the mail
     * @param string $from The sender of the mail
     * @param string $logDir
     * @param int $level The minimum logging level at which this handler will be triggered
     * @param FRouter $router
     * @param boolean $bubble Whether the messages that are handled can bubble up the stack or not
     * @param int $maxColumnWidth The maximum column width that the message lines will have
     */
    public function __construct(
        $to,
        $from,
        $logDir,
        $level = Logger::ERROR,
        FRouter $router,
        $bubble = TRUE,
        $maxColumnWidth = 70
    )
    {
        parent::__construct($level, $bubble);
        $this->to = is_array($to) ? $to : [$to];
        $this->addHeader("From: $from");
        $this->logDir = $logDir;
        $this->router = $router;
        $this->maxColumnWidth = $maxColumnWidth;
    }

    /**
     * Add headers to the message
     *
     * @param string|array $headers Custom added headers
     */
    public function addHeader($headers)
    {
        foreach ((array) $headers as $header) {
            if (strpos($header, "\n") !== FALSE || strpos($header, "\r") !== FALSE) {
                throw new \InvalidArgumentException('Headers can not contain newline characters for security reasons');
            }
            $this->headers[] = $header;
        }
    }

    /**
     * @return string $contentType
     */
    public function getContentType()
    {
        return $this->contentType;
    }

    /**
     * @param string $contentType
     * @return TracyMailHandler
     */
    public function setContentType($contentType)
    {
        $this->contentType = $contentType;

        return $this;
    }

    /**
     * @return string $encoding
     */
    public function getEncoding()
    {
        return $this->encoding;
    }

    /**
     * @param string $encoding
     * @return TracyMailHandler
     */
    public function setEncoding($encoding)
    {
        $this->encoding = $encoding;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    protected function send($content, array $records)
    {
        $errorMessage = $this->parseErrorMessage($content);
        $exceptionFile = $this->parseExceptionFile($content);

        $subject = substr($errorMessage, 0, 200);
        $content = wordwrap($content, $this->maxColumnWidth);
        $headers = ltrim(implode("\r\n", $this->headers) . "\r\n", "\r\n");

        $this->sendMail($this->to, $subject, $content, $headers, $exceptionFile);
    }

    /**
     * @param array $recipients
     * @param string $subject
     * @param string $content
     * @param string $headers
     * @param string $exceptionFile
     */
    protected function sendMail($recipients, $subject, $content, $headers, $exceptionFile)
    {
        $eol = "\r\n";
        $headers .= "Content-type: {$this->getContentType()}; charset={$this->getEncoding()}" . $eol;
        if ($this->getContentType() == 'text/html' && strpos($headers, 'MIME-Version:') === FALSE) {
            $headers .= 'MIME-Version: 1.0' . $eol;
        }

        $exceptionFilePath = $this->logDir . DIRECTORY_SEPARATOR . $exceptionFile;
        if ($exceptionFile && is_file($exceptionFilePath) && is_readable($exceptionFilePath)) {
            $content = 'View full exception: '
                . $this->router->absoluteAdminLink(
                    LoggerAdminControler::PAGE_LOGGER . ' viewException',
                    ['fileId' => basename($exceptionFilePath)]
                )
                . $eol . $eol . $content;
        }

        foreach ($recipients as $recipient) {
            mail($recipient, $subject, $content, $headers);
        }
    }

    /**
     * @param string $content
     * @return string
     */
    private function parseErrorMessage($content)
    {
        return preg_match(
            '/^\[\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}\] (.+?)(?: @@? .+)?$/',
            $content,
            $matches
        ) ? $matches[1] : NULL;
    }

    /**
     * @param string $content
     * @return string
     */
    private function parseExceptionFile($content)
    {
        return preg_match('/ @@  (.+\.html) /', $content, $matches) ? $matches[1] : NULL;
    }
}
