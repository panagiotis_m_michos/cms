<?php

namespace ErrorModule\Handlers;

use ErrorModule\Controllers\IErrorController;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;

/**
 * If emergency code is receive execution stops by rendering appropriate message
 * Class EmergencyHandler
 * @package Utils
 */
class EmergencyHandler extends AbstractProcessingHandler
{

    /**
     * @var IErrorController
     */
    private $errorController;

    /**
     * @param IErrorController $errorController
     * @param bool|int $level
     * @param bool $bubble
     */
    public function __construct(IErrorController $errorController, $level = Logger::CRITICAL, $bubble = TRUE)
    {
        $this->errorController = $errorController;
        parent::__construct($level, $bubble);
    }

    /**
     * We could provide different error pages based on context, but tracy provides just message
     * Handles a record
     *
     * @param  array $record The record to handle
     * @return Boolean true means that this handler handled the record, and that bubbling is not permitted.
     *                        false means the record was either not processed or that this handler allows bubbling.
     */
    protected function write(array $record)
    {
        $this->errorController->error500(isset($record['message']) ? $record['message'] : 'Unknown error');
    }
}