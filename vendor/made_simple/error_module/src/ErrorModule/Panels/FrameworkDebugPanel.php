<?php

namespace ErrorModule\Panels;

use FApplication;
use FLanguage;
use FNode;
use FPage;
use FRouter;
use FUser;
use Html;
use Nette\Object;
use Tracy\Debugger;
use Tracy\IBarPanel;

/**
 * @remove external dependencies
 * Class FrameworkDebugPanel
 * @package ErrorModule\Panels
 */
class FrameworkDebugPanel extends Object implements IBarPanel
{

    /**
     * @var string
     */
    private $projectName;

    /**
     * @param string $projectName
     */
    public function __construct($projectName)
    {
        $this->projectName = $projectName;
    }

    /**
     * @return string
     */
    public function getTab()
    {
        $el = Html::el();
        $text = $this->projectName;
        //$el->create('img')->src(ADMIN_ICONS_DIR . 'show2.gif')->style('margin-top: 0px;');
        if (file_exists(DOCUMENT_ROOT . '/.git/HEAD')) {
            $headContents = file_get_contents(DOCUMENT_ROOT . '/.git/HEAD');
            $pos = mb_strrpos($headContents, '/');
            if ($pos !== FALSE) {
                $text .= ':' . mb_substr($headContents, $pos + 1);
            }
        }
        $el->create('span')->setText($text);
        return $el;
    }

    /**
     * @return string
     */
    public function getPanel()
    {
        $el = Html::el();
        if (!class_exists('FApplication') || !isset(FApplication::$controler, FApplication::$controler->node)) {
            return $el;
        }
        $el->create('h1')->setText('CMS');

        $table = $el->create('table')->width('100%');
        $table->create('col')->width(100);
        $table->create('col');

        // id
        $tr = $table->create('tr');
        $tr->create('th')->setText('Id:');
        $tr->create('td')->setText(FApplication::$controler->node->getId());

        // front controller
        $tr = $table->create('tr');
        $frontUrl = FRouter::getUrl(FApplication::$controler->node->getLngFriendlyUrl(), FApplication::$nodeId, FApplication::$lang);
        $frontControler = (FApplication::$controler->node->frontControler) ? FApplication::$controler->node->frontControler : 'None';
        $tr->create('th')->setText('Front:');
        $tr->create('td')->create('a')->href($frontUrl)->setText($frontControler)->target('_blank');

        // admin controller
        $tr = $table->create('tr');
        $adminUrl =  ADMIN_URL_ROOT . FApplication::$lang . '/' . FApplication::$nodeId . '/';
        $tr->create('th')->setText('Admin:');
        $tr->create('td')->create('a')->href($adminUrl)->setText(FApplication::$controler->node->adminControler)->target('_blank');

        // action
        $tr = $table->create('tr');
        $tr->create('th')->setText('Action:');
        $tr->create('td')->setText(FApplication::$controler->action);

        // view
        $tr = $table->create('tr');
        $tr->create('th')->setText('View:');
        $tr->create('td')->setText(FApplication::$controler->getView());

        // status
        $tr = $table->create('tr');
        $tr->create('th')->setText('Status:');
        $tr->create('td')->setText(FApplication::$controler->node->status);

        // created
        $tr = $table->create('tr');
        $tr->create('th')->setText('Created:');
        $tr->create('td')->setText(date('j M, Y G:i', strtotime(FApplication::$controler->node->dtc)));

        // edited
        $tr = $table->create('tr');
        $tr->create('th')->setText('Edited:');
        $tr->create('td')->setText(date('j M, Y G:i', strtotime(FApplication::$controler->node->dtm)));

        // classes
        $tr = $table->create('tr');
        $tr->create('th')->setText('Classes:');
        $tr->create('td')->setText(sprintf('Nodes: %sx, Pages: %sx, Lngs: %sx, Users: %sx', FNode::$counter, FPage::$counter, FLanguage::$counter, FUser::$counter));

        return $el;
    }

    /**
     * Returns panel ID.
     *
     * @return string
     */
    public function getId()
    {
        return get_class($this);
    }

    public static function register($projectName)
    {
        $panel = new self($projectName);
        Debugger::getBar()->addPanel($panel);
        return $panel;
    }
}
