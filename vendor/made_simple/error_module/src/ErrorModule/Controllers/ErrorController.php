<?php

namespace ErrorModule\Controllers;

use Exception;
use FControler;
use Tracy\Debugger;
use Utils\Directory;
use Utils\File;

class ErrorController implements IErrorController
{
    /**
     * @var Directory
     */
    private $directory;

    /**
     * @param Directory $directory
     */
    public function __construct(Directory $directory)
    {
        $this->directory = $directory;
    }

    /**
     * @param string $fileName
     * @param string $message
     */
    public function render($fileName, $message)
    {
        $file = File::fromDir($this->directory, $fileName);
        if ($file->isReadable()) {
            require_once $file->getPath();
        } else {
            echo "ERROR: application encountered an error and can not continue.";
        }
    }

    /**
     * @param $message
     */
    public function error500($message)
    {
        $this->render('error_500.php', $message);
        exit;
    }

    /**
     * thats where all exceptions should be handled
     *
     * @param Exception $e
     */
    public function handleControllerError(Exception $e)
    {
        if ($e instanceof Exception) {
            //we want to log exceptions by tracy
            Debugger::log($e, Debugger::ERROR);
            $this->error500('Unknown error');
        }
    }
}
