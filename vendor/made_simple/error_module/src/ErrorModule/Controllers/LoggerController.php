<?php

namespace ErrorModule\Controllers;

use DirectoryIterator;
use ErrorModule\Utils\ExceptionFile;
use InvalidArgumentException;
use Utils\Directory;
use Utils\File;

class LoggerController
{

    /**
     * @var Directory
     */
    private $logDirectory;

    /**
     * @var File
     */
    private $errorLog;

    /**
     * @var File
     */
    private $infoLog;

    /**
     * @param Directory $logDirectory
     * @param File $errorLog
     * @param File $infoLog
     */
    public function __construct(Directory $logDirectory, File $errorLog, File $infoLog)
    {

        $this->logDirectory = $logDirectory;
        $this->errorLog = $errorLog;
        $this->infoLog = $infoLog;
    }

    /**
     * @return bool
     */
    public function hasInfoFile()
    {
        return $this->infoLog->isReadable();
    }

    /**
     * @return mixed
     */
    public function getInfoLogContent()
    {
        return $this->infoLog->getContent();
    }

    /**
     * @return mixed
     */
    public function deleteInfoLog()
    {
        return unlink($this->infoLog->getPath());
    }

    /**
     * @return bool
     */
    public function hasErrorFile()
    {
        return $this->errorLog->isReadable();
    }

    /**
     * @return mixed
     */
    public function getErrorLogContent()
    {
        return $this->errorLog->getContent();
    }

    /**
     * @return mixed
     */
    public function deleteErrorLog()
    {
        return unlink($this->errorLog->getPath());
    }

    /**
     * @return bool
     */
    public function hasExceptions()
    {
        return count($this->getExceptions()) > 0;
    }

    /**
     * @return ExceptionFile[]
     */
    public function getExceptions()
    {
        $files = array();
        foreach (new DirectoryIterator($this->logDirectory->getPath()) as $splFile) {
            if (strpos($splFile->getFilename(), 'exception') !== FALSE) {
                $exceptionFile = ExceptionFile::fromSpl($splFile);
                $files[] = $exceptionFile;
            }
        }
        usort(
            $files,
            function(ExceptionFile $exceptionFile1, ExceptionFile $exceptionFile2) {
                return $exceptionFile1->getDtc() > $exceptionFile2->getDtc() ? -1 : 1;
            }
        );
        return $files;
    }

    /**
     * @param $fileId
     * @return bool
     * @throws InvalidArgumentException
     */
    public function deleteException($fileId)
    {
        $file = File::fromExisting($this->logDirectory, $fileId);
        return unlink($file->getPath());
    }

    /**
     * @param string $fileId
     * @return string
     * @throws InvalidArgumentException
     */
    public function getExceptionContent($fileId)
    {
        $file = File::fromExisting($this->logDirectory, $fileId);
        return $file->getContent();
    }

    public function deleteExceptions()
    {
        foreach ($this->getExceptions() as $file) {
            unlink($file->getPath());
        }
    }


}