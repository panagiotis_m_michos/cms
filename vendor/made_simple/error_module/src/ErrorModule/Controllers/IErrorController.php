<?php
namespace ErrorModule\Controllers;

use Exception;

interface IErrorController
{
    /**
     * @param $message
     */
    public function error500($message);

    /**
     * thats where all exceptions should be handled
     *
     * @param Exception $e
     */
    public function handleControllerError(Exception $e);
}