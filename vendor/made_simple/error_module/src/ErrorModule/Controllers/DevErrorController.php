<?php

namespace ErrorModule\Controllers;

use BadMethodCallException;
use Exception;
use FControler;

class DevErrorController implements IErrorController
{
    /**
     * @param $message
     */
    public function error500($message)
    {
        throw new BadMethodCallException($message);
    }

    /**
     * thats where all exceptions should be handled
     *
     * @param Exception $e
     * @throws Exception
     */
    public function handleControllerError(Exception $e)
    {
        throw $e;
    }

}