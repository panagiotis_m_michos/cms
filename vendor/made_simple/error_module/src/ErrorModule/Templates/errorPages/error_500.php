<html>
<head>
<meta charset="utf-8">
<meta name=robots content=noindex>
<title>Server Error</title>
<style>
    body { color: #333; background: white; width: 700px; margin: 100px auto }
    h1 { font: bold 47px/1.5 sans-serif; margin: .6em 0 }
    p { font: 21px/1.5 Georgia,serif; margin: 1.5em 0 }
    small { font-size: 70%; color: gray }
</style>
</head>
<body>

<div id="tracy-error-body">

    <h1>Something went wrong...</h1>

    <p>
        Looks like we broke something (not your fault). Our developers have been emailed and will fix it as soon as possible.
        Please feel free to <a href="mailto:theteam@madesimplegroup.com">email us</a> about this or any other questions you have.
    </p>

    <p><small>error 500</small></p>
    <div>
</body>
</html>