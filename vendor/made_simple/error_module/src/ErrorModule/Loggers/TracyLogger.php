<?php

namespace ErrorModule\Loggers;

use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Tracy\Debugger;

/**
 * Used for tracy to monolog interaction
 */
class TracyLogger
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param string $message
     * @param string $priority
     */
    public function log($message, $priority = NULL)
    {
        if (is_array($message)) {
            //remove timestamp duplication
            array_shift($message);
            $message = implode(' ', $message);
        }
        if (strpos($message, 'PHP Warning') !== FALSE) {
            $priority = LogLevel::WARNING;
        } elseif (strpos($message, 'PHP Notice') !== FALSE) {
            $priority = LogLevel::NOTICE;
        }
        $level = $this->mapLevel($priority);
        //unfortunately tracy does not provide any meaningful context we can refer to
        $context = array();
        $this->logger->log($level, $message, $context);
    }

    /**
     * @param $level
     * @return string
     */
    private function mapLevel($level)
    {
        switch($level) {
            //if tracy is sending exception level we'll trigger emergency which will display our own html pages
            case Debugger::EXCEPTION:
                return LogLevel::CRITICAL;
            default:
                return $level !== NULL ? $level : LogLevel::DEBUG;
        }
    }

}