<?php

namespace ErrorModule\Config;

use Utils\Directory;

interface IDebugConfig
{
    /**
     * @return string
     */
    public function getEmailTo();

    /**
     * @return string
     */
    public function getEmailFrom();

    /**
     * @return string
     */
    public function getEmailSubject();

    /**
     * @return string
     */
    public function getErrorLogPath();

    /**
     * @return string
     */
    public function getErrorLogDir();

    /**
     * @return string
     */
    public function getInfoLogPath();

    /**
     * @return Directory
     */
    public function getErrorPagesDir();
}