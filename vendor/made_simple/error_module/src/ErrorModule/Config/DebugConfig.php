<?php

namespace ErrorModule\Config;

use Utils\Directory;

class DebugConfig implements IDebugConfig
{
    /**
     * @var string
     */
    private $emailTo;

    /**
     * @var string
     */
    private $emailFrom;

    /**
     * @var string
     */
    private $emailSubject;

    /**
     * @var string
     */
    private $errorLogPath;

    /**
     * @var string
     */
    private $infoLogPath;

    /**
     * @var Directory
     */
    private $errorLogDir;

    /**
     * @var Directory
     */
    private $errorPages;

    /**
     * @param $emailTo
     * @param $emailFrom
     * @param $emailSubject
     * @param $errorLogPath
     * @param $infoLogPath
     * @param Directory $errorLogDir
     * @param Directory $errorPages
     */
    public function __construct($emailTo, $emailFrom, $emailSubject, $errorLogPath, $infoLogPath, Directory $errorLogDir, Directory $errorPages)
    {
        $this->emailTo = $emailTo;
        $this->emailFrom = $emailFrom;
        $this->emailSubject = $emailSubject;
        $this->errorLogPath = $errorLogPath;
        $this->infoLogPath = $infoLogPath;
        $this->errorLogDir = $errorLogDir;
        $this->errorPages = $errorPages;
    }

    /**
     * @return string
     */
    public function getEmailTo()
    {
        return $this->emailTo;
    }

    /**
     * @return string
     */
    public function getEmailFrom()
    {
        return $this->emailFrom;
    }

    /**
     * @return string
     */
    public function getEmailSubject()
    {
        return $this->emailSubject;
    }

    /**
     * @return string
     */
    public function getErrorLogPath()
    {
        return  $this->getErrorLogDir() . DIRECTORY_SEPARATOR . $this->errorLogPath;
    }

    /**
     * @return string
     */
    public function getInfoLogPath()
    {
        return $this->getErrorLogDir() . DIRECTORY_SEPARATOR . $this->infoLogPath;
    }

    /**
     * @return string
     */
    public function getErrorLogDir()
    {
        return $this->errorLogDir->getPath();
    }

    /**
     * @return Directory
     */
    public function getErrorPagesDir()
    {
        return $this->errorPages;
    }
}