<?php

namespace ErrorModule\Utils;

use DateTime;
use SplFileInfo;

class ExceptionFile
{

    /**
     * @var string
     */
    private $fileId;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $description;

    /**
     * @var DateTime
     */
    private $dtc;

    /**
     * @param string $fileId
     * @param string $title
     * @param string $description
     * @param DateTime $dtc
     */
    public function __construct($fileId, $title, $description, DateTime $dtc)
    {

        $this->fileId = $fileId;
        $this->title = $title;
        $this->description = $description;
        $this->dtc = $dtc;
    }

    /**
     * @param SplFileInfo $splFileInfo
     * @return ExceptionFile
     */
    public static function fromSpl(SplFileInfo $splFileInfo)
    {

        preg_match_all('/tracyBluescreenError.+?<h1>(.+?)<\/h1>.*?<p>(.+?)(<a|<\/p>)/s', file_get_contents($splFileInfo->getRealPath()), $matches);
        $title = isset($matches[1][0]) ? $matches[1][0] : 'Title Not found';
        $description = isset($matches[2][0]) ? $matches[2][0]  : 'Description Not found';
        $exceptionFile = new self($splFileInfo->getFilename(), $title, $description, DateTime::createFromFormat('U', $splFileInfo->getCTime()));
        return $exceptionFile;
    }

    /**
     * @return string
     */
    public function getFileId()
    {
        return $this->fileId;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @return string
     */
    public function getDtcString()
    {
        return $this->dtc->format('Y-m-d H:i:s');
    }
}