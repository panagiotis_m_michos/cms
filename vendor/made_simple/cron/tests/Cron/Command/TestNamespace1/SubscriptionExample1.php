<?php

namespace Cron\Command\TestNamespace1;

class SubscriptionExample1 extends \Cron\Commands\CommandAbstract
{

    public function execute()
    {
        $now = new \DateTime();
        $time = !empty($this->lastExecuted) ? $this->lastExecuted->format('Y-m-d H:m:s') : 'not defined';
        echo 'Last Execution: ' . $time . PHP_EOL;
        echo 'Now: ' . $now->format('Y-m-d H:m:s') . PHP_EOL;
        $this->executed = true;
    }

}