<?php

namespace Cron;

use Notifier\Factories\ClientServiceFactory;
use Notifier\Factories\ValidationFactory;
use FApplication;
use Registry;

class CustomFactory
{
    public static function create()
    {
        $config = FApplication::$config['notifier'];
        $clientServiceFactory = new ClientServiceFactory(ValidationFactory::create());
        $notifier = $clientServiceFactory->create('notifier', $config['url'], $config['defaultParameters'], $config['oauth']);
        $notifier->setDefaultNamespace($config['defaultNamespace']);
        return Factory::createDi(CRON_DIR, LOG_DIR, CRON_CACHE_DIR, $notifier, Registry::$container);
    }

    public static function loadConfig($configName)
    {
        $configPath = CRON_DIR . DIRECTORY_SEPARATOR . 'config';
        return Factory::loadConfig($configPath, $configName);
    }

}
