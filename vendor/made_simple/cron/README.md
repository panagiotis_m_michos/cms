## Example of the structure

    -- Cron
      -- Commands
        -- DbUpdate
          -- Payments
      -- config 
        -- daily.neon
      -- index.php

## Example of the initialization file (index.php)

    /**
     * define all dependencies here
     */

    $serverHost = php_uname("n");
    if (strpos($serverHost, 'rackspace2.') === false) {
        if (!defined('ENVIRONMENT_DEVELOPMENT')) {
            define('ENVIRONMENT_DEVELOPMENT', 1); 
        }
    }

    // absolute filesystem path to the web files
    define('DOCUMENT_ROOT', dirname(dirname(dirname(dirname(__FILE__)))) . '/');

    define('WWW_DIR', DOCUMENT_ROOT);

    // absolute filesystem path to the project
    define('APP_DIR', DOCUMENT_ROOT . 'app/');

    // path to admin
    define('ADMIN_ROOT', DOCUMENT_ROOT . 'admin/');

    // absolute filesystem path to the libraries
    define('LIBS_DIR', DOCUMENT_ROOT . 'libs/');

    define('TEST_DIR', DOCUMENT_ROOT . 'tests/');

    define('CRON_CACHE_DIR', DOCUMENT_ROOT . 'temp/');
    define('LOG_DIR', DOCUMENT_ROOT . 'logs/');

    define('GEDMO_LOGGABLE_USERNAME', 'cron');

    // include settings
    include_once APP_DIR . '/bootstrap.php';

    set_time_limit(0);
    ini_set('memory_limit', '1024M');

    $cachePath = CRON_CACHE_DIR . DIRECTORY_SEPARATOR . 'cron';
    $logPath = LOG_DIR . 'command.output';
    $logger = new Cron\CronLogger($logPath, ILogger::DEBUG, new Colors());
    $cache = new Doctrine\Common\Cache\FilesystemCache($cachePath);
    $cron = new Cron\Cron($cache, $logger, dirname(dirname(__FILE__)));
    Cron\Backup::createBackup($logPath, LOG_DIR . 'cronJobsLog');

    $configName = isset($argv[1]) ? $argv[1] : null;
    $timeToExecute = isset($argv[2]) ? $argv[2] : null;
    if ($configName) {
        $configPath = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'config';
        if (file_exists($configPath . DIRECTORY_SEPARATOR . $configName)) {
            $config = ConfigAdapterNeon::load($configPath . DIRECTORY_SEPARATOR . $configName);
            $cron->executeConfig($config);
        }
        else {
            $cron->executeCommand($configName, $timeToExecute);
        }
    } else {
        throw new InvalidArgumentException('Please provide the name of the config file!');
    }


## How it works and how to execute ?

* php app/libs/Cron/index.php daily.neon (will execute commands in the config file that needs to be run)
* php app/libs/Cron/index.php Queue\\EmailQueueProcessor (will execute one file)


## Example of the configuration file which runs the command (daily.neon)

    Commands:
    # each command declared here correspond to class name ( it will be uppercased )
    # time format can be anything here it is passed to timeToExecute method unmodified
    # example: namespace : SubscriptionExample = "15 sec"
        DbUpdate:
            ServiceStatus = "00:00:01"

## Example of the cron command

    :::php
    namespace Cron\Commands\DbUpdate;

    use Cron\Commands\ICommand;
    use Cron\Commands\CommandAbstract;

    class ServiceStatus extends CommandAbstract implements ICommand
    {

        private static $ids = array(
            "61",
            "270",
            "1126",
            "1593",
            "2298",
            "2712",
            "3713",
            "5320",
            "6932",
            "7334",
            "8026",
        );

        /**
         * @var string
         */
        protected $timeType = self::TIME_TYPE_FIXED;

        /**
         * @return void
         */
        public function execute()
        {
            /* @var $em EntityManager */
            $em = \Environment::getEntityManager();
            $i = 1;
            $batchSize = 50;
            $serviceService = new \DoctrineServices\ServiceService();
            $rows = $serviceService->findByIdsIterator('serviceId', self::$ids);
            foreach ($rows as $row) {
                $service = $row[0];
                $this->logger->logMessage(\ILogger::DEBUG, 'Sending jam for service `' . $service->getServiceId() . '`');
                if (($i % $batchSize) == 0) {
                    $em->clear(); // Detaches all objects from Doctrine!
                }
                $i++;
            }
        }

    }

 
