<?php

namespace Cron\Commands;

use DateTime;

interface ICommand
{

    /**
     * $timeToExecute - time provided in configuration
     * will be called every time the commands are loaded
     *
     * @param string $timeToExecute
     */
    public function setTimeToExecute($timeToExecute);

    /**
     * return TRUE if its time to execute script
     * @return bool
     */
    public function isTimeToExecute();

    /**
     * return when the script should be executed
     * return \DateTime|NULL
     */
    public function getTimeToExecute();

    /**
     * main logic to execute command
     */
    public function execute();

    /**
     * mark when the command was executed
     *
     * @param DateTime $dateTime
     */
    public function setLastExecuted(DateTime $dateTime);

    /**
     * @return DateTime
     */
    public function getLastExecuted();

    /**
     * @param bool $dryRun
     */
    public function setDryRun($dryRun = TRUE);

    /**
     * @param DateTime $runDate
     */
    public function setRunDate($runDate = NULL);

    /**
     * @return bool
     */
    public function isDryRun();

    /**
     * @param array $options
     */
    public function setOptions($options);

    /**
     * @return array
     */
    public function getOptions();

    /**
     * @return bool
     */
    public function hasOption($option);

    /**
     * get command name
     *
     * @return string
     */
    public function getName();
    
}
