<?php

/**
 * CMS
 *
 * @license    GPL
 * @category   Project
 * @package    Cron
 * @author     Tomas Jakstas tomasj@madesimplegroup.com
 * @version    2011-03-14
 */

namespace Cron\Commands;

use Cron\INotifier;
use DateInterval;
use DateTimeZone;
use ILogger;
use DateTime;
use InvalidArgumentException;

abstract class CommandAbstract implements ICommand
{

    const TIME_TYPE_FIXED = 'fixed';
    const TIME_TYPE_INTERVAL = 'interval';

    /**
     * should the script be executed at specific time or in itervals
     * @var string
     */
    protected $timeType = self::TIME_TYPE_INTERVAL;

    /**
     * when the script will be executed
     * @var string
     */
    protected $timeToExecute;

    /**
     * when the script was last executed
     * @var DateTime
     */
    protected $lastExecuted;

    /**
     * @var ILogger
     */
    protected $logger;

    /**
     * @var INotifier
     */
    protected $notifier;

    /**
     * @var bool
     */
    protected $dryRun;

    /**
     * @var DateTime
     */
    protected $runDate;

    /**
     * @var array
     */
    protected $options = array();

    /**
     * @param ILogger $logger
     * @param INotifier $notifier
     */
    public function __construct(ILogger $logger, INotifier $notifier)
    {
        $this->logger = $logger;
        $this->notifier = $notifier;
    }

    /**
     * @param string $timeToExecute
     */
    public function setTimeToExecute($timeToExecute)
    {
        $this->timeToExecute = $timeToExecute;
    }

    /**
     * @return DateTime|FALSE
     */
    public function isTimeToExecute()
    {

        $now = new DateTime();
        $execute = FALSE;

        if ($this->timeType == self::TIME_TYPE_INTERVAL) {
            $interval = DateInterval::createFromDateString($this->timeToExecute);
            $execute = empty($this->lastExecuted) || ($now->sub($interval) > $this->lastExecuted);
        } else {
            //time taken from configuration
            $exeTime = new DateTime($this->timeToExecute);
            $execute = empty($this->lastExecuted) ? $exeTime < $now : ($exeTime > $this->lastExecuted && $exeTime < $now);
        }
        return $execute;
    }

    /**
     * @return DateTime|NULL
     */
    public function getTimeToExecute()
    {
        $now = new DateTime();
        if ($this->timeType == self::TIME_TYPE_INTERVAL) {
            if (empty($this->lastExecuted)) {
                return $now;
            }
            $interval = DateInterval::createFromDateString($this->timeToExecute);
            if ($now->sub($interval) > $this->lastExecuted) {
                return $now->sub($interval);
            } else {
                return $this->lastExecuted->add($interval);
            }
        } else {
            //time taken from configuration
            $exeTime = new DateTime($this->timeToExecute);
//            $execute = empty($this->lastExecuted) ? $exeTime < $now : ($exeTime > $this->lastExecuted && $exeTime < $now);
            if (empty($this->lastExecuted)) {
                return $now > $exeTime ? $now : $exeTime;
            } else {
                if ($exeTime > $this->lastExecuted) {
                    return $now > $exeTime ? $now : $exeTime;
                } else {
                    //execution time is unknow
                    return NULL;
                }
            }
        }
    }

    /**
     * @param DateTime $dateTime
     */
    public function setLastExecuted(DateTime $dateTime)
    {
        $this->lastExecuted = $dateTime;
    }

    /**
     * @return DateTime
     */
    public function getLastExecuted()
    {
        return $this->lastExecuted;
    }

    /**
     * @param bool $dryRun
     */
    public function setDryRun($dryRun = TRUE)
    {
        $this->dryRun = $dryRun;
    }

    /**
     * @param DateTime $runDate
     */
    public function setRunDate($runDate = NULL)
    {
        $this->runDate = $runDate;
    }

    /**
     * @return bool
     */
    public function isDryRun()
    {
        return $this->dryRun;
    }

    /**
     * @param array $options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }

    /**
     * @param array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param string $option
     * @return bool
     */
    public function hasOption($option)
    {
        return in_array($option, $this->options);
    }

    /**
     * @return string
     */
    public function getName()
    {
        $name = get_class($this);
        $name = strtolower($name);
        return str_replace('\\', '.', $name);
    }

}
