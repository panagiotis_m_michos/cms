<?php

namespace Cron;

use ILogger;
use Debug;

class CronLogger implements ILogger {

    /**
     * @var int
     */
    private $level;

    /**
     * @var string
     */
    private $logPath;

    /**
     * @var Colours
     */
    private $colors;


    /**
     * @param string $logPath
     * @param int $level
     * @param Colors $colors
     */
    public function __construct($logPath, $level = self::DEBUG, Colors $colors = NULL) {
        $this->logPath = $logPath;
        $this->level = $level;
        $this->colors = $colors;
    }

    /**
     * logMessage
     *
     * @param int $level
     * @param string $message
     */
    public function logMessage($level, $message = NULL) {
        //there is no need to output messages greater than the default level
        if ($level > $this->level) {
            return;
        }
        $messageType = $this->getMessageType($level);
        $text = date('Y-m-d H:i:s') . ': ' . $messageType . ': '. $message . PHP_EOL;
        if ($this->colors) {
            $consoleText = date('Y-m-d H:i:s') . ': ' . $messageType . ': '. $this->colors->getColoredString($message, $this->getColour($level)) . PHP_EOL;
        } else {
            $consoleText = $text;
        }
        echo $consoleText;
        $appendStatus = file_put_contents($this->logPath, $text, FILE_APPEND);
        if ($level === self::ERROR && class_exists('Debug')) {
            Debug::log('Cron error. Message: ' . $message , Debug::ERROR);
        }
        return $appendStatus;
    }

    /**
     * @param int $level
     */
    public function setMinimumLogLevel($level) {
        $this->level = $level;
    }

    /**
     * @return string
     */
    public function getLogPath() {
        return $this->logPath;
    }

    /**
     * @param string $level
     * @return string
     */
    private function getMessageType($level) {
        switch ($level) {
        case self::ERROR : return 'Error';
        case self::WARNING : return 'Warning';
        case self::DEBUG : return 'Debug';
        default : return 'Info';
        }
    }

    /**
     * @param string $level
     * @return string|null
     */
    private function getColour($level) {
        switch ($level) {
        case self::ERROR : return 'red';
        case self::WARNING : return 'yellow';
        case self::DEBUG : return NULL;
        case self::INFO : return 'green';
        default : return NULL;
        }
    }
}
