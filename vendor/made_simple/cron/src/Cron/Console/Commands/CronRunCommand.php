<?php

namespace Cron\Console\Commands;

use DateTime;
use DateTimeZone;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use InvalidArgumentException;
use Cron\Cron;
use Cron\Helpers\CronConfig;

class CronRunCommand extends Command
{

    /**
     * @var Cron
     */
    private $cron;

    /**
     * @var string
     */
    private $configPath;

    /**
     *
     * @param Cron $cron
     * @param string $configPath
     * @param string $name
     */
    public function __construct(Cron $cron, $configPath, $name = NULL)
    {
        $this->cron = $cron;
        $this->configPath = $configPath;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('cron:run')
            ->setDescription('Run cron commands')
            ->addOption('config', NULL, InputOption::VALUE_REQUIRED, 'Provide config file')
            ->addOption('command', NULL, InputOption::VALUE_REQUIRED, 'Provide command name')
            ->addOption('section', NULL, InputOption::VALUE_REQUIRED, 'Section name in a configuration file', 'Commands')
            ->addOption('dummy', NULL, InputOption::VALUE_NONE, 'Run but do not execute actual command')
            ->addOption('dry-run', NULL, InputOption::VALUE_NONE, 'Set dry-run flag which can be used to avoid executing some parts of command')
            ->addOption('option', NULL, InputOption::VALUE_IS_ARRAY | InputOption::VALUE_OPTIONAL, 'Other options for specific command')
            ->addOption('run-date', NULL, InputOption::VALUE_REQUIRED, 'Used to simulate the cron running on a different date (to be specified in the dd/mm/yyyy and any format DateTime excepts). (USE WITH --dry-run');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $configName = $input->getOption('config');
        $commandName = $input->getOption('command');
        $section = $input->getOption('section');
        $dummy = $input->getOption('dummy');
        $dryRun = $input->getOption('dry-run');
        $options = $input->getOption('option');
        $runDate = $input->getOption('run-date');

        if ($dummy) {
            $output->writeln("<info>Executing in dummy mode...</info>");
            $this->cron->setDummy(TRUE);
        }
        if ($dryRun) {
            $output->writeln("<info>Executing in dry-run mode...</info>");
            $this->cron->setDryRun(TRUE);
        }
        if ($runDate) {
            // old behaviour
            $dateObject = DateTime::createFromFormat('d/m/Y', $runDate);
            if (!$dateObject) {
                try {
                    $dateObject = new DateTime($runDate);
                } catch (Exception $e) {
                    throw new InvalidArgumentException('Invalid run-date format `' . $runDate . '`. E.g. formats 01/12/2014, -1 day, -1 week, 2015-04-10');
                }
            }
            $dateObject->setTime(0,0,0);
            $output->writeln("<info>Simulate cron is running on " . $dateObject->format('d/m/Y') . " ...</info>");
            $this->cron->setRunDate($dateObject);
        }
        if ($options) {
            $this->cron->setOptions($options);
        }
        if ($configName) {
            $output->writeln("<info>Reading info from <comment>$configName</comment></info>");
            $config = CronConfig::loadConfig($this->configPath, $configName);
            $this->cron->executeConfig($config, $section);
        } else if ($commandName) {
            $output->writeln("<info>Reading info for <comment>$commandName</comment></info>");
            $this->cron->executeCommand($commandName, NULL, $section);
        } else {
            $output->writeln("<info>Nothing to do.</info>");
        }
    }
}
