<?php

namespace Cron\Console\Commands;

use Finder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use InvalidArgumentException;
use DomainException;
use Cron\Cron;
use Cron\Helpers\CronConfig;

class CronInfoCommand extends Command
{

    /**
     * @var Cron
     */
    private $cron;

    /**
     * @var string
     */
    private $configPath;

    /**
     * @param Cron $cron
     * @param string $configPath
     * @param string $name
     */
    public function __construct(Cron $cron, $configPath, $name = NULL)
    {
        $this->cron = $cron;
        $this->configPath = $configPath;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('cron:info')
            ->setDescription('Produce Information about cron commands')
            ->addOption('config', NULL, InputOption::VALUE_REQUIRED, 'Provide config file')
            ->addOption('command', NULL, InputOption::VALUE_REQUIRED, 'Provide command name')
            ->addOption('section', NULL, InputOption::VALUE_REQUIRED, 'Section name in a configuration file', 'Commands');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $configName = $input->getOption('config');
        $command = $input->getOption('command');
        $section = $input->getOption('section');
        if ($configName) {
            $output->writeln("<info>Reading info from <comment>$configName</comment></info>");
            $config = CronConfig::loadConfig($this->configPath, $configName);
            $this->produceInfoFromConfig($output, $config, $section);
        } else if ($command) {
            if (strpos($command, '\\') === FALSE) {
                throw new InvalidArgumentException('Invalid command format `' . $command . '`. Use namespace\\command format');
            }
            list($namespace, $commandName) = explode('\\', $command);
            $timeToExecute = '- 1 day';
            $class = $this->cron->getClass($section, $namespace, $commandName);
            $command = $this->cron->loadOneJob($class, $timeToExecute);
            $output->writeln("<info>Reading info for <comment>$namespace\\\\$commandName</comment></info>");
            $this->produceInfo($output, $command, $namespace, $commandName);
        } else {
            $path = $this->cron->getModuleDirectory() . DIRECTORY_SEPARATOR . $section;
            $output->writeln("<info>Reading info about all commands from path <comment>$path</comment></info>");
            $this->produceInfoFromFilesystem($output, $section, $path);
        }
    }

    private function produceInfoFromFilesystem($output, $section, $path)
    {
        $allFiles = Finder::findFiles('*.php')->from($path);
        foreach ($allFiles as $file) {
            $commandName = $file->getBasename('.php');
            if (strpos($commandName, 'Abstract') !== FALSE) {
                continue;
            }
            $namespace = basename(dirname($file->getRealPath()));
            $timeToExecute = '-2 days';
            $class = $this->cron->getClass($section, $namespace, $commandName);
            $command = $this->cron->loadOneJob($class, $timeToExecute);
            $this->produceInfo($output, $command, $namespace, $commandName);
        }
    }

    private function produceInfoFromConfig($output, $config, $section)
    {
        $sectionConfig = isset($config[$section]) ? (array) $config[$section] : array();
        foreach ($sectionConfig as $namespace => $commands) {
            foreach ((array) $commands as $commandName => $timeToExecute) {
                $class = $this->cron->getClass($section, $namespace, $commandName);
                $command = $this->cron->loadOneJob($class, $timeToExecute);
                $this->produceInfo($output, $command, $namespace, $commandName);
            }
        }
    }

    private function produceInfo($output, $command, $namespace, $commandName)
    {
        $lastExecuted = $command->getLastExecuted() ? $command->getLastExecuted()->format('Y-m-d H:i:s') : 'unknown';
        $nextExecuted = $command->getTimeToExecute() ? $command->getTimeToExecute()->format('Y-m-d H:i:s') : 'unknown';
        $output->writeln("<info>Name: <comment>$namespace\\\\$commandName</comment> Last Executed: <comment>{$lastExecuted}</comment> Next execution:<comment>{$nextExecuted}</comment></info>");
    }

}
