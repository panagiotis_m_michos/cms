<?php

namespace Cron;

use DateTime;
use SplFileObject;

class Backup
{
    /**
     * creates cron log backups in a backup folder
     * @param string $logPath
     * @param string $backUpDir
     * @param int $backUpSize
     * @return string
     */
    public static function createBackup($logPath, $backUpDir, $backUpSize = 1000000)
    {
        if (file_exists($logPath)) {
            $fileCron = new SplFileObject($logPath, 'r');
            if ($fileCron->isWritable()) {
                $currentSize = $fileCron->getSize();
                if ($currentSize > $backUpSize) {
                    if (!file_exists($backUpDir)) {
                        mkdir($backUpDir);
                    }
                    $backupDate = new DateTime();
                    $filename = basename($logPath);
                    $backUpPath = $backUpDir . DIRECTORY_SEPARATOR . $filename . '.' . $backupDate->format('Y-m-d-H-i-s');
                    rename($logPath, $backUpPath);
                    return $backUpPath;
                }
            }
        }
    }

}