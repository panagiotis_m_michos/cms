<?php

namespace Cron;

interface INotifier
{
    public function triggerSuccess($eventKey, $objectId, $message);
    public function triggerWarning($eventKey, $objectId, $message);
    public function triggerFailure($eventKey, $objectId, $message);
    public function setDefaultNamespace($namespace);
}
