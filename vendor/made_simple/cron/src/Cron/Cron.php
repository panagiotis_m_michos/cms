<?php

namespace Cron;

use BootstrapModule\Ext\DoctrineExt;
use Cron\Commands\ICommand;
use DateTime;
use Debug;
use Doctrine\Common\Cache\Cache;
use Doctrine\ORM\EntityManager;
use Exception;
use ILogger;
use LogicException;
use Registry;
use StatCollector;

class Cron
{

    /**
     * @var path to cron module directory
     */
    protected $cronModuleDir;

    /**
     * @var Cache
     */
    protected $cache;

    /**
     * @var ILogger
     */
    protected $logger;

    /**
     * @var INotifier
     */
    protected $notifier;

    /**
     * if set to TRUE actual command will not be executed
     * @var bool
     */
    protected $dummy = FALSE;

    /**
     * @var bool
     */
    protected $dryRun = FALSE;

    /**
     * @var DateTime
     */
    protected $runDate = NULL;

    /**
     * @var array
     */
    protected $options = array();

    /**
     * @param Cache $cache
     * @param ILogger $logger
     * @param string $cronModuleDir
     * @param INotifier $notifier
     */
    public function __construct(Cache $cache, ILogger $logger, $cronModuleDir, INotifier $notifier)
    {
        $this->cache = $cache;
        $this->cronModuleDir = $cronModuleDir;
        $this->logger = $logger;
        $this->notifier = $notifier;
    }

    /**
     * get full class name path
     * @param string $section
     * @param string $namespace
     * @param string $commandName
     * @return string
     */
    public function getClass($section, $namespace, $commandName, $rootNamespace = 'Cron')
    {
        $name = ucfirst(basename($commandName));
        $class = $rootNamespace . '\\' . $section . '\\' . ucfirst($namespace) . '\\' . $name;
        return $class;
    }

    /**
     * run commands from config
     * @param array $config
     * @param string $section
     * @return array<ICommand>
     */
    public function executeConfig($config, $section = 'Commands')
    {
        try {
            $executed = array();
            $this->logger->logMessage(ILogger::INFO, 'Start...');
            $sectionConfig = isset($config[$section]) ? (array) $config[$section] : array();
            foreach ($sectionConfig as $namespace => $commands) {
                foreach ((array) $commands as $commandName => $timeToExecute) {
                    // TODO: temporary solution to avoid conflicts with SelfCleaningIterator and entities
                    /** @var EntityManager $em */
                    $em = Registry::$container->get(DoctrineExt::ENTITY_MANAGER);
                    $em->clear();

                    $class = $this->getClass($section, $namespace, $commandName);
                    $command = $this->loadOneJob($class, $timeToExecute);
                    if ($command->isTimeToExecute()) {
                        $this->runCommand($command);
                        $executed[] = $command;
                    }
                }
            }
        } catch (Exception $e) {
            $this->logger->logMessage(ILogger::ERROR, $e->getMessage());
        }
        $this->logger->logMessage(ILogger::INFO, 'End.');
        return $executed;
    }

    /**
     *
     * @param string $commandName
     * @param string $timeToExecute
     * @param string $section
     * @return ICommand
     */
    public function executeCommand($commandName, $timeToExecute = NULL, $section = 'Commands', $rootNamespace = 'Cron')
    {
        try {
            $command = NULL;
            $this->logger->logMessage(ILogger::INFO, 'Start...');
            $time = $timeToExecute ? $timeToExecute : '1 sec';
            $class = $rootNamespace . '\\' . $section . '\\' . $commandName;
            $command = $this->loadOneJob($class, $time);
            $this->runCommand($command);
        } catch (Exception $e) {
            $this->logger->logMessage(ILogger::ERROR, $e->getMessage());

        }
        $this->logger->logMessage(ILogger::INFO, 'End.');
        return $command;
    }

    /**
     * load one job to a queue
     * @param string $class
     * @param string $time
     * @return ICommand
     */
    public function loadOneJob($class, $time)
    {
        $pathName = str_replace('\\', '/', $class);
        $directory = dirname($this->cronModuleDir);
        $filePath = $directory . DIRECTORY_SEPARATOR . $pathName . '.php';
        if (file_exists($filePath)) {
            require_once $filePath;
            $obj = new $class($this->logger, $this->notifier);
            $obj->setTimeToExecute($time);
            $name = $obj->getName();
            if ($this->cache->contains($name)) {
                $lastExecuted = $this->cache->fetch($name);
                $obj->setLastExecuted($lastExecuted);
            }
            return $obj;
        } else {
            throw new LogicException('File `' . $filePath. '` does not exists!');
        }
    }

    /**
     * @param ICommand $command
     */
    public function runCommand(ICommand $command)
    {
        try {
            $name = $command->getName();
            $this->logger->logMessage(ILogger::INFO, '| Execute start ' . $name);
            $startTime = microtime(TRUE);
            if (!$this->dummy) {
                $command->setDryRun($this->dryRun);
                $command->setRunDate($this->runDate);
                $command->setOptions($this->options);
                $command->execute();
            }
            $lastExecuted = new DateTime();
            $command->setLastExecuted($lastExecuted);
            $this->cache->save($name, $lastExecuted);
            $this->logger->logMessage(ILogger::INFO, '| Execute end ' . $name);
        } catch (Exception $e) {
            $this->logger->logMessage(ILogger::ERROR, $e->getMessage());
            Debug::log('Cron script `' . $name . '` failed. Message: ' . $e->getMessage(), Debug::ERROR);
            $this->notifier->triggerFailure($command->getName(), time(), $e->getMessage());
        }
    }

    /**
     * @param bool $dummy
     */
    public function setDummy($dummy)
    {
        $this->dummy = $dummy;
    }

    /**
     * @param bool $dryRun
     */
    public function setDryRun($dryRun)
    {
        $this->dryRun = $dryRun;
    }

    /**
     * @param DateTime $runDate
     */
    public function setRunDate($runDate)
    {
        $this->runDate = $runDate;
    }

    /**
     * @param array $options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }

    /**
     * @return string
     */
    public function getModuleDirectory()
    {
        return $this->cronModuleDir;
    }

}
