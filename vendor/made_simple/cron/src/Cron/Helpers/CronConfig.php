<?php

namespace Cron\Helpers;

use ConfigAdapterNeon;
use SplFileInfo;
use InvalidArgumentException;
use Symfony\Component\Yaml\Parser;

class CronConfig
{

    /**
     * @param string $configPath
     * @param string $configName
     * @throws InvalidArgumentException
     */
    public static function loadConfig($configPath, $configName)
    {
        if (file_exists($configPath . DIRECTORY_SEPARATOR . $configName)) {
            $file = new SplFileInfo($configPath . DIRECTORY_SEPARATOR . $configName);
            if ($file->getExtension() === 'yml') {
                $yaml = new Parser();
                return $yaml->parse(file_get_contents($file->getRealPath()));
            } else if ($file->getExtension() === 'neon') {
                return ConfigAdapterNeon::load($file->getRealPath());
            } else {
                throw new InvalidArgumentException('Not supported format. Supported formats yml,neon');
            }
        } else {
            throw new InvalidArgumentException('Config file `' . $configPath . DIRECTORY_SEPARATOR . $configName . '` does not exist!');
        }
    }

}
