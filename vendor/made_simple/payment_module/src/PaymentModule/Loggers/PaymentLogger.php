<?php

namespace PaymentModule\Loggers;

use Exception;
use PaymentModule\Contracts\IPaymentData;
use Psr\Log\LoggerInterface;
use RouterModule\Helpers\ControllerHelper;
use SagePay\Token\Exception\FailedResponse;
use SagePay\Token\Exception\Transaction;
use SagePay\Token\Exception\ValidationError;
use TranslationModule\Config\MsgId;
use UserModule\Contracts\ICustomer;

class PaymentLogger implements IPaymentLogger
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    /**
     * @var IPaymentLogger
     */
    private $transactionHandler;

    /**
     * PaymentLogger constructor.
     * @param LoggerInterface $logger
     * @param ControllerHelper $controllerHelper
     * @param IPaymentLogger $transactionHandler
     */
    public function __construct(LoggerInterface $logger, ControllerHelper $controllerHelper, IPaymentLogger $transactionHandler)
    {
        $this->logger = $logger;
        $this->controllerHelper = $controllerHelper;
        $this->transactionHandler = $transactionHandler;
    }

    /**
     * @param Exception $e
     * @param ICustomer $customer
     * @param IPaymentData $paymentData
     * @param $message
     */
    public function error(
        Exception $e, 
        ICustomer $customer, 
        IPaymentData $paymentData,
        $message = NULL
    )
    {
        if ($e instanceof FailedResponse) {
            if ($e->getCode() == 2000) {
                $message .= s_(MsgId::PAYMENT_CODE_2000);
            } elseif ($e->getCode() == 2001) {
                $message .= s_(MsgId::PAYMENT_CODE_2001);
            }
        }
        if ($message) {
            $this->controllerHelper->setFlashMessage($message, ControllerHelper::MESSAGE_ERROR, NULL, FALSE);
        }
        $this->transactionHandler->error($e, $customer, $paymentData);
        if ($e instanceof ValidationError || $e instanceof Transaction) {
            return;
        }
        if ($e instanceof FailedResponse) {
            if (in_array($e->getCode(), [2000, 2001, 4027])) {
                return;
            }
        }
        $this->logger->error((string) $e);
    }
}