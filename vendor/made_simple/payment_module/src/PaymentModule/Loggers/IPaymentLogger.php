<?php

namespace PaymentModule\Loggers;

use Exception;
use PaymentModule\Contracts\IPaymentData;
use UserModule\Contracts\ICustomer;

interface IPaymentLogger
{
    /**
     * @param Exception $e
     * @param ICustomer $customer
     * @param IPaymentData $paymentData
     * @param string|NULL$message
     */
    public function error(Exception $e, ICustomer $customer, IPaymentData $paymentData, $message = NULL);
}