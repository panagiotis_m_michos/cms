<?php

namespace PaymentModule\Dto;

use BasketModule\Contracts\IBasket;
use JsonSerializable;
use PaymentModule\Contracts\IPaymentData;
use PaymentModule\Contracts\IPaymentResponse;
use PaymentModule\Contracts\IToken;
use UserModule\Contracts\ICustomer;

class PaymentDetails implements IPaymentData, JsonSerializable
{
    /**
     * @var string
     */
    public $paymentType;

    /**
     * @var CardDetails
     */
    public $cardDetails;

    /**
     * @var PayerDetails
     */
    public $payerDetails;

    /**
     * @var string
     */
    public $mediumId;

    /**
     * @var string
     */
    public $currency;

    /**
     * @var bool
     */
    public $useCredit;

    /**
     * @var bool
     */
    private $saveToken;

    /**
     * @var IToken
     */
    private $token;

    /**
     * @var IBasket
     */
    private $basket;


    /**
     * @param string $paymentType
     * @return PaymentDetails
     */
    public static function withType($paymentType)
    {
        $self = new self();
        $self->paymentType = $paymentType;
        return $self;
    }

    /**
     * @param string $currency
     * @param IBasket $basket
     * @param boolean $isTokenStorageRequired
     * @param ICustomer $customer
     * @param IToken $token
     * @return PaymentDetails
     */
    public static function create($currency,
                                  IBasket $basket,
                                  $isTokenStorageRequired,
                                  ICustomer $customer = NULL,
                                  IToken $token = NULL
    )
    {
        $self = new self();
        $self->cardDetails = $customer && $customer->getFullName()
            ?  CardDetails::fromCustomer($customer)
            : new CardDetails();
        $self->payerDetails = $customer && $customer->getPrimaryAddress()
            ? PayerDetails::fromAddress($customer->getPrimaryAddress())
            : new PayerDetails();
        $self->saveToken = $isTokenStorageRequired;
        $self->currency = $currency;
        $self->basket = $basket;
        $self->useCredit = $basket->isUsingCredit();
        $self->token = $token;
        return $self;
    }

    /**
     * @return string
     */
    public function getRealPaymentType()
    {
        switch ($this->paymentType) {
            case self::TYPE_SAGE_EXISTING_TOKEN:
            case self::TYPE_SAGE_NEW_TOKEN:
            case self::TYPE_SAGE_NO_TOKEN:
                return IPaymentResponse::TYPE_SAGE;
        }
        return $this->paymentType;
    }

    /**
     * @return bool
     */
    public function isCardPayment()
    {
        return in_array($this->paymentType, [self::TYPE_SAGE, self::TYPE_SAGE_NO_TOKEN, self::TYPE_SAGE_NEW_TOKEN]);
    }

    /**
     * @return bool
     */
    public function isUsingCredit()
    {
        return $this->useCredit;
    }

    /**
     * @return bool
     */
    public function isUsingToken()
    {
        return $this->paymentType === self::TYPE_SAGE_EXISTING_TOKEN;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->basket->getTotalPrice();
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return bool
     */
    public function hasToStoreToken()
    {
        return $this->saveToken || in_array($this->paymentType, [self::TYPE_SAGE_EXISTING_TOKEN, self::TYPE_SAGE_NEW_TOKEN]);
    }

    /**
     * @return CardDetails
     */
    public function getCardDetails()
    {
        return $this->cardDetails;
    }

    /**
     * @return PayerDetails
     */
    public function getPayerDetails()
    {
        return $this->payerDetails;
    }

    /**
     * @return IToken
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return array
     */
    function jsonSerialize()
    {
        return [
            'amount' => $this->getAmount(),
            'paymentType' => $this->paymentType,
            'useCredit' => $this->useCredit,
            'cardDetails' => $this->getCardDetails(),
            'payerDetails' => $this->getPayerDetails(),
        ];
    }
}