<?php

namespace PaymentModule\Dto;

use JsonSerializable;
use UserModule\Contracts\IAddress;

class PayerDetails implements JsonSerializable
{
    /**
     * @var string
     */
    public $firstName;

    /**
     * @var string
     */
    public $lastName;

    /**
     * @var string
     */
    public $address1;

    /**
     * @var string
     */
    public $address2;

    /**
     * @var string
     */
    public $city;

    /**
     * @var string
     */
    public $postcode;

    /**
     * @var string
     */
    public $country;

    /**
     * @var string
     */
    public $state;

    /**
     * @param IAddress $address
     * @return PayerDetails
     */
    public static function fromAddress(IAddress $address)
    {
        $self = new self();
        $self->address1 = $address->getAddress1();
        $self->address2 = $address->getAddress2();
        $self->city = $address->getCity();
        $self->postcode = $address->getPostCode();
        $self->country = $address->getCountryIso();
        return $self;
    }

    /**
     * @return bool
     */
    public function isUsa()
    {
        return $this->country === 'US';
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
            'address1' => $this->address1,
            'address2' => $this->address2,
            'city' => $this->city,
            'postcode' => $this->postcode,
            'country' => $this->country,
        ];
    }
}