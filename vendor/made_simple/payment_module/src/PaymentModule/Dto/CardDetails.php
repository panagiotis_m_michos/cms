<?php

namespace PaymentModule\Dto;

use JsonSerializable;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use UserModule\Contracts\ICustomer;
use Utils\Date;

class CardDetails implements JsonSerializable
{
    const CARD_MAESTRO = 'MAESTRO';
    const CARD_AMEX = 'AMEX';

    /**
     * @var string
     */
    public $cardholder;

    /**
     * @var string
     */
    public $cardType;

    /**
     * @var string
     */
    public $cardNumber;

    /**
     * @var Date
     */
    public $validFrom;

    /**
     * @var Date
     */
    public $expiryDate;

    /**
     * @var string
     */
    public $issueNumber;

    /**
     * @var string
     */
    public $securityCode;

    /**
     * @param ICustomer $customer
     * @return CardDetails
     */
    public static function fromCustomer(ICustomer $customer)
    {
        $self = new self();
        $self->cardholder = $customer->getFullName();
        return $self;
    }

    /**
     * @return bool
     */
    public function requiresAdditionalCheck()
    {
        return $this->cardType === self::CARD_MAESTRO;
    }
    
    /**
     * @return bool
     */
    public function requiresIssueNumber()
    {
        return $this->requiresAdditionalCheck() && !empty($this->issueNumber);
    }
    
    /**
     * @return bool
     */
    public function requiresStartDate()
    {
        return $this->requiresAdditionalCheck() && !empty($this->validFrom);
    }

    /**
     * @return bool
     */
    public function isAmex()
    {
        return $this->cardType === self::CARD_AMEX;
    }

    /**
     * @param ExecutionContextInterface $context
     */
    public function validateCv2(ExecutionContextInterface $context)
    {
        $valid = TRUE;
        $allowedDigits = 3;
        if ($this->isAmex() && strlen($this->securityCode) != 4) {
            $valid = FALSE;
            $allowedDigits = 4;
        } elseif (!$this->isAmex() && strlen($this->securityCode) != 3) {
            $valid = FALSE;
        }
        if (!$valid) {
            $context->buildViolation(sprintf('Must be %d digits', $allowedDigits))
                ->atPath('securityCode')
                ->addViolation();
        }

    }

    /**
     * return array
     */
    public function jsonSerialize()
    {
        return [
            'cardholder' => $this->cardholder,
            'cardType' => $this->cardType,
            'cardNumber' => $this->cardNumber,
            'validFrom' => $this->validFrom instanceof Date ? $this->validFrom->format('my') : NULL,
            'expiryDate' => $this->expiryDate instanceof Date ? $this->validFrom->format('my') : NULL,
            'issueNumber' => $this->issueNumber,
            'securityCode' => $this->securityCode,
        ];
    }
}