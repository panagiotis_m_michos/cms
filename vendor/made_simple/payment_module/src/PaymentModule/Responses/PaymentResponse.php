<?php

namespace PaymentModule\Responses;

use PaymentModule\Contracts\IPaymentResponse;
use PaymentModule\Contracts\IToken;
use PayPalNVP\Response\GetExpressCheckoutDetailsResponse;
use SagePay\Token\Request\Details;
use SagePay\Token\Response\Summary;
use Utils\Date;
use Utils\Exceptions\InvalidDateException;

class PaymentResponse implements IPaymentResponse
{
    const RECURRING = TRUE;

    /**
     * @var string
     */
    private $typeId;

    /**
     * @var string
     */
    private $cardHolder;

    /**
     * @var string
     */
    private $cardNumber;

    /**
     * @var string
     */
    private $cardType;

    /**
     * @var Date
     */
    private $cardExpiryDate;

    /**
     * @var string
     */
    private $sageVendorTxCode;

    /**
     * @var string
     */
    private $sageVpsTxId;

    /**
     * @var string
     */
    private $sageSecurityKey;

    /**
     * @var string
     */
    private $sageTxAuthNo;

    /**
     * @var string
     */
    private $sageToken;


    /**
     * @var string
     */
    private $paypalDetails;

    /**
     * @var string
     */
    private $paypalTransactionId;

    /**
     * @var string
     */
    private $androidTransactionId;

    /**
     * @var string
     */
    private $itunesTransactionId;

    /**
     * @var string
     */
    private $error;

    /**
     * @var bool
     */
    private $recurringPayment;

    /**
     * @var string
     */
    private $worldPayTransactionId;

    /**
     * @var GetExpressCheckoutDetailsResponse
     */
    private $paypalPaymentInfo;

    /**
     * @var Details;
     */
    private $sagePaymentInfo;

    /**
     * @var string
     */
    private $paymentMedium = self::MEDIUM_ON_SITE;

    /**
     * @var string
     */
    private $redirectUrl;

    private function __construct()
    {
    }

    /**
     * @return PaymentResponse
     */
    public static function paypal()
    {
        $self = new self();
        $self->typeId = self::TYPE_PAYPAL;
        return $self;
    }

    /**
     * @param string $customerName
     * @return PaymentResponse
     */
    public static function freePurchase($customerName)
    {
        $self = new self();
        $self->typeId = self::TYPE_FREE;
        $self->cardHolder = $customerName;
        return $self;
    }

    /**
     * @param string $customerName
     * @return PaymentResponse
     */
    public static function onAccount($customerName)
    {
        $self = new self();
        $self->typeId = self::TYPE_ON_ACCOUNT;
        $self->cardHolder = $customerName;
        return $self;
    }

    /**
     * @param string $paymentType
     * @param string $redirectionUrl
     * @return PaymentResponse
     */
    public static function createRedirection($paymentType, $redirectionUrl)
    {
        $self = new self();
        $self->typeId = $paymentType;
        $self->redirectUrl = $redirectionUrl;
        return $self;
    }

    /**
     * @param Details $details
     * @param Summary $summary
     * @return PaymentResponse
     * @throws InvalidDateException
     */
    public static function fromDetails(Details $details, Summary $summary)
    {
        $paymentResponse = new self();
        $paymentResponse->setTypeId(self::TYPE_SAGE);
        $paymentResponse->setCardNumber($summary->getCardNumber());
        $paymentResponse->setCardHolder($summary->getCardHolder());
        $paymentResponse->setCardType($summary->getCardType());
        $expiryDate = $summary->getCardExpiryDate();
        if (!empty($expiryDate)) {
            $date = Date::create('my', $expiryDate);
            $date->modify('last day of this month');
            $paymentResponse->setCardExpiryDate($date);
        }
        $paymentResponse->setSageVpsTxId($summary->getVpsTxId());
        $paymentResponse->setSageSecurityKey($summary->getSecurityKey());
        $paymentResponse->setSageTxAuthNo($summary->getTxAuthNo());
        $paymentResponse->setSageVendorTxCode($summary->getVendorTxCode());
        $paymentResponse->setSageToken($summary->getToken());
        $paymentResponse->setSagePaymentInfo($details);
        if ($details->getStoreToken() == 1) {
            $paymentResponse->setRecurringPayment(self::RECURRING);
        }
        return $paymentResponse;
    }

    /**
     * @param Details $details
     * @param Summary $summary
     * @param IToken $token
     * @return PaymentResponse
     */
    public static function fromToken(Details $details, Summary $summary, IToken $token)
    {
        $paymentResponse = self::fromDetails($details, $summary);
        $paymentResponse->setCardNumber($token->getCardNumber());
        $paymentResponse->setCardHolder($token->getCardHolder());
        $paymentResponse->setCardType($token->getCardType());
        $expiryDate = $token->getCardExpiryDate();
        if (!empty($expiryDate)) {
            $paymentResponse->setCardExpiryDate($expiryDate);
        }
        $paymentResponse->setSageToken($token->getIdentifier());
        return $paymentResponse;
    }

    /**
     * @return string
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     * @param string $typeId
     */
    public function setTypeId($typeId)
    {
        $this->typeId = $typeId;
    }

    /**
     * @return string
     */
    public function getCardHolder()
    {
        return $this->cardHolder;
    }

    /**
     * @param string $cardHolder
     */
    public function setCardHolder($cardHolder)
    {
        $this->cardHolder = $cardHolder;
    }

    /**
     * @return string
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * @param string $cardNumber
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;
    }

    /**
     * @return string
     */
    public function getSageVendorTxCode()
    {
        return $this->sageVendorTxCode;
    }

    /**
     * @param string $sageVendorTxCode
     */
    public function setSageVendorTxCode($sageVendorTxCode)
    {
        $this->sageVendorTxCode = $sageVendorTxCode;
    }

    /**
     * @return string
     */
    public function getSageVpsTxId()
    {
        return $this->sageVpsTxId;
    }

    /**
     * @param string $sageVpsTxId
     */
    public function setSageVpsTxId($sageVpsTxId)
    {
        $this->sageVpsTxId = $sageVpsTxId;
    }

    /**
     * @return string
     */
    public function getSageSecurityKey()
    {
        return $this->sageSecurityKey;
    }

    /**
     * @param string $sageSecurityKey
     */
    public function setSageSecurityKey($sageSecurityKey)
    {
        $this->sageSecurityKey = $sageSecurityKey;
    }

    /**
     * @return string
     */
    public function getSageTxAuthNo()
    {
        return $this->sageTxAuthNo;
    }

    /**
     * @param string $sageTxAuthNo
     */
    public function setSageTxAuthNo($sageTxAuthNo)
    {
        $this->sageTxAuthNo = $sageTxAuthNo;
    }

    /**
     * @return string
     */
    public function getSageToken()
    {
        return $this->sageToken;
    }

    /**
     * @param string $sageToken
     */
    public function setSageToken($sageToken)
    {
        $this->sageToken = $sageToken;
    }

    /**
     * @return string
     */
    public function getPaypalDetails()
    {
        return $this->paypalDetails;
    }

    /**
     * @param string $paypalDetails
     */
    public function setPaypalDetails($paypalDetails)
    {
        $this->paypalDetails = $paypalDetails;
    }

    /**
     * @return string
     */
    public function getPaypalTransactionId()
    {
        return $this->paypalTransactionId;
    }

    /**
     * @param string $paypalTransactionId
     */
    public function setPaypalTransactionId($paypalTransactionId)
    {
        $this->paypalTransactionId = $paypalTransactionId;
    }

    /**
     * @return string
     */
    public function getAndroidTransactionId()
    {
        return $this->androidTransactionId;
    }

    /**
     * @param string $androidTransactionId
     */
    public function setAndroidTransactionId($androidTransactionId)
    {
        $this->androidTransactionId = $androidTransactionId;
    }

    /**
     * @return string
     */
    public function getItunesTransactionId()
    {
        return $this->itunesTransactionId;
    }

    /**
     * @param string $itunesTransactionId
     */
    public function setItunesTransactionId($itunesTransactionId)
    {
        $this->itunesTransactionId = $itunesTransactionId;
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param string $error
     */
    public function setError($error)
    {
        $this->error = $error;
    }

    /**
     * @return bool
     */
    public function isRecurringPayment()
    {
        return $this->recurringPayment;
    }

    /**
     * @param boolean $recurringPayment
     */
    public function setRecurringPayment($recurringPayment)
    {
        $this->recurringPayment = $recurringPayment;
    }

    /**
     * @return string
     */
    public function getWorldPayTransactionId()
    {
        return $this->worldPayTransactionId;
    }

    /**
     * @param string $worldPayTransactionId
     */
    public function setWorldPayTransactionId($worldPayTransactionId)
    {
        $this->worldPayTransactionId = $worldPayTransactionId;
    }

    /**
     * @return GetExpressCheckoutDetailsResponse
     */
    public function getPaypalPaymentInfo()
    {
        return $this->paypalPaymentInfo;
    }

    /**
     * @param GetExpressCheckoutDetailsResponse $paypalPaymentInfo
     */
    public function setPaypalPaymentInfo($paypalPaymentInfo)
    {
        $this->paypalPaymentInfo = $paypalPaymentInfo;
    }

    /**
     * @return Details
     */
    public function getSagePaymentInfo()
    {
        return $this->sagePaymentInfo;
    }

    /**
     * @param Details $sagePaymentInfo
     */
    public function setSagePaymentInfo($sagePaymentInfo)
    {
        $this->sagePaymentInfo = $sagePaymentInfo;
    }

    /**
     * @return string
     */
    public function getPaymentMedium()
    {
        return $this->paymentMedium;
    }

    /**
     * @param string $paymentMedium
     */
    public function setPaymentMedium($paymentMedium)
    {
        $this->paymentMedium = $paymentMedium;
    }

    /**
     * @return string
     */
    public function getRedirectUrl()
    {
        return $this->redirectUrl;
    }

    /**
     * @param string $redirectUrl
     */
    public function setRedirectUrl($redirectUrl)
    {
        $this->redirectUrl = $redirectUrl;
    }

    /**
     * @return string
     */
    public function getCardType()
    {
        return $this->cardType;
    }

    /**
     * @param string $cardType
     */
    public function setCardType($cardType)
    {
        $this->cardType = $cardType;
    }

    /**
     * @return Date
     */
    public function getCardExpiryDate()
    {
        return $this->cardExpiryDate;
    }

    /**
     * @param Date $cardExpiryDate
     */
    public function setCardExpiryDate(Date $cardExpiryDate)
    {
        $this->cardExpiryDate = $cardExpiryDate;
    }
    
    /**
     * @return bool
     */
    public function isRedirectRequired()
    {
        return !empty($this->redirectUrl);
    }

    /**
     * @return bool
     */
    public function isPaypal()
    {
        return $this->typeId === self::TYPE_PAYPAL;
    }

    /**
     * @return bool
     */
    public function isSagePay()
    {
        return $this->typeId === self::TYPE_SAGE;
    }

    /**
     * @return string
     */
    public function getPaymentType()
    {
        return $this->typeId;
    }
}
