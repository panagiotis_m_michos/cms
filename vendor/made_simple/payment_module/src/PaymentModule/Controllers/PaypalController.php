<?php

namespace PaymentModule\Controllers;

use Basket;
use BasketModule\Contracts\IBasket;
use Exception;
use PaymentModule\Contracts\IPaymentData;
use PaymentModule\Contracts\IPaymentRedirection;
use PaymentModule\Contracts\IPaymentResponse;
use PaymentModule\Dto\PaymentDetails;
use PaymentModule\Exceptions\PaymentException;
use PaymentModule\Loggers\IPaymentLogger;
use PaymentModule\PaymentTypes\IPaymentMaker;
use PayPalNVP\Exception\DoExpressCheckoutException;
use PayPalNVP\Exception\RequestException;
use PayPalNVP\Exception\ResponseException;
use RouterModule\Helpers\ControllerHelper;
use Symfony\Component\HttpFoundation\RedirectResponse;
use TranslationModule\Config\MsgId;
use UserModule\Contracts\ICustomer;
use UserModule\Exceptions\CustomerRequiredException;
use UserModule\Services\CustomerAvailability;
use UserModule\Services\ICustomerAvailability;

class PaypalController
{
    /**
     * @var Basket
     */
    private $basket;

    /**
     * @var CustomerAvailability
     */
    private $customerAvailability;

    /**
     * @var IPaymentRedirection
     */
    private $paymentRedirection;

    /**
     * @var IPaymentMaker
     */
    private $paymentMaker;

    /**
     * @var IPaymentLogger
     */
    private $logger;

    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    /**
     * @param IPaymentRedirection $paymentRedirection
     * @param IBasket $basket
     * @param ICustomerAvailability $customerAvailability
     * @param IPaymentMaker $paymentMaker
     * @param IPaymentLogger $logger
     * @param ControllerHelper $controllerHelper
     */
    public function __construct(
        IPaymentRedirection $paymentRedirection,
        IBasket $basket,
        ICustomerAvailability $customerAvailability,
        IPaymentMaker $paymentMaker,
        IPaymentLogger $logger,
        ControllerHelper $controllerHelper
    )
    {
        $this->paymentRedirection = $paymentRedirection;
        $this->basket = $basket;
        $this->customerAvailability = $customerAvailability;
        $this->paymentMaker = $paymentMaker;
        $this->logger = $logger;
        $this->controllerHelper = $controllerHelper;
    }

    /**
     * @param IPaymentData $paymentData
     * @return RedirectResponse
     */
    public function payment(IPaymentData $paymentData)
    {
        try {
            $customer = $this->customerAvailability->requireCustomer();
            return $this->paymentMaker->makePayment($customer, $this->basket, $paymentData);
        } catch (PaymentException $e) {
            return $this->handleFailedPayment($customer, $e->getMessage(), $e, $paymentData);
        }
    }

    /**
     * @param string $token
     * @return RedirectResponse
     */
    public function validate($token)
    {
        $paymentData = PaymentDetails::withType(IPaymentResponse::TYPE_PAYPAL);
        try {
            $customer = $this->customerAvailability->requireCustomer();
            return $this->paymentMaker->authorizePayment($customer, $this->basket, $token);
        } catch (DoExpressCheckoutException $e) {
            return $this->handleFailedPayment($customer, $e->getMessage(), $e, $paymentData);
        } catch (RequestException $e) {
            return $this->handleFailedPayment($customer, $e->getMessage(), $e, $paymentData);
        } catch (ResponseException $e) {
            return $this->handleFailedPayment($customer, _(MsgId::GENERIC_RESPONSE_ERROR), $e, $paymentData);
        } catch (PaymentException $e) {
            return $this->handleFailedPayment($customer, $e->getMessage(), $e, $paymentData);
        } catch (CustomerRequiredException $e) {
            return $this->handleFailedPayment($customer, $e->getMessage(), $e, $paymentData);
        }
    }

    /**
     * @return RedirectResponse
     */
    public function cancel()
    {
        $this->controllerHelper->setFlashMessage(s_(MsgId::PAYMENT_CANCELED), ControllerHelper::MESSAGE_ERROR);
        return $this->paymentRedirection->getFailure();
    }

    /**
     * @param ICustomer $customer
     * @param string $message
     * @param Exception $e
     * @param IPaymentData $paymentData
     * @return RedirectResponse
     */
    private function handleFailedPayment(ICustomer $customer, $message, Exception $e, IPaymentData $paymentData)
    {
        $this->logger->error($e, $customer, $paymentData, $message);
        return $this->paymentRedirection->getFailure($e);
    }
}