<?php

namespace PaymentModule\Controllers;

use BasketModule\Contracts\IBasket;
use PaymentModule\Contracts\IPaymentData;
use PaymentModule\Contracts\IPaymentPermission;
use PaymentModule\Contracts\IPaymentRedirection;
use PaymentModule\Factories\IPaymentFormBuilder;
use RouterModule\Helpers\ControllerHelper;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use UserModule\Controllers\LoginController;
use UserModule\Exceptions\CustomerRequiredException;
use UserModule\Services\ICustomerAvailability;

final class PaymentController
{
    /**
     * @var FreePurchaseController
     */
    private $freePurchaseController;

    /**
     * @var OnAccountController
     */
    private $accountController;

    /**
     * @var SageController
     */
    private $sageController;

    /**
     * @var PaypalController
     */
    private $paypalController;

    /**
     * @var IBasket
     */
    private $basket;

    /**
     * @var LoginController
     */
    private $loginController;

    /**
     * @var ICustomerAvailability
     */
    private $customerAvailability;

    /**
     * @var IPaymentFormBuilder
     */
    private $formBuilder;

    /**
     * @var IPaymentPermission
     */
    private $paymentPermission;

    /**
     * @var IPaymentRedirection
     */
    private $paymentRedirection;

    /**
     * @param FreePurchaseController $freePurchaseController
     * @param SageController $sageController
     * @param PaypalController $paypalController
     * @param IBasket $basket
     * @param ICustomerAvailability $customerAvailability
     * @param IPaymentFormBuilder $formBuilder
     * @param IPaymentPermission $paymentPermission
     * @param IPaymentRedirection $paymentRedirection
     * @param OnAccountController $accountController
     * @param LoginController $loginController
     */
    public function __construct(
        FreePurchaseController $freePurchaseController,
        SageController $sageController,
        PaypalController $paypalController,
        IBasket $basket,
        ICustomerAvailability $customerAvailability,
        IPaymentFormBuilder $formBuilder,
        IPaymentPermission $paymentPermission,
        IPaymentRedirection $paymentRedirection,
        OnAccountController $accountController = NULL,
        LoginController $loginController = NULL
    )
    {
        $this->freePurchaseController = $freePurchaseController;
        $this->accountController = $accountController;
        $this->sageController = $sageController;
        $this->paypalController = $paypalController;
        $this->basket = $basket;
        $this->loginController = $loginController;
        $this->customerAvailability = $customerAvailability;
        $this->formBuilder = $formBuilder;
        $this->paymentPermission = $paymentPermission;
        $this->paymentRedirection = $paymentRedirection;
    }

    /**
     * @return RedirectResponse|Response
     */
    public function payment()
    {
        try {
            $loginAvailability = NULL;
            $customer = $this->customerAvailability->optionalLoggedInCustomer();
            if ($this->loginController && !$customer) {
                $login = $this->loginController->handleLoginAttempt();
                if ($login->isLoggedIn()) {
                    $customer = $login->getLoggedInCustomer();
                } else {
                    $loginAvailability = $login;
                }
            }
            $response = $this->paymentPermission->getRedirect($this->basket, $customer);
            if ($response) {
                return $response;
            }

            $paymentForm = $this->formBuilder->buildForm($this->basket, $customer);

            if ($this->basket->isFreePurchase()) {
                return $this->freePurchaseController->payment($customer, $loginAvailability);
            }

            if ($paymentForm->isSubmitted() && $paymentForm->isValid()) {
                $paymentData = $paymentForm->getData();
                if ($this->accountController) {
                    $response = $this->accountController->payment($paymentData);
                    // on account payment can be partial
                    if ($response) {
                        return $response;
                    }
                }
                if ($paymentData->getRealPaymentType() === IPaymentData::TYPE_PAYPAL) {
                    return $this->paypalController->payment($paymentData);
                }
            }
            return $this->sageController->payment($paymentForm, $loginAvailability, $customer);
        } catch (CustomerRequiredException $e) {
            return $this->paymentRedirection->getFailure($e);
        }
    }
}