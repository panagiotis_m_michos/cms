<?php

namespace PaymentModule\Controllers\Sage;

use PaymentModule\Config\ISageConfig;
use SagePay\Token\Exception\ValidationError;
use SagePay\Token\SagePay;
use Symfony\Component\HttpFoundation\Response;
use TranslationModule\Config\MsgId;

class AuthenticationController
{
    /**
     * @var SagePay
     */
    private $sagePay;

    /**
     * @var ISageConfig
     */
    private $config;

    /**
     * @param SagePay $sagePay
     * @param ISageConfig $config
     */
    public function __construct(SagePay $sagePay, ISageConfig $config)
    {
        $this->sagePay = $sagePay;
        $this->config = $config;
    }

    /**
     * @return Response
     */
    public function authenticate()
    {
        try {
            return new Response(
                $this->sagePay->getAuthenticationForm(
                    $this->config->getReturnUrl()
                )
            );
        } catch (ValidationError $e) {
            return new Response(s_(MsgId::PAYMENT_DATA_EMPTY));
        }
    }
}