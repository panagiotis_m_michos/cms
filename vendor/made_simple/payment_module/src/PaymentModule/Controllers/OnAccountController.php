<?php

namespace PaymentModule\Controllers;

use Basket;
use BasketModule\Contracts\IBasket;
use PaymentModule\Contracts\IPaymentData;
use PaymentModule\Contracts\IPaymentResponse;
use PaymentModule\Dto\PaymentDetails;
use PaymentModule\Exceptions\PaymentException;
use PaymentModule\Loggers\IPaymentLogger;
use PaymentModule\PaymentTypes\ILocalPaymentMaker;
use Symfony\Component\HttpFoundation\RedirectResponse;
use UserModule\Contracts\ICustomer;
use UserModule\Services\CustomerAvailability;
use UserModule\Services\ICustomerAvailability;

class OnAccountController
{
    /**
     * @var Basket
     */
    private $basket;

    /**
     * @var CustomerAvailability
     */
    private $customerAvailability;

    /**
     * @var ILocalPaymentMaker
     */
    private $paymentMaker;

    /**
     * @var IPaymentLogger
     */
    private $logger;

    /**
     * @param IBasket $basket
     * @param ICustomerAvailability $customerAvailability
     * @param ILocalPaymentMaker $paymentMaker
     * @param IPaymentLogger $logger
     */
    public function __construct(
        IBasket $basket,
        ICustomerAvailability $customerAvailability,
        ILocalPaymentMaker $paymentMaker,
        IPaymentLogger $logger
    )
    {
        $this->basket = $basket;
        $this->customerAvailability = $customerAvailability;
        $this->paymentMaker = $paymentMaker;
        $this->logger = $logger;
    }

    /**
     * @param IPaymentData $paymentData
     * @return RedirectResponse|NULL
     */
    public function payment(IPaymentData $paymentData)
    {
        if ($paymentData->isUsingCredit()) {
            /** @var ICustomer $customer */
            $customer = $this->customerAvailability->requireCustomer();
            if ($customer->hasCredit()) {
                if ($customer->hasEnoughCredit($this->basket->getTotalPrice() + $this->basket->getCredit())) {
                    try {
                        return $this->paymentMaker->makePayment($customer, $this->basket);
                    } catch (PaymentException $e) {
                        $this->logger->error($e, $customer, PaymentDetails::withType(IPaymentResponse::TYPE_ON_ACCOUNT), $e->getMessage());
                    }
                } else {
                    $this->basket->setCredit($customer->getCredit());
                }
            } else {
                $this->basket->setCredit(0);
            }
        } else {
            $this->basket->setCredit(0);
        }
    }
}