<?php

namespace PaymentModule\Controllers;

use BasketModule\Contracts\IBasket;
use FormModule\Helpers\ErrorHelper;
use PaymentModule\Contracts\IPaymentRedirection;
use PaymentModule\Contracts\IPaymentResponse;
use PaymentModule\Dto\PaymentDetails;
use PaymentModule\Exceptions\PaymentException;
use PaymentModule\Loggers\IPaymentLogger;
use PaymentModule\PaymentTypes\IPaymentMaker;
use SagePay\Token\Exception\AuthenticationRequired;
use SagePay\Token\Exception\ConnectionError;
use SagePay\Token\Exception\DataMismatch;
use SagePay\Token\Exception\FailedResponse;
use SagePay\Token\Exception\Transaction;
use SagePay\Token\Exception\ValidationError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Response;
use TemplateModule\Renderers\IRenderer;
use TranslationModule\Config\MsgId;
use UserModule\Contracts\ICustomer;
use UserModule\Contracts\ILoginAvailability;
use UserModule\Exceptions\CustomerRequiredException;
use UserModule\Services\ICustomerAvailability;

final class SageController
{
    /**
     * @var IRenderer
     */
    private $renderer;

    /**
     * @var ICustomerAvailability
     */
    private $customerAvailability;

    /**
     * @var IPaymentRedirection
     */
    private $paymentRedirection;

    /**
     * @var IPaymentLogger
     */
    private $logger;

    /**
     * @var IPaymentMaker
     */
    private $paymentMaker;

    /**
     * @var IBasket
     */
    private $basket;

    /**
     * @param ICustomerAvailability $customerAvailability
     * @param IRenderer $renderer
     * @param IPaymentRedirection $paymentRedirection
     * @param IPaymentLogger $logger
     * @param IPaymentMaker $paymentMaker
     * @param IBasket $basket
     */
    public function __construct(
        ICustomerAvailability $customerAvailability,
        IRenderer $renderer,
        IPaymentRedirection $paymentRedirection,
        IPaymentLogger $logger,
        IPaymentMaker $paymentMaker,
        IBasket $basket
    )
    {
        $this->customerAvailability = $customerAvailability;
        $this->renderer = $renderer;
        $this->paymentRedirection = $paymentRedirection;
        $this->logger = $logger;
        $this->paymentMaker = $paymentMaker;
        $this->basket = $basket;
    }

    /**
     * @param FormInterface $paymentForm
     * @param ILoginAvailability|NULL $loginAvailability
     * @param ICustomer|NULL $customer
     * @return Response
     */
    public function payment(FormInterface $paymentForm, ILoginAvailability $loginAvailability = NULL, ICustomer $customer = NULL)
    {

        $showAuth = FALSE;
        if ($paymentForm->isSubmitted()) {
            if ($paymentForm->isValid()) {
                $paymentData = $paymentForm->getData();
                $customer = $this->customerAvailability->requireCustomer();
                try {
                    return $this->paymentMaker->makePayment($customer, $this->basket, $paymentData);
                } catch (AuthenticationRequired $e) {
                    $showAuth = TRUE;
                } catch (Transaction $e) {
                    $this->logger->error($e, $customer, $paymentData, MsgId::TRANSACTION_USED);
                } catch (ValidationError $e) {
                    $this->logger->error($e, $customer, $paymentData, $e->getMessage());
                } catch (FailedResponse $e) {
                    $this->logger->error($e, $customer, $paymentData, $e->getMessage());
                } catch (ConnectionError $e) {
                    $this->logger->error($e, $customer, $paymentData, MsgId::GENERIC_RESPONSE_ERROR);
                } catch (PaymentException $e) {
                    $this->logger->error($e, $customer, $paymentData, $e->getMessage());
                }
            }

        }
        return $this->renderer->render([
            'basket' => $this->basket,
            'paymentForm' => $paymentForm->createView(),
            'paymentDetails' => $paymentForm->getData(),
            'customer' => $customer,
            'showAuth' => $showAuth,
            'loginAvailability' => $loginAvailability,
            'formErrorsCount' => ErrorHelper::getCount($paymentForm),
            'firstError' => ErrorHelper::getFirstErrorElementName($paymentForm)
        ]);
    }

    /**
     * @return Response
     */
    public function validate()
    {
        $paymentData = PaymentDetails::withType(IPaymentResponse::TYPE_SAGE);
        try {
            $customer = $this->customerAvailability->requireCustomer();
            $redirect = $this->paymentMaker->authorizePayment($customer, $this->basket, $_POST);
            return $this->renderer->render([
                'redirect' => $redirect->getTargetUrl()
            ]);
        } catch (CustomerRequiredException $e) {
            return $this->renderer->render([
                'error' => $e->getMessage()
            ]);
        } catch (ValidationError $e) {
            $this->logger->error($e, $customer, $paymentData, $e->getMessage());
            return $this->renderer->render([
                'error' => $e->getMessage()
            ]);
        } catch (FailedResponse $e) {
            $this->logger->error($e, $customer, $paymentData, $e->getMessage());

            return $this->renderer->render([
                'error' => $e->getMessage()
            ]);
        } catch (ConnectionError $e) {
            $this->logger->error($e, $customer, $paymentData, MsgId::GENERIC_RESPONSE_ERROR);
            return $this->renderer->render([
                'error' => $e->getMessage()
            ]);
        } catch (DataMismatch $e) {
            $this->logger->error($e, $customer, $paymentData, $e->getMessage());
            return $this->renderer->render([
                'error' => $e->getMessage()
            ]);
        } catch (PaymentException $e) {
            $this->logger->error($e, $customer, $paymentData, $e->getMessage());
            return $this->renderer->render([
                'error' => $e->getMessage()
            ]);
        }
        
    }
}