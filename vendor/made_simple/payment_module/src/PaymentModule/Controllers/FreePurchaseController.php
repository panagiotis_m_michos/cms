<?php

namespace PaymentModule\Controllers;

use Basket;
use BasketModule\Contracts\IBasket;
use PaymentModule\Contracts\IPaymentResponse;
use PaymentModule\Dto\PaymentDetails;
use PaymentModule\Exceptions\PaymentException;
use PaymentModule\Loggers\IPaymentLogger;
use PaymentModule\PaymentTypes\ILocalPaymentMaker;
use RouterModule\Helpers\ControllerHelper;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Response;
use TemplateModule\Renderers\IRenderer;
use UserModule\Contracts\ICustomer;
use UserModule\Contracts\ILoginAvailability;
use UserModule\Services\CustomerAvailability;
use UserModule\Services\ICustomerAvailability;

class FreePurchaseController
{
    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    /**
     * @var Basket
     */
    private $basket;

    /**
     * @var CustomerAvailability
     */
    private $customerAvailability;

    /**
     * @var IRenderer
     */
    private $renderer;

    /**
     * @var ILocalPaymentMaker
     */
    private $paymentMaker;

    /**
     * @var IPaymentLogger
     */
    private $paymentLogger;

    /**
     * @param ControllerHelper $controllerHelper
     * @param ICustomerAvailability $customerAvailability
     * @param IBasket $basket
     * @param IRenderer $renderer
     * @param IPaymentLogger $paymentLogger
     * @param ILocalPaymentMaker $paymentMaker
     */
    public function __construct(
        ControllerHelper $controllerHelper,
        ICustomerAvailability $customerAvailability,
        IBasket $basket,
        IRenderer $renderer,
        IPaymentLogger $paymentLogger,
        ILocalPaymentMaker $paymentMaker
    )
    {
        $this->controllerHelper = $controllerHelper;
        $this->basket = $basket;
        $this->customerAvailability = $customerAvailability;
        $this->renderer = $renderer;
        $this->paymentMaker = $paymentMaker;
        $this->paymentLogger = $paymentLogger;
    }

    /**
     * @param ICustomer $customer
     * @param ILoginAvailability $loginAvailability
     * @return Response
     */
    public function payment(ICustomer $customer = NULL, ILoginAvailability $loginAvailability = NULL)
    {
        if ($this->controllerHelper->isPostRequest()) {
            try {
                $customer = $this->customerAvailability->requireCustomer();
                return $this->paymentMaker->makePayment($customer, $this->basket);
            } catch (PaymentException $e) {
                $this->paymentLogger->error($e, $customer, PaymentDetails::withType(IPaymentResponse::TYPE_FREE), $e->getMessage());
            }
        }
        return $this->renderer->render(
            [
                'customer' => $customer,
                'loginAvailability' => $loginAvailability,
                'basket' => $this->basket
            ]
        );
    }
}