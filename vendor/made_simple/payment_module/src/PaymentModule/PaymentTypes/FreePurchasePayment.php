<?php

namespace PaymentModule\PaymentTypes;

use BasketModule\Contracts\IBasket;
use PaymentModule\Contracts\IPaymentResponse;
use PaymentModule\Exceptions\PaymentException;
use UserModule\Contracts\ICustomer;
use PaymentModule\Responses\PaymentResponse;

final class FreePurchasePayment implements ILocalPaymentType
{
    /**
     * @param ICustomer $customer
     * @param IBasket $basket
     * @return IPaymentResponse
     * @throws PaymentException
     */
    public function makePayment(ICustomer $customer, IBasket $basket)
    {
        if (!$basket->isFreePurchase()) {
            throw PaymentException::unavailableMethod('Free Purchase');
        }
        return $this->getPaymentResponse($customer);
    }

    /**
     * @param ICustomer $customer
     * @return PaymentResponse
     */
    private function getPaymentResponse(ICustomer $customer)
    {
        return PaymentResponse::freePurchase($customer->getFullName());
    }

}