<?php

namespace PaymentModule\PaymentTypes;

use BasketModule\Contracts\IBasket;
use PaymentModule\Processors\IPaymentProcessor;
use PaymentModule\Contracts\IPaymentRedirection;
use Symfony\Component\HttpFoundation\RedirectResponse;
use UserModule\Contracts\ICustomer;

final class LocalPaymentMaker implements ILocalPaymentMaker
{
    /**
     * @var ILocalPaymentType
     */
    private $paymentType;

    /**
     * @var IPaymentProcessor
     */
    private $paymentProcessor;

    /**
     * @var IPaymentRedirection
     */
    private $paymentRedirection;

    /**
     * @param ILocalPaymentType $paymentType
     * @param IPaymentProcessor $paymentProcessor
     * @param IPaymentRedirection $paymentRedirection
     */
    public function __construct(ILocalPaymentType $paymentType, IPaymentProcessor $paymentProcessor, IPaymentRedirection $paymentRedirection)
    {
        $this->paymentType = $paymentType;
        $this->paymentProcessor = $paymentProcessor;
        $this->paymentRedirection = $paymentRedirection;
    }

    /**
     * @param ICustomer $customer
     * @param IBasket $basket
     * @return RedirectResponse
     */
    public function makePayment(ICustomer $customer, IBasket $basket)
    {
        $response = $this->paymentType->makePayment($customer, $basket);
        $order = $this->paymentProcessor->processPayment($customer, $basket, $response);
        return $this->paymentRedirection->getConfirmation($order, $basket);
    }
}