<?php

namespace PaymentModule\PaymentTypes;

use BasketModule\Contracts\IBasket;
use PaymentModule\Contracts\IPaymentResponse;
use UserModule\Contracts\ICustomer;

interface ILocalPaymentType
{
    /**
     * @param ICustomer $customer
     * @param IBasket $basket
     * @return IPaymentResponse
     */
    public function makePayment(ICustomer $customer, IBasket $basket);
}