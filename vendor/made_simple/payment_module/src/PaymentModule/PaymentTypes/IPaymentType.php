<?php

namespace PaymentModule\PaymentTypes;

use PaymentModule\Contracts\IPaymentData;
use PaymentModule\Contracts\IPaymentResponse;

interface IPaymentType
{
    /**
     * @param IPaymentData $paymentData
     * @return IPaymentResponse
     */
    public function makePayment(IPaymentData $paymentData);

    /**
     * @param float $amount
     * @param array $postData
     * @return IPaymentResponse
     */
    public function authorizePayment($amount, $postData);
}