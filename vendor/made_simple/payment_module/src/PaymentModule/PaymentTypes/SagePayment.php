<?php

namespace PaymentModule\PaymentTypes;

use Exception;
use PaymentModule\Contracts\IPaymentData;
use PaymentModule\Contracts\IPaymentResponse;
use PaymentModule\Factories\IRequestFactory;
use PaymentModule\Responses\PaymentResponse;
use SagePay\Token\Exception\AuthenticationRequired;
use SagePay\Token\Exception\ConnectionError;
use SagePay\Token\Exception\DataMismatch;
use SagePay\Token\Exception\FailedResponse;
use SagePay\Token\Exception\ValidationError;
use SagePay\Token\Request\Details;
use SagePay\Token\Request\Remove;
use SagePay\Token\Response\Summary;
use SagePay\Token\SagePay;
use SagePay\Token\Storage;

final class SagePayment implements IPaymentType
{
    const STORAGE_PAYMENT = 'payment_details';
    
    /**
     * @var SagePay
     */
    private $sagePay;

    /**
     * @var Storage
     */
    private $storage;

    /**
     * @var IRequestFactory
     */
    private $requestFactory;

    /**
     * @param SagePay $sagePay
     * @param Storage $storage
     * @param IRequestFactory $requestFactory
     */
    public function __construct(SagePay $sagePay, Storage $storage, IRequestFactory $requestFactory)
    {
        $this->sagePay = $sagePay;
        $this->storage = $storage;
        $this->requestFactory = $requestFactory;
    }

    /**
     * @param IPaymentData $paymentData
     * @return IPaymentResponse
     * @throws ConnectionError
     * @throws AuthenticationRequired
     * @throws FailedResponse
     * @throws ValidationError
     */
    public function makePayment(IPaymentData $paymentData)
    {
        if ($paymentData->isUsingToken()) {
            return $this->makeTokenPayment($paymentData);
        }
        try {
            $registration = $this->requestFactory->createCardDetails($paymentData);
            $responseRegistration = $this->sagePay->registerToken($registration);
            $details = $this->requestFactory->createPaymentDetails($paymentData);
            $responseSummary = $this->sagePay->makePayment($details, $responseRegistration->getToken());
            return PaymentResponse::fromDetails($details, $responseSummary);
        } catch (AuthenticationRequired $e) {
            $this->saveDetails($details);
            throw $e;
        } catch (Exception $e) {
            $this->sagePay->finishTransaction();
            if (isset($details) && $details->getStoreToken() && $details->getToken()) {
                $this->deleteToken($details->getToken());
            }
            throw $e;
        }

    }

    /**
     * @param float $paymentAmount
     * @param array $postData
     * @return IPaymentResponse
     * @throws ConnectionError
     * @throws DataMismatch
     * @throws FailedResponse
     * @throws ValidationError
     */
    public function authorizePayment($paymentAmount, $postData)
    {
        $details = $this->getDetails();
        if (!$details || !($details instanceof Details)) {
            throw new ValidationError('No saved payment details found!');
        }
        $responseSummary = $this->sagePay->authorizePayment($postData, $paymentAmount);
        return PaymentResponse::fromDetails($details, $responseSummary);
    }

    /**
     * @param IPaymentData $paymentData
     * @return PaymentResponse
     * @throws ConnectionError
     * @throws AuthenticationRequired
     * @throws FailedResponse
     * @throws ValidationError
     */
    public function makeTokenPayment(IPaymentData $paymentData)
    {
        $details = $this->requestFactory->createPaymentDetails($paymentData);
        $responseSummary = $this->sagePay->makePayment($details, $details->getToken());
        return PaymentResponse::fromToken($details, $responseSummary, $paymentData->getToken());
    }

    /**
     * @param string $token
     * @throws FailedResponse
     * @throws ConnectionError
     * @throws ValidationError
     */
    public function deleteToken($token)
    {
        $remReq = new Remove();
        $remReq->setToken($token);
        $this->sagePay->removeToken($remReq);
    }

    /**
     * @return Details
     */
    public function getDetails()
    {
        return $this->storage->get(self::STORAGE_PAYMENT);
    }

    /**
     * @param Details $details
     */
    public function saveDetails(Details $details)
    {
        $clone = clone $details;
        $clone->setCv2(NULL);
        $this->storage->save(self::STORAGE_PAYMENT, $clone);
    }
}