<?php

namespace PaymentModule\PaymentTypes;

use PaymentModule\Contracts\IPaymentData;
use PaymentModule\Contracts\IPaymentResponse;
use PaymentModule\Exceptions\PaymentException;
use PaymentModule\Factories\IRequestFactory;
use PaymentModule\Responses\PaymentResponse;
use PayPalNVP\Exception\DoExpressCheckoutException;
use PayPalNVP\Exception\RequestException;
use PayPalNVP\Exception\ResponseException;
use PayPalNVP\PayPalNVP;
use PayPalNVP\Request\DoExpressCheckoutPayment;
use PayPalNVP\Request\GetExpressCheckoutDetails;
use PayPalNVP\Response\DoExpressCheckoutPaymentResponse;
use PayPalNVP\Response\Error;

final class PaypalPayment implements IPaymentType
{
    /**
     * @var PayPalNVP
     */
    private $paypalGateway;

    /**
     * @var IRequestFactory
     */
    private $requestFactory;

    /**
     * @param PayPalNVP $paypalGateway
     * @param IRequestFactory $requestFactory
     */
    public function __construct(PayPalNVP $paypalGateway, IRequestFactory $requestFactory)
    {
        $this->paypalGateway = $paypalGateway;
        $this->requestFactory = $requestFactory;
    }

    /**
     * @param IPaymentData $paymentData
     * @return IPaymentResponse
     * @throws PaymentException
     */
    public function makePayment(IPaymentData $paymentData)
    {
        $setEC = $this->requestFactory->createPaymentDetails($paymentData);
        $this->paypalGateway->setResponse($setEC);
        $response = $setEC->getResponse();
        if (!$response->isAckSuccess() && !$response->isAckSuccessWithWarning()) {
            throw new PaymentException("Paypal problem: " . $response->getErrorMessages());
        }
        return PaymentResponse::createRedirection(PaymentResponse::TYPE_PAYPAL, $response->getRedirectUrl());
    }

    /**
     * @param float $amountToPay
     * @param string $token
     * @return PaymentResponse
     * @throws DoExpressCheckoutException
     * @throws RequestException
     * @throws PaymentException
     * @throws ResponseException
     */
    public function authorizePayment($amountToPay, $token)
    {
        $paymentResponse = PaymentResponse::paypal();
        $paymentResponse->setPaypalDetails($token);

        // get Express Checkout
        $getEC = new GetExpressCheckoutDetails('token=' . $token);

        $this->paypalGateway->setResponse($getEC);
        $getECResponse = $getEC->getResponse();

        $getECError = $getECResponse->getErrorMessages();
        $paymentResponse->setError($getECError);

        if (!$getECResponse->isAckSuccess() && !$getECResponse->isAckSuccessWithWarning()) {
            throw RequestException::expressCheckout($getECResponse->getErrorCodes());
        }

        // do Express Checkout
        $doEC = new DoExpressCheckoutPayment($getECResponse);
        $this->paypalGateway->setResponse($doEC);
        /** @var DoExpressCheckoutPaymentResponse $doECResponse */
        $doECResponse = $doEC->getResponse();

        if (!$doECResponse->isAckSuccess() && !$doECResponse->isAckSuccessWithWarning()) {
            $error = $doECResponse->getErrorMessages();
            $paymentResponse->setError($error);

            /** @var Error $doExpressCheckoutError */
            foreach ($doECResponse->getErrors() as $doExpressCheckoutError) {
                if ($doExpressCheckoutError->getErrorCode() == DoExpressCheckoutException::ERROR_FUNDING_FAILURE) {
                    throw DoExpressCheckoutException::fundingFailure($doECResponse->getRedirectUrl($token));
                } else {
                    throw RequestException::expressCheckout($doECResponse->getErrorCodes());
                }
            }
        }

        $paymentResponse->setCardHolder($getECResponse->getPayerName()->getFullName());

        $paymentInfo = $doECResponse->getPaymentInfo();
        if (is_array($paymentInfo)) {
            $paymentInfo = array_pop($paymentInfo);
            $paymentResponse->setPaypalTransactionId($paymentInfo->getTransactionId());
        }
        if ($paymentInfo->getAmount() != $amountToPay) {
            throw new PaymentException("Payment amount {$paymentInfo->getAmount()} does not match with the amount {$amountToPay} to be paid");
        }

        $paymentResponse->setPaypalPaymentInfo($getECResponse);
        return $paymentResponse;
    }
}