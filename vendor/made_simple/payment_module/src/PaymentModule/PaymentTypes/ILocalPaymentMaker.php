<?php

namespace PaymentModule\PaymentTypes;

use BasketModule\Contracts\IBasket;
use Symfony\Component\HttpFoundation\RedirectResponse;
use UserModule\Contracts\ICustomer;

interface ILocalPaymentMaker
{
    /**
     * @param ICustomer $customer
     * @param IBasket $basket
     * @return RedirectResponse
     */
    public function makePayment(ICustomer $customer, IBasket $basket);
}