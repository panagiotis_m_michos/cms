<?php

namespace PaymentModule\PaymentTypes;

use BasketModule\Contracts\IBasket;
use PaymentModule\Contracts\IPaymentData;
use PaymentModule\Processors\IPaymentProcessor;
use PaymentModule\Contracts\IPaymentRedirection;
use Symfony\Component\HttpFoundation\RedirectResponse;
use UserModule\Contracts\ICustomer;

final class PaymentMaker implements IPaymentMaker
{
    /**
     * @var IPaymentType
     */
    private $paymentType;

    /**
     * @var IPaymentProcessor
     */
    private $paymentProcessor;

    /**
     * @var IPaymentRedirection
     */
    private $paymentRedirection;

    /**
     * @param IPaymentType $paymentType
     * @param IPaymentProcessor $paymentProcessor
     * @param IPaymentRedirection $paymentRedirection
     */
    public function __construct(IPaymentType $paymentType, IPaymentProcessor $paymentProcessor, IPaymentRedirection $paymentRedirection)
    {
        $this->paymentType = $paymentType;
        $this->paymentProcessor = $paymentProcessor;
        $this->paymentRedirection = $paymentRedirection;
    }

    /**
     * @param ICustomer $customer
     * @param IBasket $basket
     * @param IPaymentData $paymentData
     * @return RedirectResponse
     */
    public function makePayment(ICustomer $customer, IBasket $basket, IPaymentData $paymentData)
    {
        $response = $this->paymentType->makePayment($paymentData);
        if ($response->isRedirectRequired()) {
            return new RedirectResponse($response->getRedirectUrl());
        }
        $order = $this->paymentProcessor->processPayment($customer, $basket, $response);
        return $this->paymentRedirection->getConfirmation($order, $basket);
    }

    /**
     * @param ICustomer $customer
     * @param IBasket $basket
     * @param mixed $data
     * @return RedirectResponse
     */
    public function authorizePayment(ICustomer $customer, IBasket $basket, $data)
    {
        $response = $this->paymentType->authorizePayment($basket->getTotalPrice(), $data);
        $order = $this->paymentProcessor->processPayment($customer, $basket, $response);
        return $this->paymentRedirection->getConfirmation($order, $basket);
    }

}