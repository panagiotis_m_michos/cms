<?php

namespace PaymentModule\PaymentTypes;

use BasketModule\Contracts\IBasket;
use PaymentModule\Contracts\IPaymentData;
use Symfony\Component\HttpFoundation\RedirectResponse;
use UserModule\Contracts\ICustomer;

interface IPaymentMaker
{
    /**
     * @param ICustomer $customer
     * @param IBasket $basket
     * @param IPaymentData $paymentData
     * @return RedirectResponse
     */
    public function makePayment(ICustomer $customer, IBasket $basket, IPaymentData $paymentData);
    
    /**
     * @param ICustomer $customer
     * @param IBasket $basket
     * @param mixed $data
     * @return RedirectResponse
     */
    public function authorizePayment(ICustomer $customer, IBasket $basket, $data);
}