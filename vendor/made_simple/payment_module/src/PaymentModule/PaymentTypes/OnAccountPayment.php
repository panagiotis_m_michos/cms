<?php

namespace PaymentModule\PaymentTypes;

use BasketModule\Contracts\IBasket;
use PaymentModule\Contracts\IPaymentResponse;
use PaymentModule\Exceptions\PaymentException;
use PaymentModule\Responses\PaymentResponse;
use UserModule\Contracts\ICustomer;
use UserModule\Exceptions\CreditException;
use UserModule\Services\ICreditProvider;

final class OnAccountPayment implements ILocalPaymentType
{
    /**
     * @var ICreditProvider
     */
    private $creditProvider;

    /**
     * @param ICreditProvider $creditProvider
     */
    public function __construct(ICreditProvider $creditProvider)
    {
        $this->creditProvider = $creditProvider;
    }

    /**
     * @param IBasket $basket
     * @param ICustomer $customer
     * @return IPaymentResponse
     */
    public function makePayment(ICustomer $customer, IBasket $basket)
    {
        try {

            $this->creditProvider->subtractCredit($customer, $basket->getTotalPrice() + $basket->getCredit());
            $basket->setCredit(0);
            return $this->getPaymentResponse($customer);
        } catch (CreditException $e) {
            throw PaymentException::fromPrevious($e);
        }
    }
    /**
     * @param ICustomer $customer
     * @return IPaymentResponse
     */
    private function getPaymentResponse(ICustomer $customer)
    {
        return PaymentResponse::onAccount($customer->getFullName());

    }
}