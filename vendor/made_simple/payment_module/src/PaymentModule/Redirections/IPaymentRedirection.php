<?php

namespace PaymentModule\Contracts;

use BasketModule\Contracts\IBasket;
use Exception;
use Symfony\Component\HttpFoundation\RedirectResponse;

interface IPaymentRedirection
{

    /**
     * @param string $key
     * @param mixed $value
     */
    public function addAdditionalParameter($key, $value);
    
    /**
     * @param IOrder $order
     * @param IBasket $basket
     * @return RedirectResponse
     */
    public function getConfirmation(IOrder $order, IBasket $basket);

    /**
     * @param Exception|NULL $e
     * @return RedirectResponse
     */
    public function getFailure(Exception $e = NULL);
}