<?php

namespace PaymentModule\Events;

use BasketModule\Contracts\IBasket;
use PaymentModule\Contracts\IPaymentResponse;
use Symfony\Component\EventDispatcher\Event;
use UserModule\Contracts\ICustomer;

class PaymentEvent extends Event
{
    /**
     * @var ICustomer
     */
    private $customer;

    /**
     * @var IBasket
     */
    private $basket;

    /**
     * @var IPaymentResponse
     */
    private $paymentResponse;

    /**
     * @param ICustomer $customer
     * @param IBasket $basket
     * @param IPaymentResponse $paymentResponse
     */
    public function __construct(ICustomer $customer, IBasket $basket, IPaymentResponse $paymentResponse)
    {
        $this->customer = $customer;
        $this->basket = $basket;
        $this->paymentResponse = $paymentResponse;
    }

    /**
     * @return ICustomer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @return IBasket
     */
    public function getBasket()
    {
        return $this->basket;
    }

    /**
     * @return IPaymentResponse
     */
    public function getPaymentResponse()
    {
        return $this->paymentResponse;
    }
}