<?php

namespace PaymentModule\Factories;

use BadMethodCallException;
use PaymentModule\Config\IPaypalConfig;
use PaymentModule\Contracts\IPaymentData;
use PayPalNVP\Fields\BillingAgreement;
use PayPalNVP\Fields\Payment;
use PayPalNVP\Fields\PaymentItem;
use PayPalNVP\Request\SetExpressCheckout;
use PayPalNVP\Util\Currency;

class PaypalRequestFactory implements IRequestFactory
{
    /**
     * @var IPaypalConfig
     */
    private $config;

    /**
     * @param IPaypalConfig $config
     */
    public function __construct(IPaypalConfig $config)
    {
        $this->config = $config;
    }

    /**
     * @param IPaymentData $paymentData
     * @return mixed
     */
    public function createCardDetails(IPaymentData $paymentData)
    {
        throw new BadMethodCallException('Paypal does not have card details');
    }

    /**
     * @param IPaymentData $paymentData
     * @return SetExpressCheckout
     */
    public function createPaymentDetails(IPaymentData $paymentData)
    {
        $items = [PaymentItem::getRequest($this->config->getDescription(), $paymentData->getAmount(), 1)];

        $payment = Payment::getRequest($items);
        $payment->setCustomField($this->config->getCustomField());
        $payment->setCurrency(new Currency($paymentData->getCurrency()));

        $setEC = SetExpressCheckout::newInstance(
            $payment,
            $this->config->getValidationUrl(),
            $this->config->getCancelUrl()
        );

        $billingAgreement = BillingAgreement::getReqeuest($this->config->getBillingField());
        $setEC->addBillngAgreement($billingAgreement);
        return $setEC;
    }
}