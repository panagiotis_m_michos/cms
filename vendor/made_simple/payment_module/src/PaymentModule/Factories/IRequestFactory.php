<?php

namespace PaymentModule\Factories;

use PaymentModule\Contracts\IPaymentData;

interface IRequestFactory
{
    /**
     * @param IPaymentData $paymentData
     * @return mixed
     */
    public function createCardDetails(IPaymentData $paymentData);

    /**
     * @param IPaymentData $paymentData
     * @return mixed
     */
    public function createPaymentDetails(IPaymentData $paymentData);
}