<?php

namespace PaymentModule\Factories;

use InvalidArgumentException;
use PayPalNVP\Environment;
use PayPalNVP\PayPalNVP;
use PayPalNVP\Profile\ApiSignature;

class PaypalFactory
{
    /**s
     * @param array $paypalConfig
     * @return PayPalNVP
     * @throws InvalidArgumentException
     */
    public static function createPaypal(array $paypalConfig)
    {
        if (!isset($paypalConfig['apiUsername'], $paypalConfig['apiPassword'], $paypalConfig['environment'])) {
            throw new InvalidArgumentException('Paypal configuration is not valid');
        }
        $profile = new ApiSignature($paypalConfig['apiUsername'], $paypalConfig['apiPassword'], $paypalConfig['apiSignature']);
        if ($paypalConfig['environment'] === 'sandbox') {
            $environment = Environment::SANDBOX();
        } else {
            $environment = Environment::LIVE();
        }
        $paypal = new PayPalNVP($profile, $environment);
        return $paypal;
    }
}