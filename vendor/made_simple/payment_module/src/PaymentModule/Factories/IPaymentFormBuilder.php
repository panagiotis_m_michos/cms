<?php

namespace PaymentModule\Factories;

use BasketModule\Contracts\IBasket;
use Symfony\Component\Form\FormInterface;
use UserModule\Contracts\ICustomer;

interface IPaymentFormBuilder
{
    /**
     * @param IBasket $basket
     * @param ICustomer|NULL $customer
     * @return FormInterface
     */
    public function buildForm(IBasket $basket, ICustomer $customer = NULL);
}