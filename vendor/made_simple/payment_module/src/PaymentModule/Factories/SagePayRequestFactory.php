<?php

namespace PaymentModule\Factories;

use PaymentModule\Config\ISageConfig;
use PaymentModule\Contracts\IPaymentData;
use SagePay\Token\Request\Details;
use SagePay\Token\Request\Registration;

class SagePayRequestFactory implements IRequestFactory
{
    /**
     * @var ISageConfig
     */
    private $config;

    /**
     * SagePayRequestFactory constructor.
     * @param ISageConfig $config
     */
    public function __construct(ISageConfig $config)
    {
        $this->config = $config;
    }
    /**
     * @param IPaymentData $paymentData
     * @return Registration
     */
    public function createCardDetails(IPaymentData $paymentData)
    {
        $registration = new Registration();
        $registration->setCurrency($paymentData->getCurrency());
        $cardDetails = $paymentData->getCardDetails();

        $registration->setCardHolder($cardDetails->cardholder);
        $registration->setCardType($cardDetails->cardType);
        $registration->setCardNumber($cardDetails->cardNumber);
        $registration->setCv2($cardDetails->securityCode);
        if ($cardDetails->validFrom) {
            $registration->setStartDate($cardDetails->validFrom->format('my'));
        }
        $registration->setExpiryDate($cardDetails->expiryDate->format('my'));
        if ($cardDetails->issueNumber) {
            $registration->setIssueNumber($cardDetails->issueNumber);
        }
        return $registration;
    }

    /**
     * @param IPaymentData $paymentData
     * @return Details
     */
    public function createPaymentDetails(IPaymentData $paymentData)
    {
        $details = new Details();
        $details->setAmount($paymentData->getAmount());
        $details->setDescription($this->config->getDescription());
        $details->setCurrency($paymentData->getCurrency());
        $details->setStoreToken($paymentData->hasToStoreToken() ? 1 : 0);
        if ($paymentData->isUsingToken() && $paymentData->getToken()) {
            $token = $paymentData->getToken();
            $details->setBillingFirstnames($token->getBillingFirstnames());
            $details->setBillingSurname($token->getBillingSurname());
            $details->setBillingAddress1($token->getBillingAddress1());
            $details->setBillingAddress2(substr($token->getBillingAddress2(), 0, 40));
            $details->setBillingCity($token->getBillingCity());
            $details->setBillingCountry($token->getBillingCountry());
            $details->setBillingPostCode($token->getBillingPostCode());
            $details->setBillingState($token->getBillingState());
            $details->setBillingPhone($token->getBillingPhone());
            $details->setDeliveryFirstnames($token->getDeliveryFirstnames());
            $details->setDeliverySurname($token->getDeliverySurname());
            $details->setDeliveryAddress1($token->getDeliveryAddress1());
            $details->setDeliveryAddress2($token->getDeliveryAddress2());
            $details->setDeliveryCity($token->getDeliveryCity());
            $details->setDeliveryCountry($token->getDeliveryCountry());
            $details->setDeliveryPostCode($token->getDeliveryPostCode());
            $details->setDeliveryState($token->getDeliveryState());
            $details->setDeliveryPhone($token->getDeliveryPhone());
            $details->setApplyAvsCv2(Details::CV2_OFF);
            $details->setApply3dSecure(Details::SECURE3D_OFF);
            $details->setAccountType(Details::CONTINUOUS_AUTHORITY);
            $tokenIdentifier  = '{' . trim($token->getIdentifier(), '{}') . '}';
            $details->setToken($tokenIdentifier);
            
        } else {
            $cardDetails = $paymentData->getCardDetails();
            $cardHolder = $cardDetails->cardholder;
            $payerDetails = $paymentData->getPayerDetails();

            $details->setBillingFirstnames($this->getBillingFirstNames($cardHolder));
            $details->setBillingSurname($this->getBillingSurname($cardHolder));
            $details->setBillingAddress1($payerDetails->address1);
            $details->setBillingAddress2(substr($payerDetails->address2, 0, 40));
            $details->setBillingCity($payerDetails->city);
            $details->setBillingCountry($payerDetails->country);
            $details->setBillingPostCode($payerDetails->postcode);
            if ($payerDetails->state) {
                $details->setBillingState($payerDetails->state);
            }

            $details->setDeliveryFirstnames($details->getBillingFirstnames());
            $details->setDeliverySurname($details->getBillingSurname());
            $details->setDeliveryAddress1($details->getBillingAddress1());
            $details->setDeliveryAddress2($details->getBillingAddress2());
            $details->setDeliveryCity($details->getBillingCity());
            $details->setDeliveryCountry($details->getBillingCountry());
            $details->setDeliveryPostCode($details->getBillingPostCode());
            $details->setDeliveryState($details->getBillingState());
        }
        return $details;
    }
    
    /**
     * @param $cardholder
     * @return string
     */
    private function getBillingFirstNames($cardholder)
    {
        $cardholderName = isset($cardholder) ? $cardholder : FALSE;
        if (!$cardholderName) {
            return 'M';
        }
        $names = explode(' ', trim($cardholderName));
        if (count($names) > 1) {
            return substr(trim($cardholderName), 0, '-'.strlen(array_pop($names)));
        } else {
            return 'M';
        }
    }

    /**
     * @param $cardholder
     * @return mixed|null
     */
    private function getBillingSurname($cardholder) {
        if (isset($cardholder) && !empty($cardholder) && !is_int($cardholder)) {
            $names = explode(' ', trim($cardholder));
            return array_pop($names);
        } else {
            return NULL;
        }
    }
}