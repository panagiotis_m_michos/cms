<?php

namespace PaymentModule\Processors;

use BasketModule\Contracts\IBasket;
use PaymentModule\Contracts\IOrder;
use PaymentModule\Contracts\IPaymentResponse;
use PaymentModule\Events\PaymentEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use UserModule\Contracts\ICustomer;

class PaymentProcessor implements IPaymentProcessor
{
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @var IPaymentProcessor
     */
    private $paymentProcessor;

    /**
     * PaymentProcessor constructor.
     * @param EventDispatcherInterface $dispatcher
     * @param IPaymentProcessor $paymentProcessor
     */
    public function __construct(EventDispatcherInterface $dispatcher, IPaymentProcessor $paymentProcessor)
    {
        $this->dispatcher = $dispatcher;
        $this->paymentProcessor = $paymentProcessor;
    }

    /**
     * @param ICustomer $customer
     * @param IBasket $basket
     * @param IPaymentResponse $paymentResponse
     * @return IOrder
     */
    public function processPayment(ICustomer $customer, IBasket $basket, IPaymentResponse $paymentResponse)
    {
        $order = $this->paymentProcessor->processPayment($customer, $basket, $paymentResponse);
        $this->dispatcher->dispatch('payment.succeeded', new PaymentEvent($customer, $basket, $paymentResponse));
        return $order;
    }
}