<?php

namespace PaymentModule\Processors;

use BasketModule\Contracts\IBasket;
use PaymentModule\Contracts\IOrder;
use PaymentModule\Contracts\IPaymentResponse;
use UserModule\Contracts\ICustomer;

interface IPaymentProcessor
{
    /**
     * @param ICustomer $customer
     * @param IBasket $basket
     * @param IPaymentResponse $paymentResponse
     * @return IOrder
     */
    public function processPayment(ICustomer $customer, IBasket $basket, IPaymentResponse $paymentResponse);
}