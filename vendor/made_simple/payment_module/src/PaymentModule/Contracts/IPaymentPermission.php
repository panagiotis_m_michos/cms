<?php

namespace PaymentModule\Contracts;

use BasketModule\Contracts\IBasket;
use Symfony\Component\HttpFoundation\RedirectResponse;
use UserModule\Contracts\ICustomer;

interface IPaymentPermission
{
    /**
     * @param IBasket $basket
     * @param ICustomer|NULL $customer
     * @return RedirectResponse|NULL
     */
    public function getRedirect(IBasket $basket, ICustomer $customer = NULL);
}