<?php

namespace PaymentModule\Contracts;

use PaymentModule\Dto\CardDetails;
use PaymentModule\Dto\PayerDetails;

interface IPaymentData
{
    // payment types when customer can choose
    const TYPE_SAGE_NEW_TOKEN = 'SAGE_NEW_TOKEN';
    const TYPE_SAGE_EXISTING_TOKEN = 'SAGE_EXISTING_TOKEN';
    const TYPE_SAGE_NO_TOKEN = 'SAGE_NO_TOKEN';
    
    const TYPE_SAGE = IPaymentResponse::TYPE_SAGE;
    const TYPE_PAYPAL = IPaymentResponse::TYPE_PAYPAL;
    const TYPE_FREE = IPaymentResponse::TYPE_FREE;
    const TYPE_ON_ACCOUNT = IPaymentResponse::TYPE_ON_ACCOUNT;

    /**
     * @return string
     */
    public function getRealPaymentType();
    
    /**
     * @return bool
     */
    public function isCardPayment();

    /**
     * @return bool
     */
    public function isUsingCredit();

    /**
     * @return bool
     */
    public function isUsingToken();

    /**
     * @return float
     */
    public function getAmount();

    /**
     * @return string
     */
    public function getCurrency();

    /**
     * @return bool
     */
    public function hasToStoreToken();

    /**
     * @return CardDetails
     */
    public function getCardDetails();

    /**
     * @return PayerDetails
     */
    public function getPayerDetails();

    /**
     * @return IToken
     */
    public function getToken();
}