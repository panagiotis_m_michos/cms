<?php

namespace PaymentModule\Contracts;

use PayPalNVP\Response\GetExpressCheckoutDetailsResponse;
use SagePay\Token\Request\Details;

interface IPaymentResponse
{
    const TYPE_PAYPAL = 'PAYPAL';
    const TYPE_SAGE = 'SAGE';
    const TYPE_ON_ACCOUNT = 'ON_ACCOUNT';
    const TYPE_FREE = 'FREE_PURCHASE';

    const MEDIUM_ON_SITE = 'ONSITE';
    const MEDIUM_PHONE = 'PHONE';
    const MEDIUM_RENEWAL = 'AUTO_RENEWAL';
    const MEDIUM_COMPLIMENTARY = 'COMPLIMENTARY';
    const MEDIUM_NON_STANDARD = 'NON_STANDARD';

    /**
     * @return string
     */
    public function getPaymentType();
    
    /**
     * @return string
     */
    public function getPaymentMedium();
    
    /**
     * @return bool
     */
    public function isRedirectRequired();

    /**
     * @return string
     */
    public function getRedirectUrl();

    /**
     * @return bool
     */
    public function isSagePay();

    /**
     * @return bool
     */
    public function isPaypal();

    /**
     * @return GetExpressCheckoutDetailsResponse
     */
    public function getPaypalPaymentInfo();

    /**
     * @return Details
     */
    public function getSagePaymentInfo();

    /**
     * @return bool
     */
    public function isRecurringPayment();
}