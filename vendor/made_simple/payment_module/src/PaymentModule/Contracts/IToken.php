<?php

namespace PaymentModule\Contracts;

use Utils\Date;

interface IToken
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return string
     */
    public function getCardNumber();

    /**
     * @return string
     */
    public function getCardHolder();

    /**
     * @return string
     */
    public function getCardType();
    
    /**
     * @return Date
     */
    public function getCardExpiryDate();

    /**
     * @return string
     */
    public function getIdentifier();

    /**
     * @return string
     */
    public function getBillingFirstnames();

    /**
     * @return string
     */
    public function getBillingSurname();

    /**
     * @return string
     */
    public function getBillingAddress1();

    /**
     * @return string
     */
    public function getBillingAddress2();

    /**
     * @return string
     */
    public function getBillingCity();

    /**
     * @return string
     */
    public function getBillingCountry();

    /**
     * @return string
     */
    public function getBillingPostCode();

    /**
     * @return string
     */
    public function getBillingState();

    /**
     * @return string
     */
    public function getBillingPhone();

    /**
     * @return string
     */
    public function getDeliveryFirstnames();

    /**
     * @return string
     */
    public function getDeliverySurname();

    /**
     * @return string
     */
    public function getDeliveryAddress1();

    /**
     * @return string
     */
    public function getDeliveryAddress2();

    /**
     * @return string
     */
    public function getDeliveryCity();

    /**
     * @return string
     */
    public function getDeliveryCountry();

    /**
     * @return string
     */
    public function getDeliveryPostCode();

    /**
     * @return string
     */
    public function getDeliveryState();

    /**
     * @return string
     */
    public function getDeliveryPhone();
}