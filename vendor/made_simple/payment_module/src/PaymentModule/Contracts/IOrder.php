<?php

namespace PaymentModule\Contracts;

use UserModule\Contracts\ICustomer;

interface IOrder
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return ICustomer
     */
    public function getCustomer();
}