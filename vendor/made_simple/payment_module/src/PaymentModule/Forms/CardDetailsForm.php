<?php

namespace PaymentModule\Forms;

use FormModule\Types\CardCV2Type;
use FormModule\Types\CardHolderType;
use FormModule\Types\CardNumberType;
use FormModule\Types\IssueNumberType;
use FormModule\Types\MonthYearType;
use PaymentModule\Dto\CardDetails;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CardDetailsForm extends AbstractType
{
    /**
     * @var array
     */
    private $cardTypes;

    /**
     * @param array $cardTypes
     */
    public function __construct(array $cardTypes)
    {
        $this->cardTypes = $cardTypes;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        $builder->add('cardholder', new CardHolderType(), ['label' => 'Cardholder name:']);
        $builder->add(
            'cardType',
            'choice',
            [
                'label' => 'Card type:*',
                'choices' => $this->cardTypes,
                'choices_as_values' => FALSE,
                'expanded' => FALSE,
                'multiple' => FALSE,
                'empty_value' => '--- Select ---',
                'attr' => ['rv-on-change' => 'changeCardType']
            ]
        );
        $builder->add('cardNumber', new CardNumberType(), ['label' => 'Card number:*']);
        $builder->add('expiryDate', new MonthYearType(), [
            'label' => 'Expiry date:*',
            'format' => 'ddMMyy',
            'years' => range(date('Y'), date('Y') + 10, 1),
            'placeholder' => ['year' => 'YY', 'month' => 'MM']
        ]);
        $builder->add('validFrom', new MonthYearType(), [
            'label' => 'Valid from:',
            'format' => 'ddMMyy',
            'years' => range(date('Y'), date('Y') - 10, 1),
            'placeholder' => ['year' => 'YY', 'month' => 'MM'],
            'required' => FALSE
        ]);
        $builder->add('issueNumber', new IssueNumberType(), ['label' => 'Issue number:', 'required' => FALSE]);
        $builder->add('securityCode', new CardCV2Type(), ['label' => 'Security code:*']);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CardDetails::class
        ]);
    }

}