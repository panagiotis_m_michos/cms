<?php

namespace PaymentModule\Forms;

use FormModule\Types\PaymentType;
use PaymentModule\Dto\PaymentDetails;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PaymentForm extends AbstractType
{
    /**
     * @var string
     */
    private $cardTypes;

    /**
     * @var array
     */
    private $paymentTypes;

    /**
     * @var float|null
     */
    private $customerCredit;

    /**
     * @var bool
     */
    private $expanded;

    /**
     * @param array $cardTypes
     * @param array $paymentTypes
     * @param bool $expanded
     * @param float|null $customerCredit
     */
    public function __construct(array $cardTypes, array $paymentTypes, $expanded = TRUE, $customerCredit = NULL)
    {
        $this->cardTypes = $cardTypes;
        $this->paymentTypes = $paymentTypes;
        $this->customerCredit = $customerCredit;
        $this->expanded = $expanded;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        $builder->add(
            'paymentType',
            new PaymentType(),
            [
                'label' => FALSE,
                'choices' => $this->paymentTypes,
                'choices_as_values' => FALSE,
                'expanded' => $this->expanded,
                'multiple' => FALSE,
                'attr' => ['rv-on-change' => 'changePaymentType']
            ]);
        if ($this->customerCredit !== NULL) {
            $builder->add('useCredit', CheckboxType::class, [
                'attr' => ['data-credit' => $this->customerCredit, 'rv-on-change' => 'changeCredit'],
                'required' => FALSE,
                'label' => 'Use credit to make this payment'
            ]);
        }
        $builder
            ->add('cardDetails', new CardDetailsForm($this->cardTypes), ['label' => FALSE])
            ->add('payerDetails', new PayerDetailsForm(), ['label' => FALSE]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'validation_groups' => function (FormInterface $form) {
                /** @var PaymentDetails $data */
                $data = $form->getData();
                $groups = ['Default'];
                // do not validate on account payment
                if ($data->isUsingCredit() && $data->getAmount() <= $this->customerCredit) {
                    return $groups;
                }

                if (!$data->isCardPayment()) {
                    return $groups;
                }

                $groups[] = 'CardPayment';
                $cardDetails = $data->getCardDetails();
                if ($cardDetails && $cardDetails->requiresAdditionalCheck()) {
                    $anyField = FALSE;
                    if ($cardDetails->requiresIssueNumber()) {
                        $groups[] = 'IssueNumber';
                        $anyField = TRUE;
                    }
                    if ($cardDetails->requiresStartDate()) {
                        $groups[] = 'StartDate';
                        $anyField = TRUE;
                    }
                    if (!$anyField) {
                        $groups[] = 'StartDate';
                        $groups[] = 'IssueNumber';
                    }
                }
                $payerDetails = $data->getPayerDetails();
                if ($payerDetails && $payerDetails->isUsa()) {
                    $groups[] = 'State';
                }
                return $groups;
            },
            'data_class' => PaymentDetails::class
        ]);
    }
}