<?php

namespace PaymentModule\Forms;

use FormModule\Helpers\Country;
use FormModule\Helpers\State;
use PaymentModule\Dto\PayerDetails;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PayerDetailsForm extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        $builder->add('address1', 'text', ['label' => 'Address Line 1:*']);
        $builder->add('address2', 'text', ['label' => 'Address Line 2:']);
        $builder->add('city', 'text', ['label' => 'Town/City:*']);
        $builder->add('postcode', 'text', ['label' => 'Postcode:*']);
        $builder->add('country', 'choice', [
            'label' => 'Country:*',
            'choices' => Country::getCountries(),
            'empty_value' => '--- Select ---',
            'preferred_choices' => ['GB'],
            'attr' => ['rv-on-change' => 'changeCountry']
        ]);
        $builder->add('state', 'choice', ['choices' => State::getStates(), 'empty_value' => '--- Select ---', 'required' => FALSE]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PayerDetails::class
        ]);
    }

}