<?php

namespace PaymentModule\Exceptions;

use Exception;
use LogicException;
use TranslationModule\Config\MsgId;

class PaymentException extends LogicException
{
    /**
     * @param string $method
     * @return PaymentException
     */
    public static function unavailableMethod($method)
    {
        return new self(s_(MsgId::PAYMENT_METHOD_NOT_ALLOWED, $method));
    }

    /**
     * @param Exception $e
     * @return PaymentException
     */
    public static function fromPrevious(Exception $e)
    {
        return new self($e->getMessage(), $e->getCode(), $e);
    }

    /**
     * @param Exception $e
     * @return PaymentException
     */
    public static function failedOrder(Exception $e)
    {
        return new self(s_(MsgId::PAYMENT_ORDER_FAILED), $e->getCode(), $e);
    }
}