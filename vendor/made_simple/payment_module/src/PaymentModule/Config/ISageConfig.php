<?php

namespace PaymentModule\Config;

interface ISageConfig
{
    /**
     * @return string
     */
    public function getDescription();

    /**
     * @return string
     */
    public function getReturnUrl();

}