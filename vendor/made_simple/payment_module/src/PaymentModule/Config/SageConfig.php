<?php

namespace PaymentModule\Config;

use RouterModule\Helpers\ControllerHelper;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class SageConfig implements ISageConfig
{
    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    /**
     * @var ParameterBagInterface
     */
    private $config;

    /**
     * SageConfig constructor.
     * @param ControllerHelper $controllerHelper
     * @param ParameterBagInterface $config
     */
    public function __construct(ControllerHelper $controllerHelper, ParameterBagInterface $config)
    {
        $this->controllerHelper = $controllerHelper;
        $this->config = $config;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->config->get('payment.description');
    }

    /**
     * @return string
     */
    public function getReturnUrl()
    {
        return $this->controllerHelper->getSecureUrl('sage_validation');
    }
}