<?php

namespace PaymentModule\Config;

interface IPaypalConfig
{
    /**
     * @return string
     */
    public function getCustomField();

    /**
     * @return string
     */
    public function getBillingField();

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @return string
     */
    public function getCancelUrl();

    /**
     * @return string
     */
    public function getValidationUrl();

}