<?php

namespace PaymentModule\Config;

class EventLocator
{
    const PAYMENT_SUCCEEDED = 'payment.succeeded';
}