<?php

namespace PaymentModule\Config;

use RouterModule\Helpers\ControllerHelper;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class PaypalConfig implements IPaypalConfig
{
    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    /**
     * @var ParameterBagInterface
     */
    private $config;

    /**
     * SageConfig constructor.
     * @param ControllerHelper $controllerHelper
     * @param ParameterBagInterface $config
     */
    public function __construct(ControllerHelper $controllerHelper, ParameterBagInterface $config)
    {
        $this->controllerHelper = $controllerHelper;
        $this->config = $config;
    }

    /**
     * @return string
     */
    public function getCustomField()
    {
        return $this->config->get('paypal.custom_field');
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->config->get('paypal.description');
    }

    /**
     * @return string
     */
    public function getBillingField()
    {
        return $this->config->get('paypal.billing_field');
    }

    /**
     * @return string
     */
    public function getCancelUrl()
    {
        return $this->controllerHelper->getSecureUrl('paypal_cancellation');
    }

    /**
     * @return string
     */
    public function getValidationUrl()
    {
        return $this->controllerHelper->getSecureUrl('paypal_validation');
    }
}