<?php

namespace spec\PaymentModule\PaymentTypes;

use BasketModule\Contracts\IBasket;
use PaymentModule\Contracts\IPaymentResponse;
use PaymentModule\Exceptions\PaymentException;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use UserModule\Contracts\ICustomer;

class FreePurchasePaymentSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('PaymentModule\PaymentTypes\FreePurchasePayment');
        $this->shouldHaveType('PaymentModule\PaymentTypes\ILocalPaymentType');
    }

    function it_should_deduct_credit(ICustomer $customer, IBasket $basket)
    {
        $customer->getFullName()->willReturn('Test User');
        $basket->isFreePurchase()->willReturn(TRUE);
        $paymentResponse = $this->makePayment($customer, $basket);
        $paymentResponse->getCardHolder()->shouldBe('Test User');
        $paymentResponse->getPaymentType()->shouldBe(IPaymentResponse::TYPE_FREE);
    }

    function it_should_fail_when_basket_is_not_free(ICustomer $customer, IBasket $basket)
    {
        $basket->isFreePurchase()->willReturn(FALSE);
        $this->shouldThrow(PaymentException::class)->during('makePayment', [$customer, $basket]);
    }
}
