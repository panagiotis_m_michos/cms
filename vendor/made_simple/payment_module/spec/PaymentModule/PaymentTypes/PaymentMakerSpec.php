<?php

namespace spec\PaymentModule\PaymentTypes;

use BasketModule\Contracts\IBasket;
use PaymentModule\Contracts\IOrder;
use PaymentModule\Contracts\IPaymentData;
use PaymentModule\Contracts\IPaymentRedirection;
use PaymentModule\Contracts\IPaymentResponse;
use PaymentModule\PaymentTypes\IPaymentType;
use PaymentModule\Processors\IPaymentProcessor;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\HttpFoundation\RedirectResponse;
use UserModule\Contracts\ICustomer;

class PaymentMakerSpec extends ObjectBehavior
{
    /**
     * @var IPaymentProcessor
     */
    private $paymentProcessor;

    /**
     * @var IPaymentType
     */
    private $paymentType;

    /**
     * @var IPaymentRedirection
     */
    private $paymentRedirection;

    function let(IPaymentType $paymentType, IPaymentProcessor $paymentProcessor, IPaymentRedirection $paymentRedirection)
    {
        $this->beConstructedWith($paymentType, $paymentProcessor, $paymentRedirection);
        $this->paymentType = $paymentType;
        $this->paymentProcessor = $paymentProcessor;
        $this->paymentRedirection = $paymentRedirection;
    }
    
    function it_is_initializable()
    {
        $this->shouldHaveType('PaymentModule\PaymentTypes\PaymentMaker');
        $this->shouldHaveType('PaymentModule\PaymentTypes\IPaymentMaker');
    }
    
    function it_should_make_payment(ICustomer $customer, IBasket $basket, IPaymentData $paymentData, IPaymentResponse $paymentResponse, IOrder $order)
    {
        $redirection = new RedirectResponse('temp');
        $this->paymentType->makePayment($paymentData)->willReturn($paymentResponse);
        $this->paymentProcessor->processPayment($customer, $basket, $paymentResponse)->willReturn($order);
        $basket->getTotalPrice()->willReturn(10);
        $this->paymentRedirection->getConfirmation($order, $basket)->willReturn($redirection);
        $this->makePayment($customer, $basket, $paymentData)->shouldBe($redirection);
    }

    function it_should_return_redirection_if_payment_type_requires_it(ICustomer $customer, IBasket $basket, IPaymentData $paymentData, IPaymentResponse $paymentResponse)
    {
        $paymentResponse->isRedirectRequired()->willReturn(TRUE);
        $paymentResponse->getRedirectUrl()->willReturn('temp');
        $this->paymentType->makePayment($paymentData)->willReturn($paymentResponse);
        $returnRedirection = $this->makePayment($customer, $basket, $paymentData);
        $returnRedirection->getTargetUrl()->shouldBe('temp');
    }

    function it_should_authorize_payment(ICustomer $customer, IBasket $basket, IPaymentData $paymentData, IPaymentResponse $paymentResponse, IOrder $order)
    {
        $redirection = new RedirectResponse('temp');
        $this->paymentType->authorizePayment(10, ['data' => 1])->willReturn($paymentResponse);
        $this->paymentProcessor->processPayment($customer, $basket, $paymentResponse)->willReturn($order);
        $basket->getTotalPrice()->willReturn(10);
        $this->paymentRedirection->getConfirmation($order, $basket)->willReturn($redirection);
        $this->authorizePayment($customer, $basket, ['data' => 1])->shouldBe($redirection);
    }
}
