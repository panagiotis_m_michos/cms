<?php

namespace spec\PaymentModule\PaymentTypes;

use BasketModule\Contracts\IBasket;
use PaymentModule\Contracts\IPaymentResponse;
use PaymentModule\Exceptions\PaymentException;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use UserModule\Contracts\ICustomer;
use UserModule\Exceptions\CreditException;
use UserModule\Services\ICreditProvider;

class OnAccountPaymentSpec extends ObjectBehavior
{
    /**
     * @var ICreditProvider
     */
    private $creditProvider;

    function let(ICreditProvider $creditProvider)
    {
        $this->beConstructedWith($creditProvider);
        $this->creditProvider = $creditProvider;
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('PaymentModule\PaymentTypes\OnAccountPayment');
        $this->shouldHaveType('PaymentModule\PaymentTypes\ILocalPaymentType');
    }

    function it_should_take_credit(ICustomer $customer, IBasket $basket)
    {
        $basket->getTotalPrice()->willReturn(20);
        $basket->getCredit()->willReturn(20);
        $basket->setCredit(0)->shouldBeCalled();
        $this->creditProvider->subtractCredit($customer, 40);
        $paymentResponse = $this->makePayment($customer, $basket);
        $paymentResponse->getPaymentType()->shouldBe(IPaymentResponse::TYPE_ON_ACCOUNT);
    }


    function it_should_fail_if_there_is_not_enough_credit(ICustomer $customer, IBasket $basket)
    {
        $basket->getTotalPrice()->willReturn(30);
        $basket->getCredit()->willReturn(30);
        $this->creditProvider->subtractCredit($customer, 60)->willThrow(CreditException::class);
        $this->shouldThrow(PaymentException::class)->during('makePayment', [$customer, $basket]);
    }
}
