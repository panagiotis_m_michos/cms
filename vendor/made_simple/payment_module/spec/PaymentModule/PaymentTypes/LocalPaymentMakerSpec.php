<?php

namespace spec\PaymentModule\PaymentTypes;

use BasketModule\Contracts\IBasket;
use PaymentModule\Contracts\IOrder;
use PaymentModule\Contracts\IPaymentRedirection;
use PaymentModule\Contracts\IPaymentResponse;
use PaymentModule\PaymentTypes\ILocalPaymentType;
use PaymentModule\Processors\IPaymentProcessor;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\HttpFoundation\RedirectResponse;
use UserModule\Contracts\ICustomer;

class LocalPaymentMakerSpec extends ObjectBehavior
{
    /**
     * @var IPaymentProcessor
     */
    private $paymentProcessor;

    /**
     * @var ILocalPaymentType
     */
    private $paymentType;

    /**
     * @var IPaymentRedirection
     */
    private $paymentRedirection;

    function let(ILocalPaymentType $paymentType, IPaymentProcessor $paymentProcessor, IPaymentRedirection $paymentRedirection)
    {
        $this->beConstructedWith($paymentType, $paymentProcessor, $paymentRedirection);
        $this->paymentType = $paymentType;
        $this->paymentProcessor = $paymentProcessor;
        $this->paymentRedirection = $paymentRedirection;
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('PaymentModule\PaymentTypes\LocalPaymentMaker');
        $this->shouldHaveType('PaymentModule\PaymentTypes\ILocalPaymentMaker');
    }

    function it_should_make_payment(ICustomer $customer, IBasket $basket, IPaymentResponse $paymentResponse, IOrder $order)
    {
        $redirection = new RedirectResponse('temp');
        $this->paymentType->makePayment($customer, $basket)->willReturn($paymentResponse);
        $this->paymentProcessor->processPayment($customer, $basket, $paymentResponse)->willReturn($order);
        $basket->getTotalPrice()->willReturn(10);
        $this->paymentRedirection->getConfirmation($order, $basket)->willReturn($redirection);
        $this->makePayment($customer, $basket)->shouldBe($redirection);
    }
}
