<?php

namespace spec\PaymentModule\Loggers;

use Exception;
use PaymentModule\Contracts\IPaymentData;
use PaymentModule\Loggers\IPaymentLogger;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Log\LoggerInterface;
use RouterModule\Helpers\ControllerHelper;
use UserModule\Contracts\ICustomer;

class PaymentLoggerSpec extends ObjectBehavior
{
    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    function let(LoggerInterface $logger, ControllerHelper $controllerHelper, IPaymentLogger $transactionHandler)
    {
        $this->beConstructedWith($logger, $controllerHelper, $transactionHandler);
        $this->controllerHelper = $controllerHelper;
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('PaymentModule\Loggers\PaymentLogger');
        $this->shouldHaveType('PaymentModule\Loggers\IPaymentLogger');
    }

    function it_should_not_set_flash_message_if_message_is_not_provided(ICustomer $customer, IPaymentData $paymentData)
    {
        $this->controllerHelper->setFlashMessage()->shouldNotBeCalled();
        $this->error(new Exception(), $customer, $paymentData);
    }
}
