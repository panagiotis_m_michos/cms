<?php

require_once __DIR__ '/../vendor/autoload.php';

use FeatureModule\Context;
use FeatureModule\Feature;
use FeatureModule\FeatureManager;
use FeatureModule\FeatureToggle;
use FeatureModule\Matchers\HashContainer;
use FeatureModule\Matchers\Identity;

// example config

$featureFlags = [
    'feature1' => [
        'enabled' => true, //enabled => false will disable and not check other conditions
        'customer_id' => [
            ['range' => '1..3'],
            343
        ],
        'user_id' => 1,
        'ip' => [
            '192.168.100.34/24'
        ],
        'cookie' => [
            'a' => 1
        ]
    ],
];

//load context

$context = new Context();
$context->set('customer_id', new Identity(3));
$context->set('user_id', new Identity(1));
$context->set('cookie', new HashContainer($_COOKIE));

// load all required feature flags

$featureManager = new FeatureManager();
$featureManager->addMatcher('feature1', 'customer_id', new Identity(3));
$featureManager->addMatcher('feature1', 'cookie', new HashContainer(['test' => 1]));

// matching feature class
$featureToggle = new FeatureToggle($featureManager, $context);

//test features
if ($featureToggle->isEnabled('feature1')) {
    echo "enabled";
}

//to use static methods initialize first
Feature::initialize($featureToggle);

if (Feature::isEnabled('feature1')) {
    echo "enabled";
}