How to use:

As a dependency:

```
use FeatureModule\FeatureToggle;
...

if ($featureToggle->isEnabled('idCheck')) {
    echo "enabled";
}
```

Statically:

```
use FeatureModule\Feature;
...

if (Feature::isEnabled('idCheck')) {
    echo "enabled";
}
```

To integrate :

 - load context
 - load FeatureExt
 - import yml with key feature_flags

```
//example load context

$context = $this->createContext($containerBuilder->get(RouterExt::REQUEST));
$containerBuilder->set(FeatureExt::CONTEXT, $context);

/**
 * @param Request $request
 * @return Context
 */
public function createContext(Request $request)
{
    $context = new Context();
    $context->set(Context::TYPE_IP, new Ip($request->getClientIp()));
    $context->set(Context::TYPE_COOKIE, new HashContainer($request->cookies->all()));
    $context->set(Context::TYPE_QUERY_PARAM, new HashContainer($request->query->all()));
    if (Customer::isSignedIn()) {
        $customer = Customer::getSignedIn();
        $context->set('customer_id', new Identity($customer->getId()));
    }
    if (FUser::isSignedIn()) {
        $user = FUser::getSignedIn();
        $context->set('user_id', new Identity($user->getId()));
    }
    return $context;
}

```

```
# example yml file

feature_flags:
    # will match any of the conditions specified below enable: true
    id_check:
        enabled: true
        ip:
            - 192.168.0.1
            - 192.168.200.1/24
        cookie:
            isDeveloper: true
        query_params:
            debug_id_check: true
        # will match query parameter featureIdCheck, enable it when its true, disable it when its false (by setting a cookie)
        toggle_cookie:
            featureIdCheck: 3600
        user_id:
            range: [1000..2000]
        customer_id:
            0: 123212
            regexp:
                - 1238*
            range:
                - [123..124]
    # will always match
    show_blog:
        enabled: true
    # will match customer with id 1000 or user with id 2000 
    developer_test:
        enabled: true
        customer_id: 1000
        user_id: 2000
    # disabled, will never match
    old_feature:
        enabled: false
        customer_id: 1000
        user_id: 2000
```
        
            

