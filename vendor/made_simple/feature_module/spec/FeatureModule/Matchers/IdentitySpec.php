<?php

namespace spec\FeatureModule\Matchers;

use FeatureModule\Matchers\Identity;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class IdentitySpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith(true);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('FeatureModule\Matchers\Identity');
        $this->shouldHaveType('FeatureModule\Matchers\IMatcher');
    }

    function it_should_be_equal()
    {
        $this->isEqual(new Identity(TRUE))->shouldBe(TRUE);
    }

    function it_should_not_be_equal()
    {
        $this->isEqual(new Identity(FALSE))->shouldBe(FALSE);
    }
}
