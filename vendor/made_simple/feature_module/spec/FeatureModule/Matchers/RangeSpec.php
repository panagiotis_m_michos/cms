<?php

namespace spec\FeatureModule\Matchers;

use FeatureModule\Matchers\Identity;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class RangeSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedThrough('fromString', ['1..10']);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('FeatureModule\Matchers\Range');
        $this->shouldHaveType('FeatureModule\Matchers\IMatcher');
    }

    function it_should_be_equal()
    {
        $this->isEqual(new Identity(1))->shouldBe(TRUE);
        $this->isEqual(new Identity(5))->shouldBe(TRUE);
        $this->isEqual(new Identity(10))->shouldBe(TRUE);
    }

    function it_should_not_be_equal()
    {
        $this->isEqual(new Identity(FALSE))->shouldBe(FALSE);
        $this->isEqual(new Identity(0))->shouldBe(FALSE);
        $this->isEqual(new Identity(-10))->shouldBe(FALSE);
        $this->isEqual(new Identity(11))->shouldBe(FALSE);
    }

}
