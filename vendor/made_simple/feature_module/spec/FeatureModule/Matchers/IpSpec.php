<?php

namespace spec\FeatureModule\Matchers;

use FeatureModule\Matchers\Ip;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class IpSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('127.0.0.1');
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('FeatureModule\Matchers\Ip');
        $this->shouldHaveType('FeatureModule\Matchers\IMatcher');
    }

    function it_should_be_equal()
    {
        $this->isEqual(new Ip('127.0.0.1'))->shouldBe(TRUE);
        $this->isEqual(new Ip('127.0.0.2'))->shouldBe(FALSE);
    }

    function it_should_be_equal_using_subdomain()
    {
        $this->beConstructedWith('127.0.0.1/24');
        $this->isEqual(new Ip('127.0.0.2'))->shouldBe(TRUE);
        $this->isEqual(new Ip('127.0.1.2'))->shouldBe(FALSE);
    }
}
