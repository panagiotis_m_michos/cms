<?php

namespace spec\FeatureModule\Matchers;

use FeatureModule\Matchers\HashContainer;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class CookieToggleSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith(['cookieName1', 'cookieName2']);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('FeatureModule\Matchers\CookieToggle');
        $this->shouldHaveType('FeatureModule\Matchers\IMatcher');
    }

    function it_should_match_query_parameter_and_enable()
    {
        $this->beConstructedWith(['cookieName1' => 1, 'cookieName2' => 1]);
        $this->processQuery(new HashContainer(['cookieName1' => TRUE]));
        $this->isEqual(new HashContainer([]))->shouldBe(TRUE);
    }

    function it_should_match_query_parameter_and_disable()
    {
        $this->beConstructedWith(['cookieName1' => 1, 'cookieName2' => 1]);
        $this->processQuery(new HashContainer(['cookieName1' => FALSE]));
        $this->isEqual(new HashContainer([]))->shouldBe(FALSE);
    }

    function it_should_match_query_parameter_and_not_disable_enable()
    {
        $this->beConstructedWith(['cookieName1' => 1, 'cookieName2' => 1]);
        $this->processQuery(new HashContainer(['cookieName1' => 'unexpected value']));
        $this->isEqual(new HashContainer([]))->shouldBe(FALSE);
    }

    function it_should_match_cookie_parameter()
    {
        $this->beConstructedWith(['cookieName1' => 1, 'cookieName2' => 1]);
        $this->isEqual(new HashContainer(['cookieName1' => 'any value']))->shouldBe(TRUE);
    }

    function it_should_not_match_cookie_with_different_key()
    {
        $this->beConstructedWith(['cookieName1' => 1, 'cookieName2' => 1]);
        $this->isEqual(new HashContainer(['cookieName3' => 'any value']))->shouldBe(FALSE);
    }

    function it_should_not_match_empy_query_and_cookie()
    {
        $this->beConstructedWith(['cookieName1' => 1, 'cookieName2' => 1]);
        $this->processQuery(new HashContainer([]));
        $this->isEqual(new HashContainer([]))->shouldBe(FALSE);
    }

    function it_should_convert_query_param_string_and_match()
    {
        $this->beConstructedWith(['cookieName1' => 1, 'cookieName2' => 1]);
        $this->processQuery(new HashContainer(['cookieName1' => 'true']));
        $this->isEqual(new HashContainer([]))->shouldBe(TRUE);
    }

    function it_should_convert_query_param_string_and_not_match()
    {
        $this->beConstructedWith(['cookieName1' => 1, 'cookieName2' => 1]);
        $this->processQuery(new HashContainer(['cookieName1' => 'false']));
        $this->isEqual(new HashContainer([]))->shouldBe(FALSE);
    }
}
