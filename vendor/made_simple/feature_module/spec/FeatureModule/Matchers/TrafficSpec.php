<?php

namespace spec\FeatureModule\Matchers;

use Doctrine\Common\Cache\Cache;
use FeatureModule\Matchers\HashContainer;
use FeatureModule\Matchers\Traffic;
use Identity;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class TrafficSpec extends ObjectBehavior
{
    /**
     * @var Cache
     */
    private $cache;

    function let(Cache $cache)
    {
        $this->beConstructedWith('testFeature1', '20%', $cache);
        $this->cache = $cache;
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('FeatureModule\Matchers\Traffic');
        $this->shouldHaveType('FeatureModule\Matchers\IMatcher');
    }

    function it_should_not_match_request_if_cookie_is_false()
    {
        $container = new HashContainer(['testFeature1' => FALSE]);
        $this->isEqual($container)->shouldBe(FALSE);
    }

    function it_should_match_request_if_cookie_is_set()
    {
        $container = new HashContainer(['testFeature1' => TRUE]);
        $this->isEqual($container)->shouldBe(TRUE);
    }

    function it_should_match_every_fifth_request()
    {
        $container = new HashContainer([]);
        $this->cache->contains(Argument::any())->willReturn(TRUE);
        $this->cache->fetch(Argument::any())->willReturn(199);
        $this->cache->save(Argument::any(), 200, Traffic::MONTH_IN_SECS)->shouldBeCalled();
        $this->isEqual($container)->shouldBe(TRUE);
    }

    function it_should_not_match_if_its_not_fifth_request()
    {
        $container = new HashContainer([]);
        $this->cache->contains(Argument::any())->willReturn(TRUE);
        $this->cache->fetch(Argument::any())->willReturn(121);
        $this->cache->save(Argument::any(), 122, Traffic::MONTH_IN_SECS)->shouldBeCalled();
        $this->isEqual($container)->shouldBe(FALSE);
    }

    function it_should_start_counter()
    {
        $container = new HashContainer([]);
        $this->cache->contains(Argument::any())->willReturn(FALSE);
        $this->cache->save(Argument::any(), 1, Traffic::MONTH_IN_SECS)->shouldBeCalled();
        $this->isEqual($container)->shouldBe(FALSE);
    }

    function it_should_save_computed_counter()
    {
        $container = new HashContainer([]);
        $this->cache->contains(Argument::any())->willReturn(TRUE);
        $this->cache->fetch(Argument::any())->willReturn(1);
        $this->cache->save(Argument::any(), 2, Traffic::MONTH_IN_SECS)->shouldBeCalled();
        $this->isEqual($container)->shouldBe(FALSE);
        $this->isEqual($container)->shouldBe(FALSE);
        $this->isEqual($container)->shouldBe(FALSE);
    }
}
