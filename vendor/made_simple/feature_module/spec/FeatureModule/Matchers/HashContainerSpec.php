<?php

namespace spec\FeatureModule\Matchers;

use FeatureModule\Matchers\HashContainer;
use FeatureModule\Matchers\Identity;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class HashContainerSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith(['a' => 1]);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('FeatureModule\Matchers\HashContainer');
        $this->shouldHaveType('FeatureModule\Matchers\IMatcher');
    }

    function it_should_be_equal()
    {
        $this->isEqual(new HashContainer(['a' => 1, 'b' => 1]))->shouldBe(TRUE);
        $this->isEqual(new Identity(['a' => 1]))->shouldBe(TRUE);
    }

    function it_should_not_be_equal()
    {

        $this->isEqual(new HashContainer(['c' => 1, 'b' => 1]))->shouldBe(FALSE);
    }
}
