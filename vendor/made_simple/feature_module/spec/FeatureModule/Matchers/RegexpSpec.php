<?php

namespace spec\FeatureModule\Matchers;

use FeatureModule\Matchers\Identity;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class RegexpSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith(".*@test.com");
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('FeatureModule\Matchers\Regexp');
        $this->shouldHaveType('FeatureModule\Matchers\IMatcher');
    }

    function it_should_be_equal()
    {
        $this->isEqual(new Identity('test@test.com'))->shouldBe(TRUE);
    }

    function it_should_not_be_equal()
    {
        $this->isEqual(new Identity('test@testme.com'))->shouldBe(FALSE);
    }
}
