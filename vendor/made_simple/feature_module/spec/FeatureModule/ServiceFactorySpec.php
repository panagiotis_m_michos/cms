<?php

namespace spec\FeatureModule;

use FeatureModule\Context;
use FeatureModule\Feature;
use FeatureModule\FeatureManager;
use FeatureModule\FeatureToggle;
use FeatureModule\Matchers\Identity;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\DependencyInjection\Container;

class ServiceFactorySpec extends ObjectBehavior
{
    function let(Container $container)
    {
        $featureManager = new FeatureManager();
        $featureManager->addMatcher('test1', Context::TYPE_ENABLED, new Identity(TRUE));
        $featureManager->addMatcher('test2', Context::TYPE_ENABLED, new Identity(FALSE));
        $featureToggle = new FeatureToggle($featureManager, new Context());
        $this->beConstructedWith($container, $featureToggle);
        $container->get('yes')->willReturn('yes');
        $container->get('no')->willReturn('no');
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('FeatureModule\ServiceFactory');
    }

    function it_should_create_service()
    {
        $this->createService('test1', 'yes', 'no')->shouldBe('yes');
        $this->createService('test2', 'yes', 'no')->shouldBe('no');
    }
}
