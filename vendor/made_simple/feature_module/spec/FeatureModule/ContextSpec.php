<?php

namespace spec\FeatureModule;

use FeatureModule\Matchers\Ip;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ContextSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('custom', new Ip('3434'));
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('FeatureModule\Context');
    }

    function it_should_set_matcher()
    {
        $this->set('custom', new Ip('34'));
    }

    function it_should_get_matcher()
    {
        $ip = new Ip('34');
        $this->set('custom', $ip);
        $this->get('custom')->shouldBe($ip);
    }
}
