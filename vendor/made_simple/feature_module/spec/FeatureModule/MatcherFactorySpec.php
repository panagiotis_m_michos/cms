<?php

namespace spec\FeatureModule;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class MatcherFactorySpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('FeatureModule\MatcherFactory');
    }
}
