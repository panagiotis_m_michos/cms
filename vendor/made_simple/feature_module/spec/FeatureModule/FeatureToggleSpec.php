<?php

namespace spec\FeatureModule;

use FeatureModule\Context;
use FeatureModule\FeatureManager;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class FeatureToggleSpec extends ObjectBehavior
{
    function let(Context $context)
    {
        $this->beConstructedWith(new FeatureManager(), $context);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('FeatureModule\FeatureToggle');
    }
}
