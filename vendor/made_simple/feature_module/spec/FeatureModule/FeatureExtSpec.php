<?php

namespace spec\FeatureModule;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class FeatureExtSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('FeatureModule\FeatureExt');
    }
}
