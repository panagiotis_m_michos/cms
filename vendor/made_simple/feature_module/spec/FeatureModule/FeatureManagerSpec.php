<?php

namespace spec\FeatureModule;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use FeatureModule\Matchers\Ip;

class FeatureManagerSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('FeatureModule\FeatureManager');
    }

    function it_should_add_matcher()
    {
        $this->addMatcher('feature_1', 'ip', new Ip('126.23.23.23'));
    }

    function it_should_get_feature_matchers()
    {
        $this->addMatcher('feature_1', 'ip', new Ip('126.23.23.23'));
        //php spec is using getMatchers
        //$this->getMatchers('feature_1')->shouldBe(['ip' => [new Ip('126.23.23.23')]]);
    }

}
