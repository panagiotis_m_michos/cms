<?php

namespace tests\FeatureModule;

use FeatureModule\Context;
use FeatureModule\Feature;
use FeatureModule\FeatureManager;
use FeatureModule\FeatureToggle;
use FeatureModule\Matchers\Identity;
use FeatureModule\Matchers\Ip;
use PHPUnit_Framework_TestCase;

class FeatureTest extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $this->featureManager = new FeatureManager();
        $this->featureManager->addMatcher('story1', 'enabled', new Identity(true));
        $this->featureManager->addMatcher('story2', 'ip', new Ip('127.0.0.1'));
        $this->featureManager->addMatcher('story3', 'enabled', new Identity(false));
        $this->featureManager->addMatcher('story3', 'ip', new Ip('127.0.0.2'));
        Feature::initialize(new FeatureToggle($this->featureManager, new Context()));
    }

    public function testFeatureEnabled()
    {
        $this->assertTrue(Feature::isEnabled('story1'));
    }

    public function testFeatureDisabled()
    {
        $this->assertFalse(Feature::isEnabled('story1_does_not_exist'));
    }

    public function testFeatureWithIp()
    {
        Feature::initialize(new FeatureToggle($this->featureManager, new Context('ip', new Ip('127.0.0.1'))));
        $this->assertTrue(Feature::isEnabled('story2'));
    }

    public function testFeatureWithIncorrectIp()
    {
        Feature::initialize(new FeatureToggle($this->featureManager, new Context('ip', new Ip('127.0.0.2'))));
        $this->assertFalse(Feature::isEnabled('story2'));
    }

    public function testFeatureWithDisabledAndCorrectIp()
    {
        Feature::initialize(new FeatureToggle($this->featureManager, new Context('ip', new Ip('127.0.0.2'))));
        $this->assertFalse(Feature::isEnabled('story3'));
    }
}