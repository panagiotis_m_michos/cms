<?php

namespace tests\FeatureModule;

use FeatureModule\Context;
use FeatureModule\MatcherFactory;

class MatcherFactoryTest extends \PHPUnit_Framework_TestCase
{
    public function testCreate()
    {
        $matcher = MatcherFactory::create(Context::TYPE_ENABLED, 1);
        $this->assertInstanceOf('FeatureModule\Matchers\Identity', $matcher);
        $matcher = MatcherFactory::create('custom', 3);
        $this->assertInstanceOf('FeatureModule\Matchers\Identity', $matcher);
        $matcher = MatcherFactory::create(Context::TYPE_COOKIE, ['a' => 3]);
        $this->assertInstanceOf('FeatureModule\Matchers\HashContainer', $matcher);
        $matcher = MatcherFactory::create('custom', [MatcherFactory::TYPE_RANGE => '3..3']);
        $this->assertInstanceOf('FeatureModule\Matchers\Range', $matcher);
        $matcher = MatcherFactory::create('custom', [MatcherFactory::TYPE_REGEXP => '3..3']);
        $this->assertInstanceOf('FeatureModule\Matchers\Regexp', $matcher);
    }
}