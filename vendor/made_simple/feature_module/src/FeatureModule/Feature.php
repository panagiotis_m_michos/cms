<?php

namespace FeatureModule;

use RuntimeException;

final class Feature
{

    /**
     * @var FeatureToggle
     */
    private static $featureToggle;

    /**
     * @param FeatureToggle $featureToggle
     */
    public static function initialize(FeatureToggle $featureToggle)
    {
        self::$featureToggle = $featureToggle;
    }

    /**
     * @param string $featureName
     * @return bool
     * @throws RuntimeException
     */
    public static function isEnabled($featureName)
    {
        if (!self::$featureToggle) {
            throw new RuntimeException("Please initialize feature first");
        }
        return self::$featureToggle->isEnabled($featureName);
    }

}