<?php

namespace FeatureModule;

final class FeatureToggle implements IFeature
{
    /**
     * @var FeatureManager
     */
    private $featureManager;

    /**
     * @var Context
     */
    private $context;

    /**
     * @param FeatureManager $featureManager
     * @param Context $context
     */
    public function __construct(FeatureManager $featureManager, Context $context)
    {
        $this->featureManager = $featureManager;
        $this->context = $context;
    }

    /**
     * @param string $featureName
     * @return bool
     */
    public function isEnabled($featureName)
    {
        $matchers = $this->featureManager->getMatchers($featureName);
        //  enabled: false disables the feature
        if (!empty($matchers[Context::TYPE_ENABLED][0])) {
            if ($matchers[Context::TYPE_ENABLED][0]->getValue() === FALSE) {
                return FALSE;
            } else {
                unset($matchers[Context::TYPE_ENABLED]);
                if (empty($matchers)) {
                    return TRUE;
                }
            }
        }
        foreach ($matchers as $contextType => $collection) {
            if ($context = $this->context->get($contextType)) {
                foreach ($collection as $matcher) {
                    if ($matcher->isEqual($context)) {
                        return TRUE;
                    }
                }
            }
        }
        return FALSE;
    }
}
