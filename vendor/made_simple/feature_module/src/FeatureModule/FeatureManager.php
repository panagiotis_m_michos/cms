<?php

namespace FeatureModule;

use FeatureModule\Matchers\IMatcher;

final class FeatureManager
{
    /**
     * @var array
     */
    private $matchers = [];

    /**
     * @param string $featureFlag
     * @param string $contextType
     * @param IMatcher $matcher
     */
    public function addMatcher($featureFlag, $contextType, IMatcher $matcher)
    {
        $this->matchers[$featureFlag][$contextType][] = $matcher;
    }

    /**
     * @param string $featureFlag
     * @return array
     */
    public function getMatchers($featureFlag)
    {
        return isset($this->matchers[$featureFlag]) ? $this->matchers[$featureFlag] : [];
    }
}
