<?php

namespace FeatureModule;

use BootstrapModule\Ext\IExtension;
use FeatureModule\Matchers\CookieToggle;
use FeatureModule\Matchers\Traffic;
use RouterModule\RouterExt;
use Symfony\Component\DependencyInjection\Container;

final class FeatureExt implements IExtension
{
    const FEATURE_MANAGER = 'feature_module.feature_manager';
    const FEATURE = 'feature_module.feature_toggle';
    //load context and set di
    const CONTEXT = 'feature_module.context';
    //provide this key in di
    const CACHE = 'feature_module.cache';

    const CONFIG_KEY = 'feature_flags';

    /**
     * @param Container $container
     */
    public function load(Container $container)
    {
        /** @var Context $context */
        $context = $container->get(self::CONTEXT);
        $featureManager = new FeatureManager();
        $features = $container->getParameter(self::CONFIG_KEY);
        foreach ($features as $featureName => $contextTypes) {
            foreach ($contextTypes as $type => $values) {
                // requires special handling
                if ($type === MatcherFactory::TYPE_TRAFFIC) {
                    $matcher = new Traffic($featureName, $values, $container->get(self::CACHE));
                    $featureManager->addMatcher($featureName, Context::TYPE_COOKIE, $matcher);
                } elseif ($type === MatcherFactory::TYPE_COOKIE_TOGGLE) {
                    $matcher = new CookieToggle($values);
                    $matcher->processQuery($context->get(Context::TYPE_QUERY_PARAM));
                    $featureManager->addMatcher($featureName, Context::TYPE_COOKIE, $matcher);
                } elseif (is_array($values)) {
                    foreach ($values as $value) {
                        $matcher = MatcherFactory::create($type, $value);
                        $featureManager->addMatcher($featureName, $type, $matcher);
                    }
                } else {
                    $matcher = MatcherFactory::create($type, $values);
                    $featureManager->addMatcher($featureName, $type, $matcher);
                }
            }
        }
        $featureToggle = new FeatureToggle($featureManager, $context);
        Feature::initialize($featureToggle);

        // route module feature
        $routeProcessor = class_exists('RouterModule\RouterExt', TRUE) && $container->has(RouterExt::ROUTE_PROCESSOR) ? $container->get(RouterExt::ROUTE_PROCESSOR) : NULL;
        if ($routeProcessor) {
            $routeProcessor->setFeatureToggle($featureToggle);
        }

        $container->set(self::FEATURE, $featureToggle);
        $container->set(self::FEATURE_MANAGER, $featureManager);
    }
}
