<?php

namespace FeatureModule;

use Symfony\Component\DependencyInjection\Container;

class ServiceFactory
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var FeatureToggle
     */
    private $featureToggle;

    /**
     * @param Container $container
     */
    public function __construct(Container $container, FeatureToggle $featureToggle)
    {
        $this->container = $container;
        $this->featureToggle = $featureToggle;
    }

    /**
     * @param string $featureName
     * @param string $then
     * @param string $else
     * @return object
     */
    public function createService($featureName, $then, $else)
    {
        return $this->featureToggle->isEnabled($featureName) ? $this->container->get($then) : $this->container->get($else);
    }

}