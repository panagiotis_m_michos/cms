<?php

namespace FeatureModule;

interface IFeature
{
    /**
     * @param string $featureName
     * @return bool
     */
    public function isEnabled($featureName);
}