<?php

namespace FeatureModule;

use FeatureModule\Matchers\HashContainer;
use FeatureModule\Matchers\Identity;
use FeatureModule\Matchers\IMatcher;
use FeatureModule\Matchers\Ip;
use FeatureModule\Matchers\Range;
use FeatureModule\Matchers\Regexp;

class MatcherFactory
{
    const TYPE_REGEXP = 'regexp';
    const TYPE_RANGE = 'range';
    const TYPE_TRAFFIC = 'traffic_split';
    const TYPE_COOKIE_TOGGLE = 'toggle_cookie';

    /**
     * @param string $type
     * @param mixed $value
     * @return IMatcher
     */
    public static function create($type, $value)
    {
        $testType = NULL;
        if (in_array($type, Context::$types)) {
            $testType = $type;
        } else if (is_array($value)) {
            $testType = key($value);
            $value = current($value);
        }
        switch ($testType) {
            case Context::TYPE_ENABLED:
                return new Identity($value);
            case Context::TYPE_IP:
                return new Ip($value);
            case self::TYPE_REGEXP:
                return new Regexp($value);
            case self::TYPE_RANGE:
                return Range::fromString($value);
            case Context::TYPE_QUERY_PARAM:
            case Context::TYPE_COOKIE:
                return new HashContainer($value);
            default:
                return new Identity($value);
        }
    }
}
