<?php

namespace FeatureModule\Matchers;

class Regexp extends Identity
{
    /**
     * @param IMatcher $matcher
     * @return bool
     */
    public function isEqual(IMatcher $matcher)
    {
        if (parent::isEqual($matcher)) {
            return TRUE;
        }
        $regexp = $this->getValue();
        return preg_match('#' . $regexp . '#', $matcher->getValue()) === 1;
    }
}
