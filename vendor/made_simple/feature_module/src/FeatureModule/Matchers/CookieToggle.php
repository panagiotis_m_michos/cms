<?php

namespace FeatureModule\Matchers;

class CookieToggle implements IMatcher
{
    /**
     * @var bool
     */
    private $requestMatching;

    /**
     * @var array
     */
    private $cookieNames;

    /**
     * @param array $cookieNames
     */
    public function __construct(array $cookieNames)
    {
        $this->cookieNames = $cookieNames;
    }

    /**
     * @param HashContainer $query
     */
    public function processQuery(HashContainer $query)
    {
        $params = $query->getValue();
        foreach ($this->cookieNames as $key => $time) {
            if (isset($params[$key])) {
                $queryParamValue = $params[$key];
                if (is_string($queryParamValue)) {
                    if ($queryParamValue === 'true') $queryParamValue = TRUE;
                    elseif ($queryParamValue === 'false') $queryParamValue = FALSE;
                }
                if ($queryParamValue === TRUE) {
                    $this->requestMatching = TRUE;
                    setcookie($key, '1', time() + $time, '/');
                    break;
                } elseif ($queryParamValue === FALSE) {
                    $this->requestMatching = FALSE;
                    setcookie($key, '0', time() - 10, '/');
                    break;
                }
            }
        }
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->cookieNames;
    }

    /**
     * @param IMatcher $matcher
     * @return bool
     */
    public function isEqual(IMatcher $matcher)
    {
        if ($this->requestMatching !== NULL) {
            return $this->requestMatching;
        }
        $cookie = (array)$matcher->getValue();
        foreach ($this->cookieNames as $key => $time) {
            if (!empty($cookie[$key])) {
                $this->requestMatching = TRUE;
                break;
            }
        }
        return $this->requestMatching !== NULL ? $this->requestMatching : FALSE;
    }
}
