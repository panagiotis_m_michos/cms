<?php

namespace FeatureModule\Matchers;

class HashContainer implements IMatcher
{
    /**
     * @var array
     */
    private $hash;

    /**
     * @param array $hash
     */
    public function __construct(array $hash)
    {
        $this->hash = $hash;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->hash;
    }

    /**
     * @param IMatcher $matcher
     * @return bool
     */
    public function isEqual(IMatcher $matcher)
    {
        $check = (array) $matcher->getValue();
        foreach ($this->hash as $key => $value) {
            if (isset($check[$key])) {
                return $check[$key] === $value;
            }
        }
        return FALSE;
    }
}
