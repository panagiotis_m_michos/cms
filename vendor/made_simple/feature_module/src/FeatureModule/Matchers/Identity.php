<?php

namespace FeatureModule\Matchers;

class Identity implements IMatcher
{
    /**
     * @var mixed
     */
    private $value;

    /**
     * @param mixed $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param IMatcher $matcher
     * @return bool
     */
    public function isEqual(IMatcher $matcher)
    {
        return $this->value === $matcher->getValue();
    }
}
