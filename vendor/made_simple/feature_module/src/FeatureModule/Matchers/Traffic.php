<?php

namespace FeatureModule\Matchers;

use Doctrine\Common\Cache\Cache;

class Traffic implements IMatcher
{
    const MONTH_IN_SECS = 2592000;

    /**
     * @var string
     */
    private $featureName;

    /**
     * @var string
     */
    private $matchEvery;

    /**
     * @var Cache
     */
    private $cache;

    /**
     * @var bool
     */
    private $requestMatching;

    /**
     * @param string $featureName
     * @param string $matchPercent
     * @param Cache $cache
     */
    public function __construct($featureName, $matchPercent, Cache $cache)
    {
        $percentInt = (int)$matchPercent;
        $this->featureName = $featureName;
        $this->matchEvery = $percentInt > 0 && $percentInt <= 100 ? 100 / $percentInt : 0;
        $this->cacheKey = sprintf('feature_%s_counter', $this->featureName);
        $this->cache = $cache;
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->matchEvery;
    }

    /**
     * @param IMatcher $matcher
     * @return bool
     */
    public function isEqual(IMatcher $matcher)
    {
        if ($this->requestMatching !== NULL) {
            return $this->requestMatching;
        }
        $cookies = (array) $matcher->getValue();
        if (isset($cookies[$this->featureName])) {
            return (bool) $cookies[$this->featureName] === TRUE;
        }

        if ($this->cache->contains($this->cacheKey)) {
            $currentCounter = (int) $this->cache->fetch($this->cacheKey);
            $currentCounter++;

        } else {
            $currentCounter = 1;
        }
        $this->cache->save($this->cacheKey, $currentCounter, self::MONTH_IN_SECS);
        // cache computation for current request
        $this->requestMatching = $currentCounter % $this->matchEvery === 0;
        $cookieSet = setcookie($this->featureName, $this->requestMatching ? '1' : '0', time() + self::MONTH_IN_SECS);
        // if cookie could not be set
        if ($cookieSet === FALSE) {
            $this->requestMatching = FALSE;
        }
        return $this->requestMatching;
    }
}
