<?php

namespace FeatureModule\Matchers;

use Symfony\Component\HttpFoundation\IpUtils;

final class Ip extends Identity
{
    /**
     * @param IMatcher $matcher
     * @return bool
     */
    public function isEqual(IMatcher $matcher)
    {
        return parent::isEqual($matcher) || IpUtils::checkIp( $matcher->getValue(), $this->getValue());
    }
}
