<?php

namespace FeatureModule\Matchers;

final class Range implements IMatcher
{
    /**
     * @var string
     */
    private $min;

    /**
     * @var string
     */
    private $max;

    private function __construct() {}

    /**
     * @param string $range
     * @return Range
     */
    public static function fromString($range)
    {
        $self = new self();
        list($min, $max) = explode('..', $range, 2);
        $self->min = (int) $min;
        $self->max = (int) $max;
        return $self;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return "{$this->min}..{$this->max}";
    }

    /**
     * @param IMatcher $matcher
     * @return bool
     */
    public function isEqual(IMatcher $matcher)
    {
        $value = (int) $matcher->getValue();
        return $value >= $this->min && $value <= $this->max;
    }

}
