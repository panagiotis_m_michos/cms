<?php

namespace FeatureModule\Matchers;

interface IMatcher
{
    /**
     * @return mixed
     */
    public function getValue();

    /**
     * @param IMatcher $matcher
     * @return bool
     */
    public function isEqual(IMatcher $matcher);

}