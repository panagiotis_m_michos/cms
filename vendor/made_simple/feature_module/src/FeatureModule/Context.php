<?php

namespace FeatureModule;

use FeatureModule\Matchers\IMatcher;

class Context
{
    const TYPE_IP = 'ip';
    const TYPE_ENABLED = 'enabled';
    const TYPE_COOKIE = 'cookie';
    const TYPE_QUERY_PARAM = 'query_param';

    public static $types = [
        self::TYPE_IP,
        self::TYPE_ENABLED,
        self::TYPE_COOKIE,
        self::TYPE_QUERY_PARAM,
    ];

    /**
     * @var array
     */
    private $contexts = [];

    /**
     * @param string $contextType
     * @param IMatcher $matcher
     */
    public function __construct($contextType = NULL, IMatcher $matcher = NULL)
    {
        if ($contextType && $matcher) {
            $this->set($contextType, $matcher);
        }
    }

    /**
     * @param string $contextType
     * @param IMatcher $matcher
     */
    public function set($contextType, IMatcher $matcher)
    {
        $this->contexts[$contextType] = $matcher;
    }

    /**
     * @param string $contextType
     * @return NULL|IMatcher
     */
    public function get($contextType)
    {
        return isset($this->contexts[$contextType]) ? $this->contexts[$contextType] : NULL;
    }
}
