#CMS Ubiquitous Language Reference 


- [Customer](#customer)
- [Business bank account and Cash Back](#cashback)
- [Emails](#emails)
- [Products](#products)
  - [Product and Services titles](#productslist)

----

## Glossary

- **Condition**: After the fact. Check the current state for something.
- **Event**: During the fact. Happens when the event being tracked happens as well. Can only be used as the **When** step of a scenario.
- **Execution**: Executes the action. 


## Customer

- `[Condition]` **I am an existing [retail/wholesale] customer**: Will list all customers that match the query
- `[Condition]` **I am from [Country Name]**: Will list all customers that are from the [specified country ($countriesById)](https://bitbucket.org/made_simple/cms/raw/master/project/models/ValueObject/Country.php)
- `[Condition]` **[1 day, 2 days, ...] passed since the company was incorporated*:


## Business bank account and Cash Back

  - `[Condition]` **I am a bank lead**: Opt in for a business bank account during the incorporation process
  - `[Event]` **I become a bank lead**: This step is related to the event of a customer applying for the bank account and not the condition of being a bank lead.
  - `[Condition]` **I am not a business bank lead**: opt out for a business bank account during the incorporation process
  - `[Condition]` **business bank lead has been sent to the bank**: send to the bank once the company is formed
  - `[Condition]` **the company cash back status is [ELIGIBLE/BOUNCED/PAID/PENDING/EXPIRED]**: match cash back status
  - `[Condition]` **the business bank account has been opened for the company**: we imported the company bank account confirmation from the bank
  - `[Condition]` **I [have/have not] filled in the Cash Back payment preferences**: 
    - Retail customer: customer filled the **individual cashback** payment preferences
    - Wholesale customer: customer filled **his account** cashback payment preferences
  - `[Condition]` **the company cash back payment response is [PAID/BOUNCED]**: When Finance Team's payment CSV is imported back into our system with the list of cashback IDs and the respective status

## Emails

- `[Condition]` **[1 day, 2 days, ...] passed since email 'X' was sent to me**
- `[Condition]` **the email 'X' was not sent to me**: True if the email has never been sent before.
- `[Execute]` **send email 'X' to me**


## Products

- `[Condition Single Product]` **I bought the [PRODUCT/SERVICE TITLE]**
- `[Condition Group of Products]` **I bought [PRODUCT/SERVICE GROUP]**
- `[Condition Single Product]` **I have not bought the [PRODUCT/SERVICE TITLE]**
- `[Condition Group of Products]` **I have not bought [PRODUCT/SERVICE GROUP]**
- `[Condition]` **I have only one order**: Customer made only one payment - one order (used to identify new customers)
- `[Execute Single Product]` **I buy the [PRODUCT/SERVICE TITLE]**
- `[Execute Group Products]` **I buy [PRODUCT/SERVICE TITLE]**


##### Product and Services titles

NOTE: Do not include the '(id: xxx)' in the product name


- LLP Package  (id: 436)
- Limited by Guarantee Package (id: 1175)
- Limited by Guarantee Plus Package (id: 379)
- Any Core Company Package Initial
    - Basic Package Initial (id: 1313)
    - Printed Package Initial (id: 1314)
    - Privacy Package Initial (id:1315)
    - Comprehensive Package Initial (id:1316)
    - Ultimate Package Initial (id:1317)
- Any Core Company Package Renewal
    - Privacy Package Renewal  (id:1353)
    - Comprehensive Package Renewal  (id: 342)
    - Ultimate Package Renewal  (id: 342)
- Reserve a Company Name Initial (id: 400)
- Reserve a Company Name Renewal (id: 401)
- Reserve a Company Name Transfer (id: 403)
- Sole Trader Package (id: 921)
- Sole Trader Plus Package (id: 955)
- Any International Company Package Initial
  - International Classic Package Initial (id: 1430)
  - International Banking Package Initial (id: 1598)
- International Package Renewal (id: 1493)
- Shelf Company Package (id: 1302, 1428, 1605)
- Offshore Formation Service (randomize between the following)
    - Anguilla Offshore Formation Service Initial (id: 405)
    - Anguilla Offshore Formation With Bank Assistance Service (id: 409)
    - Anguilla Offshore Formation Service Renewal (id: 732)
    - Belize Offshore Formation Service Initial (id: 406)
    - Belize Offshore Formation With Bank Assistance Service (id: 410)
    - Belize Offshore Formation Service Renewal (id: 733)
    - Seychelles Offshore Formation Service Initial (id: 407)
    - Seychelles Offshore Formation With Bank Assistance Service (id: 411)
    - Seychelles Offshore Formation Service Renewal (id: 734)
- Offshore Bank Assistance (id: 413)
- Annual Return Express Service (id: 1609)
- Annual Return Service (id: 340)
- Annual Return DIY Product (id: 666)
- Nominee Shareholder Service Initial (id: 172)
- Nominee Secretary Service Initial (id: 171)
- Nominee Shareholder Service Renewal (id: 339)
- Nominee Secretary Service Renewal (id: 337)
- Registered Office Service Initial (id: 165)
- Registered Office Service Renewal (id: 334)
- Service Address Service Initial (id: 475) 
- Service Address Service Renewal (id: 806)
- Apostilled Documents Normal Product (id:115)
- Apostilled Documents Express Product (id: 287)
- Certificate of Good Standing Normal Product (id: 292)
- Certificate of Good Standing Express Product (id: 293)
- Certificate of Good Standing Apostille Normal Product (id: 1654)
- Certificate of Good Standing Apostille Express Product (id: 1655)
- Additional Bound Memorandum and Articles of Association Product (id: 317)
- Duplicate Certificate Of Incorporation (id: 318)
- Company Formation Documents (id: 319)
- Company Name Change Service (id: 843)
- Company Name Change Same Day Service (id: 844)
- Company Dissolution Service (id: 309)
- VAT Registration Assistance (id: 299)
- PAYE Registration Assistance (id: 298)
- Dormant Company Accounts Service (id: 306)
- Dormant Company Accounts Express Service (id: 882)
- Transfer of Shares Service (id: 315)
- Issue of Shares Service (id: 313)
- Share Certificate Service (id: 314)
- Confirmation of Shareholding at Incorporation (id: 311)
- Company Register (id: 301)
- Company Seal (id: 302)
- Company Stamp (id: 331)
- All-In-One Pack (id: 567)
- Credit Product (id: 609)
- Any Mail Forwarding Service
    - Mail Forwarding Address (N1) - 3 Month Service (id: 443)
    - Mail Forwarding (id: 1270)
    - Mail Forwarding & Telephone Answering (id: 1271)
    - Mail Forwarding - 3 Months (id: 1625)
