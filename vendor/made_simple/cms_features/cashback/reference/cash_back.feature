Feature: Automatic Payment of Cash Backs
  When we have eligible cashbacks
  I want to export the list of eligible cashbacks
  So that we can pay all of them at once

  Scenario: Retail Customer - Cash Back status PENDING
      Given I am an existing retail customer
        And I bought the Core Company Package Initial
        And the business bank lead has been sent to the bank
        And I have not filled in the Cash Back payment preference
       When the business bank lead has been imported
       Then the Cash Back status is PENDING

  Scenario: Wholesale Customer - Cash Back status PENDING by lack of Cash Back prefs
      Given I am an existing wholesale customer
        And I bought the Core Company Package Initial
        And the business bank lead has been sent to the bank
        And I have not filled in the Cash Back payment preference
       When the business bank lead has been imported
       Then the Cash Back status is PENDING

  Scenario: Wholesale Customer - Cash Back status PENDING by lack of Proof of ID
      Given I am an existing wholesale customer
        And I bought the Core Company Package Initial
        And the business bank lead has been sent to the bank
        And my customer Proof of ID status is REQUIRED
       When the business bank lead has been imported
       Then the Cash Back status is PENDING

  Scenario: Retail Customer - Cash Back status ELIGIBLE (Claim)
      Given I am an existing retail customer
        And I bought the Core Company Package Initial
        And the business bank account has been opened for the company
        And less than or equal 30 days passed since the business bank lead has been imported
       When I fill in the Cash Back payment preference
       Then the Cash Back status is ELIGIBLE

  Scenario: Wholesale Customer - Cash Back status ELIGIBLE
      Given I am an existing wholesale customer
        And I bought the Core Company Package Initial
        And my customer Proof of ID status is VALIDATED
        And I have filled in the Cash Back payment preference
       When the business bank lead has been imported
       Then the Cash Back status is ELIGIBLE

  Scenario: Retail Customer - Cash Back status EXPIRED
      Given I am an existing retail customer
        And I bought the Core Company Package Initial
        And the business bank account has been opened for the company
        And the Cash Back status is PENDING
       When more than 30 days passed since the business bank lead has been imported
       Then the Cash Back status is EXPIRED

  Scenario: Wholesale Customer - Cash Back status EXPIRED - Payment Preferences and ID Check
      Given I am an existing wholesale customer
        And I bought the Core Company Package Initial
        And the business bank account has been opened for the company
        And the Cash Back status is PENDING
       When more than 30 days passed since the business bank lead has been imported
       Then the Cash Back status is EXPIRED

  Scenario: Cash Back status PAID
       When the company cash back payment confirmation is PAID
       Then the Cash Back status is PAID

  Scenario: Cash Back status BOUNCED
       When the company cash back payment confirmation is BOUNCED
       Then the Cash Back status is BOUNCED

  #Make sure email is sent only once even if multiple cashbacks are paid
  #Make sure email will be sent to customers who are paid in credits
  Scenario: Cash Back status PAID - Email
      Given 1 or more days passed since email 1461 was sent to me
       When the Cash Back status is PAID
        And send email 1461 to me

  Scenario: Cash Back status BOUNCED - Email
      Given 1 or more days passed since email 1617 was sent to me
       When the Cash Back status is BOUNCED
        And send email 1617 to me

  Scenario: Email wholesale - Proof of ID Not Validated - Purchase - 1st email
      Given I am an existing wholesale customer
        And I bought the Core Company Package Initial
        And my customer Proof of ID status is REQUIRED
        And the email 'Missing Proof of ID - Wholesale' was not sent to me
       When I become a bank lead
       Then send email 'Missing Proof of ID - Wholesale' to me

  Scenario: Email wholesale - Proof of ID Not Validated - Purchase - 2nd and final email
      Given I am a bank lead
        And my customer Proof of ID status is REQUIRED
       When 7 days passed since email 'Missing Proof of ID - Wholesale' was sent to me
       Then send email 'Missing Proof of ID - Wholesale' to me

  #added the 1 or more days limit to avoid the customer from getting one email per company imported
  Scenario: Email wholesale - Proof of ID Not Validated - Leads Import
      Given I am an existing wholesale customer
        And I bought the Core Company Package Initial
        And my customer Proof of ID status is REQUIRED
        And 7 or more days passed since email 'Missing Proof of ID - Wholesale' was sent to me
       When the business bank lead has been imported
       Then send email 'Missing Proof of ID - Wholesale' to me

  Scenario: Email wholesale - Payment Preferences Not Provided - Leads Import
      Given I am an existing wholesale customer
        And I bought the Core Company Package Initial
        And I have not filled in the Cash Back payment preference
        And 7 or more days passed since email 'Missing Payment Preferences - Wholesale' was sent to me
       When the business bank lead has been imported
       Then send email 'Missing Payment Preferences - Wholesale' to me

  # For dev reference only
  @disabled
  Scenario: Automatically pay credit on account - Payment Preferences Saved 
      Given I am an existing retail/wholesale customer
        And I bought the Core Company Package Initial
        And the business bank account has been opened for the company
        And the Cash Back status is PENDING OR BOUNCED
       When I fill in the Cash Back payment preference
        And the Cash Back payment preference is credit on account
       Then the cash back amount will be added to my credits
        And the Cash Back status is PAID

  # For dev reference only
  @disabled
  Scenario: Automatically pay credit on account - Wholesale - Leads Import
      Given I am an existing wholesale customer
        And I bought the Core Company Package Initial
        And the business bank account has been opened for the company
        And the Cash Back status is PENDING
       When the business bank lead has been imported
        And the Cash Back payment preference is credit on account
       Then the cash back amount will be added to my credits
        And the Cash Back status is PAID