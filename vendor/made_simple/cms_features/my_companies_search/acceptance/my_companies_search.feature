Feature: Company Search
    As a CMS customer
    I want to be able to find a company by the company name

    Background:
        Given there are following companies
            | CompanyNumber  | CompanyName    | AccountsDue | ReturnsDue |
            | 123456         | Test1 Corp LTD | 10/10/2010  | 10/11/2010 |
            | 999988         | Test2 Ltd      | 20/10/2010  | 20/11/2010 |

    Scenario: Company Name Search
        When I search for "test1"
        Then I should see "Test1 Corp LTD"
        And I should not see "Test2 Ltd"

    Scenario: Company Number Search
        When I search for "45"
        Then I should see "Test1 Corp LTD"
        And I should not see "Test2 Ltd"

    Scenario: Company Name and Number Search
        When I search for "2"
        Then I should see "Test1 Corp LTD"
        And I should see "Test2 Ltd"

    Scenario: Non existing company search
        When I search for "test3"
        Then I should not see any results

    Scenario: Empty search (all companies)
        When I search for ""
        Then I should see "Test1 Corp LTD"
        And I should see "Test2 Ltd"