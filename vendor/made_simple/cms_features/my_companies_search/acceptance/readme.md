#Steps from "MyCompaniesSearchDomainContext" usable in "my_companies_search.feature"

```
mycompanies_search_domain_features | Given there are following companies
                                   | at `MyCompaniesSearchDomainContext::thereAreFollowingCompanies()`

mycompanies_search_domain_features | When I search for :companySearchQuery
                                   | at `MyCompaniesSearchDomainContext::iSearchFor()`

mycompanies_search_domain_features | Then I should see :company
                                   | at `MyCompaniesSearchDomainContext::iShouldSee()`

mycompanies_search_domain_features | Then I should not see :company
                                   | at `MyCompaniesSearchDomainContext::iShouldNotSee()`

mycompanies_search_domain_features | Then I should not see any results
                                   | at `MyCompaniesSearchDomainContext::iShouldNotSeeAnyResults()`
```
