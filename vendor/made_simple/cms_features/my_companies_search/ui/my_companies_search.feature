Feature: Company Search Filters
    As a CMS customer
    I want to be able to find a company by the company name

  Background:
        Given there are following companies
            | CompanyNumber  | CompanyName    | AccountsDue | ReturnsDue |
            | 123456         | Test1 Corp LTD | 10/10/2010  | 10/11/2010 |
            | 999988         | Test2 Ltd      | 20/10/2010  | 20/11/2010 |
        And I log in
        And I go to "/my-companies"

    Scenario: Company Name Search
      When I search for "test1"
      Then I should see "Test1 Corp LTD"
      And I should not see "Test2 Ltd"

    Scenario: Accounts Due search
        Given I fill in "From:" with "09/10/2010"
        And I fill in "To:" with "11/10/2010"
        And I select "Accounts Due" from "Search by date:"
        When I press "search"
        Then I should see "Test1 Corp LTD"
        And I should not see "Test2 Ltd"

    Scenario: Returns Due search
        Given I fill in "From:" with "19/11/2010"
        And I fill in "To:" with "21/11/2010"
        And I select "Returns Due" from "Search by date:"
        When I press "search"
        Then I should see "Test2 Ltd"
        And I should not see "Test1 Corp LTD"

    Scenario: Accounts Due All search
        Given I fill in "From:" with "09/10/2010"
        And I fill in "To:" with "21/10/2010"
        And I select "Accounts Due" from "Search by date:"
        When I press "search"
        Then I should see "Test1 Corp LTD"
        And I should see "Test2 Ltd"

    Scenario: Accounts Due None search
        Given I fill in "From:" with "01/10/2010"
        And I fill in "To:" with "05/10/2010"
        And I select "Accounts Due" from "Search by date:"
        When I press "search"
        Then I should not see "Test1 Corp LTD"
        And I should not see "Test2 Ltd"
        And I should see "No incorporated companies found"