#Steps from "MyCompaniesSearchWebContext" usable in "my_companies_search.feature"

```
mycompanies_search_web_features | Given there are following companies
                                | at `MyCompaniesSearchWebContext::thereAreFollowingCompanies()`

mycompanies_search_web_features | When I search for :searchTerm
                                | at `MyCompaniesSearchWebContext::iSearchFor()`

mycompanies_search_web_features | Given I am :customerEmail
                                | at `MyCompaniesSearchWebContext::iAm()`

mycompanies_search_web_features | Given I have company :companyNumber
                                | at `MyCompaniesSearchWebContext::iHaveCompany()`

mycompanies_search_web_features | Given I am :customerEmail with credit :credit
                                | at `MyCompaniesSearchWebContext::iAmWithCredit()`

mycompanies_search_web_features | Given I log in
                                | at `MyCompaniesSearchWebContext::iLogIn()`

mycompanies_search_web_features | Given I have a token
                                | at `MyCompaniesSearchWebContext::iHaveAToken()`

mycompanies_search_web_features | Given I wait for :time
                                | at `MyCompaniesSearchWebContext::wait()`

mycompanies_search_web_features | Given I am logged in customer
                                | at `MyCompaniesSearchWebContext::iAmLoggedInCustomer()`

mycompanies_search_web_features | Given I click on an element with a title :title
                                | at `MyCompaniesSearchWebContext::iClickOnTitle()`

mycompanies_search_web_features | Given Product :productId has :key :value
                                | at `MyCompaniesSearchWebContext::modifyProduct()`

mycompanies_search_web_features | Given save screenshot
                                | at `MyCompaniesSearchWebContext::saveScreenContents()`

mycompanies_search_web_features | Given /^(?:|I )am on (?:|the )homepage$/
                                | Opens homepage
                                | Example: Given I am on "/"
                                | Example: When I go to "/"
                                | Example: And I go to "/"
                                | at `MyCompaniesSearchWebContext::iAmOnHomepage()`

mycompanies_search_web_features | When /^(?:|I )go to (?:|the )homepage$/
                                | Opens homepage
                                | Example: Given I am on "/"
                                | Example: When I go to "/"
                                | Example: And I go to "/"
                                | at `MyCompaniesSearchWebContext::iAmOnHomepage()`

mycompanies_search_web_features | Given /^(?:|I )am on "(?P<page>[^"]+)"$/
                                | Opens specified page
                                | Example: Given I am on "http://batman.com"
                                | Example: And I am on "/articles/isBatmanBruceWayne"
                                | Example: When I go to "/articles/isBatmanBruceWayne"
                                | at `MyCompaniesSearchWebContext::visit()`

mycompanies_search_web_features | When /^(?:|I )go to "(?P<page>[^"]+)"$/
                                | Opens specified page
                                | Example: Given I am on "http://batman.com"
                                | Example: And I am on "/articles/isBatmanBruceWayne"
                                | Example: When I go to "/articles/isBatmanBruceWayne"
                                | at `MyCompaniesSearchWebContext::visit()`

mycompanies_search_web_features | When /^(?:|I )reload the page$/
                                | Reloads current page
                                | Example: When I reload the page
                                | Example: And I reload the page
                                | at `MyCompaniesSearchWebContext::reload()`

mycompanies_search_web_features | When /^(?:|I )move backward one page$/
                                | Moves backward one page in history
                                | Example: When I move backward one page
                                | at `MyCompaniesSearchWebContext::back()`

mycompanies_search_web_features | When /^(?:|I )move forward one page$/
                                | Moves forward one page in history
                                | Example: And I move forward one page
                                | at `MyCompaniesSearchWebContext::forward()`

mycompanies_search_web_features | When /^(?:|I )press "(?P<button>(?:[^"]|\\")*)"$/
                                | Presses button with specified id|name|title|alt|value
                                | Example: When I press "Log In"
                                | Example: And I press "Log In"
                                | at `MyCompaniesSearchWebContext::pressButton()`

mycompanies_search_web_features | When /^(?:|I )follow "(?P<link>(?:[^"]|\\")*)"$/
                                | Clicks link with specified id|title|alt|text
                                | Example: When I follow "Log In"
                                | Example: And I follow "Log In"
                                | at `MyCompaniesSearchWebContext::clickLink()`

mycompanies_search_web_features | When /^(?:|I )fill in "(?P<field>(?:[^"]|\\")*)" with "(?P<value>(?:[^"]|\\")*)"$/
                                | Fills in form field with specified id|name|label|value
                                | Example: When I fill in "username" with: "bwayne"
                                | Example: And I fill in "bwayne" for "username"
                                | at `MyCompaniesSearchWebContext::fillField()`

mycompanies_search_web_features | When /^(?:|I )fill in "(?P<field>(?:[^"]|\\")*)" with:$/
                                | Fills in form field with specified id|name|label|value
                                | Example: When I fill in "username" with: "bwayne"
                                | Example: And I fill in "bwayne" for "username"
                                | at `MyCompaniesSearchWebContext::fillField()`

mycompanies_search_web_features | When /^(?:|I )fill in "(?P<value>(?:[^"]|\\")*)" for "(?P<field>(?:[^"]|\\")*)"$/
                                | Fills in form field with specified id|name|label|value
                                | Example: When I fill in "username" with: "bwayne"
                                | Example: And I fill in "bwayne" for "username"
                                | at `MyCompaniesSearchWebContext::fillField()`

mycompanies_search_web_features | When /^(?:|I )fill in the following:$/
                                | Fills in form fields with provided table
                                | Example: When I fill in the following"
                                |              | username | bruceWayne |
                                |              | password | iLoveBats123 |
                                | Example: And I fill in the following"
                                |              | username | bruceWayne |
                                |              | password | iLoveBats123 |
                                | at `MyCompaniesSearchWebContext::fillFields()`

mycompanies_search_web_features | When /^(?:|I )select "(?P<option>(?:[^"]|\\")*)" from "(?P<select>(?:[^"]|\\")*)"$/
                                | Selects option in select field with specified id|name|label|value
                                | Example: When I select "Bats" from "user_fears"
                                | Example: And I select "Bats" from "user_fears"
                                | at `MyCompaniesSearchWebContext::selectOption()`

mycompanies_search_web_features | When /^(?:|I )additionally select "(?P<option>(?:[^"]|\\")*)" from "(?P<select>(?:[^"]|\\")*)"$/
                                | Selects additional option in select field with specified id|name|label|value
                                | Example: When I additionally select "Deceased" from "parents_alive_status"
                                | Example: And I additionally select "Deceased" from "parents_alive_status"
                                | at `MyCompaniesSearchWebContext::additionallySelectOption()`

mycompanies_search_web_features | When /^(?:|I )check "(?P<option>(?:[^"]|\\")*)"$/
                                | Checks checkbox with specified id|name|label|value
                                | Example: When I check "Pearl Necklace" from "itemsClaimed"
                                | Example: And I check "Pearl Necklace" from "itemsClaimed"
                                | at `MyCompaniesSearchWebContext::checkOption()`

mycompanies_search_web_features | When /^(?:|I )uncheck "(?P<option>(?:[^"]|\\")*)"$/
                                | Unchecks checkbox with specified id|name|label|value
                                | Example: When I uncheck "Broadway Plays" from "hobbies"
                                | Example: And I uncheck "Broadway Plays" from "hobbies"
                                | at `MyCompaniesSearchWebContext::uncheckOption()`

mycompanies_search_web_features | When /^(?:|I )attach the file "(?P<path>[^"]*)" to "(?P<field>(?:[^"]|\\")*)"$/
                                | Attaches file to field with specified id|name|label|value
                                | Example: When I attach "bwayne_profile.png" to "profileImageUpload"
                                | Example: And I attach "bwayne_profile.png" to "profileImageUpload"
                                | at `MyCompaniesSearchWebContext::attachFileToField()`

mycompanies_search_web_features | Then /^(?:|I )should be on "(?P<page>[^"]+)"$/
                                | Checks, that current page PATH is equal to specified
                                | Example: Then I should be on "/"
                                | Example: And I should be on "/bats"
                                | Example: And I should be on "http://google.com"
                                | at `MyCompaniesSearchWebContext::assertPageAddress()`

mycompanies_search_web_features | Then /^(?:|I )should be on (?:|the )homepage$/
                                | Checks, that current page is the homepage
                                | Example: Then I should be on the homepage
                                | Example: And I should be on the homepage
                                | at `MyCompaniesSearchWebContext::assertHomepage()`

mycompanies_search_web_features | Then /^the (?i)url(?-i) should match (?P<pattern>"(?:[^"]|\\")*")$/
                                | Checks, that current page PATH matches regular expression
                                | Example: Then the url should match "superman is dead"
                                | Example: Then the uri should match "log in"
                                | Example: And the url should match "log in"
                                | at `MyCompaniesSearchWebContext::assertUrlRegExp()`

mycompanies_search_web_features | Then /^the response status code should be (?P<code>\d+)$/
                                | Checks, that current page response status is equal to specified
                                | Example: Then the response status code should be 200
                                | Example: And the response status code should be 400
                                | at `MyCompaniesSearchWebContext::assertResponseStatus()`

mycompanies_search_web_features | Then /^the response status code should not be (?P<code>\d+)$/
                                | Checks, that current page response status is not equal to specified
                                | Example: Then the response status code should not be 501
                                | Example: And the response status code should not be 404
                                | at `MyCompaniesSearchWebContext::assertResponseStatusIsNot()`

mycompanies_search_web_features | Then /^(?:|I )should see "(?P<text>(?:[^"]|\\")*)"$/
                                | Checks, that page contains specified text
                                | Example: Then I should see "Who is the Batman?"
                                | Example: And I should see "Who is the Batman?"
                                | at `MyCompaniesSearchWebContext::assertPageContainsText()`

mycompanies_search_web_features | Then /^(?:|I )should not see "(?P<text>(?:[^"]|\\")*)"$/
                                | Checks, that page doesn't contain specified text
                                | Example: Then I should not see "Batman is Bruce Wayne"
                                | Example: And I should not see "Batman is Bruce Wayne"
                                | at `MyCompaniesSearchWebContext::assertPageNotContainsText()`

mycompanies_search_web_features | Then /^(?:|I )should see text matching (?P<pattern>"(?:[^"]|\\")*")$/
                                | Checks, that page contains text matching specified pattern
                                | Example: Then I should see text matching "Batman, the vigilante"
                                | Example: And I should not see "Batman, the vigilante"
                                | at `MyCompaniesSearchWebContext::assertPageMatchesText()`

mycompanies_search_web_features | Then /^(?:|I )should not see text matching (?P<pattern>"(?:[^"]|\\")*")$/
                                | Checks, that page doesn't contain text matching specified pattern
                                | Example: Then I should see text matching "Bruce Wayne, the vigilante"
                                | Example: And I should not see "Bruce Wayne, the vigilante"
                                | at `MyCompaniesSearchWebContext::assertPageNotMatchesText()`

mycompanies_search_web_features | Then /^the response should contain "(?P<text>(?:[^"]|\\")*)"$/
                                | Checks, that HTML response contains specified string
                                | Example: Then the response should contain "Batman is the hero Gotham deserves."
                                | Example: And the response should contain "Batman is the hero Gotham deserves."
                                | at `MyCompaniesSearchWebContext::assertResponseContains()`

mycompanies_search_web_features | Then /^the response should not contain "(?P<text>(?:[^"]|\\")*)"$/
                                | Checks, that HTML response doesn't contain specified string
                                | Example: Then the response should not contain "Bruce Wayne is a billionaire, play-boy, vigilante."
                                | Example: And the response should not contain "Bruce Wayne is a billionaire, play-boy, vigilante."
                                | at `MyCompaniesSearchWebContext::assertResponseNotContains()`

mycompanies_search_web_features | Then /^(?:|I )should see "(?P<text>(?:[^"]|\\")*)" in the "(?P<element>[^"]*)" element$/
                                | Checks, that element with specified CSS contains specified text
                                | Example: Then I should see "Batman" in the "heroes_list" element
                                | Example: And I should see "Batman" in the "heroes_list" element
                                | at `MyCompaniesSearchWebContext::assertElementContainsText()`

mycompanies_search_web_features | Then /^(?:|I )should not see "(?P<text>(?:[^"]|\\")*)" in the "(?P<element>[^"]*)" element$/
                                | Checks, that element with specified CSS doesn't contain specified text
                                | Example: Then I should not see "Bruce Wayne" in the "heroes_alter_egos" element
                                | Example: And I should not see "Bruce Wayne" in the "heroes_alter_egos" element
                                | at `MyCompaniesSearchWebContext::assertElementNotContainsText()`

mycompanies_search_web_features | Then /^the "(?P<element>[^"]*)" element should contain "(?P<value>(?:[^"]|\\")*)"$/
                                | Checks, that element with specified CSS contains specified HTML
                                | Example: Then the "body" element should contain "style=\"color:black;\""
                                | Example: And the "body" element should contain "style=\"color:black;\""
                                | at `MyCompaniesSearchWebContext::assertElementContains()`

mycompanies_search_web_features | Then /^the "(?P<element>[^"]*)" element should not contain "(?P<value>(?:[^"]|\\")*)"$/
                                | Checks, that element with specified CSS doesn't contain specified HTML
                                | Example: Then the "body" element should not contain "style=\"color:black;\""
                                | Example: And the "body" element should not contain "style=\"color:black;\""
                                | at `MyCompaniesSearchWebContext::assertElementNotContains()`

mycompanies_search_web_features | Then /^(?:|I )should see an? "(?P<element>[^"]*)" element$/
                                | Checks, that element with specified CSS exists on page
                                | Example: Then I should see a "body" element
                                | Example: And I should see a "body" element
                                | at `MyCompaniesSearchWebContext::assertElementOnPage()`

mycompanies_search_web_features | Then /^(?:|I )should not see an? "(?P<element>[^"]*)" element$/
                                | Checks, that element with specified CSS doesn't exist on page
                                | Example: Then I should not see a "canvas" element
                                | Example: And I should not see a "canvas" element
                                | at `MyCompaniesSearchWebContext::assertElementNotOnPage()`

mycompanies_search_web_features | Then /^the "(?P<field>(?:[^"]|\\")*)" field should contain "(?P<value>(?:[^"]|\\")*)"$/
                                | Checks, that form field with specified id|name|label|value has specified value
                                | Example: Then the "username" field should contain "bwayne"
                                | Example: And the "username" field should contain "bwayne"
                                | at `MyCompaniesSearchWebContext::assertFieldContains()`

mycompanies_search_web_features | Then /^the "(?P<field>(?:[^"]|\\")*)" field should not contain "(?P<value>(?:[^"]|\\")*)"$/
                                | Checks, that form field with specified id|name|label|value doesn't have specified value
                                | Example: Then the "username" field should not contain "batman"
                                | Example: And the "username" field should not contain "batman"
                                | at `MyCompaniesSearchWebContext::assertFieldNotContains()`

mycompanies_search_web_features | Then /^(?:|I )should see (?P<num>\d+) "(?P<element>[^"]*)" elements?$/
                                | Checks, that (?P<num>\d+) CSS elements exist on the page
                                | Example: Then I should see 5 "div" elements
                                | Example: And I should see 5 "div" elements
                                | at `MyCompaniesSearchWebContext::assertNumElements()`

mycompanies_search_web_features | Then /^the "(?P<checkbox>(?:[^"]|\\")*)" checkbox should be checked$/
                                | Checks, that checkbox with specified in|name|label|value is checked
                                | Example: Then the "remember_me" checkbox should be checked
                                | Example: And the "remember_me" checkbox is checked
                                | at `MyCompaniesSearchWebContext::assertCheckboxChecked()`

mycompanies_search_web_features | Then /^the checkbox "(?P<checkbox>(?:[^"]|\\")*)" (?:is|should be) checked$/
                                | Checks, that checkbox with specified in|name|label|value is checked
                                | Example: Then the "remember_me" checkbox should be checked
                                | Example: And the "remember_me" checkbox is checked
                                | at `MyCompaniesSearchWebContext::assertCheckboxChecked()`

mycompanies_search_web_features | Then /^the "(?P<checkbox>(?:[^"]|\\")*)" checkbox should not be checked$/
                                | Checks, that checkbox with specified in|name|label|value is unchecked
                                | Example: Then the "newsletter" checkbox should be unchecked
                                | Example: Then the "newsletter" checkbox should not be checked
                                | Example: And the "newsletter" checkbox is unchecked
                                | at `MyCompaniesSearchWebContext::assertCheckboxNotChecked()`

mycompanies_search_web_features | Then /^the checkbox "(?P<checkbox>(?:[^"]|\\")*)" should (?:be unchecked|not be checked)$/
                                | Checks, that checkbox with specified in|name|label|value is unchecked
                                | Example: Then the "newsletter" checkbox should be unchecked
                                | Example: Then the "newsletter" checkbox should not be checked
                                | Example: And the "newsletter" checkbox is unchecked
                                | at `MyCompaniesSearchWebContext::assertCheckboxNotChecked()`

mycompanies_search_web_features | Then /^the checkbox "(?P<checkbox>(?:[^"]|\\")*)" is (?:unchecked|not checked)$/
                                | Checks, that checkbox with specified in|name|label|value is unchecked
                                | Example: Then the "newsletter" checkbox should be unchecked
                                | Example: Then the "newsletter" checkbox should not be checked
                                | Example: And the "newsletter" checkbox is unchecked
                                | at `MyCompaniesSearchWebContext::assertCheckboxNotChecked()`

mycompanies_search_web_features | Then /^print current URL$/
                                | Prints current URL to console.
                                | Example: Then print current URL
                                | Example: And print current URL
                                | at `MyCompaniesSearchWebContext::printCurrentUrl()`

mycompanies_search_web_features | Then /^print last response$/
                                | Prints last response to console
                                | Example: Then print current response
                                | Example: And print current response
                                | at `MyCompaniesSearchWebContext::printLastResponse()`

mycompanies_search_web_features | Then /^show last response$/
                                | Opens last response content in browser
                                | Example: Then show last response
                                | Example: And show last response
                                | at `MyCompaniesSearchWebContext::showLastResponse()`
```
