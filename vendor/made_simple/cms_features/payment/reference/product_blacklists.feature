# This is not a true BDD file. It is designed to be a human readable and human testable aid.

Feature: Product Blacklists
    Products should not be added to the basket if they are in the blacklist of another product which is already in the basket.
    A message should display to tell the customer what has happened.

  Background:     #The Background rules are assumed for all scenarios below
    Given the Privacy Package has Limited by Guarantee set as an equivalent blacklist product
    And my basket is empty

# NB. Equivalent scenarios do not work due to a legacy override for packages (if you add a package to the basket it will clear the basket). 
# As the behaviour is effectively what we want (sans the flash message) we are leaving the existing behaviour as is. Contained scenarios should work.
  Scenario: Buy equivalent blacklisted product
    Given I add Privacy Package to my basket
    When I add Limited by Guarantee to my basket
    Then my basket will only have Privacy Package
    And I will see the equivalent blacklist flash message
    
  Scenario: Buy equivalent blacklisted product (reverse)
    Given I add Limited by Guarantee to my basket
    When I add Privacy Package to my basket
    Then my basket will only have Limited by Guarantee
    And I will see the equivalent blacklist flash message # Does not work due to Legacy Override

  Scenario: Buy equivalent blacklisted product (both products setup to blacklist each other - blacklist should still work)
    Given the Limited by Guarantee has Privacy Package set as an equivalent blacklist product
    When I add Privacy Package to my basket
    And I add Limited by Guarantee to my basket
    Then my basket will only have Privacy Package
    And I will see the equivalent blacklist flash message

  Background:     #Only use these background rules for the scenarios below (ignore the background rules above)
    Given the Privacy Package has Registered Office set as a contained blacklist product
    And my basket is empty

  Scenario: Buy contained blacklisted product
    Given I add Privacy Package to my basket
    When I add Registered Office to my basket
    Then my basket will only have Privacy Package
    And I will see the contained blacklist flash message

  Scenario: Buy contained blacklisted product (reverse)
    Given I add Registered Office to my basket
    When I add Privacy Package to my basket
    Then my basket will only have Privacy Package #remove Registered Office and add Privacy
    And I will see the contained blacklist flash message