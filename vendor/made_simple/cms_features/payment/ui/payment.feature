@javascript
Feature: Payment

  Payment page and payment form tests

  Scenario: Payment using available credit
    Given I am "tomasj@madesimplegroup.com" with credit "100"
    And I log in
    And I am on payment page with products "443"
    When I check "Use credit to make this payment"
    And I press "Pay with Credits"
    Then I should be on "/purchase-confirmation.html#auth-form-block"

  Scenario: Payment using insufficient credit
    Given I am "tomasj@madesimplegroup.com" with credit "6"
    And I log in
    And I am on payment page with products "443"
    When I check "Use credit to make this payment"
    And I complete sage form with "card"
    Then I should be on "/purchase-confirmation.html"

  Scenario: Payment using 100% percent discount code
    Given I am "tomasj@madesimplegroup.com"
    And I log in
    And I add product "443" to the basket
    # feature
    And Feature 'payment_upgrade' is enabled
    And I select my company
    And I apply existing voucher "testing" with 100 percent discount
    And I press "Checkout"
    And I press "payment-submit-button"
    Then I should be on "/purchase-confirmation.html"

  # how many different baskets do we have ?
  Scenario: Payment using 100% percent discount code not logged in
    Given I search for new company name "Non existing company 234"
    And I press "Buy Now"
    And I use existing voucher "testing" with 100 percent discount
    And I am on "/payment/"
    When I use email "tomasj@madesimplegroup.com"
    And I press "payment-submit-button"
    Then I should be on "/purchase-confirmation.html"

  Scenario: Sage payment for not registered customer
    Given I am on payment page with package "1313" and companyName "test me forever and ever"
    When I use email "tomasj@madesimplegroup.com"
    And I complete sage form with "address"
    Then I should be on "/purchase-confirmation.html"


#  Scenario: Paypal payment for registered customer
#    Given I am "tomasj@madesimplegroup.com" with credit "6"
#    And I log in
#    Given I am on payment page with package "1313" and companyName "test me forever and ever"
#    When I use email "tomasj@madesimplegroup.com"
#    And I complete paypal payment
#    Then I should be on "/purchase-confirmation.html"
#
#  Scenario: Paypal payment for not registered customer
#    Given I am on payment page with package "1313" and companyName "test me forever and ever"
#    When I use email "tomasj@madesimplegroup.com"
#    And I complete paypal payment
#    Then I should be on "/purchase-confirmation.html"
