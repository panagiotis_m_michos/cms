@javascript
Feature: Payment method
  Ability to add card to the system as a token
  Background:
    Given I am "tomasj@madesimplegroup.com"
    And I log in
  Scenario: Add card with 3d authentication
    Given I am on "/wallet"
    And I follow "Add New Card"
    When I complete payment method form with card "0006"
    And I complete 3d authentication form
    Then I should see "Your card details have been updated."
    And I should see "Visa ending in 0006"
    And Token "0006" can be used for future payments

  Scenario: Add card without 3d authentication
    Given I am on "/wallet"
    And I follow "Add New Card"
    When I complete payment method form with card "0043"
    Then I should see "Your card details have been updated."
    And I should see "Mastercard ending in 0043"
    And Token "0043" can be used for future payments

  Scenario: Delete card from the system
    Given I have a token with card "0043"
    And I am on "/wallet"
    And I follow "Show card details"
    When I press "Delete"
    And I press "Delete card"
    Then I should see "Your card details have been deleted."
    And I should see "Add new card"
    And Token "0043" can not be used for future payments
