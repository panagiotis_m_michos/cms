@javascript
Feature: Payment confirmation auto-renewal

  Background:
    Given Feature 'payment_upgrade' is enabled

  Scenario: Payment with credit
    Given I am logged in customer
    When I buy "165" with credit
    Then I should be on "/purchase-confirmation.html#auth-form-block"
    And I should not see auto-renewal checkbox

  Scenario: Payment with new card without previous saved payment method
    Given I am logged in customer
    And I don't have any payment method saved
    When I buy "165" with new card
    Then I should be on "/purchase-confirmation.html"
    And I should see checked auto-renewal checkbox

  Scenario: Payment with new card with previous saved payment method
    Given I am logged in customer
    And I have a token
    When I buy "165" with one-off card
    Then I should be on "/purchase-confirmation.html"
    And I should see unchecked auto-renewal checkbox
