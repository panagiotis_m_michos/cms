Feature: Manual Service Renew
  When my existing service is about to expire
  I want to renew it
  So that I will keep the service continuity


  This feature file is for services that will be manually renewed using the services on http://www.companiesmadesimple.com/renewal-fees.html
  For the auto-renew feature refer to: https://bitbucket.org/made_simple/cms-features/src/master/service-auto-renew.feature
  The previous version of this feature file can be found in: https://trello.com/c/J1P19F42

  References:
  -----------
  Service views: http://p1zuul.axshare.com/#p=service_life-time&c=1
  Order summary warnings at: https://docs.google.com/a/madesimplegroup.com/spreadsheets/d/1RsMFzLQQhYOm-1MbHA0y78RbBLZDmX5vpiFtPTl76pg/edit?usp=sharing


  Business logic variables:
  -------------------------
    Current state:                                              (what the customer is trying to do now?)
      renew operation:        upgrade, downgrade, same, new     (the comparison is done between the new service and the last service on the queue (can be on-hold or active))
      existing service state: on-time, overdue, expired         (if the company have a service now, what's the state?)
      company status:         incorporated, not incorporated    (what's the company status?)
      new service start:      now, on-hold                      (what the customer choose to do with the new service? (only when upgrading))

    Future state:
      operation policy: allow, deny        (is the operation allowed?)
      warning message:  none, message      (if not, what do we tell the customer?)
      email reminder:   send, don't send   (???)
      service queue:                       (what do we do with the services that are on the queue?)


  Due date and Period definitions:
  --------------------------------
    Let due date be: 01/01/2000
    if:
      today == date - 1 day (31/12/1999): on-time
      today == date         (01/01/2000): on-time
      today == date + 1 day (02/01/2000): overdue
      today == date + 28 days           : expired

      service is active from 01/01/1999 to 01/01/2000
      any on-hold service will have start date set to 02/01/2000
      service is overdue from 02/01/2000 to 29/01/2000
      service is expired on 30/01/2000
      if service is renewed when overdue, the new service will have start date set to 02/01/2000

      1 period  = 1 year of active service



  Services list:
  --------------
    | service name                 | service occurrence |
    |                              | initial  | renewal |
    | privacy package              | 1315     | 1353    |
    | comprehensive package        | 1316     | 342     |
    | ultimate package             | 1317     | 342     |
    | service address              | 475      | 806     |
    | registered office            | 165      | 334     |

  Services hierarchy definition:
  -----------------------------
    ultimate/comprehensive package > privacy package > service/registered address

    From left to right: DOWNGRADE
    From right to left: UPGRADE

  Wireframe messages:
  -------------------
    message_1: http://cnrefj.axshare.com/service_selector.html#scenario=renew operation: upgrade, existing service state: on-time, company satus: incorporated, new service start:on-hold
    message_2: The active service will be interrupted. The service you are purchasing will start today. -> if customer has an on-hold service we should inform him that the service will come after the one he is purchasing now?
    message_3: block, warning that company is not formed, try again after the company is formed

  View URLs:
  ---------
    "Services" on "admin UI"       : https://www.companiesmadesimple.com/admin/en/1427/companyServicesList/?company_id=461343
    "My Services" on "customer UI" : https://www.companiesmadesimple.com/page1364en.html



  Background:
    Given the following companies exist:
      | username  | company name | service name          | service occurrence |
      | test1     | A            | privacy package       | initial            |
      | test1     | A            | privacy package       | renewal            |
      | test1     | B            | comprehensive package | initial            |
      | test1     | C            | service address       | initial            |


  Scenario: renew operation: upgrade, existing service state: on-time, company satus: incorporated, new service start:on-hold

    - Default upgrade scenario.
    - Refer to the 'Services hierarchy definition' for what should be considered an upgrade.
    - The default start period for the new service is on-hold.
    - After a service is upgraded no emails should be sent in reference to the old service.
    - If there is an on-hold service, the currently active service can't be auto-renewed nor manually renewed.

    Given that I add to basket "renewal" of "comprehensive package"
      And company "A" service due date is "on-time"
     When I try to apply "renewal" of "comprehensive package" to company "A" on the Order Summary page
     Then I see message_1
     When I checkout
     Then company "A" services are:
      | service name           | service status | service occurrence |
      | privacy package        | active         | initial            |
      | privacy package        | on-hold        | renewal            |
      | comprehensive package  | on-hold        | renewal            |
      And on the "Services" page for company "A" on the "admin UI" I will see:
      | service name           | service status |
      | privacy package        | active         |
      | privacy package        | on-hold        |
      | comprehensive package  | on-hold        |
      And on the "My Services" page for company "A" on the "customer UI" I will see:
      | service name           | service status | period    |
      | privacy package        | active         | 2 periods |
      | comprehensive package  | on-hold        | 1 period  |
      And on the "My Services" page for company "A" on the "customer UI" the renewal checkbox for the service "privacy package" will be disabled
      And on the "My Services" page for company "A" on the "customer UI" the auto-renewal option for the service "privacy package" will be disabled




  Scenario: renew operation: upgrade, existing service state: on-time, company status: incorporated, new service start: now

    - The status of an interrupted service is 'expired'.

    Given that I add to basket "renewal" of "comprehensive package"
      And company "A" service due date is "on-time"
     When I try to apply "renewal" of "comprehensive package" to company "A" on the Order Summary page
      And I set the service to start now
     Then I see message_2
     When I checkout
     Then company "A" services are:
      | service name           | service status | service occurrence |
      | privacy package        | expired        | initial            |
      | privacy package        | on-hold        | renewal            |
      | comprehensive package  | active         | renewal            |
      And on the "Services" page for company "A" on the "admin UI" I will see:
      | service name           | service status |
      | privacy package        | expired        |
      | privacy package        | on-hold        |
      | comprehensive package  | active         |
      And on the "My Services" page for company "A" on the "customer UI" I will see:
      | service name           | service status | period   |
      | comprehensive package  | active         | 1 period |
      | privacy package        | on-hold        | 1 period |




  Scenario: renew operation: *, existing service state: *, company status: not incorporated, new service start: *

    - Don't start the service unless the company can benefit from it

    Given that I add to basket "renewal" of "comprehensive package"
      And company "B" service status is "not incorporated"
     When I try to apply "renewal" of "comprehensive package" to company "B" on the Order Summary page
     Then I see message_3



  Scenario: renew operation: upgrade, existing service state: overdue, company status: incorporated, new service start: *

    Given that I add to basket "renewal" of "comprehensive package"
      And company "C" service due date is "overdue"
      And I try to apply "renewal" of "comprehensive package" to company "C" on the Order Summary page
      And I set the service to start now
     When I checkout
     Then company "C" services are:
      | service name           | service status | service occurrence |
      | service address        | expired        | initial            |
      | comprehensive package  | active         | renewal            |
      And on the "Services" page for company "C" on the "admin UI" I will see:
      | service name           | service status |
      | service address        | expired        |
      | comprehensive package  | active         |
      And on the "My Services" page for company "C" on the "customer UI" I will see:
      | service name           | service status | period   |
      | comprehensive package  | active         | 1 period |
  # (renew operation: upgrade, existing service state: on-time, company status: incorporated, new service start: now) no warning
  # https://trello.com/c/htWcd75C -> this must happen
  #
  # Scenario: renew operation: new, existing service state: expired, company status: incorporated, new service start: *
  # start new one; split into a UI scenario: on my service always show only the active service, if there is no active services but there is an expired service, show the expired; if there are no services at all (active/expired) don't show the company on the list
  # check scenario Package expired renewal - Warning on old feat file
  #
  #
  # Scenario: renew operation: downgrade, existing service state: on-time, company status: incorporated, new service start: now
  # not possible because there is no choice to start the downgrade now - how to communicate this? (move to story body)
  #
  # Scenario: renew operation: downgrade, existing service state: on-time, company status: incorporated, new service start: on-hold
  # (default behaviour) add new service to the end of the queue, don't send emails about old service; msg: you are DOWNGRADING, this is what you are about to lose (show table with differences - last on-hold vs new) (list of existing services), the new service will start when the last on-hold ends (reassure: you won't lose your ontime stuff).
  #
  # Scenario: renew operation: downgrade, existing service state: overdue, company status: incorporated, service queue: none, new service start: on-hold
  # same as (renew operation: downgrade, existing service state: on-time, company status: incorporated, new service start: on-hold) without the (reassure: you won't lose your ontime stuff)
  #
  #
  # Scenario: renew operation: same, existing service state: on-time, services type: product->product, company status: incorporated, new service start: now
  # not possible because there is no choice to start the same now - how to communicate this? (move to story body)
  #
  # Scenario: renew operation: same, existing service state: *, company status: incorporated, new service start: on-hold
  # (default behaviour)add new service to the end of the queue, don't send emails about old service;
  #
