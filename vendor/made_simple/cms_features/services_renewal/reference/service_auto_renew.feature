Feature: Auto-Renew
  When I purchase a renewable service
  I want it to be renewed automatically
  So I don't have to manually renew it


  Scenario: Set due service to auto-renew
    Given that I have a company with a service
      And I have a payment method associated with my account
      And the service is not overdue
     When I set the 'auto-renew' flag on the 'My Services' page (http://ibo7mj.axshare.com/#p=my_services&c=1)
     Then I want the service to be renewed automatically while the flag is set
      And I won't receive any renewal email reminder from manual renewals (https://trello.com/c/VJdGdL41)

  Scenario: Set overdue service to auto-renew
    Given that I'm manually renewing an overdue service
     When I finish the payment using a bank card (sagepay)
     Then the service will have the auto-renew set to ON

  Scenario: Successful auto-renewal
    Given that I have a company with a service
      And the service have auto-renew=ON
     When the due date arrives
      And we successfully charge the token with the renewal yearly amount
     Then the service will have it's due date extended by one year
      And I will receive the email reminder that the service was renewed (http://ibo7mj.axshare.com/#p=email_reminder__renew_successfull__d_&c=1)

  Scenario: Failed auto-renewal
    Given that I have a company with a service
      And the service have auto-renew=ON
     When the due date arrives
      And we fail to charge the token with the renewal yearly amount
     Then the service will have the same due date
      And the auto-renew option will be set to OFF
      And I will receive the email reminder that the auto-renew failed (http://ibo7mj.axshare.com/#p=email_reminder__renew_failed__d_&c=1)
      And I won't receive the manual overdue email reminder (http://xc7qxp.axshare.com/#p=email_-_overdue__d_)
      And all manual payment email reminders will be operative (D+28, D+15)
      And the failed payment method will be deleted

  Scenario: Auto renew defaults to ON for new purchases
    Given that I'm purchasing products/packages that supports or not auto-renew
     When I finish the purchase using a bank card (sagepay)
     Then the services that support auto-renew will have the auto-renew set to ON

  Scenario: Payment method will expire after DueDate (D)
    Given that I have a service set to auto renew on 17/10/2015 (D)
      And the payment method linked to the account is set to expire on 18/10/2015 (D+1)
     When it's time to send the D-7 email (on the 10/10/2015)
     Then I will receive the D-7 auto-renew email (http://ibo7mj.axshare.com/#p=email_reminder__renew_in_7_days__d-7_&c=1)

  Scenario: Payment method will expire on period D-7 to D
    Given that I have a service set to auto renew on 17/10/2015 (D)
      And the payment method linked to the account is set to expire on 11/10/2015 (D-6)
     When it's time to send the D-7 email (on the 10/10/2015)
     Then I won't receive the D-7 auto-renew email (http://ibo7mj.axshare.com/#p=email_reminder__renew_in_7_days__d-7_&c=1)
      And I will receive the 'your card will expire' email (https://www.companiesmadesimple.com/admin/en/1453/)

  Scenario: Payment method will expire before the due date
    Given that I have a service set to auto renew on 17/10/2015
      And the payment method linked to the account is set to expire before the payment due date
     When the payment method expires
     Then the service will have the auto-renew set to OFF
      And all manual payment email reminders will be operative (D+28, D+15, D)
      And the payment method will be deleted

  Scenario: Correct email behaviour for non auto renewal
    Given that I have a service due to 17/10/2015
      And the auto renew is OFF
     When the current day is 16/10/2015
     Then I will receive the manual payment D-1 email (https://www.companiesmadesimple.com/admin/en/1371/)

  Scenario: Delete payment method
    Given that I have a service set to auto renew
     When I delete my current payment method
     Then the auto renew of the service will be set to OFF

  Scenario: Purchase with different card (do not save auto-renew)
    Given that I'm purchasing a service
      And I have a payment method associated with my account
     When I pay this service by selecting the 'Use new card' option on the payment method
     Then the auto-renew option of services that support auto-renew will stay OFF


  # Added for: [CMS][RENEWAL] Turn auto-renew ON when updating payment method
  Background:
    Given following service:
    | ID | Type | Service Status | Auto-Renew Status | Token ID |
    | 1  | *    | Active         | ON                | 1        |

  Scenario: Payment method is removed
    Given token:1 was deleted
      And service:1:autoRenew is OFF
      And service:1:tokenId is 1
      And service:1 is not overdue
     When a new token is inserted
     Then service:1:autoRenew is ON
      And service:1:tokenId is the new inserted token


  # Possible future scenario
  # Background:
  #   Given following services:
  #   | ID | Type | Service Status | Auto-Renew Status | Token ID |
  #   | 1  | *    | Active         | ON                | 1        |
  #   | 2  | *    | Active         | ON                | 2        |
  #   | 3  | *    | Active         | OFF               |          |


  #TODO: S544 - re-enable only active services. token was deleted for 2 services, when you renew one of them manually do not re-enable the auto-renew for the other (it will be overdue)


    # Added for: [CMS][Services] Refunding auto-renewed payment should turn off auto-renew
    Scenario: Renewed service is refunded
      Given that I have a service with auto-renew=ON
       When the service is refunded
       Then the auto-renew will be set to OFF for all services of the same service type
