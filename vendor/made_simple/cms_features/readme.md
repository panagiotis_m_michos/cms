# CMS Features

This repository contains all the feature files for companiesmadesimple.com (CMS). These describe the desired behaviour of this system. What follows is a short description of structure of this repository and it's usage in CMS.

## Structure

Each directory in the root of this repository is a different domain. They can have different structure where it makes sense but the following directory structure is strongly recommended:

- /domain_name (the name of domain in question, e.g. "payment")
    - /acceptance (contains acceptance/domain tests that do not test the web interface)
    - /ui (contains web interface tests)
    - /reference (contains reference feature files/guidelines not used in tests, yet)

## Wall-E (Workflow Engine)
The Workflow Engine does not follow the recommended directory structure since it does not contain tests. It's structure is as follows:

- /workflow_engine
    - /domain_name (contains the workflows - feature files - for this domain)