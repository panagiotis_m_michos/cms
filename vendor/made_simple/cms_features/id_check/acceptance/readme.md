#Steps from "IdSearchWebContext" usable in "id_check.feature"

```
id_check_features | Given Id check page
                  | at `IdSearchWebContext::idCheckPage()`

id_check_features | Given Id check is required
                  | at `IdSearchWebContext::idCheckIsRequired()`

id_check_features | Given Id check failed :arg1 times
                  | at `IdSearchWebContext::idCheckFailedTimes()`

id_check_features | Given I have passed Id check
                  | at `IdSearchWebContext::iHavePassedIdCheck()`

id_check_features | Given I search for address :address
                  | at `IdSearchWebContext::iSearchForAddress()`

id_check_features | Given I add product :item to the basket
                  | at `IdSearchWebContext::iAddProductToTheBasket()`

id_check_features | Given I apply existing voucher :code with :discount percent discount
                  | at `IdSearchWebContext::iApplyExistingVoucher()`

id_check_features | Given I select my company
                  | at `IdSearchWebContext::iSelectMyCompany()`

id_check_features | Given I am on payment page with products :item
                  | at `IdSearchWebContext::iAmOnPaymentPageWithProducts()`

id_check_features | Given I am on payment page with package :item and companyName :companyName
                  | at `IdSearchWebContext::iAmOnPaymentPageWithPackage()`

id_check_features | Given I complete sage form with :key
                  | at `IdSearchWebContext::iCompleteSageForm()`

id_check_features | Given I complete 3d authentication form
                  | at `IdSearchWebContext::iComplete3dAuthenticationForm()`

id_check_features | Given I use email :email
                  | at `IdSearchWebContext::iUseEmail()`

id_check_features | Given Customer :customerKey with company :companyNumber exists
                  | at `IdSearchWebContext::customerWithCompanyExists()`

id_check_features | Given I buy :productId with credit
                  | at `IdSearchWebContext::iBuyWithCredit()`

id_check_features | Given I should not see auto-renewal checkbox
                  | at `IdSearchWebContext::iShouldNotSeeAutoRenewalCheckbox()`

id_check_features | When I buy :productId with new card
                  | at `IdSearchWebContext::iBuyWithNewCard()`

id_check_features | When I buy :productId with one-off card
                  | at `IdSearchWebContext::iBuyWithOneOffCard()`

id_check_features | Given I don't have any payment method saved
                  | at `IdSearchWebContext::iDontHaveAnyPaymentMethodSaved()`

id_check_features | Then I should see checked auto-renewal checkbox
                  | at `IdSearchWebContext::iShouldSeeCheckedAutoRenewalCheckbox()`

id_check_features | Then I should see unchecked auto-renewal checkbox
                  | at `IdSearchWebContext::iShouldSeeUncheckedAutoRenewalCheckbox()`

id_check_features | Given I am :customerEmail
                  | at `IdSearchWebContext::iAm()`

id_check_features | Given I have company :companyNumber
                  | at `IdSearchWebContext::iHaveCompany()`

id_check_features | Given I am :customerEmail with credit :credit
                  | at `IdSearchWebContext::iAmWithCredit()`

id_check_features | Given I log in
                  | at `IdSearchWebContext::iLogIn()`

id_check_features | Given I have a token
                  | at `IdSearchWebContext::iHaveAToken()`

id_check_features | Given I wait for :time
                  | at `IdSearchWebContext::wait()`

id_check_features | Given I am logged in customer
                  | at `IdSearchWebContext::iAmLoggedInCustomer()`

id_check_features | Given I click on an element with a title :title
                  | at `IdSearchWebContext::iClickOnTitle()`

id_check_features | Given Product :productId has :key :value
                  | at `IdSearchWebContext::modifyProduct()`

id_check_features | Given save screenshot
                  | at `IdSearchWebContext::saveScreenContents()`

id_check_features | Given /^(?:|I )am on (?:|the )homepage$/
                  | Opens homepage
                  | Example: Given I am on "/"
                  | Example: When I go to "/"
                  | Example: And I go to "/"
                  | at `IdSearchWebContext::iAmOnHomepage()`

id_check_features | When /^(?:|I )go to (?:|the )homepage$/
                  | Opens homepage
                  | Example: Given I am on "/"
                  | Example: When I go to "/"
                  | Example: And I go to "/"
                  | at `IdSearchWebContext::iAmOnHomepage()`

id_check_features | Given /^(?:|I )am on "(?P<page>[^"]+)"$/
                  | Opens specified page
                  | Example: Given I am on "http://batman.com"
                  | Example: And I am on "/articles/isBatmanBruceWayne"
                  | Example: When I go to "/articles/isBatmanBruceWayne"
                  | at `IdSearchWebContext::visit()`

id_check_features | When /^(?:|I )go to "(?P<page>[^"]+)"$/
                  | Opens specified page
                  | Example: Given I am on "http://batman.com"
                  | Example: And I am on "/articles/isBatmanBruceWayne"
                  | Example: When I go to "/articles/isBatmanBruceWayne"
                  | at `IdSearchWebContext::visit()`

id_check_features | When /^(?:|I )reload the page$/
                  | Reloads current page
                  | Example: When I reload the page
                  | Example: And I reload the page
                  | at `IdSearchWebContext::reload()`

id_check_features | When /^(?:|I )move backward one page$/
                  | Moves backward one page in history
                  | Example: When I move backward one page
                  | at `IdSearchWebContext::back()`

id_check_features | When /^(?:|I )move forward one page$/
                  | Moves forward one page in history
                  | Example: And I move forward one page
                  | at `IdSearchWebContext::forward()`

id_check_features | When /^(?:|I )press "(?P<button>(?:[^"]|\\")*)"$/
                  | Presses button with specified id|name|title|alt|value
                  | Example: When I press "Log In"
                  | Example: And I press "Log In"
                  | at `IdSearchWebContext::pressButton()`

id_check_features | When /^(?:|I )follow "(?P<link>(?:[^"]|\\")*)"$/
                  | Clicks link with specified id|title|alt|text
                  | Example: When I follow "Log In"
                  | Example: And I follow "Log In"
                  | at `IdSearchWebContext::clickLink()`

id_check_features | When /^(?:|I )fill in "(?P<field>(?:[^"]|\\")*)" with "(?P<value>(?:[^"]|\\")*)"$/
                  | Fills in form field with specified id|name|label|value
                  | Example: When I fill in "username" with: "bwayne"
                  | Example: And I fill in "bwayne" for "username"
                  | at `IdSearchWebContext::fillField()`

id_check_features | When /^(?:|I )fill in "(?P<field>(?:[^"]|\\")*)" with:$/
                  | Fills in form field with specified id|name|label|value
                  | Example: When I fill in "username" with: "bwayne"
                  | Example: And I fill in "bwayne" for "username"
                  | at `IdSearchWebContext::fillField()`

id_check_features | When /^(?:|I )fill in "(?P<value>(?:[^"]|\\")*)" for "(?P<field>(?:[^"]|\\")*)"$/
                  | Fills in form field with specified id|name|label|value
                  | Example: When I fill in "username" with: "bwayne"
                  | Example: And I fill in "bwayne" for "username"
                  | at `IdSearchWebContext::fillField()`

id_check_features | When /^(?:|I )fill in the following:$/
                  | Fills in form fields with provided table
                  | Example: When I fill in the following"
                  |              | username | bruceWayne |
                  |              | password | iLoveBats123 |
                  | Example: And I fill in the following"
                  |              | username | bruceWayne |
                  |              | password | iLoveBats123 |
                  | at `IdSearchWebContext::fillFields()`

id_check_features | When /^(?:|I )select "(?P<option>(?:[^"]|\\")*)" from "(?P<select>(?:[^"]|\\")*)"$/
                  | Selects option in select field with specified id|name|label|value
                  | Example: When I select "Bats" from "user_fears"
                  | Example: And I select "Bats" from "user_fears"
                  | at `IdSearchWebContext::selectOption()`

id_check_features | When /^(?:|I )additionally select "(?P<option>(?:[^"]|\\")*)" from "(?P<select>(?:[^"]|\\")*)"$/
                  | Selects additional option in select field with specified id|name|label|value
                  | Example: When I additionally select "Deceased" from "parents_alive_status"
                  | Example: And I additionally select "Deceased" from "parents_alive_status"
                  | at `IdSearchWebContext::additionallySelectOption()`

id_check_features | When /^(?:|I )check "(?P<option>(?:[^"]|\\")*)"$/
                  | Checks checkbox with specified id|name|label|value
                  | Example: When I check "Pearl Necklace" from "itemsClaimed"
                  | Example: And I check "Pearl Necklace" from "itemsClaimed"
                  | at `IdSearchWebContext::checkOption()`

id_check_features | When /^(?:|I )uncheck "(?P<option>(?:[^"]|\\")*)"$/
                  | Unchecks checkbox with specified id|name|label|value
                  | Example: When I uncheck "Broadway Plays" from "hobbies"
                  | Example: And I uncheck "Broadway Plays" from "hobbies"
                  | at `IdSearchWebContext::uncheckOption()`

id_check_features | When /^(?:|I )attach the file "(?P<path>[^"]*)" to "(?P<field>(?:[^"]|\\")*)"$/
                  | Attaches file to field with specified id|name|label|value
                  | Example: When I attach "bwayne_profile.png" to "profileImageUpload"
                  | Example: And I attach "bwayne_profile.png" to "profileImageUpload"
                  | at `IdSearchWebContext::attachFileToField()`

id_check_features | Then /^(?:|I )should be on "(?P<page>[^"]+)"$/
                  | Checks, that current page PATH is equal to specified
                  | Example: Then I should be on "/"
                  | Example: And I should be on "/bats"
                  | Example: And I should be on "http://google.com"
                  | at `IdSearchWebContext::assertPageAddress()`

id_check_features | Then /^(?:|I )should be on (?:|the )homepage$/
                  | Checks, that current page is the homepage
                  | Example: Then I should be on the homepage
                  | Example: And I should be on the homepage
                  | at `IdSearchWebContext::assertHomepage()`

id_check_features | Then /^the (?i)url(?-i) should match (?P<pattern>"(?:[^"]|\\")*")$/
                  | Checks, that current page PATH matches regular expression
                  | Example: Then the url should match "superman is dead"
                  | Example: Then the uri should match "log in"
                  | Example: And the url should match "log in"
                  | at `IdSearchWebContext::assertUrlRegExp()`

id_check_features | Then /^the response status code should be (?P<code>\d+)$/
                  | Checks, that current page response status is equal to specified
                  | Example: Then the response status code should be 200
                  | Example: And the response status code should be 400
                  | at `IdSearchWebContext::assertResponseStatus()`

id_check_features | Then /^the response status code should not be (?P<code>\d+)$/
                  | Checks, that current page response status is not equal to specified
                  | Example: Then the response status code should not be 501
                  | Example: And the response status code should not be 404
                  | at `IdSearchWebContext::assertResponseStatusIsNot()`

id_check_features | Then /^(?:|I )should see "(?P<text>(?:[^"]|\\")*)"$/
                  | Checks, that page contains specified text
                  | Example: Then I should see "Who is the Batman?"
                  | Example: And I should see "Who is the Batman?"
                  | at `IdSearchWebContext::assertPageContainsText()`

id_check_features | Then /^(?:|I )should not see "(?P<text>(?:[^"]|\\")*)"$/
                  | Checks, that page doesn't contain specified text
                  | Example: Then I should not see "Batman is Bruce Wayne"
                  | Example: And I should not see "Batman is Bruce Wayne"
                  | at `IdSearchWebContext::assertPageNotContainsText()`

id_check_features | Then /^(?:|I )should see text matching (?P<pattern>"(?:[^"]|\\")*")$/
                  | Checks, that page contains text matching specified pattern
                  | Example: Then I should see text matching "Batman, the vigilante"
                  | Example: And I should not see "Batman, the vigilante"
                  | at `IdSearchWebContext::assertPageMatchesText()`

id_check_features | Then /^(?:|I )should not see text matching (?P<pattern>"(?:[^"]|\\")*")$/
                  | Checks, that page doesn't contain text matching specified pattern
                  | Example: Then I should see text matching "Bruce Wayne, the vigilante"
                  | Example: And I should not see "Bruce Wayne, the vigilante"
                  | at `IdSearchWebContext::assertPageNotMatchesText()`

id_check_features | Then /^the response should contain "(?P<text>(?:[^"]|\\")*)"$/
                  | Checks, that HTML response contains specified string
                  | Example: Then the response should contain "Batman is the hero Gotham deserves."
                  | Example: And the response should contain "Batman is the hero Gotham deserves."
                  | at `IdSearchWebContext::assertResponseContains()`

id_check_features | Then /^the response should not contain "(?P<text>(?:[^"]|\\")*)"$/
                  | Checks, that HTML response doesn't contain specified string
                  | Example: Then the response should not contain "Bruce Wayne is a billionaire, play-boy, vigilante."
                  | Example: And the response should not contain "Bruce Wayne is a billionaire, play-boy, vigilante."
                  | at `IdSearchWebContext::assertResponseNotContains()`

id_check_features | Then /^(?:|I )should see "(?P<text>(?:[^"]|\\")*)" in the "(?P<element>[^"]*)" element$/
                  | Checks, that element with specified CSS contains specified text
                  | Example: Then I should see "Batman" in the "heroes_list" element
                  | Example: And I should see "Batman" in the "heroes_list" element
                  | at `IdSearchWebContext::assertElementContainsText()`

id_check_features | Then /^(?:|I )should not see "(?P<text>(?:[^"]|\\")*)" in the "(?P<element>[^"]*)" element$/
                  | Checks, that element with specified CSS doesn't contain specified text
                  | Example: Then I should not see "Bruce Wayne" in the "heroes_alter_egos" element
                  | Example: And I should not see "Bruce Wayne" in the "heroes_alter_egos" element
                  | at `IdSearchWebContext::assertElementNotContainsText()`

id_check_features | Then /^the "(?P<element>[^"]*)" element should contain "(?P<value>(?:[^"]|\\")*)"$/
                  | Checks, that element with specified CSS contains specified HTML
                  | Example: Then the "body" element should contain "style=\"color:black;\""
                  | Example: And the "body" element should contain "style=\"color:black;\""
                  | at `IdSearchWebContext::assertElementContains()`

id_check_features | Then /^the "(?P<element>[^"]*)" element should not contain "(?P<value>(?:[^"]|\\")*)"$/
                  | Checks, that element with specified CSS doesn't contain specified HTML
                  | Example: Then the "body" element should not contain "style=\"color:black;\""
                  | Example: And the "body" element should not contain "style=\"color:black;\""
                  | at `IdSearchWebContext::assertElementNotContains()`

id_check_features | Then /^(?:|I )should see an? "(?P<element>[^"]*)" element$/
                  | Checks, that element with specified CSS exists on page
                  | Example: Then I should see a "body" element
                  | Example: And I should see a "body" element
                  | at `IdSearchWebContext::assertElementOnPage()`

id_check_features | Then /^(?:|I )should not see an? "(?P<element>[^"]*)" element$/
                  | Checks, that element with specified CSS doesn't exist on page
                  | Example: Then I should not see a "canvas" element
                  | Example: And I should not see a "canvas" element
                  | at `IdSearchWebContext::assertElementNotOnPage()`

id_check_features | Then /^the "(?P<field>(?:[^"]|\\")*)" field should contain "(?P<value>(?:[^"]|\\")*)"$/
                  | Checks, that form field with specified id|name|label|value has specified value
                  | Example: Then the "username" field should contain "bwayne"
                  | Example: And the "username" field should contain "bwayne"
                  | at `IdSearchWebContext::assertFieldContains()`

id_check_features | Then /^the "(?P<field>(?:[^"]|\\")*)" field should not contain "(?P<value>(?:[^"]|\\")*)"$/
                  | Checks, that form field with specified id|name|label|value doesn't have specified value
                  | Example: Then the "username" field should not contain "batman"
                  | Example: And the "username" field should not contain "batman"
                  | at `IdSearchWebContext::assertFieldNotContains()`

id_check_features | Then /^(?:|I )should see (?P<num>\d+) "(?P<element>[^"]*)" elements?$/
                  | Checks, that (?P<num>\d+) CSS elements exist on the page
                  | Example: Then I should see 5 "div" elements
                  | Example: And I should see 5 "div" elements
                  | at `IdSearchWebContext::assertNumElements()`

id_check_features | Then /^the "(?P<checkbox>(?:[^"]|\\")*)" checkbox should be checked$/
                  | Checks, that checkbox with specified in|name|label|value is checked
                  | Example: Then the "remember_me" checkbox should be checked
                  | Example: And the "remember_me" checkbox is checked
                  | at `IdSearchWebContext::assertCheckboxChecked()`

id_check_features | Then /^the checkbox "(?P<checkbox>(?:[^"]|\\")*)" (?:is|should be) checked$/
                  | Checks, that checkbox with specified in|name|label|value is checked
                  | Example: Then the "remember_me" checkbox should be checked
                  | Example: And the "remember_me" checkbox is checked
                  | at `IdSearchWebContext::assertCheckboxChecked()`

id_check_features | Then /^the "(?P<checkbox>(?:[^"]|\\")*)" checkbox should not be checked$/
                  | Checks, that checkbox with specified in|name|label|value is unchecked
                  | Example: Then the "newsletter" checkbox should be unchecked
                  | Example: Then the "newsletter" checkbox should not be checked
                  | Example: And the "newsletter" checkbox is unchecked
                  | at `IdSearchWebContext::assertCheckboxNotChecked()`

id_check_features | Then /^the checkbox "(?P<checkbox>(?:[^"]|\\")*)" should (?:be unchecked|not be checked)$/
                  | Checks, that checkbox with specified in|name|label|value is unchecked
                  | Example: Then the "newsletter" checkbox should be unchecked
                  | Example: Then the "newsletter" checkbox should not be checked
                  | Example: And the "newsletter" checkbox is unchecked
                  | at `IdSearchWebContext::assertCheckboxNotChecked()`

id_check_features | Then /^the checkbox "(?P<checkbox>(?:[^"]|\\")*)" is (?:unchecked|not checked)$/
                  | Checks, that checkbox with specified in|name|label|value is unchecked
                  | Example: Then the "newsletter" checkbox should be unchecked
                  | Example: Then the "newsletter" checkbox should not be checked
                  | Example: And the "newsletter" checkbox is unchecked
                  | at `IdSearchWebContext::assertCheckboxNotChecked()`

id_check_features | Then /^print current URL$/
                  | Prints current URL to console.
                  | Example: Then print current URL
                  | Example: And print current URL
                  | at `IdSearchWebContext::printCurrentUrl()`

id_check_features | Then /^print last response$/
                  | Prints last response to console
                  | Example: Then print current response
                  | Example: And print current response
                  | at `IdSearchWebContext::printLastResponse()`

id_check_features | Then /^show last response$/
                  | Opens last response content in browser
                  | Example: Then show last response
                  | Example: And show last response
                  | at `IdSearchWebContext::showLastResponse()`
```
