@javascript @standalone
Feature: IdCheck
  Customer id check for product checked with id check flag

  Background:
    Given Customer "IdCheck" with company "1" exists
    And Product 475 has "isIdCheckRequired" true

  Scenario: Id check requirement redirection
    Given I log in
    When I buy 475 with credit
    And I press "Take me to my Dashboard"
    Then I should be on "/id-check/choose-id/#auth-form-block"

  Scenario: Id check requirement redirection
    Given Id check is required
    When I log in
    Then I should be on "/id-check/choose-id/"

  Scenario: Id check license success
    Given Id check page
    And I wait for 1000
    When I follow "UK Driving Licence"
    And I fill in the following:
      | idCheckForm[personal][firstName] | Sarah |
      | idCheckForm[personal][lastName] | Kerrigan |
      | Date of birth | 26/11/1988 |
      | Gender | Female |
      | License number  | KERRI861268SR9YB |
    And I search for address "cf15 9qq"
    And I wait for 1000
    And I click on an element with a title "CF15 9QQ, 48, Maes Y Sarn, Pentyrch, Cardiff "
    And I press "Validate"
    Then I should be on "/id-check/valid/#idCheck"
    And I should see "Your ID was successfully validated"

  Scenario: Id check fail
    Given Id check page
    And I wait for 1000
    When I fill in the following:
      | idCheckForm[personal][firstName] | Sarah |
      | idCheckForm[personal][lastName] | Kerrigan |
      | Date of birth | 01/02/2001 |
      | Gender | Male |
      | idCheckForm_passport_number_first  | 0340296446GBR7909255M1411065 |
      | idCheckForm_passport_number_third  | 06 |
      | Expiration date | 01/02/2101 |
    And I search for address "cf15 9qq"
    And I wait for 1000
    And I click on an element with a title "CF15 9QQ, 48, Maes Y Sarn, Pentyrch, Cardiff "
    And I press "Validate"
    Then I should see "Sorry, the validation of the Passport provided has failed"
    And I should see an "#retry-id-check" element

  Scenario: Id check fail 3 times
    Given I log in
    And Id check is required
    And Id check failed 3 times
    And I go to "/id-check/choose-id/"
    Then I should see "Sorry, your ID validation has failed 3 times"
    And I should not see an "#retry-id-check" element

  Scenario: Id required only once
    Given I have passed Id check
    And I log in
    When I buy 475 with credit
    And I press "Take me to my Dashboard"
    Then I should be on "/dashboard/#auth-form-block"
