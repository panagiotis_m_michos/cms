Feature: ID Check
  When a customer purchase a service from the ID Check required services list
  I want to have the customer ID Checked
  So we comply with the AML requirements

  A customer will be required to be ID checked if he purchase a service that have the 'ID Check required' flag checked
  (eg: https://www.companiesmadesimple.com/admin/en/1315/properties/)

  Changelog
  ---------
  - S403: https://trello.com/c/Fma8fOc0
  - S541: https://trello.com/c/GM15s0Eb
  - https://trello.com/c/tNQnARw2

  Profiles
  ---------
  - International passport - tNQnARw2: d25109a6-93df-4303-9df1-32fe71981c8e
  - UK Address - tNQnARw2: e5ffc262-7ee7-4831-911c-68a024fe4c29
  - UK Address and Passport - tNQnARw2: 7f8550c5-381a-4e64-947a-cc477c3524e0
  - UK Address and Driving Licence - tNQnARw2: a9b79d69-f052-49de-a05e-f0471fa14a36
  - UK Address and Euro ID - tNQnARw2: 2fb1f3b2-bf9e-4173-a6d8-49952a919460
  - Profiles URL: https://www.id3global.com/GlobalAdmin/


  Scoring table
  -------------
  | Databases                                                                  | Profiles                                                                                 |
  |                                                                            | UK Address and  | UK Address and  | UK Address and | International Passport              |
  | ID (P:+300; F:+1>216)| Name (P:+300) | Address (P:+10>40; F:+2000(2x1000)) | Driving Licence | Euro ID         | Passport       | (UK Address + Internation passport) |
  | PASS                 | PASS          | PASS                                | 610-650         | 610-650         | 610-650        | 610-650                             |
  | PASS                 | FAIL          | FAIL                                | 2300-2350       | 2300-2350       | 2300-2350      | 2300-2350                           |
  | FAIL                 | PASS          | PASS                                | 311-556         | 311-556         | 311-556        | 311-556                             |
  | FAIL                 | FAIL          | PASS                                | 11-256          | 11-256          | 11-256         | 11-256                              |
  | FAIL                 | FAIL          | FAIL                                | 2001-2216       | 2001-2216       | 2001-2216      | 2001-2216                           |
  # Impossible cases: These can't happen because the name is checked *against* the address and the driving license 
  | PASS                 | FAIL          | PASS                                |
  | FAIL                 | PASS          | FAIL                                |
  | PASS                 | PASS          | FAIL                                |

  Wireframes
  ----------
  - ID Proof Details: http://p6fii6.axshare.com/#p=proof_of_id_details_-_intl_passports&c=1
  - ID check FAILED: http://ihje2e.axshare.com/#p=proof_of_id_failed&c=1
  - ID check PASSED: http://ihje2e.axshare.com/#p=proof_of_id_passed&c=1
  - Email reminder for UK customers: http://ihje2e.axshare.com/#p=email_reminder_uk&c=1
  - Email reminder for non UK customers: http://ihje2e.axshare.com/#p=email_reminder_non_uk&c=1
  - Manual verification required email: http://ihje2e.axshare.com/#p=email_manual_verification_required&c=1
  - Auto-renew UK email: http://ihje2e.axshare.com/#p=email__auto-renew_uk&c=1
  - Auto-renew non UK: http://ihje2e.axshare.com/#p=email__auto-renew_non_uk&c=1

  Scenario: Flag customer
      Given I am not required to be ID checked
       When I purchase an ID check required product
       Then I'm flagged to be ID checked

  Scenario: Purchase of an ID check required product
      Given that I have an account on CMS
        And I'm flagged to be ID checked
        And that I have FAILED the ID check
       When I click continue at the purchase confirmation page
       Then I will see the ID Proof details page

  Scenario: Hold export if customer was not ID checked
      Given I'm flagged to be ID checked
        And that I have not passed the ID check
        And I have tried to validate my details for 3 times
       When I click continue at the purchase confirmation page
       Then I will be taken to the ID check FAILED page (with 'Disable try again fail message')
        And I won't be able to receive posts (order won't be exported to LPL)

  Scenario: Saved ID check details to My Details
      Given that I don't have an account on CMS
        And I'm flagged to be ID checked
       When I finish the online ID check
       Then I will see the My Details page
        And the form will be populated with the information I just used on the ID check form

  Scenario: ID check - Name and Address: PASS | ID Doc: PASS
      Given that I submit my details for ID check validation
       When the points in the response are equal to 400
       Then I will see the ID check PASSED page
        And I will be able to receive posts (order will be exported to LPL)

  Scenario: ID check - Name and Address: FAIL | ID Doc: FAIL
      Given that I submit my details for ID check validation
       When the points in the response are within the ranges 1-6 OR 301-306
       Then I will be taken to the ID check FAILED page (with 'Normal Fail message')
        And I won't be able to receive posts (order won't be exported to LPL)

  Scenario: ID check - Name and Address: FAIL | ID Doc: PASS
      Given that I submit my details for ID check validation
       When the points in the response is equal to 300
       Then I will be taken to the ID check FAILED page (with 'ID FAIL message')
        And I won't be able to receive posts (order won't be exported to LPL)

  Scenario: ID check - Name and Address: PASS | ID Doc: FAIL
      Given that I submit my details for ID check validation
       When the points in the response are within the ranges 101-115 OR 401-415
       Then I will be taken to the ID check FAILED page (with 'Doc FAIL message')
        And I won't be able to receive posts (order won't be exported to LPL)

  Scenario: Customer want to skip the online validation
       When I click the 'Skip validation' button in the ID check FAILED page
       Then I will receive the email reminder for UK customers
        And the admin team member will receive the Manual verification required email

  Scenario: Customer is not from the UK
       When I click the 'Skip validation' button on the proof of ID details page
       Then I will receive the email reminder for non UK customers
        And the admin team member will receive the Manual verification required email

  Scenario: Forced redirection to ID proof details page
      Given I'm flagged to be ID checked
        And I have not passed the ID check
        And I have tried to have my details validated for less than 3 times
       When I login to my CMS account
       Then I will be redirected to the ID proof details page

  Scenario: Forced redirection to ID proof results page
      Given I'm flagged to be ID checked
        And that I have not passed the ID check
        And I tried to have my details validated for 3 times
       When I login to my CMS account
       Then I will be redirected to the ID check FAILED page ('Disable try again FAIL message')

  Scenario: Auto-renewal UK clients
      Given I purchase an ID check required product
        And this product is set to auto-renew
        And I'm not flagged to be ID checked
        And I'm based in the UK
       When this product is auto-renewed
       Then I will be flagged to be ID checked
        And I will receive the email   Auto-renew UK email
        And I will be able to receive posts (order will be exported to LPL)

  Scenario: Auto-renewal non UK clients
      Given I purchase an ID check required product
        And this product is set to auto-renew
        And I'm not flagged to be ID checked
        And I'm based outside the UK
       When this product is auto-renewed
       Then I will be flagged to be ID checked
        And I will receive the email Auto-renew non UK
        And I will be able to receive posts (order will be exported to LPL)

  Scenario: Send 3 email reminders to auto-renew customers
      Given I'm flagged to be ID checked
        And that I have not passed the ID check
       When 7 days have passed since I received the last auto-renew email reminder about the proof of id
        And I have not received more than 2 email reminders
       Then I will receive the auto-renew proof of ID email reminder (UK or non UK)
