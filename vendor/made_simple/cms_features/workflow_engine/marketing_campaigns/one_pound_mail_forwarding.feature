@marketing_campaign:one_pound_mail_forwarding
@workflow_engine
Feature:
	When a new customer buy a privacy or comprehensive package
	I want to send them the £1 Mail Forwarding offer 
	So they sign up as a customer on London Presence

	Scenario: [Campaign: One Pound Mail Forwarding] Send first campaign email (privacy)
		Given I am an existing retail customer
		And I bought the Privacy Package Initial product
		And I have only one order
		When 7 days passed since the company was incorporated
		Then send email 4525 to me

	Scenario: [Campaign: One Pound Mail Forwarding] Send first campaign email (comprehensive)
		Given I am an existing retail customer
		And I bought the Comprehensive Package Initial product
		And I have only one order
		When 7 days passed since the company was incorporated
		Then send email 4525 to me

	Scenario: [Campaign: One Pound Mail Forwarding] Send second campaign email
		Given 5 days passed since email 4525 was sent to me
		And I have only one order
		When 12 days passed since the company was incorporated
		Then send email 4525 to me
