#Steps from "MarketingCampaignContext" usable in "barclays_sweeper.feature"

```
marketing_campaigns | Given I am :email
                    | at `WorkflowEngineModule\Contexts\MarketingCampaignContext::iAm()`

marketing_campaigns | Given /^I am an existing (.* customer)$/
                    | Selects customers of specified role (normal, wholesale)
                    | at `WorkflowEngineModule\Contexts\MarketingCampaignContext::iAmAnExistingCustomer()`

marketing_campaigns | Given /^I bought the (.* product)$/
                    | Selects customers who bought the product specified by product name.
                    | at `WorkflowEngineModule\Contexts\MarketingCampaignContext::iBoughtTheProduct()`

marketing_campaigns | Given /^I am (from .*)$/
                    | Selects customers who are from specified country.
                    | at `WorkflowEngineModule\Contexts\MarketingCampaignContext::iAmFrom()`

marketing_campaigns | Given I am not a business bank lead
                    | Selects customers who are not bank leads.
                    | at `WorkflowEngineModule\Contexts\MarketingCampaignContext::iAmNotABusinessBankLead()`

marketing_campaigns | Given /^email (\d+) was not sent to me in the last (\d+) days$/
                    | Selects customers who did not received email (specified by its ID) within last X days.
                    | at `WorkflowEngineModule\Contexts\MarketingCampaignContext::emailWasNotSentToMe()`

marketing_campaigns | When /^([0-9]+) days? passed since the company incorporation/
                    | Selects customers with companies that were incorporated exactly X days before today.
                    | at `WorkflowEngineModule\Contexts\MarketingCampaignContext::passedSinceTheCompanyIncorporation()`

marketing_campaigns | When :days days passed since email :emailId was sent to me
                    | Selects customers who received email (specified by its ID) exactly X days before today.
                    | at `WorkflowEngineModule\Contexts\MarketingCampaignContext::daysPassedSinceEmailWasSentToMe()`

marketing_campaigns | Then /^send email (\d+) to me$/
                    | Sends email (specified by its  ID) to customers selected beforehand.
                    | at `WorkflowEngineModule\Contexts\MarketingCampaignContext::sendEmailToMe()`
```
