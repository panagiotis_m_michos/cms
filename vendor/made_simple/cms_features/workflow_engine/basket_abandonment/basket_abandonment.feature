@marketing_campaign @workflow_engine
Feature:
  Whenever a customer abandons his basket before finishing his purchase
  I want to send him a email marketing campaign
  So that we can influence him to change his mind and finish his pending puchase

  # Email 4261: 'Abandoned Cart' email (infusionsoft)
  # Note that in the case of non logged-in customer we don't have the customer name. 
  # On the email, the name should be left blank.
  Scenario: Email customers with pending baskets
     When 1 hour passed since my basket has been saved
     Then send email 4261 to me

  # For dev reference only
  # Page 1223: Payment Page
  @disabled
  Scenario: Logged-in customer - Save basket
    Given I am an existing retail customer
      And my basket is not empty
     When I go to the page 1223
     Then save my basket content

  # For dev reference only
  # Page 1223: Payment Page
  @disabled
  Scenario: Non logged-in customer - Save basket
    Given I am not logged in
      And my basket is not empty
     When I provide my email in the page 1223
     Then save my basket content

  # For dev reference only
  @disabled
  Scenario: Delete basket - Purchase is made
     When I finish a payment
     Then delete my saved basket

  # For dev reference only
  @disabled
  Scenario: Delete basket - Basket expired
     When 8 days have passed since my basket content has been saved
     Then delete my saved basket
