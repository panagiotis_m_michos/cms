Feature: Wholesale purchase flow

  Basket, search, payment behavior for wholesale packages

  Scenario: Search than add wholesale package
    Given I am on "/professional/"
    And I fill in "companyName" with "Non existing company 232"
    And I press "search"
    Then I should be on "/professional/products-and-pricing/"
    When I press "Buy Now"
    Then I should be on "/page118en.html"
    And I should see "Starter"

  Scenario: Add wholesale package than search
    Given I am on "/taxassist/products-and-pricing/"
    When I press "Buy Now"
    Then I should be on "/taxassist/search/"
    And I fill in "companyName" with "Non existing company 232"
    And I press "search"
    And I press "search"
    Then I should be on "/page118en.html"
    And I should see "Starter"

  Scenario: Ability to pay for not logged in customer for wholesale package
    And I am on "/professional/icaew/"
    And I fill in "companyName" with "Non existing company 232"
    And I press "search"
    And I press "Buy Now"
    # feature
    And Feature 'payment_upgrade' is enabled
    And I follow "Proceed to checkout"
    Then I should be on "/payment/"