Feature: Wholesale payment

  Buying wholesale packages

  Scenario: New customer buying wholesale package
    Given I add wholesale package to the basket
    When I complete purchase
    Then I should have a role "wholesale"