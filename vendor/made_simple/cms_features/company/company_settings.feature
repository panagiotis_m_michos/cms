Feature: Company settings

  Company can have settings, which affects certain features

  Background:
    Given I am customer 1
    And I have company 1

  Scenario: Bank offer is not dismissed by default
    Then Company dismissed bank offer setting should be 'disabled'

  Scenario: Dismiss bank offer
    When I dismiss bank offer for company
    Then Company dismissed bank offer setting should be 'enabled'



