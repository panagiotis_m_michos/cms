<?php

use CSVParser\Parser\Parser;
use CSVParser\tests\Helpers\ItemHelper;
use Nette\FileNotFoundException;

class ParserTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Parser
     */
    protected $object;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    protected $columnMapperMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    protected $itemFactoryMock;


    protected function setUp()
    {
        $this->columnMapperMock = $this->getMockBuilder('CSVParser\Mapper\ColumnMapper')->disableOriginalConstructor()->getMock();
        $this->itemFactoryMock = $this->getMockBuilder('CSVParser\Item\IItemFactory')->disableOriginalConstructor()->getMock();
    }

    protected function tearDown()
    {
    }

    /**
     * @covers CSVParser\Parser\Parser::getItems
     */
    public function testGetItemsCount()
    {
        $this->assertCount(2, $this->getObject()->getItems());
    }

    /**
     * @covers CSVParser\Parser\Parser::getItems
     * @expectedException Nette\FileNotFoundException
     */
    public function testGetItemsNonExistingFile()
    {
        $this->getObject('non-existing.csv')->getItems();
    }

    /**
     * @param string $filename
     * @return Parser
     */
    private function getObject($filename = 'lpl_service_address_export.csv')
    {
        return new Parser(
            $this->columnMapperMock,
            $this->itemFactoryMock,
            __DIR__ . '/../Helpers' . DIRECTORY_SEPARATOR . $filename,
            Parser::SKIP_HEADER
        );
    }

}
