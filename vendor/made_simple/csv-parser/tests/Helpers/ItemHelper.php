<?php

namespace CSVParser\tests\Helpers;

use CSVParser\Annotations\Column;
use CSVParser\Item\IItem;
use DateTime;
use Nette\Object;
use Utils\Date;

class ItemHelper extends Object implements IItem
{
    /**
     * @Column(position=1, type="int")
     * @var int
     */
    private $id;

    /**
     * @Column(position=2, type="string")
     * @var string
     */
    private $name;

    /**
     * @Column(position=3, type="date", dateFormat="d/m/Y")
     * @var Date
     */
    private $dtExpires;

    /**
     * @Column(position=4, type="datetime", dateFormat="d/m/Y")
     * @var DateTime
     */
    private $dtc;

    /**
     * @param \Utils\Date $dtExpires
     */
    public function setDtExpires($dtExpires)
    {
        $this->dtExpires = $dtExpires;
    }

    /**
     * @return \Utils\Date
     */
    public function getDtExpires()
    {
        return $this->dtExpires;
    }

    /**
     * @param \DateTime $dtc
     */
    public function setDtc($dtc)
    {
        $this->dtc = $dtc;
    }

    /**
     * @return \DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}