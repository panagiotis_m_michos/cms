<?php

namespace CSVParser\tests\Helpers;

use CSVParser\Item\IItem;
use CSVParser\Item\IItemFactory;

class ItemFactory implements IItemFactory
{
    /**
     * @return IItem
     */
    public function create()
    {
        return new ItemHelper();
    }
}