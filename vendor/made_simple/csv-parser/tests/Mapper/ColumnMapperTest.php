<?php

use CSVParser\Mapper\ColumnMapper;
use CSVParser\tests\Helpers\ItemHelper;
use Doctrine\Common\Annotations\AnnotationReader;
use Faker\Generator;
use Nette\Utils\DateTime;

class ColumnMapperTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var ColumnMapper
     */
    protected $object;

    /**
     * @var AnnotationReader
     */
    protected $mock;

    /**
     * @var ItemHelper
     */
    protected $itemHelper;

    /**
     * @var Generator
     */
    protected  $faker;

    protected function setUp()
    {
        $this->object = new ColumnMapper(new AnnotationReader());
        $this->itemHelper = new ItemHelper();
        $this->faker = Faker\Factory::create();
    }

    protected function tearDown()
    {
    }

    /**
     * @covers CSVParser\Mapper\ColumnMapper::arrayToObject
     */
    public function testArrayToObject()
    {
        /* @var $item ItemHelper */
        $item = $this->object->arrayToObject(
            array(
                $id = $this->faker->randomDigit,
                $name = $this->faker->name,
                $dtExpires = $this->faker->dateTime->format('d/m/Y'),
                $dtc = $this->faker->dateTime->format('d/m/Y'),
            ),
            $this->itemHelper
        );

        $this->assertEquals($id, $item->getId());
        $this->assertEquals($name, $item->getName());
        $this->assertEquals($dtExpires, $item->getDtExpires()->format('d/m/Y'));
        $this->assertEquals($dtc, $item->getDtc()->format('d/m/Y'));
    }

    /**
     * @covers CSVParser\Mapper\ColumnMapper::arrayToObject
     */
    public function testArrayToObjectTrim()
    {
        /* @var $item ItemHelper */
        $item = $this->object->arrayToObject(
            array(
                $id = ' ' . $this->faker->randomDigit . ' ',
                $name = ' ' . $this->faker->name . ' ',
                $dtExpires = ' ' . $this->faker->dateTime->format('d/m/Y') . ' ',
                $dtc = ' ' . $this->faker->dateTime->format('d/m/Y ') . ' ',
            ),
            $this->itemHelper
        );

        $this->assertEquals(trim($id), $item->getId());
        $this->assertEquals(trim($name), $item->getName());
        $this->assertEquals(trim($dtExpires), $item->getDtExpires()->format('d/m/Y'));
        $this->assertEquals(trim($dtc), $item->getDtc()->format('d/m/Y'));
    }

    /**
     * @covers CSVParser\Mapper\ColumnMapper::arrayToObject
     */
    public function testArrayToObjectTypes()
    {
        /* @var $item ItemHelper */
        $item = $this->object->arrayToObject(
            array(
                $this->faker->randomDigit,
                $this->faker->name,
                $this->faker->dateTime->format('d/m/Y'),
                $this->faker->dateTime->format('d/m/Y '),
            ),
            $this->itemHelper
        );

        $this->assertTrue(is_int($item->getId()));
        $this->assertTrue(is_string($item->getName()));
        $this->assertTrue($item->getDtExpires() instanceof DateTime);
        $this->assertTrue($item->getDtc() instanceof DateTime);
    }

}
