### How to use


#### Item.php
~~~
namespace Import;

use CSVParser\Annotations\Column;
use CSVParser\Item\IItem;
use DateTime;
use Nette\Object;

class Item extends Object implements IItem
{
    /**
     * @Column(position=1, type="int")
     * @var int
     */
    private $customerId;

    /**
     * @Column(position=2, type="string")
     * @var string
     */
    private $email;

    /**
     * @Column(position=3, type="datetime")
     * @var DateTime
     */
    private $dtc;
}
~~~

#### ItemFactory.php

~~~~
namespace Import;

use CSVParser\Item\IItem;
use CSVParser\Item\IItemFactory;

class ItemFactory implements IItemFactory
{
    /**
     * @return IItem
     */
    public function create()
    {
        return new Item();
    }
}
~~~~

#### Implemnetation
~~~
use CSVParser\Mapper\ColumnMapper;
use Doctrine\Common\Annotations\Reader;
use CSVParser\Parser\Parser;
use Import\ItemFactory;

$parser = new Parser(
    $this->columnMapper,
    new ItemFactory(),
    $input->getArgument('filePath'),
    Parser::SKIP_HEADER
);
dump($parser->getItems());
~~~