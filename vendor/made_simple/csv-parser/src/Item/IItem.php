<?php

namespace CSVParser\Item;

use Nette\Reflection\ClassType;

interface IItem
{
    /**
     * @return ClassType
     */
    public static function getReflection();
} 