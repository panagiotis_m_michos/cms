<?php

namespace CSVParser\Item;

interface IItemFactory
{
    /**
     * @return IItem
     */
    public function create();
} 