<?php

namespace CSVParser\Annotations;

use CSVParser\Exceptions\InvalidArgumentException;
use Nette\Object;
use Nette\Utils\DateTime;

/**
 * @Annotation
 * @Target({"PROPERTY"})
 */
final class Column extends Object implements IColumnAnnotation
{
    /**
     * @var int
     */
    public $position;

    /**
     * @var string
     */
    public $type = 'string';

    /**
     * @var bool
     */
    public $nullable = TRUE;

    /**
     * @var string
     */
    public $dateFormat;

    /**
     * @param string $value
     * @throws InvalidArgumentException
     * @return mixed
     */
    public function mapValue($value)
    {
        $value = trim($value);
        switch ($this->type) {
            case 'date':
                if (!$this->dateFormat) {
                    throw InvalidArgumentException::missingDateFormat();
                }
                $date = DateTime::createFromFormat($this->dateFormat, $value);
                $date->setTime(0, 0, 0);
                return $date;
            case 'datetime':
                if (!$this->dateFormat) {
                    throw InvalidArgumentException::missingDateFormat();
                }
                return DateTime::createFromFormat($this->dateFormat, $value);
            case 'int':
                if ($this->nullable && $value == NULL) {
                    return NULL;
                }
                return (int) $value;
            default:
                return $value;
        }
    }

    /**
     * @return int
     */
    public function getIndex()
    {
        return ($this->position - 1);
    }
}