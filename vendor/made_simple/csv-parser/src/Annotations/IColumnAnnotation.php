<?php

namespace CSVParser\Annotations;

interface IColumnAnnotation
{
    /**
     * @param string $value
     * @return mixed
     */
    public function mapValue($value);

    /**
     * @return int
     */
    public function getIndex();
}