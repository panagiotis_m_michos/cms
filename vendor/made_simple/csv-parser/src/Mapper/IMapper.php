<?php

namespace CSVParser\Mapper;

use CSVParser\Item\IItem;

interface IMapper
{
    /**
     * @param array $row
     * @param IItem $object
     * @return IItem
     */
    public function arrayToObject(array $row, IItem $object);
}