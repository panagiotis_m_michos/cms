<?php

namespace CSVParser\Mapper;

use CSVParser\Annotations\IColumnAnnotation;
use CSVParser\Item\IItem;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\Common\Annotations\Reader;
use Nette\Object;
use Nette\Utils\Arrays;

class ColumnMapper extends Object implements IMapper
{
    /**
     * @var Reader
     */
    private $reader;

    /**
     * @var string
     */
    private $annotationName = 'CSVParser\\Annotations\\Column';

    /**
     * @param Reader $reader
     */
    public function __construct(Reader $reader)
    {
        $this->reader = $reader;
        AnnotationRegistry::registerFile(__DIR__ . '/../Annotations/Column.php');
    }

    /**
     * @param array $row
     * @param IItem $object
     * @return IItem
     */
    public function arrayToObject(array $row, IItem $object)
    {
        foreach ($object->getReflection()->getProperties() as $property) {
            /* @var $annotation IColumnAnnotation */
            $annotation = $this->reader->getPropertyAnnotation($property, $this->annotationName);
            if ($annotation) {
                $value = Arrays::get($row, $annotation->getIndex(), NULL);
                $property->setAccessible(TRUE);
                $property->setValue($object, $annotation->mapValue($value));
                $property->setAccessible(FALSE);
            }
        }
        return $object;
    }

} 