<?php

namespace CSVParser\Exceptions;

use Exception;

class InvalidArgumentException extends Exception
{
    /**
     * @return InvalidArgumentException
     */
    public static function missingDateFormat()
    {
        return new self('Missing date format');
    }

    /**
     * @param $file_path
     * @return InvalidArgumentException
     */
    public static function inputFileNotFound($file_path)
    {
        return new self("File $file_path not found!");
    }
}