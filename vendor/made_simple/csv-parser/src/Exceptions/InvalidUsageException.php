<?php

namespace CSVParser\Exceptions;

use Exception;

class InvalidUsageException extends Exception
{
    /**
     * @return InvalidUsageException
     */
    public static function immutableObjectAccess()
    {
        return new self('This object is immutable!');
    }
} 