<?php

namespace CSVParser\Parser;

use CSVParser\Exceptions\InvalidArgumentException;
use CSVParser\Item\IItemFactory;
use CSVParser\Items\Items;
use CSVParser\Mapper\ColumnMapper;
use CSVParser\Mapper\IMapper;
use Nette\Object;

class Parser extends Object
{
    const SKIP_HEADER = TRUE;

    /**
     * @var ColumnMapper
     */
    private $mapper;

    /**
     * @var IItemFactory
     */
    private $itemFactory;

    /**
     * @var string
     */
    private $csvFilePath;

    /**
     * @var boolean
     */
    private $skipHeader = FALSE;

    /**
     * @param IMapper $mapper
     * @param IItemFactory $itemFactory
     * @param string $csvFilePath
     * @param bool $skipHeader
     * @throws InvalidArgumentException
     */
    public function __construct(IMapper $mapper, IItemFactory $itemFactory, $csvFilePath, $skipHeader = FALSE)
    {
        if (!file_exists($csvFilePath)) {
            throw InvalidArgumentException::inputFileNotFound($csvFilePath);
        }

        $this->mapper = $mapper;
        $this->itemFactory = $itemFactory;
        $this->csvFilePath = $csvFilePath;
        $this->skipHeader = $skipHeader;
    }

    /**
     * @return Items
     */
    public function getItems()
    {
        return new Items($this->getRows(), $this->itemFactory, $this->mapper);
    }

    /**
     * @return array
     */
    private function getRows()
    {
        $rows = array_map('str_getcsv', file($this->csvFilePath, FILE_SKIP_EMPTY_LINES));
        if ($this->skipHeader) {
            array_shift($rows);
        }
        return $rows;
    }
}