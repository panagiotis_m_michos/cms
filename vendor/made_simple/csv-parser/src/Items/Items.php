<?php

namespace CSVParser\Items;

use ArrayAccess;
use Countable;
use CSVParser\Exceptions\InvalidUsageException;
use CSVParser\Item\IItem;
use CSVParser\Item\IItemFactory;
use CSVParser\Mapper\ColumnMapper;
use ErrorException;
use Iterator;

class Items implements Iterator, ArrayAccess, Countable
{
    /**
     * @var int
     */
    private $position = 0;

    /**
     * @var array
     */
    private $rows;

    /**
     * @var IItemFactory
     */
    private $itemFactory;

    /**
     * @var ColumnMapper
     */
    private $mapper;

    /**
     * @param array $rows
     * @param IItemFactory $itemFactory
     * @param ColumnMapper $mapper
     */
    public function __construct(array $rows,  IItemFactory $itemFactory, ColumnMapper $mapper)
    {
        $this->rows = $rows;
        $this->itemFactory = $itemFactory;
        $this->mapper = $mapper;
    }

    //---------------------------------------------------------------------- Iterator

    /**
     * Return the current element
     * @return IItem
     */
    public function current()
    {
        $row = $this->rows[$this->position];
        return $this->mapper->arrayToObject($row, $this->itemFactory->create());
    }

    /**
     * Move forward to next element
     * @return NULL
     */
    public function next()
    {
        $this->position++;
    }

    /**
     * Return the key of the current element
     * @return int
     */
    public function key()
    {
        return $this->position;
    }

    /**
     * Checks if current position is valid
     * @return boolean
     */
    public function valid()
    {
        return isset($this->rows[$this->position]);
    }

    /**
     * Rewind the Iterator to the first element
     * @return void
     */
    public function rewind()
    {
        $this->position = 0;
    }

    //---------------------------------------------------------------------- ArrayAccess

    /**
     * Whether a offset exists
     * @param mixed $offset
     * @return boolean true on success or false on failure.
     */
    public function offsetExists($offset)
    {
        return isset($this->rows[$offset]);
    }

    /**
     * Offset to retrieve
     * @param mixed $offset
     * @return mixed IItem
     */
    public function offsetGet($offset)
    {
        $row = $this->rows[$offset];
        return $this->mapper->arrayToObject($row, $this->itemFactory->create());
    }

    /**
     * Offset to set
     * @param mixed $offset
     * @param mixed $value
     * @throws InvalidUsageException
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        throw InvalidUsageException::immutableObjectAccess();
    }

    /**
     * Offset to unset
     * @param mixed $offset
     * @throws InvalidUsageException
     * @return void
     */
    public function offsetUnset($offset)
    {
        throw InvalidUsageException::immutableObjectAccess();
    }

    /**
     * Count elements of an object
     * @return int
     */
    public function count()
    {
        return count($this->rows);
    }
}