<?php

namespace Tests\Id3GlobalApiClient;

use DateTime;
use Id3GlobalApiClient\Helpers\Profile;
use Id3GlobalApiClient\Id3GlobalApiClient;
use PHPUnit_Framework_TestCase;
use stdClass;

class Id3GlobalApiClientTest extends PHPUnit_Framework_TestCase {

    /**
     * @covers Id3GlobalApiClient\Id3GlobalApiClient::__construct
     */
    public function testCanInstantiate()
    {
        $profileConfigurationMock = $this->getMockBuilder('Id3GlobalApiClient\ProfileConfigurations')->disableOriginalConstructor()->getMock();
        $soapClientMock =  $this->getMockBuilder('Id3GlobalApiClient\SoapClient')->disableOriginalConstructor()->getMock();
        $username = 'username';
        $password = 'password';

        $client = new Id3GlobalApiClient($username, $password, $profileConfigurationMock, $soapClientMock);
        $this->assertInstanceOf('Id3GlobalApiClient\Id3GlobalApiClient', $client);
    }

    /**
     * @covers Id3GlobalApiClient\Id3GlobalApiClient::checkCredentials
     */
    public function testCanCheckCredentials()
    {
        $profileConfigurationMock = $this->getMockBuilder('Id3GlobalApiClient\ProfileConfigurations')->disableOriginalConstructor()->getMock();
        $soapClientMock =  $this->getMockBuilder('Id3GlobalApiClient\SoapClient')->disableOriginalConstructor()->getMock();
        $username = 'username';
        $password = 'password';

        $soapClientMock->expects($this->once())->method('__call')->with('CheckCredentials')->will($this->returnValue('ok'));

        $client = new Id3GlobalApiClient($username, $password, $profileConfigurationMock, $soapClientMock);
        $this->assertEquals('ok', $client->checkCredentials());
    }

    /**
     * @covers Id3GlobalApiClient\Id3GlobalApiClient::authenticate
     */
    public function testCanAuthenticate()
    {
        $profileConfigurationsMock = $this->getMockBuilder('Id3GlobalApiClient\ProfileConfigurations')->disableOriginalConstructor()->getMock();
        $profileConfigurationMock = $this->getMockBuilder('Id3GlobalApiClient\ProfileConfiguration')->disableOriginalConstructor()->getMock();
        $globalProfileIdVersionMock = $this->getMockBuilder('Id3GlobalApiClient\Entities\Soap\GlobalProfileIDVersion')->disableOriginalConstructor()->getMock();
        $soapClientMock =  $this->getMockBuilder('Id3GlobalApiClient\SoapClient')->disableOriginalConstructor()->getMock();
        $username = 'username';
        $password = 'password';
        $addressMock = $this->getMockBuilder('Id3GlobalApiClient\Entities\Address')->disableOriginalConstructor()->getMock();
        $personalDetailsMock = $this->getMockBuilder('Id3GlobalApiClient\Entities\PersonalDetails')->disableOriginalConstructor()->getMock();
        $ukDrivingLicenseMock = $this->getMockBuilder('Id3GlobalApiClient\Entities\UkDrivingLicense')->disableOriginalConstructor()->getMock();
        $authenticationResultMock = new StdClass;
        $authenticationResultMock->AuthenticateSPResult = new StdClass;
        $authenticationResultMock->AuthenticateSPResult->Score = 100;

        $profileConfigurationMock->expects($this->any())->method('getProfileIdVersion')->willReturn($globalProfileIdVersionMock);
        $profileConfigurationsMock->expects($this->any())->method('getProfileIdVersion')->willReturn($profileConfigurationMock);
        $addressMock->expects($this->any())->method('getAddress1')->willReturn('test');
        $personalDetailsMock->expects($this->any())->method('getDateOfBirth')->willReturn(new DateTime());
        $soapClientMock->expects($this->once())->method('__call')->with('AuthenticateSP')->willReturn($authenticationResultMock);
        $soapClientMock->expects($this->exactly(2))->method('__setSoapHeaders');

        $client = new Id3GlobalApiClient($username, $password, $profileConfigurationsMock, $soapClientMock);
        $this->assertEquals(100, $client->authenticate($personalDetailsMock, $addressMock, $ukDrivingLicenseMock)->getScore());
    }

    /**
     * @covers Id3GlobalApiClient\Id3GlobalApiClient::authenticateUkDrivingLicense
     */
    public function testCanAuthenticateUkDrivingLicense()
    {
        $profileConfigurationsMock = $this->getMockBuilder('Id3GlobalApiClient\ProfileConfigurations')->disableOriginalConstructor()->getMock();
        $profileConfigurationMock = $this->getMockBuilder('Id3GlobalApiClient\ProfileConfiguration')->disableOriginalConstructor()->getMock();
        $globalProfileIdVersionMock = $this->getMockBuilder('Id3GlobalApiClient\Entities\Soap\GlobalProfileIDVersion')->disableOriginalConstructor()->getMock();
        $soapClientMock =  $this->getMockBuilder('Id3GlobalApiClient\SoapClient')->disableOriginalConstructor()->getMock();
        $username = 'username';
        $password = 'password';
        $addressMock = $this->getMockBuilder('Id3GlobalApiClient\Entities\Address')->disableOriginalConstructor()->getMock();
        $personalDetailsMock = $this->getMockBuilder('Id3GlobalApiClient\Entities\PersonalDetails')->disableOriginalConstructor()->getMock();
        $ukDrivingLicenseMock = $this->getMockBuilder('Id3GlobalApiClient\Entities\UkDrivingLicense')->disableOriginalConstructor()->getMock();
        $authenticationResultMock = new StdClass;
        $authenticationResultMock->AuthenticateSPResult = new StdClass;
        $authenticationResultMock->AuthenticateSPResult->Score = 100;

        $profileConfigurationMock->expects($this->any())->method('getProfileIdVersion')->willReturn($globalProfileIdVersionMock);
        $profileConfigurationsMock->expects($this->any())->method('getProfileIdVersion')->willReturn($profileConfigurationMock);
        $addressMock->expects($this->any())->method('getAddress1')->willReturn('test');
        $personalDetailsMock->expects($this->any())->method('getDateOfBirth')->willReturn(new DateTime());
        $soapClientMock->expects($this->once())->method('__call')->with('AuthenticateSP')->willReturn($authenticationResultMock);
        $soapClientMock->expects($this->exactly(2))->method('__setSoapHeaders');

        $client = new Id3GlobalApiClient($username, $password, $profileConfigurationsMock, $soapClientMock);
        $this->assertEquals(100, $client->authenticateUkDrivingLicense($personalDetailsMock, $addressMock, $ukDrivingLicenseMock)->getScore());
    }

    /**
     * @covers Id3GlobalApiClient\Id3GlobalApiClient::authenticateUkPassport
     */
    public function testCanAuthenticateUkPassport()
    {
        $profileConfigurationsMock = $this->getMockBuilder('Id3GlobalApiClient\ProfileConfigurations')->disableOriginalConstructor()->getMock();
        $profileConfigurationMock = $this->getMockBuilder('Id3GlobalApiClient\ProfileConfiguration')->disableOriginalConstructor()->getMock();
        $globalProfileIdVersionMock = $this->getMockBuilder('Id3GlobalApiClient\Entities\Soap\GlobalProfileIDVersion')->disableOriginalConstructor()->getMock();
        $soapClientMock =  $this->getMockBuilder('Id3GlobalApiClient\SoapClient')->disableOriginalConstructor()->getMock();
        $username = 'username';
        $password = 'password';
        $addressMock = $this->getMockBuilder('Id3GlobalApiClient\Entities\Address')->disableOriginalConstructor()->getMock();
        $personalDetailsMock = $this->getMockBuilder('Id3GlobalApiClient\Entities\PersonalDetails')->disableOriginalConstructor()->getMock();
        $ukPassportMock = $this->getMockBuilder('Id3GlobalApiClient\Entities\UkPassport')->disableOriginalConstructor()->getMock();
        $authenticationResultMock = new StdClass;
        $authenticationResultMock->AuthenticateSPResult = new StdClass;
        $authenticationResultMock->AuthenticateSPResult->Score = 100;

        $ukPassportMock->expects($this->any())->method('getExpirationDate')->willReturn(new DateTime());
        $profileConfigurationMock->expects($this->any())->method('getProfileIdVersion')->willReturn($globalProfileIdVersionMock);
        $profileConfigurationsMock->expects($this->any())->method('getProfileIdVersion')->willReturn($profileConfigurationMock);
        $addressMock->expects($this->any())->method('getAddress1')->willReturn('test');
        $personalDetailsMock->expects($this->any())->method('getDateOfBirth')->willReturn(new DateTime());
        $soapClientMock->expects($this->once())->method('__call')->with('AuthenticateSP')->willReturn($authenticationResultMock);
        $soapClientMock->expects($this->exactly(2))->method('__setSoapHeaders');

        $client = new Id3GlobalApiClient($username, $password, $profileConfigurationsMock, $soapClientMock);
        $this->assertEquals(100, $client->authenticateUkPassport($personalDetailsMock, $addressMock, $ukPassportMock)->getScore());
    }

    /**
     * @covers Id3GlobalApiClient\Id3GlobalApiClient::authenticateEuIdCard
     */
    public function testCanAuthenticateEuIdCard()
    {
        $profileConfigurationsMock = $this->getMockBuilder('Id3GlobalApiClient\ProfileConfigurations')->disableOriginalConstructor()->getMock();
        $profileConfigurationMock = $this->getMockBuilder('Id3GlobalApiClient\ProfileConfiguration')->disableOriginalConstructor()->getMock();
        $globalProfileIdVersionMock = $this->getMockBuilder('Id3GlobalApiClient\Entities\Soap\GlobalProfileIDVersion')->disableOriginalConstructor()->getMock();
        $soapClientMock =  $this->getMockBuilder('Id3GlobalApiClient\SoapClient')->disableOriginalConstructor()->getMock();
        $username = 'username';
        $password = 'password';
        $addressMock = $this->getMockBuilder('Id3GlobalApiClient\Entities\Address')->disableOriginalConstructor()->getMock();
        $personalDetailsMock = $this->getMockBuilder('Id3GlobalApiClient\Entities\PersonalDetails')->disableOriginalConstructor()->getMock();
        $euIdCardMock = $this->getMockBuilder('Id3GlobalApiClient\Entities\EuropeanIdCard')->disableOriginalConstructor()->getMock();
        $authenticationResultMock = new StdClass;
        $authenticationResultMock->AuthenticateSPResult = new StdClass;
        $authenticationResultMock->AuthenticateSPResult->Score = 100;

        $euIdCardMock->expects($this->any())->method('getExpirationDate')->willReturn(new DateTime());
        $profileConfigurationMock->expects($this->any())->method('getProfileIdVersion')->willReturn($globalProfileIdVersionMock);
        $profileConfigurationsMock->expects($this->any())->method('getProfileIdVersion')->willReturn($profileConfigurationMock);
        $addressMock->expects($this->any())->method('getAddress1')->willReturn('test');
        $personalDetailsMock->expects($this->any())->method('getDateOfBirth')->willReturn(new DateTime());
        $soapClientMock->expects($this->once())->method('__call')->with('AuthenticateSP')->willReturn($authenticationResultMock);
        $soapClientMock->expects($this->exactly(2))->method('__setSoapHeaders');

        $client = new Id3GlobalApiClient($username, $password, $profileConfigurationsMock, $soapClientMock);
        $this->assertEquals(100, $client->authenticateEuIdCard($personalDetailsMock, $addressMock, $euIdCardMock)->getScore());
    }
}
