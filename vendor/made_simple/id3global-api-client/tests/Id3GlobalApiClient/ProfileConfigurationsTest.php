<?php

namespace Tests\Id3GlobalApiClient;

use Id3GlobalApiClient\Helpers\Profile;
use Id3GlobalApiClient\ProfileConfigurations;
use PHPUnit_Framework_TestCase;

class ProfileConfigurationsTest extends PHPUnit_Framework_TestCase {

    /**
     * @covers Id3GlobalApiClient\ProfileConfigurations::__construct
     */
    public function testCanInstantiate()
    {
        $profileConfigurations = new ProfileConfigurations(array());
        $this->assertInstanceOf('Id3GlobalApiClient\ProfileConfigurations', $profileConfigurations);
    }

    /**
     * @covers Id3GlobalApiClient\ProfileConfigurations::fromArrayConfig
     */
    public function testCanInstantiateFromArray()
    {
        $profileConfigurations = ProfileConfigurations::fromArrayConfig(array(
            'UK_ADDRESS_UK_DRIVING_LICENSE' => array(
                'id' => 'id3',
                'version' => 1.1
            ),
            'UK_ADDRESS_EU_ID_CARD' => array(
                'id' => 'id2',
                'version' => 1.2
            ),
            'UK_ADDRESS_UK_PASSPORT' => array(
                'id' => 'id1',
                'version' => 1.1
            )
        ));

        $this->assertInstanceOf('Id3GlobalApiClient\ProfileConfigurations', $profileConfigurations);
    }

    /**
     * @covers Id3GlobalApiClient\ProfileConfigurations::getProfileIdVersion
     */
    public function testCanGetProfileIdVersion()
    {
        $profileConfigurations = ProfileConfigurations::fromArrayConfig(array(
            'UK_ADDRESS_UK_DRIVING_LICENSE' => array(
                'id' => 'id3',
                'version' => 1.1
            ),
            'UK_ADDRESS_EU_ID_CARD' => array(
                'id' => 'id2',
                'version' => 1.2
            ),
            'UK_ADDRESS_UK_PASSPORT' => array(
                'id' => 'id1',
                'version' => 1.1
            )
        ));

        $actualId = $profileConfigurations->getProfileIdVersion(new Profile(Profile::UK_ADDRESS_UK_PASSPORT))->getProfileIdVersion()->getID();
        $this->assertEquals('id1', $actualId);
    }
}
