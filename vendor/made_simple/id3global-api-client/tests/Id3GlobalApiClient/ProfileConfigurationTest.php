<?php

namespace Tests\Id3GlobalApiClient;

use Id3GlobalApiClient\Entities\Soap\GlobalProfileIDVersion;
use Id3GlobalApiClient\ProfileConfiguration;
use PHPUnit_Framework_TestCase;

class ProfileConfigurationTest extends PHPUnit_Framework_TestCase {

    /**
     * @covers Id3GlobalApiClient\ProfileConfiguration::__construct
     */
    public function testCanInstantiate()
    {
        $profileMock = $this->getMockBuilder('Id3GlobalApiClient\Helpers\Profile')->disableOriginalConstructor()->getMock();
        $endpointVersionMock =  $this->getMockBuilder('Id3GlobalApiClient\Helpers\EndpointVersion')->disableOriginalConstructor()->getMock();;
        $id = 'random_string';

        $profileConfiguration = new ProfileConfiguration($profileMock, $endpointVersionMock, $id);
        $this->assertInstanceOf('Id3GlobalApiClient\ProfileConfiguration', $profileConfiguration);
    }

    /**
     * @covers Id3GlobalApiClient\ProfileConfiguration::getProfileIdVersion
     * @covers Id3GlobalApiClient\ProfileConfiguration::getProfile
     */
    public function testCanGetProfileIdVersionAndEndpointVersion()
    {

        $profileMock = $this->getMockBuilder('Id3GlobalApiClient\Helpers\Profile')
            ->disableOriginalConstructor()
            ->setMethods(array('getValue'))
            ->getMock();
        $profileMock->expects($this->once())->method('getValue')->willReturn('profile');

        $endpointVersionMock = $this->getMockBuilder('Id3GlobalApiClient\Helpers\EndpointVersion')
            ->disableOriginalConstructor()
            ->setMethods(array('getValue'))
            ->getMock();
        $endpointVersionMock->expects($this->once())->method('getValue')->willReturn(1.1);

        $profileConfiguration = new ProfileConfiguration($profileMock, $endpointVersionMock, 'id');

        $this->assertEquals('profile', $profileConfiguration->getProfile()->getValue());
        $this->assertEquals(new GlobalProfileIDVersion('id', 1.1), $profileConfiguration->getProfileIdVersion());
    }
}
