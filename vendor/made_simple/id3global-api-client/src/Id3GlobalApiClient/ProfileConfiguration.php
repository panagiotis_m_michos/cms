<?php

namespace Id3GlobalApiClient;

use Id3GlobalApiClient\Entities\Soap\GlobalProfileIDVersion;
use Id3GlobalApiClient\Helpers\EndpointVersion;
use Id3GlobalApiClient\Helpers\Profile;

class ProfileConfiguration
{
    /**
     * @var Profile
     */
    private $profile;

    /**
     * @var GlobalProfileIDVersion
     */
    private $profileIdVersion;


    /**
     * @param Profile $profile
     * @param EndpointVersion $endpointVersion
     * @param string $id
     */
    public function __construct(Profile $profile, EndpointVersion $endpointVersion, $id)
    {
        $this->profile = $profile;
        $this->profileIdVersion = new GlobalProfileIDVersion($id, $endpointVersion->getValue());
    }

    /**
     * @return Profile
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @return GlobalProfileIDVersion
     */
    public function getProfileIdVersion()
    {
        return $this->profileIdVersion;
    }
}