<?php

namespace Id3GlobalApiClient\Helpers;

use Garoevans\PhpEnum\Enum;

class Profile extends Enum
{
    const __default = self::UK_ADDRESS_UK_PASSPORT;
    const UK_ADDRESS_UK_PASSPORT = 'UK_ADDRESS_UK_PASSPORT';
    const UK_ADDRESS_UK_DRIVING_LICENSE = 'UK_ADDRESS_UK_DRIVING_LICENSE';
    const UK_ADDRESS_EU_ID_CARD = 'UK_ADDRESS_EU_ID_CARD';
    const UK_ADDRESS = 'UK_ADDRESS';
    const INTERNATIONAL_PASSPORT = 'INTERNATIONAL_PASSPORT';

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->enum;
    }
}