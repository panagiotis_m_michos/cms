<?php

namespace Id3GlobalApiClient\Helpers;

use Garoevans\PhpEnum\Enum;

class EndpointVersion extends Enum
{
    const __default = self::LATEST;
    const LATEST = '0';
    const V_1_1 = '1.1';
    const V_1_2 = '1.2';
    const V_1_5 = '1.5';

    /**
     * @return mixed
     */
    public function getValue()
    {
        return floatval($this->enum);
    }
}