<?php

namespace Id3GlobalApiClient\Helpers;

use Garoevans\PhpEnum\Enum;

class Wsdl extends Enum
{
    const __default = self::LIVE;
    const LIVE = 'https://id3Global.com/ID3gWS/Id3Global.svc?wsdl';
    const PILOT = 'https://pilot.Id3Global.com/ID3gWS/Id3Global.svc?wsdl';

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->enum;
    }
}