<?php

namespace Id3GlobalApiClient\Entities;

use DateTime;
use Id3GlobalApiClient\Interfaces\IIdCard;

class EuropeanIdCard implements IIdCard
{
    /**
     * @var NULL|string
     */
    private $line1;

    /**
     * @var NULL|string
     */
    private $line2;

    /**
     * @var NULL|string
     */
    private $line3;

    /**
     * @var NULL|DateTime
     */
    private $expirationDate;

    /**
     * @var NULL|string
     */
    private $countryOfNationality;

    /**
     * @var NULL|string
     */
    private $countryOfIssue;

    /**
     * @param string $line1
     * @param string $line2
     * @param string $line3
     * @param DateTime $expirationDate
     * @param string $countryOfNationality
     * @param string $countryOfIssue
     */
    public function __construct(
        $line1 = NULL,
        $line2 = NULL,
        $line3 = NULL,
        $expirationDate = NULL,
        $countryOfNationality = NULL,
        $countryOfIssue = NULL
    )
    {

        $this->line1 = $line1;
        $this->line2 = $line2;
        $this->line3 = $line3;
        $this->expirationDate = $expirationDate;
        $this->countryOfNationality = $countryOfNationality;
        $this->countryOfIssue = $countryOfIssue;
    }

    /**
     * @return string
     */
    public function getLine1()
    {
        return $this->line1;
    }

    /**
     * @param string $line1
     */
    public function setLine1($line1)
    {
        $this->line1 = $line1;
    }

    /**
     * @return string
     */
    public function getLine2()
    {
        return $this->line2;
    }

    /**
     * @param string $line2
     */
    public function setLine2($line2)
    {
        $this->line2 = $line2;
    }

    /**
     * @return string
     */
    public function getLine3()
    {
        return $this->line3;
    }

    /**
     * @param string $line3
     */
    public function setLine3($line3)
    {
        $this->line3 = $line3;
    }

    /**
     * @return DateTime
     */
    public function getExpirationDate()
    {
        return $this->expirationDate;
    }

    /**
     * @param DateTime $expirationDate
     */
    public function setExpirationDate(DateTime $expirationDate)
    {
        $this->expirationDate = $expirationDate;
    }

    /**
     * @return string
     */
    public function getCountryOfNationality()
    {
        return $this->countryOfNationality;
    }

    /**
     * @param string $countryOfNationality
     */
    public function setCountryOfNationality($countryOfNationality)
    {
        $this->countryOfNationality = $countryOfNationality;
    }

    /**
     * @return string
     */
    public function getCountryOfIssue()
    {
        return $this->countryOfIssue;
    }

    /**
     * @param string $countryOfIssue
     */
    public function setCountryOfIssue($countryOfIssue)
    {
        $this->countryOfIssue = $countryOfIssue;
    }
}