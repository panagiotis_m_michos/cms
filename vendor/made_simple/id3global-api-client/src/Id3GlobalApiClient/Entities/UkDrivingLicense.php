<?php

namespace Id3GlobalApiClient\Entities;

use Id3GlobalApiClient\Interfaces\IDrivingLicense;

class UkDrivingLicense implements IDrivingLicense
{
    /**
     * @var string
     */
    private $number;

    /**
     * @param string $number
     */
    public function __construct($number)
    {

        $this->number = $number;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }
}