<?php

namespace Id3GlobalApiClient\Entities;

use Id3GlobalApiClient\Interfaces\IAddress;

class Address implements IAddress
{
    /**
     * @var NULL|string
     */
    private $line1;

    /**
     * @var NULL|string
     */
    private $line2;

    /**
     * @var NULL|string
     */
    private $line3;

    /**
     * @var NULL|string
     */
    private $city;

    /**
     * @var NULL|string
     */
    private $county;

    /**
     * @var NULL|string
     */
    private $postcode;

    /**
     * @var NULL|string
     */
    private $country;

    /**
     * @param string $line1
     * @param string $line2
     * @param string $line3
     * @param string $city
     * @param string $county
     * @param string $postcode
     * @param string $country
     */
    public function __construct($line1 = NULL, $line2 = NULL, $line3 = NULL, $city = NULL, $county = NULL, $postcode = NULL, $country = NULL)
    {

        $this->line1 = $line1;
        $this->line2 = $line2;
        $this->line3 = $line3;
        $this->city = $city;
        $this->county = $county;
        $this->postcode = $postcode;
        $this->country = $country;
    }

    /**
     * @return NULL|string
     */
    public function getAddress1()
    {
        return $this->line1;
    }

    /**
     * @param NULL|string $line1
     */
    public function setAddress1($line1)
    {
        $this->line1 = $line1;
    }

    /**
     * @return NULL|string
     */
    public function getAddress2()
    {
        return $this->line2;
    }

    /**
     * @param NULL|string $line2
     */
    public function setAddress2($line2)
    {
        $this->line2 = $line2;
    }

    /**
     * @return NULL|string
     */
    public function getAddress3()
    {
        return $this->line3;
    }

    /**
     * @param NULL|string $line3
     */
    public function setAddress3($line3)
    {
        $this->line3 = $line3;
    }

    /**
     * @return NULL|string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param NULL|string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return NULL|string
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * @param NULL|string $county
     */
    public function setCounty($county)
    {
        $this->county = $county;
    }

    /**
     * @return NULL|string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @param NULL|string $postcode
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    }

    /**
     * @return NULL|string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param NULL|string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }
}