<?php

namespace Id3GlobalApiClient\Entities;

use DateTime;
use Id3GlobalApiClient\Interfaces\IPersonalDetails;

class PersonalDetails implements IPersonalDetails
{
    /**
     * @var NULL|string
     */
    private $firstName;

    /**
     * @var NULL|string
     */
    private $lastName;

    /**
     * @var NULL|DateTime
     */
    private $dateOfBirth;

    /**
     * @var NULL|string
     */
    private $gender;

    /**
     * @var NULL|string
     */
    private $middleName;

    /**
     * @param string $firstName
     * @param string $lastName
     * @param DateTime $dateOfBirth
     * @param string $gender
     * @param string $middleName
     */
    public function __construct($firstName = NULL, $lastName = NULL, DateTime $dateOfBirth = NULL, $gender = NULL, $middleName = NULL)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->dateOfBirth = $dateOfBirth;
        $this->gender = $gender;
        $this->middleName = $middleName;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return DateTime
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * @param DateTime $dateOfBirth
     */
    public function setDateOfBirth(DateTime $dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;
    }

    /**
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }


}