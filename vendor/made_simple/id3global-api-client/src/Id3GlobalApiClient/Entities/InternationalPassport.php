<?php

namespace Id3GlobalApiClient\Entities;

use DateTime;
use Id3GlobalApiClient\Interfaces\IInternationalPassport;

class InternationalPassport implements IInternationalPassport
{

    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $number;

    /**
     * @var DateTime
     */
    private $expirationDate;

    /**
     * @param string $country
     * @param string $number
     * @param DateTime $expirationDate
     */
    public function __construct($country, $number, DateTime $expirationDate)
    {
        $this->country = $country;
        $this->number = $number;
        $this->expirationDate = $expirationDate;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @return DateTime
     */
    public function getExpirationDate()
    {
        return $this->expirationDate;
    }
}