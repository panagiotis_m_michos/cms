<?php

namespace Id3GlobalApiClient\Entities\Soap;

use Id3GlobalApiClient\Entities\Soap\Interfaces\IEntity;

class GlobalUKBirth implements IEntity
{
    /**
     * @var string
     */
    private $MothersMaidenName;

    /**
     * @var string
     */
    private $SurnameAtBirth;

    /**
     * @var string
     */
    private $TownOfBirth;

    /**
     * @var string
     */
    private $ProvinceOfBirth;

    /**
     * @var string
     */
    private $MunicipalityOfBirth;

    /**
     * @var string
     */
    private $Country;

    /**
     * @param string $MothersMaidenName
     * @param string $SurnameAtBirth
     * @param string $TownOfBirth
     * @param string $ProvinceOfBirth
     * @param string $MunicipalityOfBirth
     * @param string $Country
     */
    public function __construct($MothersMaidenName = NULL, $SurnameAtBirth = NULL, $TownOfBirth = NULL, $ProvinceOfBirth = NULL, $MunicipalityOfBirth = NULL, $Country = NULL)
    {
        $this->MothersMaidenName = $MothersMaidenName;
        $this->SurnameAtBirth = $SurnameAtBirth;
        $this->TownOfBirth = $TownOfBirth;
        $this->ProvinceOfBirth = $ProvinceOfBirth;
        $this->MunicipalityOfBirth = $MunicipalityOfBirth;
        $this->Country = $Country;
    }

    /**
     * @return string
     */
    public function getMothersMaidenName()
    {
        return $this->MothersMaidenName;
    }

    /**
     * @param string $MothersMaidenName
     */
    public function setMothersMaidenName($MothersMaidenName)
    {
        $this->MothersMaidenName = $MothersMaidenName;
    }

    /**
     * @return string
     */
    public function getSurnameAtBirth()
    {
        return $this->SurnameAtBirth;
    }

    /**
     * @param string $SurnameAtBirth
     */
    public function setSurnameAtBirth($SurnameAtBirth)
    {
        $this->SurnameAtBirth = $SurnameAtBirth;
    }

    /**
     * @return string
     */
    public function getTownOfBirth()
    {
        return $this->TownOfBirth;
    }

    /**
     * @param string $TownOfBirth
     */
    public function setTownOfBirth($TownOfBirth)
    {
        $this->TownOfBirth = $TownOfBirth;
    }

    /**
     * @return string
     */
    public function getProvinceOfBirth()
    {
        return $this->ProvinceOfBirth;
    }

    /**
     * @param string $ProvinceOfBirth
     */
    public function setProvinceOfBirth($ProvinceOfBirth)
    {
        $this->ProvinceOfBirth = $ProvinceOfBirth;
    }

    /**
     * @return string
     */
    public function getMunicipalityOfBirth()
    {
        return $this->MunicipalityOfBirth;
    }

    /**
     * @param string $MunicipalityOfBirth
     */
    public function setMunicipalityOfBirth($MunicipalityOfBirth)
    {
        $this->MunicipalityOfBirth = $MunicipalityOfBirth;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->Country;
    }

    /**
     * @param string $Country
     */
    public function setCountry($Country)
    {
        $this->Country = $Country;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $classVars = get_object_vars($this);
        foreach ($classVars as $var => $value) {
            if ($value instanceof IEntity) {
                $classVars[$var] = $value->toArray();
            } elseif ($value === NULL) {
                unset($classVars[$var]);
            }
        }

        return $classVars;
    }
}