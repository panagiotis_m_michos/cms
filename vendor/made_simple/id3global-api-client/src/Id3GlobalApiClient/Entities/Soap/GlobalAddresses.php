<?php

namespace Id3GlobalApiClient\Entities\Soap;

use Id3GlobalApiClient\Entities\Soap\Interfaces\IEntity;

class GlobalAddresses implements IEntity
{
    /**
     * @var GlobalAddress
     */
    private $CurrentAddress;

    /**
     * @param GlobalAddress $CurrentAddress
     */
    public function __construct(GlobalAddress $CurrentAddress = NULL)
    {
        $this->CurrentAddress = $CurrentAddress;
    }

    /**
     * @return GlobalAddress
     */
    public function getCurrentAddress()
    {
        return $this->CurrentAddress;
    }

    /**
     * @param GlobalAddress $CurrentAddress
     */
    public function setCurrentAddress($CurrentAddress)
    {
        $this->CurrentAddress = $CurrentAddress;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $classVars = get_object_vars($this);
        foreach ($classVars as $var => $value) {
            if ($value instanceof IEntity) {
                $classVars[$var] = $value->toArray();
            } elseif ($value === NULL) {
                unset($classVars[$var]);
            }
        }

        return $classVars;
    }
}