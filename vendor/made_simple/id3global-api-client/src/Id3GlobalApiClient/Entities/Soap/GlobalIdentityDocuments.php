<?php

namespace Id3GlobalApiClient\Entities\Soap;

use Id3GlobalApiClient\Entities\Soap\Interfaces\IEntity;

class GlobalIdentityDocuments implements IEntity
{
    /**
     * @var GlobalEuropeanIdentityCard
     */
    private $EuropeanIdentityCard;

    /**
     * @var GlobalUKData
     */
    private $UK;

    /**
     * @var GlobalInternationalPassport
     */
    private $InternationalPassport;

    /**
     * @param GlobalEuropeanIdentityCard $EuropeanIdentityCard
     * @param GlobalUKData $UK
     */
    public function __construct(GlobalEuropeanIdentityCard $EuropeanIdentityCard = NULL, GlobalUKData $UK = NULL)
    {
        $this->EuropeanIdentityCard = $EuropeanIdentityCard;
        $this->UK = $UK;
    }

    /**
     * @return GlobalInternationalPassport
     */
    public function getInternationalPassport()
    {
        return $this->InternationalPassport;
    }

    /**
     * @param GlobalInternationalPassport $InternationalPassport
     */
    public function setInternationalPassport(GlobalInternationalPassport $InternationalPassport)
    {
        $this->InternationalPassport = $InternationalPassport;
    }


    /**
     * @return GlobalEuropeanIdentityCard
     */
    public function getEuropeanIdentityCard()
    {
        return $this->EuropeanIdentityCard;
    }

    /**
     * @param GlobalEuropeanIdentityCard $EuropeanIdentityCard
     */
    public function setEuropeanIdentityCard($EuropeanIdentityCard)
    {
        $this->EuropeanIdentityCard = $EuropeanIdentityCard;
    }

    /**
     * @return GlobalUKData
     */
    public function getUK()
    {
        return $this->UK;
    }

    /**
     * @param GlobalUKData $UK
     */
    public function setUK($UK)
    {
        $this->UK = $UK;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $classVars = get_object_vars($this);
        foreach ($classVars as $var => $value) {
            if ($value instanceof IEntity) {
                $classVars[$var] = $value->toArray();
            } elseif ($value === NULL) {
                unset($classVars[$var]);
            }
        }

        return $classVars;
    }
}