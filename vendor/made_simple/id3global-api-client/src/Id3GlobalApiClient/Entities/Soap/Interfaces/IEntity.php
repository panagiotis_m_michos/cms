<?php

namespace Id3GlobalApiClient\Entities\Soap\Interfaces;

interface IEntity
{
    /**
     * @return array
     */
    public function toArray();
}