<?php

namespace Id3GlobalApiClient\Entities\Soap;

use Id3GlobalApiClient\Entities\Soap\Interfaces\IEntity;

class GlobalUKPassport implements IEntity
{
    /**
     * @var string
     */
    private $Number;

    /**
     * @var int
     */
    private $ExpiryDay;

    /**
     * @var int
     */
    private $ExpiryMonth;

    /**
     * @var int
     */
    private $ExpiryYear;

    /**
     * @param string $Number
     * @param int $ExpiryDay
     * @param int $ExpiryMonth
     * @param int $ExpiryYear
     */
    public  function  __construct($Number = NULL, $ExpiryDay = NULL, $ExpiryMonth = NULL, $ExpiryYear = NULL)
    {
        $this->Number = $Number;
        $this->ExpiryDay = $ExpiryDay;
        $this->ExpiryMonth = $ExpiryMonth;
        $this->ExpiryYear = $ExpiryYear;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->Number;
    }

    /**
     * @param string $Number
     */
    public function setNumber($Number)
    {
        $this->Number = $Number;
    }

    /**
     * @return int
     */
    public function getExpiryDay()
    {
        return $this->ExpiryDay;
    }

    /**
     * @param int $ExpiryDay
     */
    public function setExpiryDay($ExpiryDay)
    {
        $this->ExpiryDay = $ExpiryDay;
    }

    /**
     * @return int
     */
    public function getExpiryMonth()
    {
        return $this->ExpiryMonth;
    }

    /**
     * @param int $ExpiryMonth
     */
    public function setExpiryMonth($ExpiryMonth)
    {
        $this->ExpiryMonth = $ExpiryMonth;
    }

    /**
     * @return int
     */
    public function getExpiryYear()
    {
        return $this->ExpiryYear;
    }

    /**
     * @param int $ExpiryYear
     */
    public function setExpiryYear($ExpiryYear)
    {
        $this->ExpiryYear = $ExpiryYear;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $classVars = get_object_vars($this);
        foreach ($classVars as $var => $value) {
            if ($value instanceof IEntity) {
                $classVars[$var] = $value->toArray();
            } elseif ($value === NULL) {
                unset($classVars[$var]);
            }
        }

        return $classVars;
    }
}