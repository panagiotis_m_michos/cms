<?php

namespace Id3GlobalApiClient\Entities\Soap;

use Id3GlobalApiClient\Entities\Soap\Interfaces\IEntity;

class GlobalUKData implements IEntity
{
    /**
     * @var GlobalUKPassport
     */
    private $Passport;

    /**
     * @var GlobalUKDrivingLicence
     */
    private $DrivingLicence;

    /**
     * @param GlobalUKPassport $Passport
     * @param GlobalUKDrivingLicence $DrivingLicence
     */
    public function __construct(GlobalUKPassport $Passport = NULL, GlobalUKDrivingLicence $DrivingLicence = NULL)
    {
        $this->Passport = $Passport;
        $this->DrivingLicence = $DrivingLicence;
    }

    /**
     * @return GlobalUKPassport
     */
    public function getPassport()
    {
        return $this->Passport;
    }

    /**
     * @param GlobalUKPassport $Passport
     */
    public function setPassport($Passport)
    {
        $this->Passport = $Passport;
    }

    /**
     * @return GlobalUKDrivingLicence
     */
    public function getDrivingLicence()
    {
        return $this->DrivingLicence;
    }

    /**
     * @param GlobalUKDrivingLicence $DrivingLicence
     */
    public function setDrivingLicence($DrivingLicence)
    {
        $this->DrivingLicence = $DrivingLicence;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $classVars = get_object_vars($this);
        foreach ($classVars as $var => $value) {
            if ($value instanceof IEntity) {
                $classVars[$var] = $value->toArray();
            } elseif ($value === NULL) {
                unset($classVars[$var]);
            }
        }

        return $classVars;
    }
}