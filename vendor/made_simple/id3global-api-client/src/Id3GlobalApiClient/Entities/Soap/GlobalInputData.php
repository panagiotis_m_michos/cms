<?php

namespace Id3GlobalApiClient\Entities\Soap;

use Id3GlobalApiClient\Entities\Soap\Interfaces\IEntity;

class GlobalInputData implements IEntity
{
    /**
     * @var GlobalPersonal
     */
    private $Personal;

    /**
     * @var GlobalAddresses
     */
    private $Addresses;

    /**
     * @var GlobalIdentityDocuments
     */
    private $IdentityDocuments;

    /**
     * @param GlobalPersonal $Personal
     * @param GlobalAddresses $Addresses
     * @param GlobalIdentityDocuments $IdentityDocuments
     */
    public function __construct(GlobalPersonal $Personal = NULL, GlobalAddresses $Addresses = NULL, GlobalIdentityDocuments $IdentityDocuments = NULL)
    {
        $this->Personal = $Personal;
        $this->Addresses = $Addresses;
        $this->IdentityDocuments = $IdentityDocuments;
    }

    /**
     * @return GlobalPersonal
     */
    public function getPersonal()
    {
        return $this->Personal;
    }

    /**
     * @param GlobalPersonal $Personal
     */
    public function setPersonal($Personal)
    {
        $this->Personal = $Personal;
    }

    /**
     * @return GlobalAddresses
     */
    public function getAddresses()
    {
        return $this->Addresses;
    }

    /**
     * @param GlobalAddresses $Addresses
     */
    public function setAddresses($Addresses)
    {
        $this->Addresses = $Addresses;
    }

    /**
     * @return GlobalIdentityDocuments
     */
    public function getIdentityDocuments()
    {
        return $this->IdentityDocuments;
    }

    /**
     * @param GlobalIdentityDocuments $IdentityDocuments
     */
    public function setIdentityDocuments($IdentityDocuments)
    {
        $this->IdentityDocuments = $IdentityDocuments;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $classVars = get_object_vars($this);
        foreach ($classVars as $var => $value) {
            if ($value instanceof IEntity) {
                $classVars[$var] = $value->toArray();
            } elseif ($value === NULL) {
                unset($classVars[$var]);
            }
        }

        return $classVars;
    }
}