<?php

namespace Id3GlobalApiClient\Entities\Soap;

use Id3GlobalApiClient\Entities\Soap\Interfaces\IEntity;

class GlobalAddress implements IEntity
{
    /**
     * @var string
     */
    private $Country;

    /**
     * @var string
     */
    private $Street;

    /**
     * @var string
     */
    private $SubStreet;

    /**
     * @var string
     */
    private $City;

    /**
     * @var string
     */
    private $SubCity;

    /**
     * @var string
     */
    private $StateDistrict;

    /**
     * @var string
     */
    private $POBox;

    /**
     * @var string
     */
    private $Region;

    /**
     * @var string
     */
    private $Principality;

    /**
     * @var string
     */
    private $ZipPostcode;

    /**
     * @var string
     */
    private $DpsZipPlus;

    /**
     * @var string
     */
    private $CedexMailsort;

    /**
     * @var string
     */
    private $Department;

    /**
     * @var string
     */
    private $Company;

    /**
     * @var string
     */
    private $Building;

    /**
     * @var string
     */
    private $SubBuilding;

    /**
     * @var string
     */
    private $Premise;

    /**
     * @var string
     */
    private $AddressLine1;

    /**
     * @var string
     */
    private $AddressLine2;

    /**
     * @var string
     */
    private $AddressLine3;

    /**
     * @var string
     */
    private $AddressLine4;

    /**
     * @var string
     */
    private $AddressLine5;

    /**
     * @var string
     */
    private $AddressLine6;

    /**
     * @param string $Country
     * @param string $Street
     * @param string $SubStreet
     * @param string $City
     * @param string $SubCity
     * @param string $StateDistrict
     * @param string $POBox
     * @param string $Region
     * @param string $Principality
     * @param string $ZipPostcode
     * @param string $DpsZipPlus
     * @param string $CedexMailsort
     * @param string $Department
     * @param string $Company
     * @param string $Building
     * @param string $SubBuilding
     * @param string $Premise
     * @param string $AddressLine1
     * @param string $AddressLine2
     * @param string $AddressLine3
     * @param string $AddressLine4
     * @param string $AddressLine5
     * @param string $AddressLine6
     */
    public function __construct($Country = NULL, $Street = NULL, $SubStreet = NULL, $City = NULL, $SubCity = NULL, $StateDistrict = NULL, $POBox = NULL, $Region = NULL, $Principality = NULL, $ZipPostcode = NULL, $DpsZipPlus = NULL, $CedexMailsort = NULL, $Department = NULL, $Company = NULL, $Building = NULL, $SubBuilding = NULL, $Premise = NULL, $AddressLine1 = NULL, $AddressLine2 = NULL, $AddressLine3 = NULL, $AddressLine4 = NULL, $AddressLine5 = NULL, $AddressLine6 = NULL)
    {
        $this->Country = $Country;
        $this->Street = $Street;
        $this->SubStreet = $SubStreet;
        $this->City = $City;
        $this->SubCity = $SubCity;
        $this->StateDistrict = $StateDistrict;
        $this->POBox = $POBox;
        $this->Region = $Region;
        $this->Principality = $Principality;
        $this->ZipPostcode = $ZipPostcode;
        $this->DpsZipPlus = $DpsZipPlus;
        $this->CedexMailsort = $CedexMailsort;
        $this->Department = $Department;
        $this->Company = $Company;
        $this->Building = $Building;
        $this->SubBuilding = $SubBuilding;
        $this->Premise = $Premise;
        $this->AddressLine1 = $AddressLine1;
        $this->AddressLine2 = $AddressLine2;
        $this->AddressLine3 = $AddressLine3;
        $this->AddressLine4 = $AddressLine4;
        $this->AddressLine5 = $AddressLine5;
        $this->AddressLine6 = $AddressLine6;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->Country;
    }

    /**
     * @param string $Country
     */
    public function setCountry($Country)
    {
        $this->Country = $Country;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->Street;
    }

    /**
     * @param string $Street
     */
    public function setStreet($Street)
    {
        $this->Street = $Street;
    }

    /**
     * @return string
     */
    public function getSubStreet()
    {
        return $this->SubStreet;
    }

    /**
     * @param string $SubStreet
     */
    public function setSubStreet($SubStreet)
    {
        $this->SubStreet = $SubStreet;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->City;
    }

    /**
     * @param string $City
     */
    public function setCity($City)
    {
        $this->City = $City;
    }

    /**
     * @return string
     */
    public function getSubCity()
    {
        return $this->SubCity;
    }

    /**
     * @param string $SubCity
     */
    public function setSubCity($SubCity)
    {
        $this->SubCity = $SubCity;
    }

    /**
     * @return string
     */
    public function getStateDistrict()
    {
        return $this->StateDistrict;
    }

    /**
     * @param string $StateDistrict
     */
    public function setStateDistrict($StateDistrict)
    {
        $this->StateDistrict = $StateDistrict;
    }

    /**
     * @return string
     */
    public function getPOBox()
    {
        return $this->POBox;
    }

    /**
     * @param string $POBox
     */
    public function setPOBox($POBox)
    {
        $this->POBox = $POBox;
    }

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->Region;
    }

    /**
     * @param string $Region
     */
    public function setRegion($Region)
    {
        $this->Region = $Region;
    }

    /**
     * @return string
     */
    public function getPrincipality()
    {
        return $this->Principality;
    }

    /**
     * @param string $Principality
     */
    public function setPrincipality($Principality)
    {
        $this->Principality = $Principality;
    }

    /**
     * @return string
     */
    public function getZipPostcode()
    {
        return $this->ZipPostcode;
    }

    /**
     * @param string $ZipPostcode
     */
    public function setZipPostcode($ZipPostcode)
    {
        $this->ZipPostcode = $ZipPostcode;
    }

    /**
     * @return string
     */
    public function getDpsZipPlus()
    {
        return $this->DpsZipPlus;
    }

    /**
     * @param string $DpsZipPlus
     */
    public function setDpsZipPlus($DpsZipPlus)
    {
        $this->DpsZipPlus = $DpsZipPlus;
    }

    /**
     * @return string
     */
    public function getCedexMailsort()
    {
        return $this->CedexMailsort;
    }

    /**
     * @param string $CedexMailsort
     */
    public function setCedexMailsort($CedexMailsort)
    {
        $this->CedexMailsort = $CedexMailsort;
    }

    /**
     * @return string
     */
    public function getDepartment()
    {
        return $this->Department;
    }

    /**
     * @param string $Department
     */
    public function setDepartment($Department)
    {
        $this->Department = $Department;
    }

    /**
     * @return string
     */
    public function getCompany()
    {
        return $this->Company;
    }

    /**
     * @param string $Company
     */
    public function setCompany($Company)
    {
        $this->Company = $Company;
    }

    /**
     * @return string
     */
    public function getBuilding()
    {
        return $this->Building;
    }

    /**
     * @param string $Building
     */
    public function setBuilding($Building)
    {
        $this->Building = $Building;
    }

    /**
     * @return string
     */
    public function getSubBuilding()
    {
        return $this->SubBuilding;
    }

    /**
     * @param string $SubBuilding
     */
    public function setSubBuilding($SubBuilding)
    {
        $this->SubBuilding = $SubBuilding;
    }

    /**
     * @return string
     */
    public function getPremise()
    {
        return $this->Premise;
    }

    /**
     * @param string $Premise
     */
    public function setPremise($Premise)
    {
        $this->Premise = $Premise;
    }

    /**
     * @return string
     */
    public function getAddressLine1()
    {
        return $this->AddressLine1;
    }

    /**
     * @param string $AddressLine1
     */
    public function setAddressLine1($AddressLine1)
    {
        $this->AddressLine1 = $AddressLine1;
    }

    /**
     * @return string
     */
    public function getAddressLine2()
    {
        return $this->AddressLine2;
    }

    /**
     * @param string $AddressLine2
     */
    public function setAddressLine2($AddressLine2)
    {
        $this->AddressLine2 = $AddressLine2;
    }

    /**
     * @return string
     */
    public function getAddressLine3()
    {
        return $this->AddressLine3;
    }

    /**
     * @param string $AddressLine3
     */
    public function setAddressLine3($AddressLine3)
    {
        $this->AddressLine3 = $AddressLine3;
    }

    /**
     * @return string
     */
    public function getAddressLine4()
    {
        return $this->AddressLine4;
    }

    /**
     * @param string $AddressLine4
     */
    public function setAddressLine4($AddressLine4)
    {
        $this->AddressLine4 = $AddressLine4;
    }

    /**
     * @return string
     */
    public function getAddressLine5()
    {
        return $this->AddressLine5;
    }

    /**
     * @param string $AddressLine5
     */
    public function setAddressLine5($AddressLine5)
    {
        $this->AddressLine5 = $AddressLine5;
    }

    /**
     * @return string
     */
    public function getAddressLine6()
    {
        return $this->AddressLine6;
    }

    /**
     * @param string $AddressLine6
     */
    public function setAddressLine6($AddressLine6)
    {
        $this->AddressLine6 = $AddressLine6;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $classVars = get_object_vars($this);
        foreach ($classVars as $var => $value) {
            if ($value instanceof IEntity) {
                $classVars[$var] = $value->toArray();
            } elseif ($value === NULL) {
                unset($classVars[$var]);
            }
        }

        return $classVars;
    }
}