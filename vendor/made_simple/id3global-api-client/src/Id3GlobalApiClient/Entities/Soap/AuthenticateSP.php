<?php

namespace Id3GlobalApiClient\Entities\Soap;

use Id3GlobalApiClient\Entities\Soap\Interfaces\IEntity;

class AuthenticateSP implements IEntity
{
    /**
     * @var GlobalProfileIDVersion
     */
    private $ProfileIDVersion;

    /**
     * @var string
     */
    private $CustomerReference;

    /**
     * @var GlobalInputData
     */
    private $InputData;

    /**
     * @param GlobalProfileIDVersion $ProfileIDVersion
     * @param string $CustomerReference
     * @param GlobalInputData $InputData
     */
    public function __construct(GlobalProfileIDVersion $ProfileIDVersion, $CustomerReference, GlobalInputData $InputData)
    {
        $this->ProfileIDVersion = $ProfileIDVersion;
        $this->CustomerReference = $CustomerReference;
        $this->InputData = $InputData;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $classVars = get_object_vars($this);
        foreach ($classVars as $var => $value) {
            if ($value instanceof IEntity) {
                $classVars[$var] = $value->toArray();
            } elseif ($value === NULL) {
                unset($classVars[$var]);
            }
        }

        return $classVars;
    }
}