<?php

namespace Id3GlobalApiClient\Entities\Soap;

use Id3GlobalApiClient\Entities\Soap\Interfaces\IEntity;

class AddressDetails implements IEntity
{
    /**
     * @var array
     */
    private $AddressLines;

    /**
     * @var string
     */
    private $ZipPostcode;

    /**
     * @return array
     */
    public function getAddressLines()
    {
        return $this->AddressLines;
    }

    /**
     * @param array $AddressLines
     */
    public function setAddressLines($AddressLines)
    {
        $this->AddressLines = $AddressLines;
    }

    /**
     * @return string
     */
    public function getZipPostcode()
    {
        return $this->ZipPostcode;
    }

    /**
     * @param string $ZipPostcode
     */
    public function setZipPostcode($ZipPostcode)
    {
        $this->ZipPostcode = $ZipPostcode;
    }

    /**
     * @param array $AddressLines
     * @param string $ZipPostcode
     */
    public function __construct(array $AddressLines = NULL, $ZipPostcode = NULL)
    {
        $this->AddressLines = $AddressLines;
        $this->ZipPostcode = $ZipPostcode;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $classVars = get_object_vars($this);
        foreach ($classVars as $var => $value) {
            if ($value instanceof IEntity) {
                $classVars[$var] = $value->toArray();
            } elseif ($value === NULL) {
                unset($classVars[$var]);
            }
        }

        return $classVars;
    }
}