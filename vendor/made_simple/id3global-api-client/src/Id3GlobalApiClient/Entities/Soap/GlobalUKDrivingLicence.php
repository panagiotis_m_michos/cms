<?php

namespace Id3GlobalApiClient\Entities\Soap;

use Id3GlobalApiClient\Entities\Soap\Interfaces\IEntity;

class GlobalUKDrivingLicence implements IEntity
{
    /**
     * @var string
     */
    private $Number;

    /**
     * @var string
     */
    private $MailSort;

    /**
     * @var string
     */
    private $Postcode;

    /**
     * @var string
     */
    private $Microfiche;

    /**
     * @var int
     */
    private $IssueDay;

    /**
     * @var int
     */
    private $IssueMonth;

    /**
     * @var int
     */
    private $IssueYear;

    /**
     * @var int
     */
    private $IssueNumber;

    /**
     * @var int
     */
    private $ExpiryDay;

    /**
     * @var int
     */
    private $ExpiryMonth;

    /**
     * @var int
     */
    private $ExpiryYear;

    /**
     * @param string $Number
     * @param string $MailSort
     * @param string $Postcode
     * @param string $Microfiche
     * @param int $IssueDay
     * @param int $IssueMonth
     * @param int $IssueYear
     * @param int $IssueNumber
     * @param int $ExpiryDay
     * @param int $ExpiryMonth
     * @param int $ExpiryYear
     */
    public function __construct($Number = NULL, $MailSort = NULL, $Postcode = NULL, $Microfiche = NULL, $IssueDay = NULL, $IssueMonth = NULL, $IssueYear = NULL, $IssueNumber = NULL, $ExpiryDay = NULL, $ExpiryMonth = NULL, $ExpiryYear = NULL)
    {
        $this->Number = $Number;
        $this->MailSort = $MailSort;
        $this->Postcode = $Postcode;
        $this->Microfiche = $Microfiche;
        $this->IssueDay = $IssueDay;
        $this->IssueMonth = $IssueMonth;
        $this->IssueYear = $IssueYear;
        $this->IssueNumber = $IssueNumber;
        $this->ExpiryDay = $ExpiryDay;
        $this->ExpiryMonth = $ExpiryMonth;
        $this->ExpiryYear = $ExpiryYear;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->Number;
    }

    /**
     * @param string $Number
     */
    public function setNumber($Number)
    {
        $this->Number = $Number;
    }

    /**
     * @return string
     */
    public function getMailSort()
    {
        return $this->MailSort;
    }

    /**
     * @param string $MailSort
     */
    public function setMailSort($MailSort)
    {
        $this->MailSort = $MailSort;
    }

    /**
     * @return string
     */
    public function getPostcode()
    {
        return $this->Postcode;
    }

    /**
     * @param string $Postcode
     */
    public function setPostcode($Postcode)
    {
        $this->Postcode = $Postcode;
    }

    /**
     * @return string
     */
    public function getMicrofiche()
    {
        return $this->Microfiche;
    }

    /**
     * @param string $Microfiche
     */
    public function setMicrofiche($Microfiche)
    {
        $this->Microfiche = $Microfiche;
    }

    /**
     * @return int
     */
    public function getIssueDay()
    {
        return $this->IssueDay;
    }

    /**
     * @param int $IssueDay
     */
    public function setIssueDay($IssueDay)
    {
        $this->IssueDay = $IssueDay;
    }

    /**
     * @return int
     */
    public function getIssueMonth()
    {
        return $this->IssueMonth;
    }

    /**
     * @param int $IssueMonth
     */
    public function setIssueMonth($IssueMonth)
    {
        $this->IssueMonth = $IssueMonth;
    }

    /**
     * @return int
     */
    public function getIssueYear()
    {
        return $this->IssueYear;
    }

    /**
     * @param int $IssueYear
     */
    public function setIssueYear($IssueYear)
    {
        $this->IssueYear = $IssueYear;
    }

    /**
     * @return int
     */
    public function getIssueNumber()
    {
        return $this->IssueNumber;
    }

    /**
     * @param int $IssueNumber
     */
    public function setIssueNumber($IssueNumber)
    {
        $this->IssueNumber = $IssueNumber;
    }

    /**
     * @return int
     */
    public function getExpiryDay()
    {
        return $this->ExpiryDay;
    }

    /**
     * @param int $ExpiryDay
     */
    public function setExpiryDay($ExpiryDay)
    {
        $this->ExpiryDay = $ExpiryDay;
    }

    /**
     * @return int
     */
    public function getExpiryMonth()
    {
        return $this->ExpiryMonth;
    }

    /**
     * @param int $ExpiryMonth
     */
    public function setExpiryMonth($ExpiryMonth)
    {
        $this->ExpiryMonth = $ExpiryMonth;
    }

    /**
     * @return int
     */
    public function getExpiryYear()
    {
        return $this->ExpiryYear;
    }

    /**
     * @param int $ExpiryYear
     */
    public function setExpiryYear($ExpiryYear)
    {
        $this->ExpiryYear = $ExpiryYear;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $classVars = get_object_vars($this);
        foreach ($classVars as $var => $value) {
            if ($value instanceof IEntity) {
                $classVars[$var] = $value->toArray();
            } elseif ($value === NULL) {
                unset($classVars[$var]);
            }
        }

        return $classVars;
    }
}