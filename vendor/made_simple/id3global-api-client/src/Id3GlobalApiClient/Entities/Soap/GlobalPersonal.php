<?php

namespace Id3GlobalApiClient\Entities\Soap;

use Id3GlobalApiClient\Entities\Soap\Interfaces\IEntity;

class GlobalPersonal implements IEntity
{
    /**
     * @var GlobalPersonalDetails
     */
    private $PersonalDetails;

    /**
     * @param GlobalPersonalDetails $PersonalDetails
     */
    public function __construct(GlobalPersonalDetails $PersonalDetails = NULL)
    {
        $this->PersonalDetails = $PersonalDetails;
    }

    /**
     * @return GlobalPersonalDetails
     */
    public function getPersonalDetails()
    {
        return $this->PersonalDetails;
    }

    /**
     * @param GlobalPersonalDetails $PersonalDetails
     */
    public function setPersonalDetails($PersonalDetails)
    {
        $this->PersonalDetails = $PersonalDetails;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $classVars = get_object_vars($this);
        foreach ($classVars as $var => $value) {
            if ($value instanceof IEntity) {
                $classVars[$var] = $value->toArray();
            } elseif ($value === NULL) {
                unset($classVars[$var]);
            }
        }

        return $classVars;
    }
}