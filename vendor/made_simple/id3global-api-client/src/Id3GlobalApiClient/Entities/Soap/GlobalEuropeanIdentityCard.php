<?php

namespace Id3GlobalApiClient\Entities\Soap;

use Id3GlobalApiClient\Entities\Soap\Interfaces\IEntity;

class GlobalEuropeanIdentityCard implements IEntity
{
    /**
     * @var string
     */
    private $Line1;

    /**
     * @var string
     */
    private $Line2;

    /**
     * @var string
     */
    private $Line3;

    /**
     * @var int
     */
    private $ExpiryDay;

    /**
     * @var int
     */
    private $ExpiryMonth;

    /**
     * @var int
     */
    private $ExpiryYear;

    /**
     * @var string
     */
    private $CountryOfNationality;

    /**
     * @var string
     */
    private $CountryOfIssue;

    /**
     * @param string $Line1
     * @param string $Line2
     * @param string $Line3
     * @param int $ExpiryDay
     * @param int $ExpiryMonth
     * @param int $ExpiryYear
     * @param string $CountryOfNationality
     * @param string $CountryOfIssue
     */
    public function __construct($Line1 = NULL, $Line2 = NULL, $Line3 = NULL, $ExpiryDay = NULL, $ExpiryMonth = NULL, $ExpiryYear = NULL, $CountryOfNationality = NULL, $CountryOfIssue = NULL)
    {

        $this->Line1 = $Line1;
        $this->Line2 = $Line2;
        $this->Line3 = $Line3;
        $this->ExpiryDay = $ExpiryDay;
        $this->ExpiryMonth = $ExpiryMonth;
        $this->ExpiryYear = $ExpiryYear;
        $this->CountryOfNationality = $CountryOfNationality;
        $this->CountryOfIssue = $CountryOfIssue;
    }

    /**
     * @return string
     */
    public function getLine1()
    {
        return $this->Line1;
    }

    /**
     * @param string $Line1
     */
    public function setLine1($Line1)
    {
        $this->Line1 = $Line1;
    }

    /**
     * @return string
     */
    public function getLine2()
    {
        return $this->Line2;
    }

    /**
     * @param string $Line2
     */
    public function setLine2($Line2)
    {
        $this->Line2 = $Line2;
    }

    /**
     * @return string
     */
    public function getLine3()
    {
        return $this->Line3;
    }

    /**
     * @param string $Line3
     */
    public function setLine3($Line3)
    {
        $this->Line3 = $Line3;
    }

    /**
     * @return int
     */
    public function getExpiryDay()
    {
        return $this->ExpiryDay;
    }

    /**
     * @param int $ExpiryDay
     */
    public function setExpiryDay($ExpiryDay)
    {
        $this->ExpiryDay = $ExpiryDay;
    }

    /**
     * @return int
     */
    public function getExpiryMonth()
    {
        return $this->ExpiryMonth;
    }

    /**
     * @param int $ExpiryMonth
     */
    public function setExpiryMonth($ExpiryMonth)
    {
        $this->ExpiryMonth = $ExpiryMonth;
    }

    /**
     * @return int
     */
    public function getExpiryYear()
    {
        return $this->ExpiryYear;
    }

    /**
     * @param int $ExpiryYear
     */
    public function setExpiryYear($ExpiryYear)
    {
        $this->ExpiryYear = $ExpiryYear;
    }

    /**
     * @return string
     */
    public function getCountryOfNationality()
    {
        return $this->CountryOfNationality;
    }

    /**
     * @param string $CountryOfNationality
     */
    public function setCountryOfNationality($CountryOfNationality)
    {
        $this->CountryOfNationality = $CountryOfNationality;
    }

    /**
     * @return string
     */
    public function getCountryOfIssue()
    {
        return $this->CountryOfIssue;
    }

    /**
     * @param string $CountryOfIssue
     */
    public function setCountryOfIssue($CountryOfIssue)
    {
        $this->CountryOfIssue = $CountryOfIssue;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $classVars = get_object_vars($this);
        foreach ($classVars as $var => $value) {
            if ($value instanceof IEntity) {
                $classVars[$var] = $value->toArray();
            } elseif ($value === NULL) {
                unset($classVars[$var]);
            }
        }

        return $classVars;
    }
}