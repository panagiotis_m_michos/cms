<?php

namespace Id3GlobalApiClient\Entities\Soap;

use Id3GlobalApiClient\Entities\Soap\Interfaces\IEntity;

class GlobalPersonalDetails implements IEntity
{
    /**
     * @var string
     */
    private $Title;

    /**
     * @var string
     */
    private $Forename;

    /**
     * @var string
     */
    private $MiddleName;

    /**
     * @var string
     */
    private $Surname;

    /**
     * @var string
     */
    private $Gender;

    /**
     * @var string
     */
    private $DOBDay;

    /**
     * @var string
     */
    private $DOBMonth;

    /**
     * @var string
     */
    private $DOBYear;

    /**
     * @var GlobalUKBirth
     */
    private $Birth;

    /**
     * @var string
     */
    private $CountryOfBirth;

    /**
     * @var string
     */
    private $SecondSurname;

    /**
     * @var array
     */
    private $AdditionalMiddleNames;

    /**
     * @param string $Title
     * @param string $Forename
     * @param string $MiddleName
     * @param string $Surname
     * @param string $Gender
     * @param string $DOBDay
     * @param string $DOBMonth
     * @param string $DOBYear
     * @param GlobalUKBirth $Birth
     * @param string $CountryOfBirth
     * @param string $SecondSurname
     * @param array $AdditionalMiddleNames
     */
    public function __construct($Title = NULL, $Forename = NULL, $MiddleName = NULL, $Surname = NULL, $Gender = NULL, $DOBDay = NULL, $DOBMonth = NULL, $DOBYear = NULL, GlobalUKBirth $Birth = NULL, $CountryOfBirth = NULL, $SecondSurname = NULL, array $AdditionalMiddleNames = NULL)
    {
        $this->Title = $Title;
        $this->Forename = $Forename;
        $this->MiddleName = $MiddleName;
        $this->Surname = $Surname;
        $this->Gender = $Gender;
        $this->DOBDay = $DOBDay;
        $this->DOBMonth = $DOBMonth;
        $this->DOBYear = $DOBYear;
        $this->Birth = $Birth;
        $this->CountryOfBirth = $CountryOfBirth;
        $this->SecondSurname = $SecondSurname;
        $this->AdditionalMiddleNames = $AdditionalMiddleNames;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->Title;
    }

    /**
     * @param string $Title
     */
    public function setTitle($Title)
    {
        $this->Title = $Title;
    }

    /**
     * @return string
     */
    public function getForename()
    {
        return $this->Forename;
    }

    /**
     * @param string $Forename
     */
    public function setForename($Forename)
    {
        $this->Forename = $Forename;
    }

    /**
     * @return string
     */
    public function getMiddleName()
    {
        return $this->MiddleName;
    }

    /**
     * @param string $MiddleName
     */
    public function setMiddleName($MiddleName)
    {
        $this->MiddleName = $MiddleName;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->Surname;
    }

    /**
     * @param string $Surname
     */
    public function setSurname($Surname)
    {
        $this->Surname = $Surname;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->Gender;
    }

    /**
     * @param string $Gender
     */
    public function setGender($Gender)
    {
        $this->Gender = $Gender;
    }

    /**
     * @return string
     */
    public function getDOBDay()
    {
        return $this->DOBDay;
    }

    /**
     * @param string $DOBDay
     */
    public function setDOBDay($DOBDay)
    {
        $this->DOBDay = $DOBDay;
    }

    /**
     * @return string
     */
    public function getDOBMonth()
    {
        return $this->DOBMonth;
    }

    /**
     * @param string $DOBMonth
     */
    public function setDOBMonth($DOBMonth)
    {
        $this->DOBMonth = $DOBMonth;
    }

    /**
     * @return string
     */
    public function getDOBYear()
    {
        return $this->DOBYear;
    }

    /**
     * @param string $DOBYear
     */
    public function setDOBYear($DOBYear)
    {
        $this->DOBYear = $DOBYear;
    }

    /**
     * @return GlobalUKBirth
     */
    public function getBirth()
    {
        return $this->Birth;
    }

    /**
     * @param GlobalUKBirth $Birth
     */
    public function setBirth($Birth)
    {
        $this->Birth = $Birth;
    }

    /**
     * @return string
     */
    public function getCountryOfBirth()
    {
        return $this->CountryOfBirth;
    }

    /**
     * @param string $CountryOfBirth
     */
    public function setCountryOfBirth($CountryOfBirth)
    {
        $this->CountryOfBirth = $CountryOfBirth;
    }

    /**
     * @return string
     */
    public function getSecondSurname()
    {
        return $this->SecondSurname;
    }

    /**
     * @param string $SecondSurname
     */
    public function setSecondSurname($SecondSurname)
    {
        $this->SecondSurname = $SecondSurname;
    }

    /**
     * @return array
     */
    public function getAdditionalMiddleNames()
    {
        return $this->AdditionalMiddleNames;
    }

    /**
     * @param array $AdditionalMiddleNames
     */
    public function setAdditionalMiddleNames($AdditionalMiddleNames)
    {
        $this->AdditionalMiddleNames = $AdditionalMiddleNames;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $classVars = get_object_vars($this);
        foreach ($classVars as $var => $value) {
            if ($value instanceof IEntity) {
                $classVars[$var] = $value->toArray();
            } elseif ($value === NULL) {
                unset($classVars[$var]);
            }
        }

        return $classVars;
    }
}