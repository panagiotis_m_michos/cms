<?php

namespace Id3GlobalApiClient\Entities;

use DateTime;
use Id3GlobalApiClient\Interfaces\IPassport;
use Id3GlobalApiClient\Interfaces\IUkPassport;

class UkPassport implements IUkPassport
{
    /**
     * @var NULL|string
     */
    private $number;

    /**
     * @var NULL|DateTime
     */
    private $expirationDate;

    /**
     * @param string $number
     * @param DateTime $expirationDate
     */
    public function __construct($number = NULL, DateTime $expirationDate = NULL)
    {
        $this->number = $number;
        $this->expirationDate = $expirationDate;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return DateTime
     */
    public function getExpirationDate()
    {
        return $this->expirationDate;
    }

    /**
     * @param DateTime $expirationDate
     */
    public function setExpirationDate(DateTime $expirationDate)
    {
        $this->expirationDate = $expirationDate;
    }
}