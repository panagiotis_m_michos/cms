<?php

namespace Id3GlobalApiClient\Interfaces;

interface IAddress
{
    /**
     * @return NULL|string
     */
    public function getAddress1();

    /**
     * @return NULL|string
     */
    public function getAddress2();

    /**
     * @return NULL|string
     */
    public function getAddress3();

    /**
     * @return NULL|string
     */
    public function getCity();

    /**
     * @return NULL|string
     */
    public function getCounty();

    /**
     * @return NULL|string
     */
    public function getPostcode();

    /**
     * @return NULL|string
     */
    public function getCountry();
}