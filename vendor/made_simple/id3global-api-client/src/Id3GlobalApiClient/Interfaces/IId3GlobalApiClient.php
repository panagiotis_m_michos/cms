<?php

namespace Id3GlobalApiClient\Interfaces;

interface IId3GlobalApiClient
{
    /**
     * @param IPersonalDetails $personalDetails
     * @param IAddress $address
     * @param IIdDocument $idDocument
     * @return IAuthenticationResult
     */
    public function authenticateDocument(IPersonalDetails $personalDetails, IAddress $address = NULL, IIdDocument $idDocument);


    public function authenticateAddress(IPersonalDetails $personalDetails, IAddress $address);
}