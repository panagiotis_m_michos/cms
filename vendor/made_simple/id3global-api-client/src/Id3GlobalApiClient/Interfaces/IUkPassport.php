<?php

namespace Id3GlobalApiClient\Interfaces;

use DateTime;

interface IUkPassport extends IIdDocument
{
    /**
     * @return string
     */
    public function getNumber();

    /**
     * @return DateTime
     */
    public function getExpirationDate();
}