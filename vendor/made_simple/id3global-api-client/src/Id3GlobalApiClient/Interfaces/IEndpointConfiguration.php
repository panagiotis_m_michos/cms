<?php

namespace Id3GlobalApiClient\Interfaces;

use Id3GlobalApiClient\Entities\Soap\GlobalProfileIDVersion;

interface IEndpointConfiguration
{
    /**
     * @return mixed
     */
    public function getVersion();

    /**
     * @return mixed
     */
    public function getId();

    /**
     * @return GlobalProfileIDVersion
     */
    public function getProfileVersionId();
}