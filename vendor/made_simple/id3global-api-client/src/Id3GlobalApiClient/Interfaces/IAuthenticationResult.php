<?php

namespace Id3GlobalApiClient\Interfaces;

interface IAuthenticationResult
{
    /**
     * @return integer
     */
    public function getScore();
}