<?php

namespace Id3GlobalApiClient\Interfaces;

use DateTime;

interface IPersonalDetails
{

    /**
     * @return string
     */
    public function getFirstName();

    /**
     * @return string
     */
    public function getMiddleName();

    /**
     * @return string
     */
    public function getLastName();

    /**
     * @return string
     */
    public function getGender();

    /**
     * @return DateTime
     */
    public function getDateOfBirth();

}