<?php

namespace Id3GlobalApiClient\Interfaces;

use DateTime;

interface IIdCard extends IIdDocument
{
    /**
     * @return string
     */
    public function getLine1();

    /**
     * @return string
     */
    public function getLine2();

    /**
     * @return string
     */
    public function getLine3();

    /**
     * @return DateTime
     */
    public function getExpirationDate();

    /**
     * @return string
     */
    public function getCountryOfNationality();

    /**
     * @return string
     */
    public function getCountryOfIssue();
}