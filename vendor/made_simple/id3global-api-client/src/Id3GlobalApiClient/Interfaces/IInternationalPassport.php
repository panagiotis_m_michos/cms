<?php

namespace Id3GlobalApiClient\Interfaces;

use DateTime;

interface IInternationalPassport extends IIdDocument
{
    /**
     * @return string
     */
    public function getCountry();

    /**
     * @return string
     */
    public function getNumber();

    /**
     * @return DateTime
     */
    public function getExpirationDate();
}