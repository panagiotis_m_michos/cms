<?php

namespace Id3GlobalApiClient\Interfaces;

interface IDrivingLicense extends IIdDocument
{
    /**
     * @return string
     */
    public function getNumber();
}