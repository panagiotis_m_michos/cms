<?php

namespace Id3GlobalApiClient;

use Id3GlobalApiClient\Helpers\Wsdl;
use SoapClient as PHPSoapClient;

class SoapClient extends PHPSoapClient
{
    /**
     * @param Wsdl $wsdl
     */
    public function __construct(Wsdl $wsdl)
    {
        parent::__construct($wsdl->getValue(), ['trace' => 1]);
    }

    /**
     * @return string
     */
    public function getOperationInfo()
    {
        return $this->__getLastRequestHeaders() . PHP_EOL
            . $this->__getLastRequest() . PHP_EOL
            . $this->__getLastResponseHeaders() . PHP_EOL
            . $this->__getLastResponse() . PHP_EOL;
    }
}