<?php

namespace Id3GlobalApiClient\Exceptions;

use Exception;

class CommunicationException extends Id3GlobalApiClientException
{
    /**
     * @param Exception $prev
     * @return $this
     */
    public static function common(Exception $prev)
    {
        return new self('Communication error with SOAP endpoint!', 0, $prev);
    }
}