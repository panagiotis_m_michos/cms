<?php

namespace Id3GlobalApiClient\Exceptions;

use Exception;

class ConfigurationException extends Id3GlobalApiClientException
{
    /**
     * @param array $array
     * @param Exception $prev
     * @return $this
     */
    public static function wrongConfigurationArraySupplied(array $array, Exception $prev = NULL)
    {
        return new self('Wrong configuration array supplied: ' . var_export($array, TRUE) . ' The configuration array must contain only ProfileConfiguration objects', 0, $prev);
    }

    /**
     * @param string $profile
     * @param Exception $prev
     * @return $this
     */
    public static function profileNotFound($profile, Exception $prev = NULL)
    {
        return new self("Profile {$profile} was not found in configuration.", 0, $prev);
    }
}