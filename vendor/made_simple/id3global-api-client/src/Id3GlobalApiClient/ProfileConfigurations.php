<?php

namespace Id3GlobalApiClient;

use Id3GlobalApiClient\Exceptions\ConfigurationException;
use Id3GlobalApiClient\Helpers\EndpointVersion;
use Id3GlobalApiClient\Helpers\Profile;

class ProfileConfigurations
{
    /**
     * @var ProfileConfiguration[]
     */
    private $profileConfigurations;


    /**
     * @param ProfileConfiguration[] $profileConfigurations
     * @throws ConfigurationException
     */
    public function __construct(array $profileConfigurations)
    {
        foreach ($profileConfigurations as $profileConfiguration) {
            if (!($profileConfiguration instanceof ProfileConfiguration)) {
                throw ConfigurationException::wrongConfigurationArraySupplied($profileConfigurations);
            }

            $this->profileConfigurations[$profileConfiguration->getProfile()->getValue()] = $profileConfiguration;
        }
    }

    /**
     * Creates new instance of self from array with following structure:
     * {
     *   $$profileString: {
     *     version: $$endpointVersion,
     *     id: $$id
     *   }
     * }
     * Where:
     * - $$profileString is key and has value of Id3GlobalApiClient\Helpers\Profile const
     * - $$endpointVersion has value of Id3GlobalApiClient\Helpers\EndpointVersion const
     * - $$id is string
     *
     * @param array $configuration
     * @return ProfileConfigurations
     */
    public static function fromArrayConfig(array $configuration)
    {
        $profileConfigurations = array();
        foreach ($configuration as $profileString => $profileData) {
            $endpointVersionString = (string) $profileData['version'];
            $id = $profileData['id'];
            $profileConfigurations[] = new ProfileConfiguration(
                new Profile($profileString),
                new EndpointVersion($endpointVersionString),
                $id
            );
        }

        return new self($profileConfigurations);
    }

    /**
     * @param Profile $profile
     * @return ProfileConfiguration
     * @throws ConfigurationException
     */
    public function getProfileIdVersion(Profile $profile)
    {
        if (!isset($this->profileConfigurations[$profile->getValue()])) {
            throw ConfigurationException::profileNotFound($profile->getValue());
        }

        return $this->profileConfigurations[$profile->getValue()];
    }
}