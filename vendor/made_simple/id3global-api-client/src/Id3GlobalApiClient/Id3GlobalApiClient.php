<?php

namespace Id3GlobalApiClient;

use Id3GlobalApiClient\Entities\Soap\AuthenticateSP;
use Id3GlobalApiClient\Entities\Soap\GlobalAddress;
use Id3GlobalApiClient\Entities\Soap\GlobalAddresses;
use Id3GlobalApiClient\Entities\Soap\GlobalEuropeanIdentityCard;
use Id3GlobalApiClient\Entities\Soap\GlobalIdentityDocuments;
use Id3GlobalApiClient\Entities\Soap\GlobalInputData;
use Id3GlobalApiClient\Entities\Soap\GlobalInternationalPassport;
use Id3GlobalApiClient\Entities\Soap\GlobalPersonal;
use Id3GlobalApiClient\Entities\Soap\GlobalPersonalDetails;
use Id3GlobalApiClient\Entities\Soap\GlobalUKData;
use Id3GlobalApiClient\Entities\Soap\GlobalUKDrivingLicence;
use Id3GlobalApiClient\Entities\Soap\GlobalUKPassport;
use Id3GlobalApiClient\Exceptions\CommunicationException;
use Id3GlobalApiClient\Exceptions\ConfigurationException;
use Id3GlobalApiClient\Helpers\Profile;
use Id3GlobalApiClient\Interfaces\IAddress;
use Id3GlobalApiClient\Interfaces\IDrivingLicense;
use Id3GlobalApiClient\Interfaces\IId3GlobalApiClient;
use Id3GlobalApiClient\Interfaces\IIdCard;
use Id3GlobalApiClient\Interfaces\IIdDocument;
use Id3GlobalApiClient\Interfaces\IInternationalPassport;
use Id3GlobalApiClient\Interfaces\IPassport;
use Id3GlobalApiClient\Interfaces\IPersonalDetails;
use Id3GlobalApiClient\Interfaces\IUkPassport;
use Id3GlobalApiClient\Responses\AuthenticationResult;
use SoapFault;

class Id3GlobalApiClient implements IId3GlobalApiClient
{
    /**
     * @var SoapClient
     */
    protected $soapClient;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var ProfileConfigurations
     */
    private $profileConfigurations;

    /**
     * @param string $username
     * @param string $password
     * @param ProfileConfigurations $profileConfigurations
     * @param SoapClient $soapClient
     */
    public function __construct(
        $username,
        $password,
        ProfileConfigurations $profileConfigurations,
        SoapClient $soapClient
    )
    {
        $this->username = $username;
        $this->password = $password;
        $this->profileConfigurations = $profileConfigurations;
        $this->soapClient = $soapClient;
    }

    /**
     * @return mixed
     */
    public function checkCredentials()
    {
        $credentials = array(
            'AccountName' => $this->username,
            'Password' => $this->password,
        );

        return $this->execute('CheckCredentials', array($credentials));
    }

    /**
     * @param IPersonalDetails $personalDetails
     * @param IAddress $address
     * @param IIdCard $idCard
     * @return AuthenticationResult
     * @throws CommunicationException
     * @throws ConfigurationException
     */
    public function authenticateEuIdCard(IPersonalDetails $personalDetails, IAddress $address, IIdCard $idCard)
    {
        return $this->authenticateDocument($personalDetails, $address, $idCard);
    }

    /**
     * @param IPersonalDetails $personalDetails
     * @param IAddress $address
     * @param IDrivingLicense $drivingLicense
     * @return AuthenticationResult
     * @throws CommunicationException
     * @throws ConfigurationException
     */
    public function authenticateUkDrivingLicense(IPersonalDetails $personalDetails, IAddress $address, IDrivingLicense $drivingLicense)
    {
        return $this->authenticateDocument($personalDetails, $address, $drivingLicense);
    }

    /**
     * @param IPersonalDetails $personalDetails
     * @param IAddress $address
     * @param IUkPassport $passport
     * @return AuthenticationResult
     * @throws CommunicationException
     * @throws ConfigurationException
     */
    public function authenticateUkPassport(IPersonalDetails $personalDetails, IAddress $address, IUkPassport $passport)
    {
        return $this->authenticateDocument($personalDetails, $address, $passport);
    }


    /**
     * @param IPersonalDetails $personalDetails
     * @param IInternationalPassport $passport
     * @return AuthenticationResult
     * @throws CommunicationException
     * @throws ConfigurationException
     */
    public function authenticateInternationalPassport(IPersonalDetails $personalDetails, IInternationalPassport $passport)
    {
        return $this->authenticateDocument($personalDetails, NULL, $passport);
    }

    /**
     * @param IPersonalDetails $personalDetails
     * @param IAddress $address
     * @return AuthenticationResult
     * @throws CommunicationException
     * @throws ConfigurationException
     */
    public function authenticateAddress(IPersonalDetails $personalDetails, IAddress $address)
    {
        $this->soapClient->__setSoapHeaders(array($this->getWsseHeader()));
        $authenticateSp = $this->buildAuthenticateSPForAddress($personalDetails, $address)->toArray();
        $result = $this->execute('AuthenticateSP', array($authenticateSp));
        $this->soapClient->__setSoapHeaders(NULL);
        return new AuthenticationResult($result);
    }

    /**
     * @param IPersonalDetails $personalDetails
     * @param IAddress $address
     * @param IIdDocument $idDocument
     * @return AuthenticationResult
     * @throws CommunicationException
     * @throws ConfigurationException
     */
    public function authenticateDocument(IPersonalDetails $personalDetails, IAddress $address = NULL, IIdDocument $idDocument)
    {
        $this->soapClient->__setSoapHeaders(array($this->getWsseHeader()));
        $authenticateSp = $this->buildAuthenticateSpForDocument($personalDetails, $address, $idDocument)->toArray();
        $result = $this->execute('AuthenticateSP', array($authenticateSp));
        $this->soapClient->__setSoapHeaders(NULL);
        return new AuthenticationResult($result);
    }

    /**
     * @param $methodName
     * @param array $arguments
     * @return mixed
     * @throws CommunicationException
     */
    protected function execute($methodName, array $arguments)
    {
        try {
            $response = call_user_func_array(array($this->soapClient, $methodName), $arguments);
            //file_put_contents(DOCUMENT_ROOT . '/logs/id_check.xml', $this->soapClient->getOperationInfo(), FILE_APPEND);
            return $response;
        } catch (SoapFault $e) {
            throw new CommunicationException($e);
        }
    }


    /**
     * @return WsseAuthHeader
     */
    protected function getWsseHeader()
    {
        return new WsseAuthHeader($this->username, $this->password);
    }

    /**
     * @param IPersonalDetails $personalDetails
     * @param IAddress $address
     * @param IIdDocument $idDocument
     * @return AuthenticateSP|null
     * @throws ConfigurationException
     */
    protected function buildAuthenticateSpForDocument(
        IPersonalDetails $personalDetails,
        IAddress $address,
        IIdDocument $idDocument
    )
    {
        $result = NULL;
        if ($idDocument instanceof IIdCard) {
            $result = $this->buildAuthenticateSPForEuIdCard($personalDetails, $address, $idDocument);
        } elseif ($idDocument instanceof IInternationalPassport) {
            $result = $this->buildAuthenticateSPForInternationalPassport($personalDetails, $idDocument);
        } elseif ($idDocument instanceof IUkPassport) {
            $result = $this->buildAuthenticateSPForUkPassport($personalDetails, $address, $idDocument);
        } elseif ($idDocument instanceof IDrivingLicense) {
            $result = $this->buildAuthenticateSPForUkDrivingLicense($personalDetails, $address, $idDocument);
        }
        return $result;
    }

    /**
     * @param IPersonalDetails $personalDetails
     * @param IInternationalPassport $passport
     * @return AuthenticateSP
     * @throws ConfigurationException
     */
    protected function buildAuthenticateSPForInternationalPassport(
        IPersonalDetails $personalDetails,
        IInternationalPassport $passport
    )
    {
        $globalPersonal = $this->buildGlobalPersonal($personalDetails);

        $globalPassport = new GlobalInternationalPassport(
            $passport->getCountry(),
            $passport->getNumber(),
            $passport->getExpirationDate()->format('j'),
            $passport->getExpirationDate()->format('n'),
            $passport->getExpirationDate()->format('Y')
        );

        $globalIdentityDocuments = new GlobalIdentityDocuments();
        $globalIdentityDocuments->setInternationalPassport($globalPassport);
        $globalInputData = new GlobalInputData($globalPersonal, NULL, $globalIdentityDocuments);
        $profileConfiguration = $this->profileConfigurations->getProfileIdVersion(
            new Profile(Profile::INTERNATIONAL_PASSPORT)
        );
        return new AuthenticateSP(
            $profileConfiguration->getProfileIdVersion(),
            $this->getRandomId(),
            $globalInputData
        );
    }

    /**
     * @param IPersonalDetails $personalDetails
     * @param IAddress $address
     * @param IIdCard $idCard
     * @return AuthenticateSP
     * @throws ConfigurationException
     */
    protected function buildAuthenticateSPForEuIdCard(
        IPersonalDetails $personalDetails,
        IAddress $address,
        IIdCard $idCard
    )
    {
        $globalPersonal = $this->buildGlobalPersonal($personalDetails);
        $globalAddresses = $this->buildGlobalAddresses($address);

        $globalEuIdCard = new GlobalEuropeanIdentityCard(
            $idCard->getLine1(),
            $idCard->getLine2(),
            $idCard->getLine3(),
            $idCard->getExpirationDate()->format('j'),
            $idCard->getExpirationDate()->format('n'),
            $idCard->getExpirationDate()->format('Y'),
            $idCard->getCountryOfNationality(),
            $idCard->getCountryOfIssue()
        );

        $globalIdentityDocuments = new GlobalIdentityDocuments($globalEuIdCard);
        $globalInputData = new GlobalInputData($globalPersonal, $globalAddresses, $globalIdentityDocuments);
        $profileConfiguration = $this->profileConfigurations->getProfileIdVersion(
            new Profile(Profile::UK_ADDRESS_EU_ID_CARD)
        );

        return new AuthenticateSP(
            $profileConfiguration->getProfileIdVersion(),
            $this->getRandomId(),
            $globalInputData
        );
    }

    /**
     * @param IPersonalDetails $personalDetails
     * @return GlobalPersonal
     */
    protected function buildGlobalPersonal(IPersonalDetails $personalDetails)
    {
        $globalPersonalDetails = new GlobalPersonalDetails(
            NULL,
            $personalDetails->getFirstName(),
            $personalDetails->getMiddleName(),
            $personalDetails->getLastName(),
            $personalDetails->getGender(),
            $personalDetails->getDateOfBirth()->format('j'),
            $personalDetails->getDateOfBirth()->format('n'),
            $personalDetails->getDateOfBirth()->format('Y')
        );

        return new GlobalPersonal($globalPersonalDetails);
    }

    /**
     * @param IAddress $address
     * @return GlobalAddresses
     */
    protected function buildGlobalAddresses(IAddress $address)
    {
        $globalAddress = new GlobalAddress();
        $globalAddress->setAddressLine1($address->getAddress1());
        $globalAddress->setAddressLine2($address->getAddress2());
        $globalAddress->setAddressLine3($address->getCity());
        $globalAddress->setAddressLine4($address->getPostcode());
        $globalAddress->setCountry($address->getCountry());
        $globalAddress->setZipPostcode($address->getPostcode());
        return new GlobalAddresses($globalAddress);
    }

    /**
     * @return string
     */
    protected function getRandomId()
    {
        return md5(time() . microtime() . rand(0, 100000));
    }

    /**
     * @param IPersonalDetails $personalDetails
     * @param IAddress $address
     * @param IUkPassport $passport
     * @return AuthenticateSP
     * @throws ConfigurationException
     */
    protected function buildAuthenticateSPForUkPassport(
        IPersonalDetails $personalDetails,
        IAddress $address,
        IUkPassport $passport
    )
    {
        $globalPersonal = $this->buildGlobalPersonal($personalDetails);
        $globalAddresses = $this->buildGlobalAddresses($address);

        $globalUkPassport = new GlobalUKPassport(
            $passport->getNumber(),
            $passport->getExpirationDate()->format('j'),
            $passport->getExpirationDate()->format('n'),
            $passport->getExpirationDate()->format('Y')
        );
        $globalUkData = new GlobalUKData($globalUkPassport);
        $globalIdentityDocuments = new GlobalIdentityDocuments(NULL, $globalUkData);
        $globalInputData = new GlobalInputData($globalPersonal, $globalAddresses, $globalIdentityDocuments);
        $profileConfiguration = $this->profileConfigurations->getProfileIdVersion(
            new Profile(Profile::UK_ADDRESS_UK_PASSPORT)
        );

        return new AuthenticateSP(
            $profileConfiguration->getProfileIdVersion(),
            $this->getRandomId(),
            $globalInputData
        );
    }

    /**
     * @param IPersonalDetails $personalDetails
     * @param IAddress $address
     * @param IDrivingLicense $drivingLicense
     * @return AuthenticateSP
     * @throws ConfigurationException
     */
    protected function buildAuthenticateSPForUkDrivingLicense(
        IPersonalDetails $personalDetails,
        IAddress $address,
        IDrivingLicense $drivingLicense
    )
    {
        $globalPersonal = $this->buildGlobalPersonal($personalDetails);
        $globalAddresses = $this->buildGlobalAddresses($address);

        $globalUkDrivingLicense = new GlobalUKDrivingLicence($drivingLicense->getNumber());
        $globalUkData = new GlobalUKData(NULL, $globalUkDrivingLicense);
        $globalIdentityDocuments = new GlobalIdentityDocuments(NULL, $globalUkData);

        $globalInputData = new GlobalInputData($globalPersonal, $globalAddresses, $globalIdentityDocuments);

        $profileConfiguration = $this->profileConfigurations->getProfileIdVersion(
            new Profile(Profile::UK_ADDRESS_UK_DRIVING_LICENSE)
        );

        return new AuthenticateSP(
            $profileConfiguration->getProfileIdVersion(),
            $this->getRandomId(),
            $globalInputData
        );
    }

    /**
     * @param IPersonalDetails $personalDetails
     * @param IAddress $address
     * @return AuthenticateSP
     * @throws ConfigurationException
     */
    protected function buildAuthenticateSPForAddress(IPersonalDetails $personalDetails, IAddress $address)
    {
        $globalPersonal = $this->buildGlobalPersonal($personalDetails);
        $globalAddresses = $this->buildGlobalAddresses($address);

        $globalInputData = new GlobalInputData($globalPersonal, $globalAddresses);

        $profileConfiguration = $this->profileConfigurations->getProfileIdVersion(
            new Profile(Profile::UK_ADDRESS)
        );

        return new AuthenticateSP(
            $profileConfiguration->getProfileIdVersion(),
            $this->getRandomId(),
            $globalInputData
        );
    }


}