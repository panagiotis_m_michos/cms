<?php

namespace Id3GlobalApiClient;

use SoapHeader;
use SoapVar;
use stdClass;

/**
 * Class taken from Id3Global API documentation
 */
class WsseAuthHeader extends SoapHeader
{
    /**
     * @var string
     */
    private $wssNs = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';

    /**
     * @param string $user
     * @param string $pass
     * @param string|NULL $ns
     */
    public function __construct($user, $pass, $ns = NULL)
    {
        if ($ns) {
            $this->wssNs = $ns;
        }
        $auth = new stdClass();
        $auth->Username = new SoapVar($user, XSD_STRING, NULL, $this->wssNs, 'Username', $this->wssNs);
        $auth->Password = new SoapVar($pass, XSD_STRING, NULL, $this->wssNs, 'Password', $this->wssNs);
        $usernameToken = new stdClass();
        $usernameToken->UsernameToken = new SoapVar(
            $auth,
            SOAP_ENC_OBJECT,
            NULL,
            $this->wssNs,
            'UsernameToken',
            $this->wssNs
        );
        $securitySv = new SoapVar(
            new SoapVar($usernameToken, SOAP_ENC_OBJECT, NULL, $this->wssNs, 'UsernameToken', $this->wssNs),
            SOAP_ENC_OBJECT,
            NULL,
            $this->wssNs,
            'Security',
            $this->wssNs
        );
        parent::__construct($this->wssNs, 'Security', $securitySv, TRUE);
    }
}