<?php

namespace Id3GlobalApiClient\Responses;

use Id3GlobalApiClient\Interfaces\IAuthenticationResult;

class AuthenticationResult implements IAuthenticationResult
{
    protected $result;

    /**
     * @param $result
     */
    public function __construct($result)
    {
        $this->result = $result;
    }

    /**
     * @return integer
     */
    public function getScore()
    {
        return $this->result->AuthenticateSPResult->Score;
    }
}