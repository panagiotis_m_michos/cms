##About
This library serves as object layer above the ID3Global web service. Right now it covers validation of credentials and following customer authentication types:
- personal data + UK address + UK passport
- personal data + UK address + UK driving license
- personal data + UK address + EU ID card

Customer authentication returns authentication result, from which you can request the authentication score.

##Requirements
This package depends only on "garoevans/php-enum" for its enum types.

##Usage
This package usage consist of following:

###Instantiate Id3Global API Client
Id3Global uses different "profiles" for different authentication types, so we will need these before instantiating the client itself. Each of the profile consist of endpoint version to connect to and profile ID.

There are 2 ways of instantiating the class `Id3GlobalApiClient\ProfileConfigurations`:
####The OOP way
Create all the objects needed for the constructor or let the DI Container do the work:
~~~
$profileConfig = new \Id3GlobalApiClient\ProfileConfiguration(
                        new Profile(Profile::UK_ADDRESS_UK_DRIVING_LICENSE),
                        new EndpointVersion(EndpointVersion::V_1_1),
                        '0b89072f-9953-4bc7-b773-6c8f2024b9a0');
$profileConfigurations = new ProfileConfigurations(array($profileConfig));
~~~
Note that all the enum types have their own classes and have to be instantiated. This prevents implementer from passing wrong values.

####Named constructor
The name constructor `\Id3GlobalApiClient\ProfileConfiguration::fromArrayConfig(array $config)` poses as a factory to build itself from input array. This actually calls the OOP code above, so you can benefit from all the type checking while avoiding creation of too much objects by hand.
~~~
$profileConfigurations = \Id3GlobalApiClient\ProfileConfigurations::fromArrayConfig(array(
    'UK_ADDRESS_UK_DRIVING_LICENSE' => array(
        'id' => '0b89072f-9953-4bc7-b773-6c8f2024b9a0',
        'version' => 1.1
    )
));
~~~

If you pass an array that can not be parsed correctly an `Id3GlobalApiClient\Exceptions\ConfigurationException` will be thrown.

####Instantiating SOAP client for Id3GlobalClient to use
The SoapClient class is an encapsulation of native SoapClient to allow for selected WSLDs to be used.

You must pass wsdl object of  `\Id3GlobalApiClient\Helpers\Wsdl`
~~~
$wsdl = new \Id3GlobalApiClient\Helpers\Wsdl(\Id3GlobalApiClient\Helpers\Wsdl::LIVE); //Choose live/pilot WSDLs
$soapClient = new \Id3GlobalApiClient\SoapClient($wsdl);
~~~

####Instantiating the client
You must pass the $username, $password, `\Id3GlobalApiClient\ProfileConfigurations` and `\Id3GlobalApiClient\SoapClient`

We will use object from previous sections of this readme.
~~~
$client = new Id3GlobalApiClient($username, $password, $profileConfigurations, $soapClient);
~~~

###Authentication
There are two ways of performing authentication:

####Specifying the documents that wil be authenticated
There are functions dedicated to validation using UK passport, UK driving license or European ID card
For example validation using UK driving license:
~~~
$personalDetails = new PersonalDetails(
    'John',
    'Doe',
    new DateTime('1990-09-09')
);

$address = new Address(
    'Address line 1',
    null,
    null,
    'London',
    null,
    'PSOSTCODE',
    'United Kingdom'
);

$ukDrivingLicense = new UkDrivingLicense('0800238828801');

$result = $client->authenticateUkDrivingLicense($personalDetails, $address, $ukDrivingLicense);
~~~

####Single method for all authentication types
We can pass any ID Document(UK passport, UK driving license or EU ID card) into the authenticate function like so:
~~~
$personalDetails = new PersonalDetails(
    'John',
    'Doe',
    new DateTime('1990-09-09')
);

$address = new Address(
    'Address line 1',
    null,
    null,
    'London',
    null,
    'PSOSTCODE',
    'United Kingdom'
);

$ukDrivingLicense = new UkDrivingLicense('0800238828801');

$result = $client->authenticate($personalDetails, $address, $ukDrivingLicense);
~~~

In both cases, the $result object is an instance of `\Id3GlobalApiClient\AuthenticationResult` from which we are able to fetch the result score by calling the method `\Id3GlobalApiClient\AuthenticationResult::getScore()`. This will return an integer representing the score of the authentication.

### Thrown exceptions
* `Id3GlobalApiClient\Exceptions\ConfigurationException` will be thrown when trying to authenticate documents that do not have configuration profile set.`
* `Id3GlobalApiClient\Exceptions\CommunicationException` encapsulates all SoapFault errors of the

