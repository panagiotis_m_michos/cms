#HTTP client

##Todo
This package is not covered by unit tests. They should be implemented in near future.

##About
This library serves as extension point for any future API libraries. Even though it is capable of working on its own, it's strongly recommended to extend `\HttpClient\HttpClient` as noted in Usage of extended client part of this readme.

##Requirements
As noted in composer.json, this package requires Guzzle as its client and garoevans/php-enum package to enable SplEnum-like behaviour on all systems.

##Basic Usage
~~~
use HttpClient\HttpMethod;
use HttpClient\Interfaces\IHttpRequest;
use HttpClient\Interfaces\IHttpResponse;
use HttpClient\HttpClient;

// Example request to google.com that returns body of the response
class GoogleHomepageRequest implements IHttpRequest
{

    /**
     * @return string
     */
    public function getUri()
    {
        return 'http://www.google.com';
    }

    /**
     * @return HttpMethod
     */
    public function getMethod()
    {
        return new HttpMethod(HttpMethod::GET);
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return '';
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return array();
    }

    /**
     * @param IHttpResponse $response
     * @return string
     */
    public function buildResponse(IHttpResponse $response)
    {
        return $response->getBody();
    }
}


$client = new HttpClient();
$response = $client->sendRequest(new GoogleHomepageRequest());
~~~

##Usage of extended client
This usage is recommended for any API/Http library that uses this one. It consist of extending the `\HttpClient\HttpClient` class and adding each request to it as a class method. This way you are able to type hint the return type, since the `\HttpClient\HttpClient::buildResponse(string, HttpStatus)` method returns mixed.
~~~
use HttpClient\HttpClient;

// Example client to handle google request shown above
class GoogleClient extends HttpClient
{
    /**
     * @return string
     */
    public function getHomepage()
    {
        $request = new GoogleHomepageRequest();
        return $this->sendRequest($request);
    }
}

$googleClient = new GoogleClient();
$response = $googleClient->getHomepage();
~~~