<?php

namespace HttpClient\Test;

use HttpClient\HttpClient;
use HttpClient\HttpMethod;
use HttpClient\HttpStatusCode;
use PHPUnit_Framework_TestCase;

class HttpClientTest extends PHPUnit_Framework_TestCase
{

    public function testCanInstantiate()
    {
        $httpClient = new HttpClient();
        $this->assertInstanceOf('HttpClient\HttpClient', $httpClient);
    }

    public function testCanSendRequest()
    {
        $method = 'POST';
        $url = 'test.sk';
        $body = '{test}';
        $headers = array('one' => 'two');

        $responseStatusCode = 200;
        $responseHeaders = array('three' => 'four');
        $responseBody = '{second}';

        $requestMock = $this->getInternalRequestMock(
            new HttpMethod(HttpMethod::POST),
            $url,
            $headers,
            $body
        );

        $responseMock = $this->getInternalResponseMock(
            $responseStatusCode,
            $responseHeaders,
            $responseBody
        );

        $middleware = $this->getMockBuilder('HttpClient\Interfaces\IHttpMiddleware')
            ->disableOriginalConstructor()
            ->getMock();
        $middleware->expects($this->once())
            ->method('sendRequest')
            ->with($requestMock)
            ->willReturn($responseMock);

        $httpClient = new HttpClient($middleware);
        $actualResponse = $httpClient->sendRequest($requestMock);

        $this->assertSame('ok', $actualResponse);
    }

    public function getInternalRequestMock(HttpMethod $method, $url, $headers, $body)
    {
        $request = $this->getMockBuilder('HttpClient\Interfaces\IHttpRequest')
            ->disableOriginalConstructor()
            ->getMock();

        $request->expects($this->any())
            ->method('getMethod')
            ->willReturn($method);

        $request->expects($this->any())
            ->method('getUrl')
            ->willReturn($url);

        $request->expects($this->any())
            ->method('getBody')
            ->willReturn($body);

        $request->expects($this->any())
            ->method('getHeaders')
            ->willReturn($headers);

        $request->expects($this->once())
            ->method('buildResponse')
            ->willReturn('ok');

        return $request;
    }

    public function getInternalResponseMock($statusCode, $headers, $body)
    {
        $response = $this->getMockBuilder('HttpClient\HttpResponse')
            ->disableOriginalConstructor()
            ->getMock();

        $response->expects($this->any())
            ->method('getStatusCode')
            ->willReturn($statusCode);

        $response->expects($this->any())
            ->method('getHeaders')
            ->willReturn($headers);

        $response->expects($this->any())
            ->method('getBody')
            ->willReturn($body);

        return $response;
    }
}