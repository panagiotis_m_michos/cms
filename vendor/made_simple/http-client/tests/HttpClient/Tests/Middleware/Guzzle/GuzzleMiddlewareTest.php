<?php

namespace HttpClient\Tests\Middleware\Guzzle;

use HttpClient\HttpMethod;
use HttpClient\HttpStatusCode;
use HttpClient\Middleware\Guzzle\GuzzleMiddleware;
use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;

class GuzzleMiddlewareTest extends PHPUnit_Framework_TestCase
{
    /** @var PHPUnit_Framework_MockObject_MockObject */
    private $guzzleClientMock;

    /** @var PHPUnit_Framework_MockObject_MockObject */
    private $requestMock;

    public function setUp()
    {
        $this->guzzleClientMock = $this->getMock('Guzzle\Http\Client');
        $this->requestMock = $this->getMock('HttpClient\Interfaces\IHttpRequest');
    }

    /**
     * @covers HttpClient\Middleware\Guzzle\GuzzleMiddleware::__construct
     */
    public function testCanInstantiate()
    {
        $guzzleMiddleware = new GuzzleMiddleware($this->guzzleClientMock);
        $this->assertInstanceOf('HttpClient\Middleware\Guzzle\GuzzleMiddleware', $guzzleMiddleware);
    }

    /**
     * @covers HttpClient\Middleware\Guzzle\GuzzleMiddleware::sendRequest
     */
    public function testCanSendRequest()
    {
        $method = 'POST';
        $url = 'test.sk';
        $body = '{test}';
        $headers = array('one' => 'two');

        $responseStatusCode = 200;
        $responseHeaders = array('three' => 'four');
        $responseBody = '{second}';

        $guzzleMiddleware = new GuzzleMiddleware($this->guzzleClientMock);

        $internalRequestMock = $this->getInternalRequestMock(
            new HttpMethod($method),
            $url,
            $headers,
            $body
        );

        $guzzleRequest = $this->getGuzzleRequestMock(
            $method,
            $url,
            $headers,
            $body
        );

        $guzzleResponse = $this->getGuzzleResponseMock(
            $responseStatusCode,
            $responseHeaders,
            $responseBody
        );

        $this->guzzleClientMock->expects($this->any())
            ->method('createRequest')
            ->with(
                $method,
                $url,
                $headers,
                $body
            )
            ->willReturn($guzzleRequest);

        $this->guzzleClientMock->expects($this->any())
            ->method('send')
            ->with($this->callback(function ($request) use ($guzzleRequest) {
                return $request == $guzzleRequest;
            }))
            ->willReturn($guzzleResponse);

        $response = $guzzleMiddleware->sendRequest($internalRequestMock);

        $this->assertSame($response->getBody(), $responseBody);
        $this->assertEquals($response->getStatusCode(), new HttpStatusCode(HttpStatusCode::OK));
        $this->assertSame($response->getHeaders(), $responseHeaders);
    }

    public function getGuzzleRequestMock($method, $url, $headers, $body)
    {
        $request = $this->getMockBuilder('Guzzle\Http\Message\Request')
            ->disableOriginalConstructor()
            ->getMock();

        $request->expects($this->any())
            ->method('getMethod')
            ->willReturn($method);

        $request->expects($this->any())
            ->method('getUri')
            ->willReturn($url);

        $request->expects($this->any())
            ->method('getBody')
            ->willReturn($body);

        $request->expects($this->any())
            ->method('getHeaders')
            ->willReturn($headers);

        return $request;
    }

    public function getInternalRequestMock(HttpMethod $method, $url, $headers, $body)
    {
        $request = $this->getMockBuilder('HttpClient\Interfaces\IHttpRequest')
            ->disableOriginalConstructor()
            ->getMock();

        $request->expects($this->any())
            ->method('getMethod')
            ->willReturn($method);

        $request->expects($this->any())
            ->method('getUrl')
            ->willReturn($url);

        $request->expects($this->any())
            ->method('getBody')
            ->willReturn($body);

        $request->expects($this->any())
            ->method('getHeaders')
            ->willReturn($headers);

        return $request;
    }

    public function getGuzzleResponseMock($statusCode, $headers, $body)
    {
        $headersMock = $this->getMockBuilder('Guzzle\Http\Message\Header\HeaderCollection')
            ->disableOriginalConstructor()
            ->getMock();

        $headersMock->expects($this->any())
            ->method('toArray')
            ->willReturn($headers);

        $response = $this->getMockBuilder('Guzzle\Http\Message\Response')
            ->disableOriginalConstructor()
            ->getMock();

        $response->expects($this->any())
            ->method('getStatusCode')
            ->willReturn($statusCode);

        $response->expects($this->any())
            ->method('getHeaders')
            ->willReturn($headersMock);

        $response->expects($this->any())
            ->method('getBody')
            ->willReturn($body);

        return $response;
    }
}