<?php

namespace HttpClient\Helpers;

use Garoevans\PhpEnum\Enum as GaroEnum;

class Enum extends GaroEnum
{
    private $value;

    /**
     * @param mixed $value
     */
    public function __construct($value)
    {
        $this->value = $value;
        parent::__construct($value, TRUE);
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $compare
     * @return bool
     */
    public function is($compare)
    {
        return $compare === $this->value;
    }
}