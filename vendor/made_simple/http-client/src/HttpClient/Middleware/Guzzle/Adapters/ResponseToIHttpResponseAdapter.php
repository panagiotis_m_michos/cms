<?php

namespace HttpClient\Middleware\Guzzle\Adapters;

use Exception;
use Guzzle\Http\Message\MessageInterface;
use Guzzle\Http\Message\Response;
use HttpClient\Exceptions\InternalException;
use HttpClient\HttpStatusCode;
use HttpClient\Interfaces\IHttpResponse;

class ResponseToIHttpResponseAdapter implements IHttpResponse
{
    /**
     * @var MessageInterface
     */
    private $response;

    /**
     * @param Response $response
     */
    public function __construct(Response $response)
    {
        $this->response = $response;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->response->getBody(TRUE);
    }

    /**
     * @return HttpStatusCode
     * @throws InternalException
     */
    public function getStatusCode()
    {
        try {
            $statusCode = new HttpStatusCode($this->response->getStatusCode());
        } catch (Exception $e) {
            throw InternalException::unableToTranslateHttpStatusCode($this->response->getStatusCode(), $e);
        }

        return $statusCode;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->response->getHeaders()->toArray();
    }
}