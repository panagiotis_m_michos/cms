<?php

namespace HttpClient\Middleware\Guzzle;

use Exception;
use Guzzle\Http\ClientInterface;
use Guzzle\Http\Exception\ClientErrorResponseException;
use HttpClient\Exceptions\CommunicationException;
use HttpClient\Interfaces\IHttpMiddleware;
use HttpClient\Interfaces\IHttpRequest;
use HttpClient\Interfaces\IHttpResponse;
use HttpClient\Middleware\Guzzle\Adapters\IHttpRequestToRequestInterfaceAdapter;
use HttpClient\Middleware\Guzzle\Adapters\ResponseToIHttpResponseAdapter;

class GuzzleMiddleware implements IHttpMiddleware
{

    /**
     * @var ClientInterface
     */
    private $guzzle;

    /**
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->guzzle = $client;
    }

    /**
     * @param IHttpRequest $request
     * @return IHttpResponse
     * @throws CommunicationException
     */
    public function sendRequest(IHttpRequest $request)
    {
        //This can't be encapsulated in an adapter, because Guzzle uses different classes for different methods/requests
        $guzzleRequest = $this->guzzle->createRequest(
            $request->getMethod()->getValue(),
            $request->getUrl(),
            $request->getHeaders(),
            $request->getBody(),
            array(
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_0,
            )
        );

        try {
            $guzzleResponse = $this->guzzle->send($guzzleRequest);
        } catch (ClientErrorResponseException $e) {
            //Error response is still a response
            $guzzleResponse = $e->getResponse();
        } catch (Exception $e) {
            throw CommunicationException::communicationUnsuccessful($e);
        }

        return new ResponseToIHttpResponseAdapter($guzzleResponse);
    }
}