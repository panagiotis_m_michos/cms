<?php

namespace HttpClient;

use HttpClient\Exceptions\HttpClientException;
use HttpClient\Exceptions\InternalException;
use HttpClient\Interfaces\IHttpClient;
use HttpClient\Interfaces\IHttpMiddleware;
use HttpClient\Interfaces\IHttpRequest;
use HttpClient\Middleware\Guzzle\GuzzleMiddleware;
use Exception;
use Guzzle\Http\Client;
use Nette\Object;

class HttpClient extends Object implements IHttpClient
{

    /**
     * @var IHttpMiddleware
     */
    private $middleware;

    /**
     * @param IHttpMiddleware $middleware
     */
    public function __construct(IHttpMiddleware $middleware = NULL)
    {
        if ($middleware === NULL) {
            $middleware = new GuzzleMiddleware(new Client());
        }

        $this->middleware = $middleware;
    }

    /**
     * Sends generic requests
     *
     * @param IHttpRequest $request
     * @return mixed
     * @throws HttpClientException
     * @throws InternalException
     */
    public function sendRequest(IHttpRequest $request)
    {
        try {
            $response = $this->middleware->sendRequest($request);
        } catch (HttpClientException $e) {
            //propagate known exceptions
            throw $e;
        } catch (Exception $e) {
            throw InternalException::unexpectedException($e);
        }

        return $request->buildResponse($response);
    }
} 