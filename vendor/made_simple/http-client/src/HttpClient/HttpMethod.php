<?php

namespace HttpClient;

use HttpClient\Helpers\Enum;

class HttpMethod extends Enum
{

    const __default = self::GET;

    const PUT = 'PUT';
    const DELETE = 'DELETE';
    const POST = 'POST';
    const GET = 'GET';

    public function __construct($httpMethod)
    {
        parent::__construct($httpMethod);
    }
}
