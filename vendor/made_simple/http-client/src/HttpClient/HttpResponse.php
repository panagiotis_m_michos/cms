<?php

namespace HttpClient;

use HttpClient\Interfaces\IHttpResponse;

class HttpResponse implements IHttpResponse
{
    /**
     * @var string
     */
    private $body;
    /**
     * @var HttpStatusCode
     */
    private $statusCode;
    /**
     * @var array
     */
    private $headers;

    /**
     * @param $body
     * @param HttpStatusCode $statusCode
     * @param array $headers
     */
    public function __construct($body, HttpStatusCode $statusCode, array $headers)
    {
        $this->body = $body;
        $this->statusCode = $statusCode;
        $this->headers = $headers;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @return HttpStatusCode
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }
}