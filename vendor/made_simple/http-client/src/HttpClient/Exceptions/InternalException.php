<?php

namespace HttpClient\Exceptions;

use Exception;

class InternalException extends HttpClientException
{
    /**
     * @param Exception $prev
     * @return self
     */
    public static function unableToBuildMessage(Exception $prev = NULL)
    {
        return new self('Internal exception occurred when building the message!', 0, $prev);
    }

    /**
     * @param $statusCode
     * @param Exception $prev
     * @return self
     */
    public static function unableToTranslateHttpStatusCode($statusCode, Exception $prev = NULL)
    {
        return new self("Unable to understand status code: {$statusCode}", 0, $prev);
    }

    /**
     * @param Exception $exception
     * @return InternalException
     */
    public static function unexpectedException(Exception $exception)
    {
        return new self("Unexpected exception occured: {$exception->getMessage()}", 0, $exception);
    }
} 