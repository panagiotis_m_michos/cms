<?php

namespace HttpClient\Exceptions;

use Exception;

class CommunicationException extends HttpClientException
{
    /**
     * @param Exception $prev
     * @return self
     */
    public static function communicationUnsuccessful(Exception $prev = NULL)
    {
        return new self('Communication with remote server failed!', 0, $prev);
    }
} 