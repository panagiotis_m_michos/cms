<?php

namespace HttpClient\Interfaces;

interface IHttpClient
{
    /**
     * @param IHttpRequest $request
     * @return mixed
     */
    public function sendRequest(IHttpRequest $request);
}