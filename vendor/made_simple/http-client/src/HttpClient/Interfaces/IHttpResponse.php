<?php

namespace HttpClient\Interfaces;

use HttpClient\HttpStatusCode;

interface IHttpResponse
{
    /**
     * @return string
     */
    public function getBody();

    /**
     * @return HttpStatusCode
     */
    public function getStatusCode();

    /**
     * @return array
     */
    public function getHeaders();
}