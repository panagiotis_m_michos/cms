<?php

namespace HttpClient\Interfaces;

use HttpClient\HttpMethod;

interface IHttpRequest
{
    /**
     * @return string
     */
    public function getUrl();

    /**
     * @return HttpMethod
     */
    public function getMethod();

    /**
     * @return string
     */
    public function getBody();

    /**
     * @return array
     */
    public function getHeaders();

    /**
     * @param IHttpResponse $response
     * @return mixed
     */
    public function buildResponse(IHttpResponse $response);
} 