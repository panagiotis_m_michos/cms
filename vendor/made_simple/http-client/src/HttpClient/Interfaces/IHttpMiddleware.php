<?php

namespace HttpClient\Interfaces;

interface IHttpMiddleware
{
    /**
     * @param IHttpRequest $request
     * @return IHttpResponse
     */
    public function sendRequest(IHttpRequest $request);
}