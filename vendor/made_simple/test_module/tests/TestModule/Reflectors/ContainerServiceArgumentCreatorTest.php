<?php

namespace TestModule\Reflectors;

use Doctrine\Common\Annotations\AnnotationReader;
use PHPUnit_Framework_TestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use TestModule\Annotations\Inject;
use TestModule\Reflectors\ContainerServiceArgumentCreator;

class A {};
class B {};

class SimpleTestCase1
{
    public function setupFromContainer(A $a, B $b) {}
}

class SimpleTestCase2
{
    /**
     * @Inject({"a", "b"})
     * @param A $a
     * @param B $b
     */
    public function setupFromContainer(A $a, B $b)
    {
    }
}

class ContainerServiceArgumentCreatorTest extends PHPUnit_Framework_TestCase
{
    private $properties;

    public function setUp()
    {
        $class = new \ReflectionClass(ContainerServiceArgumentCreator::class);
        $this->properties = $class->getStaticProperties();
    }

    public function tearDown()
    {
        call_user_func_array([ContainerServiceArgumentCreator::class, 'create'], $this->properties);
    }

    /**
     * how to test index php creates it already
     * @expectedException \RuntimeException
     */
    //public function testRetrieveFailureNoAnnotation()
    //{
    //    ContainerServiceArgumentCreator::retrieve(new SimpleTestCase1(), 'setupFromContainer');
    //}

    /**
     * @expectedException \ReflectionException
     */
    public function testRetrieveFailureNoMethod()
    {
        $this->init();
        ContainerServiceArgumentCreator::retrieve(new SimpleTestCase2(), 'setup3');
    }

    /**
     * @expectedException \Symfony\Component\DependencyInjection\Exception\InvalidArgumentException
     */
    public function testRetrieveFailureNoService()
    {
        $container = $this->init();
        $container->set('a', NULL);
        ContainerServiceArgumentCreator::retrieve(new SimpleTestCase2(), 'setupFromContainer');
    }

    public function testRetrieve()
    {
        $this->init();
        $case = new SimpleTestCase2();
        $arguments = ContainerServiceArgumentCreator::retrieve($case, 'setupFromContainer');
        $this->assertCount(2, $arguments);
        $this->assertInstanceOf(A::class, $arguments[0]);
        $this->assertInstanceOf(B::class, $arguments[1]);
    }

    /**
     * @return ContainerBuilder
     */
    private function init()
    {
        $a = new A(); $b = new B();
        $container = new ContainerBuilder();
        $container->set('a', $a);
        $container->set('b', $b);
        $annotationReader = new AnnotationReader();
        ContainerServiceArgumentCreator::create([$container], $annotationReader);
        return $container;
    }
}