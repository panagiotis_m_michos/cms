<?php

namespace TestModule;

use BootstrapModule\Ext\DoctrineExt;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use TestModule\Helpers\DatabaseHelper;
use TestModule\Reflectors\ContainerServiceArgumentCreator;

class Boot
{
    /**
     * @param Container $container
     */
    public static function setup(Container $container)
    {
        // we can not use the container for adding test services once its compiled, so we create a new one
        $databaseHelperDefinition = new Definition(DatabaseHelper::class, [new Reference(DoctrineExt::ENTITY_MANAGER)]);

        $entityManagerDefinition = new Definition(NULL, [DoctrineExt::ENTITY_MANAGER]);
        $entityManagerDefinition->setFactory([$container, 'get']);

        $testContainer = new ContainerBuilder();
        $testContainer->setDefinition(DoctrineExt::ENTITY_MANAGER, $entityManagerDefinition);
        $testContainer->setDefinition('test_module.database_helper', $databaseHelperDefinition);
        $testContainer->setDefinition('test_module.helpers.database_helper', $databaseHelperDefinition);

        ContainerServiceArgumentCreator::create([$testContainer, $container], new AnnotationReader());
    }

}