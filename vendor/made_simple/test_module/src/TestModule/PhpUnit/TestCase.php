<?php

namespace TestModule\PhpUnit;

use PHPUnit_Framework_TestCase;
use TestModule\Reflectors\ContainerServiceArgumentCreator;
use TestModule\Reflectors\PhpUnitMockArgumentCreator;

class TestCase extends PHPUnit_Framework_TestCase
{
    const METHOD_ONLY_PHPUNIT = 'setupWithMocks';
    const METHOD_ONLY_CONTAINER = 'setupWithContainer';
    const METHOD_CONTAINER_MOCK = 'setupDependencies';

    public function setUp()
    {
        if (method_exists($this, self::METHOD_CONTAINER_MOCK)) {
            $serviceArguments = ContainerServiceArgumentCreator::retrieve($this, self::METHOD_CONTAINER_MOCK);
            $arguments = PhpUnitMockArgumentCreator::retrieve($this, $this, self::METHOD_CONTAINER_MOCK, $serviceArguments);
            call_user_func_array([$this, self::METHOD_CONTAINER_MOCK], $arguments);
        } else {
            if (method_exists($this, self::METHOD_ONLY_PHPUNIT)) {
                $arguments = PhpUnitMockArgumentCreator::retrieve($this, $this, self::METHOD_ONLY_PHPUNIT);
                call_user_func_array([$this, self::METHOD_ONLY_PHPUNIT], $arguments);
            }
            if (method_exists($this, self::METHOD_ONLY_CONTAINER)) {
                $arguments = ContainerServiceArgumentCreator::retrieve($this, self::METHOD_ONLY_CONTAINER);
                call_user_func_array([$this, self::METHOD_ONLY_CONTAINER], $arguments);
            }
        }
    }
}