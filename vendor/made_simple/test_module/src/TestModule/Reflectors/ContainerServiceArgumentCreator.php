<?php

namespace TestModule\Reflectors;

use Doctrine\Common\Annotations\AnnotationReader;
use ReflectionMethod;
use RuntimeException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use TestModule\Annotations\Inject;

class ContainerServiceArgumentCreator
{
    /**
     * @var ContainerInterface[]
     */
    private static $containers;

    /**
     * @var AnnotationReader
     */
    private static $annotationReader;

    /**
     * @param ContainerInterface[] $containers
     * @param AnnotationReader $annotationReader
     */
    public static function create(array $containers, AnnotationReader $annotationReader)
    {
        self::$containers = $containers;
        self::$annotationReader = $annotationReader;
    }

    /**
     * @param mixed $object
     * @param string $method
     * @return array
     * @throws RuntimeException
     * @throws ServiceNotFoundException
     */
    public static function retrieve($object, $method)
    {
        if (!self::$containers) {
            throw new RuntimeException(__CLASS__ . ' needs to be initialized (create method)');
        }
        $annotation = self::$annotationReader->getMethodAnnotation(new ReflectionMethod($object, $method), Inject::class);
        if ($annotation === NULL) {
            return [];
        }
        $arguments = [];
        foreach ($annotation->getServiceIds() as $argumentName => $serviceId) {
            $service = NULL;
            foreach (self::$containers as $container) {
                $service = $container->get($serviceId, ContainerInterface::NULL_ON_INVALID_REFERENCE);
                if ($service) {
                    break;
                }
            }
            if (!$service) {
                throw new ServiceNotFoundException($serviceId);
            }
            $arguments[$argumentName] = $service;
        }
        return $arguments;
    }

}