<?php

namespace TestModule\Reflectors;

use Prophecy\Prophet;
use ReflectionMethod;

class ProphecyArgumentCreator
{
    /**
     * @param Prophet $prophet
     * @param mixed $object
     * @param string $method
     * @param array $useExisting
     * @return array
     */
    public static function retrieve(Prophet $prophet, $object, $method, $useExisting = [])
    {
        $reflectionMethod = new ReflectionMethod($object, $method);
        $arguments = [];

        foreach ($reflectionMethod->getParameters() as $parameter) {
            if (isset($useExisting[$parameter->getName()])) {
                $arguments[$parameter->getName()] = $useExisting[$parameter->getName()];
            } else {
                $arguments[$parameter->getName()] = $prophet->prophesize($parameter->getClass()->getName());
            }
        }
        return $arguments;
    }
}