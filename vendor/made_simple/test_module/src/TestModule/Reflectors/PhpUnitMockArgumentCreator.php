<?php

namespace TestModule\Reflectors;

use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;
use ReflectionMethod;

class PhpUnitMockArgumentCreator
{
    /**
     * @param PHPUnit_Framework_TestCase $testCase
     * @param mixed $object
     * @param string $method
     * @param array $useExisting
     * @return array
     */
    public static function retrieve(PHPUnit_Framework_TestCase $testCase, $object, $method, $useExisting = [])
    {
        $reflectionMethod = new ReflectionMethod($object, $method);
        $arguments = [];
        foreach ($reflectionMethod->getParameters() as $parameter) {
            if (isset($useExisting[$parameter->getName()])) {
                $arguments[$parameter->getName()] = $useExisting[$parameter->getName()];
            } else {
                $arguments[$parameter->getName()] = self::mock($testCase, $parameter->getClass()->getName());
            }
        }
        return $arguments;
    }

    /**
     * @param PHPUnit_Framework_TestCase $testCase
     * @param string $object
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    public static function mock(PHPUnit_Framework_TestCase $testCase, $object)
    {
        return $testCase->getMockBuilder($object)->disableOriginalConstructor()->getMock();
    }
}