<?php

namespace TestModule\Helpers;

use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManager;

class DatabaseHelper
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param array $tables
     * @throws DBALException
     */
    public function emptyTables(array $tables)
    {
        $q = $this->entityManager->getConnection();
        foreach ($tables as $table) {
            $result = $q->executeQuery("TRUNCATE $table");
        }
        $this->clearEntities();
    }

    public function clearEntities()
    {
        $this->entityManager->clear();
    }

    /**
     * @param mixed $entity
     */
    public function saveEntity($entity)
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
    }
}