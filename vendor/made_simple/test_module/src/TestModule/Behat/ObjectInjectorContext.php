<?php

namespace TestModule\Behat;

use PhpSpec\Wrapper\Unwrapper;
use Prophecy\Prophet;
use TestModule\Reflectors\ContainerServiceArgumentCreator;
use TestModule\Reflectors\ProphecyArgumentCreator;

class ObjectInjectorContext
{
    const METHOD_ONLY_PROPHECY = 'setupWithMocks';
    const METHOD_ONLY_CONTAINER = 'setupWithContainer';
    const METHOD_CONTAINER_MOCK = 'setupDependencies';

    /**
     * @var Prophet
     */
    private $prophet;

    /**
     * @BeforeScenario
     */
    public function setUpDepsBeforeScenario()
    {
        if (method_exists($this, self::METHOD_CONTAINER_MOCK)) {
            $serviceArguments = ContainerServiceArgumentCreator::retrieve($this, self::METHOD_CONTAINER_MOCK);
            $arguments = ProphecyArgumentCreator::retrieve($this->getProphet(), $this, self::METHOD_CONTAINER_MOCK, $serviceArguments);
            call_user_func_array([$this, self::METHOD_CONTAINER_MOCK], $arguments);
        } else {
            if (method_exists($this, self::METHOD_ONLY_PROPHECY)) {
                $arguments = ProphecyArgumentCreator::retrieve($this->getProphet(), $this, self::METHOD_ONLY_PROPHECY);
                call_user_func_array([$this, self::METHOD_ONLY_PROPHECY], $arguments);
            }

            if (method_exists($this, self::METHOD_ONLY_CONTAINER)) {
                $arguments = ContainerServiceArgumentCreator::retrieve($this, self::METHOD_ONLY_CONTAINER);
                call_user_func_array([$this, self::METHOD_ONLY_CONTAINER], $arguments);
            }
        }
    }

    /**
     * @AfterScenario
     */
    public function setUpDepsAfterScenario()
    {
        $this->getProphet()->checkPredictions();

    }

    /**
     * @return Prophet
     */
    protected function getProphet()
    {
        if ($this->prophet === null) {
            $this->prophet = new Prophet(NULL, new Unwrapper(), NULL);
        }
        return $this->prophet;
    }
}