<?php

namespace TestModule\Annotations;

/**
 * @Annotation
 * @Target({"METHOD"})
 */
class Inject
{
    /**
     * @var array
     */
    private $serviceIds;

    /**
     * @param array $values
     */
    public function __construct(array $values)
    {
        $this->serviceIds = $values['value'];
    }

    /**
     * @return array
     */
    public function getServiceIds()
    {
        return $this->serviceIds;
    }
}