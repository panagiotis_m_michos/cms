<?php

namespace Console;

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Helper\HelperSet;

class ConsoleRunner
{

    /**
     * Run console with the given helperset.
     *
     * @param \Symfony\Component\Console\Helper\HelperSet $helperSet
     * @param \Symfony\Component\Console\Command\Command[] $commands
     * @return void
     */
    static public function run(HelperSet $helperSet, $environment, $commands = array())
    {
        $cli = new Application('Made Simple Group console. Environment ' . $environment, '1.1');
        $cli->setCatchExceptions(TRUE);
        $cli->setHelperSet($helperSet);
        $cli->addCommands($commands);
        $cli->run();
    }

}
