<?php

namespace Console\Helpers;

interface IConnectionFactory
{
    /**
     * @param string $environment
     * @return mixed
     */
    public function create($environment);
}