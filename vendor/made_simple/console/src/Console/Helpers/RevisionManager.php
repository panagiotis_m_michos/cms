<?php

namespace Console\Helpers;

use DibiConnection;
use DibiDriverException;
use SplFileInfo;
use Symfony\Component\Console\Output\OutputInterface;

class RevisionManager
{

    /**
     * @var DibiConnection
     */
    private $connection;

    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * @var bool
     */
    private $debug;

    /**
     * @param DibiConnection $connection
     * @param OutputInterface $output
     * @param bool $debug
     */
    public function __construct(DibiConnection $connection, OutputInterface $output, $debug = FALSE)
    {
        $this->connection = $connection;
        $this->output = $output;
        $this->debug = $debug;
    }

    /**
     * @param SplFileInfo[] $schemaFiles
     */
    public function dropTables($schemaFiles)
    {
        foreach ($schemaFiles as $file) {
            $table = $file->getBasename('.sql');
            $checkTableQuery = "SHOW TABLES LIKE '" . $table . "'";
            $result = $this->connection->query($checkTableQuery);
            if ($result->getRowCount() == 1) {
                $this->printDebug("Dropping table " . $table);
                $query = "DROP TABLE " . $table;
                try {
                    $this->connection->query($query);
                    $this->printDebug("Success", "info");
                } catch(DibiDriverException $e) {
                    $this->output->writeln("<error>ERROR dropping table " . $table . "</error>");
                    $this->output->writeln("<error>" . $e->getMessage() . "</error>");
                    $this->output->writeln("<error>Try fixing the problem and run this script again with the --drop option</error>");
                    throw $e;
}
            }
        }
    }

    /**
     * @param SplFileInfo[] $schemaFiles
     */
    public function generateSchemas($schemaFiles)
    {
        foreach ($schemaFiles as $file) {
            $table = $file->getBasename('.sql');
            $checkTableQuery = "SHOW TABLES LIKE '" . $table . "'";
            $result = $this->connection->query($checkTableQuery);

            if ($result->getRowCount() < 1) {
                $query = file_get_contents($file->getRealPath());
                try {
                    $this->connection->query($query);
                    $this->printDebug("Successfully created table >> " . $table, "info");
                } catch (DibiDriverException $e) {
                    $this->output->writeln("<error>ERROR executing " . $file->getRealPath() . "</error>");
                    $this->output->writeln("<error>" . $e->getMessage() . "</error>");
                    $this->output->writeln("<error>Try fixing the problem and run this script again with the --drop option</error>");
                    exit;
                }
            } else {
                $this->printDebug("ERROR executing " . $file->getRealPath(), "error");
                $this->printDebug("Table " . $table . " already exists!", "error");
                $this->printDebug("");
                continue;
            }
        }
    }

    /**
     * @param SplFileInfo[] $revisions
     */
    public function applyRevisions($revisions)
    {
        foreach ($revisions as $file) {
            $this->printDebug("Executing >> " . $file, "comment");
            try {
                $this->connection->loadFile($file);
                $this->printDebug("Success", "info");
            } catch (DibiDriverException $e) {
                $this->printDebug("ERROR: " . $e->getMessage(), "error");
            }
        }
        $this->printDebug("");
    }

    /**
     * @param string $msg
     * @param string $msgType
     */
    private function printDebug($msg, $msgType = NULL)
    {
        if ($this->debug) {
            if (isset($msgType)) {
                $this->output->writeln("<" . $msgType . ">" . $msg . "</" . $msgType . ">");
            } else {
                $this->output->writeln($msg);
            }
        }
    }
}