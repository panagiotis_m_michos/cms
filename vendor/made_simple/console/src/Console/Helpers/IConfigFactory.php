<?php

namespace Console\Helpers;

use Console\ISshConnection;
use Console\Sync\IDatabaseConfig;
use Console\Sync\IRemoteDatabaseConfig;

interface IConfigFactory
{
    /**
     * @return IRemoteDatabaseConfig
     */
    public function createRemote();

    /**
     * @param string $environment
     * @return IDatabaseConfig
     */
    public function createLocal($environment);

    /**
     * @return ISshConnection
     */
    public function createSsh();
}