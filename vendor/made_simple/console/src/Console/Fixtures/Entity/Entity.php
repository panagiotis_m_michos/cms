<?php

return <<< 'EOT'
<?php
    
namespace Entities;

use Doctrine\ORM\Mapping as Orm;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @Orm\Table(name="[TABLE_NAME]")
 * @Orm\Entity(repositoryClass="Repositories\[ENTITY_NAME]Repository")
 */
class [ENTITY_NAME] extends \Object
{
    /**
     * @var integer $[LCFIRST_ENTITY_NAME]Id
     *
     * @Orm\Column(name="[LCFIRST_ENTITY_NAME]Id", type="integer", nullable=FALSE)
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $[LCFIRST_ENTITY_NAME]Id;

    /**
     * @var string $
     *
     * @Orm\Column(name="", type="string", length=255, nullable=TRUE)
     */
    private $;


    /**
     * @var \DateTime
     * 
     * @Orm\Column(name="dtc", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;

    /**
     * @var \DateTime
     *
     * @Orm\Column(name="dtm", type="datetime")
     * @Gedmo\Timestampable(on="create", on="update")
     */
    private $dtm;
    

    public function get[ENTITY_NAME]Id()
    {
        return $this->[LCFIRST_ENTITY_NAME]Id;
    }



    public function getDtc()
    {
        return $this->dtc;
    }

    public function setDtc(\DateTime $dtc)
    {
        $this->dtc = $dtc;
    }

    public function getDtm()
    {
        return $this->dtm;
    }

    public function setDtm(\DateTime $dtm)
    {
        $this->dtm = $dtm;
    }
}

EOT;
