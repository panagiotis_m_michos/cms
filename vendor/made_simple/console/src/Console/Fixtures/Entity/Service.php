<?php
return <<< 'EOT'
<?php
    
namespace DoctrineServices;

use Entities\[ENTITY_NAME];

/**
 * @Service(entity="Entities\[ENTITY_NAME]")
 */
class [ENTITY_NAME]Service extends BaseService_Abstract
{
    public function save([ENTITY_NAME] $[LCFIRST_ENTITY_NAME])
    {
        $this->repository->saveEntity($[LCFIRST_ENTITY_NAME]);
    }
}

EOT;
        