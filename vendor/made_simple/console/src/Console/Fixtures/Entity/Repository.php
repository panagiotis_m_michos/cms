<?php

return <<< 'EOT'
<?php
    
namespace Repositories;

class [ENTITY_NAME]Repository extends BaseRepository_Abstract
{

    /**
     * @param \DateTime $dtc
     * @return \Doctrine\ORM\Internal\Hydration\IterableResult
     */
    public function getLaterThan(\DateTime $dtc)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('i')->from('Entities\[ENTITY_NAME]', 'i');
        $qb->where('i.dtc > ?1')->setParameter(1, $dtc, \Doctrine\DBAL\Types\Type::DATETIME);     
        $iterator = $qb->getQuery()->iterate();
        return $iterator;
    }

}

EOT;
