<?php

namespace Console\Commands;

use Console\ISshConnection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UnexpectedValueException;

class DeployCommand extends Command
{
    /**
     * @var ISshConnection
     */
    private $sshConnection;

    /**
     * @param ISshConnection $sshConnection
     * @param string $name
     */
    public function __construct(ISshConnection $sshConnection, $name = NULL)
    {
        $this->sshConnection = $sshConnection;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('deploy')
            ->setDescription('Deploy application on the server side');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws UnexpectedValueException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("<info>Connecting to the server <comment>" . $this->sshConnection->getProjectName() . "</comment></info>");
        $command = sprintf(
            'ssh %s@%s "cd %s && git pull origin master"',
            $this->sshConnection->getUser(),
            $this->sshConnection->getIp(),
            $this->sshConnection->getProjectDirectory()
        );
        $output->writeln(system($command, $returnVal));
        if ($returnVal !== 0) {
            throw new UnexpectedValueException("Deploying failed! Command exited with status $returnVal!");
        }
        $output->writeln("<info>Done.</info>");
    }

}
