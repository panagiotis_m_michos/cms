<?php

namespace Console\Commands;

use Console\Helpers\IConnectionFactory;
use Console\Helpers\RevisionManager;
use DirectoryIterator;
use InvalidArgumentException;
use SplFileInfo;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Environment;



class GenerateDbCommand extends Command
{
    /**
     * @var array
     */
    private $availableEnvironments = array(Environment::CONSOLE, Environment::DEVELOPMENT);

    /**
     * @var string
     */
    private $schemasDir;

    /**
     * @var string
     */
    private $revisionsDir;

    /**
     * @var IConnectionFactory
     */
    private $connectionFactory;


    /**
     *
     * @param string $schemasDir
     * @param string $revisionsDir
     * @param IConnectionFactory $connectionFactory
     * @param string $name
     */
    public function __construct($schemasDir, $revisionsDir, IConnectionFactory $connectionFactory, $name = NULL)
    {
        $this->schemasDir = $schemasDir;
        $this->revisionsDir = $revisionsDir;
        $this->connectionFactory = $connectionFactory;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('gen:db')
            ->setDescription('Generate DB')
            ->addOption(
                'drop', 'D', InputOption::VALUE_NONE, 'If set, the script will try to drop the existing tables first!'
            )
            ->addOption(
                'environment', 'e', InputOption::VALUE_OPTIONAL, 'Specify environment which should be taken', Environment::DEVELOPMENT
            )
            ->addOption(
                'debug', 'd', InputOption::VALUE_NONE, 'If set the script will produce info output for every query it runs.'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $environment = $input->getOption('environment');
        if (!in_array($environment, $this->availableEnvironments)) {
            throw new InvalidArgumentException("Environment $environment is not available. Available environments : " . implode(', ', $this->availableEnvironments));
        }

        $connection = $this->connectionFactory->create($environment);
        $revisionManager = new RevisionManager($connection, $output, $input->getOption('debug'));

        $connection->query('SET FOREIGN_KEY_CHECKS = 0');

        $output->writeln("<fg=green;options=bold>Using database {$connection->getConfig('database')}</fg=green;options=bold>");

        $schemaFiles = $this->getSqlFiles($this->schemasDir);
        $revisionFiles = $this->getRevisionFiles($this->revisionsDir);

        if ($input->getOption('drop')) {
            $revisionManager->dropTables($schemaFiles);
        }

        $output->writeln("<fg=green;options=bold>Database is being generated.</fg=green;options=bold>");
        $output->writeln("<fg=green;options=bold>Generating Schemas</fg=green;options=bold>");

        $revisionManager->generateSchemas($schemaFiles);

        $output->writeln("<fg=green;options=bold>Applying Revisions</fg=green;options=bold>");
        $revisionManager->applyRevisions($revisionFiles);
        $connection->query('SET FOREIGN_KEY_CHECKS = 1');

        $output->writeln("<fg=green;options=bold>Done!</fg=green;options=bold>");
    }


    /**
     * @param $schemasDir
     * @return SplFileInfo[]
     */
    private function getSqlFiles($schemasDir)
    {
        $sqlFiles = array();
        $iterator = new DirectoryIterator($schemasDir);
        foreach ($iterator as $file) {
            if ($file->getExtension() === "sql" && !$file->isDot() && !$file->isDir()) {
                $splFile = new SplFileInfo($file->getRealPath());
                $sqlFiles[] = $splFile;
            }
        }
        return $sqlFiles;
    }

    /**
     * @param $revisionsDir
     * @return SplFileInfo[]
     */
    private function getRevisionFiles($revisionsDir)
    {
        $revisionFiles = $revisionsArray = array();
        $revisions = new DirectoryIterator($revisionsDir);

        foreach ($revisions as $rev) {
            if ($rev->isDir() && !$rev->isDot()) {
                $revisionsArray[] = $rev->getRealPath();
            }
        }

        asort($revisionsArray);

        foreach ($revisionsArray as $revisionDir) {
            $sqlFiles = $this->getSqlFiles($revisionDir);
            foreach ($sqlFiles as $file) {
                $revisionFiles[] = $file;
            }
        }
        return $revisionFiles;
    }


}
