<?php

namespace Console\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use DBV;
use ReflectionClass;

class SyncDbvCommand extends Command
{

    /**
     * @var DBV
     */
    private $dbv;

    /**
     * @param DBV $dbv
     * @param string $name
     */
    public function __construct(DBV $dbv, $name = NULL)
    {
        $this->dbv = $dbv;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('sync:dbv')
            ->setDescription('Sync dbv changes to local database');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("<info>Updating datbase to latest revision.</info>");
        //push non existing tables
        $this->updateSchemas($this->dbv, $output);
        //update files
        $this->updateRevisions($this->dbv, $output);
        $output->writeln("<info>Done.</info>");
    }

    /**
     * @param DBV $dbv
     * @param OutputInterface $output
     */
    private function updateSchemas(DBV $dbv, OutputInterface $output)
    {
        $items = $this->callMethod($dbv, '_getSchema');
        foreach ($items as $key => $item) {
            if (!empty($item['disk']) && empty($item['database'])) {
                $output->writeln("<info>Creating schema <comment>$key</comment></info>");
                $this->callMethod($dbv, '_createSchemaObject', $key);
            }
        }
    }

    /**
     * @param DBV $dbv
     * @param OutputInterface $output
     */
    private function updateRevisions(DBV $dbv, OutputInterface $output)
    {
        $revisions = $this->callMethod($dbv, '_getRevisions');
        $current_revision = $this->callMethod($dbv, '_getCurrentRevision');
        if (count($revisions)) {
            sort($revisions);
            foreach ($revisions as $revision) {
                if ($revision > $current_revision) {
                    $files = $this->callMethod($dbv, '_getRevisionFiles', $revision);
                    if (count($files)) {
                        foreach ($files as $file) {
                            $file = DBV_REVISIONS_PATH . DIRECTORY_SEPARATOR . $revision . DIRECTORY_SEPARATOR . $file;
                            $this->callMethod($dbv, '_runFile', $file);
                            $output->writeln("<info>Updating revision <comment>$revision</comment> file <comment>$file</comment></info>");
                        }
                    }
                    $this->callMethod($dbv, '_setCurrentRevision', $revision);
                }
            }
        }
    }

    /**
     * @param DBV $dbv
     * @param string $methodName
     * @param mixed $arg
     * @return mixed
     */
    private function callMethod(DBV $dbv, $methodName, $arg = NULL)
    {
        $reflection = new ReflectionClass($dbv);
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(TRUE);
        return $method->invoke($dbv, $arg);
    }
}
