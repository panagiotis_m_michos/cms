<?php

namespace Console\Commands;

use Console\Gen\IStructureConfig;
use Doctrine\ORM\Mapping\Driver\DatabaseDriver;
use Doctrine\ORM\Tools\DisconnectedClassMetadataFactory as Metafactory;
use Doctrine\ORM\Tools\EntityGenerator;
use Doctrine\ORM\EntityManager;
use InvalidArgumentException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateEntityCommand extends Command
{
    /**
     * @var IStructureConfig
     */
    private $structureConfig;

    /**
     * @var EntityGenerator
     */
    private $generator;
    
    /**
     * @var Metafactory 
     */
    private $cmf;
    
    /**
     * @var EntityManager 
     */
    private $em;
    
    /**
     * @var DatabaseDriver 
     */
    private $driverImpl;

    /**
     * @param IStructureConfig $structureConfig
     * @param EntityManager $em
     * @param EntityGenerator $entityGenerator
     * @param \Console\Commands\DisconnectedClassMetadataFactory|\Doctrine\ORM\Tools\DisconnectedClassMetadataFactory $cmf
     * @param \DatabaseDriver|\Doctrine\ORM\Mapping\Driver\DatabaseDriver $dataDriver
     * @param string $name
     */
    public function __construct(IStructureConfig $structureConfig, EntityManager $em, EntityGenerator $entityGenerator, Metafactory $cmf, DatabaseDriver $dataDriver, $name = NULL)
    {
        $this->structureConfig = $structureConfig;
        $this->generator = $entityGenerator;
        $this->cmf = $cmf;
        $this->em = $em;
        $this->driverImpl = $dataDriver;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('gen:entity')
            ->setDescription('Generate Service, Repository and Entity')
            ->addArgument('EntityName', InputArgument::REQUIRED, 'Provide EntityName')
            ->addOption('table', 't', InputOption::VALUE_OPTIONAL, 'Provide TableName')
            ->addOption('fixPropertyNames', NULL, NULL, 'If generated entity properties are in wrong case');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $tableName = $input->getOption('table');
        $fixNames = $input->getOption('fixPropertyNames');
        $name = ucfirst($input->getArgument('EntityName'));
        $output->writeln("<info>Generating entity <comment>$name</comment></info>");
        $this->generateEntity($name, $tableName, $fixNames);
        $output->writeln("<info>Generating repository <comment>$name</comment></info>");
        $this->generateRepository($name);
        $output->writeln("<info>Generating service <comment>$name</comment></info>");
        $this->generateService($name);
    }

    /**
     * @param string $entityName
     * @param string $tableName
     * @param bool $fixNames
     * @throws InvalidArgumentException
     * @return bool|int
     */
    public function generateEntity($entityName, $tableName, $fixNames = FALSE)
    {
        $filePath = $this->structureConfig->getEntitiesDir() . DIRECTORY_SEPARATOR . $entityName . '.php';
        if (file_exists($filePath)) {
            throw new InvalidArgumentException("File $filePath already exists!");
        }
        //using doctrine generator
        if ($tableName) {
            return $this->generateEntityFromTable($entityName, $tableName, dirname($filePath), $fixNames);
        } else {
            $lcEntityName = lcfirst($entityName);
            $tableName = $lcEntityName . 's';
            $placeholders = array('[ENTITY_NAME]', '[TABLE_NAME]', '[LCFIRST_ENTITY_NAME]');
            $replacements = array($entityName, $tableName, $lcEntityName);
            $entityTemplate = $this->getGeneratedTemplate('Entity.php', $placeholders, $replacements);
            return file_put_contents($filePath, $entityTemplate);
        }
    }

    /**
     * @param string $entityName
     * @throws InvalidArgumentException
     * @return bool|int
     */
    public function generateRepository($entityName)
    {
        $filePath = $this->structureConfig->getRepositoriesDir() . DIRECTORY_SEPARATOR . $entityName . 'Repository.php';
        if (file_exists($filePath)) {
            throw new InvalidArgumentException("File $filePath already exists!");
        }

        $lcEntityName = lcfirst($entityName);
        $placeholders = array('[ENTITY_NAME]', '[LCFIRST_ENTITY_NAME]');
        $replacements = array($entityName, $lcEntityName);
        $repositoryTemplate = $this->getGeneratedTemplate('Repository.php', $placeholders, $replacements);
        return file_put_contents($filePath, $repositoryTemplate);
    }

    /**
     * @param string $entityName
     * @throws InvalidArgumentException
     * @return bool|int
     */
    public function generateService($entityName)
    {
        $filePath = $this->structureConfig->getServicesDir() . DIRECTORY_SEPARATOR . $entityName . 'Service.php';
        if (file_exists($filePath)) {
            throw new InvalidArgumentException("File $filePath already exists!");
        }

        $lcEntityName = lcfirst($entityName);
        $placeholders = array('[ENTITY_NAME]', '[LCFIRST_ENTITY_NAME]');
        $replacements = array($entityName, $lcEntityName);
        $serviceTemplate = $this->getGeneratedTemplate('Service.php', $placeholders, $replacements);
        return file_put_contents($filePath, $serviceTemplate);
    }

    /**
     * @param string $entityName
     * @param string $tableName
     * @param string $path
     * @param bool $fixNames
     * @throws InvalidArgumentException
     * @return bool
     */
    private function generateEntityFromTable($entityName, $tableName, $path, $fixNames = FALSE)
    {
        $connection = $this->em->getConnection();
        $schemaManager = $connection->getSchemaManager();

        if (!$schemaManager->tablesExist($tableName)) {
            throw new InvalidArgumentException("Table $tableName does not exist in database {$connection->getDatabase()}!");
        }
        $entityTable = array($schemaManager->listTableDetails($tableName));
        $this->driverImpl->setClassNameForTable($tableName, $entityName);
        //fix field names
        if ($fixNames) {
            $columns = $schemaManager->listTableColumns($tableName);
            foreach ($columns as $column) {
                $this->driverImpl->setFieldNameForColumn($tableName, $column->getName(), $column->getName());
            }
        }
        $this->driverImpl->setTables($entityTable, array());

        $this->em->getConfiguration()->setMetadataDriverImpl($this->driverImpl);

        $this->cmf->setEntityManager($this->em);
        $metadata = $this->cmf->getAllMetadata();

        $this->generator->setGenerateAnnotations(TRUE);
        $this->generator->setGenerateStubMethods(TRUE);
        $this->generator->setRegenerateEntityIfExists(FALSE);
        $this->generator->setUpdateEntityIfExists(FALSE);
        $this->generator->setClassToExtend('Object');
        $this->generator->setAnnotationPrefix('Orm\\');
        $this->generator->generate($metadata, $path);

        return TRUE;
    }

    /**
     * @param string $fileName
     * @param array $placeholders
     * @param array $replacements
     * @return mixed
     */
    private function getGeneratedTemplate($fileName, array $placeholders, array $replacements)
    {
        $templatePath = $this->structureConfig->getTemplatesDir() . DIRECTORY_SEPARATOR . $fileName;
        $template = include $templatePath;
        return str_replace($placeholders, $replacements, $template);
    }
}
