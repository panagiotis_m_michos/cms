<?php

namespace Console\Commands;

use Console\Helpers\IConfigFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use UnexpectedValueException;
use InvalidArgumentException;
use Environment;

class SyncNodeCommand extends Command
{
    /**
     * @var array
     */
    private $availableEnvironments = array(Environment::CONSOLE, Environment::DEVELOPMENT);

    /**
     * @var IConfigFactory
     */
    private $configFactory;

    /**
     * @param IConfigFactory $configFactory
     * @param string $name
     */
    public function __construct(IConfigFactory $configFactory, $name = NULL)
    {
        parent::__construct($name);
        $this->configFactory = $configFactory;
    }

    protected function configure()
    {
        $this->setName('sync:nodes')
            ->setDescription('Sync nodes from remote to local database')
            ->addOption(
                'environment', 'e', InputOption::VALUE_OPTIONAL, 'Specify environment which should be taken', Environment::DEVELOPMENT
            )
            ->addOption(
                'useSsh', 's', InputOption::VALUE_NONE, 'Specify this if you need to use ssh'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws InvalidArgumentException
     * @throws UnexpectedValueException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $localDatabaseConfig = NULL;
        $environment = $input->getOption('environment');
        if (!in_array($environment, $this->availableEnvironments)) {
            throw new InvalidArgumentException("Environment $environment is not available. Available environments: " . implode(', ', $this->availableEnvironments));
        } else {
            $localDatabaseConfig = $this->configFactory->createLocal($environment);
        }

        $useSsh = $input->getOption('useSsh');
        $sshConfig = $this->configFactory->createSsh();

        $filename = tempnam(sys_get_temp_dir(), 'nodes');
        $output->writeln("<info>Connecting to the server <comment>{$sshConfig->getProjectName()}</comment></info>");

        $remoteDatabaseConfig = $this->configFactory->createRemote();
        //tables from the database to dump
        if ($useSsh) {
            $dumpCommand = sprintf(
                'ssh %s@%s "mysqldump -h %s -u %s --password=%s %s %s" > %s',
                $sshConfig->getUser(),
                $sshConfig->getIp(),
                $remoteDatabaseConfig->getHost(),
                $remoteDatabaseConfig->getUsername(),
                $remoteDatabaseConfig->getPassword(),
                $remoteDatabaseConfig->getDatabase(),
                $remoteDatabaseConfig->getTablesString(),
                $filename
            );
        } else {
            $dumpCommand = sprintf(
                'mysqldump -h %s -u %s --password=%s %s %s > %s',
                $remoteDatabaseConfig->getHost(),
                $remoteDatabaseConfig->getUsername(),
                $remoteDatabaseConfig->getPassword(),
                $remoteDatabaseConfig->getDatabase(),
                $remoteDatabaseConfig->getTablesString(),
                $filename
            );
        }

        $importCommand = sprintf(
            'mysql -h %s -u %s --password=%s %s < %s',
            $localDatabaseConfig->getHost(),
            $localDatabaseConfig->getUsername(),
            $localDatabaseConfig->getPassword(),
            $localDatabaseConfig->getDatabase(),
            $filename
        );

        $output->writeln(
            sprintf(
                "<info>Dumping remote <comment>%s</comment> database (tables: <comment>%s</comment>).</info>",
                $remoteDatabaseConfig->getDatabase(),
                $remoteDatabaseConfig->getTablesString()
            )
        );
        system($dumpCommand, $returnVal);

        if (!file_exists($filename)) {
            throw new UnexpectedValueException("Dumping database failed! File $filename does not exist!");
        }
        if ($returnVal !== 0) {
            throw new UnexpectedValueException("Dumping database failed! Command exited with status $returnVal!");
        }

        $output->writeln("<info>Importing to <comment>{$localDatabaseConfig->getDatabase()}</comment> database.</info>");
        system($importCommand, $returnVal);
        if ($returnVal !== 0) {
            throw new UnexpectedValueException("Importing database failed! Command exited with status $returnVal!");
        }
        $output->writeln("<info>Done.</info>");
    }

}
