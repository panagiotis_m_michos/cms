<?php

namespace Console\Commands;

use Doctrine\ORM\EntityManager;
use Nelmio\Alice\Fixtures;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;

class GenerateDataCommand extends Command
{
    const FIXTURES_PATH = 'storage/fixtures/';

    /**
     * @var array
     */
    private $fixturesFiles;

    /**
     * @param array $fixturesFiles
     * @param string $name
     */
    public function __construct($fixturesFiles, $name = NULL)
    {
        $this->fixturesFiles = $fixturesFiles;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('gen:data')
            ->setDescription('Load data fixtures to your database.')
            ->addOption(
                'truncate',
                't',
                InputOption::VALUE_NONE,
                'Deletes all data from the database first.'
            )
            ->setHelp(
                <<<EOT
The <info>doctrine:fixtures:load</info> command loads data fixtures.
 
If you want to delete all the data in the database first, you can use the <info>--truncate</info> option:
 
  <info>console/nikolai doctrine:fixtures:load --truncate</info>
EOT
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fixtureFiles = array();
        $em = $this->getHelper('em')->getEntityManager();

        foreach ($this->fixturesFiles as $file) {
            $fixtureFiles[] = self::FIXTURES_PATH . $file;
        }

        if ($input->getOption('truncate')) {
            $purger = new ORMPurger($em);
            $purger->setPurgeMode(ORMPurger::PURGE_MODE_TRUNCATE);
            $purger->purge();
        }

        Fixtures::load($fixtureFiles, $em, array('locale' => 'en_GB'));
    }
}