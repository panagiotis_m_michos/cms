<?php

namespace Console;

interface ISshConnection
{
    public function getProjectName();
    public function getProjectDirectory();
    public function getUser();
    public function getIp();
}
