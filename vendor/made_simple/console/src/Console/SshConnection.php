<?php

namespace Console;

class SshConnection implements ISshConnection
{
    /**
     * @var string
     */
    private $projectName;

    /**
     * @var string
     */
    private $projectDirectory;

    /**
     * @var string
     */
    private $user;

    /**
     * @var string
     */
    private $ip;

    /**
     * @param string $projectName
     * @param string $projectDirectory
     * @param string $user
     * @param string $ip
     */
    function __construct($projectName, $projectDirectory, $user, $ip)
    {
        $this->projectName = $projectName;
        $this->projectDirectory = $projectDirectory;
        $this->user = $user;
        $this->ip = $ip;
    }

    /**
     * @return string
     */
    public function getProjectName()
    {
        return $this->projectName;
    }

    /**
     * @return string
     */
    public function getProjectDirectory()
    {
        return $this->projectDirectory;
    }


    /**
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        if (getenv('MSG_VPN_IP_DIFF')) {
            return str_replace('100', getenv('MSG_VPN_IP_DIFF'), $this->ip);
        }
        return $this->ip;
    }
}
