<?php

namespace Console\Sync;

class RemoteDatabaseConfig extends DatabaseConfig implements IRemoteDatabaseConfig
{
    /**
     * @var array
     */
    private $tables;

    /**
     * @param string $host
     * @param string $username
     * @param string $password
     * @param string $database
     * @param array $tables
     */
    public function __construct($host, $username, $password, $database, array $tables)
    {
        parent::__construct($host, $username, $password, $database);
        $this->tables = $tables;
    }

    /**
     * @return array
     */
    public function getTables()
    {
        return $this->tables;
    }

    /**
     * @return string
     */
    public function getTablesString()
    {
        return implode(' ', $this->tables);
    }
}
