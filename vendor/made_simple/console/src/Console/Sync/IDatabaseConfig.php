<?php

namespace Console\Sync;

interface IDatabaseConfig
{
    public function getHost();
    public function getUsername();
    public function getPassword();
    public function getDatabase();
}
