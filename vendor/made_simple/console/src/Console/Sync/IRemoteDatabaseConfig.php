<?php

namespace Console\Sync;

interface IRemoteDatabaseConfig extends IDatabaseConfig
{
    /**
     * @return array
     */
    public function getTables();
    public function getTablesString();
}
