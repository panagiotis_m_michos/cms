<?php

namespace Console\Gen;

class StructureConfig implements IStructureConfig
{
    /**
     * @var string
     */
    private $entitiesDir;

    /**
     * @var string
     */
    private $repositoriesDir;

    /**
     * @var string
     */
    private $servicesDir;

    /**
     * @var string
     */
    private $templatesDir;

    /**
     * @param string $entitiesDir
     * @param string $repositoriesDir
     * @param string $servicesDir
     * @param string $templatesDir
     */
    public function __construct($entitiesDir, $repositoriesDir, $servicesDir, $templatesDir)
    {
        $this->entitiesDir = $entitiesDir;
        $this->repositoriesDir = $repositoriesDir;
        $this->servicesDir = $servicesDir;
        $this->templatesDir = $templatesDir;
    }

    /**
     * @return string
     */
    public function getEntitiesDir()
    {
        return $this->entitiesDir;
    }

    /**
     * @return string
     */
    public function getRepositoriesDir()
    {
        return $this->repositoriesDir;
    }

    /**
     * @return string
     */
    public function getServicesDir()
    {
        return $this->servicesDir;
    }

    /**
     * @return string
     */
    public function getTemplatesDir()
    {
        return $this->templatesDir;
    }
}
