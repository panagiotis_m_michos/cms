<?php

namespace Console\Gen;

interface IStructureConfig
{
    public function getEntitiesDir();
    public function getRepositoriesDir();
    public function getServicesDir();
    public function getTemplatesDir();
}
