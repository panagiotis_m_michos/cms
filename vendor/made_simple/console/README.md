# Console tool to manage running cli commands

When implemented inside project can be run as:

~~~
php console/cm [command] [options]
~~~

To see full options available run:

~~~
php console/cm [command] --help
~~~

Commands:

 - sync:nodes
 - gen:db


## sync:nodes

Dumps tables from the remote database specified in production enviroment configuration and imports it to the local database

** accepts the following options: **

> --useSsh[-s] use ssh connection first to dump database changes

> --enviroment[-e] local environment configuration to use for importing tables (default: development) (accepts console/development)

** optional steps: **

> if youre connecting with vpn which contains different ip (not 192.168.100.36 but 192.168.98.36)
add `export MSG_VPN_IP_DIFF="98"` to your profile or bashrc and source it (source ~/.profile)

## gen:db

Generates the local database as specified in development enviroment configuration

** accepts the following options: **

> --drop[-D] drops existing tables

> --enviroment[-e] local environment configuration to use for dabase tables (default: development)

> --debug[-d] prints info messages for every query it runs
