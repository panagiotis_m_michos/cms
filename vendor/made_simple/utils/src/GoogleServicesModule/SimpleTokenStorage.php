<?php

namespace GoogleServicesModule;

class SimpleTokenStorage implements TokenStorage
{
    /**
     * @var string
     */
    private $token;

    /**
     * @param string $token
     */
    public function store($token)
    {
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function get()
    {
        return $this->token;
    }

    /**
     * @return bool
     */
    public function isEmpty()
    {
        return $this->token === NULL;
    }
}
