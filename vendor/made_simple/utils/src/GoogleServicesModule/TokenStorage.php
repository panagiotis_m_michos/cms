<?php
namespace GoogleServicesModule;

interface TokenStorage
{
    /**
     * @param string $token
     */
    public function store($token);

    /**
     * @return string
     */
    public function get();

    /**
     * @return bool
     */
    public function isEmpty();
}
