<?php

namespace GoogleServicesModule;

use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Google_Auth_AssertionCredentials;

class CredentialsFactory
{
    /**
     * @param string $serviceAccountName
     * @param array $serviceScopes
     * @param string $keyFilePath
     * @return Google_Auth_AssertionCredentials
     */
    public static function getCredentials($serviceAccountName, array $serviceScopes, $keyFilePath)
    {
        if (!is_file($keyFilePath)) {
            throw new FileNotFoundException($keyFilePath);
        }

        $key = file_get_contents($keyFilePath);
        return new Google_Auth_AssertionCredentials(
            $serviceAccountName,
            $serviceScopes,
            $key
        );
    }
}
