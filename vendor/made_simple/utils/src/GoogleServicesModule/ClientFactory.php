<?php

namespace GoogleServicesModule;

use Google_Auth_AssertionCredentials;
use Google_Client;
use Google_Service_Books;

class ClientFactory
{
    /**
     * @param Google_Auth_AssertionCredentials $credentials
     * @param TokenStorage $tokenStorage
     * @param string $applicationName
     * @return Google_Client
     */
    public static function getClient(
        Google_Auth_AssertionCredentials $credentials,
        TokenStorage $tokenStorage,
        $applicationName
    )
    {
        $client = new Google_Client();
        $client->setApplicationName($applicationName);

        if (!$tokenStorage->isEmpty()) {
            $client->setAccessToken($tokenStorage->get());
        }

        $client->setAssertionCredentials($credentials);

        if ($client->getAuth()->isAccessTokenExpired()) {
            $client->getAuth()->refreshTokenWithAssertion($credentials);
        }

        $tokenStorage->store($client->getAccessToken());

        return $client;
    }
}
