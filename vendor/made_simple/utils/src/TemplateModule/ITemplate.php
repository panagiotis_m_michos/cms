<?php

namespace TemplateModule;

interface ITemplate
{

    /**
     * @return mixed
     */
    public function getEngine();

    /**
     * @param string $path
     * @return string
     */
    public function getHtml($path);

    /**
     * @param string $key
     * @param mixed $value
     * @return mixed
     */
    public function __set($key, $value);
}