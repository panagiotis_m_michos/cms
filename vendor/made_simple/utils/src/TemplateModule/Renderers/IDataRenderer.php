<?php

namespace TemplateModule\Renderers;

use InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;
use TemplateModule\Exceptions\TemplateException;

interface IDataRenderer
{
    /**
     * @param array $data
     * @param Response|NULL $response
     * @return Response
     * @throws TemplateException
     * @throws InvalidArgumentException
     */
    public function render(array $data = [], Response $response = NULL);
}
