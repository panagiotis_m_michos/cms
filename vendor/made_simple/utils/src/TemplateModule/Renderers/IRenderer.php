<?php

namespace TemplateModule\Renderers;

use Symfony\Component\HttpFoundation\Response;
use Utils\File;

interface IRenderer extends IDataRenderer
{
    /**
     * @param File $file
     * @param array $data
     * @param Response|NULL $response
     * @return Response
     */
    public function renderTemplate(File $file, array $data = [], Response $response = NULL);
}
