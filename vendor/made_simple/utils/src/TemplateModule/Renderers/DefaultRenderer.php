<?php

namespace TemplateModule\Renderers;

use InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;
use TemplateModule\Exceptions\TemplateException;
use TemplateModule\ITemplatePathProvider;
use TemplateModule\PlaceholderProviders\IPlaceholderProvider;
use Utils\File;

final class DefaultRenderer implements IRenderer
{
    /**
     * @var ITemplatePathProvider
     */
    private $templatePathProvider;

    /**
     * @var IPlaceholderProvider
     */
    private $placeholderProvider;


    /**
     * @param ITemplatePathProvider $templatePathProvider
     * @param IPlaceholderProvider $placeholderProvider
     */
    public function __construct(ITemplatePathProvider $templatePathProvider, IPlaceholderProvider $placeholderProvider)
    {
        $this->templatePathProvider = $templatePathProvider;
        $this->placeholderProvider = $placeholderProvider;
    }

    /**
     * @param array $data
     * @param Response|NULL $response
     * @return Response
     * @throws TemplateException
     * @throws InvalidArgumentException
     */
    public function render(array $data = [], Response $response = NULL)
    {
        return $this->renderTemplate($this->templatePathProvider->getTemplateFile(), $data, $response);
    }

    /**
     * @param File $file
     * @param array $data
     * @param Response|NULL $response
     * @return Response
     */
    public function renderTemplate(File $file, array $data = [], Response $response = NULL)
    {
        $template = $this->placeholderProvider->setPlaceholders($data);
        $content = $template->getHtml($file->getPath());
        $response = $response ? $response : new Response();
        $response->setContent($content);
        return $response;
    }

}
