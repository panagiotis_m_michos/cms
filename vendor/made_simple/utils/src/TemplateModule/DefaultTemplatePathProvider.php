<?php

namespace TemplateModule;

use InvalidArgumentException;
use RouterModule\RouteParameter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use TemplateModule\Exceptions\TemplateException;
use Utils\Directory;
use Utils\File;

final class DefaultTemplatePathProvider implements ITemplatePathProvider
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var RouteParameter
     */
    private $routeParameter;

    /**
     * @var string
     */
    private $templateFolderName;

    /**
     * @var Directory
     */
    private $rootDir;

    /**
     * @var string
     */
    private $templateExt;

    /**
     * @param ContainerInterface $container
     * @param RouteParameter $routeParameter
     * @param Directory $rootDir
     * @param string $templateFolderName
     * @param string $templateExt
     */
    public function __construct(ContainerInterface $container,
                                RouteParameter $routeParameter,
                                Directory $rootDir,
                                $templateFolderName = 'Templates',
                                $templateExt = 'tpl')
    {
        $this->container = $container;
        $this->routeParameter = $routeParameter;
        $this->templateFolderName = $templateFolderName;
        $this->rootDir = $rootDir;
        $this->templateExt = $templateExt;
    }

    /**
     * @return File
     * @throws TemplateException
     * @throws InvalidArgumentException
     */
    public function getTemplateFile()
    {
        $controller = $this->container->get($this->routeParameter->getController());
        $namespace = get_class($controller);
        $namespacePath = explode('\\', $namespace);
        //replace second part with Templates
        if (!isset($namespacePath[1])) {
            throw TemplateException::wrongNamespace($namespace);
        }
        $namespacePath[1] = $this->templateFolderName;
        return File::fromExisting(
            Directory::append($this->rootDir, implode(DIRECTORY_SEPARATOR, $namespacePath)),
            $this->routeParameter->getAction() . '.' . $this->templateExt
        );
    }
}