<?php

namespace TemplateModule\PlaceholderProviders;

use TemplateModule\ITemplate;

interface IPlaceholderProvider
{
    /**
     * @param array $data
     * @return ITemplate
     */
    public function setPlaceholders(array $data = []);
}