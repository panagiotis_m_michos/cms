<?php

namespace TemplateModule\Exceptions;

use Exception;
use RuntimeException;

class TemplateException extends RuntimeException
{
    /**
     * @param string $path
     * @param Exception $e
     * @return TemplateException
     */
    public static function notFound($path, Exception $e = NULL)
    {
        return new self(sprintf('Template does not exist %s', $path), $e ? $e->getCode() : NULL, $e);
    }

    /**
     * @param string $namespace
     * @return TemplateException
     */
    public static function wrongNamespace($namespace)
    {
        return new self(sprintf('Unsupported %s namespace for template found!', $namespace));
    }
}