<?php

namespace TemplateModule;

use Utils\File;

interface ITemplatePathProvider
{
    /**
     * @return File
     */
    public function getTemplateFile();

}