<?php

namespace TemplateModule;

use DOMDocument;
use DOMXPath;
use FileNotFoundException;
use TemplateModule\PlaceholderProviders\IPlaceholderProvider;

class HtmlExtractor
{
    /**
     * @var string
     */
    private $rootDir;

    /**
     * @var IPlaceholderProvider
     */
    private $placeholderProvider;

    /**
     * @var ITemplate
     */
    private $cachedTemplate;

    /**
     * @param IPlaceholderProvider $placeholderProvider
     * @param $rootDir
     */
    public function __construct(IPlaceholderProvider $placeholderProvider, $rootDir)
    {
        $this->rootDir = $rootDir;
        $this->placeholderProvider = $placeholderProvider;
    }

    /**
     * @param string $xPath
     * @param string $templatePath
     * @param array $templateDependancies
     * @return array
     * @throws FileNotFoundException
     */
    public function extract($xPath, $templatePath, array $templateDependancies)
    {
        $this->cachedTemplate = $template = clone ($this->cachedTemplate ? $this->cachedTemplate : $this->placeholderProvider->setPlaceholders());
        foreach ($templateDependancies as $key => $value) {
            $template->$key = $value;
        }
        $html = $template->getHtml($this->rootDir . DIRECTORY_SEPARATOR . $templatePath);
        $dom = new DOMDocument('1.0', 'UTF-8');
        // Encoding issues with domdocument
        //$isLoaded = @$dom->loadHTML('<?xml encoding="UTF-8">' . $html);
        $isLoaded = @$dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));
        $xpath = new DOMXpath($dom);
        $elements = $xpath->query($xPath);
        $arr = [];
        foreach ($elements as $element) {
            $innerHtml = '';
            foreach ($element->childNodes as $child) {
                $innerHtml .= $elementHtml = $dom->saveHTML($child);
            }
            $arr[] = $innerHtml;
        }
        return $arr;
    }

    /**
     * @param string $xPath
     * @param string $templatePath
     * @param array $objects
     * @param string $templateKey
     * @return array
     * @throws FileNotFoundException
     */
    public function extractArray($xPath, $templatePath, array $objects, $templateKey = NULL)
    {
        $arr = [];
        foreach ($objects as  $object) {
            $arr[] = $this->extract($xPath, $templatePath, [$templateKey => $object]);
        }
        return $arr;
    }

}