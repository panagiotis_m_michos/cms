<?php

namespace LogModule;

use ILogger;
use LogModule\Exceptions\LoggerException;
use Psr\Log\AbstractLogger;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

class LoggerInterfaceAdapter extends AbstractLogger implements LoggerInterface
{
    /**
     * @var ILogger
     */
    private $logger;

    /**
     * @param ILogger $logger
     */
    public function __construct(ILogger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed $level
     * @param string $message
     * @param array $context
     * @return null
     */
    public function log($level, $message, array $context = [])
    {
        $translatedLevel = $this->translateLogLevel($level);
        $this->logger->logMessage($translatedLevel, $message);
    }

    /**
     * Translate log level from LoggerInterface to ILogger
     *
     * @param string $level
     * @throws LoggerException
     * @return string
     */
    protected function translateLogLevel($level)
    {
        $dictionary = [
            LogLevel::ALERT => ILogger::ALERT,
            LogLevel::CRITICAL => ILogger::CRITICAL,
            LogLevel::DEBUG => ILogger::DEBUG,
            LogLevel::EMERGENCY => ILogger::EMERGENCY,
            LogLevel::ERROR => ILogger::ERROR,
            LogLevel::INFO => ILogger::INFO,
            LogLevel::NOTICE => ILogger::NOTICE,
            LogLevel::WARNING => ILogger::WARNING,
        ];

        if (!isset($dictionary[$level])) {
            throw LoggerException::wrongLogLevel($level);
        }

        return $dictionary[$level];
    }
}