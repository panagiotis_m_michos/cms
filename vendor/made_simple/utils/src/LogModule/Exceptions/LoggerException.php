<?php

namespace LogModule\Exceptions;

use Psr\Log\InvalidArgumentException;

class LoggerException extends InvalidArgumentException
{
    /**
     * @param int $level
     * @return LoggerException
     */
    public static function wrongLogLevel($level)
    {
        return new self("Wrong log level received: {$level}");
    }
}
