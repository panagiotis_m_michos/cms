<?php

namespace OrmModule\Contracts;

interface IRepository
{
    /**
     * @param mixed $entity
     * @return mixed
     */
    public function saveEntity($entity);

    /**
     * @param mixed $id
     * @return mixed
     */
    public function find($id);

    /**
     * @param array $query
     * @param array $orderBy
     * @return mixed
     */
    public function findOneBy(array $query, array $orderBy = []);
}