<?php

namespace BasketModule\Contracts;

interface IBasket
{
    /**
     * @param float $credit
     */
    public function setCredit($credit);

    /**
     * @return float
     */
    public function getCredit();

    /**
     * @return bool
     */
    public function isUsingCredit();

    /**
     * @return bool
     */
    public function isFreePurchase();

    /**
     * @return float
     */
    public function getTotalPrice();


    /**
     * @return bool
     */
    public function isEmpty();
}