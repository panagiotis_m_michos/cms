<?php

namespace VatModule\Calculators;

use VatModule\ValueObjects\VatRate;

class VatCalculator
{
    /**
     * @var VatRate
     */
    private $rate;

    /**
     * @param VatRate $rate
     */
    public function __construct(VatRate $rate)
    {
        $this->rate = $rate;
    }

    /**
     * @param float $amount
     * @return float
     */
    public function getVatFromTotal($amount)
    {
        return ($amount * $this->rate->getRate()) / ($this->rate->getRate() + 100);
    }

    /**
     * @param $amount
     * @return mixed
     */
    public function getVatFromSubtotal($amount)
    {
        return $amount * ($this->rate->getRate() / 100);
    }

    /**
     * @param float $amount
     * @return float
     */
    public function getSubtotalFromTotal($amount)
    {
        return $amount / (($this->rate->getRate() + 100) / 100);
    }
}
