<?php

namespace VatModule\ValueObjects;

use InvalidArgumentException;

class VatRate
{
    /**
     * @var int
     */
    private $rate;

    /**
     * @param int $rate
     */
    public function __construct($rate)
    {
        if (!is_int($rate) || $rate <= 0) {
            throw new InvalidArgumentException("VAT rate has to be positive integer value, {$rate} given");
        }

        $this->rate = $rate;
    }

    /**
     * @return int
     */
    public function getRate()
    {
        return $this->rate;
    }
}
