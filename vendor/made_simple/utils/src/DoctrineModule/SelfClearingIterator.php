<?php

namespace DoctrineModule;

use Countable;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use IteratorIterator;

class SelfClearingIterator extends IteratorIterator implements  Countable
{
    /**
     * @var QueryBuilder
     */
    private $queryBuilder;

    /**
     * @var int
     */
    private $clearStep;

    /**
     * @var int
     */
    private $counter = 0;

    /**
     * @param QueryBuilder $queryBuilder
     * @param int $clearStep
     */
    public function __construct(QueryBuilder $queryBuilder, $clearStep = 100)
    {
        parent::__construct($queryBuilder->getQuery()->iterate());
        $this->queryBuilder = $queryBuilder;
        $this->clearStep = $clearStep;
    }

    /**
     * @return mixed
     */
    public function current()
    {
        $current = parent::current();
        return $current ? $current[0] : $current;
    }

    /**
     * @return mixed
     */
    public function next()
    {
        if ($this->counter > 0 && $this->counter % $this->clearStep == 0) {
            foreach ($this->queryBuilder->getRootEntities() as $entityType) {
                $this->queryBuilder->getEntityManager()->clear($entityType);
            }
        }
        $this->counter++;
        parent::next();
        return self::current();
    }

    /**
     * @return int
     */
    public function count()
    {
        $rootEntityAlias = $this->queryBuilder->getRootAlias();
        $builder = $this->queryBuilder;
        return $builder->select("count($rootEntityAlias)")->getQuery()->getSingleScalarResult();
    }
}