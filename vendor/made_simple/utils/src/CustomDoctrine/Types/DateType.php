<?php

namespace CustomDoctrine\Types;

use Doctrine\DBAL\Types\DateType as DoctrineDateType;
use Doctrine\DBAL\Types\ConversionException;
use Utils\Date;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class DateType extends DoctrineDateType
{

    /**
     * @param mixed $value
     * @param AbstractPlatform $platform
     * @return NULL|Date
     * @throws ConversionException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === NULL || $value instanceof Date) {
            return $value;
        }

        $val = Date::createFromFormat($platform->getDateFormatString(), $value);
        if (!$val) {
            throw ConversionException::conversionFailedFormat($value, $this->getName(), $platform->getDateFormatString());
        }
        return $val;
    }
}
