<?php

namespace TranslationModule\Config;

class MsgId
{
    const GENERIC_RESPONSE_ERROR = 'Oops... something went wrong while processing your request! Please contact our team by phone or email';
    const PAYMENT_METHOD_NOT_ALLOWED = 'Selected payment type %s is not allowed';
    const NOT_ENOUGH_CREDIT = 'You do not have enough credit. Current credit: %d Requested credit: %d';
    const PAYMENT_CANCELED = 'The transaction was cancelled.';
    const PAYMENT_DATA_EMPTY = 'Sorry not payment information found';
    const PAYMENT_FAILED = 'Payment failed';
    const CUSTOMER_NOT_LOGGED_IN = 'Customer is not logged in. Please login first.';
    const CUSTOMER_NOT_TEMPORARY = 'No temporary customer found. Please login first.';
    const CUSTOMER_EMAIL_TAKEN = 'Email %s is already taken';
    const CUSTOMER_EMAIL_NOT_FOUND = 'Email %s does not exist';
    const TRANSACTION_USED = 'Transaction was not finished. Please resubmit the form.';
    const PAYMENT_ORDER_FAILED = 'Order could not be completed successfully. Please contact our team by phone or email';
    const PAYMENT_CODE_2000 = 'This could be due to funds not being available, spending limit reached or bank account closure. We advise that you contact your card issuer for more information. Alternatively you can attempt the transaction again using another card.';
    const PAYMENT_CODE_2001 = 'This is often due to the billing address, postcode or security code not matching the details held by the card issuer. Please double check all the details you have entered and try again. If you have received this error multiple times we advise that you contact your card issuer for more information.';
}