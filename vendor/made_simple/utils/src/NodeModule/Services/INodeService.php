<?php

namespace NodeModule\Services;

use FNode;

interface INodeService {

    /**
     * @param int $nodeId
     * @param bool $need
     * @return FNode
     */
    public function getNodeById($nodeId, $need = FALSE);
}
