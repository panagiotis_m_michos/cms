<?php

namespace Utils\Text\Parser;

use Utils\Text\Parser\Filters\Interfaces\IFilter;

class Parser
{
    /**
     * @var IFilter[]
     */
    private $filters;

    /**
     * @param IFilter $filter
     */
    public function addFilter(IFilter $filter)
    {
        $this->filters[] = $filter;
    }

    /**
     * @param string $input
     * @return string
     */
    public function parse($input)
    {
        $matches = $this->matchFilters($input);
        return str_replace(
            array_keys($matches),
            array_values($matches),
            $input
        );
    }

    /**
     * @param string $input
     * @return array
     */
    private function matchFilters($input)
    {
        $matches = array();
        foreach ($this->filters as $filter) {
            $matches = array_merge($matches, $filter->match($input));
        }
        return $matches;
    }
}
