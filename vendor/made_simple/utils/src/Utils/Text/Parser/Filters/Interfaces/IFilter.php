<?php

namespace Utils\Text\Parser\Filters\Interfaces;

interface IFilter 
{
    /**
     * @param string $input
     * @return array
     */
    public function match($input);
}