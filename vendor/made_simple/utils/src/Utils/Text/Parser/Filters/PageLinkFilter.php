<?php

namespace Utils\Text\Parser\Filters;

use Nette\Object;
use Utils\Text\Parser\Filters\Interfaces\IFilter;
use Utils\Text\Parser\Filters\Interfaces\IRouter;

class PageLinkFilter extends Object implements IFilter
{
    /**
     * @var IRouter
     */
    private $router;

    /**
     * @param IRouter $router
     */
    public function __construct(IRouter $router)
    {
        $this->router = $router;
    }

    /**
     * Match [page page_id]
     *
     * @param string $input
     * @return string
     */
    public function match($input)
    {
        $matches = array();
        preg_match_all('#\[page (\d+)\]#i', $input, $output);
        if (isset($output[0])) {
            $output[0] = array_unique($output[0]);
            foreach ($output[0] as $key => $val) {
                if (isset($output[1][$key])) {
                    $matches[$val] = $this->router->link($output[1][$key]);
                }
            }
        }
        return $matches;
    }
}