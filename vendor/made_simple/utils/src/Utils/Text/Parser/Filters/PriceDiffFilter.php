<?php

namespace Utils\Text\Parser\Filters;

use Nette\Object;
use Utils\Text\Parser\Filters\Interfaces\IFilter;
use Utils\Text\Parser\Filters\Interfaces\IProductRepository;

class PriceDiffFilter extends Object implements IFilter
{
    /**
     * @var IProductRepository
     */
    private $productRepository;

    /**
     * @param IProductRepository $productRepository
     */
    public function __construct(IProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * Match: [price id]
     *
     * @param string $input
     * @return string
     */
    public function match($input)
    {
        $matches = array();
        preg_match_all('#\[priceDiff (\d+) (\d+)\]#i', $input, $output, PREG_PATTERN_ORDER);
        if (isset($output[0])) {
            $output[0] = array_unique($output[0]);
            foreach ($output[0] as $key => $val) {
                if (isset($output[1][$key], $output[2][$key])) {
                    $price1 = $this->productRepository->getProductPrice($output[1][$key]);
                    $price2 = $this->productRepository->getProductPrice($output[2][$key]);
                    if ($price1 && $price2) {
                        $matches[$val] = $price1 - $price2;
                    }
                }
            }
        }
        return $matches;
    }
}
