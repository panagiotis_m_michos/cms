<?php

namespace Utils\Text\Parser\Filters;

use Nette\Object;
use Utils\Text\Parser\Filters\Interfaces\IFilter;
use Utils\Text\Parser\Filters\Interfaces\IProductRepository;

class CashBackAmountFilter extends Object implements IFilter
{
    /**
     * @var IProductRepository
     */
    private $productRepository;

    /**
     * @param IProductRepository $productRepository
     */
    public function __construct(IProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * Match: [price id]
     *
     * @param string $input
     * @return string
     */
    public function match($input)
    {
        $matches = [];
        preg_match_all('#\[cashBackAmount (\d+)\]#i', $input, $output, PREG_PATTERN_ORDER);
        if (isset($output[0])) {
            $output[0] = array_unique($output[0]);
            foreach ($output[0] as $key => $val) {
                if (isset($output[1][$key])) {
                    $price = $this->productRepository->getProductCashBackAmount($output[1][$key]);
                    if (isset($price)) {
                        $matches[$val] = $price;
                    }
                }
            }
        }

        return $matches;
    }
}
