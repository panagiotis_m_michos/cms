<?php

namespace Utils\Text\Parser\Filters\UiElements;

interface IElement 
{
    /**
     * @param array $defaultOptions
     */
    public function setDefaultOptions(array $defaultOptions);

    /**
     * @param array $args
     * @return string
     */
    public function render(array $args);
}
