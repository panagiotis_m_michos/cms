<?php

namespace Utils\Text\Parser\Filters;

use Nette\Object;
use Utils\Text\Parser\Filters\Interfaces\IFilter;
use Utils\Text\Parser\Filters\UiElements\IElement;

class UiFilter extends Object implements IFilter
{
    /**
     * @var IElement[]
     */
    private $elements;

    /**
     * @param string $name
     * @param IElement $element
     */
    public function addElement($name, IElement $element)
    {
        $this->elements[$name] = $element;
    }

    /**
     * Match [ui key="val"]
     *
     * @param string $input
     * @return string
     */
    public function match($input)
    {
        $matches = array();
        preg_match_all('#\[ui\s.*?\]#' , $input, $output);
        if (isset($output[0])) {
            $output[0] = array_unique($output[0]);
            foreach ($output[0] as $value) {
                preg_match_all('#\s(.*?)="(.*?)"#', $value, $attributes);
                if (isset($attributes[1], $attributes[2])) {
                    $args = array_combine($attributes[1], $attributes[2]);
                    if (isset($args['name']) && isset($this->elements[$args['name']])) {
                        $element = $this->elements[$args['name']];
                        $rendered = $element->render($args);
                        $matches[$value] = preg_replace("/[\r\n]+/", "", $rendered);
                    }
                }
            }
        }
        return $matches;
    }
}
