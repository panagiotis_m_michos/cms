<?php

namespace Utils\Text\Parser\Filters\Interfaces;

interface IProductRepository 
{
    /**
     * @param int $productId
     * @return float
     */
    public function getProductPrice($productId);

    /**
     * @param int $productId
     * @return int
     */
    public function getProductCashBackAmount($productId);
}
