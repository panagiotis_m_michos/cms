<?php

namespace Utils\Text\Parser\Filters\Interfaces;

interface IRouter
{
    /**
     * @param string $code
     * @param array $params
     * @return string
     */
    public function link($code = NULL, $params = NULL);
}
