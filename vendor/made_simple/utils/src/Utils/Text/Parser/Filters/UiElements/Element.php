<?php

namespace Utils\Text\Parser\Filters\UiElements;

use UiHelper\ViewHelper;

class Element implements IElement
{
    /**
     * @var ViewHelper
     */
    private $viewHelper;

    /**
     * @var array
     */
    private $defaultOptions = [];

    /**
     * @param ViewHelper $viewHelper
     */
    public function __construct(ViewHelper $viewHelper)
    {
        $this->viewHelper = $viewHelper;
    }

    /**
     * @param array $defaultOptions
     */
    public function setDefaultOptions(array $defaultOptions)
    {
        $this->defaultOptions = $defaultOptions;
    }

    /**
     * @param array $args
     * @return string
     */
    public function render(array $args)
    {
        $options = array_merge($this->defaultOptions, $args);

        return $this->viewHelper->render($options);
    }
}
