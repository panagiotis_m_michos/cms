<?php

namespace Utils\Text\Parser\Filters;

use Nette\Object;
use Utils\Text\Parser\Filters\Interfaces\IFilter;
use Utils\Text\Parser\Filters\Interfaces\IRouter;

class BasketLinkFilter extends Object implements IFilter
{
    /**
     * @var IRouter
     */
    private $router;

    /**
     * @var int
     */
    private $basketPageId;

    /**
     * @var string
     */
    private $action;

    /**
     * @param IRouter $router
     * @param int $basketPageId
     * @param string $action
     */
    public function __construct(IRouter $router, $basketPageId, $action)
    {
        $this->router = $router;
        $this->basketPageId = (int) $basketPageId;
        $this->action = $action;
    }

    /**
     * Match: [product product_id]
     *
     * @param string $input
     * @return string
     */
    public function match($input)
    {
        $matches = array();
        preg_match_all('#\[product (\d+)\]#i', $input, $output, PREG_PATTERN_ORDER);
        if (isset($output[0])) {
            $output[0] = array_unique($output[0]);
            foreach ($output[0] as $key => $val) {
                if (isset($output[1][$key])) {
                    $matches[$val] = $this->router->link($this->basketPageId, array($this->action => $output[1][$key]));
                }
            }
        }
        return $matches;
    }
}