<?php

namespace Utils;

use InvalidArgumentException;
use SplFileInfo;
use Symfony\Component\HttpFoundation\File\File as SymfonyFile;

class File extends SymfonyFile
{
    /**
     * @param string $path
     */
    public function __construct($path)
    {
        parent::__construct($path, FALSE);
    }

    /**
     * @param SplFileInfo $splFile
     * @return File
     */
    public static function fromSpl(SplFileInfo $splFile)
    {
        return new self($splFile->getRealPath());
    }

    /**
     * @param Directory $directory
     * @param $fileName
     * @return File
     * @throws InvalidArgumentException
     */
    public static function fromExisting(Directory $directory, $fileName)
    {
        $file = self::fromDir($directory, $fileName);
        if (!$file->isReadable()) {
            throw new InvalidArgumentException(sprintf('File %s is not readable', $file->getPath()));
        }
        return $file;
    }

    /**
     * @param Directory $directory
     * @param $fileName
     * @return File
     * @throws InvalidArgumentException
     */
    public static function fromDir(Directory $directory, $fileName)
    {
        $filePath = $directory->getPath() . DIRECTORY_SEPARATOR . $fileName;
        if (dirname($filePath) !== $directory->getPath()) {
            throw new InvalidArgumentException(sprintf('Filename %s is not valid', $fileName));
        }
        $file = new self($filePath);
        return $file;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->getRealPath();
    }

    /**
     * @return string
     * @throws InvalidArgumentException
     */
    public function getContent()
    {
        if (!$this->isReadable()) {
            throw new InvalidArgumentException(sprintf('File %s is not readable', $this->getPathname()));
        }
        return file_get_contents($this->getPath());
    }
}
