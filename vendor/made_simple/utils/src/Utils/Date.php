<?php

namespace Utils;

use DateTime;
use DateTimeZone;
use Utils\Exceptions\InvalidDateException;
use Exception;

class Date extends DateTime
{
    const FORMAT_DATE = 'Y-m-d';

    /**
     * @param string $time
     * @param DateTimeZone $timezone
     * @throws InvalidDateException
     */
    public function __construct($time = "now", DateTimeZone $timezone = NULL)
    {
        try {
            parent::__construct($time, $timezone);
            $this->setTime(0, 0, 0);
        } catch (Exception $e) {
            throw new InvalidDateException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param string $from
     * @param string $to
     * @return bool
     */
    public function isBetween($from, $to)
    {
        $fromDate = new Date($from);
        $toDate = new Date($to);
        return $this >= $fromDate && $this <= $toDate;
    }

    /**
     * @param mixed $format
     * @param mixed $time
     * @param DateTimeZone $timezone
     * @return Date|bool
     */
    public static function createFromFormat($format, $time, $timezone = NULL)
    {
        $dateTime = parent::createFromFormat($format, $time);
        $formatedDate = $dateTime ? $dateTime->format(self::FORMAT_DATE) : FALSE;
        return $formatedDate ? new self($formatedDate, $timezone) : FALSE;
    }

    /**
     * @param string $date
     * @param string $fromFormat
     * @param string $toFormat
     * @return string|bool
     */
    public static function changeFormat($date, $fromFormat, $toFormat)
    {
        $dateObject = self::createFromFormat($fromFormat, $date);
        return $dateObject ? $dateObject->format($toFormat) : FALSE;
    }

    /**
     * @param mixed $format
     * @param mixed $time
     * @param DateTimeZone $timezone
     * @return Date
     * @throws InvalidDateException
     */
    public static function create($format, $time, $timezone = NULL)
    {
        $dateTime = parent::createFromFormat($format, $time);
        if (!$dateTime) {
            throw new InvalidDateException(sprintf('Could not create %s date from format %s', $time, $format));
        }
        return new self($dateTime->format(self::FORMAT_DATE), $timezone);
    }

    /**
     * @param DateTime $dateTime
     * @return Date
     */
    public static function createFromDateTime(DateTime $dateTime)
    {
        return Date::create(self::FORMAT_DATE, $dateTime->format(self::FORMAT_DATE));
    }

    public function __toString()
    {
        return $this->format('d/m/Y');
    }
}
