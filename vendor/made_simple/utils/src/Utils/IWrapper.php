<?php

namespace Utils;

interface IWrapper
{
    const REQUIRED = TRUE;

    // make sure its array
    const SIMPLE_ARRAY = 1;

    /**
     * @param string $name
     * @return mixed
     */
    public function getValue($name);

    /**
     * @param string $name
     * @return IWrapper
     */
    public function getWrappedValue($name);

    /**
     * @param string $name
     * @return array
     */
    public function getArray($name);

    /**
     * @param $name
     * @return WrappedIterator
     */
    public function getWrappedIterator($name);
}