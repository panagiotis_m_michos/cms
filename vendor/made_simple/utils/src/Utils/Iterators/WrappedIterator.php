<?php

namespace Utils\Iterators;

use ArrayIterator;
use Utils\Wrapper;

class WrappedIterator extends ArrayIterator
{
    public function current()
    {
        return Wrapper::wrap(parent::current());
    }
}