<?php

namespace Utils;

use InvalidArgumentException;

class Directory
{

    /**
     * @var string
     */
    private $path;

    /**
     * @param string $path
     */
    public function __construct($path)
    {
        $this->path = $path;
    }

    /**
     * @param string $path
     * @return Directory
     * @throws InvalidArgumentException
     */
    public static function createWritable($path)
    {
        if (!is_writable($path)) {
            if (!file_exists($path)) {
                @mkdir($path, 0777, TRUE);
            } else {
                @chmod($path, 0777);
            }
            if (!is_writable($path)) {
                throw new InvalidArgumentException(sprintf('Path %s is not writable', $path));
            }
        }
        $directory = new self($path);
        return $directory;
    }

    /**
     * @param string $path
     * @return Directory
     * @throws InvalidArgumentException
     */
    public static function fromPath($path)
    {
        if (!is_readable($path)) {
            throw new InvalidArgumentException(sprintf('Path %s is not readable', $path));
        }
        $directory = new self($path);
        return $directory;
    }

    /**
     * @param Directory $directory
     * @param string $path
     * @return Directory
     */
    public static function append(Directory $directory, $path)
    {
        return self::fromPath($directory->getPath() . DIRECTORY_SEPARATOR . $path);
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }


    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getPath();
    }
}
