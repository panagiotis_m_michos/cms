<?php

namespace Utils;

use SimpleXMLElement;

class XmlElement extends SimpleXMLElement
{
    /**
     * @param string $name
     */
    public function getValue($name)
    {
        return isset($this->$name) && count($this->$name->children()) === 0 
            ? (string) $this->$name : NULL;
    }

    /**
     * @param string $name
     * @return array
     */
    public function getArray($name)
    {
        return $this->toArray($name);
    }

    /**
     * @param mixed $name
     * @return array
     */
    public function toArray($name)
    {
        $arr = array();
        if (isset($this->$name)) {
            foreach ($this->$name as $child) {
                $arr[] = $this->xml2array($child);
            }
        }
        return $arr;
    }

    /**
     * @param mixed $xmlObject
     * @param array $out
     * @return array
     */
    public function xml2array($xmlObject, $out = array())
    {
        foreach ((array) $xmlObject as $index => $node) {
            $out[$index] = ( is_object($node) ) ? $this->xml2array($node) : $node;
        }
        return $out;
    }

}
