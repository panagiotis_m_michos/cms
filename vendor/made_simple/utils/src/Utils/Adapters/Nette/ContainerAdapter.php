<?php

namespace Utils\Adapters\Nette;

use BadMethodCallException;
use Nette\DI\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\DependencyInjection\ScopeInterface;

class ContainerAdapter implements ContainerInterface
{

    /**
     * @var Container
     */
    private $container;

    /**
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Sets a service.
     *
     * @param string $id The service identifier
     * @param object $service The service instance
     * @param string $scope The scope of the service
     *
     * @api
     */
    public function set($id, $service, $scope = self::SCOPE_CONTAINER)
    {
        throw new BadMethodCallException('Not implemented');
    }

    /**
     * Gets a service.
     *
     * @param string $id The service identifier
     * @param int $invalidBehavior The behavior when the service does not exist
     *
     * @return object The associated service
     *
     * @throws InvalidArgumentException          if the service is not defined
     * @throws ServiceCircularReferenceException When a circular reference is detected
     * @throws ServiceNotFoundException          When the service is not defined
     *
     * @see Reference
     *
     * @api
     */
    public function get($id, $invalidBehavior = self::EXCEPTION_ON_INVALID_REFERENCE)
    {
        return $this->container->getService($id);
    }

    /**
     * Returns true if the given service is defined.
     *
     * @param string $id The service identifier
     *
     * @return bool true if the service is defined, false otherwise
     *
     * @api
     */
    public function has($id)
    {
        throw new BadMethodCallException('Not implemented');
    }

    /**
     * Gets a parameter.
     *
     * @param string $name The parameter name
     *
     * @return mixed The parameter value
     *
     * @throws InvalidArgumentException if the parameter is not defined
     *
     * @api
     */
    public function getParameter($name)
    {
        $parameters = $this->container->getParameters();
        if (!isset($parameters[$name])) {
            throw new InvalidArgumentException(sprintf('Parameter %s not found', $name));
        }
        return $parameters[$name];
    }

    /**
     * Checks if a parameter exists.
     *
     * @param string $name The parameter name
     *
     * @return bool The presence of parameter in container
     *
     * @api
     */
    public function hasParameter($name)
    {
        throw new BadMethodCallException('Not implemented');
    }

    /**
     * Sets a parameter.
     *
     * @param string $name The parameter name
     * @param mixed $value The parameter value
     *
     * @api
     */
    public function setParameter($name, $value)
    {
        throw new BadMethodCallException('Not implemented');
    }

    /**
     * Enters the given scope.
     *
     * @param string $name
     *
     * @api
     */
    public function enterScope($name)
    {
        throw new BadMethodCallException('Not implemented');
    }

    /**
     * Leaves the current scope, and re-enters the parent scope.
     *
     * @param string $name
     *
     * @api
     */
    public function leaveScope($name)
    {
        throw new BadMethodCallException('Not implemented');
    }

    /**
     * Adds a scope to the container.
     *
     * @param ScopeInterface $scope
     *
     * @api
     */
    public function addScope(ScopeInterface $scope)
    {
        throw new BadMethodCallException('Not implemented');
    }

    /**
     * Whether this container has the given scope.
     *
     * @param string $name
     *
     * @return bool
     *
     * @api
     */
    public function hasScope($name)
    {
        throw new BadMethodCallException('Not implemented');
    }

    /**
     * Determines whether the given scope is currently active.
     *
     * It does however not check if the scope actually exists.
     *
     * @param string $name
     *
     * @return bool
     *
     * @api
     */
    public function isScopeActive($name)
    {
        throw new BadMethodCallException('Not implemented');
    }

}