<?php

namespace Utils\Helpers;

use Traversable;

class StringHelper
{
    /**
     * @param string $constant
     * @return string
     */
    public static function humanize_constant($constant)
    {
        return mb_convert_case(mb_strtolower(str_replace('_', ' ', $constant)), MB_CASE_TITLE);
    }

    /**
     * @param array|string $constants
     * @param string $delimiter
     * @return string
     */
    public static function humanize_constants($constants, $delimiter = ', ')
    {
        if (is_array($constants) || $constants instanceof Traversable) {
            $constantNames = [];
            foreach ($constants as $constant) {
                $constantNames[] = self::humanize_constant($constant);
            }
            return implode($delimiter, $constantNames);
        }
        return self::humanize_constant($constants);
    }
}