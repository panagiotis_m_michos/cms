<?php

namespace Utils\FeedReader;

use Doctrine\Common\Cache\Cache;
use Guzzle\Common\Exception\GuzzleException;
use Guzzle\Http\ClientInterface;
use Nette\Object;

class FeedReader extends Object
{
    /**
     * @var string
     */
    private $url;
    /**
     * @var Cache
     */
    private $cache;
    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @param string $url
     * @param Cache $cache
     * @param ClientInterface $client
     */
    public function __construct($url, Cache $cache, ClientInterface $client)
    {
        $this->url = $url;
        $this->cache = $cache;
        $this->client = $client;
    }

    /**
     * @param $limit
     * @return Post[]
     */
    public function getPosts($limit)
    {
        $posts = array();
        $cacheKey = 'blog_posts_' . md5($this->url . $limit);
        if ($this->cache->contains($cacheKey)) {
            return $this->cache->fetch($cacheKey);
        }
        try {
            $response = $this->client->get($this->url)->send();
            $xml = $response->xml();
            $count = 0;
            if (isset($xml->channel->item)) {
                foreach ($xml->channel->item as $item) {
                    $posts[] = Post::fromXml($item);
                    $count++;
                    if ($count >= $limit) {
                        break;
                    }
                }

            }
            //should we be more specific ?
        } catch (GuzzleException $e) {
        }
        //save for 5min even in case of a failure
        $this->cache->save($cacheKey, $posts, $posts ? 1800 : 300);
        return $posts;
    }

} 