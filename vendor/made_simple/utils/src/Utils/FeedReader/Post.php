<?php

namespace Utils\FeedReader;

use Nette\Object;
use SimpleXMLElement;

class Post extends Object
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $link;

    /**
     * @var string
     */
    private $description;

    /**
     * @param string $title
     * @param string $link
     * @param string $description
     */
    public function __construct($title, $link, $description)
    {
        $this->title = $title;
        $this->link = $link;
        $this->description = $description;
    }

    /**
     * @param SimpleXMLElement $element
     * @param int $descriptionLength
     * @return Post
     */
    public static function fromXml(SimpleXMLElement $element)
    {
        $title = isset($element->title) ? (string) $element->title : NULL;
        $link = isset($element->link) ? (string) $element->link : NULL;
        $description = isset($element->description) ? (string) $element->description : NULL;
        return new self($title, $link, $description);
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param int $limit
     * @return string
     */
    public function getLimitedDescription($limit)
    {
        return strtok(wordwrap(str_replace("\n", " ", $this->description), $limit, "...\n"), "\n");
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
}