<?php

namespace Utils;

use Symfony\Component\PropertyAccess\Exception\NoSuchIndexException;
use Symfony\Component\PropertyAccess\Exception\NoSuchPropertyException;
use Symfony\Component\PropertyAccess\Exception\UnexpectedTypeException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Utils\Iterators\WrappedIterator;

class Wrapper implements IWrapper
{
    /**
     * @var mixed
     */
    private $object;

    /**
     * @var PropertyAccessor
     */
    private $accessor;

    /**
     * @var bool
     */
    private $throwException;

    private function __construct() {}

    /**
     * @param mixed $object
     * @param PropertyAccessorInterface $accessor
     * @param bool $throwException
     * @return Wrapper
     */
    public static function wrap($object, PropertyAccessorInterface $accessor = NULL, $throwException = FALSE)
    {
        $wrapper = new self();
        $wrapper->object = $object;
        $wrapper->throwException = $throwException;
        if (!$accessor) {
            $builder = PropertyAccess::createPropertyAccessorBuilder()
                ->disableMagicCall()
                ->enableExceptionOnInvalidIndex();
            $accessor = $builder->getPropertyAccessor();
        }
        $wrapper->accessor = $accessor;
        return $wrapper;
    }

    /**
     * @param string $name
     * @param bool $required
     * @return mixed
     * @throws NoSuchPropertyException
     * @throws UnexpectedTypeException
     * @throws NoSuchIndexException
     */
    public function getValue($name, $required = NULL)
    {
        try {
            return $this->accessor->getValue($this->object, $name);
        } catch (UnexpectedTypeException $e) {
            if ($required || $this->throwException) {
                throw $e;
            }
            return NULL;
        } catch (NoSuchPropertyException $e) {
            if ($required || $this->throwException) {
                throw $e;
            }
            return NULL;
        } catch (NoSuchIndexException $e) {
            if ($required || $this->throwException) {
                throw $e;
            }
            return NULL;
        }

    }

    /**
     * @param string $name
     * @param bool $required
     * @return array
     */
    public function getArray($name, $required = NULL)
    {
        $value = $this->getValue($name);
        return $value ? (array) $value : array();
    }

    /**
     * @param string $name
     * @param bool $required
     * @param int $expectingType
     * @return WrappedIterator
     */
    public function getWrappedIterator($name, $required = NULL, $expectingType = NULL)
    {
        $object = $this->getValue($name, $required);
        if ($expectingType === self::SIMPLE_ARRAY) {
            if (!empty($object) && !is_array($object)) {
                $object = array($object);
            }
        }
        return $object ? new WrappedIterator($object) : new WrappedIterator();
    }

    /**
     * @param string $name
     * @param bool $required
     * @return Wrapper
     */
    public function getWrappedValue($name, $required = NULL)
    {
        $object = $this->getValue($name, $required);
        return self::wrap($object);
    }
}