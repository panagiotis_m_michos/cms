<?php

namespace spec\TemplateModule\Renderers;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use TemplateModule\ITemplate;
use TemplateModule\ITemplatePathProvider;
use TemplateModule\PlaceholderProviders\IPlaceholderProvider;
use Utils\File;

class DefaultRendererSpec extends ObjectBehavior
{
    /**
     * @var ITemplatePathProvider
     */
    private $templatePathProvider;

    /**
     * @var IPlaceholderProvider
     */
    private $placeholderProvider;


    function let(ITemplatePathProvider $templatePathProvider, IPlaceholderProvider $placeholderProvider)
    {
        $this->beConstructedWith($templatePathProvider, $placeholderProvider);
        $this->templatePathProvider = $templatePathProvider;
        $this->placeholderProvider = $placeholderProvider;
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('TemplateModule\Renderers\DefaultRenderer');
        $this->shouldHaveType('TemplateModule\Renderers\IRenderer');
    }

    function it_should_render_default_path_template(ITemplate $template, File $file)
    {
        $this->templatePathProvider->getTemplateFile()->willReturn($file);
        $file->getPath()->willReturn('a');
        $template->getHtml('a')->willReturn('<span>1</span><i>2</i>');
        $data = ['a' => 1, 'b' => 2];
        $this->placeholderProvider->setPlaceholders($data)->willReturn($template);
        $this->render($data)->getContent()->shouldBe('<span>1</span><i>2</i>');
    }

    function it_should_render_template(ITemplate $template, File $file)
    {
        $file->getPath()->willReturn('a');
        $template->getHtml('a')->willReturn('<span>1</span><i>2</i>');
        $data = ['a' => 1, 'b' => 2];
        $this->placeholderProvider->setPlaceholders($data)->willReturn($template);
        $this->renderTemplate($file, $data)->getContent()->shouldBe('<span>1</span><i>2</i>');
    }
}
