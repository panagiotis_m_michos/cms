<?php

namespace spec\TemplateModule;

use InvalidArgumentException;
use org\bovigo\vfs\vfsStream;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use RouterModule\RouteParameter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use TemplateModule\Exceptions\TemplateException;
use Utils\Directory;
use Utils\File;

class DefaultTemplatePathProviderSpec extends ObjectBehavior
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var RouteParameter
     */
    private $routeParameter;

    private $rootDir;


    function let(ContainerInterface $container)
    {
        $this->routeParameter = RouteParameter::fromArray(['controller' => 'a', 'action' => 'b']);
        $this->rootDir = vfsStream::setup('app', NULL, [
            'InvalidController.php' => '<?php class InvalidController {};',
            'TestMe' => [
                'Controllers' => [
                    'A.php' => '<?php namespace TestMe\Controllers; class A {};',
                    'B.php' => '<?php namespace TestMe\Controllers; class B {};'
                ],
                'Templates' => [
                    'A' => [
                        $this->routeParameter->getAction() . '.tpl' => 'template content'
                    ]
                ]
            ]
        ]);
        $this->beConstructedWith($container, $this->routeParameter, Directory::fromPath(vfsStream::url('app')));
        $this->container = $container;
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('TemplateModule\DefaultTemplatePathProvider');
        $this->shouldHaveType('TemplateModule\ITemplatePathProvider');
    }

    function it_should_provide_path()
    {
        $filePath = vfsStream::url('app/TestMe/Templates/A/' . $this->routeParameter->getAction() . '.tpl');
        $controllerPath = vfsStream::url('app/TestMe/Controllers/A.php');
        require_once($controllerPath);
        $this->container->get($this->routeParameter->getController())->willReturn(new \TestMe\Controllers\A());
        $file = $this->getTemplateFile();
        $file->getPathname()->shouldBe($filePath);
    }

    function it_should_throw_exception_if_namespace_is_invalid()
    {
        $controllerPath = vfsStream::url('app/InvalidController.php');
        require_once($controllerPath);
        $this->container->get($this->routeParameter->getController())->willReturn(new \InvalidController());
        $this->shouldThrow(TemplateException::class)->during('getTemplateFile');
    }

    function it_should_throw_exception_if_path_does_not_exist()
    {
        $controllerPath = vfsStream::url('app/TestMe/Controllers/B.php');
        require_once($controllerPath);
        $this->container->get($this->routeParameter->getController())->willReturn(new \TestMe\Controllers\B());
        $this->shouldThrow(InvalidArgumentException::class)->during('getTemplateFile');
    }
}
