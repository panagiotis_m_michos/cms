<?php

namespace spec\Utils\Helpers;

use ArrayIterator;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class StringHelperSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Utils\Helpers\StringHelper');
    }

    function it_should_humanize_constant()
    {
        $this::humanize_constant('TSB')->shouldBe('Tsb');
        $this::humanize_constant('CARD_ONE')->shouldBe('Card One');
        //$this::humanize_constant(['tEst'])->shouldBe('');
        $this::humanize_constant('')->shouldBe('');
        $this::humanize_constant(NULL)->shouldBe('');
        $this::humanize_constant(300)->shouldBe('300');
        $this::humanize_constant('正體字/繁體字')->shouldBe('正體字/繁體字');
        $this::humanize_constant(new A('LonG PausE'))->shouldBe('Long Pause');
    }

    function it_should_humanize_constants_and_provide_string()
    {
        $this::humanize_constants('TSB')->shouldBe('Tsb');
        $this::humanize_constants(['tEst', 'CARD_ONE'])->shouldBe('Test, Card One');
        $this::humanize_constants([NULL])->shouldBe('');
        $this::humanize_constants([300, 'aBc'])->shouldBe('300, Abc');
        $this::humanize_constants(['正體字/繁體字', '指事字'])->shouldBe('正體字/繁體字, 指事字');
        $this::humanize_constants(new ArrayIterator([new A('LonG PausE'), new A('LonG PausE')]))->shouldBe('Long Pause, Long Pause');
    }
}

final class A
{
    private $value;

    public function __construct($value)
    {
        $this->value = $value;
    }
    public function __toString()
    {
        return $this->value;
    }
}