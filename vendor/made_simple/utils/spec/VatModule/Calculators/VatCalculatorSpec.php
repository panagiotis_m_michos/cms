<?php

namespace spec\VatModule\Calculators;

use PhpSpec\ObjectBehavior;
use VatModule\Calculators\VatCalculator;
use VatModule\ValueObjects\VatRate;

/**
 * @mixin VatCalculator
 */
class VatCalculatorSpec extends ObjectBehavior
{
    /**
     * @var int
     */
    private $rate = 20;

    function let()
    {
        $this->beConstructedWith(new VatRate($this->rate));
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(VatCalculator::class);
    }

    function it_can_calculate_vat_from_total()
    {
        $this->getVatFromTotal(12)->shouldBeLike(2);
    }

    function it_can_calculate_vat_from_subtotal()
    {
        $this->getVatFromSubtotal(10)->shouldBeLike(2);
    }

    function it_can_calculate_subtotal_from_total()
    {
        $this->getSubtotalFromTotal(12)->shouldBeLike(10);
    }
}
