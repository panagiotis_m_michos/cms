<?php

namespace spec\VatModule\ValueObjects;

use InvalidArgumentException;
use PhpSpec\ObjectBehavior;
use VatModule\ValueObjects\VatRate;

/**
 * @mixin VatRate
 */
class VatRateSpec extends ObjectBehavior
{
    /**
     * @var int
     */
    private $rate;

    function it_is_initializable()
    {
        $this->beConstructedWith(10);
        $this->shouldHaveType(VatRate::class);
    }

    function it_cant_be_created_with_zero_rate()
    {
        $this->beConstructedWith(0);
        $this->shouldThrow(InvalidArgumentException::class)->duringInstantiation();
    }

    function it_cant_be_created_with_negative_rate()
    {
        $this->beConstructedWith(-10);
        $this->shouldThrow(InvalidArgumentException::class)->duringInstantiation();
    }

    function it_cant_be_created_with_non_numeric()
    {
        $this->beConstructedWith('asdf');
        $this->shouldThrow(InvalidArgumentException::class)->duringInstantiation();
    }
}
