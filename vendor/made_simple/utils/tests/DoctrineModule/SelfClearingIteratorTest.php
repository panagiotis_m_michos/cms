<?php

namespace DoctrineModule\Tests;

use DoctrineModule\SelfClearingIterator;
use EntityHelper;
use PHPUnit_Framework_TestCase;

class SelfClearingIteratorTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var SelfClearingIterator
     */
    private $object;

    public function setUp()
    {
        EntityHelper::emptyTables([TBL_CUSTOMERS]);
        $this->customers[] = EntityHelper::createCustomer('test1@madesimplegroup.com');
        $this->customers[] = EntityHelper::createCustomer('test2@madesimplegroup.com');
        $queryBuilder = EntityHelper::getEntityManager()->getRepository('Entities\Customer')->createQueryBuilder('c');
        $this->object = new SelfClearingIterator($queryBuilder, 1);
    }

    public function testForeach()
    {
        $count = 0;
        $this->assertTrue(EntityHelper::getEntityManager()->contains($this->customers[0]));
        $this->assertTrue(EntityHelper::getEntityManager()->contains($this->customers[1]));
        foreach ($this->object as $customer) {
            $this->assertInstanceOf('Entities\Customer', $customer);
            $this->assertEquals(++$count, $customer->getId());
        }
        $this->assertFalse(EntityHelper::getEntityManager()->contains($this->customers[0]));
        $this->assertFalse(EntityHelper::getEntityManager()->contains($this->customers[1]));
        $this->assertEquals(2, $count);
    }

    public function testIterator()
    {
        $this->assertNull($this->object->current());
        $this->assertFalse($this->object->valid());
        $this->assertEquals(NULL, $this->object->key());
        $this->assertInstanceOf('Entities\Customer', $this->object->next());
        $this->assertTrue($this->object->valid());
        $this->assertEquals(0, $this->object->key());
        $this->assertInstanceOf('Entities\Customer', $this->object->current());
        $this->assertInstanceOf('Entities\Customer', $this->object->next());
        $this->assertTrue($this->object->valid());
        $this->assertEquals(1, $this->object->key());
        $this->assertInstanceOf('Entities\Customer', $this->object->current());
        $this->assertNull($this->object->next());
        $this->assertNull($this->object->current());
        $this->assertFalse($this->object->valid());
        $this->assertNull($this->object->key());
    }

    public function testCount()
    {
        $this->assertCount(2, $this->object);
    }
}