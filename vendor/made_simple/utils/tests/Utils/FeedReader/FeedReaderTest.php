<?php

namespace Utils\FeedReader;

use EntityHelper;
use Guzzle\Http\Client;
use Guzzle\Http\Message\Response;
use Guzzle\Plugin\Mock\MockPlugin;
use Guzzle\Tests\GuzzleTestCase;

class FeedReaderTest extends GuzzleTestCase
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var mixed
     */
    private $cache;

    /**
     * @var FeedReader
     */
    private $object;

    /**
     * @var MockPlugin
     */
    private $plugin;

    public function setUp()
    {
        $this->client = new Client();
        $this->cache = $this->getMock('Doctrine\Common\Cache\Cache');
        $this->object = new FeedReader('test', $this->cache, $this->client);
        $this->plugin = new MockPlugin();
        $this->client->addSubscriber($this->plugin);
    }

    /**
     * @covers Utils\FeedReader\FeedReader::getPosts
     */
    public function testGetPostsOk()
    {
        $this->plugin->addResponse(new Response(200, NULL, file_get_contents(__DIR__ . '/rss_feed.xml')));
        $posts = $this->object->getPosts(3);
        $this->assertCount(3, $posts);
        $this->assertEquals('Moving your registered office across the UK – Video', $posts[0]->getTitle());
        $this->assertEquals('Shares and Shareholders: 5 Quick Company Formation Tips', $posts[1]->getTitle());
        $this->assertEquals('Company Formation for Professionals: How do I remove companies from my account?', $posts[2]->getTitle());
    }

    /**
     * @covers Utils\FeedReader\FeedReader::getPosts
     */
    public function testGetPostsUrlNotFound()
    {
        $this->plugin->addResponse(new Response(404, NULL, "test"));
        $posts = $this->object->getPosts(3);
        $this->assertCount(0, $posts);
    }

    /**
     * @covers Utils\FeedReader\FeedReader::getPosts
     */
    public function testGetPostsIncorrectXml()
    {
        $this->plugin->addResponse(new Response(200, NULL, "test"));
        $posts = $this->object->getPosts(3);
        $this->assertCount(0, $posts);
    }

    /**
     * @covers Utils\FeedReader\FeedReader::getPosts
     */
    public function testGetPostsServerError()
    {
        $this->plugin->addResponse(new Response(500, NULL, "test"));
        $posts = $this->object->getPosts(3);
        $this->assertCount(0, $posts);
    }
}
 