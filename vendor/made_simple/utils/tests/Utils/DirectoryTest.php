<?php

namespace Utils;

use InvalidArgumentException;
use org\bovigo\vfs\vfsStream;
use PHPUnit_Framework_TestCase;

class DirectoryTest extends PHPUnit_Framework_TestCase
{

    public function setUp()
    {
        $this->root = vfsStream::setup('level1', 0777, ['level2a' => [], 'level2b' => ['level3' => []]]);
        $this->root->getChild('level2a')->chmod(0444);
    }

    /**
     * @covers Utils\Directory::fromPath
     */
    public function testFromPathSuccess()
    {
        $this->assertInstanceOf('Utils\Directory', Directory::fromPath(vfsStream::url('level1')));
    }

    /**
     * @covers Utils\Directory::fromPath
     * @expectedException InvalidArgumentException
     */
    public function testFromPathFailure()
    {
        $this->assertInstanceOf('Utils\Directory', Directory::fromPath(vfsStream::url('level2')));
    }

    /**
     * @covers Utils\Directory::createWritable
     */
    public function testCreateWritableNonExisting()
    {
        $dir = vfsStream::url('level1/nonexisting');
        $this->assertInstanceOf('Utils\Directory', Directory::createWritable($dir));
        $this->assertTrue(is_writable($dir));
    }

    /**
     * @covers Utils\Directory::createWritable
     */
    public function testCreateWritableExisting()
    {
        $dir = vfsStream::url('level1/level2a');
        $this->assertFalse(is_writable($dir));
        $this->assertInstanceOf('Utils\Directory', Directory::createWritable($dir));
        $this->assertTrue(is_writable($dir));
    }

     /**
     * @covers Utils\Directory::createWritable
     */
    public function testCreateWritable2Nodes()
    {
        $this->assertInstanceOf('Utils\Directory', Directory::createWritable(vfsStream::url('level1/g/j')));
    }
}
 