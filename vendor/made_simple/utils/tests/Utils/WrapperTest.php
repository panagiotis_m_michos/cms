<?php

namespace Utils;

use InvalidArgumentException;
use PHPUnit_Framework_TestCase;
use stdClass;

class WrapperTest extends PHPUnit_Framework_TestCase
{
    /**
     * @covers Utils\Wrapper::wrap
     */
    public function testWrap()
    {
        $wrapper = Wrapper::wrap('people');
        $this->assertInstanceOf('Utils\IWrapper', $wrapper);
        $wrapper = Wrapper::wrap(3);
        $this->assertInstanceOf('Utils\IWrapper', $wrapper);
        $wrapper = Wrapper::wrap(NULL);
        $this->assertInstanceOf('Utils\IWrapper', $wrapper);
        $wrapper = Wrapper::wrap(array('test'));
        $this->assertInstanceOf('Utils\IWrapper', $wrapper);
        $wrapper = Wrapper::wrap(new stdClass());
        $this->assertInstanceOf('Utils\IWrapper', $wrapper);
    }

    /**
     * @covers Utils\Wrapper::getValue
     */
    public function testGetValueObjectOrArray()
    {
        $wrapper = Wrapper::wrap(array('people' => 1));
        $this->assertEquals(1, $wrapper->getValue('[people]'));
        $this->assertEquals(NULL, $wrapper->getValue('people'));
        $object = new stdClass();
        $object->people = 1;
        $wrapper = Wrapper::wrap($object);
        $this->assertEquals(1, $wrapper->getValue('people'));
        $this->assertEquals(NULL, $wrapper->getValue('[people]'));

        $object = new stdClass();
        $object->items = array('item1' => 1);
        $object->other = new stdClass();
        $object->other->items = array(array('other1' => 1));

        $wrapper = Wrapper::wrap($object);
        $this->assertEquals(array('item1' => 1), $wrapper->getValue('items'));
        $this->assertEquals(1, $wrapper->getValue('items[item1]'));
        $this->assertEquals(1, $wrapper->getValue('other.items[0][other1]'));
    }

    /**
     * @covers Utils\Wrapper::getValue
     */
    public function testGetValueOnInvalid()
    {
        $wrapper = Wrapper::wrap('people');
        $this->assertEquals(NULL, $wrapper->getValue('people'));
        $wrapper = Wrapper::wrap(NULL);
        $this->assertEquals(NULL, $wrapper->getValue('[people]'));
        $wrapper = Wrapper::wrap(array('test'));
        $this->assertEquals(NULL, $wrapper->getValue('[people]'));
        $wrapper = Wrapper::wrap(new stdClass());
        $this->assertEquals(NULL, $wrapper->getValue('[people]'));
    }

    /**
     * @covers Utils\Wrapper::getValue
     * @expectedException Symfony\Component\PropertyAccess\Exception\NoSuchIndexException
     */
    public function testGetValueOnInvalidWithRequiredOnArray()
    {
        $wrapper = Wrapper::wrap(array('test' => 'people'));
        $this->assertEquals(NULL, $wrapper->getValue('[people]', IWrapper::REQUIRED));
    }

    /**
     * @covers Utils\Wrapper::getValue
     * @expectedException Symfony\Component\PropertyAccess\Exception\NoSuchPropertyException
     */
    public function testGetValueOnInvalidWithRequiredOnObject()
    {
        $object = new stdClass();
        $object->test = 2;
        $wrapper = Wrapper::wrap($object);
        $this->assertEquals(NULL, $wrapper->getValue('people', IWrapper::REQUIRED));
    }

    /**
     * @covers Utils\Wrapper::getValue
     */
    public function testGetValueOnArray()
    {
        $wrapper = Wrapper::wrap(array('test'));
        $this->assertEquals(NULL, $wrapper->getValue('[test]'));
        $wrapper = Wrapper::wrap(array('test' => 2));
        $this->assertEquals(2, $wrapper->getValue('[test]'));
        $wrapper = Wrapper::wrap(array('level1' => array('level2' => array('level3' => array('content')))));
        $this->assertEquals(array('content'), $wrapper->getValue('[level1][level2][level3]'));
    }

    /**
     * @covers Utils\Wrapper::getValue
     */
    public function testGetValueOnObject()
    {
        $object = new stdClass();
        $wrapper = Wrapper::wrap($object);
        $this->assertEquals(NULL, $wrapper->getValue('test'));
        $object->test = 2;
        $this->assertEquals(2, $wrapper->getValue('test'));
        $object->level1 = new stdClass();
        $object->level1->level2 = new stdClass();
        $object->level1->level2->level3 = '3levels';
        $this->assertEquals('3levels', $wrapper->getValue('level1.level2.level3'));
    }

    /**
     * @covers Utils\Wrapper::getArray
     */
    public function testGetArray()
    {
        $wrapper = Wrapper::wrap(array('test' => 1, 'real' => array(2)));
        $this->assertEquals(array(), $wrapper->getArray('[people]'));
        $this->assertEquals(array(1), $wrapper->getArray('[test]'));
        $this->assertEquals(array(2), $wrapper->getArray('[real]'));
    }

    /**
     * @covers Utils\Wrapper::getWrappedValue
     */
    public function testGetWrappedValue()
    {
        $wrapper1 = Wrapper::wrap(array('test' => 1, 'real' => array('wr' => 2)));
        $wrapper2 = $wrapper1->getWrappedValue('[test]');
        $this->assertInstanceOf('Utils\IWrapper', $wrapper2);
        $this->assertEquals(NULL, $wrapper2->getValue('[test]'));
        $wrapper2 = $wrapper1->getWrappedValue('[real]');
        $this->assertInstanceOf('Utils\IWrapper', $wrapper2);
        $this->assertEquals(2, $wrapper2->getValue('[wr]'));
    }

    /**
     * @covers Utils\Wrapper::getWrappedValue
     */
    public function testGetWrappedIterator()
    {
        $arr = array(
            'test' => array(
                array('test' => 1),
                array('test' => 2),
                array('test' => 3),
                array('test' => 4),
                array('test' => 5),
            )
        );
        $wrapper = Wrapper::wrap($arr);
        $wrappedIterator = $wrapper->getWrappedIterator('[test]');
        $this->assertInstanceOf('Utils\Iterators\WrappedIterator', $wrappedIterator);
        $this->assertCount(5, $wrappedIterator);
        $i = 1;
        foreach ($wrappedIterator as $item) {
            $this->assertInstanceOf('Utils\IWrapper', $item);
            $this->assertEquals($i, $item->getValue('[test]'));
            $i++;
        }
    }

    /**
     * @covers Utils\Wrapper::getWrappedValue
     */
    public function testGetWrappedIteratorRequiredAsSimpleArrayInCorrectArray()
    {
        $arr = array(
            'test' => array('test' => 1),
        );
        $wrapper = Wrapper::wrap($arr);
        $wrappedIterator = $wrapper->getWrappedIterator('[test]', !IWrapper::REQUIRED, IWrapper::SIMPLE_ARRAY);
        $this->assertEquals(NULL, $wrappedIterator->current()->getValue('[test]'));
        $wrappedIterator = $wrapper->getWrappedIterator('[test]', !IWrapper::REQUIRED, NULL);
        $this->assertEquals(NULL, $wrappedIterator->current()->getValue('[test]'));

        $object = new stdClass();
        $object->test = 2;
        $arr = array(
            'test' => $object,
        );
        $wrapper = Wrapper::wrap($arr);
        $wrappedIterator = $wrapper->getWrappedIterator('[test]', !IWrapper::REQUIRED, IWrapper::SIMPLE_ARRAY);
        $this->assertEquals(2, $wrappedIterator->current()->getValue('test'));
        $wrappedIterator = $wrapper->getWrappedIterator('[test]', !IWrapper::REQUIRED, NULL);
        $this->assertEquals(NULL, $wrappedIterator->current()->getValue('test'));
    }
}