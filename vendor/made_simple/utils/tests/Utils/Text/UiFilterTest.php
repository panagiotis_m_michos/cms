<?php

namespace Utils\Text\Parser\Filters;

use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;
use Utils\Text\Parser\Filters\UiElements\Element;

class UiFilterTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var UiFilter
     */
    private $object;

    /**
     * @var Element|PHPUnit_Framework_MockObject_MockObject
     */
    private $buttonElementMock;

    /**
     * @var Element|PHPUnit_Framework_MockObject_MockObject
     */
    private $buyButtonElementMock;

    protected function setUp()
    {
        $this->buttonElementMock = $this->getMockBuilder('Utils\Text\Parser\Filters\UiElements\Element')->disableOriginalConstructor()->getMock();
        $this->buyButtonElementMock = $this->getMockBuilder('Utils\Text\Parser\Filters\UiElements\Element')->disableOriginalConstructor()->getMock();

        $this->object = new UiFilter();
    }

    /**
     * @covers Utils\Text\Parser\Filters\UiFilter::match()
     */
    public function testMatchButtonElement()
    {
        $input = file_get_contents(__DIR__ . '../../../Fixtures/ui_filter.html');

        $this->buttonElementMock->expects($this->exactly(2))->method('render');
        $this->buyButtonElementMock->expects($this->exactly(2))->method('render');

        $this->object->addElement('button', $this->buttonElementMock);
        $this->object->addElement('buy_button', $this->buyButtonElementMock);
        $matches = $this->object->match($input);

        $this->assertArrayHasKey('[ui name="button" title="Buy now" colour="red"]', $matches);
        $this->assertArrayHasKey('[ui name="button" title="Buy now" colour="green"]', $matches);
        $this->assertArrayHasKey('[ui name="buy_button" product="165"]', $matches);
        $this->assertArrayHasKey('[ui name="buy_button" product="165" colour="green"]', $matches);
    }

    public function testNoMatch()
    {
        $this->buttonElementMock->expects($this->never())->method('render');
        $this->object->addElement('button', $this->buttonElementMock);
        $matches = $this->object->match('[ui2 name="button" title="Buy now" colour="red"]');
        $this->assertEmpty($matches);
    }

    public function testMissingAttributes()
    {
        $this->buttonElementMock->expects($this->never())->method('render');
        $this->object->addElement('button', $this->buttonElementMock);
        $matches = $this->object->match('[ui]');
        $this->assertEmpty($matches);
    }

    public function testMissingNameAttributes()
    {
        $this->buttonElementMock->expects($this->never())->method('render');
        $this->object->addElement('button', $this->buttonElementMock);
        $matches = $this->object->match('[ui title="Buy now"]');
        $this->assertEmpty($matches);
    }

    public function testMissingElement()
    {
        $matches = $this->object->match('[ui name="button" title="Buy now"]');
        $this->assertEmpty($matches);
    }
}
