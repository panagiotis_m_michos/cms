<?php

namespace Utils\Text\Parser\Filters;

use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;
use Utils\Text\Parser\Filters\Interfaces\IProductRepository;

class CashBackAmountFilterTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var CashBackAmountFilter
     */
    private $object;

    /**
     * @var IProductRepository|PHPUnit_Framework_MockObject_MockObject
     */
    private $productRepositoryMock;

    protected function setUp()
    {
        $this->productRepositoryMock = $this->getMockBuilder('Utils\Text\Parser\Filters\Interfaces\IProductRepository')
            ->disableOriginalConstructor()
            ->getMock();
        $this->object = new CashBackAmountFilter($this->productRepositoryMock);
    }

    public function testMatch()
    {
        $input = '[cashBackAmount 1]';
        $this->productRepositoryMock->expects($this->once())->method('getProductCashBackAmount')->willReturn('10.00');
        $matches = $this->object->match($input);
        $this->assertArrayHasKey('[cashBackAmount 1]', $matches);
    }

    public function testNoPriceMatch()
    {
        $input = '[cashBackAmount 1]';
        $this->productRepositoryMock->expects($this->once())->method('getProductCashBackAmount')->willReturn(NULL);
        $matches = $this->object->match($input);
        $this->assertEmpty($matches);
    }

    public function testMultiLineHtmlMatch()
    {
        $input = file_get_contents(__DIR__ . '../../../Fixtures/cash_back_amount_filter.html');
        $this->productRepositoryMock->expects($this->exactly(2))->method('getProductCashBackAmount')->willReturn(10);

        $matches = $this->object->match($input);

        $this->assertArrayHasKey('[cashBackAmount 1]', $matches);
        $this->assertEquals($matches['[cashBackAmount 1]'], 10);

        $this->assertArrayHasKey('[cashBackAmount 2]', $matches);
        $this->assertEquals($matches['[cashBackAmount 2]'], 10);
    }
}
