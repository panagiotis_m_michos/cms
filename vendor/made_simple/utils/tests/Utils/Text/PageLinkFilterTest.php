<?php

namespace Utils\Text\Parser\Filters;

use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;
use Utils\Text\Parser\Filters\Interfaces\IRouter;

class PageLinkFilterTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var PageLinkFilter
     */
    private $object;

    /**
     * @var IRouter|PHPUnit_Framework_MockObject_MockObject
     */
    private $routerMock;

    protected function setUp()
    {
        $this->routerMock = $this->getMockBuilder('Utils\Text\Parser\Filters\Interfaces\IRouter')
            ->disableOriginalConstructor()
            ->getMock();
        $this->object = new PageLinkFilter($this->routerMock);
    }

    public function testMatch()
    {
        $pageId = 123;
        $input = "[page $pageId]";
        $this->routerMock->expects($this->once())->method('link')->with($pageId)->willReturn('link to url');
        $matches = $this->object->match($input);

        $this->assertArrayHasKey($input, $matches);
        $this->assertEquals($matches[$input], 'link to url');
    }

    public function testNoMatch()
    {
        $input = '[pages 123]';
        $this->routerMock->expects($this->never())->method('link');
        $matches = $this->object->match($input);
        $this->assertEmpty($matches);
    }

    public function testMultiLineHtmlMatch()
    {
        $input = file_get_contents(__DIR__ . '../../../Fixtures/page_link_filter.html');
        $this->routerMock->expects($this->exactly(2))->method('link')->willReturn('link to url');

        $matches = $this->object->match($input);

        $this->assertArrayHasKey('[page 1]', $matches);
        $this->assertEquals($matches['[page 1]'], 'link to url');

        $this->assertArrayHasKey('[page 2]', $matches);
        $this->assertEquals($matches['[page 2]'], 'link to url');
    }

}
