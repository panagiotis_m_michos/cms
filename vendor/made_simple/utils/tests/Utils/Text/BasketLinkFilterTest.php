<?php

namespace Utils\Text\Parser\Filters;

use BasketControler;
use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;
use Utils\Text\Parser\Filters\Interfaces\IRouter;

class BasketLinkFilterTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var BasketLinkFilter
     */
    private $object;

    /**
     * @var IRouter|PHPUnit_Framework_MockObject_MockObject
     */
    private $routerMock;

    protected function setUp()
    {
        $this->routerMock = $this->getMockBuilder('Utils\Text\Parser\Filters\Interfaces\IRouter')
            ->disableOriginalConstructor()
            ->getMock();
        $this->object = new BasketLinkFilter($this->routerMock, BasketControler::PAGE_BASKET, 'add');
    }

    public function testHasMatch()
    {
        $productId = 164;
        $input = "[product $productId]";

        $this->routerMock->expects($this->once())->method('link')->with(BasketControler::PAGE_BASKET, array('add' => $productId))->willReturn('link to url');
        $matches = $this->object->match($input);

        $this->assertArrayHasKey($input, $matches);
        $this->assertEquals($matches[$input], 'link to url');
    }

    public function testNoMatch()
    {
        $input = '[products 164]';
        $this->routerMock->expects($this->never())->method('link');
        $matches = $this->object->match($input);
        $this->assertEmpty($matches);
    }

    public function testMultiLineHtmlMatch()
    {
        $input = file_get_contents(__DIR__ . '../../../Fixtures/basket_link_filter.html');

        $this->routerMock->expects($this->exactly(2))->method('link')->willReturn('link to url');
        $matches = $this->object->match($input);

        $this->assertArrayHasKey('[product 1]', $matches);
        $this->assertEquals($matches['[product 1]'], 'link to url');

        $this->assertArrayHasKey('[product 2]', $matches);
        $this->assertEquals($matches['[product 2]'], 'link to url');
    }
}
