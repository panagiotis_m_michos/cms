<?php

namespace Utils\Text\Parser\Filters;

use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;
use Utils\Text\Parser\Filters\Interfaces\IProductRepository;

class PriceDiffFilterTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var PriceDiffFilter
     */
    private $object;

    /**
     * @var IProductRepository|PHPUnit_Framework_MockObject_MockObject
     */
    private $productRepositoryMock;

    protected function setUp()
    {
        $this->productRepositoryMock = $this->getMockBuilder(IProductRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->object = new PriceDiffFilter($this->productRepositoryMock);
    }

    public function testMatch()
    {
        $input = '[priceDiff 1 2]';
        $this->productRepositoryMock->expects($this->exactly(2))->method('getProductPrice')->withConsecutive([1], [2])->willReturnOnConsecutiveCalls(15, 10);
        $matches = $this->object->match($input);
        $this->assertArrayHasKey('[priceDiff 1 2]', $matches);
        $this->assertEquals($matches['[priceDiff 1 2]'], 5);
    }

    public function testNoPriceMatch()
    {
        $input = '[priceDiff 1 2]';
        $this->productRepositoryMock->expects($this->exactly(2))->method('getProductPrice')->withConsecutive([1], [2])->willReturnOnConsecutiveCalls(15, NULL);
        $matches = $this->object->match($input);
        $this->assertEmpty($matches);
    }

    public function testMultiLineHtmlMatch()
    {
        $input = file_get_contents(__DIR__ . '../../../Fixtures/price_diff_filter.html');
        $this->productRepositoryMock->expects($this->exactly(4))->method('getProductPrice')->withConsecutive([1], [2], [2], [3])->willReturnOnConsecutiveCalls(15, 7, 7, 3);

        $matches = $this->object->match($input);

        $this->assertArrayHasKey('[priceDiff 1 2]', $matches);
        $this->assertEquals($matches['[priceDiff 1 2]'], 8);

        $this->assertArrayHasKey('[priceDiff 2 3]', $matches);
        $this->assertEquals($matches['[priceDiff 2 3]'], 4);
    }
}
