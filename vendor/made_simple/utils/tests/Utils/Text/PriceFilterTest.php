<?php

namespace Utils\Text\Parser\Filters;

use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;
use Utils\Text\Parser\Filters\Interfaces\IProductRepository;

class PriceFilterTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var PriceFilter
     */
    private $object;

    /**
     * @var IProductRepository|PHPUnit_Framework_MockObject_MockObject
     */
    private $productRepositoryMock;

    protected function setUp()
    {
        $this->productRepositoryMock = $this->getMockBuilder('Utils\Text\Parser\Filters\Interfaces\IProductRepository')
            ->disableOriginalConstructor()
            ->getMock();
        $this->object = new PriceFilter($this->productRepositoryMock);
    }

    public function testMatch()
    {
        $input = '[price 1]';
        $this->productRepositoryMock->expects($this->once())->method('getProductPrice')->willReturn('10.00');
        $matches = $this->object->match($input);
        $this->assertArrayHasKey('[price 1]', $matches);
    }

    public function testNoPriceMatch()
    {
        $input = '[price 1]';
        $this->productRepositoryMock->expects($this->once())->method('getProductPrice')->willReturn(NULL);
        $matches = $this->object->match($input);
        $this->assertEmpty($matches);
    }

    public function testMultiLineHtmlMatch()
    {
        $input = file_get_contents(__DIR__ . '../../../Fixtures/price_link_filter.html');
        $this->productRepositoryMock->expects($this->exactly(2))->method('getProductPrice')->willReturn(10);

        $matches = $this->object->match($input);

        $this->assertArrayHasKey('[price 1]', $matches);
        $this->assertEquals($matches['[price 1]'], 10);

        $this->assertArrayHasKey('[price 2]', $matches);
        $this->assertEquals($matches['[price 2]'], 10);
    }
}
