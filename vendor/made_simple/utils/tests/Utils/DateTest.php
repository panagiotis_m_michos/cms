<?php

namespace Utils;

use DateTime;

class DateTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @covers Utils\Date::__construct
     */
    public function testConstructTimestampComparison()
    {
        $date = new Date();
        $dateTime = new DateTime('now 0:0:0');
        $this->assertEquals($date->getTimestamp(), $dateTime->getTimestamp());
        $date = new Date('-1 min');
        $dateTime = new DateTime('now 0:0:0');
        $this->assertEquals($date->getTimestamp(), $dateTime->getTimestamp());
        $date = new Date('-1 day');
        $dateTime = new DateTime('yesterday 0:0:0');
        $this->assertEquals($date->getTimestamp(), $dateTime->getTimestamp());
        $date = new Date('2000-01-01');
        $dateTime = new DateTime('2000-01-01 0:0:0');
        $this->assertEquals($date->getTimestamp(), $dateTime->getTimestamp());
    }

    /**
     * @covers Utils\Date::__construct
     * @expectedException \Utils\Exceptions\InvalidDateException
     */
    public function testConstruct()
    {
        $date = new Date('arsdrs');
    }

    /**
     * @covers Utils\Date::createFromFormat
     */
    public function testCreateFromFormat()
    {
        $date = Date::createFromFormat('d-m-Y', '01-02-2001');
        $this->assertInstanceOf('Utils\Date', $date);
        $this->assertEquals(new Date('2001-02-01'), $date);
        $date = Date::createFromFormat('d-m-Y', '013-052-2001');
        $this->assertFalse($date);
    }

    public function testFormat()
    {
        $date = new Date();
        $dateTime = new DateTime();
        $this->assertEquals($dateTime->format('Y-m-d'), $date->format('Y-m-d'));
        $date = new Date('-1 year');
        $dateTime = new DateTime('-1 year');
        $this->assertEquals($dateTime->format('d-m-Y'), $date->format('d-m-Y'));
        $this->assertNotEquals($dateTime->format('d-m-Y H:i:s'), $date->format('d-m-Y H:i:s'));
        $this->assertEquals('00:00:00', $date->format('H:i:s'));
    }

    public function testGreaterThan()
    {
        $date1 = new Date('now');
        $date2 = new Date('-1 day');
        $this->assertTrue($date1 > $date2);
        $date1 = new Date('+2 year');
        $date2 = new Date('+1 year');
        $this->assertTrue($date1 > $date2);
    }

    public function testEquals()
    {
        $date1 = new Date('now');
        $date2 = new Date('now');
        $this->assertTrue($date1 == $date2);
        $date1 = new Date('-1 min');
        $date2 = new Date('+1 min');
        $this->assertTrue($date1 == $date2);
        $date1 = new Date('-1 day');
        $date2 = new Date('-1 day');
        $this->assertTrue($date1 == $date2);
    }

    public function testNotEquals()
    {
        $date1 = new Date('now');
        $date2 = new Date('-1 day');
        $this->assertTrue($date1 != $date2);
        $date1 = new Date('-1 year');
        $date2 = new Date('+1 year');
        $this->assertTrue($date1 != $date2);
    }

    public function testLessThan()
    {
        $date1 = new Date('-2 day');
        $date2 = new Date();
        $this->assertTrue($date1 < $date2);
        $date1 = new Date('-1 year');
        $date2 = new Date('+1 year');
        $this->assertTrue($date1 <> $date2);
    }

    /**
     * @covers Utils\Date::changeFormat
     */
    public function testChangeFormatValidFormat()
    {
        $this->assertEquals('2001-01-01', Date::changeFormat('01-01-2001', 'd-m-Y', 'Y-m-d'));
        $this->assertEquals('31-12-1901', Date::changeFormat('1901-12-31', 'Y-m-d', 'd-m-Y'));
    }

    /**
     * @covers Utils\Date::changeFormat
     */
    public function testChangeFormatInvalidReturnFormat()
    {
        $this->assertFalse(Date::changeFormat('000-01-2001', 'd-m-Y', 'Y-m-d'));
        $this->assertFalse(Date::changeFormat('00-0rst', 'd-m-Y', 'Y-m-d'));
        //unrecognized characters are printed as provided
        $this->assertEquals('3434', Date::changeFormat('01-01-2000', 'd-m-Y', '3434'));
    }

    /**
     * @covers Utils\Date::create
     */
    public function testCreate()
    {
        $today = new Date();
        $this->assertEquals(new Date('2009-01-' . $today->format('d')), Date::create('my', '0109'));
        //unfortunately this does not work
        //$this->assertEquals(new Date('2009-01-' . $today->format('d')), Date::create('nY', '12009'));
        $this->assertEquals(new Date('2001-01-01'), Date::create('Y-m-d', '2001-01-01'));
        $this->assertEquals(new Date('1999-12-27'), Date::create('dmY', '27121999'));
    }

    /**
     * @covers Utils\Date::create
     * @expectedException \Utils\Exceptions\InvalidDateException
     */
    public function testCreateInvalidFormat1()
    {
        Date::create('d-m-Y', '000-01-2001');
    }

    /**
     * @covers Utils\Date::create
     * @expectedException \Utils\Exceptions\InvalidDateException
     */
    public function testCreateInvalidFormat2()
    {
        Date::create('dmY', 'arst');
    }

    /**
     * @covers Utils\Date::isBetween
     */
    public function testIsBetweenTrue()
    {
        $date = new Date();
        $this->assertTrue($date->isBetween('today', 'today'));
        $this->assertTrue($date->isBetween('-1 day', 'today'));
        $this->assertTrue($date->isBetween('-1 year', 'today'));
        $this->assertTrue($date->isBetween('-1 year', '+1 day'));
        $this->assertTrue($date->isBetween('-1 day', '+5 days'));
        $this->assertTrue($date->isBetween('today', '+10 days'));
    }

    /**
     * @covers Utils\Date::isBetween
     */
    public function testIsBetweenFalse()
    {
        $date = new Date();
        $this->assertFalse($date->isBetween('-2 days', '-1 day'));
        $this->assertFalse($date->isBetween('-1 day', '-1 day'));
        $this->assertFalse($date->isBetween('-1 year', '-1 month'));
        $this->assertFalse($date->isBetween('+2 days', '-1 month'));
        $this->assertFalse($date->isBetween('+10 days', '-10 days'));
        $this->assertFalse($date->isBetween('today', '-10 days'));
    }

    /**
     * @covers Utils\Date::createFromDateTime()
     */
    public function testCreatedFromDateTime()
    {
        $dateTime = new DateTime();
        $date = Date::createFromDateTime($dateTime);

        $dateTime1 = clone $dateTime;
        $this->assertEquals($dateTime1->setTime(0, 0, 0)->format('Y-m-d G:i:s'), $date->format('Y-m-d G:i:s'));
    }
}
