<?php

namespace Utils;

use InvalidArgumentException;
use org\bovigo\vfs\vfsStream;
use PHPUnit_Framework_TestCase;

class FileTest extends PHPUnit_Framework_TestCase
{

    /**
     * @var string
     */
    private $firstFile;

    /**
     * @var string
     */
    private $secondFile;

    /**
     * @var Directory
     */
    private $dir;

    public function setUp()
    {
        $this->root = vfsStream::setup('fileTest', 0777);
        $this->dir = Directory::fromPath(vfsStream::url('fileTest'));
        $this->firstFile = 'abc';
        $this->secondFile = 'ab/ab';
    }

    /**
     * @covers Utils\File::fromExisting
     */
    public function testFromExistingFileExists()
    {
        $file = $this->dir->getPath() . '/' . $this->firstFile;
        touch($file);
        $this->assertInstanceOf('Utils\File', File::fromExisting($this->dir, $this->firstFile));
    }

    /**
     * @covers Utils\File::fromExisting
     * @expectedException InvalidArgumentException
     */
    public function testFromExistingFileDoesNotExists()
    {
        $this->assertInstanceOf('Utils\File', File::fromExisting($this->dir, $this->firstFile));
    }

    /**
     * @covers Utils\File::fromDir
     */
    public function testFromDirFileDoesNotExists()
    {
        $this->assertInstanceOf('Utils\File', File::fromDir($this->dir, $this->firstFile));
    }

    /**
     * @covers Utils\File::fromDir
     * @expectedException InvalidArgumentException
     */
    public function testFromDirInvalidFilename()
    {
        $this->assertInstanceOf('Utils\File', File::fromDir($this->dir, '../../a'));
    }
}