### Logable module ###

Contains log related classes

### Text filters
Filters are used to replace placeholders in admin text

### Implementation
~~~
$parser = new Utils\Text\Parser\Parser();

// basket link filter e.g. [product 123]
$basketLinkFilter = new Utils\Text\Parser\Filters\BasketLinkFilter('...');
$parser->addFilter($basketLinkFilter);

// page link filter e.g. [page 123]
$pageLinkFilter = new Utils\Text\Parser\Filters\PageLinkFilter('...');
$parser->addFilter($pageLinkFilter);

// price filter e.g. [price 123]
$priceFilter = new Utils\Text\Parser\Filters\PriceFilter('...');
$parser->addFilter($priceFilter);

// ui components filter e.g [ui name="buy_button" productId=123]
$uiFilter = new Utils\Text\Parser\Filters\UiFilter();
$uiFilter->addElement(new ButtonElement(...));
$uiFilter->addElement(new BuyButtonElement(...));
$parser->addFilter($uiFilter);

$input = '...';
$output = $parser->parse($input);
~~~

### Input
~~~
// BasketLinkFilter
<a href="[product 123]">Add product 123 to basket</a>

// PageLinkFilter
<a href="[page 123]">Link to page 123</a>

// PriceFilter
You can buy product 123 for £[price 123]

// UiFilter
[ui name="button" text="Buy now" loading="expand-right" colour="green"]
[ui name="buy_button" productId="165"]
~~~

### Output
~~~
// BasketLinkFilter
<a href="/basket/?add=123">Add product 123 to basket</a>

// PageLinkFilter
<a href="/page123en.html">Link to page 123</a>

// PriceFilter
You can buy product 123 for £29.99

// UiFilter
<button class="ladda-button" data-color="green" data-style="expand-right">
    <span class="ladda-label">Buy now</span>
</button>
<form action="/page130en.html" method="post">
    <button class="ladda-button" data-style="expand-right" data-color="orange" data-size="s">
        <span class="ladda-label">
            Buy Now
        </span>
    </button>
    <input type="hidden" name="productId" value="165">
</form>
~~~
