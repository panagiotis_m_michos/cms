# How to use

    <?php
    $profile = new ApiSignature($username, $password, $signature);
    $environment = Environment::LIVE(); // Environment::SANDBOX()

    $paypal = new PayPalNVP($profile, $environment);

    $items = array(PaymentItem::getRequest($amount));
    $payment = Payment::getRequest($items);
    $payment->setCustomField('LONDONPRESENCE');
    $payment->setCurrency(new Currency('GBP'));

    $setEC = SetExpressCheckout::newInstance($payment, $successUrl, $cancelUrl);

    $paypal->setResponse($setEC);
    $response = $setEC->getResponse();

    redirect("%{$response->getRedirectUrl()}%");