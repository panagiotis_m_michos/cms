<?php

namespace PayPalNVP\Exception;

class ResponseException extends \Exception
{
    /**
     * @param string $response
     * @return \PayPalNVP\Exception\self
     */
    public static function general($response)
    {
        $message = 'Incorrect paypal response received. Please try again later';
        \Debug::log($message . ': ' . $response);
        return new self($message);
    }
}