<?php

namespace PayPalNVP\Exception;

use Exception;

class DoExpressCheckoutException extends Exception
{
    const ERROR_FUNDING_FAILURE = 10486;

    /**
     * @var string
     */
    private $redirectUrl;

    /**
     * @return string
     */
    public function getRedirectUrl()
    {
        return $this->redirectUrl;
    }

    /**
     * @param string $redirectUrl
     */
    public function setRedirectUrl($redirectUrl)
    {
        $this->redirectUrl = $redirectUrl;
    }

    /**
     * @param string $redirectUrl
     * @return \PayPalNVP\Exception\self
     */
    public static function fundingFailure($redirectUrl)
    {
        $e = new self("This transaction couldn't be completed.", self::ERROR_FUNDING_FAILURE);
        $e->setRedirectUrl($redirectUrl);

        return $e;
    }
}
