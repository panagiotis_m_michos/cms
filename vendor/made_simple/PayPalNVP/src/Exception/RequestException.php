<?php

namespace PayPalNVP\Exception;

use Exception;

class RequestException extends Exception
{
    /**
     * @param string $code
     * @return $this
     */
    public static function expressCheckout($code)
    {
        return new self(
            sprintf(
                "[%s] Sorry, PayPal is unable to process your payment. Please try again using an alternative payment method or different credit/debit card.",
                $code
            ),
            $code
        );
    }

    /**
     * @param string $error
     * @return $this
     */
    public static function recurringPaymentsProfile($error)
    {
        return new self("Paypal recurring error: $error");
    }
}
