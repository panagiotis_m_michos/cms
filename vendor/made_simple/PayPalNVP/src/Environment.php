<?php

namespace PayPalNVP;

/**
 * paypal environment - live, sandbox or beta sandbox
 */
final class Environment
{

    /**
     * Holds instances for lazy initialization.
     *
     * @var array<Environemnt>
     */
    private static $instances = [];

    /**
     *
     * @var string enum id - name of the static method that returned instance
     */
    private $name;

    /** string representaion of the environment part url */
    private $environment;

    private function __construct($name, $environment)
    {
        $this->name = $name;
        $this->environment = $environment;
    }

    /**
     * live environment
     *
     * @return self
     */
    public static function LIVE()
    {

        $name = 'LIVE';
        $environment = '';
        if (!isset(self::$instances[$name])) {
            self::$instances[$name] = new self($name, $environment);
        }
        return self::$instances[$name];
    }

    /**
     * test environment
     *
     * @return self
     */
    public static function SANDBOX()
    {

        $name = 'SANDBOX';
        $environment = 'sandbox.';
        if (!isset(self::$instances[$name])) {
            self::$instances[$name] = new self($name, $environment);
        }
        return self::$instances[$name];
    }

    /**
     * beta test environment
     *
     * @return self
     */
    public static function BETA_SANDBOX()
    {

        $name = 'BETA_SANDBOX';
        $environment = 'beta-sandbox.';
        if (!isset(self::$instances[$name])) {
            self::$instances[$name] = new self($name, $environment);
        }
        return self::$instances[$name];
    }

    /**
     * This method is used only by PayPalNVP class
     *
     * @return String   part of the url that inidicates which environment shoud
     *                  be used
     */
    public function getEnvironmentPartUrl()
    {
        return $this->environment;
    }

    public function __toString()
    {
        return $this->name;
    }

    private function __clone()
    {
    }
}