<?php

namespace PayPalNVP;

require_once 'Profile/Profile.php';
require_once 'Request/Request.php';
require_once 'Transport/HttpPost.php';

use PayPalNVP\Profile\Profile;
use PayPalNVP\Request\Request;
use PayPalNVP\Transport\HttpPost;

final class PayPalNVP
{

    private static $transport = NULL;

    private static $version = '89.0';

    /** @var Profile */
    private $profile;

    /** @var Environment */
    private $environment;

    /**
     *
     * @param Profile $profile user
     * @param Environment $environment LIVE/TEST ...
     */
    public function __construct(Profile $profile, Environment $environment)
    {

        $this->profile = $profile;
        $this->environment = $environment;
        if (self::$transport == NULL) {
            self::$transport = new HttpPost();
        }
    }

    /**
     * Sends request to paypal and sets response
     *
     * @param Request $request
     */
    public function setResponse(Request $request)
    {

        /* request data */
        $nvpString = '';
        foreach ($this->profile->getNVPProfile() as $key => $value) {
            $nvpString .= $key . '=' . urlencode($value) . '&';
        }
        foreach ($request->getNVPRequest() as $key => $value) {
            $nvpString .= $key . '=' . urlencode($value) . '&';
        }
        $nvpString .= 'VERSION=' . urlencode(self::$version);

        /* request url */
        $endpointUrl = 'https://';
        $endpointUrl .= ($this->profile->isAPISignature()) ? 'api-3t.' : 'api.';
        $endpointUrl .= $this->environment->getEnvironmentPartUrl();
        $endpointUrl .= 'paypal.com/nvp';

        $response = self::$transport->getResponse($endpointUrl, $nvpString);
        $request->setNVPResponse($response, $this->environment);
    }

    private function __clone()
    {
    }
}