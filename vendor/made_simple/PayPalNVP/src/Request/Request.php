<?php

namespace PayPalNVP\Request;

require_once __DIR__ . '/../Environment.php';

use PayPalNVP\Environment;

interface Request
{

    /**
     * Creates and returns part of the nvp (name value pair) request containing
     * request values
     *
     * @return array<String, String>
     */
    public function getNVPRequest();

    /**
     * Setter (used by PayPalNVP class) to set response from paypal
     *
     * @param String $nvpResponse response from paypal
     * @param Environment $environment environment used to send the request
     * @return array<String, String>
     */
    public function setNVPResponse($nvpResponse, Environment $environment);

    /**
     * Returns response from paypal as Object. Call this method after sending
     * request to payapl ($payPalNVP->getResponse($request)). If response is
     * not set/received, returns null.
     *
     * @return Response
     */
    public function getResponse();
}