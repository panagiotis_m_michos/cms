<?php

namespace PayPalNVP\Request;

require_once 'Request.php';
require_once __DIR__ . '/../Response/CreateRecurringPaymentsProfileResponse.php';
require_once __DIR__ . '/../Environment.php';
require_once __DIR__ . '/../Fields/RecurringPaymentsProfile.php';
require_once __DIR__ . '/../Fields/Schedule.php';
require_once __DIR__ . '/../Fields/BillingPeriod.php';
require_once __DIR__ . '/../Fields/Activation.php';
require_once __DIR__ . '/../Fields/ShippingAddress.php';
require_once __DIR__ . '/../Fields/CreditCard.php';
require_once __DIR__ . '/../Fields/PayerInformation.php';
require_once __DIR__ . '/../Fields/PayerName.php';
require_once __DIR__ . '/../Fields/Address.php';
require_once __DIR__ . '/../Fields/Collection.php';

use PayPalNVP\Environment;
use PayPalNVP\Fields\Activation;
use PayPalNVP\Fields\Address;
use PayPalNVP\Fields\BillingPeriod;
use PayPalNVP\Fields\Collection;
use PayPalNVP\Fields\CreditCard;
use PayPalNVP\Fields\PayerInformation;
use PayPalNVP\Fields\PayerName;
use PayPalNVP\Fields\RecurringPaymentsProfile;
use PayPalNVP\Fields\Schedule;
use PayPalNVP\Fields\ShippingAddress;
use PayPalNVP\Response\CreateRecurringPaymentsProfileResponse;

final class CreateRecurringPaymentsProfile implements Request
{

    /** Method value of this request */
    private static $methodName = 'CreateRecurringPaymentsProfile';

    /**
     * @var Collection
     */
    private $collection;

    /** @var CreateRecurringPaymentsProfileResponse */
    private $response;

    /** @var RecurringPaymentsProfile */
    private $profile;

    /** @var Schedule */
    private $schedule;

    /** @var BillingPeriod */
    private $billingPeriod;

    /** @var Activation */
    private $activation;

    /** @var ShippingAddress */
    private $shippingAddress;

    /** @var CreditCard */
    private $creditCard;

    /** @var PayerInformation */
    private $payerInformation;

    /** @var PayerName */
    private $payerName;

    /** @var Address */
    private $address;

    private static $allowedValues = ['TOKEN'];

    /**
     * @param RecurringPaymentsProfile $profile
     * @param Schedule $schedule
     * @param BillingPeriod $billingPeriod
     */
    public function __construct(RecurringPaymentsProfile $profile,
                                Schedule $schedule, BillingPeriod $billingPeriod)
    {

        $this->collection = new Collection(self::$allowedValues, NULL);
        $this->collection->setValue('METHOD', self::$methodName);
        $this->nvpResponse = NULL;

        $this->profile = $profile;
        $this->schedule = $schedule;
        $this->billingPeriod = $billingPeriod;
    }

    /**
     * A timestamped token, the value of which was returned in the response to
     * the first call to SetExpressCheckout. You can also use the token
     * returned in the SetCustomerBillingAgreement response.
     * Either this token or a credit card number is required. If you include
     * both token and credit card number, the token is used and credit card
     * number is ignored.
     * Call CreateRecurringPaymentsProfile once for each billing agreement
     * included in SetExpressCheckout request and use the same token for each
     * call. Each CreateRecurringPaymentsProfile request creates a single
     * recurring payments profile.
     * Note: Tokens expire after approximately 3 hours.
     *
     * @param string $token
     */
    public function setToken($token)
    {
        $this->collection->setValue('TOKEN', $token);
    }

    /**
     * @param Activation $activation
     */
    public function setActivation(Activation $activation)
    {
        $this->activation = $activation;
    }

    /**
     * @param ShippingAddress $address
     */
    public function setShippingAddress(ShippingAddress $address)
    {
        $this->shippingAddress = $address;
    }

    /**
     * @param CreditCard $card
     */
    public function setCreditCard(CreditCard $card)
    {
        $this->creditCard = $card;
    }

    /**
     * @param PayerInformation $info
     */
    public function setPayerInformation(PayerInformation $info)
    {
        $this->payerInformation = $info;
    }

    /**
     * @param PayerName $payerName
     */
    public function setPayerName(PayerName $payerName)
    {
        $this->payerName = $payerName;
    }

    /**
     * @param Address $address
     */
    public function setAddress(Address $address)
    {
        $this->address = $address;
    }

    public function getNVPRequest()
    {

        $request = $this->collection->getAllValues();
        $request = array_merge($request, $this->schedule->getNVPArray(),
            $this->billingPeriod->getNVPArray(), $this->profile->getNVPArray());

        if ($this->activation != NULL) {
            $request = array_merge($request, $this->activation->getNVPArray());
        }

        if ($this->shippingAddress != NULL) {
            $request = array_merge($request, $this->shippingAddress->getNVPArray());
        }

        if ($this->creditCard != NULL) {
            $request = array_merge($request, $this->creditCard->getNVPArray());
        }

        if ($this->payerInformation != NULL) {
            $request = array_merge($request, $this->payerInformation->getNVPArray());
        }

        if ($this->payerName != NULL) {
            $request = array_merge($request, $this->payerName->getNVPArray());
        }

        if ($this->address != NULL) {
            $request = array_merge($request, $this->address->getNVPArray());
        }

        return $request;
    }

    public function setNVPResponse($nvpResponse, Environment $environment)
    {

        $this->response = new CreateRecurringPaymentsProfileResponse(
            $nvpResponse, $environment);
    }

    public function getResponse()
    {
        return $this->response;
    }
}
