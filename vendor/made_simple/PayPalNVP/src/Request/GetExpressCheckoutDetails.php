<?php

namespace PayPalNVP\Request;

use PayPalNVP\Environment;
use PayPalNVP\Fields\Collection;
use PayPalNVP\Response\GetExpressCheckoutDetailsResponse;

final class GetExpressCheckoutDetails implements Request
{

    /** Method value of this request */
    private static $methodName = 'GetExpressCheckoutDetails';

    /**
     * @var Collection
     */
    private $collection;

    /** @var GetExpressCheckoutDetailsResponse */
    private $response;

    private static $allowedValues = ['METHOD', 'TOKEN', 'PAYERID'];

    /**
     * @param String $queryString from paypal response
     */
    public function __construct($queryString)
    {

        $response = [];

        /* make sure that ? is not passed */
        $url = explode("?", $queryString);
        $index = count($url) - 1;
        $queryString = $url[$index];

        $parts = explode("&", $queryString);
        foreach ($parts as $part) {
            $values = explode("=", $part);
            $response[$values[0]] = $values[1];
        }

        $this->collection = new Collection(self::$allowedValues, $response);
        $this->collection->setValue('METHOD', self::$methodName);
    }

    public function getNVPRequest()
    {
        return $this->collection->getAllValues();
    }

    public function setNVPResponse($nvpResponse, Environment $environment)
    {
        $this->response = new GetExpressCheckoutDetailsResponse($nvpResponse, $environment);
    }

    public function getResponse()
    {
        return $this->response;
    }
}
