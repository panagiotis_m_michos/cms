<?php

namespace PayPalNVP\Request;

require_once 'Request.php';
require_once __DIR__ . '/../Response/GetRecurringPaymentsProfileDetailsResponse.php';
require_once __DIR__ . '/../Environment.php';
require_once __DIR__ . '/../Fields/Collection.php';

use PayPalNVP\Environment;
use PayPalNVP\Fields\Collection;
use PayPalNVP\Response\GetRecurringPaymentsProfileDetailsResponse;

final class GetRecurringPaymentsProfileDetails implements Request
{


    /** Method value of this request */
    private static $methodName = 'GetRecurringPaymentsProfileDetails';

    /** @var Collection */
    private $collection;

    /** @var GetRecurringPaymentsProfileDetailsResponse */
    private $response;


    private static $allowedValues = [''];

    /**
     * @param String $profileId Recurring payments profile ID returned in the
     *      CreateRecurringPaymentsProfile response. Character length and
     *      limitations: 14 single-byte alphanumeric characters. 19 character
     *      profile IDs are supported for compatibility with previous versions
     *      of the PayPal API.
     */
    public function __construct($profileId)
    {

        $this->collection = new Collection(self::$allowedValues, NULL);
        $this->collection->setValue('METHOD', self::$methodName);
        $this->collection->setValue('PROFILEID', $profileId);
        $this->nvpResponse = NULL;
    }


    public function getNVPRequest()
    {
        return $this->collection->getAllValues();
    }

    public function setNVPResponse($nvpResponse, Environment $environment)
    {
        $this->response = new GetRecurringPaymentsProfileDetailsResponse($nvpResponse, $environment);
    }

    public function getResponse()
    {
        return $this->response;
    }
}


