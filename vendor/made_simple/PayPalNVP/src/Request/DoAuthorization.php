<?php

namespace PayPalNVP\Request;

require_once 'Request.php';
require_once __DIR__ . '/../Response/GetExpressCheckoutDetailsResponse.php';
require_once __DIR__ . '/../Response/DoExpressCheckoutPaymentResponse.php';
require_once __DIR__ . '/../Environment.php';
require_once __DIR__ . '/../Fields/Collection.php';

use PayPalNVP\Environment;
use PayPalNVP\Fields\Collection;
use PayPalNVP\Response\DoExpressCheckoutPaymentResponse;
use PayPalNVP\Response\GetExpressCheckoutDetailsResponse;

final class DoAuthorization implements Request
{

    /** Method value of this request */
    private static $methodName = 'DoAuthorization';

    /**
     * @var Collection
     */
    private $collection;

    /**
     * @var UserOptions
     */
    private $userOptions;

    /** @var array<Payment> */
    private $payments = [];

    /** @var DoExpressCheckoutPaymentResponse */
    private $response;

    private static $allowedValues = ['TRANSACTIONID', 'AMT',
        'TRANSACTIONENTITY', 'CURRENCYCODE', 'MSGSUBID'];

    /**
     * If GetExpressCheckoutDetails is passed, then values are populated from
     * the response, otherwise, they need to be set using setters.
     *
     * @param String $queryString from paypal response
     */
    public function __construct(GetExpressCheckoutDetailsResponse $details = NULL)
    {

        $detailsArray = NULL;
        if ($details != NULL) {
            $detailsArray = $details->getResponse();
        }

        $this->collection = new Collection(self::$allowedValues, $detailsArray);
        $this->collection->setValue('METHOD', self::$methodName);

        if ($details != NULL) {
            $this->userOptions = $details->getUserOptions();
            $this->payments = $details->getPayements();
        }
    }

    /**
     * Value of the order's transaction identification number returned by PayPal.
     * Character length and limitations: 19 single-byte characters
     *
     * @return string
     */
    public function getTransactionId()
    {
        return $this->collection->getValue('TRANSACTIONID');
    }

    /**
     * Value of the order's transaction identification number returned by PayPal.
     * Character length and limitations: 19 single-byte characters
     *
     * @param string $id
     */
    public function setTransactionId($id)
    {
        $this->collection->setValue('TRANSACTIONID', $id);
    }

    /**
     * Amount to authorize.
     * Character length and limitations: Value is a positive number which cannot
     * exceed $10,000 USD in any currency. It includes no currency symbol.
     * It must have 2 decimal places, the decimal separator must be a period (.),
     * and the optional thousands separator must be a comma (,).
     *
     * @param string $amount
     */
    public function getAmount()
    {
        $this->collection->getValue('AMT');
    }

    /**
     * Amount to authorize.
     * Character length and limitations: Value is a positive number which cannot
     * exceed $10,000 USD in any currency. It includes no currency symbol.
     * It must have 2 decimal places, the decimal separator must be a period (.),
     * and the optional thousands separator must be a comma (,).
     *
     * @param string $amount
     */
    public function setAmount($amount)
    {
        $this->collection->setValue('AMT', $amount);
    }

    /**
     * Type of transaction to authorize. The only allowable value is Order,
     * which means that the transaction represents a buyer order that can be
     * fulfilled over 29 days.
     *
     * @return string
     */
    public function getTransactionEntity()
    {
        return $this->collection->getValue('TRANSACTIONENTITY');
    }

    /**
     * Type of transaction to authorize. The only allowable value is Order,
     * which means that the transaction represents a buyer order that can be
     * fulfilled over 29 days.
     *
     * @param string $entity
     */
    public function setTransactionEntity($entity)
    {
        $this->collection->setValue('TRANSACTIONENTITY', $entity);
    }

    /**
     * A 3-character currency code.
     *
     * @return string
     */
    public function getCurrency()
    {
        $this->collection->setValue('CURRENCYCODE');
    }

    /**
     * A 3-character currency code.
     *
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->collection->setValue('CURRENCYCODE', $currency);
    }


    public function getNVPRequest()
    {

        $request = $this->collection->getAllValues();

        /* payment */
        foreach ($this->payments as $index => $payment) {
            foreach ($payment->getNVPArray() as $key => $value) {
                if (is_array($value)) {    // payment item is array and has to start with L_
                    foreach ($value as $itemIndex => $item) {
                        foreach ($item as $k => $v) {
                            $request['L_PAYMENTREQUEST_' . $index . '_' . $k . $itemIndex] = $v;
                        }
                    }
                } else {
                    $request['PAYMENTREQUEST_' . $index . '_' . $key] = $value;
                }
            }
        }

        /* user selected options */
        if ($this->userOptions != NULL) {
            $request = array_merge($request, $this->userOptions->getNVPArray());
        }

        return $request;
    }

    public function setNVPResponse($nvpResponse, Environment $environment)
    {
        $this->response = new DoExpressCheckoutPaymentResponse($nvpResponse, $environment);
    }

    public function getResponse()
    {
        return $this->response;
    }
}
