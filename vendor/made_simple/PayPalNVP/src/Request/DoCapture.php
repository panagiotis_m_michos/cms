<?php

namespace PayPalNVP\Request;

require_once 'Request.php';
require_once __DIR__ . '/../Response/GetExpressCheckoutDetailsResponse.php';
require_once __DIR__ . '/../Response/DoExpressCheckoutPaymentResponse.php';
require_once __DIR__ . '/../Environment.php';
require_once __DIR__ . '/../Fields/Collection.php';

use PayPalNVP\Environment;
use PayPalNVP\Fields\Collection;
use PayPalNVP\Response\DoCaptureResponse;

final class DoCapture implements Request
{

    /** Method value of this request */
    private static $methodName = 'DoCapture';

    /**
     * @var Collection
     */
    private $collection;

    /**
     * @var UserOptions
     */
    private $userOptions;

    /** @var array<Payment> */
    private $payments = [];

    /** @var DoCaptureResponse */
    private $response;

    private static $allowedValues = ['AUTHORIZATIONID', 'AMT',
        'CURRENCYCODE', 'COMPLETETYPE', 'INVNUM', 'NOTE', 'SOFTDESCRIPTOR',
        'MSGSUBID'];

    /**
     * If GetExpressCheckoutDetails is passed, then values are populated from
     * the response, otherwise, they need to be set using setters.
     *
     * @param String $queryString from paypal response
     */
    public function __construct(GetExpressCheckoutDetailsResponse $details = NULL)
    {

        $detailsArray = NULL;
        if ($details != NULL) {
            $detailsArray = $details->getResponse();
        }

        $this->collection = new Collection(self::$allowedValues, $detailsArray);
        $this->collection->setValue('METHOD', self::$methodName);

        if ($details != NULL) {
            $this->userOptions = $details->getUserOptions();
            $this->payments = $details->getPayements();
        }
    }

    /**
     * Authorization identification number of the payment you want to capture.
     * This is the transaction ID returned from DoExpressCheckoutPayment,
     * DoDirectPayment, or CheckOut. For point-of-sale transactions, this is the
     * transaction ID returned by the CheckOut call when the payment action
     * is Authorization. Character length and limitations:
     * 19 single-byte characters maximum
     *
     * @return string
     */
    public function getAuthorizationId()
    {
        return $this->collection->getValue('AUTHORIZATIONID');
    }

    /**
     * Authorization identification number of the payment you want to capture.
     * This is the transaction ID returned from DoExpressCheckoutPayment,
     * DoDirectPayment, or CheckOut. For point-of-sale transactions, this is the
     * transaction ID returned by the CheckOut call when the payment action
     * is Authorization. Character length and limitations:
     * 19 single-byte characters maximum
     *
     * @param string $id
     */
    public function setAuthorizationId($id)
    {
        $this->collection->setValue('AUTHORIZATIONID', $id);
    }

    /**
     * Amount to capture.
     * Character length and limitations: Value is a positive number which cannot
     * exceed $10,000 USD in any currency. It includes no currency symbol.
     * It must have 2 decimal places, the decimal separator must be a period (.),
     * and the optional thousands separator must be a comma (,).
     *
     * @param string $amount
     */
    public function getAmount()
    {
        $this->collection->getValue('AMT');
    }

    /**
     * Amount to capture.
     * Character length and limitations: Value is a positive number which cannot
     * exceed $10,000 USD in any currency. It includes no currency symbol.
     * It must have 2 decimal places, the decimal separator must be a period (.),
     * and the optional thousands separator must be a comma (,).
     *
     * @param string $amount
     */
    public function setAmount($amount)
    {
        $this->collection->setValue('AMT', $amount);
    }

    /**
     * A 3-character currency code.
     *
     * @return string
     */
    public function getCurrency()
    {
        $this->collection->setValue('CURRENCYCODE');
    }

    /**
     * A 3-character currency code.
     *
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->collection->setValue('CURRENCYCODE', $currency);
    }

    /**
     * Indicates whether or not this is your last capture. It is one of the
     * following values:
     * Complete – This is the last capture you intend to make.
     * NotComplete – You intend to make additional captures.
     *
     * Note:
     * If Complete, any remaining amount of the original authorized transaction
     * is automatically voided and all remaining open authorizations are voided.
     *
     * @return string
     */
    public function getCompleteType()
    {
        return $this->collection->getValue('COMPLETETYPE');
    }

    /**
     * Indicates whether or not this is your last capture. It is one of the
     * following values:
     * Complete – This is the last capture you intend to make.
     * NotComplete – You intend to make additional captures.
     *
     * Note:
     * If Complete, any remaining amount of the original authorized transaction
     * is automatically voided and all remaining open authorizations are voided.
     *
     * @param string $type
     */
    public function setCompleteType($type)
    {
        $this->collection->setValue('COMPLETETYPE', $type);
    }

    public function getNVPRequest()
    {

        $request = $this->collection->getAllValues();

        /* payment */
        foreach ($this->payments as $index => $payment) {
            foreach ($payment->getNVPArray() as $key => $value) {
                if (is_array($value)) {    // payment item is array and has to start with L_
                    foreach ($value as $itemIndex => $item) {
                        foreach ($item as $k => $v) {
                            $request['L_PAYMENTREQUEST_' . $index . '_' . $k . $itemIndex] = $v;
                        }
                    }
                } else {
                    $request['PAYMENTREQUEST_' . $index . '_' . $key] = $value;
                }
            }
        }

        /* user selected options */
        if ($this->userOptions != NULL) {
            $request = array_merge($request, $this->userOptions->getNVPArray());
        }

        return $request;
    }

    public function setNVPResponse($nvpResponse, Environment $environment)
    {
        $this->response = new DoCaptureResponse($nvpResponse, $environment);
    }

    public function getResponse()
    {
        return $this->response;
    }
}
