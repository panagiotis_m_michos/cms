<?php

namespace PayPalNVP\Fields;

interface Field
{

    /**
     * @return array
     */
    public function getNVPArray();
}