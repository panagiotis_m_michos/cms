<?php

namespace PayPalNVP\Fields;

/**
 * Payer Information Fields for Digital Goods. Payer information is under Payer
 */
final class PayerInformation implements Field
{

    /**
     * @var Collection
     */
    private $collection;

    private static $allowedValues = [
        'EMAIL', 'PAYERID', 'PAYERSTATUS', 'COUNTRYCODE', 'BUSINESS',
    ];

    private function __construct()
    {
    }

    public static function getResponse(array $response)
    {

        $info = new self();
        $info->collection = new Collection(self::$allowedValues, $response);
        return $info;
    }

    /**
     * Email address of payer.
     * Character length and limitations: 127 single-byte characters.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->collection->getValue('EMAIL');
    }

    /**
     * Unique PayPal customer account identification number.
     * Character length and limitations:13 single-byte alphanumeric characters.
     *
     * @return string
     */
    public function getPayerId()
    {
        return $this->collection->getValue('PAYERID');
    }

    /**
     * Status of payer. Valid values are: verified, or unverified
     *
     * @return string
     */
    public function getPayerStatus()
    {
        return $this->collection->getValue('PAYERSTATUS');
    }

    /**
     * Payer's country of residence in the form of ISO standard 3166
     * two-character country codes.
     *
     * @return string
     */
    public function getCountryCode()
    {
        return $this->collection->getValue('COUNTRYCODE');
    }

    /**
     * Payer's business name.
     *
     * @return string
     */
    public function getBussinessName()
    {
        return $this->collection->getValue('BUSINESS');
    }

    public function getNVPArray()
    {
        return $this->collection->getAllValues();
    }

    private function __clone()
    {
    }
}