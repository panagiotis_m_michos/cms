<?php

namespace PayPalNVP\Fields;

require_once 'Collection.php';
require_once 'Field.php';

final class PayerName implements Field
{

    /**
     * @var Collection
     */
    private $collection;

    private static $allowedValues = [
        'SALUTATION', 'FIRSTNAME', 'MIDDLENAME', 'LASTNAME', 'SUFFIX'];

    private function __construct()
    {
    }

    public static function getResponse(array $response)
    {

        $payerName = new self();
        $payerName->collection = new Collection(self::$allowedValues, $response);
        return $payerName;
    }

    /**
     * Payer's salutation.
     * Character length and limitations: 20 single-byte characters.
     *
     * @return string
     */
    public function getSalutation()
    {
        return $this->collection->getValue('SALUTATION');
    }

    /**
     * Payer's first name.
     * Character length and limitations: 25 single-byte characters.
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->collection->getValue('FIRSTNAME');
    }

    /**
     * Payer's middle name.
     * Character length and limitations: 25 single-byte characters.
     *
     * @return string
     */
    public function getMiddleName()
    {
        return $this->collection->getValue('MIDDLENAME');
    }

    /**
     * Payer's last name.
     * Character length and limitations: 25 single-byte characters.
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->collection->getValue('LASTNAME');
    }

    /**
     * Payer's suffix.
     * Character length and limitations: 12 single-byte characters.
     *
     * @return string
     */
    public function getSuffix()
    {
        return $this->collection->getValue('SUFFIX');
    }

    public function getNVPArray()
    {
        return $this->collection->getAllValues();
    }

    public function getFullName()
    {
        return $this->getFirstName() . ' ' . $this->getLastName();
    }

    private function __clone()
    {
    }
}