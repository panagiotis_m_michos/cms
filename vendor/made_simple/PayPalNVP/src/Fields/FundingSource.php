<?php

namespace PayPalNVP\Fields;

require_once 'Field.php';

final class FundingSource implements Field
{

    /**
     * @var Collection
     */
    private $collection;

    private static $allowedValues = ['ALLOWPUSHFUNDING'];

    private function __construct()
    {
    }

    public static function getReqeuest()
    {

        $funding = new self();
        $funding->collection = new Collection(self::$allowedValues, NULL);
        return $funding;
    }

    /**
     * Whether the merchant can accept push funding:
     * true — Merchant can accept push funding
     * false — Merchant cannot accept push funding
     * Note: This field overrides the setting in the merchant's PayPal account
     *
     * @param boolean $pushFunding
     */
    private function setAllowPushFunding($pushFunding)
    {
        $this->collection->setValue('ALLOWPUSHFUNDING', $pushFunding);
    }

    public function getNVPArray()
    {
        return $this->collection->getAllValues();
    }

    private function __clone()
    {
    }
}