<?php

namespace PayPalNVP\Fields;

require_once 'Collection.php';
require_once 'Field.php';

final class RecurringPaymentsSummary implements Field
{

    /**
     * @var Collection
     */
    private $collection;

    /** @var array values allowed in response */
    private static $allowedValues = ['NEXTBILLINGDATE',
        'NUMCYCLESCOMPLETED', 'NUMCYCLESREMAINING', 'OUTSTANDINGBALANCE',
        'FAILEDPAYMENTCOUNT', 'LASTPAYMENTDATE', 'LASTPAYMENTAMT'];

    private function __construct()
    {
    }

    /**
     * @param array $response nvp response represented as an array, array needs
     * to contain only keys without 'L_' prefix and 'n' suffix.
     * @return RecurringPaymentsSummary as response
     */
    public static function getResponse(array $response)
    {

        $summary = new self();
        $summary->collection = new Collection(self::$allowedValues, $response);
        return $summary;
    }

    /**
     * @return String the next scheduled billing date, in YYYY-MM-DD format
     */
    public function getNextBillingDate()
    {
        return $this->collection->getValue('NEXTBILLINGDATE');
    }

    /**
     * @return String the number of billing cycles completed in the current
     *      active subscription period. A billing cycle is considered completed
     *      when payment is collected or after retry attempts to collect
     *      payment for the current billing cycle have failed.
     */
    public function getNumberCyclesCompleted()
    {
        return (int)$this->collection->getValue('NUMCYCLESCOMPLETED');
    }

    /**
     * @return string the number of billing cycles remaining in the current
     *      active subscription period.
     */
    public function getNumberCyclesRemaining()
    {
        return $this->collection->getValue('NUMCYCLESREMAINING');
    }

    /**
     * @return string the current past due or outstanding balance for this
     *      profile.
     */
    public function getOutstandingBalance()
    {
        return $this->collection->getValue('OUTSTANDINGBALANCE');
    }

    /**
     * @return type the total number of failed billing cycles for this profile.
     */
    public function getFailedPaymentCount()
    {
        return $this->collection->getValue('FAILEDPAYMENTCOUNT');
    }

    /**
     * @return type the date of the last successful payment received for this
     *      profile, in YYYY-MM-DD format.
     */
    public function getLastPaymentDate()
    {
        return $this->collection->getValue('LASTPAYMENTDATE');
    }

    /**
     * @return type the amount of the last successful payment received for this profile.
     */
    public function getLastPaymentAmount()
    {
        return $this->collection->getValue('LASTPAYMENTAMT');
    }

    /**
     * @return array
     */
    public function getNVPArray()
    {
        return $this->collection->getAllValues();
    }

    private function __clone()
    {
    }
}

