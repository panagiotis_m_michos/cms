<?php

namespace PayPalNVP\Fields;

require_once 'Field.php';
require_once 'Collection.php';

final class Buyer implements Field
{

    /**
     * @var Collection
     */
    private $collection;

    private static $allowedValues = ['BUYERID', 'BUYERUSERNAME',
        'BUYERREGISTRATIONDATE'];

    private function __construct()
    {
    }

    public static function getReqeuest()
    {

        $buyer = new self();
        $buyer->collection = new Collection(self::$allowedValues, NULL);
        return $buyer;
    }

    /**
     * The unique identifier provided by eBay for this buyer. The value may
     * or may not be the same as the username. In the case of eBay, it is
     * different. Character length and limitations: 255 single-byte
     * characters
     *
     * @param String $id
     */
    public function setId($id)
    {
        $this->collection->setValue('BUYERID', $id);
    }

    /**
     * The user name of the user at the marketplaces site.
     *
     * @param String $userName
     */
    public function setUsername($username)
    {
        $this->collection->setValue('BUYERUSERNAME', $username);
    }

    /**
     * Date when the user registered with the marketplace.
     *
     * @param string $date - xs:dateTime
     */
    public function setRegistrationDate($registrationDate)
    {
        $this->collection->setValue('BUYERREGISTRATIONDATE', $registrationDate);
    }

    public function getNVPArray()
    {
        return $this->collection->getAllValues();
    }

    private function __clone()
    {
    }
}
