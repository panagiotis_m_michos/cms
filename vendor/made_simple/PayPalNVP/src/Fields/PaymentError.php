<?php

namespace PayPalNVP\Fields;

require_once 'Collection.php';
require_once 'Field.php';

final class PaymentError implements Field
{

    /**
     * @var Collection
     */
    private $collection;

    private static $allowedValues = ['SHORTMESSAGE', 'LONGMESSAGE',
        'ERRORCODE', 'SEVERITYCODE', 'ACK'];

    private function __construct()
    {
    }

    public static function getResponse(array $response)
    {

        $error = new self();
        $error->collection = new Collection(self::$allowedValues, $response);
        return $error;
    }

    public function getErrorCode()
    {
        return $this->collection->getValue('ERRORCODE');
    }

    public function getShortMessage()
    {
        return $this->collection->getValue('SHORTMESSAGE');
    }

    public function getLongMessage()
    {
        return $this->collection->getValue('LONGMESSAGE');
    }

    public function getSeverityCode()
    {
        return $this->collection->getValue('SEVERITYCODE');
    }

    /**
     * Appliaction-specific error values indicating more about the error
     * condition.
     *
     * @return string
     */
    public function getAck()
    {
        return $this->collection->getValue('ACK');
    }

    public function getNVPArray()
    {
        return $this->collection->getAllValues();
    }

    private function __clone()
    {
    }
}