<?php

namespace PayPalNVP\Fields;

/**
 * Acts as an Collection for nvp pairs
 * All keys are in upper case.
 */
final class Collection
{

    /** @var array<String, String> holds name value pair */
    private $nvp;

    /**
     * If second argument is not specified or is null, then this collection is
     * initialieze as empty
     *
     * @param array $allowedValues array of keys allowed in response
     * @param array $response nvp response as array
     */
    public function __construct(array $allowedValues, array $response = NULL)
    {

        /* convert response to upper case - paypal is inconsistent */
        $upperResponse = [];
        if ($response != NULL) {
            foreach ($response as $key => $value) {
                $key = strtoupper($key);
                $upperResponse[$key] = $value;
            }
        }

        $this->nvp = [];

        /* sets only values for this response field */
        if (!empty($upperResponse)) {
            foreach ($allowedValues as $key) {
                $key = strtoupper($key);
                if (isset($upperResponse[$key])) {
                    $this->nvp[$key] = $upperResponse[$key];
                }
            }
        }
    }

    /**
     * @param string $key
     * @return string or null if the key is not set
     */
    public function getValue($key)
    {
        return (isset($this->nvp[$key])) ? $this->nvp[$key] : NULL;
    }

    /**
     * @param string $key
     * @param string $value
     */
    public function setValue($key, $value)
    {
        $this->nvp[$key] = $value;
    }

    /**
     * @return array
     */
    public function getAllValues()
    {
        return $this->nvp;
    }

    private function __clone()
    {
    }
}