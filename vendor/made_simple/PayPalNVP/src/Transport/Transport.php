<?php

namespace PayPalNVP\Transport;

interface Transport
{

    /**
     * Sends request - $data to the $url
     *
     * @return  response from the server - $url
     */
    public function getResponse($url, $data);
}