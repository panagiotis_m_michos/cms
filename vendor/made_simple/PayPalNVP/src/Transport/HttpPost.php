<?php

namespace PayPalNVP\Transport;

use PayPalNVP\Exception\ResponseException;

final class HttpPost implements Transport
{

    /** timeout in seconds */
    private static $timeout = 30;

    /**
     * @param string $url
     * @param array $request
     * @return string
     * @throws ResponseException
     */
    public function getResponse($url, $request)
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        //curl_setopt($ch, CURLOPT_VERBOSE, true);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);

        curl_setopt($ch, CURLOPT_TIMEOUT, self::$timeout);

        $response = curl_exec($ch);

        // Verify that the request executed successfully.
        if (curl_errno($ch)) {
            $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $message = curl_error($ch);
            curl_close($ch);
            throw new ResponseException($message, $code);
        }

        curl_close($ch);

        return $response;
    }

    private function __clone()
    {
    }
}