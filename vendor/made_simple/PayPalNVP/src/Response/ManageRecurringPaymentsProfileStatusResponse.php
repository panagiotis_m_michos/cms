<?php

namespace PayPalNVP\Response;

require_once 'Response.php';
require_once __DIR__ . '/../Environment.php';
require_once __DIR__ . '/../Fields/Collection.php';

use PayPalNVP\Environment;
use PayPalNVP\Fields\Collection;

final class ManageRecurringPaymentsProfileStatusResponse extends Response
{

    /**
     * @var Collection
     */
    private $collection;

    private static $allowedValues = ['PROFILEID'];

    public function __construct($response, Environment $environment)
    {

        parent::__construct($response, $environment);
        $this->collection = new Collection(self::$allowedValues, $responseArray);
    }

    /**
     * Recurring payments profile ID returned in the
     * CreateRecurringPaymentsProfile response. For each action, an error is
     * returned if the recurring payments profile has a status that is not
     * compatible with the action. Errors are returned in the following cases:
     *  Cancel - Profile status is not Active or Suspended.
     *  Suspend - Profile status is not Active.
     *  Reactivate - Profile status is not Suspended.
     *
     * @return string
     */
    public function getProfileId()
    {
        return $this->collection->getValue('PROFILEID');
    }

    private function __clone()
    {
    }
}
