<?php

namespace PayPalNVP\Response;

require_once 'Response.php';
require_once __DIR__ . '/../Environment.php';
require_once __DIR__ . '/../Fields/Collection.php';

use PayPalNVP\Environment;
use PayPalNVP\Fields\Collection;

final class RefundTransactionResponse extends Response
{

    /**
     * @var Collection
     */
    private $collection;

    private static $allowedValues = ['REFUNDTRANSACTIONID',
        'FEEREFUNDAMT', 'GROSSREFUNDAMT', 'NETREFUNDAMT', 'TOTALREFUNDEDAMT'];

    public function __construct($response, Environment $environment)
    {

        parent::__construct($response, $environment);

        $this->collection = new Collection(self::$allowedValues, $this->getResponse());
    }

    /**
     * @return String Unique transaction ID of the refund. Character length and
     *      limitations:17 single-byte characters.
     */
    public function getRefundTransactionId()
    {
        return $this->collection->getValue('REFUNDTRANSACTIONID');
    }

    /**
     * @return String transaction fee refunded to original recipient of payment.
     */
    public function getRefundAmount()
    {
        return $this->collection->getValue('FEEREFUNDAMT');
    }

    /**
     * @return String amount refunded to original payer.
     */
    public function getGrossRefundAmount()
    {
        return $this->collection->getValue('GROSSREFUNDAMT');
    }

    /**
     * @return String amount subtracted from PayPal balance of original
     *      recipient of payment to make this refund.
     */
    public function getNetRefundAmount()
    {
        return $this->collection->getValue('NETREFUNDAMT');
    }

    /**
     * @return string total of all refunds associated with this transaction.
     */
    public function getTotalRefundAmount()
    {
        return $this->collection->getValue('TOTALREFUNDEDAMT');
    }

    private function __clone()
    {
    }
}



