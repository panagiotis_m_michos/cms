<?php

namespace PayPalNVP\Response;

require_once 'Response.php';
require_once __DIR__ . '/../Environment.php';
require_once __DIR__ . '/../Fields/Collection.php';
require_once __DIR__ . '/../Fields/PayerInformation.php';
require_once __DIR__ . '/../Fields/PayerName.php';
require_once __DIR__ . '/../Fields/Payment.php';
require_once __DIR__ . '/../Fields/UserOptions.php';

use PayPalNVP\Environment;
use PayPalNVP\Fields\Collection;
use PayPalNVP\Fields\Payment;

final class DoCaptureResponse extends Response
{

    /** @var array<Payment> */
    private $payment = [];

    /**
     * @var Collection
     */
    private $collection;

    private static $allowedValues = ['AUTHORIZATIONID', 'MSGSUBID'];

    public function __construct($response, Environment $environment)
    {

        parent::__construct($response, $environment);

        $responseArray = [];

        /* payment request */
        $payments = [];
        foreach ($this->getResponse() as $key => $value) {
            $keyParts = explode('_', $key);
            if (!empty($keyParts)) {

                // PAYMENTREQUEST_n_VALUE
                if ($keyParts[0] == 'PAYMENTREQUEST') {

                    $x = $keyParts[1];
                    /* [index][key] = value  */
                    $payments[$x][$keyParts[2]] = $value;

                    // L_PAYMENTREQUEST_n_VALUEn
                } elseif ($keyParts[0] == 'L' && count($keyParts) > 3) {

                    $x = $keyParts[2];
                    $rawValue = $keyParts[3];
                    preg_match('/[\d]+$/', $rawValue, $matches);
                    if (count($matches) == 1) {
                        $size = strlen($rawValue);
                        $index = $matches[0];
                        $indexSize = strlen($index);
                        $value = substr($rawValue, 0, $size - $indexSize);
                        $payments[$x][$index] = $value;
                    }
                }
            } else {
                $responseArray[$key] = $value;
            }
        }

        $this->collection = new Collection(self::$allowedValues, $responseArray);

        /* set payments */
        foreach ($payments as $index => $value) {
            $this->payment[$index] = Payment::getResponse($value);
        }
    }

    /**
     * Authorization identification number you specified in the request.
     * Character length and limits: 19 single-byte characters maximum
     *
     * @return string
     */
    public function getAuthorizationId()
    {
        return $this->collection->getValue('AUTHORIZATIONID');
    }

    /**
     * A message ID used for idempotence to uniquely identify a message. This
     * ID can later be used to request the latest results for a previous request
     * without generating a new request. Examples of this include requests due
     * to timeouts or errors during the original request.
     * Character length and limitations: string of up to 38 single-byte
     * characters.
     *
     * @return string
     */
    public function getMessageSubId()
    {
        return $this->collection->getValue('MSGSUBID');
    }

    /**
     * @return array<PaymentResponse>
     */
    public function getPayements()
    {
        return $this->payment;
    }

    private function __clone()
    {
    }
}