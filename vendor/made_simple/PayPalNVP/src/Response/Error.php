<?php

namespace PayPalNVP\Response;

require_once __DIR__ . '/../Fields/Collection.php';

use PayPalNVP\Fields\Collection;

final class Error
{

    /**
     * @var Collection
     */
    private $collection;

    private static $allowedValues = ['ERRORCODE', 'SHORTMESSAGE',
        'LONGMESSAGE', 'SEVERITYCODE'];

    public function __construct(array $response)
    {
        $this->collection = new Collection(self::$allowedValues, $response);
    }

    public function getErrorCode()
    {
        return $this->collection->getValue('ERRORCODE');
    }

    public function getShortMessage()
    {
        return $this->collection->getValue('SHORTMESSAGE');
    }

    public function getLongMessage()
    {
        return $this->collection->getValue('LONGMESSAGE');
    }

    public function getSeverityCode()
    {
        return $this->collection->getValue('SEVERITYCODE');
    }

    private function __clone()
    {
    }
}
