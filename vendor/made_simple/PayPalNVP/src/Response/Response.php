<?php

namespace PayPalNVP\Response;

use PayPalNVP\Environment;
use PayPalNVP\Exception\ResponseException;

abstract class Response
{

    /**
     * @var array response from paypal
     */
    protected $response;

    /**
     * @var Error[]
     */
    protected $errors;

    /**
     * @var Environment
     */
    protected $environment;

    /**
     * @param string $response response string from paypal
     * @param Environment $environment
     * @throws ResponseException
     */
    protected function __construct($response, Environment $environment)
    {

        $allowedErrors = [
            'ERRORCODE',
            'SHORTMESSAGE',
            'LONGMESSAGE',
            'SEVERITYCODE'
        ];

        $this->environment = $environment;
        $this->errors = [];
        $errors = [];

        $responseArray = explode('&', $response);
        $decodedResponse = [];
        foreach ($responseArray as $item) {

            $keyVal = explode('=', $item);

            if (!array_key_exists(0, $keyVal) || !array_key_exists(1, $keyVal)) {
                throw ResponseException::general($response);
            }

            $key = $keyVal[0];
            $value = $keyVal[1];

            $keyParts = explode('_', $key);
            if (!empty($keyParts)) {
                if ($keyParts[0] == 'L' && count($keyParts) > 1) {

                    $x = strtoupper($keyParts[1]);
                    preg_match('/[\d]+$/', $x, $matches);
                    if (count($matches) == 1) {
                        $size = strlen($x);
                        $index = $matches[0];
                        $indexSize = strlen($index);
                        $v = substr($x, 0, $size - $indexSize);
                        if (in_array($v, $allowedErrors)) {
                            $errors[$index][$v] = urldecode($value);
                        }
                    }
                }
            }
            $decodedResponse[$key] = urldecode($value);
        }

        /* set response */
        $this->response = $decodedResponse;
        /* set errors - objects */
        foreach ($errors as $key => $value) {
            $this->errors[$key] = new Error($value);
        }
    }

    /**
     * Acknowledgement status, which is one of the following values:
     * Success indicates a successful operation
     * SuccessWithWarning indicates a successful operation; however, there
     *      are messages returned in the response that you should examine
     * Failure indicates that the operation failed; the response will also
     *      contain one or more error message explaining the failure.
     * FailureWithWarning indicates that the operation failed and that there
     * are messages returned in the response that you should examine
     *
     * @return string
     */
    public function getAck()
    {
        return (isset($this->response['ACK'])) ? $this->response['ACK'] : NULL;
    }

    /**
     * @return bool
     */
    public function isAckSuccess()
    {
        return (strcasecmp($this->getAck(), 'SUCCESS') === 0);
    }

    /**
     * @return bool
     */
    public function isAckSuccessWithWarning()
    {
        return (strcasecmp($this->getAck(), 'SUCCESSWITHWARNING') === 0);
    }

    /**
     * The date and time that the requested API operation was performed
     *
     * @return String
     */
    public function getTimeStamp()
    {
        return (isset($this->response['TIMESTAMP'])) ? $this->response['TIMESTAMP'] : NULL;
    }

    /**
     * Correlation ID, which uniquely identifies the transaction to PayPal
     *
     * @return String
     */
    public function getCorrelationId()
    {
        return (isset($this->response['CORRELATIONID'])) ? $this->response['CORRELATIONID'] : NULL;
    }

    /**
     * The version of the API
     *
     * @return string
     */
    public function getVersion()
    {
        return (isset($this->response['VERSION'])) ? $this->response['VERSION'] : NULL;
    }

    /**
     * The sub-version of the API
     *
     * @return string
     */
    public function getBuild()
    {
        return (isset($this->response['BUILD'])) ? $this->response['BUILD'] : NULL;
    }

    /**
     * @return array decoded response from paypal as array
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Return array of Error object or empty array if no error was received
     *
     * @return Error[]
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Return converted array of Error objects to string
     *
     * @return string
     */
    public function getErrorMessages()
    {
        $errors = [];
        foreach ($this->errors as $error) {
            $errors[] = $error->getLongMessage();
        }
        return implode('; ', $errors);
    }

    /**
     * @return string
     */
    public function getErrorCodes()
    {
        $errorCodes = [];
        /** @var Error $error */
        foreach ($this->errors as $error) {
            $errorCodes[] = $error->getErrorCode();
        }

        return implode(', ', $errorCodes);
    }
}
