<?php

namespace PayPalNVP\Response;

use PayPalNVP\Environment;
use PayPalNVP\Exception\ResponseException;
use PayPalNVP\Fields\Collection;
use PayPalNVP\Fields\PaymentError;
use PayPalNVP\Fields\PaymentInfo;
use PayPalNVP\Fields\UserOptions;

final class DoExpressCheckoutPaymentResponse extends Response
{
    // TODO - seller details - looks different than existing

    /**
     * @var Collection
     */
    private $collection;

    /**
     * @var UserOptions
     */
    private $userOptions;

    /**
     * @var PaymentInfo[]
     */
    private $paymentInfo;

    /**
     * @var PaymentError[]
     */
    private $paymentError;

    /**
     * @var array
     */
    private static $allowedValues = [
        'TOKEN',
        'PAYMENTTYPE',
        'NOTE',
        'REDIRECTREQUIRED',
        'SUCCESSPAGEREDIRECTREQUESTED'
    ];

    /**
     * @param string $response
     * @param Environment $environment
     * @throws ResponseException
     */
    public function __construct($response, Environment $environment)
    {

        parent::__construct($response, $environment);

        $responseArray = [];
        $info = [];
        $error = [];
        foreach ($this->getResponse() as $key => $value) {

            $keyParts = explode('_', $key);
            if (!empty($keyParts)) {

                if ($keyParts[0] == 'PAYMENTINFO') {

                    $x = $keyParts[1];
                    /* [index][key] = value  */
                    $info[$x][$keyParts[2]] = $value;
                } elseif ($keyParts[0] == 'PAYMENTREQUEST') {

                    $x = $keyParts[1];
                    /* [index][key] = value  */
                    $error[$x][$keyParts[2]] = $value;
                }
            } else {
                $responseArray[$key] = $value;
            }
        }

        $this->collection = new Collection(self::$allowedValues, $responseArray);

        foreach ($info as $index => $value) {
            $this->paymentInfo[$index] = PaymentInfo::getResponse($value);
        }

        foreach ($error as $index => $value) {
            $this->paymentError[$index] = PaymentError::getResponse($value);
        }

        $this->userOptions = UserOptions::getResponse($responseArray);
    }

    /**
     * The timestamped token value that was returned by SetExpressCheckout
     * response and passed on GetExpressCheckoutDetails request.
     *
     * @return string
     */
    public function getToken()
    {
        return $this->collection->getValue('TOKEN');
    }

    /**
     * Information about the payment.
     *
     * @return string
     */
    public function getPaymentType()
    {
        return $this->collection->getValue('PAYMENTTYPE');
    }

    /**
     * The text entered by the buyer on the PayPal website if the ALLOWNOTE
     * field was set to 1 in SetExpressCheckout.
     *
     * @return string
     */
    public function getNote()
    {
        return $this->collection->getValue('NOTE');
    }

    /**
     * Flag to indicate whether you need to redirect the customer to back to
     * PayPal for guest checkout after successfully completing the transaction.
     * Note: Use this field only if you are using giropay or bank transfer
     * payment methods in Germany.
     *
     * @return string
     */
    public function getRedirectRequired()
    {
        return $this->collection->getValue('REDIRECTREQUIRED');
    }

    /**
     * Flag to indicate whether you need to redirect the customer to back to
     * PayPal after completing the transaction.
     *
     * @return string
     */
    public function getSuccessRedirectRequested()
    {
        return $this->collection->getValue('SUCCESSPAGEREDIRECTREQUESTED');
    }

    /**
     * @return PaymentInfo[]
     */
    public function getPaymentInfo()
    {
        return $this->paymentInfo;
    }

    /**
     * @return PaymentError[]
     */
    public function getPaymentError()
    {
        return $this->paymentError;
    }

    /**
     * @return UserOptions
     */
    public function getUserOptions()
    {
        return $this->userOptions;
    }

    /**
     * @param string $token
     * @return string
     */
    public function getRedirectUrl($token)
    {
        return sprintf(
            "https://www.%spaypal.com/cgi-bin/webscr?cmd=_express-checkout&token=%s",
            $this->environment->getEnvironmentPartUrl(),
            $token
        );
    }

    private function __clone()
    {
    }
}
