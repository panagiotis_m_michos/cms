<?php

namespace PayPalNVP\Response;

require_once 'Response.php';
require_once __DIR__ . '/../Environment.php';
require_once __DIR__ . '/../Fields/Collection.php';
require_once __DIR__ . '/../Util/Status.php';

use PayPalNVP\Environment;
use PayPalNVP\Fields\Collection;
use PayPalNVP\Util\Status;

final class CreateRecurringPaymentsProfileResponse extends Response
{

    /**
     * @var Collection
     */
    private $collection;

    private static $allowedValues = ['PROFILEID', 'STATUS'];

    public function __construct($response, Environment $environment)
    {

        parent::__construct($response, $environment);

        $this->collection = new Collection(self::$allowedValues, $this->getResponse());
    }

    /**
     * A unique identifier for future reference to the details of this
     * recurring payment. Character length and limitations:
     * Up to 14 single-byte alphanumeric characters.
     *
     * @return String
     */
    public function getProfileId()
    {
        return $this->collection->getValue('PROFILEID');
    }

    /**
     * Status of the recurring payment profile.
     *
     * @return Status
     */
    public function getStatus()
    {

        $value = $this->collection->getValue('STATUS');

        if ($value == NULL) {
            return NULL;
        }

        switch ($value) {
            case 'ActiveProfile':
                return Status::getActive();
                break;
            case 'PendingProfile':
                return Status::getPending();
                break;
        }

        return NULL;
    }

    private function __clone()
    {
    }
}

