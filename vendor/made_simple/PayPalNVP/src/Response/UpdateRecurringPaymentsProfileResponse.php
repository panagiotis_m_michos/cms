<?php

namespace PayPalNVP\Response;

require_once 'Response.php';
require_once __DIR__ . '/../Environment.php';
require_once __DIR__ . '/../Fields/Collection.php';

use PayPalNVP\Environment;
use PayPalNVP\Fields\Collection;

final class UpdateRecurringPaymentsProfileResponse extends Response
{

    /**
     * @var Collection
     */
    private $collection;

    private static $allowedValues = ['PROFILEID'];

    public function __construct($response, Environment $environment)
    {

        parent::__construct($response, $environment);

        $this->collection = new Collection(self::$allowedValues, $this->getResponse());
    }

    /**
     * @return String Recurring payments profile ID returned in the
     *      CreateRecurringPaymentsProfile response. An error is returned if
     *      the profile specified in the BillOutstandingAmount request has a
     *      status of canceled or expired.
     */
    public function getProfileId()
    {
        return $this->collection->getValue('PROFILEID');
    }

    private function __clone()
    {
    }
}


