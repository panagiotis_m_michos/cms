<?php

namespace PayPalNVP\Util;

/**
 * Type of PayPal page to display
 */
final class LandingPage
{

    private $landingPage;

    private function __construct($landingPage)
    {
        $this->landingPage = $landingPage;
    }

    /**
     * non-PayPal account
     * @return LandingPage
     */
    public static function getBilling()
    {
        return new self('Billing');
    }

    /**
     * PayPal account login
     * @return LandingPage
     */
    public static function getMark()
    {
        return new self('Login');
    }

    public function getValue()
    {
        return $this->landingPage;
    }

    private function __clone()
    {
    }
}


