<?php

namespace PayPalNVP\Util;

/**
 * Type of refund you are making:
 */
final class RefundType
{

    private $refundType;

    private function __construct($refundType)
    {
        $this->refundType = $refundType;
    }

    /**
     * @return RefundType
     */
    public static function getFull()
    {
        return new self('Full');
    }

    /**
     * @return RefundType
     */
    public static function getPartial()
    {
        return new self('Partial');
    }

    public function getValue()
    {
        return $this->refundType;
    }

    private function __clone()
    {
    }
}

