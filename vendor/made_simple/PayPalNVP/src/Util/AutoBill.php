<?php

namespace PayPalNVP\Util;

/**
 * This field indicates whether you would like PayPal to automatically bill the
 * outstanding balance amount in the next billing cycle. The outstanding
 * balance is the total amount of any previously failed scheduled payments
 * that have yet to be successfully paid.
 */
final class AutoBill
{

    private $autoBill;

    private function __construct($autoBill)
    {
        $this->autoBill = $autoBill;
    }

    /**
     * @return AutoBill
     */
    public static function getNoAutoBill()
    {
        return new self('NoAutoBill');
    }

    /**
     * @return AutoBill
     */
    public static function getAddToNexBilling()
    {
        return new self('AddToNextBilling');
    }

    public function getValue()
    {
        return $this->autoBill;
    }

    private function __clone()
    {
    }
}




