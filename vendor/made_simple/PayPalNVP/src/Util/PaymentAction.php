<?php

namespace PayPalNVP\Util;

final class PaymentAction
{

    private $paymentAction;

    private function __construct($paymentAction)
    {
        $this->paymentAction = $paymentAction;
    }

    /**
     * Indicates that this is a final sale for which you are requesting payment.
     * @return PaymentAction
     */
    public static function getSale()
    {
        return new self('Sale');
    }

    /**
     * Indicates that this payment is a basic authorization subject to
     * settlement with PayPal Authorization & Capture.
     * @return PaymentAction
     */
    public static function getAuthorization()
    {
        return new self('Authorization');
    }

    /**
     * Indicates that this payment is an order authorization subject to
     * settlement with PayPal Authorization & Capture.
     * @return PaymentAction
     */
    public static function getOrder()
    {
        return new self('Order');
    }

    public function getValue()
    {
        return $this->paymentAction;
    }

    private function __clone()
    {
    }
}