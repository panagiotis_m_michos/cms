<?php

namespace PayPalNVP\Util;

/**
 * Status of the recurring payment profile.
 */
final class Status
{

    private $status;

    private function __construct($status)
    {
        $this->status = $status;
    }

    /**
     * The recurring payment profile has been successfully created and
     * activated for scheduled payments according the billing instructions from
     * the recurring payments profile.
     *
     * @return Status
     */
    public static function getActive()
    {
        return new self('ActiveProfile');
    }

    /**
     * The system is in the process of creating the recurring payment profile.
     * Please check your IPN messages for an update.
     *
     * @return Status
     */
    public static function getPending()
    {
        return new self('PendingProfile');
    }

    public function getValue()
    {
        return $this->status;
    }

    private function __clone()
    {
    }
}





