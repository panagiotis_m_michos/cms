<?php

namespace PayPalNVP\Util;

/**
 * Type of channel
 */
final class ChannelType
{

    private $channelType;

    private function __construct($channelType)
    {
        $this->channelType = $channelType;
    }

    /**
     * non-auction seller
     * @return ChannelType
     */
    public static function getMerchant()
    {
        return new self('Merchant');
    }

    /**
     * eBay auction
     * @return ChannelType
     */
    public static function getEbayItem()
    {
        return new self('eBayItem');
    }

    public function getValue()
    {
        return $this->channelType;
    }

    private function __clone()
    {
    }
}



