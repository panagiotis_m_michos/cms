<?php

namespace PayPalNVP\Util;

/**
 * Utility for converting the DateTime.
 */
final class Date
{
    /**
     * @param \DateTime $dateTime
     * @return string
     */
    public static function getString(\DateTime $dateTime)
    {
        return $dateTime->format("Y-m-d\TH:i:s\Z");
    }

}

