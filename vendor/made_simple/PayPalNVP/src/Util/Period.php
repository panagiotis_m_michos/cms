<?php

namespace PayPalNVP\Util;

/**
 * Unit for billing during this subscription period.
 */
final class Period
{

    private $period;

    private function __construct($period)
    {
        $this->period = $period;
    }

    /**
     * @return Period
     */
    public static function getDay()
    {
        return new self('Day');
    }

    /**
     * @return Period
     */
    public static function getWeek()
    {
        return new self('Week');
    }

    /**
     * billing is done on the 1st and 15th of each month.
     * @return Period
     */
    public static function getSemiMonth()
    {
        return new self('SemiMonth');
    }

    /**
     * @return Period
     */
    public static function getMonth()
    {
        return new self('Month');
    }

    /**
     * @return Period
     */
    public static function getYear()
    {
        return new self('Year');
    }

    public function getValue()
    {
        return $this->period;
    }

    private function __clone()
    {
    }
}




