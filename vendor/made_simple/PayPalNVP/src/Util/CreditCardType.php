<?php

namespace PayPalNVP\Util;

/**
 * Type of credit card.
 */
final class CreditCardType
{

    private $creditCardType;

    private function __construct($creditCardType)
    {
        $this->creditCardType = $creditCardType;
    }

    /**
     * @return CreditCardType
     */
    public static function getVisa()
    {
        return new self('Visa');
    }

    /**
     * @return CreditCardType
     */
    public static function getMasterCard()
    {
        return new self('MasterCard');
    }

    /**
     * @return CreditCardType
     */
    public static function getDiscover()
    {
        return new self('Discover');
    }

    /**
     * @return CreditCardType
     */
    public static function getAmex()
    {
        return new self('Amex');
    }

    /**
     * Currencycode must be GBP. In addition, either STARTDATE or ISSUENUMBER
     * must be specified.
     *
     * @return CreditCardType
     */
    public static function getMaestro()
    {
        return new self('Maestro');
    }

    /**
     * Currencycode must be GBP. In addition, either STARTDATE or ISSUENUMBER
     * must be specified.
     *
     * @return CreditCardType
     */
    public static function getSolo()
    {
        return new self('Solo');
    }

    public function getValue()
    {
        return $this->creditCardType;
    }

    private function __clone()
    {
    }
}





