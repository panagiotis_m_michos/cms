<?php

namespace PayPalNVP\Util;

/**
 * Type of checkout flow
 */
final class SolutionType
{

    private $solutionType;

    private function __construct($solutionType)
    {
        $this->solutionType = $solutionType;
    }

    /**
     * Buyer does not need to create a PayPal account to check out.
     * This is referred to as PayPal Account Optional.
     * @return SolutionType
     */
    public static function getSole()
    {
        return new self('Sole');
    }

    /**
     * Buyer must have a PayPal account to check out.
     * @return SolutionType
     */
    public static function getMark()
    {
        return new self('Mark');
    }

    public function getValue()
    {
        return $this->solutionType;
    }

    private function __clone()
    {
    }
}

