<?php

namespace PayPalNVP\Util;

final class ItemCategory
{

    private $itemCategory;

    private function __construct($itemCategory)
    {
        $this->itemCategory = $itemCategory;
    }

    public static function getDigital()
    {
        return new self('Digital');
    }

    public static function getPhysical()
    {
        return new self('Physical');
    }

    public function getValue()
    {
        return $this->itemCategory;
    }

    private function __clone()
    {
    }
}