<?php

namespace PayPalNVP\Util;

/**
 * Determines where or not PayPal displays shipping address fields on the
 * PayPal pages. For digital goods, this field is required. You must set
 * it to NotDisplay.
 */
final class ShippingOption
{

    private $shippingOption;

    private function __construct($shippingOption)
    {
        $this->shippingOption = $shippingOption;
    }

    /**
     * PayPal displays the shipping address on the PayPal pages
     * @return ShippingOption
     */
    public static function getDisplay()
    {
        return new self('0');
    }

    /**
     * PayPal does not display shipping address fields whatsoever
     * @return ShippingOption
     */
    public static function getNoDisplay()
    {
        return new self('1');
    }

    /**
     * If you do not pass the shipping address, PayPal obtains it from the
     * buyer's account profile
     * @return ShippingOption
     */
    public static function getOrder()
    {
        return new self('2');
    }

    public function getValue()
    {
        return $this->shippingOption;
    }

    private function __clone()
    {
    }
}
