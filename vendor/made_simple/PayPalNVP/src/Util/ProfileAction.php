<?php

namespace PayPalNVP\Util;

/**
 * The action to be performed to the recurring payments profile.
 */
final class ProfileAction
{

    private $action;

    private function __construct($action)
    {
        $this->action = $action;
    }

    /**
     * Only profiles in Active or Suspended state can be canceled.
     *
     * @return ProfileAction
     */
    public static function getCancel()
    {
        return new self('Cancel');
    }

    /**
     * Only profiles in Active state can be suspended.
     *
     * @return ProfileAction
     */
    public static function getSuspend()
    {
        return new self('Suspend ');
    }

    /**
     * Only profiles in a suspended state can be reactivated.
     *
     * @return ProfileAction
     */
    public static function getReactivate()
    {
        return new self('Reactivate ');
    }

    public function getValue()
    {
        return $this->action;
    }

    private function __clone()
    {
    }
}





