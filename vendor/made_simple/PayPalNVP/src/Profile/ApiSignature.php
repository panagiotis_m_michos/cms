<?php

namespace PayPalNVP\Profile;

require_once 'Profile.php';

final class ApiSignature implements Profile
{

    private $userName;

    private $password;

    private $signature;

    private $subject;

    /**
     * Required parameters
     *
     * @param String $userName obtained from paypal
     * @param String $password obtained from paypal
     * @param String $signature obtained from paypal
     */
    public function __construct($userName, $password, $signature)
    {

        $this->userName = $userName;
        $this->password = $password;
        $this->signature = $signature;
        $this->subject = NULL;
    }

    /**
     * Email address of a PayPal account that has granted you permission to
     * make this call.
     * Set this parameter only if you are calling an API on a different
     * user's behalf
     *
     * @param String $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    public function getNVPProfile()
    {

        $nvpProfile = [];
        $nvpProfile['USER'] = $this->userName;
        $nvpProfile['PWD'] = $this->password;
        $nvpProfile['SIGNATURE'] = $this->signature;
        if ($this->subject != NULL) {
            $nvpProfile['SUBJECT'] = $this->subject;
        }
        return $nvpProfile;
    }

    public function isAPISignature()
    {
        return TRUE;
    }

    public function __toString()
    {
        return 'Instance of ApiCertificate class. Values: userName - '
        . $this->userName . ', password - ' . $this->password
        . ', signature - ' . $this->signature . ', subject - '
        . $this->subject;
    }

    private function __clone()
    {
    }
}