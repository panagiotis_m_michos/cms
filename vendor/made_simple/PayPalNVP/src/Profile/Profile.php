<?php

namespace PayPalNVP\Profile;

interface Profile
{

    /**
     * Represents profile - api signature or api certificate
     *
     * @return array<String, String>    name value pair representation of the
     *                                  profile
     */
    public function getNVPProfile();

    /**
     *
     * @return boolean  indicates if the server uses api signature, or api
     *                  certificate
     */
    public function isAPISignature();
}
