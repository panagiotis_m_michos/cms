## Install PHP Code Sniffer
#### from PEAR:
    pear install PHP_CodeSniffer
#### from Composer:
    composer global require 'squizlabs/php_codesniffer=*'

(Make sure you have ~/.composer/vendor/bin/ in your PATH.)

*or alternatively, include a dependency for squizlabs/php_codesniffer in your composer.json file:*

    {
        "require": {
            "squizlabs/php_codesniffer": "1.*"
        }
    }

## Add the configuration file for phpcs and git-hook to the project composer.json file
    
    repositories:  [
    ...
    {
        "type": "git",
        "url": "git@bitbucket.com:made_simple/phpcs.git"
    }
    {
        "type": "git",
        "url": "git@bitbucket.com:made_simple/git-hooks.git"
    }
    ],    
    require: {
        ...
        "made_simple/phpcs": "~1.0"
        "made_simple/git-hooks": "~1.0"
    }

## Update composer by running:
    composer update made_simple/phpcs made_simple/git-hooks
        
Change permissions to make the hook executable

    sudo chmod +x vendor/made_simple/git-hooks/pre-commit/common/pre-commit
    
link (create shortcut) the hook file into the path_to_git_project/.git/hooks directory

    Linux:
    ln vendor/made_simple/git-hooks/pre-commit/common/pre-commit .git/hooks/pre-commit