# Install with composer

    repositories: {
        "type": "git",
        "url": "git@bitbucket.com:made_simple/datagrid.git"
    }

    require: {
        "made_simple/datagrid": "~1.0"
    }


# Datasource
----

    <?php
    $datasource = dibi::select('*')
        ->from('customers');
    $datagrid = new Datagrid($datasource, 'customerId');

# Columns
    $datagrid->addTextColumn('firstName', 'First Name', 100);
    $datagrid->addNumericColumn('count', 'Count', 50);
    $datagrid->addSizeColumn('fileSize', 'File Size', 50);
    $datagrid->addCurrencyColumn('price', 'Price', 50);
    $datagrid->addDateColumn('dtc', 'Created', 50, 'd/M/Y);

## Custom Column(IDataGridColumn)
    $column = new DatagridSpecialColumn('typeId', 'Type');
    $datagrid->addColumn($column);

## Actions Column
    $actionsColumn = $this->addActionColumn('actions', 'Actions', 70);
    $actionsColumn->addAction('create', 'Create', $createUrl, DataGridAction::WITHOUT_KEY);
    $actionsColumn->addAction('edit', 'Edit', $editUrl);
    $actionsColumn->addAction('delete', 'Delete', $deleteUrl);

# Filters
    $datagrid->addTextFilter('firstName', 'First Name');
    $datagrid->addNumericFilter('count', 'Count');
    $datagrid->addDateFilter('dtc', 'Created');
    $datagrid->addSelectFilter('statusId', 'Status', Customer::$statuses);

## Custom filter
    $filter = new DataGridFilterSelect('filesStatusId', 'Document Status:');
    $filter->setOptions(Customer::$filesStatuses);
    $this->getFilter()->addFilter($filter);


# Batch Changes

Enable batch changes and set actions



Controller has to implement IDatagridBatchChangesDelegate

    class CustomersAdminControler extends DoctrineEntityAdminControler_Abstract implements IDatagridBatchChangesDelegate
    {
        public function renderList()
        {
            $datagrid = new Datagrid($datasource, 'customerId');
            $datagrid->enableBatchChanges();
            $datagrid->batchChanges->setActions(array('delete' => 'Delete'));
            $datagrid->batchChanges->setDelegate($this);
            $this->template->datagrid = $datagrid;
        }

        public function batchChangesFormSucceeded(FForm $form, array $data)
        {
            pr($data);

            // output
            array(2) {
               "action" => "delete" (6)
               "selected" => array(1) [
                  0 => "1"
               ]
            }
        }

        public function batchChangesFormFailed(FForm $form, Exception $e)
        {
        }
    }




# Export
    $datagrid->enableExport();

# Replacement
    $datagrid->addTestColumn('statusId', 'Status')
        ->replacement(Customer::$statuses);

# Callbacks
    class CustomersAdminControler extends DoctrineEntityAdminControler_Abstract
    {
        public function renderList()
        {
            // ....
            $datagrid->addTestColumn('title', 'Title')
                ->addCallback(array($this, 'Callback_titleColumn');
        }

        public function Callback_titleColumn(DataGridTextColumn $column, $text, DibiRow $row)
        {
            $router = FApplication::$router;
            $url = $router->link($row->nodeId);
            $a = Html::el('a')->setText($text)->href($url);
            return $a;
        }
    }



