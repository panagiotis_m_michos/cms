<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    DataGridTextFilter.php 2010-10-20 divak@gmail.com
 */

class DataGridFilterItem_Abstract extends Component implements IDataGridFilterItem
{
	/**
	 * @var string
	 */
	protected $caption;
	
	/**
	 * @var mixed
	 */
	protected $defaultValue;
    
    /**
     * table alias for joined queries
     * @var string
     */
    protected $tableAlias;
	
	/**
	 * @param string $caption
	 */
	public function __construct($name, $caption = NULL, $tableAlias = NULL)
	{
		parent::__construct(NULL, $name);
		$this->caption = $caption;
		$this->tableAlias = $tableAlias;
	}
	
	/**
	 * @param string $value
	 * @return DataGridTextFilter
	 */
	public function defaultFilter($value)
	{
		$this->defaultValue = $value;
		return $this;
	}
	
    public function getFieldName() {
        if ($this->tableAlias !== null) {
            return $this->tableAlias . '.' . $this->getName();
        }
        return $this->getName();
    }
	
	/**
     * @param IDataSource $datasource
     * @param string $value
	 * @return void
	 */
	public function applyFilter(IDataSource $datasource, $value)
	{
	}
	
	/**
	 * @param FForm $form
	 */
	public function addFormControl(FForm $form)
	{
	}
}