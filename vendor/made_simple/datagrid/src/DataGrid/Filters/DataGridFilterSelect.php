<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    DataGridFilterSelect.php 2010-10-22 divak@gmail.com
 */

class DataGridFilterSelect extends DataGridFilterItem_Abstract
{
    /**
     * @var array
     */
    private $options = array();

    /**
     * @var string
     */
    private $firstOption = '--- Select ---';

    /**
     * @param array $options
     */
    public function setOptions(array $options)
    {
        $this->options = $options;
    }

    /**
     * @return string
     */
    public function getFirstOption()
    {
        return $this->firstOption;
    }

    /**
     * @param string $firstOption
     */
    public function setFirstOption($firstOption)
    {
        $this->firstOption = $firstOption;
    }

    /**
     * @return void
     */
    public function applyFilter(IDataSource $datasource, $value)
    {
        if ($datasource instanceof DoctrineDataSource) {
            $key = 'select' . uniqid();
            $datasource->where("{$this->getFieldName()} = :$key", $key, $value);
        } else {
            $datasource->where('%n = %s', $this->getFieldName(), $value);
        }
    }

    /**
     * @param FForm $form
     */
    public function addFormControl(FForm $form)
    {
        $control = $form->addSelect($this->getName(), $this->caption, $this->options)->setFirstOption($this->firstOption);
        if ($this->defaultValue !== NULL) $control->setValue($this->defaultValue);
        return $control;
    }
}
