<?php

class DataGridFilterCheckbox extends DataGridFilterItem_Abstract
{
    /**
     * @var array
     */
    private $options = array();

    /**
     * @param array $options
     */
    public function setOptions(array $options)
    {
        $this->options = $options;
    }

    /**
     * @param array $value
     * @return $this
     */
    public function defaultFilter($value)
    {
        return parent::defaultFilter((array) $value);
    }

    /**
     * @param IDataSource $datasource
     * @param array $values
     */
    public function applyFilter(IDataSource $datasource, $values)
    {
        $datasource->where('%n IN %l', $this->getFieldName(), $values);
    }

    /**
     * @param FForm $form
     * @return FCheckbox
     */
    public function addFormControl(FForm $form)
    {
        $control = $form->addCheckbox($this->getName(), $this->caption, $this->options);
        if ($this->defaultValue !== NULL) {
            $control->setValue($this->defaultValue);
        }
        return $control;
    }
}
