<?php

use Nette\Utils\Strings;

class DataGridRenderer extends Object implements IDataGridRenderer
{
    /**
     * @var string
     */
    protected $cssClass = 'datagrid';

    /**
     * @var string
     */
    protected $tableCssClass = 'grid';

    /**
     * @var DataGrid
     */
    protected $datagrid;

    /**
     * @param IDataGrid $datagrid
     */
    public function __construct(IDataGrid $datagrid)
    {
        $this->datagrid = $datagrid;
    }

    /**
     * @param string $cssClass
     * @return DataGridRenderer
     */
    public function setCssClass($cssClass)
    {
        $this->cssClass = $cssClass;
        return $this;
    }

    /**
     * @return Html
     */
    public function getHtml()
    {
        $el = Html::el('div')->class($this->cssClass);
        $this->addFilter($el);
        $this->addTopExport($el);
        $this->addTopActions($el);

        $rows = $this->datagrid->getRows();
        if ($rows->count()) {

            if ($this->datagrid->hasBatchChangesEnabled()) {
                $el->add($this->datagrid->getBatchChanges()->getFormBegin());
            }

            $table = $el->create('table')->class($this->tableCssClass)->cellpading(0)->cellspacing(0);
            $this->addHeader($table, $rows);
            $this->addBody($el, $table, $rows);

            if ($this->datagrid->hasBatchChangesEnabled()) {
                $el->add($this->datagrid->getBatchChanges()->getFormEnd());
            }

            $this->addPaginator($el, $rows);
        } else {
            $this->addNoResults($rows, $el);
        }
        return $el->render(1);
    }

    /**
     * @param Html $el
     */
    protected function addFilter(Html $el)
    {
        if (!$this->datagrid->hasFilderDisabled()) {
            $filter = $el->create('div')->class('filter');
            $form = $this->datagrid->getFilter();
            $filter->setHtml($form->getHtml());
        }
    }

    /**
     * @param Html $el
     */
    protected function addTopExport(Html $el)
    {
        if (!$this->datagrid->hasExportDisabled()) {
            $ul = $el->create('ul')->class('top-export');
            $li = $ul->create('li');
            $li->add($this->datagrid->getExport()->getExportLink());
        }
    }


    /**
     * @param Html $el
     */
    protected function addTopActions(Html $el)
    {
        $columns = $this->datagrid->getColumns();
        $ul = Html::el('ul')->class('top-action');

        foreach ($columns as $column) {
            if ($column instanceof IDataGridActionColumn) {
                foreach ($column->getActions() as $action) {
                    if ($action->getType() === DataGridAction::WITHOUT_KEY) {
                        $li = $ul->create('li');
                        $li->setHtml($action->formatContent());
                    }
                }
            }
        }

        // --- add to container only if has any children
        if ($ul->getChildren()) {
            $el->add($ul);
        }
    }


    /**
     * @param Html $table
     * @param Iterator $rows
     */
    protected function addHeader(Html $table, Iterator $rows)
    {
        $tr = $table->create('tr');

        /* @var $column DataGridColumn */
        $columns = $this->datagrid->getColumns();
        foreach ($columns as $column) {

            // --- checking actions for DataGridActionColumn ---
            if ($column instanceof IDataGridActionColumn && !$column->hasActions()) {
                continue;
            }

            $caption = $column->getCaption();

            // --- order ---
            if ($column->getCanBeOrdered() && !$this->datagrid->hasOrderDisabled()) {
                $caption = Html::el('span')->class('order');

                // --- asc ---
                $asc = Html::el('a')
                    ->href($column->getOrderLink(DataGridOrder::ASC))
                    ->setText('▲')
                    ->title(DataGridOrder::ASC)->class('asc');
                if ($column->isOrdered() && $column->getOrderDirection() === DataGridOrder::ASC) {
                    $asc->attrs['class'] .= ' active';
                }
                $caption->add($asc);

                // --- caption ---
                $caption->add($column->getCaption());

                // --- desc ---
                $desc = Html::el('a')
                    ->href($column->getOrderLink(DataGridOrder::DESC))
                    ->setText('▼')
                    ->title(DataGridOrder::DESC)
                    ->class('desc');
                if ($column->isOrdered() && $column->getOrderDirection() === DataGridOrder::DESC) {
                    $desc->attrs['class'] .= ' active';
                }
                $caption->add($desc);
            }

            // --- actions column ---
            if ($column instanceof IDataGridActionColumn) {
                $colspan = 0;
                $actions = $column->getActions();
                foreach ($actions as $action) {
                    if ($action->getType() === DataGridAction::WITH_KEY) {
                        $colspan += 1;
                    }
                }
                if ($colspan > 0) {
                    $th = $tr->create('th')->add($caption);
                    $th->attrs = $column->getHeaderPrototype()->attrs;
                    $th->colspan($colspan);
                }
            } else {
                $th = $tr->create('th')->add($caption);
                $th->attrs = $column->getHeaderPrototype()->attrs;
                $th->add($column->getHeaderPrototype());
            }
        }
    }

    protected function addBody(Html $el, Html $table, Iterator $rows)
    {
        /* @var $column DataGridColumn */
        $columns = $this->datagrid->getColumns();
        foreach ($rows as $row) {

            $tr = $table->create('tr');
            foreach ($columns as $column) {
                $name = $column->getName();
                // --- actions ---
                if ($column instanceof IDataGridActionColumn) {
                    if (!$column->hasActions()) {
                        continue;
                    }
                    $actions = $column->getActions();
                    foreach ($actions as $action) {
                        if ($action->getType() === DataGridAction::WITH_KEY) {
                            $text = clone $action->formatContent($row);
                            $td = $tr->create('td')->add($text);
                            $td->attrs = $action->getCellPrototype()->attrs;
                            $td->class('action');
                        }
                    }
                } else {
                    $text = $this->getValue($column, $row, $name);
                    $td = $tr->create('td')->setHtml($text);
                    $td->attrs = $column->getCellPrototype()->attrs;

                }
            }
            $this->datagrid->modifyRow($tr, $row);
        }

        // batch actions
        if ($this->datagrid->hasBatchChangesEnabled()) {
            $actions = $this->datagrid->getBatchChanges()->getActions();
            $el->create('div')
                ->style('margin-top: 10px;')
                ->add($actions);
        }
    }

    /**
     * @param Html $el
     * @param Iterator $rows
     */
    protected function addPaginator(Html $el, Iterator $rows)
    {
        if (!$this->datagrid->hasPaginatorDisabled()) {
            $div = $el->create('div')->class('paginator');
            $paginator = $this->datagrid->getPaginator();
            $div->add($paginator->getHtml());
        }
    }

    /**
     * @param Iterator $rows
     * @param Html $el
     */
    protected function addNoResults(Iterator $rows, Html $el)
    {
        $table = $el->create('table')->class('grid')->cellpading(0)->cellspacing(0);

        // --- header ---
        $this->addHeader($table, $rows);

        // --- empty row text ---
        $colspan = 0;
        $columns = $this->datagrid->getColumns();
        foreach ($columns as $column) {
            if ($column instanceof IDataGridActionColumn) {
                $actions = $column->getActions();
                foreach ($actions as $action) {
                    if ($action->getType() === DataGridAction::WITH_KEY) {
                        $colspan += 1;
                    }
                }
            } else {
                $colspan += 1;
            }
        }
        $table->create('tr')->create('td')->setText('No results')->colspan($colspan);
    }

    /**
     * @param IDataGridColumn $column
     * @param mixed $row
     * @param string $key
     * @return mixed
     * @throws Exception
     */
    private function getValue(IDataGridColumn $column, $row, $key)
    {
        $getter = 'get' . Strings::firstUpper($key);

        if ($column instanceof DataGridBatchActionsColumn) {
            return $column->formatContent(NULL, $row);

        } elseif (method_exists($row, $getter)) {
            return $column->formatContent($row->$getter(), $row);

        } elseif (isset($row->$key)) {
            return $column->formatContent($row->$key, $row);

        } elseif (array_key_exists($key, $row)) {
            return $column->formatContent($row[$key], $row);

        } else {
            throw new Exception("Column $key doesn't exists");
        }
    }
}
