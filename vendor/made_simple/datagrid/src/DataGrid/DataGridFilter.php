<?php

class DataGridFilter extends ComponentContainer implements IDataGridFilter
{
    /**
     * @var DataGrid
     */
    protected $datagrid;

    /**
     * @param IDataGrid $datagrid
     */
    public function __construct(IDataGrid $datagrid)
    {
        $this->datagrid = $datagrid;
        $this->addComponent(new ComponentContainer, 'filters');
    }

    /**
     * @param IDataGridFilterItem $filter
     */
    public function addFilter(IDataGridFilterItem $filter)
    {
        $this->getComponent('filters')->addComponent($filter, $filter->getName());
    }

    /**
     * @return ArrayObject
     */
    public function getFilters()
    {
        $filters = new ArrayObject();
        foreach ($this->getComponent('filters')->getComponents() as $filter) {
            $name = $filter->getName();
            $filters->offsetSet($name, $filter);
        }
        return $filters;
    }

    /**
     * @param string $index
     * @throws Exception
     * @return mixed
     */
    public function getFilter($index)
    {
        $filters = $this->getFilters();
        if (!isset($filters[$index])) {
            throw new Exception("Filter ($index) doesn't exists");
        }
        return $filters[$index];
    }

    public function applyFilter()
    {
        $data = $this->getForm()->getValues();
        $datasource = $this->datagrid->getDataSource();
        foreach ($this->getFilters() as $filter) {
            $name = $filter->getName();
            if (isset($data[$name]) && !empty($data[$name])) {
                $filter->applyFilter($datasource, $data[$name]);
            }
        }
    }

    /**
     * @return boolean
     */
    protected function hasFilters()
    {
        return $this->getFilters()->count();
    }

    /**
     * @return FForm|NULL
     */
    public function getHtml()
    {
        if ($this->hasFilters()) {
            return $this->getForm();
        }
        return NULL;
    }

    /**
     * @return string
     */
    protected function getFilterFormName()
    {
        $name = sprintf("datagrid-%s-filter", $this->datagrid->getCounter());
        return $name;
    }


    /**
     * @return FForm
     */
    public function getForm()
    {
        $form = new FForm($this->getFilterFormName(), FForm::METHOD_GET);
        $form->setRenderClass('DataGridFormRenderer');

        $form->addFieldset('Filter');
        foreach ($this->getFilters() as $filter) {
            $filter->addFormControl($form);
        }
        $form->addSubmit('submit', 'Submit')->class('submit_button');
        $form->addSubmit('reset', 'Clear')->class('reset_button');

        $form->onValid = array($this, 'Form_submitted');
        $form->clean();
        $form->start();
        return $form;
    }

    /**
     * @param FForm $form
     */
    public function Form_submitted(FForm $form)
    {
        if ($form->isSubmitedBy('reset')) {
            foreach ($this->getFilters() as $filter) {
                $form[$filter->getName()]->setValue(NULL);
            }
        }
    }
}
