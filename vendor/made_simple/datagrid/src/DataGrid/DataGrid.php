<?php

class DataGrid extends Control implements IDataGrid
{
    /**
     * @var int
     */
    static private $instancesCounter = 0;

    /**
     * @var int
     */
    private $counter;

    /**
     * @var IDataSource
     */
    private $datasource;

    /**
     * @var string
     */
    private $primaryKey;

    /**
     * @var DataGridRenderer
     */
    protected $renderer;

    /**
     * @var DataGridPaginator
     */
    protected $paginator;

    /**
     * @var DataGridOrder
     */
    protected $order;

    /**
     * @var DataGridFilter
     */
    protected $filter;

    /**
     * @var DataGridBatchChanges
     */
    protected $batchChanges;

    /**
     * @var DataGridExport
     */
    protected $export;

    /**
     * @var boolean
     */
    private $disableOrder = FALSE;

    /**
     * @var boolean
     */
    private $disableExport = TRUE;

    /**
     * @var boolean
     */
    private $disableFilter = FALSE;

    /**
     * @var boolean
     */
    private $enabledBatchChanges = FALSE;

    /**
     * @var boolean
     */
    private $disablePaginator = FALSE;

    /**
     * @var DibiResultIterator
     */
    private $rows;

    /**
     * @var int
     */
    private $rowsCount = NULL;

    /**
     * @param IDataSource $datasource
     * @param string $primaryKey
     */
    public function __construct(IDataSource $datasource, $primaryKey)
    {
        parent::__construct();
        $this->counter = ++self::$instancesCounter;
        $this->datasource = $datasource;
        $this->primaryKey = $primaryKey;
    }

    protected function init()
    {
    }

    /****************************************** getters ******************************************/

    /**
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->primaryKey;
    }

    /**
     * @return int
     */
    public function getCounter()
    {
        return $this->counter;
    }

    /**
     * @return DibiResultIterator
     */
    public function getRows()
    {
        return $this->rows;
    }

    /**
     * @return int
     */
    public function getRowsCount()
    {
        if ($this->rowsCount !== NULL) {
            return $this->rowsCount;
        }
        return $this->datasource->count();
    }

    /**
     * @return DataGridRenderer
     */
    public function getRenderer()
    {
        if (!$this->renderer) {
            $this->renderer = new DataGridRenderer($this);
        }
        return $this->renderer;
    }

    /**
     * @return DataGridPaginator
     */
    public function getPaginator()
    {
        if (!$this->paginator) {
            $this->paginator = new DataGridPaginator($this);
        }
        return $this->paginator;
    }

    /**
     * @return DataGridOrder
     */
    public function getOrder()
    {
        if (!$this->order) {
            $this->order = new DataGridOrder($this);
        }
        return $this->order;
    }

    /**
     * @return DataGridFilter
     */
    public function getFilter()
    {
        if (!$this->filter) {
            $this->filter = new DataGridFilter($this);
        }
        return $this->filter;
    }

    /**
     * @return IDatagridBatchChanges
     */
    public function getBatchChanges()
    {
        if (!$this->batchChanges) {
            $this->batchChanges = new DatagridBatchChanges($this);
        }
        return $this->batchChanges;
    }

    /**
     * @param IDatagridBatchChanges $batchChanges
     */
    public function setBatchChanges(IDatagridBatchChanges $batchChanges)
    {
        $this->batchChanges = $batchChanges;
    }

    /**
     * @return DataGridExport
     */
    public function getExport()
    {
        if (!$this->export) {
            $this->export = new DataGridExport($this);
        }
        return $this->export;
    }

    /**
     * @return Html
     */
    public function getHtml()
    {
        $this->init();
        $this->applyFilters();
        return $this->getRenderer()->getHtml();
    }

    /**
     * @return ArrayObject|IDataGridColumn[]
     */
    public function getColumns()
    {
        $columns = new ArrayObject();
        foreach ($this->getComponents() as $column) {
            $columns->append($column);
        }
        return $columns;
    }

    /**
     * @param string $name
     * @return DataGridColumn
     * @throws Exception
     */
    public function getColumn($name)
    {
        $columns = $this->getColumns();
        if (!isset($columns[$name])) {
            throw new Exception("Column $name doesn't exits");
        }
        return $columns[$name];
    }


    /**
     * @return DibiDataSource
     */
    public function getDataSource()
    {
        return $this->datasource;
    }

    /****************************************** other ******************************************/

    private function applyFilters()
    {
        //filters needs to be applied first before calling get rows count
        $this->getFilter()->applyFilter();
        $this->rowsCount = $this->getRowsCount();
        $this->getExport()->applyExport();
        $this->getOrder()->applyOrder();
        $this->getPaginator()->applyPaginator();
        $this->rows = $this->datasource->getIterator();
    }

    /**
     * @param string $name
     * @param string $caption
     * @param int $width
     * @param string $class
     * @return DataGridTextColumn
     */
    public function addTextColumn($name, $caption = NULL, $width = NULL, $class = NULL)
    {
        $column = new DataGridTextColumn($name, $caption);
        $column->getHeaderPrototype()->width($width);
        if ($class) {
            $column->getCellPrototype()->class($class);
        }
        $column = $this->addColumn($column);
        return $column;
    }

    /**
     * @param string $name
     * @param string $caption
     * @param int $width
     * @param string $class
     * @return DataGridNumericColumn
     */
    public function addNumericColumn($name, $caption = NULL, $width = NULL, $class = NULL)
    {
        $column = new DataGridNumericColumn($name, $caption);
        $column->getHeaderPrototype()->width($width);
        if ($class) {
            $column->getCellPrototype()->class('center');
        }
        $column = $this->addColumn($column);
        return $column;
    }

    /**
     * @param string $name
     * @param string $caption
     * @param int $width
     * @param string $class
     * @return DataGridNumericColumn
     */
    public function addSizeColumn($name, $caption = NULL, $width = NULL, $class = NULL)
    {
        $column = new DataGridSizeColumn($name, $caption);
        $column->getHeaderPrototype()->width($width);
        if ($class) {
            $column->getCellPrototype()->class('center');
        }
        $column = $this->addColumn($column);
        return $column;
    }

    /**
     * @param string $name
     * @param string $caption
     * @param int $width
     * @param string $class
     * @return DataGridCurrencyColumn
     */
    public function addCurrencyColumn($name, $caption = NULL, $width = NULL, $class = NULL)
    {
        $column = new DataGridCurrencyColumn($name, $caption);
        $column->getHeaderPrototype()->width($width);
        if ($class) {
            $column->getCellPrototype()->class('center');
        }
        $column = $this->addColumn($column);
        return $column;
    }

    /**
     * @param string $name
     * @param string $caption
     * @param int $width
     * @param string $format
     * @param string $class
     * @throws Exception
     * @return DataGridDateColumn
     */
    public function addDateColumn($name, $caption = NULL, $width = NULL, $format = NULL, $class = NULL)
    {
        $column = new DataGridDateColumn($name, $caption);
        $column->getHeaderPrototype()->width($width);
        if ($class) {
            $column->getCellPrototype()->class('center');
        }
        $column->setFormat($format);
        $column = $this->addColumn($column);
        return $column;
    }

    /**
     * @param string $name
     * @param string $caption
     * @param int $width
     * @param string $class
     * @throws Exception
     * @return DataGridActionColumn
     */
    public function addActionColumn($name, $caption = NULL, $width = NULL, $class = NULL)
    {
        $columns = $this->getColumns();
        if (isset($columns[$name])) {
            throw new Exception("Column $name already exists");
        }
        $column = new DataGridActionColumn($name, $caption);
        $column->getHeaderPrototype()->width($width)->class($class);
        $column->getCellPrototype()->class($class);
        $this[$name] = $column;
        return $column;
    }

    /**
     * @param IDataGridColumn $column
     * @throws Exception
     * @return IDataGridColumn
     */
    public function addColumn(IDataGridColumn $column)
    {
        $name = $column->getName();
        $columns = $this->getColumns();
        if (isset($columns[$name])) {
            throw new Exception("Column $name already exists");
        }
        $this[$name] = $column;
        return $column;
    }

    /**
     * @param string $name
     * @param string $caption
     * @param string $tableAlias
     * @return DataGridFilterText
     */
    public function addTextFilter($name, $caption = NULL, $tableAlias = NULL)
    {
        $filter = new DataGridFilterText($name, $caption, $tableAlias);
        $this->getFilter()->addFilter($filter);
        return $filter;
    }

    /**
     * @param string $name
     * @param string $caption
     * @param string $tableAlias
     * @return DataGridFilterNumeric
     */
    public function addNumericFilter($name, $caption = NULL, $tableAlias = NULL)
    {
        $filter = new DataGridFilterNumeric($name, $caption, $tableAlias);
        $this->getFilter()->addFilter($filter);
        return $filter;
    }

    /**
     * @param string $name
     * @param string $caption
     * @param string $tableAlias
     * @return DataGridFilterDate
     */
    public function addDateFilter($name, $caption = NULL, $tableAlias = NULL)
    {
        $filter = new DataGridFilterDate($name, $caption, $tableAlias);
        $this->getFilter()->addFilter($filter);
        return $filter;
    }

    /**
     * @param string $name
     * @param string $caption
     * @param array $options
     * @param string $tableAlias
     * @return DataGridFilterSelect
     */
    public function addSelectFilter($name, $caption = NULL, array $options = array(), $tableAlias = NULL)
    {
        $filter = new DataGridFilterSelect($name, $caption, $tableAlias);
        $filter->setOptions($options);
        $this->getFilter()->addFilter($filter);
        return $filter;
    }

    /**
     * @param string $name
     * @param string $caption
     * @param array $options
     * @param string $tableAlias
     * @return DataGridFilterCheckbox
     */
    public function addCheckboxFilter($name, $caption = NULL, array $options = array(), $tableAlias = NULL)
    {
        $filter = new DataGridFilterCheckbox($name, $caption, $tableAlias);
        $filter->setOptions($options);
        $this->getFilter()->addFilter($filter);
        return $filter;
    }

    public function disableOrder()
    {
        $this->disableOrder = TRUE;
    }

    public function enableOrder()
    {
        $this->disableOrder = FALSE;
    }

    /**
     * @return boolean
     */
    public function hasOrderDisabled()
    {
        $count = $this->getRowsCount();
        if ($count == 0) {
            return TRUE;
        }
        return $this->disableOrder;
    }

    public function disableExport()
    {
        $this->disableExport = TRUE;
    }

    public function enableExport()
    {
        $this->disableExport = FALSE;
    }

    public function disableFilter()
    {
        $this->disableFilter = TRUE;
    }

    public function enableFilter()
    {
        $this->disableFilter = FALSE;
    }

    /**
     * @return boolean
     */
    public function hasExportDisabled()
    {
        $count = $this->getRowsCount();
        if ($count == 0) {
            return TRUE;
        }
        return $this->disableExport;
    }

    /**
     * @return boolean
     */
    public function hasFilderDisabled()
    {
        return $this->disableFilter;
    }

    public function enableBatchChanges()
    {
        $this->enabledBatchChanges = TRUE;

        // add batch action column
        $column = $this->getBatchChanges()->getColumn();
        $this->addColumn($column);
    }

    /**
     * @return boolean
     */
    public function hasBatchChangesEnabled()
    {
        $count = $this->getRowsCount();
        if ($count == 0) {
            return FALSE;
        }
        return $this->enabledBatchChanges;
    }

    public function disablePaginator()
    {
        $this->disablePaginator = TRUE;
    }

    public function enablePaginator()
    {
        $this->disablePaginator = FALSE;
    }

    /**
     * @return boolean
     */
    public function hasPaginatorDisabled()
    {
        return $this->disablePaginator;
    }

    /**
     * @param Html $row
     * @param mixed $tableRow
     */
    public function modifyRow(Html $row, $tableRow)
    {
    }

    /**
     * @return string
     */
    public function __toString()
    {
        try {
            return $this->getHtml();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
