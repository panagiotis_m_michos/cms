<?php

interface IDataGridActionColumn
{
    /**
     * @return IDataGridAction[]
     */
    function getActions();

    /**
     * @return boolean
     */
    function hasActions();
}