<?php

interface IDatagridBatchChanges
{
    /**
     * @return IDatagridColumn
     */
    public function getColumn();
    
    /**
     * @param IDatagridBatchChangesDelegate $delegate
     */
    public function setDelegate(IDatagridBatchChangesDelegate $delegate);
    
    /**
     * @return Html 
     */
    public function getFormBegin();
    
    /**
     * @return Html 
     */
    public function getCaption();
    
    /**
     * @param int $id
     * @return FControl 
     */
    public function getRowControl($id);
    
    /**
     * @return Html 
     */
    public function getActions();
    
    /**
     * @return Html 
     */
    public function getFormEnd();
    
}