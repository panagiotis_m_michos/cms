<?php

interface IDatagridBatchChangesDelegate
{
	public function batchChangesFormSucceeded(FForm $form, array $data);
	public function batchChangesFormFailed(FForm $form, Exception $e);
}