<?php

interface IArrayConvertable 
{

    /**
     * @return array
     */
    public function toArray();
}
