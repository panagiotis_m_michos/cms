<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    IDataGridExport.php 2011-03-28 divak@gmail.com
 */


interface IDataGridExport
{
	/**
	 * @param IDataGrid $datagrid
	 */
	function __construct(IDataGrid $datagrid);
	
	/**
	 * @return void
	 */
	function applyExport();

    /**
     * @return Html
     */
    function getExportLink();
}