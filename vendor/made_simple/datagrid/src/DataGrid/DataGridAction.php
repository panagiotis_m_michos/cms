<?php

use Nette\Utils\Strings;

class DataGridAction extends Component implements IDataGridAction
{
    /* types */
    const WITH_KEY = TRUE;
    const WITHOUT_KEY = FALSE;

    /**
     * @var string
     */
    private $name;

    /**
     * @var Html
     */
    private $caption;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $type;

    /**
     * @var Html
     */
    private $cell;

    /**
     * @var array
     */
    protected $callback;


    /***************************** interface IDataGridAction *****************************/


    /**
     * @param string $name
     * @param string $caption
     * @param string $url
     * @param string $type
     */
    public function __construct($name, $caption, $url, $type)
    {
        parent::__construct();
        $this->name = $name;
        $this->setCaption($caption);
        $this->url = $url;
        $this->setType($type);
        $this->cell = Html::el();
    }

    /**
     * @param boolean $need
     * @return DataGrid
     */
    public function getDataGrid($need = TRUE)
    {
        return $this->lookup('DataGrid', $need);
    }


    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     * @throws Exception
     */
    public function setType($type)
    {
        if ($type !== self::WITH_KEY && $type !== self::WITHOUT_KEY) {
            throw new Exception("Wrong type entered `$type`");
        }
        $this->type = $type;
        return $this;
    }

    /**
     * @return Html
     */
    public function getCellPrototype()
    {
        return $this->cell;
    }

    /**
     * @return Html
     */
    public function getCaptionPrototype()
    {
        return $this->caption;
    }

    /**
     * @param mixed $row
     * @return Html
     * @throws Exception
     */
    public function formatContent($row = NULL)
    {
        // --- callback ---
        if ($this->callback) {
            $caption = call_user_func($this->callback, $this, $row);
            if (!$caption instanceof Html) {
                throw new Exception("Callback for {$this->name} action  has to return instance of Html object");
            }
            $this->caption = $caption;

        } else {
            if ($this->type === self::WITHOUT_KEY) {
                $this->caption->href($this->url);
            } else {
                $key = $this->getDataGrid()->getPrimaryKey();
                $params = array($key => $this->getValue($row, $key));
                $url = FTools::addUrlParam($params, $this->url);
                $this->caption->href($url);
            }
        }
        return $this->caption;
    }

    /**
     * @param callable $callback
     * @return DataGridColumn_Abstract
     */
    public function setCallback(callable $callback)
    {
        $this->callback = $callback;
        return $this;
    }

    /**
     * @param mixed $caption (string, Html)
     * @return $this
     */
    private function setCaption($caption)
    {
        if ($caption instanceof Html) {
            $this->caption = $caption;
        } else {
            $this->caption = Html::el('a')->setText($caption);
        }
        return $this;
    }

    /**
     * @param string $row
     * @param string $key
     * @return mixed
     */
    private function getValue($row, $key)
    {
        $getter = 'get' . Strings::firstUpper($key);
        if (method_exists($row, $getter)) {
            return $row->$getter();
        }
        return $row->$key;
    }
}
