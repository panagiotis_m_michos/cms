<?php

class DataGridExport extends Object implements IDataGridExport
{
    /**
     * @var DataGrid
     */
    protected $datagrid;

    /**
     * @var array
     */
    protected $header = array();

    /**
     * @var array
     */
    private $headerReplacement = array();

    /**
     * @var array
     */
    protected $customColumns = array();

    /**
     * @var string
     */
    private $fileName = 'export.csv';

    /**
     * @var callable
     */
    private $rowCallback;


    /**
     * @param IDataGrid $datagrid
     */
    public function __construct(IDataGrid $datagrid)
    {
        $this->datagrid = $datagrid;
    }

    /**
     * @return array
     */
    public function getHeaderReplacement()
    {
        return $this->headerReplacement;
    }

    /**
     * @param $header
     */
    public function setHeader($header)
    {
        $this->header = $header;
    }

    /**
     * @param array $headerReplacement
     */
    public function setHeaderReplacement(array $headerReplacement)
    {
        $this->headerReplacement = $headerReplacement;
    }

    /**
     * @param array $customColumns
     */
    public function setCustomColumns(array $customColumns)
    {
        $this->customColumns = $customColumns;
    }

    /**
     * @param string $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @param callable $rowCallback
     */
    public function setRowCallback(callable $rowCallback)
    {
        $this->rowCallback = $rowCallback;
    }

    /**
     * @return Html
     */
    public function getExportLink()
    {
        $params = sprintf("%s=1", $this->getRequestName());
        $url = FTools::addUrlParam($params, NULL, '&');
        $a = Html::el('a')->href($url)->setText('Export to CSV');
        return $a;
    }

    public function applyExport()
    {
        if (!$this->datagrid->hasExportDisabled() && isset($_GET[$this->getRequestName()])) {
            $this->output();
        }
    }

    private function output()
    {
        ob_clean();

        header('Content-type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . $this->fileName);
        header('Pragma: no-cache');
        header('Expires: 0');
        $this->writeToOutput();
        exit;
    }

    /**
     * write data to the output
     */
    protected function writeToOutput()
    {
        $this->datagrid->getFilter()->applyFilter();
        $iterator = $this->datagrid->getDatasource()->getIterator();
        $iterator->rewind();
        $out = fopen('php://output', 'w');
        $currentRow = $iterator->current();
        $headerArr = $currentRow instanceof IArrayConvertable ? $currentRow->toArray() : (array) $currentRow;
        $header = $this->getHeader(array_keys($headerArr));
        fputcsv($out, $header, ',', '"');
        foreach ($iterator as $row) {
            if ($this->rowCallback) {
                $data = call_user_func_array($this->rowCallback, [$row]);
            } else {
                $rowArr = $row instanceof IArrayConvertable ? $row->toArray() : (array) $row;
                $data = $this->getRow($rowArr);
            }
            fputcsv($out, $data, ',', '"');
        }
        fclose($out);
    }

    /**
     * @param array $row
     * @return array
     */
    protected function getRow(array $row)
    {
        if (count($this->customColumns) == 0) {
            return $row;
        }

        $newRow = array();
        foreach ($this->customColumns as $column) {
            $newRow[$column] = $row[$column];
        }
        return $newRow;
    }

    /**
     * @param array $header
     * @return array
     */
    protected function getHeader(array $header)
    {
        if ($this->header) {
            return $this->header;
        }

        if ($this->headerReplacement) {
            foreach ($header as &$value) {
                if (isset($this->headerReplacement[$value])) {
                    $value = $this->headerReplacement[$value];
                }
            }
            unset($value);
        }

        return $header;
    }

    /**
     * @return string
     */
    private function getRequestName()
    {
        $name = sprintf("datagrid-%s-export", $this->datagrid->getCounter());
        return $name;
    }
}
