<?php

class DataGridBatchActionsColumn extends DataGridColumn_Abstract
{
    /**
     * @var IDatagridBatchChanges 
     */
    protected $batchChanges;

    public function setBatchChanges($batchChanges)
    {
        $this->batchChanges = $batchChanges;
    }

    public function getCaption()
    {
        if ($this->batchChanges) {
            $control = $this->batchChanges->getCaption();
            return $control;
        }
    }

    public function formatContent($text, $row)
    {
        if ($this->batchChanges) {
            $primaryKey = $this->getDataGrid()->getPrimaryKey();
            $id = $row->$primaryKey;
            $control = $this->batchChanges->getRowControl($id);
            return $control;
        }
    }

    public function getCanBeOrdered()
    {
        return FALSE;
    }
}
