<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    DataGridSizeColumn.php 2011-11-22 divak@gmail.com
 */

class DataGridSizeColumn extends DataGridColumn_Abstract
{
    /**
     * @param string $text
     * @param mixed $row
     */
    public function formatContent($text, $row)
    {
        $text = TemplateHelpers::bytes($text);
        $text = parent::formatContent($text, $row);
        return $text;
    }
}
