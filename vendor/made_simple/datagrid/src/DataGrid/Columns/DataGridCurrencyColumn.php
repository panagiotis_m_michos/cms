<?php

/**
 * CMS
 *
 * Copyright (c) 2012 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2012 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    DataGridCurrencyColumn.php 2012-02-13 divak@gmail.com
 */

class DataGridCurrencyColumn extends DataGridColumn_Abstract
{
    /**
     * @var string
     */
    private $currencyFormat = '£%s';

    /**
     * @return string
     */
    public function getCurrencyFormat()
    {
        return $this->currencyFormat;
    }

    /**
     * @param string $currencyFormat
     * @return DataGridCurrencyColumn
     */
    public function setCurrencyFormat($currencyFormat)
    {
        $this->currencyFormat = $currencyFormat;
        return $this;
    }

    /**
     * @param string $text
     * @param mixed $row
     */
    public function formatContent($text, $row)
    {
        $price = (double) $text;
        $price = self::getRoundedPrice($price);
        $price = sprintf($this->currencyFormat, $price);
        $price = parent::formatContent($price, $row);
        return $price;
    }

    /**
     * @param double $price
     * @return double
     */
    private static function getRoundedPrice($price)
    {
        $price = round($price, 2);
        $price = number_format($price, 2, '.', '');
        return $price;
    }
}
