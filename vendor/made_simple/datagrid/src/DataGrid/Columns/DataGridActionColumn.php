<?php

class DataGridActionColumn extends ComponentContainer implements IDataGridColumn, IDataGridActionColumn
{
    /**
     * @var Html
     */
    private $header;

    /**
     * @var Html
     */
    private $cell;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $caption;


    /****************************** interface IDataGridColumn ************************************/

    /**
     * @param string $name
     * @param string $caption
     */
    public function __construct($name, $caption = NULL)
    {
        parent::__construct();
        $this->name = $name;
        $this->caption = $caption;
        $this->header = Html::el();
        $this->cell = Html::el();

        $this->addComponent(new ComponentContainer, 'actions');
        $this->monitor('DataGrid');
    }

    /**
     * @param DataGrid $component
     * @return void
     */
    protected function attached($component)	
    {
        if ($component instanceof IDataGrid) {
            $this->setParent($component);
        }
    }

    /**
     * @param string $name
     * @param string $caption
     * @param string $url
     * @param bool|string $type
     * @throws Exception
     * @return \DataGridAction
     */
    public function addAction($name, $caption = NULL, $url = NULL, $type = DataGridAction::WITH_KEY)
    {
        $actions = $this->getActions();
        if (isset($actions[$name])) throw new Exception("Action `$name` already exists");
        $action = new DataGridAction($name, $caption, $url, $type);
        $this->getComponent('actions')->addComponent($action, $name);
        return $action;
    }

    /**
     * @param string $name
     * @param string $caption
     * @param string $url
     * @return DataGridAction
     */
    public function addGlobalAction($name, $caption = NULL, $url = NULL)
    {
        return $this->addAction($name, $caption, $url, DataGridAction::WITHOUT_KEY);
    }

    /**
     * @return ArrayObject<DataGridAction>
     */
    public function getActions()
    {
        $actions = new ArrayObject();
        foreach ($this->getComponent('actions')->getComponents() as $action) {
            $actions->append($action);
        }
        return $actions;
    }

    /**
     * @param boolean $need
     * @return DataGrid
     */
    public function getDataGrid($need = TRUE)
    {
        return $this->lookup('DataGrid', $need);
    }

    /**
     * @return string
     */
    public function getCaption()
    {
        if (!$this->caption) return $this->getName();
        return $this->caption;
    }

    /**
     * @return Html
     */
    public function getHeaderPrototype()
    {
        return $this->header;
    }

    /**
     * @return Html
     */
    public function getCellPrototype()
    {
        return $this->cell;
    }

    /**
     * @param string $text
     * @param mixed $row
     * @throws Exception
     */
    public function formatContent($text, $row)
    {
        throw new Exception("Not implemented.");
    }

    /**
     * @return boolean
     */
    public function getCanBeOrdered()
    {
        return FALSE;
    }

    /**
     * @param string $dir
     * @throws Exception
     */
    public function getOrderLink($dir)
    {
        throw new Exception("Not implemented.");
    }

    /**
     * @throws Exception
     * @return string
     */
    public function getOrderDirection()
    {
        throw new Exception("Not implemented.");
    }

    /**
     * @return boolean
     */
    public function isOrdered()
    {
        return FALSE;
    }


    /****************************** interface IDataGridActionColumn ******************************/


    /**
     * @return boolean
     */
    public function hasActions()
    {
        $actions = $this->getActions();
        return (bool) $actions->count();
    }

}
