<?php

use Doctrine\ORM\QueryBuilder;

/**
 * datasource interface is empty adding methods for filters, paginators
 */
class DoctrineDataSource implements IDataSource
{

    /**
     * @var QueryBuilder 
     */
    private $queryBuilder;

    /**
     * @param QueryBuilder $queryBuilder
     */
    public function __construct(QueryBuilder $queryBuilder)
    {
        $this->queryBuilder = $queryBuilder; 
    }

    /**
     * @param string $condition
     * @param string $key
     * @param mixed $value
     * @return DoctrineDataSource
     */
    public function where($condition, $key, $value)
    {
        $this->queryBuilder->andWhere($condition)
            ->setParameter($key, $value);
        return $this; 
    }

    /**
     * @param int $limit
     * @return DoctrineDataSource 
     */
    public function limit($limit)
    {
        $this->queryBuilder->setMaxResults($limit);
        return $this;
    }

    /**
     * @param mixed $offset
     * @return DoctrineDataSource
     */
    public function offset($offset)
    {
        $this->queryBuilder->setFirstResult($offset);
        return $this;
    }

    /**
     * @TODO implement this
     * @param string $clause
     * @return DoctrineDataSource
     */
    public function removeClause($clause)
    {
        return $this;
    }

    /**
     * @param string $fieldName
     * @param string $direction
     * @return DoctrineDataSource
     */
    public function orderBy($fieldName, $direction)
    {
        $rootEntityAlias = $this->queryBuilder->getRootAlias();
        if ($rootEntityAlias) {
            if (strpos($fieldName, $rootEntityAlias . '.') !== 0) {
                $fieldName = $rootEntityAlias . '.' . $fieldName; 
            }
        }
        $this->queryBuilder->orderBy($fieldName, $direction);
        return $this;
    }

    /**
     * @return int
     */
    public function count()
    {
        $rootEntityAlias = $this->queryBuilder->getRootAlias();
        $builder = clone $this->queryBuilder;
        return $builder->select("count($rootEntityAlias)")->getQuery()->getSingleScalarResult();
    }

    /**
     * @return ArrayIterator
     */
    public function getIterator()
    {
        return new ArrayIterator($this->queryBuilder->getQuery()->getResult());
    }
}
