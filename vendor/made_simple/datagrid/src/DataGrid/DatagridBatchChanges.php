<?php

class DatagridBatchChanges extends ComponentContainer implements IDatagridBatchChanges
{
	/**
	 * @var DataGrid
	 */
	protected $datagrid;
    
    /**
     * @var \FForm 
     */
    protected $form;
    
    /**
     * @var IBatchChangesDelegate 
     */
    protected $delegate;
    
    /**
     * @var array 
     */
    protected $actions = array();
	
	/**
	 * @param IDataGrid $datagrid
	 */
	public function __construct(IDataGrid $datagrid)
	{
		$this->datagrid = $datagrid;
		$this->addComponent(new ComponentContainer, 'batchChanges');
	}
    
    /**
     * @param IDatagridBatchChangesDelegate $delegate 
     */
    public function setDelegate(IDatagridBatchChangesDelegate $delegate)
    {
        $this->delegate = $delegate;
    }
    
    /**
     * @param array $actions 
     */
    public function setActions(array $actions)
    {
        $this->actions = $actions;
    }
    
    /**
     * @return Html 
     */
    public function getFormBegin()
    {
        return $this->getForm()->getBegin();
    }
    
    /**
     * @return Html 
     */
    public function getFormEnd()
    {
        return $this->getForm()->getEnd();
    }
    
    /**
     * @return IDataGridBatchActionsColumn
     */
    public function getColumn()
    {
        $column = new DataGridBatchActionsColumn('batchActions', 'Caption');
        $column->getHeaderPrototype()->width(20);
        $column->setBatchChanges($this);
        return $column;
    }
    
    /**
     * @return Html 
     */
    public function getCaption()
    {
        return $this->getForm()->getControl('batchSelectAll');
    }
    
    /**
     * @param int $id
     * @return type 
     */
    public function getRowControl($id)
    {
        return $this->getForm()->getControl("batchRow_$id");
    }
    	
    
    /**
     * @return Html 
     */
    public function getActions()
    {
        $form = $this->getForm();
        
        $div = Html::el('div');
        $label = $form->getLabel('action');
        $div->add($label);

        $select = $form->getControl('action');
        $div->add($select);

        $submit = $form->getControl('submit');
        $div->add($submit);
        
        return $div;
    }
    
	/**
	 * @param \FForm $form
	 */
	public function Form_submitted(FForm $form)
	{
        if ($this->delegate) {
            $needle = 'batchRow_';
            $values = $form->getValues();
            $data = array('action' => $values['action'], 'selected' => array());
            foreach ($values as $key => $value) {
                if (String::startsWith($key, $needle) && $value) {
                    $id = str_replace($needle, '', $key);
                    $data['selected'][] = $id;
                }
            }
            $this->delegate->batchChangesFormSucceeded($form, $data);
        }
        $form->clean();
	}
    
    /**
	 * @return string
	 */
	protected function getFilterFormName()
	{
		$name = sprintf("datagrid-%s-batch-changes", $this->datagrid->getCounter());
		return $name;
	}
    
    /**
	 * @return FForm
	 */
	protected function getForm()
	{
        if (!$this->form) {

            $this->form = new FForm($this->getFilterFormName());
            $this->form->setRenderClass('DataGridFormRenderer');

            // check all
            $this->form->addCheckbox('batchSelectAll', NULL, 1);
            
            // rows 
            foreach ($this->getDataSource() as $id) {
                $this->form->addCheckbox("batchRow_$id", NULL, 1);
            }

            $this->form->addSelect('action', 'With selected:', $this->actions)
                ->setFirstOption('--- Select ---');

            $this->form->addSubmit('submit', 'Submit')
                ->style('width: 70px;')
                ->class('btn');

            $this->form->onValid = array($this, 'Form_submitted');
            $this->form->clean();
            $this->form->start();
        }
		return $this->form;
	}
    
    /**
     * @return array 
     */
    protected function getDataSource()
    {
        $datasource = array();
        $primaryKey = $this->datagrid->getPrimaryKey();
        foreach ($this->datagrid->getRows() as $row) {
            $datasource[] = $row->$primaryKey;
        }
        return $datasource;
    }
}