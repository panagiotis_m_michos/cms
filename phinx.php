<?php

use BootstrapModule\Ext\Config\ConfigYmlExt;
use Utils\File;

require_once dirname(__FILE__) . '/console/cli-config.php';

$parameterBag = $container->getParameterBag();

$environment = $parameterBag->get('environment');
$configuration = array(
    "paths" => array(
        "migrations" => DOCUMENT_ROOT . "/storage/database/migrations"
    ),
    "environments" => array(
        "default_migration_table" => "phinx_log",
        "default_database" => $environment
    )
);

$environments[$environment] = $parameterBag->get('database');
$testConfig = new File(DOCUMENT_ROOT . '/project/config/app/config.test.yml');
if ($testConfig->isReadable()) {
    $configYml = new ConfigYmlExt();
    $resources = [];
    $testConfig = $configYml->import($parameterBag, $testConfig, $resources);
    $environments[Environment::CONSOLE] = $testConfig['database'];
}

foreach ($environments as $key => $database) {
    $dbSettings = array(
        'adapter' => 'mysql',
        'host' => $database['host'],
        'name' => $database['database'],
        'user' => $database['username'],
        'pass' => $database['password'],
        'port' => 3306,
    );
    $configuration['environments'][$key] = $dbSettings;
}

return $configuration;