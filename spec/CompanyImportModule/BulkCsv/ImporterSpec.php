<?php

namespace spec\CompanyImportModule\BulkCsv;

use BasketModule\Services\PriceCalculator;
use BasketModule\ValueObject\Price;
use CompanyImportModule\BulkCsv\Importer;
use CompanyImportModule\BulkCsv\IncorporationFactory;
use Doctrine\ORM\EntityManager;
use Entities\Company;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation;
use Entities\Customer;
use Entities\OrderItem;
use FormSubmissionModule\Services\DocumentUploader;
use OrderModule\Builders\OrderItemBuilder;
use OrderModule\Builders\OrderItemBuilderFactory;
use OrderModule\Processors\OrderItemsProcessor;
use OrderModule\Resolvers\PrintedDocumentResolver;
use Package;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Services\PackageService;
use Services\ProductService;

/**
 * @mixin Importer
 */
class ImporterSpec extends ObjectBehavior
{
    /**
     * @var PrintedDocumentResolver
     */
    private $printedDocumentResolver;

    /**
     * @var PriceCalculator
     */
    private $priceCalculator;

    /**
     * @var OrderItemBuilderFactory
     */
    private $orderItemBuilderFactory;

    /**
     * @var PackageService
     */
    private $packageService;

    /**
     * @var IncorporationFactory
     */
    private $incorporationFactory;

    function let(
        EntityManager $entityManager,
        OrderItemsProcessor $orderItemsProcessor,
        ProductService $productService,
        PriceCalculator $priceCalculator,
        DocumentUploader $documentUploader,
        IncorporationFactory $incorporationFactory,
        PackageService $packageService,
        OrderItemBuilderFactory $orderItemBuilderFactory,
        PrintedDocumentResolver $printedDocumentResolver
    )
    {
        $this->incorporationFactory = $incorporationFactory;
        $this->packageService = $packageService;
        $this->orderItemBuilderFactory = $orderItemBuilderFactory;
        $this->priceCalculator = $priceCalculator;
        $this->printedDocumentResolver = $printedDocumentResolver;
        $this->beConstructedWith(
            $entityManager,
            $orderItemsProcessor,
            $productService,
            $this->priceCalculator,
            $documentUploader,
            $this->incorporationFactory,
            $this->packageService,
            $this->orderItemBuilderFactory,
            $this->printedDocumentResolver
        );
    }

    function it_can_import(
        Customer $customer,
        CompanyIncorporation $incorporation1,
        Company $company,
        Package $package,
        OrderItemBuilder $orderItemBuilder,
        OrderItem $orderItem,
        Price $price
    )
    {
        $filename = tempnam(sys_get_temp_dir(), 'spec');
        file_put_contents($filename, ",Share Information,,,Registered Office Address,,,,,Director Information,,,,,Director Service Address,,,,,Director Residential Address,,,,,Shareholder Information,,,,,,,,Shareholder security questions,,
Company Name:,Share currency:,Share class:,Value per share:,Building name or number:,Street:,Town:,Postcode:,Country:,First name:,Surname:,Nationality:,Occupation:,Date of Birth:,Building name or number:,Street:,Town:,Postcode:,Country,Building name or number:,Street:,Town:,Postcode:,Country,First name:,Surname:,Building name or number:,Street:,Town:,Postcode:,Country,Shares Allocated,Eye Colour,Telephone Number,Town of Birth
Company 1,GBP,ORD,1,2,Market Place,Carrickfergus,BT38 7Aw,Northern Ireland,William,Lewis,Northern Irish,Director,05/03/1958,20,Carrickfergus Road,Carrickfergus,BT38 7AW,Northern Ireland,20,Carrickfergus Road,Carrickfergus,BT38 7AW,Northern Ireland,William ,Lewis,20,Carrickfergus Road,Carrickfergus,BT38 7AW,Northern Ireland,1,Bro,162,NOR
Company 2,GBP,ORD,1,2,Market Place,Carrickfergus,BT38 7Aw,Northern Ireland,Example 1,Example 1,Northern Irish,Director,05/03/1958,21,Carrickfergus Road,Carrickfergus,BT38 7AW,Northern Ireland,21,Carrickfergus Road,Carrickfergus,BT38 7AW,Northern Ireland,Example 1,Example 1,21,Carrickfergus Road,Carrickfergus,BT38 7AW,Northern Ireland,1,Blu,163,NOR
");
        $file = new \SplFileObject($filename);

        $this->incorporationFactory->buildIncorporation($customer, Argument::any())->willReturn($incorporation1);
        $incorporation1->getCompany()->willReturn($company);

        $this->packageService->getPackageById(1315)->willReturn($package);

        $company->getCompanyName()->willReturn('name');
        $company->getId()->willReturn(1);
        $company->getCustomer()->willReturn($customer);

        $package->getId()->willReturn(1);
        $package->setCompanyId(1)->shouldBeCalled();
        $package->getCompanyEntity()->willReturn($company);
        $package->setCompanyEntity($company)->shouldBeCalled();

        $this->orderItemBuilderFactory->createBuilder()->willReturn($orderItemBuilder);
        $orderItemBuilder->setOrder(Argument::any())->willReturn($orderItemBuilder);
        $orderItemBuilder->setCompany(Argument::any())->willReturn($orderItemBuilder);
        $orderItemBuilder->setProductWithPrice(Argument::any())->willReturn($orderItemBuilder);
        $orderItemBuilder->build()->willReturn($orderItem);

        $this->printedDocumentResolver->resolveDocuments(1, $customer, $company)->shouldBeCalled();

        $this->priceCalculator->calculatePrice(Argument::any())->willReturn($price);

        $this->import($customer, 1315 ,$file);

        unlink($filename);
    }
}
