<?php

namespace spec\CompanyImportModule\BulkCsv;

use CompanyImportModule\BulkCsv\Exceptions\InvalidCsvFormat;
use CompanyImportModule\BulkCsv\IncorporationFactory;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporationFactory;
use Entities\Customer;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin IncorporationFactory
 */
class IncorporationFactorySpec extends ObjectBehavior
{
    /**
     * @var CompanyIncorporationFactory
     */
    private $companyIncorporationFactory;

    function let(CompanyIncorporationFactory $companyIncorporationFactory)
    {
        $this->companyIncorporationFactory = $companyIncorporationFactory;
        $this->beConstructedWith($this->companyIncorporationFactory);
    }

    function it_will_throw_on_invalid_column_count(Customer $customer)
    {
        $this->shouldThrow(InvalidCsvFormat::class)->during('buildIncorporation', [$customer, ['', '']]);
    }

    function it_will_throw_on_invalid_ro_country(Customer $customer)
    {
        $rows = [
            'company_name',
            'share_currency',
            'share_class',
            'value_per_share',
            'ro_address_line_1',
            'ro_address_line_2',
            'ro_address_town',
            'ro_address_postcode',
            'QWE',
            'director_first_name',
            'director_surname',
            'director_nationality',
            'director_occupation',
            'director_date_of_birth',
            'sa_address_line_1',
            'sa_address_line_2',
            'sa_address_town',
            'sa_address_postcode',
            'sa_address_country',
            'residential_address_line_1',
            'residential_address_line_2',
            'residential_address_town',
            'residential_address_postcode',
            'residential_address_country',
            'shareholder_first_name',
            'shareholder_surname',
            'shareholder_address_line_1',
            'shareholder_address_line_2',
            'shareholder_address_town',
            'shareholder_address_postcode',
            'shareholder_address_country',
            'shareholder_num_of_shares',
            'shareholder_auth_eye_color',
            'shareholder_auth_phone_number',
            'shareholder_auth_birth_town',
        ];

        $this->shouldThrow(InvalidCsvFormat::class)->during('buildIncorporation', [$customer, $rows]);
    }

    function it_creates_incorporation(Customer $customer, CompanyIncorporation $companyIncorporation)
    {
        $rows = [
            "Company 1",
            "GBP",
            "ORD",
            "1",
            "2",
            "Market Place",
            "Carrickfergus",
            "BT38 7Aw",
            "Northern Ireland",
            "William",
            "Lewis",
            "Northern Irish",
            "Director",
            "05/03/1958",
            "20",
            "Carrickfergus Road",
            "Carrickfergus",
            "BT38 7AW",
            "Northern Ireland",
            "20",
            "Carrickfergus Road",
            "Carrickfergus",
            "BT38 7AW",
            "Northern Ireland",
            "William",
            "Lewis",
            "20",
            "Carrickfergus Road",
            "Carrickfergus",
            "BT38 7AW",
            "Northern Ireland",
            "1",
            "Bro",
            "162",
            "NOR"
        ];

        $this->companyIncorporationFactory->createByShares(Argument::any(), Argument::any(), Argument::any(), Argument::any(), Argument::any(), Argument::any(), [], FALSE, FALSE)->willReturn($companyIncorporation);

        $this->buildIncorporation($customer, $rows)->shouldReturn($companyIncorporation);
    }
}
