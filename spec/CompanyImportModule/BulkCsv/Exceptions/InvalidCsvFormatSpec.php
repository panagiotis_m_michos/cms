<?php

namespace spec\CompanyImportModule\BulkCsv\Exceptions;

use Address;
use CompanyImportModule\BulkCsv\Exceptions\InvalidCsvFormat;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use RuntimeException;

/**
 * @mixin InvalidCsvFormat
 */
class InvalidCsvFormatSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(RuntimeException::class);
    }

    function it_can_be_constructed_for_invalid_header()
    {
        $this->beConstructedThrough('invalidHeader');
        $this->getMessage()->shouldReturn('Invalid header!');
    }

    function it_can_be_constructed_for_wrong_number_of_column()
    {
        $this->beConstructedThrough('invalidColumnCount', [5, 6]);
        $this->getMessage()->shouldReturn('Invalid number of columns! Expected 5 got 6.');
    }

    function it_can_be_constructed_for_invalid_ro_address()
    {
        $roCountries = implode(', ', Address::$registeredOfficeCountries);

        $this->beConstructedThrough('invalidRegisteredOfficeCountry', ['Country']);
        $this->getMessage()->shouldReturn("Invalid registered office address! Expected one of: $roCountries, got Country");
    }
}
