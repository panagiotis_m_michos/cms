<?php

namespace spec\OfferModule\Deciders;

use Doctrine\Common\Cache\CacheProvider;
use OfferModule\Deciders\OffersPositionDecider;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin OffersPositionDecider
 */
class OffersPositionDeciderSpec extends ObjectBehavior
{
    /**
     * @var array
     */
    private $originalOffersOrder = ['simplyBusiness', 'taxAssist', 'worldpay'];

    /**
     * @var array
     */
    private $originalLoansOrder = ['businessCentre', 'virginStartUp'];

    /**
     * @var CacheProvider
     */
    private $cache;

    function let(CacheProvider $cache)
    {
        $this->cache = $cache;
        $this->beConstructedWith($this->originalOffersOrder, $this->originalLoansOrder, $this->cache);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('OfferModule\Deciders\OffersPositionDecider');
    }

    function it_can_get_offers_position_and_rotate_it()
    {
        $key = 'offersOrder';
        $rotatedOrder = ['worldpay', 'simplyBusiness', 'taxAssist'];

        $this->cache->contains($key)->willReturn(TRUE);
        $this->cache->fetch($key)->willReturn($this->originalOffersOrder);
        $this->cache->save($key, $rotatedOrder)->shouldBeCalled();

        $this->getOffersPosition()->shouldReturn($rotatedOrder);
    }

    function it_will_set_default_offers_order_if_its_not_cached()
    {
        $key = 'offersOrder';
        array_unshift($this->originalOffersOrder, array_pop($this->originalOffersOrder));

        $this->cache->contains($key)->willReturn(FALSE);
        $this->cache->save($key, $this->originalOffersOrder)->shouldBeCalled();

        $this->getOffersPosition()->shouldReturn($this->originalOffersOrder);
    }

    function it_can_get_loans_position_and_rotate_it()
    {
        $key = 'loansOrder';
        $rotatedOrder = ['virginStartUp', 'businessCentre'];

        $this->cache->contains($key)->willReturn(TRUE);
        $this->cache->fetch($key)->willReturn($this->originalLoansOrder);
        $this->cache->save($key, $rotatedOrder)->shouldBeCalled();

        $this->getLoansPosition()->shouldReturn($rotatedOrder);
    }

    function it_will_set_default_loans_order_if_its_not_cached()
    {
        $key = 'loansOrder';
        array_unshift($this->originalLoansOrder, array_pop($this->originalLoansOrder));

        $this->cache->contains($key)->willReturn(FALSE);
        $this->cache->save($key, $this->originalLoansOrder)->shouldBeCalled();

        $this->getLoansPosition()->shouldReturn($this->originalLoansOrder);
    }
}
