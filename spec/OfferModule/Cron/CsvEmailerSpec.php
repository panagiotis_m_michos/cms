<?php

namespace spec\OfferModule\Cron;

use Cron\INotifier;
use Doctrine\ORM\Internal\Hydration\IterableResult;
use Gedmo\Mapping\ExtensionODMTest;
use ILogger;
use JourneyEmailer;
use JourneyProduct;
use OfferModule\Entities\CustomerDetails;
use OfferModule\OfferService;
use OfferModule\Repositories\OfferRepository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Tests\Helpers\ObjectHelper;
use tests\helpers\IteratorHelper;
use Utils\Date;

class CsvEmailerSpec extends ObjectBehavior
{

    /**
     * @var JourneyEmailer
     */
    private $emailer;

    /**
     * @var OfferService
     */
    private $offerService;

    function let(ILogger $logger, INotifier $notifier, OfferService $offerService, JourneyEmailer $journeyEmailer)
    {
        $this->beConstructedWith($logger, $notifier, $offerService, $journeyEmailer);
        $this->emailer = $journeyEmailer;
        $this->offerService = $offerService;
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('OfferModule\Cron\CsvEmailer');
    }

    function it_should_process_product()
    {
        $date = new Date();
        $product1 = new JourneyProduct();
        $product1->sendCsvDaily = TRUE;
        $customerDetails[] = CustomerDetails::fromProduct(
            ObjectHelper::createCustomer('bury@madesimplegroup.com'),
            $product1
        );
        $customerDetails[] = CustomerDetails::fromProduct(
            ObjectHelper::createCustomer('muzy@madesimplegroup.com'),
            $product1,
            'GM Bees'
        );
        $content = <<<EOD
firstName,lastName,email,phone,postcode,companyName
Johny,Tester,bury@madesimplegroup.com,,EC1V 4PQ,
Johny,Tester,muzy@madesimplegroup.com,,EC1V 4PQ,GM Bees
EOD;
        $this->offerService->getCustomerDetailsForDate($product1, $date)->willReturn(IteratorHelper::createDoctrine($customerDetails));
        $this->emailer->midnightCsvEmail($product1, $content)->shouldBeCalled();
        $this->process($date, $product1);
    }

    function it_executes_only_required_csv_emails(IterableResult $iterableResult)
    {
        $product1 = new JourneyProduct();
        $product1->sendCsvDaily = TRUE;
        $product2 = new JourneyProduct();
        $product2->sendCsvDaily = TRUE;
        $product3 = new JourneyProduct();
        $product3->sendCsvDaily = FALSE;
        $this->offerService->getAvailableProducts()->willReturn([$product1, $product2, $product3]);
        $this->offerService->getCustomerDetailsForDate(Argument::any(), Argument::any())->willReturn($iterableResult);
        $this->execute()->shouldReturn(2);
    }
}
