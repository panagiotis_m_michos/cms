<?php

namespace spec\OfferModule;

use OfferModule\Entities\CustomerDetails;
use OfferModule\OfferEvent;
use OfferModule\Repositories\OfferRepository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Tests\Helpers\ObjectHelper;
use Tests\ProductHelper;

class OfferServiceSpec extends ObjectBehavior
{
    /**
     * @var OfferRepository
     */
    private $repository;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    function let(OfferRepository $repository, EventDispatcherInterface $dispatcher)
    {
        $this->beConstructedWith($repository, $dispatcher);
        $this->repository = $repository;
        $this->dispatcher = $dispatcher;
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('OfferModule\OfferService');
    }

    function it_should_store_new_product()
    {
        $product = ProductHelper::createJourney(CustomerDetails::PRODUCT_TAX_ASSIST);
        $customer = ObjectHelper::createCustomer();
        $offer = CustomerDetails::fromProduct($customer, $product, 'my company name');
        $this->repository->saveEntity($offer)->shouldBeCalled();
        $this->dispatcher->dispatch(
            OfferEvent::PRODUCT_ADDED . CustomerDetails::PRODUCT_TAX_ASSIST,
            new OfferEvent($offer)
        )->shouldBeCalled();
        $this->dispatcher->dispatch(OfferEvent::CUSTOMER_DETAILS_ADDED, new OfferEvent($offer))->shouldBeCalled();
        $this->saveNewCustomer($offer);
    }

    function it_should_produce_available_products()
    {
        $this->getAvailableProducts()->shouldBeArray();
    }
}
