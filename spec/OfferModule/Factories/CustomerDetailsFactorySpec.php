<?php

namespace spec\OfferModule\Factories;

use Entities\Customer;
use OfferModule\Entities\OffersFormData;
use OfferModule\Factories\CustomerDetailsFactory;
use OfferModule\Forms\OffersForm;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Utils\Date;

/**
 * @mixin CustomerDetailsFactory
 */
class CustomerDetailsFactorySpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('OfferModule\Factories\CustomerDetailsFactory');
    }

    function it_can_create_customer_details_from_offers_form_data()
    {
        $customer = new Customer('email', 'pass');
        $formData = new OffersFormData($customer, 'companyName');

        $formData->setCompanyName('companyName');
        $formData->setEmail('email');
        $formData->setFirstName('firstName');
        $formData->setInsuranceReturnMonth(new Date());

        $formData->setOffers([OffersForm::OFFER_DIGITAL_RISKS, OffersForm::OFFER_TAX_ASSIST, OffersForm::OFFER_WORLDPAY]);
        $formData->setLoans([OffersForm::LOAN_BUSINESS_CENTRE, OffersForm::LOAN_VIRGIN_START_UP]);

        $details = $this->buildFromFormData($formData);
        $details->shouldHaveCount(5);
        $details[0]->shouldBeAnInstanceOf('OfferModule\Entities\CustomerDetails');
    }
}
