<?php

namespace spec\OfferModule\Mappers;

use JourneyProduct;
use OfferModule\Entities\CustomerDetails;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Tests\Helpers\ObjectHelper;

class CustomerDetailsMapperSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('OfferModule\Mappers\CustomerDetailsMapper');
        $this->shouldHaveType('CommonModule\Mappers\IMapper');
    }

    function it_should_map_object_to_array()
    {
        $customer = ObjectHelper::createCustomer();
        $customerDetails = CustomerDetails::fromProduct($customer, new JourneyProduct(CustomerDetails::PRODUCT_SMARTA), 'test');
        $this->toArray($customerDetails)->shouldBe([
            'firstName' => $customer->getFirstName(),
            'lastName' => $customer->getLastName(),
            'email' => $customer->getEmail(),
            'phone' => $customer->getPhone(),
            'postcode' => $customer->getPostcode(),
            'companyName' => 'test',
        ]);
    }
}
