<?php

namespace spec\OfferModule\Mappers;

use JourneyProduct;
use OfferModule\Entities\CustomerDetails;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Tests\Helpers\ObjectHelper;

class InsuranceMapperSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('OfferModule\Mappers\InsuranceMapper');
        $this->shouldHaveType('CommonModule\Mappers\IMapper');
    }

    function it_should_map_object_to_array()
    {
        $customer = ObjectHelper::createCustomer();
        $customerDetails = CustomerDetails::fromProduct($customer, new JourneyProduct(CustomerDetails::PRODUCT_SIMPLY_BUSINESS));
        $this->toArray($customerDetails)->shouldBe([
            'First Name' => $customer->getFirstName(),
            'Last Name' => $customer->getLastName(),
            'Telephone' => $customer->getPhone(),
            'Email address' => $customer->getEmail(),
            'Company name' => $customer->getCompanyName(),
            'Switch month' => 'NULL'
        ]);
    }
}
