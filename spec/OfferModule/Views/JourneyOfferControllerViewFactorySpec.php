<?php

namespace spec\OfferModule\Views;

use Entities\Customer;
use OfferModule\Deciders\OffersPositionDecider;
use OfferModule\OfferService;
use OfferModule\Views\JourneyOfferControllerView;
use OfferModule\Views\JourneyOfferControllerViewFactory;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Services\CompanyService;
use Services\PackageService;

/**
 * @mixin JourneyOfferControllerViewFactory
 */
class JourneyOfferControllerViewFactorySpec extends ObjectBehavior
{
    /**
     * @var OffersPositionDecider
     */
    private $offersPositionDecider;

    /**
     * @var OfferService
     */
    private $offerService;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var PackageService
     */
    private $packageService;

    function let(
        OffersPositionDecider $offersPositionDecider,
        OfferService $offerService,
        CompanyService $companyService,
        PackageService $packageService
    )
    {
        $this->offersPositionDecider = $offersPositionDecider;
        $this->offerService = $offerService;
        $this->companyService = $companyService;
        $this->packageService = $packageService;

        $this->beConstructedWith(
            $this->offersPositionDecider,
            $this->offerService,
            $this->companyService,
            $this->packageService
        );
    }

    function it_can_create_controller_view(Customer $customer)
    {
        $this->create($customer, 'company name')->shouldBeAnInstanceOf(JourneyOfferControllerView::class);
    }
}
