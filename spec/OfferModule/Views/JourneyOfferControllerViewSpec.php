<?php

namespace spec\OfferModule\Views;

use Entities\Company;
use Entities\Customer;
use OfferModule\Deciders\OffersPositionDecider;
use OfferModule\Entities\CustomerDetails;
use OfferModule\OfferService;
use OfferModule\Views\JourneyOfferControllerView;
use Package;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Services\CompanyService;
use Services\PackageService;

/**
 * @mixin JourneyOfferControllerView
 */
class JourneyOfferControllerViewSpec extends ObjectBehavior
{
    /**
     * @var OffersPositionDecider
     */
    private $offersPositionDecider;

    /**
     * @var OfferService
     */
    private $offerService;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var PackageService
     */
    private $packageService;

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var string
     */
    private $companyName;

    function let(
        OffersPositionDecider $offersPositionDecider,
        OfferService $offerService,
        CompanyService $companyService,
        PackageService $packageService,
        Customer $customer
    )
    {
        $this->offersPositionDecider = $offersPositionDecider;
        $this->offerService = $offerService;
        $this->companyService = $companyService;
        $this->packageService = $packageService;
        $this->customer = $customer;
        $this->companyName = 'company name';

        $this->beConstructedWith($this->offersPositionDecider,
            $this->offerService,
            $this->companyService,
            $this->packageService,
            $this->customer,
            $this->companyName
        );
    }

    function it_can_decide_to_show_offers_for_retail_and_non_international_package(Company $company, Package $package)
    {
        $this->customer->isWholesale()->willReturn(FALSE);
        $this->companyService->getCompanyByCompanyName($this->companyName)->willReturn($company);
        $company->getProductId()->willReturn(1);
        $this->packageService->getPackageById(1)->willReturn($package);
        $package->isInternational()->willReturn(FALSE);

        $this->canShowOffers()->shouldBe(TRUE);
    }

    function it_can_decide_to_show_offers_for_retail_and_no_company_name()
    {
        $this->customer->isWholesale()->willReturn(FALSE);
        $this->companyService->getCompanyByCompanyName($this->companyName)->willReturn(NULL);

        $this->canShowOffers()->shouldBe(TRUE);
    }

    function it_can_decide_to_not_show_offers_for_wholesalers()
    {
        $this->customer->isWholesale()->willReturn(TRUE);

        $this->canShowOffers()->shouldBe(FALSE);
    }

    function it_can_decide_to_not_show_offers_for_international_packages(Company $company, Package $package)
    {
        $this->customer->isWholesale()->willReturn(FALSE);
        $this->companyService->getCompanyByCompanyName($this->companyName)->willReturn($company);
        $company->getProductId()->willReturn(1);
        $this->packageService->getPackageById(1)->willReturn($package);
        $package->isInternational()->willReturn(TRUE);

        $this->canShowOffers()->shouldBe(FALSE);
    }

    function it_can_get_offers_order()
    {
        $offers = [1, 2, 3];
        
        $this->offersPositionDecider->getOffersPosition()->willReturn($offers);

        $this->getOffersOrder()->shouldBe($offers);
    }

    function it_can_get_loans_order()
    {
        $loans = [1, 2, 3];

        $this->offersPositionDecider->getLoansPosition()->willReturn($loans);

        $this->getLoansOrder()->shouldBe($loans);
    }

    function it_can_get_tax_assist_popup_content()
    {
        $product = (object) ['page' => 'asdf'];

        $this->offerService->getJourneyProduct(CustomerDetails::PRODUCT_TAX_ASSIST)->willReturn($product);

        $this->getTaxAssistPopupContent()->shouldBe($product->page);
    }

    function it_can_get_digital_risks_popup_content()
    {
        $product = (object) ['page' => 'asdf'];

        $this->offerService->getJourneyProduct(CustomerDetails::PRODUCT_DIGITAL_RISKS)->willReturn($product);

        $this->getDigitalRisksPopupContent()->shouldBe($product->page);
    }

    function it_can_get_worldpay_popup_content()
    {
        $product = (object) ['page' => 'asdf'];

        $this->offerService->getJourneyProduct(CustomerDetails::PRODUCT_WORLDPAY)->willReturn($product);

        $this->getWorldpayPopupContent()->shouldBe($product->page);
    }

    function it_can_get_business_centre_popup_content()
    {
        $product = (object) ['page' => 'asdf'];

        $this->offerService->getJourneyProduct(CustomerDetails::PRODUCT_BUSINESS_CENTRE)->willReturn($product);

        $this->getBusinessCentrePopupContent()->shouldBe($product->page);
    }

    function it_can_get_virgin_start_up_popup_content()
    {
        $product = (object) ['page' => 'asdf'];

        $this->offerService->getJourneyProduct(CustomerDetails::PRODUCT_VIRGIN_START_UP)->willReturn($product);

        $this->getVirginStartUpPopupContent()->shouldBe($product->page);
    }
}
