<?php

namespace spec\OfferModule\Listeners;

use JourneyEmailer;
use OfferModule\Entities\CustomerDetails;
use OfferModule\OfferEvent;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Tests\Helpers\ObjectHelper;
use Tests\ProductHelper;

class EmailListenerSpec extends ObjectBehavior
{
    /**
     * @var JourneyEmailer
     */
    private $emailer;

    function let(JourneyEmailer $emailer)
    {
        $this->beConstructedWith($emailer);
        $this->emailer = $emailer;
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('OfferModule\Listeners\EmailListener');
    }

    function it_should_send_emails_to_customer_and_third_parties()
    {
        $product = ProductHelper::createJourney(CustomerDetails::PRODUCT_TAX_ASSIST);
        $product->sendEmailInstantly = TRUE;
        $product->emailText = 'email content';
        $product->emails = ['tomasj@madesimplegroup.com'];
        $customer = ObjectHelper::createCustomer();
        $offer = CustomerDetails::fromProduct($customer, $product, 'my company name');
        $this->emailer->sendEmailToThirdParties($offer)->shouldBeCalled();
        $this->emailer->sendEmailToCustomer($offer)->shouldBeCalled();
        $this->handleOffer(new OfferEvent($offer));
    }

    function it_should_not_send_emails()
    {
        $product = ProductHelper::createJourney(CustomerDetails::PRODUCT_TAX_ASSIST);
        $product->sendEmailInstantly = FALSE;
        $product->emailText = '     ';
        $customer = ObjectHelper::createCustomer();
        $offer = CustomerDetails::fromProduct($customer, $product, 'my company name');
        $this->emailer->sendEmailToThirdParties($offer)->shouldNotBeCalled();
        $this->emailer->sendEmailToCustomer($offer)->shouldNotBeCalled();
        $this->handleOffer(new OfferEvent($offer));
    }
}
