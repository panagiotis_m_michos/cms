<?php

namespace spec\OfferModule\Listeners;

use OfferModule\Entities\CustomerDetails;
use OfferModule\OfferEvent;
use PhpSpec\ObjectBehavior;
use TaxAssistLeadModel;

class TaxAssistListenerSpec extends ObjectBehavior
{
    function let(TaxAssistLeadModel $taxAssistLeadModel)
    {
        $this->beConstructedWith($taxAssistLeadModel);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('OfferModule\Listeners\TaxAssistListener');
    }

    function it_should_handle_tax_assist_offers(CustomerDetails $customerDetails, OfferEvent $offerEvent)
    {
        $offerEvent->getCustomerDetails()->willReturn($customerDetails);
        $this->handleOffer($offerEvent);
    }
}
