<?php

namespace spec\PackageModule\Controllers;

use PackageModule\Controllers\FivePoundsFormationPackageController;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Services\ControllerHelper;
use Services\SessionService;
use TemplateModule\Renderers\IRenderer;

/**
 * @mixin FivePoundsFormationPackageController
 */
class FivePoundsFormationPackageControllerSpec extends ObjectBehavior
{
    function let(IRenderer $renderer, ControllerHelper $controllerHelper, SessionService $sessionService)
    {
        $this->beConstructedWith($renderer, $controllerHelper, $sessionService);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(FivePoundsFormationPackageController::class);
    }
}
