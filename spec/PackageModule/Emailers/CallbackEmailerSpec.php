<?php

namespace spec\PackageModule\Emailers;

use EmailService;
use FEmail;
use FEmailFactory;
use PackageModule\Emailers\CallbackEmailer;
use PackageModule\Forms\CallbackData;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ValueObject\Country;

/**
 * @mixin CallbackEmailer
 */
class CallbackEmailerSpec extends ObjectBehavior
{
    /**
     * @var EmailService
     */
    private $emailService;

    /**
     * @var FEmailFactory
     */
    private $emailFactory;

    function let(EmailService $emailService, FEmailFactory $emailFactory)
    {
        $this->emailService = $emailService;
        $this->emailFactory = $emailFactory;
        $this->beConstructedWith($this->emailService, $this->emailFactory);
    }

    function it_can_send_email(FEmail $email)
    {
        $this->emailFactory->getById(CallbackEmailer::EMAIL_CALLBACK)->willReturn($email);

        $uk = Country::fromIso('GB');
        $callbackData = new CallbackData();
        $callbackData->setCountry($uk);
        $callbackData->setFirstName('name');
        $callbackData->setLastName('last');
        $callbackData->setMessage('msg');
        $callbackData->setPhoneNumber('1');
        $callbackData->setSubject('International Package');

        $placeholderData = [
            '[COUNTRY]' => $uk->getName(),
            '[NAME]' => 'name last',
            '[PHONE]' => '1',
            '[MESSAGE]' => 'msg',
            '[SUBJECT]' => 'International Package',
        ];
        $email->replaceAllPlaceHolders($placeholderData)->shouldBeCalled();

        $this->emailService->send($email)->shouldBeCalled();

        $this->send($callbackData);
    }
}
