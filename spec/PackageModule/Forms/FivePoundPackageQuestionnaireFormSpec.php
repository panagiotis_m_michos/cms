<?php

namespace spec\PackageModule\Forms;

use PackageModule\Forms\FivePoundPackageQuestionnaireForm;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\Form\AbstractType;

/**
 * @mixin FivePoundPackageQuestionnaireForm
 */
class FivePoundPackageQuestionnaireFormSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(FivePoundPackageQuestionnaireForm::class);
        $this->shouldHaveType(AbstractType::class);
    }
}
