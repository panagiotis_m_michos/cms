<?php

namespace spec\PackageModule\Forms;

use PackageModule\Forms\FivePoundPackageQuestionnaireData;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class FivePoundPackageQuestionnaireDataSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(FivePoundPackageQuestionnaireData::class);
    }

    function it_can_set_and_check_if_this_is_customers_first_formation()
    {
        $this->isFirstFormation()->shouldReturn(NULL);
        $this->setIsFirstFormation(TRUE);
        $this->isFirstFormation()->shouldReturn(TRUE);
    }

    function it_can_set_and_check_if_customer_is_uk_resident()
    {
        $this->isUkResident()->shouldReturn(NULL);
        $this->setIsUkResident(TRUE);
        $this->isUkResident()->shouldReturn(TRUE);
    }

    function it_can_set_and_check_if_company_is_formed_on_behalf()
    {
        $this->isFormedOnBehalf()->shouldReturn(NULL);
        $this->setIsFormedOnBehalf(FALSE);
        $this->isFormedOnBehalf()->shouldReturn(FALSE);
    }
}
