<?php

namespace spec\PackageModule\Forms;

use PackageModule\Forms\CallbackForm;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin CallbackForm
 */
class CallbackFormSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Symfony\Component\Form\AbstractType');
    }
}
