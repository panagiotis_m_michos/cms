<?php

namespace spec\PackageModule\Forms;

use PackageModule\Forms\CallbackData;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ValueObject\Country;

/**
 * @mixin CallbackData
 */
class CallbackDataSpec extends ObjectBehavior
{
    function it_can_set_and_get_first_name()
    {
        $this->getFirstName()->shouldReturn(NULL);
        $this->setFirstName('name')->shouldReturn(NULL);
        $this->getFirstName()->shouldReturn('name');
    }

    function it_can_set_and_get_last_name()
    {
        $this->getLastName()->shouldReturn(NULL);
        $this->setLastName('name')->shouldReturn(NULL);
        $this->getLastName()->shouldReturn('name');
    }

    function it_can_set_and_get_phone_number()
    {
        $this->getPhoneNumber()->shouldReturn(NULL);
        $this->setPhoneNumber('123')->shouldReturn(NULL);
        $this->getPhoneNumber()->shouldReturn('123');
    }

    function it_can_set_and_get_country(Country $country)
    {
        $this->getCountry()->shouldReturn(NULL);
        $this->setCountry($country)->shouldReturn(NULL);
        $this->getCountry()->shouldReturn($country);
    }

    function it_can_get_and_set_message()
    {
        $this->getMessage()->shouldReturn(NULL);
        $this->setMessage('msg')->shouldReturn(NULL);
        $this->getMessage()->shouldReturn('msg');
    }
}
