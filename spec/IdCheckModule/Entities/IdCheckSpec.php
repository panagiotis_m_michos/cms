<?php

namespace spec\IdCheckModule\Entities;

use Entities\Customer;
use IdCheckModule\DataObjects\IdResponse;
use PhpSpec\ObjectBehavior;
use prophecy\argument;

class IdCheckSpec extends ObjectBehavior
{

    function let(Customer $customer, IdResponse $response)
    {
        $this->beConstructedThrough('fromResponse', array($customer, $response));
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('IdCheckModule\Entities\IdCheck');
    }
}
