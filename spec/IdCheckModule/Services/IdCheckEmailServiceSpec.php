<?php

namespace spec\IdCheckModule\Services;

use Entities\Customer;
use IdCheckModule\IdCheckEmailer;
use IdCheckModule\Services\IdCheckEmailService;
use IdCheckModule\Services\IdCheckService;
use LoggableModule\Repositories\EmailLogRepository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Tests\Helpers\ObjectHelper;
use Utils\Date;

class IdCheckEmailServiceSpec extends ObjectBehavior
{
    /**
     * @var IdCheckService
     */
    private $idCheckService;

    /**
     * @var IdCheckEmailer
     */
    private $idCheckEmailer;

    /**
     * @var EmailLogRepository
     */
    private $emailLogRepository;

    function let(IdCheckService $idCheckService, IdCheckEmailer $idCheckEmailer, EmailLogRepository $emailLogRepository)
    {
        $this->beConstructedWith($idCheckService, $idCheckEmailer, $emailLogRepository);
        $this->idCheckService = $idCheckService;
        $this->idCheckEmailer = $idCheckEmailer;
        $this->emailLogRepository = $emailLogRepository;
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('IdCheckModule\Services\IdCheckEmailService');
    }

    function it_should_send_skip_uk_validation_email(Customer $customer)
    {
        $this->idCheckService->hasAvailableIdChecks($customer)->willReturn(TRUE);
        $this->idCheckEmailer->sendSkipValidationEmail($customer, IdCheckEmailer::REMINDER_UK);
        $this->sendSkipValidation($customer, IdCheckEmailService::TYPE_UK);
    }

    function it_should_send_skip_uk_3_validation_email(Customer $customer)
    {
        $this->idCheckService->hasAvailableIdChecks($customer)->willReturn(FALSE);
        $this->idCheckEmailer->sendSkipValidationEmail($customer, IdCheckEmailer::REMINDER_UK_3);
        $this->sendSkipValidation($customer, IdCheckEmailService::TYPE_UK);
    }

    function it_should_send_skip_non_uk_validation_email(Customer $customer)
    {
        $this->idCheckEmailer->sendSkipValidationEmail($customer, IdCheckEmailer::REMINDER_ABROAD);
        $this->sendSkipValidation($customer, 'non');
    }

    function it_should_send_recurring_reminder_to_uk()
    {
        $customer = ObjectHelper::createCustomer();
        $this->emailLogRepository->getEmailsSent($customer, [IdCheckEmailer::REMINDER_RECURRING_UK])->willReturn(2);
        $this->emailLogRepository->hasEmailsSentAfter($customer, [IdCheckEmailer::REMINDER_RECURRING_UK], new Date('-7 days'))->willReturn(FALSE);
        $this->idCheckEmailer->sendUkReminder($customer)->shouldBeCalled();
        $this->sendRecurringReminderEmail($customer);
    }

    function it_should_send_recurring_reminder_to_non_uk()
    {
        $customer = ObjectHelper::createCustomer();
        $customer->setCountryId(23);
        $this->emailLogRepository->getEmailsSent($customer, [IdCheckEmailer::REMINDER_RECURRING_ABROAD])->willReturn(2);
        $this->emailLogRepository->hasEmailsSentAfter($customer, [IdCheckEmailer::REMINDER_RECURRING_ABROAD], new Date('-7 days'))->willReturn(FALSE);
        $this->idCheckEmailer->sendAbroadReminder($customer)->shouldBeCalled();
        $this->sendRecurringReminderEmail($customer);
    }

    function it_should_not_send_recurring_reminder_if_3_are_sent()
    {
        $customer = ObjectHelper::createCustomer();
        $this->emailLogRepository->getEmailsSent($customer, [IdCheckEmailer::REMINDER_RECURRING_UK])->willReturn(3);
        $this->idCheckEmailer->sendAbroadReminder($customer)->shouldNotBeCalled();
        $this->sendRecurringReminderEmail($customer);
    }

    function it_should_not_send_recurring_reminder_if_email_was_sent_in_less_than_7_days()
    {
        $customer = ObjectHelper::createCustomer();
        $this->emailLogRepository->getEmailsSent($customer, [IdCheckEmailer::REMINDER_RECURRING_UK])->willReturn(2);
        $this->emailLogRepository->hasEmailsSentAfter($customer, [IdCheckEmailer::REMINDER_RECURRING_UK], new Date('-7 days'))->willReturn(TRUE);
        $this->idCheckEmailer->sendAbroadReminder($customer)->shouldNotBeCalled();
        $this->sendRecurringReminderEmail($customer);
    }
}
