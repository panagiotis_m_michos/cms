<?php

namespace spec\IdCheckModule\Services;

use DateTime;
use Entities\Customer;
use FormModule\Country;
use Id3globalApiClient\Interfaces\IId3GlobalApiClient;
use Id3globalApiClient\Interfaces\IPersonalDetails;
use IdCheckModule\DataObjects\Address;
use IdCheckModule\DataObjects\IdRequest;
use IdCheckModule\DataObjects\Personal;
use IdCheckModule\Entities\IdCheck;
use IdCheckModule\Repositories\IdCheckRepository;
use IdCheckModule\Services\Validation;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Tests\Helpers\ObjectHelper;

class IdCheckServiceSpec extends ObjectBehavior
{
    private $client;

    private $repository;

    function let(IdCheckRepository $repository, IId3GlobalApiClient $client)
    {
        $this->client = $client;
        $this->repository = $repository;
        $this->beConstructedWith($repository, $client);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('IdCheckModule\Services\IdCheckService');
    }

    function it_returns_last_id_check(IdCheckRepository $repository, Customer $customer, IdCheck $idCheck)
    {
        $repository->getLastIdCheck($customer)->willReturn($idCheck);
        $this->getLastIdCheck($customer)->shouldReturn($idCheck);
    }

    function it_marks_id_check_for_customer_as_completed(Customer $customer)
    {
        $customer->markIdCheckNotRequired()->shouldBeCalled();
        $customer->setDtIdValidated(new DateTime('now'))->shouldBeCalled();
        $this->completeIdCheckForCustomer($customer);
    }

    function it_removes_id_check_requirement(Customer $customer)
    {
        $customer->markIdCheckNotRequired()->shouldBeCalled();
        $this->removeIdCheckRequirement($customer);
    }

    function it_checks_if_there_are_available_id_checks(IdCheckRepository $repository, Customer $customer)
    {
        $repository->countIdChecks($customer)->willReturn(2);
        $this->hasAvailableIdChecks($customer)->shouldReturn(TRUE);
    }

    function it_complete_id_check_with_request()
    {
        $customer1 = ObjectHelper::createCustomer();
        $customer2 = ObjectHelper::createCustomer();
        $this->repository->saveEntity($customer1)->willReturn($customer1);
        $customer2->setFirstName('a');
        $customer2->setLastName('b');
        $customer2->setAddress1('c');
        $customer2->setAddress2('d');
        $customer2->setCity('e');
        $customer2->setPostcode('f');

        $idRequest = IdRequest::fromCustomer($customer1, Country::NAME_UNITED_KINGDOM);
        $personal = new Personal();
        $personal->setFirstName('a');
        $personal->setLastName('b');
        $address = new Address();
        $address->setAddress1('c');
        $address->setAddress2('d');
        $address->setCity('e');
        $address->setPostcode('f');
        $address->setCountry(Country::NAME_UNITED_KINGDOM);
        $idRequest->setPersonal($personal);
        $idRequest->setAddress($address);
        $this->completeIdCheckWithRequest($customer1, $idRequest)->shouldBeSameCustomer($customer2);
    }

    public function getMatchers()
    {
        return [
            'beSameCustomer' => function (Customer $subject, Customer $customer) {
                return $subject->getFirstName() === $customer->getFirstName()
                    && $subject->getLastName() === $customer->getLastName()
                    && $subject->getAddress1() === $customer->getAddress1()
                    && $subject->getAddress2() === $customer->getAddress2()
                    && $subject->getAddress3() === $customer->getAddress3()
                    && $subject->getPostcode() === $customer->getPostcode()
                    && $subject->getCity() === $customer->getCity()
                    && $subject->getCountry() === $customer->getCountry();
            }
        ];
    }


}
