<?php

namespace spec\IdCheckModule;

use Cron\INotifier;
use FEmail;
use IdCheckModule\IdCheckEmailer;
use IdCheckModule\Services\IdCheckEmailService;
use ILogger;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Log\LoggerInterface;
use Repositories\CustomerRepository;
use tests\helpers\IteratorHelper;
use Tests\Helpers\ObjectHelper;
use Utils\Date;

class ReminderCronSpec extends ObjectBehavior
{
    /**
     * @var CustomerRepository
     */
    private $customerRepository;

    /**
     * @var IdCheckEmailService
     */
    private $idCheckEmailService;

    /**
     * @var INotifier
     */
    private $notifier;

    function let(IdCheckEmailService $idCheckEmailService, CustomerRepository $customerRepository, INotifier $notifier, LoggerInterface $logger)
    {
        $this->beConstructedWith($idCheckEmailService, $customerRepository, $notifier, $logger);
        $this->idCheckEmailService = $idCheckEmailService;
        $this->customerRepository = $customerRepository;
        $this->notifier = $notifier;
        $customers[] = ObjectHelper::createCustomer('test1@madesimplegroup.com');
        $customers[] = ObjectHelper::createCustomer('test2@madesimplegroup.com');
        $customers[] = ObjectHelper::createCustomer('test3@madesimplegroup.com');
        $customers[] = ObjectHelper::createCustomer('test4@madesimplegroup.com');
        $this->customerRepository->getCustomersWithRequiredIdCheck()->willReturn($customers);
        $idCheckEmailService->sendRecurringReminderEmail($customers[0])->willReturn(new FEmail(IdCheckEmailer::REMINDER_RECURRING_ABROAD));
        $idCheckEmailService->sendRecurringReminderEmail($customers[1])->willReturn(new FEmail(IdCheckEmailer::REMINDER_RECURRING_UK));
        $idCheckEmailService->sendRecurringReminderEmail($customers[2])->willReturn(new FEmail(IdCheckEmailer::REMINDER_RECURRING_UK));
        $idCheckEmailService->sendRecurringReminderEmail($customers[3])->willReturn(FALSE);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('IdCheckModule\ReminderCron');
    }

    function it_should_execute_id_check_email_reminders()
    {
        $date = new Date();
        $this->notifier->triggerSuccess('idcheckmodule.remindercron', $date->getTimestamp(), Argument::containingString('UK: 2, Abroad: 1'))->shouldBeCalled();
        $this->execute();
    }
}
