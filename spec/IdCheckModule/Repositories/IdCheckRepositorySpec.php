<?php

namespace spec\IdCheckModule\Repositories;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use IdCheckModule\Entities\IdCheck;
use IdCheckModule\Services\IdResponse;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Entities\Customer;

class IdCheckRepositorySpec extends ObjectBehavior
{
    function let(EntityManager $entityManager, ClassMetadata $metadata)
    {
        $this->beConstructedWith($entityManager, $metadata);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('IdCheckModule\Repositories\IdCheckRepository');
    }
}
