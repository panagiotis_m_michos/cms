<?php

namespace spec\IdCheckModule\Controllers;

use Entities\Customer;
use IdCheckModule\Controllers\IdCheckController;
use IdCheckModule\Services\IdCheckEmailService;
use IdCheckModule\Services\IdCheckService;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Log\LoggerInterface;
use Services\ControllerHelper;
use TemplateModule\Renderers\IRenderer;

class IdCheckControllerSpec extends ObjectBehavior
{
    function let(
        IRenderer $renderer,
        ControllerHelper $controllerHelper,
        IdCheckService $idCheckService,
        IdCheckEmailService $idCheckEmailer,
        Customer $customer,
        LoggerInterface $logger
    )
    {
        $this->beConstructedWith($renderer, $controllerHelper, $idCheckService, $idCheckEmailer, $customer, $logger);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(IdCheckController::class);
    }
}
