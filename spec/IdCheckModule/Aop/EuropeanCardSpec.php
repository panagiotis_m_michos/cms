<?php

namespace spec\IdCheckModule\DataObjects;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class EuropeanCardSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('IdCheckModule\DataObjects\EuropeanCard');
    }
}
