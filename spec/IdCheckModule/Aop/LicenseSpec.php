<?php

namespace spec\IdCheckModule\DataObjects;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class LicenseSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('IdCheckModule\DataObjects\License');
    }
}
