<?php

namespace spec\IdCheckModule\DataObjects;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class PersonalSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('IdCheckModule\DataObjects\Personal');
    }
}
