<?php

namespace spec\IdCheckModule;

use IEmailService;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\Routing\RouterInterface;

class IdCheckEmailerSpec extends ObjectBehavior
{
    function let(IEmailService $emailService, RouterInterface $router)
    {
        $this->beConstructedWith($emailService, $router);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('IdCheckModule\IdCheckEmailer');
    }
}
