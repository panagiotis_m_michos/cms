<?php

namespace spec\CustomerModule\QueryObjects;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class CustomerQuerySpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('CustomerModule\QueryObjects\CustomerQuery');
    }
}
