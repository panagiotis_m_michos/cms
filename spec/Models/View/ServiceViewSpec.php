<?php

namespace spec\Models\View;

use DateTime;
use Entities\Service;
use Entities\ServiceSettings;
use Models\View\ServiceView;
use PhpSpec\ObjectBehavior;
use Product;
use Prophecy\Argument;
use Repositories\ServiceSettingsRepository;

/**
 * @mixin ServiceView
 */
class ServiceViewSpec extends ObjectBehavior
{
    /**
     * @var Service
     */
    private $service1;

    /**
     * @var Service
     */
    private $service2;

    /**
     * @var ServiceSettingsRepository
     */
    private $serviceSettingsRepository;

    function let(Service $service1, Service $service2, ServiceSettingsRepository $serviceSettingsRepository)
    {
        $this->serviceSettingsRepository = $serviceSettingsRepository;
        $this->service1 = $service1;
        $this->service2 = $service2;
        $this->beConstructedWith([$this->service1, $this->service2], $this->serviceSettingsRepository);

        $this->service1->getDtExpires()->willReturn(new DateTime('2015-05-05'));
        $this->service2->getDtExpires()->willReturn(new DateTime('2016-05-05'));
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('Models\View\ServiceView');
    }

    function it_can_check_if_auto_renewal_is_enabled(ServiceSettings $settings)
    {
        $this->serviceSettingsRepository->getSettingsByService($this->service2)->willReturn($settings);

        $settings->isAutoRenewalEnabled()->willReturn(TRUE);
        $this->isAutoRenewalEnabled()->shouldBe(TRUE);

        $settings->isAutoRenewalEnabled()->willReturn(FALSE);
        $this->isAutoRenewalEnabled()->shouldBe(FALSE);
    }

    function it_returns_false_for_auto_renewal_enabled_with_no_services()
    {
        $this->beConstructedWith([], $this->serviceSettingsRepository);
        $this->isAutoRenewalEnabled()->shouldBe(FALSE);
    }

    function it_can_check_if_service_has_email_reminders_enabled(ServiceSettings $settings)
    {
        $this->serviceSettingsRepository->getSettingsByService($this->service2)->willReturn($settings);

        $settings->hasEmailReminders()->willReturn(TRUE);
        $this->hasEmailReminders()->shouldBe(TRUE);

        $settings->hasEmailReminders()->willReturn(FALSE);
        $this->hasEmailReminders()->shouldBe(FALSE);
    }

    function it_returns_false_for_has_email_reminders_with_no_services()
    {
        $this->beConstructedWith([], $this->serviceSettingsRepository);
        $this->hasEmailReminders()->shouldBe(FALSE);
    }

    function it_returns_true_for_can_receive_reminders_when_service_can_receive(ServiceSettings $settings)
    {
        $this->serviceSettingsRepository->getSettingsByService($this->service2)->willReturn($settings);

        $this->service2->isOneOff()->willReturn(FALSE);
        $settings->hasEmailReminders()->willReturn(FALSE);
        $settings->isAutoRenewalEnabled()->willReturn(FALSE);

        $this->canReceiveReminders()->shouldBe(FALSE);
    }

    function it_returns_false_for_can_receive_reminders_when_service_can_not_receive(ServiceSettings $settings)
    {
        $this->serviceSettingsRepository->getSettingsByService($this->service2)->willReturn($settings);

        $this->service2->isOneOff()->willReturn(TRUE);
        $settings->hasEmailReminders()->willReturn(TRUE);
        $settings->isAutoRenewalEnabled()->willReturn(FALSE);

        $this->canReceiveReminders()->shouldBe(FALSE);

        $this->service2->isOneOff()->willReturn(TRUE);
        $settings->hasEmailReminders()->willReturn(TRUE);
        $settings->isAutoRenewalEnabled()->willReturn(TRUE);

        $this->canReceiveReminders()->shouldBe(FALSE);

        $this->service2->isOneOff()->willReturn(FALSE);
        $settings->hasEmailReminders()->willReturn(FALSE);
        $settings->isAutoRenewalEnabled()->willReturn(FALSE);

        $this->canReceiveReminders()->shouldBe(FALSE);

        $this->service2->isOneOff()->willReturn(FALSE);
        $settings->hasEmailReminders()->willReturn(TRUE);
        $settings->isAutoRenewalEnabled()->willReturn(TRUE);

        $this->canReceiveReminders()->shouldBe(FALSE);
    }

    function it_returns_false_for_can_receive_reminders_with_no_services()
    {
        $this->beConstructedWith([], $this->serviceSettingsRepository);
        $this->canReceiveReminders()->shouldBe(FALSE);
    }

    function it_can_check_if_auto_renewal_is_allowed(Product $product)
    {
        $this->service2->getProduct()->willReturn($product);

        $product->isAutoRenewalAllowed = TRUE;
        $this->isAutoRenewalAllowed()->shouldBe(TRUE);

        $product->isAutoRenewalAllowed = FALSE;
        $this->isAutoRenewalAllowed()->shouldBe(FALSE);
    }

    function it_returns_false_for_is_auto_renewal_allowerd_with_no_services()
    {
        $this->beConstructedWith([], $this->serviceSettingsRepository);
        $this->isAutoRenewalAllowed()->shouldBe(FALSE);
    }
    
    function it_can_decide_if_can_toggle_auto_renewal()
    {
        $this->service1->isOneOff()->willReturn(FALSE);
        $this->service1->getDtStart()->willReturn(new DateTime('-2 years'));
        $this->service1->getDtExpires()->willReturn(new DateTime('-1 year'));

        $this->service2->isOneOff()->willReturn(FALSE);
        $this->service2->isEnabled()->willReturn(TRUE);
        $this->service2->isActive()->willReturn(TRUE);
        $this->service2->getDtStart()->willReturn(new DateTime('-1 year +1 day'));
        $this->service2->getDtExpires()->willReturn(new DateTime('-27 days'));

        $this->canToggleAutoRenewal()->shouldBe(TRUE);

        $this->service2->getDtExpires()->willReturn(new DateTime('-28 days'));

        $this->canToggleAutoRenewal()->shouldBe(FALSE);

        $this->service2->getDtExpires()->willReturn(new DateTime('-29 days'));

        $this->canToggleAutoRenewal()->shouldBe(FALSE);
    }
}
