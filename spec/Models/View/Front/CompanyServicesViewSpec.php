<?php

namespace spec\Models\View\Front;

use DateTime;
use Entities\Company;
use Entities\Service;
use EventLocator;
use Factories\ServiceViewFactory;
use Models\View\Front\CompanyServicesView;
use Models\View\ServiceView;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;  
use Services\EventService;

/**
 * @mixin CompanyServicesView
 */
class CompanyServicesViewSpec extends ObjectBehavior
{

    /**
     * @var Company
     */
    private $company;

    /**
     * @var ServiceViewFactory
     */
    private $serviceViewFactory;

    /**
     * @var EventService
     */
    private $eventService;

    function let(Company $company, ServiceViewFactory $serviceViewFactory, EventService $eventService)
    {
        $this->company = $company;
        $this->serviceViewFactory = $serviceViewFactory;
        $this->eventService = $eventService;

        $this->beConstructedWith($company, $serviceViewFactory, $eventService);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(CompanyServicesView::class);
    }

    function it_can_check_if_company_has_services_without_failed_auto_renewal_expiring_in_x_days(
        Service $service,
        ServiceView $serviceView
    )
    {
        $service->getServiceTypeId()->willReturn(1);
        $serviceView->getId()->willReturn(1);
        $serviceView->canReceiveReminders()->willReturn(TRUE);
        $this->company->getEnabledActivatedParentServices()->willReturn([$service]);
        $this->serviceViewFactory->create([$service])->willReturn($serviceView);
        $this->eventService->eventOccurred(EventLocator::SERVICE_EXPIRED, 1)->willReturn(FALSE);
        $this->eventService->eventOccurred(EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_FAILED, 1)->willReturn(FALSE);

        $serviceView->getDtExpires()->willReturn(new DateTime('+1 day'));
        $this->hasCompanyServicesWithoutFailedAutoRenewalExpiringIn(1)->shouldReturn(TRUE);

        $serviceView->getDtExpires()->willReturn(new DateTime('-1 day'));
        $this->hasCompanyServicesWithoutFailedAutoRenewalExpiringIn(1)->shouldReturn(FALSE);
    }

    function it_can_get_company_services_without_failed_auto_renewal_expiring_in_x_days(
        Service $service,
        ServiceView $serviceView
    )
    {
        $service->getServiceTypeId()->willReturn(1);
        $serviceView->getId()->willReturn(1);
        $serviceView->canReceiveReminders()->willReturn(TRUE);
        $this->company->getEnabledActivatedParentServices()->willReturn([$service]);
        $this->serviceViewFactory->create([$service])->willReturn($serviceView);
        $this->eventService->eventOccurred(EventLocator::SERVICE_EXPIRED, 1)->willReturn(FALSE);
        $this->eventService->eventOccurred(EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_FAILED, 1)->willReturn(FALSE);

        $serviceView->getDtExpires()->willReturn(new DateTime('+1 day'));
        $this->getCompanyServicesWithoutFailedAutoRenewalExpiringIn(1)->shouldReturn([$serviceView]);

        $serviceView->getDtExpires()->willReturn(new DateTime('-1 day'));
        $this->getCompanyServicesWithoutFailedAutoRenewalExpiringIn(1)->shouldReturn([]);
    }
}
