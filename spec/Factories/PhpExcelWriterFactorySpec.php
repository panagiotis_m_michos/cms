<?php

namespace spec\Factories;

use PHPExcel;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class PhpExcelWriterFactorySpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('Excel2007');
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('Factories\PhpExcelWriterFactory');
    }

    function it_should_create_writer(PHPExcel $excel)
    {
        $this->create($excel)->shouldBeAnInstanceOf('PHPExcel_Writer_IWriter');
    }
}
