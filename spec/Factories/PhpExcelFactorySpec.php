<?php

namespace spec\Factories;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class PhpExcelFactorySpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Factories\PhpExcelFactory');
    }

    function it_should_create_phpexcel()
    {
        $this->create()->shouldBeAnInstanceOf('PHPExcel');
    }
}
