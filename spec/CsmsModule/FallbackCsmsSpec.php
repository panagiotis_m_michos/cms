<?php

namespace spec\CsmsModule;

use CsmsModule\FallbackCsms;
use CsmsModule\ICsms;
use CsmsModule\IncorporationsStats;
use DateTime;
use Doctrine\Common\Cache\Cache;
use Exception;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin FallbackCsms
 */
class FallbackCsmsSpec extends ObjectBehavior
{
    /**
     * @var ICsms
     */
    private $csms;

    /**
     * @var Cache
     */
    private $cache;

    function let(ICsms $csms, Cache $cache)
    {
        $this->csms = $csms;
        $this->cache = $cache;
        $this->beConstructedWith($this->csms, $this->cache);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(ICsms::class);
    }

    function it_can_get_incorporation_stats(IncorporationsStats $stats)
    {
        $this->csms->getIncorporationStats()->willReturn($stats);
        $this->cache->save('csms.incorporation_stats.fallback', $stats)->shouldBeCalled();
        $this->getIncorporationStats()->shouldReturn($stats);
    }

    function it_returns_cached_version_on_exception(IncorporationsStats $stats)
    {
        $this->csms->getIncorporationStats()->willThrow(new Exception);
        $this->cache->contains('csms.incorporation_stats.fallback')->willReturn(TRUE);
        $this->cache->fetch('csms.incorporation_stats.fallback')->willReturn($stats);
        $this->getIncorporationStats()->shouldReturn($stats);
    }

    function it_returns_hardcoded_value_if_cache_is_empty()
    {
        $this->csms->getIncorporationStats()->willThrow(new Exception);
        $this->cache->contains('csms.incorporation_stats.fallback')->willReturn(FALSE);
        $this->getIncorporationStats()->shouldBeLike(
            new IncorporationsStats(
                2680,
                new Datetime(),
                50399,
                98603
            )
        );
    }
}
