<?php

namespace spec\CsmsModule;

use CsmsModule\Csms;
use CsmsModule\ICsms;
use CsmsModule\IncorporationsStats;
use DateTime;
use GuzzleHttp\Client;
use GuzzleHttp\Message\ResponseInterface;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin Csms
 */
class CsmsSpec extends ObjectBehavior
{
    /**
     * @var Client
     */
    private $client;

    function let(Client $client)
    {
        $this->client = $client;
        $this->beConstructedWith($this->client, 'apikey');
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(ICsms::class);
    }

    function it_can_get_incorporation_stats(ResponseInterface $response)
    {
        $this->client->get(Argument::any())->willReturn($response);
        $response->json()->willReturn([
            'results' => [
                'daily' => 1,
                'dailyDate' => '1/1/2015',
                'thisMonthToNow' => 2,
                'thisYearToNow' => 3
            ]
        ]);

        $stats = $this->getIncorporationStats();
        $stats->shouldHaveType(IncorporationsStats::class);
        $stats->getDailyIncorporationCount()->shouldReturn(1);
        $stats->getIncorporationCountThisMonthToNow()->shouldReturn(2);
        $stats->getIncorporationCountThisYearToNow()->shouldReturn(3);
        $stats->getDailyMeasuredDate()->shouldBeLike(new DateTime('2015-01-01 00:00:00'));
    }
}
