<?php

namespace spec\FormSubmissionModule\Services;

use FormSubmissionModule\Services\DocumentUploader;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin DocumentUploader
 */
class DocumentUploaderSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(DocumentUploader::class);
    }
}
