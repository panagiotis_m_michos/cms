<?php

namespace spec\ValidationModule;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use ValidationModule\Validator;

/**
 * @mixin Validator
 */
class ValidatorSpec extends ObjectBehavior
{
    /**
     * @var Constraint
     */
    private $constraint;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    function let(ValidatorInterface $validator, Constraint $constraint)
    {
        $this->constraint = $constraint;
        $this->validator = $validator;

        $this->beConstructedWith($this->validator, [$this->constraint]);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('ValidationModule\IValidator');
    }

    function it_forwards_violations(ConstraintViolationInterface $violation)
    {
        $violation->getMessage()->willReturn('asd');
        $this->validator->validate('value', [$this->constraint])->willReturn([$violation]);
        $this->validate('value')->shouldReturn(['asd']);
    }
}
