<?php

namespace spec\VoServiceModule\Services;

use Entities\Company;
use Entities\Customer;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use VoServiceModule\Entities\VoServiceQueue;
use VoServiceModule\Repositories\VoServiceQueueRepository;
use VoServiceModule\Services\VoServiceQueueService;

/**
 * @mixin VoServiceQueueService
 */
class VoServiceQueueServiceSpec extends ObjectBehavior
{
    /**
     * @var VoServiceQueueRepository
     */
    private $repository;

    function let(VoServiceQueueRepository $repository)
    {
        $this->repository = $repository;
        $this->beConstructedWith($this->repository);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('VoServiceModule\Services\VoServiceQueueService');
    }

    function it_can_decide_that_item_can_be_send(VoServiceQueue $queueItem, Customer $customer, Company $company)
    {
        $queueItem->getCustomer()->willReturn($customer);
        $queueItem->getCompany()->willReturn($company);

        $customer->hasCompletedRegistration()->willReturn(TRUE);
        $company->isIncorporated()->willReturn(TRUE);
        
        $this->canBeSend($queueItem)->shouldReturn(TRUE);

        $customer->hasCompletedRegistration()->willReturn(FALSE);
        $company->isIncorporated()->willReturn(TRUE);

        $this->canBeSend($queueItem)->shouldReturn(FALSE);

        $customer->hasCompletedRegistration()->willReturn(TRUE);
        $company->isIncorporated()->willReturn(FALSE);

        $this->canBeSend($queueItem)->shouldReturn(FALSE);
    }
}
