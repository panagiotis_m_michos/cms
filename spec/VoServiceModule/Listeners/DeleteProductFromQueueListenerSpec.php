<?php

namespace spec\VoServiceModule\Listeners;

use Dispatcher\Events\CompanyDeletedEvent;
use Dispatcher\Events\Order\RefundEvent;
use Entities\Company;
use Entities\OrderItem;
use EventLocator;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use VoServiceModule\Facades\VoServiceFacade;
use VoServiceModule\Listeners\DeleteProductFromQueueListener;

/**
 * @mixin DeleteProductFromQueueListener
 */
class DeleteProductFromQueueListenerSpec extends ObjectBehavior
{
    /**
     * @var VoServiceFacade
     */
    private $voServiceFacade;

    function let(VoServiceFacade $voServiceFacade)
    {
        $this->voServiceFacade = $voServiceFacade;
        $this->beConstructedWith($this->voServiceFacade);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('VoServiceModule\Listeners\DeleteProductFromQueueListener');
        $this->shouldImplement('Symfony\Component\EventDispatcher\EventSubscriberInterface');
    }

    function it_can_get_subscribed_events()
    {
        $this::getSubscribedEvents()->shouldReturn(
            [
                EventLocator::COMPANY_DELETED => 'onCompanyDeleted',
                EventLocator::ORDER_REFUNDED => 'onOrderRefunded',
            ]
        );
    }

    function it_deletes_items_by_company_on_company_deleted(CompanyDeletedEvent $event, Company $company)
    {
        $event->getCompany()->willReturn($company);

        $this->voServiceFacade->deleteByCompany($company)->shouldBeCalled();

        $this->onCompanyDeleted($event);
    }

    function it_deletes_order_items_when_refunded(RefundEvent $event, OrderItem $item1, OrderItem $item2)
    {
        $event->getOrderItems()->willReturn([$item1, $item2]);

        $this->voServiceFacade->deleteByOrderItem($item1)->shouldBeCalled();
        $this->voServiceFacade->deleteByOrderItem($item2)->shouldBeCalled();

        $this->onOrderRefunded($event);
    }
}
