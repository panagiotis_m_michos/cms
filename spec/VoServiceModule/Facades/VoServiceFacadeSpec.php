<?php

namespace spec\VoServiceModule\Facades;

use Entities\Company;
use Entities\OrderItem;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Services\CompanyService;
use Services\NodeService;
use VoServiceModule\Entities\VoServiceQueue;
use VoServiceModule\Facades\VoServiceFacade;
use VoServiceModule\Services\VoServiceQueueService;

/**
 * @mixin VoServiceFacade
 */
class VoServiceFacadeSpec extends ObjectBehavior
{
    /**
     * @var VoServiceQueueService
     */
    private $voServiceQueueService;

    /**
     * @var NodeService
     */
    private $nodeService;

    /**
     * @var CompanyService
     */
    private $companyService;

    function let(
        VoServiceQueueService $voServiceQueueService,
        NodeService $nodeService,
        CompanyService $companyService
    )
    {
        $this->voServiceQueueService = $voServiceQueueService;
        $this->nodeService = $nodeService;
        $this->companyService = $companyService;
        $this->beConstructedWith($this->voServiceQueueService, $this->nodeService, $this->companyService);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('VoServiceModule\Facades\VoServiceFacade');
    }

    function it_can_delete_item_by_company(Company $company, VoServiceQueue $item1, VoServiceQueue $item2)
    {
        $this->voServiceQueueService->getServicesByCompany($company)->willReturn([$item1, $item2]);

        $this->voServiceQueueService->remove($item1)->shouldBeCalled();
        $this->voServiceQueueService->remove($item2)->shouldBeCalled();

        $this->deleteByCompany($company);
    }

    function it_can_delete_item_by_order_item(OrderItem $orderItem, VoServiceQueue $item)
    {
        $this->voServiceQueueService->getServiceByOrderItem($orderItem)->willReturn(NULL);
        $this->voServiceQueueService->remove()->shouldNotBeCalled();

        $this->deleteByOrderItem($orderItem);

        $this->voServiceQueueService->getServiceByOrderItem($orderItem)->willReturn($item);
        $this->voServiceQueueService->remove($item)->shouldBeCalled();

        $this->deleteByOrderItem($orderItem);
    }
}
