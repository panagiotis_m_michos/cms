<?php

namespace spec\CsvParserModule\Exceptions;

use CsvParserModule\Exceptions\IncorrectStructureException;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin IncorrectStructureException
 */
class IncorrectStructureExceptionSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Exceptions\Technical\TechnicalAbstract');
        $this->shouldHaveType('CsvParserModule\Exceptions\IncorrectStructureException');
    }
}
