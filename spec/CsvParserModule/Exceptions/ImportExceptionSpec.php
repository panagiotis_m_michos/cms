<?php

namespace spec\CsvParserModule\Exceptions;

use CsvParserModule\Exceptions\ImportException;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin ImportException
 */
class ImportExceptionSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith(['error 1', 'error 2']);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('Exceptions\Technical\TechnicalAbstract');
        $this->shouldHaveType('CsvParserModule\Exceptions\ImportException');
    }
    
    function it_can_get_errors()
    {
        $this->getErrors()->shouldBe(['error 1', 'error 2']);
    }
}
