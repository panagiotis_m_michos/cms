<?php

namespace spec\CsvParserModule;

use CsvParserModule\CsvParser;
use Goodby\CSV\Import\Standard\Interpreter;
use Goodby\CSV\Import\Standard\Lexer;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Utils\File;

/**
 * @mixin CsvParser
 */
class CsvParserSpec extends ObjectBehavior
{
    /**
     * @var string
     */
    private $filePath;

    /**
     * @var Lexer
     */
    private $lexer;

    /**
     * @var Interpreter
     */
    private $interpreter;

    function let()
    {
        $this->filePath = tempnam(sys_get_temp_dir(), 'csv_export_');

        $this->lexer = new Lexer();
        $this->interpreter = new Interpreter();
        $this->beConstructedWith($this->lexer, $this->interpreter);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('CsvParserModule\CsvParser');
    }

    function it_can_parse_correctly()
    {
        file_put_contents($this->filePath, "companyNumber\n123\n456");

        $this->parse(new File($this->filePath))->shouldReturn([['companyNumber'], ['123'], ['456']]);
    }

    function it_can_parse_and_skip_header()
    {
        file_put_contents($this->filePath, "companyNumber\n123\n456");

        $this->setSkipHeader(TRUE);
        $this->parse(new File($this->filePath))->shouldReturn([['123'], ['456']]);
    }
}
