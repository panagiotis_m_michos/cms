<?php

namespace spec\CsvParserModule\Observers;

use CsvParserModule\Exceptions\IncorrectStructureException;
use CsvParserModule\Observers\ColumnCountObserver;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin ColumnCountObserver
 */
class ColumnCountObserverSpec extends ObjectBehavior
{
    /**
     * @var int
     */
    private $columnCount = 2;

    function let()
    {
        $this->beConstructedWith($this->columnCount);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('CsvParserModule\Observers\ColumnCountObserver');
        $this->shouldHaveType('CsvParserModule\Observers\IObserver');
    }

    function it_can_throw_when_header_is_not_valid()
    {
        $rowColumns = ['123', '456', '789'];
        $this->shouldThrow(IncorrectStructureException::wrongColumnCount($this->columnCount, count($rowColumns)))->during('observe', [$rowColumns]);
    }
}
