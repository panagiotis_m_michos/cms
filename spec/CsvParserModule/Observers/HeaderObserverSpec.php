<?php

namespace spec\CsvParserModule\Observers;

use CsvParserModule\Exceptions\IncorrectStructureException;
use CsvParserModule\Observers\HeaderObserver;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin HeaderObserver
 */
class HeaderObserverSpec extends ObjectBehavior
{
    /**
     * @var array
     */
    private $columns = ['companyNumber'];

    function let()
    {
        $this->beConstructedWith($this->columns);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('CsvParserModule\Observers\HeaderObserver');
        $this->shouldHaveType('CsvParserModule\Observers\IObserver');
    }

    function it_throw_when_header_is_not_valid()
    {
        $rowColumns = ['company'];
        $this->shouldThrow(IncorrectStructureException::invalidHeader($this->columns, $rowColumns))->during('observe', [$rowColumns]);
    }
}
