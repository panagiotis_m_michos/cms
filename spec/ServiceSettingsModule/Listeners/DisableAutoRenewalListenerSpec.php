<?php

namespace spec\ServiceSettingsModule\Listeners;

use Dispatcher\Events\CompanyTransferredEvent;
use Dispatcher\Events\Order\RefundEvent;
use Entities\Company;
use Entities\OrderItem;
use Entities\Service;
use Entities\ServiceSettings;
use EventLocator;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Services\ServiceService;
use ServiceSettingsModule\Facades\ServiceSettingsFacade;
use ServiceSettingsModule\Listeners\DisableAutoRenewalListener;
use ServiceSettingsModule\Services\ServiceSettingsService;

/**
 * @mixin DisableAutoRenewalListener
 */
class DisableAutoRenewalListenerSpec extends ObjectBehavior
{
    /**
     * @var ServiceService
     */
    private $serviceService;

    /**
     * @var ServiceSettingsService
     */
    private $settingsService;

    /**
     * @var ServiceSettingsFacade
     */
    private $settingsFacade;

    function let(
        ServiceService $serviceService,
        ServiceSettingsService $settingsService,
        ServiceSettingsFacade $settingsFacade
    )
    {
        $this->serviceService = $serviceService;
        $this->settingsService = $settingsService;
        $this->settingsFacade = $settingsFacade;
        $this->beConstructedWith($this->serviceService, $this->settingsService, $this->settingsFacade);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('ServiceSettingsModule\Listeners\DisableAutoRenewalListener');
        $this->shouldHaveType('Symfony\Component\EventDispatcher\EventSubscriberInterface');
    }

    function it_can_get_subscribed_events()
    {
        $this::getSubscribedEvents()->shouldReturn(
            [
                EventLocator::COMPANY_TRANSFERRED => 'onCompanyTransferred',
                EventLocator::ORDER_REFUNDED => ['onOrderRefunded', 25],
            ]
        );
    }

    function it_can_disable_auto_renewal_on_company_transferred(CompanyTransferredEvent $event, Company $company)
    {
        $event->getCompany()->willReturn($company);

        $this->settingsService->disableAutoRenewalByCompany($company)->shouldBeCalled();

        $this->onCompanyTransferred($event);
    }

    function it_can_disable_auto_renewal_on_order_refund(RefundEvent $event, OrderItem $orderItem, Service $service, ServiceSettings $settings)
    {
        $event->getOrderItems()->willReturn([$orderItem]);
        $this->serviceService->getOrderItemService($orderItem)->willReturn($service);
        $this->settingsService->getSettingsByService($service)->willReturn($settings);

        $this->settingsFacade->disableAutoRenewal($settings)->shouldBeCalled();

        $this->onOrderRefunded($event);
    }
}
