<?php

namespace spec\ServiceSettingsModule\Services;

use Doctrine\ORM\Internal\Hydration\IterableResult;
use Entities\Company;
use Entities\Customer;
use Entities\Order;
use Entities\Payment\Token;
use Entities\Service;
use Entities\ServiceSettings;
use Factories\ServiceViewFactory;
use Models\View\ServiceView;
use PhpSpec\ObjectBehavior;
use Product;
use Prophecy\Argument;
use Repositories\ServiceSettingsRepository;
use Services\ServiceService;
use ServiceSettingsModule\Facades\ServiceSettingsFacade;
use ServiceSettingsModule\Services\ServiceSettingsService;

/**
 * @mixin ServiceSettingsService
 */
class ServiceSettingsServiceSpec extends ObjectBehavior
{
    /**
     * @var ServiceSettingsRepository
     */
    private $repository;

    /**
     * @var ServiceService
     */
    private $serviceService;

    /**
     * @var ServiceSettingsFacade
     */
    private $serviceSettingsFacade;

    /**
     * @var ServiceViewFactory
     */
    private $serviceViewFactory;

    function let(
        ServiceSettingsRepository $repository,
        ServiceService $serviceService,
        ServiceSettingsFacade $serviceSettingsFacade,
        ServiceViewFactory $serviceViewFactory
    )
    {
        $this->repository = $repository;
        $this->serviceService = $serviceService;
        $this->serviceSettingsFacade = $serviceSettingsFacade;
        $this->serviceViewFactory = $serviceViewFactory;

        $this->beConstructedWith(
            $this->repository,
            $this->serviceService,
            $this->serviceSettingsFacade,
            $this->serviceViewFactory
        );
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('ServiceSettingsModule\Services\ServiceSettingsService');
    }

    function it_can_save(ServiceSettings $entity)
    {
        $this->save($entity)->shouldReturn(NULL);
        $this->repository->saveEntity($entity)->shouldBeCalled();
    }

    function it_can_flush(ServiceSettings $entity)
    {
        $this->flush($entity)->shouldReturn(NULL);
        $this->repository->flush($entity)->shouldBeCalled();
    }

    function it_can_get_settings_by_type(Company $company, ServiceSettings $settings)
    {
        $serviceTypeId = 1;
        $this->repository->getSettingsByType($company, $serviceTypeId)->willReturn($settings);
        $this->getSettingsByType($company, $serviceTypeId)->shouldReturn($settings);
    }

    function it_can_get_settings_by_service(Service $service, ServiceSettings $settings)
    {
        $this->repository->getSettingsByService($service)->willReturn($settings);
        $this->getSettingsByService($service)->shouldReturn($settings);
    }

    function it_can_get_settings_by_token(Token $token, ServiceSettings $settings)
    {
        $this->repository->getSettingsByToken($token)->willReturn($settings);
        $this->getSettingsByToken($token)->shouldReturn($settings);
    }

    function it_can_get_settings_with_enabled_auto_renewal(IterableResult $result)
    {
        $this->repository->getSettingsWithEnabledAutoRenewal()->willReturn($result);
        $this->getSettingsWithEnabledAutoRenewal()->shouldReturn($result);
    }

    function it_can_create_settings_while_getting_them_by_type(Company $company)
    {
        $serviceTypeId = 1;
        $this->repository->getSettingsByType($company, $serviceTypeId)->willReturn(NULL);
        $this->repository->saveEntity(Argument::type('Entities\ServiceSettings'))->shouldBeCalled();

        $settings = $this->getOrCreateSettingsByType($company, $serviceTypeId);
        $settings->shouldBeAnInstanceOf('Entities\ServiceSettings');
        $settings->getCompany()->shouldReturn($company);
        $settings->getServiceTypeId()->shouldReturn($serviceTypeId);
    }

    function it_will_not_create_settings_while_getting_them_by_type_if_it_already_exists(
        Company $company,
        ServiceSettings $settings
    )
    {
        $serviceTypeId = 1;
        $this->repository->getSettingsByType($company, $serviceTypeId)->willReturn($settings);
        $this->getOrCreateSettingsByType($company, $serviceTypeId)->shouldReturn($settings);
    }

    function it_can_enable_autorenewal_by_order(
        Order $order,
        Token $token,
        Service $service,
        ServiceSettings $settings,
        Product $product
    )
    {
        $order->getRenewableServices()->willReturn([$service]);
        $this->repository->getSettingsByService($service)->willReturn($settings);

        $service->isRenewable()->willReturn(TRUE);
        $service->getProduct()->willReturn($product);
        $product->isAutoRenewalAllowed = TRUE;

        $this->enableAutoRenewalByOrder($order, $token);

        $this->serviceSettingsFacade->enableAutoRenewal($settings, $token)->shouldBeCalled();
    }

    function it_will_not_enable_autorenewal_by_order_if_service_is_not_renewable_or_autorenewal_is_disabled(
        Order $order,
        Token $token,
        Service $service,
        ServiceSettings $settings,
        Product $product
    )
    {
        $order->getRenewableServices()->willReturn([$service]);
        $this->repository->getSettingsByService($service)->willReturn($settings);
        $service->getProduct()->willReturn($product);

        $service->isRenewable()->willReturn(TRUE);
        $product->isAutoRenewalAllowed = FALSE;
        $this->enableAutoRenewalByOrder($order, $token);
        $this->serviceSettingsFacade->enableAutoRenewal($settings, $token)->shouldNotBeCalled();

        $service->isRenewable()->willReturn(FALSE);
        $product->isAutoRenewalAllowed = TRUE;
        $this->enableAutoRenewalByOrder($order, $token);
        $this->serviceSettingsFacade->enableAutoRenewal($settings, $token)->shouldNotBeCalled();

        $service->isRenewable()->willReturn(FALSE);
        $product->isAutoRenewalAllowed = FALSE;
        $this->enableAutoRenewalByOrder($order, $token);
        $this->serviceSettingsFacade->enableAutoRenewal($settings, $token)->shouldNotBeCalled();
    }

    function it_can_disable_auto_renewal_by_order(
        Order $order,
        Service $service,
        ServiceSettings $settings,
        Product $product
    )
    {
        $order->getRenewableServices()->willReturn([$service]);
        $this->repository->getSettingsByService($service)->willReturn($settings);
        $service->getProduct()->willReturn($product);

        $this->disableAutoRenewalByOrder($order);

        $this->serviceSettingsFacade->disableAutoRenewal($settings)->shouldBeCalled();
    }

    function it_can_suspend_auto_renewal_by_token(
        Token $token,
        ServiceSettings $settings
    )
    {
        $this->repository->getSettingsByToken($token)->willReturn([$settings]);
        $this->suspendAutoRenewalByToken($token);

        $this->serviceSettingsFacade->suspendAutoRenewal($settings)->shouldBeCalled();
    }

    function it_can_get_settings_that_need_to_have_their_auto_renewal_reenabled_by_customer(
        Customer $customer,
        ServiceSettings $settings,
        Company $company,
        Service $service,
        ServiceView $serviceView
    )
    {
        $settings->getCompany()->willReturn($company);
        $settings->getServiceTypeId()->willReturn(1);

        $serviceSettingsData = [[$settings]];
        $this->repository->getSettingsWithInvalidRenewalTokenByCustomer($customer)->willReturn($serviceSettingsData);

        $services = [$service];
        $this->serviceService->getCompanyServicesByType($company, 1)->willReturn($services);
        $this->serviceViewFactory->create($services)->willReturn($serviceView);

        $serviceView->canToggleAutoRenewal()->willReturn(TRUE);
        $this->getSettingsToReenableAutoRenewalByCustomer($customer)->shouldReturn([$settings]);

        $serviceView->canToggleAutoRenewal()->willReturn(FALSE);
        $this->getSettingsToReenableAutoRenewalByCustomer($customer)->shouldReturn([]);
    }

    function it_can_reenable_auto_renewal_by_customer(
        Customer $customer,
        ServiceSettings $settings,
        Company $company,
        Service $service,
        ServiceView $serviceView,
        Token $token
    )
    {
        $settings->getCompany()->willReturn($company);
        $settings->getServiceTypeId()->willReturn(1);

        $serviceSettingsData = [[$settings]];
        $this->repository->getSettingsWithInvalidRenewalTokenByCustomer($customer)->willReturn($serviceSettingsData);

        $services = [$service];
        $this->serviceService->getCompanyServicesByType($company, 1)->willReturn($services);

        $this->serviceViewFactory->create($services)->willReturn($serviceView);
        $serviceView->canToggleAutoRenewal()->willReturn(TRUE);
        $this->serviceSettingsFacade->enableAutoRenewal($settings, $token)->shouldBeCalled();

        $this->reenableAutoRenewalByCustomer($customer, $token);
    }

    function it_can_check_if_customer_has_services_with_invalid_renewal_token(
        Customer $customer,
        ServiceSettings $settings,
        Service $service,
        Company $company,
        ServiceView $serviceView
    )
    {
        $serviceTypeId = 1;
        $settings->getServiceTypeId()->willReturn($serviceTypeId);
        $settings->getCompany()->willReturn($company);

        $serviceSettingsData = [[$settings]];
        $this->repository->getSettingsWithInvalidRenewalTokenByCustomer($customer)->willReturn($serviceSettingsData);

        $services = [$service];
        $this->serviceService->getCompanyServicesByType($company, $serviceTypeId)->willReturn($services);

        $this->serviceViewFactory->create($services)->willReturn($serviceView);

        $serviceView->canToggleAutoRenewal()->willReturn(TRUE);
        $this->hasCustomerServicesWithInvalidRenewalToken($customer)->shouldReturn(TRUE);

        $serviceView->canToggleAutoRenewal()->willReturn(FALSE);
        $this->hasCustomerServicesWithInvalidRenewalToken($customer)->shouldReturn(FALSE);
    }

    function it_can_disable_autorenewal_by_company(
        Company $company,
        Service $service,
        ServiceSettings $settings
    )
    {
        $company->getGroupedServices()->willReturn([$service]);
        $this->repository->getSettingsByService($service)->willReturn($settings);

        $this->serviceSettingsFacade->disableAutoRenewal($settings)->shouldBeCalled();

        $this->disableAutoRenewalByCompany($company);
    }

}
