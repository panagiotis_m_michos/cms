<?php

namespace spec\ServiceSettingsModule\Togglers;

use Entities\Payment\Token;
use Entities\ServiceSettings;
use ServiceSettingsModule\Togglers\AutoRenewalToggler;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Repositories\ServiceSettingsRepository;

/**
 * @mixin AutoRenewalToggler
 */
class AutoRenewalTogglerSpec extends ObjectBehavior
{
    /**
     * @var ServiceSettingsRepository
     */
    private $serviceSettingsRepository;

    function let(ServiceSettingsRepository $serviceSettingsRepository)
    {
        $this->beConstructedWith($serviceSettingsRepository);
        $this->serviceSettingsRepository = $serviceSettingsRepository;
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('ServiceSettingsModule\Togglers\AutoRenewalToggler');
    }

    function it_can_enable(ServiceSettings $serviceSettings, Token $token)
    {
        $serviceSettings->setIsAutoRenewalEnabled(TRUE)->shouldBeCalled();
        $serviceSettings->setRenewalToken($token)->shouldBeCalled();
        $this->serviceSettingsRepository->flush($serviceSettings)->shouldBeCalled();

        $this->enable($serviceSettings, $token);
    }

    function it_can_disable(ServiceSettings $serviceSettings)
    {
        $serviceSettings->setIsAutoRenewalEnabled(FALSE)->shouldBeCalled();
        $serviceSettings->setRenewalToken(NULL)->shouldBeCalled();
        $this->serviceSettingsRepository->flush($serviceSettings)->shouldBeCalled();

        $this->disable($serviceSettings);
    }

    function it_can_suspend(ServiceSettings $serviceSettings)
    {
        $serviceSettings->setIsAutoRenewalEnabled(FALSE)->shouldBeCalled();
        $this->serviceSettingsRepository->flush($serviceSettings)->shouldBeCalled();

        $this->suspend($serviceSettings);
    }
}
