<?php

namespace spec\ServiceSettingsModule\Togglers;

use Entities\Payment\Token;
use Entities\ServiceSettings;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Repositories\ServiceSettingsRepository;
use ServiceSettingsModule\Togglers\EmailRemindersToggler;

/**
 * @mixin EmailRemindersToggler
 */
class EmailRemindersTogglerSpec extends ObjectBehavior
{
    /**
     * @var ServiceSettingsRepository
     */
    private $serviceSettingsRepository;

    function let(ServiceSettingsRepository $serviceSettingsRepository)
    {
        $this->beConstructedWith($serviceSettingsRepository);
        $this->serviceSettingsRepository = $serviceSettingsRepository;
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('ServiceSettingsModule\Togglers\EmailRemindersToggler');
    }

    function it_can_enable(ServiceSettings $serviceSettings, Token $token)
    {
        $serviceSettings->enableEmailReminders()->shouldBeCalled();
        $this->serviceSettingsRepository->flush($serviceSettings)->shouldBeCalled();

        $this->enable($serviceSettings);
    }

    function it_can_disable(ServiceSettings $serviceSettings)
    {
        $serviceSettings->enableEmailReminders(FALSE)->shouldBeCalled();
        $this->serviceSettingsRepository->flush($serviceSettings)->shouldBeCalled();

        $this->disable($serviceSettings);
    }
}
