<?php

namespace spec\ServiceSettingsModule\Facades;

use Entities\Payment\Token;
use Entities\ServiceSettings;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ServiceSettingsModule\Facades\ServiceSettingsFacade;
use ServiceSettingsModule\Togglers\AutoRenewalToggler;
use ServiceSettingsModule\Togglers\EmailRemindersToggler;

/**
 * @mixin ServiceSettingsFacade
 */
class ServiceSettingsFacadeSpec extends ObjectBehavior
{
    /**
     * @var AutoRenewalToggler
     */
    private $autoRenewalToggler;

    /**
     * @var EmailRemindersToggler
     */
    private $emailRemindersToggler;

    function let(AutoRenewalToggler $autoRenewalToggler, EmailRemindersToggler $emailRemindersToggler)
    {
        $this->autoRenewalToggler = $autoRenewalToggler;
        $this->emailRemindersToggler = $emailRemindersToggler;
        $this->beConstructedWith($this->autoRenewalToggler, $this->emailRemindersToggler);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('ServiceSettingsModule\Facades\ServiceSettingsFacade');
    }

    function it_can_enable_auto_renewal(ServiceSettings $settings, Token $token)
    {
        $this->autoRenewalToggler->enable($settings, $token)->shouldBeCalled();
        $this->emailRemindersToggler->enable($settings)->shouldBeCalled();

        $this->enableAutoRenewal($settings, $token);
    }

    function it_can_disable_auto_renewal(ServiceSettings $settings)
    {
        $this->autoRenewalToggler->disable($settings)->shouldBeCalled();

        $this->disableAutoRenewal($settings);
    }

    function it_can_suspend_auto_renewal(ServiceSettings $settings)
    {
        $this->autoRenewalToggler->suspend($settings)->shouldBeCalled();

        $this->suspendAutoRenewal($settings);
    }

    function it_can_enable_email_reminders(ServiceSettings $settings)
    {
        $this->emailRemindersToggler->enable($settings)->shouldBeCalled();

        $this->enableEmailReminders($settings);
    }

    function it_can_disable_email_reminders(ServiceSettings $settings)
    {
        $this->emailRemindersToggler->disable($settings)->shouldBeCalled();

        $this->disableEmailReminders($settings);
    }
}
