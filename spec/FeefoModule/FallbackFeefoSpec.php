<?php

namespace spec\FeefoModule;

use Doctrine\Common\Cache\Cache;
use Exception;
use FeefoModule\FallbackFeefo;
use FeefoModule\IFeefo;
use FeefoModule\Summary;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin FallbackFeefo
 */
class FallbackFeefoSpec extends ObjectBehavior
{
    /**
     * @var IFeefo
     */
    private $feefo;

    /**
     * @var Cache
     */
    private $cache;

    function let(IFeefo $feefo, Cache $cache)
    {
        $this->feefo = $feefo;
        $this->cache = $cache;
        $this->beConstructedWith($this->feefo, $this->cache);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(IFeefo::class);
    }

    function it_can_get_summary(Summary $summary)
    {
        $this->feefo->getSummary()->willReturn($summary);
        $this->cache->save('feefo.summary.fallback', $summary);

        $this->getSummary()->shouldReturn($summary);
    }

    function it_can_fall_back_to_cached_version(Summary $summary)
    {
        $this->feefo->getSummary()->willThrow(new Exception);
        $this->cache->contains('feefo.summary.fallback')->willReturn(TRUE);
        $this->cache->fetch('feefo.summary.fallback')->willReturn($summary);

        $this->getSummary()->shouldReturn($summary);
    }

    function it_can_fall_back_to_hardcoded_version_if_cache_empty()
    {
        $this->feefo->getSummary()->willThrow(new Exception);
        $this->cache->contains('feefo.summary.fallback')->willReturn(FALSE);

        $this->getSummary()->shouldBeLike(new Summary(1997, 98));
    }
}
