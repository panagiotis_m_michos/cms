<?php

namespace spec\FeefoModule;

use FeefoModule\Feefo;
use FeefoModule\IFeefo;
use FeefoModule\Summary;
use GuzzleHttp\Client;
use GuzzleHttp\Message\ResponseInterface;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use stdClass;

/**
 * @mixin Feefo
 */
class FeefoSpec extends ObjectBehavior
{
    private $client;

    function let(Client $client)
    {
        $this->client = $client;
        $this->beConstructedWith($this->client, 'merchantId');
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(IFeefo::class);
    }

    function it_can_get_summary(ResponseInterface $response)
    {
        $this->client->get(Argument::any())->willReturn($response);
        $response->xml()->willReturn(
            json_decode(
                json_encode(['SUMMARY' => ['TOTALRESPONSES' => 1, 'AVERAGE' => 2]])
            )
        );

        $summary = $this->getSummary();
        $summary->shouldHaveType(Summary::class);
        $summary->getReviewCount()->shouldBe(1);
        $summary->getAverageRating()->shouldBe(2);
    }
}
