<?php

namespace spec\ToolkitOfferModule\Exporters;

use Goodby\CSV\Export\Standard\Exporter;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ToolkitOfferModule\Exporters\CompaniesCsvExporter;
use ToolkitOfferModule\Exporters\CompanyToolkitOffersExporter;

/**
 * @mixin CompanyToolkitOffersExporter
 */
class CompanyToolkitOffersExporterSpec extends ObjectBehavior
{
    function let(Exporter $csvExporter)
    {
        $this->beConstructedWith($csvExporter, sys_get_temp_dir());
    }

    function it_can_export_companies_csv_from_company_toolkit_offer_collection()
    {
        $file = $this->exportCompanies([]);
        $file->shouldBeAnInstanceOf('Symfony\Component\HttpFoundation\File\File');
    }
}
