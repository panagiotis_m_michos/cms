<?php

namespace spec\ToolkitOfferModule\Entities;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ToolkitOfferModule\Entities\ToolkitOffer;

/**
 * @mixin ToolkitOffer
 */
class ToolkitOfferSpec extends ObjectBehavior
{
    function it_can_get_type()
    {
        $type = 'namescoDomain';
        $this->getType()->shouldReturn(NULL);
        $this->setType($type);
        $this->getType()->shouldReturn($type);
    }

    function it_can_get_name()
    {
        $name = 'name';
        $this->getName()->shouldReturn(NULL);
        $this->setName($name);
        $this->getName()->shouldReturn($name);
    }

    function it_can_get_description()
    {
        $description = '<span>description</span>';
        $this->getDescription()->shouldReturn(NULL);
        $this->setDescription($description);
        $this->getDescription()->shouldReturn($description);
    }
}
