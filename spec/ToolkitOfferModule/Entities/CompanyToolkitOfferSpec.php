<?php

namespace spec\ToolkitOfferModule\Entities;

use DateTime;
use Entities\Company;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ToolkitOfferModule\Entities\CompanyToolkitOffer;
use ToolkitOfferModule\Entities\ToolkitOffer;

/**
 * @mixin CompanyToolkitOffer
 */
class CompanyToolkitOfferSpec extends ObjectBehavior
{
    /**
     * @var Company
     */
    private $company;

    /**
     * @var ToolkitOffer
     */
    private $toolkitOffer;

    function let(Company $company, ToolkitOffer $toolkitOffer)
    {
        $this->company = $company;
        $this->toolkitOffer = $toolkitOffer;
        $this->beConstructedWith($this->company, $this->toolkitOffer);
    }

    function it_can_set_and_get_toolkit_offer(ToolkitOffer $offer)
    {
        $this->getToolkitOffer()->shouldReturn($this->toolkitOffer);
        $this->setToolkitOffer($offer);
        $this->getToolkitOffer()->shouldReturn($offer);
    }

    function it_can_set_and_get_company(Company $company)
    {
        $this->getCompany()->shouldReturn($this->company);
        $this->setCompany($company);
        $this->getCompany()->shouldReturn($company);
    }

    function it_can_set_and_get_selected()
    {
        $this->isSelected()->shouldReturn(TRUE);
        $this->setSelected(FALSE);
        $this->isSelected()->shouldReturn(FALSE);
    }

    function it_can_set_and_get_claimed_at(DateTime $dateClaimed)
    {
        $this->getDateClaimed()->shouldReturn(NULL);
        $this->setDateClaimed($dateClaimed);
        $this->getDateClaimed()->shouldReturn($dateClaimed);
    }
}
