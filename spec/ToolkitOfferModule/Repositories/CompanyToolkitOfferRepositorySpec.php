<?php

namespace spec\ToolkitOfferModule\Repositories;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ToolkitOfferModule\Repositories\CompanyToolkitOfferRepository;

/**
 * @mixin CompanyToolkitOfferRepository
 */
class CompanyToolkitOfferRepositorySpec extends ObjectBehavior
{
    function let(EntityManager $em, ClassMetadata $metadata)
    {
        $this->beConstructedWith($em, $metadata);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(EntityRepository::class);
    }
}
