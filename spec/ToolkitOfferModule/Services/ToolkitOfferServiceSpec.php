<?php

namespace spec\ToolkitOfferModule\Services;

use Doctrine\ORM\Internal\Hydration\IterableResult;
use Entities\Company;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ToolkitOfferModule\Entities\CompanyToolkitOffer;
use ToolkitOfferModule\Entities\ToolkitOffer;
use ToolkitOfferModule\Repositories\CompanyToolkitOfferRepository;
use ToolkitOfferModule\Repositories\ToolkitOfferRepository;
use ToolkitOfferModule\Services\ToolkitOfferService;

/**
 * @mixin ToolkitOfferService
 */
class ToolkitOfferServiceSpec extends ObjectBehavior
{
    /**
     * @var ToolkitOfferRepository
     */
    private $toolkitRepository;

    /**
     * @var CompanyToolkitOfferRepository
     */
    private $companyToolkitRepository;

    function let(
        CompanyToolkitOfferRepository $companyToolkitRepository,
        ToolkitOfferRepository $toolkitRepository
    )
    {
        $this->companyToolkitRepository = $companyToolkitRepository;
        $this->toolkitRepository = $toolkitRepository;
        $this->beConstructedWith($this->companyToolkitRepository, $this->toolkitRepository);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(ToolkitOfferService::class);
    }

    function it_can_add_offer_to_company(Company $company, ToolkitOffer $offer)
    {
        $company
            ->addToolkitOffer(Argument::which('getToolkitOffer', $offer->getWrappedObject()))
            ->shouldBeCalled();

        $this->addOfferToCompany($company, $offer);
    }

    function it_can_get_all_available_offers(ToolkitOffer $offer)
    {
        $offers = [$offer];
        $this->toolkitRepository->getAvailableOffers()->willReturn($offers);
        $this->getAvailableOffers()->shouldReturn($offers);
    }

    function it_can_get_all_unclaimed_namesco_offers(IterableResult $result)
    {
        $this->companyToolkitRepository->getUnclaimedFreeNamescoDomainOffers()->willReturn($result);
        $this->getUnclaimedFreeNamescoDomainOffers()->shouldReturn($result);
    }

    function it_can_get_all_unclaimed_adwords_offers(IterableResult $result)
    {
        $this->companyToolkitRepository->getUnclaimedAdwordsVoucherOffers()->willReturn($result);
        $this->getUnclaimedAdwordsVoucherOffers()->shouldReturn($result);
    }

    function it_can_get_all_unclaimed_google_apps_offers(IterableResult $result)
    {
        $this->companyToolkitRepository->getUnclaimedGoogleAppsDiscountOffers()->willReturn($result);
        $this->getUnclaimedGoogleAppsDiscountOffers()->shouldReturn($result);
    }

    function it_can_get_all_unclaimed_make_more_profit_guide_offers(IterableResult $result)
    {
        $this->companyToolkitRepository->getUnclaimedEbookOffers()->willReturn($result);
        $this->getUnclaimedEbookOffers()->shouldReturn($result);
    }

    function it_can_get_all_unclaimed_free_agent_offers(IterableResult $result)
    {
        $this->companyToolkitRepository->getUnclaimedFreeAgentTrialOffers()->willReturn($result);
        $this->getUnclaimedFreeAgentOffers()->shouldReturn($result);
    }

    function it_can_get_all_unclaimed_tax_consultation_offers(IterableResult $result)
    {
        $this->companyToolkitRepository->getUnclaimedTaxConsultationOffers()->willReturn($result);
        $this->getUnclaimedTaxAssistOffers()->shouldReturn($result);
    }

    function it_can_get_all_unclaimed_facebook_group_offers(IterableResult $result)
    {
        $this->companyToolkitRepository->getUnclaimedFacebookGroupOffers()->willReturn($result);
        $this->getUnclaimedFacebookGroupOffers()->shouldReturn($result);
    }

    function it_can_claim_an_offer(CompanyToolkitOffer $offer)
    {
        $this->claim($offer);

        $this->companyToolkitRepository->flush()->shouldBeCalled();
        $offer->setDateClaimed(Argument::type('DateTime'))->shouldBeCalled();;
    }
}
