<?php

namespace spec\ToolkitOfferModule\Emailers;

use Entities\Customer;
use FEmail;
use IEmailService;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ToolkitOfferModule\Emailers\EmailFactory;
use ToolkitOfferModule\Emailers\ToolkitOfferEmailer;

/**
 * @mixin ToolkitOfferEmailer
 */
class ToolkitOfferEmailerSpec extends ObjectBehavior
{
    /**
     * @var EmailFactory
     */
    private $emailFactory;

    /**
     * @var IEmailService
     */
    private $emailService;

    function let(IEmailService $emailService, EmailFactory $emailFactory)
    {
        $this->emailService = $emailService;
        $this->emailFactory = $emailFactory;
        $this->beConstructedWith($this->emailService, $this->emailFactory);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(ToolkitOfferEmailer::class);
    }

    function it_can_send_tax_assist_email(Customer $customer, FEmail $email)
    {
        $this->emailFactory->getTaxAssistEmail()->willReturn($email);
        $customer->getEmail()->willReturn('customer@email.com');
        $customer->getFirstName()->willReturn('FirstName');

        $this->sendTaxAssistEmail($customer);

        $this->emailService->send($email, $customer)->shouldBeCalled();
    }

    function it_can_send_free_agent_email(Customer $customer, FEmail $email)
    {
        $this->emailFactory->getFreeAgentEmail()->willReturn($email);
        $customer->getEmail()->willReturn('customer@email.com');
        $customer->getFirstName()->willReturn('FirstName');

        $this->sendFreeAgentEmail($customer);

        $this->emailService->send($email, $customer)->shouldBeCalled();
    }

    function it_can_send_how_to_make_a_profit_ebook_email(Customer $customer, FEmail $email)
    {
        $this->emailFactory->getEbookEmail()->willReturn($email);
        $customer->getEmail()->willReturn('customer@email.com');
        $customer->getFirstName()->willReturn('FirstName');

        $this->sendMakeMoreProfitGuideEmail($customer);

        $this->emailService->send($email, $customer)->shouldBeCalled();
    }

    function it_can_send_facebook_group_invite_email(Customer $customer, FEmail $email)
    {
        $this->emailFactory->getFacebookGroupEmail()->willReturn($email);
        $customer->getEmail()->willReturn('customer@email.com');
        $customer->getFirstName()->willReturn('FirstName');

        $this->sendFacebookGroupEmail($customer);

        $this->emailService->send($email, $customer)->shouldBeCalled();
    }
}
