<?php

namespace spec\ToolkitOfferModule\Commands;

use Cron\Commands\CommandAbstract;
use Cron\INotifier;
use Doctrine\ORM\Internal\Hydration\IterableResult;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use ToolkitOfferModule\Commands\ProcessUnclaimedNamescoOffersCommand;
use ToolkitOfferModule\Entities\CompanyToolkitOffer;
use ToolkitOfferModule\Exporters\CompanyToolkitOffersExporter;
use ToolkitOfferModule\Services\ToolkitOfferService;
use ToolkitOfferModule\Uploaders\GoogleDriveUploader;
use Utils\File;

/**
 * @mixin ProcessUnclaimedNamescoOffersCommand
 */
class ProcessUnclaimedNamescoOffersCommandSpec extends ObjectBehavior
{
    /**
     * @var ToolkitOfferService
     */
    private $service;

    /**
     * @var CompanyToolkitOffersExporter
     */
    private $exporter;

    /**
     * @var GoogleDriveUploader
     */
    private $uploader;

    function let(
        ToolkitOfferService $service,
        CompanyToolkitOffersExporter $exporter,
        GoogleDriveUploader $uploader,
        LoggerInterface $logger,
        INotifier $notifier
    )
    {
        $this->service = $service;
        $this->exporter = $exporter;
        $this->uploader = $uploader;
        $this->beConstructedWith($this->service, $this->exporter, $this->uploader, $logger, $notifier);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(CommandAbstract::class);
    }

    function it_can_execute(File $file, CompanyToolkitOffer $offer)
    {
        $this->service->getUnclaimedFreeNamescoDomainOffers()->willReturn([$offer]);
        $this->exporter->exportCompanies([$offer])->willReturn($file);
        $this->uploader->upload($file, Argument::any())->shouldBeCalled();

        $filename = tempnam(sys_get_temp_dir(), 'test_upload_file_');
        $file->getPathname()->willReturn($filename);

        $this->execute();
    }
}
