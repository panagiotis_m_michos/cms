<?php

namespace spec\ToolkitOfferModule\Commands;

use Cron\Commands\CommandAbstract;
use Cron\INotifier;
use Entities\Company;
use Entities\Customer;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Log\LoggerInterface;
use ToolkitOfferModule\Commands\ProcessUnclaimedEbookOffersCommand;
use ToolkitOfferModule\Emailers\ToolkitOfferEmailer;
use ToolkitOfferModule\Entities\CompanyToolkitOffer;
use ToolkitOfferModule\Services\ToolkitOfferService;

/**
 * @mixin ProcessUnclaimedEbookOffersCommand
 */
class ProcessUnclaimedEbookOffersCommandSpec extends ObjectBehavior
{
    /**
     * @var ToolkitOfferEmailer
     */
    private $emailer;

    /**
     * @var ToolkitOfferService
     */
    private $service;

    function let(
        ToolkitOfferService $service,
        ToolkitOfferEmailer $emailer,
        LoggerInterface $logger,
        INotifier $notifier
    )
    {
        $this->service = $service;
        $this->emailer = $emailer;
        $this->beConstructedWith($this->service, $this->emailer, $logger, $notifier);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(CommandAbstract::class);
    }

    function it_can_execute(
        CompanyToolkitOffer $companyToolkitOffer,
        Company $company,
        Customer $customer
    )
    {
        $this->service->getUnclaimedEbookOffers()->willReturn([$companyToolkitOffer]);
        $companyToolkitOffer->getCompany()->willReturn($company);
        $company->getCustomer()->willReturn($customer);
        $this->emailer->sendMakeMoreProfitGuideEmail($customer)->shouldBeCalled();
        $this->service->claim($companyToolkitOffer)->shouldBeCalled();

        $this->execute();
    }
}
