<?php

namespace spec\ToolkitOfferModule\Uploaders;

use Gaufrette\Filesystem;
use org\bovigo\vfs\vfsStream;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ToolkitOfferModule\Uploaders\FtpUploader;
use Utils\File;

/**
 * @mixin FtpUploader
 */
class FtpUploaderSpec extends ObjectBehavior
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    function let(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
        $this->beConstructedWith($this->filesystem);
    }

    function it_can_upload(File $file)
    {
        vfsStream::setup()->addChild(vfsStream::newFile('test'));
        $tmpFilePath = vfsStream::url('root/test');

        $file->getPathname()->willReturn($tmpFilePath);
        $this->filesystem->write('filename', Argument::any(), TRUE)->shouldBeCalled();

        $this->upload($file, 'filename');
    }
}
