<?php

namespace spec\ToolkitOfferModule\Uploaders;

use Google_Service_Drive;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ToolkitOfferModule\Uploaders\GoogleDriveUploader;

/**
 * @mixin GoogleDriveUploader
 */
class GoogleDriveUploaderSpec extends ObjectBehavior
{
    function let(Google_Service_Drive $drive)
    {
        $this->beConstructedWith($drive, 'filename.txt', 'directory id');
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(GoogleDriveUploader::class);
    }
}
