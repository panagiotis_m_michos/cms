<?php

namespace spec\ServicesAdminModule\Providers;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ServiceActivatorModule\ProductRelationshipChecker;
use ServiceActivatorModule\RelatedServiceProvider;
use Services\NodeService;
use ServicesAdminModule\Formatters\AddServiceFormProductFormatter;
use ServicesAdminModule\Providers\AddServiceFormChoicesProvider;

/**
 * @mixin AddServiceFormChoicesProvider
 */
class AddServiceFormChoicesProviderSpec extends ObjectBehavior
{
    /**
     * @var int[]
     */
    private $productIds;

    /**
     * @var NodeService
     */
    private $nodeService;

    /**
     * @var ProductRelationshipChecker
     */
    private $productChecker;

    /**
     * @var RelatedServiceProvider
     */
    private $relatedServiceProvider;

    /**
     * @var AddServiceFormProductFormatter
     */
    private $productFormatter;

    function let(
        NodeService $nodeService,
        ProductRelationshipChecker $productChecker,
        RelatedServiceProvider $relatedServiceProvider,
        AddServiceFormProductFormatter $productFormatter
    )
    {
        $this->productIds = [1, 2, 3];
        $this->nodeService = $nodeService;
        $this->productChecker = $productChecker;
        $this->relatedServiceProvider = $relatedServiceProvider;
        $this->productFormatter = $productFormatter;
        $this->beConstructedWith(
            $this->productIds,
            $this->nodeService,
            $this->productChecker,
            $this->relatedServiceProvider,
            $this->productFormatter
        );
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(AddServiceFormChoicesProvider::class);
    }
}
