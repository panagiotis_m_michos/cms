<?php

namespace spec\ServicesAdminModule\Entities;

use BasketProduct;
use DateTime;
use Entities\Company;
use Entities\Customer;
use Entities\Order;
use Entities\OrderItem;
use Entities\User;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Services\UserService;
use ServicesAdminModule\Entities\AddServiceFormOrderFactory;
use ServicesAdminModule\Forms\AddServiceForm;
use ServicesAdminModule\Forms\AddServiceFormData;
use VatModule\Calculators\VatCalculator;

/**
 * @mixin AddServiceFormOrderFactory
 */
class AddServiceFormOrderFactorySpec extends ObjectBehavior
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var VatCalculator
     */
    private $vatCalculator;

    function let(UserService $userService, VatCalculator $vatCalculator)
    {
        $this->userService = $userService;
        $this->vatCalculator = $vatCalculator;
        $this->beConstructedWith($this->userService, $this->vatCalculator);
    }
    
    function it_is_initializable()
    {
        $this->shouldHaveType(AddServiceFormOrderFactory::class);
    }

    function it_can_create_goodwill_order(Company $company, BasketProduct $product, Customer $customer, User $agent)
    {
        $data = new AddServiceFormData;
        $data->setReason(AddServiceForm::REASON_GOODWILL);

        $this->userService->getLoggedInUser()->willReturn($agent);

        $company->getCustomer()->willReturn($customer);
        $company->getCompanyName()->willReturn('test company');
        $company->getCompanyNumber()->willReturn('12345678');

        $orderItem = $this->create($company, $product, $data);
        $orderItem->shouldBeAnInstanceOf(OrderItem::class);
        $orderItem->getCompany()->shouldBe($company);
        $orderItem->getQty()->shouldBe(0);
        $order = $orderItem->getOrder();
        $order->getCustomer()->shouldBe($customer);
        $order->getDescription()->shouldContain('Goodwill');
        $order->getDiscount()->shouldBe($orderItem->getSubTotal());
        $order->getTotal()->shouldBe(0);
        $order->getPaymentMediumId()->shouldBe(Order::PAYMENT_MEDIUM_COMPLIMENTARY);
    }

    function it_can_create_non_standard_order(Company $company, BasketProduct $product, Customer $customer, User $agent)
    {
        $total = 12;
        $subtotal = 10;
        $vat = 2;
        $data = new AddServiceFormData;
        $data->setReason(AddServiceForm::REASON_NON_STANDARD);
        $data->setPaymentDate(new DateTime('2016-05-20'));
        $data->setTotalAmount($total);

        $this->userService->getLoggedInUser()->willReturn($agent);
        $this->vatCalculator->getSubtotalFromTotal($total)->willReturn($subtotal);
        $this->vatCalculator->getVatFromTotal($total)->willReturn($vat);

        $company->getCustomer()->willReturn($customer);
        $company->getCompanyName()->willReturn('test company');
        $company->getCompanyNumber()->willReturn('12345678');

        $orderItem = $this->create($company, $product, $data);
        $orderItem->shouldBeAnInstanceOf(OrderItem::class);
        $orderItem->getCompany()->shouldBe($company);
        $orderItem->getQty()->shouldBe(1);
        $order = $orderItem->getOrder();
        $order->getCustomer()->shouldBe($customer);
        $order->getDescription()->shouldContain('Non-standard');
        $order->getDescription()->shouldContain('20/05/2016');
        $order->getDiscount()->shouldBe(0);
        $order->getPaymentMediumId()->shouldBe(Order::PAYMENT_MEDIUM_NON_STANDARD);
        $order->getSubTotal()->shouldBe($subtotal);
        $order->getTotal()->shouldBe($total);
    }
}
