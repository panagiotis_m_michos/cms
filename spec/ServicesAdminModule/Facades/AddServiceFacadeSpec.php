<?php

namespace spec\ServicesAdminModule\Facades;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ServiceActivatorModule\ProductRelationshipChecker;
use ServiceActivatorModule\RelatedServiceProvider;
use Services\NodeService;
use Services\ProductService;
use ServicesAdminModule\Entities\AddServiceFormOrderFactory;
use ServicesAdminModule\Facades\AddServiceFacade;
use ServiceSettingsModule\Services\ServiceSettingsService;

/**
 * @mixin AddServiceFacade
 */
class AddServiceFacadeSpec extends ObjectBehavior
{
    /**
     * @var NodeService
     */
    private $nodeService;

    /**
     * @var ProductService
     */
    private $productService;

    /**
     * @var AddServiceFormOrderFactory
     */
    private $orderFactory;

    /**
     * @var RelatedServiceProvider
     */
    private $relatedServiceProvider;

    /**
     * @var ProductRelationshipChecker
     */
    private $productChecker;

    /**
     * @var ServiceSettingsService
     */
    private $serviceSettingsService;

    function let(
        NodeService $nodeService,
        ProductService $productService,
        AddServiceFormOrderFactory $orderFactory,
        RelatedServiceProvider $relatedServiceProvider,
        ProductRelationshipChecker $productChecker,
        ServiceSettingsService $serviceSettingsService
    )
    {
        $this->nodeService = $nodeService;
        $this->productService = $productService;
        $this->productChecker = $productChecker;
        $this->orderFactory = $orderFactory;
        $this->relatedServiceProvider = $relatedServiceProvider;
        $this->serviceSettingsService = $serviceSettingsService;

        $this->beConstructedWith(
            $this->nodeService,
            $this->productService,
            $this->orderFactory,
            $this->relatedServiceProvider,
            $this->productChecker,
            $this->serviceSettingsService
        );
    }
    
    function it_is_initializable()
    {
        $this->shouldHaveType(AddServiceFacade::class);
    }
}
