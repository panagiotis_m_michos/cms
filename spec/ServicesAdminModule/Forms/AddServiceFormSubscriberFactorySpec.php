<?php

namespace spec\ServicesAdminModule\Forms;

use Entities\Company;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ServicesAdminModule\Forms\AddServiceFormSubscriber;
use ServicesAdminModule\Forms\AddServiceFormSubscriberFactory;
use ServicesAdminModule\Providers\AddServiceFormChoicesProvider;

/**
 * @mixin AddServiceFormSubscriberFactory
 */
class AddServiceFormSubscriberFactorySpec extends ObjectBehavior
{
    /**
     * @var AddServiceFormChoicesProvider
     */
    private $choicesProvider;

    function let(AddServiceFormChoicesProvider $choicesProvider)
    {
        $this->choicesProvider = $choicesProvider;
        $this->beConstructedWith($this->choicesProvider);
    }
    
    function it_is_initializable()
    {
        $this->shouldHaveType(AddServiceFormSubscriberFactory::class);
    }
    
    function it_can_create_add_service_form_subscriber(Company $company)
    {
        $this->create($company)->shouldBeAnInstanceOf(AddServiceFormSubscriber::class);
    }
}
