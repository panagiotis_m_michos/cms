<?php

namespace spec\ServicesAdminModule\Forms;

use Entities\Company;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ServicesAdminModule\Forms\AddServiceForm;
use ServicesAdminModule\Forms\AddServiceFormFactory;
use ServicesAdminModule\Forms\AddServiceFormSubscriber;
use ServicesAdminModule\Forms\AddServiceFormSubscriberFactory;
use ServicesAdminModule\Providers\AddServiceFormChoicesProvider;

/**
 * @mixin AddServiceFormFactory
 */
class AddServiceFormFactorySpec extends ObjectBehavior
{
    /**
     * @var AddServiceFormSubscriberFactory
     */
    private $addServiceFormSubscriberFactory;

    /**
     * @var AddServiceFormChoicesProvider
     */
    private $choicesProvider;

    function let(AddServiceFormSubscriberFactory $addServiceFormSubscriberFactory, AddServiceFormChoicesProvider $choicesProvider)
    {
        $this->addServiceFormSubscriberFactory = $addServiceFormSubscriberFactory;
        $this->choicesProvider = $choicesProvider;
        $this->beConstructedWith($this->addServiceFormSubscriberFactory, $this->choicesProvider);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(AddServiceFormFactory::class);
    }

    function it_can_create_add_service_form(Company $company, AddServiceFormSubscriber $addServiceFormSubscriber)
    {
        $this->addServiceFormSubscriberFactory->create($company)->willReturn($addServiceFormSubscriber);

        $this->create($company)->shouldBeAnInstanceOf(AddServiceForm::class);
    }
}
