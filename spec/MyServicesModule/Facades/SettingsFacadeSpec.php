<?php

namespace spec\MyServicesModule\Facades;

use Entities\Payment\Token;
use Entities\ServiceSettings;
use MyServicesModule\Facades\SettingsFacade;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ServiceSettingsModule\Facades\ServiceSettingsFacade;

/**
 * @mixin SettingsFacade
 */
class SettingsFacadeSpec extends ObjectBehavior
{
    /**
     * @var ServiceSettingsFacade
     */
    private $settingsFacade;

    function let(ServiceSettingsFacade $settingsFacade)
    {
        $this->settingsFacade = $settingsFacade;
        $this->beConstructedWith($this->settingsFacade);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('MyServicesModule\Facades\SettingsFacade');
    }

    function it_can_process_settings_array(ServiceSettings $settings)
    {
        $settingsData = [
            'setting' => 'emailReminders',
            'value' => TRUE,
        ];

        $this->settingsFacade->enableEmailReminders($settings)->shouldBeCalled();

        $this->processSettingsArray($settings, $settingsData);

        $settingsData = [
            'setting' => 'emailReminders',
            'value' => FALSE,
        ];

        $this->settingsFacade->disableEmailReminders($settings)->shouldBeCalled();

        $this->processSettingsArray($settings, $settingsData);
    }

    function it_can_enable_auto_renewal(ServiceSettings $settings, Token $token)
    {
        $this->settingsFacade->enableAutoRenewal($settings, $token)->shouldBeCalled();

        $this->enableAutoRenewal($settings, $token);
    }

    function it_can_disable_auto_renewal(ServiceSettings $settings)
    {
        $this->settingsFacade->disableAutoRenewal($settings)->shouldBeCalled();

        $this->disableAutoRenewal($settings);
    }
}
