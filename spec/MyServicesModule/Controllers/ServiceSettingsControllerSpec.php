<?php

namespace spec\MyServicesModule\Controllers;

use Entities\Company;
use Entities\Customer;
use Entities\Payment\Token;
use Entities\Service;
use Entities\ServiceSettings;
use MyServicesControler;
use MyServicesModule\Controllers\ServiceSettingsController;
use MyServicesModule\Facades\SettingsFacade;
use MyServicesModule\Renderers\CompanyBlockRenderer;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Services\CompanyService;
use Services\ControllerHelper;
use Services\ServiceService;
use ServiceSettingsModule\Services\ServiceSettingsService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @mixin ServiceSettingsController
 */
class ServiceSettingsControllerSpec extends ObjectBehavior
{
    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var ServiceService
     */
    private $serviceService;

    /**
     * @var ServiceSettingsService
     */
    private $serviceSettingsService;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var SettingsFacade
     */
    private $settingsFacade;

    /**
     * @var CompanyBlockRenderer
     */
    private $companyBlockRenderer;

    function let(
        ControllerHelper $controllerHelper,
        Customer $customer,
        ServiceService $serviceService,
        ServiceSettingsService $serviceSettingsService,
        CompanyService $companyService,
        SettingsFacade $settingsFacade,
        CompanyBlockRenderer $companyBlockRenderer
    )
    {
        $this->controllerHelper = $controllerHelper;
        $this->customer = $customer;
        $this->serviceService = $serviceService;
        $this->serviceSettingsService = $serviceSettingsService;
        $this->companyService = $companyService;
        $this->settingsFacade = $settingsFacade;
        $this->companyBlockRenderer = $companyBlockRenderer;

        $this->beConstructedWith(
            $this->controllerHelper,
            $this->customer,
            $this->serviceService,
            $this->serviceSettingsService,
            $this->companyService,
            $this->settingsFacade,
            $this->companyBlockRenderer
        );
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('MyServicesModule\Controllers\ServiceSettingsController');
    }

    function it_can_get_service_settings(Service $service, ServiceSettings $settings)
    {
        $serviceId = 1;
        $this->serviceService->getCustomerService($this->customer, $serviceId)->willReturn($service);
        $this->serviceSettingsService->getSettingsByService($service)->willReturn($settings);
        $settings->jsonSerialize()->willReturn([]);

        $this->getServiceSettings($serviceId)->shouldBeAnInstanceOf('Symfony\Component\HttpFoundation\JsonResponse');
    }

    function it_throws_exception_when_no_service_is_found()
    {
        $serviceId = 1;
        $this->serviceService->getCustomerService($this->customer, $serviceId)->willThrow('EntityNotFound');

        $this->shouldThrow(new NotFoundHttpException)->duringGetServiceSettings($serviceId);
    }

    function it_can_set_service_settings(Service $service, Request $request, ServiceSettings $settings)
    {
        $serviceId = 1;
        $settingsData = [
            'setting' => 'emailReminders',
            'value' => TRUE,
        ];

        $this->serviceService->getCustomerService($this->customer, $serviceId)->willReturn($service);
        $this->serviceSettingsService->getSettingsByService($service)->willReturn($settings);
        $this->controllerHelper->getRequest()->willReturn($request);
        $request->getContent()->willReturn(json_encode($settingsData));

        $this->settingsFacade->processSettingsArray($settings, $settingsData)->shouldBeCalled();

        $this->setServiceSetting($serviceId)->shouldBeAnInstanceOf('Symfony\Component\HttpFoundation\JsonResponse');
    }

    function it_can_disable_reminders_for_company(Company $company)
    {
        $companyId = 1;
        $link = 'link';

        $this->companyService->getCustomerCompany($this->customer, $companyId)->willReturn($company);
        $this->controllerHelper->getLink(
            MyServicesControler::PAGE_SERVICES,
            ['disabledRemindersCompanyId' => $companyId]
        )->willReturn($link);

        $this->serviceSettingsService->disableEmailRemindersByCompany($company)->shouldBeCalled();

        $this->disableRemindersForCompany($companyId)->shouldBeLike(new RedirectResponse($link));
    }

    function it_can_enable_auto_renewal(Service $service, Company $company, ServiceSettings $settings, Token $token, Request $request, ParameterBag $getParameters)
    {
        $serviceId = 1;
        $request->query = $getParameters;
        $templateString = 'template';

        $service->getCompany()->willReturn($company);
        $service->isOverdue()->willReturn(FALSE);
        $service->expiresToday()->willReturn(TRUE);
        $this->serviceService->getCustomerService($this->customer, $serviceId)->willReturn($service);
        $this->serviceSettingsService->getSettingsByService($service)->willReturn($settings);
        $this->customer->getActiveToken()->willReturn($token);
        $this->controllerHelper->getRequest()->willReturn($request);
        $getParameters->get('checkedServiceIds', [])->willReturn([]);
        $this->serviceService->getServicesByIds([])->willReturn([]);
        $this->companyBlockRenderer->getHtml($this->customer, $company, [])->willReturn($templateString);

        $this->settingsFacade->enableAutoRenewal($settings, $token)->shouldBeCalled();

        $this->enableAutoRenewal($serviceId)->shouldBeLike(new JsonResponse(['error' => FALSE, 'template' => $templateString, 'showAdditionalInfo' => TRUE]));
    }

    function it_throws_exception_when_enabling_auto_renewal_for_customer_without_active_token(Service $service, ServiceSettings $settings)
    {
        $serviceId = 1;

        $this->serviceService->getCustomerService($this->customer, $serviceId)->willReturn($service);
        $this->serviceSettingsService->getSettingsByService($service)->willReturn($settings);
        $this->customer->getActiveToken()->willReturn(NULL);

        $this->shouldThrow(new NotFoundHttpException)->duringEnableAutoRenewal($serviceId);
    }

    function it_can_disable_auto_renewal(Service $service, Company $company, ServiceSettings $settings, Request $request, ParameterBag $getParameters)
    {
        $serviceId = 1;
        $request->query = $getParameters;
        $templateString = 'template';

        $service->getCompany()->willReturn($company);
        $this->serviceService->getCustomerService($this->customer, $serviceId)->willReturn($service);
        $this->serviceSettingsService->getSettingsByService($service)->willReturn($settings);
        $this->controllerHelper->getRequest()->willReturn($request);
        $getParameters->get('checkedServiceIds', [])->willReturn([]);
        $this->serviceService->getServicesByIds([])->willReturn([]);
        $this->companyBlockRenderer->getHtml($this->customer, $company, [])->willReturn($templateString);

        $this->settingsFacade->disableAutoRenewal($settings)->shouldBeCalled();

        $this->disableAutoRenewal($serviceId)->shouldBeLike(new JsonResponse(['error' => FALSE, 'template' => $templateString]));
    }
}
