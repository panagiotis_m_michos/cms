<?php

namespace spec\ServiceActivatorModule;

use DateTime;
use Entities\Company;
use Entities\Customer;
use Entities\Service;
use Exceptions\Technical\ServiceActivationException;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ServiceActivatorModule\Exceptions\CompanyNotIncorporatedException;
use ServiceActivatorModule\Exceptions\ServiceAlreadyActivatedException;
use ServiceActivatorModule\ProductRelationshipChecker;
use ServiceActivatorModule\RelatedServiceProvider;
use ServiceActivatorModule\ServiceActivator;
use Services\ServiceService;
use Tests\Helpers\ObjectHelper;
use Utils\Date;

/**
 * @mixin ServiceActivator
 */
class ServiceActivatorSpec extends ObjectBehavior
{
    /**
     * @var ServiceService
     */
    private $serviceService;

    /**
     * @var RelatedServiceProvider
     */
    private $relatedServiceProvider;

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var Company
     */
    private $company;

    function let(ServiceService $serviceService, RelatedServiceProvider $relatedServiceProvider)
    {
        $this->relatedServiceProvider = $relatedServiceProvider;
        $this->serviceService = $serviceService;
        $this->beConstructedWith($this->serviceService, new ProductRelationshipChecker, $this->relatedServiceProvider);

        $this->customer = ObjectHelper::createCustomer();
        $this->company = ObjectHelper::createCompany($this->customer);
        $this->company->setCompanyNumber('12345678');
        $this->company->setIncorporationDate(new DateTime);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(ServiceActivator::class);
    }

    function it_can_not_activate_service_for_not_incorporated_company()
    {
        $company = ObjectHelper::createCompany($this->customer);
        $order = ObjectHelper::createOrder($this->customer);
        $orderItem = ObjectHelper::createOrderItem($order);
        $service = ObjectHelper::createServiceFromType(Service::TYPE_REGISTERED_OFFICE, 165, $company, $orderItem, new Date, new Date('+1 year'));

        $this->shouldThrow(CompanyNotIncorporatedException::class)->duringActivate($service);
    }

    function it_can_not_activate_already_active_service()
    {
        $order = ObjectHelper::createOrder($this->customer);
        $orderItem = ObjectHelper::createOrderItem($order);
        $service = ObjectHelper::createServiceFromType(Service::TYPE_REGISTERED_OFFICE, 165, $this->company, $orderItem, new Date, new Date('+1 year'));

        $this->shouldThrow(ServiceAlreadyActivatedException::class)->duringActivate($service);
    }

    function it_can_activate_service_for_incorporated_company()
    {
        $order = ObjectHelper::createOrder($this->customer);
        $orderItem = ObjectHelper::createOrderItem($order);
        $service = ObjectHelper::createServiceFromType(Service::TYPE_REGISTERED_OFFICE, 165, $this->company, $orderItem);
        $duration = $service->getProduct()->getDuration();

        $this->relatedServiceProvider->getRelatedService($this->company, $service->getProduct(), $order->getDtc())->willReturn(NULL);
        $this->serviceService->isFormationServiceForCompany($service)->willReturn(FALSE);
        $this->serviceService->getCompanyNonExpiredServicesOn($this->company, $service->getOrder()->getDtc())->willReturn([]);
        $this->serviceService->setServiceDates($service, new Date, new Date("{$duration} -1 day"))->shouldBeCalled();

        $this->activate($service);
    }

    function it_can_activate_service_as_renewal_bought_during_active_period()
    {
        $order1 = ObjectHelper::createOrder($this->customer, 1, new Date('2005-01-01'));
        $order2 = ObjectHelper::createOrder($this->customer, 1, new Date('2005-06-10'));
        $orderItem1 = ObjectHelper::createOrderItem($order1);
        $orderItem2 = ObjectHelper::createOrderItem($order2);

        $service1 = ObjectHelper::createServiceFromType(Service::TYPE_REGISTERED_OFFICE, 165, $this->company, $orderItem1, new Date('2005-01-01'), new Date('2005-12-31'));
        $service2 = ObjectHelper::createServiceFromType(Service::TYPE_REGISTERED_OFFICE, 165, $this->company, $orderItem2);
        $duration = $service2->getProduct()->getDuration();

        $this->relatedServiceProvider->getRelatedService($this->company, $service2->getProduct(), $order2->getDtc())->willReturn($service1);
        $this->serviceService->isFormationServiceForCompany($service2)->willReturn(FALSE);
        $this->serviceService->getCompanyNonExpiredServicesOn($this->company, $service2->getOrder()->getDtc())->willReturn([$service1]);
        $this->serviceService->setServiceDates($service2, new Date('2006-01-01'), new Date("2006-01-01 {$duration} -1 day"))->shouldBeCalled();

        $this->activate($service2);
    }

    function it_can_activate_service_as_renewal_bought_during_overdue_period()
    {
        $order1 = ObjectHelper::createOrder($this->customer, 1, new Date('2005-01-01'));
        $order2 = ObjectHelper::createOrder($this->customer, 1, new Date('2006-01-10'));
        $orderItem1 = ObjectHelper::createOrderItem($order1);
        $orderItem2 = ObjectHelper::createOrderItem($order2);

        $service1 = ObjectHelper::createServiceFromType(Service::TYPE_REGISTERED_OFFICE, 165, $this->company, $orderItem1, new Date('2005-01-01'), new Date('2005-12-31'));
        $service2 = ObjectHelper::createServiceFromType(Service::TYPE_REGISTERED_OFFICE, 165, $this->company, $orderItem2);
        $duration = $service2->getProduct()->getDuration();

        $this->relatedServiceProvider->getRelatedService($this->company, $service2->getProduct(), $order2->getDtc())->willReturn($service1);
        $this->serviceService->isFormationServiceForCompany($service2)->willReturn(FALSE);
        $this->serviceService->getCompanyNonExpiredServicesOn($this->company, $service2->getOrder()->getDtc())->willReturn([$service1]);
        $this->serviceService->setServiceDates($service2, new Date('2006-01-01'), new Date("2006-01-01 {$duration} -1 day"))->shouldBeCalled();

        $this->activate($service2);
    }

    function it_can_activate_service_as_new_bought_after_overdue_period()
    {
        $order1 = ObjectHelper::createOrder($this->customer, 1, new Date('2005-01-01'));
        $order2 = ObjectHelper::createOrder($this->customer, 1, new Date('2006-01-30'));
        $orderItem1 = ObjectHelper::createOrderItem($order1);
        $orderItem2 = ObjectHelper::createOrderItem($order2);

        $service1 = ObjectHelper::createServiceFromType(Service::TYPE_REGISTERED_OFFICE, 165, $this->company, $orderItem1, new Date('2005-01-01'), new Date('2005-12-31'));
        $service2 = ObjectHelper::createServiceFromType(Service::TYPE_REGISTERED_OFFICE, 165, $this->company, $orderItem2);
        $duration = $service2->getProduct()->getDuration();

        $this->relatedServiceProvider->getRelatedService($this->company, $service2->getProduct(), $order2->getDtc())->willReturn(NULL);
        $this->serviceService->isFormationServiceForCompany($service2)->willReturn(FALSE);
        $this->serviceService->getCompanyNonExpiredServicesOn($this->company, $service2->getOrder()->getDtc())->willReturn([]);
        $this->serviceService->setServiceDates($service2, new Date('2006-01-30'), new Date("2006-01-30 {$duration} -1 day"))->shouldBeCalled();

        $this->activate($service2);
    }

    function it_can_not_activate_service_when_package_is_active()
    {
        $order1 = ObjectHelper::createOrder($this->customer, 1, new Date('2005-01-01'));
        $order2 = ObjectHelper::createOrder($this->customer, 1, new Date('2005-10-20'));
        $orderItem1 = ObjectHelper::createOrderItem($order1);
        $orderItem2 = ObjectHelper::createOrderItem($order2);

        $service1 = ObjectHelper::createPackageServiceFromType(Service::TYPE_PACKAGE_PRIVACY, 1315, $this->company, $orderItem1, new Date('2005-01-01'), new Date('2005-12-31'));
        $service2 = ObjectHelper::createServiceFromType(Service::TYPE_REGISTERED_OFFICE, 165, $this->company, $orderItem2);

        $this->relatedServiceProvider->getRelatedService($this->company, $service2->getProduct(), $order2->getDtc())->willReturn($service1);
        $this->serviceService->isFormationServiceForCompany($service2)->willReturn(FALSE);
        $this->serviceService->getCompanyNonExpiredServicesOn($this->company, $service2->getOrder()->getDtc())->willReturn([$service1]);

        $this->shouldThrow(ServiceActivationException::packageToProductDowngrade($this->company, $service1, $service2))->duringActivate($service2);
    }

    function it_can_activate_formation_package_order_on_incorporation_date()
    {
        $order = ObjectHelper::createOrder($this->customer, 1, new Date('2010-03-25'));
        $orderItem = ObjectHelper::createOrderItem($order);
        $this->company->setIncorporationDate(new Date('2010-04-01'));

        $package = ObjectHelper::createPackageServiceFromType(Service::TYPE_PACKAGE_PRIVACY, 1315, $this->company, $orderItem);
        $duration = $package->getProduct()->getDuration();

        $this->serviceService->isFormationServiceForCompany($package)->willReturn(TRUE);
        $this->serviceService->getCompanyNonExpiredServicesOn($this->company, $package->getOrder()->getDtc())->willReturn([]);
        $this->serviceService->setServiceDates($package, new Date('2010-04-01'), new Date("2010-04-01 {$duration} -1 day"))->shouldBeCalled();

        $this->activate($package);
    }

    function it_can_activate_package_as_renewal_bought_during_active_period()
    {
        $order1 = ObjectHelper::createOrder($this->customer, 1, new Date('2005-01-01'));
        $order2 = ObjectHelper::createOrder($this->customer, 1, new Date('2005-10-01'));
        $orderItem1 = ObjectHelper::createOrderItem($order1);
        $orderItem2 = ObjectHelper::createOrderItem($order2);

        $package1 = ObjectHelper::createPackageServiceFromType(Service::TYPE_PACKAGE_PRIVACY, 1315, $this->company, $orderItem1, new Date('2005-01-01'), new Date('2005-12-31'));
        $package2 = ObjectHelper::createPackageServiceFromType(Service::TYPE_PACKAGE_PRIVACY, 1315, $this->company, $orderItem2);
        $duration = $package2->getProduct()->getDuration();

        $this->relatedServiceProvider->getRelatedService($this->company, $package2->getProduct(), $order2->getDtc())->willReturn($package1);
        $this->serviceService->isFormationServiceForCompany($package2)->willReturn(FALSE);
        $this->serviceService->getCompanyNonExpiredServicesOn($this->company, $package2->getOrder()->getDtc())->willReturn([$package1]);
        $this->serviceService->setServiceDates($package2, new Date('2006-01-01'), new Date("2006-01-01 {$duration} -1 day"))->shouldBeCalled();

        $this->activate($package2);
    }

    function it_can_activate_package_as_renewal_bought_during_overdue_period()
    {
        $order1 = ObjectHelper::createOrder($this->customer, 1, new Date('2005-01-01'));
        $order2 = ObjectHelper::createOrder($this->customer, 1, new Date('2006-01-28'));
        $orderItem1 = ObjectHelper::createOrderItem($order1);
        $orderItem2 = ObjectHelper::createOrderItem($order2);

        $package1 = ObjectHelper::createPackageServiceFromType(Service::TYPE_PACKAGE_PRIVACY, 1315, $this->company, $orderItem1, new Date('2005-01-01'), new Date('2005-12-31'));
        $package2 = ObjectHelper::createPackageServiceFromType(Service::TYPE_PACKAGE_PRIVACY, 1315, $this->company, $orderItem2);
        $duration = $package2->getProduct()->getDuration();

        $this->relatedServiceProvider->getRelatedService($this->company, $package2->getProduct(), $order2->getDtc())->willReturn($package1);
        $this->serviceService->isFormationServiceForCompany($package2)->willReturn(FALSE);
        $this->serviceService->getCompanyNonExpiredServicesOn($this->company, $package2->getOrder()->getDtc())->willReturn([$package1]);
        $this->serviceService->setServiceDates($package2, new Date('2006-01-01'), new Date("2006-01-01 {$duration} -1 day"))->shouldBeCalled();

        $this->activate($package2);
    }

    function it_can_upgrade_and_activate_package_to_package_during_active_period()
    {
        $order1 = ObjectHelper::createOrder($this->customer, 1, new Date('2005-01-01'));
        $order2 = ObjectHelper::createOrder($this->customer, 1, new Date('2005-10-01'));
        $orderItem1 = ObjectHelper::createOrderItem($order1);
        $orderItem2 = ObjectHelper::createOrderItem($order2);

        $package1 = ObjectHelper::createPackageServiceFromType(Service::TYPE_PACKAGE_PRIVACY, 1315, $this->company, $orderItem1, new Date('2005-01-01'), new Date('2005-12-31'));
        $package2 = ObjectHelper::createPackageServiceFromType(Service::TYPE_PACKAGE_COMPREHENSIVE_ULTIMATE, 1316, $this->company, $orderItem2);
        $duration = $package2->getProduct()->getDuration();

        $this->relatedServiceProvider->getRelatedService($this->company, $package2->getProduct(), $order2->getDtc())->willReturn(NULL);
        $this->serviceService->isFormationServiceForCompany($package2)->willReturn(FALSE);
        $this->serviceService->getCompanyNonExpiredServicesOn($this->company, $package2->getOrder()->getDtc())->willReturn([$package1]);
        $this->serviceService->markAsUpgraded($package1)->shouldBeCalled();
        $this->serviceService->setServiceDates($package2, new Date('2005-10-01'), new Date("2005-10-01 {$duration} -1 day"))->shouldBeCalled();

        $this->activate($package2);
    }

    function it_can_upgrade_and_activate_package_to_package_during_overdue_period()
    {
        $order1 = ObjectHelper::createOrder($this->customer, 1, new Date('2005-01-01'));
        $order2 = ObjectHelper::createOrder($this->customer, 1, new Date('2006-01-28'));
        $orderItem1 = ObjectHelper::createOrderItem($order1);
        $orderItem2 = ObjectHelper::createOrderItem($order2);

        $package1 = ObjectHelper::createPackageServiceFromType(Service::TYPE_PACKAGE_PRIVACY, 1315, $this->company, $orderItem1, new Date('2005-01-01'), new Date('2005-12-31'));
        $package2 = ObjectHelper::createPackageServiceFromType(Service::TYPE_PACKAGE_COMPREHENSIVE_ULTIMATE, 1316, $this->company, $orderItem2);
        $duration = $package2->getProduct()->getDuration();

        $this->relatedServiceProvider->getRelatedService($this->company, $package2->getProduct(), $order2->getDtc())->willReturn($package1);
        $this->serviceService->isFormationServiceForCompany($package2)->willReturn(FALSE);
        $this->serviceService->getCompanyNonExpiredServicesOn($this->company, $package2->getOrder()->getDtc())->willReturn([$package1]);
        $this->serviceService->markAsUpgraded($package1)->shouldBeCalled();
        $this->serviceService->setServiceDates($package2, new Date('2006-01-01'), new Date("2006-01-01 {$duration} -1 day"))->shouldBeCalled();

        $this->activate($package2);
    }

    function it_can_downgrade_and_activate_package_to_package_during_overdue_period()
    {
        $order1 = ObjectHelper::createOrder($this->customer, 1, new Date('2005-01-01'));
        $order2 = ObjectHelper::createOrder($this->customer, 1, new Date('2006-01-28'));
        $orderItem1 = ObjectHelper::createOrderItem($order1);
        $orderItem2 = ObjectHelper::createOrderItem($order2);

        $package1 = ObjectHelper::createPackageServiceFromType(Service::TYPE_PACKAGE_COMPREHENSIVE_ULTIMATE, 1316, $this->company, $orderItem1, new Date('2005-01-01'), new Date('2005-12-31'));
        $package2 = ObjectHelper::createPackageServiceFromType(Service::TYPE_PACKAGE_PRIVACY, 1315, $this->company, $orderItem2);
        $duration = $package2->getProduct()->getDuration();

        $this->relatedServiceProvider->getRelatedService($this->company, $package2->getProduct(), $order2->getDtc())->willReturn($package1);
        $this->serviceService->isFormationServiceForCompany($package2)->willReturn(FALSE);
        $this->serviceService->getCompanyNonExpiredServicesOn($this->company, $package2->getOrder()->getDtc())->willReturn([$package1]);
        $this->serviceService->markAsDowngraded($package1)->shouldBeCalled();
        $this->serviceService->setServiceDates($package2, new Date('2006-01-01'), new Date("2006-01-01 {$duration} -1 day"))->shouldBeCalled();

        $this->activate($package2);
    }

    function it_does_not_allow_to_downgrade_package_during_active_period()
    {
        $order1 = ObjectHelper::createOrder($this->customer, 1, new Date('2005-01-01'));
        $order2 = ObjectHelper::createOrder($this->customer, 1, new Date('2005-06-01'));
        $orderItem1 = ObjectHelper::createOrderItem($order1);
        $orderItem2 = ObjectHelper::createOrderItem($order2);

        $package1 = ObjectHelper::createPackageServiceFromType(Service::TYPE_PACKAGE_COMPREHENSIVE_ULTIMATE, 1316, $this->company, $orderItem1, new Date('2005-01-01'), new Date('2005-12-31'));
        $package2 = ObjectHelper::createPackageServiceFromType(Service::TYPE_PACKAGE_PRIVACY, 1315, $this->company, $orderItem2);
        $duration = $package2->getProduct()->getDuration();

        $this->relatedServiceProvider->getRelatedService($this->company, $package2->getProduct(), $order2->getDtc())->willReturn($package1);
        $this->serviceService->isFormationServiceForCompany($package2)->willReturn(FALSE);
        $this->serviceService->getCompanyNonExpiredServicesOn($this->company, $package2->getOrder()->getDtc())->willReturn([$package1]);
        $this->serviceService->setServiceDates($package2, new Date('2006-01-01'), new Date("2006-01-01 {$duration} -1 day"))->shouldNotBeCalled();

        $this->shouldThrow(ServiceActivationException::packageToProductDowngrade($this->company, $package1, $package2))->duringActivate($package2);
    }

    function it_can_upgrade_and_activate_product_to_package_during_active_period()
    {
        $order1 = ObjectHelper::createOrder($this->customer, 1, new Date('2005-01-01'));
        $order2 = ObjectHelper::createOrder($this->customer, 1, new Date('2005-06-01'));
        $orderItem1 = ObjectHelper::createOrderItem($order1);
        $orderItem2 = ObjectHelper::createOrderItem($order2);

        $sa1 = ObjectHelper::createServiceFromType(Service::TYPE_SERVICE_ADDRESS, 475, $this->company, $orderItem1, new Date('2005-01-01'), new Date('2005-12-31'));
        $sa2 = ObjectHelper::createServiceFromType(Service::TYPE_SERVICE_ADDRESS, 475, $this->company, $orderItem1, new Date('2006-01-01'), new Date('2006-12-31'));
        $sa3 = ObjectHelper::createServiceFromType(Service::TYPE_SERVICE_ADDRESS, 475, $this->company, $orderItem1, new Date('2007-01-01'), new Date('2007-12-31'));
        $ro1 = ObjectHelper::createServiceFromType(Service::TYPE_REGISTERED_OFFICE, 165, $this->company, $orderItem1, new Date('2005-01-01'), new Date('2005-12-31'));
        $ro2 = ObjectHelper::createServiceFromType(Service::TYPE_REGISTERED_OFFICE, 165, $this->company, $orderItem1, new Date('2006-01-01'), new Date('2006-12-31'));
        $ro3 = ObjectHelper::createServiceFromType(Service::TYPE_REGISTERED_OFFICE, 165, $this->company, $orderItem1, new Date('2006-01-01'), new Date('2006-12-31'));
        $package = ObjectHelper::createPackageServiceFromType(Service::TYPE_PACKAGE_PRIVACY, 1315, $this->company, $orderItem2);
        $duration = $package->getProduct()->getDuration();

        $this->serviceService->isFormationServiceForCompany($package)->willReturn(FALSE);
        $this->serviceService->getCompanyNonExpiredServicesOn($this->company, $package->getOrder()->getDtc())->willReturn([$sa1, $sa2, $sa3, $ro1, $ro2, $ro3]);
        $this->serviceService->markAsUpgraded($sa1)->shouldBeCalled();
        $this->serviceService->markAsUpgraded($sa2)->shouldBeCalled();
        $this->serviceService->markAsUpgraded($sa3)->shouldBeCalled();
        $this->serviceService->markAsUpgraded($ro1)->shouldBeCalled();
        $this->serviceService->markAsUpgraded($ro2)->shouldBeCalled();
        $this->serviceService->markAsUpgraded($ro3)->shouldBeCalled();
        $this->serviceService->setServiceDates($package, new Date('2005-06-01'), new Date("2005-06-01 {$duration} -1 day"))->shouldBeCalled();

        $this->activate($package);
    }

    function it_can_disable_all_renewals_in_the_future_when_upgrading_package()
    {
        $order1 = ObjectHelper::createOrder($this->customer, 1, new Date('2005-01-01'));
        $order2 = ObjectHelper::createOrder($this->customer, 1, new Date('2005-06-01'));
        $orderItem1 = ObjectHelper::createOrderItem($order1);
        $orderItem2 = ObjectHelper::createOrderItem($order2);

        $package1 = ObjectHelper::createPackageServiceFromType(Service::TYPE_PACKAGE_PRIVACY, 1315, $this->company, $orderItem1, new Date('2005-01-01'), new Date('2005-12-31'));
        $package2 = ObjectHelper::createPackageServiceFromType(Service::TYPE_PACKAGE_PRIVACY, 1315, $this->company, $orderItem1, new Date('2006-01-01'), new Date('2006-12-31'));
        $package3 = ObjectHelper::createPackageServiceFromType(Service::TYPE_PACKAGE_PRIVACY, 1315, $this->company, $orderItem1, new Date('2007-01-01'), new Date('2007-12-31'));
        $package4 = ObjectHelper::createPackageServiceFromType(Service::TYPE_PACKAGE_COMPREHENSIVE_ULTIMATE, 1316, $this->company, $orderItem2, NULL, NULL);
        $duration = $package4->getProduct()->getDuration();

        $this->serviceService->isFormationServiceForCompany($package4)->willReturn(FALSE);
        $this->serviceService->getCompanyNonExpiredServicesOn($this->company, $package4->getOrder()->getDtc())->willReturn([$package1, $package2, $package3]);
        $this->serviceService->markAsUpgraded($package1)->shouldBeCalled();
        $this->serviceService->markAsUpgraded($package2)->shouldBeCalled();
        $this->serviceService->markAsUpgraded($package3)->shouldBeCalled();
        $this->serviceService->setServiceDates($package4, new Date('2005-06-01'), new Date("2005-06-01 {$duration} -1 day"))->shouldBeCalled();

        $this->activate($package4);
    }
}
