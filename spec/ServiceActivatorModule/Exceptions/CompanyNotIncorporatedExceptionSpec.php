<?php

namespace spec\ServiceActivatorModule\Exceptions;

use Exception;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ServiceActivatorModule\Exceptions\CompanyNotIncorporatedException;

class CompanyNotIncorporatedExceptionSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(CompanyNotIncorporatedException::class);
        $this->shouldHaveType(Exception::class);
    }
}
