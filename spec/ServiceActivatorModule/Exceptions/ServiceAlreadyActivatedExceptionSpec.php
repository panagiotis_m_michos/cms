<?php

namespace spec\ServiceActivatorModule\Exceptions;

use Exception;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ServiceActivatorModule\Exceptions\ServiceAlreadyActivatedException;

class ServiceAlreadyActivatedExceptionSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(ServiceAlreadyActivatedException::class);
        $this->shouldHaveType(Exception::class);
    }
}
