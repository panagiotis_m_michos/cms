<?php

namespace spec\ServiceActivatorModule;

use BasketProduct;
use DateTime;
use Entities\Service;
use Package;
use PhpSpec\ObjectBehavior;
use ServiceActivatorModule\ProductRelationshipChecker;

/**
 * @mixin ProductRelationshipChecker
 */
class ProductRelationshipCheckerSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(ProductRelationshipChecker::class);
    }

    function it_should_consider_standalone_service_to_package_as_upgrade(
        BasketProduct $registeredOffice,
        Package $privacyPackage
    )
    {
        $registeredOffice->isPackage()->willReturn(FALSE);
        $registeredOffice->isProduct()->willReturn(TRUE);
        $registeredOffice->getServiceTypeId()->willReturn(Service::TYPE_REGISTERED_OFFICE);
        $registeredOffice->isPackagePrivacyType()->willReturn(TRUE);
        $privacyPackage->isProduct()->willReturn(FALSE);
        $privacyPackage->isPackage()->willReturn(TRUE);
        $privacyPackage->isPackagePrivacyType()->willReturn(TRUE);
        $privacyPackage->isPackageComprehensiveType()->willReturn(FALSE);
        $privacyPackage->getTypesForPackageProducts()->willReturn([Service::TYPE_REGISTERED_OFFICE]);

        $this->isUpgrade($registeredOffice, $privacyPackage)->shouldBe(TRUE);
        $this->isDowngrade($registeredOffice, $privacyPackage)->shouldBe(FALSE);
    }

    function it_should_consider_package_to_standalone_service_as_downgrade(
        Package $comprehensivePackage,
        BasketProduct $serviceAddress
    )
    {
        $comprehensivePackage->isProduct()->willReturn(FALSE);
        $comprehensivePackage->isPackage()->willReturn(TRUE);
        $comprehensivePackage->getServiceTypeId()->willReturn(Service::TYPE_PACKAGE_COMPREHENSIVE_ULTIMATE);
        $comprehensivePackage->isPackageComprehensiveType()->willReturn(TRUE);
        $comprehensivePackage->getTypesForPackageProducts()->willReturn([Service::TYPE_SERVICE_ADDRESS]);
        $serviceAddress->isPackage()->willReturn(FALSE);
        $serviceAddress->isProduct()->willReturn(TRUE);
        $serviceAddress->getServiceTypeId()->willReturn(Service::TYPE_SERVICE_ADDRESS);
        $serviceAddress->isPackagePrivacyType()->willReturn(FALSE);

        $this->isDowngrade($comprehensivePackage, $serviceAddress)->shouldBe(TRUE);
        $this->isUpgrade($comprehensivePackage, $serviceAddress)->shouldBe(FALSE);
    }

    function it_should_consider_privacy_to_comprehensive_as_upgrade(
        Package $privacyPackage,
        Package $comprehensivePackage
    )
    {
        $privacyPackage->isProduct()->willReturn(FALSE);
        $privacyPackage->isPackage()->willReturn(TRUE);
        $privacyPackage->getServiceTypeId()->willReturn(Service::TYPE_PACKAGE_PRIVACY);
        $privacyPackage->isPackagePrivacyType()->willReturn(TRUE);
        $privacyPackage->isPackageComprehensiveType()->willReturn(FALSE);
        $comprehensivePackage->isProduct()->willReturn(FALSE);
        $comprehensivePackage->isPackage()->willReturn(TRUE);
        $comprehensivePackage->getServiceTypeId()->willReturn(Service::TYPE_PACKAGE_COMPREHENSIVE_ULTIMATE);
        $comprehensivePackage->isPackageComprehensiveType()->willReturn(TRUE);

        $this->isUpgrade($privacyPackage, $comprehensivePackage)->shouldBe(TRUE);
        $this->isDowngrade($privacyPackage, $comprehensivePackage)->shouldBe(FALSE);
    }

    function it_should_consider_comprehensive_to_privacy_as_downgrade(
        Package $privacyPackage,
        Package $comprehensivePackage
    )
    {
        $comprehensivePackage->isProduct()->willReturn(FALSE);
        $comprehensivePackage->isPackage()->willReturn(TRUE);
        $comprehensivePackage->getServiceTypeId()->willReturn(Service::TYPE_PACKAGE_COMPREHENSIVE_ULTIMATE);
        $comprehensivePackage->isPackageComprehensiveType()->willReturn(TRUE);
        $comprehensivePackage->isPackagePrivacyType()->willReturn(FALSE);
        $privacyPackage->isProduct()->willReturn(FALSE);
        $privacyPackage->isPackage()->willReturn(TRUE);
        $privacyPackage->getServiceTypeId()->willReturn(Service::TYPE_PACKAGE_PRIVACY);
        $privacyPackage->isPackagePrivacyType()->willReturn(TRUE);

        $this->isDowngrade($comprehensivePackage, $privacyPackage)->shouldBe(TRUE);
        $this->isUpgrade($comprehensivePackage, $privacyPackage)->shouldBe(FALSE);
    }

    function it_should_consider_same_product_as_equal(
        BasketProduct $registeredOffice1,
        BasketProduct $registeredOffice2
    )
    {
        $registeredOffice1->getServiceTypeId()->willReturn(Service::TYPE_REGISTERED_OFFICE);
        $registeredOffice2->getServiceTypeId()->willReturn(Service::TYPE_REGISTERED_OFFICE);

        $this->isEqual($registeredOffice1, $registeredOffice2)->shouldBe(TRUE);
    }

    function it_can_decide_if_products_are_downgradable(
        Service $fromService,
        Package $fromPackage,
        Package $toPackage
    )
    {
        $orderDate = new DateTime;

        $fromPackage->isPackage()->willReturn(TRUE);
        $fromPackage->getServiceTypeId()->willReturn(Service::TYPE_PACKAGE_COMPREHENSIVE_ULTIMATE);
        $fromPackage->isPackageComprehensiveType()->willReturn(TRUE);
        $fromService->getProduct()->willReturn($fromPackage);
        $fromService->isOverdueOn($orderDate)->willReturn(TRUE);
        $fromService->isActiveOn($orderDate)->willReturn(FALSE);
        $toPackage->getServiceTypeId()->willReturn(Service::TYPE_PACKAGE_PRIVACY);
        $toPackage->isPackagePrivacyType()->willReturn(TRUE);

        $this->isDowngradableOn($fromService, $toPackage, $orderDate)->shouldBe(TRUE);
    }

    function it_can_decide_if_downgrade_is_forbidden(
        Service $fromService,
        Package $fromPackage,
        Package $toPackage
    )
    {
        $orderDate = new DateTime;

        $fromPackage->isPackage()->willReturn(TRUE);
        $fromPackage->getServiceTypeId()->willReturn(Service::TYPE_PACKAGE_COMPREHENSIVE_ULTIMATE);
        $fromPackage->isPackageComprehensiveType()->willReturn(TRUE);
        $fromService->getProduct()->willReturn($fromPackage);
        $fromService->isOverdueOn($orderDate)->willReturn(FALSE);
        $fromService->isActiveOn($orderDate)->willReturn(TRUE);
        $toPackage->getServiceTypeId()->willReturn(Service::TYPE_PACKAGE_PRIVACY);
        $toPackage->isPackagePrivacyType()->willReturn(TRUE);

        $this->isDowngradeForbiddenOn($fromService, $toPackage, $orderDate)->shouldBe(TRUE);
    }

    function it_can_allow_renewing_limited_by_guarantee_with_registered_office(
        Service $fromService,
        Package $fromPackage,
        BasketProduct $registeredOffice
    )
    {
        $orderDate = new DateTime;

        $fromPackage->isPackage()->willReturn(TRUE);
        $fromPackage->getServiceTypeId()->willReturn(Service::TYPE_REGISTERED_OFFICE);
        $fromPackage->isPackageComprehensiveType()->willReturn(FALSE);
        $fromPackage->getTypesForPackageProducts()->willReturn([Service::TYPE_REGISTERED_OFFICE]);

        $fromService->getProduct()->willReturn($fromPackage);
        $fromService->isOverdueOn($orderDate)->willReturn(FALSE);
        $fromService->isActiveOn($orderDate)->willReturn(TRUE);

        $registeredOffice->isPackage()->willReturn(FALSE);
        $registeredOffice->isProduct()->willReturn(TRUE);
        $registeredOffice->getServiceTypeId()->willReturn(Service::TYPE_REGISTERED_OFFICE);
        $registeredOffice->isPackagePrivacyType()->willReturn(TRUE);

        $this->isDowngradeForbiddenOn($fromService, $registeredOffice, $orderDate)->shouldBe(FALSE);
    }
}
