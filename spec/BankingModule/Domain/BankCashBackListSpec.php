<?php

namespace spec\BankingModule\Domain;

use BankingModule\Domain\BankCashBack;
use IteratorAggregate;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class BankCashBackListSpec extends ObjectBehavior
{

    function it_is_initializable()
    {
        $this->shouldHaveType('BankingModule\Domain\BankCashBackList');
        $this->shouldHaveType(IteratorAggregate::class);
    }

    function it_should_be_able_to_add_bank()
    {
        $this->addBank(new BankCashBack('test', 23, TRUE));
    }

    function it_should_be_able_to_check_if_bank_is_available()
    {
        $this->hasAvailableBank('test')->shouldBe(FALSE);
        $this->addBank(new BankCashBack('test', 23, FALSE));
        $this->hasAvailableBank('test')->shouldBe(TRUE);
    }

    function it_should_provide_check_for_all_available_banks()
    {
        $this->hasAvailableBanks()->shouldBe(FALSE);
        $this->addBank(new BankCashBack('test1', 23, TRUE));
        $this->hasAvailableBanks()->shouldBe(FALSE);
        $this->addBank(new BankCashBack('test2', 23, FALSE));
        $this->hasAvailableBanks()->shouldBe(TRUE);
    }

    function it_should_provide_check_for_applied_banks()
    {
        $this->hasAppliedBanks()->shouldBe(FALSE);
        $this->addBank(new BankCashBack('test1', 23, FALSE));
        $this->hasAppliedBanks()->shouldBe(FALSE);
        $this->addBank(new BankCashBack('test2', 23, TRUE));
        $this->hasAppliedBanks()->shouldBe(TRUE);
    }
}
