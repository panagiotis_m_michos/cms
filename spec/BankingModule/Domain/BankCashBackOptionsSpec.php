<?php

namespace spec\BankingModule\Domain;

use BankingModule\Domain\BankCashBack;
use BankingModule\Domain\BankCashBackList;
use Entities\Company;
use Entities\Customer;
use FormModule\Country;
use Package;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class BankCashBackOptionsSpec extends ObjectBehavior
{
    function let(Package $package)
    {
        $company = new Company(new Customer('test', 'test'), 'test');
        $this->beConstructedWith(new BankCashBackList(), $company, 20, $package);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('BankingModule\Domain\BankCashBackOptions');
    }

    function it_should_have_cash_back_allowed(Package $package)
    {
        $package->bankingEnabled = TRUE;
        $company = $this->getCompany();
        $this->beConstructedWith(new BankCashBackList(), $company, 20, $package);
        $this->isCashBackAllowed()->shouldBe(TRUE);
    }

    function it_should_have_cash_back_allowed_for_imported_companies()
    {
        $company = $this->getCompany();
        $this->beConstructedWith(new BankCashBackList(), $company, 20);
        $this->isCashBackAllowed()->shouldBe(TRUE);
    }

    function it_should_not_have_cash_back_allowed_for_non_uk_customer(Package $package)
    {
        $package->bankingEnabled = TRUE;
        $company = $this->getCompany('Austria');
        $this->beConstructedWith(new BankCashBackList(), $company, 20, $package);
        $this->isCashBackAllowed()->shouldBe(FALSE);
    }

    function it_should_be_able_to_apply_for_cash_back(Package $package)
    {
        $package->bankingEnabled = TRUE;
        $company = $this->getCompany();
        $bankList = new BankCashBackList();
        $bankList->addBank(new BankCashBack('test', 30, FALSE));
        $this->beConstructedWith($bankList, $company, 20, $package);
        $this->canApplyForCashBack()->shouldBe(TRUE);
    }

    function it_should_not_be_able_to_apply_for_cash_back_if_there_are_not_offers(Package $package)
    {
        $package->bankingEnabled = TRUE;
        $company = $this->getCompany();
        $bankList = new BankCashBackList();
        $this->beConstructedWith($bankList, $company, 20, $package);
        $this->canApplyForCashBack()->shouldBe(FALSE);
    }

    function it_should_be_able_to_show_bank_offer(Package $package)
    {
        $package->bankingEnabled = TRUE;
        $company = $this->getCompany();
        $bankList = new BankCashBackList();
        $bankList->addBank(new BankCashBack('test1', 30, TRUE));
        $bankList->addBank(new BankCashBack('test2', 30, FALSE));
        $this->beConstructedWith($bankList, $company, 20, $package);
        $this->canShowBankOffer()->shouldBe(TRUE);
    }

    function it_should_not_show_bank_offer_if_all_offers_are_used(Package $package)
    {
        $package->bankingEnabled = TRUE;
        $company = $this->getCompany();
        $bankList = new BankCashBackList();
        $bankList->addBank(new BankCashBack('test1', 30, TRUE));
        $bankList->addBank(new BankCashBack('test2', 30, TRUE));
        $this->beConstructedWith($bankList, $company, 20, $package);
        $this->canShowBankOffer()->shouldBe(FALSE);
    }

    function it_should_be_able_to_dismiss_bank_offer(Package $package)
    {
        $package->bankingEnabled = TRUE;
        $company = $this->getCompany();
        $bankList = new BankCashBackList();
        $bankList->addBank(new BankCashBack('test1', 30, FALSE));
        $bankList->addBank(new BankCashBack('test2', 30, TRUE));
        $this->beConstructedWith($bankList, $company, 20, $package);
        $this->canDismissBankOffer()->shouldBe(TRUE);
    }


    function it_should_be_able_to_apply_for_cash_back_for_specific_bank(Package $package)
    {
        $package->bankingEnabled = TRUE;
        $company = $this->getCompany();
        $bankList = new BankCashBackList();
        $bankList->addBank(new BankCashBack('test1', 30, FALSE));
        $bankList->addBank(new BankCashBack('test2', 30, TRUE));
        $this->beConstructedWith($bankList, $company, 20, $package);
        $this->canApplyForCashBackWith('test1')->shouldBe(TRUE);
        $this->canApplyForCashBackWith('test2')->shouldBe(FALSE);
        $this->canApplyForCashBackWith('test3')->shouldBe(FALSE);
    }

    function it_should_provide_applied_bank_names(Package $package)
    {
        $package->bankingEnabled = TRUE;
        $company = $this->getCompany();
        $bankList = new BankCashBackList();
        $bankList->addBank(new BankCashBack('TEST_BANK_1', 50, FALSE));
        $bankList->addBank(new BankCashBack('TEST_BANK_2', 50, TRUE));
        $bankList->addBank(new BankCashBack('TEST_BANK_3', 50, TRUE));
        $this->beConstructedWith($bankList, $company, 20);
        $this->getAppliedBankNames()->shouldBe(['TEST_BANK_2', 'TEST_BANK_3']);
    }

    private function getCompany($country = NULL)
    {
        $customer = new Customer('test', 'test');
        if ($country) {
            $customer->setCountryFromString($country);
        } else {
            $customer->setCountryId(223);
        }
        $company = new Company($customer, 'test');
        $company->setCompanyNumber('test');
        return $company;
    }
}
