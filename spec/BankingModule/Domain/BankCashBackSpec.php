<?php

namespace spec\BankingModule\Domain;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class BankCashBackSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('GRAMEEN', 23, FALSE);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('BankingModule\Domain\BankCashBack');
        $this->shouldHaveType('JsonSerializable');
    }

    function it_should_be_able_to_cast_to_string()
    {
        $this->__toString()->shouldBe('GRAMEEN');
    }
}
