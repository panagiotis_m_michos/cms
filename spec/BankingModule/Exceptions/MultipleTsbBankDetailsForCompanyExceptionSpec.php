<?php

namespace spec\BankingModule\Exceptions;

use BankingModule\Exceptions\MultipleTsbBankDetailsForCompanyException;
use Exceptions\Technical\TechnicalAbstract;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin MultipleTsbBankDetailsForCompanyException
 */
class MultipleTsbBankDetailsForCompanyExceptionSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('12345678');
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(TechnicalAbstract::class);
        $this->shouldHaveType(MultipleTsbBankDetailsForCompanyException::class);
    }
}
