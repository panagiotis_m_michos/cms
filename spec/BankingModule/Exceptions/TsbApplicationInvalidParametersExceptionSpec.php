<?php

namespace spec\BankingModule\Exceptions;

use BankingModule\Exceptions\TsbApplicationInvalidParametersException;
use Exceptions\Technical\TechnicalAbstract;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin TsbApplicationInvalidParametersException
 */
class TsbApplicationInvalidParametersExceptionSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith(1, 1, '12345678');
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(TechnicalAbstract::class);
        $this->shouldHaveType(TsbApplicationInvalidParametersException::class);
    }
}
