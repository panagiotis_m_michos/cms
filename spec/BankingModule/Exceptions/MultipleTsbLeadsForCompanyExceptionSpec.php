<?php

namespace spec\BankingModule\Exceptions;

use BankingModule\Exceptions\MultipleTsbLeadsForCompanyException;
use Exceptions\Business\Business_Abstract;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin MultipleTsbLeadsForCompanyException
 */
class MultipleTsbLeadsForCompanyExceptionSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('12345678');
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Business_Abstract::class);
        $this->shouldHaveType(MultipleTsbLeadsForCompanyException::class);
    }
}
