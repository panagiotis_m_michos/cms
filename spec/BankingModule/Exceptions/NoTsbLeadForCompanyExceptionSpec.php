<?php

namespace spec\BankingModule\Exceptions;

use BankingModule\Exceptions\NoTsbLeadForCompanyException;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin NoTsbLeadForCompanyException
 */
class NoTsbLeadForCompanyExceptionSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('12345678');
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('Exceptions\Business\Business_Abstract');
        $this->shouldHaveType('BankingModule\Exceptions\NoTsbLeadForCompanyException');
    }
}
