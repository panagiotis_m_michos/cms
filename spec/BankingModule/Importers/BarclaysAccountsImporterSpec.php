<?php

namespace spec\BankingModule\Importers;

use BankingModule\BankingService;
use BankingModule\Config\EventLocator;
use BankingModule\Events\BarclaysAccountImportedEvent;
use BankingModule\Importers\BarclaysAccountsImporter;
use CsvParserModule\CsvParser;
use CsvParserModule\Exceptions\IncorrectStructureException;
use Entities\Company;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Services\CompanyService;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Utils\File;

/**
 * @mixin BarclaysAccountsImporter
 */
class BarclaysAccountsImporterSpec extends ObjectBehavior
{
    /**
     * @var CsvParser
     */
    private $parser;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var BankingService
     */
    private $bankingService;

    /**
     * @var EventDispatcher
     */
    private $eventDispatcher;

    function let(
        CsvParser $parser,
        CompanyService $companyService,
        BankingService $bankingService,
        EventDispatcher $eventDispatcher
    )
    {
        $this->parser = $parser;
        $this->companyService = $companyService;
        $this->bankingService = $bankingService;
        $this->eventDispatcher = $eventDispatcher;
        $this->beConstructedWith($this->parser, $this->companyService, $this->bankingService, $this->eventDispatcher);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('BankingModule\Importers\BarclaysAccountsImporter');
    }

    function it_can_throw_when_invalid_file_format(File $file)
    {
        $this->parser->parse($file)->willThrow(new IncorrectStructureException);

        $this->shouldThrow('CsvParserModule\Exceptions\ImportException')->during('import', [$file]);
    }

    function it_will_skip_non_existing_companies(File $file)
    {
        $this->parser->parse($file)->willReturn([['10/10/2015', '123', '20/10/2015']]);
        $this->companyService->getCompanyByCompanyNumber('00000123')->willReturn(NULL);
        $this->eventDispatcher->dispatch(EventLocator::BARCLAYS_ACCOUNT_IMPORTED)->shouldNotBeCalled();

        $this->shouldThrow('CsvParserModule\Exceptions\ImportException')->during('import', [$file]);
    }

    function it_can_import_file(File $file, Company $company1, Company $company2)
    {
        $this->parser->parse($file)->willReturn(
            [['10/10/2015', '123', '20/10/2015'], ['1/1/2014', '456', '24/12/2014']]
        );
        $this->companyService->getCompanyByCompanyNumber('00000123')->willReturn($company1);
        $this->companyService->getCompanyByCompanyNumber('00000456')->willReturn($company2);

        $company1Event = new BarclaysAccountImportedEvent($company1->getWrappedObject(), '10/10/2015', '20/10/2015');
        $company2Event = new BarclaysAccountImportedEvent($company2->getWrappedObject(), '1/1/2014', '24/12/2014');
        $this->eventDispatcher->dispatch(EventLocator::BARCLAYS_ACCOUNT_IMPORTED, $company1Event)->shouldBeCalled();
        $this->eventDispatcher->dispatch(EventLocator::BARCLAYS_ACCOUNT_IMPORTED, $company2Event)->shouldBeCalled();

        $this->import($file);
    }
}
