<?php

namespace spec\BankingModule\Importers;

use BankingModule\BankingService;
use BankingModule\Config\EventLocator;
use BankingModule\Events\TsbAccountImportedEvent;
use BankingModule\Importers\TsbAccountsImporter;
use CsvParserModule\CsvParser;
use CsvParserModule\Exceptions\IncorrectStructureException;
use Entities\Company;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Services\CompanyService;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Utils\File;

/**
 * @mixin TsbAccountsImporter
 */
class TsbAccountsImporterSpec extends ObjectBehavior
{
    /**
     * @var CsvParser
     */
    private $parser;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var BankingService
     */
    private $bankingService;

    /**
     * @var EventDispatcher
     */
    private $eventDispatcher;

    function let(
        CsvParser $parser,
        CompanyService $companyService,
        BankingService $bankingService,
        EventDispatcher $eventDispatcher
    )
    {
        $this->parser = $parser;
        $this->companyService = $companyService;
        $this->bankingService = $bankingService;
        $this->eventDispatcher = $eventDispatcher;
        $this->beConstructedWith($this->parser, $this->companyService, $this->bankingService, $this->eventDispatcher);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('BankingModule\Importers\TsbAccountsImporter');
    }

    function it_can_throw_when_invalid_file_format(File $file)
    {
        $this->parser->parse($file)->willThrow(new IncorrectStructureException);

        $this->shouldThrow('CsvParserModule\Exceptions\ImportException')->during('import', [$file]);
    }

    function it_will_skip_non_existing_companies(File $file)
    {
        $this->parser->parse($file)->willReturn([['123']]);
        $this->companyService->getCompanyByCompanyNumber('00000123')->willReturn(NULL);
        $this->eventDispatcher->dispatch(EventLocator::TSB_ACCOUNT_IMPORTED, Argument::type('BankingModule\Events\TsbAccountImportedEvent'))->shouldNotBeCalled();

        $this->shouldThrow('CsvParserModule\Exceptions\ImportException')->during('import', [$file]);
    }

    function it_can_import_file(File $file, Company $company1, Company $company2)
    {
        $this->parser->parse($file)->willReturn([['123'], ['456']]);
        $this->companyService->getCompanyByCompanyNumber('00000123')->willReturn($company1);
        $this->companyService->getCompanyByCompanyNumber('00000456')->willReturn($company2);

        $this->bankingService->markTsbAccountOpened($company1->getWrappedObject())->shouldBeCalled();
        $this->bankingService->markTsbAccountOpened($company2->getWrappedObject())->shouldBeCalled();

        $company1Event = new TsbAccountImportedEvent($company1->getWrappedObject());
        $company2Event = new TsbAccountImportedEvent($company2->getWrappedObject());
        $this->eventDispatcher->dispatch(EventLocator::TSB_ACCOUNT_IMPORTED, $company1Event)->shouldBeCalled();
        $this->eventDispatcher->dispatch(EventLocator::TSB_ACCOUNT_IMPORTED, $company2Event)->shouldBeCalled();

        $this->import($file);
    }
}
