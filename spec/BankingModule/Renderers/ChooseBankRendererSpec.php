<?php

namespace spec\BankingModule\Renderers;

use BankingModule\Renderers\ChooseBankRenderer;
use BasketProduct;
use Entities\Company;
use FTemplate;
use PhpSpec\ObjectBehavior;
use Product;
use Prophecy\Argument;
use Services\CashbackService;
use Services\NodeService;

/**
 * @mixin ChooseBankRenderer
 */
class ChooseBankRendererSpec extends ObjectBehavior
{
    /**
     * @var FTemplate
     */
    private $template;

    /**
     * @var NodeService
     */
    private $nodeService;

    /**
     * @var CashbackService
     */
    private $cashbackService;

    function let(FTemplate $template, NodeService $nodeService, CashbackService $cashbackService)
    {
        $this->template = $template;
        $this->nodeService = $nodeService;
        $this->cashbackService = $cashbackService;
        $this->beConstructedWith($this->template, $this->nodeService, $this->cashbackService);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(ChooseBankRenderer::class);
    }

    function it_can_render(Company $company, Product $product)
    {
        $productId = 1;
        $cashBackAmount = 10;
        $selected = 'TSB';
        $html = '<b>template</b>';

        $company->getProductId()->willReturn($productId);
        $this->nodeService->getProductById($productId)->willReturn($product);
        $this->cashbackService->getCashBackAmountForCompany($company)->willReturn($cashBackAmount);
        $this->template->getHtml('Banking/chooseBank.tpl')->willReturn($html);
        $this->template->setVariables(Argument::type('array'))->shouldBeCalled();

        $this->getHtml($company, $selected)->shouldBe($html);
    }
}
