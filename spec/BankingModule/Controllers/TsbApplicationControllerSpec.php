<?php

namespace spec\BankingModule\Controllers;

use BankingModule\Controllers\TsbApplicationController;
use BankingModule\Exceptions\MultipleTsbBankDetailsForCompanyException;
use BankingModule\Exceptions\TsbApplicationInvalidParametersException;
use BankingModule\Facades\TsbApplicationFacade;
use Entities\Company;
use Entities\Customer;
use FTemplate;
use HomeControler;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Log\LoggerInterface;
use Services\ControllerHelper;
use Symfony\Component\HttpFoundation\RedirectResponse;
use ContentModule\VariableProviders\IVariableProvider;
use TemplateModule\Renderers\IRenderer;

/**
 * @mixin TsbApplicationController
 */
class TsbApplicationControllerSpec extends ObjectBehavior
{
    /**
     * @var IRenderer
     */
    private $renderer;

    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    /**
     * @var TsbApplicationFacade
     */
    private $tsbApplicationFacade;

    /**
     * @var LoggerInterface
     */
    private $logger;

    function let(
        IRenderer $renderer,
        ControllerHelper $controllerHelper,
        TsbApplicationFacade $tsbApplicationFacade,
        LoggerInterface $logger
    )
    {
        $this->renderer = $renderer;
        $this->controllerHelper = $controllerHelper;
        $this->tsbApplicationFacade = $tsbApplicationFacade;
        $this->logger = $logger;

        $this->beConstructedWith(
            $this->renderer,
            $this->controllerHelper,
            $this->tsbApplicationFacade,
            $this->logger
        );
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(TsbApplicationController::class);
    }

    function it_can_redirect_to_home_page_when_parameters_are_not_valid(Customer $customer, Company $company)
    {
        $companyNumber = '12312312';

        $this->controllerHelper->getLink(HomeControler::HOME_PAGE)->willReturn('/');
        $this->tsbApplicationFacade->addTsbBankDetails($customer, $company, $companyNumber)->willThrow(
            TsbApplicationInvalidParametersException::class
        );

        $this->addTsbBankDetails($customer, $company, $companyNumber)->shouldBeAnInstanceOf(RedirectResponse::class);
    }

    function it_can_log_error_in_case_of_multiple_tsb_bank_details(
        Customer $customer,
        Company $company,
        MultipleTsbBankDetailsForCompanyException $e
    )
    {
        $companyNumber = '12312312';

        $this->tsbApplicationFacade->addTsbBankDetails($customer, $company, $companyNumber)->willThrow(
            $e->getWrappedObject()
        );

        $this->logger->error($e)->shouldBeCalled();
        $this->renderer->render()->shouldBeCalled();

        $this->addTsbBankDetails($customer, $company, $companyNumber);
    }

    function it_can_add_tsb_bank_details(Customer $customer, Company $company)
    {
        $companyNumber = '12312312';

        $this->tsbApplicationFacade->addTsbBankDetails($customer, $company, $companyNumber)->shouldBeCalled();
        $this->renderer->render()->shouldBeCalled();

        $this->addTsbBankDetails($customer, $company, $companyNumber);
    }
}
