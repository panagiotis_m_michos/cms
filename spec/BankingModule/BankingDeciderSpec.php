<?php

namespace spec\BankingModule;

use BankingModule\BankingDecider;
use BankingModule\Domain\BankCashBack;
use BankingModule\Domain\BankCashBackList;
use BankingModule\Entities\CompanyCustomer;
use BankingModule\Repositories\BankingRepository;
use BasketProduct;
use Entities\Company;
use Entities\Customer;
use Package;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Services\NodeService;

/**
 * @mixin BankingDecider
 */
class BankingDeciderSpec extends ObjectBehavior
{
    /**
     * @var BankingRepository
     */
    private $bankingRepository;

    /**
     * @var NodeService
     */
    private $nodeService;

    function let(BankingRepository $bankingRepository, NodeService $nodeService)
    {
        $this->bankingRepository = $bankingRepository;
        $this->nodeService = $nodeService;
        $this->beConstructedWith($this->bankingRepository, $this->nodeService);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('BankingModule\BankingDecider');
    }

    function it_can_check_if_company_has_filled_details(Company $company)
    {
        $this->bankingRepository->hasDetailsFilled($company)->willReturn(TRUE);
        $this->hasDetailsFilled($company)->shouldReturn(TRUE);
    }

    function it_can_check_if_company_has_banking_enabled(Company $company, BasketProduct $basketProduct)
    {
        $company->getProductId()->willReturn(1);
        $basketProduct->bankingEnabled = TRUE;
        $this->nodeService->getProductById(1)->willReturn($basketProduct);
        $this->hasBankingEnabled($company)->shouldReturn(TRUE);
    }

    function it_can_check_if_company_has_bank_in_banking_options(Company $company, BasketProduct $basketProduct)
    {
        $company->getProductId()->willReturn(1);
        $basketProduct->bankingOptions = [2, 3];
        $this->nodeService->getProductById(1)->willReturn($basketProduct);
        $this->hasBankInOptions($company, 2)->shouldReturn(TRUE);
    }

    function it_should_produce_bank_options_checker_for_imported_company()
    {
        $company = new Company(new Customer('test', 'test'), 'test');
        $bankList = new BankCashBackList();
        //$bankList->addBank(new BankCashBack(CompanyCustomer::BANK_TYPE_BARCLAYS, CompanyCustomer::CASH_BACK_AMOUNT, TRUE));
        $bankList->addBank(new BankCashBack(CompanyCustomer::BANK_TYPE_TSB, CompanyCustomer::CASH_BACK_AMOUNT, FALSE));
        $bankList->addBank(new BankCashBack(CompanyCustomer::BANK_TYPE_CARD_ONE, 0, TRUE));

        $this->bankingRepository->getAppliedBanks($company)->willReturn([CompanyCustomer::BANK_TYPE_BARCLAYS, CompanyCustomer::BANK_TYPE_CARD_ONE]);

        $options = $this->getCashBackOptions($company);
        $options->getCashBackAmount()->shouldBe(CompanyCustomer::CASH_BACK_AMOUNT);
        $options->getList()->shouldBeLike($bankList);
    }

    function it_should_produce_bank_options_checker_for_company_formed(Package $package)
    {
        $bankList = new BankCashBackList();
        $bankList->addBank(new BankCashBack('test1', 30, TRUE));
        $bankList->addBank(new BankCashBack('test2', 30, FALSE));
        $company = new Company(new Customer('test', 'test'), 'test');
        $company->setProductId(1);

        $this->nodeService->getProductById(1, TRUE)->willReturn($package);
        $package->getCashBackAmount()->willReturn(30);
        $package->bankingOptions = ['test1', 'test2'];
        $this->bankingRepository->getAppliedBanks($company)->willReturn(['test1']);

        $options = $this->getCashBackOptions($company);
        $options->getCashBackAmount()->shouldBe(30);
        $options->getList()->shouldBeLike($bankList);
    }

}
