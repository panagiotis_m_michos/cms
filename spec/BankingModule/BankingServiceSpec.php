<?php

namespace spec\BankingModule;

use BankingModule\BankingService;
use BankingModule\Entities\CompanyCustomer;
use BankingModule\Entities\TsbLead;
use BankingModule\Exceptions\MultipleTsbBankDetailsForCompanyException;
use BankingModule\Repositories\BankingRepository;
use BankingModule\Repositories\TsbLeadRepository;
use Doctrine\ORM\NonUniqueResultException;
use Entities\Company;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin BankingService
 */
class BankingServiceSpec extends ObjectBehavior
{
    /**
     * @var BankingRepository
     */
    private $bankingRepository;

    /**
     * @var TsbLeadRepository
     */
    private $tsbLeadRepository;

    function let(BankingRepository $bankingRepository, TsbLeadRepository $tsbLeadRepository)
    {
        $this->bankingRepository = $bankingRepository;
        $this->tsbLeadRepository = $tsbLeadRepository;
        $this->beConstructedWith($this->bankingRepository, $this->tsbLeadRepository);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('BankingModule\BankingService');
    }

    function it_can_save_details(CompanyCustomer $companyCustomer)
    {
        $this->bankingRepository->saveEntity($companyCustomer)->shouldBeCalled();
        $this->saveDetails($companyCustomer);
    }

    function it_can_save_tsb_lead(TsbLead $tsbLead)
    {
        $this->tsbLeadRepository->saveEntity($tsbLead)->shouldBeCalled();
        $this->saveTsbLead($tsbLead);
    }

    function it_can_get_tsb_bank_details_by_company(Company $company, CompanyCustomer $companyCustomer)
    {
        $this->bankingRepository->getTsbBankDetailsByCompany($company)->willReturn($companyCustomer);
        $this->getTsbBankDetailsByCompany($company)->shouldReturn($companyCustomer);
    }

    function it_throws_exception_in_case_of_multiple_tsb_bank_details_by_company(Company $company)
    {
        $this->bankingRepository->getTsbBankDetailsByCompany($company)->willThrow(NonUniqueResultException::class);
        $this->shouldThrow(MultipleTsbBankDetailsForCompanyException::class)->during('getTsbBankDetailsByCompany', [$company]);
    }

    function it_can_get_companies_to_send_tsb_leads(CompanyCustomer $companyCustomer)
    {
        $this->bankingRepository->getCompaniesToSendTsbLeads()->willReturn([$companyCustomer]);
        $this->getCompaniesToSendTsbLeads()->shouldReturn([$companyCustomer]);
    }

    function it_can_get_companies_to_send_first_tsb_reminder(TsbLead $tsbLead)
    {
        $this->tsbLeadRepository->getLeadsToSendFirstTsbReminder()->willReturn([$tsbLead]);
        $this->getLeadsToSendFirstTsbReminder()->shouldReturn([$tsbLead]);
    }

    function it_can_get_companies_to_send_second_tsb_reminder(TsbLead $tsbLead)
    {
        $this->tsbLeadRepository->getLeadsToSendSecondTsbReminder()->willReturn([$tsbLead]);
        $this->getLeadsToSendSecondTsbReminder()->shouldReturn([$tsbLead]);
    }

}
