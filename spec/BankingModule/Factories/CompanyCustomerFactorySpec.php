<?php

namespace spec\BankingModule\Factories;

use BankingModule\Entities\CompanyCustomer;
use BankingModule\Factories\CompanyCustomerFactory;
use Entities\Company;
use Entities\Customer;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Utils\Date;

/**
 * @mixin CompanyCustomerFactory
 */
class CompanyCustomerFactorySpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(CompanyCustomerFactory::class);
    }

    function it_can_create_from_company(Company $company, Customer $customer)
    {
        $email = 'email@address.com';
        $titleId = 1;
        $firstName = 'First';
        $lastName = 'Last';
        $phone = '123';
        $bankTypeId = CompanyCustomer::BANK_TYPE_TSB;
        $contactDate = new Date('-1 day');
        $consent = CompanyCustomer::CONSENT_MY_CLIENTS_DETAILS;

        $company->getCustomer()->willReturn($customer);
        $customer->getEmail()->willReturn($email);
        $customer->getTitleId()->willReturn($titleId);
        $customer->getFirstName()->willReturn($firstName);
        $customer->getLastName()->willReturn($lastName);
        $customer->getPhone()->willReturn($phone);

        $companyCustomer = $this->createFromCompany($company, $bankTypeId, $contactDate, $consent);

        $companyCustomer->getWholesaler()->shouldBe($customer);
        $companyCustomer->getBankTypeId()->shouldBe($bankTypeId);
        $companyCustomer->getPreferredContactDate()->shouldBe($contactDate);
        $companyCustomer->getConsent()->shouldBe($consent);
        $companyCustomer->getEmail()->shouldBe($email);
        $companyCustomer->getTitleId()->shouldBe($titleId);
        $companyCustomer->getFirstName()->shouldBe($firstName);
        $companyCustomer->getLastName()->shouldBe($lastName);
        $companyCustomer->getPhone()->shouldBe($phone);
        $companyCustomer->getAdditionalPhone()->shouldBe($phone);
    }
}
