<?php

namespace spec\BankingModule\Listeners;

use BankingModule\BankingService;
use BankingModule\Emailers\TsbApplicationEmailer;
use BankingModule\Entities\CompanyCustomer;
use BankingModule\Listeners\SendTsbApplicationEmailListener;
use Dispatcher\Events\CompanyEvent;
use Entities\Company;
use EventLocator;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Log\LoggerInterface;

/**
 * @mixin SendTsbApplicationEmailListener
 */
class SendTsbApplicationEmailListenerSpec extends ObjectBehavior
{
    /**
     * @var BankingService
     */
    private $bankingService;

    /**
     * @var TsbApplicationEmailer
     */
    private $tsbApplicationEmailer;

    /**
     * @var LoggerInterface
     */
    private $logger;

    function let(BankingService $bankingService, TsbApplicationEmailer $tsbApplicationEmailer, LoggerInterface $logger)
    {
        $this->bankingService = $bankingService;
        $this->tsbApplicationEmailer = $tsbApplicationEmailer;
        $this->logger = $logger;
        $this->beConstructedWith($this->bankingService, $this->tsbApplicationEmailer, $this->logger);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(SendTsbApplicationEmailListener::class);
    }

    function it_can_get_subscribed_events()
    {
        $this::getSubscribedEvents()->shouldReturn([EventLocator::COMPANY_INCORPORATED => 'onCompanyIncorporated']);
    }

    function it_will_send_mails_on_company_incorporation(CompanyEvent $companyEvent, Company $company, CompanyCustomer $bankDetails)
    {
        $companyEvent->getCompany()->willReturn($company);
        $this->bankingService->getTsbBankDetailsByCompany($company)->willReturn($bankDetails);

        $this->tsbApplicationEmailer->sendOnlineApplication($bankDetails)->shouldBeCalled();

        $this->onCompanyIncorporated($companyEvent);
    }
}
