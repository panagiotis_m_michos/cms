<?php

namespace spec\BankingModule\Commands;

use BankingModule\BankingService;
use BankingModule\Commands\SendTsbMonthlySummaryCommand;
use BankingModule\Emailers\TsbLeadsEmailer;
use BankingModule\Entities\CompanyCustomer;
use BankingModule\Exporters\TsbLeadsExporter;
use Cron\INotifier;
use DateTime;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Log\LoggerInterface;

/**
 * @mixin SendTsbMonthlySummaryCommand
 */
class SendTsbMonthlySummaryCommandSpec extends ObjectBehavior
{
    /**
     * @var INotifier
     */
    private $notifier;

    /**
     * @var BankingService
     */
    private $bankingService;

    /**
     * @var TsbLeadsExporter
     */
    private $exporter;

    /**
     * @var TsbLeadsEmailer
     */
    private $emailer;

    /**
     * @var string
     */
    private $attachmentName;

    function let(
        LoggerInterface $logger,
        INotifier $notifier,
        BankingService $bankingService,
        TsbLeadsExporter $exporter,
        TsbLeadsEmailer $emailer
    )
    {

        $this->bankingService = $bankingService;
        $this->exporter = $exporter;
        $this->emailer = $emailer;
        $this->attachmentName = 'monthly-summary.xls';
        $this->notifier = $notifier;
        $this->beConstructedWith(
            $logger,
            $this->notifier,
            $bankingService,
            $exporter,
            $this->emailer,
            $this->attachmentName
        );
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('BankingModule\Commands\SendTsbMonthlySummaryCommand');
        $this->shouldHaveType('Cron\Commands\CommandAbstract');
    }

    function it_can_execute(CompanyCustomer $details1, CompanyCustomer $details2)
    {
        $start = new DateTime('first day of previous month 00:00:00');
        $stop = new DateTime('last day of previous month 23:59:59');

        $xlsContents = 'xls';
        $details = [$details1, $details2];

        $this->bankingService->getTsbLeadsDetailsSentBetween($start, $stop)->willReturn($details);
        $this->exporter->export($details)->willReturn($xlsContents);
        $this->emailer->sendMonthlySummary($xlsContents, $this->attachmentName)->shouldBeCalled();
        $this->notifier->triggerSuccess(Argument::any(), Argument::any(), Argument::any())->shouldBeCalled();

        $this->execute();
    }
}
