<?php

namespace spec\BankingModule\Commands;

use BankingModule\BankingService;
use BankingModule\Commands\SendTsbLeadsCommand;
use BankingModule\Emailers\TsbLeadsEmailer;
use BankingModule\Entities\CompanyCustomer;
use BankingModule\Entities\TsbLead;
use BankingModule\Exporters\TsbLeadsExporter;
use Cron\INotifier;
use Entities\Company;
use Gaufrette\File;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Log\LoggerInterface;

/**
 * @mixin SendTsbLeadsCommand
 */
class SendTsbLeadsCommandSpec extends ObjectBehavior
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var INotifier
     */
    private $notifier;

    /**
     * @var BankingService
     */
    private $bankingService;

    /**
     * @var TsbLeadsExporter
     */
    private $tsbLeadsExporter;

    /**
     * @var TsbLeadsEmailer
     */
    private $tsbLeadsEmailer;

    /**
     * @var string
     */
    private $attachmentName;

    function let(
        LoggerInterface $logger,
        INotifier $notifier,
        BankingService $bankingService,
        TsbLeadsExporter $tsbLeadsExporter,
        TsbLeadsEmailer $tsbLeadsEmailer
    )
    {
        $this->logger = $logger;
        $this->notifier = $notifier;
        $this->bankingService = $bankingService;
        $this->tsbLeadsExporter = $tsbLeadsExporter;
        $this->tsbLeadsEmailer = $tsbLeadsEmailer;
        $this->attachmentName = 'daily.xls';

        $this->beConstructedWith(
            $this->logger,
            $this->notifier,
            $this->bankingService,
            $this->tsbLeadsExporter,
            $this->tsbLeadsEmailer,
            $this->attachmentName
        );
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('BankingModule\Commands\SendTsbLeadsCommand');
        $this->shouldHaveType('Cron\Commands\CommandAbstract');
    }

    function it_can_execute(CompanyCustomer $companyCustomer, Company $company)
    {
        $xlsContents = 'xls';
        $companyCustomer->getCompany()->willReturn($company);
        $company->getProductId()->willReturn(1);

        $this->bankingService->getCompaniesToSendTsbLeads()->willReturn([$companyCustomer]);
        $this->tsbLeadsExporter->export([$companyCustomer])->willReturn($xlsContents);
        $this->tsbLeadsEmailer->sendLeads($xlsContents, $this->attachmentName)->shouldBeCalled();
        $this->bankingService
            ->saveTsbLead(
                Argument::that(
                    function (TsbLead $tsbLead) use ($companyCustomer) {
                        return $tsbLead->getCompany()->getProductId() === 1;
                    }
                )
            )
            ->shouldBeCalled();


        $this->execute();
    }
}
