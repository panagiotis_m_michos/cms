<?php

namespace spec\BankingModule\Commands;

use BankingModule\BankingService;
use BankingModule\Commands\SendTsbApplicationRemindersCommand;
use BankingModule\Config\EventLocator;
use BankingModule\Emailers\TsbApplicationEmailer;
use BankingModule\Entities\CompanyCustomer;
use BankingModule\Entities\TsbLead;
use Cron\Commands\CommandAbstract;
use Cron\INotifier;
use DateTime;
use Entities\Company;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Log\LoggerInterface;
use Services\EventService;

/**
 * @mixin SendTsbApplicationRemindersCommand
 */
class SendTsbApplicationRemindersCommandSpec extends ObjectBehavior
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var INotifier
     */
    private $notifier;

    /**
     * @var BankingService
     */
    private $bankingService;

    /**
     * @var TsbApplicationEmailer
     */
    private $tsbApplicationEmailer;

    /**
     * @var EventService
     */
    private $eventService;

    function let(
        LoggerInterface $logger,
        INotifier $notifier,
        BankingService $bankingService,
        TsbApplicationEmailer $tsbApplicationEmailer,
        EventService $eventService
    )
    {
        $this->bankingService = $bankingService;
        $this->tsbApplicationEmailer = $tsbApplicationEmailer;
        $this->eventService = $eventService;
        $this->logger = $logger;
        $this->notifier = $notifier;
        $this->beConstructedWith(
            $this->logger,
            $this->notifier,
            $this->bankingService,
            $this->tsbApplicationEmailer,
            $this->eventService
        );
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(SendTsbApplicationRemindersCommand::class);
        $this->shouldHaveType(CommandAbstract::class);
    }

    function it_should_send_reminders(
        TsbLead $tsbLead1,
        TsbLead $tsbLead2,
        Company $company1,
        Company $company2,
        CompanyCustomer $companyCustomer1,
        CompanyCustomer $companyCustomer2
    )
    {
        $tsbLead1->getCompany()->willReturn($company1);
        $tsbLead2->getCompany()->willReturn($company2);
        $tsbLead1->getId()->willReturn(1);
        $tsbLead2->getId()->willReturn(2);
        $tsbLead1->getDtc()->willReturn(new DateTime);
        $tsbLead2->getDtc()->willReturn(new DateTime);

        $this->bankingService->getTsbBankDetailsByCompany($company1)->willReturn($companyCustomer1);
        $this->bankingService->getTsbBankDetailsByCompany($company2)->willReturn($companyCustomer2);

        $this->bankingService->getLeadsToSendFirstTsbReminder()->willReturn([$tsbLead1]);
        $this->tsbApplicationEmailer->sendOnlineApplicationReminder($companyCustomer1)->shouldBeCalled();
        $this->eventService->notify(EventLocator::TSB_APPLICATION_FIRST_REMINDER, 1);

        $this->bankingService->getLeadsToSendSecondTsbReminder()->willReturn([$tsbLead2]);
        $this->tsbApplicationEmailer->sendOnlineApplicationReminder($companyCustomer2)->shouldBeCalled();
        $this->eventService->notify(EventLocator::TSB_APPLICATION_SECOND_REMINDER, 2);

        $this->execute();
    }
}
