<?php

namespace spec\BankingModule\Commands;

use BankingModule\BankingService;
use BankingModule\Commands\SendTsbWeeklySummaryCommand;
use BankingModule\Emailers\TsbLeadsEmailer;
use BankingModule\Entities\CompanyCustomer;
use BankingModule\Exporters\TsbLeadsExporter;
use Cron\INotifier;
use DateTime;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Log\LoggerInterface;

/**
 * @mixin SendTsbWeeklySummaryCommand
 */
class SendTsbWeeklySummaryCommandSpec extends ObjectBehavior
{
    /**
     * @var INotifier
     */
    private $notifier;

    /**
     * @var BankingService
     */
    private $bankingService;

    /**
     * @var TsbLeadsExporter
     */
    private $exporter;

    /**
     * @var TsbLeadsEmailer
     */
    private $emailer;

    /**
     * @var string
     */
    private $attachmentName;

    function let(
        LoggerInterface $logger,
        INotifier $notifier,
        BankingService $bankingService,
        TsbLeadsExporter $exporter,
        TsbLeadsEmailer $emailer
    )
    {
        $this->bankingService = $bankingService;
        $this->exporter = $exporter;
        $this->emailer = $emailer;
        $this->attachmentName = 'weekly-summary.xls';
        $this->notifier = $notifier;
        $this->beConstructedWith(
            $logger,
            $this->notifier,
            $bankingService,
            $exporter,
            $this->emailer,
            $this->attachmentName
        );
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('BankingModule\Commands\SendTsbWeeklySummaryCommand');
        $this->shouldHaveType('Cron\Commands\CommandAbstract');
    }

    function it_can_execute(CompanyCustomer $details1, CompanyCustomer $details2)
    {
        $stop = new DateTime('last sunday 23:59:59');
        $start = clone $stop;
        $start = $start->modify('previous monday 00:00:00');

        $xlsContents = 'xls';
        $details = [$details1, $details2];

        $this->bankingService->getTsbLeadsDetailsSentBetween($start, $stop)->willReturn($details);
        $this->exporter->export($details)->willReturn($xlsContents);
        $this->emailer->sendWeeklySummary($xlsContents, $this->attachmentName)->shouldBeCalled();
        $this->notifier->triggerSuccess(Argument::any(), Argument::any(), Argument::any())->shouldBeCalled();

        $this->execute();
    }
}
