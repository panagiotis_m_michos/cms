<?php

namespace spec\BankingModule\Repositories;

use BankingModule\Repositories\BankingRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin BankingRepository
 */
class BankingRepositorySpec extends ObjectBehavior
{
    function let(EntityManager $entityManager, ClassMetadata $metadata)
    {
        $this->beConstructedWith($entityManager, $metadata);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('BankingModule\Repositories\BankingRepository');
        $this->shouldHaveType('Repositories\BaseRepository_Abstract');
    }
}
