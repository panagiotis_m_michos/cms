<?php

namespace spec\BankingModule\Repositories;

use BankingModule\Repositories\TsbLeadRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin TsbLeadRepository
 */
class TsbLeadRepositorySpec extends ObjectBehavior
{
    function let(EntityManager $entityManager, ClassMetadata $metadata)
    {
        $this->beConstructedWith($entityManager, $metadata);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('BankingModule\Repositories\TsbLeadRepository');
        $this->shouldHaveType('Repositories\BaseRepository_Abstract');
    }
}
