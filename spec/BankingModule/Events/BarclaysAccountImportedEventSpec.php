<?php

namespace spec\BankingModule\Events;

use BankingModule\Events\BarclaysAccountImportedEvent;
use Entities\Cashback;
use Entities\Company;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Utils\Date;

/**
 * @mixin BarclaysAccountImportedEvent
 */
class BarclaysAccountImportedEventSpec extends ObjectBehavior
{
    /**
     * @var Company
     */
    private $company;

    function let(Company $company)
    {
        $this->company = $company;
        $this->beConstructedWith($this->company, '24/12/2015', '11/12/2014');
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('BankingModule\Events\BarclaysAccountImportedEvent');
        $this->shouldImplement('BankingModule\Events\IAccountImportedEvent');
    }

    function it_can_get_company()
    {
        $this->getCompany()->shouldReturn($this->company);
    }

    function it_can_get_bank_type()
    {
        $this->getBankType()->shouldReturn(Cashback::BANK_TYPE_BARCLAYS);
    }

    function it_can_get_date_lead_sent()
    {
        $this->getDateLeadSent()->shouldBeLike(new Date('2015-12-24'));
    }

    function it_can_get_date_account_opened()
    {
        $this->getDateAccountOpened()->shouldBeLike(new Date('2014-12-11'));
    }
}
