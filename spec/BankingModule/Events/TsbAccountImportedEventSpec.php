<?php

namespace spec\BankingModule\Events;

use BankingModule\Events\TsbAccountImportedEvent;
use Entities\Cashback;
use Entities\Company;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Utils\Date;

/**
 * @mixin TsbAccountImportedEvent
 */
class TsbAccountImportedEventSpec extends ObjectBehavior
{
    /**
     * @var Company
     */
    private $company;

    function let(Company $company)
    {
        $this->company = $company;
        $this->beConstructedWith($this->company);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('BankingModule\Events\TsbAccountImportedEvent');
        $this->shouldImplement('BankingModule\Events\IAccountImportedEvent');
    }

    function it_can_get_company()
    {
        $this->getCompany()->shouldReturn($this->company);
    }

    function it_can_get_bank_type()
    {
        $this->getBankType()->shouldReturn(Cashback::BANK_TYPE_TSB);
    }

    function it_can_get_date_lead_sent()
    {
        $this->getDateLeadSent()->shouldReturn(NULL);
    }

    function it_can_get_date_account_opened()
    {
        $this->getDateAccountOpened()->shouldReturn(NULL);
    }
}
