<?php

namespace spec\BankingModule\Emailers;

use BankingModule\Emailers\TsbApplicationEmailer;
use BankingModule\Entities\CompanyCustomer;
use FEmail;
use FEmailFactory;
use IEmailService;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin TsbApplicationEmailer
 */
class TsbApplicationEmailerSpec extends ObjectBehavior
{
    /**
     * @var FEmailFactory
     */
    private $emailFactory;

    /**
     * @var IEmailService
     */
    private $emailService;

    function let(IEmailService $emailService, FEmailFactory $emailFactory)
    {
        $this->emailService = $emailService;
        $this->emailFactory = $emailFactory;
        $this->beConstructedWith($this->emailService, $this->emailFactory);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('BankingModule\Emailers\TsbApplicationEmailer');
    }

    function it_can_send_online_application(CompanyCustomer $companyCustomer, FEmail $email)
    {
        $this->emailFactory->getById(TsbApplicationEmailer::EMAIL_TSB_BANKING)->willReturn($email);
        $this->emailService->send($email)->shouldBeCalled();

        $this->sendOnlineApplication($companyCustomer);
    }

    function it_can_send_tsb_reminder(CompanyCustomer $companyCustomer, FEmail $email)
    {
        $this->emailFactory->getById(TsbApplicationEmailer::EMAIL_TSB_BANKING_REMINDER)->willReturn($email);
        $this->emailService->send($email)->shouldBeCalled();

        $this->sendOnlineApplicationReminder($companyCustomer);
    }
}
