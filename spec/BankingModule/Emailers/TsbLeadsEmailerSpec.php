<?php

namespace spec\BankingModule\Emailers;

use BankingModule\Emailers\TsbLeadsEmailer;
use FEmail;
use FEmailFactory;
use Gaufrette\File;
use IEmailService;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin TsbLeadsEmailer
 */
class TsbLeadsEmailerSpec extends ObjectBehavior
{
    /**
     * @var FEmailFactory
     */
    private $emailFactory;

    /**
     * @var IEmailService
     */
    private $emailService;

    function let(IEmailService $emailService, FEmailFactory $emailFactory)
    {
        $this->emailService = $emailService;
        $this->emailFactory = $emailFactory;
        $this->beConstructedWith($this->emailService, $this->emailFactory);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('BankingModule\Emailers\TsbLeadsEmailer');
    }

    function it_can_send_leads(FEmail $email)
    {
        $xlsContents = 'content';
        $attachmentName = 'daily.xls';
        $this->emailFactory->getById(TsbLeadsEmailer::EMAIL_TSB_LEADS)->willReturn($email);
        $email->addAttachment($attachmentName, $xlsContents)->shouldBeCalled();
        $this->emailService->send($email)->shouldBeCalled();

        $this->sendLeads($xlsContents, $attachmentName);
    }

    function it_can_send_weekly_summary(FEmail $email)
    {
        $xlsContents = 'content';
        $attachmentName = 'weekly.xls';
        $this->emailFactory->getById(TsbLeadsEmailer::EMAIL_TSB_WEEKLY_SUMMARY)->willReturn($email);
        $email->addAttachment($attachmentName, $xlsContents)->shouldBeCalled();
        $this->emailService->send($email)->shouldBeCalled();

        $this->sendWeeklySummary($xlsContents, $attachmentName);
    }

    function it_can_send_monthly_summary(FEmail $email)
    {
        $xlsContents = 'content';
        $attachmentName = 'monthly.xls';
        $this->emailFactory->getById(TsbLeadsEmailer::EMAIL_TSB_MONTHLY_SUMMARY)->willReturn($email);
        $email->addAttachment($attachmentName, $xlsContents)->shouldBeCalled();
        $this->emailService->send($email)->shouldBeCalled();

        $this->sendMonthlySummary($xlsContents, $attachmentName);
    }
}
