<?php

namespace spec\BankingModule\Facades;

use BankingModule\BankingService;
use BankingModule\Emailers\TsbApplicationEmailer;
use BankingModule\Entities\CompanyCustomer;
use BankingModule\Exceptions\TsbApplicationInvalidParametersException;
use BankingModule\Facades\TsbApplicationFacade;
use BankingModule\Factories\CompanyCustomerFactory;
use Entities\Company;
use Entities\Customer;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin TsbApplicationFacade
 */
class TsbApplicationFacadeSpec extends ObjectBehavior
{
    /**
     * @var BankingService
     */
    private $bankingService;

    /**
     * @var CompanyCustomerFactory
     */
    private $companyCustomerFactory;

    /**
     * @var TsbApplicationEmailer
     */
    private $tsbApplicationEmailer;

    function let(
        BankingService $bankingService,
        CompanyCustomerFactory $companyCustomerFactory,
        TsbApplicationEmailer $tsbApplicationEmailer
    )
    {
        $this->bankingService = $bankingService;
        $this->companyCustomerFactory = $companyCustomerFactory;
        $this->tsbApplicationEmailer = $tsbApplicationEmailer;
        $this->beConstructedWith($this->bankingService, $this->companyCustomerFactory, $this->tsbApplicationEmailer);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(TsbApplicationFacade::class);
    }

    function it_throws_exception_if_parameters_are_not_valid(
        Customer $companyCustomer,
        Company $company,
        Customer $wrongCustomer
    )
    {
        $correctCompanyNumber = '12345678';
        $wrongCompanyNumber = '87654321';

        $wrongCustomer->getId()->willReturn(2);
        $wrongCustomer->isEqual($companyCustomer)->willReturn(FALSE);
        $companyCustomer->getId()->willReturn(1);
        $companyCustomer->isEqual($companyCustomer)->willReturn(TRUE);
        $company->getId()->willReturn(1);
        $company->getCustomer()->willReturn($companyCustomer);
        $company->getCompanyNumber()->willReturn($correctCompanyNumber);

        $this->shouldThrow(TsbApplicationInvalidParametersException::class)->duringAddTsbBankDetails($wrongCustomer, $company, $correctCompanyNumber);
        $this->shouldThrow(TsbApplicationInvalidParametersException::class)->duringAddTsbBankDetails($companyCustomer, $company, $wrongCompanyNumber);
    }

    function it_can_add_tsb_bank_details(Customer $customer, Company $company, CompanyCustomer $bankDetails)
    {
        $companyNumber = '12312312';

        $company->getCustomer()->willReturn($customer);
        $company->getCompanyNumber()->willReturn($companyNumber);
        $customer->isEqual($customer)->willReturn(TRUE);
        $bankDetails->isBankTsb()->willReturn(TRUE);

        $this->bankingService->getTsbBankDetailsByCompany($company)->willReturn(NULL);
        $this->companyCustomerFactory->createFromCompany($company, CompanyCustomer::BANK_TYPE_TSB)->willReturn($bankDetails);

        $this->bankingService->saveDetails($bankDetails)->shouldBeCalled();
        $this->tsbApplicationEmailer->sendOnlineApplication($bankDetails)->shouldBeCalled();

        $this->addTsbBankDetails($customer, $company, $companyNumber);
    }

    function it_will_not_add_tsb_bank_details_more_times(Customer $customer, Company $company, CompanyCustomer $bankDetails)
    {
        $companyNumber = '12312312';

        $company->getCustomer()->willReturn($customer);
        $company->getCompanyNumber()->willReturn($companyNumber);
        $customer->isEqual($customer)->willReturn(TRUE);
        $bankDetails->isBankTsb()->willReturn(TRUE);

        $this->bankingService->getTsbBankDetailsByCompany($company)->willReturn($bankDetails);
        $this->companyCustomerFactory->createFromCompany($company, CompanyCustomer::BANK_TYPE_TSB)->willReturn($bankDetails);

        $this->bankingService->saveDetails($bankDetails)->shouldNotBeCalled();
        $this->tsbApplicationEmailer->sendOnlineApplication($bankDetails)->shouldNotBeCalled();

        $this->addTsbBankDetails($customer, $company, $companyNumber);

    }
}
