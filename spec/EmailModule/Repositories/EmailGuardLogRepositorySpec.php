<?php

namespace spec\EmailModule\Repositories;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use EmailModule\Repositories\EmailGuardLogRepository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin EmailGuardLogRepository
 */
class EmailGuardLogRepositorySpec extends ObjectBehavior
{
    function let(EntityManager $entityManager, ClassMetadata $metadata)
    {
        $this->beConstructedWith($entityManager, $metadata);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('EmailModule\Repositories\EmailGuardLogRepository');
    }
}
