<?php

namespace spec\EmailModule\Emailers;

use EmailModule\Emailers\GuardedEmailService;
use EmailModule\Services\EmailGuardService;
use Entities\Customer;
use FEmail;
use IEmailService;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Log\LoggerInterface;

/**
 * @mixin GuardedEmailService
 */
class GuardedEmailServiceSpec extends ObjectBehavior
{
    /**
     * @var EmailGuardService
     */
    private $emailGuardService;

    /**
     * @var IEmailService
     */
    private $emailService;

    /**
     * @var LoggerInterface
     */
    private $loggerInterface;

    function let(IEmailService $emailService, EmailGuardService $emailGuardService, LoggerInterface $loggerInterface)
    {
        $this->emailGuardService = $emailGuardService;
        $this->emailService = $emailService;
        $this->loggerInterface = $loggerInterface;
        $this->beConstructedWith($this->emailService, $this->emailGuardService, $this->loggerInterface);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(IEmailService::class);
    }

    function it_can_send(FEmail $email, Customer $customer)
    {
        $this->emailGuardService->guard($email)->willReturn(TRUE);
        $this->emailService->send($email, $customer);

        $this->send($email, $customer);
    }

    function it_will_log_if_it_cant_send(FEmail $email, Customer $customer)
    {
        $this->emailGuardService->guard($email)->willReturn(FALSE);
        $this->loggerInterface->error(Argument::any(), Argument::any())->shouldBeCalled();

        $this->send($email, $customer);
    }
}
