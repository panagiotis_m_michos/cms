<?php

namespace spec\EmailModule\Emailers;

use EmailLogService;
use EmailModule\Emailers\LoggedEmailService;
use Entities\Customer;
use FEmail;
use IEmailService;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin LoggedEmailService
 */
class LoggedEmailServiceSpec extends ObjectBehavior
{
    /**
     * @var IEmailService
     */
    private $emailService;

    /**
     * @var EmailLogService
     */
    private $emailLogsService;

    function let(IEmailService $emailService,  EmailLogService $emailLogsService)
    {
        $this->emailService = $emailService;
        $this->emailLogsService = $emailLogsService;
        $this->beConstructedWith($this->emailService, $this->emailLogsService);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(IEmailService::class);
    }

    function it_can_send(FEmail $email, Customer $customer)
    {
        $this->emailService->send($email, $customer);
        $this->emailLogsService->logEmail($email, $customer);

        $this->send($email, $customer);
    }
}
