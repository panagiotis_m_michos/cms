<?php

namespace spec\EmailModule\Entities;

use EmailModule\Entities\EmailGuardLog;
use FEmail;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin EmailGuardLog
 */
class EmailGuardLogSpec extends ObjectBehavior
{
    function let()
    {
        $email = new FEmail();
        $this->beConstructedWith($email);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('EmailModule\Entities\EmailGuardLog');
    }
}
