<?php

namespace spec\EmailModule\Services;

use EmailModule\Entities\EmailGuardLog;
use EmailModule\Repositories\EmailGuardLogRepository;
use EmailModule\Services\EmailGuardService;
use FEmail;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin EmailGuardService
 */
class EmailGuardServiceSpec extends ObjectBehavior
{
    /**
     * @var EmailGuardLogRepository
     */
    private $repository;

    function let(EmailGuardLogRepository $guardLogRepository)
    {
        $this->repository = $guardLogRepository;
        $this->beConstructedWith($this->repository);
    }

    function it_can_pass()
    {
        $email = new FEmail();
        $emailGuardLog = new EmailGuardLog($email);
        $hash = $emailGuardLog->getEmailHash();

        $this->repository
            ->hadSameHashBefore($hash, Argument::any())
            ->willReturn(FALSE);

        $this->repository->saveEntity($emailGuardLog)->shouldBeCalled();
        $this->guard($email)->shouldReturn(TRUE);
    }

    function it_can_gurad()
    {
        $email = new FEmail();
        $emailGuardLog = new EmailGuardLog($email);
        $hash = $emailGuardLog->getEmailHash();

        $this->repository
            ->hadSameHashBefore($hash, Argument::any())
            ->willReturn(TRUE);

        $this->guard($email)->shouldReturn(FALSE);
    }
}
