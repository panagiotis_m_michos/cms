<?php

namespace spec\Emailers;

use Emailers\ServicesEmailer;
use Entities\Company;
use Entities\Customer;
use FEmail;
use IEmailService;
use Latte\Template;
use Models\View\Front\CompanyServicesEmailView;
use Models\View\Front\CompanyServicesView;
use Nette\Templating\FileTemplate;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\Routing\RouterInterface;

/**
 * @mixin ServicesEmailer
 */
class ServicesEmailerSpec extends ObjectBehavior
{
    /**
     * @var IEmailService
     */
    private $emailService;

    /**
     * @var FileTemplate
     */
    private $template;

    function let(IEmailService $emailService, FileTemplate $template, RouterInterface $router)
    {
        $this->template = $template;
        $this->emailService = $emailService;
        $this->beConstructedWith($this->emailService, $this->template, $router);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('Emailers\ServicesEmailer');
    }

    function it_can_send_email_expiration(
        CompanyServicesEmailView $companyServicesEmailView,
        FEmail $email,
        CompanyServicesView $companyServicesView,
        Customer $customer,
        Company $company
    )
    {
        $templatePath = 'template/path';
        $customerEmail = 'customer@email.com';

        $companyServicesEmailView->getCompanyServicesView()->willReturn($companyServicesView);
        $companyServicesView->getCompany()->willReturn($company);
        $company->getCustomer()->willReturn($customer);
        $company->getId()->willReturn(1);
        $customer->getEmail()->willReturn($customerEmail);

        $this->sendEmailExpiration($companyServicesEmailView, $email, $templatePath);

        $this->template->setFile(FRONT_TEMPLATES_DIR . $templatePath)->shouldBeCalled();
        $email->addTo($customerEmail)->shouldBeCalled();
        $this->emailService->send($email, $customer)->shouldBeCalled();
    }
}
