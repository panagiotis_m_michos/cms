<?php

namespace spec\CashBackModule\Exceptions;

use CashBackModule\Exceptions\CashBackAlreadyExistsException;
use Exceptions\Technical\TechnicalAbstract;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin CashBackAlreadyExistsException
 */
class CashBackAlreadyExistsExceptionSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('123', 'TSB');
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(TechnicalAbstract::class);
        $this->shouldHaveType(CashBackAlreadyExistsException::class);
    }
}
