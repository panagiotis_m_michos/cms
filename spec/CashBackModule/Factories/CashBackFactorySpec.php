<?php

namespace spec\CashBackModule\Factories;

use BankingModule\Events\IAccountImportedEvent;
use CashBackModule\Exceptions\CashBackAlreadyExistsException;
use CashBackModule\Factories\CashBackFactory;
use CashBackModule\Resolvers\CashBackDefaultsResolver;
use Entities\Cashback;
use Entities\Company;
use Entities\Customer;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Services\CashbackService;
use Services\CompanyService;
use Utils\Date;

/**
 * @mixin CashBackFactory
 */
class CashBackFactorySpec extends ObjectBehavior
{
    /**
     * @var CashbackService
     */
    private $cashBackService;

    /**
     * @var CashBackDefaultsResolver
     */
    private $defaultsResolver;

    function let(CashbackService $cashBackService, CashBackDefaultsResolver $defaultsResolver)
    {
        $this->cashBackService = $cashBackService;
        $this->defaultsResolver = $defaultsResolver;
        $this->beConstructedWith($this->cashBackService, $this->defaultsResolver);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(CashBackFactory::class);
    }

    function it_can_create_barclays_cash_back_from_event(IAccountImportedEvent $event, Company $company, Customer $customer)
    {
        $amount = 55;
        $bankTypeId = Cashback::BANK_TYPE_BARCLAYS;

        $company->getCustomer()->willReturn($customer);
        $this->cashBackService->getBankCashBackByCompany($company, $bankTypeId)->willReturn(NULL);
        $this->cashBackService->getCashBackAmountForCompany($company)->willReturn($amount);
        $this->defaultsResolver->getStatusId($customer)->willReturn(Cashback::STATUS_IMPORTED);
        $this->defaultsResolver->getPackageTypeId($company)->willReturn(Cashback::PACKAGE_TYPE_RETAIL);

        $event->getCompany()->willReturn($company);
        $event->getBankType()->willReturn($bankTypeId);
        $event->getDateLeadSent()->willReturn(new Date('2014-10-10'));
        $event->getDateAccountOpened()->willReturn(new Date('2015-10-24'));

        $cashBack = $this->createFromAccountImportedEvent($event);
        $cashBack->shouldBeAnInstanceOf(Cashback::class);
        $cashBack->getCompany()->shouldReturn($company);
        $cashBack->getAmount()->shouldReturn($amount);
        $cashBack->getBankTypeId()->shouldReturn($bankTypeId);
        $cashBack->getDateLeadSent()->format('Y-m-d')->shouldReturn('2014-10-10');
        $cashBack->getDateAccountOpened()->format('Y-m-d')->shouldReturn('2015-10-24');
        $cashBack->getStatusId()->shouldReturn(Cashback::STATUS_IMPORTED);
        $cashBack->getPackageTypeId()->shouldReturn(Cashback::PACKAGE_TYPE_RETAIL);
    }

    function it_will_throw_when_cash_back_already_exists(IAccountImportedEvent $event, Company $company, Cashback $cashback)
    {
        $bankTypeId = Cashback::BANK_TYPE_BARCLAYS;

        $this->cashBackService->getBankCashBackByCompany($company, $bankTypeId)->willReturn($cashback);
        $event->getCompany()->willReturn($company);
        $event->getBankType()->willReturn($bankTypeId);

        $this
            ->shouldThrow(CashBackAlreadyExistsException::class)
            ->during('createFromAccountImportedEvent', [$event]);
    }

    function it_can_create_barclays_cash_back(Company $company, Customer $customer)
    {
        $company->getCustomer()->willReturn($customer);

        $this->cashBackService->getCashBackAmountForCompany($company)->willReturn(50);
        $this->defaultsResolver->getStatusId($customer)->willReturn(Cashback::STATUS_IMPORTED);
        $this->defaultsResolver->getPackageTypeId($company)->willReturn(Cashback::PACKAGE_TYPE_RETAIL);

        $this->createBarclays($company, new Date('2016-01-01'), new Date('2016-05-05'))->shouldBeAnInstanceOf(Cashback::class);
    }
}
