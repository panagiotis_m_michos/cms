<?php

namespace spec\CashBackModule\Listeners;

use BankingModule\Config\EventLocator;
use BankingModule\Events\TsbAccountImportedEvent;
use CashBackModule\Exceptions\CashBackAlreadyExistsException;
use CashBackModule\Factories\CashBackFactory;
use CashBackModule\Listeners\CreateTsbCashBackListener;
use Entities\Cashback;
use Entities\Company;
use ILogger;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Log\LoggerInterface;
use Services\CashbackService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * @mixin CreateTsbCashBackListener
 */
class CreateTsbCashBackListenerSpec extends ObjectBehavior
{
    const NO_CASH_BACK = 0;
    const STANDARD_CASH_BACK = 50;

    /**
     * @var CashBackFactory
     */
    private $cashBackFactory;

    /**
     * @var CashbackService
     */
    private $cashBackService;

    /**
     * @var LoggerInterface
     */
    private $logger;

    function let(CashBackFactory $cashBackFactory, CashbackService $cashBackService, LoggerInterface $logger)
    {
        $this->cashBackFactory = $cashBackFactory;
        $this->cashBackService = $cashBackService;
        $this->logger = $logger;
        $this->beConstructedWith($this->cashBackFactory, $this->cashBackService, $this->logger);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(CreateTsbCashBackListener::class);
        $this->shouldImplement(EventSubscriberInterface::class);
    }

    function it_can_get_subscribed_events()
    {
        $this::getSubscribedEvents()->shouldReturn(
            [
                EventLocator::TSB_ACCOUNT_IMPORTED => 'onTsbAccountImported'
            ]
        );
    }

    function it_will_skip_creating_cash_back_for_products_with_zero_cash_back(TsbAccountImportedEvent $event, Company $company)
    {
        $event->getCompany()->willReturn($company);
        $this->cashBackService->getCashBackAmountForCompany($company)->willReturn(self::NO_CASH_BACK);

        $this->logger->info(Argument::type('string'))->shouldBeCalled();

        $this->onTsbAccountImported($event);
    }

    function it_will_skip_already_existing_cash_back(TsbAccountImportedEvent $event, Company $company, Cashback $cashBack)
    {
        $event->getCompany()->willReturn($company);
        $this->cashBackService->getCashBackAmountForCompany($company)->willReturn(self::STANDARD_CASH_BACK);
        $this->cashBackFactory->createFromAccountImportedEvent($event)->willThrow(CashBackAlreadyExistsException::class);

        $this->logger->info(Argument::type('string'))->shouldBeCalled();
        $this->cashBackService->saveCashback($cashBack)->shouldNotBeCalled();

        $this->onTsbAccountImported($event);
    }

    function it_can_create_cash_back_on_barclays_account_imported(TsbAccountImportedEvent $event, Cashback $cashBack, Company $company)
    {
        $event->getCompany()->willReturn($company);
        $cashBack->getCompany()->willReturn($company);
        $this->cashBackService->getCashBackAmountForCompany($company)->willReturn(self::STANDARD_CASH_BACK);
        $this->cashBackFactory->createFromAccountImportedEvent($event)->willReturn($cashBack);

        $this->cashBackService->saveCashback($cashBack)->shouldBeCalled();

        $this->onTsbAccountImported($event);
    }
}
