<?php

namespace spec\CashBackModule\Listeners;

use BankingModule\Config\EventLocator;
use BankingModule\Events\BarclaysAccountImportedEvent;
use CashBackModule\Exceptions\CashBackAlreadyExistsException;
use CashBackModule\Factories\CashBackFactory;
use CashBackModule\Listeners\CreateBarclaysCashBackListener;
use Entities\Cashback;
use Entities\Company;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Log\LoggerInterface;
use Services\CashbackService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * @mixin CreateBarclaysCashBackListener
 */
class CreateBarclaysCashBackListenerSpec extends ObjectBehavior
{
    const NO_CASH_BACK = 0;
    const STANDARD_CASH_BACK = 50;

    /**
     * @var CashBackFactory
     */
    private $cashBackFactory;

    /**
     * @var CashbackService
     */
    private $cashBackService;

    /**
     * @var LoggerInterface
     */
    private $logger;

    function let(CashBackFactory $cashBackFactory, CashbackService $cashBackService, LoggerInterface $logger)
    {
        $this->cashBackFactory = $cashBackFactory;
        $this->cashBackService = $cashBackService;
        $this->logger = $logger;
        $this->beConstructedWith($this->cashBackFactory, $this->cashBackService, $this->logger);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(CreateBarclaysCashBackListener::class);
        $this->shouldImplement(EventSubscriberInterface::class);
    }

    function it_can_get_subscribed_events()
    {
        $this::getSubscribedEvents()->shouldReturn(
            [
                EventLocator::BARCLAYS_ACCOUNT_IMPORTED => 'onBarclaysAccountImported'
            ]
        );
    }

    function it_will_skip_creating_cash_back_for_products_with_zero_cash_back(BarclaysAccountImportedEvent $event, Company $company)
    {
        $event->getCompany()->willReturn($company);
        $this->cashBackService->getCashBackAmountForCompany($company)->willReturn(self::NO_CASH_BACK);

        $this->logger->info(Argument::type('string'))->shouldBeCalled();

        $this->onBarclaysAccountImported($event);
    }

    function it_will_skip_non_importable_cash_back(BarclaysAccountImportedEvent $event, Cashback $cashBack, Company $company)
    {
        $event->getCompany()->willReturn($company);
        $cashBack->getCompany()->willReturn($company);

        $this->cashBackService->getCashBackAmountForCompany($company)->willReturn(self::STANDARD_CASH_BACK);
        $this->cashBackFactory->createFromAccountImportedEvent($event)->willReturn($cashBack);
        $this->cashBackService->isImportableForBarclays($cashBack)->willReturn(FALSE);
        $this->logger->info(Argument::type('string'))->shouldBeCalled();
        $this->cashBackService->saveCashback($cashBack)->shouldNotBeCalled();

        $this->onBarclaysAccountImported($event);
    }

    function it_will_skip_already_existing_cash_back(BarclaysAccountImportedEvent $event, Company $company, Cashback $cashBack)
    {
        $event->getCompany()->willReturn($company);

        $this->cashBackService->getCashBackAmountForCompany($company)->willReturn(self::STANDARD_CASH_BACK);
        $this->cashBackFactory->createFromAccountImportedEvent($event)->willThrow(CashBackAlreadyExistsException::class);
        $this->logger->info(Argument::type('string'))->shouldBeCalled();
        $this->cashBackService->saveCashback($cashBack)->shouldNotBeCalled();

        $this->onBarclaysAccountImported($event);
    }
    
    function it_can_create_cash_back_on_barclays_account_imported(BarclaysAccountImportedEvent $event, Cashback $cashBack, Company $company)
    {
        $event->getCompany()->willReturn($company);
        $cashBack->getCompany()->willReturn($company);

        $this->cashBackService->getCashBackAmountForCompany($company)->willReturn(50);
        $this->cashBackFactory->createFromAccountImportedEvent($event)->willReturn($cashBack);
        $this->cashBackService->isImportableForBarclays($cashBack)->willReturn(TRUE);
        $this->cashBackService->saveCashback($cashBack)->shouldBeCalled();
        
        $this->onBarclaysAccountImported($event);
    }
}
