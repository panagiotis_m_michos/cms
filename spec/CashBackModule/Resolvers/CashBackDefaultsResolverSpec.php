<?php

namespace spec\CashBackModule\Resolvers;

use CashBackModule\Resolvers\CashBackDefaultsResolver;
use Entities\Cashback;
use Entities\Company;
use Entities\Customer;
use Package;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Services\NodeService;
use WholesalePackage;

/**
 * @mixin CashBackDefaultsResolver
 */
class CashBackDefaultsResolverSpec extends ObjectBehavior
{
    /**
     * @var NodeService
     */
    private $nodeService;

    function let(NodeService $nodeService)
    {
        $this->nodeService = $nodeService;
        $this->beConstructedWith($this->nodeService);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('CashBackModule\Resolvers\CashBackDefaultsResolver');
    }

    function it_can_resolve_status_id(Customer $customer)
    {
        $customer->hasBankDetails()->willReturn(TRUE);

        $this->getStatusId($customer)->shouldReturn(Cashback::STATUS_ELIGIBLE);

        $customer->hasBankDetails()->willReturn(FALSE);
        $customer->getCashbackType()->willReturn(Cashback::CASHBACK_TYPE_CREDITS);

        $this->getStatusId($customer)->shouldReturn(Cashback::STATUS_ELIGIBLE);

        $customer->hasBankDetails()->willReturn(FALSE);
        $customer->getCashbackType()->willReturn(Cashback::CASHBACK_TYPE_BANK);

        $this->getStatusId($customer)->shouldReturn(Cashback::STATUS_IMPORTED);
    }

    function it_can_resolve_package_type_id(Company $company, Package $retailPackage, WholesalePackage $wholesalePackage)
    {
        $company->getProductId()->willReturn(1);
        $this->nodeService->getProductById(1)->willReturn($retailPackage);

        $this->getPackageTypeId($company)->shouldReturn(Cashback::PACKAGE_TYPE_RETAIL);

        $this->nodeService->getProductById(1)->willReturn($wholesalePackage);

        $this->getPackageTypeId($company)->shouldReturn(Cashback::PACKAGE_TYPE_WHOLESALE);
    }
}
