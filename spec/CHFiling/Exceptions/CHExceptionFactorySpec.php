<?php

namespace spec\CHFiling\Exceptions;

use CHFiling\Exceptions\CHExceptionFactory;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use stdClass;

/**
 * @mixin CHExceptionFactory
 */
class CHExceptionFactorySpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('CHFiling\Exceptions\CHExceptionFactory');
    }

    //function it_can_process_unknown_error()
    //{
    //    $emptyError = new stdClass();
    //    $exception = $this::createChExceptionFromXmlGwError($emptyError);
    //    $exception->shouldHaveType('CHFiling\Exceptions\UnknownCHException');
    //}
    //
    //function it_can_process_any_companies_house_error()
    //{
    //    $error = new stdClass();
    //    $error->Text = 'RandomText';
    //    $error->Number = 123456;
    //
    //    $exception = $this::createChExceptionFromXmlGwError($error);
    //    $exception->shouldHaveType('CHFiling\Exceptions\CHException');
    //    $exception->getMessage()->shouldBe($error->Text);
    //    $exception->getCode()->shouldBe($error->Number);
    //}
    //
    //function it_can_process_authentication_code_expired_error()
    //{
    //    $error = new stdClass();
    //    $error->Text = 'Authentication code expired';
    //    $error->Number = 123456;
    //
    //    $exception = $this::createChExceptionFromXmlGwError($error);
    //    $exception->shouldHaveType('CHFiling\Exceptions\AuthenticationCodeExpiredException');
    //    $exception->getMessage()->shouldBe($error->Text);
    //    $exception->getCode()->shouldBe($error->Number);
    //}
}
