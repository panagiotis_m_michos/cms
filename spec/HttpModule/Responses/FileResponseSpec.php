<?php

namespace spec\HttpModule\Responses;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class FileResponseSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('content', 'filename');
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('HttpModule\Responses\FileResponse');
        $this->shouldHaveType('Symfony\Component\HttpFoundation\Response');
    }

}
