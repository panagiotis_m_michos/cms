<?php

namespace spec\HttpModule\Responses;

use HttpModule\Responses\CsvResponse;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use SplFileObject;
use Symfony\Component\HttpFoundation\Response;

/**
 * @mixin CsvResponse
 */
class CsvResponseSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith(new SplFileObject(__FILE__), 'file.csv');
    }

    function it_can_be_instantiated()
    {
        $this->shouldHaveType(Response::class);
    }
}
