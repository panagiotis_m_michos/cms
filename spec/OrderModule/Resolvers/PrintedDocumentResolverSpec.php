<?php

namespace spec\OrderModule\Resolvers;

use Entities\Company;
use Entities\Customer;
use OrderModule\Resolvers\PrintedDocumentResolver;
use Package;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Services\CompanyService;

/**
 * @mixin PrintedDocumentResolver
 */
class PrintedDocumentResolverSpec extends ObjectBehavior
{
    /**
     * @var CompanyService
     */
    private $companyService;

    function let(CompanyService $companyService)
    {
        $this->companyService = $companyService;
        $this->beConstructedWith($this->companyService);
    }

    function it_will_resolve_documents(Customer $customer, Company $company)
    {
        $customer->isWholesale()->willReturn(FALSE);
        $customer->isRetail()->willReturn(TRUE);

        $company->setIsBronzeCoverLetterPrinted(TRUE)->shouldBeCalled();
        $company->setIsCertificatePrinted(TRUE)->shouldBeCalled();
        $company->setIsMaCoverLetterPrinted(TRUE)->shouldBeCalled();
        $company->setIsMaPrinted(TRUE)->shouldBeCalled();

        $this->companyService->saveCompany($company)->shouldBeCalled();

        $this->resolveDocuments(Package::PACKAGE_BASIC, $customer, $company);
    }
}
