<?php

namespace spec\OrderModule\Builders;

use BasketModule\Services\PriceCalculator;
use OrderModule\Builders\OrderItemBuilder;
use OrderModule\Builders\OrderItemBuilderFactory;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin OrderItemBuilderFactory
 */
class OrderItemBuilderFactorySpec extends ObjectBehavior
{
    /**
     * @var PriceCalculator
     */
    private $priceCalculator;

    function let(PriceCalculator $priceCalculator)
    {
        $this->priceCalculator = $priceCalculator;
        $this->beConstructedWith($this->priceCalculator);
    }

    function it_can_construct_order_item_builder()
    {
        $this->createBuilder()->shouldBeAnInstanceOf(OrderItemBuilder::class);
    }
}
