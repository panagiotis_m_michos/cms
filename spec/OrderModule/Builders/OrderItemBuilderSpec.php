<?php

namespace spec\OrderModule\Builders;

use BasketModule\Services\PriceCalculator;
use BasketProduct;
use Entities\Company;
use Entities\Order;
use Entities\OrderItem;
use OrderModule\Builders\OrderItemBuilder;
use PhpSpec\ObjectBehavior;
use Product;
use Prophecy\Argument;

/**
 * @mixin OrderItemBuilder
 */
class OrderItemBuilderSpec extends ObjectBehavior
{
    /**
     * @var PriceCalculator
     */
    private $priceCalculator;

    function let(PriceCalculator $priceCalculator)
    {
        $this->priceCalculator = $priceCalculator;
        $this->beConstructedWith($this->priceCalculator);
    }

    function it_can_build_order_item(Company $company, Product $product, Order $order)
    {
        $product->getLngTitle()->willReturn('title');
        $product->getLongTitle()->willReturn('long title');
        $product->getId()->willReturn(1);
        $company->getCompanyName()->willReturn('name');
        $company->getCompanyNumber()->willReturn('12345678');

        $orderItem = $this
            ->setOrder($order)
            ->setCompany($company)
            ->setProduct($product)
            ->build();

        $orderItem->shouldBeAnInstanceOf(OrderItem::class);
        $orderItem->getProductId()->shouldReturn(1);
        $orderItem->getOrder()->shouldReturn($order);
        $orderItem->getCompany()->shouldReturn($company);

    }
}
