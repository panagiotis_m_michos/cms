<?php

namespace spec\OrderModule\Listeners;

use Dispatcher\Events\CompanyEvent;
use Entities\Company;
use Entities\OrderItem;
use EventLocator;
use OrderModule\Listeners\UpdateOrderItemsDetailsListener;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Services\OrderItemService;

/**
 * @mixin UpdateOrderItemsDetailsListener
 */
class UpdateOrderItemsDetailsListenerSpec extends ObjectBehavior
{
    /**
     * @var OrderItemService
     */
    private $orderItemService;

    function let(OrderItemService $orderItemService)
    {
        $this->orderItemService = $orderItemService;
        $this->beConstructedWith($this->orderItemService);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('OrderModule\Listeners\UpdateOrderItemsDetailsListener');
        $this->shouldImplement('Symfony\Component\EventDispatcher\EventSubscriberInterface');
    }

    function it_can_get_subscribed_events()
    {
        $this::getSubscribedEvents()->shouldReturn([EventLocator::COMPANY_INCORPORATED => 'onCompanyIncorporated']);
    }

    function it_can_update_details_on_company_incorporated(CompanyEvent $event, Company $company, OrderItem $orderItem)
    {
        $event->getCompany()->willReturn($company);
        $this->orderItemService->getOrderItemsByCompany($company)->willReturn([$orderItem]);

        $orderItem->setCompany($company)->shouldBeCalled();
        $this->orderItemService->save($orderItem)->shouldBeCalled();

        $this->onCompanyIncorporated($event);
    }
}
