<?php

namespace spec\FraudProtectionModule\Services;

use Entities\Company;
use FraudProtectionModule\Entities\FraudProtectionQueue;
use FraudProtectionModule\Repositories\FraudProtectionQueueRepository;
use FraudProtectionModule\Services\FraudProtectionQueueService;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Services\CompanyService;

/**
 * @mixin FraudProtectionQueueService
 */
class FraudProtectionQueueServiceSpec extends ObjectBehavior
{
    /**
     * @var FraudProtectionQueueRepository
     */
    private $queueRepository;

    /**
     * @var CompanyService
     */
    private $companyService;

    function let(FraudProtectionQueueRepository $queueRepository, CompanyService $companyService)
    {
        $this->queueRepository = $queueRepository;
        $this->companyService = $companyService;
        $this->beConstructedWith($this->queueRepository, $this->companyService);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('FraudProtectionModule\Services\FraudProtectionQueueService');
        $this->shouldImplement('FraudProtectionModule\Interfaces\IFraudProtectionQueue');
    }

    function it_can_add_item_to_queue(Company $company)
    {
        $queueItem = new FraudProtectionQueue($company->getWrappedObject());

        $this->queueRepository->saveEntity($queueItem)->shouldBeCalled();

        $this->addToQueue($company);
    }
}
