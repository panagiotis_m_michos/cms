<?php

namespace spec\FraudProtectionModule\Listeners;

use Dispatcher\Events\OrderEvent;
use Entities\Company;
use Entities\Order;
use Entities\OrderItem;
use EventLocator;
use FraudProtectionModule\Interfaces\IFraudProtectionQueue;
use FraudProtectionModule\Listeners\AddCompanyToQueueListener;
use PhpSpec\ObjectBehavior;
use Product;
use Prophecy\Argument;
use Services\CompanyService;

/**
 * @mixin AddCompanyToQueueListener
 */
class AddCompanyToQueueListenerSpec extends ObjectBehavior
{
    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var IFraudProtectionQueue
     */
    private $queue;

    function let(CompanyService $companyService, IFraudProtectionQueue $queue)
    {
        $this->companyService = $companyService;
        $this->queue = $queue;
        $this->beConstructedWith($this->companyService, $this->queue);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('FraudProtectionModule\Listeners\AddCompanyToQueueListener');
    }

    function it_can_get_subscribed_events()
    {
        $this::getSubscribedEvents()->shouldReturn(
            [
                EventLocator::ORDER_COMPLETED => 'onOrderCompleted',
            ]
        );
    }

    function it_can_add_company_to_queue_on_order_completed_by_order_item(
        OrderEvent $event,
        Order $order,
        OrderItem $item1,
        OrderItem $item2,
        Company $company
    )
    {
        //todo: test ked orderitem nema company, ale treba najst company podla orderu

        $company->getId()->willReturn(1);
        $item1->getProductId()->willReturn(Product::PRODUCT_FRAUD_PROTECTION);
        $item1->getCompany()->willReturn($company);
        $item2->getProductId()->willReturn(Product::PRODUCT_BUSINESS_TOOLKIT);

        $event->getOrder()->willReturn($order);
        $order->getItems()->willReturn([$item1, $item2]);

        $this->queue->addToQueue($company)->shouldBeCalled();

        $this->onOrderCompleted($event);
    }

    function it_can_add_company_to_queue_on_order_completed_by_order(
        OrderEvent $event,
        Order $order,
        OrderItem $item1,
        OrderItem $item2,
        Company $company
    )
    {
        $company->getId()->willReturn(1);
        $item1->getProductId()->willReturn(Product::PRODUCT_FRAUD_PROTECTION);
        $item1->getCompany()->willReturn(NULL);
        $item1->getOrder()->willReturn($order);
        $this->companyService->getCompanyByOrder($order)->willReturn($company);
        $item2->getProductId()->willReturn(Product::PRODUCT_BUSINESS_TOOLKIT);

        $event->getOrder()->willReturn($order);
        $order->getItems()->willReturn([$item1, $item2]);

        $this->queue->addToQueue($company)->shouldBeCalled();

        $this->onOrderCompleted($event);
    }
}
