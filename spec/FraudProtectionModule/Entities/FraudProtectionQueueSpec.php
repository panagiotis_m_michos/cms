<?php

namespace spec\FraudProtectionModule\Entities;

use Entities\Company;
use FraudProtectionModule\Entities\FraudProtectionQueue;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin FraudProtectionQueue
 */
class FraudProtectionQueueSpec extends ObjectBehavior
{
    function let(Company $company)
    {
        $this->beConstructedWith($company);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('FraudProtectionModule\Entities\FraudProtectionQueue');
    }
}
