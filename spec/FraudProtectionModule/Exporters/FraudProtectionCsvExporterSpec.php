<?php

namespace spec\FraudProtectionModule\Exporters;

use Goodby\CSV\Export\Standard\Exporter;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class FraudProtectionCsvExporterSpec extends ObjectBehavior
{
    function let(Exporter $csvExporter)
    {
        $this->beConstructedWith($csvExporter, sys_get_temp_dir());
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('FraudProtectionModule\Exporters\FraudProtectionCsvExporter');
    }

    function it_can_export_companies_fraud_protection_csv()
    {
        $file = $this->exportCompanies([]);
        $file->shouldBeAnInstanceOf('Symfony\Component\HttpFoundation\File\File');
    }
}
