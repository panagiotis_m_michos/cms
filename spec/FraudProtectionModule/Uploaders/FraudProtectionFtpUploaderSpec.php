<?php

namespace spec\FraudProtectionModule\Uploaders;

use FraudProtectionModule\Uploaders\FraudProtectionFtpUploader;
use Gaufrette\Filesystem;
use org\bovigo\vfs\vfsStream;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Utils\File;

/**
 * @mixin FraudProtectionFtpUploader
 */
class FraudProtectionFtpUploaderSpec extends ObjectBehavior
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    function let(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
        $this->beConstructedWith($this->filesystem);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('FraudProtectionModule\Uploaders\FraudProtectionFtpUploader');
    }

    function it_can_upload(File $file)
    {
        vfsStream::setup()->addChild(vfsStream::newFile('test'));
        $tmpFilePath = vfsStream::url('root/test');

        $file->getPathname()->willReturn($tmpFilePath);
        $this->filesystem->write('filename', Argument::any(), TRUE)->shouldBeCalled();

        $this->upload($file, 'filename');
    }
}
