<?php

namespace spec\FraudProtectionModule\Commands;

use Cron\INotifier;
use FraudProtectionModule\Entities\FraudProtectionQueue;
use FraudProtectionModule\Exporters\FraudProtectionCsvExporter;
use FraudProtectionModule\Services\FraudProtectionQueueService;
use FraudProtectionModule\Uploaders\FraudProtectionFtpUploader;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Log\LoggerInterface;
use Utils\File;

class ProcessFraudProtectionCompaniesCommandSpec extends ObjectBehavior
{
    /**
     * @var FraudProtectionQueueService
     */
    private $service;

    /**
     * @var FraudProtectionCsvExporter
     */
    private $exporter;

    /**
     * @var FraudProtectionFtpUploader
     */
    private $uploader;

    function let(
        LoggerInterface $logger,
        INotifier $notifier,
        FraudProtectionQueueService $service,
        FraudProtectionCsvExporter $exporter,
        FraudProtectionFtpUploader $uploader
    )
    {
        $this->service = $service;
        $this->exporter = $exporter;
        $this->uploader = $uploader;
        $this->beConstructedWith($logger, $notifier, $this->service, $this->exporter, $this->uploader);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('FraudProtectionModule\Commands\ProcessFraudProtectionCompaniesCommand');
        $this->shouldHaveType('Cron\Commands\CommandAbstract');
    }

    function it_can_execute(File $file, FraudProtectionQueue $queueItem)
    {
        $this->service->getQueueItemsToSend()->willReturn([$queueItem]);
        $this->exporter->exportCompanies([$queueItem])->willReturn($file);
        $filename = tempnam(TEMP_DIR, 'test_upload_file_');
        $file->getPathname()->willReturn($filename);

        $this->uploader->upload($file, Argument::any())->shouldBeCalled();
        $this->service->removeFromQueue($queueItem)->shouldBeCalled();
        
        $this->execute();
    }
}
