<?php

namespace spec\FraudProtectionModule\Repositories;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use FraudProtectionModule\Repositories\FraudProtectionQueueRepository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin FraudProtectionQueueRepository
 */
class FraudProtectionQueueRepositorySpec extends ObjectBehavior
{
    function let(EntityManager $entityManager, ClassMetadata $classMetadata)
    {
        $this->beConstructedWith($entityManager, $classMetadata);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('FraudProtectionModule\Repositories\FraudProtectionQueueRepository');
        $this->shouldHaveType('Repositories\BaseRepository_Abstract');
    }
}
