<?php

namespace spec\FilesystemModule\GaufretteDatabaseStorage;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class CustomerFileRepositorySpec extends ObjectBehavior
{
    function let(EntityManager $entityManager, ClassMetadata $metadata)
    {
        $this->beConstructedWith($entityManager, $metadata);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('Doctrine\ORM\EntityRepository');
    }
}
