<?php

namespace spec\FilesystemModule\GaufretteDatabaseStorage;

use Doctrine\ORM\Internal\Hydration\IterableResult;
use Gaufrette\Filesystem;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class FileIteratorSpec extends ObjectBehavior
{
    function let(IterableResult $iterableResult, Filesystem $filesystem)
    {
        $this->beConstructedWith($iterableResult, $filesystem);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('FilesystemModule\GaufretteDatabaseStorage\FileIterator');
    }
}
