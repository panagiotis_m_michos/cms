<?php

namespace spec\FilesystemModule\GaufretteDatabaseStorage;

use Doctrine\ORM\Internal\Hydration\IterableResult;
use Entities\Customer;
use FilesystemModule\GaufretteDatabaseStorage\CustomerFileRepository;
use FilesystemModule\GaufretteDatabaseStorage\Entities\CustomerFileInfo;
use FilesystemModule\GaufretteDatabaseStorage\FileIterator;
use Gaufrette\File;
use Gaufrette\Filesystem;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use SplFileInfo;
use Symfony\Component\HttpFoundation\File\File as HttpFile;
use Tests\Helpers\ObjectHelper;

class CustomerFileStorageSpec extends ObjectBehavior
{
    const KEY = 'somekey';

    function let(CustomerFileRepository $customerFileRepository, Filesystem $filesystem)
    {
        $this->beConstructedWith($customerFileRepository, $filesystem);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('FilesystemModule\GaufretteDatabaseStorage\CustomerFileStorage');
    }

    function it_throws_exception_on_get_a_file(CustomerFileRepository $customerFileRepository)
    {
        $customer = ObjectHelper::createCustomer();
        $customerFileRepository->findFile($customer, 'somekey')->willThrow('Doctrine\ORM\NoResultException');
        $this->shouldThrow('FilesystemModule\GaufretteDatabaseStorage\Exceptions\FileException')->duringGetFile($customer, 'somekey');
    }

    function it_returns_a_file(CustomerFileRepository $customerFileRepository)
    {
        $customer = ObjectHelper::createCustomer();
        $customerFile = CustomerFileInfo::fromKey('somekey', $customer, 'test', 'png');
        $customerFileRepository->findFile($customer, 'somekey')->willReturn($customerFile);
        $this->getFile($customer, 'somekey')->shouldHaveType('FilesystemModule\GaufretteDatabaseStorage\CustomerFile');
    }

    function it_returns_files(CustomerFileRepository $customerFileRepository, IterableResult $iterator)
    {
        $customer = ObjectHelper::createCustomer();
        $customerFileRepository->findFiles($customer, 'test')->willReturn($iterator);
        $this->listFiles($customer, 'test')->shouldHaveType('FilesystemModule\GaufretteDatabaseStorage\FileIterator');
    }

    function it_removes_a_file(CustomerFileRepository $customerFileRepository, Filesystem $filesystem, Customer $customer, CustomerFileInfo $customerFile)
    {
        $customerFile->getKey()->willReturn(self::KEY);
        $customerFileRepository->findFile($customer, self::KEY)->willReturn($customerFile);
        $customerFileRepository->removeFile($customerFile)->shouldBeCalled();
        $filesystem->delete(self::KEY)->willReturn(TRUE);
        $this->removeFile($customer, self::KEY)->shouldBe(TRUE);
    }

    function it_adds_a_file(Filesystem $filesystem, File $file, Customer $customer)
    {
        $customer->getId()->willReturn(23434);
        $filesystem->write(Argument::containingString('30000/23434/test'), 'file contents')->shouldBeCalled();
        $filesystem->createFile(Argument::containingString('30000/23434/test'))->willReturn($file);
        $tempFile = tempnam(sys_get_temp_dir(), 'test');
        file_put_contents($tempFile, 'file contents');
        $file = new HttpFile($tempFile);
        $this->addFile($customer, 'test', $file)->shouldHaveType('FilesystemModule\GaufretteDatabaseStorage\CustomerFile');
    }

}
