<?php

namespace spec\CompanyModule\Entities;

use Entities\Company;
use Entities\Customer;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class CompanySettingSpec extends ObjectBehavior
{
    function let()
    {
        $company = new Company(new Customer('test', 'test'), 'test');
        $this->beConstructedThrough('doNotShowBankOffer', [$company]);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('CompanyModule\Entities\CompanySetting');
    }
}
