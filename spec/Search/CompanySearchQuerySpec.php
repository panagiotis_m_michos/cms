<?php

namespace spec\Search;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class CompanySearchQuerySpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Search\CompanySearchQuery');
    }

}
