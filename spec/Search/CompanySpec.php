<?php

namespace spec\Search;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class CompanySpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedThrough('fromArray', array(array('companyName' => 'companyName')));
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('Search\Company');
    }


}
