<?php

namespace spec\Search;

use Doctrine\ORM\Internal\Hydration\IterableResult;
use Doctrine\ORM\Query;
use Factories\Front\CompanyViewFactory;
use PhpSpec\ObjectBehavior;
use PhpSpec\Wrapper\Collaborator;
use Prophecy\Argument;
use Search\Company;
use Search\CompanyFinder;
use Search\CompanyIterator;
use Search\CompanySearchQuery;
use Search\ICompanyRepository;
use Tests\Helpers\ObjectHelper;

/**
 * @mixin CompanyFinder
 */
class CompanyFinderSpec extends ObjectBehavior
{
    /**
     * @var CompanyViewFactory|Collaborator
     */
    private $companyViewFactory;

    function let(ICompanyRepository $companyRepository, CompanyViewFactory $companyViewFactory)
    {
        $this->beConstructedWith($companyRepository, $companyViewFactory);
        $this->companyViewFactory = $companyViewFactory;
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('Search\CompanyFinder');
    }

    function it_finds_customer_companies(ICompanyRepository $companyRepository, IterableResult $iterableResult)
    {
        $customerEntity = ObjectHelper::createCustomer();
        $companyEntity = ObjectHelper::createCompany($customerEntity);

        $company = Company::fromEntity($companyEntity, $this->companyViewFactory->getWrappedObject());
        $iterableResult->current()->willReturn([$companyEntity]);
        $companySearchQuery = new CompanySearchQuery();
        $companySearchQuery->setTerm('test me');
        $companyRepository->findByQuery($customerEntity, $companySearchQuery)->willReturn($iterableResult);

        $mock = $this->findCustomerCompanies($customerEntity, $companySearchQuery);
        $mock->shouldHaveType('Search\CompanyIterator');
        $mock->shouldHaveItem($company);
    }

    public function getMatchers()
    {
        return array(
            'haveItem' => function(CompanyIterator $companyIterator, Company $company) {
                return $companyIterator->current() == $company;
            }
        );
    }
}
