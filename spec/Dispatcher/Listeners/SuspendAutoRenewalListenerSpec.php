<?php

namespace spec\Dispatcher\Listeners;

use Dispatcher\Events\TokenRemovedEvent;
use Dispatcher\Listeners\SuspendAutoRenewalListener;
use Entities\Payment\Token;
use EventLocator;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ServiceSettingsModule\Services\ServiceSettingsService;

/**
 * @mixin SuspendAutoRenewalListener
 */
class SuspendAutoRenewalListenerSpec extends ObjectBehavior
{
    /**
     * @var ServiceSettingsService
     */
    private $serviceSettingsService;

    function let(ServiceSettingsService $serviceSettingsService)
    {
        $this->beConstructedWith($serviceSettingsService);
        $this->serviceSettingsService = $serviceSettingsService;
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('Dispatcher\Listeners\SuspendAutoRenewalListener');
        $this->shouldHaveType('Symfony\Component\EventDispatcher\EventSubscriberInterface');
    }

    function it_can_get_subscribed_events()
    {
        $this::getSubscribedEvents()->shouldReturn(
            [
                EventLocator::TOKEN_REMOVED => 'onTokenRemoved',
            ]
        );
    }

    function it_can_suspend_auto_renewal_on_token_removed(TokenRemovedEvent $event, Token $token)
    {
        $event->getToken()->willReturn($token);
        $this->serviceSettingsService->suspendAutoRenewalByToken($token)->shouldBeCalled();

        $this->onTokenRemoved($event);
    }
}
