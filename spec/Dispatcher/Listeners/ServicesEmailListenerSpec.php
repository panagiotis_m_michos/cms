<?php

namespace spec\Dispatcher\Listeners;

use Dispatcher\Events\ServicesEmailEvent;
use Dispatcher\Listeners\ServicesEmailListener;
use Emailers\ServicesEmailer;
use EventLocator;
use FEmail;
use Models\View\Front\CompanyServicesEmailView;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin ServicesEmailListener
 */
class ServicesEmailListenerSpec extends ObjectBehavior
{
    /**
     * @var ServicesEmailer
     */
    private $emailer;

    function let(ServicesEmailer $emailer)
    {
        $this->emailer = $emailer;
        $this->beConstructedWith($this->emailer);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(ServicesEmailListener::class);
    }

    function it_can_get_subscribed_events()
    {
        $this::getSubscribedEvents()->shouldReturn(
            [
                EventLocator::COMPANY_SERVICES_EXPIRES_IN_28 => 'sendExpirationEmail',
                EventLocator::COMPANY_SERVICES_EXPIRES_IN_15 => 'sendExpirationEmail',
                EventLocator::COMPANY_SERVICES_EXPIRES_IN_1 => 'sendExpirationEmail',
                EventLocator::COMPANY_SERVICES_EXPIRED => 'sendExpirationEmail',
                EventLocator::COMPANY_SERVICES_EXPIRED_15 => 'sendExpirationEmail',
                EventLocator::COMPANY_SERVICES_EXPIRED_28 => 'sendExpirationEmail',
            ]
        );
    }

    function it_can_send_expiration_mail(ServicesEmailEvent $event, CompanyServicesEmailView $view, FEmail $email)
    {
        $event->getEmailView()->willReturn($view);
        $event->getEmail()->willReturn($email);

        $this->emailer->sendEmailExpiration($view, $email)->shouldBeCalled();

        $this->sendExpirationEmail($event);
    }
}
