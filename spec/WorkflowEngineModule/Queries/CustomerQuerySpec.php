<?php

namespace spec\WorkflowEngineModule\Queries;

use Kdyby\Persistence\Queryable;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use WorkflowEngineModule\Queries\CustomerQuery;

/**
 * @mixin CustomerQuery
 */
class CustomerQuerySpec extends ObjectBehavior
{
    /**
     * @var Queryable
     */
    private $repository;

    function let(Queryable $repository)
    {
        $this->repository = $repository;
        $this->beConstructedWith($this->repository);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('WorkflowEngineModule\Queries\CustomerQuery');
    }
}
