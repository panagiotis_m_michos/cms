<?php

namespace spec\LplActiveServicesExportModule\Repositories;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Internal\Hydration\IterableResult;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query;
use LplActiveServicesExportModule\Repositories\ActiveServicesRepository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin ActiveServicesRepository
 */
class ActiveServicesRepositorySpec extends ObjectBehavior
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    function let(EntityManager $entityManager, ClassMetadata $classMetadata)
    {
        $this->entityManager = $entityManager;
        $this->beConstructedWith($this->entityManager, $classMetadata);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('LplActiveServicesExportModule\Repositories\ActiveServicesRepository');
        $this->shouldHaveType('Doctrine\ORM\EntityRepository');
    }
}
