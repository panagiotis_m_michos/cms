<?php

namespace spec\LplActiveServicesExportModule\Exporters;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class CsvExporterFactorySpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('LplActiveServicesExportModule\Exporters\CsvExporterFactory');
    }
}
