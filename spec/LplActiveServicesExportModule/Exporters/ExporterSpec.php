<?php

namespace spec\LplActiveServicesExportModule\Exporters;

use Doctrine\ORM\Internal\Hydration\IterableResult;
use Goodby\CSV\Export\Standard\Exporter as CsvExporter;
use LplActiveServicesExportModule\Exporters\Exporter;
use LplActiveServicesExportModule\Repositories\ActiveServicesRepository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @mixin Exporter
 */
class ExporterSpec extends ObjectBehavior
{
    /**
     * @var CsvExporter
     */
    private $exporter;

    /**
     * @var ActiveServicesRepository
     */
    private $repository;

    /**
     * @var string
     */
    private $exportFilePath;

    function let(ActiveServicesRepository $repository, CsvExporter $exporter)
    {
        $this->exportFilePath = '/tmp/export.csv';
        $this->repository = $repository;
        $this->exporter = $exporter;
        $this->beConstructedWith($this->repository, $this->exporter, $this->exportFilePath);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('LplActiveServicesExportModule\Exporters\Exporter');
    }

    function it_can_export_csv(IterableResult $result)
    {
        $this->repository->findAllIterator()->willReturn($result);

        $this->export()->shouldBeAnInstanceOf('Symfony\Component\HttpFoundation\File\File');

        $this->exporter->export($this->exportFilePath, Argument::type('Iterator'))->shouldBeCalled();
    }

    function it_can_get_export_root_path()
    {
        $this->getExportFilePath()->shouldReturn($this->exportFilePath);
    }
}
