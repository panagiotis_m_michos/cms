<?php

namespace spec\LplActiveServicesExportModule\Entities;

use LplActiveServicesExportModule\Entities\ActiveService;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin ActiveService
 */
class ActiveServiceSpec extends ObjectBehavior
{
    private $companyNumber = '12345678';

    private $hasRegisteredOffice = TRUE;

    private $hasServiceAddress = FALSE;

    function let()
    {
        $this->beConstructedWith(
            $this->companyNumber,
            $this->hasRegisteredOffice,
            $this->hasServiceAddress
        );
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('LplActiveServicesExportModule\Entities\ActiveService');
    }

    function it_can_get_company_number()
    {
        $this->getCompanyNumber()->shouldReturn($this->companyNumber);
    }

    function it_check_if_registered_office_is_active()
    {
        $this->hasRegisteredOffice()->shouldReturn($this->hasRegisteredOffice);
    }

    function it_check_if_service_address_is_active()
    {
        $this->hasServiceAddress()->shouldReturn($this->hasServiceAddress);
    }
}
