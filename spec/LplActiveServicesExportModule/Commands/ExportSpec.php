<?php

namespace spec\LplActiveServicesExportModule\Commands;

use Cron\INotifier;
use Gaufrette\Filesystem;
use LplActiveServicesExportModule\Commands\Export;
use LplActiveServicesExportModule\Exporters\Exporter;
use LplActiveServicesExportModule\Uploaders\Uploader;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @mixin Export
 */
class ExportSpec extends ObjectBehavior
{
    /**
     * @var Exporter
     */
    private $exporter;

    /**
     * @var Uploader
     */
    private $uploader;

    /**
     * @var Filesystem
     */
    private $filesystem;

    function let(LoggerInterface $logger, INotifier $notifier, Exporter $exporter, Uploader $uploader, Filesystem $filesystem)
    {
        $this->uploader = $uploader;
        $this->exporter = $exporter;
        $this->filesystem = $filesystem;
        $this->beConstructedWith($logger, $notifier, $this->exporter, $this->uploader, $this->filesystem);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('LplActiveServicesExportModule\Commands\Export');
        $this->shouldHaveType('Cron\Commands\CommandAbstract');
    }

    function it_can_execute(File $file)
    {
        $this->exporter->export()->willReturn($file);

        $this->execute();

        $this->uploader->upload(Argument::any())->shouldBeCalled();
        $this->filesystem->delete(Argument::any())->shouldBeCalled();
    }
}
