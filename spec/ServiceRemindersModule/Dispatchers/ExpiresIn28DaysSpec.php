<?php

namespace spec\ServiceRemindersModule\Dispatchers;

use Dispatcher\Events\ServiceEmailEventFactory;
use Dispatcher\Events\ServicesEmailEvent;
use Emailers\ServicesEmailer;
use EventLocator;
use FEmail;
use Models\View\Front\CompanyServicesView;
use Models\View\ServiceView;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Repositories\EmailRepository;
use ServiceRemindersModule\Dispatchers\ExpiresIn15Days;
use ServiceRemindersModule\Dispatchers\ExpiresIn28Days;
use ServiceRemindersModule\Dispatchers\IDispatcher;
use Services\EventService;
use Symfony\Component\EventDispatcher\EventDispatcher;

/**
 * @mixin ExpiresIn28Days
 */
class ExpiresIn28DaysSpec extends ObjectBehavior
{
    /**
     * @var EmailRepository
     */
    private $emailRepository;
    /**
     * @var ServiceEmailEventFactory
     */
    private $eventFactory;

    /**
     * @var EventService
     */
    private $eventService;

    /**
     * @var EventDispatcher
     */
    private $eventDispatcher;

    function let(EmailRepository $emailRepository, ServiceEmailEventFactory $eventFactory, EventService $eventService, EventDispatcher $eventDispatcher)
    {
        $this->emailRepository = $emailRepository;
        $this->eventFactory = $eventFactory;
        $this->eventService = $eventService;
        $this->eventDispatcher = $eventDispatcher;

        $this->beConstructedWith($this->emailRepository, $this->eventFactory, $this->eventService, $this->eventDispatcher);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(ExpiresIn28Days::class);
        $this->shouldHaveType(IDispatcher::class);
    }

    function it_can_dispatch(CompanyServicesView $view, FEmail $email, ServiceView $sv1, ServiceView $sv2, ServicesEmailEvent $event)
    {
        $eventKey = EventLocator::SERVICE_EXPIRES_IN_28;
        $sv1->getId()->willReturn(1);
        $sv2->getId()->willReturn(2);

        $view->hasServicesWithoutEventExpiringIn(CompanyServicesView::TYPE_EXPIRES_IN_28, $eventKey)->willReturn(TRUE);
        $this->emailRepository->find(ServicesEmailer::SERVICES_EXPIRES_IN_28_EMAIL)->willReturn($email);
        $view->getServicesWithoutEventExpiringIn(CompanyServicesView::TYPE_EXPIRES_IN_28, $eventKey)->willReturn([$sv1, $sv2]);
        $this->eventFactory->create($email, $view, [$sv1, $sv2], CompanyServicesView::TYPE_EXPIRES_IN_28)->willReturn($event);

        $this->eventDispatcher->dispatch(EventLocator::COMPANY_SERVICES_EXPIRES_IN_28, $event)->shouldBeCalled();
        $this->eventService->notify($eventKey, 1)->shouldBeCalled();
        $this->eventService->notify($eventKey, 2)->shouldBeCalled();

        $this->dispatch($view);
    }
}
