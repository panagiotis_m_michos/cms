<?php

namespace spec\ServiceRemindersModule\Dispatchers;

use Models\View\Front\CompanyServicesView;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ServiceRemindersModule\Dispatchers\IDispatcher;
use ServiceRemindersModule\Dispatchers\RemindersDispatcher;

/**
 * @mixin RemindersDispatcher
 */
class RemindersDispatcherSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('ServiceRemindersModule\Dispatchers\RemindersDispatcher');
        $this->shouldHaveType('ServiceRemindersModule\Dispatchers\IDispatcher');
    }

    function it_can_dispatch(CompanyServicesView $view, IDispatcher $d1, IDispatcher $d2)
    {
        $this->addDispatcher($d1);
        $this->addDispatcher($d2);

        $d1->dispatch($view)->shouldBeCalled();
        $d2->dispatch($view)->shouldBeCalled();

        $this->dispatch($view);
    }

    function it_can_add_dispatcher(IDispatcher $dispatcher)
    {
        $this->addDispatcher($dispatcher);
    }
}
