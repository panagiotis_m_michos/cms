<?php

namespace spec\ServiceRemindersModule\Commands;

use Cron\Commands\ICommand;
use Cron\INotifier;
use Entities\Company;
use Entities\Service;
use Exception;
use Factories\Front\CompanyServicesViewFactory;
use Models\View\Front\CompanyServicesView;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Log\LoggerInterface;
use ServiceRemindersModule\Commands\SendRemindersCommand;
use ServiceRemindersModule\Dispatchers\IDispatcher;
use Services\ServiceService;

/**
 * @mixin SendRemindersCommand
 */
class SendRemindersCommandSpec extends ObjectBehavior
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var INotifier
     */
    private $notifier;

    /**
     * @var IDispatcher
     */
    private $dispatcher;

    /**
     * @var ServiceService
     */
    private $serviceService;

    /**
     * @var CompanyServicesViewFactory
     */
    private $viewFactory;

    function let(LoggerInterface $logger, INotifier $notifier, IDispatcher $dispatcher, ServiceService $serviceService, CompanyServicesViewFactory $viewFactory)
    {
        $this->logger = $logger;
        $this->notifier = $notifier;
        $this->dispatcher = $dispatcher;
        $this->serviceService = $serviceService;
        $this->viewFactory = $viewFactory;
        $this->beConstructedWith($this->logger, $this->notifier, $this->dispatcher, $this->serviceService, $this->viewFactory);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(SendRemindersCommand::class);
        $this->shouldHaveType(ICommand::class);
    }

    function it_can_execute(Service $s1, Service $s2, Company $c1, Company $c2, CompanyServicesView $cv1, CompanyServicesView $cv2)
    {
        $this->serviceService->getServicesToSendRemindersFor()->willReturn([$s1, $s2]);
        $s1->getCompany()->willReturn($c1);
        $s2->getCompany()->willReturn($c2);
        $this->viewFactory->create($c1)->willReturn($cv1);
        $this->viewFactory->create($c2)->willReturn($cv2);

        $this->dispatcher->dispatch($cv1)->shouldBeCalled();
        $this->dispatcher->dispatch($cv2)->shouldBeCalled();

        $this->notifier->triggerSuccess(Argument::any(), Argument::any(), Argument::any())->shouldBeCalled();

        $this->execute();
    }

    function it_only_logs_when_no_companies_are_found()
    {
        $this->serviceService->getServicesToSendRemindersFor()->willReturn([]);

        $this->logger->notice(Argument::any())->shouldBeCalled();

        $this->execute();
    }

    function it_logs_errors(Service $s1, Company $c1, CompanyServicesView $cv1)
    {
        $this->serviceService->getServicesToSendRemindersFor()->willReturn([$s1]);
        $s1->getCompany()->willReturn($c1);
        $this->viewFactory->create($c1)->willReturn($cv1);
        $this->dispatcher->dispatch($cv1)->willThrow(new Exception('test'));

        $this->logger->error('test')->shouldBeCalled();

        $this->execute();
    }
}
