<?php

namespace spec\VoucherModule\Validators;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use VoucherModule\Validators\VoucherExists;

/**
 * @mixin VoucherExists
 */
class VoucherExistsSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(VoucherExists::class);
    }
}
