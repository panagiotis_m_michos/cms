<?php

namespace spec\VoucherModule\Validators;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use VoucherModule\Validators\MinimalSpendReached;

/**
 * @mixin MinimalSpendReached
 */
class MinimalSpendReachedSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(MinimalSpendReached::class);
    }
}
