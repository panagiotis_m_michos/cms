<?php

namespace spec\VoucherModule\Validators;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use VoucherModule\Validators\VoucherEnabled;

/**
 * @mixin VoucherEnabled
 */
class VoucherEnabledSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(VoucherEnabled::class);
    }
}
