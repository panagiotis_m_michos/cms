<?php

namespace spec\VoucherModule\Validators;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Violation\ConstraintViolationBuilderInterface;
use VoucherModule\Entities\Voucher;
use VoucherModule\Services\VoucherService;
use VoucherModule\Validators\VoucherExists;
use VoucherModule\Validators\VoucherExistsValidator;

/**
 * @mixin VoucherExistsValidator
 */
class VoucherExistsValidatorSpec extends ObjectBehavior
{
    /**
     * @var VoucherService
     */
    private $voucherService;

    /**
     * @var ExecutionContextInterface
     */
    private $context;

    function let(VoucherService $voucherService, ExecutionContextInterface $context)
    {
        $this->beConstructedWith($voucherService);
        $this->initialize($context);

        $this->voucherService = $voucherService;
        $this->context = $context;
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(VoucherExistsValidator::class);
    }

    function it_should_add_violation(ConstraintViolationBuilderInterface $builder)
    {
        $code = 'code123';

        $this->voucherService->getVoucherByCode($code)->willReturn(NULL);

        $constraint = new VoucherExists;
        $this->context->buildViolation($constraint->message)->willReturn($builder);
        $builder->addViolation()->shouldBeCalled();

        $this->validate($code, $constraint);
    }

    function it_should_not_add_violation(Voucher $voucher)
    {
        $code = 'code123';

        $this->voucherService->getVoucherByCode($code)->willReturn($voucher);

        $constraint = new VoucherExists;

        $this->validate($code, $constraint);
    }
}
