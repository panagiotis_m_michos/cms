<?php

namespace spec\VoucherModule\Validators;

use DateTime;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\Validator\Violation\ConstraintViolationBuilderInterface;
use VoucherModule\Entities\Voucher;
use VoucherModule\Services\VoucherService;
use VoucherModule\Validators\VoucherEnabled;
use VoucherModule\Validators\VoucherEnabledValidator;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @mixin VoucherEnabledValidator
 */
class VoucherEnabledValidatorSpec extends ObjectBehavior
{
    /**
     * @var VoucherService
     */
    private $voucherService;

    /**
     * @var ExecutionContextInterface
     */
    private $context;

    function let(VoucherService $voucherService, ExecutionContextInterface $context)
    {
        $this->beConstructedWith($voucherService);
        $this->initialize($context);

        $this->voucherService = $voucherService;
        $this->context = $context;
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(VoucherEnabledValidator::class);
    }

    function it_should_add_violation(Voucher $voucher, ConstraintViolationBuilderInterface $builder)
    {
        $code = 'code123';

        $this->voucherService->getVoucherByCode($code)->willReturn($voucher);
        $voucher->canBeUsedOn(new DateTime)->willReturn(FALSE);

        $constraint = new VoucherEnabled;
        $this->context->buildViolation($constraint->message)->willReturn($builder);
        $builder->addViolation()->shouldBeCalled();

        $this->validate($code, $constraint);
    }

    function it_should_not_add_violation(Voucher $voucher)
    {
        $code = 'code123';

        $this->voucherService->getVoucherByCode($code)->willReturn($voucher);
        $voucher->canBeUsedOn(new DateTime)->willReturn(TRUE);

        $constraint = new VoucherEnabled;

        $this->validate($code, $constraint);
    }
}
