<?php

namespace spec\VoucherModule\Validators;

use Basket;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\Validator\Violation\ConstraintViolationBuilderInterface;
use VoucherModule\Entities\Voucher;
use VoucherModule\Services\VoucherService;
use VoucherModule\Validators\MinimalSpendReached;
use VoucherModule\Validators\MinimalSpendReachedValidator;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @mixin MinimalSpendReachedValidator
 */
class MinimalSpendReachedValidatorSpec extends ObjectBehavior
{
    /**
     * @var VoucherService
     */
    private $voucherService;

    /**
     * @var ExecutionContextInterface
     */
    private $context;

    /**
     * @var Basket
     */
    private $basket;

    function let(VoucherService $voucherService, Basket $basket, ExecutionContextInterface $context)
    {
        $this->beConstructedWith($voucherService, $basket);
        $this->initialize($context);

        $this->voucherService = $voucherService;
        $this->basket = $basket;
        $this->context = $context;
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(MinimalSpendReachedValidator::class);
    }

    function it_should_add_violation(Voucher $voucher, ConstraintViolationBuilderInterface $constraintViolationBuilderInterface)
    {
        $code = 'code123';
        $minSpend = 20;
        $basketPrice = 10;

        $this->voucherService->getVoucherByCode($code)->willReturn($voucher);
        $voucher->getMinSpend()->willReturn($minSpend);
        $this->basket->getPrice('subTotal')->willReturn($basketPrice);

        $constraint = new MinimalSpendReached;
        $this->context->buildViolation(sprintf($constraint->message, $minSpend))->willReturn($constraintViolationBuilderInterface);
        $constraintViolationBuilderInterface->addViolation()->shouldBeCalled();

        $this->validate($code, $constraint);
    }

    function it_should_not_add_violation(Voucher $voucher)
    {
        $code = 'code123';
        $minSpend = 20;
        $basketPrice = 30;

        $this->voucherService->getVoucherByCode($code)->willReturn($voucher);
        $voucher->getMinSpend()->willReturn($minSpend);
        $this->basket->getPrice('subTotal')->willReturn($basketPrice);

        $constraint = new MinimalSpendReached;

        $this->validate($code, $constraint);
    }
}
