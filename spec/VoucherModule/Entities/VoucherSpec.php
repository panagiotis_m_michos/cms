<?php

namespace spec\VoucherModule\Entities;

use DateTime;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use VoucherModule\Entities\Voucher;

/**
 * @mixin Voucher
 */
class VoucherSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Voucher::class);
    }

    function it_can_check_if_it_can_be_used()
    {
        $now = new DateTime;

        $this->setDtExpiry(NULL);
        $this->setUsageLimit(0);

        $this->canBeUsedOn($now)->shouldBe(TRUE);

        $this->setDtExpiry(new DateTime('+1 day'));
        $this->setUsageLimit(0);

        $this->canBeUsedOn($now)->shouldBe(TRUE);

        $this->setDtExpiry(new DateTime('+1 day'));
        $this->setUsageLimit(10);
        $this->setUsed(9);

        $this->canBeUsedOn($now)->shouldBe(TRUE);

        $this->setDtExpiry(NULL);
        $this->setUsageLimit(10);
        $this->setUsed(9);

        $this->canBeUsedOn($now)->shouldBe(TRUE);
    }

    function it_can_check_if_it_can_not_be_used()
    {
        $now = new DateTime;

        $this->setDtExpiry(new DateTime('-1 day'));
        $this->setUsageLimit(0);

        $this->canBeUsedOn($now)->shouldBe(FALSE);

        $this->setDtExpiry(new DateTime('+1 day'));
        $this->setUsageLimit(1);
        $this->setUsed(1);

        $this->canBeUsedOn($now)->shouldBe(FALSE);

        $this->setDtExpiry(new DateTime('-1 day'));
        $this->setUsageLimit(10);
        $this->setUsed(10);

        $this->canBeUsedOn($now)->shouldBe(FALSE);
    }
}
