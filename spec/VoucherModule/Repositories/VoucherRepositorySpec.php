<?php

namespace spec\VoucherModule\Repositories;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use VoucherModule\Repositories\VoucherRepository;

/**
 * @mixin VoucherRepository
 */
class VoucherRepositorySpec extends ObjectBehavior
{
    function let(EntityManager $em, ClassMetadata $class)
    {
        $this->beConstructedWith($em, $class);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(VoucherRepository::class);
    }
}
