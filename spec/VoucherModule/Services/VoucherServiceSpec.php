<?php

namespace spec\VoucherModule\Services;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use VoucherModule\Entities\Voucher;
use VoucherModule\Repositories\VoucherRepository;
use VoucherModule\Services\VoucherService;

/**
 * @mixin VoucherService
 */
class VoucherServiceSpec extends ObjectBehavior
{
    /**
     * @var VoucherRepository
     */
    private $repository;

    function let(VoucherRepository $repository)
    {
        $this->beConstructedWith($repository);
        $this->repository = $repository;
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(VoucherService::class);
    }

    function it_should_return_voucher_by_code(Voucher $voucher)
    {
        $code = 'code123';
        $this->repository->findOneBy(['voucherCode' => $code])->willReturn($voucher);

        $this->getVoucherByCode($code)->shouldReturn($voucher);
    }
}
