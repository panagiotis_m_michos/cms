<?php

namespace spec\BasketModule\Services;

use BasketModule\Factories\PriceFactory;
use BasketModule\Services\PriceCalculator;
use BasketModule\ValueObject\Price;
use BasketProduct;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin PriceCalculator
 */
class PriceCalculatorSpec extends ObjectBehavior
{
    /**
     * @var PriceFactory
     */
    private $priceFactory;

    function let(PriceFactory $priceFactory)
    {
        $this->priceFactory = $priceFactory;
        $this->beConstructedWith($this->priceFactory);
    }

    function it_calculates_price(BasketProduct $withoutVat, BasketProduct $hasNonVatable, BasketProduct $standard, Price $price)
    {
        $withoutVat->notApplyVat = TRUE;
        $withoutVat->nonVatableValue = 0;
        $withoutVat->getTotalPrice()->willReturn(1);

        $hasNonVatable->notApplyVat = FALSE;
        $hasNonVatable->nonVatableValue = 1;
        $hasNonVatable->getTotalPrice()->willReturn(2);

        $standard->notApplyVat = FALSE;
        $standard->nonVatableValue = 0;
        $standard->getTotalPrice()->willReturn(1);

        $this->priceFactory->create(4, 2, 1)->willReturn($price);

        $this->calculatePrice([$withoutVat, $hasNonVatable, $standard], 1)->shouldReturn($price);
    }
}
