<?php

namespace spec\BasketModule\Factories;

use BasketModule\Factories\PriceFactory;
use BasketModule\ValueObject\Price;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use VatModule\Calculators\VatCalculator;

/**
 * @mixin PriceFactory
 */
class PriceFactorySpec extends ObjectBehavior
{
    /**
     * @var VatCalculator
     */
    private $vatCalculator;

    function let(VatCalculator $vatCalculator)
    {
        $this->vatCalculator = $vatCalculator;
        $this->beConstructedWith($this->vatCalculator);
    }

    function it_can_create_price()
    {
        $subtotal = 10;
        $nonVatable = 2;
        $discount = 3;
        $vat = 5;

        $this->vatCalculator->getVatFromSubtotal($subtotal - $nonVatable - $discount)->willReturn($vat);

        $this->create($subtotal, $nonVatable, $discount)->shouldBeLike(new Price($subtotal, $vat, $subtotal + $vat, $discount, $nonVatable));
    }
}
