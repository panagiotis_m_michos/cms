<?php

namespace spec\BasketModule\BasketValidators;

use Basket;
use BasketModule\BasketValidators\IValidator;
use BasketModule\BasketValidators\MultipleValidatorsValidator;
use BasketModule\BasketValidators\ValidationResult;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin MultipleValidatorsValidator
 */
class MultipleValidatorsValidatorSpec extends ObjectBehavior
{
    /**
     * @var IValidator
     */
    private $validator1;

    /**
     * @var IValidator
     */
    private $validator2;

    function let(IValidator $validator1, IValidator $validator2)
    {
        $this->validator1 = $validator1;
        $this->validator2 = $validator2;

        $this->beConstructedWith([$validator1, $validator2]);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(\BasketModule\BasketValidators\MultipleValidatorsValidator::class);
    }

    function it_can_validate(Basket $basket, ValidationResult $validationResult1, ValidationResult $validationResult2)
    {
        $this->validator1->validate($basket)->willReturn($validationResult1);
        $validationResult1->getErrors()->willReturn(['Message 1']);
        $this->validator2->validate($basket)->willReturn($validationResult2);
        $validationResult2->getErrors()->willReturn(['Message 2', 'Message 3']);

        $validationResult = $this->validate($basket);

        $validationResult->shouldBeAnInstanceOf(ValidationResult::class);
        $validationResult->getErrors()->shouldReturn(['Message 1', 'Message 2', 'Message 3']);
    }
}
