<?php

namespace spec\BasketModule\BasketValidators;

use BasketModule\BasketValidators\FivePoundFormationValidator;
use BasketModule\BasketValidators\IValidator;
use Entities\Customer;
use Exception;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin FivePoundFormationValidator
 */
class FivePoundFormationValidatorSpec extends ObjectBehavior
{
    /**
     * @var Customer
     */
    private $customer;

    function let(Customer $customer)
    {
        $this->customer = $customer;
        $this->beConstructedWith($this->customer);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(FivePoundFormationValidator::class);
        $this->shouldHaveType(IValidator::class);
    }
}
