<?php

namespace spec\BasketModule\BasketValidators;

use BasketModule\BasketValidators\ValidationResult;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin ValidationResult
 */
class ValidationResultSpec extends ObjectBehavior
{
    private $errors = ['Error 1', 'Error 2'];

    function let()
    {
        $this->beConstructedWith($this->errors);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(ValidationResult::class);
    }

    function it_will_return_errors()
    {
        $this->getErrors()->shouldReturn($this->errors);
    }
}
