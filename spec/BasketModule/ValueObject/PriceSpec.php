<?php

namespace spec\BasketModule\ValueObject;

use BasketModule\ValueObject\Price;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin Price
 */
class PriceSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith(1, 2, 3, 4, 5);
    }

    function it_is_initializable()
    {
        $this->getSubtotal()->shouldReturn(1);
        $this->getVat()->shouldReturn(2);
        $this->getTotal()->shouldReturn(3);
        $this->getDiscount()->shouldReturn(4);
        $this->getNonVatable()->shouldReturn(5);
    }
}
