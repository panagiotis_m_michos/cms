<?php

namespace spec\MailgunModule\Exceptions;

use MailgunModule\Exceptions\MailgunApiCommunicationException;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use RuntimeException;

/**
 * @mixin MailgunApiCommunicationException
 */
class MailgunApiCommunicationExceptionSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith(400);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(MailgunApiCommunicationException::class);
        $this->shouldHaveType(RuntimeException::class);
    }
}
