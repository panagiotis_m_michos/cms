<?php

namespace spec\MailgunModule\Factories;

use Entities\Customer;
use MailgunModule\Entities\MailgunEvent;
use MailgunModule\Factories\MailgunResponseObjectFactory;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Services\CustomerService;

/**
 * @mixin MailgunResponseObjectFactory
 */
class MailgunResponseObjectFactorySpec extends ObjectBehavior
{
    /**
     * @var CustomerService
     */
    private $customerService;

    function let(CustomerService $customerService)
    {
        $this->customerService = $customerService;
        $this->beConstructedWith($this->customerService);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(MailgunResponseObjectFactory::class);
    }

    function it_can_build_mailgun_event_from_json_data(Customer $customer)
    {
        $data = [
            'id' => 'id',
            'event' => 'accepted',
            'recipient' => 'a@b.c',
            'timestamp' => 1,
            'user-variables' => [
                'my-custom-data' => json_encode([
                    'emailTemplateId' => 1,
                    'customerId' => 2
                ])
            ]
        ];

        $this->customerService->getCustomerById(2)->willReturn($customer);

        /** @var MailgunEvent $event */
        $event = $this->buildMailgunEvent($data);

        $event->shouldHaveType(MailgunEvent::class);
        $event->getCustomer()->shouldReturn($customer);
        $event->getEmailTemplateId()->shouldReturn(1);
        $event->getStatus()->shouldReturn('accepted');
        $event->getRecipient()->shouldReturn('a@b.c');
    }
}
