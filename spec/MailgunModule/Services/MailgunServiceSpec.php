<?php

namespace spec\MailgunModule\Services;

use DateTime;
use Mailgun\Mailgun;
use MailgunModule\Entities\MailgunEvent;
use MailgunModule\Exceptions\MailgunApiCommunicationException;
use MailgunModule\Factories\MailgunResponseObjectFactory;
use MailgunModule\Services\MailgunService;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin MailgunService
 */
class MailgunServiceSpec extends ObjectBehavior
{
    /**
     * @var MailgunResponseObjectFactory
     */
    private $responseObjectFactory;

    /**
     * @var string
     */
    private $domain = 'http://domain.com';

    /**
     * @var Mailgun
     */
    private $mailgun;

    function let(Mailgun $mailgun, MailgunResponseObjectFactory $responseObjectFactory)
    {
        $this->mailgun = $mailgun;
        $this->responseObjectFactory = $responseObjectFactory;
        $this->beConstructedWith($this->mailgun, $this->domain, $this->responseObjectFactory);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(MailgunService::class);
    }

    function it_can_get_latest_events(MailgunEvent $event)
    {
        $dateTime = new DateTime();

        $item = [
            'event' => 'accepted',
            'custom-variables' => [
                'emailTemplateId' => 1,
                'customerId' => 2
            ]
        ];

        $data = [
            'http_response_body' => [
                'items' => [$item]
            ],
            'http_response_code'=> 200
        ];

        $apiData = json_decode(json_encode($data));

        $this->mailgun->get(
            $this->domain . '/events',
            Argument::withEntry('begin', $dateTime->format(DateTime::RFC2822))
        )->willReturn($apiData);

        $this->responseObjectFactory->buildMailgunEvent($apiData->http_response_body->items[0])->willReturn($event);

        $this->getLatestEvents($dateTime)->current()->shouldReturn($event);
    }

    function it_will_throw_when_response_code_is_not_200()
    {
        $dateTime = new DateTime();

        $this->mailgun
            ->get(
                $this->domain . '/events',
                Argument::withEntry('begin', $dateTime->format(DateTime::RFC2822))
            )
            ->willReturn((object) ['http_response_code' => 400]);

        $this->getLatestEvents($dateTime)->shouldThrow(new MailgunApiCommunicationException(400))->duringCurrent();
    }
}
