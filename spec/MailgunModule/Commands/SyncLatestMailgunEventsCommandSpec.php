<?php

namespace spec\MailgunModule\Commands;

use Cron\Commands\ICommand;
use Cron\INotifier;
use DateTime;
use MailgunModule\Commands\SyncLatestMailgunEventsCommand;
use MailgunModule\Entities\MailgunEvent;
use MailgunModule\Repositories\MailgunEventRepository;
use MailgunModule\Services\MailgunService;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Log\LoggerInterface;

/**
 * @mixin SyncLatestMailgunEventsCommand
 */
class SyncLatestMailgunEventsCommandSpec extends ObjectBehavior
{
    function it_can_sync_missing_events(
        LoggerInterface $logger,
        INotifier $notifier,
        MailgunService $mailgunService,
        MailgunEventRepository $mailgunEventRepository,
        MailgunEvent $mailgunEvent,
        MailgunEvent $newEvent
    )
    {
        $this->beConstructedWith($logger, $notifier, $mailgunService, $mailgunEventRepository);
        $this->shouldHaveType(ICommand::class);

        $createdAt = new DateTime();
        $syncFrom = clone $createdAt;
        $syncFrom->modify('-5 minutes');
        $compareFrom = clone $syncFrom;
        $compareFrom->modify('-1 minute');

        $mailgunEvent->getCreatedAt()->willReturn($createdAt);
        $mailgunEvent->getUniqueId()->willReturn('a');
        $newEvent->getUniqueId()->willReturn('b');

        $mailgunEventRepository->getLatestEvent()->willReturn($mailgunEvent);
        $mailgunEventRepository->findBetween($compareFrom, Argument::any())->willReturn([$mailgunEvent]);
        $mailgunService->getLatestEvents($syncFrom)->willReturn([$newEvent]);
        $mailgunEventRepository->saveEntity($newEvent)->shouldBeCalled();

        $this->execute();
    }
}
