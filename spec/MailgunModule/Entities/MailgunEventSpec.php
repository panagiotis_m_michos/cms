<?php

namespace spec\MailgunModule\Entities;

use MailgunModule\Entities\MailgunEvent;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class MailgunEventSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(MailgunEvent::class);
    }
}
