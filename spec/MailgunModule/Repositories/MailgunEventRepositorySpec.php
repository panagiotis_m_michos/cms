<?php

namespace spec\MailgunModule\Repositories;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use MailgunModule\Repositories\MailgunEventRepository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin MailgunEventRepository
 */
class MailgunEventRepositorySpec extends ObjectBehavior
{
    function let(EntityManager $em, ClassMetadata $classMetadata)
    {
        $this->beConstructedWith($em, $classMetadata);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(MailgunEventRepository::class);
    }
}
