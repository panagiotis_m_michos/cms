<?php

namespace spec\InfusionsoftModule;

use InfusionsoftModule\EmailPlaceholderReplacer;
use InfusionsoftModule\InfusionsoftScraper;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin EmailPlaceholderReplacer
 */
class EmailPlaceholderReplacerSpec extends ObjectBehavior
{
    function let(InfusionsoftScraper $scraper)
    {
        $this->beConstructedWith('a', 'b', 'c', $scraper);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('InfusionsoftModule\EmailPlaceholderReplacer');
    }
}
