<?php

namespace spec\CompanyFormationModule\Forms\Validators;

use CompanyFormationModule\Forms\Validators\IsSicCode;
use CompanyFormationModule\Forms\Validators\IsSicCodeValidator;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * @mixin IsSicCodeValidator
 */
class IsSicCodeValidatorSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(ConstraintValidator::class);
    }

    function it_can_validate(IsSicCode $constraint)
    {
        $constraint->message = 'asd';

        $this->validate('12345', $constraint)->shouldReturn(NULL);
    }
}
