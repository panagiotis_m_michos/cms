<?php

namespace spec\CompanyFormationModule\Forms\Validators;

use CompanyFormationModule\Forms\Validators\IsSicCode;
use CompanyFormationModule\Forms\Validators\IsSicCodeValidator;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\Validator\Constraint;

/**
 * @mixin IsSicCode
 */
class IsSicCodeSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Constraint::class);
    }
    
    function it_will_return_correct_validator()
    {
        $this->validatedBy()->shouldReturn(IsSicCodeValidator::class);
    }
}
