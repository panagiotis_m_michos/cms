<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Search\CompanyFinder;
use Search\Company;
use PHPUnit_Framework_Assert as Assert;
use Search\CompanyIterator;
use Search\CompanySearchQuery;
use Tests\Helpers\ObjectHelper;

/**
 * Defines application features from the specific context.
 */
class MyCompaniesSearchDomainContext implements Context, SnippetAcceptingContext
{

    /**
     * @var CompanyIterator
     */
    private $searchResults;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * @Transform :company
     * @param $companyName
     * @return mixed
     */
    public function transformStringToCompany($companyName)
    {
        return Company::fromArray(array('companyName' => $companyName));
    }

    /**
     * @Transform :companySearchQuery
     * @param $searchTerm
     * @return mixed
     */
    public function transformStringToCompanySearchQuery($searchTerm)
    {
        $companySearchQuery = new CompanySearchQuery();
        $companySearchQuery->setTerm($searchTerm);
        return $companySearchQuery;
    }

    /**
     * @Given there are following companies
     */
    public function thereAreFollowingCompanies(TableNode $table)
    {
        EntityHelper::emptyTables(array(TBL_CUSTOMERS, TBL_COMPANY));
        $this->customer = ObjectHelper::createCustomer();
        EntityHelper::save(array($this->customer));
        foreach ($table as $row) {
            $company = ObjectHelper::createCompany($this->customer, $row['CompanyName']);
            $company->setCompanyNumber($row['CompanyNumber']);
            EntityHelper::save(array($company));
        }
        $this->companyFinder = EntityHelper::getService(DiLocator::SERVICE_COMPANY_SEARCH);
    }

    /**
     * @When I search for :companySearchQuery
     */
    public function iSearchFor(CompanySearchQuery $companySearchQuery)
    {
        $this->searchResults = $this->companyFinder->findCustomerCompanies($this->customer, $companySearchQuery);
    }

    /**
     * @Then I should see :company
     * @param Company $company
     */
    public function iShouldSee(Company $company)
    {
        $companyResult = $this->searchResults->next();
        Assert::assertNotEmpty($companyResult);
        Assert::assertEquals($company->getName(), $companyResult->getName());
    }

    /**
     * @Then I should not see :company
     */
    public function iShouldNotSee(Company $company)
    {
        $companyResult = $this->searchResults->next();
        Assert::assertEmpty($companyResult);
    }

    /**
     * @Then I should not see any results
     */
    public function iShouldNotSeeAnyResults()
    {
        $companyResult = $this->searchResults->next();
        Assert::assertEmpty($companyResult);
    }

}
