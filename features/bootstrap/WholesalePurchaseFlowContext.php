<?php

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\MinkExtension\Context\MinkContext;

class WholesalePurchaseFlowContext extends MinkContext implements Context, SnippetAcceptingContext
{

    /**
     * @Given Feature :featureName is enabled
     */
    public function featureEnable($featureName)
    {
        $this->getSession()->setCookie($featureName, '1');
    }
}