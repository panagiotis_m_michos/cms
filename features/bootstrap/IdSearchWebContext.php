<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use IdCheckModule\Tests\Helpers\IdCheckTestHelper;
use Utils\Date;

/**
 * Defines application features from the specific context.
 */
class IdSearchWebContext extends PaymentContext
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /** @BeforeScenario */
    public function before($event)
    {
        EntityHelper::emptyTables(array(TBL_CUSTOMERS, TBL_COMPANY, TBL_ID_CHECKS));
    }

    /**
     * @Given Id check page
     */
    public function idCheckPage()
    {
        $this->iLogIn();
        $this->idCheckIsRequired();
        $this->visit('/id-check/choose-id/#idCheck');
    }

    /**
     * @Given Id check is required
     */
    public function idCheckIsRequired()
    {
        $this->customer->markIdCheckRequired();
        EntityHelper::save(array($this->customer));
    }

    /**
     * @Given Id check failed :arg1 times
     */
    public function idCheckFailedTimes($arg1)
    {
        $ids[] = IdCheckTestHelper::failure($this->customer);
        $ids[] = IdCheckTestHelper::failure($this->customer);
        $ids[] = IdCheckTestHelper::failure($this->customer);
        EntityHelper::save($ids);
    }

    /**
     * @Given I have passed Id check
     */
    public function iHavePassedIdCheck()
    {
        $this->customer->markIdCheckNotRequired();
        $this->customer->setDtIdValidated(new Date('now'));
        EntityHelper::save(array($this->customer));
    }

    /**
     * @Given I search for address :address
     */
    public function iSearchForAddress($address)
    {
        $elementStr = 'input[name="idCheckForm[address][search]"]';
        $element = $this->findElement($elementStr);
        $element->setValue($address);
        //$script = "$('{$elementStr}')[0].dispatchEvent(new Event(\"focus\"));";
        $script = "$('{$elementStr}')[0].dispatchEvent(new KeyboardEvent(\"keyup\"));";
        $this->getSession()->wait(1000);
        $this->getSession()->evaluateScript($script);
    }
}
