<?php

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Entities\Customer;
use Entities\Company;
use PHPUnit_Framework_Assert as test;
use Services\CompanyService;
use TestModule\Annotations\Inject;
use TestModule\Behat\ObjectInjectorContext;
use TestModule\Helpers\DatabaseHelper;

class CompanySettingsContext extends ObjectInjectorContext implements Context, SnippetAcceptingContext
{
    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var DatabaseHelper
     */
    private $databaseHelper;


    /**
     * @var Company
     */
    private $company;

    /**
     * @Transform :bool
     */
    public function transformBool($string)
    {
        switch ($string) {
            case 'enabled':
            case 'enable':
                return TRUE;
            case 'disabled':
            case 'disable':
                return FALSE;
        }
        throw new InvalidArgumentException(sprintf('Can not convert to bool %s', $string));
    }

    /**
     * @Inject({"services.company_service", "test_module.database_helper"})
     * @param CompanyService $companyService
     * @param DatabaseHelper $databaseHelper
     */
    public function setupWithContainer(CompanyService $companyService, DatabaseHelper $databaseHelper)
    {
        $this->databaseHelper = $databaseHelper;
        $this->companyService = $companyService;
        $this->databaseHelper->emptyTables([TBL_CUSTOMERS, TBL_COMPANIES, TBL_COMPANY_SETTINGS]);
    }

    /**
     * @Given I am customer :id
     */
    public function iAmCustomer($id)
    {
        $this->customer = new Customer($id, 'test');
        $this->databaseHelper->saveEntity($this->customer);
    }

    /**
     * @Given I have company :id
     */
    public function iHaveCompany($id)
    {
        $this->company = new Company($this->customer, $id);
        $this->databaseHelper->saveEntity($this->company);
    }

    /**
     * @Then Company dismissed bank offer setting should be :bool
     */
    public function myCompanyReportTrackingSettingShouldBe($bool)
    {
        test::assertEquals($bool, $this->company->hasDismissedCashBackPromotion());
    }

    /**
     * @When I dismiss bank offer for company
     */
    public function iAutoTrackingSetting()
    {
        $this->companyService->dismissBankOffer($this->company);
    }
}