<?php

use Behat\Mink\Exception\ElementNotFoundException;
use PaymentModule\Contracts\IPaymentData;
use PaymentModule\Forms\PurchaseConfirmationForm;
use Services\Payment\TokenService;
use Tests\Helpers\ObjectHelper;
use tests\helpers\VoucherHelper;
use SagePay\Token\Exception\FailedResponse;
use SagePay\Token\SageFactory;
use Services\Payment\SageService;
use PHPUnit_Framework_Assert as Assert;

/**
 * Defines application features from the specific context.
 */
class PaymentContext extends WebContext
{
    /**
     * @Given I add product :item to the basket
     */
    public function iAddProductToTheBasket($item)
    {
        $this->visit('/page130en.html?add=' . $item);
        $this->visit('/page131en.html');
    }

    /**
     * @Given I apply existing voucher :code with :discount percent discount
     */
    public function iApplyExistingVoucher($code, $discount)
    {
        $voucher = VoucherHelper::createDiscount($code, $discount);
        $voucher->save();
        $this->fillField('voucherCode', $voucher->voucherCode);
        $this->pressButton('addVoucher');
    }

    /**
     * @Given I use existing voucher :code with :discount percent discount
     */
    public function iUseExistingVoucher($code, $discount)
    {
        $voucher = VoucherHelper::createDiscount($code, $discount);
        $voucher->save();
        $this->visit($this->getSession()->getCurrentUrl() . '&redeem=1');
        // selenium looks for first link and if its not visible throws exception
        $this->getSession()->executeScript('$(".hidden-lg.hidden-md.hidden-sm").removeClass("hidden-sm hidden-lg hidden-md")');
        $this->clickLink('I have a voucher');
        //$this->getSession()->wait(5000, '$("[name=\"VoucherForm[voucherCode]\"]:visible").length > 0');
        $this->fillField('VoucherForm[voucherCode]', $voucher->voucherCode);
        $this->pressButton('Apply');
    }

    /**
     * @Given I select my company
     */
    public function iSelectMyCompany()
    {
        $this->selectOption('company-number-select_0', $this->company->getId());
        $this->getSession()->wait(8000, '$("#payment:disabled").length === 0');
    }

    /**
     * @Given I am on payment page with products :item
     */
    public function iAmOnPaymentPageWithProducts($item)
    {
        $this->iAddProductToTheBasket($item);
        if ($this->company) {
            $this->iSelectMyCompany();
        }
        $this->featureEnable('payment_upgrade');
        $this->pressButton('Checkout');
        $this->assertPageAddress('/payment/');
    }

    /**
     * @TODO adapt to packages
     * @Given I am on payment page with package :item and companyName :companyName
     */
    public function iAmOnPaymentPageWithPackage($item, $companyName)
    {
        $this->visit('/page1245en.html?package_id=' . $item);
        $this->fillField('companyName', $companyName);
        $this->pressButton('search');
        $this->pressButton('Proceed');
        $this->featureEnable('payment_upgrade');
        $this->visit('/page131en.html');
        $this->assertPageAddress('/payment/');
    }

    /**
     * @Given I complete sage form with :key
     */
    public function iCompleteSageForm($key)
    {
        $this->fillField('Cardholder name', 'Johny Bravo');
        $this->selectOption('Card type', 'VISA');
        $this->fillField('Card number', '4929000000006');
        $this->selectOption('payment_form[cardDetails][expiryDate][month]', '3');
        $this->selectOption('payment_form[cardDetails][expiryDate][year]', '2022');
        $this->fillField('Security code', '123');

        if ($key === 'address') {
            $this->fillField('Address Line 1', '123');
            $this->fillField('Town', '123');
            $this->fillField('Postcode', '123');
            $this->selectOption('Country', 'GB');
        }

        $this->pressButton('payment-submit-button');
        $this->iComplete3dAuthenticationForm();
    }

    /**
     * @Given I complete 3d authentication form
     */
    public function iComplete3dAuthenticationForm()
    {
        $this->getSession()->switchToIFrame('sage-iframe');
        $this->fillField('password', 'password');
        $this->submitButton();
        $this->getSession()->switchToWindow();
        $this->getSession()->wait(20000, '$("iframe[name=sage-iframe]").length === 0');
    }

    public function submitButton()
    {
        $submit = $this->getSession()->getPage()->find('css', 'input[type=submit]');
        if (!$submit) {
            return new ElementNotFoundException($this->getSession(), 'input[type=submit]', 'css');
        }
        $submit->click();
    }

    /**
     * @Given I use email :email
     */
    public function iUseEmail($email)
    {
        $this->fillField('Email', $email);
        $this->pressButton("Create account or log in");
        $this->getSession()->wait(2000, '$("button:contains(\"Create account or log in\"):disabled")');
    }

    /**
     * @Given Customer :customerKey with company :companyNumber exists
     */
    public function customerWithCompanyExists($customerKey, $companyNumber)
    {
        $this->customer = ObjectHelper::createCustomer(TEST_EMAIL1);
        $this->customer->setPassword('password');
        $this->customer->setStatusId(Customer::STATUS_VALIDATED);
        $this->customer->setCredit(1900);
        $this->company = ObjectHelper::createCompany($this->customer, 'Test Company');
        $this->company->setCompanyNumber($companyNumber);
        EntityHelper::save([$this->customer, $this->company]);
    }

    /**
     * @Given I buy :productId with credit
     */
    public function iBuyWithCredit($productId)
    {
        $this->customer->setCredit(99999);
        EntityHelper::save([$this->customer]);

        $this->iAmOnPaymentPageWithProducts($productId);
        $this->completePaymentOnAccount();
    }

    public function completePaymentOnAccount()
    {
        $this->checkOption('Use credit to make this payment');
        $this->pressButton('payment-submit-button');
    }

    /**
     * @Given I should not see auto-renewal checkbox
     */
    public function iShouldNotSeeAutoRenewalCheckbox()
    {
        $this->assertElementNotOnPage(PurchaseConfirmationForm::NAME . '[enableAutoRenewal]');
    }

    /**
     * @When I buy :productId with new card
     */
    public function iBuyWithNewCard($productId)
    {
        $this->iAmOnPaymentPageWithProducts($productId);
        $this->iCompleteSageForm('address');
    }

    /**
     * @When I buy :productId with one-off card
     */
    public function iBuyWithOneOffCard($productId)
    {
        $this->iAmOnPaymentPageWithProducts($productId);
        $this->fillField('payment_form[paymentType]', IPaymentData::TYPE_SAGE_NO_TOKEN);
        $this->iCompleteSageForm('address');
    }

    /**
     * @Given I don't have any payment method saved
     */
    public function iDontHaveAnyPaymentMethodSaved()
    {
        /** @var TokenService $tokenService */
        $tokenService = Registry::$container->get(DiLocator::SERVICE_TOKEN);
        $tokens = $this->customer->getTokens();
        foreach ($tokens as $token) {
            $tokenService->removeEntity($token);
        }
    }

    /**
     * @Then I should see checked auto-renewal checkbox
     */
    public function iShouldSeeCheckedAutoRenewalCheckbox()
    {
        $this->assertCheckboxChecked(PurchaseConfirmationForm::NAME . '[enableAutoRenewal]');
    }

    /**
     * @Then I should see unchecked auto-renewal checkbox
     */
    public function iShouldSeeUncheckedAutoRenewalCheckbox()
    {
        $this->assertCheckboxNotChecked(PurchaseConfirmationForm::NAME . '[enableAutoRenewal]');
    }

    /**
     * @When I complete payment method form with card :cardType
     */
    public function iCompletePaymentMethodFormWith($cardType)
    {

        $this->fillField('cardHolder', 'Johny Bravo');
        if ($cardType === '0006') {
            $this->selectOption('cardType', 'VISA');
            $this->fillField('cardNumber', '4929000000006');
        } elseif ($cardType === '0043') {
            $this->selectOption('cardType', 'MC');
            $this->fillField('cardNumber', '5404 0000 0000 0043');
        }
        $this->selectOption('expiryDate[m]', '3');
        $this->selectOption('expiryDate[y]', '2022');
        $this->fillField('CV2', '123');

        $this->fillField('address1', '123');
        $this->fillField('city', '123');
        $this->fillField('postcode', '123');
        $this->selectOption('country', 'GB');
        $this->pressButton('submit');
    }

    /**
     * @Given I have a token with card :cardNumber
     */
    public function iHaveATokenWithCard($cardNumber)
    {
        $this->visit('/wallet');
        $this->clickLink('Add New Card');
        $this->iCompletePaymentMethodFormWith($cardNumber);
    }

    /**
     * @Given Token :cardNumber can be used for future payments
     */
    public function tokenCanBeUseForFuturePayments($cardNumber)
    {
        $this->token = EntityHelper::getSingle('Entities\Payment\Token', ['customer' => $this->customer, 'cardNumber' => $cardNumber]);
        $sagePayment = SageFactory::createGateway();
        $details = SageService::createRecurringRequest($this->token, 100);
        $response = $sagePayment->makePayment($details, $this->token->getIdentifier());
        Assert::assertInstanceOf('SagePay\Token\Response\Summary', $response);
    }

    /**
     * @Given Token :cardNumber can not be used for future payments
     */
    public function tokenCanNotBeUseForFuturePayments($cardNumber)
    {
        $this->token = EntityHelper::getSingle('Entities\Payment\Token', ['customer' => $this->customer, 'cardNumber' => $cardNumber]);
        $sagePayment = SageFactory::createGateway();
        $details = SageService::createRecurringRequest($this->token, 100);
        try {
            $sagePayment->makePayment($details, $this->token->getIdentifier());
        } catch (FailedResponse $e) {
            return;
        }
        Assert::fail('Make payment should have throw an exception');
    }

}
