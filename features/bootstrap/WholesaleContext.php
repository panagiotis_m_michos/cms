<?php

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use TestModule\Annotations\Inject;
use TestModule\Behat\ObjectInjectorContext;
use PHPUnit_Framework_Assert as phpunit;
use TestModule\Helpers\DatabaseHelper;

/**
 * @property DatabaseHelper databaseHelper
 */
class WholesaleContext extends ObjectInjectorContext implements Context, SnippetAcceptingContext
{
    /**
     * @var Basket
     */
    private $basket;

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @Inject({"basket", "test_module.database_helper"})
     * @param Basket $basket
     * @param DatabaseHelper $databaseHelper
     */
    public function setupWithContainer(Basket $basket, DatabaseHelper $databaseHelper)
    {
        $this->databaseHelper = $databaseHelper;
        $this->basket = $basket;
        $this->databaseHelper->emptyTables([TBL_CUSTOMERS]);
    }

    /**
     * @Given I add wholesale package to the basket
     */
    public function iAddWholesalePackageToTheBasket()
    {
        $this->basket->add(new WholesalePackage(WholesalePackage::PACKAGE_WHOLESALE_PROFESSIONAL));
    }

    /**
     * @When I complete purchase
     */
    public function iCompletePurchase()
    {
        $paymentService = new PaymentService(new Customer(), $this->basket);
        $paymentResponse = new PaymentResponse();
        $paymentResponse->setPaymentHolderEmail('test@madesimplegroup.com');
        $this->customer = $paymentService->processCustomerInformations($paymentResponse);
    }

    /**
     * @Then I should have a role :roleId
     */
    public function iShouldHaveARole($roleId)
    {
        phpunit::assertEquals($roleId, $this->customer->roleId);
    }


}