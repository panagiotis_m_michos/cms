<?php

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Behat\Hook\Scope\AfterStepScope;
use Behat\Mink\Driver\Selenium2Driver;
use Behat\Mink\Element\NodeElement;
use Behat\Mink\Exception\ElementNotFoundException;
use Behat\MinkExtension\Context\MinkContext;
use Entities\Payment\Token;
use Entities\Customer;
use Entities\Company;
use Tests\Helpers\ObjectHelper;
use Utils\Date;

/**
 * Defines application features from the specific context.
 */
class WebContext extends MinkContext implements Context, SnippetAcceptingContext
{

    /**
     * @var Customer
     */
    protected $customer;

    /**
     * @var Company
     */
    protected $company;

    /**
     * @var Token
     */
    protected $token;

    /**
     * Initializes context.
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {

    }

    /** @BeforeScenario */
    public function before($event)
    {
        EntityHelper::emptyTables(
            array_merge(
                [TBL_CUSTOMERS, TBL_COMPANY, TBL_TOKENS, TBL_TRANSACTIONS, TBL_ORDERS, TBL_ORDERS_ITEMS, TBL_VOUCHERS],
                EntityHelper::$tables
            )
        );
    }

    /**
     * @AfterStep
     */
    public function takeScreenshotAfterFailedStep(AfterStepScope $scope)
    {
        if (!Registry::$container->hasParameter('behat.screenshot_dir')) {
            return;
        }
        if (99 === $scope->getTestResult()->getResultCode()) {
            $driver = $this->getSession()->getDriver();
            if (!$driver instanceof Selenium2Driver) {
                return;
            }
            $this->saveScreenshot(NULL, Registry::$container->getParameter('behat.screenshot_dir'));
        }
    }

    /**
     * @Given Feature :featureName is enabled
     */
    public function featureEnable($featureName)
    {
        $this->getSession()->setCookie($featureName, '1');
    }

    /**
     * @Given I am :customerEmail
     */
    public function iAm($customerEmail)
    {
        $this->customer = ObjectHelper::createCustomer($customerEmail, 'password');
        $this->customer->setStatusId(Customer::STATUS_VALIDATED);
        EntityHelper::save([$this->customer]);
        $this->iHaveCompany('2');
    }

    /**
     * @Given I have company :companyNumber
     */
    public function iHaveCompany($companyNumber)
    {
        $this->company = ObjectHelper::createCompany($this->customer, 'Test Company');
        $this->company->setCompanyNumber($companyNumber);
        EntityHelper::save([$this->company]);
    }

    /**
     * @Given I am :customerEmail with credit :credit
     */
    public function iAmWithCredit($customerEmail, $credit)
    {
        $this->iAm($customerEmail);
        $this->customer->setCredit($credit);
        EntityHelper::save([$this->customer]);
    }

    /**
     * @Given I log in
     */
    public function iLogIn()
    {
        $this->visit('/login.html');
        $field = $this->findElement('#email-login');
        $field->setValue($this->customer->getEmail());
        $field = $this->findElement('#password-login');
        $field->setValue('password');
        $this->pressButton('login');
    }

    /**
     * @Given I have a token
     */
    public function iHaveAToken()
    {
        $this->token = ObjectHelper::createToken($this->customer, TRUE, new Date('+1 year'));
        EntityHelper::save([$this->token]);
    }

    /**
     * @param $element
     * @return NodeElement|mixed|null
     * @throws ElementNotFoundException
     */
    public function findElement($element)
    {
        $submit = $this->getSession()->getPage()->find('css', $element);
        if (!$submit) {
            throw new ElementNotFoundException($this->getSession(), $element, 'css');
        }

        return $submit;
    }

    /**
     * @Given I wait for :time
     */
    public function wait($time)
    {
        $this->getSession()->wait($time);
    }

    /**
     * @Given I am logged in customer
     */
    public function iAmLoggedInCustomer()
    {
        $this->iAm('behat_test@madesimplegroup.com');
        $this->iLogIn();
    }

    /**
     * @Given I click on an element with a title :title
     */
    public function iClickOnTitle($title)
    {
        $element = $this->findElement('[title="' . $title . '"]');
        $element->click();
    }

    /**
     * @Given Product :productId has :key :value
     */
    public function modifyProduct($productId, $key, $value)
    {
        /** @var Product $product */
        $product = FNode::getProductById($productId);
        $product->$key = $value;
        $product->save(TRUE);
    }

    /**
     * @Given save screenshot
     */
    public function saveScreenContents()
    {
        $driver = $this->getSession()->getDriver();
        if (!$driver instanceof Selenium2Driver) {
            return;
        }
        $this->saveScreenshot(NULL, Registry::$container->getParameter('behat.screenshot_dir'));
    }

    /**
     * @Given I search for new company name :companyName
     */
    public function searchForCompanyName($companyName)
    {
        $this->visit('/');
        $this->fillField('q', $companyName);
        $this->pressButton('Search');
    }
}
