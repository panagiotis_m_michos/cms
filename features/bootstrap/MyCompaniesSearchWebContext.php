<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;
use Entities\Customer;
use Tests\Helpers\ObjectHelper;
use Utils\Date;

/**
 * Defines application features from the specific context.
 */
class MyCompaniesSearchWebContext extends WebContext
{
    /**
     * @var Customer
     */
    protected $customer;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {

    }

    /**
     * @Given there are following companies
     */
    public function thereAreFollowingCompanies(TableNode $table)
    {
        EntityHelper::emptyTables(array(TBL_CUSTOMERS, TBL_COMPANY));
        $this->customer = ObjectHelper::createCustomer('email@test.com');
        $this->customer->setPassword('password');
        $this->customer->setStatusId(Customer::STATUS_VALIDATED);
            EntityHelper::save(array($this->customer));
        foreach ($table as $row) {
            $company = ObjectHelper::createCompany($this->customer, $row['CompanyName']);
            $company->setCompanyNumber($row['CompanyNumber']);
            $company->setAccountsNextDueDate(Date::createFromFormat('d/m/Y', $row['AccountsDue']));
            $company->setReturnsNextDueDate(Date::createFromFormat('d/m/Y', $row['ReturnsDue']));
            EntityHelper::save(array($company));
        }
    }

    /**
     * @When I search for :searchTerm
     */
    public function iSearchFor($searchTerm)
    {
        $this->fillField('term', $searchTerm);
        $this->pressButton('search');
    }


}
