module.exports = function(grunt) {
    grunt.initConfig({
        riot: {
            options: {
                concat : true
            },
            dist: {
                src: ['www/src/components/*.tag.html', 'www/src/components/**/*.tag.html'],
                dest: 'www/src/components/riot-compiled.js'
            }
        },
        browserify: {
            dist: {
                options: {
                    transform: [
                        ["babelify", {
                            presets: ["es2015"]
                            //loose: "all"
                        }]
                    ],
                    browserifyOptions: {
                        debug: true
                    }
                },
                files: {
                    'www/dist/modules.js': [
                        'www/src/modules/**/*.js',
                        'www/src/components/riot-compiled.js'
                    ]
                }
            }
        },
        watch: {
            riot: {
                files: ['www/src/components/*.tag.html', 'www/src/components/**/*.tag.html'],
                tasks: ['riot']
            },
            es6: {
                files: ["www/src/*.js", "www/src/**/*.js", "www/src/components/riot-compiled.js"],
                tasks: ["browserify"]
            }
        }
    });

    grunt.loadNpmTasks('grunt-riot');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks("grunt-browserify");

    grunt.registerTask("default", ["watch"]);
    grunt.registerTask("build", ["riot", "browserify"]);
};