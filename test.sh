#!/bin/bash

echo $#
while [[ $# > 0 ]]
do
key="$1"
case $key in
    -s|--setup)
    SETUP=true
    shift
    ;;
    -b|--show-browser)
    SHOW_BROWSER=true
    shift
    ;;
    -h|--host)
    HOST="$2"
    shift
    ;;
    *)
    ;;
esac
shift
done

# run selenium
#if [[ $SELENIUM_PATH && -f $SELENIUM_PATH ]]; then
#    ps ax | grep selenium > /dev/null
#    if [[ $? -eq 0 ]]; then
#        command -v xvfb-run > /dev/null 2>&1
#        if [[ $? -eq 0 && ! $SHOW_BROWSER ]]; then
#            xvfb-run java -jar $SELENIUM_PATH > /dev/null 2>&1 &
#        else
#            java -jar $SELENIUM_PATH > /dev/null 2>&1 &
#        fi
#    fi
#else
#    echo "Please add correct export SELENIUM_PATH="/path/to-selenium" to your .bashrc"
#fi
if [[ $SETUP ]]; then
    php console/cm sync:nodes -s -e console
    php console/db migrate -e console
fi

if [[ $HOST ]]; then
    export BEHAT_PARAMS="{\"extensions\" : {\"Behat\\\\MinkExtension\" : {\"base_url\" : \"https://$HOST\"}}}"
fi

php -d memory_limit=256M vendor/bin/phpspec run && vendor/bin/phpunit && vendor/bin/behat --tags ~@standalone

