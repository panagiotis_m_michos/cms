<?php

use Phinx\Migration\AbstractMigration;

class CustomerFile extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $sql = <<<EOD
CREATE TABLE IF NOT EXISTS `cms2_customer_files` (
  `customerFileId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fileKey` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `ext` varchar(20) NOT NULL,
  `customerId` int(11) NOT NULL,
  PRIMARY KEY (`customerFileId`),
  KEY `type` (`customerId`,`type`),
  KEY `fileKey` (`fileKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
EOD;
        $this->query($sql);
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $queue = $this->table('cms2_customer_files');
        $queue->drop();
    }
}