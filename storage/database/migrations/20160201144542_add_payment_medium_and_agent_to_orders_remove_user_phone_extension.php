<?php

use Phinx\Migration\AbstractMigration;

class AddPaymentMediumAndAgentToOrdersRemoveUserPhoneExtension extends AbstractMigration
{
    public function up()
    {
        $this
            ->table(TBL_ORDERS)
            ->addColumn(
                'paymentMediumId',
                'enum',
                [
                    'values' => ['ONSITE', 'PHONE', 'AUTO_RENEWAL'],
                    'default' => 'ONSITE',
                    'after' => 'refundCustomerSupportLogin',
                ]
            )
            ->addColumn(
                'agentId',
                'integer',
                [
                    'null' => TRUE,
                    'after' => 'paymentMediumId'
                ]
            )
            ->update();

        $this->execute(
            sprintf(
                'UPDATE %s SET paymentMediumId = "%s" WHERE description = "%s"',
                TBL_ORDERS,
                'AUTO_RENEWAL',
                'Auto renewed services'
            )
        );

        $this
            ->table(TBL_USERS)
            ->removeColumn('phone_extension')
            ->update();
    }

    public function down()
    {
        $this
            ->table(TBL_ORDERS)
            ->removeColumn('paymentMediumId')
            ->removeColumn('agentId')
            ->update();

        $this
            ->table(TBL_USERS)
            ->addColumn(
                'phone_extension',
                'string',
                [
                    'length' => 6,
                    'null' => TRUE,
                    'after' => 'editor_id'
                ]
            )
            ->update();
    }
}
