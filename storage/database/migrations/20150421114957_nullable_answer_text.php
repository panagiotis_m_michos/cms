<?php

use Phinx\Migration\AbstractMigration;

class NullableAnswerText extends AbstractMigration
{
    public function change()
    {
        $this->table(TBL_ANSWERS)->changeColumn('text', 'string', ['null' => TRUE])->update();
    }
}
