<?php

use Phinx\Migration\AbstractMigration;

class CreatePaymentPeriodsRequestTable extends AbstractMigration
{
    public function change()
    {
        $this->table(
            'ch_payment_periods_request',
            [
                'id' => 'paymentPeriodsRequestId',
                'primaryKey' => 'paymentPeriodsRequestId',
            ]
        )
            ->addColumn('customerId', 'integer')
            ->addColumn('envelopeId', 'integer')
            ->addColumn('companyNumber', 'string', ['length' => 8])
            ->addColumn('authenticationCode', 'string', ['length' => 6])
            ->addIndex('customerId')
            ->addIndex('envelopeId')
            ->create();
    }
}
