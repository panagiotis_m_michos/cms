<?php

use Phinx\Migration\AbstractMigration;

class AddToResendColumnToFormSubmissions extends AbstractMigration
{
    public function change()
    {
        $this
            ->table(TBL_FORM_SUBMISSIONS)
            ->addColumn('toResend', 'boolean')
            ->update();
    }
}
