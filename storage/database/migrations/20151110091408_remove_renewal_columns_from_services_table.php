<?php

use Phinx\Migration\AbstractMigration;

class RemoveRenewalColumnsFromServicesTable extends AbstractMigration
{
    public function up()
    {
        $this
            ->table(TBL_SERVICES)
            ->removeColumn('isAutoRenewalEnabled')
            ->removeColumn('renewalTokenId')
            ->update();
    }

    public function down()
    {
        $this->table('cms2_services')
            ->addColumn(
                'isAutoRenewalEnabled',
                'boolean',
                array(
                    'after' => 'stateId',
                    'default' => FALSE,
                )
            )
            ->addColumn(
                'renewalTokenId',
                'integer',
                array(
                    'null' => TRUE,
                    'after' => 'isAutoRenewalEnabled',
                    'default' => NULL,
                )
            )
            ->update();
    }
}
