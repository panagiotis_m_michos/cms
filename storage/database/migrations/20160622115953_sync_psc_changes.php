<?php

use Phinx\Migration\AbstractMigration;

class SyncPscChanges extends AbstractMigration
{
    public function up()
    {
        $this
            ->table('ch_company_member')
            ->changeColumn(
                'type',
                'enum',
                [
                    'values' => ['DIR', 'SEC', 'SUB', 'PSC'],
                    'after' => 'company_id',
                ]
            )
            ->addColumn(
                'ownership_of_shares',
                'enum',
                [
                    'values' => [
                        'OWNERSHIPOFSHARES_25TO50PERCENT',
                        'OWNERSHIPOFSHARES_50TO75PERCENT',
                        'OWNERSHIPOFSHARES_75TO100PERCENT',
                        'OWNERSHIPOFSHARES_25TO50PERCENT_AS_FIRM',
                        'OWNERSHIPOFSHARES_50TO75PERCENT_AS_FIRM',
                        'OWNERSHIPOFSHARES_75TO100PERCENT_AS_FIRM',
                        'OWNERSHIPOFSHARES_25TO50PERCENT_AS_TRUST',
                        'OWNERSHIPOFSHARES_50TO75PERCENT_AS_TRUST',
                        'OWNERSHIPOFSHARES_75TO100PERCENT_AS_TRUST',
                        'RIGHTTOSHARESURPLUSASSETS_25TO50PERCENT',
                        'RIGHTTOSHARESURPLUSASSETS_50TO75PERCENT',
                        'RIGHTTOSHARESURPLUSASSETS_75TO100PERCENT',
                        'RIGHTTOSHARESURPLUSASSETS_25TO50PERCENT_AS_FIRM',
                        'RIGHTTOSHARESURPLUSASSETS_50TO75PERCENT_AS_FIRM',
                        'RIGHTTOSHARESURPLUSASSETS_75TO100PERCENT_AS_FIRM',
                        'RIGHTTOSHARESURPLUSASSETS_25TO50PERCENT_AS_TRUST',
                        'RIGHTTOSHARESURPLUSASSETS_50TO75PERCENT_AS_TRUST',
                        'RIGHTTOSHARESURPLUSASSETS_75TO100PERCENT_AS_TRUST',
                    ],
                    'after' => 'num_shares',
                    'null' => TRUE,
                ]
            )
            ->addColumn(
                'ownership_of_voting_rights',
                'enum',
                [
                    'values' => [
                        'VOTINGRIGHTS_25TO50PERCENT',
                        'VOTINGRIGHTS_50TO75PERCENT',
                        'VOTINGRIGHTS_75TO100PERCENT',
                        'VOTINGRIGHTS_25TO50PERCENT_AS_FIRM',
                        'VOTINGRIGHTS_50TO75PERCENT_AS_FIRM',
                        'VOTINGRIGHTS_75TO100PERCENT_AS_FIRM',
                        'VOTINGRIGHTS_25TO50PERCENT_AS_TRUST',
                        'VOTINGRIGHTS_50TO75PERCENT_AS_TRUST',
                        'VOTINGRIGHTS_75TO100PERCENT_AS_TRUST'
                    ],
                    'after' => 'ownership_of_shares',
                    'null' => TRUE,
                ]
            )
            ->addColumn(
                'right_to_appoint_and_remove_directors',
                'enum',
                [
                    'values' => [
                        'RIGHTTOAPPOINTANDREMOVEDIRECTORS',
                        'RIGHTTOAPPOINTANDREMOVEDIRECTORS_AS_FIRM',
                        'RIGHTTOAPPOINTANDREMOVEDIRECTORS_AS_TRUST',
                        'RIGHTTOAPPOINTANDREMOVEMEMBERS',
                        'RIGHTTOAPPOINTANDREMOVEMEMBERS_AS_FIRM',
                        'RIGHTTOAPPOINTANDREMOVEMEMBERS_AS_TRUST',
                    ],
                    'after' => 'ownership_of_voting_rights',
                    'null' => TRUE,
                ]
            )
            ->addColumn(
                'significant_influence_or_control',
                'enum',
                [
                    'values' => [
                        'SIGINFLUENCECONTROL',
                        'SIGINFLUENCECONTROL_AS_FIRM',
                        'SIGINFLUENCECONTROL_AS_TRUST',
                    ],
                    'after' => 'right_to_appoint_and_remove_directors',
                    'null' => TRUE,
                ]
            )
            ->addColumn(
                'psc_statement_notification',
                'enum',
                [
                    'values' => [
                        'PSC_EXISTS_BUT_NOT_IDENTIFIED',
                        'PSC_DETAILS_NOT_CONFIRMED',
                        'PSC_CONTACTED_BUT_NO_RESPONSE',
                        'RESTRICTIONS_NOTICE_ISSUED_TO_PSC',
                    ],
                    'after' => 'significant_influence_or_control',
                    'null' => TRUE,
                ]
            )
            ->addColumn(
                'notification_date',
                'date',
                [
                    'after' => 'psc_statement_notification',
                    'null' => TRUE,
                ]
            )
            ->update();

        $this
            ->table('ch_company')
            ->addColumn(
                'no_psc_reason',
                'enum',
                [
                    'values' => [
                        'NO_INDIVIDUAL_OR_ENTITY_WITH_SIGNFICANT_CONTROL',
                        'STEPS_TO_FIND_PSC_NOT_YET_COMPLETED',
                    ],
                    'after' => 'cash_back_amount',
                    'null' => TRUE,
                ]
            )
            ->update();
    }

    public function down()
    {
        $this
            ->table('ch_company_member')
            ->changeColumn(
                'type',
                'enum',
                [
                    'values' => ['DIR', 'SEC', 'SUB'],
                    'after' => 'company_id',
                ]
            )
            ->removeColumn('ownership_of_shares')
            ->removeColumn('ownership_of_voting_rights')
            ->removeColumn('right_to_appoint_and_remove_directors')
            ->removeColumn('significant_influence_or_control')
            ->removeColumn('psc_statement_notification')
            ->removeColumn('notification_date')
            ->update();

        $this
            ->table('ch_company')
            ->removeColumn('no_psc_reason')
            ->update();
    }
}
