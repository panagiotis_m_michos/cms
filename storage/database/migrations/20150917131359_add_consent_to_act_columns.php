<?php

use Phinx\Migration\AbstractMigration;

class AddConsentToActColumns extends AbstractMigration
{
    public function up()
    {
        $this->execute("ALTER TABLE `ch_incorporation_member` ADD `consentToAct` tinyint(1) NOT NULL DEFAULT 1 AFTER `authentication`;");
        $this->execute("ALTER TABLE `ch_officer_appointment` ADD `consentToAct` tinyint(1) NOT NULL DEFAULT 1 AFTER `authentication`;");
        $this->execute("ALTER TABLE `ch_officer_change` ADD `consentToAct` tinyint(1) NOT NULL DEFAULT 1 AFTER `authentication`;");
    }

    public function down()
    {
        $this->execute("ALTER TABLE `ch_incorporation_member` DROP `consentToAct`");
        $this->execute("ALTER TABLE `ch_officer_appointment` DROP `consentToAct`");
        $this->execute("ALTER TABLE `ch_officer_change` DROP `consentToAct`");
    }
}
