<?php

use Phinx\Migration\AbstractMigration;

class NormalizeDurationStringsToMonths extends AbstractMigration
{
    public function up()
    {
        $this->table('cms2_services')->changeColumn('initialDuration', 'string');
        $this->execute('UPDATE cms2_services SET initialDuration = "+12 months" WHERE initialDuration = 12');
        $this->execute('UPDATE cms2_properties SET `value` = "+12 months" WHERE `name` = "duration" and `value` = "+12 month"');
    }

    public function down()
    {
        $this->execute('UPDATE cms2_services SET initialDuration = 12 WHERE initialDuration = "+12 months"');
        $this->table('cms2_services')->changeColumn('initialDuration', 'integer');
        $this->execute('UPDATE cms2_properties SET `value` = "+12 month" WHERE `name` = "duration" and `value` = "+12 months"');
    }
}
