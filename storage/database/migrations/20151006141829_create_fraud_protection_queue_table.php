<?php

use Phinx\Migration\AbstractMigration;

class CreateFraudProtectionQueueTable extends AbstractMigration
{
    public function up()
    {
        $this->execute(
            'CREATE TABLE `cms2_fraud_protection_queue` (
          `fraudProtectionQueueId` INT(11) NOT NULL AUTO_INCREMENT,
          `companyId` INT(11) NOT NULL,
          `dtc` DATETIME NOT NULL,
          PRIMARY KEY (`fraudProtectionQueueId`)
          ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;'
        );
    }

    public function down()
    {
        $this->execute(
            'DROP TABLE IF EXISTS `cms2_fraud_protection_queue`;'
        );
    }
}
