<?php

use Phinx\Migration\AbstractMigration;

class AddDtcDtmBackToAddressChange extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->table(TBL_ADDRESS_CHANGES)
            ->addColumn('dtc', 'datetime')
            ->addColumn('dtm', 'datetime')
            ->update();

        $this->execute(
            'UPDATE ch_address_change c
            INNER JOIN ch_form_submission f USING (form_submission_id)
            SET c.dtc = f.dtc, c.dtm = f.dtm'
        );
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->table(TBL_ADDRESS_CHANGES)
            ->removeColumn('dtc')
            ->removeColumn('dtm')
            ->update();
    }
}
