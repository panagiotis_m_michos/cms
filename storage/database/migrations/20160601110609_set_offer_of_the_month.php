<?php

use Phinx\Migration\AbstractMigration;

class SetOfferOfTheMonth extends AbstractMigration
{
    public function up()
    {
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (379, 'offerOfTheMonthProductId', 331, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (436, 'offerOfTheMonthProductId', 331, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (1175, 'offerOfTheMonthProductId', 331, 1, 1, NOW(), NOW())");

        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (898, 'offerOfTheMonthProductId', 331, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (899, 'offerOfTheMonthProductId', 331, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (1022, 'offerOfTheMonthProductId', 331, 1, 1, NOW(), NOW())");

        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (1430, 'offerOfTheMonthProductId', 331, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (1598, 'offerOfTheMonthProductId', 331, 1, 1, NOW(), NOW())");

        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (1313, 'offerOfTheMonthProductId', 331, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (1314, 'offerOfTheMonthProductId', 331, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (1315, 'offerOfTheMonthProductId', 331, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (1316, 'offerOfTheMonthProductId', 331, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (1317, 'offerOfTheMonthProductId', 331, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (1684, 'offerOfTheMonthProductId', 331, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (1694, 'offerOfTheMonthProductId', 331, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (1688, 'offerOfTheMonthProductId', 331, 1, 1, NOW(), NOW())");
    }

    public function down()
    {
        $this->query("DELETE FROM cms2_properties WHERE name = 'offerOfTheMonthProductId'");
    }
}
