<?php

use Phinx\Migration\AbstractMigration;

class CompanySettings extends AbstractMigration
{
    public function change()
    {
        $this->table(TBL_COMPANY_SETTINGS, ['id' => 'companySettingId'])
            ->addColumn('settingKey', 'string')
            ->addColumn('settingValue', 'text', ['null' => TRUE])
            ->addColumn('companyId', 'integer')
            ->addColumn('dtc', 'datetime')
            ->addColumn('dtm', 'datetime')
            ->addIndex('settingKey')
            ->addIndex('companyId')
            ->create();
    }
}