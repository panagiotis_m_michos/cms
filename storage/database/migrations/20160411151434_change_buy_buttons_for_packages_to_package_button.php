<?php

use Phinx\Migration\AbstractMigration;

class ChangeBuyButtonsForPackagesToPackageButton extends AbstractMigration
{
    public function up()
    {
        $this->execute("SET NAMES 'utf8'");

        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, '[ui name=\"buy_button\"', '[ui name=\"buy_package_button\"') WHERE node_id = 74");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, '[ui name=\"buy_button\"', '[ui name=\"buy_package_button\"') WHERE node_id = 76");
    }

    public function down()
    {
        $this->execute("SET NAMES 'utf8'");

        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, '[ui name=\"buy_package_button\"', '[ui name=\"buy_button\"') WHERE node_id = 74");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, '[ui name=\"buy_package_button\"', '[ui name=\"buy_button\"') WHERE node_id = 76");
    }
}
