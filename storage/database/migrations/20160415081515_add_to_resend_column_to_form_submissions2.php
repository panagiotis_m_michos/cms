<?php

use Phinx\Migration\AbstractMigration;

class AddToResendColumnToFormSubmissions2 extends AbstractMigration
{
    public function change()
    {
        $this
            ->table(TBL_FORM_SUBMISSIONS)
            ->addColumn('toResend', 'boolean')
            ->update();
    }
}
