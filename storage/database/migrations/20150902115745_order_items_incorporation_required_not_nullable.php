<?php

use Phinx\Migration\AbstractMigration;

class OrderItemsIncorporationRequiredNotNullable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->query('UPDATE cms2_orders_items SET incorporationRequired = 0 WHERE incorporationRequired IS NULL');
        $this->table('cms2_orders_items')->changeColumn('incorporationRequired', 'boolean', ['null' => FALSE, 'default' => 0])->update();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->table('cms2_orders_items')->changeColumn('incorporationRequired', 'boolean', ['null' => TRUE, 'default' => NULL])->update();
    }
}
