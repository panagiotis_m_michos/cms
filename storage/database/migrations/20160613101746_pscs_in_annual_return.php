<?php

use Phinx\Migration\AbstractMigration;

class PscsInAnnualReturn extends AbstractMigration
{
    public function up()
    {
        $this
            ->table('ch_annual_return_officer')
            ->changeColumn(
                'type',
                'enum',
                [
                    'values' => ['DIR', 'SEC', 'MEM', 'PSC'],
                    'after' => 'form_submission_id',
                ]
            )
            ->changeColumn(
                'identification_type',
                'enum',
                [
                    'values' => ['EEA', 'NonEEA', 'PSC'],
                    'after' => 'country_of_residence',
                    'null' => TRUE,
                ]
            )
            ->addColumn(
                'country_or_state',
                'string',
                [
                    'after' => 'legal_form',
                    'length' => 50,
                    'null' => TRUE,
                ]
            )
            ->addColumn(
                'ownership_of_shares',
                'enum',
                [
                    'values' => [
                        'OWNERSHIPOFSHARES_25TO50PERCENT',
                        'OWNERSHIPOFSHARES_50TO75PERCENT',
                        'OWNERSHIPOFSHARES_75TO100PERCENT',
                        'OWNERSHIPOFSHARES_25TO50PERCENT_AS_FIRM',
                        'OWNERSHIPOFSHARES_50TO75PERCENT_AS_FIRM',
                        'OWNERSHIPOFSHARES_75TO100PERCENT_AS_FIRM',
                        'OWNERSHIPOFSHARES_25TO50PERCENT_AS_TRUST',
                        'OWNERSHIPOFSHARES_50TO75PERCENT_AS_TRUST',
                        'OWNERSHIPOFSHARES_75TO100PERCENT_AS_TRUST',
                        'RIGHTTOSHARESURPLUSASSETS_25TO50PERCENT',
                        'RIGHTTOSHARESURPLUSASSETS_50TO75PERCENT',
                        'RIGHTTOSHARESURPLUSASSETS_75TO100PERCENT',
                        'RIGHTTOSHARESURPLUSASSETS_25TO50PERCENT_AS_FIRM',
                        'RIGHTTOSHARESURPLUSASSETS_50TO75PERCENT_AS_FIRM',
                        'RIGHTTOSHARESURPLUSASSETS_75TO100PERCENT_AS_FIRM',
                        'RIGHTTOSHARESURPLUSASSETS_25TO50PERCENT_AS_TRUST',
                        'RIGHTTOSHARESURPLUSASSETS_50TO75PERCENT_AS_TRUST',
                        'RIGHTTOSHARESURPLUSASSETS_75TO100PERCENT_AS_TRUST',
                    ],
                    'after' => 'legal_form',
                    'null' => TRUE,
                ]
            )
            ->addColumn(
                'ownership_of_voting_rights',
                'enum',
                [
                    'values' => [
                        'VOTINGRIGHTS_25TO50PERCENT',
                        'VOTINGRIGHTS_50TO75PERCENT',
                        'VOTINGRIGHTS_75TO100PERCENT',
                        'VOTINGRIGHTS_25TO50PERCENT_AS_FIRM',
                        'VOTINGRIGHTS_50TO75PERCENT_AS_FIRM',
                        'VOTINGRIGHTS_75TO100PERCENT_AS_FIRM',
                        'VOTINGRIGHTS_25TO50PERCENT_AS_TRUST',
                        'VOTINGRIGHTS_50TO75PERCENT_AS_TRUST',
                        'VOTINGRIGHTS_75TO100PERCENT_AS_TRUST'
                    ],
                    'after' => 'ownership_of_shares',
                    'null' => TRUE,
                ]
            )
            ->addColumn(
                'right_to_appoint_and_remove_directors',
                'enum',
                [
                    'values' => [
                        'RIGHTTOAPPOINTANDREMOVEDIRECTORS',
                        'RIGHTTOAPPOINTANDREMOVEDIRECTORS_AS_FIRM',
                        'RIGHTTOAPPOINTANDREMOVEDIRECTORS_AS_TRUST',
                        'RIGHTTOAPPOINTANDREMOVEMEMBERS',
                        'RIGHTTOAPPOINTANDREMOVEMEMBERS_AS_FIRM',
                        'RIGHTTOAPPOINTANDREMOVEMEMBERS_AS_TRUST',
                    ],
                    'after' => 'ownership_of_voting_rights',
                    'null' => TRUE,
                ]
            )
            ->addColumn(
                'significant_influence_or_control',
                'enum',
                [
                    'values' => [
                        'SIGINFLUENCECONTROL',
                        'SIGINFLUENCECONTROL_AS_FIRM',
                        'SIGINFLUENCECONTROL_AS_TRUST',
                    ],
                    'after' => 'right_to_appoint_and_remove_directors',
                    'null' => TRUE,
                ]
            )
            ->addColumn(
                'residential_premise',
                'string',
                [
                    'null' => TRUE,
                ]
            )
            ->addColumn(
                'residential_street',
                'string',
                [
                    'null' => TRUE,
                ]
            )
            ->addColumn(
                'residential_thoroughfare',
                'string',
                [
                    'null' => TRUE,
                ]
            )
            ->addColumn(
                'residential_post_town',
                'string',
                [
                    'null' => TRUE,
                ]
            )
            ->addColumn(
                'residential_county',
                'string',
                [
                    'null' => TRUE,
                ]
            )
            ->addColumn(
                'residential_country',
                'string',
                [
                    'null' => TRUE,
                ]
            )
            ->addColumn(
                'residential_postcode',
                'string',
                [
                    'null' => TRUE,
                ]
            )
            ->addColumn(
                'residential_secure_address_ind',
                'boolean',
                [
                    'default' => FALSE,
                ]
            )
            ->addColumn(
                'consentToAct',
                'boolean',
                [
                    'default' => TRUE,
                ]
            )
            ->addColumn(
                'existing_officer',
                'boolean',
                [
                    'default' => FALSE,
                ]
            )
            ->addColumn(
                'cessation_date',
                'date',
                [
                    'null' => TRUE,
                ]
            )
            ->addColumn(
                'before_changes',
                'text',
                [
                    'null' => TRUE,
                ]
            )
            ->update();

        $this
            ->table('ch_annual_return')
            ->addColumn(
                'no_psc_reason',
                'enum',
                [
                    'values' => [
                        'NO_INDIVIDUAL_OR_ENTITY_WITH_SIGNFICANT_CONTROL'
                    ],
                    'null' => TRUE,
                ]
            )
            ->addColumn(
                'pscs_pre_added',
                'boolean',
                [
                    'null' => TRUE,
                ]
            )
            ->update();
    }

    public function down()
    {
        $this
            ->table('ch_annual_return_officer')
            ->changeColumn(
                'type',
                'enum',
                [
                    'values' => ['DIR', 'SEC', 'MEM'],
                    'after' => 'form_submission_id',
                ]
            )
            ->changeColumn(
                'identification_type',
                'enum',
                [
                    'values' => ['EEA', 'NonEEA'],
                    'after' => 'country_of_residence',
                    'null' => TRUE,
                ]
            )
            ->removeColumn('country_or_state')
            ->removeColumn('ownership_of_shares')
            ->removeColumn('ownership_of_voting_rights')
            ->removeColumn('right_to_appoint_and_remove_directors')
            ->removeColumn('significant_influence_or_control')
            ->removeColumn('residential_premise')
            ->removeColumn('residential_street')
            ->removeColumn('residential_thoroughfare')
            ->removeColumn('residential_post_town')
            ->removeColumn('residential_county')
            ->removeColumn('residential_country')
            ->removeColumn('residential_postcode')
            ->removeColumn('residential_secure_address_ind')
            ->removeColumn('consentToAct')
            ->removeColumn('existing_officer')
            ->removeColumn('cessation_date')
            ->removeColumn('before_changes')
            ->update();

        $this
            ->table('ch_annual_return')
            ->removeColumn('no_psc_reason')
            ->removeColumn('pscs_pre_added')
            ->update();
    }
}
