<?php

use Phinx\Migration\AbstractMigration;

class AddServiceSettingsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $sql = <<<EOD
CREATE TABLE IF NOT EXISTS `cms2_service_settings` (
  `serviceSettingId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `serviceTypeId` enum('PACKAGE_PRIVACY','PACKAGE_COMPREHENSIVE_ULTIMATE','PACKAGE_BY_GUARANTEE','PACKAGE_SOLE_TRADER_PLUS','RESERVE_COMPANY_NAME','DORMANT_COMPANY_ACCOUNTS','APOSTILLED_DOCUMENTS','CERTIFICATE_OF_GOOD_STANDING','REGISTERED_OFFICE','SERVICE_ADDRESS','NOMINEE','ANNUAL_RETURN') NOT NULL DEFAULT 'RESERVE_COMPANY_NAME',
  `companyId` int(10) unsigned NOT NULL,
  `isAutoRenewalEnabled` tinyint(1) NOT NULL,
  `renewalTokenId` int(11) DEFAULT NULL,
  `dtc` datetime NOT NULL,
  `dtm` datetime NOT NULL,
  PRIMARY KEY (`serviceSettingId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
EOD;
        $this->query($sql);
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->table('cms2_service_settings')->drop();
    }
}
