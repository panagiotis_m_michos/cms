<?php

use Phinx\Migration\AbstractMigration;

class AddPscOnlineRegisterServiceType extends AbstractMigration
{
    const BUNDLE_PSC_ONLINE_REGISTER_CONFIRMATION_STATEMENT_DIY = 1706;
    const BUNDLE_PSC_ONLINE_REGISTER_CONFIRMATION_STATEMENT_SERVICE = 1717;
    const BUNDLE_PSC_ASSISTANCE = 1707;

    /**
     * @var array
     */
    private $currentServiceTypeIds = [
        'PACKAGE_PRIVACY',
        'PACKAGE_COMPREHENSIVE_ULTIMATE',
        'PACKAGE_BY_GUARANTEE',
        'PACKAGE_SOLE_TRADER_PLUS',
        'RESERVE_COMPANY_NAME',
        'DORMANT_COMPANY_ACCOUNTS',
        'APOSTILLED_DOCUMENTS',
        'CERTIFICATE_OF_GOOD_STANDING',
        'REGISTERED_OFFICE',
        'SERVICE_ADDRESS',
        'NOMINEE',
        'ANNUAL_RETURN',
    ];

    /**
     * @var array
     */
    private $newServiceTypeIds = [
        'PSC_ONLINE_REGISTER',
        'BUNDLE_PSC_ONLINE_REGISTER_CONFIRMATION_STATEMENT',
    ];

    public function up()
    {
        $newServiceTypeIds = array_merge($this->currentServiceTypeIds, $this->newServiceTypeIds);

        $this->table('cms2_services')
            ->changeColumn(
                'serviceTypeId',
                'enum',
                [
                    'values' => $newServiceTypeIds,
                    'after' => 'parentId',
                ]
            )
            ->update();

        $this->table('cms2_service_settings')
            ->changeColumn(
                'serviceTypeId',
                'enum',
                [
                    'values' => $newServiceTypeIds,
                    'after' => 'serviceSettingId',
                ]
            )
            ->update();

        $this->execute(
            sprintf(
                'UPDATE `cms2_nodes` SET `admin_controler` = "BundlePackageAdminControler" WHERE `node_id` IN (%d, %d, %d);',
                self::BUNDLE_PSC_ONLINE_REGISTER_CONFIRMATION_STATEMENT_DIY,
                self::BUNDLE_PSC_ONLINE_REGISTER_CONFIRMATION_STATEMENT_SERVICE,
                self::BUNDLE_PSC_ASSISTANCE
            )
        );
    }

    public function down()
    {
        $this->table('cms2_services')
            ->changeColumn(
                'serviceTypeId',
                'enum',
                [
                    'values' => $this->currentServiceTypeIds,
                    'after' => 'parentId',
                ]
            )
            ->update();

        $this->table('cms2_service_settings')
            ->changeColumn(
                'serviceTypeId',
                'enum',
                [
                    'values' => $this->currentServiceTypeIds,
                    'after' => 'serviceSettingId',
                ]
            )
            ->update();

        $this->execute(
            sprintf(
                'UPDATE `cms2_nodes` SET `admin_controler` = "PackageAdminControler" WHERE `node_id` IN (%d, %d, %d);',
                self::BUNDLE_PSC_ONLINE_REGISTER_CONFIRMATION_STATEMENT_DIY,
                self::BUNDLE_PSC_ONLINE_REGISTER_CONFIRMATION_STATEMENT_SERVICE,
                self::BUNDLE_PSC_ASSISTANCE
            )
        );
    }
}
