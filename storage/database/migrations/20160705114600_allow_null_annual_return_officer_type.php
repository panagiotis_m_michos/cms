<?php

use Phinx\Migration\AbstractMigration;

class AllowNullAnnualReturnOfficerType extends AbstractMigration
{
    public function up()
    {
        $this
            ->table('ch_annual_return_officer')
            ->changeColumn(
                'type',
                'enum',
                [
                    'values' => ['DIR', 'SEC', 'MEM', 'PSC'],
                    'after' => 'form_submission_id',
                    'null' => TRUE,
                ]
            )
            ->update();
    }

    public function down()
    {
        $this
            ->table('ch_annual_return_officer')
            ->changeColumn(
                'type',
                'enum',
                [
                    'values' => ['DIR', 'SEC', 'MEM', 'PSC'],
                    'after' => 'form_submission_id',
                ]
            )
            ->update();
    }
}
