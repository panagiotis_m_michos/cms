<?php

use Phinx\Migration\AbstractMigration;

class SimplyBusinessInsurance extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $table = $this->table('cms2_offer_customer_details');
        $table->addColumn('insuranceReturnMonth', 'string', ['length' => 20, 'null' => TRUE, 'after' => 'insuranceReturnDate']);
        $table->update();
        $this->query('update cms2_offer_customer_details set insuranceReturnMonth=MONTHNAME(insuranceReturnDate) where insuranceReturnDate is not null');
        $table->removeColumn('insuranceReturnDate');
        $table->update();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $table = $this->table('cms2_offer_customer_details');
        $table->addColumn('insuranceReturnDate', 'datetime', ['null' => TRUE, 'after' => 'insuranceReturnMonth']);
        $table->update();
        $table->removeColumn('insuranceReturnMonth');
        $table->update();
    }
}