<?php

use Phinx\Migration\AbstractMigration;

class AdditionalConfirmationStatementChanges extends AbstractMigration
{
    public function change()
    {
        $this->table('ch_annual_return_officer')
            ->addColumn('date_of_change', 'date', ['null' => TRUE])
            ->addColumn('notification_date', 'date', ['null' => TRUE])
            ->addColumn(
                'psc_statement_notification',
                'enum',
                [
                    'values' => [
                        'PSC_EXISTS_BUT_NOT_IDENTIFIED',
                        'PSC_DETAILS_NOT_CONFIRMED',
                        'PSC_CONTACTED_BUT_NO_RESPONSE',
                        'RESTRICTIONS_NOTICE_ISSUED_TO_PSC',
                    ],
                    'null' => TRUE,
                ]
            )
            ->update();
    }
}
