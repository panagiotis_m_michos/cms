<?php

use Phinx\Migration\AbstractMigration;

class UpdateOrderConfirmationEmail extends AbstractMigration
{
    public function up()
    {
        $sql = <<<SQL
UPDATE cms2_pages SET text = '<p>Hello [FIRSTNAME]&#160;[LASTNAME],</p>
<p>[PRODUCTS_EMAIL_TEXT]</p>
<p>Please find your invoice attached.</p>
<p>Any items that need to be dispatched will go to the contact address you have given.</p>
<p>Please get in touch if you have any questions.</p>
<p>Best wishes,</p>
<p><b>The Team @ Company Formation MadeSimple</b><br />
<a href="http://www.companiesmadesimple.com/">www.companiesmadesimple.com</a></p>
<p>Telephone: 020 7608 5500<br />
Email: <a href="mailto: theteam@madesimplegroup.com">theteam@madesimplegroup.com</a></p>
<p>Your business essentials MadeSimple</p>
<p><img border="0" src="https://ci4.googleusercontent.com/proxy/U1DLbETo-qwhctdNkw-yELjNnT-ZTRAMD3zhirvy_nddf6fiaGY2Tvd5-p-6tFJDFjiffixN2SgfTrNHeSK1ZLHD-A4mTicSvrWgAlEwQ4t1f3j_Gx6x2Nc=s0-d-e1-ft#https://www.companiesmadesimple.com/project/upload/imgs/img1223.png" style="color: rgb(102, 102, 102); font-family: arial, sans-serif; font-size: 12.7272720336914px;" alt="" /></p>'
WHERE node_id = 138;
SQL;

        $this->execute($sql);
    }

    public function down()
    {
        $sql = <<<SQL
UPDATE cms2_pages SET text = '<p>Hello [FIRSTNAME]&#160;[LASTNAME],</p>
<p>[PRODUCTS_EMAIL_TEXT]</p>
<p>Your invoice:</p>
<p>[SUMMARY]</p>
<p>Any items that need to be dispatched will go to the contact address you have given.</p>
<p>Please get in touch if you have any questions.</p>
<p>Best wishes,</p>
<p><b>The Team @ Company Formation MadeSimple</b><br />
<a href="http://www.companiesmadesimple.com/">www.companiesmadesimple.com</a></p>
<p>Telephone: 020 7608 5500<br />
Email: <a href="mailto: theteam@madesimplegroup.com">theteam@madesimplegroup.com</a></p>
<p>Your business essentials MadeSimple</p>
<p><img border="0" src="https://ci4.googleusercontent.com/proxy/U1DLbETo-qwhctdNkw-yELjNnT-ZTRAMD3zhirvy_nddf6fiaGY2Tvd5-p-6tFJDFjiffixN2SgfTrNHeSK1ZLHD-A4mTicSvrWgAlEwQ4t1f3j_Gx6x2Nc=s0-d-e1-ft#https://www.companiesmadesimple.com/project/upload/imgs/img1223.png" style="color: rgb(102, 102, 102); font-family: arial, sans-serif; font-size: 12.7272720336914px;" alt="" /></p>'
WHERE node_id = 138;
SQL;

        $this->execute($sql);
    }
}
