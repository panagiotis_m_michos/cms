<?php

use Phinx\Migration\AbstractMigration;

class AddInitialDurationAndStartDateToServices extends AbstractMigration
{
    public function up()
    {
        $this->table(TBL_SERVICES)
            ->addColumn('initialDuration', 'integer', ['after' => 'stateId', 'null' => TRUE])
            ->addColumn('initialDtStart', 'date', ['after' => 'initialDuration', 'null' => TRUE])
            ->update();

        $this->execute('UPDATE cms2_services SET initialDuration = 12 WHERE parentId IS NULL');

        $this
            ->table(TBL_ORDERS)
            ->changeColumn(
                'paymentMediumId',
                'enum',
                [
                    'values' => ['ONSITE', 'PHONE', 'AUTO_RENEWAL', 'COMPLIMENTARY', 'NON_STANDARD'],
                    'default' => 'ONSITE',
                    'after' => 'refundCustomerSupportLogin',
                ]
            )
            ->update();
    }

    public function down()
    {
        $this->table(TBL_SERVICES)
            ->removeColumn('initialDuration')
            ->removeColumn('initialDtStart')
            ->update();

        $this
            ->table(TBL_ORDERS)
            ->changeColumn(
                'paymentMediumId',
                'enum',
                [
                    'values' => ['ONSITE', 'PHONE', 'AUTO_RENEWAL'],
                    'default' => 'ONSITE',
                    'after' => 'refundCustomerSupportLogin',
                ]
            )
            ->update();
    }
}
