<?php

use Phinx\Migration\AbstractMigration;

class FixDatabaseEncoding extends AbstractMigration
{
    public function up()
    {
        $this->execute("SET NAMES 'utf8'");

        $this->execute("UPDATE cms2_pages SET `text` = REPLACE(`text`, 'â€™', '’') WHERE node_id IN ( 111, 279, 711, 1034, 1112, 1141, 1199, 1313, 1314, 1315, 1316, 1317, 1494, 1495, 1497, 1498, 1499, 1666, 1684 )");
        $this->execute("UPDATE cms2_pages SET `text` = REPLACE(`text`, 'Â£', '£') WHERE node_id IN ( 111, 279, 711, 1034, 1112, 1141, 1199, 1313, 1314, 1315, 1316, 1317, 1494, 1495, 1497, 1498, 1499, 1666, 1684 )");
    }

    public function down()
    {
        $this->execute("SET NAMES 'utf8'");

        $this->execute("UPDATE cms2_pages SET `text` = REPLACE(`text`, '’', 'â€™') WHERE node_id IN ( 111, 279, 711, 1034, 1112, 1141, 1199, 1313, 1314, 1315, 1316, 1317, 1494, 1495, 1497, 1498, 1499, 1666, 1684 )");
        $this->execute("UPDATE cms2_pages SET `text` = REPLACE(`text`, '£', 'Â£') WHERE node_id IN ( 111, 279, 711, 1034, 1112, 1141, 1199, 1313, 1314, 1315, 1316, 1317, 1494, 1495, 1497, 1498, 1499, 1666, 1684 )");
    }
}
