<?php

use Phinx\Migration\AbstractMigration;

class EntityLog extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $sql = <<<EOD
CREATE TABLE IF NOT EXISTS `entities_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(8) NOT NULL,
  `logged_at` datetime NOT NULL,
  `version` int(11) NOT NULL,
  `object_id` varchar(32) DEFAULT NULL,
  `object_class` varchar(255) NOT NULL,
  `data` longtext,
  `username` varchar(255) DEFAULT NULL,
  `userId` int(10) unsigned DEFAULT NULL,
  `customerId` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `log_class_lookup_idx` (`object_class`),
  KEY `log_date_lookup_idx` (`logged_at`),
  KEY `log_user_lookup_idx` (`username`),
  KEY `log_object_id` (`object_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
EOD;
        $this->execute($sql);
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $queue = $this->table('entities_log');
        $queue->drop();
    }
}