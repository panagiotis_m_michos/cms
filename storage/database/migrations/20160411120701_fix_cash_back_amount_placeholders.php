<?php

use Phinx\Migration\AbstractMigration;

class FixCashBackAmountPlaceholders extends AbstractMigration
{
    public function up()
    {
        $this->execute("SET NAMES 'utf8'");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, 'process with up to £75 cash back', 'process with up to £[cashBackAmount 1317] cash back') WHERE node_id = 447");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, 'company formation with up to £75 cash back', 'company formation with up to £[cashBackAmount 1317] cash back') WHERE node_id = 447");

        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, 'company formation with up to £75 cash back', 'company formation with up to £[cashBackAmount 1317] cash back') WHERE node_id = 1633");
    }

    public function down()
    {
        $this->execute("SET NAMES 'utf8'");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, 'process with up to £[cashBackAmount 1317] cash back', 'process with up to £75 cash back') WHERE node_id = 447");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, 'company formation with up to £[cashBackAmount 1317] cash back', 'company formation with up to £75 cash back') WHERE node_id = 447");

        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, 'company formation with up to £[cashBackAmount 1317] cash back', 'company formation with up to £75 cash back') WHERE node_id = 1633");
    }
}
