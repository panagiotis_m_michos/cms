<?php

use Phinx\Migration\AbstractMigration;

class AddServiceIsAutoRenewalEnabledAndTokenColumn extends AbstractMigration
{
    public function change()
    {
        $this->table('cms2_services')
            ->addColumn(
                'isAutoRenewalEnabled',
                'boolean',
                array(
                    'after' => 'stateId',
                    'default' => FALSE,
                )
            )
            ->addColumn(
                'renewalTokenId',
                'integer',
                array(
                    'null' => TRUE,
                    'after' => 'isAutoRenewalEnabled',
                    'default' => NULL,
                )
            )
            ->update();
    }
}
