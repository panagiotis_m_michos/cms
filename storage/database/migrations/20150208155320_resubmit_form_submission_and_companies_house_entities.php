<?php

use Phinx\Migration\AbstractMigration;

class ResubmitFormSubmissionAndCompaniesHouseEntities extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->table(TBL_ADDRESS_CHANGES)
            ->removeColumn('dtc')
            ->removeColumn('dtm')
            ->update();

        $this->table(TBL_FORM_SUBMISSIONS)
            ->addColumn(
                'date_cancelled',
                'datetime',
                array(
                    'after' => 'examiner_comment',
                    'default' => NULL,
                    'null' => TRUE
                )
            )
            ->update();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->table(TBL_ADDRESS_CHANGES)
            ->addColumn('dtc', 'datetime')
            ->addColumn('dtm', 'datetime')
            ->update();

        $this->table(TBL_FORM_SUBMISSIONS)
            ->removeColumn('date_cancelled')
            ->update();
    }
}
