<?php

use Phinx\Migration\AbstractMigration;

class AddUpgradedDowngradedStatusForService extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute(
            "ALTER TABLE cms2_services CHANGE stateId stateId ENUM('ENABLED','DISABLED','UPGRADED','DOWNGRADED') NOT NULL"
        );
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->execute("ALTER TABLE cms2_services CHANGE stateId stateId ENUM('ENABLED','DISABLED') NOT NULL");
    }
}
