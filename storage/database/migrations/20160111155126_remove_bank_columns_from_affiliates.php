<?php

use Phinx\Migration\AbstractMigration;

class RemoveBankColumnsFromAffiliates extends AbstractMigration
{
    public function up()
    {
        $this
            ->table(TBL_AFFILIATES)
            ->removeColumn('barclaysAllowed')
            ->removeColumn('HSBCAllowed')
            ->update();
    }

    public function down()
    {
        $this->table(TBL_AFFILIATES)
            ->addColumn('barclaysAllowed', 'boolean', ['after' => 'packagesIds', 'default' => 0])
            ->addColumn('HSBCAllowed', 'boolean', ['after' => 'barclaysAllowed', 'default' => 0])
            ->update();
    }
}
