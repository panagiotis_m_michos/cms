<?php

use Phinx\Migration\AbstractMigration;

class CompanyIncorporationPscChanges extends AbstractMigration
{
    public function up()
    {
        $this
            ->table('ch_incorporation_member')
            ->changeColumn(
                'type',
                'enum',
                [
                    'values' => ['DIR', 'SEC', 'SUB', 'PSC'],
                    'after' => 'nominee',
                ]
            )
            ->changeColumn(
                'identification_type',
                'enum',
                [
                    'values' => ['EEA', 'NonEEA', 'PSC'],
                    'after' => 'residential_secure_address_ind',
                    'null' => TRUE,
                ]
            )
            ->changeColumn(
                'surname',
                'string',
                [
                    'length' => 160,
                    'null' => TRUE,
                ]
            )
            ->addColumn(
                'country_or_state',
                'string',
                [
                    'after' => 'legal_form',
                    'length' => 50,
                    'null' => TRUE,
                ]
            )
            ->addColumn(
                'ownership_of_shares',
                'enum',
                [
                    'values' => [
                        'OWNERSHIPOFSHARES_25TO50PERCENT',
                        'OWNERSHIPOFSHARES_50TO75PERCENT',
                        'OWNERSHIPOFSHARES_75TO100PERCENT',
                        'OWNERSHIPOFSHARES_25TO50PERCENT_AS_FIRM',
                        'OWNERSHIPOFSHARES_50TO75PERCENT_AS_FIRM',
                        'OWNERSHIPOFSHARES_75TO100PERCENT_AS_FIRM',
                        'OWNERSHIPOFSHARES_25TO50PERCENT_AS_TRUST',
                        'OWNERSHIPOFSHARES_50TO75PERCENT_AS_TRUST',
                        'OWNERSHIPOFSHARES_75TO100PERCENT_AS_TRUST',
                        'RIGHTTOSHARESURPLUSASSETS_25TO50PERCENT',
                        'RIGHTTOSHARESURPLUSASSETS_50TO75PERCENT',
                        'RIGHTTOSHARESURPLUSASSETS_75TO100PERCENT',
                        'RIGHTTOSHARESURPLUSASSETS_25TO50PERCENT_AS_FIRM',
                        'RIGHTTOSHARESURPLUSASSETS_50TO75PERCENT_AS_FIRM',
                        'RIGHTTOSHARESURPLUSASSETS_75TO100PERCENT_AS_FIRM',
                        'RIGHTTOSHARESURPLUSASSETS_25TO50PERCENT_AS_TRUST',
                        'RIGHTTOSHARESURPLUSASSETS_50TO75PERCENT_AS_TRUST',
                        'RIGHTTOSHARESURPLUSASSETS_75TO100PERCENT_AS_TRUST',
                    ],
                    'after' => 'nomineeType',
                    'null' => TRUE,
                ]
            )
            ->addColumn(
                'ownership_of_voting_rights',
                'enum',
                [
                    'values' => [
                        'VOTINGRIGHTS_25TO50PERCENT',
                        'VOTINGRIGHTS_50TO75PERCENT',
                        'VOTINGRIGHTS_75TO100PERCENT',
                        'VOTINGRIGHTS_25TO50PERCENT_AS_FIRM',
                        'VOTINGRIGHTS_50TO75PERCENT_AS_FIRM',
                        'VOTINGRIGHTS_75TO100PERCENT_AS_FIRM',
                        'VOTINGRIGHTS_25TO50PERCENT_AS_TRUST',
                        'VOTINGRIGHTS_50TO75PERCENT_AS_TRUST',
                        'VOTINGRIGHTS_75TO100PERCENT_AS_TRUST'
                    ],
                    'after' => 'ownership_of_shares',
                    'null' => TRUE,
                ]
            )
            ->addColumn(
                'right_to_appoint_and_remove_directors',
                'enum',
                [
                    'values' => [
                        'RIGHTTOAPPOINTANDREMOVEDIRECTORS',
                        'RIGHTTOAPPOINTANDREMOVEDIRECTORS_AS_FIRM',
                        'RIGHTTOAPPOINTANDREMOVEDIRECTORS_AS_TRUST',
                        'RIGHTTOAPPOINTANDREMOVEMEMBERS',
                        'RIGHTTOAPPOINTANDREMOVEMEMBERS_AS_FIRM',
                        'RIGHTTOAPPOINTANDREMOVEMEMBERS_AS_TRUST',
                    ],
                    'after' => 'ownership_of_voting_rights',
                    'null' => TRUE,
                ]
            )
            ->addColumn(
                'significant_influence_or_control',
                'enum',
                [
                    'values' => [
                        'SIGINFLUENCECONTROL',
                        'SIGINFLUENCECONTROL_AS_FIRM',
                        'SIGINFLUENCECONTROL_AS_TRUST',
                    ],
                    'after' => 'right_to_appoint_and_remove_directors',
                    'null' => TRUE,
                ]
            )
            ->update();

        $this
            ->table('ch_company_incorporation')
            ->addColumn(
                'no_psc_reason',
                'enum',
                [
                    'values' => [
                        'NO_INDIVIDUAL_OR_ENTITY_WITH_SIGNFICANT_CONTROL'
                    ],
                    'null' => TRUE,
                ]
            )
            ->update();

        $this->execute(
            'UPDATE cms2_nodes SET front_controler = "CFPscsControler" WHERE node_id IN (1690, 1686, 1687, 1688, 1689)'
        );
    }

    public function down()
    {
        $this
            ->table('ch_incorporation_member')
            ->changeColumn(
                'type',
                'enum',
                [
                    'values' => ['DIR', 'SEC', 'SUB'],
                    'after' => 'nominee',
                ]
            )
            ->changeColumn(
                'identification_type',
                'enum',
                [
                    'values' => ['EEA', 'NonEEA'],
                    'after' => 'residential_secure_address_ind',
                    'null' => TRUE,
                ]
            )
            ->changeColumn(
                'surname',
                'string',
                [
                    'length' => 160,
                ]
            )
            ->removeColumn('country_or_state')
            ->removeColumn('ownership_of_shares')
            ->removeColumn('ownership_of_voting_rights')
            ->removeColumn('right_to_appoint_and_remove_directors')
            ->removeColumn('significant_influence_or_control')
            ->update();

        $this
            ->table('ch_company_incorporation')
            ->removeColumn('no_psc_reason')
            ->update();

        $this->execute(
            'UPDATE cms2_nodes SET front_controler = "DefaultControler" WHERE node_id IN (1690, 1686, 1687, 1688, 1689)'
        );
    }
}
