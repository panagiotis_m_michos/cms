<?php

use Phinx\Migration\AbstractMigration;

class UpdateFeefoLinksToHttps extends AbstractMigration
{
    public function up()
    {
        $from = 'http://www.feefo.com/feefo/viewvendor.jsp';
        $to = 'https://www.feefo.com/feefo/viewvendor.jsp';
        $this->query(sprintf('UPDATE %s SET text = REPLACE(text, "%s", "%s") WHERE text LIKE "%%%s%%"', TBL_PAGES, $from, $to, $from));

        $from = 'http://www.feefo.com/feefo/feefologo.jsp';
        $to = 'https://www.feefo.com/feefo/feefologo.jsp';
        $this->query(sprintf('UPDATE %s SET text = REPLACE(text, "%s", "%s") WHERE text LIKE "%%%s%%"', TBL_PAGES, $from, $to, $from));
    }

    public function down()
    {
        $from = 'https://www.feefo.com/feefo/viewvendor.jsp';
        $to = 'http://www.feefo.com/feefo/viewvendor.jsp';
        $this->query(sprintf('UPDATE %s SET text = REPLACE(text, "%s", "%s") WHERE text LIKE "%%%s%%"', TBL_PAGES, $from, $to, $from));

        $from = 'https://www.feefo.com/feefo/feefologo.jsp';
        $to = 'http://www.feefo.com/feefo/feefologo.jsp';
        $this->query(sprintf('UPDATE %s SET text = REPLACE(text, "%s", "%s") WHERE text LIKE "%%%s%%"', TBL_PAGES, $from, $to, $from));
    }
}
