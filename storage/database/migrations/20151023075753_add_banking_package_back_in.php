<?php

use Phinx\Migration\AbstractMigration;

class AddBankingPackageBackIn extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("UPDATE cms2_pages SET title = 'International Packages' WHERE page_id = 1466");
        $this->execute("UPDATE cms2_properties SET value = 1279 WHERE nodeId = 1598 AND name IN ('price', 'associatedPrice', 'wholesalePrice', 'productValue')");
        $this->execute("UPDATE cms2_pages SET text = '<p>CMS Order ID: [ORDER_ID]<br />\r\nProduct: [PRODUCT_TITLE]</p>\r\n<p>## Contact details</p>\r\n<p>Name: [FIRST_NAME] [LAST_NAME]<br />\r\nPhone: [PHONE]<br />\r\nEmail: [EMAIL]</p>\r\n<p>## Company details</p>\r\n<p>Company name: [COMPANY_NAME]<br />\r\nCompany number: [COMPANY_NUMBER]</p>\r\n<p>[BANKING_NOTICE]</p>\r\n<p>Please see attached company summary for full details.</p>' WHERE node_id = 1600 AND page_id = 1631");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->execute("UPDATE cms2_pages SET title = 'International Package' WHERE page_id = '1466'");
        $this->execute("UPDATE cms2_properties SET value = 1999 WHERE nodeId = 1598 AND name IN ('price', 'associatedPrice', 'wholesalePrice', 'productValue')");
        $this->execute("UPDATE cms2_pages SET text = '<p>CMS Order ID: [ORDER_ID]<br />\r\nProduct: [PRODUCT_TITLE]</p>\r\n<p>## Contact details</p>\r\n<p>Name: [FIRST_NAME] [LAST_NAME]<br />\r\nPhone: [PHONE]<br />\r\nEmail: [EMAIL]</p>\r\n<p>## Company details</p>\r\n<p>Company name: [COMPANY_NAME]<br />\r\nCompany number: [COMPANY_NUMBER]</p>\r\n<p>Please see attached company summary for full details.</p>' WHERE node_id = 1600 AND page_id = 1631");
    }
}
