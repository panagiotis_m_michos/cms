<?php

use Phinx\Migration\AbstractMigration;

class AddInternalFailureResponseToFormSubmission extends AbstractMigration
{
    public function up()
    {
        $this->execute("ALTER TABLE ch_form_submission CHANGE response response ENUM('ERROR','ACCEPT','REJECT','PENDING','PARKED','INTERNAL_FAILURE')");
    }

    public function down()
    {
        $this->execute("ALTER TABLE ch_form_submission CHANGE response response ENUM('ERROR','ACCEPT','REJECT','PENDING','PARKED')");
    }
}
