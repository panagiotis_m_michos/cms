<?php

use Phinx\Migration\AbstractMigration;

class AddTsbBankTypeToCashBacks extends AbstractMigration
{
    public function up()
    {
        $this->execute(
            "ALTER TABLE `cms2_cashback`
            CHANGE `bankTypeId` `bankTypeId` ENUM('BARCLAYS','HSBC','TSB') COLLATE 'latin1_swedish_ci' NOT NULL AFTER `cashbackTypeId`;"
        );
    }

    public function down()
    {
        $this->execute(
            "ALTER TABLE `cms2_cashback`
            CHANGE `bankTypeId` `bankTypeId` ENUM('BARCLAYS','HSBC') COLLATE 'latin1_swedish_ci' NOT NULL AFTER `cashbackTypeId`;"
        );
    }
}
