<?php

use Phinx\Migration\AbstractMigration;

class HelpUpdate extends AbstractMigration
{
    private $smallBusinessAdvisorId = 88;
    private $soleTraderVsLimitedId = 726;
    private $glossaryId = 91;
    private $oldFaqsId = 92;
    private $helpAndAdviceId = 1476;

    /**
     * Migrate Up.
     */
    public function up()
    {
        $helpFolderId = $this->addFolder($this->helpAndAdviceId, 'Help');
        $this->addExternalLink($helpFolderId, 'Support Home', 'http://support.companiesmadesimple.com');
        $this->addExternalLink($helpFolderId, 'FAQs', 'http://support.companiesmadesimple.com/category/132-frequently-asked-questions');
        $this->addExternalLink($helpFolderId, 'Chat with us', '#open-zopim');
        $this->addExternalLink($helpFolderId, 'Email us', '#open-beacon');

        $adviceFolderId = $this->addFolder($this->helpAndAdviceId, 'Advice');
        $this->addLink($adviceFolderId, $this->smallBusinessAdvisorId);
        $this->addLink($adviceFolderId, $this->soleTraderVsLimitedId);

        $this->softDelete($this->glossaryId);
        $this->softDelete($this->oldFaqsId);

        //glossary
        $this->execute("
            UPDATE cms2_pages
            SET text = REPLACE(text, '[page 91]', 'http://support.companiesmadesimple.com/article/286-glossary')
            WHERE text LIKE '%[page 91]%'
        ");

        //faqs
        $this->execute("
            UPDATE cms2_pages
            SET text = REPLACE(text, 'http://www.companiesmadesimple.com/company-formation-faqs.html', 'http://support.companiesmadesimple.com/category/132-frequently-asked-questions')
            WHERE text LIKE '%http://www.companiesmadesimple.com/company-formation-faqs.html%'
        ");

        $this->execute("
            UPDATE cms2_pages
            SET text = REPLACE(text, '[page 92]', 'http://support.companiesmadesimple.com/category/132-frequently-asked-questions')
            WHERE text LIKE '%[page 92]%'
        ");

        //contact us
        $this->execute("
            UPDATE cms2_pages
            SET text = REPLACE(text, 'https://www.companiesmadesimple.com/support/?/Tickets/Submit/RenderForm/22', 'http://support.companiesmadesimple.com/article/288-contacting-us')
            WHERE text LIKE '%http://www.companiesmadesimple.com/support/?/Tickets/Submit/RenderForm/22%'
        ");

        $this->execute("
            UPDATE cms2_pages
            SET text = REPLACE(text, '[page 243]', 'http://support.companiesmadesimple.com/article/288-contacting-us')
            WHERE text LIKE '%[page 243]%'
        ");

        if ($this->table('wp_posts')->exists()) {
            $this->execute("
                UPDATE wp_posts
                SET post_content = REPLACE(post_content, 'https://www.companiesmadesimple.com/support/?/Tickets/Submit/RenderForm/22', 'http://support.companiesmadesimple.com/article/288-contacting-us')
                WHERE post_content LIKE '%http://www.companiesmadesimple.com/support/?/Tickets/Submit/RenderForm/22%'
            ");

            $this->execute("
                UPDATE wp_posts
                SET post_content = REPLACE(post_content, 'http://www.companiesmadesimple.com/company-formation-faqs.html', 'http://support.companiesmadesimple.com/category/132-frequently-asked-questions')
                WHERE post_content LIKE '%http://www.companiesmadesimple.com/company-formation-faqs.html%'
            ");
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->undelete($this->glossaryId);
        $this->undelete($this->oldFaqsId);
        $this->addLink($this->helpAndAdviceId, $this->smallBusinessAdvisorId);
        $this->addLink($this->helpAndAdviceId, $this->soleTraderVsLimitedId);

        $childrenIds = FNode::getChildsIds($this->helpAndAdviceId, TRUE);
        $allowedChildrenIds = [
            $this->glossaryId,
            $this->oldFaqsId,
            $this->smallBusinessAdvisorId,
            $this->soleTraderVsLimitedId,
        ];

        foreach ($childrenIds as $id) {
            if (!in_array($id, $allowedChildrenIds)) {
                $child = new FNode($id);
                $child->delete(TRUE);
            }
        }

        //glossary
        $this->execute("
            UPDATE cms2_pages
            SET text = REPLACE(text, 'http://support.companiesmadesimple.com/article/286-glossary', '[page 91]')
            WHERE text LIKE '%http://support.companiesmadesimple.com/article/286-glossary%'
        ");

        //faqs
        $this->execute("
            UPDATE cms2_pages
            SET text = REPLACE(text, 'http://support.companiesmadesimple.com/category/132-frequently-asked-questions', 'http://www.companiesmadesimple.com/company-formation-faqs.html')
            WHERE text LIKE '%http://support.companiesmadesimple.com/category/132-frequently-asked-questions%'
        ");

        $this->execute("
            UPDATE cms2_pages
            SET text = REPLACE(text, 'http://www.companiesmadesimple.com/company-formation-faqs.html', '[page 92]')
            WHERE text LIKE '%http://www.companiesmadesimple.com/company-formation-faqs.html%'
        ");

        //contact us
        $this->execute("
            UPDATE cms2_pages
            SET text = REPLACE(text, 'http://support.companiesmadesimple.com/article/288-contacting-us', '[page 243]')
            WHERE text LIKE '%http://support.companiesmadesimple.com/article/288-contacting-us%'
        ");

        if ($this->table('wp_posts')->exists()) {
            $this->execute("
                UPDATE wp_posts
                SET post_content = REPLACE(post_content, 'http://support.companiesmadesimple.com/article/288-contacting-us', 'https://www.companiesmadesimple.com/support/?/Tickets/Submit/RenderForm/22')
                WHERE post_content LIKE '%http://support.companiesmadesimple.com/article/288-contacting-us%'
            ");

            $this->execute("
                UPDATE wp_posts]
                SET post_content = REPLACE(post_content, 'http://support.companiesmadesimple.com/category/132-frequently-asked-questions', 'http://www.companiesmadesimple.com/company-formation-faqs.html')
                WHERE post_content LIKE '%http://support.companiesmadesimple.com/category/132-frequently-asked-questions%'
            ");
        }
    }

    /**
     * @param int $parentId
     * @param string $title
     * @return int
     */
    private function addFolder($parentId, $title)
    {
        $node = new FNode();
        $node->setParentId($parentId);
        $node->page->title = $title;
        $node->setStatusId(FNode::STATUS_PUBLISHED);
        $node->adminControler = 'FolderAdminControler';
        $node->frontControler = 'DefaultControler';
        $node->save();

        return $node->getId();
    }

    /**
     * @param string $parentId
     * @param $id
     * @return int
     */
    private function addLink($parentId, $id)
    {
        $node = new FNode($id);
        $node->setParentId($parentId);
        $node->save();

        return $node->getId();
    }

    /**
     * @param int $parentId
     * @param string $title
     * @param string $url
     * @return int
     */
    private function addExternalLink($parentId, $title, $url)
    {
        $node = new ExternalLinkNode();
        $node->setParentId($parentId);
        $node->page->title = $title;
        $node->setUrl($url);
        $node->setStatusId(FNode::STATUS_PUBLISHED);
        $node->adminControler = 'ExternalLinkAdminControler';
        $node->frontControler = 'DefaultControler';
        $node->save();

        return $node->getId();
    }

    /**
     * @param int $id
     */
    private function softDelete($id)
    {
        $node = new FNode($id);
        $node->delete();
    }

    /**
     * @param int $id
     */
    private function undelete($id)
    {
        $node = new FNode($id);
        $node->setStatusId(FNode::STATUS_PUBLISHED);
        $node->isDeleted = FALSE;
        $node->save();
    }
}
