<?php

use Phinx\Migration\AbstractMigration;

class NullableInitialDuration extends AbstractMigration
{
    public function up()
    {
        $this->table('cms2_services')->changeColumn('initialDuration', 'string', ['null' => TRUE]);
        $this->execute('UPDATE cms2_services SET initialDuration = NULL WHERE initialDuration = ""');
    }

    public function down()
    {
        $this->execute('UPDATE cms2_services SET initialDuration = "" WHERE initialDuration = NULL');
        $this->table('cms2_services')->changeColumn('initialDuration', 'string');
    }
}
