<?php

use Phinx\Migration\AbstractMigration;

class FacebookGroupOffer extends AbstractMigration
{
    public function up()
    {
        $this->query("INSERT INTO cms2_toolkit_offers (type, name, description, dateValidFrom, dateValidTo) VALUES ('facebookGroup', 'Join MadeSimple Startup Community - where startups support startups', 'This private ‘customers only’ Facebook Group is dedicated to helping our startup customers collaborate, get advice and share advice. Join the group to post questions and view the other group posts.', now(), NULL)");
    }

    public function down()
    {
        $this->query("DELETE FROM cms2_toolkit_offers WHERE type = 'facebookGroup'");
    }
}
