<?php

use Phinx\Migration\AbstractMigration;

class CustomerIdCheck extends AbstractMigration
{
    public function change()
    {
        $customersTable = $this->table(TBL_CUSTOMERS);
        $customersTable->addColumn('isIdCheckRequired', 'boolean', array('after' => 'sortCode', 'null' => FALSE, 'default' => 0))
            ->addColumn('dtIdValidated', 'datetime', array('after' => 'isIdCheckRequired', 'null' => TRUE))
            ->update();

        $ordersTable = $this->table(TBL_ORDERS_ITEMS);
        $ordersTable->addColumn('dtExported', 'datetime', array('after' => 'markup', 'null' => TRUE))
            ->update();
    }
}