<?php

use Phinx\Migration\AbstractMigration;

class AddPreAddedPscsFlashToIncorporation extends AbstractMigration
{
    public function change()
    {
        $this->table('ch_company_incorporation')
            ->addColumn('preAddedPscs', 'boolean', ['default' => FALSE])
            ->update();
    }
}
