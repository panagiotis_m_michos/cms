<?php

use Phinx\Migration\AbstractMigration;

class VoServicesQueue extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $queue = $this->table('cms2_vo_services_queue', array('id' => 'voServiceQueueId'));
        $queue->addColumn('customerEmail', 'string')
            ->addColumn('productId', 'integer')
            ->addColumn('orderId', 'integer')
            ->addColumn('durationInMonths', 'integer')
            ->addColumn('dateSent', 'datetime', array('default' => NULL, 'null' => TRUE))
            ->addColumn('dtc', 'datetime')
            ->addColumn('dtm', 'datetime')
            ->create();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $queue = $this->table('cms2_vo_services_queue');
        $queue->drop();
    }
}