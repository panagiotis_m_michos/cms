<?php

use Phinx\Migration\AbstractMigration;

class MailgunEventTemporaryId extends AbstractMigration
{
    public function change()
    {
        $this->table('cms2_mailgun_events')
            ->addColumn('temporaryId', 'string')
            ->update();
            
    }
}
