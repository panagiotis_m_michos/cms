<?php

use Phinx\Migration\AbstractMigration;

class EmailGuardLog extends AbstractMigration
{
    public function change()
    {
        $this->table('cms2_email_guard_log', ['id' => 'emailGuardLogId'])
            ->addColumn('emailHash', 'string')
            ->addColumn('createdAt', 'datetime')
            ->addColumn('emailFrom', 'string')
            ->addColumn('emailTo', 'string')
            ->addColumn('emailSubject', 'text')
            ->create();
    }
}
