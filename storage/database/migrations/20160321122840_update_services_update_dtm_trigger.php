<?php

use Phinx\Migration\AbstractMigration;

class UpdateServicesUpdateDtmTrigger extends AbstractMigration
{
    public function up()
    {
        $this->execute('DROP TRIGGER servicesUpdateDtm');

        $sql = <<<QUERY
    CREATE TRIGGER servicesUpdateDtm BEFORE UPDATE ON cms2_services
    FOR EACH ROW
    BEGIN
        IF NOT (NEW.dtStart <=> OLD.dtStart AND NEW.dtExpires <=> OLD.dtExpires) THEN
            SET NEW.dtm = NOW();
        END IF;
    END;
QUERY;

        $this->execute($sql);
    }

    public function down()
    {
        $this->execute('DROP TRIGGER servicesUpdateDtm');

        $sql = <<<QUERY
    CREATE TRIGGER servicesUpdateDtm BEFORE UPDATE ON cms2_services
    FOR EACH ROW
    BEGIN
        IF (NEW.dtStart <=> OLD.dtStart OR NEW.dtExpires <=> OLD.dtExpires) THEN
            SET NEW.dtm = NOW();
        END IF;
    END;
QUERY;

        $this->execute($sql);
    }
}
