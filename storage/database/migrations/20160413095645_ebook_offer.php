<?php

use Phinx\Migration\AbstractMigration;

class EbookOffer extends AbstractMigration
{
    public function up()
    {
        $this->query('
            UPDATE cms2_toolkit_offers
            SET
                type = "ebook",
                name="The Ultimate Small Business Startup Guide",
                description="<b>Everything you need to know to help your business make a profit</b> <br>
                      &bullet; Receive an expert’s guide to making profit<br>
                      &bullet; Easy to understand and jargon-free<br>
                      &bullet; Avoid the common mistakes many news businesses make<br>
                      &bullet; Perfect if you’ve never started a business before"
            WHERE type = "makeMoreProfitGuide"
        ');
    }

    public function down()
    {
        $this->query('
            UPDATE cms2_toolkit_offers
            SET
                type = "makeMoreProfitGuide",
                name="Free How To Make A Profit eBook",
                description="Everything you need to know to start, run and grow your business. Our guide covers; choosing the right business structure, creating a business plan, funding options, marketing, accounting and much more. An absolute must for all budding entrepreneurs."
            WHERE type = "ebook"
        ');
    }
}
