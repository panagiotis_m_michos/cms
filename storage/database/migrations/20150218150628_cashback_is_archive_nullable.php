<?php

use Phinx\Migration\AbstractMigration;

class CashbackIsArchiveNullable extends AbstractMigration
{
    public function up()
    {
        $this->table('cms2_cashback')->changeColumn('isArchived', 'boolean', array('null' => TRUE))->save();
    }

    public function down()
    {

    }
}