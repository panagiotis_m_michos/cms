<?php

use Phinx\Migration\AbstractMigration;

class AddCashBackAmountToCompany extends AbstractMigration
{
    public function up()
    {
        $this->execute("SET NAMES 'utf8'");

        $this
            ->table(TBL_COMPANY)
            ->addColumn('cash_back_amount', 'integer', ['after' => 'accepted_date', 'default' => 0])
            ->update();

        $this->query("UPDATE ch_company SET cash_back_amount = 50 WHERE product_id IN (379, 436, 898, 899, 1022, 1175, 1313, 1314, 1315, 1316, 1317, 596, 597, 598, 729, 837, 838, 840, 841, 911, 912, 913, 914)");

        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (379, 'cashBackAmount', 50, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (436, 'cashBackAmount', 50, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (898, 'cashBackAmount', 50, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (899, 'cashBackAmount', 50, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (1022, 'cashBackAmount', 50, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (1175, 'cashBackAmount', 50, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (1313, 'cashBackAmount', 50, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (1314, 'cashBackAmount', 50, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (1315, 'cashBackAmount', 50, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (1316, 'cashBackAmount', 50, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (1317, 'cashBackAmount', 50, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (596, 'cashBackAmount', 50, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (597, 'cashBackAmount', 50, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (598, 'cashBackAmount', 50, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (729, 'cashBackAmount', 50, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (837, 'cashBackAmount', 50, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (838, 'cashBackAmount', 50, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (840, 'cashBackAmount', 50, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (841, 'cashBackAmount', 50, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (911, 'cashBackAmount', 50, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (912, 'cashBackAmount', 50, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (913, 'cashBackAmount', 50, 1, 1, NOW(), NOW())");
        $this->query("INSERT INTO cms2_properties (nodeId, name, value, authorId, editorId, dtc, dtm) VALUES (914, 'cashBackAmount', 50, 1, 1, NOW(), NOW())");

        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, 'process with £50 cash back', 'process with up to £75 cash back') WHERE node_id = 447");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, 'company formation with £50 cash back', 'company formation with up to £75 cash back') WHERE node_id = 447");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, '£50 cash back', '£[cashBackAmount 1313]-£[cashBackAmount 1317] cash back') WHERE node_id = 447");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, '(NB. You must log in to access this page) to apply for your cash back.</p>', '(NB. You must log in to access this page) to apply for your cash back.</p><p style=\"padding-top: 14px;\">The amount payable is based on cash back amount offered on the formation package on the date of purchase.</p>') WHERE node_id = 447");

        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, 'company formation with £50 cash back', 'company formation with up to £75 cash back') WHERE node_id = 1633");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, '£50 cash back', '£[cashBackAmount 1313]-£[cashBackAmount 1317] cash back') WHERE node_id = 1633");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, '(NB. You must log in to access this page) to apply for your cash back.', '(NB. You must log in to access this page) to apply for your cash back. The amount payable is based on cash back amount offered on the formation package on the date of purchase.') WHERE node_id = 1633");

        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, '£50 Cash Back', '£[cashBackAmount 1313] Cash Back') WHERE node_id = 1313");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, '£50 cash back', '£[cashBackAmount 1313] cash back') WHERE node_id = 1313");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, '£50 Cash Back', '£[cashBackAmount 1314] Cash Back') WHERE node_id = 1314");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, '£50 cash back', '£[cashBackAmount 1314] cash back') WHERE node_id = 1314");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, '£50 Cash Back', '£[cashBackAmount 1315] Cash Back') WHERE node_id = 1315");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, '£50 cash back', '£[cashBackAmount 1315] cash back') WHERE node_id = 1315");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, '£50 Cash Back', '£[cashBackAmount 1316] Cash Back') WHERE node_id = 1316");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, '£50 cash back', '£[cashBackAmount 1316] cash back') WHERE node_id = 1316");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, '£50 Cash Back', '£[cashBackAmount 1317] Cash Back') WHERE node_id = 1317");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, '£50 cash back', '£[cashBackAmount 1317] cash back') WHERE node_id = 1317");
    }

    public function down()
    {
        $this->execute("SET NAMES 'utf8'");

        $this
            ->table(TBL_COMPANY)
            ->removeColumn('cash_back_amount')
            ->update();

        $this->query("DELETE FROM cms2_properties WHERE name = 'cashBackAmount'");
        
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, 'process with up to £75 cash back', 'process with £50 cash back') WHERE node_id = 447");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, 'company formation with up to £75 cash back', 'company formation with £50 cash back') WHERE node_id = 447");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, '£[cashBackAmount 1313]-£[cashBackAmount 1317] cash back', '£50 cash back') WHERE node_id = 447");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, '(NB. You must log in to access this page) to apply for your cash back.</p><p style=\"padding-top: 14px;\">The amount payable is based on cash back amount offered on the formation package on the date of purchase.</p>', '(NB. You must log in to access this page) to apply for your cash back.</p>') WHERE node_id = 447");

        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, 'company formation with up to £75 cash back', 'company formation with £50 cash back') WHERE node_id = 1633");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, '£[cashBackAmount 1313]-£[cashBackAmount 1317] cash back', '£50 cash back') WHERE node_id = 1633");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, '(NB. You must log in to access this page) to apply for your cash back. The amount payable is based on cash back amount offered on the formation package on the date of purchase.', '(NB. You must log in to access this page) to apply for your cash back.') WHERE node_id = 1633");

        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, '£[cashBackAmount 1313] Cash Back', '£50 Cash Back') WHERE node_id = 1313");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, '£[cashBackAmount 1313] cash back', '£50 cash back') WHERE node_id = 1313");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, '£[cashBackAmount 1314] Cash Back', '£50 Cash Back') WHERE node_id = 1314");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, '£[cashBackAmount 1314] cash back', '£50 cash back') WHERE node_id = 1314");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, '£[cashBackAmount 1315] Cash Back', '£50 Cash Back') WHERE node_id = 1315");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, '£[cashBackAmount 1315] cash back', '£50 cash back') WHERE node_id = 1315");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, '£[cashBackAmount 1316] Cash Back', '£50 Cash Back') WHERE node_id = 1316");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, '£[cashBackAmount 1316] cash back', '£50 cash back') WHERE node_id = 1316");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, '£[cashBackAmount 1317] Cash Back', '£50 Cash Back') WHERE node_id = 1317");
        $this->query("UPDATE cms2_pages SET `text` = REPLACE(`text`, '£[cashBackAmount 1317] cash back', '£50 cash back') WHERE node_id = 1317");
    }
}
