<?php

use Phinx\Migration\AbstractMigration;

class AddFormSubmissionNomineeType extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->table('ch_incorporation_member')->addColumn('nomineeType', 'string', array('null' => TRUE))->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->table('ch_incorporation_member')->removeColumn('nomineeType')->save();
    }
}