<?php

use Phinx\Migration\AbstractMigration;

class UpdateBusinessToolkitNodeController extends AbstractMigration
{
    public function up()
    {
        $this->query(
            "UPDATE cms2_nodes SET front_controler = 'CFToolkitControler' WHERE node_id IN (1630);"
        );
    }

    public function down()
    {
        $this->query(
            "UPDATE cms2_nodes SET front_controler = 'DefaultControler' WHERE node_id IN (1630);"
        );
    }
}
