<?php

use Phinx\Migration\AbstractMigration;

class UpdateCrePackagesNodesToNewDesign extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->query(
            "UPDATE cms2_nodes SET front_controler = 'CrePackageControler' WHERE node_id IN (1313, 1314, 1315, 1316, 1317);

            DELETE FROM cms2_properties
            WHERE `name` IN ('disabledLeftColumn', 'disabledRightColumn', 'disabledFeedback')
                AND nodeId IN (1313, 1314, 1315, 1316, 1317);

            INSERT INTO `cms2_properties` (`nodeId`, `NAME`, `VALUE`, `authorId`, `editorId`, `dtc`, `dtm`)
            VALUES
                (1313, 'disabledLeftColumn', '1', 1, 1, NOW(), NOW()),
                (1313, 'disabledRightColumn', '1', 1, 1, NOW(), NOW()),
                (1314, 'disabledLeftColumn', '1', 1, 1, NOW(), NOW()),
                (1314, 'disabledRightColumn', '1', 1, 1, NOW(), NOW()),
                (1315, 'disabledLeftColumn', '1', 1, 1, NOW(), NOW()),
                (1315, 'disabledRightColumn', '1', 1, 1, NOW(), NOW()),
                (1316, 'disabledLeftColumn', '1', 1, 1, NOW(), NOW()),
                (1316, 'disabledRightColumn', '1', 1, 1, NOW(), NOW()),
                (1317, 'disabledLeftColumn', '1', 1, 1, NOW(), NOW()),
                (1317, 'disabledRightColumn', '1', 1, 1, NOW(), NOW());"
        );
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->query(
            "UPDATE cms2_nodes SET front_controler = 'PackageControler' WHERE node_id IN (1313, 1314, 1315, 1316, 1317);

            DELETE FROM cms2_properties
            WHERE `name` IN ('disabledLeftColumn', 'disabledRightColumn', 'disabledFeedback')
                AND nodeId IN (1313, 1314, 1315, 1316, 1317);"
        );
    }
}
