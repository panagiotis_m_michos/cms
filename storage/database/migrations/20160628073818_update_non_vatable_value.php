<?php

use Phinx\Migration\AbstractMigration;

class UpdateNonVatableValue extends AbstractMigration
{
    private $nodes = [
        111,
        279,
        711,
        1034,
        1112,
        1141,
        1199,
        1313,
        1314,
        1315,
        1316,
        1317,
        1494,
        1495,
        1497,
        1498,
        1499,
        1666,
        1684
    ];

    const SEARCH_REGEXP = '/£13([^\d\.]|$)/';
    const SEARCH_REPLACE = '£10\1';

    public function up()
    {
        $adapter = $this->getAdapter()->getConnection();

        foreach ($this->nodes as $nodeId) {
            $node = new FNode($nodeId);
            $text = $node->getLngText();
            $abstract = $node->getLngAbstract();
            $description = $node->getLngDescription();

            $newText = preg_replace(self::SEARCH_REGEXP, self::SEARCH_REPLACE, $text);
            $newAbstract = preg_replace(self::SEARCH_REGEXP, self::SEARCH_REPLACE, $abstract);
            $newDescription = preg_replace(self::SEARCH_REGEXP, self::SEARCH_REPLACE, $description);

            $stm = $adapter->prepare("
                UPDATE cms2_pages
                SET abstract = ?,
                text = ?,
                description = ?
                WHERE node_id = ?;
            ");
            $stm->execute([
                $newAbstract,
                $newText,
                $newDescription,
                $nodeId
            ]);
        }

        $query = "
            UPDATE cms2_properties
            SET value = 10
            WHERE name='nonVatableValue'
            AND value = 13";

        $this->query($query);

    }

    public function down()
    {
        $adapter = $this->getAdapter()->getConnection();

        foreach ($this->nodes as $pageId) {
            $node = new FNode($pageId);
            $text = $node->getLngText();
            $abstract = $node->getLngAbstract();
            $description = $node->getLngDescription();

            $newText = preg_replace(self::SEARCH_REGEXP, self::SEARCH_REPLACE, $text);
            $newAbstract = preg_replace(self::SEARCH_REGEXP, self::SEARCH_REPLACE, $abstract);
            $newDescription = preg_replace(self::SEARCH_REGEXP, self::SEARCH_REPLACE, $description);

            $stm = $adapter->prepare("
                UPDATE cms2_pages
                SET abstract = ?,
                text = ?,
                description = ?
                WHERE node_id = ?;
            ");
            $stm->execute([
                $newAbstract,
                $newText,
                $newDescription,
                $pageId
            ]);
        }

        $query = "
            UPDATE cms2_properties
            SET value = 13
            WHERE name='nonVatableValue'
            AND value = 10";

        $this->query($query);
    }
}
