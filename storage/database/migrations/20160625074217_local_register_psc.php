<?php

use Phinx\Migration\AbstractMigration;

class LocalRegisterPsc extends AbstractMigration
{
    public function up()
    {
        $this->table('cms2_register_pscs', ['id' => 'entryId', 'primary_key' => 'entryId'])
            ->addColumn('companyId', 'integer')
            ->addColumn(
                'entryType',
                'enum',
                ['values' => ['PERSON_PSC', 'CORPORATE_PSC', 'COMPANY_STATEMENT']]
            )
            ->addColumn(
                'companyStatement',
                'enum',
                [
                    'values' => [
                        'NO_INDIVIDUAL_OR_ENTITY_WITH_SIGNFICANT_CONTROL',
                        'STEPS_TO_FIND_PSC_NOT_YET_COMPLETED',
                    ],
                    'null' => TRUE,
                ]
            )
            ->addColumn(
                'pscStatementNotification',
                'enum',
                [
                    'values' => [
                        'PSC_EXISTS_BUT_NOT_IDENTIFIED',
                        'PSC_DETAILS_NOT_CONFIRMED',
                        'PSC_CONTACTED_BUT_NO_RESPONSE',
                        'RESTRICTIONS_NOTICE_ISSUED_TO_PSC',
                    ],
                    'null' => TRUE,
                ]
            )
            ->addColumn('title', 'string', ['null' => TRUE])
            ->addColumn('forename', 'string', ['null' => TRUE])
            ->addColumn('middleName', 'string', ['null' => TRUE])
            ->addColumn('surname', 'string', ['null' => TRUE])
            ->addColumn('corporateName', 'string', ['null' => TRUE])
            ->addColumn('premise', 'string', ['null' => TRUE])
            ->addColumn('street', 'string', ['null' => TRUE])
            ->addColumn('thoroughfare', 'string', ['null' => TRUE])
            ->addColumn('postTown', 'string', ['null' => TRUE])
            ->addColumn('county', 'string', ['null' => TRUE])
            ->addColumn('country', 'string', ['null' => TRUE])
            ->addColumn('postcode', 'string', ['null' => TRUE])
            ->addColumn('dob', 'string', ['null' => TRUE])
            ->addColumn('nationality', 'string', ['null' => TRUE])
            ->addColumn('countryOfResidence', 'string', ['null' => TRUE])
            ->addColumn('residentialPremise', 'string', ['null' => TRUE])
            ->addColumn('residentialStreet', 'string', ['null' => TRUE])
            ->addColumn('residentialThoroughfare', 'string', ['null' => TRUE])
            ->addColumn('residentialPostTown', 'string', ['null' => TRUE])
            ->addColumn('residentialCounty', 'string', ['null' => TRUE])
            ->addColumn('residentialCountry', 'string', ['null' => TRUE])
            ->addColumn('residentialPostcode', 'string', ['null' => TRUE])
            ->addColumn('residentialSecureAddressInd', 'string', ['null' => TRUE])
            ->addColumn('placeRegistered', 'string', ['null' => TRUE])
            ->addColumn('registrationNumber', 'string', ['null' => TRUE])
            ->addColumn('lawGoverned', 'string', ['null' => TRUE])
            ->addColumn('legalForm', 'string', ['null' => TRUE])
            ->addColumn('countryOrState', 'string', ['null' => TRUE])
            ->addColumn(
                'ownershipOfShares',
                'enum',
                [
                    'values' => [
                        'OWNERSHIPOFSHARES_25TO50PERCENT',
                        'OWNERSHIPOFSHARES_50TO75PERCENT',
                        'OWNERSHIPOFSHARES_75TO100PERCENT',
                        'OWNERSHIPOFSHARES_25TO50PERCENT_AS_FIRM',
                        'OWNERSHIPOFSHARES_50TO75PERCENT_AS_FIRM',
                        'OWNERSHIPOFSHARES_75TO100PERCENT_AS_FIRM',
                        'OWNERSHIPOFSHARES_25TO50PERCENT_AS_TRUST',
                        'OWNERSHIPOFSHARES_50TO75PERCENT_AS_TRUST',
                        'OWNERSHIPOFSHARES_75TO100PERCENT_AS_TRUST',
                        'RIGHTTOSHARESURPLUSASSETS_25TO50PERCENT',
                        'RIGHTTOSHARESURPLUSASSETS_50TO75PERCENT',
                        'RIGHTTOSHARESURPLUSASSETS_75TO100PERCENT',
                        'RIGHTTOSHARESURPLUSASSETS_25TO50PERCENT_AS_FIRM',
                        'RIGHTTOSHARESURPLUSASSETS_50TO75PERCENT_AS_FIRM',
                        'RIGHTTOSHARESURPLUSASSETS_75TO100PERCENT_AS_FIRM',
                        'RIGHTTOSHARESURPLUSASSETS_25TO50PERCENT_AS_TRUST',
                        'RIGHTTOSHARESURPLUSASSETS_50TO75PERCENT_AS_TRUST',
                        'RIGHTTOSHARESURPLUSASSETS_75TO100PERCENT_AS_TRUST',
                    ],
                    'null' => TRUE,
                ]
            )
            ->addColumn(
                'ownershipOfVotingRights',
                'enum',
                [
                    'values' => [
                        'VOTINGRIGHTS_25TO50PERCENT',
                        'VOTINGRIGHTS_50TO75PERCENT',
                        'VOTINGRIGHTS_75TO100PERCENT',
                        'VOTINGRIGHTS_25TO50PERCENT_AS_FIRM',
                        'VOTINGRIGHTS_50TO75PERCENT_AS_FIRM',
                        'VOTINGRIGHTS_75TO100PERCENT_AS_FIRM',
                        'VOTINGRIGHTS_25TO50PERCENT_AS_TRUST',
                        'VOTINGRIGHTS_50TO75PERCENT_AS_TRUST',
                        'VOTINGRIGHTS_75TO100PERCENT_AS_TRUST',
                    ],
                    'null' => TRUE,
                ]
            )
            ->addColumn(
                'rightToAppointAndRemoveDirectors',
                'enum',
                [
                    'values' => [
                        'RIGHTTOAPPOINTANDREMOVEDIRECTORS',
                        'RIGHTTOAPPOINTANDREMOVEDIRECTORS_AS_FIRM',
                        'RIGHTTOAPPOINTANDREMOVEDIRECTORS_AS_TRUST',
                        'RIGHTTOAPPOINTANDREMOVEMEMBERS',
                        'RIGHTTOAPPOINTANDREMOVEMEMBERS_AS_FIRM',
                        'RIGHTTOAPPOINTANDREMOVEMEMBERS_AS_TRUST',
                    ],
                    'null' => TRUE,
                ]
            )
            ->addColumn(
                'significantInfluenceOrControl',
                'enum',
                [
                    'values' => [
                        'SIGINFLUENCECONTROL',
                        'SIGINFLUENCECONTROL_AS_FIRM',
                        'SIGINFLUENCECONTROL_AS_TRUST',
                    ],
                    'null' => TRUE,
                ]
            )
            ->addColumn('dateEntry', 'date', ['null' => TRUE])
            ->addColumn('dateCessated', 'date', ['null' => TRUE])
            ->addColumn('dtc', 'datetime')
            ->addColumn('dtm', 'datetime')
            ->create();

        $this->execute(
            'UPDATE cms2_nodes SET front_controler = "CURegisterPscControler" WHERE node_id IN (1710, 1711, 1712, 1713, 1714, 1715, 1716)'
        );
    }

    public function down()
    {
        $this->dropTable('cms2_register_pscs');

        $this->execute(
            'UPDATE cms2_nodes SET front_controler = "DefaultControler" WHERE node_id IN (1710, 1711, 1712, 1713, 1714, 1715, 1716)'
        );
    }
}
