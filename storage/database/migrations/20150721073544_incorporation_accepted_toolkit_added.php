<?php

use Phinx\Migration\AbstractMigration;

class IncorporationAcceptedToolkitAdded extends AbstractMigration
{
    private $newMail = <<<MAIL
<p>Hello [FIRSTNAME],</p>
<p>Congratulations!</p>
<p>We are pleased to advise you that your company, [COMPANY_NAME] has been accepted for incorporation by Companies House.</p>
<p>Your company details are shown below:</p>
<p>Company Name: [COMPANY_NAME]<br />
Company Number: [COMPANY_NUMBER]</p>
<p>You can view your full company details from <a href="[COMPANY_INFO_URL]">your online account</a>, including all your company documents. Simply log in, click 'My Companies' and then click on your company name. Scroll down to 'My Company Documents' to access:</p>
<ul>
    <li>Certificate of Incorporation</li>
    <li>Articles of Association</li>
    <li>Memorandum of Association</li>
</ul>
<p>If the above link does not work – please copy and paste the below link into your browser:</p>
<p><a href="[COMPANY_INFO_URL]">[COMPANY_INFO_URL]</a></p>
<p>Please note: Although your company is now formed and we have received a registration number, it can still take between 24 and 72 hours for Companies House to update their records online.</p>
<p><strong>Business Startup Toolkit </strong>- Your company formation package includes a selection of exclusive offers designed to help your business start, run and grow. More information about these will be emailed within 24 hours.</p>
<p>Please get in contact if you have any questions, a member of the team will be happy to help.</p>
<p>Best wishes,</p>
<p><b>The Team @ Company Formation MadeSimple</b><br />
<a href="http://www.companiesmadesimple.com/">www.companiesmadesimple.com</a></p>
<p>Telephone: 020 7608 5500<br />
Email: <a href="mailto: theteam@madesimplegroup.com">theteam@madesimplegroup.com</a></p>
<p>Your business essentials MadeSimple</p>
<p><img border="0" src="https://ci4.googleusercontent.com/proxy/U1DLbETo-qwhctdNkw-yELjNnT-ZTRAMD3zhirvy_nddf6fiaGY2Tvd5-p-6tFJDFjiffixN2SgfTrNHeSK1ZLHD-A4mTicSvrWgAlEwQ4t1f3j_Gx6x2Nc=s0-d-e1-ft#https://www.companiesmadesimple.com/project/upload/imgs/img1223.png" style="color: rgb(102, 102, 102); font-family: arial, sans-serif; font-size: 12.7272720336914px;" alt="" /></p>
MAIL;

    private $oldMail = <<<MAIL
<p>Hello [FIRSTNAME],</p>
<p>Congratulations!</p>
<p>We are pleased to advise you that your company, [COMPANY_NAME] has been accepted for incorporation by Companies House.</p>
<p>Your company details are shown below:</p>
<p>Company Name: [COMPANY_NAME]<br />
Company Number: [COMPANY_NUMBER]</p>
<p>You can view your full company details from <a href="[COMPANY_INFO_URL]">your online account</a>, including all your company documents. Simply log in, click 'My Companies' and then click on your company name. Scroll down to 'My Company Documents' to access:</p>
<ul>
    <li>Certificate of Incorporation</li>
    <li>Articles of Association</li>
    <li>Memorandum of Association</li>
</ul>
<p>If the above link does not work – please copy and paste the below link into your browser:</p>
<p><a href="[COMPANY_INFO_URL]">[COMPANY_INFO_URL]</a></p>
<p>Please note: Although your company is now formed and we have received a registration number, it can still take between 24 and 72 hours for Companies House to update their records online.</p>
<p>Please get in contact if you have any questions, a member of the team will be happy to help.</p>
<p>Best wishes,</p>
<p><b>The Team @ Company Formation MadeSimple</b><br />
<a href="http://www.companiesmadesimple.com/">www.companiesmadesimple.com</a></p>
<p>Telephone: 020 7608 5500<br />
Email: <a href="mailto: theteam@madesimplegroup.com">theteam@madesimplegroup.com</a></p>
<p>Your business essentials MadeSimple</p>
<p><img border="0" src="https://ci4.googleusercontent.com/proxy/U1DLbETo-qwhctdNkw-yELjNnT-ZTRAMD3zhirvy_nddf6fiaGY2Tvd5-p-6tFJDFjiffixN2SgfTrNHeSK1ZLHD-A4mTicSvrWgAlEwQ4t1f3j_Gx6x2Nc=s0-d-e1-ft#https://www.companiesmadesimple.com/project/upload/imgs/img1223.png" style="color: rgb(102, 102, 102); font-family: arial, sans-serif; font-size: 12.7272720336914px;" alt="" /></p>
MAIL;


    /**
     * Migrate Up.
     */
    public function up()
    {
        $escapedQuotes = str_replace('"', '\"', $this->newMail);
        $this->query('UPDATE cms2_pages SET text = "' . $escapedQuotes . '" WHERE node_id = 501');
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $escapedQuotes = str_replace('"', '\"', $this->oldMail);
        $this->query('UPDATE cms2_pages SET text = "' . $escapedQuotes . '" WHERE node_id = 501');
    }
}
