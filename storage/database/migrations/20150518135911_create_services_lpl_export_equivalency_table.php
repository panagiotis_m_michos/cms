<?php

use Phinx\Migration\AbstractMigration;

class CreateServicesLplExportEquivalencyTable extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('lpl_product_equivalencies', ['id' => 'lplProductEquivalencyId', 'primary_key' => 'lplProductEquivalencyId']);
        $table->addColumn('productId', 'integer')
            ->addColumn('isRegisteredOffice', 'integer')
            ->addColumn('isServiceAddress', 'integer')
            ->save();

        $this->query(
            'INSERT INTO
              lpl_product_equivalencies (productId, isRegisteredOffice, isServiceAddress)
            VALUES
              (165,1,0),
              (334,1,0),
              (342,1,1),
              (379,1,0),
              (475,0,1),
              (806,0,1),
              (899,1,1),
              (1257,1,0),
              (1315,1,1),
              (1316,1,1),
              (1317,1,1),
              (1353,1,1)'
        );

        $this->query(
            'CREATE VIEW lpl_active_services_export AS
            SELECT
              company_number as companyNumber,
              IF(SUM(isRegisteredOffice) > 0, 1, 0) AS hasRegisteredOffice,
              IF(SUM(isServiceAddress) > 0, 1, 0) AS hasServiceAddress
            FROM
              cms2_services s
            LEFT JOIN lpl_product_equivalencies USING (productId)
            LEFT JOIN ch_company c ON (s.companyId = c.company_id)
            WHERE parentId IS NULL
            AND dtExpires > NOW()
            GROUP BY companyId'
        );
    }

    public function down()
    {
        $this->dropTable('lpl_product_equivalencies');
        $this->query('DROP VIEW lpl_active_services_export');
    }
}
