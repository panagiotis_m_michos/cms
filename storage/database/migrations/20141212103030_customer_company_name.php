<?php

use Phinx\Migration\AbstractMigration;

class CustomerCompanyName extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('cms2_customers');
        $table->addColumn(
            'companyName',
            'string',
            array(
                'after' => 'lastName',
                'default' => NULL,
                'null' => TRUE
            )
        );
        $table->update();
    }
}