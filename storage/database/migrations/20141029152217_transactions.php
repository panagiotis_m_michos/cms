<?php

use Phinx\Migration\AbstractMigration;

class Transactions extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $table = $this->table('cms2_transactions');
        $this->execute("ALTER TABLE cms2_transactions ADD tokenId INT NULL DEFAULT NULL AFTER cardNumber");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $table = $this->table('cms2_transactions');
        $table->removeColumn('tokenId');
    }
}