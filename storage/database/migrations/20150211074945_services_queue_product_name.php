<?php

use Phinx\Migration\AbstractMigration;

class ServicesQueueProductName extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('cms2_vo_services_queue');
        $table->addColumn('productName', 'string', array('after' => 'productId'));
        $table->update();
    }
}