<?php

use Phinx\Migration\AbstractMigration;

class PscOnlineRegisterChanges extends AbstractMigration
{
    public function up()
    {
        $this->table('cms2_register_pscs')
            ->addColumn('legalPersonName', 'string', ['after' => 'corporateName', 'null' => TRUE])
            ->renameColumn('dateCessated', 'dateCancelled')
            ->changeColumn(
                'entryType',
                'enum',
                [
                    'values' => ['PERSON_PSC', 'CORPORATE_PSC', 'LEGAL_PERSON_PSC', 'COMPANY_STATEMENT'],
                    'after' => 'companyId',
                ]
            )
            ->update();
    }

    public function down()
    {
        $this->table('cms2_register_pscs')
            ->removeColumn('legalPersonName')
            ->renameColumn('dateCancelled', 'dateCessated')
            ->changeColumn(
                'entryType',
                'enum',
                [
                    'values' => ['PERSON_PSC', 'CORPORATE_PSC', 'COMPANY_STATEMENT'],
                    'after' => 'companyId',
                ]
            )
            ->update();
    }
}
