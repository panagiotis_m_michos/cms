<?php

use Phinx\Migration\AbstractMigration;

class RenameHowToMakeProfitEbookOffer extends AbstractMigration
{
    public function up()
    {
        $this->query('UPDATE cms2_toolkit_offers SET type = "makeMoreProfitGuide" WHERE type = "howToMakeAProfitEbook"');
    }

    public function down()
    {
        $this->query('UPDATE cms2_toolkit_offers SET type = "howToMakeAProfitEbook" WHERE type = "makeMoreProfitGuide"');
    }
}
