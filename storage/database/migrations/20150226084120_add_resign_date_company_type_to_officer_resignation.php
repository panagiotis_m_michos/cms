<?php

use Phinx\Migration\AbstractMigration;

class AddResignDateCompanyTypeToOfficerResignation extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $table = $this->table(TBL_OFFICER_RESIGNATIONS);
        $table->addColumn(
            'resign_date',
            'date',
            array(
                'default' => NULL,
                'null' => TRUE
            )
        );
        $table->addColumn(
            'company_type',
            'string',
            array(
                'length' => 160,
                'default' => NULL,
                'null' => TRUE
            )
        );
        $table->update();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $table = $this->table(TBL_OFFICER_RESIGNATIONS);
        $table->removeColumn('resign_date');
        $table->removeColumn('company_type');
        $table->update();
    }
}
