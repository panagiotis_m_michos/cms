<?php

use Phinx\Migration\AbstractMigration;

class HttpToHttps extends AbstractMigration
{
    /**
     * @var array
     */
    private $websites = [
        'www.companysearchesmadesimple.com' => 'www.companysearchesmadesimple.com',
        'www.londonpresence.com' => 'www.londonpresence.com',
        'www.companiesmadesimple.com' => 'www.companiesmadesimple.com',
        'www.businesstrainingmadesimple.co.uk' => 'www.businesstrainingmadesimple.co.uk',
        'www.madesimplegroup.com' => 'www.madesimplegroup.com',
        'companysearchesmadesimple.com' => 'www.companysearchesmadesimple.com',
        'londonpresence.com' => 'www.londonpresence.com',
        'companiesmadesimple.com' => 'www.companiesmadesimple.com',
        'businesstrainingmadesimple.co.uk' => 'www.businesstrainingmadesimple.co.uk',
        'madesimplegroup.com' => 'www.madesimplegroup.com',
    ];
    
    public function up()
    {
        foreach ($this->websites as $website) {
            $from = 'http://' . $website;
            $to = 'https://' . $website;
            $this->query(sprintf('UPDATE %s SET text = REPLACE(text, "%s", "%s") WHERE text LIKE "%%%s%%"', TBL_PAGES, $from, $to, 'http://'));
        }
    }

    public function down()
    {
        foreach ($this->websites as $website) {
            $from = 'https://' . $website;
            $to = 'http://' . $website;
            $this->query(sprintf('UPDATE %s SET text = REPLACE(text, "%s", "%s") WHERE text LIKE "%%%s%%"', TBL_PAGES, $from, $to, 'https://'));
        }
    }
}
