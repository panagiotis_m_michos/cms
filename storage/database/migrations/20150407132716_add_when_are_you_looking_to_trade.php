<?php

use Phinx\Migration\AbstractMigration;

class AddWhenAreYouLookingToTrade extends AbstractMigration
{
    public function change()
    {
        $this->table(TBL_ANSWERS)->addColumn('tradingStart', 'string', ['null' => TRUE])->update();
        $this->table(TBL_ANSWERS)->addColumn('companyId', 'integer', ['null' => TRUE])->update();
    }
}
