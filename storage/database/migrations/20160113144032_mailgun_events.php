<?php

use Phinx\Migration\AbstractMigration;

class MailgunEvents extends AbstractMigration
{
    public function change()
    {
        $queue = $this->table('cms2_mailgun_events', ['id' => 'mailgunEventId']);
        $queue
            ->addColumn('status', 'string')
            ->addColumn('emailTemplateId', 'string', ['null' => TRUE])
            ->addColumn('recipient', 'string')
            ->addColumn('customerId', 'integer', ['null' => TRUE])
            ->addColumn('eventBody', 'text')
            ->addColumn('createdAt', 'datetime')
            ->create();
    }
}
