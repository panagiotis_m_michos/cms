<?php

use Phinx\Migration\AbstractMigration;

class OrderRefundEmail extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $email = <<<END
<p>Hello&#160;[FIRST_NAME],</p>
<p>We can confirm that a refund has been carried out on your account.</p>
<p>Order Id:&#160;[ORDER_ID]</p>
<p>[REFUND_AMOUNT]</p>
<p>Please get in touch if you have any questions.</p>
<p>Best wishes,</p>
<p>The Team @ Company Formation MadeSimple<br />
<a href="http://www.companiesmadesimple.com/">www.companiesmadesimple.com</a></p>
<p>Telephone: 020 7608 5500<br />
Email: <a href="mailto: theteam@madesimplegroup.com">theteam@madesimplegroup.com</a></p>
<p>Your business essentials MadeSimple</p>
<p><img border="0" src="https://ci4.googleusercontent.com/proxy/U1DLbETo-qwhctdNkw-yELjNnT-ZTRAMD3zhirvy_nddf6fiaGY2Tvd5-p-6tFJDFjiffixN2SgfTrNHeSK1ZLHD-A4mTicSvrWgAlEwQ4t1f3j_Gx6x2Nc=s0-d-e1-ft#https://www.companiesmadesimple.com/project/upload/imgs/img1223.png" style="color: rgb(102, 102, 102); font-family: arial, sans-serif; font-size: 12.7272720336914px;" alt="" /></p>
END;
        $this->runQuery($email);
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $email = <<<END
<p>Hello&#160;[FIRST_NAME],</p>
<p>We can confirm that a refund has been carried out on your account.</p>
<p>Order Id:&#160;[ORDER_ID]</p>
<p>Refund Amount: £[REFUND_AMOUNT]</p>
<p>Please get in touch if you have any questions.</p>
<p>Best wishes,</p>
<p>The Team @ Company Formation MadeSimple<br />
<a href="http://www.companiesmadesimple.com/">www.companiesmadesimple.com</a></p>
<p>Telephone: 020 7608 5500<br />
Email: <a href="mailto: theteam@madesimplegroup.com">theteam@madesimplegroup.com</a></p>
<p>Your business essentials MadeSimple</p>
<p><img border="0" src="https://ci4.googleusercontent.com/proxy/U1DLbETo-qwhctdNkw-yELjNnT-ZTRAMD3zhirvy_nddf6fiaGY2Tvd5-p-6tFJDFjiffixN2SgfTrNHeSK1ZLHD-A4mTicSvrWgAlEwQ4t1f3j_Gx6x2Nc=s0-d-e1-ft#https://www.companiesmadesimple.com/project/upload/imgs/img1223.png" style="color: rgb(102, 102, 102); font-family: arial, sans-serif; font-size: 12.7272720336914px;" alt="" /></p>
END;
        $this->runQuery($email);

    }

    /**
     * @param $email
     */
    private function runQuery($email)
    {
        $this->query("UPDATE cms2_pages SET text = '$email' WHERE node_id = 621 AND language_pk = 'EN'");
    }
}