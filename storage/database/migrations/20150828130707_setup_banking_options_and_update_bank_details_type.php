<?php

use Phinx\Migration\AbstractMigration;

class SetupBankingOptionsAndUpdateBankDetailsType extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $sql = <<<SQL

                INSERT INTO `cms2_properties` (`nodeId`, `name`, `value`, `authorId`, `editorId`, `dtc`, `dtm`) VALUES
                (1313, 'bankingEnabled', '1', 111, 111, NOW(), NOW()),
                (1313, 'bankingOptions', 'BARCLAYS,TSB,CARD_ONE', 111, 111, NOW(), NOW()),
                (1314, 'bankingEnabled', '1', 111, 111, NOW(), NOW()),
                (1314, 'bankingOptions', 'BARCLAYS,TSB,CARD_ONE', 111, 111, NOW(), NOW()),
                (1315, 'bankingEnabled', '1', 111, 111, NOW(), NOW()),
                (1315, 'bankingOptions', 'BARCLAYS,TSB,CARD_ONE', 111, 111, NOW(), NOW()),
                (1316, 'bankingEnabled', '1', 111, 111, NOW(), NOW()),
                (1316, 'bankingOptions', 'BARCLAYS,TSB,CARD_ONE', 111, 111, NOW(), NOW()),
                (1317, 'bankingEnabled', '1', 111, 111, NOW(), NOW()),
                (1317, 'bankingOptions', 'BARCLAYS,TSB,CARD_ONE', 111, 111, NOW(), NOW()),

                (379, 'bankingEnabled', '1', 111, 111, NOW(), NOW()),
                (379, 'bankingOptions', 'BARCLAYS', 111, 111, NOW(), NOW()),
                (1175, 'bankingEnabled', '1', 111, 111, NOW(), NOW()),
                (1175, 'bankingOptions', 'BARCLAYS', 111, 111, NOW(), NOW()),

                (1022, 'bankingEnabled', '1', 111, 111, NOW(), NOW()),
                (1022, 'bankingOptions', 'BARCLAYS,CARD_ONE', 111, 111, NOW(), NOW()),
                (898, 'bankingEnabled', '1', 111, 111, NOW(), NOW()),
                (898, 'bankingOptions', 'BARCLAYS,CARD_ONE', 111, 111, NOW(), NOW()),
                (899, 'bankingEnabled', '1', 111, 111, NOW(), NOW()),
                (899, 'bankingOptions', 'BARCLAYS,CARD_ONE', 111, 111, NOW(), NOW());

                UPDATE `cms2_nodes`
                SET `admin_controler` = 'TsbEmailAdminControler'
                WHERE `node_id` = 1634;
SQL;

        $this->execute($sql);
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $sql = <<<SQL

                DELETE FROM `cms2_properties`
                WHERE
                  `nodeId` IN (1313, 1314, 1315, 1316, 1317, 379, 1175, 1022, 898, 899)
                  AND `name` IN ('bankingEnabled', 'bankingOptions');


                UPDATE `cms2_nodes`
                SET `admin_controler` = 'EmailAdminControler'
                WHERE `node_id` = 1634;
SQL;

        $this->execute($sql);
    }
}
