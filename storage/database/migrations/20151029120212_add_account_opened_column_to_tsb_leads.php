<?php

use Phinx\Migration\AbstractMigration;

class AddAccountOpenedColumnToTsbLeads extends AbstractMigration
{
    public function change()
    {
        $this
            ->table(TBL_TSB_LEADS)
            ->addColumn('accountOpened', 'boolean', ['after' => 'companyId', 'null' => FALSE, 'default' => 0])
            ->update();
    }
}
