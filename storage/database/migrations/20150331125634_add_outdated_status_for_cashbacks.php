<?php

use Phinx\Migration\AbstractMigration;

class AddOutdatedStatusForCashbacks extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("ALTER TABLE cms2_cashback CHANGE statusId statusId ENUM('IMPORTED','ELIGIBLE','PAID','OUTDATED') NOT NULL");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->execute("ALTER TABLE cms2_cashback CHANGE statusId statusId ENUM('IMPORTED','ELIGIBLE','PAID') NOT NULL");
    }
}
