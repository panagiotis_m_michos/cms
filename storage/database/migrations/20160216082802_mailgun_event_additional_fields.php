<?php

use Phinx\Migration\AbstractMigration;

class MailgunEventAdditionalFields extends AbstractMigration
{
    public function change()
    {
        $this->table('cms2_mailgun_events')
            ->addColumn('campaign', 'string', ['null' => TRUE])
            ->addColumn('scenario', 'string', ['null' => TRUE])
            ->addColumn('site', 'string', ['null' => TRUE])
            ->update();
    }
}
