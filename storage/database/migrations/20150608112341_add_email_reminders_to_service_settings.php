<?php

use Phinx\Migration\AbstractMigration;

class AddEmailRemindersToServiceSettings extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     */
    public function change()
    {
        $this->table(TBL_SERVICE_SETTINGS)
            ->addColumn(
                'emailReminders',
                'boolean',
                ['default' => TRUE, 'after' => 'renewalTokenId']
            )
            ->update();
    }
}
