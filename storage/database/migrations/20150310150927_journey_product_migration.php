<?php

use Phinx\Migration\AbstractMigration;

class JourneyProductMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $sql = <<<EOD
CREATE TABLE IF NOT EXISTS `cms2_offer_customer_details` (
  `customerDetailsId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `postcode` varchar(255) DEFAULT NULL,
  `companyName` varchar(255) DEFAULT NULL,
  `insuranceReturnDate` date DEFAULT NULL,
  `productId` int(10) unsigned NOT NULL,
  `customerId` int(10) unsigned NOT NULL,
  `dtc` datetime NOT NULL,
  `dtm` datetime NOT NULL,
  PRIMARY KEY (`customerDetailsId`),
  KEY `productId` (`productId`,`customerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
EOD;

        $this->query($sql);

    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $table = $this->table('cms2_offer_customer_details');
        $table->drop();

    }
}