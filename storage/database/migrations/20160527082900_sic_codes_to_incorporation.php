<?php

use Phinx\Migration\AbstractMigration;

class SicCodesToIncorporation extends AbstractMigration
{
    public function change()
    {
        $this->table('ch_company_incorporation')
            ->addColumn('sicCode1', 'string', ['null' => TRUE])
            ->addColumn('sicCode2', 'string', ['null' => TRUE])
            ->addColumn('sicCode3', 'string', ['null' => TRUE])
            ->addColumn('sicCode4', 'string', ['null' => TRUE])
            ->update();
    }
}
