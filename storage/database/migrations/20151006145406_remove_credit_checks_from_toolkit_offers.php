<?php

use Phinx\Migration\AbstractMigration;

class RemoveCreditChecksFromToolkitOffers extends AbstractMigration
{
    public function up()
    {
        $this->table(TBL_TOOLKIT_OFFERS)
            ->addColumn('dateValidTo', 'date', ['null' => TRUE, 'after' => 'dateValidFrom', 'default' => NULL])
            ->update();

        $this->query(sprintf('UPDATE %s SET dateValidTo = NOW() - INTERVAL 1 DAY WHERE type = "creditChecks"', TBL_TOOLKIT_OFFERS));
    }

    public function down()
    {
        $this->table(TBL_TOOLKIT_OFFERS)
            ->removeColumn('dateValidTo');
    }
}
