<?php

use Phinx\Migration\AbstractMigration;

class AdditionalIndices extends AbstractMigration
{
    public function change()
    {
        $this->table('cms2_customers')
            ->addIndex(['statusId'])
            ->addIndex(['tagId'])
            ->addIndex(['email'])
            ->save();

        $this->table('ch_tsb_leads')
            ->addIndex(['companyId'])
            ->save();

        $this->table('cms2_mailgun_events')
            ->addIndex(['emailTemplateId'])
            ->addIndex(['customerId'])
            ->save();

        $this->table('cms2_orders_items')
            ->addIndex(['refundedOrderItemId'])
            ->save();
    }
}
