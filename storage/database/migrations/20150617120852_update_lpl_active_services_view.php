<?php

use Phinx\Migration\AbstractMigration;

class UpdateLplActiveServicesView extends AbstractMigration
{
    public function up()
    {
        $this->query('DROP VIEW lpl_active_services_export');
        $this->query(
            "CREATE VIEW lpl_active_services_export AS
            SELECT
              company_number as companyNumber,
              IF(SUM(isRegisteredOffice) > 0, 1, 0) AS hasRegisteredOffice,
              IF(SUM(isServiceAddress) > 0, 1, 0) AS hasServiceAddress
            FROM
              cms2_services s
            LEFT JOIN lpl_product_equivalencies USING (productId)
            LEFT JOIN ch_company c ON (s.companyId = c.company_id)
            WHERE parentId IS NULL
            AND dtExpires >= (CURDATE() - INTERVAL 28 DAY)
            GROUP BY companyId"
        );
    }

    public function down()
    {
        $this->query('DROP VIEW lpl_active_services_export');
        $this->query(
            'CREATE VIEW lpl_active_services_export AS
            SELECT
              company_number as companyNumber,
              IF(SUM(isRegisteredOffice) > 0, 1, 0) AS hasRegisteredOffice,
              IF(SUM(isServiceAddress) > 0, 1, 0) AS hasServiceAddress
            FROM
              cms2_services s
            LEFT JOIN lpl_product_equivalencies USING (productId)
            LEFT JOIN ch_company c ON (s.companyId = c.company_id)
            WHERE parentId IS NULL
            AND dtExpires > NOW()
            GROUP BY companyId'
        );
    }
}
