<?php

use Phinx\Migration\AbstractMigration;

class AddCashbackIsArchive extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->table('cms2_cashback')->addColumn('isArchived', 'boolean')->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->table('cms2_cashback')->removeColumn('isArchived')->save();
    }
}