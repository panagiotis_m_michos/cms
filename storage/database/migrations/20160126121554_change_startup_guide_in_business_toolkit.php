<?php

use Phinx\Migration\AbstractMigration;

class ChangeStartupGuideInBusinessToolkit extends AbstractMigration
{
    public function up()
    {
        $this->query(
            <<<QUERY
            UPDATE
              cms2_toolkit_offers
            SET
              type = "howToMakeAProfitEbook",
              name = "Free How To Make A Profit eBook",
              description = "<b>Everything you need to know to help your business make a profit</b> <br>
              &bullet; Receive an expert’s guide to making profit<br>
              &bullet; Easy to understand and jargon-free<br>
              &bullet; Avoid the common mistakes many news businesses make<br>
              &bullet; Perfect if you’ve never started a business before"
            WHERE
              type = "businessStartupGuide"
QUERY
        );
    }

    public function down()
    {
        $this->query(
            <<<QUERY
            UPDATE
              cms2_toolkit_offers
            SET
              type = "businessStartupGuide",
              name = "Free Business Startup Guide",
              description = "<b>Free Business Startup Guide (worth &pound;7.99)</b> <br>
              &bullet; Receive an expert's guide to starting your business (ebook)<br>
              &bullet; Easy to understand and jargon-free<br>
              &bullet; Avoid the common mistakes many new businesses make<br>
              &bullet; Perfect if you've never started a business before"
            WHERE
              type = "howToMakeAProfitEbook"
QUERY
        );
    }
}
