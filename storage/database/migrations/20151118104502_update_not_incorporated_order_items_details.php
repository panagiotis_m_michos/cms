<?php

use Phinx\Migration\AbstractMigration;

class UpdateNotIncorporatedOrderItemsDetails extends AbstractMigration
{
    public function up()
    {
        $formations = <<<SQL
UPDATE cms2_orders_items oi
INNER JOIN ch_company c ON c.order_id = oi.orderId
SET
    oi.companyId = c.company_id,
    oi.companyName = c.company_name,
    oi.companyNumber = c.company_number
WHERE 1=1
AND companyId IS NULL
AND companyName IS NULL
AND companyNumber IS NULL
SQL;

        $missingCompanyName = <<<SQL
UPDATE cms2_orders_items oi
INNER JOIN ch_company c ON c.company_id = oi.companyId
SET
    oi.companyName = c.company_name
WHERE 1=1
AND c.company_number IS NOT NULL
AND c.incorporation_date IS NOT NULL
AND companyName IS NULL
SQL;

        $missingCompanyNumber = <<<SQL
UPDATE cms2_orders_items oi
INNER JOIN ch_company c ON c.company_id = oi.companyId
SET
    oi.companyNumber = c.company_number
WHERE 1=1
AND c.company_number IS NOT NULL
AND c.incorporation_date IS NOT NULL
AND companyNumber IS NULL
SQL;

        $this->execute($formations);
        $this->execute($missingCompanyName);
        $this->execute($missingCompanyNumber);
    }
}
