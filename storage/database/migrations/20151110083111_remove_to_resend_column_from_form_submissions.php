<?php

use Phinx\Migration\AbstractMigration;

class RemoveToResendColumnFromFormSubmissions extends AbstractMigration
{
    public function up()
    {
        $this
            ->table(TBL_FORM_SUBMISSIONS)
            ->removeColumn('toResend')
            ->update();
    }

    public function down()
    {
        $this
            ->table(TBL_FORM_SUBMISSIONS)
            ->addColumn('toResend', 'boolean')
            ->update();
    }
}
