<?php

use Phinx\Migration\AbstractMigration;

class UpdateVoServicesQueueTableWithRelationships extends AbstractMigration
{
    public function up()
    {
        $this->execute(
            'ALTER TABLE `cms2_vo_services_queue`
          ADD `customerId` INT(11) NOT NULL AFTER `voServiceQueueId`,
          ADD `companyId` INT(11) NOT NULL AFTER `customerId`,
          ADD `orderItemId` INT(11) NOT NULL AFTER `productName`,
          DROP `customerEmail`,
          DROP `orderId`,
          DROP `dateSent`;'
        );
    }

    public function down()
    {
        $this->execute(
            'ALTER TABLE `cms2_vo_services_queue`
          DROP `customerId`,
          DROP `companyId`,
          DROP `orderItemId`,
          ADD `customerEmail` VARCHAR(255) NOT NULL AFTER `voServiceQueueId`,
          ADD `orderId` INT(11) NOT NULL AFTER `productName`,
          ADD `dateSent` DATETIME NULL AFTER `durationInMonths`;'
        );
    }
}
