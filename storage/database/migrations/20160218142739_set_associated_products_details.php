<?php

use Phinx\Migration\AbstractMigration;

class SetAssociatedProductsDetails extends AbstractMigration
{
    public function up()
    {
        $sql = <<<QUERY
DELETE FROM cms2_properties WHERE name IN ("associatedText", "associatedDescription", "associatedIconClass") AND nodeId IN (589, 115, 165, 172, 298, 299, 301, 302, 317, 318, 321, 322, 324, 325, 331, 443, 475, 565, 567, 569, 666, 782);

INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (589, "associatedText", "Jump the processing queue at Companies House when you submit incorporation details before 2pm (Monday-Friday, excluding Bank Holidays).", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (589, "associatedDescription", "If you need an absolute guarantee that your company will be incorporated on the same day - this is for you. Average formation time with the queue jump is 24 minutes.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (301, "associatedIconClass", "fa-book", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (301, "associatedText", "A purpose built register which is perfect for storing all of your official statutory company documents.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (301, "associatedDescription", "All companies are legally required to keep an ongoing set of company records.  A company register will help keep everything ordered and in one place.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (302, "associatedIconClass", "fa-envelope-o", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (302, "associatedText", "A tool that allows you to emboss any document with your legally registered company name and incorporation number.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (302, "associatedDescription", "Although not mandatory, use of the seal has legal force within the UK and overseas.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (321, "associatedIconClass", "fa-file-pdf-o", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (321, "associatedText", "A comprehensive e-guide which provides a wide range of tax tips, planning advice and practical how-to’s, geared to the small and startup communities.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (321, "associatedDescription", "To help you understand tax for your business, the guide covers topics such as business strategies, tax and work, savings and investments, estate planning, and more.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (322, "associatedIconClass", "fa-file-pdf-o", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (322, "associatedText", "An easy to understand e-guide full of practical tips and essential information to help you start, run and grow your new business.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (322, "associatedDescription", "Learn all about cash flow management, marketing, insurance, finance and keeping records, taxation, disaster planning and much more.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (443, "associatedIconClass", "fa-envelope-o", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (443, "associatedText", "Ideal if you work from home and want to keep your home address private, or if you want a prestigious London address in the heart of tech city.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (443, "associatedDescription", "With your own N1 address, we’ll forward all your business mail for 3 months (price includes a £20 postage deposit). We highly recommend this service to any customer using our N1 Registered Office.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (317, "associatedIconClass", "fa-book", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (317, "associatedText", "Receive a copy of your company’s Memorandum & Articles of Association in a bound folder, detailing the company’s ‘rules and regulations’ by which it will be managed.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (317, "associatedDescription", "Your company’s M&As may be required when opening a business bank account and when dealing with insurance companies or providers of finance.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (324, "associatedIconClass", "fa-file-pdf-o", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (324, "associatedText", "A specially written 24-page e-guide encompassing everything you need to know about improving profit for your new company.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (324, "associatedDescription", "The guide is full of helpful advice and practical information including how to make your business profitable, profit improvement strategy and creating a company profit plan.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (565, "associatedIconClass", "fa-folder-open-o", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (565, "associatedText", "A pack of 3 comprehensive startup e-guides, designed to help get your new business off to the best possible start.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (565, "associatedDescription", "Includes: Business Startup Guide, Tax and Financial Strategies Guide and How to Make a Profit Guide.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (569, "associatedIconClass", "fa-file-archive-o", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (569, "associatedText", "A purpose built set of business forms and templates designed for new companies.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (569, "associatedDescription", "The pack contains downloadable templates such as terms and conditions, employment contract, health and safety policy, invoice template, final payment reminder and more.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (165, "associatedIconClass", "fa-building", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (165, "associatedText", "Use our prestigious N1 London address as your company’s registered office and keep your residential address off the public register.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (165, "associatedDescription", "The registered office is the official address of a limited company and must be a physical location in the UK. Our service is ideal if you want to protect your residential address from being shown on the public register or if you’re an overseas customer who needs a UK address. NB. This is not a full mail forwarding service. Only mail from Companies House, HMRC and other Government bodies will be forwarded (this mail is forwarded to you free of charge)", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (475, "associatedIconClass", "fa-home", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (475, "associatedText", "Keep the residential address of all your company officers confidential with the use of our London N1 Address.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (475, "associatedDescription", "All official government mail (from HMRC, Companies House) will be forwarded for 1 year for all company directors, shareholders and secretaries.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (331, "associatedIconClass", "fa-file-text-o", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (331, "associatedText", "Put your official mark on company documents, including receipts and invoices.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (331, "associatedDescription", "A custom made, self-inking stamp with your company name and number which lasts for up to 20,000 prints.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (666, "associatedIconClass", "fa-calendar", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (666, "associatedText", "Cut the hassle out of filing your Annual Return, use our simple online system with a highly automated process, pre-populated fields and error checking facilities.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (666, "associatedDescription", "All companies must file an Annual Return with Companies House at least once every 12 months. This Annual Return DIY service includes the Companies House filing fee.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (782, "associatedIconClass", "fa-folder-open-o", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (782, "associatedText", "A pack of 3 comprehensive startup e-guides, designed to help get your new business off to the best possible start.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (782, "associatedDescription", "Includes: Business Startup Guide, Tax and Financial Strategies Guide and How to Make a Profit Guide.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (299, "associatedIconClass", "fa-pencil-square-o", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (299, "associatedText", "We can help you register your company for VAT (Value Added Tax) with HMRC.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (299, "associatedDescription", "Being VAT registered allows you to be able to claim back the VAT on anything your company pays for.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (298, "associatedIconClass", "fa-users", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (298, "associatedText", "We’ll help register you as an employer for HMRC’s Pay As You Earn (PAYE) system, a requirement if you intend to hire employees.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (298, "associatedDescription", "Complete a simple online form and it will be reviewed by one of our experts. We’ll then submit the application to HMRC for you.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (325, "associatedIconClass", "fa-file-word-o", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (325, "associatedText", "A 135 page e-guide full of tips to help you use Microsoft Office; including Word, Excel, Outlook, PowerPoint and Access.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (325, "associatedDescription", "Written by one of the leading Microsoft product experts - Simon Hurst - this guide has animated routines so you can watch and learn and get the most out of the software.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (318, "associatedIconClass", "fa-files-o", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (318, "associatedText", "Receive a printed copy of your certificate of incorporation on special Companies House approved paper.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (318, "associatedDescription", "A hard copy of this document is often required when opening a business bank account or when dealing with insurance companies or providers of finance.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (567, "associatedIconClass", "fa-book", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (567, "associatedText", "A company register to store your statutory records, plus your company name and number on a custom made self-inking stamp and a company seal to emboss any document.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (567, "associatedDescription", "This pack helps you keep all your company records in one place and enables you to put your official mark on any company documents, including invoices and receipts.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (172, "associatedIconClass", "fa-user", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (172, "associatedText", "Using a nominee shareholder ensures your anonymity and confidentiality, as only our nominee details will appear on the public record.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (172, "associatedDescription", "With this service, we will be appointed as the nominee shareholder of your new company. We will handle any signing of documents on your instruction.", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (115, "associatedIconClass", "fa-certificate", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (115, "associatedText", "This service will get your company documents certified by the Foreign Commonwealth Office (FCO).", NOW(), NOW());
INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (115, "associatedDescription", "Once we know what country/countries the apostille will be used in and your chosen delivery address, the required documents are produced, certified, apostilled and dispatched to you by special delivery. The service generally takes 10-14 working days.", NOW(), NOW());

INSERT INTO cms2_properties (nodeId, name, value, dtc, dtm) VALUES (1350, "removableFromBasket", 0, NOW(), NOW());
INSERT INTO `cms2_properties` (`nodeId`, `name`, `value`, `dtc`, `dtm`) VALUES (201, 'removeFromBasketConfirmation', 'Company fraud protection notifies you of any changes to your company (director resignations etc.), even if you didn\'t make them. OK = remove, Cancel = keep.', NOW(), NOW());
QUERY;

        $this->execute($sql);
    }

    public function down()
    {
        $sql = <<<QUERY
        DELETE FROM cms2_properties WHERE name IN ("associatedText", "associatedDescription", "associatedIconClass") AND nodeId IN (589, 115, 165, 172, 298, 299, 301, 302, 317, 318, 321, 322, 324, 325, 331, 443, 475, 565, 567, 569, 666, 782);

        INSERT INTO `cms2_properties` (`nodeId`, `name`, `value`, `authorId`, `editorId`, `dtc`, `dtm`) VALUES
(115,	'associatedText',	'',	8,	12,	'2013-09-24 13:57:02',	'2016-01-15 12:50:36'),
(165,	'associatedText',	'',	8,	81,	'2014-03-18 09:19:16',	'2015-06-24 14:35:52'),
(321,	'associatedText',	'',	8,	109,	'2014-03-18 09:22:56',	'2016-01-20 17:35:37'),
(475,	'associatedText',	'',	8,	12,	'2014-03-18 09:23:56',	'2015-03-27 16:03:20'),
(301,	'associatedText',	'',	12,	12,	'2013-08-19 11:05:40',	'2016-01-15 12:43:49'),
(666,	'associatedText',	'',	12,	12,	'2013-11-21 14:43:52',	'2015-05-07 12:38:04'),
(317,	'associatedText',	'',	43,	81,	'2014-08-07 15:18:39',	'2015-12-22 11:37:03'),
(172,	'associatedText',	'',	81,	56,	'2013-07-02 09:00:08',	'2015-12-03 09:22:49'),
(443,	'associatedText',	'',	81,	129,	'2014-07-25 10:00:17',	'2015-08-14 15:19:42'),
(589,	'associatedText',	'Jump the processing queue at Companies House, Mon-Fri when submitted by 2pm.',	81,	12,	'2014-07-25 13:28:37',	'2015-05-07 12:32:56'),
(565,	'associatedText',	'',	81,	94,	'2014-08-01 11:29:31',	'2016-01-26 11:20:41'),
(782,	'associatedText',	'Start, run and grow with our \"Tax & Financial Strategies Guide\" and \"How to Make a Profit Guide\".',	94,	94,	'2013-06-26 10:28:42',	'2016-01-26 11:11:45'),
(318,	'associatedText',	'An extra copy of the Certificate of Incorporation.',	94,	12,	'2013-06-26 10:23:02',	'2015-05-07 12:50:52'),
(299,	'associatedText',	'We can help you register your company for VAT (Value Added Tax) with HMRC.',	94,	56,	'2013-06-26 10:29:59',	'2015-12-02 12:55:41'),
(567,	'associatedText',	'Get off to a professional start with a Company Register, a bespoke Company Seal and a bespoke Company Stamp.',	94,	12,	'2013-06-26 10:30:34',	'2016-01-15 12:44:29'),
(298,	'associatedText',	'Our comprehensive and confidential payroll registration service.',	94,	12,	'2013-06-26 10:31:12',	'2015-09-07 16:59:20'),
(302,	'associatedText',	'Custom made Company Seal for embossing documents with your company name and company number.',	94,	12,	'2013-06-26 10:31:43',	'2015-05-07 12:26:18'),
(331,	'associatedText',	'Custom made Company Stamp for stamping documents with your company name.',	94,	12,	'2013-06-26 10:32:33',	'2015-06-22 13:25:31'),
(569,	'associatedText',	'',	114,	81,	'2014-09-25 10:49:11',	'2015-04-13 09:29:03'),
(322,	'associatedText',	'',	114,	109,	'2014-09-25 10:55:13',	'2016-01-20 17:47:35'),
(324,	'associatedText',	'',	114,	109,	'2014-09-25 11:01:48',	'2016-01-20 17:47:55'),
(325,	'associatedText',	'',	114,	114,	'2014-09-25 11:03:50',	'2014-09-25 11:03:50');

DELETE FROM cms2_properties WHERE nodeId = 1350 AND `name` = "removableFromBasket";
DELETE FROM cms2_properties WHERE nodeId = 201 AND `name` = "removeFromBasketConfirmation";
QUERY;

        $this->execute($sql);
    }
}
