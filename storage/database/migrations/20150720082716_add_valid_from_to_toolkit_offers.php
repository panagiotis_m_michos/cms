<?php

use Phinx\Migration\AbstractMigration;

class AddValidFromToToolkitOffers extends AbstractMigration
{
    public function up()
    {
        $this->table(TBL_TOOLKIT_OFFERS)
            ->addColumn('dateValidFrom', 'date', ['null' => TRUE, 'after' => 'description', 'default' => NULL])
            ->update();

        $this->query(sprintf('UPDATE %s SET dateValidFrom = NOW() WHERE type != "googleAppsDiscount"', TBL_TOOLKIT_OFFERS));
    }

    public function down()
    {
        $this->table(TBL_TOOLKIT_OFFERS)
            ->removeColumn('dateValidFrom')
            ->update();
    }
}
