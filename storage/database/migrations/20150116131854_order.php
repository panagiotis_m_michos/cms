<?php

use Phinx\Migration\AbstractMigration;

class Order extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->query("
ALTER TABLE  `cms2_orders` ADD  `credit` FLOAT( 7, 2 ) NOT NULL DEFAULT  '0' AFTER  `vat`;
ALTER TABLE  `cms2_orders` ADD  `refundCreditValue` FLOAT( 7, 2 ) NULL DEFAULT NULL AFTER  `refundValue` ;
");
    
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->query("
ALTER TABLE  `cms2_orders` DROP  `credit`;
ALTER TABLE  `cms2_orders` DROP  `refundCreditValue`;
");
    }
}