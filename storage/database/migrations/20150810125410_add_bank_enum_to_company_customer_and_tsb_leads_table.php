<?php

use Phinx\Migration\AbstractMigration;

class AddBankEnumToCompanyCustomerAndTsbLeadsTable extends AbstractMigration
{
    public function up()
    {
        $this->execute(
            "ALTER TABLE `cms2_company_customer`
            ADD `bankTypeId` ENUM('BARCLAYS','TSB','CARD_ONE','HSBC') COLLATE 'utf8_general_ci' NOT NULL AFTER `wholesalerId`;"
        );

        $this->table('ch_tsb_leads', ['id' => 'tsbLeadId', 'primary_key' => 'tsbLeadId'])
            ->addColumn('companyId', 'integer')
            ->addColumn('dtc', 'datetime')
            ->addColumn('dtm', 'datetime')
            ->create();

        $this->execute("
            DELETE cc
            FROM cms2_company_customer cc
            LEFT JOIN ch_company c ON c.company_id = cc.companyId
            WHERE c.company_id IS NULL;

            UPDATE cms2_company_customer cc
            SET cc.bankTypeId = '';

            UPDATE cms2_company_customer cc
            INNER JOIN ch_hsbc h ON h.company_id = cc.companyId
            SET cc.bankTypeId = 'HSBC';

            UPDATE cms2_company_customer cc
            INNER JOIN ch_card_one co ON co.company_id = cc.companyId
            SET cc.bankTypeId = 'CARD_ONE';

            UPDATE cms2_company_customer cc
            INNER JOIN ch_barclays b ON b.company_id = cc.companyId
            SET cc.bankTypeId = 'BARCLAYS';

            UPDATE cms2_company_customer cc
            INNER JOIN ch_company c ON c.company_id = cc.companyId
            INNER JOIN cms2_orders_items oi ON oi.orderId = c.order_id
            SET cc.bankTypeId = 'HSBC'
            WHERE oi.productId = 1024
            AND cc.bankTypeId = '';

            UPDATE cms2_company_customer cc
            INNER JOIN ch_company c ON c.company_id = cc.companyId
            INNER JOIN cms2_orders_items oi ON oi.orderId = c.order_id
            SET cc.bankTypeId = 'CARD_ONE'
            WHERE oi.productId = 1405
            AND cc.bankTypeId = '';

            UPDATE cms2_company_customer cc
            INNER JOIN ch_company c ON c.company_id = cc.companyId
            INNER JOIN cms2_orders_items oi ON oi.orderId = c.order_id
            SET cc.bankTypeId = 'BARCLAYS'
            WHERE oi.productId = 619
            AND cc.bankTypeId = '';

            SELECT cc.companyCustomerId, c.company_number
            FROM cms2_company_customer cc
            INNER JOIN ch_company c ON c.company_id = cc.companyId
            WHERE bankTypeId = '';
        ");
    }

    public function down()
    {
        $this->execute(
            "ALTER TABLE `cms2_company_customer` DROP `bankTypeId`"
        );

        $this->table('ch_tsb_leads')->drop();
    }
}
