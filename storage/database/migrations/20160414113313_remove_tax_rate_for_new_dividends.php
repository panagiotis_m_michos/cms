<?php

use Phinx\Migration\AbstractMigration;

class RemoveTaxRateForNewDividends extends AbstractMigration
{
    public function up()
    {
        $this->execute('UPDATE ch_dividend SET tax_rate = 0 WHERE paid_date >= "2016-04-06"');
    }

    public function down()
    {
        $this->execute('UPDATE ch_dividend SET tax_rate = 10 WHERE paid_date >= "2016-04-06"');
    }
}
