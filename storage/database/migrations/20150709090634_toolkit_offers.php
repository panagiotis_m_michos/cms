<?php

use Phinx\Migration\AbstractMigration;

class ToolkitOffers extends AbstractMigration
{

    public function up()
    {
        $this->table('cms2_toolkit_offers', ['id' => 'toolkitOfferId', 'primary_key' => 'toolkitOfferId'])
            ->addColumn('type', 'string')
            ->addColumn('name', 'string')
            ->addColumn('description', 'text')
            ->create();

        $this->table(
            'cms2_company_toolkit_offers',
            ['id' => 'companyToolkitOfferId', 'primary_key' => 'companyToolkitOfferId']
        )
            ->addColumn('companyId', 'integer')
            ->addColumn('toolkitOfferId', 'integer')
            ->addColumn('selected', 'boolean')
            ->addColumn('dateClaimed', 'datetime', ['null' => TRUE])
            ->addColumn('dtc', 'datetime')
            ->addColumn('dtm', 'datetime')
            ->create();

        $this->query(
            <<<QUERY
            INSERT INTO
              cms2_toolkit_offers (type, name, description)
            VALUES
              ("namescoDomain", "Free .co.uk website", "<b>Free .co.uk website plus more</b><br>
              Save &pound;s by getting your online presence started with our MadeSimple solution - Websites MadeSimple.<br>
              <br>
              Your free package includes:<br>
              &bullet; Free .co.uk  Website for 1 year<br>
              &bullet; Free Personalised Email Address<br>
              &bullet; Free 1-page Website<br>
              &bullet; Free Professional Stock Images"),

              ("adwordsVoucher", "£75 Google Adwords Voucher", "<b>Google AdWords Voucher</b> <br>
              &bullet; Advertise your products & services to millions of potential customers using Google's online advertising service.<br>
              &bullet; Find more customers by advertising your site with Google.<br>
              &bullet; Spend &pound;25 and get an additional &pound;75.<br>
              &bullet; Use Google's phone setup service to get started now."),

              ("googleAppsDiscount", "20% Off Google Apps", "<b>Try Google Apps free for 1 month</b> <br>
              Google Apps is a cloud-based productivity suite for your business that helps you get work done from anywhere, on any device.
              Gmail, Calendar, Drive, Docs, and so much more. What’s more - if you decide to sign up after the trial we can save you 20% per user.
              "),

              ("taxConsultation", "Free Acountancy & Tax Consultation", "<b>Free Accountancy & Tax Consultation</b> <br>
              &bullet; Find out if you're paying too much tax.<br>
              &bullet; Free review of your affairs by a qualified accountant.<br>
              &bullet; Branches nationwide for convenient appointments.<br>
              &bullet; UK customers only.
              "),

              ("freeAgentTrial", "Exclusive 2-Month Free Trial to FreeAgent\'s Online Accounting Software", "<b>Free 2-Month trial to FreeAgent</b> <br>
              &bullet; Exclusive discount for Companies Made Simple customers - you won't find this offer anywhere else.<br>
              &bullet; Plus get a 10% discount on the monthly subscription for 12-months after your free trial ends.<br>
              &bullet; Keep track of all your company's finances. <br>
              &bullet; FreeAgent is super easy-to-use, you don't need any accounting or bookkeeping experience to start using it.<br>
              "),

              ("businessStartupGuide", "Free Business Startup Guide", "<b>Free Business Startup Guide (worth &pound;7.99)</b> <br>
              &bullet; Receive an expert's guide to starting your business (ebook)<br>
              &bullet; Easy to understand and jargon-free<br>
              &bullet; Avoid the common mistakes many new businesses make<br>
              &bullet; Perfect if you've never started a business before
              "),

              ("creditChecks", "3 Free Company Credit Checks", "<b>Free Company Credit Checks (worth &pound;16.35)</b> <br>
              &bullet; Run a free credit check on your first 3 business customers to make sure you'll get paid<br>
              &bullet; Easy to use service from our sister site Company Searches Made Simple (credit checks available on UK companies only).")
QUERY
        );
    }

    public function down()
    {
        $this->dropTable('cms2_company_toolkit_offers');
        $this->dropTable('cms2_toolkit_offers');
    }
}
