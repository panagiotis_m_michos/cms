<?php

use Phinx\Migration\AbstractMigration;

class IdCheck extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $sql = <<<EOD
CREATE TABLE IF NOT EXISTS `cms2_id_checks` (
  `idCheckId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `points` smallint(6) NOT NULL,
  `documentTypeId` enum('PASSPORT','LICENSE','EUROPEAN_CARD') DEFAULT NULL,
  `response` text DEFAULT NULL,
  `customerId` int(10) unsigned NOT NULL,
  `dtc` datetime NOT NULL,
  PRIMARY KEY (`idCheckId`),
  KEY `customerId` (`customerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
EOD;
        $this->query($sql);

    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $queue = $this->table('cms2_id_checks');
        $queue->drop();
    }
}