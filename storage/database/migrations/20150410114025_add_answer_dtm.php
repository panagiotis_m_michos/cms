<?php

use Phinx\Migration\AbstractMigration;

class AddAnswerDtm extends AbstractMigration
{
    public function change()
    {
        $this->table('cms2_answers')->addColumn('dtm', 'datetime', ['null' => TRUE])->update();
    }
}
