<?php

use Phinx\Migration\AbstractMigration;

class AddReferenceToRefundedOrderItem extends AbstractMigration
{
    public function change()
    {
        $this
            ->table(TBL_ORDERS_ITEMS)
            ->addColumn('refundedOrderItemId', 'integer', ['after' => 'isRefunded', 'null' => TRUE])
            ->update();
    }
}
