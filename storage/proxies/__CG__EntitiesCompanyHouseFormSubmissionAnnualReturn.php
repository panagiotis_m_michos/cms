<?php

namespace CMS\Proxy\__CG__\Entities\CompanyHouse\FormSubmission;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class AnnualReturn extends \Entities\CompanyHouse\FormSubmission\AnnualReturn implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array();



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }

    /**
     * {@inheritDoc}
     * @param string $name
     */
    public function & __get($name)
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__get', array($name));

        return parent::__get($name);
    }

    /**
     * {@inheritDoc}
     * @param string $name
     * @param mixed  $value
     */
    public function __set($name, $value)
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__set', array($name, $value));

        return parent::__set($name, $value);
    }

    /**
     * {@inheritDoc}
     * @param  string $name
     * @return boolean
     */
    public function __isset($name)
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__isset', array($name));

        return parent::__isset($name);

    }

    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return array('__isInitialized__', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'companyCategory', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'trading', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'dtr5', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'madeUpDate', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'sicCodeType', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'sicCode', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'premise', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'street', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'thoroughfare', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'postTown', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'county', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'country', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'postcode', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'careOfName', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'poBox', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'sailPremise', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'sailStreet', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'sailThoroughfare', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'sailPostTown', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'sailCounty', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'sailCountry', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'sailPostcode', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'sailCareOfName', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'sailPoBox', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'sailRecords', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'membersList', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'regulatedMarkets', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'capitals', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'officers', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'shareholdings');
        }

        return array('__isInitialized__', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'companyCategory', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'trading', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'dtr5', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'madeUpDate', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'sicCodeType', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'sicCode', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'premise', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'street', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'thoroughfare', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'postTown', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'county', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'country', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'postcode', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'careOfName', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'poBox', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'sailPremise', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'sailStreet', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'sailThoroughfare', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'sailPostTown', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'sailCounty', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'sailCountry', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'sailPostcode', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'sailCareOfName', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'sailPoBox', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'sailRecords', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'membersList', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'regulatedMarkets', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'capitals', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'officers', '' . "\0" . 'Entities\\CompanyHouse\\FormSubmission\\AnnualReturn' . "\0" . 'shareholdings');
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (AnnualReturn $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * {@inheritDoc}
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', array());

        parent::__clone();
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', array());
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getAnnualReturnId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAnnualReturnId', array());

        return parent::getAnnualReturnId();
    }

    /**
     * {@inheritDoc}
     */
    public function addCapital(\Entities\CompanyHouse\FormSubmission\AnnualReturn\Capital $capital)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addCapital', array($capital));

        return parent::addCapital($capital);
    }

    /**
     * {@inheritDoc}
     */
    public function addOfficer(\Entities\CompanyHouse\FormSubmission\AnnualReturn\Officer $officer)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addOfficer', array($officer));

        return parent::addOfficer($officer);
    }

    /**
     * {@inheritDoc}
     */
    public function addShareholding(\Entities\CompanyHouse\FormSubmission\AnnualReturn\Shareholding $shareholding)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addShareholding', array($shareholding));

        return parent::addShareholding($shareholding);
    }

    /**
     * {@inheritDoc}
     */
    public function getCompanyCategory()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCompanyCategory', array());

        return parent::getCompanyCategory();
    }

    /**
     * {@inheritDoc}
     */
    public function setCompanyCategory($companyCategory)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCompanyCategory', array($companyCategory));

        return parent::setCompanyCategory($companyCategory);
    }

    /**
     * {@inheritDoc}
     */
    public function getTrading()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTrading', array());

        return parent::getTrading();
    }

    /**
     * {@inheritDoc}
     */
    public function setTrading($trading)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTrading', array($trading));

        return parent::setTrading($trading);
    }

    /**
     * {@inheritDoc}
     */
    public function getDtr5()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDtr5', array());

        return parent::getDtr5();
    }

    /**
     * {@inheritDoc}
     */
    public function setDtr5($dtr5)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDtr5', array($dtr5));

        return parent::setDtr5($dtr5);
    }

    /**
     * {@inheritDoc}
     */
    public function getMadeUpDate()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getMadeUpDate', array());

        return parent::getMadeUpDate();
    }

    /**
     * {@inheritDoc}
     */
    public function setMadeUpDate(\Utils\Date $madeUpDate)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setMadeUpDate', array($madeUpDate));

        return parent::setMadeUpDate($madeUpDate);
    }

    /**
     * {@inheritDoc}
     */
    public function getSicCodeType()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSicCodeType', array());

        return parent::getSicCodeType();
    }

    /**
     * {@inheritDoc}
     */
    public function setSicCodeType($sicCodeType)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSicCodeType', array($sicCodeType));

        return parent::setSicCodeType($sicCodeType);
    }

    /**
     * {@inheritDoc}
     */
    public function getSicCode()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSicCode', array());

        return parent::getSicCode();
    }

    /**
     * {@inheritDoc}
     */
    public function setSicCode($sicCode)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSicCode', array($sicCode));

        return parent::setSicCode($sicCode);
    }

    /**
     * {@inheritDoc}
     */
    public function getPremise()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPremise', array());

        return parent::getPremise();
    }

    /**
     * {@inheritDoc}
     */
    public function setPremise($premise)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPremise', array($premise));

        return parent::setPremise($premise);
    }

    /**
     * {@inheritDoc}
     */
    public function getStreet()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getStreet', array());

        return parent::getStreet();
    }

    /**
     * {@inheritDoc}
     */
    public function setStreet($street)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setStreet', array($street));

        return parent::setStreet($street);
    }

    /**
     * {@inheritDoc}
     */
    public function getThoroughfare()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getThoroughfare', array());

        return parent::getThoroughfare();
    }

    /**
     * {@inheritDoc}
     */
    public function setThoroughfare($thoroughfare)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setThoroughfare', array($thoroughfare));

        return parent::setThoroughfare($thoroughfare);
    }

    /**
     * {@inheritDoc}
     */
    public function getPostTown()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPostTown', array());

        return parent::getPostTown();
    }

    /**
     * {@inheritDoc}
     */
    public function setPostTown($postTown)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPostTown', array($postTown));

        return parent::setPostTown($postTown);
    }

    /**
     * {@inheritDoc}
     */
    public function getCounty()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCounty', array());

        return parent::getCounty();
    }

    /**
     * {@inheritDoc}
     */
    public function setCounty($county)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCounty', array($county));

        return parent::setCounty($county);
    }

    /**
     * {@inheritDoc}
     */
    public function getCountry()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCountry', array());

        return parent::getCountry();
    }

    /**
     * {@inheritDoc}
     */
    public function setCountry($country)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCountry', array($country));

        return parent::setCountry($country);
    }

    /**
     * {@inheritDoc}
     */
    public function getPostcode()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPostcode', array());

        return parent::getPostcode();
    }

    /**
     * {@inheritDoc}
     */
    public function setPostcode($postcode)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPostcode', array($postcode));

        return parent::setPostcode($postcode);
    }

    /**
     * {@inheritDoc}
     */
    public function getCareOfName()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCareOfName', array());

        return parent::getCareOfName();
    }

    /**
     * {@inheritDoc}
     */
    public function setCareOfName($careOfName)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCareOfName', array($careOfName));

        return parent::setCareOfName($careOfName);
    }

    /**
     * {@inheritDoc}
     */
    public function getPoBox()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPoBox', array());

        return parent::getPoBox();
    }

    /**
     * {@inheritDoc}
     */
    public function setPoBox($poBox)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPoBox', array($poBox));

        return parent::setPoBox($poBox);
    }

    /**
     * {@inheritDoc}
     */
    public function getSailPremise()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSailPremise', array());

        return parent::getSailPremise();
    }

    /**
     * {@inheritDoc}
     */
    public function setSailPremise($sailPremise)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSailPremise', array($sailPremise));

        return parent::setSailPremise($sailPremise);
    }

    /**
     * {@inheritDoc}
     */
    public function getSailStreet()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSailStreet', array());

        return parent::getSailStreet();
    }

    /**
     * {@inheritDoc}
     */
    public function setSailStreet($sailStreet)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSailStreet', array($sailStreet));

        return parent::setSailStreet($sailStreet);
    }

    /**
     * {@inheritDoc}
     */
    public function getSailThoroughfare()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSailThoroughfare', array());

        return parent::getSailThoroughfare();
    }

    /**
     * {@inheritDoc}
     */
    public function setSailThoroughfare($sailThoroughfare)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSailThoroughfare', array($sailThoroughfare));

        return parent::setSailThoroughfare($sailThoroughfare);
    }

    /**
     * {@inheritDoc}
     */
    public function getSailPostTown()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSailPostTown', array());

        return parent::getSailPostTown();
    }

    /**
     * {@inheritDoc}
     */
    public function setSailPostTown($sailPostTown)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSailPostTown', array($sailPostTown));

        return parent::setSailPostTown($sailPostTown);
    }

    /**
     * {@inheritDoc}
     */
    public function getSailCounty()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSailCounty', array());

        return parent::getSailCounty();
    }

    /**
     * {@inheritDoc}
     */
    public function setSailCounty($sailCounty)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSailCounty', array($sailCounty));

        return parent::setSailCounty($sailCounty);
    }

    /**
     * {@inheritDoc}
     */
    public function getSailCountry()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSailCountry', array());

        return parent::getSailCountry();
    }

    /**
     * {@inheritDoc}
     */
    public function setSailCountry($sailCountry)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSailCountry', array($sailCountry));

        return parent::setSailCountry($sailCountry);
    }

    /**
     * {@inheritDoc}
     */
    public function getSailPostcode()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSailPostcode', array());

        return parent::getSailPostcode();
    }

    /**
     * {@inheritDoc}
     */
    public function setSailPostcode($sailPostcode)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSailPostcode', array($sailPostcode));

        return parent::setSailPostcode($sailPostcode);
    }

    /**
     * {@inheritDoc}
     */
    public function getSailCareOfName()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSailCareOfName', array());

        return parent::getSailCareOfName();
    }

    /**
     * {@inheritDoc}
     */
    public function setSailCareOfName($sailCareOfName)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSailCareOfName', array($sailCareOfName));

        return parent::setSailCareOfName($sailCareOfName);
    }

    /**
     * {@inheritDoc}
     */
    public function getSailPoBox()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSailPoBox', array());

        return parent::getSailPoBox();
    }

    /**
     * {@inheritDoc}
     */
    public function setSailPoBox($sailPoBox)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSailPoBox', array($sailPoBox));

        return parent::setSailPoBox($sailPoBox);
    }

    /**
     * {@inheritDoc}
     */
    public function getSailRecords()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSailRecords', array());

        return parent::getSailRecords();
    }

    /**
     * {@inheritDoc}
     */
    public function setSailRecords($sailRecords)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSailRecords', array($sailRecords));

        return parent::setSailRecords($sailRecords);
    }

    /**
     * {@inheritDoc}
     */
    public function getMembersList()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getMembersList', array());

        return parent::getMembersList();
    }

    /**
     * {@inheritDoc}
     */
    public function setMembersList($membersList)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setMembersList', array($membersList));

        return parent::setMembersList($membersList);
    }

    /**
     * {@inheritDoc}
     */
    public function getRegulatedMarkets()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRegulatedMarkets', array());

        return parent::getRegulatedMarkets();
    }

    /**
     * {@inheritDoc}
     */
    public function setRegulatedMarkets($regulatedMarkets)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRegulatedMarkets', array($regulatedMarkets));

        return parent::setRegulatedMarkets($regulatedMarkets);
    }

    /**
     * {@inheritDoc}
     */
    public function getCapitals()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCapitals', array());

        return parent::getCapitals();
    }

    /**
     * {@inheritDoc}
     */
    public function setCapitals(\Doctrine\Common\Collections\ArrayCollection $capitals)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCapitals', array($capitals));

        return parent::setCapitals($capitals);
    }

    /**
     * {@inheritDoc}
     */
    public function getOfficers()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getOfficers', array());

        return parent::getOfficers();
    }

    /**
     * {@inheritDoc}
     */
    public function setOfficers(\Doctrine\Common\Collections\ArrayCollection $officers)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setOfficers', array($officers));

        return parent::setOfficers($officers);
    }

    /**
     * {@inheritDoc}
     */
    public function getShareholdings()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getShareholdings', array());

        return parent::getShareholdings();
    }

    /**
     * {@inheritDoc}
     */
    public function setShareholdings(\Doctrine\Common\Collections\ArrayCollection $shareholdings)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setShareholdings', array($shareholdings));

        return parent::setShareholdings($shareholdings);
    }

    /**
     * {@inheritDoc}
     */
    public function getId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', array());

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function getFormSubmissionId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getFormSubmissionId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFormSubmissionId', array());

        return parent::getFormSubmissionId();
    }

    /**
     * {@inheritDoc}
     */
    public function getFormIdentifier()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFormIdentifier', array());

        return parent::getFormIdentifier();
    }

    /**
     * {@inheritDoc}
     */
    public function setFormIdentifier($formIdentifier)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFormIdentifier', array($formIdentifier));

        return parent::setFormIdentifier($formIdentifier);
    }

    /**
     * {@inheritDoc}
     */
    public function getLanguage()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLanguage', array());

        return parent::getLanguage();
    }

    /**
     * {@inheritDoc}
     */
    public function setLanguage($language)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLanguage', array($language));

        return parent::setLanguage($language);
    }

    /**
     * {@inheritDoc}
     */
    public function getResponse()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getResponse', array());

        return parent::getResponse();
    }

    /**
     * {@inheritDoc}
     */
    public function setResponse($response)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setResponse', array($response));

        return parent::setResponse($response);
    }

    /**
     * {@inheritDoc}
     */
    public function hasResponse()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'hasResponse', array());

        return parent::hasResponse();
    }

    /**
     * {@inheritDoc}
     */
    public function getRejectReference()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRejectReference', array());

        return parent::getRejectReference();
    }

    /**
     * {@inheritDoc}
     */
    public function setRejectReference($rejectReference)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRejectReference', array($rejectReference));

        return parent::setRejectReference($rejectReference);
    }

    /**
     * {@inheritDoc}
     */
    public function getExaminerTelephone()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getExaminerTelephone', array());

        return parent::getExaminerTelephone();
    }

    /**
     * {@inheritDoc}
     */
    public function setExaminerTelephone($examinerTelephone)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setExaminerTelephone', array($examinerTelephone));

        return parent::setExaminerTelephone($examinerTelephone);
    }

    /**
     * {@inheritDoc}
     */
    public function getExaminerComment()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getExaminerComment', array());

        return parent::getExaminerComment();
    }

    /**
     * {@inheritDoc}
     */
    public function setExaminerComment($examinerComment)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setExaminerComment', array($examinerComment));

        return parent::setExaminerComment($examinerComment);
    }

    /**
     * {@inheritDoc}
     */
    public function getDateCancelled()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDateCancelled', array());

        return parent::getDateCancelled();
    }

    /**
     * {@inheritDoc}
     */
    public function setDateCancelled(\DateTime $dateCancelled)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDateCancelled', array($dateCancelled));

        return parent::setDateCancelled($dateCancelled);
    }

    /**
     * {@inheritDoc}
     */
    public function getDtc()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDtc', array());

        return parent::getDtc();
    }

    /**
     * {@inheritDoc}
     */
    public function setDtc(\DateTime $dtc)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDtc', array($dtc));

        return parent::setDtc($dtc);
    }

    /**
     * {@inheritDoc}
     */
    public function getDtm()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDtm', array());

        return parent::getDtm();
    }

    /**
     * {@inheritDoc}
     */
    public function setDtm(\DateTime $dtm)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDtm', array($dtm));

        return parent::setDtm($dtm);
    }

    /**
     * {@inheritDoc}
     */
    public function getCompany()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCompany', array());

        return parent::getCompany();
    }

    /**
     * {@inheritDoc}
     */
    public function setCompany(\Entities\Company $company)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCompany', array($company));

        return parent::setCompany($company);
    }

    /**
     * {@inheritDoc}
     */
    public function getFormSubmissionErrors()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFormSubmissionErrors', array());

        return parent::getFormSubmissionErrors();
    }

    /**
     * {@inheritDoc}
     */
    public function setFormSubmissionErrors(array $formSubmissionErrors)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFormSubmissionErrors', array($formSubmissionErrors));

        return parent::setFormSubmissionErrors($formSubmissionErrors);
    }

    /**
     * {@inheritDoc}
     */
    public function addFormSubmissionError(\Entities\CompanyHouse\FormSubmissionError $formSubmission)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addFormSubmissionError', array($formSubmission));

        return parent::addFormSubmissionError($formSubmission);
    }

    /**
     * {@inheritDoc}
     */
    public function isAccepted()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'isAccepted', array());

        return parent::isAccepted();
    }

    /**
     * {@inheritDoc}
     */
    public function isPending()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'isPending', array());

        return parent::isPending();
    }

    /**
     * {@inheritDoc}
     */
    public function isInternalFailure()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'isInternalFailure', array());

        return parent::isInternalFailure();
    }

    /**
     * {@inheritDoc}
     */
    public function isError()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'isError', array());

        return parent::isError();
    }

    /**
     * {@inheritDoc}
     */
    public function isCancelled()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'isCancelled', array());

        return parent::isCancelled();
    }

    /**
     * {@inheritDoc}
     */
    public function markAsCancelled()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'markAsCancelled', array());

        return parent::markAsCancelled();
    }

    /**
     * {@inheritDoc}
     */
    public function unsetResponse()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'unsetResponse', array());

        return parent::unsetResponse();
    }

    /**
     * {@inheritDoc}
     */
    public function setFormSubmissionType()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFormSubmissionType', array());

        return parent::setFormSubmissionType();
    }

    /**
     * {@inheritDoc}
     */
    public function __call($name, $args)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, '__call', array($name, $args));

        return parent::__call($name, $args);
    }

    /**
     * {@inheritDoc}
     */
    public function __unset($name)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, '__unset', array($name));

        return parent::__unset($name);
    }

}
