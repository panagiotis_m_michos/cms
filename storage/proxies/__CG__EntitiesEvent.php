<?php

namespace CMS\Proxy\__CG__\Entities;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Event extends \Entities\Event implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array();



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }

    /**
     * {@inheritDoc}
     * @param string $name
     */
    public function & __get($name)
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__get', array($name));

        return parent::__get($name);
    }

    /**
     * {@inheritDoc}
     * @param string $name
     * @param mixed  $value
     */
    public function __set($name, $value)
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__set', array($name, $value));

        return parent::__set($name, $value);
    }

    /**
     * {@inheritDoc}
     * @param  string $name
     * @return boolean
     */
    public function __isset($name)
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__isset', array($name));

        return parent::__isset($name);

    }

    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return array('__isInitialized__', '' . "\0" . 'Entities\\Event' . "\0" . 'eventId', '' . "\0" . 'Entities\\Event' . "\0" . 'eventKey', '' . "\0" . 'Entities\\Event' . "\0" . 'objectId', '' . "\0" . 'Entities\\Event' . "\0" . 'dtc');
        }

        return array('__isInitialized__', '' . "\0" . 'Entities\\Event' . "\0" . 'eventId', '' . "\0" . 'Entities\\Event' . "\0" . 'eventKey', '' . "\0" . 'Entities\\Event' . "\0" . 'objectId', '' . "\0" . 'Entities\\Event' . "\0" . 'dtc');
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Event $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', array());
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', array());
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', array());

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function getEventId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getEventId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getEventId', array());

        return parent::getEventId();
    }

    /**
     * {@inheritDoc}
     */
    public function getEventKey()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getEventKey', array());

        return parent::getEventKey();
    }

    /**
     * {@inheritDoc}
     */
    public function setEventKey($eventKey)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setEventKey', array($eventKey));

        return parent::setEventKey($eventKey);
    }

    /**
     * {@inheritDoc}
     */
    public function getObjectId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getObjectId', array());

        return parent::getObjectId();
    }

    /**
     * {@inheritDoc}
     */
    public function setObjectId($objectId)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setObjectId', array($objectId));

        return parent::setObjectId($objectId);
    }

    /**
     * {@inheritDoc}
     */
    public function getDtc()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDtc', array());

        return parent::getDtc();
    }

    /**
     * {@inheritDoc}
     */
    public function setDtc(\DateTime $dtc)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDtc', array($dtc));

        return parent::setDtc($dtc);
    }

    /**
     * {@inheritDoc}
     */
    public function __call($name, $args)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, '__call', array($name, $args));

        return parent::__call($name, $args);
    }

    /**
     * {@inheritDoc}
     */
    public function __unset($name)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, '__unset', array($name));

        return parent::__unset($name);
    }

}
