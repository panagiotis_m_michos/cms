<?php

namespace CMS\Proxy\__CG__\Entities;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Customer extends \Entities\Customer implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array();



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }

    /**
     * {@inheritDoc}
     * @param string $name
     */
    public function & __get($name)
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__get', array($name));

        return parent::__get($name);
    }

    /**
     * {@inheritDoc}
     * @param string $name
     * @param mixed  $value
     */
    public function __set($name, $value)
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__set', array($name, $value));

        return parent::__set($name, $value);
    }

    /**
     * {@inheritDoc}
     * @param  string $name
     * @return boolean
     */
    public function __isset($name)
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__isset', array($name));

        return parent::__isset($name);

    }

    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return array('__isInitialized__', '' . "\0" . 'Entities\\Customer' . "\0" . 'customerId', '' . "\0" . 'Entities\\Customer' . "\0" . 'statusId', '' . "\0" . 'Entities\\Customer' . "\0" . 'roleId', '' . "\0" . 'Entities\\Customer' . "\0" . 'tagId', '' . "\0" . 'Entities\\Customer' . "\0" . 'icaewId', '' . "\0" . 'Entities\\Customer' . "\0" . 'myDetailsQuestion', '' . "\0" . 'Entities\\Customer' . "\0" . 'email', '' . "\0" . 'Entities\\Customer' . "\0" . 'password', '' . "\0" . 'Entities\\Customer' . "\0" . 'titleId', '' . "\0" . 'Entities\\Customer' . "\0" . 'firstName', '' . "\0" . 'Entities\\Customer' . "\0" . 'lastName', '' . "\0" . 'Entities\\Customer' . "\0" . 'companyName', '' . "\0" . 'Entities\\Customer' . "\0" . 'address1', '' . "\0" . 'Entities\\Customer' . "\0" . 'address2', '' . "\0" . 'Entities\\Customer' . "\0" . 'address3', '' . "\0" . 'Entities\\Customer' . "\0" . 'city', '' . "\0" . 'Entities\\Customer' . "\0" . 'county', '' . "\0" . 'Entities\\Customer' . "\0" . 'postcode', '' . "\0" . 'Entities\\Customer' . "\0" . 'countryId', '' . "\0" . 'Entities\\Customer' . "\0" . 'phone', '' . "\0" . 'Entities\\Customer' . "\0" . 'additionalPhone', '' . "\0" . 'Entities\\Customer' . "\0" . 'isSubscribed', '' . "\0" . 'Entities\\Customer' . "\0" . 'howHeardId', '' . "\0" . 'Entities\\Customer' . "\0" . 'industryId', '' . "\0" . 'Entities\\Customer' . "\0" . 'credit', '' . "\0" . 'Entities\\Customer' . "\0" . 'affiliateId', '' . "\0" . 'Entities\\Customer' . "\0" . 'activeCompanies', '' . "\0" . 'Entities\\Customer' . "\0" . 'monitoredActiveCompanies', '' . "\0" . 'Entities\\Customer' . "\0" . 'cashbackType', '' . "\0" . 'Entities\\Customer' . "\0" . 'accountNumber', '' . "\0" . 'Entities\\Customer' . "\0" . 'sortCode', '' . "\0" . 'Entities\\Customer' . "\0" . 'isIdCheckRequired', '' . "\0" . 'Entities\\Customer' . "\0" . 'dtIdValidated', '' . "\0" . 'Entities\\Customer' . "\0" . 'dtc', '' . "\0" . 'Entities\\Customer' . "\0" . 'dtm', '' . "\0" . 'Entities\\Customer' . "\0" . 'cashbacks', '' . "\0" . 'Entities\\Customer' . "\0" . 'companies', '' . "\0" . 'Entities\\Customer' . "\0" . 'orders', '' . "\0" . 'Entities\\Customer' . "\0" . 'tokens', '' . "\0" . 'Entities\\Customer' . "\0" . 'temporaryPassword');
        }

        return array('__isInitialized__', '' . "\0" . 'Entities\\Customer' . "\0" . 'customerId', '' . "\0" . 'Entities\\Customer' . "\0" . 'statusId', '' . "\0" . 'Entities\\Customer' . "\0" . 'roleId', '' . "\0" . 'Entities\\Customer' . "\0" . 'tagId', '' . "\0" . 'Entities\\Customer' . "\0" . 'icaewId', '' . "\0" . 'Entities\\Customer' . "\0" . 'myDetailsQuestion', '' . "\0" . 'Entities\\Customer' . "\0" . 'email', '' . "\0" . 'Entities\\Customer' . "\0" . 'password', '' . "\0" . 'Entities\\Customer' . "\0" . 'titleId', '' . "\0" . 'Entities\\Customer' . "\0" . 'firstName', '' . "\0" . 'Entities\\Customer' . "\0" . 'lastName', '' . "\0" . 'Entities\\Customer' . "\0" . 'companyName', '' . "\0" . 'Entities\\Customer' . "\0" . 'address1', '' . "\0" . 'Entities\\Customer' . "\0" . 'address2', '' . "\0" . 'Entities\\Customer' . "\0" . 'address3', '' . "\0" . 'Entities\\Customer' . "\0" . 'city', '' . "\0" . 'Entities\\Customer' . "\0" . 'county', '' . "\0" . 'Entities\\Customer' . "\0" . 'postcode', '' . "\0" . 'Entities\\Customer' . "\0" . 'countryId', '' . "\0" . 'Entities\\Customer' . "\0" . 'phone', '' . "\0" . 'Entities\\Customer' . "\0" . 'additionalPhone', '' . "\0" . 'Entities\\Customer' . "\0" . 'isSubscribed', '' . "\0" . 'Entities\\Customer' . "\0" . 'howHeardId', '' . "\0" . 'Entities\\Customer' . "\0" . 'industryId', '' . "\0" . 'Entities\\Customer' . "\0" . 'credit', '' . "\0" . 'Entities\\Customer' . "\0" . 'affiliateId', '' . "\0" . 'Entities\\Customer' . "\0" . 'activeCompanies', '' . "\0" . 'Entities\\Customer' . "\0" . 'monitoredActiveCompanies', '' . "\0" . 'Entities\\Customer' . "\0" . 'cashbackType', '' . "\0" . 'Entities\\Customer' . "\0" . 'accountNumber', '' . "\0" . 'Entities\\Customer' . "\0" . 'sortCode', '' . "\0" . 'Entities\\Customer' . "\0" . 'isIdCheckRequired', '' . "\0" . 'Entities\\Customer' . "\0" . 'dtIdValidated', '' . "\0" . 'Entities\\Customer' . "\0" . 'dtc', '' . "\0" . 'Entities\\Customer' . "\0" . 'dtm', '' . "\0" . 'Entities\\Customer' . "\0" . 'cashbacks', '' . "\0" . 'Entities\\Customer' . "\0" . 'companies', '' . "\0" . 'Entities\\Customer' . "\0" . 'orders', '' . "\0" . 'Entities\\Customer' . "\0" . 'tokens', '' . "\0" . 'Entities\\Customer' . "\0" . 'temporaryPassword');
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Customer $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', array());
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', array());
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', array());

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function getCustomerId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getCustomerId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCustomerId', array());

        return parent::getCustomerId();
    }

    /**
     * {@inheritDoc}
     */
    public function getStatusId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getStatusId', array());

        return parent::getStatusId();
    }

    /**
     * {@inheritDoc}
     */
    public function getRoleId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRoleId', array());

        return parent::getRoleId();
    }

    /**
     * {@inheritDoc}
     */
    public function getTagId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTagId', array());

        return parent::getTagId();
    }

    /**
     * {@inheritDoc}
     */
    public function getIcaewId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getIcaewId', array());

        return parent::getIcaewId();
    }

    /**
     * {@inheritDoc}
     */
    public function getMyDetailsQuestion()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getMyDetailsQuestion', array());

        return parent::getMyDetailsQuestion();
    }

    /**
     * {@inheritDoc}
     */
    public function getEmail()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getEmail', array());

        return parent::getEmail();
    }

    /**
     * {@inheritDoc}
     */
    public function getPassword()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPassword', array());

        return parent::getPassword();
    }

    /**
     * {@inheritDoc}
     */
    public function getTitleId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTitleId', array());

        return parent::getTitleId();
    }

    /**
     * {@inheritDoc}
     */
    public function getFirstName()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFirstName', array());

        return parent::getFirstName();
    }

    /**
     * {@inheritDoc}
     */
    public function getLastName()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLastName', array());

        return parent::getLastName();
    }

    /**
     * {@inheritDoc}
     */
    public function getCompanyName()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCompanyName', array());

        return parent::getCompanyName();
    }

    /**
     * {@inheritDoc}
     */
    public function getFullName()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFullName', array());

        return parent::getFullName();
    }

    /**
     * {@inheritDoc}
     */
    public function getAddress1()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAddress1', array());

        return parent::getAddress1();
    }

    /**
     * {@inheritDoc}
     */
    public function getAddress2()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAddress2', array());

        return parent::getAddress2();
    }

    /**
     * {@inheritDoc}
     */
    public function getAddress3()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAddress3', array());

        return parent::getAddress3();
    }

    /**
     * {@inheritDoc}
     */
    public function getCity()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCity', array());

        return parent::getCity();
    }

    /**
     * {@inheritDoc}
     */
    public function getCounty()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCounty', array());

        return parent::getCounty();
    }

    /**
     * {@inheritDoc}
     */
    public function getPostcode()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPostcode', array());

        return parent::getPostcode();
    }

    /**
     * {@inheritDoc}
     */
    public function getCountryId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCountryId', array());

        return parent::getCountryId();
    }

    /**
     * {@inheritDoc}
     */
    public function getPhone()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPhone', array());

        return parent::getPhone();
    }

    /**
     * {@inheritDoc}
     */
    public function getAdditionalPhone()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAdditionalPhone', array());

        return parent::getAdditionalPhone();
    }

    /**
     * {@inheritDoc}
     */
    public function getIsSubscribed()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getIsSubscribed', array());

        return parent::getIsSubscribed();
    }

    /**
     * {@inheritDoc}
     */
    public function getHowHeardId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getHowHeardId', array());

        return parent::getHowHeardId();
    }

    /**
     * {@inheritDoc}
     */
    public function getIndustryId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getIndustryId', array());

        return parent::getIndustryId();
    }

    /**
     * {@inheritDoc}
     */
    public function getCredit()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCredit', array());

        return parent::getCredit();
    }

    /**
     * {@inheritDoc}
     */
    public function getAffiliateId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAffiliateId', array());

        return parent::getAffiliateId();
    }

    /**
     * {@inheritDoc}
     */
    public function getActiveCompanies()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getActiveCompanies', array());

        return parent::getActiveCompanies();
    }

    /**
     * {@inheritDoc}
     */
    public function getMonitoredActiveCompanies()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getMonitoredActiveCompanies', array());

        return parent::getMonitoredActiveCompanies();
    }

    /**
     * {@inheritDoc}
     */
    public function getCashbackType()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCashbackType', array());

        return parent::getCashbackType();
    }

    /**
     * {@inheritDoc}
     */
    public function getAccountNumber()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAccountNumber', array());

        return parent::getAccountNumber();
    }

    /**
     * {@inheritDoc}
     */
    public function getSortCode()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSortCode', array());

        return parent::getSortCode();
    }

    /**
     * {@inheritDoc}
     */
    public function isIsIdCheckRequired()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'isIsIdCheckRequired', array());

        return parent::isIsIdCheckRequired();
    }

    /**
     * {@inheritDoc}
     */
    public function setIsIdCheckRequired($isIdCheckRequired)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setIsIdCheckRequired', array($isIdCheckRequired));

        return parent::setIsIdCheckRequired($isIdCheckRequired);
    }

    /**
     * {@inheritDoc}
     */
    public function getDtIdValidated()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDtIdValidated', array());

        return parent::getDtIdValidated();
    }

    /**
     * {@inheritDoc}
     */
    public function setDtIdValidated(\DateTime $dtIdValidated = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDtIdValidated', array($dtIdValidated));

        return parent::setDtIdValidated($dtIdValidated);
    }

    /**
     * {@inheritDoc}
     */
    public function getDtc()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDtc', array());

        return parent::getDtc();
    }

    /**
     * {@inheritDoc}
     */
    public function getDtm()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDtm', array());

        return parent::getDtm();
    }

    /**
     * {@inheritDoc}
     */
    public function setCustomerId($customerId)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCustomerId', array($customerId));

        return parent::setCustomerId($customerId);
    }

    /**
     * {@inheritDoc}
     */
    public function setStatusId($statusId)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setStatusId', array($statusId));

        return parent::setStatusId($statusId);
    }

    /**
     * {@inheritDoc}
     */
    public function setRoleId($roleId)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRoleId', array($roleId));

        return parent::setRoleId($roleId);
    }

    /**
     * {@inheritDoc}
     */
    public function setTagId($tagId)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTagId', array($tagId));

        return parent::setTagId($tagId);
    }

    /**
     * {@inheritDoc}
     */
    public function setIcaewId($icaewId)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setIcaewId', array($icaewId));

        return parent::setIcaewId($icaewId);
    }

    /**
     * {@inheritDoc}
     */
    public function setMyDetailsQuestion($myDetailsQuestion)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setMyDetailsQuestion', array($myDetailsQuestion));

        return parent::setMyDetailsQuestion($myDetailsQuestion);
    }

    /**
     * {@inheritDoc}
     */
    public function setEmail($email)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setEmail', array($email));

        return parent::setEmail($email);
    }

    /**
     * {@inheritDoc}
     */
    public function setPassword($password)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPassword', array($password));

        return parent::setPassword($password);
    }

    /**
     * {@inheritDoc}
     */
    public function setTitleId($titleId)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTitleId', array($titleId));

        return parent::setTitleId($titleId);
    }

    /**
     * {@inheritDoc}
     */
    public function setFirstName($firstName)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFirstName', array($firstName));

        return parent::setFirstName($firstName);
    }

    /**
     * {@inheritDoc}
     */
    public function setLastName($lastName)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLastName', array($lastName));

        return parent::setLastName($lastName);
    }

    /**
     * {@inheritDoc}
     */
    public function setCompanyName($companyName)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCompanyName', array($companyName));

        return parent::setCompanyName($companyName);
    }

    /**
     * {@inheritDoc}
     */
    public function setAddress1($address1)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAddress1', array($address1));

        return parent::setAddress1($address1);
    }

    /**
     * {@inheritDoc}
     */
    public function setAddress2($address2)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAddress2', array($address2));

        return parent::setAddress2($address2);
    }

    /**
     * {@inheritDoc}
     */
    public function setAddress3($address3)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAddress3', array($address3));

        return parent::setAddress3($address3);
    }

    /**
     * {@inheritDoc}
     */
    public function setCity($city)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCity', array($city));

        return parent::setCity($city);
    }

    /**
     * {@inheritDoc}
     */
    public function setCounty($county)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCounty', array($county));

        return parent::setCounty($county);
    }

    /**
     * {@inheritDoc}
     */
    public function setPostcode($postcode)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPostcode', array($postcode));

        return parent::setPostcode($postcode);
    }

    /**
     * {@inheritDoc}
     */
    public function setCountryId($countryId)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCountryId', array($countryId));

        return parent::setCountryId($countryId);
    }

    /**
     * {@inheritDoc}
     */
    public function setPhone($phone)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPhone', array($phone));

        return parent::setPhone($phone);
    }

    /**
     * {@inheritDoc}
     */
    public function setAdditionalPhone($additionalPhone)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAdditionalPhone', array($additionalPhone));

        return parent::setAdditionalPhone($additionalPhone);
    }

    /**
     * {@inheritDoc}
     */
    public function setIsSubscribed($isSubscribed)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setIsSubscribed', array($isSubscribed));

        return parent::setIsSubscribed($isSubscribed);
    }

    /**
     * {@inheritDoc}
     */
    public function setHowHeardId($howHeardId)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setHowHeardId', array($howHeardId));

        return parent::setHowHeardId($howHeardId);
    }

    /**
     * {@inheritDoc}
     */
    public function setIndustryId($industryId)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setIndustryId', array($industryId));

        return parent::setIndustryId($industryId);
    }

    /**
     * {@inheritDoc}
     */
    public function setCredit($credit)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCredit', array($credit));

        return parent::setCredit($credit);
    }

    /**
     * {@inheritDoc}
     */
    public function setAffiliateId($affiliateId)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAffiliateId', array($affiliateId));

        return parent::setAffiliateId($affiliateId);
    }

    /**
     * {@inheritDoc}
     */
    public function setActiveCompanies($activeCompanies)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setActiveCompanies', array($activeCompanies));

        return parent::setActiveCompanies($activeCompanies);
    }

    /**
     * {@inheritDoc}
     */
    public function setMonitoredActiveCompanies($monitoredActiveCompanies)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setMonitoredActiveCompanies', array($monitoredActiveCompanies));

        return parent::setMonitoredActiveCompanies($monitoredActiveCompanies);
    }

    /**
     * {@inheritDoc}
     */
    public function setCashbackType($cashbackType)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCashbackType', array($cashbackType));

        return parent::setCashbackType($cashbackType);
    }

    /**
     * {@inheritDoc}
     */
    public function setAccountNumber($accountNumber)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAccountNumber', array($accountNumber));

        return parent::setAccountNumber($accountNumber);
    }

    /**
     * {@inheritDoc}
     */
    public function setSortCode($sortCode)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSortCode', array($sortCode));

        return parent::setSortCode($sortCode);
    }

    /**
     * {@inheritDoc}
     */
    public function setDtc(\DateTime $dtc)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDtc', array($dtc));

        return parent::setDtc($dtc);
    }

    /**
     * {@inheritDoc}
     */
    public function setDtm(\DateTime $dtm)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDtm', array($dtm));

        return parent::setDtm($dtm);
    }

    /**
     * {@inheritDoc}
     */
    public function isRetail()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'isRetail', array());

        return parent::isRetail();
    }

    /**
     * {@inheritDoc}
     */
    public function toArray()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'toArray', array());

        return parent::toArray();
    }

    /**
     * {@inheritDoc}
     */
    public function isEqual(\Entities\Customer $customer)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'isEqual', array($customer));

        return parent::isEqual($customer);
    }

    /**
     * {@inheritDoc}
     */
    public function getAddress($newLine = '
')
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAddress', array($newLine));

        return parent::getAddress($newLine);
    }

    /**
     * {@inheritDoc}
     */
    public function hasBankDetails()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'hasBankDetails', array());

        return parent::hasBankDetails();
    }

    /**
     * {@inheritDoc}
     */
    public function hasCompletedRegistration()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'hasCompletedRegistration', array());

        return parent::hasCompletedRegistration();
    }

    /**
     * {@inheritDoc}
     */
    public function hasCashbackTypeBank()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'hasCashbackTypeBank', array());

        return parent::hasCashbackTypeBank();
    }

    /**
     * {@inheritDoc}
     */
    public function isWholesale()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'isWholesale', array());

        return parent::isWholesale();
    }

    /**
     * {@inheritDoc}
     */
    public function getCompanies()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCompanies', array());

        return parent::getCompanies();
    }

    /**
     * {@inheritDoc}
     */
    public function getTokens()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTokens', array());

        return parent::getTokens();
    }

    /**
     * {@inheritDoc}
     */
    public function hasCashbackTypeCredits()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'hasCashbackTypeCredits', array());

        return parent::hasCashbackTypeCredits();
    }

    /**
     * {@inheritDoc}
     */
    public function hasCompanies()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'hasCompanies', array());

        return parent::hasCompanies();
    }

    /**
     * {@inheritDoc}
     */
    public function hasWholesaleQuestion()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'hasWholesaleQuestion', array());

        return parent::hasWholesaleQuestion();
    }

    /**
     * {@inheritDoc}
     */
    public function addCompany(\Entities\Company $company)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addCompany', array($company));

        return parent::addCompany($company);
    }

    /**
     * {@inheritDoc}
     */
    public function hasActiveToken()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'hasActiveToken', array());

        return parent::hasActiveToken();
    }

    /**
     * {@inheritDoc}
     */
    public function getActiveToken()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getActiveToken', array());

        return parent::getActiveToken();
    }

    /**
     * {@inheritDoc}
     */
    public function isNew()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'isNew', array());

        return parent::isNew();
    }

    /**
     * {@inheritDoc}
     */
    public function addCredit($credit)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addCredit', array($credit));

        return parent::addCredit($credit);
    }

    /**
     * {@inheritDoc}
     */
    public function getCountry2Letter()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCountry2Letter', array());

        return parent::getCountry2Letter();
    }

    /**
     * {@inheritDoc}
     */
    public function setCountryFromString($country)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCountryFromString', array($country));

        return parent::setCountryFromString($country);
    }

    /**
     * {@inheritDoc}
     */
    public function getCountry()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCountry', array());

        return parent::getCountry();
    }

    /**
     * {@inheritDoc}
     */
    public function markIdCheckRequired()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'markIdCheckRequired', array());

        return parent::markIdCheckRequired();
    }

    /**
     * {@inheritDoc}
     */
    public function markIdCheckNotRequired()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'markIdCheckNotRequired', array());

        return parent::markIdCheckNotRequired();
    }

    /**
     * {@inheritDoc}
     */
    public function isIdCheckRequired()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'isIdCheckRequired', array());

        return parent::isIdCheckRequired();
    }

    /**
     * {@inheritDoc}
     */
    public function isIdCheckCompleted()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'isIdCheckCompleted', array());

        return parent::isIdCheckCompleted();
    }

    /**
     * {@inheritDoc}
     */
    public function isUkCustomer()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'isUkCustomer', array());

        return parent::isUkCustomer();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrders()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getOrders', array());

        return parent::getOrders();
    }

    /**
     * {@inheritDoc}
     */
    public function getRole()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRole', array());

        return parent::getRole();
    }

    /**
     * {@inheritDoc}
     */
    public function getTitle()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTitle', array());

        return parent::getTitle();
    }

    /**
     * {@inheritDoc}
     */
    public function getPrimaryAddress()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPrimaryAddress', array());

        return parent::getPrimaryAddress();
    }

    /**
     * {@inheritDoc}
     */
    public function hasEnoughCredit($credit)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'hasEnoughCredit', array($credit));

        return parent::hasEnoughCredit($credit);
    }

    /**
     * {@inheritDoc}
     */
    public function subtractCredit($credit)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'subtractCredit', array($credit));

        return parent::subtractCredit($credit);
    }

    /**
     * {@inheritDoc}
     */
    public function hasCredit()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'hasCredit', array());

        return parent::hasCredit();
    }

    /**
     * {@inheritDoc}
     */
    public function setCountryIso($country)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCountryIso', array($country));

        return parent::setCountryIso($country);
    }

    /**
     * {@inheritDoc}
     */
    public function getCountryIso()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCountryIso', array());

        return parent::getCountryIso();
    }

    /**
     * {@inheritDoc}
     */
    public function jsonSerialize()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'jsonSerialize', array());

        return parent::jsonSerialize();
    }

    /**
     * {@inheritDoc}
     */
    public function setTemporaryPassword($password)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTemporaryPassword', array($password));

        return parent::setTemporaryPassword($password);
    }

    /**
     * {@inheritDoc}
     */
    public function getTemporaryPassword()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTemporaryPassword', array());

        return parent::getTemporaryPassword();
    }

    /**
     * {@inheritDoc}
     */
    public function __call($name, $args)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, '__call', array($name, $args));

        return parent::__call($name, $args);
    }

    /**
     * {@inheritDoc}
     */
    public function __unset($name)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, '__unset', array($name));

        return parent::__unset($name);
    }

}
