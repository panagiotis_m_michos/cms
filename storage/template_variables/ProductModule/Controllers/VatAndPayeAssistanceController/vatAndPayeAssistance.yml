seo:
    title: VAT and PAYE Registration Assistance
    description: We offer VAT and PAYE registration assistance and these services include filing at HM Revenue and Customs.
    keywords:  VAT and PAYE Registration Assistance

introductionStrip:
    title: VAT and PAYE Registration Assistance
    lead: Ensure your application is up to standard with our VAT and PAYE registration assistance.
    description|markdown: |
        Our assistance services will help you:

        - **Save time** - Up to 50% of applications to HM Revenue and Customs are rejected and delayed due to errors.

        - **Have peace of mind** - Our experienced team will ensure your application is complete and correct.
        
matrixStrip:
    blocks:
        -
            title: VAT Registration Assistance
            description|markdown: |
                We'll email you the registration form to complete. Once emailed back to us, it is thoroughly checked and approved before being submitted to HMRC. Once the application has been submitted to HMRC, it takes around 4-6 weeks to be processed. 
                
                ***IMPORTANT:*** *The VAT assistance service is for companies Limited by shares only. As a default we will process applications as Standard VAT Applications. We can process Flat Rate VAT Registrations but this will need to be requested when submitting your information.*
            price: £[price 299]
            buyButton: '[ui name="buy_button" productId="299" size="m" loading="expand-right"]'
        -
            title: PAYE Registration Assistance
            description|markdown: |
                Complete our simple online form and have it reviewed by one of our experts. We then submit the application to HMRC for you.
            price: £[price 298]
            buyButton: '[ui name="buy_button" productId="298" size="m" loading="expand-right"]'
        -
            title: VAT & PAYE Registration Assistance Bundle
            description|markdown: |
                VAT & PAYE Registration Assistance combined in a single product for a special price
            price: £[price 1674]
            buyButton: '[ui name="buy_button" productId="1674" size="m" loading="expand-right"]'
            
faqStrip:
    title: Company VAT Registration FAQs
    faqBlock:
        -               
            title: What happens if I've made a mistake on my application?
            description|markdown|allow_paragraph: |
                If your form is incomplete or contains any errors, a member from our friendly team will contact you to obtain the correct information. Only once all relevant sections of the form are accurate will it be sent to HMRC.
        -
            title: Why register for VAT?
            description|markdown|allow_paragraph: |
                If your business sells or provides taxable supplies to another business or non-business customer, you must be registered to charge Value Added Tax (VAT) when your turnover exceeds the VAT threshold of £82,000 within 12 months.

                If you are not VAT registered, you are not able to reclaim any VAT charges that you pay when you purchase goods or services from another VAT registered business.
        -
            title: When completing the form, which address should I use as my business address?
            description|markdown|allow_paragraph: |
                The business address refers to your actual trading address and cannot be our address (20-22 Wenlock Road, N1), even if you currently have a registered office service with us.
        -
            title: How does the VAT application process work?
            description|markdown|allow_paragraph: |
                We'll email you the application form which you will need to complete and e-mail back to us. Once we have checked over your form, we will create a Government Gateway account and will submit your application online. We will then e-mail you HMRC's Acknowledgement Reference Number as confirmation of submission. It takes around 4-6 weeks for HMRC to process. Once your application has been successful, you will receive a VAT registration certificate with your VAT registration number.
        -
            title: Can all companies use the VAT assistance service?
            description|markdown|allow_paragraph: |
                The VAT assistance service is for companies Limited (Ltd) by shares. Unfortunately, we cannot complete the VAT registration for other types of companies such as LLPs.
        -
            title: Can non-UK residents register for VAT?
            description|markdown|allow_paragraph: |
                Yes you can. If you have a foreign Tax ID you must supply 3 items of documentary evidence as proof of identity. These 3 items must be:

                - 1 copy of government issued photo identification, such as a passport, photo driver’s licence or national identity card, and

                - 2 copies of correspondence which includes your name and address, such as a bank / credit card statement or a recent utility bill.
        -
            title: Can you register my company for the VAT Flat Rate Scheme?
            description|markdown|allow_paragraph: |
                We can register your company for this scheme if eligible. Check here to see if your company meets the criteria. If eligible, please place an order for the VAT registration assistance service above. We will then email you the application form to complete. Please clearly indicate that you would like to be registered for the flat rate scheme when you email back your completed application. Otherwise, your company will be registered for the standard rate by default.
        -
            title: What is the VAT Flat Rate Scheme?
            description|markdown|allow_paragraph: |
                The VAT flat rate scheme is an alternative way for small businesses to work out how much VAT to pay to HMRC each quarter. With this scheme you pay a fixed rate of VAT to HMRC of your VAT inclusive turnover. The actual percentage you use depends on your type of business. Check here for the percentage.

                You keep the difference between what you charge your customers and pay to HMRC but you can’t reclaim the VAT on your purchases - except for certain capital assets over £2000. To join the scheme your VAT turnover must be £150,000 or less (excluding VAT).
        -
            title: Why register for PAYE?
            description|markdown|allow_paragraph: |
                If you intend to hire employees, including yourself, you will need to register as an employer with HMRC. The system used by HMRC is referred to as 'Pay As You Earn' (PAYE) and collects Income Tax and National Insurance Contributions (NICs) from eligible employees.
        -
            title: What information is needed from me to register for PAYE?
            description|markdown|allow_paragraph: |
                When filling out the online form, you will need to provide your contact details as well as the following information:

                - Business trading address
                - Description of what the company does
                - The number of employees (can be just the director)
                - When the first pay day will be
                - Whether the pay will come to more than £390 per month
                - If they are sub-contractors
                - The director's name and National Insurance number
        -
            title: How does the PAYE application process work?
            description|markdown|allow_paragraph: |
                Once you have completed our online form and it has been approved by us, we will submit an application to HMRC on your behalf. Then, once you've been successfully registered as an employer by HMRC, you'll receive a letter in the post with your PAYE reference as well as a link for a guide to help get you started with reporting payroll.