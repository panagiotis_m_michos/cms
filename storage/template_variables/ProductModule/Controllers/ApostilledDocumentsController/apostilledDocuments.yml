seo:
    title: Apostilled Documents - Company Formation Services
    description: Incorporation documents certified by the Foreign and Commonwealth Office.
    keywords: Apostilled Documents

introductionStrip:
    title: Apostilled Documents
    lead: Get your documents certified by the Foreign Commonwealth Office (FCO).
    imgFeature: /front/imgs/img1104.jpg
    imgFeatureDescription: Apostilled Documents
    description|markdown: |
        What documents are signed?

        - A copy of the Certificate of Incorporation
        - Memorandum & Articles of Association
        - A list of the current company officers
        
matrixStrip:
    blocks:
        -
            title: Apostilled Documents (Normal service)
            description|markdown: |
                Normal service generally takes 10-14 working days to come back from the FCO. We send and receive the documents by Royal Mail Recorded Delivery.
            price: £[price 115]
            buyButton: '[ui name="buy_button" productId="115" size="m" loading="expand-right"]'
        -
            title: Apostilled Documents (Express service)
            description|markdown: |
                The express service generally takes 3-5 working days to come back from the FCO.
            price: £[price 287]
            buyButton: '[ui name="buy_button" productId="287" size="m" loading="expand-right"]'
            
faqStrip:
    title: Apostilled Documents FAQs
    faqBlock:
        -
            title: What does 'apostille' mean?
            description|markdown|allow_paragraph: |
                The word 'apostille' is a French word which means 'certification'. It is commonly used in English to refer to the legalization of an official document.
        -
            title: Why would I get my documents certified?
            description|markdown|allow_paragraph: |
                Under the 1961 Hague Convention, documents which have been notarized by a solicitor and then certified with a conformant apostille are accepted for legal and international use in all the nations that have signed the Hague Convention.
        -
            title: What kind of documents can be apostilled?
            description|markdown|allow_paragraph: |
                Any document from Companies House or our systems, for example:

                - Certificate of Incorporation
                - Memorandum & Articles of Association
                - List of Directors/Shareholders
                - Share certificate
                - Minutes
        -
            title: Need additional documents apostilled?
            description|markdown|allow_paragraph: |
                If you wish additional documents to be included in the apostille other than the standard list of documents, please advise by email to [cosec@madesimplegroup.com](mailto:cosec@madesimplegroup.com). If you have signed documents you wish to include in the apostille, these documents must be supplied.
        -
            title: How many documents can I get certified at any one time?
            description|markdown|allow_paragraph: |
                There is no limit on the amount of documents that can be certified at any one time- however the binder for certification can only hold 10 pages so any amount of documents in that 10 page allocation. Anything over that will require a second apostille service.
        -
            title: Can you send my completed documents to someone else?
            description|markdown|allow_paragraph: |
                Yes. We can send your document to an individual, bank or business. Simply give us your instructions in writing and we will send the documents to that address and also confirm that your request has been carried out.
        -
            title: How does the service work?
            description|markdown|allow_paragraph: |
                You will place your order online and we will send you an email confirming your order and requesting the country/countries the apostille will be used in. We will ask you to confirm your delivery address. At this point, you can inform us of where you would like the documents sent. Once we have this information, the documents will be produced and we will get them certified, apostilled and dispatched to you by special delivery. If you wish us to send the documents to you by courier service, we can also arrange this for an extra charge.
        -
            title: Will an apostilled document be accepted by any country?
            description|markdown|allow_paragraph: |
                There are currently 105 parties of the 1961 Hague Convention Abolishing the Requirement for Legalisation for Foreign Public Documents who will recognise an apostille certificate. For countries who are not members, we are unable to advise whether an apostille certificate will be accepted.