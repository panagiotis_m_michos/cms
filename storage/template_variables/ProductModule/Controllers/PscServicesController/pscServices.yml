seo:
    title: Register of People with Significant Control (PSC) 
    description: Register of People with Significant Control (PSC) 
    keywords: Register of People with Significant Control (PSC) 

introductionStrip:
    title: Register of People with Significant Control (PSC) 
    lead: PSC services to help you stay compliant
    description|markdown: |
        All UK companies must maintain and store a **Register of people with significant control (PSC)** in the company. This PSC information should then be included in the company’s annual **Confirmation Statement** (which replaced the Annual Return from 30 June 2016).
        
        ***Important:*** *Failure to keep a PSC register is a criminal offence punishable by a fine and/or up to two years' imprisonment.*
        
matrixStrip:
    blocks:
        -
            title: PSC Online Register
            description|markdown: |
                Complete and store your company’s PSC information today with our quick and simple online admin system. Once complete we’ll store your company’s PSC information and the history of any changes made in our online register.*
            price: £[price 1705]
            buyButton: '[ui name="buy_button" productId="1705" size="m" loading="expand-right"]'
        -
            title: PSC Online Register & Confirmation Statement
            description|markdown: |
                Complete and store your company’s PSC information and then give this information to Companies House (the UK’s registrar of companies) with the Confirmation Statement. Confirmation Statement filing fee included. File as many Confirmation Statements in a 12 month period as you wish.* 
            price: £[price 1706]
            buyButton: '[ui name="buy_button" productId="1706" size="m" loading="expand-right"]'
#        -
#            title: PSC Assistance
#            description|markdown: |
#                Need help completing your PSC information? Not sure what information should be provided?  We can help you fill out your PSC Register on our online admin system, we’ll then send the information to Companies House as part of the Confirmation Statement. Confirmation Statement filing fee included.
#            price: £[price 1707]
#            buyButton: '[ui name="buy_button" productId="1707" size="m" loading="expand-right"]'

featuresColLeft:
    features:
        -
            title: Who is a person with significant control?
            description|markdown: |
                As set out by the <a href="https://www.gov.uk/government/news/the-small-business-enterprise-and-employment-bill-is-coming" target="_blank">Government <i class="fa fa-external-link" aria-hidden="true"></i></a>, this is anyone who:
                    
                - owns more than 25% of the company’s shares
                - holds more than 25% of the company’s voting rights
                - holds the right to appoint or remove the majority of directors
                - has the right to, or actually exercises significant influence or control
                - holds the right to exercise or actually exercises significant control over a trust or company that meets one of the first 4 conditions.

                For the overwhelming majority of people, your **person/people with significant control** will simply be your company shareholders. If you’re unsure of who your company’s PSCs are, call us on 0207 608 5500 and we’ll be able to advise you.
featuresColRight:
    features:
        -
            title: How the PSC Register and the Confirmation Statement (formerly known as the Annual Return) work together
            description|markdown: |
                1. You keep and maintain a PSC Register made up of your PSC information
                2. Once a year you send this information to Companies House as part of the Confirmation Statement
                3. If any company information (including PSC information) changes, you can file an unlimited amount of extra Confirmation Statements for free within the year

faqStrip:
    title: Register of People with Significant Control FAQs
    faqBlock:
        -
            title: Why has the PSC Register been introduced?
            description|markdown|allow_paragraph: |
                To increase transparency in UK companies and combat issues such as money laundering. With the help of the Register, the UK Government can see who really owns / runs a company. It has been introduced as part of the Small Business, Enterprise and Employment Act 2015.
        -
            title: When does a company need to start keep a PSC Register?
            description|markdown|allow_paragraph: |
                Companies should have been keeping a register since 6 April 2016. But don’t worry, our services can get your company compliant quickly.
        -
            title: What information do I need to keep on the PSC Register?
            description|markdown|allow_paragraph: |
                For each PSC you will need to record the:
                - Full Name 
                - Date of birth
                - Service address 
                - Residential address (not on the public register)
                - Country of residence 
                - Nationality 
                - Date the person became registrable
                - Nature of their control
        -
            title: What is the Confirmation Statement?
            description|markdown|allow_paragraph: |
                As of 30 June 2016 the Confirmation Statement has replaced the Annual Return. The Confirmation Statement includes a section dedicated to PSC, other than this, it includes the same information as the Annual Return. Your Confirmation Statement due date will take your current Annual Return due date.
        -
            title: "*What is the renewal cost?"
            description|markdown|allow_paragraph: |
                The yearly renewal cost for the **PSC Online Register is £[price 1705]**. The yearly renewal cost for the **PSC Online Register & Confirmation Statement is £[price 1706]**.
#                Finally the **PSC Assistance** renews to the **PSC Online Register & Confirmation Statement Service** with an annual cost of **£[price 1717]**
