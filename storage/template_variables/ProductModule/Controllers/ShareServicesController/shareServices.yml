seo:
    title: Share Services for Company Formations
    description: Confirmation of Shareholding at Incorporation, Issue of Shares, Share Certificate, Transfer of Shares.
    keywords:  Share Services

introductionStrip:
    title: Share Services
    lead: Transfer or issue new shares and obtain official share documents.
    description|markdown: |
        This service is for UK Companies Ltd by shares.
        
matrixStrip:
    blocks:
        -
            title: Transfer of Shares
            description|markdown: |
                This service is for customers who want to transfer share/s from one shareholder to another and require the transfer to be documented on paper. Completed documents will include the J30 stock transfer form, meeting minutes and a blank share certificate. These documents act as a receipt for your new shareholder.
                *Maximum of 2 transfers per order. If applicable, stamp duty fee is not included.*

                ***IMPORTANT:*** *Details of this transfer are not automatically updated online at Companies House. Any changes to the shareholding will only reflect on the Companies House register once an Confirmation Statement has been filed.*
            price: £[price 315]
            buyButton: '[ui name="buy_button" productId="315" size="m" loading="expand-right"]'
        -
            title: Issue of Shares
            description|markdown: |
                This service is for customers who want to issue new shares in the company. This includes submitting a return of allotment to Companies House to apply for new shares, as well as, preparation of meeting minutes, board resolution and a blank share certificate.
                *Maximum of 2 shareholders per order.*

                ***IMPORTANT:*** *Details of this transfer are not automatically updated online at Companies House. Any changes to the shareholding will only reflect on the Companies House register once an Confirmation Statement has been filed.*
            price: £[price 313]
            buyButton: '[ui name="buy_button" productId="313" size="m" loading="expand-right"]'
        -
            title: Share Certificate
            description|markdown: |
                Validate who owns shares in the company. The certificate can act as an invoice/receipt for shares allocated to shareholders.
                *1 certificate per shareholder. Max 5 certificates per order.*
            price: £[price 314]
            buyButton: '[ui name="buy_button" productId="314" size="m" loading="expand-right"]'
        -
            title: Confirmation of Shareholding at Incorporation
            description|markdown: |
                A letterheaded document stating the original shareholding at the time of incorporation. Generally accepted by banks for the opening of accounts.
                *Please allow 2-3 working days for delivery.*
            price: £[price 311]
            buyButton: '[ui name="buy_button" productId="311" size="m" loading="expand-right"]'
            
faqStrip:
    title: Share Services FAQs
    faqBlock:
        -               
            title: What is stamp duty?
            description|markdown|allow_paragraph: |
                For shares over £1,000, the J30 stock transfer form is required to be stamped by HM Revenue and Customs (HMRC). The payment for this is known as stamp duty and is currently 0.5% of the share amount, rounded to the nearest £5.

                For example, to transfer shares valued at £1,800, 0.5 x 1800 = £9.00 so a £10 stamp duty fee is payable.
        -               
            title: How do I pay the stamp duty?
            description|markdown|allow_paragraph: |
                For all share transfers valued over £1,000, we recommend speaking with a solicitor for further information.
        -               
            title: What class of shares can be issued?
            description|markdown|allow_paragraph: |
                Currently, shares of the same class/value as the company's initial shares can be issued. We do not get involved in creating or issuing new classes of shares.
        -               
            title: How many shares can be issued at any one time?
            description|markdown|allow_paragraph: |
                There is no maximum on the amount of shares that can be issued.
        -               
            title: What information do I need to provide to transfer shares?
            description|markdown|allow_paragraph: |
                To process a share transfer we require:

                - The name and address of the person who is transferring the shares
                - The name and address of the person who is receiving the shares
                - The number of shares being transferred