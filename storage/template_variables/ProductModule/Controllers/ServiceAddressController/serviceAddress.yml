@imports:
  - proof_of_id.yml

seo:
    title: Service Address
    description: This service is for any officer wishing to keep their residential address confidential.
    keywords: Service Address, Director's Service Address

introductionStrip:
    title: Service Address
    lead: Keep your residential address private by using our London N1 office as your service address
    imgFeature: /front/imgs/service-address.gif
    imgFeatureDescription: Service Address
    description|markdown: |
        This service is for directors, company secretaries and shareholders who want to keep their residential address confidential and off the Companies House public register.
        
        **You get:**
        - Use of our London N1 address as your service address
        - Official Mail (HMRC, Companies House) sent to your service address and forwarded to you for one year – with no charge for postage
        
matrixStrip:
    blocks:
        -
            title: Service Address - 1 Year
            description|markdown: |
                Buy this service if you're a company director, secretary or shareholder and want to keep your residential address private.
            price: £[price 475]
            buyButton: '[ui name="buy_button" productId="475" size="m" loading="expand-right"]'
                
featuresStrip:
    features:
        -
            title: Forming a limited company?
            description|markdown|allow_paragraph: |
                Our **Privacy company formation package** includes a company limited by shares, a prestigious registered office address for 1 year and a service address for 1 year all for just £[price 1315] plus VAT. [More information](/company-formation-name-search.html).

faqStrip:
    title: Service Address FAQs
    faqBlock:
        -
            title: What is Company Formation MadeSimple’s Service Address service?
            description|markdown|allow_paragraph: |
                Company Formation MadeSimple’s Service Address service lets company directors, secretaries and shareholders use our 20-22 Wenlock Road, London, N1 7GU address as their service address.
        -
            title: What is a Service Address?
            description|markdown|allow_paragraph: |
                When a company is incorporated, a director must include their residential address and a service address. The service address will be on the public record at Companies House; the residential address will be protected information and is not available to the public (although it is available to some public authorities). Shareholders and secretaries only need to supply one address each - a service address.
        -
            title: Why would I want to use the Service Address service?
            description|markdown|allow_paragraph: |
                Many directors, company secretaries and shareholders only have one address - their residential address - so this is the address they use as their service address at Companies House. This means that their residential address is shown on the public record at Companies House. By using Company Formation MadeSimple’s Service Address service, directors, company secretaries and shareholders can keep their residential address confidential as our London N1 address will be shown instead of their residential address.
        -
            title: What will happen to my mail?
            description|markdown|allow_paragraph: |
                This service provides the forwarding of all official government mail addressed to the director/s. This includes mail from governing bodies such as Companies House, HM Revenue and Custom, Government Gateway etc. If you wish to receive standard business mail, our company <a href="https://www.londonpresence.com/mail-forwarding/" target="_blank">Virtual Office MadeSimple</a> can provide this service from as little as £15 a month.
        -
            title: I have multiple directors/secretaries/shareholders in my company. Can I use the service address for all of them?
            description|markdown|allow_paragraph: |
                Yes. The service can be spread across multiple officers for no extra cost.
        -
            title: What is the renewal cost?
            description|markdown|allow_paragraph: |
                The service address can be renewed at a cost of £49.99 plus VAT per year. Alternatively, you may benefit from our Privacy Package Renewal which includes the renewal of both the service address for 1 year and renewal of the registered office for 1 year, for just £59.99 plus VAT. [More information on all renewal fees]([page 82]).