seo:
    title: Dormant Company Accounts (DCA)
    description: Dormant Company Accounts explained. Company Formation MadeSimple also offer a service which includes filing dormant company accounts.
    keywords:  Dormant Company Accounts (DCA)

introductionStrip:
    title: Dormant Company Accounts (DCA)
    lead: File your Dormant Company Accounts here. Simple.
    imgFeature: /front/imgs/img457.jpg
    imgFeatureDescription: Dormant Company Accounts (DCA)
    description|markdown: |
        Dormant company accounts are annual accounts that can only be filed by companies that are exempt from audit (ie. a dormant company).

        **Important:** Accounts are not to be confused with the Confirmation Statement (previously known as Annual Returns). A Confirmation Statement is a snapshot of general information about a company (not financial information). Both must be filed with Companies House on an annual basis. If you are unsure which is due please contact us and we can check for you.
        
matrixStrip:
    blocks:
        -
            title: Dormant Company Accounts
            description|markdown: |
                For companies Limited by Shares and Limited by Guarantee. Includes preparation and filing of the Dormant Company Accounts to Companies House.
            price: £[price 306]
            buyButton: '[ui name="buy_button" productId="306" size="m" loading="expand-right"]'
        -
            title: Dormant Company Accounts Express Service
            description|markdown: |
                Express service for companies Limited by Shares and Limited by Guarantee. Includes preparation and filing within 24 hours of purchase to Companies House (excluding weekends and Bank Holidays).
            price: £[price 882]
            buyButton: '[ui name="buy_button" productId="882" size="m" loading="expand-right"]'
            
faqStrip:
    title: Dormant Company Accounts FAQs
    faqBlock:
        -                            
            title: What are Dormant Company Accounts?
            description|markdown|allow_paragraph: |
                Dormant company accounts are annual accounts that can only be filed by companies that are exempt from audit (ie. a dormant company).
        -
            title: What is a Dormant Company?
            description|markdown|allow_paragraph: |
                A limited company is deemed to be dormant if it’s had no 'significant accounting transactions' since incorporation, or since the last filing of accounts. In layman’s terms, a company is dormant if it has not traded.
        -
            title: How often must Dormant Company Accounts be filed?
            description|markdown|allow_paragraph: |
                Company accounts must be filed for every financial year. If filing your first accounts they must be filed within 21 months of the date of incorporation. Every subsequent filing of accounts must be done within 9 months from the accounting reference date (ARD). If you are unsure of your ARD you can login to your account to view it.
        -
            title: What details do Company Formation MadeSimple need to file my Dormant Company Accounts?
            description|markdown|allow_paragraph: |
                None. The only possible issue might be if the shares in the company are paid or unpaid:

                - Paid - The shareholders have paid the company for their shares
                - Unpaid - The shareholders have not paid the company for their shares

                By default we will mark shares as paid unless otherwise advised within 24 hours of purchase. For an explanation as to why this is, please see the next question.
        -
            title: Why do we mark shares as paid by default?
            description|markdown|allow_paragraph: |
                The reason we mark shares as paid by default is that an unpaid share capital may result in a tax liability. It means that the shareholders owe money to the company, which HM Revenue & Customs can interpret as the company lending the shareholders the money. As these shareholders are participators in the company, HM Revenue & Customs could demand that a S419 tax charge is paid.
        -
            title: How long will it take to process my dormant company accounts?
            description|markdown|allow_paragraph: |
                The processing time for the Standard Service is 48-72 hours (excluding weekends and Bank Holidays). By default we will mark shares as paid unless advised otherwise within 24 hours of purchase.

                For the Express Service, your accounts will be processed within 24 hours (excluding weekends and Bank Holidays). Due to the nature of this service, we will mark shares as paid unless advised otherwise within 3 hours of purchase.

                We strongly advise that customers purchase their dormant accounts service at least 15 days before the due date of filing. **We cannot be held responsible for any late penalties or fines incurred as a result of purchasing the service within 15 days of the due date.**
        -
            title: Can I purchase the Dormant Company Accounts service if I did not form my company through Company Formation MadeSimple?
            description|markdown|allow_paragraph: |
                Yes, we just need a few details from you. We need: the company name, company number, webfiling authentication code, share capital and the name of one director. We also need to know if the shares in the company are paid or unpaid (see above).

                You can not use this service if:

                - The company has ever traded, even in a previous financial year
                - The company has filed a Return of allotment of shares document (SH01) with Companies House
                - You do not have a valid Companies House webfiling authentication code (we will normally have the code if the company was formed by us)
                - Important notice regarding late filing penalties:

                If a company files accounts after the due date, Companies House will impose a penalty:

                Lateness:

                - Not more than one month £150
                - More than one month but not more than three months £375
                - More than three months but not more than six months £750
                - More than six months £1500

                If the accounts are overdue we will double check that you are happy for us to file despite the penalty.

                If company accounts are overdue the only ways to avoid the penalty are:

                - [Dissolve](/company-dissolution.html) the company instead of filing accounts
                - File the accounts and then appeal against the penalty (appeals may not be successful)