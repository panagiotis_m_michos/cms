seo:
    title: Registered Office - Service Address - Offer
    description: 
    keywords: 
    index: false

introductionStrip:
    title|markdown: |
        Impress your customers  
        and contacts with a professional 
        **London Registered Office Address** or **Service Address** for just **£[price 1679]**, or...  
    titleSecondary|markdown: |
        Use both for only **£[price 1681]**
    buyButton:
        text: Buy Now
        size: m
        actionUrl: '[page 130]'
        productId: 1681
        loading: expand-left
        
matrixStrip:
    blocks:
        -
            title|markdown: |
                Combine our **Registered Office & Service Address** for
            price: only £[price 1681]
            oldPrice: normally £59.99
            fom|markdown: "[Find out more](#RO-FAQS)"
            buyButton:
                text: Buy Combo
                size: m
                actionUrl: '[page 130]'
                productId: 1681
                loading: expand-left
        -
            title|markdown: |
                Get a prestigious **Registered Office** in N1 London for
            price: £[price 1679]
            oldPrice: normally £49.99
            fom|markdown: "[Find out more](#RO-FAQS)"
            buyButton:
                text: Buy Registered Office
                size: m
                actionUrl: '[page 130]'
                productId: 1679
                loading: expand-left
        -
            title|markdown: |
                Use our **Service Address** to protect your directors for
            price: £[price 1680]
            oldPrice: normally £49.99
            fom|markdown: "[Find out more](#SA-FAQS)" 
            buyButton:
                text: Buy Service Address
                size: m
                actionUrl: '[page 130]'
                productId: 1680
                loading: expand-left
    
featuresStrip:
    title: Why use our Registered Office and/or Service Address?
    features:
        -
            title: Privacy
            description: Protect your residential address from the public register*.
        -
            title: Prestige
            description: Having a London address as your company’s/director’s official address conveys professionalism.
        -
            title: Junk Mail Filter
            description: You only get your company’s/director’s official government mail.
        -
            title: Rental Contract Restriction
            description: Many rental contracts don’t allow the rented properties to be used as a registered office/service address.

faqStrip:
    faqs:
        -
            blockTitle: Important information
            faqBlocks:
                -
                    title: "The address used for our Registered Office and Service Address is:"
                    description: 20-22 Wenlock Road, London, N1 7GU
                -
                    title: Government mail forwarded to you free of charge
                    description: With the Registered Office and/or Service Address we’ll forward on all official government mail that arrives at our offices on to you free of charge, wherever you are based (UK and overseas).
                -
                    title: Proof of ID
                    description: We have a legal obligation to check proof of ID and proof of address for all customers who use our address services. This is to ensure we comply with Anti-Money Laundering (AML) regulations and Know Your Customer (KYC) requirements. <a href="http://support.companiesmadesimple.com/article/255-do-i-need-to-provide-proof-of-id-documents" target="_blank">Please see the FAQs for more information</a>.
                -
                    title: Combo package renewal cost
                    description: The renewal cost is £59.99 plus VAT per year. <a href="[page 82]" target="_blank">More information on renewal fees</a>.
        -
            blockTitle: Registered Office FAQs
            blockId: RO-FAQS
            faqBlocks:
                -
                    title: What is Company Formation MadeSimple’s Registered Office service?
                    description|markdown: | 
                        Company Formation MadeSimple provides a London based registered office for limited companies at: 20-22 Wenlock Road, London, N1 7GU.

                        Using this service your company will be registered at our address and we will forward on all your company’s official government mail. The service is renewable annually and a reminder will be sent to you via email.
                -
                    title: What is a Registered Office?
                    description: A registered office is the official address of a limited company. It must be a physical location in the United Kingdom at which official documents can be served. The registered office address need not be the company’s place of business and is often the address of the persons providing company services, such as Company Formation MadeSimple. The address is on the public register, available to everyone.
                -
                    title: Can this service be used for company officers' (directors and secretaries) or shareholders' addresses?
                    description: No, not unless you use our Service Address service as well.
                -
                    title: What is the renewal cost?
                    description: The renewal cost is £49.99 plus VAT per year. You may benefit from our Privacy Package Renewal which includes the renewal of both the Registered Office service for 1 year and renewal of the Service Address for 1 year, for just £59.99 plus VAT. <a href="[page 82]" target="_blank">More information on renewal fees</a>.
                -
                    title: What is official government mail?
                    description: Official government mail includes mail from UK governing bodies such as Companies House, HMRC, Government Gateway and the Intellectual Property Office. If you are interested in a mail forwarding service where all your items of business mail get forwarded to you, our company <a href="https://www.londonpresence.com/mail-forwarding/" target="_blank">Virtual Office MadeSimple</a> can provide this service from as little as £19.99 a month.
        -
            blockTitle: Service Address FAQs
            blockId: SA-FAQS
            faqBlocks:
                -
                    title: What is Company Formation MadeSimple’s Service Address service?
                    description: Company Formation MadeSimple’s Service Address service allows company directors, secretaries and shareholders to use our 20-22 Wenlock Road, London, N1 7GU address as their service address.
                -
                    title: What is a Service Address?
                    description: When a director is appointed, they must provide their residential address and a service address. The service address will be on the public record at Companies House; the residential address will be protected information and is not available to the public (although it is available to some public authorities). Shareholders and secretaries only need to supply one address each - a service address.
                -
                    title: What will happen to my mail?
                    description: This service provides the forwarding of all official government mail addressed to the director/s. This includes mail from governing bodies such as Companies House, HM Revenue and Custom, Government Gateway etc. If you wish to receive standard business mail, our company <a href="https://www.londonpresence.com/mail-forwarding/" target="_blank">Virtual Office MadeSimple</a> can provide this service from as little as £19.99 a month.
                -
                    title: I have multiple directors/secretaries/shareholders in my company. Can I use the service address for all of them?
                    description: Yes. The service can be spread across multiple officers for no extra cost.
                -
                    title: What is the renewal cost?
                    description: The service address can be renewed at a cost of £49.99 plus VAT per year. Alternatively, you may benefit from our Privacy Package Renewal which includes the renewal of both the service address for 1 year and renewal of the registered office for 1 year, for just £59.99 plus VAT. <a href="[page 82]" target="_blank">More information on renewal fees</a>.
            footNote: "*Once an address has appeared on the public register it can’t be removed. However, using our Registered Office and/or Service Address means any previously used addresses are harder to find."
