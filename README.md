# companiesmadesimple.com #

## Description ##

- **Server IP**: 192.168.100.170
- **MySQL server IP**: 192.168.100.35

For credentials and database name use *passpack*. Production and development/testing databases are on the same machine
and differ in username and database name.

## Dependencies ##

 - php >= 5.6
 - web server
 - mysql    
 - mssql

### PHP extension packages (centos) ###
```
yum install -y php php-pdo php-mssql php-mysql php-mbstring
```

### Apache modules ###
```
mod_rewrite mod_ssl
```

## How to get it running ##

1. clone repository
2. make sure `temp/`, `logs/` and `www/webtemp/` are writable for the web server
3. create a new database and import sql from `storage/database/cms_dev.sql`
4. copy `project/config/app/examples/config.local.yml` to `project/config/app/config.local.yml`
5. modify `config.local.yml` according to your current database
7. run database migration using `php console/db migrate`

### Development note ###
For setting up development environment follow these steps:

1. create symlink www/index_dev.php pointing to project/bootstrap/examples/index_dev.php or copy the file directly into www/. If so, make sure to modify DOCUMENT_ROOT constant in this file properly.
2. copy (or create symlink)`project/bootstrap/examples/console_dev.php` to `console/`
3. put `RewriteRule .* - [E=DEV_INDEX_SUFFIX:_dev]` in apache *vhost* file
4. set up some kind of [mailcatcher](http://developers.madesimplegroup.com/?p=1127) (maildev, mailcatcher) application to prevent from spamming
5. set sync password in `project/config/app/config.local.yml` from *passpack*:

```
    console:
      sync:
        database:
          password: 
```
and run synchronization with production using `php console/cm sync:nodes -s` ([VPN must be connected](http://developers.madesimplegroup.com/?p=1133))