<?php

namespace Tests\VoServiceModule\Commands;

use Cron\INotifier;
use GuzzleHttp\Client;
use ILogger;
use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;
use Psr\Log\LoggerInterface;
use VoServiceModule\Services\VoServiceQueueService;
use tests\helpers\MockHelper;
use Tests\Helpers\ObjectHelper;
use VoServiceModule\Commands\VoServiceCommand;

class VoServiceCommandTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var LoggerInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $loggerMock;

    /**
     * @var INotifier|PHPUnit_Framework_MockObject_MockObject
     */
    private $notifierMock;

    /**
     * @var VoServiceQueueService|PHPUnit_Framework_MockObject_MockObject
     */
    private $voServiceQueueServiceMock;

    /**
     * @var Client|PHPUnit_Framework_MockObject_MockObject
     */
    private $clientMock;

    /**
     * @var VoServiceCommand
     */
    private $object;

    protected function setUp()
    {
        $this->loggerMock = MockHelper::mock($this, 'Psr\Log\LoggerInterface');
        $this->notifierMock = $this->getMockBuilder('Cron\INotifier')->disableOriginalConstructor()->getMock();
        $this->voServiceQueueServiceMock = $this->getMockBuilder('VoServiceModule\Services\VoServiceQueueService')->disableOriginalConstructor()->getMock();
        $this->clientMock = $this->getMockBuilder('GuzzleHttp\Client')->disableOriginalConstructor()->getMock();

        $this->object = new VoServiceCommand($this->loggerMock, $this->notifierMock, $this->voServiceQueueServiceMock, $this->clientMock);
    }

    /**
     * @test
     * @covers \Cron\Commands\Queue\VoService::execute
     */
    public function execute()
    {
        $queueItem = ObjectHelper::createVoServiceQueue();

        $this->voServiceQueueServiceMock->expects($this->once())->method('getServicesToSend')->willReturn([[$queueItem]]);
        $this->voServiceQueueServiceMock->expects($this->any())->method('canBeSend')->with($queueItem)->willReturn(TRUE);
        $this->clientMock->expects($this->once())->method('post');
        $this->loggerMock->expects($this->once())->method('debug');

        $this->voServiceQueueServiceMock->expects($this->once())->method('remove')->with($queueItem);

        $this->object->execute();
    }

    /**
     * @test
     * @covers \Cron\Commands\Queue\VoService::execute
     */
    public function executeClientException()
    {
        $exceptionMessage = 'Client exception error';
        $queueItem = ObjectHelper::createVoServiceQueue();

        $this->voServiceQueueServiceMock->expects($this->once())->method('getServicesToSend')->willReturn([[$queueItem]]);
        $this->voServiceQueueServiceMock->expects($this->once())->method('canBeSend')->with($queueItem)->willReturn(TRUE);

        $clientExceptionMock = $this->getMockBuilder('GuzzleHttp\Exception\ClientException')->disableOriginalConstructor()->getMock();

        $responseMock = $this->getMockBuilder('Guzzle\Http\Message\Response')->disableOriginalConstructor()->getMock();

        $responseMock->expects($this->once())->method('json')->willReturn(['error' => $exceptionMessage]);

        $clientExceptionMock->expects($this->once())->method('getResponse')->willReturn($responseMock);
        $this->clientMock->expects($this->once())->method('post')->willThrowException($clientExceptionMock);

        $this->loggerMock->expects($this->once())->method('error');

        $this->voServiceQueueServiceMock->expects($this->never())->method('remove');

        $this->object->execute();
    }

    /**
     * @test
     * @covers \Cron\Commands\Queue\VoService::execute
     */
    public function executeServerException()
    {
        $exceptionMessage = 'Server exception error';
        $queueItem = ObjectHelper::createVoServiceQueue();

        $this->voServiceQueueServiceMock->expects($this->once())->method('getServicesToSend')->willReturn([[$queueItem]]);
        $this->voServiceQueueServiceMock->expects($this->once())->method('canBeSend')->with($queueItem)->willReturn(TRUE);

        $serverExceptionMock = $this->getMockBuilder('GuzzleHttp\Exception\ServerException')->disableOriginalConstructor()->getMock();

        $responseMock = $this->getMockBuilder('Guzzle\Http\Message\Response')->disableOriginalConstructor()->getMock();

        $responseMock->expects($this->once())->method('json')->willReturn(['error' => $exceptionMessage]);

        $serverExceptionMock->expects($this->once())->method('getResponse')->willReturn($responseMock);
        $this->clientMock->expects($this->once())->method('post')->willThrowException($serverExceptionMock);

        $this->loggerMock->expects($this->once())->method('error');

        $this->voServiceQueueServiceMock->expects($this->never())->method('remove');

        $this->object->execute();
    }

    /**
     * @test
     * @covers \Cron\Commands\Queue\VoService::execute
     */
    public function executeException()
    {
        $queueItem = ObjectHelper::createVoServiceQueue();

        $this->voServiceQueueServiceMock->expects($this->once())->method('getServicesToSend')->willReturn([[$queueItem]]);
        $this->voServiceQueueServiceMock->expects($this->once())->method('canBeSend')->with($queueItem)->willReturn(TRUE);
        $exceptionMock = $this->getMockBuilder('Exception')->disableOriginalConstructor()->getMock();
        $this->clientMock->expects($this->once())->method('post')->willThrowException($exceptionMock);

        $this->loggerMock->expects($this->once())->method('error');

        $this->voServiceQueueServiceMock->expects($this->never())->method('remove');

        $this->object->execute();
    }

    /**
     * @test
     * @covers \Cron\Commands\Queue\VoService::execute
     */
    public function executeSkipItemsNotEligibleToSend()
    {
        $queueItem = ObjectHelper::createVoServiceQueue();

        $this->voServiceQueueServiceMock->expects($this->once())->method('getServicesToSend')->willReturn([[$queueItem]]);
        $this->voServiceQueueServiceMock->expects($this->once())->method('canBeSend')->with($queueItem)->willReturn(FALSE);

        $this->clientMock->expects($this->never())->method('post');
        $this->voServiceQueueServiceMock->expects($this->never())->method('remove');

        $this->object->execute();
    }
}
