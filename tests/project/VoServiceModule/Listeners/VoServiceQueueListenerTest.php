<?php

namespace Tests\VoServiceModule\Listeners;

use Dispatcher\Events\OrderEvent;
use EntityHelper;
use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;
use VoServiceModule\Facades\VoServiceFacade;
use VoServiceModule\Listeners\VoServiceQueueListener;

class VoServiceQueueListenerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var VoServiceFacade|PHPUnit_Framework_MockObject_MockObject
     */
    private $voServiceFacadeMock;

    /**
     * @var VoServiceQueueListener
     */
    private $object;

    protected function setUp()
    {
        EntityHelper::emptyTables(array(TBL_CUSTOMERS, TBL_ORDERS));

        $this->voServiceFacadeMock = $this->getMockBuilder('VoServiceModule\Facades\VoServiceFacade')->disableOriginalConstructor()->getMock();
        $this->object = new VoServiceQueueListener($this->voServiceFacadeMock);
    }

    /**
     * @covers Dispatcher\Listeners\VoServiceQueueListener::onOrderCompleted
     */
    public function testOnOrderCompleted()
    {
        $customer = EntityHelper::createCustomer('jaredt@madesimplegroup.com');
        $order = EntityHelper::createOrder($customer, 2);

        $this->voServiceFacadeMock->expects($this->once())
            ->method('save')
            ->with($order);

        $this->object->onOrderCompleted(new OrderEvent($order));
    }
}
