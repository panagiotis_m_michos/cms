<?php

namespace Tests\VoServiceModule\Services;

use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;
use VoServiceModule\Repositories\VoServiceQueueRepository;
use tests\helpers\MockHelper;
use Tests\Helpers\ObjectHelper;
use VoServiceModule\Services\VoServiceQueueService;

class VoServiceQueueServiceTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var VoServiceQueueService
     */
    private $object;

    /**
     * @var VoServiceQueueRepository|PHPUnit_Framework_MockObject_MockObject
     */
    private $voServiceQueueRepositoryMock;

    protected function setUp()
    {
        $this->voServiceQueueRepositoryMock = MockHelper::mock($this, 'VoServiceModule\Repositories\VoServiceQueueRepository');
        $this->object = new VoServiceQueueService($this->voServiceQueueRepositoryMock);
    }

    /**
     * @covers \Services\VoServiceQueueService::canBeSend
     */
    public function testCanBeSend()
    {
        $customer = ObjectHelper::createCustomer();
        $company = ObjectHelper::createCompany($customer);
        $queueItem = ObjectHelper::createVoServiceQueue($customer, $company);

        $customer->setFirstName(NULL);
        $company->setCompanyNumber(NULL);

        $this->assertFalse($this->object->canBeSend($queueItem));

        $customer->setFirstName('Test');
        $company->setCompanyNumber(NULL);

        $this->assertFalse($this->object->canBeSend($queueItem));

        $customer->setFirstName(NULL);
        $company->setCompanyNumber('12345678');

        $this->assertFalse($this->object->canBeSend($queueItem));

        $customer->setFirstName('Test');
        $company->setCompanyNumber('12345678');

        $this->assertTrue($this->object->canBeSend($queueItem));
    }
}
