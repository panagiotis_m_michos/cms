<?php

namespace Tests\VoServiceModule\Facades;

use Exceptions\Business\NodeException;
use Package;
use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;
use Services\CompanyService;
use Services\NodeService;
use VoServiceModule\Facades\VoServiceFacade;
use VoServiceModule\Services\VoServiceQueueService;
use tests\helpers\MockHelper;
use Tests\Helpers\ObjectHelper;
use VirtualOfficeControler;

class VoServiceFacadeTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var VoServiceFacade
     */
    private $object;

    /**
     * @var VoServiceQueueService|PHPUnit_Framework_MockObject_MockObject
     */
    private $voServiceQueueServiceMock;

    /**
     * @var NodeService|PHPUnit_Framework_MockObject_MockObject
     */
    private $nodeServiceMock;

    /**
     * @var CompanyService|PHPUnit_Framework_MockObject_MockObject
     */
    private $companyServiceMock;

    protected function setUp()
    {
        $this->voServiceQueueServiceMock = $this->getMockBuilder('VoServiceModule\Services\VoServiceQueueService')->disableOriginalConstructor()->getMock();
        $this->nodeServiceMock = $this->getMockBuilder('Services\NodeService')->disableOriginalConstructor()->getMock();
        $this->companyServiceMock = MockHelper::mock($this, 'Services\CompanyService');
        $this->object = new VoServiceFacade($this->voServiceQueueServiceMock, $this->nodeServiceMock, $this->companyServiceMock);
    }

    /**
     * @covers Services\Facades\VoServiceFacade::save
     */
    public function testSaveFormationPackage()
    {
        $customer = ObjectHelper::createCustomer();
        $company = ObjectHelper::createCompany($customer);
        $order = ObjectHelper::createOrder($customer, 0);

        $orderItem = ObjectHelper::createOrderItem($order, 20, 1, 1);
        $order->addItem($orderItem);

        $productMock = $this->getMockBuilder('BasketProduct')->disableOriginalConstructor()->getMock();
        $this->nodeServiceMock->expects($this->once())->method('getProductById')->willReturn($productMock);
        $productMock->expects($this->once())->method('isVoServiceEligible')->willReturn(TRUE);
        $this->companyServiceMock->expects($this->any())->method('getCompanyByOrder')->with($order)->willReturn($company);

        $this->voServiceQueueServiceMock->expects($this->once())->method('save');
        $this->object->save($order);
    }

    /**
     * @covers Services\Facades\VoServiceFacade::save
     */
    public function testSaveProduct()
    {
        $customer = ObjectHelper::createCustomer();
        $company = ObjectHelper::createCompany($customer);
        $order = ObjectHelper::createOrder($customer, 0);

        $orderItem = ObjectHelper::createOrderItem($order, 20, 1, 1);
        $orderItem->setCompany($company);
        $order->addItem($orderItem);

        $productMock = $this->getMockBuilder('BasketProduct')->disableOriginalConstructor()->getMock();
        $this->nodeServiceMock->expects($this->once())->method('getProductById')->willReturn($productMock);
        $productMock->expects($this->once())->method('isVoServiceEligible')->willReturn(TRUE);
        $this->companyServiceMock->expects($this->any())->method('getCompanyByOrder')->with($order)->willReturn(NULL);

        $this->voServiceQueueServiceMock->expects($this->once())->method('save');
        $this->object->save($order);
    }

    /**
     * @covers Services\Facades\VoServiceFacade::save
     * @expectedException \Exceptions\Business\NodeException
     */
    public function testNoProduct()
    {
        $customer = ObjectHelper::createCustomer();
        $order = ObjectHelper::createOrder($customer, 0);

        $orderItem = ObjectHelper::createOrderItem($order, 20, 1, 1);
        $order->addItem($orderItem);

        $this->nodeServiceMock->expects($this->exactly(1))->method('getProductById')->willThrowException(NodeException::nodeIsNotAProduct(1));

        $this->voServiceQueueServiceMock->expects($this->never())->method('save');
        $this->object->save($order);
    }

    /**
     * @covers Services\Facades\VoServiceFacade::save
     */
    public function testSaveNoEligibleProducts()
    {
        $customer = ObjectHelper::createCustomer();
        $order = ObjectHelper::createOrder($customer, 0);

        $orderItem = ObjectHelper::createOrderItem($order, 20, 1, 1);
        $order->addItem($orderItem);

        $productMock = $this->getMockBuilder('BasketProduct')->disableOriginalConstructor()->getMock();
        $this->nodeServiceMock->expects($this->once())->method('getProductById')->willReturn($productMock);
        $productMock->expects($this->once())->method('isVoServiceEligible')->willReturn(FALSE);

        $this->voServiceQueueServiceMock->expects($this->never())->method('save');
        $this->object->save($order);
    }
}
