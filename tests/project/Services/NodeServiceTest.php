<?php

namespace Services;

use BasketProduct;
use FNode;
use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;
use Repositories\Nodes\NodeRepository;
use Repositories\Nodes\ProductRepository;

class NodeServiceTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var NodeService
     */
    private $object;

    /**
     * @var NodeRepository|PHPUnit_Framework_MockObject_MockObject
     */
    private $nodeRepositoryMock;

    /**
     * @var ProductRepository|PHPUnit_Framework_MockObject_MockObject
     */
    private $productRepositoryMock;

    protected function setUp()
    {
        $this->nodeRepositoryMock = $this->getMockBuilder('Repositories\Nodes\NodeRepository')->disableOriginalConstructor()->getMock();
        $this->productRepositoryMock = $this->getMockBuilder('Repositories\Nodes\ProductRepository')->disableOriginalConstructor()->getMock();

        $this->object = new NodeService($this->nodeRepositoryMock, $this->productRepositoryMock);
    }

    /****************************************** getNodeById() ******************************************/

    /**
     * @covers Services\NodeService::getNodeById()
     */
    public function testGetNodeById()
    {
        $nodeId = 1;
        $this->nodeRepositoryMock->expects($this->once())->method('getNodeById')->willReturn(new FNode($nodeId));

        $node = $this->object->getNodeById($nodeId);

        $this->assertInstanceOf('FNode', $node);
        $this->assertEquals($nodeId, $node->getId());
    }

    /**
     * @covers Services\NodeService::getNodeById()
     */
    public function testGetNodeByIdDoesNotExist()
    {
        $nodeId = 1;
        $this->nodeRepositoryMock->expects($this->once())->method('getNodeById')->willReturn(NULL);
        $node = $this->object->getNodeById($nodeId);
        $this->assertNull($node);
    }

    /**
     * @covers Services\NodeService::getNodeById()
     * @expectedException \Exceptions\Business\NodeException
     */
    public function testGetNodeByIdDoesNotExistNeeded()
    {
        $nodeId = 1;
        $this->nodeRepositoryMock->expects($this->once())->method('getNodeById')->willReturn(NULL);
        $node = $this->object->getNodeById($nodeId, TRUE);
        $this->assertNull($node);
    }


    /****************************************** getProductById ******************************************/

    /**
     * @covers Services\NodeService::getProductById()
     */
    public function testGetProductById()
    {
        $nodeId = 1;
        $this->productRepositoryMock->expects($this->once())->method('getProductById')->with($nodeId)->willReturn(new BasketProduct($nodeId));

        $product = $this->object->getProductById($nodeId);
        $this->assertInstanceOf('BasketProduct', $product);
    }

    /**
     * @covers Services\NodeService::getProductById()
     */
    public function testGetProductByIdDoesNotExist()
    {
        $nodeId = 1;
        $this->productRepositoryMock->expects($this->once())->method('getProductById')->with($nodeId)->willReturn(NULL);

        $product = $this->object->getProductById($nodeId);
        $this->assertNull($product);
    }

    /**
     * @covers Services\NodeService::getProductById()
     * @expectedException \Exceptions\Business\NodeException
     */
    public function testGetProductByIdNotAProductException()
    {
        $nodeId = 1;
        $this->productRepositoryMock->expects($this->once())->method('getProductById')->with($nodeId)->willReturn(NULL);

        $product = $this->object->getProductById($nodeId, TRUE);
        $this->assertNull($product);
    }


    /****************************************** getProductsIdsWithIdCheckRequired ******************************************/

    /**
     * @test
     * @covers Services\NodeService::getProductsIdsWithIdCheckRequired()
     * @return array
     */
    public function getProductsIdsWithIdCheckRequired()
    {
        $expectedIds = [1, 2, 3];
        $this->productRepositoryMock->expects($this->once())->method('getProductsIdsWithIdCheckRequired')->willReturn($expectedIds);
        $ids = $this->object->getProductsIdsWithIdCheckRequired();
        $this->assertSame($expectedIds, $ids);
    }
}
