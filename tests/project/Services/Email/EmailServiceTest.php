<?php

class EmailServiceTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var EmailService
     */
    private $object;

    /**
     * @var EmailLogService|PHPUnit_Framework_MockObject_MockObject
     */
    private $emailLogServiceMock;

    /**
     * @var IMailFactory|PHPUnit_Framework_MockObject_MockObject
     */
    private $mailFactory;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        TestHelper::clearTables(array('cms2_customers'));
        $this->emailLogServiceMock = $this->getMock('EmailLogService');
        $this->mailFactory = $this->getMock('IMailFactory');
        $this->object = new EmailService($this->emailLogServiceMock, $this->mailFactory);
    }

    /**
     * @covers EmailService::send
     */
    public function testSendSuccess()
    {
        $mailMock = $this->getMock('Mail');

        $this->emailLogServiceMock->expects($this->once())->method('logEmail');
        $this->mailFactory->expects($this->once())->method('create')->willReturn($mailMock);
        $mailMock->expects($this->once())->method('send');

        $email = TestHelper::createEmail();
        $customer = TestHelper::createCustomer();
        $this->object->send($email, $customer);
    }

    /**
     * @covers EmailService::send
     * @expectedException Exceptions\Technical\EmailException 
     */
    public function testSendBadTo()
    {
        $mailMock = $this->getMock('Mail', ['send']);

        $this->mailFactory->expects($this->once())->method('create')->willReturn($mailMock);

        $email = TestHelper::createEmail();
        $email->addTo('bal');
        $customer = TestHelper::createCustomer();
        $this->object->send($email, $customer);
    }
}
