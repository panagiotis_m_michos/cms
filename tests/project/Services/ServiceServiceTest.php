<?php

namespace Services;

use Entities\Company;
use Entities\Order;
use Entities\OrderItem;
use EntityHelper;
use DateTime;
use Entities\Service;
use DiLocator;
use PHPUnit_Framework_TestCase;
use EntityNotFound;
use Entities\Customer as CustomerEntity;
use Utils\Date;

class ServiceServiceTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var ServiceService
     */
    protected $object;

    /**
     * @var CustomerEntity
     */
    protected $customer;

    /**
     * @var Company
     */
    private $company;

    /**
     * @var Company
     */
    private $company2;

    /**
     * @var Order
     */
    private $order;

    /**
     * @var OrderItem
     */
    private $orderItem;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        EntityHelper::emptyTables(array(TBL_CUSTOMERS, TBL_COMPANY, TBL_ORDERS, TBL_ORDERS_ITEMS, TBL_SERVICES, TBL_FORM_SUBMISSIONS));
        $this->customer = EntityHelper::createCustomer();
        $this->company = EntityHelper::createCompany($this->customer);
        $this->company2 = EntityHelper::createCompany($this->customer);
        $this->order = EntityHelper::createOrder($this->customer);
        $this->orderItem = $this->order->getItems()->first();
        $this->object = EntityHelper::getService(DiLocator::SERVICE_SERVICE);
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {

    }

    /**
     * @covers Services\ServiceService::getLastService
     */
    public function testLastServiceEmptyServices()
    {
        // ask for last one
        $service = $this->object->getLastService($this->company, Service::TYPE_ANNUAL_RETURN);
        $this->assertEmpty($service);
    }

    /**
     * @covers Services\ServiceService::getLastService
     */
    public function testLastServiceOneSameService()
    {
        // add to db
        $service = EntityHelper::createService($this->company, $this->orderItem, new DateTime('now'), new DateTime('+1 year'), Service::TYPE_REGISTERED_OFFICE);
        // ask for last one
        $lastService = $this->object->getLastService($this->company, Service::TYPE_REGISTERED_OFFICE);
        $this->assertEquals($service->id, $lastService->id);
    }

    /**
     * @covers Services\ServiceService::getLastService
     */
    public function testLastServiceOneDifferentService()
    {
        // add to db
        $service = EntityHelper::createService($this->company, $this->orderItem, new DateTime('now'), new DateTime('+1 year'), Service::TYPE_REGISTERED_OFFICE);

        // ask for last one
        $lastService = $this->object->getLastService($this->company, Service::TYPE_ANNUAL_RETURN);
        $this->assertEmpty($lastService);
    }

    /**
     * @covers Services\ServiceService::getLastService
     */
    public function testLastServiceTwoDifferentServices()
    {
        // add 2 to db
        $service = EntityHelper::createService($this->company, $this->orderItem, new DateTime('now'), new DateTime('+1 year'), Service::TYPE_REGISTERED_OFFICE);
        EntityHelper::createService($this->company, $this->orderItem, new DateTime('+1 year'), new DateTime('+2 year'), Service::TYPE_ANNUAL_RETURN);

        // ask for last one
        $lastService = $this->object->getLastService($this->company, Service::TYPE_REGISTERED_OFFICE);
        $this->assertEquals($service->id, $lastService->id);
    }

    /**
     * @covers Services\ServiceService::getLastService
     */
    public function testLastServiceTwoSameServices()
    {
        // add 2 to db
        EntityHelper::createService($this->company, $this->orderItem, new DateTime('now'), new DateTime('+1 year'), Service::TYPE_REGISTERED_OFFICE);
        $service2 = EntityHelper::createService($this->company, $this->orderItem, new DateTime('+1 year'), new DateTime('+2 year'), Service::TYPE_REGISTERED_OFFICE);

        // ask for last one
        $lastService = $this->object->getLastService($this->company, Service::TYPE_REGISTERED_OFFICE);
        $this->assertEquals($service2->id, $lastService->id);
    }

    /**
     * @covers Services\ServiceService::getLastService
     */
    public function testLastServiceDiffCompany()
    {
        $service1 = EntityHelper::createService($this->company, $this->orderItem, new DateTime('now'), new DateTime('+1 year'), Service::TYPE_REGISTERED_OFFICE);
        $service2 = EntityHelper::createService($this->company2, $this->orderItem, new DateTime('now'), new DateTime('+1 year'), Service::TYPE_REGISTERED_OFFICE);

        // ask for last one
        $lastService = $this->object->getLastService($this->company, Service::TYPE_REGISTERED_OFFICE);
        $this->assertEquals($service1->id, $lastService->id);

        $lastService = $this->object->getLastService($this->company2, Service::TYPE_REGISTERED_OFFICE);
        $this->assertEquals($service2->id, $lastService->id);
    }

    /**
     * @covers Services\ServiceService::getLastService
     */
    public function testLastServiceDiffCompanyAndType()
    {
        $service1 = EntityHelper::createService($this->company2, $this->orderItem, new DateTime('now'), new DateTime('+1 year'), Service::TYPE_REGISTERED_OFFICE);
        $service2 = EntityHelper::createService($this->company2, $this->orderItem, new DateTime('now'), new DateTime('+1 year'), Service::TYPE_CERTIFICATE_OF_GOOD_STANDING);

        // ask for last one
        $lastService = $this->object->getLastService($this->company, Service::TYPE_REGISTERED_OFFICE);
        $this->assertEmpty($lastService);

        $lastService = $this->object->getLastService($this->company, Service::TYPE_CERTIFICATE_OF_GOOD_STANDING);
        $this->assertEmpty($lastService);

        $lastService = $this->object->getLastService($this->company2, Service::TYPE_REGISTERED_OFFICE);
        $this->assertEquals($service1->id, $lastService->id);

        $lastService = $this->object->getLastService($this->company2, Service::TYPE_CERTIFICATE_OF_GOOD_STANDING);
        $this->assertEquals($service2->id, $lastService->id);
    }

    /**
     * @covers Services\ServiceService::getLastService
     */
    public function testLastServiceNotTheSameService()
    {
        $service1 = EntityHelper::createService($this->company2, $this->orderItem, new DateTime('now'), new DateTime('+1 year'), Service::TYPE_REGISTERED_OFFICE);
        $service2 = EntityHelper::createService($this->company2, $this->orderItem, new DateTime('+1 year'), new DateTime('+2 year'), Service::TYPE_REGISTERED_OFFICE);

        $lastService = $this->object->getLastService($this->company2, Service::TYPE_REGISTERED_OFFICE);
        $this->assertEquals($service2->id, $lastService->id);
        $lastService = $this->object->getLastService($this->company2, Service::TYPE_REGISTERED_OFFICE, $service2);
        $this->assertEquals($service1->id, $lastService->id);
    }

    /**
     * @covers Services\ServiceService::getCustomerService
     */
    public function testGetCustomerService()
    {
        $service = EntityHelper::createService($this->company, $this->orderItem, new DateTime('now'), new DateTime('+1 year'), Service::TYPE_REGISTERED_OFFICE);
        $this->assertSame($service, $this->object->getCustomerService($this->customer, $service->id));
    }

    /**
     * @covers Services\ServiceService::getCustomerService
     * @expectedException EntityNotFound
     */
    public function testGetCustomerServiceNonExistingService()
    {
        $customerStub = $this->getMockBuilder('Entities\Customer')->disableOriginalConstructor()->getMock();
        $this->object->getCustomerService($customerStub, 1);
    }

    /**
     * @covers Services\ServiceService::getCustomerService
     * @expectedException EntityNotFound
     */
    public function testGetCustomerServiceDifferentCustomer()
    {
        $service = EntityHelper::createService($this->company, $this->orderItem, new DateTime('now'), new DateTime('+1 year'), Service::TYPE_REGISTERED_OFFICE);
        $customer2 = EntityHelper::createCustomer('customer@email.com');
        $this->object->getCustomerService($customer2, $service->id);
    }

    /**
     * @covers Services\ServiceService::getServicesDataExpiringWithinDatesForEvent
     */
    public function testCanReturnServicesDataExpiringWithingDatesForEvent()
    {
        $serviceRepositoryMock = $this->getMockBuilder('Repositories\ServiceRepository')->disableOriginalConstructor()->getMock();
        $iterableResultMock = $this->getMockBuilder('Doctrine\ORM\Internal\Hydration\IterableResult')->disableOriginalConstructor()->getMock();

        $dateFrom = new Date();
        $dateTo = new Date('+10 days');
        $event = 'events.testing_events';

        $serviceRepositoryMock->expects($this->once())->method('getServicesDataExpiringWithinDatesForEvent')->with(
            $dateFrom,
            $dateTo,
            $event
        )->willReturn($iterableResultMock);

        $serviceService = new ServiceService($serviceRepositoryMock);
        $actualResult = $serviceService->getServicesDataExpiringWithinDatesForEvent($dateFrom, $dateTo, $event);

        $this->assertSame($iterableResultMock, $actualResult);
    }

    /****************************************** getOrderItemService ******************************************/

    /**
     * @test
     * @covers Services\ServiceService::getOrderItemService
     */
    public function getOrderItemServiceOneService()
    {
        $orderItem = EntityHelper::createOrderItem($this->order, 20, 1, 165);
        $service = EntityHelper::createService($this->company, $orderItem);

        $itemService = $this->object->getOrderItemService($orderItem);
        $this->assertTrue($itemService->isEqual($service));
    }

    /**
     * @test
     * @covers Services\ServiceService::getOrderItemService
     */
    public function getOrderItemServiceTwoServicesMatchedByCompany()
    {
        $service1 = EntityHelper::createService($this->company, $this->orderItem);
        $orderItem1 = EntityHelper::createOrderItem($this->order, 20, 1, 165);
        $orderItem1->setCompany($this->company);

        $service2 = EntityHelper::createService($this->company2, $this->orderItem);
        $orderItem2 = EntityHelper::createOrderItem($this->order, 20, 1, 165);
        $orderItem2->setCompany($this->company2);

        $itemService1 = $this->object->getOrderItemService($orderItem1);
        $this->assertTrue($itemService1->isEqual($service1));

        $itemService2 = $this->object->getOrderItemService($orderItem2);
        $this->assertTrue($itemService2->isEqual($service2));
    }

    /**
     * @test
     * @covers Services\ServiceService::getOrderItemService
     * @expectedException \Exceptions\Technical\OrderRefundException
     */
    public function getOrderItemServiceTwoServicesNotMatched()
    {
        EntityHelper::createService($this->company, $this->orderItem);
        EntityHelper::createService($this->company, $this->orderItem);
        $orderItem1 = EntityHelper::createOrderItem($this->order, 20, 1, 165);

        $this->object->getOrderItemService($orderItem1);
    }
}
