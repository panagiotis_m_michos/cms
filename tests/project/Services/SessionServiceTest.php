<?php

namespace Services;

use Entities\Customer;
use TestModule\Annotations\Inject;
use TestModule\Helpers\DatabaseHelper;
use TestModule\PhpUnit\TestCase;
use UserModule\Domain\Credentials;

class SessionServiceTest extends TestCase
{
    /**
     * @var SessionService
     */
    private $object;

    /**
     * @Inject({"services.session", "test_module.helpers.database_helper"})
     * @param SessionService $service
     * @param DatabaseHelper $databaseHelper
     */
    protected function setupWithContainer(SessionService $service, DatabaseHelper $databaseHelper)
    {
        $databaseHelper->emptyTables([TBL_CUSTOMERS]);
        $customer = new Customer('existing', 'other');
        $customer->setStatusId(Customer::STATUS_VALIDATED);
        $databaseHelper->saveEntity($customer);
        $this->object = $service;
    }

    /**
     * @covers Services\SessionService::useCredentials
     * @expectedException UserModule\Exceptions\CustomerAvailabilityException
     */
    public function testUsernameIsTaken()
    {
        $credentials = Credentials::withUsername('existing');
        $this->object->useCredentials($credentials);
    }

    /**
     * @covers Services\SessionService::useCredentials
     */
    public function testUsernameIsAvailable()
    {
        $credentials = Credentials::withUsername('non-existing');
        $customer = $this->object->useCredentials($credentials);
        $this->assertEquals('non-existing', $customer->getEmail());
    }

    /**
     * @covers Services\SessionService::useCredentials
     */
    public function testLogin()
    {
        $credentials = Credentials::withLogin('existing', 'other');
        $customer = $this->object->useCredentials($credentials);
        $this->assertEquals('existing', $customer->getEmail());
    }

    /**
     * @covers Services\SessionService::useCredentials
     */
    public function testInvalidUsernameLogin()
    {
        $credentials = Credentials::withLogin('non-existing', 'other');
        $customer = $this->object->useCredentials($credentials);
        $this->assertEquals('non-existing', $customer->getEmail());
    }

    /**
     * @covers Services\SessionService::useCredentials
     * @expectedException UserModule\Exceptions\LoginException
     */
    public function testInvalidPasswordLogin()
    {
        $credentials = Credentials::withLogin('existing', 'ot');
        $this->object->useCredentials($credentials);
    }
}
