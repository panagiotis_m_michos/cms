<?php

namespace Services;

use Entities\Cashback;
use EntityHelper;
use LogicException;
use PHPUnit_Framework_TestCase;
use DiLocator;
use Faker\Factory as FakerFactory;
use Faker\Generator as FakerGenerator;

class CashbackServiceTest extends PHPUnit_Framework_TestCase
{

    /**
     * @var CashbackService
     */
    protected $cashbackService;

    /**
     * @var FakerGenerator
     */
    private $faker;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        EntityHelper::emptyTables(
            array('cms2_customers', 'ch_company', 'cms2_services', 'ch_form_submission', 'cms2_cashback')
        );
        $this->cashbackService = EntityHelper::getService(DiLocator::SERVICE_CASHBACK);
        $this->faker = FakerFactory::create();
    }

    /**
     * @covers Services\CashbackService::markCashbacksAsPaid
     * @expectedException LogicException
     */
    public function testMarkOneCashbackAsPaidFailedWithNotEligibleCashback()
    {
        /** @var Cashback[] $cashbacks */
        $cashbacks = array(
            EntityHelper::createRetailCashback(Cashback::STATUS_IMPORTED),
        );

        $this->cashbackService->markCashbacksAsPaid($cashbacks);
    }

    /**
     * @covers Services\CashbackService::markCashbacksAsPaid
     * @expectedException LogicException
     */
    public function testMarkMultipleCashbacksAsPaidFailedWithNotEligibleCashback()
    {
        /** @var Cashback[] $cashbacks */
        $cashbacks = array(
            EntityHelper::createRetailCashback(Cashback::STATUS_ELIGIBLE),
            EntityHelper::createRetailCashback(Cashback::STATUS_PAID),
        );

        $this->cashbackService->markCashbacksAsPaid($cashbacks);
    }

    /**
     * @covers Services\CashbackService::markCashbacksAsPaid
     */
    public function testMarkOneCashbacksAsPaid()
    {
        /** @var Cashback[] $cashbacks */
        $cashbacks = array(
            EntityHelper::createRetailCashback(Cashback::STATUS_ELIGIBLE),
        );

        $this->cashbackService->markCashbacksAsPaid($cashbacks);

        foreach ($cashbacks as $cashback) {
            $this->assertEquals($cashback->getStatusId(), Cashback::STATUS_PAID);
        }
    }

    /**
     * @covers Services\CashbackService::markCashbacksAsPaid
     */
    public function testMarkMultipleCashbacksAsPaid()
    {
        /** @var Cashback[] $cashbacks */
        $cashbacks = array(
            EntityHelper::createRetailCashback(Cashback::STATUS_ELIGIBLE),
            EntityHelper::createRetailCashback(Cashback::STATUS_ELIGIBLE),
            EntityHelper::createRetailCashback(Cashback::STATUS_ELIGIBLE),
        );

        $this->cashbackService->markCashbacksAsPaid($cashbacks);

        foreach ($cashbacks as $cashback) {
            $this->assertEquals($cashback->getStatusId(), Cashback::STATUS_PAID);
        }
    }

    /**
     * @covers Services\CashbackService::getCustomerImportedCashbacks
     */
    public function testGetCustomerImportedCashbacks()
    {
        $customer = EntityHelper::createCustomer($this->faker->companyEmail);
        EntityHelper::createRetailCashbackForCustomer($customer, Cashback::STATUS_IMPORTED);
        EntityHelper::createRetailCashbackForCustomer($customer, Cashback::STATUS_ELIGIBLE);
        EntityHelper::createRetailCashbackForCustomer($customer, Cashback::STATUS_IMPORTED);
        EntityHelper::createRetailCashbackForCustomer($customer, Cashback::STATUS_PAID);

        $importedCashbacks = $this->cashbackService->getCustomerImportedCashbacks($customer);
        $this->assertCount(2, $importedCashbacks);
        $this->assertContainsOnlyInstancesOf('Entities\Cashback', $importedCashbacks);
    }

    /**
     * @covers Services\CashbackService::getCustomerImportedCashbacks
     */
    public function testGetCustomerImportedCashbacksEmpty()
    {
        $customer = EntityHelper::createCustomer($this->faker->companyEmail);
        EntityHelper::createRetailCashbackForCustomer($customer, Cashback::STATUS_ELIGIBLE);
        EntityHelper::createRetailCashbackForCustomer($customer, Cashback::STATUS_PAID);

        $importedCashbacks = $this->cashbackService->getCustomerImportedCashbacks($customer);
        $this->assertEmpty($importedCashbacks);
    }
}
