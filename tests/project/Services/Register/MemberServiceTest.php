<?php

namespace Services\Register;

use Entities\Register\ShareClassEvent;
use Entities\Register\Member;
use Entities\Register\ShareClass;
use EntityNotFound;
use PHPUnit_Framework_TestCase;
use EntityHelper;
use DiLocator;
use Basket;
use Faker\Factory as FakerFactory;
use Faker\Generator as FakerGenerator;
use Faker\Provider\Lorem as LoremProvider;
use Entities\Company as CompanyEntity;

class MemberServiceTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var MemberService
     */
    private $object;

    /**
     * @var FakerGenerator
     */
    private $faker;

    /**
     * @var CompanyEntity
     */
    private $company;

    protected function setUp()
    {
        EntityHelper::emptyTables(array('cms2_customers', 'ch_company', 'cms2_register_members', 'cms2_register_share_classes', 'cms2_register_share_class_events'));
        $this->object = $this->object = EntityHelper::getService(DiLocator::SERVICE_REGISTER_MEMBER);

        $this->faker = FakerFactory::create();
        $this->faker->addProvider(new LoremProvider($this->faker));

        $this->company = EntityHelper::createCompany(EntityHelper::createCustomer($this->faker->email));
    }

    /**
     * @covers Services\Register\MemberService::getMemberById
     */
    public function testGetMemberById()
    {
        $this->createMember($this->company);
        $member = $this->object->getMemberById(1);
        $this->assertInstanceOf('Entities\Register\Member', $member);
        $this->assertEquals(1, $member->getId());
    }

    /**
     * @covers Services\Register\MemberService::saveMember
     */
    public function testSaveMember()
    {
        $this->createMember($this->company);
        $member = $this->object->getMemberById(1);

        $shareClasses = $member->getShareClasses();
        $this->assertEquals(2, count($shareClasses));
        $this->assertEquals(3, count($shareClasses[0]->getShareClassEvents()));
        $this->assertEquals(2, count($shareClasses[1]->getShareClassEvents()));
    }

    /**
     * @covers Services\Register\MemberService::deleteMember
     */
    public function testDeleteMember()
    {
        $this->createMember($this->company);
        $member = $this->object->getMemberById(1);
        $this->assertEquals(1, $member->getId());

        $this->object->deleteMember($member);
        $member = $this->object->getMemberById(1);
        $this->assertNull($member);
    }

    /**
     * @covers Services\Register\MemberService::getCompanyMemberById
     */
    public function testGetCompanyMemberById()
    {
        $this->createMember($this->company);
        $this->assertInstanceOf('Entities\Register\Member', $this->object->getCompanyMemberById($this->company, 1));
    }

    /**
     * @covers Services\Register\MemberService::getCompanyMemberById
     * @expectedException \Exceptions\Business\Forbidden
     */
    public function testGetCompanyMemberByIdDifferentCompany()
    {
        $this->createMember($this->company);
        $company2 = EntityHelper::createCompany(EntityHelper::createCustomer($this->faker->email));
        $this->object->getCompanyMemberById($company2, 1);
    }


    /**
     * @covers Services\Register\MemberService::getCompanyMemberById
     * @expectedException EntityNotFound
     */
    public function testGetCompanyMemberByIdNonExistingMember()
    {
        $this->createMember($this->company);
        $this->object->getCompanyMemberById($this->company, 2);
    }

    /**
     * @covers Services\Register\MemberService::getListDatasource
     */
    public function testGetListDatasource()
    {
        $this->assertInstanceOf('DoctrineDataSource', $this->object->getListDatasource($this->company));
    }


    /****************************************** private functions ******************************************/

    /**
     * @param CompanyEntity $company
     * @return Member
     */
    private function createMember(CompanyEntity $company)
    {
        $member = $this->getMember($company);

        $shareClass1 = $this->getShareClass($member);
        $shareClass1->addShareClassEvent($this->getShareClassEvent($shareClass1));
        $shareClass1->addShareClassEvent($this->getShareClassEvent($shareClass1));
        $shareClass1->addShareClassEvent($this->getShareClassEvent($shareClass1));
        $member->addShareClass($shareClass1);

        $shareClass2 = $this->getShareClass($member);
        $shareClass2->addShareClassEvent($this->getShareClassEvent($shareClass2));
        $shareClass2->addShareClassEvent($this->getShareClassEvent($shareClass2));
        $member->addShareClass($shareClass2);

        $this->object->saveMember($member);
        return $member;
    }

    /**
     * @param CompanyEntity $company
     * @return Member
     */
    private function getMember(CompanyEntity $company)
    {
        $member = new Member($company);
        $member->setName($this->faker->firstName . ' ' . $this->faker->lastName);
        $member->setAddress($this->faker->address);
        return $member;
    }

    /**
     * @param $member
     * @return ShareClass
     */
    private function getShareClass($member)
    {
        $shareClass = new ShareClass($member);
        $shareClass->setClassType($this->faker->word());
        $shareClass->setCurrencyIso($this->faker->word());
        $shareClass->setPrice($this->faker->randomNumber(3));
        return $shareClass;
    }

    /**
     * @param $shareClass
     * @return ShareClassEvent
     */
    private function getShareClassEvent($shareClass)
    {
        $event = new ShareClassEvent($shareClass);
        $event->setDtEntry($this->faker->dateTimeThisMonth);
        $event->setTypeId(ShareClassEvent::TYPE_ALLOTMENT);
        $event->setQuantity($this->faker->randomNumber(2));
        $event->setCertificateNumber($this->faker->randomNumber(8));
        $event->setAcquired($this->faker->randomNumber(1));
        $event->setDisposed($this->faker->randomNumber(1));
        $event->setBalance($this->faker->randomNumber(1));
        $event->setPricePerShare($this->faker->randomNumber(2));
        $event->setTotalAmount($this->faker->randomNumber(2));
        $event->setNotes($this->faker->sentence(8));
        return $event;
    }
}
