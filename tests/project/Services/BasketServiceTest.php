<?php

namespace Services;

use Basket;
use Basket\ProductToPackageUpgradeNotification;
use BasketModule\Services\BasketService;
use Doctrine\Common\Collections\ArrayCollection;
use Entities\Company as CompanyEntity;
use Entities\Customer as CustomerEntity;
use Entities\Service;
use EntityHelper;
use Factories\Front\BasketNotificationFactory;
use Factories\ServiceViewFactory;
use LogicException;
use Package;
use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;
use Product;
use ReflectionClass;
use RenewalPackage;
use Tests\ProductHelper;

class BasketServiceTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var BasketService
     */
    private $object;

    /**
     * @var Basket
     */
    private $basketMock;

    /**
     * @var ServiceService
     */
    private $serviceServiceMock;

    /**
     * @var Product
     */
    private $productMock;

    /**
     * @var ServiceViewFactory
     */
    private $serviceViewFactoryMock;

    /**
     * @var BasketNotificationFactory
     */
    private $basketNotificationFactoryMock;

    /**
     * @var ServiceView
     */
    private $serviceViewMock;

    /**
     * @var CompanyService|PHPUnit_Framework_MockObject_MockObject
     */
    private $companyServiceMock;

    /**
     * @var CompanyEntity
     */
    private $companyMock;

    /**
     * @var RenewalPackage
     */
    private $renewalPackageMock;

    /**
     * @var ArrayCollection
     */
    private $servicesMock;

    /**
     * @var CustomerEntity
     */
    private $customerMock;

    /**
     * @var Service
     */
    private $serviceMock;

    /**
     * @var Package|PHPUnit_Framework_MockObject_MockObject
     */
    private $packageMock;

    /**
     * @var ArrayCollection|PHPUnit_Framework_MockObject_MockObject
     */
    private $arrayCollectionMock;

    /**
     * @var ProductToPackageUpgradeNotification|PHPUnit_Framework_MockObject_MockObject
     */
    private $productUpgradeItemMock;

    protected function setUp()
    {
        EntityHelper::emptyTables(array(TBL_CUSTOMERS, TBL_COMPANY));

        $this->companyMock = $this->getMockBuilder('Entities\Company')->disableOriginalConstructor()->getMock();
        $this->basketMock = $this->getMockBuilder('Basket')->disableOriginalConstructor()->getMock();
        $this->serviceViewMock = $this->getMockBuilder('Models\View\ServiceView')->disableOriginalConstructor()->getMock();
        $this->serviceServiceMock = $this->getMockBuilder('Services\ServiceService')->disableOriginalConstructor()->getMock();
        $this->companyServiceMock = $this->getMockBuilder('Services\CompanyService')->disableOriginalConstructor()->getMock();
        $this->serviceViewFactoryMock = $this->getMockBuilder('Factories\ServiceViewFactory')->disableOriginalConstructor()->getMock();
        $this->basketNotificationFactoryMock = $this->getMockBuilder('Factories\Front\BasketNotificationFactory')->disableOriginalConstructor()->getMock();
        $this->servicesMock = $this->getMockBuilder('Doctrine\Common\Collections\ArrayCollection')->disableOriginalConstructor()->getMock();
        $this->customerMock = $this->getMockBuilder('Entities\Customer')->disableOriginalConstructor()->getMock();
        $this->serviceMock = $this->getMockBuilder('Entities\Service')->disableOriginalConstructor()->getMock();
        $this->companyServiceMock = $this->getMockBuilder('Services\CompanyService')->disableOriginalConstructor()->getMock();
        $this->packageMock = $this->getMockBuilder('Package')->disableOriginalConstructor()->getMock();
        $this->productMock = $this->getMockBuilder('Product')->disableOriginalConstructor()->getMock();
        $this->productMock->method('getCompanyId')->willReturn(1);
        $this->renewalPackageMock = $this->getMockBuilder('RenewalPackage')->disableOriginalConstructor()->getMock();
        $this->renewalPackageMock->method('getCompanyId')->willReturn(1);
        $this->arrayCollectionMock = $this->getMockBuilder('Doctrine\Common\Collections\ArrayCollection')->disableOriginalConstructor()->getMock();
        $this->productUpgradeItemMock = $this->getMockBuilder('Basket\ProductUpgradeItem')->disableOriginalConstructor()->getMock();

        $this->object = new BasketService($this->basketMock, $this->serviceServiceMock, $this->companyServiceMock, $this->serviceViewFactoryMock, $this->basketNotificationFactoryMock);
    }

    /******************************************** getPackagesWithCompany() ********************************************/

    /**
     * @covers BasketModule\Services\BasketService::getPackagesWithCompany
     */
    public function testGetPackagesWithCompany()
    {
        $this->basket = new Basket();
        $this->package = new RenewalPackage(1353);
        $this->package->setCompanyId(1);
        $this->basket->add($this->package);

        $reflector = new ReflectionClass(BasketService::class);
        $method = $reflector->getMethod('getPackagesWithCompany');
        $method->setAccessible(TRUE);
        $basketService = new BasketService($this->basket, $this->serviceServiceMock, $this->companyServiceMock, $this->serviceViewFactoryMock, $this->basketNotificationFactoryMock);

        $items = $method->invoke($basketService);

        $basketItem=$items[0];
        $this->assertInstanceOf('IPackage', $basketItem);
        $this->assertEquals(1, count($items));

    }

    /******************************************** getProductToPackageUpgradeNotifications() ********************************************/

    /**
     * @covers BasketModule\Services\BasketService::getProductToPackageUpgradeNotifications
     */
    public function testProductUpgradeItemNoUpgrade()
    {
        $arrayCollectionMock = $this->getMockBuilder('Doctrine\Common\Collections\ArrayCollection')
            ->disableOriginalConstructor()
            ->getMock();

        $this->companyServiceMock->expects($this->any())
            ->method('getCompanyById')
            ->willReturn($this->companyMock);

        $this->companyMock->expects($this->any())
            ->method('getProductServicesByTypes')
            ->willReturn($arrayCollectionMock);

        $arrayCollectionMock->expects($this->any())
            ->method('isEmpty')
            ->willReturn(TRUE);

        $basket = new Basket();
        $product = ProductHelper::getRegisteredOfficeProduct();
        $basket->add($product);

        $basketService = new BasketService($basket, $this->serviceServiceMock, $this->companyServiceMock, $this->serviceViewFactoryMock, $this->basketNotificationFactoryMock);
        $productUpgradeItems = $basketService->getProductToPackageUpgradeNotifications();

        $this->assertCount(0, $productUpgradeItems);
    }

    /**
     * @covers BasketModule\Services\BasketService::getProductToPackageUpgradeNotifications
     * @covers BasketModule\Services\BasketService::getPackagesWithCompany
     */
    public function testProductUpgradeItemUpgradeOneProduct()
    {
        $this->companyMock->expects($this->once())->method('getProductServicesByTypes')->willReturn($this->arrayCollectionMock);
        $this->packageMock->expects($this->once())->method('getTypesForPackageProducts')->willReturn(array(Service::TYPE_REGISTERED_OFFICE, Service::TYPE_SERVICE_ADDRESS));
        $this->packageMock->method('isPackage')->willReturn(TRUE);
        $this->packageMock->method('getCompanyId')->willReturn(1);

        $this->arrayCollectionMock->expects($this->once())->method('isEmpty')->willReturn(FALSE);
        $this->arrayCollectionMock->expects($this->once())->method('toArray')->willReturn([$this->packageMock]);
        $this->basketMock->expects($this->once())->method('getItems')->willReturn([$this->packageMock]);
        $this->companyServiceMock->expects($this->once())->method('getCompanyById')->willReturn($this->companyMock);
        $this->basketNotificationFactoryMock->expects($this->once())->method('createProductToPackageUpgradeNotification')->willReturn($this->productUpgradeItemMock);

        $productUpgradeItems = $this->object->getProductToPackageUpgradeNotifications();

        $this->assertCount(1, $productUpgradeItems);
        $this->assertEquals(array($this->productUpgradeItemMock), $productUpgradeItems);
    }

    /******************************************** getPackageToProductDowngradeNotification() ********************************************/

    /**
     * @covers BasketModule\Services\BasketService::getPackageToProductDowngradeNotification
     */
    public function testPackageDowngradeItem()
    {
        $this->companyMock->expects($this->any())
            ->method('getId')
            ->willReturn(1);

        $this->companyMock->expects($this->once())
            ->method('getLastPackageService')
            ->willReturn($this->serviceMock);

        $this->serviceMock->expects($this->once())
            ->method('getServiceTypeId')
            ->willReturn(-1);

        $this->companyServiceMock->expects($this->once())
            ->method('getCompanyById')
            ->with($this->companyMock->getId())
            ->willReturn($this->companyMock);

        $this->basketMock->expects($this->once())
            ->method('getItems')
            ->willReturn(array($this->productMock));

        $this->basketNotificationFactoryMock->expects($this->once())
            ->method('createPackageToProductDowngradeNotification');

        $this->object->getPackageToProductDowngradeNotification();
    }

    /**
     * @covers BasketModule\Services\BasketService::getPackageToProductDowngradeNotification
     */
    public function testPackageDowngradeItemNoItems()
    {
        $this->basketMock->expects($this->once())->method('getItems')->willReturn(array());
        $this->assertNull($this->object->getPackageToProductDowngradeNotification());
    }

    /**
     * @covers BasketModule\Services\BasketService::getPackageToProductDowngradeNotification
     */
    public function testPackageDowngradeItemNoServiceItem()
    {
        $this->basketMock->expects($this->once())->method('getItems')->willReturn(array($this->productMock));
        $this->companyServiceMock->expects($this->once())->method('getCompanyById')->willReturn($this->companyMock);
        $this->assertNull($this->object->getPackageToProductDowngradeNotification());
    }

    /******************************************** addRenewableServices() ********************************************/

    /**
     * @covers BasketModule\Services\BasketService::addRenewableServices
     */
    public function testAddRenewableServices()
    {
        $services = array(1, 2, 3);
        $this->serviceServiceMock->expects($this->exactly(count($services)))->method('getCustomerService')->willReturn($this->serviceMock);
        $this->serviceMock->expects($this->exactly(count($services)))->method('isRenewable')->willReturn(TRUE);
        $this->serviceMock->expects($this->exactly(count($services)))->method('getRenewalProduct')->willReturn($this->productMock);
        $this->basketMock->expects($this->exactly(count($services)))->method('add')->willReturn(TRUE);
        $this->object->addRenewableServices($this->customerMock, $services);
    }


    /**
     * @covers BasketModule\Services\BasketService::addRenewableServices
     * @expectedException LogicException
     */
    public function testAddRenewableServicesLogicException()
    {
        $services = array(1, 2, 3);
        $this->serviceServiceMock->expects($this->once())->method('getCustomerService')->willReturn($this->serviceMock);
        $this->serviceMock->expects($this->once())->method('isRenewable')->willReturn(FALSE);
        $this->object->addRenewableServices($this->customerMock, $services);
    }

    /******************************************** getSuspendedService() ********************************************/

    /**
     * @covers BasketModule\Services\BasketService::getSuspendedServiceItem
     */
    public function testGetSuspendedServiceItemHasSuspendedService()
    {
        $this->basketMock->expects($this->once())->method('getItems')->willReturn([$this->productMock]);
        $this->productMock->expects($this->once())->method('hasServiceType')->willReturn(TRUE);
        $this->companyServiceMock->expects($this->once())->method('getCompanyById')->willReturn($this->companyMock);
        $this->companyMock->expects($this->once())->method('getServicesByType')->willReturn(new ArrayCollection([$this->servicesMock]));
        $this->serviceViewFactoryMock->expects($this->once())->method('create')->willReturn($this->serviceViewMock);
        $this->serviceViewMock->expects($this->once())->method('isExpired')->willReturn(TRUE);

        $this->assertInstanceOf('Basket\SuspendedItem', $this->object->getSuspendedServiceItem());
    }

    /**
     * @covers BasketModule\Services\BasketService::getSuspendedServiceItem
     */
    public function testGetSuspendedServiceItemNoSuspendedService()
    {
        $this->basketMock->expects($this->once())->method('getItems')->willReturn(array($this->productMock));
        $this->productMock->expects($this->once())->method('hasServiceType')->willReturn(TRUE);
        $this->companyServiceMock->expects($this->once())->method('getCompanyById')->willReturn($this->companyMock);
        $this->companyMock->expects($this->once())->method('getServicesByType')->willReturn(new ArrayCollection(array($this->servicesMock)));
        $this->serviceViewFactoryMock->expects($this->once())->method('create')->willReturn($this->serviceViewMock);
        $this->serviceViewMock->expects($this->once())->method('isExpired')->willReturn(FALSE);
        $this->assertNull($this->object->getSuspendedServiceItem());
    }

}
