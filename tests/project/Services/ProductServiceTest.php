<?php

namespace Services;

use DateTime;
use Entities\Company;
use Entities\Order;
use Entities\OrderItem;
use Entities\Service;
use EntityHelper;
use Package;
use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;
use Product;
use tests\helpers\MockHelper;
use Tests\ProductHelper;

class ProductServiceTest extends PHPUnit_Framework_TestCase
{
    const NUMBER_OF_PRODUCTS = 2;

    /**
     * @var ProductService
     */
    private $object;

    /**
     * @var ServiceService|PHPUnit_Framework_MockObject_MockObject
     */
    private $serviceServiceMock;

    /**
     * @var Order|PHPUnit_Framework_MockObject_MockObject
     */
    private $orderEntity;

    /**
     * @var Company|PHPUnit_Framework_MockObject_MockObject
     */
    private $companyEntity;

    protected function setUp()
    {
        EntityHelper::emptyTables(array('cms2_services', 'cms2_customers', 'cms2_orders', 'ch_company', 'ch_form_submission'));

        $this->serviceServiceMock = $this->getMockBuilder('Services\ServiceService')->disableOriginalConstructor()->getMock();
        $this->companyServiceMock = $this->getMockBuilder('Services\CompanyService')->disableOriginalConstructor()->getMock();
        $this->dispatcherMock = $this->getMockBuilder('Symfony\Component\EventDispatcher\EventDispatcher')->disableOriginalConstructor()->getMock();

        $this->object = new ProductService($this->serviceServiceMock, $this->companyServiceMock, $this->dispatcherMock);

        $customer = EntityHelper::createCustomer();
        $this->orderEntity = EntityHelper::createOrder($customer);
        $this->companyEntity = EntityHelper::createCompany($customer);
        $this->companyServiceMock->expects($this->any())
            ->method('getCompanyById')
            ->will($this->returnValue($this->companyEntity));
    }

    /**
     * @covers Services\ProductService::saveServices
     */
    public function testSaveServicesNoServiceType()
    {
        $services = array();
        $this->serviceServiceMock->expects($this->exactly(0))
            ->method('saveService')
            ->will(
                $this->returnCallback(
                    function(Service $service) use (&$services) {
                        $services[] = $service;
                    }
                )
            );

        $basketItems = array($this->getProduct(), $this->getProduct());
        $this->object->saveServices($basketItems);
        $this->assertEmpty($services);
    }

    /**
     * @covers Services\ProductService::saveServices
     */
    public function testSaveServicesAsRenewalProductService()
    {
        $services = array();
        $this->serviceServiceMock->expects($this->exactly(self::NUMBER_OF_PRODUCTS))
            ->method('saveService')
            ->will(
                $this->returnCallback(
                    function(Service $service) use (&$services) {
                        $services[] = $service;
                    }
                )
            );
        $basketItems = array($this->getRenewalServiceProduct(), $this->getRenewalServiceProduct());
        $this->object->saveServices($basketItems);

        $this->assertCount(2, $services);
        $this->assertInstanceOf('Entities\Service', $services[0]);
        $this->assertEmpty($services[0]->getDtStart()); 
        $this->assertEmpty($services[0]->getDtExpires()); 
        $this->assertInstanceOf('Entities\Service', $services[1]);
        $this->assertEmpty($services[1]->getDtStart()); 
        $this->assertEmpty($services[1]->getDtExpires()); 
    }

    /**
     * @covers Services\ProductService::saveServices
     */
    public function testSaveServicesAsOneOffProductService()
    {
        $services = array();
        $this->serviceServiceMock->expects($this->exactly(self::NUMBER_OF_PRODUCTS))
            ->method('saveService')
            ->will(
                $this->returnCallback(
                    function(Service $service) use (&$services) {
                        $services[] = $service;
                    }
                )
            );
        $basketItems = array($this->getServiceProduct(), $this->getServiceProduct());
        $this->object->saveServices($basketItems);

        $this->assertCount(2, $services);
        $this->assertInstanceOf('Entities\Service', $services[0]);
        $this->assertInstanceOf('Entities\Service', $services[1]);
        $this->assertEquals(new DateTime(), $services[0]->getDtStart());
        $this->assertEmpty($services[0]->getDtExpires());
        $this->assertEquals(new DateTime(), $services[1]->getDtStart());
        $this->assertEmpty($services[1]->getDtExpires()); 
    }


    /**
     * @covers Services\ProductService::saveServices
     */
    public function testSaveServicesAsPackageService()
    {
        $products = [$this->getRenewalServiceProduct()];
        // this is due to getPackageProducts method 
        $packageMock = MockHelper::mock($this, Package::class);
        $packageMock->expects($this->exactly(1))->method('getPackageProducts')->willReturn($products);
        $packageMock->expects($this->exactly(1))->method('hasRenewalProduct')->willReturn(TRUE);
        $packageMock->expects($this->exactly(1))->method('hasServiceType')->willReturn(TRUE);
        $packageMock->expects($this->exactly(1))->method('getRenewalProduct')->willReturn($products[0]);

        $services = [];
        $basketItems = [ProductHelper::getPrivacyPackage($packageMock)];

        $this->serviceServiceMock->expects($this->once())
            ->method('saveService')
            ->will(
                $this->returnCallback(
                    function(Service $service) use (&$services) {
                        $services[] = $service;
                    }
                )
            );

        $this->object->saveServices($basketItems);

        $this->assertCount(1, $services);
        $this->assertEquals(Service::TYPE_PACKAGE_PRIVACY, $services[0]->getServiceTypeId());
        $childs = $services[0]->getChildren();
        $this->assertCount(1, $childs);
        $this->assertEquals(Service::TYPE_REGISTERED_OFFICE, $childs[0]->getServiceTypeId());
    }

    private function getProduct()
    {
        $product = new Product();
        $product->setCompanyId($this->companyEntity->id);
        $product->serviceTypeId = NULL;
        $product->renewalProductId = NULL;
        $product->setDuration(NULL);
        $product->setOrderItem(new OrderItem($this->orderEntity));
        return $product;
    }

    private function getServiceProduct()
    {
        $product = new Product();
        $product->setCompanyId($this->companyEntity->id);
        $product->serviceTypeId = Service::TYPE_REGISTERED_OFFICE;
        $product->renewalProductId = NULL;
        $product->setDuration(NULL);
        $product->setOrderItem(new OrderItem($this->orderEntity));
        return $product;
    }

    private function getRenewalServiceProduct()
    {
        $product = new Product();
        $product->setCompanyId($this->companyEntity->id);
        $product->serviceTypeId = Service::TYPE_REGISTERED_OFFICE;
        $product->renewalProductId = 165;
        $product->setDuration("+12 months");
        $product->setOrderItem(new OrderItem($this->orderEntity));
        return $product;
    }

}
