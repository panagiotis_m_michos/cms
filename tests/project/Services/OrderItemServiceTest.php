<?php

namespace Services;

use DateTime;
use DiLocator;
use Doctrine\ORM\EntityManager;
use Entities\Customer;
use Entities\Order;
use EntityHelper;
use PHPUnit_Framework_TestCase;
use Registry;

class OrderItemServiceTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var Order
     */
    private $order;

    /**
     * @var OrderItemService
     */
    private $object;

    protected function setUp()
    {
        EntityHelper::emptyTables([TBL_COMPANY, TBL_CUSTOMERS, TBL_ORDERS, TBL_ORDERS_ITEMS]);

        $this->em = EntityHelper::getEntityManager();
        $this->customer = EntityHelper::createCustomer();
        $this->order = EntityHelper::createOrder($this->customer, 0);

        $this->object =  Registry::getService(DiLocator::SERVICE_ORDER_ITEM);
    }

    /**
     * @test
     * @covers Services\OrderItemService::markItemsAsExported
     * @dataProvider dataProviderMarkItemsAsExported
     */
    public function markItemsAsExported($companyIncorporated, $incorporationRequired, $dtc, $dateTo, $expectedAffectedRows)
    {
        $item = EntityHelper::createOrderItem($this->order);
        if ($companyIncorporated) {
            $item->setCompany(EntityHelper::createCompany($this->customer, NULL, 12345678));
        } else {
            $item->setCompany(EntityHelper::createCompany($this->customer));
        }
        if ($incorporationRequired) {
            $item->setIncorporationRequired(TRUE);
        }
        $item->setDtc($dtc);

        $this->em->flush();

        $affectedRows = $this->object->markItemsAsExported($dateTo);
        $this->assertEquals($expectedAffectedRows, $affectedRows);
    }

    public function dataProviderMarkItemsAsExported()
    {
        return [
            [FALSE, FALSE, new DateTime('-1 day'), new DateTime(), 1], // incorporation not required
            [FALSE, TRUE, new DateTime('-1 day'), new DateTime(), 0], // incorporation required and company not incorporated
            [TRUE, TRUE, new DateTime('-1 day'), new DateTime(), 1], // incorporation required and company incorporated
            [FALSE, FALSE, new DateTime('+1 day'), new DateTime(), 0], // item created in future
            [FALSE, FALSE, new DateTime('-1 day'), new DateTime('-1 day'), 1], // item created same day
            [FALSE, FALSE, new DateTime('2015-02-26 15:00:00'), new DateTime('2015-02-26 16:00:00'), 1], // item created same day in past
        ];
    }

}
