<?php

namespace Services;

use Entities\CompanyHouse\FormSubmission\ChangeOfName;
use EntityHelper;
use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;
use Repositories\CompanyHouse\FormSubmission\AnnualReturnRepository;
use Repositories\CompanyHouse\FormSubmissionErrorRepository;
use Repositories\CompanyHouse\FormSubmissionRepository;
use SubmisionResponseHelper;
use Entities\CompanyHouse\FormSubmission;
use EventLocator;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Tests\Helpers\FormSubmissionEntityHelper;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation;
use Tests\Helpers\FormSubmissionObjectHelper;

class SubmissionServiceTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var SubmissionService
     */
    protected $object;

    /**
     * @var ChangeOfName
     */
    protected $changeNameFormSubmision;

    /**
     * @var CompanyIncorporation
     */
    protected $companyIncorporationFormSubmision;

    /**
     * @var EventDispatcher|PHPUnit_Framework_MockObject_MockObject
     */
    protected $eventDispatcherMock;

    protected function setUp()
    {
        EntityHelper::emptyTables(EntityHelper::$tables);

        /** @var FormSubmissionErrorRepository $formSubmissionErrorRepository */
        $formSubmissionErrorRepository = EntityHelper::getService('repositories.company_house.form_submission_error_repository');
        /** @var FormSubmissionRepository $formSubmissionRepository */
        $formSubmissionRepository = EntityHelper::getService('repositories.company_house.form_submission_repository');
        /** @var AnnualReturnRepository $annualReturnRepository */
        $annualReturnRepository = EntityHelper::getService('repositories.company_house.form_submission.annual_return_repository');

        $this->getMockBuilder('Symfony\Component\EventDispatcher\EventDispatcher')
            ->setMethods(array('dispatch'))
            ->disableOriginalConstructor()->getMock();

        $this->eventDispatcherMock = $this->getMockBuilder('Symfony\Component\EventDispatcher\EventDispatcher')
            ->setMethods(array('dispatch'))
            ->disableOriginalConstructor()->getMock();

        $this->object = new SubmissionService(
            $formSubmissionRepository,
            $formSubmissionErrorRepository,
            $annualReturnRepository,
            $this->eventDispatcherMock
        );

        $customer = EntityHelper::createCustomer('dev-test@madesimplegroup.com');
        $companyEntity = EntityHelper::createCompany($customer);
        $this->changeNameFormSubmision = FormSubmissionEntityHelper::createChangeOfName($companyEntity);

        $customer = EntityHelper::createCustomer('test@madesimplegroup.com');
        $companyEntity2 = EntityHelper::createCompanyToIncorporate($customer);
        $this->companyIncorporationFormSubmision = $companyEntity2->getFormSubmissions()->first();
    }

    /**
     * @covers Services\SubmissionService::getCompanyFormSubmissions
     */
    public function testGetCompanyFormSubmissionsNoFormSubmission()
    {
        $customer = EntityHelper::createCustomer();
        $companyEntity = EntityHelper::createCompany($customer);
        $submissions = $this->object->getCompanyFormSubmissions($companyEntity);
        $this->assertEquals(array(), $submissions);
    }

    /**
     * @covers Services\SubmissionService::getCompanyFormSubmissions
     */
    public function testGetCompanyFormSubmissions()
    {
        $customer = EntityHelper::createCustomer();
        $companyEntity = EntityHelper::createCompany($customer);

        //create all formsubmissions
        foreach (array_keys(EntityHelper::$submissionIdentifiers) as $identifier) {
            $formSubmission = FormSubmissionObjectHelper::createFormSubmission($companyEntity, $identifier);
            FormSubmissionEntityHelper::save($formSubmission);
        }

        $submissions = array_reverse($this->object->getCompanyFormSubmissions($companyEntity));

        $i = 0;
        foreach (EntityHelper::$submissionIdentifiers as $key => $value) {
            if ($key == 'AnnualReturn') {
                $key = 'CHAnnualReturn';
            }
            $submission = $submissions[$i];
            $this->assertInstanceOf('FormSubmission', $submission);
            $this->assertInstanceOf($key, $submission->getForm());
            $i++;
        }
    }

    /**
     * @covers Services\SubmissionService::acceptChangeOfName
     */
    public function testAcceptChangeOfName()
    {
        $submissionStatus = SubmisionResponseHelper::createNameChangeAccept($this->changeNameFormSubmision->getId());
        $result = $this->object->acceptChangeOfName(
            $this->changeNameFormSubmision,
            $submissionStatus,
            array('attachmentPath' => 'docName')
        );

        $this->assertEquals(
            array(
                'formSubmissionId' => $this->changeNameFormSubmision->getId(),
                'requestKey' => $submissionStatus->getChangeOfNameDetails()->getDocRequestKey(),
                'companyNumber' => $submissionStatus->getCompanyNumber(),
                'attachmentPath' => 'docName'
            ),
            $result
        );
    }

    /**
     * @covers Services\SubmissionService::acceptChangeOfName
     * @expectedException \Exceptions\Business\DocumentKeyMissingException
     */
    public function testAcceptChangeOfNameDocumentKeyMissingException()
    {
        $submissionStatus = SubmisionResponseHelper::createOtherAccept($this->changeNameFormSubmision->getId());
        $this->object->acceptChangeOfName(
            $this->changeNameFormSubmision,
            $submissionStatus,
            array('attachmentPath' => 'docName')
        );
    }

    /**
     * @covers Services\SubmissionService::acceptChangeOfName
     * @expectedException \Exceptions\Business\CompanyIncorporationException
     */
    public function testAcceptChangeOfNameCompanyIncorporationException()
    {
        $submissionStatus = SubmisionResponseHelper::createNameChangeAccept($this->changeNameFormSubmision->getId());
        $this->object->acceptChangeOfName($this->changeNameFormSubmision, $submissionStatus, array());
    }

    /**
     * @covers Services\SubmissionService::acceptCompanyIncorporation
     */
    public function testAcceptCompanyIncorporation()
    {
        $submissionStatus = SubmisionResponseHelper::createCompanyIncorporationAccept(
            $this->companyIncorporationFormSubmision->getId()
        );
        $result = $this->object->acceptCompanyIncorporation(
            $this->companyIncorporationFormSubmision,
            $submissionStatus,
            array('attachmentPath' => 'docName')
        );

        $this->assertEquals(
            array(
                'formSubmissionId' => $this->companyIncorporationFormSubmision->getId(),
                'requestKey' => $submissionStatus->getIncorporationDetails()->getDocRequestKey(),
                'companyNumber' => $submissionStatus->getCompanyNumber(),
                'incorporationDate' => $submissionStatus->getIncorporationDetails()->getIncorporationDate(),
                'authenticationCode' => $submissionStatus->getIncorporationDetails()->getAuthenticationCode(),
                'attachmentPath' => 'docName'
            ),
            $result
        );
    }

    /**
     * @covers Services\SubmissionService::rejectCompanyIncorporation
     */
    public function testRejectCompanyIncorporation()
    {
        $submissionStatus = SubmisionResponseHelper::createCompanyIncorporationReject(
            $this->companyIncorporationFormSubmision->getId()
        );
        $result = $this->object->rejectCompanyIncorporation(
            $this->companyIncorporationFormSubmision,
            $submissionStatus
        );

        $this->assertNull($result);
    }

    /**
     * @covers Services\SubmissionService::processSubmissionStatus
     */
    public function testProcessSubmissionStatus()
    {
        $submissionStatuses = array(
            SubmisionResponseHelper::createNameChangeAccept($this->changeNameFormSubmision->getId()),
            SubmisionResponseHelper::createCompanyIncorporationAccept(
                $this->companyIncorporationFormSubmision->getId()
            ),
            SubmisionResponseHelper::createCompanyIncorporationReject(
                $this->companyIncorporationFormSubmision->getId()
            ),
            //SubmisionResponseHelper::createOtherReject($this->changeNameFormSubmision->getId()),
            //SubmisionResponseHelper::createOtherAccept($this->changeNameFormSubmision->getId()),
            //SubmisionResponseHelper::createFailure($this->changeNameFormSubmision->getId()),
        );

        $calledParams = array();
        $this->eventDispatcherMock->expects($this->exactly(6))
            ->method('dispatch')
            ->will(
                $this->returnCallback(
                    function ($eventName) use (&$calledParams) {
                        $calledParams[] = $eventName;
                    }
                )
            );

        foreach ($submissionStatuses as $submissionStatus) {
            $this->object->processSubmissionStatus($submissionStatus);
        }

        $this->assertEquals(
            array(
                EventLocator::FORM_SUBMISSION_ACCEPT_CHANGE_OF_NAME,
                EventLocator::FORM_SUBMISSION_ACCEPT,
                EventLocator::FORM_SUBMISSION_ACCEPT_INCORPORATION,
                EventLocator::FORM_SUBMISSION_ACCEPT,
                EventLocator::FORM_SUBMISSION_REJECT_INCORPORATION,
                EventLocator::FORM_SUBMISSION_REJECT,
                //EventLocator::FORM_SUBMISSION_REJECT,
                //EventLocator::FORM_SUBMISSION_ACCEPT,
                //EventLocator::FORM_SUBMISSION_FAILURE,
            ),
            $calledParams
        );
    }
    
    public function testIncorporationEntities()
    {
        EntityHelper::emptyTables(EntityHelper::$tables);
        $customer = EntityHelper::createCustomer();
        $companyEntity = EntityHelper::createCompany($customer);
        FormSubmissionEntityHelper::createCompanyIncorporation($companyEntity);

        $companyIncorp = EntityHelper::getEntityManager()->getRepository('Entities\CompanyHouse\FormSubmission')->find(1);
        $appoins = $companyIncorp->getAppointments();
        $capitals = $companyIncorp->getCapitals();
        $subs = $companyIncorp->getSubscribers(); 
        $docs = $companyIncorp->getDocuments();

        //testing register office
        $this->assertEquals('premise', $companyIncorp->getRegisteredOfficeAddress()->getPremise());
        
        //testing directors
        $this->assertEquals(FormSubmissionObjectHelper::createPersonDirector()->getResidentialAddress(), $appoins[0]->getResidentialAddress());
        $this->assertEquals(FormSubmissionObjectHelper::createCorporateDirectorEea()->getOfficer()->getIdentification(), $appoins[1]->getOfficer()->getIdentification());
        $this->assertEquals(FormSubmissionObjectHelper::createCorporateDirectorNonEea()->getOfficer()->getIdentification()->getLawGoverned(), $appoins[2]->getOfficer()->getIdentification()->getLawGoverned());
        
        //testing secretaries
        $this->assertEquals(FormSubmissionObjectHelper::createPersonSecretary()->getOfficer()->getAddress(), $appoins[3]->getOfficer()->getAddress());
        $this->assertEquals(FormSubmissionObjectHelper::createCorporateSecretaryEea()->getOfficer()->getIdentification(), $appoins[4]->getOfficer()->getIdentification());
        $this->assertEquals(FormSubmissionObjectHelper::createCorporateDirectorNonEea()->getOfficer()->getIdentification()->getLawGoverned(), $appoins[5]->getOfficer()->getIdentification()->getLawGoverned());

        //testing subscribers
        $this->assertEquals(FormSubmissionObjectHelper::createPersonSubscriber()->getShare(), $subs[0]->getShare());
        $this->assertEquals(FormSubmissionObjectHelper::createCorporateSubscriber()->getOfficer(), $subs[1]->getOfficer());
        $this->assertEquals(FormSubmissionObjectHelper::createCorporateSubscriber()->getAuthentication(), $subs[1]->getAuthentication());
        
        // testing capital
        $this->assertEquals('GBP', $capitals[0]->getCurrency());
        
        //testing documents
        $this->assertEquals('MEMARTS', $docs[0]->getCategory());
        
    }
}
