<?php

namespace Services;

use CompanyIncorporation;
use Entities\Service;
use EntityHelper;
use Factories\Front\CompanyViewFactory;
use NomineeDirector;
use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;
use DateTime;
use DiLocator;
use Symfony\Component\EventDispatcher\EventDispatcher;
use tests\helpers\MockHelper;

class CompanyServiceTest extends PHPUnit_Framework_TestCase
{

    /**
     * @var CompanyService
     */
    protected $object;

    /**
     * @var CompanyViewFactory|PHPUnit_Framework_MockObject_MockObject
     */
    private $companyViewFactoryMock;

    /**
     * @var EventDispatcher|PHPUnit_Framework_MockObject_MockObject
     */
    private $eventDispatcherMock;

    protected function setUp()
    {
        EntityHelper::emptyTables(
            [TBL_CUSTOMERS, TBL_COMPANY, TBL_ORDERS, TBL_SERVICES, TBL_FORM_SUBMISSIONS, TBL_COMPANY_INCORPORATIONS]
        );
        $this->object = EntityHelper::getService(DiLocator::SERVICE_COMPANY);

        $this->companyViewFactoryMock = MockHelper::mock($this, 'Factories\Front\CompanyViewFactory');
        $this->eventDispatcherMock = MockHelper::mock($this, 'Symfony\Component\EventDispatcher\EventDispatcher');
    }

    /**
     * @covers Services\CompanyService::saveCompany
     */
    public function testSaveCompany()
    {
        $companyRepositoryMock = MockHelper::mock($this, 'Repositories\CompanyRepository');
        $companyMock = MockHelper::mock($this, 'Entities\Company');
        $companySettingsRepositoryMock = MockHelper::mock($this, 'CompanyModule\Repositories\CompanySettingsRepository');

        $companyRepositoryMock
            ->expects($this->once())
            ->method('saveEntity')->with($companyMock)
            ->willReturn($companyMock);

        $companyService = new CompanyService($companyRepositoryMock, $this->companyViewFactoryMock, $this->eventDispatcherMock, $companySettingsRepositoryMock);

        $companyService->saveCompany($companyMock);
    }

    /**
     * @covers Services\CompanyService::getCompanyById
     */
    public function testGetCompanyById()
    {
        $companyRepositoryMock = MockHelper::mock($this, 'Repositories\CompanyRepository');
        $companyMock = MockHelper::mock($this, 'Entities\Company');
        $companyId = 5;
        $companySettingsRepositoryMock = MockHelper::mock($this, 'CompanyModule\Repositories\CompanySettingsRepository');


        $companyRepositoryMock
            ->expects($this->once())
            ->method('find')->with($companyId)
            ->willReturn($companyMock);

        $companyService = new CompanyService($companyRepositoryMock, $this->companyViewFactoryMock, $this->eventDispatcherMock, $companySettingsRepositoryMock);

        $this->assertEquals($companyMock, $companyService->getCompanyById($companyId));
    }

    /**
     * @covers Services\CompanyService::getCustomerCompanies
     */
    public function testGetCustomerCompanies()
    {
        $companyRepositoryMock = MockHelper::mock($this, 'Repositories\CompanyRepository');
        $companyMock = MockHelper::mock($this, 'Entities\Company');
        $customerMock = MockHelper::mock($this, 'Entities\Customer');
        $companySettingsRepositoryMock = MockHelper::mock($this, 'CompanyModule\Repositories\CompanySettingsRepository');


        $companyRepositoryMock
            ->expects($this->once())
            ->method('findBy')->with(array('customer' => $customerMock))
            ->willReturn($companyMock);

        $companyService = new CompanyService($companyRepositoryMock, $this->companyViewFactoryMock, $this->eventDispatcherMock, $companySettingsRepositoryMock);

        $this->assertEquals($companyMock, $companyService->getCustomerCompanies($customerMock));
    }

    /**
     * @covers Services\CompanyService::getUnsubmittedCompanies
     */
    public function testGetOneCompany()
    {
        $companyRepositoryMock = MockHelper::mock($this, 'Repositories\CompanyRepository');
        $companySettingsRepositoryMock = MockHelper::mock($this, 'CompanyModule\Repositories\CompanySettingsRepository');
        $companyMock = MockHelper::mock($this, 'Entities\Company');
        $customerMock = MockHelper::mock($this, 'Entities\Customer');

        $companyRepositoryMock
            ->expects($this->once())
            ->method('getUnsubmittedCompanies')->with($customerMock)
            ->willReturn($companyMock);

        $companyService = new CompanyService($companyRepositoryMock, $this->companyViewFactoryMock, $this->eventDispatcherMock, $companySettingsRepositoryMock);
        $this->assertEquals($companyMock, $companyService->getUnsubmittedCompanies($customerMock));
    }

    /**
     * @covers Services\CompanyService::getCompaniesWithActiveServices
     */
    public function testGetCompaniesWithActiveServices()
    {
        $customer = EntityHelper::createCustomer('test23@email.com');

        $data = array(
            28 => Service::TYPE_REGISTERED_OFFICE, 
            15 => Service::TYPE_ANNUAL_RETURN, 
            1 => Service::TYPE_PACKAGE_BY_GUARANTEE, 
            0 => Service::TYPE_PACKAGE_PRIVACY, 

            -30 => Service::TYPE_NOMINEE,
            30 => Service::TYPE_REGISTERED_OFFICE,
            -15 => Service::TYPE_SERVICE_ADDRESS,
            -27 => Service::TYPE_NOMINEE,
        );
        
        foreach ($data as $day => $type) {
            $company = EntityHelper::createCompany($customer);
            $order = EntityHelper::createOrder($customer);
            $date = new DateTime($day .' days 00:00:00');
            $service = EntityHelper::createService($company, $order->getItems()->first(), new DateTime('-300 days'), $date, $type);
            $service->setCompany($company);
        }
        
        $companies = $this->object->getCompaniesWithActiveServices();
        
        foreach ($companies as $company) {
            $ids[] = $company->getId();
        }
        
        $this->assertEquals(array(1,2,3,4,7,8), $ids);
    }

    /**
     *
     */
    public function testCanAddNomineeDirectorToCompany()
    {
        $customer = EntityHelper::createCustomer('test@mail.com');
        $order = EntityHelper::createOrder($customer);
        $company = EntityHelper::createCompanyToIncorporate($customer, $order);
        $oldCompany = \Company::getCompany($company->getCompanyId());

        /** @var \NomineeDirector $nomineeDirectorMock */
        $nomineeDirectorMock = MockHelper::mock($this, 'NomineeDirector');

        $nomineeDirectorMock->method('isOk2Show')->willReturn(TRUE);
        $nomineeDirectorMock->method('getIsCorporate')->willReturn(FALSE);
        $nomineeDirectorMock->personDob = '1990-01-01';
        $nomineeDirectorMock->nodeId = 987;
        $nomineeDirectorMock->personBirtown = 'qwe';
        $nomineeDirectorMock->personAuthTel = 'qwe';
        $nomineeDirectorMock->personAuthEye = 'qwe';
        $nomineeDirectorMock->personAddress1 = 'qwe';
        $nomineeDirectorMock->personAddress2 = 'qwe';
        $nomineeDirectorMock->personAddress3 = 'qwe';
        $nomineeDirectorMock->personNationality = 'qwe';
        $nomineeDirectorMock->personOccupation = 'qwe';
        $nomineeDirectorMock->personCountryOfResidence = 'qwe';

        /** @var CompanyIncorporation $companyIncorporation */
        $companyIncorporation = $oldCompany->getLastIncorporationFormSubmission()->getForm();
        $companyIncorporation->removeNomineeDirector();

        $this->assertTrue($companyIncorporation->hasNomineeDirector() == 0);
        $this->object->addNomineeDirectorToCompany($oldCompany, $nomineeDirectorMock);
        $this->assertTrue($companyIncorporation->hasNomineeDirector() == 1);
    }
}
