<?php

namespace Services\Facades;

use AdminFee;
use Doctrine\Common\Collections\ArrayCollection;
use Entities\Customer;
use Entities\Order;
use Entities\OrderItem;
use EventLocator;
use FUser;
use Order\Refund\Calculator\Result;
use Order\Refund\Data;
use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;
use Services\OrderService;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Tests\Helpers\ObjectHelper;

class OrderRefundFacadeTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var OrderRefundFacade
     */
    private $object;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|OrderService
     */
    private $orderServiceMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|EventDispatcher
     */
    private $eventDispatcherMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|Result
     */
    private $calculatorResultMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|Data
     */
    private $refundDataMock;

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var FUser
     */
    private $adminUser;


    protected function setUp()
    {
        $this->orderServiceMock = $this->getMockBuilder('Services\OrderService')->disableOriginalConstructor()->getMock();
        $this->eventDispatcherMock = $this->getMockBuilder('Symfony\Component\EventDispatcher\EventDispatcher')->disableOriginalConstructor()->getMock();
        $this->refundDataMock = $this->getMockBuilder('Order\Refund\Data')->disableOriginalConstructor()->getMock();
        $this->calculatorResultMock = $this->getMockBuilder('Order\Refund\Calculator\Result')->disableOriginalConstructor()->getMock();

        $this->object = new OrderRefundFacade($this->orderServiceMock, $this->eventDispatcherMock);

        $this->customer = ObjectHelper::createCustomer(TEST_EMAIL1);

        $userRole = ObjectHelper::createUserRole();
        $this->adminUser = ObjectHelper::createUser($userRole, TEST_EMAIL2);
    }


    /**
     * @test
     * @dataProvider refundDataProvider
     */
    public function refund(
        Order $order,
        ArrayCollection $refundItems,
        $adminFee,
        $hasTotalOrderItems,
        $hasMatchedItems,
        $hasRefundedItemsCount,
        $hasRefundItemsCount,
        $hasAdminFeeItemsCount,
        $orderStatus
    )
    {

        $this->refundDataMock->expects($this->once())->method('getDescription');
        $this->calculatorResultMock->expects($this->once())->method('getTotalMoneyRefund');
        $this->calculatorResultMock->expects($this->once())->method('getTotalCreditRefund');
        $this->refundDataMock->expects($this->once())->method('getOrderItems')->willReturn($refundItems);
        $this->calculatorResultMock->expects($this->once())->method('getAdminFee')->willReturn($adminFee);
        $this->orderServiceMock->expects($this->once())->method('saveOrder');
        $this->eventDispatcherMock->expects($this->once())->method('dispatch')->with(EventLocator::ORDER_REFUNDED);

        $this->object->refund($order, $this->refundDataMock, $this->calculatorResultMock, $this->adminUser);

        // has refunded order items
        $this->assertCount($hasTotalOrderItems, $order->getItems());

        $matchedItems = $refundedItemsCount = $refundItemsCount = 0;
        foreach ($order->getItems() as $item) {

            if ($item->isRefunded() && $this->matchRefundItem($order, $item)) {
                $matchedItems++;
            }
            if ($item->isRefunded()) {
                $refundedItemsCount++;
            }
            if ($item->getIsRefund()) {
                $refundItemsCount++;
            }
        }

        $this->assertSame($hasMatchedItems, $matchedItems, "Order should have $hasMatchedItems items with same productId");
        $this->assertSame($hasRefundedItemsCount, $refundedItemsCount, "Order should have $hasRefundedItemsCount refunded item");
        $this->assertSame($hasRefundItemsCount, $refundItemsCount, "Order should have $hasRefundItemsCount refund item");

        // has admin fee item
        $count = 0;
        foreach ($order->getItems() as $item) {
            if ($item->getProductId() === AdminFee::ADMIN_FEE_PRODUCT) {
                $count++;
            }
        }
        $this->assertSame($hasAdminFeeItemsCount, $count, "Order doesn't contain any admin fee order item product");
        $this->assertSame($orderStatus, $order->getStatusId());
    }

    /**
     * @todo: create iterator object
     * @return array
     */
    public function refundDataProvider()
    {
        return array(
            $this->dataProvider1(),
            $this->dataProvider2(),
            $this->dataProvider3(),
            $this->dataProvider4(),
            $this->dataProvider5(),
        );
    }

    /**
     * 2 items, 2 refunds, has admin fee
     * @return array
     */
    private function dataProvider1()
    {
        $customer = ObjectHelper::createCustomer(TEST_EMAIL1);
        $order = ObjectHelper::createOrder($customer, 0);
        $orderItem1 = ObjectHelper::createOrderItem($order, 20, 1, 123);
        $order->addItem($orderItem1);
        $orderItem2 = ObjectHelper::createOrderItem($order, 20, 1, 1234);
        $order->addItem($orderItem2);

        $refundItems = new ArrayCollection(array($orderItem1, $orderItem2));

        return array($order, $refundItems, 20, 5, 2, 2, 2, 1, Order::STATUS_CANCELLED);
    }

    /**
     * 2 items, 2 refunds, no admin fee
     * @return array
     */
    private function dataProvider2()
    {
        $customer = ObjectHelper::createCustomer(TEST_EMAIL1);
        $order = ObjectHelper::createOrder($customer, 0);
        $orderItem1 = ObjectHelper::createOrderItem($order, 20, 1, 123);
        $order->addItem($orderItem1);
        $orderItem2 = ObjectHelper::createOrderItem($order, 20, 1, 1234);
        $order->addItem($orderItem2);

        $refundItems = new ArrayCollection(array($orderItem1, $orderItem2));

        return array($order, $refundItems, 0, 4, 2, 2, 2, 0, Order::STATUS_CANCELLED);
    }

    /**
     * 2 items, 1 refunds, no admin fee
     * @return array
     */
    private function dataProvider3()
    {
        $customer = ObjectHelper::createCustomer(TEST_EMAIL1);
        $order = ObjectHelper::createOrder($customer, 0);
        $orderItem1 = ObjectHelper::createOrderItem($order, 20, 1, 123);
        $order->addItem($orderItem1);
        $orderItem2 = ObjectHelper::createOrderItem($order, 20, 1, 1234);
        $order->addItem($orderItem2);

        $refundItems = new ArrayCollection(array($orderItem1));

        return array($order, $refundItems, 0, 3, 1, 1, 1, 0, Order::STATUS_CANCELLED);
    }

    /**
     * 2 items, 0 refund, admin fee
     * @return array
     */
    private function dataProvider4()
    {
        $customer = ObjectHelper::createCustomer(TEST_EMAIL1);
        $order = ObjectHelper::createOrder($customer, 0);
        $orderItem1 = ObjectHelper::createOrderItem($order, 20, 1, 123);
        $order->addItem($orderItem1);
        $orderItem2 = ObjectHelper::createOrderItem($order, 20, 1, 1234);
        $order->addItem($orderItem2);

        return array($order, new ArrayCollection(), 20, 3, 0, 0, 0, 1, Order::STATUS_CANCELLED);
    }

    /**
     * 2 items, 0 refund, no admin fee
     * @return array
     */
    private function dataProvider5()
    {
        $customer = ObjectHelper::createCustomer(TEST_EMAIL1);
        $order = ObjectHelper::createOrder($customer, 0);
        $orderItem1 = ObjectHelper::createOrderItem($order, 20, 1, 123);
        $order->addItem($orderItem1);
        $orderItem2 = ObjectHelper::createOrderItem($order, 20, 1, 1234);
        $order->addItem($orderItem2);

        return array($order, new ArrayCollection(), 0, 2, 0, 0, 0, 0, Order::STATUS_CANCELLED);
    }

    /**
     * @param Order $order
     * @param OrderItem $refundedItem
     * @return bool
     */
    private function matchRefundItem(Order $order, OrderItem $refundedItem)
    {
        foreach ($order->getItems() as $item) {
            if ($item->getIsRefund() && $item->getProductId() == $refundedItem->getProductId()) {
                if ($item->getQty() == -1 * $refundedItem->getQty()) {
                    if ($item->getTotalPrice() == -1 * $refundedItem->getTotalPrice()) {
                        return TRUE;
                    }
                }
            }
        }
        return FALSE;
    }
}
