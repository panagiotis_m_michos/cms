<?php

namespace Services\Facades;

use EntityHelper;
use DiLocator;
use PHPUnit_Framework_TestCase;
use dibi;
use Entities\CompanyHouse\FormSubmission;

class CompanyCloneFacadeTest extends PHPUnit_Framework_TestCase
{

    /**
     * @var CompanyCloneFacade
     */
    protected $object;

    protected function setUp()
    {
        EntityHelper::emptyTables(EntityHelper::$tables);

        $this->object = EntityHelper::getService(DiLocator::FACADE_CLONE_COMPANY);
    }

    /**
     * @covers Services\Facades\CompanyCloneFacade::copyCompany
     */
    public function testCopyCompany()
    {
        $customer = EntityHelper::createCustomer();
        $oldCompany = EntityHelper::createCompanyToIncorporate($customer);
        $oldCompanyName = $oldCompany->getCompanyName();
        $newCompanyName = 'testName';
        $this->assertNotEquals($oldCompanyName, $newCompanyName);
        $this->assertEquals(dibi::select('*')->from(TBL_FORM_SUBMISSIONS)->count(), 1);
        $this->assertEquals(dibi::select('*')->from(TBL_COMPANY_INCORPORATIONS)->count(), 1);
        $this->assertEquals(dibi::select('*')->from(TBL_INCORPORATION_MEMBERS)->count(), 8);
        $this->assertEquals(dibi::select('*')->from(TBL_DOCUMENTS)->count(), 1);

        $newCompany = $this->object->copyCompany($customer, $oldCompany, $newCompanyName);

        $this->assertEquals($newCompanyName, $newCompany->getCompanyName());
        $this->assertEquals($oldCompanyName, $oldCompany->getCompanyName());
        $this->assertEquals(dibi::select('*')->from(TBL_FORM_SUBMISSIONS)->count(), 2);
        $this->assertEquals(dibi::select('*')->from(TBL_COMPANY_INCORPORATIONS)->count(), 2);
        $this->assertEquals(dibi::select('*')->from(TBL_INCORPORATION_MEMBERS)->count(), 16);
        $this->assertEquals(dibi::select('*')->from(TBL_DOCUMENTS)->count(), 1);
    }

    /**
     * @covers Services\Facades\CompanyCloneFacade::copyOrder
     */
    public function testCopyOrder()
    {
        $customer = EntityHelper::createCustomer();
        $oldOrder = EntityHelper::createOrder($customer);
        $oldCompany = EntityHelper::createCompany($customer, $oldOrder);
        $newCompany = EntityHelper::createCompany($customer);
        $this->assertEquals(2, $oldOrder->getItems()->count());
        $this->assertEquals(NULL, $newCompany->getOrder());

        $newOrder = $this->object->copyOrder($oldOrder, $newCompany);

        $this->assertEquals($newOrder->getId(), $newCompany->getOrder()->getId());
        $this->assertEquals($oldOrder->getId(), $oldCompany->getOrder()->getId());
        $this->assertEquals(2, $oldOrder->getItems()->count());
        $this->assertEquals(2, $newOrder->getItems()->count());
        foreach ($oldOrder->getItems() as $item) {
            $this->assertEquals($oldCompany->getId(), $item->getCompany()->getId());
        }
        foreach ($newOrder->getItems() as $item) {
            $this->assertEquals($newCompany->getId(), $item->getCompany()->getId());
        }
    }

    /**
     * @covers Services\Facades\CompanyCloneFacade::copyFullCompany
     */
    public function testCopyFullCompany()
    {
        $customer = EntityHelper::createCustomer();
        $oldOrder = EntityHelper::createOrder($customer);
        $oldCompany = EntityHelper::createCompanyToIncorporate($customer, $oldOrder);
        $oldCompanyName = $oldCompany->getCompanyName();
        $newCompanyName = 'testName';
        $this->assertNotEquals($oldCompanyName, $newCompanyName);
        $this->assertEquals(dibi::select('*')->from(TBL_FORM_SUBMISSIONS)->count(), 1);
        $this->assertEquals(dibi::select('*')->from(TBL_COMPANY_INCORPORATIONS)->count(), 1);
        $this->assertEquals(dibi::select('*')->from(TBL_INCORPORATION_MEMBERS)->count(), 8);
        $this->assertEquals(dibi::select('*')->from(TBL_DOCUMENTS)->count(), 1);

        $newCompany = $this->object->copyFullCompany($customer, $oldCompany, $newCompanyName);

        $this->assertEquals($newCompanyName, $newCompany->getCompanyName());
        $this->assertEquals($oldCompanyName, $oldCompany->getCompanyName());
        $this->assertEquals(dibi::select('*')->from(TBL_FORM_SUBMISSIONS)->count(), 2);
        $this->assertEquals(dibi::select('*')->from(TBL_COMPANY_INCORPORATIONS)->count(), 2);
        $this->assertEquals(dibi::select('*')->from(TBL_INCORPORATION_MEMBERS)->count(), 16);
        $this->assertEquals(dibi::select('*')->from(TBL_DOCUMENTS)->count(), 2);
    }

    /**
     * @covers Services\Facades\CompanyCloneFacade::copyCompany
     * @expectedException \Exceptions\Business\MissingSubmission
     */
    public function testCopyCompanyNoIncorporationType()
    {
        $customer = EntityHelper::createCustomer();
        $oldCompany = EntityHelper::createCompanyToIncorporate($customer);
        $oldCompanyName = $oldCompany->getCompanyName();
        $newCompanyName = 'testName';
        $this->assertNotEquals($oldCompanyName, $newCompanyName);
        dibi::update(TBL_FORM_SUBMISSIONS, array('form_identifier' => FormSubmission::TYPE_ANNUAL_RETURN))->execute();
        $this->assertEquals(dibi::select('*')->from(TBL_FORM_SUBMISSIONS)->count(), 1);
        $this->assertEquals(dibi::select('*')->from(TBL_COMPANY_INCORPORATIONS)->count(), 1);
        $this->assertEquals(dibi::select('*')->from(TBL_INCORPORATION_MEMBERS)->count(), 8);
        $this->assertEquals(dibi::select('*')->from(TBL_DOCUMENTS)->count(), 1);

        $this->object->copyCompany($customer, $oldCompany, $newCompanyName);
    }

}
