<?php

namespace Emailers;

use Entities\Company;
use Entities\Order;
use Entities\Customer;
use Entities\OrderItem;
use Entities\Service;
use IEmailService;
use Models\View\ServiceView;
use PHPUnit_Framework_MockObject_MockObject;
use RegisterOffice;
use Factories\TemplateFactory;
use PHPUnit_Framework_TestCase;
use FEmail;
use Models\View\Front\CompanyServicesEmailView;
use Models\View\Front\CompanyServicesView;
use Symfony\Component\Routing\Router;
use tests\helpers\MockHelper;

class ServicesEmailerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var ServicesEmailer
     */
    protected $servicesEmailer;

    /**
     * @var IEmailService
     */
    protected $emailService;

    /**
     * @var TemplateFactory
     */
    protected $template;

    /**
     * @var array
     */
    protected $services;

    /**
     * @var FEmail
     */
    protected $emailMock;

    /**
     * @var CompanyServicesView
     */
    protected $companyServicesViewMock;

    /**
     * @var ServiceView|PHPUnit_Framework_MockObject_MockObject
     */
    protected $serviceViewMock;

    /**
     * @var Router|PHPUnit_Framework_MockObject_MockObject
     */
    protected $routerMock;

    /**
     * @var array
     */
    protected $emails = array(
        ServicesEmailer::SERVICES_EXPIRED_15_AGO_EMAIL,
        ServicesEmailer::SERVICES_EXPIRED_EMAIL,
        ServicesEmailer::SERVICES_EXPIRED_28_AGO_EMAIL,
        ServicesEmailer::SERVICES_EXPIRES_IN_15_EMAIL,
        ServicesEmailer::SERVICES_EXPIRES_IN_1_EMAIL,
        ServicesEmailer::SERVICES_EXPIRES_IN_28_EMAIL
    );

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->emailService = $this->getMock('IEmailService');
        $this->template = TemplateFactory::createLatteTemplate();
        $this->routerMock = MockHelper::mock($this, 'Symfony\Component\Routing\RouterInterface');
        $this->servicesEmailer = new ServicesEmailer($this->emailService, $this->template, $this->routerMock);
        $this->emailMock = $this->getMock('\FEmail');
        $this->serviceViewMock = MockHelper::mock($this, 'Models\View\ServiceView');
        $this->companyServicesViewMock = MockHelper::mock($this, 'Models\View\Front\CompanyServicesView');

        $this->companyServicesViewMock->expects($this->any())->method('hasServicesExpiresIn28')->willReturn(TRUE);
        $this->companyServicesViewMock->expects($this->any())->method('hasServicesExpiresIn15')->willReturn(TRUE);
        $this->companyServicesViewMock->expects($this->any())->method('hasServicesExpiresIn1')->willReturn(TRUE);
        $this->companyServicesViewMock->expects($this->any())->method('hasServicesExpired')->willReturn(TRUE);
        $this->companyServicesViewMock->expects($this->any())->method('hasServicesExpired15Days')->willReturn(TRUE);
        $this->companyServicesViewMock->expects($this->any())->method('hasServicesExpired28Days')->willReturn(TRUE);

        $this->companyServicesViewMock->expects($this->any())->method('getServicesExpiresIn28')->willReturn([$this->serviceViewMock]);
        $this->companyServicesViewMock->expects($this->any())->method('getServicesExpiresIn15')->willReturn([$this->serviceViewMock]);
        $this->companyServicesViewMock->expects($this->any())->method('getServicesExpiresIn1')->willReturn([$this->serviceViewMock]);
        $this->companyServicesViewMock->expects($this->any())->method('getServicesExpired')->willReturn([$this->serviceViewMock]);
        $this->companyServicesViewMock->expects($this->any())->method('getServicesExpired15Days')->willReturn([$this->serviceViewMock]);
        $this->companyServicesViewMock->expects($this->any())->method('getServicesExpired28Days')->willReturn([$this->serviceViewMock]);

        $this->companyServicesViewMock->expects($this->any())->method('getId')->willReturn(1);
        $this->companyServicesViewMock->expects($this->any())->method('getCompanyName')
            ->will(
                $this->onConsecutiveCalls(
                    'CompanyName1',
                    'CompanyName2',
                    'CompanyName3',
                    'CompanyName4',
                    'CompanyName5',
                    'CompanyName6'
                )
            );
    }

    public function testTemplate()
    {
        $customer = new Customer('test', 'test');
        $company = new Company($customer, 'Testing Company Name');
        $orderItem = new OrderItem(new Order($customer));
        $service = new Service(Service::TYPE_PACKAGE_PRIVACY, new RegisterOffice(165), $company, $orderItem);
        $this->template->setFile(DOCUMENT_ROOT . '/tests/fixtures/servicesEmailer/testTemplate.latte');
        $this->template->service = $service;
        $this->assertEquals(Service::TYPE_PACKAGE_PRIVACY, $this->template);
    }

    public function testAllServicesEmails()
    {
        $companyMock = MockHelper::mock($this, 'Entities\Company');
        $customerMock = MockHelper::mock($this, 'Entities\Customer');

        $companyMock->expects($this->any())->method('getCustomer')->willReturn($customerMock);
        $this->companyServicesViewMock->expects($this->any())->method('getCompany')->willReturn($companyMock);

        $companyServicesEmailView = new CompanyServicesEmailView(
            $this->emailMock,
            $this->companyServicesViewMock,
            [$this->serviceViewMock],
            28
        );

        $emailBodies = [];
        $this->emailService->expects($this->exactly(6))->method('send')
            ->will(
                $this->returnCallback(
                    function($email) use (&$emailBodies)
                    {
                        $emailBodies[] = $email->body->emailView->getCompanyServicesView()->getCompanyName();
                    }
                )
            );

        $this->servicesEmailer->sendEmailExpiration(
            $companyServicesEmailView,
            $this->emailMock,
            '/../../../tests/fixtures/servicesEmailer/serviceEmail.latte'
        );

        $this->servicesEmailer->sendEmailExpiration(
            $companyServicesEmailView,
            $this->emailMock,
            '/../../../tests/fixtures/servicesEmailer/serviceEmail.latte'
        );
        $this->servicesEmailer->sendEmailExpiration(
            $companyServicesEmailView,
            $this->emailMock,
            '/../../../tests/fixtures/servicesEmailer/serviceEmail.latte'
        );
        $this->servicesEmailer->sendEmailExpiration(
            $companyServicesEmailView,
            $this->emailMock,
            '/../../../tests/fixtures/servicesEmailer/serviceEmail.latte'
        );
        $this->servicesEmailer->sendEmailExpiration(
            $companyServicesEmailView,
            $this->emailMock,
            '/../../../tests/fixtures/servicesEmailer/serviceEmail.latte'
        );
        $this->servicesEmailer->sendEmailExpiration(
            $companyServicesEmailView,
            $this->emailMock,
            '/../../../tests/fixtures/servicesEmailer/serviceEmail.latte'
        );

        $this->assertEquals(
            ['CompanyName1', 'CompanyName2', 'CompanyName3', 'CompanyName4', 'CompanyName5', 'CompanyName6'],
            $emailBodies
        );
    }

}
