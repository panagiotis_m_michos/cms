<?php

use Tests\Helpers\ObjectHelper;

class OrderEmailerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var OrderEmailer
     */
    private $object;

    /**
     * @var IEmailService|PHPUnit_Framework_MockObject_MockObject
     */
    private $emailServiceMock;

    protected function setUp()
    {
        $this->emailServiceMock = $this->getMockBuilder('IEmailService')->disableOriginalConstructor()->getMock();
        $this->object = new OrderEmailer($this->emailServiceMock);
    }

    /**
     * @test
     * @covers       OrderEmailer::sendCustomerRefundEmail
     * @dataProvider dataProvider
     */
    public function sendCustomerRefundEmail($creditRefundValue, $moneyRefundValue)
    {
        /** @var FEmail $emailObject */
        $emailObject = NULL;
        $this->emailServiceMock
            ->expects($this->once())
            ->method('send')
            ->will(
                $this->returnCallback(
                    function ($email) use (&$emailObject) {
                        $emailObject = $email;
                    }
                )
            );

        $customer = ObjectHelper::createCustomer(TEST_EMAIL1);
        $order = ObjectHelper::createOrder($customer);
        $this->object->sendCustomerRefundEmail($order, $creditRefundValue, $moneyRefundValue);

        $body = html_entity_decode($emailObject->body);
        $this->assertContains('Hello ' . $customer->getFirstName(), $body);
        $this->assertContains('Order Id: ' . $order->getId(), $body);

        if ($creditRefundValue) {
            $this->assertContains('Credit refund amount: £' . $creditRefundValue, $body);
        }

        if ($moneyRefundValue) {
            $this->assertContains('Money refund amount: £' . $moneyRefundValue, $body);
        }
    }

    public function dataProvider()
    {
        return array(
            array(10, 20),
            array(0, 20),
            array(10, 0),
        );
    }
}
