<?php

namespace Services;

use Entities\Customer;
use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;
use Repositories\CustomerRepository;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Tests\Helpers\ObjectHelper;

class CustomerServiceTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var CustomerRepository|PHPUnit_Framework_MockObject_MockObject
     */
    private $customerRepositoryMock;

    /**
     * @var EventDispatcher|PHPUnit_Framework_MockObject_MockObject
     */
    private $dispatcherMock;

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var CustomerService
     */
    private $object;

    protected function setUp()
    {
        $this->customerRepositoryMock = $this->getMockBuilder('Repositories\CustomerRepository')->disableOriginalConstructor()->getMock();
        $this->dispatcherMock = $this->getMockBuilder('Symfony\Component\EventDispatcher\EventDispatcher')->disableOriginalConstructor()->getMock();

        $this->customer = ObjectHelper::createCustomer(TEST_EMAIL1);
        $this->object = new CustomerService($this->customerRepositoryMock, $this->dispatcherMock);
    }

    /**
     * @test
     * @covers Services\CustomerService::addCredit()
     */
    public function addCredit()
    {
        $this->customerRepositoryMock->expects($this->once())->method('saveEntity');
        $this->object->addCredit($this->customer, 10);
    }



}
