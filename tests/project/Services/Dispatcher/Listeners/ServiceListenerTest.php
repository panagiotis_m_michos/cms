<?php

namespace Dispatcher\Listeners;

use Dispatcher\Events\Order\RefundEvent;
use Dispatcher\Events\ServicePurchasedEvent;
use PHPUnit_Framework_MockObject_MockObject;
use ServiceActivatorModule\ServiceActivator;
use Services\ServiceService;
use Entities\Service;
use EventLocator;
use PHPUnit_Framework_TestCase;
use Symfony\Component\EventDispatcher\EventDispatcher;
use tests\helpers\MockHelper;
use Tests\Helpers\ObjectHelper;

class ServiceListenerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var ServiceListener
     */
    protected $object;

    /**
     * @var ServiceService|PHPUnit_Framework_MockObject_MockObject
     */
    private $serviceServiceMock;

    /**
     * @var ServiceActivator|PHPUnit_Framework_MockObject_MockObject
     */
    private $serviceActivatorMock;

    /**
     * @var EventDispatcher|PHPUnit_Framework_MockObject_MockObject
     */
    private $dispatcherMock;

    protected function setUp()
    {
        $this->serviceServiceMock = MockHelper::mock($this, 'Services\ServiceService');
        $this->serviceActivatorMock = MockHelper::mock($this, 'ServiceActivatorModule\ServiceActivator');
        $this->dispatcherMock = MockHelper::mock($this, 'Symfony\Component\EventDispatcher\EventDispatcher');

        $this->object = new ServiceListener($this->serviceServiceMock, $this->serviceActivatorMock);
    }


    /**
     * @covers Dispatcher\Listeners\ServiceListener::onServicePurchased
     */
    public function testOnPurchasedIncorporated()
    {
        $service = ObjectHelper::createService();
        $service->getCompany()->setCompanyNumber('a');
        $this->serviceActivatorMock->expects($this->once())->method('activate')->with($service);
        $serviceEvent = new ServicePurchasedEvent($service);
        $this->object->onServicePurchased($serviceEvent);
    }

    /**
     * @covers Dispatcher\Listeners\ServiceListener::onServicePurchased
     */
    public function testOnPurchasedNotIncorporated()
    {
        $service = ObjectHelper::createService();
        $this->serviceActivatorMock->expects($this->never())->method('activate');
        $serviceEvent = new ServicePurchasedEvent($service);
        $this->object->onServicePurchased($serviceEvent);
    }

    /****************************************** onOrderRefunded ******************************************/

    /**
     * @test
     * @covers Dispatcher\Listeners\ServiceListener::onOrderRefunded
     */
    public function onOrderRefunded()
    {
        $customer = ObjectHelper::createCustomer();
        $order = ObjectHelper::createOrder($customer);
        $company = ObjectHelper::createCompany($customer);

        $orderItem1 = ObjectHelper::createOrderItem($order);
        $event = new RefundEvent($order, [$orderItem1], 0, 0);

        $this->serviceServiceMock->expects($this->once())
            ->method('getOrderItemService')
            ->willReturn(ObjectHelper::createService($company, NULL, NULL, TRUE, Service::TYPE_REGISTERED_OFFICE, $order));

        $this->serviceServiceMock->expects($this->once())->method('deleteService');

        $this->object->onOrderRefunded($event, EventLocator::ORDER_REFUNDED, $this->dispatcherMock);
    }

    /**
     * @test
     * @covers Dispatcher\Listeners\ServiceListener::onOrderRefunded
     */
    public function onOrderRefundedNoService()
    {
        $customer = ObjectHelper::createCustomer();
        $order = ObjectHelper::createOrder($customer);

        $company1 = ObjectHelper::createCompany($customer);

        $orderItem1 = ObjectHelper::createOrderItem($order);
        $orderItem1->setCompany($company1);

        $this->serviceServiceMock->expects($this->exactly(1))->method('getOrderItemService');
        $this->serviceServiceMock->expects($this->exactly(0))->method('deleteService');

        $event = new RefundEvent($order, [$orderItem1], 0, 0);
        $this->object->onOrderRefunded($event, EventLocator::ORDER_REFUNDED, $this->dispatcherMock);
    }

}
