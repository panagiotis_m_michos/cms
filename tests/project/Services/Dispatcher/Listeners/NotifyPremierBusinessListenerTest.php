<?php

namespace project\services\Dispatcher\Listeners;

use Dispatcher\Listeners\NotifyPremierBusinessListener;
use PHPUnit_Framework_MockObject_MockObject;
use \PHPUnit_Framework_TestCase;
use PremierBusinessEmailer;
use Psr\Log\LoggerInterface;
use Services\CompanyService;
use Services\PackageService;

class NotifyPremierBusinessListenerTest extends PHPUnit_Framework_TestCase
{
    private $companyEvent;

    /**
     * @var LoggerInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $logger;

    /**
     * @var PremierBusinessEmailer|PHPUnit_Framework_MockObject_MockObject
     */
    private $emailer;

    /**
     * @var CompanyService|PHPUnit_Framework_MockObject_MockObject
     */
    private $companyService;

    /**
     * @var PackageService|PHPUnit_Framework_MockObject_MockObject
     */
    private $packageService;

    /**
     * @var NotifyPremierBusinessListener|PHPUnit_Framework_MockObject_MockObject
     */
    private $listener;

    public function setUp()
    {
        $this->packageService = $this->getMockBuilder('Services\PackageService')
            ->disableOriginalConstructor()
            ->getMock();

        $this->companyService = $this->getMockBuilder('Services\CompanyService')
            ->disableOriginalConstructor()
            ->getMock();

        $this->emailer = $this->getMockBuilder('PremierBusinessEmailer')
            ->disableOriginalConstructor()
            ->getMock();

        $this->logger = $this->getMockBuilder('Psr\Log\LoggerInterface')
            ->disableOriginalConstructor()
            ->getMock();

        $this->companyEvent = $this->getMockBuilder('Dispatcher\Events\CompanyEvent')
            ->disableOriginalConstructor()
            ->getMock();

        $this->listener = new NotifyPremierBusinessListener(
            $this->packageService,
            $this->companyService,
            $this->emailer,
            $this->logger
        );
    }

    public function testWillSendMailForInternationalCompany()
    {
        $companyMock = $this->getMockBuilder('Entities\Company')
            ->disableOriginalConstructor()
            ->getMock();

        $packageMock = $this->getMockBuilder('Package')
            ->disableOriginalConstructor()
            ->getMock();

        $this->companyService->method('getCompanySummaryByCompany')->willReturn(new CompanySummaryDouble());
        $this->companyEvent->method('getCompany')->willReturn($companyMock);
        $packageMock->method('isInternational')->willReturn(TRUE);
        $this->packageService->method('getPackageById')->willReturn($packageMock);
        $this->companyService->method('getCompanyById')->willReturn($companyMock);

        $this->emailer->expects($this->once())->method('sendInternationalCompanyIncorporated')->willReturn(NULL);
        $this->listener->onIncorporated($this->companyEvent);
    }
}

class CompanySummaryDouble
{
    public function getCompanySummaryPdfString()
    {
        return '';
    }
}
