<?php

namespace Dispatcher\Listeners;

use Tests\Helpers\ObjectHelper;
use Dispatcher\Events\TokenEvent;
use Emailers\TokenEmailer;
use Entities\Customer;

/**
 * Generated by PHPUnit_SkeletonGenerator 1.2.1 on 2014-05-22 at 12:30:58.
 */
class TokenEmailListenerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var TokenEmailListener
     */
    protected $object;

    /**
     * @var TokenEmailer 
     */
    private $tokenEmailerMock;

    /**
     * @var Customer
     */
    private $customer;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->tokenEmailerMock = $this->getMockBuilder('Emailers\TokenEmailer')->disableOriginalConstructor()->getMock();
        $this->object = new TokenEmailListener($this->tokenEmailerMock);
        $this->customer = ObjectHelper::createCustomer();
    }

    /**
     * @covers Dispatcher\Listeners\TokenEmailListener::onExpires
     */
    public function testOnExpires()
    {
        $token = ObjectHelper::createToken($this->customer);
        $this->tokenEmailerMock->expects($this->once())
            ->method('sendEmailSoonExpires')
            ->with($token);
        $this->object->onExpires(new TokenEvent($token, $this->getMock('ILogger')));
    }

    /**
     * @covers Dispatcher\Listeners\TokenEmailListener::onExpired
     */
    public function testOnExpired()
    {
        $token = ObjectHelper::createToken($this->customer);
        $this->tokenEmailerMock->expects($this->once())
            ->method('sendEmailExpired')
            ->with($token);
        $this->object->onExpired(new TokenEvent($token, $this->getMock('ILogger')));
    }
}
