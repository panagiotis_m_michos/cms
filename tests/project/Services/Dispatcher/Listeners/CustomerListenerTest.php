<?php

namespace Dispatcher\Listeners;

use Dispatcher\Events\Order\RefundEvent;
use Entities\Customer;
use Entities\Order;
use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;
use Services\CustomerService;
use Tests\Helpers\ObjectHelper;

class CustomerListenerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var CustomerListener
     */
    private $object;

    /**
     * @var CustomerService|PHPUnit_Framework_MockObject_MockObject
     */
    private $customerServiceMock;

    /**
     * @var RefundEvent|PHPUnit_Framework_MockObject_MockObject
     */
    private $refundEventMock;

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var Order
     */
    private $order;

    protected function setUp()
    {
        $this->customerServiceMock = $this->getMockBuilder('Services\CustomerService')->disableOriginalConstructor()->getMock();
        $this->refundEventMock = $this->getMockBuilder('Dispatcher\Events\Order\RefundEvent')->disableOriginalConstructor()->getMock();

        $this->customer = ObjectHelper::createCustomer(TEST_EMAIL1);
        $this->order = ObjectHelper::createOrder($this->customer, 0);

        $this->object = new CustomerListener($this->customerServiceMock);
    }

    /**
     * @dataProvider dataProvider
     * @covers Dispatcher\Listeners\CustomerListener::onOrderRefunded()
     */
    public function testOnOrderRefunded($totalCreditRefund, $exactly)
    {
        $this->refundEventMock->expects($this->once())->method('getOrder')->willReturn($this->order);
        $this->refundEventMock->expects($this->once())->method('getTotalCreditRefund')->willReturn($totalCreditRefund);
        $this->customerServiceMock->expects($this->exactly($exactly))->method('addCredit')->with($this->customer, $totalCreditRefund);

        $this->object->onOrderRefunded($this->refundEventMock);
    }

    public function dataProvider()
    {
        return array(
            array(100, 1), // credit to refund
            array(0, 0), // no credit
            array(-10, 0), // negative credit
        );
    }

}
