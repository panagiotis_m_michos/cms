<?php

namespace Dispatcher\Listeners;

use Dispatcher\Events\Order\RefundEvent;
use Package;
use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;
use Product;
use Services\CompanyService;
use Services\PackageService;
use Services\ServiceService;
use tests\helpers\MockHelper;
use Tests\Helpers\ObjectHelper;

class DeleteUnformedCompanyListenerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var DeleteUnformedCompanyListener
     */
    private $object;

    /**
     * @var ServiceService|PHPUnit_Framework_MockObject_MockObject
     */
    private $serviceServiceMock;

    /**
     * @var CompanyService|PHPUnit_Framework_MockObject_MockObject
     */
    private $companyServiceMock;

    /**
     * @var PackageService|PHPUnit_Framework_MockObject_MockObject
     */
    private $packageServiceMock;

    protected function setUp()
    {
        $this->serviceServiceMock = MockHelper::mock($this, 'Services\ServiceService');
        $this->companyServiceMock = MockHelper::mock($this, 'Services\CompanyService');
        $this->packageServiceMock = MockHelper::mock($this, 'Services\PackageService');

        $this->object = new DeleteUnformedCompanyListener($this->serviceServiceMock, $this->companyServiceMock, $this->packageServiceMock);
    }

    public function testOnOrderRefunded1()
    {
        $service = ObjectHelper::createService();

        $companyEntityMock = $this->getMockBuilder('Entities\Company')->disableOriginalConstructor()->getMock();
        $companyEntityMock->expects($this->once())->method('isIncorporated')->willReturn(FALSE);

        $this->packageServiceMock->expects($this->once())->method('getPackageById')->willReturn(new Package());

        $this->companyServiceMock->expects($this->once())->method('getCompanyByOrder')->willReturn($companyEntityMock);
        $this->companyServiceMock->expects($this->once())->method('isCompanyIncorporationPending')->willReturn(FALSE);
        $this->companyServiceMock->expects($this->once())->method('deleteCompany')->with($companyEntityMock);

        $this->serviceServiceMock->expects($this->once())->method('getOrderItemService')->willReturn($service);
        $this->serviceServiceMock->expects($this->once())->method('deleteService')->with($service);

        $this->object->onOrderRefunded(self::getRefundEvent());
    }

    public function testOnOrderRefundedNoPackage()
    {
        $companyEntityMock = $this->getMockBuilder('Entities\Company')->disableOriginalConstructor()->getMock();

        $this->packageServiceMock->expects($this->once())->method('getPackageById')->willReturn(NULL);

        $this->companyServiceMock->expects($this->once())->method('getCompanyByOrder')->willReturn($companyEntityMock);
        $this->companyServiceMock->expects($this->never())->method('deleteCompany');

        $this->serviceServiceMock->expects($this->never())->method('getOrderItemService');
        $this->serviceServiceMock->expects($this->never())->method('deleteService');

        $this->object->onOrderRefunded(self::getRefundEvent());
    }

    public function testOnOrderRefundedIncorporatedCompany()
    {
        $companyEntityMock = $this->getMockBuilder('Entities\Company')->disableOriginalConstructor()->getMock();
        $companyEntityMock->expects($this->once())->method('isIncorporated')->willReturn(TRUE);

        $this->packageServiceMock->expects($this->once())->method('getPackageById')->willReturn(new Package());

        $this->companyServiceMock->expects($this->once())->method('getCompanyByOrder')->willReturn($companyEntityMock);
        $this->companyServiceMock->expects($this->never())->method('deleteCompany');

        $this->serviceServiceMock->expects($this->never())->method('getOrderItemService');
        $this->serviceServiceMock->expects($this->never())->method('deleteService');

        $this->object->onOrderRefunded(self::getRefundEvent());
    }

    public function testOnOrderRefundedNoService()
    {
        $companyEntityMock = $this->getMockBuilder('Entities\Company')->disableOriginalConstructor()->getMock();
        $companyEntityMock->expects($this->once())->method('isIncorporated')->willReturn(FALSE);

        $this->packageServiceMock->expects($this->once())->method('getPackageById')->willReturn(new Package());

        $this->companyServiceMock->expects($this->once())->method('getCompanyByOrder')->willReturn($companyEntityMock);
        $this->companyServiceMock->expects($this->once())->method('isCompanyIncorporationPending')->willReturn(FALSE);
        $this->companyServiceMock->expects($this->once())->method('deleteCompany')->with($companyEntityMock);

        $this->serviceServiceMock->expects($this->once())->method('getOrderItemService')->willReturn(NULL);
        $this->serviceServiceMock->expects($this->never())->method('deleteService');

        $this->object->onOrderRefunded(self::getRefundEvent());
    }

    public function testOnOrderRefundedPendingIncorporation()
    {
        $companyEntityMock = $this->getMockBuilder('Entities\Company')->disableOriginalConstructor()->getMock();
        $companyEntityMock->expects($this->once())->method('isIncorporated')->willReturn(FALSE);

        $this->packageServiceMock->expects($this->once())->method('getPackageById')->willReturn(new Package());

        $this->companyServiceMock->expects($this->once())->method('getCompanyByOrder')->willReturn($companyEntityMock);
        $this->companyServiceMock->expects($this->once())->method('isCompanyIncorporationPending')->willReturn(TRUE);
        $this->companyServiceMock->expects($this->never())->method('deleteCompany');

        $this->serviceServiceMock->expects($this->never())->method('getOrderItemService');
        $this->serviceServiceMock->expects($this->never())->method('deleteService');

        $this->object->onOrderRefunded(self::getRefundEvent());
    }

    /**
     * @param array $items
     * @return RefundEvent
     */
    private static function getRefundEvent($items = array())
    {
        $order = ObjectHelper::createOrder(ObjectHelper::createCustomer());
        if (!$items) {
            $items[] = ObjectHelper::createOrderItem($order);
        }
        return new RefundEvent($order, $items, 0, 0);
    }

}
