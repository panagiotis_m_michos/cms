<?php

namespace Cron\Commands\Reminders;

use Dispatcher\Events\TokenEvent;
use Entities\Customer;
use EventLocator;
use PHPUnit_Framework_TestCase;
use Services\EventService;
use Services\Payment\TokenService;
use Tests\Helpers\ObjectHelper;
use Utils\Date;

class TokensTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Tokens
     */
    protected $object;

    /**
     * @var TokenService 
     */
    protected $tokenServiceMock;

    /**
     * @var EventService 
     */
    protected $eventServiceMock;

    /**
     * @var Customer
     */
    protected $customer;

    protected function setUp()
    {
        $this->tokenServiceMock = $this->getMockBuilder('Services\Payment\TokenService')
            ->setMethods(array('getTokensForCardReminders'))
            ->disableOriginalConstructor()->getMock();
        $this->eventServiceMock = $this->getMockBuilder('Services\EventService')
            ->setMethods(array('dispatch'))
            ->disableOriginalConstructor()->getMock();
        $this->object = new Tokens(
            $this->getMock('ILogger'),
            $this->getMock('Cron\INotifier'),
            $this->eventServiceMock,
            $this->tokenServiceMock, 
            $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock()
        );

        $this->customer = self::getCustomerWithActiveServices();
    }

    /**
     * @covers Cron\Commands\Reminders\Tokens::execute
     */
    public function testExecute()
    {
        $returnedTokens = array();
        $tokens[][] = ObjectHelper::createToken($this->customer, TRUE, new Date('-8 days'), TRUE);
        $tokens[][] = ObjectHelper::createToken($this->customer, TRUE, new Date('-1 days'), TRUE);
        $tokens[][] = ObjectHelper::createToken($this->customer, TRUE, new Date('+1 days'), TRUE);
        $tokens[][] = ObjectHelper::createToken($this->customer, TRUE, new Date('+8 days'), TRUE);

        $this->tokenServiceMock->expects($this->once())
            ->method('getTokensForCardReminders')
            ->will($this->returnValue($tokens));

        $this->eventServiceMock->expects($this->exactly(3))
            ->method('dispatch')
            ->will(
                $this->returnCallback(
                    function($key, $objectId, TokenEvent $tokenEvent) use (&$returnedTokens) {
                        $returnedTokens[][] = $tokenEvent->getToken();
                    }
                )
            );
        $this->object->execute();

        $this->assertEquals(array_slice($tokens, 0, 3), $returnedTokens);
    }

    /**
     * @covers Cron\Commands\Reminders\Tokens::processExpiryDate
     */
    public function testProcessExpiryDateSevenDaysAgo()
    {
        $token = ObjectHelper::createToken($this->customer, TRUE, new Date('-7 days'), TRUE);
        $this->eventServiceMock->expects($this->once())
            ->method('dispatch')
            ->with(EventLocator::TOKEN_EMAIL_EXPIRED, $token->getId());
        $this->object->processExpiryDate($token);
    }

    /**
     * @covers Cron\Commands\Reminders\Tokens::processExpiryDate
     */
    public function testProcessExpiryDateOneDayAgo()
    {
        $token = ObjectHelper::createToken($this->customer, TRUE, new Date('-1 days'), TRUE);
        $this->eventServiceMock->expects($this->once())
            ->method('dispatch')
            ->with(EventLocator::TOKEN_EMAIL_EXPIRED, $token->getId());
        $this->object->processExpiryDate($token);
    }

    /**
     * @covers Cron\Commands\Reminders\Tokens::processExpiryDate
     */
    public function testProcessExpiryDateToday()
    {
        $token = ObjectHelper::createToken($this->customer, TRUE, new Date('today'), TRUE);
        $this->eventServiceMock->expects($this->once())
            ->method('dispatch')
            ->with(EventLocator::TOKEN_EMAIL_SOON_EXPIRES, $token->getId());
        $this->object->processExpiryDate($token);
    }

    /**
     * @covers Cron\Commands\Reminders\Tokens::processTokenReminders
     */
    public function testProcessTokenRemindersNonActiveService()
    {
        $returnedTokens = array();
        $tokens[][] = ObjectHelper::createToken(self::getCustomerWithNonActiveServices(), TRUE, new Date('today'), TRUE);

        $this->eventServiceMock->expects($this->never())
            ->method('dispatch')
            ->will(
                $this->returnCallback(
                    function ($key, $objectId, TokenEvent $tokenEvent) use (&$returnedTokens) {
                        $returnedTokens[][] = $tokenEvent->getToken();
                    }
                )
            );
        $this->object->processTokenReminders($tokens);

        $this->assertEmpty($returnedTokens);
    }

    /****************************************** private ******************************************/

    /**
     * @return Customer
     */
    private static function getCustomerWithActiveServices()
    {
        $customer = ObjectHelper::createCustomer();
        $company = ObjectHelper::createCompany($customer);
        $company->addService(ObjectHelper::createService(NULL, new Date('today'), new Date('+1 year')));
        $customer->addCompany($company);

        return $customer;
    }

    /**
     * @return Customer
     */
    private static function getCustomerWithNonActiveServices()
    {
        $customer = ObjectHelper::createCustomer();
        $company = ObjectHelper::createCompany($customer);
        $company->addService(ObjectHelper::createService(NULL, new Date("-1 year"), new Date("-2 months")));
        $customer->addCompany($company);

        return $customer;
    }
}
