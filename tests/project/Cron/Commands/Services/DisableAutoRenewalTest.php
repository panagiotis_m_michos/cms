<?php

namespace Cron\Commands\Services;

use Doctrine\ORM\EntityManager;
use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;
use Services\ServiceService;
use ServiceSettingsModule\Services\ServiceSettingsService;
use tests\helpers\IteratorHelper;
use tests\helpers\MockHelper;
use Tests\Helpers\ObjectHelper;

class DisableAutoRenewalTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var DisableAutoRenewal
     */
    private $object;

    /**
     * @var ServiceService|PHPUnit_Framework_MockObject_MockObject
     */
    private $serviceServiceMock;

    /**
     * @var ServiceSettingsService|PHPUnit_Framework_MockObject_MockObject
     */
    private $serviceSettingsServiceMock;

    /**
     * @var EntityManager|PHPUnit_Framework_MockObject_MockObject
     */
    private $entityManagerMock;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->serviceServiceMock = $this->getMockBuilder('Services\ServiceService')->disableOriginalConstructor()->getMock();
        $this->serviceSettingsServiceMock = MockHelper::mock($this, 'ServiceSettingsModule\Services\ServiceSettingsService');
        $this->entityManagerMock = $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock();

        $this->object = new DisableAutoRenewal(
            $this->getMock('ILogger'),
            $this->getMock('Cron\INotifier'),
            $this->serviceServiceMock,
            $this->serviceSettingsServiceMock,
            $this->entityManagerMock
        );
    }

    /**
     * @covers Cron\Commands\Services\DisableAutoRenewal::execute
     */
    public function testWillNotDisableAutoRenewalWithNoServices()
    {
        $this->serviceSettingsServiceMock->expects($this->once())->method('getSettingsWithEnabledAutoRenewal')
            ->willReturn(IteratorHelper::createDoctrine([]));

        $this->entityManagerMock->expects($this->once())->method('flush');
        $this->object->execute();
    }

    /**
     * @covers Cron\Commands\Services\DisableAutoRenewal::execute
     */
    public function testCanDisableOneService()
    {
        $companyMock = MockHelper::mock($this, 'Entities\Company');
        $companyMock->method('getId')->willReturn(1);

        $serviceMock = MockHelper::mock($this, 'Entities\Service');
        $serviceMock->method('getCompany')->willReturn($companyMock);
        $serviceMock->method('hasDates')->willReturn(TRUE);
        $serviceMock->method('getDaysAfterExpire')->willReturn(29);

        $serviceSettingsMock = MockHelper::mock($this, 'Entities\ServiceSettings');
        $serviceSettingsMock->expects($this->once())->method('setIsAutoRenewalEnabled')->with(FALSE);

        $this->serviceServiceMock->method('getLatestServiceBySettings')->with($serviceSettingsMock)->willReturn($serviceMock);

        $this->serviceSettingsServiceMock->method('getSettingsWithEnabledAutoRenewal')
            ->willReturn(IteratorHelper::createDoctrine([$serviceSettingsMock]));

        $this->entityManagerMock->expects($this->once())->method('flush');
        $this->object->execute();
    }

    /**
     * @covers Cron\Commands\Services\DisableAutoRenewal::execute
     */
    public function testCanDisableMultipleServices()
    {
        $companyMock = MockHelper::mock($this, 'Entities\Company');
        $companyMock->method('getId')->willReturn(1);

        $serviceMock1 = MockHelper::mock($this, 'Entities\Service');
        $serviceMock2 = MockHelper::mock($this, 'Entities\Service');
        $serviceMock3 = MockHelper::mock($this, 'Entities\Service');
        $serviceMock1->method('getCompany')->willReturn($companyMock);
        $serviceMock1->method('hasDates')->willReturn(TRUE);
        $serviceMock1->method('getDaysAfterExpire')->willReturn(28);
        $serviceMock2->method('getCompany')->willReturn($companyMock);
        $serviceMock2->method('hasDates')->willReturn(TRUE);
        $serviceMock2->method('getDaysAfterExpire')->willReturn(29);
        $serviceMock3->method('getCompany')->willReturn($companyMock);
        $serviceMock3->method('hasDates')->willReturn(TRUE);
        $serviceMock3->method('getDaysAfterExpire')->willReturn(50);

        $serviceSettingsMock1 = MockHelper::mock($this, 'Entities\ServiceSettings');
        $serviceSettingsMock2 = MockHelper::mock($this, 'Entities\ServiceSettings');
        $serviceSettingsMock3 = MockHelper::mock($this, 'Entities\ServiceSettings');
        $serviceSettingsMock1->expects($this->never())->method('setIsAutoRenewalEnabled')->with(FALSE);
        $serviceSettingsMock2->expects($this->once())->method('setIsAutoRenewalEnabled')->with(FALSE);
        $serviceSettingsMock3->expects($this->once())->method('setIsAutoRenewalEnabled')->with(FALSE);

        $this->serviceServiceMock->expects($this->exactly(3))->method('getLatestServiceBySettings')->willReturnOnConsecutiveCalls(
            $serviceMock1, $serviceMock2, $serviceMock3
        );

        $this->serviceSettingsServiceMock->method('getSettingsWithEnabledAutoRenewal')
            ->willReturn(IteratorHelper::createDoctrine([$serviceSettingsMock1, $serviceSettingsMock2, $serviceSettingsMock3]));

        $this->entityManagerMock->expects($this->once())->method('flush');
        $this->object->execute();
    }

}
