<?php

namespace Cron\Commands\Services;

use Emailers\ServicesEmailer;
use EventLocator;
use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;
use Services\EventService;
use Services\ServiceService;
use tests\helpers\IteratorHelper;
use Utils\Date;

class AutoRenewalReminderTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var AutoRenewalReminder
     */
    private $object;

    /**
     * @var ServiceService|PHPUnit_Framework_MockObject_MockObject
     */
    private $serviceServiceMock;

    /**
     * @var EventService|PHPUnit_Framework_MockObject_MockObject
     */
    private $eventServiceMock;

    /**
     * @var ServicesEmailer|PHPUnit_Framework_MockObject_MockObject
     */
    private $servicesEmailerMock;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->serviceServiceMock = $this->getMockBuilder('Services\ServiceService')->disableOriginalConstructor()->getMock();
        $this->eventServiceMock = $this->getMockBuilder('Services\EventService')->disableOriginalConstructor()->getMock();
        $this->servicesEmailerMock = $this->getMockBuilder('Emailers\ServicesEmailer')->disableOriginalConstructor()->getMock();

        $this->object = new AutoRenewalReminder(
            $this->getMock('ILogger'),
            $this->getMock('Cron\INotifier'),
            $this->serviceServiceMock,
            $this->eventServiceMock,
            $this->servicesEmailerMock
        );
    }

    /**
     * @covers Cron\Commands\Services\AutoRenewal::execute
     */
    public function testWillNotSendReminderWithNoExpiringServices()
    {
        $iterableResultMock = $this->getMockBuilder('Doctrine\ORM\Internal\Hydration\IterableResult')->disableOriginalConstructor()->getMock();
        $iterableResultMockSetup = IteratorHelper::mockIterator($this, $iterableResultMock, array());

        $this->serviceServiceMock->expects($this->once())->method('getServicesDataExpiringWithinDatesForEvent')
            ->with(new Date('+1 days'), new Date('+7 days'), EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_EXPIRES_IN_7)
            ->willReturn($iterableResultMockSetup);

        $this->servicesEmailerMock->expects($this->never())->method('sendEmailServiceAutoRenewalIn7');
        $this->object->execute();
    }

    /**
     * @covers Cron\Commands\Services\AutoRenewal::execute
     */
    public function testCanSendReminderToOneCustomerWithOneExpiringService()
    {
        $customerMock1 = $this->getMockBuilder('Entities\Customer')->disableOriginalConstructor()->getMock();
        $companyMock1 = $this->getMockBuilder('Entities\Company')->disableOriginalConstructor()->getMock();
        $serviceMock1 = $this->getMockBuilder('Entities\Service')->disableOriginalConstructor()->getMock();
        $iterableResultMock = $this->getMockBuilder('Doctrine\ORM\Internal\Hydration\IterableResult')->disableOriginalConstructor()->getMock();

        $customerMock1->expects($this->any())->method('getCustomerId')->willReturn(1);
        $companyMock1->expects($this->any())->method('getCompanyId')->willReturn(1);
        $companyMock1->expects($this->any())->method('getCustomer')->willReturn($customerMock1);
        $serviceMock1->expects($this->any())->method('getCompany')->willReturn($companyMock1);

        $serviceMocks[][] = $serviceMock1;

        $iterableResultMockSetup = IteratorHelper::mockIterator($this, $iterableResultMock, $serviceMocks);

        $this->serviceServiceMock->expects($this->once())->method('getServicesDataExpiringWithinDatesForEvent')
            ->with(new Date('+1 days'), new Date('+7 days'), EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_EXPIRES_IN_7)
            ->willReturn($iterableResultMockSetup);

        $this->servicesEmailerMock->expects($this->once())->method('sendEmailServiceAutoRenewalIn7');
        $this->eventServiceMock->expects($this->once())->method('notify');
        $this->object->execute();
    }

    /**
     * @covers Cron\Commands\Services\AutoRenewal::execute
     */
    public function testCanSendReminderToOneCustomerWithMultipleExpiringServices()
    {
        $customerMock1 = $this->getMockBuilder('Entities\Customer')->disableOriginalConstructor()->getMock();
        $companyMock1 = $this->getMockBuilder('Entities\Company')->disableOriginalConstructor()->getMock();
        $serviceMock1 = $this->getMockBuilder('Entities\Service')->disableOriginalConstructor()->getMock();
        $serviceMock2 = $this->getMockBuilder('Entities\Service')->disableOriginalConstructor()->getMock();
        $iterableResultMock = $this->getMockBuilder('Doctrine\ORM\Internal\Hydration\IterableResult')->disableOriginalConstructor()->getMock();

        $customerMock1->expects($this->any())->method('getCustomerId')->willReturn(1);
        $companyMock1->expects($this->any())->method('getCompanyId')->willReturn(1);
        $companyMock1->expects($this->any())->method('getCustomer')->willReturn($customerMock1);
        $serviceMock1->expects($this->any())->method('getCompany')->willReturn($companyMock1);
        $serviceMock2->expects($this->any())->method('getCompany')->willReturn($companyMock1);

        $serviceMocks[][] = $serviceMock1;
        $serviceMocks[][] = $serviceMock2;

        $iterableResultMockSetup = IteratorHelper::mockIterator($this, $iterableResultMock, $serviceMocks);

        $this->serviceServiceMock->expects($this->once())->method('getServicesDataExpiringWithinDatesForEvent')
            ->with(new Date('+1 days'), new Date('+7 days'), EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_EXPIRES_IN_7)
            ->willReturn($iterableResultMockSetup);

        $this->servicesEmailerMock->expects($this->once())->method('sendEmailServiceAutoRenewalIn7');
        $this->eventServiceMock->expects($this->exactly(2))->method('notify');
        $this->object->execute();
    }

    /**
     * @covers Cron\Commands\Services\AutoRenewal::execute
     */
    public function testCanSendRemindersToMultipleCustomersWithExpiringServices()
    {
        $customerMock1 = $this->getMockBuilder('Entities\Customer')->disableOriginalConstructor()->getMock();
        $customerMock2 = $this->getMockBuilder('Entities\Customer')->disableOriginalConstructor()->getMock();
        $companyMock1 = $this->getMockBuilder('Entities\Company')->disableOriginalConstructor()->getMock();
        $companyMock2 = $this->getMockBuilder('Entities\Company')->disableOriginalConstructor()->getMock();
        $serviceMock1 = $this->getMockBuilder('Entities\Service')->disableOriginalConstructor()->getMock();
        $serviceMock2 = $this->getMockBuilder('Entities\Service')->disableOriginalConstructor()->getMock();
        $serviceMock3 = $this->getMockBuilder('Entities\Service')->disableOriginalConstructor()->getMock();
        $iterableResultMock = $this->getMockBuilder('Doctrine\ORM\Internal\Hydration\IterableResult')->disableOriginalConstructor()->getMock();

        $customerMock1->expects($this->any())->method('getCustomerId')->willReturn(1);
        $customerMock2->expects($this->any())->method('getCustomerId')->willReturn(2);
        $companyMock1->expects($this->any())->method('getCompanyId')->willReturn(1);
        $companyMock1->expects($this->any())->method('getCustomer')->willReturn($customerMock1);
        $companyMock2->expects($this->any())->method('getCompanyId')->willReturn(2);
        $companyMock2->expects($this->any())->method('getCustomer')->willReturn($customerMock2);
        $serviceMock1->expects($this->any())->method('getCompany')->willReturn($companyMock1);
        $serviceMock1->expects($this->any())->method('getServiceId')->willReturn(1);
        $serviceMock2->expects($this->any())->method('getCompany')->willReturn($companyMock2);
        $serviceMock2->expects($this->any())->method('getServiceId')->willReturn(2);
        $serviceMock3->expects($this->any())->method('getCompany')->willReturn($companyMock2);
        $serviceMock3->expects($this->any())->method('getServiceId')->willReturn(3);

        $serviceMocks[][] = $serviceMock1;
        $serviceMocks[][] = $serviceMock2;
        $serviceMocks[][] = $serviceMock3;

        $iterableResultMockSetup = IteratorHelper::mockIterator($this, $iterableResultMock, $serviceMocks);

        $this->serviceServiceMock->expects($this->once())->method('getServicesDataExpiringWithinDatesForEvent')
            ->with(new Date('+1 days'), new Date('+7 days'), EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_EXPIRES_IN_7)
            ->willReturn($iterableResultMockSetup);

        $this->servicesEmailerMock->expects($this->exactly(2))->method('sendEmailServiceAutoRenewalIn7')->with(
            $this->isInstanceOf('Company\Service\RenewalData')
        );
        $this->eventServiceMock->expects($this->exactly(3))->method('notify')->withConsecutive(
            array(EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_EXPIRES_IN_7, 1),
            array(EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_EXPIRES_IN_7, 2),
            array(EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_EXPIRES_IN_7, 3)
        );

        $this->object->execute();
    }

    /**
     * @covers Cron\Commands\Services\AutoRenewal::execute
     */
    public function testCanSendRemindersToMultipleCustomersWithMultipleCompaniesAndExpiringServices()
    {
        $customerMock1 = $this->getMockBuilder('Entities\Customer')->disableOriginalConstructor()->getMock();
        $customerMock2 = $this->getMockBuilder('Entities\Customer')->disableOriginalConstructor()->getMock();
        $companyMock1 = $this->getMockBuilder('Entities\Company')->disableOriginalConstructor()->getMock();
        $companyMock2 = $this->getMockBuilder('Entities\Company')->disableOriginalConstructor()->getMock();
        $companyMock3 = $this->getMockBuilder('Entities\Company')->disableOriginalConstructor()->getMock();
        $serviceMock1 = $this->getMockBuilder('Entities\Service')->disableOriginalConstructor()->getMock();
        $serviceMock2 = $this->getMockBuilder('Entities\Service')->disableOriginalConstructor()->getMock();
        $serviceMock3 = $this->getMockBuilder('Entities\Service')->disableOriginalConstructor()->getMock();
        $serviceMock4 = $this->getMockBuilder('Entities\Service')->disableOriginalConstructor()->getMock();
        $iterableResultMock = $this->getMockBuilder('Doctrine\ORM\Internal\Hydration\IterableResult')->disableOriginalConstructor()->getMock();

        $customerMock1->expects($this->any())->method('getCustomerId')->willReturn(1);
        $customerMock2->expects($this->any())->method('getCustomerId')->willReturn(2);
        $companyMock1->expects($this->any())->method('getCompanyId')->willReturn(1);
        $companyMock1->expects($this->any())->method('getCustomer')->willReturn($customerMock1);
        $companyMock2->expects($this->any())->method('getCompanyId')->willReturn(2);
        $companyMock2->expects($this->any())->method('getCustomer')->willReturn($customerMock2);
        $companyMock3->expects($this->any())->method('getCompanyId')->willReturn(3);
        $companyMock3->expects($this->any())->method('getCustomer')->willReturn($customerMock2);
        $serviceMock1->expects($this->any())->method('getCompany')->willReturn($companyMock1);
        $serviceMock1->expects($this->any())->method('getServiceId')->willReturn(1);
        $serviceMock2->expects($this->any())->method('getCompany')->willReturn($companyMock2);
        $serviceMock2->expects($this->any())->method('getServiceId')->willReturn(2);
        $serviceMock3->expects($this->any())->method('getCompany')->willReturn($companyMock3);
        $serviceMock3->expects($this->any())->method('getServiceId')->willReturn(3);
        $serviceMock4->expects($this->any())->method('getCompany')->willReturn($companyMock3);
        $serviceMock4->expects($this->any())->method('getServiceId')->willReturn(4);

        $serviceMocks[][] = $serviceMock1;
        $serviceMocks[][] = $serviceMock2;
        $serviceMocks[][] = $serviceMock3;
        $serviceMocks[][] = $serviceMock4;

        $iterableResultMockSetup = IteratorHelper::mockIterator($this, $iterableResultMock, $serviceMocks);

        $this->serviceServiceMock->expects($this->once())->method('getServicesDataExpiringWithinDatesForEvent')
            ->with(new Date('+1 days'), new Date('+7 days'), EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_EXPIRES_IN_7)
            ->willReturn($iterableResultMockSetup);

        $this->servicesEmailerMock->expects($this->exactly(2))->method('sendEmailServiceAutoRenewalIn7')->with(
            $this->isInstanceOf('Company\Service\RenewalData')
        );
        $this->eventServiceMock->expects($this->exactly(4))->method('notify')->withConsecutive(
            array(EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_EXPIRES_IN_7, 1),
            array(EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_EXPIRES_IN_7, 2),
            array(EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_EXPIRES_IN_7, 3),
            array(EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_EXPIRES_IN_7, 4)
        );
        $this->object->execute();
    }
}
