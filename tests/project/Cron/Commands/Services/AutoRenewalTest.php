<?php

namespace Cron\Commands\Services;

use Emailers\ServicesEmailer;
use EventLocator;
use ILogger;
use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;
use SagePay\Reporting\SagePay;
use Services\EventService;
use Services\Payment\SageService;
use Services\Payment\TokenService;
use Services\ProductService;
use Services\ServiceService;
use ServiceSettingsModule\Services\ServiceSettingsService;
use Services\TransactionService;
use tests\helpers\IteratorHelper;
use tests\helpers\MockHelper;
use Utils\Date;
use Symfony\Component\EventDispatcher\EventDispatcher;

class AutoRenewalTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var AutoRenewal
     */
    private $object;

    /**
     * @var ILogger|PHPUnit_Framework_MockObject_MockObject
     */
    private $loggerMock;

    /**
     * @var ProductService|PHPUnit_Framework_MockObject_MockObject
     */
    private $productServiceMock;

    /**
     * @var ServiceService|PHPUnit_Framework_MockObject_MockObject
     */
    private $serviceServiceMock;

    /**
     * @var EventService|PHPUnit_Framework_MockObject_MockObject
     */
    private $eventServiceMock;

    /**
     * @var TransactionService|PHPUnit_Framework_MockObject_MockObject
     */
    private $transactionServiceMock;

    /**
     * @var SageService|PHPUnit_Framework_MockObject_MockObject
     */
    private $sageServiceMock;

    /**
     * @var ServicesEmailer|PHPUnit_Framework_MockObject_MockObject
     */
    private $servicesEmailerMock;

    /**
     * @var EventDispatcher|PHPUnit_Framework_MockObject_MockObject
     */
    private $eventDispatcherMock;

    /**
     * @var ServiceSettingsService|PHPUnit_Framework_MockObject_MockObject
     */
    private $serviceSettingsServiceMock;

    /**
     * @var TokenService|PHPUnit_Framework_MockObject_MockObject
     */
    private $tokenServiceMock;

    /**
     * @var SagePay|PHPUnit_Framework_MockObject_MockObject
     */
    private $sagePayMock;

    protected function setUp()
    {
        $this->loggerMock = $this->getMockBuilder('ILogger')->disableOriginalConstructor()->getMock();
        $this->productServiceMock = $this->getMockBuilder('Services\ProductService')->disableOriginalConstructor()->getMock();
        $this->serviceServiceMock = $this->getMockBuilder('Services\ServiceService')->disableOriginalConstructor()->getMock();
        $this->eventServiceMock = $this->getMockBuilder('Services\EventService')->disableOriginalConstructor()->getMock();
        $this->transactionServiceMock = $this->getMockBuilder('Services\TransactionService')->disableOriginalConstructor()->getMock();
        $this->sageServiceMock = $this->getMockBuilder('Services\Payment\SageService')->disableOriginalConstructor()->getMock();
        $this->servicesEmailerMock = $this->getMockBuilder('Emailers\ServicesEmailer')->disableOriginalConstructor()->getMock();
        $this->eventDispatcherMock = MockHelper::mock($this, 'Symfony\Component\EventDispatcher\EventDispatcher');
        $this->serviceSettingsServiceMock = MockHelper::mock($this, 'ServiceSettingsModule\Services\ServiceSettingsService');
        $this->tokenServiceMock = MockHelper::mock($this, 'Services\Payment\TokenService');
        $this->sagePayMock = MockHelper::mock($this, 'SagePay\Reporting\SagePay');

        $this->object = new AutoRenewal(
            $this->loggerMock,
            $this->getMock('Cron\INotifier'),
            $this->productServiceMock,
            $this->serviceServiceMock,
            $this->transactionServiceMock,
            $this->eventServiceMock,
            $this->sageServiceMock,
            $this->serviceSettingsServiceMock,
            $this->tokenServiceMock,
            $this->sagePayMock,
            $this->servicesEmailerMock,
            $this->eventDispatcherMock
        );
    }

    public function testWillNotAutoRenewWithNoServices()
    {
        $iterableMock = IteratorHelper::createDoctrine([]);

        $this->serviceServiceMock->expects($this->once())->method('getServicesDataExpiringWithinDatesForEvent')
            ->with(new Date('-28 days'), new Date, EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_SUCCEDED)
            ->willReturn($iterableMock);

        $this->loggerMock->expects($this->once())->method('logMessage')
            ->with(ILogger::NOTICE, 'No Companies with services to renew found');
        $this->object->execute();
    }

    public function testWillAutoRenewOneCustomerWithOneService()
    {
        $customerMock1 = $this->getMockBuilder('Entities\Customer')->disableOriginalConstructor()->getMock();

        $companyMock1 = $this->getMockBuilder('Entities\Company')->disableOriginalConstructor()->getMock();
        $serviceMock1 = $this->getMockBuilder('Entities\Service')->disableOriginalConstructor()->getMock();
        $tokenMock1 = $this->getMockBuilder('Entities\Payment\Token')->disableOriginalConstructor()->getMock();
        $productMock1 = $this->getMockBuilder('Product')->disableOriginalConstructor()->getMock();

        $serviceMock2 = $this->getMockBuilder('Entities\Service')->disableOriginalConstructor()->getMock();
        $productMock2 = $this->getMockBuilder('Product')->disableOriginalConstructor()->getMock();

        $paymentResponseMock = $this->getMockBuilder('Services\Payment\PaymentResponse')->disableOriginalConstructor()->getMock();

        $this->sageServiceMock->expects($this->once())->method('processRecurringPayment')->willReturn($paymentResponseMock);

        $tokenMock1->expects($this->any())->method('isEqual')->willReturn(TRUE);

        $customerMock1->expects($this->any())->method('getCustomerId')->willReturn(1);
        $customerMock1->expects($this->once())->method('getActiveToken')->willReturn($tokenMock1);
        $companyMock1->expects($this->any())->method('getCompanyId')->willReturn(1);
        $companyMock1->expects($this->exactly(2))->method('getCustomer')->willReturn($customerMock1);
        $productMock1->expects($this->once())->method('canBeAdded')->willReturn(TRUE);
        $serviceMock1->expects($this->any())->method('getServiceId')->willReturn(1);
        $serviceMock1->expects($this->once())->method('getCompany')->willReturn($companyMock1);
        $serviceMock1->expects($this->once())->method('getRenewalProduct')->willReturn($productMock1);

        $productMock2->expects($this->once())->method('canBeAdded')->willReturn(TRUE);
        $serviceMock2->expects($this->any())->method('getServiceId')->willReturn(2);
        $serviceMock2->expects($this->once())->method('getCompany')->willReturn($companyMock1);
        $serviceMock2->expects($this->once())->method('getRenewalProduct')->willReturn($productMock2);

        $serviceMocks[] = $serviceMock1;
        $serviceMocks[] = $serviceMock2;

        $serviceSettingsMock1 = MockHelper::mock($this, 'Entities\ServiceSettings');
        $serviceSettingsMock1->method('getRenewalToken')->willReturn($tokenMock1);
        $serviceSettingsMock2 = MockHelper::mock($this, 'Entities\ServiceSettings');
        $serviceSettingsMock2->method('getRenewalToken')->willReturn($tokenMock1);

        $this->serviceSettingsServiceMock->method('getSettingsByService')->willReturnOnConsecutiveCalls(
            $serviceSettingsMock1, $serviceSettingsMock2
        );

        $iterableResultMock = IteratorHelper::createDoctrine($serviceMocks);

        $this->serviceServiceMock->expects($this->once())->method('getServicesDataExpiringWithinDatesForEvent')
            ->with(new Date('-28 days'), new Date, EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_SUCCEDED)
            ->willReturn($iterableResultMock);

        $this->eventServiceMock->expects($this->exactly(2))->method('notify')
            ->withConsecutive(
                [EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_SUCCEDED, 1],
                [EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_SUCCEDED, 2]
            );

        $this->productServiceMock->expects($this->once())->method('saveServices')
            ->with([$productMock1, $productMock2], $tokenMock1);
        $this->servicesEmailerMock->expects($this->once())->method('sendEmailServiceAutoRenewalSucceeded');

        $this->object->execute();
    }

}
