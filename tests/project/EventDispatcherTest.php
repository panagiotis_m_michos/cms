<?php

use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\Event;

class EventDispatcherTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var EventDispatcher
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->object = Registry::$container->get(DiLocator::DISPATCHER);
    }

    public function testDispatch()
    {
        $test = FALSE;
        $this->object->addListener(
            'test', 
            function(Event $event) use (&$test) {
                $test = TRUE;
            }
        );
        $this->assertFalse($test);
        $this->object->dispatch('test');
        $this->assertTrue($test);
        $test = FALSE;
        $this->object->dispatch('test1');
        $this->assertFalse($test);
    }
}
