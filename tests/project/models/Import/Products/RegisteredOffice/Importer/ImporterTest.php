<?php

namespace Import\Products\RegisteredOffice\Importer;

use CSVParser\Items\Items;
use DateTime;
use Import\Products\RegisteredOffice\Parser\Item;
use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;
use tests\helpers\IteratorHelper;
use tests\helpers\MockHelper;

class ImporterTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    private $entityManager;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    private $companyService;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    private $orderService;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    private $customerService;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    private $chData;


    protected function setUp()
    {
        $this->markTestSkipped('This would need a lot of time to change Importer because Service entity has changed');
        $this->entityManager = $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock();
        $this->companyService = $this->getMockBuilder('Services\CompanyService')->disableOriginalConstructor()->getMock();
        $this->orderService = $this->getMockBuilder('Services\OrderService')->disableOriginalConstructor()->getMock();
        $this->customerService = $this->getMockBuilder('Services\CustomerService')->disableOriginalConstructor()->getMock();
        $this->chData = $this->getMockBuilder('CompaniesHouse\Data\Data')->disableOriginalConstructor()->getMock();
    }

    public function testImport()
    {
        $items = [
            $this->getItem(
                [
                    'productId' => 165,
                    'orderId' => NULL,
                    'companyId' => 3174684,
                    'customerId' => 246777,
                    'renewalProductId' => 806,
                    'dtStart' => new DateTime('2014-01-12'),
                    'dtExpires' => new DateTime('2015-01-11'),
                    'dtc' => new DateTime('2014-01-12'),
                ]
            )
        ];
        $importer = $this->getObject($items);

        $companyDetails = MockHelper::mock($this, 'CompaniesHouse\Data\Entities\CompanyDetails');
        $accountsMock = MockHelper::mock($this, 'CompaniesHouse\Data\Entities\Accounts');
        $returnsMock = MockHelper::mock($this, 'CompaniesHouse\Data\Entities\Returns');
        $regAddressMock = MockHelper::mock($this, 'CompaniesHouse\Data\Entities\RegisteredAddress');

        $companyDetails->expects($this->any())->method('getAccounts')->willReturn($accountsMock);
        $companyDetails->expects($this->any())->method('getReturns')->willReturn($returnsMock);
        $companyDetails->expects($this->any())->method('getRegAddress')->willReturn($regAddressMock);
        $this->chData->expects($this->once())->method('getCompanyDetails')->willReturn($companyDetails);

        $importer->import();
        $this->assertEmpty($importer->getErrors());
    }

    /**
     * @param array $items
     * @return Importer
     */
    private function getObject(array $items)
    {
        return new Importer(
            $this->getItems($items),
            $this->entityManager,
            $this->companyService,
            $this->orderService,
            $this->customerService,
            $this->chData
        );
    }

    /**
     * @param array $itemsArray
     * @return Items|PHPUnit_Framework_MockObject_MockObject
     */
    private function getItems(array $itemsArray)
    {
        $mockItems = $this->getMockBuilder('CSVParser\Items\Items')
            ->disableOriginalConstructor()
            ->getMock();

        IteratorHelper::mockIterator($this, $mockItems, $itemsArray);

        return $mockItems;
    }

    /**
     * @param array $data
     * @return Item
     */
    private function getItem(array $data)
    {
        $item = new Item();
        $classReflection = $item->getReflection();
        foreach ($data as $propertyName => $value) {
            $reflectionProperty = $classReflection->getProperty($propertyName);
            $reflectionProperty->setAccessible(TRUE);
            $reflectionProperty->setValue($item, $value);
        }
        return $item;

    }
}
