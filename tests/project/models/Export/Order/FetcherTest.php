<?php

namespace Export\Order;

use BarclaysBanking;
use BootstrapModule\Ext\FrameworkExt_Abstract;
use DateTime;
use DibiResultIterator;
use DibiRow;
use Entities\Customer;
use Entities\Order;
use EntityHelper;
use FormModule\Model\DatesRange;
use PHPUnit_Framework_TestCase;
use Registry;

class FetcherTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Fetcher
     */
    private $object;

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var Order
     */
    private $order;

    protected function setUp()
    {
        EntityHelper::emptyTables(
            [
                TBL_CUSTOMERS,
                TBL_ORDERS,
                TBL_ORDERS_ITEMS,
                TBL_COMPANY
            ]
        );

        $this->customer = EntityHelper::createCustomer(TEST_EMAIL1);
        $this->order = EntityHelper::createOrder($this->customer, 0);

        $this->object = new Fetcher(Registry::getService(FrameworkExt_Abstract::DIBI));

    }

    /****************************************** getIncorporationNotRequiredIterator ******************************************/

    /**
     * @test
     * @covers Export\Order\Fetcher::getIncorporationNotRequiredIterator()
     * @dataProvider incorporationNotRequiredDataProvider
     */
    public function getIncorporationNotRequiredIterator(array $products, array $excluded, array $idCheckRequiredProductsIds, $isIdCheckRequired, $dtIdValidated, $dtExportedFrom, $dtExportedTo, $expectedCount)
    {
        $em = EntityHelper::getEntityManager();
        $this->customer->setIsIdCheckRequired($isIdCheckRequired);
        $this->customer->setDtIdValidated($dtIdValidated);
        foreach ($products as $product) {
            list ($productId, $incorporationRequired, $isRefundItem, $dtExported) = $product;
            $item = EntityHelper::createOrderItem($this->order, 20, 2, $productId);
            $item->setIncorporationRequired($incorporationRequired);
            $item->setDtExported($dtExported);
            if ($isRefundItem) {
                $refundItem = EntityHelper::createOrderItem($this->order, 20, 2, $productId);
                $item->setRefundedOrderItem($refundItem);
            }
        }
        $em->flush();

        $filter = new Filter($excluded, $idCheckRequiredProductsIds, [], BarclaysBanking::BARCLAYS_BANKING_PRODUCT);
        $filter->setDatesRange(new DatesRange($dtExportedFrom, $dtExportedTo));

        $iterator = $this->object->getIncorporationNotRequiredIterator($filter);
        $this->assertEquals($expectedCount, $iterator->count());

        $this->checkHeader($iterator);
    }


    public function incorporationNotRequiredDataProvider()
    {
        $barclaysProductId = BarclaysBanking::BARCLAYS_BANKING_PRODUCT;
        // products(productId, incorporationRequired, isRefundItem, dtExported), excluded, isIdCheckRequired, idCheckCompletedDate, dtExportedFrom, dtExportedTo, expectedCount
        return [
            [[[1, NULL, FALSE, NULL]], [], [], FALSE, NULL, NULL, NULL, 1],
            [[[1, NULL, TRUE, NULL]], [], [], FALSE, NULL, NULL, NULL, 1], // refund item
            [[[1, NULL, FALSE, NULL], [$barclaysProductId, NULL, FALSE, NULL]], [], [], 0, NULL, NULL, NULL, 1], // barclays should be excluded
            [[[1, TRUE, FALSE, NULL], [2, NULL, FALSE, NULL]], [], [], FALSE, NULL, NULL, NULL, 1], // just not incorporated
            [[[1, NULL, FALSE, NULL], [2, NULL, FALSE, NULL]], [1], [], FALSE, NULL, NULL, NULL, 1], // one product excluded
            //[[[1, NULL, FALSE, NULL], [2, NULL, TRUE, NULL]], [1], [], FALSE, NULL, NULL, NULL, 0], // one product excluded, other is refund item

            [[[1, NULL, FALSE, NULL]], [], [1], 1, new DateTime(), NULL, NULL, 1], // id check required for product and finished
            [[[1, NULL, FALSE, NULL]], [], [1], 1, NULL, NULL, NULL, FALSE], // id check required for product but not verified
            [[[1, NULL, FALSE, NULL]], [], [1], 0, NULL, NULL, NULL, TRUE], // id check required for product but not required for customer
            [[[1, NULL, FALSE, NULL]], [], [2], 0, NULL, NULL, NULL, TRUE], // id check not required for product

            [[[1, NULL, FALSE, new DateTime('-2 days')]], [], [], FALSE, NULL, new DateTime('-1 day'), NULL, 0], // dtExportedFrom > dtExported
            [[[1, NULL, FALSE, new DateTime('-1 days')]], [], [], FALSE, NULL, new DateTime('-2 day'), NULL, 1], // dtExportedFrom < dtExported

            [[[123, NULL, FALSE, new DateTime('-2 days')]], [], [], FALSE, NULL, NULL, new DateTime('-1 days'), 1], // dtExportedTo > dtExported
            [[[123, NULL, FALSE, new DateTime('-1 days')]], [], [], FALSE, NULL, NULL, new DateTime('-3 days'), 0], // dtExportedTo < dtExported

            [[[123, NULL, FALSE, new DateTime('-2 days')]], [], [], FALSE, NULL, new DateTime('-3 days'), new DateTime('-1 days'), 1], // dtExported is between
            [[[123, NULL, FALSE, new DateTime('-3 days')]], [], [], FALSE, NULL, new DateTime('-2 days'), new DateTime('-1 days'), 0], // dtExported is in past
            [[[123, NULL, FALSE, new DateTime('-1 days')]], [], [], FALSE, NULL, new DateTime('-3 days'), new DateTime('-2 days'), 0], // dtExported is in future
        ];
    }


    /****************************************** getIncorporationRequiredNotPackageAndNotBelongsToPackage ******************************************/


    /**
     * @test
     * @covers Export\Order\Fetcher::getIncorporationRequiredNotPackageAndNotBelongsToPackageIterator()
     * @dataProvider incorporationRequiredNotPackageAndNotBelongsToPackageDataProvider
     */
    public function getIncorporationRequiredNotPackageAndNotBelongsToPackageIterator($companyNumber, array $products, array $excluded, array $idCheckRequiredProductsIds, array $packages, $isIdCheckRequired, $dtIdValidated, $dtExportedFrom, $dtExportedTo, $expectedCount)
    {
        $em = EntityHelper::getEntityManager();

        $company = EntityHelper::createCompany($this->customer, $this->order);
        if ($companyNumber) {
            $company->setCompanyNumber($companyNumber);
        }

        $this->customer->setIsIdCheckRequired($isIdCheckRequired);
        $this->customer->setDtIdValidated($dtIdValidated);
        foreach ($products as $product) {
            list ($productId, $belongsToFormationPackage, $incorporationRequired, $isRefundItem, $dtExported) = $product;

            $order = $belongsToFormationPackage ? $this->order :EntityHelper::createOrder($this->customer, 0);
            $item = EntityHelper::createOrderItem($order, 20, 2, $productId);
            $item->setCompany($company);
            $item->setIncorporationRequired($incorporationRequired);
            $item->setDtExported($dtExported);
            if ($isRefundItem) {
                $refundItem = EntityHelper::createOrderItem($this->order, 20, 2, $productId);
                $item->setRefundedOrderItem($refundItem);
            }
        }
        $em->flush();

        $filter = new Filter($excluded, $idCheckRequiredProductsIds, $packages, BarclaysBanking::BARCLAYS_BANKING_PRODUCT);
        $filter->setDatesRange(new DatesRange($dtExportedFrom, $dtExportedTo));

        $iterator = $this->object->getIncorporationRequiredNotPackageAndNotBelongsToPackageIterator($filter);
        $this->assertEquals($expectedCount, $iterator->count());

        $this->checkHeader($iterator);
    }

    public function incorporationRequiredNotPackageAndNotBelongsToPackageDataProvider()
    {
        $packages = [1, 2, 3];
        // companyNumber, products(productId, belongsToFormationPackage, incorporationRequired, isRefundItem, dtExported), products, excluded, isIdCheckRequired, packages, isIdCheckRequired, idCheckCompletedDate, dtExportedFrom, dtExportedTo, expectedCount
        return [
            ['12345678', [[4, FALSE, TRUE, FALSE, NULL]], [], [], $packages, FALSE, NULL, NULL, NULL, 1],
            ['12345678', [[4, FALSE, TRUE, TRUE, NULL]], [], [], $packages, FALSE, NULL, NULL, NULL, 0], // refund item
            ['12345678', [[4, TRUE, TRUE, FALSE, NULL]], [], [], $packages, FALSE, NULL, NULL, NULL, 0], // order item is part of formation package
            [NULL, [[4, FALSE, TRUE, FALSE, NULL]], [], [], $packages, FALSE, NULL, NULL, NULL, 0], // company number is NULL
            ['12345678', [[2, TRUE, TRUE, FALSE, NULL]], [], [], $packages, FALSE, NULL, NULL, NULL, 0], // product is package
            ['12345678', [[4, FALSE, NULL, FALSE, NULL]], [], [], $packages, FALSE, NULL, NULL, NULL, 0], // order item incorporation not required

            ['12345678', [[4, FALSE, TRUE, FALSE, NULL]], [], [4], $packages, TRUE, NULL, NULL, NULL, 0], // id check required and not validated
            ['12345678', [[4, FALSE, TRUE, FALSE, NULL]], [], [], $packages, TRUE, new DateTime('-1 day'), NULL, NULL, 1], // id check required and validated
            ['12345678', [[4, FALSE, TRUE, FALSE, NULL]], [], [3], $packages, TRUE, NULL, NULL, NULL, 1], // product not required for id check
            ['12345678', [[4, FALSE, TRUE, TRUE, NULL]], [], [3], $packages, TRUE, NULL, NULL, NULL, 0], // product not required for id check, refund item

            ['12345678', [[4, FALSE, TRUE, FALSE, NULL]], [4], [], $packages, FALSE, NULL, NULL, NULL, 0], // product is excluded

            ['12345678', [[4, FALSE, TRUE, FALSE, new DateTime('-2 day')]], [], [], $packages, FALSE, NULL, new DateTime('-1 day'), NULL, 0], // dtExportedFrom > dtExported
            ['12345678', [[4, FALSE, TRUE, FALSE, new DateTime('-1 day')]], [], [], $packages, FALSE, NULL, new DateTime('-2 day'), NULL, 1], // dtExportedFrom < dtExported

            ['12345678', [[4, FALSE, TRUE, FALSE, new DateTime('-2 day')]], [], [], $packages, FALSE, NULL, NULL, new DateTime('-1 day'), 1], // dtExportedTo > dtExported
            ['12345678', [[4, FALSE, TRUE, FALSE, new DateTime('-1 day'), ]], [], [], $packages, FALSE, NULL, NULL, new DateTime('-2 day'), 0], // dtExportedTo < dtExported

            ['12345678', [[4, FALSE, TRUE, FALSE, new DateTime('-2 day')]], [], [], $packages, FALSE, NULL, new DateTime('-3 day'), new DateTime('-1 day'), 1], // dtExported is between
            ['12345678', [[4, FALSE, TRUE, FALSE, new DateTime('-3 day')]], [], [], $packages, FALSE, NULL, new DateTime('-2 day'), new DateTime('-1 day'), 0], // dtExported is in past
            ['12345678', [[4, FALSE, TRUE, FALSE, new DateTime('-1 day')]], [], [], $packages, FALSE, NULL, new DateTime('-3 day'), new DateTime('-2 day'), 0], // dtExported is in future
        ];
    }


    /****************************************** getIncorporationRequiredNotPackageAndBelongsToPackageIterator ******************************************/

    /**
     * @test
     * @covers Export\Order\Fetcher::getIncorporationRequiredNotPackageAndBelongsToPackageIterator()
     * @dataProvider incorporationRequiredNotPackageAndBelongsToPackageDataProvider
     */
    public function getIncorporationRequiredNotPackageAndBelongsToPackageIterator($companyNumber, $sameOrder, array $products, array $excluded, array $idCheckRequiredProductsIds, array $packages, $isIdCheckRequired, $dtIdValidated, $dtExportedFrom, $dtExportedTo, $expectedCount)
    {
        $this->markTestSkipped('Will be refactored soon - hopefully!');

        $em = EntityHelper::getEntityManager();

        $company = EntityHelper::createCompany($this->customer);
        if ($companyNumber) {
            $company->setCompanyNumber($companyNumber);
        }
        if ($sameOrder) {
            $company->setOrder($this->order);
        }

        $this->customer->setIsIdCheckRequired($isIdCheckRequired);
        $this->customer->setDtIdValidated($dtIdValidated);
        foreach ($products as $product) {
            list ($productId, $incorporationRequired, $isRefundItem, $dtExported) = $product;
            $item = EntityHelper::createOrderItem($this->order, 20, 2, $productId);
            $item->setIncorporationRequired($incorporationRequired);
            $item->setDtExported($dtExported);
            if ($isRefundItem) {
                $refundItem = EntityHelper::createOrderItem($this->order, 20, 2, $productId);
                $item->setRefundedOrderItem($refundItem);
            }
        }
        $em->flush();

        $filter = new Filter($excluded, $idCheckRequiredProductsIds, $packages, BarclaysBanking::BARCLAYS_BANKING_PRODUCT);
        $filter->setDatesRange(new DatesRange($dtExportedFrom, $dtExportedTo));

        $iterator = $this->object->getIncorporationRequiredNotPackageAndBelongsToPackageIterator($filter);
        $this->assertEquals($expectedCount, $iterator->count());

        $this->checkHeader($iterator);
    }

    public function incorporationRequiredNotPackageAndBelongsToPackageDataProvider()
    {
        $packages = [1, 2, 3];
        // companyNumber, isRefunded, sameOrder, products(productId, incorporationRequired, isRefundItem, dtExported), excluded, isIdCheckRequired, packages, isIdCheckRequired, idCheckCompletedDate, dtExportedFrom, dtExportedTo, expectedCount
        return [
            ['12345678', TRUE, [[4, TRUE, FALSE, NULL]], [], [], $packages, FALSE, NULL, NULL, NULL, 1],
            ['12345678', TRUE, [[4, TRUE, TRUE, NULL]], [], [], $packages, FALSE, NULL, NULL, NULL, 0], //refund item
            ['12345678', FALSE, [[4, TRUE, FALSE, NULL]], [], [], $packages, FALSE, NULL, NULL, NULL, 0], // item wasn't purchased with package
            [NULL, TRUE, [[4, TRUE, FALSE, NULL]], [], [], $packages, FALSE, NULL, NULL, NULL, 0], // no company number
            ['12345678', TRUE, [[1, TRUE, FALSE, NULL]], [], [], $packages, FALSE, NULL, NULL, NULL, 0], // product is package

            ['12345678', TRUE, [[4, TRUE, FALSE, NULL]], [], [4], $packages, TRUE, NULL, NULL, NULL, 0], // id check required and not validated
            ['12345678', TRUE, [[4, TRUE, FALSE, NULL]], [], [4], $packages, TRUE, new DateTime('-1 day'), NULL, NULL, 1], // id check required and validated
            ['12345678', TRUE, [[4, TRUE, TRUE, NULL]], [], [4], $packages, TRUE, new DateTime('-1 day'), NULL, NULL, 0], // id check required and validated, refund item
            ['12345678', TRUE, [[4, TRUE, FALSE, NULL]], [], [1], $packages, TRUE, NULL, NULL, NULL, 1],
            ['12345678', TRUE, [[4, TRUE, TRUE, NULL]], [], [1], $packages, TRUE, NULL, NULL, NULL, 0], // refund item

            ['12345678', TRUE, [[4, TRUE, FALSE, NULL]], [4], [], $packages, FALSE, NULL, NULL, NULL, 0], // product is excluded

            ['12345678', TRUE, [[4, TRUE, FALSE, new DateTime('-2 days')]], [], [], $packages, FALSE, NULL, new DateTime('-1 days'), NULL, 0], // dtExportedFrom > dtExported
            ['12345678', TRUE, [[4, TRUE, FALSE, new DateTime('-1 days')]], [], [], $packages, FALSE, NULL, new DateTime('-2 days'), NULL, 1], // dtExportedFrom < dtExported

            ['12345678', TRUE, [[4, TRUE, FALSE, new DateTime('-2 days')]], [], [], $packages, FALSE, NULL, NULL, new DateTime('-1 days'), 1], // dtExportedTo > dtExported
            ['12345678', TRUE, [[4, TRUE, FALSE, new DateTime('-1 days')]], [], [], $packages, FALSE, NULL, NULL, new DateTime('-2 days'), 0], // dtExportedTo < dtExported

            ['12345678', TRUE, [[4, TRUE, FALSE, new DateTime('-2 days')]], [], [], $packages, FALSE, NULL, new DateTime('-3 days'), new DateTime('-1 days'), 1], // dtExported is between
            ['12345678', TRUE, [[4, TRUE, FALSE, new DateTime('-3 days')]], [], [], $packages, FALSE, NULL, new DateTime('-2 days'), new DateTime('-1 days'), 0], // dtExported is in past
            ['12345678', TRUE, [[4, TRUE, FALSE, new DateTime('-1 days')]], [], [], $packages, FALSE, NULL, new DateTime('-3 days'), new DateTime('-2 days'), 0], // dtExported is in future

        ];
    }

    /****************************************** getIncorporationRequiredAndPackageIterator ******************************************/

    /**
     * @test
     * @covers Export\Order\Fetcher::getIncorporationRequiredAndPackageIterator()
     * @dataProvider incorporationRequiredAndPackageDataProvider
     */
    public function getIncorporationRequiredAndPackageIterator($companyNumber, $sameOrder, array $products, array $excluded, array $idCheckRequiredProductsIds, array $packages, $isIdCheckRequired, $dtIdValidated, $dtExportedFrom, $dtExportedTo, $expectedCount)
    {
        $em = EntityHelper::getEntityManager();

        $company = EntityHelper::createCompany($this->customer);
        if ($companyNumber) {
            $company->setCompanyNumber($companyNumber);
        }
        if ($sameOrder) {
            $company->setOrder($this->order);
        }

        $this->customer->setIsIdCheckRequired($isIdCheckRequired);
        $this->customer->setDtIdValidated($dtIdValidated);
        foreach ($products as $product) {
            list ($productId, $incorporationRequired, $isRefundItem, $dtExported) = $product;
            $item = EntityHelper::createOrderItem($this->order, 20, 2, $productId);
            $item->setIncorporationRequired($incorporationRequired);
            $item->setDtExported($dtExported);
            if ($isRefundItem) {
                $refundItem = EntityHelper::createOrderItem($this->order, 20, 2, $productId);
                $item->setRefundedOrderItem($refundItem);
            }
        }
        $em->flush();

        $filter = new Filter($excluded, $idCheckRequiredProductsIds, $packages, BarclaysBanking::BARCLAYS_BANKING_PRODUCT);
        $filter->setDatesRange(new DatesRange($dtExportedFrom, $dtExportedTo));

        $iterator = $this->object->getIncorporationRequiredAndPackageIterator($filter);
        $this->assertEquals($expectedCount, $iterator->count());

        $this->checkHeader($iterator);
    }

    public function incorporationRequiredAndPackageDataProvider()
    {
        $packages = [1, 2, 3];
        // companyNumber, sameOrder, products(productId, incorporationRequired, isRefundItem, dtExported), excluded, idCheckRequiredProductsIds, package, isIdCheckRequired, idCheckCompletedDate, dtExportedFrom, dtExportedTo, expectedCount
        return [
            ['12345678', TRUE, [[1, TRUE, FALSE, NULL]], [], [], $packages, FALSE, NULL, NULL, NULL, 1],
            ['12345678', TRUE, [[1, TRUE, TRUE, NULL]], [], [], $packages, FALSE, NULL, NULL, NULL, 0], // refund item
            ['12345678', FALSE, [[1, TRUE, FALSE, NULL]], [], [], $packages, FALSE, NULL, NULL, NULL, 0], // different order
            [NULL, TRUE, [[1, TRUE, FALSE, NULL]], [], [], $packages, FALSE, NULL, NULL, NULL, 0], // no company number
            ['12345678', TRUE, [[4, TRUE, FALSE, NULL]], [], [], $packages, FALSE, NULL, NULL, NULL, 0], // product is not a package
            ['12345678', TRUE, [[1, NULL, FALSE, NULL]], [], [], $packages, FALSE, NULL, NULL, NULL, 0], // incorporation not required

            ['12345678', TRUE, [[1, TRUE, FALSE, NULL]], [], [1], $packages, TRUE, NULL, NULL, NULL, 0], // id check required and not validated
            ['12345678', TRUE, [[1, TRUE, FALSE, NULL]], [], [1], $packages, TRUE, new DateTime('-1 day'), NULL, NULL, 1], // id check required and validated
            ['12345678', TRUE, [[1, TRUE, TRUE, NULL]], [], [1], $packages, TRUE, new DateTime('-1 day'), NULL, NULL, 0], // id check required and validated, refund item
            ['12345678', TRUE, [[1, TRUE, FALSE, NULL]], [], [2], $packages, TRUE, NULL, NULL, NULL, 1],
            ['12345678', TRUE, [[1, TRUE, TRUE, NULL]], [], [2], $packages, TRUE, NULL, NULL, NULL, 0], // refund item

            ['12345678', TRUE, [[1, TRUE, FALSE, NULL]], [1], [], $packages, FALSE, NULL, NULL, NULL, 0], // excluded

            ['12345678', TRUE, [[1, TRUE, FALSE, new DateTime('-2 day')]], [], [], $packages, FALSE, NULL, new DateTime('-1 day'), NULL, 0], // dtExportedFrom > dtExported
            ['12345678', TRUE, [[1, TRUE, FALSE, new DateTime('-1 day')]], [], [], $packages, FALSE, NULL, new DateTime('-2 day'), NULL, 1], // dtExportedFrom < dtExported

            ['12345678', TRUE, [[1, TRUE, FALSE, new DateTime('-2 day')]], [], [], $packages, FALSE, NULL, NULL, new DateTime('-1 day'), 1], // dtExportedTo > dtExported
            ['12345678', TRUE, [[1, TRUE, FALSE, new DateTime('-1 day')]], [], [], $packages, FALSE, NULL, NULL, new DateTime('-2 day'), 0], // dtExportedTo < dtExported

            ['12345678', TRUE, [[1, TRUE, FALSE, new DateTime('-2 day')]], [], [], $packages, FALSE, NULL, new DateTime('-3 days'), new DateTime('-1 day'), 1], // dtExported is between
            ['12345678', TRUE, [[1, TRUE, FALSE, new DateTime('-3 day')]], [], [], $packages, FALSE, NULL, new DateTime('-2 days'), new DateTime('-1 day'), 0], // dtExported is in past
            ['12345678', TRUE, [[1, TRUE, FALSE, new DateTime('-1 day')]], [], [], $packages, FALSE, NULL, new DateTime('-3 days'), new DateTime('-2 day'), 0], // dtExported is in future
        ];
    }

    /**
     * @param DibiResultIterator $iterator
     */
    private function checkHeader(DibiResultIterator $iterator)
    {
        /** @var DibiRow $row */
        foreach ($iterator as $row) {
            $this->assertEquals(CsvExporterFactory::$header, array_keys($row->toArray()));
        }
    }

}

