<?php


/**
 * Test class for TokenLogs.
 * Generated by PHPUnit on 2012-09-04 at 09:30:14.
 */
class TokenLogsTest extends PHPUnit_Framework_TestCase {

    /**
     * @var TokenLogs
     */
    protected $object;
    
    
    protected $isNew = 1;
    
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp() {
        
        TestHelper::clearTables(array(TBL_TOKEN_LOGS));
        
        //$this->object = new TokenLogs();
        $this->object = TestTokenHelper::createTokenLog();
        if ($this->object->getId() == 1) {
            $this->isNew = 0;
        }
        
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown() {
        
    }

    /**
     * @covers TokenLogs::getId
     * @todo Implement testGetId().
     */
    public function testGetId() {
        // Remove the following lines when you implement this test.
        $id = $this->object->getId();
        if($this->isNew)
        {
            $this->assertEquals($id,0);
        } else {
            $this->assertEquals($id,1);
        }
        
    }

    /**
     * @covers TokenLogs::isNew
     * @todo Implement testIsNew().
     */
    public function testIsNew() {
        
        if($this->isNew)
        {
            $id = $this->object->isNew();
            $this->assertTrue($id);
        } else {
            $id = $this->object->isNew();
            $this->assertFalse($id);
        }
        
    }

    /**
     * @covers TokenLogs::save
     * @todo Implement testSave().
     */
    public function testSave() {
        //var_dump(TokenLogs::TOKEN_STATUS_INCOLPLETE);
        $this->object->tokenStatus      = TokenLogs::TOKEN_STATUS_ERROR;
        $this->object->token            = 'C00711FD-D54E-7651-F1C2-7CVF';
        $this->object->cardNumber       = '0006';
        $this->object->name             = 'Razvan Preda';
        $this->object->address          = '88 Road, London, GB';
        $this->object->email            = 'razvanp@madesimplegroup.com';
        $this->object->dtc              = new DateTime;
        $this->object->dtm              = new DateTime;
        
        $id = $this->object->save();
        $this->assertEquals($id,1);
        
    }


    /**
     * @covers TokenLogs::getCount
     * @todo Implement testGetCount().
     */
    public function testGetCount() {
        // Remove the following lines when you implement this test.
        $ids = $this->object->getCount();
        if($this->isNew)
        {
            $this->assertEquals($ids,0);
        } else {
            $this->assertEquals($ids,1);
        }
         
        
    }

    /**
     * @covers TokenLogs::getAll
     * @todo Implement testGetAll().
     */
    public function testGetAll() {
        // Remove the following lines when you implement this test.
        $ids = $this->object->getAll();
        if($this->isNew)
         {
             $this->assertEquals(count($ids),0);
         } else {
             $this->assertEquals(count($ids),1);
         }
        
    }

}
