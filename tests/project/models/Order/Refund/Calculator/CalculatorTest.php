<?php

namespace Order\Refund\Calculator;

use PHPUnit_Framework_TestCase;

class CalculatorTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Calculator
     */
    private $object;

    protected function setUp()
    {
        $this->object = new Calculator();
    }

    /**
     * @dataProvider dataProvider
     * @covers Order\Refund\Calculator\Calculator::calculate
     */
    public function testGetSubtotal($itemsRefundTotal, $adminFee, $subtotal, $orderCredit, $totalCreditRefund, $totalMoneyRefund)
    {
        $result = $this->object->calculate($itemsRefundTotal, $adminFee, $orderCredit);
        $this->assertEquals($subtotal, $result->getSubtotal());
    }

    /**
     * @dataProvider dataProvider
     * @covers Order\Refund\Calculator\Calculator::getTotalCreditRefund
     */
    public function testGetTotalCreditRefund($itemsRefundTotal, $adminFee, $subtotal, $orderCredit, $totalCreditRefund, $totalMoneyRefund)
    {
        $result = $this->object->calculate($itemsRefundTotal, $adminFee, $orderCredit);
        $this->assertEquals($totalCreditRefund, $result->getTotalCreditRefund());
    }

    /**
     * @dataProvider dataProvider
     * @covers Order\Refund\Calculator\Calculator::getTotalMoneyRefund
     */
    public function testGetTotalMoneyRefund($itemsRefundTotal, $adminFee, $subtotal, $orderCredit, $totalCreditRefund, $totalMoneyRefund)
    {
        $result = $this->object->calculate($itemsRefundTotal, $adminFee, $orderCredit);
        $this->assertEquals($totalMoneyRefund, $result->getTotalMoneyRefund());
    }

    public function dataProvider()
    {
        return array(
            array(100, 20, 80, 100, 80, 0), // full credit refund
            array(100, 20, 80, 80, 80, 0), // full credit refund
            array(100, 20, 80, 70, 70, 10), // partial money refund
            array(100, 20, 80, 0, 0, 80), // full money refund
            array(100, 0, 100, 0, 0, 100), // full money refund
            array(100, 0, 100, 100, 100, 0), // full credit refund
            array(90, 0, 90, 100, 90, 0), // full credit refund
            array(100, 100, 0, 0, 0, 0), // nothing to refund
            array(100, 110, 0, 0, 0, 0), // admin fee greater than total refund
        );
    }

}
