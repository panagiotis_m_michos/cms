<?php

/**
 * Test class for VoucherNew.
 * Generated by PHPUnit on 2011-10-31 at 12:52:46.
 */
class VoucherNewTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var VoucherNew
	 */
	protected $voucher;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		TestHelper::clearTables(array(TBL_VOUCHERS));
		$this->voucher = TestHelper::createVoucher();
	}

	public function testDiscount() {
		$amount = 30;
		$this->voucher->typeId = VoucherNew::TYPE_AMOUNT;
		$this->voucher->typeValue = 20;
		$this->assertEquals($this->voucher->discount($amount), 20);
		$this->voucher->typeValue = 30;
		$this->assertEquals($this->voucher->discount($amount), 30);
		$this->voucher->typeValue = 40;
		$this->assertEquals($this->voucher->discount($amount), 30);
		
		$amount = 50;
		$this->voucher->typeId = VoucherNew::TYPE_PERCENT;
		$this->voucher->typeValue = 20;
		$this->assertEquals($this->voucher->discount($amount), 10);
		$this->voucher->typeValue = 100;
		$this->assertEquals($this->voucher->discount($amount), 50);
		$this->voucher->typeValue = 200;
		$this->assertEquals($this->voucher->discount($amount), 50);
	}

	public function testIsAvailableForUse() {
		$this->voucher->minSpend = 20;
		$amount = 10;
		$this->assertFalse($this->voucher->isAvailableForUse($amount), 'Min amount not reached');
		$amount = 20;
		$this->assertTrue($this->voucher->isAvailableForUse($amount), 'Min amount should be reached');

		$amount = 30;
		
		$this->voucher->dtExpiry = date('Y-m-d', strtotime('-1 day'));
		$this->assertFalse($this->voucher->isAvailableForUse($amount), 'Voucher should be expired');
		
		$this->voucher->dtExpiry = date('Y-m-d');
		$this->assertTrue($this->voucher->isAvailableForUse($amount), 'Voucher should not be expired');
		
		$this->voucher->usageLimit = 0;
		$this->voucher->used = 10;
		$this->assertTrue($this->voucher->isAvailableForUse($amount), 'Voucher usage limit should not be reached');
		
		$this->voucher->usageLimit = 10;
		$this->voucher->used = 9;
		$this->assertTrue($this->voucher->isAvailableForUse($amount), 'Voucher usage limit should not be reached');
		
		$this->voucher->usageLimit = 10;
		$this->voucher->used = 10;
		$this->assertFalse($this->voucher->isAvailableForUse($amount), 'Voucher usage limit should be reached');
	}

	public function testGetByCode() {
		$voucher = VoucherNew::getByCode($this->voucher->voucherCode);
		$this->assertEquals($voucher->getId(), $this->voucher->getId());
		$voucher = VoucherNew::getByCode('non existand voucher code', 'noThrow');
		$this->assertEmpty($voucher);
		try {
			$voucher = VoucherNew::getByCode('non existand voucher code');
		} catch (Exception $e) {
			return;
		}
		$this->fail('Non existent voucher exception should have been thrown');
	}

	/**
	 * generate 1000 vouchers to test voucher code generator
	 */
	public function testGenerateUniqueCode() {
		$i = 500;
		while($i-- > 0) {
			TestHelper::createVoucher();
		}
	}

}

?>
