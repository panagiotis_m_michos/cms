<?php

namespace Entities;

use Tests\ServicesHelper;
use Tests\Helpers\ObjectHelper;
use Doctrine\Common\Collections\ArrayCollection;
use Utils\Date;

class CompanyTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Company
     */
    protected $object;

    /**
     * @var Service
     */
    protected $serviceServiceMock;

    protected function setUp()
    {
        $this->serviceServiceMock = $this->getMockBuilder('Services\ServiceService')->disableOriginalConstructor()->getMock();

        $customer = new Customer('diviak@gmail.com', 'password');
        $this->object = new Company($customer, 'Test');
    }

    /**
     * @covers Entities\Company::hasActiveService
     */
    public function testHasServicesNoServices()
    {
        $this->assertFalse($this->object->hasActiveService());
    }

    /**
     * @covers Entities\Company::hasActiveService
     */
    public function testHasServicesOneService()
    {
        $this->object->addService(ServicesHelper::getActiveService());
        $this->assertTrue($this->object->hasActiveService());
    }

    /**
     * @covers Entities\Company::hasActiveService
     */
    public function testHasServicesTwoServices()
    {
        $this->object->addService(ServicesHelper::getActiveService());
        $this->object->addService(ServicesHelper::getActiveService());
        $this->assertTrue($this->object->hasActiveService());
    }

    /**
     * @covers Entities\Company::getActiveProductServices
     */
    //public function testGetProductServicesProductTypes()
    //{
    //    $types = array(
    //        Service::TYPE_SERVICE_ADDRESS,
    //        Service::TYPE_ANNUAL_RETURN,
    //    );
    //    $collection = new ArrayCollection();
    //    $this->assertEmpty($this->object->getActiveProductServices($types));
    //
    //    $service = ServicesHelper::getService($types[0]);
    //    $collection->add($service);
    //    $this->object->addService($service);
    //    $this->assertEmpty($this->object->getActiveProductServices($types));
    //
    //    $service = ServicesHelper::getService($types[1]);
    //    $collection->add($service);
    //    $this->object->addService($service);
    //    $this->assertEmpty($this->object->getActiveProductServices($types));
    //}

    /**
     * @covers Entities\Company::getActiveProductServices
     */
    public function testGetProductServicesExpiredProductTypes()
    {
        $types = array(
            Service::TYPE_SERVICE_ADDRESS,
            Service::TYPE_ANNUAL_RETURN,
        );
        $collection = new ArrayCollection();
        $service = ServicesHelper::getService($types[0]);
        $service->setDtStart(new Date('-1 day'));
        $service->setDtExpires(new Date('+1 day'));
        $collection->add($service);
        $this->object->addService($service);
        $this->assertEquals($collection, $this->object->getActiveProductServices($types));

        $service = ServicesHelper::getService($types[1]);
        $service->setDtStart(new Date('-2 day'));
        $service->setDtExpires(new Date('-1 day'));
        $this->object->addService($service);
        $this->assertEquals($collection, $this->object->getActiveProductServices($types));
    }

    /**
     * @covers Entities\Company::getActiveProductServices
     */
    public function testGetProductServicesPackageTypesWithoutActivatedProduct()
    {
        $types = array(
            Service::TYPE_PACKAGE_PRIVACY,
            Service::TYPE_ANNUAL_RETURN,
        );
        $service = ServicesHelper::getService($types[0], TRUE);
        $this->object->addService($service);
        $this->assertCount(0, $this->object->getActiveProductServices($types));

        $service = ServicesHelper::getService($types[1]);
        $this->object->addService($service);
        $services = $this->object->getActiveProductServices($types);
        $this->assertCount(0, $services);
    }

    /**
     * @covers Entities\Company::getActiveProductServices
     */
    public function testGetProductServicesPackageTypesWithActivatedProduct()
    {
        $types = array(
            Service::TYPE_PACKAGE_PRIVACY,
            Service::TYPE_ANNUAL_RETURN,
        );
        $service = ServicesHelper::getService($types[0], TRUE);
        $service->setDtStart(new Date('-1 day'));
        $service->setDtExpires(new Date('+1 day'));
        $this->object->addService($service);
        $this->assertCount(0, $this->object->getActiveProductServices($types));

        $service = ServicesHelper::getService($types[1]);
        $this->object->addService($service);
        $service->setDtStart(new Date('-1 day'));
        $service->setDtExpires(new Date('+1 day'));
        $services = $this->object->getActiveProductServices($types);
        $this->assertCount(1, $services);
        $this->assertEquals($types[1], $services->first()->getServiceTypeId());
    }

    /**
     * @covers Entities\Company::getLastPackageService
     */
    public function testGetLastPackageServiceWithNotActivatedPackageType()
    {
        $types = array(
            Service::TYPE_PACKAGE_PRIVACY,
        );
        $service1 = ObjectHelper::createService($this->object, NULL, NULL, FALSE, $types[0]);
        $this->assertEquals($service1, $this->object->getLastPackageService($types[0]));
    }

    /**
     * @covers Entities\Company::getLastPackageService
     */
    public function testGetLastPackageServiceWithPackageType()
    {
        $types = array(
            Service::TYPE_PACKAGE_PRIVACY,
        );
        $service1 = ObjectHelper::createService($this->object, new Date(), new Date('+1 month'), FALSE, $types[0]);
        $this->assertEquals($service1, $this->object->getLastPackageService($types[0]));
        $service2 = ObjectHelper::createService($this->object, new Date('+1 month'), new Date('+2 month'), FALSE, $types[0]);
        $this->assertEquals($service2, $this->object->getLastPackageService($types[0]));
    }

    /**
     * @covers Entities\Company::getLastPackageService
     */
    public function testGetLastPackageServiceWithProductType()
    {
        $types = array(
            Service::TYPE_PACKAGE_PRIVACY,
            Service::TYPE_ANNUAL_RETURN,
        );
        $service1 = ObjectHelper::createService($this->object, new Date(), new Date('+1 month'), FALSE, $types[0]);
        $productService1 = ObjectHelper::createService($this->object, NULL, NULL, TRUE, $types[1]);
        $service1->addChild($productService1);
        $this->assertEquals($service1, $this->object->getLastPackageService($types[1]));
    }

    /**
     * @covers Entities\Company::getLastPackageService
     */
    public function testGetLastPackageServiceWithProductTypeInTwoExistingServices()
    {
        $types = array(
            Service::TYPE_PACKAGE_PRIVACY,
            Service::TYPE_ANNUAL_RETURN,
        );
        $service1 = ObjectHelper::createService($this->object, new Date(), new Date('+1 month'), FALSE, $types[0]);
        $productService = ObjectHelper::createService($this->object, NULL, NULL, TRUE, $types[1]);
        $service1->addChild($productService);
        $service2 = ObjectHelper::createService($this->object, new Date('+1 month'), new Date('+2 month'), FALSE, $types[0]);
        $service2->addChild($productService);
        $this->assertEquals($service2, $this->object->getLastPackageService($types[1]));
    }
}
