<?php

namespace Entities;

use PHPUnit_Framework_TestCase;
use Tests\Helpers\ObjectHelper;

class CashbackTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Cashback
     */
    protected $cashback;

    /**
     * @covers Entities\Cashback::getStatusId
     */
    public function testGetStatusIdImported()
    {
        $cashback = ObjectHelper::createRetailCashback(Cashback::STATUS_IMPORTED);
        $this->assertEquals($cashback->getStatusId(), Cashback::STATUS_IMPORTED);
    }

    /**
     * @covers Entities\Cashback::getStatusId
     */
    public function testGetStatusIdEligible()
    {
        $cashback = ObjectHelper::createRetailCashback(Cashback::STATUS_ELIGIBLE);
        $this->assertEquals($cashback->getStatusId(), Cashback::STATUS_ELIGIBLE);
    }

    /**
     * @covers Entities\Cashback::getStatusId
     */
    public function testGetStatusIdPaid()
    {
        $cashback = ObjectHelper::createRetailCashback(Cashback::STATUS_PAID);
        $this->assertEquals($cashback->getStatusId(), Cashback::STATUS_PAID);
    }

    /**
     * @covers Entities\Cashback::getStatus
     */
    public function testGetStatusImported()
    {
        $cashback = $this->getCashbackMock(array('getStatusId'));
        $cashback->expects($this->once())->method('getStatusId')->will($this->returnValue(Cashback::STATUS_IMPORTED));
        $this->assertEquals($cashback->getStatus(), Cashback::$statuses[Cashback::STATUS_IMPORTED]);
    }

    /**
     * @covers Entities\Cashback::getStatus
     */
    public function testGetStatusNotImported()
    {
        $cashback = $this->getCashbackMock(array('getStatusId'));
        $cashback->expects($this->once())->method('getStatusId')->will($this->returnValue(Cashback::STATUS_PAID));
        $this->assertNotEquals($cashback->getStatus(), Cashback::$statuses[Cashback::STATUS_IMPORTED]);
    }

    /**
     * @covers Entities\Cashback::getStatus
     */
    public function testGetStatusEligible()
    {
        $cashback = $this->getCashbackMock(array('getStatusId'));
        $cashback->expects($this->once())->method('getStatusId')->will($this->returnValue(Cashback::STATUS_ELIGIBLE));
        $this->assertEquals($cashback->getStatus(), Cashback::$statuses[Cashback::STATUS_ELIGIBLE]);
    }

    /**
     * @covers Entities\Cashback::getStatus
     */
    public function testGetStatusNotEligible()
    {
        $cashback = $this->getCashbackMock(array('getStatusId'));
        $cashback->expects($this->once())->method('getStatusId')->will($this->returnValue(Cashback::STATUS_IMPORTED));
        $this->assertNotEquals($cashback->getStatus(), Cashback::$statuses[Cashback::STATUS_ELIGIBLE]);
    }

    /**
     * @covers Entities\Cashback::getStatus
     */
    public function testGetStatusPaid()
    {
        $cashback = $this->getCashbackMock(array('getStatusId'));
        $cashback->expects($this->once())->method('getStatusId')->will($this->returnValue(Cashback::STATUS_PAID));
        $this->assertEquals($cashback->getStatus(), Cashback::$statuses[Cashback::STATUS_PAID]);
    }

    /**
     * @covers Entities\Cashback::getStatus
     */
    public function testGetStatusNotPaid()
    {
        $cashback = $this->getCashbackMock(array('getStatusId'));
        $cashback->expects($this->once())->method('getStatusId')->will($this->returnValue(Cashback::STATUS_ELIGIBLE));
        $this->assertNotEquals($cashback->getStatus(), Cashback::$statuses[Cashback::STATUS_PAID]);
    }

    /**
     * @covers Entities\Cashback::getStatusId
     */
    public function testGetStatusNotExisting()
    {
        $cashback = $this->getCashbackMock(array('getStatusId'));
        $cashback->expects($this->once())->method('getStatusId')->will($this->returnValue('UNKNOWN_STATUS'));
        $this->assertNull($cashback->getStatus());
    }

    /**
     * @param array $mockMethods
     * @return Cashback
     */
    private function getCashbackMock(array $mockMethods = array())
    {
        return $this->getMockBuilder('Entities\Cashback')
            ->disableOriginalConstructor()
            ->setMethods($mockMethods)
            ->getMock();
    }

}
