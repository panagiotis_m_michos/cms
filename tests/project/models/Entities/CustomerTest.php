<?php

namespace Entities;

use PHPUnit_Framework_TestCase;

class CustomerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Customer
     */
    protected $customer1;

    /**
     * @var Customer
     */
    protected $customer2;

    protected function setUp()
    {
        $this->customer1 = $this->getMockBuilder('Entities\Customer')
            ->disableOriginalConstructor()
            ->setMethods(array('getId'))
            ->getMock();
        $this->customer2 = $this->getMockBuilder('Entities\Customer')
            ->disableOriginalConstructor()
            ->setMethods(array('getId'))
            ->getMock();
    }

    /**
     * @covers Entities\Customer::isEqual
     */
    public function testIsEqualNotSame()
    {
        $this->customer1
            ->expects($this->once())
            ->method('getId')
            ->will($this->returnValue(1));
        $this->customer2
            ->expects($this->once())
            ->method('getId')
            ->will($this->returnValue(2));
        $this->assertFalse($this->customer1->isEqual($this->customer2));
    }

    /**
     * @covers Entities\Customer::isEqual
     */
    public function testIsEqualSame()
    {
        $this->customer1
            ->expects($this->once())
            ->method('getId')
            ->will($this->returnValue(1));
        $this->customer2
            ->expects($this->once())
            ->method('getId')
            ->will($this->returnValue(1));
        $this->assertTrue($this->customer1->isEqual($this->customer2));
    }


    /**
     * @test
     * @dataProvider creditsDataProvider
     */
    public function addCredit($initialCredit, $addedCredit, $expectedCredit)
    {
        $this->customer1->setCredit($initialCredit);
        $this->customer1->addCredit($addedCredit);
        $this->assertEquals($expectedCredit, $this->customer1->getCredit());
    }

    public function creditsDataProvider()
    {
        return array(
            array(10, 5, 15),
            array(10, 0, 10),
            array(0, 0, 0),
            array(10, -10, 10),
            array(0, -10, 0),
        );
    }


}
