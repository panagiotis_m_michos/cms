<?php

namespace Repositories;

use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use Entities\Company;
use Entities\Customer;
use Entities\Order;
use Entities\OrderItem;
use Entities\Payment\Token;
use Entities\Service;
use EntityHelper;
use EventLocator;
use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;
use Tests\ServiceSettingsHelper;
use Utils\Date;

class ServiceRepositoryTest extends PHPUnit_Framework_TestCase
{

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var ServiceRepository
     */
    private $object;

    /**
     * @var Customer|PHPUnit_Framework_MockObject_MockObject
     */
    private $customer1;

    /**
     * @var Customer|PHPUnit_Framework_MockObject_MockObject
     */
    private $customer2;

    /**
     * @var Company|PHPUnit_Framework_MockObject_MockObject
     */
    private $company1;

    /**
     * @var Company|PHPUnit_Framework_MockObject_MockObject
     */
    private $company2;

    /**
     * @var Order|PHPUnit_Framework_MockObject_MockObject
     */
    private $order1;

    /**
     * @var Order|PHPUnit_Framework_MockObject_MockObject
     */
    private $order2;

    /**
     * @var OrderItem|PHPUnit_Framework_MockObject_MockObject
     */
    private $orderItem1;

    /**
     * @var OrderItem|PHPUnit_Framework_MockObject_MockObject
     */
    private $orderItem2;

    /**
     * @var Token|PHPUnit_Framework_MockObject_MockObject
     */
    private $token1;

    /**
     * @var Token|PHPUnit_Framework_MockObject_MockObject
     */
    private $token2;

    protected function setUp()
    {
        EntityHelper::emptyTables([TBL_CUSTOMERS, TBL_COMPANY, TBL_SERVICES, TBL_ORDERS, TBL_EVENTS, TBL_TOKENS, TBL_SERVICE_SETTINGS]);

        $this->em = EntityHelper::getEntityManager();
        $this->object = new ServiceRepository($this->em, new ClassMetadata('Entities\Service'));

        $this->customer1 = EntityHelper::createCustomer(TEST_EMAIL1);
        $this->order1 = EntityHelper::createOrder($this->customer1);
        $this->orderItem1 = $this->order1->getItems()->first();
        $this->company1 = EntityHelper::createCompany($this->customer1);
        $this->token1 = EntityHelper::createToken($this->customer1, TRUE, new Date('2020-12-31'));

        $this->customer2 = EntityHelper::createCustomer(TEST_EMAIL2);
        $this->order2 = EntityHelper::createOrder($this->customer2);
        $this->orderItem2 = $this->order2->getItems()->first();
        $this->company2 = EntityHelper::createCompany($this->customer2);
        $this->token2 = EntityHelper::createToken($this->customer2, TRUE, new Date('2020-12-31'));
    }

    /**
     * @covers Repositories\ServiceRepository::getServicesDataExpiringWithinDatesForEvent
     */
    public function testOneServiceValid()
    {
        $service1 = EntityHelper::createService($this->company1, $this->orderItem1, new DateTime, new DateTime('+10 days'));
        $settings1 = ServiceSettingsHelper::fromService($service1, TRUE, $this->token1);

        $this->em->persist($settings1);
        $this->em->flush();

        $serviceIterator = $this->object->getServicesDataExpiringWithinDatesForEvent(
            new Date('+9 days'),
            new Date('+11 days'),
            EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_EXPIRES_IN_7
        );

        $expectedServices = [$service1];
        $actualServices = [];

        foreach ($serviceIterator as $serviceData) {
            $actualServices[] = $serviceData[0];
        }

        $this->assertEquals($expectedServices, $actualServices);
    }

    /**
     * @covers Repositories\ServiceRepository::getServicesDataExpiringWithinDatesForEvent
     */
    public function testMultipleServicesValid()
    {
        $service1 = EntityHelper::createService($this->company1, $this->orderItem1, new DateTime, new DateTime('+10 days'));
        $service2 = EntityHelper::createService($this->company1, $this->orderItem1, new DateTime('+11 days'), new DateTime('+30 days'));
        $settings1 = ServiceSettingsHelper::fromService($service1, TRUE, $this->token1);

        $this->em->persist($settings1);
        $this->em->flush();

        $serviceIterator = $this->object->getServicesDataExpiringWithinDatesForEvent(
            new Date('+29 days'),
            new Date('+31 days'),
            EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_EXPIRES_IN_7
        );

        $expectedServices = [$service2];
        $actualServices = [];

        foreach ($serviceIterator as $serviceData) {
            $actualServices[] = $serviceData[0];
        }

        $this->assertEquals($expectedServices, $actualServices);
    }

    /**
     * @covers Repositories\ServiceRepository::getServicesDataExpiringWithinDatesForEvent
     */
    public function testCardExpiresBeforeService()
    {
        $token = EntityHelper::createToken($this->customer1, TRUE, new Date('2015-02-28'));
        $service1 = EntityHelper::createService($this->company1, $this->orderItem1, new DateTime('2014-01-01'), new DateTime('2015-03-01'));
        $settings1 = ServiceSettingsHelper::fromService($service1, TRUE, $token);

        $this->em->persist($settings1);
        $this->em->flush();

        $serviceIterator = $this->object->getServicesDataExpiringWithinDatesForEvent(
            new Date('2015-02-01'),
            new Date('2015-04-01'),
            EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_EXPIRES_IN_7
        );

        $actualServices = [];
        foreach ($serviceIterator as $serviceData) {
            $actualServices[] = $serviceData[0];
        }

        $this->assertEmpty($actualServices);
    }

    /**
     * @covers Repositories\ServiceRepository::getServicesDataExpiringWithinDatesForEvent
     */
    public function testOneServiceAlreadySent()
    {
        $service1 = EntityHelper::createService($this->company1, $this->orderItem1, new DateTime, new DateTime('+10 days'));
        $settings1 = ServiceSettingsHelper::fromService($service1, TRUE, $this->token1);

        $this->em->persist($settings1);
        $this->em->flush();

        EntityHelper::createEvent(
            EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_EXPIRES_IN_7,
            $service1->getServiceId()
        );

        $serviceIterator = $this->object->getServicesDataExpiringWithinDatesForEvent(
            new Date('+29 days'),
            new Date('+31 days'),
            EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_EXPIRES_IN_7
        );

        $expectedServices = [];
        $actualServices = [];

        foreach ($serviceIterator as $serviceData) {
            $actualServices[] = $serviceData[0];
        }

        $this->assertEquals($expectedServices, $actualServices);
    }

    /**
     * @covers Repositories\ServiceRepository::getServicesDataExpiringWithinDatesForEvent
     */
    public function testMultipleServicesOneAlreadySent()
    {
        $service1 = EntityHelper::createService($this->company1, $this->orderItem1, new DateTime, new DateTime('+10 days'), Service::TYPE_REGISTERED_OFFICE);
        $settings1 = ServiceSettingsHelper::fromService($service1, TRUE, $this->token1);

        $service2 = EntityHelper::createService($this->company1, $this->orderItem1, new DateTime('+5 days'), new DateTime('+10 days'), Service::TYPE_SERVICE_ADDRESS);
        $settings2 = ServiceSettingsHelper::fromService($service2, TRUE, $this->token2);

        $this->em->persist($settings1);
        $this->em->persist($settings2);
        $this->em->flush();

        EntityHelper::createEvent(
            EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_EXPIRES_IN_7,
            $service2->getServiceId()
        );

        $serviceIterator = $this->object->getServicesDataExpiringWithinDatesForEvent(
            new Date('+7 days'),
            new Date('+12 days'),
            EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_EXPIRES_IN_7
        );

        $expectedServices = [$service1];
        $actualServices = [];

        foreach ($serviceIterator as $serviceData) {
            $actualServices[] = $serviceData[0];
        }

        $this->assertEquals($expectedServices, $actualServices);
    }

    /**
     * @covers Repositories\ServiceRepository::getServicesDataExpiringWithinDatesForEvent
     */
    public function testMultipleCustomersMultipleServices()
    {
        $service1 = EntityHelper::createService($this->company1, $this->orderItem1, new DateTime, new DateTime('+11 days'), Service::TYPE_REGISTERED_OFFICE);
        $settings1 = ServiceSettingsHelper::fromService($service1, TRUE, $this->token1);

        $service2 = EntityHelper::createService($this->company2, $this->orderItem2, new DateTime('+5 days'), new DateTime('+11 days'), Service::TYPE_SERVICE_ADDRESS);
        $settings2 = ServiceSettingsHelper::fromService($service2, TRUE, $this->token2);

        $this->em->persist($settings1);
        $this->em->persist($settings2);
        $this->em->flush();

        $serviceIterator = $this->object->getServicesDataExpiringWithinDatesForEvent(
            new Date('+7 days'),
            new Date('+12 days'),
            EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_EXPIRES_IN_7
        );

        $expectedServices = [$service1, $service2];
        $actualServices = [];

        foreach ($serviceIterator as $serviceData) {
            $actualServices[] = $serviceData[0];
        }

        $this->assertEquals($expectedServices, $actualServices);
    }

    /**
     * @covers Repositories\ServiceRepository::getServicesDataExpiringWithinDatesForEvent
     */
    public function testMultipleServicesCurrentIsDisabled()
    {
        $service1 = EntityHelper::createService($this->company1, $this->orderItem1, new DateTime, new DateTime('+11 days'), Service::TYPE_REGISTERED_OFFICE);
        $settings1 = ServiceSettingsHelper::fromService($service1, TRUE, $this->token1);

        $service2 = EntityHelper::createService($this->company1, $this->orderItem1, new DateTime('+12 days'), new DateTime('+20 days'), Service::TYPE_REGISTERED_OFFICE);
        $service2->setStateId(Service::STATE_DISABLED);
        $settings2 = ServiceSettingsHelper::fromService($service2, TRUE, $this->token2);

        $this->em->persist($settings1);
        $this->em->persist($settings2);
        $this->em->flush();

        $serviceIterator = $this->object->getServicesDataExpiringWithinDatesForEvent(
            new Date('+20 days'),
            new Date('+20 days'),
            EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_EXPIRES_IN_7
        );

        $expectedServices = [];
        $actualServices = [];

        foreach ($serviceIterator as $serviceData) {
            $actualServices[] = $serviceData[0];
        }

        $this->assertEquals($expectedServices, $actualServices);
    }
}
