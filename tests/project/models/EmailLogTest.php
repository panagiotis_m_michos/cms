<?php

use Tests\Helpers\ObjectHelper;

class EmailLogTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var EmailLog
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        TestHelper::clearTables(array('cms2_email_logs'));
        $this->object = new EmailLog;
    }

    /**
     * @covers EmailLog::__construct
     */
    public function testCanInstantiate()
    {
        $this->assertInstanceOf('EmailLog', new EmailLog());
    }

    /**
     * @covers EmailLog::getId
     * @covers EmailLog::setEmailLogId
     * @todo: rename method (or create one) to getEmailLogId()
     */
    public function testGetId()
    {
        $this->assertEquals(0, $this->object->getId());
        $this->object->setEmailLogId(2);
        $this->assertEquals(2, $this->object->getId());
    }

    /**
     * @covers EmailLog::isNew
     */
    public function testIsNew()
    {
        $this->assertEquals(TRUE, $this->object->isNew());
    }

    /**
     * @covers EmailLog::save
     */
    public function testSave()
    {
        $emailLogId = $this->createSampleEmailLog();
        $emailLog = new EmailLog($emailLogId);
        $this->assertEquals('body', $emailLog->getEmailBody());
    }

    /**
     * @covers EmailLog::delete
     */
    public function testDelete()
    {
        $this->object->setEmailTo('to@to.com');
        $this->object->setEmailFrom('from@from.com');
        $this->object->setEmailSubject('subject');
        $this->object->setEmailBody('body');
        $this->object->setAttachment(0);

        $emailLogId = $this->object->save();
        $emailLog = new EmailLog($emailLogId);
        $this->assertInstanceOf('EmailLog', $emailLog);
        $emailLog->delete();
        $this->setExpectedException('EntityNotFound');
        new EmailLog($emailLogId);

    }

    /**
     * @depends testSave
     * @covers EmailLog::getCount
     */
    public function testGetCount()
    {
        $emailLogId = $this->createSampleEmailLog();
        $this->assertEquals(1, $this->object->getCount());
        $this->assertEquals(1, $this->object->getCount(array('emailLogId' => $emailLogId)));
        $this->assertEquals(0, $this->object->getCount(array('emailLogId' => -1)));
    }

    /**
     * @depends testSave
     * @covers EmailLog::getAll
     */
    public function testGetAll()
    {
        $emailLogId = $this->createSampleEmailLog();
        $emailLogRows = $this->object->getAll();
        $this->assertEquals(1, $emailLogRows[1]['emailLogId']);
        $this->assertCount(1, $this->object->getAll());
        $this->assertCount(1, $this->object->getAll(1, 0));
        $this->assertCount(1, $this->object->getAll(1, 0, array('emailLogId' => $emailLogId)));
        $this->assertCount(0, $this->object->getAll(1, 1));
    }

    /**
     * @depends testSave
     * @covers EmailLog::getAllObjects
     */
    public function testGetAllObjects()
    {
        $emailLogId = $this->createSampleEmailLog();

        $emailLogs = $this->object->getAllObjects();
        $emailLog = $emailLogs[$emailLogId];

        $this->assertCount(1, $emailLogs);
        $this->assertInstanceOf('EmailLog', $emailLog);
        $this->assertEquals($emailLogId, $emailLog->getId());

    }

    /**
     * @depends testSave
     * @covers EmailLog::getCustomerId
     * @covers EmailLog::setCustomerId
     */
    public function testGetCustomerId()
    {
        $this->assertEquals(NULL, $this->object->getCustomerId());
        $this->object->setCustomerId(1);
        $this->assertEquals(1, $this->object->getCustomerId());
    }

    /**
     * @covers EmailLog::getCustomer
     * @covers EmailLog::setCustomerId
     */
    public function testGetCustomer()
    {
        $customerOnNewEmailLog = $this->object->getCustomer();
        $this->assertInstanceOf('Customer', $customerOnNewEmailLog);
        $this->assertEquals(TRUE, $customerOnNewEmailLog->isNew());
        $customer = ObjectHelper::createCustomer('mail@mail.com');
        $this->object->setCustomerId($customer->getCustomerId());
        $setCustomer = $this->object->getCustomer();
        $this->assertEquals($customer->getCustomerId(), $setCustomer->getId());
    }

    /**
     * @covers EmailLog::hasCustomer
     * @covers EmailLog::setCustomerId
     */
    public function testHasCustomer()
    {
        $this->assertEquals(FALSE, $this->object->hasCustomer());
        $this->object->setCustomerId(1);
        $this->assertEquals(TRUE, $this->object->hasCustomer());
    }

    /**
     * @covers EmailLog::getEmailName
     * @covers EmailLog::setEmailName
     */
    public function testGetEmailName()
    {
        $emailName = 'name';
        $this->assertEquals(NULL, $this->object->getEmailName());
        $this->object->setEmailName($emailName);
        $this->assertEquals($emailName, $this->object->getEmailName());
    }

    /**
     * @covers EmailLog::getEmailNodeId
     * @covers EmailLog::setEmailNodeId
     */
    public function testGetEmailNodeId()
    {
        $this->assertEquals(0, $this->object->getEmailNodeId());
        $this->object->setEmailNodeId(2);
        $this->assertEquals(2, $this->object->getEmailNodeId());
    }

    /**
     * @covers EmailLog::getEmailFrom
     * @covers EmailLog::setEmailFrom
     */
    public function testGetEmailFrom()
    {
        $emailFrom = 'from@from.com';
        $this->assertEquals(NULL, $this->object->getEmailFrom());
        $this->object->setEmailFrom($emailFrom);
        $this->assertEquals($emailFrom, $this->object->getEmailFrom());
    }

    /**
     * @covers EmailLog::getEmailTo
     * @covers EmailLog::setEmailTo
     */
    public function testGetEmailTo()
    {
        $emailTo = 'to@to.com';
        $this->assertEquals(NULL, $this->object->getEmailTo());
        $this->object->setEmailTo($emailTo);
        $this->assertEquals($emailTo, $this->object->getEmailTo());
    }

    /**
     * @covers EmailLog::getEmailSubject
     * @covers EmailLog::setEmailSubject
     */
    public function testGetEmailSubject()
    {
        $emailSubject = 'Email Subject';
        $this->assertEquals(NULL, $this->object->getEmailSubject());
        $this->object->setEmailSubject($emailSubject);
        $this->assertEquals($emailSubject, $this->object->getEmailSubject());
    }

    /**
     * @covers EmailLog::getEmailBody
     * @covers EmailLog::setEmailBody
     */
    public function testGetEmailBody()
    {
        $emailBody = 'Email Body';
        $this->assertEquals(NULL, $this->object->getEmailBody());
        $this->object->setEmailBody($emailBody);
        $this->assertEquals($emailBody, $this->object->getEmailBody());
    }

    /**
     * @covers EmailLog::getAttachment
     * @covers EmailLog::setAttachment
     */
    public function testGetAttachment()
    {
        $this->assertEquals(NULL, $this->object->getAttachment());
        $this->object->setAttachment(1);
        $this->assertEquals(1, $this->object->getAttachment());
    }

    /**
     * @covers EmailLog::getDtc
     * @covers EmailLog::setDtc
     */
    public function testGetDtc()
    {
        $dtc = new DateTime();
        $this->assertEquals(NULL, $this->object->getDtc());
        $this->object->setDtc($dtc);
        $this->assertEquals($dtc, $this->object->getDtc());
    }

    /**
     * @covers EmailLog::getDtm
     * @covers EmailLog::setDtm
     */
    public function testGetDtm()
    {
        $dtm = new DateTime();
        $this->assertEquals(NULL, $this->object->getDtm());
        $this->object->setDtm($dtm);
        $this->assertEquals($dtm, $this->object->getDtm());
    }

    /**
     * @return int
     */
    private function createSampleEmailLog()
    {
        $el = new EmailLog();
        $el->setEmailTo('to@to.com');
        $el->setEmailFrom('from@from.com');
        $el->setEmailSubject('subject');
        $el->setEmailBody('body');
        $el->setAttachment(0);
        return $el->save();
    }
}
