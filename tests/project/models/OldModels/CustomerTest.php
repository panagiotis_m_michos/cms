<?php
/**
 * Created by PhpStorm.
 * User: tomas
 * Date: 23/10/14
 * Time: 14:22
 */

class CustomerTest extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        EntityHelper::emptyTables(array(TBL_CUSTOMERS, TBL_ORDERS));
    }

    /**
     * @covers Customer::getCustomerOrderCount
     */
    public function testGetCustomerOrderCount()
    {
        $customer = EntityHelper::createCustomer();
        $oldCustomer = new Customer($customer->getId());
        $this->assertEquals(0, $oldCustomer->getCustomerOrderCount());
        EntityHelper::createOrder($customer);
        EntityHelper::createOrder($customer);
        $this->assertEquals(2, $oldCustomer->getCustomerOrderCount());
    }

}
 