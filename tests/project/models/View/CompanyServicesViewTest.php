<?php

namespace Models\View\Front;

use DateTime;
use Entities\Company;
use Entities\Service;
use PHPUnit_Framework_TestCase;
use tests\helpers\MockHelper;

class CompanyServicesViewTest extends PHPUnit_Framework_TestCase
{

    /**
     * @var CompanyServicesView
     */
    protected $object;

    /**
     * @var array
     */
    protected $services = array();

    /**
     * @var Company
     */
    protected $company;

    /**
     * @covers Models\View\Front\CompanyServicesView::getDaysRange
     */
    public function testGetDaysRange()
    {
        $companyDummy = $this->getMockBuilder('Entities\Company')->disableOriginalConstructor()->getMock();
        $serviceViewFactoryDummy = MockHelper::mock($this, 'Factories\ServiceViewFactory');
        $eventServiceDummy = MockHelper::mock($this, 'Services\EventService');

        $this->object = new CompanyServicesView($companyDummy, $serviceViewFactoryDummy, $eventServiceDummy);
        $this->assertEquals(28, $this->object->getDaysRange(new DateTime('+28 days 00:00:00')));
        $this->assertEquals(15, $this->object->getDaysRange(new DateTime('+15 days 00:00:00')));
        $this->assertEquals(1, $this->object->getDaysRange(new DateTime('+1 days 00:00:00')));
        $this->assertEquals(0, $this->object->getDaysRange(new DateTime('+0 days 00:00:00')));
        $this->assertEquals(-15, $this->object->getDaysRange(new DateTime('-15 days 00:00:00')));
        $this->assertEquals(-28, $this->object->getDaysRange(new DateTime('-28 days 00:00:00')));
        $this->assertEquals(-10, $this->object->getDaysRange(new DateTime('-10 days 00:00:00')));
    }

    /**
     * @dataProvider dataHasExpiringServicesNoServices
     * @covers Models\View\Front\CompanyServicesView::hasServicesWithoutEventExpiringIn
     * @param int $checkDays
     */
    public function testCanCheckCompanyForServicesExpiringWithNoServices($checkDays)
    {
        $companyMock = MockHelper::mock($this, 'Entities\Company');
        $serviceViewFactoryMock = MockHelper::mock($this, 'Factories\ServiceViewFactory');
        $eventServiceMock = MockHelper::mock($this, 'Services\EventService');
        $companyMock->expects($this->once())->method('getEnabledActivatedParentServices')->willReturn([]);

        $this->object = new CompanyServicesView($companyMock, $serviceViewFactoryMock, $eventServiceMock);
        $this->assertFalse($this->object->hasServicesWithoutEventExpiringIn($checkDays, 'dummy.event'));
    }

    /**
     * @return array
     */
    public function dataHasExpiringServicesNoServices()
    {
        // checkDays
        return [
            [-10],
            [0],
            [10],
        ];
    }

    /**
     * @dataProvider dataHasExpiringServicesOneService
     * @covers       Models\View\Front\CompanyServicesView::hasServicesWithoutEventExpiringIn
     * @param int $checkDays
     * @param DateTime $serviceDtExpires
     * @param bool $serviceViewCanReceiveReminders
     * @param bool $expectedResult
     */
    public function testCanCheckCompanyForServicesExpiringWithOneService(
        $checkDays,
        $serviceDtExpires,
        $event,
        $eventOccurred,
        $serviceViewCanReceiveReminders,
        $expectedResult
    )
    {
        $eventServiceMock = MockHelper::mock($this, 'Services\EventService');
        $eventServiceMock->expects($this->any())->method('eventOccurred')->with($event, 1)->willReturn($eventOccurred);

        $serviceMock1 = MockHelper::mock($this, 'Entities\Service');
        $serviceMock1->expects($this->any())->method('getServiceTypeId')->willReturn(Service::TYPE_SERVICE_ADDRESS);
        $serviceMock1->expects($this->any())->method('getDtExpires')->willReturn($serviceDtExpires);

        $serviceViewMock = MockHelper::mock($this, 'Models\View\ServiceView');
        $serviceViewMock->method('getId')->willReturn(1);
        $serviceViewMock->method('getDtExpires')->willReturn($serviceMock1->getDtExpires());
        $serviceViewMock->method('canReceiveReminders')->willReturn($serviceViewCanReceiveReminders);

        $companyMock = MockHelper::mock($this, 'Entities\Company');
        $companyMock->expects($this->once())->method('getEnabledActivatedParentServices')->willReturn([$serviceMock1]);

        $serviceViewFactoryMock = MockHelper::mock($this, 'Factories\ServiceViewFactory');
        $serviceViewFactoryMock->expects($this->once())->method('create')->with([$serviceMock1])->willReturn($serviceViewMock);

        $this->object = new CompanyServicesView($companyMock, $serviceViewFactoryMock, $eventServiceMock);
        $this->assertEquals($expectedResult, $this->object->hasServicesWithoutEventExpiringIn($checkDays, $event));
    }

    /**
     * @return array
     */
    public function dataHasExpiringServicesOneService()
    {
        // checkDays, serviceDtExpires, event, eventOccurred, serviceViewCanReceiveReminders, expectedResult
        return [
            [-10, new DateTime('-10 days'), 'event', FALSE, TRUE, TRUE],
            [-10, new DateTime('-10 days'), 'event', TRUE, TRUE, FALSE],
            [-10, new DateTime('-10 days'), 'event', FALSE, FALSE, FALSE],
            [-10, new DateTime('-10 days'), 'event', TRUE, FALSE, FALSE],
            [-10, new DateTime('-9 days'), 'event', FALSE, TRUE, FALSE],
            [-10, new DateTime('-9 days'), 'event', TRUE, TRUE, FALSE],
            [-10, new DateTime('-9 days'), 'event', FALSE, FALSE, FALSE],
            [-10, new DateTime('-9 days'), 'event', TRUE, FALSE, FALSE],

            [0, new DateTime('+0 days'), 'event', FALSE, TRUE, TRUE],
            [0, new DateTime('+0 days'), 'event', TRUE, TRUE, FALSE],
            [0, new DateTime('+0 days'), 'event', FALSE, FALSE, FALSE],
            [0, new DateTime('+0 days'), 'event', TRUE, FALSE, FALSE],
            [0, new DateTime('+1 days'), 'event', FALSE, TRUE, FALSE],
            [0, new DateTime('+1 days'), 'event', TRUE, TRUE, FALSE],
            [0, new DateTime('+1 days'), 'event', FALSE, FALSE, FALSE],
            [0, new DateTime('+1 days'), 'event', TRUE, FALSE, FALSE],

            [10, new DateTime('+10 days'), 'event', FALSE, TRUE, TRUE],
            [10, new DateTime('+10 days'), 'event', TRUE, TRUE, FALSE],
            [10, new DateTime('+10 days'), 'event', FALSE, FALSE, FALSE],
            [10, new DateTime('+10 days'), 'event', TRUE, FALSE, FALSE],
            [10, new DateTime('+9 days'), 'event', FALSE, TRUE, FALSE],
            [10, new DateTime('+9 days'), 'event', TRUE, TRUE, FALSE],
            [10, new DateTime('+9 days'), 'event', FALSE, FALSE, FALSE],
            [10, new DateTime('+9 days'), 'event', TRUE, FALSE, FALSE],
        ];
    }

    /**
     * @dataProvider dataGetExpiringServicesNoServices
     * @covers Models\View\Front\CompanyServicesView::getServicesWithoutEventExpiringIn
     * @param int $checkDays
     */
    public function testWillNotGetCompanyForServicesExpiringWithNoServices($checkDays)
    {
        $eventServiceMock = MockHelper::mock($this, 'Services\EventService');
        $companyMock = MockHelper::mock($this, 'Entities\Company');
        $companyMock->expects($this->once())->method('getEnabledActivatedParentServices')->willReturn([]);

        $serviceViewFactoryMock = MockHelper::mock($this, 'Factories\ServiceViewFactory');

        $this->object = new CompanyServicesView($companyMock, $serviceViewFactoryMock, $eventServiceMock);
        $this->assertEmpty($this->object->getServicesWithoutEventExpiringIn($checkDays, 'dummy.event'));
    }

    /**
     * @return array
     */
    public function dataGetExpiringServicesNoServices()
    {
        // checkDays
        return [
            [-10],
            [0],
            [10],
        ];
    }

    /**
     * @dataProvider dataGetExpiringServicesOneService
     * @covers       Models\View\Front\CompanyServicesView::getServicesWithoutEventExpiringIn
     * @param int $checkDays
     * @param DateTime $serviceDtExpires
     * @param bool $serviceViewCanReceiveReminders
     * @param bool $expectedHasServices
     */
    public function testWillGetCompanyForServicesExpiringWithOneService(
        $checkDays,
        $serviceDtExpires,
        $event,
        $eventOccurred,
        $serviceViewCanReceiveReminders,
        $expectedHasServices
    )
    {
        $eventServiceMock = MockHelper::mock($this, 'Services\EventService');
        $eventServiceMock->expects($this->any())->method('eventOccurred')->with($event, 1)->willReturn($eventOccurred);

        $serviceMock1 = MockHelper::mock($this, 'Entities\Service');
        $serviceMock1->expects($this->any())->method('getServiceTypeId')->willReturn(Service::TYPE_SERVICE_ADDRESS);
        $serviceMock1->expects($this->any())->method('getDtExpires')->willReturn($serviceDtExpires);

        $serviceViewMock = MockHelper::mock($this, 'Models\View\ServiceView');
        $serviceViewMock->method('getId')->willReturn(1);
        $serviceViewMock->method('getDtExpires')->willReturn($serviceMock1->getDtExpires());
        $serviceViewMock->method('canReceiveReminders')->willReturn($serviceViewCanReceiveReminders);

        $companyMock = MockHelper::mock($this, 'Entities\Company');
        $companyMock->expects($this->once())->method('getEnabledActivatedParentServices')->willReturn([$serviceMock1]);

        $serviceViewFactoryMock = MockHelper::mock($this, 'Factories\ServiceViewFactory');
        $serviceViewFactoryMock->expects($this->once())->method('create')->with([$serviceMock1])->willReturn($serviceViewMock);

        $this->object = new CompanyServicesView($companyMock, $serviceViewFactoryMock, $eventServiceMock);
        $actualResult = $this->object->getServicesWithoutEventExpiringIn($checkDays, $event);
        $this->assertEquals($expectedHasServices, !empty($actualResult));
        if ($expectedHasServices) {
            $this->assertContainsOnlyInstancesOf('Models\View\ServiceView', $actualResult);
            $this->assertCount(1, $actualResult);
        } else {
            $this->assertCount(0, $actualResult);
        }
    }

    /**
     * @return array
     */
    public function dataGetExpiringServicesOneService()
    {
        // checkDays, serviceDtExpires, event, eventOccurred, serviceViewCanReceiveReminders, expectedResult
        return [
            [-10, new DateTime('-10 days'), 'event', FALSE, TRUE, TRUE],
            [-10, new DateTime('-10 days'), 'event', TRUE, TRUE, FALSE],
            [-10, new DateTime('-10 days'), 'event', FALSE, FALSE, FALSE],
            [-10, new DateTime('-10 days'), 'event', TRUE, FALSE, FALSE],
            [-10, new DateTime('-9 days'), 'event', FALSE, TRUE, FALSE],
            [-10, new DateTime('-9 days'), 'event', TRUE, TRUE, FALSE],
            [-10, new DateTime('-9 days'), 'event', FALSE, FALSE, FALSE],
            [-10, new DateTime('-9 days'), 'event', TRUE, FALSE, FALSE],

            [0, new DateTime('+0 days'), 'event', FALSE, TRUE, TRUE],
            [0, new DateTime('+0 days'), 'event', TRUE, TRUE, FALSE],
            [0, new DateTime('+0 days'), 'event', FALSE, FALSE, FALSE],
            [0, new DateTime('+0 days'), 'event', TRUE, FALSE, FALSE],
            [0, new DateTime('+1 days'), 'event', FALSE, TRUE, FALSE],
            [0, new DateTime('+1 days'), 'event', TRUE, TRUE, FALSE],
            [0, new DateTime('+1 days'), 'event', FALSE, FALSE, FALSE],
            [0, new DateTime('+1 days'), 'event', TRUE, FALSE, FALSE],

            [10, new DateTime('+10 days'), 'event', FALSE, TRUE, TRUE],
            [10, new DateTime('+10 days'), 'event', TRUE, TRUE, FALSE],
            [10, new DateTime('+10 days'), 'event', TRUE, FALSE, FALSE],
            [10, new DateTime('+10 days'), 'event', TRUE, FALSE, FALSE],
            [10, new DateTime('+9 days'), 'event', FALSE, TRUE, FALSE],
            [10, new DateTime('+9 days'), 'event', TRUE, TRUE, FALSE],
            [10, new DateTime('+9 days'), 'event', FALSE, FALSE, FALSE],
            [10, new DateTime('+9 days'), 'event', TRUE, FALSE, FALSE],
        ];
    }

    /**
     * @dataProvider dataGetExpiringServicesMultipleServices
     * @covers       Models\View\Front\CompanyServicesView::getServicesWithoutEventExpiringIn
     * @param int $checkDays
     * @param array $serviceViews
     * @param bool $expectedHasServices
     * @param int $expectedCount
     */
    public function testWillGetCompanyServicesExpiringWithMultipleServices(
        $checkDays,
        array $serviceViews,
        $expectedHasServices,
        $expectedCount
    )
    {
        $event = 'dummy.event';
        $eventServiceMock = MockHelper::mock($this, 'Services\EventService');
        $eventServiceMock->expects($this->any())->method('eventOccurred')->with($event)->willReturn(FALSE);
        $serviceViewFactoryMock = MockHelper::mock($this, 'Factories\ServiceViewFactory');
        $allServiceMocks = [];

        $i = 0;
        foreach ($serviceViews as $serviceView) {
            $serviceMocks = [];

            foreach ($serviceView['services'] as $service) {
                $serviceMock = MockHelper::mock($this, 'Entities\Service');
                $serviceMock->expects($this->any())->method('getServiceTypeId')->willReturn($service[0]);
                $serviceMock->expects($this->any())->method('getDtExpires')->willReturn($service[1]);
                $serviceMocks[] = $serviceMock;
                $allServiceMocks[] = $serviceMock;
            }

            $serviceViewMock = MockHelper::mock($this, 'Models\View\ServiceView');
            $serviceViewMock->method('getDtExpires')->willReturn($serviceMock->getDtExpires());
            $serviceViewMock->method('canReceiveReminders')->willReturn($serviceView['canReceiveReminders']);

            $serviceViewFactoryMock->expects($this->at($i))->method('create')->with($serviceMocks)->willReturn($serviceViewMock);
            $i++;
        }

        $companyMock = MockHelper::mock($this, 'Entities\Company');
        $companyMock->expects($this->once())->method('getEnabledActivatedParentServices')->willReturn($allServiceMocks);

        $this->object = new CompanyServicesView($companyMock, $serviceViewFactoryMock, $eventServiceMock);
        $actualResult = $this->object->getServicesWithoutEventExpiringIn($checkDays, $event);
        $this->assertEquals($expectedHasServices, !empty($actualResult));
        $this->assertCount($expectedCount, $actualResult);
        if ($expectedHasServices) {
            $this->assertContainsOnlyInstancesOf('Models\View\ServiceView', $actualResult);
        }
    }

    /**
     * @return array
     */
    public function dataGetExpiringServicesMultipleServices()
    {
        // checkDays,
        // array of service views of (
        //      array of (serviceDtExpires, serviceIsEnabled, serviceIsOneOff, serviceIsAutoRenewalEnabled)
        // )
        // expectedHasServices,
        // expectedCount
        return [
            [
                -10,
                [
                    [
                        'services' => [
                            [Service::TYPE_SERVICE_ADDRESS, new DateTime('-10 days')],
                        ],
                        'canReceiveReminders' => FALSE,
                    ],
                    [
                        'services' => [
                            [Service::TYPE_REGISTERED_OFFICE, new DateTime('-10 days')],
                        ],
                        'canReceiveReminders' => FALSE,
                    ],
                ],
                FALSE,
                0
            ],
            [
                -10,
                [
                    [
                        'services' => [
                            [Service::TYPE_SERVICE_ADDRESS, new DateTime('-10 days')],
                            [Service::TYPE_SERVICE_ADDRESS, new DateTime('-5 days')],
                        ],
                        'canReceiveReminders' => FALSE,
                    ],
                ],
                FALSE,
                0
            ],
            [
                0,
                [
                    [
                        'services' => [
                            [Service::TYPE_PACKAGE_PRIVACY, new DateTime('+5 days')],
                        ],
                        'canReceiveReminders' => FALSE,
                    ],
                    [
                        'services' => [
                            [Service::TYPE_PACKAGE_COMPREHENSIVE_ULTIMATE, new DateTime('+7 days')],
                        ],
                        'canReceiveReminders' => FALSE,
                    ],
                ],
                FALSE,
                0
            ],
            [
                0,
                [
                    [
                        'services' => [
                            [Service::TYPE_PACKAGE_PRIVACY, new DateTime('-10 days')],
                            [Service::TYPE_PACKAGE_PRIVACY, new DateTime('+0 days')],
                        ],
                        'canReceiveReminders' => TRUE,
                    ],
                ],
                TRUE,
                1
            ],
            [
                0,
                [
                    [
                        'services' => [
                            [Service::TYPE_SERVICE_ADDRESS, new DateTime('+0 days')],
                        ],
                        'canReceiveReminders' => TRUE,
                    ],
                    [
                        'services' => [
                            [Service::TYPE_PACKAGE_PRIVACY, new DateTime('+0 days')],
                        ],
                        'canReceiveReminders' => TRUE,
                    ],
                ],
                TRUE,
                2
            ],
            [
                10,
                [
                    [
                        'services' => [
                            [Service::TYPE_REGISTERED_OFFICE, new DateTime('+10 days')],
                            [Service::TYPE_REGISTERED_OFFICE, new DateTime('+20 days')],
                        ],
                        'canReceiveReminders' => FALSE,
                    ],
                ],
                FALSE,
                0
            ],
            [
                10,
                [
                    [
                        'services' => [
                            [Service::TYPE_REGISTERED_OFFICE, new DateTime('+5 days')],
                            [Service::TYPE_REGISTERED_OFFICE, new DateTime('+10 days')],
                        ],
                        'canReceiveReminders' => TRUE,
                    ],
                ],
                TRUE,
                1
            ],
        ];
    }

}
