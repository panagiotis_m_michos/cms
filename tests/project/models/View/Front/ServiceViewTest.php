<?php

namespace Models\View\Front;

use Entities\OrderItem;
use Entities\Service;
use Entities\Company;
use Entities\Customer;
use Entities\Order;
use Models\View\ServiceView;
use Product;
use DateTime;
use Repositories\ServiceSettingsRepository;
use tests\helpers\MockHelper;
use Tests\Helpers\ObjectHelper;

/**
 * Generated by PHPUnit_SkeletonGenerator 1.2.1 on 2014-04-17 at 16:01:14.
 */
class ServiceViewTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ServiceSettingsRepository|\PHPUnit_Framework_MockObject_MockObject
     */
    private $serviceSettingsRepositoryMock;

    /**
     * @var ServiceView
     */
    private $object;

    /**
     * @var Service
     */
    private $service1;

    /**
     * @var Service
     */
    private $service2;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->service1 = $this->getService();
        $this->service2 = $this->getService();
        $this->serviceSettingsRepositoryMock = MockHelper::mock($this, 'Repositories\ServiceSettingsRepository');

        $this->object = new ServiceView(
            [$this->service1, $this->service2],
            $this->serviceSettingsRepositoryMock
        );
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
    }

    /**
     * @covers Models\View\ServiceView::isOnHold
     */
    public function testIsOnHoldNoDates()
    {
        $this->assertTrue($this->object->isOnHold());
    }

    /**
     * @covers Models\View\ServiceView::isOnHold
     */
    public function testIsOnHoldWithDatesForOneService()
    {
        $this->service1->setDtStart(new DateTime('+1 min'));
        $this->assertTrue($this->object->isOnHold());
        $this->service1->setDtStart(new DateTime('-1 min'));
        $this->assertFalse($this->object->isOnHold());
    }

    /**
     * @covers Models\View\ServiceView::isOnHold
     */
    public function testIsOnHoldWithDatesForTwoServices()
    {
        $this->service1->setDtStart(new DateTime('+1 min'));
        $this->service2->setDtStart(new DateTime('-1 min'));
        $this->assertFalse($this->object->isOnHold());
        $this->service1->setDtStart(new DateTime('+1 min'));
        $this->service2->setDtStart(new DateTime('+2 min'));
        $this->assertTrue($this->object->isOnHold());
        $this->service1->setDtStart(new DateTime('-1 min'));
        $this->service2->setDtStart(new DateTime('-2 min'));
        $this->assertFalse($this->object->isOnHold());
    }

    /**
     * @covers Models\View\ServiceView::isActive
     */
    public function testIsActiveForOneService()
    {
        $this->assertFalse($this->object->isActive());
        $this->service1->setDtStart(new DateTime('-1 day'));
        $this->assertFalse($this->object->isActive());
        $this->service1->setDtExpires(new DateTime('+1 day'));
        $this->assertTrue($this->object->isActive());
        $this->service1->setDtExpires(new DateTime('+28 days'));
        $this->assertTrue($this->object->isActive());
        $this->service1->setDtExpires(new DateTime('+29 days'));
        $this->assertTrue($this->object->isActive());
        $this->service1->setDtExpires(new DateTime('-1 days'));
        $this->assertTrue($this->object->isActive());
        $this->service1->setDtExpires(new DateTime('-28 days'));
        $this->assertTrue($this->object->isActive());
        $this->service1->setDtExpires(new DateTime('-29 days'));
        $this->assertFalse($this->object->isActive());
    }

    /**
     * @covers Models\View\ServiceView::isActive
     */
    public function testIsActiveOneOff()
    {
        $this->service1->setRenewalProduct(NULL);
        $this->service2->setRenewalProduct(NULL);
        $this->assertFalse($this->object->isActive());
        $this->service1->setDtStart(new DateTime('-1 day'));
        $this->assertTrue($this->object->isActive());

        $this->service1->setDtStart(new DateTime('+1 day'));
        $this->assertFalse($this->object->isActive());

        $this->service1->setDtStart(new DateTime('-2 day'));
        $this->service1->setDtExpires(new DateTime('-1 day'));
        $this->assertFalse($this->object->isActive());

        $this->service1->setDtExpires(new DateTime('+1 day'));
        $this->assertTrue($this->object->isActive());
    }

    /**
     * @covers Models\View\ServiceView::isActive
     */
    public function testIsActiveForTwoServices()
    {
        $this->assertFalse($this->object->isActive());
        $this->service1->setDtStart(new DateTime('-1 days'));
        $this->service2->setDtStart(new DateTime('-2 days'));
        $this->assertFalse($this->object->isActive());
        $this->service1->setDtExpires(new DateTime('+1 days'));
        $this->assertTrue($this->object->isActive());
        $this->service2->setDtExpires(new DateTime('+2 days'));
        $this->assertTrue($this->object->isActive());

        $this->service1->setDtExpires(new DateTime('-2 days'));
        $this->assertTrue($this->object->isActive());
        $this->service2->setDtExpires(new DateTime('-30 days'));
        $this->assertTrue($this->object->isActive());

        $this->service1->setDtExpires(new DateTime('-28 days'));
        $this->assertTrue($this->object->isActive());

        $this->service1->setDtExpires(new DateTime('-29 days'));
        $this->assertFalse($this->object->isActive());
    }

    /**
     * @covers Models\View\ServiceView::isDue
     */
    public function testIsDueIn1MonthForOneService()
    {
        $this->assertFalse($this->object->isDue());
        $this->service1->setDtStart(new DateTime('-1 days'));
        $this->assertFalse($this->object->isDue());
        $this->service1->setDtExpires(new DateTime('+1 days'));
        $this->assertTrue($this->object->isDue());

        $this->service1->setDtExpires(new DateTime('+28 days'));
        $this->assertTrue($this->object->isDue());

        $this->service1->setDtExpires(new DateTime('+30 days'));
        $this->assertFalse($this->object->isDue());
    }

    /**
     * @covers Models\View\ServiceView::isDue
     */
    public function testIsDueIn1MonthForTwoServices()
    {
        $this->assertFalse($this->object->isDue());
        $this->service1->setDtStart(new DateTime('-1 days'));
        $this->service2->setDtStart(new DateTime('-2 days'));
        $this->assertFalse($this->object->isDue());
        $this->service1->setDtExpires(new DateTime('+1 days'));
        $this->assertTrue($this->object->isDue());
        $this->service2->setDtExpires(new DateTime('+2 days'));
        $this->assertTrue($this->object->isDue());

        $this->service1->setDtExpires(new DateTime('-2 days'));
        $this->assertTrue($this->object->isDue());

        $this->service2->setDtExpires(new DateTime('+28 days'));
        $this->assertTrue($this->object->isDue());

        $this->service2->setDtExpires(new DateTime('+29 days'));
        $this->assertFalse($this->object->isDue());
    }

    /**
     * @covers Models\View\ServiceView::isOverdue
     */
    public function testIsOverdueForOneService()
    {
        $this->assertFalse($this->object->isOverdue());
        $this->service1->setDtStart(new DateTime('-1 days'));
        $this->assertFalse($this->object->isOverdue());
        $this->service1->setDtExpires(new DateTime('-1 days'));
        $this->assertTrue($this->object->isOverdue());

        $this->service1->setDtExpires(new DateTime('+28 days'));
        $this->assertFalse($this->object->isOverdue());

        $this->service1->setDtExpires(new DateTime('-28 days'));
        $this->assertTrue($this->object->isOverdue());

        $this->service1->setDtExpires(new DateTime('-30 days'));
        $this->assertFalse($this->object->isOverdue());
    }

    /**
     * @covers Models\View\ServiceView::isOverdue
     */
    public function testIsOverdueForTwoServices()
    {
        $this->assertFalse($this->object->isOverdue());
        $this->service1->setDtStart(new DateTime('-1 days'));
        $this->service2->setDtStart(new DateTime('-2 days'));
        $this->assertFalse($this->object->isOverdue());
        $this->service1->setDtExpires(new DateTime('-1 days'));
        $this->assertTrue($this->object->isOverdue());
        $this->service2->setDtExpires(new DateTime('-2 days'));
        $this->assertTrue($this->object->isOverdue());

        $this->service1->setDtExpires(new DateTime('+1 days'));
        $this->assertFalse($this->object->isOverdue());

        $this->service1->setDtExpires(new DateTime('-28 days'));
        $this->service2->setDtExpires(new DateTime('-28 days'));
        $this->assertTrue($this->object->isOverdue());

        $this->service1->setDtExpires(new DateTime('-29 days'));
        $this->service2->setDtExpires(new DateTime('-29 days'));
        $this->assertFalse($this->object->isOverdue());

        $this->service1->setDtExpires(new DateTime('+28 days'));
        $this->service2->setDtExpires(new DateTime('+29 days'));
        $this->assertFalse($this->object->isOverdue());
    }

    /**
     * @covers Models\View\ServiceView::isExpired
     */
    public function testIsSuspendedForOneService()
    {
        $this->assertFalse($this->object->isExpired());
        $this->service1->setDtStart(new DateTime('-1 days'));
        $this->assertFalse($this->object->isExpired());
        $this->service1->setDtExpires(new DateTime('-1 days'));
        $this->assertFalse($this->object->isExpired());

        $this->service1->setDtExpires(new DateTime('-28 days'));
        $this->assertFalse($this->object->isExpired());

        $this->service1->setDtExpires(new DateTime('-29 days'));
        $this->assertTrue($this->object->isExpired());

        $this->service1->setDtExpires(new DateTime('+28 days'));
        $this->assertFalse($this->object->isExpired());
    }

    /**
     * @covers Models\View\ServiceView::isExpired
     */
    public function testIsSuspendedForTwoServices()
    {
        $this->assertFalse($this->object->isExpired());
        $this->service1->setDtStart(new DateTime('-1 days'));
        $this->service2->setDtStart(new DateTime('-2 days'));
        $this->assertFalse($this->object->isExpired());
        $this->service1->setDtExpires(new DateTime('-29 days'));
        $this->assertTrue($this->object->isExpired());
        $this->service2->setDtExpires(new DateTime('-30 days'));
        $this->assertTrue($this->object->isExpired());

        $this->service1->setDtExpires(new DateTime('+1 days'));
        $this->assertFalse($this->object->isExpired());

        $this->service1->setDtExpires(new DateTime('-28 days'));
        $this->service2->setDtExpires(new DateTime('-28 days'));
        $this->assertFalse($this->object->isExpired());

        $this->service1->setDtExpires(new DateTime('+28 days'));
        $this->service2->setDtExpires(new DateTime('+28 days'));
        $this->assertFalse($this->object->isExpired());

        $this->service1->setDtExpires(new DateTime('-300 days'));
        $this->assertFalse($this->object->isExpired());
        $this->service2->setDtExpires(new DateTime('-258 days'));
        $this->assertTrue($this->object->isExpired());
    }

    /**
     * @covers Models\View\ServiceView::isExpired
     */
    public function testIsSuspendedOneOff()
    {
        $this->service1->setRenewalProduct(NULL);
        $this->service2->setRenewalProduct(NULL);
        $this->assertFalse($this->object->isExpired());
        $this->service1->setDtStart(new DateTime('-2 day'));
        $this->service1->setDtExpires(new DateTime('-1 day'));
        $this->assertTrue($this->object->isExpired());
    }

    /**
     * @covers Models\View\ServiceView::getStatusId
     */
    public function testGetStatusIdForOneService()
    {
        $settingsMock = MockHelper::mock($this, 'Entities\ServiceSettings');
        $this->serviceSettingsRepositoryMock->expects($this->any())->method('getSettingsByService')->willReturn($settingsMock);
        $settingsMock->expects($this->any())->method('isAutoRenewalEnabled')->willReturnOnConsecutiveCalls(
            TRUE, FALSE, TRUE, FALSE
        );

        $this->assertEquals(Service::STATUS_ON_HOLD, $this->object->getStatusId());

        $this->service1->setDtStart(new DateTime('+1 days'));
        $this->service1->setDtExpires(new DateTime('+2 days'));
        $this->assertEquals(Service::STATUS_ON_HOLD, $this->object->getStatusId());

        $this->service1->setDtStart(new DateTime('-30 days'));
        $this->service1->setDtExpires(new DateTime('+30 days'));
        $this->assertEquals(Service::STATUS_ACTIVE, $this->object->getStatusId());

        $this->service1->setDtStart(new DateTime('-1 days'));
        $this->service1->setDtExpires(new DateTime('+28 days'));
        $this->assertEquals(Service::STATUS_ACTIVE, $this->object->getStatusId()); // isAutoRenewal = TRUE
        $this->assertEquals(Service::STATUS_SOON_DUE, $this->object->getStatusId()); // isAutoRenewal = FALSE

        $this->service1->setDtExpires(new DateTime());
        $this->assertEquals(Service::STATUS_ACTIVE, $this->object->getStatusId()); // isAutoRenewal = TRUE
        $this->assertEquals(Service::STATUS_EXPIRES_TODAY, $this->object->getStatusId()); // isAutoRenewal = FALSE

        $this->service1->setDtExpires(new DateTime('-1 days'));
        $this->assertEquals(Service::STATUS_OVERDUE, $this->object->getStatusId());

        $this->service1->setDtExpires(new DateTime('-28 days'));
        $this->assertEquals(Service::STATUS_OVERDUE, $this->object->getStatusId());

        $this->service1->setDtExpires(new DateTime('-29 days'));
        $this->assertEquals(Service::STATUS_EXPIRED, $this->object->getStatusId());
    }

    /**
     * @covers Models\View\ServiceView::getStatusId
     */
    public function testGetStatusIdForTwoServicesWithOneOnHold()
    {
        $this->assertEquals(Service::STATUS_ON_HOLD, $this->object->getStatusId());

        $this->service1->setDtStart(new DateTime('+1 days'));
        $this->service1->setDtExpires(new DateTime('+30 days'));
        $this->service2->setDtStart(new DateTime('+10 days'));
        $this->service2->setDtExpires(new DateTime('+20 days'));
        $this->assertEquals(Service::STATUS_ON_HOLD, $this->object->getStatusId());

        $this->service2->setDtStart(new DateTime('-10 days'));
        $this->service2->setDtExpires(new DateTime('+30 days'));
        $this->assertEquals(Service::STATUS_ACTIVE, $this->object->getStatusId());

        $this->service2->setDtExpires(new DateTime('+20 days'));
        $this->assertEquals(Service::STATUS_ACTIVE, $this->object->getStatusId());

        $this->service2->setDtExpires(new DateTime());
        $this->assertEquals(Service::STATUS_ACTIVE, $this->object->getStatusId());

        $this->service2->setDtExpires(new DateTime('-1 days'));
        $this->assertEquals(Service::STATUS_ACTIVE, $this->object->getStatusId());

        $this->service2->setDtExpires(new DateTime('-28 days'));
        $this->assertEquals(Service::STATUS_ACTIVE, $this->object->getStatusId());

        $this->service2->setDtExpires(new DateTime('-29 days'));
        $this->assertEquals(Service::STATUS_ACTIVE, $this->object->getStatusId());
    }

    /**
     * @covers Models\View\ServiceView::getStatusId
     */
    public function testGetStatusIdForTwoServicesWithOneSuspended()
    {
        $settingsMock = MockHelper::mock($this, 'Entities\ServiceSettings');
        $this->serviceSettingsRepositoryMock->expects($this->any())->method('getSettingsByService')->willReturn($settingsMock);
        $settingsMock->expects($this->any())->method('isAutoRenewalEnabled')->willReturnOnConsecutiveCalls(
            FALSE, TRUE, FALSE, TRUE
        );

        $this->service1->setDtStart(new DateTime('-220 days'));
        $this->service1->setDtExpires(new DateTime('-29 days'));
        $this->service2->setDtStart(new DateTime('-10 days'));
        $this->service2->setDtExpires(new DateTime('+30 days'));
        $this->assertEquals(Service::STATUS_ACTIVE, $this->object->getStatusId());

        $this->service2->setDtExpires(new DateTime('+20 days'));
        $this->assertEquals(Service::STATUS_SOON_DUE, $this->object->getStatusId());
        $this->assertEquals(Service::STATUS_ACTIVE, $this->object->getStatusId());

        $this->service2->setDtExpires(new DateTime());
        $this->assertEquals(Service::STATUS_EXPIRES_TODAY, $this->object->getStatusId());
        $this->assertEquals(Service::STATUS_ACTIVE, $this->object->getStatusId());

        $this->service2->setDtExpires(new DateTime('-1 days'));
        $this->assertEquals(Service::STATUS_OVERDUE, $this->object->getStatusId());

        $this->service2->setDtExpires(new DateTime('-28 days'));
        $this->assertEquals(Service::STATUS_OVERDUE, $this->object->getStatusId());

        $this->service2->setDtExpires(new DateTime('-29 days'));
        $this->assertEquals(Service::STATUS_EXPIRED, $this->object->getStatusId());
    }

    /**
     * @covers Models\View\ServiceView::getStatus
     */
    public function testGetStatus()
    {
        $this->assertEquals('On Hold', $this->object->getStatus());
    }

    /**
     * @covers Models\View\ServiceView::getDtStart
     */
    public function testGetDtStart()
    {
        $dtStart = new DateTime();
        $this->service1->setDtStart($dtStart);
        $this->assertEquals($dtStart, $this->object->getDtStart());
    }

    /**
     * @covers Models\View\ServiceView::getDtExpires
     */
    public function testGetDtExpires()
    {
        $dtExpires = new DateTime();
        $this->service1->setDtExpires($dtExpires);
        $this->assertEquals($dtExpires, $this->object->getDtExpires());
    }

    /**
     * @covers Models\View\ServiceView::getRenewalProduct
     */
    public function testGetRenewalProduct()
    {
        $actualRenewalProduct = $this->getService()->getRenewalProduct();
        $this->assertEquals(0, $actualRenewalProduct->getId());
    }

    /**
     * @covers Models\View\ServiceView::isUpgraded
     */
    public function testServiceIsUpgraded()
    {
        $this->service1->setDtExpires(new DateTime('+1 days'));
        $this->service2->setDtExpires(new DateTime('+30 days'));
        $this->service2->setStateId(Service::STATE_UPGRADED);

        $this->assertTrue($this->object->isUpgraded());
    }

    /**
     * @covers Models\View\ServiceView::isDowngraded
     */
    public function testServiceIsDowngraded()
    {
        $this->service1->setDtExpires(new DateTime('+1 days'));
        $this->service2->setDtExpires(new DateTime('+30 days'));
        $this->service2->setStateId(Service::STATE_DOWNGRADED);

        $this->assertTrue($this->object->isDowngraded());
    }

    /**
     * @return Service
     */
    private function getService()
    {
        $service = ObjectHelper::createService();
        $service->setRenewalProduct(new Product());
        return $service;
    }
}
