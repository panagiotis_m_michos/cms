<?php

namespace Models\View\Front;

use Entities\Company;
use Entities\Service;
use Factories\ServiceViewFactory;
use Models\View\CompanyView;
use PHPUnit_Framework_MockObject_MockObject;
use tests\helpers\MockHelper;
use Tests\Helpers\ObjectHelper;
use Tests\ServicesHelper;

class CompanyViewTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var CompanyView
     */
    protected $object;

    /**
     * @var Company
     */
    private $company;

    /**
     * @var Company|PHPUnit_Framework_MockObject_MockObject
     */
    private $companyMock;

    /**
     * @var ServiceViewFactory|PHPUnit_Framework_MockObject_MockObject
     */
    private $serviceViewFactoryMock;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->company = ObjectHelper::createCompany(ObjectHelper::createCustomer('test', 'test'), 'test');

        $this->companyMock = $this->getMockBuilder('Entities\Company')->disableOriginalConstructor()->getMock();
        $this->serviceViewFactoryMock = MockHelper::mock($this, 'Factories\ServiceViewFactory');

        $this->object = new CompanyView($this->companyMock, $this->serviceViewFactoryMock);
    }


    /**
     * @covers Models\View\Front\CompanyView::getServices
     */
    public function testGetServices()
    {
        $ar1 = ServicesHelper::getService(Service::TYPE_ANNUAL_RETURN);
        $ar2 = ServicesHelper::getService(Service::TYPE_ANNUAL_RETURN);
        $pp1 = ServicesHelper::getService(Service::TYPE_PACKAGE_PRIVACY);
        $pp2 = ServicesHelper::getService(Service::TYPE_PACKAGE_PRIVACY);
        $sa1 = ServicesHelper::getService(Service::TYPE_SERVICE_ADDRESS);

        $this->companyMock->expects($this->once())->method('getEnabledParentServices')->willReturn([$ar1, $ar2, $pp1, $pp2, $sa1]);
        $this->serviceViewFactoryMock
            ->expects($this->exactly(3))
            ->method('create')
            ->withConsecutive([[$ar1, $ar2]], [[$pp1, $pp2]], [[$sa1]])
            ->willReturnOnConsecutiveCalls($ar1, $pp1, $sa1);

        $services = $this->object->getServices();
        $this->assertCount(3, $services);
        $this->assertEquals(Service::TYPE_ANNUAL_RETURN, $services[0]->getServiceTypeId());
        $this->assertEquals(Service::TYPE_PACKAGE_PRIVACY, $services[1]->getServiceTypeId());
        $this->assertEquals(Service::TYPE_SERVICE_ADDRESS, $services[2]->getServiceTypeId());
    }

    /**
     * @covers Models\View\Front\CompanyView::isServiceActionRequired
     */
    public function testIsServiceActionRequired()
    {
        $s1 = ServicesHelper::getService();
        $s2 = ServicesHelper::getService();
        $this->company->addService($s1);
        $this->company->addService($s2);

        $serviceViewMock = MockHelper::mock($this, 'Models\View\ServiceView');
        $serviceViewMock->method('isOverdue')->willReturn(FALSE);
        $serviceViewMock->method('isDue')->willReturn(FALSE);

        $this->serviceViewFactoryMock->expects($this->once())->method('create')->with([$s1, $s2])->willReturn($serviceViewMock);

        $companyView = new CompanyView($this->company, $this->serviceViewFactoryMock);
        $this->assertFalse($companyView->isServiceActionRequired());
    }

    /**
     * @covers Models\View\Front\CompanyView::isServiceActionRequired
     */
    public function testIsServiceActionRequiredOverdueService()
    {
        $s1 = ServicesHelper::getService(Service::TYPE_ANNUAL_RETURN);
        $this->company->addService($s1);

        $serviceViewMock = MockHelper::mock($this, 'Models\View\ServiceView');
        $serviceViewMock->method('isActionRequired')->willReturn(TRUE);

        $this->serviceViewFactoryMock->expects($this->once())->method('create')->with([$s1])->willReturn($serviceViewMock);

        $companyView = new CompanyView($this->company, $this->serviceViewFactoryMock);
        $this->assertTrue($companyView->isServiceActionRequired());
    }

    /**
     * @covers Models\View\Front\CompanyView::isServiceActionRequired
     */
    public function testIsServiceActionRequiredTwoServicesDifferentTypeOneOverdue()
    {
        $s1 = ServicesHelper::getService(Service::TYPE_ANNUAL_RETURN);
        $s2 = ServicesHelper::getService();
        $this->company->addService($s1);
        $this->company->addService($s2);

        $serviceViewMock1 = MockHelper::mock($this, 'Models\View\ServiceView');
        $serviceViewMock1->method('isActionRequired')->willReturn(FALSE);
        $serviceViewMock2 = MockHelper::mock($this, 'Models\View\ServiceView');
        $serviceViewMock2->method('isActionRequired')->willReturn(TRUE);

        $this->serviceViewFactoryMock
            ->expects($this->exactly(2))
            ->method('create')
            ->withConsecutive([[$s1]], [[$s2]])
            ->willReturnOnConsecutiveCalls($serviceViewMock1, $serviceViewMock2);

        $companyView = new CompanyView($this->company, $this->serviceViewFactoryMock);
        $this->assertTrue($companyView->isServiceActionRequired());
    }
}
