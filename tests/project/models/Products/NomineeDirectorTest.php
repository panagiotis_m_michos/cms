<?php

class NomineeDirectorTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var NomineeDirector|PHPUnit_Framework_MockObject_MockObject
     */
    private $nomineeDirector;

    public function setUp()
    {
        $this->clearData();

        $this->nomineeDirector = new NomineeDirector();

        $englishLanguage = array(
            'pk' => 'EN',
            'name' => 'English',
            'is_active' => 1,
            'ord' => 1,
        );
        dibi::insert('cms2_languages', $englishLanguage)->execute();
    }

    public function tearDown()
    {
        $this->clearData();
    }

    /**
     * @covers NomineeDirector::isCorporate
     * @covers NomineeDirector::getIsCorporate
     */
    public function testManipulateIsCorporate()
    {
        $this->nomineeDirector->setIsCorporate(1);
        $this->assertEquals(1, $this->nomineeDirector->isCorporate());
    }

    /**
     * @covers NomineeDirector::isIncludedByDefault
     * @covers NomineeDirector::includeByDefault
     */
    public function testManipulateIncludedByDefault()
    {
        $this->nomineeDirector->includeByDefault(2);
        $this->assertEquals(2, $this->nomineeDirector->isIncludedByDefault());
    }

    /**
     * @covers NomineeDirector::save
     */
    public function testCanSaveCorrectly()
    {
        $this->setNomineeDirectorDataRequiredToSave($this->nomineeDirector);
        $nomineeDirectorId = $this->nomineeDirector->save();

        $this->assertGreaterThan(0, $nomineeDirectorId);
    }

    /**
     * @covers NomineeDirector::__construct
     * @covers NomineeDirector::save
     */
    public function testCanGetCorrectSavedNomineeDirector()
    {
        $this->setNomineeDirectorDataRequiredToSave($this->nomineeDirector);
        $nomineeDirectorId = $this->nomineeDirector->save();
        $nomineeDirector = new NomineeDirector($nomineeDirectorId);

        $this->assertTrue($nomineeDirector->isCorporate());
        $this->assertTrue($nomineeDirector->isIncludedByDefault());
        $this->assertSame(1, $nomineeDirector->getParentId());
        $this->assertSame(1, $nomineeDirector->getStatusId());
    }

    private function clearData()
    {
        dibi::delete('cms2_languages')->where('pk = %s', 'EN')->execute();
    }

    /**
     * @param NomineeDirector $nomineeDirector
     */
    private function setNomineeDirectorDataRequiredToSave(NomineeDirector $nomineeDirector)
    {
        $nomineeDirector->includeByDefault(TRUE);
        $nomineeDirector->setIsCorporate(TRUE);
        $nomineeDirector->setParentId(1);
        $nomineeDirector->setStatusId(1);
        $nomineeDirector->adminControler = 'NomineeDirectorAdminControler';
    }
}
