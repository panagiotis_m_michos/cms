<?php

class BasketProductTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var BasketProduct
     */
    private $basketProduct;

    public function setUp()
    {
        $this->basketProduct = new BasketProduct();
    }

    /**
     * @covers BasketProduct::getPrice
     */
    public function testReturnsOfferPrice()
    {
        $offerPrice = 11.1;
        $associatedPrice = 22.2;
        $wholesalePrice = 33.3;
        $this->basketProduct->markAsOffer();
        $this->basketProduct->setOfferPrice($offerPrice);
        $this->basketProduct->associatedPrice = $associatedPrice;
        $this->basketProduct->wholesalePrice = $wholesalePrice;
        
        $price = $this->basketProduct->getPrice();
        $this->assertEquals($offerPrice, $price);
    }

    /**
     * @covers BasketProduct::getPrice
     */
    public function testReturnsAssociatedPrice()
    {
        $offerPrice = 11.1;
        $associatedPrice = 22.2;
        $wholesalePrice = 33.3;
        $this->basketProduct->isAssociated = TRUE;
        $this->basketProduct->setOfferPrice($offerPrice);
        $this->basketProduct->associatedPrice = $associatedPrice;
        $this->basketProduct->wholesalePrice = $wholesalePrice;

        $price = $this->basketProduct->getPrice();
        $this->assertEquals($associatedPrice, $price);
    }

    /**
     * @covers BasketProduct::getPrice
     */
    public function testReturnsPrice()
    {
        $offerPrice = 11.1;
        $associatedPrice = 22.2;
        $wholesalePrice = 33.3;
        $price = 44.4;

        $this->basketProduct->setOfferPrice($offerPrice);
        $this->basketProduct->associatedPrice = $associatedPrice;
        $this->basketProduct->wholesalePrice = $wholesalePrice;
        $this->basketProduct->setPrice($price);

        $returnedPrice = $this->basketProduct->getPrice();
        $this->assertEquals($price, $returnedPrice);
    }
}
