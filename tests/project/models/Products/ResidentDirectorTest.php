<?php

class ResidentDirectorTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var ResidentDirector|PHPUnit_Framework_MockObject_MockObject
     */
    private $residentDirector;

    public function setUp()
    {
        $this->residentDirector = new ResidentDirector();
    }

    /**
     * @covers ResidentDirector::getNomineeType
     */
    public function testHasCorrectNomineeType()
    {
        $this->assertEquals(ResidentDirector::$nomineeType, $this->residentDirector->getNomineeType());
    }
}
