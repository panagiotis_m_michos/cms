<?php

namespace Remote\VirtualOffice\Api\Message;

use PHPUnit_Framework_TestCase;

class JsonResponseTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     * @covers Remote\VirtualOffice\Api\Message\JsonResponse::__construct
     * @expectedException \Remote\VirtualOffice\Api\Message\JsonResponseException
     */
    public function responseMessage()
    {
        $streamInterfaceMock = $this->getMockBuilder('GuzzleHttp\Stream\StreamInterface')->disableOriginalConstructor()->getMock();
        $streamInterfaceMock->expects($this->once())->method('__toString')->willReturn(json_encode(array()));
        new JsonResponse(NULL, [], $streamInterfaceMock, []);
    }

    /**
     * @test
     * @covers Remote\VirtualOffice\Api\Message\JsonResponse::__construct
     * @expectedException \Remote\VirtualOffice\Api\Message\JsonResponseException
     */
    public function responseStatusCode()
    {
        $streamInterfaceMock = $this->getMockBuilder('GuzzleHttp\Stream\StreamInterface')->disableOriginalConstructor()->getMock();
        $streamInterfaceMock->expects($this->once())->method('__toString')->willReturn(json_encode(array('success' => FALSE)));
        new JsonResponse(200, [], $streamInterfaceMock, []);
    }

}
