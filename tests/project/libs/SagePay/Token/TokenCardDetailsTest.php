<?php

class TokenCardDetailsTest extends PHPUnit_Framework_TestCase
{

    /**
     * @var TokenCardDetails
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $data = array(
            'emails' => 'sdfsaljfaksj@sdjfhgsjd.com',
            'amount' => '19.39',
            //'cardholder' => 'Preda',
            'cardType' => 'VISA',
            'cardNumber' => '4929000000006',
            'issueNumber' => '',
            'validFrom' => array('m' => '?', 'y' => '?'),
            'expiryDate' => array('m' => '4', 'y' => '2017'),
            'CV2' => '123',
            'address1' => '88 road',
            'address2' => '',
            'address3' => '',
            'town' => 'London',
            'postcode' => '412',
            'town' => 'London',
            'country' => 'GB',
            'town' => 'London',
            'billingState' => '',
        );
        $this->object = new TokenCardDetails($data, new SagePayAddress);
    }

    /**
     * @covers TokenCardDetails::getFirstNames
     */
    public function testGetFirstNames()
    {
        $firstName = $this->object->getFirstNames();
        $this->assertEquals($firstName, 'M');

        $this->object->data['cardholder'] = "Razvan";
        $firstName = $this->object->getFirstNames();
        $this->assertEquals($firstName, 'M');

        $this->object->data['cardholder'] = "";
        $firstName = $this->object->getFirstNames();
        $this->assertEquals($firstName, 'M');

        $this->object->data['cardholder'] = NULL;
        $firstName = $this->object->getFirstNames();
        $this->assertEquals($firstName, 'M');

        $this->object->data['cardholder'] = 'RazvanRazvanRazvanRazvan JGJGjh';
        $firstName = $this->object->getFirstNames();
        $this->assertEquals($firstName, 'RazvanRazvanRazvanRazvan ');
    }

    /**
     * @covers TokenCardDetails::getSurname
     */
    public function testGetSurname()
    {
        $this->object->data['cardholder'] = 'RazvanRazvanRazvanRazvan JGJGjh';
        $firstName = $this->object->getSurname();
        $this->assertEquals($firstName, 'JGJGjh');

        $this->object->data['cardholder'] = 'RazvanRazvanRazvanRazvan JGJGjh TTT';
        $firstName = $this->object->getSurname();
        $this->assertEquals($firstName, 'TTT');

        $this->object->data['cardholder'] = 'RazvanRazvanRazvanRazvan JGJGjh TTT';
        $firstName = $this->object->getSurname();
        $this->assertEquals($firstName, 'TTT');

        $this->object->data['cardholder'] = 'RazvanRazvanRazvanRazvan JGJGjh TTT JGJGjh JGJGjh';
        $firstName = $this->object->getSurname();
        $this->assertEquals($firstName, 'JGJGjh');

        $this->object->data['cardholder'] = '';
        $firstName = $this->object->getSurname();
        $this->assertNull($firstName);

        $this->object->data['cardholder'] = NULL;
        $firstName = $this->object->getSurname();
        $this->assertNull($firstName);
    }

    /**
     * @covers TokenCardDetails::getAddresLine1
     */
    public function testGetAddresLine1()
    {
        $this->object->data['address1'] = '';
        $firstName = $this->object->getAddresLine1();
        $this->assertNull($firstName);

        $this->object->data['address1'] = 'Razvan P';
        $firstName = $this->object->getAddresLine1();
        $this->assertEquals($firstName, 'Razvan P');

        $this->object->data['address1'] = '88Rod88Rod88Rod88Rod88Rod88Rod88Rod88Rod88Rod88Rod88Rod88Rod88Rod88Rod88Rod88Rod88Rod88Rod88Rod88Rara';
        $firstName = $this->object->getAddresLine1();
        $this->assertEquals($firstName, '88Rod88Rod88Rod88Rod88Rod88Rod88Rod88Rod88Rod88Rod88Rod88Rod88Rod88Rod88Rod88Rod88Rod88Rod88Rod88Rar');
    }

    /**
     * @covers TokenCardDetails::getAddresLine2
     */
    public function testGetAddresLine2()
    {
        $this->object->data['address2'] = 'Razvan P';
        $this->object->data['address3'] = '';
        $firstName = $this->object->getAddresLine2();
        $this->assertEquals($firstName, 'Razvan P');

        $this->object->data['address2'] = '';
        $this->object->data['address3'] = '';
        $firstName = $this->object->getAddresLine2();
        $this->assertFalse($firstName);
    }

    /**
     * @covers TokenCardDetails::getCity
     */
    public function testGetCity()
    {
        $this->object->data['town'] = 'london';
        $firstName = $this->object->getCity();
        $this->assertEquals($firstName, 'London');

        $this->object->data['town'] = NULL;
        $firstName = $this->object->getCity();
        $this->assertNull($firstName);

    }

    /**
     * @covers TokenCardDetails::getPostCode
     */
    public function testGetPostCode()
    {
        $this->object->data['postcode'] = '656';
        $var = $this->object->getPostCode();
        $this->assertEquals($var, '656');

        $this->object->data['postcode'] = NULL;
        $var = $this->object->getPostCode();
        $this->assertNull($var);
    }

    /**
     * @covers TokenCardDetails::getCountry
     */
    public function testGetCountry()
    {
        $this->object->data['country'] = 'gb';
        $var = $this->object->getCountry();
        $this->assertEquals($var, 'GB');

        $this->object->data['country'] = NULL;
        $var = $this->object->getCountry();
        $this->assertNull($var);
    }

    /**
     * @covers TokenCardDetails::getState
     */
    public function testGetState()
    {
        $this->object->data['billingState'] = 'gb';
        $var = $this->object->getState();
        $this->assertEquals($var, 'GB');

        $this->object->data['billingState'] = NULL;
        $var = $this->object->getState();
        $this->assertNull($var);
    }

    /**
     * @covers TokenCardDetails::getPhone
     */
    public function testGetPhone()
    {

        $var = $this->object->getPhone();
        $this->assertNull($var);

        $this->object->data['phone'] = '34564564';
        $var = $this->object->getPhone();
        $this->assertEquals($var, '34564564');

        $this->object->data['phone'] = NULL;
        $var = $this->object->getPhone();
        $this->assertNull($var);
    }

}