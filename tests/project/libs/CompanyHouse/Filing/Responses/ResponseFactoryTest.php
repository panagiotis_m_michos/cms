<?php

namespace CompanyHouse\Filling\Responses;

use Utils\XmlElement;

/**
 * Generated by PHPUnit_SkeletonGenerator 1.2.1 on 2014-06-04 at 12:32:37.
 */
class ResponseFactoryTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ResponseFactory
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->object = new ResponseFactory();
    }

    /**
     * @covers CompanyHouse\Filling\Responses\ResponseFactory::createSubmissionStatus
     */
    public function testCreateSubmissionStatus()
    {
        $xml = <<<'EOD'
<Status>
<SubmissionNumber>045343</SubmissionNumber>
<StatusCode>REJECT</StatusCode>
<CompanyNumber>07193795</CompanyNumber>
<ChangeOfNameDetails>
    <DocRequestKey>12345678900987654321</DocRequestKey>
</ChangeOfNameDetails>
<IncorporationDetails>
    <DocRequestKey>abcdefghijklmnopqrstuvwxyz</DocRequestKey>
    <IncorporationDate>2010-12-21</IncorporationDate>
    <AuthenticationCode>1234</AuthenticationCode>
</IncorporationDetails>
<Examiner>
    <Telephone>Fred Blogs</Telephone>
    <Comment>Accepted submission</Comment>
</Examiner>
<Rejections>
    <Reject>
    <RejectCode>1838</RejectCode>
    <Description>The new accounting period you have requested is greater than 18 months. Please amend the date you have given as your requested period end date. When you extend a company's first accounting period, you must calculate from the date of incorporation.</Description>
    <InstanceNumber>1</InstanceNumber>
    </Reject>
    <RejectReference>test reject reference</RejectReference>
</Rejections>
</Status>
EOD;
        $xmlObject = new XmlElement($xml);
        $submissionStatus = $this->object->createSubmissionStatus($xmlObject);
        $this->assertEquals('045343', $submissionStatus->getSubmissionNumber());
        $this->assertEquals('07193795', $submissionStatus->getCompanyNumber());
        $this->assertEquals('REJECT', $submissionStatus->getStatus());
        $this->assertEquals('12345678900987654321', $submissionStatus->getChangeOfNameDetails()->getDocRequestKey());
        $this->assertEquals('abcdefghijklmnopqrstuvwxyz', $submissionStatus->getIncorporationDetails()->getDocRequestKey());
        $this->assertEquals('2010-12-21', $submissionStatus->getIncorporationDetails()->getIncorporationDate());
        $this->assertEquals('1234', $submissionStatus->getIncorporationDetails()->getAuthenticationCode());
        $this->assertEquals('Fred Blogs', $submissionStatus->getExaminer()->getTelephone());
        $this->assertEquals('Accepted submission', $submissionStatus->getExaminer()->getComment());
        $this->assertEquals('1838', $submissionStatus->getRejections()[0]->getCode());
        $this->assertEquals('The new accounting period you have requested is greater than 18 months. Please amend the date you have given as your requested period end date. When you extend a company\'s first accounting period, you must calculate from the date of incorporation.', $submissionStatus->getRejections()[0]->getDescription());
        $this->assertEquals('1', $submissionStatus->getRejections()[0]->getInstanceNumber());
        $this->assertEquals('test reject reference', $submissionStatus->getRejectReference());
    }
}
