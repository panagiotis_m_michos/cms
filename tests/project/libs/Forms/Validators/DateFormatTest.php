<?php

namespace Forms\Validators;

class DateFormatTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @covers Forms\Validators\DateFormat::isValid
     */
    public function testIsValidEnglishFormat() 
    {
        $validator = new DateFormat('test', 'd-m-Y', NULL, NULL);
        $this->assertTrue($validator->isValid('02-02-2245'));
        $this->assertTrue($validator->isValid('31-01-2014'));
        $this->assertTrue($validator->isValid('01-01-0000'));
        $this->assertTrue($validator->isValid('01-01-2003'));
        $this->assertTrue($validator->isValid('30-04-2014'));

        $this->assertFalse($validator->isValid('01-01-22-45'));
        $this->assertFalse($validator->isValid('01-00-2014'));
        $this->assertFalse($validator->isValid('31-02-2014'));
        $this->assertFalse($validator->isValid('2-2-2245'));
        $this->assertFalse($validator->isValid('-2-34-2001'));
        $this->assertFalse($validator->isValid('test'));
        $this->assertFalse($validator->isValid(NULL));
        $this->assertFalse($validator->isValid(array()));
    }

    /**
     * @covers Forms\Validators\DateFormat::isValid
     */
    public function testIsValidOtherFormats() 
    {
        $validator = new DateFormat('test', 'Y-m-j', NULL, NULL);
        $this->assertTrue($validator->isValid('2014-02-2'));
        $this->assertTrue($validator->isValid('2000-01-1'));
        $this->assertFalse($validator->isValid('2014-02-02'));
        $this->assertFalse($validator->isValid('01-01-2003'));
    }

}
