<?php

use Tests\Helpers\ObjectHelper;

class CardOneTest extends PHPUnit_Framework_TestCase
{
    private $company1;
    private $company2;
    private $businessBankingEmailer;
    private $customer;
    private $oldCustomer;

    /**
     * @var CardOne|PHPUnit_Framework_MockObject_MockObject
     */
    protected $object;


    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->businessBankingEmailer = $this->getMock('BusinessBankingEmailer', array(), array(), '', FALSE);
        $this->object = new CardOne($this->businessBankingEmailer);
        TestHelper::clearTables(
            array(
                TBL_CUSTOMERS,
                TBL_COMPANY,
                TBL_ORDERS,
                TBL_ORDERS_ITEMS,
                TBL_COMPANY_CUSTOMER,
                TBL_CARD_ONE,
            )
        );

        $this->customer = EntityHelper::createCustomer("test@test.co.uk");
        $order = EntityHelper::createOrder($this->customer);

        $this->company1 = EntityHelper::createCompany($this->customer, $order);
        $this->company2 = EntityHelper::createCompany($this->customer, $order);
        $this->company2->setCompanyNumber('090909');
        Registry::$container->get(DiLocator::ENTITY_MANAGER)->flush($this->company2);

        $oldCompany = Company::getCompany($this->company2->getId());
        $this->oldCustomer = new Customer($this->customer->getId());

        CompanyHelper::addCardOne($oldCompany, $this->oldCustomer);
    }

    public function testSelectedCompanies()
    {
        $ids = $this->object->getCompaniesIds();
        $this->assertEquals(array($this->company2->getCompanyId()), $ids);
    }

    public function testJustIncorporatedCompanyEmailIsSent()
    {
        $self = $this;
        $this->businessBankingEmailer->expects($this->exactly(1))
            ->method('sendCompanyToCardOne')
            ->will(
                $this->returnCallback(
                    function ($company, $customer) use ($self) {
                        $self->assertEquals($self->company2->getCompanyId(), $company->getCompanyId());
                    }
                )
            );

        $this->object->sendCardOneCompanies(
            array(
                $this->company1->getCompanyId(),
                $this->company2->getCompanyId()
            )
        );
    }

    /**
     * @depends testSelectedCompanies
     */
    public function testEmailAlreadySent()
    {
        $ids = array($this->company1->getId(), $this->company2->getId());
        $this->object->sendCardOneCompanies($ids);

        $ids = $this->object->getCompaniesIds();
        $this->assertEmpty($ids);
    }

}
