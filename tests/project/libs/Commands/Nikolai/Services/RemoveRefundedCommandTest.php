<?php

namespace Console\Commands\Services;

use Doctrine\ORM\EntityManager;
use Entities\Company;
use Entities\Customer;
use ILogger;
use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;
use Services\ServiceService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Tests\Helpers\ObjectHelper;

class RemoveRefundedCommandTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var ILogger|PHPUnit_Framework_MockObject_MockObject
     */
    private $loggerMock;

    /**
     * @var ServiceService|PHPUnit_Framework_MockObject_MockObject
     */
    private $serviceServiceMock;

    /**
     * @var EntityManager|PHPUnit_Framework_MockObject_MockObject
     */
    private $emMock;

    /**
     * @var RemoveRefundedCommand
     */
    private $object;

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var Company
     */
    private $company;

    /**
     * @var InputInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $inputMock;

    /**
     * @var OutputInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $outputMock;

    protected function setUp()
    {
        $this->loggerMock = $this->getMockBuilder('ILogger')->disableOriginalConstructor()->getMock();
        $this->serviceServiceMock = $this->getMockBuilder('Services\ServiceService')->disableOriginalConstructor()->getMock();
        $this->emMock = $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock();

        $this->customer = ObjectHelper::createCustomer(TEST_EMAIL1);
        $this->company = ObjectHelper::createCompany($this->customer);

        $this->inputMock = $this->getMock('Symfony\Component\Console\Input\InputInterface');
        $this->outputMock = $this->getMock('Symfony\Component\Console\Output\OutputInterface');

        $this->object = new RemoveRefundedCommand($this->loggerMock, $this->serviceServiceMock, $this->emMock);

    }

    /**
     * @covers RemoveRefundedCommand::run()
     */
    public function testRun()
    {
        $service1 = ObjectHelper::createService($this->company);

        $this->inputMock->expects($this->once())->method('getOption')->willReturn(FALSE);
        $this->serviceServiceMock->expects($this->once())->method('getRefundedServices')->willReturn(array(array($service1)));

        $this->emMock->expects($this->once())->method('remove');
        $this->emMock->expects($this->once())->method('flush');
        $this->emMock->expects($this->once())->method('clear');

        $this->object->run($this->inputMock, $this->outputMock);
    }

    /**
     * @covers RemoveRefundedCommand::run()
     */
    public function testRunDryMode()
    {
        $service1 = ObjectHelper::createService($this->company);

        $this->inputMock->expects($this->once())->method('getOption')->willReturn(TRUE);
        $this->serviceServiceMock->expects($this->once())->method('getRefundedServices')->willReturn(array(array($service1)));

        $this->emMock->expects($this->once())->method('remove');
        $this->emMock->expects($this->never())->method('flush');
        $this->emMock->expects($this->never())->method('clear');

        $this->object->run($this->inputMock, $this->outputMock);
    }

    /**
     * @covers RemoveRefundedCommand::run()
     */
    public function testRunMultipleServices()
    {
        $service1 = ObjectHelper::createService($this->company);
        $service2 = ObjectHelper::createService($this->company);

        $this->inputMock->expects($this->once())->method('getOption')->willReturn(FALSE);
        $this->serviceServiceMock->expects($this->once())->method('getRefundedServices')->willReturn(array(array($service1, $service2)));

        $hasError = FALSE;
        $this->loggerMock->expects($this->any())->method('logMessage')->will(
            $this->returnCallback(
                function($level) use(&$hasError) {
                    if ($level === ILogger::ERROR) {
                        $hasError = TRUE;
                    }
                }
            )
        );

        $this->emMock->expects($this->never())->method('remove');

        $this->emMock->expects($this->once())->method('flush');
        $this->emMock->expects($this->once())->method('clear');

        $this->object->run($this->inputMock, $this->outputMock);

        $this->assertTrue($hasError);
    }



}
