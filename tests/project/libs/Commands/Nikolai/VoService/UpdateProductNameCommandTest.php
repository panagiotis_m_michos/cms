<?php

namespace Console\Commands\VoService;

use Doctrine\ORM\EntityManager;
use VoServiceModule\Entities\VoServiceQueue;
use Exceptions\Business\NodeException;
use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;
use Psr\Log\LoggerInterface;
use Services\NodeService;
use VoServiceModule\Services\VoServiceQueueService;
use Tests\Helpers\ObjectHelper;

class UpdateProductNameCommandTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var LoggerInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $loggerMock;

    /**
     * @var EntityManager|PHPUnit_Framework_MockObject_MockObject
     */
    private $emMock;

    /**
     * @var VoServiceQueueService|PHPUnit_Framework_MockObject_MockObject
     */
    private $voServiceQueueService;

    /**
     * @var NodeService|PHPUnit_Framework_MockObject_MockObject
     */
    private $nodeServiceMock;

    /**
     * @var UpdateProductNameCommand
     */
    private $objectMock;


    protected function setUp()
    {
        $this->loggerMock = $this->getMockBuilder('Psr\Log\LoggerInterface')->disableOriginalConstructor()->getMock();
        $this->emMock = $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock();
        $this->voServiceQueueService = $this->getMockBuilder('VoServiceModule\Services\VoServiceQueueService')
            ->disableOriginalConstructor()
            ->getMock();
        $this->nodeServiceMock = $this->getMockBuilder('Services\NodeService')->disableOriginalConstructor()->getMock();

        $this->objectMock = new UpdateProductNameCommand(
            $this->loggerMock,
            $this->emMock,
            $this->voServiceQueueService,
            $this->nodeServiceMock
        );
    }

    /**
     * @covers \Console\Commands\VoService\UpdateProductNameCommand::run()
     */
    public function testRun()
    {
        $productName = 'Product name';
        $voServiceQueue = ObjectHelper::createVoServiceQueue();

        $this->voServiceQueueService
            ->expects($this->once())
            ->method('getServicesWithEmptyProductName')
            ->willReturn([[$voServiceQueue]]);
        $productMock = $this->getMockBuilder('Product')->disableOriginalConstructor()->getMock();
        $productMock->expects($this->once())->method('getLngTitle')->willReturn($productName);
        $this->nodeServiceMock->expects($this->once())->method('getProductById')->willReturn($productMock);

        $this->loggerMock->expects($this->exactly(3))->method('log');
        $this->emMock->expects($this->once())->method('flush');
        $this->emMock->expects($this->once())->method('clear');

        $inputMock = $this->getMockBuilder('Symfony\Component\Console\Input\InputInterface')
            ->disableOriginalConstructor()
            ->getMock();
        $outputMock = $this->getMockBuilder('Symfony\Component\Console\Output\OutputInterface')
            ->disableOriginalConstructor()
            ->getMock();
        $this->objectMock->run($inputMock, $outputMock);
    }

    /**
     * @covers \Console\Commands\VoService\UpdateProductNameCommand::run()
     */
    public function testRunProductDoesNotExist()
    {
        $items = [
            [ObjectHelper::createVoServiceQueue(),],
            [ObjectHelper::createVoServiceQueue(),]
        ];
        $this->voServiceQueueService->expects($this->once())->method('getServicesWithEmptyProductName')->willReturn(
            $items
        );

        $productMock = $this->getMockBuilder('Product')->disableOriginalConstructor()->getMock();
        $this->nodeServiceMock->expects($this->exactly(2))->method('getProductById')->will(
            $this->returnCallback(
                function ($productId) use ($productMock) {
                    if ($productId == 1) {
                        return $productMock;
                    }
                    throw NodeException::nodeDoesNotExist($productId);
                }
            )
        );

        $this->nodeServiceMock->expects($this->exactly(2))->method('getProductById')->willReturnMap(
            [
                [1, $productMock],
                [2, NULL],
            ]
        );

        $this->loggerMock->expects($this->exactly(4))->method('log');
        $this->emMock->expects($this->once())->method('flush');
        $this->emMock->expects($this->once())->method('clear');

        $inputMock = $this->getMockBuilder('Symfony\Component\Console\Input\InputInterface')
            ->disableOriginalConstructor()
            ->getMock();
        $outputMock = $this->getMockBuilder('Symfony\Component\Console\Output\OutputInterface')
            ->disableOriginalConstructor()
            ->getMock();
        $this->objectMock->run($inputMock, $outputMock);
    }
}
