<?php

use tests\helpers\VoucherHelper;

class BasketTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Basket
     */
    private $object;

    public function setUp()
    {
        $this->object = new Basket('test_namespace');
        $this->object->clear(TRUE);
    }

    public function testTotalPrice()
    {
        $this->assertEquals($this->object->getTotalPrice(), 0);
        $this->addProduct();
        // vat added
        $this->assertEquals($this->object->getTotalPrice(), 100 * (1 + Basket::VAT));
    }

    public function testTotalPriceWithCredits()
    {
        $this->addProduct();
        $this->object->setCredit(20);
        $this->assertEquals($this->object->getTotalPrice(), 100);
        $this->object->setCredit(120);
        $this->assertEquals($this->object->getTotalPrice(), 0);
        // is this correct ?
        $this->object->setCredit(130);
        $this->assertEquals($this->object->getTotalPrice(), 120);
    }

    public function testIsUsingCredit()
    {
        $this->assertFalse($this->object->isUsingCredit());
        $this->object->setCredit(100);
        $this->assertTrue($this->object->isUsingCredit());
        $this->object->setCredit(0);
        $this->assertFalse($this->object->isUsingCredit());
    }

    public function testInsufficientCredit()
    {
        $this->addProduct();
        $this->assertFalse($this->object->isInsufficientCredit());
        $this->object->setCredit(100);
        $this->assertTrue($this->object->isInsufficientCredit());
        $this->object->setCredit(120);
        $this->assertFalse($this->object->isInsufficientCredit());
    }

    public function testIsCreditMinimumAmountReached()
    {
        $this->assertFalse($this->object->isCreditMinimumAmountReached(0));
        $this->assertFalse($this->object->isCreditMinimumAmountReached(1));
        $this->assertFalse($this->object->isCreditMinimumAmountReached(100));

        $this->addProduct();

        $this->assertTrue($this->object->isCreditMinimumAmountReached(119));
        $this->assertTrue($this->object->isCreditMinimumAmountReached(119.99));

        $this->assertFalse($this->object->isCreditMinimumAmountReached(120));
        $this->assertFalse($this->object->isCreditMinimumAmountReached(20));


        $this->object->setCredit(100);
        $this->assertFalse($this->object->isCreditMinimumAmountReached(100));
        $this->object->setCredit(119);
        $this->assertTrue($this->object->isCreditMinimumAmountReached(119));
        $this->object->setCredit(119.99);
        $this->assertTrue($this->object->isCreditMinimumAmountReached(119.99));
        $this->object->setCredit(120);
        $this->assertFalse($this->object->isCreditMinimumAmountReached(120));

        $this->object->setCredit(10);
        $this->assertFalse($this->object->isCreditMinimumAmountReached(120));
        $this->assertTrue($this->object->isCreditMinimumAmountReached(119.92));
    }

    public function testIsFreePurchase()
    {
        $this->assertFalse($this->object->isFreePurchase());
        $this->addProduct();
        $this->assertFalse($this->object->isFreePurchase());

        $voucher = VoucherHelper::createDiscount(uniqid(), 100);
        $voucher->save();
        $this->object->setVoucher($voucher->getId());
        $this->assertTrue($this->object->isFreePurchase());

        $this->object->removeVoucher();
        $this->assertFalse($this->object->isFreePurchase());

        $voucher = VoucherHelper::createDiscount(uniqid(), 99);
        $voucher->save();
        $this->object->setVoucher($voucher->getId());
        $this->assertFalse($this->object->isFreePurchase());
    }

    private function addProduct()
    {
        $product = new BasketProduct(RegisterOffice::PRODUCT_BUSINESS_TOOLKIT);
        $product->setPrice(100);
        $product->setStatusId(BasketProduct::STATUS_PUBLISHED);
        $this->object->add($product);
    }
}