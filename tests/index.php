<?php

use Bootstrap\ApplicationLoader;
use TestModule\Boot;

define('DOCUMENT_ROOT', dirname(dirname(__FILE__)));

$config = require_once DOCUMENT_ROOT . '/project/bootstrap/default.php';
$config['show_errors'] = TRUE;
$config['config_path'] = DOCUMENT_ROOT . '/project/config/app/config.test.yml';
$config['dirs_to_load'][] = DOCUMENT_ROOT . '/tests';
$config['environment'] = 'console';

$applicationLoader = new ApplicationLoader();
$container = $applicationLoader->load($config);

Boot::setup($container);

//@TODO find a way to remove it
include_once DOCUMENT_ROOT . '/vendor/made_simple/notifier/tests/Notifier/ModelHelper.php';
include_once DOCUMENT_ROOT . '/vendor/made_simple/sagepaytoken/tests/SagePay/Token/PaymentHelper.php';


