<?php

namespace Tests;

use Entities\Customer;
use Entities\Order;
use Entities\OrderItem;
use JourneyProduct;
use Package;
use Product;
use RegisterOffice;
use Entities\Service;

class ProductHelper
{
    /**
     * @param RegisterOffice $mock
     * @return RegisterOffice
     */
    public static function getRegisteredOfficeProduct(RegisterOffice $mock = NULL)
    {
        /* @var $product Product */
        $product = $mock ? $mock : new RegisterOffice();
        $product->nodeId = 165;
        $product->frontControler = 'ProductControler';
        $product->adminControler = 'RegisterOfficeAdminControler';
        $product->price = 45;
        $product->productValue = 45;
        $product->associatedPrice = 40;
        $product->wholesalePrice = 40;
        $product->serviceTypeId = Service::TYPE_REGISTERED_OFFICE;
        $product->setDuration('+12 months');
        $product->renewalProductId = 165;
        return $product;
    }

    /**
     * @param Package $mock
     * @return Package
     */
    public static function getPrivacyPackage(Package $mock = NULL)
    {
        $package = $mock ? $mock : new Package();
        $package->nodeId = 1315;
        $package->frontControler = 'PackageControler';
        $package->adminControler = 'PackageAdminControler';
        $package->price = 59.99;
        $package->productValue = 194.98;
        $package->associatedPrice = 54.99;
        $package->wholesalePrice = 59.99;
        $package->serviceTypeId = Service::TYPE_PACKAGE_PRIVACY;
        $package->setDuration('+12 months');
        $package->renewalProductId = 165;
        $package->orderItem = new OrderItem(new Order(new Customer('test', 'test')));
        return $package;
    }

    /**
     * @param int $productId
     * @return JourneyProduct
     */
    public static function createJourney($productId)
    {
        $product = new JourneyProduct();
        $product->nodeId = $productId;
        return $product;
    }
}
