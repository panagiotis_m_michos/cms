<?php

namespace tests\helpers;

use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;

class MockUnitTestCase extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        MockHelper::setup($this, 'setUpMocks');
    }

    /**
     * @param string $class
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    public function mock($class)
    {
        return MockHelper::mock($this, $class);
    }

}