<?php

namespace Tests\Helpers;

use DateTime;
use Entities\Cashback;
use Entities\OrderItem;
use Entities\Service;
use Entities\Customer;
use Entities\Company;
use Entities\Order;
use Entities\Payment\Token;
use VoServiceModule\Entities\VoServiceQueue;
use Exceptions\Business\NodeException;
use FUser;
use FUserRole;
use Package;
use Product;
use Registry;
use Services\NodeService;
use Utils\Date;
use SagePay\Token\Request\Registration;
use SagePay\Token\Request\Details;
use DiLocator;
use EntityHelper;
use ReflectionProperty;
use Faker\Factory as FakerFactory;

class ObjectHelper
{
    /**
     * @var int
     */
    private static $serviceId = 1;

    /**
     * @param mixed $entity
     * @param string $field
     * @return int|string
     */
    public static function increment($entity, $field)
    {
        $property = new ReflectionProperty($entity, $field);
        $property->setAccessible(TRUE);
        $value = $field === 'formSubmissionId' ? str_pad(self::$serviceId++, 6, '0', STR_PAD_LEFT) : self::$serviceId++;
        $property->setValue($entity, $value);

        return $value;
    }

    /**
     * @param Company $company
     * @param Date $dtStart
     * @param Date $dtExpires
     * @param bool $product
     * @param string $typeId
     * @return Service
     */
    public static function createService(
        Company $company = NULL,
        Date $dtStart = NULL,
        Date $dtExpires = NULL,
        $product = TRUE,
        $typeId = Service::TYPE_REGISTERED_OFFICE,
        $order = NULL
    )
    {
        $customer = new Customer('test', 'test');

        if (!$order) {
            $order = new Order($customer);
        }

        if (!$company) {
            $company = new Company($customer, 'test');
        }
        $product = $product ? new Product() : new Package();
        $product->setCompanyId(1);
        $product->serviceTypeId = $typeId;
        $product->renewalProductId = 165;
        $product->setDuration("+12 months");

        $orderItem = new OrderItem($order);

        $service = new Service($typeId, $product, $company, $orderItem);
        $serviceIdProperty = new ReflectionProperty($service, 'serviceId');
        $serviceIdProperty->setAccessible(TRUE);
        $serviceIdProperty->setValue($service, self::$serviceId++);

        $company->addService($service);
        if ($dtStart) {
            $service->setDtStart($dtStart);
        }
        if ($dtExpires) {
            $service->setDtExpires($dtExpires);
        }
        $service->setRenewalProduct(new Product());

        return $service;
    }

    /**
     * @return Details
     */
    public static function createDetails()
    {
        $det = new Details();
        $det->amount = 12.23;
        $det->currency = 'GBP';
        $det->description = 'description';
        $det->billingSurname = 'Tester';
        $det->billingFirstnames = 'Johny';
        $det->billingAddress1 = '88';
        $det->billingCity = 'London';
        $det->billingPostCode = '412';
        $det->billingCountry = 'GB';
        $det->deliverySurname = 'OtherTester';
        $det->deliveryFirstnames = 'OtherJohny';
        $det->deliveryAddress1 = '13 St. John Street';
        $det->deliveryCity = 'London';
        $det->deliveryPostCode = 'ec1 4r1';
        $det->deliveryCountry = 'GB';
        $det->token = '492900000000649290000000060000';

        return $det;
    }

    /**
     * @return Registration
     */
    public static function createRegistration()
    {
        $req = new Registration();
        $req->cardHolder = 'Big Daddy';
        $req->cardNumber = '4929000000006';
        $req->expiryDate = '1218';
        $req->cardType = 'VISA';
        $req->cv2 = '123';
        $req->currency = 'GBP';

        return $req;
    }

    /**
     * @param Customer $customer
     * @param bool $isActive
     * @param Date|NULL $cardExpiryDate
     * @param bool $produceId
     * @return Token
     */
    public static function createToken(
        Customer $customer,
        $isActive = TRUE,
        Date $cardExpiryDate = NULL,
        $produceId = FALSE
    )
    {
        $cardExpiryDate = $cardExpiryDate ? $cardExpiryDate : new Date();
        $tokenEntity = new Token(
            $customer,
            '{E76C0C13-79F7-E3F1-99B4-02FA7F779B7D}',
            'VISA',
            'lucky luke',
            '0001',
            $cardExpiryDate
        );
        $tokenEntity->setIsCurrent($isActive);
        $details = self::createDetails();
        $tokenService = EntityHelper::getService(DiLocator::SERVICE_TOKEN);
        $tokenService->mapRequestToToken($details, $tokenEntity);
        if ($produceId) {
            static $tokenId = 1;
            $tokenIdReflection = new ReflectionProperty($tokenEntity, 'tokenId');
            $tokenIdReflection->setAccessible(TRUE);
            $tokenIdReflection->setValue($tokenEntity, $tokenId++);
        }

        return $tokenEntity;
    }

    /**
     * @param string|NULL $email
     * @param string $password
     * @return Customer
     */
    public static function createCustomer($email = NULL, $password = 'n/a')
    {
        $customer = new Customer($email ? $email : TEST_EMAIL1, $password);
        $customer->setFirstName('Johny');
        $customer->setLastName('Tester');
        $customer->setAddress1('145 Road');
        $customer->setAddress2('Faringdon');
        $customer->setPostcode('EC1V 4PQ');
        $customer->setCity('London');
        $customer->setCountryId(223);

        return $customer;
    }

    /**
     * @param Customer $customer
     * @param string $companyName
     * @return Company
     */
    public static function createCompany(Customer $customer, $companyName = 'TEST COMPANY NAME')
    {
        return new Company($customer, $companyName);
    }

    /**
     * @param string $status
     * @return Cashback
     */
    public static function createRetailCashback($status)
    {
        $faker = FakerFactory::create();
        $customer = self::createCustomer($faker->companyEmail);

        return self::createRetailCashbackForCustomer($customer, $status);
    }

    /**
     * @param Customer $customer
     * @param string $status
     * @return Cashback
     */
    public static function createRetailCashbackForCustomer(Customer $customer, $status)
    {
        $faker = FakerFactory::create();

        $cashback = new Cashback(
            self::createCompany(
                $customer,
                $faker->company
            ),
            Cashback::BANK_TYPE_BARCLAYS,
            50,
            new Date($faker->date()),
            new Date($faker->date())
        );
        $cashback->setPackageTypeId(Cashback::PACKAGE_TYPE_RETAIL);
        $cashback->setStatusId($status);

        return $cashback;
    }

    /**
     * @param FUserRole $role
     * @param string $email
     * @param string $login
     * @return FUser
     */
    public static function createUser(FUserRole $role, $email = NULL, $login = NULL)
    {
        $faker = FakerFactory::create();

        $user = FUser::create();
        $user->email = $email ? $email : TEST_EMAIL1;
        $user->statusId = FUser::STATUS_ACTIVE;
        $user->login = $login ? $login : $faker->userName;
        $user->firstName = $faker->firstName;
        $user->lastName = $faker->lastName;
        $user->roleId = $role->getId();
        $user->save();

        return $user;
    }

    /**
     * @param string $key
     * @param string $title
     * @return FUserRole
     */
    public static function createUserRole($key = 'admin', $title = 'admin')
    {
        $role = new FUserRole();
        $role->key = $key;
        $role->title = $title;
        $role->save();

        return $role;
    }

    /**
     * @param Customer $customer
     * @param int $numberOfItems
     * @param DateTime $date
     * @return Order
     */
    public static function createOrder($customer, $numberOfItems = 2, DateTime $date = NULL)
    {
        $order = new Order($customer);
        $order->realSubTotal = $numberOfItems * 2 * 10;
        $order->subTotal = $numberOfItems * 2 * 10;
        $order->vat = $order->subTotal / 5;
        $order->total = $order->subTotal + $order->vat;
        $order->customerName = $customer->getFirstName();
        $order->description = 'Test order';
        $order->setDtc($date ?: new DateTime);
        $order->setDtm($date ?: new DateTime);
        $i = 0;
        while ($numberOfItems > $i) {
            $orderItem = self::createOrderItem($order);
            $order->addItem($orderItem);
            $i++;
        }

        return $order;
    }

    /**
     * @param Order $order
     * @param int $price
     * @param int $qty
     * @param int|NULL $productId
     * @return OrderItem
     */
    public static function createOrderItem($order, $price = 20, $qty = 2, $productId = NULL)
    {
        $orderItem = new OrderItem($order);
        $orderItem->setPrice($price);
        $orderItem->setQty($qty);
        $orderItem->setSubTotal($price);
        $orderItem->setVat($price * 0.2);
        $orderItem->setTotalPrice($orderItem->getSubTotal() + $orderItem->getVat());
        $orderItem->setIsFee(FALSE);
        $orderItem->setProductTitle('Test item');
        $orderItem->setNotApplyVat(FALSE);
        $orderItem->setNonVatableValue(0);

        if ($productId) {
            $orderItem->setProductId($productId);
        }

        return $orderItem;
    }

    /**
     * @param int $serviceTypeId
     * @param int $productId
     * @param Company $company
     * @param OrderItem $orderItem
     * @param DateTime $dtStart
     * @param DateTime $dtExpires
     * @return Service
     * @throws NodeException
     */
    public static function createServiceFromType(
        $serviceTypeId,
        $productId,
        Company $company,
        OrderItem $orderItem,
        DateTime $dtStart = NULL,
        DateTime $dtExpires = NULL
    )
    {
        /** @var NodeService $nodeService */
        $nodeService = Registry::$container->get(DiLocator::SERVICE_NODE);
        $product = $nodeService->getProductById($productId);
        $renewalProduct = $product->getRenewalProduct();

        $service = new Service($serviceTypeId, $product, $company, $orderItem);
        $service->setRenewalProduct($renewalProduct);
        if ($dtStart) {
            $service->setDtStart($dtStart);
        }
        if ($dtExpires) {
            $service->setDtExpires($dtExpires);
        }

        $company->addService($service);

        return $service;
    }

    /**
     * @param int $serviceTypeId
     * @param int $packageId
     * @param Company $company
     * @param OrderItem $orderItem
     * @param DateTime $dtStart
     * @param DateTime $dtExpires
     * @return Service
     * @throws NodeException
     */
    public static function createPackageServiceFromType(
        $serviceTypeId,
        $packageId,
        Company $company,
        OrderItem $orderItem,
        DateTime $dtStart = NULL,
        DateTime $dtExpires = NULL
    )
    {
        /** @var NodeService $nodeService */
        $nodeService = Registry::$container->get(DiLocator::SERVICE_NODE);
        /** @var Package $package */
        $package = $nodeService->getProductById($packageId);
        $renewalPackage = $package->getRenewalProduct();

        $service = new Service($serviceTypeId, $package, $company, $orderItem);
        $service->setRenewalProduct($renewalPackage);
        if ($dtStart) {
            $service->setDtStart($dtStart);
        }
        if ($dtExpires) {
            $service->setDtExpires($dtExpires);
        }

        /** @var Product[] $products */
        $products = $package->getPackageProducts('includedProducts');
        foreach ($products as $product) {
            if ($product->hasServiceType()) {
                $childService = self::createServiceFromType(
                    $product->serviceTypeId,
                    $product->getId(),
                    $company,
                    $orderItem,
                    $dtStart,
                    $dtExpires
                );
                $service->addChild($childService);
            }
        }

        $company->addService($service);

        return $service;
    }

    /**
     * @param Customer $customer
     * @param Company $company
     * @param OrderItem $orderItem
     * @param int $productId
     * @param string $productName
     * @param int $durationInMonths
     * @return VoServiceQueue
     */
    public static function createVoServiceQueue(
        Customer $customer = NULL,
        Company $company = NULL,
        OrderItem $orderItem = NULL,
        $productId = 1,
        $productName = 'Product Name',
        $durationInMonths = 12
    )
    {
        $customer = $customer ?: self::createCustomer();
        $company = $company ?: self::createCompany($customer);
        $orderItem = $orderItem ?: self::createOrderItem(self::createOrder($customer));

        //return new VoServiceQueue($email, $orderId, $productId, $productName, $durationInMonths);
        return new VoServiceQueue($customer, $company, $orderItem, $productId, $productName, $durationInMonths);
    }
}

