<?php

namespace Tests;

use Entities\Payment\Token;
use Entities\Service;
use Entities\ServiceSettings;

class ServiceSettingsHelper
{

    /**
     * @param Service $service
     * @param bool $isAutoRenewalEnabled
     * @param Token $renewalToken
     * @param bool $emailReminders
     * @return ServiceSettings
     */
    public static function fromService(
        Service $service,
        $isAutoRenewalEnabled = TRUE,
        Token $renewalToken = NULL,
        $emailReminders = TRUE
    )
    {
        $settings = new ServiceSettings($service->getCompany(), $service->getServiceTypeId());
        if ($isAutoRenewalEnabled) {
            $settings->setIsAutoRenewalEnabled(TRUE);
        }
        if ($renewalToken) {
            $settings->setRenewalToken($renewalToken);
        }
        if ($emailReminders) {
            $settings->enableEmailReminders();
        }

        return $settings;
    }
}
