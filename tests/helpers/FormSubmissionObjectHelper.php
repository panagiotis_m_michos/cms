<?php

namespace Tests\Helpers;

use Entities\CompanyHouse\Incorporation\Member;
use Nette\Object;
use EntityHelper;
use Entities\CompanyHouse\FormSubmission\OfficerAppointment;
use Entities\CompanyHouse\FormSubmission\OfficerChangeDetails;
use Entities\CompanyHouse\FormSubmission\OfficerResignation;
use Entities\CompanyHouse\FormSubmission\ReturnOfAllotmentShares;
use Entities\CompanyHouse\FormSubmission\ChangeAccountingReferenceDate;
use Entities\CompanyHouse\FormSubmission\ChangeOfName;
use Entities\CompanyHouse\FormSubmission\ChangeRegisteredOfficeAddress;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation;
use Entities\CompanyHouse\FormSubmission\AnnualReturn;
use Entities\CompanyHouse\FormSubmission;
use Entities\CompanyHouse\FormSubmission\AnnualReturn\Shareholding;
use Entities\CompanyHouse\FormSubmission\AnnualReturn\Shareholder;
use Entities\CompanyHouse\FormSubmission\AnnualReturn\Transfer;
use Entities\CompanyHouse\FormSubmission\AnnualReturn\Officer;
use Entities\CompanyHouse\FormSubmission\AnnualReturn\Capital as ARCapital;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation\Capital as CICapital;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation\Document;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation\Subscriber;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation\Director;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation\Secretary;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation\MemberFactory;
use Entities\CompanyHouse\Helper\Authentication;
use Entities\CompanyHouse\Helper\Address;
use Entities\CompanyHouse\Helper\PersonOfficer;
use Entities\CompanyHouse\Helper\CorporateOfficer;
use Entities\CompanyHouse\Helper\EeaIdentification;
use Entities\CompanyHouse\Helper\NonEeaIdentification;
use Entities\CompanyHouse\Helper\PersonDirector;
use Entities\CompanyHouse\Helper\Share;
use DateTime;
use Utils\Date;

class FormSubmissionObjectHelper extends Object
{

    public static function createOfficerAppointment($companyEntity)
    {
        $formSubmission = new OfficerAppointment($companyEntity);
        $formSubmission->setType(Member::TYPE_DIRECTOR);
        $formSubmission->setCorporate(FALSE);
        $formSubmission->setAppointmentDate(new Date);
        $formSubmission->setPremise(123);
        $formSubmission->setSurname('test');
        $formSubmission->setStreet('test');
        return $formSubmission;
    }

    public static function createOfficerChangeDetails($companyEntity)
    {
        $formSubmission = new OfficerChangeDetails($companyEntity);
        return $formSubmission;
    }

    public static function createOfficerResignation($companyEntity)
    {
        $formSubmission = new OfficerResignation($companyEntity);
        return $formSubmission;
    }

    public static function createChangeOfName($companyEntity)
    {
        $formSubmission = new ChangeOfName($companyEntity);
        $formSubmission->setMeetingDate(new Date);
        $formSubmission->setMethodOfChange('RESOLUTION');
        $formSubmission->setNoticeGiven(1);
        $formSubmission->setNewCompanyName('newName');
        return $formSubmission;
    }

    public static function createChangeRegisteredOfficeAddress($companyEntity)
    {
        $formSubmission = new ChangeRegisteredOfficeAddress($companyEntity);
        return $formSubmission;
    }

    public static function createCompanyIncorporation($companyEntity)
    {
        $formSubmission = new CompanyIncorporation($companyEntity);
        $formSubmission->setMsgAddress(TRUE);
        $formSubmission->setSameDay(TRUE);
        $formSubmission->setRegisteredOfficeAddress(new Address('premise', 'street', 'postTown', 'postcode', 'country'));
        $formSubmission->addAppointment(self::createPersonDirector());
        $formSubmission->addAppointment(self::createCorporateDirectorEea());
        $formSubmission->addAppointment(self::createCorporateDirectorNonEea());
        $formSubmission->addAppointment(self::createPersonSecretary());
        $formSubmission->addAppointment(self::createCorporateSecretaryEea());
        $formSubmission->addAppointment(self::createCorporateSecretaryNonEea());
        $formSubmission->addSubscriber(self::createPersonSubscriber());
        $formSubmission->addSubscriber(self::createCorporateSubscriber());
        $formSubmission->addCapital(new CICapital('GBP', 'ORD', 'bla bla', 2, 2, 0, 4));
        $formSubmission->addDocument(new Document('MEMARTS', 'article.pdf'));
        return $formSubmission;
    }

    public static function createPersonDirector()
    {
        return new Director(
            new PersonDirector(
                'foreName', 'surname', new DateTime, 'nationality', 'occupation', 'countryOfResidence',
                new Address('premise', 'street', 'postTown', 'postcode', 'country'),
                new Address('premise', 'street', 'postTown', 'postcode', 'country')
            ),
            new Authentication('LON', '123', 'BRO')
        );
    }

    public static function createCorporateDirectorEea()
    {
        return new Director(
            new CorporateOfficer(
                'foreName', 'surname', 'corporateName',
                new Address('premise', 'street', 'postTown', 'postcode', 'country'),
                new EeaIdentification('Uk', 12345)
            ),
            new Authentication('LON', '123', 'BRO')
        );
    }

    public static function createCorporateDirectorNonEea()
    {
        return new Director(
            new CorporateOfficer(
                'foreName', 'surname', 'corporateName',
                new Address('premise', 'street', 'postTown', 'postcode', 'country'),
                new NonEeaIdentification('nonuk', 123, 13, 12)
            ),
            new Authentication('LON', '123', 'BRO')
        );
    }

    public static function createPersonSecretary()
    {
        return new Secretary(
            new PersonOfficer(
                'foreName', 'surname', new Address('premise', 'street', 'postTown', 'postcode', 'country')

            ),
            new Authentication('LON', '123', 'BRO')
        );
    }

    public static function createCorporateSecretaryEea()
    {
        return new Secretary(
            new CorporateOfficer(
                'foreName', 'surname', 'corporateName',
                new Address('premise', 'street', 'postTown', 'postcode', 'country'),
                new EeaIdentification('Uk', 12345)
            ),
            new Authentication('LON', '123', 'BRO')
        );
    }

    public static function createCorporateSecretaryNonEea()
    {
        return new Secretary(
            new CorporateOfficer(
                'foreName', 'surname', 'corporateName',
                new Address('premise', 'street', 'postTown', 'postcode', 'country'),
                new NonEeaIdentification('nonuk', 123, 13, 12)
            ),
            new Authentication('LON', '123', 'BRO')
        );
    }

    public static function createPersonSubscriber()
    {
        return new Subscriber(
            new PersonOfficer(
                'foreName', 'surname',
                new Address('premise', 'street', 'postTown', 'postcode', 'country')
            ),
            new Share('GBP', 'ord', 12, 2),
            new Authentication('LON', '123', 'BRO')
        );
    }

    public static function createCorporateSubscriber()
    {
        return new Subscriber(
            new CorporateOfficer(
                'foreName', 'surname', 'corporateName',
                new Address('premise', 'street', 'postTown', 'postcode', 'country')
            ),
            new Share('GBP', 'ord', 12, 2),
            new Authentication('LON', '123', 'BRO')
        );
    }

    public static function createAnnualReturn($companyEntity)
    {
        $formSubmission = new AnnualReturn($companyEntity, 'AnnualReturn');
        $formSubmission->setDtr5(TRUE);
        $formSubmission->addCapital(new ARCapital());
        $formSubmission->addOfficer(new Officer());

        $shareholding = new Shareholding();
        $shareholding->addShareholder(new Shareholder());
        $shareholding->addTransfer(new Transfer(new \DateTime, 33));
        $formSubmission->addShareholding($shareholding);
        return $formSubmission;
    }

    public static function createReturnOfAllotmentShares($companyEntity)
    {
        $formSubmission = new ReturnOfAllotmentShares($companyEntity);
        return $formSubmission;
    }

    public static function createChangeAccountingReferenceDate($companyEntity)
    {
        $formSubmission = new ChangeAccountingReferenceDate($companyEntity);
        $formSubmission->setDate(new Date);
        $formSubmission->setExtensionAuthorisedCode(56);
        $formSubmission->setExtensionReason('erte');
        $formSubmission->setFiveYearExtensionDetails('sdfsf');
        return $formSubmission;
    }

    public static function createFormSubmission($companyEntity, $identifier)
    {
        switch ($identifier) {
            case FormSubmission::TYPE_COMPANY_INCORPORATION:
                $formSubmission = self::createCompanyIncorporation($companyEntity);
                break;
            case FormSubmission::TYPE_CHANGE_OF_NAME:
                $formSubmission = self::createChangeOfName($companyEntity);
                break;
            case FormSubmission::TYPE_ANNUAL_RETURN:
                $formSubmission = self::createAnnualReturn($companyEntity);
                break;
            case FormSubmission::TYPE_CHANGE_REGISTERED_OFFICE_ADDRESS:
                $formSubmission = self::createChangeRegisteredOfficeAddress($companyEntity);
                break;
            case FormSubmission::TYPE_RETURN_OF_ALLOTMENT_SHARES:
                $formSubmission = self::createReturnOfAllotmentShares($companyEntity);
                break;
            case FormSubmission::TYPE_CHANGE_ACCOUNTING_REFERENCE_DATE:
                $formSubmission = self::createChangeAccountingReferenceDate($companyEntity);
                break;
            case FormSubmission::TYPE_OFFICER_APPOINTMENT:
                $formSubmission = self::createOfficerAppointment($companyEntity);
                break;
            case FormSubmission::TYPE_OFFICER_CHANGE_DETAILS:
                $formSubmission = self::createOfficerChangeDetails($companyEntity);
                break;
            case FormSubmission::TYPE_OFFICER_RESIGNATION:
                $formSubmission = self::createOfficerResignation($companyEntity);
                break;
            default:
                $formSubmission = NULL;
                break;
        }
        return $formSubmission;
    }
}
