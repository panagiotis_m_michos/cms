<?php

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Entities\Event;
use Entities\Payment\Token;
use Entities\Service;
use Entities\CompanyHouse\FormSubmission;
use Entities\OrderItem;
use Entities\Company;
use Entities\Customer;
use Entities\Order;
use Entities\Cashback;
use Tests\Helpers\FormSubmissionEntityHelper;
use Tests\Helpers\ObjectHelper;
use Utils\Date;
use Doctrine\ORM\Internal\Hydration\IterableResult;
use Faker\Factory as FakerFactory;

class EntityHelper
{

    /**
     * @var array
     */
    public static $tables = [
        'cms2_customers',
        'ch_company',
        'ch_form_submission',
        'ch_company_documents',
        'ch_company_incorporation',
        'ch_company_member',
        'ch_incorporation_capital',
        'ch_incorporation_member',
        'ch_document',
        'ch_address_change',
        'ch_officer_resignation',
        'ch_officer_appointment',
        'ch_officer_change',
        'ch_return_of_allotment_shares',
        'ch_annual_return',
        'ch_annual_return_officer',
        'ch_annual_return_shareholder',
        'ch_annual_return_shareholding',
        'ch_annual_return_shares',
        'ch_annual_return_transfer',
        'ch_change_company_name',
        'ch_change_accounting_reference_date',
    ];

    /**
     * @var array
     */
    public static $submissionIdentifiers = [
        FormSubmission::TYPE_COMPANY_INCORPORATION => 'ch_company_incorporation',
        FormSubmission::TYPE_CHANGE_REGISTERED_OFFICE_ADDRESS => 'ch_address_change',
        FormSubmission::TYPE_OFFICER_RESIGNATION => 'ch_officer_resignation',
        FormSubmission::TYPE_OFFICER_APPOINTMENT => 'ch_officer_appointment',
        FormSubmission::TYPE_OFFICER_CHANGE_DETAILS => 'ch_officer_change',
        FormSubmission::TYPE_RETURN_OF_ALLOTMENT_SHARES => 'ch_return_of_allotment_shares',
        FormSubmission::TYPE_CHANGE_OF_NAME => 'ch_change_company_name',
        FormSubmission::TYPE_CHANGE_ACCOUNTING_REFERENCE_DATE => 'ch_change_accounting_reference_date',
        FormSubmission::TYPE_ANNUAL_RETURN => 'ch_annual_return',
    ];

    /**
     * @param string $name
     * @return object
     */
    public static function getService($name)
    {
        return Registry::$container->get($name);
    }

    /**
     * @return EntityManager
     */
    public static function getEntityManager()
    {
        return self::getService(DiLocator::ENTITY_MANAGER);
    }

    public static function clear()
    {
        $em = self::getEntityManager();
        $em->clear();
    }

    /**
     * @param string $entityName
     * @param array $params
     * @return mixed
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public static function getSingle($entityName, array $params)
    {
        $result = self::getRepository($entityName)->findBy($params);
        if (!$result) {
            throw new NoResultException;
        }
        if (count($result) > 1) {
            throw new NonUniqueResultException(sprintf('Found more than one %s with params %s', $entityName, var_export(array_keys($params), TRUE)));
        }
        return reset($result);
    }

    /**
     * @param string $entityName
     * @return EntityRepository|mixed
     */
    public static function getRepository($entityName)
    {
        $em = self::getEntityManager();
        return $em->getRepository($entityName);
    }

    /**
     * @param array $entities
     */
    public static function save(array $entities)
    {
        $em = self::getEntityManager();
        foreach ($entities as $entity) {
            $em->persist($entity);
        }
        $em->flush();
    }

    /**
     * Generate a random string
     *
     * @param int $length
     * @param int $mode 1 = Alpha, 2 = Alpha-numeric, 3 = Alpha-numeric with symbols
     * @param bool $char_set Set true for Upper and Lower case letters
     * @return string
     */
    public static function randomString($length = 16, $mode = 1, $char_set = FALSE)
    {
        $string = '';
        $possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        if ($char_set) {

            $possible .= strtolower($possible);
        }

        switch ($mode) {
            case 2:
                $possible .= '0123456789';
                break;
            case 3:
                $possible .= '0123456789';
                $possible .= '`~!@#$%^&*()_-+=|}]{[":;<,>.?/';
                break;
        }

        for ($i = 1; $i < $length; $i++) {
            $char = substr($possible, mt_rand(0, strlen($possible) - 1), 1);
            $string .= $char;
        }

        return $string;
    }

    /**
     * @param array $tables
     * @throws Doctrine\DBAL\DBALException
     */
    public static function emptyTables(array $tables)
    {
        $em = self::getEntityManager();
        $q = $em->getConnection();
        foreach ($tables as $table) {
            $q->executeQuery("SET FOREIGN_KEY_CHECKS=0");
            $q->executeQuery("TRUNCATE $table");
            $q->executeQuery("SET FOREIGN_KEY_CHECKS=1;");
        }
        self::clear();
    }

    /**
     * @param array $entities
     */
    public static function remove(array $entities)
    {
        $em = self::getEntityManager();
        foreach ($entities as $entity) {
            $em->remove($entity);
        }
        $em->flush();
    }

    /**
     * @param string $email
     * @return Customer
     */
    public static function createCustomer($email = NULL)
    {
        $em = self::getEntityManager();
        $customer = ObjectHelper::createCustomer($email);

        $em->persist($customer);
        $em->flush();
        return $customer;
    }

    /**
     * @param Customer $customer
     * @param Order $order
     * @param string|NULL $companyNumber
     * @return Company
     */
    public static function createCompany($customer, $order = NULL, $companyNumber = NULL)
    {
        $em = self::getEntityManager();
        $company = new Company($customer, 'test name');
        if ($companyNumber) {
            $company->setCompanyNumber($companyNumber);
        }
        $em->persist($company);
        if ($order) {
            $company->setOrder($order);
            foreach ($order->getItems() as $item) {
                $item->setCompany($company);
            }
            $em->persist($order);
        }
        $em->flush();
        return $company;
    }

    /**
     * @param Customer $customer
     * @param Order $order
     * @return Company
     */
    public static function createCompanyToIncorporate($customer, $order = NULL)
    {
        $companyEntity = self::createCompany($customer, $order);
        $formSubmission = FormSubmissionEntityHelper::createCompanyIncorporation($companyEntity);
        $companyEntity->addFormSubmission($formSubmission);
        return $companyEntity;
    }

    /**
     * @param Customer $customer
     * @param int $numberOfItems
     * @param DateTime $date
     * @return Order
     */
    public static function createOrder($customer, $numberOfItems = 2, DateTime $date = NULL)
    {
        $em = self::getEntityManager();

        $order = new Order($customer);
        $order->realSubTotal = $numberOfItems * 2 * 10;
        $order->subTotal = $numberOfItems * 2 * 10;
        $order->vat = $order->subTotal / 5;
        $order->total = $order->subTotal + $order->vat;
        $order->description = 'Test order';
        $order->setDtc($date ?: new DateTime);
        $order->setDtm($date ?: new DateTime);
        $em->persist($order);
        $em->flush();
        $i = 0;
        while ($numberOfItems > $i) {
            $orderItem = self::createOrderItem($order);
            $order->addItem($orderItem);
            $i++;
        }
        return $order;
    }

    /**
     * @param Order $order
     * @param int $price
     * @param int $qty
     * @param int|NULL $productId
     * @return OrderItem
     */
    public static function createOrderItem($order, $price = 20, $qty = 2, $productId = NULL)
    {
        $em = self::getEntityManager();
        $orderItem = new OrderItem($order);
        $orderItem->setPrice($price);
        $orderItem->setQty($qty);
        $orderItem->setSubTotal($price);
        $orderItem->setVat($price * 0.2);
        $orderItem->setTotalPrice($orderItem->getSubTotal() + $orderItem->getVat());
        $orderItem->setIsFee(FALSE);
        $orderItem->setProductTitle('Test item');
        $orderItem->setNotApplyVat(FALSE);
        $orderItem->setNonVatableValue(0);

        if ($productId) {
            $orderItem->setProductId($productId);
        }

        $em->persist($orderItem);
        $em->flush();
        return $orderItem;
    }

    /**
     * @param Company $company
     * @param OrderItem $orderItem
     * @param DateTime $dtStart
     * @param DateTime $dtExpires
     * @param string $serviceTypeId
     * @return Service
     */
    public static function createService($company, $orderItem, $dtStart = NULL, $dtExpires = NULL, $serviceTypeId = Service::TYPE_REGISTERED_OFFICE)
    {
        $em = self::getEntityManager();
        $service = new Service(
            $serviceTypeId, new RegisterOffice(165), $company, $orderItem
        );
        $service->setDtStart($dtStart ? $dtStart : new DateTime('-3 days'));
        $service->setDtExpires($dtExpires ? $dtExpires : new DateTime('20 days'));
        //$service->setStatusId($statusId);
        $em->persist($service);
        $em->flush();
        return $service;
    }

    /**
     * @param Company $company
     * @param string $identifier
     * @return FormSubmission
     */
    public static function createFormSubmission($company, $identifier)
    {
        $em = self::getEntityManager();
        $formSubmission = new FormSubmission($company, $identifier);
        $em->persist($formSubmission);
        $em->flush();
        return $formSubmission;
    }

    /**
     * @param $formSubmission
     * @param string $type
     * @return DibiRow|FALSE
     */
    public static function createCompanyIncorporation($formSubmission, $type)
    {
        $submissionId = isset($formSubmission->form_submission_id) ? $formSubmission->form_submission_id : $formSubmission->getId();
        $companyIncorporation = [
            'form_submission_id' => $submissionId,
            'type' => $type
        ];
        dibi::insert(TBL_COMPANY_INCORPORATIONS, $companyIncorporation)->execute();
        return dibi::select('*')->from(TBL_COMPANY_INCORPORATIONS)
                ->where('form_submission_id = %s', $submissionId)
                ->execute()->fetch();
    }

    /**
     * @param FormSubmission $formSubmission
     * @return DibiRow|FALSE
     */
    public static function createFormSubmissionForm($formSubmission)
    {
        $submissionId = isset($formSubmission->form_submission_id) ? $formSubmission->form_submission_id : $formSubmission->getId();
        $data = [
            'form_submission_id' => $submissionId,
        ];

        $identifier = $formSubmission->getFormIdentifier();
        $tables = self::$submissionIdentifiers;

        dibi::insert($tables[$identifier], $data)->execute();
        return dibi::select('*')->from($tables[$identifier])
                ->where('form_submission_id = %s', $submissionId)
                ->execute()->fetch();
    }

    /**
     * @param $formSubmission
     * @param string $type
     * @return DibiRow|FALSE
     */
    public static function createIncorporationMember($formSubmission, $type)
    {
        $submissionId = isset($formSubmission->form_submission_id) ? $formSubmission->form_submission_id : $formSubmission->getId();
        $memberIncorporation = [
            'form_submission_id' => $submissionId,
            'type' => $type
        ];
        $memberIncorporationId = dibi::insert(TBL_INCORPORATION_MEMBERS, $memberIncorporation)->execute(dibi::IDENTIFIER);
        return dibi::select('*')->from(TBL_INCORPORATION_MEMBERS)
                ->where('incorporation_member_id = %i', $memberIncorporationId)
                ->execute()->fetch();
    }

    /**
     * @param $formSubmission
     * @param string $category
     * @return DibiRow|FALSE
     */
    public static function createDocument($formSubmission, $category)
    {
        $submissionId = isset($formSubmission->form_submission_id) ? $formSubmission->form_submission_id : $formSubmission->getId();
        $document = [
            'form_submission_id' => $submissionId,
            'category' => $category
        ];
        $documentId = dibi::insert(TBL_DOCUMENTS, $document)->execute(dibi::IDENTIFIER);
        return dibi::select('*')->from(TBL_DOCUMENTS)
                ->where('document_id = %i', $documentId)
                ->execute()->fetch();
    }

    /**
     * @param Customer $customer
     * @param bool $isActive
     * @param NULL|Date $cardExpiryDate
     * @return Token
     */
    public static function createToken(Customer $customer, $isActive = TRUE, Date $cardExpiryDate = NULL)
    {
        $em = self::getEntityManager();
        $tokenEntity = ObjectHelper::createToken($customer, $isActive, $cardExpiryDate);
        $em->persist($tokenEntity);
        $em->flush();
        return $tokenEntity;
    }

    /**
     * @param IterableResult $iterator
     * @return array
     */
    public static function iteratorToArray(IterableResult $iterator)
    {
        $entities = [];
        foreach ($iterator as $row) {
            $entities[] = $row[0];
        }
        return $entities;
    }

    /**
     * @param string $status
     * @return Cashback
     */
    public static function createRetailCashback($status)
    {
        $faker = FakerFactory::create();
        $customer = EntityHelper::createCustomer($faker->companyEmail);
        $cashback = ObjectHelper::createRetailCashbackForCustomer($customer, $status);
        return $cashback;
    }

    /**
     * @param Customer $customer
     * @param string $status
     * @return Cashback
     */
    public static function createRetailCashbackForCustomer(Customer $customer, $status)
    {
        $em = self::getEntityManager();
        $cashback = ObjectHelper::createRetailCashbackForCustomer($customer, $status);

        $em->persist($cashback);
        $em->flush();
        return $cashback;
    }

    /**
     * @param string $eventKey
     * @param int $objectId
     * @return Event
     */
    public static function createEvent($eventKey, $objectId)
    {
        $em = self::getEntityManager();
        $event = new Event();
        $event->setEventKey($eventKey);
        $event->setObjectId($objectId);
        $em->persist($event);
        $em->flush();

        return $event;
    }
}
