<?php

use Nette\Object;
use CompanyHouse\Filling\Responses\SubmissionStatus;
use CompanyHouse\Filling\Responses\Internal\IncorporationDetails;
use CompanyHouse\Filling\Responses\Internal\Examiner;
use CompanyHouse\Filling\Responses\Internal\Rejection;
use CompanyHouse\Filling\Responses\Internal\ChangeOfNameDetails;

class SubmisionResponseHelper extends Object
{

    public static function createOtherAccept($id)
    {
        $submission = new SubmissionStatus($id, SubmissionStatus::STATUS_ACCEPT);
        $submission->setCompanyNumber(05120000);
        $submission->setExaminer(new Examiner('111111'));
        return $submission;
    }

    public static function createCompanyIncorporationAccept($id)
    {
        $submission = self::createOtherAccept($id);
        $submission->setIncorporationDetails(new IncorporationDetails('abcdefghijklmnopqrstuvwxyz', '2010-12-21', 1234));
        return $submission;
    }

    public static function createNameChangeAccept($id)
    {
        $submission = self::createOtherAccept($id);
        $submission->setChangeOfNameDetails(new ChangeOfNameDetails('abcdefghijklmnopqrstuvwxyz'));
        return $submission;
    }

    public static function createCompanyIncorporationReject($id)
    {
        $submission = new SubmissionStatus($id, SubmissionStatus::STATUS_REJECT);
        $submission->addRejection(new Rejection(1, 'rejected'));
        $submission->setExaminer(new Examiner('111111'));
        return $submission;
    }

    public static function createOtherReject($id)
    {
        $submission = self::createOtherAccept($id);
        $submission->setCompanyNumber(05120000);
        return $submission;
    }

    public static function createPending($id)
    {
        $submission = new SubmissionStatus($id, SubmissionStatus::STATUS_PENDING);
        return $submission;
    }

    public static function createFailure($id)
    {
        $submission = new SubmissionStatus($id, SubmissionStatus::STATUS_INTERNAL_FAILURE);
        return $submission;
    }

}
