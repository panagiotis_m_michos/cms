<?php

namespace Tests;

use Entities\OrderItem;
use Entities\Service;
use Product;
use Package;
use Entities\Customer;
use Entities\Order;
use Entities\Company;
use DateTime;

class ServicesHelper
{
    /**
     * @param string $type
     * @return Service
     */
    public static function getService($type = Service::TYPE_REGISTERED_OFFICE, $package = FALSE)
    {
        $product = $package ? new Package(0) : new Product(0); 
        return new Service(
            $type,
            $product,
            new Company(new Customer('test', 'test'), 'test'),
            new OrderItem(new Order(new Customer('test', 'test')))
        );
    }

    /**
     * @return Service
     */
    public static function getOverDueService()
    {
        $service = self::getService();
        $service->setDtStart(new DateTime('-1 year'));
        $service->setDtExpires(new DateTime('-1 day'));
        return $service;
    }

    /**
     * @return Service
     */
    public static function getActiveService()
    {
        $service = self::getService();
        $service->setDtStart(new DateTime('-1 day'));
        $service->setDtExpires(new DateTime('+1 day'));
        return $service;
    }
}
