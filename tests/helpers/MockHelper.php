<?php

namespace tests\helpers;

use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;
use ReflectionMethod;

class MockHelper
{

    /**
     * @param PHPUnit_Framework_TestCase $testCase
     * @param string $method
     * @return PHPUnit_Framework_MockObject_MockObject[]
     */
    public static function setup(PHPUnit_Framework_TestCase $testCase, $method)
    {
        $reflectionMethod = new ReflectionMethod($testCase, $method);
        $arguments = [];
        foreach ($reflectionMethod->getParameters() as $parameter) {
            $arguments[] = self::mock($testCase, $parameter->getClass()->getName());
        }
        call_user_func_array([$testCase, $method], $arguments);
        return $arguments;
    }

    /**
     * @param PHPUnit_Framework_TestCase $testCase
     * @param string $object
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    public static function mock(PHPUnit_Framework_TestCase $testCase, $object)
    {
        return $testCase->getMockBuilder($object)->disableOriginalConstructor()->getMock();
    }

}
