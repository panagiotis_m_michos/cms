<?php

namespace tests\helpers;

use DateTime;
use Entities\Customer;
use FEmail;
use LoggableModule\Entities\EmailLog;

class EmailHelper
{
    /**
     * @param Customer $customer
     * @param FEmail $email
     * @param DateTime $dtc
     * @return EmailLog
     */
    public static function createLog(Customer $customer, FEmail $email, DateTime $dtc)
    {
        $emailLog = EmailLog::fromEmailNode($email, $customer);
        $emailLog->setDtc($dtc);
        return $emailLog;
    }

}