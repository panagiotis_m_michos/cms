<?php

/**
 * CMS
 *
 * @license    GPL
 * @category   Project
 * @package    SagePay
 * @author     Tomas Jakstas tomasj@madesimplegroup.com
 * @version    2011-01-04
 */
class TestHelper
{

    public static function clearTables(array $tables)
    {
        foreach ($tables as $table) {
            FApplication::$db->query('TRUNCATE %n', $table);
        }
    }

    public static function createCustomer($email = NULL)
    {
        $customer = new Customer();
        $customer->firstName = 'Johny';
        $customer->lastName = 'Tester';
        $customer->email = $email ? $email : TEST_EMAIL1;
        $customer->address1 = '';
        $customer->address1 = '145 Road';
        $customer->address2 = 'Faringdon';
        $customer->countryId = 'GB';
        $customer->postcode = 'EC1V 4PQ';
        $customer->city = 'London';
        $customer->countryId = 223;
        $customer->save();
        return $customer;
    }

    public static function createOrder(Customer $customer, $numberOfItems = 1, $productId = 0)
    {
        $order = new Order();
        $order->statusId = Order::STATUS_NEW;
        $order->customerId = $customer->getId();
        $order->subtotal = $numberOfItems * 2 * 10;
        $order->vat = $order->subtotal / 5;
        $order->total = $order->subtotal + $order->vat;
        $order->customerName = $customer->firstName;
        $order->description = 'Test order';
        $order->save();

        $orderItem = new OrderItem();
        $orderItem->orderId = $order->getId();
        $orderItem->productId = $productId;
        $orderItem->qty = $numberOfItems;
        $orderItem->price = 10;
        $orderItem->subTotal = $numberOfItems * $orderItem->price;
        $orderItem->vat = $orderItem->subTotal / 5;
        $orderItem->totalPrice = $orderItem->subTotal + $orderItem->vat;
        $orderItem->productTitle = 'Test product';
        $orderItem->save();

        $orderItem = new OrderItem();
        $orderItem->orderId = $order->getId();
        $orderItem->productId = $productId;
        $orderItem->qty = $numberOfItems;
        $orderItem->price = 10;
        $orderItem->subTotal = $numberOfItems * $orderItem->price;
        $orderItem->vat = $orderItem->subTotal / 5;
        $orderItem->totalPrice = $orderItem->subTotal + $orderItem->vat;
        $orderItem->productTitle = 'Test product';
        $orderItem->save();

        return $order;
    }

    public static function createVoucher()
    {
        $voucher = new VoucherNew();
        $voucher->name = 'test voucher';
        $voucher->typeId = VoucherNew::TYPE_AMOUNT;
        $voucher->typeValue = 10;
        $voucher->voucherCode = VoucherNew::generateUniqueCode('test');
        $voucher->save();
        return $voucher;
    }

    public static function createEmail()
    {
        $email = new FEmail(278);
        $email->addTo(TEST_EMAIL1);
        return $email;
    }

}
