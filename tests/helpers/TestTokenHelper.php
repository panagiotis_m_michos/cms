<?php

/**
 * cms
 *
 * @license    GPL
 * @category   Project
 * @package    cms
 * @author     Razvan Preda <razvanp@madesimplegroup.com>
 * @version    TestTokenHelper.php 21-Aug-2012 
 */

class TestTokenHelper {
    
    //put your code here
    public static function createCustomerToken() {
		$customer = TestHelper::createCustomer('tomasj@madesimplegroup.com');
		//save in db
        $response['Token'] = "{7FBA2DD1-A40F-A6E8-500F-12C273D08A00}";
        //$response['Token'] = "{ABE222AC-5968-94A6-1C25-B001A23A94C7}";
        //$response['Token'] = "{A46D123D-5AC5-3E29-B1ED-F7CF16662908}";

        $token = new Tokens();
        $token->customerId  = $customer->getId();
        $token->token       = trim(str_replace(array('{','}'), '', $response['Token']));
        $token->cardNumber  = "0006";

        $token->firstname   = $customer->firstName;
        $token->surname     = $customer->lastName;
        $token->address1    = $customer->address1;
        $token->address2    = $customer->address2;     //optional
        $token->city        = $customer->city;
        $token->state       = NULL;     //optional
        $token->postCode    = $customer->postcode;
        $token->country     = $customer->country;
        $token->email       = $customer->email;
        //$token->phone       = $address->getPhone();     //optional

        $token->save();
	}
    
    public static function createIndividualToken() {
            
            //ALTER TABLE  `cms2_tokens` ADD  `email` VARCHAR( 120 ) NULL DEFAULT NULL AFTER  `state`; 21-08-2012
            //ALTER TABLE  `cms2_tokens` CHANGE  `customerId`  `customerId` INT( 11 ) NULL DEFAULT NULL; 21-08-2012
        
            //save in db
            $response['Token'] = "{7FBA2DD1-A40F-A6E8-500F-12C273D08A00}";
            //$response['Token'] = "{ABE222AC-5968-94A6-1C25-B001A23A94C7}";
            //$response['Token'] = "{A46D123D-5AC5-3E29-B1ED-F7CF16662908}";
        
            $token = new Tokens();
            $token->customerId  = NULL;
            $token->token       = trim(str_replace(array('{','}'), '', $response['Token']));
            $token->cardNumber  = "0006";
            
            $token->firstname   = "Razvan";
            $token->surname     = "Preda";
            $token->address1    = "8 newlyn gardens";
            $token->address2    = "Harrow";     //optional
            $token->city        = "London";
            $token->state       = NULL;     //optional
            $token->postCode    = "HA1 9TV";
            $token->country     = "GB";
            $token->email       = "razvanp@madesimplegroup.com";
            //$token->phone       = $address->getPhone();     //optional

            $token->save(); 		

    }
    
    
    public static function createTokenLog() {
        
        $tokenLog = new TokenLogs();
        $tokenLog->tokenStatus      = TokenLogs::TOKEN_STATUS_INCOLPLETE;
        $tokenLog->token            = 'C00711FD-D54E-7651-F1C2-7CVF';
        $tokenLog->cardNumber       = '0006';
        $tokenLog->name             = 'Razvan Preda';
        $tokenLog->address          = '88 Road, London, GB';
        $tokenLog->email            = 'razvanp@madesimplegroup.com';
        $tokenLog->dtc              = new DateTime;
        $tokenLog->dtm              = new DateTime;
        
        $id = $tokenLog->save();
        return new TokenLogs($id);
    }
    
}
