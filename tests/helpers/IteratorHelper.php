<?php

namespace tests\helpers;

use ArrayIterator;
use Doctrine\ORM\Internal\Hydration\IterableResult;
use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;
use stdClass;

class MockHydrator
{
    private $iterator;

    public function __construct(array $arr)
    {
        $this->iterator = new ArrayIterator($arr);
    }

    /**
     * @return bool|mixed
     */
    public function hydrateRow()
    {
        if ($this->iterator->valid()) {
            $current = $this->iterator->current();
            $this->iterator->next();
            return [$current];
        }
        return FALSE;
    }
}

class IteratorHelper
{
    /**
     * @param IterableResult $iterator
     * @return array
     */
    public static function doctrineIteratorToArray(IterableResult $iterator)
    {
        $entities = array();
        foreach ($iterator as $row) {
            $entities[] = $row[0];
        }
        return $entities;
    }

    /**
     * @param array $arr
     * @return IterableResult
     */
    public static function createDoctrine(array $arr)
    {
        return new IterableResult(new MockHydrator($arr));
    }

    /**
     * Setup methods required to mock an iterator
     *
     * @param PHPUnit_Framework_TestCase $testCase
     * @param PHPUnit_Framework_MockObject_MockObject $iteratorMock The mock to attach the iterator methods to
     * @param array $items The mock data we're going to use with the iterator
     * @return PHPUnit_Framework_MockObject_MockObject The iterator mock
     */
    public static function mockIterator(
        PHPUnit_Framework_TestCase $testCase,
        PHPUnit_Framework_MockObject_MockObject $iteratorMock,
        array $items
    )
    {
        $iteratorData = new stdClass();
        $iteratorData->array = $items;
        $iteratorData->position = 0;

        $iteratorMock->expects($testCase->any())
            ->method('rewind')
            ->will(
                $testCase->returnCallback(
                    function () use ($iteratorData) {
                        $iteratorData->position = 0;
                    }
                )
            );

        $iteratorMock->expects($testCase->any())
            ->method('current')
            ->will(
                $testCase->returnCallback(
                    function () use ($iteratorData) {
                        return $iteratorData->array[$iteratorData->position];
                    }
                )
            );

        $iteratorMock->expects($testCase->any())
            ->method('key')
            ->will(
                $testCase->returnCallback(
                    function () use ($iteratorData) {
                        return $iteratorData->position;
                    }
                )
            );

        $iteratorMock->expects($testCase->any())
            ->method('next')
            ->will(
                $testCase->returnCallback(
                    function () use ($iteratorData) {
                        $iteratorData->position++;
                    }
                )
            );

        $iteratorMock->expects($testCase->any())
            ->method('valid')
            ->will(
                $testCase->returnCallback(
                    function () use ($iteratorData) {
                        return isset($iteratorData->array[$iteratorData->position]);
                    }
                )
            );

        $iteratorMock->expects($testCase->any())
            ->method('count')
            ->will(
                $testCase->returnCallback(
                    function () use ($iteratorData) {
                        return count($iteratorData->array);
                    }
                )
            );

        return $iteratorMock;
    }
}