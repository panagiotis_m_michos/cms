<?php

use BankingModule\Entities\CompanyCustomer as BankingCompanyCustomer;
use Nette\Object;
use Entities\Customer as CustomerEntity;

class CompanyHelper extends Object
{

    /**
     * @param Customer|CustomerEntity $customer
     * @param string $companyNumber
     * @return Company
     */
    public static function createCompany($customer, $companyNumber = NULL)
    {
        $company = Company::getNewCompany($customer->getId(), 'Test Company');

        if (isset($companyNumber)) {
            $company->setCompanyNumber($companyNumber);
        }

        return $company;
    }

    /**
     * @param Company $company
     * @param Customer $customer
     */
    public static function addCardOne(Company $company, Customer $customer)
    {
        $companyCustomer = new CompanyCustomer();
        $companyCustomer->bankTypeId = BankingCompanyCustomer::BANK_TYPE_CARD_ONE;
        $companyCustomer->preferredContactDate = 0;
        $companyCustomer->wholesalerId = $customer->getId();
        $companyCustomer->companyId = $company->getCompanyId();
        $companyCustomer->email = 'TEST';
        $companyCustomer->titleId = 'TEST';
        $companyCustomer->firstName = 'TEST';
        $companyCustomer->lastName = 'TEST';
        $companyCustomer->phone = 'TEST';
        $companyCustomer->postcode = 'TEST';

        $companyCustomer->address1 = 'NULL';
        $companyCustomer->address3 = 'NULL';
        $companyCustomer->city = 'NULL';
        $companyCustomer->countryId = 'NULL';
        $companyCustomer->consent = 0;

        $companyCustomer->save();

        $service = new CFControllerModel($company, $customer);
        $service->addBankOrderItem(CardOne::CARD_ONE_PRODUCT);
    }

}
