<?php

namespace Tests\Helpers;

use Nette\Object;
use EntityHelper;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation;
use Entities\CompanyHouse\FormSubmission\AnnualReturn;
use Entities\CompanyHouse\FormSubmission;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation\MemberFactory;

class FormSubmissionEntityHelper extends Object
{

    public static function createOfficerAppointment($companyEntity)
    {
        $formSubmission = FormSubmissionObjectHelper::createOfficerAppointment($companyEntity);
        self::save($formSubmission);
        return $formSubmission;
    }

    public static function createOfficerChangeDetails($companyEntity)
    {
        $formSubmission = FormSubmissionObjectHelper::createOfficerChangeDetails($companyEntity);
        self::save($formSubmission);
        return $formSubmission;
    }

    public static function createOfficerResignation($companyEntity)
    {
        $formSubmission = FormSubmissionObjectHelper::createOfficerResignation($companyEntity);
        self::save($formSubmission);
        return $formSubmission;
    }

    public static function createChangeOfName($companyEntity)
    {
        $formSubmission = $formSubmission = FormSubmissionObjectHelper::createChangeOfName($companyEntity);
        self::save($formSubmission);
        return $formSubmission;
    }

    public static function createChangeRegisteredOfficeAddress($companyEntity)
    {
        $formSubmission = $formSubmission = FormSubmissionObjectHelper::createChangeRegisteredOfficeAddress($companyEntity);
        self::save($formSubmission);
        return $formSubmission;
    }

    public static function createCompanyIncorporation($companyEntity)
    {
        $formSubmission = FormSubmissionObjectHelper::createCompanyIncorporation($companyEntity);
        self::save($formSubmission);
        return $formSubmission;
    }

    public static function createAnnualReturn($companyEntity)
    {
        $formSubmission = FormSubmissionObjectHelper::createAnnualReturn($companyEntity);
        self::save($formSubmission);
        return $formSubmission;
    }

    public static function createReturnOfAllotmentShares($companyEntity)
    {
        $formSubmission = FormSubmissionObjectHelper::createReturnOfAllotmentShares($companyEntity);
        self::save($formSubmission);
        return $formSubmission;
    }

    public static function createChangeAccountingReferenceDate($companyEntity)
    {
        $formSubmission = FormSubmissionObjectHelper::createChangeAccountingReferenceDate($companyEntity);
        self::save($formSubmission);
        return $formSubmission;
    }

    public static function save($formSubmission)
    {
        $em = EntityHelper::getEntityManager();
        $em->persist($formSubmission);
        $em->flush();
    }

}
