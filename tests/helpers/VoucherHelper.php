<?php

namespace tests\helpers;

use VoucherNew;

class VoucherHelper
{
    /**
     * @param string $code
     * @param int $discount
     * @return VoucherNew
     */
    public static function createDiscount($code, $discount)
    {
        $voucher = new VoucherNew();
        $voucher->voucherCode = $code;
        $voucher->name = $code;
        $voucher->typeId = VoucherNew::TYPE_PERCENT;
        $voucher->typeValue = $discount;
        return $voucher;
    }
}