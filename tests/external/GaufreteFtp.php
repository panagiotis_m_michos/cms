<?php

namespace external;

use Gaufrette\Adapter\Ftp;
use Gaufrette\Filesystem;
use PHPUnit_Framework_TestCase;

class GaufreteFtp extends PHPUnit_Framework_TestCase
{
    /**
     * @var Filesystem
     */
    private $object;

    public function setUp()
    {
        $this->markTestSkipped('Use this test to test ftp upload and delete');
        $this->object = new Filesystem(
            new Ftp(
                '/imports/fraud_protection_credit_import',
                '192.168.100.244',
                [
                    'username' => '',
                    'password' => '',
                    'ssl' => true,
                    'passive' => true,
                    'mode' => FTP_ASCII
                ]
            )
        );
    }

    public function testFtpUpload()
    {
        $this->object->write('any_unique_name', 'test_content');
    }

    public function testDelete()
    {
        $this->object->delete('any_unique_name');
    }

}