<?php

class EmailTest extends PHPUnit_Framework_TestCase
{

    /**
     * @var string[]
     */
    private $validEmails;

    /**
     * @var string[]
     */
    private $invalidEmails;

    protected function setUp()
    {
        $this->validEmails = [
            'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@letters-in-local.org',
            '01234567890@numbers-in-local.net',
            '&\'*+-./=?^_{}~@other-valid-characters-in-local.net',
            'mixed-1234-in-{+^}-local@sld.net',
            'a@single-character-in-local.org',
            'one-character-third-level@a.example.com',
            'single-character-in-sld@x.org',
            'local@dash-in-sld.com',
            'letters-in-sld@123.com',
            'one-letter-sld@x.org',
            'uncommon-tld@sld.museum',
            'uncommon-tld@sld.travel',
            'uncommon-tld@sld.mobi',
            'country-code-tld@sld.uk',
            'country-code-tld@sld.rw',
            'local@sld.newTLD',
            'punycode-numbers-in-tld@sld.xn--3e0b707e',
            'local@sub.domains.com',
        ];

        $this->invalidEmails = [
            '@missing-local.org',
            'missing-sld@.com',
            'missing-dot-before-tld@com',
            'missing-tld@sld.',
            'missing-at-sign.net',
        ];
    }

    public function testCheckEmail()
    {
        $emailValidator = new Email(NULL, NULL, NULL, NULL);
        foreach ($this->validEmails as $email) {
            $this->assertTrue($emailValidator->isValid($email), 'Email should be valid ' . $email);
        }
        foreach ($this->invalidEmails as $email) {
            $this->assertFalse($emailValidator->isValid($email), 'Email should not be valid ' . $email);
        }
    }

}
