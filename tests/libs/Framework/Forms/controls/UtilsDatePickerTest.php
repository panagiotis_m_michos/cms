<?php

use Utils\Date;

class UtilsDatePickerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var UtilsDatePicker
     */
    private $object;

    protected function setUp()
    {
        $formMock = $this->getMockBuilder('FForm')->disableOriginalConstructor()->getMock();
        $this->object = new UtilsDatePicker('date', 'Date', $formMock);
    }

    /**
     * @covers UtilsDatePicker::setValue
     */
    public function testSetValueNull()
    {
        $this->object->setValue(NULL);
        $this->assertNull($this->object->getValue());
    }

    /**
     * @covers UtilsDatePicker::setValue
     */
    public function testSetValueEmpty()
    {
        $this->object->setValue('');
        $this->assertNull($this->object->getValue());
    }

    /**
     * @covers UtilsDatePicker::setValue
     */
    public function testSetValueInvalidString()
    {
        $this->object->setValue('invalid date');
        $this->assertFalse($this->object->getValue());
    }

    /**
     * @covers UtilsDatePicker::setValue
     * @covers UtilsDatePicker::getValue
     */
    public function testSetValueValidString()
    {
        $this->object->setValue('16/02/1980');
        $this->assertInstanceOf('Utils\Date', $this->object->getValue());
    }

    /**
     * @covers UtilsDatePicker::setValue
     * @covers UtilsDatePicker::getValue
     */
    public function testSetValueValidDate()
    {
        $this->object->setValue(Date::createFromFormat(UtilsDatePicker::DATE_FORMAT, '16/02/1980'));
        $this->assertInstanceOf('Utils\Date', $this->object->getValue());
    }


}