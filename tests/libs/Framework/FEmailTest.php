<?php

class FEmailTest extends PHPUnit_Framework_TestCase
{
    private $mail1 = 'mail@one.com';
    private $mail2 = 'mail@two.com';
    private $attachmentData = array(
        'file' => 'filename',
        'content' => 'attachment content',
        'contentType' => 'text/plain',
    );

    /**
     * @var FEmail
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->object = new FEmail;
    }

    /**
     * @covers FEmail::setTo
     * @covers FEmail::getTo
     */
    public function testGetTo()
    {
        $this->object->setTo($this->mail1);
        $this->assertEquals($this->mail1, $this->object->getTo());
    }

    /**
     * @covers FEmail::setCc
     * @covers FEmail::getCc
     */
    public function testGetCc()
    {
        $this->object->setCc($this->mail1);
        $this->assertEquals($this->mail1, $this->object->getCc());
    }

    /**
     * @covers FEmail::setBcc
     * @covers FEmail::getBcc
     */
    public function testGetBcc()
    {
        $this->object->setBcc($this->mail1);
        $this->assertEquals($this->mail1, $this->object->getBcc());
    }

    /**
     * @covers FEmail::addTo
     */
    public function testAddTo()
    {
        $this->object->addTo($this->mail1);
        $this->object->addTo($this->mail2);
        $this->assertEquals("{$this->mail1},{$this->mail2}", $this->object->getTo());
    }

    /**
     * @covers FEmail::addCc
     */
    public function testAddCc()
    {
        $this->object->addCc($this->mail1);
        $this->object->addCc($this->mail2);
        $this->assertEquals("{$this->mail1},{$this->mail2}", $this->object->getCc());
    }

    /**
     * @covers FEmail::addBcc
     */
    public function testAddBcc()
    {
        $this->object->addBcc($this->mail1);
        $this->object->addBcc($this->mail2);
        $this->assertEquals("{$this->mail1},{$this->mail2}", $this->object->getBcc());
    }

    /**
     * @covers FEmail::addAttachment
     * @covers FEmail::getAttachments
     */
    public function testAddAttachment()
    {
        $file = $this->attachmentData['file'];
        $content = $this->attachmentData['content'];
        $contentType = $this->attachmentData['contentType'];

        $this->object->addAttachment($file, $content, $contentType);
        $attachments = $this->object->getAttachments();
        $expectedAttachments = array(
            $this->attachmentData,
        );
        $this->assertEquals($expectedAttachments, $attachments);
    }

    /**
     * @covers FEmail::addCmsFileAttachment
     * @covers FEmail::getCmsFileAttachments
     */
    public function testAddCmsFileAttachment()
    {
        $cmsFileAttachment = 'file_path';

        $this->object->addCmsFileAttachment($cmsFileAttachment);
        $this->assertEquals(array($cmsFileAttachment), $this->object->getCmsFileAttachments());
    }

    /**
     * @covers FEmail::setCmsFileAttachments
     */
    public function testSetCmsFileAttachments()
    {
        $cmsFileAttachments = array('file_path1, file_path2');

        $this->object->setCmsFileAttachments($cmsFileAttachments);
        $this->assertEquals($cmsFileAttachments, $this->object->getCmsFileAttachments());
    }

    /**
     * @covers FEmail::replacePlaceHolders
     */
    public function testReplacePlaceHolders()
    {
        $this->object->body = 'place[holder]';
        $this->object->replacePlaceHolders(array('[holder]' => 'no'));
        $this->assertEquals('placeno', $this->object->body);
    }

    /**
     * @covers FEmail::isEmail
     */
    public function testIsEmail()
    {
        $this->assertEquals(FALSE, $this->object->isEmail());
        $this->object->adminControler = 'EmailAdminControler';
        $this->assertEquals(TRUE, $this->object->isEmail());
    }
}
