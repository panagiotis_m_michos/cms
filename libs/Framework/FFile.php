<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   CMS
 * @package    Cms\File
 * @version    File.php 2009-10-22 divak@gmail.com
 */

/*namespace Cms\File;*/


/**
 * Work with uploaded files
 * 
 * @author     Stanislav Bazik
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @package    Cms\File
 */


class FFile extends Object
{
	/** @var string */
	public static $storageDir = FILES_DIR;
	
	public static $storageDirR = FILES_DIR_R;
	
	/** @var string */
	protected $table = TBL_FILES;
	
	/** @var int */
	protected $primaryKey = 'fileId';
	
	/** @var int */
	public $fileId;

	/** @var boolean */
	protected $isImage = FALSE;
	
	/** @var boolean */
	public $isActive = TRUE;
	
	/** @var boolean */
	public $isDeleted = FALSE;

	/** @var int */
	public $ord;

	/** @var int */
	public $nodeId;

	/** @var string */
	public $name;
	
	/** @var string */
	public $description;

	/** @var string */
	public $ext;

	/** @var int */
	public $size;

	/** @var int */
	public $authorId;

	/** @var int */
	public $editorId;

	/** @var string */
	public $dtc;

	/** @var string */
	public $dtm;
	
	/** @var string */
	public $nameOld;

	/** @var array */
	protected $uploadedFile = NULL;
	
	/** @var string */
	public $localFile = NULL;
	

	/**
	 * @return int
	 */
	final public function getId() { return ($this->{$this->primaryKey}); }

	/**
	 * @return bool
	 */
	final public function exists() { return (bool) ($this->{$this->primaryKey} > 0); }

	/**
	 * @return bool
	 */
	final public function isNew() { return (bool) ($this->{$this->primaryKey} == 0); }
	
	/**
	 * @param int $fileId
	 * @return void
	 */
	public function __construct($fileId = 0)
	{
		$this->fileId = (int)$fileId;
		if ($this->{$this->primaryKey}) {
			$w = dibi::select('*')->from($this->table)->where('isImg IS NULL')->and('fileId=%i', $this->{$this->primaryKey})->execute()->fetch(); 
			if (!empty($w)) {
				$this->{$this->primaryKey} = $w['fileId'];
				$this->isActive = $w['isActive'];
				$this->isDeleted = $w['isDeleted'];
				$this->ord = $w['ord'];
				$this->nodeId = $w['nodeId'];
				$this->name = $w['name'];
				$this->nameOld = $w['name'];
				$this->description = $w['description'];
				$this->ext = $w['ext'];
				$this->size = $w['size'];
				$this->dtc = $w['dtc'];
				$this->dtm = $w['dtm'];
				$this->authorId = $w['authorId'];
				$this->editorId = $w['editorId'];
			} else {
				throw new Exception("File with id `$fileId` not found!");
			}
		}
	}
	
	
	public function getAuthor()
	{
		try {
			$author = FUser::create($this->authorId);
		} catch (Exception $e) {
			$author = FUser::create();
		}
		return $author;
	}

	/**
	 * @param array $uploadedFile
	 */
	public function setUploadedFile(array $uploadedFile)
	{
		$this->uploadedFile = (object) $uploadedFile;
	}
	
	public function getUploadedFile()
	{
		return $this->uploadedFile;
	}
	
	/**
	 * @return bool
	 */
	public function isActive() 
	{ 
		return (bool)$this->isActive; 
	}
	
	/**
	 * @return bool
	 */
	public function isDeleted() 
	{ 
		return (bool) $this->isDeleted; 
	}
	
	/**
	 * @return bool
	 */
	public function isUploaded() 
	{ 
		return ($this->uploadedFile !== NULL && $this->uploadedFile->error === 0) ? TRUE : FALSE;
	}
	
	/**
	 * @return bool
	 */
	public function isLocalFile() 
	{ 
		return ($this->localFile !== NULL) ? TRUE : FALSE;
	}
		
	/*
	 * @return int
	 * @throws Exception
	 */
	public function save()
	{
		// is dir writeable
		if (!is_dir(self::getStorageDir()) || !is_writable(self::getStorageDir())) {
			throw new Exception('Files directory "' . self::getStorageDir() . '" is not writable.');
		}
		
		// pathinfo
		if (!function_exists('pathinfo')) {
			throw new Exception('Function `pathinfo()` could not be found!');
		}
		
		// check is there is any file
		if ($this->isNew() === TRUE && $this->isUploaded() === FALSE && $this->isLocalFile() === FALSE) {
			throw new Exception('No file has been set!');
		}
		
		// info about file
		$fileInfo = $this->getFileInfo();
		
		// data to save
		$data = array();
		$data['isActive'] = $this->isActive;
		$data['isDeleted'] = $this->isDeleted;
		$data['ord'] = $this->ord ? $this->ord : $this->getNextOrd();
		$data['nodeId'] = $this->nodeId;
		$data['dtm'] =  $this->dtm;
		$data['editorId'] = FUser::getSignedIn()->getId();
		$data['name'] = $fileInfo->name;
		$data['description'] = $this->description;
		$data['ext'] = $fileInfo->ext;
		$data['size'] = $fileInfo->size;
		$data['dtm'] = new DibiDateTime();
		
		// move_uploaded_file or rename
		if ($this->isUploaded()) {
			move_uploaded_file($this->uploadedFile->tmp_name, $fileInfo->filepath);
		// local file
		} elseif ($this->isLocalFile()) {
			@rename($fileInfo->source, $fileInfo->filepath);
		// rename file
		} elseif ($this->name != $this->nameOld) {
			@rename($fileInfo->source, $fileInfo->filepath);
		}
		
		// delete old file
		if ($this->isNew() === FALSE && $this->nameOld != $fileInfo->name) {
			$this->deleteFile();
		}
		
		// insert
		if ($this->isNew()) {
			$data['dtc'] =  new DibiDateTime();
			$data['authorId'] = FUser::getSignedIn()->getId();
			$this->{$this->primaryKey} = dibi::insert($this->table, $data)->execute(dibi::IDENTIFIER);
		} else {
			dibi::update($this->table, $data)->where('fileId=%i', $this->{$this->primaryKey})->execute();
		}
		return $this->{$this->primaryKey};
	}
	
	
	/**
	 * @param array $file
	 * @return array $ret
	 */
	protected function getFileInfo()
	{
		$ret = array();
		
		// edit
		if ($this->isNew() === FALSE && $this->isUploaded() === FALSE && $this->isLocalFile() === FALSE) {
			
			// new name of file
			if ($this->name != $this->nameOld && !empty($this->name)) {
				$ret['name'] = String::webalize($this->name); // safe name
				$ret['name'] = self::getUniqFilename($ret['name'], $this->ext, self::getStorageDir()); // uniq name
			} else {
				$ret['name'] = $this->name;
			}
			
			$ret['ext'] = $this->ext; 
			$ret['size'] = $this->size;
			$ret['source'] = $this->getFilePath();
			$ret['filename'] = $ret['name'] . '.' . $ret['ext'];
			
		} else {
			// uploaded
			if ($this->isUploaded()) {
				$info = pathinfo($this->uploadedFile->name);
				$ret['size'] = $this->uploadedFile->size;
				$ret['source'] = $this->uploadedFile->tmp_name;
				$ret['ext'] = strtolower($info['extension']);
				$ret['name'] = !empty($this->name) ? $this->name : $info['filename'];
				
			// file e.g. from import
			} elseif ($this->isLocalFile()) { 
				$info = pathinfo($this->localFile);
				$ret['size'] = filesize($this->localFile);
				$ret['source'] = $this->localFile;
				$ret['ext'] = strtolower($info['extension']);
				$ret['name'] = !empty($this->name) ? $this->name : $info['filename'];
			}
			
			// new filename
			if ($this->isNew() === TRUE || $this->isNew() === FALSE &&  String::webalize($this->name) != $this->nameOld) {
				$ret['name'] = String::webalize($ret['name']); // safe name
				$ret['name'] = self::getUniqFilename($ret['name'], $ret['ext'], self::getStorageDir()); // uniq name
			}
			$ret['filename'] = $ret['name'] . '.' . $ret['ext'];
		}
		$ret['filepath'] = self::getStorageDir() . '/' . $ret['filename'];
		return (object)$ret;
	}


	/**
	 * @return void
	 */
	public function delete()
	{
		// if its only first delete
		if (!$this->isDeleted()) {
			$this->isDeleted = 1;
			$this->save();
		} else {
			$this->deleteFile();
			dibi::delete($this->table)->where($this->primaryKey.'=%i', $this->getId())->execute();
		}
	}
	
	
	protected function deleteFile()
	{
		if(is_file($this->getFilePath())) {
			@unlink($this->getFilePath());
		}
	}
	
	/**
	 * @param string $name
	 * @param string $ext
	 * @param string $dir
	 * @return string $newName
	 */
	static public function getUniqFilename($name, $ext, $dir)
	{
		$poc=0;
		$newName = $name;
		while (is_file($dir . $newName . '.' . $ext)) {
			$newName = $name.' ('.++$poc.')';
		}
		return $newName;
	}

	

	/**
	 * @return string
	 */
	public function getFileName()
	{
		return $this->nameOld . '.' . $this->ext;
	}
	
	/**
	 * @param boolean $relative
	 * @return string
	 */
	public function getFilePath($relative = FALSE)
	{
		$path = self::getStorageDir($relative) . '/' . $this->getFileName(); 
		return $path;
	}
	
	/**
	 * @param boolean $relative
	 * @return string
	 */
	static public function getStorageDir($relative = FALSE)
	{
		if ($relative === FALSE) {
			$path = self::$storageDir;
		} else {
			$httpRequest = new HttpRequest();
			$path = $httpRequest->getUri()->getBasePath(); 
			$path = self::$storageDirR;
		}
		return $path;
	}
	
	
	/**
	 * @return int $maxOrd
	 */
	protected function getNextOrd()
	{
		$maxOrd = dibi::select('MAX(ord)')->from($this->table)->where('isImg IS NULL')->and('nodeId=%i', $this->nodeId)->execute()->fetchSingle();
		$nextord = (int)$maxOrd + 10;
		return $nextord;
	}
	
	/**
	 * @param boolean $modify
	 * @return string $size
	 */
	public function getSize($modify = TRUE)
	{
		return ($modify === TRUE) ? TemplateHelpers::bytes($this->size) : $this->size;
	}
	
	
	/**
	 * @param boolean $modify
	 * @return string $dtc
	 */
	public function getDtc($modify = TRUE)
	{
		return ($modify === TRUE) ? date('j/m/Y G:i',strtotime($this->dtc)) : $this->dtc;
	}
	
	
	/**
	 * @return mixed
	 */
	public function getHtmlTag()
	{
		$el = Html::el('a')->href($this->getFilePath(TRUE))->title($this->getFilePath(TRUE))->setText($this->getFileName());
		return $el;
	}
	
	
	static public function getAll($where = array(), $limit = NULL, $offset = NULL, $returnAsObjects = TRUE)
	{
		$result = FApplication::$db->select('*')->from(TBL_FILES)->where('isImg IS NULL')->orderBy('dtc', dibi::DESC);
		// where
	 	if (!empty($where)) {
			foreach ($where as $key=>$val) {
				$result->where('%n', $key, ' LIKE %s',$val);
			}
		}
		// limit
		if ($limit !== NULL) $result->limit($limit);
		// offset
		if ($offset !== NULL) $result->offset($offset);
		$return = $result->execute()->fetchAll();
		// orders
		if ($returnAsObjects === TRUE) {
			$files = array();
			foreach ($return as $key => $val) {
				$files[$val->fileId] = new self($val->fileId);
			}
			return $files;
		}
		return $return;
	}
	
	
	
	static public function getCount($where = array())
	{
		$result = FApplication::$db->select('COUNT(*)')->from(TBL_FILES)->where('isImg IS NULL')->orderBy('dtc', dibi::DESC);
		// where
	 	if (!empty($where)) {
			foreach ($where as $key=>$val) {
				$result->where('%n', $key, ' LIKE %s',$val);
			}
		}
		$count = $result->execute()->fetchSingle();
		return $count;
	}
}
