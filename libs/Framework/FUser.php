<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   CMS
 * @package    Cms\User
 * @version    User.php 2009-01-08 divak@gmail.com
 */

/*namespace Cms\User;*/


/**
 * Provides handle with User
 *
 * @author     Stanislav Bazik
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @package    Cms\User
 */
class FUser extends Object
{
	// statuses
	const STATUS_ACTIVE = 1;
	const STATUS_BLOCKED = 2;
	
	/** @var array */
	static public $statuses = Array(
		self::STATUS_ACTIVE => 'Active',
		self::STATUS_BLOCKED => 'Blocked'
	);
	
	// error types
	const USERNAME_NOT_FOUND = 1;
	const PASSWORD_DOESNT_MATCH = 2;
	const ACCOUNT_IS_NOT_ACTIVE = 3;
	
	/** @var int */
	public $userId;

	/** @var int */
	public $statusId;
	
	/** @var string */
	public $status;

	/** @var string */
	public $login;

	/** @var string */
	public $password;

	/** @var string */
	public $firstName;

	/** @var string */
	public $lastName;

	/** @var string */
	public $email;

	/** @var int */
	public $roleId;
	
	/** @var FUserRole */
	public $role;

	/** @var string */
	public $languagePk;

	/** @var string */
	public $lastSigned;

	/** @var int */
	public $authorId;

	/** @var int */
	public $editorId;
    
    /**
     * @var int
     */
	public $hasPhonePaymentAccess = 0;

	/** @var string */
	public $dtc;

	/** @var string */
	public $dtm;
	
	/** @var int */
	static public $counter=0;
	
	/** @var string */
	static private $cacheKey;
	
	/** @var string */
	static private $nameSpace = 'user';

	/**
	 * @param int $userId
	 * @return void
	 */
	private function __construct($userId=0)
	{
		self::$counter++;
		
		$this->userId = (int)$userId; 

		if ($this->userId) {
			$w = FApplication::$db->select('*')
				->from(TBL_USERS)
				->where('user_id=%i', $this->getId())
				->execute()->fetch();
				
			if ($w) {
				$this->userId = $w['user_id'];
				$this->statusId = $w['status_id'];
				$this->status = isSet(self::$statuses[$this->statusId]) ? self::$statuses[$this->statusId] : 'N/A'; 
				$this->login = $w['login'];
				$this->firstName = $w['first_name'];
				$this->lastName = $w['last_name'];
				$this->email = $w['email'];
				$this->roleId = $w['role_id'];
				$this->hasPhonePaymentAccess = $w['hasPhonePaymentAccess'];
				
				try {
					$this->role = new FUserRole($this->roleId);
				} catch (Exception $e) {
				}
				
				$this->languagePk = $w['language_pk'];
				
				# when was last signed
				$this->lastSigned = FApplication::$db->select('dtc')
					->from(TBL_USERS_LOGS)
					->where('user_id=%i', $this->getId())->and('successful=%i', 1)
					->orderBy('dtc', dibi::DESC)
					->execute()->fetchSingle();
				
				$this->authorId = $w['author_id'];
				$this->editorId = $w['editor_id'];
				$this->dtc = $w['dtc'];
				$this->dtm = $w['dtm'];
				
			} else {
				//$this->userId = -1 * $this->getId();
				throw new Exception("User with id `$userId` doesn't exists!");
			}
		}
	}
	
	/**
	 * @param int $userId
	 * @return FUser
	 */
	static public function create($userId=0)
	{
		# cache
		self::$cacheKey = 'user_'.(int)$userId;
		if (StaticCache::read(self::$cacheKey)) {
			return StaticCache::read(self::$cacheKey);
		} else {
			$user = new self($userId);
			StaticCache::write(self::$cacheKey, $user);
			return $user;
		}
	}

	/**
	 * @return boolean
	 */
	public function getId() { return( $this->userId ); }

	/**
	 * @return boolean
	 */
	public function exists() { return( $this->userId > 0 ); }

	/**
	 * @return boolean
	 */
	public function isNew() { return( $this->userId == 0 ); }

	/**
	 * @return int $userId
	 */
	public function save()
	{
		$action = $this->getId() ? 'update' : 'insert';
		
		$values = Array();
		$values['status_id'] = $this->statusId;
		$values['login'] = $this->login;
		
		if (!empty($this->password)) {
			$values['password'] = md5($this->password);
		}
		
		$values['first_name'] = $this->firstName;
		$values['last_name'] = $this->lastName;
		$values['email'] = $this->email;
		$values['role_id'] = $this->roleId;
		$values['language_pk'] = $this->languagePk;
		$values['hasPhonePaymentAccess'] = $this->hasPhonePaymentAccess;
		
		if ($action == 'update') {
			$values['user_id'] = $this->getId();
			$values['dtm'] = new DibiDateTime();
			$values['editor_id'] = self::getSignedIn()->getId();
			FApplication::$db->update(TBL_USERS, $values)->where('user_id=%i', $this->getId())->execute();
			
		} else {
			$values['dtc'] = new DibiDateTime();
			$values['dtm'] = new DibiDateTime();
			$values['author_id'] = self::getSignedIn()->getId();
			$values['editor_id'] = self::getSignedIn()->getId();
			
			$lastId = FApplication::$db->insert(TBL_USERS, $values)->execute(dibi::IDENTIFIER);
			
			// new id for user
			$this->userId = $lastId;
		}
		
		StaticCache::remove(self::$cacheKey); // remove from cache
		return $this->userId;
	}

	/**
	 * @return void
	 */
	public function delete()
	{
		if ($this->exists()) {
			FApplication::$db->delete(TBL_USERS)->where('user_id=%i', $this->userId)->execute();
		}
	}
	
	/**
	 * @param string $username
	 * @param string $password
	 * @return object self
	 */
	static public function doAuthenticate($username, $password)
	{
		$user = FApplication::$db->select('`user_id`, `password`, `status_id`')->from(TBL_USERS)->where('login=%s', $username)->execute()->fetch();
		
        // username is not found
        if (empty($user)) {
        	throw new Exception(self::USERNAME_NOT_FOUND);
        }
        
        //  password doesn't match
        if ($user['password'] != md5($password)) {
            throw new Exception(self::PASSWORD_DOESNT_MATCH);
        }
        
		// account is not active
        if ($user['status_id'] != self::STATUS_ACTIVE) {
            throw new Exception(self::ACCOUNT_IS_NOT_ACTIVE);
        }
        
        return new self($user['user_id']);
	}
	
	/**
	 * @param int $userId
	 * @param string $nameSpace
	 * @return void
	 */
	static function signIn($userId)
	{
		$session = FApplication::$session->getNameSpace(self::$nameSpace);
		$session->userId = (int)$userId;
	}
	
	/**
	 * @param string $nameSpace
	 * @return object
	 */
	static public function isSignedIn()
	{
		$session = FApplication::$session->getNameSpace(self::$nameSpace);
		return $session->userId ? TRUE : FALSE;
	}
	
	
	/**
	 * @param string $nameSpace
	 * @return FUser
	 */
	static public function getSignedIn()
	{
		$session = FApplication::$session->getNameSpace(self::$nameSpace);
		return self::isSignedIn() ? new self($session->userId) : new self(0);
	}
	
	/**
	 * @param string $nameSpace
	 * @return void
	 */
	static function signOut()
	{
		if (self::isSignedIn()) {
			$session = FApplication::$session->getNameSpace(self::$nameSpace);
			unset($session->userId);
		}
	}
	
	
	static public function getRoles()
	{
		$roles = FApplication::$db->select('`user_role_id`, `parent_id`, `key`,title')->from(TBL_USERS_ROLES)->execute()->fetchAssoc('user_role_id');
		return $roles;
	}
	
	
	static public function getAll($returnAsObjects = TRUE)
	{
		$users = FApplication::$db->select('*')->from(TBL_USERS)->execute()->fetchAll();
		
		if ($returnAsObjects === TRUE) {
			$return = array();
			foreach ($users as $key=>$user) {
				$return[$user->user_id] = new self($user->user_id); 
			}
			return $return;
		} else {
			return $users;	
		}
	}
	
	
	/**
	 * Returns user by email address
	 * 
	 * @param string $login
	 * @return User
	 * @throws Exception
	 */
	static public function getCustomerByLogin($login)
	{
		$id = FApplication::$db->select('user_id')->from(TBL_USERS)->where('login=%s', $login)->execute()->fetchSingle();
		if ($id === FALSE) {
			throw new Exception("User with login `$login` doesn't exist!");
		}
		$user = new self($id);
		return $user;
	}
    
    static public function getUsersAllowedToTakePhonePayments()
    {
        $roles = FApplication::$db->select('`user_id`, `login`')
                ->from(TBL_USERS)
                ->where('hasPhonePaymentAccess=%d', 1)
                ->execute()
                ->fetch('user_role_id');
		
    }

	/**
	 * @return bool
	 */
	public static function isAdminSignedIn()
	{
		return self::isSignedIn() && self::getSignedIn()->role->key == 'admin';
	}
}
