<?php


/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    FSitemap.php 2011-05-17 divak@gmail.com
 */
class FSitemap extends Object
{
    /**
     * @param int $parentId
     * @param array $nodes
     * @return array
     */
    static public function get($parentId, array $nodes = NULL)
    {
        // --- first call ---
        if ($nodes === NULL) {
            $nodes = FMenu::getNodes();
        }

        $children = [];
        $sitemap = [];
        foreach ($nodes as $key => $val) {
            if (!$val['excludeFromSitemap'] && self::isOk2show($val) && ($parentId == $val['parent_id'] || $parentId == $key)) {
                $url = $val['front_controler'] ? self::getUrl($key, $val['friendly_url']) : NULL;
                if ($val['admin_controler'] == 'FolderAdminControler' && $val['front_controler'] == 'DefaultControler') {
                    $url = NULL;
                }

                if ($parentId == $key) {
                    $sitemap = [
                        'id' => $key,
                        'status_id' => $val['status_id'],
                        'title' => $val['title'],
                        'url' => $url,
                        'childs' => []//self::get($key, $nodes),
                    ];
                }


                if ($parentId == $val['parent_id']) {
                    $children[] = self::get($key, $nodes);
                }
            }

        }

        $sitemap['childs'] = $children;

        return $sitemap;
    }

    /**
     * @param array $sitemap
     * @return Html $ul
     */
    static public function getHtml(array $sitemap)
    {
        $ul = Html::el('ul');
        foreach ($sitemap as $item) {
            $li = $ul->create('li');

            // --- link ---
            if ($item['url']) {
                $li->create('a')->href($item['url'])->setText($item['title']);
                // --- just text ---
            } else {
                $strong = Html::el('strong')->setText($item['title']);
                $li->add($strong);
            }

            // --- childs ---
            if ($item['childs']) {
                $child = self::getHtml($item['childs']);
                $li->add($child);
            }
        }

        return $ul;
    }

    /**
     * @param string $nodeId
     * @param string $friendlyUrl
     * @return string
     */
    static public function getUrl($nodeId, $friendlyUrl)
    {
        if (FApplication::hasAllowedFriendlyUrl()) {
            $url = ($friendlyUrl != '/') ? $friendlyUrl : '';
        } else {
            $url = 'index.php?id=' . $nodeId . '&lng=' . FApplication::$router->getCurrentLanguage();
        }

        return URL_ROOT . $url;
    }


    /**
     * @param array $data
     * @return boolean
     */
    static private function isOk2show($data)
    {
        $data = (object) $data;

        // has been deleted - it's in bin
        if ($data->is_deleted) {
            return FALSE;

            // check status status
        } elseif ($data->status_id == FNode::STATUS_DRAFT || $data->status_id == FNode::STATUS_PENDING) {
            return FALSE;

            // showing time
        } elseif ($data->dt_show > date('Y-m-d G:i:s')) {
            return FALSE;

            // sensitive to date
        } elseif ($data->is_dt_sensitive && ($data->dt_start > date('Y-m-d G:i:s') || $data->dt_end < date('Y-m-d'))) {
            return FALSE;

        }

        return TRUE;
    }
}
