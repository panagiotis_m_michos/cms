<?php


include_once LIBS_DIR . '/3rdParty/Nette/Object.php';

class FTools extends Object
{
    // for scan dir
    const RETURN_ALL = 'all';
    const RETURN_DIRS = 'dirs';
    const RETURN_FILES = 'files';

    /**
     * @param mixed $params
     * @param string $url
     * @param string $sep
     * @return mixed|null|string
     */
    public static function addUrlParam($params = NULL, $url = NULL, $sep = '&amp;')
    {
        $newParams = array();
        // current url
        if (is_null($url)) {
            $url = FApplication::$httpRequest->uri->getAbsoluteUri();
        }
        $url = str_replace('&amp;', '&', $url);
        $uri = new Uri($url);

        // if $params is not an array do split and make array
        if (!is_Array($params)) {
            $param = explode('=', $params);
            // wrong params
            if (count($param) != 2) {
                return $url;
            }
            $params = Array($param[0] => $param[1]);

            // look for a=2 instead of 'a'=>2
        } else {
            foreach ($params as $key => $val) {
                $temp = explode('=', $val);
                if (count($temp) == 2) {
                    $params[$temp[0]] = $temp[1];
                    unset($params[$key]);
                }
            }
        }

        // old params
        if (!empty($uri->query)) {
            foreach (explode('&', $uri->query) as $key => $val) {
                $pom = explode('=', $val);
                // wrong params
                if (count($pom) != 2) {
                    continue;
                }
                $newParams[$pom[0]] = $pom[1];
            }
        }

        // add/remove new
        foreach ($params as $key => $val) {
            if ($val !== '') {
                $newParams[$key] = $val;

            } elseif (isSet($params[$key])) {
                unset($newParams[$key]);
            }
        }

        // put url together
        if (!empty($newParams)) {
            if (empty($uri->scheme)) {
                $url = $uri->path . '?' . http_build_query($newParams, NULL, $sep);
            } else {
                $url = $uri->scheme . '://' . $uri->host . $uri->path . '?' . http_build_query($newParams, NULL, $sep);
            }
        } else {
            if (empty($uri->scheme)) {
                $url = $uri->path;
            } else {
                $url = $uri->scheme . '://' . $uri->host . $uri->path;
            }
        }

        // add fragment
        if (!empty($uri->fragment)) $url .= '#' . $uri->fragment;
        return str_replace('&amp;', '&', $url);
    }


    /**
     * @param mixed $value
     */
    public static function pr($value)
    {
        $title = 'N/A';
        $debug = debug_backtrace();
        if (isset($debug[1]['file'], $debug[1]['line'])) {
            $title = sprintf('(%s) %s', $debug[1]['line'], $debug[1]['file']);
        }

        echo '<pre style="text-align: left; overflow: auto; border: 3px solid #F3FF8F; background: black; color: #F3FF8F; padding: 10px; margin: 10px;" title="' . $title . '">';
        print_r($value);
        echo '</pre>';
    }

    /**
     * Is adding session ID if is neccesary
     * @param string $url
     * @return string
     */
    public static function addUrlSid($url = NULL)
    {
        $url = !$url ? $_SERVER['REQUEST_URI'] : $url;
        return !isset($_SERVER['HTTP_COOKIE']) ? self::addUrlParam('sid=' . session_id(), $url) : self::addUrlParam('sid=', $url);
    }

    /**
     * @param $dir
     * @param string $return
     * @return array
     */
    public static function scanDir($dir, $return = 'all')
    {
        $scan['dirs'] = $scan['files'] = Array();

        # add / if it's not in path
        $dir = (substr($dir, -1) != '/') ? $dir . '/' : $dir;

        # go through dir
        if ($handle = opendir($dir)) {
            while (false !== ($file = readdir($handle))) {
                if ($file != '..' && $file != '.') {
                    if (is_file($dir . $file)) {
                        $scan['files'][] = $file;

                    } else {
                        $scan['dirs'][] = $file;
                    }
                }
            }

            closedir($handle);
        }

        # return dirs
        if ($return === self::RETURN_DIRS) { # return dirs
            $ret = $scan['dirs'];

        } elseif ($return === self::RETURN_FILES) { # return dirs
            $ret = $scan['files'];

        } else { # return all
            $ret = $scan;
        }

        return $ret;
    }

    /**
     * @param string $date
     * @param string $format
     * @return string
     */
    public static function dateFormat($date, $format)
    {
        return date($format, strtotime($date));
    }

    /**
     * @param int $count
     * @return string
     */
    public static function generPwd($count = 5)
    {
        $pass = '';
        $salt = "abchefghjkmnpqrstuvwxyz0123456789";
        srand((double)microtime() * 1000000);
        $i = 0;

        while ($i < $count) {
            $num = rand() % 33;
            $tmp = substr($salt, $num, 1);
            $pass = $pass . $tmp;
            $i++;
        }

        return $pass;
    }

    /**
     * @param \DateTime $date
     * @param string $format
     * @return string
     */
    public static function datetime(\DateTime $date = NULL, $format = DATE_DEFAULT)
    {
        if ($date instanceof DateTime) {
            return $date->format($format);
        }
    }
}
