<?php

final class FProperty extends Object
{
	/**
	 * @var array
	 */
    private static $cache = [];

    /**
     * @var int
     */
    public $propertyId;

    /**
     * @var int
     */
    public $nodeId;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $value;

    /**
     * @var string
     */
    private $authorId;

    /**
     * @var string
     */
    private $editorId;

    /**
     * @var string
     */
    private $dtc;

    /**
     * @var string
     */
    private $dtm;

    /**
     * @param int $id
     */
	public function __construct($id = 0)
	{
		// just for sure
		$this->propertyId = (int) $id;
		if ($this->propertyId) {
			$w = FApplication::$db->select('*')->from(TBL_PROPERTIES)->where('propertyId=%i', $this->getId())->execute()->fetch();
			if ($w) {
				$this->propertyId = $w['propertyId'];
				$this->nodeId = $w['nodeId'];
				$this->name = $w['name'];
				$this->value = stripslashes($w['value']);
				$this->authorId = $w['authorId'];
				$this->editorId = $w['editorId'];
				$this->dtc = $w['dtc'];
				$this->dtm = $w['dtm'];

			} else {
				$this->propertyId = -1 * $this->propertyId;
			}
		} else {
			$this->dtc = new DibiDateTime();
			$this->dtm = new DibiDateTime();
			$this->authorId = FUser::getSignedIn()->getId();
			$this->editorId = FUser::getSignedIn()->getId();
		}
	}

    /**
     * @return int
     */
    public function getId()
    {
        return $this->propertyId;
    }

    /**
     * @return bool
     */
    public function exists()
    {
        return $this->propertyId > 0;
    }

    /**
     * @return bool
     */
    public function isNew()
    {
        return $this->propertyId == 0;
    }

    /**
     * @return int|null
     */
	public function save()
	{
        $action = $this->propertyId ? 'update' : 'insert';
        $values = [
            'nodeId' => $this->nodeId,
            'name' => $this->name,
            'value' => $this->value,
            'editorId' => FUser::getSignedIn()->getId(),
            'dtm' => new DibiDateTime(),
        ];

        // update
		if ($action == 'update') {
			FApplication::$db->update(TBL_PROPERTIES, $values)->where('propertyId=%i', $this->getId())->execute();
		// insert
		} else {
			$values['authorId'] = FUser::getSignedIn()->getId();
			$values['dtc'] = new DibiDateTime();
			$lastId = FApplication::$db->insert(TBL_PROPERTIES, $values)->execute(dibi::IDENTIFIER);
			$this->propertyId = $lastId;
		}

        return $this->propertyId;
	}

	/**
	 * @return void
	 */
	public function delete()
	{
		FApplication::$db->delete(TBL_PROPERTIES)->where('propertyId=%i', $this->getId())->execute();
	}


    /**
     * @param string $name
     * @param int|null $nodeId
     * @return $this
     */
	public static function get($name, $nodeId = NULL)
	{
		if ($nodeId === NULL) $nodeId = FApplication::$nodeId;
		$id = FApplication::$db->select('propertyId')->from(TBL_PROPERTIES)->where('nodeId=%i', $nodeId)->and('name=%s', $name)->execute()->fetchSingle();
		// check if exists
		if ($id) {
			$props = new self($id);
		} else {
			$props = new self;
			$props->nodeId = $nodeId;
			$props->name = $name;
		}
		return $props;
	}

	/**
	 * @param string $value
	 * @return $this
	 */
	public function setValue($value)
	{
        $this->value = (string) $value;

		return $this;
	}

	/*
	public static function getValue($name)
	{
		$props = self::get($name);
		return $props->value;
	}
	*/

	/**
	 * @param int $nodeId
	 * @return array $props
	 */
	public static function getNodeProps($nodeId = NULL)
	{
        $nodeId = $nodeId ?: FApplication::$nodeId;

		return FApplication::$db->select('*')->from(TBL_PROPERTIES)->where('nodeId=%i', $nodeId)->execute()->fetchAll();
	}


	/**
	 * @param int $nodeId
	 * @return array $props
	 */
	public static function getNodePropsValues($nodeId = NULL)
	{
		$nodeId = $nodeId ?: FApplication::$nodeId;

		return FApplication::$db->select('name, value')->from(TBL_PROPERTIES)->where('nodeId=%i', $nodeId)->execute()->fetchPairs();
	}

	/**
     * @param string $key
	 * @param int $nodeId
	 * @return array
	 */
	public static function getNodePropsValue($key, $nodeId = NULL)
	{
        $nodeId = $nodeId ?: FApplication::$nodeId;
        // simple cache for node properties introduced in story "InZRTpGX", remove this comment if there is no problem after some time
        $props = isset(self::$cache[$nodeId]) ? self::$cache[$nodeId] : self::$cache[$nodeId] = self::getNodePropsValues($nodeId);

        return isset($props[$key]) ? $props[$key] : NULL;
	}
}
