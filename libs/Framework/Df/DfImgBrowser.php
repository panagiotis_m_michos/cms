<?php

/**
 +----------------------------------------------------------------------+
 | CMS2		   		                                                    |
 +----------------------------------------------------------------------+
 | Author: stan <diviak@gmail.com>		                                |
 +----------------------------------------------------------------------+

 FILE: lib.DfImg.php
 CREATED: 23/09/2008

 Working with CmsImg input and FckEditor browse server

 */

include_once(LOCAL_LIBS_PATH.'df/DfImg.php');

class DfImgBrowser extends DfImg
{
	protected $where = 'is_deleted=0 AND is_img=1';
	
	# default img node
	protected $galery_id = 9;
	
	/**
	 * Cooperate with lib filter
	 * @return string $limit
	 */
	protected function getWhere()
	{
		$where = ' WHERE 1';

		if(!is_Null($this->where))
		{
			$where .= ' AND '.$this->where;
		}
		
		# galery_id
		if(isSet($this->get['galery_id']) && $this->get['galery_id'])
		{
			$where .= ' AND node_id='.(int)$this->get['galery_id'];
			
			$this->galery_id = (int)$this->get['galery_id'];
		}
		else
		{
			$where .= ' AND node_id='.(int)$this->galery_id;
			 
			$this->galery_id = (int)$this->galery_id;
		}
		
		return($where);
	}
		
	/**
	 * @return string $html
	 */
	protected function getHeadingsHtml()
	{
		$headings = Html::el('thead')->startTag();
		$headings .= Html::el('tr')->startTag();

		$th = Html::el('th')
			->align('left')
			->colspan(4);
		
	    $form = Html::el('form')
	    	->action($_SERVER['REQUEST_URI'])
	    	->method('get');
	    
		$select = Html::el('select')
			->name('galery_id')
			->onchange('this.form.submit()');
		
			
		foreach(Menu2::getSelect(1) as $key=>$val)
		{
		   $option = Html::el('option')
				->value($key)
				->setText($val);
			
			if($this->galery_id == $key)
			{
				$option->selected('selected');
			}
				
			$select->add($option->render(0));
		}
		
		$form->add($select);
		
		# get params
		foreach($this->get as $key=>$val)
		{
			if($key == 'galery_id')
			{
				continue;
			}
			
			$form->add(
				Html::el('input')
					->type('hidden')
					->name($key)
					->value($val)
			);
		}
			
		$headings .= $th->add($form->render());
		
		$headings .= Html::el('tr')->endTag();
		$headings .= Html::el('thead')->endTag();
		
		return($headings);
	}
	
	/**
	 * @return string $html
	 */
	protected function getRowsHtml()
	{
		if(isSet($this->get['fck']))
		{
			$rows = $this->getFckRowsHtml();
		}
		else
		{
			$rows = $this->getCmsImgRowsHtml();
		}
		return($rows);
	}
	
	/**
	 * Working with CmsImg control
	 * @return string $rows
	 */
	protected function getCmsImgRowsHtml()
	{
		$poc=0;
		
		# check input name
		if(!isSet($this->get['input_name']))
		{
			trigger_error('DfImgBrowser::getCmsImgRowsHtml(): You have to specify "input_name" in get parameter!', E_USER_ERROR);
		}

		$rows = Html::el('tbody')->startTag();

		# no data to show - only notice
		if(!$this->hasAnyData())
		{
			$rows .= Html::el('tr')->startTag();
			
			$rows .= Html::el('td')
				->colspan(4)
				->add($this->text_no_data);

			$rows .= Html::el('tr')->endTag();
		}

		$rows .= Html::el('tr')->startTag();

		# foreach rows
		foreach($this->getRows() as $key=>$val)
		{
			$poc++;
			
			$img = new Img($val['id']);
			
			$text = Array();
			$text[] = Img::getImgTag($img->getId(),'s');
			
			# non active
			if($img->isActive())
			{
				$text[] = Html::el('b')->add(
					$img->name.'.'.$img->ext
				);
			}
			else
			{
				$text[] = Html::el('del')->add(
					$img->name.'.'.$img->ext
				);
			}
			
			$text[] = 'Ord: '.$img->ord.', Author: '.User::create($img->author_id)->login;
			$text[] = $img->getResolution().' ('.$img->getSize().')';
			$text[] = $img->getDtc();
			
			# add br
			foreach($text as $key2=>&$val2)
			{
				$val2 .= Html::el('br');
			}
			
			# td cell
			$td = Html::el('td')
				->align('center')
				->class($this->name)
				->id($img->getId())
				->onclick("
					parent.$('#".$this->get['input_name']."').val(".$img->getId().");
					parent.$('#".$this->get['input_name']."_span').html('".Img::getImgTag($img->getId(),'s')."');
					parent.$('#".$this->get['input_name']."_add').attr('style', 'display: none;');
					parent.$('#".$this->get['input_name']."_remove').attr('style', 'display: inline;');
					self.parent.tb_remove();
					return false;
				")
				->add(implode($text));
			
			if(!$img->isActive())
			{
				$td->style('color: silver;');	
			}
			
				
			$rows .= $td->render(0); 

			if($poc == 4 && $this->hasAnyData())
			{
				$rows .= Html::el('tr')->endTag();
				$rows .= Html::el('tr')->startTag();

				$poc=0;
			}

		}
		
		$rows .= Html::el('tr')->endTag();
		$rows .= Html::el('tbody')->endTag();
		$rows .= Html::el('table')->endTag();

		return($rows);
	}
	
	/**
	 * Working with FckEditor
	 * @return string $rows
	 */
	protected function getFckRowsHtml()
	{
		$poc=0;
		
		# check input name
		if(!isSet($this->get['input_name']))
		{
			trigger_error('DfImgBrowser::getFckRowsHtml(): You have to specify "input_name" in get parameter!', E_USER_ERROR);
		}
		
		# check alt name
		if(!isSet($this->get['alt_name']))
		{
			trigger_error('DfImgBrowser::getFckRowsHtml(): You have to specify "alt_name" in get parameter!', E_USER_ERROR);
		}

		$rows = Html::el('tbody')->startTag();

		# no data to show - only notice
		if(!$this->hasAnyData())
		{
			$rows .= Html::el('tr')->startTag();
			
			$rows .= Html::el('td')
				->colspan(4)
				->add($this->text_no_data);

			$rows .= Html::el('tr')->endTag();
		}

		$rows .= Html::el('tr')->startTag();

		# foreach rows
		foreach($this->getRows() as $key=>$val)
		{
			$poc++;
			
			$img = new Img($val['id']);
			
			$text = Array();
			$text[] = Img::getImgTag($img->getId(),'s');
			
			# non active
			if($img->isActive())
			{
				$text[] = Html::el('b')->add(
					$img->name.'.'.$img->ext
				);
			}
			else
			{
				$text[] = Html::el('del')->add(
					$img->name.'.'.$img->ext
				);
			}
			
			$text[] = 'Ord: '.$img->ord.', Author: '.User::create($img->author_id)->login;
			$text[] = $img->getResolution().' ('.$img->getSize().')';
			$text[] = $img->getDtc();
			
			# add br
			foreach($text as $key2=>&$val2)
			{
				$val2 .= Html::el('br');
			}
			
		     # td cell
			 $td = Html::el('td')
				->align('center')
				->class($this->name)
				->id($img->getId())
				->onclick("
					opener.document.getElementById('".$this->get['input_name']."').value = '".Img::getImgPathR($img->getId())."';
					opener.document.getElementById('".$this->get['alt_name']."').value = '".$img->name."';
					opener.UpdatePreview();
					self.close();
					return false;
				")
				->add(implode($text));
			
			if(!$img->isActive())
			{
				$td->style('color: silver;');	
			}
			
				
			$rows .= $td->render(0); 

			if($poc == 4 && $this->hasAnyData())
			{
				$rows .= Html::el('tr')->endTag();
				$rows .= Html::el('tr')->startTag();

				$poc=0;
			}

		}
		
		$rows .= Html::el('tr')->endTag();
		$rows .= Html::el('tbody')->endTag();
		$rows .= Html::el('table')->endTag();

		return($rows);
	}
}
?>
