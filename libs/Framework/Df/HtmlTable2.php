<?php

/*
 +----------------------------------------------------------------------+
 | CMS                                                                  |
 +----------------------------------------------------------------------+
 | AUTHOR: stan <diviak@gmail.com>   	                                |
 +----------------------------------------------------------------------+
 | FILE: libs/local_libs/lib.HtmlTable2.php                            	|
 | CREATED: 24/09/2008                                                  |
 +----------------------------------------------------------------------+
 */

class HtmlTable2
{
	# attributes
	# ==========

	protected $headings = Null;

	protected $rows = Null;

	protected $config = Array();

	protected $css_class = 'htmltable';

	protected $allow_row_even = TRUE;

	# METHODS
	# =======

	/**
	 * @param array $headings
	 * @return void
	 */
	public function setHeadings($headings)
	{
		if(!is_Array($headings))
		{
			trigger_error(get_class($this).'::setHeadings() - must be an Array!', E_USER_ERROR);
		}

		$this->headings = $headings;
	}

	/**
	 * @return array self::headings
	 */
	protected function getHeadings()
	{
		# if headings wasn't set - return all keys from array
		if(is_Null($this->headings))
		{
			reset($this->rows);
			return(array_keys(current($this->rows)));
		}
		
		return($this->headings);
	}

	/**
	 * @param array $rows
	 */
	public function setRows($rows)
	{
		if(!is_Array($rows))
		{
			trigger_error(get_class($this).'::setRows() - must be an Array!', E_USER_ERROR);
		}

		$this->rows = $rows;
	}

	/**
	 * @return array self::headings
	 */
	public function getRows()
	{
		return($this->rows);
	}

	/**
	 * @param array $config
	 * @return void
	 */
	public function setConfig($config)
	{
		$this->config = $config;
	}

	/**
	 * @param string $css
	 * @return void
	 */
	public function setCssClass($css)
	{
		$this->css_class = $css;
	}

	/**
	 * @return string
	 */
	protected function getCssClass()
	{
		return($this->css_class);
	}
	
	public function setAllowRowEven($even)
	{
		$this->allow_row_even = (bool)$even;
	}

	/**
	 * @param string $key
	 * @return string/void
	 */
	protected function getConfigWidth($key)
	{
		if(isSet($this->config[$key]['width']))
		{
			return(' width="'.$this->config[$key]['width'].'"');
		}
	}

	/**
	 * @param string $key
	 * @return string/void
	 */
	protected function getConfigAlign($key)
	{
		if(isSet($this->config[$key]['align']))
		{
			return(' class="'.$this->config[$key]['align'].'"');
		}
	}

	/**
	 * @param string $key
	 * @return string/void
	 */
	protected function getConfigName($key)
	{
		if(isSet($this->config[$key]['name']))
		{
			return($this->config[$key]['name']);
		}
		else
		{
			return($key);
		}
	}

	/**
	 * @param int $poc
	 * @return string
	 */
	protected function getRowEven($poc)
	{
		# is allowed draw even rows or is not even
		if(!$this->allow_row_even || $poc%2)
		{
			return;
		}

		return(' class="even"');
	}


	/**
	 * alingn/width for cells
	 * @return string $cols
	 */
	protected function getColHtml()
	{
		$cols = '';

		foreach($this->getHeadings() as $key=>$val)
		{
			$cols .= '<col'.$this->getConfigWidth($val).' />'."\n";
		}

		return($cols);
	}


	/**
	 * @return string $html
	 */
	protected function getHeadingsHtml()
	{
		$headings = '<thead>'."\n";
		
		$headings .= "\t".'<tr>'."\n";

		foreach($this->getHeadings() as $key=>$val)
		{
			$headings .= "\t\t".'<th'.$this->getConfigAlign($val).'>'.$this->getConfigName($val).'</th>'."\n";
		}

		$headings .= "\t".'</tr>'."\n";
		
		$headings .= '</thead>'."\n";

		return($headings);
	}

	/**
	 * @return string $html
	 */
	protected function getRowsHtml()
	{
		$poc=0;
		
		$rows = '<tbody>'."\n";

		# foreach rows
		foreach($this->getRows() as $key=>$val)
		{
			$poc++;
				
			$rows .= "\t".'<tr'.$this->getRowEven($poc).'>'."\n";
				
			# foreach headings
			foreach($this->getHeadings() as $key2=>$val2)
			{
				$rows .= "\t\t".'<td'.$this->getConfigAlign($val2).'>'.$val[$val2].'</td>'."\n";
			}
				
			$rows .= "\t".'</tr>'."\n";
		}
		
		$rows .= '<tbody>'."\n";

		return($rows);
	}


	/**
	 * @param boolean $return_at_least_header
	 * @return string
	 */
	public function getHtml($return_at_least_header=FALSE)
	{
		# rows wasn't set
		if(is_Null($this->rows))
		{
			trigger_error(get_class($this).'- $rows wasn\'t set!', E_USER_ERROR);
		}
		
		# empty data
		if(empty($this->rows) && !$return_at_least_header)
		{
			return(FALSE);
		}
		
		$html = '<table border="0" class="'.$this->getCssClass().'">'."\n";

		# cols
		$html .= $this->getColHtml();

		# html headings
		$html .= $this->getHeadingsHtml();

		# html rows
		$html .= $this->getRowsHtml();

		$html .= '</table>'."\n";

		return($html);
	}

	/**
	 * @return void
	 */
	public function printHtml()
	{
		echo $this->getHtml();
	}
	
	/**
	 * @param array $rows
	 * @return string
	 */
	private static function get($rows)
	{
		$table = HtmlTable();
		$table->setRows($rows);
		
		return($table->getHtml());
	}
}
?>