<?php

/**
 +----------------------------------------------------------------------+
 | CMS2      		                                                    |
 +----------------------------------------------------------------------+
 | Author: Stan <diviak@gmail.com>                                      |
 +----------------------------------------------------------------------+
 | FILE: libs/local_libs/df/lib.Df.php                                  |
 | CREATED: 04/08/08                                                    |
 +----------------------------------------------------------------------+
 */

class Df extends HtmlTable2
{
	protected $url = 'modal.php?id=#id#&tab=#tab#&handle_id=#handle_id#';

	protected $modal_size = Array(800,600);

	protected $buttons = Array(
				'insert' => Array(
					'text' => 'Insert',
					'icon' => 'insert2.gif',
					'modal'=>TRUE
	),
				'edit' => Array(
					'text' => 'Edit',
					'icon' => 'edit2.gif',
					'modal'=>TRUE
	),
				'delete' => Array(
					'text' => 'Delete',
					'icon' => 'delete2.gif',
					'modal_size' => Array(440,300),
					'modal'=>TRUE
	),
	);

	# buttons permisions
	protected $has_perm = Array('insert'=>TRUE,'edit'=>TRUE,'delete'=>TRUE);

	protected $sql_tbl = Null;

	protected $where = Null;
	
	public $group = NULL;
	
	public $limit = NULL;

	protected $primary_key = 'id';

	protected $order = Null;

	protected $can_be_sorted = TRUE;

	public $path_to_icons = '/admin/etc/imgs/icons/';

	protected $asc_icon = 'asc.gif';

	protected $desc_icon = 'desc.gif';

	protected $allow_order_imgs = TRUE;

	protected $allow_pager = TRUE;

	protected $pager = Null;

	protected $pager_ipp=10;

	protected $pager_display_options = Array(10=>10, 20=>20, 40=>40, 80=>80);

	protected $text_no_data = 'No data to show.';

	/**
	 * @param string $sql_tbl
	 * @return void
	 */
	public function __construct($sql_tbl=Null)
	{
		if(is_Null($sql_tbl))
		{
			trigger_error('__construct: You have to set SQL Table!',E_USER_ERROR);
		}
		
		$this->path_to_icons = ICONS_DIR_R;
		
		$this->get = FApplication::$get;

		$this->sql_tbl = $sql_tbl;
	}


	
	/* ============== GETTERS ============== */
	
	
	

	/**
	 * @return string $order
	 */
	protected function getOrder()
	{
		# order from URL
		if(isSet($this->get['ord']) && $this->canBeSorted())
		{
			$by = substr($this->get['ord'],0,-1);

			# check if ord is possible
			if (in_Array($by,$this->headings) || (!is_Null($this->order) && $by == $this->order['by'])) {
				if (substr($this->get['ord'],-1) == 'A') {
					$how = 'DESC';
					
				} else {
					$how = 'ASC';
				}
					
				return ' ORDER BY '.$by.' '.$how;
				
			} else {
				return;
			}
		} elseif (!is_Null($this->order)) { //order from settings
			return ' ORDER BY '.$this->order['by'] . ' ' . $this->order['how'];
			
		} else {
			return;
		}

	}

	/**
	 * @return array $data
	 */
	protected function getAllData()
	{
		$data = FApplication::$db->query('
			SELECT '.$this->primary_key.' 
			FROM '.$this->sql_tbl.
			$this->getWhere().
			$this->getGroup()
		)->fetchAll();
		
		return count($data);
	}

	/**
	 * @return
	 */
	protected function getPager()
	{
		if(is_Null($this->pager))
		{
			# how many results
			$result = $this->getAllData();

			$this->pager = new Pager2($result,$this->pager_ipp); //TODO
			$this->pager->setDisplayOptions($this->pager_display_options);

			$this->pager->isAllowedBeginText(FALSE);
			$this->pager->setHtmlFirst('<img src="'.$this->path_to_icons.'first.gif" alt="first" title="first" />');
			$this->pager->setHtmlPrevious('<img src="'.$this->path_to_icons.'prev.gif" alt="prev" title="prev" />');
			$this->pager->setHtmlNext('<img src="'.$this->path_to_icons.'next.gif" alt="next" title="next" />');
			$this->pager->setHtmlLast('<img src="'.$this->path_to_icons.'last.gif" alt="last" title="last" />');
		}
		
		return($this->pager);
	}

	/**
	 * Cooperate with lib pager
	 * @return mixed
	 */
	protected function getPagerLimit()
	{
		if (!is_Null($this->limit)) {
			return ' LIMIT '.$this->limit;
			
		} elseif(!$this->isAllowedPager()) {
			return NULL;
			
		} else {
			$pager = $this->getPager();
			return $pager->getSqlLimit();
		}
	}

	/**
	 * @return string $html
	 */
	protected function getPagerHtml()
	{
		if(!$this->isAllowedPager())
		{
			return;
		}

		$pager = $this->getPager();

		return($pager->getHtml());
	}


	/**
	 * Cooperate with lib filter
	 * @return string $limit
	 */
	protected function getWhere()
	{
		$where = ' WHERE 1';

		if(!is_Null($this->where))
		{
			$where .= ' AND '.$this->where;
		}


		return($where);
	}
	
	/**
	 * Cooperate with lib filter
	 * @return string
	 */
	protected function getGroup()
	{
		if (!is_Null($this->group)) {
			return ' GROUP BY '.$this->group;
		}
	}

	/**
	 * @return arr $data
	 */
	function getData()
	{
		$data = FApplication::$db->query('SELECT * FROM '.$this->sql_tbl.$this->getWhere().$this->getGroup().$this->getOrder().$this->getPagerLimit())->fetchAll();		
		return($data);
	}

	/**
	 * @param string $name
	 * @return string $img
	 */
	protected function getOrdImg($name)
	{
		$img = '';$how='';

		# are imgs allowed
		if(!$this->allow_order_imgs)
		{
			return;
		}

		# how is ording
		if(isSet($this->get['ord']) && $name == substr($this->get['ord'],0,-1))
		{
			$how = strtoupper(substr($this->get['ord'],-1));
		}
		elseif(!isSet($this->get['ord']) && !is_Null($this->order) && $name == $this->order['by'])
		{
			$how = strtoupper($this->order['how']);
		}


		# which image
		if($how == 'ASC' ||  $how == 'A')
		{
			$img = '<img src="'.$this->path_to_icons.$this->asc_icon.'" alt="ASC" title="ASC" />';
		}
		elseif($how == 'DESC' ||  $how == 'D')
		{
			$img = '<img  src="'.$this->path_to_icons.$this->desc_icon.'" alt="DESC" title="DESC" />';
		}

		return($img);
	}

	/**
	 * @param string $name
	 * @return string $url
	 */
	protected function getHeadingsUrl($name)
	{
		# if order is not allow
		if(!$this->canBeSorted())
		{
			return($this->getConfigName($name));
		}

		if(isSet($this->get['ord']) && substr($this->get['ord'],0,-1) == $name)
		{
			if(substr($this->get['ord'],-1) == 'D')
			{
				$ord = $name.'A';
			}
			else
			{
				$ord = $name.'D';
			}
		}
		else
		{
			$ord = $name.'D';
		}

		$name = '<a href="'.Tools::addUrlParam('ord='.$ord).'">'.$this->getConfigName($name).'</a>'.$this->getOrdImg($name);


		return($name);
	}

	/**
	 * alingn/width for cells
	 * @return string $cols
	 */
	protected function getColHtml()
	{
		$cols = '';

		foreach($this->getHeadings() as $key=>$val)
		{
			$cols .= '<col'.$this->getConfigWidth($val).' />'."\n";
		}

		# buttons
		if($this->hasAnyButton())
		{
			$cols .= '<col'.$this->getConfigWidth('buttons').' />'."\n";
		}

		return($cols);
	}

	/**
	 * @return string $html
	 */
	protected function getHeadingsHtml()
	{
		$headings = '<thead>'."\n";

		$headings .= "\t".'<tr>'."\n";

		foreach($this->getHeadings() as $key=>$val)
		{
			$headings .= "\t\t".'<th'.$this->getConfigAlign($val).'>'.$this->getHeadingsUrl($val).'</th>'."\n";
		}

		# if has any button or has right for buttons
		if($this->hasAnyButton())
		{
			if(!isSet($this->buttons['insert']))
			{
				$headings .= "\t".'<th '.$this->getConfigAlign('buttons').'></th>'."\n";
			}
			else
			{
				$headings .= "\t".'<th '.$this->getConfigAlign('buttons').'>'.$this->getButton('insert').'</th>'."\n";
			}
		}

		$headings .= "\t".'</tr>'."\n";

		$headings .= '</thead>'."\n";

		return($headings);
	}

	/**
	 * @return string $html
	 */
	protected function getRowsHtml()
	{
		$poc=0;

		$rows = Html::el('tbody')->startTag();

		# no data to show - only notice
		if(!$this->getRows())
		{
			$rows .= Html::el('tr')->startTag();
			
			$rows .= Html::el('td')
				->colspan(count($this->headings)+1)
				->setText($this->text_no_data);
			
			$rows .= Html::el('tr')->endTag();

			$rows .= Html::el('tbody')->endTag();
		}

		# foreach rows
		foreach($this->getRows() as $key=>$val)
		{
			$poc++;

			$rows .= "\t".'<tr'.$this->getRowEven($poc).'>'."\n";

			# foreach headings
			foreach($this->getHeadings() as $key2=>$val2)
			{
				$rows .= "\t\t".'<td'.$this->getConfigAlign($val2).'>'.$this->modifyData($val[$val2],$val2).'</td>'."\n";
			}

			# buttons
			if($this->hasAnyButton())
			{
				$rows .= "\t\t".'<td '.$this->getConfigAlign('buttons').'>'.$this->getButtons($val[$this->primary_key], $val).'</td>'."\n";
			}

			$rows .= "\t".'</tr>'."\n";
		}

		$rows .= '<tbody>'."\n";

		return($rows);
	}


	/**
	 * @returnt string $html
	 */
	public function getHtml($return_at_least_header=FALSE)
	{
		# insert links
		$this->start();

		# if have insert than show table heading and row no data
		if(isSet($this->buttons['insert']))
		{
			$html = parent::getHtml(TRUE);
		}
		else
		{
			$html = parent::getHtml(TRUE);
		}

		# no data info
		if(empty($html))
		{
			$html .= $this->text_no_data;
		}

		$html .= $this->getPagerHtml();

		return($html);
	}

	/**
	 * @param string $button
	 * @param int $handle_id
	 * @return string $url
	 */
	protected function getButton($button, $handle_id='none', $add_text=FALSE, $row = array())
	{
		$a = Html::el('a')->href($this->getButtonUrl($button, $handle_id, $row));
		
		# is modal
		if($this->canHaveModalWindow($button))
		{
			$a->class('thickbox');
		}
		
		# add text after icon
		if($add_text)
		{
			$text = Html::el('span')->add($this->getButtonText($button))->render();
			
			$a->add($this->getButtonIcon($button).$text);	
		}
		else
		{
			$a->add($this->getButtonIcon($button));
		}

		return($a->render());
	}
	
	/**
	 * @param string $button
	 * @param int $handle_id
	 * @param array $row
	 * @return string
	 */
	protected function getButtonUrl($button, $handle_id=0, $row = array())
	{
		$handle_id = (int)$handle_id;
		
		$params=Array();
		
		// URL is set for this button
		if (isSet($this->buttons[$button]['url'])) {
			$url = 	$this->buttons[$button]['url'];
			foreach ($row as $key=>$val) {
				$url = str_replace('%'.$key.'%', $val, $url);
			}
			
		// default URL + params
		} else {
			$url = $this->url;

			# params will be added
			$params['id'] = FApplication::$nodeId;
			$params['tab'] = $button;
			$params['handle_id'] = $handle_id;
		}

		# MODAL
		# add params
		if ($this->canHaveModalWindow($button)) {
			$params['height'] = $this->getModalsize($button,1);
			$params['width'] = $this->getModalsize($button,0);
			$params['KeepThis'] = true;
			$params['TB_iframe'] = true;
		}
		
		# add params to URL
		$url = FTools::addUrlParam($params, $url);
		$url = str_replace('%handle_id%', $handle_id, $url);
		
		// temporary
		$url = FApplication::$router->link("$button", "handle_id=$handle_id");
		return($url);
	}
	

	/**
	 * Check if img has text and has icon
	 * @param string $button
	 * @param int $handle_id
	 * @return string $url
	 */
	protected function getButtonText($button)
	{
		# if text is not set
		if(!isSet($this->buttons[$button]['text']))
		{
			trigger_error($button.' button: text has to be set!',E_USER_ERROR);
		}

   	    return($this->buttons[$button]['text'].'');
	}
	
	/**
	 * @param string $button
	 * @return 
	 */
	protected function getButtonIcon($button)
	{
		if(isSet($this->buttons[$button]['icon']))
		{
			$img = Html::el('img')
				->src($this->path_to_icons.$this->buttons[$button]['icon'])
				->alt($this->buttons[$button]['text'])
				->title($this->buttons[$button]['text']);
				
			return($img->render(0));
		}
		else
		{
			return NULL;
		}
	}

	/**
	 * @param strin $button
	 * @param int $coord
	 * @return int
	 */
	protected function getModalsize($button, $coord)
	{
		if(isSet($this->buttons[$button]['modal_size'][$coord]))
		{
			return($this->buttons[$button]['modal_size'][$coord]);
		}
		else
		{
			return($this->modal_size[$coord]);
		}
	}

	/**
	 * @param int $handle_id
	 * @return string $buttons
	 */
	protected function getButtons($handle_id=0, $row)
	{
		$buttons = '';

		foreach($this->buttons as $key=>$val)
		{
			if($key=='insert')
			{
				continue;
			}

			$buttons .= $this->getButton($key,$handle_id, FALSE, $row);
		}

		return($buttons);
	}

	/**
	 * @param string $val
	 * @param string $key
	 */
	protected function getConfigRef($val,$key)
	{
		# if $key has set reference
		if(isSet($this->config[$key]['ref']))
		{
			# if this reference is set
			if(isSet($this->config[$key]['ref'][$val]))
			{
				return($this->config[$key]['ref'][$val]);
			}
		}

		return($val);
	}

	/**
	 * @param string $text
	 */
	protected function getConfigFormat($val,$key)
	{
		# if $key has set reference
		if(isSet($this->config[$key]['format']))
		{
			switch($this->config[$key]['format'][0])
			{
				case 'date':
					$val = date($this->config[$key]['format'][1],strtotime($val));
					break;
			}
		}

		return($val);
	}


	/* ============== SETTERS ============== */

	/**
	 * @param strin $text
	 * @return void
	 */
	public function setTextNoData($text)
	{
		$this->text_no_data = $text;
	}

	/**
	 * @param boolean $allow
	 * @return void
	 */
	public function setIsAllowedPager($allow)
	{
		$this->allow_pager = (bool)$allow;
	}

	/**
	 * @param int $ipp
	 * @return void
	 */
	public function setPagerIpp($ipp)
	{
		$this->pager_ipp = (int)$ipp;
	}

	/**
	 * @param array $$options
	 * @return void
	 */
	public function setPagerDisplayOptions($options)
	{
		$this->pager_display_options = (array)$options;
	}

	/**
	 * @param bool $icon
	 * @return void
	 */
	public function setAscIcon($icon)
	{
		$this->asc_icon = $icon;
	}

	/**
	 * @param bool $icon
	 * @return void
	 */
	public function setDescIcon($icon)
	{
		$this->desc_icon = $icon;
	}

	/**
	 * @param string $path
	 * @return void
	 */
	protected function setPathToIcon($path)
	{
		$this->path_to_icon = $path;
	}

	/**
	 * @param array $buttons
	 * @return void
	 */
	public function setButtons($buttons)
	{
		$this->buttons = $buttons;
	}

	/**
	 * @param array $size
	 * @return void
	 */
	public function setModalSize($size)
	{
		$this->modal_size = $size;
	}

	/**
	 * @param array $url
	 * @return void
	 */
	public function setUrl($url)
	{
		$this->url = $url;
	}

	/**
	 * @param array $perms
	 * @return void
	 */
	public function setPerm($perms)
	{
		if(!is_Array($perms))
		{
			trigger_error('setHasPerm: $perms has to be array!',E_USER_ERROR);
		}

		$this->has_perm = $perms;
	}

	/**
	 * @param string $key
	 * @return void
	 */
	public function setPrimaryKey($key)
	{
		$this->primary_key = $key;
	}

	/**
	 * @param boolean $can_be_sorted
	 * @return void
	 */
	public function SetCanBeSorted($can_be_sorted)
	{
		$this->can_be_sorted = (bool)$can_be_sorted;
	}

	/**
	 * @param string $order
	 */
	public function setOrder($by,$how='ASC')
	{
		$this->order = Array('by'=>$by,'how'=>$how);
	}

	/**
	 * @param string $where
	 */
	public function setWhere($where)
	{
		$this->where = (string)$where;

		return($this);
	}
	
	/**
	 * @param string $group
	 */
	public function setGroup($group)
	{
		$this->group = (string)$group;

		return($this);
	}
	
	/**
	 * @param string $where
	 */
	public function addWhere($where, $operator='AND')
	{
		$this->where .= ' '.$operator.' '.$where;

		return($this);
	}

	/**
	 * @param bool $allow
	 * @return void
	 */
	public function allowOrdersImgs($allow)
	{
		$this->allow_order_imgs = (bool)$allow;
	}

	/* ============== QUESTIONS ============== */

	/**
	 * @return boolean
	 */
	protected function isAllowedPager()
	{
		if(!$this->allow_pager || !$this->hasAnyData())
		{
			return(FALSE);
		}
		else
		{
			return(TRUE);
		}
	}

	/**
	 * @return boolean
	 */
	protected function canBeSorted()
	{
		if(!$this->can_be_sorted || !$this->hasAnyData())
		{
			return(FALSE);
		}
		else
		{
			return(TRUE);
		}
	}

	/**
	 * @return boolean
	 */
	public function hasAnyData()
	{
		$data = $this->getAllData();

		if(empty($data))
		{
			return(FALSE);
		}
		else
		{
			return(TRUE);
		}
	}

	/**
	 * @param string $button
	 * @return boolean
	 */
	protected function hasPerm($button)
	{
		if(isSet($this->has_perm[$button]) && (bool)$this->has_perm[$button])
		{
			return(TRUE);
		}

		return(FALSE);
	}

	/**
	 * @param string $button
	 * @return boolean
	 */
	protected function canHaveModalWindow($button)
	{
		# if set modal and it's true or not set
		if(isSet($this->buttons[$button]['modal']) && $this->buttons[$button]['modal'] == TRUE || !isSet($this->buttons[$button]['modal']))
		{
			return(TRUE);
		}

		return(FALSE);
	}
	
	/**
	 * @return boolean
	 */
	protected function hasAnyButton()
	{
		# no buttons added
		if(empty($this->buttons))
		{
			return(FALSE);
		}

		# or has permission
		foreach($this->has_perm as $key=>$val)
		{
			# if its TRUE
			if((bool)$val)
			{
				return(TRUE);
			}
		}

		return(FALSE);
	}

	/* ============== ADDING ============== */

	/**
	 * @param string $key
	 * @param array $val
	 * @return void
	 */
	public function addButton($key,$val)
	{
		# button
		$this->buttons[$key] = $val;

		# perm
		$this->has_perm[$key] = TRUE;
	}

	/**
	 * @param string $button
	 * @param bool $perm
	 * @return void
	 */
	public function addPerm($button,$perm)
	{
		$this->has_perm[$button] = (bool)$perm;
	}

	/* ============== OTHERS ============== */


	/**
	 * @param string $val - text of value
	 * @param string $key
	 * @return  string $data
	 */
	protected function modifyData($val,$key)
	{
		# reference data
		$val = $this->getConfigRef($val,$key);

		# format data
		$val = $this->getConfigFormat($val,$key);

		return($val);
	}

	/**
	 * set source of data
	 * @return void
	 */
	public function start()
	{
		# check if headers have been set
		if(is_Null($this->headings))
		{
			trigger_error('Headings has to be set!', E_USER_ERROR);
		}

		$data =  $this->getData();

		$this->setRows($data);
	}
	
	public function __toString()
	{
		return $this->getHtml();
	}
}
?>