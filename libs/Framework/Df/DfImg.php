<?php

/**
 +----------------------------------------------------------------------+
 | CMS2		   		                                                    |
 +----------------------------------------------------------------------+
 | Author: stan <diviak@gmail.com>		                                |
 +----------------------------------------------------------------------+

 FILE: lib.DfImg.php
 CREATED: 23/09/2008
 */

class DfImg extends Df
{
	protected $pager_display_options = Array(16=>16, 32=>32, 48=>48, 72=>72);

	protected $pager_ipp=16;

	protected $css_class='htmlgrid';
	
	protected $name = 'dfimg';

	protected $buttons = Array(
				'insert' => Array(
					'text' => 'Insert image',
					'icon' => 'insert2.gif',
					'modal'=>TRUE
	),
				'show' => Array(
					'text' => 'Show',
					'icon' => 'show.gif',
					'modal'=>TRUE
	),
				'edit' => Array(
					'text' => 'Edit',
					'icon' => 'edit2.gif',
					'modal'=>TRUE
	),
				'delete' => Array(
					'text' => 'Delete',
					'icon' => 'delete2.gif',
					'modal_size' => Array(440,300),
					'modal'=>TRUE
	),
	);


	/**
	 * alingn/width for cells
	 * @return string $cols
	 */
	protected function getColHtml()
	{
		$cols = '';

		for($i=0; $i<4; $i++)
		{
			$cols .= Html::el('col')->width(190)->render();
		}

		return($cols);
	}

	/**
	 * @return string $html
	 */
	protected function getHeadingsHtml()
	{
		$headings = Html::el('thead')->startTag();
		$headings .= Html::el('tr')->startTag();

		# if has any button or has right for buttons
		if($this->hasAnyButton())
		{
			$th = Html::el('th')
				->align('left')
				->colspan(4);
				
			if($this->hasPerm('insert'))
			{
				$th->add($this->getButton('insert',0,TRUE));
			}
			else
			{
				$th->setText('Grid of images');
			}
			
			$headings .= $th->render();
		}
		
		$headings .= Html::el('tr')->endTag();
		$headings .= Html::el('thead')->endTag();
		
		return($headings);
	}


	/**
	 * @return string $html
	 */
	protected function getRowsHtml()
	{
		$poc=0;

		$rows = Html::el('tbody')->startTag();

		# no data to show - only notice
		if(!$this->hasAnyData())
		{
			$rows .= Html::el('tr')->startTag();
			
			$rows .= Html::el('td')
				->colspan(4)
				->add($this->text_no_data);

			$rows .= Html::el('tr')->endTag();
		}

		$rows .= Html::el('tr')->startTag();

		# foreach rows
		foreach($this->getRows() as $key=>$val)
		{
			$poc++;
			
			$img = new Img($val['id']);
			
			$text = Array();
			$text[] = Img::getImgTag($img->getId(),'s');
			
			# non active
			if($img->isActive())
			{
				$text[] = Html::el('b')->setText(
					$img->name.'.'.$img->ext
				)->render();
			}
			else
			{
				$text[] = Html::el('del')->setText(
					$img->name.'.'.$img->ext
				)->render();
			}
			
			$text[] = 'Ord: '.$img->ord.', Author: '.User::create($img->author_id)->login;
			$text[] = $img->getResolution().' ('.$img->getSize().')';
			$text[] = $img->getDtc();
			
			# add br
			foreach($text as $key2=>&$val2)
			{
				$val2 .= Html::el('br')->render();
			}
			
			# td cell
			$td = Html::el('td')
				->align('center')
				->class($this->name)
				->id($img->getId())
				->add(implode($text));
			
			if(!$img->isActive())
			{
				$td->style('color: silver;');	
			}
			
				
			$rows .= $td->render(0); 

			if($poc == 4 && $this->hasAnyData())
			{
				$rows .= Html::el('tr')->endTag();
				$rows .= Html::el('tr')->startTag();

				$poc=0;
			}

		}
		
		$rows .= Html::el('tr')->endTag();
		$rows .= Html::el('tbody')->endTag();
		$rows .= Html::el('table')->endTag();
		
		# context menu
		$rows .= $this->getContextMenu();

		return($rows);
	}

	/**
	 * @return string $html
	 */
	protected function getContextMenu()
	{
		if(!$this->hasAnyData())
		{
			return;
		}
		
		# HTML menu
		$ul = Html::el('ul');
		
		foreach($this->buttons as $key=>$val)
		{
			if($key == 'insert')
			{
				continue;
			}
			
			$img = Html::el('img')
				->src($this->path_to_icons.$val['icon'])
				->alt($val['text'])
				->title($val['text'])
				->render();
			
			$text = $val['text'];
			
			$ul->create('li')
				->id($key)
				->add($img.$text)
				->render();
		}
		
		$menu = Html::el('div')
			->class('contextMenu')
			->id('ImgsGrid')
			->add($ul)
			->render(0);
		
			
		# handler script
		$menu .= Html::el('script')
			->type('text/javascript')
			->setText("
		     /**
		      * @param string url
		      * @param string id
		      * @return string url
		      */
		     function handleUrl(url,id)
		     {
		     	return(url.replace(/#id#/,id));
		     }
		     
		     ".$this->getImgsJsPath()."
		     
		     $('.".$this->name."').contextMenu('ImgsGrid', {
		      bindings: {
		      	'show': function(t) {
		      		tb_show('',imgPath[t.id])
		      	},
		      	".$this->getJsActions()."
		      }
		    })"
		)
		->render();
		
		return($menu);
	}
	
	/**
	 * @return string $html
	 */
	protected function getJsActions()
	{
		$ret='';
		
		foreach($this->buttons as $key=>$val)
		{
			if($key == 'insert' || $key == 'show')
			{
				continue;
			}
			
			$ret .= "'".$key."': function(t) { tb_show('', handleUrl('".$this->getButtonUrl($key,'#id#')."', t.id))},";
		}
		
		
		
		return($ret);
	}
	
	/**
	 * @return string
	 */
	protected function getImgsJsPath()
	{
		if(!$this->hasAnyData())
		{
			return;
		}
		
		$js = 'var imgPath = new Array();'."\n";
		
		foreach($this->getData() as $key=>$val)
		{
			$js .= 'imgPath['.$val['id'].'] = "'.Img::getImgPath($val['id']).'"'."\n";
		}
		
		return($js);
	}
}
?>
