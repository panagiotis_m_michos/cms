<?php

/**
 +----------------------------------------------------------------------+
 | CMS2		   		                                                    |
 +----------------------------------------------------------------------+
 | Author: stan <diviak@gmail.com>		                                |
 +----------------------------------------------------------------------+

 FILE: lib.DfImg.php
 CREATED: 23/09/2008

 Working with CmsFile control
 */

class DfFileBrowser extends Df
{
	protected $buttons = Array();
	
	protected $can_be_sorted = FALSE;
	
	protected $where = 'is_deleted=0 AND is_img=0';
	
	protected $galery_id = 10;
	
	/**
	 * Cooperate with lib filter
	 * @return string $limit
	 */
	protected function getWhere()
	{
		$where = ' WHERE 1';

		if(!is_Null($this->where))
		{
			$where .= ' AND '.$this->where;
		}
		
		# galery_id
		if(isSet($this->get['galery_id']) && $this->get['galery_id'])
		{
			$where .= ' AND node_id='.(int)$this->get['galery_id'];
			
			$this->galery_id = (int)$this->get['galery_id'];
		}
		else
		{
			$where .= ' AND node_id='.(int)$this->galery_id;
			 
			$this->galery_id = (int)$this->galery_id;
		}
		
		return($where);
	}
	
	/**
	 * @return string $html
	 */
	protected function getHeadingsHtml()
	{
		$headings = Html::el('thead')->startTag();
		$headings .= Html::el('tr')->startTag();

		$th = Html::el('th')
			->align('left')
			->colspan(count($this->headings));
		
	    $form = Html::el('form')
	    	->action($_SERVER['REQUEST_URI'])
	    	->method('get');
	    
		$select = Html::el('select')
			->name('galery_id')
			->onchange('this.form.submit()');
		
			
		foreach(Menu2::getSelect(1) as $key=>$val)
		{
		   $option = Html::el('option')
				->value($key)
				->setText($val);
			
			if($this->galery_id == $key)
			{
				$option->selected('selected');
			}
				
			$select->add($option->render(0));
		}
		
		$form->add($select);
		
		# get params
		foreach($this->get as $key=>$val)
		{
			if($key == 'galery_id')
			{
				continue;
			}
			
			$form->add(
				Html::el('input')
					->type('hidden')
					->name($key)
					->value($val)
			);
		}
			
		$headings .= $th->add($form->render());
		
		$headings .= Html::el('tr')->endTag();
		$headings .= Html::el('thead')->endTag();
		
		# heading from parent
		$headings .= parent::getHeadingsHtml();
		
		return($headings);
	}
	
	/**
	 * @return string $html
	 */
	protected function getRowsHtml()
	{
		if(isSet($this->get['fck']))
		{
			$rows = $this->getFckRowsHtml();
		}
		else
		{
			$rows = $this->getCmsImgRowsHtml();
		}
		return($rows);
	}
	
	
	/**
	 * @return string $html
	 */
	protected function getCmsImgRowsHtml()
	{
		# check input name
		if(!isSet($this->get['input_name']))
		{
			trigger_error('DfFileBrowser::getCmsImgRowsHtml(): You have to specify "input_name" in get parameter!', E_USER_ERROR);
		}
		
		$poc=0;

		$rows = Html::el('tbody')->startTag();

		# no data to show - only notice
		if(!$this->getRows())
		{
			$rows .= Html::el('tr')->startTag();
			
			$rows .= Html::el('td')
				->colspan(count($this->headings))
				->setText($this->text_no_data);
							
			$rows .= Html::el('tr')->endTag();

			$rows .= Html::el('tbody')->endTag();
		}
		
		# foreach rows
		foreach($this->getRows() as $key=>$val)
		{
			$poc++;
			
			$file = new File2($val['id']);

			$rows .= "\t".'<tr'.$this->getRowEven($poc).'>'."\n";
			
			
			$a = Html::el('a')
				->href(FILES_PATH_R.$file->getFileName())
				->setText($file->getFileName());
			
			
			$tr = Html::el('tr')
				->onclick("
					parent.$('#".$this->get['input_name']."').val(".$file->getId().");
					parent.$('#".$this->get['input_name']."_span').html('".$a->render()."');
					parent.$('#".$this->get['input_name']."_add').attr('style', 'display: none;');
					parent.$('#".$this->get['input_name']."_remove').attr('style', 'display: inline;');
					self.parent.tb_remove();
					return false;
				");
				
			if($this->rowEven($poc))
			{
				$tr->class('even');
			}
				
			$rows .= $tr->startTag();
			
			# foreach headings
			foreach($this->getHeadings() as $key2=>$val2)
			{
				$rows .= "\t\t".'<td'.$this->getConfigAlign($val2).'>'.$this->modifyData($val[$val2],$val2).'</td>'."\n";
			}

			# buttons
			if($this->hasAnyButton())
			{
				$rows .= "\t\t".'<td '.$this->getConfigAlign('buttons').'>'.$this->getButtons($val[$this->primary_key]).'</td>'."\n";
			}

			$rows .= "\t".'</tr>'."\n";
			
		}

		$rows .= '<tbody>'."\n";
		

		return($rows);
	}
	
	/**
	 * @return string $html
	 */
	protected function getFckRowsHtml()
	{
		# check input name
		if(!isSet($this->get['input_name']))
		{
			trigger_error('DfFileBrowser::getCmsImgRowsHtml(): You have to specify "input_name" in get parameter!', E_USER_ERROR);
		}
		
		$poc=0;

		$rows = Html::el('tbody')->startTag();

		# no data to show - only notice
		if(!$this->getRows())
		{
			$rows .= Html::el('tr')->startTag();
			
			$rows .= Html::el('td')
				->colspan(count($this->headings))
				->setText($this->text_no_data);
							
			$rows .= Html::el('tr')->endTag();

			$rows .= Html::el('tbody')->endTag();
		}

		# foreach rows
		foreach($this->getRows() as $key=>$val)
		{
			$poc++;
			
			$file = new File2($val['id']);

			$rows .= "\t".'<tr'.$this->getRowEven($poc).'>'."\n";
			
			
			$a = Html::el('a')
				->href(FILES_PATH_R.$file->getFileName())
				->setText($file->getFileName());
			
			
			$tr = Html::el('tr')
				->onclick("
					opener.document.getElementById('".$this->get['input_name']."').value = '".FILES_PATH_R.$file->getFileName()."';
					opener.document.getElementById('cmbLinkProtocol').value = '';
					opener.BrowseServer();
					self.close();
					return false;
				");
				
			if($this->rowEven($poc))
			{
				$tr->class('even');
			}
				
			$rows .= $tr->startTag();
			
			# foreach headings
			foreach($this->getHeadings() as $key2=>$val2)
			{
				$rows .= "\t\t".'<td'.$this->getConfigAlign($val2).'>'.$this->modifyData($val[$val2],$val2).'</td>'."\n";
			}

			# buttons
			if($this->hasAnyButton())
			{
				$rows .= "\t\t".'<td '.$this->getConfigAlign('buttons').'>'.$this->getButtons($val[$this->primary_key]).'</td>'."\n";
			}

			$rows .= "\t".'</tr>'."\n";
		}

		$rows .= '<tbody>'."\n";

		return($rows);
	}
	
	/**
	 * @param int $poc
	 * @return string
	 */
	protected function RowEven($poc)
	{
		# is allowed draw even rows or is not even
		if(!$this->allow_row_even || $poc%2)
		{
			return(FALSE);
		}

		return(TRUE);
	}
}
?>
