<?php

/**
 +----------------------------------------------------------------------+
 | CMS2		   		                                                    |
 +----------------------------------------------------------------------+
 | Author: stan <diviak@gmail.com>		                                |
 +----------------------------------------------------------------------+

 FILE: lib.DfImg.php
 CREATED: 23/09/2008
 */

class DfFlashBrowser extends DfFileBrowser
{
	/**
	 * @return string $html
	 */
	protected function getFckRowsHtml()
	{
		# check input name
		if(!isSet($this->get['input_name']))
		{
			trigger_error('DfFileBrowser::getCmsImgRowsHtml(): You have to specify "input_name" in get parameter!', E_USER_ERROR);
		}
		
		$poc=0;

		$rows = Html::el('tbody')->startTag();

		# no data to show - only notice
		if(!$this->getRows())
		{
			$rows .= Html::el('tr')->startTag();
			
			$rows .= Html::el('td')
				->colspan(count($this->headings))
				->setText($this->text_no_data);
							
			$rows .= Html::el('tr')->endTag();

			$rows .= Html::el('tbody')->endTag();
		}

		# foreach rows
		foreach($this->getRows() as $key=>$val)
		{
			$poc++;
			
			$file = new File2($val['id']);

			$rows .= "\t".'<tr'.$this->getRowEven($poc).'>'."\n";
			
			
			$a = Html::el('a')
				->href(FILES_PATH_R.$file->getFileName())
				->setText($file->getFileName());
			
			
			$tr = Html::el('tr')
				->onclick("
					opener.document.getElementById('".$this->get['input_name']."').value = '".FILES_PATH_R.$file->getFileName()."';
					opener.BrowseServer();
					self.close();
					return false;
				");
				
			if($this->rowEven($poc))
			{
				$tr->class('even');
			}
				
			$rows = $tr->startTag();
			
			# foreach headings
			foreach($this->getHeadings() as $key2=>$val2)
			{
				$rows .= "\t\t".'<td'.$this->getConfigAlign($val2).'>'.$this->modifyData($val[$val2],$val2).'</td>'."\n";
			}

			# buttons
			if($this->hasAnyButton())
			{
				$rows .= "\t\t".'<td '.$this->getConfigAlign('buttons').'>'.$this->getButtons($val[$this->primary_key]).'</td>'."\n";
			}

			$rows .= "\t".'</tr>'."\n";
		}

		$rows .= '<tbody>'."\n";

		return($rows);
	}
}
?>
