<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   CMS
 * @package    Cms\FImage
 * @version    File.php 2009-10-22 divak@gmail.com
 */

/*namespace Cms\FImage;*/


/**
 * Work with uploaded files
 * 
 * @author     Stanislav Bazik
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @package    Cms\FImage
 */


class FImage extends FFile
{
	/** @var string */
	public static $storageDir = IMAGES_DIR;
	
	public static $storageDirR = IMAGES_DIR_R;
	
	/** @var boolean */
	protected $isImage = TRUE;
	
	/** @var int */
	public $width;
	
	/** @var int */
	public $height;
	
	
	/**
	 * @param int $fileId
	 * @return void
	 * @throws Exception
	 */
	public function __construct($fileId = 0)
	{
		$this->fileId = (int)$fileId;
		if ($this->{$this->primaryKey}) {
			$w = dibi::select('*')->from($this->table)->where('isImg IS NOT NULL')->and('fileId=%i', $this->{$this->primaryKey})->execute()->fetch(); 
			if (!empty($w)) {
				$this->{$this->primaryKey} = $w['fileId'];
				$this->isActive = $w['isActive'];
				$this->isDeleted = $w['isDeleted'];
				$this->ord = $w['ord'];
				$this->nodeId = $w['nodeId'];
				$this->name = $w['name'];
				$this->nameOld = $w['name'];
				$this->description = $w['description'];
				$this->ext = $w['ext'];
				$this->size = $w['size'];
				$this->width = $w['width'];
				$this->height = $w['height'];	
				$this->dtc = $w['dtc'];
				$this->dtm = $w['dtm'];
				$this->authorId = $w['authorId'];
				$this->editorId = $w['editorId'];
				
				// check if main image exists
				if (!is_file($this->getFilePath())) {
					throw new Exception("Image `{$this->getFileName()}` not found!");
				}
				
			} else {
				throw new Exception("File with id `$fileId` not found!");
			}
		}
	}
	
	/**
	 * @param int $fileId
	 * @return object
	 */
	static public function create($fileId = 0)
	{
		return new self($fileId);
	}
	
	/*
	 * Provides create/update of image
	 * 
	 * @return int
	 * @throws Exception
	 */
	public function save()
	{
		// is dir writeable
		if (!is_dir(self::getStorageDir()) || !is_writable(self::getStorageDir())) {
			throw new Exception('Files directory "' . self::getStorageDir() . '" is not writable.');
		}
		
		// pathinfo
		if (!function_exists('pathinfo')) {
			throw new Exception('Function `pathinfo()` could not be found!');
		}
		
		// check is there is any file
		if ($this->isNew() === TRUE && $this->isUploaded() === FALSE && $this->isLocalFile() === FALSE) {
			throw new Exception('No file has been set!');
		}
		
		// info about file
		$fileInfo = $this->getFileInfo();
		$this->size = $fileInfo->size;
		$this->ext = $fileInfo->ext;
		$this->width = $fileInfo->width;
		$this->height = $fileInfo->height;
		
		// data to save
		$data = array();
		$data['isActive'] = $this->isActive;
		$data['isDeleted'] = $this->isDeleted;
		$data['isImg'] = $this->isImage;
		$data['ord'] = $this->ord ? $this->ord : $this->getNextOrd();
		$data['nodeId'] = $this->nodeId;
		$data['name'] = $fileInfo->name;
		$data['description'] = $this->description;
		$data['ext'] = $this->ext;
		$data['size'] = $this->size;
		$data['width'] = $this->width;
		$data['height'] = $this->height;
		$data['editorId'] = FUser::getSignedIn()->getId();
		$data['dtm'] = new DibiDateTime();
		
		// insert
		if ($this->isNew()) {
			$data['dtc'] =  new DibiDateTime();
			$data['authorId'] = FUser::getSignedIn()->getId();
			$this->{$this->primaryKey} = dibi::insert($this->table, $data)->execute(dibi::IDENTIFIER);
		} else {
			dibi::update($this->table, $data)->where('fileId=%i', $this->{$this->primaryKey})->execute();
		}
		
		// move_uploaded_file or rename
		if ($this->isUploaded()) {
			@move_uploaded_file($this->uploadedFile->tmp_name, $this->getFilePath());
			$this->deleteThumbs();
		// local file
		} elseif ($this->isLocalFile()) {
			@copy($fileInfo->source, $this->getFilePath());
			$this->deleteThumbs();
		} 
		
		return $this->{$this->primaryKey};
	}
	
	/**
	 * Provides deleting image and his thumbnail
	 * @return void
	 */
	public function delete()
	{
		//if its only first delete
		if($this->isDeleted() === FALSE) {
			$this->isDeleted = 1;
			$this->save();
		} else {
			// delete all thumbs
			$this->deleteThumbs();
			
			// delete main image
			$filePath = $this->getFilePath();
			if (is_file($filePath)) @unlink($filePath);

			// delete file from db
			dibi::delete($this->table)->where($this->primaryKey.'=%i', $this->getId())->execute();
		}
	}
	
	/**
	 * Provides remove all thumbs
	 * @return void
	 */
	private function deleteThumbs()
	{
		$dirs = FTools::scanDir($this->getStorageDir(), FTools::RETURN_DIRS);
		foreach ($dirs as $key => $dir) {
			$filePath = $this->getStorageDir() . DIRECTORY_SEPARATOR .$dir . DIRECTORY_SEPARATOR. $this->getFileName();
			if(is_file($filePath)) @unlink($filePath);
		}
	}
	
	/**
	 * Returns max ord for node
	 * @return int $maxOrd
	 */
	protected function getNextOrd()
	{
		$maxOrd = dibi::select('MAX(ord)')->from($this->table)->where('isImg IS NOT NULL')->and('nodeId=%i', $this->nodeId)->execute()->fetchSingle();
		$nextord = (int)$maxOrd + 10;
		return $nextord;
	}
	
	
	/**
	 * Returns info about file 
	 * 
	 * @param array $file
	 * @return array $ret
	 */
	protected function getFileInfo()
	{
		$ret = array();
		
		// edit
		if ($this->isNew() === FALSE && $this->isUploaded() === FALSE && $this->isLocalFile() === FALSE) {
			
			// new name of file
			$ret['name'] = $this->name;
			$ret['ext'] = $this->ext; 
			$ret['size'] = $this->size;
			$ret['source'] = $this->getFilePath();
			$ret['width'] = $this->width;
			$ret['height'] = $this->height;
			
		} else {
			// uploaded
			if ($this->isUploaded()) {
				$info = pathinfo($this->uploadedFile->name);
				$ret['size'] = $this->uploadedFile->size;
				$ret['source'] = $this->uploadedFile->tmp_name;
				$ret['ext'] = strtolower($info['extension']);
				$ret['name'] = !empty($this->name) ? $this->name : $info['filename'];
				
				$imageSize = @getimagesize($this->uploadedFile->tmp_name);
				$ret['width'] = isset($imageSize[0]) ? $imageSize[0] : 0;
				$ret['height'] = isset($imageSize[1]) ? $imageSize[1] : 0;
				
				
			// file e.g. from import
			} elseif ($this->isLocalFile()) { 
				$info = pathinfo($this->localFile);
				$ret['size'] = filesize($this->localFile);
				$ret['source'] = $this->localFile;
				$ret['ext'] = strtolower($info['extension']);
				$ret['name'] = !empty($this->name) ? $this->name : $info['filename'];
				
				$imageSize = @getimagesize($this->localFile);
				$ret['width'] = isset($imageSize[0]) ? $imageSize[0] : 0;
				$ret['height'] = isset($imageSize[1]) ? $imageSize[1] : 0;
			}
		}
		return (object)$ret;
	}
	

	/**
	 * Returns file name
	 * 
	 * @return string
	 */
	public function getFileName()
	{
		return 'img' . $this->getId() . '.' . $this->ext;
	}
	
	
	/**
	 * Returns file path based on $thumb
	 * 
	 * @param boolean $relative
	 * @return string
	 */
	public function getFilePath($relative = FALSE, $thumb = NULL)
	{
		// create thumb
		$fileName = self::getStorageDir(FALSE, $thumb) . DIRECTORY_SEPARATOR . $this->getFileName();
		if ($thumb !== NULL && is_file($fileName) == FALSE) { 
			$original = self::getStorageDir() . DIRECTORY_SEPARATOR . $this->getFileName();
			$this->createThumbnail($original, $thumb);
		}
		$fileName = self::getStorageDir($relative, $thumb) . DIRECTORY_SEPARATOR . $this->getFileName();
		return $fileName;
	}
	
	/**
	 * Provides create thumbnail
	 * 
	 * @param string $fileName
	 * @param string $thumb
	 * @return void
	 */
	public function createThumbnail($fileName, $thumb)
	{
		if (!isset(FApplication::$config['thumbs'][$thumb])) {
			throw new Exception("Thumbnail config `$thumb` doesn't exists!");
		}
		
		// create image
		$thumbFileName = self::getStorageDir(FALSE, $thumb) . DIRECTORY_SEPARATOR . $this->getFileName();
		$thumb = FApplication::$config['thumbs'][$thumb];
		$width = isset($thumb['width']) ? $thumb['width'] : NULL;
		$height = isset($thumb['height']) ? $thumb['height'] : NULL;
		$image = Image::fromFile($fileName)->resize($width, $height)/*->sharpen()*/->save($thumbFileName);
	}
	
	/**
	 * Returns storage dir based on $thumb
	 * @param boolean $relative
	 * @return string
	 */
	static public function getStorageDir($relative = FALSE, $thumb = NULL)
	{
		if ($relative === FALSE) {
			$path = self::$storageDir;
		} else {
			$httpRequest = new HttpRequest();
			$path = $httpRequest->getUri()->getBasePath(); 
			$path = self::$storageDirR;
		}
		
		// add folder for thumbs
		if ($thumb !== NULL && isset(FApplication::$config['thumbs'][$thumb])) {
			$thumb = FApplication::$config['thumbs'][$thumb];
			$path .= DIRECTORY_SEPARATOR . $thumb['dir']; 
		}
		
		return $path;
	}
	
	
	/**
	 * Returns HTML tag for image
	 * @param string $thumb
	 * @return mixed
	 */
	public function getHtmlTag($thumb = NULL, $alt = NULL)
	{
		$filePath = $this->getFilePath(FALSE, $thumb); 
		if (is_file($filePath)) {
			$el = Html::el('img')
				->src($this->getFilePath(TRUE, $thumb))
				//->alt($this->name)
				//->title($this->name)
				;
            ($alt) ? $el->alt($alt) : NULL;
			//  diff size for thumbnail
			if ($thumb !== NULL) {
				$size = @getimagesize($filePath);
				if (isset($size[0])) $el->width($size[0]);
				if (isset($size[1])) $el->height($size[1]);
			} else {
				$el->width($this->width)->heigth($this->height);
			}
			return $el;
		}
		return NULL;
	}
	
	/**
	 * Returns all images
	 * @param array $where
	 * @param int $limit
	 * @param int $offset
	 * @param boolean $returnAsObjects
	 * @return array
	 */
	static public function getAll($where = array(), $limit = NULL, $offset = NULL, $returnAsObjects = TRUE)
	{
		$result = FApplication::$db->select('*')->from(TBL_FILES)->where('isImg IS NOT NULL')->orderBy('dtc', dibi::DESC);
		// where
	 	if (!empty($where)) {
			foreach ($where as $key=>$val) {
				$result->where('%n', $key, ' LIKE %s',$val);
			}
		}
		// limit
		if ($limit !== NULL) $result->limit($limit);
		// offset
		if ($offset !== NULL) $result->offset($offset);
		$return = $result->execute()->fetchAll();
		
		// images
		if ($returnAsObjects === TRUE) {
			$images = array();
			foreach ($return as $key => $val) {
				try {
					$images[$val->fileId] = new self($val->fileId);
				} catch (Exception $e) {
				}
			}
			return $images;
		}
		return $return;
	}
	
	
	/**
	 * Returns all images by where
	 * @param array $where
	 * @return int
	 */	
	static public function getCount($where = array())
	{
		$result = FApplication::$db->select('COUNT(*)')->from(TBL_FILES)->where('isImg IS NOT NULL')->orderBy('dtc', dibi::DESC);
		// where
	 	if (!empty($where)) {
			foreach ($where as $key=>$val) {
				$result->where('%n', $key, ' LIKE %s',$val);
			}
		}
		$count = $result->execute()->fetchSingle();
		return $count;
	}
	
	
	/**
	 * Returns node images
	 * @param int $nodeId
	 * @return array $files
	 */
	static public function getNodeImages($nodeId, $returnAsObjects = TRUE)
	{
		$all = dibi::select('*')->from(TBL_FILES)->where('isImg IS NOT NULL')->and('nodeId=%i', $nodeId)->execute()->fetchAll();
		// return as objects
		if ($returnAsObjects === TRUE) {
			$images = array();
			foreach ($all as $key => $val) {
				try {
					$images[$val->fileId] = new self($val->fileId);
				} catch (Exception $e) {
				}
			}
			return $images;
		}
		return $all;
	}
}
