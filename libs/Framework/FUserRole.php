<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   CMS
 * @package    Cms\FUserRole
 * @version    User.php 2009-10-13 divak@gmail.com
 */

/*namespace Cms\FUserRole;*/


/**
 *
 * @author     Stanislav Bazik
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @package    Cms\FUserRole
 */
class FUserRole extends Object
{
	/** @var int */
	public $userRoleId;

	/** @var string */
	public $parentId;
	
	/** @var FUserRole */
	public $parent;

	/** @var string */
	public $key;

	/** @var string */
	public $title;

	/** @var int */
	public $authorId;

	/** @var int */
	public $editorId;

	/** @var string */
	public $dtc;

	/** @var string */
	public $dtm;
	
	/**
	 * @param int $userId
	 * @return void
	 */
	public function __construct($userRoleId=0)
	{
		$this->userRoleId = (int)$userRoleId; 

		if ($this->userRoleId) {
			$w = FApplication::$db->select('*')
				->from(TBL_USERS_ROLES)
				->where('user_role_id=%i', $this->getId())
				->execute()->fetch();
				
			if ($w) {
				$this->userRoleId = $w['user_role_id'];
				$this->parentId = $w['parent_id'];
				$this->parent = ($this->parentId !== NULL) ? new self($this->parentId) : NULL;
				$this->key = $w['key'];
				$this->title = $w['title'];
				$this->authorId = $w['author_id'];
				$this->editorId = $w['editor_id'];
				$this->dtc = $w['dtc'];
				$this->dtm = $w['dtm'];
				
			} else {
				throw new Exception("User role with id `id` doesn't exists!");
			}
		}
	}
	
	/**
	 * @return boolean
	 */
	public function getId() { return( $this->userRoleId ); }

	/**
	 * @return boolean
	 */
	public function exists() { return( $this->userRoleId > 0 ); }

	/**
	 * @return boolean
	 */
	public function isNew() { return( $this->userRoleId == 0 ); }

	/**
	 * @return int $userId
	 */
	public function save()
	{
		$action = $this->getId() ? 'update' : 'insert';
		
		$values = array();
		$values['parent_id'] = $this->parentId;
		$values['key'] = $this->key;
		$values['title'] = $this->title;
		
		if ($action == 'update') {
			$values['user_role_id'] = $this->getId();
			$values['dtm'] = new DibiDateTime();
			$values['editor_id'] = FUser::getSignedIn()->getId();
			FApplication::$db->update(TBL_USERS_ROLES, $values)->where('user_role_id=%i', $this->getId())->execute();
			
		} else {
			$values['dtc'] = new DibiDateTime();
			$values['dtm'] = new DibiDateTime();
			$values['author_id'] = FUser::getSignedIn()->getId();
			$values['editor_id'] = FUser::getSignedIn()->getId();
			
			$lastId = FApplication::$db->insert(TBL_USERS_ROLES, $values)->execute(dibi::IDENTIFIER);
			
			// new id for user
			$this->userRoleId = $lastId;
		}
	}

	/**
	 * @return void
	 */
	public function delete()
	{
		if ($this->exists()) {
			FApplication::$db->delete(TBL_USERS_ROLES)->where('user_role_id=%i', $this->getId())->execute();
		}
	}
	
	static public function getAll($returnAsObjects = TRUE)
	{
		$roles = FApplication::$db->select('*')->from(TBL_USERS_ROLES)->execute()->fetchAll();
		if ($returnAsObjects === TRUE) {
			$objects = array();
			foreach ($roles as $key=>$role) {
				$objects[$role->user_role_id] = new self($role->user_role_id); 
			}
			return $objects;
		} 
		return $users;	
	}
	
	static public function getDropdown()
	{
		$roles = FApplication::$db->select('user_role_id, title')->from(TBL_USERS_ROLES)->execute()->fetchPairs();
		return $roles;
	}
    /**
     * @return boolean
     */
    public function isAdmin()
    {
        return ($this->key === 'admin');
    }
}