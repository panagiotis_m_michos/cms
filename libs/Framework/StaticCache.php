<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   CMS
 * @package    Cms\Aplication
 * @version    StaticCache.php 2009-01-13 divak@gmail.com
 */

/*namespace Cms\StaticCache;*/

/**
 * Provides caching data
 *
 * @author     Stanislav Bazik
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @package    Cms\StaticCache
 */
class StaticCache extends Object
{
	/** @var boolean */
	static public $enabled = TRUE; 
	
	/** @var array */
	static private $cache = array();
	
	/**
	 * @param string $key
	 * @param mixed $data
	 * @return boolean
	 */
	static public function write($key, $data)
	{
		# is enabled
		if (!self::$enabled) {
			return FALSE;
		}
		
		if (!is_String($key)) {
			throw new Exception("Cache key name must be string, " . gettype($key) ." was given.");
		}
		
		self::$cache[$key] = $data;
		return TRUE;
	}
	
	/**
	 * @param string $key
	 * @return mixed
	 */
	static public function read($key)
	{
		if (!is_String($key)) {
			throw new Exception("Cache key name must be string, " . gettype($key) ." was given.");
		}
		
		return isSet(self::$cache[$key]) ? self::$cache[$key] : FALSE;
	}
	
	/**
	 * @param string $key
	 * @return boolean
	 */
	static public function remove($key)
	{
		if (!is_String($key)) {
			throw new Exception("Cache key name must be string, " . gettype($key) ." was given.");
		}
		
		if (isSet(self::$cache[$key])) {
			unset(self::$cache[$key]);
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	/**
	 * @return boolean
	 */
	static public function clean()
	{
		self::$cache = Array();
	}
}
