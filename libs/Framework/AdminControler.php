<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    AdminControler.php 2009-03-19 divak@gmail.com
 */

abstract class AdminControler extends FControler
{
	/** @var string */
	protected $templateExtendedFrom = array();
	
	/**
	 * This method is calling from Menu::getJsTreeMenu()
	 * Here can be changes for render
	 * @return void
	 */
	public function beforePrepare()
	{
		// change action if is empty
		if (empty($this->action)) {
			if (!empty($this->tabs)) {
				$tab = current(array_keys($this->tabs));
				$this->action = $tab;
				$this->view = $this->action;
			} elseif (empty($this->tabs)) {
				$this->action = 'default';
			}
		}
	}
	
	/**
	 * @return string
	 */	
	final public function getHtml()
    {
        // paths
        $paths = array();
        //$paths[] = ADMIN_PROJECT_TEMPLATES_DIR . '/';
        $paths[] = ADMIN_TEMPLATES_DIR . '/';
        
        // controler name
        $controler = str_replace('Controler','',get_class($this));
        
        // all templates possibilities
        $tpls = array();
        foreach ($paths as $key=>$path) {
	        $tpls[] = $path . $controler . '.' . $this->getView() . '.tpl'; # [controler].[view].tpl
	        $tpls[] = $path . $controler . '/' . $this->getView() . '.tpl'; # [controler]/[view].tpl
	        
	        // template extended
	        if (!empty($this->templateExtendedFrom)) {
	        	foreach ($this->templateExtendedFrom as $key=>$val) {
	        		$tpls[] = $path . $val . '.' . $this->getView() . '.tpl'; # [controler].[view].tpl
	        		$tpls[] = $path . $val . '/' . $this->getView() . '.tpl'; # [controler]/[view].tpl
	        	}
	        }
        }
        
        // check in defaults
        foreach ($paths as $key=>$path) {
	        $tpls[] = $path . $controler . '.default.tpl'; # [controler].default.tpl
		    $tpls[] = $path . $controler . '/default.tpl'; # [controler]/default.tpl
        }
        
        // find template
        $found = FALSE;
        foreach ($tpls as $key=>$tpl) {
            if (is_file($tpl)) {
            	$found = TRUE;
                return FApplication::$template->getHtml($tpl);
            }
        }
        
        // can't find a template
        if (!$found) trigger_error(get_class($this).': Can\'t find template for view: ' . $this->getView() . ' or default', E_USER_ERROR);
    }
}
