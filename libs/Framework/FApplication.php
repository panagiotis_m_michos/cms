<?php

use Bootstrap\Ext\FrameworkExt;
use BootstrapModule\Ext\DoctrineExt;
use ErrorModule\Controllers\IErrorController;
use ErrorModule\Ext\DebugExt;
use ErrorModule\Ext\PanelExt;
use RouterModule\ConverterManager;
use RouterModule\Converters\DateTimeConverter;
use RouterModule\Converters\EntityConverter;
use RouterModule\Converters\SimpleTypeConverter;
use RouterModule\Dispatcher;
use RouterModule\Exceptions\RouteException;
use RouterModule\ObjectMapper;
use RouterModule\ResponseHandler;
use RouterModule\RouterExt;
use RouterModule\TemplateRenderer;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Routing\Router;
use Utils\Directory;

include LIBS_DIR . '/3rdParty/Smarty/Smarty.class.php';

include LIBS_DIR . '/3rdParty/Nette/compatibility.php';

/**
 * Provides run application
 */
class FApplication extends Object
{
    /** @var int */
    public static $nodeId = NULL;

    /** @var string */
    public static $lang = 'EN';

    /** @var string */
    public static $action = NULL;

    /** @var string */
    public static $view;

    /** @var string */
    public static $host;

    /** @var array */
    public static $config;

    /**
     * @var HttpRequest
     */
    public static $httpRequest;

    /** @var array */
    public static $get;

    /** @var array */
    public static $post;

    /** @var array */
    public static $files;

    /** @var array */
    public static $cookies;

    /**
     * @var HttpResponse
     */
    public static $httpResponse;

    /**
     * @var Session
     */
    public static $session;

    /** @var object */
    public static $db;

    /** @var object */
    public static $template;

    /** @var object */
    public static $controler;

    /**
     * @var FRouter
     */
    public static $router;

    /**
     * @var Container
     */
    public static $container;

    /**
     * @var IErrorController
     */
    private $errorController;

    /**
     * @var Dispatcher
     */
    private $dispatcher;

    /**
     * @var Router
     */
    private $newRouter;

    /**
     * @var ResponseHandler
     */
    private $responseHandler;

    /**
     * @param $config
     * @param Container $containerBuilder
     * @throws DibiException
     */
    public function __construct($config, Container $containerBuilder)
    {
        self::$config = $config;
        self::$container = $containerBuilder;
        $this->errorController = $containerBuilder->get(DebugExt::ERROR_CONTROLLER);
        $this->newRouter = $containerBuilder->get(RouterExt::ROUTER);

        self::$httpRequest = new HttpRequest();

        self::$get = self::$httpRequest->getQuery();
        self::$post = self::$httpRequest->getPost();
        self::$files = self::$httpRequest->getFiles();
        self::$cookies = self::$httpRequest->getCookies();

        //removing friendly url from global get (symfony forms used without form name produces error since friendly_url is additional parameter)
        if (isset($_GET['friendly_url'])) {
            unset($_GET['friendly_url']);
        }

        self::$httpResponse = new HttpResponse();
        self::$session = $containerBuilder->get(DiLocator::SESSION);
        self::$db = $containerBuilder->get(FrameworkExt::DIBI);

        $db = self::$config['database'];
        CHFiling::setDb([$db['host'], $db['username'], $db['password'], $db['database']]);
        CHFiling::setDocPath(PROJECT_DIR . '/temp/upload/ch_documents');

        self::$template = $containerBuilder->get('templating_module.template_factory')->create();
        self::$router = $containerBuilder->get('router');
        self::$nodeId = self::$router->getCurrentNodeId();
        self::$lang = self::$router->getCurrentLanguage();
        self::$action = self::$router->getCurrentAction();
        self::$view = self::$router->getCurrentView();

        if ($containerBuilder->getParameter('isProduction') && !self::$httpRequest->isAjax() && FUser::isAdminSignedIn()) {
            PanelExt::$showDebugBar = TRUE;
        }

        if (isset(self::$get['redeem'])) {
            VoucherService::enableUpgradeVoucherForm();
        }

        $converterManager = new ConverterManager();
        $converterManager->addConverter(
            ['doctrine'],
            new EntityConverter($containerBuilder->get(DoctrineExt::ENTITY_MANAGER))
        );
        $converterManager->addConverter(['DateTime', 'Utils\Date'], new DateTimeConverter());
        $converterManager->addConverter(['float', 'int', 'integer', 'bool', 'boolean', 'string', 'array'], new SimpleTypeConverter());

        $this->dispatcher = new Dispatcher($containerBuilder, new ObjectMapper($converterManager));
        $this->responseHandler = new ResponseHandler($containerBuilder, new TemplateRenderer(self::$template, DOCUMENT_ROOT . '/project', 'tpl'));
    }

    public function oldRun()
    {
        self::$controler = self::$router->getControler();
        try {
            self::$controler->run();
        } catch(Exception $e) {
            $this->errorController->handleControllerError($e, self::$controler);
        }
    }

    public function run()
    {

        try {
            FApplication::$container->get('router_module.route_processor')->handle(self::$container->get(RouterExt::REQUEST));
        } catch (RouteException $e) {
            $this->oldRun();
        } catch (Exception $e) {
            $this->errorController->handleControllerError($e);
        }
    }

    /**
     * @return boolean
     */
    public static function isAdmin()
    {
        return !empty($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') === 0;
    }

    /**
     * @return boolean
     */
    public static function hasAllowedFriendlyUrl()
    {
        return (bool) self::$config['friendly_url'];
    }

    /**
     * @param string $text
     * @return string
     */
    public static function modifyCmsText($text)
    {
        $parser = Registry::getService(DiLocator::UTILS_TEXT_PARSER);

        return $parser->parse($text);
    }

    /**
     * @param string $namespace
     * @return object $cache
     */
    public static function getCache($namespace = 'cache-cms')
    {
        return new Cache(Registry::getService(DiLocator::CACHE_NETTE_FILESYSTEM), $namespace);
    }

    public static function getControlers($type)
    {
        $controlers = [];
        switch ($type) {
            case 'front':
                $files = FTools::scanDir(FRONT_CONTROLERS_DIR, FTools::RETURN_FILES);
                foreach ($files as $key => $file) {
                    if ($file == 'BaseControler.php' || $file == 'ErrorControler.php')
                        continue;
                    $key = substr($file, 0, -4);
                    $controlers[$key] = preg_replace('#Controler|\.php#', '', $file);
                }
                asort($controlers);
                break;
            case 'admin':
                // from project admin
                //$files = FTools::scanDir(ADMIN_PROJECT_CONTROLERS_DIR, FTools::RETURN_FILES);
                //foreach ($files as $key => $file) {
                    //$key = substr($file, 0, -4);
                    //$controlers['Custom'][$key] = preg_replace('#AdminControler|\.php#', '', $file);
                //}
                //asort($controlers['Custom']);
                // from admin
                $files = FTools::scanDir(ADMIN_CONTROLERS_DIR, FTools::RETURN_FILES);
                foreach ($files as $key => $file) {
                    if (in_array($file, ['BaseAdminControler.php', 'ErrorAdminControler.php', 'AuthAdminControler.php']))
                        continue;
                    $key = substr($file, 0, -4);
                    $controlers['Main'][$key] = preg_replace('#AdminControler|\.php#', '', $file);
                }
                asort($controlers['Main']);
                break;
        }
        return $controlers;
    }

    /**
     * @return FTemplate
     */
    public static function getTemplate()
    {
        return self::$template;
    }
}
