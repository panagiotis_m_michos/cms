<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   CMS
 * @package    Cms\FLoader
 * @version    FLoader.php 2009-01-08 divak@gmail.com
 */

/*namespace Cms\FLoader;*/

include_once dirname(__FILE__) . '/FTools.php';

include_once LIBS_DIR . '/3rdParty/Nette/Object.php';

/**
 * Loading all classes when needed
 * 
 * @author     Stanislav Bazik
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @package    Cms\FLoader
 */
class FLoader extends Object
{
	/** @var array */
	private $dirs = array();
	
	/** @var array */
	private $exludedDirs = array();
	
	/** @var array */
	private $files = array();
	
	/** @var boolean */
	private $useCache = FALSE;
	
	/** @var string */
	private $cacheFile;
	
	/** @var boolean */
	public $autoRebuild = FALSE;
	
	/**
	 * @return object
	 */
	public function __construct()
	{
		$this->cacheFile = PROJECT_DIR . '/temp/cache/cache_FLoader';
		
		// read from cache
		if (is_file($this->cacheFile)) {
			$file = @file_get_contents($this->cacheFile);
			$this->files = unserialize($file);
			$this->useCache = TRUE;
		} 
	}
	
	/**
	 * @param string $dir
	 * @return void
	 */
	public function addDir($dir)
	{
		$this->dirs[] = (string)$dir;
		return $this;
	}
	
	/**
	 * @param string $dir
	 * @return void
	 */
	public function addExludedDir($dir)
	{
		$this->exludedDirs[] = (string)$dir;
		return $this;
	}
	
	/**
	 * @param string $dir
	 * @return void
	 */
	private function __rebuild($dir)
	{
		$files = FTools::scanDir($dir, FTools::RETURN_FILES); 
		if (!empty($files)) {
			foreach ($files as $key=>$file) {
				$pathInfo = pathinfo($file);
				if (isset($pathInfo['extension']) && $pathInfo['extension'] == 'php') {
					$className = substr($file, 0, -4);
					$this->files[$className] = $dir . '/' . $file;
				}
			}
		}
		
		// recursive adding files
		foreach (FTools::scanDir($dir, FTools::RETURN_DIRS) as $key=>$val) {
			$dirPath = $dir . '/' . $val . '/';
			if (!in_array($dirPath, $this->exludedDirs)) {
				$this->__rebuild($dir.'/'.$val);
			}
		}
		
	}
	
	/**
	 * @return void
	 */
	private function rebuild()
	{
		foreach ($this->dirs as $key=>$val) {
			$this->__rebuild($val);
		}
		
		// try to create file...
		if (@file_put_contents($this->cacheFile, serialize($this->files)) === FALSE) {
			trigger_error('Cannot write to '.$this->cacheFile, E_USER_ERROR);
		}
	}
	
	
	/**
	 * @return void
	 */
	public function run()
	{
		// if caches hasn't been created
		if (!$this->useCache) {
			$this->rebuild();
		} 
		
		spl_autoload_register(array($this, 'tryLoad'));
	}
	
	/**
	 * @param string $x
	 * @return void
	 */
	private function tryLoad($x)
	{
		if (class_exists($x)) {
			return;
		}
		
		// found and include
		if (isSet($this->files[$x])) {
			include $this->files[$x];
		// try to rebuild
		} elseif ($this->autoRebuild) {
			$this->rebuild();
			
			// try include again
			if (isSet($this->files[$x])) {
				include $this->files[$x];
			}
		}
	}
}
