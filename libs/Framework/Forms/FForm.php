<?php

class FForm extends Object implements ArrayAccess, IteratorAggregate
{
    const METHOD_GET = 'get';
    const METHOD_POST = 'post';
    const Required = 'Required';
    const Email = 'Email';
    const Equal = 'Eq';
    const NUMERIC = 'Numeric';
    const GREATER_THAN = 'Gt';
    const REGEXP = 'RegExp';
    const MIME_TYPE = 'MimeType';
    const MAX_LENGTH = 'MaxLength';
    const MIN_LENGTH = 'MinLength';

    /** @var string */
    public $action;

    /** @var string */
    public $method = 'post';

    /** @var string */
    public $name;

    /** @var string */
    public $enctype = 'multipart/form-data';

    /** @var array */
    public $elements = [];

    /** @var array */
    public $errors = [];

    /** @var boolean */
    public $hasEnctype = FALSE;

    /** @var array */
    public $controlsValues = [];

    /**
     * values for controls defined on beginning
     * @var array
     */
    protected $initControlsValues = [];

    /**
     * contanier for groups
     * @var array
     */
    public $groups = [
        'plain' => [
            'type' => 'plain',
            'elements' => []
        ]
    ];

    /** @var integer */
    public $sessionInstanties = 5;

    /** @var string */
    protected $render_class = 'DefaultRender';

    /** @var object Render */
    protected $render;

    /**
     * required paths
     * @var array
     */
    public $paths = [
        'URL_ROOT' => NULL,
        'FORMS_DIR_R' => NULL,
        'FILES_DIR_R' => NULL
    ];

    /**
     * is for inlcuding js files and css file for dyncal
     * if is dyncal generated, this value will be change to TRUE
     */
    public $dyncalUsed = FALSE;

    /** @var array */
    public $onValid = [];

    /** @var array */
    public $beforeValidation = [];

    /**
     * can we store form inside session
     * if false there will not be any redirection after it fails
     * @var bool
     */
    protected $storable = TRUE;


    /************************** METHODS ************************************/

    public function __construct($name, $method = 'post')
    {
        $this->controlsValues = $method == 'post' ? $_POST : $_GET;
        $this->method = $method;

        self::setName($name);
        self::setAction($_SERVER['REQUEST_URI']);

        $this->addHidden('submited_' . $this->getName(), 1);

        if (defined('URL_ROOT')) {
            $this->setPath('URL_ROOT', URL_ROOT);
        }
        if (defined('FORMS_DIR_R')) {
            $this->setPath('FF_PATH_R', FORMS_DIR_R);
        }
    }

    /**
     * @param string $name
     * @param string $method
     * @return $this
     */
    public static function create($name, $method = 'post')
    {
        $self = new self($name, $method);

        return $self;
    }

    /**
     * Is called only if method is get
     * @param array $params - get/post
     */
    protected function addHiddenGetParams($params)
    {
        $controls = array_keys($this->elements);
        foreach ($params as $key => $val) {
            if (!in_array($key, $controls) && $key != 'friendly_url' && $key != 'route') {
                $this->addHidden($key, $val);
            }
        }
    }


    /************************** SETTERS ************************************/

    /**
     * @param string $path
     * @param  string $value
     * @return $this
     */
    public function setPath($path, $value)
    {
        $this->paths[$path] = (string) $value;

        return $this;
    }

    /**
     * @param string $action
     * @return $this
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param string $enctype
     * @return $this
     */
    public function setEnctype($enctype)
    {
        $this->enctype = $enctype;

        return $this;
    }

    /**
     * @param boolean $has
     * @return $this
     */
    public function setHasEnctype($has)
    {
        $this->hasEnctype = (bool) $has;

        return $this;
    }

    /**
     * @param array $source
     * @return $this
     */
    public function setElementsValue($source)
    {
        foreach ($this->elements as $key => $control) {
            if ($control->getGroup() != NULL) {
                if (isSet($source[$control->getGroup()][$key])) {
                    $val = $source[$control->getGroup()][$key];
                    if (is_string($val)) {
                        $val = trim($val);
                    }
                    $control->setValue($val);
                } else {
                    $control->setValue(NULL);
                }
            } else {
                if (isSet($source[$key])) {
                    $val = $source[$key];
                    if (is_string($val)) {
                        $val = trim($val);
                    }
                    $control->setValue($val);
                } else {
                    $control->setValue(NULL);
                }
            }
        }

        return $this;
    }

    /**
     * @param array $values
     * @return $this
     */
    public function setInitValues($values)
    {
        $this->initControlsValues = (array) $values;

        return $this;
    }

    /**
     * @param int $count
     * @return $this
     */
    public function setSessionInstanties($count)
    {
        $this->sessionInstanties = (int) $count;

        return $this;
    }

    /**
     * @param string $render_class
     * @return $this
     */
    public function setRenderClass($render_class)
    {
        $this->render_class = (string) $render_class;

        return $this;
    }


    /************************** GETTERS ************************************/


    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getEnctype()
    {
        return $this->enctype;
    }


    /**
     * @return array
     */
    public function getElements()
    {
        return $this->elements;
    }

    /**
     * @param object $element
     * @return object
     */
    public function getLabel($element)
    {
        if (!isSet($this->elements[$element])) {
            trigger_error('getLabel($element): ' . $element . ' doesn\'t exists', E_USER_ERROR);
        }

        return $this->elements[$element]->getLabel();
    }

    /**
     * @param object $element
     * @param mixed $key
     * @return object
     */
    public function getControl($element, $key = NULL)
    {
        if (!isSet($this->elements[$element])) {
            trigger_error('getControl($element): ' . $element . ' doesn\'t exists', E_USER_ERROR);
        }

        return $this->elements[$element]->getControl($key);
    }

    /**
     * returns begin from form
     * @return string $html
     */
    public function getBegin()
    {
        return $this->render->getBegin();
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return string $html
     */
    public function getEnd()
    {
        return $this->render->getEnd();
    }

    /**
     * @return string $html
     */
    public function getHtmlErrors()
    {
        return $this->render->getHtmlErrors();
    }

    /**
     * @return string $html
     */
    public function getHtmlErrorsInfo()
    {
        return $this->render->getHtmlErrorsInfo();
    }

    /**
     * @param string $control
     * @param string $name
     * @param string $label
     * @return object
     */
    public static function getElement($control, $name, $label = NULL, $_this = NULL)
    {
        $control_path = FORMS_DIR . '/controls/' . ucfirst($control) . '.php';
        if (!is_file($control_path)) {
            trigger_error('addElement($control, ...): ' . $control . ' doesn\'t exists!', E_USER_ERROR);
        }

        include_once($control_path);
        $element = new $control($name, $label, $_this);

        return $element;
    }

    /**
     * Can put to fieldsets, better error messages
     * @return string $html
     */
    public function getHtml()
    {
        return $this->render->getHtml();
    }

    /**
     * @param string $control
     * @return string
     */
    public function getError($control)
    {
        if (isSet($this->errors[$control])) {
            return $this->errors[$control];
        }
    }

    /**
     * @param string $control
     * @return bool
     */
    public function hasError($control)
    {
        return isset($this->errors[$control]) ? TRUE : FALSE;
    }

    /**
     * @param string $control
     * @return string
     */
    public function getHtmlError($control)
    {
        return $this->render->getHtmlError($control);
    }

    /**
     * @param string $control
     * @return misc
     */
    public function getValue($control)
    {
        if (!isSet($this->elements[$control])) {
            trigger_error('getValue($control): ' . $control . ' doesn\'t exists.', E_USER_ERROR);
        }

        return urldecode($this->elements[$control]->getValue());
    }

    /**
     * @return array
     */
    public function getValues()
    {
        foreach ($this->elements as $key => $val) {

            if ($val->getGroup() != NULL) {
                $values[$val->getGroup()][$key] = $val->getValue();
            } else {
                $values[$key] = $val->getValue();
            }
        }

        return $values;
    }



    /************************** QUESTIONS ************************************/


    /**
     * @return boolean
     */
    public function hasEnctype()
    {
        return $this->hasEnctype;
    }

    /**
     * @return boolena
     */
    public function hasErrors()
    {
        return !empty($this->errors);
    }

    /**
     * @return boolean
     */
    public function isOk2Save()
    {
        return $this->isSubmited() && $this->isValid();
    }

    /**
     * @return boolean
     */
    protected function isValid()
    {
        return !$this->hasErrors();
    }

    /**
     * @return boolean
     */
    public function isSubmited()
    {
        return isSet($this->controlsValues['submited_' . $this->getName()]);
    }

    /**
     * @param string $control
     * @return boolean
     */
    public function isSubmitedBy($control)
    {
        return isSet($this->controlsValues[$control]);
    }

    /**
     * @param $control
     * @return bool
     */
    public function isSubmitedByMatch($control)
    {
        $matched = preg_grep('/' . $control . '/', array_keys($this->controlsValues));

        return !empty($matched);
    }

    /**
     * @param $control
     * @return string|NULL
     */
    public function getSubmitNameByMatch($control)
    {
        $matched = preg_grep('/' . $control . '/', array_keys($this->controlsValues));

        return (!empty($matched) ? current($matched) : NULL);
    }

    /************************** ADDINGS ************************************/


    /**
     * @param string $name
     * @param string $label
     * @return Text $el
     */
    public function addText($name, $label = NULL)
    {
        return $this->createElement('text', $name, $label);
    }

    /**
     * @param string $name
     * @param string $label
     * @return Password $el
     */
    public function addPassword($name, $label = NULL)
    {
        return $this->createElement('password', $name, $label);
    }

    /**
     * @param string $name
     * @param string $value
     * @return Hidden $el
     */
    public function addHidden($name, $value)
    {
        return $this->createElement('Hidden', $name)->setValue($value);
    }

    /**
     * @param string $name
     * @param $label
     * @return Area $el
     */
    public function addArea($name, $label)
    {
        return $this->createElement('area', $name, $label);
    }

    /**
     * @param string $name
     * @param $label
     * @return FckArea $el
     */
    public function addFckArea($name, $label)
    {
        return $this->createElement('FckArea', $name, $label);
    }

    /**
     * @param string $name
     * @param string $value
     * @return Submit $el
     */
    public function addSubmit($name, $value)
    {
        return $this->createElement('Submit', $name)->setValue($value);
    }

    /**
     * @param string $name
     * @param string $value
     * @return Reset $el
     */
    public function addReset($name, $value)
    {
        return $this->createElement('Reset', $name)->setValue($value);
    }

    /**
     * @param string $name
     * @param string $label
     * @param array $value
     * @return Radio
     */
    public function addRadio($name, $label, $value)
    {
        return $this->createElement('Radio', $name, $label)->setOptions($value);
    }

    /**
     * @param string $name
     * @param string $label
     * @param array $value
     * @return Checkbox
     */
    public function addCheckbox($name, $label, $value)
    {
        return $this->createElement('Checkbox', $name, $label)->setOptions($value);
    }

    /**
     * @param string $name
     * @param string $label
     * @param array $value
     * @return Select
     */
    public function addSelect($name, $label, $value)
    {
        return $this->createElement('Select', $name, $label)->setOptions($value);
    }

    /**
     * @param string $name
     * @param string $label
     * @param null $path
     * @return InputFile
     */
    public function addFile($name, $label = NULL, $path = NULL)
    {
        return $this->createElement('InputFile', $name, $label)
            ->setPath($path);
    }

    /**
     * @param string $name
     * @param string $label
     * @param null $path
     * @return InputImg
     */
    public function addImg($name, $label = NULL, $path = NULL)
    {
        return $this->createElement('InputImg', $name, $label)->setPath($path);
    }

    /**
     * @param string $key
     * @param string $msg
     */
    public function addErr($key, $msg)
    {
        $this->errors[$key] = $msg;
    }

    /**
     * @param object $control
     * @param string $name
     * @param string $label
     * @return object
     */
    public function add($control, $name, $label)
    {
        return $this->createElement($control, $name, $label);
    }

    /**
     * @param FControl $control
     * @return mixed
     */
    public function addControl(FControl $control)
    {
        $this->elements[$control->getName()] = $control;
        $keys = array_keys($this->groups);
        $this->groups[end($keys)]['elements'][] = $control->getName();

        return $this->elements[$control->getName()];
    }

    /**
     * @param string $legend_name
     */
    public function addFieldset($legend_name)
    {
        $this->groups[] = [
            'type' => 'fieldset',
            'legend' => $legend_name,
            'elements' => []
        ];
    }

    public function endFieldset()
    {
        $this->groups[] = [
            'type' => 'plain',
            'elements' => []
        ];
    }

    protected function checkSessionInstanties()
    {
        if (isSet($_SESSION[get_class($this)])) {
            while (count($_SESSION[get_class($this)]) > $this->sessionInstanties) {
                array_shift($_SESSION[get_class($this)]);
            }
        }
    }

    /************************** OTHERS ************************************/

    public function start()
    {
        // how many instanties are in session
        self::checkSessionInstanties();

        // add hidden fields for get parameters
        if ($this->method == 'get') {
            $this->addHiddenGetParams($_GET);
        }

        // form has been submitted
        if ($this->isSubmited()) {

            // input values
            $this->setElementsValue($this->controlsValues);

            // if form is not valid and method is post
            if ($this->beforeValidation) {
                call_user_func($this->beforeValidation, $this);
            }

            // if controls have validators and they are valid
            $this->checkControlsValidate();

            // save forms data and errors to session for future use
            if ($this->isStorable()) {
                $this->saveToSession();
            }

            // if form is not valid and method is post
            if (!$this->isValid() && $this->method == 'post') {
                // razvan add this :)
                if (FApplication::$httpRequest->isAjax()) {
                    echo json_encode(['error' => count($this->getErrors()), 'message' => $this->getErrors()]);
                    die();
                }
                if ($this->isStorable()) {
                    Header('Location: ' . $this->getAction());
                    exit;
                }
            } else {
                // call user function
                if (!empty($this->onValid)) {
                    call_user_func($this->onValid, $this);
                }
            }
        } else {

            // put posted data to controls
            if (isSet($_SESSION[get_class($this)][$this->getName()]['values'])) {
                $this->setElementsValue($_SESSION[get_class($this)][$this->getName()]['values']);
            }

            // errors
            if (isSet($_SESSION[get_class($this)][$this->getName()]['errors'])) {
                $this->errors = $_SESSION[get_class($this)][$this->getName()]['errors'];
            }
        }


        // ON First START - set init values
        if (!isSet($_SESSION[get_class($this)][$this->name]['values'])) {
            foreach ($this->initControlsValues as $key => $val) {
                if (!isSet($this->elements[$key])) {
                    continue;
                }
                $this->elements[$key]->setValue($val);
            }
        }

        // render class
        include_once FORMS_DIR . '/renders/' . $this->render_class . '.php';
        $this->render = new $this->render_class($this);
    }

    public function saveToSession()
    {
        // save post data for new render
        $_SESSION[get_class($this)][$this->getName()]['values'] = $this->controlsValues;

        // save error msg for new render
        $_SESSION[get_class($this)][$this->getName()]['errors'] = $this->errors;
    }

    /**
     * Do check validators and sets error messages
     */
    protected function checkControlsValidate()
    {
        foreach ($this->elements as $key => $control) {
            foreach ($control->validators as $key2 => $val2) {
                if (is_array($val2) && is_callable($val2[0])) {
                    $ret = call_user_func($val2[0], $control, $val2[1], $val2[2]);
                    if ($ret !== TRUE) {
                        $this->addErr($control->getName(), $ret);
                        break;
                    }
                } else {
                    if (!$control->validators[$key2]->isValid($control->getValue())) {
                        $this->addErr($control->getName(), $control->validators[$key2]->err_msg);
                        break;
                    }
                }
            }
        }
    }

    /**
     * @param string $control
     * @param string $name
     * @param string $label
     * @return object
     */
    public function createElement($control, $name, $label = NULL)
    {
        $exists_path = NULL;
        $control_path = FORMS_DIR . '/controls/' . ucfirst($control) . '.php';

        // normal control path
        if (is_file($control_path)) {
            $exists_path = $control_path;
        }

        // if is not find, try in custom
        if (is_Null($exists_path) && defined('FF_CUSTOM_CONTROLS')) {
            $custom_control_path = FF_CUSTOM_CONTROLS . 'lib.' . ucfirst($control) . '.php';

            if (is_file($custom_control_path)) {
                $exists_path = $custom_control_path;
            }
        }

        if (!is_Null($exists_path)) {
            include_once $exists_path;
        }

        $element = new $control($name, $label, $this);
        $this->elements[$element->getName()] = $element;

        // add fieldset
        $keys = array_keys($this->groups);

        $this->groups[end($keys)]['elements'][] = $name;

        return $this->elements[$element->getName()];
    }

    public function clean()
    {
        unset($_SESSION[get_class($this)][$this->getName()]);
    }


    /**
     * @param string $name
     */
    public static function cleanForm($name)
    {
        unset($_SESSION[get_class()][$name]);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        try {
            return $this->getHtml();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function setStorable($storable)
    {
        $this->storable = $storable;
    }

    public function isStorable()
    {
        return $this->storable;
    }

    /* ******************** interface \ArrayAccess **************** */


    /**
     * @param  string $name
     * @param  mixed $value
     */
    final public function offsetSet($name, $value)
    {
        // check if element exists
        if (!isSet($this->elements[$name])) {
            trigger_error("Controller '$name' doesn't exists!", E_USER_ERROR);
        }
    }

    /**
     * @param  string $name
     * @return object form control
     */
    final public function offsetGet($name)
    {
        if (isSet($this->elements[$name])) {
            return $this->elements[$name];
        } else {
            trigger_error("Controler '$name' doesn't exists!", E_USER_ERROR);
        }
    }


    /**
     * @param  string $name
     * @return bool
     */
    final public function offsetExists($name)
    {
        return isset($this->elements[$name]) ? $this->elements[$name] : NULL;
    }


    /**
     * @param  string $name
     */
    final public function offsetUnset($name)
    {
        unset($this->elements[$name]);
    }



    /* ******************** interface \IteratorAggregate **************** */


    /**
     * @return object ArrayIterator
     */
    final public function getIterator()
    {
        return new ArrayIterator($this->elements);
    }

    public function getDefaultErrorMessage()
    {
        return sprintf('Oops! There are %d errors, please correct them below.', count($this->errors));
    }

}
