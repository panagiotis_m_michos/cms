<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    Checkbox.php 2009-03-19 divak@gmail.com
 */

require_once dirname(__FILE__) . '/Radio.php';

class Checkbox extends Radio
{
	/** @var array */
	protected $options = array();

	/** @var array */
	protected $value = array();
	
	/**
	 * @return string $html
	 */
	public function getControl($key=NULL)
	{
		// list of checkboxes
		$html='';
		if (!is_Null($key)) {
			$options = Array($key => $this->options[$key]);
		} else {
			$options = $this->options;
		}
		
		if (is_Array($options)) {
			foreach ($options as $key=>$val) {
				
				// controls
				$control = HTML::el('input')->type('checkbox')
					->value($key)
					->id($this->getName().$key);
				
				// group
				if ($this->group != NULL) {
					$control->name($this->group .'['.$this->getName().'][]');
				} else {
					$control->name($this->getName().'[]');
				}

				// this control should be set if
				if (in_Array($key,(array)$this->value)) {
					$control->checked('checked');
				}

				self::applyAttribs($control);

				// labels
				$label = HTML::el('label')
					->setHtml($val)
					->for($this->getName().$key);

				$html .= $control->render() . $label->render() . HTML::el('br');
			}
			
		// only one checkbox
		} else {
			$control = HTML::el('input')
				->type('checkbox')
				->value($options)
				->id($this->getName());
				
			// group
			if ($this->group != NULL) {
				$control->name($this->group .'['.$this->getName().']');
			} else {
				$control->name($this->getName());
			}
				
			// this control should be set if
			if ($options == $this->value) {
				$control->checked('checked');
			}
			
			self::applyAttribs($control);
			$html .= $control->render();
		}

		return $html;
	}
	
	/**
	 * @param array/string/int $options
	 */
	public function setOptions($options)
	{
		$this->options = $options;
		return $this;
	}

	/**
	 * @return string $html
	 */
	public function getLabel()
	{
		if (is_Null($this->label)) {
			return $this->label;
		}

		if(!is_Array($this->options)) {
			$label = HTML::el('label')
				->for($this->name)
				->setHtml((string)$this->label)
				->render() ;
			return $label;
		} else {
			return $this->label;
		}
	}
	
	/**
	 * @return misc $value
	 */
	public function getValue()
	{
		if (is_array($this->options)) {
			$this->value = $this->value;
		} else {
			$this->value = (int) $this->value;
		}
		
		return $this->value;
	}
}
?>