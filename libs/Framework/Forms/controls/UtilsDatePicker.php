<?php

use Utils\Date;

class UtilsDatePicker extends DatePicker
{
    const DATE_FORMAT = 'd/m/Y';

    /**
     * @param string $name
     * @param string $label
     * @param FForm $owner
     */
    public function __construct($name, $label, &$owner)
    {
        parent::__construct($name, $label, $owner);
        $this->options['dateFormat'] = 'dd/mm/yy';
    }


    /**
     * @param Date|string $value
     * @throws Exceptions\Business\InvalidDateException
     * @return UtilsDatePicker
     */
    public function setValue($value)
    {
        $this->value = NULL;

        if ($value) {
            if ($value instanceof Date) {
                $this->value = $value->format(self::DATE_FORMAT);
            } else {
                $value = Date::createFromFormat(self::DATE_FORMAT, $value);
                if ($value instanceof Date) {
                    $this->value = $value->format(self::DATE_FORMAT);
                } else {
                    $this->value = FALSE;
                }
            }
        }

        return $this;
    }

    /**
     * @return Date|boolean|NULL
     */
    public function getValue()
    {
        if ($this->value) {
            return Date::createFromFormat(self::DATE_FORMAT, $this->value);
        }
        return $this->value;
    }
}
