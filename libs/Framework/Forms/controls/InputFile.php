<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    InputFile.php 2009-03-19 divak@gmail.com
 */

require_once dirname(__FILE__) . '/FControl.php';

class InputFile extends FControl
{
	/** @var string */
	protected $error;
	
	/**
	 * @param string $name
	 * @param string $label
	 * @return void
	 */
	public function __construct($name,$label,&$owner)
	{
		parent::__construct($name,$label,$owner);
		$this->owner->hasEnctype = TRUE;
	}
	
	public function getError()
	{
		return $this->error;
	}
	
	/**
	 * @return string $html
	 */
	public function getControl()
	{
		if (is_Null($this->value) || empty($this->value)) {
			$control = HTML::el('input')
				->type('file')
				->name($this->name)
				->id($this->getId());

			self::applyAttribs($control);
			return $control->render();
			
		} else {
			
			$el = Html::el('a')
				->href($this->path . $this->value)
				->setText($this->value)
				->target('_blank');
				
			return $el->render();
		}
	}
	
	/**
	 * @param string $path
	 * @return void
	 */
	public function setPath($path)
	{
		$this->path = $path;
		return $this;
	}
	

	/**
	 * @return misc $value
	 */
	public function getValue()
	{
		if (isSet($_FILES[$this->name])) {
			if ($_FILES[$this->name]['error'] === UPLOAD_ERR_OK) {
				return $_FILES[$this->name];	
			} else {
				$this->error = $_FILES[$this->name]['error'];
				return NULL;	
			}
		}
		return NULL;
	}
	
	/**
	 * @param mixed $value
	 */
	public function setValue($value)
	{
		if (!is_Null($value)) {
			$this->value = $value;
		}
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getFileType()
	{
	}
}
