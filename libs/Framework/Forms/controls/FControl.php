<?php

abstract class FControl
{
    /**
     * @var FForm
     */
    public $owner;

    /**
     * @var array
     */
    public $validators = array();

    /**
     * @var string (namespace to use for custom validators)
     */
    public static $defaultValidatorNamespace = 'Forms\Validators';

    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $label;

    /**
     * @var mixed
     */
    protected $value = NULL;

    /**
     * @var array
     */
    protected $attribs = array();

    /**
     * @var string
     */
    protected $description = NULL;

    /**
     * @var string
     */
    protected $group;

    /**
     * @param string $name
     * @param string $label
     * @param $owner
     * @return FControl
     */
    public function __construct($name, $label, &$owner)
    {
        $this->id = $name;
        $this->name = $name;
        $this->label = $label;
        $this->owner = $owner;
    }

    /**
     * @param string $key - name of the called function
     * @param array $val - arguments of called function
     * @return object $this
     */
    public function __call($key, $val)
    {
        if (isset($val[0]) && $val[0] !== TRUE) {
            self::addAtrib($key, $val[0]);

            // e.g. readonly="readonly"
        } else {
            self::addAtrib($key, $key);
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return object $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }


    /**
     * @param string $group
     * @return $this
     */
    public function setGroup($group)
    {
        $this->group = $group;
        return $this;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string $desc
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @return string $html
     */
    public function getHtmlDescription()
    {
        if ($this->description === NULL) {
            return NULL;
        }

        $el = Html::el('span')
            ->setText($this->description)
            ->class('ff_desc');

        return $el->render();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed $value
     */
    public function getValue()
    {
        return $this->value;
    }


    /**
     * @return string $html
     */
    public function getLabel()
    {
        if ($this->label === NULL) {
            return NULL;
        }

        $el = HTML::el('label')
            ->for($this->name)
            ->setHtml($this->label);

        return $el->render();
    }

    /**
     * @return void
     */
    public function getControl()
    {
        return NULL;
    }

    /**
     * @param mixed $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @param string $key
     * @param string $val
     * @return $this
     */
    public function addAtrib($key, $val)
    {
        $this->attribs[strtolower($key)] = $val;
        return $this;
    }

    /**
     * @param $control
     * @param string $validator
     * @param string $err_msg
     * @param array $params
     * @throws InvalidArgumentException
     * @return object
     */
    public function getRule($control, $validator, $err_msg, $params = Array())
    {
        //try with namespace first
        $fullClassName = self::$defaultValidatorNamespace . '\\' . $validator;
        $fullClassName = class_exists($fullClassName) ? $fullClassName : $validator;
        if (!class_exists($fullClassName)) {
            throw new InvalidArgumentException(sprintf('Class %s does not exist', $fullClassName));
        }
        return new $fullClassName($err_msg, $params, $this->owner, $control);
    }

    /**
     * @param string $validator
     * @param string $err_msg
     * @param array $params
     * @return $this
     */
    public function addRule($validator, $err_msg, $params = Array())
    {
        if (is_callable($validator)) {
            $this->validators[] = array($validator, $err_msg, $params);
        } else {
            $this->validators[$validator] = $this->getRule($this, $validator, $err_msg, $params);
        }
        return $this;
    }

    /**
     * @param $validator
     */
    public function removeRule($validator)
    {
        if (is_array($validator)) {
            foreach ($this->validators as $key => $item) {
                if (is_array($item) && $item[0] === $validator) {
                    unset($this->validators[$key]);
                }
            }
        } elseif (isset($this->validators[$validator])) {
            unset($this->validators[$validator]);
        }
    }

    /**
     * @param object $control
     * @return void
     */
    protected function applyAttribs(&$control)
    {
        // insert attribs for controls
        foreach ($this->attribs as $key => $val) {
            $control->$key($val);
        }
    }
}
