<?php

class CmsImg extends CmsFile
{
    /**
     * Img browser
     */
    protected $browser = Array(
        'nodeId' => 493,
        'action' => 'imageBrowser',
        'params' => Array(
            'input_name' => NULL,
            'height'=>650,
            'width'=>755,
            'KeepThis'=>'true',
            'TB_iframe'=>'true'
        ),
    );

    /**
     * @return string $html
     */
    public function getControl()
    {
        // hidden input
        $hidden =  HTML::el('input')
            ->type('hidden')
            ->name($this->name)
            ->value($this->value)
            ->id($this->getId());

        // img tag
        $span = Html::el('span')
            ->id($this->name.'_span');

        // add
        $add = Html::el('input')
            ->type('submit')
            ->name($this->name.'_add')
            ->id($this->name.'_add')
            ->class('btn')
            ->value('Add Image')
            ->onclick(
                'tb_show("Add Image","'.$this->getBrowserUrl().'");
				return false;'
            );


        // remove
        $remove = Html::el('input')
            ->type('submit')
            ->name($this->name.'_remove')
            ->id($this->name.'_remove')
            ->class('btn')
            ->value('Remove')
            ->onclick(
                '$("#'.$this->name.'_span").html("");
				$("#'.$this->name.'").val(0);
				$("#'.$this->name.'_add").attr("style", "display: inline;");
				$("#'.$this->name.'_remove").attr("style","display: none;");
				return false;'
            );

        // if value has been set
        if ($this->value) {
            try {
                $span->add(FImage::create($this->value)->getHtmlTag('s'));
            } catch(Exception $e) {
                $span->add(Html::el('img')->src('')->alt('no image'));
            }
            $add->style('display: none;');
        } else {
            $remove->style('display: none;');
        }

        // html control in table
        $el = Html::el('table')
            ->border(0);

        $tr = $el->create('tr');

        $tr->create('td')
            ->width(155)
            ->add($span->render() . $hidden->render());

        $tr->create('td')->add($add->render());
        $tr->create('td')->add($remove->render());
        return $el->render();
    }
}
