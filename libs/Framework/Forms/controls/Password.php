<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    Password.php 2009-03-19 divak@gmail.com
 */

require_once dirname(__FILE__) . '/Text.php';

class Password extends Text
{
	protected $type = 'password';
	
	/**
	 * @return string $html
	 */
	public function getControl()
	{
		$control = HTML::el('input')
			->type($this->type)
			->name($this->name)
			->id($this->getId());
			
		self::applyAttribs($control);
		return $control->render();
	}
}
?>