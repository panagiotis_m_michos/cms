<?php

class DateSelect extends FControl
{
	protected $options = array();

	protected $controls = array('d','m','y');

	protected $firstOptions = ['d' => 'Day', 'm' => 'Month', 'y' => 'Year'];

	protected $startYear = 1980;

	protected $endYear;

	protected $canHaveJsCalendar = FALSE;

	protected $withFirstOptions = FALSE;

	/**
	 * @param string $name
	 * @param string $label
	 * @return void
	 */
	public function __construct($name,$label,&$owner)
	{
		parent::__construct($name,$label,$owner);
        $this->endYear = date('Y');
		if (!isSet($this->owner->paths['FF_PATH_R'])) {
			trigger_error('DateSelect::__construct(): You have to specify `FORMS_DIR_R` in `Ff::$paths`!', E_USER_ERROR);
		}
	}

	/**
	 * @param int $start_year
	 * @return void
	 */
	public function setStartYear($startYear)
	{
		$this->startYear = $startYear;
		return $this;
	}

	/**
	 * @param int $endYear
	 * @return void
	 */
	public function setEndYear($endYear)
	{
		$this->endYear = $endYear;
		return $this;
	}

	/**
	 * @param bool $canHaveJsCalendar
	 * @return void
	 */
	public function setCanHaveJsCalendar($canHaveJsCalendar)
	{
		$this->canHaveJsCalendar = (bool)$canHaveJsCalendar;
		return $this;
	}

	/**
	 * @param mixed $value
	 */
	public function setValue($value)
	{
		if (empty($value)) {
			if ($this->withFirstOptions) {
				$this->value = NULL;

				return $this;
			} else {
				$value =  date('Y-m-d');
			}
		}

		if (!is_Array($value)) {

			// check date format
			if (!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $value) && !preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$/', $value)) {
				trigger_error('DateSelect::setValue(): Wrong date format `'.$value.'` for '.$this->name.', should be in form `YYYY-MM-DD`', E_USER_ERROR);
			}

			$time = strtotime($value);
			$this->value = Array();
			$this->value['y'] = date('Y', $time);
			$this->value['m'] = date('m', $time);
			$this->value['d'] = date('d', $time);

		} else {
			$this->value = $value;
		}

		return $this;
	}

	/**
	 * @return array
	 */
	protected function getOptions()
	{
		// days
		for ($i=1; $i<=31; $i++) {
			$day = date('d',mktime(0,0,0,1,$i,date('Y')));
			$options['d'][$day] = $day;
		}

		// months
		for ($i=1; $i<=12; $i++) {
			$options['m'][date('m',mktime(0,0,0,$i,1,date('Y')))] = date('m',mktime(0,0,0,$i,1,date('Y')));
		}

		// check
		if ($this->endYear < $this->startYear) {
			trigger_error('$end_year is lower than $start_year', E_USER_ERROR);
		}

		// years
		for ($i=$this->startYear; $i<=$this->endYear; $i++) {
			$options['y'][$i] = $i;
		}

		return $options;
	}

	/**
	 * @return string $html
	 */
	public function getControl()
	{
		$html = '';
		$options = $this->getOptions();

		foreach ($this->controls as $key=>$val) {

			// create control
			$control = FForm::getElement('select',$this->getName().'['.$val.']')
				->setOptions($options[$val]);

			if ($this->withFirstOptions && isset($this->firstOptions[$val])) {
				$control->setFirstOption($this->firstOptions[$val]);
			}
				
			// check selected value
			if (isSet($this->value[$val])) {
				$control->setValue($this->value[$val]);
			}


			// apply attributes on it
			self::applyAttribs($control);
			$html .= $control->getControl();
		}

		// js calendar
		if ($this->canHaveJsCalendar && 1==2) { // TODO

			// includes - css, js
			if  (!$this->owner->dyncalUsed) {

				$includes = file_get_contents(dirname(__FILE__) . '/dyncal/' . 'includes.html');

				$replace = Array(
					'#DYNCAL_PATH#' => $this->owner->paths['FF_PATH_R'] . 'controls/dyncal/',
				);

				$html .= str_replace(array_keys($replace), $replace, $includes);
				$this->owner->dyncalUsed = TRUE;
			}

			// handler js
			$handler = file_get_contents(dirname(__FILE__).'/dyncal/'.'handler.html');

			$replace = Array(
				'#NAME#' => $this->name,
				'#DYNCAL_IMGS_PATH#' => $this->owner->paths['FF_PATH_R'].'controls/dyncal/images/',
				'#MONTH#' => (int)$this->value['m']-1,
				'#YEAR#' => (int)$this->value['y']
			);

			$html .= str_replace(array_keys($replace), $replace, $handler);
		}

		return $html;
	}

	/**
	 * @return string
	 */
	public function getValue()
	{
		$value = NULL;
		if (isSet($this->value['y'],$this->value['m'],$this->value['d'])) {
			$value = $this->value['y'].'-'.$this->value['m'].'-'.$this->value['d'];
		}

		return $value;
	}

	/**
	 * @return DateSelect
	 */
	public function withFirstOptions()
	{
		$this->withFirstOptions = TRUE;

		return $this;
	}
}
