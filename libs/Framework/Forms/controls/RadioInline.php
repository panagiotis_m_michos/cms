<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    RadioInline.php 2010-08-20 divak@gmail.com
 */


require_once dirname(__FILE__) . '/Radio.php';


class RadioInline extends Radio
{
	/**
	 * @param int $key
	 * @return Html
	 */
	public function getControl($key = NULL)
	{
		$el = Html::el();
		$options = $this->getOptions($key);
		foreach ($options as $key => $val) {
			// control
			$control = Html::el('input')
				->type('radio')
				->name($this->getName())
				->value($key)
				->id($this->getName() . $key);
				
			if ($this->value !== NULL && $this->value == $key) $control->checked('checked');
			if (in_array($key,$this->disabled_options)) $control->disabled('disabled');
			self::applyAttribs($control);
			$el->add($control->render());
		    
		    // label
		    $el->create('label')
		   		->setHtml($val)
		    	->for($this->getName() . $key);
		}
		return $el;
	}
	
	/**
	 * @param mixed $key
	 * @return mixed
	 */
	private function getOptions($key = NULL)
	{
		if ($key !== NULL && isset($this->options[$key])) {
			$options  = array($key => $this->options[$key]);
		} else {
			$options = $this->options;
		}
		return $options;
	}
	
}