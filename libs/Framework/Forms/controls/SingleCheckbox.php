<?php

class SingleCheckbox extends Checkbox implements IRowControl
{
    /**
	 * @return Html
	 */
	public function getLabel()
	{
        return Html::el();
	}

    /**
     * @param string $key
     * @return Html
     */
	public function getControl($key = NULL)
	{
        $el = Html::el('div')->class('single-checkbox-container');
        
        // --- control ---
        $control = $el->create('input')
            ->type('checkbox')
            ->value(1)
            ->id($this->getName())
            ->name($this->getName())
            ->class('single-checkbox-input');

        if ($this->value == 1) {
            $control->checked('checked');
        }
        self::applyAttribs($control);

        $label = HTML::el('label')
            ->setHtml($this->label)
            ->class('single-checkbox-label')
            ->for($this->getName().$key);

        // --- label ---
        $el->add($label);
        
        return $el;
	}
}
