<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    Area.php 2009-03-19 divak@gmail.com
 */

require_once dirname(__FILE__) . '/FControl.php';

class Area extends FControl
{
	/**
	 * @return string $html
	 */
	public function getControl()
	{
		$el = HTML::el('textarea')
			->name($this->name)
			->setText($this->value)
			->id($this->getId());
			
		self::applyAttribs($el);
		return $el->render();
	}
}
?>