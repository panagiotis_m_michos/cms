<?php

/**
 * DatePicker setting value changes the format
 * this class was created just to change that and add initial date
 */

class DatePickerNew extends DatePicker
{
    /**
     * {@inheritsDoc}
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
}
