<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    Radio.php 2009-03-19 divak@gmail.com
 */

require_once dirname(__FILE__) . '/FControl.php';

class Radio extends FControl
{
	protected $options = Array();
	
	protected $disabled_options = Array();
	
	/**
	 * @param misc $$options
	 */
	public function setDisabledOptions($options)
	{
		$this->disabled_options = (array)$options;
		return $this;
	}
	
	/**
	 * @param int $key
	 * @return string $html
	 */
	public function getControl($key=Null)
	{
		$html = '';
		if (!is_Null($key)) {
			$options = Array($key => $this->options[$key]);
		} else {
			$options = $this->options;
		}
		
		foreach ($options as $key=>$val) {
			
			// controls
			$control = Html::el('input')
				->type('radio')
				->name($this->getName())
				->value($key)
				->id($this->getName() . $key);
				
			// this control should be set if	
			if ((string)$this->value == $key) {
				$control->checked('checked');
			}
			
			// disabled options
			if (in_Array($key,$this->disabled_options)) {
				$control->disabled('disabled');
			}
				
			self::applyAttribs($control);
		    
		    // labels
		    $label = Html::el('label')
		   		->setHtml($val)
		    	->for($this->getName() . $key);
		    
		    $html .= $control->render() . $label->render() . HTML::el('br')->render();
		}
		return $html;
	}
	
	/**
	 * @param 
	 */
	public function setOptions($options)
	{
		$this->options = (array)$options;
		return $this;
	}
	
	/**
	 * @return string $html
	 */
	public function getLabel()
	{
		if (is_Null($this->label)) {
			return NULL;
		}
		return $this->label;
	}
}
?>
