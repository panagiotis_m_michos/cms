<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    AsmSelect.php 2009-03-19 divak@gmail.com
 */

require_once dirname(__FILE__) . '/Select.php';


class AsmSelect extends Select
{
	/** @var array */
	protected $attribs = array('multiple' => 'multiple');
	
	/**
	 * @return string $html
	 */
	public function getControl($key = NULL)
	{
		// create container
		$el = Html::el();
		$el->setHtml(self::getJavaScriptCode($this->getName()));
		
		// reorder options
		$this->options = self::getReorderedOptions((array) $this->value, (array) $this->options);
		
		// controls
		$control = $el->create('select')->id($this->getName());
		
		// is multiple
		if (isSet($this->attribs['multiple'])) {
			$control->name($this->getName().'[]');
		} else {
			$control->name($this->getName());
		}
		
		// add control
		$control->add($this->getOptions($this->options));
		self::applyAttribs($control);
		return $el;
	}
	
	
	static protected function getReorderedOptions(array $values, array $options)
	{
		$_values = array();
		foreach ($values as $key=>$val) {
			if (isset($options[$val])) $_values[$val] = $options[$val];
		}
		$diff = array_diff_key($options, $_values);
		return $diff + $_values;
	}
	
	
	static protected function getJavaScriptCode($name)
	{
		$ret = '<script type="text/javascript"> 
			$(document).ready(function() {
				$("#'.$name.'").asmSelect({
					sortable: true,
					removeLabel: \'X\'
				});
			}); 
			</script>';
		return $ret;
	}
}
