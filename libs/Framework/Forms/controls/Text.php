<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    Text.php 2009-03-19 divak@gmail.com
 */

require_once dirname(__FILE__) . '/FControl.php';

class Text extends FControl
{
	protected $type = 'text';
	
	/**
	 * @return string $html
	 */
	public function getControl()
	{
		$control = HTML::el('input')
			->type($this->type)
			->value($this->value)
			->id($this->getId());
			
		// group
		if ($this->group != NULL) {
			$control->name($this->group .'['.$this->getName().']');
		} else {
			$control->name($this->getName());
		}
		
		
		self::applyAttribs($control);
		return $control->render();
	}
	
}
