<?php

require_once dirname(__FILE__) . '/FControl.php';

class HtmlControl extends FControl
{
    protected $type = 'html';

    /**
     * @var Html
     */
    private $html;

    public function __construct($name, $label, Html $html, &$owner)
    {
        parent::__construct($name, $label, $owner);
        $this->html = $html;
    }

    /**
     * @return string $html
     */
    public function getControl()
    {
        self::applyAttribs($this->html);
        return $this->html->render();
    }
}
