<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    Reset.php 2009-03-19 divak@gmail.com
 */

require_once dirname(__FILE__) . '/Text.php';

class Reset extends Text
{
	protected $type = 'reset';
	
	/**
	 * @param string $value
	 * @retun void
	 */
	public function setValue($value)
	{
	    if (is_Null($this->value)) {
	    	$this->value = $value;
	    }
	    
		return $this;
	}
	
	/**
	 * @return void
	 */
	public function getLabel()
	{
		return NULL;
	}
	
	/**
	 * @return string $html
	 */
	public function getControl()
	{
		$control = HTML::el('input')
			->type($this->type)
			->name($this->name)
			->value($this->value)
			->id($this->getId());
			
		
		self::applyAttribs($control);
		return $control->render();
	}
}
?>