<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    Select.php 2009-03-19 divak@gmail.com
 */

require_once dirname(__FILE__) . '/Radio.php';

class Select extends Radio
{
	protected $firstOption = NULL;

	protected $options = Array();

	protected $disabled_options = Array();

	protected $firstOptionValue = '?';

	/**
	 * @param misc $$options
	 */
	public function setDisabledOptions($options)
	{
		$this->disabled_options = (array)$options;
		return $this;
	}

	/**
	 * @param string $firstOption
	 * @return $this
	 */
	public function setFirstOption($firstOption)
	{
		$this->firstOption = (string)$firstOption;
		return $this;
	}

	/**
	 * @param string $firstOptionValue
	 * @return void
	 */
	public function setFirstOptionValue($firstOptionValue)
	{
		$this->firstOptionValue = (string)$firstOptionValue;
		return $this;
	}

	/**
	 * @return string $html
	 */
	public function getControl($key=NULL)
	{
		// controls
		$control = HTML::el('select')
			->id($this->getName());

		// is multiple
		if (isSet($this->attribs['multiple'])) {
			$control->name($this->getName().'[]');
		} else {
			$control->name($this->getName());
		}

		// add control
		$control->add($this->getOptions($this->options));
		self::applyAttribs($control);
		return $control->render(0);
	}

	/**
	 * Rekurzive function
	 * @param mixed $arr
	 * @param bool $second_call
	 * @return string
	 */
	protected function getOptions($arr, $second_call=FALSE)
	{
		// first option
		$options = '';
		if (!is_Null($this->firstOption) && $second_call==FALSE) {
			$el = HTML::el('option')
				->value($this->firstOptionValue)
				->setText((string)$this->firstOption);

			// this control should be set if
			if (in_Array($this->firstOptionValue,(array)$this->value)) {
				$el->selected('selected');
			}

			$options .= $el->render(0);
		}

		// other options
		foreach ($arr as $key=>$val) {

			// only 2 dimensional array
			if ($second_call && is_Array($val)) {
				trigger_error('Select::getOptions(): Only 2-dimensional array is supported by HTML!', E_USER_ERROR);
			}

			if (is_Array($val)) {
				$options .= Html::el('optgroup')
					->label($key)
					->add($this->getOptions($val, TRUE));
			} else {

				$el = HTML::el('option')
						->value($key)
						->setText((string)$val);

				// disabled options
				if (in_Array($key,$this->disabled_options)) {
					$el->disabled('disabled');
				}

				// this control should be set if
				$values = array_flip((array)$this->value);
				if (isset($values[$key])) {
					$el->selected('selected');
				}

				$options .= $el->render(0);
			}
		}

		return $options;
	}

	/**
	 * @return string
	 */
	public function getLabel()
	{
		if (is_Null($this->label)) {
			return null;
		}

		$el = HTML::el('label')
			->for($this->name)
			->setHtml((string)$this->label);

		return $el->render();
	}

	/**
	 * @return misc $value
	 */
	public function getValue()
	{
		if ($this->value == '?') {
			return NULL;
		}
		return $this->value;
	}
}
