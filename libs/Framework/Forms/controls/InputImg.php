<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    InputImg.php 2009-03-19 divak@gmail.com
 */
require_once dirname(__FILE__) . '/InputFile.php';

class InputImg extends InputFile
{
	protected $path;
	
	/**
	 * @return string $html
	 */
	public function getControl()
	{
		if (is_Null($this->value) || empty($this->value)) {
			$control = HTML::el('input')
				->type('file')
				->name($this->name)
				->id($this->getId());

			self::applyAttribs($control);
			return $control->render();
			
		} else {
			$el = Html::el('img')->src($this->path.$this->value)->alt($this->value);
			return $el->render();
		}
	}
}
?>