<?php

interface IRowControl
{
	/**
	 * @return Html
	 */
	public function getControl();
}
