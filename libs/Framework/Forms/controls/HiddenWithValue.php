<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    Hidden.php 2009-03-19 divak@gmail.com
 */

require_once dirname(__FILE__) . '/Text.php';

class HiddenWithValue extends Text
{
	protected $type = 'hidden';
    	/**
	 * @param string $value
	 * @retun void
	 */
	public function setValue($value)
	{
	    if (is_Null($this->value)) {
	    	$this->value = $value;
	    }
		return $this;
	}
}
?>