<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    CmsFckArea.php 2009-03-19 divak@gmail.com
 */

require_once dirname(__FILE__) . '/FckArea.php';

class CmsFckArea extends FckArea
{
    protected $toolbar = 'Cms';

    protected $lang = 'en';

    protected $fck_config = Array(
        'DefaultLanguage' => 'en',
        'ProcessHTMLEntities' => FALSE,
        'AutoDetectLanguage' => FALSE,
        'StartupShowBlocks' => FALSE,
        'ToolbarStartExpanded' => TRUE,
        'ImageBrowserURL' => NULL,
        'ImageBrowserWindowWidth' => NULL,
        'ImageBrowserWindowHeight' => NULL,
        'ImageUpload' => FALSE,
        'LinkBrowserURL' => NULL,
        'LinkBrowserWindowWidth' => NULL,
        'LinkBrowserWindowHeight' => NULL,
        'FlashBrowserURL' => NULL,
        'FlashBrowserWindowWidth' => NULL,
        'FlashBrowserWindowHeight' => NULL,
        'ToolbarSets' => Array(
            'cms'=> Array(
                "[
                    ['Source'],
                    ['FitWindow','ShowBlocks'],
                    ['PasteText','PasteWord','-','SpellCheck'],
                    ['Undo','Redo','-','Anchor'],
                    ['FontFormat','Image'],
                    '/',
                    ['Bold','Italic','Underline','-','Subscript','Superscript'],
                    ['OrderedList','UnorderedList','-','Outdent','Indent'],
                    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyFull'],
                    ['Link','Unlink'],
                    ['Table','Rule','SpecialChar']
                ]"
            )
        )
    );

    /**
     * Img browser
     */
    protected $img_browser = Array(
        'nodeId' => 493,
        'action' => 'imageBrowser',  
        'params' => Array(
            'fck' => 1,
        ),
        'size' => Array(780,650),
    );

    /**
     * File browser
     */
    protected $file_browser = Array(
        'nodeId' => 179,
        'action' => 'fileBrowser',
        'params' => Array(
            'fck' => 1,
        ),
        'size' => Array(780,650),
    );

    /**
     * File browser
     */
    protected $flash_browser = Array(
        'nodeId' => NULL,
        'action' => 'flashBrowser',
        'params' => Array(
            'fck' => 1,
        ),
        'size' => Array(780,650),
    );

    /**
     * @param string $name
     * @param string $label
     * @return void
     */
    public function __construct($name,$label,&$owner)
    {
        parent::__construct($name,$label,$owner);

        if (!isSet($this->owner->paths['URL_ROOT'])) {
            trigger_error('FckArea::__construct(): You have to specify `URL_ROOT` in `Ff::$paths`!', E_USER_ERROR);
        }
    }

    /**
     * @param int $img_galery_id
     * @return void
     */
    public function setImgGaleryId($img_galery_id)
    {
        $this->img_browser['node_id'] = (int)$img_galery_id;
        return $this;
    }

    /**
     * @param int $file_galery_id
     * @return void
     */
    public function setFileGaleryId($file_galery_id)
    {
        $this->file_browser['node_id'] = (int)$file_galery_id;
        return $this;
    }

    /**
     * @param int $flash_galery_id
     * @return void
     */
    public function setFlashGaleryId($flash_galery_id)
    {
        $this->flash_browser['node_id'] = (int)$flash_galery_id;
        return $this;
    }

    /**
     * @return string $html
     */
    public function getControl()
    {
        include_once COMPONENT_DIR . '/fck/fckeditor_php5.php';

        $editor = new FCKeditor($this->name);
        $editor->BasePath = '/components/fck/';
        $editor->Width = $this->width;
        $editor->Height = $this->height;
        $editor->Value = $this->value;

        if (!is_Null($this->css)) {
            $editor->Config['EditorAreaCSS'] = $this->css;
        }

        // TODO use config
        $editor->Config['DefaultLanguage'] = $this->lang;
        $editor->ToolbarSet	= $this->toolbar;
        $editor->Config['ProcessHTMLEntities'] = FALSE;
        $editor->Config['AutoDetectLanguage'] = FALSE;
        $editor->Config['StartupShowBlocks'] = FALSE;
        $editor->Config['ToolbarStartExpanded'] = TRUE;

        // image browser
        $editor->Config['ImageBrowserURL'] = $this->getBrowserUrl('img');
        $editor->Config['ImageBrowserWindowWidth'] = $this->img_browser['size'][0];
        $editor->Config['ImageBrowserWindowHeight'] = $this->img_browser['size'][1];
        $editor->Config['ImageUpload'] = false;

        // file browser
        $editor->Config['LinkBrowserURL'] = $this->getBrowserUrl('file');
        $editor->Config['LinkBrowserWindowWidth'] = $this->file_browser['size'][0];
        $editor->Config['LinkBrowserWindowHeight'] = $this->file_browser['size'][1];

        // flash browser
        $editor->Config['FlashBrowserURL'] = $this->getBrowserUrl('flash');
        $editor->Config['FlashBrowserWindowWidth'] = $this->flash_browser['size'][0];
        $editor->Config['FlashBrowserWindowHeight'] = $this->flash_browser['size'][1];

        return $editor->CreateHtml();
    }

    /**
     * @param string $browser
     */
    protected function getBrowserUrl($browser)
    {
        $config = $this->{$browser.'_browser'};
        $nodeId = $config['nodeId'];
        $action = $config['action'];
        $params = $config['params'];	
        $url = FApplication::$router->link("$nodeId $action", $params);
        return $url;
    }
}
