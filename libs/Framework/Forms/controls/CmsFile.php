<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    CmsFile.php 2009-03-19 divak@gmail.com
 */

require_once dirname(__FILE__) . '/InputFile.php';

class CmsFile extends InputFile
{
	/**
	 * File browser
	 */
	protected $browser = Array(
		'nodeId' => 179,
		'action' => 'fileBrowser',
		'params' => Array(
			'input_name' => NULL,
			'height'=>600,
			'width'=>755,
			'KeepThis'=>'true',
			'TB_iframe'=>'true'
		),
	);
	
	/**
	 * @param int $nodeId
	 * @return void
	 */
	public function setNodeId($nodeId)
	{
		$this->browser['nodeId'] = (int)$nodeId;
		return $this;
	}
	
	/**
	 * @return string $html
	 */
	public function getControl()
	{
		// hidden input
		$hidden =  HTML::el('input')
			->type('hidden')
			->name($this->name)
			->value($this->value)
			->id($this->getId());
		
		// img tag
		$span = Html::el('span')
			->id($this->name.'_span');
			
		// add
		$add = Html::el('input')
			->type('submit')
			->name($this->name.'_add')
			->id($this->name.'_add')
			->class('btn')
			->value('Add File')
			->onclick('
				tb_show("Add File","'.$this->getBrowserUrl().'");
				return false;');	
	 
			
		// remove
		$remove = Html::el('input')
			->type('submit')
			->name($this->name.'_remove')
			->id($this->name.'_remove')
			->class('btn')
			->value('Remove')
			->onclick('
				$("#'.$this->name.'_span").html("");
				$("#'.$this->name.'").val(0);
				$("#'.$this->name.'_add").attr("style", "display: inline;");
				$("#'.$this->name.'_remove").attr("style","display: none;");
				return false;
				'
			);

		// value has been set
		if ($this->value) {
			try {
				$file = new FFile($this->value);
				$a = Html::el('a')
					->href($file->getFilePath(TRUE))
					->style('text-decoration: underline;')
					->target('_blank')
					->setText($file->getFileName());
				$span->add($a->render());
				$add->style('display: none;');
			} catch (Exception $e) {
				
			}
		} else {
			$remove->style('display: none;');
		}
			
		// html control in table
		$el = Html::el('table')
			->border(0);
			
		$tr = $el->create('tr');

		$tr->create('td')
			->width(155)
			->add($span->render() . $hidden->render());
			
		$tr->create('td')->add($add->render());
		$tr->create('td')->add($remove->render());
		return $el->render();
	}
	
	/**
	 * @return misc $value
	 */
	public function getValue()
	{
		return $this->value;
	}
	
	/**
	 * @return string
	 */
	protected function getBrowserUrl()
	{
		$nodeId = $this->browser['nodeId'];
		$action = $this->browser['action'];
		
		if ($this->getGroup() !== NULL) {
			throw new Exception('Groups are not supported');
		}
		
		$this->browser['params']['input_name'] = $this->getName();
		$params = $this->browser['params'];
		$url = FApplication::$router->link("$nodeId $action", $params);
		return $url;
	}
}
