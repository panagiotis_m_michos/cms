<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    Captcha.php 2009-03-19 divak@gmail.com
 */

require_once dirname(__FILE__) . '/Text.php';

class Captcha extends Text
{
    protected $value = Array();

    protected $description = '(Match characters in the picture)';

    protected $config = Array(
        'path' => '/captcha/get_captcha_img.php',
        'params' => Array(
            'width' => 100,
            'height' => 40,
            'characters' => 5,
            'owner_class' => NULL,
            'owner_name' => NULL,
            'control'=> NULL
        )
    );

    /**
     * @param string $name
     * @param string $label
     * @return void
     */
    public function __construct($name,$label,&$owner)
    {
        parent::__construct($name,$label,$owner);

        if (!isSet($this->owner->paths['FF_PATH_R'])) {
            trigger_error('Captcha::__construct(): You have to specify `FILES_PATH_R` in `Ff::$paths`!', E_USER_ERROR);
        }
    }

    /**
     * @param int $width
     * @return void
     */
    public function setWidth($width=Null)
    {
        if (!is_Null($width) && (int)$width > 0) {
            $this->config['params']['width'] = (int)$width;
        }
    }

    /**
     * @param int $height
     * @return void
     */
    public function setHeight($height=Null)
    {
        if (!is_Null($height) && (int)$height > 0) {
            $this->config['params']['height'] = (int)$height;
        }
    }

    /**
     * @param int $characters
     * @return void
     */
    public function setCharacters($characters=Null)
    {
        if (!is_Null($characters) && (int)$characters > 0) {
            $this->config['params']['characters']= (int)$characters;
        }		
    }

    /**
     * @return string $html
     */
    public function getControl()
    {
        $html = Html::el('img')
            ->class('captcha_img')
            ->src($this->getImgSrc());

        $html .= Html::el('input')
            ->id($this->name)
            ->name($this->name);

        self::applyAttribs($html);
        return $html;
    }

    /**
     * @return string
     */
    protected function getImgSrc()
    {
        // add session name
        $this->config['params']['session_name'] = session_name();
        $this->config['params'][session_name()] = session_id();
        $this->config['params']['session_save_path'] = session_save_path();

        // owner class name
        $this->config['params']['owner_class'] = get_class($this->owner);

        // owner instance name
        $this->config['params']['owner_name'] = $this->owner->name;

        // name of this control
        $this->config['params']['control'] = $this->name;
        $src = FTools::addUrlParam($this->config['params'], COMPONENT_URL . $this->config['path']);
        $src = str_replace('&amp;','&',$src);
        return $src;
    }


    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        if (isSet($_SESSION[get_class($this->owner)][$this->owner->name]['captcha'][$this->name])) {
            $this->value = Array(
                'value' => $value,
                'img_value' => $_SESSION[get_class($this->owner)][$this->owner->name]['captcha'][$this->name], 
            );
        }

        return $this;
    }
}
?>
