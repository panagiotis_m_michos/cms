<?php

class DatePicker extends Text
{
    /**
     * Attribs for html
     * 
     * @var array
     */
    protected $attribs = array(
        'size' => 10
    );


    /**
     * Options for datepicker
     * 
     * @var array
     */
    protected $options = array(
        'dateFormat' => 'dd-mm-yy',
        'showButtonPanel' => 'true',
        'changeMonth' => 'true',
        'changeYear' => 'true',
        'minDate' => NULL, //there is a bug with string dates
        'maxDate' => NULL,
        'firstDay' => 1,
        'yearRange' => '-10:+10'
    );


    /**
     * Provide setting option
     *
     * @param string $key
     * @param string $value
     */
    public function setOption($key, $value)
    {
        //if (!isset($this->options[$key])) {
        //	throw new Exception("Option $key doesn't exist");
        //}
        $this->options[$key] = $value;
    }


    /**
     * Date value
     * If format is wrong will use today's day
     *
     * @param string $value
     * @return $this|void
     */
    public function setValue($value)
    {
        if ($value) {
            if (strtotime($value) === FALSE) {
                $this->value = date('Y-m-d');
            } else {
                $this->value = date('Y-m-d', strtotime($value));
            }
        }
    }

    /**
     * new set date in case d-m-y
     * 
     *
     * @param string $value
     */
    public function setValueDMY($value)
    {
        $this->value = date('d-m-Y', strtotime($value));
    }

    /**
     * returns value
     *
     * @return string $this->value
     */
    public function getValue()
    {
        return $this->value;
    }


    /**
     * Returns control: text field and datepicker
     *
     * @return Html
     */
    public function getControl()
    {
        // container for this control
        $el = Html::el();
        $el->add($this->getJSCode());
        $control = HTML::el('input')->type($this->type)->value($this->value)->id($this->getId());
        $name = ($this->group !== NULL) ? sprintf('%s[%s]', $this->group, $this->getName()) : $this->getName();
        $control->name($name);
        self::applyAttribs($control);
        $el->add($control->render());
        return $el;
    }


    /**
     * Returns js code for datepicker
     *
     * @return HTML
     */
    protected function getJSCode()
    {
        $el = Html::el('script')->type('text/javascript');
        $el->add(
            '$(document).ready(function (){
                $("#'.$this->getId().'")
                    .datepicker('.$this->getJSOptions().')
            });'
        );
        return $el;
    }


    /**
     * Convert php array option to JSON options
     *
     * @return string
     */
    protected function getJSOptions()
    {
        if ($this->getValue()) {
            $this->options['setDate'] = $this->getValue();
        }
        return json_encode($this->options);
    }
}
