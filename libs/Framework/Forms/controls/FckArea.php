<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    FckArea.php 2009-03-19 divak@gmail.com
 */

require_once dirname(__FILE__) . '/FControl.php';

class FckArea extends FControl
{
    protected $width = '100%';

    protected $height = '200';

    protected $lng = 'en';

    protected $toolbar = 'Basic';

    protected $css = NULL;

    /**
     * @param string $name
     * @param string $label
     * @return void
     */
    public function __construct($name,$label,&$owner)
    {
        parent::__construct($name,$label,$owner);

        if (!isSet($this->owner->paths['FF_PATH_R'])) {
            trigger_error('FckArea::__construct(): You have to specify `FF_PATH_R` in `Ff::$paths`!', E_USER_ERROR);
        }
    }

    /**
     * @param string $path
     * @return void
     */
    public function setPath($path)
    {
        $this->path = (string)$path;
        return $this;
    }

    /**
     * @param string $width
     * @param string $height
     * @return void
     */
    public function setSize($width, $height)
    {
        $this->width = $width;
        $this->height = $height;
        return $this;
    }

    /**
     * @param string $lng
     * @return void
     */
    public function setLng($lng)
    {
        $this->lng = $lng;
        return $this;
    }

    /**
     * @param string $toolbar
     * @return void
     */
    public function setToolbar($toolbar)
    {
        $this->toolbar = $toolbar;
        return $this;
    }

    /**
     * @param string $css
     * @return void
     */
    public function setCss($css)
    {
        $this->css = $css;
        return $this;
    }

    /**
     * @return string $html
     */
    public function getControl()
    {
        include_once  COMPONENT_DIR . '/fck/fckeditor_php5.php';

        $editor = new FCKeditor($this->name);
        $editor->BasePath = '/components/fck/';
        $editor->Width = $this->width;
        $editor->Height = $this->height;
        $editor->Value = $this->value;
        $editor->Config['DefaultLanguage'] = $this->lng;
        $editor->ToolbarSet	= $this->toolbar;

        if (!is_Null($this->css)) {
            $editor->Config['EditorAreaCSS'] = $this->css;
        }

        return $editor->CreateHtml();
    }

    /**
     * @param string $value
     * @return void
     */
    public function setValue($value)
    {
        $this->value = stripslashes($value);
        return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        $this->value = stripslashes($this->value);
        return $this->value;
    }
}
?>
