<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    MinLength.php 2009-03-19 divak@gmail.com
 */

require_once dirname(__FILE__) . '/Gt.php';

class MinLength extends Gt
{
	/**
	 * @return boolean
	 */
	public function isValid($value)
	{
		if (empty($value)) {
			return TRUE;
		}
		
		if ((strlen($value) < $this->params[0])) {
			return FALSE;
		} else {
			return TRUE;
		}
	}
}
?>