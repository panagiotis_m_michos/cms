<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    Eq.php 2009-03-19 divak@gmail.com
 */

require_once dirname(__FILE__) . '/Validator.php';

class Eq extends Validator
{
	public function __construct($err_msg, $params = array(), $owner, $control)
	{
		parent::__construct($err_msg,$params,$owner, $control);
		
		// check added compare control
		if (empty($this->params)) {
			trigger_error('Eq::__construct(): You have to add compare control as param!', E_USER_ERROR);
		}
		
		// check if contorol is exists
		if (!isSet($this->owner[$this->params[0]])) {
			trigger_error('Eq::__construct(): Compare control: '.$this->params[0].' doesn\'t exists!', E_USER_ERROR);
		}
	}
	
	
	/**
	 * @return boolean
	 */
	public function isValid($value)
	{
		// if is control which shoudl be compare - valid
		if ($this->owner->getError($this->params[0])) {
			return TRUE;	
		}
		
		if ($value !== $this->owner->elements[$this->params[0]]->getValue()) {
			return FALSE;
		} else {
			return TRUE;
		}
	}
}
?>