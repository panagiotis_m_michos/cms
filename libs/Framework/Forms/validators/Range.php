<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    Gt.php 2009-03-19 divak@gmail.com
 */

require_once dirname(__FILE__) . '/Validator.php';

class Range extends Validator
{
	/**
	 * @param
	 * @return void
	 */
	public function __construct($err_msg, $params=Array(),$owner, $control)
	{
		parent::__construct($err_msg, $params,$owner, $control);
		
		if (is_Array($params) && empty($params)) {
			trigger_error('Range::__construct(): missing param!', E_USER_ERROR);
		}
		
		// replace value in err_msg
		$this->err_msg = str_replace('#1', $this->params[0], $this->err_msg);
		$this->err_msg = str_replace('#2', $this->params[1], $this->err_msg);
	}
	
	/**
	 * @return boolean
	 */
	public function isValid($value)
	{
		if ((int)$value >= $this->params[0] && (int)$value <= $this->params[1]) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}
?>