<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    RegExp.php 2009-07-21 divak@gmail.com
 */

require_once dirname(__FILE__) . '/Validator.php';

class RegExp extends Validator
{
	/**
	 * @param string $value
	 * @return boolean
	 */
	public function isValid($value)
	{
		$condition = isset($this->params[1]) ? (bool) $this->params[1] : FALSE; 
		if (!empty($value) && preg_match($this->params[0], $value) == $condition) {
			return FALSE;
		} 
		return TRUE;
	}
}
