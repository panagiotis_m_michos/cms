<?php

require_once dirname(__FILE__) . '/Validator.php';

class Email extends Validator
{
    /**
     * @param string $value
     * @return bool
     */
    public function isValid($value)
    {
        if (empty($value)) {
            return TRUE;
        } else {
            return empty(FApplication::$container->get('validation_module.email_validator')->validate($value));
        }
    }
}
