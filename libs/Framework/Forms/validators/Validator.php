<?php

abstract class Validator
{
    public $err_msg;

    protected $params = array();

    protected $owner;

    /**
     * @param
     * @return void
     */
    public function __construct($err_msg, $params = array(), $owner, $control)
    {
        $this->err_msg = $err_msg;
        $this->params = (array) $params;
        $this->control = $control;
        $this->owner = $owner;
    }

    /**
     * @return boolean
     */
    public function isValid($value)
    {
        return TRUE;
    }
}
