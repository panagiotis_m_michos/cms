<?php

require_once dirname(__FILE__) . '/Validator.php';

class MimeType extends Validator
{
    /* types */

    const PDF = 'PDF';
    const EXCEL = 'application/vnd.ms-excel';
    const DOC = 'application/msword';
    const DOCX = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
    const TXT = 'text/plain';
    const JPG = 'image/jpg';
    const JPEG = 'image/jpeg';
    const PNG = 'image/png';
    const CSV = 'text/csv';

    /**
     * types arrays
     * @var array
     */
    private static $mimeTypes = array(
        self::PDF => array('application/pdf', 'application/x-pdf', 'application/acrobat', 'application/x-download', 'applications/vnd.pdf', 'text/pdf', 'text/x-pdf'),
    );

    /**
     * @return boolean
     */
    public function isValid($value)
    {
        // combine plain mime types with types arrays
        $allowedMimeTypes = array();

        foreach ($this->params as $param) {
            if (empty(self::$mimeTypes[$param])) {
                $allowedMimeTypes[] = $param;
            } else {
                $allowedMimeTypes = array_merge($allowedMimeTypes, self::$mimeTypes[$param]);
            }
        }

        if ($value == NULL) {
            return TRUE;
        } elseif (!isset($value['type'])) {
            return FALSE;
        } else if (in_array($value['type'], $allowedMimeTypes, TRUE)) {
            return TRUE;
            // using e.g. "image/*"
        } elseif (in_array(preg_replace('#/.*#', '/*', $value['type']), $allowedMimeTypes, TRUE)) {
            return TRUE;
        }
        return FALSE;
    }

}
