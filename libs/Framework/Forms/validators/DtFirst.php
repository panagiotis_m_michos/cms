<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    DtFirst.php 2009-03-19 divak@gmail.com
 */

require_once dirname(__FILE__) . '/Validator.php';

class DtFirst extends Validator
{
	/**
	 * @return boolean
	 */
	public function isValid($value)
	{
		if (strtotime($value) < mktime(0,0,0,date('m'),date('d'),date('Y'))) {
			return FALSE;
		} else {
			return TRUE;
		}
	}
}
?>