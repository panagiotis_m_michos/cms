<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    Required.php 2009-03-19 divak@gmail.com
 */

require_once dirname(__FILE__) . '/Validator.php';

class Required extends Validator
{
	/**
	 * @return boolean
	 */
	public function isValid($value)
	{
		# ? is for dropdown
		if ($value == '' || $value == '?') {
			return FALSE;
		} else { 
			return TRUE;
		}
	}
}
?>