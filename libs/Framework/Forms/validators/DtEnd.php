<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    DtEnd.php 2009-03-19 divak@gmail.com
 */

require_once dirname(__FILE__) . '/Validator.php';

class DtEnd extends Validator
{
	/**
	 * @return boolean
	 */
	public function isValid($value)
	{
		if ((bool)$this->owner->getValue('is_dt_sensitive')) {
			
			// have to be bigger than today
			if ($this->owner->getValue('dt_end') < date('Y-m-d')) {
				$this->err_msg = $this->err_msg[0];
				return FALSE;
			}
			
			// have to be bigger than today
			if ($this->owner->getValue('dt_end') < $this->owner->getValue('dt_start')) {
				$this->err_msg = $this->err_msg[1];
				return FALSE;
			}
		}
		
		return TRUE;
	}
}
?>