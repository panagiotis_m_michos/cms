<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    Phone.php 2009-03-19 divak@gmail.com
 */

require_once dirname(__FILE__) . '/Validator.php';

class Phone extends Validator
{
	/**
	 * @return boolean
	 */
	public function isValid($value)
	{
		if (empty($value)) {
			return TRUE;
		} else {
			return self::checkPhone($value);
		}
	}

	/**
	 * @param string $phone
	 * @return bool
	 */
	static public function checkPhone($phone)
	{
		return preg_match('/^[\(+\d]{1}[\(\)+\s\d]+$/', $phone);
	}
}
?>