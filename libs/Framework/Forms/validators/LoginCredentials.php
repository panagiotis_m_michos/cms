<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    Phone.php 2009-03-19 divak@gmail.com
 */

require_once dirname(__FILE__) . '/Validator.php';

class LoginCredentials extends Validator
{
    	protected $errors = Array(
		1 => 'Email not found!',
		2 => 'Password doesn\'t match!',
		3 => 'Your account haven\'t been validated!',
		4 => 'Your account is blocked. Please contact our customer supoort line on 0207 608 5500',
	);
     
        
    /**
	 * @return boolean
	 */
	public function isValid($password)
	{
        try {
          $control = $this->control;
		  $user = Customer::doAuthenticate($control->owner['email']->getValue(), $password);
		  return TRUE;
		} catch (Exception $e) {//pr($e->getMessage());exit;
            if ($e->getMessage() == 1) {
            	$this->err_msg = $this->errors['1'];
            	return FALSE;
            } elseif ($e->getMessage() == 2) {
            	$this->err_msg = $this->errors['2'];
            	return FALSE;
            } elseif ($e->getMessage() == 3) {
            	$this->err_msg = $this->errors['3'];
            	return FALSE;
            } else {
            	$this->err_msg = $this->errors['4'];
            	return FALSE;
            }
		}
    }
}
?>