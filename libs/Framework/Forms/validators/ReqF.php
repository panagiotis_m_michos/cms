<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    ReqF.php 2009-03-19 divak@gmail.com
 */

require_once dirname(__FILE__) . '/Validator.php';

class ReqF extends Validator
{
	protected $errors = Array(
		1 => 'The uploaded file exceeds the upload_max_filesize directive in php.ini.',
		2 => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
		3 => 'The uploaded file was only partially uploaded.',
		6 => 'Missing a temporary folder. Introduced in PHP 4.3.10 and PHP 5.0.3.',
		7 => 'Failed to write file to disk. Introduced in PHP 5.1.0.',
		8 => 'File upload stopped by extension. Introduced in PHP 5.2.0.',
	);
	
	/**
	 * @return boolean
	 */
	public function isValid($value)
	{
		if(empty($value) || $value['error'] == 4) {
			return FALSE;
			
		} elseif($value['error'] != 0) {
			$this->err_msg = $this->errors[$value['error']];
			return FALSE;
			
		} elseif($value['error'] === 0) {
			return TRUE;
			
		} else {
			return FALSE;
		}
	}
}
?>