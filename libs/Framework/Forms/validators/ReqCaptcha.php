<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    ReqCaptcha.php 2009-03-19 divak@gmail.com
 */

require_once dirname(__FILE__) . '/Validator.php';

class ReqCaptcha extends Validator
{
	/**
	 * @return boolean
	 */
	public function isValid($value)
	{
		if (empty($value['value'])) {
			$this->err_msg = $this->err_msg[0];
			return FALSE;
		} elseif ($value['value'] != $value['img_value']) {
			$this->err_msg = $this->err_msg[1];
			return FALSE;
		}
		return TRUE;
	}
}
?>