<?php

class InlineSubmitRenderer extends DefaultRender
{
    public function getHtml()
    {
        $el = Html::el();

        $errorInfo = $this->getHtmlErrorsInfo();
        if ($errorInfo) {
            $el->add($errorInfo);
        }

        $el->add($this->getBegin());

        foreach ($this->ff->groups as $groupId => $group) {

            $container = $this->addContainer($el, $group, $groupId);
            $table = $container->create('table')->class('ff_table');

            $submits = array();
            foreach ($group['elements'] as $elementName) {

                $element = $this->ff->elements[$elementName];
                if ($element instanceof Hidden || $element instanceof Submit) {
                    if ($element instanceof Submit) {
                        $submits[] = $element;
                    }
                    continue;
                }

                $tr = $table->create('tr');

                $label = $this->ff->getLabel($elementName);
                if ($label) {
                    $tr->create('th')
                        ->add($label);
                }

                $control = $this->ff->getControl($elementName);
                if ($control) {
                    $td = $tr->create('td')
                        ->add($control);
                }

                $description = $element->getHtmlDescription();
                if ($description) {
                    $td->add($description);
                }

                $error = $this->getHtmlError($elementName);
                if ($error) {
                    $tr->create('td')->add($error);
                }

            }

            if ($submits) {
                $tr = $table->create('tr');
                $tr->create('th');
                $td = $tr->create('td');
                foreach ($submits as $submit) {
                    $td->add($submit->getControl());
                }
            }
        }

        $el->add($this->getEnd());
        return $el->render();
    }

    /**
     * @param Html $el
     * @param array $group
     * @param int $groupId
     * @return Html
     */
    private function addContainer(Html $el, array $group, $groupId)
    {
        if ($group['type'] == 'fieldset') {
            $container = $el->create('fieldset')
                ->id("fieldset_$groupId");
            $container->create('legend')->setText($group['legend']);
        } else {
            $container = $el->add(Html::el());
        }
        return $container;
    }
}
