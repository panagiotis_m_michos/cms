<?php

class DataGridFormRenderer extends DefaultRender
{

    /**
     * @return string
     */
    public function getBody()
    {
        $el = Html::el();
        $this->addElements($el);
        $this->addSubmit($el);
        return (string) $el;
    }

    /**
     * @return string
     */
    public function getHtml()
    {
        $form = Html::el('form');
        $this->addBegin($form);
        $this->addErrorMessage($form);
        $this->addElements($form);
        $this->addEnd($form);
        return (string) $form;
    }

    /**
     * @param Html $form
     */
    private function addBegin(Html $form)
    {
        $form->action($this->ff->action)->method($this->ff->method)->name($this->ff->name);
        if ($this->ff->hasEnctype())
            $form->enctype($this->ff->enctype);
    }

    /**
     * @param Html $form
     */
    private function addErrorMessage(Html $form)
    {
        $errors = $this->ff->errors;
        if (!empty($errors)) {
            $form->create('div')
                ->class('flash error')
                ->setHtml(str_replace('[count]', count($errors), $this->error_info_msg));
        }
    }

    /**
     * @param Html $form
     */
    private function addElements(Html $form)
    {
        foreach ($this->ff->groups as $group) {
            if ($group['type'] === 'fieldset') {
                $fieldset = $form->create('fieldset');
                $fieldset->create('legend')->setText($group['legend']);
                $table = $fieldset->create('table')->class('form-table');

                // --- elements ---
                foreach ($group['elements'] as $key => $name) {
                    /* @var $element FControl */
                    $element = $this->ff[$name];
                    $className = $element->getReflection()->getName();

                    if ($element instanceof Hidden || $element instanceof Submit || $element instanceof Reset) {
                        continue;
                    }

                    $tr = $table->create('tr');
                    $label = $element->getLabel() ? $element->getLabel() : '';
                    $tr->create('th')->add($label);
                    $tr->create('td')->add($element->getControl());
                }

                // ---- submits ---
                $tr = $table->create('tr');
                $th = $tr->create('th')->colspan(2);
                foreach ($this->ff->getElements() as $element) {
                    if ($element instanceof Submit || $element instanceof Reset) {
                        $th->add($element->getControl());
                    }
                }
            }
        }
    }

    /**
     * @param Html $form
     */
    private function addEnd(Html $form)
    {
        /* @var $element FControl */
        foreach ($this->ff->elements as $element) {
            if ($element instanceof Hidden) {
                $control = $element->getControl();
                $form->add($control);
            }
        }
    }

}
