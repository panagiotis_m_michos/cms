<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    DefaultRender.php 2009-03-19 divak@gmail.com
 */

require_once dirname(__FILE__) . '/IRender.php';

class DefaultRender implements IRender
{
	protected $ff;
	
	public $control_err_class = 'ff_control_err';
	
	public $class_ff_errors = 'ff_errors';
	
	public $error_info_msg = 'Form has <b>[count]</b> error(s)';
	
	/**
	 * @param object $ff
	 * @return void
	 */
	public function __construct($ff)
	{
		$this->ff =& $ff;
	}
	
	/**
	 * @param string $class
	 * @return void
	 */
	public function setControlErrClass($class)
	{
		$this->control_err_class = (string)$class;
		return $this;
	}

	/**
	 * @return string $html
	 */
	public function getBegin()
	{
		$el = Html::el('form')
			->action($this->ff->action)
			->method($this->ff->method)
			->name($this->ff->name);
			
		// only if should be there - files
		if($this->ff->hasEnctype()) {
			$el->enctype($this->ff->enctype);
		}

		return $el->startTag();
	}

	/**
	 * @return string $html
	 */
	public function getEnd()
	{
		// hidden inputs
		$html='';
		foreach ($this->ff->elements as $key=>$val) {
			
			// hidden inputs at the end of form
			if (get_class($val) != 'Hidden') {
				continue;
			}

			$html .= $this->ff->getControl($val->getName());
		}

		$html .= Html::el('form')->endTag();
		return $html;
	}

	/**
	 * @return string $html
	 */
	public function getHtmlErrors()
	{
		// has any errors
		if (!$this->ff->hasErrors()) {
			return NULL;
		}

		$el = Html::el('ul')->class($this->class_ff_errors);

		foreach ($this->ff->getErrors() as $key=>$val) {
			$el->create('li')->setText($val);
		}

		return $el;
	}

	/**
	 * @return string $html
	 */
	public function getHtmlErrorsInfo()
	{
		if (!empty($this->ff->errors)) {
			$el = Html::el('p')
			->class('ff_err_notice')
			->setHtml(str_replace('[count]', count($this->ff->errors),$this->error_info_msg));
			return $el->render(0);
		}

		return NULL;
	}

	/**
	 * @param string $return
	 * @return string $html
	 */
	public function getHtmlError($control)
	{
		$error = $this->ff->getError($control);
		
		if (empty($error)) {
			return;
		}

		$el = Html::el('span')
			->class($this->control_err_class)
			->setHtml((string)$error);
			
		return $el->render(0);
	}
	
	/**
	 * @return string
	 */
	public function getHtml()
	{
		// info about errors
		$html = $this->getHtmlErrorsInfo();
			
		// begin of form
		$html .= $this->getBegin();
		
		foreach ($this->ff->groups as $key=>$val) {
			
			// inputs
			$el = Html::el('table')
				->class('ff_table');

			foreach ($val['elements'] as $key2=>$val2) {
				$control = $this->ff->elements[$val2];
					
				// hidden inputs at the end of form
				if (get_class($control) == 'Hidden') {
					continue;
				}

				$tr = $el->create('tr');

				if ($control instanceof IRowControl) {
					$td = $tr->create('td')->colspan(2);

					$td->add((string) $this->getHtmlError($val2));
					$td->add($control->getControl());
				} else {
					// label
					$tr->create('th')
						->add((string) $this->ff->getLabel($val2));

					// control
					$td = $tr->create('td')
						->add((string) $this->ff->getControl($val2));

					// description
					if (!is_Null($control->getDescription())) {
						$td->add($control->getHtmlDescription());
					}

					// error message
					$tr->create('td')
						->add((string) $this->getHtmlError($val2));
				}
			}

			// no childrens - no table
			$children = $el->getChildren();

			if (empty($children)) {
				continue;
			}
			
			// fieldset
			if ($val['type'] == 'fieldset') {
				$legend = Html::el('legend')
					->setText($val['legend']);

				$fieldset = Html::el('fieldset')
					->id('fieldset_'.$key)
					->add($legend)
					->add($el->render(0));
					

				$html .= $fieldset->render(0);
				
			// plain
			} else {
				$html  .= $el->render(0);
			}
		}


		// end of form
		$html .= $this->getEnd();
		return $html;
	}
}
