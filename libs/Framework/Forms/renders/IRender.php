<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    IRender.php 2009-03-19 divak@gmail.com
 */

interface IRender
{
	/**
	 * @return string
	 */
	public function getBegin();

	/**
	 * @return string
	 */
	public function getEnd();
	
	/**
	 * @return string
	 */
    public function getHtmlErrors();
    
    /**
	 * @return string
	 */
    public function getHtmlErrorsInfo();
    
    /**
	 * @return string
	 */
    public function getHtml();

	/**
	 * @param string $control
	 * @return string
	 */
	public function getHtmlError($control);
}
