<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    Default2Render.php 2009-03-19 divak@gmail.com
 */


class Default2Render extends DefaultRender
{
	/**
	 * @return string
	 */
	public function getHtml()
	{
		// info about errors
		$html = $this->getHtmlErrorsInfo();
			
		// begin of form
		$html .= $this->getBegin();
		
		foreach ($this->ff->groups as $key=>$val) {
			
			// inputs
			$el = Html::el('table')
				->class('ff_table');

			foreach ($val['elements'] as $key2=>$val2) {
				$control = $this->ff->elements[$val2];
					
				// hidden inputs at the end of form
				if (get_class($control) == 'Hidden') {
					continue;
				}

				$tr = $el->create('tr');
				
				if (get_class($control) ===  'Submit') {
					// label
					$tr->create('th')
						->add((string)$this->ff->getControl($val2));
	
					// control
					$td = $tr->create('td')
						->add((string) $this->ff->getLabel($val2));
				} else {
					// label
					$tr->create('th')
						->add((string)$this->ff->getLabel($val2));
	
					// control
					$td = $tr->create('td')
						->add((string)$this->ff->getControl($val2));
				}

				// description
				if (!is_Null($control->getDescription())) {
					$td->add($control->getHtmlDescription());
				}

				// error message
				$tr->create('td')
					->add((string)$this->getHtmlError($val2));
			}

			// no childrens - no table
			$children = $el->getChildren();

			if (empty($children)) {
				continue;
			}
			
			// fieldset
			if ($val['type'] == 'fieldset') {
				$legend = Html::el('legend')
					->setText($val['legend']);

				$fieldset = Html::el('fieldset')
					->id('fieldset_'.$key)
					->add($legend)
					->add($el->render(0));
					

				$html .= $fieldset->render(0);
				
			// plain
			} else {
				$html  .= $el->render(0);
			}
		}


		// end of form
		$html .= $this->getEnd();
		return $html;
	}
}
