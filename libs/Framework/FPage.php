<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   CMS
 * @package    Cms\FPage
 * @version    FPage.php 2009-01-08 divak@gmail.com
 */

/*namespace Cms\FPage;*/


/**
 * Is under Node/FPage
 *
 * @author     Stanislav Bazik
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @package    Cms\FPage
 */
class FPage extends Object
{
	/** @var int */
	public $pageId;

	/** @var int */
	public $nodeId;

	/** @var string */
	private $title;

	/** @var string */
	public $alternativeTitle;

	/** @var string */
	public $seoTitle;

	/** @var string */
	public $friendlyUrl;

	/** @var string */
	public $keywords;

	/** @var string */
	public $description;

	/** @var string */
	public $languagePk;

	/** @var string */
	public $abstract;

	/** @var string */
	public $text;

	/** @var int */
	static public $counter = 0;

	/** @var string */
	static private $cacheKey;

	/** @var boolean */
	private $clearCache = FALSE;

    /** @var string */
    public $dtm;

	/**
	 * @param int $pageId
	 * @return void
	 */
	private function __construct($pageId=0)
	{
		self::$counter++;
		$this->pageId = (int)$pageId;

		if ($this->pageId) {
			$w = FApplication::$db->select('*')->from(TBL_PAGES)->where('page_id=%i', $this->getId())->execute()->fetch();

			if ($w) {
				$this->pageId = $w['page_id'];
				$this->nodeId = $w['node_id'];
				$this->title = !empty($w['title']) ? $w['title'] : 'no_title';
				$this->alternativeTitle = $w['alternative_title'];
				$this->seoTitle = $w['seo_title'];
				$this->friendlyUrl = $w['friendly_url'];
				$this->keywords = $w['keywords'];
				$this->description = $w['description'];
				$this->languagePk = $w['language_pk'];
				$this->abstract = $w['abstract'];
				$this->text = $w['text'];
				$this->dtm = $w['dtm'];
			} else {
				$this->pageId = $this->pageId >0 ? -1 * $this->pageId : $this->pageId;
			}
		}
	}

	/**
	 * @param int $pageId
	 * @return object
	 */
	static public function create($pageId=0)
	{
		# cache
		self::$cacheKey = 'page_'.(int)$pageId;
		if (StaticCache::read(self::$cacheKey)) {
			return StaticCache::read(self::$cacheKey);
		} else {
			$page = new self($pageId);
			StaticCache::write(self::$cacheKey, $page);
			return $page;
		}
	}

	/**
	 * @return int
	 */
	public function getId() { return( $this->pageId ); }

	/**
	 * @return bool
	 */
	public function exists() { return( $this->pageId > 0 ); }

	/**
	 * @return bool
	 */
	public function isNew() { return( $this->pageId == 0 ); }

	/**
	 * @return string
	 */
	public function getSeoTitle()
	{
		return !empty($this->seoTitle) ? $this->seoTitle : $this->getTitle();
	}

	/**
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title)
	{
		if ($this->title !== $title) {
			$this->clearCache = TRUE;
		}
		$this->title = $title;
	}

	/**
	 * @return void
	 */
	public function save()
	{
		$action = $this->pageId ? 'update' : 'insert';

		$values = Array();
		$values['node_id'] = $this->nodeId;
		$values['title'] = $this->title;
		$values['alternative_title'] = $this->alternativeTitle;
		$values['seo_title'] = $this->seoTitle;
		$values['friendly_url'] = $this->friendlyUrl;
		$values['description'] = $this->description;
		$values['keywords'] = $this->keywords;
		$values['language_pk'] = $this->languagePk;
		$values['abstract'] = $this->abstract;
		$values['text'] = $this->text;
        $values['dtm'] = new DibiDateTime();

		# UPDATE or INSERT ($this->id == 0)
		if ($action == 'update') {
			$values['page_id'] = $this->getId();
			FApplication::$db->update(TBL_PAGES, $values)->where('page_id=%i', $this->getId())->execute();

		} else {
			//$this->pageId = Db::LastId();

			foreach (FLanguage::getLanguages() as $key=>$val) {
				// title
				if ($this->title !== NULL) {
					$values['title'] = $this->title;
				} else {
					$values['title'] = 'page' . $this->nodeId . strtolower($key);
				}

				$values['language_pk'] = $key;
				$values['friendly_url'] = 'page' . $this->nodeId . strtolower($key).'.html';

				FApplication::$db->insert(TBL_PAGES, $values)->execute(dibi::IDENTIFIER);
			}
		}

		// remove from cache
		StaticCache::remove(self::$cacheKey);

		// cache
		if ($this->clearCache === TRUE) {
			$cache = FApplication::getCache();
			$cache->clean(array(Cache::TAGS => 'node'));
		}

		return $this->pageId;
	}

	/**
	 * @return void
	 */
	public function delete()
	{
		FApplication::$db->delete(TBL_PAGES)->where('page_id=%i', $this->getId())->execute();

		// cache
		$cache = FApplication::getCache();
		$cache->clean(array(Cache::TAGS => 'node'));
	}

	/**
	 * Return description for current page
	 * If empty than return root description
	 * @return string
	 */
	public function getDescription()
	{
		if (empty($this->description)) {
			$node = new FNode(1);
			$description = $node->page->description;
		} else {
			$description = $this->description;
		}
		return $description;
		//return empty($this->description) ? FNode::create(1)->page->description : $this->description;
	}

	/**
	 * Return keywords for current page
	 * If empty than return root keywords
	 * @return string
	 */
	public function getKeywords()
	{
		if (empty($this->keywords)) {
			$node = new FNode(1);
			$keywords = $node->page->keywords;
		} else {
			$keywords = $this->keywords;
		}
		return $keywords;
		//return empty($this->keywords) ? FNode::create(1)->page->keywords : $this->keywords;
	}

	/**
	 * @param int $parentId
	 * @return array $pages
	 */
	static public function getTitlesList()
	{
		# cache
		$cacheKey = 'pages_titles';
		if (StaticCache::read($cacheKey)) {
			return StaticCache::read($cacheKey);
		}

		$pages = FApplication::$db->select('n.`node_id`, p.`title`')
			->from(TBL_NODES.' n')
			->leftJoin(TBL_PAGES.' p')
				->on('n.`node_id`=p.`node_id`')->and(' p.`language_pk`=%s', FLanguage::getCurrentLanguage())
			->execute()->fetchPairs();

		StaticCache::write($cacheKey, $pages); // add to cache
		return $pages;
	}

	/**
	 * @param int $nodeId
	 * @return void
	 */
	static public function deleteNodePages($nodeId)
	{
		// delete all pages
		$ids = FApplication::$db->select('page_id')->from(TBL_PAGES)->where('node_id=%i', $nodeId)->execute()->fetchPairs();

		foreach ($ids as $key=>$val) {
			FPage::create($val)->delete();
		}
	}


	static public function getNodePages($nodeId)
	{
		$ids = FApplication::$db->select('`language_pk`,`page_id`')->from(TBL_PAGES)->where('node_id=%i', $nodeId)->execute()->fetchPairs();
		return $ids;
	}
}
