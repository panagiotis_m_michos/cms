<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   CMS
 * @package    Cms\FLanguage
 * @version    Lng.php 2009-01-08 divak@gmail.com
 */

/*namespace Cms\FLanguages;*/


/**
 * Handle work with languages
 * 
 * @author     Stanislav Bazik
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @package    Cms\Lng
 */
class FLanguage extends Object
{
	/* @var int */
	static public $counter=0;

	/**
	 * @param bool $onlyActive
	 * @return array $lngs
	 */
	static public function getLanguages($onlyActive=TRUE)
	{
		# cache
		$cacheKey = 'curr_lng_'.(int)$onlyActive;
		if (StaticCache::read($cacheKey)) {
			return StaticCache::read($cacheKey);
		}
		
		self::$counter++;
		
		# only active lngs
		$where = $onlyActive ? '`is_active`=1' : '1';

		$lngs = FApplication::$db->select('pk, is_active, name, ord')
			->from(TBL_LANGUAGES)
			->where($where)
			->orderBy('ord')->asc()
			->execute()->fetchAssoc('pk');
			
		StaticCache::write($cacheKey, $lngs); //add to cache
		return $lngs;
	}
	
	
	
	static public function isValid($lng, $onlyActive = TRUE)
	{
		$lngs = self::getLanguages($onlyActive);
		$lng = strtoupper($lng);
		return isset($lngs[$lng]); 
	}
	

	/**
	 * @return string
	 */
	static public function getDefaultLng()
	{
		return FApplication::$config['default_lng'];
	}
}