<?php

use BootstrapModule\Ext\SymfonyForms\SymfonyFormsExt;
use ErrorModule\Ext\DebugExt;
use FeatureModule\Feature;
use Nette\Utils\Arrays;
use Nette\Utils\Json;
use Psr\Log\LoggerInterface;
use RouterModule\RouterExt;
use Services\ControllerHelper;
use Symfony\Bundle\FrameworkBundle\Templating\Helper\FormHelper;
use Symfony\Component\DependencyInjection\Dump\Container;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;

abstract class FControler extends Object
{
    /** @var string */
    public static $handleObject = 'FNode';

    /** @var string */
    public $action = 'default';

    /** @var string */
    public $view;

    /** @var array */
    public $possibleActions = array();

    /** @var object Node */
    public $node;

    /** @var FRouter */
    public $router;

    /** @var int */
    public $nodeId;

    /** @var string */
    public $lang;

    /** @var array */
    public $get;

    /** @var array */
    public $post;

    /** @var object Session */
    public $session;

    /** @var object */
    public $db;

    /** @var object */
    protected $template;

    /** @var boolean */
    protected $terminated = FALSE;

    /**
     * If https is reuquired, than redirect
     *
     * @var Boolean
     */
    protected $httpsRequired = FALSE;

    /**
     * @var EmailerFactory
     * */
    protected $emailerFactory;

    /**
     * @var Container
     */
    protected $container;

    /**
     * @var array
     */
    protected $templateExtendedFrom = array();

    /**
     * @var Router
     */
    protected $newRouter;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @param int $nodeId
     */
    public function __construct($nodeId = NULL)
    {
        $handleObject = $this->getReflection()->getStaticPropertyValue('handleObject');
        $this->node = ($nodeId == NULL) ? new $handleObject(FApplication::$nodeId) : new $handleObject($nodeId);

        $this->router = FApplication::$router;
        $this->nodeId = FApplication::$nodeId;
        $this->lang = FApplication::$lang;
        $this->action = FApplication::$action;
        $this->view = FApplication::$view;
        $this->get = FApplication::$get;
        $this->post = FApplication::$post;
        $this->session = FApplication::$session;
        $this->db = FApplication::$db;
        $this->template = FApplication::$template;
        $this->emailerFactory = Registry::$emailerFactory;
        $this->container = Registry::$container;
        $this->newRouter = $this->getService(RouterExt::ROUTER);
        $this->logger = $this->getService(DebugExt::LOGGER);
        $this->request = $this->getService(RouterExt::REQUEST);
    }

    /**
     * Here we can change e.g. connect to database, or similar
     */
    public function startup()
    {
        $url = NULL;
        
        if (isset($this->get['popup']) && !$this->httpsRequired) {
            $url = FALSE;
        }

        if ($url) {
            $this->redirect("%$url%");
        }

        //	possible actions
        if (isset($this->possibleActions[$this->nodeId])) {
            $this->action = $this->possibleActions[$this->nodeId];
        }
    }

    /**
     * Here we can change e.g. action for whole controler
     * @return void
     */
    public function beforePrepare()
    {
        // change action if is empty
        if (empty($this->action)) {
            $this->action = 'default';
        }
    }

    /**
     * Here we can catch some action to handle with it
     * @return void
     */
    public function beforeHandle()
    {

    }

    /**
     * Here we can e.g. add menu
     * @return void
     */
    public function beforeRender()
    {
        // add variables to the template
        $this->template->this = $this;
        $this->template->config = FApplication::$config;

        // urls
        $this->template->homeUrl = self::getHomeLink();

        // other text stuff
        $this->template->author = FApplication::$config['author'];
        $this->template->copyright = FApplication::$config['copyright'];
        $this->template->projectName = FApplication::$config['projectName'];
        $this->template->languages = FLanguage::getLanguages();
        $this->template->currLng = FApplication::$lang;
        $this->template->keywords = $this->node->getLngKeywords();
        $this->template->description = $this->node->getLngDescription();
        $this->template->seoTitle = $this->node->getLngSeoTitle();
        $this->template->text = FApplication::modifyCmsText($this->node->getLngText());
        $this->template->isProduction = FApplication::$container->getParameter('isProduction');
        $this->template->disabledFeedback = FProperty::get('disabledFeedback')->value;

        $this->template->title = $this->node->getLngTitle();
        $this->template->visibleTitle = $this->node->getVisibleTitle();
        $this->template->setVariables(FApplication::$container->get('templating_module.layout_variable_provider')->getLayoutVariables());
    }

    /**
     * Terminate controller - no further actions
     * @param string $message
     */
    final protected function terminate($message = NULL)
    {
        if ($message) {
            echo $message;
        }
        $this->terminated = TRUE;
    }

    final public function run()
    {
        $this->startup();

        // e.g. change action
        if (!$this->terminated) {
            $this->beforePrepare();
        }

        // prepare[action]
        if (!$this->terminated && method_exists($this, 'prepare' . ucfirst($this->action))) {
            $this->{'prepare' . ucfirst($this->action)}();
        }

        // e.g. handle some action
        if (!$this->terminated) {
            $this->beforeHandle();
        }

        // handle[action]
        if (!$this->terminated && method_exists($this, 'handle' . ucfirst($this->action))) {
            $this->{'handle' . ucfirst($this->action)}();
        }


        // e.g. set menu
        if (!$this->terminated) {
            $this->beforeRender();
        }

        // render[action]
        if (!$this->terminated && method_exists($this, 'render' . ucfirst($this->action))) {
            $this->{'render' . ucfirst($this->action)}();
        }

        // show template
        if (!$this->terminated) {
            $this->printHtml();
        }

        // e.g. set menu
        if (!$this->terminated) {
            $this->afterRender();
        }
    }

    public function afterRender()
    {
    }

    /**
     * @return string
     * @throws FileNotFoundException
     */
    public function getHtml()
    {
        $templatesPath = array();

        // extended templates
        foreach ($this->templateExtendedFrom as $val) {
            $templatesPath[] = sprintf("%s.%s.tpl", $val, $this->getView());
            $templatesPath[] = sprintf("%s/%s.tpl", $val, $this->getView());
        }

        // controller templates
        $controler = str_replace('Controler', '', get_class($this));
        $templatesPath[] = sprintf("%s.%s.tpl", $controler, $this->getView());
        $templatesPath[] = sprintf("%s/%s.tpl", $controler, $this->getView());

        // default templates
        $templatesPath[] = "Default.default.tpl";
        $templatesPath[] = "Default/default.tpl";

        foreach ($templatesPath as  $tpl) {
            if (is_file(FRONT_TEMPLATES_DIR . DIRECTORY_SEPARATOR . $tpl)) {
                return FApplication::$template->getHtml($tpl);
            }
        }

        throw new FileNotFoundException(sprintf(
            "%s: Can't find template for view: %s or default",
            $this->getReflection()->getName(),
            $this->getView()
        ));
    }

    public function printHtml()
    {
        echo $this->getHtml();
    }

    /**
     * @param array|mixed $params
     * @return string
     */
    public function getHomeLink($params = NULL)
    {
        return $this->router->link(FApplication::$config['home_node'], $params);
    }

    /**
     * @param array $arr
     */
    protected function addBreadCrumbs(array $arr)
    {
        $bc = array();
        foreach ($arr as $key => $val) {
            if (is_array($val)) {
                // node
                if (isSet($val['node_id'])) {
                    $node = new FNode((int)$val['node_id']);
                    $url = $this->router->link($val['node_id']);

                    if (isSet($val['params'])) {
                        $url = FTools::addUrlParam($val['params'], $url);
                    }
                    $bc[] = Array('title' => $node->getLngTitle(), 'url' => $url);
                } else {
                    $bc[] = Array('title' => $val['title'], 'url' => $val['url']);
                }
            } elseif (is_int($val)) {
                $node = new FNode((int)$val);
                $bc[] = Array('title' => $node->getLngTitle(), 'url' => $this->router->link($node->getId()));
            } else {
                $bc[] = Array('title' => $val, 'url' => '');
            }
        }
        $this->template->breadcrumbs = $bc;
    }

    /**
     * Examples:
     * ========
     * $this->redirect(112, 'a=1');
     * $this->redirect(112, array('a=1', 'b=2'));
     * $this->redirect(112, array('a'=>1, 'b'=>2));
     * $this->redirect('%http://bernolak.sk%', 's=1');
     *
     * @param mixed $url
     * @param mixed $params
     */
    public function redirect($url = NULL, $params = NULL)
    {
        // use current address
        if (is_null($url)) {
            $url = FApplication::$httpRequest->uri->absoluteUri;
            // use CMS address
        } else {
            $url = FApplication::$router->link($url);
        }

        $url = FTools::addUrlParam($params, $url);
        Header('Location: ' . $url);
        exit;
    }

    /**
     * @param string $name
     * @param array $params
     */
    public function redirectRoute($name, $params = array())
    {
        // @TODO remove once payment_upgrade feature is removed
        if ($name === 'payment') {
            if (!Feature::isEnabled('payment_upgrade')) {
                $redirectResponse = new RedirectResponse($this->router->link(PaymentControler::SAGE_PAGE, $params));
                $redirectResponse->send();
                exit;
            }
        }
        $redirectResponse = new RedirectResponse($this->newRouter->generate($name, $params));
        $redirectResponse->send();
        exit;
    }

    /**
     * Wrapper for smarty - get php constant
     * @param string $constant
     * @return mixed
     */
    public function constant($constant)
    {
        return eval("return $constant;");
    }

    /**
     * Provide add flash message to session
     * @param string $text
     * @param string $type
     * @param bool $escape
     */
    final protected function flashMessage($text, $type = 'info', $escape = TRUE)
    {
        $session = FApplication::$session->getNameSpace(ControllerHelper::SESSION_FLASH);
        $session->messages[] = ['text' => $text, 'type' => $type, 'escape' => $escape];
    }

    /**
     * Provide read data from session and add to the template
     */
    final protected function readFlashMessages()
    {
        $session = FApplication::$session->getNameSpace(ControllerHelper::SESSION_FLASH);
        $this->template->flashes = $session->messages;
        $session->remove();
    }

    /**
     * Provides saving backlink
     * @param string $backlink
     */
    final protected function saveBacklink($backlink = NULL)
    {
        $nameSpaceName = FApplication::isAdmin() ? 'backlink_admin' : ControllerHelper::SESSION_BACKLINK;
        if (!$backlink) {
            $backlink = FApplication::$httpRequest->uri->getAbsoluteUri();
        }
        $namespace = $this->session->getNamespace($nameSpaceName);
        $namespace->backlink = $backlink;
    }

    /**
     * Returns backlink or false if is not set
     * @return mixed
     */
    final protected function getBacklink()
    {
        $nameSpaceName = FApplication::isAdmin() ? 'backlink_admin' : ControllerHelper::SESSION_BACKLINK;
        $namespace = $this->session->getNamespace($nameSpaceName);

        if (isset($namespace->backlink)) {
            return $namespace->backlink;
        } else {
            return FALSE;
        }
    }

    /**
     * Provides remove backlink
     * @return void
     */
    final protected function removeBacklink()
    {
        $nameSpaceName = FApplication::isAdmin() ? 'backlink_admin' : ControllerHelper::SESSION_BACKLINK;
        $namespace = $this->session->getNamespace($nameSpaceName);
        $namespace->remove();
    }

    /**
     * Provides changing view
     * @param string $view
     * @return void
     */
    final protected function changeView($view)
    {
        $this->view = (string)$view;
    }

    /**
     * Returns actual view
     * @return string
     */
    final public function getView()
    {
        if ($this->view != NULL) {
            return $this->view;
        }
        return $this->action;
    }

    final protected function getSession($nameSpace)
    {
        $session = FApplication::$session->getNameSpace($nameSpace);
        return $session;
    }

    /**
     * terminate request outputing json data
     * @param array $array
     * @param int $responseCode
     */
    public function jsonTerminate($array, $responseCode = 200)
    {
        $response = new HttpResponse();
        if ($responseCode) {
            $response->setCode($responseCode);
        }
        echo json_encode($array);
        exit;
    }

    /**
     * get json data from input
     * @return array
     */
    public function getJson()
    {
        $data = file_get_contents("php://input");
        return $data ? json_decode($data, true) : array();
    }

    /**
     * Returns true if it's ajax request
     *
     * @return boolean
     */
    public function isAjax()
    {
        return FApplication::$httpRequest->isAjax();
    }

    /**
     * @return boolean
     */
    public function isSecured()
    {
        return FApplication::$httpRequest->isSecured();
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function getService($name)
    {
        return $this->container->get($name);
    }

    /**
     * @param array $payload
     */
    final protected function sendJSONResponse(array $payload)
    {
        FApplication::$httpResponse->setContentType('application/json');
        FApplication::$httpResponse->expire(0);
        $this->terminate(Json::encode($payload));
    }

    /**
     * @param mixed $key
     * @param mixed $default
     * @return mixed
     */
    public function getParameter($key, $default = NULL)
    {
        return Arrays::get($this->get, $key, $default);
    }

    /**
     * @param string|FormTypeInterface $type
     * @param mixed|NULL $data
     * @param array $options
     * @return Form
     */
    public function createForm($type, $data = NULL, array $options = array())
    {
        /** @var FormFactory $formFactory */
        $formFactory = $this->getService(SymfonyFormsExt::FORM_FACTORY);
        $form = $formFactory->create($type, $data, $options);
        $form->handleRequest($this->request);
        return $form;
    }

    /**
     * @return FormHelper
     */
    public function getFormHelper()
    {
        $template = $this->getService(SymfonyFormsExt::TEMPLATING_PHP);
        return $template['form'];
    }


}
