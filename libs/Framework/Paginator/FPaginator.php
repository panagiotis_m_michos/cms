<?php

require_once dirname(__FILE__) . '/../../3rdParty/Nette/Object.php';

class FPaginator extends Object
{
    /** @var array */
    protected $get;

    /** @var string */
    protected $getName = 'page';

    /** @var integer */
    protected $items = 0;

    /** @var integer */
    protected $itemsPerPage = 10;

    /** @var integer */
    protected $firstPage = 1;

    /**
     * @param int $items - how many items we have
     * @param int $itemsPerPage
     * @param string $queryOffsetName
     * @param int $firstPage
     */
    public function __construct($items = 0, $itemsPerPage = 10, $queryOffsetName = 'page', $firstPage = 1)
    {
        // how many items we have
        $this->items = (int)$items;
        $this->itemsPerPage = $itemsPerPage;
        $this->getName = $queryOffsetName;
        $this->firstPage = $firstPage;

        // get
        if (class_exists('FApplication') && property_exists('FApplication', 'get')) {
            $this->get = FApplication::$get;
        } else {
            $this->get = $_GET;
        }
    }

    /**
     * Returns current page
     * @return int current page
     */
    public function getCurrentPage()
    {
        // this getname is not set
        if (!isSet($this->get[$this->getName])) {
            return 1;
        } else {
            // check if do we have so many pages
            if ((int)$this->get[$this->getName] <= $this->getLastPage()) {
                // it should be greater than first page
                if ((int)$this->get[$this->getName] < $this->firstPage) {
                    return $this->firstPage;
                } else {
                    return (int)$this->get[$this->getName];
                }
            } else {
                return $this->getLastPage();
            }
        }
    }

    /**
     * @return int
     */
    protected function getNextPage()
    {
        // check if its lower than last page
        if ($this->getCurrentPage() + 1 > $this->getLastPage()) {
            return $this->getCurrentPage();
        } else {
            return $this->getCurrentPage() + 1;
        }
    }


    /**
     * @return int
     */
    protected function getPreviousPage()
    {
        // check if its greater than first page
        if ($this->getCurrentPage() - 1 < $this->firstPage) {
            return $this->firstPage;
        } else {
            return $this->getCurrentPage() - 1;
        }
    }

    /**
     * @return int $last_page
     */
    protected function getLastPage()
    {
        return $this->getNumPages();
    }

    /**
     * @return int
     */
    protected function getNumPages()
    {
        $numPages = ceil($this->items / $this->getItemsPerPage());
        $numPages += max(0, $this->firstPage - 1); // if first page is not 1
        return $numPages;
    }

    /**
     * @return integer
     */
    protected function getItemsPerPage()
    {
        if (property_exists($this, 'getNameIpp') && isSet($this->get[$this->getNameIpp])) {
            // check if it's list of options
            if (!isSet($this->displayOptions[$this->get[$this->getNameIpp]])) {
                trigger_error($this->getClass() . '::itemsPerPage is not in ' . $this->getClass() . '::$display_options', E_USER_ERROR);
            }
            return $this->get[$this->getNameIpp];
        } else {
            return $this->itemsPerPage;
        }
    }

    /**
     * @return bool
     */
    protected function isFirstPage()
    {
        return $this->getCurrentPage() == $this->firstPage;
    }

    /**
     * @return bool
     */
    protected function isLastPage()
    {
        return $this->getCurrentPage() == $this->getLastPage();
    }

    /**
     * @return string $limit
     */
    public function getSqlLimit()
    {
        return 'LIMIT ' . $this->getOffset() . ', ' . $this->getLimit();
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->getItemsPerPage();
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        $offset = 0;
        if ($this->getNumPages() > 1) {
            $offset = ($this->getCurrentPage() - 1) * $this->getItemsPerPage();
        }
        return $offset;
    }

    /**
     * @return string
     */
    public function render()
    {
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->render();
    }



    /***************************** SETTERS AND GETTERS **********************************/


    /**
     * @param string $name
     * @return unknown
     */
    public function &__get($propertyName)
    {
        if (!property_exists($this->getClass(), $propertyName)) {
            trigger_error("Class property `$propertyName` doesn't exists!", E_USER_ERROR);
        }

        return $this->$propertyName;
    }


    /**
     * @param string $propertyName
     * @param mixed $value
     */
    public function __set($propertyName, $value)
    {
        if (!property_exists($this->getClass(), $propertyName)) {
            trigger_error("Class property `$propertyName` doesn't exists!", E_USER_ERROR);
        }

        $this->$propertyName = $value;
    }
}
