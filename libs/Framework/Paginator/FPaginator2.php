<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   CMS
 * @package    Cms\Paginator2
 * @version    Paginator2.php 2009-04-04 divak@gmail.com
 */

/*namespace Cms\Paginator2;*/


/**
 * @example |< << 1 .. 3,4,|5|,6,7 .. 11 >> >| 777 results - display |10v|
 * @author     Stanislav Bazik
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @package    Cms\Paginator2
 */

class FPaginator2 extends FPaginator1
{
	/** @var string */
	protected $htmlResult = '<b>#</b> Results';

	/** @var string */
	protected $htmlDisplay = ' - Display: #';

	/** @var string */
	protected $getNameIpp = 'ipp';

	/** @var array */
	protected $displayOptions = Array(
		10  => 10,
		25  => 25,
		40  => 40,
		80  => 80,
		300 => 300
	);

	/** @var boolean */
	protected $htmlResultEnabled = TRUE;

	/** @var boolean */
	protected $htmlDisplayEnabled = TRUE;


	/**
	 * @return object Html
	 */
	protected function getResultHtml()
	{
		$el = Html::el();
		if ($this->htmlResultEnabled) {
			if ($this->getNumPages() != 1) {
				$text = "\x0A-\x0A" . str_replace('#', $this->items, $this->htmlResult);
			} else {
				$text = str_replace('#', $this->items, $this->htmlResult);
			}
			$el->create('span')->class('result_info')->setHtml($text);
		}
		return $el;
	}

	/**
	 * @return object Html
	 */
	protected function getDisplayHtml()
	{
		$span = Html::el('span')->class('display');
		if ($this->htmlDisplayEnabled) {
			$select = Html::el('select')->name($this->getNameIpp)->onchange('this.form.submit();');
			foreach ($this->displayOptions as $key=>$val) {
				$option = $select->create('option')->value($key)->setText($val);
				// selected option
				if (isSet($this->get[$this->getNameIpp]) && $this->get[$this->getNameIpp] == $key || $key == $this->getItemsPerPage()) {
					$option->selected('selected');
				}
			}
			$text = str_replace('#', $select, $this->htmlDisplay);
			$span->setHtml($text);
		}
		return $span;
	}

	/**
	 * Returns submit button if there is no javascript
	 * and add hidden inputs for get parameters
	 * @return object Html
	 */
	private function getSubmitAndHiddenInputs()
	{
		$el = Html::el();
		if ($this->htmlDisplayEnabled) {
			// noscript submit
			$el->create('noscript')->create('input')->type('submit')->value('Send'); 
			foreach ($this->get as $key=>$val) {
				// if getname is the same as get parameter 
				// we don't want to change it
				if ($key == $this->getNameIpp) {
					continue;
				}
				$el->create('input')->type('hidden')->name($key)->value($val);
			}
		}
		return $el;
	}

	/**
	 * @return object Html
	 */
	private function getPages() //TODO
	{
		$el = Html::el();
		
		// from 
		if (($this->getCurrentPage() - 2) < $this->firstPage) {
			$from = $this->firstPage;
		} else {
			$from = $this->getCurrentPage() - 2;
		}
		
		// to
		$to = $from + 4;

		// re-counting $from and $to
		if ($to > $this->getLastPage()) {
			$from = $from - ($to - $this->getLastPage());
			$from = max($this->firstPage, $from);
			$to = $this->getLastPage();
		}

		// first page
		if($from !== $this->firstPage) {
			if (($from - $this->firstPage) >  1) {
				$el->add($this->getPageHtml($this->firstPage, FALSE) . "&hellip;");
			} else {
				$el->add($this->getPageHtml($this->firstPage, TRUE));
			}
		}

		// pages between
		for ($page=$from; $page<=$to; $page++) {
			if ($page == $to && $this->getLastPage() - $to >= 2) {
				$el->add($this->getPageHtml($page, FALSE));
				if ($this->getLastPage() - $to >= 2) {
					$el->add("&hellip;");
				}
			} else {
				$el->add($this->getPageHtml($page, TRUE));
			}
		}

		// last page
		if ($to != $this->getLastPage()) {
			$lastPage = $this->getPageHtml($this->getLastPage());
			$el->add($lastPage);
		}

		return $el;
	}
	

	/**
	 * @return object Html
	 */
	public function render()
	{
		if ($this->getNumPages() !== 0) {
			$pager = Html::el('p')->class('pager');
			// display options
			if ($this->htmlDisplayEnabled) {
				$el = $pager->create('form')->action($_SERVER['REQUEST_URI'])->method('get');
			} else {
				$el = $pager->create(Html::el());
			}
	
			// is not necessary write pager 
			// only result and display
			if ($this->getNumPages() <= $this->firstPage) {
				// at least one is enabled
				if ($this->htmlResultEnabled || $this->htmlDisplayEnabled) {
					$el->add($this->getResultHtml());
					//$el->add($this->getDisplayHtml());
				}
			} else  {
				$el->add($this->getBeginText());
				$el->add($this->getFirstArrowHtml());
				$el->add($this->getPrevArrowHtml());
				$el->add($this->getPages());
				$el->add($this->getNextArrowHtml());
				$el->add($this->getLastArrowHtml());
				$el->add($this->getResultHtml());
//				$el->add($this->getDisplayHtml());
			}
			
			// get params
			$el->add($this->getSubmitAndHiddenInputs());
			return $pager;
		}

		// no rows
		return '';
	}
}