<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   CMS
 * @package    Cms\Paginator1
 * @version    Paginator1.php 2009-04-04 divak@gmail.com
 */

/*namespace Cms\Paginator1;*/

require_once dirname(__FILE__) . '/../../3rdParty/Nette/Web/Html.php';


/**
 * @example |< << 1,2,3,4,5 >> >|
 * @author     Stanislav Bazik
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @package    Cms\Paginator1
 */

class FPaginator1 extends FPaginator
{
	/** @var string */
	protected $htmlFirst = "|&lt;\x0A";
	
	/** @var string */
	protected $htmlPrevious = "&lt;&lt;\x0A";

	/** @var string */
	protected $htmlFirstDelim = '';

	/** @var string */
	protected $htmlDelim = ",\x0A";

	/** @var string */
	protected $htmlLastDelim = "\x0A";

	/** @var string */
	protected $htmlNext = "&gt;&gt;\x0A";

	/** @var string */
	protected $htmlLast = "&gt;|";

	/** @var string */
	protected $htmlBeginText = "Page:\x0A";

	/** @var boolean */
	protected $firstEnabled = TRUE;

	/** @var boolean */
	protected $previousEnabled = TRUE;

	/** @var boolean */
	protected $nextEnabled = TRUE;

	/** @var boolean */
	protected $lastEnabled = TRUE;

	/** @var boolean */
	protected $beginTextEnabled = TRUE;
	
	/**
	 * @param int $page
	 * @param boolean $delimEnabled
	 * @return object Html
	 */
	protected function getPageHtml($page, $delimEnabled=TRUE)
	{
		$el = Html::el();
		// current page
		if ($this->getCurrentPage() == $page) {
			$el->add($this->getCurrentPageHtml($page));
		// first page
		} elseif ($this->firstPage == $page) {
			$el->create('a')->href($this->getUrl($page))->setHtml($page);
			$el->add($this->htmlFirstDelim);
			if ($delimEnabled) {
				$el->add($this->htmlDelim); 
			}
		// last page
		} elseif ($this->getLastPage() == $page) {
			$el->create('a')->href($this->getUrl($page))->setHtml($page);
			$el->add($this->htmlLastDelim);
		} else {
			$el->create('a')->href($this->getUrl($page))->setHtml($page);
			if ($delimEnabled) {
				$el->add($this->htmlDelim); 
			}
		}
		
		return $el;
	}

	/**
	 * @param int $page
	 * @return object Html
	 */
	protected function getCurrentPageHtml($page)
	{
		$el = Html::el();
		// first page
		if ($this->firstPage == $page) {
			$el->add($this->htmlFirstDelim);
			$span = $el->create('span')->add($page);
			$span->add($this->htmlDelim);
		// last page
		} elseif($this->getLastPage() == $page) {
			$span = $el->create('span')->class('curr')->add($page);
			$span->add($this->htmlLastDelim);
		} else {
			$span = $el->create('span')->class('curr')->add($page);
			$span->add($this->htmlDelim);
		}
		return $el;
	}

	/**
	 * @return object Html
	 */
	protected function getFirstArrowHtml()
	{
		$el = Html::el();
		// is enabled
		if($this->firstEnabled) {
			if ($this->isFirstPage()) {
				$el->create('span')->class('htmlFirst')->add($this->htmlFirst);
			} else{
				$url = $this->getUrl($this->firstPage);
				$el->create('a')->href($url)->class('htmlFirst')->add($this->htmlFirst);
			}
		}
		return $el;
	}

	/**
	 * @return object Html
	 */
	protected function getPrevArrowHtml()
	{
		$el = Html::el();
		// is enabled
		if ($this->previousEnabled) {
			if ($this->isFirstPage()) {
				$el->create('span')->class('html_prev')->add($this->htmlPrevious);
			} else {
				$url = $this->getUrl($this->getCurrentPage()-1);
				$el->create('a')->href($url)->class('html_prev')->add($this->htmlPrevious);
			}
		}
		return $el;
	}

	/**
	 * @return object Html
	 */
	protected function getNextArrowHtml()
	{
		$el = Html::el();
		// is enabled
		if ($this->nextEnabled) {
			if ($this->isLastPage()) {
				$el->create('span')->class('htmlNext')->add($this->htmlNext);
			} else {
				$url = $this->getUrl($this->getCurrentPage()+1);
				$el->create('a')->href($url)->class('htmlNext')->add($this->htmlNext);
			}
		}
		return $el;
	}

	/**
	 * @return object Html
	 */
	protected function getLastArrowHtml()
	{
		$el = Html::el();
		// is enabled
		if ($this->lastEnabled) {
			if ($this->isLastPage()) {
				$el->create('span')->class('htmlLast')->add($this->htmlLast);
			} else {
				$url = $this->getUrl($this->getLastPage());
				$el->create('a')->href($url)->class('htmlLast')->add($this->htmlLast);
			}
		}
		return $el;
	}

	/**
	 * @return object Html
	 */
	protected function getBeginText()
	{
		$el = Html::el();
		// is enabled
		if ($this->beginTextEnabled) {
			$el->create('span')->add($this->htmlBeginText);
		}
		return $el;
	}

	/**
	 * @param int $page
	 * @return string $url
	 */
	protected function getUrl($page)
	{
		$param = $this->getName . '=' . $page;
		return FTools::addUrlParam($param,NULL, '&');
	}

	/**
	 * @return mixed
	 */
	public function render()
	{
		$el = Html::el('p')->class('pager');
		if ($this->getNumPages() > $this->firstPage) {
			$p = $el->create('p')->class('pager');
			
			$p->add($this->getBeginText());
			$p->add($this->getFirstArrowHtml());
			$p->add($this->getPrevArrowHtml());
	
			// all pages
			for ($page=$this->firstPage; $page<=$this->getLastPage(); $page++) {
				$p->add($this->getPageHtml($page));
			}
	
			$p->add($this->getNextArrowHtml());
			$p->add($this->getLastArrowHtml());
		}
		
		return $el;
	}
}
