<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   CMS
 * @package    Cms\FMenu
 * @version    FMenu.php 2009-01-15 divak@gmail.com
 */
/*namespace Cms\FMenu;*/

class FMenu extends Object
{
    /**
     * @return array $nodes
     */
    public static function getNodes()
    {
        $nodes = FApplication::$db->select('n.`node_id`, n.`status_id`, n.`is_deleted`, n.`parent_id`, n.`level`, n.`admin_controler`,n.`front_controler`, n.`ord`,n.`dt_show`,n.`is_dt_sensitive`,n.`dt_start`,n.`dt_end`, n.`excludeFromSitemap`,p.`title`, p.`friendly_url`, pr.propertyId, pr.name, pr.value')
            ->from(TBL_NODES . ' n')
            ->leftJoin(TBL_PAGES . ' p')->on('n.node_id=p.node_id')->and('p.language_pk=%s', FApplication::$lang)
            ->leftJoin(TBL_PROPERTIES . ' pr')->on('n.node_id=pr.nodeId')
            ->where('n.is_deleted=%i', 0)
            ->orderBy('n.ord, n.node_id')
            ->execute()
            ->fetchAssoc('node_id,=,propertyId');

        $nodes = array_map(
            function ($node) {
                $properties = $node['propertyId'];
                $propertiesFiltered = [];

                foreach ($properties as $propertyId => $property) {
                    if ($propertyId != "") {
                        $propertiesFiltered[$property['name']] = $property['value'];
                    }
                }

                $node['properties'] = $propertiesFiltered;
                unset($node['propertyId']);
                unset($node['name']);
                unset($node['value']);

                return $node;
            },
            $nodes
        );

        return $nodes;
    }


    /**
     * @param int $parentId
     * @param bool $returnParent
     * @return array
     */
    public static function get($parentId = NULL, $returnParent = FALSE)
    {
        $cache = Registry::$container->get(DiLocator::CACHE_MEMORY);
        $cacheKey = 'all_menu_' . (int) $parentId . (int) $returnParent;
        if ($cache->contains($cacheKey)) {
            return $cache->fetch($cacheKey);
        }
        if ($parentId === NULL) {
            $menu = self::_get(1, TRUE, self::getNodes());
        } else {
            $menu = self::_get($parentId, $returnParent, self::getNodes());
        }

        $cache->save($cacheKey, $menu, 1800);

        return $menu;
    }

    /**
     * @param $parentId
     * @param bool $returnParent
     * @return array
     */
    public static function getAvailableNodes($parentId, $returnParent = FALSE)
    {
        $cache = Registry::$container->get(DiLocator::CACHE_MEMORY);
        $cacheKey = 'visible_fmenu_' . (int) $parentId . (int) $returnParent;
        if ($cache->contains($cacheKey)) {
            return $cache->fetch($cacheKey);
        }
        $items = self::_getAvailableNodes($parentId, $returnParent, self::getNodes());


        $cache->save($cacheKey, $items, 1800);

        return $items;
    }


    /**
     * @param int $parentId
     * @param bool $returnParent
     * @return string
     */
    public static function getHtml($parentId = NULL, $returnParent = FALSE)
    {
        $menu = (string) self::_getHtml($parentId, self::get($parentId, $returnParent));

        return $menu;
    }


    /**
     * @param int $parentId
     * @param bool $retParent
     * @return array
     */
    public static function getSelect($parentId = NULL, $retParent = FALSE)
    {
        return self::_getSelect(self::get($parentId, $retParent));
    }


    /**
     * @param bool $useCache
     * @param string $cacheKey
     * @param null $action
     * @param array $urlParams
     * @return array
     */
    public static function getAdminJsTreeMenu(
        $useCache = TRUE,
        $cacheKey = 'admin_js_tree_menu',
        $action = NULL,
        $urlParams = []
    )
    {
        // cache
        if ($useCache) {
            $cache = FApplication::getCache();
            if (isset($cache[$cacheKey])) {
                return $cache[$cacheKey];
            }
        }

        $menu = NULL;
        $nodes = self::getNodes();
        foreach ($nodes as $key => $val) {

            $class = $val['admin_controler'];
            $vars = get_class_vars($class);
            $icon = $vars['icon'];


            if ($val['front_controler'] !== NULL && !self::isOk2show($val)) { // change icon to non-active
                $icon = 'page_i.gif';
            }

            $params = [
                'id' => $key,
                'pid' => $val['parent_id'],
                'name' => _e($val['title']),
                'url' => self::getUrl($key, NULL, $action, $urlParams),
                'title' => 'id: ' . $key . ', ord: ' . $val['ord'],
                'target' => '',
                'icon' => ICONS_DIR_R . $icon,
                'iconOpen' => ICONS_DIR_R . $icon,
            ];

            $menu .= 'd.add(' . "'" . implode("','", $params) . "'" . ');' . "\n";
        }

        // cache
        if ($useCache && isset($cache)) {
            $cache->save(
                $cacheKey,
                $menu,
                [
                    'tags' => ['node'],
                ]
            );
        }

        return $menu;
    }

    /**
     * @param string $nodeId
     * @param string $friendlyUrl
     * @param string $action
     * @param array $params
     * @return bool
     */
    public static function getUrl($nodeId, $friendlyUrl = NULL, $action = NULL, $params = [])
    {
        if (FApplication::isAdmin()) {
            $url = ADMIN_URL_ROOT . FApplication::$lang . '/' . $nodeId . '/';
            if ($action !== NULL) {
                $url .= $action . '/';
            }
            $url = FTools::addUrlParam($params, $url);
        } else {
            if (FApplication::hasAllowedFriendlyUrl()) {
                $url = ($friendlyUrl != '/') ? $friendlyUrl : '';
            } else {
                $url = 'index.php?id=' . $nodeId . '&lng=' . FApplication::$router->getCurrentLanguage();
            }

            $url = URL_ROOT . $url;
        }

        return $url;
    }

    /**
     * @param mixed $data
     * @return bool
     */
    private static function isOk2show($data)
    {
        $data = (object) $data;
        // no front controler
        if ($data->front_controler === NULL) {
            return FALSE;
            // has been deleted - it's in bin
        } elseif ($data->is_deleted) {
            return FALSE;
            // check status status
        } elseif ($data->status_id == FNode::STATUS_DRAFT || $data->status_id == FNode::STATUS_PENDING) {
            return FALSE;
            // showing time
        } elseif ($data->dt_show > date('Y-m-d G:i:s')) {
            return FALSE;
            // sensitive to date
        } elseif ($data->is_dt_sensitive && ($data->dt_start > date('Y-m-d G:i:s') || $data->dt_end < date('Y-m-d'))) {
            return FALSE;
        }

        return TRUE;
    }

    /**
     * @param int $parent_id
     * @param array $arr
     * @param bool $first_call
     * @return string $ret
     */
    public static function _getHtml($parent_id = NULL, $arr = NULL, $first_call = TRUE)
    {
        $ul = Html::el('ul');

        if ($first_call) { // add id="menu"
            $ul->id('menu');
        }

        foreach ($arr as $key => $val) {
            $el = Html::el('a')
                ->setText(htmlspecialchars($val['title']))
                ->title(htmlspecialchars($val['title']))
                ->href(self::getUrl($key, $val['friendly_url']));

            // if is selected or child is
            if ($key == FApplication::$nodeId || $val['has_selected_child']) {
                $el->class('curr');
            }

            // if has_childs
            if ($val['childs']) {
                $el->add(self::_getHtml($parent_id, $val['childs'], FALSE));
            }

            $ul->add(Html::el('li')->add($el));
        }

        return $ul;
    }

    /**
     * @param int $parentId
     * @param array $arr
     * @param bool|int $selected_child
     * @return array $pole
     */
    protected static function _get($parentId = NULL, $returnParent = FALSE, $arr = NULL, &$selected_child = 0)
    {
        $ret = [];
        foreach ($arr as $key => $val) {
            if ($returnParent && ($parentId == $key) || !$returnParent && $parentId == $val['parent_id'] /*&& self::isOk2show($val)*/) {

                // don't need anymore
                unset($arr[$key]);

                $ret[$key] = [
                    'id' => $key,
                    'status_id' => $val['status_id'],
                    'level' => $val['level'],
                    'title' => $val['title'],
                    'ord' => $val['ord'],
                    'parent_id' => $val['parent_id'],
                    'friendly_url' => $val['friendly_url'],
                    'url' => self::getUrl($key, $val['friendly_url']),
                    'ok2show' => self::isOk2show($val),
                    'excludeFromSitemap' => $val['excludeFromSitemap'],
                    'is_selected' => FApplication::$nodeId == $key ? 1 : 0,
                    'childs' => self::_get($key, FALSE, $arr, $selected_child),
                    'has_selected_child' => ($selected_child && $key == $selected_child) ? 1 : 0,
                ];

                // send info to parent
                if (FApplication::$nodeId == $key || $ret[$key]['has_selected_child']) {
                    $selected_child = $val['parent_id'];
                }
            }

        }

        return $ret;
    }

    /**
     * @param int $parentId
     * @param bool $returnParent
     * @param array $arr
     * @param bool|int $selected_child
     * @return array $pole
     */
    protected static function _getAvailableNodes(
        $parentId = NULL,
        $returnParent = FALSE,
        $arr = NULL,
        &$selected_child = 0
    )
    {
        $ret = [];
        foreach ($arr as $key => $val) {
            if ($returnParent && ($parentId == $key) || !$returnParent && $parentId == $val['parent_id'] && self::isOk2show($val)) {

                // don't need anymore
                unset($arr[$key]);

                $ret[$key] = [
                    'id' => $key,
                    'status_id' => $val['status_id'],
                    'level' => $val['level'],
                    'title' => $val['title'],
                    'ord' => $val['ord'],
                    'parent_id' => $val['parent_id'],
                    'friendly_url' => $val['friendly_url'],
                    'url' => self::getUrl($key, $val['friendly_url']),
                    'ok2show' => self::isOk2show($val),
                    'excludeFromSitemap' => $val['excludeFromSitemap'],
                    'is_selected' => FApplication::$nodeId == $key ? 1 : 0,
                    'childs' => self::_getAvailableNodes($key, FALSE, $arr, $selected_child),
                    'has_selected_child' => ($selected_child && $key == $selected_child) ? 1 : 0,
                    'properties' => $val['properties']
                ];

                // send info to parent
                if (FApplication::$nodeId == $key || $ret[$key]['has_selected_child']) {
                    $selected_child = $val['parent_id'];
                }
            }

        }

        return $ret;
    }


    /**
     * @param array $arr
     * @param string $prefix
     * @return array
     */
    public static function _getSelect($arr = NULL, $prefix = NULL)
    {
        // prefix
        $prefix = $prefix === NULL && $prefix != 0 ? $prefix . '.' : '';

        $poc = 0;
        $select = [];
        foreach ($arr as $key => $val) {
            if ($val['parent_id'] != -1) {
                $select[$key] = $prefix . ++$poc . '. - ' . $val['title'];
            } else {
                $select[$key] = $val['title'];
            }
            // add childs
            if ($val['childs']) {
                $select += self::_getSelect($val['childs'], $prefix . $poc);
            }
        }

        return $select;
    }


    /**
     * Returns nodes without parent - there went something wrong :)
     * @return array
     */
    public static function getNodesWithoutParent()
    {
        $nodes = FApplication::$db->select('node_id, parent_id')->from(TBL_NODES)->execute()->fetchPairs();
        $badNodes = [];
        foreach ($nodes as $nodeId => $parentId) {
            if ($nodeId != 1) {
                $id = FApplication::$db->select('node_id')
                    ->from(TBL_NODES)
                    ->where('node_id=%i', $parentId)
                    ->and('is_deleted=%i', 0)
                    ->execute()
                    ->fetchSingle();
                if (!$id) {
                    $badNodes[] = $nodeId;
                }
            }
        }

        return $badNodes;
    }
}
