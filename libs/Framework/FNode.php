<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   CMS
 * @package    Cms\FNode
 * @version    Node.php 2009-01-08 divak@gmail.com
 */
/* namespace Cms\FNode; */

include_once dirname(__FILE__) . '/FPage.php';

/**
 * Basic item in CMS - is working with Page
 *
 * @author     Stanislav Bazik
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @package    Cms\FNode
 */
class FNode extends Object
{
    // statuses
    const STATUS_DRAFT = 1;
    const STATUS_PUBLISHED = 2;
    const STATUS_PENDING = 3;

    /** @var array */
    static public $statuses = array(
        self::STATUS_DRAFT => 'Draft',
        self::STATUS_PUBLISHED => 'Published',
        self::STATUS_PENDING => 'Pending',
    );

    /** @var int */
    public $nodeId;

    /** @var int */
    private $statusId;

    /** @var string */
    public $status;

    /** @var boolean */
    public $isDeleted = FALSE;

    /** @var int */
    private $parentId;

    /** @var string */
    public $frontControler;

    /** @var string */
    public $adminControler;

    /** @var int */
    public $level;

    /** @var int */
    private $ord;

    /** @var int */
    public $authorId;

    /** @var object User */
    public $author;

    /** @var int */
    public $editorId;

    /** @var object User */
    public $editor;

    /** @var string */
    public $dtShow;

    /** @var boolean */
    public $isDtSensitive;

    /** @var string */
    public $dtStart;

    /** @var string */
    public $dtEnd;

    /**
     * @var boolean
     */
    public $excludeFromSitemap;

    /** @var string */
    public $dtc;

    /** @var string */
    public $dtm;

    /** @var object Page */
    public $page;

    /** @var int */
    static public $counter = 0;

    /** @var string */
    static protected $cacheKey;

    /** @var boolean */
    private $clearCache = TRUE;

    /**
     * @var EmailerFactory
     * */
    protected $emailerFactory;

    /**
     * @param int $nodeId
     * @param string $language
     */
    public function __construct($nodeId = 0, $language = NULL)
    {
        self::$counter++;
        $this->nodeId = (int) $nodeId;
        $language = empty($language) ? FLanguage::getDefaultLng() : $language;

        if ($this->nodeId) {

            $w = FApplication::$db->select('n.*, p.page_id')
                    ->from(TBL_NODES . ' n')
                    ->join(TBL_PAGES . ' p')
                    ->on('n.node_id=p.node_id')->and('p.language_pk=%s', strtoupper($language))
                    ->where('n.node_id=%i', $this->getId())
                    ->execute()->fetch();

            if ($w) {
                $this->nodeId = $w['node_id'];
                $this->statusId = $w['status_id'];
                $this->status = isset(self::$statuses[$w['status_id']]) ? self::$statuses[$w['status_id']] : 'N/A';
                $this->isDeleted = (bool) $w['is_deleted'];
                $this->parentId = $w['parent_id'];
                $this->frontControler = $w['front_controler'];
                $this->adminControler = $w['admin_controler'];
                $this->level = $w['level'];
                $this->ord = $w['ord'];
                $this->authorId = $w['author_id'];
                $this->author = FUser::create($this->authorId);
                $this->editorId = $w['editor_id'];
                $this->editor = FUser::create($this->editorId);
                $this->dtShow = $w['dt_show'];
                $this->isDtSensitive = $w['is_dt_sensitive'];
                $this->dtStart = $w['dt_start'];
                $this->dtEnd = $w['dt_end'];
                $this->excludeFromSitemap = $w['excludeFromSitemap'];
                $this->dtc = $w['dtc'];
                $this->dtm = $w['dtm'];
                $this->page = FPage::create($w['page_id']);
                // add custom data
                $this->addCustomData();
            } else {
                $this->nodeId = -1 * $this->nodeId;
                $this->page = FPage::create(-1, $language);
            }
        } else {
            $this->page = FPage::create(0, $language);
        }
        $this->emailerFactory = Registry::$emailerFactory;
    }

    /**
     * Adding custom data in case of extend
     * @return void
     */
    protected function addCustomData()
    {

    }

    /**
     * @return int
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @param int $parentId
     * @return void
     */
    public function setParentId($parentId)
    {
        if ($this->parentId !== $parentId) {
            $this->clearCache = TRUE;
        }
        $this->parentId = $parentId;
    }

    /**
     * @return int
     */
    public function getOrd()
    {
        return $this->ord;
    }

    /**
     * @param int $ord
     * @return void
     */
    public function setOrd($ord)
    {
        if ($this->ord !== $ord) {
            $this->clearCache = TRUE;
        }
        $this->ord = $ord;
    }

    /**
     * @return int
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * @param int $ord
     * @return void
     */
    public function setStatusId($statusId)
    {
        if ($this->statusId !== $statusId) {
            $this->clearCache = TRUE;
        }
        $this->statusId = $statusId;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return($this->nodeId);
    }

    /**
     * @return bool
     */
    public function exists()
    {
        return($this->nodeId > 0);
    }

    /**
     * @return bool
     */
    public function isNew()
    {
        return($this->nodeId == 0);
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return((bool) $this->isDeleted);
    }

    /**
     * @param boolean $cleanCache
     * @return int $nodeId
     */
    public function save($cleanCache = TRUE)
    {
        // check for existing parent
        $parentId = FApplication::$db->select('node_id')->from(TBL_NODES)->where('node_id=%i', $this->parentId)->execute()->fetchSingle();

        if (!(bool) $parentId) {
            throw new Exception('Parent not found: ' . $this->parentId);
        }

        $action = $this->nodeId ? 'update' : 'insert';

        $values = Array();
        $values['status_id'] = $this->statusId;
        $values['is_deleted'] = $this->isDeleted;
        $values['parent_id'] = $this->parentId;
        $values['front_controler'] = $this->frontControler;
        $values['admin_controler'] = $this->adminControler;
        $values['editor_id'] = FUser::getSignedIn()->getId();
        $values['dt_show'] = $this->dtShow;
        $values['is_dt_sensitive'] = $this->isDtSensitive;
        $values['dt_start'] = $this->dtStart;
        $values['dt_end'] = $this->dtEnd;
        $values['excludeFromSitemap'] = $this->excludeFromSitemap;
		$values['dtm'] = new DibiDateTime();

        // UPDATE
        if ($action == 'update') {
            $values['node_id'] = $this->getId();
            $values['level'] = $this->level;
            $values['ord'] = $this->ord;

            FApplication::$db->update(TBL_NODES, $values)->where('node_id=%i', $this->getId())->execute();

            // INSERT
        } else {
            $values['author_id'] = FUser::getSignedIn()->getId();
			$values['dtc'] = new DibiDateTime();

            // level
            $parentLevel = FApplication::$db->select('level')->from(TBL_NODES)->where('node_id=%i', $this->parentId)->execute()->fetchSingle();
            $values['level'] = $parentLevel + 1;

            // ord
            if (empty($this->ord)) {
//				$maxOrd = FApplication::$db->select('MAX(ord)')->from(TBL_NODES)->where('level=%i', $values['level'])->execute()->fetchSingle();
//				$values['ord'] = $maxOrd + 10;
                $values['ord'] = self::getNextOrd($this->parentId);
            } else {
                $values['ord'] = (int) $this->ord;
            }

            $lastId = FApplication::$db->insert(TBL_NODES, $values)->execute(dibi::IDENTIFIER);
            $this->nodeId = $lastId;
            $this->page->nodeId = $this->nodeId; // we need node id for page
        }

        $this->page->save(); // save the page
        //StaticCache::remove(self::$cacheKey); // remove from static cache
        // save custom
        $this->saveCustomData($action);

        // cache
        if ($this->clearCache == TRUE) {
            $cache = FApplication::getCache();
            $cache->clean(array(Cache::TAGS => 'node'));
        }
        return $this->nodeId;
    }

    /**
     * @param int $parentId
     * @return int $ord
     */
    public static function getNextOrd($parentId)
    {
        $maxOrd = FApplication::$db->select('MAX(ord)')->from(TBL_NODES)->where('parent_id=%i', $parentId)->and('is_deleted=0')->execute()->fetchSingle();
        $ord = $maxOrd + 10;
        return $ord;
    }

    /**
     * For saving custom data
     * @param string $action
     * @return void
     */
    protected function saveCustomData($action)
    {

    }

    /**
     * @param bool $hardDelete
     * @return void
     */
    public function delete($hardDelete = FALSE)
    {
        // check if exists
        if (!$this->exists()) {
            return FALSE;
        }

        # if its only first delete
        if (!$hardDelete && !$this->isDeleted()) {
            $this->isDeleted = 1;
            $this->save();
        } else {
            // delete all node pages
            FPage::deleteNodePages($this->nodeId);

            FApplication::$db->delete(TBL_NODES)->where('node_id=%i', $this->getId())->execute();
        }

        // delete cache
        $cache = FApplication::getCache();
        $cache->clean(array(Cache::TAGS => 'node'));
    }

    /**
     * @return boolean
     */
    public function isOk2show()
    {
        // no front controler
        if (is_null($this->frontControler) || empty($this->frontControler)) {
            return FALSE;
            // is exists
        } elseif (!$this->exists()) {
            return FALSE;
            // has been deleted - it's in bin
        } elseif ($this->isDeleted()) {
            return FALSE;
            // check status pending
        } elseif ($this->statusId == self::STATUS_PENDING) {
            return FALSE;
            // check status draft
        } elseif ($this->statusId == self::STATUS_DRAFT && !FUser::isSignedIn()) {
            return FALSE;
            // showing date
        } elseif ($this->dtShow > date('Y-m-d G:i:s')) {
            return FALSE;
            // sensitive to date
        } elseif ($this->isDtSensitive && ($this->dtStart > date('Y-m-d G:i:s') || $this->dtEnd < date('Y-m-d'))) {
            return FALSE;
        }
        return TRUE;
    }

    /**
     * @return string
     */
    public function getLngSeoTitle()
    {
        return($this->page->getSeoTitle());
    }

    /**
     * @return string
     */
    public function getLngTitle()
    {
        return empty($this->page->alternativeTitle) ? $this->page->title : $this->page->alternativeTitle;
    }

    /**
     * @return string
     */
    public function getLngAbstract()
    {
        return($this->page->abstract);
    }

    /**
     * @return string
     */
    public function getLngText()
    {
        return($this->page->text);
    }

    /**
     * @return string
     */
    public function getLngKeywords()
    {
        return($this->page->getKeywords());
    }

    /**
     * @return string
     */
    public function getLngDescription()
    {
        return($this->page->getDescription());
    }

    /**
     * @return string
     */
    public function getLngFriendlyUrl()
    {
        return $this->page->friendlyUrl;
    }

    /**
     * @param int $nodeId
     * @param bool $nested
     * @param array $arr
     * @return array
     */
    static private function _getChildsIds($nodeId = NULL, $nested = TRUE, $arr = array())
    {
        // 1.time
        if (empty($arr)) {
            $nodeId = is_Null($nodeId) ? FApplication::$nodeId : $nodeId;
            $arr = FMenu::get((int) $nodeId);
        }

        $ids = Array();
        foreach ($arr as $key => $val) {
            $ids[] = $key;
            // if we are asking for nested childs and has childs
            if ($nested && !empty($val['childs'])) {
                $ids = array_merge($ids, self::_getChildsIds($nodeId, $nested, $val['childs']));
            }
        }

        return $ids;
    }

    /**
     * @param int $nodeId
     * @param bool $nested
     * @return array
     */
    static public function getChildsIds($nodeId = NULL, $nested = TRUE)
    {
        # cache
        $cacheKey = 'node_childs_ids_' . (int) $nodeId . '_' . (int) $nested;
        if (StaticCache::read($cacheKey)) {
            return StaticCache::read($cacheKey);
        }

        $ids = self::_getChildsIds($nodeId, $nested);
        StaticCache::write($cacheKey, $ids); // add to cache
        return $ids;
    }

    /**
     * @param int $nodeId
     */
    static public function getChildsNodes($nodeId = NULL, $nested = TRUE, $nodeObject = 'FNode')
    {
        # cache
        $cacheKey = 'node_childs_nodes_' . (int) $nodeId . '_' . (int) $nested;
        if (StaticCache::read($cacheKey)) {
            return StaticCache::read($cacheKey);
        }

        $ids = self::getChildsIds($nodeId, $nested);
        $nodes = Array();
        foreach ($ids as $key => $val) {
            $nodes[$val] = new $nodeObject($val);
        }

        StaticCache::write($cacheKey, $nodes); // add to cache
        return($nodes);
    }

    /**
     * Method do recount node levels, e.g. if there is any parent changes
     * @param array $nodes
     * @param int $level
     */
    static public function recountNodeLevels($nodes, $level)
    {
        // call recursive function
        self::_recountNodeLevels($nodes, $level);
        // delete cache for tag node
        $cache = FApplication::getCache();
        $cache->clean(array(Cache::TAGS => 'node'));
    }

    /**
     * @param array $nodes
     * @param int $level
     */
    static public function _recountNodeLevels($nodes, $level)
    {
        foreach ($nodes as $key => $val) {
            // data to update
            $data = array();
            $data['node_id'] = $key;
            $data['level'] = (int) $level + 1;
            // update data for node
            FApplication::$db->update(TBL_NODES, $data)->where('node_id=%i', $key)->execute();

            // if there are any childs
            if (!empty($val['childs'])) {
                self::_recountNodeLevels($val['childs'], $level + 1);
            }
        }
    }

    /**
     * Method do recount node order - if there is any parent changes
     * @param array $nodes
     * @param int $level
     */
    static public function recountNodeOrders($nodes, $step = 10)
    {
        // call recursive function
        self::_recountNodeOrders($nodes, $step);
        // delete cache for tag node
        $cache = FApplication::getCache();
        $cache->clean(array(Cache::TAGS => 'node'));
    }

    /**
     * @param array $nodes
     * @param int $level
     */
    static public function _recountNodeOrders($nodes, $step)
    {
        $order = 0;
        foreach ($nodes as $key => $val) {
            $order += $step;
            // data to update
            $data = array();
            $data['node_id'] = $key;
            $data['ord'] = $order;
            // update data for node
            FApplication::$db->update(TBL_NODES, $data)->where('node_id=%i', $key)->execute();

            // if there are any childs
            if (!empty($val['childs'])) {
                self::_recountNodeOrders($val['childs'], $step);
            }
        }
    }

    /**
     * reordering nodes
     * @param array $nodes
     * @param int $level
     */
    static public function recountNodeObjectOrders($nodes, $step = 10)
    {
        $order = 0;
        foreach ($nodes as $key => $node) {
            $order += $step;
            $node->ord = $order;
            $node->save();
        }
    }

    /**
     * return product object based on admin_controler and handleObject
     * @param string $controler
     * @param int $productId
     * @return mixed
     */
    static public function getProductByAdminControler($controler, $productId)
    {
        $vars = get_class_vars($controler);
        $product = isset($vars['handleObject']) ? new $vars['handleObject']($productId) : new FNode($productId);
        return $product;
    }

    /**
     * return product object based on admin_controler and handleObject
     * @param int $productId
     * @return mixed
     */
    static public function getProductById($productId)
    {
        $controler = FApplication::$db->select('admin_controler')->from(TBL_NODES)->where('node_id=%i', $productId)->execute()->fetchSingle();
        $product = self::getProductByAdminControler($controler, $productId);
        return $product;
    }

    /**
     * Returns value from FProperties
     * @param string $key
     * @param int $nodeId
     * @return string
     */
    public function getProperty($key, $nodeId = NULL)
    {
        if ($nodeId == NULL)
            $nodeId = $this->getId();
        return FProperty::getNodePropsValue($key, $nodeId);
    }

    /**
     * Saving to FProperty value
     * @param string $key
     * @param string $value
     * @param int $nodeId
     * @return void
     */
    public function saveProperty($key, $value, $nodeId = NULL)
    {
        $value = (string) $value;
        if ($nodeId == NULL)
            $nodeId = $this->getId();
        FProperty::get($key, $nodeId)->setValue($value)->save();
    }

    /**
     * Returns array title and link
     *
     * @param mixed $ids - number, or array
     * @return array
     */
    static public function getTitleAndLink($ids)
    {
        $ids = (array) $ids;
        $result = FApplication::$db->select('n.node_id, p.language_pk, p.title, p.friendly_url')
            ->from(TBL_NODES . ' n')
            ->leftJoin(TBL_PAGES . ' p')->on('n.node_id=p.node_id')
            ->where('n.node_id')->in('%l', $ids)
            ->and('language_pk=%s', FApplication::$lang);

        $ret = array();
        $result = $result->execute()->fetchAssoc('node_id');
        foreach ($ids as $key => $val) {
            if (isset($result[$val])) {
                $ret[$val] = array(
                    'title' => $result[$val]->title,
                    'link' => FRouter::getUrl($result[$val]->friendly_url, $result[$val]->node_id, $result[$val]->language_pk),
                );
            }
        }
        return $ret;
    }

    /**
     * Returns link to current node
     * @return string
     */
    public function getLink()
    {
        $link = FRouter::getUrl($this->getLngFriendlyUrl(), $this->nodeId, FApplication::$lang);
        return $link;
    }

    /**
     * Get title visibility
     * @return title or FALSE
     */
    public function getVisibleTitle()
    {
        if (!$this->getProperty('hasVisibleTitle')) {
            return TRUE;
        }
        return FALSE;
    }

}
