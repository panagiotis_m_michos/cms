<?php

use RouterModule\Generators\ILegacyUrlGenerator;
use Utils\Text\Parser\Filters\Interfaces\IRouter;

class FRouter extends Object implements IRouter, ILegacyUrlGenerator
{

    /** @var boolean */
    private $hasRoute = FALSE;

    /** @var boolean */
    private $notFound = FALSE;


    /**
     * Handle with url: if there is missing / at the end -> redirect
     * @return void
     */
    public function __construct()
    {
        // check if path ending on slash, if not, just redirect
        if (FApplication::hasAllowedFriendlyUrl()) {
            $path = FApplication::$httpRequest->uri->path;
            if (!preg_match('#\.html|\.php$#', $path) && substr($path, -1) != '/') {
                Header('Location: ' . $path . '/');
                exit;
            }
        }
    }

    /**
     * Returns route variables as $nodeid, $lang, $action
     * @return array
     */
    private function getRoute()
    {
        // cache
        $cacheKey = 'router_get_route';
        if (StaticCache::read($cacheKey)) {
            return StaticCache::read($cacheKey);
        }
        // check if there is any route in url
        if (!empty(FApplication::$get['route'])) {
            $this->hasRoute = TRUE;
        }
        // admin
        if (FApplication::isAdmin()) {
            $route = [];
            if (!empty(FApplication::$get['route'])) {
                $temp = FApplication::$get['route'];
                if (substr($temp, -1) == '/') {
                    $temp = substr($temp, 0, -1);
                }
                $temp = explode('/', $temp);
                $route['lang'] = isset($temp[0]) ? $temp[0] : FApplication::$config['default_lng'];
                $route['nodeId'] = isset($temp[1]) ? $temp[1] : FApplication::$config['home_node'];
                $route['action'] = isset($temp[2]) ? $temp[2] : NULL;

            } else {
                $route['lang'] = FApplication::$config['default_lng'];
                $route['nodeId'] = FApplication::$config['home_node'];
                $route['action'] = NULL;
            }
            // route
        } else {
            $route = [];
            if (FApplication::hasAllowedFriendlyUrl()) {
                // home
                if (!isSet(FApplication::$get['friendly_url'])) {
                    $route['lang'] = FApplication::$config['default_lng'];
                    $route['nodeId'] = FApplication::$config['home_node'];
                    $route['action'] = NULL;
                    // find friendly url from database
                } else {

                    $w = FApplication::$db->select('node_id, language_pk')
                        ->from(TBL_PAGES)
                        ->where('friendly_url=%s', FApplication::$get['friendly_url'])
                        ->execute()->fetch();

                    if (!empty($w)) {
                        $route['nodeId'] = $w['node_id'];
                        $route['lang'] = $w['language_pk'];
                        $route['action'] = NULL;
                    } else {
                        $this->notFound = TRUE;
                        $route['nodeId'] = NULL;
                        $route['lang'] = FApplication::$config['default_lng'];
                        $route['action'] = NULL;
                    }
                }
                // just e.g index.php?id=23
            } else {
                $route['lang'] = !isSet(FApplication::$get['lng']) ? FApplication::$config['default_lng'] : FApplication::$get['lng'];
                $route['nodeId'] = !isSet(FApplication::$get['id']) ? FApplication::$config['home_node'] : (int)FApplication::$get['id'];
                $route['action'] = NULL;
            }
        }
        // add to cache
        StaticCache::write($cacheKey, $route);
        return $route;
    }


    /**
     * Returns controler for application
     * @return object instance of Controler
     */
    public function getControler()
    {
        // admin controler
        if (FApplication::isAdmin()) {
            // cache
            $cacheKey = 'router_controler_admin_' . $this->getCurrentNodeId();
            if (StaticCache::read($cacheKey)) {
                return StaticCache::read($cacheKey);
            }
            // has to signin
            if (!$this->hasRoute) {
                $controler = new AuthAdminControler;
            } else {
                $controler = FApplication::$db->select('front_controler, admin_controler')
                    ->from(TBL_NODES . ' n')
                    ->join(TBL_PAGES . ' p')->on('n.node_id=p.node_id')
                    ->where('n.node_id=%i', $this->getCurrentNodeId())->and('p.language_pk=%s', $this->getCurrentLanguage())
                    ->execute()->fetch();

                // which controler
                if ($controler['admin_controler'] !== NULL) {
                    $controler = new $controler['admin_controler'];
                } else {
                    $this->notFound = TRUE;
                    return new ErrorAdminControler;
                }
            }
            // add to cache
            StaticCache::write($cacheKey, $controler); // add to cache
            return $controler;
            // front controler
        } else {
            // cache
            $cacheKey = 'router_controler_front_' . $this->getCurrentNodeId();
            if (StaticCache::read($cacheKey)) {
                return StaticCache::read($cacheKey);
            }
            // not found
            if ($this->notFound) {
                return new ErrorControler;
            } else {
                $controler = FApplication::$db->select('front_controler, admin_controler')
                    ->from(TBL_NODES)
                    ->where('node_id=%i', $this->getCurrentNodeId())
                    ->execute()->fetch();

                if (!empty($controler['front_controler'])) {
                    $controler = new $controler['front_controler'];
                } else {
                    return new ErrorControler;
                }
            }
            // if isn't ok to show - error presenter
            if (!$controler->node->isOk2Show()) {
                $controler = new ErrorControler();
            }
            // add to cache
            StaticCache::write($cacheKey, $controler); // add to cache
            return $controler;
        }
    }


    /**
     * Returns current node id from route
     * @return integer
     */
    public function getCurrentNodeId()
    {
        $route = $this->getRoute();
        return (int)$route['nodeId'];
    }


    /**
     * Returns current lang from route
     * @return string
     */
    public function getCurrentLanguage()
    {
        $route = $this->getRoute();
        return strtolower($route['lang']);
    }


    /**
     * Returns current action from route
     * @return string
     */
    public function getCurrentAction()
    {
        $route = $this->getRoute();
        return $route['action'];
    }


    /**
     * Returns current view from route
     * @return string
     */
    public function getCurrentView()
    {
        $route = $this->getRoute();
        return $route['action'];
    }


    /**
     * Returns links depends on $code and add params to URL
     *
     * Legend for code:
     * ================
     * :string     = lang
     * int         = nodeId
     * string     = action
     * %string%  = url
     * #action#  = action on front (current controler)
     *
     * @param mixed $code
     * @param mixed $params
     * @return string
     */
    public function link($code = NULL, $params = NULL)
    {
        // wraper for template
        if ($code === 'NULL' || empty($code)) $code = NULL;
        // params
        if (!empty($params)) {
            $params = func_get_args();
            unset($params[0]);
            foreach ($params as $key => $val) {
                if (is_array($val)) {
                    unset($params[$key]);
                    $params = array_merge($params, $val);
                }
            }
        } else {
            $params = NULL;
        }

        return $this->_link($code, $params);
    }

    /**
     * Returns front links depends on $code and add params to URL
     *
     * Legend for code:
     * ================
     * :string     = lang
     * int         = nodeId
     * string     = action
     * %string%  = url
     * #action#  = action on front (current controler)
     *
     * @param mixed $code
     * @param mixed $params
     * @return string
     */
    public function frontLink($code = NULL, $params = NULL)
    {
        $args = func_get_args();
        unset($args[0]);
        $url = call_user_func([$this, "_link"], $code, $args, 'front');
        return $url;
    }

    /**
     * Returns absolute front links depends on $code and add params to URL
     *
     * Legend for code:
     * ================
     * :string     = lang
     * int         = nodeId
     * string     = action
     * %string%  = url
     * #action#  = action on front (current controler)
     *
     * @param mixed $code
     * @param mixed $params
     * @return string
     */
    public function absoluteFrontLink($code = NULL, $params = NULL)
    {
        $args = func_get_args();
        unset($args[0]);
        $url = call_user_func([$this, "_link"], $code, $args, 'front');
        return 'https://' . FApplication::$host . $url;
    }


    /**
     * Returns admin links depends on $code and add params to URL
     *
     * Legend for code:
     * ================
     * :string     = lang
     * int         = nodeId
     * string     = action
     * %string%  = url
     * #action#  = action on front (current controler)
     *
     * @param mixed $code
     * @param mixed $params
     * @return string
     */
    public function adminLink($code = NULL, $params = NULL)
    {
        $args = func_get_args();
        unset($args[0]);
        $url = call_user_func([$this, "_link"], $code, $args, 'admin');
        return $url;
    }

    /**
     * Returns absolute admin links depends on $code and add params to URL
     *
     * Legend for code:
     * ================
     * :string     = lang
     * int         = nodeId
     * string     = action
     * %string%  = url
     * #action#  = action on front (current controler)
     *
     * @param mixed $code
     * @param mixed $params
     * @return string
     */
    public function absoluteAdminLink($code = NULL, $params = NULL)
    {
        $args = func_get_args();
        unset($args[0]);
        $url = call_user_func([$this, "_link"], $code, $args, 'admin');
        return 'https://' . FApplication::$host . $url;
    }

    /**
     * Returns full https links depends on $code and add params to URL
     *
     * Legend for code:
     * ================
     * :string     = lang
     * int         = nodeId
     * string     = action
     * %string%  = url
     * #action#  = action on front (current controler)
     *
     * @param mixed $code
     * @param mixed $params
     * @return string
     */

    public function secureLink($code = NULL, $params = NULL)
    {
        $args = func_get_args();
        unset($args[0]);
        $url = call_user_func([$this, "_link"], $code, $args);
        return 'https://' . FApplication::$host . $url;
    }


    /**
     * Returns link by $code, params and zone
     *
     * @param mixed $code
     * @param mixed $params
     * @param string $zone
     * @return string
     */
    private function _link($code = NULL, $params = NULL, $zone = 'detect')
    {
        // explodre parameters
        if (!empty($params)) {
            foreach ($params as $key => $val) {
                if (!empty($val)) {
                    if (is_array($val)) {
                        unset($params[$key]);
                        $params = array_merge($params, $val);
                    }
                } else {
                    unset($params[$key]);
                }
            }
        }

        // defaults
        $nodeId = $this->getCurrentNodeId();
        $lang = $this->getCurrentLanguage();
        $action = $this->getCurrentAction();

        // has any code for url
        if (!is_null($code)) {

            // front action in the same controler #action#
            if (String::startsWith($code, '#') && String::endsWith($code, '#')) {
                $possibleActions = array_flip(FApplication::$controler->possibleActions);
                $code = str_replace('#', '', $code);
                if (isset($possibleActions[$code])) {
                    $nodeId = $possibleActions[$code];
                    $node = new FNode($nodeId);
                    $friendlyUrl = $node->getLngFriendlyUrl();
                    $url = self::getUrl($friendlyUrl, $nodeId, NULL);
                    $url = FTools::addUrlParam($params, $url);
                    return $url;
                }

                // url in code %url%
            } elseif (String::startsWith($code, '%') && String::startsWith($code, '%')) {
                $url = substr($code, 1, -1);
                // as a constant
                if (defined($url)) $url = eval("return $url;");
                $url = FTools::addUrlParam($params, $url);
                return $url;
                // code for construct url
            } else {
                $actionDefined = FALSE;
                $code = explode(' ', $code);
                foreach ($code as $key => $val) {
                    // lang
                    if (String::startsWith($val, ':')) {
                        $lang = substr($val, 1);
                        $lang = strtolower($lang);
                        continue;
                    }
                    // node id
                    if (is_numeric($val) || defined($val)) {
                        $nodeId = defined($val) ? eval("return $val;") : $val;
                        continue;
                    }
                    // action
                    $action = $val;
                    $actionDefined = TRUE; // changed 19/06/2009
                }

                // admin
                if (FApplication::isAdmin() && $zone != 'front' || $zone == 'admin') {
                    $url = ADMIN_URL_ROOT . $lang . '/' . $nodeId . '/';
                    if (!empty($action) && $actionDefined) $url .= $action . '/'; // changed 19/06/2009
                    //if (!empty($action)) $url .= $action . '/';
                    $url = FTools::addUrlParam($params, $url);
                    return $url;
                    // front
                } else {
                    // SEO router
                    $node = new FNode($nodeId, $lang);
                    $friendlyUrl = $node->getLngFriendlyUrl();
                    $url = self::getUrl($friendlyUrl, $nodeId, $lang);
                    $url = FTools::addUrlParam($params, $url);
                    return $url;
                }
            }

            // use current url address
        } else {
            if (FApplication::isAdmin() && $zone != 'front' || $zone == 'admin') {
                $uri = FApplication::$httpRequest->uri;
                $url = $uri->path;
                $url = FTools::addUrlParam($params, $url);
            } else {
                $uri = FApplication::$httpRequest->uri;
                $url = $uri->path;
            }
            if (!empty($uri->query)) $url .= '?' . $uri->query;
            $url = FTools::addUrlParam($params, $url);
            return $url;
        }
    }

    /**
     * @param string $friendlyUrl
     * @param int $nodeId
     * @param string $lang
     * @return string
     */
    public static function getUrl($friendlyUrl, $nodeId, $lang)
    {
        if (FApplication::hasAllowedFriendlyUrl()) {
            $node = new FNode($nodeId, $lang);
            $friendlyUrl = $node->getLngFriendlyUrl();
            $url = $friendlyUrl == '/' ? URL_ROOT : URL_ROOT . $friendlyUrl;
            // simple router
        } else {
            $url = URL_ROOT . '?id=' . $nodeId . '&lng=' . $lang;
        }
        return $url;
    }
}
