<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   CMS
 * @package    Cms\File
 * @version    File.php 2009-02-11 divak@gmail.com
 */

/*namespace Cms\File;*/


/**
 * Work with uploaded files
 * 
 * @author     Stanislav Bazik
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @package    Cms\File
 */

class File extends Object
{
	/** @var int */
	public $fileId;

	/** @var boolean */
	public $isActive;
	
	/** @var boolean */
	public $isDeleted;

	/** @var int */
	public $ord;

	/** @var int */
	public $nodeId;

	/** @var string */
	public $name;
	
	/** @var string */
	public $description;

	/** @var string */
	public $ext;

	/** @var int */
	public $size;

	/** @var int */
	public $authorId;

	/** @var int */
	public $editorId;

	/** @var string */
	public $dtc;

	/** @var string */
	public $dtm;
	
	/** @var string */
	public $nameOld;

	/** @var array */
	public $uploadedFile = NULL;
	
	/** @var string */
	public $localFile = NULL;
	

	/**
	 * @param int $fileId
	 * @return void
	 */
	public function __construct($fileId = 0)
	{
		$this->fileId = (int)$fileId;
		if ($this->fileId) {
			$w = FApplication::$db->select('*')->from(TBL_FILES)->where('is_img=0')->and('file_id=%i', $this->fileId)->execute()->fetch(); 
			if (!empty($w)) {
				$this->fileId = $w['file_id'];
				$this->isActive = $w['is_active'];
				$this->isDeleted = $w['is_deleted'];
				$this->ord = $w['ord'];
				$this->nodeId = $w['node_id'];
				$this->name = $w['name'];
				$this->nameOld = $w['name'];
				$this->description = $w['description'];
				$this->ext = $w['ext'];
				$this->size = $w['size'];
				$this->dtc = $w['dtc'];
				$this->dtm = $w['dtm'];
				$this->authorId = $w['author_id'];
				$this->editorId = $w['editor_id'];
			} else {
				$this->fileId = ($this->fileId > 0) ? -1 * $this->fileId : $this->fileId;
			}
		}
	}

	/**
	 * @param int $fileId
	 * @return object
	 */
	static public function create($fileId = 0)
	{
		return new self($fileId);
	}

	/**
	 * @return int
	 */
	public function getId() 
	{ 
		return $this->fileId; 
	}

	/**
	 * @return bool
	 */
	public function exists() 
	{ 
		return (bool)$this->fileId > 0; 
	}

	/**
	 * @return bool
	 */
	public function isNew() 
	{ 
		return (bool)$this->fileId == 0; 
	}

	/**
	 * @return bool
	 */
	public function isActive() 
	{ 
		return (bool)$this->isActive; 
	}
	
	/**
	 * @return bool
	 */
	public function isDeleted() 
	{ 
		return (bool)$this->isDeleted; 
	}
	
	/**
	 * @return bool
	 */
	public function isUploaded() 
	{ 
		if ($this->uploadedFile !== NULL && isset($this->uploadedFile['tmp_name']) && is_uploaded_file($this->uploadedFile['tmp_name'])) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	/**
	 * @return bool
	 */
	public function isLocalFile() 
	{ 
		if ($this->localFile !== NULL && is_file($this->localFile)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
		
	/*
	 * @return int
	 */
	public function save()
	{
		// is dir writeable
		if (!is_dir(FILES_DIR) || !is_writable(FILES_DIR)) {
			throw new Exception('Files directory "' . FILES_DIR . '" is not writable.');
		}
		
		// pathinfo
		if (!function_exists('pathinfo')) {
			throw new Exception('function pathinfo() could not be found.');
		}
		
		// info about file
		$file = $this->getFileInfo();
		
		// data to save
		$values = array();
		$values['is_active'] = $this->isActive;
		$values['is_deleted'] = $this->isDeleted;
		$values['ord'] = $this->ord;
		$values['node_id'] = $this->nodeId;
		$values['dtm'] =  $this->dtm;
		$values['editor_id'] = FUser::getSignedIn()->getId();
		$values['name'] = $file['name'];
		$values['description'] = $file['description'];
		$values['ext'] = $file['ext'];
		$values['size'] = $file['size'];
		$values['dtm'] = new DibiDateTime();
		
		// move_uploaded_file or rename
		if ($this->isUploaded()) {
			@move_uploaded_file($file['source'], FILES_DIR . $file['filename']);
		// local file
		} elseif ($this->isLocalFile()) {
			@rename($file['source'], FILES_DIR . $file['filename']);
		// rename file
		} elseif ($this->name != $this->nameOld) {
			@rename($file['source'], FILES_DIR . $file['filename']);
		}
		
		// delete old file
		if (!$this->isNew() && $this->nameOld != $file['name']) {
			if(is_file(FILES_DIR . $this->getFileName())) {
				@unlink(FILES_DIR . $this->getFileName());
			}
		}
		
		// insert
		if ($this->isNew()) {
			$values['dtc'] =  new DibiDateTime();
			$values['author_id'] = FUser::getSignedIn()->getId();;
			$this->fileId = FApplication::$db->insert(TBL_FILES, $values)->execute(dibi::IDENTIFIER);
		} else {
			FApplication::$db->update(TBL_FILES, $data)->where('fileId=%i', $this->fileId)->execute();
		}
		return $this->fileId;
	}


	/**
	 * @return void
	 */
	public function delete()
	{
		// if its only first delete
		if (!$this->isDeleted()) {
			$this->isDeleted = 1;
			$this->save();
		} else {
			if (is_file(FILES_DIR . $this->getFileName())) { // delete file from dir
				@unlink(FILES_DIR . $this->getFileName());
			}
			// delete file from db
			FApplication::$db->delete(TBL_FILES)->where('id=%i', $this->getId())->execute();
		}
	}
	
	/**
	 * @param string $name
	 * @param string $ext
	 * @param string $dir
	 * @return string $newName
	 */
	static public function getUniqFilename($name, $ext, $dir=FILES_DIR)
	{
		$poc=0;
		$newName = $name;
		while (is_file($dir . $newName . '.' . $ext)) {
			$newName = $name.' ('.++$poc.')';
		}
		return $newName;
	}

	/**
	 * @param array $file
	 * @return array $ret
	 */
	protected function getFileInfo()
	{
		// edit, but no file change
		if (!$this->isNew() && (is_Null($file) || empty($file))) {
			$ret=array();
			$ret['name'] = !empty($this->name) ? $this->name : $this->name_old;
			$ret['ext'] = strtolower($this->ext); 
			$ret['size'] = $this->size;
			$ret['source'] = FILES_DIR . $this->getFileName();
			
			// new filename
			if ($this->name != $this->name_old && !empty($this->name)) {
				$ret['name'] = String::webalize($ret['name']); // safe name
				$ret['name'] = self::getUniqFilename($ret['name'], $ret['ext']); // uniq name
			}
			$ret['filename'] = $ret['name'] . '.' . $ret['ext'];
		// new file
		} else {
			// uploaded
			if ($this->isUploaded()) {
				$info = pathinfo($file['name']);
				$info['size'] = $file['size'];
				$ret['source'] = $file['tmp_name'];
				
			// file e.g. from import
			} elseif ($this->isLocalFile()) { 
				$info = pathinfo($file);
				$info['size'] = filesize($file);
				$ret['source'] = $file;
			}
			
			// return data
			$ret=array();
			$ret['ext'] = strtolower($info['extension']);
			$ret['size'] = $info['size'];
			$ret['name'] = !empty($this->name) ? $this->name : $info['filename'];
			
			// new filename
			if ($this->isNew() || !$this->isNew() && $this->name != $this->name_old) {
				$ret['name'] = String::webalize($ret['name']); // safe name
				$ret['name'] = self::getUniqFilename($ret['name'], $ret['ext']); // uniq name
			}
			
			$ret['filename'] = $ret['name'] . '.' . $ret['ext'];
		}
		
		return $ret;
	}

	/**
	 * @return string
	 */
	public function getFileName()
	{
		return $this->nameOld . '.' . $this->ext;
	}
	
	/**
	 * @return string
	 */
	public function getFilePath()
	{
		return FILES_DIR . $this->name_old . '.' . $this->ext;
	}
	
	/**
	 * @return string
	 */
	public function getFilePathR()
	{
		return FILES_DIR_R . $this->nameOld . '.' . $this->ext;
	}
	
	/**
	 * @param int $node_id
	 * @return int $maxOrd
	 */
	static public function getNextOrd($nodeId)
	{
		$maxOrd = Aplication::$db->getOne('
			SELECT MAX(`ord`) 
			FROM '.TBL_FILES.' 
			WHERE `is_img`=0 
				AND node_id='.(int)$nodeId
		);
		
		$nextord = (int)$maxOrd + 10;
		return $nextord;
	}
	
	/**
	 * @param boolean $modified
	 * @return string $size
	 */
	public function getSize($modified=TRUE)
	{
		if ($modified) {
			return String::bytes($this->size);
			
		} else {
			return $this->size;
		}
	}
	
	/**
	 * @param boolean $modified
	 * @return string $dtc
	 */
	public function getDtc($modified=TRUE)
	{
		if ($modified) {
			return date('j/m/Y G:i',strtotime($this->dtc));
			
		} else {
			return $this->dtc;
		}
	}
	
	/**
	 * @param int $nodeId
	 * @return array $files
	 */
	static public function getNodeFiles($nodeId)
	{
		$ids = Aplication::$db->getCol('
			SELECT * 
			FROM '.TBL_FILES.' 
			WHERE `is_img`=0 
				AND `node_id`='.(int)$nodeId
		);

		$files=array();
		foreach ($ids as $key=>$val) {
			$files[] = new self($val);
		}
		
		return $files;
	}
	
	/**
	 * @return mixed
	 */
	public function getHtmlTag()
	{
		// doesn't exists
		if(!$this->exists()) {
			return NULL;
		}
		
		$el = Html::el('a')
			->href($this->getFilePathR())
			->tilte($this->getFilePath())
			->setText($this->getFileName());
		
		return $el->render();
	}
}
