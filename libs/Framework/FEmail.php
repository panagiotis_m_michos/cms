<?php

class FEmail extends FNode
{
    /** @var string */
    public $from;

    /** @var string */
    public $fromName;

    /** @var array */
    private $to = [];

    /** @var array */
    private $cc = [];

    /** @var array */
    private $bcc = [];

    /** @var array */
    private $attachments = [];

    /** @var array */
    private $cmsFileAttachments = [];

    /** @var string */
    public $subject;

    /** @var string */
    public $body;

    /**
     * @var string
     */
    private $textBody = '';

    /**
     * @var array
     */
    private $additionalData = [];

    /**
     * @return array
     **/
    public function getFromArr()
    {
        return (array) $this->from;
    }

    /**
     * @return mixed
     */
    public function getTo()
    {
        return $this->_get('to');
    }

    /**
     * @return array
     **/
    public function getToArr()
    {
        return $this->to;
    }

    /**
     * @return mixed
     */
    public function getCc()
    {
        return $this->_get('cc');
    }

    /**
     * @return array
     **/
    public function getCCArr()
    {
        return $this->cc;
    }

    /**
     * @return mixed
     */
    public function getBcc()
    {
        return $this->_get('bcc');
    }

    /**
     * @return array
     **/
    public function getBccArr()
    {
        return $this->bcc;
    }

    /**
     * @param mixed $to
     * @return void
     */
    public function setTo($to)
    {
        $this->_set('to', $to);
    }

    /**
     * @param mixed $cc
     * @return void
     */
    public function setCc($cc)
    {
        $this->_set('cc', $cc);
    }

    /**
     * @param mixed $bcc
     * @return void
     */
    public function setBcc($bcc)
    {
        $this->_set('bcc', $bcc);
    }

    /**
     * @param string $to
     * @return void
     */
    public function addTo($to)
    {
        $this->_add('to', $to);
    }

    /**
     * @param string $cc
     * @return void
     */
    public function addCc($cc)
    {
        $this->_add('cc', $cc);
    }

    /**
     * @param string $bcc
     * @return void
     */
    public function addBcc($bcc)
    {
        $this->_add('bcc', $bcc);
    }

    /**
     * @param string $file
     * @param string|NULL $content
     * @param string|NULL $contentType
     */
    public function addAttachment($file, $content = NULL, $contentType = NULL)
    {
        $this->attachments[] = [
            'file' => $file,
            'content' => $content,
            'contentType' => $contentType,
        ];
    }

    /**
     * @return array
     **/
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * @param $cmsFileattachment
     */
    public function addCmsFileAttachment($cmsFileattachment)
    {
        $this->cmsFileAttachments[] = $cmsFileattachment;
    }

    /**
     * @return array
     */
    public function getCmsFileAttachments()
    {
        return $this->cmsFileAttachments;
    }


    /**
     * @param array $attachments
     * @return array
     */
    public function setCmsFileAttachments(array $attachments)
    {
        $this->cmsFileAttachments = $attachments;
    }


    /**
     * @param string $property
     * @return mixed
     */
    private function _get($property)
    {
        if (is_array($this->$property) && !empty($this->$property)) {
            $value = implode(',', $this->$property);

            return $value;
        }

        return NULL;
    }

    /**
     * @param string $property
     * @param mixed $value
     * @throws Exception
     */
    private function _set($property, $value)
    {
        if (!is_array($value)) {
            $value = explode(',', $value);
            foreach ($value as $key => &$val) {
                $val = trim($val);
            }
            unset($val);
            $this->$property = $value;
        } elseif (is_array($value)) {
            $this->$property = $value;
        } else {
            throw new Exception("$value has to be array or comma separated string!");
        }
    }

    /**
     * @param string $property
     * @param string $value
     * @return void
     */
    private function _add($property, $value)
    {
        // comma separated
        if (preg_match('#,#', $value)) {
            if (!empty($value)) {
                $value = explode(',', $value);
                foreach ($value as $key => $val) {
                    $this->{"$property"}[] = trim((string) $val);
                }
            }
        } else {
            $this->{"$property"}[] = trim((string) $value);
        }
    }

    /**
     * @return array
     */
    public function getAdditionalData()
    {
        return $this->additionalData;
    }

    /**
     * @param array $additionalData
     */
    public function setAdditionalData($additionalData)
    {
        $this->additionalData = $additionalData;
    }

    protected function addCustomData()
    {
        if ($this->adminControler != 'EmailAdminControler' && !is_subclass_of($this->adminControler, 'EmailAdminControler')) {
            throw new Exception("Node with id `{$this->getId()}` is not an email node.");
        }

        $this->from = $this->getProperty('from', $this->getId());
        $this->fromName = $this->getProperty('fromName', $this->getId());
        $this->subject = $this->getProperty('subject', $this->getId());

        $to = $this->getProperty('to', $this->getId());
        if (!empty($to)) {
            $this->to = explode(',', $to);
        }

        $cc = $this->getProperty('cc', $this->getId());
        if (!empty($cc)) {
            $this->cc = explode(',', $cc);
        }

        $bcc = $this->getProperty('bcc', $this->getId());
        if (!empty($bcc)) {
            $this->bcc = explode(',', $bcc);
        }

        $cmsFileAttachments = $this->getProperty('cmsFileAttachments');
        if (!empty($cmsFileAttachments)) {
            $this->cmsFileAttachments = explode(',', $cmsFileAttachments);
        }
        $this->body = $this->getLngText();
    }

    /**
     * @param string $action
     */
    public function saveCustomData($action)
    {
        FProperty::get('from', $this->getId())->setValue($this->from)->save();
        FProperty::get('fromName', $this->getId())->setValue($this->fromName)->save();

        $to = !empty($this->to) ? implode(',', $this->to) : NULL;
        FProperty::get('to', $this->getId())->setValue($to)->save();

        $cc = !empty($this->cc) ? implode(',', $this->cc) : NULL;
        FProperty::get('cc', $this->getId())->setValue($cc)->save();

        $bcc = !empty($this->bcc) ? implode(',', $this->bcc) : NULL;
        FProperty::get('bcc', $this->getId())->setValue($bcc)->save();

        $cmsFileAttachments = !empty($this->cmsFileAttachments) ? implode(',', $this->cmsFileAttachments) : NULL;
        $this->saveProperty('cmsFileAttachments', $cmsFileAttachments);

        FProperty::get('subject', $this->getId())->setValue($this->subject)->save();
    }

    /**
     * @param array $replace
     */
    public function replacePlaceHolders(array $replace)
    {
        $this->body = str_replace(array_keys($replace), $replace, $this->body);
    }

    /**
     * @param array $replace
     */
    public function replaceSubjectPlaceholders(array $replace)
    {
        $this->subject = str_replace(array_keys($replace), $replace, $this->subject);
    }

    /**
     * @param array $replace
     */
    public function replaceAllPlaceHolders(array $replace)
    {
        $this->replacePlaceHolders($replace);
        $this->replaceSubjectPlaceholders($replace);
    }

    /**
     * @return boolean
     */
    public function isEmail()
    {
        return $this->adminControler == 'EmailAdminControler' || is_subclass_of($this->adminControler, 'EmailAdminControler');
    }
}
