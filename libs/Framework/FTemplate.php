<?php

use TemplateModule\ITemplate;

class FTemplate extends Object implements ITemplate
{
    /**
     * @var Smarty
     */
    private $engine;

    /**
     * @var array
     */
    static private $container;

    /**
     * @param Smarty $engine
     * @throws SmartyException
     */
    public function __construct(Smarty $engine)
    {
        $this->engine = $engine;
    }

    /**
     * @param array $variables
     */
    public function setVariables(array $variables)
    {
        foreach ($variables as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * @param string $template
     * @return string
     */
    public function getHtml($template)
    {
        return $this->engine->fetch($template);
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function &__get($name)
    {
        return self::$container[$name];
    }

    /**
     * @param string $name
     * @param mixed $args
     * @return void
     */
    public function __set($name, $args)
    {
        self::$container[$name] = $args;
        $this->engine->assign($name, self::$container[$name]);
    }

    /**
     * @return Smarty
     */
    public function getEngine()
    {
        return $this->engine;
    }
}

