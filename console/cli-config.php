<?php

use Bootstrap\ApplicationLoader;
use BootstrapModule\Ext\DoctrineExt;
use Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper;
use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;
use Symfony\Component\Console\Helper\HelperSet;

define('DOCUMENT_ROOT', dirname(dirname(__FILE__)));

$config = require_once DOCUMENT_ROOT . '/project/bootstrap/default.php';
$config['show_errors'] = TRUE;
$config['config_path'] = DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'project/config/app/config.local.yml';

$customConfig = DOCUMENT_ROOT . '/console/console_dev.php';
if (file_exists($customConfig)) {
    require_once $customConfig;
}

$applicationLoader = new ApplicationLoader();
$container = $applicationLoader->load($config);

$em = $container->get(DoctrineExt::ENTITY_MANAGER);

$helperSet = new HelperSet(
    array(
        'db' => new ConnectionHelper($em->getConnection()),
        'em' => new EntityManagerHelper($em)
    )
);

set_time_limit(0);
ini_set('memory_limit', '1024M');
