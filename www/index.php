<?php

use Bootstrap\ApplicationLoader;
use Bootstrap\Ext\FrameworkExt;

define('DOCUMENT_ROOT', dirname(dirname(__FILE__)));

$config = require_once DOCUMENT_ROOT . '/project/bootstrap/default.php';
$config['config_path'] = DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'project/config/app/config.local.yml';

$applicationLoader = new ApplicationLoader();
$container = $application = $applicationLoader->load($config);
$container->get(FrameworkExt::APPLICATION)->run();
