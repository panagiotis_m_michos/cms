(function($, namespace) {

    "use strict";

    var DragDrop = function(url, dTree, loader) {
        this.serverUrl = url || null;
        this.loader = loader || {};
        this.dTree = dTree || {};
    }
    DragDrop.prototype = {
        initialMouseX: null,
        initialMouseY: null,
        startX: null,
        startY: null,
        draggedObject: null,
        currentTarget: null,
        //called when object is dropped
        onDrop : function(evt) {},
        //while dragging this method is constantly called
        onDrag : function(evt) {},
        initElement: function (element) {
            if (typeof element == 'string')
                element = document.getElementById(element);

            this.draggedObject = element;
            this.bindDragMouse = bind(this, this.startDragMouse);
            $(this.draggedObject).bind('mousedown', this.bindDragMouse);

        },
        startDragMouse: function (evt) {
            evt.preventDefault();
            var elementToDrag = evt.target;

            //we are draging link
            this.startDrag(elementToDrag);

            this.initialMouseX = evt.clientX + parseInt($(elementToDrag).width());
            this.initialMouseY = evt.clientY;
            this.bindRelease = bind(this, this.releaseElement);
            this.bindDrag = bind(this, this.dragMouse);
            this.bindSelect = function() {
                return false;
            };

            $(document).mouseup(this.bindRelease);
            $(document).mousemove(this.bindDrag);
            $(document).bind('selectstart', this.bindSelect);
            $(elementToDrag).bind('dragstart', function() { return false; });

            return false;
        },
        startDrag: function (obj) {

            if (this.draggedObject)
                this.releaseElement();
            var position = $(obj).position();
            this.startX = position.left;
            this.startY = position.top;
            this.draggedObject = obj;
            this.draggedObject.style.position = 'absolute';

            $(obj).addClass('dragged');
        },
        dragMouse: function (evt) {
            var dX = evt.clientX - this.initialMouseX;
            var dY = evt.clientY - this.initialMouseY;
            this.setPosition(dX,dY);
            this.onDrag(evt);
            return false;
        },

        setPosition: function (dx,dy) {
            this.draggedObject.style.left = this.startX + dx + 'px';
            this.draggedObject.style.top = this.startY + dy + 'px';
        },
        releaseElement: function(evt) {
            try {
                if (evt) {
                    this.currentTarget = evt.target != null ? evt.target : evt.srcElement;
                    if (typeof this.onDrop == 'function') {
                        this.onDrop(evt);
                    }
                }
            } catch (e) {
                alert(e.message);
                this.resetPosition();
            }

            $(document).unbind('mouseup');
            $(document).unbind('mousemove');
            $(document).unbind('selectstart');
            $(this.draggedObject).unbind('dragstart');

            this.currentTarget = null;
        },
        removeElement : function() {
            if (this.draggedObject) {
                $(this.draggedObject).unbind('mousedown');
            }
            this.releaseElement();
        }
    }
    DragDrop.prototype.updateServer = function(data) {
        if (!this.serverUrl) {
            throw new Error('Server url undefined');
        }
        data = $.extend(data, {
            'updateMenu' : 1
        });
        $.ajax({
            url : this.serverUrl,
            type : 'post',
            dataType : 'json',
            data : data,
            success : function(response){
            },
            error: function(response) {
                alert('Failed to update server!');
            }
        });
    }

    DragDrop.prototype.resetPosition = function() {
        if (this.draggedObject) {
            this.draggedObject.style.left = this.startX + 'px';
            this.draggedObject.style.top = this.startY + 'px';
            this.draggedObject.style.position = 'static';
            $(this.draggedObject).removeClass('dragged');
        }
    }

    Node.prototype.equals = function(other) {
        return this.id === other.id;
    }

    Node.prototype.isParentOf = function(node) {
        var parent = node.hasOwnProperty('_p') ? node._p : {} ;
        while (!isEmpty(parent)) {
            if (this.equals(parent)) {
                return true;
            }
            parent = parent.hasOwnProperty('_p') ? parent._p : {} ;
        }
        return false;
    }
    //extend dTree to include removeNode
    dTree.prototype.appendNode = function(node, toNode) {
        node.pid = toNode.id;
        if (toNode._hc) {
            var child = this.getFirstChild(toNode);
            this.map.moveBefore(node.id, child.id);
        }
        node._hc = false;
        node._ls = false;
        this.increaseIndent(toNode);
        return this.addNode(toNode._p);
    }
    dTree.prototype.addAfterNode = function(node, afterNode) {
        node.pid = afterNode.pid;
        this.map.moveAfter(node.id, afterNode.id);
        node._hc = false;
        node._ls = false;
        afterNode._hc = false;
        afterNode._ls = false;
        this.increaseIndent(afterNode);
        return this.addNode(afterNode._p);
    }

    dTree.prototype.increaseIndent = function(node) {
        this.aIndent = [];
        var parent = node.hasOwnProperty('_p') ? node._p : {} ;
        while (!isEmpty(parent)) {
            var value = parent._ls ? 0 : 1;
            this.aIndent.unshift(value);
            parent = parent.hasOwnProperty('_p') ? parent._p : {} ;
        }
        //remove root indent
        this.aIndent.shift();
        this.aIndent.shift();
    }
    dTree.prototype.getFirstChild = function(node) {
        var child;
        this.map.forEach(function(c, cn) {
            if (cn.pid == node.id) {
                child = cn;
                return false;
            }
        });
        if (!child) {
            throw new Error('Node does not have any children');
        }
        return child;
    };
    dTree.prototype.getNode = function(id) {
        return this.map.get(id);
    };

    namespace.DragLoader = function(dTree, url, dragElements, onDrop, onDrag) {
        var self = this;
        var serverUrl = url;
        var elements = dragElements || null;
        this.arrMap = [];
        this.afterInit = function() {};
        this.afterUnload = function() {};
        this.initDragElements = function(context) {
            var elementsToApply = context ? $(elements, context) : $(elements);
            elementsToApply.each(function(index, element) {
                var dandd = new DragDrop(serverUrl, dTree, self);
                dandd.onDrop = typeof onDrop === 'function' ? onDrop : function() {};
                dandd.onDrag = typeof onDrag === 'function' ? onDrag : function() {};
                dandd.initElement(element);
                self.arrMap.push(dandd);
            });
            this.afterInit();
        }
        this.unloadDragElements = function() {
            for (var i in this.arrMap) {
                if (this.arrMap.hasOwnProperty(i)) {
                    this.arrMap[i].removeElement();
                    delete this.arrMap[i];
                }
            }
            this.arrMap = [];
            this.afterUnload();
        }
    }

})(jQuery, window.DragAndDrop = window.DragAndDrop || {});

//custom events

var dTreeOnDrop = function(evt) {
    var self = this;
    var handleMove = function(target) {
        //for folder we need to remove childs as well
        if ($(target).parent().next().hasClass('clip')) {
            $(target).parent().next().remove();
        }
        $(target).parent().remove();
    }

    var getOrderNo = function(target) {
        var parent = target.closest('.dTreeNode');
        var index = parent.siblings('.dTreeNode').andSelf().index(parent);
        return index;
    }
    var currentNodeIndex = $(this.draggedObject).length > 0 ? parseInt($(this.draggedObject).attr('id').replace('sd', '')) : 0;
    var targetNodeIndex = $(this.currentTarget).length > 0  && $(this.currentTarget).attr('id') ? parseInt($(this.currentTarget).attr('id').replace('sd', '')) : 0;
    var currentNode, parentNode, html;
    //appending element
    if (currentNodeIndex && targetNodeIndex && currentNodeIndex != targetNodeIndex) {

        var toNode = this.dTree.getNode(targetNodeIndex);
        parentNode = toNode._p;
        currentNode = this.dTree.getNode(currentNodeIndex);

        if (currentNode.isParentOf(toNode)) {
            throw new Error('Parent node cannot be appended to child node!');
        }
        handleMove(this.draggedObject);
        html = this.dTree.appendNode(currentNode, toNode);
        $('#dd' + parentNode._ai).html(html);
        self.loader.initDragElements('#dd' + parentNode._ai);

        this.updateServer({
            nodeId : currentNode.id,
            parentId : currentNode.pid,
            orderNo : 1
        });
        this.dTree.openTo(currentNode.id);


    } //add after the element
    else if ($(this.currentTarget).hasClass('dropAfter')) {
        var target = $(this.currentTarget).prev().find('a.node, a.nodeSel');
        var resetPosition = true;
        if (target.length > 0) {
            targetNodeIndex = parseInt(target.attr('id').replace('sd', ''));
            if (targetNodeIndex > 0) {
                //targets index
                var currentOrderNo = getOrderNo(target);
                //appended elements index
                currentOrderNo = currentOrderNo > 0 ? currentOrderNo + 1 : currentOrderNo;
                var afterNode = this.dTree.getNode(targetNodeIndex);
                parentNode = afterNode._p;
                currentNode = this.dTree.getNode(currentNodeIndex);
                if (currentNode.isParentOf(afterNode)) {
                    throw new Error('Parent node cannot be added after child node!');
                }
                html = this.dTree.addAfterNode(currentNode, afterNode);

                handleMove(this.draggedObject, '#dd' + parentNode._ai);

                $('#dd' + parentNode._ai).html(html);
                self.loader.initDragElements('#dd' + parentNode._ai);

                this.updateServer({
                    nodeId : currentNode.id,
                    parentId : currentNode.pid,
                    orderNo : currentOrderNo
                });
                resetPosition = false;
            }
        }
        if (resetPosition) {
            this.resetPosition();
        }
    } else {
        this.resetPosition();
    }
};

var dTreeOnDrag = function(evt) {
    var self = this;
    var element = $(document.elementFromPoint(evt.clientX, evt.clientY));
    if ('lastElement' in self) {
        if (element === self.lastElement) {
            return;
        }
    }
    //show add after element effect
    if (element.hasClass('dropAfter')) {
        $(element).addClass('afterElement');
        self.lastElement = element;
        $(element).one('mouseleave', function(evt) {
            $(element).removeClass('afterElement');
            self.lastElement = null;
        });
    }
    //show append element effect
    else if (element.hasClass('node')) {
        element.addClass('appendElement');
        self.lastElement = element;
        element.one('mouseleave', function(evt) {
            element.removeClass('appendElement');
            self.lastElement = null;
        });

    }
    //if its a folder open it
    else if (element.is('img')) {
        if (element.attr('src').search('plus') > -1) {
            var parent = element.parent();
            window.location = $(parent).attr('href');
        }
    }
};