
/**
 * Provides total order refund calculation
 */
(function(CMS, $) {

    /**
     * UI
     *
     * @param loader
     * @param options
     * @constructor
     */
    var UI = function(loader, options) {
        this.loader = loader;
        this.options = $.extend({}, UI.DEFAULTS, options);
    }
    UI.DEFAULTS = {
        itemPriceSelector: 'input[name^="orderRefund[orderItems]"]',
        itemsTotalSelector: '#orderRefund_itemsRefund',
        adminFeeTotalSelector: '#orderRefund_adminFee',
        creditTotalSelector: '#orderRefund_totalCreditRefund',
        moneyTotalSelector: '#orderRefund_totalRefund',
        actionCalculateSelector: 'input[data-refund-action="calculate"]',
        refundButtonSelector: '#orderRefund_refund'
    }
    UI.prototype.selectedItems = function() {
        var items = {};
        $(this.options.itemPriceSelector).each(function(){
            var checkbox = $(this);
            if (checkbox.is("input[type='checkbox']") && checkbox.is(":checked")) {
                var id = checkbox.attr('id');
                items[id] = checkbox.data('refund-item-price');
            }
        });
        return items;
    }
    UI.prototype.adminFeeTotal = function() {
        return $(this.options.adminFeeTotalSelector).val();
    }
    UI.prototype.renderTotals = function (itemsTotal, adminFeeTotal, creditTotal, moneyTotal) {
        $(this.options.itemsTotalSelector).val(this.formatPrice(itemsTotal));
        $(this.options.adminFeeTotalSelector).val(this.formatPrice(adminFeeTotal));
        $(this.options.creditTotalSelector).val(this.formatPrice(creditTotal));
        $(this.options.moneyTotalSelector).val(this.formatPrice(moneyTotal));
    }
    UI.prototype.formatPrice = function(price) {
        return price.toFixed(2);
    }
    UI.prototype.hideLoader = function () {
        this.loader.hide();
    }
    UI.prototype.showLoader = function () {
        this.loader.show();
    }

    /**
     * Calculator
     *
     * @constructor
     * @param ui
     * @param calculationUrl
     */
    var Calculator = function(ui, calculationUrl) {
        this.ui = ui;
        this.calculationUrl = calculationUrl;
    }
    Calculator.prototype.itemsTotal = function() {
        var total = 0;
        var selectedItems = this.ui.selectedItems();
        for (var property in selectedItems) {
            if (selectedItems.hasOwnProperty(property)) {
                var price = selectedItems[property];
                total += this.parsePrice(price);
            }
        }
        return total;
    }
    Calculator.prototype.adminFeeTotal = function() {
        return this.parsePrice(this.ui.adminFeeTotal());
    }
    Calculator.prototype.calculate = function() {
        var self = this;
        this.ui.showLoader();
        $.ajax({
            url: this.calculationUrl,
            type: "GET",
            dataType: 'json',
            data: {
                do: 'calculateTotal',
                itemsTotal: this.itemsTotal(),
                adminFeeTotal: this.adminFeeTotal()
            },
            success: function(response) {
                if (response.error) {
                    alert(response.messsage);
                } else {
                    var price = response.price;
                    self.ui.renderTotals(
                        price.itemsTotal,
                        price.adminFeeTotal,
                        price.creditTotal,
                        price.moneyTotal
                    );
                }
                self.ui.hideLoader();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                self.ui.hideLoader();
                alert(thrownError);
            }
        });
    }
    Calculator.prototype.parsePrice = function(price) {
        var floatPrice = parseFloat(price);
        return isNaN(floatPrice) ? 0 : floatPrice;
    }

    /**
     * Form
     *
     * @constructor
     * @param ui
     * @param calculator
     */
    var Form = function(ui, calculator) {
        this.ui = ui;
        this.calulator = calculator;
    }
    Form.prototype.calculateTotals = function(){
        this.calulator.calculate();
    }
    Form.prototype.init = function(){
        this.calculateTotals();

        var self = this;
        $(this.ui.options.actionCalculateSelector).on('change', function(){
            self.calculateTotals();
        });
    }

    /**
     * @param loader
     * @param calculationUrl
     * @param options
     */
    CMS.initOrderRefund = function (loader, calculationUrl, options) {
        var ui = new UI(loader, options);
        var calculator = new Calculator(ui, calculationUrl);
        var form = new Form(ui, calculator);
        form.init();
    }


}(window.CMS = window.CMS || {}, $));