
/**
 * Plugin for table rows toggle
 * Options: rows, textAll, textLess
 */
jQuery.fn.tableToggle = function(options) {
	// settings
	var settings = { 
		rows: 10,
		textAll: 'Show All',
		textLess: 'Show Less' 
	};
	if (options) $.extend(settings, options);
	
	// count of all elements
	var elementsCount = $('tbody > tr', this).size();
	
	// apply just when elements count is greater than settings.rows
	if (elementsCount > settings.rows) {
	
		// add count of elements
		settings.textAll += ' (' + elementsCount + ')';
		settings.textLess += ' (' + settings.rows + ')';
		
		// wrap to div
		$(this).wrap('<div style="position: relative;"/>');
		
		// rows selector
		var $rows =  $('tbody > tr:gt(' + (settings.rows - 1) + ')', this);
		var $lastRow = $('tbody > tr:last', this);
		$rows.toggle();
		
		// link to toggle with rows
		$('<a href="#" style="position: absolute; right: 5px; top: -25px;"/>')
		.text(settings.textAll)
		.click(function() {
			var display = $lastRow.css('display');
			var text = (display == 'none') ? settings.textLess : settings.textAll;
			$rows.toggle();
			$(this).text(text)
			return false;
		})
		.insertBefore($(this));
	}
}

/**
 * select checkboxes provided as argument
 */
jQuery.fn.selectAll = function(elemUsed) {
	return this.each(function(index, elem) {
		$(elem).bind('click', function() {
			if ($(this).is(':checked')) {
				$(elemUsed).attr('checked', 'checked');
			}
			else {
				$(elemUsed).removeAttr('checked');
			}
		});
	});
}

$(function(){
	$('input.massSelect').selectAll('input.massDelete');
	$('form.massDeleteAction').submit(function(e) {
		var numberToDelete = $(this).find('input.massDelete:checked').length;
		if (numberToDelete > 0) {
			if (!confirm('Are you sure you want to delete `' + numberToDelete + '` records ?')) {
				e.preventDefault();
			}
		}
		else {
			alert('No recorts selected!');
			e.preventDefault();
		}
	});
	$('a.buttonCheckSecondParent').click(function(e) {
		e.preventDefault();
		$(this).parent().parent().find('input.massDelete').attr('checked', 'checked');
		$(this).closest('form').submit();
	});
	$('.tooltip').tooltip({
		position: { my: "left+15 center", at: "right center" },
		content: function() {
			return $('<div/>').html($(this).attr('title'));
		}
	});

	//$('span.question-pop').popover();

    /**
     * HTML 5 date picker fallback
     */
    if (!Modernizr.touch || !Modernizr.inputtypes.date) {
        $("input[type=date]")
            .attr('type', 'text')
            .datepicker({
                dateFormat: 'yy-mm-dd' // Consistent format with the HTML5 picker
            });
    }
});

$(document).on('click', '[data-confirmation]', function() {
	return confirm($(this).data('confirmation'));
});
