/**
 * common helper functions / functions defined for cross browser compatability
**/
	
function bind(scope, fn) {
	return function () {
		fn.apply(scope, arguments);
	};
}

function isEmpty(obj) {
	for(var prop in obj) {
		if(obj.hasOwnProperty(prop)) {
			return false;
		}
	}
	return true;
}

/**
 * ordered map to for each 
 */
function OrderedMap() {
    this.map = {};
    this.array = [];
}
OrderedMap.prototype.set = function(key, value) {
    // key already exists, replace value
    if(key in this.map) {
        this.map[key] = value;
    }
    // insert new key and value
    else {
        this.array.push(key);
        this.map[key] = value;
    }
};

OrderedMap.prototype.remove = function(key) {
    var index = this.array.indexOf(key);
    if(index == -1) {
        throw new Error('key does not exist');
    }
    this.array.splice(index, 1);
    delete this.map[key];
};

OrderedMap.prototype.moveAfter = function(key, afterKey) {
    var currentIndex = this.array.indexOf(key);
    var afterIndex = this.array.indexOf(afterKey);
    if(currentIndex == -1) {
        throw new Error('key does not exist');
    }
    this.array.splice(currentIndex, 1);
    this.array.splice(afterIndex + 1, 0, key);
};

OrderedMap.prototype.moveBefore = function(key, beforeKey) {
    var currentIndex = this.array.indexOf(key);
    var beforeIndex = this.array.indexOf(beforeKey);
    if(currentIndex == -1) {
        throw new Error('key does not exist');
    }
    this.array.splice(currentIndex, 1);
    this.array.splice(beforeIndex - 1, 0, key);
};

OrderedMap.prototype.get = function(key) {
    return this.map[key];
};

OrderedMap.prototype.forEachContext = function(context, f) {
    var key, value, returnValue;
    for(var i = 0; i < this.array.length; i++) {
        key = this.array[i];
        value = this.map[key];
        returnValue = f.call(context, key, value);
		if (returnValue === false) {
			break;
		}
    }
};

OrderedMap.prototype.forEach = function(f) {
    var key, value, returnValue;
    for(var i = 0; i < this.array.length; i++) {
        key = this.array[i];
        value = this.map[key];
        returnValue = f(key, value);
		if (returnValue === false) {
			break;
		}
    }
};