
(function(CMS, $) {
    var AjaxLoader = function(el, options) {
        this.container 	= $(el);
        this.options = $.extend({}, AjaxLoader.DEFAULTS, options);
    }
    AjaxLoader.DEFAULTS = {
        bgColor 		: '#fff',
        duration		: 100,
        opacity			: 0.7,
        loaderClass 	: 'square_loader'
    }
    AjaxLoader.prototype.show = function () {
        var container = this.container;

        // delete any other loaders
        this.hide();

        var overlay = $('<div class="ajax_overlay"></div>').css({
            'background-color': this.options.bgColor,
            'opacity': this.options.opacity,
            'width':container.width(),
            'height':container.height()
        });

        // add loader
        var loader = $('<div class="ajax_loader"></div>').addClass(this.options.loaderClass);
        overlay.append(loader);

        container.append(overlay).fadeIn(this.options.duration);
    }
    AjaxLoader.prototype.hide = function(){
        var overlay = this.container.children(".ajax_overlay");
        if (overlay.length) {
            overlay.fadeOut(this.options.loaderClass, function() {
                overlay.remove();
            });
        }
    }

    CMS.AjaxLoader = AjaxLoader;

}(window.CMS = window.CMS || {}, $));