$.validator.setDefaults({
    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    invalidHandler: function(event, validator) {
        var errors = validator.numberOfInvalids();
        if (errors > 0) {
            var first = $(validator.errorList[0].element);
            $('html, body').animate({
                scrollTop: $("label[for='"+first.attr('id')+"']").offset().top - 15
            }, 750);
            first.focus();
        }
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
        var errorPlacementAfter = element.parents('.form-error-after:eq(0)');
        if(errorPlacementAfter.length) {
            error.insertAfter(errorPlacementAfter);
        } else if(element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    }
});

/**
 *
 * @param {string} formName
 * @param {object} errors
 * @returns {object}
 */
function formatValidationErrors(formName, errors) {
    var firstErrors = {};
    for (var name in errors) {
        var elementName = formName ? formName + '[' + name + ']' : name;
        for (var i in errors[name]) {
            firstErrors[elementName] = errors[name][i];
        }
    }
    return firstErrors;
}

var msgValidation = function(form, options) {
    $(form).on( "submit.validate", function( event ) {
        $(this).data('validator').isSubmitted = true;
    });
    $(form).validate(options);

};

$.validator.addMethod('validExpiryDate', function (value, element, params) {
    var now = new Date();
    var month = $('select[name="' + params[0] + '"]').val(),
        year = $('select[name="' + params[1] + '"]').val();
    if (!this.isSubmitted) {
        if (!month || !year) return true;
    }
    if (!month || !year) return false;
    if (now.getFullYear() < year) return true;
    if (now.getFullYear() == year && now.getMonth() <= month - 1) return true;
}, "Must be a valid Expiration Date.");

$.validator.addMethod('validStartDate', function (value, element, params) {
    $.validator.messages.validStartDate = "default";
    var issueNumber = $(params[2]).val();
    var now = new Date();
    var month = $('select[name="' + params[0] + '"]').val(),
        year = $('select[name="' + params[1] + '"]').val();

    var isRequired = issueNumber ? false : true;
    var isValid = false;
    var isEmpty = true;

    if (month && year) isEmpty = false;
    if (!month || !year) isValid = false;

    if (!isEmpty && now.getFullYear() > year) isValid = true;
    if (!isEmpty && now.getFullYear() == year && now.getMonth() > month - 1) isValid = true;

    if (!this.isSubmitted) {
        return true;
    } else {
        $('select[name="' + params[0] + '"], select[name="' + params[1] + '"]').on(
            'change', function(){
                $(params[2]).valid();
            });
    }
    if (isValid) return true;
    if (!isRequired && isEmpty) return true;
    if (!isValid) {
        $.validator.messages.validStartDate = "Please provide a valid date for Valid From";
    }
    if (isRequired && isEmpty) {
        $.validator.messages.validStartDate = "Please provide Issue number or Valid From";
    }
    return false;
}, "");

$.validator.addMethod('isIssueNumberRequired', function (value, element, params) {
    var month = $('select[name="' + params[0] + '"]').val(),
        year = $('select[name="' + params[1] + '"]').val();

    if (this.isSubmitted) {
        $(element).on('keyup', function(){
            $('select[name="' + params[0] + '"]').valid();
        });
    }

    if (value) return true;
    return (month && year);
}, "Please provide Issue number or Valid From");