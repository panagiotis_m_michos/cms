/**
 *  Optional behaviour:
 *  <div id="message-container-success" class="hidden">Form submitted successfully</div>
 *
 *  <form class="paulo-ajax-form" action="url" method="post" data-show="#message-container-success">
 *      <input type="submit" value="Submit" />
 *  </form>
 *
 *
 *  Api:
 *
 *  How to initialize
 *
 *  var ajaxForm = $('.paulo-ajax-form').ajaxForm();
 *
 */

(function(forms, $) {

    'use strict';

    forms.ajaxForm = function() {
        var form = $(this);
        function removeError() {
            var errorElement = form.prev();
            if (errorElement.hasClass('error-block')) {
                errorElement.remove();
            }
        }
        $.ajax({
            url: form.attr('action'),
            method: form.attr('method'),
            dataType: "json"
        }).done(function(data) {
            removeError();
            var showElement = form.attr('data-show');
            var hideElement = form.attr('data-hide');
            if (showElement) {
                $(showElement).show();
            }
            if (hideElement) {
                $(hideElement).hide();
            }
        }).fail(function(data) {
            if ($.fn.ladda) {
                $('.ladda-button', form).ladda().ladda('stop');
            }
            if (data.hasOwnProperty('responseJSON')) {
                if (data.responseJSON.hasOwnProperty('error')) {
                    removeError();
                    form.before('<div class="error-block">' + data.responseJSON.error + '</div>');
                }
            }
        });
    };

    $.fn.ajaxForm = forms.ajaxForm;
    $(document).ready(function() {
        $('body').on('submit', 'form.paulo-ajax-form', function(e) {
            e.preventDefault();
            $(this).ajaxForm();
        });
    });


}(window.uiserver = window.uiserver || {}, $));
