(function(maps) {
    maps.addGoogleMap = function(placementElementId, address) {
        var element = document.getElementById(placementElementId);
        if (!element) {
            return;
        }
        if (!address) {
            element.style.display = 'none';
            return;
        }
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({
            'address': address
        }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var myOptions = {
                    zoom: 15,
                    center: results[0].geometry.location,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                var map = new google.maps.Map(element, myOptions);

                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location
                });
            } else {
                element.style.display = 'none';
            }
        });
    };

})(window.maps = window.maps || {}, jQuery);