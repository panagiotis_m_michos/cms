
(function(uiserver, $) {

    'use strict';

    // @TODO move to a different file. how do you specify dependencies ?
    var HashHistory = function() {
        this.hashesUpdated = {};
    };

    /**
     * @param {string} hashName
     * @param {function} func
     */
    HashHistory.prototype.addEntry = function(hashName, func) {
        this.hashesUpdated[hashName] = func;
    };

    /**
     * @param {string} hashName
     * @param {function} func
     */
    HashHistory.prototype.changeHash = function(hashName, func)
    {
        this.addEntry(hashName, func);
        this.updateHash(hashName);
    };

    /**
     * @param {string} hashName
     * @param {Event} e
     * @returns {null|object}
     */
    HashHistory.prototype.executeEntry = function(hashName, e) {
        if (hashName in this.hashesUpdated) {
            return this.hashesUpdated[hashName].call(this, hashName, e);
        }
    };

    /**
     * @param {string} hashName
     */
    HashHistory.prototype.updateHash = function(hashName) {

        var fx, node = $( hashName), elementId = node.attr('id');
        if ( node.length ) {
            node.attr( 'id', '' );
            fx = $( '<div></div>' )
                .css({
                    position:'absolute',
                    visibility:'hidden',
                    top: $(document).scrollTop() + 'px'
                })
                .attr( 'id', elementId )
                .appendTo( document.body );
        }
        window.location.hash = hashName;
        if ( node.length ) {
            fx.remove();
            node.attr( 'id', elementId );
        }
    };

    /**
     * @param {Event} e
     */
    HashHistory.prototype.restore = function(hashName, e) {
        return this.executeEntry(hashName, e);
    };

    HashHistory.prototype.listen = function(namespace) {
        var self = this, event = 'hashchange.' + namespace;
        $(window).off(event).on(event, function(e) {
            self.restore(window.location.hash, e);
        });
    };

    var hashHistory = new HashHistory();
    hashHistory.listen('paulo.ui');

    uiserver.hashHistory = hashHistory;
    uiserver.HashHistory = HashHistory;

    function modifyHoverButtonState()
    {
        var item = $(this);
        var value = item.data('current-value');
        if (value) {
            item.data('hover-value', item.html());
            item.html(value);
        }
    }

    uiserver.reloadState = function() {
        $('button.paulo-hover-button').each(function() {
            modifyHoverButtonState.call(this);
        });
    };

    uiserver.addCss = function (path) {
        var head = document.getElementsByTagName("head")[0];
        if (head) {
            var scriptStyles = document.createElement("link");
            scriptStyles.rel = "stylesheet";
            scriptStyles.type = "text/css";
            scriptStyles.href = path;
            head.appendChild(scriptStyles);
        }
    };

    $.fn.doOnce = function(func) {
        this.length && func.apply(this);
        return this;
    };
    $(document).ready(function() {

        $("body").on("click", ".paulo-ui-dialog", function(e) {
            e.preventDefault();
            var dialogBox = $(this).attr('href') || $(this).data('show');
            $(dialogBox).dialog({
                width: 'auto'
            });
        });

        $(".paulo-hide-show").on("click", ".paulo-hide-show-button", function(e) {

            var updateElements = function(parent, toggleClass, elem, hide, show) {
                $(".paulo-hide-show-button", parent).removeClass(toggleClass);
                elem.addClass(toggleClass);
                $(hide).hide();
                $(show).show();
            };

            e.stopPropagation();
            var elem = $(this);
            var parent = $(e.delegateTarget),
                show = elem.data('data-show') || elem.attr('href'),
                hide = parent.data('hide'),
                toggleClass = parent.data('toggle-class'),
                enableDefaultAction = parent.data('prevent-default') === false,
                noScroll = parent.data('scroll') === false;

            if (!enableDefaultAction) {
                e.preventDefault();
            }
            if (noScroll) {
                hashHistory.changeHash(show, function(hashName, e) {
                    updateElements(parent, toggleClass, elem, hide, show);
                });
            }
            updateElements(parent, toggleClass, elem, hide, show);

        });

        $("body").on("click", ".paulo-hide-show-button", function(e) {
            var elem = $(this);
            var show = elem.data('show') || elem.attr('href'),
                hide = elem.data('hide'),
                enableDefaultAction = elem.data('prevent-default') === false;
            if (!enableDefaultAction) {
                e.preventDefault();
            }
            $(hide).hide();
            $(show).show();
        });

        $(".paulo-ui-datepicker" ).each(function(index, item) {
            var elem = $(item),
                options = {
                    dateFormat: 'dd/mm/yy',
                    changeYear: true,
                    changeMonth: true
                };
            if (elem.data('year-range')) options.yearRange = elem.data('year-range');
            if (elem.data('default-date')) options.defaultDate = elem.data('default-date');
            elem.datepicker(options).on('change', function() {
                if (typeof $(this).valid === 'function') {
                    $(this).valid();
                }
            });
        });

        /**
         * <a href="#" class="paulo-ui-toggle" data-toggle-block-selector="#block" data-toggle-text="Hide">Show</a>
         * <div id="block" style="display: none;">
         */
        $('body').on('click', '.paulo-ui-toggle', function(e){
            e.preventDefault();
            var blockSelector = $(this).data('toggle-block-selector');
            $(blockSelector).each(function(){
                $(this).toggle();
            });
            var toggleText = $(this).data('toggle-text');
            $(this).data('toggle-text', $(this).text());
            $(this).text(toggleText);
        });


        $('body').on({
            "mouseenter": function (e) {
                var item = $(this);
                if (item.data('hover-value')) {
                    if (!item.data('previous-value')) {
                        item.data('previous-value', item.html());
                    }
                    item.html(item.data('hover-value'));
                }
                item.removeClass(item.data('hover-remove-class'));
                item.addClass(item.data('hover-class'));
            },
            "mouseleave": function (e) {
                var item = $(this);
                if (item.data('previous-value')) {
                    item.html(item.data('previous-value'));
                }
                item.addClass(item.data('hover-remove-class'));
                item.removeClass(item.data('hover-class'));
            }
        }, 'button.paulo-hover-button');

        uiserver.reloadState();

    });
})(window.uiserver = window.uiserver || {}, jQuery);
