(function (n, $, rivets) {

    /**
     * @param {object} paymentDetailsData
     * @param {object} basket
     * @param {object} customer
     * @param {string} buttonText
     * @param {customerLogin.LoginAvailability} loginAvailability
     */
    n.PaymentDetails = function (paymentDetailsData, basket, customer, buttonText, loginAvailability) {
        var self = this;
        this.paymentDetails = paymentDetailsData;
        if (!this.paymentDetails.cardDetails) {
            this.paymentDetails.cardDetails = {};
        }
        if (!this.paymentDetails.payerDetails) {
            this.paymentDetails.payerDetails = {};
        }
        this.basket = basket;
        this.customer = customer;
        this.buttonText = buttonText;
        this.loginAvailability = loginAvailability;
        this.initialCustomer = $.extend({}, customer);
        this.initialBasket = $.extend({}, basket);
        this.cardPayment = ['SAGE', 'SAGE_NEW_TOKEN', 'SAGE_NO_TOKEN'];
        /**
         * @param {string} paymentType
         * @returns {string}
         */
        function getButtonText(paymentType) {
            switch (paymentType) {
                case 'SAGE_NEW_TOKEN':
                case 'SAGE_EXISTING_TOKEN':
                case 'SAGE_NO_TOKEN':
                case 'SAGE':
                    return 'Pay using Card';
                    break;
                case 'PAYPAL':
                    return 'Pay with Paypal';
                    break;
                case 'ON_ACCOUNT':
                    return 'Pay with Credits';
                    break;
                default:
                    return 'Pay using Card';
            }
        }

        this.changeCardType = function () {
            self.paymentDetails.cardDetails.cardType = $(this).val();
        };

        this.changeCountry = function () {
            self.paymentDetails.payerDetails.country = $(this).val();
        };

        this.changePaymentType = function () {
            self.paymentDetails.paymentType = $(this).val();
            self.buttonText = getButtonText(self.paymentDetails.paymentType);
        };

        this.changeCredit = function () {
            if ($(this).is(':checked')) {
                self.paymentDetails.useCredit = true;
                self.updateBasketWithCredit();
                if (self.isPayingWithCreditOnly()) {
                    self.buttonText = getButtonText('ON_ACCOUNT');
                }
            } else {
                self.paymentDetails.useCredit = false;
                self.buttonText = getButtonText(self.paymentDetails.paymentType);
                self.resetBasketCredit();
            }

        };

        this.resetBasketCredit = function () {
            this.customer.credit = this.initialCustomer.credit;
            this.basket.creditPrice = 0;
            this.basket.totalPrice = this.basket.totalPriceWithoutCredit;
        };

        this.updateBasketWithCredit = function () {
            if (this.basket.totalPriceWithoutCredit > this.customer.credit) {
                this.basket.creditPrice = this.customer.credit;
                this.basket.totalPrice = this.basket.totalPriceWithoutCredit - this.basket.creditPrice;
                this.customer.credit = 0;
            } else {
                this.customer.credit -= this.basket.totalPriceWithoutCredit;
                this.basket.creditPrice = this.basket.totalPriceWithoutCredit;
                this.basket.totalPrice = 0;
            }
        };

        this.isPayingWithCreditOnly = function () {
            if (this.paymentDetails.useCredit) {
                if (this.initialCustomer.credit >= this.basket.totalPriceWithoutCredit) {
                    return true;
                }
            }
            return false;
        };

        this.isInsufficientCredit = function() {
            if (this.paymentDetails.useCredit) {
                if (this.initialCustomer.credit < this.basket.totalPriceWithoutCredit) {
                    return true;
                }
            }
            return false;
        };

        this.isCreditMinimumAmountReached = function() {
            if (this.paymentDetails.useCredit) {
                return false;
            }
            var priceDiff = this.basket.totalPriceWithoutCredit - this.initialCustomer.credit;
            return priceDiff > 0 && priceDiff <= 1;
        };

        this.isUsingCard = function () {
            return $.inArray(this.paymentDetails.paymentType, this.cardPayment) !== -1;
        };

        this.isAmex = function () {
            return this.paymentDetails.cardDetails.cardType === 'AMEX';

        };

        this.isMaestro = function () {
            return this.paymentDetails.cardDetails.cardType === 'MAESTRO';
        };

        this.isUsa = function () {
            return this.paymentDetails.payerDetails.country === 'US';
        };

        this.isSpain = function () {
            return this.paymentDetails.payerDetails.country === 'ES';
        };
    };

    /**
     * @param {string|jQuery} form
     * @param {string} firstErrorElementName
     */
    n.validate = function (form, firstErrorElementName) {
        var formName = $(form).attr('name'),
            firstErrorElement = $('#' + formName + "_" + firstErrorElementName);
        if (firstErrorElement.length) {
            $(document).ready(function () {
                $('body').animate({scrollTop: firstErrorElement.offset().top - 15}, 750);
            });
        }
        msgValidation(form, {
            groups: {
                expiryDate: "payment_form[cardDetails][expiryDate][month] payment_form[cardDetails][expiryDate][year]",
                validFrom: "payment_form[cardDetails][validFrom][month] payment_form[cardDetails][validFrom][year]"
            },
            rules: {
                "payment_form[cardDetails][cardholder]": {
                    required: true,
                    maxlength: 40
                },
                "payment_form[cardDetails][cardType]": {
                    required: true
                },
                "payment_form[cardDetails][cardNumber]": {
                    required: true,
                    number: true,
                    minlength: 12,
                    maxlength: 20
                },
                "payment_form[cardDetails][expiryDate][month]": {
                    validExpiryDate: ['payment_form[cardDetails][expiryDate][month]', 'payment_form[cardDetails][expiryDate][year]']
                },
                "payment_form[cardDetails][expiryDate][year]": {
                    validExpiryDate: ['payment_form[cardDetails][expiryDate][month]', 'payment_form[cardDetails][expiryDate][year]']
                },
                "payment_form[cardDetails][validFrom][month]": {
                    validStartDate: ['payment_form[cardDetails][validFrom][month]', 'payment_form[cardDetails][validFrom][year]', '#payment_form_cardDetails_issueNumber']
                },
                "payment_form[cardDetails][validFrom][year]": {
                    validStartDate: ['payment_form[cardDetails][validFrom][month]', 'payment_form[cardDetails][validFrom][year]', '#payment_form_cardDetails_issueNumber']
                },
                "payment_form[cardDetails][issueNumber]": {
                    maxlength: 2,
                    number: true,
                    isIssueNumberRequired: ['payment_form[cardDetails][validFrom][month]', 'payment_form[cardDetails][validFrom][year]']
                },
                "payment_form[cardDetails][securityCode]": {
                    required: true,
                    number: true,
                    rangelength: function () {
                        switch ($('#payment_form_cardDetails_cardType').val()) {
                            case 'AMEX':
                                $.validator.messages.rangelength = 'Must be 4 digits.';
                                return [4, 4];
                            default:
                                $.validator.messages.rangelength = 'Must be 3 digits.';
                                return [3, 3];
                        }
                    }
                },
                "payment_form[payerDetails][firstName]": {
                    required: true,
                    maxlength: 20
                },
                "payment_form[payerDetails][lastName]": {
                    required: true,
                    maxlength: 20
                },
                "payment_form[payerDetails][address1]": {
                    required: true,
                    maxlength: 100
                },
                "payment_form[payerDetails][address2]": {
                    maxlength: 40
                },
                "payment_form[payerDetails][city]": {
                    required: true,
                    maxlength: 40
                },
                "payment_form[payerDetails][postcode]": {
                    required: true,
                    maxlength: 40
                },
                "payment_form[payerDetails][country]": {
                    required: true,
                    maxlength: 2
                },
                "payment_form[payerDetails][state]": {
                    required: true,
                    maxlength: 2
                }
            },
            messages: {
                "payment_form[cardDetails][cardholder]": {
                    maxlength: "Card Holder name can have a maximum of {0} characters"
                },
                "payment_form[cardDetails][cardNumber]": {
                    number: "Please use numbers only for Card Number",
                    minlength: "Card Number should have at least {0} digits",
                    maxlength: "Card Number can have a maximum of {0} digits"
                },
                "payment_form[cardDetails][issueNumber]": {
                    number: "Please use numbers only for Issue Number",
                    maxlength: "Issue Number can have a maximum of {0} digits",
                    required: "Please provide Issue number or Valid From"
                },
                "payment_form[cardDetails][secutityCode]": {
                    number: "Please use numbers only for Security code",
                    minlength: "Security Code should have at least {0} digits",
                    maxlength: "Security Code can have a maximum of {0} digits"
                },
                "payment_form[payerDetails][firstName]": {
                    maxlength: "First Name can have a maximum of {0} digits"
                },
                "payment_form[payerDetails][lastName]": {
                    maxlength: "Last Name can have a maximum of {0} digits"
                },
                "payment_form[payerDetails][address1]": {
                    maxlength: "This field can have a maximum of {0} digits"
                },
                "payment_form[payerDetails][address2]": {
                    maxlength: "This fieldcan have a maximum of {0} digits"
                },
                "payment_form[payerDetails][city]": {
                    maxlength: "City  name can have a maximum of {0} digits"
                },
                "payment_form[payerDetails][postcode]": {
                    maxlength: "Postcode can have a maximum of {0} digits"
                },
                "payment_form[payerDetails][country]": {
                    maxlength: "Country name can have a maximum of {0} digits"
                },
                "payment_form[payerDetails][state]": {
                    maxlength: "State can have a maximum of {0} digits"
                }
            }
        });
    };

    /**
     * @param {string|jQuery} paymentForm
     * @param {PaymentDetails} paymentDetails
     * @param {string} firstErrorElementName
     * @returns {*}
     */
    n.bind = function (paymentForm, paymentDetails, firstErrorElementName) {
        n.validate(paymentForm, firstErrorElementName);
        return rivets.bind($(paymentForm), paymentDetails);
    }

})(window.payment = {}, jQuery, rivets);