$(function() {
    var noticeShown = document.cookie.replace(/(?:(?:^|.*;\s*)noticeShown\s*\=\s*([^;]*).*$)|^.*$/, "$1");
    if (!noticeShown) {
        $('#overlay-notice').show();
        $('#overlay-notice-link, #overlay-notice-cross').click(function() {
            $('#overlay-notice').hide();
            document.cookie = "noticeShown=1; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/";
        });
    }
});
