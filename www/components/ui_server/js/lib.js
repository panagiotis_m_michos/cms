/**
 * @param object1
 * @param object2
 * @returns {Array}
 */
function createArrayByExistingKeys(object1, object2)
{
    var mainObject = arguments[0];
    var arr = [];
    for (var key in mainObject) {
        if (mainObject.hasOwnProperty(key)) {
            var i = 1, extented = $.extend({}, mainObject[key]);
            for (i in arguments) {
                if (arguments[i].hasOwnProperty(key)) {
                    extented = $.extend({}, extented, arguments[i][key]);
                }
            }
            arr.push(extented);
        }
    }
    return arr;
}

/**
 * 
 * @param {object} data
 * @param {string} defaultMessage
 * @returns {string}
 */
function failedAjaxError(errorData, defaultMessage)
{
    var errorMessage = defaultMessage;
    if ('responseJSON' in errorData) {
        if (errorData.responseJSON.error) {
            errorMessage = errorData.responseJSON.error;
        }
    }
    return errorMessage;
}

if ('rivets' in window) {
    rivets.binders.show = function (el, value) {
        if (value) {
            $(el).removeClass('hidden');
        } else {
            $(el).addClass('hidden');
        }

        return value;
    };

    rivets.binders.hide = function (el, value) {
        return rivets.binders.show(el, !value);
    };

    rivets.binders['no-class-*'] = function (el, value) {
        if (value) {
            $(el).removeClass(this.args[0]);
        } else {
            $(el).addClass(this.args[0]);
        }
    };

    rivets.formatters.price = function (value) {
        return '£' + ($.isNumeric(value) ? Number(value).toFixed(2) : 0);
    };

    rivets.formatters.prefix = function (value, prefix) {
        return prefix + '-' +  String(value).toLowerCase().replace('_', '-');
    };

    rivets.binders.readonly = function(el, value) {
        return el.readonly = !value;
    };
}