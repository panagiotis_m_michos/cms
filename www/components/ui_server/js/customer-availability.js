(function (n, $, rivets, analytics) {

    /**
     * @param {bool} emailConfirmed
     * @param {bool} requiresToLogin
     * @param {string} loginUrl
     * @param {string} forgottenUrl
     * @param {$.validation} validation
     * @param {string} returnType
     * @constructor
     */
    n.LoginAvailability = function (emailConfirmed, requiresToLogin, loginUrl, forgottenUrl, validation, returnType) {
        var self = this;
        this.emailConfirmed = emailConfirmed;
        this.requiresToLogin = requiresToLogin;
        this.loginUrl = loginUrl;
        this.forgottenUrl = forgottenUrl;
        this.validation = validation;
        this.returnType = returnType;
        this.sendingEmail = null;
        this.emailSent = null;
        this.password = null;

        this.forgottenEmail = function(event) {
            event.preventDefault();
            if (!confirm('Are you sure ?')) {
                return;
            }
            var postData = self.createData($(this).closest('form'));

            self.sendingEmail = true;
            var promise = $.ajax({
                url: self.forgottenUrl,
                method: 'POST',
                data: postData,
                timeout: 10000,
                type: 'application/json'
            });
            promise.fail(function (data) {
                var errorMessage = failedAjaxError(data, 'Failed to send email!');
                alert(errorMessage);
                self.sendingEmail = false;

            });
            promise.done(function (data) {
                self.emailSent = true;
                self.sendingEmail = false;
            });
        };

        this.useAnotherEmail = function () {
            self.emailConfirmed = false;
            self.requiresToLogin = false;
            self.password = null;
            self.validation.showErrors([]);
        };
        
        this.canUseCredentials = function() {
            return self.emailConfirmed && !self.requiresToLogin;
        };

        this.isEmailLocked = function() {
            return self.emailConfirmed || self.requiresToLogin;
        };

        this.useCredentials = function (event) {
            if (!$(this).valid()) {
                return;
            }
            event.preventDefault();
            var postData = self.createData($(this).closest('form')),
                formName = $(this).attr('name'),
                promise = $.ajax({
                    url: self.loginUrl,
                    method: 'POST',
                    data: postData,
                    timeout: 10000,
                    type: 'application/json'
                });
            promise.fail(function () {
                alert('Failed to retrieve customer information');
            });
            promise.done(function (data) {
                var loginDetails = data.loginDetails || {};
                if (loginDetails.isLoggedIn) {
                    window.location.reload();
                    return;
                }
                self.emailConfirmed = loginDetails.emailConfirmed;
                self.requiresToLogin = loginDetails.requiresToLogin;
                if (loginDetails.errors) {
                    self.validation.showErrors(
                        formatValidationErrors(formName, loginDetails.errors)
                    );
                }
            });
            analytics.push(['_trackEvent', 'Links', 'Click', 'Clicked Create Account or Sign In button on payment page']);
        };

        /**
         * @param {jQuery} form
         * @returns {object}
         */
        this.createData = function(form) {
            var values = form.serializeArray();
            values.push({name: 'resourceType', value: form.attr('name')});
            values.push({name: 'returnType', value: returnType});
            form.find('input:disabled').each(function() {
                values.push({name: $(this).attr('name'), value: $(this).val()});
            });
            return $.param(values);
        }
    };

    /**
     * @param {bool} emailConfirmed
     * @param {bool} requiresToLogin
     * @param {string} loginUrl
     * @param {string} forgottenUrl
     * @param {string} returnType
     * @returns {n.LoginAvailability}
     */
    n.bind = function(emailConfirmed, requiresToLogin, loginUrl, forgottenUrl, returnType)
    {
        var loginValidation = $('customer-availability-login form').validate({
            rules: {
                'login_availability_form[email]': {
                    required: true,
                    email: true
                },
                'login_availability_form[password]': {
                    required: true
                }
            },
            messages: {
                'login_availability_form[email]': {
                    required: 'Please provide a valid email address',
                    email: 'Please provide a valid email address'
                },
                'login_availability_form[password]': {
                    required: 'Please provide a password'
                }
            }
        });
        var loginAvailability = new n.LoginAvailability(emailConfirmed, requiresToLogin, loginUrl, forgottenUrl, loginValidation, returnType);
        rivets.bind($('customer-availability-login'), loginAvailability);
        return loginAvailability;
    }

})(window.customerLogin = {}, jQuery, rivets, window._gaq);