## Button

### Syntax
~~~
{ui name="button"}
~~~

### Optional
- classAttr
- idAttr
- nameAttr
- loading
- colour
- size
- text

## Buy Button

### Syntax
~~~
{ui name="buy_button" actionUrl="basket/" inputName="productId" productId="165"}
~~~

### Required
- actionUrl
- inputName
- productId

### Inherited from button
- classAttr
- idAttr
- nameAttr
- loading
- colour
- size
- text

## js/default.js - Adding these classes to the page automatically adds behaviour without writing a single line of js

### css/js.css

description: classes applied only when there is javascript enabled

class:

* .paulo-js-hidden - element is not displayed

### Datepicker

description: adds datepicker to elements containing the class

class: .paulo-datepicker

default attributes:

* dateFormat - dd/mm/yy

### Dialog

description: shows dialog on click
 
class: .paulo-ui-dialog

attributes:

* href or data-show - show element as dialog

```
<a class="paulo-ui-dialog" href="#dialog-box">Show dialog</a>
<div id="dialog-box" class="paulo-js-hidden">contents</div>
```

### Show hide elements

description: shows hides elements on click
 
class:

* .paulo-hide-show (optional parent)
* .paulo-hide-show-button

attributes:

* href or data-show  - make element visible
* data-hide - make elements invisible
* data-prevent-default - prevent default action (optional) default: true

additional attributes using .paulo-hide-show

* data-toggle-class - toggle element class
    

```
<a id="show-dialog" class="paulo-hide-show-button" href="#dialog-box" 
    data-hide="#show-dialog" data-toggle-class="pressed">Show dialog</a>
<div id="dialog-box" class="paulo-js-hidden">contents</div>
```
```
<div class="paulo-hide-show" data-hide="div.element" data-prevent-default="false">
    <a id="show-dialog" class="paulo-hide-show-button" href="#dialog-box1">Show dialog</a>
    <a id="show-dialog" class="paulo-hide-show-button" href="#dialog-box2">Show dialog</a>
</div>
<div id="dialog-box1" class="paulo-js-hidden element">contents</div>
<div id="dialog-box2" class="paulo-js-hidden element">contents</div>
```

### Ajax form

description: make ajax request instead of submitting the form using json

class: .paulo-ajax-form

attributes:

* data-show - show element when ajax response succeeds
* data-hide - hide elements when ajax response succeeds
    
using with jquery on any other element:
```
$('#my-form").ajaxForm();
```

```
<div id="ajax-success" class="hidden">Success</div>
<form class="paulo-ajax-form" action="link" method="post" data-show="#ajax-success" 
data-hide=".paulo-ajax-form">
    <input type="submit" value="submit" />
</form>
```

error ajax response creates element above form:
response
{error: "error message"}
creates element
<div class="error-box">error message</div>

### Toggle
description: toggle block

class: .paulo-ui-toggle

attributes:

* data-toggle-block-selector: selector for sections which should be toggled
* data-toggle-text: text which will be toggled when section will be opened


```
<a href="#" class="paulo-ui-toggle" data-toggle-block-selector="#block" data-toggle-text="Hide">Show</a>
<div id="block" style="display: none;">
```