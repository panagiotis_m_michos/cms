/**
 * Created by tomas on 14/07/14.
 */
module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        less: {
            development: {
                options: {
//                    cleancss: true
                },
                files: {
                    "css/main.css": [
                        "less/gheader.less",
                        "less/cheader.less",
                        "less/megamenu.less",
                        "less/cfooter.less",
                        "less/gfooter.less"
                    ]
                }
            }
        },
        watch: {
            less: {
                files: "less/*.less",
                tasks: ["less"]
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.registerTask('default', ['less']);
};