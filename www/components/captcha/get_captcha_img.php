<?php
/*
 +----------------------------------------------------------------------+
 | CMS 2                                                                |
 +----------------------------------------------------------------------+
 | Author: Stan <diviak@gmail.com>                                      |
 +----------------------------------------------------------------------+

 FILE: get_captcha_img.php
 CREATED: 09/10/2008
 */


# PHP.INI
if(!isSet($_GET['session_name']))
{
	trigger_error('You have to specify g.session_name', E_USER_ERROR);	
}

# PHP.INI
if(!isSet($_GET['session_save_path']))
{
	trigger_error('You have to specify g.session_save_path', E_USER_ERROR);	
}

# own session name
ini_set('session.name', $_GET['session_name']);
ini_set('session.save_path',$_GET['session_save_path']);

session_start();

# class for generating session
include_once(dirname(__FILE__).'/lib.CaptchaImg.php');

CaptchaImg::get();
?>