<?php
/*
 +----------------------------------------------------------------------+
 | CMS2	                                                                |
 +----------------------------------------------------------------------+
 | Author: stan <diviak@gmail.com>                                      |
 +----------------------------------------------------------------------+

 FILE: lib.CaptchaImg.php
 CREATED: 09/10/2008
 */

class CaptchaImg 
{
	protected $font = 'monofont.ttf';
	
	protected $width = 120;
	
	protected $height = 40;
	
	protected $characters = 6;
	
	/**
	 * @reurn void
	 */
	protected function __construct()
	{
		# set width, height and characters
		$this->setParams($_GET);
	}
	
	/**
	 * @return object
	 */
	static public function get()
	{
		$self = new self();
		
		return($self->create());
	}
	
	/**
	 * @param array $params
	 * @return void
	 */
	protected function setParams($params)
	{
		$this->width = isSet($params['width']) ? $params['width'] : $this->width;
		$this->height = isSet($params['height']) ? $params['height'] : $this->height;
		$this->characters = isSet($params['characters']) ? $params['characters'] : $this->width;
	}
	
	/**
	 * @param int $characters
	 */
	protected function getCode($characters) 
	{
		$code = '';$i = 0;
		
		$possible = '23456789bcdfghjkmnpqrstvwxyz';
		
		while($i < (int)$characters) 
		{
			$code .= substr($possible, mt_rand(0, strlen($possible)-1), 1);
			$i++;
		}
		return $code;
	}
	
	/**
	 * @return string
	 */
	protected function getFont()
	{
		$file_path = dirname(__FILE__).'/'.$this->font;
		
		if(!is_File($file_path))
		{
			die('Font couldn\'t be find!');
		}
		
		return($file_path);
	}
	
	/**
	 * @param string $code
	 * @return void
	 */
	protected function save2session($code)
	{
		if(isSet($_GET['owner_class'],$_GET['owner_name'],$_GET['control']))
		{
			$_SESSION[$_GET['owner_class']][$_GET['owner_name']]['captcha'][$_GET['control']] = $code;
		}
		else
		{
			die('g.owner_class, g.owner_name, g.control has to be set!');
		}
	}
	
	/**
	 * @param string $image
	 * @return void
	 */
	protected function outPut($image)
	{
		header('Content-Type: image/jpeg');
		imagejpeg($image);
		imagedestroy($image);
	}
	
	/**
	 * @return void
	 * @copyright 2006 Simon Jarvis
	 */
	protected function create()
	{
		$this->font = $this->getFont();
		
		# code for img
		$code = $this->getCode($this->characters);

		# font size will be 75% of the image height
		$font_size = $this->height * 0.75;
		
		$image = @imagecreate($this->width, $this->height) or 
			die('Cannot initialize new GD image stream');
		
		# set the colours
		$background_color = imagecolorallocate($image, 255, 255, 255);
		$text_color = imagecolorallocate($image, 20, 40, 100);
		$noise_color = imagecolorallocate($image, 100, 120, 180);
		
		# generate random dots in background
		for($i=0; $i<($this->width*$this->height)/3; $i++) 
		{
			imagefilledellipse($image, mt_rand(0,$this->width), mt_rand(0,$this->height), 1, 1, $noise_color);
		}
		
		# generate random lines in background
		for($i=0; $i<($this->width*$this->height)/150; $i++) 
		{
			imageline($image, mt_rand(0,$this->width), mt_rand(0,$this->height), mt_rand(0,$this->width), mt_rand(0,$this->height), $noise_color);
		}
		
		# create textbox and add text
		$textbox = imagettfbbox($font_size, 0, $this->font, $code) or 
			die('Error in imagettfbbox function');
			
		$x = ($this->width - $textbox[4])/2;
		$y = ($this->height - $textbox[5])/2;
		
		imagettftext($image, $font_size, 0, $x, $y, $text_color, $this->font , $code) or 
			die('Error in imagettftext function');
		
		# save to session this code
		$this->save2Session($code);
		
		# output captcha image to browser 
		$this->outPut($image);
	}
}
?>