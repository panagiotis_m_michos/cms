
/**
 * Handle with add dividend company form
 *
 * @param string addShareholderLink
 * @param string removeShareholderLink
 * @return void
 */
var Dividend = function (addShareholderLink, removeShareholderLink) {

	/** @var string */
	var addShareholderLink = addShareholderLink;

	/** @var string */
	var removeShareholderLink = removeShareholderLink;

	/**
	 * Provides toggle shares
	 *
	 * @return void
	 */
	this.toggleShare =  function() {
		if ($('#shareTypeToggle1').is(':checked')) {
			$('#prefilledShareClass').parents('tr').fadeIn();
			$('#shareClass').parents('tr').hide();
			$('#currency').parents('tr').hide();
			$('#valuePerShare').parents('tr').hide();
		} else if ($('#shareTypeToggle2').is(':checked')) {
			$('#prefilledShareClass').parents('tr').hide();
			$('#shareClass').parents('tr').fadeIn();
			$('#currency').parents('tr').fadeIn();
			$('#valuePerShare').parents('tr').fadeIn();
		} else {
			$('#prefilledShareClass').parents('tr').hide();
			$('#shareClass').parents('tr').hide();
			$('#currency').parents('tr').hide();
			$('#valuePerShare').parents('tr').hide();
		}
	}

	/**
	 * Provides toggle officer
	 *
	 * @return void
	 */
	this.toggleOfficer =  function() {
		if ($('#officerTypeToggle1').is(':checked')) {
			$('#prefilledOfficer').parents('tr').fadeIn();
			$('#officerName').parents('tr').hide();
			$('#officerType').parents('tr').hide();
		} else if ($('#officerTypeToggle2').is(':checked')) {
			$('#prefilledOfficer').parents('tr').hide();
			$('#officerName').parents('tr').fadeIn();
			$('#officerType').parents('tr').fadeIn();
		} else {
			$('#prefilledOfficer').parents('tr').hide();
			$('#officerName').parents('tr').hide();
			$('#officerType').parents('tr').hide();
		}
	}

	/**
	 * Provides adding add shareholder link
	 *
	 * @return void
	 */
	this.createAddShareholderLink =  function() {
		$fieldset = $('input[name^="name"]:last').parents('fieldset');
		$a = $('<a href="#" id="ajax_add_shareholder" style="position: absolute; top: -15px; right: 0px;">Add Shareholder</a>').bind('click', function() {
				Dividend.processShareholderAjaxRequest($(this));
				return false;
			});
		$add = $('<div style="position: relative; height: 20px;"/>').html($a);
		$add.insertAfter($fieldset);
	}

	/**
	 * Provides adding remove shareholder link
	 *
	 * @return void
	 */
	this.createRemoveShareholderLink =  function() {
		if ($('input[name^="name"]').length > 1) {
			$fieldset = $('input[name^="name"]:last').parents('fieldset');
			$a = $('<a href="#" id="ajax_remove_shareholder" style="position: absolute; top: -10px; right: 0px;">remove</a>').bind('click', function() {
				Dividend.processShareholderAjaxRequest($(this));
				return false;
			});
			$remove = $('<div style="position: relative;"/>').html($a);
			$fieldset.prepend($remove);
		}
	}

	/**
	 * Provides init form after ajax request
	 *
	 * @return void
	 */
	this.initializeForm =  function() {
		//Dividend.toggleShare();
		//Dividend.toggleOfficer();
		Dividend.createAddShareholderLink();
		Dividend.createRemoveShareholderLink();
		//$('input[name="shareTypeToggle"]').click(function() { Dividend.toggleShare(); });
		//$('input[name="officerTypeToggle"]').click(function() { Dividend.toggleOfficer(); });
          $(".shareholderSelect").change(function () {
            var value = $(this).val();
            var value2 = $(this).attr("name").split('_')[1];
            shareholder = shareholders[value];
            for (var name2 in shareholder) {
                $('#'+name2+'_'+value2).val(shareholder[name2]);
            }
        });
        // prefill shares
        $("#prefilledShareClass").change(function () {
            var value = $(this).val();
            share = shares[value];
            for (var name in share) {
                $('#'+name).val(share[name]);
            }
        });

        // prefill officers
        $("#prefilledOfficer").change(function () {
            var value = $(this).val();
            officer = officers[value];
            for (var name in officer) {
                $('#'+name).val(officer[name]);
            }
        });
	}

	/**
	 * Provides init form after ajax request
	 *
	 * @return void
	 */
	this.onDocumentReady =  function() {
		Dividend.createAddShareholderLink();
		Dividend.createRemoveShareholderLink();
		//$('input[name="shareTypeToggle"]').click(function() { Dividend.toggleShare(); });
		//$('input[name="officerTypeToggle"]').click(function() { Dividend.toggleOfficer(); });
	}

	/**
	 * AJAX: add, remove shareholder
	 *
	 * @return void
	 */
	this.processShareholderAjaxRequest =  function($_this) {
		$.ajax({
			url: ($_this.attr('id') == 'ajax_add_shareholder') ? ajaxAddShareholderLink : ajaxRemoveShareholderLink,
			dataType: 'html',
			success: function(data){
				var formData = $('#form form').serializeArray();
	    		$('#form').html(data);
	    		for (var i in formData) {

	    			var name = formData[i].name.replace('[','\\[').replace(']','\\]');
	    			var value = formData[i].value;

	    			// radio buttons
	    			if (name == 'shareTypeToggle' || name == 'officerTypeToggle') {
	    				$('#' + name + value).attr('checked', true);
	    			} else {
	    				$('#' + name).val(value);
	    			}
				}
				Dividend.initializeForm();
			}
		});
	}
}

var AuthForm = function(contextWindow, beforeElement) {
	var errorElem = '<div class="flash error"></div>';
	var context = contextWindow.document;

	this.handleSuccess = function(location) {
		contextWindow.location = location;
	}
	this.handleError = function(error, button, form) {
		var appendHmtl = $(errorElem).text(error);
		$(beforeElement, context).before(appendHmtl);
		//hide frame
		$(form, context).hide();
		//buttons
		$(button, context).show();
		$(context).scrollTop(1);
	}
}

/* support link */
$(function(){
	$('.support_link').click(function() {
		window.open('https://www.companiesmadesimple.com/support/visitor/index.php?/Default/LiveChat/Chat/Request/_sessionID=/_promptType=chat/_proactive=0/_filterDepartmentID=16/_randomNumber=doyjlfgg6lcmhg86mjiahgzyq24efhrs/_fullName=/_email=/', 'livechatwin', 'toolbar=0,location=0,directories=0,status=1,menubar=0,scrollbars=0,resizable=1,width=600,height=680');
		return false;
	});
});

$(document).ready( function(){
    $('body').tooltip({
        selector: '[data-toggle="tooltip"]',
        html: true
    });

    $(".help-button a").hover(
        function() {
            $(this).next("em").stop(true, true).animate({opacity: "show"}, "slow");
        },
        function() {
            $(this).next("em").animate({opacity: "hide"}, "fast");
        }
    );

    $(".help-button-matrix a").hover(
        function() {
            $(this).next("em").stop(true, true).animate({opacity: "show"}, "slow");
        },
        function() {
            $(this).next("em").animate({opacity: "hide"}, "fast");
        }
    );
	$(".help-button-matrix span").hover(
        function() {
            $(this).next("em").stop(true, true).animate({opacity: "show"}, "slow");
        },
        function() {
            $(this).next("em").animate({opacity: "hide"}, "fast");
        }
    );

    $('#cse-search-box input[name="q"]').focus(function () {
        this.value = '';
    });

    $('.home-page-tooltip-icq').each(function() {
        var self = $(this);
        self.tooltip({
            tip : "#tooltip" +  $(this).attr('rel'),
            position: 'center left',
            offset: [-15, -15],
            opacity: 1
        });
    });

    // general handler for confirmations
    $('a[data-confirm]').click( function(e){
        if (!confirm($(this).attr('data-confirm'))) {
            e.stopImmediatePropagation();
            e.preventDefault();
        }
    });

    $('a.feedback_show').click(function(){
        var url = $(this).attr('href');
        overlay(url);
        return false;
    });

    //$('a.change-company-auth-code, a.popup-auth-code-required').on('click.auth-code', function() {
    $('a.auth-code-required').on('click', function() {
        if (!companyHasAuthenticationCode) {
            var url = $(this).attr('href');
            authCodeOverlay(url);
            return false;
        }
    });

    $('a.auth-code-required-popup').on('click', function() {
        var url = $(this).attr('href');
        if (!companyHasAuthenticationCode) {
            authCodeOverlay(url, true);
        } else {
            overlay(url);
        }
        return false;
    });

    $('.popupnew').click(function(){
        var url = $(this).attr('href');
        overlay(url);
        return false;
    });

    $('.popup-jquery-tools').click(function(){
        var url = $(this).attr('href');
        $(url).overlay({
             load: true,
             left:"center",
             mask: '#000',
             oneInstance: false,
             api : true,
             width: '350'
        }).load();
        return false;
    });

    /**
     * Simple Popup Dialog - CF Page choose bank account
     *
     * load a specific content
     */
    $("a.cf-more-info[rel]").click(function(){
        var html = $('#'+ $(this).attr('rel') ).html();
        $overlay = $('<div class="overlay">'+ html +'</div>');
        $('body').append($overlay);
        openOverlay($overlay);
        return false;
    });

    $(".startup-overlay-link a[rel]").click(function(){
        var html = $('#'+ $(this).attr('rel') ).html();
        $overlay = $('<div class="overlay" id="startup-overlay">'+ html +'</div>');
        $('body').append($overlay);
        openOverlay($overlay);
        return false;
    });


    $("#matrix-page a[rel]").tooltip({
        fadeOutSpeed: 100,
        position: "center right",
        offset: [1, 10],
        layout: '<div><div class="img-arrow">&nbsp;</div></div>'
    });
});

var openNortonSecure = function() {
    var link = 'https://trustsealinfo.verisign.com/splash?form_file=fdf/splash.fdf&dn=www.companiesmadesimple.com&lang=en';
    var windowName = "norton_secure";
    var windowSizeArray = ["width=550,height=460"];
    window.open(link, windowName, windowSizeArray[0]);
    return false;
}

var creditCardSafeLink = function() {
    var link = 'https://www.securitymetrics.com/site_certificate.adp?s=94%2e236%2e66%2e34&i=374287';
    var windowName = "norton_secure";
    var windowSizeArray = ["width=650,height=460"];
    window.open(link, windowName, windowSizeArray[0]);
    return false;
}

var addQueryParameter = function(url, key, value) {
    var separator = '?';
    if (url.indexOf('?') != -1) {
        separator = '&';
    }
    return url + separator + key + '=' + value;
}

var overlay = function(url) {
    $overlay = $('<div class="overlay"><iframe id="overlay_iframe" src="' + addQueryParameter(url, 'popup', 1) + '" frameborder="0" width="600" height="475"></iframe></div>');
    $('body').append($overlay);
    openOverlay($overlay);
}

var authCodeOverlay = function(url, popup) {
    url = addQueryParameter(url, 'authCodePopup', 1);
    if (popup) {
        url = addQueryParameter(url, 'popup', 1);
    }
    $overlay = $('<div class="overlay"><iframe id="overlay_iframe" src="' + url + '" frameborder="0" width="600" height="475"></iframe></div>');
    $('body').append($overlay);
    openOverlay($overlay);
}

var openOverlay = function(elem) {
	return $(elem).overlay({
             effect: 'apple',
             load: true,
             left:"center",
             mask: '#000',
             oneInstance: false,
             api : true
     });
}

var openOverlay2 = function(elem) {
	$(elem).overlay({
             effect: 'apple',
             load: true,
             left:"center",
             mask: '#000',
             oneInstance: false,
             api : true,
             top : "0px"
     });
}

var defaultOverlay = function(elem) {
	return $(elem).overlay({
             load: true,
             left:"center",
             mask: '#000',
             oneInstance: false,
             api : true
     });
}

// ---------------------- MATRIX PAGE ---------------------------------------
var noTimeLeftSameDay = function()
{
    $('.result-name-time').remove();
    $('.result-name-time-off').show();
}


var UpdateTableHeaders =  function()
{
    $(".persist-area").each(function() {

        var el             = $(this),
            offset         = el.offset(),
            scrollTop      = $(window).scrollTop(),
            floatingHeader = $(".floatingHeader", this)

        if ((scrollTop > offset.top) && (scrollTop < offset.top + el.height())) {
            floatingHeader.css({
                "visibility": "visible"
            });
        } else {
            floatingHeader.css({
                "visibility": "hidden"
            });
        };
    });
}

function createEmbed()
 {
    $('#videoHomePage .embed-video').show().html('<object data="http://view.vzaar.com/1085832/flashplayer" height="234" id="video" type="application/x-shockwave-flash" width="416"><param name="allowFullScreen" value="true" /><param name="allowScriptAccess" value="always" /><param name="wmode" value="transparent" /><param name="movie" value="http://view.vzaar.com/1085832/flashplayer" /><param name="flashvars" value="endLink=https%3A%2F%2Fwww.companiesmadesimple.com%2F&amp;showplaybutton=false&amp;brandText=What+our+customers+say...&amp;autoplay=true&amp;endText=Search+For+Your+Company+Name+Above&amp;border=none" /><video controls height="234" id="vzvid" onclick="this.play();" poster="http://view.vzaar.com/1085832/image" preload="none" src="http://view.vzaar.com/1085832/video" width="416"></video></object>').fadeIn();
    $('#homeImage').delay(900).hide();
 }

// DOM Ready  --- persist headers
$(function() {

    if(window.oldIE == 1){
        var clonedHeaderRow;

        $(".persist-area").each(function() {
            clonedHeaderRow = $(".persist-header", this);
            clonedHeaderRow
                .before(clonedHeaderRow.clone())
                .css("width", clonedHeaderRow.width())
                .addClass("floatingHeader");

        });

        $(window)
        .scroll(UpdateTableHeaders)
        .trigger("scroll");
        }


});


$.Placeholders = function() {

    var selectors = {
            placeholders: $('input[placeholder]')
        };

    var togglePlaceholders = function()
    {
        return selectors.placeholders.each(function() {
            $(this)
            .data("holder", $(this).attr("placeholder"))
            .focusin(function(){
                $(this).attr('placeholder','');
            })
            .focusout(function(){
                $(this).attr('placeholder',$(this).data('holder'));
            });
        });
    }

    var toggleIEPlaceholders =  function()
    {
        selectors.placeholders.focus(function(){
            var input = $(this);
            v = input.val();
            if (v == '' || v == input.attr('placeholder')) {
                input.val('');
                input.removeClass('placeholder');
            }
        }).blur(function() {
            var input = $(this);
            if (input.val() == '' || input.val() == input.attr('placeholder')) {
                input.addClass('placeholder');
                input.val(input.attr('placeholder'));
            }
        }).blur();

        //clear forms before submit
        selectors.placeholders.parents('form').submit(function() {
            $(this).find('[placeholder]').each(function() {
                var input = $(this);
                if (input.val() == input.attr('placeholder')) {
                  input.val('');
                }
            })
        });
    }

    return {
        init: function() {
            if (!$.browser.msie) {
                togglePlaceholders();
            } else {
                toggleIEPlaceholders();
            }
        }
    }
}

window.cms = window.cms || {};

(function(form, $) {
    form.selectOption = function(mainElement, selectOptions, elementToApply, classToApply) {
        $(mainElement).change(function() {
            var selected = $(this).find(':selected');
            var cardType = selected.length > 0 ? selected.val() : null;
            if (cardType in selectOptions ) {
                $(elementToApply).removeClass(classToApply);
            } else {
                $(elementToApply).addClass(classToApply);
            }
        });
        $(mainElement).trigger('change');
    }

    form.largeSelect = function(mainElement, additionalClose) {
        $(mainElement).click(function() {
            var id = '#' + $(this).attr('data-toggle');
            $(this).find('a').toggleClass('fa-chevron-down fa-chevron-up');
            $(id).toggle();
            return false;
        });
        $(additionalClose).click(function() {
            $(mainElement).trigger('click');
        });
    }

}(window.cms.form = window.cms.form || {}, $));

(function(popup, $) {

    popup.overlay = function(mainElement, cancelElement) {
        $(mainElement).click(function() {
            var id = '#' + $(this).attr('data-toggle');
            var over =	$(id).overlay({
                load: true,
                left:"center",
                mask: '#000',
                oneInstance: false,
                api : true
            });
            over.load();
            return false;
        });
        $(cancelElement).click(function() {
            var id = '#' + $(mainElement).attr('data-toggle');
            $(id).overlay().close();
            return false;
        });

    }

}(window.cms.popup = window.cms.popup || {}, $));

// Track basic JavaScript errors
window.addEventListener('error', function(e) {
    _gaq.push([
        '_trackEvent',
        'JavaScript Error',
        e.message,
        e.filename + ':  ' + e.lineno,
        true
    ]);
});

// Track AJAX errors (jQuery API)
$(document).ajaxError(function(e, request, settings) {
    _gaq.push([
        '_trackEvent',
        'Ajax error',
        settings.url,
        e.result,
        true
    ]);
});

$(document).ready(function() {
    $('.tooltip-link').each(function() {
        var self = $(this);
        var position = self.data('tooltip-position') || 'top center';

        self.tooltip({
            tip: $(this).find('.tooltip'),
            position: position,
            offset: [-10, -5],
            delay: 0,
            opacity: 1
        });
    });

    $("#megamenu").find("a[href^='http://']").attr("target", "_blank");
});

$('.home-page-tooltip-right').each(function() {
    var self = $(this);
    self.tooltip({
        tip : "#tooltip" + $(this).attr('rel'),
        position: 'center right',
        offset: [-15, 0],
        opacity: 1
    });
});

$.fn.formData = function() {
    var paramObj = {};
    $.each(this.serializeArray(), function(_, kv) {
        if (paramObj.hasOwnProperty(kv.name)) {
            paramObj[kv.name] = $.makeArray(paramObj[kv.name]);
            paramObj[kv.name].push(kv.value);
        }
        else {
            paramObj[kv.name] = kv.value;
        }
    });
    return paramObj;
};

var getParameterByName = function(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
};
