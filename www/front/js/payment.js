// login and email related to payment page

var checkEmail = function (self, email) {
    if (window.CMS.Validator.validateEmail(email)) {
        $('.payment-page .email-form-check .ff_control_err').remove();
        produceEmail(self, email);

    } else {
        $('.payment-page .email-form-check .ff_control_err').remove();
        $('.payment-page .editEmailLink').after('<span class="ff_control_err new-err-msg" style="color:red">Email address is not valid!</span>');
    }
};

var produceEmail = function(elem, email) {
    $.ajax({
        url: $(elem).attr('href') + "&email=" + encodeURIComponent(email),
        type: 'GET',
        dataType: "json",
        data: {},
        success: function(data) {
            $('.payment-page .show-ckeck-company').fadeIn('slow');
            if (data.succes == 1) {
                if (data.showLogin == 1) {
                    $(".payment-page .email-form-check").hide();
                    $('.payment-page .check-email-login-form').fadeIn();
                    $(".payment-page form[name$='login'] #email").val($(".payment-page form[name$='sage_payment'] #emails").val());
                    $(".payment-page form[name$='login'] #password").focus().val(null);
                    $(".payment-page form[name$='login'] #email").attr('readonly', true).css('background-color', '#D3D3D3');
                    $('.payment-page .editEmailLinkLogin').fadeIn();

                } else {
                    $('.payment-page .isOkToShow').show();
                    $('.payment-page .isNotOkToShow').hide();
                    $('.payment-page .check-email-avaiability').hide();
                    doEnable();
                    $(".payment-page form[name$='sage_payment'] #emails").attr('readonly', true).css('background-color', '#D3D3D3');
                    $(".payment-page .pound-style").css('color', "#434343");
                    $(".payment-page .editEmailLink").show();
                    $(".payment-page .dec_new_cust").hide();
                    $(".payment-page .payment-email-ok").show();
                }
            } else {
                $('.payment-page .isNotOkToShow').show();
                $('.payment-page .isOkToShow').hide();
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert('Oops...an error occurred (form error)');
        },
        timeout: function() {
            alert('Oops...an error occurred (form timeout)');
        }
    });
};

var validateEmail = function($email) {
    if ($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,99})?$/;
        if (!emailReg.test($email)) {
            return false;
        } else {
            return true;
        }
    }
    return false;
};

var doDisable = function() {
    $('.fancy-combo').fancyCombo('disable');
    $(".payment-page form[name$='sage_payment'] input[type='text']").attr('disabled', 'disabled');
    $(".payment-page form[name$='sage_payment'] select").attr('disabled', 'disabled');
    $(".payment-page form[name$='sage_payment'] input[type='checkbox']").attr('disabled', 'disabled');
    $(".payment-page form[name$='sage_payment'] input[type='submit']").attr('disabled', 'disabled');
    $(".payment-page form[name$='sage_payment'] label").css('color', '#CCCCCC');
    $(".payment-page form[name$='sage_payment'] input[type='button']").attr('disabled', 'disabled');
    $(".payment-page form[name$='sage_payment'] input[type='radio']").attr('disabled', 'disabled');
    $(".payment-page form[name$='sage_payment'] button[id='submit']").attr('disabled', 'disabled');
};

var doEnable = function() {
    $('.fancy-combo').fancyCombo('enable');
    $(".payment-page form[name$='sage_payment'] input[type='text']").removeAttr('disabled');
    $(".payment-page form[name$='sage_payment'] select").removeAttr('disabled');
    $(".payment-page form[name$='sage_payment'] input[type='checkbox']").removeAttr('disabled');
    $(".payment-page form[name$='sage_payment'] input[type='submit']").removeAttr('disabled');
    $(".payment-page form[name$='sage_payment'] label").css('color', "#434343");
    $(".payment-page form[name$='sage_payment'] input[type='button']").removeAttr('disabled');
    $(".payment-page form[name$='sage_payment'] input[type='radio']").removeAttr('disabled');
    $(".payment-page form[name$='sage_payment'] button").removeAttr('disabled');

};

// DOCUMENT READY!!!
$(document).ready(function() {

    $('#country option:selected').each(function () {
		if($(this).text() == "United States")
		{
	      	$('.state').show();
	    }
	    else
	    {
	       	$('.state').hide();
	    }
	});

	$('#cardType option:selected').each(function () {
		if(($(this).text() == "Maestro") || ($(this).text() == "Solo"))
		{
	       	$('.issuered').show();
	    }
	    else
	    {
	      	$('.issuered').hide();
	    }
	});

	$('#cardType').change(function () {
		$('#cardType option:selected').each(function () {
			if(($(this).text() == "Maestro") || ($(this).text() == "Solo"))
			{
	         	$('.issuered').show();
	        }
	        else
	        {
	        	$('.issuered').hide();
	        }
	     });
	});

    updateCvvHint();
    $('#cardType').change(updateCvvHint);

    function updateCvvHint () {
        $('#cardType option:selected').each(function () {
            if(($(this).text() == "American Express"))
            {
                $('#amex-cvv-hint').show();
                $('#general-cvv-hint').hide();
            }
            else
            {
                $('#general-cvv-hint').show();
                $('#amex-cvv-hint').hide();

            }
        });
    }

	$('#country').change(function () {
		$('#country option:selected').each(function () {
			if($(this).text() == "United States")
			{
	         	$('.state').show();
	        }
	        else
	        {
	        	$('.state').hide();
	        }
	     });
	});

	$(".help-button2 a").hover(
        function() {
            $(this).next("em").stop(true, true).animate({opacity: "show"}, "slow");
        },
        function() {
            $(this).next("em").animate({opacity: "hide"}, "fast");
        }
    );

    // This shows a custom flash message if a user selects Spain from the billing Country dropdown.
    // The message directs them to use Paypal because Sagepay has a LOT of issues with Spanish cards.
    $('.payment-page #country, .payment-page-customer #country').change(function() {
        var isSpain = $('option:selected', $(this)).text() == 'Spain';
        $('.payment-page #spaintext, .payment-page-customer #spaintext').toggle(isSpain);
    });

    // login/emails
    doDisable();
    $(".payment-page form[name$='sage_payment'] #emails").removeAttr('disabled');
    $(".payment-page form[name$='sage_payment'] label[for='emails']").css('color', "#434343");
    $(".payment-page .pound-style").css('color', "#D3D3D3");
    $(".payment-page .check-email-login-form").hide();
    $(".payment-page .payment-email-ok").hide();
    if ($("form[name$='sage_payment'] #emails").val() != 0) {
        $('.payment-page .editEmailLink').show();
        $('.payment-page .check-email-avaiability').hide();
        doEnable();
        $(".payment-page form[name$='sage_payment'] #emails").attr('readonly', true).css('background-color', '#D3D3D3');

    };

    $(".payment-page .editEmailFields").click(function() {
        doDisable();
        $(".payment-page form[name$='sage_payment'] #emails").removeAttr('disabled');
        $(".payment-page form[name$='sage_payment'] #emails").removeAttr('readonly', true).removeAttr('style');
        $(".payment-page form[name$='sage_payment'] label[for='emails']").css('color', "#434343");
        $(".payment-page .pound-style").css('color', "#D3D3D3");
        $('.payment-page .isOkToShow').hide();
        $('.payment-page .isNotOkToShow').hide();
        $(".payment-page .editEmailLink").hide();
        $(".payment-page .check-email-avaiability").show();
        $(".payment-page .dec_new_cust").show();
        $(".payment-page .payment-email-ok").hide();

    });

    $('.payment-page .editEmailLinkLogin').click(function() {
        $(".payment-page .check-email-login-form").hide();
        $(".payment-page .email-form-check").show();
    });


    $('.payment-page .forgotpassword-email').click(function() {
        if (!confirm('We will email you a new password. Are you sure?')) {
            return false;
        }
        $('.payment-page .loading-image').show();
        $('.payment-page .forgotpassword-email').hide();
        $('.payment-page .newPaswwordSent').hide();
        var self = $(this);
        $.ajax({
            url: self.attr('href') + "&email=" + encodeURIComponent($("#email").val()),
            type: 'GET',
            dataType: "json",
            data: {},
            success: function(data) {
                if (data.succes == 1) {
                    $('.payment-page .loading-image').hide();
                    $('.payment-page .newPaswwordSent').show();
                    $('.payment-page .after-emailing').show();
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert('Oops...an error occurred (form error)');
            },
            timeout: function() {
                alert('Oops...an error occurred (form timeout)');
            }
        });

        return false;
    });

    $('.payment-page form[name$="_login"]').live('submit', function() {
        var self = $(this);
        var dataForm = $(self).serialize();
        $.ajax({
            type: "POST",
            url: self.attr('action'),
            data: dataForm,
            dataType: "json",
            success: function(data) {
                if (data.error == 1) {
                    if (data.message) {
                        $(".new-err-msg").html(data.message.password);
                    }
                }
                if (data.redirect == 1) {
                    window.location.href = data.link;
                }

            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert('Oops...an error occurred (form error)');
            },
            timeout: function() {
                alert('Oops...an error occurred (form timeout)');
            }
        });
        return false;
    });


    //check if email exists
    $('.payment-page .check-email-avaiability').click(function() {
        var email = $(".payment-page #emails").val();
        checkEmail(this, email);
        return false;
    });

    $('.payment-page #emails').keypress(function(e) {
        if (e.which == 13) {
            checkEmail($('.payment-page .check-email-avaiability'), $(".payment-page #emails").val());
        }
    });

    function round(amount)
    {
        amount = parseFloat(amount);
        return amount.toFixed(2);
    }

    function showCreditPrice(elem) {
        $('#credit-error').show().removeClass('hidden');
        $('.credit-price').show().removeClass('hidden');
        $('#credit-price').html('-&pound;&nbsp;' + round(elem.data('credit')));
        $('.price-total').html(
            '<strong id="recalculated-total">&pound;&nbsp;'
            + round(elem.data('price') - elem.data('credit')) + '</strong>'
        );
    }
    function hideCreditPrice(elem) {
        if ($("#recalculated-total").length > 0) {
            $('.price-total').html('<strong>&pound;&nbsp;' + round(elem.data('price')) + '</strong>');
            $('#credit-error').hide();
            $('.credit-price').hide();
        }
    }

    // check for payment on Account
    $("#credit").change(function() {
        var elem = $(this);
        var paymentTypeElement = "form[name$='sage_payment'] input[name='paymentType']";
        var elements = '#paymentMethods, #fieldset_1';

        var submitElement = '.ladda-label';
        if (elem.is(":checked")) {
            if (elem.data('price') > elem.data('credit')) {
                showCreditPrice(elem);
            } else {
                $(elements).hide('slow');
                $(submitElement).html("Pay using credit");
                $(paymentTypeElement).val(10);
            }
        } else {
            hideCreditPrice(elem);

            $(elements).show('slow');
            $('#selectedMethod').trigger('change');
        }
    });
    $("#credit").trigger('change');

    $('#selectedMethod').change(function() {
        var $paymentTypeElement = $("form[name$='sage_payment'] input[name='paymentType']");
        var $element = $('#fieldset_1');
        var $submitElement = $('.ladda-label');
        switch (this.value) {
            case '0':
                // customer with no token, token is stored
                $element.show("slow");
                $submitElement.html("Pay using card");
                $paymentTypeElement.val(6);
                break;
            case 'paypal':
                // paypal
                $element.hide("slow");
                $submitElement.html("Pay using Paypal");
                $paymentTypeElement.val(1);
                break;
            case 'one_off_card':
                // customer has token but pays with a different card,
                // new token is not stored
                $element.show("slow");
                $submitElement.html("Pay using card");
                $paymentTypeElement.val(6);
                break;
            default:
                // customer pays with existing token
                $element.hide("slow");
                $submitElement.html("Pay using card");
                $paymentTypeElement.val(6);
                break;
        }
    }).trigger('change');

    $('form[name$="sage_payment"]').submit(function() {
        $(this).submit(function() {
            return false;
        });
        $('#submit').val('Processing...');
    });
});
