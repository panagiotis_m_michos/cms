(function(CMS, $) {

    var ServiceSettingsPopovers = function(settingsUrl) {
        this.popoverTogglersSelector = '[data-toggle="settings-popover"]';
        this.settingsUrl = settingsUrl;
    };

    ServiceSettingsPopovers.prototype.init = function() {
        var self = this;

        $(document).on('click', '.popover-title .close', function() {
            var serviceId = $(this).data('service-id');
            self.closePopover(serviceId);
        });

        $(document).on('click', '[data-action="enable-reminders"].toggle-link', function () {
            self.enableEmailReminders($(this).data('service-id'));
            return false;
        }).on('click', '[data-action="disable-reminders"].toggle-link', function () {
            self.disableEmailReminders($(this).data('service-id'));
            return false;
        });

        this.initPopovers();
    };

    ServiceSettingsPopovers.prototype.initPopovers = function() {
        var self = this;

        this.getPopoverTogglers().popover({
            placement: 'left',
            html: 'true',
            title: function () {
                return 'Settings<i class="loading"></i><i class="hidden close fa fa-times grey3" data-service-id="' + $(this).data('service-id') + '">&nbsp;</i>'
            },
            content: function () {
                self.loadContent($(this));
                return 'Loading...';
            },
            animation: false
        }).on('click', function () {
            self.getPopoverTogglers().not(this).popover('hide');
        });
    };

    ServiceSettingsPopovers.prototype.loadContent = function($toggler) {
        var self = this;
        var serviceId = $toggler.data('serviceId');

        $.getJSON(this.getSettingUrlForService(serviceId), function(data) {
            self.renderSettings(serviceId, data);
            self.hideLoading(serviceId);
            $toggler.attr('data-original-title', $toggler.data('bs.popover').$tip.find('.popover-title').html());
        });
    };

    ServiceSettingsPopovers.prototype.renderSettings = function(serviceId, data)
    {
        var $toggler = this.getPopoverTogglerForService(serviceId);
        var emailReminders = '<b>Email reminders:</b> ';

        if (data['emailReminders'] == true) {
            emailReminders += '<b>On</b>';
            if (data['canDisableEmailReminders']) {
                emailReminders += '<a href="#" class="toggle-link" data-action="disable-reminders" data-service-id="' + serviceId + '">Off</a>'
            } else {
                emailReminders += '<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="You can\'t turn email reminders off if your service is set to be automatically renewed"></i>'

            }
        } else {
            emailReminders += '<b>Off</b>';
            emailReminders += '<a href="#" class="toggle-link" data-action="enable-reminders" data-service-id="' + serviceId + '">On</a>'
        }

        var popover = $toggler.data('bs.popover');
        $toggler.attr('data-content', emailReminders);

        popover.setContent();
        popover.show();

        this.hideLoading(serviceId);
    };

    ServiceSettingsPopovers.prototype.getSettingUrlForService = function(serviceId) {
        return this.settingsUrl.replace('{serviceId}', serviceId);
    };

    ServiceSettingsPopovers.prototype.getPopoverTogglers = function() {
        return $(this.popoverTogglersSelector);
    };

    ServiceSettingsPopovers.prototype.getPopoverTogglerForService = function(serviceId) {
        return this.getPopoverTogglers().filter('[data-service-id=' + serviceId + ']');
    };

    ServiceSettingsPopovers.prototype.showLoading = function(serviceId) {
        var popover = this.getPopoverTogglerForService(serviceId).data('bs.popover');
        popover.$tip.find('h3 .loading').removeClass('hidden');
        popover.$tip.find('h3 .close').addClass('hidden');
    };

    ServiceSettingsPopovers.prototype.hideLoading = function(serviceId) {
        var popover = this.getPopoverTogglerForService(serviceId).data('bs.popover');
        popover.$tip.find('h3 .loading').addClass('hidden');
        popover.$tip.find('h3 .close').removeClass('hidden');
    };

    ServiceSettingsPopovers.prototype.closePopover = function(serviceId) {
        this.getPopoverTogglerForService(serviceId).popover('hide');
    };

    ServiceSettingsPopovers.prototype.disableEmailReminders = function(serviceId)
    {
        this.updateServiceSetting(serviceId, 'emailReminders', false);
    };

    ServiceSettingsPopovers.prototype.enableEmailReminders = function(serviceId)
    {
        this.updateServiceSetting(serviceId, 'emailReminders', true);
    };

    ServiceSettingsPopovers.prototype.updateServiceSetting = function(serviceId, settingName, settingValue)
    {
        var self = this;
        var data = {
            setting: settingName,
            value: settingValue
        };

        this.showLoading(serviceId);

        $.ajax({
            type: 'POST',
            url: self.getSettingUrlForService(serviceId),
            data: JSON.stringify(data),
            contentType: "application/json",
            dataType: 'json',
            success: function(data) {
                self.renderSettings(serviceId, data);
            },
            error: function() {
            }
        });
    };

    CMS.ServiceSettingsPopovers = ServiceSettingsPopovers;

})(window.CMS = window.CMS || {}, $);
