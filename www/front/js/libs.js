$(document).ready(function () {
    /**
     * Enable form submit button on inputs focus, disable on inputs blur (unless data's changed)
     *  <form action="..." method="..." name="..." class="togglableSubmitForm">
     *        <input type="text" value="..." id="..." name="..." />
     *        ...
     *        <input type="submit" name="..." value="..." id="submit" data-toggleElement="true" />
     *  </form>
     *  
     *  Form class:
     *  $this->addSubmit('submit', 'Submit')
     *      ->addAtrib('data-toggleElement', 'true');
     *      
     *  Latte template:
     *  <form action="{$form->getAction()}" method="post" name="{$form->getName()}" class="togglableSubmitForm">    
     */
    var dataHasChanged = false;
    $('.togglableSubmitForm').each(function () {
        var toggleElement = $(this).find('[data-toggleElement=true]');
        $(toggleElement).prop('disabled', true);
        $('.togglableSubmitForm *').filter(':input').each(function () {
            $(this).focus(function () {
                $(toggleElement).prop('disabled', false);
            });
            $(this).blur(function () {
                if (!dataHasChanged)
                    $(toggleElement).prop('disabled', true);
            });
            $(this).change(function () {
                dataHasChanged = true;
            });
        });
    });
    
});

/**
 * function toggles visibility of an element (target) based on the selected value of a Radio Button Group (master)
 *
 * USAGE:
 * in the form class:
 * $this->addRadio(
 *      'radio_name',
 *      'some_label',
 *      array(
 *          0 => 'value (target is hidden when selected)' ,
 *          1 => 'value (target is hidden when selected))'
 *      )
 *
 * in the template:
 * <div class="conditionalShown-Radio" data-master="radio_name" data-value-for-show="1">
 *
 * data-master is the name Radio Group
 * data-value-for-show is the value of the "master" radio that shows the element (target) when selected
 */

var toggleVisibility = function(masterVal, target, valueForShow) {
    if (masterVal == valueForShow) {
        target.show();
    } else {
        target.hide();
    }
};

var toggleConditionalVisibility = function(el){
    var element = $(el);
    var master = $('input[name='+element.data("master")+']');
    var checkedOption = $('input[name='+element.data("master")+']:checked');
    var valueForShow = element.data('value-for-show');
    toggleVisibility(checkedOption.val(), element, valueForShow);
    $(master).change(function(){
        checkedOption = $('input[name='+element.data("master")+']:checked');
        toggleVisibility(checkedOption.val(), element, valueForShow);
    });
};