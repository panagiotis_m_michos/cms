window.cms = window.cms || {};

(function(validations, $) {

    $.validator.addMethod("dateITA", function(value, element) {
        var check = false,
            re = /^\d{2}\/\d{2}\/\d{4}$/,
            adata, gg, mm, aaaa, xdata;
        if ( re.test(value)) {
            adata = value.split("/");
            gg = parseInt(adata[0], 10);
            mm = parseInt(adata[1], 10);
            aaaa = parseInt(adata[2], 10);
            xdata = new Date(aaaa, mm - 1, gg, 12, 0, 0, 0);
            if ( ( xdata.getUTCFullYear() === aaaa ) && ( xdata.getUTCMonth () === mm - 1 ) && ( xdata.getUTCDate() === gg ) ) {
                check = true;
            } else {
                check = false;
            }
        } else {
            check = false;
        }
        return this.optional(element) || check;
    }, "Please enter a correct date");

    $.validator.addMethod("dateUk", function(value, element) {
        var check = false,
            re = /^\d{2}\/\d{2}\/\d{4}$/,
            adata, gg, mm, aaaa, xdata;
        if ( re.test(value)) {
            adata = value.split("/");
            gg = parseInt(adata[0], 10);
            mm = parseInt(adata[1], 10);
            aaaa = parseInt(adata[2], 10);
            xdata = new Date(aaaa, mm - 1, gg, 12, 0, 0, 0);
            if ( ( xdata.getUTCFullYear() === aaaa ) && ( xdata.getUTCMonth () === mm - 1 ) && ( xdata.getUTCDate() === gg ) ) {
                check = true;
            } else {
                check = false;
            }
        } else {
            check = false;
        }
        return this.optional(element) || check;
    }, "Please enter a correct date");

    $.validator.addMethod("notTitleNorSingleChar", function(value, element){
        var check = true;

        var titles = [
            'Mr',
            'mr',
            'Miss',
            'miss',
            'Mrs',
            'mrs',
            'Ms',
            'ms',
            'Dr',
            'dr',
            'Prof',
            'prof',
            'Master',
            'master',
            'Rev',
            'rev',
            'Sir',
            'sir',
            'Lord',
            'lord',
            'Lady',
            'lady'
        ];

        var parts = value.split(" ");
        for (var i = 0; i < parts.length; i++) {
            var part = parts[i];
            if (titles.indexOf(part) >= 0 || part.length == 1) {
                check = false;
                break;
            }
        }

        return check;
    }, 'Single letters or titles are not accepted in the<br/>first name. Please insert your full first name<br/>without the title or abbreviations.<br/>(e.g.: instead of **Mr P** use **Paul**)');

    validations.useBoostrap = function(form) {
        var insertError = function(error, element) {
            var labels = element.nextAll('label.help-block');
            if (labels.length) {
                error.insertAfter(labels.last());
            } else {
                error.insertAfter(element);
            }
        };

        $.validator.setDefaults({
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                var errorPlacementAfter = element.parents('.form-error-after:eq(0)');
                if(errorPlacementAfter.length) {
                    insertError(error, errorPlacementAfter);
                } else if(element.parent('.input-group').length) {
                    insertError(error, element.parent());
                } else {
                    insertError(error, element);
                }
            }
        });
    };

    validations.handleLadda = function(form) {
        $('.ladda-button', form).click(function(e) {
            if (!$(form).valid()) {
                e.stopImmediatePropagation();
            }

        });
    };

    validations.myCompaniesFilter = function(formId, dateFIlterType, dateFrom, dateTo) {
        $(formId).validate({
            errorElement: 'span',
            errorClass: 'err',
            rules: {
                "dateFrom": {
                    required: {
                        depends: dateFromValidator
                    },
                    dateITA: {
                        depends: dateFromValidator
                    }
                },
                "dateTo": {
                    required: {
                        depends: dateToValidator
                    },
                    dateITA: {
                        depends: dateToValidator
                    }
                }
            }
        });

        function dateToValidator(element) {
            if ($(dateFilterType).val() != "") {
                if ($(element).val() != ""){
                    return true;
                } else if ($(dateFrom).val() == "") {
                    return true;
                }
            }
            return false;
        }

        function dateFromValidator(element) {
            if ($(dateFilterType).val() != "") {
                if ($(element).val() != ""){
                    return true;
                } else if ($(dateTo).val() == "") {
                    return true;
                }
            }
            return false;
        }
    };

   validations.idCheck = function(form) {
        validations.useBoostrap();
        validations.handleLadda(form);
        //pattern and required is present in html
        return $(form).validate({
            //groups: {
            //    passportNumber: "idCheckForm[passport][number][first] idCheckForm[passport][number][second] idCheckForm[passport][number][third]"
            //},
            rules: {
                'idCheckForm[personal][firstName]': {
                    required: true,
                    notTitleNorSingleChar: true,
                    pattern: '[\\sa-zA-Z0-9&/`´\'-.,;"!_ÀÁÂÃÄÅÆàáâãäåąæßÇçćčÐðÈÉÊËèéêëěğĞÌÍÎÏİıìíîïÒÓÔÕÖØøòóôõöŁłÑñńŞşšśÙÚÛÜùúûüÝŸýÿŻžżþÞ]{0,256}'
                },
                'idCheckForm[personal][middleName]': {
                    required: false,
                    pattern: '[\\sa-zA-Z0-9&/`´\'-.,;"!_ÀÁÂÃÄÅÆàáâãäåąæßÇçćčÐðÈÉÊËèéêëěğĞÌÍÎÏİıìíîïÒÓÔÕÖØøòóôõöŁłÑñńŞşšśÙÚÛÜùúûüÝŸýÿŻžżþÞ]{0,256}'
                },
                'idCheckForm[personal][lastName]': {
                    required: true,
                    pattern: '[\\sa-zA-Z0-9&/`´\'-.,;"!_ÀÁÂÃÄÅÆàáâãäåąæßÇçćčÐðÈÉÊËèéêëěğĞÌÍÎÏİıìíîïÒÓÔÕÖØøòóôõöŁłÑñńŞşšśÙÚÛÜùúûüÝŸýÿŻžżþÞ]{0,256}'
                },
                'idCheckForm[personal][dateOfBirth]': {
                    required: true,
                    dateUk: true
                },
                'idCheckForm[passport][number][first]': {
                    required: '#idCheckForm_documentType_0:checked',
                    minlength: 28,
                    maxlength: 28,
                    pattern: false
                },
                'idCheckForm[passport][number][second]': {
                    required: '#idCheckForm_documentType_0:checked',
                    minlength: 14,
                    maxlength: 14,
                    pattern: false
                },
                'idCheckForm[passport][number][third]': {
                    required: '#idCheckForm_documentType_0:checked',
                    minlength: 2,
                    maxlength: 2,
                    pattern: false
                },
                'idCheckForm[passport][expirationDate]': {
                    required: '#idCheckForm_documentType_0:checked',
                    dateUk: true
                },
                'idCheckForm[drivingLicense][number]': {
                    required: '#idCheckForm_documentType_1:checked',
                    minlength: 16,
                    maxlength: 16,
                    pattern: false
                },
                'idCheckForm[europeanCard][line1]': {
                    required: '#idCheckForm_documentType_2:checked',
                    minlength: 30,
                    maxlength: 30,
                    pattern: false
                },
                'idCheckForm[europeanCard][line2]': {
                    required: '#idCheckForm_documentType_2:checked',
                    minlength: 30,
                    maxlength: 30,
                    pattern: false
                },
                'idCheckForm[europeanCard][line3]': {
                    required: '#idCheckForm_documentType_2:checked',
                    minlength: 30,
                    maxlength: 30,
                    pattern: false
                },
                'idCheckForm[europeanCard][expirationDate]': {
                    required: '#idCheckForm_documentType_2:checked',
                    dateUk: true
                },
                'idCheckForm[europeanCard][countryOfNationality]': {
                    required: '#idCheckForm_documentType_2:checked'
                },
                'idCheckForm[europeanCard][countryOfIssue]': {
                    required: '#idCheckForm_documentType_2:checked'
                },
                'idCheckForm[address][search]': {
                    required: true
                },
                'idCheckForm[address][address1]': {
                    required: true,
                    pattern: '[\\sa-zA-Z0-9&/`´\'-.,;"!_ÀÁÂÃÄÅÆàáâãäåąæßÇçćčÐðÈÉÊËèéêëěğĞÌÍÎÏİıìíîïÒÓÔÕÖØøòóôõöŁłÑñńŞşšśÙÚÛÜùúûüÝŸýÿŻžżþÞ@]{0,256}'
                },
                'idCheckForm[address][address2]': {
                    pattern: '[\\sa-zA-Z0-9&/`´\'-.,;"!_ÀÁÂÃÄÅÆàáâãäåąæßÇçćčÐðÈÉÊËèéêëěğĞÌÍÎÏİıìíîïÒÓÔÕÖØøòóôõöŁłÑñńŞşšśÙÚÛÜùúûüÝŸýÿŻžżþÞ@]{0,256}'
                },
                'idCheckForm[address][city]': {
                    required: true,
                    pattern: '[\\sa-zA-Z0-9&/`´\'-.,;"!_ÀÁÂÃÄÅÆàáâãäåąæßÇçćčÐðÈÉÊËèéêëěğĞÌÍÎÏİıìíîïÒÓÔÕÖØøòóôõöŁłÑñńŞşšśÙÚÛÜùúûüÝŸýÿŻžżþÞ@]{0,256}'
                },
                'idCheckForm[address][county]': {
                    required: true
                },
                'idCheckForm[address][postcode]': {
                    required: true,
                    pattern: '[\\sa-zA-Z0-9\-.]{0,8}'
                }
            },
            messages: {
                'idCheckForm[passport][number][first]': {
                    required: "First section must be 28 characters long",
                    minlength: "First section must be 28 characters long",
                    maxlength: "First section must be 28 characters long"
                },
                'idCheckForm[passport][number][second]': {
                    required: "Second section must be 14 characters long",
                    minlength: "Second section must be 14 characters long",
                    maxlength: "Second section must be 14 characters long"
                },
                'idCheckForm[passport][number][third]': {
                    required: "Third section must be 2 characters long",
                    minlength: "Third section must be 2 characters long",
                    maxlength: "Third section must be 2 characters long"
                },
                'idCheckForm[drivingLicense][number]': {
                    required: "License number must be 16 characters long",
                    minlength: "License number must be 16 characters long",
                    maxlength: "License number must be 16 characters long"
                },
                'idCheckForm[europeanCard][line1]': {
                    required: "Line1 must be 30 characters long",
                    minlength: "Line1 must be 30 characters long",
                    maxlength: "Line1 must be 30 characters long"
                },
                'idCheckForm[europeanCard][line2]': {
                    required: "Line2 must be 30 characters long",
                    minlength: "Line2 must be 30 characters long",
                    maxlength: "Line2 must be 30 characters long"
                },
                'idCheckForm[europeanCard][line3]': {
                    required: "Line3 must be 30 characters long",
                    minlength: "Line3 must be 30 characters long",
                    maxlength: "Line3 must be 30 characters long"
                }
            }
        });
    };

    validations.wholesaleContactForm = function(form) {
        validations.useBoostrap(form);
        $(form).validate({
            rules: {
                'contact_form[fullName]': {
                    required: true
                },
                'contact_form[email]': {
                    required: true,
                    email: true
                },
                'contact_form[phone]': {
                    required: true
                }
            }
        });
    }

}(window.cms.validations = window.cms.validations || {}, $));
