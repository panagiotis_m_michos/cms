window.CMS = window.CMS || {};

(function(validator) {
    validator.validateEmail = function(email) {
        if (email) {
            var re = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,99})?$/;
            return re.test(email);
        }
        return false;
    }
}(window.CMS.Validator = window.CMS.Validator || {}));
