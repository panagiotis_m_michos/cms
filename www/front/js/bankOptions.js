(function(n) {
    function selected(list) {
        for (var i in list) {
            if ('applied' in list[i] && list[i].applied === false) {
                list[i].selected = true;
                return list[i].url;
            }
        }
        return null;
    }

    function select(event, context) {
        event.preventDefault();
        context.selected.url = context.items[context.index].url;
        for (var i in context.items) {
            context.items[i].selected = false;
        }
        context.items[context.index].selected = true;
    }

    n.init = function(element, list) {
        var arr = createArrayByExistingKeys.apply(null, list);
        rivets.bind(element, {items: arr, selected: { url: selected(arr) }, select: select});
    };

    n.dismissBankOffer = function(element, url) {
        $(element).on('click', function() {
            var promise = $.ajax({
                url: url,
                timeout: 5000
            });
            promise.fail(function() {
                alert('Could not save settings');
            });
        });
    };
})(window.bankOptions = {});