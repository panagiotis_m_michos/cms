
(function(idCheck, validations, $, hashHistory) {
    idCheck.init = function () {
        var selectedDocumentType = $('#idCheck [name="idCheckForm[documentType]"]:checked').val();
        var index = selectedDocumentType ? $('#tabs a[href="#' + selectedDocumentType.toLowerCase() + '"]').parent().index() : 0;

        $("#tabs").tabs({
            active: index,
            create: function (event, ui) {
                $('#idCheck [name="idCheckForm[documentType]"][value="' + ui.panel.attr('id').toUpperCase() + '"]').attr('checked', 'checked');
                $('#idCheck .selected-document-type').text(ui.tab.text());
            },
            activate: function (event, ui) {
                $('#idCheck [name="idCheckForm[documentType]"][value="' + ui.newPanel.attr('id').toUpperCase() + '"]').attr('checked', 'checked');
                $('#idCheck .selected-document-type').text(ui.newTab.text());
            }
        });

        $('#retry-id-check').click(function() {
            $('a.tab-btn[href="' + $(this).attr('href') + '"]').click();
            $($(this).data('show')).show();
            $($(this).data('hide')).hide();
        });

        var handleUk = function() {
            var elem = $('#idCheckForm_passport_number_second');
            var link = $('#passport-example');
            if ($(this).val() === 'United Kingdom') {
                if (elem.val() != elem.data('placeholder')) {
                    elem.val(elem.data('placeholder'));
                }
                elem.attr('readonly', 'readonly');
                link.attr('href', '#ukpassport');
            } else {
                elem.removeAttr('readonly');
                if (elem.val() == elem.data('placeholder')) {
                    elem.val('');
                }
                link.attr('href', '#international-passport');
            }
        };
        $('#idCheckForm_passport_country').change(handleUk).trigger('change');

        $(".focus-maxlength").keyup(function () {
            var elem = $(this);
            if (elem.val().length == elem.attr('maxlength')) {
                var form = elem.parents('form');
                var fields = $('.focus-maxlength:enabled:not([readonly])', form);
                var index = fields.index(this);
                if ( index > -1 && ( index + 1 ) < fields.length ) {
                    fields.eq( index + 1 ).focus();
                }
            }
        });

        if (window.capturePlus && 'listen' in capturePlus) {
            capturePlus.listen("options", function (options) { options.languagePreference = "ENG"; });
            capturePlus.listen('populate', function() {
                var dataContainer = $('.completion-address-data').empty();
                $('.completion-address-prefiller').hide();
                $('#idCheckForm_address').find('input').each(function() {
                    $('<div/>').text($(this).val()).appendTo(dataContainer);
                });
                $('.completion-address-details').show();
            });
        }

        /**
         * initial state
         */
        hashHistory.addEntry('', function(hash) {
            $('div.idcontent').hide();
            $('.toggle-btn-grp .tab-btn').removeClass('toggle-btn-on');
        });

        if (location.hash) {
            /**
             * initial state with a hash
             */
            hashHistory.addEntry(location.hash, function(hash) {
                $('div.idcontent').hide();
                $('a.tab-btn[href="' + hash + '"]').addClass('toggle-btn-on');
                $(hash).show();
            });
        }

        hashHistory.executeEntry(location.hash);

        validations.idCheck("#idCheck form");
    };
}(window.cms.idCheck = window.cms.idCheck || {}, window.cms.validations, $, uiserver.hashHistory));
