$(function() {
    $('body').tooltip({
        selector: '[data-toggle="tooltip"]',
        html: true
    });

    $(document).on('click', 'a[data-confirm]', function(e){
        if (!confirm($(this).data('confirm'))) {
            e.stopImmediatePropagation();
            e.preventDefault();
        }
    });
});



