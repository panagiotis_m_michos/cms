/**
 *  To use:
 *
 *  Create html
 *
 *  <div class="fancy-combo hidden" data-combo="#select-to-use">
 *      <div class=selected-value">
 *          <div class="selected-content">Option 2</div>        
 *          <div class="fright"><a href="javascript:;" class="combo-arrow fa fa-chevron-down">&nbsp;</a></div>
 *      </div>
 *      <div class="combo-options">
 *          <div class="option" data-option="1">Option 1</div>
 *          <div class="option" data-option-value="2">Option 2</div>
 *      </div>
 *  </div>
 *
 *  <select id="select-to-use">
 *      <option value="1">Option 1</option>
 *      <option value="2" selected="selected">Option 2</option>
 *  </select>
 *  
 *  Default behaviour
 *
 *  $('[data-option^=')
 *
 */

(function(controls, $) {

    'use strict';

    var FancyCombo = function(element, options) {
        this.options = $.extend({}, FancyCombo.DEFAULTS, options);
        this.element = $(element);
        this.selectBox = $(this.element.attr('data-combo'));
        if (this.selectBox.length < 1) throw 'select Box for ' + this.element.html() + ' not found!';
        this.arrow = $(this.options.arrow, this.element);
        this.hiddenOptions = $(this.options.hiddenOptions, this.element);
        this.selectedElement = $(this.options.selectedElement, this.element);
        this.elementToUpdate = $(this.options.elementToUpdate, this.selectedElement);
    };

    FancyCombo.DEFAULTS = {
        selectedElement: '.selected-value',
        elementToUpdate: '.selected-content',
        hiddenOptions: '.combo-options',
        arrow: '.combo-arrow',
        option: '.option'
    };

    FancyCombo.prototype.init = function() {
        var currentlySelected = $('[data-option="' + this.selectBox.val() + '"]', this.hiddenOptions);
        if (currentlySelected.length > 0) {
            this.elementToUpdate.html(currentlySelected.clone());
        }
        $(this.selectBox).hide();
        $(this.element).show();
        this.selectedElement.click($.proxy(this.toggle, this));
        $(this.options.option, this.hiddenOptions).click($.proxy(this.select, this));
    };

    FancyCombo.prototype.toggle = function() {
        if (this.selectedElement.hasClass('disabled')) {
            return;
        }
        this.arrow.toggleClass('fa-chevron-down fa-chevron-up');
        this.hiddenOptions.toggleClass('hidden');
    };
    
    FancyCombo.prototype.disable = function() {
        $(this.selectedElement).addClass('disabled');
    };
    
    FancyCombo.prototype.enable = function() {
        $(this.selectedElement).removeClass('disabled');
    };

    FancyCombo.prototype.select = function(e) {
        var optionElement = $(e.target);
        var selectValue = optionElement.attr('data-option');
        this.selectBox.val(selectValue);
        this.elementToUpdate.html(optionElement.clone());
        this.toggle();
        this.selectBox.trigger('change');
    };
    
    function Plugin(option) {
        return this.each(function () {
            var $this   = $(this);
            var data    = $this.data('msg.fancyCombo');
            var options = typeof option == 'object' && option;
            if (!data) {
                data = new FancyCombo(this, options);
                data.init();
                $this.data('msg.fancyCombo', data);
            }
            if (option == 'disable') data.disable();
            else if (option == 'enable') data.enable();
        });
    }
    
    $.fn.fancyCombo             = Plugin;
    $.fn.fancyCombo.Constructor = FancyCombo;
    
    $(document).ready(function () {
        $('.fancy-combo').each(function () {
            Plugin.call($(this));
        });
    });

}(window.controls = window.controls || {}, $));
