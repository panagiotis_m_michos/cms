(function(CMS, $) {
    var RenewalForm = function(selectorForm, serviceSettingsPopovers) {
        this.selectorForm = selectorForm;
        this.popoverTogglersSelector = '[data-toggle="autorenewal-popover"]';
        this.serviceSettingsPopovers = serviceSettingsPopovers;
        this.options = {
            selectorService: '[data-renewal-selector="service"]',
            selectorPriceSubtotal: '[data-renewal-selector="subtotal"]',
            selectorPriceVat: '[data-renewal-selector="vat"]',
            selectorPriceTotal: '[data-renewal-selector="total"]',
            selectorSubmit: '[data-renewal-selector="submit"]',
            selectorToggableCalculatePrice: '[data-renewal-toggable="calculatePrice"]',
            selectorToggableToggleCompany: '[data-renewal-toggable="toggleCompany"]',
            actionCalculatePrice: '[data-renewal-action="calculatePrice"]',
            actionToggleCompanyServices: '[data-renewal-action="toggleCompanyServices"]',
            actionToggleAllCompanyServices: '[data-renewal-action="toggleAllCompanyServices"]',
            toggleAllState: 'shown'
        };
    };

    RenewalForm.prototype.init = function() {
        var self = this;

        $(document)
            .on('click', this.options.actionCalculatePrice, function(){
                self.calculatePrice();
            })
            .on('click', this.options.actionToggleCompanyServices, function(e) {
                e.preventDefault();
                self.toggleCompanyServices($(this));
            })
            .on('click', this.options.actionToggleAllCompanyServices, function(e) {
                e.preventDefault();
                self.toggleAllCompanyServices();
            });

        $(this.selectorForm).submit(function(e){
            e.preventDefault();
            self.submitForm();
        });

        $(document).on('change', '.radio-enable-autorenewal', function() {
            var $this = $(this);
            var serviceId = $this.data('service-id');
            var $popoverContent = self.getPopoverTogglerForService(serviceId).data('bs.popover').$tip.find('.popover-content');

            self.enableAutoRenewal($this);

            $popoverContent.find('.radio-disable-autorenewal').removeAttr('checked');
            $(this).attr('checked', 'checked');
        });

        $(document).on('change', '.radio-disable-autorenewal', function() {
            var $this = $(this);
            var serviceId = $this.data('service-id');
            var $popoverContent = self.getPopoverTogglerForService(serviceId).data('bs.popover').$tip.find('.popover-content');

            self.disableAutoRenewal($this);

            $popoverContent.find('.radio-disable-autorenewal').removeAttr('checked');
            $(this).attr('checked', 'checked');
        });

        self.toggleAllCompanyServices();
        $(this.selectorForm).FormCache();
        self.preExpandCompanies();
        self.calculatePrice();
        self.initPopovers();

        $(document).on('click', '.popover-title .close', function() {
            var serviceId = $(this).data('service-id');
            self.closePopover(serviceId);
        });
    };

    RenewalForm.prototype.initPopovers = function() {
        var self = this;

        this.getPopoverTogglers().popover({
            placement: 'top',
            html: 'true',
            title: function() {
                return $('.autorenewal-popover-title-' + $(this).data('service-id')).html();
            },
            content: function () {
                return $('.autorenewal-popover-content-' + $(this).data('service-id')).html();
            },
            animation: false
        }).on('click', function() {
            self.getPopoverTogglers().not(this).popover('hide');
        });
    };

    RenewalForm.prototype.getPopoverTogglers = function() {
        return $(this.popoverTogglersSelector);
    };

    RenewalForm.prototype.disableSubmit = function() {
        $(this.options.selectorSubmit).addClass('loading').attr('disabled', 'disabled');
    };

    RenewalForm.prototype.enableSubmit = function() {
        $(this.options.selectorSubmit).removeClass('loading').removeAttr('disabled');
    };

    RenewalForm.prototype.showLoading = function(serviceId) {
        var popover = this.getPopoverTogglerForService(serviceId).data('bs.popover');
        popover.$tip.find('h3 .loading').removeClass('hidden');
        popover.$tip.find('h3 .close').addClass('hidden');
    };

    RenewalForm.prototype.hideLoading = function(serviceId) {
        var popover = this.getPopoverTogglerForService(serviceId).data('bs.popover');
        popover.$tip.find('h3 .loading').addClass('hidden');
        popover.$tip.find('h3 .close').removeClass('hidden');
    };

    RenewalForm.prototype.getCheckedProductIds = function() {
        return $(this.options.selectorService).filter(':checked').map(function () {
            return $(this).val();
        }).toArray();
    };

    RenewalForm.prototype.getCheckedServiceIds = function() {
        return $(this.options.selectorService).filter(':checked').map(function () {
            return $(this).attr('id');
        }).toArray();
    };

    RenewalForm.prototype.currency = function(price) {
        price = parseFloat(price).toFixed(2);
        return '£' + price;
    };

    RenewalForm.prototype.displayPrice = function(price) {
        $(this.options.selectorPriceSubtotal).html(this.currency(price.subtotal));
        $(this.options.selectorPriceVat).html(this.currency(price.vat));
        $(this.options.selectorPriceTotal).html(this.currency(price.total));
    };

    RenewalForm.prototype.calculatePrice = function() {
        var self = this;
        var ids = this.getCheckedProductIds();
        var filteredUrl = $(location).attr('href').split('#')[0];
        $.ajax({
            url: filteredUrl,
            type: "GET",
            dataType: 'json',
            data: {
                do: 'calculateTotal',
                ids: ids
            },
            beforeSend: function() {
                self.disableSubmit();
            },
            success: function(response) {
                if (response.error) {
                    alert(response.message);
                } else {
                    self.displayPrice(response.price);
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError);
            },
            complete: function() {
                self.enableSubmit();
            }
        });
    };

    RenewalForm.prototype.enableAutoRenewal = function($link) {
        var self = this;
        $.ajax({
            url: $link.data('url'),
            type: "GET",
            dataType: 'json',
            data: {
                checkedServiceIds: self.getCheckedServiceIds()
            },
            beforeSend: function() {
                self.showLoading($link.data('service-id'));
            },
            success: function(response) {
                if (response.error) {
                    alert(response.message);
                } else {
                    var $tr = $link.closest('tr');
                    var $checkbox = $tr.find('[data-renewal-selector="service"]:checkbox');
                    var serviceId = $link.data('service-id');

                    $tr.find('.disabled-autorenewal-container').addClass('hidden');
                    $tr.find('.enabled-autorenewal-container').removeClass('hidden');
                    $tr.find('.renewal-price').addClass('disabled');

                    $checkbox.removeAttr('checked').attr('disabled', 'disabled');
                    self.getPopoverTogglerForService(serviceId).popover('destroy');
                    self.afterRefresh($tr, serviceId, response.template, response.showAdditionalInfo);

                    self.calculatePrice();
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError);
            }
        });
    };

    RenewalForm.prototype.disableAutoRenewal = function($link) {
        var self = this;
        $.ajax({
            url: $link.data('url'),
            type: "GET",
            dataType: 'json',
            data: {
                checkedServiceIds: self.getCheckedServiceIds()
            },
            beforeSend: function() {
                self.showLoading($link.data('service-id'));
            },
            success: function(response) {
                if (response.error) {
                    alert(response.message);
                } else {
                    var $tr = $link.closest('tr');
                    var $checkbox = $tr.find('[data-renewal-selector="service"]:checkbox');
                    var serviceId = $link.data('service-id');

                    $tr.find('.disabled-autorenewal-container').removeClass('hidden');
                    $tr.find('.enabled-autorenewal-container').addClass('hidden');
                    $tr.find('.renewal-price').removeClass('disabled');
                    $checkbox.removeAttr('disabled');

                    self.afterRefresh($tr, serviceId, response.template);
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError);
            }
        });
    };

    RenewalForm.prototype.afterRefresh = function($tr, serviceId, template, showAdditionalInfo) {
        $tr.closest('.company-block').replaceWith(template);
        this.initPopovers();
        this.showPopoverWithFlashMessage(serviceId, showAdditionalInfo);

        this.serviceSettingsPopovers.initPopovers();
    };

    RenewalForm.prototype.showPopoverWithFlashMessage = function(serviceId, showAdditionalInfo) {
        var $toggler = this.getPopoverTogglerForService(serviceId);
        $toggler.popover('show');

        var popover = $toggler.data('bs.popover');
        var $flashMessage = popover.$tip.find('.autorenewal-popover-flash');

        if (showAdditionalInfo) {
            popover.$tip.find('.autorenewal-popover-flash-overdue').removeClass('hidden');
        }
        $flashMessage.removeClass('hidden').hide().fadeIn(200);

        var flashMessageHeight = $flashMessage.outerHeight(true);
        popover.$tip.css('top', '-=' + parseFloat(flashMessageHeight));
    };

    RenewalForm.prototype.toggleCompanyServices = function(ahref) {
        var table = ahref.closest('table');

        $('tbody', table).toggleClass('hidden');
        $('thead tr', table).toggleClass('hidden');
    };

    RenewalForm.prototype.toggleAllCompanyServices = function() {
        switch (this.options.toggleAllState) {
            case 'shown':
                $('.company-block tbody').addClass('hidden');
                $('.company-block thead tr').addClass('hidden');
                $('.company-block thead tr.company-header-collapsed').removeClass('hidden');
                $('.toggle-company-hide').addClass('hidden');
                $('.toggle-company-show').removeClass('hidden');

                this.options.toggleAllState = 'hidden';
                break;

            case 'hidden':
                $('.company-block tbody').removeClass('hidden');
                $('.company-block thead tr').addClass('hidden');
                $('.company-block thead tr.company-header-expanded').removeClass('hidden');
                $('.toggle-company-hide').removeClass('hidden');
                $('.toggle-company-show').addClass('hidden');

                this.options.toggleAllState = 'shown';
                break;
        }
    };

    RenewalForm.prototype.submitForm = function() {
        var self = this;
        this.disableSubmit();
        $.ajax({
            type: $(this.selectorForm).attr('method'),
            url: $(this.selectorForm).attr('action'),
            data: $(this.selectorForm).serialize(),
            success: function(response) {
                if (response.error == false) {
                    document.location.href = response.redirect;
                } else {
                    self.enableSubmit();
                    var jsonResponse = $.parseJSON(response);
                    var message = jsonResponse.message.submit || jsonResponse.message;
                    alert(message);
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError);
                self.enableSubmit();
            }
        });
    };

    RenewalForm.prototype.preExpandCompanies = function() {
        var self = this;

        var companyId = getParameterByName('companyId');
        if (companyId) {
            $('.expand-'+companyId).click();
        }

        $(self.selectorForm).find('input').each(function(){
            if($(this).attr('type') != 'submit') {
                var input = $(this);
                if(input.attr('type') == 'checkbox') {
                    if(input.is(':checked') && input.attr('data-companyId') !== companyId) {
                        $('.expand-'+input.attr('data-companyId')).click();
                    } else if (input.attr('data-companyId') === companyId && input.attr('name') === 'services[' + getParameterByName('serviceId') + ']') {
                        input.attr('checked', true);
                        input.change(); //triggers change so the form gets cached again!
                    }
                }
            }
        });

        var disabledRemindersCompanyId = getParameterByName('disabledRemindersCompanyId');
        if (disabledRemindersCompanyId) {
            $('html, body').animate({
                scrollTop: $('.reminders-disabled-flash').offset().top - 10
            }, 50);

            var table = $('.company-block[data-company-id="' + disabledRemindersCompanyId + '"]');

            $('tbody', table).removeClass('hidden');
            $('thead tr', table).removeClass('hidden');
            $('thead tr.company-header-collapsed', table).addClass('hidden');
            $('.toggle-company-hide', table).addClass('hidden');
            $('.toggle-company-show', table).removeClass('hidden');
        }
    };

    RenewalForm.prototype.getPopoverTogglerForService = function(serviceId) {
        return this.getPopoverTogglers().filter('[data-service-id=' + serviceId + ']');
    };

    RenewalForm.prototype.closePopover = function(serviceId) {
        this.getPopoverTogglerForService(serviceId).popover('hide');
    };

    CMS.RenewalForm = RenewalForm;

}(window.CMS = window.CMS || {}, $));
