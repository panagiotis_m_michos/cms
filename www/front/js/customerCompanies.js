window.cms = window.cms || {};

(function(customerCompanies, validations, $) {

    customerCompanies.init = function(perPage, companiesCount, url, companyPageUrl, myServicesPageUrl) {
        var options = {
            term : '#term',
            onlyActive : '#onlyActive',
            dateFilterType : '#dateFilterType',
            dateFrom : '#dateFrom',
            dateTo : '#dateTo',
            currentFilter : '#currentFilter',
            filterText : '#filterText',
            syncAllLink : '#syncAllLink'
        }

        var getTextualFilter = function(recordsFiltered) {
            var filter = '';
            var termFilter = '';
            var dateFilter = '';

            if ($(options.term).val() != '') {
                termFilter = '<strong>"' + $(this.term).val() + '"</strong>';
            }

            if (
                $(options.dateFilterType).val() != '' &&
                ($(options.dateFrom).val() != '' || $(options.dateTo).val() != '')
            ) {
                dateFilter = '<strong>' + $(options.dateFilterType + ' option[value='+$(options.dateFilterType).val()+']').text() + " ";
                if ($(options.dateFrom).val() != '') {
                    dateFilter += "from " + $(options.dateFrom).val() + " ";
                }
                if ($(options.dateTo).val() != '') {
                    dateFilter += "to " + $(options.dateTo).val();
                }
                dateFilter += "</strong>"
            }

            onlyActive = $(options.onlyActive).is(':checked') ? " <strong>active</strong>" : "";
            numberCo = recordsFiltered == 1 ? " company" : " companies";
            if (termFilter != '' || dateFilter != '') {
                filter = recordsFiltered + onlyActive + numberCo + " found for: " + termFilter;
                if (dateFilter != '') {
                    filter += " | " + dateFilter;
                }
            }

            return filter;
        }

        var enableSyncAllLink = function() {
            $(options.syncAllLink).css('color', '#000');
            $(options.syncAllLink).css('cursor','pointer');
            $(options.syncAllLink).removeAttr('title');
            $(options.syncAllLink).unbind('click');
            $(options.syncAllLink).click(function() {
                var answer = confirm('If you have a lot of companies this may take a while. NB. This will update due date dates but not company officers.');
                if(answer){
                    $(this).hide();
                }
                return answer;
            });
        }

        var disableSyncAllLink = function() {
            var message = 'To synchronize your companies with Companies House please clear all search filters.';
            $(options.syncAllLink).unbind('click');
            $(options.syncAllLink).prop('title', message);
            $(options.syncAllLink).css('color', '#999');
            $(options.syncAllLink).css('cursor','default');
            $(options.syncAllLink).click(function(e){
                e.preventDefault();
            });
        }

        var toggleExportButtonVisibility = function(recordsFiltered) {
            if (recordsFiltered == 0) {
                $("#exportCsv").hide();
            } else {
                $("#exportCsv").show();
            }
        }

        var toggleFilterVisibility = function(recordsFiltered) {
            var textualFilter = getTextualFilter(recordsFiltered);
            if (textualFilter == '') {
                $(options.currentFilter).hide();
                enableSyncAllLink();
            } else {
                $(options.filterText).html(textualFilter);
                $(options.currentFilter).show();
                disableSyncAllLink();
            }
            toggleExportButtonVisibility(recordsFiltered);
            $("#not-incorporated-companies a").css('text-decoration', 'none');
            $("#incorporated-companies a").css('text-decoration', 'none');
        }

        var decorateDate = function(date) {
            var dateArr = date.split("/");
            var dateObj = moment(new Date(dateArr[2], dateArr[1] - 1, dateArr[0]));
            var now = moment(new Date());
            var diff = dateObj.diff(now, 'months');

            if (diff < 0) {
                return '<span style="color: red">' + date + '</span>';
            } else if (diff > 0 && diff <= 2) {
                return '<span style="color: orange">' + date + '</span>';
            } else if (diff == 0){
                diff = dateObj.diff(now, 'days');
                if (diff < 0) {
                    return '<span style="color: red">' + date + '</span>';
                } else {
                    return '<span style="color: orange">' + date + '</span>';
                }
            }else {
                return date;
            }
        }

        var dataTable = $('#incorporated-companies').dataTable({
            "order": [[0, "asc"]],
            "columns": [
                { "width": "35%" },
                { "width": "10%" },
                { "width": "10%" },
                { "width": "10%" },
                { "width": "10%" },
                { "width": "10%" },
                { "width": "15%" }
            ],
            "lengthChange": false,
            "pageLength": perPage,
            "deferLoading": companiesCount,
            "serverSide": true,
            "processing": true,
            "searching": false,
            "language": {
                "zeroRecords": "No companies found."
            },
            "ajax": {
                "url": url,
                "data": function(data) {
                    $.extend(data, $('#customerCompaniesFilterForm').formData());
                },
                "error": function(error) {
                    alert('Error occurred while searching');
                },
                timeout: 30000
            },
            "columnDefs": [ {
                    "targets": [0,1],
                    "render": function (data, type, full, meta) {
                        if (!data) {
                            return "";
                        } else if (($(full[6]).is('i') || $(full[6]).is('span')) && $(data).is('a')) {
                            // this if handles the first time the page is rendered!
                            return data;
                        }

                        var companyLink = '<a href="' + companyPageUrl + '?company_id=' + full[6]+ '">' + data + '</a>';
                        var term = $(options.term).val() ? $(options.term).val().toLowerCase() : '';
                        var termPos = data.toLowerCase().indexOf(term);
                        var posAfterTerm = parseInt(termPos) + parseInt(term.length);
                        return term != '' && type === 'display' && (termPos) > -1 ?
                            '<a href="' + companyPageUrl + '?company_id=' + full[6]+ '">' + data.substr( 0, termPos ) + '<span style="font-weight: bold">' + data.substr( termPos, term.length ) + '</span>' + data.substr(posAfterTerm, data.length) + '</a>' :
                            '<a href="' + companyPageUrl + '?company_id=' + full[6]+ '">' + data + '</a>';
                    }
                },
                {
                    "targets" : 3,
                    "render": function ( data ) {
                        if (data) {
                            return (data.toLowerCase() != 'active') ? '<span style="color: grey">' + data + '</span>' : data;
                        } else {
                            return "";
                        }
                    }
                },
                {
                    "targets" : [4, 5],
                    "render": function ( data, type, full ) {
                        var status = full[3];
                        var text = "";
                        if (data) {
                            text = data;
                        }
                        if (status && data) {
                            return (status.toLowerCase() != 'active') ? '<span style="color: grey">' + text + '</span>' : decorateDate(text)
                        } else {
                            return '<span style="color: grey">' + text + '</span>'
                        }
                    }
                },
                {
                    "targets" : [6],
                    "render": function ( data, type, full ) {
                        if ($(data).is('i') || $(data).is('span')) {
                            return data;
                        } else if (!full[7]) {
                            return "---";
                        } else {
                            action_link = full[8] ? '<i class="fa fa-exclamation-circle midfont red"></i>' : '<i class="fa fa-check-circle midfont green1"></i>';
                            action_link += '&nbsp;<span><a href="' + myServicesPageUrl + '?companyId=' + full[6] + '#company=' + full[6] + '">' + full[9] + '</a></span>';
                            return action_link;
                        }
                    }
                } ]
        }).api();

        var timeoutId;
        $(options.term).keyup(function(){
            clearTimeout(timeoutId);
            timeoutId = setTimeout(function(){
                $("#search").click();
            }, 750)
        });

        $("#search").click(function() {
            $(this).addClass("clicked");
        });

        $("#exportCsv").click(function() {
            $("#search").removeClass("clicked");
        });

        $('#customerCompaniesFilterForm').on('submit', function (e) {
            if( $(this).find(".clicked").attr("id") === "search" ) {
                e.preventDefault();
                dataTable.ajax.reload( function (json) {
                    toggleFilterVisibility(json.recordsFiltered);
                });
            }
        });

        $("#advbtn").click(function(){
            $("#advinc").toggle();
        });

        $( "#dateFrom, #dateTo" ).datepicker({ dateFormat: 'dd/mm/yy' });
        $("#not-incorporated-companies a").css('text-decoration', 'none');
        $("#incorporated-companies a").css('text-decoration', 'none');

        enableSyncAllLink();
        toggleExportButtonVisibility(companiesCount);

        validations.myCompaniesFilter('#customerCompaniesFilterForm' , '#dateFIlterType', '#dateFrom', '#dateTo');
    }

}(window.cms.customerCompanies = window.cms.customerCompanies || {}, window.cms.validations, $));
