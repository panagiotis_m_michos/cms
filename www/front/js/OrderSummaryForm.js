window.cms = window.cms || {};

(function(summaryForm, $) {
    summaryForm.selectors = {
        orderSummaryContainer: '#order-summary-page',
        importButtons: '.import-company',
        importTexts: '.company-number-text',
        editImportButtons: '.edit-import',
        radioButtons: '.company-radio',
        checkedRadioButtons: '.company-radio:checked',
        companySelects: '.company-number-select',
        companySelectStatus: '.company-number-select-status'
    };

    summaryForm.disableImportButton = function($importButton) {
        $importButton.data('default-text', $importButton.val());
        $importButton.val($importButton.data('action-text'));
        $importButton.attr('disabled', 'disabled');
    };

    summaryForm.enableImportButton = function($importButton) {
        var defaultText = $importButton.data('default-text');
        if (typeof defaultText != 'undefined') {
            $importButton.val(defaultText);
        }
        $importButton.removeAttr('disabled');

        // ladda button
        if ($importButton.hasClass('ladda-button')) {
            $importButton.ladda().ladda('stop');
        }
    };

    summaryForm.disableSelectInput = function($selectInput, $status) {
        $selectInput.attr('disabled', 'disabled');
        $status.addClass('loading');
    };

    summaryForm.enableSelectInput = function($selectInput, $status) {
        $selectInput.removeAttr('disabled');
        $status.removeClass('loading');
    };

    summaryForm.disableCheckoutButtons = function() {
        $('.btn_submit').attr('disabled', 'disabled');
    };

    summaryForm.enableCheckoutButtons = function() {
        $('.btn_submit').removeAttr('disabled');
    };

    summaryForm.redrawForm = function($form, $importButton) {
        var formAction = $form.attr('action');
        $.ajax({
            type: "GET",
            url: formAction,
            dataType: "json",
            success: function (formData) {
                $('#order-summary-page').html(formData.template);
                $(summaryForm.selectors.checkedRadioButtons).trigger('change');
                $('.summary-imported .company-import-text').hide();
                $('.summary-imported .company-import-edit').show();
            },
            complete: function() {
                summaryForm.enableCheckoutButtons();
                if ($importButton) {
                    summaryForm.enableImportButton($importButton);
                }
                $(summaryForm.selectors.importButtons).ladda('bind');
            }
        });
    };

    summaryForm.selectCompany = function($select) {
        var $form = $select.closest('form');
        var $box = $select.closest('.summary-service');
        var $status = $(summaryForm.selectors.companySelectStatus, $box);

        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: $form.serialize() + '&companySelectChanged=1',
            dataType: "json",
            beforeSend: function() {
                summaryForm.disableCheckoutButtons();
                summaryForm.disableSelectInput($select, $status);
            },
            success: function() {
                summaryForm.redrawForm($form);
            },
            error: function() {
                summaryForm.enableSelectInput($select, $status);
                alert('Oops...an error occurred (form error)');
            },
            timeout: function() {
                summaryForm.enableSelectInput($select, $status);
                alert('Oops...an error occurred (form timeout)');
            }
        });
        return false;
    };

    summaryForm.importCompany = function($importButton) {
        var $box = $importButton.closest('.summary-service');
        var $form = $importButton.closest('form');

        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: $form.serialize() + '&' + $importButton.attr('name') + '=' + $importButton.attr('value'),
            dataType: "json",
            beforeSend: function() {
                summaryForm.disableCheckoutButtons();
                $importButton.attr('disabled', 'disabled');
                $('.company-import-error', $box).text('').hide();
            },
            success: function() {
                summaryForm.redrawForm($form, $importButton);
            },
            error: function() {
                summaryForm.enableImportButton($importButton);
                alert('Oops...an error occurred (form error)');
            },
            timeout: function() {
                summaryForm.enableImportButton($importButton);
                alert('Oops...an error occurred (form timeout)');
            }
        });
        return false;
    };

    summaryForm.editImportCompany = function($editButton) {
        var $form = $editButton.closest('form');

        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: $form.serialize() + '&' + $editButton.attr('name') + '=' + $editButton.attr('value'),
            dataType: "json",
            beforeSend: function() {
                summaryForm.disableCheckoutButtons();
                summaryForm.disableImportButton($editButton);
            },
            success: function() {
                summaryForm.redrawForm($form);
            },
            error: function() {
                summaryForm.enableImportButton($editButton);
                alert('Oops...an error occurred (form error)');
            },
            timeout: function() {
                summaryForm.enableImportButton($editButton);
                alert('Oops...an error occurred (form timeout)');
            }
        });
        return false;
    };

    summaryForm.init = function() {
        var self = this;

        $(this.selectors.orderSummaryContainer).on('click', this.selectors.importButtons, function() {
            self.importCompany($(this));
            return false;
        });

        $(this.selectors.orderSummaryContainer).on('keypress', this.selectors.importTexts, function(e) {
            if (e.which == 13) {
                $(summaryForm.selectors.importButtons, $(this).closest('.summary-service')).trigger('click');
                return false;
            }
        });

        $(this.selectors.orderSummaryContainer).on('click', this.selectors.editImportButtons, function() {
            self.editImportCompany($(this));
            return false;
        });

        $(this.selectors.orderSummaryContainer).on('change', this.selectors.radioButtons, function() {
            var $this = $(this);
            var val = $this.val();
            var $box = $this.closest('.summary-service');

            if (val == 'import') {
                $('.company-import-container', $box).show();
                $('.company-existing-container', $box).hide();
            } else {
                $('.company-import-container', $box).hide();
                $('.company-existing-container', $box).show();
            }
        });

        $(this.selectors.orderSummaryContainer).on('change', this.selectors.companySelects, function() {
           self.selectCompany($(this));
        });

        $(this.selectors.checkedRadioButtons).trigger('change');
        $('.summary-imported .company-import-text').hide();
        $('.summary-imported .company-import-edit').show();
    }
}(window.cms.summaryForm = window.cms.summaryForm || {}, $));
