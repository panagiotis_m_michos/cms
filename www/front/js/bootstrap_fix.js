//jQuery UI and Bootstrap have namespacing collision when it comes to buttons
var btn = $.fn.button.noConflict(); // reverts $.fn.button to jqueryui btn
$.fn.btn = btn; // assigns bootstrap button functionality to $.fn.btn
