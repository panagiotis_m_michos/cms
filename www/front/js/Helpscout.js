(function(CMS, $) {

    var Helpscout = function() {

    };

    Helpscout.prototype.init = function (searchLabel) {
        !function(e,o,n){window.HSCW=o,window.HS=n,n.beacon=n.beacon||{};var t=n.beacon;t.userConfig={},t.readyQueue=[],t.config=function(e){this.userConfig=e},t.ready=function(e){this.readyQueue.push(e)},o.config={docs:{enabled:!0,baseUrl:"https://companyformationmadesimple.helpscoutdocs.com/"},contact:{enabled:!0,formId:"a340a0f6-635a-11e5-8846-0e599dc12a51"}};var r=e.getElementsByTagName("script")[0],c=e.createElement("script");c.type="text/javascript",c.async=!0,c.src="https://djtflbt20bdde.cloudfront.net/",r.parentNode.insertBefore(c,r)}(document,window.HSCW||{},window.HS||{});

        this.beacon = window.HS.beacon;

        var self = this;

        self.beacon.config({
            modal: true,
            topArticles: false,
            translation: {
                searchLabel: searchLabel
            },
            attachment: true,
            poweredBy: false
        });

        $(document).on('click', 'a[href="#open-zopim"]', function(e) {
            e.preventDefault();

            if (window.$zopim != undefined) {
                window.$zopim.livechat.window.show();
            } else {
                self.beacon.open();
            }
        }).on('click', 'a[href="#open-beacon"]', function(e) {
            e.preventDefault();
            self.beacon.open();
        });
    };

    Helpscout.prototype.identify = function(name, email, id) {
        var self = this;

        self.beacon.ready(
            function() {
                self.beacon.identify({
                    name: name,
                    email: email,
                    id: id
                });
            }

        );
    };

    CMS.Helpscout = Helpscout;

})(window.CMS = window.CMS || {}, $);
