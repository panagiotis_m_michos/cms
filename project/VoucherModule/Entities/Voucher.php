<?php

namespace VoucherModule\Entities;

use DateTime;
use Doctrine\ORM\Mapping as Orm;
use Entities\Company;
use Entities\EntityAbstract;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @Orm\Entity(repositoryClass="VoucherModule\Repositories\VoucherRepository")
 * @Orm\Table(name="cms2_vouchers")
 */
class Voucher extends EntityAbstract
{
    const TYPE_PERCENT = 'PERCENT';
    const TYPE_AMOUNT = 'AMOUNT';

    /**
     * @var int
     *
     * @Orm\Column(name="voucherId", type="integer")
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Orm\Column(type="string")
     */
    private $name;

    /**
     * @var string
     *
     * @Orm\Column(type="string")
     */
    private $voucherCode;

    /**
     * @var int
     *
     * @Orm\Column(type="integer")
     */
    private $typeId;

    /**
     * @var int
     *
     * @Orm\Column(type="integer")
     */
    private $typeValue;

    /**
     * @var int
     *
     * @Orm\Column(type="integer", nullable=true)
     */
    private $minSpend;

    /**
     * @var int
     *
     * @Orm\Column(type="integer", nullable=true)
     */
    private $usageLimit;

    /**
     * @var int
     *
     * @Orm\Column(type="integer")
     */
    private $used;

    /**
     * @var DateTime
     *
     * @Orm\Column(type="datetime")
     */
    private $dtExpiry;

    /**
     * @var DateTime
     *
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;

    /**
     * @var DateTime
     *
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create", on="update")
     */
    private $dtm;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getVoucherCode()
    {
        return $this->voucherCode;
    }

    /**
     * @param string $voucherCode
     */
    public function setVoucherCode($voucherCode)
    {
        $this->voucherCode = $voucherCode;
    }

    /**
     * @return int
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     * @param int $typeId
     */
    public function setTypeId($typeId)
    {
        $this->typeId = $typeId;
    }

    /**
     * @return int
     */
    public function getTypeValue()
    {
        return $this->typeValue;
    }

    /**
     * @param int $typeValue
     */
    public function setTypeValue($typeValue)
    {
        $this->typeValue = $typeValue;
    }

    /**
     * @return int
     */
    public function getMinSpend()
    {
        return $this->minSpend;
    }

    /**
     * @param int $minSpend
     */
    public function setMinSpend($minSpend)
    {
        $this->minSpend = $minSpend;
    }

    /**
     * @return int
     */
    public function getUsageLimit()
    {
        return $this->usageLimit;
    }

    /**
     * @param int $usageLimit
     */
    public function setUsageLimit($usageLimit)
    {
        $this->usageLimit = $usageLimit;
    }

    /**
     * @return int
     */
    public function getUsed()
    {
        return $this->used;
    }

    /**
     * @param int $used
     */
    public function setUsed($used)
    {
        $this->used = $used;
    }

    /**
     * @return DateTime
     */
    public function getDtExpiry()
    {
        return $this->dtExpiry;
    }

    /**
     * @param DateTime $dtExpiry
     */
    public function setDtExpiry(DateTime $dtExpiry = NULL)
    {
        $this->dtExpiry = $dtExpiry;
    }

    /**
     * @param DateTime $dtc
     */
    public function setDtc(DateTime $dtc)
    {
        $this->dtc = $dtc;
    }

    /**
     * @return DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }
    /**
     * @param DateTime $dtm
     */
    public function setDtm(DateTime $dtm)
    {
        $this->dtm = $dtm;
    }

    /**
     * @return DateTime
     */
    public function getDtm()
    {
        return $this->dtm;
    }

    /**
     * @param DateTime $date
     * @return bool
     */
    public function canBeUsedOn(DateTime $date)
    {
        if ($this->getDtExpiry() && $this->getDtExpiry() < $date) {
            return FALSE;
        }

        if ($this->getUsageLimit() && ($this->getUsed() >= $this->getUsageLimit())) {
            return FALSE;
        }

        return TRUE;
    }
}
