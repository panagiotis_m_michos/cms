<?php

namespace VoucherModule\Validators;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class VoucherEnabled extends Constraint
{
    /**
     * @var string
     */
    public $message = 'Voucher is no longer valid';

    /**
     * @return string
     */
    public function validatedBy()
    {
        return 'voucher_module.validators.voucher_enabled';
    }
}
