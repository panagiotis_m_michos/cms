<?php

namespace VoucherModule\Validators;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class VoucherExists extends Constraint
{
    /**
     * @var string
     */
    public $message = 'Voucher does not exist';

    /**
     * @return string
     */
    public function validatedBy()
    {
        return 'voucher_module.validators.voucher_exists';
    }
}
