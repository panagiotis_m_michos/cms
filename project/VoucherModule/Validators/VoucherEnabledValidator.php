<?php

namespace VoucherModule\Validators;

use DateTime;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use VoucherModule\Services\VoucherService;

class VoucherEnabledValidator extends ConstraintValidator
{
    /**
     * @var VoucherService
     */
    private $voucherService;

    /**
     * @param VoucherService $voucherService
     */
    public function __construct(VoucherService $voucherService)
    {
        $this->voucherService = $voucherService;
    }

    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        $voucher = $this->voucherService->getVoucherByCode($value);
        if ($voucher && !$voucher->canBeUsedOn(new DateTime)) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}
