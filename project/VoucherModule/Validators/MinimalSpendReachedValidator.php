<?php

namespace VoucherModule\Validators;

use Basket;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use VoucherModule\Services\VoucherService;

class MinimalSpendReachedValidator extends ConstraintValidator
{
    /**
     * @var VoucherService
     */
    private $voucherService;

    /**
     * @var Basket
     */
    private $basket;

    /**
     * @param VoucherService $voucherService
     * @param Basket $basket
     */
    public function __construct(VoucherService $voucherService, Basket $basket)
    {
        $this->voucherService = $voucherService;
        $this->basket = $basket;
    }

    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        $voucher = $this->voucherService->getVoucherByCode($value);
        if ($voucher && $voucher->getMinSpend() > $this->basket->getPrice('subTotal')) {
            $this->context->buildViolation(sprintf($constraint->message, $voucher->getMinSpend()))->addViolation();
        }
    }
}
