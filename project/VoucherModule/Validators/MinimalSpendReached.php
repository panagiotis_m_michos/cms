<?php

namespace VoucherModule\Validators;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class MinimalSpendReached extends Constraint
{
    /**
     * @var string
     */
    public $message = 'In order to apply this voucher the minimum amount spend must be over £%.2f (before VAT)';

    /**
     * @return string
     */
    public function validatedBy()
    {
        return 'voucher_module.validators.minimal_spend_reached';
    }
}
