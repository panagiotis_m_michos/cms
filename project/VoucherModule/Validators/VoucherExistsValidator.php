<?php

namespace VoucherModule\Validators;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use VoucherModule\Services\VoucherService;

class VoucherExistsValidator extends ConstraintValidator
{
    /**
     * @var VoucherService
     */
    private $voucherService;

    /**
     * @param VoucherService $voucherService
     */
    public function __construct(VoucherService $voucherService)
    {
        $this->voucherService = $voucherService;
    }

    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        $voucher = $this->voucherService->getVoucherByCode($value);
        if (!$voucher) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}
