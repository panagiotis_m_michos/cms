<?php

namespace VoucherModule\Services;

use DateTime;
use VoucherModule\Entities\Voucher;
use VoucherModule\Repositories\VoucherRepository;

class VoucherService
{
    /**
     * @var VoucherRepository
     */
    private $repository;

    /**
     * @param VoucherRepository $repository
     */
    public function __construct(VoucherRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param int $id
     * @return Voucher|null
     */
    public function getVoucherById($id)
    {
        return $this->repository->find($id);
    }

    /**
     * @param string $code
     * @return Voucher|null
     */
    public function getVoucherByCode($code)
    {
        return $this->repository->findOneBy(['voucherCode' => $code]);
    }
}
