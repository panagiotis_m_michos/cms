<?php

use Nette\Object;

class DiLocator extends Object
{
    const DISPATCHER = 'symfony.event_dispatcher';
    const ENTITY_MANAGER = 'doctrine.orm.entity_manager';
    const DOCTRINE_ANNOTATION_READER = 'doctrine.common.annotations.annotation_reader';
    const CSV_PARSER_COLUMN_MAPPER = 'csv_parser.mapper.column_mapper';
    const CH_XML_GATEWAY = 'ch_xml_gateway';
    const CH_DATA = 'companies_house.data';
    const SESSION = 'session';
    const BLOG_READER = 'blog_feed_reader';
    const EXPORT_ORDER_CSV_STREAM ='export.order.csv_stream';
    const UTILS_TEXT_PARSER = 'utils.text.parser.parser';

    const SERVICE_SERVICE = 'services.service_service';
    const SERVICE_CUSTOMER = 'services.customer_service';
    const SERVICE_COMPANY = 'services.company_service';
    const SERVICE_COMPANY_DOCUMENT = 'services.company_document_service';
    const SERVICE_USER = 'services.user_service';
    const SERVICE_ORDER = 'services.order_service';
    const SERVICE_PRODUCT = 'services.product_service';
    const SERVICE_CUSTOMER_NOTE = 'services.customer_note_service';
    const SERVICE_SUBMISSION = 'services.submission_service';
    const SERVICE_TAX_ASSIST_LEAD = 'services.tax_assist_lead_service';
    const SERVICE_ANNUAL_RETURN = 'services.annual_return_service';
    const SERVICE_REGISTER_MEMBER = 'services.register.member_service';
    const SERVICE_REGISTER_SHARE_CLASS = 'services.register.share_class_service';
    const SERVICE_REGISTER_SHARE_CLASS_EVENT = 'services.register.share_class_event_service';
    const SERVICE_ANSWER = 'services.answer_service';
    const SERVICE_SAGE = 'services.payment.sage_service';
    const SERVICE_TOKEN = 'services.payment.token_service';
    const SERVICE_CASHBACK = 'services.cashback_service';
    const SERVICE_EVENT = 'services.event_service';
    const SERVICE_TRANSACTION = 'services.transaction_service';
    const SERVICE_UI = 'services.ui';
    const SERVICE_FEEFO = 'services.feefo_service';
    const SERVICE_NODE = 'services.node_service';
    const SERVICE_ORDER_ITEM = 'services.order_item_service';
    const SERVICE_PACKAGE = 'services.package_service';
    const SERVICE_COMPANY_CUSTOMER = 'company_customer_service';
    const SERVICE_COMPANY_SEARCH = 'services.search.company';
    const SERVICE_ID_CHECK = 'id_check_module.services.id_check_service';
    const SERVICE_CUSTOMER_FILE = 'services.customer_file_storage';
    const SERVICE_EMAIL_LOGS = 'loggable_module.services.email_logs_service';
    const SERVICE_COMPANY_TRANSFERRER = 'services.company.company_transferrer';

    const SESSION_REENABLE_AUTO_RENEWAL = 'session.reenable_auto_renewal';
    const MODEL_TAX_ASSIST_LEAD = 'tax_assist_lead_model';
    const MODEL_PAYMENT_AUTO_RENEWAL_REENABLER = 'payment.auto_renewal_reenabler';

    const EMAILER_FACTORY = 'emailer_factory';
    const EMAILER_TAX_ASSIST_ACCOUNTS = 'tax_assist_accounts_emailer';
    const EMAILER_JOURNEY = 'journey_emailer';
    const EMAILER_CONTACT_US = 'contact_us_emailer';
    const EMAILER_CASHBACK = 'emailer.cashback';
    const EMAILER_WHOLESALE_PROFESSIONAL = 'emailer.wholesale.professional';
    const EMAILER_WHOLESALE_ICAEW = 'emailer.wholesale.icaew';
    const EMAILER_WHOLESALE_TAX_ASSIST = 'emailer.wholesale.tax_assists';
    const EMAILER_PAYPAL = 'payment.paypal_emailer';
    const EMAILER_PAYMENT = 'payment_emailer';
    const EMAILER_FORGOTTEN_PASSWORD = 'forgotten_password_emailer';

    const FACADE_CLONE_COMPANY = 'services.facades.company_clone_facade';
    const FACADE_WHOLESALE_SEARCH = 'facade.wholesale.search';
    const FACADE_ORDER_REFUND = 'services.facades.order_refund_facade';

    const LISTENER_INCORPORATION = 'dispatcher.listeners.incorporation_listener';
    const LISTENER_SERVICE = 'dispatcher.listeners.service_listener';
    const LISTENER_CREATE_SERVICE_SETTINGS = 'dispatcher.listeners.create_service_settings_listener';
    const LISTENER_SUSPEND_AUTO_RENEWAL = 'dispatcher.listeners.suspend_auto_renewal_listener';
    const LISTENER_SERVICES_EMAIL = 'dispatcher.listeners.services_email_listener';
    const LISTENER_DELETE_UNFORMED_COMPANY = 'dispatcher.listeners.delete_unformed_company_listener';
    const LISTENER_TOKEN_EMAIL = 'dispatcher.listeners.token_email_listener';
    const LISTENER_TOKEN = 'dispatcher.listeners.token_listener';
    const LISTENER_ORDER_EMAIL = 'dispatcher.listeners.order_email';
    const LISTENER_NOMINEE_DIRECTOR = 'dispatcher.listeners.nominee_director_listener';
    const LISTENER_NOTIFY_PREMIER_BUSINESS = 'dispatcher.listeners.notify_premier_business';

    const LISTENER_COMPANY_HOUSE_SUBMISSION_STATUS = 'dispatcher.listeners.company_house.submission_status_listener';
    const LISTENER_COMPANY_HOUSE_INCORPORATION = 'dispatcher.listeners.company_house.incorporation_listener';
    const LISTENER_COMPANY_HOUSE_CHANGE_OF_NAME = 'dispatcher.listeners.company_house.change_of_name_listener';
    const LISTENER_CASHBACK = 'dispatcher.listeners.cashback';
    const LISTENER_CUSTOMER = 'dispatcher.listeners.customer';
    const LISTENER_SIGN_UP = 'listeners.signUp';
    const LISTENER_ORDER = 'dispatcher.listeners.order_listener';
    const LISTENER_PAYMENT = 'dispatcher.listeners.payment_listener';
    const LISTENER_FEEFO_UPDATE_STATUS = 'dispatcher.listeners.feefo.update_status_listener';
    const LISTENER_BASKET = 'dispatcher.listeners.basket_listener';
    const LISTENER_TAX_ASSIST = 'offer_module.tax_assist_listener';
    const LISTENER_OFFER_EMAIL = 'offer_module.email_listener';
    const LISTENER_ID_CHECK = 'id_check_module.listeners.id_check';

    const FACTORY_FRONT_COMPANY_VIEW = 'factories.front.company_view_factory';
    const FACTORY_FRONT_SERVICE_VIEW = 'factories.front.service_view_factory';
    const FACTORY_BASKET_NOTIFICATION = 'factories.front.basket_notification_factory';
    const FACTORY_COMPANY_HOUSE_RESPONSE = 'company_house.filling.responses.response_factory';
    const FACTORY_SERVICE_ADDRESS_PARSER_ITEM = 'import.products.service_address.parser.item_factory';
    const FACTORY_REGISTERED_OFFICE_PARSER_ITEM = 'import.products.registered_office.parser.item_factory';
    const FACTORY_PRIVACY_PACKAGE_PARSER_ITEM = 'import.products.privacy_package.parser.item_factory';
    const FACTORY_SAGE_REPORTING_API = 'sagepay.reporting.sagepay_factory';
    const SAGE_REPORTING_API = 'sagepay.reporting.sagepay';

    const CRON = 'cron_di';
    const NOTIFIER_CRON = 'notifier.plugins.cron_notifier';

    const NIKOLAI_LOGGER = 'loggers.nikolai_logger';
    const NIKOLAI_LOGGER_PSR = 'loggers.nikolai_logger_psr';

    // commands
    const COMMAND_IMPORT_SERVICE_ADDRESS = 'console.commands.import.service_address.products_command';
    const COMMAND_IMPORT_REGISTERED_OFFICE = 'console.commands.import.registered_office.products_command';
    const COMMAND_IMPORT_PRIVACY_PACKAGE = 'console.commands.import.privacy_package.products_command';
    const COMMAND_IMPORT_COMPREHENSIVE_ULTIMATE_PACKAGE = 'console.commands.import.comprehensive_ultimate_package.products_command';
    const COMMAND_PACKAGES_ACTIVATE = 'console.commands.packages.activate_command';
    const COMMAND_SAGE_TOKENS_SYNC = 'console.commands.sage.tokens.sync_command';
    const COMMAND_ORDER_ITEMS_FILL_MISSING_COMPANY_REFERENCES = 'console.commands.order_items.fill_company_references_command';
    const COMMAND_SERVICES_REMOVE_ORPHANS = 'console.commands.services.remove_orphans_command';
    const COMMAND_SERVICES_REMOVE_REFUNDED = 'console.commands.services.remove_refunded_command';
    const COMMAND_SERVICES_SHORTEN_BY_ONE_DAY = 'console.commands.services.shorten_by_one_day_command';
    const COMMAND_SERVICES_SET_UPGRADE_DOWNGRADE = 'console.commands.services.set_upgrade_downgrade_command';
    const COMMAND_SERVICES_RENEW_TODDINGTON_SERVICES = 'console.commands.services.renew_toddington_services_command';
    const COMMAND_SERVICES_CREATE_INTERNATIONAL_SERVICES = 'console.commands.services.create_international_services_command';
    const COMMAND_ORDER_ITEMS_MARK_AS_EXPORTED = 'commands.nikolai.order_items.mark_items_as_exported_command';
    const COMMAND_VO_SERVICE_UPDATE_PRODUCT_NAME = 'console.commands.vo_service.update_product_command';
    const COMMAND_OUTDATE_OLD_CASHBACKS = 'console.commands.cashbacks.outdate_old_cashbacks_command';
    const COMMAND_CLEAR_ALL_CACHES = 'cache_module.commands.clear_all_caches';
    const COMMAND_ASSIGN_ORDER_ITEMS = 'console.commands.services.assign_order_items_command';

    const PAYPAL_LOGGER = 'loggers.paypal_logger';
    const NIKOLAI_MISSING_PACKAGE_SERVICES = 'console.commands.services.update_missing_children_command';
    const NIKOLAI_RESEND_FAKED_SUBMISSIONS = 'console.commands.submissions.resend_faked_command';
    const NIKOLAI_UPDATE_REFUNDED_ORDER_ITEMS = 'console.commands.order_items.update_refunded_order_items_command';
    const NIKOLAI_IMPORT_MISSING_ORDER_ITEMS = 'console.commands.order_items.diff_lpl_command';
    const NIKOLAI_UPDATE_SERVICES_DATES_FROM_LPL = 'console.commands.services.update_services_from_lpl_command';

    const CACHE_FS = 'cache.filesystem';
    const CACHE_MEMORY = 'cache.memory';
    const CACHE_NETTE_FILESYSTEM = 'cache.nette.filesystem';
    const CACHE_CRON = 'cron.cache';
}
