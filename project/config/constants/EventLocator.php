<?php

use Nette\Object;

class EventLocator extends Object
{
    //const AR_FILED = 'events.ar.filed';
    //const AR_REMINDER_SENT = 'events.ar.reminder_sent';
    //const AR_MISSING_DEPENDENCIES = 'events.ar.missing_dependencies';
    //const AR_CANCELLED = 'events.ar.cancelled';
    //const AR_SUBMITTED_TO_CUSTOMER = 'events.ar.submited_to_customer';
    //const AR_REJECTED_BY_CUSTOMER = 'events.ar.rejected_by_customer';
    //const AR_SUBMITTED_TO_COMPANIES_HOUSE = 'events.ar.submited_to_companies_house';
    //const AR_REJECTED_BY_COMPANIES_HOUSE = 'events.ar.rejected_by_companies_house';
    //const AR_ERROR_BY_COMPANIES_HOUSE = 'events.ar.error_by_companies_house';
    //const AR_PENDING_BY_COMPANIES_HOUSE = 'events.ar.pending_by_companies_house';

    const CUSTOMER_UPDATED = 'events.customer.updated';
    
    const COMPANY_INCORPORATED = 'events.company.incorporated';
    const COMPANY_INCORPORATION_PENDING = 'events.company.incorporation.pending';
    const COMPANY_INCORPORATION_ERROR = 'events.company.incorporation.error';
    const COMPANY_TRANSFERRED = 'events.company.transferred';
    const COMPANY_DELETED = 'events.company.deleted';

    const PAYMENT_SUCCEEDED = 'events.payment.succeeded';
    //event should be called when payment completes
    const PAYMENT_RECURRING_COMPLETED = 'events.recurring_payment.completed';
    const ORDER_COMPLETED = 'events.order.completed';

    const CUSTOMER_BANK_DETAILS_UPDATED = 'events.customer.bank_details.updated';

    const SERVICE_PURCHASED = 'events.sevice.purchased';
    const SERVICE_DELETED = 'events.service.deleted';

    const SERVICE_EXPIRES_IN_28 = 'events.service.expires_in_28';
    const SERVICE_EXPIRES_IN_15 = 'events.service.expires_in_15';
    const SERVICE_EXPIRES_IN_1 = 'events.service.expires_in_1';
    const SERVICE_EXPIRED = 'events.service.expired';
    const SERVICE_EXPIRED_15 = 'events.service.expired_15';
    const SERVICE_EXPIRED_28 = 'events.service.expired_28';

    const COMPANY_SERVICES_EXPIRES_IN_28 = 'events.company_services.expires_in_28';
    const COMPANY_SERVICES_EXPIRES_IN_15 = 'events.company_services.expires_in_15';
    const COMPANY_SERVICES_EXPIRES_IN_1 = 'events.company_services.expires_in_1';
    const COMPANY_SERVICES_EXPIRED = 'events.company_services.expired';
    const COMPANY_SERVICES_EXPIRED_15 = 'events.company_services.expired_15';
    const COMPANY_SERVICES_EXPIRED_28 = 'events.company_services.expired_28';

    const COMPANY_SERVICES_AUTO_RENEWAL_EXPIRES_IN_7 = 'events.company_services.auto_renewal.expires_in_7';
    const COMPANY_SERVICES_AUTO_RENEWAL_SUCCEDED = 'events.company_services.auto_renewed_succeeded';
    const COMPANY_SERVICES_AUTO_RENEWAL_FAILED = 'events.company_services.auto_renewed_failed';

    const TOKEN_EMAIL_SOON_EXPIRES = 'events.token.email.soonExpires';
    const TOKEN_EMAIL_EXPIRED = 'events.token.email.expired';
    const TOKEN_REMOVED = 'events.token.removed';
    const CUSTOMER_SIGN_UP = 'events.customer_sign_up';
    const BASKET_ITEM_REMOVED = 'events.basket.item.removed';

    const ORDER_REFUNDED = 'events.order_refunded';

    //company house events
    //@warning must be the same as formsubmission identifier lowercase
    const FORM_SUBMISSION_ACCEPT_CHANGE_OF_NAME = 'events.companyhouse.accept.changeofname';
    const FORM_SUBMISSION_ACCEPT_INCORPORATION = 'events.companyhouse.accept.companyincorporation';
    const FORM_SUBMISSION_REJECT_INCORPORATION = 'events.companyhouse.reject.companyincorporation';
    const FORM_SUBMISSION_REJECT = 'events.companyhouse.reject';
    const FORM_SUBMISSION_ACCEPT = 'events.companyhouse.accept';
    const FORM_SUBMISSION_FAILURE = 'events.companyhouse.internal_failure';

    const CASHBACK_ARCHIVED = 'events.cashback.archived';
}
