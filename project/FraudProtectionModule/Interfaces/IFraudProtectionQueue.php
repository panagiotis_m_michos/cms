<?php

namespace FraudProtectionModule\Interfaces;

use Entities\Company;

interface IFraudProtectionQueue
{
    /**
     * @param Company $company
     * @return
     */
    public function addToQueue(Company $company);
}
