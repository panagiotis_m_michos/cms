<?php

namespace FraudProtectionModule\Listeners;

use Dispatcher\Events\OrderEvent;
use EventLocator;
use FraudProtectionModule\Interfaces\IFraudProtectionQueue;
use Product;
use Services\CompanyService;

class AddCompanyToQueueListener
{
    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var IFraudProtectionQueue
     */
    private $queueService;

    /**
     * @param CompanyService $companyService
     * @param IFraudProtectionQueue $queueService
     */
    public function __construct(CompanyService $companyService, IFraudProtectionQueue $queueService)
    {
        $this->companyService = $companyService;
        $this->queueService = $queueService;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            EventLocator::ORDER_COMPLETED => 'onOrderCompleted',
        ];
    }

    /**
     * @param OrderEvent $event
     */
    public function onOrderCompleted(OrderEvent $event)
    {
        foreach ($event->getOrder()->getItems() as $orderItem) {
            if ($orderItem->getProductId() == Product::PRODUCT_FRAUD_PROTECTION) {
                $company = $orderItem->getCompany() ?: $this->companyService->getCompanyByOrder($orderItem->getOrder());
                $this->queueService->addToQueue($company);
            }
        }
    }
}
