<?php

namespace FraudProtectionModule\Entities;

use DateTime;
use Doctrine\ORM\Mapping as Orm;
use Entities\Company;
use Entities\EntityAbstract;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @Orm\Entity(repositoryClass="FraudProtectionModule\Repositories\FraudProtectionQueueRepository")
 * @Orm\Table(name="cms2_fraud_protection_queue")
 */
class FraudProtectionQueue extends EntityAbstract
{
    /**
     * @var int
     * @Orm\Column(type="integer", name="fraudProtectionQueueId")
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Company
     * @Orm\OneToOne(targetEntity="Entities\Company")
     * @Orm\JoinColumn(name="companyId", referencedColumnName="company_id")
     */
    private $company;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;

    /**
     * FraudProtectionQueue constructor.
     *
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }
}
