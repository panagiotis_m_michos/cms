<?php

namespace FraudProtectionModule\Exporters;

use FraudProtectionModule\Entities\FraudProtectionQueue;
use Goodby\CSV\Export\Standard\Exporter;
use Utils\File;

class FraudProtectionCsvExporter
{
    /**
     * @var Exporter
     */
    private $csvExporter;

    /**
     * @var string
     */
    private $exportDirectory;

    /**
     * @param Exporter $csvExporter
     * @param string $exportDirectory
     */
    public function __construct(Exporter $csvExporter, $exportDirectory)
    {
        $this->csvExporter = $csvExporter;
        $this->exportDirectory = $exportDirectory;
    }

    /**
     * @param FraudProtectionQueue[] $queueItems
     * @return File
     */
    public function exportCompanies(array $queueItems)
    {
        $rows = [];

        foreach ($queueItems as $queueItem) {
            $company = $queueItem->getCompany();
            $customer = $company->getCustomer();

            $rows[] = [
                $company->getCompanyName(),
                $customer->getFirstName(),
                $customer->getLastName(),
                $company->getCompanyNumber(),
                $customer->getEmail(),
            ];
        }

        $filename = $this->exportDirectory . DIRECTORY_SEPARATOR . uniqid('fraud_protection_export') . '.csv';
        $this->csvExporter->export($filename, $rows);

        return new File($filename);
    }
}
