<?php

namespace FraudProtectionModule\Config;

class DiLocator
{
    const LISTENER_ADD_COMPANY_TO_QUEUE = 'fraud_protection_module.listeners.add_company_to_queue_listener';
    const COMMAND_PROCESS_FRAUD_PROTECTION_COMPANIES = 'fraud_protection_module.commands.process_fraud_protection_companies_command';
}
