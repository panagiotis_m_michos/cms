<?php

namespace FraudProtectionModule\Services;

use Entities\Company;
use FraudProtectionModule\Entities\FraudProtectionQueue;
use FraudProtectionModule\Interfaces\IFraudProtectionQueue;
use FraudProtectionModule\Repositories\FraudProtectionQueueRepository;
use Services\CompanyService;

class FraudProtectionQueueService implements IFraudProtectionQueue
{
    /**
     * @var FraudProtectionQueueRepository
     */
    private $repository;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @param FraudProtectionQueueRepository $repository
     * @param CompanyService $companyService
     */
    public function __construct(FraudProtectionQueueRepository $repository, CompanyService $companyService)
    {
        $this->repository = $repository;
        $this->companyService = $companyService;
    }

    /**
     * @param Company $company
     */
    public function addToQueue(Company $company)
    {
        $queueItem = new FraudProtectionQueue($company);

        $this->repository->saveEntity($queueItem);
    }

    /**
     * @return FraudProtectionQueue[]
     */
    public function getQueueItemsToSend()
    {
        return $this->repository->getQueueItemsToSend();
    }

    /**
     * @param FraudProtectionQueue $item
     */
    public function removeFromQueue(FraudProtectionQueue $item)
    {
        $this->repository->removeEntity($item);
    }
}
