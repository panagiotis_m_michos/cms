<?php

namespace FraudProtectionModule\Commands;

use Cron\Commands\CommandAbstract;
use Cron\Commands\ICommand;
use Cron\INotifier;
use DateTime;
use FraudProtectionModule\Exporters\FraudProtectionCsvExporter;
use FraudProtectionModule\Services\FraudProtectionQueueService;
use FraudProtectionModule\Uploaders\FraudProtectionFtpUploader;
use Psr\Log\LoggerInterface;

class ProcessFraudProtectionCompaniesCommand extends CommandAbstract implements ICommand
{
    /**
     * @var string
     */
    protected $timeType = self::TIME_TYPE_FIXED;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var INotifier
     */
    protected $notifier;

    /**
     * @var FraudProtectionQueueService
     */
    private $service;

    /**
     * @var FraudProtectionCsvExporter
     */
    private $exporter;

    /**
     * @var FraudProtectionFtpUploader
     */
    private $uploader;

    /**
     * @param FraudProtectionQueueService $service
     * @param FraudProtectionCsvExporter $exporter
     * @param FraudProtectionFtpUploader $uploader
     * @param LoggerInterface $logger
     * @param INotifier $notifier
     */
    public function __construct(
        LoggerInterface $logger,
        INotifier $notifier,
        FraudProtectionQueueService $service,
        FraudProtectionCsvExporter $exporter,
        FraudProtectionFtpUploader $uploader
    )
    {
        $this->logger = $logger;
        $this->notifier = $notifier;
        $this->service = $service;
        $this->exporter = $exporter;
        $this->uploader = $uploader;
    }

    public function execute()
    {
        $this->processFraudProtections();
        $this->logger->info('CSV with fraud protected companies has been uploaded.');

        $date = new DateTime;
        $this->notifier->triggerSuccess(
            $this->getName(),
            $date->getTimestamp(),
            "CSV with fraud protected companies has been uploaded. Date {$date->format('d/m/Y')}"
        );
    }

    private function processFraudProtections()
    {
        $queueItems = $this->service->getQueueItemsToSend();

        $file = $this->exporter->exportCompanies($queueItems);
        $filename = date('dmy') . '.csv';
        $this->uploader->upload($file, $filename);
        unlink($file->getPathname());

        foreach ($queueItems as $queueItem) {
            $this->service->removeFromQueue($queueItem);
        }
    }
}
