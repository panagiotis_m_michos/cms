<?php

namespace FraudProtectionModule\Repositories;

use FraudProtectionModule\Entities\FraudProtectionQueue;
use Repositories\BaseRepository_Abstract;

class FraudProtectionQueueRepository extends BaseRepository_Abstract
{
    /**
     * @return FraudProtectionQueue[]
     */
    public function getQueueItemsToSend()
    {
        return $this->createQueryBuilder('q')
            ->join('q.company', 'c')
            ->andWhere('c.companyNumber IS NOT NULL')
            ->andWhere('c.incorporationDate IS NOT NULL')
            ->getQuery()
            ->getResult();
    }
}
