<?php

namespace WholesaleModule\Controllers;

use Basket;
use BasketModule\Controllers\PackageBasketController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use WholesaleModule\Redirections\PackageRedirection;

class BasketController
{
    /**
     * @var PackageBasketController
     */
    private $basketController;

    /**
     * @var PackageRedirection
     */
    private $packageRedirection;

    /**
     * @var Basket
     */
    private $basket;

    /**
     * @param PackageBasketController $basketController
     * @param PackageRedirection $packageRedirection
     * @param Basket $basket
     */
    public function __construct(PackageBasketController $basketController, PackageRedirection $packageRedirection, Basket $basket)
    {
        $this->basketController = $basketController;
        $this->packageRedirection = $packageRedirection;
        $this->basket = $basket;
    }

    /**
     * @param int $packageId
     * @return RedirectResponse
     */
    public function addPackage($packageId)
    {
        $this->basketController->addPackage($packageId);
        $package = $this->basket->getPackageOrNull();
        return $this->packageRedirection->getRedirection($package);
    }
}