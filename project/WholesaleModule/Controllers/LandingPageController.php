<?php

namespace WholesaleModule\Controllers;

use Exceptions\Technical\EmailException;
use FeefoModule\IFeefo;
use RouterModule\Helpers\ControllerHelper;
use Symfony\Component\HttpFoundation\Response;
use TemplateModule\Renderers\IRenderer;
use TranslationModule\MsgId;
use WholesaleModule\Emailers\WholesaleEmailer;
use WholesaleModule\Forms\ContactForm;

class LandingPageController
{
    /**
     * @var IFeefo
     */
    private $feefo;

    /**
     * @var IRenderer
     */
    private $renderer;

    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    /**
     * @var WholesaleEmailer
     */
    private $wholesaleEmailer;

    /**
     * @param IFeefo $feefo
     * @param IRenderer $renderer
     * @param ControllerHelper $controllerHelper
     * @param WholesaleEmailer $wholesaleEmailer
     */
    public function __construct(IFeefo $feefo, IRenderer $renderer, ControllerHelper $controllerHelper, WholesaleEmailer $wholesaleEmailer)
    {
        $this->feefo = $feefo;
        $this->renderer = $renderer;
        $this->controllerHelper = $controllerHelper;
        $this->wholesaleEmailer = $wholesaleEmailer;
    }

    /**
     * @return Response
     */
    public function professional()
    {
        $data = $this->getDefaultData();
        if (!$data) {
            return $this->controllerHelper->redirectionTo('professional_landing_page');
        }
        $data['searchUrl'] = $this->controllerHelper->getUrl('professional_company_search', ['proceed' => TRUE]);
        $data['packageUrl'] = $this->controllerHelper->getUrl('professional_add_package');
        return $this->renderer->render($data);
    }

    /**
     * @return Response
     */
    public function taxassist()
    {
        $data = $this->getDefaultData();
        if (!$data) {
            return $this->controllerHelper->redirectionTo('taxassist_landing_page');
        }
        $data['searchUrl'] = $this->controllerHelper->getUrl('taxassist_company_search', ['proceed' => TRUE]);
        $data['packageUrl'] = $this->controllerHelper->getUrl('taxassist_add_package');
        return $this->renderer->render($data);
    }

    /**
     * @return Response
     */
    public function icaew()
    {
        $data = $this->getDefaultData();
        if (!$data) {
            return $this->controllerHelper->redirectionTo('icaew_landing_page');
        }
        $data['searchUrl'] = $this->controllerHelper->getUrl('icaew_company_search', ['proceed' => TRUE]);
        $data['packageUrl'] = $this->controllerHelper->getUrl('icaew_add_package');
        return $this->renderer->render($data);
    }

    /**
     * @return array
     */
    private function getDefaultData()
    {
        $form = $this->controllerHelper->buildForm(new ContactForm());
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->wholesaleEmailer->sendContactUs($form->getData());
                $this->controllerHelper->setFlashMessage(MsgId::EMAIL_SENT, ControllerHelper::MESSAGE_SUCCESS);
                return [];
            } catch (EmailException $e) {
                $this->controllerHelper->setFlashMessage(MsgId::EMAIL_EXCEPTION, ControllerHelper::MESSAGE_ERROR);
            }
        }
        return [
            'feefoAverage' => $this->feefo->getSummary()->getAverageRating(),
            'feefoReviewCount' => $this->feefo->getSummary()->getReviewCount(),
            'form' => $form->createView()
        ];
    }
}