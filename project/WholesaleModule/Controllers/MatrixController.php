<?php

namespace WholesaleModule\Controllers;

use RouterModule\Helpers\ControllerHelper;
use Services\SessionService;
use Symfony\Component\HttpFoundation\Response;
use TemplateModule\Renderers\IRenderer;
use Basket;

class MatrixController
{
    /**
     * @var IRenderer
     */
    private $renderer;

    /**
     * @var Basket
     */
    private $basket;

    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    /**
     * @param IRenderer $renderer
     * @param Basket $basket
     * @param ControllerHelper $controllerHelper
     */
    public function __construct(
        IRenderer $renderer,
        Basket $basket,
        ControllerHelper $controllerHelper
    )
    {
        $this->renderer = $renderer;
        $this->basket = $basket;
        $this->controllerHelper = $controllerHelper;
    }

    /**
     * @return Response
     */
    public function renderProfessional()
    {
        return $this->renderer->render($this->getDefault('professional_add_package'));
    }

    /**
     * @return Response
     */
    public function renderIcaew()
    {
        return $this->renderer->render($this->getDefault('icaew_add_package'));
    }

    /**
     * @return Response
     */
    public function renderTaxAssist()
    {
        return $this->renderer->render($this->getDefault('taxassist_add_package'));
    }
    
    private function getDefault($url)
    {
        if ($this->basket->getIncorporationCompanyName()) {
            $data['introductionStrip']['title'] = "<b>Good News</b>, <span class='lead__company-name'>{$this->basket->getIncorporationCompanyName()}</span> is available to register!";
        }
        $data['packageUrl'] = $this->controllerHelper->getUrl($url);
        return $data;
    }
}
