<?php

namespace WholesaleModule\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class ContactData
{
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $fullName;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    public $email;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $phone;
}