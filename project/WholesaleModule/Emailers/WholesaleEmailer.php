<?php

namespace WholesaleModule\Emailers;

use AspectModule\Annotations\Loggable;
use EmailService;
use Exceptions\Technical\EmailException;
use FEmail;
use WholesaleModule\Dto\ContactData;

final class WholesaleEmailer
{
    const CONTACT_EMAIL = 1692;

    /**
     * @var EmailService
     */
    private $emailService;

    /**
     * @param EmailService $emailService
     */
    public function __construct(EmailService $emailService)
    {
        $this->emailService = $emailService;
    }

    /**
     * @Loggable()
     * @param ContactData $contactData
     * @throws EmailException
     */
    public function sendContactUs(ContactData $contactData)
    {
        $email = new FEmail(self::CONTACT_EMAIL);
        $email->replacePlaceHolders([
                '[FULLNAME]' => $contactData->fullName,
                '[EMAIL]' => $contactData->email,
                '[PHONE]' => $contactData->phone
            ]
        );
        $this->emailService->send($email);
    }
}