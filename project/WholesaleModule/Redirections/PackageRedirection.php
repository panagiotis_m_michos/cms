<?php

namespace WholesaleModule\Redirections;

use CommonModule\Contracts\IRedirectionProvider;
use Package;
use RouterModule\Helpers\ControllerHelper;
use Symfony\Component\HttpFoundation\RedirectResponse;

final class PackageRedirection implements IRedirectionProvider
{
    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    /**
     * @var string
     */
    private $packagesUrl;

    /**
     * @var string
     */
    private $searchUrl;

    /**
     * @param ControllerHelper $controllerHelper
     * @param string $packagesUrl
     * @param string $searchUrl
     */
    public function __construct(ControllerHelper $controllerHelper, $packagesUrl, $searchUrl)
    {
        $this->controllerHelper = $controllerHelper;
        $this->packagesUrl = $packagesUrl;
        $this->searchUrl = $searchUrl;
    }

    /**
     * @param Package $package
     * @return RedirectResponse
     */
    public function getRedirection(Package $package = NULL)
    {
        if (!$package) {
            return $this->controllerHelper->redirectionTo($this->packagesUrl);
        }
        if (!$package->companyName) {
            return $this->controllerHelper->redirectionTo($this->searchUrl);
        }
        return $this->controllerHelper->redirectionTo('basket_module_package_basket_upgrade', ['package_id' => $package->getId()]);
    }
}