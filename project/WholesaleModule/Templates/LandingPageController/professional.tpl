{extends '@structure/layout.tpl'}

{block content}

    <div class="width100 home-banner5 padcard-mobile">
        <div class="container">
            {include '@blocks/flashMessageBt.tpl'}
            <div class="row btm30">
                <div class="col-xs-12 col-md-6">
                    <h1>{$introductionStrip.title nofilter}</h1>
                    <h2 class="font-300">{$introductionStrip.description nofilter}</h2>
                </div>
            </div>
            <div class="row btm30">
                <div class="col-xs-12">
                    <form id="home2016-search" class="form-horizontal" action="{$searchUrl}" method="post" name="search">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-12 col-md-10">
                                    <input class="width100 form-control" id="companyName" name="companyName" type="text" placeholder="{$introductionStrip.searchPlaceholder nofilter}">
                                </div>
                                <div class="col-xs-12 col-md-2">
                                    <button class="width100 btn btn-lg btn-default" type="submit" name="search"><span><i class="fa fa-search"></i>&nbsp;Search</span></button>
                                </div>
                            </div>
                            <input type="hidden" value="1" id="submited_search" name="submited_search">
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-6 text-left btm20 text-center-xs">
                    <h3><a class="grey7" href="{$introductionStrip.packagesLinkURL nofilter}">{$introductionStrip.packagesLink nofilter}</a></h3>
                </div>
                <div class="hidden-xs hidden-sm col-md-2">
                    <img src="{$urlImgs}home2016/icaew-pro.png" alt="ICAEW" style="height: 85px;" class="pull-right img-responsive">
                </div>
                <div class="col-xs-12 col-md-4 btm30 pad0">
                    <div id="home2016-feefo">
                        <div class="feefo2016-top">
                            <span><strong>GOLD</strong> TRUSTED MERCHANT</span>
                        </div>
                        <div class="feefo2016-central">
                            <div class="feefo2016-left">
                                <img src="{$urlImgs}home2016/feefo.png" alt="" />
                            </div>
                            <div class="feefo2016-right">
                                <span class="yellow2 lead"><strong>{$feefoAverage}%</strong></span>
                                <span class="display-inline white">
                                    <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                </span>
                                <p class="margin0">
                                    <a href="http://www.feefo.com/feefo/viewvendor.jsp?logon=www.madesimplegroup.com/companiesmadesimple" onclick="window.open(this.href, 'Feefo', 'width=1000,height=600,scrollbars,resizable'); return false;">
                                        {$feefoReviewCount} reviews
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="home2016-progress" class="width100">
        <div class="progress-indicator-bg-left hidden-xs hidden-sm"></div>
        <div class="progress-indicator-bg-right hidden-xs hidden-sm"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 pad0">
                    <ul class="progress-indicator">
                        <li class="active"><span class="bubble">1</span><span>Search</span></li>
                        <li><span class="bubble">2</span><span>Select</span></li>
                        <li><span class="bubble">3</span><span>Buy</span></li>
                        <li><span class="bubble">4</span><span>Complete</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="width100 bg-grey-blue padcard-mobile">
        <div class="container">
            <div class="row">
                {foreach $featuresStrip.features as $feature}
                    <div class="col-xs-12 col-md-4 text-center white">
                        <img class="padcard-mobile" src="{$feature.icon nofilter}" alt="" />
                        <p class="lead font-200">{$feature.title nofilter}</p>
                        <p class="font-200">{$feature.description nofilter}</p>
                    </div>
                {/foreach}
            </div>
        </div>
    </div>
    <div class="width100 home-banner6 padcard-mobile">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h2 class="top0 btm30">{$cashbackStrip.title nofilter}</h2>
                </div>
            </div>
            <div class="row">
                {foreach $cashbackStrip.banks as $bank}
                    <div class="col-xs-12 col-md-6 text-center">
                        <div class="padcard blueborder btm10 bg-white">
                            <img class="padcard" src="{$bank.icon nofilter}" alt="" />
                            <p class="lead">{$bank.title nofilter}</p>
                            <p>{$bank.description nofilter}</p>
                        </div>
                    </div>
                {/foreach}
            </div>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="margin0 top10">{$cashbackStrip.footer nofilter}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="width100 bg-white padcard-mobile">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h2 class="top0 btm30">{$matrixStrip.title nofilter}</h2>
                </div>
            </div>
            <div class="row">
                {foreach $matrixStrip.packages as $package}
                    <div class="col-xs-12 col-md-4 matrix-price-box">
                        <h4>{$package.title nofilter}</h4>
                        <div class="main-txt button-wide-mobile">
                            <p class="text-center">{$package.description nofilter}</p>
                            <h2 class="mtop0">{$package.price}<span>+VAT</span></h2>
                            <div class="text-center">
                                {include '../addPackage.tpl' package=$package button=$defaultBuyButton packageUrl=$packageUrl}
                            </div>
                        </div>
                    </div>
                {/foreach}
            </div>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="lead margin0 top10">{$matrixStrip.footer nofilter}</p>
                </div>
            </div>
        </div>
    </div>

    {include file="@structure/feefoStrip.tpl"}

    <div class="width100 bg-white padcard-mobile">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h2 class="top0">{$servicesStrip.title}</h2>
                    <p class="lead">{$servicesStrip.description nofilter}</p>
                </div>
            </div>
            <div class="row">
                {foreach $servicesStrip.services as $service}
                    <div class="col-xs-12 col-md-6 top20">
                        <div class="row">
                            <div class="col-xs-2 col-sm-1">
                                <i class="green fa {$service.icon nofilter} fa-2x fa-fw" aria-hidden="true"></i>
                            </div>
                            <div class="col-xs-10 col-sm-11">
                                <h4 class="top0">{$service.title nofilter}</h4>
                                <p>{$service.description nofilter}</p>
                            </div>
                        </div>
                    </div>
                {/foreach}
            </div>
        </div>
    </div>
    <div class="width100 home-banner6 padcard-mobile">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h2 class="top0 btm30">{$bulkFormStrip.title nofilter}</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    {$bulkFormStrip.description nofilter}
                </div>
                <div class="col-xs-12 col-md-6" id="wholesale-contact-block">
                    {$formHelper->setTheme($form, 'cms_horizontal_default.html.twig')}
                    {$formHelper->start($form) nofilter}
                        {$formHelper->errors($form) nofilter}
                        {$formHelper->row($form['fullName'], ['label' => 'Name*:']) nofilter}
                        {$formHelper->row($form['email'], ['label' => 'Email*:']) nofilter}
                        {$formHelper->row($form['phone'], ['label' => 'Phone*:']) nofilter}
                        <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">Contact me</button>
                          </div>
                        </div>
                    {$formHelper->end($form) nofilter}
                    <script type="text/javascript">
                        window.cms.validations.wholesaleContactForm('#wholesale-contact-block form')
                    </script>
                </div>
            </div>
        </div>
    </div>
    <div class="width100 bg-white padcard-mobile">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h2 class="top0">{$partnersStrip.title nofilter}</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-center">
                    {foreach $partnersStrip.partners as $partner}
                        <img class="padcard-mobile" src="{$partner.icon nofilter}" alt="" />
                    {/foreach}
                </div>
            </div>
        </div>
    </div>
    <div class="width100 bg-grey1 padcard-mobile">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h2 class="top0">{$faqsStrip.title nofilter}</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    {if isset($faqsStrip.faqs)}
                        {foreach $faqsStrip.faqs as $faq}
                            <h4 class="display-inline">{$faq.title nofilter}</h4>
                            {$faq.description nofilter}
                        {/foreach}
                    {/if}
                </div>
            </div>
        </div>
    </div>
    

{/block}