{extends '@structure/layout.tpl'}

{block content}


    <div class="width100 bg-white padcard">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="font-300 top0">{$introductionStrip.title nofilter}</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="width100 bg-white padcard">
        <div class="container">
            {include '@blocks/flashMessageBt.tpl'}
            <div class="row">
                <div class="hidden-xs hidden-sm col-md-6"></div>

                <div class="col-xs-12 col-md-2 matrix-price-box">
                    <h4>{$matrixStrip.title1 nofilter}</h4>
                    <div class="main-txt button-wide-mobile">
                        <h2 class="mtop0">{$matrixStrip.price1}<span>+VAT</span></h2>
                        <div class="text-center">
                            {include '../addPackage.tpl' package=$packages.starter button=$defaultBuyButton packageUrl=$packageUrl}
                        </div>
                        <div class="top10 text-center hidden-md hidden-lg">
                            <a href="#collapse-{$matrixStrip.title1 nofilter}" role="button" data-toggle="collapse" class="btn btn-white">{$matrixStrip.secondaryTitle1 nofilter}</a>
                        </div>
                        <div class="collapse hidden-md hidden-lg" id="collapse-Starter">
                            <div class="well mobile-included-description">
                                <p class="small">{$matrixStrip.mDescription1 nofilter}</p>
                                <a data-toggle="tooltip" html="true" title="{$matrixStrip.mTtooltip1 nofilter}">
                                    <i class="fa fa-info-circle"></i>
                                </a>
                                <p class="small">{$matrixStrip.mDescription2 nofilter}</p>
                                <a data-toggle="tooltip" html="true" title="{$matrixStrip.mTtooltip2 nofilter}">
                                    <i class="fa fa-info-circle"></i>
                                </a>
                                <p class="small">{$matrixStrip.mDescription3 nofilter}</p>
                                <a data-toggle="tooltip" html="true" title="{$matrixStrip.mTtooltip3 nofilter}">
                                    <i class="fa fa-info-circle"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-md-2 matrix-price-box">
                    <h4>{$matrixStrip.title2 nofilter}</h4>
                    <div class="main-txt button-wide-mobile">
                        <h2 class="mtop0">{$matrixStrip.price2 nofilter}<span>+VAT</span></h2>
                        <div class="text-center">
                            {include '../addPackage.tpl' package=$packages.executive button=$defaultBuyButton packageUrl=$packageUrl}
                        </div>
                        <div class="top10 text-center hidden-md hidden-lg">
                            <a href="#collapse-{$matrixStrip.title2 nofilter}" role="button" data-toggle="collapse" class="btn btn-white">{$matrixStrip.secondaryTitle2 nofilter}</a>
                        </div>
                        <div class="collapse hidden-sm hidden-md hidden-lg" id="collapse-Executive">
                            <div class="well mobile-included-description">
                                <p class="small">{$matrixStrip.mDescription1 nofilter}</p>
                                <a data-toggle="tooltip" html="true" title="{$matrixStrip.mTtooltip1 nofilter}">
                                    <i class="fa fa-info-circle"></i>
                                </a>
                                <p class="small">{$matrixStrip.mDescription2 nofilter}</p>
                                <a data-toggle="tooltip" html="true" title="{$matrixStrip.mTtooltip2 nofilter}">
                                    <i class="fa fa-info-circle"></i>
                                </a>
                                <p class="small">{$matrixStrip.mDescription3 nofilter}</p>
                                <a data-toggle="tooltip" html="true" title="{$matrixStrip.mTtooltip3 nofilter}">
                                    <i class="fa fa-info-circle"></i>
                                </a>
                                <p class="small">{$matrixStrip.mDescription4 nofilter}</p>
                                <a data-toggle="tooltip" html="true" title="{$matrixStrip.mTtooltip4 nofilter}">
                                    <i class="fa fa-info-circle"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-md-2 matrix-price-box">
                    <h4>{$matrixStrip.title3 nofilter}</h4>
                    <div class="main-txt button-wide-mobile">
                        <h2 class="mtop0">{$matrixStrip.price3 nofilter}<span>+VAT</span></h2>
                        <div class="text-center">
                            {include '../addPackage.tpl' package=$packages.professional button=$defaultBuyButton packageUrl=$packageUrl}
                        </div>
                        <div class="top10 text-center hidden-md hidden-lg">
                            <a href="#collapse-{$matrixStrip.title3 nofilter}" role="button" data-toggle="collapse" class="btn btn-white">{$matrixStrip.secondaryTitle3 nofilter}</a>
                        </div>
                        <div class="collapse hidden-sm hidden-md hidden-lg" id="collapse-Professional">
                            <div class="well mobile-included-description">
                                <p class="small">{$matrixStrip.mDescription1 nofilter}</p>
                                <a data-toggle="tooltip" html="true" title="{$matrixStrip.mTtooltip1 nofilter}">
                                    <i class="fa fa-info-circle"></i>
                                </a>
                                <p class="small">{$matrixStrip.mDescription2 nofilter}</p>
                                <a data-toggle="tooltip" html="true" title="{$matrixStrip.mTtooltip2 nofilter}">
                                    <i class="fa fa-info-circle"></i>
                                </a>
                                <p class="small">{$matrixStrip.mDescription3 nofilter}</p>
                                <a data-toggle="tooltip" html="true" title="{$matrixStrip.mTtooltip3 nofilter}">
                                    <i class="fa fa-info-circle"></i>
                                </a>
                                <p class="small">{$matrixStrip.mDescription4 nofilter}</p>
                                <a data-toggle="tooltip" html="true" title="{$matrixStrip.mTtooltip4 nofilter}">
                                    <i class="fa fa-info-circle"></i>
                                </a>
                                <p class="small">{$matrixStrip.mDescription5 nofilter}</p>
                                <a data-toggle="tooltip" html="true" title="{$matrixStrip.mTtooltip5 nofilter}">
                                    <i class="fa fa-info-circle"></i>
                                </a>
                                <p class="small">{$matrixStrip.mDescription6 nofilter}</p>
                                <a data-toggle="tooltip" html="true" title="{$matrixStrip.mTtooltip6 nofilter}">
                                    <i class="fa fa-info-circle"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {if isset($matrixStrip.descriptions)}
                {foreach $matrixStrip.descriptions as $description}
                    <div class="row hidden-xs hidden-sm">
                        <div class="col-md-6 matrix-description-box">
                            <div>
                                <p>{$description.description nofilter}</p>
                                <a data-toggle="tooltip" html="true" title="{$description.tooltip nofilter}">
                                    <i class="fa fa-info-circle"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-2 matrix-check-box">
                            <div class="matrix-check-box-bg-{$description.starter nofilter}"><i class="fa fa-{$description.starter nofilter} fa-fw fa-lg" aria-hidden="true"></i></div>
                        </div>
                        <div class="col-md-2 matrix-check-box">
                            <div class="matrix-check-box-bg-{$description.executive nofilter}"><i class="fa fa-{$description.executive nofilter} fa-fw fa-lg" aria-hidden="true"></i></div>
                        </div>
                        <div class="col-md-2 matrix-check-box">
                            <div class="matrix-check-box-bg-{$description.professional nofilter}"><i class="fa fa-{$description.professional nofilter} fa-fw fa-lg" aria-hidden="true"></i></div>
                        </div>
                    </div>
                {/foreach}
            {/if}
            <div class="row hidden-xs hidden-sm ">
                <div class="col-md-6 btm20"></div>
                <div class="col-xs-12 col-md-2 matrix-price-box">
                    <div class="main-txt button-wide-mobile">
                        <h2 class="mtop0">{$matrixStrip.price1 nofilter}<span>+VAT</span></h2>
                        <div class="text-center">
                            {include '../addPackage.tpl' package=$packages.starter button=$defaultBuyButton packageUrl=$packageUrl}
                        </div>
                    </div>
                    <h4>{$matrixStrip.title1 nofilter}</h4>
                </div>
                <div class="col-xs-12 col-md-2 matrix-price-box">
                    <div class="main-txt button-wide-mobile">
                        <h2 class="mtop0">{$matrixStrip.price2 nofilter}<span>+VAT</span></h2>
                        <div class="text-center">
                            {include '../addPackage.tpl' package=$packages.executive button=$defaultBuyButton packageUrl=$packageUrl}
                        </div>
                    </div>
                    <h4>{$matrixStrip.title2 nofilter}</h4>
                </div>
                <div class="col-xs-12 col-md-2 matrix-price-box">
                    <div class="main-txt button-wide-mobile">
                        <h2 class="mtop0">{$matrixStrip.price3 nofilter}<span>+VAT</span></h2>
                        <div class="text-center">
                            {include '../addPackage.tpl' package=$packages.professional button=$defaultBuyButton packageUrl=$packageUrl}
                        </div>
                    </div>
                    <h4>{$matrixStrip.title3 nofilter}</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="width100 bg-white padcard" style="border-bottom: 2px solid #ddd;">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 btm20">
                    <h3 class="price-box-strip">{$productsStrip.title nofilter}</h3>
                </div>
            </div>
            <div class="row">
                {if isset($productsStrip.products)}
                    {foreach $productsStrip.products as $product}
                        <div class="col-xs-12 col-md-6 btm20">
                            <div class="price-box">
                                <h4>{$product.title nofilter}</h4>
                                <div class="main-txt button-wide-mobile">
                                    <p class="margin0">
                                        {$product.description nofilter}
                                    </p>
                                    <h2>{$product.price nofilter}<span>+VAT</span></h2>
                                    <div class="text-right">
                                        {include '../addPackage.tpl' package=$product button=$defaultBuyButton packageUrl=$packageUrl}
                                    </div>
                                </div>
                            </div>
                        </div>
                    {/foreach}
                {/if}
            </div>
        </div>
    </div>

{/block}
