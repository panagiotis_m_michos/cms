<?php

/**
 * symlink this file to www/
 * or
 * copy to the same directory and change DOCUMENT_ROOT
 */

use Bootstrap\ApplicationLoader;
use Bootstrap\Ext\FrameworkExt;

define('DOCUMENT_ROOT', dirname(dirname(dirname(dirname(__FILE__)))));

$config = require_once DOCUMENT_ROOT . '/project/bootstrap/default.php';
$config['show_errors'] = TRUE;
$config['config_path'] = DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'project/config/app/config.local.yml';
$config['environment'] = 'development';

$applicationLoader = new ApplicationLoader();
$container = $application = $applicationLoader->load($config);
$container->get(FrameworkExt::APPLICATION)->run();
