<?php

/**
 * symlink this file to www/
 * or
 * copy to the same directory and change DOCUMENT_ROOT
 */

use Bootstrap\ApplicationLoader;
use Bootstrap\Ext\FrameworkExt;

define('DOCUMENT_ROOT', dirname(dirname(dirname(dirname(__FILE__)))));

$config = require_once DOCUMENT_ROOT . '/project/bootstrap/default.php';
$config['show_errors'] = TRUE;
$config['environment'] = 'console';
$config['config_path'] = DOCUMENT_ROOT . '/project/config/app/config.test.yml';

$applicationLoader = new ApplicationLoader();
$container = $application = $applicationLoader->load($config);
$container->get(FrameworkExt::APPLICATION)->run();
