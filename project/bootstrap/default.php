<?php

require_once DOCUMENT_ROOT . '/vendor/autoload.php';
require_once DOCUMENT_ROOT . '/project/bootstrap/functions.php';
require_once DOCUMENT_ROOT . '/project/bootstrap/ApplicationLoader.php';

// @TODO: temporary, should be considered in FileStorage
umask(0000);
ini_set('max_execution_time', 240);
date_default_timezone_set("Europe/London");

return [
    'root' => DOCUMENT_ROOT,
    'show_errors' => FALSE,
    'dirs_to_load' => [
        DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'libs',
        DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'project',
    ],
    'temp_dir' => DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'temp',
    'cache_dir' => DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'temp' . DIRECTORY_SEPARATOR . 'cache',
    'app_dir' => DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'project',
    'dirs_to_ignore' => [
        DOCUMENT_ROOT . '/libs/3rdParty/Nette/Forms',
    ],
    'config_path' => DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'project/config/app/config.shared.yml',
    'use_new_config_loading' => TRUE
];
