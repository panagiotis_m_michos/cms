<?php

namespace Bootstrap\Ext;

use BankingModule\Config\DiLocator as BankingDiLocator;
use BootstrapModule\Ext\IExtension;
use DiLocator;
use OrderModule\Config\DiLocator as OrderDiLocator;
use ServiceSettingsModule\Config\DiLocator as ServiceSettingsDiLocator;
use Symfony\Component\DependencyInjection\Container;
use VoServiceModule\Config\DiLocator as VoServiceModuleDiLocator;
use FraudProtectionModule\Config\DiLocator as FraudProtectionDiLocator;
use CashBackModule\Config\DiLocator as CashBackDiLocator;

class EventExt implements IExtension
{

    /**
     * @param Container $container
     */
    public function load(Container $container)
    {
        $dispatcher = $container->get(DiLocator::DISPATCHER);
        $dispatcher->addSubscriber($container->get(DiLocator::LISTENER_INCORPORATION));
        $dispatcher->addSubscriber($container->get(DiLocator::LISTENER_SERVICE));
        $dispatcher->addSubscriber($container->get(DiLocator::LISTENER_SERVICES_EMAIL));
        $dispatcher->addSubscriber($container->get(DiLocator::LISTENER_TOKEN_EMAIL));
        $dispatcher->addSubscriber($container->get(DiLocator::LISTENER_TOKEN));
        $dispatcher->addSubscriber($container->get(DiLocator::LISTENER_SIGN_UP));
        $dispatcher->addSubscriber($container->get(DiLocator::LISTENER_COMPANY_HOUSE_SUBMISSION_STATUS));
        $dispatcher->addSubscriber($container->get(DiLocator::LISTENER_COMPANY_HOUSE_INCORPORATION));
        $dispatcher->addSubscriber($container->get(DiLocator::LISTENER_COMPANY_HOUSE_CHANGE_OF_NAME));
        $dispatcher->addSubscriber($container->get(DiLocator::LISTENER_CASHBACK));
        $dispatcher->addSubscriber($container->get(DiLocator::LISTENER_CUSTOMER));
        $dispatcher->addSubscriber($container->get(DiLocator::LISTENER_FEEFO_UPDATE_STATUS));
        $dispatcher->addSubscriber($container->get(DiLocator::LISTENER_PAYMENT));
        $dispatcher->addSubscriber($container->get(DiLocator::LISTENER_ORDER));
        $dispatcher->addSubscriber($container->get(VoServiceModuleDiLocator::LISTENER_VO_SERVICE_QUEUE));
        $dispatcher->addSubscriber($container->get(DiLocator::LISTENER_DELETE_UNFORMED_COMPANY));
        $dispatcher->addSubscriber($container->get(DiLocator::LISTENER_ORDER_EMAIL));
        $dispatcher->addSubscriber($container->get(DiLocator::LISTENER_BASKET));
        $dispatcher->addSubscriber($container->get(DiLocator::LISTENER_NOMINEE_DIRECTOR));
        $dispatcher->addSubscriber($container->get(DiLocator::LISTENER_NOTIFY_PREMIER_BUSINESS));
        $dispatcher->addSubscriberService(DiLocator::LISTENER_CREATE_SERVICE_SETTINGS, 'Dispatcher\Listeners\CreateServiceSettingsListener');
        $dispatcher->addSubscriberService(DiLocator::LISTENER_SUSPEND_AUTO_RENEWAL, 'Dispatcher\Listeners\SuspendAutoRenewalListener');
        $dispatcher->addSubscriberService(ServiceSettingsDiLocator::LISTENERS_DISABLE_AUTO_RENEWAL_LISTENER, 'ServiceSettingsModule\Listeners\DisableAutoRenewalListener');
        $dispatcher->addSubscriberService(DiLocator::LISTENER_TAX_ASSIST, 'OfferModule\Listeners\TaxAssistListener');
        $dispatcher->addSubscriberService(DiLocator::LISTENER_OFFER_EMAIL, 'OfferModule\Listeners\EmailListener');
        $dispatcher->addSubscriberService(DiLocator::LISTENER_ID_CHECK, 'IdCheckModule\Listeners\PaymentListener');
        $dispatcher->addSubscriberService(ServiceSettingsDiLocator::LISTENER_DISABLE_AUTO_RENEWAL, 'ServiceSettingsModule\Listeners\DisableAutoRenewalListener');
        $dispatcher->addSubscriberService(BankingDiLocator::LISTENER_SEND_TSB_APPLICATION_EMAIL, 'BankingModule\Listeners\SendTsbApplicationEmailListener');
        $dispatcher->addSubscriberService(VoServiceModuleDiLocator::LISTENER_DELETE_PRODUCT_FROM_QUEUE, 'VoServiceModule\Listeners\DeleteProductFromQueueListener');
        $dispatcher->addSubscriberService(FraudProtectionDiLocator::LISTENER_ADD_COMPANY_TO_QUEUE, 'FraudProtectionModule\Listeners\AddCompanyToQueueListener');
        $dispatcher->addSubscriberService(CashBackDiLocator::LISTENER_CREATE_BARCLAYS_CASH_BACK, 'CashBackModule\Listeners\CreateBarclaysCashBackListener');
        $dispatcher->addSubscriberService(CashBackDiLocator::LISTENER_CREATE_TSB_CASH_BACK, 'CashBackModule\Listeners\CreateTsbCashBackListener');
        $dispatcher->addSubscriberService(OrderDiLocator::UPDATE_ORDER_ITEMS_DETAILS_LISTENER, 'OrderModule\Listeners\UpdateOrderItemsDetailsListener');
    }
}
