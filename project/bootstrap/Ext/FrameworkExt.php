<?php

namespace Bootstrap\Ext;

use BootstrapModule\Ext\FrameworkExt_Abstract;
use BootstrapModule\Ext\IExtension;
use Customer;
use DiLocator;
use Environment;
use FApplication;
use FeatureModule\Context;
use FeatureModule\FeatureExt;
use FeatureModule\Matchers\HashContainer;
use FeatureModule\Matchers\Identity;
use FeatureModule\Matchers\Ip;
use FUser;
use Registry;
use RouterModule\RouterExt;
use SagePay\Token\SageFactory;
use StatCollector;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Request;

class FrameworkExt extends FrameworkExt_Abstract implements IExtension
{
    /**
     * @param Container $containerBuilder
     * @return FApplication
     */
    public function load(Container $containerBuilder)
    {
        Registry::$emailerFactory = $containerBuilder->get(DiLocator::EMAILER_FACTORY);
        Registry::$container = $containerBuilder;
        //sage pay annotations
        SageFactory::register(DOCUMENT_ROOT . '/vendor/');

        $application = new FApplication(
            $containerBuilder->getParameter('config'),
            $containerBuilder
        );
        if ($containerBuilder->getParameter('environment') === Environment::PRODUCTION) {
            StatCollector::setup($containerBuilder->getParameter('stats_collector'), TRUE);
            FApplication::$host = $containerBuilder->getParameter('host');
        } else {
            FApplication::$host = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : $containerBuilder->getParameter('host');
        }

        $context = $this->createContext($containerBuilder->get(RouterExt::REQUEST));

        $containerBuilder->set(FeatureExt::CONTEXT, $context);
        $containerBuilder->set(self::APPLICATION, $application);
        $containerBuilder->set(self::ROUTER, FApplication::$router);
        $containerBuilder->set(self::TEMPLATE_ENGINE, FApplication::$template->getEngine());
    }

    /**
     * @param Request $request
     * @return Context
     */
    public function createContext(Request $request)
    {
        $context = new Context();
        $context->set(Context::TYPE_IP, new Ip($request->getClientIp()));
        $context->set(Context::TYPE_COOKIE, new HashContainer($request->cookies->all()));
        $context->set(Context::TYPE_QUERY_PARAM, new HashContainer($request->query->all()));
        if (Customer::isSignedIn()) {
            $customer = Customer::getSignedIn();
            $context->set('customer_id', new Identity($customer->getId()));
        }
        if (FUser::isSignedIn()) {
            $user = FUser::getSignedIn();
            $context->set('user_id', new Identity($user->getId()));
        }
        return $context;
    }
}
