<?php

function get_apache_var($var)
{
    if (function_exists('apache_getenv')) {
        return apache_getenv($var);
    } elseif (function_exists('getenv')) {
        return getenv($var);
    }

    return NULL;
}

/**
 * Debug function
 * @param mixed $param
 * @return mixed
 */
function pr($param)
{
    return FTools::pr($param);
}

/**
 * escape html string
 * @param string $string
 * @return string
 */
function _e($string)
{
    return htmlspecialchars($string, ENT_QUOTES);
}

/**
 * @return boolean
 */
function isCli()
{
    if (php_sapi_name() == 'cli' && empty($_SERVER['REMOTE_ADDR'])) {
        return TRUE;
    } else {
        return FALSE;
    }
}
