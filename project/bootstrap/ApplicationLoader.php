<?php

namespace Bootstrap;

use AspectModule\AopExt;
use Bootstrap\Ext\EventExt;
use Bootstrap\Ext\FrameworkExt;
use BootstrapModule\ApplicationLoader_Abstract;
use BootstrapModule\Ext\BootExt;
use BootstrapModule\Ext\Config\ConfigEnvExt;
use BootstrapModule\Ext\Config\ConfigYmlExt;
use BootstrapModule\Ext\Di\SymfonyDiExt;
use BootstrapModule\Ext\DoctrineExt;
use BootstrapModule\Ext\RobotExt;
use BootstrapModule\Ext\SymfonyForms\SymfonyFormsExt;
use BootstrapModule\Ext\UiContentStructure\SmartyExt;
use CacheModule\BootstrapExtension\CacheExt;
use ErrorModule\Ext\DebugExt;
use ErrorModule\Ext\PanelExt;
use FeatureModule\FeatureExt;
use RouterModule\RouterExt;
use LoggableModule\LoggableExt;
use Symfony\Component\DependencyInjection\Container;
use Utils\Directory;

require_once DOCUMENT_ROOT . '/project/bootstrap/Ext/FrameworkExt.php';
require_once DOCUMENT_ROOT . '/project/bootstrap/Ext/EventExt.php';
require_once DOCUMENT_ROOT . '/project/LoggableModule/LoggableExt.php';

class ApplicationLoader extends ApplicationLoader_Abstract
{
    /**
     * @TODO reduce amount of defines
     * @param Directory $docRoot
     */
    public function define(Directory $docRoot)
    {
        define('PROJECT_DIR', $docRoot->getPath() . '/project');
        define('WWW_DIR', $docRoot->getPath() . '/www');
        define('LIBS_DIR', $docRoot->getPath() . '/libs');
        define('TEMP_DIR', $docRoot->getPath() . '/temp');
        define('CACHE_DIR', TEMP_DIR . '/cache');
        define('LOG_DIR', $docRoot->getPath() . '/logs');
        define('GEDMO_LOGGABLE_USERNAME', 'cron');
        define('CONSOLE_DIR', $docRoot->getPath() . '/console/');
        define('TEST_DIR', $docRoot->getPath() . DIRECTORY_SEPARATOR . 'test');
        define('TEST_EMAIL1', 'dev-test1@madesimplegroup.com');
        define('TEST_EMAIL2', 'dev-test2@madesimplegroup.com');
        define('TEST_EMAIL3', 'dev-test3@madesimplegroup.com');

        // database tables
        define('TBL_NODES', 'cms2_nodes');
        define('TBL_PAGES', 'cms2_pages');
        define('TBL_USERS_LOGS', 'cms2_users_logs');
        define('TBL_LANGUAGES', 'cms2_languages');
        define('TBL_USERS', 'cms2_users');
        define('TBL_USERS_ROLES', 'cms2_users_roles');
        define('TBL_FILES', 'cms2_files');
        define('TBL_STATIC_TEXTS', 'cms2_static_texts');
        define('TBL_STATIC_TEXTS_LANGS', 'cms2_static_texts_langs');
        define('TBL_PROPERTIES', 'cms2_properties');
        define('TBL_TRANSACTIONS', 'cms2_transactions');
        define('TBL_ORDERS', 'cms2_orders');
        define('TBL_ORDERS_ITEMS', 'cms2_orders_items');
        define('TBL_CUSTOMERS', 'cms2_customers');
        define('TBL_JOURNEY_CUSTOMERS', 'cms2_journey_customers');
        define('TBL_JOURNEY_CUSTOMER_PRODUCTS', 'cms2_journey_customer_products');
        define('TBL_JOURNEY_PRODUCTS_QUESTIONS', 'cms2_journey_products_questions');
        define('TBL_RESERVED_WORDS', 'cms2_reserved_words');
        define('TBL_AFFILIATES', 'cms2_affiliates');
        define('TBL_COMPANY_CUSTOMER', 'cms2_company_customer');
        define('TBL_ANNUAL_RETURN_SERVICE', 'cms2_annual_return_service');
        define('TBL_MR_SITE_CODES', 'cms2_mr_site_codes');
        define('TBL_SIGNUPS', 'cms2_signups');
        define('TBL_BARCLAYS_SOLETRADER', 'cms2_barclays_soletrader');
        define('TBL_COMPANY_DOCUMENTS', 'ch_company_documents');
        define('TBL_CUSTOMER_LOG', 'cms2_customer_log');
        define('TBL_COMPANIES_MONITORING', 'ch_company_monitoring');
        define('TBL_FEEDBACKS', 'cms2_feedbacks');
        define('TBL_DELETED_COMPANIES', 'cms2_deleted_companies');
        define('TBL_AFFILIATE_PRODUCTS', 'cms2_affiliate_products');
        define('TBL_VOUCHERS', 'cms2_vouchers');
        define('TBL_CREDITS_ADDED', 'cms2_credits_added');
        define('TBL_TAX_ASSIST_LEAD_TRACKING', 'cms2_tax_assist_lead_tracking');
        define('TBL_CASHBACK', 'cms2_cashback');
        define('TBL_TOKENS', 'cms2_tokens');
        define('TBL_TOKEN_LOGS', 'cms2_token_logs');
        define('TBL_PAYMENTS_FORM_LOGS', 'cms2_payment_form_logs');
        define('TBL_EMAIL_LOGS', 'cms2_email_logs');
        define('TBL_SERVICES', 'cms2_services');
        define('TBL_SERVICE_SETTINGS', 'cms2_service_settings');
        define('TBL_FORM_SUBMISSIONS', 'ch_form_submission');
        define('TBL_COMPANY_INCORPORATIONS', 'ch_company_incorporation');
        define('TBL_INCORPORATION_MEMBERS', 'ch_incorporation_member');
        define('TBL_DOCUMENTS', 'ch_document');
        define('TBL_ADDRESS_CHANGES', 'ch_address_change');
        define('TBL_COMPANIES', 'ch_company');
        //used everywhere
        define('TBL_COMPANY', TBL_COMPANIES);
        define('TBL_TOKEN_HISTORY', 'cms2_token_logs');
        define('TBL_CASHBACKS', 'cms2_cashback');
        define('TBL_OFFICER_RESIGNATIONS', 'ch_officer_resignation');
        define('TBL_EVENTS', 'cms2_events');
        define('TBL_CUSTOMER_DETAILS', 'cms2_offer_customer_details');
        define('TBL_CARD_ONE', 'ch_card_one');
        define('TBL_ID_CHECKS', 'cms2_id_checks');
        define('TBL_ENTITY_LOGS', 'entities_log');
        define('TBL_CUSTOMER_FILES', 'cms2_customer_files');
        define('TBL_ANSWERS', 'cms2_answers');
        define('TBL_TOOLKIT_OFFERS', 'cms2_toolkit_offers');
        define('TBL_BARCLAYS', 'ch_barclays');
        define('TBL_TSB_LEADS', 'ch_tsb_leads');
        define('TBL_COMPANY_SETTINGS', 'cms2_company_settings');

        // PATHS
        define('TEMP_SESSION_DIR', TEMP_DIR . '/sessions');
        define('TEMP_LOGS_DIR', LOG_DIR);
        define('TEMP_CACHE_DIR', CACHE_DIR);
        define('TEMP_OTHER_DIR', PROJECT_DIR . '/temp/upload/ch_documents/_other/');
        define('ADMIN_MODULE_DIR', PROJECT_DIR . '/AdminModule');
        define('ADMIN_CONTROLERS_DIR', ADMIN_MODULE_DIR . '/controlers');
        define('ADMIN_TEMPLATES_DIR', ADMIN_MODULE_DIR . '/templates');
        define('FRONT_MODULE_DIR', PROJECT_DIR . '/FrontModule');
        define('FRONT_CONTROLERS_DIR', FRONT_MODULE_DIR . '/controlers');
        define('FRONT_TEMPLATES_DIR', FRONT_MODULE_DIR . '/Templates');
        define('UPLOAD_DIR', DOCUMENT_ROOT . '/www/project/upload');
        define('FILES_DIR', UPLOAD_DIR . '/files');
        define('IMAGES_DIR', UPLOAD_DIR . '/imgs');
        define('FORMS_DIR', LIBS_DIR . '/Framework/Forms');
        define('FF_CUSTOM_CONTROLS', PROJECT_DIR . '/custom/ff/custom_controls');
        define('FF_CUSTOM_VALIDATORS', PROJECT_DIR . '/custom/ff/custom_validators');
        define('MODELS_DIR', PROJECT_DIR . '/models/');
        define('VENDOR_DIR', DOCUMENT_ROOT . '/vendor/');
        define('CRON_DIR', PROJECT_DIR . '/Cron');
        define('CRON_LOG_DIR', TEMP_LOGS_DIR);
        define('ENTITY_DIR', PROJECT_DIR . '/models/Entities');
        define('REPOSITORY_DIR', PROJECT_DIR . '/models/Repositories');
        define('SERVICE_DIR', PROJECT_DIR . '/services');
        define('COMPONENT_DIR', DOCUMENT_ROOT . '/www/components');

        // URL
        define('URL_ROOT', '/');
        define('PROJECT_DIR_R', URL_ROOT . 'project');
        define('ADMIN_URL_ROOT', URL_ROOT . 'admin/');
        define('COMPONENT_URL', URL_ROOT . 'components');
        define('TEMP_OTHER_DIR_R', URL_ROOT . 'project/temp/upload/ch_documents/_other/');
        define('UPLOAD_DIR_R', PROJECT_DIR_R . '/upload');
        define('FILES_DIR_R', UPLOAD_DIR_R . '/files');
        define('IMAGES_DIR_R', UPLOAD_DIR_R . '/imgs');
        define('ICONS_DIR_R', URL_ROOT . 'admin/imgs/icons/');
        define('FORMS_DIR_R', URL_ROOT . 'libs/Framework/Forms/');

        //various
        define('DATE_MYSQL', 'Y-m-d H:i:s');
        define('DATE_DEFAULT', 'd/m/Y');
        define('CRON_HASH', '6a256241ea0b7a305ba21473a7667543');
    }

    /**
     * @param Container $containerBuilder
     * @return array
     */
    public function getExtensions(Container $containerBuilder)
    {
        return [
            new BootExt(),
            new RobotExt(),
            new ConfigYmlExt(),
            new ConfigEnvExt(),
            new DebugExt(),
            new SymfonyDiExt(),
            new SymfonyFormsExt(),
            new RouterExt(),
            new DoctrineExt(),
            new FrameworkExt(),
            new PanelExt(),
            new EventExt(),
            new SmartyExt(),
            new CacheExt(),
            //new AopExt(),
            new LoggableExt(),
            new FeatureExt()
        ];
    }
}
