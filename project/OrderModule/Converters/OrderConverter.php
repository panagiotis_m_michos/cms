<?php

namespace OrderModule\Converters;

use Order as OldOrder;
use Entities\Order;
use Services\OrderService;

class OrderConverter
{
    /**
     * @var OrderService
     */
    private $orderService;

    /**
     * @param OrderService $orderService
     */
    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    /**
     * @param OldOrder $order
     * @return Order
     */
    public function toEntity(OldOrder $order)
    {
        return $this->orderService->getOrderById($order->getId());
    }

    /**
     * @param Order $order
     * @return OldOrder
     */
    public function toOldOrder(Order $order)
    {
        return new OldOrder($order->getId());
    }
}
