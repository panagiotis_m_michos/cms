<?php

namespace OrderModule\Pdf;

use Entities\Order;

interface IInvoiceGenerator
{
    /**
     * @param Order $order
     * @return string
     */
    public function getContents(Order $order);
}
