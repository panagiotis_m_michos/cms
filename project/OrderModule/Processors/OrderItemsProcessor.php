<?php

namespace OrderModule\Processors;

use AnnualReturn;
use AnnualReturnServiceModel;
use Basket;
use BasketProduct;
use CronJobsControler;
use Customer as OldCustomer;
use Company as OldCompany;
use CHFiling;
use Exception;
use Package;
use Services\CompanyService;
use Services\OrderItemService;

class OrderItemsProcessor
{
    /**
     * @var CHFiling
     */
    private $chFiling;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var OrderItemService
     */
    private $orderItemService;

    /**
     * @param CompanyService $companyService
     * @param OrderItemService $orderItemService
     * @param CHFiling $chFiling
     */
    public function __construct(CompanyService $companyService, OrderItemService $orderItemService, CHFiling $chFiling)
    {
        $this->chFiling = $chFiling;
        $this->companyService = $companyService;
        $this->orderItemService = $orderItemService;
    }

    /**
     * @param OldCustomer $customer
     * @param BasketProduct[] $basketItems
     * @param int $orderId
     * @throws Exception
     */
    public function saveItems(OldCustomer $customer, $basketItems, $orderId)
    {
        // search for package
        foreach ($basketItems as $basketItem) {
            if ($basketItem->getClass() == 'Package' || $basketItem instanceof Package) {
                if ($basketItem->companyName == NULL) {
                    if ($basketItem->getId() == Package::PACKAGE_RESERVE_COMPANY_NAME) {
                        $basketItem->companyName = 'Reserve a Company Name';
                    } else {
                        $basketItem->companyName = 'Change your company name';
                    }
                }

                $company = $this->chFiling->setCompany($customer->getId(), $basketItem->companyName);
                $basketItem->setCompanyId($company->getCompanyId());

                $this->resolvePrintedDocuments($customer, $company, $basketItem);

                if ($customer->isRetail()) {
                    // setting reminder for normal customer
                    // and don't enable erminders for Sole Trader Plus and RACN
                    if (!($basketItem->getId() == CronJobsControler::RACN || $basketItem->getId() == CronJobsControler::SOLE_TRADER_PLUS)) {
                        $company->setEreminderId(1);
                    }
                }

                $company->setOrderId($orderId);
                $company->setProductId($basketItem->getId());
                $company->setCashBackAmount($basketItem->getCashBackAmount());
                $company->getLastFormSubmission()->getForm()->setType($basketItem->typeId);

                // lock company
                if ($basketItem->lockCompany) {
                    $company->lockCompany();
                }

                // included products
                $includedProducts = $basketItem->getPackageProducts('includedProducts');
                $this->saveItemsToCompany($customer, $includedProducts, $company);

                // other products from basket
                $this->saveItemsToCompany($customer, $basketItems, $company);
                return;

                // add credit to customer's account
            } elseif ($basketItem->getClass() == 'CreditProduct') {
                $customer->credit += $basketItem->getPrice();
                $customer->save();
            }
        }

        // save other basket items - no package there
        $this->saveItemsToCompany($customer, $basketItems, NULL);
    }

    /**
     * @param OldCustomer $customer
     * @param BasketProduct[] $basketItems
     * @param OldCompany $company
     */
    public function saveItemsToCompany($customer, $basketItems, $company = NULL)
    {
        foreach ($basketItems as $basketItem) {
            $itemCompanyId = $basketItem->getCompanyId();
            if ($itemCompanyId) { // product assigned to company (not in package)
                $itemCompany = $this->chFiling->getCustomerCompany($itemCompanyId, $customer->getId());
            } elseif ($company) { // product included in formation package (will be assigned to company in argument)
                $itemCompany = $company;
                $basketItem->setCompanyId($itemCompany->getCompanyId());
            } else {
                continue;
            }

            $orderItem = $basketItem->orderItem;
            if ($itemCompany && $orderItem && !$orderItem->getCompany()) {
                $orderItem->setCompany($this->companyService->getCompanyById($itemCompany->getCompanyId()));
                $this->orderItemService->save($orderItem);
            }

            switch ($basketItem->getClass()) {
                case 'BundlePackage':
                    $includedProducts = $basketItem->getPackageProducts('includedProducts');
                    $this->saveItemsToCompany($customer, $includedProducts, $itemCompany);
                    break;
                case 'AnnualReturn':

                    // check incorporation
                    // setAnnualReturn() will create form submission
                    if ($itemCompany->getStatus() == 'complete') {
                        $itemCompany->setAnnualReturn($basketItem->getId());
                    } else {
                        $itemCompany->setAnnualReturnId($basketItem->getId());
                    }

                    // service
                    if ($basketItem->getId() == AnnualReturn::ANNUAL_RETURN_SERVICE_PRODUCT) {
                        try {
                            $ars = new AnnualReturnServiceModel();
                            $ars->companyId = $itemCompany->getCompanyId();
                            $ars->customerId = $customer->getId();
                            $ars->productId = AnnualReturn::ANNUAL_RETURN_SERVICE_PRODUCT;
                            $ars->save();
                        } catch (Exception $e) {

                        }
                    }

                    break;
                case 'DCA':
                    $itemCompany->setDcaId($basketItem->getId());
                    break;
                case 'NomineeDirector':
                case 'ResidentDirector':
                    $itemCompany->setNomineeDirectorId($basketItem->getId());
                    break;
                case 'NomineeSecretary':
                    $itemCompany->setNomineeSecretaryId($basketItem->getId());
                    break;
                case 'NomineeShareholder':
                    $itemCompany->setNomineeShareholderId($basketItem->getId());
                    break;
                case 'RegisterOffice':
                    $itemCompany->setRegisteredOfficeId($basketItem->getId());
                    break;
                case 'SameDayFormation':
                    $itemCompany->getLastFormSubmission()->getForm()->setSameDay(TRUE);
                    break;
                case 'TaxAssist':
                    //TaxAssistLeadModel::saveLead($customer, $itemCompany);
                    break;
                case 'CompanyNameChange':
                    $itemCompany->setChangeName($basketItem->getId());
                    break;
                case 'ServiceAddress':
                    $itemCompany->setServiceAddress($basketItem->getId());
                    break;
                case 'MemorandumArticles':
                    $itemCompany->setMAPrinted(FALSE);
                    $itemCompany->setMACoverLetterPrinted(FALSE);
                    break;
                case 'CertificateUpgrade':
                    $itemCompany->setCertificatePrinted(FALSE);
                    break;
            }
        }
    }

    /**
     * @param OldCustomer $customer
     * @param OldCompany $company
     * @param BasketProduct $item
     */
    private function resolvePrintedDocuments(OldCustomer $customer, OldCompany $company, BasketProduct $item)
    {
        // ALL USERS
        if ($item->getId() == Package::PACKAGE_BRONZE) {
            $company->setCertificatePrinted(TRUE);
        } elseif ($item->getId() == Package::PACKAGE_BASIC
            || $item->getId() == Package::PACKAGE_DIGITAL
            || $item->getId() == Package::PACKAGE_CONTRACTOR
            || $item->getId() == Package::PACKAGE_INTERNATIONAL
            || $item->getId() == Package::PACKAGE_RESERVE_COMPANY_NAME
            || $item->getId() == Package::PACKAGE_WHOLESALE_STARTER
        ) {
            $company->setCertificatePrinted(TRUE);
            $company->setBronzeCoverLetterPrinted(TRUE);
        }

        if ($customer->isWholesale()) {
            //don't print coverLetter for starter/wholesale customer
            if ($item->getId() == Package::PACKAGE_WHOLESALE_STARTER) {
                $company->setBronzeCoverLetterPrinted(TRUE);
            }
        }

        if ($customer->isRetail()) {
            // print coverLetter for bronze/normal customer only
            if ($item->getId() != Package::PACKAGE_BRONZE) {
                $company->setBronzeCoverLetterPrinted(TRUE);
            }
        }

        $company->setMACoverLetterPrinted(TRUE);
        $company->setMAPrinted(TRUE);

        if ($item->getId() == Package::PACKAGE_COMPREHENSIVE
            || $item->getId() == Package::PACKAGE_ULTIMATE
            || $item->getId() == Package::PACKAGE_GOLD
            || $item->getId() == Package::PACKAGE_PLATINUM
            || $item->getId() == Package::PACKAGE_DIAMOND
            || $item->getId() == Package::PACKAGE_WHOLESALE_PROFESSIONAL
        ) {
            $company->setMACoverLetterPrinted(FALSE);
            $company->setMAPrinted(FALSE);
        }
    }
}
