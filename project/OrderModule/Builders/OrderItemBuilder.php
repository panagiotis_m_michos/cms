<?php

namespace OrderModule\Builders;

use BasketModule\Services\PriceCalculator;
use DateTime;
use Entities\Company;
use Entities\Order;
use Entities\OrderItem;
use Product;

class OrderItemBuilder
{
    /**
     * @var Order
     */
    private $order;

    /**
     * @var bool
     */
    private $isRefund = FALSE;

    /**
     * @var Product
     */
    private $product;

    /**
     * @var bool
     */
    private $isRefunded = FALSE;

    /**
     * @var DateTime
     */
    private $dtExported;

    /**
     * @var float
     */
    private $markup;

    /**
     * @var Company
     */
    private $company;

    /**
     * @var bool
     */
    private $isFee = FALSE;

    /**
     * @var bool
     */
    private $incorporationRequired = FALSE;

    /**
     * @var float
     */
    private $totalPrice;

    /**
     * @var string
     */
    private $additional;

    /**
     * @var float
     */
    private $vat;

    /**
     * @var float
     */
    private $subTotal;

    /**
     * @var bool
     */
    private $notApplyVat = FALSE;

    /**
     * @var float
     */
    private $nonVatableValue = 0.0;

    /**
     * @var string
     */
    private $productTitle;

    /**
     * @var int
     */
    private $qty = 1;

    /**
     * @var int
     */
    private $productId;

    /**
     * @var OrderItem
     */
    private $refundedOrderItem;

    /**
     * @var PriceCalculator
     */
    private $priceCalculator;

    /**
     * @param PriceCalculator $priceCalculator
     */
    public function __construct(PriceCalculator $priceCalculator)
    {
        $this->priceCalculator = $priceCalculator;
    }

    /**
     * @param Order $order
     * @return $this
     */
    public function setOrder(Order $order)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * @param bool $isRefund
     * @return $this
     */
    public function setIsRefund($isRefund)
    {
        $this->isRefund = $isRefund;
        return $this;
    }

    /**
     * @param boolean $isRefunded
     * @return $this
     */
    public function setIsRefunded($isRefunded)
    {
        $this->isRefunded = $isRefunded;
        return $this;
    }

    /**
     * @param OrderItem $refundedOrderItem
     * @return $this
     */
    public function setRefundedOrderItem(OrderItem $refundedOrderItem)
    {
        $this->refundedOrderItem = $refundedOrderItem;
        return $this;
    }

    /**
     * @param bool $isFee
     * @return $this
     */
    public function setIsFee($isFee)
    {
        $this->isFee = $isFee;
        return $this;
    }

    /**
     * @param int $productId
     * @return $this
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
        return $this;
    }

    /**
     * @param string $productTitle
     * @return $this
     */
    public function setProductTitle($productTitle)
    {
        $this->productTitle = $productTitle;
        return $this;
    }

    /**
     * @param int $qty
     * @return $this
     */
    public function setQty($qty)
    {
        $this->qty = $qty;
        return $this;
    }

    /**
     * @param bool $notApplyVat
     * @return $this
     */
    public function setNotApplyVat($notApplyVat)
    {
        $this->notApplyVat = $notApplyVat;
        return $this;
    }

    /**
     * @param float $nonVatableValue
     * @return $this
     */
    public function setNonVatableValue($nonVatableValue)
    {
        $this->nonVatableValue = $nonVatableValue;
        return $this;
    }

    /**
     * @param float $subTotal
     * @return $this
     */
    public function setSubTotal($subTotal)
    {
        $this->subTotal = $subTotal;
        return $this;
    }

    /**
     * @param float $vat
     * @return $this
     */
    public function setVat($vat)
    {
        $this->vat = $vat;
        return $this;
    }

    /**
     * @param string $additional
     * @return $this
     */
    public function setAdditional($additional)
    {
        $this->additional = $additional;
        return $this;
    }

    /**
     * @param float $totalPrice
     * @return $this
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;
        return $this;
    }

    /**
     * @param bool $incorporationRequired
     * @return $this
     */
    public function setIncorporationRequired($incorporationRequired)
    {
        $this->incorporationRequired = $incorporationRequired;
        return $this;
    }

    /**
     * @param Company $company
     * @return $this
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;
        return $this;
    }

    /**
     * @param float $markup
     * @return $this
     */
    public function setMarkup($markup)
    {
        $this->markup = $markup;
        return $this;
    }

    /**
     * @param DateTime $dtExported
     * @return $this
     */
    public function setDtExported(DateTime $dtExported = NULL)
    {
        $this->dtExported = $dtExported;
        return $this;
    }

    /**
     * @param Product $product
     * @return $this
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @param Product $product
     * @return $this
     */
    public function setProductWithPrice(Product $product)
    {
        $this->product = $product;
        $price = $this->priceCalculator->calculatePrice([$product]);
        $this->notApplyVat = $product->notApplyVat;
        $this->nonVatableValue = $price->getNonVatable();
        $this->subTotal = $price->getSubtotal();
        $this->totalPrice = $price->getTotal();
        $this->vat = $price->getVat();

        return $this;
    }

    /**
     * @return OrderItem
     */
    public function build()
    {
        $orderItem = new OrderItem($this->order);

        $orderItem->setAdditional($this->additional);
        $orderItem->setCompany($this->company);
        $orderItem->setDtExported($this->dtExported);
        $orderItem->setIncorporationRequired($this->incorporationRequired);
        $orderItem->setIsFee($this->isFee);
        $orderItem->setIsRefund($this->isRefund);
        $orderItem->setIsRefunded($this->isRefunded);
        $orderItem->setMarkup($this->markup);
        $orderItem->setOrder($this->order);
        $orderItem->setIncorporationRequired($this->incorporationRequired);
        $orderItem->setProduct($this->product);
        $orderItem->setQty($this->qty);
        $orderItem->setNotApplyVat($this->notApplyVat);
        $orderItem->setNonVatableValue($this->nonVatableValue);
        $orderItem->setPrice($this->subTotal);
        $orderItem->setSubTotal($this->subTotal);
        $orderItem->setTotalPrice($this->totalPrice);
        $orderItem->setVat($this->vat);
        $orderItem->setProductTitle($this->productTitle ?: "{$this->product->getLongTitle()}");

        if ($this->refundedOrderItem) {
            $orderItem->setRefundedOrderItem($this->refundedOrderItem);
        }

        return $orderItem;
    }
}
