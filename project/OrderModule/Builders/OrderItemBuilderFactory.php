<?php

namespace OrderModule\Builders;

use BasketModule\Services\PriceCalculator;

class OrderItemBuilderFactory
{
    /**
     * @var PriceCalculator
     */
    private $priceCalculator;

    /**
     * @param PriceCalculator $priceCalculator
     */
    public function __construct(PriceCalculator $priceCalculator)
    {
        $this->priceCalculator = $priceCalculator;
    }

    /**
     * @return OrderItemBuilder
     */
    public function createBuilder()
    {
        return new OrderItemBuilder($this->priceCalculator);
    }
}
