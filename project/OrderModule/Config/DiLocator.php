<?php

namespace OrderModule\Config;

class DiLocator
{
    const CONVERTERS_ORDER_CONVERTER = 'order_module.converters.order_converter';
    const FPDF_INVOICE_GENERATOR = 'order_module.fpdf.invoice_generator';
    const ORDER_ITEMS_PROCESSOR = 'order_module.processors.order_items_processor';
    const UPDATE_ORDER_ITEMS_DETAILS_LISTENER = 'order_module.listeners.update_order_items_details_listener';
}
