<?php

namespace OrderModule\Resolvers;

use Entities\Company;
use Entities\Customer;
use Package;
use Services\CompanyService;

class PrintedDocumentResolver
{
    /**
     * @var string[]
     */
    private static $certificatePrintedPackages = [
        Package::PACKAGE_BASIC,
        Package::PACKAGE_DIGITAL,
        Package::PACKAGE_CONTRACTOR,
        Package::PACKAGE_BRONZE,
        Package::PACKAGE_INTERNATIONAL,
        Package::PACKAGE_RESERVE_COMPANY_NAME,
        Package::PACKAGE_WHOLESALE_STARTER,
    ];

    /**
     * @var string[]
     */
    private static $bronzeCoverLetterPrintedPackages = [
        Package::PACKAGE_BASIC,
        Package::PACKAGE_DIGITAL,
        Package::PACKAGE_CONTRACTOR,
        Package::PACKAGE_INTERNATIONAL,
        Package::PACKAGE_RESERVE_COMPANY_NAME
    ];

    /**
     * @var string[]
     */
    private static $maNonPrintedPackages = [
        Package::PACKAGE_COMPREHENSIVE,
        Package::PACKAGE_ULTIMATE,
        Package::PACKAGE_GOLD,
        Package::PACKAGE_PLATINUM,
        Package::PACKAGE_DIAMOND,
        Package::PACKAGE_WHOLESALE_PROFESSIONAL,
    ];

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @param CompanyService $companyService
     */
    public function __construct(CompanyService $companyService)
    {
        $this->companyService = $companyService;
    }

    /**
     * @param int $packageId
     * @param Customer $customer
     * @param Company $company
     */
    public function resolveDocuments($packageId, Customer $customer, Company $company)
    {
        $company->setIsCertificatePrinted(in_array($packageId, self::$certificatePrintedPackages));

        $printedBronzeCoverLetter = in_array($packageId, self::$bronzeCoverLetterPrintedPackages)
            || ($customer->isWholesale() && $packageId == Package::PACKAGE_WHOLESALE_STARTER)
            || ($customer->isRetail() && $packageId != Package::PACKAGE_BRONZE);
        $company->setIsBronzeCoverLetterPrinted($printedBronzeCoverLetter);

        $printedMa = !in_array($packageId, self::$maNonPrintedPackages);
        $company->setIsMaPrinted($printedMa);
        $company->setIsMaCoverLetterPrinted($printedMa);

        $this->companyService->saveCompany($company);
    }
}
