<?php

namespace OrderModule\Listeners;

use Dispatcher\Events\CompanyEvent;
use EventLocator;
use Nette\Object;
use Services\OrderItemService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UpdateOrderItemsDetailsListener extends Object implements EventSubscriberInterface
{
    /**
     * @var OrderItemService
     */
    private $orderItemService;

    /**
     * @param OrderItemService $orderItemService
     */
    public function __construct(OrderItemService $orderItemService)
    {
        $this->orderItemService = $orderItemService;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            EventLocator::COMPANY_INCORPORATED => 'onCompanyIncorporated',
        ];
    }

    /**
     * @param CompanyEvent $event
     */
    public function onCompanyIncorporated(CompanyEvent $event)
    {
        $company = $event->getCompany();
        $orderItems = $this->orderItemService->getOrderItemsByCompany($company);

        foreach ($orderItems as $orderItem) {
            $orderItem->setCompany($company);
            $this->orderItemService->save($orderItem);
        }
    }
}
