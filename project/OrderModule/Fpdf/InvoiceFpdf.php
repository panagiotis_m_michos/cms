<?php

namespace OrderModule\Fpdf;

use FPDF;
use Order as OldOrder;

class InvoiceFpdf extends FPDF
{
    /**
     * @var OldOrder
     */
    private $order;

    /**
     * @param OldOrder $order
     */
    public function __construct(OldOrder $order)
    {
        parent::FPDF();

        $this->order = $order;
    }

    public function Header()
    {
        $this->SetFont('Arial', '', 16);
        $this->SetTextColor(28, 141, 190);

        $y = $this->GetY();
        $this->SetX(135);
        $this->Cell(60, 5, 'Invoice', 0, 0, 'R');
        $this->SetY($y);

        $this->Image(COMPONENT_DIR . '/ui_server/imgs/cms-logo-text.png', 10, $y, 75);

        $this->setY(35);

        $this->SetFont('Arial', '', 8);
        $this->MultiCell(160, 4, "20-22 Wenlock Road, London, N1 7GU \nTel 0207 608 5500 | www.companiesmadesimple.com");
        $this->SetTextColor(0, 0, 0);
        $this->Ln();
        $this->Ln();
    }

    public function Footer()
    {
        $this->SetY(-30);
        $this->SetFont('Arial', '', 8);
        $this->SetTextColor(28, 141, 190);
        $this->MultiCell(190, 4, "Made Simple Group Ltd \n Registered Office: 20-22 Wenlock Road, London, N1 7GU \n Registered in England No: 4214713. VAT Number: 820956327", 0, 'R');
        $this->Ln();
        $this->SetFont('Arial', 'I', 11);
        $this->MultiCell(190, 5, "Making business simple ...", 0, 'R');
        //Page number
        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }

    public function addBody()
    {
        $this->AliasNbPages();
        $this->SetAutoPageBreak(0);

        $height = 5;
        $fontSize = 11;

        $this->AddPage();

        $y = $this->GetY();
        $this->SetFont('Arial', 'B', $fontSize);
        $this->MultiCell(105, $height, 'Address');

        $this->SetFont('Arial', '', $fontSize);
        $this->MultiCell(105, $height, $this->order->customerName);
        $address = explode('-', $this->order->customerAddress);
        foreach ($address as $v) {
            $this->MultiCell(105, $height, $v);
        }
        $this->Ln();

        $this->SetFont('Arial', 'B', $fontSize);
        $this->MultiCell(105, $height, 'Contact number');
        $this->SetFont('Arial', '', $fontSize);
        $this->MultiCell(105, $height, $this->order->customerPhone);

        $this->SetY($y);
        $this->SetFont('Arial', 'B', $fontSize);
        $this->SetX(130);
        $this->MultiCell(55, $height, 'Document type');
        $this->SetFont('Arial', '', $fontSize);
        $this->SetX(130);
        $this->MultiCell(55, $height, 'Invoice');
        $this->Ln();

        $this->SetFont('Arial', 'B', $fontSize);
        $this->SetX(130);
        $this->MultiCell(55, $height, 'Date (tax point)');
        $this->SetFont('Arial', '', $fontSize);
        $this->SetX(130);
        $this->MultiCell(55, $height, date('d-m-Y', strtotime($this->order->dtc)));
        $this->Ln();

        $this->SetFont('Arial', 'B', $fontSize);
        $this->SetX(130);
        $this->MultiCell(55, $height, 'Document number');
        $this->SetFont('Arial', '', $fontSize);
        $this->SetX(130);
        $this->MultiCell(55, $height, $this->order->orderId);
        $this->Ln();

        $this->SetFont('Arial', 'B', $fontSize);
        $this->SetX(130);
        $this->MultiCell(55, $height, 'Purchase order number');
        $this->SetFont('Arial', '', $fontSize);
        $this->SetX(130);
        $this->MultiCell(55, $height, $this->order->orderId);
        $this->Ln();

        $this->Ln();
        $this->Ln();

        // --- cells' width ---
        $c1w = 20;
        $c2w = 125;
        $c3w = 15;
        $c4w = 30;

        $y = $this->GetY();
        $x = $this->GetX();

        $this->SetFont('Arial', 'B', $fontSize);
        $this->MultiCell($c1w, $height, 'Code', 'B', 'C');
        $x += $c1w;
        $this->SetXY($x, $y);
        $this->MultiCell($c2w, $height, 'Product description', 'B');
        $x += $c2w;
        $this->SetXY($x, $y);
        $this->MultiCell($c3w, $height, 'Qty', 'B', 'C');
        $x += $c3w;
        $this->SetXY($x, $y);
        $this->MultiCell($c4w, $height, 'Each', 'B', 'R');

        $this->SetFont('Arial', '', $fontSize);
        $qty = 0;
        foreach($this->order->items as $item) {
            // --- add new page if needed ---
            $cellsWidth = array($c1w, $c2w, $c3w, $c4w);
            $texts      = array($item->orderItemId, $item->productTitle, $item->qty, $item->price);
            $this->setMultiCellPage($cellsWidth, $texts, $height, 270);

            $x = $this->GetX();
            $y = $this->GetY();

            // --- first cell ---
            $this->MultiCell($c1w, $height, $texts[0], 0, 'C');

            $newLine = $this->GetY();

            // --- second cell ---
            $x += $c1w;
            $this->SetXY($x, $y);
            $this->MultiCell($c2w, $height, $texts[1]);

            if ($newLine < $this->GetY()) { $newLine = $this->GetY(); }

            // --- third cell ---
            $x += $c2w;
            $this->SetXY($x, $y);
            $this->MultiCell($c3w, $height, $texts[2], 0, 'C');
            $qty += $texts[2];

            if ($newLine < $this->GetY()) { $newLine = $this->GetY(); }

            // --- fourth cell ---
            $x += $c3w;
            $this->SetXY($x, $y);
            $this->MultiCell($c4w, $height, utf8_decode('£').number_format($texts[3], 2, '.', ','), 0, 'R');

            if ($newLine < $this->GetY()) { $newLine = $this->GetY(); }

            $this->SetXY($x, $newLine);   // set Y position (according to highest cell)
            $this->Ln(0);                 // new line for next row
        }

        // --- total ---
        $c1w = 130;
        $c2w = 10;
        $c3w = 25;
        $c4w = 25;

        $y = $this->GetY();
        $x = $this->GetX();
        $this->SetFont('Arial', 'B', $fontSize);
        $this->MultiCell($c1w, $height, 'Total Net', 'T', 'L');
        $x += $c1w;
        $this->SetXY($x, $y);
        $this->MultiCell($c2w, $height, '', 'T', 'C');
        $x += $c2w;
        $this->SetXY($x, $y);
        $this->MultiCell($c3w, $height, '', 'T', 'C');
        $x += $c3w;
        $this->SetXY($x, $y);
        $this->MultiCell($c4w, $height, utf8_decode('£').number_format($this->order->getRealSubtotal(), 2, '.', ','), 'T', 'R');

        if ($this->order->discount > 0) {
            $y = $this->GetY();
            $x = $this->GetX();
            $this->SetFont('Arial', 'B', $fontSize);
            $this->MultiCell($c1w, $height, 'Discount', 0, 'L');
            $x += $c1w;
            $this->SetXY($x, $y);
            $this->MultiCell($c2w, $height, '', 0, 'C');
            $x += $c2w;
            $this->SetXY($x, $y);
            $this->MultiCell($c3w, $height, '', 0);
            $x += $c3w;
            $this->SetXY($x, $y);
            $this->MultiCell($c4w, $height, '-' . utf8_decode('£').number_format($this->order->discount, 2, '.', ','), 0, 'R');
        }

        $y = $this->GetY();
        $x = $this->GetX();
        $this->SetFont('Arial', 'B', $fontSize);
        $this->MultiCell($c1w, $height, 'VAT', 0, 'L');
        $x += $c1w;
        $this->SetXY($x, $y);
        $this->MultiCell($c2w, $height, '', 0, 'C');
        $x += $c2w;
        $this->SetXY($x, $y);
        $this->MultiCell($c3w, $height, '', 0);
        $x += $c3w;
        $this->SetXY($x, $y);
        $this->MultiCell($c4w, $height, utf8_decode('£').number_format($this->order->vat, 2, '.', ','), 0, 'R');

        if ($this->order->credit > 0) {
            $y = $this->GetY();
            $x = $this->GetX();
            $this->SetFont('Arial', 'B', $fontSize);
            $this->MultiCell($c1w, $height, 'Credit', 0, 'L');
            $x += $c1w;
            $this->SetXY($x, $y);
            $this->MultiCell($c2w, $height, '', 0, 'C');
            $x += $c2w;
            $this->SetXY($x, $y);
            $this->MultiCell($c3w, $height, '', 0);
            $x += $c3w;
            $this->SetXY($x, $y);
            $this->MultiCell($c4w, $height, '-' . utf8_decode('£') . number_format($this->order->credit, 2, '.', ','), 0, 'R');
            if ($this->order->isRefunded) {
                $yLine = $y + 2.5;
                $this->line($x, $yLine, $x + $c4w, $yLine);
            }
        }

        $y = $this->GetY();
        $x = $this->GetX();
        $this->SetFont('Arial', 'B', $fontSize);
        $this->MultiCell($c1w, $height, 'Total', 0, 'L');
        $x += $c1w;
        $this->SetXY($x, $y);
        $this->MultiCell($c2w, $height, '', 0, 'C');
        $x += $c2w;
        $this->SetXY($x, $y);
        $this->MultiCell($c3w, $height, '', 0);
        $x += $c3w;
        $this->SetXY($x, $y);
        $this->MultiCell($c4w, $height, utf8_decode('£').number_format($this->order->total, 2, '.', ','), 0, 'R');

        if ($this->order->isRefunded) {
            //strike through total price
            $yLine = $y + 2.5;
            $this->line($x, $yLine, $x + $c4w, $yLine);

            $y = $this->GetY();
            $x = $this->GetX();

            $this->SetFont('Arial', 'B', $fontSize);
            $this->MultiCell($c1w, $height, 'Money Refunded', 'T', 'L');
            $x += $c1w;
            $this->SetXY($x, $y);
            $this->MultiCell($c2w, $height, '', 'T', 'C');
            $x += $c2w;
            $this->SetXY($x, $y);
            $this->MultiCell($c3w, $height, '', 'T');
            $x += $c3w;
            $this->SetXY($x, $y);
            $this->MultiCell($c4w, $height, utf8_decode('£').number_format($this->order->refundValue, 2, '.', ','), 'T', 'R');

            $y = $this->GetY();
            $x = $this->GetX();

            $this->SetFont('Arial', 'B', $fontSize);
            $this->MultiCell($c1w, $height, 'Credit Refunded', 0, 'L');
            $x += $c1w;
            $this->SetXY($x, $y);
            $this->MultiCell($c2w, $height, '', 0, 'C');
            $x += $c2w;
            $this->SetXY($x, $y);
            $this->MultiCell($c3w, $height, '', 0);
            $x += $c3w;
            $this->SetXY($x, $y);
            $this->MultiCell($c4w, $height, utf8_decode('£').number_format($this->order->refundCreditValue, 2, '.', ','), 0, 'R');

            if ($this->order->hasAdminFee()) {
                $y = $this->GetY();
                $x = $this->GetX();
                $this->SetFont('Arial', 'B', $fontSize);
                $this->MultiCell($c1w, $height, 'Admin Fee', 0, 'L');
                $x += $c1w;
                $this->SetXY($x, $y);
                $this->MultiCell($c2w, $height, '', 0, 'C');
                $x += $c2w;
                $this->SetXY($x, $y);
                $this->MultiCell($c3w, $height, '', 0);
                $x += $c3w;
                $this->SetXY($x, $y);
                $this->MultiCell($c4w, $height, utf8_decode('£').number_format($this->order->getAdminFee(), 2, '.', ','), 0, 'R');
            }
        }
    }

    /**
     * @param string $name
     * @param string $dest
     * @return mixed
     */
    public function Output($name = '', $dest = '')
    {
        $this->addBody();

        return parent::Output($name, $dest);
    }

    /**
     * getNbLines
     *
     * returns number of lines from passed mutlicell
     * code is taken from:
     * http://www.fpdf.de/downloads/addons/3/
     *
     * @param float $w width of multicell
     * @param string $txt text
     * @access public
     * @return integer
     */
    private function getNbLines($w, $txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r", '', $txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }

    /**
     * setMultiCellPage
     *
     * helper method - checks all passed multicells and their text.
     * Then it calculates if needs to add new page
     *
     * @param array $cells width of multi cells
     * @param array $texts text inside multi cells
     * @param float $height height of multi cells
     * @param int $endPage
     */
    private function setMultiCellPage(array $cells, array $texts, $height, $endPage)
    {
        $y = $this->GetY();
        $maxHeight = 0;
        for($x = 0; $x < count($cells); $x++) {
            $currHeight = $this->getNbLines($cells[$x], $texts[$x]);
            if ($currHeight > $maxHeight) {
                $maxHeight = $currHeight;
            }
        }

        if (($y + ($maxHeight * $height)) > $endPage) {
            $this->AddPage();
        }
    }
}
