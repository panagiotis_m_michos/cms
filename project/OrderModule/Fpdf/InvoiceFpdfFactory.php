<?php

namespace OrderModule\Fpdf;

use Order as OldOrder;

class InvoiceFpdfFactory
{
    /**
     * @param OldOrder $order
     * @return InvoiceFpdf
     */
    public function create(OldOrder $order)
    {
        return new InvoiceFpdf($order);
    }
}
