<?php

namespace OrderModule\Fpdf;

use Entities\Order;
use OrderModule\Converters\OrderConverter;
use OrderModule\Pdf\IInvoiceGenerator;

class InvoiceGenerator implements IInvoiceGenerator
{
    /**
     * @var InvoiceFpdfFactory
     */
    private $factory;

    /**
     * @var OrderConverter
     */
    private $orderConverter;

    /**
     * @param InvoiceFpdfFactory $factory
     * @param OrderConverter $orderConverter
     */
    public function __construct(InvoiceFpdfFactory $factory, OrderConverter $orderConverter)
    {
        $this->factory = $factory;
        $this->orderConverter = $orderConverter;
    }

    /**
     * @param Order $order
     * @return string
     */
    public function getContents(Order $order)
    {
        $oldOrder = $this->orderConverter->toOldOrder($order);

        return $this->factory->create($oldOrder)->Output('', 'S');
    }
}
