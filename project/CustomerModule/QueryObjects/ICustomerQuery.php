<?php
namespace CustomerModule\QueryObjects;

use DateTimeInterface;

interface ICustomerQuery
{
    public function withId($customerId);

    /**
     * @param $email
     * @return $this
     */
    public function withEmail($email);

    /**
     * @return $this
     */
    public function active();

    /**
     * @param string $roleId
     * @return $this
     */
    public function byRole($roleId);

    /**
     * @param array $productIds
     * @return $this
     */
    public function withSomeProducts(array $productIds);

    /**
     * @return $this
     */
    public function withoutBankLeads();

    /**
     * @param DateTimeInterface $date
     * @return $this
     */
    public function withCompanyIncorporationOn(DateTimeInterface $date);

    /**
     * @param int $emailId
     * @param DateTimeInterface $notReceivedAfter
     * @return $this
     */
    public function didNotReceiveEmailRecently($emailId, DateTimeInterface $notReceivedAfter);

    /**
     * @param int $emailId
     * @param DateTimeInterface $start
     * @param DateTimeInterface $end
     * @return $this
     */
    public function receivedEmailBetween($emailId, DateTimeInterface $start, DateTimeInterface $end);

    /**
     * @param int $countryId
     * @return $this
     */
    public function byCustomerCountry($countryId);
}
