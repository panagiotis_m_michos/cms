<?php

namespace CustomerModule\QueryObjects;

use BankingModule\Entities\CompanyCustomer;
use BankingModule\Entities\TsbLead;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\Internal\Hydration\IterableResult;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Entities\CompanyHouse\FormSubmission;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation;
use Entities\Customer;
use Kdyby;
use Kdyby\Doctrine\QueryObject;
use Kdyby\Persistence\Queryable;
use MailgunModule\Entities\MailgunEvent;

class CustomerQuery extends QueryObject implements ICustomerQuery
{
    /**
     * @var callable[]
     */
    private $modifiers = [];

    public function withId($customerId)
    {
        $this->modifiers[] = function (QueryBuilder $qb) use ($customerId) {
            $qb
                ->andWhere('c.customerId = :customerId')
                ->setParameter('customerId', $customerId);
        };

        return $this;
    }

    /**
     * @param $email
     * @return $this
     */
    public function withEmail($email)
    {
        $this->modifiers[] = function (QueryBuilder $qb) use ($email) {
            $qb
                ->andWhere('c.email = :email')
                ->setParameter('email', $email);
        };

        return $this;
    }

    /**
     * @return $this
     */
    public function active()
    {
        $this->modifiers[] = function (QueryBuilder $qb) {
            $qb
                ->andWhere('c.statusId = :statusActive')
                ->setParameter('statusActive', Customer::STATUS_VALIDATED);
        };

        return $this;
    }

    /**
     * @param string $roleId
     * @return $this
     */
    public function byRole($roleId)
    {
        $this->modifiers[] = function (QueryBuilder $qb) use ($roleId) {
            $qb
                ->andWhere('c.roleId = :roleId')
                ->setParameter('roleId', $roleId);
        };

        return $this;
    }

    /**
     * @param array $productIds
     * @return $this
     */
    public function withSomeProducts(array $productIds)
    {
        $this->modifiers[] = function (QueryBuilder $qb) use ($productIds) {
            $qb
                ->leftJoin('c.orders', 'o')
                ->leftJoin('o.items', 'oi')
                ->andWhere('oi.productId IN (:productIds)')
                ->andWhere('oi.refundedOrderItem IS NULL')
                ->andWhere('oi.isRefunded = FALSE OR oi.isRefunded IS NULL')
                ->andWhere('oi.isRefund = FALSE OR oi.isRefund IS NULL')
                ->setParameter('productIds', $productIds);
        };

        return $this;
    }

    /**
     * @return $this
     */
    public function withoutBankLeads()
    {
        $this->modifiers[] = function (QueryBuilder $qb) {
            $qb
                ->addSelect('SUM(CASE WHEN (cc IS NOT NULL) THEN 1 ELSE 0 END) AS HIDDEN numberOfLeads')
                ->leftJoin('c.companies', 'co')
                ->leftJoin(CompanyCustomer::class, 'cc', 'WITH', 'cc.company = co')
                ->groupBy('c')
                ->andHaving('numberOfLeads = 0');
        };

        return $this;
    }

    /**
     * @param DateTimeInterface $date
     * @return $this
     */
    public function withCompanyIncorporationOn(DateTimeInterface $date)
    {
        $this->modifiers[] = function (QueryBuilder $qb) use ($date) {
            $qb
                ->leftJoin('c.companies', 'co1')
                ->andWhere('co1.incorporationDate = :date')
                ->setParameter('date', $date->format('Y-m-d'));
        };

        return $this;
    }

    /**
     * @param int $emailId
     * @param DateTimeInterface $notReceivedAfter
     * @return $this
     */
    public function didNotReceiveEmailRecently($emailId, DateTimeInterface $notReceivedAfter)
    {
        $this->modifiers[] = function (QueryBuilder $qb) use ($emailId, $notReceivedAfter) {
            $qb
                ->addSelect('SUM(CASE WHEN (me.emailTemplateId = :emailId AND me.createdAt > :notReceivedAfter) THEN 1 ELSE 0 END) AS HIDDEN numberOfReceivedEmails')
                ->leftJoin(MailgunEvent::class, 'me', 'WITH', 'me.customer = c')
                ->setParameter('emailId', $emailId)
                ->setParameter('notReceivedAfter', $notReceivedAfter)
                ->andHaving('numberOfReceivedEmails = 0');
        };

        return $this;
    }

    /**
     * @param int $emailId
     * @param DateTimeInterface $start
     * @param DateTimeInterface $end
     * @return $this
     */
    public function receivedEmailBetween($emailId, DateTimeInterface $start, DateTimeInterface $end)
    {
        $this->modifiers[] = function (QueryBuilder $qb) use ($emailId, $start, $end) {
            $qb
                ->leftJoin(MailgunEvent::class, 'me2', 'WITH', 'me2.customer = c')
                ->andWhere('me2.emailTemplateId = :emailId')
                ->andWhere('me2.createdAt BETWEEN :start AND :end')
                ->setParameter('emailId', $emailId)
                ->setParameter('start', $start)
                ->setParameter('end', $end);
        };

        return $this;
    }

    /**
     * @param int $countryId
     * @return $this
     */
    public function byCustomerCountry($countryId)
    {
        $this->modifiers[] = function (QueryBuilder $qb) use ($countryId) {
            $qb
                ->andWhere('c.countryId = :countryId')
                ->setParameter('countryId', $countryId);
        };

        return $this;
    }

    /**
     * @param Queryable $repository
     * @return IterableResult
     */
    public function iterate(Queryable $repository)
    {
        return $this->doCreateQuery($repository)
            ->groupBy('c.customerId')
            ->distinct()
            ->getQuery()
            ->iterate(NULL, AbstractQuery::HYDRATE_SIMPLEOBJECT);

    }

    /**
     * @param Queryable $repository
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function exists(Queryable $repository)
    {
        $qb = $repository->createQueryBuilder()
            ->select('1')
            ->from(Customer::class, 'c');

        foreach ($this->modifiers as $modifier) {
            $modifier($qb);
        }

        $query = $qb->getQuery();
        $query->useQueryCache(FALSE);
        $query->useResultCache(FALSE);

        $result = (bool) $query->getScalarResult();
        $query->free();

        return $result;
    }

    /**
     * @param Queryable $repository
     * @return Query|QueryBuilder
     */
    protected function doCreateQuery(Queryable $repository)
    {
        $qb = $repository->createQueryBuilder()
            ->select('c')
            ->from(Customer::class, 'c');

        foreach ($this->modifiers as $modifier) {
            $modifier($qb);
        }

        return $qb;
    }

    /**
     * @param Queryable $repository
     * @return Kdyby\Doctrine\QueryBuilder
     */
    protected function doCreateCountQuery(Queryable $repository)
    {
        $qb = $repository->createQueryBuilder()
            ->select('COUNT(c)')
            ->from(Customer::class, 'c');

        foreach ($this->modifiers as $modifier) {
            $modifier($qb);
        }

        return $qb;
    }
}
