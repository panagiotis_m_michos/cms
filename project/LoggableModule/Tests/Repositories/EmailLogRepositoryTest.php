<?php

namespace LoggableModule\Tests\Repositories;

use DateTime;
use Emailers\ServicesEmailer;
use EntityHelper;
use FEmail;
use LoggableModule\Repositories\EmailLogRepository;
use tests\helpers\EmailHelper;
use Tests\Helpers\ObjectHelper;
use Utils\Date;

class EmailLogRepositoryTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var EmailLogRepository
     */
    private $object;

    const EMAIL_1_ID = ServicesEmailer::SERVICES_AUTO_RENEWABLE_EXPIRES_IN_7_EMAIL;
    const EMAIL_2_ID = ServicesEmailer::SERVICES_AUTO_RENEWAL_FAILED;

    public function setUp()
    {
        EntityHelper::emptyTables([TBL_CUSTOMERS, TBL_EMAIL_LOGS]);
        $this->object = EntityHelper::getService('loggable_module.repositories.email_log_repository');
        $this->customer1 = ObjectHelper::createCustomer(TEST_EMAIL1);
        $this->customer2 = ObjectHelper::createCustomer(TEST_EMAIL2);
        $logs[] = EmailHelper::createLog($this->customer1, new FEmail(self::EMAIL_1_ID), new DateTime('-2 day'));
        $logs[] = EmailHelper::createLog($this->customer1, new FEmail(self::EMAIL_1_ID), new DateTime('-3 days'));
        $logs[] = EmailHelper::createLog($this->customer1, new FEmail(self::EMAIL_1_ID), new DateTime('-7 days'));
        $logs[] = EmailHelper::createLog($this->customer2, new FEmail(self::EMAIL_1_ID), new DateTime('-10 day 00:00:00'));
        EntityHelper::save(array_merge([$this->customer1, $this->customer2], $logs));
    }

    public function testEmailsSent()
    {
        $this->assertEquals(3, $this->object->getEmailsSent($this->customer1, [self::EMAIL_1_ID]));
        $this->assertEquals(1, $this->object->getEmailsSent($this->customer2, [self::EMAIL_1_ID]));
        $this->assertEquals(0, $this->object->getEmailsSent($this->customer2, [self::EMAIL_2_ID]));
    }

    public function testHasEmailsSentAfter()
    {
        $this->assertTrue($this->object->hasEmailsSentAfter($this->customer1, [self::EMAIL_1_ID], new Date('-2 days')));
        $this->assertTrue($this->object->hasEmailsSentAfter($this->customer1, [self::EMAIL_1_ID], new Date('-10 days')));
        $this->assertTrue($this->object->hasEmailsSentAfter($this->customer2, [self::EMAIL_1_ID], new Date('-11 days')));
        $this->assertFalse($this->object->hasEmailsSentAfter($this->customer2, [self::EMAIL_1_ID], new Date('-10 days')));
        $this->assertFalse($this->object->hasEmailsSentAfter($this->customer2, [self::EMAIL_1_ID], new Date('-9 days')));
        $this->assertFalse($this->object->hasEmailsSentAfter($this->customer1, [self::EMAIL_1_ID], new Date('-1 days')));
        $this->assertFalse($this->object->hasEmailsSentAfter($this->customer1, [self::EMAIL_2_ID], new Date('-2 days')));
    }
}