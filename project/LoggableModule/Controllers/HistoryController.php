<?php

namespace LoggableModule\Controllers;

use FTemplate;
use LoggableModule\HistoryDatagrid;
use LoggableModule\Repositories\LoggableRepository;

class HistoryController
{
    /**
     * @var LoggableRepository
     */
    private $loggableRepository;

    /**
     * @var FTemplate
     */
    private $template;

    /**
     * @param FTemplate $template
     * @param LoggableRepository $loggableRepository
     */
    public function __construct(FTemplate $template, LoggableRepository $loggableRepository)
    {
        $this->loggableRepository = $loggableRepository;
        $this->template = $template;
    }

    public function renderList()
    {
        $datasource = $this->loggableRepository->getHistoryDataSource();
        $this->template->datagrid = new HistoryDatagrid($datasource, 'id');
    }
}
