<?php

namespace LoggableModule\Services;

use DoctrineDataSource;
use Entities\Customer;
use LoggableModule\Repositories\EmailLogRepository;

class EmailLogsService
{

    /**
     * @var EmailLogRepository
     */
    protected $repository;

    /**
     * @param EmailLogRepository $repository
     */
    public function __construct(EmailLogRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Customer $customer
     * @return DoctrineDataSource
     */
    public function getEmailsLogDataSource(Customer $customer = NULL)
    {
        $qb = $this->repository->getEmailsLogQueryBuilder($customer);
        return new DoctrineDataSource($qb);
    }
}