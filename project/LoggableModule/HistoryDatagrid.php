<?php

namespace LoggableModule;

use DataGrid;
use DataGridTextColumn;
use DateTime;
use dibi;
use DibiRow;
use FUser;
use Html;
use LoggableModule\Entities\LogEntry;

class HistoryDatagrid extends DataGrid
{
    /**
     * @var array 
     */
    private $customers = array();

    /**
     * @var array 
     */
    private $users = array();

    /**
     * @return void
     */
    protected function init()
    {
        $this->disableOrder();

        $this->addTextFilter('objectClass', 'FQC name', 'h');
        $this->addTextFilter('objectId', 'Object Id', 'h');

        // --- columns ---
        $this->addDateColumn('loggedAt', 'Date', 40, 'd/m/Y H:i:s');
        $this->addTextColumn('action', 'Action', 30);
        $this->addTextColumn('version', 'Version', 30);
        $this->addTextColumn('userId', 'User', 30)->addCallback(array($this, 'Callback_user'));
        $this->addTextColumn('data', 'Details', 60)->addCallback(array($this, 'Callback_data'));
    }

    /**
     * @param DataGridTextColumn $column
     * @param string $text
     * @param LogEntry $logEntry
     * @return string
     */
    public function Callback_user(DataGridTextColumn $column, $text, LogEntry $logEntry)
    {
        $userId = $text;
        if ($userId) {
            // caches user
            if (empty($this->users[$userId])) {
                $user = FUser::create($userId);
                $this->users[$userId] = $user->login;
            }

            return $this->users[$userId];
        }

        $customerId = $logEntry->getCustomerId();
        if ($customerId) {
            // caches customer
            if (empty($this->customers[$customerId])) {
                $customerEmail = dibi::select('email')->from(TBL_CUSTOMERS)->where('customerId=%i', $customerId)->fetchSingle();
                $this->customers[$customerId] = $customerEmail ? $customerEmail : 'unknown';
            }

            return $this->customers[$customerId];
        }

        return $logEntry->getUsername();
    }

    /**
     * @param DataGridTextColumn $column
     * @param string $text
     * @param LogEntry $logEntry
     * @return Html
     */
    public function Callback_data(DataGridTextColumn $column, $text, LogEntry $logEntry)
    {
        $data = $text;

        $table = Html::el('table')->width(200)->class('grid')->cellpadding(0)->cellspacing(0);
        $tr = $table->create('tr');
        foreach ($data as $key => $value) {

            if ($value instanceof DateTime) {
                $value = $value->format('d/m/Y H:i:s');
            } elseif ($value === TRUE) {
                $value = 'true';
            } elseif ($value === FALSE) {
                $value = 'false';
            } elseif ($value === NULL) {
                $value = 'null';
            }

            $tr = $table->create('tr');
            $tr->create('th')->setText($key);
            $tr->create('td')->setText(is_array($value) ? implode(', ', $value) : $value);
        }

        $a = Html::el('a')->href('javascript:;')->title($table)->class('tooltip')->setText('view');
        return $a;
    }

}
