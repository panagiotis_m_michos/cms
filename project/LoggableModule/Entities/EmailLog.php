<?php

namespace LoggableModule\Entities;

use DateTime;
use Doctrine\ORM\Mapping as Orm;
use Entities\Customer;
use FEmail;
use Gedmo\Mapping\Annotation as Gedmo;
use Nette\Object;

/**
 * @Orm\Table(name="cms2_email_logs")
 * @Orm\Entity(repositoryClass="LoggableModule\Repositories\EmailLogRepository")
 */
class EmailLog extends Object
{
    /**
     * @var integer
     *
     * @Orm\Column(name="emailLogId", type="integer", nullable=false)
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $emailLogId;
    
    /**
     * @var string
     *
     * @Orm\Column(name="emailName", type="string", length=255, nullable=true)
     */
    private $emailName;
    
    /**
     * @var string $emailNodeId
     *
     * @Orm\Column(name="emailNodeId", type="integer", nullable=false)
     */
    private $emailNodeId;
    
    /**
     * @var string
     *
     * @Orm\Column(name="emailFrom", type="json_array")
     */
    private $emailFrom;
    
    /**
     * @var string $emailTo
     *
     * @Orm\Column(name="emailTo", type="json_array")
     */
    private $emailTo;
    
    /**
     * @var string
     *
     * @Orm\Column(name="emailSubject", type="string", length=255, nullable=true)
     */
    private $emailSubject;
    
    /**
     * @var string
     *
     * @Orm\Column(name="emailBody", type="text", nullable=true)
     */
    private $emailBody;
    
    /**
     * @var string
     *
     * @Orm\Column(name="attachment", type="boolean", nullable=false)
     */
    private $attachment;
    
    /**
     * @var Customer
     *
     * @Orm\ManyToOne(targetEntity="Entities\Customer")
     * @Orm\JoinColumns({
     *   @Orm\JoinColumn(name="customerId", referencedColumnName="customerId")
     * })
     */
    private $customer;
    
    /**
     * @var DateTime
     * 
     * @Orm\Column(name="dtc", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;
    
    /**
     * @var DateTime
     *
     * @Orm\Column(name="dtm", type="datetime")
     * @Gedmo\Timestampable(on="create", on="update")
     */
    private $dtm;

    /**
     * @param FEmail $email
     * @param Customer $customer
     * @return EmailLog
     */
    public static function fromEmailNode(FEmail $email, Customer $customer)
    {
        $emailLog = new self();
        $emailLog->setEmailName($email->page->title);
        $emailLog->setEmailNodeId($email->getId());
        $emailLog->setEmailFrom($email->getFromArr());
        $emailLog->setCustomer($customer);
        $emailsTo = array_merge($email->getToArr(), $email->getCcArr(), $email->getBccArr());
        $emailLog->setEmailTo($emailsTo);
        $emailLog->setEmailSubject($email->subject);
        $emailLog->setEmailBody($email->body);
        $emailLog->setAttachment(!empty($email->attachments) ? 1 : 0);
        return $emailLog;
    }

    /**
     * @return int
     */
    public function getEmailLogId()
    {
        return $this->emailLogId;
    }

    /**
     * @param int $emailLogId
     */
    public function setEmailLogId($emailLogId)
    {
        $this->emailLogId = $emailLogId;
    }

    /**
     * @return string
     */
    public function getEmailName()
    {
        return $this->emailName;
    }

    /**
     * @param string $emailName
     */
    public function setEmailName($emailName)
    {
        $this->emailName = $emailName;
    }

    /**
     * @return string
     */
    public function getEmailNodeId()
    {
        return $this->emailNodeId;
    }

    /**
     * @param string $emailNodeId
     */
    public function setEmailNodeId($emailNodeId)
    {
        $this->emailNodeId = $emailNodeId;
    }

    /**
     * @return string
     */
    public function getEmailFrom()
    {
        return $this->emailFrom;
    }

    /**
     * @param string $emailFrom
     */
    public function setEmailFrom($emailFrom)
    {
        $this->emailFrom = $emailFrom;
    }

    /**
     * @return string
     */
    public function getEmailTo()
    {
        return $this->emailTo;
    }

    /**
     * @param string $emailTo
     */
    public function setEmailTo($emailTo)
    {
        $this->emailTo = $emailTo;
    }

    /**
     * @return string
     */
    public function getEmailSubject()
    {
        return $this->emailSubject;
    }

    /**
     * @param string $emailSubject
     */
    public function setEmailSubject($emailSubject)
    {
        $this->emailSubject = $emailSubject;
    }

    /**
     * @return string
     */
    public function getEmailBody()
    {
        return $this->emailBody;
    }

    /**
     * @param string $emailBody
     */
    public function setEmailBody($emailBody)
    {
        $this->emailBody = $emailBody;
    }

    /**
     * @return bool
     */
    public function getAttachment()
    {
        return $this->attachment;
    }

    /**
     * @param bool $attachment
     */
    public function setAttachment($attachment)
    {
        $this->attachment = $attachment;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @param DateTime $dtc
     */
    public function setDtc(DateTime $dtc)
    {
        $this->dtc = $dtc;
    }

    /**
     * @return DateTime
     */
    public function getDtm()
    {
        return $this->dtm;
    }

    /**
     * @param DateTime $dtm
     */
    public function setDtm(DateTime $dtm)
    {
        $this->dtm = $dtm;
    }

}