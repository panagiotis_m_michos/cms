<?php

namespace LoggableModule;

use BootstrapModule\Ext\DoctrineExt;
use BootstrapModule\Ext\IExtension;
use DiLocator;
use FApplication;
use FUser;
use Customer as CustomerOld;
use Services\CustomerService;
use Symfony\Component\DependencyInjection\Container;

class LoggableExt implements IExtension
{
    /**
     * @param Container $containerBuilder
     */
    public function load(Container $containerBuilder)
    {
        $reader = $containerBuilder->get(DoctrineExt::ANNOTATION_READER);
        $evm = $containerBuilder->get(DoctrineExt::EVENT_MANAGER);
        $loggableListener = new LoggableListener();
        $loggableListener->setAnnotationReader($reader);
        $evm->addEventSubscriber($loggableListener);
        if (FApplication::isAdmin()) {
            $user = FUser::getSignedIn();
            if ($user) {
                $loggableListener->setUser($user);
            }
        } else {
            if (CustomerOld::isSignedIn()) {
                $customerOld = CustomerOld::getSignedIn();
                /* @var CustomerService $customerService */
                $customerService = $containerBuilder->get(DiLocator::SERVICE_CUSTOMER);
                $customer = $customerService->getCustomerById($customerOld->getId());
                $loggableListener->setCustomer($customer);
            } elseif (defined('GEDMO_LOGGABLE_USERNAME')) {
                $loggableListener->setUsername(GEDMO_LOGGABLE_USERNAME);
            }
        }
    }
}
