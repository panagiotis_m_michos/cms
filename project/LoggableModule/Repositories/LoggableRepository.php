<?php

namespace LoggableModule\Repositories;

use Doctrine\ORM\QueryBuilder;
use DoctrineDataSource;
use Gedmo\Loggable\Entity\Repository\LogEntryRepository;

class LoggableRepository extends LogEntryRepository
{
    /**
     * @return QueryBuilder
     */
    public function getHistoryDataSource()
    {
        $qb = $this->createQueryBuilder('h')
            ->orderBy('h.loggedAt', 'DESC');
        return new DoctrineDataSource($qb);
    }
}
