<?php

namespace LoggableModule\Repositories;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Entities\Customer;
use Utils\Date;

class EmailLogRepository extends EntityRepository
{
    /**
     * @param Customer $customer
     * @param array $emailIds
     * @return int
     */
    public function getEmailsSent(Customer $customer, array $emailIds)
    {
        $qb = $this->createQueryBuilder('l')
            ->select('count(l)')
            ->where('l.customer = :customer')
            ->andWhere('l.emailNodeId IN (:emailNodes)')
            ->setParameter('customer', $customer)
            ->setParameter('emailNodes', $emailIds);
        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param Customer $customer
     * @param array $emailIds
     * @param Date $date
     * @return int
     */
    public function hasEmailsSentAfter(Customer $customer, array $emailIds, Date $date)
    {
        $qb = $this->createQueryBuilder('l')
            ->select('count(l)')
            ->where('l.customer = :customer')
            ->andWhere('l.emailNodeId IN (:emailNodes)')
            ->andWhere('l.dtc > :date')
            ->setParameters(
                [
                    'customer' => $customer,
                    'emailNodes' => $emailIds,
                    'date' => $date
                ]
            );
        return $qb->getQuery()->getSingleScalarResult() > 0;
    }

    /**
     * @param Customer $customer
     * @return QueryBuilder
     */
    public function getEmailsLogQueryBuilder(Customer $customer = NULL)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('l')
            ->from('LoggableModule\Entities\EmailLog', 'l')
            ->orderBy('l.dtc', 'DESC');
        if ($customer) {
            $qb->where('l.customer = :customer')
                ->setParameter('customer', $customer);
        }
        return $qb;
    }
}
