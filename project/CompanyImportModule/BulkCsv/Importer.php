<?php

namespace CompanyImportModule\BulkCsv;

use Basket;
use BasketModule\Services\PriceCalculator;
use CompanyImportModule\BulkCsv\Exceptions\InvalidCsvFormat;
use Customer as OldCustomer;
use Doctrine\ORM\EntityManager;
use DusanKasan\Knapsack\Collection;
use Entities\Company;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation;
use Entities\Customer;
use Entities\Order;
use Entities\OrderItem;
use Entities\Transaction;
use FormSubmissionModule\Services\DocumentUploader;
use InvalidArgumentException;
use OrderModule\Builders\OrderItemBuilderFactory;
use OrderModule\Processors\OrderItemsProcessor;
use OrderModule\Resolvers\PrintedDocumentResolver;
use Package;
use Services\PackageService;
use Services\ProductService;
use SplFileObject;

class Importer
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var OrderItemsProcessor
     */
    private $orderItemsProcessor;

    /**
     * @var PriceCalculator
     */
    private $priceCalculator;

    /**
     * @var ProductService
     */
    private $productService;

    /**
     * @var array
     */
    private $header = [
        [
            "",
            "Share Information",
            "",
            "",
            "Registered Office Address",
            "",
            "",
            "",
            "",
            "Director Information",
            "",
            "",
            "",
            "",
            "Director Service Address",
            "",
            "",
            "",
            "",
            "Director Residential Address",
            "",
            "",
            "",
            "",
            "Shareholder Information",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "Shareholder security questions",
            "",
            "",
        ],
        [
            "Company Name:",
            "Share currency:",
            "Share class:",
            "Value per share:",
            "Building name or number:",
            "Street:",
            "Town:",
            "Postcode:",
            "Country:",
            "First name:",
            "Surname:",
            "Nationality:",
            "Occupation:",
            "Date of Birth:",
            "Building name or number:",
            "Street:",
            "Town:",
            "Postcode:",
            "Country",
            "Building name or number:",
            "Street:",
            "Town:",
            "Postcode:",
            "Country",
            "First name:",
            "Surname:",
            "Building name or number:",
            "Street:",
            "Town:",
            "Postcode:",
            "Country",
            "Shares Allocated",
            "Eye Colour",
            "Telephone Number",
            "Town of Birth",
        ]
    ];

    /**
     * @var IncorporationFactory
     */
    private $incorporationFactory;

    /**
     * @var PackageService
     */
    private $packageService;

    /**
     * @var OrderItemBuilderFactory
     */
    private $orderItemBuilderFactory;

    /**
     * @var PrintedDocumentResolver
     */
    private $printedDocumentResolver;

    /**
     * @param EntityManager $entityManager
     * @param OrderItemsProcessor $orderItemsProcessor
     * @param ProductService $productService
     * @param PriceCalculator $priceCalculator
     * @param DocumentUploader $documentUploader
     * @param IncorporationFactory $incorporationFactory
     * @param PackageService $packageService
     * @param OrderItemBuilderFactory $orderItemBuilderFactory
     * @param PrintedDocumentResolver $printedDocumentResolver
     */
    public function __construct(
        EntityManager $entityManager,
        OrderItemsProcessor $orderItemsProcessor,
        ProductService $productService,
        PriceCalculator $priceCalculator,
        DocumentUploader $documentUploader,
        IncorporationFactory $incorporationFactory,
        PackageService $packageService,
        OrderItemBuilderFactory $orderItemBuilderFactory,
        PrintedDocumentResolver $printedDocumentResolver
    ) {
        $this->entityManager = $entityManager;
        $this->orderItemsProcessor = $orderItemsProcessor;
        $this->productService = $productService;
        $this->priceCalculator = $priceCalculator;
        $this->documentUploader = $documentUploader;
        $this->incorporationFactory = $incorporationFactory;
        $this->packageService = $packageService;
        $this->orderItemBuilderFactory = $orderItemBuilderFactory;
        $this->printedDocumentResolver = $printedDocumentResolver;
    }

    /**
     * @param Customer $customer
     * @param SplFileObject $file
     * @throws InvalidCsvFormat
     * @throws InvalidArgumentException
     */
    public function import(Customer $customer, $packageId, SplFileObject $file)
    {
        $order = new Order($customer);
        $this->entityManager->persist($order);

        $file->setFlags(SplFileObject::READ_CSV);
        $parts = Collection::from($file)->splitAt(2);

        $header = $parts->get(0)->toArray();
        if ($header != $this->header) {
            throw InvalidCsvFormat::invalidHeader();
        }

        $packages = $parts
            ->get(1)
            ->reject(
                function (array $row) {
                    return $this->isEmptyLine($row);
                }
            )
            ->map(
                function ($row) use ($customer) {
                    return $this->buildIncorporation($customer, $row);
                }
            )
            ->realize()
            ->map(
                function (CompanyIncorporation $incorporation) use ($order, $packageId, $customer) {
                    $this->saveIncorporation($incorporation);
                    $package = $this->buildPackage($packageId, $incorporation->getCompany());
                    $this->savePackageToOrder($package, $order);
                    $this->printedDocumentResolver->resolveDocuments($package->getId(), $customer, $package->getCompanyEntity());

                    return $package;
                }
            )
            ->toArray();

        $price = $this->priceCalculator->calculatePrice($packages);
        $order->setPrice($price);

        $transaction = new Transaction($customer, Transaction::TYPE_FREE);
        $order->setTransaction($transaction);
        $order->setPaymentMediumId(Order::PAYMENT_MEDIUM_NON_STANDARD);

        $this->entityManager->flush();

    }

    /**
     * @param int $packageId
     * @param Company $company
     * @return Package
     */
    private function buildPackage($packageId, Company $company)
    {
        $package = $this->packageService->getPackageById($packageId);
        $package->companyName = $company->getCompanyName();
        $package->setCompanyId($company->getId());
        $package->setCompanyEntity($company);

        return $package;
    }

    /**
     * @param Package $package
     * @param Order $order
     * @return OrderItem
     */
    private function createOrderItemFromPackage(Package $package, Order $order)
    {
        $orderItem = $this->orderItemBuilderFactory->createBuilder()
            ->setOrder($order)
            ->setCompany($package->getCompanyEntity())
            ->setProductWithPrice($package)
            ->build();

        $order->addItem($orderItem);
        $this->orderItemsProcessor->saveItemsToCompany(
            new OldCustomer($package->getCompanyEntity()->getCustomer()->getId()),
            [$package],
            $package->getCompanyEntity()
        );

        return $orderItem;
    }

    /**
     * @return bool
     */
    private function isEmptyLine(array $row)
    {
        $hasOnlyEmptyItems = \DusanKasan\Knapsack\every(
            $row,
            function ($item) {
                return empty($item);
            }
        );

        $isEmptyLine = count($row) == 1 && \DusanKasan\Knapsack\getOrDefault($row, 0, NULL) == NULL;

        return $hasOnlyEmptyItems || $isEmptyLine;
    }

    /**
     * @param Customer $customer
     * @param array $row
     * @return CompanyIncorporation
     */
    private function buildIncorporation(Customer $customer, array $row)
    {
        $incorporation = $this->incorporationFactory->buildIncorporation($customer, $row);
        $this->entityManager->persist($incorporation);

        return $incorporation;
    }

    /**
     * @param CompanyIncorporation $incorporation
     */
    private function saveIncorporation(CompanyIncorporation $incorporation)
    {
        $this->entityManager->flush($incorporation);
        $this->documentUploader->addStandardArticles($incorporation);
    }

    /**
     * @param Package $package
     * @param Order $order
     */
    public function savePackageToOrder(Package $package, Order $order)
    {
        $package->orderItem = $this->createOrderItemFromPackage($package, $order);
        $this->entityManager->flush($order);
        $this->productService->saveServices([$package]);
    }
}
