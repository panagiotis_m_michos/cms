<?php

namespace CompanyImportModule\BulkCsv\Exceptions;

use Address;
use RuntimeException;

class InvalidCsvFormat extends RuntimeException
{
    /**
     * @param string $country
     * @return InvalidCsvFormat
     */
    public static function invalidRegisteredOfficeCountry($country)
    {
        $roCountries = implode(', ', Address::$registeredOfficeCountries);
        return new self("Invalid registered office address! Expected one of: $roCountries, got $country");
    }

    /**
     * @param int $expected
     * @param int $actual
     * @return InvalidCsvFormat
     */
    public static function invalidColumnCount($expected, $actual)
    {
        return new self("Invalid number of columns! Expected $expected got $actual.");
    }

    /**
     * @return InvalidCsvFormat
     */
    public static function invalidHeader()
    {
        return new self('Invalid header!');
    }
}
