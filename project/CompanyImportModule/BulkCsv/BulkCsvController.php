<?php

namespace CompanyImportModule\BulkCsv;

use HttpModule\Responses\CsvResponse;

class BulkCsvController
{
    /**
     * @return CsvResponse
     */
    public function downloadExampleCsv()
    {
        return new CsvResponse(
            file_get_contents(PROJECT_DIR . '/CompanyImportModule/BulkCsv/example.csv'),
            'example.csv'
        );
    }

    /**
     * @return CsvResponse
     */
    public function downloadTemplateCsv()
    {
        return new CsvResponse(
            file_get_contents(PROJECT_DIR . '/CompanyImportModule/BulkCsv/template.csv'),
            'template.csv'
        );
    }
}
