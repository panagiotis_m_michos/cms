<?php

namespace CompanyImportModule\BulkCsv;

use Address as OldAddress;
use Basket;
use CompanyImportModule\BulkCsv\Exceptions\InvalidCsvFormat;
use DateTime;
use Entities\Company;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation\Capital;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation\Director;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation\Subscriber;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporationFactory;
use Entities\CompanyHouse\Helper\Address;
use Entities\CompanyHouse\Helper\Authentication;
use Entities\CompanyHouse\Helper\PersonDirector;
use Entities\CompanyHouse\Helper\PersonOfficer;
use Entities\CompanyHouse\Helper\Share;
use Entities\Customer;
use InvalidArgumentException;

class IncorporationFactory
{
    /**
     * @var CompanyIncorporationFactory
     */
    private $companyIncorporationFactory;

    /**
     * IncorporationFactory constructor.
     * @param CompanyIncorporationFactory $companyIncorporationFactory
     */
    public function __construct(CompanyIncorporationFactory $companyIncorporationFactory)
    {
        $this->companyIncorporationFactory = $companyIncorporationFactory;
    }

    /**
     * @param Customer $customer
     * @param array $row from the csv
     * @return CompanyIncorporation
     * @throws InvalidCsvFormat
     * @throws InvalidArgumentException
     */
    public function buildIncorporation(Customer $customer, array $row)
    {
        $values = $this->preprocessRow($row);

        $company = new Company($customer, $values['company_name']);

        $address = new Address(
            $values['ro_address_line_1'],
            $values['ro_address_line_2'],
            $values['ro_address_town'],
            $values['ro_address_postcode'],
            $values['ro_address_country']
        );

        $directorAddress = new Address(
            $values['sa_address_line_1'],
            $values['sa_address_line_2'],
            $values['sa_address_town'],
            $values['sa_address_postcode'],
            $values['sa_address_country']
        );

        $directorResidentialAddress = new Address(
            $values['residential_address_line_1'],
            $values['residential_address_line_2'],
            $values['residential_address_town'],
            $values['residential_address_postcode'],
            $values['residential_address_country']
        );

        $person = new PersonDirector(
            $values['director_first_name'],
            $values['director_surname'],
            DateTime::createFromFormat('d/m/Y', $values['director_date_of_birth']),
            $values['director_nationality'],
            $values['director_occupation'],
            $values['residential_address_country'],
            $directorAddress,
            $directorResidentialAddress
        );
        $appointments[] = new Director($person);

        $capitals[] = new Capital(
            $values['share_currency'],
            $values['share_class'],
            'Full voting rights',
            $values['shareholder_num_of_shares'],
            $values['value_per_share'],
            0,
            $values['value_per_share'] * $values['shareholder_num_of_shares']
        );

        $subscriberAddress = new Address(
            $values['shareholder_address_line_1'],
            $values['shareholder_address_line_2'],
            $values['shareholder_address_town'],
            $values['shareholder_address_postcode'],
            $values['shareholder_address_country']
        );

        $person = new PersonOfficer(
            $values['shareholder_first_name'],
            $values['shareholder_surname'],
            $subscriberAddress
        );
        $subscribers[] = new Subscriber(
            $person,
            new Share(
                $values['share_currency'],
                $values['share_class'],
                $values['shareholder_num_of_shares'],
                $values['value_per_share']
            ),
            new Authentication(
                $values['shareholder_auth_eye_color'],
                $values['shareholder_auth_phone_number'],
                $values['shareholder_auth_birth_town']
            )
        );

        $incorporation = $this->companyIncorporationFactory->createByShares(
            $company,
            $address,
            $values['country_of_incorporation'],
            $appointments,
            $capitals,
            $subscribers,
            [],
            FALSE,
            FALSE
        );

        return $incorporation;
    }

    /**
     * @param array $row
     * @return mixed
     */
    private function preprocessRow(array $row)
    {
        $rowKeys = [
            'company_name',
            'share_currency',
            'share_class',
            'value_per_share',
            'ro_address_line_1',
            'ro_address_line_2',
            'ro_address_town',
            'ro_address_postcode',
            'ro_address_country',
            'director_first_name',
            'director_surname',
            'director_nationality',
            'director_occupation',
            'director_date_of_birth',
            'sa_address_line_1',
            'sa_address_line_2',
            'sa_address_town',
            'sa_address_postcode',
            'sa_address_country',
            'residential_address_line_1',
            'residential_address_line_2',
            'residential_address_town',
            'residential_address_postcode',
            'residential_address_country',
            'shareholder_first_name',
            'shareholder_surname',
            'shareholder_address_line_1',
            'shareholder_address_line_2',
            'shareholder_address_town',
            'shareholder_address_postcode',
            'shareholder_address_country',
            'shareholder_num_of_shares',
            'shareholder_auth_eye_color',
            'shareholder_auth_phone_number',
            'shareholder_auth_birth_town',
        ];

        if (count($row) != count($rowKeys)) {
            throw InvalidCsvFormat::invalidColumnCount(count($rowKeys), count($row));
        }

        $values = array_combine($rowKeys, $row);
        
        $roCountryMap = array_flip(OldAddress::$registeredOfficeCountries);
        if (!isset($roCountryMap[$values['ro_address_country']])) {
            throw InvalidCsvFormat::invalidRegisteredOfficeCountry($values['ro_address_country']);
        }

        $values['ro_address_country'] = $roCountryMap[$values['ro_address_country']];

        switch ($values['ro_address_country']) {
            case 'GB-ENG':
                $countryOfIncorporation = 'EW';
                break;
            case 'GB-WLS':
                $countryOfIncorporation = 'WA';
                break;
            case 'GB-SCT':
                $countryOfIncorporation = 'SC';
                break;
            case 'GB-NIR':
                $countryOfIncorporation = 'NI';
                break;
            default:
                throw new InvalidArgumentException(
                    'Can not translate registered office address to country of incorporation.'
                );
        }
        $values['country_of_incorporation'] = $countryOfIncorporation;

        return $values;
    }
}
