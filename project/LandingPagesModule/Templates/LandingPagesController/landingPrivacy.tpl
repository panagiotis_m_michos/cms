{extends '@structure/layout.tpl'}

{block content}
    <div class="width100 home-banner1 padcard-mobile" style="background: url('{$introductionStrip.background nofilter}') no-repeat top;">
        <div class="container">
            <div class="row btm10">
                <div class="col-xs-12 col-md-10">
                    <h1 class="white">{$introductionStrip.title nofilter}</h1>
                    <p class="lead white">{$introductionStrip.description nofilter}</p>
                </div>
            </div>
            <div class="row btm20">
                <div class="col-xs-12 col-md-2">
                    {if $namesearch}
                        {$introductionStrip.callToAction nofilter}
                    {else}
                        {$introductionStrip.callToActionWithoutSearch nofilter}
                    {/if}
                </div>
                <div class="col-xs-12 col-md-4 pad0 text-center-xs">
                    <h2 class="white top10">{$introductionStrip.price nofilter}</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-8 text-left btm20 text-center-xs">
                    {if $namesearch}
                        <p class="lead white-link">{$introductionStrip.firstLinkWithSearch nofilter}</p>
                    {else}
                        <p class="lead white-link">{$introductionStrip.firstLinkWithoutSearch nofilter}</p>
                    {/if}
                    <p class="lead white-link">{$introductionStrip.secondLink nofilter}</p>
                </div>
                <div class="col-xs-12 col-md-4 btm20 pad0">
                    <div id="home2016-feefo">
                        <div class="feefo2016-top">
                            <span><strong>GOLD</strong> TRUSTED MERCHANT</span>
                        </div>
                        <div class="feefo2016-central">
                            <div class="feefo2016-left">
                                <img src="{$urlImgs}home2016/feefo.png" alt="" />
                            </div>
                            <div class="feefo2016-right">
                                <span class="yellow2 lead"><strong>{$feefoAverage}%</strong></span>
                                <span class="display-inline white">
                                    <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                </span>
                                <p class="margin0">
                                    <a href="http://www.feefo.com/feefo/viewvendor.jsp?logon=www.madesimplegroup.com/companiesmadesimple" onclick="window.open(this.href, 'Feefo', 'width=1000,height=600,scrollbars,resizable'); return false;">
                                        {$feefoReviewCount} reviews
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {if isset($callUsStrip.title)}
        <div id="home2016-progress" class="padcard-mobile width100">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-center ">
                        <p class="lead white">{$callUsStrip.title nofilter}</p>
                        <a href="{$callUsStrip.link nofilter}" class="btn btn-lg btn-success"><i class="fa fa-phone" aria-hidden="true"></i> {$callUsStrip.cta nofilter}</a>
                    </div>
                </div>
            </div>
        </div>
    {/if}
    {if isset($featuresStrip.features)}
        <div class="width100 bg-grey-blue padcard-mobile">
            <div class="container">
                <div class="row">
                    {foreach $featuresStrip.features as $feature}
                        <div class="col-xs-12 col-md-4 text-center white">
                            <img class="padcard" src="{$feature.icon nofilter}" alt="" />
                            <p class="lead font-200">{$feature.title nofilter}</p>
                            <p class="font-200">{$feature.description nofilter}</p>
                        </div>
                    {/foreach}
                </div>
            </div>
        </div>
    {/if}
    {if isset($cashbackStrip.title)}
        <div class="width100 bg-white padcard-mobile">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <h2 class="top10">{$cashbackStrip.title nofilter}</h2>
                        <p class="lead">{$cashbackStrip.description nofilter}</p>
                    </div>
                </div>
                <div class="row btm30">
                    {foreach $cashbackStrip.banks as $bank}
                        <div class="col-xs-12 col-md-6 text-center btm10">
                            <div class="padcard blueborder">
                                <a href="{$bank.iconLink nofilter}"><img class="padcard" src="{$bank.icon nofilter}" alt="" /></a>
                                <p>{$bank.description nofilter}</p>
                            </div>
                        </div>
                    {/foreach}
                </div>
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <p><strong>{$cashbackStrip.footer nofilter}</strong></p>
                    </div>
                </div>
            </div>
        </div>
    {/if}
    {if isset($seoCopy4.title)}
        <div class="container-fluid home-banner6 padcard-mobile">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <h2 class="top10">{$seoCopy4.title nofilter}</h2>
                        {foreach $seoCopy4.blocks as $block}
                            {if isset($block.title)}<h4>{$block.title nofilter}</h4>{/if}
                            <p>{$block.description nofilter}</p>
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
    {/if}
    <div class="width100 plus-matrix"><img src="{$urlImgs}plus-matrix.png" class="center-block" alt=""/></div>
    <div class="width100 bg-white padcard-mobile">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h2 class="top0">{$toolkitStrip.title nofilter}</h2>
                    <p class="lead">{$toolkitStrip.description nofilter}</p>
                </div>
            </div>
            {foreach $toolkitStrip.tools as $tool}
                <div class="matrix-toolkit-wrap col-xs-12 col-md-4 text-center btm10">
                    <div class="matrix-toolkit-in padcard-mobile">
                        <i class="fa fa-3x fa-fw top10 padcard-mobile {$tool.icon nofilter}" aria-hidden="true"></i>
                        <p><strong>{$tool.title nofilter}</strong></p>
                        <p class="small">{$tool.overlay nofilter}</p>
                    </div>
                </div>
            {/foreach}
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="lead btm10 top20">{$toolkitStrip.footer nofilter}</p>
                    <p class="margin0">{$toolkitStrip.footerDescription nofilter}</p>
                </div>
            </div>
        </div>
    </div>

    {include file="@structure/feefoStrip.tpl"}
    {if isset($partnersStrip.title)}
        <div class="width100 bg-white padcard-mobile">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <h2 class="top10">{$partnersStrip.title nofilter}</h2>
                        <p class="lead">{$partnersStrip.description nofilter}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 text-center">
                        {foreach $partnersStrip.partners as $partner}
                            <img class="padcard" src="{$partner.icon nofilter}" alt="" />
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
    {/if}
    {if isset($seoCopy1.title)}
        <div class="width100 bg-grey1 padcard-mobile">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <h2 class="top10">{$seoCopy1.title nofilter}</h2>
                        <p class="lead">{$seoCopy1.description nofilter}</p>
                        {foreach $seoCopy1.blocks as $block}
                            {if isset($block.title)}<h4>{$block.title nofilter}</h4>{/if}
                            <p>{$block.description nofilter}</p>
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
    {/if}
{/block}

{include file="@footer.tpl"}
