<?php

namespace LandingPagesModule\Controllers;

use Symfony\Component\HttpFoundation\Response;
use TemplateModule\Renderers\IRenderer;
use SearchControler;
use FeefoModule\IFeefo;

class LandingPagesController
{

    /**
     * @var IRenderer
     */
    private $renderer;
    
    /**
     * @var IFeefo
     */
    private $feefo;

    /**
     * @param IRenderer $renderer
     * @param IFeefo $feefo
     */
    public function __construct(IRenderer $renderer, IFeefo $feefo)
    {
        $this->renderer = $renderer;
        $this->feefo = $feefo;
    }

    /**
     * @return Response
     */
    public function landingPrivacy()
    {
        $variables = [
            'namesearch' => (bool) SearchControler::getCompanyName(),
            'feefoAverage' => $this->feefo->getSummary()->getAverageRating(),
            'feefoReviewCount' => $this->feefo->getSummary()->getReviewCount(),
        ];
        
        return $this->renderer->render($variables);
    }

}
