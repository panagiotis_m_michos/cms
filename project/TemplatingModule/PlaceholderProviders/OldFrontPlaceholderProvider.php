<?php

namespace TemplatingModule\PlaceholderProviders;

use Basket;
use Customer;
use FApplication;
use FTemplate;
use RouterModule\RouteParameter;
use Services\ControllerHelper;
use Services\NodeService;
use TemplateModule\PlaceholderProviders\IPlaceholderProvider;

final class OldFrontPlaceholderProvider implements IPlaceholderProvider
{
    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    /**
     * @var RouteParameter
     */
    private $routeParameter;

    /**
     * @var NodeService
     */
    private $nodeService;

    /**
     * @var FrontPlaceholderProvider
     */
    private $frontPlaceholderProvider;

    /**
     * @var Basket
     */
    private $basket;

    /**
     * @param FrontPlaceholderProvider $frontPlaceholderProvider
     * @param ControllerHelper $controllerHelper
     * @param RouteParameter $routeParameter
     * @param NodeService $nodeService
     * @param Basket $basket
     */
    public function __construct(
        FrontPlaceholderProvider $frontPlaceholderProvider,
        ControllerHelper $controllerHelper,
        RouteParameter $routeParameter,
        NodeService $nodeService,
        Basket $basket
    )
    {
        $this->frontPlaceholderProvider = $frontPlaceholderProvider;
        $this->controllerHelper = $controllerHelper;
        $this->routeParameter = $routeParameter;
        $this->nodeService = $nodeService;
        $this->basket = $basket;
    }

    /**
     * @param array $data
     * @return FTemplate
     */
    public function setPlaceholders(array $data = [])
    {
        $template = $this->frontPlaceholderProvider->setPlaceholders();

        $nodeId = isset($data['nodeId']) ? $data['nodeId'] : NULL;
        if ($nodeId === NULL) {
            if ($this->routeParameter->getPage()) {
                $nodeId = $this->routeParameter->getPage();
            } else {
                $nodeId = FApplication::$config['home_node'];
            }
        }

        $node = $this->nodeService->getNodeById($nodeId);

        $template->customer = Customer::getSignedIn();
        $template->basket = $this->basket;
        $template->node = $node;
        $template->page = $node->page;
        $template->config = FApplication::$config;
        $template->router = FApplication::$router;
        $template->currLng = FApplication::$lang;
        $template->dateTimeNow = date('M j, Y H:i:s O');

        foreach ($data as $key => $value) {
            $template->$key = $value;
        }

        return $template;
    }
}
