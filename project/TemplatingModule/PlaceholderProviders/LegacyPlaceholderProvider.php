<?php

namespace TemplatingModule\PlaceholderProviders;

use BaseControler;
use FApplication;
use FLanguage;
use FProperty;
use HomeControler;
use TemplateModule\ITemplate;
use TemplateModule\PlaceholderProviders\IPlaceholderProvider;
use Utils\FeedReader\FeedReader;

class LegacyPlaceholderProvider implements IPlaceholderProvider
{
    /**
     * @var OldFrontPlaceholderProvider
     */
    private $oldFrontPlaceholderProvider;

    /**
     * @var FeedReader
     */
    private $feedReader;

    /**
     * @param OldFrontPlaceholderProvider $oldFrontPlaceholderProvider
     * @param FeedReader $feedReader
     */
    public function __construct(OldFrontPlaceholderProvider $oldFrontPlaceholderProvider, FeedReader $feedReader)
    {
        $this->oldFrontPlaceholderProvider = $oldFrontPlaceholderProvider;
        $this->feedReader = $feedReader;
    }

    /**
     * @param array $data
     * @return ITemplate
     */
    public function setPlaceholders(array $data = [])
    {
        $template = $this->oldFrontPlaceholderProvider->setPlaceholders($data);

        //$this->template->topLoginForm = $this->getComponent('login');
        ////$this->template->bottomMenu = FMenu::get(self::BOTTOM_MENU_FOLDER);
        //$this->template->rightBlock1 = $this->getComponentRightBlock1();
        //$this->template->rightBlock2 = $this->getComponentRightBlock2();
        //$this->template->rightBlockStartUpGuide = $this->getComponent('startUpGuide');
        $template->testimonials = FProperty::getNodePropsValue('testimonials', HomeControler::HOME_PAGE);

        // top menu
        //$this->template->topMenu = $this->getComponentTopMenu();

        // left menu
        //$leftMenu = $this->getCompomentLefMenu();
        //$this->template->leftMenu = $leftMenu;
        //$this->template->currentLeftMenu = $this->getComponentCurrentLeftMenu($leftMenu);

        // back link
        //$backlink = FApplication::$httpRequest->uri->getAbsoluteUri();
        //$this->template->backLink = urlencode($backlink);


        /** @var FeedReader $postReader */
        $blogPosts = $this->feedReader->getPosts(5);
        $template->homeBlogPosts = array_slice($blogPosts, 0, 2);
        $template->leftBlogPosts = $blogPosts;

        // left menu items
        $template->leftMenuItems = BaseControler::getComponentLeftMenuItems();

        $template->disabledLeftColumn = FProperty::get('disabledLeftColumn')->value;
        $template->disabledRightColumn = FProperty::get('disabledRightColumn')->value;

        // other text stuff
        $template->author = FApplication::$config['author'];
        $template->copyright = FApplication::$config['copyright'];
        $template->projectName = FApplication::$config['projectName'];
        $template->languages = FLanguage::getLanguages();
        $template->keywords = $template->node->getLngKeywords();
        $template->description = $template->node->getLngDescription();
        $template->seoTitle = $template->node->getLngSeoTitle();
        $template->text = FApplication::modifyCmsText($template->node->getLngText());
        $template->isProduction = FApplication::$container->getParameter('isProduction');
        $template->disabledFeedback = FProperty::get('disabledFeedback')->value;

        $template->title = $template->node->getLngTitle();
        $template->visibleTitle = $template->node->getVisibleTitle();
        $template->setVariables($data);
        return $template;
    }
}