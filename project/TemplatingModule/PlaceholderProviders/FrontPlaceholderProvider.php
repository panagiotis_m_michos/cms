<?php

namespace TemplatingModule\PlaceholderProviders;

use TemplateModule\ITemplate;
use TemplateModule\PlaceholderProviders\IPlaceholderProvider;
use TemplatingModule\MatchedRouteDetails;
use TemplatingModule\MethodVariableProvider;
use TemplatingModule\TemplateFactory;

class FrontPlaceholderProvider implements IPlaceholderProvider
{
    /**
     * @var TemplateFactory
     */
    private $templateFactory;

    /**
     * @var MethodVariableProvider
     */
    private $methodVariableProvider;

    /**
     * @var MatchedRouteDetails
     */
    private $routeDetails;

    /**
     * @param TemplateFactory $templateFactory
     * @param MethodVariableProvider $methodVariableProvider
     * @param MatchedRouteDetails $routeDetails
     */
    public function __construct(
        TemplateFactory $templateFactory,
        MethodVariableProvider $methodVariableProvider,
        MatchedRouteDetails $routeDetails
    )
    {
        $this->templateFactory = $templateFactory;
        $this->methodVariableProvider = $methodVariableProvider;
        $this->routeDetails = $routeDetails;
    }

    /**
     * @param array $data
     * @return ITemplate
     */
    public function setPlaceholders(array $data = [])
    {
        $template = $this->templateFactory->create();

        $variables = $this->methodVariableProvider->getMethodVariables(
            $this->routeDetails->getClass(),
            $this->routeDetails->getAction()
        );

        $template->setVariables(array_merge_replace_recursive($variables, $data));

        return $template;
    }
}
