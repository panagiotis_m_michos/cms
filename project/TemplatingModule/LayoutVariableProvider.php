<?php

namespace TemplatingModule;

use Basket;
use ContentModule\VariableProviders\IVariableProvider;
use Customer;
use Services\ControllerHelper;
use Symfony\Bundle\FrameworkBundle\Templating\Helper\FormHelper;

class LayoutVariableProvider
{
    /**
     * @var IVariableProvider
     */
    private $variableProvider;

    /**
     * @var FormHelper
     */
    private $formHelper;

    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    /**
     * @param IVariableProvider $variableProvider
     * @param FormHelper $formHelper
     * @param ControllerHelper $controllerHelper
     */
    public function __construct(
        IVariableProvider $variableProvider,
        FormHelper $formHelper,
        ControllerHelper $controllerHelper
    )
    {
        $this->variableProvider = $variableProvider;
        $this->formHelper = $formHelper;
        $this->controllerHelper = $controllerHelper;
    }

    /**
     * @return array
     */
    public function getLayoutVariables()
    {
        $megamenu = $this->variableProvider->getVariablesByKey('megamenu');
        $megamenu['isLoggedIn'] = Customer::isSignedIn();
        $megamenu['basketCount'] = (new Basket())->getItemsCount();

        $header = $this->variableProvider->getVariablesByKey('header');
        $dayNumber = date('N');
        if ($dayNumber == 5) {
            $openingHours = 'Call today: 9am - 4.30pm';
        } elseif (in_array($dayNumber, [6, 7])) {
            $openingHours = 'Office opens at 9am Monday';
        } else {
            $openingHours = 'Call today: 9am - 5.30pm';
        }
        $header['openingHours'] = $openingHours;

        $variables = [
            'megamenu' => $megamenu,
            'gbrandlinks' => $this->variableProvider->getVariablesByKey('gbrandlinks'),
            'header' => $header,
            'footer' => $this->variableProvider->getVariablesByKey('footer'),
            'urlRoot' => URL_ROOT,
            'urlCss' => URL_ROOT . 'front/css/',
            'urlJs' => URL_ROOT . 'front/js/',
            'urlImgs' => URL_ROOT . 'front/imgs/',
            'urlComponent' => URL_ROOT . 'components/',
            'flashes' => $this->controllerHelper->readFlashMessages(),
            'formHelper' => $this->formHelper,
            'isLoggedIn' => Customer::isSignedIn()
        ];
        return $variables;
    }
}
