<?php

namespace TemplatingModule;

use RouterModule\RouteParameter;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MatchedRouteDetails
{
    /**
     * @var RouteParameter
     */
    private $routeParameter;
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param RouteParameter $routeParameter
     * @param ContainerInterface $container
     */
    public function __construct(RouteParameter $routeParameter, ContainerInterface $container)
    {
        $this->routeParameter = $routeParameter;
        $this->container = $container;
    }

    /**
     * @return object
     */
    public function getObject()
    {
        return $this->container->get($this->routeParameter->getController());
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return get_class($this->getObject());
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->routeParameter->getAction();
    }
}
