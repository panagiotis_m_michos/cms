<?php

namespace TemplatingModule;

use FTemplate;

class TemplateFactory
{
    /**
     * @var SmartySingletonFactory
     */
    private $smartyFactory;

    /**
     * @param SmartySingletonFactory $smartyFactory
     */
    public function __construct(SmartySingletonFactory $smartyFactory)
    {
        $this->smartyFactory = $smartyFactory;
    }

    /**
     * @return FTemplate
     */
    public function create()
    {
        return new FTemplate($this->smartyFactory->create());
    }
}
