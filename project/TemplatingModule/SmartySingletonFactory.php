<?php

namespace TemplatingModule;

use CommonModule\UrlGenerator;
use Exception;
use FApplication;
use FeatureModule\Feature;
use FTools;
use Helpers\Currency;
use InvalidArgumentException;
use Services\NodeService;
use Smarty;
use Smarty_Internal_Template;
use SmartyException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Utils\Helpers\StringHelper;
use WebloaderHelper\Config as WebloaderConfig;
use WebloaderHelper\Factories\TemplateHelperFactory;

class SmartySingletonFactory
{
    /**
     * @var Smarty
     */
    private static $smarty;

    /**
     * @var UrlGenerator
     */
    private $urlGenerator;

    /**
     * @var NodeService
     */
    private $nodeService;

    /**
     * @var WebloaderConfig
     */
    private $config;

    /**
     * @param UrlGenerator $urlGenerator
     * @param NodeService $nodeService
     * @param ParameterBagInterface $config
     * @throws SmartyException
     */
    public function __construct(
        UrlGenerator $urlGenerator,
        NodeService $nodeService,
        ParameterBagInterface $config
    )
    {
        $this->urlGenerator = $urlGenerator;
        $this->nodeService = $nodeService;
        $this->config = $config;
    }

    /**
     * @return Smarty
     * @throws SmartyException
     */
    public function create()
    {
        if (self::$smarty === NULL) {
            self::$smarty = new Smarty;
            self::$smarty->escape_html = TRUE;
            self::$smarty->template_dir = FApplication::isAdmin() ? ADMIN_TEMPLATES_DIR : FRONT_TEMPLATES_DIR;
            self::$smarty->cache_dir = $this->config->get('smarty.cache_dir');

            if (FApplication::isAdmin()) {
                self::$smarty->compile_dir = $this->config->get('smarty.compiled.admin.cache_dir');
            } else {
                self::$smarty->compile_dir = $this->config->get('smarty.compiled.front.cache_dir');
            }

            self::$smarty->registerPlugin('modifier', 'pr', [FTools::class, 'pr']);
            self::$smarty->registerPlugin('modifier', 'h', 'htmlspecialchars');
            self::$smarty->registerPlugin('modifier', 'cms', [FApplication::class, 'modifyCmsText']);
            self::$smarty->registerPlugin('modifier', 'df', [FTools::class, 'dateFormat']);
            self::$smarty->registerPlugin('modifier', 'datetime', [FTools::class, 'datetime']);
            self::$smarty->registerPlugin('modifier', 'currency', [Currency::class, 'format']);
            self::$smarty->registerPlugin('modifier', 'json', 'json_encode');
            self::$smarty->registerPlugin('modifier', 'humanize_constants', [StringHelper::class, 'humanize_constants']);

            self::$smarty->registerPlugin('function', 'url', [$this, 'functionUrl']);
            self::$smarty->registerPlugin('function', 'product', [$this, 'functionGetProduct']);
            //self::$smarty->registerPlugin('function', 'link', function($arguments) {
            //    return FApplication::$router->link($arguments['route'], $arguments['params']);
            //});

            self::$smarty->registerPlugin('block', 'feature', [$this, 'blockFeature']);

            $config = new WebloaderConfig(
                PROJECT_DIR . '/config/webloader/',
                WWW_DIR,
                WWW_DIR . '/webtemp/',
                WWW_DIR,
                URL_ROOT . 'webtemp'
            );
            $minify = FApplication::$container->getParameter('isProduction');
            // hack for <=IE9 do not concatenate, minify
            // TODO remove once support for IE9 is dropped
            if (isset($_SERVER['HTTP_USER_AGENT'])) {
                if (preg_match('/(?i)msie [5-9]\./i', $_SERVER['HTTP_USER_AGENT'])) {
                    $minify = FALSE;
                }
            }
            TemplateHelperFactory::registerMacro(
                $config,
                self::$smarty,
                $minify
            );
        }

        return self::$smarty;
    }

    /**
     * @param array $params
     * @param string $content
     * @return string
     */
    public function blockFeature(array $params, $content)
    {
        if (isset($content) && isset($params['type'])) {
            if (Feature::isEnabled($params['type'])) {
                return $content;
            }
        }
    }

    /**
     * @param array $parameters
     * @return string
     * @throws Exception
     */
    public function functionUrl(array $parameters)
    {
        if (isset($parameters['route'])) {
            $route = $parameters['route'];
            unset($parameters['route']);
        } else {
            throw new InvalidArgumentException('No "route" parameter given for url macro');
        }

        if (isset($parameters['absolute']) && $parameters['absolute']) {
            unset($parameters['absolute']);

            return $this->urlGenerator->absoluteLink($route, $parameters);
        }

        return $this->urlGenerator->link($route, $parameters);
    }

    /**
     * @param array $params
     * @param Smarty_Internal_Template $template
     * @return string
     */
    public function functionGetProduct(array $params, Smarty_Internal_Template $template)
    {
        if (isset($params['var'], $params['id'])) {
            $variableName = $params['var'];
            $product = $this->nodeService->getProductById($params['id']);

            $template->assign($variableName, $product);
        }
    }
}

