<?php

namespace TemplatingModule;

use ContentModule\VariableProviders\IVariableProvider;

class MethodVariableProvider
{
    /**
     * @var LayoutVariableProvider
     */
    private $layoutVariableProvider;

    /**
     * @var IVariableProvider
     */
    private $variableProvider;

    /**
     * @param LayoutVariableProvider $layoutVariableProvider
     * @param IVariableProvider $variableProvider
     */
    public function __construct(LayoutVariableProvider $layoutVariableProvider, IVariableProvider $variableProvider)
    {
        $this->layoutVariableProvider = $layoutVariableProvider;
        $this->variableProvider = $variableProvider;
    }

    /**
     * @param string $className
     * @param string $methodName
     * @return array
     */
    public function getMethodVariables($className, $methodName)
    {
        return array_merge_recursive(
            $this->layoutVariableProvider->getLayoutVariables(),
            $this->variableProvider->getVariablesByKey("{$className}.{$methodName}")
        );
    }
}
