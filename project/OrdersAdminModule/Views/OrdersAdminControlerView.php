<?php

namespace OrdersAdminModule\Views;

use Entities\Order;

class OrdersAdminControlerView
{
    /**
     * @var Order
     */
    private $order;

    /**
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @return bool
     */
    public function canBeRefunded()
    {
        return !$this->order->isRefunded() && $this->order->getTotal() > 0;
    }
}
