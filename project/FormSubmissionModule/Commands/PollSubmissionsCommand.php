<?php

namespace FormSubmissionModule\Commands;

use Cron\Commands\CommandAbstract;
use Cron\INotifier;
use DateTime;
use Exception;
use PollingModel;
use Psr\Log\LoggerInterface;
use SettingsModel;

class PollSubmissionsCommand extends CommandAbstract
{
    /**
     * @var string
     */
    protected $timeType = self::TIME_TYPE_INTERVAL;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var INotifier
     */
    protected $notifier;

    /**
     * @param LoggerInterface $logger
     * @param INotifier $notifier
     */
    public function __construct(LoggerInterface $logger, INotifier $notifier)
    {
        $this->logger = $logger;
        $this->notifier = $notifier;
    }

    public function execute()
    {
        $now = new DateTime;
        $from = new DateTime('8:10');
        $to = new DateTime('18:10');

        if (in_array(date('N'), [6, 7]) || $now < $from || $now > $to) {
            return;
        }

        $settings = SettingsModel::getSettingsByKey('autopolling');
        if (!$settings->getValue()) {
            $this->logger->info('Autopolling is not allowed');

            return;
        }

        try {
            PollingModel::processFormSubmissionStatus();
        } catch (Exception $e) {
            $this->logger->error($e);
        }
    }
}
