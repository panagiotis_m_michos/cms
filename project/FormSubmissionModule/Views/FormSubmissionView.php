<?php

namespace FormSubmissionModule\Views;

use DateTime;
use Entities\CompanyHouse\FormSubmission;
use FormSubmission as OldFormSubmission;

class FormSubmissionView
{
    /**
     * @var OldFormSubmission
     */
    private $formSubmission;

    /**
     * @param OldFormSubmission $formSubmission
     */
    public function __construct(OldFormSubmission $formSubmission)
    {
        $this->formSubmission = $formSubmission;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->formSubmission->getFormSubmissionId();
    }

    /**
     * @return string
     */
    public function getFormIdentifier()
    {
        return $this->formSubmission->getFormIdentifier();
    }

    /**
     * @return bool
     */
    public function hasResponse()
    {
        return (bool) $this->formSubmission->getSubmissionStatus();
    }

    /**
     * @return string
     */
    public function getResponse()
    {
        return $this->formSubmission->getSubmissionStatus();
    }

    /**
     * @return string
     */
    public function getResponseMessage()
    {
        return $this->formSubmission->isInternalFailure() ? FormSubmission::RESPONSE_PENDING : $this->getResponse();
    }

    /**
     * @return DateTime
     */
    public function getDateCancelled()
    {
        return $this->formSubmission->getDateCancelled();
    }

    /**
     * @return bool
     */
    public function isAccept()
    {
        return $this->formSubmission->isAccept();
    }

    /**
     * @return bool
     */
    public function isError()
    {
        return $this->formSubmission->isError();
    }

    /**
     * @return bool
     */
    public function isPending()
    {
        return $this->formSubmission->isPending() || $this->formSubmission->isInternalFailure();
    }

    /**
     * @return bool
     */
    public function isInternalFailure()
    {
        return $this->formSubmission->isInternalFailure();
    }

    /**
     * @return bool
     */
    public function isCancelled()
    {
        return (bool) $this->formSubmission->getDateCancelled();
    }

    /**
     * @return bool
     */
    public function isCompanyIncorporationType()
    {
        return $this->formSubmission->isCompanyIncorporationType();
    }

    /**
     * @return bool
     */
    public function isAnnualReturnType()
    {
        return $this->formSubmission->isAnnualReturnType();
    }

    /**
     * @return int
     */
    public function getCompanyId()
    {
        return $this->formSubmission->getCompanyId();
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->formSubmission->getLanguage();
    }

    /**
     * @return string
     */
    public function getRejectReference()
    {
        return $this->formSubmission->getRejectReference();
    }

    /**
     * @return string
     */
    public function getExaminerTelephone()
    {
        return $this->formSubmission->getExaminerTelephone();
    }

    /**
     * @return string
     */
    public function getExaminerComment()
    {
        return $this->formSubmission->getExaminerComment();
    }

    /**
     * @return string
     */
    public function getFirstSubmissionDate()
    {
        return $this->formSubmission->getFirstSubmissionDate();
    }

    /**
     * @return string
     */
    public function getLastSubmissionDate()
    {
        return $this->formSubmission->getLastSubmissionDate();
    }

    /**
     * @return bool
     */
    public function isResubmittable()
    {
        return !$this->isCancelled() && ($this->isPending() || $this->isError());
    }

    /**
     * @return bool
     */
    public function isViewableType()
    {
        return in_array(
            $this->getFormIdentifier(),
            [
                FormSubmission::TYPE_OFFICER_RESIGNATION,
                FormSubmission::TYPE_OFFICER_APPOINTMENT,
                FormSubmission::TYPE_CHANGE_REGISTERED_OFFICE_ADDRESS,
                FormSubmission::TYPE_COMPANY_INCORPORATION,
            ]
        );
    }

    /**
     * @return bool
     */
    public function isViewable()
    {
        return $this->isAccept() && $this->isViewableType();
    }
}
