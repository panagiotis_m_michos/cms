<?php

namespace FormSubmissionModule\Views;

class FormSubmissionsView
{
    /**
     * @var FormSubmissionView[]
     */
    private $formSubmissionViews;

    /**
     * @param FormSubmissionView[] $formSubmissionViews
     */
    public function __construct(array $formSubmissionViews)
    {
        $this->formSubmissionViews = $formSubmissionViews;
    }

    /**
     * @return FormSubmissionView[]
     */
    public function getFormSubmissionViews()
    {
        return $this->formSubmissionViews;
    }

    /**
     * @return bool
     */
    public function hasInternalFailureSubmission()
    {
        foreach ($this->formSubmissionViews as $formSubmissionView) {
            if ($formSubmissionView->isInternalFailure()) {
                return TRUE;
            }
        }

        return FALSE;
    }
}
