<?php

namespace FormSubmissionModule\Factories;

use Entities\CompanyHouse\FormSubmission;
use FormSubmission as OldFormSubmission;
use FormSubmissionModule\Converters\FormSubmission\EntityToOldConverter;
use FormSubmissionModule\Views\FormSubmissionView;
use Services\SubmissionService;

class FormSubmissionViewFactory
{
    /**
     * @var EntityToOldConverter
     */
    private $converter;

    /**
     * @var SubmissionService
     */
    private $submissionService;

    /**
     * @param EntityToOldConverter $converter
     * @param SubmissionService $submissionService
     */
    public function __construct(EntityToOldConverter $converter, SubmissionService $submissionService)
    {
        $this->converter = $converter;
        $this->submissionService = $submissionService;
    }

    /**
     * @param FormSubmission $formSubmission
     * @return FormSubmissionView
     */
    public function createFromEntity(FormSubmission $formSubmission)
    {
        return new FormSubmissionView($this->converter->convert($formSubmission));
    }

    /**
     * @param OldFormSubmission $formSubmission
     * @return FormSubmissionView
     */
    public function createFromOldFormSubmission(OldFormSubmission $formSubmission)
    {
        return new FormSubmissionView($formSubmission);
    }

    /**
     * @param int $formSubmissionId
     * @return FormSubmissionView
     */
    public function createFromId($formSubmissionId)
    {
        return new FormSubmissionView(
            $this->converter->convert(
                $this->submissionService->getFormSubmissionById($formSubmissionId)
            )
        );
    }
}
