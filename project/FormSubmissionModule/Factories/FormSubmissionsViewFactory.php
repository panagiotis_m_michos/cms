<?php

namespace FormSubmissionModule\Factories;

use Entities\Company;
use Entities\CompanyHouse\FormSubmission;
use FormSubmissionModule\Views\FormSubmissionsView;

class FormSubmissionsViewFactory
{
    /**
     * @var FormSubmissionViewFactory
     */
    private $formSubmissionViewFactory;

    /**
     * @param FormSubmissionViewFactory $formSubmissionViewFactory
     */
    public function __construct(FormSubmissionViewFactory $formSubmissionViewFactory)
    {
        $this->formSubmissionViewFactory = $formSubmissionViewFactory;
    }

    /**
     * @param Company $company
     * @return FormSubmissionsView
     */
    public function createFromCompany(Company $company)
    {
        $formSubmissionViews = [];
        foreach ($company->getFormSubmissions() as $submission) {
            $formSubmissionViews[] = $this->formSubmissionViewFactory->createFromEntity($submission);
        }

        return new FormSubmissionsView($formSubmissionViews);
    }

    /**
     * @param Company $company
     * @return FormSubmissionsView
     */
    public function createFromCompanyWithResponse(Company $company)
    {
        $formSubmissionViews = [];
        foreach ($company->getFormSubmissions() as $submission) {
            if ($submission->hasResponse()) {
                $formSubmissionViews[] = $this->formSubmissionViewFactory->createFromEntity($submission);
            }
        }

        return new FormSubmissionsView($formSubmissionViews);
    }
}
