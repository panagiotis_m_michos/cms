<?php

namespace FormSubmissionModule\Converters\FormSubmission;

use Entities\CompanyHouse\FormSubmission;
use FormSubmission as OldFormSubmission;
use Services\SubmissionService;

class EntityToOldConverter
{
    /**
     * @var SubmissionService
     */
    private $submissionService;

    /**
     * @param SubmissionService $submissionService
     */
    public function __construct(SubmissionService $submissionService)
    {
        $this->submissionService = $submissionService;
    }

    /**
     * @param FormSubmission $submission
     * @return OldFormSubmission
     */
    public function convert(FormSubmission $submission)
    {
        return $this->submissionService->getOldFormSubmission($submission->getCompany(), $submission->getId());
    }
}
