<?php

namespace FormSubmissionModule\Services;

use CHFiling;
use DusanKasan\Knapsack\Collection;
use Entities\CompanyHouse\FormSubmission;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation\Document;
use InvalidArgumentException;

class DocumentUploader
{
    /**
     * @param CompanyIncorporation $incorporation
     */
    public function addStandardArticles(CompanyIncorporation $incorporation)
    {
        $companyId = $incorporation->getCompanyId();

        if ($companyId == NULL) {
            throw new InvalidArgumentException('Incorporation company must have ID!');
        }

        $incorporation->setArticles($incorporation->getType() . 'MODEL');

        $filePath =
            CHFiling::getDocPath() . '/' .
            CHFiling::getDirName($companyId) . '/' .
            "company-{$companyId}" . '/' .
            "formSubmission-{$incorporation->getId()}";

        if (!is_dir($filePath)) {
            mkdir($filePath, 0777, TRUE);
        }

        $defaultArticles = FILES_DIR . '/' . $incorporation->getType() . '/articles.pdf';
        copy($defaultArticles, $filePath . '/article.pdf');

        $documents = Collection::from($incorporation->getDocuments())
            ->reject(
                function (Document $document) {
                    return $document->getCategory() === 'MEMARTS';
                }
            )
            ->append(new Document('MEMARTS', 'article.pdf'));

        $incorporation->setDocuments($documents);
    }
}
