<?php

namespace IdCheckModule\Listeners;

use Dispatcher\Events\PaymentEvent;
use EventLocator;
use IdCheckModule\IdCheckEmailer;
use IdCheckModule\Services\IdCheckService;
use Nette\Object;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class PaymentListener extends Object implements EventSubscriberInterface
{
    /**
     * @var IdCheckService
     */
    private $idCheckService;

    /**
     * @var IdCheckEmailer
     */
    private $emailer;

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            EventLocator::PAYMENT_SUCCEEDED => 'onPaymentSucceeded',
            EventLocator::PAYMENT_RECURRING_COMPLETED => 'onRecurringPaymentSucceeded',
        );
    }

    /**
     * @param IdCheckService $idCheckService
     * @param IdCheckEmailer $emailer
     */
    public function __construct(IdCheckService $idCheckService, IdCheckEmailer $emailer)
    {
        $this->idCheckService = $idCheckService;
        $this->emailer = $emailer;
    }

    /**
     * @param PaymentEvent $paymentEvent
     */
    public function onPaymentSucceeded(PaymentEvent $paymentEvent)
    {
        if ($paymentEvent->isIdCheckRequiredToBeCompleted()) {
            $this->idCheckService->setIdCheckIsRequired($paymentEvent->getCustomer());
        }
    }

    /**
     * @param PaymentEvent $paymentEvent
     */
    public function onRecurringPaymentSucceeded(PaymentEvent $paymentEvent)
    {
        if ($paymentEvent->isIdCheckRequiredToBeCompleted() && !$paymentEvent->isIdCheckRequiredForCustomer()) {
            $this->idCheckService->setIdCheckIsRequired($paymentEvent->getCustomer());
            if ($paymentEvent->isUkCustomer()) {
                $this->emailer->sendUkReminder($paymentEvent->getCustomer());
            } else {
                $this->emailer->sendAbroadReminder($paymentEvent->getCustomer());
            }
        }
    }
}