<?php

namespace IdCheckModule\Repositories;

use Doctrine\ORM\Internal\Hydration\IterableResult;
use Doctrine\ORM\NoResultException;
use Entities\Customer;
use IdCheckModule\Entities\IdCheck;
use IdCheckModule\Services\IdResponse;
use Repositories\BaseRepository_Abstract;
use Utils\Date;

class IdCheckRepository extends BaseRepository_Abstract
{
    /**
     * @param Customer $customer
     * @return int
     */
    public function countIdChecks(Customer $customer)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('count(i)')
            ->from($this->_entityName, 'i')
            ->where('i.customer = :customer')
            ->setParameter('customer', $customer);
        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param Customer $customer
     * @return IdCheck
     * @throws NoResultException
     */
    public function getLastIdCheck(Customer $customer)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('i')
            ->from($this->_entityName, 'i')
            ->where('i.customer = :customer')
            ->setParameter('customer', $customer)
            ->orderBy('i.idCheckId', 'DESC')
            ->setMaxResults(1);
        return $qb->getQuery()->getSingleResult();
    }
}
