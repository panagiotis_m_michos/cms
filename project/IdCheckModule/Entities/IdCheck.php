<?php

namespace IdCheckModule\Entities;

use DateTime;
use Doctrine\ORM\Mapping as Orm;
use Entities\Customer;
use Gedmo\Mapping\Annotation as Gedmo;
use IdCheckModule\DataObjects\IdResponse;
use Nette\Object;

/**
 * @Orm\Entity(repositoryClass = "IdCheckModule\Repositories\IdCheckRepository")
 * @Orm\Table(name="cms2_id_checks")
 * @Gedmo\Loggable(logEntryClass="LoggableModule\Entities\LogEntry")
 */
class IdCheck extends Object
{
    const TYPE_PASSPORT = 'PASSPORT';
    const TYPE_LICENSE = 'LICENSE';
    const TYPE_EUROPEAN_CARD = 'EUROPEAN_CARD';

    /**
     * @var array
     */
    public static $documentTypes = [
        self::TYPE_PASSPORT => 'Passport',
        self::TYPE_LICENSE => 'UK license',
        self::TYPE_EUROPEAN_CARD => 'European Id',
    ];

    /**
     * points represent manual validation
     */
    const MANUAL_POINTS = 20000;

    /**
     * @var int
     *
     * @Orm\Column(type="integer")
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $idCheckId;

    /**
     * @var int
     *
     * @Orm\Column(type="integer")
     * @Gedmo\Versioned
     */
    private $points;

    /**
     * @var string
     *
     * @Orm\Column(type="string")
     * @Gedmo\Versioned
     */
    private $documentTypeId;

    /**
     * @var string
     *
     * @Orm\Column(type="object")
     */
    private $response;

    /**
     * @var Customer
     * @Orm\ManyToOne(targetEntity="Entities\Customer")
     * @Orm\JoinColumn(name="customerId", referencedColumnName="customerId")
     * @Gedmo\Versioned
     */
    private $customer;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;

    private function __construct()
    {
    }

    /**
     * @param Customer $customer
     * @param IdResponse $response
     * @return IdCheck
     */
    public static function fromResponse(Customer $customer, IdResponse $response)
    {
        $self = new self();
        $self->customer = $customer;
        $self->points = (int) $response->getPoints();
        $self->documentTypeId = $response->getDocumentType();
        $self->response = $response;
        return $self;
    }

    /**
     * @param Customer $customer
     * @return IdCheck
     */
    public static function fromCustomer(Customer $customer)
    {
        $self = new self();
        $self->customer = $customer;
        return $self;
    }

    /**
     * @return int
     */
    public function getIdCheckId()
    {
        return $this->idCheckId;
    }

    /**
     * @return int
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * @return string
     */
    public function getDocumentTypeId()
    {
        return $this->documentTypeId;
    }

    /**
     * @return string
     */
    public function getDocumentType()
    {
        return isset(self::$documentTypes[$this->documentTypeId]) ? self::$documentTypes[$this->documentTypeId] : '';
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @return DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @param int $points
     */
    public function setPoints($points)
    {
        $this->points = $points;
    }

    /**
     * @param DateTime $dtc
     */
    public function setDtc($dtc)
    {
        $this->dtc = $dtc;
    }

    /**
     * @return IdResponse
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return string
     */
    public function getPointsMessage()
    {
        if ($this->getPoints() == self::MANUAL_POINTS) {
            return 'Manual Validation';
        } elseif ($this->isValid()) {
            return 'Validation passed';
        } elseif ($this->isOnlyDocumentAndNameValid()) {
            return 'Address validation failed';
        } elseif ($this->isOnlyDocumentAndAddressValid()) {
            return 'Name validation failed';
        } elseif ($this->isOnlyDocumentValid()) {
            return 'Name and Address validations failed';
        } elseif ($this->isOnlyNameAndAddressValid()) {
            return 'Document validation failed';
        } elseif ($this->isOnlyNameValid()) {
            return 'Document and Address validations failed';
        } elseif ($this->isOnlyAddressValid()) {
            return 'Document and Name validations failed';
        } else {
            return 'Validation failed';
        }
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        return ($this->getPoints() >= 610 && $this->getPoints() <= 650) || $this->getPoints() === self::MANUAL_POINTS;
    }

    /**
     * @return bool
     */
    public function isOnlyDocumentAndNameValid()
    {
        return $this->getPoints() == 350;
    }

    /**
     * @return bool
     */
    public function isOnlyDocumentAndAddressValid()
    {
        return ($this->getPoints() >= 351 && $this->getPoints() <= 366);
    }

    /**
     * @return bool
     */
    public function isOnlyDocumentValid()
    {
        return ($this->getPoints() >= 2300 && $this->getPoints() <= 2350);
    }

    /**
     * @return bool
     */
    public function isOnlyNameAndAddressValid()
    {
        return ($this->getPoints() >= 311 && $this->getPoints() <= 556);
    }

    /**
     * @return bool
     */
    public function isOnlyNameValid()
    {
        return ($this->getPoints() >= 51 && $this->getPoints() <= 66);
    }

    /**
     * @return bool
     */
    public function isOnlyAddressValid()
    {
        return ($this->getPoints() >= 11 && $this->getPoints() <= 256);
    }
}
