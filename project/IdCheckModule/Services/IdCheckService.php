<?php

namespace IdCheckModule\Services;

use DateTime;
use Doctrine\ORM\NoResultException;
use Entities\Customer;
use Id3GlobalApiClient\Exceptions\CommunicationException;
use Id3GlobalApiClient\Interfaces\IId3GlobalApiClient;
use IdCheckModule\DataObjects\IdRequest;
use IdCheckModule\DataObjects\IdResponse;
use IdCheckModule\Entities\IdCheck;
use IdCheckModule\Exceptions\IdCheckException;
use IdCheckModule\Repositories\IdCheckRepository;
use Nette\Object;
use AspectModule\Annotations\Loggable;

class IdCheckService extends Object
{
    const MAX_CHECKS = 3;

    /**
     * @var IdCheckRepository
     */
    private $idCheckRepository;

    /**
     * @var IId3GlobalApiClient
     */
    private $client;

    /**
     * @param IdCheckRepository $idCheckRepository
     * @param IId3GlobalApiClient $client
     */
    public function __construct(IdCheckRepository $idCheckRepository, IId3GlobalApiClient $client)
    {
        $this->idCheckRepository = $idCheckRepository;
        $this->client = $client;
    }

    /**
     * @param Customer $customer
     * @return IdCheck
     * @throws IdCheckException
     */
    public function getLastIdCheck(Customer $customer)
    {
        try {
            return $this->idCheckRepository->getLastIdCheck($customer);
        } catch (NoResultException $e) {
            throw IdCheckException::notFound($customer);
        }
    }

    /**
     * @param Customer $customer
     * @return Customer
     */
    public function setIdCheckIsRequired(Customer $customer)
    {
        $customer->markIdCheckRequired();

        return $this->idCheckRepository->saveEntity($customer);
    }

    /**
     * @param Customer $customer
     * @return Customer
     */
    public function completeIdCheckForCustomer(Customer $customer)
    {
        $customer->markIdCheckNotRequired();
        $customer->setDtIdValidated(new DateTime);

        return $this->idCheckRepository->saveEntity($customer);
    }

    /**
     * @param Customer $customer
     * @param IdRequest $idRequest
     * @return Customer
     */
    public function completeIdCheckWithRequest(Customer $customer, IdRequest $idRequest)
    {
        $personal = $idRequest->getPersonal();
        $customer->setFirstName($personal->getFirstName());
        $customer->setLastName($personal->getLastName());
        $address = $idRequest->getAddress();
        $customer->setAddress1($address->getAddress1());
        $customer->setAddress2($address->getAddress2());
        $customer->setAddress3(NULL);
        $customer->setCounty(NULL);
        $customer->setCity($address->getCity());
        $customer->setPostcode($address->getPostcode());
        $customer->setCountryFromString($address->getCountry());

        return $this->completeIdCheckForCustomer($customer);
    }

    /**
     * @param Customer $customer
     * @return Customer
     */
    public function removeIdCheckRequirement(Customer $customer)
    {
        $customer->markIdCheckNotRequired();

        return $this->idCheckRepository->saveEntity($customer);
    }

    /**
     * @param Customer $customer
     * @return bool
     */
    public function hasAvailableIdChecks(Customer $customer)
    {
        return $this->idCheckRepository->countIdChecks($customer) < self::MAX_CHECKS;
    }

    /**
     * @param Customer $customer
     * @return bool
     */
    public function countIdChecks(Customer $customer)
    {
        return $this->idCheckRepository->countIdChecks($customer);
    }

    /**
     * @Loggable
     *
     * @param Customer $customer
     * @param IdRequest $idRequest
     * @return IdCheck
     * @throws CommunicationException
     */
    public function checkCustomerDetails(Customer $customer, IdRequest $idRequest)
    {
        if ($idRequest->isForInternationalPassport()) {
            $addressValidation = $this->client->authenticateAddress(
                $idRequest->getPersonal(),
                $idRequest->getAddress()
            );
            $response = IdResponse::fromAuthenticationResult($idRequest->getDocumentType(), $addressValidation);

            $documentValidation = $this->client->authenticateDocument(
                $idRequest->getPersonal(),
                $idRequest->getAddress(),
                $idRequest->getProvidedDocument()
            );
            $response->addResult($documentValidation);
        } else {
            $documentValidation = $this->client->authenticateDocument(
                $idRequest->getPersonal(),
                $idRequest->getAddress(),
                $idRequest->getProvidedDocument()
            );
            $response = IdResponse::fromAuthenticationResult($idRequest->getDocumentType(), $documentValidation);
        }

        $idCheck = IdCheck::fromResponse($customer, $response);
        $this->idCheckRepository->saveEntity($idCheck);

        return $idCheck;
    }

    /**
     * @param IdCheck $idCheck
     */
    public function completeWithDocument(IdCheck $idCheck)
    {
        $idCheck->setPoints(IdCheck::MANUAL_POINTS);
        $this->idCheckRepository->saveEntity($idCheck);
        $this->completeIdCheckForCustomer($idCheck->getCustomer());
    }
}
