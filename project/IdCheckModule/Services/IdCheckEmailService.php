<?php

namespace IdCheckModule\Services;

use Entities\Customer;
use FEmail;
use IdCheckModule\IdCheckEmailer;
use LoggableModule\Repositories\EmailLogRepository;
use Utils\Date;

class IdCheckEmailService
{
    const TYPE_UK = 'uk';
    const MAX_EMAILS = 3;

    /**
     * @var string
     */
    private $idCheckService;

    /**
     * @var string
     */
    private $idCheckEmailer;
    /**
     * @var EmailLogRepository
     */
    private $emailLogRepository;

    /**
     * @param IdCheckService $idCheckService
     * @param IdCheckEmailer $idCheckEmailer
     * @param EmailLogRepository $emailLogRepository
     */
    public function __construct(IdCheckService $idCheckService, IdCheckEmailer $idCheckEmailer, EmailLogRepository $emailLogRepository)
    {
        $this->idCheckService = $idCheckService;
        $this->idCheckEmailer = $idCheckEmailer;
        $this->emailLogRepository = $emailLogRepository;
    }

    /**
     * @param Customer $customer
     * @param string $type
     */
    public function sendSkipValidation(Customer $customer, $type)
    {
        $emailId = IdCheckEmailer::REMINDER_ABROAD;
        if ($type === self::TYPE_UK) {
            if ($this->idCheckService->hasAvailableIdChecks($customer)) {
                $emailId = IdCheckEmailer::REMINDER_UK;
            } else {
                $emailId = IdCheckEmailer::REMINDER_UK_3;
            }
        }
        $this->idCheckEmailer->sendSkipValidationEmail($customer, $emailId);
        //@TODO question this
        $this->idCheckEmailer->sendInternalIdCheckEmail($customer);
    }

    /**
     * @param Customer $customer
     * @return bool|FEmail
     */
    public function sendRecurringReminderEmail(Customer $customer)
    {
        $emailId = $customer->isUkCustomer() ? IdCheckEmailer::REMINDER_RECURRING_UK : IdCheckEmailer::REMINDER_RECURRING_ABROAD;
        $emailsSent = $this->emailLogRepository->getEmailsSent($customer, [$emailId]);
        if ($emailsSent < self::MAX_EMAILS) {
            if (!$this->emailLogRepository->hasEmailsSentAfter($customer, [$emailId], new Date('-7 days'))) {
                if ($customer->isUkCustomer()) {
                    return $this->idCheckEmailer->sendUkReminder($customer);
                } else {
                    return $this->idCheckEmailer->sendAbroadReminder($customer);
                }
            }
        }
        return FALSE;
    }

}
