<?php

namespace IdCheckModule\Forms;

use FormModule\Gender;
use FormModule\Types\UkDateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PersonalForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', 'text')
            ->add(
                'middleName',
                'text',
                [
                    'required' => FALSE,
                ]
            )
            ->add('lastName', 'text')
            ->add(
                'gender',
                'choice',
                [
                    'choices' => Gender::getNames(),
                    'placeholder' => '-- Please select --',
                ]
            )
            ->add(
                'dateOfBirth',
                new UkDateType(['attr' => ['data-year-range' => '-100:-18', 'data-default-date' => '-30y']])
            );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'personal';
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'IdCheckModule\DataObjects\Personal',
            ]
        );
    }
}
