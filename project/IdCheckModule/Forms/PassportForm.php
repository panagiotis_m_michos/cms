<?php

namespace IdCheckModule\Forms;

use FormModule\Country;
use FormModule\Types\UkDateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PassportForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'country',
                'country',
                [
                    'choices' => Country::getCountryNames(),
                ]
            )
            ->add(
                'number',
                new PassportNumberType(),
                [
                    'label' => "Passport number",
                    'attr' => ['size' => 44, 'style' => 'width:415px;'],
                ]
            )
            ->add('expirationDate', new UkDateType(['attr' => ['data-year-range' => '+0:+50']]));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'passport';
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'IdCheckModule\DataObjects\Passport',
            ]
        );
    }
}
