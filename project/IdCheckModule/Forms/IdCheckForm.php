<?php

namespace IdCheckModule\Forms;

use IdCheckModule\DataObjects\IdRequest;
use IdCheckModule\Entities\IdCheck;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class IdCheckForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('personal', new PersonalForm())
            ->add(
                'documentType', 'choice', array(
                'choices' => IdCheck::$documentTypes,
                'multiple'  => FALSE,
                'expanded'  => TRUE,
                )
            )
            ->add('passport', new PassportForm(), array('required' => FALSE))
            ->add('drivingLicense', new UkLicenseForm(), array('required' => FALSE))
            ->add('europeanCard', new EuropeanCardForm(), array('required' => FALSE))
            ->add('address', new AddressForm(), array('label' => FALSE))
            ->add('validate', 'submit', array('label' => 'Validate', 'attr' => array('class' => 'ladda-button', 'data-style' => 'expand-right', 'data-color' => 'orange', 'data-size' => 's', 'style' => 'height:auto;')));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'idCheckForm';
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
            'validation_groups' =>  function(FormInterface $form) {
                $groups = array('Default');
                $idRequest = $form->getData();
                if ($idRequest->getDocumentType()) {
                    $groups[] = strtolower($idRequest->getDocumentType());
                }
                return $groups;
            },
            )
        );
    }
}
