<?php

namespace IdCheckModule\Forms;

use FormModule\Country;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AddressForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'search',
                'text',
                [
                    'mapped' => FALSE,
                    'required' => FALSE,
                    'attr' =>
                        [
                            'placeholder' => 'Start typing a postcode, street or address',
                            'size' => 40,
                        ],
                ]
            )
            ->add(
                'address1',
                'text',
                [
                    'label' => 'Address',
                    'attr' => ['size' => 40],
                ]
            )
            ->add(
                'address2',
                'text',
                [
                    'label' => FALSE,
                    'required' => FALSE,
                    'attr' => ['size' => 40],
                ]
            )
            ->add(
                'city',
                'text',
                [
                    'attr' => ['size' => 40]
                ]
            )
            ->add(
                'postcode',
                'text',
                [
                    'attr' => ['size' => 40]
                ]
            )
            ->add(
                'country',
                'choice',
                [
                    'choices' => [Country::NAME_UNITED_KINGDOM => Country::NAME_UNITED_KINGDOM],
                    'disabled' => TRUE,
                ]
            );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'address';
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'IdCheckModule\DataObjects\Address',
            ]
        );
    }
}
