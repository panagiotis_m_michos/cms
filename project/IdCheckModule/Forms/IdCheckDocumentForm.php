<?php

namespace IdCheckModule\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Required;

class IdCheckDocumentForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'document', 'file', array(
            'required' => TRUE,
            'label' => FALSE,
            'constraints' => array(
                new File(
                    array(
                    'maxSize' => '6M',
                    'mimeTypes' => array(
                        'application/pdf',
                        'application/x-pdf',
                        'image/jpeg',
                        'image/gif',
                        'image/png',
                        'image/tiff',
                    ),
                    'mimeTypesMessage' => 'File upload failed. Please upload jpg, png, gif, tif, pdf files only.',
                    )
                )
            )
            )
        )->add('submit', 'submit', array('label' => 'Upload'));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'idCheckDocument';
    }
}