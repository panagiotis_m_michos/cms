<?php

namespace IdCheckModule\Forms;

use FormModule\Country;
use FormModule\Types\UkDateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EuropeanCardForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('line1', 'text', ['attr' => ['size' => 40, 'maxlength' => 30, 'style' => 'width:315px;']])
            ->add('line2', 'text', ['attr' => ['size' => 40, 'maxlength' => 30, 'style' => 'width:315px;']])
            ->add('line3', 'text', ['attr' => ['size' => 40, 'maxlength' => 30, 'style' => 'width:315px;']])
            ->add(
                'expirationDate',
                new UkDateType(['attr' => ['data-year-range' => "+0:+50", 'style' => 'width:218px;']])
            )
            ->add(
                'countryOfNationality',
                'country',
                [
                    'choices' => Country::getCountryNames(),
                    'placeholder' => '-- Select --',
                    'attr' => ['style' => 'width:230px;'],
                ]
            )
            ->add(
                'countryOfIssue',
                'country',
                [
                    'choices' => Country::getEuCountryNames(),
                    'placeholder' => '-- Select --',
                    'attr' => ['style' => 'width:230px;'],
                ]
            );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'europeanCard';
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'IdCheckModule\DataObjects\EuropeanCard',
            ]
        );
    }
}
