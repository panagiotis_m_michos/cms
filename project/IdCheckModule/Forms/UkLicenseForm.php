<?php

namespace IdCheckModule\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UkLicenseForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'number',
                NULL,
                [
                    'label' => 'License number',
                    'attr' => ['maxlength' => 16],
                ]
            );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'license';
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'IdCheckModule\DataObjects\License',
            ]
        );
    }
}
