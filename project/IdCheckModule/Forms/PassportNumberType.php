<?php

namespace IdCheckModule\Forms;

use IdCheckModule\DataObjects\PassportNumber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PassportNumberType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'first',
                'text',
                ['label' => FALSE, 'attr' => ['class' => 'focus-maxlength', 'maxlength' => 28, 'size' => 28]]
            )
            ->add(
                'second',
                'text',
                ['label' => FALSE, 'attr' => ['class' => 'focus-maxlength', 'data-placeholder' => str_repeat(
                    '<',
                    PassportNumber::SECOND_SIZE
                ), 'maxlength' => PassportNumber::SECOND_SIZE, 'size' => 14]]
            )
            ->add(
                'third',
                'text',
                ['label' => FALSE, 'attr' => ['class' => 'focus-maxlength', 'maxlength' => 2, 'size' => 2]]
            );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'passportNumber';
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'IdCheckModule\DataObjects\PassportNumber',
            ]
        );
    }
}
