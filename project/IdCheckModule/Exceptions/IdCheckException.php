<?php

namespace IdCheckModule\Exceptions;

use Entities\Customer;
use LogicException;

class IdCheckException extends LogicException
{
    /**
     * @param Customer $customer
     * @return IdCheckException
     */
    public static function notFound(Customer $customer)
    {
        return new self(sprintf('There are no id checks for customer %s', $customer->getId()));
    }
} 