<?php

namespace IdCheckModule;

use Cron\Commands\CommandAbstract;
use Cron\INotifier;
use IdCheckModule\Services\IdCheckEmailService;
use Psr\Log\LoggerInterface;
use Repositories\CustomerRepository;
use Utils\Date;

class ReminderCron extends CommandAbstract
{
    /**
     * @var IdCheckEmailService
     */
    private $idCheckEmailService;

    /**
     * @var CustomerRepository
     */
    private $customerRepository;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @param IdCheckEmailService $idCheckEmailService
     * @param CustomerRepository $customerRepository
     * @param INotifier $notifier
     * @param LoggerInterface $logger
     */
    public function __construct(
        IdCheckEmailService $idCheckEmailService,
        CustomerRepository $customerRepository,
        INotifier $notifier,
        LoggerInterface $logger)
    {
        $this->notifier = $notifier;
        $this->idCheckEmailService = $idCheckEmailService;
        $this->customerRepository = $customerRepository;
        $this->logger = $logger;
    }

    public function execute()
    {
        $ukReminderCount = $abroadReminderCount = 0;
        $today = new Date();
        $customers = $this->customerRepository->getCustomersWithRequiredIdCheck();
        foreach ($customers as $customer) {
            $email = $this->idCheckEmailService->sendRecurringReminderEmail($customer);
            if ($email) {
                if ($email->getId() === IdCheckEmailer::REMINDER_RECURRING_UK) {
                    $ukReminderCount++;
                } elseif ($email->getId() === IdCheckEmailer::REMINDER_RECURRING_ABROAD) {
                    $abroadReminderCount++;
                }
            }
        }
        $message = sprintf('Sent reminders UK: %d, Abroad: %d. Date: %s', $ukReminderCount, $abroadReminderCount, $today);
        $this->logger->debug($message);
        $this->notifier->triggerSuccess($this->getName(), $today->getTimestamp(), $message);
    }
}
