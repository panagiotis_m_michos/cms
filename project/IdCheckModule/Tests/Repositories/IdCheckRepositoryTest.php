<?php

namespace IdCheckModule\Tests\Repositories;

use Entities\Customer;
use EntityHelper;
use IdCheckModule\Repositories\IdCheckRepository;
use IdCheckModule\Tests\Helpers\IdCheckTestHelper;
use PHPUnit_Framework_TestCase;

class IdCheckRepositoryTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var IdCheckRepository
     */
    private $object;

    /**
     * @var Customer
     */
    private $customer;

    public function setUp()
    {
        EntityHelper::emptyTables(array(TBL_CUSTOMERS, TBL_ID_CHECKS));
        $this->customer = EntityHelper::createCustomer();
        $this->object = EntityHelper::getService('id_check_module.repositories.id_check_repository');
    }

    /**
     * @covers IdCheckModule\Repositories\IdCheckRepository::countIdChecks
     */
    public function testCountIdChecks()
    {
        $ids[] = IdCheckTestHelper::success($this->customer);
        $ids[] = IdCheckTestHelper::failure($this->customer);
        $ids[] = IdCheckTestHelper::success($this->customer);
        EntityHelper::save($ids);
        $this->assertEquals(3, $this->object->countIdChecks($this->customer));
    }

    /**
     * @covers IdCheckModule\Repositories\IdCheckRepository::getLastIdCheck
     */
    public function testGetLastIdCheck()
    {
        $id = IdCheckTestHelper::success($this->customer);
        EntityHelper::save(array($id));
        $this->assertEquals($id, $this->object->getLastIdCheck($this->customer));
        $id = IdCheckTestHelper::success($this->customer);
        EntityHelper::save(array($id));
        $this->assertEquals($id, $this->object->getLastIdCheck($this->customer));
        $id = IdCheckTestHelper::success($this->customer);
        EntityHelper::save(array($id));
        $this->assertEquals($id, $this->object->getLastIdCheck($this->customer));
    }

    /**
     * @covers IdCheckModule\Repositories\IdCheckRepository::getLastIdCheck
     * @expectedException Doctrine\ORM\NoResultException
     */
    public function testGetLastIdCheckFailure()
    {
        $this->object->getLastIdCheck($this->customer);
    }
}