<?php

namespace IdCheckModule\Tests\Helpers;

use Entities\Customer;
use Id3GlobalApiClient\Responses\AuthenticationResult;
use IdCheckModule\DataObjects\IdResponse;
use IdCheckModule\Entities\IdCheck;
use stdClass;

class IdCheckTestHelper
{

    /**
     * @param Customer $customer
     * @return IdCheck
     */
    public static function success(Customer $customer)
    {
        $result = new stdClass();
        $result->AuthenticateSPResult = new stdClass();
        $result->AuthenticateSPResult->Score = 1511;
        $idCheck = IdCheck::fromResponse($customer, IdResponse::fromAuthenticationResult(IdCheck::TYPE_PASSPORT, new AuthenticationResult($result)));
        return $idCheck;
    }

    /**
     * @param Customer $customer
     * @return IdCheck
     */
    public static function failure(Customer $customer)
    {
        $result = new stdClass();
        $result->AuthenticateSPResult = new stdClass();
        $result->AuthenticateSPResult->Score = 132;
        $idCheck = IdCheck::fromResponse($customer, IdResponse::fromAuthenticationResult(IdCheck::TYPE_PASSPORT, new AuthenticationResult($result)));
        return $idCheck;
    }
}
