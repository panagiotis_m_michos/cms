<?php

namespace IdCheckModule;

use CustomersAdminControler;
use EmailerAbstract;
use Entities\Customer;
use Entities\Service;
use FApplication;
use FEmail;
use IdCheckModule\Controllers\IdCheckController;
use IEmailService;
use InvalidArgumentException;
use Symfony\Component\Routing\Router;
use Symfony\Component\Routing\RouterInterface;

class IdCheckEmailer extends EmailerAbstract
{
    const REMINDER_UK = 1526;
    const REMINDER_UK_3 = 1528;
    const REMINDER_ABROAD = 1548;
    const REMINDER_INTERNAL = 1593;
    const REMINDER_RECURRING_UK = 1539;
    const REMINDER_RECURRING_ABROAD = 1557;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @param IEmailService $emailService
     * @param RouterInterface $router
     */
    public function __construct(IEmailService $emailService, RouterInterface $router)
    {
        parent::__construct($emailService);
        $this->router = $router;
    }

    /**
     * @param Customer $customer
     * @param $emailId
     * @throws InvalidArgumentException
     */
    public function sendSkipValidationEmail(Customer $customer, $emailId)
    {
        if (!in_array($emailId, array(self::REMINDER_UK_3, self::REMINDER_UK, self::REMINDER_ABROAD))) {
            throw new InvalidArgumentException(sprintf('Email %s is not allowed', $emailId));
        }
        $email = new FEmail($emailId);
        $email->replacePlaceHolders(
            array(
                '[FIRST_NAME]' => $customer->getFirstName(),
                '[ONLINE_VALIDATION_LINK]' => $this->router->generate(IdCheckController::PAGE_DEFAULT, [], Router::ABSOLUTE_URL)
            )
        );
        $email->addTo($customer->getEmail());
        $this->emailService->send($email, $customer);
    }

    /**
     * @param Customer $customer
     */
    public function sendInternalIdCheckEmail(Customer $customer)
    {
        $email = new FEmail(self::REMINDER_INTERNAL);
        $email->replacePlaceHolders(
            array(
                '[FULL_NAME]' => $customer->getFullName(),
                '[EMAIL]' => $customer->getEmail(),
                '[COUNTRY]' => $customer->getCountry(),
                '[CUSTOMER_PAGE]' => FApplication::$router->absoluteAdminLink(CustomersAdminControler::CUSTOMERS_PAGE . ' view', array('customer_id' => $customer->getId()))
            )
        );
        $this->emailService->send($email, $customer);
    }

    /**
     * @param Customer $customer
     * @return FEmail
     */
    public function sendUkReminder(Customer $customer)
    {
        $email = new FEmail(self::REMINDER_RECURRING_UK);
        $email->replacePlaceHolders(
            array(
                '[FIRST_NAME]' => $customer->getFirstName(),
                '[ONLINE_VALIDATION_LINK]' => $this->router->generate(IdCheckController::PAGE_DEFAULT, ['fromEmail' => TRUE], Router::ABSOLUTE_URL)
            )
        );
        $email->addTo($customer->getEmail());
        $this->emailService->send($email, $customer);
        return $email;
    }

    /**
     * @param Customer $customer
     * @return FEmail
     */
    public function sendAbroadReminder(Customer $customer)
    {
        $email = new FEmail(self::REMINDER_RECURRING_ABROAD);
        $email->replacePlaceHolders(
            array(
                '[FIRST_NAME]' => $customer->getFirstName(),
            )
        );
        $email->addTo($customer->getEmail());
        $this->emailService->send($email, $customer);
        return $email;
    }
}
