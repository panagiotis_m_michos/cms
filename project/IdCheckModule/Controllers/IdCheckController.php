<?php

namespace IdCheckModule\Controllers;

use DashboardCustomerControler;
use Entities\Customer;
use Exception;
use Exceptions\Technical\EmailException;
use FormModule\Country;
use Id3GlobalApiClient\Exceptions\CommunicationException;
use IdCheckModule\DataObjects\IdRequest;
use IdCheckModule\Forms\IdCheckForm;
use IdCheckModule\Services\IdCheckEmailService;
use IdCheckModule\Services\IdCheckService;
use Psr\Log\LoggerInterface;
use Services\ControllerHelper;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use TemplateModule\Renderers\IRenderer;

class IdCheckController
{
    const NODE_DEFAULT = 1521;
    const PAGE_DEFAULT = 'id_check_choose_id';
    const PAGE_VALID = 'id_check_valid';
    const PAGE_SKIP = 'id_check_skip_validation';
    const ID_CHECK_SESSION = 'skipIdCheckTemporary';

    /**
     * @var IRenderer
     */
    private $renderer;

    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    /**
     * @var IdCheckService
     */
    private $idCheckService;

    /**
     * @var IdCheckEmailService
     */
    private $idCheckEmailService;

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param IRenderer $renderer
     * @param ControllerHelper $controllerHelper
     * @param IdCheckService $idCheckService
     * @param IdCheckEmailService $idCheckEmailService
     * @param Customer $customer
     * @param LoggerInterface $logger
     */
    public function __construct(
        IRenderer $renderer,
        ControllerHelper $controllerHelper,
        IdCheckService $idCheckService,
        IdCheckEmailService $idCheckEmailService,
        Customer $customer,
        LoggerInterface $logger
    )
    {
        $this->renderer = $renderer;
        $this->controllerHelper = $controllerHelper;
        $this->idCheckService = $idCheckService;
        $this->idCheckEmailService = $idCheckEmailService;
        $this->customer = $customer;
        $this->logger = $logger;
    }

    /**
     * @param bool $fromEmail
     * @return RedirectResponse|Response
     * @throws CommunicationException
     * @throws Exception
     */
    public function chooseId($fromEmail = FALSE)
    {
        if (!$this->customer->isIdCheckRequired()) {
            return new RedirectResponse($this->controllerHelper->getLink(DashboardCustomerControler::DASHBOARD_PAGE));
        }

        $variables['fromEmail'] = $fromEmail;
        //form and invalid messages displaying in the same page
        if (!$this->idCheckService->hasAvailableIdChecks($this->customer)) {
            $variables['hasRemainingChecks'] = FALSE;
            $variables['showInvalid'] = TRUE;
        } else {
            $variables['hasRemainingChecks'] = TRUE;
            $idRequest = IdRequest::fromCustomerWithoutPersonal($this->customer, Country::NAME_UNITED_KINGDOM);
            $form = $this->controllerHelper->buildForm(new IdCheckForm(), $idRequest);
            if ($form->isValid()) {
                try {
                    $variables['idCheck'] = $idCheck = $this->idCheckService->checkCustomerDetails(
                        $this->customer,
                        $idRequest
                    );

                    if ($idCheck->isValid()) {
                        $this->idCheckService->completeIdCheckWithRequest($this->customer, $idRequest);
                        return new RedirectResponse($this->controllerHelper->getUrl(self::PAGE_VALID));
                    }
                    $variables['hasRemainingChecks'] = $this->idCheckService->hasAvailableIdChecks($this->customer);
                    $variables['idCheckCount'] = $this->idCheckService->countIdChecks($this->customer);
                    $variables['showInvalid'] = TRUE;
                } catch (CommunicationException $e) {
                    $this->logger->error('Id check: ' . (string) $e);
                    $this->controllerHelper->setFlashMessage('Unable to communicate with id check service. Please try again');
                    $variables['flashes'] = $this->controllerHelper->readFlashMessages();
                }
            }
            $variables['form'] = $form->createView();
        }

        return $this->renderer->render($variables);
    }

    /**
     * @return Response
     */
    public function idCheckValid()
    {
        $continueLink = $this->controllerHelper->getBacklink() ?: $this->controllerHelper->getLink(DashboardCustomerControler::DASHBOARD_PAGE);

        return $this->renderer->render(['continueLink' => $continueLink]);
    }

    /**
     * @param string $email
     * @return RedirectResponse|JsonResponse
     */
    public function skipValidation($email)
    {
        try {
            $this->idCheckEmailService->sendSkipValidation($this->customer, $email);
            $this->controllerHelper->setSessionValue(self::ID_CHECK_SESSION, TRUE);
            return $this->controllerHelper->getAjaxResponse(
                'Email reminder sent! Check your inbox.',
                $this->controllerHelper->getLink(DashboardCustomerControler::DASHBOARD_PAGE)
            );
        } catch (EmailException $e) {
            return $this->controllerHelper->getAjaxResponse($e, $this->controllerHelper->getLink(DashboardCustomerControler::DASHBOARD_PAGE));
        }
    }
}
