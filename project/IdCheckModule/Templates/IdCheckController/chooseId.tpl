{include file="@header.tpl"}

{if $visibleTitle}
    <h1 class="mleft20">{$title}</h1>
{/if}
{$text nofilter}

<div class="clear"></div>

{if !empty($showInvalid)}
    <div id="invalid-id-page">
        {include file="./invalid.tpl"}
    </div>
{/if}

{if !empty($hasRemainingChecks)}
    <link rel="stylesheet" type="text/css" href="//services.postcodeanywhere.co.uk/css/captureplus-2.30.min.css?key=jg19-yk91-hh94-xu51" />
    <script type="text/javascript" src="//services.postcodeanywhere.co.uk/js/captureplus-2.30.min.js?key=jg19-yk91-hh94-xu51"></script>

    <div id="validate-id-page" class="{if !empty($showInvalid)}paulo-js-hidden{/if}" style="padding: 0 20px 10px 20px">
        {if empty($fromEmail)}
            <div class="mbottom20">
                <p class="grey4 midfont noptop">
                    We have a legal obligation to check proof of ID and proof of address for all customers who use our <strong>address services</strong>. This is to ensure we comply with Anti-Money Laundering (AML) regulations and Know Your Customer (KYC) requirements.
                </p>
                <p class="grey4 midfont ptop15">
                    We need to check your ID because your purchase includes: Registered Office / Service Address
                </p>
                <p class="grey4 midfont ptop15">
                    <strong>NB. Your address services will not become active until your ID has been verified.</strong>
                </p>
            </div>
        {/if}
        <div id="iddocs-left">
            <h3>Where are you based?</h3>
            <div class="toggle-btn-grp cssonly paulo-hide-show" data-scroll="false" data-hide="div.idcontent" data-toggle-class="toggle-btn-on">
                <div>
                    <a class="tab-btn paulo-hide-show-button" href="#idCheck">In the UK</a>
                </div>
                <div>
                    <a class="tab-btn paulo-hide-show-button" href="#abroad">Somewhere else</a>
                </div>
            </div>
        </div>
        <div id="iddocs-right">
            <div id="idCheck" class="idcontent form">
                {$formHelper->setTheme($form, 'cms_horizontal_no_container.html.twig')}
                {$formHelper->start($form) nofilter}
                    <div class="inlineblock width450 mbottom">
                        <p>This information will be checked by ID3 Global against the UK Edited Electoral Roll and UK National Identity Register.</p>
                        <p>Fields marked with <span class="red">*</span> are required.</p>
                    </div>
                    <div class="inlineblock valign-top">
                        <img src="{$urlImgs}id3global.png" alt="">
                    </div>

                    <h3 class="iddocs-header">Personal details</h3>

                    <label>Personal ID (one of the following)<span class="red">*</span></label>
                    <div>
                        <p class="grey2">Your personal ID information won't be saved to our database.</p>
                    </div>
                    <div class="paulo-js-hidden" id="document-type">
                        {$formHelper->widget($form['documentType']) nofilter}
                    </div>

                    <div id="tabs" class="mbottom20 mtop">
                        <ul id="document-type-tab">
                          <li><a href="#passport">Passport</a></li>
                          <li><a href="#license">UK Driving Licence</a></li>
                          <li><a href="#european_card">European ID card</a></li>
                        </ul>
                        <div id="passport">
                            {$formHelper->row($form['passport']['country']) nofilter}
                            <div class="form-group{if !$form['passport']['number']->vars['valid']} has-error{/if}">
                                {$formHelper->label($form['passport']['number']) nofilter}
                                <p class="normalfont">
                                    The Passport number is the last line of text on your photo page.
                                    <a id="passport-example" class="popup-jquery-tools flink" href="#international-passport">View example</a>
                                </p>
                                <div class="input-group">
                                    {$formHelper->widget($form['passport']['number']['first']) nofilter}
                                    {$formHelper->widget($form['passport']['number']['second']) nofilter}
                                    {$formHelper->widget($form['passport']['number']['third']) nofilter}
                                </div>
                                {$formHelper->errors($form['passport']['number']['first']) nofilter}
                                {$formHelper->errors($form['passport']['number']['second']) nofilter}
                                {$formHelper->errors($form['passport']['number']['third']) nofilter}
                            </div>
                            {$formHelper->row($form['passport']['expirationDate']) nofilter}
                            <div id="international-passport" class="paulo-js-hidden overlayjs" title="Passport (example)">
                                <img src="{$urlImgs}international-passport.jpg" alt="passport example" />
                            </div>
                            <div id="ukpassport" class="paulo-js-hidden overlayjs" title="Passport (example)">
                                <img src="{$urlImgs}ukpassport.png" alt="passport example" />
                            </div>
                        </div>
                        <div id="license">
                            {$formHelper->setTheme($form['drivingLicense'], 'cms.html.twig')}
                            {$formHelper->row($form['drivingLicense'], ['label' => FALSE]) nofilter}
                            <div class="txtright">
                                <a class="popup-jquery-tools flink" href="#ukdriving">View example</a>
                            </div>
                            <div id="ukdriving" class="paulo-js-hidden overlayjs" title="UK Driving Licence (example)">
                                <img src="{$urlImgs}ukdriving.png" alt="" />
                            </div>

                        </div>
                        <div id="european_card">
                            <div class="txtright">
                                <a class="popup-jquery-tools flink" href="#euid">View example</a>
                            </div>
                            {$formHelper->setTheme($form['europeanCard'], 'cms.html.twig')}
                            {$formHelper->row($form['europeanCard'], ['label' => FALSE]) nofilter}
                            <div id="euid" class="paulo-js-hidden overlayjs" title="EU ID (example)">
                                <img src="{$urlImgs}euid.png" alt="" />
                            </div>
                        </div>
                    </div>

                    <label>Name <span class="red">*</span></label>
                    <p class="grey2">
                        Please write your name exactly as shown on your <span class="selected-document-type">Passport</span>.
                    </p>
                    <div class="form-group inlineblock valign-top {if !$form['personal']['firstName']->vars['valid']}has-error{/if}">
                        <label class="grey2">First name</label>
                        <div>
                            {$formHelper->widget($form['personal']['firstName']) nofilter}
                            {$formHelper->errors($form['personal']['firstName']) nofilter}
                        </div>
                    </div>
                    <div class="form-group inlineblock valign-top {if !$form['personal']['middleName']->vars['valid']}has-error{/if}">
                        <label class="grey2">Middle name</label>
                        <div>
                            {$formHelper->widget($form['personal']['middleName']) nofilter}
                            {$formHelper->errors($form['personal']['middleName']) nofilter}
                        </div>
                    </div>
                    <div class="form-group inlineblock valign-top {if !$form['personal']['lastName']->vars['valid']}has-error{/if}">
                        <label class="grey2">Last name</label>
                        <div>
                            {$formHelper->widget($form['personal']['lastName']) nofilter}
                            {$formHelper->errors($form['personal']['lastName']) nofilter}
                        </div>
                    </div>
                    {$formHelper->row($form['personal'], ['label' => FALSE]) nofilter}

                    <h3 class="iddocs-header">Home Address</h3>
                    <div class="">
                        <p class="grey2">Your address will be checked by ID3 Global against the UK Edited Electoral Roll and UK National Identity Register.
                            <span style="color:#666;font-weight:bold">Please ensure the address is associated to your name against one of these registers.</span>
                            If you have not registered your name against your current address, you may need to enter an old address to validate.
                        </p>
                    </div>
                    <div class="completion-address-prefiller">{$formHelper->row($form['address']['search'], ['label' => 'Find your address', 'required' => TRUE]) nofilter}</div>
                    <div class="completion-address-details" style="display:none">
                        <div><strong>Address</strong> <span class="red">*</span></div>
                        <div class="completion-address-data"></div>
                        <div>or <a href="javascript:;" class="paulo-hide-show-button" data-show=".completion-address-prefiller" data-hide=".completion-address-details">find another address</a></div>
                    </div>
                    <div class="paulo-js-hidden">
                        {$formHelper->row($form['address']) nofilter}
                    </div>
                    <div class="center">
                        {$formHelper->widget($form['validate']) nofilter}
                    </div>
                 {$formHelper->end($form) nofilter}
            </div>
            <div id="abroad" class="idcontent">
                <p class="mbottom20">You need to send the following documents to <a href="mailto:id-check@companiesmadesimple.com">id-check@companiesmadesimple.com</a></p>
                <h3>Documents checklist:</h3>
                <ul>
                    <li class="cbox">
                        <h4>Proof of ID (one of the following)</h4>
                        <ul>
                             <li>Passport</li>
                             <li>Driving licence (with photocard)</li>
                             <li>National identity card</li>
                             <li>HM Forces identity card</li>
                             <li>Employment identification card</li>
                             <li>Disabled drivers blue pass</li>
                        </ul>
                    </li>
                    <li class="cbox">
                        <h4>Proof of Address (one of the following)</h4>
                        <ul>
                            <li>Gas or electricity bill</li>
                            <li>Telephone bill (excluding mobile phone bill)</li>
                            <li>Water bill</li>
                            <li>Mortgage statement</li>
                            <li>Council tax bill</li>
                            <li>Bank statement</li>
                            <li>TV licence</li>
                        </ul>
                        <p>Note: The document must be dated within the last 3 months and it must show both name and address.</p>
                    </li>
                    <li class="cbox">
                        <h4>Documents are notarised</h4>
                        <p>Both documents must be notarised by a Public Notary.</p>
                    </li>
                </ul>
                <div class="midfont">
                    <p>
                        <strong>Want to do it later?</strong> No problem! We can remind you.
                    </p>
                    <p class="mtop mbottom">
                        <div id="id-abroad-check-message" class="hidden-msg">
                            <p class="mbottom20 displayblock greenflash">Email reminder sent! Check your inbox.</p>
                            <a class="orange-submit" href="{$this->router->link('DashboardCustomerControler::DASHBOARD_PAGE')}">Continue <i class="fa fa-angle-right"></i></a>
                        </div>
                        <form id="id-check-abroad-form" class="paulo-ajax-form" data-show="#id-abroad-check-message" data-hide="#id-check-abroad-form" action="{url route="id_check_skip_validation" email="abroad"}" method="post">
                            <button class="ladda-button" data-style="expand-right" data-color="blue" data-size="s" type="submit">
                                <span class="ladda-label">Send me an email reminder with the documents list <i class="fa fa-angle-right"></i></span>
                            </button>
                        </form>
                    </p>
                    <p class="grey2">
                        We have a legal obligation to check proof of ID and proof of address for all of our customers. This is to ensure we comply with Anti-Money Laundering (AML) regulations and Know Your Customer (KYC) requirements. <strong>Your service will not be active until your ID has been received and accepted.</strong>
                    </p>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
{/if}

<script type="text/javascript">
    cms.idCheck.init();
</script>

{include file="@footer.tpl"}
