
{if !empty($idCheck)}
    {assign "documentType" $idCheck->getDocumentType()}

    <div class="m20">
        {if $idCheck->isOnlyDocumentAndNameValid()}
            <div class="warningbox">
                <span class="inlineblock fleft mright"><i class="fa fa-exclamation fa-3x yellow"></i></span>
                <p class="displaytable">
                    <strong>Sorry, the validation of the Address provided has failed</strong>
                </p>
                <ul class="displaytable pleft20 lheight20">
                    <li class="pbtm10">The {$documentType} and Name you provided have passed the validation but the <strong>address is not matching</strong> the information on the databases we are querying against.</li>
                    <li class="pbtm10">Usually this problem happens because the address provided does not match an existing address (based on the registers: UK Electoral Roll or UK National Identity Register). Make sure you use the address finder to locate your exact address and double check the entered address for typos or misspellings.</li>
                    <li class="pbtm10">You may want to try again with an updated address.</li>
                    <li>This was try number {$idCheckCount}.</li>
                </ul>
            </div>
        {elseif $idCheck->isOnlyDocumentAndAddressValid()}
            <div class="warningbox">
                <span class="inlineblock fleft mright"><i class="fa fa-exclamation fa-3x yellow"></i></span>
                <p class="displaytable">
                    <strong>Sorry, the validation of the Name provided has failed</strong>
                </p>
                <ul class="displaytable pleft20 lheight20">
                    <li class="pbtm10">The {$documentType} and Address you provided have passed the validation but the <strong>name you provided is not matching</strong> the information on the databases we are querying against.</li>
                    <li class="pbtm10">Usually this problem happens because the name you provided is not registered at this address (based on the registers: UK Electoral Roll or UK National Identity Register). You may need to use an older address where you are registered on any of the registers.</li>
                    <li class="pbtm10">Make sure that no titles (e.g.: Mr, Ms) are included in the name field, try also replacing any abbreviation with the full name.</li>
                    <li>This was try number {$idCheckCount}.</li>
                </ul>
            </div>
        {elseif $idCheck->isOnlyDocumentValid()}
            <div class="warningbox">
                <span class="inlineblock fleft mright"><i class="fa fa-exclamation fa-3x yellow"></i></span>
                <p class="displaytable">
                    <strong>Sorry, the validation of the Name and Address provided have failed</strong>
                </p>
                <ul class="displaytable pleft20 lheight20">
                    <li class="pbtm10">The {$documentType} you provided has passed the validation but the <strong>name and address are not matching</strong> the information on the databases we are querying against.</li>
                    <li class="pbtm10">The <strong>name validation</strong> can fail because the name you provided is not registered at the address provided (based on the registers: UK Electoral Roll or UK National Identity Register). You may need to use an older address where you are registered on any of the registers.</li>
                    <li class="pbtm10">Also make sure that no titles (e.g.: Mr, Ms) are included in the name field, try also replacing any abbreviation with the full name.</li>
                    <li class="pbtm10">The <strong>address validation</strong> can fail because the address provided does not match an existing address. Make sure you use the address finder to locate your exact address and double check the entered address for typos or misspellings.</li>
                    <li class="pbtm10">You may want to try again with an updated name and address.</li>
                    <li>This was try number {$idCheckCount}.</li>
                </ul>
            </div>
        {elseif $idCheck->isOnlyNameAndAddressValid()}
            <div class="warningbox">
                <span class="inlineblock fleft mright"><i class="fa fa-exclamation fa-3x yellow"></i></span>
                <p class="displaytable">
                    <strong>Sorry, the validation of the {$documentType} provided has failed</strong>
                </p>
                <ul class="displaytable pleft20 lheight20">
                    <li class="pbtm10">The name and address you provided have passed the validation but the <strong>{$documentType} is not matching</strong> the information on the databases we are querying against.</li>
                    <li class="pbtm10">Usually this problem is related with typos.</li>
                    <li class="pbtm10">You may want to try again and double check all information entered.</li>
                    <li>This was try number {$idCheckCount}.</li>
                </ul>
            </div>
        {elseif $idCheck->isOnlyNameValid()}
            <div class="warningbox">
                <span class="inlineblock fleft mright"><i class="fa fa-exclamation fa-3x yellow"></i></span>
                <p class="displaytable">
                    <strong>Sorry, the validation of the {$documentType} and Address provided have failed</strong>
                </p>
                <ul class="displaytable pleft20 lheight20">
                    <li class="pbtm10">The name you provided has passed the validation but the <strong>{$documentType} and address are not matching</strong> the information on the databases we are querying against.</li>
                    <li class="pbtm10">The <strong>address validation</strong> can fail because the address provided does not match an existing address. Make sure you use the address finder to locate your exact address and double check the entered address for typos or misspellings.</li>
                    <li class="pbtm10">The {$documentType} validation fail is usually related with typos. Please double check all information entered.</li>
                    <li class="pbtm10">You may want to try again with an updated {$documentType} and address information.</li>
                    <li>This was try number {$idCheckCount}.</li>
                </ul>
            </div>
        {elseif $idCheck->isOnlyAddressValid()}
            <div class="warningbox">
                <span class="inlineblock fleft mright"><i class="fa fa-exclamation fa-3x yellow"></i></span>
                <p class="displaytable">
                    <strong>Sorry, the validation of the {$documentType} and Name have failed</strong>
                </p>
                <ul class="displaytable pleft20 lheight20">
                    <li class="pbtm10">The address you provided has passed the validation but the <strong>{$documentType} and name are not matching</strong> the information on the databases we are querying against.</li>
                    <li class="pbtm10">The <strong>name validation</strong> can fail because the name you provided is not registered at the address provided (based on the registers: UK Electoral Roll or UK National Identity Register). You may need to use an older address where you are registered on any of the registers.</li>
                    <li class="pbtm10">Also make sure that no titles (e.g.: Mr, Ms) are included in the name field, try also replacing any abbreviation with the full name.</li>
                    <li class="pbtm10">The {$documentType} validation fail is usually related with typos. Please double check all information entered.</li>
                    <li class="pbtm10">You may want to try again with an updated {$documentType} and name information.</li>
                    <li>This was try number {$idCheckCount}.</li>
                </ul>
            </div>
        {else}
            <div class="failedbox">
                <span class="inlineblock fleft mright"><i class="fa fa-times fa-3x red2"></i></span>
                <p class="displaytable">
                    <strong>Sorry, the validation of the Name, Address and {$documentType} have failed</strong>
                </p>
                <ul class="displaytable pleft20 lheight20">
                    <li class="pbtm10">The information you provided does not match the information on the databases we are querying against.</li>
                    <li class="pbtm10">The <strong>name validation</strong> can fail because the name you provided is not registered at the address provided (based on the registers: UK Electoral Roll or UK National Identity Register). You may need to use an older address where you are registered on any of the registers.</li>
                    <li class="pbtm10">Also make sure that no titles (e.g.: Mr, Ms) are included in the name field, try also replacing any abbreviation with the full name.</li>
                    <li class="pbtm10">The <strong>address validation</strong> can fail because the address provided does not match an existing address. Make sure you use the address finder to locate your exact address and double check the entered address for typos or misspellings.</li> 
                    <li class="pbtm10">The {$documentType} validation fail is usually related with typos. Please double check all information entered.</li>
                    <li class="pbtm10">You can either try again (up to 3 times) with updated information or use the manual ID check process as explained at the bottom of this page.</li>
                    <li>This was try number {$idCheckCount}.</li>
                </ul>
            </div>
        {/if}
    </div>
{/if}

{if $hasRemainingChecks}
    <div class="m20">
        <a id="retry-id-check" class="orange-submit" href="#idCheck" data-show="#validate-id-page" data-hide="#invalid-id-page"><i class="fa fa-angle-left"></i> Try again</a>
    </div>
{else}
    <div class="m20">
        <div>
            <div class="failedbox">
                <span class="inlineblock fleft mright"><i class="fa fa-times fa-3x red2"></i></span>
                <p class="displaytable">
                    <strong>Sorry, your ID validation has failed 3 times</strong>
                </p>
                <ul class="displaytable pleft20">
                    <li class="pbtm10">You have tried to validate your ID information 3 times.</li>
                    <li>Please use the manual ID check as described at the bottom of this page and we will assist you.</li>
                </ul>
            </div>
        </div>
    </div>
{/if}

<div class="m20 grey2 midfont">
    <p class="mtop100 black">
        <strong>Proof of ID not validating?</strong>
    </p>
    <p>
        If you are having problems to validate your ID you can try again or you can send us copies of your documents so we can validate it manually for you.
    </p>
    <p class="mtop mbottom">
        <div id="id-check-message" class="hidden-msg">
            <p class="mbottom20 displayblock greenflash">Email reminder sent! Check your inbox.</p>
            <a class="orange-submit" href="{$this->router->link('DashboardCustomerControler::DASHBOARD_PAGE')}">Continue <i class="fa fa-angle-right"></i></a>
        </div>
        <div id="skip-email-validation-container" class="mtop">
            <form class="paulo-ajax-form" data-show="#id-check-message" data-hide="#skip-email-validation-container"
                  action="{url route="id_check_skip_validation" email="uk"}" method="post">
                <button class="ladda-button" data-style="expand-right" data-color="blue" data-size="s" type="submit">
                    <span class="ladda-label">I will provide my proof of ID later, proceed <i class="fa fa-angle-right"></i></span>
                </button>
            </form>
        </div>
    </p>
    <p>
        We have a legal obligation to check proof of ID and proof of address for all of our customers.
        This is to ensure we comply with Anti-Money Laundering (AML) regulations and Know Your Customer (KYC) requirements. <strong>Your address services will not become active until your ID has been received and accepted.</strong>
    </p>
</div>
