{include file="@header.tpl"}
{if $visibleTitle}
    <h1 class="mleft20">{$title}</h1>
{/if}
<div class="m20">
    <div>
        <div class="successbox">
            <span class="inlineblock fleft mright"><i class="fa fa-check fa-3x green1"></i></span>
            <p class="displaytable">
                <strong>Your ID was successfully validated!</strong><br /><br />
            You can start using your address services.
            </p>
        </div>
    </div>
    <div class="mtop20">
        <a class="orange-submit" href="{$continueLink}">Continue <i class="fa fa-angle-right"></i></a>
    </div>
</div>
{include file="@footer.tpl"}