<?php

namespace IdCheckModule\Validators\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Customer;

class NotTitleNorSingleCharValidator extends ConstraintValidator
{
    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        $titles = [];
        foreach (Customer::$titles as $uppercaseTitle) {
            $titles[] = $uppercaseTitle;
            $titles[] = strtolower($uppercaseTitle);
        }
        $parts = explode(" ", $value);
        foreach ($parts as $part) {
            if (in_array($value, $titles) || strlen($part) == 1) {
                $this->context->buildViolation($constraint->message)
                    ->setParameter('%string%', $value)
                    ->addViolation();
            }
        }
    }
}
