<?php

namespace IdCheckModule\Validators\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class NotTitleNorSingleChar extends Constraint
{
    /**
     * @var string
     */
    public $message = 'Single letters or titles are not accepted in the first name. Please insert your full first name without the title or abbreviations. (e.g.: instead of **Mr P** use **Paul**)';

}
