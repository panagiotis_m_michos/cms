<?php

namespace IdCheckModule\DataObjects;

use Utils\Date;

class Passport
{
    /**
     * @var PassportNumber
     */
    private $number;

    /**
     * @var Date
     */
    private $expirationDate;

    /**
     * @var string
     */
    private $country;

    /**
     * @return PassportNumber
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @return string
     */
    public function getUkNumber()
    {
        return $this->number->getFirst() . $this->number->getThird();
    }

    /**
     * @return string
     */
    public function getInternationalNumber()
    {
        return $this->number->getFirst() . $this->number->getSecond() . $this->number->getThird();
    }

    /**
     * @param PassportNumber $number
     */
    public function setNumber(PassportNumber $number)
    {
        $this->number = $number;
    }

    /**
     * @return Date
     */
    public function getExpirationDate()
    {
        return $this->expirationDate;
    }

    /**
     * @param Date $expirationDate
     */
    public function setExpirationDate(Date $expirationDate)
    {
        $this->expirationDate = $expirationDate;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }
}
