<?php

namespace IdCheckModule\DataObjects;

use Id3GlobalApiClient\Interfaces\IDrivingLicense;

class License implements IDrivingLicense
{
    /**
     * @var string
     */
    private $number;

    /**
     * @var string
     */
    private $mailSort;

    /**
     * @var string
     */
    private $postcode;

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return string
     */
    public function getMailSort()
    {
        return $this->mailSort;
    }

    /**
     * @param string $mailSort
     */
    public function setMailSort($mailSort)
    {
        $this->mailSort = $mailSort;
    }

    /**
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @param string $postcode
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    }
}
