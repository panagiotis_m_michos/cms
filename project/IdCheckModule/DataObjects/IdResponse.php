<?php

namespace IdCheckModule\DataObjects;

use Id3GlobalApiClient\Interfaces\IAuthenticationResult;
use Nette\Object;

class IdResponse extends Object
{
    /**
     * @var string
     */
    private $documentType;

    /**
     * @var IAuthenticationResult[]
     */
    private $authenticationResults;

    private function __construct()
    {
    }

    /**
     * @param string $documentType
     * @param IAuthenticationResult $authenticationResult
     * @return IdResponse
     */
    public static function fromAuthenticationResult($documentType, IAuthenticationResult $authenticationResult)
    {
        $self = new self();
        $self->authenticationResults[] = $authenticationResult;
        $self->documentType = $documentType;
        return $self;
    }

    /**
     * @param IAuthenticationResult $authenticationResult
     */
    public function addResult(IAuthenticationResult $authenticationResult)
    {
        $this->authenticationResults[] = $authenticationResult;
    }

    /**
     * @return int
     */
    public function getPoints()
    {
        $points = 0;
        foreach ($this->authenticationResults as $result) {
            $points += $result->getScore();
        }
        return $points;
    }

    /**
     * @return string
     */
    public function getDocumentType()
    {
        return $this->documentType;
    }

    /**
     * @return IAuthenticationResult[]
     */
    public function getResults()
    {
        return $this->authenticationResults;
    }

    /**
     * @return array
     */
    public function getMessages()
    {
        return array();// $this->authenticationResult->getMessages();
    }
}
