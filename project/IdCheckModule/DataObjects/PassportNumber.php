<?php

namespace IdCheckModule\DataObjects;

class PassportNumber
{
    const SECOND_SIZE = 14;

    /**
     * @var string
     */
    private $first;

    /**
     * @var string
     */
    private $second;

    /**
     * @var string
     */
    private $third;

    /**
     * @return PassportNumber
     */
    public static function createUk()
    {
        $self = new self();
        $self->second = str_repeat('<', self::SECOND_SIZE);
        return $self;
    }

    /**
     * @return string
     */
    public function getFirst()
    {
        return $this->first;
    }

    /**
     * @param string $first
     */
    public function setFirst($first)
    {
        $this->first = $first;
    }

    /**
     * @return string
     */
    public function getSecond()
    {
        return $this->second;
    }

    /**
     * @param string $second
     */
    public function setSecond($second)
    {
        $this->second = $second;
    }

    /**
     * @return string
     */
    public function getThird()
    {
        return $this->third;
    }

    /**
     * @param string $third
     */
    public function setThird($third)
    {
        $this->third = $third;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->first . $this->second . $this->third;
    }
}
