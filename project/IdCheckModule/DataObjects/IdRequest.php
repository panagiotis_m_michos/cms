<?php

namespace IdCheckModule\DataObjects;

use Entities\Customer;
use FormModule\Country;
use Id3GlobalApiClient\Entities\InternationalPassport;
use Id3GlobalApiClient\Entities\UkPassport;
use IdCheckModule\Entities\IdCheck;
use IdCheckModule\Forms\PassportNumberType;
use UnexpectedValueException;

class IdRequest
{
    /**
     * @var string
     */
    private $documentType;

    /**
     * @var Personal
     */
    private $personal;

    /**
     * @var License
     */
    private $drivingLicense;

    /**
     * @var Passport
     */
    private $passport;

    /**
     * @var EuropeanCard
     */
    private $europeanCard;

    /**
     * @var Address
     */
    private $address;

    /**
     * @param Customer $customer
     * @param $defaultCountry
     * @param Personal $personal
     */
    private function __construct(Customer $customer, $defaultCountry, Personal $personal)
    {
        $this->documentType = IdCheck::TYPE_PASSPORT;
        $this->personal = $personal;
        $this->address = Address::fromCustomer($customer, $defaultCountry);
        //if these values are not provided validation will not start for them. bug ?
        $this->passport = new Passport();
        $this->passport->setCountry($defaultCountry);
        if ($defaultCountry === Country::NAME_UNITED_KINGDOM) {
            $passportNumber = PassportNumber::createUk();
            $this->passport->setNumber($passportNumber);
        }
        $this->drivingLicense = new License();
        $this->europeanCard = new EuropeanCard();
    }

    /**
     * @param Customer $customer
     * @param string $defaultCountry
     * @return IdRequest
     */
    public static function fromCustomer(Customer $customer, $defaultCountry)
    {
        $personal = Personal::fromCustomer($customer);

        return new self($customer, $defaultCountry, $personal);
    }

    /**
     * @param Customer $customer
     * @param string $defaultCountry
     * @return IdRequest
     */
    public static function fromCustomerWithoutPersonal(Customer $customer, $defaultCountry)
    {
        $personal = new Personal();

        return new self($customer, $defaultCountry, $personal);
    }

    /**
     * @return mixed
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * @param mixed $points
     */
    public function setPoints($points)
    {
        $this->points = $points;
    }

    /**
     * @return string
     */
    public function getDocumentType()
    {
        return $this->documentType;
    }

    /**
     * @param string $documentType
     */
    public function setDocumentType($documentType)
    {
        $this->documentType = $documentType;
    }

    /**
     * @return Personal
     */
    public function getPersonal()
    {
        return $this->personal;
    }

    /**
     * @param Personal $personal
     */
    public function setPersonal(Personal $personal)
    {
        $this->personal = $personal;
    }

    /**
     * @return License
     */
    public function getDrivingLicense()
    {
        return $this->drivingLicense;
    }

    /**
     * @param License $drivingLicense
     */
    public function setDrivingLicense(License $drivingLicense)
    {
        $this->drivingLicense = $drivingLicense;
    }

    /**
     * @return Passport
     */
    public function getPassport()
    {
        return $this->passport;
    }

    /**
     * @param Passport $passport
     */
    public function setPassport(Passport $passport)
    {
        $this->passport = $passport;
    }

    /**
     * @return Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param Address $address
     */
    public function setAddress(Address $address)
    {
        $this->address = $address;
    }

    /**
     * @return EuropeanCard
     */
    public function getEuropeanCard()
    {
        return $this->europeanCard;
    }

    /**
     * @param EuropeanCard $europeanCard
     */
    public function setEuropeanCard(EuropeanCard $europeanCard)
    {
        $this->europeanCard = $europeanCard;
    }

    /**
     * @return UkPassport
     */
    public function getUkPassport()
    {
        return new UkPassport($this->passport->getUkNumber(), $this->passport->getExpirationDate());
    }

    /**
     * @return InternationalPassport
     */
    public function getInternationalPassport()
    {
        return new InternationalPassport(
            $this->passport->getCountry(),
            $this->passport->getInternationalNumber(),
            $this->passport->getExpirationDate()
        );
    }

    /**
     * @return bool
     */
    public function isForInternationalPassport()
    {
        return $this->getDocumentType() === IdCheck::TYPE_PASSPORT && $this->passport->getCountry() !== Country::NAME_UNITED_KINGDOM;
    }

    /**
     * @return EuropeanCard|License|Passport
     */
    public function getProvidedDocument()
    {
        switch ($this->getDocumentType()) {
            case IdCheck::TYPE_PASSPORT:
                if ($this->isForInternationalPassport()) {
                    return $this->getInternationalPassport();
                } else {
                    return $this->getUkPassport();
                }
                break;
            case IdCheck::TYPE_LICENSE:
                return $this->getDrivingLicense();
            case IdCheck::TYPE_EUROPEAN_CARD:
                return $this->getEuropeanCard();
            default:
                return NULL;
        }
    }

}
