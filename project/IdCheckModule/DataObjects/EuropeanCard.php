<?php

namespace IdCheckModule\DataObjects;

use Id3GlobalApiClient\Interfaces\IIdCard;
use Utils\Date;

class EuropeanCard implements IIdCard
{

    /**
     * @var string
     */
    private $line1;

    /**
     * @var string
     */
    private $line2;

    /**
     * @var string
     */
    private $line3;

    /**
     * @var Date
     */
    private $expirationDate;

    /**
     * @var string
     */
    private $countryOfNationality;

    /**
     * @var string
     */
    private $countryOfIssue;

    /**
     * @return string
     */
    public function getLine1()
    {
        return $this->line1;
    }

    /**
     * @param string $line1
     */
    public function setLine1($line1)
    {
        $this->line1 = $line1;
    }

    /**
     * @return string
     */
    public function getLine2()
    {
        return $this->line2;
    }

    /**
     * @param string $line2
     */
    public function setLine2($line2)
    {
        $this->line2 = $line2;
    }

    /**
     * @return string
     */
    public function getLine3()
    {
        return $this->line3;
    }

    /**
     * @param string $line3
     */
    public function setLine3($line3)
    {
        $this->line3 = $line3;
    }

    /**
     * @return Date
     */
    public function getExpirationDate()
    {
        return $this->expirationDate;
    }

    /**
     * @param Date $expirationDate
     */
    public function setExpirationDate(Date $expirationDate)
    {
        $this->expirationDate = $expirationDate;
    }

    /**
     * @return string
     */
    public function getCountryOfNationality()
    {
        return $this->countryOfNationality;
    }

    /**
     * @param string $countryOfNationality
     */
    public function setCountryOfNationality($countryOfNationality)
    {
        $this->countryOfNationality = $countryOfNationality;
    }

    /**
     * @return string
     */
    public function getCountryOfIssue()
    {
        return $this->countryOfIssue;
    }

    /**
     * @param string $countryOfIssue
     */
    public function setCountryOfIssue($countryOfIssue)
    {
        $this->countryOfIssue = $countryOfIssue;
    }

}
