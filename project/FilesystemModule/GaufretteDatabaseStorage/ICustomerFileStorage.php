<?php

namespace FilesystemModule\GaufretteDatabaseStorage;

use Entities\Customer;
use Symfony\Component\HttpFoundation\File\File;

interface ICustomerFileStorage
{
    /**
     * @param Customer $customer
     * @param string $key
     * @return CustomerFile
     */
    public function getFile(Customer $customer, $key);

    /**
     * @param Customer $customer
     * @param string $type
     * @param File $file
     * @return CustomerFile
     */
    public function addFile(Customer $customer, $type, File $file);

    /**
     * @param Customer $customer
     * @param string $type
     * @return CustomerFile[]
     */
    public function listFiles(Customer $customer, $type);

    /**
     * @param Customer $customer
     * @param string $key
     * @return bool
     */
    public function removeFile(Customer $customer, $key);

}