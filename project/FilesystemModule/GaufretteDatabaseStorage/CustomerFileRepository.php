<?php

namespace FilesystemModule\GaufretteDatabaseStorage;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Entities\Customer;
use FilesystemModule\GaufretteDatabaseStorage\Entities\CustomerFileInfo;

class CustomerFileRepository extends EntityRepository
{
    /**
     * @param Customer $customer
     * @param string $type
     * @return IterableResult
     */
    public function findFiles(Customer $customer, $type)
    {
        $qb = $this->createQueryBuilder('f');
        $qb->where('f.customer = :customer')
            ->andWhere('f.type = :type')
            ->setParameter('customer', $customer)
            ->setParameter('type', $type);
        return $qb->getQuery()->iterate();
    }

    /**
     * @param Customer $customer
     * @param string $key
     * @return CustomerFile
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function findFile(Customer $customer, $key)
    {
        $qb = $this->createQueryBuilder('f');
        $qb->where('f.customer = :customer')
            ->andWhere('f.key = :key')
            ->setParameter('customer', $customer)
            ->setParameter('key', $key);
        return $qb->getQuery()->getSingleResult();
    }

    /**
     * @param CustomerFile $customerFile
     */
    public function saveFile(CustomerFileInfo $customerFile)
    {
        $this->_em->persist($customerFile);
        $this->_em->flush();
    }

    /**
     * @param CustomerFile $customerFile
     */
    public function removeFile(CustomerFileInfo $customerFile)
    {
        $this->_em->remove($customerFile);
        $this->_em->flush();
    }

}