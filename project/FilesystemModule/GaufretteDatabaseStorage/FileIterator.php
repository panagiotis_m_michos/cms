<?php

namespace FilesystemModule\GaufretteDatabaseStorage;

use Doctrine\ORM\Internal\Hydration\IterableResult;
use Gaufrette\Filesystem;
use IteratorIterator;

class FileIterator extends IteratorIterator
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @param IterableResult $iterableResult
     * @param Filesystem $filesystem
     */
    public function __construct(IterableResult $iterableResult, Filesystem $filesystem)
    {
        parent::__construct($iterableResult);
        $this->filesystem = $filesystem;
    }

    /**
     * @return CustomerFile
     */
    public function current()
    {
        $value = parent::current();
        return new CustomerFile($value[0], $this->filesystem);
    }
}