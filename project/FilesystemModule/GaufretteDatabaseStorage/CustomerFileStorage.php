<?php

namespace FilesystemModule\GaufretteDatabaseStorage;

use Doctrine\ORM\UnexpectedResultException;
use Entities\Customer;
use FilesystemModule\GaufretteDatabaseStorage\Entities\CustomerFileInfo;
use FilesystemModule\GaufretteDatabaseStorage\Exceptions\FileException;
use Gaufrette\Exception\FileAlreadyExists;
use Gaufrette\Filesystem;
use RuntimeException;
use SplFileInfo;
use AspectModule\Annotations\Loggable;
use Symfony\Component\HttpFoundation\File\File;

class CustomerFileStorage implements ICustomerFileStorage
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var CustomerFileRepository
     */
    private $customerFileRepository;

    /**
     * @param CustomerFileRepository $customerFileRepository
     * @param Filesystem $filesystem
     */
    public function __construct(CustomerFileRepository $customerFileRepository, Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
        $this->customerFileRepository = $customerFileRepository;
    }

    /**
     * @param Customer $customer
     * @param string $key
     * @return CustomerFile
     * @throws FileException
     */
    public function getFile(Customer $customer, $key)
    {
        try {
            $file = $this->customerFileRepository->findFile($customer, $key);
            return new CustomerFile($file, $this->filesystem);
        } catch (UnexpectedResultException $e) {
            throw FileException::notFound($customer, $key);
        }
    }

    /**
     * @Loggable
     *
     * @param Customer $customer
     * @param string $type
     * @param File $file
     * @return CustomerFile
     * @throws FileException
     */
    public function addFile(Customer $customer, $type, File $file)
    {
        try {
            $key = $this->generateFullKey($customer->getId(), $type, uniqid());
            $customerFile = CustomerFileInfo::fromKey($key, $customer, $type, $file->guessExtension());
            $this->filesystem->write($key, file_get_contents($file->getRealPath()));
            $this->customerFileRepository->saveFile($customerFile);
            return new CustomerFile($customerFile, $this->filesystem);
        } catch (FileAlreadyExists $e) {
            throw FileException::unableToCreate($customer, $key, $e);
        } catch (RuntimeException $e) {
            throw FileException::unableToCreate($customer, $key, $e);
        }
    }

    /**
     * @param Customer $customer
     * @return FileIterator
     */
    public function listFiles(Customer $customer, $type)
    {
        $iterator = $this->customerFileRepository->findFiles($customer, $type);
        return new FileIterator($iterator, $this->filesystem);
    }

    /**
     * @Loggable
     *
     * @param Customer $customer
     * @param string $key
     * @return bool
     */
    public function removeFile(Customer $customer, $key)
    {
        try {
            $file = $this->customerFileRepository->findFile($customer, $key);
            $this->customerFileRepository->removeFile($file);
            return $this->filesystem->delete($file->getKey());
        } catch (UnexpectedResultException $e) {
            throw FileException::notFound($customer, $key);
        } catch (RuntimeException $e) {
            throw FileException::notFound($customer, $key);
        }
    }


    /**
     * @param int $identifier
     * @param string $type
     * @param string $suffix
     * @return string
     */
    private function generateFullKey($identifier, $type, $suffix)
    {
        $dir = floor($identifier / 10000) + 1;
        $dir = (string) $dir . '0000';
        $path = $dir . DIRECTORY_SEPARATOR . $identifier;
        return $path . DIRECTORY_SEPARATOR . $type . DIRECTORY_SEPARATOR . $suffix;
    }

}
