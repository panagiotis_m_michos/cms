<?php

namespace FilesystemModule\GaufretteDatabaseStorage\Exceptions;

use Entities\Customer;
use UnexpectedValueException;

class FileException extends UnexpectedValueException
{
    /**
     * @param Customer $customer
     * @param string $key
     * @return FileException
     */
    public static function notFound(Customer $customer, $key)
    {
        return new self(sprintf('File with %s for customer %s does not exists', $key, $customer->getId()));
    }

    /**
     * @param Customer $customer
     * @param string $key
     * @return FileException
     */
    public static function unableToCreate(Customer $customer, $key, $previous = NULL)
    {
        return new self(sprintf('Unable to create file with %s for customer %s', $key, $customer->getId()), 0, $previous);
    }
}