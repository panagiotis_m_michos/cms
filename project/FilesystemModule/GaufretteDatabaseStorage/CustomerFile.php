<?php

namespace FilesystemModule\GaufretteDatabaseStorage;

use FilesystemModule\GaufretteDatabaseStorage\Entities\CustomerFileInfo;
use Gaufrette\Filesystem;
use Nette\Object;

class CustomerFile extends Object
{
    /**
     * @var CustomerFileInfo
     */
    private $customerFileInfo;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @param CustomerFileInfo $customerFileInfo
     * @param Filesystem $filesystem
     */
    public function __construct(CustomerFileInfo $customerFileInfo, Filesystem $filesystem)
    {
        $this->customerFileInfo = $customerFileInfo;
        $this->filesystem = $filesystem;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->customerFileInfo->getKey();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->customerFileInfo->getType() . $this->customerFileInfo->getId() . '.' . $this->customerFileInfo->getExt();
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->filesystem->read($this->customerFileInfo->getKey());
    }

}