<?php

namespace FilesystemModule\GaufretteDatabaseStorage\Entities;

use Entities\Customer;
use Doctrine\ORM\Mapping as Orm;
use Nette\Object;

/**
 * Represents data in database
 * @Orm\Table(name="cms2_customer_files")
 * @Orm\Entity(repositoryClass="FilesystemModule\GaufretteDatabaseStorage\CustomerFileRepository")
 */
class CustomerFileInfo extends Object
{
    const TYPE_ID = 'id';

    /**
     * @var integer $id
     *
     * @ORM\Column(name="customerFileId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @var string
     *
     * @Orm\Column(type="string", name="fileKey")
     */
    private $key;

    /**
     * @var string
     * @Orm\Column(type="string")
     */
    private $type;

    /**
     * @var string
     * @Orm\Column(type="string")
     */
    private $ext;

    /**
     * @var Customer
     * @Orm\ManyToOne(targetEntity="Entities\Customer")
     * @Orm\JoinColumn(name="customerId", referencedColumnName="customerId")
     */
    private $customer;

    private function __construct()
    {
    }

    /**
     * @param string $key
     * @param Customer $customer
     * @param string $type
     * @param string $ext
     * @return CustomerFileInfo
     */
    public static function fromKey($key, Customer $customer, $type, $ext)
    {
        $self = new self();
        $self->key = $key;
        $self->customer = $customer;
        $self->type = $type;
        $self->ext = $ext;
        return $self;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getExt()
    {
        return $this->ext;
    }


    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}