<?php

namespace FilesystemModule\Tests\GaufretteDatabaseStorage;

use DiLocator;
use EntityHelper;
use FilesystemModule\GaufretteDatabaseStorage\Entities\CustomerFile;
use PHPUnit_Framework_TestCase;
use Tests\Helpers\ObjectHelper;

class CustomerFileRepositoryTest extends PHPUnit_Framework_TestCase
{

    /**
     * @var Customer[]
     */
    private $customers;

    /**
     * @var CustomerFile[]
     */
    private $files;

    public function setUp()
    {
        EntityHelper::emptyTables(array(TBL_CUSTOMERS, TBL_CUSTOMER_FILES));
        $this->object = EntityHelper::getEntityManager()->getRepository('FilesystemModule\GaufretteDatabaseStorage\Entities\CustomerFile');
        $customers[] = ObjectHelper::createCustomer(TEST_EMAIL1);
        $customers[] = ObjectHelper::createCustomer(TEST_EMAIL2);
        $files[] = CustomerFile::fromKey('1', $customers[0], 'test');
        $files[] = CustomerFile::fromKey('2', $customers[0], 'test');
        $files[] = CustomerFile::fromKey('3', $customers[0], 'other');
        $files[] = CustomerFile::fromKey('3', $customers[1], 'test');
        EntityHelper::save(array_merge($customers, $files));
        $this->customers = $customers;
        $this->files = $files;
    }

    /**
     * @covers FilesystemModule\GaufretteDatabaseStorage\CustomerFileRepository::findFile
     */
    public function testFindFile()
    {
        $file = $this->object->findFile($this->customers[0], '1');
        $this->assertEquals($this->files[0]->getKey(), $file->getKey());
        $file = $this->object->findFile($this->customers[1], '3');
        $this->assertEquals($this->files[3]->getKey(), $file->getKey());
    }


    /**
     * @covers FilesystemModule\GaufretteDatabaseStorage\CustomerFileRepository::findFile
     * @expectedException Doctrine\ORM\NoResultException
     */
    public function testFindFileFailure()
    {
        $this->object->findFile($this->customers[0], '4');
    }

    /**
     * @covers FilesystemModule\GaufretteDatabaseStorage\CustomerFileRepository::findFiles
     */
    public function testFindFiles()
    {
        $iterator = $this->object->findFiles($this->customers[0], 'test');
        $index = 0;
        foreach ($iterator as $file) {
            $this->assertEquals($file[0]->getKey(), $this->files[$index++]->getKey());
        }
        $this->assertEquals(2, $index);
    }
}
