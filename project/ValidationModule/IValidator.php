<?php
namespace ValidationModule;

interface IValidator
{
    /**
     * @param mixed $value
     * @return array of validation error messages
     */
    public function validate($value);
}
