<?php

namespace ValidationModule;

use DusanKasan\Knapsack\Collection;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class Validator implements IValidator
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var Constraint[]
     */
    private $constraints;

    /**
     * @param ValidatorInterface $validator
     * @param Constraint[] $constraints
     */
    public function __construct(ValidatorInterface $validator, array $constraints)
    {
        $this->validator = $validator;
        $this->constraints = $constraints;
    }

    /**
     * {@inheritdoc}
     */
    public function validate($value)
    {
        return Collection::from($this->validator->validate($value, $this->constraints))
            ->map(
                function (ConstraintViolationInterface $violation) {
                    return $violation->getMessage();
                }
            )
            ->toArray();
    }
}
