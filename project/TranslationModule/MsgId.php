<?php

namespace TranslationModule;

final class MsgId
{
    const NAME_CHECK_NOT_AVAILABLE = 'We are unable to check the availability of this company name at the moment because the Companies House name checking service is unavailable. You may still make this purchase, however you may need to supply an alternative name when the Companies House name checking service becomes available again.';
    const COMPANY_NAME_NOT_AVAILABLE = 'Sorry your name %s is unavailable';
    const EMAIL_SENT = 'Thank you for contacting us';
    const EMAIL_EXCEPTION = 'Sorry, unable to send email. Please try again later.';
}