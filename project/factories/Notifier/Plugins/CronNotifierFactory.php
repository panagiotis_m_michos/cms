<?php

namespace Notifier\Plugins;

use FApplication;
use Notifier\Factories\ClientServiceFactory;
use Notifier\Factories\ValidationFactory;

class CronNotifierFactory
{
    private function __construct()
    {
    }

    /**
     * @return CronNotifier
     */
    public static function create()
    {
        $notifierConf = FApplication::$config['notifier'];
        $clientServiceFactory = new ClientServiceFactory(ValidationFactory::create());
        $notifier = $clientServiceFactory->create(
            'notifier',
            $notifierConf['url'],
            $notifierConf['defaultParameters'],
            $notifierConf['oauth']
        );
        $notifier->setDefaultNamespace($notifierConf['defaultNamespace']);
        return $notifier;
    }


}
