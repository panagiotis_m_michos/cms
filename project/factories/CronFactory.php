<?php

use Cron\Backup;
use Cron\Colors;
use Cron\Cron;
use Cron\CronDi;
use Cron\CronLogger;
use Doctrine\Common\Cache\FilesystemCache;
use Notifier\Factories\ClientServiceFactory;
use Notifier\Factories\ValidationFactory;

class CronFactory
{
    private function __construct()
    {
    }

    /**
     * @return Cron
     */
    public static function create()
    {
        $logPath = CRON_LOG_DIR . DS . 'command.output';
        $backUpDir = CRON_LOG_DIR . DS . 'cronJobsLog';

        $cache = Registry::$container->get(DiLocator::CACHE_CRON);
        $logger = new CronLogger($logPath, ILogger::DEBUG, new Colors());
        $notifier = Registry::$container->get(DiLocator::NOTIFIER_CRON);

        $cron = new CronDi($cache, $logger, CRON_DIR, $notifier, Registry::$container);
        Backup::createBackup($logPath, $backUpDir); // Tomas: should be here?

        return $cron;
    }
}
