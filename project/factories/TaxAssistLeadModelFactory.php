<?php

namespace Factories;

class TaxAssistLeadModelFactory
{
    private function __construct()
    {
    }

    /**
     * @return \TaxAssistLeadModel
     */
    public static function create()
    {
        $config = \FApplication::$config['taxassist'];
        $connection = new \DibiConnection($config);
        $model = new \TaxAssistLeadModel($connection, $config['table']);
        return $model;
    }
} 