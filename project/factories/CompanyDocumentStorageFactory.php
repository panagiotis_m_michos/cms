<?php

namespace Factories;

use CompanyDocumentStorage;

class CompanyDocumentStorageFactory
{
    private function __construct()
    {
    }

    /**
     * @return CompanyDocumentStorage
     */
    public static function create()
    {
        return new CompanyDocumentStorage(TEMP_OTHER_DIR, TEMP_OTHER_DIR_R);
    }
}
