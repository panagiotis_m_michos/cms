<?php

namespace Utils\Text\Parser\Filters\UiElements;

use UiHelper\ViewHelper;
use Utils\Text\Parser\Filters\Interfaces\IRouter;

class BuyButtonElementFactory
{
    /**
     * @var ViewHelper
     */
    private $viewHelper;

    /**
     * @var IRouter
     */
    private $router;

    /**
     * @var string
     */
    private $route;

    /**
     * @var string
     */
    private $inputName;

    /**
     * @param ViewHelper $viewHelper
     * @param IRouter $router
     * @param string $route
     * @param string $inputName
     */
    public function __construct(ViewHelper $viewHelper, IRouter $router, $route, $inputName = 'productId')
    {
        $this->viewHelper = $viewHelper;
        $this->router = $router;
        $this->route = $route;
        $this->inputName = $inputName;
    }

    /**
     * @return IElement
     */
    public function createElement()
    {
        $button = new Element($this->viewHelper);
        $button->setDefaultOptions(
            [
                'actionUrl' => $this->router->link($this->route),
                'inputName' => $this->inputName,
                'loading' => 'expand-right',
                'text' => 'Buy Now',
            ]
        );

        return $button;
    }
}
