<?php

namespace Factories;

use Latte\MacroNode;
use Latte\Macros\MacroSet;
use Latte\PhpWriter;
use Nette\Templating\FileTemplate;
use Nette\Caching\Storages\FileStorage;
use Nette\Latte\Engine;

class TemplateFactory
{

    /**
     * @return FileTemplate
     */
    public static function createLatteTemplate()
    {
        $latte = new Engine;
        $set = new MacroSet($latte->getCompiler());
        $set->addMacro(
            'link', 
            function(MacroNode $node, PhpWriter $writer) {
                return $writer->write('echo FApplication::$router->absoluteFrontLink(%node.args)');
            }
        );

        $latte->addFilter('df', array('FTools', 'dateFormat'));
        $latte->addFilter('datetime', array('FTools', 'datetime'));
        $latte->addFilter('currency', array('Helpers\Currency', 'format'));

        $template = new FileTemplate();
        $template->setCacheStorage(new FileStorage(TEMP_CACHE_DIR));
        $template->onPrepareFilters[] = function($template) use ($latte) {
            $template->registerFilter($latte);
        };

        return $template;
    }

}
