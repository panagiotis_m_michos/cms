<?php

namespace Factories;

use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Writer_IWriter;

class PhpExcelWriterFactory
{

    /**
     * @var string
     */
    private $type;

    /**
     * @param $type
     */
    public function __construct($type)
    {
        $this->type = $type;
    }

    /**
     * @param PHPExcel $excel
     * @return PHPExcel_Writer_IWriter
     */
    public function create(PHPExcel $excel)
    {
        return PHPExcel_IOFactory::createWriter($excel, $this->type);
    }
}
