<?php

namespace Factories;

use PHPExcel;

class PhpExcelFactory
{

    /**
     * @return PHPExcel
     */
    public function create()
    {
        return new PHPExcel();
    }
}
