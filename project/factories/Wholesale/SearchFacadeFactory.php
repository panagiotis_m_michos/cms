<?php

namespace Factories\Wholesale;

use Basket;
use FApplication;
use SearchControler;
use Services\Facades\Wholesale\SearchFacade;

class SearchFacadeFactory
{
    /**
     * @param Basket $basket
     * @return SearchFacade
     */
    public static function create(Basket $basket)
    {
        return new SearchFacade(FApplication::$session->getNameSpace(SearchControler::$nameSpace), $basket);
    }
} 
