<?php

namespace Factories;

use FApplication;
use FNode;
use RouterModule\FrontTemplate;

class Template
{
    public static function createFrontTemplate($templating)
    {
        $node = new FNode(FApplication::$nodeId ? FApplication::$nodeId : 44);
        return new FrontTemplate(
            FApplication::$template,
            FApplication::$router,
            $templating['form'],
            $node
        );
    }
}