<?php

namespace Console;

use ConfigAdapterIni;
use Console\Commands\DeployCommand;
use Console\Commands\GenerateDataCommand;
use Console\Commands\GenerateDbCommand;
use Console\Commands\GenerateEntityCommand;
use Console\Commands\SyncDbvCommand;
use Console\Commands\SyncNodeCommand;
use Console\Commands\Tokens\UsCardsBillingStateUpdateCommand;
use Console\Commands\UpdateCashbacksCommand;
use Console\Commands\UpdateTokensCommand;
use Console\Gen\StructureConfig;
use Console\Sync\DatabaseConfig;
use Console\Sync\RemoteDatabaseConfig;
use Cron\Console\Commands\CronInfoCommand;
use Cron\Console\Commands\CronRunCommand;
use Cron\CronDi;
use DBV;
use DibiConnection;
use Doctrine\ORM\Mapping\Driver\DatabaseDriver;
use Doctrine\ORM\Tools\DisconnectedClassMetadataFactory;
use Doctrine\ORM\Tools\EntityGenerator;
use Environment;
use Factories\Console\ConfigFactory;
use Factories\Console\DibiConnectionFactory;
use FApplication;
use DiLocator;
use Loggers\NikolaiLogger;
use Registry;
use Doctrine\ORM\EntityManager;
use SagePay\Reporting\SagePayFactory;
use Services\CompanyService;
use Services\CustomerService;
use Services\OrderService;
use Services\Payment\TokenService;
use Services\TransactionService;

class CommandFactory
{
    private function __construct()
    {
    }

    /**
     * @return CronInfoCommand
     */
    public static function getCronInfoCommand()
    {
        /* @var $cron CronDi */
        $cron = Registry::$container->get(DiLocator::CRON);
        return new CronInfoCommand($cron, self::getCronConfigPath());
    }

    /**
     * @return CronRunCommand
     */
    public static function getCronRunCommand()
    {
        /* @var $cron CronDi */
        $cron = Registry::$container->get(DiLocator::CRON);
        return new CronRunCommand($cron, self::getCronConfigPath());
    }

    /**
     * @return SyncNodeCommand
     */
    public static function getSyncNodeCommand()
    {
        return new SyncNodeCommand(new ConfigFactory(FApplication::$config, PROJECT_DIR . '/config/app/config.test.yml', FApplication::$container->getParameterBag()));
    }

    /**
     * @return UsCardsBillingStateUpdateCommand
     */
    public static function getTokensUsCardsBillingStateUpdateCommand()
    {
        /** @var $em EntityManager */
        $em = Registry::$container->get(DiLocator::ENTITY_MANAGER);
        /** @var $logger NikolaiLogger */
        $logger = Registry::$container->get(DiLocator::NIKOLAI_LOGGER);
        /** @var $tokenService TokenService */
        $tokenService = Registry::$container->get(DiLocator::SERVICE_TOKEN);
        /** @var SagePayFactory $sagePayFactory */
        $sagePayFactory = Registry::$container->get(DiLocator::FACTORY_SAGE_REPORTING_API);
        /** @var TransactionService $transactionService */
        $transactionService = Registry::$container->get(DiLocator::SERVICE_TRANSACTION);

        return new UsCardsBillingStateUpdateCommand($em, $logger, $tokenService, $sagePayFactory, $transactionService);
    }

    /**
     * @return DeployCommand
     */
    public static function getDeployCommand()
    {
        $configFactory = new ConfigFactory(FApplication::$config, PROJECT_DIR . '/config/app/config.test.yml', FApplication::$container->getParameterBag());
        $sshConnection = $configFactory->createSsh();
        return new DeployCommand($sshConnection);
    }

    /**
     * @return GenerateEntityCommand
     */
    public static function getGenerateEntityCommand()
    {
        $structure = new StructureConfig(
            PROJECT_DIR . '/models/Entities/',
            PROJECT_DIR . '/models/Repositories/',
            PROJECT_DIR . '/Services/',
            VENDOR_DIR . '/made_simple/console/src/Console/Fixtures/Entity/'
        );

        /** @var $em EntityManager */
        $em = Registry::$container->get(DiLocator::ENTITY_MANAGER);
        $connection = $em->getConnection();
        $schemaManager = $connection->getSchemaManager();
        $driverImpl = new DatabaseDriver($schemaManager);

        return new GenerateEntityCommand(
            $structure,
            $em,
            new EntityGenerator(),
            new DisconnectedClassMetadataFactory(),
            $driverImpl
        );
    }

    /**
     * @return SyncDbvCommand
     */
    public static function getSyncDbvCommand()
    {
        $filePath = LIBS_DIR . '/3rdParty/dbv/config.php';
        if (file_exists($filePath)) {
            require_once $filePath;
        }

        return new SyncDbvCommand(
            DBV::instance()
        );
    }

    /**
     * @return GenerateDbCommand
     */
    public static function getGenerateDbCommand()
    {
        return new GenerateDbCommand(
            DOCUMENT_ROOT . '/storage/dbv/data/schema',
            DOCUMENT_ROOT . '/storage/dbv/data/revisions',
            new DibiConnectionFactory(new ConfigFactory(FApplication::$config, PROJECT_DIR . '/config/app/config.test.yml', FApplication::$container->getParameterBag()))
        );
    }

    /**
     * @return GenerateDataCommand
     */
    public static function getGenerateDataCommand()
    {
        $fixturesFiles = array(
            'customers.yml',
            'orders.yml',
            'orders_items.yml',
            'company.yml',
            'tokens.yml',
            'services.yml',
            'transactions.yml',
            'form_submission.yml'
        );

        return new GenerateDataCommand($fixturesFiles);
    }

    /**
     * @return UpdateTokensCommand
     */
    public static function getUpdateTokensCommand()
    {
        /** @var $em EntityManager */
        $em = Registry::$container->get(DiLocator::ENTITY_MANAGER);
        /** @var $logger NikolaiLogger */
        $logger = Registry::$container->get(DiLocator::NIKOLAI_LOGGER);
        return new UpdateTokensCommand($em, $logger, VENDOR_DIR);
    }


    /**
     * @return UpdateTokensCommand
     */
    public static function getUpdateCashbacksCommand()
    {
        /** @var $logger NikolaiLogger */
        $logger = Registry::$container->get(DiLocator::NIKOLAI_LOGGER);

        $configFactory = new ConfigFactory(FApplication::$config, PROJECT_DIR . '/config/app/config.test.yml', FApplication::$container->getParameterBag());
        $dbConfig = $configFactory->createLocal(Environment::DEVELOPMENT)->toArray();

        return new UpdateCashbacksCommand(
            new DibiConnection($dbConfig),
            $logger
        );
    }

    /**
     * @return string
     */
    private static function getCronConfigPath()
    {
        return CRON_DIR . DS . 'config';
    }

    /**
     * @param string $environment
     * @return array
     */
    //private static function getDatabaseConfig($environment)
    //{
    //    $siteConfigPath = realpath(APP_DIR) . DIRECTORY_SEPARATOR . 'config.ini';
    //    $config = ConfigAdapterIni::load($siteConfigPath, $environment);
    //    return $config['database'];
    //}
    //
    ///**
    // * @param $section
    // * @return mixed
    // */
    //private static function getConsoleConfig($section)
    //{
    //    return FApplication::$config['console'][$section];
    //}
}
