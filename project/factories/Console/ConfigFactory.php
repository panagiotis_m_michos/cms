<?php

namespace Factories\Console;

use BootstrapModule\Ext\Config\ConfigYmlExt;
use Console\Helpers\IConfigFactory;
use Console\SshConnection;
use Console\Sync\DatabaseConfig;
use Console\Sync\RemoteDatabaseConfig;
use Environment;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Utils\File;

class ConfigFactory implements IConfigFactory
{

    /**
     * @var array
     */
    private $config;

    /**
     * @var string
     */
    private $testConfigPath;

    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;

    /**
     * @param array $config
     * @param $testConfigPath
     * @param ParameterBagInterface $parameterBag
     */
    public function __construct($config, $testConfigPath, ParameterBagInterface $parameterBag)
    {
        $this->config = $config;
        $this->testConfigPath = $testConfigPath;
        $this->parameterBag = $parameterBag;
    }

    /**
     * @inheritdoc
     */
    public function createRemote()
    {
        $config = $this->config['console']['sync']['database'];
        $tables = $this->config['console']['sync']['tables'];
        return new RemoteDatabaseConfig($config['host'], $config['username'], $config['password'], $config['database'], $tables);
    }

    /**
     * @inheritdoc
     */
    public function createLocal($environment)
    {

        if ($environment === Environment::CONSOLE) {
            $testConfig = new File($this->testConfigPath);
            $configYml = new ConfigYmlExt();
            $resources = [];
            $testConfig = $configYml->import($this->parameterBag, $testConfig, $resources);
            $config = $testConfig['database'];
        } else {
            $config = $this->config['database'];
        }
        return new DatabaseConfig($config['host'], $config['username'], $config['password'], $config['database']);
    }

    /**
     * @inheritdoc
     */
    public function createSsh()
    {
        $config = $this->config['console']['ssh'];
        return new SshConnection($config['projectName'], $config['projectDirectory'], $config['user'], $config['ip']);
    }
}
