<?php

namespace Factories\Console;

use Console\Helpers\IConfigFactory;
use Console\Helpers\IConnectionFactory;
use DibiConnection;

class DibiConnectionFactory implements IConnectionFactory
{
    /**
     * @var IConfigFactory
     */
    private $configFactory;

    /**
     * @param IConfigFactory $configFactory
     */
    public function __construct(IConfigFactory $configFactory)
    {
        $this->configFactory = $configFactory;
    }

    /**
     * @inheritdoc
     */
    public function create($environment)
    {
        $config = $this->configFactory->createLocal($environment);
        return new DibiConnection($config->toArray());
    }

}
