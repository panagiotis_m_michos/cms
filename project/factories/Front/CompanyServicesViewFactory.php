<?php

namespace Factories\Front;

use Entities\Company;
use Factories\ServiceViewFactory;
use Models\View\Front\CompanyServicesView;
use Services\EventService;

class CompanyServicesViewFactory
{

    /**
     * @var ServiceViewFactory
     */
    private $serviceViewFactory;

    /**
     * @var EventService
     */
    private $eventService;

    /**
     * @param ServiceViewFactory $serviceViewFactory
     * @param EventService $eventService
     */
    public function __construct(ServiceViewFactory $serviceViewFactory, EventService $eventService)
    {
        $this->serviceViewFactory = $serviceViewFactory;
        $this->eventService = $eventService;
    }

    /**
     * @param Company $company
     * @return CompanyServicesView
     */
    public function create(Company $company)
    {
        return new CompanyServicesView($company, $this->serviceViewFactory, $this->eventService);
    }
}
