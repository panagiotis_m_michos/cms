<?php

namespace Factories\Front;

use Basket\PackageToPackageDowngradeNotification;
use Basket\PackageToPackageUpgradeNotification;
use Basket\PackageToProductDowngradeNotification;
use Basket\ProductToPackageUpgradeNotification;
use Entities\Company;
use Entities\Service;
use Factories\ServiceViewFactory;
use IPackage;
use Nette\Object;
use Product;
use Services\CompanyService;

class BasketNotificationFactory extends Object
{

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var CompanyViewFactory
     */
    private $companyViewFactory;

    /**
     * @var ServiceViewFactory
     */
    private $serviceViewFactory;

    /**
     * @param CompanyService $companyService
     * @param CompanyViewFactory $companyViewFactory
     * @param ServiceViewFactory $serviceViewFactory
     */
    public function __construct(
        CompanyService $companyService,
        CompanyViewFactory $companyViewFactory,
        ServiceViewFactory $serviceViewFactory
    )
    {
        $this->companyService = $companyService;
        $this->companyViewFactory = $companyViewFactory;
        $this->serviceViewFactory = $serviceViewFactory;
    }

    /**
     * @param IPackage $package
     * @param Company $company
     * @param array $companyServices
     * @return ProductToPackageUpgradeNotification
     */
    public function createProductToPackageUpgradeNotification(IPackage $package, Company $company, array $companyServices)
    {
        $companyView = $this->companyViewFactory->create($company);
        $serviceViews = $groupedByTypeServices = [];
        foreach ($companyServices as $service) {
            $groupedByTypeServices[$service->getServiceTypeId()][] = $service;
        }
        foreach ($groupedByTypeServices as $serviceGroup) {
            $serviceViews[] = $this->serviceViewFactory->create($serviceGroup);
        }

        return new ProductToPackageUpgradeNotification($package, $companyView, $serviceViews);
    }

    /**
     * @param $basketPackage
     * @param $packageService
     * @return PackageToPackageUpgradeNotification
     */
    public function createPackageToPackageUpgradeNotification(IPackage $basketPackage, Service $packageService)
    {
        return new PackageToPackageUpgradeNotification($basketPackage, $packageService);
    }

    /**
     * @param Product $basketProduct
     * @param Service $packageService
     * @return PackageToProductDowngradeNotification
     */
    public function createPackageToProductDowngradeNotification(Product $basketProduct, Service $packageService)
    {
        $isCompanyIncorporationPending = $this->companyService->isCompanyIncorporationPending($packageService->getCompany());

        return new PackageToProductDowngradeNotification($basketProduct, $packageService, $isCompanyIncorporationPending);
    }

    /**
     * @param IPackage $basketPackage
     * @param Service $packageService
     * @return PackageToPackageDowngradeNotification
     */
    public function createPackageToPackageDowngradeNotificion(IPackage $basketPackage, Service $packageService)
    {
        $isCompanyIncorporationPending = $this->companyService->isCompanyIncorporationPending($packageService->getCompany());

        return new PackageToPackageDowngradeNotification($basketPackage, $packageService, $isCompanyIncorporationPending);
    }
}
