<?php

namespace Factories\Front;

use Entities\Company;
use Factories\ServiceViewFactory;
use Models\View\CompanyView;

class CompanyViewFactory
{

    /**
     * @var ServiceViewFactory
     */
    private $serviceViewFactory;

    /**
     * @param ServiceViewFactory $serviceViewFactory
     */
    public function __construct(ServiceViewFactory $serviceViewFactory)
    {
        $this->serviceViewFactory = $serviceViewFactory;
    }

    /**
     * @param Company $company
     * @return CompanyView
     */
    public function create(Company $company)
    {
        return new CompanyView($company, $this->serviceViewFactory);
    }
}
