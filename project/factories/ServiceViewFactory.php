<?php

namespace Factories;

use Models\View\ServiceView;
use Repositories\ServiceSettingsRepository;

class ServiceViewFactory
{

    /**
     * @var ServiceSettingsRepository
     */
    private $serviceSettingsRepository;

    /**
     * @param ServiceSettingsRepository $serviceSettingsRepository
     */
    public function __construct(ServiceSettingsRepository $serviceSettingsRepository)
    {
        $this->serviceSettingsRepository = $serviceSettingsRepository;
    }

    /**
     * @param array $services
     * @return ServiceView
     */
    public function create(array $services)
    {
        return new ServiceView($services, $this->serviceSettingsRepository);
    }
}
