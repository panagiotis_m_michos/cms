<?php

namespace ServiceActivatorModule;

use DateTime;
use Entities\Service;
use Exceptions\Technical\ServiceActivationException;
use ServiceActivatorModule\Exceptions\CompanyNotIncorporatedException;
use ServiceActivatorModule\Exceptions\ServiceAlreadyActivatedException;
use Services\ServiceService;

class ServiceActivator
{
    /**
     * @var ServiceService
     */
    private $serviceService;

    /**
     * @var ProductRelationshipChecker
     */
    private $productRelationshipChecker;

    /**
     * @var RelatedServiceProvider
     */
    private $relatedServiceProvider;

    /**
     * @param ServiceService $serviceService
     * @param ProductRelationshipChecker $productRelationshipChecker
     * @param RelatedServiceProvider $relatedServiceProvider
     */
    public function __construct(
        ServiceService $serviceService,
        ProductRelationshipChecker $productRelationshipChecker,
        RelatedServiceProvider $relatedServiceProvider
    )
    {
        $this->serviceService = $serviceService;
        $this->productRelationshipChecker = $productRelationshipChecker;
        $this->relatedServiceProvider = $relatedServiceProvider;
    }

    /**
     * @param Service $service
     * @throws CompanyNotIncorporatedException
     * @throws ServiceAlreadyActivatedException
     */
    public function activate(Service $service)
    {
        if (!$service->getCompany()->isIncorporated()) {
            throw CompanyNotIncorporatedException::generic($service);
        }

        if ($service->hasDates()) {
            throw ServiceAlreadyActivatedException::generic($service);
        }

        $this->activateService($service);
    }

    /**
     * @param Service $newService
     * @throws ServiceActivationException
     */
    private function activateService(Service $newService)
    {
        $company = $newService->getCompany();
        $orderDate = $newService->getOrder()->getDtc();

        $previousService = $this->relatedServiceProvider->getRelatedService($company, $newService->getProduct(), $orderDate);
        if ($previousService) {
            $this->verifyDowngrade($previousService, $newService);
        }

        $this->disableCurrentServices($newService);
        $this->setServiceDates($newService, $previousService);
    }

    /**
     * @param Service $newService
     * @param Service $previousService
     * @throws ServiceActivationException
     */
    private function verifyDowngrade(Service $previousService, Service $newService)
    {
        $orderDate = $newService->getOrder()->getDtc();

        if ($this->productRelationshipChecker->isDowngradeForbiddenOn($previousService, $newService->getProduct(), $orderDate)) {
            throw ServiceActivationException::packageToProductDowngrade(
                $newService->getCompany(),
                $previousService,
                $newService
            );
        }
    }

    /**
     * @param Service $newService
     */
    private function disableCurrentServices(Service $newService)
    {
        $orderDate = $newService->getOrder()->getDtc();
        $currentServices = $this->serviceService->getCompanyNonExpiredServicesOn($newService->getCompany(), $orderDate);

        foreach ($currentServices as $currentService) {
            if ($this->productRelationshipChecker->isDowngrade($currentService->getProduct(), $newService->getProduct())) {
                $this->serviceService->markAsDowngraded($currentService);
            } elseif ($this->productRelationshipChecker->isUpgrade($currentService->getProduct(), $newService->getProduct())) {
                $this->serviceService->markAsUpgraded($currentService);
            }
        }
    }

    /**
     * @param Service $newService
     * @param Service|NULL $previousService
     */
    private function setServiceDates(Service $newService, Service $previousService = NULL)
    {
        $newDtStart = $this->getNewServiceDtStart($newService, $previousService);
        $newDtExpires = clone $newDtStart;
        $newDtExpires = $newDtExpires->modify("{$newService->getInitialDuration()} -1 day");

        $this->serviceService->setServiceDates($newService, $newDtStart, $newDtExpires);
    }

    /**
     * @param Service $newService
     * @param Service|NULL $previousService
     * @return DateTime
     */
    private function getNewServiceDtStart(Service $newService, Service $previousService = NULL)
    {
        $orderDate = $newService->getOrder()->getDtc();

        if ($this->serviceService->isFormationServiceForCompany($newService)) {
            $newDtStart = clone $newService->getCompany()->getIncorporationDate();

        } elseif ($previousService && $previousService->isExpiringAfter($orderDate)) {
            $newDtStart = clone $previousService->getDtExpires();
            $newDtStart->modify('+1 day');

        } elseif ($newService->getInitialDtStart()) {
            $newDtStart = clone $newService->getInitialDtStart();

        } else {
            $newDtStart = clone $orderDate;
        }

        return $newDtStart->setTime(0, 0, 0);
    }
}
