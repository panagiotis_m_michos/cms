<?php

namespace ServiceActivatorModule\Validators;

use Basket;
use DateTime;
use ServiceActivatorModule\ProductRelationshipChecker;
use ServiceActivatorModule\RelatedServiceProvider;
use Services\NodeService;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class NotDowngradeForbiddenValidator extends ConstraintValidator
{
    /**
     * @var NodeService
     */
    private $nodeService;

    /**
     * @var RelatedServiceProvider
     */
    private $relatedServiceProvider;

    /**
     * @var ProductRelationshipChecker
     */
    private $productChecker;

    /**
     * @param NodeService $nodeService
     * @param RelatedServiceProvider $relatedServiceProvider
     * @param ProductRelationshipChecker $productRelationshipChecker
     */
    public function __construct(
        NodeService $nodeService,
        RelatedServiceProvider $relatedServiceProvider,
        ProductRelationshipChecker $productRelationshipChecker
    )
    {
        $this->nodeService = $nodeService;
        $this->relatedServiceProvider = $relatedServiceProvider;
        $this->productChecker = $productRelationshipChecker;
    }

    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof NotDowngradeForbidden) {
            throw new UnexpectedTypeException($constraint, NotDowngradeForbidden::class);
        }

        if (empty($value)) {
            return;
        }

        $product = $this->nodeService->getProductById($value);
        $orderDate = new DateTime;
        $relatedService = $this->relatedServiceProvider->getRelatedService($constraint->company, $product, $orderDate);
        if ($relatedService && $this->productChecker->isDowngradeForbiddenOn($relatedService, $product, $orderDate)) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}
