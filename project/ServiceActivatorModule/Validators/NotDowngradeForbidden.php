<?php

namespace ServiceActivatorModule\Validators;

use Entities\Company;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class NotDowngradeForbidden extends Constraint
{
    /**
     * @var string
     */
    public $message = "Service can't be selected because downgrade is forbidden";

    /**
     * @var Company
     */
    public $company;

    /**
     * @return array
     */
    public function getRequiredOptions()
    {
        return ['company'];
    }

    /**
     * @return string
     */
    public function validatedBy()
    {
        return 'service_activator_module.validators.not_downgrade_forbidden';
    }
}
