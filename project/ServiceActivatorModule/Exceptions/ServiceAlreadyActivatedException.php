<?php

namespace ServiceActivatorModule\Exceptions;

use Entities\Service;
use Exceptions\Technical\TechnicalAbstract;

class ServiceAlreadyActivatedException extends TechnicalAbstract
{
    /**
     * @param Service $service
     * @return ServiceAlreadyActivatedException
     */
    public static function generic(Service $service)
    {
        return new self("Service (id: {$service->getId()}) is already activated");
    }
}
