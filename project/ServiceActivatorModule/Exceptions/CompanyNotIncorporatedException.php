<?php

namespace ServiceActivatorModule\Exceptions;

use Entities\Service;
use Exceptions\Technical\TechnicalAbstract;

class CompanyNotIncorporatedException extends TechnicalAbstract
{
    /**
     * @param Service $service
     * @return CompanyNotIncorporatedException
     */
    public static function generic(Service $service)
    {
        return new self(
            sprintf(
                'Service (id: %d) can not be activated because its company (id: %d) is not yet incorporated',
                $service->getId(),
                $service->getCompany()->getId()
            )
        );
    }
}
