<?php

namespace ServiceActivatorModule;

use BasketProduct;
use DateTime;
use Entities\Service;

class ProductRelationshipChecker
{
    /**
     * @param BasketProduct $from
     * @param BasketProduct $to
     * @return bool
     */
    public function isUpgrade(BasketProduct $from, BasketProduct $to)
    {
        if (!$to->isPackage()) {
            return FALSE;
        } elseif ($this->isPackagePrivacyToComprehensive($from, $to)) {
            return TRUE;
        } elseif ($from->isProduct()) {
            return in_array($from->getServiceTypeId(), $to->getTypesForPackageProducts());
        }

        return FALSE;
    }

    /**
     * @param BasketProduct $from
     * @param BasketProduct $to
     * @return bool
     */
    public function isDowngrade(BasketProduct $from, BasketProduct $to)
    {
        if (!$from->isPackage()) {
            return FALSE;
        } elseif ($this->isPackageComprehensiveToPrivacy($from, $to)) {
            return TRUE;
        } elseif ($to->isProduct() && !$this->isEqual($from, $to)) {
            return in_array($to->getServiceTypeId(), $from->getTypesForPackageProducts());
        }

        return FALSE;
    }

    /**
     * @param BasketProduct $from
     * @param BasketProduct $to
     * @return bool
     */
    public function isEqual(BasketProduct $from, BasketProduct $to)
    {
        return $from->getServiceTypeId() == $to->getServiceTypeId();
    }

    /**
     * @param Service $fromService
     * @param BasketProduct $to
     * @param DateTime $date
     * @return bool
     */
    public function isDowngradableOn(Service $fromService, BasketProduct $to, DateTime $date)
    {
        return $this->isDowngrade($fromService->getProduct(), $to)
            && ($fromService->isOverdueOn($date) || !$fromService->isActiveOn($date));
    }

    /**
     * @param Service $fromService
     * @param BasketProduct $to
     * @param DateTime $date
     * @return bool
     */
    public function isDowngradeForbiddenOn(Service $fromService, BasketProduct $to, DateTime $date)
    {
        return $this->isDowngrade($fromService->getProduct(), $to)
            && !$this->isDowngradableOn($fromService, $to, $date);
    }

    /**
     * @param BasketProduct $from
     * @param BasketProduct $to
     * @return bool
     */
    private function isPackagePrivacyToComprehensive(BasketProduct $from, BasketProduct $to)
    {
        return $from->isPackagePrivacyType() && $to->isPackageComprehensiveType();
    }

    /**
     * @param BasketProduct $from
     * @param BasketProduct $to
     * @return bool
     */
    private function isPackageComprehensiveToPrivacy(BasketProduct $from, BasketProduct $to)
    {
        return $from->isPackageComprehensiveType() && $to->isPackagePrivacyType();
    }
}
