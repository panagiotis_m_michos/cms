<?php

namespace ServiceActivatorModule;

use BasketProduct;
use DateTime;
use Entities\Company;
use Entities\Service;
use Services\ServiceService;

class RelatedServiceProvider
{
    /**
     * @var ProductRelationshipChecker
     */
    private $productChecker;

    /**
     * @var ServiceService
     */
    private $serviceService;

    /**
     * @param ServiceService $serviceService
     * @param ProductRelationshipChecker $productChecker
     */
    public function __construct(ServiceService $serviceService, ProductRelationshipChecker $productChecker)
    {
        $this->productChecker = $productChecker;
        $this->serviceService = $serviceService;
    }

    /**
     * @param Company $company
     * @param BasketProduct $newProduct
     * @param DateTime $orderDate
     * @return Service|null
     */
    public function getRelatedService(Company $company, BasketProduct $newProduct, DateTime $orderDate)
    {
        $currentServices = $this->serviceService->getCompanyNonExpiredServicesOn($company, $orderDate);

        /** @var Service $previousService */
        $previousService = NULL;

        foreach ($currentServices as $currentService) {
            if ($previousService && $currentService->getDtExpires() < $previousService->getDtExpires()) {
                continue;
            }

            if ($this->isRelated($newProduct, $orderDate, $currentService)) {
                $previousService = $currentService;
            }
        }

        return $previousService;
    }

    /**
     * @param BasketProduct $newProduct
     * @param DateTime $orderDate
     * @param Service $currentService
     * @return bool
     */
    private function isRelated(BasketProduct $newProduct, DateTime $orderDate, Service $currentService)
    {
        $currentProduct = $currentService->getProduct();

        return ($this->productChecker->isDowngrade($currentProduct, $newProduct) && $currentService->isActiveOn($orderDate))
            || ($this->productChecker->isUpgrade($currentProduct, $newProduct) && $currentService->isOverdueOn($orderDate))
            || ($currentService->getServiceTypeId() == $newProduct->getServiceTypeId());
    }
}
