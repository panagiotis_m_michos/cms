<?php

namespace ProductModule\Controllers;

use Symfony\Component\HttpFoundation\Response;
use TemplateModule\Renderers\IRenderer;

class ConfirmationStatementController
{
    /**
     * @var IRenderer
     */
    private $renderer;

    /**
     * @param IRenderer $renderer
     */
    public function __construct(IRenderer $renderer)
    {
        $this->renderer = $renderer;
    }

    /**
     * @return Response
     */
    public function confirmationStatement()
    {
        return $this->renderer->render();
    }
}
