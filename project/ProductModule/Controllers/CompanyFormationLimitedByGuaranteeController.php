<?php

namespace ProductModule\Controllers;

use Symfony\Component\HttpFoundation\Response;
use TemplateModule\Renderers\IRenderer;

class CompanyFormationLimitedByGuaranteeController
{
    /**
     * @var IRenderer
     */
    private $renderer;

    /**
     * @param IRenderer $renderer
     */
    public function __construct(IRenderer $renderer)
    {
        $this->renderer = $renderer;
    }

    /**
     * @return Response
     */
    public function companyFormationLimitedByGuarantee()
    {
        return $this->renderer->render();
    }
}
