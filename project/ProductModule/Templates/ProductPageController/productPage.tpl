{extends '@structure/layout.tpl'}

{block content}
    <div class="width100 bg-white padcard-mobile">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-9">
                    <h1>{$introductionStrip.title nofilter}</h1>
                    <p class="lead">{$introductionStrip.lead nofilter}</p>
                    {$introductionStrip.description nofilter}
                </div>
                {if isset($introductionStrip.imgFeature)}
                    <div class="col-xs-12  col-md-3 hidden-xs">
                        <img class="img-responsive center-block" src="{$introductionStrip.imgFeature nofilter}" alt="{$introductionStrip.imgFeatureDescription nofilter}">
                    </div>
                {/if}
            </div>
        </div>
    </div>
    {if isset($matrixStrip.blocks)}
        <div class="width100 bg-white pad8">
            <div class="container">
                {foreach $matrixStrip.blocks as $block}
                    <div class="row companyservices-matrix-block">
                        <div class="col-xs-12 col-md-10">
                            <div class="row">
                                <h2 class="top0 btm10">{$block.title nofilter}</h2>
                                {if isset($block.description)}<p>{$block.description nofilter}</p>{/if}
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-2 text-center button-wide-mobile">
                            <div class="row">
                                <p class="lead btm10"><strong>{if isset($block.price)}{$block.price nofilter}{/if}</strong></p>
                                {$block.buyButton nofilter}
                            </div>
                        </div>
                    </div>
                {/foreach}
            </div>
        </div>
    {/if}
        <div class="width100 bg-white padcard-mobile {if isset($featuresStrip.bgColor)}{$featuresStrip.bgColor}{/if}">
            <div class="container">
                <div class="row">
                    {if isset($featuresColLeft.features)}
                        <div class="col-xs-12 col-md-6">
                            {foreach $featuresColLeft.features as $feature}
                                <h3>{$feature.title nofilter}</h3>
                                {$feature.description nofilter}
                            {/foreach}
                        </div>
                    {/if}
                    {if isset($featuresColRight.features)}
                        <div class="col-xs-12 col-md-6">
                            {foreach $featuresColRight.features as $feature}
                                <h3>{$feature.title nofilter}</h3>
                                {$feature.description nofilter}
                            {/foreach}
                        </div>
                    {/if}
                </div>
                {if isset($proofOfId)}
                    <div class="row">
                        <div class="col-xs-12">
                            {ui name='proof_of_id' data=$proofOfId}
                        </div>
                    </div>
                {/if}
                {if isset($featuresStrip.features)}
                    <div class="row">
                        <div class="col-xs-12">
                            {foreach $featuresStrip.features as $feature}
                                <h3>{$feature.title nofilter}</h3>
                                {$feature.description nofilter}
                            {/foreach}
                        </div>
                    </div>
                {/if}
            </div>
        </div>
    {if isset($faqStrip.title)}
        <div class="width100 bg-grey1 padcard-mobile">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h3>{$faqStrip.title nofilter}</h3>
                        {foreach $faqStrip.faqBlock as $faqBlock}
                            <h4 class="display-inline">{$faqBlock.title nofilter}</h4>
                            {$faqBlock.description nofilter}
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
    {/if}
    {if isset($footer.cta) || isset($footer.description)}
        <div class="width100 bg-grey1 padcard-mobile">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        {if isset($footer.cta)}<a href="{$footer.link nofilter}" class="btn btn-lg btn-default top20 btm20">{$footer.cta nofilter}</a>{/if}
                        {if isset($footer.description)}<p>{$footer.description nofilter}</p>{/if}
                    </div>
                </div>
            </div>
        </div>
    {/if}
{/block}
