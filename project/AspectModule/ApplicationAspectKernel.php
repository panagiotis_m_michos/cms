<?php

namespace AspectModule;

use AspectModule\Aspects\LoggableAspect;
use DiLocator;
use ErrorModule\Ext\DebugExt;
use Go\Core\AspectKernel;
use Go\Core\AspectContainer;
use Symfony\Component\DependencyInjection\Container;

/**
 * Application Aspect Kernel
 */
class ApplicationAspectKernel extends AspectKernel
{
    /**
     * @var ContainerBuilder
     */
    private $containerBuilder;

    /**
     * @param Container $containerBuilder
     */
    protected function __construct(Container $containerBuilder)
    {
        $this->containerBuilder = $containerBuilder;
    }

    /**
     * @param Container $containerBuilder
     * @return ApplicationAspectKernel
     */
    public static function getKernel(Container $containerBuilder)
    {
        if (!self::$instance) {
            self::$instance = new static($containerBuilder);
        }
        return self::$instance;
    }

    /**
     * Configure an AspectContainer with advisors, aspects and pointcuts
     *
     * @param AspectContainer $container
     *
     * @return void
     */
    protected function configureAop(AspectContainer $container)
    {
        $container->registerAspect(
            new LoggableAspect($this->containerBuilder->get(DebugExt::LOGGER))
        );
    }
}

