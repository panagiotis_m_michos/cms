<?php

namespace AspectModule\Annotations;

use Doctrine\Common\Annotations\Annotation;

/**
 * @Annotation
 * @Target("METHOD")
 */
class Loggable extends Annotation
{
}