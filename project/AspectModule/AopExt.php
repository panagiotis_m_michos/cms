<?php

namespace AspectModule;

use BootstrapModule\Ext\IExtension;
use Symfony\Component\DependencyInjection\Container;
use Utils\Directory;

class AopExt implements IExtension
{
    /**
     * @param Container $containerBuilder
     */
    public function load(Container $containerBuilder)
    {
        $cacheDir = Directory::createWritable(dirname($containerBuilder->getParameter('cache_dir')) . '/newcache/');
        $applicationAspectKernel = ApplicationAspectKernel::getKernel($containerBuilder);
        $applicationAspectKernel->init(
            array(
            //@TODO remove cache when deploying. waiting for cachePerms feature
            'debug' => TRUE, //$containerBuilder->getParameter('environment') !== Environment::PRODUCTION, // use 'false' for production mode
            'appDir' => $containerBuilder->getParameter('root'),
            // Cache directory
            //@TODO using the same folder for console and apache creates permission issues. library is using mkdir with 0770 permissions
            'cacheDir'  => $cacheDir->getPath() . DIRECTORY_SEPARATOR . (php_sapi_name() === 'cli' ? 'consoleaop' : 'aop'),
            // Include paths restricts the directories where aspects should be applied, or empty for all source files
            'includePaths' => array(
                $containerBuilder->getParameter('root') . '/project'
            ),
            )
        );
    }
}