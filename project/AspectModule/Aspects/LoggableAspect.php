<?php

namespace AspectModule\Aspects;

use Go\Aop\Aspect;
use Go\Aop\Intercept\MethodInvocation;
use Go\Lang\Annotation\Around;
use Psr\Log\LoggerInterface;
use Exception;

/**
 * Loggable aspect
 */
class LoggableAspect implements Aspect
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @Around("@annotation(AspectModule\Annotations\Loggable)")
     * @param MethodInvocation $invocation Invocation
     * @return mixed
     * @throws Exception
     */
    public function aroundLoggable(MethodInvocation $invocation)
    {
        try {
            return $invocation->proceed();
        } catch (Exception $e) {
            $this->logger->error('Error occured in: ' . (string) $e);
            throw $e;
        }
    }
}