<?php

namespace Extensions;

use PhpSpec\Extension\ExtensionInterface;
use PhpSpec\ServiceContainer;

class PhpSpecLoader implements ExtensionInterface
{
    /**
     * @param ServiceContainer $container
     */
    public function load(ServiceContainer $container)
    {
        require_once __DIR__ . '/../../tests/index.php';
    }

}
