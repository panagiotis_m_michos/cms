{extends '@structure/layout.tpl'}

{block content}
    <div class="width100 bg-white padcard-mobile">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-9">
                    <h1>{$introductionStrip.title nofilter}</h1>
                    <p class="lead">{$introductionStrip.lead nofilter}</p>
                </div>
                {if isset($introductionStrip.imgFeature)}
                    <div class="col-xs-12  col-md-3 hidden-xs">
                        <img class="img-responsive center-block" src="{$introductionStrip.imgFeature nofilter}" alt="{$introductionStrip.imgFeatureDescription nofilter}">
                    </div>
                {/if}
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <h3>{$benefitsStrip.title nofilter}</h3>
                    <ul class="fa-ul">
                        {foreach $benefitsStrip.features as $feature}
                            <li><i class="fa-li fa fa-check green"></i>{$feature.description nofilter}</li>
                        {/foreach}
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <h3>{$aboutUsStrip.title nofilter}</h3>
                    <ul class="fa-ul">
                        {foreach $aboutUsStrip.features as $feature}
                            <li><i class="fa-li fa fa-star yellow"></i>{$feature.description nofilter}</li>
                        {/foreach}
                    </ul>
                </div>
            </div>
        </div>
    </div>
            
    <div class="width100 bg-grey1 padcard">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <a href="{$footer.link nofilter}" class="btn btn-lg btn-default top20 btm20">{$footer.cta nofilter}</a>
                    <p>{$footer.description nofilter}</p>
                </div>
            </div>
        </div>
    </div>
{/block}
