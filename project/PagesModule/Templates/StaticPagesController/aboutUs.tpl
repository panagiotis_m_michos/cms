{extends '@structure/layout.tpl'}

{block content}
    <div class="width100 bg-white padcard-mobile">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>{$introductionStrip.title nofilter}</h1>
                    <p class="lead">{$introductionStrip.lead nofilter}</p>
                    <p>{$introductionStrip.description nofilter}</p>
                    {foreach $faqStrip.faqBlock as $faqBlock}
                        <h4 class="display-inline">{$faqBlock.title nofilter}</h4>
                        {$faqBlock.description nofilter}
                    {/foreach}
                </div>
            </div>
        </div>
    </div>
    <div class="width100 bg-grey1 padcard-mobile">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <a href="{$footer.link nofilter}" class="btn btn-lg btn-default top20 btm20">{$footer.cta nofilter}</a>
                    <p>{$footer.description nofilter}</p>
                </div>
            </div>
        </div>
    </div>
{/block}
