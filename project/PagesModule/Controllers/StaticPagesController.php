<?php

namespace PagesModule\Controllers;

use Symfony\Component\HttpFoundation\Response;
use TemplateModule\Renderers\IRenderer;

class StaticPagesController
{

    /**
     * @var IRenderer
     */
    private $renderer;

    /**
     * @param IRenderer $renderer
     */
    public function __construct(IRenderer $renderer)
    {
        $this->renderer = $renderer;
    }

    /**
     * @return Response
     */
    public function aboutUs()
    {
        return $this->renderer->render();
    }
    
    /**
     * @return Response
     */
    public function whyChooseUs()
    {
        return $this->renderer->render();
    }

}
