<?php

namespace EmailModule\Emailers;

use Customer;
use EmailLogService;
use Entities;
use Entities\Customer as CustomerEntity;
use FEmail;
use IEmailService;

class LoggedEmailService implements IEmailService
{
    /**
     * @var EmailLogService
     */
    private $emailLogService;

    /**
     * @var IEmailService
     */
    private $emailService;

    /**
     * @param IEmailService $emailService
     * @param EmailLogService $emailLogService
     */
    public function __construct(IEmailService $emailService, EmailLogService $emailLogService)
    {
        $this->emailLogService = $emailLogService;
        $this->emailService = $emailService;
    }


    /**
     * @param FEmail $email
     * @param Customer|CustomerEntity $customer
     */
    public function send(FEmail $email, $customer = NULL)
    {
        $this->emailService->send($email, $customer);
        $this->emailLogService->logEmail($email, $customer);
    }
}
