<?php

namespace EmailModule\Emailers;

use EmailModule\Services\EmailGuardService;
use Entities\Customer;
use FEmail;
use IEmailService;
use Psr\Log\LoggerInterface;

class GuardedEmailService implements IEmailService
{
    /**
     * @var IEmailService
     */
    private $emailer;

    /**
     * @var EmailGuardService
     */
    private $guardService;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param IEmailService $emailer
     * @param EmailGuardService $guardService
     * @param LoggerInterface $logger
     */
    public function __construct(
        IEmailService $emailer,
        EmailGuardService $guardService,
        LoggerInterface $logger
    )
    {
        $this->emailer = $emailer;
        $this->guardService = $guardService;
        $this->logger = $logger;
    }

    /**
     * @param FEmail $email
     * @param Customer $customer
     */
    public function send(FEmail $email, $customer = NULL)
    {
        if ($this->guardService->guard($email)) {
            $this->emailer->send($email, $customer);
        } else {
            $emailContext = [
                'from' => $email->from,
                'to' => $email->getTo(),
                'cc' => $email->getCc(),
                'bcc' => $email->getBcc(),
                'subject' => $email->subject,
                'body' => $email->body,
            ];

            $this->logger->error('Unable to send email!', $emailContext);
        }
    }
}
