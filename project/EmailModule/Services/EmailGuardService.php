<?php

namespace EmailModule\Services;

use DateTime;
use EmailModule\Entities\EmailGuardLog;
use EmailModule\Repositories\EmailGuardLogRepository;
use FEmail;

class EmailGuardService
{
    /**
     * @var EmailGuardLogRepository
     */
    private $guardLogRepository;

    /**
     * @param EmailGuardLogRepository $guardLogRepository
     */
    public function __construct(EmailGuardLogRepository $guardLogRepository)
    {
        $this->guardLogRepository = $guardLogRepository;
    }

    /**
     * @param FEmail $email
     * @return bool
     */
    public function guard(FEmail $email)
    {
        $emailGuardLog = new EmailGuardLog($email);

        if (!$this->guardLogRepository->hadSameHashBefore($emailGuardLog->getEmailHash(), new DateTime('-1 day'))) {
            $this->guardLogRepository->saveEntity($emailGuardLog);

            return TRUE;
        }

        return FALSE;
    }
}
