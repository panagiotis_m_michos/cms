<?php

namespace EmailModule\Entities;

use DateTime;
use Doctrine\ORM\Mapping as Orm;
use FEmail;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class CustomerDetails
 * @Orm\Entity(repositoryClass="EmailModule\Repositories\EmailGuardLogRepository")
 * @Orm\Table(name="cms2_email_guard_log")
 */
class EmailGuardLog
{
    /**
     * @var int
     * @Orm\Column(type="integer", name="emailGuardLogId")
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @Orm\Column(type="string")
     */
    private $emailHash;

    /**
     * @var string
     * @Orm\Column(type="string", name="emailFrom")
     */
    private $from;

    /**
     * @var string
     * @Orm\Column(type="string", name="emailTo")
     */
    private $to;

    /**
     * @var string
     * @Orm\Column(type="string", name="emailSubject")
     */
    private $subject;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @param FEmail $email
     */
    public function __construct(FEmail $email)
    {
        $guardedData = [
            $email->getTo(),
            $email->body,
            $email->subject
        ];

        $this->emailHash = md5(serialize($guardedData));
        $this->to = implode(',', $email->getToArr());
        $this->from = implode(',', $email->getFromArr());
        $this->subject = $email->subject;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEmailHash()
    {
        return $this->emailHash;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
