<?php

namespace EmailModule\Repositories;

use DateTime;
use EmailModule\Entities\EmailGuardLog;
use Repositories\BaseRepository_Abstract;

class EmailGuardLogRepository extends BaseRepository_Abstract
{
    /**
     * @param string $hash
     * @param DateTime $from
     * @return EmailGuardLog|null
     */
    public function hadSameHashBefore($hash, DateTime $from)
    {
        return $this->createQueryBuilder('gl')
            ->where('gl.emailHash = :hash')
            ->andWhere('gl.createdAt > :from')
            ->setParameters([
                'hash' => $hash,
                'from' => $from,
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }
}
