<?php

namespace CsvParserModule\Observers;

use CsvParserModule\Exceptions\IncorrectStructureException;

class HeaderObserver implements IObserver
{
    /**
     * @var array
     */
    private $columns;

    /**
     * @var bool
     */
    private $checked = FALSE;

    /**
     * @param array $columns
     */
    public function __construct($columns)
    {
        $this->columns = $columns;
    }

    /**
     * @param array $row
     * @throws IncorrectStructureException
     */
    public function observe($row)
    {
        if (!$this->checked && $row != $this->columns) {
            throw IncorrectStructureException::invalidHeader($this->columns, $row);
        }

        $this->checked = TRUE;
    }
}
