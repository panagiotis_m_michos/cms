<?php

namespace CsvParserModule\Observers;

use CsvParserModule\Exceptions\IncorrectStructureException;

class ColumnCountObserver implements IObserver
{
    /**
     * @var int
     */
    private $columnCount;

    /**
     * @param int $columnCount
     */
    public function __construct($columnCount)
    {
        $this->columnCount = $columnCount;
    }

    /**
     * @param array $row
     * @throws IncorrectStructureException
     */
    public function observe($row)
    {
        if (count($row) != $this->columnCount) {
            throw IncorrectStructureException::wrongColumnCount($this->columnCount, count($row));
        }
    }
}
