<?php

namespace CsvParserModule\Observers;

interface IObserver
{
    /**
     * @param array $row
     */
    public function observe($row);
}
