<?php

namespace CsvParserModule;

use CsvParserModule\Exceptions\IncorrectStructureException;
use CsvParserModule\Observers\IObserver;
use Goodby\CSV\Import\Standard\Exception\StrictViolationException;
use Goodby\CSV\Import\Standard\Interpreter;
use Goodby\CSV\Import\Standard\Lexer;
use Utils\File;

class CsvParser
{

    /**
     * @var bool
     */
    private $skipHeader = FALSE;

    /**
     * @var Lexer
     */
    private $lexer;

    /**
     * @var Interpreter
     */
    private $interpreter;

    /**
     * @param Lexer $lexer
     * @param Interpreter $interpreter
     */
    public function __construct(Lexer $lexer, Interpreter $interpreter)
    {
        $this->lexer = $lexer;
        $this->interpreter = $interpreter;
    }

    /**
     * @param IObserver $observer
     */
    public function addObserver(IObserver $observer)
    {
        $this->interpreter->addObserver([$observer, 'observe']);
    }

    /**
     * @param bool $skip
     */
    public function setSkipHeader($skip)
    {
        $this->skipHeader = $skip;
    }

    /**
     * @param File $file
     * @return array
     * @throws IncorrectStructureException
     */
    public function parse(File $file)
    {
        $rows = [];
        $lineNumber = 1;
        $this->interpreter->addObserver(
            function ($row) use (&$lineNumber, &$rows) {
                if ($this->skipHeader && $lineNumber == 1) {
                    $lineNumber++;
                    return;
                }

                $rows[] = $row;
            }
        );

        try {
            $this->lexer->parse($file->getPath(), $this->interpreter);
        } catch (StrictViolationException $e) {
            throw IncorrectStructureException::fromStrictViolation($e);
        }

        return $rows;
    }
}
