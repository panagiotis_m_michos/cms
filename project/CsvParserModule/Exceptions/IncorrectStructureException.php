<?php

namespace CsvParserModule\Exceptions;

use Exceptions\Technical\TechnicalAbstract;
use Goodby\CSV\Import\Standard\Exception\StrictViolationException;

class IncorrectStructureException extends TechnicalAbstract
{
    /**
     * @param array $expected
     * @param array $actual
     * @return IncorrectStructureException
     */
    public static function invalidHeader($expected, $actual)
    {
        return new self(
            sprintf(
                'Wrong header found: [%s] expected, [%s] given',
                implode(', ', $expected),
                implode(', ', $actual)
            )
        );
    }

    /**
     * @param int $expected
     * @param int $actual
     * @return IncorrectStructureException
     */
    public static function wrongColumnCount($expected, $actual)
    {
        return new self(
            sprintf(
                'Wrong number of columns: %d expected, %d given',
                $expected,
                $actual
            )
        );
    }

    /**
     * @param StrictViolationException $e
     * @return IncorrectStructureException
     */
    public static function fromStrictViolation(StrictViolationException $e)
    {
        return new self($e->getMessage(), 0, $e);
    }
}
