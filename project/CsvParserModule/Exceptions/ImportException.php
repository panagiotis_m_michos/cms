<?php

namespace CsvParserModule\Exceptions;

use Exceptions\Technical\TechnicalAbstract;

class ImportException extends TechnicalAbstract
{
    /**
     * @var array
     */
    private $errors;

    /**
     * @param array $errors
     */
    public function __construct(array $errors)
    {
        parent::__construct(implode(' ', $errors));
        $this->errors = $errors;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }
}
