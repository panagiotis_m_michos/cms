<?php


class ConfigurationException extends RuntimeException
{
    /**
     * @param string $configParameter
     * @return ConfigurationException
     */
    public static function missingConfigParameter($configParameter)
    {
        return new self('Missing config parameter: '.$configParameter);
    }
}
