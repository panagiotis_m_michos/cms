<?php

namespace CompanyFormationModule\Views;

use CompanyIncorporation;
use Entities\Company;

class IncorporationSummaryViewFactory
{
    /**
     * @var IncorporationPscViewFactory
     */
    private $incorporationPscViewFactory;

    /**
     * @param IncorporationPscViewFactory $incorporationPscViewFactory
     */
    public function __construct(IncorporationPscViewFactory $incorporationPscViewFactory)
    {
        $this->incorporationPscViewFactory = $incorporationPscViewFactory;
    }

    /**
     * @param CompanyIncorporation $incorporation
     * @param Company $company
     * @return IncorporationSummaryView
     */
    public function from(CompanyIncorporation $incorporation, Company $company)
    {
        $pscs = array_merge($incorporation->getPersonPscs(), $incorporation->getCorporatePscs());
        $pscViews = $this->incorporationPscViewFactory->fromList($pscs, $company);

        return new IncorporationSummaryView($incorporation, $pscViews);
    }
}
