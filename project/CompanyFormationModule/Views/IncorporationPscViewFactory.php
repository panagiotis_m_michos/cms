<?php

namespace CompanyFormationModule\Views;

use CommonModule\UrlGenerator;
use Entities\Company;
use IncorporationCorporatePsc;
use IncorporationPersonPsc;

class IncorporationPscViewFactory
{
    /**
     * @var UrlGenerator
     */
    private $urlGenerator;

    /**
     * @param UrlGenerator $urlGenerator
     */
    public function __construct(UrlGenerator $urlGenerator)
    {
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * @param IncorporationPersonPsc|IncorporationCorporatePsc $psc
     * @param Company $company
     * @return PscView
     */
    public function fromPsc($psc, Company $company)
    {
        return new IncorporationPscView($this->urlGenerator, $company, $psc);
    }

    /**
     * @param IncorporationPersonPsc[]|IncorporationCorporatePsc[] $pscs
     * @param Company $company
     * @return IncorporationPscView[]
     */
    public function fromList(array $pscs, Company $company)
    {
        $views = [];
        foreach ($pscs as $psc) {
            $views[] = $this->fromPsc($psc, $company);
        }

        return $views;
    }
}
