<?php

namespace CompanyFormationModule\Views;

use CFPscsControler;
use CommonModule\UrlGenerator;
use Entities\Company;
use Entities\CompanyHouse\FormSubmission;
use IncorporationCorporatePsc;
use IncorporationPersonPsc;

class IncorporationPscView extends PscView
{
    /**
     * @var UrlGenerator
     */
    private $urlGenerator;

    /**
     * @var Company
     */
    private $company;

    /**
     * @var IncorporationCorporatePsc|IncorporationPersonPsc
     */
    protected $psc;

    /**
     * @param UrlGenerator $urlGenerator
     * @param Company $company
     * @param IncorporationPersonPsc|IncorporationCorporatePsc $psc
     */
    public function __construct(UrlGenerator $urlGenerator, Company $company, $psc)
    {
        parent::__construct($psc);
        $this->urlGenerator = $urlGenerator;
        $this->company = $company;
        $this->psc = $psc;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->psc->getId();
    }

    /**
     * @return string
     */
    public function getEditLink()
    {
        return $this->urlGenerator->link(
            $this->psc->isCorporate() ? CFPscsControler::EDIT_PSC_CORPORATE_PAGE : CFPscsControler::EDIT_PSC_PERSON_PAGE,
            [
                'company_id' => $this->company->getId(),
                'psc_id' => $this->psc->getId(),
            ]
        );
    }

    /**
     * @return string
     */
    public function getDeleteLink()
    {
        return $this->urlGenerator->link(
            CFPscsControler::PSCS_PAGE,
            [
                'company_id' => $this->company->getId(),
                $this->psc->isCorporate() ? 'delete_corporate_id' : 'delete_person_id' => $this->psc->getId(),
            ]
        );
    }
}
