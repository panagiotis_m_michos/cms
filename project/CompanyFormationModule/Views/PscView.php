<?php

namespace CompanyFormationModule\Views;

use Address;
use Corporate;
use CorporatePsc;
use Entities\CompanyHouse\FormSubmission;
use Identification;
use LegalPersonPsc;
use NatureOfControl;
use PeopleWithSignificantControl\Interfaces\IPsc;
use Person;
use PersonPsc;

class PscView
{
    const PERSON = 'person';
    const CORPORATE = 'corporate';
    const LEGAL_PERSON = 'legal person';

    //todo: psc - protected? kvoli extendujucim triedam
    /**
     * @var PersonPsc|CorporatePsc|LegalPersonPsc
     */
    protected $psc;

    /**
     * @param PersonPsc|CorporatePsc|LegalPersonPsc $psc
     */
    public function __construct($psc)
    {
        $this->psc = $psc;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        //todo: psc - fix
        if ($this->psc->getPscStatementNotification()) {
            return '-';
        } else {
            return ($this->psc->isCorporate() || $this->psc->isLegalPerson())
                ? $this->psc->getCorporate()->getCorporateName()
                : $this->psc->getPerson()->getFullName();
        }
    }

    /**
     * @return Person
     */
    public function getPerson()
    {
        return $this->psc->getPerson();
    }

    /**
     * @return Corporate
     */
    public function getCorporate()
    {
        return $this->psc->getCorporate();
    }

    /**
     * @return Address
     */
    public function getAddress()
    {
        return $this->psc->getAddress();
    }

    /**
     * @return Address
     */
    public function getResidentialAddress()
    {
        return $this->psc->getResidentialAddress();
    }

    /**
     * @return Identification
     */
    public function getIdentification()
    {
        return $this->psc->getIdentification();
    }

    /**
     * @return bool
     */
    public function isCorporate()
    {
        return $this->psc->isCorporate();
    }

    /**
     * @return bool
     */
    public function hasStatementNotification()
    {
        return (bool) $this->psc->getPscStatementNotification();
    }

    /**
     * @return string
     */
    public function getStatementNotificationText()
    {
        $texts = [
            IPsc::PSC_EXISTS_BUT_NOT_IDENTIFIED => 'The company knows or has reasonable cause to believe there is a registrable person in relation to the company and has been unable to identify the registrable person',
            IPsc::PSC_DETAILS_NOT_CONFIRMED => 'The company has identified a registrable person in relation to the company and all the required particulars of that person have not been confirmed',
            IPsc::PSC_CONTACTED_BUT_NO_RESPONSE => 'The company has given notice under section 790D of the Act; and the addressees of the notice have failed to comply with the notice within the time specified in it',
            IPsc::RESTRICTIONS_NOTICE_ISSUED_TO_PSC => 'The company has issued a restriction notice under paragraph 1 of Schedule 1B of the Companies Act 2006',
        ];

        return isset($texts[$this->psc->getPscStatementNotification()]) ? $texts[$this->psc->getPscStatementNotification()] : '';
    }

    /**
     * @return array
     */
    public function getNatureOfControlsTexts()
    {
        return array_filter(
            [
                $this->getOwnershipOfSharesText(),
                $this->getOwnershipOfVotingRightsText(),
                $this->getRightToAppointAndRemoveDirectorsText(),
                $this->getSignificantInfluenceOrControlText(),
            ]
        );
    }

    /**
     * @return string
     */
    private function getOwnershipOfSharesText()
    {
        $entity = $this->psc->isLegalPerson() ? self::LEGAL_PERSON : ($this->psc->isCorporate() ? self::CORPORATE : self::PERSON);

        $texts = [
            NatureOfControl::OWNERSHIPOFSHARES_25_TO_50_PERCENT => "The {$entity} holds more than 25% but not more than 50% of shares",
            NatureOfControl::OWNERSHIPOFSHARES_50_TO_75_PERCENT => "The {$entity} holds more than 50% but not more than 75% of shares",
            NatureOfControl::OWNERSHIPOFSHARES_75_TO_100_PERCENT => "The {$entity} holds more than 75% of shares",
            NatureOfControl::OWNERSHIPOFSHARES_25_TO_50_PERCENT_AS_FIRM => 'The members of the firm hold more than 25% but not more than 50% of shares',
            NatureOfControl::OWNERSHIPOFSHARES_50_TO_75_PERCENT_AS_FIRM => 'The members of the firm hold hold more than 50% but not more than 75% of shares',
            NatureOfControl::OWNERSHIPOFSHARES_75_TO_100_PERCENT_AS_FIRM => 'The members of the firm hold hold more than 75% of shares',
            NatureOfControl::OWNERSHIPOFSHARES_25_TO_50_PERCENT_AS_TRUST => 'The trustees of a trust hold more than 25% but not more than 50% of shares',
            NatureOfControl::OWNERSHIPOFSHARES_50_TO_75_PERCENT_AS_TRUST => 'The trustees of a trust hold more than 50% but not more than 75% of shares',
            NatureOfControl::OWNERSHIPOFSHARES_75_TO_100_PERCENT_AS_TRUST => 'The trustees of a trust hold more than 75% of shares',
            NatureOfControl::RIGHTTOSHARESURPLUSASSETS_25_TO_50_PERCENT => "The {$entity} holds more than 25% but not more than 50% of surplus assets",
            NatureOfControl::RIGHTTOSHARESURPLUSASSETS_50_TO_75_PERCENT => "The {$entity} holds more than 50% but not more than 75% of surplus assets",
            NatureOfControl::RIGHTTOSHARESURPLUSASSETS_75_TO_100_PERCENT => "The {$entity} holds more than 75% of surplus assets",
            NatureOfControl::RIGHTTOSHARESURPLUSASSETS_25_TO_50_PERCENT_AS_FIRM => 'The members of the firm hold more than 25% but not more than 50% of surplus assets',
            NatureOfControl::RIGHTTOSHARESURPLUSASSETS_50_TO_75_PERCENT_AS_FIRM => 'The members of the firm hold more than 50% but not more than 75% of surplus assets',
            NatureOfControl::RIGHTTOSHARESURPLUSASSETS_75_TO_100_PERCENT_AS_FIRM => 'The members of the firm hold more than 75% of surplus assets',
            NatureOfControl::RIGHTTOSHARESURPLUSASSETS_25_TO_50_PERCENT_AS_TRUST => 'The trustees of a trust hold more than 25% but not more than 50% of surplus assets',
            NatureOfControl::RIGHTTOSHARESURPLUSASSETS_50_TO_75_PERCENT_AS_TRUST => 'The trustees of a trust hold more than 50% but not more than 75% of surplus assets',
            NatureOfControl::RIGHTTOSHARESURPLUSASSETS_75_TO_100_PERCENT_AS_TRUST => 'The trustees of a trust hold more than 75% of surplus assets',
        ];

        return isset($texts[$this->psc->getNatureOfControl()->getOwnershipOfShares()]) ? $texts[$this->psc->getNatureOfControl()->getOwnershipOfShares()] : '';
    }

    /**
     * @return string
     */
    private function getOwnershipOfVotingRightsText()
    {
        $entity = $this->psc->isLegalPerson() ? self::LEGAL_PERSON : ($this->psc->isCorporate() ? self::CORPORATE : self::PERSON);

        $texts = [
            NatureOfControl::VOTINGRIGHTS_25_TO_50_PERCENT => "The {$entity} holds more than 25% but not more than 50% of voting rights",
            NatureOfControl::VOTINGRIGHTS_50_TO_75_PERCENT => "The {$entity} holds more than 50% but not more than 75% of voting rights",
            NatureOfControl::VOTINGRIGHTS_75_TO_100_PERCENT => "The {$entity} holds more than 75% of voting rights",
            NatureOfControl::VOTINGRIGHTS_25_TO_50_PERCENT_AS_FIRM => 'The members of the firm hold more than 25% but not more than 50% of voting rights',
            NatureOfControl::VOTINGRIGHTS_50_TO_75_PERCENT_AS_FIRM => 'The members of the firm hold hold more than 50% but not more than 75% of voting rights',
            NatureOfControl::VOTINGRIGHTS_75_TO_100_PERCENT_AS_FIRM => 'The members of the firm hold hold more than 75% of voting rights',
            NatureOfControl::VOTINGRIGHTS_25_TO_50_PERCENT_AS_TRUST => 'The trustees of a trust hold more than 25% but not more than 50% of voting rights',
            NatureOfControl::VOTINGRIGHTS_50_TO_75_PERCENT_AS_TRUST => 'The trustees of a trust hold more than 50% but not more than 75% of voting rights',
            NatureOfControl::VOTINGRIGHTS_75_TO_100_PERCENT_AS_TRUST => 'The trustees of a trust hold more than 75% of voting rights',
        ];

        return isset($texts[$this->psc->getNatureOfControl()->getOwnershipOfVotingRights()]) ? $texts[$this->psc->getNatureOfControl()->getOwnershipOfVotingRights()] : '';
    }

    /**
     * @return string
     */
    private function getRightToAppointAndRemoveDirectorsText()
    {
        $entity = $this->psc->isLegalPerson() ? self::LEGAL_PERSON : ($this->psc->isCorporate() ? self::CORPORATE : self::PERSON);

        $texts = [
            NatureOfControl::RIGHTTOAPPOINTANDREMOVEMEMBERS => "The {$entity} has the right to appoint or remove the majority of the board of directors of the company",
            NatureOfControl::RIGHTTOAPPOINTANDREMOVEMEMBERS_AS_FIRM => "The {$entity} has control over the firm and members of that firm (in their capacity as such) hold the right to appoint of remove the majority of the board of directors of the company",
            NatureOfControl::RIGHTTOAPPOINTANDREMOVEMEMBERS_AS_TRUST => "The {$entity} has control over the truste and the trustees of that trust (in their capacity as such) hold the right to appoint or remove the majority of the board of directors of the company",
            NatureOfControl::RIGHTTOAPPOINTANDREMOVEDIRECTORS => "The {$entity} has the right to appoint or remove the majority of the board of directors of the company",
            NatureOfControl::RIGHTTOAPPOINTANDREMOVEDIRECTORS_AS_FIRM => "The {$entity} has control over the firm and members of that firm (in their capacity as such) hold the right to appoint of remove the majority of the board of directors of the company",
            NatureOfControl::RIGHTTOAPPOINTANDREMOVEDIRECTORS_AS_TRUST => "The {$entity} has control over the truste and the trustees of that trust (in their capacity as such) hold the right to appoint or remove the majority of the board of directors of the company",
        ];

        return isset($texts[$this->psc->getNatureOfControl()->getRightToAppointAndRemoveDirectors()]) ? $texts[$this->psc->getNatureOfControl()->getRightToAppointAndRemoveDirectors()] : '';
    }

    /**
     * @return string
     */
    private function getSignificantInfluenceOrControlText()
    {
        $entity = $this->psc->isLegalPerson() ? self::LEGAL_PERSON : ($this->psc->isCorporate() ? self::CORPORATE : self::PERSON);

        $texts = [
            NatureOfControl::SIGINFLUENCECONTROL => "The {$entity} has significant influence or control over the company",
            NatureOfControl::SIGINFLUENCECONTROL_AS_FIRM => "The {$entity} has control over the firm and members of that firm (in their capacity as such) have significant influence or control over the company",
            NatureOfControl::SIGINFLUENCECONTROL_AS_TRUST => "The {$entity} has control over the trust and the trustees of that trust (in their capacity as such) have significant influence or control over the company",
        ];

        return isset($texts[$this->psc->getNatureOfControl()->getSignificantInfluenceOrControl()]) ? $texts[$this->psc->getNatureOfControl()->getSignificantInfluenceOrControl()] : '';
    }
}
