<?php

namespace CompanyFormationModule\Views;

use CorporatePsc;
use PersonPsc;

class PscViewFactory
{
    /**
     * @param PersonPsc|CorporatePsc $psc
     * @return PscView
     */
    public static function fromPsc($psc)
    {
        return new PscView($psc);
    }

    /**
     * @param PersonPsc[]|CorporatePsc[] $pscs
     * @return PscView[]
     */
    public static function fromList(array $pscs)
    {
        $views = [];
        foreach ($pscs as $psc) {
            $views[] = new PscView($psc);
        }

        return $views;
    }
}
