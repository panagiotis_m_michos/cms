<?php

namespace CompanyFormationModule\Views;

use Entities\CompanyHouse\FormSubmission;
use FormSubmissionModule\Views\FormSubmissionsView;
use FormSubmissionModule\Views\FormSubmissionView;

class CFSummaryControlerView
{
    /**
     * @var FormSubmissionsView
     */
    private $formSubmissionsView;

    /**
     * @param FormSubmissionsView $formSubmissionsView
     */
    public function __construct(FormSubmissionsView $formSubmissionsView)
    {
        $this->formSubmissionsView = $formSubmissionsView;
    }

    /**
     * @return FormSubmissionView[]
     */
    public function getFormSubmissions()
    {
        return $this->formSubmissionsView->getFormSubmissionViews();
    }
}
