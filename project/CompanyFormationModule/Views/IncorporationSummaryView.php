<?php

namespace CompanyFormationModule\Views;

use CompanyIncorporation;
use Entities\CompanyHouse\FormSubmission;
use PeopleWithSignificantControl\Interfaces\IPscAware;

class IncorporationSummaryView
{
    const PERSON = 'person';
    const CORPORATE = 'corporate';

    /**
     * @var CompanyIncorporation
     */
    private $incorporation;

    /**
     * @var IncorporationPscView[]
     */
    private $pscViews;

    /**
     * @param CompanyIncorporation $incorporation
     * @param IncorporationPscView[] $pscViews
     */
    public function __construct(CompanyIncorporation $incorporation, array $pscViews)
    {
        $this->incorporation = $incorporation;
        $this->pscViews = $pscViews;
    }

    /**
     * @return IncorporationPscView[]
     */
    public function getPscs()
    {
        return $this->pscViews;
    }

    /**
     * @return IncorporationPscView[]
     */
    public function getPersonPscs()
    {
        $persons = [];
        foreach ($this->pscViews as $pscView) {
            if (!$pscView->isCorporate()) {
                $persons[] = $pscView;
            }
        }

        return $persons;
    }

    /**
     * @return IncorporationPscView[]
     */
    public function getCorporatePscs()
    {
        $corporates = [];
        foreach ($this->pscViews as $pscView) {
            if ($pscView->isCorporate()) {
                $corporates[] = $pscView;
            }
        }

        return $corporates;
    }

    /**
     * @return bool
     */
    public function hasPscs()
    {
        return (bool) $this->pscViews;
    }

    /**
     * @return bool
     */
    public function hasNoPscReason()
    {
        return $this->incorporation->hasNoPscReason();
    }
    
    /**
     * @return string
     */
    public function getNoPscReason()
    {
        $texts = [
            IPscAware::NO_INDIVIDUAL_OR_ENTITY_WITH_SIGNFICANT_CONTROL => 'The company knows or has reasonable cause to believe that there is no registrable person or registrable relevant legal entity in relation to the company.',
        ];

        return isset($texts[$this->incorporation->getNoPscReason()]) ? $texts[$this->incorporation->getNoPscReason()] : '';
    }
}
