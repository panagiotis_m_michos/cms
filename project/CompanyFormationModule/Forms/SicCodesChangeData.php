<?php

namespace CompanyFormationModule\Forms;

use CompanyFormationModule\Forms\Validators\IsSicCode;
use Symfony\Component\Validator\Constraints as Assert;

class SicCodesChangeData
{
    /**
     * @var string
     * @Assert\NotBlank()
     * @IsSicCode()
     */
    private $sicCode0;

    /**
     * @var string
     * @IsSicCode()
     */
    private $sicCode1;

    /**
     * @var string
     * @IsSicCode()
     */
    private $sicCode2;

    /**
     * @var string
     * @IsSicCode()
     */
    private $sicCode3;

    /**
     * @param array $sicCodes
     */
    public function setSicCodes(array $sicCodes)
    {
        $sicCodes = array_pad(array_values(array_unique($sicCodes)), 4, NULL);

        $this->sicCode0 = $sicCodes[0];
        $this->sicCode1 = $sicCodes[1];
        $this->sicCode2 = $sicCodes[2];
        $this->sicCode3 = $sicCodes[3];
    }

    /**
     * @return string[]
     */
    public function getSicCodes()
    {
        return array_filter(
            [
                $this->getSicCode0(),
                $this->getSicCode1(),
                $this->getSicCode2(),
                $this->getSicCode3(),
            ]
        );
    }

    /**
     * @return string
     */
    public function getSicCode0()
    {
        return $this->sicCode0;
    }

    /**
     * @param string $sicCode0
     */
    public function setSicCode0($sicCode0)
    {
        $this->sicCode0 = $sicCode0;
    }

    /**
     * @return string
     */
    public function getSicCode1()
    {
        return $this->sicCode1;
    }

    /**
     * @param string $sicCode1
     */
    public function setSicCode1($sicCode1)
    {
        $this->sicCode1 = $sicCode1;
    }

    /**
     * @return string
     */
    public function getSicCode2()
    {
        return $this->sicCode2;
    }

    /**
     * @param string $sicCode2
     */
    public function setSicCode2($sicCode2)
    {
        $this->sicCode2 = $sicCode2;
    }

    /**
     * @return string
     */
    public function getSicCode3()
    {
        return $this->sicCode3;
    }

    /**
     * @param string $sicCode3
     */
    public function setSicCode3($sicCode3)
    {
        $this->sicCode3 = $sicCode3;
    }
}
