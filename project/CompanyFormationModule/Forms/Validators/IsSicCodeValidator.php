<?php

namespace CompanyFormationModule\Forms\Validators;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class IsSicCodeValidator extends ConstraintValidator
{
    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof IsSicCode) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\IsSicCode');
        }

        if (!empty($value) && !preg_match('/[0-9]{5}/', $value)) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}
