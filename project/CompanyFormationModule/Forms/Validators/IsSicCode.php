<?php

namespace CompanyFormationModule\Forms\Validators;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsSicCode extends Constraint
{
    /**
     * @var string
     */
    public $message = 'SIC code must be 5 digits';
}
