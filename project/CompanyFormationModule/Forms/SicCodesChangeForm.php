<?php

namespace CompanyFormationModule\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class SicCodesChangeForm extends AbstractType
{
    /**
     * @return string The name of this type
     */
    public function getName()
    {
        return 'SicCodeChangeForm';
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add(
            'sicCode0',
            'text',
            [
                'required' => TRUE,
                'label' => 'SIC code',
                'attr' => [
                    'pattern' => '[0-9]{5}',
                    'title' => '5 digit SIC code',
                ]
            ]
        );

        $builder->add(
            'sicCode1',
            'text',
            [
                'required' => FALSE,
                'label' => 'SIC code',
                'attr' => [
                    'pattern' => '[0-9]{5}',
                    'title' => '5 digit SIC code'
                ]
            ]
        );

        $builder->add(
            'sicCode2',
            'text',
            [
                'required' => FALSE,
                'label' => 'SIC code',
                'attr' => [
                    'pattern' => '[0-9]{5}',
                    'title' => '5 digit SIC code'
                ]
            ]
        );

        $builder->add(
            'sicCode3',
            'text',
            [
                'required' => FALSE,
                'label' => 'SIC code',
                'attr' => [
                    'pattern' => '[0-9]{5}',
                    'title' => '5 digit SIC code'
                ]
            ]
        );

        $builder->add(
            'save',
            'submit',
            [
                'label' => 'Save changes',
                'attr' => [
                    'class' => 'btn-orange'
                ]
            ]
        );
    }
}
