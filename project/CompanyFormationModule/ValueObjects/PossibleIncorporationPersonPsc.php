<?php

namespace CompanyFormationModule\ValueObjects;

use IncorporationPersonDirector;
use NatureOfControl;
use PersonMember;

class PossibleIncorporationPersonPsc
{
    /**
     * @var IncorporationPersonDirector
     */
    private $director;

    /**
     * @var NatureOfControl
     */
    private $natureOfControl;

    /**
     * @param IncorporationPersonDirector $director
     * @param NatureOfControl $natureOfControl
     */
    public function __construct(
        IncorporationPersonDirector $director,
        NatureOfControl $natureOfControl
    )
    {
        $this->natureOfControl = $natureOfControl;
        $this->director = $director;
    }

    /**
     * @return IncorporationPersonDirector
     */
    public function getDirector()
    {
        return $this->director;
    }

    /**
     * @return NatureOfControl
     */
    public function getNatureOfControl()
    {
        return $this->natureOfControl;
    }

    /**
     * @param PersonMember $person
     * @return bool
     */
    public function isSamePerson(PersonMember $person)
    {
        return $this->getDirector()->isSamePerson($person);
    }
}
