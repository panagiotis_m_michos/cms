{extends '@structure/layout.tpl'}

{block content}
    <div class="width100 bg-white padcard-mobile">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>{$introductionStrip.title nofilter}</h1>
                    <p class="lead">{$introductionStrip.lead nofilter}</p>
                </div>
            </div>
            {foreach $featuresStrip.features as $feature}
                <div class="row btm20">
                    <div class="col-xs-2 col-sm-1">
                        <img class="img-responsive" src="{$feature.img nofilter}" alt="Step">
                    </div>
                    <div class="col-xs-10 col-sm-11">
                        <h3 class="top0">{$feature.title nofilter}</h3>
                        {$feature.description nofilter}
                    </div>
                </div>
            {/foreach}
        </div>
    </div>
            
    <div class="width100 bg-grey1 padcard">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <a href="{$footer.link nofilter}" class="btn btn-lg btn-default top20 btm20">{$footer.cta nofilter}</a>
                    <p>{$footer.description nofilter}</p>
                </div>
            </div>
        </div>
    </div>
{/block}
