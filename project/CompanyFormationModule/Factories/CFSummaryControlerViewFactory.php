<?php

namespace CompanyFormationModule\Factories;

use CompanyFormationModule\Views\CFSummaryControlerView;
use Entities\Company;
use FormSubmissionModule\Factories\FormSubmissionsViewFactory;

class CFSummaryControlerViewFactory
{
    /**
     * @var FormSubmissionsViewFactory
     */
    private $formSubmissionsViewFactory;

    /**
     * @param FormSubmissionsViewFactory $formSubmissionsViewFactory
     */
    public function __construct(FormSubmissionsViewFactory $formSubmissionsViewFactory)
    {
        $this->formSubmissionsViewFactory = $formSubmissionsViewFactory;
    }

    /**
     * @param Company $company
     * @return CFSummaryControlerView
     */
    public function create(Company $company)
    {
        $formSubmissionsView = $this->formSubmissionsViewFactory->createFromCompanyWithResponse($company);

        return new CFSummaryControlerView($formSubmissionsView);
    }
}
