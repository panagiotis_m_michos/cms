<?php

namespace VoServiceModule\Repositories;

use Doctrine\ORM\Internal\Hydration\IterableResult;
use Entities\Company;
use Entities\OrderItem;
use Repositories\BaseRepository_Abstract;
use VoServiceModule\Entities\VoServiceQueue;

class VoServiceQueueRepository extends BaseRepository_Abstract
{
    /**
     * @return IterableResult
     */
    public function getServicesToSend()
    {
        return $this->createQueryBuilder('s')->getQuery()->iterate();
    }

    /**
     * @return IterableResult
     */
    public function getServicesWithEmptyProductName()
    {
        return $this->createQueryBuilder('q')
            ->where('q.productName = ?1')
            ->setParameter(1, '')
            ->getQuery()
            ->iterate();
    }

    /**
     * @param Company $company
     * @return VoServiceQueue[]
     */
    public function getServicesByCompany(Company $company)
    {
        return $this->createQueryBuilder('q')
            ->where('q.company = :company')
            ->setParameter('company', $company)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param OrderItem $orderItem
     * @return VoServiceQueue|NULL
     */
    public function getServiceByOrderItem(OrderItem $orderItem)
    {
        return $this->createQueryBuilder('q')
            ->where('q.orderItem = :orderItem')
            ->setParameter('orderItem', $orderItem)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
