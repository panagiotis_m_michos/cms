<?php

namespace VoServiceModule\Entities;

use DateTime;
use Doctrine\ORM\Mapping as Orm;
use Entities\Company;
use Entities\Customer;
use Entities\EntityAbstract;
use Entities\OrderItem;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @Orm\Entity(repositoryClass = "VoServiceModule\Repositories\VoServiceQueueRepository")
 * @Orm\Table(name="cms2_vo_services_queue")
 */
class VoServiceQueue extends EntityAbstract
{
    /**
     * @var integer
     *
     * @Orm\Column(type="integer", name="voServiceQueueId")
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Customer
     *
     * @Orm\ManyToOne(targetEntity="Entities\Customer", cascade={"persist"})
     * @Orm\JoinColumn(name="customerId", referencedColumnName="customerId")
     */
    private $customer;

    /**
     * @var Company
     *
     * @Orm\ManyToOne(targetEntity="Entities\Company", cascade={"persist"})
     * @Orm\JoinColumn(name="companyId", referencedColumnName="company_id")
     */
    private $company;

    /**
     * @var OrderItem
     * @Orm\OneToOne(targetEntity="Entities\OrderItem", cascade={"persist"})
     * @Orm\JoinColumn(name="orderItemId", referencedColumnName="orderItemId")
     */
    private $orderItem;

    /**
     * @var integer
     *
     * @Orm\Column(type="integer")
     */
    private $productId;

    /**
     * @var string
     *
     * @Orm\Column(type="string")
     */
    private $productName;

    /**
     * @var integer
     *
     * @Orm\Column(type="integer")
     */
    private $durationInMonths;

    /**
     * @var DateTime
     *
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;

    /**
     * @var DateTime
     *
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create", on="update")
     */
    private $dtm;

    /**
     * @param Customer $customer
     * @param Company $company
     * @param OrderItem $orderItem
     * @param int $productId
     * @param string $productName
     * @param int $durationInMonths
     */
    public function __construct(
        Customer $customer,
        Company $company,
        OrderItem $orderItem,
        $productId,
        $productName,
        $durationInMonths
    )
    {
        $this->customer = $customer;
        $this->company = $company;
        $this->orderItem = $orderItem;
        $this->productId = $productId;
        $this->productName = $productName;
        $this->durationInMonths = $durationInMonths;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @return string
     */
    public function getCustomerEmail()
    {
        return $this->customer->getEmail();
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return OrderItem
     */
    public function getOrderItem()
    {
        return $this->orderItem;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @return string
     */
    public function getProductName()
    {
        return $this->productName;
    }

    /**
     * @return int
     */
    public function getDurationInMonths()
    {
        return $this->durationInMonths;
    }

    /**
     * @return DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @return DateTime
     */
    public function getDtm()
    {
        return $this->dtm;
    }

    /**
     * @param string $productName
     */
    public function setProductName($productName)
    {
        $this->productName = $productName;
    }
}
