<?php

namespace VoServiceModule\Facades;

use Entities\Company;
use Entities\Order;
use Entities\OrderItem;
use VoServiceModule\Entities\VoServiceQueue;
use Nette\Object;
use Services\CompanyService;
use Services\NodeService;
use VoServiceModule\Services\VoServiceQueueService;

class VoServiceFacade extends Object
{
    /**
     * @var VoServiceQueueService
     */
    private $voServiceQueueService;

    /**
     * @var NodeService
     */
    private $nodeService;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @param VoServiceQueueService $voServiceQueueService
     * @param NodeService $nodeService
     * @param CompanyService $companyService
     */
    public function __construct(
        VoServiceQueueService $voServiceQueueService,
        NodeService $nodeService,
        CompanyService $companyService
    )
    {
        $this->voServiceQueueService = $voServiceQueueService;
        $this->nodeService = $nodeService;
        $this->companyService = $companyService;
    }

    /**
     * @param Order $order
     */
    public function save(Order $order)
    {
        $customer = $order->getCustomer();
        foreach ($order->getItems() as $orderItem) {
            $product = $this->nodeService->getProductById($orderItem->getProductId(), TRUE);
            if ($product->isVoServiceEligible()) {
                $company = $this->companyService->getCompanyByOrder($orderItem->getOrder()) ?: $orderItem->getCompany();

                $queue = new VoServiceQueue(
                    $customer,
                    $company,
                    $orderItem,
                    $product->getId(),
                    $product->getLngTitle(),
                    $product->getVoServiceDurationInMonths()
                );
                $this->voServiceQueueService->save($queue);
            }
        }
    }

    /**
     * @param Company $company
     */
    public function deleteByCompany(Company $company)
    {
        foreach ($this->voServiceQueueService->getServicesByCompany($company) as $item) {
            $this->voServiceQueueService->remove($item);
        }
    }

    /**
     * @param OrderItem $orderItem
     */
    public function deleteByOrderItem(OrderItem $orderItem)
    {
        $item = $this->voServiceQueueService->getServiceByOrderItem($orderItem);
        if ($item) {
            $this->voServiceQueueService->remove($item);
        }
    }
}
