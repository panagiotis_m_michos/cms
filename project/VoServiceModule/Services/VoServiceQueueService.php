<?php

namespace VoServiceModule\Services;

use DateTime;
use Doctrine\ORM\Internal\Hydration\IterableResult;
use Entities\Company;
use Entities\OrderItem;
use VoServiceModule\Entities\VoServiceQueue;
use Nette\Object;
use VoServiceModule\Repositories\VoServiceQueueRepository;

class VoServiceQueueService extends Object
{
    /**
     * @var VoServiceQueueRepository
     */
    private $repository;

    /**
     * @param VoServiceQueueRepository $repository
     */
    public function __construct(VoServiceQueueRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return IterableResult
     */
    public function getServicesToSend()
    {
        return $this->repository->getServicesToSend();
    }

    /**
     * @param VoServiceQueue $entity
     */
    public function remove(VoServiceQueue $entity)
    {
        $this->repository->removeEntity($entity);
    }

    /**
     * @param VoServiceQueue $entity
     */
    public function save(VoServiceQueue $entity)
    {
        $this->repository->saveEntity($entity);
    }

    /**
     * @return IterableResult
     */
    public function getServicesWithEmptyProductName()
    {
        return $this->repository->getServicesWithEmptyProductName();
    }

    /**
     * @param Company $company
     * @return IterableResult|VoServiceQueue[]
     */
    public function getServicesByCompany(Company $company)
    {
        return $this->repository->getServicesByCompany($company);
    }

    /**
     * @param OrderItem $orderItem
     * @return VoServiceQueue|NULL
     */
    public function getServiceByOrderItem(OrderItem $orderItem)
    {
        return $this->repository->getServiceByOrderItem($orderItem);
    }

    /**
     * @param VoServiceQueue $serviceQueue
     * @return bool
     */
    public function canBeSend(VoServiceQueue $serviceQueue)
    {
        return $serviceQueue->getCustomer()->hasCompletedRegistration() && $serviceQueue->getCompany()->isIncorporated();
    }
}
