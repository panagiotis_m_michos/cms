<?php

namespace VoServiceModule\Config;

class DiLocator
{
    const NIKOLAI_LOG_SENT_VO_SERVICES = 'vo_service_module.commands.log_sent_vo_services_command';
    const LISTENER_VO_SERVICE_QUEUE = 'vo_service_module.listeners.vo_service_queue_listener';
    const LISTENER_DELETE_PRODUCT_FROM_QUEUE = 'vo_service_module.listeners.delete_product_from_queue_listener';
}
