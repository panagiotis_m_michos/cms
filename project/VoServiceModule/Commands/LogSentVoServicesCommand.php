<?php

namespace VoServiceModule\Commands;

use DibiConnection;
use Doctrine\ORM\EntityManager;
use VoServiceModule\Entities\VoServiceQueue;
use Exception;
use Psr\Log\LoggerInterface;
use VoServiceModule\Repositories\VoServiceQueueRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LogSentVoServicesCommand extends Command
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var VoServiceQueueRepository
     */
    private $queueRepository;

    /**
     * @param EntityManager $em
     * @param LoggerInterface $logger
     * @param VoServiceQueueRepository $queueRepository
     * @param string $name
     */
    public function __construct(
        EntityManager $em,
        LoggerInterface $logger,
        VoServiceQueueRepository $queueRepository,
        $name = NULL
    )
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->queueRepository = $queueRepository;

        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('services:logSentVoServices')
            ->setDescription('Create logs for already sent VO services')
            ->addOption('dry-run', 'd', InputOption::VALUE_NONE, 'If set, changes are not flushed to database');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $processed = 0;

        if ($input->getOption('dry-run')) {
            $this->logger->debug("Dry-run flag set");
        }

        $services = $this->queueRepository->createQueryBuilder('q')
            ->where('q.dateSent IS NOT NULL')
            ->getQuery()
            ->iterate();

        foreach ($services as $row) {
            /** @var VoServiceQueue $service */
            $service = $row[0];

            $this->logger->debug(
                'Sending VO service',
                [
                    'customerEmail' => $service->getCustomerEmail(),
                    'productId' => $service->getProductId(),
                    'productName' => $service->getProductName(),
                    'orderId' => $service->getOrderId(),
                    'durationInMonths' => $service->getDurationInMonths(),
                    'dateSent' => $service->getDateSent()->format('d/m/Y H:i:s'),
                ]
            );

            $this->em->remove($service);
            $processed++;
        }

        $this->flushUpdates($input);
        $this->logger->debug("$processed services logged and deleted.");
    }

    /**
     * @param InputInterface $input
     */
    private function flushUpdates(InputInterface $input)
    {
        if (!$input->getOption('dry-run')) {
            $this->em->flush();
        }
        $this->em->clear();
        $this->logger->debug('Updates have been executed');
    }
}
