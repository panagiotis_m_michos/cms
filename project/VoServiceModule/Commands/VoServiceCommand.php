<?php

namespace VoServiceModule\Commands;

use Cron\Commands\CommandAbstract;
use Cron\Commands\ICommand;
use Cron\INotifier;
use DateTime;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Psr\Log\LoggerInterface;
use VoServiceModule\Entities\VoServiceQueue;
use VoServiceModule\RequestData\VoServiceData;
use VoServiceModule\Services\VoServiceQueueService;

class VoServiceCommand extends CommandAbstract implements ICommand
{
    /**
     * @var string
     */
    protected $timeType = self::TIME_TYPE_INTERVAL;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var INotifier
     */
    protected $notifier;

    /**
     * @var VoServiceQueueService
     */
    private $voServiceQueueService;

    /**
     * @var Client
     */
    private $client;

    /**
     * @param LoggerInterface $logger
     * @param INotifier $notifier
     * @param VoServiceQueueService $voServiceQueueService
     * @param ClientInterface $client
     */
    public function __construct(
        LoggerInterface $logger,
        INotifier $notifier,
        VoServiceQueueService $voServiceQueueService,
        ClientInterface $client
    )
    {
        $this->logger = $logger;
        $this->notifier = $notifier;
        $this->voServiceQueueService = $voServiceQueueService;
        $this->client = $client;
    }

    public function execute()
    {
        $items = $this->voServiceQueueService->getServicesToSend();
        foreach ($items as $row) {
            /** @var VoServiceQueue $item */
            $item = $row[0];

            if (!$this->voServiceQueueService->canBeSend($item)) {
                continue;
            }

            try {
                $this->client->post(
                    'cms/service/',
                    [
                        'json' => new VoServiceData($item),
                        'headers' => [
                            'content-type' => 'application/json'
                        ]
                    ]
                );

                $this->logSuccess($item);
                $this->voServiceQueueService->remove($item);
            } catch (ClientException $e) {
                $this->logClientException($item, $e);
            } catch (ServerException $e) {
                $this->logServerException($item, $e);
            } catch (Exception $e) {
                $this->logOtherException($item, $e);
            }
        }
        $this->notify();
    }

    /**
     * @param VoServiceQueue $item
     * @param BadResponseException $e
     */
    private function logClientException(VoServiceQueue $item, BadResponseException $e)
    {
        $this->logException('Client error', $item, $this->getResponseMessage($e));
    }

    /**
     * @param VoServiceQueue $item
     */
    private function logSuccess(VoServiceQueue $item)
    {
        $this->logger->debug(
            'Sending VO service and deleting from queue',
            [
                'id' => $item->getId(),
                'customerId' => $item->getCustomer()->getId(),
                'companyId' => $item->getCompany()->getId(),
                'orderItemId' => $item->getOrderItem()->getId(),
                'productId' => $item->getProductId(),
                'productName' => $item->getProductName(),
            ]
        );
    }

    /**
     * @param VoServiceQueue $item
     * @param Exception $e
     */
    private function logOtherException(VoServiceQueue $item, Exception $e)
    {
        $this->logException('Sending VO service failed', $item, $e);
    }

    /**
     * @param VoServiceQueue $item
     * @param BadResponseException $e
     */
    private function logServerException(VoServiceQueue $item, BadResponseException $e)
    {
        $this->logException('Server error', $item, $this->getResponseMessage($e));
    }

    /**
     * @param string $message
     * @param VoServiceQueue $item
     * @param mixed $exception
     */
    private function logException($message, VoServiceQueue $item, $exception)
    {
        $this->logger->error(
            $message,
            [
                'id' => $item->getId(),
                'customerId' => $item->getCustomer()->getId(),
                'companyId' => $item->getCompany()->getId(),
                'orderItemId' => $item->getOrderItem()->getId(),
                'exception' => $exception,
            ]
        );
    }

    /**
     * @param BadResponseException $e
     * @return string
     */
    private function getResponseMessage(BadResponseException $e)
    {
        $responseBody = $e->getResponse()->json();
        $message = isset($responseBody['error']) ? $responseBody['error'] : $e->getMessage();

        return $message;
    }

    private function notify()
    {
        $date = new DateTime();
        $this->notifier->triggerSuccess(
            $this->getName(),
            $date->getTimestamp(),
            sprintf('Queue VO services. Date %s', $date->format('d/m/Y'))
        );
    }
}
