<?php

namespace VoServiceModule\RequestData;

use Entities\Customer;
use JsonSerializable;

class AddressData implements JsonSerializable
{
    /**
     * @var Customer
     */
    private $customer;

    /**
     * @param Customer $customer
     */
    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'address1' => $this->customer->getAddress1(),
            'address2' => $this->customer->getAddress2(),
            'address3' => $this->customer->getAddress3(),
            'postcode' => $this->customer->getPostcode(),
            'city' => $this->customer->getCity(),
            'countryId' => $this->customer->getCountry2Letter(),
        ];
    }
}
