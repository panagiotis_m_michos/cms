<?php

namespace VoServiceModule\RequestData;

use Entities\Customer;
use Customer as OldCustomer;
use JsonSerializable;
use Nette\Utils\Arrays;

class CustomerData implements JsonSerializable
{
    /**
     * @var Customer
     */
    private $customer;

    /**
     * @param Customer $customer
     */
    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'title' => Arrays::get(OldCustomer::$titles, $this->customer->getTitleId(), NULL),
            'firstName' => $this->customer->getFirstName(),
            'lastName' => $this->customer->getLastName(),
            'phone' => $this->customer->getPhone(),
            'email' => $this->customer->getEmail(),
        ];
    }
}
