<?php

namespace VoServiceModule\RequestData;

use JsonSerializable;
use VoServiceModule\Entities\VoServiceQueue;

class VoServiceData implements JsonSerializable
{
    /**
     * @var VoServiceQueue
     */
    private $voService;

    /**
     * @param VoServiceQueue $voService
     */
    public function __construct(VoServiceQueue $voService)
    {
        $this->voService = $voService;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'customer' => new CustomerData($this->voService->getCustomer()),
            'address' => new AddressData($this->voService->getCustomer()),
            'companyName' => $this->voService->getCompany()->getCompanyName(),
            'orderId' => $this->voService->getOrderItem()->getOrder()->getId(),
            'productId' => $this->voService->getProductId(),
            'productName' => $this->voService->getProductName(),
            'durationInMonths' => $this->voService->getDurationInMonths(),
        ];
    }
}
