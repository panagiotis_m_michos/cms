<?php

namespace VoServiceModule\Listeners;

use Dispatcher\Events\OrderEvent;
use Nette\Object;
use VoServiceModule\Facades\VoServiceFacade;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use EventLocator;

class VoServiceQueueListener extends Object implements EventSubscriberInterface
{
    /**
     * @var VoServiceFacade
     */
    private $voServiceFacade;

    /**
     * @param VoServiceFacade $voServiceFacade
     */
    public function __construct(VoServiceFacade $voServiceFacade)
    {
        $this->voServiceFacade = $voServiceFacade;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            EventLocator::ORDER_COMPLETED => 'onOrderCompleted',
        );
    }

    /**
     * @param OrderEvent $orderEvent
     */
    public function onOrderCompleted(OrderEvent $orderEvent)
    {
        $this->voServiceFacade->save($orderEvent->getOrder());
    }
}
