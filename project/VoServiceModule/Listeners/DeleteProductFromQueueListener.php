<?php

namespace VoServiceModule\Listeners;

use Dispatcher\Events\CompanyDeletedEvent;
use Dispatcher\Events\Order\RefundEvent;
use EventLocator;
use Nette\Object;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use VoServiceModule\Facades\VoServiceFacade;

class DeleteProductFromQueueListener extends Object implements EventSubscriberInterface
{
    /**
     * @var VoServiceFacade
     */
    private $voServiceFacade;

    /**
     * @param VoServiceFacade $voServiceFacade
     */
    public function __construct(VoServiceFacade $voServiceFacade)
    {
        $this->voServiceFacade = $voServiceFacade;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            EventLocator::COMPANY_DELETED => 'onCompanyDeleted',
            EventLocator::ORDER_REFUNDED => 'onOrderRefunded',
        ];
    }

    /**
     * @param CompanyDeletedEvent $event
     */
    public function onCompanyDeleted(CompanyDeletedEvent $event)
    {
        $this->voServiceFacade->deleteByCompany($event->getCompany());
    }

    /**
     * @param RefundEvent $event
     */
    public function onOrderRefunded(RefundEvent $event)
    {
        foreach ($event->getOrderItems() as $orderItem) {
            $this->voServiceFacade->deleteByOrderItem($orderItem);
        }
    }
}
