{extends '@structure/layout.tpl'}

{block content}
    <div class="container bg-white btm30">
        <div class="pad025 top45">
            {ui name="product_introduction" data=$introduction}

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1">
                    <div class="col-xs-12 col-sm-6 col-md-6 btm30">
                        {ui name="package_information_column" data=$packages.classicInternational}
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 btm30">
                        {ui name="package_information_column" data=$packages.bankingInternational}
                    </div>
                </div>
            </div>
        </div>

        {include '@structure/callbackRequest.tpl'}

        <div class="bg-grey4 btm20">
            <div class="col-xs-12">
                <h4 class="text-center top20 btm30">{$review.title}</h4>
            </div>
            <div class="col-xs-12">
                <div class="col-xs-12 col-sm-6 col-md-6 btm20">
                    {ui name="quote" data = $review.quote}
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 flag-wrap btm20">
                    <p class="small pad025 btm10 top10 text-center font-300">{$review.marketingPush}</p>
                    <img src="{$review.marketingPushImage.url}"
                         title="{$review.marketingPushImage.title|default:''}"
                         alt="{$review.marketingPushImage.alt|default:''}"
                         class="img-responsive img-center">
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="bg-white btm20">
            {ui name='proof_of_id' data=$proofOfId}
        </div>
        <div class="bg-blue2 padcard btm20">
            <h3 class="top0">{$renewals.title nofilter}</h3>
            <div class="row">
                <div class="hidden-xs col-sm-1">
                    <i class="fa fa-refresh fa-fw fa-3x blue" aria-hidden="true"></i>
                </div>
                <div class="col-xs-12 col-sm-11">
                    <div>{$renewals.description nofilter}</div>
                </div>
            </div>
        </div>
        <div class="bg-white clear">
            {ui name='faqs' data=$faqs}
        </div>
    </div>
{/block}
