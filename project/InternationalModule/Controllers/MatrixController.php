<?php

namespace InternationalModule\Controllers;

use PackageModule\Emailers\CallbackEmailer;
use PackageModule\Forms\CallbackData;
use PackageModule\Forms\CallbackForm;
use Services\ControllerHelper;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use TemplateModule\Renderers\IRenderer;

class MatrixController
{
    /**
     * @var IRenderer
     */
    private $renderer;

    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    /**
     * @var CallbackEmailer
     */
    private $callbackEmailer;

    /**
     * @param IRenderer $renderer
     * @param ControllerHelper $controllerHelper
     * @param CallbackEmailer $callbackEmailer
     */
    public function __construct(
        IRenderer $renderer,
        ControllerHelper $controllerHelper,
        CallbackEmailer $callbackEmailer
    )
    {
        $this->renderer = $renderer;
        $this->controllerHelper = $controllerHelper;
        $this->callbackEmailer = $callbackEmailer;
    }

    /**
     * @return RedirectResponse|Response
     */
    public function renderMatrix()
    {
        $callbackForm = $this->controllerHelper->buildForm(new CallbackForm(), new CallbackData());

        if ($callbackForm->isSubmitted() && $callbackForm->isValid()) {
            /** @var CallbackData $callbackData */
            $callbackData = $callbackForm->getData();
            $callbackData->setSubject('International Package');
            $this->callbackEmailer->send($callbackData);

            return new RedirectResponse('?sent=1#callback-form-container');
        }

        $additionalVariables = [
            'callbackForm' => $callbackForm->createView(),
            'callbackMailSent' => isset($_GET['sent']),
        ];

        return $this->renderer->render($additionalVariables);
    }
}
