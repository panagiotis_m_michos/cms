<?php

namespace Cron;

use Symfony\Component\DependencyInjection\Container;
use Doctrine\Common\Cache\Cache;
use ILogger;

class CronDi extends Cron
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * @param Cache $cache
     * @param ILogger $logger
     * @param string $cronModuleDir
     * @param INotifier $notifier
     * @param Container $container
     */
    public function __construct(Cache $cache, ILogger $logger,
                                $cronModuleDir, INotifier $notifier, Container $container)
    {
        $this->container  = $container;
        parent::__construct($cache, $logger, $cronModuleDir, $notifier);
    }

    /**
     * {@inheritDoc}
     */
    public function loadOneJob($class, $time)
    {
        $serviceId = $this->convertCamelCaseToUnderscore($class);
        if (!$this->container->has($serviceId)) {
            //check if the key is specified
            $parts = explode('\\', $class);
            $serviceId = end($parts);
        }
        if ($this->container->has($serviceId)) {
            $job = $this->container->get($serviceId);
            $job->setTimeToExecute($time);
            $name = $job->getName();
            if ($this->cache->contains($name)) {
                $lastExecuted = $this->cache->fetch($name);
                $job->setLastExecuted($lastExecuted);
            }
            return $job;
        }
        return parent::loadOneJob($class, $time);
    }

    /**
     * @param $camelCase
     * @return string
     */
    private function convertCamelCaseToUnderscore($camelCase)
    {
        $underscore = preg_replace('/(?<=\w)(?=[A-Z])/', '_$1', $camelCase);
        return str_replace('\\', '.', strtolower($underscore));
    }
}
