<?php

namespace Cron\Commands\Cleaner;

use Cron\Commands\CommandAbstract;
use Cron\Commands\ICommand;
use CHFiling;
use dibi;
use Exception;
use ILogger;
use Utils\Date;

class SubmissionStatusCleaner extends CommandAbstract implements ICommand
{
    /**
     *
     * @var string
     */
    protected $timeType = self::TIME_TYPE_INTERVAL;

    /**
     * @var bool
     */
    private $persist = TRUE;

    public function execute()
    {
        CHFiling::setDocPath(PROJECT_DIR . '/temp/upload/ch_documents');
        $this->cleanSubmissionStatuses();
        $this->cleanIncorporatedDocuments();
        $date = new Date();
        $this->notifier->triggerSuccess(
            $this->getName(),
            $date->getTimestamp(),
            sprintf('Submission cleaner. Date %s', $date->format('d/m/Y'))
        );
    }

    public function cleanIncorporatedDocuments()
    {
        $this->logger->logMessage(ILogger::INFO, 'Cleaning incorporated document request and response');
        $companies = dibi::select('form_submission_id, company_id')->from('ch_form_submission')
            ->where('form_identifier=%s', 'CompanyIncorporation')
            ->and('response')
            ->in(array('ACCEPT'))
            ->execute()
            ->getIterator();
        $envelopeCount = $companiesCount = 0;
        $envelopeIds = $formSubmissionIds = array();
        foreach ($companies as $company) {
            $envelopeId = dibi::select('e.envelope_id')->from('ch_envelope e')
                ->where('e.class=%s', 'GetDocument')
                ->and('e.form_submission_id=%i', $company->form_submission_id)
                ->fetchSingle();
            if ($envelopeId) {
                $envelopeIds[] = $envelopeId;
                $this->deleteEnvelope($envelopeId);
                $envelopeCount++;
            }
            $formSubmissionIds[] = $company->form_submission_id;
            $this->deleteSuppNameAuth($company->company_id, $company->form_submission_id);
            //delete memorandum and articles
            $this->deleteArticle($company->company_id, $company->form_submission_id);

            foreach ($this->getIncorporationSubmissions($company->company_id, 'REJECT') as $submission) {
                $formSubmissionIds[] = $submission->form_submission_id;
                $this->deleteArticle($submission->company_id, $submission->form_submission_id);
            }

            $companiesCount++;

            if ($envelopeCount % 1000 == 0 && $envelopeIds) {
                $this->logger->logMessage(ILogger::INFO, 'Last envelope id `' . $envelopeId . '`');
                if ($this->persist) {
                    dibi::delete('ch_envelope')->where('envelope_id')
                        ->in($envelopeIds)
                        ->execute();
                }
                $envelopeIds = array();
            }
            if ($companiesCount % 1000 == 0 && $formSubmissionIds) {
                $this->logger->logMessage(ILogger::INFO, 'Last form submission id `' . $company->form_submission_id . '`');
                if ($this->persist) {
                    dibi::delete('ch_document')
                        ->where('category')
                        ->in(array('MEMARTS', 'SUPPNAMEAUTH'))
                        ->and('form_submission_id')
                        ->in($formSubmissionIds)
                        ->execute();
                }
                $formSubmissionIds = array();
            }
        }
        if ($envelopeIds) {
            if ($this->persist) {
                dibi::delete('ch_envelope')->where('envelope_id')
                    ->in($envelopeIds)
                    ->execute();
            }
        }
        if ($formSubmissionIds) {
            if ($this->persist) {
                dibi::delete('ch_document')
                    ->where('category')
                    ->in(array('MEMARTS', 'SUPPNAMEAUTH'))
                    ->and('form_submission_id')
                    ->in($formSubmissionIds)
                    ->execute();
            }
        }
        $this->logger->logMessage(ILogger::INFO, 'Initial records with ACCEPT status `' . $companiesCount . '`');
        $this->logger->logMessage(ILogger::INFO, 'Envelopes deleted `' . $envelopeCount . '`');
    }

    public function cleanSubmissionStatuses()
    {
        $this->logger->logMessage(ILogger::INFO, 'Cleaning submission statuses');

        $companies = dibi::select('form_submission_id')->from('ch_form_submission')
            //->where('form_identifier=%s', 'CompanyIncorporation')
            ->where('response')
            ->in(array('ERROR', 'ACCEPT', 'REJECT'))
            ->execute()
            ->getIterator();
        $envelopeCount = $companiesCount = 0;
        $date = date('Y-m-d', strtotime('-1 month'));
        $envelopeIds = array();
        foreach ($companies as $company) {
            $envelopes = dibi::select('e.envelope_id')->from('ch_envelope e')
                ->where('e.class=%s', 'GetSubmissionStatus')
                ->and('e.form_submission_id=%i', $company->form_submission_id)
                ->and('e.date_sent < %d', $date)
                ->orderBy('e.date_sent DESC')
                ->execute()
                ->getIterator();
            $i = 0;
            foreach ($envelopes as $envelope) {
                // ignore the first since its going to be accepted, rejected or error
                if ($i == 0) {
                    $i++;
                    continue;
                }
                $envelopeCount++;
                $envelopeIds[] = $envelope->envelope_id;
                $this->deleteEnvelope($envelope->envelope_id);
                $i++;
                if ($envelopeCount % 1000 == 0 && $envelopeIds) {
                    $this->logger->logMessage(ILogger::INFO, 'Last envelope id `' . end($envelopeIds) . '`');
                    if ($this->persist) {
                        dibi::delete('ch_envelope')->where('envelope_id')
                            ->in($envelopeIds)
                            ->execute();
                    }
                    $envelopeIds = array();
                }
            }

            $companiesCount ++;
        }
        if ($envelopeIds) {
            if ($this->persist) {
                dibi::delete('ch_envelope')->where('envelope_id')
                    ->in($envelopeIds)
                    ->execute();
            }
        }
        $this->logger->logMessage(ILogger::INFO, 'Initial records with ERROR, ACCEPT, REJECT statuses `' . $companiesCount . '`');
        $this->logger->logMessage(ILogger::INFO, 'Envelopes deleted `' . $envelopeCount . '` with date less than `' . $date . '`');
    }

    /**
     * @param $companyId
     * @param $formSubmissionId
     * @throws Exception
     */
    private function deleteSuppNameAuth($companyId, $formSubmissionId)
    {
        $filePath = CHFiling::getDocPath() . '/' . CHFiling::getDirName($companyId) . "/company-$companyId" . "/formSubmission-$formSubmissionId" . "/suppnameauth.pdf";
        if (!$this->persist) {
            $this->logger->logMessage(ILogger::INFO, 'Deleting suppnameauth `' . $filePath . '`');
            return;
        }
        if (file_exists($filePath)) {
            unlink($filePath);
        }
    }

    /**
     * @param int $companyId
     * @param string $formSubmissionId
     * @throws Exception
     */
    private function deleteArticle($companyId, $formSubmissionId)
    {
        $directory = CHFiling::getDocPath() . '/' . CHFiling::getDirName($companyId) . "/company-$companyId" . "/formSubmission-$formSubmissionId";
        $filePath =  $directory . "/article.pdf";
        if (!$this->persist) {
            $this->logger->logMessage(ILogger::INFO, 'Deleting article.pdf `' . $filePath . '`');
            return;
        }
        if (file_exists($filePath)) {
            unlink($filePath);
        }
        if ($this->isDirEmpty($directory) === TRUE) {
            rmdir($directory);
        }
    }

    /**
     * @param int $envelopeId
     * @throws Exception
     */
    private function deleteEnvelope($envelopeId)
    {
        $requestXmlPath = CHFiling::getDocPath() . '/_submission/' . CHFiling::getDirName($envelopeId) . '/envelope-' . $envelopeId . '/request.xml';
        $responseXmlPath = CHFiling::getDocPath() . '/_submission/' . CHFiling::getDirName($envelopeId) . '/envelope-' . $envelopeId . '/response.xml';
        $directory = CHFiling::getDocPath() . '/_submission/' . CHFiling::getDirName($envelopeId) . '/envelope-' . $envelopeId;
        if (!$this->persist) {
            $this->logger->logMessage(ILogger::INFO, 'Deleting envelope `' . $envelopeId . '` directory `' . $directory . '`');
            return;
        }
        if (file_exists($requestXmlPath)) {
            unlink($requestXmlPath);
        }
        if (file_exists($responseXmlPath)) {
            unlink($responseXmlPath);
        }
        if (file_exists($directory)) {
            rmdir($directory);
        }
    }

    /**
     * @param int $companyId
     * @param string $status
     * @return array
     */
    private function getIncorporationSubmissions($companyId, $status)
    {
        return dibi::select('form_submission_id, company_id')->from('ch_form_submission')
            ->where('form_identifier=%s', 'CompanyIncorporation')
            ->and('company_id=%s', $companyId)
            ->and('response')
            ->in(array($status))
            ->fetchAll();
    }

    /**
     * @param string $dir
     * @return bool
     */
    private function isDirEmpty($dir)
    {
        if (!is_readable($dir)) return NULL;
        $handle = opendir($dir);
        while (FALSE !== ($entry = readdir($handle))) {
            if ($entry !== '.' && $entry !== '..') { // <-- better use strict comparison here
                return FALSE;
            }
        }
        closedir($handle); // <-- always clean up! Close the directory stream
        return TRUE;
    }

}
