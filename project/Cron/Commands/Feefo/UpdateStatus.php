<?php

namespace Cron\Commands\Feefo;

use Cron\Commands\CommandAbstract;
use Cron\Commands\ICommand;
use Cron\INotifier;
use Doctrine\ORM\EntityNotFoundException;
use Entities\Feefo;
use Exception;
use ILogger;
use Services\CompanyService;
use Services\FeefoService;
use Utils\Date;

class UpdateStatus extends CommandAbstract implements ICommand
{
    /**
     * @var string
     */
    protected $timeType = self::TIME_TYPE_FIXED;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var FeefoService
     */
    private $feefoService;

    /**
     * @param ILogger $logger
     * @param INotifier $notifier
     * @param CompanyService $companyService
     * @param FeefoService $feefoService
     */
    public function __construct(ILogger $logger, INotifier $notifier, CompanyService $companyService, FeefoService $feefoService)
    {
        parent::__construct($logger, $notifier);

        $this->companyService = $companyService;
        $this->feefoService = $feefoService;
    }

    public function execute()
    {
        $feefos = $this->feefoService->getWaitingOrders();
        $count = $errorCount = 0;
        /* @var $feefo Feefo */
        foreach ($feefos as $row) {
            try {
                $feefo = $row[0];
                $order = $feefo->getOrder();
                $company = $this->companyService->getCompanyByOrder($order);
                if ($company) {
                    $lastSubmission = $this->companyService->getLastCompanyIncorporationSubmission($company);

                    if ($company->isIncorporated() || ($lastSubmission && $lastSubmission->isAccepted())) {
                        $feefo->markAsEligible();
                        $this->feefoService->saveFeefo($feefo);
                        $count++;
                        $logMessage = "Changing status to " . Feefo::STATUS_ELIGIBLE;
                    } elseif ($lastSubmission) {
                        $status = $lastSubmission->getResponse() ?: 'INCOMPLETE';
                        $logMessage = "Company is not incorporated yet (last status is {$status})";
                    } else {
                        $logMessage = "Company doesn't have any CompanyIncorporation form submission";
                    }
                } else {
                    $logMessage = "No company found for order {$order->getOrderId()}";
                }
                $this->logDebug($feefo, $logMessage);
            } catch (EntityNotFoundException $e) {
                $this->logger->logMessage(ILogger::ERROR, "Feefo {$feefo->getId()} - Message: {$e->getMessage()}");
                $errorCount++;
            }
        }
        $date = new Date();
        $this->notifier->triggerSuccess(
            $this->getName(),
            $date->getTimestamp(),
            sprintf('Feefo update status processed %d. Errors %d. Date %s', $count, $errorCount, $date->format('d/m/Y'))
        );
    }

    /**
     * @param Feefo $feefo
     * @param string $message
     */
    private function logDebug(Feefo $feefo, $message)
    {
        $this->logger->logMessage(ILogger::DEBUG, sprintf("Feefo %s: %s", $feefo->getId(), $message));
    }
}
