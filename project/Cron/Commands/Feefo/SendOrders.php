<?php

namespace Cron\Commands\Feefo;

use Cron\Commands\CommandAbstract;
use Cron\Commands\ICommand;
use Cron\INotifier;
use DateTime;
use Doctrine\ORM\EntityNotFoundException;
use Entities\Feefo;
use Exception;
use Exceptions\Business\CurlException;
use Feefo\FeefoManager;
use ILogger;
use Services\FeefoService;

class SendOrders extends CommandAbstract implements ICommand
{
    /**
     * @var string
     */
    protected $timeType = self::TIME_TYPE_INTERVAL;

    /**
     * @var FeefoService
     */
    private $feefoService;

    /**
     * @var FeefoManager
     */
    private $feefoManager;

    /**
     * @var int[]
     */
    private $ignoredCurlErrors;

    /**
     * @param ILogger $logger
     * @param INotifier $notifier
     * @param FeefoService $feefoService
     * @param FeefoManager $feefoManager
     * @param array $ignoredCurlErrors
     */
    public function __construct(
        ILogger $logger,
        INotifier $notifier,
        FeefoService $feefoService,
        FeefoManager $feefoManager,
        array $ignoredCurlErrors = []
    )
    {
        parent::__construct($logger, $notifier);

        $this->feefoService = $feefoService;
        $this->feefoManager = $feefoManager;
        $this->ignoredCurlErrors = $ignoredCurlErrors;
    }

    public function execute()
    {
        $items = $this->feefoService->getEligibleOrders();
        $count = $errorCount = 0;
        /* @var $feefo Feefo */
        foreach ($items as $row) {
            try {
                $feefo = $row[0];
                if ($this->feefoManager->orderNotify($feefo)) {
                    $count++;
                    $this->logDebug($feefo, "Sending order");
                } else {
                    $this->logDebug($feefo, "Couldn't send order because of missing data");
                    $errorCount++;
                }
            } catch (CurlException $e) {
                if (in_array($e->getCode(), $this->ignoredCurlErrors)) {
                    $this->logger->logMessage(ILogger::WARNING, "Feefo {$feefo->getId()} - {$e->getMessage()}");
                } else {
                    throw $e;
                }
            } catch (EntityNotFoundException $e) {
                $this->logger->logMessage(ILogger::ERROR, "Feefo {$feefo->getId()} - {$e->getMessage()}");
                $errorCount++;
            }
        }
        $date = new DateTime();
        $this->notifier->triggerSuccess(
            $this->getName(),
            $date->getTimestamp(),
            sprintf('Feefo orders sent %d. Errors %d. DateTime %s', $count, $errorCount, $date->format('d/m/Y H:i:s'))
        );
    }

    /**
     * @param Feefo $feefo
     * @param string $message
     */
    private function logDebug(Feefo $feefo, $message)
    {
        $this->logger->logMessage(ILogger::DEBUG, sprintf("Feefo %s: %s", $feefo->getId(), $message));
    }
}
