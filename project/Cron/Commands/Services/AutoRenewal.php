<?php

namespace Cron\Commands\Services;

use Basket;
use BasketProduct;
use Company\Service\RenewalData;
use Company\Service\RenewalDataIterator;
use Cron\Commands\CommandAbstract;
use Cron\INotifier;
use Dispatcher\Events\PaymentEvent;
use Emailers\ServicesEmailer;
use Entities\Company;
use Entities\Customer;
use Entities\Order;
use Entities\OrderItem;
use Entities\Payment\Token;
use Entities\Service;
use EventLocator;
use Exception;
use Exceptions\Technical\AutoRenewalException;
use ILogger;
use Product;
use SagePay\Reporting\SagePay;
use SagePay\Token\Exception\FailedResponse;
use Services\EventService;
use Services\Payment\SageService;
use Services\Payment\TokenService;
use Services\ProductService;
use Services\ServiceService;
use ServiceSettingsModule\Services\ServiceSettingsService;
use Services\TransactionService;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Utils\Date;

class AutoRenewal extends CommandAbstract
{
    const OPTION_FAILED_PAYMENT = 'failedPayment';

    /**
     * @var string
     */
    protected $timeType = self::TIME_TYPE_FIXED;

    /**
     * @var ServiceService
     */
    private $serviceService;

    /**
     * @var ProductService
     */
    private $productService;

    /**
     * @var TransactionService
     */
    private $transactionService;

    /**
     * @var EventService
     */
    private $eventService;

    /**
     * @var SageService
     */
    private $sageService;

    /**
     * @var ServiceSettingsService
     */
    private $serviceSettingsService;

    /**
     * @var TokenService
     */
    private $tokenService;

    /**
     * @var SagePay
     */
    private $sagePay;

    /**
     * @var ServicesEmailer
     */
    private $servicesEmailer;

    /**
     * @var EventDispatcher
     */
    private $dispatcher;

    /**
     * @param ILogger $logger
     * @param INotifier $notifier
     * @param ProductService $productService
     * @param ServiceService $serviceService
     * @param TransactionService $transactionService
     * @param EventService $eventService
     * @param SageService $sageService
     * @param ServiceSettingsService $serviceSettingsService
     * @param TokenService $tokenService
     * @param SagePay $sagePay
     * @param ServicesEmailer $servicesEmailer
     * @param EventDispatcher $dispatcher
     */
    public function __construct(
        ILogger $logger,
        INotifier $notifier,
        ProductService $productService,
        ServiceService $serviceService,
        TransactionService $transactionService,
        EventService $eventService,
        SageService $sageService,
        ServiceSettingsService $serviceSettingsService,
        TokenService $tokenService,
        SagePay $sagePay,
        ServicesEmailer $servicesEmailer,
        EventDispatcher $dispatcher
    )
    {
        parent::__construct($logger, $notifier);
        $this->productService = $productService;
        $this->serviceService = $serviceService;
        $this->transactionService = $transactionService;
        $this->eventService = $eventService;
        $this->sageService = $sageService;
        $this->serviceSettingsService = $serviceSettingsService;
        $this->tokenService = $tokenService;
        $this->sagePay = $sagePay;
        $this->servicesEmailer = $servicesEmailer;
        $this->dispatcher = $dispatcher;
    }

    public function execute()
    {
        $servicesIterator = $this->serviceService->getServicesDataExpiringWithinDatesForEvent(
            new Date('-28 days'),
            new Date,
            EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_SUCCEDED
        );
        $customerServiceIterator = new RenewalDataIterator($servicesIterator);
        $customersProcessed = 0;

        foreach ($customerServiceIterator as $customerServicesData) {
            try {
                $this->processAutoRenewalsForCustomer($customerServicesData);
                $customersProcessed++;
            } catch (AutoRenewalException $e) {
                $this->logger->logMessage(ILogger::ERROR, $e->getMessage());
            }
        }

        if (!$customersProcessed) {
            $this->logger->logMessage(ILogger::NOTICE, 'No Companies with services to renew found');
        }
    }

    /**
     * @param RenewalData $customerRenewalData
     * @throws Exception
     */
    private function processAutoRenewalsForCustomer(RenewalData $customerRenewalData)
    {
        /** @var BasketProduct[] $newServices */
        $newServices = [];
        $customer = $customerRenewalData->getCustomer();
        if (!$customer) {
            throw AutoRenewalException::customersDataMissing();
        }

        $customerToken = $customer->getActiveToken();
        if (!$customerToken) {
            throw AutoRenewalException::noActiveTokenForCustomer($customer);
        }

        try {
            $order = $this->createOrder($customer);

            $basket = new Basket('autoRenewal');
            $basket->clear();

            foreach ($customerRenewalData->getCompanies() as $companyId => $companyData) {
                /** @var Company $company */
                $company = $companyData['entity'];

                /** @var Service $service */
                foreach ($companyData['services'] as $service) {
                    $serviceSettings = $this->serviceSettingsService->getSettingsByService($service);

                    $renewalToken = $serviceSettings->getRenewalToken();
                    if (!$renewalToken->isEqual($customerToken)) {
                        throw AutoRenewalException::differentTokens($customer, $customerToken, $service, $renewalToken);
                    }

                    $renewalProduct = $service->getRenewalProduct();
                    $orderItem = $this->createOrderItem($order, $company, $renewalProduct);
                    $order->addItem($orderItem);
                    $basket->add($renewalProduct);

                    $renewalProduct->setOrderItem($orderItem);
                    $newServices[] = $renewalProduct;
                }
            }

            $price = $basket->getPrice();

            $order->setSubTotal($price->subTotal2);
            $order->setRealSubtotal($price->subTotal);
            $order->setDiscount($price->discount);
            $order->setVat($price->vat);
            $order->setTotal($price->total);

            $detailsRequest = SageService::createRecurringRequest($customerToken, $order->getTotal());

            try {
                if ($this->hasOption(self::OPTION_FAILED_PAYMENT)) {
                    $this->logMessage("Simulating failed payment for customer {$customer->getCustomerId()}");
                    throw new FailedResponse('Simulating failed payment');
                }
                $paymentResponse = $this->sageService->processRecurringPayment($detailsRequest, $customerToken);
                $this->logMessage("Auto renewal payment for customer {$customer->getCustomerId()} succeded.");
                $this->productService->saveServices($newServices, $customerToken);
                $this->logMessage("Services for customer {$customer->getCustomerId()} created.");
            } catch (FailedResponse $e) {
                // payment wasn't successful, disable auto renewals and send "failed" email to customer
                $this->notifyServices($customerRenewalData->getCustomerServices(), EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_FAILED);
                $this->deleteCustomerToken($customerToken);
                $this->logMessage("Sending 'renewal failed' email to customer {$customer->getCustomerId()}");
                $this->servicesEmailer->sendEmailServiceAutoRenewalFailed($customerRenewalData);
                $this->transactionService->saveFailedSageTransaction($customer, $e->getMessage());

                throw $e;
            }

            $this->transactionService->saveTransactionFromResponse($customer, $paymentResponse, $order);
            $this->dispatcher->dispatch(EventLocator::PAYMENT_RECURRING_COMPLETED, new PaymentEvent($customer, $basket));
            $this->notifyServices($customerRenewalData->getCustomerServices(), EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_SUCCEDED);
            $this->logMessage("Sending 'success' email to customer {$customer->getCustomerId()}");
            $this->servicesEmailer->sendEmailServiceAutoRenewalSucceeded($customerRenewalData);
        } catch (FailedResponse $e) {
            $level = ($e->getCode() == FailedResponse::AUTHORISATION_DECLINED_BY_BANK) ? ILogger::WARNING : ILogger::ERROR;
            $this->logMessage(
                "Auto renewal for customer {$customer->getCustomerId()} failed with error: {$e->getMessage()}",
                $level
            );
        } catch (Exception $e) {
            // for any other error, send error email and continue
            $this->logMessage(
                "Auto renewal for customer {$customer->getCustomerId()} failed with error: {$e->getMessage()}",
                ILogger::ERROR
            );
        }
    }

    /**
     * @param Token $token
     */
    private function deleteCustomerToken(Token $token)
    {
        $this->logMessage("Deleting token for customer {$token->getCustomer()->getId()}");

        $this->tokenService->removeToken($token, 'Auto Renewal');
        $tokenIdentifier = $token->getIdentifier();
        if ($this->sagePay->isTokenActive($tokenIdentifier)) {
            $this->sageService->deleteToken($tokenIdentifier);
        }
    }

    /**
     * @param Service[] $services
     * @param string $eventKey
     */
    private function notifyServices(array $services, $eventKey)
    {
        foreach ($services as $service) {
            $this->eventService->notify($eventKey, $service->getServiceId());
        }
    }

    /**
     * @param Customer $customer
     * @return Order
     */
    private function createOrder(Customer $customer)
    {
        $order = new Order($customer);
        $order->setDescription('Auto renewed services');
        $order->setPaymentMediumId(Order::PAYMENT_MEDIUM_AUTO_RENEWAL);

        return $order;
    }

    /**
     * @param Order $order
     * @param Company $company
     * @param Product $product
     * @return OrderItem
     */
    private function createOrderItem(Order $order, Company $company, Product $product)
    {
        $orderItem = new OrderItem($order);
        $orderItem->setCompany($company);
        $orderItem->setProductId($product->getId());
        $orderItem->setProductTitle("{$product->getLongTitle()} for company {$company->getCompanyName()}");
        $orderItem->setPrice($product->getPrice());
        $orderItem->setQty(1);
        $orderItem->setSubTotal($product->getPrice());
        $orderItem->setVat($product->getVat());
        $orderItem->setTotalPrice($product->getTotalVatPrice());
        $orderItem->setIsFee(FALSE);
        $orderItem->setNotApplyVat(FALSE);
        $orderItem->setNonVatableValue(0);

        return $orderItem;
    }

    /**
     * @param string $message
     * @param int $level
     */
    private function logMessage($message, $level = ILogger::DEBUG)
    {
        $this->logger->logMessage($level, $message);
    }
}
