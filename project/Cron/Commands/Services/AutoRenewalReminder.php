<?php

namespace Cron\Commands\Services;

use Cron\Commands\CommandAbstract;
use Exception;
use Exceptions\Technical\AutoRenewalReminderException;
use Company\Service\RenewalData;
use Company\Service\RenewalDataIterator;
use EventLocator;
use ILogger;
use Cron\INotifier;
use Services\EventService;
use Emailers\ServicesEmailer;
use Services\ServiceService;
use Utils\Date;

class AutoRenewalReminder extends CommandAbstract
{

    /**
     * @var string
     */
    protected $timeType = self::TIME_TYPE_FIXED;

    /**
     * @var EventService
     */
    private $eventService;

    /**
     * @var ServicesEmailer
     */
    private $servicesEmailer;

    /**
     * @var ServiceService
     */
    private $serviceService;

    /**
     * @param ILogger $logger
     * @param INotifier $notifier
     * @param ServiceService $serviceService
     * @param EventService $eventService
     * @param ServicesEmailer $servicesEmailer
     */
    public function __construct(
        ILogger $logger,
        INotifier $notifier,
        ServiceService $serviceService,
        EventService $eventService,
        ServicesEmailer $servicesEmailer
    )
    {
        parent::__construct($logger, $notifier);

        $this->serviceService = $serviceService;
        $this->eventService = $eventService;
        $this->servicesEmailer = $servicesEmailer;
    }

    public function execute()
    {
        $servicesIterableResult = $this->serviceService->getServicesDataExpiringWithinDatesForEvent(
            new Date('+1 days'),
            new Date('+7 days'),
            EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_EXPIRES_IN_7
        );

        $customerServiceIterator = new RenewalDataIterator($servicesIterableResult);
        $customersProcessed = 0;

        foreach ($customerServiceIterator as $customerServicesData) {
            try {
                $this->logger->logMessage(
                    ILogger::INFO,
                    "Processing reminders for customer {$customerServicesData->getCustomer()->getCustomerId()}"
                );
                $this->processRemindersForCustomer($customerServicesData);
                $customersProcessed++;
            } catch (Exception $e) {
                $this->logger->logMessage(
                    ILogger::ERROR,
                    sprintf(
                        "Reminders for customer %d coudln't be sent with error: %s",
                        $customerServicesData->getCustomer()->getCustomerId(),
                        $e->getMessage()
                    )
                );

                throw AutoRenewalReminderException::common($e);
            }
        }

        if (!$customersProcessed) {
            $this->logger->logMessage(ILogger::NOTICE, 'No Companies with expiring auto renewal services');
        }

        $date = new Date();
        $this->notifier->triggerSuccess(
            $this->getName(),
            $date->getTimestamp(),
            sprintf('Auto renewal service reminders processed %d. Date %s', $customersProcessed, $date->format('d/m/Y'))
        );
    }

    /**
     * @param RenewalData $customerData
     */
    private function processRemindersForCustomer(RenewalData $customerData)
    {
        $this->servicesEmailer->sendEmailServiceAutoRenewalIn7($customerData);

        foreach ($customerData->getCustomerServices() as $service) {
            $this->eventService->notify(
                EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_EXPIRES_IN_7,
                $service->getServiceId()
            );
        }
    }
}
