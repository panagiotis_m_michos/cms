<?php

namespace Cron\Commands\Services;

use Cron\Commands\CommandAbstract;
use Cron\INotifier;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NoResultException;
use Entities\ServiceSettings;
use ILogger;
use Services\ServiceService;
use ServiceSettingsModule\Services\ServiceSettingsService;

class DisableAutoRenewal extends CommandAbstract
{

    /**
     * @var string
     */
    protected $timeType = self::TIME_TYPE_FIXED;

    /**
     * @var ServiceService
     */
    private $serviceService;

    /**
     * @var ServiceSettingsService
     */
    private $serviceSettingsService;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @param ILogger $logger
     * @param INotifier $notifier
     * @param ServiceService $serviceService
     * @param ServiceSettingsService $serviceSettingsService
     * @param EntityManager $em
     */
    public function __construct(
        ILogger $logger,
        INotifier $notifier,
        ServiceService $serviceService,
        ServiceSettingsService $serviceSettingsService,
        EntityManager $em
    )
    {
        parent::__construct($logger, $notifier);

        $this->serviceService = $serviceService;
        $this->serviceSettingsService = $serviceSettingsService;
        $this->em = $em;
    }

    public function execute()
    {
        if ($this->isDryRun()) {
            $this->logger->logMessage(ILogger::DEBUG, "Running in dry-run mode");
        }

        $batchSize = 50;
        $batchCount = 0;
        $servicesSettings = $this->serviceSettingsService->getSettingsWithEnabledAutoRenewal();

        foreach ($servicesSettings as $serviceSettingsData) {
            /** @var ServiceSettings $serviceSettings */
            $serviceSettings = $serviceSettingsData[0];

            try {
                $service = $this->serviceService->getLatestServiceBySettings($serviceSettings);
            } catch (NoResultException $e) {
                $this->logger->logMessage(
                    ILogger::INFO,
                    "No active service found for settings {$serviceSettings->getServiceTypeId()}, disabling auto-renewal"
                );
                $serviceSettings->setIsAutoRenewalEnabled(FALSE);
                $serviceSettings->setRenewalToken(NULL);
                $batchCount++;
                continue;
            }

            if ($service->hasDates() && $service->getDaysAfterExpire() > 28) {
                $this->logger->logMessage(
                    ILogger::INFO,
                    "Disabling auto renewal for overdue service {$serviceSettings->getServiceTypeId()} (company {$service->getCompany()->getId()})"
                );
                $serviceSettings->setIsAutoRenewalEnabled(FALSE);
                $serviceSettings->setRenewalToken(NULL);
                $batchCount++;
            }

            if ($batchCount && $batchCount % $batchSize === 0) {
                $this->flushUpdates();
            }
        }

        $this->flushUpdates();
    }

    private function flushUpdates()
    {
        if (!$this->isDryRun()) {
            $this->em->flush();
        }
        $this->em->clear();
        $this->logger->logMessage(ILogger::DEBUG, 'Updates have been executed');
    }
}
