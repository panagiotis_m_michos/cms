<?php

namespace Cron\Commands\Reminders;

use Cron\Commands\CommandAbstract;
use Cron\INotifier;
use Doctrine\ORM\Internal\Hydration\IterableResult;
use Exception;
use Utils\Date;
use Doctrine\ORM\EntityManager;
use Services\EventService;
use Services\Payment\TokenService;
use Entities\Payment\Token;
use ILogger;
use Dispatcher\Events\TokenEvent;
use EventLocator;

class Tokens extends CommandAbstract
{
    /**
     * @var string
     */
    protected $timeType = self::TIME_TYPE_FIXED;

    /**
     * @var EventService
     */
    protected $eventService;

    /**
     * @var TokenService
     */
    protected $tokenService;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @param ILogger $logger
     * @param INotifier $notifier
     * @param EventService $eventService
     * @param TokenService $tokenService
     * @param EntityManager $entityManager
     */
    public function __construct(
        ILogger $logger,
        INotifier $notifier,
        EventService $eventService,
        TokenService $tokenService,
        EntityManager $entityManager)
    {
        parent::__construct($logger, $notifier);
        $this->eventService = $eventService;
        $this->tokenService = $tokenService;
        $this->entityManager = $entityManager;
    }

    public function execute()
    {
        $now = new Date();
        $fromDate = new Date('-2 days');
        $toDate = new Date('+7 days');
        $rows = $this->tokenService->getTokensForCardReminders($fromDate, $toDate);

        $i = $this->processTokenReminders($rows);
        $this->logger->logMessage(ILogger::INFO, sprintf('Token reminders processed `%d`. Date: `%s`', $i, $now->format('d/m/Y')));
        $this->notifier->triggerSuccess(
            $this->getName(),
            $now->getTimestamp(),
            sprintf('Token reminders processed `%d`. Date: `%s`', $i, $now->format('d/m/Y'))
        );
    }

    /**
     * @TODO find a way how to mock iterator
     * @param IterableResult $rows
     * @return int
     */
    public function processTokenReminders($rows)
    {
        $i = 0;
        $batchSize = 50;

        /* @var $token Token */
        foreach ($rows as $row) {
            $token = $row[0];
            // tokenId is saved because when it's deleted and then exception is thrown, getId() returns 0
            $tokenId = $token->getTokenId();
            try {
                $customer = $token->getCustomer();
                foreach ($customer->getCompanies() as $company) {
                    if ($company->hasActiveService()) {
                        $this->processExpiryDate($token);
                        break;
                    }
                }
            } catch (Exception $e) {
                $this->logger->logMessage(
                    ILogger::ERROR,
                    sprintf("Token `%d` reminder error:  %s", $tokenId, $e->getMessage())
                );
            }
            $i++;
            if ($i % $batchSize == 0) {
                $this->entityManager->clear();
            }
        }
        return $i;
    }

    /**
     * @param Token $token
     */
    public function processExpiryDate(Token $token)
    {
        $tokenExpiryDate = $token->getCardExpiryDate();
        if (!empty($tokenExpiryDate)) {
            if ($token->isCardExpiringIn7Days()) {
                $this->eventService->dispatch(
                    EventLocator::TOKEN_EMAIL_SOON_EXPIRES,
                    $token->getId(),
                    new TokenEvent($token, $this->logger)
                );
            } elseif ($token->isCardExpired()) {
                $this->eventService->dispatch(
                    EventLocator::TOKEN_EMAIL_EXPIRED,
                    $token->getId(),
                    new TokenEvent($token, $this->logger)
                );
            }
        }
    }

}
