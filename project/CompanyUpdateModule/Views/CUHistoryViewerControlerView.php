<?php

namespace CompanyUpdateModule\Views;

use Entities\CompanyHouse\FormSubmission;
use FormSubmissionModule\Views\FormSubmissionView;

class CUHistoryViewerControlerView
{
    /**
     * @var FormSubmissionView
     */
    private $formSubmissionView;

    /**
     * @param FormSubmissionView $formSubmissionView
     */
    public function __construct(FormSubmissionView $formSubmissionView)
    {
        $this->formSubmissionView = $formSubmissionView;
    }

    /**
     * @return FormSubmissionView
     */
    public function getFormSubmission()
    {
        return $this->formSubmissionView;
    }
}
