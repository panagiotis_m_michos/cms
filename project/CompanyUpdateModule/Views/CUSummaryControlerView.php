<?php

namespace CompanyUpdateModule\Views;

use CUHistoryViewerControler;
use Entities\CompanyHouse\FormSubmission;
use FormSubmissionModule\Views\FormSubmissionsView;
use FormSubmissionModule\Views\FormSubmissionView;
use FRouter;

class CUSummaryControlerView
{
    /**
     * @var FRouter
     */
    private $router;

    /**
     * @var FormSubmissionsView
     */
    private $formSubmissionsView;

    /**
     * @param FRouter $router
     * @param FormSubmissionsView $formSubmissionsView
     */
    public function __construct(FRouter $router, FormSubmissionsView $formSubmissionsView)
    {
        $this->router = $router;
        $this->formSubmissionsView = $formSubmissionsView;
    }

    /**
     * @return bool
     */
    public function hasFormSubmissions()
    {
        return (bool) $this->getFormSubmissions();
    }

    /**
     * @return FormSubmissionView[]
     */
    public function getFormSubmissions()
    {
        return $this->formSubmissionsView->getFormSubmissionViews();
    }

    /**
     * @param FormSubmissionView $formSubmissionView
     * @return bool
     */
    public function canViewMinutes(FormSubmissionView $formSubmissionView)
    {
        return $formSubmissionView->isViewable();
    }

    /**
     * @param int $companyId
     * @param FormSubmissionView $formSubmissionView
     * @return string
     */
    public function getViewLink($companyId, FormSubmissionView $formSubmissionView)
    {
        $params = ['company_id' => $companyId, 'submission_id' => $formSubmissionView->getId()];

        if ($formSubmissionView->isCompanyIncorporationType()) {
            $link = $this->router->link(CUHistoryViewerControler::PAGE_INCORPORATION, $params);
        } elseif ($formSubmissionView->isAnnualReturnType()) {
            $link = $this->router->link(CUHistoryViewerControler::PAGE_ANNUAL_RETURN, $params);
        } else {
            $link = $this->router->link(CUHistoryViewerControler::PAGE_OTHER, $params);
        }

        return $link;
    }
}
