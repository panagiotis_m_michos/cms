<?php

namespace CompanyUpdateModule\Factories;

use CompanyUpdateModule\Views\CUSummaryControlerView;
use Entities\Company;
use FormSubmissionModule\Factories\FormSubmissionsViewFactory;
use FRouter;

class CUSummaryControlerViewFactory
{
    /**
     * @var FRouter
     */
    private $router;

    /**
     * @var FormSubmissionsViewFactory
     */
    private $formSubmissionsViewFactory;

    /**
     * @param FRouter $router
     * @param FormSubmissionsViewFactory $formSubmissionsViewFactory
     */
    public function __construct(FRouter $router, FormSubmissionsViewFactory $formSubmissionsViewFactory)
    {
        $this->router = $router;
        $this->formSubmissionsViewFactory = $formSubmissionsViewFactory;
    }

    /**
     * @param Company $company
     * @return CUSummaryControlerView
     */
    public function create(Company $company)
    {
        $formSubmissionsView = $this->formSubmissionsViewFactory->createFromCompanyWithResponse($company);

        return new CUSummaryControlerView($this->router, $formSubmissionsView);
    }
}
