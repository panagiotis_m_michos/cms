<?php

use Guzzle\Common\Event;
use Guzzle\Http\Message\Request;
use Guzzle\Http\Message\RequestInterface;
use Guzzle\Http\Message\Response;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class RedirectLoggingPlugin implements EventSubscriberInterface
{
    const DISABLE = 'redirect.disable';
    static $outputStarted = FALSE;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public static function getSubscribedEvents()
    {
        return [
            'request.sent' => ['onRequestSent', 200],
        ];
    }

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }


    /**
     * Called when a request receives a redirect response
     *
     * @param Event $event Event emitted
     */
    public function onRequestSent(Event $event)
    {
        /** @var Response $response */
        $response = $event['response'];
        /** @var Request $request */
        $request = $event['request'];

        if (!$response || $request->getParams()->get(self::DISABLE)) {
            return;
        }

        if (!$response->isRedirect() || !$response->hasHeader('Location')) {
            return;
        }

        if ($response->hasHeader('Location')) {
            $redirection = $response->getHeader('Location');
        } else {
            $redirection = $response->getEffectiveUrl();
        }

        if (!self::$outputStarted) {
            $this->logger->info("---------------------- Guzzle redirection detected! ----------------------");
            self::$outputStarted = TRUE;
        }

        $this->logger->info("Guzzle redirected from {$request->getUrl()} to {$redirection}");
    }
}
