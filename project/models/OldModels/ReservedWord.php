<?php

class ReservedWord extends Object
{
	// types
	const TYPE_NORMAL = 1;
	const TYPE_TEMPLATE	 = 2;
	
	/** @var array */
	static public $types = Array(
		self::TYPE_NORMAL => 'Normal',
		self::TYPE_TEMPLATE => 'Template',
	);
	
	/** @var int */
	public $reservedWordId;
	
	/** @var int */
	public $typeId;
	
	/** @var string */
	public $type;
	
	/** @var string */
	public $word;
	
	/** @var string */
	public $description;
	
	/** @var int */
	public $fileId;
	
	/** @var object File */
	public $file;
	
	/** @var string */
	public $dtc;
	
	/** @var string */
	public $dtm;
	
	/**
	 * @return int
	 */
	public function getId() { return( $this->reservedWordId ); }

	/**
	 * @return bool
	 */
	public function exists() { return( $this->reservedWordId > 0 ); }

	/**
	 * @return bool
	 */
	public function isNew() { return( $this->reservedWordId == 0 ); }

	/**
	 * @param int $reservedWordId
	 * @return void
	 */
	public function __construct($reservedWordId = 0)
	{
		$this->reservedWordId = (int)$reservedWordId; 
		if ($this->reservedWordId) {
			$w = FApplication::$db->select('*')->from(TBL_RESERVED_WORDS)->where('reservedWordId=%i', $this->reservedWordId)->execute()->fetch();
			if ($w) {
				$this->reservedWordId = $w['reservedWordId'];
				
				$this->typeId = $w['typeId'];
				$this->type = isSet(self::$types[$w['typeId']]) ? self::$types[$w['typeId']] : NULL;
				
				$this->word = $w['word'];
				$this->description = $w['description'];
				$this->fileId = $w['fileId'];
				
				if ($this->fileId != NULL) {
					try {
						$this->file = new FFile($this->fileId);
					} catch (Exception $e) {
						$this->file = new FFile();
					}
				}
				
				$this->dtc = $w['dtc'];
				$this->dtm = $w['dtm'];
			} else {
				$this->reservedWordId = -1 * $this->reservedWordId;
			}
		} 
	}
	
	/**
	 * Provide saving reserved word to database
	 * @return int $id
	 */
	public function save()
	{
		$action = $this->reservedWordId ? 'update' : 'insert';

		// data
		$data = array();
		$data['typeId'] = $this->typeId;
		$data['word'] = $this->word;
		$data['description'] = $this->description;
		$data['fileId'] = $this->fileId;
		$data['dtm'] = new DibiDateTime();
		
		// insert
		if ($action == 'insert'){
			$data['dtc'] = new DibiDateTime();
			$this->reservedWordId = FApplication::$db->insert(TBL_RESERVED_WORDS, $data)->execute(dibi::IDENTIFIER);
		// update
		} else {
			FApplication::$db->update(TBL_RESERVED_WORDS, $data)->where('reservedWordId=%i', $this->reservedWordId)->execute();
		}
		
		return $this->reservedWordId;
	}

	/**
	 * @return void
	 */
	public function delete()
	{
		if ($this->exists()) {
			FApplication::$db->delete(TBL_RESERVED_WORDS)->where('reservedWordId=%i', $this->reservedWordId)->execute();
		}
	}
	
	
	/**
	 * Returns matched reserved words to company name
	 * @param string $companyName
	 * @param string $type
	 * @return mixed
	 */
	static public function match($companyName, $type = NULL)
	{
		$reserved = array();
		$all = FApplication::$db->select('reservedWordId, word, typeId')->from(TBL_RESERVED_WORDS)->execute()->fetchAll();
		foreach ($all as $key => $word) {
			if (preg_match('#\b'.String::lower($word->word).'\b#', String::lower($companyName))) {
				if ($type === NULL || $type !== NULL && $word->typeId == $type) {
					$reserved[$word->reservedWordId] = new self($word->reservedWordId);
				}
			}
		}
		return $reserved;
	}
	
	/**
	 * Returns reserverd words
	 * @param int $typeId
	 * @param int $limit
	 * @param int $offset
	 * @return array $words
	 */
	static public function getAll($typeId = NULL, $limit = NULL, $offset = NULL)
	{
		$result = FApplication::$db->select('reservedWordId')->from(TBL_RESERVED_WORDS)->orderBy('dtc', dibi::DESC);
		
		// filter
		if ($typeId !== NULL) $result->where('typeId=%i', $typeId);
		if ($limit 	!== NULL) $result->limit($limit);
		if ($offset !== NULL) $result->offset($offset);
		
		// get result
		$ids = $result->execute()->fetchPairs();
		
		// words
		$words = array();
		foreach ($ids as $key => $val) {
			$words[$val] = new self($val);
		}
		return $words;
	}
}
