<?php

use OfferModule\Entities\CustomerDetails;
use OfferModule\Entities\Offer;

class TaxAssistLeadModel extends Object
{

    // types
    const TYPE_GENERAL = 'General';
    const TYPE_SOLE_TRADER = 'Sole Trader';
    const TYPE_ANNUAL_ACCOUNTS = 'Annual Accounts';
    const TYPE_JOURNEY = 'N/A';

    /**
     * @var DibiConnection
     */
    private $connection;

    /**
     * @var string
     */
    private $table;

    /**
     * @param DibiConnection $connection
     * @param string $table
     */
    public function __construct(DibiConnection $connection, $table)
    {
        $this->connection = $connection;
        $this->table = $table;
    }

    /**
     * @param Customer $customer
     * @param Company $company
     */
    public function saveLead(Customer $customer, Company $company)
    {
        $data = array(
            'created_at' => time(),
            'contact_name' => $customer->firstName . ' ' . $customer->lastName,
            'business_name' => $company->getCompanyName(),
            'business_type' => ($company->getCompanyNumber() == NULL) ? $company->getLastFormSubmission()->getForm()->getType() : $company->getCompanyCategory(),
            'daytime_telephone' => $customer->phone,
            'evening_telephone' => $customer->additionalPhone,
            'special_instructions' => '',
            'email_address' => $customer->email,
            'address_1' => (isset($customer->address1)) ? $customer->address1 : 'N/A',
            'address_2' => (isset($customer->address2)) ? $customer->address1 : 'N/A',
            'address_3' => (isset($customer->address3)) ? $customer->address1 : 'N/A',
            'address_4' => 'N/A',
            'area_code' => $customer->postcode,
            'source' => 'Companies Made Simple',
            'comments' => 'N/A',
            'status' => 'Work Queue - CMS Contact Data',
            'call_transfer_status' => 'No',
            'number_supplied' => 'No',
            'input_method' => 'Web',
            'office_id' => 0,
            'master_id' => 1,
        );
        $this->connection->insert($this->table, $data)->execute();
    }

    /**
     * @param $type
     * @param $email
     * @param $fullName
     * @param $phone
     * @param $postcode
     * @param $companyName
     */
    public function saveTALead($type, $email, $fullName, $phone, $postcode, $companyName)
    {
        $data = array(
            'created_at' => time(), //Unix TimeStamp of Date and Time lead was created
            'contact_name' => $fullName, //Client Name
            'business_name' => $companyName, //Business Name
            'business_type' => 'N/A', //Business Type
            'daytime_telephone' => $phone, //Daytime Telephone Number
            'evening_telephone' => 'N/A', //Evening Telephone Number
            'special_instructions' => '', //Best time to call the client
            'email_address' => $email, //Email Address of client
            'address_1' => 'N/A', //First Line of Client's Address
            'address_2' => 'N/A', //Second Line of Client's Address
            'address_3' => 'N/A', //Third Line of Client's Address
            'address_4' => 'N/A', //Forth Line of Client's Address
            'area_code' => $postcode, //Postcode of user
            'source' => 'Companies Made Simple', //Where the client heard about TaxAssist
            'comments' => $type, //Details of Client Enquiry
            'status' => 'Work Queue - CMS Contact Data', //Current status of the lead
            'call_transfer_status' => 'No', //Was the call transferred to the Franchisee directly?
            'number_supplied' => 'No', //Was the franchisees contact number left with the client?
            'input_method' => 'Web', //Manual or Web lead
            'office_id' => 0, //no infornation
            'master_id' => 1, //no infornation
        );

        $this->connection->insert($this->table, $data)->execute();
    }

    /**
     * @param CustomerDetails $customerDetails
     */
    public function saveFromCustomerDetails(CustomerDetails $customerDetails)
    {
        $this->saveTALead(
            self::TYPE_JOURNEY,
            $customerDetails->getEmail(),
            $customerDetails->getFullName(),
            $customerDetails->getPhone(),
            $customerDetails->getPostcode(),
            $customerDetails->getCompanyName()
        );
    }

}
