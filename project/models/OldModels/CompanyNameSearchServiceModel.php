<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    CompanyNameSearchServiceModel.php 2010-11-09 divak@gmail.com
 */

class CompanyNameSearchServiceModel extends FNode
{
	/**
     * @return void
     */
    public function outputJSON(array $arr)
    {
		$json = json_encode($arr);
		
		header('Cache-Control: no-cache, must-revalidate');
    	header('Content-type: application/json');
		echo $json;
		exit;
    }
}