<?php

class WidgetCalculatorModel extends FNode
{
    /**
     * @var int
     */
    private $annualProfit;

    /**
     * @var Affiliate
     */
    private $affiliate;

    /**
     * @param string $hash
     * @throws Exception
     */
    public function startup($hash)
    {
        $affiliateId = Affiliate::getAffiliateByHash($hash);
        if ($affiliateId === FALSE) throw new Exception("Affiliate doesn't exists");
        $this->affiliate = new Affiliate($affiliateId);
        if (!$this->affiliate->hasService(Affiliate::SERVICE_WIDGET_CALCULATOR)) throw new Exception("Service doesn't exist for affiliate");
    }

    /**
     * @return Affiliate
     */
    public function getAffiliate()
    {
        return $this->affiliate;
    }


    /**
     * @param array	$callback
     * @return FForm
     */
    //public function getComponentWidgetForm()
    //{
    //$form = new FForm('LTDCalculator', 'get');
    //$form->addText('annualProfit', 'Enter your annual profits:')
    //->size(7)
    //->addRule(FForm::Required, 'Please provide annual profit')
    //->addRule(FForm::NUMERIC, 'Please provide numeric value');
    //$form->addSubmit('submit', 'Calculate')->style('width: 100px; height: 30px;');

    //$form->onValid = array($this, 'Form_widgetFormSubmitted');
    //$form->clean();
    //$form->start();

    //return $form;
    //}

    /**
     * @param FForm $form
     */
    //public function Form_widgetFormSubmitted(FForm $form)
    //{
        //$this->annualProfit = $form['annualProfit']->getValue();
    //}

    /**
     * Returns ltd calculator
     *
     * @return LTDCalculator
     */
    //public function getLTDCalculator()
    //{
        //$ltdCalculator = new LTDCalculator($this->annualProfit);
        //return $ltdCalculator;
    //}
}
