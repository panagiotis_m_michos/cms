<?php

class AnnualReturnServiceModel extends Object
{
    const STATUS_DRAFT = 'draft';
    const STATUS_COMPLETED = 'completed';
    const STATUS_REJECTED_BY_USER = 'rejected_by_user';
    const STATUS_PENDING = 'pending';
    const STATUS_ERROR= 'error';
    const STATUS_REJECTED = 'rejected';

    /**
     * @var array
     */
    public static $statues = array(
        self::STATUS_DRAFT => 'Draft',
        self::STATUS_COMPLETED => 'Completed',
        self::STATUS_REJECTED_BY_USER => 'Rejected_by_user',
        self::STATUS_PENDING => 'Pending',
        self::STATUS_ERROR => 'Error',
        self::STATUS_REJECTED => 'Rejected',
    );

    /**
     * @var int
     */
    public $annualReturnServiceId;

    /**
     * @var int
     */
    public $statusId = self::STATUS_DRAFT;

    /**
     * @var int
     */
    public $companyId;

    /**
     * @var int
     */
    public $customerId;

    /**
     * @var int
     */
    public $productId;

    /**
     * @var string
     */
    private $dtc;

    /**
     * @var string
     */
    private $dtm;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->annualReturnServiceId;
    }


    /**
     * @return bool
     */
    public function isNew()
    {
        return $this->annualReturnServiceId == 0;
    }


    /**
     * @return string
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @return string
     */
    public function getDtm()
    {
        return $this->dtm;
    }

    /**
     * @return Customer
     * @throws Exception
     */
    public function getCustomer()
    {
        try {
            return new Customer($this->customerId);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @return Company
     * @throws Exception
     */
    public function getCompany()
    {
        try {
            return Company::getCompany($this->companyId);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @return AnnualReturn
     * @throws Exception
     */
    public function getProduct()
    {
        try {
            return FNode::getProductById($this->productId);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return isset(self::$statues[$this->statusId]) ? self::$statues[$this->statusId] : NULL;
    }


    /**
     * @param int $annualReturnServiceId
     * @throws Exception
     */
    public function __construct($annualReturnServiceId = 0)
    {
        $this->annualReturnServiceId = (int) $annualReturnServiceId;
        if ($this->annualReturnServiceId) {
            $w = FApplication::$db->select('*')->from(TBL_ANNUAL_RETURN_SERVICE)->where('annualReturnServiceId=%i', $this->annualReturnServiceId)->execute()->fetch();
            if ($w) {
                $this->annualReturnServiceId = $w['annualReturnServiceId'];
                $this->statusId = $w['statusId'];
                $this->companyId = $w['companyId'];
                $this->customerId = $w['customerId'];
                $this->productId = $w['productId'];
                $this->dtc = $w['dtc'];
                $this->dtm = $w['dtm'];
            } else {
                throw new Exception("AnnualReturnServiceModel with id `$companyCustomerId` doesn't exist.");
            }
        }
    }


    /**
     * @return int
     */
    public function save()
    {
        $action = $this->annualReturnServiceId ? 'update' : 'insert';

        // data
        $data = array();
        $data['statusId'] = $this->statusId;
        $data['companyId'] = $this->companyId;
        $data['customerId'] = $this->customerId;
        $data['productId'] = $this->productId;
        $data['dtm'] = new DibiDateTime();

        // insert
        if ($action == 'insert') {
            $data['dtc'] = new DibiDateTime();
            $this->annualReturnServiceId = FApplication::$db->insert(TBL_ANNUAL_RETURN_SERVICE, $data)->execute(dibi::IDENTIFIER);
        } else {
            // update
            FApplication::$db->update(TBL_ANNUAL_RETURN_SERVICE, $data)->where('annualReturnServiceId=%i', $this->annualReturnServiceId)->execute();
        }
        return $this->annualReturnServiceId;
    }

    public function delete()
    {
        FApplication::$db->delete(TBL_ANNUAL_RETURN_SERVICE)->where('annualReturnServiceId=%i', $this->annualReturnServiceId)->execute();
    }

    /**
     * @param int $companyId
     * @return AnnualReturnServiceModel
     * @throws Exception
     */
    public static function getByCompanyId($companyId)
    {
        $id = dibi::select('annualReturnServiceId')
                ->from(TBL_ANNUAL_RETURN_SERVICE)
                ->where('companyId=%i', $companyId)
                ->orderBy('dtm')->desc()
                ->execute()->fetchSingle();

        if ($id === FALSE) {
            throw new Exception("AnnualReturnServiceModel for companyId `$companyId` doesn't exist");
        } else {
            return new self($id);
        }
    }

    /**
     * @param string $companyId
     * @throws Exception
     */
    public static function removeCompanyAnnualReturnService($companyId)
    {
        try {
            self::getByCompanyId($companyId)->delete();
            Company::removeAnnualReturn($companyId);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @param Company $company
     * @param Customer $customer
     * @throws Exception
     */
    public static function processCompletedEmail(Company $company, Customer $customer)
    {
        try {
            // change status to completed
            $ars = self::getByCompanyId($company->getCompanyId());
            $ars->statusId = self::STATUS_COMPLETED;
            $ars->save();
            $annualReturnsEmailer = Registry::$emailerFactory->get(EmailerFactory::ANNUAL_RETURN);
            $annualReturnsEmailer->sendServiceCompleted($customer, $company);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @param Company $company
     * @param Customer $customer
     * @param array $data
     * @throws Exception
     */
    public static function processMissingDataEmail(Company $company, Customer $customer, array $data)
    {
        try {
            $annualReturnsEmailer = Registry::$emailerFactory->get(EmailerFactory::ANNUAL_RETURN);
            $annualReturnsEmailer->sendServiceMissingInfo($customer, $company, $data['body']);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @param Company $company
     * @param AnnualReturnServiceModel $arsm
     * @param array $data
     * @throws Exception
     */
    public static function processRejectedByCustomer(Company $company, AnnualReturnServiceModel $arsm, array $data, Customer $customer)
    {
        try {

            // change status
            $arsm->statusId = AnnualReturnServiceModel::STATUS_REJECTED_BY_USER;
            $arsm->save();
            $annualReturnsEmailer = Registry::$emailerFactory->get(EmailerFactory::ANNUAL_RETURN);
            $annualReturnsEmailer->sendServiceRejected($customer, $company, $data['rejectionText']);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @param Company $company
     * @param CHAnnualReturn $chAnnualReturn
     * @param AnnualReturnServiceModel $arsm
     * @param Customer $customer
     * @throws Exception
     */
    public static function processAcceptedByCustomer(Company $company, CHAnnualReturn $chAnnualReturn, AnnualReturnServiceModel $arsm, Customer $customer)
    {
        try {
            // send application to CH
            $response = $chAnnualReturn->send($company);

            // send email to staff + exception
            if ($response === 'ERROR') {

                // change status
                $arsm->statusId = AnnualReturnServiceModel::STATUS_ERROR;
                $arsm->save();
                $annualReturnsEmailer = Registry::$emailerFactory->get(EmailerFactory::ANNUAL_RETURN);
                $annualReturnsEmailer->sendServiceError($customer, $company);

                throw new Exception('There has been an error with your submission to Companies House. Our staff have been alerted to the problem and are actively investigating. We expect to have the issue resolved shortly and will email you as soon as it is done.');

            } else {
                // send email to customer
                // change status
                $arsm->statusId = AnnualReturnServiceModel::STATUS_PENDING;
                $arsm->save();
                $annualReturnsEmailer = Registry::$emailerFactory->get(EmailerFactory::ANNUAL_RETURN);
                $annualReturnsEmailer->sendServiceAccepted($customer, $company);

            }

        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    /**
     * @param Customer $customer
     * @param array $return
     * @param array $replace
     * @throws Exception
     */
    public static function processCHRespond(Customer $customer, array $return, array $replace)
    {
        try {
            switch ($return['status']) {
                case 'ACCEPT':
                    $annualReturnsEmailer = Registry::$emailerFactory->get(EmailerFactory::ANNUAL_RETURN);
                    $annualReturnsEmailer->sendCompanyHouseServiceAccepted($customer, $replace);

                    //                    $arsm = AnnualReturnServiceModel::getByCompanyId($return['companyId']);
                    //                    $arsm->statusId = AnnualReturnServiceModel::STATUS_ACCEPTED;
                    //                    $arsm->save();
                    // remove annual product from company and table
                    self::removeCompanyAnnualReturnService($return['companyId']);

                    break;
                case 'REJECT':
                    $annualReturnsEmailer = Registry::$emailerFactory->get(EmailerFactory::ANNUAL_RETURN);
                    $annualReturnsEmailer->sendCompanyHouseServiceRejected($customer, $replace);


                    $arsm = AnnualReturnServiceModel::getByCompanyId($return['companyId']);
                    $arsm->statusId = AnnualReturnServiceModel::STATUS_REJECTED;
                    $arsm->save();
                    break;
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    /**
     * @param FControler $controler
     * @return FForm
     */
    public static function getCompomentSummaryForm(FControler $controler)
    {
        $form = new FForm('ARsummary');

        // confirmation
        $form->addFieldset('Confirmation');
        $form->addCheckbox('confirm', 'I confirm all above details are true and correct', 1)
            ->addRule(array('AnnualReturnServiceModel', 'Validator_requiredSummaryConfirmation'), 'Please confirm details');

        // rejection text
        $form->addFieldset('Rejection text');
        $form->addArea('rejectionText', 'Rejection text:')
            ->cols(40)->rows(5)
            ->addRule(array('AnnualReturnServiceModel', 'Validator_requiredSummaryRejectionText'), 'Please provide rejection text');
        $form->addSubmit('rejectSubmit', 'Submit')->style('height: 30px; width: 100px;');

        // action
        $form->addFieldset('Action');
        $form->addSubmit('reject', 'Reject')->style('height: 30px; width: 200px;');
        $form->addSubmit('submit', 'Accept')->style('height: 30px; width: 200px;');

        $form->onValid = array($controler, 'Form_summaryServiceFormSubmitted');
        $form->start();
        return $form;
    }


    /**
     * @param Checkbox $control
     * @param string $error
     * @param array $params
     * @return mixed
     */
    public static function Validator_requiredSummaryConfirmation(Checkbox $control, $error, array $params)
    {
        $value = $control->getValue();
        if ($control->owner->isSubmitedBy('submit') && empty($value)) {
            return $error;
        }
        return TRUE;
    }

    /**
     * @param Area $control
     * @param string $error
     * @param array $params
     * @return mixed
     */
    public static function Validator_requiredSummaryRejectionText(Area $control, $error, array $params)
    {
        $value = $control->getValue();
        if ($control->owner->isSubmitedBy('rejectSubmit') && empty($value)) {
            return $error;
        }
        return TRUE;
    }

    /**
     * @param Customer $customer
     * @param Company $company
     * @param array $data
     * @throws Exception
     */
    public static function addAnnualReturnToCustomerCompany(Customer $customer, Company $company, array $data)
    {
        try {
            // check if company has this product
            if ($company->getAnnualReturnId() == $data['productId']) {
                throw new Exception('Company already has this product.');
            }

            // check incorporation
            // setAnnualReturn() will create form submission
            if ($company->getStatus() == 'complete') {
                $company->setAnnualReturn($data['productId']);
            } else {
                $company->setAnnualReturnId($data['productId']);
            }


            // save just for service
            if ($data['productId'] == AnnualReturn::ANNUAL_RETURN_SERVICE_PRODUCT) {
                $ars = new AnnualReturnServiceModel();
                $ars->companyId = $company->getCompanyId();
                $ars->customerId = $customer->getId();
                $ars->productId = AnnualReturn::ANNUAL_RETURN_SERVICE_PRODUCT;
                $ars->save();
            }


            /* === SAVE ORDER === */

            $product = FNode::getProductById($data['productId']);

            $order = new Order();
            $order->statusId = Order::STATUS_NEW;
            $order->customerId = $customer->getId();
            $order->realSubtotal = 0;
            $order->subtotal = 0;
            $order->vat = 0;
            $order->total = 0;
            $order->customerName = $customer->firstName . ' ' . $customer->lastName;

            $item = new OrderItem();
            $item->productId = $product->getId();
            $item->productTitle = 'Custom ' . $product->getLongTitle();
            $item->qty = 1;
            $item->price = 0;
            $item->subTotal = 0;
            $item->vat = 0;
            $item->totalPrice = 0;
            $item->incorporationRequired = FALSE;

            if (!empty($data['additional'])) {
                $item->additional = $data['additional']. "\n";
                $item->additional .= "=====\n";
                $item->additional .= 'by '.FUser::getSignedIn()->login;
            } else {
                $item->additional = 'by '.FUser::getSignedIn()->login;
            }

            $order->items[] = $item;
            $orderId = $order->save();

            $transaction = new Transaction();
            $transaction->customerId = $customer->getId();
            $transaction->typeId = Transaction::ON_ACCOUNT;
            $transaction->orderId = $orderId;
            $transaction->save();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}
