<?php

use Entities\Company as CompanyEntity;
use Entities\CompanyHouse\FormSubmission;

class CompanyCustomerService extends Object
{
    /**
     * @var CHFiling
     */
    private $chFiling;

    /**
     * @var FRouter
     */
    private $router;

    /**
     * @param CHFiling $chFiling
     * @param FRouter $router
     */
    public function __construct(CHFiling $chFiling, FRouter $router)
    {
        $this->chFiling = $chFiling;
        $this->router = $router;
    }

    /**
     * @param Customer $customer
     * @return array
     */
    public function getNotIncorporateCompanies(Customer $customer)
    {
        $companies = $this->chFiling->getCustomerNotIncorporatedCompanies($customer->getId());
        $companiesWithLinks = $this->addLinksToCompanies($companies);

        foreach ($companiesWithLinks as $company) {
            if ($company['submission_status'] == FormSubmission::RESPONSE_INTERNAL_FAILURE) {
                $company['submission_status'] = FormSubmission::RESPONSE_PENDING;
            }
        }

        return $companiesWithLinks;
    }

    /**
     * @param array $companies
     * @return array
     */
    public function syncCompanies(array $companies)
    {
        $errorMessages = [];

        /** @var CompanyEntity $companyEntity */
        foreach ($companies as $companyEntity) {
            try {
                $company = Company::getCompany($companyEntity->getId());
                $company->syncPublicData();
            } catch (Exception $e) {
                $errorMessages[] = ['companyName' => $companyEntity->getCompanyName(), 'error' => $e->getMessage()];
                continue;
            }
        }

        return $errorMessages;
    }

    /**
     * @param array $companies
     * @return array
     */
    private function addLinksToCompanies(array $companies)
    {
        foreach ($companies as $companyId => $company) {

            // exclude in case of hidden
            if ($company['hidden']) {
                unset($companies[$companyId]);
                continue;
            }

            switch ($company['submission_status']) {
                case 'INCOMPLETE':
                    $companies[$companyId]['link'] = $this->router->link(
                        CFRegisteredOfficeControler::REGISTER_OFFICE_PAGE,
                        "company_id=$companyId"
                    );
                    break;
                case FormSubmission::RESPONSE_REJECT:
                case FormSubmission::RESPONSE_PENDING:
                case FormSubmission::RESPONSE_INTERNAL_FAILURE:
                    $companies[$companyId]['link'] = $this->router->link(
                        CFSummaryControler::SUMMARY_PAGE,
                        "company_id=$companyId"
                    );
                    break;
                default:
                    $companies[$companyId]['link'] = '#';
                    break;
            }
        }

        return $companies;
    }
}
