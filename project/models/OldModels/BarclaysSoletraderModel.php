<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS 
 * @author 		Stan (diviak@gmail.com)
 * @internal 	project/models/BarclaysSoletraderModel.php
 * @created 	17/05/2010
 */
class BarclaysSoletraderModel extends FNode
{

    /**
     * Returns component form for barclaysSoletrader
     *
     * @param array $callback
     * @return FForm
     */
    public function getComponentBarclaysSoletraderForm(array $callback)
    {
        $form = new FForm('BarclaysSoletrader');
        // personal details
        $form->addFieldset('Sole Trader Details');
        $form->addText('companyName', 'Company Name *')
            ->addRule(FForm::Required, 'Please provide company name');
        //$form->add('DatePicker', 'establishedDate', 'Date Established *')
        //    ->addRule(FForm::Required, 'Please provide date!.')
        //    ->class('date')->setValue(date('d-m-Y'));
        // contact date
        //$form->addFieldset('When would you like Barclays to contact you about opening a new business bank account');
        //$form->addSelect('preferredContactDate', 'Please select *', Customer::$dates)
        //->setValue(0);
        //->addRule(FForm::Required, 'Please provide date!');
        // personal details
        $form->addFieldset('What are the best details for Barclays to use to contact you?');
        $form->addText('email', 'Email *')
            ->addRule(FForm::Required, 'Please provide e-mail address!')
            ->addRule(FForm::Email, 'Email is not valid!');

        $form->addSelect('titleId', 'Title &nbsp;&nbsp;', array_combine(Customer::$titles, Customer::$titles))
            ->setFirstOption('--- Select ---');
        $form->addText('firstName', 'First Name *')
            ->addRule(FForm::Required, 'Please provide firstname!');
        $form->addText('lastName', 'Last Name  *')
            ->addRule(FForm::Required, 'Please provide lastname!');
        $form->addText('phone', 'Mobile Number *')
            ->setDescription('(The following formats are allowed: 01234567890, +441234567890, 00441234567890)')
            ->addRule(FForm::Required, 'Please provide phone!')
            ->addRule(FForm::REGEXP, 'Phone is not valid.', '#^\+([4]{2}[0-9]{10})$|^([0]{1}[0-9]{10})$|^([0]{2}[4]{2}[0-9]{10})$#');
        $form->addText('additionalPhone', 'Alternative Number *')
            ->setDescription('(The following formats are allowed: 01234567890, +441234567890, 00441234567890)')
            ->addRule(FForm::Required, 'Please provide phone!')
            ->addRule(FForm::REGEXP, 'Phone is not valid.', '#^\+([4]{2}[0-9]{10})$|^([0]{1}[0-9]{10})$|^([0]{2}[4]{2}[0-9]{10})$#');
        // address details
        $form->addFieldset('Where would you like to have your Barclays branch close to?');
        $form->addText('address1', 'Address 1  *')
            ->addRule(FForm::Required, 'Please provide address 1!');
        $form->addText('address2', 'Address 2 &nbsp;&nbsp;');
        $form->addText('address3', 'Address 3 &nbsp;&nbsp;');
        $form->addText('city', 'City  *')
            ->addRule(FForm::Required, 'Please provide city!');
        $form->addText('county', 'County &nbsp;&nbsp;');
        $form->addText('postcode', 'Postcode  *')
            ->addRule(FForm::Required, 'Please provide post code!')
            ->addRule(array('BarclaysSoletraderModel', 'Validator_postcode'), 'Postcode is not valid')
            ->setDescription('Please add 1 space in the middle');
        $form->addSelect('countryId', 'Country  *', array(223 => "United Kingdom"));
        // action
        $form->addFieldset('Action');
        $form->addSubmit('login', 'Send')
            ->setDescription('(Please double check all your details before saving them!)')
            ->style('width: 200px; height: 30px');

        //prefill in case of wholesale customer
        $data = (array) customer::getSignedIn();
        $data['phone'] = NULL;
        $data['additionalPhone'] = NULL;
        $form->setInitValues($data);

        $form->onValid = $callback;
        $form->start();

        return $form;
    }

    /**
     * Provides proccess barclaysSoletrader data
     *
     * @param FForm $form
     * @return void
     * @throws Exception
     */
    public function processBarclaysSoletraderForm(FForm $form)
    {
        try {
            $data = $form->getValues();
            $barclaysSoletrader = new BarclaysSoletrader();
            $barclaysSoletrader->companyName = $data['companyName'];
            $barclaysSoletrader->establishedDate = date('d-m-Y');//$data['establishedDate'];
            $barclaysSoletrader->preferredContactDate = 0; //$data['preferredContactDate'];
            //$barclaysSoletrader->wholesalerId = $wholesalerId;
            //$barclaysSoletrader->companyId = $companyId;
            $barclaysSoletrader->email = $data['email'];
            $barclaysSoletrader->titleId = $data['titleId'];
            $barclaysSoletrader->firstName = $data['firstName'];
            $barclaysSoletrader->lastName = $data['lastName'];
            $barclaysSoletrader->address1 = $data['address1'];
            $barclaysSoletrader->address2 = $data['address2'];
            $barclaysSoletrader->address3 = $data['address3'];
            $barclaysSoletrader->city = $data['city'];
            $barclaysSoletrader->county = $data['county'];
            $barclaysSoletrader->postcode = $data['postcode'];
            $barclaysSoletrader->countryId = $data['countryId'];
            $barclaysSoletrader->phone = $data['phone'];
            $barclaysSoletrader->additionalPhone = $data['additionalPhone'];
            $barclaysSoletrader->save();
            Barclays::sendCompanySoletrader($barclaysSoletrader->getId(), FALSE);
            $form->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

	/**
     * Validate postcode for UK
     *
     * @param Text $control
     * @param string $error
     * @param array $params
     * @return boolean
     */
    static public function Validator_postcode(Text $control, $error, array $params)
    {
        $value = $control->getValue();
        $countryId = $control->owner['countryId']->getValue();
        if ($countryId == Customer::UK_CITIZEN && (!preg_match('#^(GIR 0AA|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]|[A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y]))|[0-9][A-HJKPS-UW]) [0-9][ABD-HJLNP-UW-Z]{2})$#i', $value) || !preg_match('#^([A-Z][\dA-Z]{1,3}[\s][\d][A-Z]{2,2})$#i', $value))) {
            return $error;
        }
        return TRUE;
    }


}