<?php

class Tokens extends Object
{
    /**
     * primary key
     *
     * @var int
     */
    public $tokenId;

    /**
     * @var string
     */
    public $customerId;

    /**
     * @var string
     */
    public $token;

    /**
     * @var string
     */
    public $cardNumber;

    /**
     * @var string
     */
    public $cardHolder;

    /**
     * @var string
     */
    public $cardType;

    /**
     * @var string
     */
    public $cardExpiryDate;

    /**
     * @var string
     */
    public $firstname;

    /**
     * @var string
     */
    public $surname;	

    /**
     * @var string
     */
    public $address1;

    /**
     * @var string
     */
    public $address2;

    /**
     * @var string
     */
    public $city;

    /**
     * @var string
     */
    public $postCode;	

    /**
     * @var string
     */
    public $country;

    /**
     * @var string
     */
    public $state;

    /**
     * @var bool
     */
    public $isCurrent;


    /**
     * @var string
     */
    public $dtc;

    /**
     * @var string
     */
    public $dtm;


    /**
     * @return int
     */
    public function getId() 
    { 
        return $this->tokenId; 
    }

    /**
     * @return bool
     */
    public function isNew() 
    { 
        return ($this->tokenId == 0); 
    }

    /**
     * @param int $tokenId
     * @return void
     * @throws Exception
     */
    public function __construct($tokenId = 0)
    {
        $this->tokenId = (int) $tokenId; 
        if ($this->tokenId) {
            $w = dibi::select('*')->from(TBL_TOKENS)->where('tokenId=%i', $tokenId)->execute()->fetch();
            if ($w) {				
                $this->tokenId      = $w['tokenId'];
                $this->customerId   = $w['customerId'];
                $this->token        = $w['identifier'];
                $this->cardNumber   = $w['cardNumber'];
                $this->cardHolder   = $w['cardHolder'];
                $this->cardType     = $w['cardType'];
                $this->cardExpiryDate = $w['cardExpiryDate'];
                $this->firstname    = $w['billingFirstnames'];
                $this->surname      = $w['billingSurname'];
                $this->address1     = $w['billingAddress1'];
                $this->address2     = $w['billingAddress2'];
                $this->city         = $w['billingCity'];
                $this->postCode     = $w['billingPostCode'];
                $this->country      = $w['billingCountry'];
                $this->state        = $w['billingState'];
                $this->isCurrent    = (bool) $w['isCurrent'];
                $this->dtc          = $w['dtc'];
                $this->dtm          = $w['dtm'];
            } else {
                throw new Exception("Token with id `$tokenId` doesn't exist!");
            }
        } 
    }

    /**
     * @return int
     */
    public function save()
    {
        $data = array();
        $data['customerId'] = $this->customerId;
        $data['identifier'] = $this->token;
        $data['cardNumber'] = $this->cardNumber;
        $data['cardHolder'] = $this->cardHolder;
        $data['cardType'] = $this->cardType;
        $data['cardExpiryDate'] = $this->cardExpiryDate;
        $data['billingFirstnames'] = $data['deliveryFirstnames'] = $this->firstname;
        $data['billingSurname'] = $data['deliverySurname'] = $this->surname;
        $data['billingAddress1'] = $data['deliveryAddress1'] = $this->address1;
        $data['billingAddress2'] = $data['deliveryAddress2'] = $this->address2;
        $data['billingCity'] = $data['deliveryCity'] = $this->city;
        $data['billingPostCode'] = $data['deliveryPostCode'] = $this->postCode;
        $data['billingCountry'] = $data['deliveryCountry'] = $this->country;
        $data['billingState'] = $data['deliveryState'] = $this->state;
        $data['isCurrent'] = $this->isCurrent;
        $data['dtm'] = new DibiDateTime();

        if ($this->isNew()) {
            $data['dtc'] = new DibiDateTime();
            $this->tokenId = dibi::insert(TBL_TOKENS, $data)->execute(dibi::IDENTIFIER);

        } else {
            dibi::update(TBL_TOKENS, $data)->where('tokenId=%i', $this->tokenId)->execute();
        }
        return $this->tokenId;
    }

    /**
     * @return void
     */
    public function delete()
    {
        dibi::delete(TBL_TOKENS)->where('tokenId=%i', $this->tokenId)->execute();
    }

    /**
     * Returns count of signups
     *
     * @param array $where
     * @return int
     */
    public static function getCount(array $where = array())
    {
        $result = dibi::select('count(*)')->from(TBL_TOKENS);
        foreach ($where as $key => $val) $result->where('%n', $key, ' = %s', $val);
        $count = $result->execute()->fetchSingle();
        return $count;
    }

    /**
     * Returns all signups
     *
     * @param int $limit
     * @param int $offset
     * @param array $where
     * @return int
     */
    public static function getAll($limit = NULL, $offset = NULL, array $where = array())
    {
        $result = dibi::select('*')->from(TBL_TOKENS)->orderBy('dtc', dibi::DESC);
        if ($limit !== NULL) $result->limit($limit);
        if ($offset !== NULL) $result->offset($offset);
        foreach ($where as $key => $val) $result->where('%n', $key, ' = %s', $val);
        return $result->execute()->fetchAssoc('tokenId');
    }

    /**
     * Returns result as objects
     *
     * @param int $limit
     * @param int $offset
     * @param array $where
     * @return array<MrSiteCode>
     */
    public static function getAllObjects($limit = NULL, $offset = NULL, array $where = array())
    {
        $objects = array();
        $all = self::getAll($limit, $offset, $where);
        foreach ($all as $key => $val) {
            $objects[$key] = new self($key);		
        }
        return $objects;
    }



}
