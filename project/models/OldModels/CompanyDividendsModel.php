<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS
 * @author 		Stan (diviak@gmail.com)
 * @internal 	project/models/CompanyDividendsModel.php
 * @created 	09/04/2010
 */
class CompanyDividendsModel extends Object
{

    /**
     * @var Company
     */
    private $company;
    
    
    public static $cloneDividendId;

    /**
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    /**
     * Enter description here...
     *
     * @return
     * @throws Exception
     */
    public function getDividends()
    {
        try {
            return Dividend::getDividends($this->company->getCompanyId());
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Enter description here...
     *
     * @param FControler $controler
     * @return FForm $form
     */
    public function getAddDividendComponentForm(FControler $controler, $prefillShareholders=NULL, $prefillShares=NULL, $prefillOfficers=NULL, $cloneDividendId=NULL)
    {
        CompanyDividendsModel::$cloneDividendId = $cloneDividendId;
        
        $form = new FForm('addDividend_' . $this->company->getCompanyId());
        
        $form->setRenderClass('Default2Render');

        // action
        $action = FApplication::$router->link(NULL, "company_id={$this->company->getCompanyId()}", "ajaxRemoveShareholder=", "ajaxAddShareholder=");
        $form->setAction($action);


        $form->addFieldset('Dividend');
        $currentYear = date('Y');
        $form->add('DateSelect', 'directors_meeting', 'Directors Meeting:')->setStartYear($currentYear - 61)->setEndYear($currentYear + 5);
        $form->add('DateSelect', 'end_of_acc_period', 'End of Acc. Period:')->setStartYear($currentYear - 61)->setEndYear($currentYear + 5);
        $form->addText('dividend', 'Net Dividend Per Share: *')
            ->addRule(FForm::Required, 'Please provide net dividend per share')
            ->setDescription('Eg. 1.00');
        $form->add('DateSelect', 'paid_date', 'Pay Date:')->setStartYear($currentYear - 61)->setEndYear($currentYear + 5);

        $form->addFieldset('Share');

        // prefill
        //$form->addRadio('shareTypeToggle', 'Share type: ', array(1 => 'Use Prefilled Share Class', 2 => 'Mannually Add Share Class'));
        $form->addSelect('prefilledShareClass', 'Prefilled:', $prefillShares['select'])
            ->setFirstOption('--- Select ---'); //->addRule(array($this, 'Validator_RequiredShareType'), 'Please provide prefilled');

        $form->addText('share_class', 'Share class: *')
            ->addRule(FForm::Required, 'Please provide share class');
        $form->addSelect('currency', 'Currency:', array('GBP' => 'GBP', 'USD' => 'USD', 'EUR' => 'EUR', 'AED' => 'AED', 'AFN' => 'AFN', 'ALL' => 'ALL',
                'AMD' => 'AMD', 'ANG' => 'ANG', 'AOA' => 'AOA', 'ARS' => 'ARS',
                'AUD' => 'AUD', 'AWG' => 'AWG', 'AZN' => 'AZN', 'BAM' => 'BAM', 'BBD' => 'BBD', 'BDT' => 'BDT', 'BGN' => 'BGN', 'BHD' => 'BHD', 'BIF' => 'BIF', 'BMD' => 'BMD',
                'BND' => 'BND', 'BOB' => 'BOB', 'BOV' => 'BOV', 'BRL' => 'BRL', 'BSD' => 'BSD', 'BTN' => 'BTN', 'BWP' => 'BWP', 'BYR' => 'BYR', 'BZD' => 'BZD', 'CAD' => 'CAD',
                'CDF' => 'CDF', 'CHE' => 'CHE', 'CHF' => 'CHF', 'CHW' => 'CHW', 'CLF' => 'CLF', 'CLP' => 'CLP', 'CNY' => 'CNY', 'COP' => 'COP', 'COU' => 'COU', 'CRC' => 'CRC',
                'CUC' => 'CUC', 'CUP' => 'CUP', 'CVE' => 'CVE', 'CZK' => 'CZK', 'DJF' => 'DJF', 'DKK' => 'DKK', 'DOP' => 'DOP', 'DZD' => 'DZD', 'EEK' => 'EEK', 'EGP' => 'EGP',
                'ERN' => 'ERN', 'ETB' => 'ETB', 'FJD' => 'FJD', 'FKP' => 'FKP', 'GEL' => 'GEL', 'GHS' => 'GHS', 'GIP' => 'GIP', 'GMD' => 'GMD',
                'GNF' => 'GNF', 'GTQ' => 'GTQ', 'GYD' => 'GYD', 'HKD' => 'HKD', 'HNL' => 'HNL', 'HRK' => 'HRK', 'HTG' => 'HTG', 'HUF' => 'HUF', 'IDR' => 'IDR', 'ILS' => 'ILS',
                'INR' => 'INR', 'IQD' => 'IQD', 'IRR' => 'IRR', 'ISK' => 'ISK', 'JMD' => 'JMD', 'JOD' => 'JOD', 'JPY' => 'JPY', 'KES' => 'KES', 'KGS' => 'KGS', 'KHR' => 'KHR',
                'KMF' => 'KMF', 'KPW' => 'KPW', 'KRW' => 'KRW', 'KWD' => 'KWD', 'KYD' => 'KYD', 'KZT' => 'KZT', 'LAK' => 'LAK', 'LBP' => 'LBP', 'LKR' => 'LKR', 'LRD' => 'LRD',
                'LSL' => 'LSL', 'LTL' => 'LTL', 'LVL' => 'LVL', 'LYD' => 'LYD', 'MAD' => 'MAD', 'MDL' => 'MDL', 'MGA' => 'MGA', 'MKD' => 'MKD', 'MMK' => 'MMK', 'MNT' => 'MNT',
                'MOP' => 'MOP', 'MRO' => 'MRO', 'MUR' => 'MUR', 'MVR' => 'MVR', 'MWK' => 'MWK', 'MXN' => 'MXN', 'MXV' => 'MXV', 'MYR' => 'MYR', 'MZN' => 'MZN', 'NAD' => 'NAD',
                'NGN' => 'NGN', 'NIO' => 'NIO', 'NOK' => 'NOK', 'NPR' => 'NPR', 'NZD' => 'NZD', 'OMR' => 'OMR', 'PAB' => 'PAB', 'PEN' => 'PEN', 'PGK' => 'PGK', 'PHP' => 'PHP',
                'PKR' => 'PKR', 'PLN' => 'PLN', 'PYG' => 'PYG', 'QAR' => 'QAR', 'RON' => 'RON', 'RSD' => 'RSD', 'RUB' => 'RUB', 'RWF' => 'RWF', 'SAR' => 'SAR', 'SBD' => 'SBD',
                'SCR' => 'SCR', 'SDG' => 'SDG', 'SEK' => 'SEK', 'SGD' => 'SGD', 'SHP' => 'SHP', 'SLL' => 'SLL', 'SOS' => 'SOS', 'SRD' => 'SRD', 'STD' => 'STD', 'SYP' => 'SYP',
                'SZL' => 'SZL', 'THB' => 'THB', 'TJS' => 'TJS', 'TMT' => 'TMT', 'TND' => 'TND', 'TOP' => 'TOP', 'TRY' => 'TRY', 'TTD' => 'TTD', 'TWD' => 'TWD', 'TZS' => 'TZS',
                'UAH' => 'UAH', 'UGX' => 'UGX', 'USN' => 'USN', 'USS' => 'USS', 'UYU' => 'UYU', 'UZS' => 'UZS', 'VEF' => 'VEF', 'VND' => 'VND', 'VUV' => 'VUV',
                'WST' => 'WST', 'XAF' => 'XAF', 'XAG' => 'XAG', 'XAU' => 'XAU', 'XBA' => 'XBA', 'XBB' => 'XBB', 'XBC' => 'XBC', 'XBD' => 'XBD', 'XCD' => 'XCD', 'XDR' => 'XDR',
                'XFU' => 'XFU', 'XOF' => 'XOF', 'XPD' => 'XPD', 'XPF' => 'XPF', 'XPT' => 'XPT', 'XTS' => 'XTS', 'XXX' => 'XXX', 'YER' => 'YER', 'ZAR' => 'ZAR', 'ZMK' => 'ZMK',
                'ZWL' => 'ZWL'))
            ->setFirstOption('--- Select ---')
            ->addRule(FForm::Required, 'Please provide currency');
        $form->addText('share_value', 'Value per Share: *')
            ->addRule(FForm::Required, 'Please provide value per share');

        // take counter for session
        $shareholdersCount = self::getShareholdersFieldsCount();
        for ($i = 1; $i <= $shareholdersCount; $i++) {
            $form->addFieldset('Shareholder ' . $i);

            // prefill
            //$form->addRadio('shareTypeToggle', 'Officer type: ', array(1 => 'Use Prefilled Officer', 2 => 'Mannually Add Officer'));
            $form->addSelect('prefillShareholders_' . $i, 'Prefill Shareholders', $prefillShareholders['select'])->class('shareholderSelect')
                ->setFirstOption('--- Select --');

            $form->addText('name_' . $i, 'Name: *')
                ->addRule(FForm::Required, 'Please provide name');
            $form->addText('shares_' . $i, 'Number Held: *')
                ->addRule(FForm::Required, 'Please provide number held');
            $form->addText('premise_' . $i, 'House name/number: *')
                ->addRule(FForm::Required, 'Please provide premise');
            $form->addText('street_' . $i, 'Street: *')
                ->addRule(FForm::Required, 'Please provide street');
            $form->addText('thoroughfare_' . $i, 'Address Line 3:');
            $form->addText('post_town_' . $i, 'Post town: *')
                ->addRule(FForm::Required, 'Please provide post town');
            $form->addText('county_' . $i, 'County:');
            $form->addSelect('country_' . $i, 'Country: *', Address::$countries)
                ->setFirstOption('--- Select ---')
                ->addRule(FForm::Required, 'Please provide country');
            $form->addText('postcode_' . $i, 'Postcode: *')
                ->addRule(FForm::Required, 'Please provide postcode');
        }
        $form->addHidden('shareholdersCount', 2);

        $form->addFieldset('Authorising Officer');

        //prefill
        //$form->addRadio('officerTypeToggle', 'Officer type: ', array(1 => 'Use Prefilled Officer', 2 => 'Mannually Add Officer'));
        $form->addSelect('prefilledOfficer', 'Prefilled:', $prefillOfficers['select'])
            ->setFirstOption('--- Select ---');
            //->addRule(array($this, 'Validator_RequiredAuthorisingOfficer'), 'Please provide prefilled officer');
        $form->addText('officer_name', 'Name: *')
            ->addRule(FForm::Required, 'Please provide name');
        $form->addSelect('officer_type', 'Type:', array('Director' => 'Director', 'Secretary' => 'Secretary'))
            ->setFirstOption('--- Select ---')
            ->addRule(FForm::Required, 'Please provide type');

        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Submit')->style('width: 200px; height: 30px;');
        
        if(isset($cloneDividendId)){
            $form->clean();
            $form->setInitValues((array) $this->setCloneValues($cloneDividendId)); 
        } 
        
        $form->onValid = array($controler, 'Form_addDividendSubmitted');
        $form->start();

        return $form;
    }

    public function setCloneValues($cloneDividendId){
        $shareholding = DividendShare::getDividendShare($cloneDividendId);
        $shareholders = $shareholding->getShareholders();
        
    
        $values = array();

        $values['share_class'] = $shareholding->getShareClass();
        $values['currency'] = $shareholding->getCurrency();
        $values['share_value'] = $shareholding->getShareValue();
        $i=1;
        $dividend = Dividend::getDividend($this->company->getCompanyId(), $cloneDividendId);
        //pr($dividend);exit;
        $values['directors_meeting'] = $dividend->getDirectorsMeeting();
        $values['end_of_acc_period'] = $dividend->getAccountingPeriod();
        $values['paid_date'] = $dividend->getPaidDate();
        $values['dividend'] = $dividend->getNetDividend();
        $values['officer_name'] = $dividend->getOfficerName();
        $values['officer_type'] = $dividend->getOfficerType();
        
        foreach ($shareholders as  $value) {
            $values['name_'.$i]           = $value->getName();
            $values['shares_'.$i]         = $value->getNumberOfShares();
            $values['premise_'.$i]        = $value->getPremise();
            $values['street_'.$i]         = $value->getStreet();
            $values['thoroughfare_'.$i]   = $value->getThoroughfare();
            $values['post_town_'.$i]      = $value->getPostTown();
            $values['county_'.$i]         = $value->getCounty();
            $values['country_'.$i]        = $value->getCountry();
            $values['postcode_'.$i]       = $value->getPostcode();
            $i++;
        }
        

        return $values;
                
    }
    /**
     * Enter description here...
     *
     * @param array $data
     * @return void
     * @throws Exception
     */
    public function saveDividendData(array $data)
    {
        try {

            /* columns from db, divided by tables */
            $shareholderCols = array('name', 'premise', 'street', 'thoroughfare', 'post_town', 'county', 'country', 'postcode', 'shares');
            $dividendCols = array('type', 'directors_meeting', 'end_of_acc_period', 'record_date', 'tax_rate', 'dividend', 'declared_as', 'period', 'paid_date', 'officer_name', 'officer_type');
            $shareCols = array('share_class', 'currency', 'share_value');

            /* values received from the form */
            $shareholders = array();
            $dividend = array();
            $share = array();

            /* number of shareholders */
            //for($x = 1; $x <= $data['shareholdersCount']; $x++) {
            $x = 1;
            do {
                /* columns in shareholder table */
                foreach ($shareholderCols as $v) {
                    $key = $v . '_' . $x;   // key from the submitted form
                    if (isset($data[$key])) {
                        $value = trim($data[$key]);
                        if (!empty($value)) {
                            $shareholders[$x - 1][$v] = $data[$key];
                        }
                    }
                }
                $x++;
                if (!isset($data['name_' . $x])) {
                    break;
                }
            } while (true);

            /* dividend */
            foreach ($dividendCols as $v) {
                if (isset($data[$v])) {
                    $value = trim($data[$v]);
                    if (!empty($value)) {
                        $dividend[$v] = $data[$v];
                    }
                }
            }

            /* share */
            foreach ($shareCols as $v) {
                if (isset($data[$v])) {
                    $value = trim($data[$v]);
                    if (!empty($value)) {
                        $share[$v] = $data[$v];
                    }
                }
            }

            $paidDate = new DateTime($data['paid_date']);
            $taxRate = ($paidDate >= new DateTime('2016-04-06')) ? 0 : 10;

            /* create share */
            $shareObj = DividendShare::getNewDbDividendShare($share);

            /* create dividend and save it */
            $dividendObj = Dividend::getNewDividend($this->company->getCompanyId(), $shareObj, $dividend['dividend'], $taxRate);
            $dividendObj->setDirectorsMeeting($dividend['directors_meeting']);
            $dividendObj->setAccountingPeriod($dividend['end_of_acc_period']);
            $dividendObj->setPaidDate($dividend['paid_date']);
            $dividendObj->setOfficer($dividend['officer_name'], $dividend['officer_type']);
            $dividendObj->save();

            /* create shareholders and save them */
            foreach ($shareholders as $v) {
                $shareholder = DividendShareholder::getNewDbShareholder($v);
                $shareholder->save($dividendObj->getDividendId());
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Enter description here...
     *
     * @param int $dividendId
     * @return
     * @throws Exception
     */
    public function getShareholders($dividendId)
    {
        try {
            return Dividend::getDividend($this->company->getCompanyId(), $dividendId)->getShareholders();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Removes specified dividend
     *
     * @param int $dividendId id of the dividend to be removed
     */
    public function removeDividend($dividendId)
    {
        Dividend::getDividend($this->company->getCompanyId(), $dividendId)->remove();
    }

    /**
     * Enter description here...
     *
     * @param int $shareholderId
     * @return
     * @throws Exception
     */
    public function getShareholder($shareholderId)
    {
        try {
            return DividendShareholder::getShareholder($shareholderId);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Enter description here...
     *
     * @param int $dividendId
     * @return void
     * @throws Exception
     */
    public function outputPdfShareholdersVoucher($dividendId)
    {
        try {
            return Dividend::getDividend($this->company->getCompanyId(), $dividendId)->getVouchers();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Enter description here...
     *
     * @param int $shareholderId
     * @param int $dividendId
     * @return void
     * @throws Exception
     */
    public function outputPdfShareholderVoucher($shareholderId, $dividendId)
    {
        try {
            return Dividend::getDividend($this->company->getCompanyId(), $dividendId)->getVoucher($shareholderId);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Enter description here...
     *
     * @param int $dividendId
     * @return void
     * @throws Exception
     */
    public function getPdfReport($dividendId)
    {
        try {
            return Dividend::getDividend($this->company->getCompanyId(), $dividendId)->getPdfReport();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Provides add another fields to shareholder
     *
     * @return void
     */
    public function addShareholderField()
    {
        $count = self::getShareholdersFieldsCount() + 1;
        $this->setShareholdersFieldsCount($count);
    }

    /**
     * Provides remove fields for shareholder
     *
     * @return void
     */
    public function removeShareholderField()
    {
        $count = self::getShareholdersFieldsCount() - 1;
        $this->setShareholdersFieldsCount($count);
    }

    /**
     * Returns count of shareholders (fields)
     *
     * @return int
     */
    static private function getShareholdersFieldsCount()
    {
        $count = self::getShareholdersFieldsSession()->shareholdersCount;
        return $count;
    }

    /**
     * Set fields count to session
     *
     * @param int $count
     * @return void
     */
    static private function setShareholdersFieldsCount($count)
    {
        $session = self::getShareholdersFieldsSession();
        $session->shareholdersCount = max(1, $count);
    }

    /**
     * Returns session namespace
     *
     * @return Session
     */
    static private function getShareholdersFieldsSession()
    {
        $session = FApplication::$session->getNamespace('companyDividendsShareholders');
        if (!isset($session->shareholdersCount))
            $session->shareholdersCount = count(DividendShareholder::getShareholders(CompanyDividendsModel::$cloneDividendId));
        return $session;
    }

    /**
     * Validator for dividdend authorising officer
     *
     * @param FControl $control
     * @param string $error
     * @param array $params
     * @return mixed
     */
    public function Validator_RequiredAuthorisingOfficer($control, $error, array $params)
    {
        $name = $control->getName();
        $value = $control->getValue();
        $type = $control->owner['officerTypeToggle']->getValue();
        if ($type == 1 && $name == 'prefilledOfficer' && empty($value)) {
            return $error;
        } elseif ($type == 2 && in_array($name, array('officerName', 'officerType')) && empty($value)) {
            return $error;
        }
        return TRUE;
    }

    /**
     * Validator for dividdend share type
     *
     * @param FControl $control
     * @param string $error
     * @param array $params
     * @return mixed
     */
    public function Validator_RequiredShareType($control, $error, array $params)
    {
        $name = $control->getName();
        $value = $control->getValue();
        $type = $control->owner['shareTypeToggle']->getValue();
        if ($type == 1 && $name == 'prefilledShareClass' && empty($value)) {
            return $error;
        } elseif ($type == 2 && in_array($name, array('shareClass', 'currency', 'valuePerShare')) && empty($value)) {
            return $error;
        }
        return TRUE;
    }

    /**
     *
     * @param Company $company
     * @return <type> 
     */
    static public function getPrefillShareholders(Company $company)
    {
        $return = array();
        $return['js'] = "shareholders = new Array();\n";
        $return['select'] = array();
        $data = $company->getData();
        $persons = $data['shareholders'];
        foreach ($persons as $key => $person) {
            $return['js'] .= "shareholders[$key] = new Object();\n";
            $return['js'] .= "shareholders[$key].shares='$person[num_shares]';\n";
            $return['js'] .= "shareholders[$key].premise='$person[premise]';\n";
            $return['js'] .= "shareholders[$key].street='$person[street]';\n";
            $return['js'] .= "shareholders[$key].post_town='$person[post_town]';\n";
            $return['js'] .= "shareholders[$key].county='$person[county]';\n";
            $return['js'] .= "shareholders[$key].country='$person[country]';\n";
            $return['js'] .= "shareholders[$key].postcode='$person[postcode]';\n";
            if ($person['corporate'] == 0) {
                $return['js'] .= "shareholders[$key].name='$person[surname] $person[forename]';\n";
                $return['select'][$key] = $person['surname'] . ' ' . $person['forename'];
            } elseif ($person['corporate'] == 1) {
                $return['js'] .= "shareholders[$key].name='$person[corporate_name]';\n";
                $return['select'][$key] = $person['corporate_name'];
            }
        }
        return $return;
    }

    /**
     *
     * @param Company $company
     * @return string
     */
    static public function getPrefillOfficers(Company $company)
    {
        $return = array();
        $return['js'] = "officers = new Array();\n";
        $return['select'] = array();
        $data = $company->getData();
        if (isset($data['secretaries'])) {
            $officers = $data['secretaries'];
            foreach ($officers as $key => $officer) {
                $return['js'] .= "officers[$key] = new Object();\n";
                $return['js'] .= "officers[$key].officer_type='Secretary';\n";
                if ($officer['corporate'] == 0) {
                    $return['js'] .= "officers[$key].officer_name='$officer[forename] $officer[surname]';\n";
                    $return['select'][$key] = $officer['forename'] . ' ' . ucfirst(strtolower($officer['surname'])) . ' (Secretary)';
                } elseif ($officer['corporate'] == 1) {
                    $return['js'] .= "officers[$key].officer_name='$officer[corporate_name]';\n";
                    $return['select'][$key] = $officer['corporate_name'] . ' (Secretary)';
                }
            }
        }
        if (isset($data['directors'])) {
            $officers = $data['directors'];
            foreach ($officers as $key => $officer) {
                $return['js'] .= "officers[$key] = new Object();\n";
                $return['js'] .= "officers[$key].officer_type='Director';\n";
                if ($officer['corporate'] == 0) {
                    $return['js'] .= "officers[$key].officer_name='$officer[forename] $officer[surname]';\n";
                    $return['select'][$key] = $officer['forename'] . ' ' . ucfirst(strtolower($officer['surname'])) . ' (Director)';
                } elseif ($officer['corporate'] == 1) {
                    $return['js'] .= "officers[$key].officer_name='$officer[corporate_name]';\n";
                    $return['select'][$key] = $officer['corporate_name'] . ' (Director)';
                }
            }
        }
        return $return;
    }

    /**
     *
     * @param Company $company
     * @return string
     */
    static public function getPrefillShares(Company $company)
    {
        $return = array();
        $return['js'] = "shares = new Array();\n";
        $return['select'] = array();
        $data = $company->getData();
        foreach ($data['capitals'] as $key => $capital) {
            $fields = $capital['shares'];
            foreach ($fields as $key2 => $val2) {
                $return['js'] .= "shares[$key2] = new Object();\n";
                $return['js'] .= "shares[$key2].share_class='$val2[share_class]';\n";
                $return['js'] .= "shares[$key2].share_value='$val2[num_shares]';\n";
                $return['js'] .= "shares[$key2].currency='$capital[currency]';\n";
                $return['select'][$key2] = $val2['share_class'] . ' ' . $capital['currency'] . ' ' . $val2['num_shares'];
            }
        }
        return $return;
    }

}
