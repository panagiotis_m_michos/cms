<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    FeedbacksForm.php 2011-03-21 razvanp@madesimplegroup.com
 */

class FeedBacksModel extends FNode
{
    /**
     * @var array
     */
    static public $ratings = array(
        'Positive' => 'Positive',
        'Neutral'  => 'Neutral',
        'Negative' => 'Negative',
    );

    /** 
     * @var array
     */
    public $filter = array();

  	/**
	 * Returns feedback  paginator
	 *
	 * @return FPaginator2
	 */
	public function getFeebackPaginator(FeedBackAdminForm $filter = NULL)
	{
        $paginator = new FPaginator2(FeedBack::getCount($this->getWhereOptions($filter->getValues())));
		$paginator->htmlDisplayEnabled = FALSE;
		$paginator->itemsPerPage = 10;
		return $paginator;
	}

    /**
	 * Returns list of feedbacks
	 *
     * @param FF $filter
	 * @param FPaginator2 $paginator
	 * @return array<DibiRow>
	 */
	public function getFeedbacks(FeedBackAdminForm $filter, FPaginator2 $paginator = NULL)
	{
        if ($paginator !== NULL) {
            $feedbacks = FeedBack::getAll($this->getWhereOptions($filter->getValues()), $paginator->getLimit(), $paginator->getOffset());
        } else {
            $feedbacks = FeedBack::getAll($this->getWhereOptions($filter->getValues()));
        }
		return $feedbacks;
	}

    /**
	 * Returns where options
	 *
	 * @return array
	 */
    public function getWhereOptions($filter){
       $where =array();
       if(isset($filter['nodeId']))  $where['nodeId'] = $filter['nodeId'];
       if(isset($filter['rating']))  $where['rating'] = $filter['rating'];
       if(isset($filter['dtcFrom'])) $where['dtcFrom'] = $filter['dtcFrom'];
       if(isset($filter['dtcTo']))   $where['dtcTo'] = $filter['dtcTo'];
       return $where;
    }

    /**
	 * @param FForm $filter
	 * @return void
	 */
    public function outputFeedbackToCSV(FeedBackAdminForm $filter){
       $arr = $this->getFeedbacks($filter);
       if(empty($arr)){
            throw new Exception('No data to export !');
       }
        $header = array_keys((array) current($arr));
        array_unshift($arr, $header);
        $filename = sprintf("feedback-%s.csv", date('d-m-Y'));
        Array2Csv::output($arr, $filename);
    }
}