<?php

class CFControllerModel extends Object
{
    /**
     * @param Company $compan
     */
    protected $company;


    /**
     * @param Customer $customer
     */
    protected $customer;

    /**
     *
     * @param Company $company
     * @param Customer $customer
     */
    public function __construct(Company $company,Customer $customer)
    {
        $this->company = $company;
        $this->customer = $customer;
    }

    /**
     * Check if a specific order have product
     * @param int $productId
     * @return bool
     */
    public function checkOrderHasItem($productId)
    {
        return Order::hasItem($this->company->getOrderId(), $productId);
    }

    /**
     * Check if customer is UK CITIZEN
     * @return bool
     */
    public function checkUKCitizen()
    {
        return ($this->customer->countryId == Customer::UK_CITIZEN) ? TRUE : FALSE;
    }

    /**
     * @return bool
     */
    public function checkHasCompanyFilled()
    {
        return CompanyCustomer::hasCustomerCompanyFilled($this->company->getCompanyId());
    }

    /**
     * Check if a non uk citizen have barclays product
     * @return bool
     */
    public function checkNOUKCitizenHasBarclays()
    {
        if (!$this->checkUKCitizen() && $this->checkOrderHasItem(BarclaysBanking::BARCLAYS_BANKING_PRODUCT) && !$this->checkHasCompanyFilled()) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Add bank order item  (Card One, HSBC or BARCLAYS)
     * @param integer $productId
     * @throws Exception
     */
    public function addBankOrderItem($productId)
    {
        if (!$this->company->getOrderId()) {
            throw new Exception('Order Id is required');
        }

        $product = new Product($productId);
        $item = new OrderItem();
        $item->orderId = $this->company->getOrderId();
        $item->productId = $product->getId();
        $item->productTitle = $product->getLngTitle();
        $item->qty = 1;
        $item->price = 0;
        $item->subTotal = 0;
        $item->vat = 0;
        $item->totalPrice = 0;
        $item->incorporationRequired = FALSE;

        $item->save();
    }

    /**
     * @param int $itemId
     * @return bool
     * diferent than checkOrderHasItem
     */
    public function checkOrderItemsHasProduct($itemId)
    {
        $result = dibi::select('*')
            ->from(TBL_ORDERS_ITEMS)
            ->where('companyId =%i', $this->company->getCompanyId())
            ->and('productId=%i', $itemId)
            ->execute()
            ->fetch();

        return ($result === FALSE) ? FALSE : TRUE;
    }

}
