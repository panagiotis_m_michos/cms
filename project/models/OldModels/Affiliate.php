<?php

class Affiliate extends Object
{
    const STATUS_ACTIVE = 1;
    const STATUS_BLOCKED = 2;
    const STATUS_NOT_VALIDATED = 3;

    /**
     * @var array
     */
    public static $statuses = [
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_BLOCKED => 'Blocked',
        self::STATUS_NOT_VALIDATED => 'Not Validated',
    ];

    const AUTH_EMAIL_NOT_FOUND = 1;
    const AUTH_PASSWORD_DOESNT_MATCH = 2;
    const AUTH_NOT_VALIDATED = 3;
    const AUTH_BLOCKED = 4;

    const SERVICE_WHITE_LABEL = 'whiteLabel';
    const SERVICE_WIDGET_CALCULATOR = 'widgetCalculator';
    const SERVICE_WIDGET_SEARCH = 'widgetSearch';
    
    /**
     * @var array
     */
    public static $services = [
        self::SERVICE_WHITE_LABEL => 'White Label',
        self::SERVICE_WIDGET_CALCULATOR => 'Widget Calculator',
        self::SERVICE_WIDGET_SEARCH => 'Widget Search',
    ];

    /**
     * @var string
     */
    public static $analyticsReportId = '2676250';

    /**
     * @var array
     */
    public static $analyticsDimensions = ['source', 'medium'];

    /**
     * @var array
     */
    public static $analyticsMetrics = ['visits', 'transactionRevenue', 'transactions', 'uniquePurchases'];

    /**
     * @var array
     */
    public static $analyticsSort = ['-transactions'];

    /**
     * @var int
     */
    public $affiliateId;
    
    /**
     * @var int
     */
    public $statusId = self::STATUS_ACTIVE;

    /**
     * @var string
     */
    public $name;
    
    /**
     * @var string
     */
    public $hash;
    
    /**
     * @var string
     */
    public $companyName;

    /**
     * @var string
     */
    public $firstName;

    /**
     * @var string
     */
    public $lastName;
    
    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $password;
    
    /**
     * @var string
     */
    public $phone;
    
    /**
     * @var string
     */
    public $web;

    /**
     * @var array
     */
    public $packagesIds = [];

    /**
     * @var string
     */
    public $companyColour;
    
    /**
     * @var string
     */
    public $textColour;
    
    /**
     * @var array
     */
    public $servicesIds = [];

    /**
     * @var string
     */
    public $googleAnalyticsTrackingTag;
    
    /**
     * @var string
     */
    public $dtc;
    
    /**
     * @var string
     */
    public $dtm;

    /**
     * @var string
     */
    public $textbox;

    /**
     * @var array
     */
    public static $defaultProducts = [];

    /**
     * @var string
     */
    private static $nameSpace = 'affiliate';

    /**
     * @param int $affiliateId
     * @throws Exception
     */
    public function __construct($affiliateId = 0)
    {
        $this->affiliateId = (int) $affiliateId;
        if ($this->affiliateId) {
            $w = dibi::select('*')->from(TBL_AFFILIATES)->where('affiliateId=%i', $this->affiliateId)->execute()->fetch();
            if ($w) {
                $this->affiliateId = $w['affiliateId'];
                $this->statusId = $w['statusId'];
                $this->name = $w['name'];
                $this->hash = $w['hash'];
                $this->companyName = $w['companyName'];
                $this->firstName = $w['firstName'];
                $this->lastName = $w['lastName'];
                $this->email = $w['email'];
                //$this->password = $w['password'];
                $this->phone = $w['phone'];
                $this->web = $w['web'];
                $this->companyColour = $w['companyColour'];
                $this->textColour = $w['textColour'];
                $this->googleAnalyticsTrackingTag = $w['googleAnalyticsTrackingTag'];
                $this->textbox = $w['textbox'];

                if ($w['servicesIds'] != NULL) {
                    $this->servicesIds = explode('|', $w['servicesIds']);
                }
                if ($w['packagesIds'] != NULL) {
                    $this->packagesIds = AffiliateProduct::getAffiliateProductIds($this->affiliateId);
                }

                $this->dtc = $w['dtc'];
                $this->dtm = $w['dtm'];
            } else {
                throw new Exception("Affiliate with id `$affiliateId` doesn't exist.");
            }
        }
    }

    /**
     * @return int
     */
    public function getId()
    {
        return ($this->affiliateId);
    }

    /**
     * @return bool
     */
    public function isNew()
    {
        return ($this->affiliateId == 0);
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return (isset(self::$statuses[$this->statusId])) ? self::$statuses[$this->statusId] : NULL;
    }

    /**
     * @return Package[]
     */
    public function getPackages()
    {
        $products = [];
        foreach (AffiliateProduct::getAffiliateProducts($this->getId()) as $item) {
            $product = $item->getProduct();
            $product->markUp = $item->markUp;
            $product->price += $item->markUp;
            $products[$item->productId] = $product;
        }

        return $products;
    }

    /**
     * @return Package[]
     */
    public function getPackagesForRadioForm()
    {
        $packages = [];
        foreach ($this->packagesIds as $packageId) {
            $packages[$packageId] = FNode::getProductById($packageId);
        }

        return $packages;
    }

    /**
     * @return array
     */
    public function getServices()
    {
        $services = [];
        foreach ($this->servicesIds as $serviceId) {
            if (isset(self::$services[$serviceId])) {
                $services[$serviceId] = self::$services[$serviceId];
            }
        }

        return $services;
    }

    /**
     * @param string $serviceId
     * @return boolean
     */
    public function hasService($serviceId)
    {
        $services = $this->getServices();

        return isset($services[$serviceId]);
    }

    /**
     * @return int
     */
    public function saveCustomize()
    {
        $data = [];
        $data['companyColour'] = $this->companyColour;
        $data['textColour'] = $this->textColour;
        dibi::update(TBL_AFFILIATES, $data)->where('affiliateId=%i', $this->affiliateId)->execute();

        return $this->affiliateId;
    }
    /**
     * @return int
     */
    public function save()
    {
        $data = [];
        $data['statusId'] = $this->statusId;
        $data['name'] = $this->name;
        $data['hash'] = $this->hash;
        $data['companyName'] = $this->companyName;
        $data['firstName'] = $this->firstName;
        $data['lastName'] = $this->lastName;
        $data['email'] = $this->email;
        $data['phone'] = $this->phone;
        $data['web'] = $this->web;
        $data['companyColour'] = $this->companyColour;
        $data['textColour'] = $this->textColour;
        $data['googleAnalyticsTrackingTag'] = $this->googleAnalyticsTrackingTag;
        $data['textbox'] = $this->textbox;
        $data['dtm'] = new DibiDateTime();

        if (!empty($this->password)) {
            $data['password'] = md5($this->password);
        }

        if (!empty($this->packagesIds)) {
            $data['packagesIds'] = implode('|', $this->packagesIds);
        } else {
            $data['packagesIds'] = NULL;
        }

        if (!empty($this->servicesIds)) {
            $data['servicesIds'] = implode('|', $this->servicesIds);
        } else {
            $data['servicesIds'] = NULL;
        }
        
        if ($this->isNew()) {
            $data['dtc'] = new DibiDateTime();
            $this->affiliateId = dibi::insert(TBL_AFFILIATES, $data)->execute(dibi::IDENTIFIER);

            //insert affiliate products by default
            if (!empty($this->packagesIds)) {
                foreach ($this->packagesIds as $value) {
                    $affiliateProduct = new AffiliateProduct;
                    $affiliateProduct->affiliateId = $this->affiliateId;
                    $affiliateProduct->productId = $value;
                    $affiliateProduct->markUp = 0.00;
                    $affiliateProduct->save();
                }
            }
        } else {
            dibi::update(TBL_AFFILIATES, $data)->where('affiliateId=%i', $this->affiliateId)->execute();
        }

        return $this->affiliateId;
    }

    public function delete()
    {
        dibi::delete(TBL_AFFILIATES)->where('affiliateId=%i', $this->affiliateId)->execute();
        dibi::delete(TBL_AFFILIATE_PRODUCTS)->where('affiliateId=%i', $this->affiliateId)->execute();
    }

    /**
     * @param int $limit
     * @param int $offset
     * @param array $where
     * @return DibiRow[]
     */
    public static function getAll($limit = NULL, $offset = NULL, array $where = [])
    {
        $result = dibi::select('*')->from(TBL_AFFILIATES)->orderBy('dtc', dibi::DESC);

        if ($limit !== NULL) {
            $result->limit($limit);
        }
        if ($offset !== NULL) {
            $result->offset($offset);
        }

        foreach ($where as $key => $val) {
            $result->where('%n', $key, ' = %s', $val);
        }

        return $result->execute()->fetchAssoc('affiliateId');
    }

    /**
     * @param int $limit
     * @param int $offset
     * @param array $where
     * @return Affiliate[]
     */
    public static function getAllObjects($limit = NULL, $offset = NULL, array $where = [])
    {
        $objects = [];
        $all = self::getAll($limit, $offset, $where);
        foreach ($all as $key => $val) {
            $objects[$key] = new self($key);
        }

        return $objects;
    }

    /**
     * @param array $where
     * @return int
     */
    public static function getCount(array $where = [])
    {
        $result = dibi::select('count(*)')->from(TBL_AFFILIATES);

        foreach ($where as $key => $val) {
            $result->where('%n', $key, ' = %s', $val);
        }

        return $result->execute()->fetchSingle();
    }

    /**
     * @return array
     */
    public static function getDropdown()
    {
        return dibi::select('affiliateId, companyName')
            ->from(TBL_AFFILIATES)
            ->orderBy('companyName')
            ->execute()
            ->fetchPairs();
    }
    
    /**
     * @param string $hash
     * @return int
     */
    public static function getAffiliateByHash($hash)
    {
        $affiliate = FApplication::$db->select('*')->from(TBL_AFFILIATES)->where('hash=%s', $hash)->execute()->fetch();
        if ($affiliate === FALSE || $affiliate->statusId == self::STATUS_BLOCKED) {
            return FALSE;
        }

        return $affiliate->affiliateId;
    }

    /**
     * @param int $limit
     * @param int $offset
     * @param int $dtFrom
     * @param int $dtTill
     * @return DibiRow[]
     */
    public function getAffiliateOrders($limit = NULL, $offset = NULL, $dtFrom = NULL, $dtTill = NULL)
    {
        $result = FApplication::$db->select('o.*,oi.*')
            ->from(TBL_ORDERS . ' o')
            ->join(TBL_CUSTOMERS, ' c')->on('c.customerId=o.customerId')
            ->join(TBL_ORDERS_ITEMS . ' oi')->on('o.orderId=oi.orderId')
            ->where('c.affiliateId=%i', $this->getId())
            ->orderBy('o.dtc', dibi::DESC);

        if ($limit !== NULL) {
            $result->limit($limit);
        }
        if ($offset !== NULL) {
            $result->offset($offset);
        }

        if ($dtFrom !== NULL) {
            $result->where('DATE_FORMAT(o.dtc, "%Y-%m-%d") >= %s', $dtFrom);
        }
        if ($dtTill !== NULL) {
            $result->where('DATE_FORMAT(o.dtc, "%Y-%m-%d") <= %s', $dtTill);
        }

        return $result->execute()->fetchAssoc('orderId');
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return Order[]
     */
    public function getAffiliateOrdersObjects($limit = NULL, $offset = NULL)
    {
        $objects = [];
        $all = self::getAffiliateOrders($limit, $offset);
        foreach ($all as $orderId => $order) {
            $objects[$orderId] = new Order($orderId);
        }

        return $objects;
    }

    /**
     * @param string $dtFrom
     * @param string $dtTill
     * @return int
     */
    public function getAffiliateOrdersCount($dtFrom = NULL, $dtTill = NULL)
    {
        $result = FApplication::$db->select('COUNT(*)')
            ->from(TBL_ORDERS . ' o')
            ->join(TBL_CUSTOMERS, ' c')->on('c.customerId=o.customerId')
            ->where('c.affiliateId=%i', $this->getId())
            ->orderBy('o.dtc', dibi::DESC);

        if ($dtFrom !== NULL) {
            $result->where('DATE_FORMAT(o.dtc, "%Y-%m-%d") >= %s', $dtFrom);
        }
        if ($dtTill !== NULL) {
            $result->where('DATE_FORMAT(o.dtc, "%Y-%m-%d") <= %s', $dtTill);
        }

        return $result->execute()->fetchSingle();
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return DibiRow[]
     */
    public function getAffiliateCustomers($limit = NULL, $offset = NULL)
    {
        $where = ['affiliateId' => '===' . $this->getId()];

        return Customer::getCustomers($where, $limit, $offset, FALSE);
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return Customer[]
     */
    public function getAffiliateCustomersObjects($limit = NULL, $offset = NULL)
    {
        $where = ['affiliateId' => '===' . $this->getId()];

        return Customer::getCustomers($where, $limit, $offset, TRUE);
    }

    /**
     * @return int
     */
    public function getAffiliateCustomersCount()
    {
        $where = ['affiliateId' => '===' . $this->getId()];

        return Customer::getCustomersCount($where);
    }

    /**
     * @param int $affiliateId
     * @param string $dtFrom
     * @oaram string $dtTill
     * @return DibiRow[]
     */
    public static function getAffiliateReport($affiliateId, $dtFrom = NULL, $dtTill = NULL)
    {
        $sql = dibi::select('c.customerId')
            ->select('c.email')
            ->select('COUNT(*)')->as('count')
            ->from(TBL_ORDERS, 'o')
            ->join(TBL_CUSTOMERS, 'c')
            ->on('o.customerId=c.customerId')
            ->where('c.affiliateId=%i', $affiliateId)
            ->groupBy('o.customerId');

        if ($dtFrom !== NULL) {
            $sql->where('DATE_FORMAT(o.dtc, "%Y-%m-%d") >= %s', $dtFrom);
        }
        if ($dtTill !== NULL) {
            $sql->where('DATE_FORMAT(o.dtc, "%Y-%m-%d") <= %s', $dtTill);
        }

        return $sql->execute()->fetchAll();
    }

    /**
     * @param string $dtFrom
     * @param string $dtTill
     * @return DibiRow[]
     */
    public static function getAffiliatesReport($dtFrom = NULL, $dtTill = NULL)
    {
        $reports = [];
        $all = self::getAll();
        foreach ($all as $key => $affiliate) {
            $reports[$affiliate->companyName] = self::getAffiliateReport($affiliate->affiliateId, $dtFrom, $dtTill);
        }

        return $reports;
    }

    /**
     * @param array $data
     */
    public static function getCSVAffiliateReport(array $data)
    {
        debug::disableProfiler();

        $header = array_keys((array) current($data));
        $footer = ['', 'Total', 0];
        foreach ($data as $key => $val) {
            $footer[2] += $val['count'];
        }

        array_unshift($data, $header);
        array_push($data, $footer);

        Array2Csv::output($data);
    }

    /**
     * @param array $data
     */
    public static function getCSVAffiliatesReport(array $data)
    {
        debug::disableProfiler();

        $csv = [];
        $footer = ['', 'Total', 0];
        foreach ($data as $name => $affiliate) {
            $csv[] = [$name];
            if (!empty($affiliate)) {
                $header = array_keys((array) current($affiliate));
                array_push($csv, $header);
                foreach ($affiliate as $key => $customer) {
                    array_push($csv, $customer);
                    $footer[2] += $customer['count'];
                }
            } else {
                $csv[] = ['No orders'];
            }
            $csv[] = [''];
        }

        array_push($csv, $footer);
        Array2Csv::output($csv);
    }

    /**
     * @param string $email
     * @param string $password
     * @return Affiliate self
     * @throws Exception
     */
    public static function doAuthenticate($email, $password)
    {
        $affiliate = dibi::select('`affiliateId`, `password`, `statusId`')
            ->from(TBL_AFFILIATES)
            ->where('email=%s', $email)
            ->execute()
            ->fetch();

        // email is not found
        if (empty($affiliate)) {
            throw new Exception(self::AUTH_EMAIL_NOT_FOUND);
        }
        //  password doesn't match
        if ($affiliate['password'] != md5($password)) {
            throw new Exception(self::AUTH_PASSWORD_DOESNT_MATCH);
        }
        //  not validated yet
        if ($affiliate['statusId'] == self::STATUS_NOT_VALIDATED) {
            throw new Exception(self::AUTH_NOT_VALIDATED);
        }
        //  blocked
        if ($affiliate['statusId'] == self::STATUS_BLOCKED) {
            throw new Exception(self::AUTH_BLOCKED);
        }

        return new self($affiliate['affiliateId']);
    }

    /**
     * @param int $affiliateId
     */
    public static function signIn($affiliateId)
    {
        $session = FApplication::$session->getNameSpace(self::$nameSpace);
        $session->affiliateId = (int) $affiliateId;
    }

    /**
     * @return boolean
     */
    public static function isSignedIn()
    {
        $session = FApplication::$session->getNameSpace(self::$nameSpace);

        return isset($session->affiliateId) ? TRUE : FALSE;
    }

    /**
     * @return Affiliate
     */
    public static function getSignedIn()
    {
        $session = FApplication::$session->getNameSpace(self::$nameSpace);

        return self::isSignedIn() ? new self($session->affiliateId) : new self(0);
    }

    public static function signOut()
    {
        if (self::isSignedIn()) {
            $session = FApplication::$session->getNameSpace(self::$nameSpace);
            unset($session->affiliateId);
        }
    }

    /**
     * @param string $email
     * @return Affiliate
     * @throws Exception
     */
    public static function getAffiliateByEmail($email)
    {
        $id = dibi::select('affiliateId')->from(TBL_AFFILIATES)->where('email=%s', $email)->execute()->fetchSingle();
        if ($id === FALSE) {
            throw new Exception("Affiliate with email `$email` doesn't exist!");
        }

        return new self($id);
    }

    /**
     * @param string $website
     * @return Affiliate
     * @throws Exception
     */
    public static function getAffiliateByWeb($website)
    {
        $id = dibi::select('affiliateId')->from(TBL_AFFILIATES)->where('web=%s', $website)->execute()->fetchSingle();
        if ($id === FALSE) {
            throw new Exception("Affiliate with website `$website` doesn't exist!");
        }

        return new self($id);
    }
}
