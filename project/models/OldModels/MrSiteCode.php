<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS 
 * @author 		Stan (diviak@gmail.com)
 * @internal 	project/models/MrSiteCode.php
 * @created 	17/08/2010
 */

class MrSiteCode extends Object
{
	/**
	 * primary key
	 *
	 * @var int
	 */
	public $mrSiteCodeId;
	
	/**
	 * @var int
	 */
	public $orderId;
	
	/**
	 * @var int
	 */
	public $productId;
	
	/**
	 * @var string
	 */
	public $code;
	
	/**
	 * @var string
	 */
	public $dtc;
	
	/**
	 * @var string
	 */
	public $dtm;
	
		
	/**
	 * @return int
	 */
	public function getId() 
	{ 
		return $this->mrSiteCodeId; 
	}
	
	/**
	 * @return bool
	 */
	public function isNew() 
	{ 
		return ($this->mrSiteCodeId == 0); 
	}

	/**
	 * @param int $mrSiteCodeId
	 * @return void
	 * @throws Exception
	 */
	public function __construct($mrSiteCodeId = 0)
	{
		$this->mrSiteCodeId = (int) $mrSiteCodeId; 
		if ($this->mrSiteCodeId) {
			$w = dibi::select('*')->from(TBL_MR_SITE_CODES)->where('mrSiteCodeId=%i', $mrSiteCodeId)->execute()->fetch();
			if ($w) {				
				$this->mrSiteCodeId = $w['mrSiteCodeId'];
				$this->orderId = $w['orderId'];
				$this->productId = $w['productId'];
				$this->code = $w['code'];
				$this->dtc = $w['dtc'];
				$this->dtm = $w['dtm'];
			} else {
				throw new Exception("Mr Site Code with id `$mrSiteCodeId` doesn't exist!");
			}
		} 
	}
	
	/**
	 * @return int
	 */
	public function save()
	{
		$data = array();
		$data['orderId'] = $this->orderId;
		$data['productId'] = $this->productId;
		$data['code'] = $this->code;
		$data['dtm'] = new DibiDateTime();
		
		if ($this->isNew()) {
			$data['dtc'] = new DibiDateTime();
			$this->mrSiteCodeId = dibi::insert(TBL_MR_SITE_CODES, $data)->execute(dibi::IDENTIFIER);
		} else {
			dibi::update(TBL_MR_SITE_CODES, $data)->where('mrSiteCodeId=%i', $this->mrSiteCodeId)->execute();
		}
		return $this->mrSiteCodeId;
	}

	/**
	 * @return void
	 */
	public function delete()
	{
		dibi::delete(TBL_MR_SITE_CODES)->where('mrSiteCodeId=%i', $this->mrSiteCodeId)->execute();
	}
	
	/**
	 * @return MrSiteProduct
	 */
	public function getProduct()
	{
		try {
			$product = FNode::getProductById($this->productId);
		} catch (Exception $e) {
			$product = new FNode();
		}
		return $product;
	}
	
	/**
	 * Returns count of mr site codes
	 *
	 * @param array $where
	 * @return int
	 */
	static public function getCount(array $where = array())
	{
		$result = dibi::select('count(*)')->from(TBL_MR_SITE_CODES);
		foreach ($where as $key => $val) $result->where('%n', $key, ' = %s',$val);
		$count = $result->execute()->fetchSingle();
		return $count;
	}
	
	/**
	 * Returns all cpdes
	 *
	 * @param int $limit
	 * @param int $offset
	 * @param array $where
	 * @return int
	 */
	static public function getAll($limit = NULL, $offset = NULL, array $where = array())
	{
		$result = dibi::select('*')->from(TBL_MR_SITE_CODES)->orderBy('dtc', dibi::DESC);
		if ($limit !== NULL) $result->limit($limit);
		if ($offset !== NULL) $result->offset($offset);
		foreach ($where as $key => $val) $result->where('%n', $key, ' = %s',$val);
		return $result->execute()->fetchAssoc('mrSiteCodeId');
	}
	
	/**
	 * Returns result as objects
	 *
	 * @param int $limit
	 * @param int $offset
	 * @param array $where
	 * @return array<MrSiteCode>
	 */
	static public function getAllObjects($limit = NULL, $offset = NULL, array $where = array())
	{
		$objects = array();
		$all = self::getAll($limit, $offset, $where);
		foreach ($all as $key => $val) {
			$objects[$key] = new self($key);		
		}
		return $objects;
	}
	
	/**
	 * Returns list of MrSiteProducts
	 *
	 * @return array
	 */
	static public function getAllProducts()
	{
		/* @var $product MrSiteProduct*/
		$return = array();
		$nodes = FNode::getChildsNodes(MrSiteCodesModel::PRODUCTS_FOLDER);
		foreach ($nodes as $product) {
			$return[$product->getId()] = $product->getLngTitle();
		}
		return $return;
	}
	
	/**
	 * Returns code and will mark it as used
	 *
	 * @param int $productId
	 * @param int $orderId
	 * @return string
	 * @throws Exception
	 */
	static public function getAndUseAvailableCode($productId, $orderId)
	{
		$codeId = dibi::select('mrSiteCodeId')
			->from(TBL_MR_SITE_CODES)
			->where('productId=%i', $productId)
				->and('orderId IS NULL')
			->execute()
			->fetchSingle();
			
		if ($codeId === FALSE) {
			throw new Exception('There is no available Mr. Site Code');
		}
		
		$code = new self($codeId);
		$code->orderId = $orderId;
		$code->save();
		
		return $code->code;
	}
}