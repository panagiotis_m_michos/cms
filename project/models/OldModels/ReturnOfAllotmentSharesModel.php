<?php


class ReturnOfAllotmentSharesModel extends Object
{
    /**
     * @var ReturnOfAllotmentShares 
     */
    private $returnOfAllotment;

    
    /**
     * @param Company $company
     */
    public function __construct(Company $company) {
        $this->returnOfAllotment = $company->getReturnOfAllotmentShares();
    }

    /**
     * @return array
     */
    public function getShares() {
        return $this->returnOfAllotment->getShares();
    }
    

    /**
     * Sends return of allotment shares to companies house
     *
     * @param Company $company
     * @return void
     */
	public function saveSummary(Company $company, array $data)
	{
        $data2 = array();
        $shares = $this->getShares();
        foreach ($shares as $key => $share) {
            $data2[$key]['share_class'] = isset($data['share_class_'.$key]) ? $data['share_class_'.$key] : $share['share_class'];
            $data2[$key]['prescribed_particulars'] = isset($data['prescribed_particulars_'.$key]) ? $data['prescribed_particulars_'.$key] : $share['prescribed_particulars'];
            $data2[$key]['num_shares'] = isset($data['num_shares_'.$key]) ? $data['num_shares_'.$key] : $share['num_shares'];
            $data2[$key]['amount_paid'] = isset($data['amount_paid_'.$key]) ? $data['amount_paid_'.$key] : $share['amount_paid'];
            $data2[$key]['amount_unpaid'] = isset($data['amount_unpaid_'.$key]) ? $data['amount_unpaid_'.$key] : $share['amount_unpaid'];
            $data2[$key]['currency'] = isset($data['currency_'.$key]) ? $data['currency_'.$key] : $share['currency'];
            $data2[$key]['new_shares_add'] = isset($data['new_shares_add_'.$key]) ? $data['new_shares_add_'.$key] : $share['new_shares_add'];
        }

        $this->returnOfAllotment->setAllotment($data2);
        
        $this->returnOfAllotment->send($company);
	}
	
	
	
	/************************** components **************************/
	
	
	/**
	 * Returns form
	 *
	 * @param FControler $controler
	 * @param unknown_type $model
	 * @param unknown_type $type
	 * @return unknown
	 */
	static public function getComponentSummaryForm(FControler $controler, $model, $type = NULL)
	{
		$form = new FForm('CUReturnAllotmentShares');
		
		// action
		
        foreach($model->getShares() as $k => $share) {
            $form->addFieldset('Share '.$k);
           // pr($model->getShares()); exit;
            $form->addText('share_class_'.$k, "Share class:")
            	//->setGroup($k)
            	->setValue($share['share_class']);
           
            $form->addText('prescribed_particulars_'.$k, "Prescribed Particulars:")
            	->setValue($share['prescribed_particulars']);
//   		->setValue('FULL VOTING RIGHTS');
//            ->setGroup($k);
            
            $form->addText('num_shares_'.$k, "Num Shares:")
//            ->setGroup($k)
            	->setValue($share['num_shares'])
            	->addRule(FForm::REGEXP, 'Number of shares can be whole number (positive)', '#^\d{1,11}$#');

            $form->addText('amount_paid_'.$k, "Amount Paid")
//            ->setGroup($k)
            	->setValue($share['amount_paid']);
//		->addRule(FForm::REGEXP, 'Amount Paid can be whole number (positive)', '#^\d{1,11}$#');

            $form->addText('amount_unpaid_'.$k, "Amount Unpaid")
//            ->setGroup($k)
            	->setValue($share['amount_unpaid']);
// 		->addRule(FForm::REGEXP, 'Amount Unpaid can be whole number (positive)', '#^\d{1,11}$#');

            $form->addSelect('currency_'.$k, "Currency", array('GBP' => 'GBP ', 'USD' => 'USD ', 'EUR' => 'EUR '))
//            ->setGroup($k)
		->setValue($share['currency']);

             $form->addText('new_shares_add_'.$k, "No. of New Shares")
		->addRule(FForm::REGEXP, 'Number of new shares can be whole number (positive)', '#^\d{1,11}$#')
//            ->setGroup($k)
            	->setValue('0');

        }

        $form->addFieldset('Action');
		$form->addSubmit('submit', 'Submit');
        
		if ($type == 'admin') {
			$form['submit']->class('btn');
			$form['submit']->onClick('return confirm("Are you sure?")');
			$form->onValid = array($controler, 'Form_CUreturnOfAllotmentSharesFormSubmitted');
		} else {
			$form['submit']->class('btn_submit mbottom');
			$form['submit']->onClick('return confirm("Are you sure?")');
			$form->onValid = array($controler, 'Form_summaryFormSubmitted');	
		}

		$form->start();
		return $form;
	}
}