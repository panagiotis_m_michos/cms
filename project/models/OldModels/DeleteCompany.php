<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    DeleteCompany.php 2011-05-25 razvanp@madesimplegroup.com
 */
class DeleteCompany extends Object
{

    /**
     * primary key
     *
     * @var int
     */
    public $deletedCompanyId;

    /**
     * @var int
     */
    public $customerId;

    /**
     * @var int
     */
    public $companyId;

    /**
     * @var string
     */
    public $customerName;

    /**
     * @var int
     */
    public $companyNumber;

    /**
     * @var tinytext
     */
    public $companyName;

    /**
     * @var text
     */
    public $rejectMessage;

    /**
     * @var date
     */
    public $deletedDate;


    /**
     * @var datetime
     */
    public $dtc;

    /**
     * @var datetime
     */
    public $dtm;

    /**
     * @return bool
     */
    public function isNew()
    {
        return ($this->deletedCompanyId == 0);
    }

    /**
     * @return int
     */
    public function save()
    {
        $data = array();
        $data['customerId'] = $this->customerId;
        $data['companyId'] = $this->companyId;
        $data['customerName'] = $this->customerName;
        $data['companyNumber'] = $this->companyNumber;
        $data['companyName'] = $this->companyName;
        $data['rejectMessage'] = $this->rejectMessage;
        $data['deletedDate'] = new DibiDateTime();
        $data['dtm'] = new DibiDateTime();
        if ($this->isNew()) {
            $data['dtc'] = new DibiDateTime();
            $this->deletedCompanyId = dibi::insert(TBL_DELETED_COMPANIES, $data)->execute(dibi::IDENTIFIER);
        }
        return $this->deletedCompanyId;
    }

    /**
     * Returns count of all records from cms2_deleted_companies table
     *
     * @param array $where
     * @return int
     */
    public static function getCount(array $where = array())
    {

        $result = FApplication::$db->select('COUNT(*)')->from(TBL_DELETED_COMPANIES)->orderBy('dtc')->desc();
        foreach ($where as $key => $val) {
            $result->where($key . '=%i', $val);
        }
        $count = $result->execute()->fetchSingle();

        return $count;
    }


    /**
     * Returns deleted companyes as array
     *
     * @param int $limit
     * @param int $offset
     * @param array $where
     * @return array<DibiRow>
     */
    public static function getAll(array $where = array(), $limit = NULL, $offset = NULL)
    {
        $result = FApplication::$db->select('*')->from(TBL_DELETED_COMPANIES)->orderBy('dtc')->desc();
        foreach ($where as $key => $val) {
            $result->where($key . '=%i', $val);
        }
        if ($limit !== NULL) $result->limit($limit);
        if ($offset !== NULL) $result->offset($offset);

        $feedbacks = $result->execute()->fetchAssoc('deletedCompanyId');
        return $feedbacks;
    }

    /**
     * @param $companyId
     * @return mixed
     */
    public static function getDeletedCompanyById($companyId)
    {
        $result = FApplication::$db->select('*')->from(TBL_DELETED_COMPANIES)->orderBy('dtc')->desc();
        $result->where('companyId=%i', $companyId);
        $result->limit(1);
        $company = $result->execute()->fetch('deletedCompanyId');
        return $company;
    }

    /**
     * @param $companyNumber
     * @return mixed
     */
    public static function isADeletedCompany($companyNumber)
    {
        $result = FApplication::$db->select('*')->from(TBL_DELETED_COMPANIES)->orderBy('dtc')->desc();
        $result->where('companyNumber=%i', $companyNumber);
        $result->limit(1);
        $company = $result->execute()->fetch('deletedCompanyId');
        return $company;
    }

    /**
     * Returns company informations as array
     *
     * @return array <DibiRow>
     */
    public static function isOk2Restore($companyNumber)
    {
        $companyInformations = dibi::select('company_id,customer_id, deleted ')
            ->from('ch_company', 'c')
            ->where('c.company_number=%s', $companyNumber)
            ->execute()->fetch();
        if (empty($companyInformations)) {
            return FALSE;
        } else {
            return $companyInformations;
        }
    }

    /**
     * Update ch_company table if a company was restored
     * @param $companyId
     * @param $companyNumber
     */
    public static function restoreDeletedCompany($companyId, $companyNumber)
    {

        CHFiling::getDb()->update('ch_company', array('deleted' => FALSE, 'company_number' => $companyNumber), 'company_id = ' . $companyId);

    }
}

