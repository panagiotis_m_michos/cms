<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    MatrixServicesModel.php 2012-03-13 razvanp@madesimplegroup.com
 */

class MatrixServicesModel extends FNode
{
    
    /**
     * Matrix Pakages Nodes
     * @var array  
     */
    protected $serviceAvailableForPakage = array();
    
    /**
     * @var string 
     */
    protected $summaryPakageName;
    
    
    protected $matrixPakageNrOfYears;
    protected $packageTooltipText;
    /**
     * @var int 
     */
    protected $tickImage;
    
    /**
     * @var string 
     */
    protected $firstText;
    
    /**
     * @var string 
     */
    protected $seccondText;
    
    
    
    /**
     *
     * @param string
     */
    public function setTickImage($tickImage)
    {
        $this->tickImage = $tickImage;
    }
    
    /**
     *
     * @return string 
     */
    public function getTickImage()
    {
        return $this->tickImage;
    }
    
    /**
     *
     * @param string
     */
    public function setFirstText($firstText)
    {
        $this->firstText = $firstText;
    }
    
    /**
     *
     * @return string 
     */
    public function getFirstText()
    {
        return $this->firstText;
    }
    
    /**
     *
     * @param string
     */
    public function setSeccondText($seccondText)
    {
        $this->seccondText = $seccondText;
    }
    
    /**
     *
     * @return string 
     */
    public function getSeccondText()
    {
        return $this->seccondText;
    }
    
    /**
     *
     * @param string
     */
    public function setSummaryPakageName($summaryPakageName)
    {
        $this->summaryPakageName = $summaryPakageName;
    }
    
    /**
     *
     * @return string 
     */
    public function getSummaryPakageName()
    {
        return $this->summaryPakageName;
    }
    
    
    /**
     *
     * @param string
     */
    public function setPackageTooltipText($packageTooltipText)
    {
        $this->packageTooltipText = $packageTooltipText;
    }
    
    /**
     *
     * @return string 
     */
    public function getPackageTooltipText()
    {
        return $this->packageTooltipText;
    }
    
    
    /**
     *
     * @param string
     */
    public function setMatrixPakageNrOfYears($matrixPakageNrOfYears)
    {
        $this->matrixPakageNrOfYears = $matrixPakageNrOfYears;
    }
    
    /**
     *
     * @return string 
     */
    public function getMatrixPakageNrOfYears()
    {
        return $this->matrixPakageNrOfYears;
    }
    
    /**
     *
     * @param array $matrixPakages 
     */
    public function setServiceAvailableForPakage($serviceAvailableForPakage)
    {
        $this->serviceAvailableForPakage = $serviceAvailableForPakage;
    }
    
    /**
     *
     * @return array 
     */
    public function getServiceAvailableForPakage()
    {
        return $this->serviceAvailableForPakage;
    }
    
    
    /**
	 * Returns Matrix packages
	 * @param boolean $returnAsObjects
	 * @return array
	 */
	public function getAllPackageServices()
	{
		$packages = array();
		foreach (FNode::getChildsNodes(MatrixServicesAdminControler::MATRIX_PAKAGE_SERVICE_FOLDER) as $key=>$val) {
				$packages[$key] = FProperty::getNodePropsValues($key) + array('page'=>$val);
//                /pr($val);
		}
		return $packages;
	}
    
    /**
	 * Returns Matrix packages
	 * @param boolean $returnAsObjects
	 * @return array
	 */
	static public function getAllPackageOffers()
	{
		$packages = array();
		foreach (FNode::getChildsNodes(MatrixServicesAdminControler::MATRIX_PAKAGE_OFFERS_FOLDER) as $key=>$val) {
				$packages[$key] = FProperty::getNodePropsValues($key) + array('page'=>$val);
		}
		return $packages;
	}
    
    
    /*     * ********************************************* ADMIN ********************************************** */

    /**
     * Provides adding custom data
     * 
     * @return void 
     */
    public function addCustomData()
    {
        $this->serviceAvailableForPakage = json_decode($this->getProperty('serviceAvailableForPakage'));
        $this->summaryPakageName = $this->getProperty('summaryPakageName');
        $this->packageTooltipText = $this->getProperty('packageTooltipText');
        $this->matrixPakageNrOfYears = $this->getProperty('matrixPakageNrOfYears');
        $this->tickImage = $this->getProperty('tickImage');
        $this->firstText = $this->getProperty('firstText');
        $this->seccondText = $this->getProperty('seccondText');
        parent::addCustomData();
    }

    /**
     * Provides saving custom data
     *
     * @param string $action
     * @return void
     */
    public function saveCustomData($action)
    {
        $this->saveProperty('serviceAvailableForPakage', json_encode($this->serviceAvailableForPakage));
        $this->saveProperty('summaryPakageName', $this->summaryPakageName);
        $this->saveProperty('packageTooltipText', $this->packageTooltipText);
        $this->saveProperty('matrixPakageNrOfYears', $this->matrixPakageNrOfYears);
        $this->saveProperty('tickImage', $this->tickImage);
        $this->saveProperty('firstText', $this->firstText);
        $this->saveProperty('seccondText', $this->seccondText);
        parent::saveCustomData($action);
    }
    
  
}