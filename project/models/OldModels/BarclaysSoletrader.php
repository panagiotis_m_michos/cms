<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS 
 * @author 		Stan (diviak@gmail.com)
 * @internal 	project/models/Signups.php
 * @created 	17/08/2010
 */

class BarclaysSoletrader extends Object
{
	/** @var int */
	public $barclaysSoletraderId;

	/** @var int */
	public $companyName;

	/** @var int */
	public $preferredContactDate;

	/** @var int */
	public $establishedDate;

	/** @var string */
	public $email;

	/** @var string */
	public $titleId;

	/** @var string */
	public $firstName;

	/** @var string */
	public $lastName;

	/** @var string */
	public $address1;

	/** @var string */
	public $address2;

	/** @var string */
	public $address3;

	/** @var string */
	public $city;

	/** @var string */
	public $county;

	/** @var string */
	public $postcode;

	/** @var string */
	public $countryId;

	/** @var string */
	public $phone;

	/** @var string */
	public $additionalPhone;

	/** @var string */
	public $dtc;

	/**
	 * @var string
	 */
	public $dtm;

	/**
	 * @return int
	 */
	public function getId() { return( $this->barclaysSoletraderId ); }

	/**
	 * @return bool
	 */
	public function isNew() { return( $this->barclaysSoletraderId == 0 ); }

	/**
	 * Conctruct
	 *
	 * @param int $barclaysSoletraderId
	 * @return void
	 * @throws Exception
	 */
	public function __construct($barclaysSoletraderId = 0)
	{
		$this->barclaysSoletraderId = (int)$barclaysSoletraderId;
		if ($this->barclaysSoletraderId) {
			$w = FApplication::$db->select('*')->from(TBL_BARCLAYS_SOLETRADER)->where('barclaysSoletraderId=%i', $this->barclaysSoletraderId)->execute()->fetch();
			if ($w) {
				$this->preferredContactDate = $w['preferredContactDate'];
				$this->barclaysSoletraderId = $w['barclaysSoletraderId'];
				$this->companyName = $w['companyName'];
				$this->establishedDate = $w['establishedDate'];
				$this->email = $w['email'];
				$this->titleId = $w['titleId'];
				$this->firstName = $w['firstName'];
				$this->lastName = $w['lastName'];
				$this->address1 = $w['address1'];
				$this->address2 = $w['address2'];
				$this->address3 = $w['address3'];
				$this->city = $w['city'];
				$this->county = $w['county'];
				$this->postcode = $w['postcode'];
				$this->countryId = $w['countryId'];
				$this->phone = $w['phone'];
				$this->additionalPhone = $w['additionalPhone'];
				$this->dtc = $w['dtc'];
                $this->dtm = $w['dtm'];
			} else {
			    throw new Exception("barclaysSoletrader with id `$barclaysSoletraderId` doesn't exist.");
			}
		}
	}


	/**
	 * Provides saving barclaysSoletrader
	 *
	 * @return int
	 */
	public function save()
	{
		$action = $this->barclaysSoletraderId ? 'update' : 'insert';

		$data = array();
		$data['preferredContactDate'] = date('Y-m-d',strtotime('+'.$this->preferredContactDate.'day' ));
        $data['companyName'] = $this->companyName;
        $data['establishedDate'] = $this->establishedDate;
		$data['email'] = $this->email;
		$data['titleId'] = $this->titleId;
		$data['firstName'] = $this->firstName;
		$data['lastName'] = $this->lastName;
		$data['address1'] = $this->address1;
		$data['address2'] = $this->address2;
		$data['address3'] = $this->address3;
		$data['city'] = $this->city;
		$data['county'] = $this->county;
		$data['postcode'] = $this->postcode;
		$data['countryId'] = $this->countryId;
		$data['phone'] = $this->phone;
		$data['additionalPhone'] = $this->additionalPhone;
        $data['dtm'] = new DibiDateTime();
		// insert
		if ($action == 'insert'){
			$data['dtc'] = new DibiDateTime();
			$this->barclaysSoletraderId = FApplication::$db->insert(TBL_BARCLAYS_SOLETRADER, $data)->execute(dibi::IDENTIFIER);
		// update
		} else {
			FApplication::$db->update(TBL_BARCLAYS_SOLETRADER, $data)->where('barclaysSoletraderId=%i', $this->barclaysSoletraderId)->execute();
		}
		return $this->barclaysSoletraderId;
	}

	/**
	 * Provides deleting barclaysSoletrader
	 *
	 * @return void
	 */
	public function delete()
	{
		FApplication::$db->delete(TBL_BARCLAYS_SOLETRADER)->where('barclaysSoletraderId=%i', $this->barclaysSoletraderId)->execute();
	}
	
	/**
	 * Returns count of signups
	 *
	 * @param array $where
	 * @return int
	 */
	static public function getCount(array $where = array())
	{
		$result = dibi::select('count(*)')->from(TBL_BARCLAYS_SOLETRADER);
		foreach ($where as $key => $val) $result->where('%n', $key, ' = %s',$val);
		$count = $result->execute()->fetchSingle();
		return $count;
	}
	
	/**
	 * Returns all signups
	 *
	 * @param int $limit
	 * @param int $offset
	 * @param array $where
	 * @return int
	 */
	static public function getAll($limit = NULL, $offset = NULL, array $where = array())
	{
		$result = dibi::select('*')->from(TBL_BARCLAYS_SOLETRADER)->orderBy('dtc', dibi::DESC);
		if ($limit !== NULL) $result->limit($limit);
		if ($offset !== NULL) $result->offset($offset);
		foreach ($where as $key => $val) $result->where('%n', $key, ' = %s',$val);
		return $result->execute()->fetchAssoc('barclaysSoletraderId');
	}
	
	/**
	 * Returns result as objects
	 *
	 * @param int $limit
	 * @param int $offset
	 * @param array $where
	 * @return array<MrSiteCode>
	 */
	static public function getAllObjects($limit = NULL, $offset = NULL, array $where = array())
	{
		$objects = array();
		$all = self::getAll($limit, $offset, $where);
		foreach ($all as $key => $val) {
			$objects[$key] = new self($key);		
		}
		return $objects;
	}
	
	
}