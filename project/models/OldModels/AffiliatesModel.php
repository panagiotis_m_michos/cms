<?php

class AffiliatesModel extends FNode
{

    /**
     * @var Affiliate
     */
    private $affiliate;

    /**
     * @param int $affiliateId
     */
    public function startup($affiliateId)
    {
        $this->affiliate = new Affiliate($affiliateId);
    }

    /**
     * @return Affiliate
     */
    public function getAffiliate()
    {
        return $this->affiliate;
    }

    /**
     * @return FPaginator2
     */
    public function getAffiliatesPaginator()
    {
        $paginator = new FPaginator2(Affiliate::getCount());
        $paginator->htmlDisplayEnabled = FALSE;

        return $paginator;
    }

    /**
     * @param FPaginator2 $paginator
     * @return DibiRow[]
     */
    public function getAffiliates(FPaginator2 $paginator)
    {
        return Affiliate::getAll($paginator->getLimit(), $paginator->getOffset());
    }

    /**
     * @return FPaginator2
     */
    public function getAffiliateCustomersPaginator()
    {
        $paginator = new FPaginator2($this->affiliate->getAffiliateCustomersCount());
        $paginator->htmlDisplayEnabled = FALSE;
        $paginator->itemsPerPage = 25;

        return $paginator;
    }

    /**
     * @param FPaginator2 $paginator
     * @return DibiRow[]
     */
    public function getAffiliateCustomers(FPaginator2 $paginator)
    {
        return $this->affiliate->getAffiliateCustomers($paginator->getLimit(), $paginator->getOffset());
    }

    /**
     * @param FForm $filter
     * @return FPaginator2
     */
    public function getAffiliateOrdersPaginator(FForm $filter)
    {
        $data = $filter->getValues();
        $paginator = new FPaginator2($this->affiliate->getAffiliateOrdersCount($data['dtFrom'], $data['dtTill']));
        $paginator->htmlDisplayEnabled = FALSE;
        $paginator->itemsPerPage = 25;

        return $paginator;
    }

    /**
     * @param FForm $filter
     * @param FPaginator2 $paginator
     * @return DibiRow[]
     */
    public function getAffiliateOrders(FForm $filter, FPaginator2 $paginator = NULL)
    {
        $data = $filter->getValues();
        if ($paginator !== NULL) {
            $orders = $this->affiliate->getAffiliateOrders(
                $paginator->getLimit(),
                $paginator->getOffset(),
                $data['dtFrom'],
                $data['dtTill']
            );
        } else {
            $orders = $this->affiliate->getAffiliateOrders(NULL, NULL, $data['dtFrom'], $data['dtTill']);
        }

        return $orders;
    }

    /**
     * @param array $callback
     * @param string $action - enum(create|edit)
     * @return FForm
     */
    public function getComponentAffiliateForm(array $callback, $action)
    {
        $form = new FForm('affiliate' . $action);

        $form->addFieldset('Data');
        $form->addSelect('statusId', 'Status *', Affiliate::$statuses)
            ->setFirstOption('--- Select ---')
            ->addRule(FForm::Required, 'Please provide status');
        $form->addText('hash', 'Hash *')
            ->size(40)
            ->addRule(FForm::Required, 'Please provide hash');
        $form->addText('name', 'Name *')
            ->size(40)
            ->addRule(FForm::Required, 'Please provide name');
        $form->addText('email', 'Email')
            ->size(40)
            ->addRule(FForm::Email, 'Email is not valid');
        $form->addText('phone', 'Phone')
            ->size(40);
        $form->addText('web', 'Web *')
            ->size(40)
            ->addRule(FForm::Required, 'Please provide web');

        $form->addFieldset('Colours');
        $form->addText('companyColour', 'Company colour *')
            ->size(40)
            ->addRule(FForm::Required, 'Please provide company colour');
        $form->addText('textColour', 'Text colour *')
            ->size(40)
            ->addRule(FForm::Required, 'Please provide text colour');

        $form->addFieldset('Services');
        $form->add('AsmSelect', 'servicesIds', 'Services:')
            ->title('--- Select --')
            ->setOptions(Affiliate::$services);

        $form->addFieldset('White Label');
        $form->add('AsmSelect', 'packagesIds', 'Products:')
            ->title('--- Select --')
            ->setOptions(BasketProduct::getAllProducts())
            ->addRule([$this, 'Validator_requiredProducts'], 'Required');
        $form->add('FckArea', 'textbox', 'Addition textbox');

        $form->addFieldset('LTD Calculator Widget');
        $form->addText('googleAnalyticsTrackingTag', 'Tracking Tag')
            ->size(40)
            ->setDescription('(Used to identify which site the user is referred from (via Analytics). Eg. Domain name.)')
            ->addRule([$this, 'Validator_requiredTrackingTag'], 'Required');

        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Submit')->class('btn');

        if ($action === 'edit') {
            $form->setInitValues((array) $this->affiliate);
        }
        $form->onValid = $callback;
        $form->start();

        return $form;
    }

    /**
     * @param AsmSelect $control
     * @param string $error
     * @param array $params
     * @return bool|string
     */
    public function Validator_requiredProducts(AsmSelect $control, $error, array $params)
    {
        $value = $control->getValue();
        $services = $control->owner['servicesIds']->getValue();
        if (is_array($services) && in_array('whiteLabel', $services) && empty($value)) {
            return $error;
        }

        return TRUE;
    }

    /**
     * @param AsmSelect|Text $control
     * @param string $error
     * @param array $params
     * @return bool|string
     */
    public function Validator_requiredTrackingTag(Text $control, $error, array $params)
    {
        $value = $control->getValue();
        $services = $control->owner['servicesIds']->getValue();
        if (is_array($services) && in_array('widgetCalculator', $services) && empty($value)) {
            return $error;
        }

        return TRUE;
    }

    /**
     * @param FForm $form
     * @param string $action - enum(create|edit)
     * @return int
     */
    public function processSaveAffiliate(FForm $form, $action)
    {
        $data = $form->getValues();

        $affiliate = ($action === 'create') ? new Affiliate() : $this->affiliate;
        $affiliate->statusId = $data['statusId'];
        $affiliate->hash = $data['hash'];
        $affiliate->name = $data['name'];
        $affiliate->email = $data['email'];
        $affiliate->phone = $data['phone'];
        $affiliate->web = $data['web'];
        $affiliate->packagesIds = $data['packagesIds'];
        $affiliate->companyColour = $data['companyColour'];
        $affiliate->textColour = $data['textColour'];
        $affiliate->servicesIds = $data['servicesIds'];
        $affiliate->googleAnalyticsTrackingTag = $data['googleAnalyticsTrackingTag'];
        $affiliate->textbox = $data['textbox'];
        $affiliateId = $affiliate->save();

        $this->saveAffiliateProducts($data['packagesIds'], $affiliateId);

        $form->clean();

        return $affiliateId;
    }

    /**
     * @return FForm
     */
    public function getComponentReportFilterForm()
    {
        $form = new FForm('affiliateReport', 'get');
        $form->add('DatePicker', 'dtFrom', 'Date:')
            ->setValue(self::getYesterdayDate());
        $form->add('DatePicker', 'dtTill', '-')
            ->setValue(self::getYesterdayDate());
        $form->addSubmit('submit', 'Go')
            ->class('btn');
        $form->start();

        return $form;
    }

    /**
     * @return FForm
     */
    public function getComponentOrdersFilterForm()
    {
        $form = new FForm('affiliateOrders', 'get');
        $form->add('DatePicker', 'dtFrom', 'Date:')
            ->setValue(self::getYesterdayDate());
        $form->add('DatePicker', 'dtTill', '-')
            ->setValue(self::getYesterdayDate());
        $form->addSubmit('submit', 'Go')
            ->class('btn');
        $form->clean();
        $form->start();

        return $form;
    }

    /**
     * @return string
     */
    public static function getYesterdayDate()
    {
        return date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') - 1, date('Y')));
    }

    public function outputAffiliateCustomersCSV()
    {
        $customers = $this->getAffiliate()->getAffiliateCustomers();
        $header = array_keys((array) current($customers));
        array_unshift($customers, $header);
        Array2Csv::output($customers, 'affiliateCustomers.csv');
    }

    /**
     * @param FForm $filter
     */
    public function outputAffiliateOrdersCSV(FForm $filter)
    {
        $orders = $this->getAffiliateOrders($filter);
        $header = array_keys((array) current($orders));
        array_unshift($orders, $header);
        Array2Csv::output($orders, 'affiliateOrders.csv');
    }

    /**
     * @throws Exception
     */
    public function outputAffiliateCustomersPublicCSV()
    {
        $customers = $this->getAffiliate()->getAffiliateCustomers();

        if (empty($customers)) {
            throw new Exception('No data to export !');
        }
        foreach ($customers as $key => $customer) {
            unset($customer['customerId']);
            unset($customer['statusId']);
            unset($customer['roleId']);
            unset($customer['tagId']);
            unset($customer['icaewId']);
            unset($customer['password']);
            unset($customer['titleId']);
            unset($customer['additionalPhone']);
            unset($customer['isSubscribed']);
            unset($customer['howHeardId']);
            unset($customer['industryId']);
            unset($customer['credit']);
            unset($customer['affiliateId']);
            unset($customer['dtc']);
            unset($customer['dtm']);
            $cleanCustomers[$key] = $customer;
        }
        $header = array_keys((array) current($cleanCustomers));
        array_unshift($cleanCustomers, $header);

        Array2Csv::output($cleanCustomers, 'affiliateCustomers.csv');
    }

    /**
     * @param FForm $filter
     * @throws Exception
     */
    public function outputAffiliateOrdersPublicCSV(FForm $filter)
    {
        $orders = $this->getAffiliateOrders($filter);
        if (empty($orders)) {
            throw new Exception('No data to export !');
        }
        foreach ($orders as $key => $order) {
            unset($order['customerId']);
            unset($order['voucherId']);
            unset($order['voucherName']);
            unset($order['description']);
            unset($order['statusId']);
            unset($order['isRefunded']);
            unset($order['refundValue']);

            $cleanOrder[$key] = $order;
        }
        $header = array_keys((array) current($cleanOrder));
        array_unshift($cleanOrder, $header);
        Array2Csv::output($cleanOrder, 'affiliateOrders.csv');
    }

    /**
     * @param array $productIds
     * @param int $affiliateId
     */
    public function saveAffiliateProducts($productIds, $affiliateId)
    {
        $packagesIds = $productIds;
        if (is_array($packagesIds)) {
            $affiliateProducts = AffiliateProduct::getAffiliateProducts($affiliateId);

            //delete all mark ups which are not currently provided
            foreach ($affiliateProducts as $affiliateProduct) {
                $index = array_search($affiliateProduct->productId, $packagesIds);
                if ($index === FALSE) {
                    $affiliateProduct->delete();
                } else {
                    unset($packagesIds[$index]);
                }
            }

            //create mark ups from package ids provided
            foreach ($packagesIds as $productId) {
                $affiliateProduct = new AffiliateProduct();
                $affiliateProduct->affiliateId = $affiliateId;
                $affiliateProduct->productId = $productId;
                $affiliateProduct->markUp = 0.00;
                $affiliateProduct->save();
            }
        }
    }

    /**
     * @return FForm
     */
    public function getComponentAnalyticsFilterForm()
    {
        $form = new FForm('affiliateOrders', 'get');
        $form->add('DatePicker', 'dtFrom', 'Date:')
            ->setValue(self::getYesterdayDate());
        $form->add('DatePicker', 'dtTill', '-')
            ->setValue(self::getYesterdayDate());
        $form->addSubmit('submit', 'Go')
            ->class('btn');
        $form->clean();
        $form->start();

        return $form;
    }

    /**
     * @param FForm $filter
     * @throws Exception
     */
    public function outputAffiliateAnalyticsCSV(FForm $filter)
    {
        $reports = $this->getAffiliateAnalytics($filter);
        if (empty($reports)) {
            throw new Exception('No data to export !');
        }
        foreach ($reports as $key => $report) {
            $cleanReport[$key]['Source'] = $report->getSource();
            $cleanReport[$key]['Medium'] = $report->getMedium();
            $cleanReport[$key]['Visits'] = $report->getVisits();
            $cleanReport[$key]['TransactionRevenue'] = $report->getTransactionRevenue();
            $cleanReport[$key]['Transactions'] = $report->getTransactions();
            //$cleanReport[$key]['UniquePurchases'] = $report->getUniquePurchases();
        }
        $header = array_keys((array) current($cleanReport));
        array_unshift($cleanReport, $header);
        Array2Csv::output($cleanReport, 'affiliatereports.csv');
    }

    /**
     * @param FForm $filter
     * @return DibiRow[]
     */
    public function getAffiliateAnalytics(FForm $filter)
    {
        $data = $filter->getValues();
        $analyticsFilter = 'medium =@ affiliate && source==' . $this->affiliate->web;
        $analytics = new Gapi(
            FApplication::$config['google_services']['service_account_name'],
            FApplication::$config['google_services']['key_file_path']
        );
        $analytics->requestReportData(
            Affiliate::$analyticsReportId,
            Affiliate::$analyticsDimensions,
            Affiliate::$analyticsMetrics,
            Affiliate::$analyticsSort,
            $analyticsFilter,
            $data['dtFrom'],
            $data['dtTill']
        );

        return $analytics->getResults();
    }

    /**
     * @param FForm $filter
     * @return DibiRow[]
     */
    public function getAffiliateSoleVLtdReport(FForm $filter)
    {
        $data = $filter->getValues();
        $analyticsFilter = 'medium =@ affiliate-solevltd && source==' . $this->affiliate->googleAnalyticsTrackingTag;
        $analytics = new Gapi(
            FApplication::$config['google_services']['service_account_name'],
            FApplication::$config['google_services']['key_file_path']
        );
        $analytics->requestReportData(
            Affiliate::$analyticsReportId,
            Affiliate::$analyticsDimensions,
            Affiliate::$analyticsMetrics,
            Affiliate::$analyticsSort,
            $analyticsFilter,
            $data['dtFrom'],
            $data['dtTill']
        );

        return $analytics->getResults();
    }
}
