<?php

class Reminder
{

    /**
     *
     * @var string company number in /[A-Z0-9]{8}/i format
     */
    private $companyNumber;

    /**
     *
     * @var String in YYYY-MM-DD format
     */
    private $accountsDueDate;

    /**
     *
     * @var String in YYYY-MM-DD format
     */
    private $returnsDueDate;

    /**
     *
     * @var String the date when the last reminder has been sent
     */
    private $accountsLastSentDate;

    /**
     *
     * @var String the date when the last reminder has been sent
     */
    private $returnsLastSentDate;

    /**
     *
     * @var String email address to which email reminder is send
     */
    private $emailAddress;

    /**
     *
     * @var boolean
     */
    private $active;

    /**
     *
     * @var int timestamp, how long before the due date reminder should be sent
     */
    private static $reminderStartPeriod = -86400; // month and a day time stamp

    /**
     *
     * @var int timestamp, how long after the due date reminder should be sent
     */
    private static $reminderEndPeriod = 2716143; // one day backwards timestamp

    /**
     * use getXmlGateway method, it makes sure that this variable is
     * instanciated
     *
     * @var CHXmlGateway
     */
    private static $gateway;

    /**
     *
     * @var String table name that backs this class
     */
    private static $tableName = "ch_reminder";

    public function __construct($companyNumber) 
    {

        $result = dibi::query('SELECT * FROM ' . self::$tableName . ' WHERE company_number  = %s', $companyNumber)->fetch();

        if (!$result) { // nothing in db
            $this->companyNumber = $companyNumber;
            $this->accountsDueDate = NULL;
            $this->returnsDueDate = NULL;
            $this->accountsLastSentDate = NULL;
            $this->returnsLastSentDate = NULL;
            $this->emailAddress = NULL;
            $this->active = 0;
        } else {        // result from db
            $this->companyNumber = $companyNumber;
            $this->accountsDueDate = is_null($result['accounts_due_date']) ? NULL
                    : strtotime($result['accounts_due_date']);
            $this->returnsDueDate = is_null($result['returns_due_date']) ? NULL
                    : strtotime($result['returns_due_date']);
            $this->accountsLastSentDate = is_null($result['accounts_last_sent_date']) ? NULL
                    : strtotime($result['accounts_last_sent_date']);
            $this->returnsLastSentDate = is_null($result['returns_last_sent_date']) ? NULL
                    : strtotime($result['returns_last_sent_date']);
            $this->emailAddress = $result['email_address'];
            $this->active = (boolean) $result['active'];
        }
    }

    /**
     *
     * @return CHXmlGateway
     */
    public static function getXmlGateway() 
    {

        if (!isset(self::$gateway)) {
            self::$gateway = new CHXmlGateway();
        }

        return self::$gateway;
    }

    /**
     * 
     * @param String    timestamp of the due date to be checked if it is 
     *                  within the specified period
     * @return boolean  indicates if reminder is within the period, if it can 
     *                  be sent to the customer 
     */
    public static function isReminderReady($timeStamp) 
    {

        $period = $timeStamp - time();

        if ($period > (self::$reminderStartPeriod) 
            && $period < (self::$reminderEndPeriod)
        ) {

            return TRUE;
        }

        return FALSE;
    }

    /**
     * returns array of timestamps representing due dates
     *
     * @param string $companyNumber in /[A-Z0-9]{8}/i format
     * @return array    key is string accountsDueDate|returnsDueDate and value
     *                  is timestamp representing the due date
     *
     */
    public static function getDueDates($companyNumber) 
    {

        $gateway = self::getXmlGateway();
        $companyDetails = $gateway->getCompanyDetails($companyNumber);
        $xml = $gateway->getResponse($companyDetails);
        $xml = simplexml_load_string($xml);

        $dueDates = array();

        if (isset($xml->Body->CompanyDetails->CompanyName) 
            && !empty($xml->Body->CompanyDetails->CompanyName)
        ) {

            $dueDates['name'] = (string) $xml->Body->CompanyDetails->CompanyName;
        }
        if (isset($xml->Body->CompanyDetails->Accounts->NextDueDate) 
            && !empty($xml->Body->CompanyDetails->Accounts->NextDueDate)
        ) {

            $accounts = (string) $xml->Body->CompanyDetails->Accounts->NextDueDate;
            $dueDates['accountsDueDate'] = strtotime($accounts);
        }
        if (isset($xml->Body->CompanyDetails->Returns->NextDueDate) 
            && !empty($xml->Body->CompanyDetails->Returns->NextDueDate)
        ) {

            $returns = (string) $xml->Body->CompanyDetails->Returns->NextDueDate;
            $dueDates['returnsDueDate'] = strtotime($returns);
        }

        return $dueDates;
    }

    /**
     * Returns array representation of companies.
     *
     * @return array key is company number and the value is email address.
     */
    public static function getCompanies() 
    {

        return dibi::query(
            'SELECT company_number, email_address FROM ' .
            self::$tableName .
            ' WHERE %and', array(
                    array('active = %i', 1),
                    array('email_address IS NOT NULL'))
        )->fetchPairs();
    }

    /**
     * creates new tables for reminder
     */
    public static function updateReminders() 
    {

        /* get all companies without reminder */
        $companies = dibi::query(
            'SELECT comp.company_number, cust.email
                FROM ch_company as comp
                JOIN cms2_customers as cust ON comp.customer_id = cust.customerId
                LEFT JOIN ch_reminder as reminder ON reminder.company_number = comp.company_number
                WHERE cust.roleId = \'wholesale\' 
                AND comp.company_number IS NOT NULL
                AND reminder.company_number IS NULL'
        )->fetchPairs();

        /* insert all companies */
        foreach($companies as $k => $v) {
            $active = ($v == 'adriank@westbury.co.uk') ? 0 : 1; // adrian koe doesn't want reminder
            dibi::query('INSERT INTO ' . self::$tableName, array('company_number' => $k, 'email_address' => $v, 'active' => $active));
        }
    }

    /**
     * Send reminders
     *
     * @param FEmail
     */
    public static function sendReminders(FEmail $returnsEmail, FEmail $accountsEmail) 
    {

        /* update reminders */
        self::updateReminders();  // TODO - uncomment, to refresh companies

        /* get all companies with active reminder */
        $companies = self::getCompanies();

        foreach($companies as $companyNumber => $emailAddress) {

            /* check if reminders should be sent */
            $dueDates = self::getDueDates($companyNumber);

            $companyName = (isset($dueDates['name'])) ? $dueDates['name'] : '';

            /* values to be updated in db */
            $vals = array();
            $date = date('Y-m-d H:i:s');    // now()

            if (isset($dueDates['accountsDueDate'])) {
                $vals['accounts_due_date'] = date('Y-m-d H:i:s', $dueDates['accountsDueDate']);
                if (self::isReminderReady($dueDates['accountsDueDate'])) {
                    //                    $accEmail = clone $accountsEmail;
                    //
                    //                    $accPlaceholders = array(
                    //                        '[COMPANY_NAME]' => $companyName,
                    //                        '[ACCOUNTS]' => date('d/m/Y', $dueDates['accountsDueDate']));
                    //                    $accEmail->addTo($emailAddress);
                    //                    $accEmail->replacePlaceHolders($accPlaceholders);
                    //                    $accEmail->send();

                    $vals['accounts_last_sent_date'] = $date;
                }
            }

            if (isset($dueDates['returnsDueDate'])) {
                $vals['returns_due_date'] = date('Y-m-d H:i:s', $dueDates['returnsDueDate']);
                if (self::isReminderReady($dueDates['returnsDueDate'])) {
                    //                    $retEmail = clone $returnsEmail;
                    //
                    //                    $retPlaceholders = array(
                    //                        '[COMPANY_NAME]' => $companyName,
                    //                        '[RETURN]' => date('d/m/Y', $dueDates['returnsDueDate']));
                    //                    $retEmail->addTo($emailAddress);
                    //                    $retEmail->replacePlaceHolders($retPlaceholders);
                    //                    $retEmail->send();

                    $vals['returns_last_sent_date'] = $date;
                }
            }

            if (!empty($vals)) {
                dibi::query('UPDATE ' . self::$tableName . ' SET ', $vals, 'WHERE company_number =%s', $companyNumber);
            }
        }
    }

    /**
     *
     * @return string comapny number /[A-Z0-9]{8}/i format
     */
    public function getCompanyNumber() 
    {
        return $this->companyNumber;
    }

    public function getAccountsDueDate() 
    {

        if (is_null($this->accountsDueDate)) {
            return NULL;
        }
        return date('Y-m-d', $this->accountsDueDate);
    }

    public function getReturnsDueDate() 
    {

        if (is_null($this->returnsDueDate)) {
            return NULL;
        }
        return date('Y-m-d', $this->returnsDueDate);
    }

    public function getAccountsLastSentDate() 
    {

        if (is_null($this->accountsLastSentDate)) {
            return NULL;
        }
        return date('Y-m-d', $this->accountsLastSentDate);
    }

    public function getReturnsLastSentDate() 
    {

        if (is_null($this->returnsLastSentDate)) {
            return NULL;
        }
        return date('Y-m-d', $this->returnsLastSentDate);
    }

    public function getEmailAddress() 
    {
        return $this->emailAddress;
    }

    public function setEmailAddress($emailAddress) 
    {
        $this->emailAddress = $emailAddress;
    }

    public function getActive() 
    {
        return $this->active;
    }

    public function setActive($active) 
    {
        $this->active = (boolean) $active;
    }

    /*
     * inserts new row or update existing record.
     */
    public function save() 
    {

        $vals = array(
            'company_number' => $this->companyNumber,
            'email_address' => $this->emailAddress,
            'active' => (int) $this->active);

        dibi::query(
            'INSERT INTO ' . self::$tableName, $vals,
            'ON DUPLICATE KEY UPDATE email_address=\'' . $this->emailAddress . '\', active=' . (int) $this->active
        );
    }
}
