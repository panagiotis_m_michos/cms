<?php

/**
 * CMS
 * @category   Project
 * @package    CMS
 * @author     Nikolai Senkevich
 * @version    SignupsModel.php 2012-03-14 
 */
class SignupsModel extends FNode
{
    const ATTACHMENT_FILE_PAGE = 753;

    /**
     * @var int
     */
    public $startingBusiness;
    
    /**
     * @var int
     */
    public $savvySurvey;

    /**
     * @return void
     */
    public function addCustomData()
    {
        $this->startingBusiness = $this->getProperty('startingBusiness', SignupsControler::STARTING_BUSINESS);
        $this->savvySurvey = $this->getProperty('savvySurvey', SignupsControler::SAVVY_SURVEY);
    }

    /**
     * @param string $action
     */
    public function saveCustomData($action)
    {
        $this->saveProperty('startingBusiness', $this->startingBusiness, SignupsControler::STARTING_BUSINESS);
        $this->saveProperty('savvySurvey', $this->savvySurvey, SignupsControler::SAVVY_SURVEY);
    }

    /**
     * @return FFile 
     */
    public function getSavvySurveyFile()
    {
        try {
            $file = new FFile($this->savvySurvey);
        } catch (Exception $e) {
            $file = new FFile();
        }
        return $file;
    }
    
    /**
     * @return PDF
     */
    public function outputSavvySurveyPdf()
    {
        Debug::disableProfiler();
        $filePath = $this->savvySurveyFile->getFilePath();
        $out = @file_get_contents($filePath);
        if ($out === FALSE) {
            throw new Exception("Document doesn't exist!");
        }
        
        // --- prepare headers ---
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header('Content-disposition: attachment; filename=' . $this->savvySurveyFile->getFileName());
        header("Content-Type: application/pdf");
        header("Content-Transfer-Encoding: binary");
        //header('Content-Length: ' . strlen($this->savvySurveyFile->getFileName()));
       
        echo $out;
        exit;
    }
    
        /**
     * @return FFile 
     */
    public function getStartingBusinessFile()
    {
        try {
            $file = new FFile($this->startingBusiness);
        } catch (Exception $e) {
            $file = new FFile();
        }
        return $file;
    }
    
    /**
     * @return PDF
     */
    public function outputStartingBusinessPdf()
    {   
        Debug::disableProfiler();
        $filePath = $this->startingBusinessFile->getFilePath();
        $out = @file_get_contents($filePath);
        if ($out === FALSE) {
            throw new Exception("Document doesn't exist!");
        }
        
        // --- prepare headers ---
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header('Content-disposition: attachment; filename=' . $this->startingBusinessFile->getFileName());
        header("Content-Type: application/pdf");
        header("Content-Transfer-Encoding: binary");
        //header('Content-Length: ' . strlen($this->startingBusinessFile->getFileName()));
        echo $out;
        exit;
    }
}