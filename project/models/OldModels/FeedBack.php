<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    FeedbacksForm.php 2011-03-21 razvanp@madesimplegroup.com
 */

class FeedBack extends Object
{
    /**
	 * primary key
	 *
	 * @var int
	 */
	public $feedbackId;

	/**
	 * @var text
	 */
	public $text;

	/**
	 * @var string
	 */
	public $rating;

    /**
	 * @var int
	 */
	public $nodeId;
    /**
	 * @var string
	 */
	public $pageTitle;
    /**
	 * @var string
	 */
	public $url;

    /**
	 * @var string
	 */
	public $dtc;

	/**
	 * @var string
	 */
	public $dtm;

    /**
	 * @return bool
	 */
	public function isNew()
	{
		return ($this->feedbackId == 0);
	}

    /**
	 * @return int
	 */
	public function save()
	{
		$data = array();
		$data['text'] = $this->text;
		$data['rating'] = $this->rating;
		$data['nodeId'] = $this->nodeId;
		$data['pageTitle'] = $this->pageTitle;
		$data['url'] = $this->url;
		$data['dtm'] = new DibiDateTime();
		if ($this->isNew()) {
			$data['dtc'] = new DibiDateTime();
			$this->feedbackId = dibi::insert(TBL_FEEDBACKS, $data)->execute(dibi::IDENTIFIER);
		} 
		return $this->feedbackId;
	}

    /**
	 * Returns count of all records from feedback
	 *
	 * @param array $where
	 * @return int
	 */
	static public function getCount(array $where = array())
	{
        $result = FApplication::$db->select('COUNT(*)')->from(TBL_FEEDBACKS)->orderBy('dtc')->desc();
		foreach ($where as $key=>$val) {
            if($key == 'dtcFrom') {
                $result->where('dtc >=%s', $val);
                continue;
            }
            if($key == 'dtcTo') {
                $result->where('DATE_FORMAT(dtc,"%Y-%m-%d") <=%s', $val);
                continue;
            }
            $val = '%'.$val.'%';
			$result->where('%n', $key, ' LIKE %s', $val);
		}
		$count = $result->execute()->fetchSingle();
		return $count;
	}

    /**
	 * Returns feedbacks as array
	 *
	 * @param int $limit
	 * @param int $offset
	 * @param array $where
	 * @return array<DibiRow>
	 */
	static public function getAll(array $where = array(), $limit = NULL, $offset = NULL)
	{
        $result = FApplication::$db->select('*')->from(TBL_FEEDBACKS)->orderBy('dtc')->desc();
        foreach ($where as $key=>$val) {
			if($key == 'dtcFrom') {
                $result->where('dtc >=%s', $val);
                continue;
            }
            if($key == 'dtcTo') {
                $result->where('DATE_FORMAT(dtc,"%Y-%m-%d") <=%s', $val);
                continue;
            }
            $val = '%'.$val.'%';
			$result->where('%n', $key, ' LIKE %s', $val);
		}
		if ($limit !== NULL) $result->limit($limit);
		if ($offset !== NULL) $result->offset($offset);

		$feedbacks = $result->execute()->fetchAssoc('feedbackId');
        return $feedbacks;
	}

}

