<?php

use Entities\Customer as CustomerEntity;

class CustomerActionLogger extends Object
{
    // statuses
    const LOG_IN = 'LOG_IN';
    const LOG_OUT = 'LOG_OUT';
    const DETAILS_CHANGED = 'DETAILS_CHANGED';

    /**
     * @var <type>
     */
    public static $statuses = array(
        self::LOG_IN => 'Log In',
        self::LOG_OUT => 'Log Out',
        self::DETAILS_CHANGED => 'Details have been changed',
    );


    /**
     *
     * @var int
     */
    private $customerActionLoggerId;

    /**
     *
     * @var int
     */
    private $customerId;

    /**
     *
     * @var string
     */
    private $actionId;

    /**
     *
     * @var string
     */
    private $ip;

    /**
     *
     * @var string
     */
    private $dtc;

    /**
     *
     * @var string
     */
    private $dtm;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->customerActionLoggerId;
    }

    /**
     * @return bool
     */
    public function isNew()
    {
        return ($this->customerActionLoggerId == 0);
    }


    public function getAction()
    {
        return isset(self::$statuses[$this->actionId]) ? self::$statuses[$this->actionId] : NULL;
    }


    /**
     * @param int $customerActionLoggerId
     * @return void
     * @throws Exception
     */
    public function __construct($customerActionLoggerId = 0)
    {
        $this->customerActionLoggerId = (int) $customerActionLoggerId;
        if ($this->customerActionLoggerId) {
            $w = dibi::select('*')->from(TBL_CUSTOMER_LOG)->where(
                'customerActionLoggerId=%i',
                $customerActionLoggerId
            )->execute()->fetch();
            if ($w) {
                $this->customerActionLoggerId = $w['customerActionLoggerId'];
                $this->customerId = $w['customerId'];
                $this->actionId = $w['actionId'];
                $this->ip = $w['ip'];
                $this->dtc = $w['dtc'];
                $this->dtm = $w['dtm'];
            } else {
                throw new Exception("Document with id `$customerActionLoggerId` doesn't exist!");
            }
        }
    }

    /**
     * @return int
     */
    public function save()
    {
        $data = array();
        $data['customerId'] = $this->customerId;
        $data['actionId'] = $this->actionId;
        $data['ip'] = $this->ip;
        $data['dtm'] = new DibiDateTime();

        if ($this->isNew()) {
            $data['dtc'] = new DibiDateTime();
            $this->customerActionLoggerId = dibi::insert(TBL_CUSTOMER_LOG, $data)->execute(dibi::IDENTIFIER);
        } else {
            dibi::update(TBL_CUSTOMER_LOG, $data)->where(
                'customerActionLoggerId=%i',
                $this->customerActionLoggerId
            )->execute();
        }
        return $this->customerActionLoggerId;
    }


    /**
     * @return void
     */
    public function delete()
    {
        dibi::delete(TBL_CUSTOMER_LOG)->where('customerActionLoggerId=%i', $this->customerActionLoggerId)->execute();
    }

    /**
     * Returns customer actions based on params
     *
     * @param int $limit
     * @param int $offset
     * @param array $where
     * @return array<DibiRow>
     */
    public static function getAll($limit = NULL, $offset = NULL, array $where = array())
    {
        $result = dibi::select('*')->from(TBL_CUSTOMER_LOG)->orderBy('dtc', dibi::DESC);

        if ($limit !== NULL) {
            $result->limit($limit);
        }
        if ($offset !== NULL) {
            $result->offset($offset);
        }

        foreach ($where as $key => $val) {
            $result->where('%n', $key, ' = %s', $val);
        }

        return $result->execute()->fetchAssoc('customerActionLoggerId');
    }

    /**
     * Returns result as objects
     *
     * @param unknown_type $limit
     * @param unknown_type $offset
     * @param array $where
     * @return array<Affiliate>
     */
    public static function getAllObjects($limit = NULL, $offset = NULL, array $where = array())
    {
        $objects = array();
        $all = self::getAll($limit, $offset, $where);
        foreach ($all as $key => $val) {
            $objects[$key] = new self($key);
        }
        return $objects;
    }

    /**
     *
     * @param Customer $customer
     * @param string $status
     */
    public static function log(Customer $customer, $status)
    {
        $log = new self();
        $log->setCustomerId($customer->getId());
        $log->setActionId($status);
        $log->setIp($log->getIp());
        $log->save();
    }

    /**
     * @param CustomerEntity $customer
     * @param $status
     */
    public static function logEntity(CustomerEntity $customer, $status)
    {
        $log = new self();
        $log->setCustomerId($customer->getId());
        $log->setActionId($status);
        $log->setIp($log->getIp());
        $log->save();
    }

    //geters
    /**
     *
     * @return string
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     *
     * @return string
     */
    public function getActionId()
    {
        return $this->actionId;
    }

    /**
     *
     * @return string
     */
    public function getIp()
    {
        $this->ip = $_SERVER['REMOTE_ADDR'];
        return $this->ip;
    }

    /**
     * @return string
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @return string
     */
    public function getDtm()
    {
        return $this->dtm;
    }

    //seters


    /**
     *
     * @param string $ip
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }

    /**
     *
     * @param string $actionId
     */
    public function setActionId($actionId)
    {
        $this->actionId = $actionId;
    }

    /**
     *
     * @param string $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    /**
     * @param string $dtc
     */
    private function setDtc($dtc)
    {
        $this->dtc = $dtc;
    }

    /**
     * @param string $dtm
     */
    private function setDtm($dtm)
    {
        $this->dtm = $dtm;
    }


}
