<?php

use Entities\Customer as CustomerEntity;
use Entities\Payment\Token;
use Services\OrderService as OrderEntityService;
use Services\Payment\TokenService;
use Services\ProductService;
use Utils\Date;

class TokensModel extends FNode
{
    /**
     * @var SagePayDirect
     */
    private $sage;

    /**
     * @var Tokens
     */
    private $token;

    public function init()
    {
        $sage = new SagePayDirect();
        $sageConfig = FApplication::$config['sage'];
        $sage->setVendor($sageConfig['vendor']);
        $sage->setSystem($sageConfig['server']);
        $this->sage = $sage;
    }

    //----------------------- Token Generating ------------------------------

    /**
     *
     * @param Customer $customer
     * @param SagePayTokenCard $card
     * @param SagePayAddress $address
     */
    public function getToken(Customer $customer, SagePayTokenCard $card, SagePayAddress $address)
    {

        //get token
        $this->init();
        $tokenSage = new SagePayTokenGenerate();
        $tokenSage->setCard($card);
        $this->sage->setAction($tokenSage);

        $response = $this->sage->doAction();

        if (trim($response['Status']) == 'OK') {
            $tokenDB = new Tokens();
            $tokenDB->customerId = $customer->getId();
            //@TODO why do we have replace_here
            $tokenDB->token = trim(str_replace(array('{', '}'), '', $response['Token']));
            $tokenDB->cardNumber = $card->getCardNumber();

            $tokenDB->firstname = $address->getFirstnames();
            $tokenDB->surname = $address->getSurname();
            $tokenDB->address1 = $address->getLine1();
            $tokenDB->address2 = $address->getLine2();     //optional
            $tokenDB->city = $address->getCity();
            $tokenDB->state = $address->getState();     //optional
            $tokenDB->postCode = $address->getPostcode();
            $tokenDB->country = $address->getCountry();
            //$tokenDB->phone       = $address->getPhone();     //optional

            return $tokenDB;
        } else {
            //            pr($response['StatusDetail']);
            //            die('erreor');
            throw new Exception($response['StatusDetail']);
        }
    }

    //-------------------- Token Removing -----------------------
    /**
     *
     * @param Tokens $tokenDB
     */
    public function sageRemoveToken(Tokens $tokenDB)
    {

        $this->init();
        $tokenSage = new SagePayTokenRemove();

        $tokenSage->setToken($tokenDB->token);
        $this->sage->setAction($tokenSage);

        $response = $this->sage->doAction();
        if (trim($response['Status']) == 'OK') {
            return $response;
        } else {
            throw new Exception($response['StatusDetail']);
        }
    }

    public function removeToken(Tokens $tokenDB)
    {
        $this->sageRemoveToken($tokenDB);
        $tokenDB->delete();
    }

    // ----------------------- Token Payment --------------------------
    /**
     *
     * @param Customer $customer
     * @param Basket $basket
     * @return type
     */
    public function useToken(Customer $customer, Basket $basket)
    {
        $this->getTokenData($customer, $basket);
        $backUrl = FApplication::$router->secureLink(TokensControler::TOKEN_SAGEPAY_3DRESPONCE);
        $response = $this->sage->doAction($backUrl);
        return $response;
    }

    /**
     *
     * @param Customer $customer
     * @param Basket $basket
     * @throws Exception
     */
    public function getTokenData(Customer $customer, Basket $basket)
    {
        if ($this->getCustomerToken($customer) == NULL) {
            throw new Exception('Token not exist');
        }

        $this->init();
        $this->token = $this->getCustomerToken($customer);

        // building token payment
        $tokenPayment = new SagePayTokenPayment();
        $tokenPayment->setToken($this->token->token);
        $tokenPayment->setStoreToken(1);
        $tokenPayment->setApplyAVSCV2(2);
        //$tokenPayment->setApply3DSecure(2);
        $tokenPayment->setAccountType('C');
        $tokenPayment->setBillingAddress(trim($this->token->firstname), $this->token->surname, $this->token->address1, $this->token->city, $this->token->postCode, $this->token->country);

        $tokenPayment->setDeliveryAddress($this->token->firstname, $this->token->surname, $this->token->address1, $this->token->city, $this->token->postCode, $this->token->country);

        if (isset($this->token->address2)) {
            $tokenPayment->getBillingAddress()->setLine2(trim($this->token->address2));
            $tokenPayment->getDeliveryAddress()->setLine2(trim($this->token->address2));
        }

        if (isset($this->token->state) && trim($this->token->country == 'US')) {
            $tokenPayment->getBillingAddress()->setState(trim($this->token->state));
            $tokenPayment->getDeliveryAddress()->setState(trim($this->token->state));
        }

        $product = "";
        foreach ($basket->getItems() as $item) {
            $product.= $item->getLngTitle() . " ";
        }
        $tokenPayment->setPaymentDetails("Cms One Click " . substr($product, 0, 85), $basket->getPrice('total'));
        $this->sage->setAction($tokenPayment);
    }

    /**
     *
     * @param type $customer
     * @return Tokens
     */
    public function getCustomerToken($customer)
    {
        $result = dibi::select('tokenId')->from(TBL_TOKENS)
            ->where('customerId = %i', $customer->customerId)
            ->where('cardExpiryDate >= ?', new Date)
            ->where('sageStatus = %s', Token::SAGE_STATUS_ACTIVE)
            ->fetch('tokenId');
        if (empty($result['tokenId'])) {
            return NULL;
        } else {
            return new Tokens($result['tokenId']);
        }
    }

    // ----------------------- Receiving response and saving to DB--------------------------

    /**
     * @param Customer $customer
     * @param Basket $basket
     * @param array $response
     * @param \Entities\Customer $customerEntity
     * @param \Services\OrderService $orderEntityService
     * @param \Services\ProductService $productService
     */
    public function getTokenAnswer(Customer $customer, Basket $basket, $response, CustomerEntity $customerEntity = NULL, OrderEntityService $orderEntityService = NULL, ProductService $productService = NULL)
    {
        /** @var TokenService $tokenService */
        $tokenService =  Registry::$container->get(DiLocator::SERVICE_TOKEN);
        $token = $tokenService->getTokenByCustomer($customerEntity);
        $paymentService = new PaymentService($customer, $basket, $customerEntity, $orderEntityService, $productService);
        $this->handlePaymentResponse($response);
        $paymentResponse = $this->getPaymentResponse($response);
        $paymentService->completeTransaction($paymentResponse, $token);
    }

    /**
     *
     * @param SageDirect $response
     * @throws Exception
     */
    private function handlePaymentResponse($response)
    {
        if (!empty($response)) {
            if (trim($response['Status']) != 'OK') {
                SagePayDirect::cleanAuthForm();
                if (isset($_SESSION['SagePaymentForm'])) {
                    unset($_SESSION['SagePaymentForm']);
                }
                $msg = trim($response['Status']) . '. ' . trim($response['StatusDetail']);
                if (preg_match('/2000/', $msg)) {
                    throw new Exception($msg . " This could be due to funds not being available, spending limit reached or bank account closure. We advise that you contact your card issuer for more information. Alternatively you can attempt the transaction again using another card.");
                } elseif (preg_match('/2001/', $msg)) {
                    throw new Exception($msg . "This is often due to the billing address, postcode or security code not matching the details held by the card issuer. Please double check all the details you have entered and try again. If you have received this error multiple times we advise that you contact your card issuer for more information.");
                } else {
                    throw new Exception($msg . " If you have any questions regarding this message please contact us +44 (0) 207 608 5500.");
                }
            }
        } else {
            SagePayDirect::cleanAuthForm();
            throw new Exception('ERROR: MSG01. Please contact us on +44 (0) 207 608 5500 or info@madesimplegroup.com');
        }
    }

    /**
     * @param Sage $response
     * @return \PaymentResponse
     */
    private function getPaymentResponse($response)
    {
        $paymentResponse = new PaymentResponse();
        $paymentResponse->setTypeId(Transaction::TYPE_SAGEPAY);
        $sage = unserialize($_SESSION['SagePayDirect']);

        $paymentDetails = $sage->getAction();
        $newCustomerAddress = $paymentDetails->getDeliveryAddress();

        $paymentResponse->setPaymentTransactionTime(date('Y m d H:i:s'));
        $paymentResponse->setPaymentHolderEmail($paymentDetails->getCustomerEMail());
        $paymentResponse->setPaymentHolderFirstName($newCustomerAddress->getFirstnames());
        $paymentResponse->setPaymentHolderLastName($newCustomerAddress->getSurname());
        $paymentResponse->setPaymentHolderAddressStreet($newCustomerAddress->getLine1() . ' ' . $newCustomerAddress->getLine2());
        $paymentResponse->setPaymentHolderAddressCity($newCustomerAddress->getCity());
        $paymentResponse->setPaymentHolderAddressState($newCustomerAddress->getState() ? $newCustomerAddress->getState() : NULL);
        $paymentResponse->setPaymentHolderAddressPostCode($newCustomerAddress->getPostcode());
        $paymentResponse->setPaymentHolderAddressCountry($newCustomerAddress->getCountry());

        $paymentResponse->setPaymentTransactionDetails(serialize($response));
        $paymentResponse->setPaymentCardNumber($this->token->cardNumber);
        $paymentResponse->setPaymentOrderCode(trim($response['VPSTxId']));
        $paymentResponse->setPaymentVpsAuthCode(trim($response['TxAuthNo']));
        $paymentResponse->setPaymentVendorTXCode($_SESSION['VendorTxCode']);

        unset($_SESSION['FForm']['sage_form']);
        unset($_SESSION['SagePayDirect']);
        SagePayDirect::cleanAuthForm();
        return $paymentResponse;
    }

    /**
     *
     * @param Basket $basket
     * @param Customer $customer
     * @return array
     */
    public function getConfirmationPageLinkParams($basket, $customer)
    {
        $linkPackageVariable = array();
        if (!isset($basket) && empty($basket)) {
            return $linkPackageVariable;
        }

        foreach ($basket->items as $item) {
            if ($item instanceof Package && isset(Package::$types[$item->getId()])) {
                if ($customer instanceof Customer) {
                    $orders = $customer->getCustomerOrderCount();
                    if ($orders) {
                        $linkPackageVariable = array('package' => str_replace(' ', '', strtolower(Package::$types[$item->getId()])));
                    } else {
                        $linkPackageVariable = array('packagenew' => str_replace(' ', '', strtolower(Package::$types[$item->getId()])));
                    }
                }
            }
        }

        return $linkPackageVariable;
    }

}
