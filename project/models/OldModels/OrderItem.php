<?php

class OrderItem extends Object
{
    const ADMIN_FEE_PRODUCT = 615;

    /**
     * @var int
     */
    public $orderItemId;

    /**
     * @var int
     */
    public $orderId;

    /**
     * @var boolean
     */
    public $isRefund;

    /**
     * @var boolean
     */
    public $isRefunded;

    /**
     * @var boolean
     */
    public $isFee = FALSE;

    /**
     * @var int
     */
    public $productId;

    /**
     * @var string
     */
    public $productTitle;

    /**
     * @var string
     */
    public $companyName;

    /**
     * @var string
     */
    public $companyNumber;

    /**
     * @var int
     */
    public $qty;

    /**
     * @var double
     */
    public $price;

    /**
     * @var boolean
     */
    public $notApplyVat = FALSE;

    /**
     * @var boolean
     */
    public $nonVatableValue = 0;

    /**
     * @var double
     */
    public $subTotal;

    /**
     * @var double
     */
    public $vat;

    /**
     * @var double
     */
    public $totalPrice;

    /**
     * @var string
     */
    public $additional;

    /**
     * @var boolean
     */
    public $incorporationRequired = FALSE;

    /**
     * @var int
     */
    public $companyId;

    /**
     * @var double
     */
    public $markUp;

    /**
     * @var string
     */
    public $dtc;

    /**
     * @var string
     */
    public $dtm;

    /**
     * @return int
     */
    public function getId()
    {
        return ($this->orderItemId);
    }

    /**
     * @return bool
     */
    public function exists()
    {
        return ($this->orderId > 0);
    }

    /**
     * @return bool
     */
    public function isNew()
    {
        return ($this->orderId == 0);
    }

    /**
     * @param int $orderItemId
     */
    public function __construct($orderItemId = 0)
    {
        $this->orderItemId = (int)$orderItemId;
        if ($this->orderItemId) {
            $w = FApplication::$db
                ->select('*')
                ->from(TBL_ORDERS_ITEMS)
                ->where('orderItemId=%i', $orderItemId)
                ->execute()
                ->fetch();

            if ($w) {
                $this->orderItemId = $w['orderItemId'];
                $this->orderId = $w['orderId'];
                $this->isRefund = (bool)$w['isRefund'];
                $this->isRefunded = (bool)$w['isRefunded'];
                $this->isFee = (bool)$w['isFee'];
                $this->productId = $w['productId'];
                $this->productTitle = $w['productTitle'];
                $this->companyName = $w['companyName'];
                $this->companyNumber = $w['companyNumber'];
                $this->qty = $w['qty'];
                $this->price = $w['price'];
                $this->notApplyVat = (bool)$w['notApplyVat'];
                $this->nonVatableValue = $w['nonVatableValue'];
                $this->subTotal = $w['subTotal'];
                $this->vat = $w['vat'];
                $this->totalPrice = $w['totalPrice'];
                $this->additional = $w['additional'];
                $this->incorporationRequired = (bool)$w['incorporationRequired'];
                $this->companyId = $w['companyId'];
                $this->markUp = $w['markUp'];
                $this->dtc = $w['dtc'];
                $this->dtm = $w['dtm'];
            } else {
                $this->orderItemId = -1 * $this->orderItemId;
            }
        }
    }

    /**
     * Provides save order item to database
     * @return int
     */
    public function save()
    {
        $action = $this->orderItemId ? 'update' : 'insert';

        $data = [];
        $data['orderId'] = $this->orderId;
        $data['isRefund'] = $this->isRefund;
        $data['isRefunded'] = $this->isRefunded;
        $data['isFee'] = $this->isFee;
        $data['productId'] = $this->productId;
        $data['productTitle'] = $this->productTitle;
        $data['companyName'] = $this->companyName;

        // 7 numbers have to lead with zero
        if ($this->companyNumber != NULL) {
            $data['companyNumber'] = sprintf("%'08s", $this->companyNumber);
        }

        $data['qty'] = $this->qty;
        $data['price'] = $this->price;
        $data['notApplyVat'] = $this->notApplyVat;
        $data['nonVatableValue'] = $this->nonVatableValue;
        $data['subTotal'] = $this->subTotal;
        $data['vat'] = $this->vat;
        $data['totalPrice'] = $this->totalPrice;
        $data['additional'] = $this->additional;
        $data['incorporationRequired'] = (bool) $this->incorporationRequired;
        $data['companyId'] = $this->companyId;
        $data['markUp'] = $this->markUp;
        $data['dtm'] = new DibiDateTime();
        // insert
        if ($action == 'insert') {
            $data['dtc'] = new DibiDateTime();
            $this->orderItemId = FApplication::$db->insert(TBL_ORDERS_ITEMS, $data)->execute(dibi::IDENTIFIER);
            // update
        } else {
            FApplication::$db->update(TBL_ORDERS_ITEMS, $data)->where('orderItemId=%i', $this->orderItemId)->execute();
        }

        return $this->orderItemId;
    }

    public function delete()
    {
        if ($this->exists()) {
            FApplication::$db->delete(TBL_ORDERS_ITEMS)->where('orderItemId=%i', $this->orderItemId)->execute();
        }
    }

    /**
     * Returns orders byt params
     * @param int $orderId
     * @param int $limit
     * @param int $offset
     * @return array $orderItems
     */
    public static function getOrderItems($orderId, $limit = NULL, $offset = NULL)
    {
        $result = FApplication::$db
            ->select('orderItemId')
            ->from(TBL_ORDERS_ITEMS)
            ->where('orderId=%i', $orderId)
            ->orderBy('orderItemId', dibi::ASC);

        if ($limit !== NULL) {
            $result->limit($limit);
        }

        if ($offset !== NULL) {
            $result->offset($offset);
        }

        $ids = $result->execute()->fetchPairs();

        $orderItems = [];
        foreach ($ids as $key => $val) {
            $orderItems[$val] = new self($val);
        }

        return $orderItems;
    }
}
