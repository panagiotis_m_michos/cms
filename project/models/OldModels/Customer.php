<?php

use UserModule\Exceptions\CustomerAvailabilityException;

/**
 * @deprecated use Entities\Customer
 */
class Customer extends Object implements IIdentifier
{
    // statuses
    const STATUS_NOT_VALIDATED = 1;
    const STATUS_VALIDATED = 2;
    const STATUS_BLOCKED = 3;
    // error types
    const AUTH_EMAIL_NOT_FOUND = 1;
    const AUTH_PASSWORD_DOESNT_MATCH = 2;
    const AUTH_NOT_VALIDATED = 3;
    const AUTH_BLOCKED = 4;

    /** @var array */
    static public $statuses = Array(
        self::STATUS_NOT_VALIDATED => 'Not validated',
        self::STATUS_VALIDATED => 'Active',
        self::STATUS_BLOCKED => 'Blocked'
    );

    // tags
    const TAG_RETAIL = 'RETAIL';
    const TAG_COSEC = 'COSEC';
    const TAG_AFFILIATE = 'AFFILIATE';
    const TAG_ICAEW = 'ICAEW';
    const TAG_ICAEWWHOLESALE = 'ICAEWWHOLESALE';
    const TAG_GENERALWHOLESALE = 'GENERALWHOLESALE';
    const TAG_TAXASSISTHOLESALE = 'TAXASSISTWHOLESALE';

    /** @var array */
    static public $tags = Array(
        self::TAG_RETAIL => 'Retail',
        self::TAG_COSEC => 'COSEC',
        self::TAG_AFFILIATE => 'Affiliate',
        self::TAG_ICAEW => 'ICAEW',
        self::TAG_ICAEWWHOLESALE => 'ICAEWWHOLESALE',
        self::TAG_GENERALWHOLESALE => 'GENERALWHOLESALE',
        self::TAG_TAXASSISTHOLESALE => 'TAXASSISTWHOLESALE'
    );

    const ROLE_NORMAL = 'normal';
    const ROLE_WHOLESALE = 'wholesale';

    /** @var array */
    static public $roles = array(
        self::ROLE_NORMAL => 'Normal',
        self::ROLE_WHOLESALE => 'Wholesale'
    );
    static public $dates = array(
        0 => 'I\'m ready now',
        7 => 'In 1 week',
        14 => 'In 2 weeks',
        28 => 'In 4 weeks',
    );

    /** @var array */
    static public $titles = array(
        1 => 'Mr',
        2 => 'Miss',
        3 => 'Mrs',
        4 => 'Ms',
        5 => 'Dr',
        6 => 'Prof',
        7 => 'Master',
        8 => 'Rev',
        9 => 'Sir',
        10 => 'Lord',
        11 => 'Lady'
    );

    const UK_CITIZEN = 223;

    /** @var array */
    static public $countries = array(
        223 => "United Kingdom",
        224 => "United States",
        15 => "Austria",
        22 => "Belgium",
        58 => "Denmark",
        74 => "France",
        82 => "Germany",
        106 => "Italy",
        125 => "Luxembourg",
        151 => "Netherlands",
        161 => "Norway",
        172 => "Portugal",
        196 => "Spain",
        204 => "Sweden",
        2 => "Afghanistan",
        3 => "Albania",
        4 => "Algeria",
        5 => "American Samoa",
        6 => "Andorra",
        7 => "Angola",
        8 => "Anguilla",
        9 => "Antarctica",
        10 => "Antigua And Barbuda",
        11 => "Argentina",
        12 => "Armenia",
        13 => "Aruba",
        14 => "Australia",
        16 => "Azerbaijan",
        17 => "Bahamas",
        18 => "Bahrain",
        19 => "Bangladesh",
        20 => "Barbados",
        21 => "Belarus",
        23 => "Belize",
        24 => "Benin",
        25 => "Bermuda",
        26 => "Bhutan",
        27 => "Bolivia",
        28 => "Bosnia And Herzegowina",
        29 => "Botswana",
        30 => "Bouvet Island",
        31 => "Brazil",
        33 => "Brunei Darussalam",
        34 => "Bulgaria",
        35 => "Burkina Faso",
        36 => "Burundi",
        37 => "Cambodia",
        38 => "Cameroon",
        39 => "Canada",
        40 => "Cape Verde",
        41 => "Cayman Islands",
        42 => "Central African Republic",
        43 => "Chad",
        44 => "Chile",
        45 => "China",
        46 => "Christmas Island",
        47 => "Cocos (Keeling) Islands",
        48 => "Colombia",
        49 => "Comoros",
        50 => "Congo",
        51 => "Cook Islands",
        52 => "Costa Rica",
        53 => "Cote D'Ivoire",
        54 => "Croatia",
        55 => "Cuba",
        56 => "Cyprus",
        57 => "Czech Republic",
        59 => "Djibouti",
        60 => "Dominica",
        61 => "Dominican Republic",
        62 => "East Timor",
        63 => "Ecuador",
        64 => "Egypt",
        65 => "El Salvador",
        66 => "Equatorial Guinea",
        67 => "Eritrea",
        68 => "Estonia",
        69 => "Ethiopia",
        70 => "Falkland Islands",
        71 => "Faroe Islands",
        72 => "Fiji",
        73 => "Finland",
        75 => "France Metropolitan",
        76 => "French Guiana",
        77 => "French Polynesia",
        78 => "French Southern Territories",
        79 => "Gabon",
        80 => "Gambia",
        81 => "Georgia",
        83 => "Ghana",
        84 => "Gibraltar",
        85 => "Greece",
        86 => "Greenland",
        87 => "Grenada",
        88 => "Guadeloupe",
        89 => "Guam",
        90 => "Guatemala",
        246 => "Guernsey",
        91 => "Guinea",
        92 => "Guinea-Bissau",
        93 => "Guyana",
        94 => "Haiti",
        96 => "Honduras",
        97 => "Hong Kong",
        98 => "Hungary",
        99 => "Iceland",
        100 => "India",
        101 => "Indonesia",
        102 => "Iran (Islamic Republic Of)",
        103 => "Iraq",
        104 => "Ireland",
        245 => "Isle of Man",
        247 => "Isle of Wight",
        105 => "Israel",
        107 => "Jamaica",
        108 => "Japan",
        109 => "Jordan",
        244 => "Jersey",
        110 => "Kazakhstan",
        111 => "Kenya",
        112 => "Kiribati",
        113 => "South Korea",
        114 => "North Korea",
        115 => "Kuwait",
        116 => "Kyrgyzstan",
        118 => "Latvia",
        119 => "Lebanon",
        120 => "Lesotho",
        121 => "Liberia",
        122 => "Libyan Arab Jamahiriya",
        123 => "Liechtenstein",
        124 => "Lithuania",
        126 => "Macau",
        127 => "Macedonia",
        128 => "Madagascar",
        129 => "Malawi",
        130 => "Malaysia",
        131 => "Maldives",
        132 => "Mali",
        133 => "Malta",
        134 => "Marshall Islands",
        135 => "Martinique",
        136 => "Mauritania",
        137 => "Mauritius",
        138 => "Mayotte",
        139 => "Mexico",
        140 => "Micronesia",
        141 => "Moldova",
        142 => "Monaco",
        143 => "Mongolia",
        243 => "Montenegro",
        144 => "Montserrat",
        145 => "Morocco",
        146 => "Mozambique",
        147 => "Myanmar",
        148 => "Namibia",
        149 => "Nauru",
        150 => "Nepal",
        152 => "Netherlands Antilles",
        153 => "New Caledonia",
        154 => "New Zealand",
        155 => "Nicaragua",
        156 => "Niger",
        157 => "Nigeria",
        158 => "Niue",
        159 => "Norfolk Island",
        160 => "Northern Mariana Islands",
        162 => "Oman",
        163 => "Pakistan",
        164 => "Palau",
        165 => "Panama",
        166 => "Papua New Guinea",
        167 => "Paraguay",
        242 => "Palestinian Territory",
        168 => "Peru",
        169 => "Philippines",
        170 => "Pitcairn",
        171 => "Poland",
        173 => "Puerto Rico",
        174 => "Qatar",
        175 => "Reunion",
        176 => "Romania",
        177 => "Russian Federation",
        178 => "Rwanda",
        179 => "Saint Kitts And Nevis",
        180 => "Saint Lucia",
        248 => "Saint Vincent and the Grenadines",
        182 => "Samoa",
        183 => "San Marino",
        184 => "Sao Tome And Principe",
        185 => "Saudi Arabia",
        186 => "Senegal",
        241 => "Serbia",
        187 => "Seychelles",
        188 => "Sierra Leone",
        189 => "Singapore",
        190 => "Slovakia (Slovak Republic)",
        191 => "Slovenia",
        192 => "Solomon Islands",
        193 => "Somalia",
        194 => "South Africa",
        197 => "Sri Lanka",
        198 => "St. Helena",
        199 => "St. Pierre And Miquelon",
        200 => "Sudan",
        201 => "Suriname",
        203 => "Swaziland",
        205 => "Switzerland",
        206 => "Syrian Arab Republic",
        207 => "Taiwan",
        208 => "Tajikistan",
        209 => "Tanzania",
        210 => "Thailand",
        211 => "Togo",
        212 => "Tokelau",
        213 => "Tonga",
        214 => "Trinidad And Tobago",
        215 => "Tunisia",
        216 => "Turkey",
        217 => "Turkmenistan",
        218 => "Turks And Caicos Islands",
        219 => "Tuvalu",
        220 => "Uganda",
        221 => "Ukraine",
        222 => "United Arab Emirates",
        226 => "Uruguay",
        227 => "Uzbekistan",
        228 => "Vanuatu",
        229 => "Vatican City State",
        230 => "Venezuela",
        231 => "Vietnam",
        232 => "Virgin Islands (British)",
        233 => "Virgin Islands (U.S.)",
        234 => "Wallis And Futuna Islands",
        235 => "Western Sahara",
        236 => "Yemen",
        237 => "Yugoslavia",
        238 => "Zaire",
        239 => "Zambia",
        240 => "Zimbabwe"
    );

    /** @var array */
    static public $howHeards = array(
        1 => 'Web Site',
        2 => 'Word of Mouth',
        3 => 'Advertisment',
        4 => 'Other'
    );

    /** @var array */
    static public $industries = array(
        1 => 'Accountancy',
        2 => 'Advertising',
        3 => 'Aerospace & Defense',
        4 => 'Agriculture',
        5 => 'Alternative Energy',
        6 => 'Armed Forces',
        7 => 'Arts (Collective)',
        8 => 'Automotive',
        9 => 'Banks',
        10 => 'Business Services',
        11 => 'Catering & Hospitality',
        12 => 'Chemicals',
        13 => 'Child Care',
        14 => 'Construction & Materials',
        15 => 'Education & Training',
        16 => 'Electronic & Electrical Equipment',
        17 => 'Engineering',
        18 => 'Environment',
        19 => 'Event Management',
        20 => 'Fashion',
        21 => 'Financial Services',
        22 => 'Food & Drug Retailers',
        23 => 'Forestry & Paper',
        24 => 'General Industrials',
        25 => 'General Retailers',
        26 => 'Health Care',
        27 => 'Household Goods & Home Construction',
        28 => 'Import and Export',
        29 => 'Industrial Metals & Mining',
        30 => 'Insurance',
        31 => 'Law',
        32 => 'Leisure Goods',
        33 => 'Manufacturing',
        34 => 'Media',
        35 => 'Mobile Telecommunications',
        36 => 'Oil & Gas Producers',
        37 => 'Oil Equipment, Services & Distribution',
        38 => 'Other',
        39 => 'Personal Goods',
        40 => 'Pharmaceuticals & Biotechnology',
        41 => 'Publishing / Print & Design',
        42 => 'Real Estate',
        43 => 'Real Estate Investment & Services',
        44 => 'Removals',
        45 => 'Security',
        46 => 'Social Care',
        47 => 'Software & Computer Services',
        48 => 'Sports and Nutrition',
        49 => 'Support Services',
        50 => 'Technology Hardware & Equipment',
        51 => 'Telecommunications',
        52 => 'Tourism & Leisure',
        53 => 'Training',
        54 => 'Transportation',
        55 => 'Utilities'
    );

    const SUBSCRIBE_DEFAULT = 2;
    const SUBSCRIBE_YES = 1;
    const SUBSCRIBE_NO = 0;

    /** @var int */
    public $customerId;

    /** @var int */
    public $statusId;

    /** @var string */
    public $status;

    /** @var int */
    public $roleId = self::ROLE_NORMAL;

    /** @var string */
    public $role;

    /** @var string */
    public $tagId = self::TAG_RETAIL;

    /** @var array */
    public $tag;

    /** @var string */
    public $icaewId;

    /** @var string */
    public $myDetailsQuestion;

    /** @var string */
    public $email;

    /** @var string */
    public $password;

    /** @var int */
    public $titleId;

    /** @var string */
    public $title;

    /** @var string */
    public $firstName;

    /** @var string */
    public $lastName;

    /**
     * @var string
     */
    public $companyName;

    /** @var string */
    public $address1;

    /** @var string */
    public $address2;

    /** @var string */
    public $address3;

    /** @var string */
    public $city;

    /** @var string */
    public $county;

    /** @var string */
    public $postcode;

    /** @var int */
    public $countryId;

    /** @var string */
    public $country;

    /** @var string */
    public $phone;

    /** @var string */
    public $additionalPhone;

    /** @var string */
    public $isSubscribed = self::SUBSCRIBE_DEFAULT;

    /** @var int */
    public $howHeardId;

    /** @var string */
    public $howHeard;

    /** @var int */
    public $industryId;

    /** @var string */
    public $industry;

    /** @var float */
    public $credit = 0.00;

    /** @var int */
    public $affiliateId;

    /** @var 1nt */
    public $activeCompanies;

    /** @var 1nt */
    public $monitoredActiveCompanies;

    /** @var string */
    public $dtc;

    /** @var string */
    public $dtm;

    /** @var string */
    static private $nameSpace = 'customer';

    /**
     * Provides get customer from database
     * @param int $customerId
     * @return void
     * @throws Exception
     */
    public function __construct($customerId = 0)
    {
        $this->customerId = (int) $customerId;
        if ($this->customerId) {
            $w = FApplication::$db->select('*')
                ->from(TBL_CUSTOMERS)
                ->where('customerId=%i', $this->getId())
                ->execute()->fetch();

            if ($w) {
                $this->customerId = $w['customerId'];

                $this->statusId = $w['statusId'];
                $this->status = isSet(self::$statuses[$this->statusId]) ? self::$statuses[$this->statusId] : 'N/A';

                $this->tagId = $w['tagId'];
                $this->tag = isSet(self::$tags[$this->tagId]) ? self::$tags[$this->tagId] : 'N/A';
                $this->icaewId = $w['icaewId'];
                $this->myDetailsQuestion = $w['myDetailsQuestion'];
                $this->email = $w['email'];
                //$this->password = $w['password'];

                $this->titleId = $w['titleId'];
                $this->title = isSet(self::$titles[$this->titleId]) ? self::$titles[$this->titleId] : 'N/A';

                $this->firstName = $w['firstName'];
                $this->lastName = $w['lastName'];
                $this->companyName = $w['companyName'];
                $this->address1 = $w['address1'];
                $this->address2 = $w['address2'];
                $this->address3 = $w['address3'];
                $this->city = $w['city'];
                $this->county = $w['county'];
                $this->postcode = $w['postcode'];

                $this->countryId = $w['countryId'];
                $this->country = isSet(self::$countries[$this->countryId]) ? self::$countries[$this->countryId] : 'N/A';

                $this->phone = $w['phone'];
                $this->additionalPhone = $w['additionalPhone'];


                $this->isSubscribed = $w['isSubscribed'];

                $this->howHeardId = $w['howHeardId'];
                $this->howHeard = isSet(self::$howHeards[$this->howHeardId]) ? self::$howHeards[$this->howHeardId] : 'N/A';

                $this->industryId = $w['industryId'];
                $this->industry = isSet(self::$industries[$this->industryId]) ? self::$industries[$this->industryId] : 'N/A';

                $this->roleId = $w['roleId'];
                $this->role = isSet(self::$roles[$this->roleId]) ? self::$roles[$this->roleId] : 'N/A';

                $this->credit = (float) $w['credit'];
                $this->affiliateId = $w['affiliateId'];
                $this->activeCompanies = $w['activeCompanies'];
                $this->monitoredActiveCompanies = $w['monitoredActiveCompanies'];
                $this->dtc = $w['dtc'];
                $this->dtm = $w['dtm'];
            } else {
                throw new Exception("Customer with id `$customerId` doesn't exist!");
            }
        }
    }

    /**
     * Returns customerId
     * @return boolean
     */
    public function getId()
    {
        return ($this->customerId);
    }

    /**
     * Returns if customer exists
     * @return boolean
     */
    public function exists()
    {
        return ($this->customerId > 0);
    }

    /**
     * Returns if customer is new
     * @return boolean
     */
    public function isNew()
    {
        return ($this->customerId == 0);
    }

    /**
     * Provides save customer to database
     * @return int $customerId
     */
    public function save()
    {
        // action
        $action = $this->getId() ? 'update' : 'insert';

        // values to save
        $values = array();
        $values['email'] = $this->email;

        if (!empty($this->password)) {
            $values['password'] = md5($this->password);
        }

        $values['titleId'] = $this->titleId;
        $values['firstName'] = $this->firstName;
        $values['lastName'] = $this->lastName;
        $values['companyName'] = $this->companyName;
        $values['address1'] = $this->address1;
        $values['address2'] = $this->address2;
        $values['address3'] = $this->address3;
        $values['city'] = $this->city;
        $values['county'] = $this->county;
        $values['postcode'] = $this->postcode;
        $values['countryId'] = $this->countryId;
        $values['phone'] = $this->phone;
        $values['additionalPhone'] = $this->additionalPhone;
        $values['isSubscribed'] = $this->isSubscribed;
        $values['howHeardId'] = $this->howHeardId;
        $values['industryId'] = $this->industryId;
        $values['roleId'] = $this->roleId;
        $values['credit'] = $this->credit;
        $values['affiliateId'] = $this->affiliateId;
        $values['tagId'] = $this->tagId;
        $values['icaewId'] = $this->icaewId;
        $values['myDetailsQuestion'] = $this->myDetailsQuestion;
        $values['activeCompanies'] = $this->activeCompanies;
        $values['monitoredActiveCompanies'] = $this->monitoredActiveCompanies;
        // insert
        if ($action == 'insert') {
            $values['statusId'] = self::STATUS_VALIDATED;
            $values['dtc'] = new DibiDateTime();
            $values['dtm'] = new DibiDateTime();
            $lastId = FApplication::$db->insert(TBL_CUSTOMERS, $values)->execute(dibi::IDENTIFIER);
            // new id for customer
            $this->customerId = $lastId;
            // update
        } else {
            $values['statusId'] = $this->statusId;
            $values['dtm'] = new DibiDateTime();
            FApplication::$db->update(TBL_CUSTOMERS, $values)->where('customerId=%i', $this->getId())->execute();
        }
        return $this->customerId;
    }

    /**
     * Provides delete customer from database
     * @return void
     */
    public function delete()
    {
        if ($this->exists()) {
            FApplication::$db->delete(TBL_CUSTOMERS)->where('TBL_CUSTOMERS=%i', $this->customerId)->execute();
        }
    }

    /**
     * Provides customer authentification
     * @param string $email
     * @param string $password
     * @return object self
     */
    public static function doAuthenticate($email, $password)
    {
        $customer = FApplication::$db->select('`customerId`, `password`, `statusId`')->from(TBL_CUSTOMERS)->where(
            'email=%s',
            $email
        )->execute()->fetch();
        // email is not found
        if (empty($customer)) {
            throw new Exception(self::AUTH_EMAIL_NOT_FOUND);
        }
        //  password doesn't match
        if ($customer['password'] != md5($password)) {
            throw new Exception(self::AUTH_PASSWORD_DOESNT_MATCH);
        }
        //  not validated yet
        if ($customer['statusId'] == self::STATUS_NOT_VALIDATED) {
            throw new Exception(self::AUTH_NOT_VALIDATED);
        }
        //  blocked
        if ($customer['statusId'] == self::STATUS_BLOCKED) {
            throw new Exception(self::AUTH_BLOCKED);
        }
        return new self($customer['customerId']);
    }

    /**
     * Provides customer signin
     * @param int $customerId
     * @param string $nameSpace
     * @return void
     */
    public static function signIn($customerId)
    {
        $session = FApplication::$session->getNameSpace(self::$nameSpace);
        $session->customerId = (int) $customerId;
    }

    /**
     * Returns if customer is signin
     * @param string $nameSpace
     * @return object
     */
    public static function isSignedIn()
    {
        $session = FApplication::$session->getNameSpace(self::$nameSpace);
        return isset($session->customerId) ? TRUE : FALSE;
    }

    /**
     * Returns signin customer
     * @param string $nameSpace
     * @return Customer
     */
    public static function getSignedIn()
    {
        $session = FApplication::$session->getNameSpace(self::$nameSpace);
        return self::isSignedIn() ? new self($session->customerId) : new self(0);
    }

    /**
     * Provides customer signout
     * @param string $nameSpace
     * @return void
     */
    public static function signOut()
    {
        if (self::isSignedIn()) {
            $session = FApplication::$session->getNameSpace(self::$nameSpace);
            unset($session->customerId);
        }
    }

    /**
     * Returns if customer compleded registration
     * @return boolean
     */
    public function hasCompletedRegistration()
    {
        return ($this->firstName == NULL || $this->postcode == NULL) ? FALSE : TRUE;
    }

    /**
     * Returns whole address
     *
     * @param string $newLine
     * @return string
     */
    public function getAddress($newLine = "\n")
    {
        $address = array();
        if ($this->address1 != NULL) {
            $address[] = $this->address1;
        }
        if ($this->address2 != NULL) {
            $address[] = $this->address2;
        }
        if ($this->address3 != NULL) {
            $address[] = $this->address3;
        }
        if ($this->city != NULL) {
            $address[] = $this->city;
        }
        if ($this->county != NULL) {
            $address[] = $this->county;
        }
        if ($this->postcode != NULL) {
            $address[] = $this->postcode;
        }
        if ($this->country != NULL) {
            $address[] = $this->country;
        }

        $address = implode($newLine, $address);
        return $address;
    }

    /**
     * Enter description here...
     *
     * @param array $where
     * @param int $limit
     * @param int $offset
     * @return array Customer
     */
    public static function getCustomers(array $where = array(), $limit = NULL, $offset = NULL, $returnAsObjects = TRUE)
    {
        $result = dibi::select('*')->from(TBL_CUSTOMERS)->orderBy('dtc')->desc();
        if (!empty($where)) {
            foreach ($where as $key => $val) {
                if (String::startsWith($val, '===')) {
                    $val = str_replace('===', '', $val);
                    $result->where('%n', $key, ' = %s', $val);
                } else {
                    $val = '%' . $val . '%';
                    $result->where('%n', $key, ' LIKE %s', $val);
                }
            }
        }

        // limit
        if ($limit !== NULL) {
            $result->limit($limit);
        }

        // offset
        if ($offset !== NULL) {
            $result->offset($offset);
        }

        $return = $result->execute()->fetchAssoc('customerId');

        // return as objects Customer
        if ($returnAsObjects === TRUE) {
            $customers = array();
            foreach ($return as $key => $val) {
                $customers[$val->customerId] = new self($val->customerId);
            }
            return $customers;
        }
        return $return;
    }

    public static function getCustomersCount(array $where = array())
    {
        $result = dibi::select('count(*)')->from(TBL_CUSTOMERS);
        // where
        if (!empty($where)) {
            foreach ($where as $key => $val) {
                if (String::startsWith($val, '===')) {
                    $val = str_replace('===', '', $val);
                    $result->where('%n', $key, ' = %s', $val);
                } else {
                    $val = '%' . $val . '%';
                    $result->where('%n', $key, ' LIKE %s', $val);
                }
            }
        }
        $count = $result->execute()->fetchSingle();
        return $count;
    }

    /**
     * Return customer orders
     *
     * @param int $limit
     * @param int $offset
     * @return array Order
     */
    public function getCustomerOrders($limit = NULL, $offset = NULL)
    {
        $orders = Order::getOrders($this->getId(), $limit = NULL, $offset = NULL);
        return $orders;
    }

    /**
     * @return int
     */
    public function getCustomerOrderCount()
    {
        return dibi::select('count(*)')->from(TBL_ORDERS)->where('customerId=%i', $this->getId())->execute()->fetchSingle();
    }

    /**
     * Return customer order
     *
     * @param int $orderId
     * @return Order
     * @throws Exception
     */
    public function getCustomerOrder($orderId)
    {
        $order = new Order($orderId);
        if ($order->customerId != $this->getId()) {
            throw new Exception("Order with id `$orderId` doesn't belong to customer with id `{$this->getId()}`");
        }
        return $order;
    }

    /**
     * Return customer companies
     * @return array $return
     */
    public function getCustomerCompanies()
    {
        $return = array();
        $chFiling = new CHFiling();
        $companies = $chFiling->getCustomerCompanies($this->getId());
        foreach ($companies as $companyId => $company) {
            $return[$companyId] = array(
                'companyName' => $company['company_name'],
                'companyNumber' => $company['company_number'],
                'authenticationCode' => $company['authentication_code'],
                'companyCategory' => $company['company_category'],
                'registeredOfficeId' => $company['registered_office_id'],
                'orderId' => $company['order_id'],
                'nomineeDirectorId' => $company['nominee_director_id'],
                'nomineeSubscriberId' => $company['nominee_subscriber_id'],
                'nomineeSecretaryId' => $company['nominee_secretary_id'],
                'incorporationDate' => $company['incorporation_date'] != NULL ? date('d/m/Y', strtotime($company['incorporation_date'])) : NULL,
                'outstandingRequests' => 'TODO',
            );
        }
        return $return;
    }

    /**
     * Returns customer by email address
     *
     * @param string $email
     * @return Customer
     * @throws Exception
     */
    public static function getCustomerByEmail($email)
    {
        $id = FApplication::$db->select('customerid')->from(TBL_CUSTOMERS)->where('email=%s', $email)->execute(
        )->fetchSingle();
        if ($id === FALSE) {
            throw new CustomerAvailabilityException("Customer with email `$email` doesn't exist!");
        }
        $customer = new self($id);
        return $customer;
    }

    /**
     * Remove credit from customer on payment
     *
     * @param int $subtractValue
     * @return void
     * @throws Exception
     */
    public function subtractCredit($subtractValue)
    {
        $subtractValue = (float) $subtractValue;

        // not enough credit
        if ($subtractValue > $this->credit) {
            throw new Exception("You don't have enough credit for this payment!");
        }

        $this->credit -= (float) $subtractValue;
        $this->save(FALSE);
    }

    /**
     * Returns if customer is UK citizen
     *
     * @return boolean
     */
    public function isUKCitizen()
    {
        return (bool) $this->countryId == self::UK_CITIZEN;
    }

    /**
     * Returns list of all customer companies for select input
     *
     * @param int $customerId
     * @return array
     */
    public static function getDropdownCompanies($customerId)
    {
        $companies = array();
        $chFiling = new CHFiling();

        $temp['inc'] = $chFiling->getCustomerIncorporatedCompanies($customerId);
        $temp['notInc'] = $chFiling->getCustomerNotIncorporatedCompanies($customerId);

        // incorporated
        if (!empty($temp['inc'])) {
            foreach ($temp['inc'] as $companyId => $company) {
                $companies['Incorporated'][$companyId] = $company['company_name'];
            }
        }

        // not incorporated
        if (!empty($temp['notInc'])) {
            foreach ($temp['notInc'] as $companyId => $company) {
                $companies['Not Incorporated'][$companyId] = $company['company_name'];
            }
        }

        return $companies;
    }
    
    /**
     * Returns if customer is retail
     *
     * @return boolean
     */
    public function isRetail()
    {
        return ($this->roleId == self::ROLE_NORMAL);
    }

    /**
     * Returns if customer is wholesaler
     *
     * @return boolean
     */
    public function isWholesale()
    {
        return ($this->roleId == self::ROLE_WHOLESALE);
    }

    /**
     * @return bool
     */
    public function isAffiliate()
    {
        return ($this->tagId == self::TAG_AFFILIATE && $this->roleId == self::ROLE_NORMAL);
    }

    public function hasWholesaleQuestion()
    {
        //Only show to users that have a company in their account and not wholesaler
        if (count($this->getCustomerCompanies()) >= 1 && !$this->isWholesale()) {
            // show if didn't answered our questions
            if ($this->myDetailsQuestion == NULL) {
                return TRUE;
            }
        }

        return FALSE;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return trim(sprintf("%s %s", $this->firstName, $this->lastName));
    }
}
