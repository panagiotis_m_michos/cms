<?php

class CompanyOtherDocument extends Object
{
    /* types */
    const TYPE_OTHERS = 'OTHER';
    const TYPE_OTHERS_MAX_NUMBER = 10;

    /**
     * @var array
     */
    static public $types = array(
        self::TYPE_OTHERS => 'Other'
    );

    /**
     * @var string
     */
    static public $storageDir = TEMP_OTHER_DIR;

    /**
     * @var string
     */
    static public $storageDirR = TEMP_OTHER_DIR_R;

    /**
     * primary key
     *
     * @var int
     */
    public $companyDocumentId;

    /**
     * @var int
     */
    public $companyId;

    /**
     * @var string
     */
    public $typeId;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $fileTitle;

    /**
     * @var string
     */
    public $size;

    /**
     * @var string
     */
    public $ext;

    /**
     * @var string
     */
    public $dtc;

    /**
     * @var string
     */
    public $dtm;

    /**
     * @var array
     */
    public $uploadedFile;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->companyDocumentId;
    }

    /**
     * @return bool
     */
    public function isNew()
    {
        return ($this->companyDocumentId == 0);
    }

    /**
     * @param int $companyDocumentId
     * @throws Exception
     */
    public function __construct($companyDocumentId = 0)
    {
        $this->check();
        $this->companyDocumentId = (int) $companyDocumentId;
        if ($this->companyDocumentId) {
            $w = dibi::select('*')
                ->from(TBL_COMPANY_DOCUMENTS)
                ->where('companyDocumentId=%i', $companyDocumentId)
                ->execute()
                ->fetch();
            if ($w) {
                $this->companyDocumentId = $w['companyDocumentId'];
                $this->companyId = $w['companyId'];
                $this->typeId = $w['typeId'];
                $this->name = $w['name'];
                $this->fileTitle = $w['fileTitle'];
                $this->size = $w['size'];
                $this->ext = $w['ext'];
                $this->dtc = $w['dtc'];
                $this->dtm = $w['dtm'];
            } else {
                throw new Exception("Document with id `$companyDocumentId` doesn't exist!");
            }
        }
    }

    /**
     * @return int
     */
    public function save()
    {
        $data = array();
        $data['companyId'] = $this->companyId;
        $data['typeId'] = $this->typeId;
        $data['name'] = $this->name;
        $data['fileTitle'] = $this->fileTitle;
        $data['size'] = $this->size;
        $data['ext'] = $this->ext;
        $data['dtm'] = new DibiDateTime();

        if ($this->isNew()) {
            $data['dtc'] = new DibiDateTime();
            $this->companyDocumentId = dibi::insert(TBL_COMPANY_DOCUMENTS, $data)->execute(dibi::IDENTIFIER);
            if (!is_dir($this->getStorageDir())) {
                mkdir($this->getStorageDir(), 0777, TRUE);
            }
            $this->getUploadedFile()->move($this->getFilePath());
        } else {
            dibi::update(TBL_COMPANY_DOCUMENTS, $data)
                ->where('companyDocumentId=%i', $this->companyDocumentId)
                ->execute();
        }
        return $this->companyDocumentId;
    }

    /**
     * @return HttpUploadedFile
     */
    private function getUploadedFile()
    {
        return $this->uploadedFile;
    }


    /**
     * @return string
     */
    public function getFilePathRelative()
    {
        return $this->getFilePath(TRUE);
    }

    /**
     * @param boolean $relative
     * @return string
     */
    public function getFilePath($relative = FALSE)
    {
        $path = sprintf("%s%s", $this->getStorageDir($relative), $this->getFileName());
        return $path;
    }

    /**
     * @param boolean $relative
     * @return string
     */
    public function getStorageDir($relative = FALSE)
    {
        $dir = (string) floor($this->companyId / 10000) + 1;
        $dir = sprintf("%s0000", $dir);
        $path = $relative ? self::$storageDirR : self::$storageDir;
        return sprintf("%s%s/", $path, $dir . '/' . $this->companyId);
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return sprintf("%s.%s", $this->name, $this->ext);
    }

    /**
     * @return string
     */
    public function getRealFileName()
    {
        return sprintf("%s.%s", $this->fileTitle, $this->ext);
    }

    /**
     * Provides checking
     * @throws Exception
     */
    private function check()
    {
        // is dir writeable
        if (!is_dir(self::$storageDir) || !is_writable(self::$storageDir)) {
            throw new Exception('Files directory "' . $this->getStorageDir() . '" is not writable.');
        }
        // pathinfo
        if (!function_exists('pathinfo')) {
            throw new Exception('Function `pathinfo()` could not be found!');
        }
    }

    public function delete()
    {
        $fileToRemove = $this->getFilePath($relative = FALSE);
        if (file_exists($fileToRemove)) {
            if (@unlink($fileToRemove) === TRUE) {
                dibi::delete(TBL_COMPANY_DOCUMENTS)->where('companyDocumentId=%i', $this->companyDocumentId)->execute();
            } else {
                throw new Exception("Something is wrong, we may not have enough permission to delete `$this->companyDocumentId`");
            }
        } else {
            throw new Exception("the file `$this->companyDocumentId` is not found");
        }
    }

    /**
     * Returns count of signups
     * @param array $where
     * @return int
     */
    public static function getCount(array $where = array())
    {
        $result = dibi::select('count(*)')->from(TBL_COMPANY_DOCUMENTS);
        foreach ($where as $key => $val) {
            $result->where('%n', $key, ' = %s', $val);
        }
        $count = $result->execute()->fetchSingle();
        return $count;
    }

    /**
     * Returns all signups
     * @param int $limit
     * @param int $offset
     * @param array $where
     * @return int
     */
    public static function getAll($limit = NULL, $offset = NULL, array $where = array())
    {
        $result = dibi::select('*')->from(TBL_COMPANY_DOCUMENTS)->orderBy('dtc', dibi::DESC);
        if ($limit !== NULL) {
            $result->limit($limit);
        }
        if ($offset !== NULL) {
            $result->offset($offset);
        }
        foreach ($where as $key => $val) {
            $result->where('%n', $key, ' = %s', $val);
        }
        return $result->execute()->fetchAssoc('companyDocumentId');
    }

    /**
     * Returns result as objects
     * @param int $limit
     * @param int $offset
     * @param array $where
     * @return array<Documents>
     */
    public static function getAllObjects($limit = NULL, $offset = NULL, array $where = array())
    {
        $objects = array();
        $all = self::getAll($limit, $offset, $where);

        foreach ($all as $key => $val) {
            $objects[$key] = new self($key);
        }
        return $objects;
    }
}
