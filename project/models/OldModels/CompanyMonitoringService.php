<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS
 * @author 		Nikolai Senkevich, Stan Bazik
 * @internal 	project/models/CompanyMonitoringService.php
 * @created 	21/03/2011
 */
class CompanyMonitoringService extends Object
{
    /* directions */
    const ASC = 'ASC';
    const DESC = 'DESC';
    const ASC_SHORT = 'A';
    const DESC_SHORT = 'D';

    /**
     * @var array
     */
    static private $directions = array(
        'A' => self::ASC,
        'D' => self::DESC,
    );
    private $order;

    /**
     *
     * @param int $companyNumber
     * @return array
     */
    static public function getCompanyData($companyNumber)
    {
        $chXmlGateway = new CHXmlGateway();
        $request = $chXmlGateway->getCompanyDetails($companyNumber);
        $result = $chXmlGateway->getResponse($request);
        $result = simplexml_load_string($result);
        if (isset($result->Body->CompanyDetails)) {
            $result = $result->Body->CompanyDetails;
        } else {
                throw new Exception('Company number does not exist');
        }

        $data = array();
        $data['companyName'] = isset($result->CompanyName) ? (string) $result->CompanyName : '';
        $data['companyNumber'] = isset($result->CompanyNumber) ? (string) $result->CompanyNumber : '';
        $data['companyStatus'] = isset($result->CompanyStatus) ? (string) $result->CompanyStatus : '';
        if(isset($result->IncorporationDate)){
            $data['incorporationDate'] = (string) $result->IncorporationDate;
        }elseif(isset($result->RegistrationDate)){
            $data['incorporationDate'] = (string) $result->RegistrationDate;
        }else{
            $data['incorporationDate'] = '';
        }
        $data['accountsNextDueDate'] = isset($result->Accounts->NextDueDate) ? (string) $result->Accounts->NextDueDate : '';
        $data['returnsNextDueDate'] = isset($result->Returns->NextDueDate) ? (string) $result->Returns->NextDueDate : '';

        // --- remove empty values ---
        foreach ($data as $k => $v) {
            if (!isset($v) || empty($v)) {
                unset($data[$k]);
            }
        }

        return $data;
    }

    /**
     * Returns feedback  paginator
     *
     * @return FPaginator2
     */
    public function getCustmerCompaniesWhere(Customer $customer, CompaniesMonitoringListForm $filter)
    {
        $where = array();
        $where['customerId=%i'] = $customer->customerId;
        $data = $filter->getValues();
        if ($data['selector'] == 'Return') {
            if (!empty($data['dateFrom']))
                $where['DATE_FORMAT(`returnsNextDueDate`,"%Y-%m-%d") >= %s'] = $data['dateFrom'];
            if (!empty($data['dateTo']))
                $where['DATE_FORMAT(`returnsNextDueDate`,"%Y-%m-%d") <= %s'] = $data['dateTo'];
        }
        if ($data['selector'] == 'Accounts') {
            if (!empty($data['dateFrom']))
                $where['DATE_FORMAT(`accountsNextDueDate`,"%Y-%m-%d") >= %s'] = $data['dateFrom'];
            if (!empty($data['dateTo']))
                $where['DATE_FORMAT(`accountsNextDueDate`,"%Y-%m-%d") <= %s'] = $data['dateTo'];
        }
         if ($customer->monitoredActiveCompanies == 1){
                $where['companyStatus LIKE %s'] = '%'."Active".'%';       
        }
        return $where;
    }
    
    /**
     * @param Customer $customer
     * @param CompaniesMonitoringACForm $filter 
     */
     public function saveActiveCompaniesStatus(Customer $customer, CompaniesMonitoringACForm $filter){
            $data = $filter->getValues();
            $customer->monitoredActiveCompanies = NULL;
            if ($data['active'] == 1){
                $customer->monitoredActiveCompanies = 1;
            }
            $customer->save();
        }
        
    /**
     *
     * @param string $order
     * @return string
     */
    public function getCustmerCompaniesOrder($order)
    {
        // --- default ---
        $this->order = 'accountsNextDueDate ASC';
        // --- column ---
        $column = substr($order, 0, -1);
        // --- direction ---
        $dir = strtoupper(substr($order, -1));
        $direction = ($dir === self::ASC_SHORT) ? self::ASC : self::DESC;

        $this->order = $column . ' ' . $direction;
        return $this->order;
    }

    /**
     *
     * @param Customer $customer
     * @param CompaniesMonitoringListForm $filter
     * @return FPaginator2
     */
    public function getCustmerCompaniesPaginator(Customer $customer, CompaniesMonitoringListForm $filter = NULL)
    {
        $where = $this->getCustmerCompaniesWhere($customer, $filter);
        $paginator = new FPaginator2(CompanyMonitoring::getCount($where));
        $paginator->htmlDisplayEnabled = FALSE;
        $paginator->itemsPerPage = 50;
        return $paginator;
    }

    /**
     * Returns list of feedbacks
     *
     * @param FF $filter
     * @param FPaginator2 $paginator
     * @return array<DibiRow>
     */
    public function getCustmerCompaniesList(Customer $customer, CompaniesMonitoringListForm $filter, FPaginator2 $paginator = NULL)
    {
        $where = $this->getCustmerCompaniesWhere($customer, $filter);
        $order = $this->order;

        if ($paginator !== NULL) {
            $companiesList = CompanyMonitoring::getAll($paginator->getLimit(), $paginator->getOffset(), $where, $order);
        } else {
            $companiesList = CompanyMonitoring::getAll(NULL, NULL, $where);
        }
        return $companiesList;
    }

    /**
     * @param FForm $filter
     * @return void
     */
    public function outputCustmerCompaniesListToCSV(Customer $customer, CompaniesMonitoringListForm $filter)
    {

        $arr = $this->getCustmerCompaniesList($customer, $filter);

        if (empty($arr)) {
            throw new Exception('No data to export !');
        }
        foreach ($arr as $key => $company) {
            unset($company['companyMonitoredId']);
            unset($company['customerId']);
            unset($company['dtm']);
            unset($company['dtc']);
            $arr[$key] = $company;
        }
        $header = array_keys((array) current($arr));
        array_unshift($arr, $header);
        $filename = sprintf("custmerCompaniesList.csv", date('d-m-Y'));
        Array2Csv::output($arr, $filename);
    }

    /**
     *
     * @param Customer $customer
     */
    public function syncCustomerCompanies(Customer $customer)
    {
        /* only companies with company number */
        $ids    = dibi::select('companyMonitoredId')
                ->from(TBL_COMPANIES_MONITORING)
                ->where('customerId = %i', $customer->customerId)
                ->and('companyNumber IS NOT NULL')
                ->execute()
                ->fetchAll();
		if (!empty($ids)) {
            foreach($ids as $id) {
                try {
                    $companyUpdate = new CompanyMonitoring($id['companyMonitoredId']);
                    $data = CompanyMonitoringService::getCompanyData($companyUpdate->companyNumber);
                    $companyUpdate->companyName = $data['companyName'];
                    $companyUpdate->companyNumber = $data['companyNumber'];
                    $companyUpdate->companyStatus = $data['companyStatus'];
                    $companyUpdate->incorporationDate = $data['incorporationDate'];
                    (isset($data['accountsNextDueDate'])) ? $companyUpdate->accountsNextDueDate = $data['accountsNextDueDate'] : $companyUpdate->accountsNextDueDate = NULL;
                    (isset($data['returnsNextDueDate'])) ? $companyUpdate->returnsNextDueDate = $data['returnsNextDueDate'] : $companyUpdate->returnsNextDueDate = NULL;
            
                    // save
                    $companyUpdate->save();
                }  catch (Exception $e) {
                   $errors[] = "<b>" . $companyUpdate->companyNumber . "</b> - " . $e->getMessage();
                }
            }
		}
        if(isset($errors)){throw new Exception(implode("<br />", $errors));}
    }
}