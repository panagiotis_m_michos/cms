<?php

class CUSummaryModel extends FNode
{
    /**
     * @var Company
     */
    private $company;

    /**
     * @var Customer
     */
    private $customer;
    
    /**
     * @param Company $company
     * @param Customer $customer
     */
    public function startup(Company $company, Customer $customer)
    {
        $this->company = $company;
        $this->customer = $customer;
    }
    
    /**
     * @return bool
     */
    public function hasAllowedSubscriberCertificates()
    {
        $items = OrderItem::getOrderItems($this->company->getOrderId());
        foreach ($items as $item) {
            if ($item->productId == Package::PACKAGE_BRONZE) {
                return FALSE;
            }
        }
        return TRUE;
    }
     
    /**
     * @throws Exception
     */
    public function syncCompany()
    {
        $this->company->syncAllData();
    }
    
    /**
     * @param string $documentId
     */
    public function outputDocument($documentId)
    {
        $this->company->getDocument($documentId);
    }

    public function outputCompanySummary()
    {
        $data = $this->company->getData();
        $summary = CompanySummary::getCompanySummary(
            $data['company_details'],
            $data['registered_office'],
            $data['sail_address'],
            $data['directors'],
            $data['secretaries'],
            $data['shareholders'],
            $data['capitals']
        );
        $summary->outputCompanySummaryPdf();
    }

    /**
     * @param int $subscriberCertificateId
     * @throws Exception
     */
    public function outputSubscriberCertificate($subscriberCertificateId)
    {
        if ($this->hasAllowedSubscriberCertificates() === FALSE) {
            throw new Exception('Action `Print Subscriber Certificate` is not allowed');
        }

        $data = $this->company->getData();
        $num = $subscriberCertificateId + 1;

        if (isset($data['subscribers'][$subscriberCertificateId]['corporate_name'])) { // --- corporate certificate ---
            if (FormSubmission::getCompanyIncorporationFormSubmission($this->company) !== NULL) {
                $companyIncorporation = FormSubmission::getCompanyIncorporationFormSubmission($this->company);
                $sub = $companyIncorporation->getForm()->getIncCorporate($data['subscribers'][$subscriberCertificateId]['id'], 'SUB');
            }
            $corp = ShareCert::getCorporateCertificate($sub, $this->company->getCompanyName(), $this->company->getCompanyNumber(), $data['registered_office'], $num);
            $corp->getPdfCertificate();


        } else { // --- person certificate ---
            if (FormSubmission::getCompanyIncorporationFormSubmission($this->company) !== NULL) {
                $companyIncorporation = FormSubmission::getCompanyIncorporationFormSubmission($this->company);
                $sub = $companyIncorporation->getForm()->getIncPerson($data['subscribers'][$subscriberCertificateId]['id'], 'SUB');
            }
            $person = ShareCert::getPersonCertificate($sub, $this->company->getCompanyName(), $this->company->getCompanyNumber(), $data['registered_office'], $num);
            $person->getPdfCertificate();
        }
    }
    
    /**
     * @param array $callback
     * @return FForm
     */
    public function getComponentBarclaysForm(array $callback)
    {
        return CompanyCustomerModel::getComponentFrontForm($callback, $this->customer);
    }
    
    /**
     * @param FForm $form
     */
    public function processBarclaysForm(FForm $form)
    {
        $data = $form->getValues();
        CompanyCustomerModel::saveFrontForm($this->customer->getId(), $this->company->getCompanyId(), $data);
        Barclays::sendCompany($this->company->getCompanyId(), FALSE, TRUE, TRUE);
        $form->clean();
    }

    /**
     * @throws Exception
     */
    public function checkBarclaysPage()
    {
        $companyId = $this->company->getCompanyId();
        if (Barclays::isCompanySubmitted($companyId)) {
            throw new Exception('Company has already been submitted');
        } elseif (BarclaysCompany::canBeSubmitted($companyId) === FALSE) {
            throw new Exception("Sorry, this company cannot be submitted via our Fast Track Banking service and is not eligible for cash back. This is because the company is either too old or it was imported on to our system.");
        }
    }
    

    /**
     * @return bool
     */
    public function displayBarclaysBox()
    {
        try {
            $this->checkBarclaysPage();
            return TRUE;    
        } catch (Exception $e) {
            return FALSE;
        }
    }
}
