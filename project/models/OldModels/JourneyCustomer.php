<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS 
 * @author 		Stan (diviak@gmail.com)
 * @internal 	project/models/JourneyCustomer.php
 * @created 	17/07/2009
 */

class JourneyCustomer extends Object
{
	/** @var int */
	private $journeyCustomerId;
	
	/** @var string */
	public $fullName;
	
	/** @var string */
	public $email;
	
	/** @var string */
	public $phone;
	
	/** @var mixed */
	public $customerId = NULL;
	
	/** @var string */
	public $postcode;
	
	/** @var string */
	public $companyName;
	
	/** @var string */
	public $dtc;
	
	/** @var string */
	public $dtm;
    
	
	/**
	 * @param int $journeyCustomerId
	 * @return void
	 */
	public function __construct($journeyCustomerId = 0)
	{
		$this->journeyCustomerId = (int) $journeyCustomerId; 
		if ($this->journeyCustomerId) {
			$w = FApplication::$db->select('*')->from(TBL_JOURNEY_CUSTOMERS)->where('journeyCustomerId=%i', $this->journeyCustomerId)->execute()->fetch();
			if ($w) {
				$this->journeyCustomerId = $w['journeyCustomerId'];
				$this->fullName = $w['fullName'];
				$this->email = $w['email'];
				$this->phone = $w['phone'];
				$this->customerId = $w['customerId'];
				$this->postcode = $w['postcode'];
				$this->companyName = $w['companyName'];
                $this->dtc = $w['dtc'];
				$this->dtm = $w['dtm'];
			} else {
				throw new Exception("Journey customer with id `$journeyCustomerId` doesn't exist!");
			}
		} 
	}
	
	/**
	 * Returns primary key
	 * 
	 * @return int
	 */
	public function getId() 
	{ 
		return $this->journeyCustomerId; 
	}


	/**
	 * Returns if Journey customer is new
	 * 
	 * @return bool
	 */
	public function isNew() 
	{ 
		return ($this->journeyCustomerId == 0); 
	}
	
	
	/**
	 * Provide saving journey customer to database
	 * 
	 * @return int $id
	 */
	public function save()
	{
		$data = array();
		$data['fullName'] = $this->fullName;
		$data['email'] = $this->email;
		$data['phone'] = $this->phone;
		$data['customerId'] = $this->customerId;
		$data['postcode'] = $this->postcode;
		$data['companyName'] = $this->companyName;
		$data['dtm'] = new DibiDateTime();
		// insert
		if ($this->isNew()){
			$data['dtc'] = new DibiDateTime();
			$this->journeyCustomerId = FApplication::$db->insert(TBL_JOURNEY_CUSTOMERS, $data)->execute(dibi::IDENTIFIER);
		// update
		} else {
			FApplication::$db->update(TBL_JOURNEY_CUSTOMERS, $data)->where('journeyCustomerId=%i', $this->journeyCustomerId)->execute();
		}
		
		return $this->journeyCustomerId;
	}
	
	/**
	 * @return void
	 */
	public function delete()
	{
		dibi::delete(TBL_JOURNEY_CUSTOMERS)->where('journeyCustomerId=%i', $this->journeyCustomerId)->execute();
	}
	
	
	/**
	 * Returns Journey customer products
	 *
	 * @return array<JourneyCustomerProduct>
	 */
	public function getJourneyCustomerProducts()
	{
		$items = JourneyCustomerProduct::getJourneyCustomerProducts($this->getId());
		return $items;
	}
	
	/**
	 * Returns count of all records
	 *
	 * @param array $where
	 * @return int
	 */
	static public function getCount(array $where = array())
	{
		$result = FApplication::$db->select('COUNT(*)')->from(TBL_JOURNEY_CUSTOMERS)->orderBy('dtc')->desc();
		foreach ($where as $key=>$val) {
			$val = '%'.$val.'%';
			$result->where('%n', $key, ' LIKE %s', $val);
		}
		$count = $result->execute()->fetchSingle();
		return $count;	
	}
	
	
	/**
	 * Returns journey customer as array
	 *
	 * @param int $limit
	 * @param int $offset
	 * @param array $where
	 * @return array<DibiRow>
	 */
	static public function getAll($limit = NULL, $offset = NULL, array $where = array())
	{
		$result = FApplication::$db->select('*')->from(TBL_JOURNEY_CUSTOMERS)->orderBy('dtc')->desc();
		
		foreach ($where as $key=>$val) {
			$val = '%'.$val.'%';
			$result->where('%n', $key, ' LIKE %s', $val);
		}
		if ($limit !== NULL) $result->limit($limit);
		if ($offset !== NULL) $result->offset($offset);
		
		$journey = $result->execute()->fetchAssoc('journeyCustomerId');
		return $journey;	
	}
	
	/**
	 * Returns journey customer based on conditions
	 *
	 * @param int $limit
	 * @param int $offset
	 * @param array $where
	 * @return array<JourneyCustomer>
	 */
	static public function getAllObjects($limit = NULL, $offset = NULL, array $where = array())
	{
		$objects = array();
		$data = self::getAll($limit, $offset, $where);
		foreach ($data as $key => $val) {
			try {
				$id = $val->journeyCustomerId;
				$objects[$id] = new self($id);
			} catch (Exception $e) {
			}
		}
		return $objects;
	}
	
		/**
	 * Returns csv data for products based on product_id and specific date
	 *
	 * @param int $productId
	 * @param string $date
	 * @return string
	 */
	static public function geCsvCustomers($productId, $date = NULL)
	{
		if ($date === NULL) {
			$yesterday  = mktime(0, 0, 0, date('m')  , date('d')-1, date('Y'));
			$date = date('Y-m-d', $yesterday);
		}
		
		$products = dibi::select('*')
			->from(TBL_JOURNEY_CUSTOMERS, 'jc')
			->leftJoin(TBL_JOURNEY_CUSTOMER_PRODUCTS, 'jcp')->on('jc.`journeyCustomerId`=jcp.`journeyCustomerId`')
            ->leftJoin(TBL_JOURNEY_PRODUCTS_QUESTIONS, 'jcp2')->on('jcp.`journeyCustomerProductId`=jcp2.`journeyCustomerProductId`')
			->where('jcp.`productId`=%i', $productId)
				->and('jc.`dtc` LIKE %s', $date.'%')
			->execute()->fetchAll();
		$product = array();
        $product2 = array();
        $i = 1;
        $id = 1;
		if (empty($products)) return NULL;
        foreach ($products as $key => $value) {
            if ($value['journeyCustomerId']!= $id){$i = 1;}
            if($products[$key]['answer'] || $products[$key]['question']){
                $product2[$value['journeyCustomerId']]['answer'.$i] = $value['answer'];
                $product2[$value['journeyCustomerId']]['question'.$i] = $value['question'];
                $id = $value['journeyCustomerId'];
            }$i++;
            $customer[$value['journeyCustomerId']] = $value;
            unset($customer[$value['journeyCustomerId']]['answer']);
            unset($customer[$value['journeyCustomerId']]['question']);
            unset($customer[$value['journeyCustomerId']]['questionId']);
            unset($customer[$value['journeyCustomerId']]['customerId']);
        }
        $header = array_keys((array) current($customer));
        if(!empty($product2)){
            foreach ($customer as $key => &$item) {
                $item = array_merge((array) $item, $product2[$key]);
            }
        }
		array_unshift($customer, $header);
		$csv = Array2Csv::getContent($customer);
		return $csv;
	}
}
