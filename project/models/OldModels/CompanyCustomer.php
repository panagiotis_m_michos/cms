<?php

class CompanyCustomer extends Object
{
	/** @var int */
	public $companyCustomerId;

	/** @var int */
	public $companyId;

	/** @var int */
	public $preferredContactDate;

	/** @var int */
	public $wholesalerId;
	
	/** @var string */
	public $bankTypeId;

	/** @var string */
	public $email;
	
	/** @var string */
	public $titleId;
	
	/** @var string */
	public $firstName;
	
	/** @var string */
	public $lastName;
	
	/** @var string */
	public $address1;
	
	/** @var string */
	public $address2;
	
	/** @var string */
	public $address3;
	
	/** @var string */
	public $city;
	
	/** @var string */
	public $county;
	
	/** @var string */
	public $postcode;
	
	/** @var string */
	public $countryId;
	
	/** @var string */
	public $phone;
	
	/** @var string */
	public $additionalPhone;
    
    /** @var int */
	public $consent;
	
	/** @var string */
	public $dtc;
	
	
	/**
	 * @return int
	 */
	public function getId() { return( $this->companyCustomerId ); }

	/**
	 * @return bool
	 */
	public function isNew() { return( $this->companyCustomerId == 0 ); }

	/**
	 * @param int $companyCustomerId
	 * @throws Exception
	 */
	public function __construct($companyCustomerId = 0)
	{
		$this->companyCustomerId = (int)$companyCustomerId; 
		if ($this->companyCustomerId) {
			$w = FApplication::$db->select('*')->from(TBL_COMPANY_CUSTOMER)->where('companyCustomerId=%i', $this->companyCustomerId)->execute()->fetch();
			if ($w) {
				$this->preferredContactDate= $w['preferredContactDate'];
				$this->companyCustomerId = $w['companyCustomerId'];
				$this->companyId = $w['companyId'];
				$this->wholesalerId = $w['wholesalerId'];
				$this->bankTypeId = $w['bankTypeId'];
				$this->email = $w['email'];
				$this->titleId = $w['titleId'];
				$this->firstName = $w['firstName'];
				$this->lastName = $w['lastName'];
				$this->address1 = $w['address1'];
				$this->address2 = $w['address2'];
				$this->address3 = $w['address3'];
				$this->city = $w['city'];
				$this->county = $w['county'];
				$this->postcode = $w['postcode'];
				$this->countryId = $w['countryId'];
				$this->phone = $w['phone'];
				$this->additionalPhone = $w['additionalPhone'];
                $this->consent = $w['consent'];
				$this->dtc = $w['dtc'];
			} else {
			    throw new Exception("CompanyCustomer with id `$companyCustomerId` doesn't exist.");
			}
		} 
	}
	
	
	/**
	 * Provides saving CompanyCustomer
	 * 
	 * @return int
	 */
	public function save()
	{
		$action = $this->companyCustomerId ? 'update' : 'insert';

		$data = array();
		$data['preferredContactDate'] = date('Y-m-d',strtotime('+'.$this->preferredContactDate.'day' ));
		$data['companyId'] = $this->companyId;
		$data['wholesalerId'] = $this->wholesalerId;
		$data['bankTypeId'] = $this->bankTypeId;
		$data['email'] = $this->email;
		$data['titleId'] = $this->titleId;
		$data['firstName'] = $this->firstName;
		$data['lastName'] = $this->lastName;
		$data['address1'] = $this->address1;
		$data['address2'] = $this->address2;
		$data['address3'] = $this->address3;
		$data['city'] = $this->city;
		$data['county'] = $this->county;
		$data['postcode'] = $this->postcode;
		$data['countryId'] = $this->countryId;
		$data['phone'] = preg_replace('/\s+/', '', $this->phone);
		$data['additionalPhone'] = preg_replace('/\s+/', '', $this->additionalPhone);
		$data['consent'] = $this->consent;
        
		// insert
		if ($action == 'insert'){
			$data['dtc'] = new DibiDateTime();
			$this->companyCustomerId = FApplication::$db->insert(TBL_COMPANY_CUSTOMER, $data)->execute(dibi::IDENTIFIER);
		// update
		} else {
			FApplication::$db->update(TBL_COMPANY_CUSTOMER, $data)->where('companyCustomerId=%i', $this->companyCustomerId)->execute();
		}
		return $this->companyCustomerId;
	}

	/**
	 * Provides deleting CompanyCustomer
	 * 
	 * @return void
	 */
	public function delete()
	{
		FApplication::$db->delete(TBL_COMPANY_CUSTOMER)->where('companyCustomerId=%i', $this->companyCustomerId)->execute();
	}
	
	
	/**
	 * Checks if customer for company is filled
	 *
	 * @param int $companyId
	 * @return boolean
	 */
	static public function hasCustomerCompanyFilled($companyId)
	{
		$result = dibi::select('*')->from(TBL_COMPANY_CUSTOMER)->where('companyId=%i', $companyId)->execute()->fetch();
		return ($result === FALSE) ? FALSE : TRUE;
	}

	/**
	 * Returns company customer by company ID
	 *
	 * @param int $companyId
	 * @param string $bankTypeId
	 * @return CompanyCustomerModel
	 * @throws Exception
	 */
	static public function getCompanyCustomerByCompanyId($companyId, $bankTypeId)
	{
		$companyCustomerId = dibi::select('companyCustomerId')
			->from(TBL_COMPANY_CUSTOMER)
			->where('companyId=%i', $companyId)
			->and('bankTypeId=%s', $bankTypeId)
			->execute()->fetchSingle();
		
		if ($companyCustomerId == FALSE)  {
			throw new Exception("Company Customer for Company Id ($companyId) doesn't exist!");
		}
		
		$companyCustomer = new self($companyCustomerId);
		return $companyCustomer;
	}
	
	/**
	 * Returns country as a text
	 *
	 * @return string
	 */
	public function getCountry()
	{
		return isset(Customer::$countries[$this->countryId]) ? Customer::$countries[$this->countryId] : NULL;	
	}
	
}
