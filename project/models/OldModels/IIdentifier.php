<?php

interface IIdentifier
{
    public function getId();
}