<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    WestburyAccountsConsultationModel.php 2010-09-20 divak@gmail.com
 */

class WestburyAccountsConsultationModel extends FNode
{
    /**
     * @param array $callback
     * @return FForm
     */
    public function getComponentDefaultForm(array $callback)
    {
        $form = new FForm('WestburyAccountsConsultationModelForm');
        
        $form->addText('email', 'Email: *')
            ->addRule(FForm::Required, 'Required!')
            ->addRule(FForm::Email, 'Not valid!')
            ->class('field220');
        $form->addText('fullName', 'Full name: *')
            ->addRule(FForm::Required, 'Required!')
            ->class('field220');
        $form->addText('phone', 'Contact number: *')
            ->addRule(FForm::Required, 'Required!')
            ->class('field220');
        $form->addText('postcode', 'Postcode: *')
            ->addRule(FForm::Required, 'Required!')
            ->class('field220');
        $form->addText('companyName', 'Company name: *')
            ->addRule(FForm::Required, 'Required!')
            ->class('field220');
        $form->addArea('details', 'Details: ')
            ->cols(40)
            ->rows(7);
        $form->addSubmit('send', 'Submit')
            ->class('btn_submit fleft mbottom mright');
            
        $form->onValid = $callback;
        $form->start();
        return $form;
    }
    
    /**
     * Enter description here...
     *
     * @param FForm $form
     * @throws Exception
     */
    public function processDefaultForm(FForm $form)
    {
        try {
            $data = $form->getValues();
                        $westburyEmailer = Registry::$emailerFactory->get(EmailerFactory::WESTBURY);
                        $westburyEmailer->consultationEmail($data);
            $form->clean();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
             $this->redirect();
            throw $e;
        }
    }
}