<?php

class CompanyOtherDocumentsModel extends FNode
{
    
    /**
     * Delete a uploaded file for a specific company
     *
     * @param int $documentId
     * @param Company $company
     * @param UDocumentsUploadControler $controler
     */
    public function deleteUploadedFile($documentId) 
    {
        $document = new CompanyOtherDocument($documentId);
        $document->delete();
    }
    
    /**
     * Check max number of allowed upload file
     *
     * @param Company $company
     * @throws Exception
     */
    public function checkMaxFilesUpload(Company $company) 
    {
        if (CompanyOtherDocument::TYPE_OTHERS_MAX_NUMBER <= $this->getNumberOfFiles($company)) {
            throw new Exception('Max number of files: 10');
        }
    }
    
    /**
     * Get number of file for a company
     *
     * @param Company $company
     * @return int $nr
     */
    public function getNumberOfFiles(Company $company) {
        return CompanyOtherDocument::getCount(array('companyId' => $company->getCompanyId()));
    }

  
}