<?php

use DusanKasan\Knapsack\Collection;
use PeopleWithSignificantControl\Views\AnnualReturnPscView;
use PeopleWithSignificantControl\Views\AnnualReturnSummaryView;
use Utils\Date;

class AnnualReturnModel extends Object
{
    /*     * ********************************************* company details ****************************************** */

    /**
     * Returns Company details form
     *
     * @param FControler $controler
     * @param array $data
     * @param string $type
     * @return FForm
     */
    public static function getCompomentCompanyDetailsForm(FControler $controler, array $officers, array $data, $type = 'front')
    {

        $form = new FForm('companyDetails');
        $form->addFieldset('Return Date');
        //$dataPicker = $form->add('DatePicker', 'returnDate', 'Return Date:')->style(' width: 70px;')->class('date');
        //$dataPicker->setOption('dateFormat', 'dd-mm-yy');
        //$dataPicker->setValueDMY($data['returnDate']);

        $datePicker = $form->add('DatePickerNew', 'returnDate', 'Return Date:')->style(' width: 70px;')->class('date')
            ->addRule(FForm::Required, 'Please select your Return Date')
            ->addRule('DateFormat', 'Please enter date format as DD-MM-YYYY', 'd-m-Y')
            ->addRule('DateMax', 'Date cannot be in the future', array('today', 'd-m-Y'))
            ->setDescription('Date cannot be in the future');
        $datePicker->class('date')->setValueDMY($data['returnDate']);

        $form->addHidden('CompanyCategory', $controler->chAnnualReturn->getCompanyCategory())->class('CompanyCategory');
        if ($controler->chAnnualReturn->getCompanyCategory() != 'LLP') {
            $form->addFieldset('Trading On Regulated Market');
            $form->add('RadioInline', 'trading', 'Please select *')
                ->setOptions(array(1 => 'Yes', 0 => 'No'))->setValue($data['trading'])
                ->addRule(FForm::Required, 'Please Provide Type');

            $form->addFieldset('DTR5');
            $form->add('RadioInline', 'dtr5', 'DTR5 Applies *')
                ->setOptions(array(1 => 'Yes', 0 => 'No'))->setValue($data['dtr5'])
                ->addRule(FForm::Required, 'Please Provide DTR5Applies');

            $form->addFieldset('Codes');
            for ($i = 1; $i < 5; $i++) {
                $sic = $form->addText('code_' . $i, "Classification Code $i")
                    ->setGroup('classificationCode')
                    ->addRule(array('AnnualReturnModel', 'Validator_requiredCompanyDetailsCode'), 'Please Provide at Least One Code')
                    ->addRule(FForm::REGEXP, 'Please provide 5 digit number', '#^(\d){5}$#')
                    ->size(4);
                if (isset($data['code_' . $i]) && strlen($data['code_' . $i]) == 5) {
                    $sic->setValue($data['code_' . $i]);
                }
            }
        }

        if (empty($data['premise']) || empty($data['street'])) {
            $form->addFieldset('Registered Office');
            $form->addText('premise', 'Premise *')->setValue($data['premise'])
                ->addRule(FForm::Required, 'Please Provide Premise');
            $form->addText('street', 'Street *')->setValue($data['street'])
                ->addRule(FForm::Required, 'Please Provide Street');
        }
        // officers
        foreach ($officers as $officerId => $officer) {
            $form->add('AnnualReturnOfficerForm', $officerId, $officer['name'])
                ->setOfficer($officer)
                ->setValue($officer);
        }

        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Continue >')->style('height: 30px; width: 200px;');

        if ($type === 'admin') {
            $form['submit']->class('btn');
            $form->onValid = array($controler, 'FormCompanyDetailsSubmitted');
        } else {
            $form->onValid = array($controler, 'Form_companyDetailsFormSubmitted');
        }

        $form->start();

        return $form;
    }

    public function Validator_futureDate(DatePicker $control, $error, $params)
    {
        $value = $control->getValue();
        if (strtotime($value) >= strtotime(date('d-m-Y'))) {
            return $error;
        }
        return TRUE;
    }

    /**
     * Returns company details form data
     *
     * @param AnnualReturn $chAnnualReturn
     * @param string $type
     * @return array
     */
    public static function getCompanyDetailsData(CHAnnualReturn $chAnnualReturn, $type = 'front')
    {
        $data = array();

        // return date
        $data['returnDate'] = $chAnnualReturn->getMadeUpDate();
        $address = $chAnnualReturn->getRegOffice();
        $data['premise'] = $address->getPremise();
        $data['street'] = $address->getStreet();
        if ($chAnnualReturn->getCompanyCategory() != 'LLP') {
            $data['trading'] = $chAnnualReturn->getTrading();
            $data['dtr5'] = $chAnnualReturn->getDtr5();
        }
        // description
        $data['type'] = 1;
        foreach ($chAnnualReturn->getSicCodes() as $key => $value) {
            $keyname = 'code_' . ($key + 1);
            $data[$keyname] = $value;
        }

        return $data;
    }

    /**
     * Provides saving company details
     *
     * @param CHAnnualReturn $annualReturn
     * @param array $data
     * @return void
     * @throws Exception
     */
    public static function saveCompanyDetails(CHAnnualReturn $annualReturn, array $data)
    {
        try {
            $annualReturn->setMadeUpDate($data['returnDate']);
            if ($annualReturn->getCompanyCategory() != 'LLP') {
                $dataTrading = isset($data['trading']) ? $data['trading'] : 0;
                $dtr5 = isset($data['dtr5']) ? $data['dtr5'] : 0;
                $annualReturn->setDtr5($dtr5);
                $annualReturn->setTrading($dataTrading);
                $annualReturn->setSicCodes($data['classificationCode']);
            }
            if (isset($data['premise']) && isset($data['street'])) {
                $regoffice = $annualReturn->getRegOffice();
                $regoffice->setPremise($data['premise']);
                $regoffice->setStreet($data['street']);
            }

            // officers
            if (isset($data['officers'])) {
                foreach ($data['officers'] as $key => $officer) {
                    $annualReturn->updateOfficer($key, $officer);
                }
            }

            $annualReturn->save();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /*     * ********************************************* capitals ****************************************** */

    /**
     * Returns capital form
     *
     * @param FControler $controler
     * @param array $capitals
     * @param string $type
     * @return FForm
     */
    public static function getComponentCapitalForm(FControler $controler, array $capitals, $type = 'front')
    {
        $form = new FForm('capital');
        $form->setRenderClass('Default2Render');
        foreach ($capitals as $key => $capital) {

            $form->addFieldset('Capital');
            $form->addText('share_class_' . $key, 'Share Class *')
                ->setValue($capital['share_class'])
                ->addRule(FForm::Required, 'Please provide');
            $form->addText('share_currency_' . $key, 'Share Currency')
                ->readonly()->style('border: 0;')
                ->setValue($capital['currency']);
            $form->addText('num_shares_' . $key, 'Number of Shares')
                ->readonly()->style('border: 0;')
                ->setValue($capital['num_shares']);
            $form->addText('share_value_' . $key, 'Share Value')
                ->readonly()->style('border: 0;')
                ->setValue($capital['share_value']);

            if ($type === 'admin') {
                $form['share_currency_' . $key]->style('background: white;  border: 0;');
                $form['num_shares_' . $key]->style('background: white;  border: 0;');
                $form['share_value_' . $key]->style('background: white;  border: 0;');
            }

            // paid
            $paid = array('paid' => 'Fully Paid', 'unpaid' => 'Unpaid', 'partialy_paid' => 'Partially Paid');
            $paid = array_intersect_key($paid, $capital['paid']);
            $form->addRadio('paid_' . $key, '&nbsp;Amount Paid <br />&nbsp;(per Share)', $paid);

            // set value for paid
            foreach ($capital['paid'] as $key2 => $val2) {
                if ($val2 == 1) {
                    $form['paid_' . $key]->setValue($key2);
                    break;
                }
            }

            if (isset($capital['partialy_paid'])) {
                $form->addText('partialy_paid_' . $key, NULL)
                    ->setValue($capital['partialy_paid'])
                    ->addRule(array('AnnualReturnModel', 'Validator_capitalPaidRequired'), 'Please provide value for partially paid')
                    ->addRule(FForm::REGEXP, 'Please provide numeric value', '#^([\d\.])+$#');
            }
        }

        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Continue >')->style('width: 200px; height: 30px;');

        if ($type === 'admin') {
            $form['submit']->class('btn');
            $form->onValid = array($controler, 'FormCapitalSubmitted');
        } else {
            $form->onValid = array($controler, 'Form_capitalSubmitted');
        }

        $form->start();
        return $form;
    }

    /**
     * Provides saving capital
     *
     * @param CHAnnualReturn $annualReturn
     * @param array $capitals
     * @param array $data
     * @return void
     * @throws Exception
     */
    public static function saveCapital(CHAnnualReturn $annualReturn, array $capitals, array $data)
    {
        try {
            foreach ($capitals as $key => $capital) {
                if (isset($data['partialy_paid_' . $key])) {
                    $annualReturn->setStatementOfCapital($key, $data['share_class_' . $key], $data['paid_' . $key], $data['partialy_paid_' . $key]);
                } else {
                    $annualReturn->setStatementOfCapital($key, $data['share_class_' . $key], $data['paid_' . $key]);
                }
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Validator for capital - partial paid value
     *
     * @param Text $control
     * @param string $error
     * @param array $params
     * @return mixed
     */
    public static function Validator_capitalPaidRequired(Text $control, $error, array $params)
    {
        $id = explode('_', $control->getName());
        $id = 'paid_' . $id[2];
        $paid = $control->owner[$id]->getValue();
        if ($paid == 'partialy_paid' && $control->getValue() == '') {
            return $error;
        }
        return TRUE;
    }

    /*     * ********************************************* add shareholding ****************************************** */

    /**
     * Provides create new shareholding
     *
     * @param Company $company
     * @param array $data
     * @return void
     * @throws Exception
     */
    public static function addShareholding(Shareholding $shareholding, array $data)
    {
        try {
            $shareholding->setNumberHeld($data['number_held']);
            $shareholding->setShareClass($data['share_class']);
            $shareholding->save();
            $shareholder = $shareholding->getNewShareholder();
            $shareholder->setFields($data);
            $shareholder->save();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Provides edit shareholding
     *
     * @param Company $company
     * @param Shareholding $shareholding
     * @param array $data
     * @return void
     * @throws Exception
     */
    public static function editShareholding(Shareholding $shareholding, array $data)
    {
        try {

            $shareholding->setNumberHeld($data['number_held']);

            if ($shareholding->isSynced() == FALSE || $shareholding->getShareClass() == NULL) {
                $shareholding->setShareClass($data['share_class']);
            }
            $shareholder = $shareholding->getShareholders();
            foreach ($shareholder as $value) {
                $value->setFields($data);
                $value->save();
            }
            $shareholding->save();


            // shareholders
            if ($shareholding->isSynced() == FALSE) {
                $shareholder = $shareholding->getShareholders();
                foreach ($shareholder as $value) {
                    $value->setFields($data);
                    $value->save();
                }
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Provides remove shareholding
     *
     * @param Shareholding $shareholding
     * @return void
     * @throws Exception
     */
    public static function removeShareholding(Shareholding $shareholding)
    {
        try {
            $shareholding->remove();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Provides create new transfer
     *
     * @param Company $company
     * @param array $data
     * @return void
     * @throws Exception
     */
    public static function addTransfer(Company $company, Shareholding $shareholding, array $data)
    {

        try {
            $transfer = $shareholding->getNewTransfer();
            $transfer->setDateOfTransfer($data['dateOfTransfer']);
            $transfer->setSharesTransfered($data['numberOfSharesTransfered']);
            $transfer->save();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Provides remove transfer
     *
     * @param Shareholding $shareholding
     * @param $transferId
     * @return void
     * @throws Exception
     */
    public static function removeTransfer(Shareholding $shareholding, $transferId)
    {
        try {
            $transfer = $shareholding->getTransfer($transferId);
            if (isset($transfer)) {
                foreach ($transfer as $value) {
                    $value->remove();
                }
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * returns array of all summary details
     *
     * @param CHAnnualReturn $annualReturn
     * @param Company $company
     * @return array
     */
    public static function getSummaryDetails($annualReturn, $company)
    {
        $return = $annualReturn->getFields();
        $return['form_submission_id'] = $annualReturn->getFormSubmissionId();

        $return['company_name'] = $company->getCompanyName();
        $return['company_number'] = $company->getCompanyNumber();
        if ($company->getType() != 'LLP') {
            $return['trading'] = $annualReturn->getTrading();
            $return['dtr5'] = $annualReturn->getDtr5();
            $shares = $annualReturn->getShares();
            foreach ($shares as $key => $value) {
                $return['shares'][$key] = $value;
            }
            $shareholdings = $annualReturn->getShareholdings();
            foreach ($shareholdings as $value) {
                if ($value instanceof Shareholding) {
                    $return['shareholdings'][] = self::getShareholdingArray($value);
                }
            }
        }
        $officers = $annualReturn->getOfficers();
        foreach ($officers as $key => $value) {
            if ($value['type'] == 'DIR')
                $value['type'] = 'Director';
            elseif ($value['type'] == 'SEC')
                $value['type'] = 'Secretary';
            elseif ($value['type'] == 'MEM')
                $value['type'] = 'Member';
            $return['officers'][$key] = $value;
        }
        return $return;
    }

    /*     * ************************************ components ******************************************* */

    /**
     * Returns shareholdings array
     *
     * @param array of objects $shareholdings
     * @return array
     */
    public static function getShareholdingsArray($shareholdings)
    {
        if (!empty($shareholdings)) {
            foreach ($shareholdings as $key => $value) {
                $shareholding[$key]['synced'] = $value->isSynced();
                $shareholding[$key]['share_class'] = $value->getShareClass();
                $shareholding[$key]['number_held'] = $value->getNumberHeld();
                $shareholders = $value->getShareholders();
                foreach ($shareholders as $k => $v) {
                    $name = $v->getForename();
                    $name .= $v->getForename() ? ' ' . $v->getMiddleName() : $v->getMiddleName();
                    $name .= $v->getMiddleName() ? ' ' . $v->getSurname() : $v->getSurname();
                    $shareholding[$key]['shareholders'][$k]['name'] = $name;
                }
                $transfers = $value->getTransfers();
                foreach ($transfers as $k => $v) {
                    $array = $v->getFields();
                    $shareholding[$key]['transfers'][$k]['date'] = $array['date_of_transfer'];
                    $shareholding[$key]['transfers'][$k]['num'] = $array['shares_transfered'];
                }
            }
            return $shareholding;
        }
        return NULL;
    }

    /**
     * Returns shareholdings form
     *
     * @param FControler $controler
     * @param string $type
     * @return FForm
     */
    public static function getCompomentShareholdingsForm(FControler $controler, $type = 'front')
    {
        $form = new FForm('shareholdings');
        $form->setRenderClass('Default2Render');
        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Continue >');

        if ($type == 'admin') {
            $form['submit']->style('width: 200px; height: 30px;')->class('btn');
            $form->onValid = array($controler, 'FormShareholdingsSubmitted');
        } else {
            $form['submit']->style('width: 200px; height: 30px;');
            $form->onValid = array($controler, 'Form_shareholdingsSubmitted');
        }

        $form->start();
        return $form;
    }

    /**
     * @param FControler $controler
     * @param string $type
     * @return FForm
     */
    public static function getCompomentPscssForm(FControler $controler, $type = 'front')
    {
        $form = new FForm('pscs');
        $form->setRenderClass('Default2Render');
        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Continue >');

        if ($type == 'admin') {
            $form['submit']->style('width: 200px; height: 30px;')->class('btn');
            $form->onValid = array($controler, 'FormPscsSubmitted');
        } else {
            $form['submit']->style('width: 200px; height: 30px;');
            $form->onValid = array($controler, 'Form_pscsSubmitted');
        }

        $form->start();
        return $form;
    }

    /**
     * Returns shareholding array for edit action
     *
     * @param object $shareholding
     * @return array
     */
    public static function getShareholdingArray(Shareholding $shareholding)
    {
        $result = array();

        $result['annual_return_shareholding_id'] = $shareholding->getId();
        $result['share_class'] = $shareholding->getShareClass();
        $result['number_held'] = $shareholding->getNumberHeld();

        $x = 0;
        $shareholders = $shareholding->getShareholders();
        foreach ($shareholders as $shareholder) {
            $shareholderFields = $shareholder->getFields();
            $result['shareholder'][$x]['annual_return_shareholder_id'] = $shareholder->getId();
            $result['shareholder'][$x]['forename'] = $shareholderFields['forename'];
            $result['shareholder'][$x]['middle_name'] = $shareholderFields['middle_name'];
            $result['shareholder'][$x]['surname'] = $shareholderFields['surname'];
            $result['shareholder'][$x]['premise'] = $shareholderFields['premise'];
            $result['shareholder'][$x]['street'] = $shareholderFields['street'];
            $result['shareholder'][$x]['thoroughfare'] = $shareholderFields['thoroughfare'];
            $result['shareholder'][$x]['post_town'] = $shareholderFields['post_town'];
            $result['shareholder'][$x]['county'] = $shareholderFields['county'];
            $result['shareholder'][$x]['country'] = $shareholderFields['country'];
            $result['shareholder'][$x]['postcode'] = $shareholderFields['postcode'];
            $result['shareholder'][$x]['isRemoved'] = $shareholderFields['isRemoved'];
            $x++;
        }

        $x = 0;
        $transfers = $shareholding->getTransfers();
        foreach ($transfers as $transfer) {
            $result['transfer'][$x]['annual_return_transfer_id'] = $transfer->getId();
            $result['transfer'][$x]['date_of_transfer'] = $transfer->getDateOfTransfer();
            $result['transfer'][$x]['shares_transfered'] = $transfer->getSharesTransfered();
            $x++;
        }

        return $result;
    }

    /**
     * Returns shareholding array data
     *
     * @param $data
     * @return array
     */
    public static function getShareholdingDataInfo($data)
    {
        $result = array();

        $result['annual_return_shareholding_id'] = $data['annual_return_shareholding_id'];
        $result['share_class'] = $data['share_class'];
        $result['number_held'] = $data['number_held'];

        if (isset($data['shareholder']) && !empty($data['shareholder'])) {
            $result['annual_return_shareholder_id'] = $data['shareholder'][0]['annual_return_shareholder_id'];
            $result['forename'] = $data['shareholder'][0]['forename'];
            $result['middle_name'] = $data['shareholder'][0]['middle_name'];
            $result['surname'] = $data['shareholder'][0]['surname'];
            $result['premise'] = $data['shareholder'][0]['premise'];
            $result['street'] = $data['shareholder'][0]['street'];
            $result['thoroughfare'] = $data['shareholder'][0]['thoroughfare'];
            $result['post_town'] = $data['shareholder'][0]['post_town'];
            $result['county'] = $data['shareholder'][0]['county'];
            $result['country'] = $data['shareholder'][0]['country'];
            $result['postcode'] = $data['shareholder'][0]['postcode'];
            $result['isRemoved'] = $data['shareholder'][0]['isRemoved'];
        }

        return $result;
    }

    /**
     * Returns add shareholding's form
     *
     * @param FControler $controler
     * @param string $action
     * @param array $data
     * @param string $type
     * @return FForm
     */
    public static function getCompomentShareholdingForm(FControler $controler, $action = 'insert', array $data = array(), $type = 'front')
    {
        if ($action === 'edit') {
            $form = new FForm('editShareholding');
        } else {
            $form = new FForm('addShareholding');
        }
        $form->addFieldset('Shareholding');

        if ($controler->shareholding->isSynced() == FALSE || $controler->shareholding->getShareClass() == NULL) {
            $form->addSelect('share_class', 'Share Class *', $controler->shareholding->getAvailableShareClasses())
                ->setFirstOption('--- Select ---')
                ->addRule(FForm::Required, 'Please Provide Share Class');
        }

        $form->addText('number_held', 'Number Held *')
            ->addRule(FForm::Required, 'Please Provide Number Held')
            ->addRule(FForm::REGEXP, 'Please Provide Number', '#^(\d)+$#')
            //->addRule(FForm::GREATER_THAN, 'Value must be greater than 0', 0)
            ->setDescription("(Available Shares: {$controler->shareholding->getAvailableShares()})");

        if ($form->getName() == 'addShareholding') {
            $form->addFieldset('Shareholder');
            $form->addText('forename', 'Forename')
                ->addRule(FForm::MAX_LENGTH, 'Max #1 Characters', 50);
            $form->addText('middle_name', 'Middle Name')
                ->addRule(FForm::MAX_LENGTH, 'Max #1 Characters', 50);
            $form->addText('surname', 'Surname *')
                ->addRule(FForm::Required, 'Please Provide Surname')
                ->addRule(FForm::MAX_LENGTH, 'Max #1 Characters', 160);
        }

        $form->addFieldset('Address');
        $form->addText('premise', 'Premise *')
            ->addRule(FForm::Required, 'Please Provide Premise')
            ->addRule(FForm::MAX_LENGTH, 'Max #1 Characters', 50);
        $form->addText('street', 'Street *')
            ->addRule(FForm::Required, 'Please Provide Premise')
            ->addRule(FForm::MAX_LENGTH, 'Max #1 Characters', 50);
        $form->addText('thoroughfare', 'Thoroughfare')
            ->addRule(FForm::MAX_LENGTH, 'Max #1 Characters', 50);
        $form->addText('post_town', 'Post Town *')
            ->addRule(FForm::Required, 'Please Provide Post Town')
            ->addRule(FForm::MAX_LENGTH, 'Max #1 Characters', 50);
        $form->addText('county', 'County')
            ->addRule(FForm::MAX_LENGTH, 'Max #1 Characters', 70);
        $form->addSelect('country', 'Country *', Address::$countries)
            ->setFirstOption('--- Select ---')
            ->addRule(FForm::Required, 'Please Provide Country');
        $form->addText('postcode', 'PostCode *')
            ->addRule(FForm::Required, 'Please Provide PostCode')
            ->addRule(FForm::MAX_LENGTH, 'Max #1 Characters', 15);

        if ($form->getName() == 'editShareholding') {
            $form->addFieldset('Remove Address');

            $form->addCheckbox('isRemoved', 'Disable Address:', 1)
                ->setDescription('(The address will not be sent to Companies House!)');
        }


        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Save');


        // admin
        if ($type === 'admin') {
            if ($action === 'edit') {
                $newFormDate = AnnualReturnModel::getShareholdingDataInfo($data);
                $form->setInitValues($newFormDate);
                $form->onValid = array($controler, 'FormEditShareholdingSubmitted');
            } else {
                $form->onValid = array($controler, 'FormAddShareholdingSubmitted');
            }
            $form['submit']->class('btn');
            // front
        } else {
            if ($action === 'edit') {
                $newFormDate = AnnualReturnModel::getShareholdingDataInfo($data);
                $form->setInitValues($newFormDate);
                $form->onValid = array($controler, 'Form_editShareholdingSubmitted');
            } else {
                $form->onValid = array($controler, 'Form_addShareholdingSubmitted');
            }
            $form['submit']->class('btn_submit');
        }
        $form->start();
        return $form;
    }

    /**
     * Returns add transfer form
     *
     * @param FControler $controler
     * @param string $type
     * @return FForm
     */
    public static function getCompomentAddTransferForm(FControler $controler, $type = 'front')
    {
        $form = new FForm('addTransfer');

        $form->addFieldset('Data');
        $form->add('DateSelect', 'dateOfTransfer', 'Date of Transfer')
            ->setValue(date('Y-m-d'));
        $form->addText('numberOfSharesTransfered', 'No. of Shares Transfered')
            ->addRule(FForm::Required, 'Please Provide No. of Shares Transfered')
            ->addRule(FForm::REGEXP, 'Please Provide Just Numbers', '#^(\d)+$#');

        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Save')->class('btn_submit');

        if ($type === 'admin') {
            $form->onValid = array($controler, 'FormAddTransferSubmitted');
            $form['submit']->class('btn');
        } else {
            $form->onValid = array($controler, 'Form_addTransferSubmitted');
            $form['submit']->class('btn_submit');
        }
        $form->start();
        return $form;
    }

    /**
     * Returns summary form
     *
     * @param FControler $controler
     * @param string $type
     * @param string $isService
     * @return FForm
     */
    public static function getCompomentSummaryForm(FControler $controler, $type = 'front', $isService = FALSE)
    {
        $form = new FForm('ARsummary');

        if ($type === 'front') {
            $form->addFieldset('Confirmation');
            $form->addCheckbox('confirm', 'I confirm all above details are true and correct', 1)
                ->addRule(FForm::Required, 'Please confirm details');
        }

        // action
        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Send Application')->style('width: 200px; height: 30px');


        if ($type === 'admin') {
            $form->onValid = array($controler, 'FormSummaryFormSubmitted');
            $form['submit']->class('btn')->onclick('return confirm("ONLY send this to Companies House if you have written approval from the client")');
        } else {
            $form->onValid = array($controler, 'Form_summaryFormSubmitted');
        }


        $form->start();
        return $form;
    }

    /**
     * Returns summary form
     *
     * @param FControler $controler
     * @param array $return
     * @return FForm
     */
    public static function getCompomentAdminSummaryForm(ARAdminControler $controler, Company $company, array $return)
    {

        $form = new FForm('ARsummary');
        /*
          pr("AnnualReturnModel - 736");
          pr($return);
         */

        $form->addFieldset('Company Details');
        $form->addHidden('form_submission_id', $return['form_submission_id']);
        //$form->addText('company_category', 'Company Category:')->setValue($return['company_category']);
        $companyType = array('PLC' => 'PLC', 'BYSHR' => 'BYSHR', 'BYGUAR' => 'BYGUAR',
            'BYSHREXUNDSEC60' => 'BYSHREXUNDSEC60', 'BYGUAREXUNDSEC60' => 'BYGUAREXUNDSEC60',
            'UNLCWSHRCAP' => 'UNLCWSHRCAP', 'UNLCWOSHRCAP' => 'UNLCWOSHRCAP', 'LLP' => 'LLP');

        $form->addSelect('company_category', 'Company Category:', $companyType)->setValue($return['company_category']);
        $date2 = $form->add('DatePickerNew', 'made_up_date', 'Made Up Date:')->class('date');
        $date2->setValue(!empty($return['made_up_date']) ? Date::changeFormat($return['made_up_date'], 'Y-m-d', 'd-m-Y') : date('d-m-Y'));

        if ($company->getType() != 'LLP') {
            $form->addSelect('sic_code_type', 'Sic Code Type:', array('code' => 'Code'))->setValue($return['sic_code_type']);
            $form->addSelect('members_list', 'Members List:', array('NONE' => 'None', 'FULL' => 'Full', 'NOCHANGE' => 'No Change'))->setValue($return['members_list']);
            $form->addFieldset('Sic Codes');
            $x = 0;
            foreach ($return['sic_code'] as $code) {
                $form->addText('sic_code_' . $x, 'Sic Code ' . ($x + 1) . ':')->setValue($code);
                $x++;
            }


            $form->addFieldset('Trading');
            $form->add('RadioInline', 'trading', 'TradingOnMarket *')->setOptions(array(1 => 'Yes', 0 => 'No'))->setValue($return['trading'])
                ->addRule(FForm::Required, 'Please Provide TradingOnMarket');

            $form->addFieldset('DTR5');
            $form->add('RadioInline', 'dtr5', 'DTR5Applies *')->setOptions(array(1 => 'Yes', 0 => 'No'))->setValue($return['dtr5'])
                ->addRule(FForm::Required, 'Please Provide DTR5Applies');
        }

        $form->addHidden('CompanyCategory', $controler->chAnnualReturn->getCompanyCategory())->class('CompanyCategory');
        $form->addFieldset('Registered Office');
        //pr($return['reg_office']['street']);exit;
        $form->addText('premise', 'Premise:')->setValue($return['reg_office']['premise']);
        $form->addText('street', 'Street:')->setValue($return['reg_office']['street']);
        $form->addText('thoroughfare', 'Thoroughfare:')->setValue($return['reg_office']['thoroughfare']);
        $form->addText('post_town', 'Post Town:')->setValue($return['reg_office']['post_town']);
        $form->addText('county', 'County:')->setValue($return['reg_office']['county']);
        $form->addSelect('country', 'Country:', Address::$countries)->setValue($return['reg_office']['country']);
        $form->addText('postcode', 'Postcode:')->setValue($return['reg_office']['postcode']);
        $form->addText('care_of_name', 'Care of name:')->setValue($return['reg_office']['care_of_name']);
        $form->addText('po_box', 'PoBox:')->setValue($return['reg_office']['po_box']);
        $form->addText('secure_address_ind', 'Secure Address Ind:')->setValue($return['reg_office']['secure_address_ind']);

        $form->addFieldset('Sail address');
        $form->addText('sail_premise', 'Premise:')->setValue($return['sail_address']['premise']);
        $form->addText('sail_street', 'Street:')->setValue($return['sail_address']['street']);
        $form->addText('sail_thoroughfare', 'Thoroughfare:')->setValue($return['sail_address']['thoroughfare']);
        $form->addText('sail_post_town', 'Post Town:')->setValue($return['sail_address']['post_town']);
        $form->addText('sail_county', 'County:')->setValue($return['sail_address']['county']);
        $form->addSelect('sail_country', 'Country:', array_merge(Address::$countries, array('UNDEF' => 'UNDEF')))->setValue($return['sail_address']['country']);
        $form->addText('sail_postcode', 'Postcode:')->setValue($return['sail_address']['postcode']);
        $form->addText('sail_care_of_name', 'Care of name:')->setValue($return['sail_address']['care_of_name']);
        $form->addText('sail_po_box', 'PoBox:')->setValue($return['sail_address']['po_box']);
        $form->addText('sail_secure_address_ind', 'Secure Address Ind:')->setValue($return['sail_address']['secure_address_ind']);

        $currencies = array('GBP' => 'GBP', 'USD' => 'USD', 'EUR' => 'EUR', 'AED' => 'AED', 'AFN' => 'AFN', 'ALL' => 'ALL',
            'AMD' => 'AMD', 'ANG' => 'ANG', 'AOA' => 'AOA', 'ARS' => 'ARS',
            'AUD' => 'AUD', 'AWG' => 'AWG', 'AZN' => 'AZN', 'BAM' => 'BAM', 'BBD' => 'BBD', 'BDT' => 'BDT', 'BGN' => 'BGN', 'BHD' => 'BHD', 'BIF' => 'BIF', 'BMD' => 'BMD',
            'BND' => 'BND', 'BOB' => 'BOB', 'BOV' => 'BOV', 'BRL' => 'BRL', 'BSD' => 'BSD', 'BTN' => 'BTN', 'BWP' => 'BWP', 'BYR' => 'BYR', 'BZD' => 'BZD', 'CAD' => 'CAD',
            'CDF' => 'CDF', 'CHE' => 'CHE', 'CHF' => 'CHF', 'CHW' => 'CHW', 'CLF' => 'CLF', 'CLP' => 'CLP', 'CNY' => 'CNY', 'COP' => 'COP', 'COU' => 'COU', 'CRC' => 'CRC',
            'CUC' => 'CUC', 'CUP' => 'CUP', 'CVE' => 'CVE', 'CZK' => 'CZK', 'DJF' => 'DJF', 'DKK' => 'DKK', 'DOP' => 'DOP', 'DZD' => 'DZD', 'EEK' => 'EEK', 'EGP' => 'EGP',
            'ERN' => 'ERN', 'ETB' => 'ETB', 'FJD' => 'FJD', 'FKP' => 'FKP', 'GEL' => 'GEL', 'GHS' => 'GHS', 'GIP' => 'GIP', 'GMD' => 'GMD',
            'GNF' => 'GNF', 'GTQ' => 'GTQ', 'GYD' => 'GYD', 'HKD' => 'HKD', 'HNL' => 'HNL', 'HRK' => 'HRK', 'HTG' => 'HTG', 'HUF' => 'HUF', 'IDR' => 'IDR', 'ILS' => 'ILS',
            'INR' => 'INR', 'IQD' => 'IQD', 'IRR' => 'IRR', 'ISK' => 'ISK', 'JMD' => 'JMD', 'JOD' => 'JOD', 'JPY' => 'JPY', 'KES' => 'KES', 'KGS' => 'KGS', 'KHR' => 'KHR',
            'KMF' => 'KMF', 'KPW' => 'KPW', 'KRW' => 'KRW', 'KWD' => 'KWD', 'KYD' => 'KYD', 'KZT' => 'KZT', 'LAK' => 'LAK', 'LBP' => 'LBP', 'LKR' => 'LKR', 'LRD' => 'LRD',
            'LSL' => 'LSL', 'LTL' => 'LTL', 'LVL' => 'LVL', 'LYD' => 'LYD', 'MAD' => 'MAD', 'MDL' => 'MDL', 'MGA' => 'MGA', 'MKD' => 'MKD', 'MMK' => 'MMK', 'MNT' => 'MNT',
            'MOP' => 'MOP', 'MRO' => 'MRO', 'MUR' => 'MUR', 'MVR' => 'MVR', 'MWK' => 'MWK', 'MXN' => 'MXN', 'MXV' => 'MXV', 'MYR' => 'MYR', 'MZN' => 'MZN', 'NAD' => 'NAD',
            'NGN' => 'NGN', 'NIO' => 'NIO', 'NOK' => 'NOK', 'NPR' => 'NPR', 'NZD' => 'NZD', 'OMR' => 'OMR', 'PAB' => 'PAB', 'PEN' => 'PEN', 'PGK' => 'PGK', 'PHP' => 'PHP',
            'PKR' => 'PKR', 'PLN' => 'PLN', 'PYG' => 'PYG', 'QAR' => 'QAR', 'RON' => 'RON', 'RSD' => 'RSD', 'RUB' => 'RUB', 'RWF' => 'RWF', 'SAR' => 'SAR', 'SBD' => 'SBD',
            'SCR' => 'SCR', 'SDG' => 'SDG', 'SEK' => 'SEK', 'SGD' => 'SGD', 'SHP' => 'SHP', 'SLL' => 'SLL', 'SOS' => 'SOS', 'SRD' => 'SRD', 'STD' => 'STD', 'SYP' => 'SYP',
            'SZL' => 'SZL', 'THB' => 'THB', 'TJS' => 'TJS', 'TMT' => 'TMT', 'TND' => 'TND', 'TOP' => 'TOP', 'TRY' => 'TRY', 'TTD' => 'TTD', 'TWD' => 'TWD', 'TZS' => 'TZS',
            'UAH' => 'UAH', 'UGX' => 'UGX', 'USN' => 'USN', 'USS' => 'USS', 'UYU' => 'UYU', 'UZS' => 'UZS', 'VEF' => 'VEF', 'VND' => 'VND', 'VUV' => 'VUV',
            'WST' => 'WST', 'XAF' => 'XAF', 'XAG' => 'XAG', 'XAU' => 'XAU', 'XBA' => 'XBA', 'XBB' => 'XBB', 'XBC' => 'XBC', 'XBD' => 'XBD', 'XCD' => 'XCD', 'XDR' => 'XDR',
            'XFU' => 'XFU', 'XOF' => 'XOF', 'XPD' => 'XPD', 'XPF' => 'XPF', 'XPT' => 'XPT', 'XTS' => 'XTS', 'XXX' => 'XXX', 'YER' => 'YER', 'ZAR' => 'ZAR', 'ZMK' => 'ZMK',
            'ZWL' => 'ZWL');
        if ($company->getType() != 'LLP') {
            // shares
            if (isset($return['shares'])) {
                $x = 0;
                foreach ($return['shares'] as $share) {
                    $form->addFieldset('Share');
                    $form->addHidden('share_annual_return_shares_id_' . $x, $share['annual_return_shares_id']);
                    $form->addText('share_share_class_' . $x, 'Share class:')->setValue($share['share_class']);
                    $form->addText('share_prescribed_particulars_' . $x, 'Prescribed Particulars:')->setValue($share['prescribed_particulars']);
                    $form->addText('share_num_shares_' . $x, 'Num Shares:')->setValue($share['num_shares']);
                    $form->addText('share_amount_paid_' . $x, 'Amount Paid:')->setValue($share['amount_paid']);
                    $form->addText('share_amount_unpaid_' . $x, 'Amount Unpaid:')->setValue($share['amount_unpaid']);
                    //$form->addText('share_currency_'.$x, 'Curency:')->setValue($share['currency']);
                    $form->addSelect('share_currency_' . $x, 'Curency:', $currencies)->setValue($share['currency']);
                    $form->addText('share_aggregate_nom_value_' . $x, 'Aggregate Nom Value:')->setValue($share['aggregate_nom_value']);
                    $x++;
                }
            }
        }
        // officers

        $officers = [];
        $pscs = [];
        if (isset($return['officers'])) {
            $grouped = Collection::from($return['officers'])
                ->groupBy(function (array $officerData) {
                    return $officerData['type'] == 'PSC' ? 'pscs' : 'officers';
                });

            $officers = $grouped->getOrDefault('officers', [], true)->toArray();
            $pscs = $grouped->getOrDefault('pscs', [], true)->toArray();
        }

        $officerIndex = 0;
        if (count($officers) > 0) {
            foreach ($officers as $officer) {
                $form->addFieldset('Officer');
                $form->addHidden('officer_annual_return_officer_id_' . $officerIndex, $officer['annual_return_officer_id']);
                //$form->addText('officer_type_'.$officerIndex, 'Type:')->setValue($officer['type']);
                $form->addSelect('officer_type_' . $officerIndex, 'Type:', array('Director' => 'Director', 'Secretary' => 'Secretary', 'Member' => 'Member'))->setValue($officer['type']);
                $form->addCheckbox('officer_corporate_' . $officerIndex, 'Corporate:', 1)->setValue($officer['corporate']);
                $form->addText('officer_title_' . $officerIndex, 'Title:')->setValue($officer['title']);
                $form->addText('officer_forename_' . $officerIndex, 'Forename:')->setValue($officer['forename']);
                $form->addText('officer_middle_name_' . $officerIndex, 'Middle Name:')->setValue($officer['middle_name']);
                $form->addText('officer_surname_' . $officerIndex, 'Surname:')->setValue($officer['surname']);
                $form->addText('officer_corporate_name_' . $officerIndex, 'Corporate Name:')->setValue($officer['corporate_name']);
                $form->addText('officer_premise_' . $officerIndex, 'Premise:')->setValue($officer['premise']);
                $form->addText('officer_street_' . $officerIndex, 'Street:')->setValue($officer['street']);
                $form->addText('officer_thoroughfare_' . $officerIndex, 'Thoroughfare:')->setValue($officer['thoroughfare']);
                $form->addText('officer_post_town_' . $officerIndex, 'Post town:')->setValue($officer['post_town']);
                $form->addText('officer_county_' . $officerIndex, 'County:')->setValue($officer['county']);
                //$form->addText('officer_country_'.$officerIndex, 'Country:')->setValue($officer['country']);
                $form->addSelect('officer_country_' . $officerIndex, 'Country:', Address::$countries)->setValue($officer['country']);
                $form->addText('officer_postcode_' . $officerIndex, 'Postcode:')->setValue($officer['postcode']);
                $form->addText('officer_care_of_name_' . $officerIndex, 'Care of Name:')->setValue($officer['care_of_name']);
                $form->addText('officer_same_as_reg_office_' . $officerIndex, 'Same as reg. office:')->setValue($officer['same_as_reg_office']);
                $form->addText('officer_previous_names_' . $officerIndex, 'Previous Names:')->setValue($officer['previous_names']);
                $form->addText('officer_dob_' . $officerIndex, 'DOB:')->setValue($officer['dob']);
                $form->addText('officer_nationality_' . $officerIndex, 'Nationality:')->setValue($officer['nationality']);
                $form->addText('officer_occupation_' . $officerIndex, 'Occupation:')->setValue($officer['occupation']);
                $form->addText('officer_country_of_residence_' . $officerIndex, 'Country of Residence:')->setValue($officer['country_of_residence']);
                if ($company->getType() == 'LLP') {
                    $form->addSelect('officer_designated_ind_' . $officerIndex, 'Designated Member:', array(0 => 'no', 1 => 'yes'))->setValue($officer['designated_ind']);
                }
                // country of residence is limited only to 50 characters
                //$form->addSelect('officer_country_of_residence_'.$officerIndex, 'Country of Residence:', Address::$countries)->setValue($officer['country_of_residence']);
                //$form->addText('officer_identification_type_'.$officerIndex, 'Identification Type:')->setValue($officer['identification_type']);
                $form->addSelect('officer_identification_type_' . $officerIndex, 'Identification Type:', array('EEA' => 'EEA', 'NonEEA' => 'NonEEA'))->setValue($officer['identification_type']);
                $form->addText('officer_place_registered_' . $officerIndex, 'Place Registered:')->setValue($officer['place_registered']);
                $form->addText('officer_registration_number_' . $officerIndex, 'Registration Number:')->setValue($officer['registration_number']);
                $form->addText('officer_law_governed_' . $officerIndex, 'Law Governed:')->setValue($officer['law_governed']);
                $form->addText('officer_legal_form_' . $officerIndex, 'Legal form:')->setValue($officer['legal_form']);
                $officerIndex++;
            }
        }

        if (count($pscs) > 0) {
            foreach ($pscs as $officer) {
                $form->addFieldset('PSC');
                $form->addHidden('officer_annual_return_officer_id_' . $officerIndex, $officer['annual_return_officer_id']);
                //$form->addText('officer_type_'.$officerIndex, 'Type:')->setValue($officer['type']);
                $form->addHidden('officer_type_' . $officerIndex, 'PSC');
                $form->addSelect('officer_corporate_' . $officerIndex, 'Type:', [0 => 'Person', 1 => 'Corporate', 2 => 'Legal Person'])->setValue($officer['corporate']);
                $form->addText('officer_title_' . $officerIndex, 'Title:')->setValue($officer['title']);
                $form->addText('officer_forename_' . $officerIndex, 'Forename:')->setValue($officer['forename']);
                $form->addText('officer_middle_name_' . $officerIndex, 'Middle Name:')->setValue($officer['middle_name']);
                $form->addText('officer_surname_' . $officerIndex, 'Surname:')->setValue($officer['surname']);
                $form->addText('officer_corporate_name_' . $officerIndex, 'Corporate Name:')->setValue($officer['corporate_name']);
                $form->addText('officer_premise_' . $officerIndex, 'Premise:')->setValue($officer['premise']);
                $form->addText('officer_street_' . $officerIndex, 'Street:')->setValue($officer['street']);
                $form->addText('officer_thoroughfare_' . $officerIndex, 'Thoroughfare:')->setValue($officer['thoroughfare']);
                $form->addText('officer_post_town_' . $officerIndex, 'Post town:')->setValue($officer['post_town']);
                $form->addText('officer_county_' . $officerIndex, 'County:')->setValue($officer['county']);
                //$form->addText('officer_country_'.$officerIndex, 'Country:')->setValue($officer['country']);
                $form->addSelect('officer_country_' . $officerIndex, 'Country:', Address::$countries)->setValue($officer['country']);
                $form->addText('officer_postcode_' . $officerIndex, 'Postcode:')->setValue($officer['postcode']);
                $form->addText('officer_care_of_name_' . $officerIndex, 'Care of Name:')->setValue($officer['care_of_name']);
                $form->addText('officer_same_as_reg_office_' . $officerIndex, 'Same as reg. office:')->setValue($officer['same_as_reg_office']);
                $form->addText('officer_previous_names_' . $officerIndex, 'Previous Names:')->setValue($officer['previous_names']);
                $form->addText('officer_dob_' . $officerIndex, 'DOB:')->setValue($officer['dob']);
                $form->addText('officer_nationality_' . $officerIndex, 'Nationality:')->setValue($officer['nationality']);
                $form->addText('officer_occupation_' . $officerIndex, 'Occupation:')->setValue($officer['occupation']);
                $form->addText('officer_country_of_residence_' . $officerIndex, 'Country of Residence:')->setValue($officer['country_of_residence']);
                if ($company->getType() == 'LLP') {
                    $form->addSelect('officer_designated_ind_' . $officerIndex, 'Designated Member:', array(0 => 'no', 1 => 'yes'))->setValue($officer['designated_ind']);
                }
                // country of residence is limited only to 50 characters
                //$form->addSelect('officer_country_of_residence_'.$officerIndex, 'Country of Residence:', Address::$countries)->setValue($officer['country_of_residence']);
                //$form->addText('officer_identification_type_'.$officerIndex, 'Identification Type:')->setValue($officer['identification_type']);
                $form->addSelect('officer_identification_type_' . $officerIndex, 'Identification Type:', array('PSC' => 'PSC'))->setValue($officer['identification_type']);
                $form->addText('officer_place_registered_' . $officerIndex, 'Place Registered:')->setValue($officer['place_registered']);
                $form->addText('officer_registration_number_' . $officerIndex, 'Registration Number:')->setValue($officer['registration_number']);
                $form->addText('officer_law_governed_' . $officerIndex, 'Law Governed:')->setValue($officer['law_governed']);
                $form->addText('officer_legal_form_' . $officerIndex, 'Legal form:')->setValue($officer['legal_form']);

                switch ($officer['corporate']) {
                    case 1: $psc = new AnnualReturnCorporatePsc($company->getAnnualReturn()->getFormSubmissionId(), $officer['annual_return_officer_id']);
                        break;
                    case 2: $psc = new AnnualReturnLegalPersonPsc($company->getAnnualReturn()->getFormSubmissionId(), $officer['annual_return_officer_id']);
                        break;
                    default: $psc = new AnnualReturnPersonPsc($company->getAnnualReturn()->getFormSubmissionId(), $officer['annual_return_officer_id']);
                }

                $pscView = new AnnualReturnPscView($psc);
                $el = Html::el('ul');
                $el->style = 'list-style: none;';

                foreach ($pscView->getNatureOfControlsTexts() as $noc) {
                    $el->add(Html::el('li')->setText($noc));
                }

                $html = new HtmlControl('noc_' . $officerIndex, 'Nature of control:', $el, $form);
                $form->addControl($html);

                switch ($officer['corporate']) {
                    case 1: $editLinkUrl = FApplication::$router->link('ARAdminControler::ANNUAL_RETURN_PAGE editCorporatePsc', ["company_id" => $company->getId(), "psc_id" => $psc->getId()]);
                        break;
                    case 2: $editLinkUrl = FApplication::$router->adminLink('ARAdminControler::ANNUAL_RETURN_PAGE editLegalPersonPsc', ["company_id" => $company->getId(), "psc_id" => $psc->getId()]);
                        break;
                    default: $editLinkUrl = FApplication::$router->adminLink('ARAdminControler::ANNUAL_RETURN_PAGE editPersonPsc', ["company_id" => $company->getId(), "psc_id" => $psc->getId()]);
                }

                $editLink = Html::el('a')
                    ->class('btn')
                    ->href($editLinkUrl)
                    ->setText('Edit');

                $editLinkContainer = Html::el('span');
                $editLinkContainer->add(Html::el('br'));
                $editLinkContainer->add($editLink);

                $form->addControl(new HtmlControl('edit_' . $officerIndex, '', $editLinkContainer, $form));

                $officerIndex++;
            }
        }

        if ($company->getType() != 'LLP') {
            // shareholdings
            if (isset($return['shareholdings'])) {
                $x = 0;
                foreach ($return['shareholdings'] as $share) {
                    $form->addFieldset('Shareholding');
                    $form->addHidden('shareholding_annual_return_shareholding_id_' . $x, $share['annual_return_shareholding_id']);
                    $form->addText('shareholding_share_class_' . $x, 'Share class:')->setValue($share['share_class']);
                    ;
                    $form->addText('shareholding_number_held_' . $x, 'Number Held:')->setValue($share['number_held']);
                    $i = 0;
                    foreach ($share['shareholder'] as $shareholder) {
                        $form->addHidden('shareholder_annual_return_shareholder_id_' . $x . '_' . $i, $shareholder['annual_return_shareholder_id']);
                        $form->addText('shareholder_forename_' . $x . '_' . $i, 'Forename:')->setValue($shareholder['forename']);
                        $form->addText('shareholder_middle_name_' . $x . '_' . $i, 'Middle Name:')->setValue($shareholder['middle_name']);
                        $form->addText('shareholder_surname_' . $x . '_' . $i, 'Surname:')->setValue($shareholder['surname']);
                        if (isset($shareholder['isRemoved']) && 0 == $shareholder['isRemoved']) {
                            $form->addText('shareholder_premise_' . $x . '_' . $i, 'Premise:')->setValue($shareholder['premise']);
                            $form->addText('shareholder_street_' . $x . '_' . $i, 'Street:')->setValue($shareholder['street']);
                            $form->addText('shareholder_thoroughfare_' . $x . '_' . $i, 'Thoroughfare:')->setValue($shareholder['thoroughfare']);
                            $form->addText('shareholder_post_town_' . $x . '_' . $i, 'Post town:')->setValue($shareholder['post_town']);
                            $form->addText('shareholder_county_' . $x . '_' . $i, 'County:')->setValue($shareholder['county']);
                            //$form->addText('shareholder_country_'.$x.'_'.$i, 'Country:')->setValue($shareholder['country']);
                            $form->addSelect('shareholder_country_' . $x . '_' . $i, 'Country:', Address::$countries)->setValue($shareholder['country']);
                            $form->addText('shareholder_postcode_' . $x . '_' . $i, 'Postcode:')->setValue($shareholder['postcode']);
                        }


                        $i++;
                    }
                    if (isset($share['transfer'])) {
                        $i = 0;
                        foreach ($share['transfer'] as $transfer) {
                            $form->addHidden('transfer_annual_return_transfer_id_' . $x . '_' . $i, $transfer['annual_return_transfer_id']);
                            $form->addText('transfer_date_of_transfer_' . $x . '_' . $i, 'Date of Transfer:')->setValue($transfer['date_of_transfer']);
                            $form->addText('transfer_shares_transfered_' . $x . '_' . $i, 'Shares Transfered:')->setValue($transfer['shares_transfered']);
                            $i++;
                        }
                    }
                    $x++;
                }
            }
        }
        // action
        $form->addFieldset('Action');

        // send email just for Annual return service
        if ($company->getAnnualReturnId() == AnnualReturn::ANNUAL_RETURN_SERVICE_PRODUCT) {
            $arsm = AnnualReturnServiceModel::getByCompanyId($company->getCompanyId());
            if ($arsm->statusId != AnnualReturnServiceModel::STATUS_COMPLETED) {
                $form->addSubmit('send_email', 'Send Email')->class('btn')->style('width: 200px; height: 30px');
            }
        }

        $form->addSubmit('submit', 'Send Application')->style('width: 200px; height: 30px');
        $form->addSubmit('save', 'Save')->style('width: 200px; height: 30px')->class('btn');

        $form->onValid = array($controler, 'FormSummaryFormSubmitted');
        $form['submit']->class('btn')->onclick('return confirm("ONLY send this to Companies House if you have written approval from the client")');
        //$form->setInitValues(self::getAdminSummaryFormInitValues($return));

        $form->clean();
        $form->start();
        return $form;
    }

    /**
     * Provides saving admin summary data
     *
     * @param array $data
     */
    public static function saveAdminSummaryData(array $data)
    {

        $annualReturnCols = array('form_submission_id', 'company_category', 'made_up_date', 'trading', 'dtr5', 'sic_code_type', 'sic_code', 'premise', 'street', 'thoroughfare', 'post_town', 'county',
            'country', 'postcode', 'care_of_name', 'po_box', 'sail_premise', 'sail_street', 'sail_thoroughfare', 'sail_post_town', 'sail_county', 'sail_country',
            'sail_postcode', 'sail_care_of_name', 'sail_po_box', 'sail_records', 'members_list', 'regulated_markets');

        $officerCols = array('annual_return_officer_id', 'type', 'corporate', 'title', 'forename', 'middle_name', 'surname', 'corporate_name', 'premise', 'street', 'thoroughfare',
            'post_town', 'county', 'country', 'postcode', 'care_of_name', 'po_box', 'same_as_reg_office', 'previous_names', 'dob', 'nationality', 'occupation',
            'country_of_residence', 'designated_ind', 'identification_type', 'place_registered', 'registration_number', 'law_governed', 'legal_form');

        $shareholdingCols = array('annual_return_shareholding_id', 'share_class', 'number_held');
        $shareholderCols = array('annual_return_shareholder_id', 'forename', 'middle_name', 'surname', 'premise', 'street', 'thoroughfare', 'post_town', 'county', 'country', 'postcode');
        $transferCols = array('annual_return_transfer_id', 'date_of_transfer', 'shares_transfered');

        $shareCols = array('annual_return_shares_id', 'share_class', 'prescribed_particulars', 'num_shares', 'amount_paid', 'amount_unpaid', 'currency', 'aggregate_nom_value',
            'sync_paid', 'paid');

        $annualReturn = array();
        $officers = array();
        $shareholding = array();
        $shares = array();

        /* annual return */
        foreach ($annualReturnCols as $v) {
            if ($v == 'sic_code') {
                //llp dont need cis codes
                if ($data['company_category'] != 'LLP') {
                    if ($data['sic_code_type'] == 'code') {
                        $sicCode = array();
                        $x = 0;
                        do {
                            $sicCode[$x] = $data['sic_code_' . $x];
                            $x++;
                            if (!array_key_exists('sic_code_' . $x, $data)) {
                                break;
                            }
                        } while (TRUE);
                        $sicCode = implode(';', $sicCode);
                    } else {
                        $sicCode = $data['sic_code_0'];
                    }
                    $annualReturn[$v] = $sicCode;
                }
            } else {
                if (array_key_exists($v, $data)) {
                    $value = trim($data[$v]);
                    if (!empty($value) || $value == 0) {
                        $annualReturn[$v] = $data[$v];
                    } else {
                        $annualReturn[$v] = NULL;
                    }
                }
            }
        }

        /* officers */
        $x = 0;
        do {
            /* columns in shareholder table */
            foreach ($officerCols as $v) {
                $key = 'officer_' . $v . '_' . $x;   // key from the submitted form
                if (array_key_exists($key, $data)) {
                    if ($v == 'type') {
                        if ($data[$key] == 'Director' || $data[$key] == 'director' || $data[$key] == 'dir') {
                            $data[$key] = 'DIR';
                        }
                        if ($data[$key] == 'Secretary' || $data[$key] == 'secretary' || $data[$key] == 'sec') {
                            $data[$key] = 'SEC';
                        }
                        if ($data[$key] == 'Member' || $data[$key] == 'member' || $data[$key] == 'mem') {
                            $data[$key] = 'MEM';
                        }
                        if ($data[$key] == 'PSC' || $data[$key] == 'psc') {
                            $data[$key] = 'PSC';
                        }
                    }
                    $value = trim($data[$key]);
                    if (!empty($value) || $value == 0) {
                        $officers[$x][$v] = $data[$key];
                    } else {
                        $officers[$x][$v] = NULL;
                    }
                }
                unset($data[$key]);
            }
            $x++;
            if (!array_key_exists('officer_type_' . $x, $data)) {  // this is set all the time, so we use it as a counter
                break;
            }
        } while (TRUE);
        //pr($officers);exit;
        // llp company type dont need shareholding and shares
        if ($data['company_category'] != 'LLP') {
            /* shareholdings */
            $x = 0;
            do {
                /* columns in shareholdings table */
                foreach ($shareholdingCols as $v) {
                    $key = 'shareholding_' . $v . '_' . $x;   // key from the submitted form
                    if (array_key_exists($key, $data)) {
                        $value = trim($data[$key]);
                        if (!empty($value) || $value == 0) {
                            $shareholding[$x][$v] = $data[$key];
                        } else {
                            $shareholding[$x][$v] = NULL;
                        }
                        unset($data[$key]);
                    }
                }
                $shareholding[$x]['shareholders'] = array();
                $shareholding[$x]['transfers'] = array();
                $i = 0;
                do {
                    foreach ($shareholderCols as $shareholder) {
                        $shareholderKey = 'shareholder_' . $shareholder . '_' . $x . '_' . $i;
                        if (array_key_exists($shareholderKey, $data)) {
                            $value = trim($data[$shareholderKey]);
                            if (!empty($value) || $value == 0) {
                                $shareholding[$x]['shareholders'][$i][$shareholder] = $data[$shareholderKey];
                            } else {
                                $shareholding[$x]['shareholders'][$i][$shareholder] = NULL;
                            }
                            unset($data[$shareholderKey]);
                        }
                    }
                    $i++;
                    if (!array_key_exists('shareholder_' . $shareholder . '_' . $x . '_' . $i, $data)) {
                        break;
                    }
                } while (TRUE);
                /* transfers */
                $i = 0;
                do {
                    foreach ($transferCols as $transfer) {
                        $transferKey = 'transfer_' . $transfer . '_' . $x . '_' . $i;
                        if (array_key_exists($transferKey, $data)) {
                            $value = trim($data[$transferKey]);
                            if (!empty($value) || $value == 0) {
                                $shareholding[$x]['transfers'][$i][$transfer] = $data[$transferKey];
                            } else {
                                $shareholding[$x]['transfers'][$i][$transfer] = NULL;
                            }
                            unset($data[$transferKey]);
                        }
                    }
                    $i++;
                    if (!array_key_exists('transfer_' . $transfer . '_' . $x . '_' . $i, $data)) {
                        break;
                    }
                } while (TRUE);
                $x++;
                if (!array_key_exists('shareholding_' . $v . '_' . $x, $data)) {  // this is set all the time, so we use it as a counter
                    break;
                }
            } while (TRUE);

            /* shares */
            $x = 0;
            do {
                /* columns in shareholder table */
                foreach ($shareCols as $v) {
                    $key = 'share_' . $v . '_' . $x;   // key from the submitted form
                    if (array_key_exists($key, $data)) {
                        $value = trim($data[$key]);
                        if (!empty($value) || $value == 0) {
                            $shares[$x][$v] = $data[$key];
                        } else {
                            $shares[$x][$v] = NULL;
                        }
                        if (($v == 'amount_paid' || $v == 'amount_unpaid') && $shares[$x][$v] == NULL) {
                            $shares[$x][$v] = 0;
                        }
                        unset($data[$key]);
                    }
                }
                $x++;
                if (!isset($data['share_currency_' . $x])) {  // this is set all the time, so we use it as a counter
                    break;
                }
            } while (TRUE);
        }
        /* ready for update */
        $data = $annualReturn;
        $data['officers'] = $officers;
        if ($data['company_category'] != 'LLP') {
            $data['shares'] = $shares;
            $data['shareholdings'] = $shareholding;
        }
        return CHAnnualReturn::getUpdatedAnnualReturn($data);
    }

    /**
     * Returns init values for admin summary form
     *
     * @param array $return
     * @return array
     */
    private static function getAdminSummaryFormInitValues(array $return)
    {
        $initValues = $return;
        foreach ($return['sic_code'] as $key => $val)
            $initValues['sic_code_' . $key] = $val;
        foreach ($return['reg_office'] as $key => $val)
            $initValues['reg_office_' . $key] = $val;
        foreach ($return['sail_address'] as $key => $val)
            $initValues['sail_address_' . $key] = $val;

        // shares
        foreach ($return['shares'] as $key => $share) {
            foreach ($share as $key2 => $val2) {
                $initValues['share_' . $key2 . '_' . $key] = $val2;
            }
        }

        // officers
        foreach ($return['officers'] as $key => $share) {
            foreach ($share as $key2 => $val2) {
                $initValues['officer_' . $key2 . '_' . $key] = $val2;
            }
        }

        // shareholdings
        /*
          foreach ($return['shareholdings'] as $key => $share)  {
          foreach ($share as $key2 => $val2) {
          $initValues['shareholding_'.$key2.'_'.$key] = $val2;
          }
          }
         */

        return $initValues;
    }

    /*     * **************************************** validators ***************************************** */

    /**
     * Provides validation return date for company details
     *
     * @param DateSelect $control
     * @param string $error
     * @param array $params
     * @return mixed
     */
    public static function Validator_returnDate(DateSelect $control, $error, array $params)
    {
        $returnDate = $control->getValue();

        // check date
        $date = explode('-', $returnDate);
        if (checkdate($date[1], $date[2], $date[0]) === FALSE)
            return 'Date is not valid';

        // return day can't be greater than today's date
        if ($returnDate > date('Y-m-d'))
            return $error;

        return TRUE;
    }

    /**
     * Provides validation description for company details
     *
     * @param Text $control
     * @param string $error
     * @param array $params
     * @return mixed
     */
    public static function Validator_requiredCompanyDetailsDescription(Text $control, $error, array $params)
    {
        $value = $control->getValue();
        $type = $control->owner['type']->getValue();
        if ($type == 2 && empty($value)) {
            return $error;
        }
        return TRUE;
    }

    /**
     * Provides validation code for company details
     *
     * @param Text $control
     * @param string $error
     * @param array $params
     * @return mixed
     */
    public static function Validator_requiredCompanyDetailsCode(Text $control, $error, array $params)
    {
        // just for first field
        if ($control->getName() == 'code_1') {
            $data = $control->owner->getValues();
            foreach ($data['classificationCode'] as $key => $val) {
                if (!empty($val)) {
                    return TRUE;
                }
            }
            return $error;
        }
        return TRUE;
    }

    /**
     * Provides CH respond - accept, reject
     *
     * @param Customer $customer
     * @param array $return
     * @param array $replace
     * @return void
     * @throws Exception
     */
    public static function processCHRespond(Customer $customer, array $return, array $replace)
    {
        try {
            switch ($return['status']) {
                case 'ACCEPT':

                    $annualReturnsEmailer = Registry::$emailerFactory->get(EmailerFactory::ANNUAL_RETURN);
                    $annualReturnsEmailer->sendManualServiceAccepted($customer, $replace);

                    // remove annual product
                    Company::removeAnnualReturn($return['companyId']);
                    break;

                case 'REJECT':
                    $annualReturnsEmailer = Registry::$emailerFactory->get(EmailerFactory::ANNUAL_RETURN);
                    $annualReturnsEmailer->sendManualServiceRejected($customer, $replace);
                    break;
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public static function processSendApplication(Company $company, CHAnnualReturn $chAnnualReturn, Customer $customer)
    {
        try {
            $response = $chAnnualReturn->send($company);
            // send email to staff + exception
            if ($response === 'ERROR') {
                // change status
                $annualReturnsEmailer = Registry::$emailerFactory->get(EmailerFactory::ANNUAL_RETURN);
                $annualReturnsEmailer->sendManualServiceError($customer, $company);
                throw new Exception('There has been an error with your submission to Companies House. Our staff have been alerted to the problem and are actively investigating. We expect to have the issue resolved shortly and will email you as soon as it is done.');
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     *
     * @param type Shareholding  $shareholders
     */
    public static function removeAllShareholderAddresses($shareholdings)
    {
        foreach ($shareholdings as $shareholding) {
            $data = array('isRemoved' => 1);
            $shareholders = $shareholding->getShareholders();
            foreach ($shareholders as $shareholder) {
                $shareholder->setFields($data);
                $shareholder->save();
            }
        }
    }

}
