<?php

class PaymentFormLogs extends Object
{

    /**
     * @var int
     */
    public $paymentFormId;

    /**
     * @var int
     */
    public $email;
    
    /**
     * @var int
     */
    public $customerId;

    /**
     * @var string
     */
    public $cardHolder;

    /**
     * @var string
     */
    public $cardNumber;

    /**
     * @var string
     */
    public $cardType;

    /**
     * @var string
     */
    public $expiryDate;

    /**
     * @var string
     */
    public $securityCode;

    /**
     * @var string
     */
    public $address1;

    /**
     * @var string
     */
    public $town;

    /**
     * @var string
     */
    public $postCode;

    /**
     * @var string
     */
    public $country;

    /**
     * @var type 
     */
    public $termscond;

    /**
     * @var string
     */
    public $dtc;

    /**
     * @var string
     */
    public $dtm;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->paymentFormId;
    }

    /**
     * @return bool
     */
    public function isNew()
    {
        return ($this->paymentFormId == 0);
    }

    /**
     * @param int $paymentFormId
     * @return void
     * @throws Exception
     */
    public function __construct($paymentFormId = 0)
    {
        $this->paymentFormId = (int) $paymentFormId;
        if ($this->paymentFormId) {
            $w = dibi::select('*')->from(TBL_PAYMENTS_FORM_LOGS)->where('paymentFormId=%i', $paymentFormId)->execute()->fetch();
            if ($w) {
                $this->customerId = $w['customerId'];
                $this->email = $w['email'];
                $this->cardHolder = $w['cardHolder'];
                $this->cardNumber = $w['cardNumber'];
                $this->cardType = $w['cardType'];
                $this->expiryDate = $w['expiryDate'];
                $this->securityCode = $w['securityCode'];
                $this->address1 = $w['address1'];
                $this->town = $w['town'];
                $this->postCode = $w['postCode'];
                $this->country = $w['country'];
                $this->termscond = $w['termscond'];
                $this->dtc = $w['dtc'];
                $this->dtm = $w['dtm'];
            } else {
                throw new Exception("Payment Form Error with id `$paymentFormId` doesn't exist!");
            }
        }
    }

    /**
     * @return int
     */
    public function save()
    {
        $data = array();
        $data['customerId'] = $this->customerId;
        $data['email'] = $this->email;
        $data['cardHolder'] = $this->cardHolder;
        $data['cardNumber'] = $this->cardNumber;
        $data['cardType'] = $this->cardType;
        $data['expiryDate'] = $this->expiryDate;
        $data['securityCode'] = $this->securityCode;
        $data['address1'] = $this->address1;
        $data['town'] = $this->town;
        $data['postCode'] = $this->postCode;
        $data['country'] = $this->country;
        $data['termscond'] = $this->termscond;
        $data['dtm'] = new DibiDateTime();

        if ($this->isNew()) {
            $data['dtc'] = new DibiDateTime();
            $this->paymentFormId = dibi::insert(TBL_PAYMENTS_FORM_LOGS, $data)->execute(dibi::IDENTIFIER);
        } else {
            dibi::update(TBL_PAYMENTS_FORM_LOGS, $data)->where('paymentFormId=%i', $this->paymentFormId)->execute();
        }
        return $this->paymentFormId;
    }

    /**
     * @return void
     */
    public function delete()
    {
        dibi::delete(TBL_PAYMENTS_FORM_LOGS)->where('paymentFormId=%i', $this->paymentFormId)->execute();
    }

    /**
     * Returns count of errors
     *
     * @param array $where
     * @return int
     */
    static public function getCount(array $where = array())
    {
        $result = dibi::select('count(*)')->from(TBL_PAYMENTS_FORM_LOGS);
        foreach ($where as $key => $val)
            $result->where('%n', $key, ' = %s', $val);
        $count = $result->execute()->fetchSingle();
        return $count;
    }

    /**
     * Returns all
     *
     * @param int $limit
     * @param int $offset
     * @param array $where
     * @return int
     */
    static public function getAll($limit = NULL, $offset = NULL, array $where = array())
    {
        $result = dibi::select('*')->from(TBL_PAYMENTS_FORM_LOGS)->orderBy('dtc', dibi::DESC);
        if ($limit !== NULL)
            $result->limit($limit);
        if ($offset !== NULL)
            $result->offset($offset);
        foreach ($where as $key => $val)
            $result->where('%n', $key, ' = %s', $val);
        return $result->execute()->fetchAssoc('paymentFormId');
    }

    /**
     * Returns result as objects
     *
     * @param int $limit
     * @param int $offset
     * @param array $where
     * @return array
     */
    static public function getAllObjects($limit = NULL, $offset = NULL, array $where = array())
    {
        $objects = array();
        $all = self::getAll($limit, $offset, $where);
        foreach ($all as $key => $val) {
            $objects[$key] = new self($key);
        }
        return $objects;
    }

}