<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS 
 * @author 		Stan (diviak@gmail.com)
 * @internal 	project/models/RemindersModel.php
 * @created 	14/05/2010
 */

class RemindersModel extends FNode
{

    const RETURNS_EMAIL = 719;
    const ACCOUNTS_EMAIL = 720;

	/**
	 * @var Company
	 */
	private $company;
	
	/**
	 * @var Customer
	 */
	private $customer;
	
	/**
	 * @var Reminder
	 */
	private $reminder;

    public static function sendReminders() {
        Reminder::sendReminders(new FEmail(self::RETURNS_EMAIL), new FEmail(self::ACCOUNTS_EMAIL));
    }
	
	/**
	 * Provides init settings
	 *
	 * @param Company $company
	 * @return void
	 */
	public function startup(Company $company, Customer $customer)
	{
        //self::sendReminders();
		$this->company = $company;
		$this->customer = $customer;
		$this->reminder = new Reminder($this->company->getCompanyNumber());

      	// in case if don't have reminder yet
		$email = $this->reminder->getEmailAddress();
		if (empty($email)) $this->reminder->setEmailAddress($this->customer->email);
	}

	/**
	 *
	 * @return Reminder
	 */
    public function getReminder() {
        return $this->reminder;
    }
	
	/**
	 * Returns reminders form
	 *
	 * @param array $callback
	 * @return unknown
	 */
	public function getComponentRemindersForm(array $callback)
	{
		$form = new FForm('reminders');
		
		$form->addFieldset('Data');
		$form->addCheckbox('isActive', 'Active', 1)
			->setValue($this->reminder->getActive());
		$form->addText('email', 'Email: ')
			->addRule(FForm::Required, 'Please provide email')
			->addRule(FForm::Email, 'Email is not valid')
			->setValue($this->reminder->getEmailAddress());
			
		$form->addFieldset('Action');
		$form->addSubmit('save', 'Submit')->style('width: 200px; height: 30px;');
		
		$form->onValid = $callback;
		$form->start();
		return $form;	
	}
	
	/**
	 * Provides saving reminders data
	 *
	 * @param FForm $form
	 * @return void
	 * @throws Exception
	 */
	public function saveReminderData(FForm $form, Company $company)
	{
		try {
			$data = $form->getValues();
			$this->reminder->setActive($data['isActive']);
			$this->reminder->setEmailAddress($data['email']);
			$this->reminder->save();
			$form->clean();
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
}
