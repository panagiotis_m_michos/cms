<?php
use Utils\Date;

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    ReportsAdminModel.php 2010-01-27 stan
 */
class ReportsAdminModel extends FNode
{

    /**
     * @return FForm
     */
    public function getComponentBarclaysForm()
    {
        $form = new FForm('barclays_stats', 'get');
        $form->add('DatePicker', 'dateFrom', 'Date:')
            ->setValue(self::getYesterdayDate());
        $form->add('DatePicker', 'dateTo', '-&nbsp;&nbsp;')
            ->setValue(self::getYesterdayDate());
        $form->addSubmit('submit', 'Go')
            ->class('btn');
        $form->start();
        $form->clean();

        return $form;
    }


    /**
     * @param FForm $filter
     * @return array
     */
    static public function getBarclaysData(FForm $filter)
    {
        $data = $filter->getValues();
        $dateFrom = $data['dateFrom'];
        $dateTill = $data['dateTo'];

        $incorporated = dibi::select('DATE_FORMAT(incorporation_date,"%Y-%m-%d") as date')
            ->select('COUNT(*) as incorporationsCount')
            ->from(TBL_COMPANY)
            ->where('(DATE_FORMAT(`incorporation_date`,"%Y-%m-%d") >= %s', $dateFrom)
            ->and('DATE_FORMAT(`incorporation_date`,"%Y-%m-%d") <= %s)', $dateTill)
            ->groupBy('date')
            ->execute()
            ->fetchAssoc('date');

        $sentTsb = dibi::select('DATE_FORMAT(dtc,"%Y-%m-%d") as date')
            ->select('COUNT(*) as sent')
            ->from(TBL_TSB_LEADS)
            ->where('(DATE_FORMAT(`dtc`,"%Y-%m-%d") >= %s', $dateFrom)
            ->and('DATE_FORMAT(`dtc`,"%Y-%m-%d") <= %s)', $dateTill)
            ->groupBy('date')
            ->execute()
            ->fetchAssoc('date');

        $sentBarclays = dibi::select('DATE_FORMAT(date_sent,"%Y-%m-%d") as date')
            ->select('COUNT(DISTINCT barclays_id) as sent')
            ->from(TBL_BARCLAYS)
            ->where('(DATE_FORMAT(`date_sent`,"%Y-%m-%d") >= %s', $dateFrom)
            ->and('DATE_FORMAT(`date_sent`,"%Y-%m-%d") <= %s)', $dateTill)
            ->and('`success`=1')
            ->groupBy('date')
            ->execute()
            ->fetchAssoc('date');

        $errorsBarclays = dibi::select('DATE_FORMAT(date_sent,"%Y-%m-%d") as date')
            ->select('COUNT(DISTINCT barclays_id) as sent')
            ->from(TBL_BARCLAYS)
            ->where('(DATE_FORMAT(`date_sent`,"%Y-%m-%d") >= %s', $dateFrom)
            ->and('DATE_FORMAT(`date_sent`,"%Y-%m-%d") <= %s)', $dateTill)
            ->and('success=0')
            ->groupBy('date')
            ->execute()
            ->fetchAssoc('date');

        $return = [];

        $dtFrom = new Date($dateFrom);
        $dtTo = new Date($dateTill);
        while ($dtFrom <= $dtTo) {
            $date = $dtFrom->format('Y-m-d');
            $tsbSentCount = isset($sentTsb[$date]) ? $sentTsb[$date]->sent : 0;
            $barclaysSentCount = isset($sentBarclays[$date]) ? $sentBarclays[$date]->sent : 0;
            $barclaysErrorsCount = isset($errorsBarclays[$date]) ? $errorsBarclays[$date]->sent : 0;
            $incorporationsCount = isset($incorporated[$date]) ? $incorporated[$date]->incorporationsCount : 0;

            $return[] = [
                'date' => $dtFrom->format('D, d/m/Y'),
                'incorporationsCount' => $incorporationsCount,
                'sentTsbCount' => $tsbSentCount,
                'sentBarclaysCount' => $barclaysSentCount,
                'barclaysErrorCount' => $barclaysErrorsCount,
                'sentTotal' => $barclaysSentCount + $tsbSentCount,
            ];

            $dtFrom->modify('+1 day');
        }

        $total = [
            'incorporationsCount' => 0,
            'sentTsbCount' => 0,
            'sentBarclaysCount' => 0,
            'barclaysErrorCount' => 0,
            'sentTotal' => 0,
        ];
        foreach ($return as $row) {
            $total['incorporationsCount'] += $row['incorporationsCount'];
            $total['sentTsbCount'] += $row['sentTsbCount'];
            $total['sentBarclaysCount'] += $row['sentBarclaysCount'];
            $total['barclaysErrorCount'] += $row['barclaysErrorCount'];
            $total['sentTotal'] += $row['sentTotal'];
        }
        $return['total'] = $total;

        return $return;
    }


    /**
     * @return FForm
     */
    public function getComponentOrdersForm()
    {
        $form = new FForm('form_name', 'get');

        $form->addFieldset('Filter');
        $form->add('DatePicker', 'dtFrom', 'From:')
            ->setValue(self::getYesterdayDate());
        $form->add('DatePicker', 'dtTo', 'To:')
            ->setValue(self::getYesterdayDate());
        $form->add('AsmSelect', 'typeId', 'Type:')
            ->multiple()
            ->title('--- Select --')
            ->setOptions(Transaction::$types);

        $form->addFieldset('Submit');
        $form->addSubmit('submit', 'Submit')
            ->class('btn')
            ->style('width: 200px; height: 30px;');

        $form->start();
        $form->clean();

        return $form;
    }


    /**
     * @param FForm $filter
     * @return FPaginator2
     */
    public function getComponentOrdersPaginator(FForm $filter)
    {
        $data = $filter->getValues();

        $sql = dibi::select('COUNT(*)')
            ->from(TBL_ORDERS . ' o')
            ->join(TBL_TRANSACTIONS, 't')
            ->on('o.orderId=t.orderId')
            ->where('t.error IS NULL')
            ->and('(DATE_FORMAT(o.`dtc`,"%Y-%m-%d") >= %s', $data['dtFrom'])
            ->and('DATE_FORMAT(o.`dtc`,"%Y-%m-%d") <= %s)', $data['dtTo']);

        if (!empty($data['typeId'])) {
            $sql->and('t.typeId')->in('%l', $data['typeId']);
        }

        $count = $sql->execute()->fetchSingle();
        $paginator = new FPaginator2($count);
        $paginator->itemsPerPage = 50;
        $paginator->htmlDisplayEnabled = FALSE;

        return $paginator;
    }


    /**
     * @param FForm $filter
     * @param FPaginator2 $paginator
     * @return array<DibiRow>
     */
    public function getOrdersData(FForm $filter, FPaginator2 $paginator = NULL)
    {
        $data = $filter->getValues();
        $sql = dibi::select('o.orderId, o.customerName, o.customerEmail, o.total, o.dtc, o.refundValue, t.typeId')
            ->from(TBL_ORDERS . ' o')
            ->join(TBL_TRANSACTIONS, 't')
            ->on('o.orderId=t.orderId')
            ->where('t.error IS NULL')
            ->and('(DATE_FORMAT(o.`dtc`,"%Y-%m-%d") >= %s', $data['dtFrom'])
            ->and('DATE_FORMAT(o.`dtc`,"%Y-%m-%d") <= %s)', $data['dtTo']);

        // --- type ---
        if (!empty($data['typeId'])) {
            $sql->and('t.typeId')->in('%l', $data['typeId']);
        }

        // --- paginator ---
        if ($paginator !== NULL) {
            $sql->limit($paginator->getLimit());
            $sql->offset($paginator->getOffset());
        }

        // --- order ---
        $sql->orderBy('dtc')->desc();

        $all = $sql->execute()->fetchAll();

        // --- change transaction type ---
        $all = self::changeType($all);

        return $all;
    }


    /**
     * @param FForm $filter
     * @return void
     */
    public function outputOrdersCSV(FForm $filter)
    {
        $orders = $this->getOrdersData($filter);

        // --- change transaction type ---
        $orders = self::changeType($orders);

        $header = array_keys((array) reset($orders));
        array_unshift($orders, $header);
        $filename = sprintf("orders_report_%s.csv", date('Y_m_d'));
        Array2Csv::output($orders, $filename);
    }


    /**
     * Rerurns default date - yesterday
     *
     * @return string
     */
    static public function getYesterdayDate($format = NULL)
    {
        static $return;
        if ($return === NULL || $format !== NULL) {
            $return = mktime(0, 0, 0, date("m"), date("d") - 1, date("Y"));
            $return = ($format === NULL) ? date('d-m-Y', $return) : date($format, $return);
        }

        return $return;
    }


    /**
     * Provides change transaction type
     *
     * @param array $data
     * @return array
     */
    static private function changeType(array $data)
    {
        foreach ($data as &$row) {
            $typeId = $row['typeId'];
            $row['type'] = isset(Transaction::$types[$typeId]) ? Transaction::$types[$typeId] : NULL;
        }
        unset($row);

        return $data;
    }

    public function getIncorporationData($filter)
    {
        $data = $filter->getValues();
        $dateFrom = $data['dateFrom'];
        $dateTill = $data['dateTo'];

        $datasource = dibi::select('COUNT(DISTINCT company_id) as incorporated')
            ->from('ch_company')
            ->select('DATE_FORMAT(incorporation_date, "%Y-%m-%d / %W") as dti');
        if (isset($dateFrom) && isset($dateTill)) {
            $datasource->where(
                '(DATE_FORMAT(incorporation_date, "%Y-%m-%d") >=%s AND DATE_FORMAT(incorporation_date, "%Y-%m-%d") <=%s)',
                $dateFrom,
                $dateTill
            );
        } elseif (isset($dateFrom)) {
            $datasource->where('DATE_FORMAT(incorporation_date, "%Y-%m-%d") >=%s', $dateFrom);
        } elseif (isset($dateTill)) {
            $datasource->where('DATE_FORMAT(incorporation_date, "%Y-%m-%d") <=%s', $dateTill);
        };
        $datasource->and('order_id IS NOT NULL');
        $datasource->orderBy('dti')->desc()->groupBy('dti');

        return $datasource;
    }

    public function getIncorporationTotal($filter)
    {
        $data = $filter->getValues();
        $dateFrom = $data['dateFrom'];
        $dateTill = $data['dateTo'];

        $datasource = dibi::select('COUNT(DISTINCT company_id) as incorporated')
            ->from('ch_company')
            ->select('DATE_FORMAT(incorporation_date, "%Y-%m-%d") as dti');
        if (isset($dateFrom) && isset($dateTill)) {
            $datasource->where(
                '(DATE_FORMAT(incorporation_date, "%Y-%m-%d") >=%s AND DATE_FORMAT(incorporation_date, "%Y-%m-%d") <=%s)',
                $dateFrom,
                $dateTill
            );
        } elseif (isset($dateFrom)) {
            $datasource->where('DATE_FORMAT(incorporation_date, "%Y-%m-%d") >=%s', $dateFrom);
        } elseif (isset($dateTill)) {
            $datasource->where('DATE_FORMAT(incorporation_date, "%Y-%m-%d") <=%s', $dateTill);
        };
        $datasource->and('order_id IS NOT NULL');

        return $datasource->execute()->fetchSingle('incorporated');
    }
}
