<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    WidgetSearchModel.php 2010-11-17 divak@gmail.com
 */

class WidgetSearchModel extends FNode
{
	/**
	 * @var Affiliate
	 */
	private $affiliate;
	
	/**
	 * @param string $hash
	 * @throws Exception
	 */
	public function startup($hash)
	{
		try {
			$affiliateId = Affiliate::getAffiliateByHash($hash);
			if ($affiliateId === FALSE) throw new Exception("Affiliate doesn't exist");
			
			$this->affiliate = new Affiliate($affiliateId);
			if (!$this->affiliate->hasService(Affiliate::SERVICE_WIDGET_SEARCH)) throw new Exception("Service doesn't exist for affiliate");
		} catch (Exception $e) {
			throw $e;
		}
	}
	
	/**
	 * @return Affiliate
	 */
	public function getAffiliate()
	{
		return $this->affiliate;
	}
	
	/**
	 * @param array $callback
	 * @return FForm
	 */
	public function getComponentSearchForm(array $callback)
	{
		$form = new FForm('widgetSearch', 'get');
		$form->addText('companyName', 'Company Name:');
		$form->addSubmit('submit', 'Search');
		$form->onValid = $callback;
		$form->start();
		return $form;
	}
	
}