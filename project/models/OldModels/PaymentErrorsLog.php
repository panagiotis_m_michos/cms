<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    PaymentErrorsLog.php 2012-07-19 razvanp@madesimplegroup.com
 */

class PaymentErrorsLog extends Object
{
    
    //table
    const TABLE_PAYMENT_ERROR_LOGS = 'cms2_payment_error_logs';
    
    // status
	const SAGE = 'SAGE';
	const PAYPAL = 'PAYPAL';
    
	const STATUS_RESUBMIT = 'RESUBMIT';
	const STATUS_EMPTY_RESPONSE = 'EMPTY_RESPONSE';
	const STATUS_FAILED = 'FAILED';
	const STATUS_CURL_ERROR = 'CURL_ERROR';
	
	/** @var array */
	static public $paymentStatusTypeId = Array(
		self::SAGE => 'Sage',
		self::PAYPAL => 'Paypal',
	);
    
    /** @var array */
	static public $errorStatusTypeId = Array(
		self::STATUS_RESUBMIT => 'Resubmit',
		self::STATUS_EMPTY_RESPONSE => 'Empty Response',
		self::STATUS_FAILED => 'Failed',
		self::STATUS_CURL_ERROR => 'Curl Error (Timeout or Empty Response)',
	);


    /**
	 * primary key
	 *
	 * @var int
	 */
	public $errorId;

	/**
	 * @var enum (SAGE,PAYPAL)
	 */
	public $paymentTypeId;

	/**
	 * @var enum (RESUBMIT,EMPTY_RESPONSE,FAILED,CURL_ERROR)
	 */
	public $errorTypeId;

    /**
	 * @var string
	 */
	public $vendorTXCode;
    /**
	 * @var string
	 */
	public $email;
    /**
	 * @var string
	 */
	public $name;
    
    /**
	 * @var string
	 */
	public $address1;
    
    /**
	 * @var string
	 */
	public $address2;
    
    /**
	 * @var string
	 */
	public $postcode;
    
    /**
	 * @var string
	 */
	public $country;
    
    /**
	 * @var string
	 */
	public $basketPrice;
    
    /**
	 * @var text
	 */
	public $basketItems;
    
    /**
	 * @var text
	 */
	public $paymentSystem;
    
    /**
	 * @var text
	 */
    public $errorMessage;
    
    /**
    * @var text
    */
    public $auth3D = 0;
    
    /**
	 * @var string
	 */
	public $dtc;

	/**
	 * @var string
	 */
	public $dtm;

    /**
	 * @return bool
	 */
	public function isNew()
	{
		return ($this->errorId == 0);
	}
    
    /**
	 * Provides get customer from database
	 * @param int $customerId
	 * @return void
	 * @throws Exception
	 */
	public function __construct($errorId=0)
	{
		$this->errorId = (int) $errorId; 
		if ($this->errorId) {
			$w = FApplication::$db->select('*')
				->from(self::TABLE_PAYMENT_ERROR_LOGS)
				->where('errorId=%i', $this->getId())
				->execute()->fetch();
			
			if ($w) {
				$this->errorId = $w['errorId'];
				$this->paymentTypeId = $w['paymentTypeId'];
				$this->errorTypeId = $w['errorTypeId'];
				$this->vendorTXCode = $w['vendorTXCode'];
				$this->email = $w['email'];
				$this->name = $w['name'];
				$this->address1 = $w['address1'];
				$this->address2 = $w['address2'];
				$this->postcode = $w['postcode'];
				$this->country = $w['country'];
				$this->basketPrice = $w['basketPrice'];
				$this->basketItems = json_decode($w['basketItems']);
				$this->paymentSystem = $w['paymentSystem'];
				$this->errorMessage = $w['errorMessage'];
				$this->auth3D = $w['auth3D'];
				$this->dtc = $w['dtc'];
				$this->dtm = $w['dtm'];
				
			} else {
			    throw new Exception("Error with id `$errorId` doesn't exist!");
			}
		}
	}
    
    /**
	 * Returns customerId
	 * @return boolean
	 */
	public function getId() { return( $this->errorId ); }
    

    /**
	 * @return int
	 */
	public function save()
	{
		$action = $this->getId() ? 'update' : 'insert';
        
        $data = array();

        $data['paymentTypeId'] = $this->paymentTypeId;
        $data['errorTypeId'] = $this->errorTypeId;
        $data['vendorTXCode'] = $this->vendorTXCode;
        $data['email'] = $this->email;
        $data['name'] = $this->name;
        $data['address1'] = $this->address1;
        $data['address2'] = $this->address2;
        $data['postcode'] = $this->postcode;
        $data['country'] = $this->country;
        $data['basketPrice'] = $this->basketPrice;
        $data['basketItems'] = json_encode($this->basketItems);
        $data['paymentSystem'] = $this->paymentSystem;
        $data['errorMessage'] = $this->errorMessage;
        $data['auth3D'] = $this->auth3D;
        $data['dtc'] = $this->dtc;
		
        if ($action == 'insert') {
            $data['dtm'] = new DibiDateTime();
			$data['dtc'] = new DibiDateTime();
			$this->errorId = dibi::insert(self::TABLE_PAYMENT_ERROR_LOGS, $data)->execute(dibi::IDENTIFIER);
		// update
		} else {
			$data['dtm'] = new DibiDateTime();
			FApplication::$db->update(self::TABLE_PAYMENT_ERROR_LOGS, $data)->where('errorId=%i', $this->getId())->execute();
		}
        
		return $this->errorId;
	}

    /**
	 * Returns count of all records from feedback
	 *
	 * @param array $where
	 * @return int
	 */
	static public function getCount(array $where = array())
	{
        $result = FApplication::$db->select('COUNT(*)')->from(TBL_FEEDBACKS)->orderBy('dtc')->desc();
		foreach ($where as $key=>$val) {
            if($key == 'dtcFrom') {
                $result->where('dtc >=%s', $val);
                continue;
            }
            if($key == 'dtcTo') {
                $result->where('DATE_FORMAT(dtc,"%Y-%m-%d") <=%s', $val);
                continue;
            }
            $val = '%'.$val.'%';
			$result->where('%n', $key, ' LIKE %s', $val);
		}
		$count = $result->execute()->fetchSingle();
		return $count;
	}

    /**
	 * Returns feedbacks as array
	 *
	 * @param int $limit
	 * @param int $offset
	 * @param array $where
	 * @return array<DibiRow>
	 */
	static public function getAll(array $where = array(), $limit = NULL, $offset = NULL)
	{
        $result = FApplication::$db->select('*')->from(TBL_FEEDBACKS)->orderBy('dtc')->desc();
        foreach ($where as $key=>$val) {
			if($key == 'dtcFrom') {
                $result->where('dtc >=%s', $val);
                continue;
            }
            if($key == 'dtcTo') {
                $result->where('DATE_FORMAT(dtc,"%Y-%m-%d") <=%s', $val);
                continue;
            }
            $val = '%'.$val.'%';
			$result->where('%n', $key, ' LIKE %s', $val);
		}
		if ($limit !== NULL) $result->limit($limit);
		if ($offset !== NULL) $result->offset($offset);

		$feedbacks = $result->execute()->fetchAssoc('feedbackId');
        return $feedbacks;
	}

}

