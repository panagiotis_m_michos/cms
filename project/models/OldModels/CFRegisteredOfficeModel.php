<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    CFRegisteredOfficeModel.php 2011-11-18 razvanp@madesimplegroup.com
 */

class CFRegisteredOfficeModel extends FNode{
    
    
    /**
     * @var bool
     */
    private $hasVisibleHtmlBox;
    
    /**
     * @var string
     */
    private $htmlBoxTitle;
    
    /**
     * @var string
     */
    private $underGuideHtmlBox;
    
    
    /**
     *
     * @return string 
     */
    public function getHasVisibleHtmlBox(){
        return $this->hasVisibleHtmlBox;
    }
    
   
    /**
     *
     * @return string 
     */
    public function getUnderGuideHtmlBox(){
        return $this->underGuideHtmlBox;
    }
    
    /**
     *
     * @return string 
     */
    public function setHasVisibleHtmlBox($hasVisibleHtmlBox){
        $this->hasVisibleHtmlBox = $hasVisibleHtmlBox;
    }
    
        
    /**
     *
     * @return string 
     */
    public function setUnderGuideHtmlBox($underGuideHtmlBox){
        $this->underGuideHtmlBox = $underGuideHtmlBox;
    }
    
    
    
    /*     * ********************************************* ADMIN ********************************************** */

    /**
     * Provides adding custom data
     * 
     * @return void 
     */
    public function addCustomData()
    {
        $this->hasVisibleHtmlBox =        $this->getProperty('hasVisibleHtmlBox');
        $this->underGuideHtmlBox = $this->getProperty('underGuideHtmlBox');
        parent::addCustomData();
    }

    /**
     * Provides saving custom data
     *
     * @param string $action
     * @return void
     */
    public function saveCustomData($action)
    {
        $this->saveProperty('hasVisibleHtmlBox', $this->hasVisibleHtmlBox);
        $this->saveProperty('underGuideHtmlBox', $this->underGuideHtmlBox);
        parent::saveCustomData($action);
    }
    
}
