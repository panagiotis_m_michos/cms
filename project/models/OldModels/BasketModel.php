<?php

use Dispatcher\Events\Basket\ItemRemovedEvent;
use Symfony\Component\EventDispatcher\EventDispatcher;

class BasketModel extends FNode
{
    /**
     * @var Basket
     */
    private $basket;

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var EventDispatcher
     */
    private $eventDispatcher;

    /** @var array */
    public $associatedProductIds = array();

    /**
     * @param Basket $basket
     * @param Customer $customer
     * @param EventDispatcher $eventDispatcher
     */
    public function startup(Basket $basket, Customer $customer, EventDispatcher $eventDispatcher)
    {
        $this->basket = $basket;
        $this->customer = $customer;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Provides getting extra data to FNode
     */
    protected function addCustomData()
    {
        parent::addCustomData();
        $associatedProducts = $this->getProperty('associatedProductIds');
        if ($associatedProducts != NULL) {
            $associatedProducts = explode('|', $this->getProperty('associatedProductIds'));
            $this->associatedProductIds = $associatedProducts;
        }
    }

    /**
     * Provides saving extra data to FNode
     *
     * @param string $action
     */
    protected function saveCustomData($action)
    {
        parent::saveCustomData($action);
        $associatedProdudts = implode('|', $this->associatedProductIds);
        $this->saveProperty('associatedProductIds', $associatedProdudts);
    }

    /**
     * @param array $callback
     * @return FForm
     */
    public function getComponentAdminPropertiesForm(array $callback)
    {
        $form = new FForm('properties_' . $this->nodeId);

        $form->addFieldset('Data');
        $form->add('AsmSelect', 'associatedProductIds', 'Associated Products:')
            ->title('--- Select --')
            ->setValue($this->associatedProductIds)
            ->setOptions(BasketProduct::getAllProducts());

        $form->addFieldset('Action');
        $form->addSubmit('save', 'Submit')->class('btn')->style('width: 200px; height: 30px;');

        $form->onValid = $callback;
        $form->start();
        return $form;
    }

    /**
     * Provides saving properties tab data
     *
     * @param FForm $form
     * @throws Exception
     */
    public function saveAdminPropertiesTabData(FForm $form)
    {
        $data = $form->getValues();
        $this->associatedProductIds = $data['associatedProductIds'];
        $this->save();
        $form->clean();
    }

    /**
     * Returns basket form
     *
     * @param array $callback
     * @return FForm
     */
    public function getComponentBasketForm(array $callback)
    {
        $form = new FForm('basket');
        $form->clean();

        if ($this->basket->isEmpty() == FALSE) {
            foreach ($this->basket->items as $key => $val) {
                $form->addText($key, NULL)
                    ->size(1)
                    ->setGroup('items')
                    ->setValue($val->qty);
            }
            $form->addSubmit('removeVoucher', 'Remove')
                ->class('linkButton');
            $form->addSubmit('update', 'Update Basket')
                ->class('linkButton');
            $form->addSubmit('checkout', 'Checkout')
                ->class('btn_submit mtop fright');
        }
        $form->addSubmit('continue', '< Continue shopping');
        $form->onValid = $callback;
        $form->start();
        return $form;
    }

    /**
     * Provides saving basket form data
     *
     * @param FForm $form
     * @throws Exception
     */
    public function saveBasketFormData(FForm $form)
    {
        $data = $form->getValues();
        if ($form->isSubmitedBy('removeVoucher')) {
            $this->basket->removeVoucher();
        } elseif ($form->isSubmitedBy('update') && isset($data['items'])) {
            foreach ($data['items'] as $key => $val) {
                $this->basket->update($key, $val);
            }
        }
    }

    /**
     * Returns associated products
     *
     * @return array<FNode>
     */
    public function getAssociatedProducts()
    {
        $products = array();
        foreach ($this->associatedProductIds as $productId) {
            $products[$productId] = FNode::getProductById($productId);
        }
        return $products;
    }


    /**
     * Returns customer companies dropdown (Company Id => Company Name)
     *
     * @return array
     */
    public function getCustomerCompaniesDropdown()
    {
        /** @var SessionNamespace orderSummarySession */
        $orderSummarySession = Registry::$container->get(DiLocator::SESSION)->getNamespace('orderSummary');
        $companies = array();
        $customerId = $this->customer->getId();
        $chFiling = new CHFiling();

        $temp['inc'] = $chFiling->getCustomerIncorporatedCompanies($customerId);
        $temp['notInc'] = $chFiling->getCustomerNotIncorporatedCompanies($customerId);

        // incorporated
        if (!empty($temp['inc'])) {
            foreach ($temp['inc'] as $companyId => $company) {
                $companies['Incorporated'][$companyId] = $company['company_name'];
            }
        }

        // current imported copmanies (stored in session)
        foreach ($orderSummarySession->importedCompanies as $importedCompany) {
            $companies['Incorporated']['imported_' . $importedCompany['companyNumber']] = $importedCompany['companyName'];
        }

        if (!empty($companies['Incorporated'])) {
            asort($companies['Incorporated']);
        }

        // not incorporated
        if (!empty($temp['notInc'])) {
            foreach ($temp['notInc'] as $companyId => $company) {
                $companies['Not Incorporated'][$companyId] = $company['company_name'];
            }
        }

        return $companies;
    }

    /**
     * Returns customer companies dropdown (Company Id => Company Name)
     *
     * @return array
     */
    public function getIncCustomerCompaniesDropdown()
    {
        $companies = array();
        $customerId = $this->customer->getId();
        $chFiling = new CHFiling();

        $temp['inc'] = $chFiling->getCustomerIncorporatedCompanies($customerId);

        // incorporated
        if (!empty($temp['inc'])) {
            foreach ($temp['inc'] as $companyId => $company) {
                $companies['Incorporated'][$companyId] = $company['company_name'];
            }
        }

        return $companies;
    }

    /**
     * Returns associated products for summary page
     * If there is no package will return default products for this page
     *
     * @return array<FNode>
     */
    public function getSummaryAssociatedProducts()
    {
        $associatedProducts = array();
        if ($this->basket->hasPackageIncluded()) {
            $associatedProducts = $this->basket->getPackage()->getPackageProducts('associatedProducts2');
        } else {
            $associatedProducts = $this->getAssociatedProducts();
        }

        // exclude that ones which are in basket
        foreach ($associatedProducts as $key => &$product) {
            if ($product->inBasket($this->basket) !== FALSE) {
                unset($associatedProducts[$key]);
                continue;
            }
            $product->isAssociated = TRUE;
        }

        return $associatedProducts;
    }

    /**
     * Provides add item to basket
     *
     * @param int $id
     * @throws Exception
     */
    public function addBasketItem($id)
    {
        $item = FNode::getProductById($id);
        $associated = $this->getSummaryAssociatedProducts();
        if (isset($associated[$item->getId()])) {
            $item->isAssociated = TRUE;
        }
        $this->basket->add($item);
    }

    /**
     * @param BasketProduct $product
     * @throws Exception
     */
    public function addProduct(BasketProduct $product)
    {
        $associated = $this->getSummaryAssociatedProducts();
        if (isset($associated[$product->getId()])) {
            $product->isAssociated = TRUE;
        }
        $this->basket->add($product);

        // TODO: S644 - Added until buying process is refactored to add all Associated products
        if (in_array(
            $product->getId(),
            [
                Package::PACKAGE_LIMITED_BY_GUARANTEE,
                Package::PACKAGE_LIMITED_BY_GUARANTEE_WITHOUT_REGISTERED_OFFICE,
                Package::PACKAGE_LIMITED_LIABILITY_PARTNERSHIP,
            ]
        )) {
            $product->addAssociatedProducts();
        }
    }

    /**
     * Provides remove item from basket
     *
     * @param int $itemId
     * @throws Exception
     */
    public function removeBasketItem($itemId)
    {
        $item = $this->basket->getItem($itemId);
        if ($item) {
            $this->eventDispatcher->dispatch(EventLocator::BASKET_ITEM_REMOVED, new ItemRemovedEvent($itemId, $item));
        }
        $this->basket->remove($itemId);
    }

    /**
     * Provides clean basket - remove all items
     *
     * @throws Exception
     */
    public function removeAllBasketItems()
    {
        foreach ($this->basket->getItems() as $itemId => $item) {
            $this->eventDispatcher->dispatch(EventLocator::BASKET_ITEM_REMOVED, new ItemRemovedEvent($itemId, $item));
        }
        $this->basket->clear();
    }

    /**
     * Returns if barclays is included in basket
     *
     * @return boolean
     */
    public function hasBarclaysBankingInBasket()
    {
        $items = $this->basket->getItems();
        foreach ($items as $item) {
            if ($item->getId() == BarclaysBanking::BARCLAYS_BANKING_PRODUCT) {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * Returns if credit Products is included in basket
     * @param Basket $basket
     * @return boolean
     */
    public function hasCreditProduct(Basket $basket)
    {
        foreach ($basket->items as $val) {
            if ($val instanceof CreditProduct) {
                return TRUE;
            }
        }
        return FALSE;
    }
}
