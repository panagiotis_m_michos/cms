<?php

use BankingModule\Entities\CompanyCustomer as CompanyCustomerEntity;

class CompanyCustomerModel extends Object
{

    /**
     * Returns company customer form for front end
     *
     * @param array $callback
     * @param Customer $customer
     * @return FForm
     */
    public static function getComponentFrontForm(array $callback, Customer $customer)
    {
        $form = new FForm('companyCustomer');

        // contact date
        //$form->addFieldset('When would you like Barclays to contact you about opening a new business bank account');
        //$form->addSelect('preferredContactDate', 'Please select *', Customer::$dates)
        //->setValue(0);
        //->addRule(FForm::Required, 'Please provide date!');

        // personal details
        $form->addFieldset('What are the best details for Barclays to use to contact you?');
        $form->addText('email', 'Email *')
            ->addRule(FForm::Required, 'Please provide e-mail address!')
            ->addRule(FForm::Email, 'Email is not valid!');

        if ($customer->isWholesale()) {
            $form['email']->setDescription(
                '(if you are the accountant - please ensure the details provided are that of the client wishing to be contacted by Barclays)'
            );
        }

        $form->addSelect('titleId', 'Title &nbsp;&nbsp;', array_combine(Customer::$titles, Customer::$titles))
            ->setFirstOption('--- Select ---');
        $form->addText('firstName', 'First Name *')
            ->addRule(FForm::Required, 'Please provide firstname!');
        $form->addText('lastName', 'Last Name  *')
            ->addRule(FForm::Required, 'Please provide lastname!');
        $form->addText('phone', 'Daytime Number *')
            ->setDescription('(The following formats are allowed: 01234567890, +441234567890, 00441234567890)')
            ->addRule(FForm::Required, 'Please provide phone!')
            ->addRule(['CompanyCustomerModel', 'Validator_phonenumber'], 'Phone is not valid.');
        $form->addText('additionalPhone', 'Alternative Number *')
            ->setDescription('(The following formats are allowed: 01234567890, +441234567890, 00441234567890)')
            ->addRule(FForm::Required, 'Please provide phone!')
            ->addRule(['CompanyCustomerModel', 'Validator_phonenumber'], 'Phone is not valid.');

        // address details
        $form->addFieldset('Where would you like to have your Barclays branch close to?');
        $form->addText('address1', 'Address 1  *')
            ->addRule(FForm::Required, 'Please provide address 1!');
        $form->addText('address2', 'Address 2 &nbsp;&nbsp;');
        $form->addText('address3', 'Address 3 &nbsp;&nbsp;');
        $form->addText('city', 'City  *')
            ->addRule(FForm::Required, 'Please provide city!');
        $form->addText('county', 'County &nbsp;&nbsp;');
        $form->addText('postcode', 'Postcode  *')
            ->addRule(FForm::Required, 'Please provide post code!')
            ->addRule(['CompanyCustomerModel', 'Validator_postcode'], 'Postcode is not valid')
            ->setDescription('Please add 1 space in the middle');
        $form->addSelect('countryId', 'Country  *', [223 => "United Kingdom"]);

        $form->addFieldset('Consent');
        $form->addRadio(
            'consent',
            'Consent',
            [1 => "I consent to my details being sent to Barclays", 2 => "I am sending my client's details to Barclays and they have given me consent"]
        )
            ->addRule(FForm::Required, 'Please provide consent!');

        // action
        $form->addFieldset('Action');
        $form->addSubmit('login', 'Save')
            ->setDescription('(Please double check all your details before saving them!)')
            ->style('width: 200px; height: 30px');


        // prefill in case of wholesale customer
        if ($customer->isWholesale() === FALSE) {
            $data = (array) $customer;
            $data['phone'] = NULL;
            $data['additionalPhone'] = NULL;
            $form->setInitValues($data);
        }
        $form->onValid = $callback;
        $form->start();

        return $form;
    }


    /**
     * Provides saving CompanyCustomer (front end)
     *
     * @param int $wholesalerId
     * @param int $companyId
     * @param array $data
     * @throws Exception
     */
    public static function saveFrontForm($wholesalerId, $companyId, array $data)
    {
        $companyCustomer = new CompanyCustomer();
        $companyCustomer->bankTypeId = CompanyCustomerEntity::BANK_TYPE_BARCLAYS;
        $companyCustomer->preferredContactDate = 0;//$data['preferredContactDate'];
        $companyCustomer->wholesalerId = $wholesalerId;
        $companyCustomer->companyId = $companyId;
        $companyCustomer->email = $data['email'];
        $companyCustomer->titleId = $data['titleId'];
        $companyCustomer->firstName = $data['firstName'];
        $companyCustomer->lastName = $data['lastName'];
        $companyCustomer->address1 = $data['address1'];
        $companyCustomer->address2 = $data['address2'];
        $companyCustomer->address3 = $data['address3'];
        $companyCustomer->city = $data['city'];
        $companyCustomer->county = $data['county'];
        $companyCustomer->postcode = $data['postcode'];
        $companyCustomer->countryId = $data['countryId'];
        $companyCustomer->phone = $data['phone'];
        $companyCustomer->additionalPhone = $data['additionalPhone'];
        $companyCustomer->consent = $data['consent'];
        $companyCustomer->save();
    }

    /**
     * @param array $callback
     * @param Customer $customer
     * @param Company $company
     * @param string $bank
     * @return FForm
     */
    public static function getComponentAdminForm(array $callback, Customer $customer, Company $company, $bank)
    {
        $form = new FForm('companyCustomer');

        // contact date
        $form->addFieldset('When would you like Barclays to contact you about opening a new business bank account');
        $form->addSelect('preferredContactDate', 'Please select *', Customer::$dates)
            ->setFirstOption('--- Select ---')
            ->addRule(FForm::Required, 'Please provide date!');

        // personal details
        $form->addFieldset('What are the best details for Barclays to use to contact you?');
        $form->addText('email', 'Email *')
            ->addRule(FForm::Required, 'Please provide e-mail address!')
            ->addRule(FForm::Email, 'Email is not valid!');

        if ($customer->isWholesale()) {
            $form['email']->setDescription(
                '(if you are the accountant - please ensure the details provided are that of the client wishing to be contacted by Barclays)'
            );
        }

        $form->addSelect('titleId', 'Title &nbsp;&nbsp;', array_combine(Customer::$titles, Customer::$titles))
            ->setFirstOption('--- Select ---');
        $form->addText('firstName', 'First Name *')
            ->addRule(FForm::Required, 'Please provide firstname!');
        $form->addText('lastName', 'Last Name  *')
            ->addRule(FForm::Required, 'Please provide lastname!');
        $form->addText('phone', 'Main Phone Number *')
            ->setDescription('(The following formats are allowed: 01234567890, +441234567890, 00441234567890)')
            ->addRule(FForm::Required, 'Please provide phone!')
            ->addRule(['CompanyCustomerModel', 'Validator_phonenumber'], 'Phone is not valid.');
        $form->addText('additionalPhone', 'Mobile/Other Number *')
            ->setDescription('(The following formats are allowed: 01234567890, +441234567890, 00441234567890)')
            ->addRule(['CompanyCustomerModel', 'Validator_phonenumber'], 'Phone is not valid.');

        // address details
        $form->addFieldset('Where would you like to have your Barclays branch close to?');
        $form->addText('address1', 'Address 1  *')
            ->addRule(FForm::Required, 'Please provide address 1!');
        $form->addText('address2', 'Address 2 &nbsp;&nbsp;');
        $form->addText('address3', 'Address 3 &nbsp;&nbsp;');
        $form->addText('city', 'City  *')
            ->addRule(FForm::Required, 'Please provide city!');
        $form->addText('county', 'County &nbsp;&nbsp;');
        $form->addText('postcode', 'Postcode  *')
            ->addRule(FForm::Required, 'Please provide post code!')
            ->addRule(['CompanyCustomerModel', 'Validator_postcode'], 'Postcode is not valid')
            ->setDescription('Please add 1 space in the middle');
        $form->addSelect('countryId', 'Country  *', [223 => "United Kingdom"]);

        // action
        $form->addFieldset('Action');
        $form->addSubmit('login', 'Save')
            ->setDescription('(Please double check all your details before saving them!)')
            ->style('width: 200px; height: 30px')
            ->class('btn');

        try {
            // prefill data in form
            $data = (array) CompanyCustomer::getCompanyCustomerByCompanyId($company->getCompanyId(), $bank);
            $data['preferredContactDate'] = (int) ceil((strtotime($data['preferredContactDate']) - strtotime($data['dtc'])) / 86400);
        } catch (Exception $e) {
            $data = (array) $customer;
        }

        $form->setInitValues($data);

        $form->onValid = $callback;
        $form->start();

        return $form;
    }

    /**
     * Provides saving admin form
     *
     * @param FForm $form
     * @param Customer $customer
     * @param Company $company
     * @param string $bank
     * @throws Exception
     */
    public static function saveAdminForm(FForm $form, Customer $customer, Company $company, $bank)
    {
        try {
            $companyCustomer = CompanyCustomer::getCompanyCustomerByCompanyId($company->getCompanyId(), $bank);
        } catch (Exception $e) {
            $companyCustomer = new CompanyCustomer();
        }

        $data = $form->getValues();
        $companyCustomer->bankTypeId = $bank;
        $companyCustomer->preferredContactDate = $data['preferredContactDate'];
        $companyCustomer->wholesalerId = $customer->getId();
        $companyCustomer->companyId = $company->getCompanyId();
        $companyCustomer->email = $data['email'];
        $companyCustomer->titleId = $data['titleId'];
        $companyCustomer->firstName = $data['firstName'];
        $companyCustomer->lastName = $data['lastName'];
        $companyCustomer->address1 = $data['address1'];
        $companyCustomer->address2 = $data['address2'];
        $companyCustomer->address3 = $data['address3'];
        $companyCustomer->city = $data['city'];
        $companyCustomer->county = $data['county'];
        $companyCustomer->postcode = $data['postcode'];
        $companyCustomer->countryId = $data['countryId'];
        $companyCustomer->phone = $data['phone'];
        $companyCustomer->additionalPhone = $data['additionalPhone'];
        $companyCustomer->save();

        $form->clean();
    }

    /**
     * @param Company $company
     * @return void
     * @throws Exception
     */
    public static function resendBarclaysData(Company $company)
    {
        Barclays::sendCompany($company->getCompanyId(), FALSE, TRUE, TRUE);
    }


    /**
     * Validate postcode for UK
     *
     * @param Text $control
     * @param string $error
     * @param array $params
     * @return boolean
     */
    public static function Validator_postcode(Text $control, $error, array $params)
    {
        $value = $control->getValue();
        $countryId = $control->owner['countryId']->getValue();
        if ($countryId == Customer::UK_CITIZEN
            && (!preg_match('#^(GIR 0AA|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]|[A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y]))|[0-9][A-HJKPS-UW]) [0-9][ABD-HJLNP-UW-Z]{2})$#i', $value) || !preg_match('#^([A-Z][\dA-Z]{1,3}[\s][\d][A-Z]{2,2})$#i', $value))
        ) {
            return $error;
        }

        return TRUE;
    }


    /**
     * Returns if company customer for barclays is read only - in case of successed request
     *
     * @param Company $company
     * @return boolean
     */
    public static function readonly(Company $company)
    {
        //allow for admin and staff
        $user = FUser::getSignedIn();
        if ($user->roleId == 1 || $user->roleId == 2) {
            return FALSE;
        }

        $requests = $company->getBarclaysRequestData();
        foreach ($requests as $key => $val) {
            if ($val['success']) {
                return TRUE;
            }
        }

        return FALSE;
    }

    /**
     * @param Text $control
     * @param type $error
     * @param array $params
     * @return boolean
     */
    public static function Validator_phonenumber(Text $control, $error, array $params)
    {
        $value = preg_replace('/\s+/', '', $control->getValue());
        if (!preg_match('#^\+([4]{2}[0-9]{10})$|^([0]{1}[0-9]{10})$|^([0]{2}[4]{2}[0-9]{10})$#', $value)) {
            return $error;
        }

        return TRUE;
    }

    /**
     * @param FControl $control
     * @param string $error
     * @param array $params
     * @return boolean
     */
    public static function Validator_country(FControl $control, $error, array $params)
    {
        return $control->getValue() == 'UK' ? TRUE : $error;
    }
}
