<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS
 * @author 		Nikolai Senkevich, Stan Bazik
 * @internal 	project/models/AccountantsInstituteModel.php
 * @created 	28/03/2011
 */
class AccountantsInstituteModel extends FNode
{
    /**
     * @var string
     */
    private $homeSlideShow;
    /**
     * @var int
     */
    private $templateRightImageId;

    /**
     * Returns top menu
     *
     * @return array
     */
    public function getTopMenu()
    {
        $menu = FMenu::get(AccountantsInstituteControler::ACCOUNTANTS_INSTITUTE_FOLDER);
        return $menu;
    }

    /**
     * @return FImage
     */
    public function getTemplateRightImage()
    {
        try {
            $image = new FImage($this->templateRightImageId);
        } catch (Exception $e) {
            $image = new FImage();
        }
        return $image;
    }

    /**
     * Return image id
     *
     * @return string
     */
    public function getTemplateRightImageId()
    {
        return $this->templateRightImageId;
    }

    /**
     * Return home video
     *
     * @return string
     */
    public function getHomeSlideShow()
    {
        return $this->homeSlideShow;
    }

    /**
     * Set image id
     *
     * @return void
     */
    public function setTemplateRightImageId($templateRightImageId)
    {
        $this->templateRightImageId = $templateRightImageId;
    }

    /**
     * Set home video
     *
     * @return void
     */
    public function setHomeSlideShow($homeSlideShow)
    {
        $this->homeSlideShow = $homeSlideShow;
    }

    /*     * ********************************************* ADMIN ********************************************** */

    /**
     * Provides adding custom data
     * 
     * @return void 
     */
    public function addCustomData()
    {
        $this->homeSlideShow = $this->getProperty('homeSlideShow');
        $this->templateRightImageId = $this->getProperty('templateRightImageId');
        parent::addCustomData();
    }

    /**
     * Provides saving custom data
     *
     * @param string $action
     * @return void
     */
    public function saveCustomData($action)
    {
        $this->saveProperty('homeSlideShow', $this->homeSlideShow);
        $this->saveProperty('templateRightImageId', $this->templateRightImageId);
        parent::saveCustomData($action);
    }

}