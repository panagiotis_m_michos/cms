<?php

class Export extends FNode
{
    /**
     * @var array
     */
    public $excludedProducts = array();

    protected function addCustomData()
    {
        parent::addCustomData();
        $this->excludedProducts = array_filter(explode(',', $this->getProperty('excludedProducts')));
    }

    /**
     * @param string $action
     */
    public function saveCustomData($action)
    {
        parent::saveCustomData($action);
        $this->saveProperty('excludedProducts', implode(',', $this->excludedProducts));
    }

    public function isOk2show()
    {
        if ($this->adminControler != 'OrdersExportAdminControler') {
            return FALSE;
        }
        return parent::isOk2show();
    }

}
