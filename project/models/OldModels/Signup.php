<?php

/**
 * CMS
 * @category   Project
 * @package    CMS
 * @author     Nikolai Senkevich, Stanislav Bazik
 * @version    Signup.php 2012-03-14 
 */

class Signup extends Object
{
	/* types */
	const TYPE_STARTBUSINESS = 'STARTBUSINESS';
    const TYPE_SAVVYSURVEY = 'SAVVYSURVEY';

	/**
	 * @var array
	 */
	static public $types = array(
		self::TYPE_STARTBUSINESS => 'Start business'
	);
	
	/**
	 * primary key
	 *
	 * @var int
	 */
	public $signupId;
	
	/**
	 * @var string
	 */
	public $name;
	
	/**
	 * @var string
	 */
	public $email;
	
	/**
	 * @var string
	 */
	public $typeId;
	
	/**
	 * @var string
	 */
	public $dtc;
	
	/**
	 * @var string
	 */
	public $dtm;
	
		
	/**
	 * @return int
	 */
	public function getId() 
	{ 
		return $this->signupId; 
	}
	
	/**
	 * @return bool
	 */
	public function isNew() 
	{ 
		return ($this->signupId == 0); 
	}
	
	/**
	 * @return string
	 */
	public function getType()
	{
		return (isset(self::$types[$this->typeId])) ? self::$types[$this->typeId] : NULL;
	}

	/**
	 * @param int $signupId
	 * @return void
	 * @throws Exception
	 */
	public function __construct($signupId = 0)
	{
		$this->signupId = (int) $signupId;
		if ($this->signupId) {
			$w = dibi::select('*')->from(TBL_SIGNUPS)->where('signupId=%i', $signupId)->execute()->fetch();
			if ($w) {				
				$this->signupId = $w['signupId'];
				$this->name = $w['name'];
				$this->email = $w['email'];
				$this->typeId = $w['typeId'];
				$this->dtc = $w['dtc'];
				$this->dtm = $w['dtm'];
			} else {
				throw new Exception("Signup with id `$signupId` doesn't exist!");
			}
		} 
	}
	
	/**
	 * @return int
	 */
	public function save()
	{
		$data = array();
		$data['name'] = $this->name;
		$data['email'] = $this->email;
		$data['typeId'] = $this->typeId;
		$data['dtm'] = new DibiDateTime();
		
		if ($this->isNew()) {
			$data['dtc'] = new DibiDateTime();
			$this->signupId = dibi::insert(TBL_SIGNUPS, $data)->execute(dibi::IDENTIFIER);
		} else {
			dibi::update(TBL_SIGNUPS, $data)->where('signupId=%i', $this->signupId)->execute();
		}
		return $this->signupId;
	}

	/**
	 * @return void
	 */
	public function delete()
	{
		dibi::delete(TBL_SIGNUPS)->where('signupId=%i', $this->signupId)->execute();
	}
	
	/**
	 * Returns count of signups
	 *
	 * @param array $where
	 * @return int
	 */
	static public function getCount(array $where = array())
	{
		$result = dibi::select('count(*)')->from(TBL_SIGNUPS);
		foreach ($where as $key => $val) $result->where('%n', $key, ' = %s',$val);
		$count = $result->execute()->fetchSingle();
		return $count;
	}
	
	/**
	 * Returns all signups
	 *
	 * @param int $limit
	 * @param int $offset
	 * @param array $where
	 * @return int
	 */
	static public function getAll($limit = NULL, $offset = NULL, array $where = array())
	{
		$result = dibi::select('*')->from(TBL_SIGNUPS)->orderBy('dtc', dibi::DESC);
		if ($limit !== NULL) $result->limit($limit);
		if ($offset !== NULL) $result->offset($offset);
		foreach ($where as $key => $val) $result->where('%n', $key, ' = %s',$val);
		return $result->execute()->fetchAssoc('signupId');
	}
	
	/**
	 * Returns result as objects
	 *
	 * @param int $limit
	 * @param int $offset
	 * @param array $where
	 * @return array<MrSiteCode>
	 */
	static public function getAllObjects($limit = NULL, $offset = NULL, array $where = array())
	{
		$objects = array();
		$all = self::getAll($limit, $offset, $where);
		foreach ($all as $key => $val) {
			$objects[$key] = new self($key);		
		}
		return $objects;
	}
	
	
}