<?php

/**
 * @package     Companies Made Simple
 * @subpackage     CMS 
 * @author         Stan (diviak@gmail.com)
 * @internal     project/models/JourneyModel.php
 * @created     24/05/2010
 */

class JourneyModel extends Object
{

    /**
     * Provides sending CVS file with journey customer every night
     *
     * @return void
     */
    public static function sendMidnightCsv()
    {
        /* @var $product JourneyProduct */
        $products = JourneyProduct::getCsvSendProducts();
        foreach ($products as $productId => $product) {
            $csv = JourneyCustomer::geCsvCustomers($productId);
            if ($csv !== NULL) {
                $journeyEmailer = Registry::$emailerFactory->get(EmailerFactory::JOURNEY);
                $journeyEmailer->midnightCsvEmail($product, $csv);
            }
        }
    }
}