<?php

/**
 * @package 	CMS
 * @subpackage 	CMS 
 * @author 		Stan (diviak@gmail.com)
 * @internal 	project/models/JourneyProduct.php
 * @created 	16/07/2009
 */

class JourneyPage extends FNode
{
    const JOURNEY_PAGE = 942;
	
    /** @var array */
	public $column1 = array();

	/** @var array */
	public $column2 = array();

    /** @var array */
	public $column3 = array();

	/**
	 * Provides saving additional data
	 *
	 * @return void
	 */
	protected function addCustomData()
	{   
		parent::addCustomData();
        $arr = array('column1','column2','column3');
        foreach ($arr as $key=>$val) {
			$products = $this->getProperty($val);
            //pr($products);
			$this->$val = explode(',', $products);
		}//exit;
	}
	
	/**
	 * Provides saving additional data
	 *
	 * @param string $action
	 * @return void
	 */
	public function saveCustomData($action)
	{
		parent::saveCustomData($action);
		
        // save products
		$arr = array('column1','column2','column3');
		foreach ($arr as $key=>$val) {
			$products = NULL;
			if (!empty($this->$val)) {
				$products = implode(',', $this->$val);
			}
			$this->saveProperty($val, $products);
		}
	}
	
	/**
	 * Returns if product is OK to display
	 *
	 * @return boolean
	 */
	public function isOk2show()
	{
		if ($this->adminControler != 'JourneyPageAdminControler') {
			return FALSE;
		}
		return parent::isOk2show();
	}

    /**
     * @return array<FNode>
     */
    public static  function getCategories()
    {
        $folders = array();
        foreach (FNode::getChildsNodes(141, false) as $key=>$val) {
			if ($val->adminControler === 'FolderAdminControler') {
				$folders[$key] = $val->getLngTitle();
			}
		}
        return $folders;
    }

}