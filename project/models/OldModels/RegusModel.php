<?php

class RegusModel
{

    protected $tableName = 'ch_regus';

    /**
     * @var DibiConnection 
     */
    protected $dibi;

    /**
     * @param DibiConnection $dibi
     */
    public function __construct($dibi) 
    {
        $this->dibi = $dibi;
    }
    
    /**
     * @param int $id
     * @return Regus
     * @throws Exception 
     */
    public function findById($id) 
    {
        $r = $this->dibi->select('*')->from($this->tableName)->where('id=%i', $id)->execute()->fetch();
        if (!$r) {
            throw new Exception("Could not find Regus Entity with id: $id");
        }
        $entity = $this->createRegus($r);
        return $entity;
    }

    /**
     * @param int $customerId
     * @return Regus
     * @throws Exception
     */
    public function findByCustomerId($customerId) 
    {
        $r = $this->dibi->select('*')->from($this->tableName)->where('customerId=%i', $customerId)->execute()->fetch();
        if(!$r) {
            return NULL;
        }
        $entity = $this->createRegus($r);
        return $entity;
    }
    /**
     * @return array 
     */
    public function getAll() 
    {
        $records = $this->dibi->select('*')->from($this->tableName)->execute()->fetchAll();
        $entities = array();
        foreach ($records as $r) {
            $entities[] = $this->createRegus($r);
        }
        return $entities;
    }
    
    /**
     * @param Regus $entity 
     * @return void
     */
    public function save(Regus $entity) 
    {
        $data = array();
        $data['customerId'] = $entity->getCustomerId();
        $data['firstName'] = $entity->getFirstName();
        $data['lastName'] = $entity->getLastName();
        $data['email'] = $entity->getEmail();
        $data['dialCode'] = $entity->getDialCode();
        $data['phone'] = $entity->getPhone();
        $data['send'] = $entity->getSend();
        $data['dtm'] = new DibiDateTime();

        if ($entity->isNew()) {
            $data['dtc'] = new DibiDateTime();
            $id = $this->dibi->insert($this->tableName, $data)->execute(dibi::IDENTIFIER);
            $entity->setId($id);
        } else {
            $this->dibi->update($this->tableName, $data)->where('id=%i', $entity->getId())->execute();
        }
    }

    /**
     * @return void
     */
    public function delete($id) 
    {
        $this->dibi->delete($this->tableName)->where('id=%i', $id)->execute();
    }

    /**
     * @param type $r
     * @return Regus
     * @throws Exception
     */
    public function createRegus($r)
    {
        $entity = new Regus(
            $r['customerId'], $r['firstName'], $r['lastName'], 
            $r['email'], $r['phone'], $r['dialCode']
        );
        $entity->setId($r['id']);
        $entity->setSend($r['send']);
        $entity->setDtc($r['dtc']);
        $entity->setDtm($r['dtm']);
        return $entity;
    }
}
