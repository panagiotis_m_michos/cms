<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    PaymentTokenLogsModel.php 2011-09-04 <razvanp@madesimplegroup.com>
 */

class PaymentTokenLogsModel extends FNode
{
    /**
     * @param type $tokenLogId
     * @return \TokenLogs
     */
    public function getTokenLogById($tokenLogId) {
        $error = new TokenLogs($tokenLogId);
        return $error;
    }
    
}