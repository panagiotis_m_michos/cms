<?php

class AffiliatesPackagesModel extends FNode
{
    /**
     * @var Affiliate 
     */
    private $affiliate;
    
    /**
     * @var Basket
     */
    private $basket;
    
    /**
     * @var string
     */
    private $companyName;

    /**
     * @param Affiliate $affiliate
     * @param Basket $basket
     * @param string $companyName
     */
    public function startup(Affiliate $affiliate, Basket $basket, $companyName)
    {
        $this->affiliate = $affiliate;            
        $this->basket = $basket;
        $this->companyName = $companyName;
    }
    
    /**
     * @return Affiliate
     */
    public function getAffiliate()
    {
        return $this->affiliate;
    }
    
    /**
     * @param array $callback
     * @return FForm
     */
    public function getComponentForm(array $callback)
    {
        $form = new FForm('packages');
        $form->addRadio('packageId', 'Packages', $this->getPackagesRadioOptions())
            ->addRule(FForm::Required, 'Please provide package');

        $form->addSubmit('submit', 'Submit')->style('width: 200px; height: 30px;');
        $form->onValid = $callback;
        $form->start();

        return $form;
    }

    /**
     * @return array
     */
    private function getPackagesRadioOptions()
    {
        $options = [];
        $packages = $this->affiliate->getPackages();
        foreach ($packages as $key => $val) {
            $options[$key] = '';
        }

        return $options;
    }

    /**
     * @param FForm $form
     */
    public function processDefaultForm(FForm $form)
    {
        $data = $form->getValues();

        // add package
        $package = FNode::getProductById($data['packageId']);
        $package->companyName = $this->companyName;
        $this->basket->add($package);

        $form->clean();
    }
}
