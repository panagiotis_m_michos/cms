<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS 
 * @author 		Stan (diviak@gmail.com)
 * @internal 	project/models/TaxAssistLeadTracking.php
 * @created 	17/08/2010
 */

class TaxAssistLeadTracking extends Object
{

	/**
	 * primary key
	 *
	 * @var int
	 */
	public $leadId;
    	
	/**
	 * @var string
	 */
	public $type;
    
	/**
	 * @var string
	 */
	public $name;
	
	/**
	 * @var string
	 */
	public $email;
	
	/**
	 * @var string
	 */
	public $postcode;
    
    /**
     * @var string 
     */
    public $phone;
	
    /**
     * @var string 
     */
    public $companyName;
    
	/**
	 * @var string
	 */
	public $dtc;
	
	/** @var string */
	public $dtm;
	
	/**
	 * @return int
	 */
	public function getId() 
	{ 
		return $this->leadId; 
	}
	
	/**
	 * @return bool
	 */
	public function isNew() 
	{ 
		return ($this->leadId == 0); 
	}
	
	/**
	 * @return string
	 */
	public function getType()
	{
		return (isset(self::$types[$this->type])) ? self::$types[$this->type] : NULL;
	}

	/**
	 * @param int $leadId
	 * @return void
	 * @throws Exception
	 */
	public function __construct($leadId = 0)
	{
		$this->leadId = (int) $leadId;
		if ($this->leadId) {
			$w = dibi::select('*')->from(TBL_TAX_ASSIST_LEAD_TRACKING)->where('leadId=%i', $leadId)->execute()->fetch();
			if ($w) {				
				$this->leadId = $w['leadId'];
                $this->type = $w['type'];
				$this->name = $w['name'];
				$this->email = $w['email'];
                $this->postcode = $w['postcode'];
                $this->phone = $w['phone'];
                $this->companyName = $w['companyName'];
				$this->dtc = $w['dtc'];
				$this->dtm = $w['dtm'];
			} else {
				throw new Exception("Signup with id `$leadId` doesn't exist!");
			}
		} 
	}
	
	/**
	 * @return int
	 */
	public function save()
	{
		$data = array();
        $data['type'] = $this->type;
		$data['name'] = $this->name;
		$data['email'] = $this->email;
		$data['postcode'] = $this->postcode;
        $data['phone'] = $this->phone;
		$data['companyName'] = $this->companyName;
		$data['dtm'] = new DibiDateTime();
        
        
		if ($this->isNew()) {
			$data['dtc'] = new DibiDateTime();
			$this->leadId = dibi::insert(TBL_TAX_ASSIST_LEAD_TRACKING, $data)->execute(dibi::IDENTIFIER);
		} else {
			dibi::update(TBL_TAX_ASSIST_LEAD_TRACKING, $data)->where('leadId=%i', $this->leadId)->execute();
		}
		return $this->leadId;
	}

	/**
	 * @return void
	 */
	public function delete()
	{
		dibi::delete(TBL_TAX_ASSIST_LEAD_TRACKING)->where('leadId=%i', $this->leadId)->execute();
	}
	
	/**
	 * Returns count of signups
	 *
	 * @param array $where
	 * @return int
	 */
	static public function getCount(array $where = array())
	{
		$result = dibi::select('count(*)')->from(TBL_TAX_ASSIST_LEAD_TRACKING);
		foreach ($where as $key => $val) $result->where('%n', $key, ' = %s',$val);
		$count = $result->execute()->fetchSingle();
		return $count;
	}
	
	/**
	 * Returns all signups
	 *
	 * @param int $limit
	 * @param int $offset
	 * @param array $where
	 * @return int
	 */
	static public function getAll($limit = NULL, $offset = NULL, array $where = array())
	{
		$result = dibi::select('*')->from(TBL_TAX_ASSIST_LEAD_TRACKING)->orderBy('dtc', dibi::DESC);
		if ($limit !== NULL) $result->limit($limit);
		if ($offset !== NULL) $result->offset($offset);
		foreach ($where as $key => $val) $result->where('%n', $key, ' = %s',$val);
		return $result->execute()->fetchAssoc('leadId');
	}
	
	/**
	 * Returns result as objects
	 *
	 * @param int $limit
	 * @param int $offset
	 * @param array $where
	 * @return array<MrSiteCode>
	 */
	static public function getAllObjects($limit = NULL, $offset = NULL, array $where = array())
	{
		$objects = array();
		$all = self::getAll($limit, $offset, $where);
		foreach ($all as $key => $val) {
			$objects[$key] = new self($key);		
		}
		return $objects;
	}
	
	
}