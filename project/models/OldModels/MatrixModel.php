<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    MatrixModel.php 2012-03-13 razvanp@madesimplegroup.com
 */

class MatrixModel extends FNode
{
    
    /**
     * Matrix Pakages Nodes
     * @var array  
     */
    protected $matrixPakages = array();
    
    /**
     *
     * @param array $matrixPakages 
     */
    public function setMatrixPakages($matrixPakages)
    {
        $this->matrixPakages = $matrixPakages;
    }
    
    /**
     *
     * @return array 
     */
    public function getMatrixPakages()
    {
        return $this->matrixPakages;
    }
    
    
    
    public function getMatrixProperties()
    {
        $pakages = FProperty::getNodePropsValues(MatrixAdminControler::MATRIX_ADMIN_PAGE);
        if (isset($pakages) && array_key_exists('matrixPakages', $pakages)) {
            return json_decode($pakages['matrixPakages']);
        }
        throw new Exception('No Pakage was selected for view');
    }
    
    
    /**
	 * Returns Matrix packages
	 * @param boolean $returnAsObjects
	 * @return array
	 */
	static public function getAllPackages($returnAsObjects = FALSE)
	{
		$packages = array();
		$whiteList = array('PackageAdminControler');
		foreach (FNode::getChildsNodes(108) as $key=>$val) {
            if (in_array($val->adminControler, $whiteList) && array_key_exists($key, Package::$types)) {
				$packages[$key] = $val->getLngTitle();
			}
		}
		return $packages;
	}
    
    /*     * ********************************************* ADMIN ********************************************** */

    /**
     * Provides adding custom data
     * 
     * @return void 
     */
    public function addCustomData()
    {
        $this->matrixPakages = json_decode($this->getProperty('matrixPakages'));
        parent::addCustomData();
    }

    /**
     * Provides saving custom data
     *
     * @param string $action
     * @return void
     */
    public function saveCustomData($action)
    {
        $this->saveProperty('matrixPakages', json_encode($this->matrixPakages));
        parent::saveCustomData($action);
    }
    
  
}