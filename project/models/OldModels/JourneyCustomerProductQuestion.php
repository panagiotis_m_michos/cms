<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS 
 * @author 		Stan (diviak@gmail.com)
 * @internal 	project/models/JourneyCustomerProductQuestion.php
 * @created 	17/07/2009
 */

class JourneyCustomerProductQuestion extends Object
{
    	/** @var int */
	public $journeyCustomerProductId;

	/** @var int */
	public $questionId;
	
	/** @var string */
	public $question;

    /** @var string */
	public $answer;
	
		
	/**
	 * @return int
	 */
	public function getId() { return( $this->questionId ); }

	/**
	 * @return bool
	 */
	public function exists() { return( $this->questionId > 0 ); }

	/**
	 * @return bool
	 */
	public function isNew() { return( $this->questionId == 0 ); }

	/**
	 * @param int $questionId
	 * @return void
	 */
	public function __construct($questionId = 0)
	{
		$this->questionId = (int)$questionId;
		if ($this->questionId) {
			$w = FApplication::$db->select('*')->from(TBL_JOURNEY_PRODUCTS_QUESTIONS)->where('questionId=%i', $questionId)->execute()->fetch();
			if ($w) {
				$this->questionId = $w['questionId'];
                $this->journeyCustomerProductId = $w['journeyCustomerProductId'];
				$this->question = $w['question'];
                $this->answer = $w['answer'];
			} else {
				throw new Exception("__CLASS__ with id `$questionId` doesn't exist!");
			}
		} 
	}
	
	/**
	 * Provides save order item to database
	 * @return void
	 */
	public function save()
	{
		$data = array();
        $data['journeyCustomerProductId'] = $this->journeyCustomerProductId;
		$data['question'] = $this->question;
        $data['answer'] = $this->answer;
		
		// insert
		if ($this->isNew()){
			$this->questionId = FApplication::$db->insert(TBL_JOURNEY_PRODUCTS_QUESTIONS, $data)->execute(dibi::IDENTIFIER);
		// update
		} else {
			FApplication::$db->update(TBL_JOURNEY_PRODUCTS_QUESTIONS, $values)->where('questionId=%i', $this->questionId)->execute();
		}
		return $this->questionId;
	}

	/**
	 * @return void
	 */
	public function delete()
	{
		FApplication::$db->delete(TBL_JOURNEY_PRODUCTS_QUESTIONS)->where('questionId=%i', $this->questionId)->execute();
	}
	
	/**
	 * Returns all products by the conditions
	 *
	 * @param int $limit
	 * @param int $offset
	 * @param array $where
	 * @return array<DibiRow>
	 */
	static public function getAll($limit = NULL, $offset = NULL, array $where = array())
	{
		$result = FApplication::$db->select('questionId')
			->from(TBL_JOURNEY_PRODUCTS_QUESTIONS)
			->orderBy('questionId', dibi::DESC);
			
		foreach ($where as $key => $val) {
			$val = '%'.$val.'%';
			$result->where('%n', $key, ' LIKE %s', $val);
		}
		if ($limit !== NULL) $result->limit($limit);
		if ($offset !== NULL) $result->offset($offset);
		
		$result->execute()->fetchSingle();
		return $result;
	}
	
	/**
	 * Returns items as objects
	 *
	 * @param int $limit
	 * @param int $offset
	 * @param array $where
	 * @return array<JourneyCustomerProductQuestion>
	 */
	static public function getAllObjects($limit = NULL, $offset = NULL, array $where = array())
	{
		$return = array();
		$array = self::getAll($limit, $offset, $where);
		foreach ($array as $key => $val) {
			try {
				$return[$val->questionId] = new self($val->questionId);
			} catch (Exception $e) {
			}
		}
		return $return;
	}
	
	
}