<?php

class JourneyProduct extends FNode
{
    const FOLDER_PAGE = 141;
    const JOURNEY_PRODUCTS_FOLDER = 141;
    
    /**
     * @var string
     */
    public $emails;
    
    /**
     * @var string
     */
    public $emailText;
    
    /**
     * @var int
     */
    public $imageId;

    /**
     * @var FImage
     */
    public $image;

    /**
     * @var array
     */
    public $relatedProducts = [];

    /**
     * @var array
     */
    public $blacklistedProducts = [];

    /**
     * @var bool
     */
    public $sendEmailInstantly;

    /**
     * @var bool
     */
    public $sendCsvDaily;

    /**
     * @var array
     */
    public $question;
    
    protected function addCustomData()
    {
        parent::addCustomData();
        $this->emails = $this->getProperty('emails');
        $this->emailText = $this->getProperty('emailText');
        $this->imageId = $this->getProperty('imageId');
        $this->sendEmailInstantly = (bool) $this->getProperty('sendEmailInstantly');
        $this->sendCsvDaily = (bool) $this->getProperty('sendCsvDaily');
        $this->question = json_decode($this->getProperty('question'));

        if ($this->imageId != NULL) {
            try {
                $this->image = new FImage($this->imageId);
            } catch (Exception $e) {
                $this->imageId = NULL;
            }
        }
        
        $relatedProducts = $this->getProperty('relatedProducts');
        if ($relatedProducts != NULL) {
            $this->relatedProducts = explode(',', $relatedProducts);
        }
                
        $blacklistedProducts = $this->getProperty('blacklistedProducts');
        if ($blacklistedProducts != NULL) {
            $this->blacklistedProducts = explode(',', $blacklistedProducts);
        }
    }
    
    /**
     * @param string $action
     */
    public function saveCustomData($action)
    {
        parent::saveCustomData($action);
        $this->saveProperty('emails', $this->emails);
        $this->saveProperty('emailText', $this->emailText);
        $this->saveProperty('imageId', $this->imageId);
        $this->saveProperty('sendEmailInstantly', $this->sendEmailInstantly);
        $this->saveProperty('sendCsvDaily', $this->sendCsvDaily);
        $this->saveProperty('question', json_encode($this->question));
        

        if (!empty($this->relatedProducts)) {
            $relatedProducts = implode(',', $this->relatedProducts);
            $this->saveProperty('relatedProducts', $relatedProducts);
        } else {
            $this->saveProperty('relatedProducts', NULL);
        }
        if (!empty($this->blacklistedProducts)) {
            $blacklistedProducts = implode(',', $this->blacklistedProducts);
            $this->saveProperty('blacklistedProducts', $blacklistedProducts);
        } else {
            $this->saveProperty('blacklistedProducts', NULL);
        }
    }

    /**
     * @return bool
     */
    public function isOk2show()
    {
        if ($this->adminControler != 'JourneyProductAdminControler') {
            return FALSE;
        }

        return parent::isOk2show();
    }


    /**
     * @return JourneyProduct[]
     */
    public static function getCsvSendProducts()
    {
        /* @var $product JourneyProduct */
        $products = FNode::getChildsNodes(self::JOURNEY_PRODUCTS_FOLDER, TRUE, __CLASS__);
        foreach ($products as $key => $product) {
            if ($product->sendCsvDaily == FALSE || empty($product->emails)) {
                unset($products[$key]);
            }
        }

        return $products;
    }

    /**
     * @param int $nodeId
     * @return mixed
     */
    public static function getNodeTitle($nodeId = NULL)
    {
        if ($nodeId === NULL) {
            $nodeId = FApplication::$nodeId;
        }
        $props = FApplication::$db->select('title')
            ->from(TBL_PAGES)
            ->where('node_id=%i', $nodeId)
            ->execute()
            ->fetchSingle();

        return $props;
    }

    /**
     * @return array
     */
    public function getQuestionsArray()
    {
        $newjorney = [];
        if (!empty($this->question->rows)) {
            foreach ($this->question->rows as $key2 => $val2) {
                if (!empty($val2->text2) & !empty($val2->area) & !empty($val2->text1)) {
                    $newjorney[] = [
                        'answers' => explode(',', $val2->area),
                        'question' => $val2->text1,
                        'helper' => $val2->text2
                    ];
                }
            }
        }

        return $newjorney;
    }
}
