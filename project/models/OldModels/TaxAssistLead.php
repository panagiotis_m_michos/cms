<?php

/**
 * @package     Companies Made Simple
 * @subpackage     CMS 
 * @author         Stan (diviak@gmail.com)
 * @internal     project/models/Affiliate.php
 * @created     19/04/2010
 * 
 */

class TaxAssistLead extends Object
{
    /* database table */
    const TABLE = 'lead_enquiry';
    
    /** @var array */
    static private $optionsTest = array(
        'driver'   => 'mysql',
        'host'     => '83.138.143.145',
        'username' => 'cmstest',
        'password' => 'Cmstest25',
        'database' => 'supportsite',
    );
    
    /** @var array */
    static private $options = array(
        'driver'   => 'mysql',
        'host'     => '94.236.123.145',
        'username' => 'cmsleads',
        'password' => 'nGewLPceQ1!',
        'database' => 'supportsite',
    );
    
    /**
     * @var DibiConnection
     */
    private $conection;
    
    
    // types
    const TYPE_GENERAL = 'General';
    const TYPE_SOLE_TRADER = 'Sole Trader';
    const TYPE_ANNUAL_ACCOUNTS = 'Annual Accounts';
    const TYPE_JOURNEY = 'N/A';    
    /**
     * Provide connection do database
     * 
     * @return void
     */
    public function __construct()
    {
        $this->conection = new DibiConnection(self::$options);
    } 
    
    /**
     * Provides saving data to database
     *
     * @param Customer $customer
     * @param Company $company
     * @return void
     * @throws Exception
     */
    public function saveLead(Customer $customer, Company $company)
    { 
        try {
            $data = array(
                'date' => time(),
                 'name' => $customer->firstName. ' ' . $customer->lastName,
                'company' => $company->getCompanyName(),
                'bustype' => ($company->getCompanyNumber() == NULL) ? $company->getLastFormSubmission()->getForm()->getType() : $company->getCompanyCategory(),
                'daytel' => $customer->phone,
                'evetel' => $customer->additionalPhone,
                'bestcall' => 'N/A',
                'email' => $customer->email,
                'address' => $customer->getAddress(),
                'postcode' => $customer->postcode,
                'hearus' => 'Companies Made Simple',
                'comments' => 'N/A',
                'status' => 'Work Queue - Companies Made Simple',
                'leadsource' => 'Web',
                'calltransferred' => 'No',
                'numbersupplied' => 'No',
            );
            $this->conection->insert(self::TABLE, $data)->execute();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
    
    /**
     * Provides saving lead from journey page
     *
     * @param string $type
     * @param string $email
     * @param string $fullName
     * @param string $phone
     * @param string $postcode
     * @param string $companyName
     * @return void
     * @throws Exception
     */
    public function saveTALead($type, $email, $fullName, $phone, $postcode, $companyName)
    {
        try {
            $data = array(
                'date' => time(),
                 'name' => $fullName,
                'company' => $companyName,
                'bustype' => 'N/A',
                'daytel' => $phone,
                'evetel' => 'N/A',
                'bestcall' => 'N/A',
                'email' => $email,
                'address' => 'N/A',
                'postcode' => $postcode,
                'hearus' => 'Companies Made Simple',
                'comments' => $type,
                'status' => 'Work Queue - CMS Contact Data',
                'leadsource' => 'Web',
                'calltransferred' => 'No',
                'numbersupplied' => 'No',
            );
            
            $this->conection->insert(self::TABLE, $data)->execute();
            
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}
