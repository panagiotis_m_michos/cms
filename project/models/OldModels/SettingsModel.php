<?php

class SettingsModel extends FNode {

    /**
     * Returns existing settings object, identified by id
     *
     * @param int $id
     * @return Settings
     */
    public static function getSettings($id) {

        try {
            return Settings::getInstance($id);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Returns new instance of Settings class. Call save method on this object
     * to save it in database.
     *
     * @return Settings
     */
    public static function getNewSettings() {
        return Settings::getNewInstance();
    }

    /**
     * Returns existing settings object, identified by key
     *
     * @param string $key
     * @return Settings
     */
    public static function getSettingsByKey($key) {

        try {
            return Settings::getInstanceByKey($key);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * returns form to view and edit settings
     *
     * @param array $callback
     * @return FForm
     */
    public function getSettingsForm(array $callback) {

		$form = new FForm('settings_' . $this->nodeId);

		$form->addFieldset('Settings');
        $form->addCheckbox('autopolling', 'Autopolling: ', 1)->setValue(self::getSettingsByKey('autopolling')->getValue());

		$form->addFieldset('Action');
		$form->addSubmit('save', 'Submit')->class('btn')->style('width: 200px; height: 30px;');

        $form->onValid = $callback;
        $form->clean();
		$form->start();
		return $form;
    }

    /**
     * Saves data from the settings form
     *
     * @param FForm $form
     */
    public function saveSettingsFormData(FForm $form) {

		try {
			$data = $form->getValues();
            unset($data['submited_settings_723']);
            unset($data['save']);

			if ($form->isSubmitedBy('save')) {
                /* save all settings */
                foreach($data as $key => $value) {
                    $settings = self::getSettingsByKey($key);
                    $settings->setValue($value);
                    $settings->save();
                }
			}
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
    }
}
