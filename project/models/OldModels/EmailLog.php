<?php

class EmailLog extends Object
{

    /**
     * @var int
     */
    private $emailLogId;

    /**
     * @var int
     */
    private $customerId;

    /**
     * @var string
     */
    private $emailName;

    /**
     * @var int
     */
    private $emailNodeId;

    /**
     * @var array
     */
    private $emailFrom;

    /**
     * @var array
     */
    private $emailTo;

    /**
     * @var string
     */
    private $emailSubject;

    /**
     * @var mixed
    */
    private $emailBody;

    /**
     * @var int
     */
    private $attachment;

    /**
     * @var DateTime
     */
    private $dtc;

    /**
     * @var DateTime
     */
    private $dtm;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->emailLogId;
    }

    /**
     * @return bool
     */
    public function isNew()
    {
        return ($this->emailLogId == 0);
    }

    /**
     * @param int $emailLogId
     * @throws EntityNotFound
     */
    public function __construct($emailLogId = 0)
    {
        $this->emailLogId = (int) $emailLogId;
        if ($this->emailLogId) {
            $w = dibi::select('*')
                ->from(TBL_EMAIL_LOGS)
                ->where('emailLogId=%i', $emailLogId)
                ->execute()
                ->fetch();
            if ($w) {
                $this->emailLogId = $w['emailLogId'];
                $this->customerId = $w['customerId'];
                $this->emailName = $w['emailName'];
                $this->emailNodeId = $w['emailNodeId'];
                $this->emailFrom = json_decode($w['emailFrom'], TRUE);
                $this->emailTo = json_decode($w['emailTo'], TRUE);
                $this->emailSubject = $w['emailSubject'];
                $this->emailBody = $w['emailBody'];
                $this->attachment = $w['attachment'];
                $this->dtc = new DateTime($w['dtc']);
                $this->dtm = new DateTime($w['dtm']);
            } else {
                throw new EntityNotFound("EmailLog with id `$emailLogId` doesn't exist!");
            }
        }
    }

    /**
     * @return int
     */
    public function save()
    {
        $data = array();
        $data['customerId'] = $this->customerId;
        $data['emailName'] = $this->emailName;
        $data['emailNodeId'] = $this->emailNodeId;
        $data['emailFrom'] = json_encode($this->emailFrom);
        $data['emailTo'] = json_encode($this->emailTo);
        $data['emailSubject'] = $this->emailSubject;
        $data['emailBody'] = (string) $this->emailBody;
        $data['attachment'] = $this->attachment;
        $data['dtm'] = new DibiDateTime();
        $this->dtm = new DateTime();

        if ($this->isNew()) {
            $data['dtc'] = new DibiDateTime();
            $this->dtc = new DateTime();
            $this->emailLogId = dibi::insert(TBL_EMAIL_LOGS, $data)->execute(dibi::IDENTIFIER);
        } else {
            dibi::update(TBL_EMAIL_LOGS, $data)
                ->where('emailLogId=%i', $this->emailLogId)
                ->execute();
        }
        return $this->emailLogId;
    }

    public function delete()
    {
        dibi::delete(TBL_EMAIL_LOGS)
            ->where('emailLogId=%i', $this->emailLogId)
            ->execute();
    }

    /**
     * Returns count of signups
     *
     * @param array $where
     * @return int
     */
    public static function getCount(array $where = array())
    {
        $result = dibi::select('count(*)')->from(TBL_EMAIL_LOGS);
        foreach ($where as $key => $val) {
            $result->where('%n', $key, ' = %s', $val);
        }
        return $result->execute()->fetchSingle();
    }

    /**
     * Returns all signups
     *
     * @param int $limit
     * @param int $offset
     * @param array $where
     * @return DibiRow
     */
    public static function getAll($limit = NULL, $offset = NULL, array $where = array())
    {
        $result = dibi::select('*')->from(TBL_EMAIL_LOGS)->orderBy('dtc', dibi::DESC);
        if ($limit !== NULL) {
            $result->limit($limit);
        }
        if ($offset !== NULL) {
            $result->offset($offset);
        }
        foreach ($where as $key => $val) {
            $result->where('%n', $key, ' = %s', $val);
        }
        return $result->execute()->fetchAssoc('emailLogId');
    }

    /**
     * Returns result as objects
     *
     * @param int $limit
     * @param int $offset
     * @param array $where
     * @return EmailLog[]
     */
    public static function getAllObjects($limit = NULL, $offset = NULL, array $where = array())
    {
        $objects = array();
        $all = self::getAll($limit, $offset, $where);
        foreach ($all as $key => $val) {
            $objects[$key] = new self($key);
        }
        return $objects;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return new Customer($this->customerId);
    }

    /**
     * @return bool
     */
    public function hasCustomer()
    {
        return $this->customerId > 0;
    }

    /**
     * @return int
     */
    public function getEmailLogId()
    {
        return $this->emailLogId;
    }

    /**
     * @param int $emailLogId
     */
    public function setEmailLogId($emailLogId)
    {
        $this->emailLogId = $emailLogId;
    }

    /**
     * @return int
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @param int $customerId
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }

    /**
     * @return string
     */
    public function getEmailName()
    {
        return $this->emailName;
    }

    /**
     * @param string $emailName
     */
    public function setEmailName($emailName)
    {
        $this->emailName = $emailName;
    }

    /**
     * @return int
     */
    public function getEmailNodeId()
    {
        return $this->emailNodeId;
    }

    /**
     * @param int $emailNodeId
     */
    public function setEmailNodeId($emailNodeId)
    {
        $this->emailNodeId = $emailNodeId;
    }

    /**
     * @return array
     */
    public function getEmailFrom()
    {
        return $this->emailFrom;
    }

    /**
     * @param array $emailFrom
     */
    public function setEmailFrom($emailFrom)
    {
        $this->emailFrom = $emailFrom;
    }

    /**
     * @return array
     */
    public function getEmailTo()
    {
        return $this->emailTo;
    }

    /**
     * @param array $emailTo
     */
    public function setEmailTo($emailTo)
    {
        $this->emailTo = $emailTo;
    }

    /**
     * @return string
     */
    public function getEmailSubject()
    {
        return $this->emailSubject;
    }

    /**
     * @param string $emailSubject
     */
    public function setEmailSubject($emailSubject)
    {
        $this->emailSubject = $emailSubject;
    }

    /**
     * @return string
     */
    public function getEmailBody()
    {
        return $this->emailBody;
    }

    /**
     * @param mixed $emailBody
     */
    public function setEmailBody($emailBody)
    {
        $this->emailBody = $emailBody;
    }

    /**
     * @return int
     */
    public function getAttachment()
    {
        return $this->attachment;
    }

    /**
     * @param int $attachment
     */
    public function setAttachment($attachment)
    {
        $this->attachment = $attachment;
    }

    /**
     * @return DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @param DateTime $dtc
     */
    public function setDtc($dtc)
    {
        $this->dtc = $dtc;
    }

    /**
     * @return DateTime
     */
    public function getDtm()
    {
        return $this->dtm;
    }

    /**
     * @param DateTime $dtm
     */
    public function setDtm($dtm)
    {
        $this->dtm = $dtm;
    }
}
