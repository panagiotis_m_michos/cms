<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS 
 * @author 		Stan (diviak@gmail.com)
 * @internal 	project/models/CustomersReviewsModel.php
 * @created 	21/07/2010
 */

class CustomersReviewsModel extends FNode
{
	const REVIEWS_PAGINATOR_ITEMS_PER_PAGE = 25;
	
	/**
	 * Returns paginator for reviews
	 *
	 * @return FPaginator2
	 */
	public function getReviewsPaginator()
	{
		$paginator = new FPaginator2(CustomerReview::getCustomerAllowedAndApproveReviewsCount());
		$paginator->itemsPerPage = self::REVIEWS_PAGINATOR_ITEMS_PER_PAGE;
		$paginator->htmlDisplayEnabled = FALSE;
		return $paginator;
	}
	
	/**
	 * Returns list of reviews
	 *
	 * @param FPaginator2 $paginator
	 * @param array<CustomerReview>
	 */
	public function getReviews(FPaginator2 $paginator)
	{
		$reviews = CustomerReview::getAllowedAndApprovedCustomerReviews(array(), $paginator->getLimit(), $paginator->getOffset());
		return $reviews;
	}
	
	/**
	 * Provides replace text 
	 *
	 * @return string
	 */
	public function getReplacesReviewsText()
	{
		$text = $this->getLngText();
		$replace = array(
			'[SERVICE_RATING]' => $this->getServiceValueFinalPercentage(),
			'[SERVICE_RECOMMEND]' => $this->getRecomendationFinalPercentage(),
		);
		$text = str_replace(array_keys($replace), $replace, $text);
		$text = FApplication::modifyCmsText($text);
		return $text;
	}
	
	/**
	 * @return int
	 */
	public function getSatisfactionFinalPercentage()
	{
		$customerSatiscation = new CustomerReviewsSatisfaction();
		return $customerSatiscation->getFinalPercentageValue();
	}
	
	/**
	 * @return int
	 */
	public function getServiceValueFinalPercentage()
	{
		$serviceValue = new CustomerReviewsServiceValue();
		return $serviceValue->getFinalPercentageValue();
	}
	
	/**
	 * @return int
	 */
	public function getRecomendationFinalPercentage()
	{
		$recommendation = new CustomerReviewsRecommendation();
		return $recommendation->getFinalPercentageValue();
	}
}