<?php

class CompanyShareholderCerificateModel extends FNode
{

    /**
     * Table name
     * @var string
     */
    protected $table = 'ch_company_member';

    /**
     * @var array
     */
    public $filter = array();

    /**
     * @var Person
     */
    protected $shareHolder;

    /**
     * @var array $data
     */
    protected $data;

    /**
     * @var object $shareHolderInfo
     */
    protected $shareHolderInfo;

    /**
     * @var int
     */
    protected $shareholderId;

    /**
     *
     * @var Company
     */
    protected $company;

    /**
     * @return object  $shareHolderInfo
     */
    public function checkIsOkForPrint()
    {
        $AllotmentShares = (object) array_map("trim", $this->shareHolder->getAllotmentShares()->getFields());
        $this->shareHolderInfo = $this->getShareHolderBtMamberAndCapital($this->company, $this->shareholderId);
        if (!$this->checkShareClass($this->data) || !$AllotmentShares->share_class || !$AllotmentShares->num_shares || empty($this->shareHolderInfo)) {
            throw new Exception('Sorry, we are unable to auto produce your share certificate. Please download this <a href="/project/upload/files/share-certificate-template.doc" title="" style="color: blue;"> share certificate template</a>.');
        }

        return $this->shareHolderInfo;
    }

    /**
     *
     * @param Company $company
     * @param int $shareholderId
     */
    public function getShareholderInformations(Company $company, $shareholderId)
    {
        $this->data = $company->getData();
        $this->shareholderId = $shareholderId;
        $this->company = $company;

        if (!isset($shareholderId) || !array_key_exists($shareholderId, $this->data['shareholders'])) {
            throw new Exception('Shareholder not exist!');
        }
        $this->shareHolder = $this->getShareholderType($company, $shareholderId);
        return $this->shareHolder;
    }

    /**
     *
     * @param array $data - Company capitals
     * @return bool
     */
    public function checkShareClass($data)
    {
        if (empty($data['capitals']) || count($data['capitals']) > 1) {

            return FALSE;
        }
        if (empty($data['capitals']) || count($data['capitals'][0]['shares']) > 2) {
            return FALSE;
        };
        return TRUE;
    }

    /**
     *
     * @param Company $company
     * @param int $shareholderId
     * @return type Person
     */
    public function getShareholderType(Company $company, $shareholderId)
    {
        $data = $company->getData();
        if ($data['shareholders'][$shareholderId]['corporate'] == 1) {
            return $company->getCorporate($shareholderId, 'SUB');
        } else {
            return $company->getPerson($shareholderId, 'SUB');
        }
    }

    /**
     *
     * @param array $data
     * @param int $shareholderId
     * @return type
     */
    public function isCorporate()
    {
        return ($this->data['shareholders'][$this->shareholderId]['corporate'] == 1) ? TRUE : FALSE;
    }

    /**
     *
     * @param Company $company
     * @param int $shareholderId
     * @return object $fields
     */
    public function getShareHolderBtMamberAndCapital(Company $company, $shareholderId)
    {
        $share = dibi::select('c.company_id , c.company_member_id')
            ->select('c.corporate, c.type, c.share_class AS MemberShare')
            ->select('c.corporate_name,c.forename,c.middle_name, c.surname, c.corporate_name, c.premise, c.street, c.thoroughfare, c.post_town, c.county, c.country,c.postcode, c.dob, c.nationality,c.occupation,c.country_of_residence')
            ->select('c.num_shares as MemberNrShares, cc.company_capital_id, cc.company_id, cc.currency')
            ->select('csh.share_class AS CapitalShare, csh.num_shares')
            ->select('csh.amount_paid, csh.amount_unpaid, csh.nominal_value')
            ->select('(csh.nominal_value / csh.num_shares) AS share_value')
            ->from('ch_company_member', 'c')
            ->leftJoin('ch_company_capital', 'cc')
            ->on('c.company_id=cc.company_id')
            ->leftJoin('ch_company_capital_shares', 'csh')
            ->on('cc.company_capital_id=csh.company_capital_id')
            ->where('c.company_id=%i', $company->getCompanyId())
            ->and('c.company_member_id=%i', $shareholderId)
            ->and('c.type="SUB"')
            ->and('c.share_class=csh.share_class')
            ->execute()
            ->fetchAll();
        //pr($share);
        $fields = array();
        if (count($share) == 1) {
            $fields['shareholderInfo'] = $share[0];
            $fields['shareholderRegistredOffice'] = (object) $company->getRegisteredOffice()->getFields();
            $fields['companyInfo'] = (object) $company->getData();
            return (object) $fields;
        } else {
            return array();
        }
    }

    /**
     *
     * @param array $fields
     * @param int $memberId
     * @return array
     */
    public function updateShareHolderAddress($fields, $memberId)
    {
        return dibi::update($this->table, $fields)->where('company_member_id=%i', $memberId)->execute();
    }

    /**
     *
     * @param array $shareholder
     * @param array $fields
     * @return array
     */
    public function updateShareHolderData($shareholder, $fields)
    {

        $shareholder->shareholderInfo->premise = $fields['premise'];
        $shareholder->shareholderInfo->street = $fields['street'];
        $shareholder->shareholderInfo->thoroughfare = $fields['thoroughfare'];
        $shareholder->shareholderInfo->post_town = $fields['post_town'];
        $shareholder->shareholderInfo->county = $fields['county'];
        $shareholder->shareholderInfo->postcode = $fields['postcode'];
        $shareholder->shareholderInfo->country = $fields['country'];

        return $shareholder;
    }

    /**
     * Format Currency
     * @param array $shareholder info
     * @return string
     */
    public function getFileCurrencyClass($shareholder)
    {

        switch ($shareholder->shareholderInfo->currency) {
            case 'GBP':
                $currency = iconv("UTF-8", "ISO-8859-1", "£");
                break;
            case 'USD':
                $currency = iconv("UTF-8", "ISO-8859-1", "$");
                break;
            case 'EUR':
                $currency = chr(128);
                break;
            default:
                $currency = $shareholder->shareholderInfo->currency;
        }
        return $currency;
    }

    /**
     *
     * @param type $shareholder
     * @return string
     */
    public function getFileShareClass($shareholder)
    {
        $total = $shareholder->shareholderInfo->share_value;
        $class = $this->getFileCurrencyClass($shareholder) . $total;
        return $class;
    }

    /**
     *
     * @param type $shareholder
     * @return string
     */
    public function getMemberShareClass($shareholder)
    {
        return $shareholder->shareholderInfo->MemberShare;
    }

    /**
     *
     * @param type $shareholder
     * @return string
     */
    public function getShareholderCompanyName($shareholder)
    {
        $companyDet = $shareholder->companyInfo->company_details;
        return $companyDet['company_name'];
    }

    /**
     *
     * @param type $shareholder
     * @return string
     */
    public function getShareholderCompanyNumber($shareholder)
    {
        $companyDet = $shareholder->companyInfo->company_details;
        return $companyDet['company_number'];
    }

    /**
     *
     * @param type $shareholder
     * @return string
     */
    public function getShareholderGetFullName($shareholder)
    {
        if ($shareholder->shareholderInfo->corporate == 1) {
            return $shareholder->shareholderInfo->corporate_name;
        }
        return trim($shareholder->shareholderInfo->forename) . ' ' . trim($shareholder->shareholderInfo->middle_name) . ' ' . trim($shareholder->shareholderInfo->surname);
    }

    /**
     *
     * @param object $registredAddress
     * @return object Address $registredAddress
     */
    public function addCountryNameForRegistredAddress($registredAddress)
    {
        if ($registredAddress->country) {
            if (isset(Address::$countries[$registredAddress->country])) {
                $registredAddress->country = Address::$countries[$registredAddress->country];
            }
        }
        return $registredAddress;
    }

    /**
     *
     * @param object $shareholder
     * @return array
     */
    public function getShareholderGetFullAddress($shareholder)
    {
        $address = array();
        (isset($shareholder->shareholderInfo->premise)) ? $address['premise'] = $shareholder->shareholderInfo->premise : NULL;
        (isset($shareholder->shareholderInfo->street)) ? $address['street'] = $shareholder->shareholderInfo->street : NULL;
        (isset($shareholder->shareholderInfo->thoroughfare)) ? $address['thoroughfare'] = $shareholder->shareholderInfo->thoroughfare : NULL;
        (isset($shareholder->shareholderInfo->post_town)) ? $address['post_town'] = $shareholder->shareholderInfo->post_town : NULL;
        (isset($shareholder->shareholderInfo->county)) ? $address['county'] = $shareholder->shareholderInfo->county : NULL;
        (isset($shareholder->shareholderInfo->country)) ? $address['country'] = Address::$countries[$shareholder->shareholderInfo->country] : NULL;
        (isset($shareholder->shareholderInfo->postcode)) ? $address['postcode'] = $shareholder->shareholderInfo->postcode : NULL;

        return $this->removeArrayEmptyValues($address, TRUE);
    }

    /**
     *
     * @param array $array
     * @param bool $remove_null_number
     * @return array
     */
    public function removeArrayEmptyValues($array, $remove_null_number = TRUE)
    {
        $new_array = array();
        $null_exceptions = array();
        foreach ($array as $key => $value) {
            $value = trim($value);
            if ($remove_null_number) {
                $null_exceptions[] = '0';
            }
            if (!in_array($value, $null_exceptions) && $value != "") {
                $new_array[] = $value;
            }
        }
        return $new_array;
    }

    /**
     *
     * @param object $shareholder
     * @param string $pdfName
     * @param int $certNumber
     * @return output
     */
    public function createPdfShareholderCerificate($shareholder, $pdfName = 'ShareholderCertificate.pdf', $certNumber = 1)
    {
        /* create pdf */
        $pdf = new FPDF('L');
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(FALSE);

        /* black border - big black cell */
        $pdf->SetXY(16, 9);
        $pdf->SetMargins(16, 9, 16);
        $pdf->Cell(0, 190, '', 0, 0, 'L', TRUE);

        /* white inner cell */
        $pdf->SetXY(18, 11);
        $pdf->SetMargins(18, 11, 18);
        $pdf->SetFillColor(255, 255, 255);
        $pdf->Cell(0, 186, '', 0, 0, 'L', TRUE);

        /* top boxes */
        $pdf->SetFont('Arial', 'B', 11);
        $pdf->setY(18);

        /* first cell */
        $pdf->SetX(34);
        $pdf->Cell(38, 6, 'Certificate No', 1, 0, 'C');

        /* second cell */
        $pdf->SetX(85);
        $pdf->Cell(127, 6, 'Class', 1, 0, 'C');

        /* third cell */
        $pdf->SetX(225);
        $pdf->Cell(38, 6, 'No of Shares', 1, 0, 'C');

        /* bottom boxes - grey */
        $pdf->SetFont('Arial', '', 11);
        $pdf->SetFillColor(211, 211, 211);
        $pdf->setY(24);

        /* first cell */
        $pdf->SetX(34);
        $pdf->Cell(38, 6, $certNumber, 1, 0, 'C', TRUE);

        /* second cell */
        $pdf->SetX(85);
        $pdf->Cell(127, 6, $this->getFileShareClass($shareholder) . ' ' . $this->getMemberShareClass($shareholder) . '-Shares', 1, 0, 'C', TRUE);

        /* third cell */
        $pdf->SetX(225);
        $pdf->Cell(38, 6, $shareholder->shareholderInfo->MemberNrShares, 1, 0, 'C', TRUE);

        /* text */
        $pdf->SetFont('Arial', 'B', 11);
        $pdf->SetY(42);
        $height = 4;

        $pdf->Cell(0, $height, strtoupper($this->getShareholderCompanyName($shareholder)), 0, 1, 'C');

        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(0, $height, 'Registered in United Kingdom, Number: ' . $this->getShareholderCompanyNumber($shareholder), 0, 1, 'C');
        $pdf->Ln();

        $height = 8;

        $pdf->Cell(0, $height, 'This is to Certify that', 0, 1, 'C');
        $pdf->Cell(0, $height, iconv('UTF-8', 'ISO-8859-1', $this->getShareholderGetFullName($shareholder)), 0, 1, 'C');
        $pdf->Cell(0, $height, 'of', 0, 1, 'C');
        $pdf->Cell(0, $height, implode(', ', $this->getShareholderGetFullAddress($shareholder)), 0, 1, 'C');

        $height = 4;

        $pdf->Cell(0, $height, 'is the Registered Holder of ' . $shareholder->shareholderInfo->MemberNrShares . ' fully paid ' . $this->getMemberShareClass($shareholder) . '-shares of ' . $this->getFileShareClass($shareholder) . ' each in the above-named Company,', 0, 1, 'C');
        $pdf->Cell(0, $height, 'subject to the Memorandum and Articles of Association of the said Company', 0, 1, 'C');
        $pdf->Ln();

        $pdf->SetFont('Arial', 'I', 10);
        $pdf->Cell(0, $height, 'This Certificate was Authorised by:', 0, 1, 'C');
        $pdf->Ln();
        $pdf->Ln();

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetLeftMargin(53);
        $height = 8;

        /* director, secretary - first line */
        $pdf->Cell(25, $height, 'Director(s)', 0, 0, 'R');
        $pdf->Cell(65, $height, '', 'B', 0);

        $pdf->Cell(10, $height, '', 0, 0);

        $pdf->Cell(25, $height, 'Secretary', 0, 0, 'R');
        $pdf->Cell(65, $height, '', 'B', 1);

        /* date - second line */
        $pdf->Cell(25, $height, '', 0, 0, 'R');
        $pdf->Cell(65, $height, '', 'B', 0);

        $pdf->Cell(10, $height, '', 0, 0);

        $pdf->Cell(25, $height, 'Date', 0, 0, 'R');
        $pdf->Cell(65, $height, '', 'B', 1);

        /* bottom lines */
        $pdf->Cell(25, $height, 'Witness', 0, 0, 'R');
        $pdf->Cell(165, $height, '', 'B', 1);

        $pdf->Cell(25, $height, 'Name', 0, 0, 'R');
        $pdf->Cell(165, $height, '', 'B', 1);

        $pdf->Cell(25, $height, 'Address', 0, 0, 'R');
        $pdf->Cell(165, $height, '', 'B', 1);

        $pdf->Cell(25, $height, '', 0, 0, 'R');
        $pdf->Cell(165, $height, '', 'B', 0);

        $pdf->Ln();
        $pdf->Ln();

        /* registered office address */
        $pdf->SetFont('Arial', '', 8);
        $pdf->setX(18);
        $height = 4;

        $pdf->Cell(0, $height, 'Registered Office, ' . implode(', ', $this->removeArrayEmptyValues((array) $this->addCountryNameForRegistredAddress($shareholder->shareholderRegistredOffice))), 0, 1, 'C');

        $pdf->Ln();

        $pdf->setX(18);
        $pdf->Cell(0, $height, 'NOTE: No transfer of any of the above mentioned shares can be registered until this Certificate', 0, 1, 'C');
        $pdf->setX(18);
        $pdf->Cell(0, $height, 'has been deposited at the Company\'s Registered Office', 0, 1, 'C');

        if ($pdfName == NULL) {
            $pdf->Output();
        } else {
            return $pdf->Output($pdfName, 'D');
        }
    }

}
