<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS 
 * @author 		Stan (diviak@gmail.com)
 * @internal 	project/models/CustomersReviewsAdminModel.php
 * @created 	22/07/2010
 */


class CustomersReviewsAdminModel extends FNode
{
	const REVIEWS_PAGINATOR_ITEMS_PER_PAGE = 50;
	
	/**
	 * Returns filter form for list of reviews
	 *
	 * @return FForm
	 */
	public function getComponentReviewsFilter()
	{
		$form = new FForm('reviewsFilter', 'get');
		
		$form->addFieldset('Filter');
		$form->addSelect('statusId', 'Status', CustomerReview::$statuses)
			->setFirstOption('--- All ---')
			->setValue(CustomerReview::STATUS_UNAPPROVED);
		
		$form->addSubmit('submit', 'Submit')->class('btn');
		$form->start();
		return $form;
	}
	
	/**
	 * Returns paginator for reviews
	 *
	 * @return FPaginator2
	 */
	public function getReviewsPaginator(FForm $form)
	{
		try {
			$where = array();
			$data = $form->getValues();
			if (isset($data['statusId'])) $where['statusId'] = $data['statusId'];
			$form->clean();
			$paginator = new FPaginator2(CustomerReview::getCustomerReviewsCount($where));
			$paginator->itemsPerPage = self::REVIEWS_PAGINATOR_ITEMS_PER_PAGE;
			$paginator->htmlDisplayEnabled = FALSE;
			return $paginator;	
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	/**
	 * Returns list of reviews
	 *
	 * @param FPaginator2 $paginator
	 * @param array<CustomerReview>
	 */
	public function getReviews(FPaginator2 $paginator, FForm $form)
	{
		try {
			$where = array();
			$data = $form->getValues();
			if (isset($data['statusId'])) $where['statusId'] = $data['statusId'];
			$form->clean();
			$reviews = CustomerReview::getCustomerReviewsObjects($where, $paginator->getLimit(), $paginator->getOffset());
			return $reviews;	
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	/**
	 * Returns form for batch actions with reviews
	 *
	 * @param array $callback
	 * @param array $reviews
	 * @return FForm
	 */
	public function getBatchActionsForm(array $callback, array $reviews)
	{
		$form = new FForm('batchActions');
		
		/* @var $review CustomerReview */
		$ids = array();
		foreach ($reviews as $review) {
			$id = $review->getId();
			$ids[$id] = NULL;
		}
		$form->addCheckbox('ids', NULL, $ids);
		
		$form->addSelect('action', 'Selected', array(
				'approve' => 'Approve',
				'unapprove' => 'Unapprove',
				'delete' => 'Delete'
			))
			->setFirstOption('--- Select ---')
			->addRule(FForm::Required, 'Please provide action');
		
		$form->addSubmit('actionSubmit', 'Submit')->class('btn');
		
		$form->onValid = $callback;
		$form->start();
		return $form;	
	}
	
	/**
	 * Will process batch actions form
	 *
	 * @param FForm $form
	 * @return void
	 * @throws Exception
	 */
	public function processBatchActionsForm(FForm $form)
	{
		try {
			$data = $form->getValues();
			
			if (isset($data['ids']) && !empty($data['ids'])) {
				switch ($data['action']) {
					
					case 'approve':
						foreach ($data['ids'] as $reviewId) CustomerReview::getCustomerReview($reviewId)->approve();
						break;
						
					case 'unapprove':
						foreach ($data['ids'] as $reviewId) CustomerReview::getCustomerReview($reviewId)->unapprove();
						break;
						
					case 'delete':
						foreach ($data['ids'] as $reviewId) CustomerReview::getCustomerReview($reviewId)->delete();
						break;
						
					default:
						throw new Exception("Action {$data['action']} is not supported!");
						break;
				}
			}
			$form->clean();
			
		} catch (Exception $e) {
			throw $e;
		}
	}
	
	
	/**
	 * Returns customer review edit form
	 *
	 * @param array $callback
	 * @param CustomerReview $customerReview
	 * @return FForm
	 */
	public function getComponentEditForm(array $callback, CustomerReview $customerReview)
	{
		$form = new FForm('customerReviewEdit');
		$form->setRenderClass('Default2Render');
		$grades = array_combine(range(0, 5), range(0, 5));
		
		$form->addFieldset('Customer Satisfaction');
		$form->addSelect('formationProcess', 'formationProcess', $grades)
			->setValue($customerReview->getFormationProcess());
		$form->addSelect('communicationOurProducts', 'communicationOurProducts', $grades)
			->setValue($customerReview->getCommunicationOurProducts());
		$form->addSelect('customerService', 'customerService', $grades)
			->setValue($customerReview->getCustomerService());
		$form->addSelect('providingValueForMoney', 'providingValueForMoney', $grades)
			->setValue($customerReview->getProvidingValueForMoney());
		$form->addSelect('respondingPromptlyToQueries', 'respondingPromptlyToQueries', $grades)
			->setValue($customerReview->getRespondingPromptlyToQueries());
		
		$form->addFieldset('Service Value');
		$form->addSelect('rateQualityOfProducts', 'rateQualityOfProducts', $grades)
			->setValue($customerReview->getRateQualityOfProducts());
		
		$form->addFieldset('Service Recommendation');
		$form->addSelect('recomendOurServices', 'recomendOurServices', $grades)
			->setValue($customerReview->getRecomendOurServices());
		
		$form->addFieldset('Suggestions');
		$form->addArea('suggestions', 'Suggestions')
			->cols(40)->rows(6)
			->setValue($customerReview->getSuggestions());
		$form->addArea('comment', 'Comment')
			->cols(40)->rows(6)
			->setValue($customerReview->getComment());
			
		$form->addFieldset('Personal Details');
		$form->addText('name', 'Name:')
			->setValue($customerReview->getName());
		$form->addText('companyName', 'Company Name:')
			->setValue($customerReview->getCompanyName());
			
		$form->addFieldset('Other');
		$form->addCheckbox('allowed', 'Allowed', 1)
			->setValue($customerReview->getAllowed());
		$form->addRadio('statusId', 'Status', CustomerReview::$statuses)
			->setValue($customerReview->getStatusId());
		
		$form->addFieldset('Action');
		$form->addSubmit('submit', 'Submit')
			->class('btn')
			->style('width: 200px; height: 30px;');
			
		$form->onValid = $callback;
		$form->start();
		return $form;
	}
	
	/**
	 * Provides saving edit form
	 *
	 * @param FForm $form
	 * @param CustomerReview $customerReview
	 * @return void
	 * @throws Exception
	 */
	public function processEditForm(FForm $form, CustomerReview $customerReview)
	{
		try {
			$data = $form->getValues();
			
			$customerReview->setStatusId($data['statusId']);
			$customerReview->setFormationProcess($data['formationProcess']);
			$customerReview->setCommunicationOurProducts($data['communicationOurProducts']);
			$customerReview->setCustomerService($data['customerService']);
			$customerReview->setProvidingValueForMoney($data['providingValueForMoney']);
			$customerReview->setRespondingPromptlyToQueries($data['respondingPromptlyToQueries']);
			$customerReview->setRateQualityOfProducts($data['rateQualityOfProducts']);
			$customerReview->setRecomendOurServices($data['recomendOurServices']);
			$customerReview->setSuggestions($data['suggestions']);
			$customerReview->setComment($data['comment']);
			$customerReview->setAllowed($data['allowed']);
			$customerReview->setName($data['name']);
			$customerReview->setCompanyName($data['companyName']);
			$customerReview->save();
			
			$form->clean();
			
		} catch (Exception $e) {
			throw $e;
		}
	}
	
	/**
	 * Returns import form
	 *
	 * @param array $callback
	 * @return FForm
	 */
	public function getComponentImportForm(array $callback)
	{
		$form = new FForm('reviewsImport');
		$form->setRenderClass('Default2Render');
		
		$form->addFieldset('CSV File to Import');
		$form->addFile('csv', 'File')
			->addRule(FForm::Required, 'Please provide file');
		
		$form->addFieldset('Action');
		$form->addSubmit('submit', 'Submit')
			->style('width: 200px; height: 30px;')
			->class('btn');
		
		$form->onValid = $callback;
		$form->start();
		return $form;
	}
	
	/**
	 * Will process import form
	 *
	 * @param FForm $form
	 * @return void
	 * @throws Exception
	 */
	public function processImportForm(FForm $form)
	{
		try {
			$data = $form->getValues();
			CustomerReviewsCSVImport::import($data['csv']['tmp_name']);
			$form->clean();
		} catch (Exception $e) {
			throw $e;
		}
	}
	
}