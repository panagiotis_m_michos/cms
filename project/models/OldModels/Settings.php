<?php

class Settings {

    private $id;
    private $key;
    private $value;
    private static $tableName = 'cms2_settings';

    private function __construct() {
        $this->setVariables(array());   // set instance variables to null
    }

    /**
     * sets instance variables to the values from database.
     * If specified id does not exist, exception is thrown.
     *
     * @param int $id
     * @return array result
     */
    private static function getDataById($id) {

        $id = (int) $id;
        $result = dibi::select('settings_id')
            ->select('key')
            ->select('value')
            ->from(self::$tableName)
            ->where('settings_id = %i', $id)
            ->execute()->fetchAll();

        if (!$result) {
            throw new Exception('Record with id - ' . $id . ' does not exist');
        }

        return (array) $result[0];
    }

    /**
     * sets instance variables to the values from database.
     * If specified key does not exist, exception is thrown.
     * Returns only one row
     *
     * @param string $key
     * @return array result
     */
    private static function getDataByKey($key) {

        $key = (string) $key;
        $result = dibi::select('settings_id')
            ->select('key')
            ->select('value')
            ->from(self::$tableName)
            ->where('`key` = %s', $key)
            ->execute()->fetchAll();

        if (!$result) {
            throw new Exception('Record with key - ' . $key . ' does not exist');
        }

        return (array) $result[0];
    }

    /**
     * sets instance variables to the values from the supplied array.
     * if any key is missing, then value is set to null.
     *
     * @param array $result represents one row from database
     */
    private function setVariables(array $result) {

        $this->id = isset($result['settings_id']) ? $result['settings_id'] : null;
        $this->key = isset($result['key']) ? $result['key'] : null;
        $this->value = isset($result['value']) ? $result['value'] : null;
    }

    /**
     * returns new instance, after setting values call save() save method
     *
     * @return Settings
     */
    public static function getNewInstance() {
        return new Settings();
    }

    /**
     *
     * @param int $id   id from database
     * @return Settings
     */
    public static function getInstance($id) {

        $settings = new Settings();
        $settings->setVariables(self::getDataById($id));
        return $settings;
    }

    /**
     *
     * @param int $key key in database
     * @return Settings
     */
    public static function getInstanceByKey($key) {

        $settings = new Settings();
        $settings->setVariables(self::getDataByKey($key));
        return $settings;
    }

    /**
     * Primary key
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     *
     * @return string
     */
    public function getKey() {
        return $this->key;
    }

    /**
     *
     * @return string
     */
    public function getValue() {
        return $this->value;
    }

    /**
     *
     * @param string $key
     */
    public function setKey($key) {
        $this->key = $key;
    }

    /**
     *
     * @param string $key
     */
    public function setValue($value) {
        $this->value = $value;
    }

    /**
     * saves new instance or updates existing one.
     */
    public function save() {

        $data = array('key' => $this->key, 'value' => $this->value);

        if (isset($this->id) && !is_null($this->id)) {
            dibi::update(self::$tableName, $data)->where('settings_id=%i', $this->id)->execute();
        } else {
            dibi::insert(self::$tableName, $data)->execute();
        }
    }

    /**
     * deletes record from database
     */
    public function remove() {

        if (isset($this->id) && !is_null($this->id)) {
            dibi::delete(self::$tableName)->where('settings_id=%i', $this->id)->execute();
        }
    }
}
