<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS 
 * @author 		Stan (diviak@gmail.com)
 * @internal 	project/models/Signups.php
 * @created 	17/08/2010
 */

class Signups extends Object
{
	/**
	 * primary key
	 *
	 * @var int
	 */
	public $signups_id;
	
	/**
	 * @var string
	 */
	public $name;
	
	/**
	 * @var string
	 */
	public $email;
	
	/**
	 * @var string
	 */
	public $type;
	
	/**
	 * @var string
	 */
	public $dtc;
	
	/**
	 * @var string
	 */
	public $dtm;
	
		
	/**
	 * @return int
	 */
	public function getId() 
	{ 
		return $this->signups_id; 
	}
	
	/**
	 * @return bool
	 */
	public function isNew() 
	{ 
		return ($this->signups_id == 0); 
	}

	/**
	 * @param int $signups_id
	 * @return void
	 * @throws Exception
	 */
	public function __construct($signups_id = 0)
	{
		$this->signups_id = (int) $signups_id; 
		if ($this->signups_id) {
			$w = dibi::select('*')->from(TBL_SIGNUPS)->where('signups_id=%i', $signups_id)->execute()->fetch();
			if ($w) {				
				$this->signups_id = $w['signups_id'];
				$this->name = $w['name'];
				$this->email = $w['email'];
				$this->type = $w['type'];
				$this->dtc = $w['dtc'];
				$this->dtm = $w['dtm'];
			} else {
				throw new Exception("Signup with id `$signups_id` doesn't exist!");
			}
		} 
	}
	
	/**
	 * @return int
	 */
	public function save()
	{
		$data = array();
		$data['name'] = $this->name;
		$data['email'] = $this->email;
		$data['type'] = $this->type;
		$data['dtm'] = new DibiDateTime();
		
		if ($this->isNew()) {
			$data['dtc'] = new DibiDateTime();
			$this->signups_id = dibi::insert(TBL_SIGNUPS, $data)->execute(dibi::IDENTIFIER);
		} else {
			dibi::update(TBL_SIGNUPS, $data)->where('signups_id=%i', $this->signups_id)->execute();
		}
		return $this->signups_id;
	}

	/**
	 * @return void
	 */
	public function delete()
	{
		dibi::delete(TBL_SIGNUPS)->where('signups_id=%i', $this->signups_id)->execute();
	}
	
	/**
	 * Returns count of signups
	 *
	 * @param array $where
	 * @return int
	 */
	static public function getCount(array $where = array())
	{
		$result = dibi::select('count(*)')->from(TBL_SIGNUPS);
		foreach ($where as $key => $val) $result->where('%n', $key, ' = %s',$val);
		$count = $result->execute()->fetchSingle();
		return $count;
	}
	
	/**
	 * Returns all signups
	 *
	 * @param int $limit
	 * @param int $offset
	 * @param array $where
	 * @return int
	 */
	static public function getAll($limit = NULL, $offset = NULL, array $where = array())
	{
		$result = dibi::select('*')->from(TBL_SIGNUPS)->orderBy('dtc', dibi::DESC);
		if ($limit !== NULL) $result->limit($limit);
		if ($offset !== NULL) $result->offset($offset);
		foreach ($where as $key => $val) $result->where('%n', $key, ' = %s',$val);
		return $result->execute()->fetchAssoc('signups_id');
	}
	
	/**
	 * Returns result as objects
	 *
	 * @param int $limit
	 * @param int $offset
	 * @param array $where
	 * @return array<MrSiteCode>
	 */
	static public function getAllObjects($limit = NULL, $offset = NULL, array $where = array())
	{
		$objects = array();
		$all = self::getAll($limit, $offset, $where);
		foreach ($all as $key => $val) {
			$objects[$key] = new self($key);		
		}
		return $objects;
	}
	
	
}