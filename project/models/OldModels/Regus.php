<?php

class Regus extends Object
{

    /**
     * @var int 
     */
    protected $id = 0;

    /**
     * @var int 
     */
    protected $customerId = 0;
    
    /**
     * @var string 
     */
    protected $firstName;

    /**
     * @var string 
     */
    protected $lastName;

    /**
     * @var string 
     */
    protected $email;

    /**
     * @var int
     */
    protected $dialCode = 44;
    
    /**
     * @var string
     */
    protected $phone;

    /**
     * @var int 
     */
    protected $send = 0;

    /**
     * @var DateTime 
     */
    protected $dtm;

    /**
     * @var DateTime 
     */
    protected $dtc;

    /**
     * @return bool
     */
    public function isNew()
    {
        return ($this->id == 0);
    }

    public function __construct($customerId, $firstName, $lastName, $email, $phone, $dialCode = 44)
    {
        $this->setCustomerId($customerId);
        $this->setFirstName($firstName);
        $this->setLastName($lastName);
        $this->setEmail($email);
        $this->setPhone($phone);
        $this->setDialCode($dialCode);
    }
    
    public function getFirstName()
    {
        return $this->firstName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function getSend()
    {
        return $this->send;
    }

    public function getDtm()
    {
        return $this->dtm;
    }

    public function getDtc()
    {
        return $this->dtc;
    }

    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function setPhone($phone)
    {
        $this->phone = str_replace(' ', '', $phone);
    }

    public function setSend($send)
    {
        $this->send = $send;
    }

    public function setDtm($dtm)
    {
        $this->dtm = new DateTime($dtm);
    }

    public function setDtc($dtc)
    {
        $this->dtc = new DateTime($dtc);
    }
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }
    
    public function getDialCode()
    {
        return $this->dialCode;
    }

    public function setDialCode($dialCode)
    {
        $this->dialCode = $dialCode;
    }
    
    public function getCustomerId()
    {
        return $this->customerId;
    }

    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }


}
