<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    FeedbacksForm.php 2011-03-21 razvanp@madesimplegroup.com
 */

class DeleteCompanyModel extends FNode
{

    /** 
     * @var array
     */
    public $filter = array();

  	/**
	 * Returns deleted companies  paginator
	 *
	 * @return FPaginator2
	 */
	public function getDeletedCompniesPaginator(DeleteCompanyAdminForm $filter = NULL)
	{
        
        $paginator = new FPaginator2(DeleteCompany::getCount($this->getWhereOptions($filter->getValues())));
	
        $paginator->htmlDisplayEnabled = FALSE;
		$paginator->itemsPerPage = 10;
		return $paginator;
	}

    /**
	 * Returns list of deleted companies
	 *
     * @param FF $filter
	 * @param FPaginator2 $paginator
	 * @return array<DibiRow>
	 */
	public function getDeletedCompanies(DeleteCompanyAdminForm $filter, FPaginator2 $paginator = NULL)
	{
        if ($paginator !== NULL) {
            $deleteCompany = DeleteCompany::getAll($this->getWhereOptions($filter->getValues()), $paginator->getLimit(), $paginator->getOffset());
        } else {
            $deleteCompany = DeleteCompany::getAll($this->getWhereOptions($filter->getValues()));
        }
		return $deleteCompany;
	}

    /**
	 * Returns where options
	 *
	 * @return array
	 */
    public function getWhereOptions($filter){
       $where =array();
       if(isset($filter['deleteCompanyNumber']))  $where['companyNumber'] = $filter['deleteCompanyNumber'];
       //if(isset($filter['deletedCustomerName']))  $where['customerId'] = $filter['deletedCustomerName'];
       //if(isset($filter['deletedCustomerId']))  $where['customerId'] = $filter['deletedCustomerId'];
       return $where;
    }

    /**
	 * @param FForm $filter
	 * @return void
	 */
    public function outputFeedbackToCSV(FeedBackAdminForm $filter){
       $arr = $this->getFeedbacks($filter);
       if(empty($arr)){
            throw new Exception('No data to export !');
       }
        $header = array_keys((array) current($arr));
        array_unshift($arr, $header);
        $filename = sprintf("feedback-%s.csv", date('d-m-Y'));
        Array2Csv::output($arr, $filename);
    }
}