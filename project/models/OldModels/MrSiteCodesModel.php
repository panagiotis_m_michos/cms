<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS 
 * @author 		Stan (diviak@gmail.com)
 * @internal 	project/models/MrSiteCodesModel.php
 * @created 	17/08/2010
 */

class MrSiteCodesModel extends FNode
{
	/* vevericka */
//	const PRODUCTS_FOLDER = 727;
	
	/* live */
	const PRODUCTS_FOLDER = 774;
	
	/** 
     * @var MrSiteCode 
     */
	private $mrSiteCode;
	
	
	/**
	 * @param int $mrSiteCodeId
	 * @return void
	 * @throws Exception
	 */
	public function createMrSiteCode($mrSiteCodeId)
	{
		try {
			$this->mrSiteCode = new MrSiteCode($mrSiteCodeId);
		} catch (Exception $e) {
			throw $e;
		}
	}
	
	/**
	 * Return mr site code
	 *
	 * @return MrSiteCode
	 */
	public function getMrSiteCode()
	{
		return $this->mrSiteCode;
	}
	
	/**
	 * Returns codes paginator
	 *
	 * @return FPaginator2
	 */
	public function getMrSiteCodesPaginator()
	{
		$paginator = new FPaginator2(MrSiteCode::getCount());
		$paginator->htmlDisplayEnabled = FALSE;
		return $paginator;
	}
	
	
	/**
	 * Returns list of codes
	 *
	 * @param FPaginator2 $paginator
	 * @return array<DibiRow>
	 */
	public function getMrSiteCodes(FPaginator2 $paginator)
	{
		$codes = MrSiteCode::getAllObjects($paginator->getLimit(), $paginator->getOffset());
		return $codes;
	}
	
	/**
	 * Returns mr site code form
	 *
	 * @param array $callback
	 * @return FForm
	 */
	public function getComponentMrSiteCodeAdminForm(array $callback)
	{
		$form = new FForm('mrSiteCode');
		
		$form->addFieldset('Data');
		$form->addSelect('productId', 'Product *', MrSiteCode::getAllProducts())
			->addRule(FForm::Required, 'Please provide product')
			->setFirstOption('--- Select ---');
		$form->addText('code', 'Code *')->size(40)
			->addRule(FForm::Required, 'Please provide code');
			
		$form->addFieldset('Action');
		$form->addSubmit('submit', 'Submit')->class('btn');
		
		$form->setInitValues((array) $this->mrSiteCode);
		$form->onValid = $callback;
		$form->start();
		return $form;
	}
	
	
	/**
	 * @param FForm $form
	 * @return int
	 */
	public function processMrSiteCodeForm(FForm $form)
	{
		try {
			$data = $form->getValues();
			$mrSiteCode = ($this->mrSiteCode === NULL) ? new MrSiteCode() : $this->mrSiteCode;
			$mrSiteCode->productId = $data['productId'];
			$mrSiteCode->code = $data['code'];
			$mrSiteCodeId = $mrSiteCode->save();
			$form->clean();
			return $mrSiteCodeId;
		} catch (Exception $e) {
			throw $e;
		}
	}
}