<?php

/**
 * cms
 *
 * @license    GPL
 * @category   Project
 * @package    cms
 * @author     Razvan Preda <razvanp@madesimplegroup.com>
 * @version    DirectTokenPayment.php 04-Sep-2012 
 */

class TokenLogs extends Object
{
	
    const TOKEN_STATUS_INCOLPLETE = 'INCOMPLETE';
    const TOKEN_STATUS_ERROR = 'ERROR';
    const TOKEN_STATUS_COMPLETE = 'COMPLETE';
    const TOKEN_STATUS_REMOVED = 'REMOVED';
    const TOKEN_STATUS_REMOVED_ERROR = 'REMOVED_ERROR';
    
    /** @var array */
	static public $errorStatusTypeId = Array(
		self::TOKEN_STATUS_INCOLPLETE => 'Incomplete',
		self::TOKEN_STATUS_ERROR => 'Error',
		self::TOKEN_STATUS_COMPLETE => 'Complete',
		self::TOKEN_STATUS_REMOVED => 'Removed',
		self::TOKEN_STATUS_REMOVED_ERROR => 'Removed Error',
	);
    
    /**
	 * primary key
	 *
	 * @var int
	 */
	public $tokenStatusId;
	
	/**
	 * @var string
	 */
	public $tokenStatus;
	
	/**
	 * @var string
	 */
	public $token;
	
	/**
	 * @var string
	 */
	public $cardNumber;
    
    /**
	 * @var string
	 */
	public $name;
    
    /**
	 * @var string
	 */
	public $address;
    
    /**
     * @var type 
     */
    public $email;

	/**
	 * @var string
	 */
	public $dtc;
	
	/**
	 * @var string
	 */
	public $dtm;
	
		
	/**
	 * @return int
	 */
	public function getId() 
	{ 
		return $this->tokenStatusId; 
	}
	
	/**
	 * @return bool
	 */
	public function isNew() 
	{ 
		return ($this->tokenStatusId == 0); 
	}

	/**
	 * @param int $tokenStatusId
	 * @return void
	 * @throws Exception
	 */
	public function __construct($tokenStatusId = 0)
	{
		$this->tokenStatusId = (int) $tokenStatusId; 
		if ($this->tokenStatusId) {
			$w = dibi::select('*')->from(TBL_TOKEN_LOGS)->where('tokenStatusId=%i', $tokenStatusId)->execute()->fetch();
			if ($w) {				
				$this->tokenStatusId    = $w['tokenStatusId'];
				$this->tokenStatus      = $w['tokenStatus'];
				$this->token            = $w['token'];
				$this->cardNumber       = $w['cardNumber'];
                $this->name             = $w['name'];
                $this->address          = $w['address'];
                $this->email            = $w['email'];
				$this->dtc              = $w['dtc'];
				$this->dtm              = $w['dtm'];
			} else {
				throw new Exception("TokenLog with id `$tokenStatusId` doesn't exist!");
			}
		} 
	}
	
	/**
	 * @return int
	 */
	public function save()
	{
		$data = array();
		$data['tokenStatus']   = $this->tokenStatus;
		$data['token']         = $this->token;
		$data['cardNumber']    = $this->cardNumber;
        $data['name']          = $this->name;
        $data['address']       = $this->address;
        $data['email']         = $this->email;
		$data['dtm']           = new DibiDateTime();
		
		if ($this->isNew()) {
			$data['dtc'] = new DibiDateTime();
			$this->tokenStatusId = dibi::insert(TBL_TOKEN_LOGS, $data)->execute(dibi::IDENTIFIER);
            
		} else {
			dibi::update(TBL_TOKEN_LOGS, $data)->where('tokenStatusId=%i', $this->tokenStatusId)->execute();
		}
		return $this->tokenStatusId;
	}

	/**
	 * @return void
	 */
	public function delete()
	{
		dibi::delete(TBL_TOKEN_LOGS)->where('tokenStatusId=%i', $this->tokenStatusId)->execute();
	}
	
	/**
	 * Returns count of signups
	 *
	 * @param array $where
	 * @return int
	 */
	static public function getCount(array $where = array())
	{
		$result = dibi::select('count(*)')->from(TBL_TOKEN_LOGS);
		foreach ($where as $key => $val) $result->where('%n', $key, ' = %s',$val);
		$count = $result->execute()->fetchSingle();
		return $count;
	}
	
	/**
	 * Returns all signups
	 *
	 * @param int $limit
	 * @param int $offset
	 * @param array $where
	 * @return int
	 */
	static public function getAll($limit = NULL, $offset = NULL, array $where = array())
	{
		$result = dibi::select('*')->from(TBL_TOKEN_LOGS)->orderBy('dtc', dibi::DESC);
		if ($limit !== NULL) $result->limit($limit);
		if ($offset !== NULL) $result->offset($offset);
		foreach ($where as $key => $val) $result->where('%n', $key, ' = %s',$val);
		return $result->execute()->fetchAssoc('tokenStatusId');
	}
	
	/**
	 * Returns result as objects
	 *
	 * @param int $limit
	 * @param int $offset
	 * @param array $where
	 * @return array<MrSiteCode>
	 */
	static public function getAllObjects($limit = NULL, $offset = NULL, array $where = array())
	{
		$objects = array();
		$all = self::getAll($limit, $offset, $where);
		foreach ($all as $key => $val) {
			$objects[$key] = new self($key);		
		}
		return $objects;
	}
    
 
	
}