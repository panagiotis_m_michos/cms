<?php
/**
 * @package     Companies Made Simple
 * @subpackage     CMS 
 * @author         Stan (diviak@gmail.com)
 * @internal     project/models/AffiliateCustomersImportModel.php
 * @created     19/04/2010
 */

class AffiliateCustomersImportModel extends FNode
{
    /**
     * Rerurns import form
     *
     * @param AffiliateCustomersImportAdminControler $controler
     * @return FForm
     */
    public function getComponentImportForm(AffiliateCustomersImportAdminControler $controler)
    {
        $form = new FForm('affiliateCustomersImport');
        $form->setRenderClass('Default2Render');
        
        $form->addFieldset('Data');
        $form->addSelect('statusId', 'Status: *', Customer::$statuses)->style('width: 150px;')
            ->setFirstOption('--- Select ---')
            ->setValue(Customer::STATUS_VALIDATED)
            ->addRule(FForm::Required, 'Please provide status');
        $form->addSelect('roleId', 'Role: *', Customer::$roles)->style('width: 150px;')
            ->setFirstOption('--- Select ---')
            ->setValue(Customer::ROLE_WHOLESALE)
            ->addRule(FForm::Required, 'Please provide role');
        $form->addSelect('affiliateId', 'Affiliate: *', Affiliate::getDropdown())->style('width: 150px;')
            ->setFirstOption('--- Select ---')
            ->addRule(FForm::Required, 'Please provide affiliate');
    
        $form->addFieldset('CSV File');
        $form->addFile('file', 'File: *');
        
        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Submit')->class('btn')->style('width: 200px; height: 30px;');
        $form->onValid = array($controler, 'Form_affiliateCustomersImportFormSubmitted');
        $form->start();
        return $form;
    }
    
    
    /**
     * Provides proccess import data
     *
     * @param array $data
     * @return void
     * @throws Exception
     */
    public function processImportData(array $data)
    {
        try {
            $customers = $this->saveCustomersFromFile($data['file']['tmp_name'], $data['statusId'], $data['roleId'], $data['affiliateId']);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
    
    
    /**
     * Enter description here...
     *
     * @param string $file
     * @param int $statusId
     * @param int $roleId
     * @param int $affiliateId
     * @return array<Customer>
     * @throws Exception
     */
    private function saveCustomersFromFile($file, $statusId, $roleId, $affiliateId)
    {
        $poc = 0;
        $customers = array();
        $handle = fopen($file, 'r');
        while (($row = fgetcsv($handle, 1000, ",")) !== FALSE) {
            $poc++;
            
            // skip header
            if ($poc === 1) { continue; 
            }
            
            try {
                // existing customer
                $customer = Customer::getCustomerByEmail($row[0]);
                $customer->roleId = $roleId;
                $customer->affiliateId = $affiliateId;
                $customer->save();

            } catch (Exception $e) {
                try {
                    // new customer
                    $customer = new Customer();
                    $customer->statusId = $statusId;
                    $customer->roleId = $roleId;
                    $customer->affiliateId = $affiliateId;
                    $customer->email = $row[0];
                    $customer->titleId = $row[1];
                    $customer->firstName = $row[2];
                    $customer->lastName = $row[3];
                    $customer->address1 = $row[4];
                    $customer->address2 = $row[5];
                    $customer->address3 = $row[6];
                    $customer->city = $row[7];
                    $customer->county = $row[8];
                    $customer->postcode = $row[9];
                    $customer->countryId = $row[10];
                    $customer->phone = $row[11];
                    $customer->password = FTools::generPwd(8);
                    $customer->save();
                
                                        $affiliateEmailer = Registry::$emailerFactory->get(EmailerFactory::AFFILIATE);
                                        $affiliateEmailer->customerImportEmail($customer);
                } catch (Exception $e) {
                    throw new Exception($e->getMessage());
                }
            }
            
            $customers[] = $customer;
        }
        return $customers;
    }
}