<?php

use CHFiling\Exceptions\EmptyResponseException;
use Tracy\Debugger;
use Entities\CompanyHouse\FormSubmission;

class PollingModel
{
    const MAIL_FORWARDING = 443;

    /**
     * Process form submission id
     *
     * sends emails to custmoers - company incorporation and company rejection
     * plus sends data to Barclays
     *
     * @param array $ids
     * @return void
     * @throws Exception
     */
    public static function processFormSubmissionStatus(array $ids = NULL)
    {
        if ($ids == NULL) {
            $ids = dibi::select('form_submission_id')
                ->from('ch_form_submission')
                ->where('response IN (%s)', [FormSubmission::RESPONSE_PENDING, FormSubmission::RESPONSE_INTERNAL_FAILURE])
                ->where('date_cancelled IS NULL')
                ->where('toResend = 0')
                ->fetchPairs();
        }

        foreach ($ids as $submissionId) {
            try {
                $return = CHFiling::getFormSubmissionStatus($submissionId);

                /* only CompanyIncorporation, AnnualReturn, ChangeOfName, ChangeAccountingReferenceDate */
                if ($return !== FALSE && ($return['type'] == 'CompanyIncorporation' || $return['type'] == 'AnnualReturn' || $return['type'] == 'ChangeOfName' || $return['type'] == 'ChangeAccountingReferenceDate')
                ) {

                    $customer = new Customer($return['customerId']);
                    $replace = self::createReplaceEmailData($customer, $return);

                    // Company Incorporation
                    if ($return['type'] == 'CompanyIncorporation') {
                        // Reserved Companies Incorporation
                        $reserv = self::checkReserve($return['companyId']);
                        if ($reserv == Package::PACKAGE_RESERVE_COMPANY_NAME) {
                            self::processReserveCompanyIncorporation($customer, $return, $replace);
                        } else {
                            self::processCompanyIncorporationEmail($customer, $return, $replace);
                        }

                        // Annual Return
                    } elseif ($return['type'] == 'AnnualReturn') {
                        self::processAnnualReturn($customer, $return, $replace);

                        // Change Name
                    } elseif ($return['type'] == 'ChangeOfName') {
                        self::processChangeCompanyName($customer, $return, $replace);

                        // Change Accounting Reference Date
                    } elseif ($return['type'] == 'ChangeAccountingReferenceDate') {
                        self::processChangeARD($customer, $return, $replace);
                    }
                }
            } catch (EmptyResponseException $e) {
                Debugger::log($e, Debugger::INFO);
            } catch (Exception $e) {
                // introduced as replacement for sending errors through errorTestingEmailer
                // remove node ERROR_EMAIL = 1127 and sendPollingErrorMessage method in ErrorTestingEmailer
                Debugger::log($e, Debugger::ERROR);
                $errorTestingEmailer = Registry::$emailerFactory->get(EmailerFactory::ERROR_TESTING);
                $errorTestingEmailer->sendPollingErrorMessage($e->__toString());
                continue;
            }
        }

        self::sendBarclaysCompanies();
        self::sendHSBCCompanies();

        $businessBankingEmailer = Registry::$emailerFactory->get(EmailerFactory::BUSINESS_BANKING);
        $cardOne = new CardOne($businessBankingEmailer);
        $companyIds = $cardOne->getCompaniesIds();
        $cardOne->sendCardOneCompanies($companyIds);

        self::sendEreminders();
    }

    /**
     * @param Customer $customer
     * @param array $return
     * @return array
     */
    private static function createReplaceEmailData($customer, $return)
    {
        $replace = array();
        $replace['[FIRSTNAME]'] = isset($customer->firstName) ? $customer->firstName : NULL;
        $replace['[COMPANY_NAME]'] = isset($return['companyName']) ? $return['companyName'] : NULL;
        $replace['[COMPANY_ID]'] = isset($return['companyId']) ? $return['companyId'] : NULL;
        $replace['[COMPANY_NUMBER]'] = isset($return['companyNumber']) ? $return['companyNumber'] : 0;
        $replace['[COMPANY_INFO_URL]'] = isset($return['companyId']) ? 'https://www.companiesmadesimple.com' .
            FApplication::$router->frontLink(CUSummaryControler::SUMMARY_PAGE, array('company_id' => $return['companyId'])) : NULL;
        if (isset($return['date'])) {
            $replace['[DATE]'] = $return['date'];
        }
        if (isset($return['description'])) {
            $replace['[REJECT_TEXT]'] = $return['description'];
            if (preg_match('#credit limit#', $replace['[REJECT_TEXT]'])) {
                $replace['[REJECT_TEXT]'] = 'There has been an unexpected error sending your request to Companies House. We apologise for the inconvenience. Please resubmit your application and it should be formed in 3 working hours';
            }
        }
        return $replace;
    }

    /**
     * Will process company incorporation - send emails
     * @param Customer $customer
     * @param array $return - individual form submission
     * @param array $replace
     * @throws Exception
     * @return void
     */
    private static function processCompanyIncorporationEmail(Customer $customer, array $return, array $replace)
    {
        $companyEmailer = Registry::$emailerFactory->get(EmailerFactory::COMPANY);
        $businessBankingEmailer = Registry::$emailerFactory->get(EmailerFactory::BUSINESS_BANKING);

        switch ($return['status']) {
            case 'ACCEPT':
                if (!isset($return['attachmentPath']) || empty($return['attachmentPath']) || !is_file($return['attachmentPath'])) {
                    throw new Exception("We don't have a valid incorporation certificate file for company: {$return['companyName']}, company id= {$return['companyId']} ");
                }

                $company = Company::getCompany($replace['[COMPANY_ID]']);
                //LP Addvert mail check
                $LPAdd = self::checkLondonPresence($company->getCompanyId());
                // don't add attachment for reserve package
                $reserv = self::checkReserve($company->getCompanyId());

                $companyEmailer->sendCompanyIncorporationAccepted($customer, $company, $replace, $return, $LPAdd, $reserv);

                break;
            case 'REJECT':
                $companyEmailer->sendCompanyIncorporationRejected($customer, $replace);
                break;
        }
    }

    /**
     * Will process reserve company incorporation - send emails
     *
     * @param Customer $customer
     * @param array $return - individual form submission
     * @param array $replace
     * @return void
     */
    private static function processReserveCompanyIncorporation(Customer $customer, array $return, array $replace)
    {
        $companyEmailer = Registry::$emailerFactory->get(EmailerFactory::COMPANY);
        switch ($return['status']) {
            case 'ACCEPT':
                $companyEmailer->sendCompanyIncorporationReservedAccepted($customer, $replace);
                break;
            case 'REJECT':
                $companyEmailer->sendCompanyIncorporationReservedRejected($customer, $replace);
                break;
        }
    }

    /**
     * Will process annual return - emails
     *
     * @param Customer $customer
     * @param array $return
     * @param array $replace
     */
    private static function processAnnualReturn(Customer $customer, array $return, array $replace)
    {
        // service - diff emails and dealling
        if (Company::getCompany($return['companyId'])->getAnnualReturnId() == AnnualReturn::ANNUAL_RETURN_SERVICE_PRODUCT) {
            AnnualReturnServiceModel::processCHRespond($customer, $return, $replace);
        } else {
            AnnualReturnModel::processCHRespond($customer, $return, $replace);
        }
    }

    /**
     * Will process change name - send emails
     *
     * @param Customer $customer
     * @param array $return - individual form submission
     * @param array $replace
     * @return void
     */
    private static function processChangeCompanyName(Customer $customer, array $return, array $replace)
    {
        $companyNameEmailer = Registry::$emailerFactory->get(EmailerFactory::COMPANY_NAME);
        $replace['[NAME_CHANGE_LINK]'] = 'https://www.companiesmadesimple.com' . FApplication::$router->frontLink(CUChangeNameControler::CHANGE_NAME_PAGE, array('company_id' => $return['companyId']));
        switch ($return['status']) {
            case 'ACCEPT':
                $companyNameEmailer->nameChangeAcceptEmail($customer, $replace, $return);
                break;
            case 'REJECT':
                $companyNameEmailer->nameChangeRejectEmail($customer, $replace);
                break;
        }
    }

    /**
     * Will process change Accounting Reference Date - send emails
     * @param Customer $customer
     * @param array $return
     * @param array $replace
     */
    private static function processChangeARD(Customer $customer, array $return, array $replace)
    {
        $ardEmailer = Registry::$emailerFactory->get(EmailerFactory::ARD);
        switch ($return['status']) {
            case 'ACCEPT':
                $ardEmailer->ardChangeAcceptEmail($customer, $replace);
                break;
            case 'REJECT':
                $ardEmailer->ardChangeRejectEmail($customer, $replace);
                break;
        }
    }

    ////////////////////////////////////////// Send emails For Banks//////////////////////////////////////////////
    private static function sendBarclaysCompanies()
    {
        $businessBankingEmailer = Registry::$emailerFactory->get(EmailerFactory::BUSINESS_BANKING);
        $ids = Barclays::getCompaniesIds();
        foreach ($ids as $id) {
            try {
                if (CompanyCustomer::hasCustomerCompanyFilled($id) === TRUE) {
                    $return = Barclays::sendCompany($id, FALSE, FALSE, TRUE);
                }
            } catch (Exception $e) {
                $businessBankingEmailer->barclaysErrorEmail($e, $id);
            }
        }
    }

    private static function sendHSBCCompanies()
    {
        $businessBankingEmailer = Registry::$emailerFactory->get(EmailerFactory::BUSINESS_BANKING);
        $ids = HSBC::getCompaniesIds();
        foreach ($ids as $id) {
            try {
                if (CompanyCustomer::hasCustomerCompanyFilled($id) === TRUE) {
                    $return = HSBC::sendCompany($id);
                }
            } catch (Exception $e) {
                $businessBankingEmailer->hsbcErrorEmail($e, $id);
            }
        }
    }

    private static function sendEreminders()
    {
        $ids = Company::getEreminderCompanyIds();
        foreach ($ids as $id) {
            try {
                $company = Company::getCompany($id);
                $customer = new Customer($company->getCustomerId());
                $reminder = SetEReminders::getNewReminder(array($customer->email), $company);
                $reminder->sendRequest();
                $company->setEreminderId(2);
            } catch (Exception $e) {
                $ereminderEmailer = Registry::$emailerFactory->get(EmailerFactory::EREMINDER);
                $ereminderEmailer->sendEreminderErrorEmail($e, $id);
            }
        }
    }

    /**
     * @param int $companyId
     * @return int
     */
    private static function checkReserve($companyId)
    {
        $companyReserve = Company::getCompany($companyId);
        $reserv = dibi::select('productId')
            ->from('cms2_orders_items')
            ->where('productId=%s', Package::PACKAGE_RESERVE_COMPANY_NAME)
            ->where('orderId=%s', $companyReserve->getOrderId())
            // get productId or null
            ->fetchSingle();
        return $reserv;
    }

    /**
     * @param int $companyId
     * @return int
     */
    private static function checkLondonPresence($companyId)
    {
        $company = Company::getCompany($companyId);
        $result = dibi::select('productId')
            ->from('cms2_orders_items')
            ->where('productId IN (%i)', array(Package::PACKAGE_ULTIMATE, self::MAIL_FORWARDING))
            ->and('orderId=%s', $company->getOrderId())
            // get productId or null
            ->fetchSingle();
        return $result;
    }

    ////////////////////////////////////////// Testing//////////////////////////////////////////////

    /**
     * @param array $ids
     * @param string $response
     * @return string
     */
    private static function showReportStats($ids, $response)
    {
        $xcount = count($ids);
        $ycount = count($response);
        $x = json_encode($ids);
        $y = serialize($response);
        $html = "Form submission ids : {$x} <br /> Nr of form submission: {$xcount} <br /> Response: {$y} <br /> Nr of response: {$ycount}";
        return $html;
    }

}
