<?php

class Transaction extends Object
{
    // statuses
    const STATUS_SUCCEEDED = 'SUCCEEDED';
    const STATUS_FAILED = 'FAILED';
    
    /**
     * @var array 
     */
    public static $statuses = array(
        self::STATUS_SUCCEEDED => 'Succeded',
        self::STATUS_FAILED => 'Failed',
    );
    
    // types
    const TYPE_PAYPAL = 1;
    const TYPE_GOOGLE = 2;
    const TYPE_WORLDPAY = 3;
    const TYPE_CREDIT = 4;
    const TYPE_FREE = 5;
    const TYPE_SAGEPAY = 6;
    const TYPE_PAYPAL_REFUND = 7;
    const TYPE_GOOGLE_REFUND = 8;
    const TYPE_SAGEPAY_REFUND = 9;
    const ON_ACCOUNT = 10;

    /** @var array */
    public static $types = Array(
        self::TYPE_PAYPAL => 'Paypal',
        self::TYPE_GOOGLE => 'Google Checkout',
        self::TYPE_WORLDPAY => 'Worldpay',
        self::TYPE_CREDIT => 'Credit',
        self::TYPE_FREE => 'Free',
        self::TYPE_SAGEPAY => 'SagePay',
        self::TYPE_PAYPAL_REFUND => 'Paypal Refund',
        self::TYPE_GOOGLE_REFUND => 'Google Refund',
        self::TYPE_SAGEPAY_REFUND => 'Sagepay Refund',
        self::ON_ACCOUNT => 'On Account'
    );
    
    /** @var int */
    public $transactionId;

    /** @var int */
    public $customerId;
    
    /**
     * @var string 
     */
    public $statusId = self::STATUS_SUCCEEDED;

    /** @var int */
    public $typeId;

    /** @var string */
    public $type;

    /** @var string */
    public $cardHolder;

    /** @var string */
    public $cardNumber;

    /** @var int */
    public $orderId;

    /** @var string */
    public $orderCode;

    /** @var string */
    public $error;

    /** @var string */
    public $dtc;

    /** @var string */
    public $details;

    /** @var string */
    public $request;

    /** @var string */
    public $vendorTXCode;

    /** @var string */
    public $vpsAuthCode;

    /** @var int */
    public $tokenId;

    /**
     * @return int
     */
    public function getId()
    {
        return( $this->transactionId );
    }

    /**
     * @return bool
     */
    public function exists()
    {
        return( $this->transactionId > 0 );
    }

    /**
     * @return bool
     */
    public function isNew()
    {
        return( $this->transactionId == 0 );
    }

    /**
     * @param int $transactionId
     * @return void
     */
    public function __construct($transactionId = 0)
    {
        $this->transactionId = (int) $transactionId;
        if ($this->transactionId) {
            $w = FApplication::$db->select('*')->from(TBL_TRANSACTIONS)->where('transactionId=%i', $this->transactionId)->execute()->fetch();
            if ($w) {
                $this->transactionId = $w['transactionId'];
                $this->customerId = $w['customerId'];

                $this->statusId = $w['statusId'];
                $this->typeId = $w['typeId'];
                $this->type = isSet(self::$types[$w['typeId']]) ? self::$types[$w['typeId']] : NULL;

                $this->cardHolder = $w['cardHolder'];
                $this->cardNumber = $w['cardNumber'];
                $this->tokenId = $w['tokenId'];
                $this->orderId = $w['orderId'];
                $this->orderCode = $w['orderCode'];
                $this->error = $w['error'];
                $this->dtc = $w['dtc'];
                $this->details = $w['details'];
                $this->request = $w['request'];
                $this->vendorTXCode = $w['vendorTXCode'];
                $this->vpsAuthCode = $w['vpsAuthCode'];
            } else {
                $this->transactionId = -1 * $this->transactionId;
            }
        }
    }

    /**
     * Provide saving transaction to database
     * @return int $id
     */
    public function save()
    {
        $action = $this->transactionId ? 'update' : 'insert';

        // data
        $data = array();
        $data['customerId'] = $this->customerId;
        $data['statusId'] = $this->statusId;
        $data['typeId'] = $this->typeId;
        $data['cardHolder'] = $this->cardHolder;
        $data['cardNumber'] = $this->cardNumber;
        $data['tokenId'] = $this->tokenId;
        $data['orderId'] = $this->orderId;
        $data['orderCode'] = $this->orderCode;
        $data['error'] = $this->error;
        $data['details'] = $this->details;
        $data['request'] = $this->request;
        $data['vendorTXCode'] = $this->vendorTXCode;
        $data['vpsAuthCode'] = $this->vpsAuthCode;

        // insert
        if ($action == 'insert') {
            $data['dtc'] = new DibiDateTime();
            $this->transactionId = FApplication::$db->insert(TBL_TRANSACTIONS, $data)->execute(dibi::IDENTIFIER);
        // update
        } else {
            FApplication::$db->update(TBL_TRANSACTIONS, $values)->where('transactionId=%i', $this->transactionId)->execute();
        }

        return $this->transactionId;
    }

    /**
     * @return void
     */
    public function delete()
    {
        if ($this->exists()) {
            FApplication::$db->delete(TBL_TRANSACTIONS)->where('transactionId=%i', $this->transactionId)->execute();
        }
    }

    /**
     * Returns transactions by params
     * @param int $customerId
     * @param int $orderId
     * @param int $limit
     * @param int $offset
     * @return array $transactions
     */
    static public function getTransaction($customerId = NULL, $orderId = NULL, $limit = NULL, $offset = NULL)
    {
        $result = FApplication::$db->select('transactionId')->from(TBL_TRANSACTIONS)->orderBy('dtc', dibi::DESC);

        // customerId
        if ($customerId !== NULL)
            $result->where('customerId=%i', $customerId);

        // orderId
        if ($orderId !== NULL)
            $result->where('orderId=%i', $orderId);

        // limit
        if ($limit !== NULL)
            $result->limit($limit);

        // offset
        if ($offset !== NULL)
            $result->offset($offset);

        $ids = $result->execute()->fetchPairs();

        // transactions
        $transactions = array();
        foreach ($ids as $key => $val) {
            $transactions[$val] = new self($val);
        }
        return $transactions;
    }
    
    /**
     * @return string 
     */
    public function getStatus()
    {
        $id = $this->statusId;
        return (isset(self::$statuses[$id])) ? self::$statuses[$id]: NULL;
    }
    
    /**
     * @return Customer 
     */
    public function getCustomer()
    {
        try {
            $customer = new Customer($this->customerId);
            return $customer;
        } catch (Exception $e) {
        }
    }

    /**
     * @return boolean 
     */
    public function isFailed()
    {
        return ($this->statusId === self::STATUS_FAILED) ? TRUE : FALSE;
    }
}
