<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    CosecModel.php 2010-07-27 divak@gmail.com
 */


class CosecModel extends FNode
{
    /**
     * @var string
     */
    private $homeSlideShow;
    
    /**
     * @var int
     */
    private $templateRightImageId;
    
    /**
     * Provides adding custom data
     * 
     * @return void 
     */
    public function addCustomData()
    {
        $this->homeSlideShow = $this->getProperty('homeSlideShow');
        $this->templateRightImageId = $this->getProperty('templateRightImageId');
        parent::addCustomData();
    }
    
    /**
     * Provides saving custom data
     *
     * @param string $action
     * @return void
     */
    public function saveCustomData($action)
    {
        $this->saveProperty('homeSlideShow', $this->homeSlideShow);
        $this->saveProperty('templateRightImageId', $this->templateRightImageId);
        parent::saveCustomData($action);
    }
    
    
    /**
     * Returns top menu
     *
     * @return array
     */
    public function getTopMenu()
    {
        $menu = FMenu::get(CosecControler::COSEC_FOLDER);
        return $menu;
    }
    
    /**
     * Return home video
     *
     * @return string
     */
    public function getHomeSlideShow()
    {
        return $this->homeSlideShow;
    }

    /**
     * Return image id
     *
     * @return string
     */
    public function getTemplateRightImageId()
    {
        return $this->templateRightImageId;
    }

    /**
     * @return FImage
     */
    public function getTemplateRightImage()
    {
        try {
            $image = new FImage($this->templateRightImageId);
        } catch (Exception $e) {
            $image = new FImage();
        }
        return $image;
    }

    /**
     * Set image id
     *
     * @return void
     */
    public function setTemplateRightImageId($templateRightImageId)
    {
        $this->templateRightImageId = $templateRightImageId;
    }

    /**
     * Set home video
     *
     * @return void
     */
    public function setHomeSlideShow($homeSlideShow)
    {
        $this->homeSlideShow = $homeSlideShow;
    }
    
}