<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    LoggerAdminNode.php 2011-03-05 divak@gmail.com
 * @version        2011-11-04 <tomasj@madesimplegroup.com>
 */

class LoggerAdminNode extends FNode
{
    /**
     * @return ArrayObject
     */
    public function getExceptionFiles()
 {
        static $files;
        if ($files === NULL) {
            $files = new ArrayObject();
            /* @var $file SplFileInfo */
            foreach (Finder::findFiles('*.html')->exclude('index.html')->in(TEMP_LOGS_DIR . '/php/') as $key => $file) {
                $item = new ArrayObject();
                $item->offsetSet('description', $this->getExceptionDescription($file->getPathname()));
                $item->offsetSet('fileName', $file->getFileName());
                $item->offsetSet('pathName', $file->getPathname());
                $item->offsetSet('created', date('M d, Y @ G:i, D', $file->getCTime()));
                $files->append($item);
            }

            // --- sort files by date ---
            $files->uasort(array($this, 'Callback_sortFiles'));
        }
        return $files;
 }

    /**
     * return exception title and description
     * 
     * @param string $path
     * @return string
     */
    public function getExceptionDescription($path)
 {
        $matches = array();
        $content = file_get_contents($path);

        preg_match_all('/netteBluescreenError.+?<h1>(.+?)<\/h1>.*?<p>(.+?)<\/p>/s', $content, $matches);
        $title = isset($matches[1][0]) ? $matches[1][0]  : 'Title Not found';
        $description = isset($matches[2][0]) ? $matches[2][0]  : 'Description Not found';

        $el = Html::el();
        $el->create('div')->style('font-weight: bold;')->setText($title);
        $el->create('div')->setText($description);
        return $el;
    }


    /**
     * @param ArrayObject $a
     * @param ArrayObject $b
     * @return int
     */
    public function Callback_sortFiles(ArrayObject $a, ArrayObject $b)
    {
        if ($a['created'] == $b['created']) {
            return 0;
        }
        return ($a['created'] > $b['created']) ? -1 : 1;
    }

    /**
     * @param int $fileId
     * @return string
     * @throws Exception
     */
    public function getExceptionContent($fileId)
    {
        $files = $this->getExceptionFiles();
        if (!isset($files[$fileId])) { throw new Exception("Exception file with id `$fileId` doesn't exist"); 
        }
        $content = file_get_contents($files[$fileId]['pathName']);
        return $content;
    }


    /**
     * @param int $fileId
     */
    public function deleteExceptionFile($fileId)
    {
        $files = $this->getExceptionFiles();
        if (!isset($files[$fileId])) { throw new Exception("Exception file with id `$fileId` doesn't exist"); 
        }
        @unlink($files[$fileId]['pathName']);
    }

    /**
     * @return boolean
     */
    public function hasExceptions()
    {
        $files = $this->getExceptionFiles();
        return (bool) $files->count();
    }

    /**
     * @return void
     */
    public function deleteExceptions()
    {
        $files = $this->getExceptionFiles();
        foreach ($files as $file) {
            @unlink($file['pathName']);
        }
    }

    /**
     * @return boolean
     */
    public function hasMailingFile()
    {
        return is_file(TEMP_LOGS_DIR . '/php/php_error.log.monitor');
    }

    /**
     * @return void
     */
    public function deleteMailingFile()
    {
        if ($this->hasMailingFile()) {
            @unlink(TEMP_LOGS_DIR . '/php/php_error.log.monitor');
        }
    }

    /**
     * @return boolean
     */
    public function hasErrorLogFile()
    {
        return is_file(TEMP_LOGS_DIR . '/php/php_error.log');
    }

    /**
     * @return string
     */
    public function getErrorLogContent()
    {
        $content = NULL;
        if ($this->hasErrorLogFile()) {
            // use only 1000 last lines
            $file = popen('tail -n 1000 ' . escapeshellarg(TEMP_LOGS_DIR . '/php/php_error.log'), 'r');
            while (($line = fgets($file)) !== FALSE) {
                $content .= $line;
            }
            pclose($file);
            return mb_convert_encoding($content, 'UTF-8', 'UTF-8');
        }
        return $content;
    }


    /**
     * @return void
     */
    public function deleteErroLogFile()
    {
        if ($this->hasErrorLogFile()) {
            @unlink(TEMP_LOGS_DIR . '/php/php_error.log');
        }
    }


    /**
     * @return array
     */
    public function getOptions()
    {
        $options = array();
        if ($this->hasErrorLogFile()) { $options['errorLog'] =  'Error Log'; 
        }
        if ($this->hasMailingFile()) { $options['mailingFile'] =  'Maling File'; 
        }
        if ($this->hasExceptions()) { $options['exceptions'] =  'Exceptions'; 
        }
        return $options;
    }

}