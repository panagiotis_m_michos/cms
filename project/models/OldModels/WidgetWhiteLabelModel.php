<?php

class WidgetWhiteLabelModel extends FNode
{
    /**
     * @var Affiliate
     */
    private $affiliate;
    
    /**
     * @var string
     */
    private $companyName;
    
    /**
     * @var Basket
     */
    private $basket;

    /**
     * @param string $hash
     * @throws Exception
     */
    public function startup($hash)
    {
        $affiliateId = Affiliate::getAffiliateByHash($hash);
        if ($affiliateId === FALSE) {
            throw new Exception("Affiliate doesn't exist");
        }

        $this->affiliate = new Affiliate($affiliateId);
        if (!$this->affiliate->hasService(Affiliate::SERVICE_WHITE_LABEL)) {
            throw new Exception("Service doesn't exist for affiliate");
        }

        $this->basket = new Basket();
    }
    
    /**
     * @return Affiliate
     */
    public function getAffiliate()
    {
        return $this->affiliate;
    }
    
    /**
     * @return Basket
     */
    public function getBasket()
    {
        return $this->basket;
    }
    
    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param string $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     * @param array $callback
     * @return FForm
     */
    public function getComponentHomeSearchForm(array $callback)
    {
        $form = new FForm('widgetWhiteLabelSearchForm');

        $form->addFieldset('Search');
        $form->addText('companyName', 'Company name:')
            ->size(40)
            ->addRule(FForm::Required, 'Please provide company name')
            ->addRule([$this, 'Validator_checkCompanyName'], 'Company name has been taken');

        $form->addSubmit('search', 'Search')
            ->class('button');

        $form->onValid = $callback;
        $form->start();

        return $form;
    }

    /**
     * @param array $callback
     * @return FForm
     */
    public function getComponentPackagesForm(array $callback)
    {
        $form = new FForm('widgetWhiteLabelPackages');
        $form->addRadio('packageId', 'Packages', $this->getPackagesRadioOptions())
            ->addRule(FForm::Required, 'Please provide package');

        $form->addSubmit('submit', 'Submit')->style('width: 200px; height: 30px;');
        $form->onValid = $callback;
        $form->start();

        return $form;
    }

    /**
     * @return array
     */
    private function getPackagesRadioOptions()
    {
        $options = [];
        $packages = $this->getAffiliate()->getPackages();
        foreach ($packages as $key => $val) {
            $options[$key] = NULL;
        }

        return $options;
    }

    /**
     * @param FForm $form
     */
    public function processPackagesForm(FForm $form)
    {
        $data = $form->getValues();

        // --- add package ---
        $package = FNode::getProductById($data['packageId']);
        $package->price += AffiliateProduct::getMarkup($data['packageId'], $this->affiliate->affiliateId);
        $package->markUp = AffiliateProduct::getMarkup($data['packageId'], $this->affiliate->affiliateId);
        $package->companyName = $this->getCompanyName();
        $this->getBasket()->add($package);

        $form->clean();
    }
    
    /**
     * @param array $callback
     * @return FForm
     */
    public function getComponentLoginForm(array $callback)
    {
        $form = new FForm('widgetWhiteLabelLogin');

        $form->addFieldset('Login');
        $form->addText('email', 'Email address:')
            ->addRule(FForm::Required, 'Please provide e-mail address!')
            ->addRule('Email', 'Email address is not valid!');
        $form->addPassword('password', 'Password:')
            ->addRule(FForm::Required, 'Please provide password!')
            ->addRule(
                [$this, 'Validator_loginCredentials'],
                [1 => "Email not found!", 2 => "Password doesn't match!", 3 => "Your account haven't been validated!", 4 => "Your account is blocked. Please contact our customer supoort line on 0207 608 5500."]
            );
        $form->addSubmit('login', 'Login')
            ->class('button');

        $form->onValid = $callback;
        $form->start();

        return $form;
    }

    /**
     * @param FForm $form
     */
    public function processLoginForm(FForm $form)
    {
        $data = $form->getValues();
        $customer = Customer::doAuthenticate($data['email'], $data['password']);
        Customer::signIn($customer->getId());
        $form->clean();
    }

    /**
     * @param array $callback
     * @return FForm
     */
    public function getComponentRegistrationForm(array $callback)
    {
        $form = new FForm('widgetWhiteLabelRegistration');

        $form->addFieldset('Registration');
        $form->addText('email', 'Email *')
            ->addRule(FForm::Required, 'Please provide e-mail address!')
            ->addRule(FForm::Email, 'Email is not valid!')
            ->addRule([$this, 'Validator_uniqueLogin'], 'Email address has been taken.');
        $form->addPassword('password', 'Password *')
            ->addRule(FForm::Required, 'Please provide password!');
        $form->addPassword('passwordConf', 'Confirm *')
            ->addRule(FForm::Equal, 'Passwords are not the same!', 'password');
        $form->addSelect('titleId', 'Title ', Customer::$titles)
            ->setFirstOption('--- Select ---');
        $form->addText('firstName', 'First Name *')
            ->addRule(FForm::Required, 'Please provide firstname!');
        $form->addText('lastName', 'Last Name  *')
            ->addRule(FForm::Required, 'Please provide lastname!');
        $form->addText('address1', 'Address 1  *')
            ->addRule(FForm::Required, 'Please provide address 1!');
        $form->addText('address2', 'Address 2 &nbsp;&nbsp;');
        $form->addText('address3', 'Address 3 &nbsp;&nbsp;');
        $form->addText('city', 'City  *')
            ->addRule(FForm::Required, 'Please provide city!');
        $form->addText('county', 'County &nbsp;&nbsp;');
        $form->addText('postcode', 'Postcode *')
            ->addRule(FForm::Required, 'Please provide post code!');
        $form->addSelect('countryId', 'Country  *', Customer::$countries)
            ->setFirstOption('--- Select ---')
            ->addRule(FForm::Required, 'Please provide country!');
        $form->addText('phone', 'Phone *')
            ->addRule(FForm::Required, 'Please provide phone!');
        $form->addSubmit('register', 'Create Account')
            ->class('button');

        $form->onValid = $callback;
        $form->start();

        return $form;
    }

    /**
     * @param FForm $form
     * @throws Exception
     */
    public function processRegistrationForm(FForm $form)
    {
        $customer = new Customer();
        $data = $form->getValues();
        $customer->roleId = Customer::ROLE_NORMAL;
        $customer->tagId = Customer::TAG_AFFILIATE;
        $customer->email = $data['email'];
        $customer->password = $data['password'];
        $customer->titleId = $data['titleId'];
        $customer->firstName = $data['firstName'];
        $customer->lastName = $data['lastName'];
        $customer->address1 = $data['address1'];
        $customer->address2 = $data['address2'];
        $customer->address3 = $data['address3'];
        $customer->city = $data['city'];
        $customer->county = $data['county'];
        $customer->postcode = $data['postcode'];
        $customer->countryId = $data['countryId'];
        $customer->phone = $data['phone'];
        $customer->affiliateId = $this->affiliate->getId();
        $customerId = $customer->save();
        $form->clean();
        Customer::signIn($customerId);
    }

    /**
     * @return FForm
     */
    public function getComponentSummaryForm()
    {
        $form = new FForm('widgetWhiteLabelSummary');

        $action = FApplication::$router->link(
            WidgetWhiteLabelControler::REDIRECT_PAGE,
            ['hash' => $this->getAffiliate()->hash]
        );
        $form->setAction($action);

        $form->addSubmit('continue', 'Continue >');
        $form->start();

        return $form;
    }

    /**
     * @param Text $control
     * @param string $error
     * @param array $params
     * @return string|bool
     */
    public function Validator_checkCompanyName(Text $control, $error, array $params)
    {
        try {
            $companyName = $control->getValue();
            $available = CompanySearch::isAvailable($companyName);
            if ($available == TRUE) {
                return TRUE;
            } else {
                return $error;
            }
        } catch (Exception $e) {
            return TRUE;
        }
    }

    /**
     * @param Password $control
     * @param array $error
     * @param array $params
     * @return string|bool
     */
    public function Validator_loginCredentials(Password $control, array $error, array $params)
    {
        // authentificate user
        try {
            $customer = Customer::doAuthenticate(
                $control->owner['email']->getValue(),
                $control->owner['password']->getValue()
            );

            return TRUE;
        } catch (Exception $e) {
            if ($e->getMessage() == 1) {
                $error = $error[1];

                return $error;
            } elseif ($e->getMessage() == 2) {
                $error = $error[2];

                return $error;
            } elseif ($e->getMessage() == 3) {
                $error = $error[3];

                return $error;
            } else {
                $error = $error[4];

                return $error;
            }
        }
    }

    /**
     * @param FControl $control
     * @param string $error
     * @param array $params
     * @return string|bool
     */
    public function Validator_uniqueLogin(FControl $control, $error, array $params)
    {
        $customerId = dibi::select('customerId')
            ->from(TBL_CUSTOMERS)
            ->where('email=%s', $control->getValue())
            ->execute()->fetchSingle();
        if ($customerId) {
            return $error;
        }

        return TRUE;
    }
}
