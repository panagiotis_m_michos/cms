<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS 
 * @author 		Stan (diviak@gmail.com)
 * @internal 	project/models/JourneyCustomerProduct.php
 * @created 	17/07/2009
 */

class JourneyCustomerProduct extends Object
{
	/** @var int */
	public $journeyCustomerProductId;
	
	/** @var int */
	public $journeyCustomerId;
	
	/** @var int */
	public $productId;
	
	/** @var string */
	public $productName;
	
	/** @var string */
	public $dtc;
	
	/** @var string */
	public $dtm;
    		
	/**
	 * @return int
	 */
	public function getId() { return( $this->journeyCustomerProductId ); }

	/**
	 * @return bool
	 */
	public function exists() { return( $this->journeyCustomerProductId > 0 ); }

	/**
	 * @return bool
	 */
	public function isNew() { return( $this->journeyCustomerProductId == 0 ); }

	/**
	 * @param int $journeyCustomerProductId
	 * @return void
	 */
	public function __construct($journeyCustomerProductId = 0)
	{
		$this->journeyCustomerProductId = (int)$journeyCustomerProductId; 
		if ($this->journeyCustomerProductId) {
			$w = FApplication::$db->select('*')->from(TBL_JOURNEY_CUSTOMER_PRODUCTS)->where('journeyCustomerProductId=%i', $journeyCustomerProductId)->execute()->fetch();
			if ($w) {
				$this->journeyCustomerProductId = $w['journeyCustomerProductId'];
				$this->journeyCustomerId = $w['journeyCustomerId'];
				$this->productId = $w['productId'];
				$this->productName = $w['productName'];
                $this->dtc = $w['dtc'];
				$this->dtm = $w['dtm'];
			} else {
				throw new Exception("__CLASS__ with id `$journeyCustomerProductId` doesn't exist!");
			}
		} 
	}
	
	/**
	 * Provides save order item to database
	 * @return void
	 */
	public function save()
	{
		$data = array();
		$data['journeyCustomerId'] = $this->journeyCustomerId;
		$data['productId'] = $this->productId;
		$data['productName'] = $this->productName;
		$data['dtm'] = new DibiDateTime();
		// insert
		if ($this->isNew()){
            $data['dtc'] = new DibiDateTime();
			$this->journeyCustomerProductId = FApplication::$db->insert(TBL_JOURNEY_CUSTOMER_PRODUCTS, $data)->execute(dibi::IDENTIFIER);
		// update
		} else {
			FApplication::$db->update(TBL_JOURNEY_CUSTOMER_PRODUCTS, $data)->where('journeyCustomerProductId=%i', $this->journeyCustomerProductId)->execute();
		}
		return $this->journeyCustomerProductId;
	}

	/**
	 * @return void
	 */
	public function delete()
	{
		FApplication::$db->delete(TBL_JOURNEY_CUSTOMER_PRODUCTS)->where('journeyCustomerProductId=%i', $this->journeyCustomerProductId)->execute();
	}
	
	/**
	 * Returns all products by the conditions
	 *
	 * @param int $limit
	 * @param int $offset
	 * @param array $where
	 * @return array<DibiRow>
	 */
	static public function getAll($limit = NULL, $offset = NULL, array $where = array())
	{
		$result = FApplication::$db->select('journeyCustomerProductId')
			->from(TBL_JOURNEY_CUSTOMER_PRODUCTS)
			->orderBy('journeyCustomerProductId', dibi::DESC);
			
		foreach ($where as $key => $val) {
			$val = '%'.$val.'%';
			$result->where('%n', $key, ' LIKE %s', $val);
		}
		if ($limit !== NULL) $result->limit($limit);
		if ($offset !== NULL) $result->offset($offset);
		
		$result->execute()->fetchSingle();
		return $result;
	}
	
	/**
	 * Returns items as objects
	 *
	 * @param int $limit
	 * @param int $offset
	 * @param array $where
	 * @return array<JourneyCustomerProduct>
	 */
	static public function getAllObjects($limit = NULL, $offset = NULL, array $where = array())
	{
		$return = array();
		$array = self::getAll($limit, $offset, $where);
		foreach ($array as $key => $val) {
			try {
				$return[$val->journeyCustomerProductId] = new self($val->journeyCustomerProductId);
			} catch (Exception $e) {
			}
		}
		return $return;
	}
	
	/**
	 * Returns JourneyCustomerProducts byt $journeyCustomerId
	 *
	 * @param int $journeyCustomerId
	 * @return array<JourneyCustomerProduct>
	 */
	static public function getJourneyCustomerProducts($journeyCustomerId)
	{
		$result = FApplication::$db->select('journeyCustomerProductId')
			->from(TBL_JOURNEY_CUSTOMER_PRODUCTS)
			->where('journeyCustomerId=%i', $journeyCustomerId)
			->orderBy('journeyCustomerProductId', dibi::DESC)
			->execute()->fetchAll();
		
		$return = array();
		foreach ($result as $key => $val) {
			try {
				$id = $val->journeyCustomerProductId;
				$return[$id] = new self($id);
			} catch (Exception $e) {
			}
		}
		return $return;
	}
}