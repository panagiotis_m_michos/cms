<?php

class HistoryViewerModel extends FNode
{

    /** @var array */
    static public $identifier = array(
        'ChangeRegisteredOfficeAddress' => 'ch_address_change',
        'OfficerResignation' => 'ch_officer_resignation',
        'OfficerAppointment' => 'ch_officer_appointment',
        'OfficerChangeDetails' => 'ch_officer_change',
        'ReturnOfAllotmentShares' => 'ch_return_shares',
        'ChangeAccountingReferenceDate' => 'ch_change_accounting_reference_date',
        'ChangeOfName' => 'ch_change_company_name',
    );
    private $company;

    private $submision;

    public function setCompany($company)
    {
        $this->company = $company;
    }

    public function getErrors($submisionId)
    {
        $error = dibi::select('*')->from('ch_form_submission_error')->where('form_submission_id=%i', $submisionId)->execute()->fetchAll();
        return $error;    
	}

    public function getOtherDecoratedData($formid, $form_identifier)
    {
        $data = dibi::select('*')->from(self::$identifier[$form_identifier])->where('form_submission_id=%i', $formid)->execute()->fetch();
        unset($data['form_submission_id']);
        if(isset($data['reject_description'])){
			unset($data['reject_description']);
		}
        if (self::$identifier[$form_identifier] == 'ch_officer_appointment') {
            if ($data['corporate'] == 1) {
                unset($data['care_of_name']);
                unset($data['po_box']);
                unset($data['dob']);
                unset($data['nationality']);
                unset($data['occupation']);
                unset($data['country_of_residence']);
                unset($data['residential_premise']);
                unset($data['residential_street']);
                unset($data['residential_thoroughfare']);
                unset($data['residential_post_town']);
                unset($data['residential_county']);
                unset($data['residential_country']);
                unset($data['residential_postcode']);
                unset($data['residential_secure_address_ind']);
            }
            if ($data['corporate'] == 0 && $data['type'] == 'SEC') {
                unset($data['corporate_name']);
                unset($data['care_of_name']);
                unset($data['po_box']);
                unset($data['dob']);
                unset($data['nationality']);
                unset($data['occupation']);
                unset($data['country_of_residence']);
                unset($data['residential_premise']);
                unset($data['residential_street']);
                unset($data['residential_thoroughfare']);
                unset($data['residential_post_town']);
                unset($data['residential_county']);
                unset($data['residential_country']);
                unset($data['residential_postcode']);
                unset($data['residential_secure_address_ind']);
                unset($data['identification_type']);
                unset($data['place_registered']);
                unset($data['registration_number']);
                unset($data['law_governed']);
                unset($data['legal_form']);
            }
            if ($data['corporate'] == 0 && $data['type'] == 'DIR') {
                unset($data['corporate_name']);
                unset($data['care_of_name']);
                unset($data['po_box']);
                unset($data['identification_type']);
                unset($data['place_registered']);
                unset($data['registration_number']);
                unset($data['law_governed']);
                unset($data['legal_form']);
            }
        }
        //decorating
        foreach ($data as $key => $value) {
            if ($key == 'date') {
                $key = 'New Date';
            }
            $key = preg_replace('/([A-Z])/', ' $1', $key);
            $key = str_replace("_", " ", $key);
            $key = ucwords(strtolower($key));
            if (($value == '1' || $value == '0') && self::$identifier[$form_identifier] != 'ch_return_shares') {
                $value = str_replace("1", "Yes", $value);
                $value = str_replace("0", "No", $value);
            }
            $dataDecorated[$key] = $value;
        }
        return $dataDecorated;
    }

    public function setFormSubmision($submision)
    {
        $this->submision = $submision;
    }

    public function getFormSubmision()
    {
        return $this->submision;
    }

    /*     * **************************************** components ***************************************** */

    public function getComponentOffice()
    {
        $countries = array('EW' => 'England and Wales', 'SC' => 'Scotland');
        $office = (object) $this->company->getFormSubmission($this->getFormSubmision())->getForm()->getAddress()->getFields();
        $office->country = isset($countries[$office->country]) ? $countries[$office->country] : $office->country;
        return $office;
    }

    public function getComponentPersons($type)
    {
        $persons = $this->company->getFormSubmission($this->getFormSubmision())->getForm()->getIncPersons(array($type));
        $corporates = $this->company->getFormSubmission($this->getFormSubmision())->getForm()->getIncCorporates(array($type));
        $persons = array_merge($persons, $corporates);

        $return = array();
        foreach ($persons as $key => $val) {
            $return[] = (object) $val->getFields();
        }
        return $return;
    }

    public function getComponentArticle()
    {
        $articles = array(
            'article' => $this->company->getFormSubmission($this->getFormSubmision())->getMemarts(),
            'support' => $this->company->getFormSubmission($this->getFormSubmision())->getSuppnameauth()
        );
        return $articles;
    }

}
