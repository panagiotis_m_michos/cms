<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    Affiliate.php 2010-12-16 divak@gmail.com
 */


class AffiliateProduct extends Object
{
	/** @var int */
	public $affiliateProductId;
	
	/** @var int */
	public $affiliateId;

	/** @var int */
	public $productId;
    
	/** @var float */
	public $markUp;
	
	/** @var string */
	public $dtc;
	
	/** @var string */
	public $dtm;

    /**
     * @var string
     */
	static private $nameSpace = 'affiliateProduct';


	/**
	 * @param int $affiliateId
	 * @return void
	 */
	public function __construct($affiliateProductId = 0)
	{
		$this->affiliateProductId = (int)$affiliateProductId; 
		if ($this->affiliateProductId) {
			$w = dibi::select('*')->from(TBL_AFFILIATE_PRODUCTS)->where('affiliateProductId=%i', $this->affiliateProductId)->execute()->fetch();
			if ($w) {
				$this->affiliateProductId = $w['affiliateProductId'];
				$this->affiliateId = $w['affiliateId'];
				$this->productId = $w['productId'];
				$this->markUp = $w['markUp'];
				$this->dtc = $w['dtc'];
				$this->dtm = $w['dtm'];
			} else {
			    throw new Exception("Affiliate with id `$affiliateProductId` doesn't exist.");
			}
		} 
	}
	
	/**
	 * @return int
	 */
	public function getId() { return( $this->affiliateProductId ); }

	/**
	 * @return bool
	 */
	public function isNew() { return( $this->affiliateProductId == 0 ); }
	
	/**
	 * @return int
	 */
	public function save()
	{
		// data
		$data = array();
		$data['affiliateProductId'] = $this->affiliateProductId;
		$data['affiliateId'] = $this->affiliateId;
		$data['productId'] = $this->productId;
		$data['markUp'] = $this->markUp;
		$data['dtc'] = $this->dtc;
		$data['dtm'] = new DibiDateTime();	
		// insert
		if ($this->isNew()){
			$data['dtc'] = new DibiDateTime();
			$this->affiliateProductId = dibi::insert(TBL_AFFILIATE_PRODUCTS, $data)->execute(dibi::IDENTIFIER);
		// update
		} else {
			dibi::update(TBL_AFFILIATE_PRODUCTS, $data)->where('affiliateProductId=%i', $this->affiliateProductId)->execute();
		}
		return $this->affiliateProductId;
	}

	/**
	 * @return void
	 */
	public function delete()
	{
		dibi::delete(TBL_AFFILIATE_PRODUCTS)->where('affiliateProductId=%i', $this->affiliateProductId)->execute();
	}
	
	/**
	 * @return array<>
	 */
	public function getProduct() {
		return FNode::getProductById($this->productId);
	}
	
	/**
	 * Returns affiliates based on params
	 *
	 * @param int $limit
	 * @param int $offset
	 * @param array $where
	 * @return array<DibiRow>
	 */
	static public function getAll($limit = NULL, $offset = NULL, array $where = array())
	{
		$result = dibi::select('*')->from(TBL_AFFILIATE_PRODUCTS)->orderBy('dtc', dibi::DESC);
		
		if ($limit !== NULL) $result->limit($limit);
		if ($offset !== NULL) $result->offset($offset);
		
		foreach ($where as $key => $val) {
			$result->where('%n', $key, ' = %s',$val);
		}
		
		return $result->execute()->fetchAssoc('affiliateProductId');
	}
	
	/**
	 * Returns result as objects
	 *
	 * @param unknown_type $limit
	 * @param unknown_type $offset
	 * @param array $where
	 * @return array<AffiliateProduct>
	 */
	static public function getAllObjects($limit = NULL, $offset = NULL, array $where = array())
	{
		$objects = array();
		$all = self::getAll($limit, $offset, $where);
		foreach ($all as $key => $val) {
			$objects[$key] = new self($key);		
		}
		return $objects;
	}
	
	/**
	 * Return count of affilates based on param
	 *
	 * @param array $where
	 * @return int
	 */
	static public function getCount(array $where = array())
	{
	 	$result = dibi::select('count(*)')->from(TBL_AFFILIATE_PRODUCTS);
	 	
		foreach ($where as $key => $val) {
			$result->where('%n', $key, ' = %s',$val);
		}
		
		$count = $result->execute()->fetchSingle();
		return $count;
	}
	
	/**
	 * @param type $affiliateId
	 * @return array<AffiliateProduct>
	 */
	static public function getAffiliateProducts($affiliateId){
		return self::getAllObjects(null, null, array('affiliateId' => $affiliateId));
	}
	
	/**
	 * @param type $affiliateId
	 * @return array<AffiliateProduct>
	 */
	static public function getAffiliateProductIds($affiliateId){
		$products = self::getAffiliateProducts($affiliateId);
		$arr = array();
		foreach ($products as $product) {
			$arr[] = $product->productId;
		}
		return $arr;
	}
	    
    /**
	 * @param type $packageId
	 * @param type $affiliateId
	 * @return array<AffiliateProduct>
	 */
	static public function getMarkup($packageId, $affiliateId){
        $result = dibi::select('markUp')->from(TBL_AFFILIATE_PRODUCTS)->where('productId=%s', $packageId)->where('affiliateId=%s', $affiliateId)->execute()->fetchSingle();
        return $result;
    }
}