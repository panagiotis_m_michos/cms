<?php
/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS
 * @author 		Nikolai Senkevich, Stan Bazik
 * @internal 	project/models/CompanyMonitoring.php
 * @created 	21/03/2011
 */

class CompanyMonitoring extends Object
{	
	/**
	 * primary key
	 *
	 * @var int
	 */
	public $companyMonitoredId;
	
	/**
	 * @var string
	 */
	public $companyName;
	
	/**
	 * @var int
	 */
	public $customerId;
	
	/**
	 * @var int
	 */
	public $companyNumber;

    /**
	 * @var string
	 */
	public $companyStatus;

    /**
	 * @var date
	 */
	public $incorporationDate;

    /**
	 * @var date
	 */
	public $accountsNextDueDate;

    /**
	 * @var date
	 */
	public $returnsNextDueDate;
	
	/**
	 * @var string
	 */
	public $dtc;
	
	/**
	 * @var string
	 */
	public $dtm;
	
		
	/**
	 * @return int
	 */
	public function getId() 
	{ 
		return $this->companyMonitoredId;
	}
	
	/**
	 * @return bool
	 */
	public function isNew() 
	{ 
		return ($this->companyMonitoredId == 0);
	}
	
	/**
	 * @return string
	 */
	public function getType()
	{
		return (isset(self::$types[$this->companyNumber])) ? self::$types[$this->companyNumber] : NULL;
	}

	/**
	 * @param int $companyMonitoredId
	 * @return void
	 * @throws Exception
	 */
	public function __construct($companyMonitoredId = 0)
	{
		$this->companyMonitoredId = (int) $companyMonitoredId;
		if ($this->companyMonitoredId) {
			$w = dibi::select('*')->from(TBL_COMPANIES_MONITORING)->where('companyMonitoredId=%i', $companyMonitoredId)->execute()->fetch();
			if ($w) {				
				$this->companyMonitoredId = $w['companyMonitoredId'];
				$this->customerId = $w['customerId'];
                $this->companyName = $w['companyName'];
				$this->companyNumber = $w['companyNumber'];
                $this->companyStatus = $w['companyStatus'];
                $this->incorporationDate = $w['incorporationDate'];
                $this->accountsNextDueDate = $w['accountsNextDueDate'];
                $this->returnsNextDueDate = $w['returnsNextDueDate'];
				$this->dtc = $w['dtc'];
				$this->dtm = $w['dtm'];
			} else {
				throw new Exception("Signup with id `$companyMonitoredId` doesn't exist!");
			}
		} 
	}
	
	/**
	 * @return int
	 */
	public function save()
	{
		$data = array();
		$data['companyName'] = $this->companyName;
		$data['customerId'] = $this->customerId;
		$data['companyNumber'] = $this->companyNumber;
        $data['companyStatus'] = $this->companyStatus;
        $data['incorporationDate'] = $this->incorporationDate;
        $data['accountsNextDueDate'] = $this->accountsNextDueDate;
        $data['returnsNextDueDate'] = $this->returnsNextDueDate;
		$data['dtm'] = new DibiDateTime();
		
		if ($this->isNew()) {
			$data['dtc'] = new DibiDateTime();
			$this->companyMonitoredId = dibi::insert(TBL_COMPANIES_MONITORING, $data)->execute(dibi::IDENTIFIER);
		} else {
			dibi::update(TBL_COMPANIES_MONITORING, $data)->where('companyMonitoredId=%i', $this->companyMonitoredId)->execute();
		}
		return $this->companyMonitoredId;
	}

	/**
	 * @return void
	 */
	public function delete()
	{
		dibi::delete(TBL_COMPANIES_MONITORING)->where('companyMonitoredId=%i', $this->companyMonitoredId)->execute();
	}
	
	
	/**
	 * Returns all companies
	 *
	 * @param int $limit
	 * @param int $offset
	 * @param array $where
	 * @return int
	 */
	static public function getAll($limit = NULL, $offset = NULL, array $where = array(), $order = NULL)
	{
		$result = dibi::select('*')->from(TBL_COMPANIES_MONITORING)->orderBy($order);
		if ($limit !== NULL) $result->limit($limit);
		if ($offset !== NULL) $result->offset($offset);
		foreach ($where as $key => $val) $result->where($key,$val);
		return $result->execute()->fetchAssoc('companyMonitoredId');
	}
	
	/**
	 * Returns result as objects
	 *
	 * @param int $limit
	 * @param int $offset
	 * @param array $where
	 * @return array<MrSiteCode>
	 */
	static public function getAllObjects($limit = NULL, $offset = NULL, array $where = array())
	{
		$objects = array();
		$all = self::getAll($limit, $offset, $where);
		foreach ($all as $key => $val) {
			$objects[$key] = new self($key);		
		}
		return $objects;
	}
	
    /**
	 * Returns count of companies
	 *
	 * @param array $where
	 * @return int
	 */
	static public function getCount(array $where = array())
	{
		$result = dibi::select('count(*)')->from(TBL_COMPANIES_MONITORING);
		foreach ($where as $key => $val) $result->where($key,$val);
		$count = $result->execute()->fetchSingle();
		return $count;
	}
	
}