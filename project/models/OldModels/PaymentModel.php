<?php

class PaymentModel extends FNode
{

    const TRANSACTION_SAGE = 'SAGE';
    const TRANSACTION_PAYPAL = 'PAYPAL';
    const PAYMNET_PAGE_NEW_CUSTOMER_VIEW = "paymentNewCustomer";
    const PAYMNET_PAGE_CUSTOMER_VIEW = "paymentCustomer";
    const PAYMNET_PAGE_FREE_ORDER_VIEW = "paymentFreeOrder";

    /**
     * @tutorial Payment New Controller Admin Property
     * @var $enableMessages bool
     */
    private $enableMessages;

    /**
     * @tutorial Payment New Controller Admin Property
     * @var $paymentPageMessages text
     */
    private $paymentPageMessages;

    /**
     * @tutorial Disable Sage Payment Form
     * @var $disableSage bool
     */
    private $disableSage;

    /**
     * @param bool $enableMessages
     */
    public function setEnableMessages($enableMessages)
    {
        $this->enableMessages = (bool) $enableMessages;
    }

    /**
     * @param text $paymentPageMessages
     */
    public function setPaymentPageMessages($paymentPageMessages)
    {
        $this->paymentPageMessages = $paymentPageMessages;
    }

    /**
     * @return bool
     */
    public function getEnableMessages()
    {
        return (bool) $this->enableMessages;
    }

    /**
     * @return text
     */
    public function getPaymentPageMessages()
    {
        return $this->paymentPageMessages;
    }

    /**
     * @param bool $disableSage
     */
    public function setDisableSage($disableSage)
    {
        $this->disableSage = $disableSage;
    }

    /**
     * @return bool
     */
    public function getDisableSage()
    {
        return $this->disableSage;
    }

    /**
     *
     * @param Basket $basket
     * @param array $additionalParams
     * @return array
     */
    public function getConfirmationPageLinkParams($basket, $additionalParams = [])
    {
        $linkPackageVariable = [];
        if (!isset($basket) && empty($basket)) {
            return $linkPackageVariable;
        }

        $linkPackageVariable['orderId'] = $basket->getOrderId();
        $linkPackageVariable += $additionalParams;

        return $linkPackageVariable;
    }

    /**
     *
     * @param array $data
     * @return string
     * @throws Exception
     */
    public function getNewCustomerName($data)
    {
        if (!isset($data['cardholder'])) {
            throw new Exception('Cardholder name is missing!');
        }

        if (preg_match("#(\bMRS\b|\bMR\b|\bMS\b|\bDR\b|\bPROF\b|\bMASTER\b|\bREV\b|\bSIR\b|\bLORD\b)#i", strstr($data['cardholder'], ' ', 1), $matches)) {
            return trim(preg_replace("/$matches[0]/", '', $data['cardholder'], 1));
        } else {
            return $data['cardholder'];
        }
    }

    /**
     *
     * @param array $data
     * @return string
     * @throws Exception
     */
    public function getNewCustomerEmail($data)
    {
        if (!isset($data['newAccountEmail']) || empty($data['newAccountEmail'])) {
            throw new Exception('Error creating new account');
        }

        if ($this->reValidateUniqueLogin($data)) {
            throw new Exception('Error creating account');
        }

        return $data['newAccountEmail'];
    }

    /**
     * Validator
     * @param array $data
     * @return bool
     */
    public function reValidateUniqueLogin($data)
    {
        $customerId = FApplication::$db->select('customerId')->from(TBL_CUSTOMERS)->where('email=%s', $data['newAccountEmail'])->execute()->fetchSingle();
        return $customerId ? FALSE : TRUE;
    }

    /**
     * Validator
     * @param array $data
     * @return bool
     */
    public function validateUniqueLogin($email)
    {
        $customerId = FApplication::$db->select('customerId')->from(TBL_CUSTOMERS)->where('email=%s', $email)->execute()->fetchSingle();
        return ($customerId) ? TRUE : FALSE;
    }

    /**
     *
     * @param type $email
     * @return string
     */
    public function checkEmail($email)
    {
        if (preg_match('/^[a-z0-9!#$%&\'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/i', $email)) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     *
     * @param type $email
     * @return boolean
     */
    public function generateNewCustomerPassord($email)
    {
        if ($this->getJsonEmailResponse($email)) {
            // re-save customer
            $customer = Customer::getCustomerByEmail($email);
            $customer->password = FTools::generPwd(8);
            $customer->save();

            $link = FApplication::$httpRequest->uri->getHostUri() . FApplication::$router->link(LoginControler::LOGIN_PAGE);
            /** @var ForgottenPasswordEmailer $forgottenPasswordEmailer */
            $forgottenPasswordEmailer = Registry::$container->get(DiLocator::EMAILER_FORGOTTEN_PASSWORD);
            $forgottenPasswordEmailer->forgottenPasswordEmail($link, $customer);

            $json = ['succes' => 1];
        } else {
            $json = ['error' => 1];
        }

        return $json;
    }

    /**
     *
     * @param type $email
     * @return boolean
     */
    public function getJsonEmailResponse($email)
    {
        if (isset($email) && $this->checkEmail($email)) {
            if ($this->validateUniqueLogin($email)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public static function savePaymentFailedTransatcion($post, $basket, $VendorTxCode, $paymentSystem, $message, $paymentType, $errorType, Customer $customer = NULL)
    {

        $basketItems = [];
        if ($basket instanceof Basket) {
            $basketPrice = $basket->getPrice()->total;
            foreach ($basket->getItems() as $product) {
                $basketItems[] = ['ProductName' => $product->getLongTitle(), 'ProductPrice' => $product->getPrice()];
            }
        } else {
            $basketPrice = NULL;
        }


        if ($customer) {
            $customerEmail = $customer->email;
        } else {
            $customerEmail = NULL;
        }

        $post = unserialize($post);

        if ($post instanceof SagePayDirect) {
            $paymentDetails = $post->getAction();
            if (!$paymentDetails instanceof SagePayTokenRemove) {
                $newCustomerAddress = $paymentDetails->getDeliveryAddress();
                $email = $paymentDetails->getCustomerEMail() ? $paymentDetails->getCustomerEMail() : $customerEmail;
                $name = $newCustomerAddress->getFirstnames() . ' ' . $newCustomerAddress->getSurname();
                $address1 = $newCustomerAddress->getLine1();
                $address2 = $newCustomerAddress->getLine2();
                $postcode = $newCustomerAddress->getPostcode();
                $country = $newCustomerAddress->getCountry();
                $auth3D = 0;
            } else {
                $email = NULL;
                $name = NULL;
                $address1 = NULL;
                $address2 = NULL;
                $postcode = NULL;
                $country = NULL;
                $auth3D = 0;
            }
        } else {
            $email = isset($post['emails']) ? $post['emails'] : $customerEmail;
            $name = isset($post['cardholder']) ? $post['cardholder'] : NULL;
            $address1 = isset($post['address1']) ? $post['address1'] : NULL;
            $address2 = isset($post['address2']) ? $post['address2'] : NULL;
            $postcode = isset($post['postcode']) ? $post['postcode'] : NULL;
            $country = isset($post['country']) ? $post['country'] : NULL;
            $auth3D = isset($post['auth3D']) ? $post['auth3D'] : 0;
        }

        $paymentErrorLog = new PaymentErrorsLog();
        $paymentErrorLog->paymentTypeId = $paymentType;
        $paymentErrorLog->errorTypeId = $errorType;
        $paymentErrorLog->vendorTXCode = $VendorTxCode;
        $paymentErrorLog->email = $email;
        $paymentErrorLog->name = $name;
        $paymentErrorLog->address1 = $address1;
        $paymentErrorLog->address2 = $address2;
        $paymentErrorLog->postcode = $postcode;
        $paymentErrorLog->country = $country;
        $paymentErrorLog->basketPrice = $basketPrice;
        $paymentErrorLog->basketItems = $basketItems;
        $paymentErrorLog->paymentSystem = $paymentSystem;
        $paymentErrorLog->errorMessage = $message;
        $paymentErrorLog->auth3D = $auth3D;
        $paymentErrorLog->dtc = new DibiDateTime();
        $paymentErrorLog->dtm = new DibiDateTime();
        $id = $paymentErrorLog->save();
    }

    public function getPaymentErrorLogById($errorId)
    {

        $error = new PaymentErrorsLog($errorId);
        return $error;
    }

    /**
     * Provides adding custom data
     *
     * @return void
     */
    public function addCustomData()
    {
        $this->enableMessages = $this->getProperty('enableMessages');
        $this->disableSage = $this->getProperty('disableSage');
        $this->paymentPageMessages = $this->getProperty('paymentPageMessages');
        parent::addCustomData();
    }

    /**
     * Provides saving custom data
     *
     * @param string $action
     * @return void
     */
    public function saveCustomData($action)
    {
        $this->saveProperty('disableSage', $this->disableSage);
        $this->saveProperty('enableMessages', $this->enableMessages);
        $this->saveProperty('paymentPageMessages', $this->paymentPageMessages);
        parent::saveCustomData($action);
    }

    /**
     * @param Customer $customer
     * @param Basket $basket
     * @return string
     */
    public function getPaymentView(Customer $customer, Basket $basket)
    {
        $view = $customer->isNew() ? self::PAYMNET_PAGE_NEW_CUSTOMER_VIEW : self::PAYMNET_PAGE_CUSTOMER_VIEW;
        return $basket->isFreeBasket() ? self::PAYMNET_PAGE_FREE_ORDER_VIEW : $view;
    }

    /**
     * @param Customer $customer
     * @param Basket $basket
     */
    public function isOkToShowOnAccountPayment(Customer $customer, Basket $basket)
    {
        $basketMoadel = new BasketModel();
        if (!$basketMoadel->hasCreditProduct($basket) && $customer->credit > 0) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * @param Customer $customer
     * @return null
     */
    public function getCustomerPaymentCountry(Customer $customer)
    {
        $swap = array_flip(WPDirect::$countries);
        if (isset($swap[$customer->country])) {
            return $swap[$customer->country];
        }
        return NULL;
    }

    /**
     * @param Customer $customer
     * @param Basket $basket
     * @return bool
     */
    public function isAbleToUseCredits(Customer $customer, Basket $basket)
    {
        if ($customer->credit > 0) {
            if ($customer->credit >= $basket->getPrice('total')) {
                return TRUE;
            }
            $deductedPrice = $basket->getPrice('total') - $customer->credit;
            if ($deductedPrice >= 1 && $deductedPrice <= 100000) {
                return TRUE;
            }
        }
        return FALSE;
    }

}
