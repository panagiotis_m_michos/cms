<?php

class VoucherNew extends Object
{

    /** types */
    const TYPE_PERCENT = 'PERCENT';
    const TYPE_AMOUNT = 'AMOUNT';

    /** @var array */
    static public $types = array(
        self::TYPE_PERCENT => 'Percent',
        self::TYPE_AMOUNT => 'Amount',
    );

    /** @var int * */
    public $voucherId;

    /** @var string */
    public $name;

    /** @var string */
    public $voucherCode;

    /** @var int */
    public $typeId;

    /** @var int */
    public $typeValue;

    /** @var int */
    public $minSpend;

    /** @var int */
    public $usageLimit;

    /** @var int */
    public $used = 0;

    /** @var string */
    public $dtExpiry;

    /** @var string */
    public $dtc;

    /** @var string */
    public $dtm;

    /**
     * @return int
     */
    public function getId()
    {
        return( $this->voucherId );
    }

    /**
     * @return bool
     */
    public function exists()
    {
        return( $this->voucherId > 0 );
    }

    /**
     * @return bool
     */
    public function isNew()
    {
        return( $this->voucherId == 0 );
    }

    public function getType()
    {
        return isset(self::$types[$this->typeId]) ? self::$types[$this->typeId] : '';
    }

    /**
     * @param int$voucherId
     * @return void
     */
    public function __construct($voucherId = 0)
    {
        $this->voucherId = (int) $voucherId;
        if ($this->voucherId) {
            $w = FApplication::$db->select('*')->from(TBL_VOUCHERS)->where('voucherId=%i', $voucherId)->execute()->fetch();
            if ($w) {
                $this->voucherId = $w['voucherId'];
                $this->name = $w['name'];
                $this->voucherCode = $w['voucherCode'];
                $this->typeId = $w['typeId'];
                $this->typeValue = $w['typeValue'];
                $this->minSpend = $w['minSpend'];
                $this->usageLimit = $w['usageLimit'];
                $this->used = $w['used'];
                $this->dtExpiry = $w['dtExpiry'];
                $this->dtc = $w['dtc'];
                $this->dtm = $w['dtm'];
            } else {
                throw new Exception("Voucher with id `$voucherId` doesn't exist.");
            }
        }
    }

    /**
     * Provides save order item to database
     * @return void
     */
    public function save()
    {
        $action = $this->voucherId ? 'update' : 'insert';

        $data = array();
        $data['voucherId'] = $this->voucherId;
        $data['name'] = $this->name;
        $data['voucherCode'] = $this->voucherCode;
        $data['typeId'] = $this->typeId;
        $data['typeValue'] = $this->typeValue;
        $data['minSpend'] = $this->minSpend;
        $data['usageLimit'] = $this->usageLimit;
        $data['used'] = $this->used;
        $data['dtExpiry'] = $this->dtExpiry;
        // insert
        if ($action == 'insert') {
            $data['dtc'] = new DibiDateTime();
            $data['dtm'] = new DibiDateTime();
            $this->voucherId = FApplication::$db->insert(TBL_VOUCHERS, $data)->execute(dibi::IDENTIFIER);
            // update
        } else {
            $data['dtm'] = new DibiDateTime();
            FApplication::$db->update(TBL_VOUCHERS, $data)->where('voucherId=%i', $this->voucherId)->execute();
        }

        return $this->voucherId;
    }

    /**
     * @return void
     */
    public function delete()
    {
        if ($this->exists()) {
            FApplication::$db->delete(TBL_VOUCHERS)->where('voucherId=%i', $this->voucherId)->execute();
        }
    }

    /**
     * @param int $code
     * @param string $throwException
     * @return VoucherNew
     * @throws Exception
     */
    public static function getByCode($code, $throwException = 'throw')
    {
        $voucher = NULL;
        $voucherId = FApplication::$db->select('voucherId')->from(TBL_VOUCHERS)->where('voucherCode=%s', $code)->execute()->fetchSingle();
        if (empty($voucherId)) {
            if ($throwException === 'throw') {
                throw new Exception('Voucher with code `' . $code . '` does not exist!');
            }
        } else {
            $voucher = new self($voucherId);
        }
        return $voucher;
    }

    /**
     * Returns vouchers based on params
     *
     * @param int $limit
     * @param int $offset
     * @param array $where
     * @return array<DibiRow>
     */
    public static function getAll($limit = NULL, $offset = NULL, array $where = array())
    {
        $result = dibi::select('*')->from(TBL_VOUCHERS)->orderBy('dtc', dibi::DESC);

        if ($limit !== NULL) {
            $result->limit($limit);
        }
        if ($offset !== NULL) {
            $result->offset($offset);
        }

        foreach ($where as $key => $val) {
            $result->where('%n', $key, ' = %s', $val);
        }

        return $result->execute()->fetchAssoc('voucherId');
    }

    /**
     * Returns result as objects
     *
     * @param unknown_type $limit
     * @param unknown_type $offset
     * @param array $where
     * @return array<Affiliate>
     */
    public static function getAllObjects($limit = NULL, $offset = NULL, array $where = array())
    {
        $objects = array();
        $all = self::getAll($limit, $offset, $where);
        foreach ($all as $key => $val) {
            $objects[$key] = new self($key);
        }
        return $objects;
    }

    /**
     * Return count of affilates based on param
     *
     * @param array $where
     * @return int
     */
    public static function getCount(array $where = array())
    {
        $result = dibi::select('count(*)')->from(TBL_VOUCHERS);

        foreach ($where as $key => $val) {
            $result->where('%n', $key, ' = %s', $val);
        }

        $count = $result->execute()->fetchSingle();
        return $count;
    }

    /**
     * get voucher discount
     * @param numeric $amount
     * @return string
     */
    public function discount($amount)
    {
        $discount = 0;
        // percent
        if ($this->minSpend <= $amount) {
            if ($this->typeId == self::TYPE_PERCENT) {
                $discount = $this->typeValue > 100 ? $amount : $amount / 100 * $this->typeValue;
                // amount
            } elseif ($this->typeId == self::TYPE_AMOUNT) {
                $discount = $this->typeValue > $amount ? $amount : $this->typeValue;
            }
        }
        return Basket::round($discount);
    }

    /**
     * can we use the voucher
     * @param type $amount
     * @return bool 
     */
    public function isAvailableForUse($amount)
    {
        //not expired
        if (!empty($this->dtExpiry)) {
            $today = new DateTime(date('Y-m-d'));
            $expires = new DateTime($this->dtExpiry);
            if ($today > $expires) {
                return FALSE;
            }
        }
        if (!empty($this->usageLimit)) {
            if ($this->used >= $this->usageLimit) {
                return FALSE;
            }
        }
        if (!empty($this->minSpend)) {
            if ($amount < $this->minSpend) {
                return FALSE;
            }
        }
        return TRUE;
    }

    public static function Validator_VoucherCode($control, $error, $params)
    {
        if (($control->owner->isSubmitedBy('addVoucher') || $control->owner->isSubmitedBy('payment')) && $control->getValue() != '') {
            $voucher = VoucherNew::getByCode($control->getValue(), 'noThrow');
            //voucher exists ?
            if ($voucher) {
                // is valid voucher
                if ($voucher->isAvailableForUse($params['basket']->getPrice('subTotal'))) {
                    return TRUE;
                } elseif ($voucher->minSpend > $params['basket']->getPrice('subTotal')) {
                    $error[1] = sprintf($error[1], $voucher->minSpend);
                    return $error[1];
                }
            }
            return $error[0];
        } else {
            return TRUE;
        }
    }

    public static function codeExists($code)
    {
        $voucher = VoucherNew::getByCode($code, 'noThrow');
        return !empty($voucher);
    }

    /**
     * generate vouche unique code
     * @param string $code
     * @return string 
     */
    public static function generateUniqueCode($code)
    {
        $tries = 0;
        $newCode = 0;
        $voucherExists = TRUE;
        while ($voucherExists && $tries < 100) {
            $newCode = $code . RandString::generate(5, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ');
            $voucherExists = self::codeExists($newCode);
            $tries++;
        }
        if ($voucherExists) {
            throw new Exception('Unable to generate unique code');
        }
        return $newCode;
    }

}
