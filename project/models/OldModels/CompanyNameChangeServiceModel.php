<?php
use Entities\CompanyHouse\FormSubmission;

/**
 * @package     Companies Made Simple
 * @subpackage     CMS
 * @author         Stan (diviak@gmail.com)
 * @internal     project/models/CompanyNameChangeServiceModel.php
 * @created     10/03/2010
 */

class CompanyNameChangeServiceModel extends Object
{


    const LLP = "LLP";
    const RESOLUTION = "RESOLUTION";

    /**
     * @var array $ltdCompanySuffix
     */
    static public $ltdCompanySuffix = array(
        'LTD' => 'Ltd',
        'LIMITED'  => 'Limited',
        'CYF' => 'Cyf',
        'CYFYNGEDIG' => 'Cyfyngedig',
    );

    /**
     * @var array $plcCompanySuffix
     */
    static public $plcCompanySuffix = array(
        'PLC' => 'Plc',
        'PUBLIC LIMITED COMPANY'  => 'Public Limited Company',
    );

    /**
     * @var array $llpCompanySuffix
     */
    static public $llpCompanySuffix = array(
        'LLP' => 'Llp',
        'LIMITED LIABILITY PARTHENERSHIP'  => 'Limited Liability Partnership',
    );


    static public $changeCompanyNameProducts = array (
        CompanyNameChange::CHANGE_NAME_PRODUCT => 'Change name',
        CompanyNameChange::CHANGE_NAME_SAME_DAY_PRODUCT => 'Same day change name'
    );


    /**
     * @var Company $company
     */
    public $company;

    public function __construct(Company $company)
    {
        $this->company = $company;
    }

        /**
     * Provides adding company name change product to customer company
     *
     * @param Customer $customer
     * @param Company $company
     * @param array $data
     * @return void
     * @throws Exception
     */
    public static function addCompanyNameChangeToCustomerCompany(Customer $customer, Company $company, array $data)
    {
        try {
            /* === SAVE CHANGE NAME TYPE === */
            $company->setChangeNameId($data['changeNameId']);

            /* === SAVE ORDER === */
            $product = FNode::getProductById($data['changeNameId']);

            $order = new Order();
            $order->statusId = Order::STATUS_NEW;
            $order->customerId = $customer->getId();
            $order->realSubtotal = 0;
            $order->subtotal = 0;
            $order->vat = 0;
            $order->total = 0;
            $order->customerName = $customer->firstName . ' ' . $customer->lastName;

            $item = new OrderItem();
            $item->productId = $product->getId();
            $item->productTitle = 'Custom ' . $product->getLongTitle();
            $item->qty = 1;
            $item->price = 0;
            $item->subTotal = 0;
            $item->vat = 0;
            $item->totalPrice = 0;
            $item->incorporationRequired = FALSE;

            if (!empty($data['additional'])) {
                $item->additional = $data['additional']. "\n";
                $item->additional .= "=====\n";
                $item->additional .= 'by '.FUser::getSignedIn()->login;
            } else {
                $item->additional = 'by '.FUser::getSignedIn()->login;
            }

            $order->items[] = $item;
            $orderId = $order->save();

            $transaction = new Transaction();
            $transaction->customerId = $customer->getId();
            $transaction->typeId = Transaction::ON_ACCOUNT;
            $transaction->orderId = $orderId;
            $transaction->save();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Admin  - Check if is ok to change name
     * @throws Exception
     */
    public function isOkToChangeNameAdmin()
    {
        $this->checkIfCompanyIsPending();
        return TRUE;
    }


    /**
     * Front  - Check if is ok to change name
     * @throws Exception
     */
    public function isOkToChangeName()
    {
        $this->checkIfCompanyIsPending();
        $this->checkIfHaveProduct();
        return TRUE;
    }

    /**
     * Check if company have change name product bought
     * @throws Exception
     */
    protected function checkIfHaveProduct()
    {
        if ($this->company->hasChangeName() === FALSE) {
            throw new Exception("You need to purchase the Change Name Product. <a href=\"".FApplication::$router->link(CUChangeNameControler::COMPANY_NAME_CHANGE_PRODUCT_PAGE)."\">Please click here to purchase</a>");
        }
    }

    /**
     * Check if company is in panding
     * @throws Exception
     */
    protected function checkIfCompanyIsPending()
    {
        foreach(ChangeOfName::getSubmissionStatus($this->company->getCompanyId()) as $v) {
            if ($v['response'] == FormSubmission::RESPONSE_PENDING || $v['response'] == FormSubmission::RESPONSE_INTERNAL_FAILURE) {
                throw new Exception("Your name change is currently pending. We will email you as soon as it's done!");
            }
        }
    }

    /**
     * Check if company is in available
     * @return json object
     */
    public function ajaxCheckCompanyAvailability($companyName)
    {
        $available = NULL;
        if(isset($companyName) && !empty($companyName)) {
            try {
                $available = CompanySearch::isAvailable($companyName);
            } catch (Exception $e) {
                $available = NULL;
            }
        }
        //$available = NULL;
        $json = array();
        if($available) {
            $json = array( 'succes' => 1);
        } else {
            $json = array( 'succes' => 0);
        }
        echo json_encode($json);
        exit;
    }

    /**
     * @param string $companyType
     * @return array
     */
    public static function getSuffixOptions($companyType)
    {
        switch ($companyType) {
            case CompanyIncorporation::LIMITED_LIABILITY_PARTHENERSHIP:
                return self::$llpCompanySuffix;
                break;
            case CompanyIncorporation::PUBLIC_LIMITED:
                return self::$plcCompanySuffix;
                break;
            case CompanyIncorporation::LIMITED_BY_SHARES:
                return self::$ltdCompanySuffix;
                break;
            case CompanyIncorporation::BY_GUARANTEE:
                return self::$ltdCompanySuffix;
                break;
            case CompanyIncorporation::BY_GUARANTEE_EXEMPT:
                return self::$ltdCompanySuffix;
                break;
        }
        return NULL;
    }


    /**
     *
     * @param Company $company
     * Return company type MethodOfChange
     * @return string
     */
    public static function setCompanyChangeNameMethodOfChange(Company $company)
    {
        if (strtoupper(trim($company->getType())) == self::LLP) {
            return self::LLP;
        }
        return self::RESOLUTION;
    }


    /**
     *
     * @param Company $company
     * Return company type SameDay
     * @return bool
     */
    public static function setCompanyChangeNameSameDay(Company $company)
    {
        switch ($company->getChangeNameId()) {
            case CompanyNameChange::CHANGE_NAME_SAME_DAY_PRODUCT:
                return TRUE;
                break;
            case CompanyNameChange::CHANGE_NAME_PRODUCT:
                return FALSE;
                break;
        }
    }

    /**
     *
     * @param Company $company
     * Return company type SameDay
     * @return bool
     */
    public static function setAdminCompanyChangeNameSameDay($productNumber)
    {
        switch ($productNumber) {
            case CompanyNameChange::CHANGE_NAME_SAME_DAY_PRODUCT:
                return TRUE;
                break;
            case CompanyNameChange::CHANGE_NAME_PRODUCT:
                return FALSE;
                break;
        }
    }



}
