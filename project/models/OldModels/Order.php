<?php

use Entities\Order as OrderEntity;

class Order extends Object
{
    // status

    const STATUS_NEW = 1;
    const STATUS_CONFIRMED = 2;
    const STATUS_FAILED = 3;
    const STATUS_CANCELLED = 4;

    /** @var array */
    static public $statuses = Array(
        self::STATUS_NEW => 'New',
        self::STATUS_CONFIRMED => 'Confirmed',
        self::STATUS_FAILED => 'Failed',
        self::STATUS_CANCELLED => 'Cancelled'
    );

    /**
     * fields which can be rounded (and used in getRounded method)
     * @var array
     */
    static public $allowedRoundedFields = array('subtotal', 'realSubtotal', 'total', 'vat', 'discount', 'refundValue', 'credit', 'refundCreditValue');

    /** @var int */
    public $orderId;

    /** @var int */
    public $statusId = NULL;

    /** @var string */
    public $status;

    /** @var double */
    public $subtotal;

    /** @var double */
    public $realSubtotal = 0;

    /** @var double */
    public $discount = 0;

    /** @var double */
    public $vat;

    /** @var double */
    public $credit = 0;

    /** @var double */
    public $total;

    /** @var string */
    public $dtc;

    /** @var string */
    public $dtm;

    /** @var string */
    public $customerName;

    /** @var string */
    public $customerAddress;

    /** @var string */
    public $customerPhone;

    /** @var string */
    public $customerEmail;

    /** @var voucher */
    public $voucherId;

    /** @var int */
    public $voucherName;

    /** @var int */
    public $customerId;

    /** @var string */
    public $description;

    /** @var boolean */
    public $isRefunded;

    /** @var float */
    public $refundValue;

    /** @var float */
    public $refundCreditValue;

    /** @var int */
    public $refundCustomerSupportLogin;

    /**
     * @var string
     */
    public $paymentMediumId = OrderEntity::PAYMENT_MEDIUM_ONSITE;

    /**
     * @var int
     */
    public $agentId;

    /** @var array */
    public $items = array();

    /**
     * @var array<Transaction> 
     */
    public $transactions = array();

    /**
     * @return int
     */
    public function getId()
    {
        return( $this->orderId );
    }

    /**
     * @return bool
     */
    public function exists()
    {
        return( $this->orderId > 0 );
    }

    /**
     * @return bool
     */
    public function isNew()
    {
        return( $this->orderId == 0 );
    }

    /**
     * @param int $orderId
     * @return void
     */
    public function __construct($orderId = 0)
    {
        $this->orderId = (int) $orderId;
        if ($this->orderId) {
            $w = FApplication::$db->select('*')->from(TBL_ORDERS)->where('orderId=%i', $this->orderId)->execute()->fetch();
            if ($w) {
                $this->orderId = $w['orderId'];

                $this->statusId = $w['statusId'];
                $this->status = isSet(self::$statuses[$w['statusId']]) ? self::$statuses[$w['statusId']] : NULL;

                $this->customerId = $w['customerId'];
                $this->realSubtotal = $w['realSubtotal'];
                $this->subtotal = $w['subtotal'];
                $this->discount = $w['discount'];
                $this->vat = $w['vat'];
                $this->credit = $w['credit'];
                $this->total = $w['total'];
                $this->dtc = $w['dtc'];
                $this->dtm = $w['dtm'];
                $this->customerName = $w['customerName'];
                $this->customerAddress = $w['customerAddress'];
                $this->customerPhone = $w['customerPhone'];
                $this->customerEmail = $w['customerEmail'];
                $this->voucherId = $w['voucherId'];
                $this->voucherName = $w['voucherName'];
                $this->description = $w['description'];
                $this->isRefunded = (bool) $w['isRefunded'];
                $this->refundValue = $w['refundValue'];
                $this->refundCreditValue = $w['refundCreditValue'];
                $this->refundCustomerSupportLogin = $w['refundCustomerSupportLogin'];
                $this->paymentMediumId = $w['paymentMediumId'];
                $this->agentId = $w['agentId'];
                $this->items = OrderItem::getOrderItems($this->getId());
                $this->transactions = Transaction::getTransaction(NULL, $this->getId());

                // in case of refund - recount
                //				if ($this->isRefunded === TRUE) {
                //					$this->recountOrder();
                //				}
            } else {
                throw new Exception("Order with id `$orderId` doesn't exist.");
            }
        }
    }

    /**
     * @param OrderIteM $item
     * @return void
     */
    public function setItem(OrderItem $item)
    {
        $this->items[] = $item;
    }

    /**
     * Provide saving order to database
     * @return int $id
     */
    public function save()
    {
        $action = $this->orderId ? 'update' : 'insert';

        // data
        $data = array();
        $data['statusId'] = $this->statusId;
        $data['customerId'] = $this->customerId;
        $data['realSubtotal'] = $this->realSubtotal;
        $data['subtotal'] = $this->subtotal;
        $data['discount'] = $this->discount;
        $data['vat'] = $this->vat;
        $data['credit'] = $this->credit;
        $data['total'] = $this->total;
        $data['dtc'] = $this->dtc;
        $data['customerName'] = $this->customerName;
        $data['customerAddress'] = $this->customerAddress;
        $data['customerPhone'] = $this->customerPhone;
        $data['customerEmail'] = $this->customerEmail;
        $data['voucherId'] = $this->voucherId;
        $data['voucherName'] = $this->voucherName;
        $data['description'] = $this->description;
        $data['isRefunded'] = $this->isRefunded;
        $data['refundValue'] = $this->refundValue;
        $data['refundCreditValue'] = $this->refundCreditValue;
        $data['refundCustomerSupportLogin'] = $this->refundCustomerSupportLogin;
        $data['paymentMediumId'] = $this->paymentMediumId;
        $data['agentId'] = $this->agentId;
        $data['dtm'] = new DibiDateTime();

        // insert
        if ($action == 'insert') {
            $data['dtc'] = new DibiDateTime();
            $this->orderId = FApplication::$db->insert(TBL_ORDERS, $data)->execute(dibi::IDENTIFIER);
            // update
        } else {
            FApplication::$db->update(TBL_ORDERS, $data)->where('orderId=%i', $this->orderId)->execute();
        }

        // order items
        foreach ($this->items as $key => $item) {
            $item->orderId = $this->orderId;
            $item->save();
        }

        return $this->orderId;
    }

    /**
     * @return void
     */
    public function delete()
    {
        if ($this->exists()) {
            FApplication::$db->delete(TBL_ORDERS)->where('orderId=%i', $this->orderId)->execute();
        }
    }

    /**
     * Returns orders byt params
     * 
     * @param int $customerId
     * @param int $limit
     * @param int $offset
     * @param array $whereOrders
     * @param array $whereTransactions
     * @return array $orders
     */
    public static function getOrders($customerId = NULL, $limit = NULL, $offset = NULL, array $whereOrders = array(), array $whereTransactions = array())
    {
        $result = FApplication::$db->select('o.orderId')->from(TBL_ORDERS, 'o');

        // customerId
        if ($customerId !== NULL) {
            $result->where('customerId=%i', $customerId); 
        }
        $result = self::addSQLQuery($result, $whereOrders, $whereTransactions);
        $result->orderBy('o.dtc', dibi::DESC);

        // limit
        if ($limit !== NULL) {
            $result->limit($limit); 
        }
        if ($offset !== NULL) {
            $result->offset($offset); 
        }
        $ids = $result->execute()->fetchPairs();

        // orders
        $orders = array();
        foreach ($ids as $key => $val) {
            $orders[$val] = new self($val);
        }

        return $orders;
    }

    /**
     * Returns count of orders based on where conditions
     *
     * @param array $whereOrders
     * @param array $whereTransactions
     * @return int
     */
    public static function getOrdersCount(array $whereOrders = array(), array $whereTransactions = array())
    {
        $result = dibi::select('count(*)')->from(TBL_ORDERS, 'o');
        $result = self::addSQLQuery($result, $whereOrders, $whereTransactions);
        $count = $result->execute()->fetchSingle();
        return $count;
    }

    /**
     * Will provide add where conditions for orders and transactions
     *
     * @param DibiFluent $result
     * @param array $whereOrders
     * @param array $whereTransactions.
     * @return DibiFluent
     */
    private static function addSQLQuery(DibiFluent $result, array $whereOrders = array(), array $whereTransactions = array())
    {
        // add transaction
        if (!empty($whereTransactions)) {
            $result->join(TBL_TRANSACTIONS, 't')->on('t.orderId=o.orderId');
            foreach ($whereTransactions as $key => $val) {
                $result->and('t.%n', $key, ' LIKE %s', "%$val%");
            }
        }

        // where
        if (!empty($whereOrders)) {
            foreach ($whereOrders as $key => $val) {
                $result->where('o.%n', $key, ' = %s', $val);
            }
        }

        return $result;
    }

    /**
     * Adding refund item to items
     * 
     * @param int $itemId
     * @return void
     * @throws Exception
     */
    public function addRefundItems(array $items)
    {
        foreach ($items as $key => $itemId) {
            if (isset($this->items[$itemId])) {

                $item = clone $this->items[$itemId];
                $item->orderItemId = 0;
                $item->isRefund = TRUE;
                $item->qty *= -1;

                if ($item->price != 0) {
                    $item->price *= -1; 
                }
                if ($item->subTotal != 0) {
                    $item->subTotal *= -1; 
                }
                if ($item->vat != 0) {
                    $item->vat *= -1; 
                }
                if ($item->totalPrice != 0) {
                    $item->totalPrice *= -1; 
                }

                $this->setItem($item);

                // mark as refunded
                $this->items[$itemId]->isRefunded = TRUE;
            } else {
                throw new Exception("Order item with id `$itemId` doesn't exist!");
            }
        }
    }

    /**
     * Adding admin fee to order
     * 
     * @param int $itemId
     * @return void
     * @throws Exception
     */
    public function addAdminFeeItem($price)
    {
        $product = new AdminFee(OrderItem::ADMIN_FEE_PRODUCT);
        $item = new OrderItem;
        $item->productTitle = $product->getLngTitle();
        $item->qty = 1;
        $item->price = $price;
        $item->subTotal = $price;
        $item->vat = 0.00;
        $item->totalPrice = $price;
        $item->isFee = TRUE;
        $this->setItem($item);
    }

    /**
     * Return transaction code
     * 
     * @return string
     */
    public function getTransactionCode()
    {
        foreach ($this->transactions as $key => $transaction) {
            if ($transaction->orderCode != NULL) {
                return $transaction->orderCode;
            }
        }
        return NULL;
    }

    /**
     * Return transaction TYPE
     * 
     * @return string
     */
    public function getTransactionType()
    {
        foreach ($this->transactions as $key => $transaction) {
            if ($transaction->orderCode != NULL) {
                return $transaction->type;
            }
        }
        return NULL;
    }

    /**
     * returns real subtotal based on order items (currently order subtotal is with discount applied)
     * @return string 
     */
    public function getRealSubtotal()
    {
        $subtotal = 0;
        if ($this->realSubtotal > 0) {
            $subtotal = $this->realSubtotal;
        } else {
            $productPrice = $this->getOrderItemsPrice('products');
            $subtotal = $productPrice;
        }
        return $subtotal;
    }

    /**
     * get basket items subtotal
     * @param string $only (refunds | fees | products)
     * @return float 
     */
    public function getOrderItemsPrice($only = 'all')
    {
        $subtotal = 0.00;
        foreach ($this->items as $key => $item) {
            if ($only === 'refunds') {
                if ($item->isRefund) {
                    $subtotal += abs($item->qty) * $item->price;
                }
            } elseif ($only === 'fees') {
                if ($item->isFee) {
                    $subtotal += abs($item->qty) * $item->price;
                }
            } elseif ($only === 'products') {
                if (!$item->isRefund && !$item->isFee) {
                    $subtotal += abs($item->qty) * $item->price;
                }
            } else {
                $subtotal += abs($item->qty) * $item->price;
            }
        }
        return $subtotal;
    }

    /**
     * does order has admin
     * @return bool
     */
    public function hasAdminFee()
    {
        foreach ($this->items as $key => $item) {
            if ($item->isFee) {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * get fee amount
     * @param string $rounded
     * @return float | string 
     */
    public function getAdminFee($rounded = 'notrounded')
    {
        $price = $this->getOrderItemsPrice('fees');
        if ($rounded === 'rounded') {
            return Basket::round($price);
        }
        return $price;
    }

    /**
     * get rounded object field (rounded field must be in allowedRoundedFields array)
     * @param string $field
     * @return string 
     */
    public function getRounded($field)
    {
        if (isset($this->{$field}) && in_array($field, self::$allowedRoundedFields, TRUE)) {
            $methodName = 'get' . ucfirst($field);
            if (method_exists($this, $methodName)) {
                return Basket::round($this->$methodName());
            }
            return Basket::round($this->{$field});
        }
        return '';
    }

    /**
     * Will recount order in cas of refund etc.
     *
     * @return void
     */
    public function recountOrder()
    {
        $totalPrice = $subtotal = 0;
        foreach ($this->items as $key => $value) {
            if ($value->isRefund == FALSE && $value->isRefunded == FALSE) {
                $totalPrice += $value->totalPrice;
                $subtotal += abs($value->qty) * $value->price;
            }
        }
        $this->subtotal = $subtotal;
        $this->vat = Basket::round($totalPrice - $subtotal);
        $this->total = $totalPrice;
    }

    /**
     * Returns VoucherNew if any
     *
     * @return VoucherNew
     */
    public function getVoucher()
    {
        if ($this->voucherId) {
            $voucher = new VoucherNew($this->voucherId);
            return $voucher;
        }
        return FALSE;
    }
    
    public function getFeefoData()
    {
        // deduplicate product ids & titles
        $titles = array();
        $productIds = array();
        $link = FALSE;
        foreach ($this->items as $item) {
            if (array_search($item->productId, $productIds) === FALSE) {
                $product = new BasketProduct($item->productId);
                if ($product->isFeefo()) {
                    $titles[] = $product->getLngTitle();
                    $productIds[] = $item->productId;
                    if (!$link) {
                        $link = $product->getLngFriendlyUrl();
                    }
                }
            }
        }
        
        $date = new DateTime($this->dtc);

        return array(
            'email' => $this->customerEmail,
            'name' => trim($this->customerName),
            'date' => $date->format('d/m/Y'),
            'description' => implode(', ', $titles),
            'orderref' => $this->orderId,
            'itemref' => implode(', ', $productIds),
            'customerref' => $this->customerId,
            'link' => 'http://' . FApplication::$config['host'] . '/' . $link,
        );
    }

    /**
     * Check if order has included product in it
     *
     * @param int $orderId
     * @param int $itemId
     * @return boolean
     */
    public static function hasItem($orderId, $itemId)
    {
        $result = dibi::select('*')->from(TBL_ORDERS_ITEMS)->where('orderId=%i', $orderId)->and('productId=%i', $itemId)->execute()->fetch();
        return ($result === FALSE) ? FALSE : TRUE;
    }

    /**
     * Provides remove item from order
     *
     * @param int $orderId
     * @param int $itemId
     * @return boolean
     * @throws Exception
     */
    public static function removeItem($orderId, $itemId)
    {
        $items = OrderItem::getOrderItems($orderId);
        foreach ($items as $_itemId => $item) {
            if ($item->productId == $itemId) {
                $_item = new OrderItem($_itemId);
                $_item->delete();
                return TRUE;
            }
        }
        throw new Exception("Product #$itemId doesn't exist in this order #$orderId");
    }

    public static function getTotalPerProductPerDate($date, $productId)
    {
        $result = dibi::select('SUM(oi.qty)')
                        ->from(TBL_ORDERS_ITEMS . ' oi')
                        ->join(TBL_ORDERS . ' o')->on('o.orderId=oi.orderId')
                        ->where('oi.productId = %i', $productId)
                        ->and('(DATE_FORMAT(o.dtc,"%Y-%m-%d") = %s)', $date)
                        ->execute()->fetchSingle();

        return $result;
    }

    /**
     * get how many products were bought by date
     * @param Customer $customer
     * @param array $productIds
     * @param string $fromDate
     * @return int
     */
    public static function getTotalProductsByDate(Customer $customer, array $productIds, $fromDate)
    {
        $result = dibi::select('SUM(oi.qty)')
                        ->from(TBL_ORDERS_ITEMS . ' oi')
                        ->innerJoin(TBL_ORDERS . ' o')->on('o.orderId=oi.orderId')
                        ->where('oi.productId IN(%i)', $productIds)
                        ->and('o.customerId=%i', $customer->getId())
                        ->and('o.dtc > %s', $fromDate)
                        ->execute()->fetchSingle();
        return (int) $result;
    }
}
