<?php


class ExternalLinkNode extends FNode
{
    /**
     * @var string
     */
    private $url;

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    protected function addCustomData()
    {
        $this->url = $this->getProperty('url');
        parent::addCustomData();
    }

    /**
     * @param string $action
     */
    protected function saveCustomData($action)
    {
        $this->saveProperty('url', $this->url);
        parent::saveCustomData($action);
    }
}
