<?php

class WholesaleSearchNode extends FNode
{
    /**
     * @var array
     */
    private $packagesIds = array();

    /**
     * @return array
     */
    public function getPackagesIds()
    {
        return $this->packagesIds;
    }

    /**
     * @param array $ids
     */
    public function setPackagesIds(array $ids)
    {
        $this->packagesIds = $ids;
    }

    /**
     * @return Package[] array
     */
    public function getPackages()
    {
        $packages = array();
        foreach ($this->getPackagesIds() as $packageId) {
            $packages[$packageId] = FNode::getProductById($packageId);
        }
        return $packages;
    }


    public function addCustomData()
    {
        $packagesIds = $this->getProperty('packagesIds');
        if ($packagesIds != NULL) {
            $this->setPackagesIds(explode('|', $packagesIds));
        }
    }

    /**
     * @param string $action
     */
    public function saveCustomData($action)
    {
        parent::saveCustomData($action);
        if (!empty($this->packagesIds)) {
            $this->saveProperty('packagesIds', implode('|', $this->getPackagesIds()));
        }
    }
}