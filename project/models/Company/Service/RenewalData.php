<?php

namespace Company\Service;

use DateTime;
use Entities\Service;
use Nette\Object;
use Entities\Customer;

/*
   companies => array (1)
   |  207645 => array (2)
   |  |  entity => Entities\Company #da60 { ... }
   |  |  services => array (1) [ ... ]
   customer => CMS\Proxy\__CG__\Entities\Customer #c1cf
   serviceCount => int
*/

class RenewalData extends Object
{

    /**
     * @var array
     */
    private $renewalData;

    /**
     * @param array $renewalData
     */
    public function __construct($renewalData)
    {
        $this->renewalData = $renewalData;
    }

    /**
     * @return int
     */
    public function getServiceCount()
    {
        return $this->renewalData['serviceCount'];
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->renewalData['customer'];
    }

    /**
     * @return array
     */
    public function getCompanies()
    {
        return $this->renewalData['companies'];
    }

    /**
     * @return Service[]
     */
    public function getCustomerServices()
    {
        $services = array();

        foreach ($this->renewalData['companies'] as $companyData) {
            foreach ($companyData['services'] as $service) {
                $services[] = $service;
            }
        }

        return $services;
    }

    /**
     * @return DateTime|NULL
     */
    public function getEarliestServiceExpiryDate()
    {
        $date = NULL;

        foreach ($this->getCustomerServices() as $service) {
            if (!$date || $service->getDtExpires() < $date) {
                $date = $service->getDtExpires();
            }
        }

        return $date;
    }

    /**
     * @return int
     */
    public function getLowestServiceDaysToExpire()
    {
        $days = 0;

        foreach ($this->getCustomerServices() as $service) {
            if (!$days || $service->getDaysToExpire() < $days) {
                $days = $service->getDaysToExpire();
            }
        }

        return $days;
    }
}
