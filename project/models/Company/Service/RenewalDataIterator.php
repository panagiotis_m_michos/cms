<?php

namespace Company\Service;

use Doctrine\ORM\Internal\Hydration\IterableResult;
use Entities\Service;
use Iterator;

class RenewalDataIterator implements Iterator
{

    /**
     * @var IterableResult
     */
    private $renewalDataIterableResult;

    /**
     * @var RenewalData
     */
    private $currentCustomerRenewalData = NULL;

    /**
     * @var int
     */
    private $key = 0;

    /**
     * @var bool
     */
    private $fetchNext = FALSE;

    /**
     * @param IterableResult $renewalDataIterableResult
     */
    public function __construct(IterableResult $renewalDataIterableResult)
    {
        $this->renewalDataIterableResult = $renewalDataIterableResult;
    }

    /**
     * @return RenewalData
     */
    public function current()
    {
        if ($this->fetchNext) {
            $this->currentCustomerRenewalData = $this->fetchNext();
            $this->fetchNext = FALSE;
        }

        return $this->currentCustomerRenewalData;
    }

    public function next()
    {
        $this->fetchNext = TRUE;
        $this->key++;
    }

    /**
     * @return int
     */
    public function key()
    {
        return $this->key;
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return $this->renewalDataIterableResult->valid();
    }

    public function rewind()
    {
        $this->renewalDataIterableResult->rewind();
        $this->fetchNext = TRUE;
        $this->key = 0;
    }

    /**
     * @return RenewalData
     */
    private function fetchNext()
    {
        $lastCustomerId = NULL;
        $customerRenewalData = array();
        $serviceCount = 0;

        while ($this->renewalDataIterableResult->valid()) {
            $row = $this->renewalDataIterableResult->current();

            /** @var Service $service */
            $service = $row[0];
            $company = $service->getCompany();
            $customer = $company->getCustomer();
            $serviceCount++;

            if ($lastCustomerId && $customer->getCustomerId() != $lastCustomerId) {
                break;
            }

            $customerRenewalData['companies'][$company->getCompanyId()]['entity'] = $company;
            $customerRenewalData['companies'][$company->getCompanyId()]['services'][] = $service;

            $customerRenewalData['customer'] = $customer;
            $customerRenewalData['serviceCount'] = $serviceCount;

            $lastCustomerId = $customer->getCustomerId();
            $this->renewalDataIterableResult->next();
        }

        return new RenewalData($customerRenewalData);
    }
}
