<?php

namespace Export\Order;

use AppendIterator;
use DibiConnection;
use DibiFluent;
use DibiResultIterator;
use Iterator;
use Nette\Object;

class Fetcher extends Object
{
    /**
     * @var DibiConnection
     */
    private $connection;

    /**
     * @param DibiConnection $connection
     */
    public function __construct(DibiConnection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param Filter $filter
     * @return Iterator
     */
    public function getIterator(Filter $filter)
    {
        $appendIterator = new AppendIterator();
        $appendIterator->append(
            $this->getIncorporationNotRequiredIterator($filter)
        );
        $appendIterator->append(
            $this->getIncorporationRequiredNotPackageAndNotBelongsToPackageIterator($filter)
        );
        $appendIterator->append(
            $this->getIncorporationRequiredNotPackageAndBelongsToPackageIterator($filter)
        );
        $appendIterator->append(
            $this->getIncorporationRequiredAndPackageIterator($filter)
        );
        return $appendIterator;
    }

    /**
     * @param Filter $filter
     * @return DibiResultIterator
     */
    public function getIncorporationNotRequiredIterator(Filter $filter)
    {
        $sql = $this->connection->select('oi.orderItemId')
            ->select('o.orderId')
            ->select('co.incorporation_date')
            ->select('oi.productTitle')
            ->select('oi.productId')
            ->select('oi.companyNumber')->as('company_number')
            ->select('o.dtc')
            ->select('oi.companyName')->as('company_name')
            ->select('c.customerId')
            ->select('c.firstName')
            ->select('c.lastName')
            ->select('c.address1')
            ->select('c.address2')
            ->select('c.address3')
            ->select('c.city')
            ->select('c.county')
            ->select('c.postcode')
            ->select('c.countryId')
            ->select('c.email')
            ->select('c.phone')
            ->select('c.howHeardId')
            ->select('c.industryId')
            ->select('oi.notApplyVat')
            ->select('oi.nonVatableValue')
            ->select('oi.qty')
            ->select('oi.subTotal')
            ->select('oi.vat')->as('tax')
            ->select('oi.totalPrice')
            ->select('co.company_id')->as('companyId')
            ->from(TBL_ORDERS_ITEMS . ' oi')
            ->join(TBL_ORDERS . ' o')->on('o.orderId=oi.orderId')
            ->leftJoin('ch_company' . ' co')->on('oi.orderId=co.order_id')
            ->join(TBL_CUSTOMERS . ' c')->on('o.customerId=c.customerId')
            ->where('oi.incorporationRequired = FALSE')
            ->and('oi.productId != %i', $filter->getBarclaysBankingProductId())
            ->and('(oi.isRefund = FALSE OR oi.isRefund IS NULL)')
            ->and('(oi.isRefunded = FALSE OR oi.isRefunded IS NULL)')
            ->and('oi.refundedOrderItemId IS NULL');
        $this->addIdCheckCondition($filter, $sql);
        $this->addFilterCondition($sql, $filter);
        return $sql->getIterator();
    }

    /**
     * @param Filter $filter
     * @return DibiResultIterator
     */
    public function getIncorporationRequiredNotPackageAndNotBelongsToPackageIterator(Filter $filter)
    {
        $sql = $this->connection->select('oi.orderItemId')
            ->select('o.orderId')
            ->select('co.incorporation_date')
            ->select('oi.productTitle')
            ->select('oi.productId')
            ->select('co.company_number')
            ->select('o.dtc')
            ->select('co.company_name')
            ->select('c.customerId')
            ->select('c.firstName')
            ->select('c.lastName')
            ->select('c.address1')
            ->select('c.address2')
            ->select('c.address3')
            ->select('c.city')
            ->select('c.county')
            ->select('c.postcode')
            ->select('c.countryId')
            ->select('c.email')
            ->select('c.phone')
            ->select('c.howHeardId')
            ->select('c.industryId')
            ->select('oi.notApplyVat')
            ->select('oi.nonVatableValue')
            ->select('oi.qty')
            ->select('oi.subTotal')
            ->select('oi.vat')->as('tax')
            ->select('oi.totalPrice')
            ->select('co.company_id')->as('companyId')
            ->from(TBL_ORDERS_ITEMS . ' oi')
            ->join(TBL_ORDERS . ' o')->on('o.orderId=oi.orderId')
            ->join('ch_company' . ' co')->on('(oi.companyId=co.company_id AND o.orderId != co.order_id)')
            ->join(TBL_CUSTOMERS . ' c')->on('o.customerId=c.customerId')
            ->where('co.company_number IS NOT NULL')
            ->and('oi.productId NOT IN %in', $filter->getPackagesIds()) // not package
            ->and('oi.incorporationRequired = TRUE') // incorporation required
            ->and('(oi.isRefund = FALSE OR oi.isRefund IS NULL)')
            ->and('(oi.isRefunded = FALSE OR oi.isRefunded IS NULL)')
            ->and('oi.refundedOrderItemId IS NULL');
        $this->addIdCheckCondition($filter, $sql);
        $this->addFilterCondition($sql, $filter);
        return $sql->getIterator();
    }

    /**
     * @param Filter $filter
     * @return DibiResultIterator
     */
    public function getIncorporationRequiredNotPackageAndBelongsToPackageIterator(Filter $filter)
    {
        $sql = $this->connection->select('oi.orderItemId')
            ->select('o.orderId')
            ->select('co.incorporation_date')
            ->select('oi.productTitle')
            ->select('oi.productId')
            ->select('co.company_number')
            ->select('o.dtc')
            ->select('co.company_name')
            ->select('c.customerId')
            ->select('c.firstName')
            ->select('c.lastName')
            ->select('c.address1')
            ->select('c.address2')
            ->select('c.address3')
            ->select('c.city')
            ->select('c.county')
            ->select('c.postcode')
            ->select('c.countryId')
            ->select('c.email')
            ->select('c.phone')
            ->select('c.howHeardId')
            ->select('c.industryId')
            ->select('oi.notApplyVat')
            ->select('oi.nonVatableValue')
            ->select('oi.qty')
            ->select('oi.subTotal')
            ->select('oi.vat')->as('tax')
            ->select('oi.totalPrice')
            ->select('co.company_id')->as('companyId')
            ->from(TBL_ORDERS_ITEMS . ' oi')
            ->join(TBL_ORDERS . ' o')->on('o.orderId=oi.orderId')
            ->join('ch_company' . ' co')->on('oi.orderId=co.order_id AND oi.companyId=co.company_id')
            ->join(TBL_CUSTOMERS . ' c')->on('o.customerId=c.customerId')
            ->where('co.company_number IS NOT NULL')
            ->and('oi.productId NOT IN %l', $filter->getPackagesIds())
            ->and('oi.incorporationRequired = TRUE')
            ->and('(oi.isRefund = FALSE OR oi.isRefund IS NULL)')
            ->and('(oi.isRefunded = FALSE OR oi.isRefunded IS NULL)')
            ->and('oi.refundedOrderItemId IS NULL');
        $this->addIdCheckCondition($filter, $sql);
        $this->addFilterCondition($sql, $filter);
        return $sql->getIterator();
    }

    /**
     * @param Filter $filter
     * @return DibiResultIterator
     */
    public function getIncorporationRequiredAndPackageIterator(Filter $filter)
    {
        $sql = $this->connection->select('oi.orderItemId')
            ->select('o.orderId')
            ->select('co.incorporation_date')
            ->select('oi.productTitle')
            ->select('oi.productId')
            ->select('co.company_number')
            ->select('o.dtc')
            ->select('co.company_name')
            ->select('c.customerId')
            ->select('c.firstName')
            ->select('c.lastName')
            ->select('c.address1')
            ->select('c.address2')
            ->select('c.address3')
            ->select('c.city')
            ->select('c.county')
            ->select('c.postcode')
            ->select('c.countryId')
            ->select('c.email')
            ->select('c.phone')
            ->select('c.howHeardId')
            ->select('c.industryId')
            ->select('oi.notApplyVat')
            ->select('oi.nonVatableValue')
            ->select('oi.qty')
            ->select('oi.subTotal')
            ->select('oi.vat')->as('tax')
            ->select('oi.totalPrice')
            ->select('co.company_id')->as('companyId')
            ->from(TBL_ORDERS_ITEMS . ' oi')
            ->join(TBL_ORDERS . ' o')->on('o.orderId=oi.orderId')
            ->join('ch_company' . ' co')->on('oi.orderId=co.order_id')
            ->join(TBL_CUSTOMERS . ' c')->on('o.customerId=c.customerId')
            ->where('co.company_number IS NOT NULL')
            ->and('oi.productId IN %l', $filter->getPackagesIds())
            ->and('oi.incorporationRequired = TRUE')
            ->and('(oi.isRefund = FALSE OR oi.isRefund IS NULL)')
            ->and('(oi.isRefunded = FALSE OR oi.isRefunded IS NULL)')
            ->and('oi.refundedOrderItemId IS NULL');
        $this->addIdCheckCondition($filter, $sql);
        $this->addFilterCondition($sql, $filter);
        return $sql->getIterator();
    }

    /**
     * @param Filter $filter
     * @param DibiFluent $sql
     */
    private function addIdCheckCondition(Filter $filter, DibiFluent $sql)
    {
        $idCheckRequiredProductsIds = $filter->getIdCheckRequiredProductsIds();
        if ($idCheckRequiredProductsIds) {
            $sql->and(
                '((oi.productId IN %in AND (c.isIdCheckRequired = 1 AND c.dtIdValidated IS NOT NULL) OR c.isIdCheckRequired = 0) OR oi.productId NOT IN %in)',
                $idCheckRequiredProductsIds,
                $idCheckRequiredProductsIds
            );
        } else {
            $sql->and('((c.isIdCheckRequired = 1 AND c.dtIdValidated IS NOT NULL) OR c.isIdCheckRequired = 0)');
        }
    }

    /**
     * @param DibiFluent $sql
     * @param Filter $filter
     */
    private function addFilterCondition(DibiFluent $sql, Filter $filter)
    {
        // excluded products
        if ($filter->getExcludedProductIds()) {
            $sql->and('oi.productId NOT IN %in', $filter->getExcludedProductIds());
        }

        // dt exported
        $datesRange = $filter->getDatesRange();
        if ($datesRange->hasDates()) {
            if ($datesRange->getStart()) {
                $sql->and('DATE(oi.dtExported) >= %s', $datesRange->getStart()->format('Y-m-d'));
            }
            if ($datesRange->getEnd()) {
                $sql->and('DATE(oi.dtExported) <= %s', $datesRange->getEnd()->format('Y-m-d'));
            }
        } else {
            $sql->and('oi.dtExported IS NULL');
        }
    }
}
