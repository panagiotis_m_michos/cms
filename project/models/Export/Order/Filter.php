<?php

namespace Export\Order;

use FormModule\Model\DatesRange;
use Nette\Object;

class Filter extends Object
{
    /**
     * @var array
     */
    private $excludedProductIds = [];

    /**
     * @var array
     */
    private $idCheckRequiredProductsIds = [];

    /**
     * @var array
     */
    private $packagesIds;

    /**
     * @var int
     */
    private $barclaysBankingProductId;

    /**
     * @var DatesRange
     */
    private $datesRange;

    /**
     * @param array $excludedProductIds
     * @param array $idCheckRequiredProductsIds
     * @param array $packagesIds
     * @param int $barclaysBankingProductId
     */
    public function __construct(array $excludedProductIds, array $idCheckRequiredProductsIds, array $packagesIds, $barclaysBankingProductId)
    {
        $this->excludedProductIds = $excludedProductIds;
        $this->idCheckRequiredProductsIds = $idCheckRequiredProductsIds;
        $this->packagesIds = $packagesIds;
        $this->barclaysBankingProductId = (int) $barclaysBankingProductId;
    }

    /**
     * @return array
     */
    public function getPackagesIds()
    {
        return $this->packagesIds;
    }

    /**
     * @return int
     */
    public function getBarclaysBankingProductId()
    {
        return $this->barclaysBankingProductId;
    }

    /**
     * @return array
     */
    public function getIdCheckRequiredProductsIds()
    {
        return $this->idCheckRequiredProductsIds;
    }

    /**
     * @return array
     */
    public function getExcludedProductIds()
    {
        return $this->excludedProductIds;
    }

    /**
     * @return DatesRange
     */
    public function getDatesRange()
    {
        return $this->datesRange;
    }

    /**
     * @param DatesRange $datesRange
     */
    public function setDatesRange(DatesRange $datesRange)
    {
        $this->datesRange = $datesRange;
    }

    /**
     * @return bool
     */
    public function hasDates()
    {
        return $this->datesRange->hasDates();
    }
}