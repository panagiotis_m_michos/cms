<?php

namespace Export\Order;

use DateTime;
use DibiConnection;
use Doctrine\Common\Collections\ArrayCollection;
use Goodby\CSV\Export\Standard\Collection\CallbackCollection;
use Goodby\CSV\Export\Standard\Exporter;
use Nette\Object;

class CsvStream extends Object
{
    /**
     * @var Fetcher
     */
    private $fetcher;

    /**
     * @var DibiConnection
     */
    private $connection;

    /**
     * @var Exporter
     */
    private $exporter;

    /**
     * @param Fetcher $fetcher
     * @param DibiConnection $connection
     * @param Exporter $exporter
     */
    public function __construct(Fetcher $fetcher, DibiConnection $connection, Exporter $exporter)
    {
        $this->fetcher = $fetcher;
        $this->connection = $connection;
        $this->exporter = $exporter;
    }

    /**
     * @param Filter $filter
     */
    public function stream(Filter $filter)
    {
        $orderItemIds = new ArrayCollection();
        $iterator = $this->fetcher->getIterator($filter);

        $collection = new CallbackCollection(
            $iterator,
            function ($row) use ($orderItemIds) {
                $orderItemIds->add($row['orderItemId']);
                return (array) $row;
            }
        );

        $this->logExport($collection);
        $this->exporter->export('php://output', $collection);

        // mark as exported
        if (!$filter->hasDates()) {
            $this->connection->update(TBL_ORDERS_ITEMS, ['dtExported' => new Datetime()])->where('[orderItemId] IN %in', $orderItemIds->toArray())->execute();
        }

    }

    private function logExport($collection)
    {
        $exportDir = LOG_DIR . DIRECTORY_SEPARATOR . 'order_exports';

        if (!is_dir($exportDir)) {
            mkdir($exportDir);
        }

        $this->exporter->export($exportDir . DIRECTORY_SEPARATOR . date('YmdHis') . '.csv', $collection);
    }
}
