<?php

namespace Export\Order;

use Goodby\CSV\Export\Standard\Exporter;
use Goodby\CSV\Export\Standard\ExporterConfig;
use Nette\Object;

class CsvExporterFactory extends Object
{
    /**
     * @var array
     */
    public static $header = array(
        'orderItemId',
        'orderId',
        'incorporation_date',
        'productTitle',
        'productId',
        'company_number',
        'dtc',
        'company_name',
        'customerId',
        'firstName',
        'lastName',
        'address1',
        'address2',
        'address3',
        'city',
        'county',
        'postcode',
        'countryId',
        'email',
        'phone',
        'howHeardId',
        'industryId',
        'notApplyVat',
        'nonVatableValue',
        'qty',
        'subTotal',
        'tax',
        'totalPrice',
        'companyId'
    );

    /**
     * @return Exporter
     */
    public static function createExporter()
    {
        $config = new ExporterConfig();
        $config->setColumnHeaders(self::$header);
        return new Exporter($config);
    }
}