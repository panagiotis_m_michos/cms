<?php

namespace Import\Common\Importer;

use CompaniesHouse\Data\Data;
use DateTime;
use Entities\Company;
use Entities\Customer;
use Entities\Order;
use Entities\OrderItem;
use Entities\Transaction;
use ErrorException;
use FTools;
use Nette\Object;
use Product;

abstract class Importer_Abstract extends Object
{
    /**
     * @var Data
     */
    private $chData;

    /**
     * @param Data $chData
     */
    public function __construct(Data $chData)
    {
        $this->chData = $chData;
    }


    /**
     * @param string $companyNumber
     * @param Customer $customer
     * @param Product $product
     * @return Company
     */
    protected function createCompany($companyNumber, Customer $customer, Product $product)
    {
        $companyDetails = $this->chData->getCompanyDetails($companyNumber);

        $company = new Company($customer, $companyDetails->getCompanyName());
        $company->setCompanyNumber($companyDetails->getCompanyNumber());
        $company->setCompanyStatus($companyDetails->getCompanyStatus());
        $company->setRegisteredOfficeId($product->getId());
        $company->setCountryOfOrigin($companyDetails->getCountryOfOrigin());
        $company->setAccountsRefDate($companyDetails->getAccounts()->getAccountRefDay() . '-' . $companyDetails->getAccounts()->getAccountRefMonth());

        $incorporationDate = $companyDetails->getIncorporationDate();
        if ($incorporationDate instanceof DateTime) {
            $company->setIncorporationDate($incorporationDate);
        }

        $accountsNextDueDate = $companyDetails->getAccounts()->getNextDueDate();
        if ($accountsNextDueDate instanceof DateTime) {
            $company->setAccountsNextDueDate($accountsNextDueDate);
        }

        $accountLastMadeUpDate = $companyDetails->getAccounts()->getLastMadeUpDate();
        if ($accountLastMadeUpDate instanceof DateTime) {
            $company->setAccountsLastMadeUpDate($accountLastMadeUpDate);
        }

        $returnsLastMadeUpDate = $companyDetails->getReturns()->getLastMadeUpDate();
        if ($returnsLastMadeUpDate instanceof DateTime) {
            $company->setReturnsLastMadeUpDate($returnsLastMadeUpDate);
        }

        $returnsNextDueDate = $companyDetails->getReturns()->getNextDueDate();
        if ($returnsNextDueDate instanceof DateTime) {
            $company->setReturnsNextDueDate($returnsNextDueDate);
        }

        $company->setCareOfName($companyDetails->getRegAddress()->getCareOf());
        $company->setPoBox($companyDetails->getRegAddress()->getPobox());
        $company->setIsCertificatePrinted(TRUE);
        $company->setIsBronzeCoverLetterPrinted(TRUE);
        $company->setIsMaPrinted(TRUE);
        $company->setIsMaCoverLetterPrinted(TRUE);

        return $company;
    }

    /**
     * @param Company $company
     * @param Product $product
     * @return Order
     */
    protected function createOrder(Company $company, Product $product)
    {
        $order = $this->createOrderEntity($company, $product);
        $order->setRealSubTotal(0);
        $order->setSubTotal(0);
        $order->setVat(0);
        $order->setTotal(0);

        $orderItem = $this->createOrderItemEntity($order, $company, $product);
        $orderItem->setPrice(0);
        $orderItem->setSubTotal(0);
        $orderItem->setVat(0);
        $orderItem->setTotalPrice(0);

        $order->addItem($orderItem);

        return $order;
    }

    /**
     * @param Company $company
     * @param Product $product
     * @param $realOrderId
     * @return Order
     */
    protected function createFakeOrder(Company $company, Product $product, $realOrderId)
    {
        $order = $this->createOrderEntity($company, $product);
        $order->setRealSubTotal(0);
        $order->setSubTotal(0);
        $order->setVat(0);
        $order->setTotal(0);
        $order->setDescription(
            "Created by import command - Real order is: ' . $realOrderId . ' (this was created so services work correctly)"
        );

        $orderItem = $this->createOrderItemEntity($order, $company, $product);
        $orderItem->setPrice(0);
        $orderItem->setSubTotal(0);
        $orderItem->setVat(0);
        $orderItem->setTotalPrice(0);

        $order->addItem($orderItem);

        return $order;
    }

    /**
     * @param Company $company
     * @param Product $product
     * @return Order
     */
    protected function createOrderEntity(Company $company, Product $product)
    {
        $customer = $company->getCustomer();
        $order = new Order($customer);
        $order->setRealSubTotal($product->getPrice());
        $order->setSubTotal($product->getPrice());
        $order->setVat($product->getVat());
        $order->setTotal($product->getTotalVatPrice());
        $order->setDescription('Created by import command');

        return $order;
    }

    /**
     * @param Order $order
     * @param Company $company
     * @param Product $product
     * @return OrderItem
     */
    protected function createOrderItemEntity(Order $order, Company $company, Product $product)
    {
        $orderItem = new OrderItem($order);
        $orderItem->setCompany($company);
        $orderItem->setProductId($product->getId());
        $orderItem->setProductTitle("{$product->getLongTitle()} for company {$company->getCompanyName()}");
        $orderItem->setPrice($product->getPrice());
        $orderItem->setQty(1);
        $orderItem->setSubTotal($product->getPrice());
        $orderItem->setVat($product->getVat());
        $orderItem->setTotalPrice($product->getTotalVatPrice());
        $orderItem->setIsFee(FALSE);
        $orderItem->setNotApplyVat(FALSE);
        $orderItem->setNonVatableValue(0);

        return $orderItem;
    }

    /**
     * @param Order $order
     * @return Transaction
     */
    protected function createTransaction(Order $order)
    {
        $transaction = new Transaction($order->getCustomer(), Transaction::TYPE_FREE);
        $transaction->setOrder($order);
        $transaction->setDtc(new DateTime());

        return $transaction;
    }

    /**
     * @param string $email
     * @return Customer
     */
    protected  function createCustomer($email)
    {
        return new Customer($email, FTools::generPwd(8));
    }
} 
