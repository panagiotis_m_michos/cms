<?php

namespace Import\Products\ComprehensiveUltimatePackage\Importer;

use CompaniesHouse\Data\Data;
use Doctrine\ORM\EntityManager;
use Entities\Company;
use Entities\Order;
use Entities\Service;
use Exception;
use Import\Common\Importer\Importer_Abstract;
use Import\Products\ComprehensiveUltimatePackage\Parser\Item;
use InvalidArgumentException;
use Nette\Utils\DateTime;
use Product;
use Services\CompanyService;
use Services\CustomerService;
use Services\OrderService;
use CSVParser\Items\Items;
use Utils\Date;

class Importer extends Importer_Abstract
{
    const VALIDATE_ONLY = TRUE;

    /**
     * @var Item[]
     */
    private $items;

    /**
     * @var array
     */
    private $errors = array();

    /**
     * @var array
     */
    private $skippedDuplicates = array();

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var OrderService
     */
    private $orderService;

    /**
     * @var CustomerService
     */
    private $customerService;

    /**
     * @var array
     */
    private $productCache = array();

    /**
     * @param Item[] $items
     * @param EntityManager $entityManager
     * @param CompanyService $companyService
     * @param OrderService $orderService
     * @param CustomerService $customerService
     * @param Data $chData
     */
    public function __construct(
        Items $items,
        EntityManager $entityManager,
        CompanyService $companyService,
        OrderService $orderService,
        CustomerService $customerService,
        Data $chData
    )
    {
        parent::__construct($chData);
        $this->items = $items;
        $this->entityManager = $entityManager;
        $this->companyService = $companyService;
        $this->orderService = $orderService;
        $this->customerService = $customerService;
    }

    /**
     * @return array
     */
    public function validate()
    {
        $this->import(self::VALIDATE_ONLY);
        return $this->errors;
    }

    /**
     * @param bool $validateOnly
     */
    public function import($validateOnly = FALSE)
    {
        $this->skippedDuplicates = array();

        $batchSize = 50;
        $batchCount = $errors = 0;
        $line = 0;

        $registeredOfficeProduct = Product::getProductById(165); //@todo: find a cleaner way to get products
        $serviceAddressProduct = Product::getProductById(475);

        echo "\n";

        /* @var Item $item */
        foreach ($this->items as $item) {
            try {
                $line++;
                $action = $validateOnly ? 'Validating' : 'Processing';
                echo "$action item $line ({$item->getCompanyNumber()})!\n";

                $itemCompanyId = $item->getCompanyId();
                $itemCustomerId = $item->getCustomerId();
                $itemCompanyNumber = str_pad($item->getCompanyNumber(), 8, 0, STR_PAD_LEFT);
                $itemOrderId = $item->getOrderId();
                $itemProductId = $item->getProductId();
                $itemRenewalProductId = $item->getRenewalProductId();

                $product = $this->getProduct($itemProductId);
                $renewalProduct = $this->getProduct($itemRenewalProductId);

                if (!$itemCompanyNumber) {
                    throw new InvalidArgumentException('Company number ' . $itemCompanyNumber . ' is missing in CSV');
                }

                // if company is not found in our database, create a new one from company number
                $company = $this->companyService->getCompanyByCompanyNumber($itemCompanyNumber);

                //try match companyId if the company number was not found (it could be empty)
                if (!$company) {
                    $company = $this->companyService->getCompanyById($itemCompanyId);
                }

                if (!$company) {
                    $customer = $this->customerService->getCustomerById($item->getCustomerId());
                    if (!$customer) {
                        $email = $item->getCustomerEmail();
                        $customer = $this->customerService->getCustomerByEmail($email);
                        if (!$customer) {
                            $customer = $this->createCustomer($email);
                        }
                    }
                    $product = $this->getProduct($item->getProductId());
                    $company = $this->createCompany($itemCompanyNumber, $customer, $product);
                }

                $transaction = NULL;
                if (!$itemOrderId) {
                    // if orderId is missing in CSV, create new order
                    $order = $this->createOrder($company, $product);
                    $transaction = $this->createTransaction($order);
                } else {
                    $order = $this->orderService->getOrderById($itemOrderId);
                    if (!$order) {
                        throw new InvalidArgumentException('OrderId ' . $itemOrderId . ' does not exist in our database');
                    } elseif ($order->getCustomer()->getCustomerId() != $itemCustomerId) {
                        // if order was created by different account, create a fake one with original ID in description
                        $order = $this->createFakeOrder($company, $product, $order->getOrderId());
                        $transaction = $this->createTransaction($order);
                    }
                }

                $service = $this->getService(
                    $item,
                    Service::TYPE_PACKAGE_COMPREHENSIVE_ULTIMATE,
                    $product,
                    $company,
                    $order,
                    $renewalProduct
                );

                if ($company->hasEqualService($service)) {
                    throw new Exception("Line {$line}: Skipping Service {$service->getServiceTypeId()} for company id {$company->getCompanyId()} with same data already exists in our database");
                }

                if (!$validateOnly) {
                    //if company has any active SA or RO services, expire them upon package activation
                    $types = array(
                        Service::TYPE_REGISTERED_OFFICE,
                        Service::TYPE_SERVICE_ADDRESS,
                        Service::TYPE_PACKAGE_PRIVACY,
                        Service::TYPE_PACKAGE_COMPREHENSIVE_ULTIMATE,
                    );

                    $activeServices = $company->getServices()->filter(
                        function(Service $service) use ($types) {
                            return !$service->isExpired() && in_array($service->getServiceTypeId(), $types);
                        }
                    );

                    /** @var $activeService Service */
                    foreach ($activeServices as $activeService) {
                        //if customer already had this package, add the remaining length to the new one
                        if ($activeService->getServiceTypeId() == Service::TYPE_PACKAGE_COMPREHENSIVE_ULTIMATE
                            && $activeService->getDtStart() != NULL
                            && $activeService->getDtExpires() != NULL
                        ) {
                            $today = new DateTime();

                            if ($today < $activeService->getDtExpires()) {
                                $diff = $today->diff($activeService->getDtExpires());
                                $expiry =  $item->getDtExpires()->add($diff);
                                $expiryDate = new Date($expiry->format('Y-m-d'));
                                $item->setDtExpires($expiryDate);
                                $service->setDtExpires($expiryDate);
                            }
                        }

                        $activeService->setDtExpires($item->getDtStart());
                        $activeService->setParent(NULL);
                        $this->entityManager->persist($activeService);
                    }

                    //Add the RO service
                    $registeredOfficeService = $this->getService(
                        $item,
                        Service::TYPE_REGISTERED_OFFICE,
                        $registeredOfficeProduct,
                        $company,
                        $order
                    );
                    $registeredOfficeService->setParent($service);

                    //Add the SA service
                    $serviceAddressService = $this->getService(
                        $item,
                        Service::TYPE_SERVICE_ADDRESS,
                        $serviceAddressProduct,
                        $company,
                        $order
                    );
                    $serviceAddressService->setParent($service);

                    //persist services (cascades) and transaction
                    $this->entityManager->persist($service);
                    $this->entityManager->persist($registeredOfficeService);
                    $this->entityManager->persist($serviceAddressService);
                    if ($transaction) {
                        $this->entityManager->persist($transaction);
                    }
                } else {
                    //upon validation persist company
                    $this->entityManager->persist($company);
                }

                $batchCount++;
                if ($batchCount % $batchSize === 0) {
                    $batchCount = 0;
                    $this->flushUpdates();
                }


            } catch (Exception $e) {
                $batchCount++;
                $this->errors[] = "Line {$batchCount}: {$e->getMessage()}";
            }
        }

        $this->flushUpdates();
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    public function getSkippedDuplicates()
    {
        return $this->skippedDuplicates;
    }

    private function flushUpdates()
    {
        $this->entityManager->flush();
        $this->entityManager->clear();
    }

    /**
     * Get cached product
     *
     * @param $productId
     * @return mixed
     * @throws InvalidArgumentException
     */
    private function getProduct($productId)
    {
        if (!isset($this->productCache[$productId])) {
            $this->productCache[$productId] = Product::getProductById($productId);
        }

        if (!$this->productCache[$productId]) {
            throw new InvalidArgumentException('Product with id ' . $productId . ' does not exist');
        }

        return $this->productCache[$productId];
    }

    /**
     * @param Item $item
     * @param string $serviceType
     * @param Product $product
     * @param Company $company
     * @param Order $order
     * @param Product $renewalProduct
     * @return Service
     */
    private function getService(Item $item, $serviceType, Product $product, Company $company, Order $order, Product $renewalProduct = NULL)
    {
        $service = new Service(
            $serviceType,
            $product,
            $company,
            $order
        );

        $service->setRenewalProduct($renewalProduct);
        $service->setDtStart($item->getDtStart());
        $service->setDtExpires($item->getDtExpires());
        $service->setDtc($item->getDtc());
        $service->setDtm($item->getDtc());

        return $service;
    }
}
