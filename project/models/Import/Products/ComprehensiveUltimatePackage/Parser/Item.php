<?php

namespace Import\Products\ComprehensiveUltimatePackage\Parser;

use CSVParser\Annotations\Column;
use CSVParser\Item\IItem;
use DateTime;
use Nette\Object;
use Utils\Date;

class Item extends Object implements IItem
{
    /**
     * @Column(position=1, type="int")
     * @var int
     */
    private $productId;

    /**
     * @Column(position=2, type="int")
     * @var int
     */
    private $orderId;

    /**
     * @Column(position=3, type="int")
     * @var int
     */
    private $companyId;

    /**
     * @Column(position=4, type="int")
     * @var int
     */
    private $customerId;

    /**
     * @Column(position=5, type="int")
     * @var int
     */
    private $renewalProductId;

    /**
     * @Column(position=6, type="string")
     * @var string
     */
    private $companyName;

    /**
     * @Column(position=7, type="string")
     * @var string
     */
    private $companyNumber;

    /**
     * @Column(position=8, type="string")
     * @var string
     */
    private $customerEmail;

    /**
     * @Column(position=9, type="string")
     * @var int
     */
    private $serviceName;

    /**
     * @Column(position=10, type="date", dateFormat="d/m/Y")
     * @var Date
     */
    private $dtStart;

    /**
     * @Column(position=11, type="date", dateFormat="d/m/Y")
     * @var Date
     */
    private $dtExpires;

    /**
     * @Column(position=12, type="datetime", dateFormat="d/m/Y")
     * @var DateTime
     */
    private $dtc;

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @return int
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * @return int
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @return int
     */
    public function getRenewalProductId()
    {
        return $this->renewalProductId;
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @return string
     */
    public function getCompanyNumber()
    {
        return $this->companyNumber;
    }

    /**
     * @return string
     */
    public function getCustomerEmail()
    {
        return $this->customerEmail;
    }

    /**
     * @return int
     */
    public function getServiceName()
    {
        return $this->serviceName;
    }

    /**
     * @return Date
     */
    public function getDtStart()
    {
        return $this->dtStart;
    }

    /**
     * @return Date
     */
    public function getDtExpires()
    {
        return $this->dtExpires;
    }

    /**
     * @return DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @param Date $date
     */
    public function setDtExpires(Date $date)
    {
        $this->dtExpires = $date;
    }
}
