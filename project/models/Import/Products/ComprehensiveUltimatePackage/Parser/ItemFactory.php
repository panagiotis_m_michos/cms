<?php

namespace Import\Products\ComprehensiveUltimatePackage\Parser;

use CSVParser\Item\IItemFactory;

class ItemFactory implements IItemFactory
{
    /**
     * @return Item
     */
    public function create()
    {
        return new Item();
    }
}
