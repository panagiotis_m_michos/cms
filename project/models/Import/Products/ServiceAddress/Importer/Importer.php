<?php

namespace Import\Products\ServiceAddress\Importer;

use CompaniesHouse\Data\Data;
use CSVParser\Items\Items;
use Doctrine\ORM\EntityManager;
use Entities\Service;
use Exception;
use Import\Common\Importer\Importer_Abstract;
use Import\Products\ServiceAddress\Parser\Item;
use InvalidArgumentException;
use Product;
use Services\CompanyService;
use Services\CustomerService;
use Services\OrderService;

class Importer extends Importer_Abstract
{
    const VALIDATE_ONLY = TRUE;

    /**
     * @var Item[]
     */
    private $items;

    /**
     * @var array
     */
    private $errors = array();

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var OrderService
     */
    private $orderService;

    /**
     * @var CustomerService
     */
    private $customerService;

    /**
     * @param Items $items
     * @param EntityManager $entityManager
     * @param CompanyService $companyService
     * @param OrderService $orderService
     * @param CustomerService $customerService
     * @param Data $chData
     */
    public function __construct(
        Items $items,
        EntityManager $entityManager,
        CompanyService $companyService,
        OrderService $orderService,
        CustomerService $customerService,
        Data $chData
    )
    {
        parent::__construct($chData);
        $this->items = $items;
        $this->entityManager = $entityManager;
        $this->companyService = $companyService;
        $this->orderService = $orderService;
        $this->customerService = $customerService;
    }

    /**
     * @return array
     */
    public function validate()
    {
        $this->import(self::VALIDATE_ONLY);
        return $this->errors;
    }

    /**
     * @param bool $validateOnly
     */
    public function import($validateOnly = FALSE)
    {
        $batchSize = 50;
        $batchCount = $processed = $errors = 0;
        $line = 1;

        /* @var $item Item */
        foreach ($this->items as $item) {
            try {
                $itemCompanyId = $item->getCompanyId();
                $itemCustomerId = $item->getCustomerId();
                $itemCompanyNumber = str_pad($item->getCompanyNumber(), 8, 0, STR_PAD_LEFT);
                $itemOrderId = $item->getOrderId();
                $itemProductId = $item->getProductId();
                $itemRenewalProductId = $item->getRenewalProductId();

                $product = Product::getProductById($itemProductId);
                if (!$product) {
                    throw new InvalidArgumentException('Product with id ' . $itemProductId . ' (companyId ' . $itemCompanyId . ' does not exist');
                }

                $renewalProduct = Product::getProductById($itemRenewalProductId);
                if (!$renewalProduct) {
                    throw new InvalidArgumentException('Renewal product with id ' . $itemRenewalProductId . ' (companyId ' . $itemCompanyId . ' does not exist');
                }

                if (!$itemCompanyNumber) {
                    throw new InvalidArgumentException('Company number ' . $itemCompanyNumber . ' is missing in CSV');
                } else {
                    // if company is not found in our database, create a new one from company number
                    $company = $this->companyService->getCompanyByCompanyNumber($itemCompanyNumber);
                    if (!$company) {
                        $customer = $this->customerService->getCustomerById($itemCustomerId);
                        if (!$customer) {
                            throw new InvalidArgumentException('Customer with id ' . $itemCustomerId . ' does not exist in our database');
                        }
                        $company = $this->createCompany($itemCompanyNumber, $customer, $product);
                    }
                }

                $transaction = NULL;

                if (!$itemOrderId) {
                    // if orderId is missing in CSV, create new order
                    $order = $this->createOrder($company, $product);
                    $transaction = $this->createTransaction($order);
                } else {
                    $order = $this->orderService->getOrderById($itemOrderId);
                    if (!$order) {
                        throw new InvalidArgumentException('OrderId ' . $itemOrderId . ' does not exist in our database');
                    } elseif ($order->getCustomer()->getCustomerId() != $itemCustomerId) {
                        // if order was created by different account, create a fake one with original ID in description
                        $order = $this->createFakeOrder($company, $product, $order->getOrderId());
                        $transaction = $this->createTransaction($order);
                    }
                }

                $service = new Service(
                    Service::TYPE_SERVICE_ADDRESS,
                    $product,
                    $company,
                    $order
                );
                $service->setRenewalProduct($renewalProduct);
                $service->setDtStart($item->getDtStart());
                $service->setDtExpires($item->getDtExpires());
                $service->setDtc($item->getDtc());
                $service->setDtm($item->getDtc());

                if ($company->hasEqualService($service)) {
                    throw new InvalidArgumentException("Service {$service->getServiceTypeId()} for company id {$company->getCompanyId()} with same data already exists in our database");
                }

                if (!$validateOnly) {
                    $this->entityManager->persist($service);
                    if ($transaction) {
                        $this->entityManager->persist($transaction);
                    }

                    $processed++;
                    $batchCount++;
                    if (!$validateOnly && ($batchCount % $batchSize === 0)) {
                        $batchCount = 0;
                        $this->flushUpdates();
                    }
                }
            } catch (Exception $e) {
                $this->errors[] = "Line {$line}: {$e->getMessage()}";
            }

            $line++;
        }

        if (!$validateOnly && $batchCount > 0) {
            $this->flushUpdates();
        }
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    private function flushUpdates()
    {
        $this->entityManager->flush();
        $this->entityManager->clear();
    }
}
