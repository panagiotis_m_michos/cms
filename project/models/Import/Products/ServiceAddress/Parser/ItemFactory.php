<?php

namespace Import\Products\ServiceAddress\Parser;

use CSVParser\Item\IItemFactory;

class ItemFactory implements IItemFactory
{
    /**
     * @return Item
     */
    public function create()
    {
        return new Item();
    }
}
