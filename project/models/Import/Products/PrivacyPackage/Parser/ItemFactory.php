<?php

namespace Import\Products\PrivacyPackage\Parser;

use CSVParser\Item\IItemFactory;

class ItemFactory implements IItemFactory
{
    /**
     * @return Item
     */
    public function create()
    {
        return new Item();
    }
}
