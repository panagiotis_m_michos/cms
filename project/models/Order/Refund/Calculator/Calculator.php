<?php

namespace Order\Refund\Calculator;

use Nette\Object;

class Calculator extends Object
{
    public function calculate($itemsRefundTotal, $adminFee, $orderCredit)
    {
        $subtotal = $this->getSubtotal($itemsRefundTotal, $adminFee);
        $totalCreditRefund = $this->getTotalCreditRefund($subtotal, $orderCredit);

        return new Result(
            $itemsRefundTotal,
            $adminFee,
            $subtotal,
            $totalCreditRefund,
            $this->getTotalMoneyRefund($subtotal, $totalCreditRefund)
        );
    }

    /**
     * @param $itemsRefundTotal
     * @param float $adminFee
     * @return float
     */
    private function getSubtotal($itemsRefundTotal, $adminFee)
    {
        return max(0, $itemsRefundTotal - $adminFee);
    }

    /**
     * @param float $subtotal
     * @param float $orderCredit
     * @return mixed
     */
    private function getTotalCreditRefund($subtotal, $orderCredit)
    {
        if ($subtotal <= $orderCredit) {
            $totalCreditRefund = $subtotal;
        } else {
            $totalCreditRefund = $orderCredit;
        }
        return max(0, $totalCreditRefund);
    }

    /**
     * @param float $subtotal
     * @param float $totalCreditRefund
     * @return mixed
     */
    private function getTotalMoneyRefund($subtotal, $totalCreditRefund)
    {
        return max(0, $subtotal - $totalCreditRefund);
    }
}