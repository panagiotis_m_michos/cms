<?php

namespace Order\Refund\Calculator;

use Nette\Object;

class Result extends Object
{
    /**
     * @var float
     */
    private $moneyRefund;

    /**
     * @var float
     */
    private $adminFee;

    /**
     * @var float
     */
    private $subtotal;

    /**
     * @var float
     */
    private $totalCreditRefund;

    /**
     * @var float
     */
    private $totalMoneyRefund;

    /**
     * @param float $moneyRefund
     * @param float $adminFee
     * @param float $subtotal
     * @param float $totalCreditRefund
     * @param float $totalMoneyRefund
     */
    public function __construct($moneyRefund, $adminFee, $subtotal, $totalCreditRefund, $totalMoneyRefund)
    {
        $this->moneyRefund = (float) $moneyRefund;
        $this->adminFee = (float) $adminFee;
        $this->subtotal = (float) $subtotal;
        $this->totalCreditRefund = (float) $totalCreditRefund;
        $this->totalMoneyRefund = (float) $totalMoneyRefund;
    }

    /**
     * @return float
     */
    public function getMoneyRefund()
    {
        return $this->moneyRefund;
    }

    /**
     * @return float
     */
    public function getAdminFee()
    {
        return $this->adminFee;
    }

    /**
     * @return float
     */
    public function getSubtotal()
    {
        return $this->subtotal;
    }

    /**
     * @return float
     */
    public function getTotalCreditRefund()
    {
        return $this->totalCreditRefund;
    }

    /**
     * @return float
     */
    public function getTotalMoneyRefund()
    {
        return $this->totalMoneyRefund;
    }
}