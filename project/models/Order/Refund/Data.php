<?php

namespace Order\Refund;

use Doctrine\Common\Collections\ArrayCollection;
use Entities\OrderItem;
use Nette\Object;

class Data extends Object
{
    /**
     * @var OrderItem[]|ArrayCollection
     */
    private $orderItems;

    /**
     * @var string
     */
    private $description;

    /**
     * @var float
     */
    private $adminFee;

    public function __construct()
    {
        $this->orderItems = new ArrayCollection();
    }

    /**
     * @return OrderItem[]|ArrayCollection
     */
    public function getOrderItems()
    {
        return $this->orderItems;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return float
     */
    public function getAdminFee()
    {
        return $this->adminFee;
    }

    /**
     * @param ArrayCollection $items
     */
    public function setOrderItems($items)
    {
        $this->orderItems = $items;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param float $adminFee
     */
    public function setAdminFee($adminFee)
    {
        $this->adminFee = $adminFee;
    }

    /**
     * @return float
     */
    public function getItemsRefundTotal()
    {
        $total = 0;
        foreach ($this->getOrderItems() as $item) {
            $total += $item->getTotalPrice();
        }
        return $total;
    }
}