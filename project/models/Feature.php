<?php

use Nette\Object;

class Feature extends Object
{
    /**
     * @param string $type
     * @return bool
     */
    public static function isEnabled($type)
    {
        $methodName = 'feature' . $type;
        if (method_exists(__CLASS__, $methodName)) {
            return self::$methodName();
        }
        return FALSE;
    }

    /**
     * @return bool
     */
    public static function featureS404()
    {
        return FALSE;
    }

    /**
     * @return bool
     */
    public static function featureFakeSubmissionSending()
    {
        $now = new DateTime;

        return $now >= new DateTime('2016-04-16 00:00:00') && $now <= new DateTime('2016-04-17 23:59:59');
    }

    /**
     * @return bool
     */
    public static function featurePsc()
    {
        return new DateTime >= new DateTime('2016-06-30 00:00:00');
    }

    /**
     * @return bool
     */
    public static function featurePscTesting()
    {
        return new DateTime < new DateTime('2016-06-30 00:00:00');
    }
}
