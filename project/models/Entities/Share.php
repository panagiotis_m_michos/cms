<?php

namespace Entities;

use Exceptions\Business\BusinessLogic as BusinessLogicException;

class Share
{

    /**
     * @var string
     */
    protected $shareClass;

    /**
     * @var int
     */
    protected $numShares;

    /**
     * @var int
     */
    protected $amountPaidDuePerShare = 0;

    /**
     * @var int
     */
    protected $amountUnpaidPerShare = 0;

    /**
     * @return string
     */
    public function getShareClass()
    {
        return $this->shareClass;
    }

    /**
     * @return int
     */
    public function getNumShares()
    {
        return $this->numShares;
    }

    /**
     * @return int
     */
    public function getAmountPaidDuePerShare()
    {
        return $this->amountPaidDuePerShare;
    }

    /**
     * @return int
     */
    public function getAmountUnpaidPerShare()
    {
        return $this->amountUnpaidPerShare;
    }

    /**
     * @param string $shareClass
     * @return Share
     * @throws InvalidArgumentException
     */
    public function setShareClass($shareClass)
    {
        if ($shareClass == NULL) {
            throw new BusinessLogicException("The shareClass is invalid.");
        }
        $this->shareClass = $shareClass;
        return $this;
    }

    /**
     * @param int $numShares
     * @return Share
     * @throws InvalidArgumentException
     */
    public function setNumShares($numShares)
    {
        if ($numShares == NULL) {
            throw new BusinessLogicException("The numShares is invalid.");
        }
        $this->numShares = $numShares;
        return $this;
    }

    /**
     * @param int $amountPaidDuePerShare
     */
    public function setAmountPaidDuePerShare($amountPaidDuePerShare)
    {
        $this->amountPaidDuePerShare = $amountPaidDuePerShare;
    }

    /**
     * @param int $amountUnpaidPerShare
     */
    public function setAmountUnpaidPerShare($amountUnpaidPerShare)
    {
        $this->amountUnpaidPerShare = $amountUnpaidPerShare;
    }

}
