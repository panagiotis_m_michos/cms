<?php

namespace Entities;

use Doctrine\ORM\Mapping as Orm;
use Gedmo\Mapping\Annotation as Gedmo;
use DateTime;
use Product;

/**
 * OrderItem
 * @Orm\Entity(repositoryClass = "Repositories\OrderItemRepository")
 * @Orm\Table(name="cms2_orders_items")
 */
class OrderItem extends EntityAbstract
{
    /**
     * @var integer
     *
     * @Orm\Column(name="orderItemId", type="integer", nullable=false)
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $orderItemId;

    /**
     * @var boolean
     *
     * @Orm\Column(name="isRefund", type="boolean", nullable=true)
     */
    private $isRefund;

    /**
     * @var boolean
     *
     * @Orm\Column(name="isRefunded", type="boolean", nullable=true)
     */
    private $isRefunded;

    /**
     * @var OrderItem
     * @Orm\ManyToOne(targetEntity="OrderItem")
     * @Orm\JoinColumn(name="refundedOrderItemId", referencedColumnName="orderItemId")
     */
    private $refundedOrderItem;

    /**
     * @var boolean
     *
     * @Orm\Column(name="isFee", type="boolean", nullable=false)
     */
    private $isFee;

    /**
     * @var integer
     *
     * @Orm\Column(name="productId", type="integer", nullable=true)
     */
    private $productId;

    /**
     * @var string
     *
     * @Orm\Column(name="productTitle", type="string", length=255, nullable=false)
     */
    private $productTitle;

    /**
     * @var string
     *
     * @Orm\Column(name="companyName", type="string", length=255, nullable=true)
     */
    private $companyName;

    /**
     * @var string
     *
     * @Orm\Column(name="companyNumber", type="string", length=255, nullable=true)
     */
    private $companyNumber;

    /**
     * @var integer
     *
     * @Orm\Column(name="qty", type="smallint", nullable=false)
     */
    private $qty;

    /**
     * @var float
     *
     * @Orm\Column(name="price", type="float", nullable=false)
     */
    private $price;

    /**
     * @var boolean
     *
     * @Orm\Column(name="notApplyVat", type="boolean", nullable=false)
     */
    private $notApplyVat;

    /**
     * @var float
     *
     * @Orm\Column(name="nonVatableValue", type="float", nullable=false)
     */
    private $nonVatableValue;

    /**
     * @var float
     *
     * @Orm\Column(name="subTotal", type="float", nullable=false)
     */
    private $subTotal;

    /**
     * @var float
     *
     * @Orm\Column(name="vat", type="float", nullable=false)
     */
    private $vat;

    /**
     * @var string
     *
     * @Orm\Column(name="additional", type="string", length=255, nullable=true)
     */
    private $additional;

    /**
     * @var float
     *
     * @Orm\Column(name="totalPrice", type="float", nullable=false)
     */
    private $totalPrice;

    /**
     * @var boolean
     *
     * @Orm\Column(name="incorporationRequired", type="boolean")
     */
    private $incorporationRequired = FALSE;

    /**
     * @var float
     *
     * @Orm\Column(name="markUp", type="float", nullable=true)
     */
    private $markup;

    /**
     * @var Datetime
     * @Orm\Column(type="datetime")
     */
    private $dtExported;

    /**
     * @var Company
     * @Orm\ManyToOne(targetEntity="Company")
     * @Orm\JoinColumn(name="companyId", referencedColumnName="company_id")
     */
    private $company;

    /**
     * @var \Entities\Order
     * @Orm\ManyToOne(targetEntity = "Order", inversedBy = "items")
     * @Orm\JoinColumn(name = "orderId", referencedColumnName="orderId")
     */
    private $order;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create", on="update")
     */
    private $dtm;

    /**
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->setOrder($order);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->getOrderItemId();
    }

    /**
     * @return int
     */
    public function getOrderItemId()
    {
        return $this->orderItemId;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @return bool
     */
    public function getIsRefund()
    {
        return $this->isRefund;
    }

    /**
     * @return bool
     */
    public function isRefunded()
    {
        return $this->isRefunded;
    }

    /**
     * @return OrderItem
     */
    public function getRefundedOrderItem()
    {
        return $this->refundedOrderItem;
    }

    /**
     * @return bool
     */
    public function getIsFee()
    {
        return $this->isFee;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @return string
     */
    public function getProductTitle()
    {
        return $this->productTitle;
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @return string
     */
    public function getCompanyNumber()
    {
        return $this->companyNumber;
    }

    /**
     * @return int
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return bool
     */
    public function getNotApplyVat()
    {
        return $this->notApplyVat;
    }

    /**
     * @return float
     */
    public function getNonVatableValue()
    {
        return $this->nonVatableValue;
    }

    /**
     * @return float
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * @return float
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @return string
     */
    public function getAdditional()
    {
        return $this->additional;
    }

    /**
     * @return float
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * @return bool
     */
    public function getIncorporationRequired()
    {
        return $this->incorporationRequired;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return float
     */
    public function getMarkup()
    {
        return $this->markup;
    }

    /**
     * @return DateTime
     */
    public function getDtExported()
    {
        return $this->dtExported;
    }

    /**
     * @return DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @return DateTime
     */
    public function getDtm()
    {
        return $this->dtm;
    }

    /**
     * @param Order $order
     */
    public function setOrder(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @param bool $isRefund
     */
    public function setIsRefund($isRefund)
    {
        $this->isRefund = $isRefund;
    }

    /**
     * @param boolean $isRefunded
     */
    public function setIsRefunded($isRefunded)
    {
        $this->isRefunded = $isRefunded;
    }

    /**
     * @param OrderItem $refundedOrderItem
     */
    public function setRefundedOrderItem(OrderItem $refundedOrderItem)
    {
        $this->refundedOrderItem = $refundedOrderItem;
    }

    /**
     * @param bool $isFee
     */
    public function setIsFee($isFee)
    {
        $this->isFee = $isFee;
    }

    /**
     * @param int $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * @param string $productTitle
     */
    public function setProductTitle($productTitle)
    {
        $this->productTitle = $productTitle;
    }

    /**
     * @param int $qty
     */
    public function setQty($qty)
    {
        $this->qty = $qty;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @param bool $notApplyVat
     */
    public function setNotApplyVat($notApplyVat)
    {
        $this->notApplyVat = $notApplyVat;
    }

    /**
     * @param float $nonVatableValue
     */
    public function setNonVatableValue($nonVatableValue)
    {
        $this->nonVatableValue = $nonVatableValue;
    }

    /**
     * @param float $subTotal
     */
    public function setSubTotal($subTotal)
    {
        $this->subTotal = $subTotal;
    }

    /**
     * @param float $vat
     */
    public function setVat($vat)
    {
        $this->vat = $vat;
    }

    /**
     * @param string $additional
     */
    public function setAdditional($additional)
    {
        $this->additional = $additional;
    }

    /**
     * @param float $totalPrice
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;
    }

    /**
     * @param bool $incorporationRequired
     */
    public function setIncorporationRequired($incorporationRequired)
    {
        $this->incorporationRequired = $incorporationRequired;
    }

    /**
     * @param Company $company
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;
        $this->companyName = $company->getCompanyName();
        $this->companyNumber = $company->getCompanyNumber();
    }

    /**
     * @param float $markup
     */
    public function setMarkup($markup)
    {
        $this->markup = $markup;
    }

    /**
     * @param DateTime $dtExported
     */
    public function setDtExported(DateTime $dtExported = NULL)
    {
        $this->dtExported = $dtExported;
    }

    /**
     * @param DateTime $dtc
     */
    public function setDtc(DateTime $dtc)
    {
        $this->dtc = $dtc;
    }

    /**
     * @param DateTime $dtm
     */
    public function setDtm(DateTime $dtm)
    {
        $this->dtm = $dtm;
    }

    /**
     * @param Product $product
     */
    public function setProduct(Product $product)
    {
        $this->productId = $product->getId();
        $this->productTitle = $product->getLngTitle();
    }

    /**
     * @return string
     */
    public function getProductTitleAndTotalPrice()
    {
        return sprintf("%s (£%s)", $this->getProductTitle(), number_format($this->getTotalPrice(), 2));
    }

    public function __clone()
    {
        if ($this->orderItemId) {
            $this->dtExported = NULL;
            $this->dtc = NULL;
            $this->dtm = NULL;
        }
    }
}
