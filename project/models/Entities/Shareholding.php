<?php

//<Shareholdings>
//    <ShareClass>Ordinary</ShareClass>
//    <NumberHeld>60</NumberHeld>
//    <Shareholders>
//        <Name>
//            <Surname>glover</Surname><Forename>phil</Forename>
//        </Name>
//        <Address>
//            <Premise>19</Premise><Street>winnipeg drive</Street><PostTown>cardiff</PostTown><Country>GB-WLS</Country>
//        </Address>
//    </Shareholders>
//    <Shareholders>
//        <Name>
//            <Surname>glover</Surname><Forename>bob</Forename>
//        </Name>
//        <Address>
//            <Premise>21</Premise><Street>winnipeg drive</Street><PostTown>cardiff</PostTown><Country>GB-WLS</Country>
//        </Address>
//    </Shareholders>
//</Shareholdings>

namespace Entities;

class Shareholding implements EntityInterface
{

    /**
     * @var array
     */
    protected $shareholders = array();

    /**
     * @var string
     */
    protected $shareClass;

    /**
     * @var int
     */
    protected $numberHeld;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $sync;

    /**
     * @var int
     */
    protected $formSubmissionId;

    /**
     * @var array
     */
    protected $transfers = array();

    /**
     * @param string $shareClass
     * @param int $numberHeld
     */
    public function __construct($shareClass, $numberHeld)
    {
        $this->shareClass = $shareClass;
        $this->numberHeld = $numberHeld;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getSync()
    {
        return $this->sync;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param int $sync
     */
    public function setSync($sync)
    {
        $this->sync = $sync;
    }

    /**
     * @return string
     */
    public function getShareClass()
    {
        return $this->shareClass;
    }

    /**
     * @return int
     */
    public function getNumberHeld()
    {
        return $this->numberHeld;
    }

    /**
     * @return int
     */
    public function getFormSubmissionId()
    {
        return $this->formSubmissionId;
    }

    /**
     * @param int $formSubmissionId
     */
    public function setFormSubmissionId($formSubmissionId)
    {
        $this->formSubmissionId = $formSubmissionId;
    }

    /**
     * @param string $shareClass
     */
    public function setShareClass($shareClass)
    {
        $this->shareClass = $shareClass;
    }

    /**
     * @param int $numberHeld
     */
    public function setNumberHeld($numberHeld)
    {
        $this->numberHeld = $numberHeld;
    }

    /**
     * @return array
     */
    public function getShareholders()
    {
        return $this->shareholders;
    }

    /**
     * @param array $shareholders
     */
    public function setShareholders($shareholders)
    {
        $this->shareholders = $shareholders;
    }

    /**
     * @return array
     */
    public function getTransfers()
    {
        return $this->transfers;
    }

    /**
     * @param array $transfers
     */
    public function setTransfers($transfers)
    {
        $this->transfers = $transfers;
    }

}
