<?php

namespace Entities\Payment;

use Doctrine\ORM\Mapping as Orm;
use Gedmo\Mapping\Annotation as Gedmo;
use DateTime;
use Nette\Object;

/**
 * @Orm\Table(name="cms2_token_logs")
 * @Orm\Entity(repositoryClass="Repositories\Payment\TokenHistoryRepository")
 */
class TokenHistory extends Object
{
    const STATUS_INCOMPLETE = "INCOMPLETE";
    const STATUS_ERROR = "ERROR";
    const STATUS_COMPLETE = "COMPLETE";
    const STATUS_REMOVED = "REMOVED";
    const STATUS_REMOVED_ERROR = "REMOVED_ERROR";

    /**
     * @var integer $tokenHistoryId
     *
     * @Orm\Column(name="tokenStatusId", type="integer", nullable=false)
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $tokenHistoryId;

    
    /**
     * @var string
     * @Orm\Column(name="tokenStatus", type="string", nullable=false)
     */
    private $statusId;

    
    /**
     * @var string
     * @Orm\Column(name="token", type="string", length=100, nullable=false)
     */
    private $token;

    /**
     * @var string
     * @Orm\Column(name="cardNumber", type="string", length=100, nullable=false)
     */
    private $cardNumber;

    /**
     * @var string
     * @Orm\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     * @Orm\Column(name="address", type="string", nullable=false)
     */
    private $address;
    
    /**
     * @var string
     * @Orm\Column(name="email", type="string", length=120, nullable=false)
     */
    private $email;
    
    /**
     * @var DateTime
     * 
     * @Orm\Column(name="dtc", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;

    /**
     * @var DateTime
     *
     * @Orm\Column(name="dtm", type="datetime")
     * @Gedmo\Timestampable(on="create", on="update")
     */
    private $dtm;

    /**
     * @param string $statusId
     * @param string $token
     * @param string $cardNumber
     * @param string $name 
     * @param string $email
     */
    public function __construct($statusId, $token, $cardNumber, $name, $email)
    {
        $this->statusId = $statusId;
        $this->token = $token;
        $this->cardNumber = $cardNumber;
        $this->name = $name;
        $this->email = $email;
    }


    /**
     * @return integer $tokenHistoryId
     */
    public function getTokenHistoryId()
    {
        return $this->tokenHistoryId;
    }


    /**
     * @return string
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * @param string $statusId
     */
    public function setStatusId($statusId)
    {
        return $this->statusId = $statusId;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        return $this->token = $token;
    }

    /**
     * @return string
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * @param string $cardNumber
     */
    public function setCardNumber($cardNumber)
    {
        return $this->cardNumber = $cardNumber;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        return $this->name = $name;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        return $this->address = $address;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        return $this->email = $email;
    }

    /**
     * @return DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @param DateTime $dtc
     */
    public function setDtc(DateTime $dtc)
    {
        return $this->dtc = $dtc;
    }

    /**
     * @return DateTime
     */
    public function getDtm()
    {
        return $this->dtm;
    }

    /**
     * @param DateTime $dtm
     */
    public function setDtm(DateTime $dtm)
    {
        return $this->dtm = $dtm;
    }
}
