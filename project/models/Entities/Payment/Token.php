<?php

namespace Entities\Payment;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as Orm;
use Entities\Service;
use Gedmo\Mapping\Annotation as Gedmo;
use Nette\Object;
use SagePay\Reporting\Interfaces\IToken;
use Utils\Date;
use Entities\Customer;
use WPDirect;
use SagePayDirect;
use DateTime;
use PaymentModule\Contracts\IToken as IPaymentToken;

/**
 * @Orm\Table(name="cms2_tokens")
 * @Orm\Entity(repositoryClass="Repositories\Payment\TokenRepository")
 */
class Token extends Object implements IToken, IPaymentToken
{
    const STATUS_ACTIVE = 'ACTIVE';
    const STATUS_DISABLED = 'DISABLED';

    const SAGE_STATUS_ACTIVE = 'ACTIVE';
    const SAGE_STATUS_DELETED = 'DELETED';

    /**
     * @var integer $tokenId
     *
     * @Orm\Column(name="tokenId", type="integer", nullable=false)
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $tokenId;

    /**
     * @var string $cardNumber
     *
     * @Orm\Column(name="cardNumber", type="string", length=4, nullable=false)
     */
    private $cardNumber;
    
    /**
     * @var string $cardNumber
     *
     * @Orm\Column(name="cardHolder", type="string", length=50, nullable=false)
     */
    private $cardHolder;
    
    /**
     * @var string $cardType
     *
     * @Orm\Column(name="cardType", type="string", length=15, nullable=false)
     */
    private $cardType;

    /**
     * @var string $identifier
     *
     * @Orm\Column(name="identifier", type="string", length=255, nullable=false)
     */
    private $identifier;
    
    /**
     * @var Date
     * 
     * @Orm\Column(name="cardExpiryDate", type="date", nullable=true)
     */
    private $cardExpiryDate;

    /**
     * @var string
     * @Orm\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @var string max 20  
     * @Orm\Column(name="billingSurname", type="string", length=20, nullable=false)
     */
    private $billingSurname;

    /**
     * @var string max 20  
     * @Orm\Column(name="billingFirstnames", type="string", length=20, nullable=false)
     */
    private $billingFirstnames;

    /**
     * @var string max 100  
     * @Orm\Column(name="billingAddress1", type="string", length=100, nullable=false)
     */
    private $billingAddress1;

    /**
     * @var string max 40 optional
     * @Orm\Column(name="billingAddress2", type="string", length=40, nullable=true)
     */
    private $billingAddress2;

    /**
     * @var string max 40  
     * @Orm\Column(name="billingCity", type="string", length=40, nullable=false)
     */
    private $billingCity;

    /**
     * @var string max 40  
     * @Orm\Column(name="billingPostCode", type="string", length=40, nullable=false)
     */
    private $billingPostCode;

    /**
     * @var string max 2  
     * @Orm\Column(name="billingCountry", type="string", length=2, nullable=false)
     */
    private $billingCountry;

    /**
     * @var string max 2 optional
     * @Orm\Column(name="billingState", type="string", length=2, nullable=true)
     */
    private $billingState;

    /**
     * @var string max 20 optional
     * @Orm\Column(name="billingPhone", type="string", length=20, nullable=true)
     */
    private $billingPhone;

    /**
     * @var string max 20  
     * @Orm\Column(name="deliverySurname", type="string", length=20, nullable=false)
     */
    private $deliverySurname;

    /**
     * @var string max 20  
     * @Orm\Column(name="deliveryFirstnames", type="string", length=20, nullable=false)
     */
    private $deliveryFirstnames;

    /**
     * @var string max 100  
     * @Orm\Column(name="deliveryAddress1", type="string", length=100, nullable=false)
     */
    private $deliveryAddress1;

    /**
     * @var string max 100  optional
     * @Orm\Column(name="deliveryAddress2", type="string", length=100, nullable=true)
     */
    private $deliveryAddress2;

    /**
     * @var string max 40  
     * @Orm\Column(name="deliveryCity", type="string", length=40, nullable=false)
     */
    private $deliveryCity;

    /**
     * @var string max 10  
     * @Orm\Column(name="deliveryPostCode", type="string", length=10, nullable=false)
     */
    private $deliveryPostCode;

    /**
     * @var string max 2  
     * @Orm\Column(name="deliveryCountry", type="string", length=2, nullable=false)
     */
    private $deliveryCountry;

    /**
     * @var string max 2 optional
     * @Orm\Column(name="deliveryState", type="string", length=2, nullable=true)
     */
    private $deliveryState;

    /**
     * @var string max 20 optional
     * @Orm\Column(name="deliveryPhone", type="string", length=20, nullable=true)
     */
    private $deliveryPhone;
    
    /**
     * marks that the token can be used for transaction
     * @var bool
     * @Orm\Column(name="isCurrent", type="boolean", nullable=false)
     */
    private $isCurrent;

    /**
     * indicates the status of the token in the sage database
     * @var string
     * @Orm\Column(name="sageStatus", type="string", nullable=false)
     */
    private $sageStatus = self::STATUS_ACTIVE;
    
    /**
     * @var Customer
     *
     * @Orm\ManyToOne(targetEntity="Entities\Customer", inversedBy="tokens")
     * @Orm\JoinColumns({
     *   @Orm\JoinColumn(name="customerId", referencedColumnName="customerId")
     * })
     */
    private $customer;

    /**
     * @var DateTime
     * 
     * @Orm\Column(name="dtc", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;

    /**
     * @var DateTime
     *
     * @Orm\Column(name="dtm", type="datetime")
     * @Gedmo\Timestampable(on="create", on="update")
     */
    private $dtm;


    /**
     * @param Customer $customer
     * @param string $identifier
     * @param string $cardType
     * @param string $cardHolder
     * @param string $cardNumber
     * @param Date $cardExpiryDate
     */
    public function __construct(Customer $customer, $identifier, $cardType, $cardHolder, $cardNumber, Date $cardExpiryDate)
    {
        $this->setCustomer($customer);
        $this->setIdentifier($identifier);
        $this->setCardType($cardType);
        $this->setCardHolder($cardHolder);
        $this->setCardNumber($cardNumber);
        $this->setCardExpiryDate($cardExpiryDate);
    }

    /**
     * @return integer $tokenId
     */
    public function getId()
    {
        return $this->tokenId;
    }

    /**
     * @return integer $tokenId
     */
    public function getTokenId()
    {
        return $this->tokenId;
    }

    /**
     * @param integer $tokenId
     */
    public function setTokenId($tokenId)
    {
        $this->tokenId = $tokenId;
    }

    /**
     * @return string $cardNumber
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * @param string $cardNumber
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = substr($cardNumber, -4, 4);
    }

    /**
     * @return string $cardNumber
     */
    public function getCardHolder()
    {
        return $this->cardHolder;
    }

    /**
     * @param string $cardHolder
     */
    public function setCardHolder($cardHolder)
    {
        $this->cardHolder = $cardHolder;
    }

    /**
     * @return string
     */
    public function getCardType()
    {
        return $this->cardType;
    }

    /**
     * @return string
     */
    public function getCardTypeText()
    {
        $cardTypes = SagePayDirect::getCardTypes();
        return isset($cardTypes[$this->cardType]) ? $cardTypes[$this->cardType] : NULL;
    }

    /**
     * @param string $cardType
     */
    public function setCardType($cardType)
    {
        $this->cardType = $cardType;
    }

    /**
     * @return string $identifier
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
    }

    /**
     * @return Date
     */
    public function getCardExpiryDate()
    {
        return $this->cardExpiryDate;
    }

    /**
     * @param Date $cardExpiryDate
     */
    public function setCardExpiryDate(Date $cardExpiryDate)
    {
        $this->cardExpiryDate = $cardExpiryDate;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string max 20
     */
    public function getBillingSurname()
    {
        return $this->billingSurname;
    }

    /**
     * @param string max 20 $billingSurname
     */
    public function setBillingSurname($billingSurname)
    {
        return $this->billingSurname = $billingSurname;
    }

    /**
     * @return string max 20
     */
    public function getBillingFirstnames()
    {
        return $this->billingFirstnames;
    }

    /**
     * @param string max 20 $billingFirstnames
     */
    public function setBillingFirstnames($billingFirstnames)
    {
        return $this->billingFirstnames = $billingFirstnames;
    }

    /**
     * @return string max 100
     */
    public function getBillingAddress1()
    {
        return $this->billingAddress1;
    }

    /**
     * @param string max 100 $billingAddress1
     */
    public function setBillingAddress1($billingAddress1)
    {
        return $this->billingAddress1 = $billingAddress1;
    }

    /**
     * @return string max 40 optional
     */
    public function getBillingAddress2()
    {
        return $this->billingAddress2;
    }

    /**
     * @param string max 40 optional $billingAddress2
     */
    public function setBillingAddress2($billingAddress2)
    {
        return $this->billingAddress2 = $billingAddress2;
    }

    /**
     * @return string max 40
     */
    public function getBillingCity()
    {
        return $this->billingCity;
    }

    /**
     * @param string max 40 $billingCity
     */
    public function setBillingCity($billingCity)
    {
        return $this->billingCity = $billingCity;
    }

    /**
     * @return string max 40
     */
    public function getBillingPostCode()
    {
        return $this->billingPostCode;
    }

    /**
     * @param string max 40 $billingPostCode
     */
    public function setBillingPostCode($billingPostCode)
    {
        return $this->billingPostCode = $billingPostCode;
    }

    /**
     * @return string
     */
    public function getBillingCountry()
    {
        return $this->billingCountry;
    }

    /**
     * @return string
     */
    public function getBillingCountryText()
    {
        $countries = WPDirect::$countries;
        return isset($countries[$this->billingCountry]) ? $countries[$this->billingCountry] : NULL;
    }

    /**
     * @param string max 2 $billingCountry
     */
    public function setBillingCountry($billingCountry)
    {
        return $this->billingCountry = $billingCountry;
    }

    /**
     * @return string
     */
    public function getBillingState()
    {
        return $this->billingState;
    }

    /**
     * @return string max 2 optional
     */
    public function getBillingStateText()
    {
        $states = SagePayDirect::getStates();
        return isset($states[$this->billingState]) ? $states[$this->billingState] : NULL;
    }

    /**
     * @param string max 2 optional $billingState
     */
    public function setBillingState($billingState)
    {
        return $this->billingState = $billingState;
    }

    /**
     * @return string max 20 optional
     */
    public function getBillingPhone()
    {
        return $this->billingPhone;
    }

    /**
     * @param string max 20 optional $billingPhone
     */
    public function setBillingPhone($billingPhone)
    {
        return $this->billingPhone = $billingPhone;
    }

    /**
     * @return string max 20
     */
    public function getDeliverySurname()
    {
        return $this->deliverySurname;
    }

    /**
     * @param string max 20 $deliverySurname
     */
    public function setDeliverySurname($deliverySurname)
    {
        return $this->deliverySurname = $deliverySurname;
    }

    /**
     * @return string max 20
     */
    public function getDeliveryFirstnames()
    {
        return $this->deliveryFirstnames;
    }

    /**
     * @param string max 20 $deliveryFirstnames
     */
    public function setDeliveryFirstnames($deliveryFirstnames)
    {
        return $this->deliveryFirstnames = $deliveryFirstnames;
    }

    /**
     * @return string max 100
     */
    public function getDeliveryAddress1()
    {
        return $this->deliveryAddress1;
    }

    /**
     * @param string max 100 $deliveryAddress1
     */
    public function setDeliveryAddress1($deliveryAddress1)
    {
        return $this->deliveryAddress1 = $deliveryAddress1;
    }

    /**
     * @return string max 100  optional
     */
    public function getDeliveryAddress2()
    {
        return $this->deliveryAddress2;
    }

    /**
     * @param string max 100  optional $deliveryAddress2
     */
    public function setDeliveryAddress2($deliveryAddress2)
    {
        return $this->deliveryAddress2 = $deliveryAddress2;
    }

    /**
     * @return string max 40
     */
    public function getDeliveryCity()
    {
        return $this->deliveryCity;
    }

    /**
     * @param string max 40 $deliveryCity
     */
    public function setDeliveryCity($deliveryCity)
    {
        return $this->deliveryCity = $deliveryCity;
    }

    /**
     * @return string max 10
     */
    public function getDeliveryPostCode()
    {
        return $this->deliveryPostCode;
    }

    /**
     * @param string max 10 $deliveryPostCode
     */
    public function setDeliveryPostCode($deliveryPostCode)
    {
        return $this->deliveryPostCode = $deliveryPostCode;
    }

    /**
     * @return string max 2
     */
    public function getDeliveryCountry()
    {
        return $this->deliveryCountry;
    }

    /**
     * @param string max 2 $deliveryCountry
     */
    public function setDeliveryCountry($deliveryCountry)
    {
        return $this->deliveryCountry = $deliveryCountry;
    }

    /**
     * @return string max 2 optional
     */
    public function getDeliveryState()
    {
        return $this->deliveryState;
    }

    /**
     * @param string max 2 optional $deliveryState
     */
    public function setDeliveryState($deliveryState)
    {
        return $this->deliveryState = $deliveryState;
    }

    /**
     * @return string max 20 optional
     */
    public function getDeliveryPhone()
    {
        return $this->deliveryPhone;
    }

    /**
     * @param string max 20 optional $deliveryPhone
     */
    public function setDeliveryPhone($deliveryPhone)
    {
        return $this->deliveryPhone = $deliveryPhone;
    }

    /**
     * @return bool
     */
    public function getIsCurrent()
    {
        return $this->isCurrent;
    }

    /**
     * @param bool $isCurrent
     */
    public function setIsCurrent($isCurrent)
    {
        $this->isCurrent = $isCurrent;
    }

    /**
     * @return string
     */
    public function getSageStatus()
    {
        return $this->sageStatus;
    }

    /**
     * @param string $sageStatus
     */
    public function setSageStatus($sageStatus)
    {
        $this->sageStatus = $sageStatus;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @param DateTime $dtc
     */
    public function setDtc(DateTime $dtc)
    {
        $this->dtc = $dtc;
    }

    /**
     * @return DateTime
     */
    public function getDtm()
    {
        return $this->dtm;
    }

    /**
     * @param DateTime $dtm
     */
    public function setDtm(DateTime $dtm)
    {
        $this->dtm = $dtm;
    }

    /**
     * @return bool
     */
    public function isCardExpired()
    {
        $cardExpiryDate = $this->getCardExpiryDate();
        $today = new Date();
        return ($today > $cardExpiryDate);
    }

    /**
     * @return bool
     */
    public function isCardExpiringIn7Days()
    {
        if ($this->isCardExpired()) {
            return FALSE;
        }

        $expiringIn7DaysDate = clone $this->getCardExpiryDate();
        $expiringIn7DaysDate->modify('-7 days');
        $today = new Date();
        return ($today > $expiringIn7DaysDate);
    }

    /**
     * @return string
     */
    public function getBillingAddress()
    {
        $address = array();
        if ($this->getBillingFirstnames() || $this->getBillingSurname()) {
            $address[] = trim($this->getBillingFirstnames() . ' ' . $this->getBillingSurname());
        }
        if ($this->getBillingAddress1()) {
            $address[] = $this->getBillingAddress1();
        }
        if ($this->getBillingAddress2()) {
            $address[] = $this->getBillingAddress2();
        }
        if ($this->getBillingCity()) {
            $address[] = $this->getBillingCity();
        }
        if ($this->getBillingPostCode()) {
            $address[] = $this->getBillingPostCode();
        }
        if ($this->getBillingState()) {
            $address[] = $this->getBillingStateText();
        }
        if ($this->getBillingCountry()) {
            $address[] = $this->getBillingCountryText();
        }

        return implode(PHP_EOL, $address);
    }

    /**
     * @param Token $token
     * @return bool
     */
    public function isEqual(Token $token)
    {
        return $this->getTokenId() === $token->getTokenId();
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->getSageStatus() == self::SAGE_STATUS_ACTIVE;
    }
}
