<?php

namespace Entities;

use Doctrine\ORM\Mapping as Orm;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * CustomerNote
 * 
 * @Orm\Entity(repositoryClass = "Repositories\CustomerNoteRepository")
 * @Orm\Table(name="cms2_customer_notes")
 */
class CustomerNote extends EntityAbstract
{
    const BLOCKED_TEXT = 'Blocked reason:';

    /**
     * @var integer
     *
     * @Orm\Column(type="integer")
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $customerNoteId;
    
    /**
     * @var Customer 
     * @Orm\OneToOne(targetEntity="Customer", cascade={"persist"})
     * @Orm\JoinColumn(name="customerId", referencedColumnName="customerId")
     */
    private $customer;
    
    /**
     * @var User
     * @Orm\OneToOne(targetEntity="User", cascade={"persist"})
     * @Orm\JoinColumn(name="userId", referencedColumnName="user_id")
     */
    private $user;
    
    /**
     * @var string
     * 
     * @Orm\Column(type="string")
     */
    private $note;
    
     /**
     * @var \DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;

    /**
     * @var \DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create", on="update")
     */
    private $dtm;
    
    /**
     * Alias
     * 
     * @return int
     */
    public function getId()
    {
        return $this->getCustomerNoteId();
    }
    
    /**
     * @return int
     */
    public function getCustomerNoteId()
    {
        return $this->customerNoteId;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return \DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @return \DateTime
     */
    public function getDtm()
    {
        return $this->dtm;
    }

    /**
     * @param int $customerNoteId
     */
    public function setCustomerNoteId($customerNoteId)
    {
        $this->customerNoteId = $customerNoteId;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;
    }

    /**
     * @param string $note
     */
    public function setNote($note)
    {
        $this->note = $note;
    }

    /**
     * @param \Entities\User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return bool
     */
    public function hasBlocked()
    {
        return strpos($this->getNote(), self::BLOCKED_TEXT) !== FALSE;
    }

    /**
     * @return string|NULL
     */
    public function getBlockedText()
    {
        if ($this->hasBlocked()) {
            return trim(str_replace(self::BLOCKED_TEXT, '', $this->getNote()));
        }
    }
}
