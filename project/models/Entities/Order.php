<?php

namespace Entities;

use BasketModule\ValueObject\Price;
use BasketProduct;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as Orm;
use FApplication;
use Gedmo\Mapping\Annotation as Gedmo;
use DusanKasan\Knapsack\Collection;
use Nette\Utils\Arrays;
use Order as OldOrder;
use PaymentModule\Contracts\IOrder;

/**
 * @Orm\Entity(repositoryClass = "Repositories\OrderRepository")
 * @Orm\Table(name="cms2_orders")
 */
class Order extends EntityAbstract implements IOrder
{
    const STATUS_NEW = OldOrder::STATUS_NEW;
    const STATUS_CONFIRMED = OldOrder::STATUS_CONFIRMED;
    const STATUS_FAILED = OldOrder::STATUS_FAILED;
    const STATUS_CANCELLED = OldOrder::STATUS_CANCELLED;

    const PAYMENT_MEDIUM_ONSITE = 'ONSITE';
    const PAYMENT_MEDIUM_PHONE = 'PHONE';
    const PAYMENT_MEDIUM_AUTO_RENEWAL = 'AUTO_RENEWAL';
    const PAYMENT_MEDIUM_COMPLIMENTARY = 'COMPLIMENTARY';
    const PAYMENT_MEDIUM_NON_STANDARD = 'NON_STANDARD';

    /**
     * @var array
     */
    public static $statuses = [
        self::STATUS_NEW => 'New',
        self::STATUS_CONFIRMED => 'Confirmed',
        self::STATUS_FAILED => 'Failed',
        self::STATUS_CANCELLED => 'Cancelled'
    ];

    /**
     * @var array
     */
    public static $paymentMediums = [
        self::PAYMENT_MEDIUM_ONSITE => 'Onsite',
        self::PAYMENT_MEDIUM_PHONE => 'Phone',
        self::PAYMENT_MEDIUM_AUTO_RENEWAL => 'Auto-renewal',
        self::PAYMENT_MEDIUM_COMPLIMENTARY => 'Complimentary',
        self::PAYMENT_MEDIUM_NON_STANDARD => 'Non-standard',
    ];

    /**
     * @var integer
     *
     * @Orm\Column(name="orderId", type="integer", nullable=false)
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $orderId;

    /**
     * @var float
     *
     * @Orm\Column(name="realSubtotal", type="float", nullable=false)
     */
    private $realSubTotal = 0;

    /**
     * @var float
     *
     * @Orm\Column(name="subtotal", type="float", nullable=false)
     */
    private $subTotal = 0;

    /**
     * @var float
     *
     * @Orm\Column(name="discount", type="float", nullable=false)
     */
    private $discount = 0;

    /**
     * @var float
     *
     * @Orm\Column(name="vat", type="float", nullable=false)
     */
    private $vat = 0;

    /**
     * @var float
     *
     * @Orm\Column(name="credit", type="float", nullable=false)
     */
    private $credit = 0;

    /**
     * @var float
     *
     * @Orm\Column(name="total", type="float", nullable=false)
     */
    private $total = 0;

    /**
     * @var string
     *
     * @Orm\Column(name="customerName", type="string", length=255, nullable=true)
     */
    private $customerName;

    /**
     * @var string
     *
     * @Orm\Column(name="customerAddress", type="string", length=255, nullable=true)
     */
    private $customerAddress;

    /**
     * @var string
     *
     * @Orm\Column(name="customerPhone", type="string", length=255, nullable=true)
     */
    private $customerPhone;

    /**
     * @var string
     *
     * @Orm\Column(name="customerEmail", type="string", length=255, nullable=true)
     */
    private $customerEmail;

    /**
     * @var integer
     *
     * @Orm\Column(name="voucherId", type="integer", nullable=true)
     */
    private $voucherId;

    /**
     * @var string
     *
     * @Orm\Column(name="voucherName", type="string", length=255, nullable=true)
     */
    private $voucherName;

    /**
     * @var string
     *
     * @Orm\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var int
     *
     * @Orm\Column(name="statusId", type="integer", nullable=false)
     */
    private $statusId = self::STATUS_NEW;

    /**
     * @var bool
     *
     * @Orm\Column(name="isRefunded", type="boolean", nullable=true)
     */
    private $isRefunded;

    /**
     * @var float
     *
     * @Orm\Column(name="refundValue", type="float", nullable=true)
     */
    private $refundValue;

    /**
     * @var float
     *
     * @Orm\Column(name="refundCreditValue", type="float", nullable=true)
     */
    private $refundCreditValue;

    /**
     * @var string
     *
     * @Orm\Column(name="refundCustomerSupportLogin", type="string", length=255, nullable=true)
     */
    private $refundCustomerSupportLogin;

    /**
     * @var string
     *
     * @Orm\Column(name="paymentMediumId")
     */
    private $paymentMediumId = self::PAYMENT_MEDIUM_ONSITE;

    /**
     * @var User
     *
     * @Orm\ManyToOne(targetEntity="User", cascade={"persist"})
     * @Orm\JoinColumn(name="agentId", referencedColumnName="user_id")
     */
    private $agent;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create", on="update")
     */
    private $dtm;

    /**
     * @var Service[]|ArrayCollection
     * @Orm\OneToMany(targetEntity="Service", mappedBy="order", cascade={"persist"})
     */
    private $services;

    /**
     * @var OrderItem[]|ArrayCollection
     * @Orm\OneToMany(targetEntity = "OrderItem", mappedBy = "order", cascade={"persist"}, orphanRemoval=true)
     */
    private $items;

    /**
     * @var Customer
     * @Orm\ManyToOne(targetEntity="Customer", cascade={"persist"})
     * @Orm\JoinColumn(name="customerId", referencedColumnName="customerId")
     */
    private $customer;

    /**
     * @var Transaction
     * @Orm\OneToOne(targetEntity="Transaction", mappedBy="order", cascade={"persist"})
     */
    private $transaction;

    /**
     * @param Customer customer
     */
    public function __construct(Customer $customer)
    {
        $this->items = new ArrayCollection();
        $this->setCustomer($customer);
        $this->setCustomerDetails($customer);
    }

    /**
     * @return Order
     */
    public function cloneObject()
    {
        $order = clone $this;
        $items = $order->getItems();
        $newItems = new ArrayCollection();
        $order->setItems($newItems);
        foreach ($items as $item) {
            $newItem = clone $item;
            $order->addItem($newItem);
        }
        return $order;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->orderId;
    }

    /**
     * @return ArrayCollection|OrderItem[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param OrderItem $item
     */
    public function addItem(OrderItem $item)
    {
        $this->items[] = $item;
        $item->setOrder($this);
    }

    /**
     * @param ArrayCollection $items
     */
    public function setItems(ArrayCollection $items)
    {
        $this->items = $items;
    }

    /**
     * @return integer
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @return float
     */
    public function getRealSubTotal()
    {
        return $this->realSubTotal;
    }

    /**
     * @param float $realSubTotal
     */
    public function setRealSubTotal($realSubTotal)
    {
        $this->realSubTotal = $realSubTotal;
    }

    /**
     * @return float
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * @param float $subTotal
     */
    public function setSubTotal($subTotal)
    {
        $this->subTotal = $subTotal;
    }

    /**
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param float $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return float
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param float $vat
     */
    public function setVat($vat)
    {
        $this->vat = $vat;
    }

    /**
     * @return float
     */
    public function getCredit()
    {
        return $this->credit;
    }

    /**
     * @param float $credit
     */
    public function setCredit($credit)
    {
        $this->credit = $credit;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return string
     */
    public function getCustomerName()
    {
        return $this->customerName;
    }

    /**
     * @param string $customerName
     */
    public function setCustomerName($customerName)
    {
        $this->customerName = $customerName;
    }

    /**
     * @return string
     */
    public function getCustomerAddress()
    {
        return $this->customerAddress;
    }

    /**
     * @param string $customerAddress
     */
    public function setCustomerAddress($customerAddress)
    {
        $this->customerAddress = $customerAddress;
    }

    /**
     * @return string
     */
    public function getCustomerPhone()
    {
        return $this->customerPhone;
    }

    /**
     * @param string $customerPhone
     */
    public function setCustomerPhone($customerPhone)
    {
        $this->customerPhone = $customerPhone;
    }

    /**
     * @return string
     */
    public function getCustomerEmail()
    {
        return $this->customerEmail;
    }

    /**
     * @param string $customerEmail
     */
    public function setCustomerEmail($customerEmail)
    {
        $this->customerEmail = $customerEmail;
    }

    /**
     * @return integer
     */
    public function getVoucherId()
    {
        return $this->voucherId;
    }

    /**
     * @param integer $voucherId
     */
    public function setVoucherId($voucherId)
    {
        $this->voucherId = $voucherId;
    }

    /**
     * @return string
     */
    public function getVoucherName()
    {
        return $this->voucherName;
    }

    /**
     * @param string $voucherName
     */
    public function setVoucherName($voucherName)
    {
        $this->voucherName = $voucherName;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * @param int $statusId
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;
    }

    /**
     * @return bool
     */
    public function isRefunded()
    {
        return $this->isRefunded;
    }

    /**
     * @param bool $isRefunded
     */
    public function setIsRefunded($isRefunded)
    {
        $this->isRefunded = $isRefunded;
    }

    /**
     * @return float
     */
    public function getRefundValue()
    {
        return $this->refundValue;
    }

    /**
     * @param float $refundValue
     */
    public function setRefundValue($refundValue)
    {
        $this->refundValue = $refundValue;
    }

    /**
     * @return float
     */
    public function getRefundCreditValue()
    {
        return $this->refundCreditValue;
    }

    /**
     * @param float $refundCreditValue
     */
    public function setRefundCreditValue($refundCreditValue)
    {
        $this->refundCreditValue = $refundCreditValue;
    }


    /**
     * @return string
     */
    public function getRefundCustomerSupportLogin()
    {
        return $this->refundCustomerSupportLogin;
    }

    /**
     * @param string $refundCustomerSupportLogin
     */
    public function setRefundCustomerSupportLogin($refundCustomerSupportLogin)
    {
        $this->refundCustomerSupportLogin = $refundCustomerSupportLogin;
    }

    /**
     * @return string
     */
    public function getPaymentMediumId()
    {
        return $this->paymentMediumId;
    }

    /**
     * @param string $paymentMediumId
     */
    public function setPaymentMediumId($paymentMediumId)
    {
        $this->paymentMediumId = $paymentMediumId;
    }

    /**
     * @return User
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * @param User $agent
     */
    public function setAgent(User $agent)
    {
        $this->agent = $agent;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomerDetails(Customer $customer)
    {
        $this->setCustomerName($customer->getFullName());
        $this->setCustomerAddress($customer->getAddress('-'));
        $this->setCustomerPhone($customer->getPhone());
        $this->setCustomerEmail($customer->getEmail());
    }

    /**
     * @return Transaction
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * @param Transaction $transaction
     */
    public function setTransaction(Transaction $transaction)
    {
        $this->transaction = $transaction;
        $transaction->setOrder($this);
    }

    /**
     * @return DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @param DateTime $dtc
     */
    public function setDtc(DateTime $dtc)
    {
        $this->dtc = $dtc;
    }

    /**
     * @return DateTime
     */
    public function getDtm()
    {
        return $this->dtm;
    }

    /**
     * @param DateTime $dtm
     */
    public function setDtm(DateTime $dtm)
    {
        $this->dtm = $dtm;
    }

    /**
     * @return string|null
     */
    public function getPaymentMedium()
    {
        return Arrays::get(self::$paymentMediums, $this->paymentMediumId, NULL);
    }

    /**
     * @return array
     */
    public function getFeefoData()
    {
        // deduplicate product ids & titles
        $titles = [];
        $productIds = [];
        $link = FALSE;
        foreach ($this->getItems() as $item) {
            $productId = $item->getProductId();
            if (array_search($productId, $productIds) === FALSE) {
                $product = new BasketProduct($productId);
                if ($product->isFeefo()) {
                    $titles[] = $product->getLngTitle();
                    $productIds[] = $productId;
                    if (!$link) {
                        $link = $product->getLngFriendlyUrl();
                    }
                }
            }
        }

        return [
            'email' => $this->getCustomerEmail(),
            'name' => trim($this->getCustomerName()),
            'date' => $this->getDtc()->format('d/m/Y'),
            'description' => implode(', ', $titles),
            'orderref' => $this->getOrderId(),
            'itemref' => implode(', ', $productIds),
            'customerref' => $this->getCustomer()->getCustomerId(),
            'link' => 'http://' . FApplication::$config['host'] . '/' . $link,
        ];
    }

    /**
     * @return bool
     */
    public function isPaidBySagePay()
    {
        return $this->transaction && $this->transaction->isSagePay();
    }

    /**
     * @return bool
     */
    public function isPaidBySagePayToken()
    {
        return $this->isPaidBySagePay() && $this->transaction->hasToken();
    }

    /**
     * @return Service[]
     */
    public function getRenewableServices()
    {
        return $this->services->filter(
            function (Service $service) {
                return $service->isRenewable();
            }
        )->toArray();
    }

    /**
     * @return bool
     */
    public function containsRenewableServices()
    {
        foreach ($this->services as $service) {
            if ($service->isRenewable()) {
                return TRUE;
            }
        }

        return FALSE;
    }

    /**
     * @return bool
     */
    public function hasVoucher()
    {
        return (bool) $this->getVoucherId();
    }

    /**
     * @return bool
     */
    public function hasCredit()
    {
        return (bool) $this->getCredit();
    }

    /**
     * @param Order $order
     * @return bool
     */
    public function isEqual(Order $order)
    {
        return ($this->getId() === $order->getId());
    }

    /**
     * @return int
     */
    public function getDistinctProductCount()
    {
        return Collection::from($this->items)
            ->map(
                function (OrderItem $item) {
                    return $item->getProductId();
                }
            )
            ->distinct()
            ->size();
    }

    /**
     * @return bool
     */
    public function isPhoneOrder()
    {
        return $this->paymentMediumId == self::PAYMENT_MEDIUM_PHONE;
    }

    /**
     * @return bool
     */
    public function isComplimentaryOrder()
    {
        return $this->paymentMediumId == self::PAYMENT_MEDIUM_COMPLIMENTARY;
    }

    /**
     * @param Price $price
     */
    public function setPrice(Price $price)
    {
        $this->setRealSubTotal($price->getSubtotal());
        $this->setSubTotal($price->getSubtotal() - $price->getDiscount());
        $this->setDiscount($price->getDiscount());
        $this->setVat($price->getVat());
        $this->setTotal($price->getTotal());
    }
}
