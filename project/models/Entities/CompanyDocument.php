<?php

namespace Entities;

use Doctrine\ORM\Mapping as Orm;
use Gedmo\Mapping\Annotation as Gedmo;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Entities\CompanyHouse\FormSubmission;
use Entities\Register\Member;
use Doctrine\Common\Collections\Criteria;

/**
 * @Orm\Entity(repositoryClass = "Repositories\CompanyDocumentRepository")
 * @Orm\Table(name="ch_company_documents")
 */
class CompanyDocument extends EntityAbstract
{
    const TYPE_OTHER = 'OTHER';

    /**
     * @var array
     */
    public static $types = array(
        self::TYPE_OTHER => 'Other document',
    ) ;

    /**
     * @var integer
     * @Orm\Column(name="companyDocumentId", type="integer", nullable=false)
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $companyDocumentId;

    /**
     * @var Company
     * @Orm\ManyToOne(targetEntity="Company")
     * @Orm\JoinColumn(name="companyId", referencedColumnName="company_id")
     */
    private $company;

    /**
     * @var string
     * @Orm\Column(name="typeId", type="string", columnDefinition="ENUM('OTHER')")
     */
    private $typeId;

    /**
     * @var string
     * @Orm\Column(name="name", type="string", nullable=false)
     */
    private $name;

    /**
     * @var string
     * @Orm\Column(name="fileTitle", type="string", nullable=false)
     */
    private $fileTitle;

    /**
     * @var string
     * @Orm\Column(name="ext", type="string", nullable=false)
     */
    private $ext;

    /**
     * @var integer
     * @Orm\Column(name="size", type="integer", nullable=false)
     */
    private $size;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create", on="update")
     */
    private $dtm;

    private $storageDir;
    private $storageDirR;

    /**
     * @param Company $company
     */
    public function __construct(Company $company, $storageDir, $storageDirR)
    {
        $this->setCompany($company);
        $this->storageDir = $storageDir;
        $this->storageDirR = $storageDirR;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->companyDocumentId;
    }

    /**
     * @return int
     */
    public function getCompanyDocumentId()
    {
        return $this->companyDocumentId;
    }

    /**
     * @param int $companyDocumentId
     */
    public function setCompanyDocumentId($companyDocumentId)
    {
        $this->companyDocumentId = $companyDocumentId;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Company $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return string
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     * @param string $typeId
     */
    public function setTypeId($typeId)
    {
        $this->typeId = $typeId;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getFileTitle()
    {
        return $this->fileTitle;
    }

    /**
     * @param string $fileTitle
     */
    public function setFileTitle($fileTitle)
    {
        $this->fileTitle = $fileTitle;
    }

    /**
     * @return string
     */
    public function getExt()
    {
        return $this->ext;
    }

    /**
     * @param string $ext
     */
    public function setExt($ext)
    {
        $this->ext = $ext;
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param int $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @return DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @param DateTime $dtc
     */
    public function setDtc($dtc)
    {
        $this->dtc = $dtc;
    }

    /**
     * @return DateTime
     */
    public function getDtm()
    {
        return $this->dtm;
    }

    /**
     * @param DateTime $dtm
     */
    public function setDtm($dtm)
    {
        $this->dtm = $dtm;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return sprintf("%s.%s", $this->name, $this->ext);
    }

    /**
     * @return string
     */
    public function getRealFileName()
    {
        return sprintf("%s.%s", $this->fileTitle, $this->ext);
    }
}
