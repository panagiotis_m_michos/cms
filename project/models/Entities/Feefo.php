<?php

namespace Entities;

use Doctrine\ORM\Mapping as Orm;
use Gedmo\Mapping\Annotation as Gedmo;
use DateTime;

/**
 * @Orm\Entity(repositoryClass="Repositories\FeefoRepository")
 * @Orm\Table(name="cms2_feefo")
 */
class Feefo extends EntityAbstract
{
    const STATUS_WAITING = 'WAITING';
    const STATUS_SENT = 'SENT';
    const STATUS_ELIGIBLE = 'ELIGIBLE';

    /**
     * @var array 
     */
    public static $statuses = array(
        self::STATUS_WAITING => 'Waiting',
        self::STATUS_SENT => 'Sent',
        self::STATUS_ELIGIBLE => 'ELIGIBLE',
    );

    /**
     * @var integer
     *
     * @Orm\Column(type="integer")
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $feefoId;

    /**
     * @var Order
     *
     * @Orm\OneToOne(targetEntity="Order", cascade={"persist"})
     * @Orm\JoinColumn(name="orderId", referencedColumnName="orderId")
     */
    private $order;

    /**
     * @var string
     *
     * @Orm\Column(type="string", length=160, nullable=true)
     */
    private $statusId = self::STATUS_WAITING;

    /**
     * @var DateTime
     * 
     * @Orm\Column(type="datetime")
     */
    private $dateSent;

    /**
     * @var DateTime
     *
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->feefoId;
    }

    /**
     * @return int
     */
    public function getFeefoId()
    {
        return $this->feefoId;
    }

    /**
     * @param int $feefoId
     */
    public function setFeefoId($feefoId)
    {
        $this->feefoId = $feefoId;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param Order $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return string
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * @param string $statusId
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;
    }

    /**
     * @return DateTime
     */
    public function getDateSent()
    {
        return $this->dateSent;
    }

    /**
     * @param DateTime $dateSent
     */
    public function setDateSent($dateSent)
    {
        $this->dateSent = $dateSent;
    }

    /**
     * @return DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @param DateTime $dtc
     */
    public function setDtc($dtc)
    {
        $this->dtc = $dtc;
    }

    /**
     * @return string 
     */
    public function getStatus()
    {
        $key = $this->getStatusId();
        return (isset(self::$statuses[$key])) ? self::$statuses[$key] : NULL;
    }

    public function markAsSent()
    {
        $this->setStatusId(self::STATUS_SENT);
        $this->setDateSent(new DateTime());
    }

    public function markAsEligible()
    {
        $this->setStatusId(self::STATUS_ELIGIBLE);
    }
}
