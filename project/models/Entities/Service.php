<?php

namespace Entities;

use DateTimeInterface;
use Doctrine\ORM\Mapping as Orm;
use Gedmo\Mapping\Annotation as Gedmo;
use FNode;
use Product;
use IPackage;
use IIdentifier;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use IArrayConvertable;
use Utils\Date;

/**
 * @Orm\Entity(repositoryClass = "Repositories\ServiceRepository")
 * @Orm\Table(name = "cms2_services")
 */
class Service extends EntityAbstract implements IIdentifier, IArrayConvertable
{
    // types
    const TYPE_PACKAGE_PRIVACY = 'PACKAGE_PRIVACY';
    const TYPE_PACKAGE_COMPREHENSIVE_ULTIMATE = 'PACKAGE_COMPREHENSIVE_ULTIMATE';
    const TYPE_PACKAGE_BY_GUARANTEE = 'PACKAGE_BY_GUARANTEE';
    const TYPE_PACKAGE_SOLE_TRADER_PLUS = 'PACKAGE_SOLE_TRADER_PLUS';
    const TYPE_RESERVE_COMPANY_NAME = 'RESERVE_COMPANY_NAME';
    const TYPE_DORMANT_COMPANY_ACCOUNTS = 'DORMANT_COMPANY_ACCOUNTS';
    const TYPE_APOSTILLED_DOCUMENTS = 'APOSTILLED_DOCUMENTS';
    const TYPE_CERTIFICATE_OF_GOOD_STANDING = 'CERTIFICATE_OF_GOOD_STANDING';
    const TYPE_REGISTERED_OFFICE = 'REGISTERED_OFFICE';
    const TYPE_SERVICE_ADDRESS = 'SERVICE_ADDRESS';
    const TYPE_NOMINEE = 'NOMINEE';
    const TYPE_ANNUAL_RETURN = 'ANNUAL_RETURN';
    const TYPE_OFFSHORE = 'OFFSHORE';
    const TYPE_PSC_ONLINE_REGISTER = 'PSC_ONLINE_REGISTER';
    const TYPE_CONFIRMATION_STATEMEMT = 'CONFIRMATION_STATEMEMT';
    const TYPE_BUNDLE_PSC_ONLINE_REGISTER_CONFIRMATION_STATEMENT = 'BUNDLE_PSC_ONLINE_REGISTER_CONFIRMATION_STATEMENT';

    // durations
    const DURATION_ONE_YEAR = '+12 months';

    // statuses
    const STATUS_ON_HOLD = 'STATUS_ON_HOLD';
    const STATUS_ACTIVE = 'STATUS_ACTIVE';
    const STATUS_SUSPENDED = 'STATUS_SUSPENDED';
    const STATUS_OVERDUE = 'STATUS_OVERDUE';
    const STATUS_SOON_DUE = 'STATUS_SOON_DUE';
    const STATUS_EXPIRES_TODAY = 'STATUS_EXPIRES_TODAY';
    const STATUS_EXPIRED = 'STATUS_EXPIRED';
    const STATUS_UPGRADED = 'STATUS_UPGRADED';
    const STATUS_DOWNGRADED = 'STATUS_DOWNGRADED';

    // states [used for upgrade/downgrade]
    const STATE_ENABLED = 'ENABLED';
    const STATE_DISABLED = 'DISABLED';
    const STATE_UPGRADED = 'UPGRADED';
    const STATE_DOWNGRADED = 'DOWNGRADED';

    /**
     * @var array
     */
    public static $statuses = [
        self::STATUS_ON_HOLD => 'On Hold',
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_SUSPENDED => 'Suspended',
        self::STATUS_OVERDUE => 'Overdue',
        self::STATUS_SOON_DUE => 'Due in %s',
        self::STATUS_EXPIRES_TODAY => 'Expires Today',
        self::STATUS_EXPIRED => 'Expired',
        self::STATUS_UPGRADED => 'Upgraded',
        self::STATUS_DOWNGRADED => 'Downgraded',
    ];

    /**
     * @var array
     */
    public static $types = [
        self::TYPE_PACKAGE_PRIVACY => 'Package Privacy',
        self::TYPE_PACKAGE_COMPREHENSIVE_ULTIMATE => 'Package Comprehensive & Ultimate',
        self::TYPE_PACKAGE_BY_GUARANTEE => 'Package By Guarantee',
        self::TYPE_PACKAGE_SOLE_TRADER_PLUS => 'Package Sole Trader Plus',
        self::TYPE_RESERVE_COMPANY_NAME => 'Reserve a Company Name',
        self::TYPE_DORMANT_COMPANY_ACCOUNTS => 'Dormant Company Accounts',
        self::TYPE_APOSTILLED_DOCUMENTS => 'Apostilled Documents',
        self::TYPE_CERTIFICATE_OF_GOOD_STANDING => 'Certificate of Good Standing',
        self::TYPE_REGISTERED_OFFICE => 'Registered Office',
        self::TYPE_SERVICE_ADDRESS => 'Service Address',
        self::TYPE_NOMINEE => 'Nominee',
        self::TYPE_ANNUAL_RETURN => 'Annual Return',
        self::TYPE_PSC_ONLINE_REGISTER => 'PSC Online Register',
        self::TYPE_BUNDLE_PSC_ONLINE_REGISTER_CONFIRMATION_STATEMENT => 'Bundle PSC Online Register and Confirmation Statement',
    ];

    /**
     * @var array
     */
    public static $durations = [
        self::DURATION_ONE_YEAR => '12 Months',
    ];

    /**
     * @var integer
     * @Orm\Column(name="serviceId", type="integer")
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $serviceId;

    /**
     * @var Service
     *
     * @Orm\ManyToOne(targetEntity="Service", inversedBy="children")
     * @Orm\JoinColumn(name="parentId", referencedColumnName="serviceId")
     */
    private $parent;

    /**
     * @var string
     * @Orm\Column(type="string", length=100, nullable=false)
     */
    private $serviceTypeId;

    /**
     * @var int
     * @Orm\Column(type="integer", length=10, nullable=false)
     */
    private $productId;

    /**
     * @var int
     * @Orm\Column(type="integer", length=10, nullable=true)
     */
    private $renewalProductId = NULL;

    /**
     * @var string
     * @Orm\Column(type="string", length=255, nullable=true)
     */
    private $serviceName = NULL;

    /**
     * @var DateTime
     * @Orm\Column(type="date", nullable=true)
     */
    private $dtStart = NULL;

    /**
     * @var DateTime
     * @Orm\Column(type="date", nullable=true)
     */
    private $dtExpires = NULL;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=false)
     */
    private $stateId = self::STATE_ENABLED;

    /**
     * @var string
     * @Orm\Column(type="string", length=255, nullable=true)
     */
    private $initialDuration = self::DURATION_ONE_YEAR;

    /**
     * @var DateTime
     * @Orm\Column(type="date")
     */
    private $initialDtStart;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create", on="update")
     */
    private $dtm;

    /**
     * @var Order
     * @Orm\OneToOne(targetEntity="Order", cascade={"persist"})
     * @Orm\JoinColumn(name="orderId", referencedColumnName="orderId")
     */
    private $order;

    /**
     * @var OrderItem
     * @Orm\OneToOne(targetEntity="OrderItem", cascade={"persist"})
     * @Orm\JoinColumn(name="orderItemId", referencedColumnName="orderItemId")
     */
    private $orderItem;

    /**
     * @var Company
     * @Orm\ManyToOne(targetEntity="Company", inversedBy = "services", cascade={"persist"})
     * @Orm\JoinColumn(name="companyId", referencedColumnName="company_id")
     */
    private $company;

    /**
     * @var Product
     */
    private $product;

    /**
     * @var Product
     */
    private $renewalProduct;

    /**
     * @var Service[]|ArrayCollection
     * @Orm\OneToMany(targetEntity="Service", mappedBy="parent", cascade={"persist", "remove"})
     */
    private $children;

    /**
     * @param string $serviceTypeId
     * @param Product $product
     * @param Company $company
     * @param OrderItem $orderItem
     */
    public function __construct($serviceTypeId, Product $product, Company $company, OrderItem $orderItem)
    {
        $this->children = new ArrayCollection();

        $this->setServiceTypeId($serviceTypeId);
        $this->setProduct($product);
        $this->setInitialSettings($product);
        $this->setCompany($company);
        $this->setOrderItem($orderItem);
        $this->setOrder($orderItem->getOrder());
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->serviceId;
    }

    /**
     * @return Service
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return int
     */
    public function getServiceId()
    {
        return $this->serviceId;
    }

    /**
     * @return string
     */
    public function getServiceTypeId()
    {
        return $this->serviceTypeId;
    }

    /**
     * @return string
     */
    public function getServiceName()
    {
        return $this->serviceName;
    }

    /**
     * @return DateTime
     */
    public function getDtStart()
    {
        return $this->dtStart;
    }

    /**
     * @return DateTime
     */
    public function getDtExpires()
    {
        return $this->dtExpires;
    }

    /**
     * @param string $stateId
     */
    public function setStateId($stateId)
    {
        $this->stateId = $stateId;
    }

    /**
     * @return string
     */
    public function getStateId()
    {
        return $this->stateId;
    }

    /**
     * @return string
     */
    public function getInitialDuration()
    {
        return $this->initialDuration;
    }

    /**
     * @param string $initialDuration
     */
    public function setInitialDuration($initialDuration)
    {
        $this->initialDuration = $initialDuration;
    }

    /**
     * @return DateTimeInterface
     */
    public function getInitialDtStart()
    {
        return $this->initialDtStart;
    }

    /**
     * @param DateTimeInterface $initialDtStart
     */
    public function setInitialDtStart(DateTimeInterface $initialDtStart)
    {
        $this->initialDtStart = $initialDtStart;
    }

    /**
     * @param DateTime $dtc
     */
    public function setDtc(DateTime $dtc)
    {
        $this->dtc = $dtc;
    }

    /**
     * @return DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @param DateTime $dtm
     */
    public function setDtm($dtm)
    {
        $this->dtm = $dtm;
    }

    /**
     * @return DateTime
     */
    public function getDtm()
    {
        return $this->dtm;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @return OrderItem
     */
    public function getOrderItem()
    {
        return $this->orderItem;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        if (!$this->product) {
            $this->setProduct(FNode::getProductById($this->productId));
        }
        return $this->product;
    }

    /**
     * @return Product
     */
    public function getRenewalProduct()
    {
        if (!$this->renewalProduct) {
            $product = Product::getProductById($this->renewalProductId);
            $product->linkedCompanyStatus = Product::LINK_EXISTING_SELECT;
            $product->setCompanyId($this->getCompany()->getId());
            $this->setRenewalProduct($product);
        }
        return $this->renewalProduct;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
        $this->unsetInitialSettings();
    }

    /**
     * @param string $serviceTypeId
     */
    public function setServiceTypeId($serviceTypeId)
    {
        $this->serviceTypeId = $serviceTypeId;
    }

    /**
     * @param DateTimeInterface $dtStart
     */
    public function setDtStart(DateTimeInterface $dtStart)
    {
        $this->dtStart = $dtStart;
    }

    /**
     * @param DateTimeInterface $dtExpires
     */
    public function setDtExpires(DateTimeInterface $dtExpires)
    {
        $this->dtExpires = $dtExpires;
    }

    /**
     * @param Order $order
     */
    public function setOrder(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @param OrderItem $orderItem
     */
    public function setOrderItem(OrderItem $orderItem)
    {
        $this->orderItem = $orderItem;
        foreach ($this->children as $child) {
            $child->setOrderItem($orderItem);
        }
    }

    /**
     * @param Company $company
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @param Product $product
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;
        $this->productId = $product->getId();
        $this->serviceName = $product->lngTitle;
    }

    /**
     * @param Product $renewalProduct
     */
    public function setRenewalProduct(Product $renewalProduct = NULL)
    {
        if ($renewalProduct) {
            $this->renewalProduct = $renewalProduct;
            $this->renewalProductId = $renewalProduct->getId();
        } else {
            $this->renewalProduct = NULL;
            $this->renewalProductId = NULL;
        }
    }

    /**
     * @return string
     */
    public function getServiceType()
    {
        $typeId = $this->getServiceTypeId();
        $types = (isset(self::$types[$typeId])) ? self::$types[$typeId] : NULL;
        return $types;
    }

    /**
     * @return bool
     */
    public function hasDates()
    {
        if ($this->hasParent()) {
            return $this->parent->hasDates();
        }
        return !empty($this->dtStart) && !empty($this->dtExpires);
    }

    /**
     * @return bool
     */
    public function isOnHold()
    {
        return $this->isOnHoldOn(new DateTime);
    }

    /**
     * @param DateTime $date
     * @return bool
     */
    public function isOnHoldOn(DateTime $date)
    {
        if ($this->hasParent()) {
            return $this->parent->isOnHold();
        }

        if (!$this->hasDates()) {
            // one off service is not on hold if there is at least dtStart set
            if ($this->isOneOff() && !empty($this->dtStart)) {
                return FALSE;
            }
            return TRUE;
        }

        return $this->dtStart > $date && $this->dtExpires > $date;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->isActiveOn(new DateTime);
    }

    /**
     * @param DateTime $date
     * @return bool
     */
    public function isActiveOn(DateTime $date)
    {
        if ($this->hasParent()) {
            return $this->parent->isActiveOn($date);
        }

        $activeEnd = clone $date;
        $activeEnd->modify('-28 days 00:00:00');

        // one off service isActive when there is dtStart and no dtExpires as well
        if ($this->isOneOff()) {
            $active = $this->dtStart && $this->dtStart <= $date;
            return $this->dtExpires ? $this->dtExpires >= $date && $active : $active;
        }

        return $this->hasDates() && $this->dtStart <= $date && $this->dtExpires >= $activeEnd;
    }

    /**
     * @return bool
     */
    public function isOverdue()
    {
        return $this->isOverdueOn(new DateTime);
    }

    /**
     * @param DateTime $date
     * @return bool
     */
    public function isOverdueOn(DateTime $date)
    {
        if ($this->isOneOff()) {
            return FALSE;
        }

        return $this->isActiveOn($date) && $date >= $this->getDtExpires();
    }

    /**
     * @return bool
     */
    public function isExpired()
    {
        return $this->isExpiredOn(new DateTime);
    }

    /**
     * @param DateTime $date
     * @return bool
     */
    public function isExpiredOn(DateTime $date)
    {
        if ($this->hasParent()) {
            return $this->parent->isExpiredOn($date);
        }

        return $this->hasDates() && $this->dtExpires < $date;
    }

    /**
     * @return bool
     */
    public function isActivatedAndNotExpired()
    {
        return $this->isActivatedAndNotExpiredOn(new DateTime);
    }

    /**
     * @param DateTime $date
     * @return bool
     */
    public function isActivatedAndNotExpiredOn(DateTime $date)
    {
        return $this->hasDates() && !$this->isExpiredOn($date);
    }

    /**
     * @return bool
     */
    public function isActivatedAndExpired()
    {
        return $this->isActivatedAndExpiredOn(new DateTime);
    }

    /**
     * @param DateTime $date
     * @return bool
     */
    public function isActivatedAndExpiredOn(DateTime $date)
    {
        return $this->hasDates() && $this->isExpiredOn($date);
    }

    /**
     * @param DateTime $date
     * @return bool
     */
    public function isExpiringAfter(DateTime $date)
    {
        return $this->hasDates() && ($this->isActiveOn($date) || $this->isOnHoldOn($date));
    }

    /**
     * @return Service[]|ArrayCollection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param Service $service
     */
    public function addChild(Service $service)
    {
        $service->setParent($this);
        $this->children[] = $service;
    }

    /**
     * Clears the parent id and makes it null.
     */
    public function clearParent()
    {
        $this->parent = NULL;
    }

    /**
     * @return bool
     */
    public function hasParent()
    {
        return !empty($this->parent);
    }

    /**
     * @return bool
     */
    public function isPackageType()
    {
        if ($this->hasParent()) {
            return $this->parent->isPackageType();
        }
        $product = $this->getProduct();
        return $product instanceof IPackage ? TRUE : FALSE;
    }

    /**
     * @return bool
     */
    public function isProductType()
    {
        return !$this->isPackageType();
    }

    /**
     * @return bool
     */
    public function hasRenewalProduct()
    {
        return !empty($this->renewalProductId) || !empty($this->renewalProduct);
    }

    /**
     * @return bool
     */
    public function isRenewable()
    {
        return $this->hasRenewalProduct();
    }

    /**
     * @return bool
     */
    public function isOneOff()
    {
        return !$this->hasRenewalProduct();
    }

    /**
     * @return bool
     */
    public function isDisabled()
    {
        return $this->getStateId() === self::STATE_DISABLED;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->getStateId() === self::STATE_ENABLED;
    }

    /**
     * @return bool
     */
    public function isUpgraded()
    {
        return $this->getStateId() === self::STATE_UPGRADED;
    }

    /**
     * @return bool
     */
    public function isDowngraded()
    {
        return $this->getStateId() === self::STATE_DOWNGRADED;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = [];
        $array['serviceName'] = $this->getServiceName();
        $array['dtStart'] = $this->getDtStart() ? $this->getDtStart()->format('d/m/Y') : NULL;
        $array['dtExpires'] = $this->getDtExpires() ? $this->getDtExpires()->format('d/m/Y') : NULL;
        return $array;
    }

    /**
     * @param Service $service
     * @return bool
     */
    public function isEqual(Service $service)
    {
        return ($this->company->isEqual($service->getCompany()))
            && ($this->serviceTypeId == $service->getServiceTypeId())
            && ($this->dtStart == $service->getDtStart())
            && ($this->dtExpires == $service->getDtExpires());
    }

    /**
     * @param Service $service
     * @return bool
     */
    public function isEqualType(Service $service)
    {
        return $this->serviceTypeId == $service->getServiceTypeId();
    }

    /**
     * @return bool
     */
    public function hasChildren()
    {
        return $this->children->isEmpty() === FALSE;
    }

    /**
     * @return string
     */
    public function getDaysToExpire()
    {
        $now = new Date();

        return (int) $now->diff($this->getDtExpires())->format("%r%a");
    }

    /**
     * @return int
     */
    public function getDaysAfterExpire()
    {
        return (int) $this->getDtExpires()->diff(new Date)->format("%r%a");
    }

    /**
     * @return DateTime|NULL
     */
    public function getRenewalProductExpiryDate()
    {
        if ($this->hasRenewalProduct()) {
            $renewalProductDuration = $this->getRenewalProduct()->getDuration();
            $renewalDtExpires = ($this->isExpired() ? new Date : clone $this->getDtExpires());
            $renewalDtExpires->modify($renewalProductDuration);
            $renewalDtExpires->modify('-1 day');

            return $renewalDtExpires;
        }
    }

    /**
     * @param string $serviceTypeId
     * @return bool
     */
    public function containsType($serviceTypeId)
    {
        foreach ($this->getChildren() as $child) {
            if ($child->getServiceTypeId() === $serviceTypeId) {
                return TRUE;
            }
        }

        return FALSE;
    }

    /**
     * @return bool
     */
    public function isAutoRenewalAllowed()
    {
        return $this->getProduct()->isAutoRenewalAllowed;
    }

    /**
     * @return bool
     */
    public function expiresToday()
    {
        if ($this->isOneOff()) {
            return FALSE;
        }

        return $this->getDaysToExpire() == 0;
    }

    /**
     * @param Product $product
     */
    private function setInitialSettings(Product $product)
    {
        $this->initialDuration = $product->getDuration();
        $this->initialDtStart = $product->getActivateFrom();
    }

    private function unsetInitialSettings()
    {
        $this->initialDuration = NULL;
        $this->initialDtStart = NULL;
    }
}
