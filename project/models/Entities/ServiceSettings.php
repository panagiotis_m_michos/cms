<?php

namespace Entities;

use Doctrine\ORM\Mapping as Orm;
use Entities\Payment\Token;
use Gedmo\Mapping\Annotation as Gedmo;
use JsonSerializable;
use DateTime;

/**
 * @Orm\Entity(repositoryClass = "Repositories\ServiceSettingsRepository")
 * @Orm\Table(name = "cms2_service_settings")
 * @Gedmo\Loggable(logEntryClass = "LoggableModule\Entities\LogEntry")
 */
class ServiceSettings extends EntityAbstract implements JsonSerializable
{

    /**
     * @var integer
     * @Orm\Column(name="serviceSettingId", type="integer")
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Company
     * @Orm\ManyToOne(targetEntity="Company")
     * @Orm\JoinColumn(name="companyId", referencedColumnName="company_id")
     * @Gedmo\Versioned
     */
    private $company;

    /**
     * @var integer
     * @Orm\Column(name="serviceTypeId", type="string")
     * @Gedmo\Versioned
     */
    private $serviceTypeId;

    /**
     * @var bool
     * @Orm\Column(type="boolean")
     * @Gedmo\Versioned
     */
    private $isAutoRenewalEnabled = FALSE;

    /**
     * @var Token
     * @Orm\ManyToOne(targetEntity="Entities\Payment\Token")
     * @Orm\JoinColumn(name="renewalTokenId", referencedColumnName="tokenId")
     * @Gedmo\Versioned
     */
    private $renewalToken;

    /**
     * @var bool
     * @Orm\Column(type="boolean")
     * @Gedmo\Versioned
     */
    private $emailReminders = TRUE;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create", on="update")
     */
    private $dtm;

    /**
     * @param Company $company
     * @param string $serviceTypeId
     */
    public function __construct(Company $company, $serviceTypeId)
    {
        $this->company = $company;
        $this->serviceTypeId = $serviceTypeId;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return int
     */
    public function getServiceTypeId()
    {
        return $this->serviceTypeId;
    }

    /**
     * @return bool
     */
    public function isAutoRenewalEnabled()
    {
        return $this->isAutoRenewalEnabled;
    }
    /**
     * @param bool $isAutoRenewalEnabled
     */
    public function setIsAutoRenewalEnabled($isAutoRenewalEnabled)
    {
        $this->isAutoRenewalEnabled = $isAutoRenewalEnabled;
    }

    /**
     * @return Token
     */
    public function getRenewalToken()
    {
        return $this->renewalToken;
    }

    /**
     * @param Token $renewalToken
     */
    public function setRenewalToken(Token $renewalToken = NULL)
    {
        $this->renewalToken = $renewalToken;
    }

    /**
     * @return boolean
     */
    public function hasEmailReminders()
    {
        return $this->emailReminders;
    }

    /**
     * @param boolean $enable
     */
    public function enableEmailReminders($enable = TRUE)
    {
        $this->emailReminders = $enable;
    }

    /**
     * @return boolean
     */
    public function isEmailReminders()
    {
        return $this->emailReminders;
    }

    /**
     * @return DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @return DateTime
     */
    public function getDtm()
    {
        return $this->dtm;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'emailReminders' => $this->hasEmailReminders(),
            'canDisableEmailReminders' => !$this->isAutoRenewalEnabled(),
        ];
    }
}
