<?php

namespace Entities;

interface EntityInterface
{

    public function getId();

    public function setId($id);
}
