<?php

namespace Entities;

use Doctrine\ORM\Mapping as Orm;
use Gedmo\Mapping\Annotation as Gedmo;
use DateTime;

/**
 * User
 * 
 * @Orm\Entity(repositoryClass = "Repositories\UserRepository")
 * @Orm\Table(name="cms2_users")
 */
class User extends EntityAbstract
{
    /**
     * @var integer
     *
     * @Orm\Column(name="user_id", type="integer")
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $userId;
    
    /**
     * @var integer
     *
     * @Orm\Column(name="status_id", type="integer")
     */
    private $statusId;
    
    /**
     * @var string
     *
     * @Orm\Column(type="string")
     */
    private $login;
    
    /**
     * @var string
     *
     * @Orm\Column(type="string")
     */
    private $password;
    
    /**
     * @var string
     *
     * @Orm\Column(name="first_name", type="string")
     */
    private $firstName;
    
    /**
     * @var string
     *
     * @Orm\Column(name="last_name", type="string")
     */
    private $lastName;
    
    /**
     * @var string
     *
     * @Orm\Column(type="string")
     */
    private $email;
    
    /**
     * @var integer
     *
     * @Orm\Column(name="role_id", type="integer")
     */
    private $roleId;
    
    /**
     * @var string
     *
     * @Orm\Column(name="language_pk", type="string")
     */
    private $languagePk;
    
    /**
     * @var integer
     *
     * @Orm\Column(name="author_id", type="integer")
     */
    private $authorId;
    
    /**
     * @var integer
     *
     * @Orm\Column(name="editor_id", type="integer")
     */
    private $editorId;
    
    /**
     * @var bool
     *
     * @Orm\Column(type="boolean")
     */
    private $hasPhonePaymentAccess;
    
     /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create", on="update")
     */
    private $dtm;
    
    /**
     * Alias
     * 
     * @return int
     */
    public function getId()
    {
        return $this->userId;
    }
    
    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return int
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return int
     */
    public function getRoleId()
    {
        return $this->roleId;
    }

    /**
     * @return string
     */
    public function getLanguagePk()
    {
        return $this->languagePk;
    }

    /**
     * @return int
     */
    public function getAuthorId()
    {
        return $this->authorId;
    }

    /**
     * @return int
     */
    public function getEditorId()
    {
        return $this->editorId;
    }

    /**
     * @return bool
     */
    public function getHasPhonePaymentAccess()
    {
        return $this->hasPhonePaymentAccess;
    }
    
    /**
     * @return DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @return DateTime
     */
    public function getDtm()
    {
        return $this->dtm;
    }

    /**
     * @param int $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }
    
    /**
     * @param int $statusId
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;
    }

    /**
     * @param string $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param int $roleId
     */
    public function setRoleId($roleId)
    {
        $this->roleId = $roleId;
    }

    /**
     * @param string $languagePk
     */
    public function setLanguagePk($languagePk)
    {
        $this->languagePk = $languagePk;
    }

    /**
     * @param int $authorId
     */
    public function setAuthorId($authorId)
    {
        $this->authorId = $authorId;
    }

    /**
     * @param int $editorId
     */
    public function setEditorId($editorId)
    {
        $this->editorId = $editorId;
    }

    /**
     * @param bool $hasPhonePaymentAccess
     */
    public function setHasPhonePaymentAccess($hasPhonePaymentAccess)
    {
        $this->hasPhonePaymentAccess = $hasPhonePaymentAccess;
    }
    
    /**
     * @return string
     */
    public function getFullName()
    {
        return sprintf("%s %s", $this->firstName, $this->lastName);
    }
}
