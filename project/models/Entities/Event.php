<?php

namespace Entities;

use Doctrine\ORM\Mapping as Orm;
use Gedmo\Mapping\Annotation as Gedmo;
use DateTime;
use Nette\Object;

/**
 * @Orm\Table(name="cms2_events")
 * @Orm\Entity(repositoryClass="Repositories\EventRepository")
 */
class Event extends Object
{
    /**
     * @var integer
     *
     * @Orm\Column(name="eventId", type="integer", nullable=false)
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $eventId;

    /**
     * @var string
     *
     * @Orm\Column(name="eventKey", type="string", length=25, nullable=true)
     */
    private $eventKey;

    /**
     * @var string
     *
     * @Orm\Column(name="objectId", type="integer", nullable=false)
     */
    private $objectId;

    /**
     * @var DateTime
     * 
     * @Orm\Column(name="dtc", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;
    

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->eventId;
    }

    /**
     * @return integer
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    /**
     * @return string $emailName
     */
    public function getEventKey()
    {
        return $this->eventKey;
    }

    /**
     * @param string $eventKey
     */
    public function setEventKey($eventKey)
    {
        return $this->eventKey = $eventKey;
    }

    /**
     * @return string
     */
    public function getObjectId()
    {
        return $this->objectId;
    }

    /**
     * @param string $objectId
     */
    public function setObjectId($objectId)
    {
        return $this->objectId = $objectId;
    }

    /**
     * @return DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @param DateTime $dtc
     */
    public function setDtc(DateTime $dtc)
    {
        return $this->dtc = $dtc;
    }
}
