<?php

namespace Entities;

use Doctrine\ORM\Mapping as Orm;
use Entities\Payment\Token;
use Gedmo\Mapping\Annotation as Gedmo;
use Nette\Object;
use Transaction as OldTransaction;
use DateTime;

/**
 * Transaction
 *
 * @Orm\Table(name="cms2_transactions")
 * @Orm\Entity(repositoryClass = "Repositories\TransactionRepository")
 */
class Transaction extends Object
{

    const STATUS_SUCCEEDED = OldTransaction::STATUS_SUCCEEDED;
    const STATUS_FAILED = OldTransaction::STATUS_FAILED;
        
    // types
    const TYPE_PAYPAL = OldTransaction::TYPE_PAYPAL;
    const TYPE_GOOGLE = OldTransaction::TYPE_GOOGLE;
    const TYPE_WORLDPAY = OldTransaction::TYPE_WORLDPAY;
    const TYPE_CREDIT = OldTransaction::TYPE_CREDIT;
    const TYPE_FREE = OldTransaction::TYPE_FREE;
    const TYPE_SAGEPAY = OldTransaction::TYPE_SAGEPAY;
    const TYPE_PAYPAL_REFUND =OldTransaction::TYPE_PAYPAL_REFUND;
    const TYPE_GOOGLE_REFUND = OldTransaction::TYPE_GOOGLE_REFUND;
    const TYPE_SAGEPAY_REFUND = OldTransaction::TYPE_SAGEPAY_REFUND;
    const ON_ACCOUNT = OldTransaction::ON_ACCOUNT;

    /**
     * @var integer
     *
     * @Orm\Column(name="transactionId", type="integer", nullable=false)
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $transactionId;

    /**
     * @var string
     *
     * @Orm\Column(name="statusId", type="string", nullable=false)
     */
    private $statusId = self::STATUS_SUCCEEDED;

    /**
     * @var integer
     *
     * @Orm\Column(name="typeId", type="integer", nullable=false)
     */
    private $typeId;

    /**
     * @var string
     *
     * @Orm\Column(name="cardHolder", type="string", length=255, nullable=true)
     */
    private $cardHolder;

    /**
     * @var string
     *
     * @Orm\Column(name="cardNumber", type="string", length=255, nullable=true)
     */
    private $cardNumber;

    /**
     * @var string
     *
     * @Orm\Column(name="orderCode", type="string", length=255, nullable=true)
     */
    private $orderCode;

    /**
     * @var string
     *
     * @Orm\Column(name="error", type="string", length=255, nullable=true)
     */
    private $error;

    /**
     * @var DateTime
     *
     * @Orm\Column(name="dtc", type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;

    /**
     * @var string
     *
     * @Orm\Column(name="details", type="text", nullable=true)
     */
    private $details;

    /**
     * @var string
     *
     * @Orm\Column(name="request", type="text", nullable=true)
     */
    private $request;

    /**
     * @var string
     *
     * @Orm\Column(name="vendorTXCode", type="string", length=255, nullable=true)
     */
    private $vendorTXCode;

    /**
     * @var string
     *
     * @Orm\Column(name="vpsAuthCode", type="string", length=255, nullable=true)
     */
    private $vpsAuthCode;

    /**
     * @var Customer
     * @Orm\ManyToOne(targetEntity="Customer", cascade={"persist"})
     * @Orm\JoinColumn(name="customerId", referencedColumnName="customerId")
     */
    private $customer;

    /**
     * @var Order
     * @Orm\OneToOne(targetEntity="Order", inversedBy="transaction", cascade={"persist"})
     * @Orm\JoinColumn(name="orderId", referencedColumnName="orderId")
     */
    private $order;

    /**
     * @var Token
     * @Orm\ManyToOne(targetEntity="Entities\Payment\Token")
     * @Orm\JoinColumn(name="tokenId", referencedColumnName="tokenId")
     */
    private $token;

    /**
     * @param Customer $customer
     * @param mixed $typeId
     */
    public function __construct(Customer $customer, $typeId)
    {
        $this->setCustomer($customer);
        $this->setTypeId($typeId);
    }

    /**
     * @return integer 
     */
    public function getId()
    {
        return $this->transactionId;
    }

    /**
     * Get transactionId
     *
     * @return integer 
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * Set statusId
     *
     * @param string $statusId
     * @return Transaction
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;
    
        return $this;
    }

    /**
     * Get statusId
     *
     * @return string 
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * Set typeId
     *
     * @param integer $typeId
     * @return Transaction
     */
    public function setTypeId($typeId)
    {
        $this->typeId = $typeId;
    
        return $this;
    }

    /**
     * Get typeId
     *
     * @return integer 
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     * Set cardHolder
     *
     * @param string $cardHolder
     * @return Transaction
     */
    public function setCardHolder($cardHolder)
    {
        $this->cardHolder = $cardHolder;
    
        return $this;
    }

    /**
     * Get cardHolder
     *
     * @return string 
     */
    public function getCardHolder()
    {
        return $this->cardHolder;
    }

    /**
     * Set cardNumber
     *
     * @param string $cardNumber
     * @return Transaction
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;
    
        return $this;
    }

    /**
     * Get cardNumber
     *
     * @return string 
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * Set orderCode
     *
     * @param string $orderCode
     * @return Transaction
     */
    public function setOrderCode($orderCode)
    {
        $this->orderCode = $orderCode;
    
        return $this;
    }

    /**
     * Get orderCode
     *
     * @return string 
     */
    public function getOrderCode()
    {
        return $this->orderCode;
    }

    /**
     * Set error
     *
     * @param string $error
     * @return Transaction
     */
    public function setError($error)
    {
        $this->error = $error;
    
        return $this;
    }

    /**
     * Get error
     *
     * @return string 
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * Set dtc
     *
     * @param DateTime $dtc
     * @return Transaction
     */
    public function setDtc(DateTime $dtc)
    {
        $this->dtc = $dtc;
    
        return $this;
    }

    /**
     * Get dtc
     *
     * @return DateTime 
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * Set details
     *
     * @param string $details
     * @return Transaction
     */
    public function setDetails($details)
    {
        $this->details = $details;
    
        return $this;
    }

    /**
     * Get details
     *
     * @return string 
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * Set request
     *
     * @param string $request
     * @return Transaction
     */
    public function setRequest($request)
    {
        $this->request = $request;
    
        return $this;
    }

    /**
     * Get request
     *
     * @return string 
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Set vendorTXCode
     *
     * @param string $vendorTXCode
     * @return Transaction
     */
    public function setVendorTXCode($vendorTXCode)
    {
        $this->vendorTXCode = $vendorTXCode;
    
        return $this;
    }

    /**
     * Get vendorTXCode
     *
     * @return string 
     */
    public function getVendorTXCode()
    {
        return $this->vendorTXCode;
    }

    /**
     * Set vpsAuthCode
     *
     * @param string $vpsAuthCode
     * @return Transaction
     */
    public function setVpsAuthCode($vpsAuthCode)
    {
        $this->vpsAuthCode = $vpsAuthCode;
    
        return $this;
    }

    /**
     * Get vpsAuthCode
     *
     * @return string 
     */
    public function getVpsAuthCode()
    {
        return $this->vpsAuthCode;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     * @return Customer
     */
    public function setCustomer(Customer $customer)
    {
        return $this->customer = $customer;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param Order $order
     * @return Order
     */
    public function setOrder(Order $order)
    {
        return $this->order = $order;
    }

    /**
     * @return Token
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param Token $token
     * @return Token
     */
    public function setToken(Token $token)
    {
        return $this->token = $token;
    }

    /**
     * @return bool
     */
    public function isSagePay()
    {
        return $this->typeId == self::TYPE_SAGEPAY;
    }

    /**
     * @return bool
     */
    public function hasToken()
    {
        return (bool) $this->token;
    }
}
