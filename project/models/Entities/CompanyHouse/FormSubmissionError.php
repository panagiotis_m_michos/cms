<?php

namespace Entities\CompanyHouse;

use DateTime;
use Doctrine\ORM\Mapping as Orm;
use Gedmo\Mapping\Annotation as Gedmo;
use Entities\EntityAbstract;

/**
 * @Orm\Entity(repositoryClass = "Repositories\CompanyHouse\FormSubmissionErrorRepository")
 * @Orm\Table(name="ch_form_submission_error")
 */
class FormSubmissionError extends EntityAbstract
{

    /**
     * @var integer
     * @Orm\Column(name="form_submission_error_id", type="integer", nullable=false)
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $formSubmissionErrorId;

    /**
     * @var FormSubmission
     * @Orm\ManyToOne(targetEntity="FormSubmission", inversedBy = "formSubmissionErrors", cascade={"persist"})
     * @Orm\JoinColumn(name="form_submission_id", referencedColumnName="form_submission_id")
     */
    private $formSubmission;

    /**
     * @var integer
     * @Orm\Column(name="reject_code", type="integer", nullable=false)
     */
    private $rejectCode;

    /**
     * @var string
     * @Orm\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @var string
     * @Orm\Column(name="instance_number", type="string", length=12, nullable=true)
     */
    private $instanceNumber;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create", on="update")
     */
    private $dtm;

    /**
     * @param FormSubmission $formSubmission
     * @param string $rejectCode
     * @param string $description
     */
    public function __construct(FormSubmission $formSubmission, $rejectCode, $description)
    {
        $this->setFormSubmission($formSubmission);
        $this->setRejectCode($rejectCode);
        $this->setDescription($description);
    }

    /**
     * @return integer
     */
    public function getFormSubmissionErrorId()
    {
        return $this->formSubmissionErrorId;
    }

    /**
     * @return FormSubmission
     */
    public function getFormSubmission()
    {
        return $this->formSubmission;
    }

    /**
     * @param FormSubmission $formSubmission
     */
    public function setFormSubmission(FormSubmission $formSubmission)
    {
        $this->formSubmission = $formSubmission;
    }

    /**
     * @return integer
     */
    public function getRejectCode()
    {
        return $this->rejectCode;
    }

    /**
     * @param integer $rejectCode
     * @return FormSubmissionError
     */
    public function setRejectCode($rejectCode)
    {
        $this->rejectCode = $rejectCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return FormSubmissionError
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getInstanceNumber()
    {
        return $this->instanceNumber;
    }

    /**
     * @param string $instanceNumber
     * @return FormSubmissionError
     */
    public function setInstanceNumber($instanceNumber)
    {
        $this->instanceNumber = $instanceNumber;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @param DateTime $dtc
     * @return FormSubmissionError
     */
    public function setDtc($dtc)
    {
        $this->dtc = $dtc;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDtm()
    {
        return $this->dtm;
    }

    /**
     * @param DateTime $dtm
     * @return FormSubmissionError
     */
    public function setDtm($dtm)
    {
        $this->dtm = $dtm;

        return $this;
    }
}
