<?php

namespace Entities\CompanyHouse\Incorporation;

use Nette\Object;

class Company extends Object
{
    const TYPE_BYSHR = 'BYSHR';
    const TYPE_PLC = 'PLC';
}
