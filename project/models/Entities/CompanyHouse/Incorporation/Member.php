<?php

namespace Entities\CompanyHouse\Incorporation;

use Nette\Object;

class Member extends Object
{
    const TYPE_DIRECTOR = 'DIR';
}
