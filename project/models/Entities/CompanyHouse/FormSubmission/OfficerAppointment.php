<?php

namespace Entities\CompanyHouse\FormSubmission;

use Doctrine\ORM\Mapping as ORM;
use Entities\CompanyHouse\FormSubmission;
use DateTime;

/**
 * @Orm\Table(name="ch_officer_appointment")
 * @Orm\Entity(repositoryClass = "Repositories\CompanyHouse\FormSubmission\OfficerAppointmentRepository")
 */
class OfficerAppointment extends FormSubmission
{

    /**
     * @var string
     * @Orm\Column(name="type", type="string", nullable=false)
     */
    private $type;

    /**
     * @var boolean
     * @Orm\Column(name="corporate", type="boolean", nullable=false)
     */
    private $corporate;

    /**
     * @var boolean
     * @Orm\Column(name="designated_ind", type="boolean", nullable=true)
     */
    private $designatedInd;

    /**
     * @var DateTime
     * @Orm\Column(name="appointment_date", type="date", nullable=false)
     */
    private $appointmentDate;

    /**
     * @var string
     * @Orm\Column(name="title", type="string", length=50, nullable=true)
     */
    private $title;

    /**
     * @var string
     * @Orm\Column(name="forename", type="string", length=50, nullable=true)
     */
    private $forename;

    /**
     * @var string
     * @Orm\Column(name="middle_name", type="string", length=50, nullable=true)
     */
    private $middleName;

    /**
     * @var string
     * @Orm\Column(name="surname", type="string", length=160, nullable=false)
     */
    private $surname;

    /**
     * @var string
     * @Orm\Column(name="corporate_name", type="string", length=160, nullable=true)
     */
    private $corporateName;

    /**
     * @var string
     * @Orm\Column(name="authentication", type="string", length=45, nullable=true)
     */
    private $authentication;

    /**
     * @var bool
     * @Orm\Column(name="consentToAct", type="boolean")
     */
    private $consentToAct = FALSE;

    /**
     * @var string
     * @Orm\Column(name="premise", type="string", length=50, nullable=false)
     */
    private $premise;

    /**
     * @var string
     * @Orm\Column(name="street", type="string", length=50, nullable=false)
     */
    private $street;

    /**
     * @var string
     * @Orm\Column(name="thoroughfare", type="string", length=50, nullable=true)
     */
    private $thoroughfare;

    /**
     * @var string
     * @Orm\Column(name="post_town", type="string", length=50, nullable=true)
     */
    private $postTown;

    /**
     * @var string
     * @Orm\Column(name="county", type="string", length=50, nullable=true)
     */
    private $county;

    /**
     * @var string
     * @Orm\Column(name="country", type="string", length=50, nullable=true)
     */
    private $country;

    /**
     * @var string
     * @Orm\Column(name="postcode", type="string", length=15, nullable=true)
     */
    private $postcode;

    /**
     * @var string
     * @Orm\Column(name="care_of_name", type="string", length=100, nullable=true)
     */
    private $careOfName;

    /**
     * @var string
     * @Orm\Column(name="po_box", type="string", length=10, nullable=true)
     */
    private $poBox;

    /**
     * @var DateTime
     * @Orm\Column(name="dob", type="date", nullable=true)
     */
    private $dob;

    /**
     * @var string
     * @Orm\Column(name="nationality", type="string", length=20, nullable=true)
     */
    private $nationality;

    /**
     * @var string
     * @Orm\Column(name="occupation", type="string", length=35, nullable=true)
     */
    private $occupation;

    /**
     * @var string
     * @Orm\Column(name="country_of_residence", type="string", length=50, nullable=true)
     */
    private $countryOfResidence;

    /**
     * @var string
     * @Orm\Column(name="residential_premise", type="string", length=50, nullable=true)
     */
    private $residentialPremise;

    /**
     * @var string
     * @Orm\Column(name="residential_street", type="string", length=50, nullable=true)
     */
    private $residentialStreet;

    /**
     * @var string
     * @Orm\Column(name="residential_thoroughfare", type="string", length=50, nullable=true)
     */
    private $residentialThoroughfare;

    /**
     * @var string
     * @Orm\Column(name="residential_post_town", type="string", length=50, nullable=true)
     */
    private $residentialPostTown;

    /**
     * @var string
     * @Orm\Column(name="residential_county", type="string", length=50, nullable=true)
     */
    private $residentialCounty;

    /**
     * @var string
     * @Orm\Column(name="residential_country", type="string", length=50, nullable=true)
     */
    private $residentialCountry;

    /**
     * @var string
     * @Orm\Column(name="residential_postcode", type="string", length=15, nullable=true)
     */
    private $residentialPostcode;

    /**
     * @var boolean
     * @Orm\Column(name="residential_secure_address_ind", type="boolean", nullable=true)
     */
    private $residentialSecureAddressInd;

    /**
     * @var string
     * @Orm\Column(name="identification_type", type="string", nullable=true)
     */
    private $identificationType;

    /**
     * @var string
     * @Orm\Column(name="place_registered", type="string", length=50, nullable=true)
     */
    private $placeRegistered;

    /**
     * @var string
     * @Orm\Column(name="registration_number", type="string", length=20, nullable=true)
     */
    private $registrationNumber;

    /**
     * @var string
     * @Orm\Column(name="law_governed", type="string", length=50, nullable=true)
     */
    private $lawGoverned;

    /**
     * @var string
     * @Orm\Column(name="legal_form", type="string", length=50, nullable=true)
     */
    private $legalForm;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return boolean
     */
    public function getCorporate()
    {
        return $this->corporate;
    }

    /**
     * @param boolean $corporate
     */
    public function setCorporate($corporate)
    {
        $this->corporate = $corporate;
    }

    /**
     * @return boolean
     */
    public function getDesignatedInd()
    {
        return $this->designatedInd;
    }

    /**
     * @param boolean $designatedInd
     */
    public function setDesignatedInd($designatedInd)
    {
        $this->designatedInd = $designatedInd;
    }

    /**
     * @return DateTime
     */
    public function getAppointmentDate()
    {
        return $this->appointmentDate;
    }

    /**
     * @param DateTime $appointmentDate
     */
    public function setAppointmentDate($appointmentDate)
    {
        $this->appointmentDate = $appointmentDate;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getForename()
    {
        return $this->forename;
    }

    /**
     * @param string $forename
     */
    public function setForename($forename)
    {
        $this->forename = $forename;
    }

    /**
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * @param string $middleName
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return string
     */
    public function getCorporateName()
    {
        return $this->corporateName;
    }

    /**
     * @param string $corporateName
     */
    public function setCorporateName($corporateName)
    {
        $this->corporateName = $corporateName;
    }

    /**
     * @return string
     */
    public function getAuthentication()
    {
        return $this->authentication;
    }

    /**
     * @param string $authentication
     */
    public function setAuthentication($authentication)
    {
        $this->authentication = $authentication;
    }

    /**
     * @return bool
     */
    public function hasConsentToAct()
    {
        return $this->consentToAct;
    }

    /**
     * @param bool $consentToAct
     */
    public function setConsentToAct($consentToAct)
    {
        $this->consentToAct = $consentToAct;
    }

    /**
     * @return string
     */
    public function getPremise()
    {
        return $this->premise;
    }

    /**
     * @param string $premise
     */
    public function setPremise($premise)
    {
        $this->premise = $premise;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getThoroughfare()
    {
        return $this->thoroughfare;
    }

    /**
     * @param string $thoroughfare
     */
    public function setThoroughfare($thoroughfare)
    {
        $this->thoroughfare = $thoroughfare;
    }

    /**
     * @return string
     */
    public function getPostTown()
    {
        return $this->postTown;
    }

    /**
     * @param string $postTown
     */
    public function setPostTown($postTown)
    {
        $this->postTown = $postTown;
    }

    /**
     * @return string
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * @param string $county
     */
    public function setCounty($county)
    {
        $this->county = $county;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @param string $postcode
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    }

    /**
     * @return string
     */
    public function getCareOfName()
    {
        return $this->careOfName;
    }

    /**
     * @param string $careOfName
     */
    public function setCareOfName($careOfName)
    {
        $this->careOfName = $careOfName;
    }

    /**
     * @return string
     */
    public function getPoBox()
    {
        return $this->poBox;
    }

    /**
     * @param string $poBox
     */
    public function setPoBox($poBox)
    {
        $this->poBox = $poBox;
    }

    /**
     * @return DateTime
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * @param DateTime $dob
     */
    public function setDob($dob)
    {
        $this->dob = $dob;
    }

    /**
     * @return string
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * @param string $nationality
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;
    }

    /**
     * @return string
     */
    public function getOccupation()
    {
        return $this->occupation;
    }

    /**
     * @param string $occupation
     */
    public function setOccupation($occupation)
    {
        $this->occupation = $occupation;
    }

    /**
     * @return string
     */
    public function getCountryOfResidence()
    {
        return $this->countryOfResidence;
    }

    /**
     * @param string $countryOfResidence
     */
    public function setCountryOfResidence($countryOfResidence)
    {
        $this->countryOfResidence = $countryOfResidence;
    }

    /**
     * @return string
     */
    public function getResidentialPremise()
    {
        return $this->residentialPremise;
    }

    /**
     * @param string $residentialPremise
     */
    public function setResidentialPremise($residentialPremise)
    {
        $this->residentialPremise = $residentialPremise;
    }

    /**
     * @return string
     */
    public function getResidentialStreet()
    {
        return $this->residentialStreet;
    }

    /**
     * @param string $residentialStreet
     */
    public function setResidentialStreet($residentialStreet)
    {
        $this->residentialStreet = $residentialStreet;
    }

    /**
     * @return string
     */
    public function getResidentialThoroughfare()
    {
        return $this->residentialThoroughfare;
    }

    /**
     * @param string $residentialThoroughfare
     */
    public function setResidentialThoroughfare($residentialThoroughfare)
    {
        $this->residentialThoroughfare = $residentialThoroughfare;
    }

    /**
     * @return string
     */
    public function getResidentialPostTown()
    {
        return $this->residentialPostTown;
    }

    /**
     * @param string $residentialPostTown
     */
    public function setResidentialPostTown($residentialPostTown)
    {
        $this->residentialPostTown = $residentialPostTown;
    }

    /**
     * @return string
     */
    public function getResidentialCounty()
    {
        return $this->residentialCounty;
    }

    /**
     * @param string $residentialCounty
     */
    public function setResidentialCounty($residentialCounty)
    {
        $this->residentialCounty = $residentialCounty;
    }

    /**
     * @return string
     */
    public function getResidentialCountry()
    {
        return $this->residentialCountry;
    }

    /**
     * @param string $residentialCountry
     */
    public function setResidentialCountry($residentialCountry)
    {
        $this->residentialCountry = $residentialCountry;
    }

    /**
     * @return string
     */
    public function getResidentialPostcode()
    {
        return $this->residentialPostcode;
    }

    /**
     * @param string $residentialPostcode
     */
    public function setResidentialPostcode($residentialPostcode)
    {
        $this->residentialPostcode = $residentialPostcode;
    }

    /**
     * @return boolean
     */
    public function getResidentialSecureAddressInd()
    {
        return $this->residentialSecureAddressInd;
    }

    /**
     * @param boolean $residentialSecureAddressInd
     */
    public function setResidentialSecureAddressInd($residentialSecureAddressInd)
    {
        $this->residentialSecureAddressInd = $residentialSecureAddressInd;
    }

    /**
     * @return string
     */
    public function getIdentificationType()
    {
        return $this->identificationType;
    }

    /**
     * @param string $identificationType
     */
    public function setIdentificationType($identificationType)
    {
        $this->identificationType = $identificationType;
    }

    /**
     * @return string
     */
    public function getPlaceRegistered()
    {
        return $this->placeRegistered;
    }

    /**
     * @param string $placeRegistered
     */
    public function setPlaceRegistered($placeRegistered)
    {
        $this->placeRegistered = $placeRegistered;
    }

    /**
     * @return string
     */
    public function getRegistrationNumber()
    {
        return $this->registrationNumber;
    }

    /**
     * @param string $registrationNumber
     */
    public function setRegistrationNumber($registrationNumber)
    {
        $this->registrationNumber = $registrationNumber;
    }

    /**
     * @return string
     */
    public function getLawGoverned()
    {
        return $this->lawGoverned;
    }

    /**
     * @param string $lawGoverned
     */
    public function setLawGoverned($lawGoverned)
    {
        $this->lawGoverned = $lawGoverned;
    }

    /**
     * @return string
     */
    public function getLegalForm()
    {
        return $this->legalForm;
    }

    /**
     * @param string $legalForm
     */
    public function setLegalForm($legalForm)
    {
        $this->legalForm = $legalForm;
    }
}
