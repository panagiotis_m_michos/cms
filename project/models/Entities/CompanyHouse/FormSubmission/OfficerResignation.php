<?php

namespace Entities\CompanyHouse\FormSubmission;

use Doctrine\ORM\Mapping as ORM;
use Entities\CompanyHouse\FormSubmission;
use DateTime;

/**
 * @Orm\Table(name="ch_officer_resignation")
 * @Orm\Entity(repositoryClass = "Repositories\CompanyHouse\FormSubmission\OfficerResignationRepository")
 */
class OfficerResignation extends FormSubmission
{

    /**
     * @var string
     * @Orm\Column(name="type", type="string", nullable=true)
     */
    private $type;

    /**
     * @var boolean
     * @Orm\Column(name="corporate", type="boolean", nullable=true)
     */
    private $corporate;

    /**
     * @var string
     * @Orm\Column(name="corporate_name", type="string", length=160, nullable=true)
     */
    private $corporateName;

    /**
     * @var string
     * @Orm\Column(name="surname", type="string", length=160, nullable=true)
     */
    private $surname;

    /**
     * @var string
     * @Orm\Column(name="forename", type="string", length=50, nullable=true)
     */
    private $forename;

    /**
     * @var string
     * @Orm\Column(name="middle_name", type="string", length=50, nullable=true)
     */
    private $middleName;

    /**
     * @var DateTime
     * @Orm\Column(name="dob", type="date", nullable=true)
     */
    private $dob;

    /**
     * @var DateTime
     * @Orm\Column(name="resign_date", type="date", nullable=true)
     */
    private $resignDate;

    /**
     * @var string
     * @Orm\Column(name="company_type", type="string", length=160, nullable=true)
     */
    private $companyType;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return boolean
     */
    public function getCorporate()
    {
        return $this->corporate;
    }

    /**
     * @param boolean $corporate
     */
    public function setCorporate($corporate)
    {
        $this->corporate = $corporate;
    }

    /**
     * @return string
     */
    public function getCorporateName()
    {
        return $this->corporateName;
    }

    /**
     * @param string $corporateName
     */
    public function setCorporateName($corporateName)
    {
        $this->corporateName = $corporateName;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return string
     */
    public function getForename()
    {
        return $this->forename;
    }

    /**
     * @param string $forename
     */
    public function setForename($forename)
    {
        $this->forename = $forename;
    }

    /**
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * @param string $middleName
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;
    }

    /**
     * @return DateTime
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * @param DateTime $dob
     */
    public function setDob($dob)
    {
        $this->dob = $dob;
    }

    /**
     * @return DateTime
     */
    public function getResignDate()
    {
        return $this->resignDate;
    }

    /**
     * @param DateTime $resignDate
     */
    public function setResignDate($resignDate)
    {
        $this->resignDate = $resignDate;
    }

    /**
     * @return string
     */
    public function getCompanyType()
    {
        return $this->companyType;
    }

    /**
     * @param string $companyType
     */
    public function setCompanyType($companyType)
    {
        $this->companyType = $companyType;
    }
}
