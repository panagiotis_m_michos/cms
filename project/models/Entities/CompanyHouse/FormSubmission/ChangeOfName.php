<?php

namespace Entities\CompanyHouse\FormSubmission;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Entities\CompanyHouse\FormSubmission;

/**
 * @Orm\Table(name="ch_change_company_name")
 * @Orm\Entity(repositoryClass = "Repositories\CompanyHouse\FormSubmission\ChangeOfNameRepository")
 */
class ChangeOfName extends FormSubmission
{

    /**
     * @var string
     * @Orm\Column(name="sameDay", type="string", length=4, nullable=true)
     */
    private $sameDay;

    /**
     * @var DateTime
     * @Orm\Column(name="meetingDate", type="date", nullable=false)
     */
    private $meetingDate;

    /**
     * @var string
     * @Orm\Column(name="methodOfChange", type="string", length=20, nullable=false)
     */
    private $methodOfChange;

    /**
     * @var string
     * @Orm\Column(name="noticeGiven", type="string", length=50, nullable=false)
     */
    private $noticeGiven;

    /**
     * @var string
     * @Orm\Column(name="newCompanyName", type="string", length=160, nullable=false)
     */
    private $newCompanyName;

    /**
     * @return string
     */
    public function getSameDay()
    {
        return $this->sameDay;
    }

    /**
     * @param string $sameDay
     */
    public function setSameDay($sameDay)
    {
        $this->sameDay = $sameDay;
    }

    /**
     * @return string
     */
    public function getMeetingDate()
    {
        return $this->meetingDate;
    }

    /**
     * @param string $meetingDate
     */
    public function setMeetingDate($meetingDate)
    {
        $this->meetingDate = $meetingDate;
    }

    /**
     * @return string
     */
    public function getMethodOfChange()
    {
        return $this->methodOfChange;
    }

    /**
     * @param string $methodOfChange
     */
    public function setMethodOfChange($methodOfChange)
    {
        $this->methodOfChange = $methodOfChange;
    }

    /**
     * @return string
     */
    public function getNoticeGiven()
    {
        return $this->noticeGiven;
    }

    /**
     * @param string $noticeGiven
     */
    public function setNoticeGiven($noticeGiven)
    {
        $this->noticeGiven = $noticeGiven;
    }

    /**
     * @return string
     */
    public function getNewCompanyName()
    {
        return $this->newCompanyName;
    }

    /**
     * @param string $newCompanyName
     */
    public function setNewCompanyName($newCompanyName)
    {
        $this->newCompanyName = $newCompanyName;
    }

}
