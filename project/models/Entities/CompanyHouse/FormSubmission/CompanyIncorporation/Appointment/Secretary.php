<?php

namespace Entities\CompanyHouse\FormSubmission\CompanyIncorporation;

use Doctrine\ORM\Mapping as ORM;
use Entities\CompanyHouse\Helper\BaseOfficer;
use Entities\CompanyHouse\Helper\Authentication;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation\Appointment;

/**
 * @Orm\Entity
 */
class Secretary extends Appointment
{

    /**
     * we need this for old system
     *
     * @var string
     */
    private $type = self::TYPE_SECRETARY;
}
