<?php

namespace Entities\CompanyHouse\FormSubmission\CompanyIncorporation;

use DateTime;
use Doctrine\ORM\Mapping as Orm;
use Entities\CompanyHouse\Helper\Address;
use Entities\CompanyHouse\Helper\BaseOfficer;
use Entities\CompanyHouse\Helper\PersonDirector;

/**
 * @Orm\Entity
 */
class Director extends Appointment
{

    /**
     * we need this for old system
     *
     * @var string
     */
    private $type = self::TYPE_DIRECTOR;

    /**
     * @var DateTime
     * @Orm\Column(name="dob", type="date", nullable=true)
     */
    private $dob;

    /**
     * @var string
     * @Orm\Column(name="nationality", type="string", length=20, nullable=true)
     */
    private $nationality;

    /**
     * @var string
     * @Orm\Column(name="occupation", type="string", length=35, nullable=true)
     */
    private $occupation;

    /**
     * @var string
     * @Orm\Column(name="country_of_residence", type="string", length=50, nullable=true)
     */
    private $countryOfResidence;

    /**
     * @var string
     * @Orm\Column(name="residential_premise", type="string", length=50, nullable=true)
     */
    private $residentialPremise;

    /**
     * @var string
     * @Orm\Column(name="residential_street", type="string", length=50, nullable=true)
     */
    private $residentialStreet;

    /**
     * @var string
     * @Orm\Column(name="residential_thoroughfare", type="string", length=50, nullable=true)
     */
    private $residentialThoroughfare;

    /**
     * @var string
     * @Orm\Column(name="residential_post_town", type="string", length=50, nullable=true)
     */
    private $residentialPostTown;

    /**
     * @var string
     * @Orm\Column(name="residential_county", type="string", length=50, nullable=true)
     */
    private $residentialCounty;

    /**
     * @var string
     * @Orm\Column(name="residential_country", type="string", length=50, nullable=true)
     */
    private $residentialCountry;

    /**
     * @var string
     * @Orm\Column(name="residential_postcode", type="string", length=15, nullable=true)
     */
    private $residentialPostcode;

    /**
     * @var boolean
     * @Orm\Column(name="residential_secure_address_ind", type="boolean", nullable=true)
     */
    private $residentialSecureAddressInd;

    /**
     * @var Address
     * (NOTE: no ORM annotation here)
     */
    private $residentialAddress;

    /**
     * @param BaseOfficer $officer
     */
    public function __construct(BaseOfficer $officer)
    {
        parent::__construct($officer);
        $this->extractPersonDirector();
    }

    /**
     * @return BaseOfficer
     */
    public function getOfficer()
    {
        return $this->officer;
    }

    /**
     * @return Address
     */
    public function getResidentialAddress()
    {
        return $this->residentialAddress;
    }

    /**
     * @param Address $residentialAddress
     */
    public function setResidentialAddress($residentialAddress)
    {
        $this->residentialAddress = $residentialAddress;
    }

    /**
     * @param BaseOfficer $officer
     */
    public function setOfficer(BaseOfficer $officer)
    {
        $this->officer = $officer;
    }

    /**
     * @Orm\PreFlush
     * set all properties when this object is to
     * save so that doctrine can easily save these scalar values
     */
    public function extractPersonDirector()
    {
        if ($this->officer instanceof PersonDirector) {
            $this->dob = $this->officer->getDob();
            $this->nationality = $this->officer->getNationality();
            $this->occupation = $this->officer->getOccupation();
            $this->countryOfResidence = $this->officer->getCountryOfResidence();
            $this->extractResidentalAddress($this->officer->getAddress());
        }
    }

    /**
     * @param Address $residentialAddress
     */
    public function extractResidentalAddress($residentialAddress)
    {
        $this->residentialPremise = $residentialAddress->getPremise();
        $this->residentialStreet = $residentialAddress->getStreet();
        $this->residentialPostTown = $residentialAddress->getPostTown();
        $this->residentialPostcode = $residentialAddress->getPostcode();
        $this->residentialThoroughfare = $residentialAddress->getThoroughfare();
        $this->residentialCounty = $residentialAddress->getCounty();
        $this->residentialCountry = $residentialAddress->getCountry();
        $this->residentialSecureAddressInd = $residentialAddress->getSecureAddressInd();
    }

    /**
     * @Orm\PostLoad
     * When the row is hydrated into this class,
     * PersonDirector is not set because that isn't mapped.
     * so simply, map it manually
     */
    public function hydratePersonDirector()
    {
        if (!$this->getCorporate()) {
            $address = $this->getHydratedAddress();
            $this->officer = new PersonDirector(
                $this->forename,
                $this->surname,
                $this->dob,
                $this->nationality,
                $this->occupation,
                $this->countryOfResidence,
                $address,
                $this->residentialAddress
            );
        }
    }

    /**
     * @return Address
     */
    public function getHydratedAddress()
    {
        $residentialAddress = new Address(
            $this->residentialPremise,
            $this->residentialStreet,
            $this->residentialPostTown,
            $this->residentialPostcode,
            $this->residentialCountry
        );
        $residentialAddress->setThoroughfare($this->residentialThoroughfare);
        $residentialAddress->setCounty($this->residentialCounty);
        $residentialAddress->setSecureAddressInd($this->residentialSecureAddressInd);

        return $residentialAddress;
    }
}
