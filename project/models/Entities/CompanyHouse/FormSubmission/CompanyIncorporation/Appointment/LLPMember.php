<?php

namespace Entities\CompanyHouse\FormSubmission\CompanyIncorporation;

use Doctrine\ORM\Mapping as ORM;
use Entities\CompanyHouse\Helper\BaseOfficer;
use Entities\CompanyHouse\Helper\Authentication;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation\Appointment;

/**
 * @Orm\Entity
 */
class LLPMember extends Appointment
{

    /**
     * we need this for old system
     *
     * @var string
     */
    private $type = self::TYPE_LLP_MEMBER;

    /**
     * @var bool
     */
    private $designatedInd = TRUE;

    /**
     * @param BaseOfficer $officer
     * @param Authentication $authentication
     */
    public function __construct(BaseOfficer $officer, Authentication $authentication)
    {
        parent::__construct($officer, $authentication);
    }

    /**
     * @return bool
     */
    public function getDesignatedInd()
    {
        return $this->designatedInd;
    }

    /**
     * @param bool $designatedInd
     */
    public function setDesignatedInd($designatedInd)
    {
        $this->designatedInd = $designatedInd;
    }

}
