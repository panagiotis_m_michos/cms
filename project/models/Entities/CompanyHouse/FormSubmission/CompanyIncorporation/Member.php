<?php

namespace Entities\CompanyHouse\FormSubmission\CompanyIncorporation;

use Doctrine\ORM\Mapping as Orm;
use Entities\CompanyHouse\Helper\Identification;
use Entities\EntityAbstract;
use Entities\CompanyHouse\Helper\BaseOfficer;
use Entities\CompanyHouse\Helper\Address;
use Entities\CompanyHouse\Helper\EeaIdentification;
use Entities\CompanyHouse\Helper\NonEeaIdentification;
use Entities\CompanyHouse\Helper\PersonOfficer;
use Entities\CompanyHouse\Helper\CorporateOfficer;

/**
 * @Orm\Entity
 * @Orm\Table(name="ch_incorporation_member")
 * @Orm\InheritanceType("SINGLE_TABLE")
 * @Orm\DiscriminatorColumn(name="type", type="string")
 * @Orm\DiscriminatorMap({
 *      "DIR" = "Entities\CompanyHouse\FormSubmission\CompanyIncorporation\Director",
 *      "SEC" = "Entities\CompanyHouse\FormSubmission\CompanyIncorporation\Secretary",
 *      "SUB" = "Entities\CompanyHouse\FormSubmission\CompanyIncorporation\Subscriber",
 *      "MEM" = "Entities\CompanyHouse\FormSubmission\CompanyIncorporation\LLPMember"
 * })
 */
abstract class Member extends EntityAbstract
{

    const TYPE_DIRECTOR = 'DIR';
    const TYPE_SECRETARY = 'SEC';
    const TYPE_SUBSCRIBER = 'SUB';
    const TYPE_LLP_MEMBER = 'MEM';

    /**
     * @var string
     * @Orm\Column(name="title", type="string", length=50, nullable=true)
     */
    protected $title;

    /**
     * @var string
     * @Orm\Column(name="forename", type="string", length=50, nullable=true)
     */
    protected $forename;

    /**
     * @var string
     * @Orm\Column(name="middle_name", type="string", length=50, nullable=true)
     */
    protected $middleName;

    /**
     * @var string
     * @Orm\Column(name="surname", type="string", length=160, nullable=false)
     */
    protected $surname;

    /**
     * @var string
     * @Orm\Column(name="corporate_name", type="string", length=160, nullable=true)
     */
    protected $corporateName;

    /**
     * @var string
     * @Orm\Column(name="premise", type="string", length=50, nullable=false)
     */
    protected $premise;

    /**
     * @var string
     * @Orm\Column(name="street", type="string", length=50, nullable=false)
     */
    protected $street;

    /**
     * @var string
     * @Orm\Column(name="thoroughfare", type="string", length=50, nullable=true)
     */
    protected $thoroughfare;

    /**
     * @var string
     * @Orm\Column(name="post_town", type="string", length=50, nullable=true)
     */
    protected $postTown;

    /**
     * @var string
     * @Orm\Column(name="county", type="string", length=50, nullable=true)
     */
    protected $county;

    /**
     * @var string
     * @Orm\Column(name="country", type="string", length=50, nullable=true)
     */
    protected $country;

    /**
     * @var string
     * @Orm\Column(name="postcode", type="string", length=15, nullable=true)
     */
    protected $postcode;

    /**
     * @var string
     * @Orm\Column(name="care_of_name", type="string", length=100, nullable=true)
     */
    protected $careOfName;

    /**
     * @var string
     * @Orm\Column(name="po_box", type="string", length=10, nullable=true)
     */
    protected $poBox;

    /**
     * @var BaseOfficer
     * (NOTE: no ORM annotation here)
     */
    protected $officer;

    /**
     * @var integer
     * @Orm\Column(name="incorporation_member_id", type="integer", nullable=false)
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $memberId;

    /**
     * @var boolean
     * @Orm\Column(name="nominee", type="boolean", nullable=true)
     */
    private $nominee;

    /**
     * @var string
     */
    private $type;

    /**
     * @var boolean
     * @Orm\Column(name="corporate", type="boolean", nullable=false)
     */
    private $corporate = FALSE;


    /**
     * @var bool
     * @Orm\Column(name="consentToAct", type="boolean")
     */
    private $consentToAct = TRUE;

    /**
     * @var string
     * @Orm\Column(name="identification_type", type="string", nullable=true)
     */
    private $identificationType;

    /**
     * @var string
     * @Orm\Column(name="place_registered", type="string", length=50, nullable=true)
     */
    private $placeRegistered;

    /**
     * @var string
     * @Orm\Column(name="registration_number", type="string", length=20, nullable=true)
     */
    private $registrationNumber;

    /**
     * @var string
     * @Orm\Column(name="law_governed", type="string", length=50, nullable=true)
     */
    private $lawGoverned;

    /**
     * @var string
     * @Orm\Column(name="legal_form", type="string", length=50, nullable=true)
     */
    private $legalForm;

    /**
     * @param BaseOfficer $officer
     */
    public function __construct(BaseOfficer $officer)
    {
        $this->setOfficer($officer);
        $this->extractOfficer();
        $this->extractIdentification();
    }

    /**
     * @return BaseOfficer
     */
    public function getOfficer()
    {
        return $this->officer;
    }

    /**
     * @param BaseOfficer $officer
     */
    public function setOfficer(BaseOfficer $officer)
    {
        $this->officer = $officer;
    }

    /**
     * @return bool
     */
    public function hasConsentToAct()
    {
        return $this->consentToAct;
    }

    /**
     * @param bool $consentToAct
     */
    public function setConsentToAct($consentToAct)
    {
        $this->consentToAct = $consentToAct;
    }

    /**
     * @return integer
     */
    public function getMemberId()
    {
        return $this->memberId;
    }

    /**
     * @return boolean
     */
    public function getNominee()
    {
        return $this->nominee;
    }

    /**
     * @param boolean $nominee
     */
    public function setNominee($nominee)
    {
        $this->nominee = $nominee;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return bool
     */
    public function getCorporate()
    {
        return $this->corporate;
    }

    /**
     * @param bool $corporate
     */
    public function setCorporate($corporate)
    {
        $this->corporate = $corporate;
    }

    /**
     * @Orm\PreFlush
     * set all properties when this object is to
     * save so that doctrine can easily save these scalar values
     */
    public function extractOfficer()
    {
        $this->forename = $this->officer->getForeName();
        $this->surname = $this->officer->getSurname();
        $this->middleName = $this->officer->getOtherForenames();
        $this->title = $this->officer->getTitle();

        if ($this->officer instanceof CorporateOfficer) {
            $this->corporateName = $this->officer->getCorporateName();
            $this->corporate = TRUE;
        }
        $this->extractAddress($this->officer->getAddress());
    }

    /**
     * @Orm\PreFlush
     * set all properties when this object is to
     * save so that doctrine can easily save these scalar values
     */
    public function extractIdentification()
    {
        if ($this->officer instanceof CorporateOfficer && $this instanceof Appointment) {
            $identification = $this->officer->getIdentification();
            $this->placeRegistered = $identification->getPlaceRegistered();
            $this->registrationNumber = $identification->getRegistrationNumber();
            if ($identification instanceof NonEeaIdentification) {
                $this->lawGoverned = $identification->getLawGoverned();
                $this->legalForm = $identification->getLegalForm();
            }
        }
    }

    /**
     * @param Address $address
     */
    public function extractAddress($address)
    {
        $this->premise = $address->getPremise();
        $this->street = $address->getStreet();
        $this->postTown = $address->getPostTown();
        $this->postcode = $address->getPostcode();
        $this->thoroughfare = $address->getThoroughfare();
        $this->careOfName = $address->getCareOfName();
        $this->county = $address->getCounty();
        $this->country = $address->getCountry();
        $this->poBox = $address->getPoBox();
    }

    /**
     * @Orm\PostLoad
     * When the row is hydrated into this class,
     * Identification is not set because that isn't mapped.
     * so simply, map it manually
     */
    public function hydrateIdentification()
    {
        if ($this->identificationType == Identification::IDENTIFICATION_TYPE_EEA) {
            $this->officer->setIdentification(new EeaIdentification($this->placeRegistered, $this->registrationNumber));
        } else {
            $this->officer->setIdentification(
                new NonEeaIdentification(
                    $this->placeRegistered,
                    $this->registrationNumber,
                    $this->lawGoverned,
                    $this->legalForm
                )
            );
        }
    }

    /**
     * @Orm\PostLoad
     * When the row is hydrated into this class,
     * $officer is not set because that isn't mapped.
     * so simply, map it manually
     */
    public function hydrateOfficer()
    {
        $address = $this->getHydrateAddress();
        if ($this->corporate) {
            $this->officer = new CorporateOfficer($this->corporateName, $this->forename, $this->surname, $address);
        } else {
            $this->officer = new PersonOfficer($this->forename, $this->surname, $address);
            $this->officer->setTitle($this->title);
            $this->officer->setOtherForenames($this->middleName);
        }
    }

    /**
     * @return Address
     */
    public function getHydrateAddress()
    {
        $address = new Address(
            $this->premise, 
            $this->street, 
            $this->postTown, 
            $this->postcode,
            $this->country
        );
        $address->setThoroughfare($this->thoroughfare);
        $address->setCareOfName($this->careOfName);
        $address->setCounty($this->county);
        $address->setPoBox($this->poBox);

        return $address;
    }
}
