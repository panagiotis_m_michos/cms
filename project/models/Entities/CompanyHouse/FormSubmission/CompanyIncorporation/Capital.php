<?php

namespace Entities\CompanyHouse\FormSubmission\CompanyIncorporation;

use Doctrine\ORM\Mapping as ORM;
use Entities\EntityAbstract;
use ValueObject\Currency;
use Exception;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation;

/**
 * @Orm\Table(name="ch_incorporation_capital")
 * @Orm\Entity
 */
class Capital extends EntityAbstract
{

    /**
     * @var integer
     * @Orm\Column(name="incorporation_capital_id", type="integer", nullable=false)
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $capitalId;

    /**
     * @var string
     * @Orm\Column(name="currency", type="string", length=3, nullable=false)
     */
    private $currency;

    /**
     * @var string
     * @Orm\Column(name="num_shares", type="string", length=11, nullable=false)
     */
    private $numShares;

    /**
     * @var string
     * @Orm\Column(name="share_class", type="string", length=50, nullable=false)
     */
    private $shareClass;

    /**
     * @var string
     * @Orm\Column(name="prescribed_particulars", type="text", nullable=false)
     */
    private $prescribedParticulars;

    /**
     * @var string
     * @Orm\Column(name="amount_paid", type="string", length=20, nullable=false)
     */
    private $amountPaid;

    /**
     * @var string
     * @Orm\Column(name="amount_unpaid", type="string", length=20, nullable=false)
     */
    private $amountUnpaid;

    /**
     * @var string
     * @Orm\Column(name="aggregate_nom_value", type="string", length=19, nullable=false)
     */
    private $aggregateNomValue;

    /**
     * @var CompanyIncorporation
     * @Orm\ManyToOne(targetEntity = "Entities\CompanyHouse\FormSubmission\CompanyIncorporation", inversedBy = "capitals")
     * @Orm\JoinColumn(name = "form_submission_id", referencedColumnName="form_submission_id")
     */
    private $formSubmission;

    /**
     * @todo move shareClass to separate entity
     * @param string $currency
     * @param string $shareClass
     * @param string $prescrPartic
     * @param string $numShares
     * @param string $amountPaid
     * @param string $amountUnpaid
     * @param string $aggregateNomValue
     */
    public function __construct(
        $currency,
        $shareClass,
        $prescrPartic,
        $numShares,
        $amountPaid,
        $amountUnpaid,
        $aggregateNomValue
    )
    {
        $this->setCurrency($currency);
        $this->setShareClass($shareClass);
        $this->setPrescribedParticulars($prescrPartic);
        $this->setNumShares($numShares);
        $this->setAmountPaid($amountPaid);
        $this->setAmountUnpaid($amountUnpaid);
        $this->setAggregateNomValue($aggregateNomValue);
    }

    /**
     * @param CompanyIncorporation $formSubmission
     */
    public function setFormSubmission(CompanyIncorporation $formSubmission)
    {
        $this->formSubmission = $formSubmission;
    }

    /**
     * @return integer
     */
    public function getCapitalId()
    {
        return $this->capitalId;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @throws Exception
     */
    public function setCurrency($currency)
    {
        if (!in_array($currency, Currency::getAllCurrencies())) {
            throw new Exception("The Currency is invalid.");
        }

        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getNumShares()
    {
        return $this->numShares;
    }

    /**
     * @param string $numShares
     */
    public function setNumShares($numShares)
    {
        $this->numShares = $numShares;
    }

    /**
     * @return string
     */
    public function getShareClass()
    {
        return $this->shareClass;
    }

    /**
     * @param string $shareClass
     */
    public function setShareClass($shareClass)
    {
        $this->shareClass = $shareClass;
    }

    /**
     * @return string
     */
    public function getPrescribedParticulars()
    {
        return $this->prescribedParticulars;
    }

    /**
     * @param string $prescribedParticulars
     */
    public function setPrescribedParticulars($prescribedParticulars)
    {
        $this->prescribedParticulars = $prescribedParticulars;
    }

    /**
     * @return string
     */
    public function getAmountPaid()
    {
        return $this->amountPaid;
    }

    /**
     * @param string $amountPaid
     */
    public function setAmountPaid($amountPaid)
    {
        $this->amountPaid = $amountPaid;
    }

    /**
     * @return string
     */
    public function getAmountUnpaid()
    {
        return $this->amountUnpaid;
    }

    /**
     * @param string $amountUnpaid
     */
    public function setAmountUnpaid($amountUnpaid)
    {
        $this->amountUnpaid = $amountUnpaid;
    }

    /**
     * @return string
     */
    public function getAggregateNomValue()
    {
        return $this->aggregateNomValue;
    }

    /**
     * @param string $aggregateNomValue
     */
    public function setAggregateNomValue($aggregateNomValue)
    {
        $this->aggregateNomValue = $aggregateNomValue;
    }
}
