<?php

namespace Entities\CompanyHouse\FormSubmission\CompanyIncorporation;

use Entities\CompanyHouse\FormSubmission\CompanyIncorporation;
use Doctrine\ORM\Mapping as Orm;
use Entities\CompanyHouse\Helper\BaseOfficer;
use Entities\CompanyHouse\Helper\Authentication;
use Entities\CompanyHouse\Helper\Share;

/**
 * @Orm\Entity
 * @Orm\HasLifecycleCallbacks
 */
class Subscriber extends Member
{

    /**
     * we need this for old system
     *
     * @var string
     */
    private $type = self::TYPE_SUBSCRIBER;

    /**
     * @var string
     * @Orm\Column(name="share_class", type="string", length=50, nullable=true)
     */
    private $shareClass;

    /**
     * @var string
     * @Orm\Column(name="num_shares", type="string", length=11, nullable=true)
     */
    private $numShares;

    /**
     * @var string
     * @Orm\Column(name="amount_paid", type="string", length=20, nullable=true)
     */
    private $amountPaid;

    /**
     * @var string
     * @Orm\Column(name="amount_unpaid", type="string", length=20, nullable=true)
     */
    private $amountUnpaid;

    /**
     * @var string
     * @Orm\Column(name="currency", type="string", length=3, nullable=true)
     */
    private $currency;

    /**
     * @var string
     * @Orm\Column(name="share_value", type="string", length=20, nullable=true)
     */
    private $shareValue;

    /**
     * @var CompanyIncorporation
     * @Orm\ManyToOne(targetEntity = "Entities\CompanyHouse\FormSubmission\CompanyIncorporation", inversedBy = "subscribers")
     * @Orm\JoinColumn(name = "form_submission_id", referencedColumnName="form_submission_id")
     */
    private $formSubmission;

    /**
     * @var string
     * @Orm\Column(name="authentication", type="string", length=45, nullable=true)
     */
    private $authenticationString;

    /**
     * @var Authentication
     */
    private $authentication;

    /**
     * @var Share
     * (NOTE: no ORM annotation here)
     */
    private $share;

    /**
     * @param BaseOfficer $officer
     * @param Share $share
     * @param Authentication $authentication
     */
    public function __construct(BaseOfficer $officer, Share $share, Authentication $authentication)
    {
        parent::__construct($officer);
        $this->authentication = $authentication;
        $this->serializeAuthentication();

        $this->setShare($share);
        $this->extractShare();
    }

    /**
     * @param CompanyIncorporation $formSubmission
     */
    public function setFormSubmission(CompanyIncorporation $formSubmission)
    {
        $this->formSubmission = $formSubmission;
    }

    /**
     * @return Share
     */
    public function getShare()
    {
        return $this->share;
    }

    /**
     * @param Share $share
     */
    public function setShare(Share $share)
    {
        $this->share = $share;
    }

    /**
     * @Orm\PreFlush
     * set all properties when this object is to
     * save so that doctrine can easily save these scalar values
     */
    public function extractShare()
    {
        $this->currency = $this->share->getCurrency();
        $this->shareClass = $this->share->getShareClass();
        $this->numShares = $this->share->getNumShares();
        $this->shareValue = $this->share->getShareValue();
        $this->amountPaid = $this->share->getAmountPaid();
        $this->amountUnpaid = $this->share->getAmountUnpaid();
    }

    /**
     * @Orm\PrePersist
     */
    public function serializeAuthentication()
    {
        $this->authenticationString =
            Authentication::PERSONAL_ATTRIBUTE_TYPE_BIRTOWN . '-' . $this->authentication->getBirthTown() . ';' .
            Authentication::PERSONAL_ATTRIBUTE_TYPE_TEL . '-' . $this->authentication->getTelephone() . ';' .
            Authentication::PERSONAL_ATTRIBUTE_TYPE_EYE . '-' . $this->authentication->getEyeColour();
    }
    /**
     * @Orm\PostLoad
     * When the row is hydrated into this class,
     * $address is not set because that isn't mapped.
     * so simply, map it manually
     */
    public function packAuthentication()
    {
        if (preg_match_all('/-(.*?)(?:;|$)/', $this->authenticationString, $matches)) {
            $this->share = new Authentication(...$matches[1]);
        }
    }

    /**
     * @Orm\PostLoad
     * When the row is hydrated into this class,
     * $address is not set because that isn't mapped.
     * so simply, map it manually
     */
    public function packShare()
    {
        $this->share = new Share($this->currency, $this->shareClass, $this->numShares, $this->shareValue);
    }

    /**
     * @return Authentication
     */
    public function getAuthentication()
    {
        return $this->authentication;
    }
}
