<?php

namespace Entities\CompanyHouse\FormSubmission\CompanyIncorporation;

use Entities\CompanyHouse\FormSubmission\CompanyIncorporation\Member;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Orm\Entity
 */
abstract class Appointment extends Member
{

    /**
     * @var CompanyIncorporation
     * @Orm\ManyToOne(targetEntity = "Entities\CompanyHouse\FormSubmission\CompanyIncorporation", inversedBy = "appointments")
     * @Orm\JoinColumn(name = "form_submission_id", referencedColumnName="form_submission_id")
     */
    private $formSubmission;

    /**
     * @param CompanyIncorporation $formSubmission
     */
    public function setFormSubmission(CompanyIncorporation $formSubmission)
    {
        $this->formSubmission = $formSubmission;
    }
}
