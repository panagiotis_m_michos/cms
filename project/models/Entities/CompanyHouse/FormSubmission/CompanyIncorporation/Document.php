<?php

namespace Entities\CompanyHouse\FormSubmission\CompanyIncorporation;

use Doctrine\ORM\Mapping as ORM;
use Entities\EntityAbstract;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation;

/**
 * @Orm\Table(name="ch_document")
 * @Orm\Entity
 */
class Document extends EntityAbstract
{

    /**
     * @var integer
     * @Orm\Column(name="document_id", type="integer", nullable=false)
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $documentId;

    /**
     * @var string
     * @Orm\Column(name="category", type="string", nullable=false)
     */
    private $category;

    /**
     * @var string
     * @Orm\Column(name="filename", type="string", length=32, nullable=false)
     */
    private $filename;

    /**
     * @var string
     * @Orm\Column(name="file_path", type="string", length=255, nullable=true)
     */
    private $filePath;

    /**
     * @var boolean
     * @Orm\Column(name="custom", type="boolean", nullable=true)
     */
    private $custom;

    /**
     * @var CompanyIncorporation
     * @Orm\ManyToOne(targetEntity = "Entities\CompanyHouse\FormSubmission\CompanyIncorporation", inversedBy = "documents")
     * @Orm\JoinColumn(name = "form_submission_id", referencedColumnName="form_submission_id")
     */
    private $formSubmission;

    /**
     * @param string $category
     * @param string $filename
     */
    public function __construct($category, $filename)
    {
        $this->setCategory($category);
        $this->setFilename($filename);
    }

    /**
     * @param CompanyIncorporation $formSubmission
     */
    public function setFormSubmission(CompanyIncorporation $formSubmission)
    {
        $this->formSubmission = $formSubmission;
    }

    /**
     * @return integer
     */
    public function getDocumentId()
    {
        return $this->documentId;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param string $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
    }

    /**
     * @return string
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * @param string $filePath
     */
    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;
    }

    /**
     * @return boolean
     */
    public function getCustom()
    {
        return $this->custom;
    }

    /**
     * @param boolean $custom
     */
    public function setCustom($custom)
    {
        $this->custom = $custom;
    }

}
