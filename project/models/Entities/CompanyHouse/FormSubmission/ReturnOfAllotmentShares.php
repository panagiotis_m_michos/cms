<?php

namespace Entities\CompanyHouse\FormSubmission;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Entities\CompanyHouse\FormSubmission;
use DateTime;
use Entities\CompanyHouse\FormSubmission\ReturnOfAllotmentShares\Shares;

/**
 * @Orm\Table(name="ch_return_of_allotment_shares")
 * @Orm\Entity(repositoryClass = "Repositories\CompanyHouse\FormSubmission\ReturnOfAllotmentSharesRepository")
 */
class ReturnOfAllotmentShares extends FormSubmission
{

    /**
     * @var DateTime
     * @Orm\Column(name="start_period", type="date", nullable=true)
     */
    private $startPeriod;

    /**
     * @var DateTime
     * @Orm\Column(name="end_period", type="date", nullable=true)
     */
    private $endPeriod;

    /**
     * @var Shares[]|ArrayCollection
     * @Orm\OneToMany(targetEntity = "Entities\CompanyHouse\FormSubmission\ReturnOfAllotmentShares\Shares", mappedBy = "returnOfAllotmentShares", cascade={"persist"}, orphanRemoval=true)
     */
    private $shares;

    /**
     * @return DateTime
     */
    public function getStartPeriod()
    {
        return $this->startPeriod;
    }

    /**
     * @param DateTime $startPeriod
     */
    public function setStartPeriod($startPeriod)
    {
        $this->startPeriod = $startPeriod;
    }

    /**
     * @return DateTime
     */
    public function getEndPeriod()
    {
        return $this->endPeriod;
    }

    /**
     * @param DateTime $endPeriod
     */
    public function setEndPeriod($endPeriod)
    {
        $this->endPeriod = $endPeriod;
    }

    /**
     * @param Shares $shares
     */
    public function addShares(Shares $shares)
    {
        $this->shares[] = $shares;
        $shares->setReturnOfAllotmentShares($this);
    }

    /**
     * @return Shares[]|ArrayCollection
     */
    public function getShares()
    {
        return $this->shares;
    }

    /**
     * @param ArrayCollection $shares
     */
    public function setShares(ArrayCollection $shares)
    {
        $this->shares = $shares;
    }

    public function __clone()
    {
        if ($this->getFormSubmissionId()) {
            $this->cloneShares();
        }

        parent::__clone();
    }

    private function cloneShares()
    {
        $shares = $this->getShares();
        $this->shares = new ArrayCollection();

        foreach ($shares as $share) {
            $shareClone = clone $share;
            $this->shares->add($shareClone);
            $shareClone->setReturnOfAllotmentShares($this);
        }
    }
}
