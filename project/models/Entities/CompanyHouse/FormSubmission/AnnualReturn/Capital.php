<?php

namespace Entities\CompanyHouse\FormSubmission\AnnualReturn;

use Entities\EntityAbstract;
use Doctrine\ORM\Mapping as ORM;
use Entities\CompanyHouse\FormSubmission\AnnualReturn;

/**
 * @Orm\Table(name="ch_annual_return_shares")
 * @Orm\Entity
 */
class Capital extends EntityAbstract
{

    /**
     * @var integer
     *
     * @Orm\Column(name="annual_return_shares_id", type="integer", nullable=false)
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $capitalShareId;

    /**
     * @var string
     * @Orm\Column(name="share_class", type="string", length=50, nullable=true)
     */
    private $shareClass;

    /**
     * @var string
     * @Orm\Column(name="prescribed_particulars", type="text", nullable=true)
     */
    private $prescribedParticulars;

    /**
     * @var string
     * @Orm\Column(name="num_shares", type="string", length=11, nullable=true)
     */
    private $numShares;

    /**
     * @var string
     * @Orm\Column(name="amount_paid", type="string", length=20, nullable=true)
     */
    private $amountPaid;

    /**
     * @var string
     * @Orm\Column(name="amount_unpaid", type="string", length=20, nullable=true)
     */
    private $amountUnpaid;

    /**
     * @var string
     * @Orm\Column(name="currency", type="string", length=3, nullable=true)
     */
    private $currency;

    /**
     * @var string
     * @Orm\Column(name="aggregate_nom_value", type="string", length=19, nullable=true)
     */
    private $aggregateNomValue;

    /**
     * @var string
     * @Orm\Column(name="sync_paid", type="string", length=255, nullable=true)
     */
    private $syncPaid;

    /**
     * @var string
     * @Orm\Column(name="paid", type="string", length=255, nullable=true)
     */
    private $paid;

    /**
     * @var AnnualReturn
     * @Orm\ManyToOne(targetEntity = "Entities\CompanyHouse\FormSubmission\AnnualReturn", inversedBy = "capitals")
     * @Orm\JoinColumn(name = "form_submission_id", referencedColumnName="form_submission_id")
     */
    private $annualReturn;

    /**
     * @return int
     */
    public function getCapitalShareId()
    {
        return $this->capitalShareId;
    }

    /**
     * @return AnnualReturn
     */
    public function getAnnualReturn()
    {
        return $this->annualReturn;
    }

    /**
     * @param AnnualReturn $annualReturn
     */
    public function setAnnualReturn(AnnualReturn $annualReturn)
    {
        $this->annualReturn = $annualReturn;
    }

    /**
     * @param string $shareClass
     */
    public function setShareClass($shareClass)
    {
        $this->shareClass = $shareClass;
    }

    /**
     * @return string
     */
    public function getShareClass()
    {
        return $this->shareClass;
    }

    /**
     * @param string $prescribedParticulars
     */
    public function setPrescribedParticulars($prescribedParticulars)
    {
        $this->prescribedParticulars = $prescribedParticulars;
    }

    /**
     * @return string
     */
    public function getPrescribedParticulars()
    {
        return $this->prescribedParticulars;
    }

    /**
     * @param string $numShares
     */
    public function setNumShares($numShares)
    {
        $this->numShares = $numShares;
    }

    /**
     * @return string
     */
    public function getNumShares()
    {
        return $this->numShares;
    }

    /**
     * @param string $amountPaid
     */
    public function setAmountPaid($amountPaid)
    {
        $this->amountPaid = $amountPaid;
    }

    /**
     * @return string
     */
    public function getAmountPaid()
    {
        return $this->amountPaid;
    }

    /**
     * @param string $amountUnpaid
     */
    public function setAmountUnpaid($amountUnpaid)
    {
        $this->amountUnpaid = $amountUnpaid;
    }

    /**
     * @return string
     */
    public function getAmountUnpaid()
    {
        return $this->amountUnpaid;
    }

    /**
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $aggregateNomValue
     */
    public function setAggregateNomValue($aggregateNomValue)
    {
        $this->aggregateNomValue = $aggregateNomValue;
    }

    /**
     * @return string
     */
    public function getAggregateNomValue()
    {
        return $this->aggregateNomValue;
    }

    /**
     * @param string $syncPaid
     */
    public function setSyncPaid($syncPaid)
    {
        $this->syncPaid = $syncPaid;
    }

    /**
     * @return string
     */
    public function getSyncPaid()
    {
        return $this->syncPaid;
    }

    /**
     * @param string $paid
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;
    }

    /**
     * @return string
     */
    public function getPaid()
    {
        return $this->paid;
    }
}
