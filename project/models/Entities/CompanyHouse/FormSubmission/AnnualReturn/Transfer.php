<?php

namespace Entities\CompanyHouse\FormSubmission\AnnualReturn;

use Doctrine\ORM\Mapping as ORM;
use Entities\CompanyHouse\FormSubmission\AnnualReturn\Shareholding;
use DateTime;
use Entities\EntityAbstract;

/**
 * @Orm\Table(name="ch_annual_return_transfer")
 * @Orm\Entity
 */
class Transfer extends EntityAbstract
{

    /**
     * @var integer
     * @Orm\Column(name="annual_return_transfer_id", type="integer", nullable=false)
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $transferId;

    /**
     * @var DateTime
     * @Orm\Column(name="date_of_transfer", type="date", nullable=true)
     */
    private $dateOfTransfer;

    /**
     * @var string
     * @Orm\Column(name="shares_transfered", type="string", length=250, nullable=true)
     */
    private $sharesTransfered;

    /**
     * @var Shareholding
     * @Orm\ManyToOne(targetEntity = "Shareholding", inversedBy = "shareholders")
     * @Orm\JoinColumn(name = "annual_return_shareholding_id", referencedColumnName="annual_return_shareholding_id")
     */
    private $shareholding;

    /**
     * @param DateTime $dateOfTransfer
     * @param string $sharesTransfered
     */
    public function __construct($dateOfTransfer, $sharesTransfered)
    {
        $this->setDateOfTransfer($dateOfTransfer);
        $this->setSharesTransfered($sharesTransfered);
    }

    /**
     * @return integer
     */
    public function getTransferId()
    {
        return $this->transferId;
    }

    /**
     * @return DateTime
     */
    public function getDateOfTransfer()
    {
        return $this->dateOfTransfer;
    }

    /**
     * @param DateTime $dateOfTransfer
     */
    public function setDateOfTransfer($dateOfTransfer)
    {
        $this->dateOfTransfer = $dateOfTransfer;
    }

    /**
     * @return string
     */
    public function getSharesTransfered()
    {
        return $this->sharesTransfered;
    }

    /**
     * @param string $sharesTransfered
     */
    public function setSharesTransfered($sharesTransfered)
    {
        $this->sharesTransfered = $sharesTransfered;
    }

    /**
     * @return Shareholding
     */
    public function getShareholding()
    {
        return $this->shareholding;
    }

    /**
     * @param Shareholding $shareholding
     */
    public function setShareholding(Shareholding $shareholding)
    {
        $this->shareholding = $shareholding;
    }

}
