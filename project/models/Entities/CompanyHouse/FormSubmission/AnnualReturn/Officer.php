<?php

namespace Entities\CompanyHouse\FormSubmission\AnnualReturn;

use Entities\CompanyHouse\FormSubmission\AnnualReturn;
use Entities\EntityAbstract;
use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
 * @Orm\Table(name="ch_annual_return_officer")
 * @Orm\Entity
 */
class Officer extends EntityAbstract
{

    /**
     * @var integer
     * @Orm\Column(name="annual_return_officer_id", type="integer", nullable=false)
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $officerId;

    /**
     * @var integer
     * @Orm\Column(name="form_submission_id", type="integer", nullable=false)
     */
    private $formSubmissionId;

    /**
     * @var string
     * @Orm\Column(name="type", type="string", nullable=true)
     */
    private $type;

    /**
     * @var boolean
     * @Orm\Column(name="corporate", type="boolean", nullable=true)
     */
    private $corporate;

    /**
     * @var boolean
     * @Orm\Column(name="designated_ind", type="boolean", nullable=true)
     */
    private $designatedInd;

    /**
     * @var string
     * @Orm\Column(name="title", type="string", length=50, nullable=true)
     */
    private $title;

    /**
     * @var string
     * @Orm\Column(name="forename", type="string", length=50, nullable=true)
     */
    private $forename;

    /**
     * @var string
     * @Orm\Column(name="middle_name", type="string", length=50, nullable=true)
     */
    private $middleName;

    /**
     * @var string
     * @Orm\Column(name="surname", type="string", length=160, nullable=true)
     */
    private $surname;

    /**
     * @var string
     * @Orm\Column(name="corporate_name", type="string", length=160, nullable=true)
     */
    private $corporateName;

    /**
     * @var string
     * @Orm\Column(name="premise", type="string", length=50, nullable=true)
     */
    private $premise;

    /**
     * @var string
     * @Orm\Column(name="street", type="string", length=50, nullable=true)
     */
    private $street;

    /**
     * @var string
     * @Orm\Column(name="thoroughfare", type="string", length=50, nullable=true)
     */
    private $thoroughfare;

    /**
     * @var string
     * @Orm\Column(name="post_town", type="string", length=50, nullable=true)
     */
    private $postTown;

    /**
     * @var string
     * @Orm\Column(name="county", type="string", length=50, nullable=true)
     */
    private $county;

    /**
     * @var string
     * @Orm\Column(name="country", type="string", length=50, nullable=true)
     */
    private $country;

    /**
     * @var string
     * @Orm\Column(name="postcode", type="string", length=15, nullable=true)
     */
    private $postcode;

    /**
     * @var string
     * @Orm\Column(name="care_of_name", type="string", length=100, nullable=true)
     */
    private $careOfName;

    /**
     * @var string
     * @Orm\Column(name="po_box", type="string", length=10, nullable=true)
     */
    private $poBox;

    /**
     * @var boolean
     * @Orm\Column(name="same_as_reg_office", type="boolean", nullable=true)
     */
    private $sameAsRegOffice;

    /**
     * @var string
     * @Orm\Column(name="previous_names", type="string", length=250, nullable=true)
     */
    private $previousNames;

    /**
     * @var DateTime
     * @Orm\Column(name="dob", type="date", nullable=true)
     */
    private $dob;

    /**
     * @var string
     * @Orm\Column(name="nationality", type="string", length=20, nullable=true)
     */
    private $nationality;

    /**
     * @var string
     * @Orm\Column(name="occupation", type="string", length=35, nullable=true)
     */
    private $occupation;

    /**
     * @var string
     * @Orm\Column(name="country_of_residence", type="string", length=50, nullable=true)
     */
    private $countryOfResidence;

    /**
     * @var string
     * @Orm\Column(name="identification_type", type="string", nullable=true)
     */
    private $identificationType;

    /**
     * @var string
     * @Orm\Column(name="place_registered", type="string", length=50, nullable=true)
     */
    private $placeRegistered;

    /**
     * @var string
     * @Orm\Column(name="registration_number", type="string", length=20, nullable=true)
     */
    private $registrationNumber;

    /**
     * @var string
     * @Orm\Column(name="law_governed", type="string", length=50, nullable=true)
     */
    private $lawGoverned;

    /**
     * @var string
     * @Orm\Column(name="legal_form", type="string", length=50, nullable=true)
     */
    private $legalForm;

    /**
     * @var boolean
     * @Orm\Column(name="eea_not_synced", type="boolean", nullable=true)
     */
    private $eeaNotSynced;

    /**
     * @var boolean
     * @Orm\Column(name="eea_complete", type="boolean", nullable=true)
     */
    private $eeaComplete;

    /**
     * @var boolean
     * @Orm\Column(name="country_not_synced", type="boolean", nullable=true)
     */
    private $countryNotSynced;

    /**
     * @var boolean
     * @Orm\Column(name="country_of_residence_not_synced", type="boolean", nullable=true)
     */
    private $countryOfResidenceNotSynced;

    /**
     * @var AnnualReturn
     * @Orm\ManyToOne(targetEntity = "Entities\CompanyHouse\FormSubmission\AnnualReturn", inversedBy = "officers")
     * @Orm\JoinColumn(name = "form_submission_id", referencedColumnName="form_submission_id")
     */
    private $annualReturn;

    /**
     * @return AnnualReturn
     */
    public function getAnnualReturn()
    {
        return $this->annualReturn;
    }

    /**
     * @param AnnualReturn $annualReturn
     */
    public function setAnnualReturn(AnnualReturn $annualReturn)
    {
        $this->annualReturn = $annualReturn;
    }

    /**
     * @return integer
     */
    public function getOfficerId()
    {
        return $this->officerId;
    }

    /**
     * @return integer
     */
    public function getFormSubmissionId()
    {
        return $this->formSubmissionId;
    }

    /**
     * @param integer $formSubmissionId
     * @return Officer
     */
    public function setFormSubmissionId($formSubmissionId)
    {
        $this->formSubmissionId = $formSubmissionId;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Officer
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getCorporate()
    {
        return $this->corporate;
    }

    /**
     * @param boolean $corporate
     * @return Officer
     */
    public function setCorporate($corporate)
    {
        $this->corporate = $corporate;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getDesignatedInd()
    {
        return $this->designatedInd;
    }

    /**
     * @param boolean $designatedInd
     * @return Officer
     */
    public function setDesignatedInd($designatedInd)
    {
        $this->designatedInd = $designatedInd;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Officer
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getForename()
    {
        return $this->forename;
    }

    /**
     * @param string $forename
     * @return Officer
     */
    public function setForename($forename)
    {
        $this->forename = $forename;

        return $this;
    }

    /**
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * @param string $middleName
     * @return Officer
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;

        return $this;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     * @return Officer
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * @return string
     */
    public function getCorporateName()
    {
        return $this->corporateName;
    }

    /**
     * @param string $corporateName
     * @return Officer
     */
    public function setCorporateName($corporateName)
    {
        $this->corporateName = $corporateName;

        return $this;
    }

    /**
     * @return string
     */
    public function getPremise()
    {
        return $this->premise;
    }

    /**
     * @param string $premise
     * @return Officer
     */
    public function setPremise($premise)
    {
        $this->premise = $premise;

        return $this;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     * @return Officer
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * @return string
     */
    public function getThoroughfare()
    {
        return $this->thoroughfare;
    }

    /**
     * @param string $thoroughfare
     * @return Officer
     */
    public function setThoroughfare($thoroughfare)
    {
        $this->thoroughfare = $thoroughfare;

        return $this;
    }

    /**
     * @return string
     */
    public function getPostTown()
    {
        return $this->postTown;
    }

    /**
     * @param string $postTown
     * @return Officer
     */
    public function setPostTown($postTown)
    {
        $this->postTown = $postTown;

        return $this;
    }

    /**
     * @return string
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * @param string $county
     * @return Officer
     */
    public function setCounty($county)
    {
        $this->county = $county;

        return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     * @return Officer
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @param string $postcode
     * @return Officer
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * @return string
     */
    public function getCareOfName()
    {
        return $this->careOfName;
    }

    /**
     * @param string $careOfName
     * @return Officer
     */
    public function setCareOfName($careOfName)
    {
        $this->careOfName = $careOfName;

        return $this;
    }

    /**
     * @return string
     */
    public function getPoBox()
    {
        return $this->poBox;
    }

    /**
     * @param string $poBox
     * @return Officer
     */
    public function setPoBox($poBox)
    {
        $this->poBox = $poBox;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getSameAsRegOffice()
    {
        return $this->sameAsRegOffice;
    }

    /**
     * @param boolean $sameAsRegOffice
     * @return Officer
     */
    public function setSameAsRegOffice($sameAsRegOffice)
    {
        $this->sameAsRegOffice = $sameAsRegOffice;

        return $this;
    }

    /**
     * @return string
     */
    public function getPreviousNames()
    {
        return $this->previousNames;
    }

    /**
     * @param string $previousNames
     * @return Officer
     */
    public function setPreviousNames($previousNames)
    {
        $this->previousNames = $previousNames;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * @param DateTime $dob
     * @return Officer
     */
    public function setDob($dob)
    {
        $this->dob = $dob;

        return $this;
    }

    /**
     * @return string
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * @param string $nationality
     * @return Officer
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;

        return $this;
    }

    /**
     * @return string
     */
    public function getOccupation()
    {
        return $this->occupation;
    }

    /**
     * @param string $occupation
     * @return Officer
     */
    public function setOccupation($occupation)
    {
        $this->occupation = $occupation;

        return $this;
    }

    /**
     * @return string
     */
    public function getCountryOfResidence()
    {
        return $this->countryOfResidence;
    }

    /**
     * @param string $countryOfResidence
     * @return Officer
     */
    public function setCountryOfResidence($countryOfResidence)
    {
        $this->countryOfResidence = $countryOfResidence;

        return $this;
    }

    /**
     * @return string
     */
    public function getIdentificationType()
    {
        return $this->identificationType;
    }

    /**
     * @param string $identificationType
     * @return Officer
     */
    public function setIdentificationType($identificationType)
    {
        $this->identificationType = $identificationType;

        return $this;
    }

    /**
     * @return string
     */
    public function getPlaceRegistered()
    {
        return $this->placeRegistered;
    }

    /**
     * @param string $placeRegistered
     * @return Officer
     */
    public function setPlaceRegistered($placeRegistered)
    {
        $this->placeRegistered = $placeRegistered;

        return $this;
    }

    /**
     * @return string
     */
    public function getRegistrationNumber()
    {
        return $this->registrationNumber;
    }

    /**
     * @param string $registrationNumber
     * @return Officer
     */
    public function setRegistrationNumber($registrationNumber)
    {
        $this->registrationNumber = $registrationNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getLawGoverned()
    {
        return $this->lawGoverned;
    }

    /**
     * @param string $lawGoverned
     * @return Officer
     */
    public function setLawGoverned($lawGoverned)
    {
        $this->lawGoverned = $lawGoverned;

        return $this;
    }

    /**
     * @return string
     */
    public function getLegalForm()
    {
        return $this->legalForm;
    }

    /**
     * @param string $legalForm
     * @return Officer
     */
    public function setLegalForm($legalForm)
    {
        $this->legalForm = $legalForm;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getEeaNotSynced()
    {
        return $this->eeaNotSynced;
    }

    /**
     * @param boolean $eeaNotSynced
     * @return Officer
     */
    public function setEeaNotSynced($eeaNotSynced)
    {
        $this->eeaNotSynced = $eeaNotSynced;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getEeaComplete()
    {
        return $this->eeaComplete;
    }

    /**
     * @param boolean $eeaComplete
     * @return Officer
     */
    public function setEeaComplete($eeaComplete)
    {
        $this->eeaComplete = $eeaComplete;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getCountryNotSynced()
    {
        return $this->countryNotSynced;
    }

    /**
     * @param boolean $countryNotSynced
     * @return Officer
     */
    public function setCountryNotSynced($countryNotSynced)
    {
        $this->countryNotSynced = $countryNotSynced;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getCountryOfResidenceNotSynced()
    {
        return $this->countryOfResidenceNotSynced;
    }

    /**
     * @param boolean $countryOfResidenceNotSynced
     * @return Officer
     */
    public function setCountryOfResidenceNotSynced($countryOfResidenceNotSynced)
    {
        $this->countryOfResidenceNotSynced = $countryOfResidenceNotSynced;

        return $this;
    }

}
