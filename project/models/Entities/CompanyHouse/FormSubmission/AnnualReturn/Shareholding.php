<?php

namespace Entities\CompanyHouse\FormSubmission\AnnualReturn;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Entities\EntityAbstract;
use Entities\CompanyHouse\FormSubmission\AnnualReturn\Shareholder;
use Entities\CompanyHouse\FormSubmission\AnnualReturn\Transfer;
use Entities\CompanyHouse\FormSubmission\AnnualReturn;

/**
 * @Orm\Table(name="ch_annual_return_shareholding")
 * @Orm\Entity
 */
class Shareholding extends EntityAbstract
{

    /**
     * @var integer
     * @Orm\Column(name="annual_return_shareholding_id", type="integer", nullable=false)
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $shareholdingId;

    /**
     * @var integer
     * @Orm\Column(name="form_submission_id", type="integer", nullable=false)
     */
    private $formSubmissionId;

    /**
     * @var string
     * @Orm\Column(name="share_class", type="string", length=50, nullable=true)
     */
    private $shareClass;

    /**
     * @var string
     * @Orm\Column(name="number_held", type="string", length=250, nullable=true)
     */
    private $numberHeld;

    /**
     * @var boolean
     * @Orm\Column(name="sync", type="boolean", nullable=false)
     */
    private $sync = FALSE;

    /**
     * @var Shareholder[]|ArrayCollection
     * @Orm\OneToMany(targetEntity="ShareHolder", mappedBy="shareholding", cascade={"persist"}, orphanRemoval=true)
     */
    private $shareholders;

    /**
     * @var Transfer[]|ArrayCollection
     * @Orm\OneToMany(targetEntity="Transfer", mappedBy="shareholding", cascade={"persist"}, orphanRemoval=true)
     */
    private $transfers;

    /**
     * @var AnnualReturn
     * @Orm\ManyToOne(targetEntity="Entities\CompanyHouse\FormSubmission\AnnualReturn", inversedBy = "shareholdings")
     * @Orm\JoinColumn(name="form_submission_id", referencedColumnName="form_submission_id")
     */
    private $annualReturn;

    /**
     * @param bool $sync
     */
    public function __construct($sync = FALSE)
    {
        $this->shareholders = new ArrayCollection();
        $this->transfers = new ArrayCollection();

        $this->setSync($sync);
    }

    /**
     * @return AnnualReturn
     */
    public function getAnnualReturn()
    {
        return $this->annualReturn;
    }

    /**
     * @param AnnualReturn $annualReturn
     */
    public function setAnnualReturn(AnnualReturn $annualReturn)
    {
        $this->annualReturn = $annualReturn;
    }

    /**
     * @param Transfer $transfer
     */
    public function addTransfer(Transfer $transfer)
    {
        $this->transfers[] = $transfer;
        $transfer->setShareholding($this);
    }

    /**
     * @param Shareholder $shareholder
     */
    public function addShareholder(Shareholder $shareholder)
    {
        $this->shareholders[] = $shareholder;
        $shareholder->setShareholding($this);
    }

    /**
     * @return integer
     */
    public function getShareholdingId()
    {
        return $this->shareholdingId;
    }

    /**
     * @return integer
     */
    public function getFormSubmissionId()
    {
        return $this->formSubmissionId;
    }

    /**
     * @param integer $formSubmissionId
     * @return Shareholding
     */
    public function setFormSubmissionId($formSubmissionId)
    {
        $this->formSubmissionId = $formSubmissionId;

        return $this;
    }

    /**
     * @return string
     */
    public function getShareClass()
    {
        return $this->shareClass;
    }

    /**
     * @param string $shareClass
     * @return Shareholding
     */
    public function setShareClass($shareClass)
    {
        $this->shareClass = $shareClass;

        return $this;
    }

    /**
     * @return string
     */
    public function getNumberHeld()
    {
        return $this->numberHeld;
    }

    /**
     * @param string $numberHeld
     * @return Shareholding
     */
    public function setNumberHeld($numberHeld)
    {
        $this->numberHeld = $numberHeld;

        return $this;
    }

    /**
     * @return bool
     */
    public function getSync()
    {
        return $this->sync;
    }

    /**
     * @param bool $sync
     * @return Shareholding
     */
    public function setSync($sync)
    {
        $this->sync = (bool) $sync;

        return $this;
    }

    /**
     * @return Shareholder[]|ArrayCollection
     */
    public function getShareholders()
    {
        return $this->shareholders;
    }

    /**
     * @param Shareholder[]|ArrayCollection $shareholders
     */
    public function setShareholders($shareholders)
    {
        $this->shareholders = $shareholders;
    }

    /**
     * @return Transfer[]|ArrayCollection
     */
    public function getTransfers()
    {
        return $this->transfers;
    }

    /**
     * @param Transfer[]|ArrayCollection $transfers
     */
    public function setTransfers($transfers)
    {
        $this->transfers = $transfers;
    }

    public function __clone()
    {
        if ($this->shareholdingId) {
            $this->shareholdingId = NULL;
            $this->cloneShareholders();
            $this->cloneTransfers();
        }
    }

    private function cloneShareholders()
    {
        $shareholders = $this->getShareholders();
        $this->shareholders = new ArrayCollection();

        foreach ($shareholders as $shareholder) {
            $shareholderClone = clone $shareholder;
            $this->shareholders->add($shareholderClone);
            $shareholderClone->setShareholding($this);
        }
    }

    private function cloneTransfers()
    {
        $transfers = $this->getTransfers();
        $this->transfers = new ArrayCollection();

        foreach ($transfers as $transfer) {
            $transferClone = clone $transfer;
            $this->transfers->add($transferClone);
            $transferClone->setShareholding($this);
        }
    }
}
