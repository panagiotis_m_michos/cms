<?php

namespace Entities\CompanyHouse\FormSubmission\AnnualReturn;

use Doctrine\ORM\Mapping as ORM;
use Entities\EntityAbstract;
use Entities\CompanyHouse\FormSubmission\AnnualReturn\Shareholding;

/**
 * @Orm\Table(name="ch_annual_return_shareholder")
 * @Orm\Entity
 */
class Shareholder extends EntityAbstract
{

    /**
     * @var integer
     * @Orm\Column(name="annual_return_shareholder_id", type="integer", nullable=false)
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $shareholderId;

    /**
     * @var string
     * @Orm\Column(name="forename", type="string", length=50, nullable=true)
     */
    private $forename;

    /**
     * @var string
     * @Orm\Column(name="middle_name", type="string", length=50, nullable=true)
     */
    private $middleName;

    /**
     * @var string
     * @Orm\Column(name="surname", type="string", length=160, nullable=true)
     */
    private $surname;

    /**
     * @var string
     * @Orm\Column(name="premise", type="string", length=50, nullable=true)
     */
    private $premise;

    /**
     * @var string
     * @Orm\Column(name="street", type="string", length=50, nullable=true)
     */
    private $street;

    /**
     * @var string
     * @Orm\Column(name="thoroughfare", type="string", length=50, nullable=true)
     */
    private $thoroughfare;

    /**
     * @var string
     * @Orm\Column(name="post_town", type="string", length=50, nullable=true)
     */
    private $postTown;

    /**
     * @var string
     * @Orm\Column(name="county", type="string", length=50, nullable=true)
     */
    private $county;

    /**
     * @var string
     * @Orm\Column(name="country", type="string", length=50, nullable=true)
     */
    private $country;

    /**
     * @var string
     * @Orm\Column(name="postcode", type="string", length=15, nullable=true)
     */
    private $postcode;

    /**
     * @var boolean
     * @Orm\Column(name="isRemoved", type="boolean", nullable=false)
     */
    private $isRemoved = 0;

    /**
     * @var Shareholding
     * @Orm\ManyToOne(targetEntity = "Shareholding", inversedBy = "shareholders")
     * @Orm\JoinColumn(name = "annual_return_shareholding_id", referencedColumnName="annual_return_shareholding_id")
     */
    private $shareholding;

    /**
     * @param int $isRemoved
     */
    public function __construct($isRemoved = 0)
    {
        $this->setIsRemoved($isRemoved);
    }

    /**
     * @return int
     */
    public function getShareholderId()
    {
        return $this->shareholderId;
    }

    /**
     * @return Shareholding
     */
    public function getShareholding()
    {
        return $this->shareholding;
    }

    /**
     * @param Shareholding $shareholding
     */
    public function setShareholding(Shareholding $shareholding)
    {
        $this->shareholding = $shareholding;
    }

    /**
     * @return string
     */
    public function getForename()
    {
        return $this->forename;
    }

    /**
     * @param string $forename
     */
    public function setForename($forename)
    {
        $this->forename = $forename;
    }

    /**
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * @param string $middleName
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return string
     */
    public function getPremise()
    {
        return $this->premise;
    }

    /**
     * @param string $premise
     */
    public function setPremise($premise)
    {
        $this->premise = $premise;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getThoroughfare()
    {
        return $this->thoroughfare;
    }

    /**
     * @param string $thoroughfare
     */
    public function setThoroughfare($thoroughfare)
    {
        $this->thoroughfare = $thoroughfare;
    }

    /**
     * @return string
     */
    public function getPostTown()
    {
        return $this->postTown;
    }

    /**
     * @param string $postTown
     */
    public function setPostTown($postTown)
    {
        $this->postTown = $postTown;
    }

    /**
     * @return string
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * @param string $county
     */
    public function setCounty($county)
    {
        $this->county = $county;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @param string $postcode
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    }

    /**
     * @return boolean
     */
    public function getIsRemoved()
    {
        return $this->isRemoved;
    }

    /**
     * @param $isRemoved
     */
    public function setIsRemoved($isRemoved)
    {
        $this->isRemoved = $isRemoved;
    }
}
