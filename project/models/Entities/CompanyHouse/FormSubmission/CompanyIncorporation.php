<?php

namespace Entities\CompanyHouse\FormSubmission;

use Doctrine\ORM\Mapping as Orm;
use Entities\Company;
use Entities\CompanyHouse\FormSubmission;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation\Subscriber;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation\Appointment;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation\Capital;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation\Document;
use Doctrine\Common\Collections\ArrayCollection;
use Entities\CompanyHouse\Helper\Address;

/**
 * @Orm\Table(name="ch_company_incorporation")
 * @Orm\Entity(repositoryClass = "Repositories\CompanyHouse\FormSubmission\CompanyIncorporationRepository")
 */
class CompanyIncorporation extends FormSubmission
{

    const TYPE_BYSHR = 'BYSHR';
    const TYPE_PLC = 'PLC';
    const TYPE_LLP = 'LLP';

    /**
     * @var boolean
     * @Orm\Column(name="msg_address", type="boolean", nullable=false)
     */
    private $msgAddress;

    /**
     * @var string
     * @Orm\Column(name="type", type="string", nullable=true)
     */
    private $type;

    /**
     * @var string
     * @Orm\Column(name="reject_reference", type="string", length=8, nullable=true)
     */
    private $rejectReference;

    /**
     * @var string
     * @Orm\Column(name="reject_description", type="text", nullable=true)
     */
    private $rejectDescription;

    /**
     * @var boolean
     * @Orm\Column(name="same_day", type="boolean", nullable=false)
     */
    private $sameDay;

    /**
     * @var string
     * @Orm\Column(name="country_of_incorporation", type="string", nullable=true)
     */
    private $countryOfIncorporation;

    /**
     * @var string
     * @Orm\Column(name="premise", type="string", length=50, nullable=true)
     */
    private $premise;

    /**
     * @var string
     * @Orm\Column(name="street", type="string", length=50, nullable=true)
     */
    private $street;

    /**
     * @var string
     * @Orm\Column(name="thoroughfare", type="string", length=50, nullable=true)
     */
    private $thoroughfare;

    /**
     * @var string
     * @Orm\Column(name="post_town", type="string", length=50, nullable=true)
     */
    private $postTown;

    /**
     * @var string
     * @Orm\Column(name="county", type="string", length=50, nullable=true)
     */
    private $county;

    /**
     * @var string
     * @Orm\Column(name="country", type="string", nullable=true)
     */
    private $country;

    /**
     * @var string
     * @Orm\Column(name="postcode", type="string", length=15, nullable=true)
     */
    private $postcode;

    /**
     * @var string
     * @Orm\Column(name="care_of_name", type="string", length=100, nullable=true)
     */
    private $careOfName;

    /**
     * @var string
     * @Orm\Column(name="po_box", type="string", length=10, nullable=true)
     */
    private $poBox;

    /**
     * @var string
     * @Orm\Column(name="articles", type="string", nullable=true)
     */
    private $articles;

    /**
     * @var boolean
     * @Orm\Column(name="restricted_articles", type="boolean", nullable=true)
     */
    private $restrictedArticles;

    /**
     * @var boolean
     * @Orm\Column(name="same_name", type="boolean", nullable=true)
     */
    private $sameName;

    /**
     * @var boolean
     * @Orm\Column(name="name_authorisation", type="boolean", nullable=true)
     */
    private $nameAuthorisation;

    /**
     * @var Capital[]|ArrayCollection
     * @Orm\OneToMany(targetEntity = "Entities\CompanyHouse\FormSubmission\CompanyIncorporation\Capital", mappedBy = "formSubmission", cascade={"persist"}, orphanRemoval=true)
     */
    private $capitals;

    /**
     * @var Subscriber[]|ArrayCollection
     * @Orm\OneToMany(targetEntity = "Entities\CompanyHouse\FormSubmission\CompanyIncorporation\Subscriber", mappedBy = "formSubmission", cascade={"persist"}, orphanRemoval=true)
     */
    private $subscribers;

    /**
     * @var Appointment[]|ArrayCollection
     * @Orm\OneToMany(targetEntity = "Entities\CompanyHouse\FormSubmission\CompanyIncorporation\Appointment", mappedBy = "formSubmission", cascade={"persist"}, orphanRemoval=true)
     */
    private $appointments;

    /**
     * @var Document[]|ArrayCollection
     * @Orm\OneToMany(targetEntity = "Entities\CompanyHouse\FormSubmission\CompanyIncorporation\Document", mappedBy = "formSubmission", cascade={"persist"}, orphanRemoval=true)
     */
    private $documents;

    /**
     * @var Address
     */
    private $registeredOfficeAddress;

    /**
     * @var string
     *
     * @Orm\Column(type="string", length=5, nullable=true)
     */
    private $sicCode1 = '82990';

    /**
     * @var string
     *
     * @Orm\Column(type="string", length=5, nullable=true)
     */
    private $sicCode2;

    /**
     * @var string
     *
     * @Orm\Column(type="string", length=5, nullable=true)
     */
    private $sicCode3;

    /**
     * @var string
     *
     * @Orm\Column(type="string", length=5, nullable=true)
     */
    private $sicCode4;

    /**
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        parent::__construct($company);

        $this->capitals = new ArrayCollection;
        $this->appointments = new ArrayCollection;
        $this->subscribers = new ArrayCollection;
        $this->documents = new ArrayCollection;
    }

    /**
     * @return array
     */
    public function getSicCodes()
    {
        return array_filter(
            [
                $this->sicCode1,
                $this->sicCode2,
                $this->sicCode3,
                $this->sicCode4
            ]
        );
    }

    /**
     * @param array $sicCodes
     */
    public function setSicCodes(array $sicCodes)
    {
        $sicCodes = array_pad(array_values(array_unique($sicCodes)), 4, NULL);

        $this->sicCode1 = $sicCodes[0];
        $this->sicCode2 = $sicCodes[1];
        $this->sicCode3 = $sicCodes[2];
        $this->sicCode4 = $sicCodes[3];

    }

    /**
     * @param Subscriber $subscriber
     */
    public function addSubscriber(Subscriber $subscriber)
    {
        $this->subscribers[] = $subscriber;
        $subscriber->setFormSubmission($this);
    }

    /**
     * @param Capital $capital
     */
    public function addCapital(Capital $capital)
    {
        $this->capitals[] = $capital;
        $capital->setFormSubmission($this);
    }

    /**
     * @param Appointment $appointment
     */
    public function addAppointment(Appointment $appointment)
    {
        $this->appointments[] = $appointment;
        $appointment->setFormSubmission($this);
    }

    /**
     * @param Document $document
     */
    public function addDocument(Document $document)
    {
        $this->documents[] = $document;
        $document->setFormSubmission($this);
    }

    /**
     * @return Address
     */
    public function getRegisteredOfficeAddress()
    {
        return $this->registeredOfficeAddress;
    }

    /**
     * @param Address $registeredOfficeAddress
     */
    public function setRegisteredOfficeAddress(Address $registeredOfficeAddress)
    {
        $this->registeredOfficeAddress = $registeredOfficeAddress;
        $this->extractRegisteredOfficeAddress();
    }

    /**
     * @Orm\PreOnFlush
     * set all properties when this object is to
     * save so that doctrine can easily save these scalar values
     */
    public function extractRegisteredOfficeAddress()
    {
        $this->premise = $this->registeredOfficeAddress->getPremise();
        $this->street = $this->registeredOfficeAddress->getStreet();
        $this->postTown = $this->registeredOfficeAddress->getPostTown();
        $this->postcode = $this->registeredOfficeAddress->getPostcode();
        $this->country = $this->registeredOfficeAddress->getCountry();
        $this->county = $this->registeredOfficeAddress->getCounty();
        $this->thoroughfare = $this->registeredOfficeAddress->getThoroughfare();
        $this->poBox = $this->registeredOfficeAddress->getPoBox();
        $this->careOfName = $this->registeredOfficeAddress->getCareOfName();
    }

    /**
     * @return boolean
     */
    public function getMsgAddress()
    {
        return $this->msgAddress;
    }

    /**
     * @param boolean $msgAddress
     */
    public function setMsgAddress($msgAddress)
    {
        $this->msgAddress = $msgAddress;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getRejectReference()
    {
        return $this->rejectReference;
    }

    /**
     * @param string $rejectReference
     */
    public function setRejectReference($rejectReference)
    {
        $this->rejectReference = $rejectReference;
    }

    /**
     * @return string
     */
    public function getRejectDescription()
    {
        return $this->rejectDescription;
    }

    /**
     * @param string $rejectDescription
     */
    public function setRejectDescription($rejectDescription)
    {
        $this->rejectDescription = $rejectDescription;
    }

    /**
     * @return boolean
     */
    public function getSameDay()
    {
        return $this->sameDay;
    }

    /**
     * @param boolean $sameDay
     */
    public function setSameDay($sameDay)
    {
        $this->sameDay = $sameDay;
    }

    /**
     * @return string
     */
    public function getCountryOfIncorporation()
    {
        return $this->countryOfIncorporation;
    }

    /**
     * @param string $countryOfIncorporation
     */
    public function setCountryOfIncorporation($countryOfIncorporation)
    {
        $this->countryOfIncorporation = $countryOfIncorporation;
    }

    /**
     * @return string
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * @param string $articles
     */
    public function setArticles($articles)
    {
        $this->articles = $articles;
    }

    /**
     * @return boolean
     */
    public function getRestrictedArticles()
    {
        return $this->restrictedArticles;
    }

    /**
     * @param boolean $restrictedArticles
     */
    public function setRestrictedArticles($restrictedArticles)
    {
        $this->restrictedArticles = $restrictedArticles;
    }

    /**
     * @return boolean
     */
    public function getSameName()
    {
        return $this->sameName;
    }

    /**
     * @param boolean $sameName
     */
    public function setSameName($sameName)
    {
        $this->sameName = $sameName;
    }

    /**
     * @return boolean
     */
    public function getNameAuthorisation()
    {
        return $this->nameAuthorisation;
    }

    /**
     * @param boolean $nameAuthorisation
     */
    public function setNameAuthorisation($nameAuthorisation)
    {
        $this->nameAuthorisation = $nameAuthorisation;
    }

    /**
     * @ORM\PostLoad
     * When the row is hydrated into this class,
     * registeredOfficeAddress is not set because that isn't mapped.
     * so simply, map it manually
     */
    public function packRegisteredOfficeAddress()
    {
        $this->registeredOfficeAddress = new Address(
            $this->premise,
            $this->street,
            $this->postTown,
            $this->postcode,
            $this->country
        );
        $this->registeredOfficeAddress->setCounty($this->county);
        $this->registeredOfficeAddress->setThoroughfare($this->thoroughfare);
        $this->registeredOfficeAddress->setPoBox($this->poBox);
        $this->registeredOfficeAddress->setCareOfName($this->careOfName);
    }

    /**
     * @return Capital[]|ArrayCollection
     */
    public function getCapitals()
    {
        return $this->capitals;
    }

    /**
     * @param Capital[] $capitals
     */
    public function setCapitals($capitals)
    {
        $this->capitals = new ArrayCollection;

        foreach ($capitals as $capital) {
            $this->addCapital($capital);
        }
    }

    /**
     * @return Subscriber[]|ArrayCollection
     */
    public function getSubscribers()
    {
        return $this->subscribers;
    }

    /**
     * @param Subscriber[] $subscribers
     */
    public function setSubscribers($subscribers)
    {
        $this->subscribers = new ArrayCollection;

        foreach ($subscribers as $subscriber) {
            $this->addSubscriber($subscriber);
        }
    }

    /**
     * @return Appointment[]|ArrayCollection
     */
    public function getAppointments()
    {
        return $this->appointments;
    }

    /**
     * @param Appointment[] $appointments
     */
    public function setAppointments($appointments)
    {
        $this->appointments = new ArrayCollection;

        foreach ($appointments as $appointment) {
            $this->addAppointment($appointment);
        }
    }

    /**
     * @return Document[]|ArrayCollection
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * @param Document[] $documents
     */
    public function setDocuments($documents)
    {
        $this->documents = new ArrayCollection;

        foreach ($documents as $document) {
            $this->addDocument($document);
        }
    }

    /**
     * @return int|null
     */
    public function getCompanyId()
    {
        return $this->getCompany() ? $this->getCompany()->getId() : NULL;
    }

    public function __clone()
    {
        if ($this->getFormSubmissionId()) {
            $this->rejectReference = NULL;
            $this->rejectDescription = NULL;

            $this->cloneCapitals();
            $this->cloneSubscribers();
            $this->cloneAppointments();
            $this->cloneDocuments();
        }

        parent::__clone();
    }

    private function cloneCapitals()
    {
        $capitals = $this->getCapitals();
        $this->capitals = new ArrayCollection();

        foreach ($capitals as $capital) {
            $capitalClone = clone $capital;
            $this->capitals->add($capitalClone);
            $capitalClone->setFormSubmission($this);
        }
    }

    private function cloneSubscribers()
    {
        $subscribers = $this->getSubscribers();
        $this->subscribers = new ArrayCollection();

        foreach ($subscribers as $subscriber) {
            $subscriberClone = clone $subscriber;
            $this->subscribers->add($subscriberClone);
            $subscriberClone->setFormSubmission($this);
        }
    }

    private function cloneAppointments()
    {
        $appointments = $this->getAppointments();
        $this->appointments = new ArrayCollection();

        foreach ($appointments as $appointment) {
            $appointmentClone = clone $appointment;
            $this->appointments->add($appointmentClone);
            $appointmentClone->setFormSubmission($this);
        }
    }

    private function cloneDocuments()
    {
        $documents = $this->getDocuments();
        $this->documents = new ArrayCollection();

        foreach ($documents as $document) {
            $documentClone = clone $document;
            $this->documents->add($documentClone);
            $documentClone->setFormSubmission($this);
        }
    }
}
