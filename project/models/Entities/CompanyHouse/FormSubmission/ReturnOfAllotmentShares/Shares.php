<?php

namespace Entities\CompanyHouse\FormSubmission\ReturnOfAllotmentShares;

use Doctrine\ORM\Mapping as ORM;
use Entities\CompanyHouse\FormSubmission\ReturnOfAllotmentShares;
use Entities\EntityAbstract;
use Entities\CompanyHouse\FormSubmission\AnnualReturn;

/**
 * @Orm\Table(name="ch_return_shares")
 * @Orm\Entity
 */
class Shares extends EntityAbstract
{

    /**
     * @var integer
     * @Orm\Column(name="return_shares_id", type="integer", nullable=false)
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $sharesId;

    /**
     * @var integer
     * @Orm\Column(name="form_submission_id", type="integer", nullable=false)
     */
    private $formSubmissionId;

    /**
     * @var string
     * @Orm\Column(name="share_class", type="string", length=50, nullable=true)
     */
    private $shareClass;

    /**
     * @var string
     * @Orm\Column(name="prescribed_particulars", type="text", nullable=true)
     */
    private $prescribedParticulars;

    /**
     * @var string
     * @Orm\Column(name="num_shares", type="string", length=11, nullable=true)
     */
    private $numShares;

    /**
     * @var string
     * @Orm\Column(name="amount_paid", type="string", length=20, nullable=true)
     */
    private $amountPaid;

    /**
     * @var string
     * @Orm\Column(name="amount_unpaid", type="string", length=20, nullable=true)
     */
    private $amountUnpaid;

    /**
     * @var string
     * @Orm\Column(name="currency", type="string", length=3, nullable=true)
     */
    private $currency;

    /**
     * @var string
     * @Orm\Column(name="aggregate_nom_value", type="string", length=19, nullable=true)
     */
    private $aggregateNomValue;

    /**
     * @var string
     * @Orm\Column(name="allotment", type="string", length=11, nullable=true)
     */
    private $allotment;

    /**
     * @var ReturnOfAllotmentShares
     * @Orm\ManyToOne(targetEntity="Entities\CompanyHouse\FormSubmission\ReturnOfAllotmentShares", inversedBy = "shares")
     * @Orm\JoinColumn(name="form_submission_id", referencedColumnName="form_submission_id")
     */
    private $returnOfAllotmentShares;

    /**
     * @return int
     */
    public function getSharesId()
    {
        return $this->sharesId;
    }

    /**
     * @return int
     */
    public function getFormSubmissionId()
    {
        return $this->formSubmissionId;
    }

    /**
     * @param int $formSubmissionId
     */
    public function setFormSubmissionId($formSubmissionId)
    {
        $this->formSubmissionId = $formSubmissionId;
    }

    /**
     * @return string
     */
    public function getShareClass()
    {
        return $this->shareClass;
    }

    /**
     * @param string $shareClass
     */
    public function setShareClass($shareClass)
    {
        $this->shareClass = $shareClass;
    }

    /**
     * @return string
     */
    public function getPrescribedParticulars()
    {
        return $this->prescribedParticulars;
    }

    /**
     * @param string $prescribedParticulars
     */
    public function setPrescribedParticulars($prescribedParticulars)
    {
        $this->prescribedParticulars = $prescribedParticulars;
    }

    /**
     * @return string
     */
    public function getNumShares()
    {
        return $this->numShares;
    }

    /**
     * @param string $numShares
     */
    public function setNumShares($numShares)
    {
        $this->numShares = $numShares;
    }

    /**
     * @return string
     */
    public function getAmountPaid()
    {
        return $this->amountPaid;
    }

    /**
     * @param string $amountPaid
     */
    public function setAmountPaid($amountPaid)
    {
        $this->amountPaid = $amountPaid;
    }

    /**
     * @return string
     */
    public function getAmountUnpaid()
    {
        return $this->amountUnpaid;
    }

    /**
     * @param string $amountUnpaid
     */
    public function setAmountUnpaid($amountUnpaid)
    {
        $this->amountUnpaid = $amountUnpaid;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getAggregateNomValue()
    {
        return $this->aggregateNomValue;
    }

    /**
     * @param string $aggregateNomValue
     */
    public function setAggregateNomValue($aggregateNomValue)
    {
        $this->aggregateNomValue = $aggregateNomValue;
    }

    /**
     * @return string
     */
    public function getAllotment()
    {
        return $this->allotment;
    }

    /**
     * @param string $allotment
     */
    public function setAllotment($allotment)
    {
        $this->allotment = $allotment;
    }

    /**
     * @return ReturnOfAllotmentShares
     */
    public function getReturnOfAllotmentShares()
    {
        return $this->returnOfAllotmentShares;
    }

    /**
     * @param ReturnOfAllotmentShares $returnOfAllotmentShares
     */
    public function setReturnOfAllotmentShares(ReturnOfAllotmentShares $returnOfAllotmentShares)
    {
        $this->returnOfAllotmentShares = $returnOfAllotmentShares;
    }
}
