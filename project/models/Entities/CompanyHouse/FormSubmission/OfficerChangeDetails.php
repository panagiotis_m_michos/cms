<?php

namespace Entities\CompanyHouse\FormSubmission;

use Doctrine\ORM\Mapping as ORM;
use Entities\CompanyHouse\FormSubmission;
use DateTime;

/**
 * @Orm\Table(name="ch_officer_change")
 * @Orm\Entity(repositoryClass = "Repositories\CompanyHouse\FormSubmission\OfficerChangeDetailsRepository")
 */
class OfficerChangeDetails extends FormSubmission
{

    /**
     * @var string
     * @Orm\Column(name="type", type="string", nullable=true)
     */
    private $type;

    /**
     * @var boolean
     * @Orm\Column(name="corporate", type="boolean", nullable=true)
     */
    private $corporate;

    /**
     * @var boolean
     * @Orm\Column(name="designated_ind", type="boolean", nullable=true)
     */
    private $designatedInd;

    /**
     * @var string
     * @Orm\Column(name="authentication", type="string", length=45, nullable=true)
     */
    private $authentication;

    /**
     * @var bool
     * @Orm\Column(name="consentToAct", type="boolean")
     */
    private $consentToAct = FALSE;

    /**
     * @var string
     * @Orm\Column(name="corporate_name", type="string", length=160, nullable=true)
     */
    private $corporateName;

    /**
     * @var string
     * @Orm\Column(name="title", type="string", length=50, nullable=true)
     */
    private $title;

    /**
     * @var string
     * @Orm\Column(name="forename", type="string", length=50, nullable=true)
     */
    private $forename;

    /**
     * @var string
     * @Orm\Column(name="middle_name", type="string", length=50, nullable=true)
     */
    private $middleName;

    /**
     * @var string
     * @Orm\Column(name="surname", type="string", length=160, nullable=true)
     */
    private $surname;

    /**
     * @var DateTime
     * @Orm\Column(name="dob", type="date", nullable=true)
     */
    private $dob;

    /**
     * @var string
     * @Orm\Column(name="new_title", type="string", length=50, nullable=true)
     */
    private $newTitle;

    /**
     * @var string
     * @Orm\Column(name="new_forename", type="string", length=50, nullable=true)
     */
    private $newForename;

    /**
     * @var string
     * @Orm\Column(name="new_middle_name", type="string", length=50, nullable=true)
     */
    private $newMiddleName;

    /**
     * @var string
     * @Orm\Column(name="new_surname", type="string", length=160, nullable=true)
     */
    private $newSurname;

    /**
     * @var string
     * @Orm\Column(name="new_corporate_name", type="string", length=160, nullable=true)
     */
    private $newCorporateName;

    /**
     * @var string
     * @Orm\Column(name="new_premise", type="string", length=50, nullable=true)
     */
    private $newPremise;

    /**
     * @var string
     * @Orm\Column(name="new_street", type="string", length=50, nullable=true)
     */
    private $newStreet;

    /**
     * @var string
     * @Orm\Column(name="new_thoroughfare", type="string", length=50, nullable=true)
     */
    private $newThoroughfare;

    /**
     * @var string
     * @Orm\Column(name="new_post_town", type="string", length=50, nullable=true)
     */
    private $newPostTown;

    /**
     * @var string
     * @Orm\Column(name="new_county", type="string", length=50, nullable=true)
     */
    private $newCounty;

    /**
     * @var string
     * @Orm\Column(name="new_country", type="string", length=50, nullable=true)
     */
    private $newCountry;

    /**
     * @var string
     * @Orm\Column(name="new_postcode", type="string", length=15, nullable=true)
     */
    private $newPostcode;

    /**
     * @var string
     * @Orm\Column(name="new_care_of_name", type="string", length=100, nullable=true)
     */
    private $newCareOfName;

    /**
     * @var string
     * @Orm\Column(name="new_po_box", type="string", length=10, nullable=true)
     */
    private $newPoBox;

    /**
     * @var string
     * @Orm\Column(name="new_residential_premise", type="string", length=50, nullable=true)
     */
    private $newResidentialPremise;

    /**
     * @var string
     * @Orm\Column(name="new_residential_street", type="string", length=50, nullable=true)
     */
    private $newResidentialStreet;

    /**
     * @var string
     * @Orm\Column(name="new_residential_thoroughfare", type="string", length=50, nullable=true)
     */
    private $newResidentialThoroughfare;

    /**
     * @var string
     * @Orm\Column(name="new_residential_post_town", type="string", length=50, nullable=true)
     */
    private $newResidentialPostTown;

    /**
     * @var string
     * @Orm\Column(name="new_residential_county", type="string", length=50, nullable=true)
     */
    private $newResidentialCounty;

    /**
     * @var string
     * @Orm\Column(name="new_residential_country", type="string", length=50, nullable=true)
     */
    private $newResidentialCountry;

    /**
     * @var string
     * @Orm\Column(name="new_residential_postcode", type="string", length=15, nullable=true)
     */
    private $newResidentialPostcode;

    /**
     * @var boolean
     * @Orm\Column(name="new_residential_secure_address_ind", type="boolean", nullable=true)
     */
    private $newResidentialSecureAddressInd;

    /**
     * @var string
     * @Orm\Column(name="new_nationality", type="string", length=20, nullable=true)
     */
    private $newNationality;

    /**
     * @var string
     * @Orm\Column(name="new_country_of_residence", type="string", length=50, nullable=true)
     */
    private $newCountryOfResidence;

    /**
     * @var string
     * @Orm\Column(name="new_occupation", type="string", length=35, nullable=true)
     */
    private $newOccupation;

    /**
     * @var string
     * @Orm\Column(name="new_identification_type", type="string", nullable=true)
     */
    private $newIdentificationType;

    /**
     * @var string
     * @Orm\Column(name="new_place_registered", type="string", length=50, nullable=true)
     */
    private $newPlaceRegistered;

    /**
     * @var string
     * @Orm\Column(name="new_registration_number", type="string", length=20, nullable=true)
     */
    private $newRegistrationNumber;

    /**
     * @var string
     * @Orm\Column(name="new_law_governed", type="string", length=50, nullable=true)
     */
    private $newLawGoverned;

    /**
     * @var string
     * @Orm\Column(name="new_legal_form", type="string", length=50, nullable=true)
     */
    private $newLegalForm;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return boolean
     */
    public function getCorporate()
    {
        return $this->corporate;
    }

    /**
     * @param boolean $corporate
     */
    public function setCorporate($corporate)
    {
        $this->corporate = $corporate;
    }

    /**
     * @return boolean
     */
    public function getDesignatedInd()
    {
        return $this->designatedInd;
    }

    /**
     * @param boolean $designatedInd
     */
    public function setDesignatedInd($designatedInd)
    {
        $this->designatedInd = $designatedInd;
    }

    /**
     * @return string
     */
    public function getAuthentication()
    {
        return $this->authentication;
    }

    /**
     * @param string $authentication
     */
    public function setAuthentication($authentication)
    {
        $this->authentication = $authentication;
    }

    /**
     * @return bool
     */
    public function hasConsentToAct()
    {
        return $this->consentToAct;
    }

    /**
     * @param bool $consentToAct
     */
    public function setConsentToAct($consentToAct)
    {
        $this->consentToAct = $consentToAct;
    }

    /**
     * @return string
     */
    public function getCorporateName()
    {
        return $this->corporateName;
    }

    /**
     * @param string $corporateName
     */
    public function setCorporateName($corporateName)
    {
        $this->corporateName = $corporateName;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getForename()
    {
        return $this->forename;
    }

    /**
     * @param string $forename
     */
    public function setForename($forename)
    {
        $this->forename = $forename;
    }

    /**
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * @param string $middleName
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return DateTime
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * @param DateTime $dob
     */
    public function setDob($dob)
    {
        $this->dob = $dob;
    }

    /**
     * @return string
     */
    public function getNewTitle()
    {
        return $this->newTitle;
    }

    /**
     * @param string $newTitle
     */
    public function setNewTitle($newTitle)
    {
        $this->newTitle = $newTitle;
    }

    /**
     * @return string
     */
    public function getNewForename()
    {
        return $this->newForename;
    }

    /**
     * @param string $newForename
     */
    public function setNewForename($newForename)
    {
        $this->newForename = $newForename;
    }

    /**
     * @return string
     */
    public function getNewMiddleName()
    {
        return $this->newMiddleName;
    }

    /**
     * @param string $newMiddleName
     */
    public function setNewMiddleName($newMiddleName)
    {
        $this->newMiddleName = $newMiddleName;
    }

    /**
     * @return string
     */
    public function getNewSurname()
    {
        return $this->newSurname;
    }

    /**
     * @param string $newSurname
     */
    public function setNewSurname($newSurname)
    {
        $this->newSurname = $newSurname;
    }

    /**
     * @return string
     */
    public function getNewCorporateName()
    {
        return $this->newCorporateName;
    }

    /**
     * @param string $newCorporateName
     */
    public function setNewCorporateName($newCorporateName)
    {
        $this->newCorporateName = $newCorporateName;
    }

    /**
     * @return string
     */
    public function getNewPremise()
    {
        return $this->newPremise;
    }

    /**
     * @param string $newPremise
     */
    public function setNewPremise($newPremise)
    {
        $this->newPremise = $newPremise;
    }

    /**
     * @return string
     */
    public function getNewStreet()
    {
        return $this->newStreet;
    }

    /**
     * @param string $newStreet
     */
    public function setNewStreet($newStreet)
    {
        $this->newStreet = $newStreet;
    }

    /**
     * @return string
     */
    public function getNewThoroughfare()
    {
        return $this->newThoroughfare;
    }

    /**
     * @param string $newThoroughfare
     */
    public function setNewThoroughfare($newThoroughfare)
    {
        $this->newThoroughfare = $newThoroughfare;
    }

    /**
     * @return string
     */
    public function getNewPostTown()
    {
        return $this->newPostTown;
    }

    /**
     * @param string $newPostTown
     */
    public function setNewPostTown($newPostTown)
    {
        $this->newPostTown = $newPostTown;
    }

    /**
     * @return string
     */
    public function getNewCounty()
    {
        return $this->newCounty;
    }

    /**
     * @param string $newCounty
     */
    public function setNewCounty($newCounty)
    {
        $this->newCounty = $newCounty;
    }

    /**
     * @return string
     */
    public function getNewCountry()
    {
        return $this->newCountry;
    }

    /**
     * @param string $newCountry
     */
    public function setNewCountry($newCountry)
    {
        $this->newCountry = $newCountry;
    }

    /**
     * @return string
     */
    public function getNewPostcode()
    {
        return $this->newPostcode;
    }

    /**
     * @param string $newPostcode
     */
    public function setNewPostcode($newPostcode)
    {
        $this->newPostcode = $newPostcode;
    }

    /**
     * @return string
     */
    public function getNewCareOfName()
    {
        return $this->newCareOfName;
    }

    /**
     * @param string $newCareOfName
     */
    public function setNewCareOfName($newCareOfName)
    {
        $this->newCareOfName = $newCareOfName;
    }

    /**
     * @return string
     */
    public function getNewPoBox()
    {
        return $this->newPoBox;
    }

    /**
     * @param string $newPoBox
     */
    public function setNewPoBox($newPoBox)
    {
        $this->newPoBox = $newPoBox;
    }

    /**
     * @return string
     */
    public function getNewResidentialPremise()
    {
        return $this->newResidentialPremise;
    }

    /**
     * @param string $newResidentialPremise
     */
    public function setNewResidentialPremise($newResidentialPremise)
    {
        $this->newResidentialPremise = $newResidentialPremise;
    }

    /**
     * @return string
     */
    public function getNewResidentialStreet()
    {
        return $this->newResidentialStreet;
    }

    /**
     * @param string $newResidentialStreet
     */
    public function setNewResidentialStreet($newResidentialStreet)
    {
        $this->newResidentialStreet = $newResidentialStreet;
    }

    /**
     * @return string
     */
    public function getNewResidentialThoroughfare()
    {
        return $this->newResidentialThoroughfare;
    }

    /**
     * @param string $newResidentialThoroughfare
     */
    public function setNewResidentialThoroughfare($newResidentialThoroughfare)
    {
        $this->newResidentialThoroughfare = $newResidentialThoroughfare;
    }

    /**
     * @return string
     */
    public function getNewResidentialPostTown()
    {
        return $this->newResidentialPostTown;
    }

    /**
     * @param string $newResidentialPostTown
     */
    public function setNewResidentialPostTown($newResidentialPostTown)
    {
        $this->newResidentialPostTown = $newResidentialPostTown;
    }

    /**
     * @return string
     */
    public function getNewResidentialCounty()
    {
        return $this->newResidentialCounty;
    }

    /**
     * @param string $newResidentialCounty
     */
    public function setNewResidentialCounty($newResidentialCounty)
    {
        $this->newResidentialCounty = $newResidentialCounty;
    }

    /**
     * @return string
     */
    public function getNewResidentialCountry()
    {
        return $this->newResidentialCountry;
    }

    /**
     * @param string $newResidentialCountry
     */
    public function setNewResidentialCountry($newResidentialCountry)
    {
        $this->newResidentialCountry = $newResidentialCountry;
    }

    /**
     * @return string
     */
    public function getNewResidentialPostcode()
    {
        return $this->newResidentialPostcode;
    }

    /**
     * @param string $newResidentialPostcode
     */
    public function setNewResidentialPostcode($newResidentialPostcode)
    {
        $this->newResidentialPostcode = $newResidentialPostcode;
    }

    /**
     * @return boolean
     */
    public function getNewResidentialSecureAddressInd()
    {
        return $this->newResidentialSecureAddressInd;
    }

    /**
     * @param boolean $newResidentialSecureAddressInd
     */
    public function setNewResidentialSecureAddressInd($newResidentialSecureAddressInd)
    {
        $this->newResidentialSecureAddressInd = $newResidentialSecureAddressInd;
    }

    /**
     * @return string
     */
    public function getNewNationality()
    {
        return $this->newNationality;
    }

    /**
     * @param string $newNationality
     */
    public function setNewNationality($newNationality)
    {
        $this->newNationality = $newNationality;
    }

    /**
     * @return string
     */
    public function getNewCountryOfResidence()
    {
        return $this->newCountryOfResidence;
    }

    /**
     * @param string $newCountryOfResidence
     */
    public function setNewCountryOfResidence($newCountryOfResidence)
    {
        $this->newCountryOfResidence = $newCountryOfResidence;
    }

    /**
     * @return string
     */
    public function getNewOccupation()
    {
        return $this->newOccupation;
    }

    /**
     * @param string $newOccupation
     */
    public function setNewOccupation($newOccupation)
    {
        $this->newOccupation = $newOccupation;
    }

    /**
     * @return string
     */
    public function getNewIdentificationType()
    {
        return $this->newIdentificationType;
    }

    /**
     * @param string $newIdentificationType
     */
    public function setNewIdentificationType($newIdentificationType)
    {
        $this->newIdentificationType = $newIdentificationType;
    }

    /**
     * @return string
     */
    public function getNewPlaceRegistered()
    {
        return $this->newPlaceRegistered;
    }

    /**
     * @param string $newPlaceRegistered
     */
    public function setNewPlaceRegistered($newPlaceRegistered)
    {
        $this->newPlaceRegistered = $newPlaceRegistered;
    }

    /**
     * @return string
     */
    public function getNewRegistrationNumber()
    {
        return $this->newRegistrationNumber;
    }

    /**
     * @param string $newRegistrationNumber
     */
    public function setNewRegistrationNumber($newRegistrationNumber)
    {
        $this->newRegistrationNumber = $newRegistrationNumber;
    }

    /**
     * @return string
     */
    public function getNewLawGoverned()
    {
        return $this->newLawGoverned;
    }

    /**
     * @param string $newLawGoverned
     */
    public function setNewLawGoverned($newLawGoverned)
    {
        $this->newLawGoverned = $newLawGoverned;
    }

    /**
     * @return string
     */
    public function getNewLegalForm()
    {
        return $this->newLegalForm;
    }

    /**
     * @param string $newLegalForm
     */
    public function setNewLegalForm($newLegalForm)
    {
        $this->newLegalForm = $newLegalForm;
    }
}
