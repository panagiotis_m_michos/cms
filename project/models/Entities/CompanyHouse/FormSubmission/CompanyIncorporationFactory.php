<?php

namespace Entities\CompanyHouse\FormSubmission;

use Doctrine\Common\Collections\ArrayCollection;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation;
use Entities\Company;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation\Appointment;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation\Capital;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation\Document;
use Entities\CompanyHouse\FormSubmission\CompanyIncorporation\Subscriber;
use Entities\CompanyHouse\Helper\Address;

class CompanyIncorporationFactory
{
    /**
     * @param Company $company
     * @param Address $registeredOfficeAddress
     * @param string $countryOfIncorporation
     * @param Appointment[]|ArrayCollection $appointments
     * @param Capital[]|ArrayCollection $capitals
     * @param Subscriber[]|ArrayCollection $subscribers
     * @param Document[]|ArrayCollection $documents
     * @param bool $msgAddress
     * @param bool $sameDay
     * @return \Entities\CompanyHouse\FormSubmission\CompanyIncorporation
     */
    public function createByShares(
        Company $company,
        Address $registeredOfficeAddress,
        $countryOfIncorporation,
        array $appointments,
        array $capitals,
        array $subscribers,
        array $documents,
        $msgAddress,
        $sameDay
    )
    {
        $formSubmission = new CompanyIncorporation($company);
        $formSubmission->setType(CompanyIncorporation::TYPE_BYSHR);
        $formSubmission->setMsgAddress($msgAddress);
        $formSubmission->setSameDay($sameDay);
        $formSubmission->setCountryOfIncorporation($countryOfIncorporation);
        $formSubmission->setRegisteredOfficeAddress($registeredOfficeAddress);
        $formSubmission->setAppointments($appointments);
        $formSubmission->setSubscribers($subscribers);
        $formSubmission->setCapitals($capitals);
        $formSubmission->setDocuments($documents);

        return $formSubmission;
    }

}
