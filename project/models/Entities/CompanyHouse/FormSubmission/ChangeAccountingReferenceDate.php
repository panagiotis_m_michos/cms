<?php

namespace Entities\CompanyHouse\FormSubmission;

use Doctrine\ORM\Mapping as ORM;
use Entities\CompanyHouse\FormSubmission;
use DateTime;

/**
 * @Orm\Table(name="ch_change_accounting_reference_date")
 * @Orm\Entity(repositoryClass = "Repositories\CompanyHouse\FormSubmission\ChangeAccountingReferenceDateRepository")
 */
class ChangeAccountingReferenceDate extends FormSubmission
{

    /**
     * @var DateTime
     * @Orm\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var boolean
     * @Orm\Column(name="fiveYearExtensionDetails", type="boolean", nullable=false)
     */
    private $fiveYearExtensionDetails;

    /**
     * @var string
     * @Orm\Column(name="extensionReason", type="string", length=20, nullable=false)
     */
    private $extensionReason;

    /**
     * @var string
     * @Orm\Column(name="extensionAuthorisedCode", type="string", length=20, nullable=false)
     */
    private $extensionAuthorisedCode;

    /**
     * @return DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getFiveYearExtensionDetails()
    {
        return $this->fiveYearExtensionDetails;
    }

    /**
     * @param string $fiveYearExtensionDetails
     */
    public function setFiveYearExtensionDetails($fiveYearExtensionDetails)
    {
        $this->fiveYearExtensionDetails = $fiveYearExtensionDetails;
    }

    /**
     * @return string
     */
    public function getExtensionReason()
    {
        return $this->extensionReason;
    }

    /**
     * @param string $extensionReason
     */
    public function setExtensionReason($extensionReason)
    {
        $this->extensionReason = $extensionReason;
    }

    /**
     * @return string
     */
    public function getExtensionAuthorisedCode()
    {
        return $this->extensionAuthorisedCode;
    }

    /**
     * @param string $extensionAuthorisedCode
     */
    public function setExtensionAuthorisedCode($extensionAuthorisedCode)
    {
        $this->extensionAuthorisedCode = $extensionAuthorisedCode;
    }

}
