<?php

namespace Entities\CompanyHouse\FormSubmission;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Entities\Company;
use Entities\CompanyHouse\FormSubmission\AnnualReturn\Shareholding;
use Entities\CompanyHouse\FormSubmission\AnnualReturn\Officer;
use Entities\CompanyHouse\FormSubmission\AnnualReturn\Capital;
use Entities\CompanyHouse\FormSubmission;
use Utils\Date;

/**
 * @Orm\Table(name="ch_annual_return")
 * @Orm\Entity(repositoryClass = "Repositories\CompanyHouse\FormSubmission\AnnualReturnRepository")
 */
class AnnualReturn extends FormSubmission
{

    /**
     * @var string
     * @Orm\Column(name="company_category", type="string", nullable=true)
     */
    private $companyCategory;

    /**
     * @var boolean
     * @Orm\Column(name="trading", type="boolean", nullable=true)
     */
    private $trading;

    /**
     * @var boolean
     * @Orm\Column(name="dtr5", type="boolean", nullable=false)
     */
    private $dtr5;

    /**
     * @var Date
     * @Orm\Column(name="made_up_date", type="date", nullable=true)
     */
    private $madeUpDate;

    /**
     * @var string
     * @Orm\Column(name="sic_code_type", type="string", nullable=true)
     */
    private $sicCodeType;

    /**
     * @var string
     * @Orm\Column(name="sic_code", type="string", length=250, nullable=true)
     */
    private $sicCode;

    /**
     * @var string
     * @Orm\Column(name="premise", type="string", length=50, nullable=true)
     */
    private $premise;

    /**
     * @var string
     * @Orm\Column(name="street", type="string", length=50, nullable=true)
     */
    private $street;

    /**
     * @var string
     * @Orm\Column(name="thoroughfare", type="string", length=50, nullable=true)
     */
    private $thoroughfare;

    /**
     * @var string
     * @Orm\Column(name="post_town", type="string", length=50, nullable=true)
     */
    private $postTown;

    /**
     * @var string
     * @Orm\Column(name="county", type="string", length=50, nullable=true)
     */
    private $county;

    /**
     * @var string
     * @Orm\Column(name="country", type="string", length=50, nullable=true)
     */
    private $country;

    /**
     * @var string
     * @Orm\Column(name="postcode", type="string", length=15, nullable=true)
     */
    private $postcode;

    /**
     * @var string
     * @Orm\Column(name="care_of_name", type="string", length=100, nullable=true)
     */
    private $careOfName;

    /**
     * @var string
     * @Orm\Column(name="po_box", type="string", length=10, nullable=true)
     */
    private $poBox;

    /**
     * @var string
     * @Orm\Column(name="sail_premise", type="string", length=50, nullable=true)
     */
    private $sailPremise;

    /**
     * @var string
     * @Orm\Column(name="sail_street", type="string", length=50, nullable=true)
     */
    private $sailStreet;

    /**
     * @var string
     * @Orm\Column(name="sail_thoroughfare", type="string", length=50, nullable=true)
     */
    private $sailThoroughfare;

    /**
     * @var string
     * @Orm\Column(name="sail_post_town", type="string", length=50, nullable=true)
     */
    private $sailPostTown;

    /**
     * @var string
     * @Orm\Column(name="sail_county", type="string", length=50, nullable=true)
     */
    private $sailCounty;

    /**
     * @var string
     * @Orm\Column(name="sail_country", type="string", length=50, nullable=true)
     */
    private $sailCountry;

    /**
     * @var string
     * @Orm\Column(name="sail_postcode", type="string", length=15, nullable=true)
     */
    private $sailPostcode;

    /**
     * @var string
     * @Orm\Column(name="sail_care_of_name", type="string", length=100, nullable=true)
     */
    private $sailCareOfName;

    /**
     * @var string
     * @Orm\Column(name="sail_po_box", type="string", length=10, nullable=true)
     */
    private $sailPoBox;

    /**
     * @var string
     * @Orm\Column(name="sail_records", type="string", length=250, nullable=true)
     */
    private $sailRecords;

    /**
     * @var string
     * @Orm\Column(name="members_list", type="string", nullable=true)
     */
    private $membersList;

    /**
     * @var boolean
     * @Orm\Column(name="regulated_markets", type="boolean", nullable=true)
     */
    private $regulatedMarkets;

    /**
     * @var Capital[]|ArrayCollection
     * @Orm\OneToMany(targetEntity = "Entities\CompanyHouse\FormSubmission\AnnualReturn\Capital", mappedBy = "annualReturn", cascade={"persist"}, orphanRemoval=true)
     */
    private $capitals;

    /**
     * @var Officer[]|ArrayCollection
     * @Orm\OneToMany(targetEntity = "Entities\CompanyHouse\FormSubmission\AnnualReturn\Officer", mappedBy = "annualReturn", cascade={"persist"}, orphanRemoval=true)
     */
    private $officers;

    /**
     * @var Shareholding[]|ArrayCollection
     * @Orm\OneToMany(targetEntity = "Entities\CompanyHouse\FormSubmission\AnnualReturn\Shareholding", mappedBy = "annualReturn", cascade={"persist"}, orphanRemoval=true)
     */
    private $shareholdings;

    /**
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        parent::__construct($company);

        $this->capitals = new ArrayCollection();
        $this->officers = new ArrayCollection();
        $this->shareholdings = new ArrayCollection();
    }

    /**
     * @return int|string
     */
    public function getAnnualReturnId()
    {
        return $this->getFormSubmissionId();
    }

    /**
     * @param Capital $capital
     */
    public function addCapital(Capital $capital)
    {
        $this->capitals[] = $capital;
        $capital->setAnnualReturn($this);
    }

    /**
     * @param Officer $officer
     */
    public function addOfficer(Officer $officer)
    {
        $this->officers[] = $officer;
        $officer->setAnnualReturn($this);
    }

    /**
     * @param Shareholding $shareholding
     */
    public function addShareholding(Shareholding $shareholding)
    {
        $this->shareholdings[] = $shareholding;
        $shareholding->setAnnualReturn($this);
    }

    /**
     * @return string
     */
    public function getCompanyCategory()
    {
        return $this->companyCategory;
    }

    /**
     * @param string $companyCategory
     */
    public function setCompanyCategory($companyCategory)
    {
        $this->companyCategory = $companyCategory;
    }

    /**
     * @return boolean
     */
    public function getTrading()
    {
        return $this->trading;
    }

    /**
     * @param boolean $trading
     */
    public function setTrading($trading)
    {
        $this->trading = $trading;
    }

    /**
     * @return boolean
     */
    public function getDtr5()
    {
        return $this->dtr5;
    }

    /**
     * @param boolean $dtr5
     */
    public function setDtr5($dtr5)
    {
        $this->dtr5 = $dtr5;
    }

    /**
     * @return Date
     */
    public function getMadeUpDate()
    {
        return $this->madeUpDate;
    }

    /**
     * @param Date $madeUpDate
     */
    public function setMadeUpDate(Date $madeUpDate)
    {
        $this->madeUpDate = $madeUpDate;
    }

    /**
     * @return string
     */
    public function getSicCodeType()
    {
        return $this->sicCodeType;
    }

    /**
     * @param string $sicCodeType
     */
    public function setSicCodeType($sicCodeType)
    {
        $this->sicCodeType = $sicCodeType;
    }

    /**
     * @return string
     */
    public function getSicCode()
    {
        return $this->sicCode;
    }

    /**
     * @param string $sicCode
     */
    public function setSicCode($sicCode)
    {
        $this->sicCode = $sicCode;
    }

    /**
     * @return string
     */
    public function getPremise()
    {
        return $this->premise;
    }

    /**
     * @param string $premise
     */
    public function setPremise($premise)
    {
        $this->premise = $premise;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getThoroughfare()
    {
        return $this->thoroughfare;
    }

    /**
     * @param string $thoroughfare
     */
    public function setThoroughfare($thoroughfare)
    {
        $this->thoroughfare = $thoroughfare;
    }

    /**
     * @return string
     */
    public function getPostTown()
    {
        return $this->postTown;
    }

    /**
     * @param string $postTown
     */
    public function setPostTown($postTown)
    {
        $this->postTown = $postTown;
    }

    /**
     * @return string
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * @param string $county
     */
    public function setCounty($county)
    {
        $this->county = $county;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @param string $postcode
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    }

    /**
     * @return string
     */
    public function getCareOfName()
    {
        return $this->careOfName;
    }

    /**
     * @param string $careOfName
     */
    public function setCareOfName($careOfName)
    {
        $this->careOfName = $careOfName;
    }

    /**
     * @return string
     */
    public function getPoBox()
    {
        return $this->poBox;
    }

    /**
     * @param string $poBox
     */
    public function setPoBox($poBox)
    {
        $this->poBox = $poBox;
    }

    /**
     * @return string
     */
    public function getSailPremise()
    {
        return $this->sailPremise;
    }

    /**
     * @param string $sailPremise
     */
    public function setSailPremise($sailPremise)
    {
        $this->sailPremise = $sailPremise;
    }

    /**
     * @return string
     */
    public function getSailStreet()
    {
        return $this->sailStreet;
    }

    /**
     * @param string $sailStreet
     */
    public function setSailStreet($sailStreet)
    {
        $this->sailStreet = $sailStreet;
    }

    /**
     * @return string
     */
    public function getSailThoroughfare()
    {
        return $this->sailThoroughfare;
    }

    /**
     * @param string $sailThoroughfare
     */
    public function setSailThoroughfare($sailThoroughfare)
    {
        $this->sailThoroughfare = $sailThoroughfare;
    }

    /**
     * @return string
     */
    public function getSailPostTown()
    {
        return $this->sailPostTown;
    }

    /**
     * @param string $sailPostTown
     */
    public function setSailPostTown($sailPostTown)
    {
        $this->sailPostTown = $sailPostTown;
    }

    /**
     * @return string
     */
    public function getSailCounty()
    {
        return $this->sailCounty;
    }

    /**
     * @param string $sailCounty
     */
    public function setSailCounty($sailCounty)
    {
        $this->sailCounty = $sailCounty;
    }

    /**
     * @return string
     */
    public function getSailCountry()
    {
        return $this->sailCountry;
    }

    /**
     * @param string $sailCountry
     */
    public function setSailCountry($sailCountry)
    {
        $this->sailCountry = $sailCountry;
    }

    /**
     * @return string
     */
    public function getSailPostcode()
    {
        return $this->sailPostcode;
    }

    /**
     * @param string $sailPostcode
     */
    public function setSailPostcode($sailPostcode)
    {
        $this->sailPostcode = $sailPostcode;
    }

    /**
     * @return string
     */
    public function getSailCareOfName()
    {
        return $this->sailCareOfName;
    }

    /**
     * @param string $sailCareOfName
     */
    public function setSailCareOfName($sailCareOfName)
    {
        $this->sailCareOfName = $sailCareOfName;
    }

    /**
     * @return string
     */
    public function getSailPoBox()
    {
        return $this->sailPoBox;
    }

    /**
     * @param string $sailPoBox
     */
    public function setSailPoBox($sailPoBox)
    {
        $this->sailPoBox = $sailPoBox;
    }

    /**
     * @return string
     */
    public function getSailRecords()
    {
        return $this->sailRecords;
    }

    /**
     * @param string $sailRecords
     */
    public function setSailRecords($sailRecords)
    {
        $this->sailRecords = $sailRecords;
    }

    /**
     * @return string
     */
    public function getMembersList()
    {
        return $this->membersList;
    }

    /**
     * @param string $membersList
     */
    public function setMembersList($membersList)
    {
        $this->membersList = $membersList;
    }

    /**
     * @return boolean
     */
    public function getRegulatedMarkets()
    {
        return $this->regulatedMarkets;
    }

    /**
     * @param boolean $regulatedMarkets
     */
    public function setRegulatedMarkets($regulatedMarkets)
    {
        $this->regulatedMarkets = $regulatedMarkets;
    }

    /**
     * @return Capital[]|ArrayCollection
     */
    public function getCapitals()
    {
        return $this->capitals;
    }

    /**
     * @param ArrayCollection $capitals
     */
    public function setCapitals(ArrayCollection $capitals)
    {
        $this->capitals = $capitals;
    }

    /**
     * @return Officer[]|ArrayCollection
     */
    public function getOfficers()
    {
        return $this->officers;
    }

    /**
     * @param ArrayCollection $officers
     */
    public function setOfficers(ArrayCollection $officers)
    {
        $this->officers = $officers;
    }

    /**
     * @return Shareholding[]|ArrayCollection
     */
    public function getShareholdings()
    {
        return $this->shareholdings;
    }

    /**
     * @param ArrayCollection $shareholdings
     */
    public function setShareholdings(ArrayCollection $shareholdings)
    {
        $this->shareholdings = $shareholdings;
    }

    public function __clone()
    {
        if ($this->getFormSubmissionId()) {
            $this->cloneCapitals();
            $this->cloneOfficers();
            $this->cloneShareholdings();
        }

        parent::__clone();
    }

    private function cloneCapitals()
    {
        $capitals = $this->getCapitals();
        $this->capitals = new ArrayCollection();

        foreach ($capitals as $capital) {
            $capitalClone = clone $capital;
            $this->capitals->add($capitalClone);
            $capitalClone->setAnnualReturn($this);
        }
    }

    private function cloneOfficers()
    {
        $officers = $this->getOfficers();
        $this->officers = new ArrayCollection();

        foreach ($officers as $officer) {
            $officerClone = clone $officer;
            $this->officers->add($officerClone);
            $officerClone->setAnnualReturn($this);
        }
    }

    private function cloneShareholdings()
    {
        $shareholdings = $this->getShareholdings();
        $this->shareholdings = new ArrayCollection();

        foreach ($shareholdings as $shareholding) {
            $shareholdingClone = clone $shareholding;
            $this->shareholdings->add($shareholdingClone);
            $shareholdingClone->setAnnualReturn($this);
        }
    }
}
