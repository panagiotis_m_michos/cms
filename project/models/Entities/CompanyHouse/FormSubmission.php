<?php

namespace Entities\CompanyHouse;

use Doctrine\ORM\Mapping as Orm;
use Gedmo\Mapping\Annotation as Gedmo;
use Entities\EntityAbstract;
use Entities\Company;
use DateTime;

/**
 * @Orm\Entity(repositoryClass = "Repositories\CompanyHouse\FormSubmissionRepository")
 * @Orm\Table(name="ch_form_submission")
 * @Orm\InheritanceType("JOINED")
 * @Orm\DiscriminatorColumn(name="form_identifier", type="string")
 * @Orm\DiscriminatorMap({
 *   "FormSubmission" = "FormSubmission",
 *   "CompanyIncorporation" = "Entities\CompanyHouse\FormSubmission\CompanyIncorporation",
 *   "ChangeRegisteredOfficeAddress" = "Entities\CompanyHouse\FormSubmission\ChangeRegisteredOfficeAddress",
 *   "OfficerResignation" = "Entities\CompanyHouse\FormSubmission\OfficerResignation",
 *   "OfficerAppointment" = "Entities\CompanyHouse\FormSubmission\OfficerAppointment",
 *   "OfficerChangeDetails" = "Entities\CompanyHouse\FormSubmission\OfficerChangeDetails",
 *   "ReturnOfAllotmentShares" = "Entities\CompanyHouse\FormSubmission\ReturnOfAllotmentShares",
 *   "ChangeOfName" = "Entities\CompanyHouse\FormSubmission\ChangeOfName",
 *   "ChangeAccountingReferenceDate" = "Entities\CompanyHouse\FormSubmission\ChangeAccountingReferenceDate",
 *   "AnnualReturn" = "Entities\CompanyHouse\FormSubmission\AnnualReturn"
 * })
 * @Orm\HasLifecycleCallbacks
 */
class FormSubmission extends EntityAbstract
{

    const TYPE_COMPANY_INCORPORATION = 'CompanyIncorporation';
    const TYPE_ANNUAL_RETURN = 'AnnualReturn';
    const TYPE_CHANGE_REGISTERED_OFFICE_ADDRESS = 'ChangeRegisteredOfficeAddress';
    const TYPE_OFFICER_RESIGNATION = 'OfficerResignation';
    const TYPE_OFFICER_APPOINTMENT = 'OfficerAppointment';
    const TYPE_OFFICER_CHANGE_DETAILS = 'OfficerChangeDetails';
    const TYPE_RETURN_OF_ALLOTMENT_SHARES = 'ReturnOfAllotmentShares';
    const TYPE_CHANGE_OF_NAME = 'ChangeOfName';
    const TYPE_CHANGE_ACCOUNTING_REFERENCE_DATE = 'ChangeAccountingReferenceDate';
    const LANGUAGE_EN = 'EN';
    const LANGUAGE_CY = 'CY';

    const RESPONSE_ERROR = 'ERROR';
    const RESPONSE_ACCEPT = 'ACCEPT';
    const RESPONSE_REJECT = 'REJECT';
    const RESPONSE_PENDING = 'PENDING';
    const RESPONSE_PARKED = 'PARKED';
    const RESPONSE_INTERNAL_FAILURE = 'INTERNAL_FAILURE';

    /**
     * @var array
     */
    public static $responses = [
        self::RESPONSE_ERROR => 'Error',
        self::RESPONSE_ACCEPT => 'Accept',
        self::RESPONSE_REJECT => 'Reject',
        self::RESPONSE_PENDING => 'Pending',
        self::RESPONSE_PARKED => 'Parked',
        self::RESPONSE_INTERNAL_FAILURE => 'Internal Failure',
    ];

    /**
     * @var integer
     * @Orm\Column(name="form_submission_id", type="integer", nullable=false)
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $formSubmissionId;

    /**
     * @var string
     */
    private $formIdentifier;

    /**
     * @var string
     * @Orm\Column(name="language", type="string", length=14, nullable=false)
     */
    private $language = self::LANGUAGE_EN;

    /**
     * @var string
     * @Orm\Column(name="response", type="string", length=14, nullable=true)
     */
    private $response = NULL;

    /**
     * @var string
     */
    private $rejectReference = NULL;

    /**
     * @var string
     * @Orm\Column(name="examiner_telephone", type="string", length=50, nullable=true)
     */
    private $examinerTelephone = NULL;

    /**
     * @var string
     * @Orm\Column(name="examiner_comment", type="string", length=14, nullable=true)
     */
    private $examinerComment = NULL;

    /**
     * @var DateTime
     * @Orm\Column(name="date_cancelled", type="datetime")
     */
    private $dateCancelled;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create", on="update")
     */
    private $dtm;

    /**
     * @var Company
     * @Orm\ManyToOne(targetEntity="Entities\Company", inversedBy = "formSubmissions", cascade={"persist"})
     * @Orm\JoinColumn(name="company_id", referencedColumnName="company_id")
     */
    private $company;

    /**
     * @var array<FormSubmission>
     * @Orm\OneToMany(targetEntity = "FormSubmissionError", mappedBy = "formSubmission")
     * @Orm\JoinColumn(name="form_submission_error_id", referencedColumnName="form_submission_error_id")
     */
    private $formSubmissionErrors;

    /**
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        $this->setCompany($company);
        $this->setFormSubmissionType();
    }

    /**
     * @return int|string
     */
    public function getId()
    {
        return $this->formSubmissionId;
    }

    /**
     * @return integer
     */
    public function getFormSubmissionId()
    {
        return $this->formSubmissionId;
    }

    /**
     * @return string
     */
    public function getFormIdentifier()
    {
        return $this->formIdentifier;
    }

    /**
     * @param string $formIdentifier
     */
    public function setFormIdentifier($formIdentifier)
    {
        $this->formIdentifier = $formIdentifier;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return string
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param string $response
     */
    public function setResponse($response)
    {
        $this->response = $response;
    }

    /**
     * @return bool
     */
    public function hasResponse()
    {
        return (bool) $this->response;
    }

    /**
     * @return string
     */
    public function getRejectReference()
    {
        return $this->rejectReference;
    }

    /**
     * @param string $rejectReference
     */
    public function setRejectReference($rejectReference)
    {
        $this->rejectReference = $rejectReference;
    }

    /**
     * @return string
     */
    public function getExaminerTelephone()
    {
        return $this->examinerTelephone;
    }

    /**
     * @param string $examinerTelephone
     */
    public function setExaminerTelephone($examinerTelephone)
    {
        $this->examinerTelephone = $examinerTelephone;
    }

    /**
     * @return string
     */
    public function getExaminerComment()
    {
        return $this->examinerComment;
    }

    /**
     * @param string $examinerComment
     */
    public function setExaminerComment($examinerComment)
    {
        $this->examinerComment = $examinerComment;
    }

    /**
     * @return DateTime
     */
    public function getDateCancelled()
    {
        return $this->dateCancelled;
    }

    /**
     * @param DateTime $dateCancelled
     */
    public function setDateCancelled(DateTime $dateCancelled)
    {
        $this->dateCancelled = $dateCancelled;
    }

    /**
     * @return DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @param DateTime $dtc
     */
    public function setDtc(DateTime $dtc)
    {
        $this->dtc = $dtc;
    }

    /**
     * @return DateTime
     */
    public function getDtm()
    {
        return $this->dtm;
    }

    /**
     * @param DateTime $dtm
     */
    public function setDtm(DateTime $dtm)
    {
        $this->dtm = $dtm;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Company $company
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @return array<FormSubmissionError>
     */
    public function getFormSubmissionErrors()
    {
        return $this->formSubmissionErrors;
    }

    /**
     * @param array $formSubmissionErrors
     */
    public function setFormSubmissionErrors(array $formSubmissionErrors)
    {
        $this->formSubmissionErrors = $formSubmissionErrors;
    }

    /**
     * @param FormSubmissionError $formSubmission
     */
    public function addFormSubmissionError(FormSubmissionError $formSubmission)
    {
        $formSubmission->setFormSubmission($this);
        $this->formSubmissionErrors[] = $formSubmission;
    }

    /**
     * @return bool
     */
    public function isAccepted()
    {
        return $this->getResponse() == self::RESPONSE_ACCEPT;
    }

    /**
     * @return bool
     */
    public function isPending()
    {
        return $this->getResponse() == self::RESPONSE_PENDING;
    }

    /**
     * @return bool
     */
    public function isInternalFailure()
    {
        return $this->getResponse() == self::RESPONSE_INTERNAL_FAILURE;
    }

    /**
     * @return bool
     */
    public function isError()
    {
        return $this->getResponse() == self::RESPONSE_ERROR;
    }

    /**
     * @return bool
     */
    public function isCancelled()
    {
        return $this->getDateCancelled() != NULL;
    }

    public function markAsCancelled()
    {
        $this->setDateCancelled(new DateTime);
    }

    public function unsetResponse()
    {
        $this->response = NULL;
    }

    public function __clone()
    {
        if ($this->formSubmissionId) {
            $this->formSubmissionId = NULL;
            $this->dtc = NULL;
            $this->dtm = NULL;
        }
    }

    /**
     * @Orm\PostLoad
     */
    public function setFormSubmissionType()
    {
        $this->setFormIdentifier($this->getReflection()->getShortName());
    }
}
