<?php

namespace Entities\CompanyHouse\Helper;

abstract class BaseOfficer
{

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $foreName;

    /**
     * More than one name can be entered separated by spaces
     *
     * @var string
     */
    private $otherForenames;

    /**
     * @var string
     */
    private $surname;

    /**
     * @param string $foreName
     * @param string $surname
     */
    public function __construct($foreName, $surname)
    {
        $this->setForeName($foreName);
        $this->setSurname($surname);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getForeName()
    {
        return $this->foreName;
    }

    /**
     * @param string $foreName
     */
    public function setForeName($foreName)
    {
        $this->foreName = $foreName;
    }

    /**
     * More than one name can be entered separated by spaces
     *
     * @return string
     */
    public function getOtherForenames()
    {
        return $this->otherForenames;
    }

    /**
     * More than one name can be entered separated by spaces
     *
     * @param string $otherForenames
     */
    public function setOtherForenames($otherForenames)
    {
        $this->otherForenames = $otherForenames;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

}
