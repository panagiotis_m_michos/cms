<?php

namespace Entities\CompanyHouse\Helper;

class Share
{

    /**
     * @var string
     */
    private $shareClass;

    /**
     * @var string
     */
    private $numShares;

    /**
     * @var string
     */
    private $amountPaid;

    /**
     * @var string
     */
    private $amountUnpaid;

    /**
     * @var string
     */
    private $currency;

    /**
     * @var string
     */
    private $shareValue;

    /**
     * @param string $currency
     * @param string $shareClass
     * @param string $numShares
     * @param string $shareValue
     */
    public function __construct($currency, $shareClass, $numShares, $shareValue)
    {
        $this->setCurrency($currency);
        $this->setShareClass($shareClass);
        $this->setNumShares($numShares);
        $this->setShareValue($shareValue);
        $this->setAmountPaid($this->shareValue * $this->numShares);
    }

    /**
     * @return string
     */
    public function getShareClass()
    {
        return $this->shareClass;
    }

    /**
     * @param string $shareClass
     */
    public function setShareClass($shareClass)
    {
        $this->shareClass = $shareClass;
    }

    /**
     * @return string
     */
    public function getNumShares()
    {
        return $this->numShares;
    }

    /**
     * @param string $numShares
     */
    public function setNumShares($numShares)
    {
        $this->numShares = $numShares;
    }

    /**
     * @return string
     */
    public function getAmountPaid()
    {
        return $this->amountPaid;
    }

    /**
     * @param string $amountPaid
     */
    public function setAmountPaid($amountPaid)
    {
        $this->amountPaid = $amountPaid;
    }

    /**
     * @return string
     */
    public function getAmountUnpaid()
    {
        return $this->amountUnpaid;
    }

    /**
     * @param string $amountUnpaid
     */
    public function setAmountUnpaid($amountUnpaid)
    {
        $this->amountUnpaid = $amountUnpaid;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getShareValue()
    {
        return $this->shareValue;
    }

    /**
     * @param string $shareValue
     */
    public function setShareValue($shareValue)
    {
        $this->shareValue = $shareValue;
    }

}
