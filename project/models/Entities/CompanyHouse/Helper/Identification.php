<?php

namespace Entities\CompanyHouse\Helper;

abstract class Identification
{

    const IDENTIFICATION_TYPE_EEA = 'EEA';
    const IDENTIFICATION_TYPE_NON_EEA = 'NonEEA';

    /**
     * @var string
     */
    protected $identificationType;

    /**
     * @var string
     */
    private $placeRegistered;

    /**
     * @var string
     */
    private $registrationNumber;

    /**
     * @var string
     */
    private $lawGoverned;

    /**
     * @var string
     */
    private $legalForm;

    /**
     * @return string
     */
    public function getIdentificationType()
    {
        return $this->identificationType;
    }

    /**
     * @param string $identificationType
     */
    public function setIdentificationType($identificationType)
    {
        $this->identificationType = $identificationType;
    }

    /**
     * @return string
     */
    public function getPlaceRegistered()
    {
        return $this->placeRegistered;
    }

    /**
     * @param string $placeRegistered
     */
    public function setPlaceRegistered($placeRegistered)
    {
        $this->placeRegistered = $placeRegistered;
    }

    /**
     * @return string
     */
    public function getRegistrationNumber()
    {
        return $this->registrationNumber;
    }

    /**
     * @param string $registrationNumber
     */
    public function setRegistrationNumber($registrationNumber)
    {
        $this->registrationNumber = $registrationNumber;
    }

    /**
     * @return string
     */
    public function getLawGoverned()
    {
        return $this->lawGoverned;
    }

    /**
     * @param string $lawGoverned
     */
    public function setLawGoverned($lawGoverned)
    {
        $this->lawGoverned = $lawGoverned;
    }

    /**
     * @return string
     */
    public function getLegalForm()
    {
        return $this->legalForm;
    }

    /**
     * @param string $legalForm
     */
    public function setLegalForm($legalForm)
    {
        $this->legalForm = $legalForm;
    }

}
