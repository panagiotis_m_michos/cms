<?php

namespace Entities\CompanyHouse\Helper;

use DateTime;

class PersonDirector extends PersonOfficer
{

    /**
     * @var DateTime
     */
    private $dob;

    /**
     * @var string
     */
    private $nationality;

    /**
     * @var string
     */
    private $occupation;

    /**
     * @var string
     */
    private $countryOfResidence;

    /**
     * @var Address
     */
    private $residentialAddress;

    /**
     * @param string $foreName
     * @param string $surname
     * @param DateTime $dob
     * @param string $nationality
     * @param string $occupation
     * @param string $countryOfResidence
     * @param Address $address
     * @param Address $residentialAddress
     */
    public function __construct(
        $foreName,
        $surname,
        DateTime $dob,
        $nationality,
        $occupation,
        $countryOfResidence,
        Address $address,
        Address $residentialAddress
    )
    {
        parent::__construct($foreName, $surname, $address);

        $this->setDob($dob);
        $this->setNationality($nationality);
        $this->setOccupation($occupation);
        $this->setCountryOfResidence($countryOfResidence);
        $this->setResidentialAddress($residentialAddress);
    }

    /**
     * @return Address $residentialAddress
     */
    public function getResidentialAddress()
    {
        return $this->residentialAddress;
    }

    /**
     * @param Address $residentialAddress
     */
    public function setResidentialAddress(Address $residentialAddress)
    {
        $this->residentialAddress = $residentialAddress;
    }

    /**
     * @return string
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * @param DateTime $dob
     */
    public function setDob(DateTime $dob)
    {
        $this->dob = $dob;
    }

    /**
     * @return string
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * @param string $nationality
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;
    }

    /**
     * @return string
     */
    public function getOccupation()
    {
        return $this->occupation;
    }

    /**
     * @param string $occupation
     */
    public function setOccupation($occupation)
    {
        $this->occupation = $occupation;
    }

    /**
     * @return string
     */
    public function getCountryOfResidence()
    {
        return $this->countryOfResidence;
    }

    /**
     * @param string $countryOfResidence
     */
    public function setCountryOfResidence($countryOfResidence)
    {
        $this->countryOfResidence = $countryOfResidence;
    }

}
