<?php

namespace Entities\CompanyHouse\Helper;

class Authentication
{
    const PERSONAL_ATTRIBUTE_TYPE_EYE = 'EYE';
    const PERSONAL_ATTRIBUTE_TYPE_BIRTOWN = 'BIRTOWN';
    const PERSONAL_ATTRIBUTE_TYPE_TEL = 'TEL';

    /**
     * @var string
     */
    private $birthTown;

    /**
     * @var string
     */
    private $telephone;

    /**
     * @var string
     */
    private $eyeColour;

    /**
     * @param string $birthTown
     * @param string $telephone
     * @param string $eyeColour
     */
    public function __construct($birthTown, $telephone, $eyeColour)
    {
        $this->birthTown = $birthTown;
        $this->telephone = $telephone;
        $this->eyeColour = $eyeColour;
    }

    /**
     * @return string
     */
    public function getBirthTown()
    {
        return $this->birthTown;
    }

    /**
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @return string
     */
    public function getEyeColour()
    {
        return $this->eyeColour;
    }
}
