<?php

namespace Entities\CompanyHouse\Helper;

use Entities\CompanyHouse\Helper\Identification;
use Entities\CompanyHouse\Helper\Address;

class CorporateOfficer extends BaseOfficer
{

    /**
     * @var string
     */
    private $corporateName;

    /**
     * @var Identification
     */
    private $identification;

    /**
     * @var Address
     */
    private $address;

    /**
     * @param string $foreName
     * @param string $surname
     * @param string $corporateName
     * @param Address $address
     * @param Identification $identification
     */
    public function __construct(
        $foreName,
        $surname,
        $corporateName,
        Address $address,
        Identification $identification = NULL
    )
    {
        parent::__construct($foreName, $surname);
        $this->setCorporateName($corporateName);
        $this->setIdentification($identification);
        $this->setAddress($address);
    }

    /**
     * @return Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param Address $address
     */
    public function setAddress(Address $address)
    {
        $this->address = $address;
    }

    /**
     * @return Identification
     */
    public function getIdentification()
    {
        return $this->identification;
    }

    /**
     * can be NULL for subscribers
     *
     * @param Identification $identification
     */
    public function setIdentification($identification)
    {
        $this->identification = $identification;
    }

    /**
     * @return string
     */
    public function getCorporateName()
    {
        return $this->corporateName;
    }

    /**
     * @param string $corporateName
     */
    public function setCorporateName($corporateName)
    {
        $this->corporateName = $corporateName;
    }

}
