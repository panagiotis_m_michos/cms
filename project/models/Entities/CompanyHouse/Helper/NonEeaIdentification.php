<?php

namespace Entities\CompanyHouse\Helper;

class NonEeaIdentification extends Identification
{

    /**
     * @var string
     */
    protected $identificationType = self::IDENTIFICATION_TYPE_NON_EEA;

    /**
     * @param string $placeRegistered
     * @param string $registrationNumber
     * @param string $lawGoverned
     * @param string $legalForm
     */
    public function __construct($placeRegistered, $registrationNumber, $lawGoverned, $legalForm)
    {
        $this->setPlaceRegistered($placeRegistered);
        $this->setRegistrationNumber($registrationNumber);
        $this->setLawGoverned($lawGoverned);
        $this->setLegalForm($legalForm);
    }

}
