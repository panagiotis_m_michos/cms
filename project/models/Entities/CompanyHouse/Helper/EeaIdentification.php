<?php

namespace Entities\CompanyHouse\Helper;

class EeaIdentification extends Identification
{

    /**
     * @var string
     */
    protected $identificationType = self::IDENTIFICATION_TYPE_EEA;

    /**
     * @param string $placeRegistered
     * @param string $registrationNumber
     */
    public function __construct($placeRegistered, $registrationNumber)
    {
        $this->setPlaceRegistered($placeRegistered);
        $this->setRegistrationNumber($registrationNumber);
    }

}
