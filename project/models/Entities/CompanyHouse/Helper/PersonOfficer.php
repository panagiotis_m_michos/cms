<?php

namespace Entities\CompanyHouse\Helper;

class PersonOfficer extends BaseOfficer
{

    /**
     * @var string
     */
    private $address;

    /**
     * @param string $foreName
     * @param string $surname
     * @param Address $address
     */
    public function __construct($foreName, $surname, Address $address)
    {
        parent::__construct($foreName, $surname, $address);

        $this->setAddress($address);
    }

    /**
     * @return Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param Address $address
     */
    public function setAddress(Address $address)
    {
        $this->address = $address;
    }

}
