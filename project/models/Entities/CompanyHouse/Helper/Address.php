<?php

namespace Entities\CompanyHouse\Helper;

class Address
{

    /**
     * @var string
     */
    private $premise;

    /**
     * @var string
     */
    private $street;

    /**
     * @var string
     */
    private $thoroughfare;

    /**
     * @var string
     */
    private $postTown;

    /**
     * @var string
     */
    private $county;

    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $postcode;

    /**
     * @var string
     */
    private $careOfName;

    /**
     * @var string
     */
    private $poBox;

    /**
     * @var string
     */
    private $secureAddressInd;

    /**
     * @param string $premise
     * @param string $street
     * @param string $postTown
     * @param string $postcode
     * @param $country
     */
    public function __construct($premise, $street, $postTown, $postcode, $country)
    {
        $this->setPremise($premise);
        $this->setStreet($street);
        $this->setPostTown($postTown);
        $this->setPostcode($postcode);
        $this->setCountry($country);
    }

    /**
     * @return string
     */
    public function getPremise()
    {
        return $this->premise;
    }

    /**
     * @param string $premise
     */
    public function setPremise($premise)
    {
        $this->premise = $premise;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getThoroughfare()
    {
        return $this->thoroughfare;
    }

    /**
     * @param string $thoroughfare
     */
    public function setThoroughfare($thoroughfare)
    {
        $this->thoroughfare = $thoroughfare;
    }

    /**
     * @return string
     */
    public function getPostTown()
    {
        return $this->postTown;
    }

    /**
     * @param string $postTown
     */
    public function setPostTown($postTown)
    {
        $this->postTown = $postTown;
    }

    /**
     * @return string
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * @param string $county
     */
    public function setCounty($county)
    {
        $this->county = $county;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @param string $postcode
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    }

    /**
     * @return string
     */
    public function getCareOfName()
    {
        return $this->careOfName;
    }

    /**
     * @param string $careOfName
     */
    public function setCareOfName($careOfName)
    {
        $this->careOfName = $careOfName;
    }

    /**
     * @return string
     */
    public function getPoBox()
    {
        return $this->poBox;
    }

    /**
     * @param string $poBox
     */
    public function setPoBox($poBox)
    {
        $this->poBox = $poBox;
    }

    /**
     * @return string
     */
    public function getSecureAddressInd()
    {
        return $this->secureAddressInd;
    }

    /**
     * @param $secureAddressInd
     */
    public function setSecureAddressInd($secureAddressInd)
    {
        $this->secureAddressInd = $secureAddressInd;
    }

}
