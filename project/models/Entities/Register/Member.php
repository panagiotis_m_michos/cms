<?php

namespace Entities\Register;

use Doctrine\Common\Collections\ArrayCollection;
use Entities\EntityAbstract;
use Doctrine\ORM\Mapping as Orm;
use Gedmo\Mapping\Annotation as Gedmo;
use DateTime;
use Entities\Company;
use Utils\Date;

/**
 * @Orm\Entity(repositoryClass = "Repositories\Register\MemberRepository")
 * @Orm\Table(name="cms2_register_members")
 */
class Member extends EntityAbstract
{
    /**
     * @var integer
     *
     * @Orm\Column(type="integer")
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $registerMemberId;

    /**
     * @var string
     * @Orm\Column(type="string")
     */
    private $name;

    /**
     * @var Date
     * @Orm\Column(type="date", nullable=true)
     */
    private $dateEntry;

    /**
     * @var Date
     * @Orm\Column(type="date", nullable=true)
     */
    private $dateCeased;

    /**
     * @var string
     * @Orm\Column(type="text")
     */
    private $address;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create", on="update")
     */
    private $dtm;

    /**
     * @var ShareClass[]
     *
     * @Orm\OneToMany(targetEntity="ShareClass", mappedBy="member", cascade={"persist"}, orphanRemoval=true)
     */
    private $shareClasses;

    /**
     * @var Company
     *
     * @Orm\ManyToOne(targetEntity="Entities\Company", inversedBy="registerMembers", cascade={"persist"})
     * @Orm\JoinColumn(name="companyId", referencedColumnName="company_id", nullable=false)
     */
    private $company;

    public function __construct(Company $company)
    {
        $this->company = $company;
        $this->shareClasses = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->getRegisterMemberId();
    }

    /**
     * @return int
     */
    public function getRegisterMemberId()
    {
        return $this->registerMemberId;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param Date $dateEntry
     */
    public function setDateEntry(Date $dateEntry = NULL)
    {
        $this->dateEntry = $dateEntry;
    }

    /**
     * @return Date
     */
    public function getDateEntry()
    {
        return $this->dateEntry;
    }

    /**
     * @param Date $dateCeased
     */
    public function setDateCeased(Date $dateCeased = NULL)
    {
        $this->dateCeased = $dateCeased;
    }

    /**
     * @return \Utils\Date
     */
    public function getDateCeased()
    {
        return $this->dateCeased;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return \DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @return \DateTime
     */
    public function getDtm()
    {
        return $this->dtm;
    }

    /**
     * @param ShareClass $shareClass
     */
    public function addShareClass(ShareClass $shareClass)
    {
        $shareClass->setMember($this);
        $this->shareClasses[] = $shareClass;
    }

    /**
     * @return ShareClass[]
     */
    public function getShareClasses()
    {
        return $this->shareClasses;
    }

    /**
     * @param Company $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return bool
     */
    public function isNew()
    {
        return ($this->getId() === NULL);
    }



}
