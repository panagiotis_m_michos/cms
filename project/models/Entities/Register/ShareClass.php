<?php

namespace Entities\Register;

use Doctrine\Common\Collections\ArrayCollection;
use Entities\EntityAbstract;
use Doctrine\ORM\Mapping as Orm;
use Gedmo\Mapping\Annotation as Gedmo;
use DateTime;

/**
 * @Orm\Entity(repositoryClass = "Repositories\Register\ShareClassRepository")
 * @Orm\Table(name="cms2_register_share_classes")
 */
class ShareClass extends EntityAbstract
{
    /**
     * @var integer
     *
     * @Orm\Column(type="integer")
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $registerShareClassId;

    /**
     * @var string
     * @Orm\Column(type="string")
     */
    private $classType;

    /**
     * @var string
     * @Orm\Column(type="string")
     */
    private $currencyIso;

    /**
     * @var float
     * @Orm\Column(type="decimal", scale=2)
     */
    private $price;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create", on="update")
     */
    private $dtm;

    /**
     * @var Member
     *
     * @Orm\ManyToOne(targetEntity="Member", inversedBy="shareClasses")
     * @Orm\JoinColumn(name="registerMemberId", referencedColumnName="registerMemberId")
     */
    private $member;

    /**
     * @var ShareClassEvent[]
     *
     * @Orm\OneToMany(targetEntity="ShareClassEvent", mappedBy="shareClass", cascade={"persist"}, orphanRemoval=true)
     */
    private $shareClassEvents;

    /**
     * @param Member $member
     */
    public function __construct(Member $member)
    {
        $this->member = $member;
        $this->shareClassEvents = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->getRegisterShareClassId();
    }

    /**
     * @return int
     */
    public function getRegisterShareClassId()
    {
        return $this->registerShareClassId;
    }

    /**
     * @param string $classType
     */
    public function setClassType($classType)
    {
        $this->classType = $classType;
    }

    /**
     * @return string
     */
    public function getClassType()
    {
        return $this->classType;
    }

    /**
     * @param string $currency
     */
    public function setCurrencyIso($currency)
    {
        $this->currencyIso = $currency;
    }

    /**
     * @return string
     */
    public function getCurrencyIso()
    {
        return $this->currencyIso;
    }

    /**
     * @param string $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return \DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @return \DateTime
     */
    public function getDtm()
    {
        return $this->dtm;
    }

    /**
     * @param Member $member
     */
    public function setMember($member)
    {
        $this->member = $member;
    }

    /**
     * @return Member
     */
    public function getMember()
    {
        return $this->member;
    }

    /**
     * @return ArrayCollection|ShareClassEvent[]
     */
    public function getShareClassEvents()
    {
        return $this->shareClassEvents;
    }

    /**
     * @param ShareClassEvent $shareClassEvent
     */
    public function addShareClassEvent(ShareClassEvent $shareClassEvent)
    {
        $shareClassEvent->setShareClass($this);
        $this->shareClassEvents[] = $shareClassEvent;
    }

    /**
     * @return bool
     */
    public function isNew()
    {
        return ($this->getId() === NULL);
    }

}
