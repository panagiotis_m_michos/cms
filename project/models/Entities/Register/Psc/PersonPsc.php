<?php

namespace Entities\Register\Psc;

use DateTime;
use Doctrine\ORM\Mapping as Orm;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @Orm\Entity
 */
class PersonPsc extends Psc
{
    /**
     * @var string
     */
    protected $entryType = self::TYPE_PERSON_PSC;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $title;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $forename;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $middleName;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $surname;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime", nullable=true)
     */
    private $dob;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $nationality;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $countryOfResidence;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $residentialPremise;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $residentialStreet;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $residentialThoroughfare;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $residentialPostTown;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $residentialCounty;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $residentialCountry;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $residentialPostcode;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $residentialSecureAddressInd;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getForename()
    {
        return $this->forename;
    }

    /**
     * @param string $forename
     */
    public function setForename($forename)
    {
        $this->forename = $forename;
    }

    /**
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * @param string $middleName
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return DateTime
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * @param DateTime $dob
     */
    public function setDob($dob)
    {
        $this->dob = $dob;
    }

    /**
     * @return string
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * @param string $nationality
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;
    }

    /**
     * @return string
     */
    public function getCountryOfResidence()
    {
        return $this->countryOfResidence;
    }

    /**
     * @param string $countryOfResidence
     */
    public function setCountryOfResidence($countryOfResidence)
    {
        $this->countryOfResidence = $countryOfResidence;
    }

    /**
     * @return string
     */
    public function getResidentialPremise()
    {
        return $this->residentialPremise;
    }

    /**
     * @param string $residentialPremise
     */
    public function setResidentialPremise($residentialPremise)
    {
        $this->residentialPremise = $residentialPremise;
    }

    /**
     * @return string
     */
    public function getResidentialStreet()
    {
        return $this->residentialStreet;
    }

    /**
     * @param string $residentialStreet
     */
    public function setResidentialStreet($residentialStreet)
    {
        $this->residentialStreet = $residentialStreet;
    }

    /**
     * @return string
     */
    public function getResidentialThoroughfare()
    {
        return $this->residentialThoroughfare;
    }

    /**
     * @param string $residentialThoroughfare
     */
    public function setResidentialThoroughfare($residentialThoroughfare)
    {
        $this->residentialThoroughfare = $residentialThoroughfare;
    }

    /**
     * @return string
     */
    public function getResidentialPostTown()
    {
        return $this->residentialPostTown;
    }

    /**
     * @param string $residentialPostTown
     */
    public function setResidentialPostTown($residentialPostTown)
    {
        $this->residentialPostTown = $residentialPostTown;
    }

    /**
     * @return string
     */
    public function getResidentialCounty()
    {
        return $this->residentialCounty;
    }

    /**
     * @param string $residentialCounty
     */
    public function setResidentialCounty($residentialCounty)
    {
        $this->residentialCounty = $residentialCounty;
    }

    /**
     * @return string
     */
    public function getResidentialCountry()
    {
        return $this->residentialCountry;
    }

    /**
     * @param string $residentialCountry
     */
    public function setResidentialCountry($residentialCountry)
    {
        $this->residentialCountry = $residentialCountry;
    }

    /**
     * @return string
     */
    public function getResidentialPostcode()
    {
        return $this->residentialPostcode;
    }

    /**
     * @param string $residentialPostcode
     */
    public function setResidentialPostcode($residentialPostcode)
    {
        $this->residentialPostcode = $residentialPostcode;
    }

    /**
     * @return string
     */
    public function getResidentialSecureAddressInd()
    {
        return $this->residentialSecureAddressInd;
    }

    /**
     * @param string $residentialSecureAddressInd
     */
    public function setResidentialSecureAddressInd($residentialSecureAddressInd)
    {
        $this->residentialSecureAddressInd = $residentialSecureAddressInd;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return implode(' ', array_filter([$this->title, $this->forename, $this->middleName, $this->surname]));
    }
}
