<?php

namespace Entities\Register\Psc;

use Doctrine\ORM\Mapping as Orm;
use Gedmo\Mapping\Annotation as Gedmo;
use Entities\EntityAbstract;
use Entities\Company;
use DateTime;

/**
 * @Orm\Table(name="cms2_register_pscs")
 * @Orm\Entity(repositoryClass="Repositories\Register\Psc\EntryRepository")
 * @Orm\InheritanceType("SINGLE_TABLE")
 * @Orm\DiscriminatorColumn(name="entryType", type="string")
 * @Orm\DiscriminatorMap({
 *   "PERSON_PSC"="Entities\Register\Psc\PersonPsc",
 *   "CORPORATE_PSC"="Entities\Register\Psc\CorporatePsc",
 *   "LEGAL_PERSON_PSC"="Entities\Register\Psc\LegalPersonPsc",
 *   "COMPANY_STATEMENT"="Entities\Register\Psc\CompanyStatement"
 * })
 * @Orm\HasLifecycleCallbacks
 */
class Entry extends EntityAbstract
{
    const TYPE_PERSON_PSC = 'PERSON_PSC';
    const TYPE_CORPORATE_PSC = 'CORPORATE_PSC';
    const TYPE_LEGAL_PERSON_PSC = 'LEGAL_PERSON_PSC';
    const TYPE_COMPANY_STATEMENT = 'COMPANY_STATEMENT';

    /**
     * @var string
     */
    protected $entryType = 'ENTRY';

    /**
     * @var int
     * @Orm\Column(name="entryId", type="integer", nullable=false)
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create", on="update")
     */
    private $dtm;

    /**
     * @var Company
     * @Orm\ManyToOne(targetEntity="Entities\Company", inversedBy="registerPscEntries", cascade={"persist"})
     * @Orm\JoinColumn(name="companyId", referencedColumnName="company_id")
     */
    private $company;

    /**
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        $this->setCompany($company);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEntryType()
    {
        return $this->entryType;
    }

    /**
     * @param string $entryType
     */
    public function setEntryType($entryType)
    {
        $this->entryType = $entryType;
    }

    /**
     * @return DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @return DateTime
     */
    public function getDtm()
    {
        return $this->dtm;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Company $company
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @return bool
     */
    public function isPsc()
    {
        return $this instanceof Psc;
    }

    /**
     * @return bool
     */
    public function isPerson()
    {
        return $this->entryType == self::TYPE_PERSON_PSC;
    }

    /**
     * @return bool
     */
    public function isCorporate()
    {
        return $this->entryType == self::TYPE_CORPORATE_PSC;
    }

    /**
     * @return bool
     */
    public function isLegalPerson()
    {
        return $this->entryType == self::TYPE_LEGAL_PERSON_PSC;
    }

    /**
     * @return bool
     */
    public function isCompanyStatement()
    {
        return $this->entryType == self::TYPE_COMPANY_STATEMENT;
    }
}
