<?php

namespace Entities\Register\Psc;

use Doctrine\ORM\Mapping as Orm;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @Orm\Entity
 */
class LegalPersonPsc extends Psc
{
    /**
     * @var string
     */
    protected $entryType = self::TYPE_LEGAL_PERSON_PSC;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $legalPersonName;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $lawGoverned;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $legalForm;

    /**
     * @return string
     */
    public function getLegalPersonName()
    {
        return $this->legalPersonName;
    }

    /**
     * @param string $legalPersonName
     */
    public function setLegalPersonName($legalPersonName)
    {
        $this->legalPersonName = $legalPersonName;
    }

    /**
     * @return string
     */
    public function getLawGoverned()
    {
        return $this->lawGoverned;
    }

    /**
     * @param string $lawGoverned
     */
    public function setLawGoverned($lawGoverned)
    {
        $this->lawGoverned = $lawGoverned;
    }

    /**
     * @return string
     */
    public function getLegalForm()
    {
        return $this->legalForm;
    }

    /**
     * @param string $legalForm
     */
    public function setLegalForm($legalForm)
    {
        $this->legalForm = $legalForm;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->getLegalPersonName();
    }
}
