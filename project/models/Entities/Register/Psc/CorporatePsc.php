<?php

namespace Entities\Register\Psc;

use Doctrine\ORM\Mapping as Orm;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @Orm\Entity
 */
class CorporatePsc extends Psc
{
    /**
     * @var string
     */
    protected $entryType = self::TYPE_CORPORATE_PSC;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $corporateName;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $placeRegistered;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $registrationNumber;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $lawGoverned;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $legalForm;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $countryOrState;

    /**
     * @return string
     */
    public function getCorporateName()
    {
        return $this->corporateName;
    }

    /**
     * @param string $corporateName
     */
    public function setCorporateName($corporateName)
    {
        $this->corporateName = $corporateName;
    }

    /**
     * @return string
     */
    public function getPlaceRegistered()
    {
        return $this->placeRegistered;
    }

    /**
     * @param string $placeRegistered
     */
    public function setPlaceRegistered($placeRegistered)
    {
        $this->placeRegistered = $placeRegistered;
    }

    /**
     * @return string
     */
    public function getRegistrationNumber()
    {
        return $this->registrationNumber;
    }

    /**
     * @param string $registrationNumber
     */
    public function setRegistrationNumber($registrationNumber)
    {
        $this->registrationNumber = $registrationNumber;
    }

    /**
     * @return string
     */
    public function getLawGoverned()
    {
        return $this->lawGoverned;
    }

    /**
     * @param string $lawGoverned
     */
    public function setLawGoverned($lawGoverned)
    {
        $this->lawGoverned = $lawGoverned;
    }

    /**
     * @return string
     */
    public function getLegalForm()
    {
        return $this->legalForm;
    }

    /**
     * @param string $legalForm
     */
    public function setLegalForm($legalForm)
    {
        $this->legalForm = $legalForm;
    }

    /**
     * @return string
     */
    public function getCountryOrState()
    {
        return $this->countryOrState;
    }

    /**
     * @param string $countryOrState
     */
    public function setCountryOrState($countryOrState)
    {
        $this->countryOrState = $countryOrState;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->getCorporateName();
    }
}
