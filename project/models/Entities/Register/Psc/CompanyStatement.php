<?php

namespace Entities\Register\Psc;

use Doctrine\ORM\Mapping as Orm;
use Gedmo\Mapping\Annotation as Gedmo;
use Entities\EntityAbstract;
use Entities\Company;
use DateTime;
use PeopleWithSignificantControl\Interfaces\IPscAware;

/**
 * @Orm\Entity
 */
class CompanyStatement extends Entry
{
    /**
     * @var string
     */
    protected $entryType = self::TYPE_COMPANY_STATEMENT;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $companyStatement;

    /**
     * @var DateTime
     * @Orm\Column(name="dateCancelled", type="datetime", nullable=true)
     */
    private $dateWithdrew;

    /**
     * @param Company $company
     * @param string $companyStatement
     */
    public function __construct(Company $company, $companyStatement)
    {
        parent::__construct($company);
        $this->companyStatement = $companyStatement;
    }

    /**
     * @return string
     */
    public function getCompanyStatement()
    {
        return $this->companyStatement;
    }

    /**
     * @param string $companyStatement
     */
    public function setCompanyStatement($companyStatement)
    {
        $this->companyStatement = $companyStatement;
    }

    /**
     * @return string
     */
    public function getCompanyStatementText()
    {
        $texts = [
            IPscAware::NO_INDIVIDUAL_OR_ENTITY_WITH_SIGNFICANT_CONTROL => 'The company knows or has reasonable cause to believe that there is no registrable person or registrable relevant legal entity in relation to the company.',
            IPscAware::STEPS_TO_FIND_PSC_NOT_YET_COMPLETED => 'The company has not entered the particulars of any registrable person or registrable relevant legal entity and has not yet completed taking reasonable steps to find out if there is anyone who is a registrable person or a registrable relevant legal entity in relation to the company under section 790D of the Act',
        ];

        return isset($texts[$this->getCompanyStatement()]) ? $texts[$this->getCompanyStatement()] : '';
    }

    /**
     * @return DateTime
     */
    public function getDateWithdrew()
    {
        return $this->dateWithdrew;
    }

    /**
     * @param DateTime $dateWithdrew
     */
    public function setDateWithdrew($dateWithdrew)
    {
        $this->dateWithdrew = $dateWithdrew;
    }
}
