<?php

namespace Entities\Register\Psc;

use DateTime;
use Doctrine\ORM\Mapping as Orm;
use Gedmo\Mapping\Annotation as Gedmo;
use NatureOfControl;
use PeopleWithSignificantControl\Interfaces\IPsc;

/**
 * @Orm\MappedSuperclass
 */
abstract class Psc extends Entry implements IRegisterPsc
{
    const PERSON = 'person';
    const CORPORATE = 'corporate';
    const LEGAL_PERSON = 'legal person';

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $pscStatementNotification;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $premise;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $street;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $thoroughfare;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $postTown;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $county;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $country;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $postcode;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $ownershipOfShares;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $ownershipOfVotingRights;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $rightToAppointAndRemoveDirectors;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $significantInfluenceOrControl;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dateEntry;

    /**
     * @var DateTime
     * @Orm\Column(name="dateCancelled", type="datetime", nullable=true)
     */
    private $dateCessated;

    /**
     * @return string
     */
    public function getPscStatementNotification()
    {
        return $this->pscStatementNotification;
    }

    /**
     * @param string $pscStatementNotification
     */
    public function setPscStatementNotification($pscStatementNotification)
    {
        $this->pscStatementNotification = $pscStatementNotification;
    }

    /**
     * @return string
     */
    public function getStatementNotificationText()
    {
        $texts = [
            IPsc::PSC_EXISTS_BUT_NOT_IDENTIFIED => 'The company knows or has reasonable cause to believe there is a registrable person in relation to the company and has been unable to identify the registrable person',
            IPsc::PSC_DETAILS_NOT_CONFIRMED => 'The company has identified a registrable person in relation to the company and all the required particulars of that person have not been confirmed',
            IPsc::PSC_CONTACTED_BUT_NO_RESPONSE => 'The company has given notice under section 790D of the Act; and the addressees of the notice have failed to comply with the notice within the time specified in it',
            IPsc::RESTRICTIONS_NOTICE_ISSUED_TO_PSC => 'The company has issued a restriction notice under paragraph 1 of Schedule 1B of the Companies Act 2006',
        ];

        return isset($texts[$this->getPscStatementNotification()]) ? $texts[$this->getPscStatementNotification()] : '';
    }

    /**
     * @return string
     */
    public function getPremise()
    {
        return $this->premise;
    }

    /**
     * @param string $premise
     */
    public function setPremise($premise)
    {
        $this->premise = $premise;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getThoroughfare()
    {
        return $this->thoroughfare;
    }

    /**
     * @param string $thoroughfare
     */
    public function setThoroughfare($thoroughfare)
    {
        $this->thoroughfare = $thoroughfare;
    }

    /**
     * @return string
     */
    public function getPostTown()
    {
        return $this->postTown;
    }

    /**
     * @param string $postTown
     */
    public function setPostTown($postTown)
    {
        $this->postTown = $postTown;
    }

    /**
     * @return string
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * @param string $county
     */
    public function setCounty($county)
    {
        $this->county = $county;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @param string $postcode
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    }

    /**
     * @return string
     */
    public function getOwnershipOfShares()
    {
        return $this->ownershipOfShares;
    }

    /**
     * @param string $ownershipOfShares
     */
    public function setOwnershipOfShares($ownershipOfShares)
    {
        $this->ownershipOfShares = $ownershipOfShares;
    }

    /**
     * @return string
     */
    public function getOwnershipOfVotingRights()
    {
        return $this->ownershipOfVotingRights;
    }

    /**
     * @param string $ownershipOfVotingRights
     */
    public function setOwnershipOfVotingRights($ownershipOfVotingRights)
    {
        $this->ownershipOfVotingRights = $ownershipOfVotingRights;
    }

    /**
     * @return string
     */
    public function getRightToAppointAndRemoveDirectors()
    {
        return $this->rightToAppointAndRemoveDirectors;
    }

    /**
     * @param string $rightToAppointAndRemoveDirectors
     */
    public function setRightToAppointAndRemoveDirectors($rightToAppointAndRemoveDirectors)
    {
        $this->rightToAppointAndRemoveDirectors = $rightToAppointAndRemoveDirectors;
    }

    /**
     * @return string
     */
    public function getSignificantInfluenceOrControl()
    {
        return $this->significantInfluenceOrControl;
    }

    /**
     * @param string $significantInfluenceOrControl
     */
    public function setSignificantInfluenceOrControl($significantInfluenceOrControl)
    {
        $this->significantInfluenceOrControl = $significantInfluenceOrControl;
    }

    /**
     * @return array
     */
    public function getNatureOfControlsTexts()
    {
        return array_filter(
            [
                $this->getOwnershipOfSharesText(),
                $this->getOwnershipOfVotingRightsText(),
                $this->getRightToAppointAndRemoveDirectorsText(),
                $this->getSignificantInfluenceOrControlText(),
            ]
        );
    }

    /**
     * @return DateTime
     */
    public function getDateEntry()
    {
        return $this->dateEntry;
    }

    /**
     * @param DateTime $dateEntry
     */
    public function setDateEntry($dateEntry)
    {
        $this->dateEntry = $dateEntry;
    }

    /**
     * @return DateTime
     */
    public function getDateCessated()
    {
        return $this->dateCessated;
    }

    /**
     * @param DateTime $dateCessated
     */
    public function setDateCessated($dateCessated)
    {
        $this->dateCessated = $dateCessated;
    }

    /**
     * @return string
     */
    private function getOwnershipOfSharesText()
    {
        $entity = $this->isCorporate() ? self::CORPORATE : ($this->isLegalPerson() ? self::LEGAL_PERSON : self::PERSON);

        $texts = [
            NatureOfControl::OWNERSHIPOFSHARES_25_TO_50_PERCENT => "The {$entity} holds more than 25% but not more than 50% of shares",
            NatureOfControl::OWNERSHIPOFSHARES_50_TO_75_PERCENT => "The {$entity} holds more than 50% but not more than 75% of shares",
            NatureOfControl::OWNERSHIPOFSHARES_75_TO_100_PERCENT => "The {$entity} holds more than 75% of shares",
            NatureOfControl::OWNERSHIPOFSHARES_25_TO_50_PERCENT_AS_FIRM => 'The members of the firm hold more than 25% but not more than 50% of shares',
            NatureOfControl::OWNERSHIPOFSHARES_50_TO_75_PERCENT_AS_FIRM => 'The members of the firm hold hold more than 50% but not more than 75% of shares',
            NatureOfControl::OWNERSHIPOFSHARES_75_TO_100_PERCENT_AS_FIRM => 'The members of the firm hold hold more than 75% of shares',
            NatureOfControl::OWNERSHIPOFSHARES_25_TO_50_PERCENT_AS_TRUST => 'The trustees of a trust hold more than 25% but not more than 50% of shares',
            NatureOfControl::OWNERSHIPOFSHARES_50_TO_75_PERCENT_AS_TRUST => 'The trustees of a trust hold more than 50% but not more than 75% of shares',
            NatureOfControl::OWNERSHIPOFSHARES_75_TO_100_PERCENT_AS_TRUST => 'The trustees of a trust hold more than 75% of shares',
            NatureOfControl::RIGHTTOSHARESURPLUSASSETS_25_TO_50_PERCENT => "The {$entity} holds more than 25% but not more than 50% of surplus assets",
            NatureOfControl::RIGHTTOSHARESURPLUSASSETS_50_TO_75_PERCENT => "The {$entity} holds more than 50% but not more than 75% of surplus assets",
            NatureOfControl::RIGHTTOSHARESURPLUSASSETS_75_TO_100_PERCENT => "The {$entity} holds more than 75% of surplus assets",
            NatureOfControl::RIGHTTOSHARESURPLUSASSETS_25_TO_50_PERCENT_AS_FIRM => 'The members of the firm hold more than 25% but not more than 50% of surplus assets',
            NatureOfControl::RIGHTTOSHARESURPLUSASSETS_50_TO_75_PERCENT_AS_FIRM => 'The members of the firm hold more than 50% but not more than 75% of surplus assets',
            NatureOfControl::RIGHTTOSHARESURPLUSASSETS_75_TO_100_PERCENT_AS_FIRM => 'The members of the firm hold more than 75% of surplus assets',
            NatureOfControl::RIGHTTOSHARESURPLUSASSETS_25_TO_50_PERCENT_AS_TRUST => 'The trustees of a trust hold more than 25% but not more than 50% of surplus assets',
            NatureOfControl::RIGHTTOSHARESURPLUSASSETS_50_TO_75_PERCENT_AS_TRUST => 'The trustees of a trust hold more than 50% but not more than 75% of surplus assets',
            NatureOfControl::RIGHTTOSHARESURPLUSASSETS_75_TO_100_PERCENT_AS_TRUST => 'The trustees of a trust hold more than 75% of surplus assets',
        ];

        return isset($texts[$this->getOwnershipOfShares()]) ? $texts[$this->getOwnershipOfShares()] : '';
    }

    /**
     * @return string
     */
    private function getOwnershipOfVotingRightsText()
    {
        $entity = $this->isCorporate() ? self::CORPORATE : ($this->isLegalPerson() ? self::LEGAL_PERSON : self::PERSON);

        $texts = [
            NatureOfControl::VOTINGRIGHTS_25_TO_50_PERCENT => "The {$entity} holds more than 25% but not more than 50% of voting rights",
            NatureOfControl::VOTINGRIGHTS_50_TO_75_PERCENT => "The {$entity} holds more than 50% but not more than 75% of voting rights",
            NatureOfControl::VOTINGRIGHTS_75_TO_100_PERCENT => "The {$entity} holds more than 75% of voting rights",
            NatureOfControl::VOTINGRIGHTS_25_TO_50_PERCENT_AS_FIRM => 'The members of the firm hold more than 25% but not more than 50% of voting rights',
            NatureOfControl::VOTINGRIGHTS_50_TO_75_PERCENT_AS_FIRM => 'The members of the firm hold hold more than 50% but not more than 75% of voting rights',
            NatureOfControl::VOTINGRIGHTS_75_TO_100_PERCENT_AS_FIRM => 'The members of the firm hold hold more than 75% of voting rights',
            NatureOfControl::VOTINGRIGHTS_25_TO_50_PERCENT_AS_TRUST => 'The trustees of a trust hold more than 25% but not more than 50% of voting rights',
            NatureOfControl::VOTINGRIGHTS_50_TO_75_PERCENT_AS_TRUST => 'The trustees of a trust hold more than 50% but not more than 75% of voting rights',
            NatureOfControl::VOTINGRIGHTS_75_TO_100_PERCENT_AS_TRUST => 'The trustees of a trust hold more than 75% of voting rights',
        ];

        return isset($texts[$this->getOwnershipOfVotingRights()]) ? $texts[$this->getOwnershipOfVotingRights()] : '';
    }

    /**
     * @return string
     */
    private function getRightToAppointAndRemoveDirectorsText()
    {
        $entity = $this->isCorporate() ? self::CORPORATE : ($this->isLegalPerson() ? self::LEGAL_PERSON : self::PERSON);

        $texts = [
            NatureOfControl::RIGHTTOAPPOINTANDREMOVEMEMBERS => "The {$entity} has the right to appoint or remove the majority of the board of directors of the company",
            NatureOfControl::RIGHTTOAPPOINTANDREMOVEMEMBERS_AS_FIRM => "The {$entity} has control over the firm and members of that firm (in their capacity as such) hold the right to appoint of remove the majority of the board of directors of the company",
            NatureOfControl::RIGHTTOAPPOINTANDREMOVEMEMBERS_AS_TRUST => "The {$entity} has control over the truste and the trustees of that trust (in their capacity as such) hold the right to appoint or remove the majority of the board of directors of the company",
            NatureOfControl::RIGHTTOAPPOINTANDREMOVEDIRECTORS => "The {$entity} has the right to appoint or remove the majority of the board of directors of the company",
            NatureOfControl::RIGHTTOAPPOINTANDREMOVEDIRECTORS_AS_FIRM => "The {$entity} has control over the firm and members of that firm (in their capacity as such) hold the right to appoint of remove the majority of the board of directors of the company",
            NatureOfControl::RIGHTTOAPPOINTANDREMOVEDIRECTORS_AS_TRUST => "The {$entity} has control over the truste and the trustees of that trust (in their capacity as such) hold the right to appoint or remove the majority of the board of directors of the company",
        ];

        return isset($texts[$this->getRightToAppointAndRemoveDirectors()]) ? $texts[$this->getRightToAppointAndRemoveDirectors()] : '';
    }

    /**
     * @return string
     */
    private function getSignificantInfluenceOrControlText()
    {
        $entity = $this->isCorporate() ? self::CORPORATE : ($this->isLegalPerson() ? self::LEGAL_PERSON : self::PERSON);

        $texts = [
            NatureOfControl::SIGINFLUENCECONTROL => "The {$entity} has significant influence or control over the company",
            NatureOfControl::SIGINFLUENCECONTROL_AS_FIRM => "The {$entity} has control over the firm and members of that firm (in their capacity as such) have significant influence or control over the company",
            NatureOfControl::SIGINFLUENCECONTROL_AS_TRUST => "The {$entity} has control over the trust and the trustees of that trust (in their capacity as such) have significant influence or control over the company",
        ];

        return isset($texts[$this->getSignificantInfluenceOrControl()]) ? $texts[$this->getSignificantInfluenceOrControl()] : '';
    }
}
