<?php

namespace Entities\Register;

use Entities\EntityAbstract;
use Doctrine\ORM\Mapping as Orm;
use Gedmo\Mapping\Annotation as Gedmo;
use DateTime;

/**
 * @Orm\Entity(repositoryClass = "Repositories\Register\ShareClassEventRepository")
 * @Orm\Table(name="cms2_register_share_class_events")
 */
class ShareClassEvent extends EntityAbstract
{
    const TYPE_ALLOTMENT = 'ALLOTMENT';
    const TYPE_TRANSFER = 'TRANSFER';

    /**
     * @var array
     */
    public static $types = array(
        self::TYPE_ALLOTMENT => 'Allotment',
        self::TYPE_TRANSFER => 'Transfer'
    );

    /**
     * @var integer
     *
     * @Orm\Column(type="integer")
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $shareClassEventId;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     */
    private $dtEntry;

    /**
     * @var string
     * @Orm\Column(type="string", columnDefinition="ENUM('ALLOTMENT', 'TRANSFER')")
     */
    private $typeId;

    /**
     * @var int
     * @Orm\Column(type="integer")
     */
    private $quantity;

    /**
     * @var string
     * @Orm\Column(type="string")
     */
    private $certificateNumber;

    /**
     * @var integer
     * @Orm\Column(type="integer")
     */
    private $acquired;

    /**
     * @var integer
     * @Orm\Column(type="integer")
     */
    private $disposed;

    /**
     * @var integer
     * @Orm\Column(type="integer")
     */
    private $balance;

    /**
     * @var float
     * @Orm\Column(type="decimal", scale=2)
     */
    private $pricePerShare;

    /**
     * @var float
     * @Orm\Column(type="decimal", scale=2)
     */
    private $totalAmount;

    /**
     * @var string
     *
     * @Orm\Column(type="text")
     */
    private $notes;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create", on="update")
     */
    private $dtm;

    /**
     * @var ShareClass
     *
     * @Orm\ManyToOne(targetEntity="ShareClass", inversedBy="entries")
     * @Orm\JoinColumn(name="registerShareClassId", referencedColumnName="registerShareClassId")
     */
    private $shareClass;

    public function __construct(ShareClass $shareClass)
    {
        $this->shareClass = $shareClass;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->getShareClassEventId();
    }

    /**
     * @return int
     */
    public function getShareClassEventId()
    {
        return $this->shareClassEventId;
    }

    /**
     * @param mixed $dtEntry
     */
    public function setDtEntry($dtEntry)
    {
        $this->dtEntry = $dtEntry;
    }

    /**
     * @return mixed
     */
    public function getDtEntry()
    {
        return $this->dtEntry;
    }

    /**
     * @param string $typeId
     */
    public function setTypeId($typeId)
    {
        $this->typeId = $typeId;
    }

    /**
     * @return string
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = (int) $quantity;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $acquired
     */
    public function setAcquired($acquired)
    {
        $this->acquired = (int) $acquired;
    }

    /**
     * @param string $certificateNumber
     */
    public function setCertificateNumber($certificateNumber)
    {
        $this->certificateNumber = $certificateNumber;
    }

    /**
     * @return string
     */
    public function getCertificateNumber()
    {
        return $this->certificateNumber;
    }

    /**
     * @return int
     */
    public function getAcquired()
    {
        return $this->acquired;
    }

    /**
     * @param int $disposed
     */
    public function setDisposed($disposed)
    {
        $this->disposed = (int) $disposed;
    }

    /**
     * @return int
     */
    public function getDisposed()
    {
        return $this->disposed;
    }

    /**
     * @param int $balance
     */
    public function setBalance($balance)
    {
        $this->balance = (int) $balance;
    }

    /**
     * @return int
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param float $pricePerShare
     */
    public function setPricePerShare($pricePerShare)
    {
        $this->pricePerShare = (float) $pricePerShare;
    }

    /**
     * @return float
     */
    public function getPricePerShare()
    {
        return $this->pricePerShare;
    }

    /**
     * @param float $totalAmount
     */
    public function setTotalAmount($totalAmount)
    {
        $this->totalAmount = (float) $totalAmount;
    }

    /**
     * @return float
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    /**
     * @param string $notes
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    }

    /**
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @return \DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @return \DateTime
     */
    public function getDtm()
    {
        return $this->dtm;
    }

    /**
     * @param ShareClass $shareClass
     */
    public function setShareClass($shareClass)
    {
        $this->shareClass = $shareClass;
    }

    /**
     * @return ShareClass
     */
    public function getShareClass()
    {
        return $this->shareClass;
    }

    /**
     * @return bool
     */
    public function isAllotmentType()
    {
        return ($this->getTypeId() == self::TYPE_ALLOTMENT);
    }

    /**
     * @return bool
     */
    public function isTransferType()
    {
        return ($this->getTypeId() == self::TYPE_TRANSFER);
    }

}
