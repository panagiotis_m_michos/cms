<?php

namespace Entities;

use Doctrine\ORM\Mapping as Orm;
use FNode;
use Gedmo\Mapping\Annotation as Gedmo;
use Nette\Object;
use DateTime;
use Nette\Utils\Arrays;
use Utils\Date;
use WholesalePackage;

/**
 * @Orm\Table(name="cms2_cashback")
 * @Orm\Entity(repositoryClass = "Repositories\CashbackRepository")
 */
class Cashback extends Object
{
    // statuses
    const STATUS_IMPORTED = 'IMPORTED';
    const STATUS_ELIGIBLE = 'ELIGIBLE';
    const STATUS_PAID = 'PAID';
    const STATUS_OUTDATED = 'OUTDATED';

    /**
     * @var array
     */
    public static $statuses = array(
        self::STATUS_IMPORTED => 'Imported',
        self::STATUS_ELIGIBLE => 'Eligible',
        self::STATUS_PAID => 'Paid',
        self::STATUS_OUTDATED => 'Outdated',
    );

    // bank types
    const BANK_TYPE_BARCLAYS = 'BARCLAYS';
    const BANK_TYPE_HSBC = 'HSBC';
    const BANK_TYPE_TSB = 'TSB';

    /**
     * @var array
     */
    public static $bankTypes = array(
        self::BANK_TYPE_BARCLAYS => 'Barclays',
        self::BANK_TYPE_HSBC => 'HSBC',
        self::BANK_TYPE_TSB => 'TSB',
    );

    /**
     * @var array
     */
    public static $cashBackBanks = array(
        self::BANK_TYPE_BARCLAYS => 'Barclays',
        self::BANK_TYPE_TSB => 'TSB',
    );

    // package types
    const PACKAGE_TYPE_RETAIL = 'RETAIL';
    const PACKAGE_TYPE_WHOLESALE = 'WHOLESALE';

    /**
     * @var array
     */
    public static $packageTypes = array(
        self::PACKAGE_TYPE_RETAIL => 'Retail',
        self::PACKAGE_TYPE_WHOLESALE => 'Wholesale',
    );

    // cashback types
    const CASHBACK_TYPE_BANK = 'BANK';
    const CASHBACK_TYPE_CREDITS = 'CREDITS';

    /**
     * @var array
     */
    public static $cashbackTypes = array(
        self::CASHBACK_TYPE_BANK => 'Bank',
        self::CASHBACK_TYPE_CREDITS => 'Credits',
    );

    /**
     * @var integer
     * @Orm\Column(type="integer")
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $cashbackId;

    /**
     * @var string
     * @Orm\Column(type="string", columnDefinition="ENUM('IMPORTED', 'ELIGIBLE', 'PAID')")
     */
    private $statusId;

    /**
     * @var string
     * @Orm\Column(type="string")
     */
    private $groupId;

    /**
     * @var Company
     * @Orm\ManyToOne(targetEntity="Company", cascade={"persist"})
     * @Orm\JoinColumn(name="companyId", referencedColumnName="company_id")
     */
    private $company;

    /**
     * @var Customer
     * @Orm\ManyToOne(targetEntity="Customer", inversedBy="cashbacks", cascade={"persist"})
     * @Orm\JoinColumn(name="customerId", referencedColumnName="customerId")
     */
    private $customer;

    /**
     * @var string
     * @Orm\Column(type="string", columnDefinition="ENUM('RETAIL', 'WHOLESALE')")
     */
    private $packageTypeId;

    /**
     * @var string
     * @Orm\Column(type="string", columnDefinition="ENUM('BANK', 'CREDITS')")
     */
    private $cashbackTypeId;

    /**
     * @var string
     * @Orm\Column(type="string")
     */
    private $bankTypeId;

    /**
     * @var string
     * @Orm\Column(type="string")
     */
    private $sortCode;

    /**
     * @var string
     * @Orm\Column(type="string")
     */
    private $accountNumber;

    /**
     * @var integer
     * @Orm\Column(type="integer")
     */
    private $amount;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime", nullable=true)
     */
    private $dateLeadSent;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime", nullable=true)
     */
    private $dateAccountOpened;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     */
    private $datePaid;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create", on="update")
     */
    private $dtm;

    /**
     * @var bool
     * @Orm\Column(type="boolean")
     */
    private $isArchived;

    /**
     * @param Company $company
     * @param string $bankTypeId
     * @param int $amount
     * @param DateTime $dateLeadSent
     * @param DateTime $dateAccountOpened
     */
    public function __construct(
        Company $company,
        $bankTypeId,
        $amount,
        DateTime $dateLeadSent = NULL,
        DateTime $dateAccountOpened = NULL
    )
    {
        $this->company = $company;
        $this->customer = $company->getCustomer();
        $this->bankTypeId = $bankTypeId;
        $this->amount = $amount;
        $this->dateAccountOpened = $dateAccountOpened;
        $this->dateLeadSent = $dateLeadSent;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->getCashbackId();
    }

    /**
     * @return int
     */
    public function getCashbackId()
    {
        return $this->cashbackId;
    }

    /**
     * @return string
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * @param string $statusId
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;
    }

    /**
     * @return string
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * @param string $groupId
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;
    }

    /**
     * @param Company $company
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @return mixed
     */
    public function getPackageTypeId()
    {
        return $this->packageTypeId;
    }

    /**
     * @param mixed $packageTypeId
     */
    public function setPackageTypeId($packageTypeId)
    {
        $this->packageTypeId = $packageTypeId;
    }

    /**
     * @return string
     */
    public function getCashbackTypeId()
    {
        return $this->cashbackTypeId;
    }

    /**
     * @param string $cashbackTypeId
     */
    public function setCashbackTypeId($cashbackTypeId)
    {
        $this->cashbackTypeId = $cashbackTypeId;
    }

    /**
     * @param string $bankTypeId
     */
    public function setBankTypeId($bankTypeId)
    {
        $this->bankTypeId = $bankTypeId;
    }

    /**
     * @return string
     */
    public function getBankTypeId()
    {
        return $this->bankTypeId;
    }

    /**
     * @return string
     */
    public function getSortCode()
    {
        return $this->sortCode;
    }

    /**
     * @param string $sortCode
     */
    public function setSortCode($sortCode)
    {
        $this->sortCode = $sortCode;
    }

    /**
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * @param string $accountNumber
     */
    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = $accountNumber;
    }

    /**
     * @param int $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param DateTime $dateLeadSent
     */
    public function setDateLeadSent(DateTime $dateLeadSent)
    {
        $this->dateLeadSent = $dateLeadSent;
    }

    /**
     * @return Date
     */
    public function getDateLeadSent()
    {
        return $this->dateLeadSent;
    }

    /**
     * @param DateTime $dateAccountOpened
     */
    public function setDateAccountOpened(DateTime $dateAccountOpened)
    {
        $this->dateAccountOpened = $dateAccountOpened;
    }

    /**
     * @return Date
     */
    public function getDateAccountOpened()
    {
        return $this->dateAccountOpened;
    }

    /**
     * @param DateTime $datePaid
     */
    public function setDatePaid(DateTime $datePaid)
    {
        $this->datePaid = $datePaid;
    }

    /**
     * @return DateTime
     */
    public function getDatePaid()
    {
        return $this->datePaid;
    }

    /**
     * @return DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @return DateTime
     */
    public function getDtm()
    {
        return $this->dtm;
    }

    /**
     * @return mixed
     */
    public function getBankType()
    {
        return Arrays::get(self::$bankTypes, $this->getBankTypeId(), NULL);
    }

    /**
     * @return bool
     */
    public function isImported()
    {
        return ($this->getStatusId() === self::STATUS_IMPORTED);
    }

    /**
     * @return bool
     */
    public function isEligible()
    {
        return ($this->getStatusId() === self::STATUS_ELIGIBLE);
    }

    /**
     * @return bool
     */
    public function isPaid()
    {
        return ($this->getStatusId() === self::STATUS_PAID);
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return Arrays::get(self::$statuses, $this->getStatusId(), NULL);
    }

    /**
     * @return mixed
     */
    public function getPackageType()
    {
        return Arrays::get(self::$packageTypes, $this->getPackageTypeId(), NULL);
    }

    /**
     * @return bool
     */
    public function isArchived()
    {
        return $this->isArchived == TRUE;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return !$this->isArchived();
    }

    /**
     * @param bool $isArchived
     */
    public function setIsArchived($isArchived)
    {
        $this->isArchived = $isArchived;
    }

}
