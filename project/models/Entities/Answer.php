<?php

namespace Entities;

use Doctrine\ORM\Mapping as Orm;
use Gedmo\Mapping\Annotation as Gedmo;
use Nette\Object;
use IArrayConvertable;
use DateTime;

/**
 * Answer
 *
 * @Orm\Table(name="cms2_answers")
 * @Orm\Entity(repositoryClass = "Repositories\AnswerRepository")
 */
class Answer extends Object implements IArrayConvertable
{
    const TYPE_NONE = 'none';

    /**
     * @var array
     */
    public static $customerTypes = [
        1 => "I'm based overseas and need a UK company",
        "I'm starting my first business",
        "I'm a contractor / freelancer",
        "I want to reserve the company name. I'm not looking to trade yet.",
        "I already have a Ltd company but need another",
        "I'm an accountant / professional forming companies for others",
        self::TYPE_NONE => "None of the above"
    ];

    /**
     * @var array
     */
    public static $tradingPeriods = [
        'Immediately',
        '1-2 months',
        '3-6 months',
        '6+ months'
    ];

    /**
     * @var integer
     *
     * @Orm\Column(name="answerId", type="integer", nullable=false)
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $answerId;

    /**
     * @var string
     *
     * @Orm\Column(name="text", type="text", nullable=false)
     */
    private $text;

    /**
     * @var string
     *
     * @Orm\Column(name="additionalText", type="text", nullable=true)
     */
    private $additionalText;

    /**
     * @var string
     *
     * @Orm\Column(name="tradingStart", type="text", nullable=true)
     */
    private $tradingStart;

    /**
     * @var Customer
     * @Orm\ManyToOne(targetEntity="Customer", cascade={"persist"})
     * @Orm\JoinColumn(name="customerId", referencedColumnName="customerId")
     */
    private $customer;

    /**
     * @var Company
     * @Orm\ManyToOne(targetEntity="Company", cascade={"persist"})
     * @Orm\JoinColumn(name="companyId", referencedColumnName="company_id")
     */
    private $company;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create", on="update")
     */
    private $dtm;

    /**
     * Get answerId
     *
     * @return integer
     */
    public function getAnswerId()
    {
        return $this->answerId;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Answer
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set additionalText
     *
     * @param string $additionalText
     * @return Answer
     */
    public function setAdditionalText($additionalText)
    {
        $this->additionalText = $additionalText;

        return $this;
    }

    /**
     * Get additionalText
     *
     * @return string
     */
    public function getAdditionalText()
    {
        return $this->additionalText;
    }

    /**
     * @param Customer $customer
     * @return Answer
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set dtc
     *
     * @param DateTime $dtc
     * @return Answer
     */
    public function setDtc(DateTime $dtc)
    {
        $this->dtc = $dtc;

        return $this;
    }

    /**
     * Get dtc
     *
     * @return DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @param mixed $typeId
     */
    public function setTextById($typeId)
    {
        $this->setText($this->getType($typeId));
    }

    /**
     * @param mixed $typeId
     * @return string|NULL
     */
    public function getType($typeId)
    {
        return isset(self::$customerTypes[$typeId]) ? self::$customerTypes[$typeId] : NULL;
    }

    /**
     * @return string
     */
    public function getTradingStart()
    {
        return $this->tradingStart;
    }

    /**
     * @param string $tradingStart
     */
    public function setTradingStart($tradingStart)
    {
        $this->tradingStart = $tradingStart;
    }

    /**
     * @param int $id
     */
    public function setTradingStartById($id)
    {
        $this->setTradingStart(self::$tradingPeriods[$id]);
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Company $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = [];
        $array['customerId'] = $this->getCustomer()->getCustomerId();
        $array['bestDescribes'] = $this->getText();
        $array['tellUsMore'] = $this->getAdditionalText();
        $array['dtc'] = $this->getDtc()->format('d/m/Y');
        return $array;
    }

    /**
     * @return DateTime
     */
    public function getDtm()
    {
        return $this->dtm;
    }

    /**
     * @param DateTime $dtm
     */
    public function setDtm($dtm)
    {
        $this->dtm = $dtm;
    }
}
