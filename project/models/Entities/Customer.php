<?php

namespace Entities;

use CountryMap;
use Customer as OldCustomer;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as Orm;
use Entities\Payment\Token;
use Exception;
use Gedmo\Mapping\Annotation as Gedmo;
use IIdentifier;
use JsonSerializable;
use Nette\Utils\Arrays;
use UserModule\Contracts\IAddress;
use UserModule\Contracts\ICustomer;
use Utils\Date;

/**
 * Customer
 * @Orm\Entity(repositoryClass = "Repositories\CustomerRepository")
 * @Orm\Table(name="cms2_customers")
 * @Gedmo\Loggable(logEntryClass="LoggableModule\Entities\LogEntry")
 */
class Customer extends EntityAbstract implements IIdentifier, ICustomer, IAddress, JsonSerializable
{
    // statuses
    const STATUS_NOT_VALIDATED = OldCustomer::STATUS_NOT_VALIDATED;
    const STATUS_VALIDATED = OldCustomer::STATUS_VALIDATED;
    const STATUS_BLOCKED = OldCustomer::STATUS_BLOCKED;

    // roles
    const ROLE_NORMAL = OldCustomer::ROLE_NORMAL;
    const ROLE_WHOLESALE = OldCustomer::ROLE_WHOLESALE;

    const CASHBACK_ON_BANK_ACCOUNT = 'BANK';
    const CASHBACK_ON_CREDITS = 'CREDITS';
    // tags
    const TAG_ICAEW = OldCustomer::TAG_ICAEW;
    const TAG_GENERALWHOLESALE = OldCustomer::TAG_GENERALWHOLESALE;

    /**
     * @var array
     */
    public static $roles = [
        self::ROLE_NORMAL => 'Normal',
        self::ROLE_WHOLESALE => 'Wholesale',
    ];

    /**
     * @var array
     */
    public static $titles = [
        1 => 'Mr',
        2 => 'Miss',
        3 => 'Mrs',
        4 => 'Ms',
        5 => 'Dr',
        6 => 'Prof',
        7 => 'Master',
        8 => 'Rev',
        9 => 'Sir',
        10 => 'Lord',
        11 => 'Lady',
    ];

    /**
     * @var integer
     *
     * @Orm\Column(name="customerId", type="integer", nullable=false)
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $customerId;

    /**
     * @var integer
     *
     * @Orm\Column(name="statusId", type="integer", nullable=false)
     */
    private $statusId = self::STATUS_NOT_VALIDATED;

    /**
     * @var string
     *
     * @Orm\Column(name="roleId", type="string", length=100, nullable=false)
     */
    private $roleId = self::ROLE_NORMAL;

    /**
     * @var string
     *
     * @Orm\Column(name="tagId", type="string", nullable=true)
     */
    private $tagId;

    /**
     * @var string
     *
     * @Orm\Column(name="icaewId", type="string", length=200, nullable=true)
     */
    private $icaewId;

    /**
     * @var boolean
     *
     * @Orm\Column(name="myDetailsQuestion", type="boolean", nullable=true)
     */
    private $myDetailsQuestion;

    /**
     * @var string
     *
     * @Orm\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @Orm\Column(name="password", type="string", length=255, nullable=false)
     */
    private $password;

    /**
     * @var integer
     *
     * @Orm\Column(name="titleId", type="integer", nullable=true)
     */
    private $titleId;

    /**
     * @var string
     *
     * @Orm\Column(name="firstName", type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @var string
     *
     * @Orm\Column(name="lastName", type="string", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @var string
     *
     * @Orm\Column(type="string")
     */
    private $companyName;

    /**
     * @var string
     *
     * @Orm\Column(name="address1", type="string", length=255, nullable=true)
     */
    private $address1;

    /**
     * @var string
     *
     * @Orm\Column(name="address2", type="string", length=255, nullable=true)
     */
    private $address2;

    /**
     * @var string
     *
     * @Orm\Column(name="address3", type="string", length=255, nullable=true)
     */
    private $address3;

    /**
     * @var string
     *
     * @Orm\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @Orm\Column(name="county", type="string", length=255, nullable=true)
     */
    private $county;

    /**
     * @var string
     *
     * @Orm\Column(name="postcode", type="string", length=20, nullable=true)
     */
    private $postcode;

    /**
     * @var integer
     *
     * @Orm\Column(name="countryId", type="integer", nullable=true)
     */
    private $countryId;

    /**
     * @var string
     *
     * @Orm\Column(name="phone", type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @Orm\Column(name="additionalPhone", type="string", length=14, nullable=true)
     */
    private $additionalPhone;

    /**
     * @var integer
     *
     * @Orm\Column(name="isSubscribed", type="integer", nullable=true)
     */
    private $isSubscribed;

    /**
     * @var integer
     *
     * @Orm\Column(name="howHeardId", type="integer", nullable=true)
     */
    private $howHeardId;

    /**
     * @var integer
     *
     * @Orm\Column(name="industryId", type="integer", nullable=true)
     */
    private $industryId;

    /**
     * @var float
     *
     * @Orm\Column(name="credit", type="float", nullable=true)
     */
    private $credit;

    /**
     * @var integer
     *
     * @Orm\Column(name="affiliateId", type="integer", nullable=true)
     */
    private $affiliateId;

    /**
     * @var boolean
     *
     * @Orm\Column(name="activeCompanies", type="boolean", nullable=true)
     */
    private $activeCompanies;

    /**
     * @var boolean
     *
     * @Orm\Column(name="monitoredActiveCompanies", type="boolean", nullable=true)
     */
    private $monitoredActiveCompanies;

    /**
     * @var string
     *
     * @Orm\Column(name="cashbackType", type="string", nullable=true)
     */
    private $cashbackType = self::CASHBACK_ON_BANK_ACCOUNT;

    /**
     * @var string
     * @Orm\Column(name="accountNumber", type="string", nullable=true)
     */
    private $accountNumber;

    /**
     * @var string
     * @Orm\Column(name="sortCode", type="string", nullable=true)
     */
    private $sortCode;

    /**
     * @var boolean
     * @Orm\Column(type="boolean")
     * @Gedmo\Versioned
     */
    private $isIdCheckRequired = FALSE;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Versioned
     */
    private $dtIdValidated;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create", on="update")
     */
    private $dtm;

    /**
     * @var Cashback[]
     * @Orm\OneToMany(targetEntity="Cashback", mappedBy="customer")
     */
    private $cashbacks;

    /**
     * @var Company[] ArrayCollection
     * @Orm\OneToMany(targetEntity="Company", mappedBy="customer")
     */
    private $companies;

    /**
     * @var Order[] ArrayCollection
     * @Orm\OneToMany(targetEntity="Order", mappedBy="customer")
     */
    private $orders;

    /**
     * @var Token[]|ArrayCollection
     * @Orm\OneToMany(targetEntity="Entities\Payment\Token", mappedBy="customer")
     */
    private $tokens;

    /**
     * @var string
     */
    private $temporaryPassword;

    /**
     * @param string $email
     * @param string $password
     */
    public function __construct($email, $password)
    {
        $this->setEmail($email);
        $this->setPassword($password);

        $this->cashbacks = new ArrayCollection();
        $this->companies = new ArrayCollection();
        $this->tokens = new ArrayCollection();
    }

    /**
     * @param string $email
     * @return Customer
     */
    public static function temporary($email)
    {
        $self = new self($email, uniqid());
        return $self;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->customerId;
    }

    /**
     * @return int
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @return int
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * @return string
     */
    public function getRoleId()
    {
        return $this->roleId;
    }

    /**
     * @return string
     */
    public function getTagId()
    {
        return $this->tagId;
    }

    /**
     * @return string
     */
    public function getIcaewId()
    {
        return $this->icaewId;
    }

    /**
     * @return bool
     */
    public function getMyDetailsQuestion()
    {
        return $this->myDetailsQuestion;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return int
     */
    public function getTitleId()
    {
        return $this->titleId;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return trim($this->getFirstName() . ' ' . $this->getLastName());
    }

    /**
     * @return string
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * @return string
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * @return string
     */
    public function getAddress3()
    {
        return $this->address3;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @return int
     */
    public function getCountryId()
    {
        return $this->countryId;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getAdditionalPhone()
    {
        return $this->additionalPhone;
    }

    /**
     * @return int
     */
    public function getIsSubscribed()
    {
        return $this->isSubscribed;
    }

    /**
     * @return int
     */
    public function getHowHeardId()
    {
        return $this->howHeardId;
    }

    /**
     * @return int
     */
    public function getIndustryId()
    {
        return $this->industryId;
    }

    /**
     * @return float
     */
    public function getCredit()
    {
        return $this->credit;
    }

    /**
     * @return int
     */
    public function getAffiliateId()
    {
        return $this->affiliateId;
    }

    /**
     * @return bool
     */
    public function getActiveCompanies()
    {
        return $this->activeCompanies;
    }

    /**
     * @return bool
     */
    public function getMonitoredActiveCompanies()
    {
        return $this->monitoredActiveCompanies;
    }

    /**
     * @return string
     */
    public function getCashbackType()
    {
        return $this->cashbackType;
    }

    /**
     * @return integer
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * @return integer
     */
    public function getSortCode()
    {
        return $this->sortCode;
    }

    /**
     * @return boolean
     */
    public function isIsIdCheckRequired()
    {
        return $this->isIdCheckRequired;
    }

    /**
     * @param boolean $isIdCheckRequired
     */
    public function setIsIdCheckRequired($isIdCheckRequired)
    {
        $this->isIdCheckRequired = $isIdCheckRequired;
    }

    /**
     * @return DateTime
     */
    public function getDtIdValidated()
    {
        return $this->dtIdValidated;
    }

    /**
     * @param DateTime $dtIdValidated
     */
    public function setDtIdValidated(DateTime $dtIdValidated = NULL)
    {
        $this->dtIdValidated = $dtIdValidated;
    }

    /**
     * @return DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @return DateTime
     */
    public function getDtm()
    {
        return $this->dtm;
    }

    /**
     * @param int $customerId
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }

    /**
     * @param int $statusId
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;
    }

    /**
     * @param string $roleId
     */
    public function setRoleId($roleId)
    {
        $this->roleId = $roleId;
    }

    /**
     * @param string $tagId
     */
    public function setTagId($tagId)
    {
        $this->tagId = $tagId;
    }

    /**
     * @param string $icaewId
     */
    public function setIcaewId($icaewId)
    {
        $this->icaewId = $icaewId;
    }

    /**
     * @param bool $myDetailsQuestion
     */
    public function setMyDetailsQuestion($myDetailsQuestion)
    {
        $this->myDetailsQuestion = $myDetailsQuestion;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param string $password
     * @throws Exception
     */
    public function setPassword($password)
    {
        if (!$password) {
            throw new Exception('Password can not be empty!');
        }
        $this->password = md5($password);
    }

    /**
     * @param int $titleId
     */
    public function setTitleId($titleId)
    {
        $this->titleId = $titleId;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @param string $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     * @param string $address1
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;
    }

    /**
     * @param string $address2
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;
    }

    /**
     * @param string $address3
     */
    public function setAddress3($address3)
    {
        $this->address3 = $address3;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @param string $county
     */
    public function setCounty($county)
    {
        $this->county = $county;
    }

    /**
     * @param string $postcode
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    }

    /**
     * @param int $countryId
     */
    public function setCountryId($countryId)
    {
        $this->countryId = $countryId;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @param string $additionalPhone
     */
    public function setAdditionalPhone($additionalPhone)
    {
        $this->additionalPhone = $additionalPhone;
    }

    /**
     * @param int $isSubscribed
     */
    public function setIsSubscribed($isSubscribed)
    {
        $this->isSubscribed = $isSubscribed;
    }

    /**
     * @param int $howHeardId
     */
    public function setHowHeardId($howHeardId)
    {
        $this->howHeardId = $howHeardId;
    }

    /**
     * @param int $industryId
     */
    public function setIndustryId($industryId)
    {
        $this->industryId = $industryId;
    }

    /**
     * @param float $credit
     */
    public function setCredit($credit)
    {
        $this->credit = $credit;
    }

    /**
     * @param int $affiliateId
     */
    public function setAffiliateId($affiliateId)
    {
        $this->affiliateId = $affiliateId;
    }

    /**
     * @param bool $activeCompanies
     */
    public function setActiveCompanies($activeCompanies)
    {
        $this->activeCompanies = $activeCompanies;
    }

    /**
     * @param bool $monitoredActiveCompanies
     */
    public function setMonitoredActiveCompanies($monitoredActiveCompanies)
    {
        $this->monitoredActiveCompanies = $monitoredActiveCompanies;
    }

    /**
     * @param string $cashbackType
     */
    public function setCashbackType($cashbackType)
    {
        $this->cashbackType = $cashbackType;
    }

    /**
     * @param integer $accountNumber
     */
    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = $accountNumber;
    }

    /**
     * @param integer $sortCode
     */
    public function setSortCode($sortCode)
    {
        $this->sortCode = $sortCode;
    }

    /**
     * @param DateTime $dtc
     */
    public function setDtc(DateTime $dtc)
    {
        $this->dtc = $dtc;
    }

    /**
     * @param DateTime $dtm
     */
    public function setDtm(DateTime $dtm)
    {
        $this->dtm = $dtm;
    }

    /**
     * @return bool
     */
    public function isRetail()
    {
        return $this->getRoleId() === self::ROLE_NORMAL;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = [];
        foreach ($this->getReflection()->getProperties() as $property) {
            if ($property->isStatic()) {
                continue;
            }

            $name = $property->name;
            $array[$name] = $this->$name;
        }
        return $array;
    }

    /**
     * @param Customer $customer
     * @return bool
     */
    public function isEqual(Customer $customer)
    {
        return ($this->getId() === $customer->getId());
    }

    /**
     * @param string $newLine
     * @return string
     */
    public function getAddress($newLine = "\n")
    {
        $address = [];
        $address[] = $this->getAddress1();
        $address[] = $this->getAddress2();
        $address[] = $this->getAddress3();
        $address[] = $this->getCity();
        $address[] = $this->getCounty();
        $address[] = $this->getPostcode();
        $address[] = $this->getCountryId();

        return implode($newLine, $address);
    }

    /**
     * @return bool
     */
    public function hasBankDetails()
    {
        return ($this->getAccountNumber() && $this->getSortCode());
    }

    /**
     * @return bool
     */
    public function hasCompletedRegistration()
    {
        return ($this->firstName && $this->postcode);
    }

    /**
     * @return bool
     */
    public function hasCashbackTypeBank()
    {
        return $this->getCashbackType() == self::CASHBACK_ON_BANK_ACCOUNT;
    }

    /**
     * @return bool
     */
    public function isWholesale()
    {
        return ($this->roleId == self::ROLE_WHOLESALE);
    }

    /**
     * @return Company[]|ArrayCollection
     */
    public function getCompanies()
    {
        return $this->companies;
    }

    /**
     * @return Token[]|ArrayCollection
     */
    public function getTokens()
    {
        return $this->tokens;
    }

    /**
     * @return bool
     */
    public function hasCashbackTypeCredits()
    {
        return $this->getCashbackType() == self::CASHBACK_ON_CREDITS;
    }

    /**
     * @return bool
     */
    public function hasCompanies()
    {
        return !$this->companies->isEmpty();
    }

    /**
     * Only show to users that have a company in their account and not wholesaler
     * @return bool
     */
    public function hasWholesaleQuestion()
    {
        return ($this->hasCompanies() && !$this->isWholesale() && !$this->myDetailsQuestion);
    }

    /**
     * @param Company $company
     */
    public function addCompany(Company $company)
    {
        $company->setCustomer($this);
        $this->companies[] = $company;
    }

    /**
     * @return bool
     */
    public function hasActiveToken()
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('isCurrent', TRUE))
            ->andWhere(Criteria::expr()->eq('sageStatus', Token::STATUS_ACTIVE))
            ->andWhere(Criteria::expr()->gte('cardExpiryDate', new Date));

        return !$this->tokens->matching($criteria)->isEmpty();
    }

    /**
     * @return Token|NULL
     */
    public function getActiveToken()
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('isCurrent', TRUE))
            ->andWhere(Criteria::expr()->eq('sageStatus', Token::STATUS_ACTIVE))
            ->andWhere(Criteria::expr()->gte('cardExpiryDate', new Date));

        return $this->tokens->matching($criteria)->first();
    }

    /**
     * @return bool
     */
    public function isNew()
    {
        return ($this->getId() === NULL);
    }

    /**
     * @param float $credit
     */
    public function addCredit($credit)
    {
        $this->credit += max(0, $credit);
    }

    /**
     * @return NULL|string
     */
    public function getCountry2Letter()
    {
        return CountryMap::mapIntTo2Letter($this->countryId);
    }

    /**
     * @param $country (full name)
     * @return NULL|string
     */
    public function setCountryFromString($country)
    {
        $this->countryId = array_search($country, OldCustomer::$countries) ? : NULL;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return isset(OldCustomer::$countries[$this->getCountryId()]) ? OldCustomer::$countries[$this->getCountryId()] : '';
    }

    public function markIdCheckRequired()
    {
        $this->isIdCheckRequired = TRUE;
    }

    public function markIdCheckNotRequired()
    {
        $this->isIdCheckRequired = FALSE;
    }

    /**
     * @return bool
     */
    public function isIdCheckRequired()
    {
        return !$this->isIdCheckCompleted() && $this->isIdCheckRequired;
    }

    /**
     * @return bool
     */
    public function isIdCheckCompleted()
    {
        return $this->getDtIdValidated() ? TRUE : FALSE;
    }

    /**
     * @return bool
     */
    public function isUkCustomer()
    {
        return $this->getCountryId() === OldCustomer::UK_CITIZEN;
    }

    /**
     * @return Order[]
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return Arrays::get(self::$roles, $this->roleId, NULL);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return Arrays::get(self::$titles, $this->titleId, NULL);
    }

    /**
     * @return IAddress
     */
    public function getPrimaryAddress()
    {
        return $this;
    }

    /**
     * @param float $credit
     * @return bool
     */
    public function hasEnoughCredit($credit)
    {
        return $this->credit >= $credit;
    }

    /**
     * @param float $credit
     */
    public function subtractCredit($credit)
    {
        $this->credit -= $credit;
    }

    /**
     * @return bool
     */
    public function hasCredit()
    {
        return $this->credit > 0;
    }

    /**
     * @param string $country
     */
    public function setCountryIso($country)
    {
        $countryNumeric = CountryMap::map2LetterToInt($country);
        return $this->setCountryId($countryNumeric);
    }

    /**
     * @return string
     */
    public function getCountryIso()
    {
        return $this->getCountry2Letter();
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $addressObject = $this->getPrimaryAddress();
        $address = $addressObject ? [
            'address1' => $addressObject->getAddress1(),
            'address2' => $addressObject->getAddress2(),
            'postcode' => $addressObject->getPostCode(),
            'city' => $addressObject->getCity(),
            'country' => $addressObject->getCountry(),
            'countryIso' => $addressObject->getCountryIso()
        ] : [];

        return [
            'role' => $this->getRole(),
            'fullName' => $this->getFullName(),
            'title' => $this->getTitle(),
            'firstName' => $this->getFirstName(),
            'lastName' => $this->getLastName(),
            'email' => $this->getEmail(),
            'phone' => $this->getPhone(),
            'address' => $address,
            'credit' => $this->getCredit(),
            'receivesNewsLetter' => $this->getIsSubscribed() ? TRUE : FALSE
        ];
    }

    /**
     * @param string $password
     */
    public function setTemporaryPassword($password)
    {
        //@TODO create a story to use temporary passwords instead of resetting the real one
        $this->setPassword($password);
        $this->temporaryPassword = $password;
    }

    /**
     * @return string
     */
    public function getTemporaryPassword()
    {
        return $this->temporaryPassword;
    }
}
