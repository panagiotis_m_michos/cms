<?php

namespace Entities;

use CompanyModule\Entities\CompanySetting;
use Doctrine\ORM\Mapping as Orm;
use Gedmo\Mapping\Annotation as Gedmo;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Entities\CompanyHouse\FormSubmission;
use Entities\Register\Member;
use Doctrine\Common\Collections\Criteria;
use ToolkitOfferModule\Entities\CompanyToolkitOffer;
use ToolkitOfferModule\Entities\ToolkitOffer;

/**
 * Company
 * @Orm\Entity(repositoryClass = "Repositories\CompanyRepository")
 * @Orm\Table(name="ch_company")
 */
class Company extends EntityAbstract
{
    const COMPANY_CATEGORY_BYSHR = 'BYSHR';
    const COMPANY_CATEGORY_BYGUAR = 'BYGUAR';
    const COMPANY_CATEGORY_PLC = 'PLC';
    const COMPANY_CATEGORY_LLP = 'LLP';

    const COMPANY_STATUS_DISSOLVED = 'Dissolved';

    /**
     * @var array
     */
    public static $companyCategories = [
        self::COMPANY_CATEGORY_BYSHR => 'Private company limited by shares',
        self::COMPANY_CATEGORY_BYGUAR => 'Private company limited by guarantee',
        self::COMPANY_CATEGORY_PLC => 'Public limited company',
        self::COMPANY_CATEGORY_LLP => 'Limited Liability Partnership',
    ] ;

    /**
     * @var integer
     *
     * @Orm\Column(name="company_id", type="integer", nullable=false)
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $companyId;

    /**
     * @var integer
     *
     * @Orm\Column(name="product_id", type="integer", nullable=true)
     */
    private $productId;

    /**
     * @var boolean
     *
     * @Orm\Column(name="is_certificate_printed", type="boolean", nullable=false)
     */
    private $isCertificatePrinted = FALSE;

    /**
     * @var boolean
     *
     * @Orm\Column(name="is_bronze_cover_letter_printed", type="boolean", nullable=false)
     */
    private $isBronzeCoverLetterPrinted = FALSE;

    /**
     * @var boolean
     *
     * @Orm\Column(name="is_ma_printed", type="boolean", nullable=false)
     */
    private $isMaPrinted = TRUE;

    /**
     * @var boolean
     *
     * @Orm\Column(name="is_ma_cover_letter_printed", type="boolean", nullable=false)
     */
    private $isMaCoverLetterPrinted = TRUE;

    /**
     * @var string
     *
     * @Orm\Column(name="company_name", type="string", length=160, nullable=false)
     */
    private $companyName;

    /**
     * @var string
     *
     * @Orm\Column(name="company_number", type="string", length=17, nullable=true)
     */
    private $companyNumber;

    /**
     * @var string
     *
     * @Orm\Column(name="authentication_code", type="string", length=6, nullable=true)
     */
    private $authenticationCode;

    /**
     * @var DateTime
     *
     * @Orm\Column(name="incorporation_date", type="date", nullable=true)
     */
    private $incorporationDate;

    /**
     * @var string
     *
     * @Orm\Column(name="company_category", type="string", length=160, nullable=true)
     */
    private $companyCategory;

    /**
     * @var string
     *
     * @Orm\Column(name="jurisdiction", type="string", length=20, nullable=true)
     */
    private $jurisdiction;

    /**
     * @var \DateTime
     *
     * @Orm\Column(name="made_up_date", type="date", nullable=true)
     */
    private $madeUpDate;

    /**
     * @var \DateTime
     *
     * @Orm\Column(name="next_due_date", type="date", nullable=true)
     */
    private $nextDueDate;

    /**
     * @var string
     *
     * @Orm\Column(name="premise", type="string", length=50, nullable=true)
     */
    private $premise;

    /**
     * @var string
     *
     * @Orm\Column(name="street", type="string", length=50, nullable=true)
     */
    private $street;

    /**
     * @var string
     *
     * @Orm\Column(name="thoroughfare", type="string", length=50, nullable=true)
     */
    private $thoroughfare;

    /**
     * @var string
     *
     * @Orm\Column(name="post_town", type="string", length=50, nullable=true)
     */
    private $postTown;

    /**
     * @var string
     *
     * @Orm\Column(name="county", type="string", length=50, nullable=true)
     */
    private $county;

    /**
     * @var string
     *
     * @Orm\Column(name="country", type="string", nullable=true)
     */
    private $country;

    /**
     * @var string
     *
     * @Orm\Column(name="postcode", type="string", length=15, nullable=true)
     */
    private $postcode;

    /**
     * @var string
     *
     * @Orm\Column(name="care_of_name", type="string", length=100, nullable=true)
     */
    private $careOfName;

    /**
     * @var string
     *
     * @Orm\Column(name="po_box", type="string", length=10, nullable=true)
     */
    private $poBox;

    /**
     * @var string
     *
     * @Orm\Column(name="sail_premise", type="string", length=50, nullable=true)
     */
    private $sailPremise;

    /**
     * @var string
     *
     * @Orm\Column(name="sail_street", type="string", length=50, nullable=true)
     */
    private $sailStreet;

    /**
     * @var string
     *
     * @Orm\Column(name="sail_thoroughfare", type="string", length=50, nullable=true)
     */
    private $sailThoroughfare;

    /**
     * @var string
     *
     * @Orm\Column(name="sail_post_town", type="string", length=50, nullable=true)
     */
    private $sailPostTown;

    /**
     * @var string
     *
     * @Orm\Column(name="sail_county", type="string", length=50, nullable=true)
     */
    private $sailCounty;

    /**
     * @var string
     *
     * @Orm\Column(name="sail_country", type="string", nullable=true)
     */
    private $sailCountry;

    /**
     * @var string
     *
     * @Orm\Column(name="sail_postcode", type="string", length=15, nullable=true)
     */
    private $sailPostcode;

    /**
     * @var string
     *
     * @Orm\Column(name="sail_care_of_name", type="string", length=100, nullable=true)
     */
    private $sailCareOfName;

    /**
     * @var string
     *
     * @Orm\Column(name="sail_po_box", type="string", length=10, nullable=true)
     */
    private $sailPoBox;

    /**
     * @var string
     *
     * @Orm\Column(name="sic_code1", type="string", length=6, nullable=true)
     */
    private $sicCode1 = '82990';

    /**
     * @var string
     *
     * @Orm\Column(name="sic_code2", type="string", length=6, nullable=true)
     */
    private $sicCode2;

    /**
     * @var string
     *
     * @Orm\Column(name="sic_code3", type="string", length=6, nullable=true)
     */
    private $sicCode3;

    /**
     * @var string
     *
     * @Orm\Column(name="sic_code4", type="string", length=6, nullable=true)
     */
    private $sicCode4;

    /**
     * @var string
     *
     * @Orm\Column(name="sic_description", type="string", length=255, nullable=true)
     */
    private $sicDescription;

    /**
     * @var string
     *
     * @Orm\Column(name="company_status", type="string", length=255, nullable=true)
     */
    private $companyStatus;

    /**
     * @var string
     *
     * @Orm\Column(name="country_of_origin", type="string", length=20, nullable=true)
     */
    private $countryOfOrigin;

    /**
     * @var string
     *
     * @Orm\Column(name="accounts_ref_date", type="string", length=10, nullable=true)
     */
    private $accountsRefDate;

    /**
     * @var DateTime
     *
     * @Orm\Column(name="accounts_next_due_date", type="date", nullable=true)
     */
    private $accountsNextDueDate;

    /**
     * @var DateTime
     *
     * @Orm\Column(name="accounts_last_made_up_date", type="date", nullable=true)
     */
    private $accountsLastMadeUpDate;

    /**
     * @var DateTime
     *
     * @Orm\Column(name="returns_next_due_date", type="date", nullable=true)
     */
    private $returnsNextDueDate;

    /**
     * @var DateTime
     *
     * @Orm\Column(name="returns_last_made_up_date", type="date", nullable=true)
     */
    private $returnsLastMadeUpDate;

    /**
     * @var integer
     *
     * @Orm\Column(name="dca_id", type="integer", nullable=true)
     */
    private $dcaId;

    /**
     * @var integer
     *
     * @Orm\Column(name="registered_office_id", type="integer", nullable=true)
     */
    private $registeredOfficeId;

    /**
     * @var integer
     *
     * @Orm\Column(name="service_address_id", type="integer", nullable=true)
     */
    private $serviceAddressId;

    /**
     * @var integer
     *
     * @Orm\Column(name="nominee_director_id", type="integer", nullable=true)
     */
    private $nomineeDirectorId;

    /**
     * @var integer
     *
     * @Orm\Column(name="nominee_secretary_id", type="integer", nullable=true)
     */
    private $nomineeSecretaryId;

    /**
     * @var integer
     *
     * @Orm\Column(name="nominee_subscriber_id", type="integer", nullable=true)
     */
    private $nomineeSubscriberId;

    /**
     * @var integer
     *
     * @Orm\Column(name="annual_return_id", type="integer", nullable=true)
     */
    private $annualReturnId;

    /**
     * @var integer
     *
     * @Orm\Column(name="change_name_id", type="integer", nullable=true)
     */
    private $changeNameId;

    /**
     * @var int
     *
     * @Orm\Column(name="ereminder_id", type="boolean", nullable=true)
     */
    private $ereminderId;

    /**
     * @var DateTime
     *
     * @Orm\Column(name="document_date", type="date", nullable=true)
     */
    private $documentDate;

    /**
     * @var string
     *
     * @Orm\Column(name="document_id", type="string", length=255, nullable=true)
     */
    private $documentId;

    /**
     * @var DateTime
     *
     * @Orm\Column(name="accepted_date", type="date", nullable=true)
     */
    private $acceptedDate;

    /**
     * @var integer
     *
     * @Orm\Column(name="cash_back_amount", type="integer")
     */
    private $cashBackAmount = 0;

    /**
     * @var string
     *
     * @Orm\Column(name="no_psc_reason", type="string")
     */
    private $noPscReason;

    /**
     * @var boolean
     *
     * @Orm\Column(name="locked", type="boolean", nullable=false)
     */
    private $locked = FALSE;

    /**
     * @var boolean
     *
     * @Orm\Column(name="hidden", type="boolean", nullable=false)
     */
    private $hidden = FALSE;

    /**
     * @var boolean
     *
     * @Orm\Column(name="deleted", type="boolean", nullable=false)
     */
    private $deleted = FALSE;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create", on="update")
     */
    private $dtm;

    /**
     * @var array<Service>
     * @Orm\OneToMany(targetEntity = "Service", mappedBy = "company", cascade = {"persist"})
     */
    private $services;

    /**
     * @var Customer
     * @Orm\ManyToOne(targetEntity="Customer", cascade={"persist"}, inversedBy="companies")
     * @Orm\JoinColumn(name="customer_id", referencedColumnName="customerId")
     */
    private $customer;

    /**
     * @var Order
     * @Orm\OneToOne(targetEntity="Order", cascade={"persist"})
     * @Orm\JoinColumn(name="order_id", referencedColumnName="orderId")
     */
    private $order;

    /**
     * @var ArrayCollection|FormSubmission[]
     * @Orm\OneToMany(targetEntity = "Entities\CompanyHouse\FormSubmission", mappedBy = "company")
     * @Orm\OrderBy({"formSubmissionId" = "DESC"})
     */
    private $formSubmissions;

    /**
     * @var Member[]
     *
     * @Orm\OneToMany(targetEntity="Entities\Register\Member", mappedBy="company", cascade={"persist"}, orphanRemoval=true)
     */
    private $registerMembers;

    /**
     * @var ArrayCollection|CompanyToolkitOffer[]
     *
     * @ORM\OneToMany(targetEntity="ToolkitOfferModule\Entities\CompanyToolkitOffer", mappedBy="company", cascade={"persist"}, orphanRemoval=true)
     */
    private $toolkitOffers;

    /**
     * @var CompanySetting[]
     * @Orm\OneToMany(targetEntity="CompanyModule\Entities\CompanySetting", mappedBy="company")
     */
    private $settings;

    /**
     * @param Customer $customer
     * @param $companyName
     */
    public function __construct(Customer $customer, $companyName)
    {
        $this->services = new ArrayCollection();
        $this->formSubmissions = new ArrayCollection();
        $this->registerMembers = new ArrayCollection();
        $this->toolkitOffers = new ArrayCollection();
        $this->settings = new ArrayCollection();

        $this->setCustomer($customer);
        $this->setCompanyName($companyName);
    }

    public function getId()
    {
        return $this->companyId;
    }


    /**
     * @return integer
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * @return bool
     */
    public function isIncorporated()
    {
        return !empty($this->companyNumber);
    }

    /**
     * @return integer
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param integer $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * @return boolean
     */
    public function getIsCertificatePrinted()
    {
        return $this->isCertificatePrinted;
    }

    /**
     * @param boolean $isCertificatePrinted
     */
    public function setIsCertificatePrinted($isCertificatePrinted)
    {
        $this->isCertificatePrinted = $isCertificatePrinted;
    }

    /**
     * @return boolean
     */
    public function getIsBronzeCoverLetterPrinted()
    {
        return $this->isBronzeCoverLetterPrinted;
    }

    /**
     * @param boolean $isBronzeCoverLetterPrinted
     */
    public function setIsBronzeCoverLetterPrinted($isBronzeCoverLetterPrinted)
    {
        $this->isBronzeCoverLetterPrinted = $isBronzeCoverLetterPrinted;
    }

    /**
     * @return boolean
     */
    public function getIsMaPrinted()
    {
        return $this->isMaPrinted;
    }

    /**
     * @param boolean $isMaPrinted
     */
    public function setIsMaPrinted($isMaPrinted)
    {
        $this->isMaPrinted = $isMaPrinted;
    }

    /**
     * @return boolean
     */
    public function getIsMaCoverLetterPrinted()
    {
        return $this->isMaCoverLetterPrinted;
    }

    /**
     * @param boolean $isMaCoverLetterPrinted
     */
    public function setIsMaCoverLetterPrinted($isMaCoverLetterPrinted)
    {
        $this->isMaCoverLetterPrinted = $isMaCoverLetterPrinted;
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param string $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     * @return string
     */
    public function getCompanyNumber()
    {
        return $this->companyNumber;
    }

    /**
     * @param string $companyNumber
     */
    public function setCompanyNumber($companyNumber)
    {
        $this->companyNumber = (string) $companyNumber;
    }

    /**
     * @return string
     */
    public function getAuthenticationCode()
    {
        return $this->authenticationCode;
    }

    /**
     * @param string $authenticationCode
     */
    public function setAuthenticationCode($authenticationCode)
    {
        $this->authenticationCode = $authenticationCode;
    }

    /**
     * @return DateTime
     */
    public function getIncorporationDate()
    {
        return $this->incorporationDate;
    }

    /**
     * @param DateTime $incorporationDate
     */
    public function setIncorporationDate(DateTime $incorporationDate)
    {
        $this->incorporationDate = $incorporationDate;
    }

    /**
     * @return string
     */
    public function getCompanyCategory()
    {
        return $this->companyCategory;
    }

    /**
     * @param string $companyCategory
     */
    public function setCompanyCategory($companyCategory)
    {
        $this->companyCategory = $companyCategory;
    }

    /**
     * @return string
     */
    public function getJurisdiction()
    {
        return $this->jurisdiction;
    }

    /**
     * @param string $jurisdiction
     */
    public function setJurisdiction($jurisdiction)
    {
        $this->jurisdiction = $jurisdiction;
    }

    /**
     * @return DateTime
     */
    public function getMadeUpDate()
    {
        return $this->madeUpDate;
    }

    /**
     * @param DateTime $madeUpDate
     */
    public function setMadeUpDate(DateTime $madeUpDate)
    {
        $this->madeUpDate = $madeUpDate;
    }

    /**
     * @return DateTime
     */
    public function getNextDueDate()
    {
        return $this->nextDueDate;
    }

    /**
     * @param DateTime $nextDueDate
     */
    public function setNextDueDate(DateTime $nextDueDate)
    {
        $this->nextDueDate = $nextDueDate;
    }

    /**
     * @return string
     */
    public function getPremise()
    {
        return $this->premise;
    }

    /**
     * @param string $premise
     */
    public function setPremise($premise)
    {
        $this->premise = $premise;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getThoroughfare()
    {
        return $this->thoroughfare;
    }

    /**
     * @param string $thoroughfare
     */
    public function setThoroughfare($thoroughfare)
    {
        $this->thoroughfare = $thoroughfare;
    }

    /**
     * @return string
     */
    public function getPostTown()
    {
        return $this->postTown;
    }

    /**
     * @param string $postTown
     */
    public function setPostTown($postTown)
    {
        $this->postTown = $postTown;
    }

    /**
     * @return string
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * @param string $county
     */
    public function setCounty($county)
    {
        $this->county = $county;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @param string $postcode
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    }

    /**
     * @return string
     */
    public function getCareOfName()
    {
        return $this->careOfName;
    }

    /**
     * @param string $careOfName
     */
    public function setCareOfName($careOfName)
    {
        $this->careOfName = (string) $careOfName;
    }

    /**
     * @return string
     */
    public function getPoBox()
    {
        return $this->poBox;
    }

    /**
     * @param string $poBox
     */
    public function setPoBox($poBox)
    {
        $this->poBox = (string) $poBox;
    }

    /**
     * @return string
     */
    public function getSailPremise()
    {
        return $this->sailPremise;
    }

    /**
     * @param string $sailPremise
     */
    public function setSailPremise($sailPremise)
    {
        $this->sailPremise = $sailPremise;
    }

    /**
     * @return string
     */
    public function getSailStreet()
    {
        return $this->sailStreet;
    }

    /**
     * @param string $sailStreet
     */
    public function setSailStreet($sailStreet)
    {
        $this->sailStreet = $sailStreet;
    }

    /**
     * @return string
     */
    public function getSailThoroughfare()
    {
        return $this->sailThoroughfare;
    }

    /**
     * @param string $sailThoroughfare
     */
    public function setSailThoroughfare($sailThoroughfare)
    {
        $this->sailThoroughfare = $sailThoroughfare;
    }

    /**
     * @return string
     */
    public function getSailPostTown()
    {
        return $this->sailPostTown;
    }

    /**
     * @param string $sailPostTown
     */
    public function setSailPostTown($sailPostTown)
    {
        $this->sailPostTown = $sailPostTown;
    }

    /**
     * @return string
     */
    public function getSailCounty()
    {
        return $this->sailCounty;
    }

    /**
     * @param string $sailCounty
     */
    public function setSailCounty($sailCounty)
    {
        $this->sailCounty = $sailCounty;
    }

    /**
     * @return string
     */
    public function getSailCountry()
    {
        return $this->sailCountry;
    }

    /**
     * @param string $sailCountry
     */
    public function setSailCountry($sailCountry)
    {
        $this->sailCountry = $sailCountry;
    }

    /**
     * @return string
     */
    public function getSailPostcode()
    {
        return $this->sailPostcode;
    }

    /**
     * @param string $sailPostcode
     */
    public function setSailPostcode($sailPostcode)
    {
        $this->sailPostcode = $sailPostcode;
    }

    /**
     * @return string
     */
    public function getSailCareOfName()
    {
        return $this->sailCareOfName;
    }

    /**
     * @param string $sailCareOfName
     */
    public function setSailCareOfName($sailCareOfName)
    {
        $this->sailCareOfName = $sailCareOfName;
    }

    /**
     * @return string
     */
    public function getSailPoBox()
    {
        return $this->sailPoBox;
    }

    /**
     * @param string $sailPoBox
     */
    public function setSailPoBox($sailPoBox)
    {
        $this->sailPoBox = $sailPoBox;
    }

    /**
     * @return string
     */
    public function getSicCode1()
    {
        return $this->sicCode1;
    }

    /**
     * @param string $sicCode1
     */
    public function setSicCode1($sicCode1)
    {
        $this->sicCode1 = $sicCode1;
    }

    /**
     * @return string
     */
    public function getSicCode2()
    {
        return $this->sicCode2;
    }

    /**
     * @param string $sicCode2
     */
    public function setSicCode2($sicCode2)
    {
        $this->sicCode2 = $sicCode2;
    }

    /**
     * @return string
     */
    public function getSicCode3()
    {
        return $this->sicCode3;
    }

    /**
     * @param string $sicCode3
     */
    public function setSicCode3($sicCode3)
    {
        $this->sicCode3 = $sicCode3;
    }

    /**
     * @return string
     */
    public function getSicCode4()
    {
        return $this->sicCode4;
    }

    /**
     * @param string $sicCode4
     */
    public function setSicCode4($sicCode4)
    {
        $this->sicCode4 = $sicCode4;
    }

    /**
     * @return array
     */
    public function getSicCodes()
    {
        return array_filter(
            [
                $this->getSicCode1(),
                $this->getSicCode2(),
                $this->getSicCode3(),
                $this->getSicCode4(),
            ]
        );
    }

    /**
     * @param array $sicCodes
     */
    public function setSicCodes(array $sicCodes)
    {
        $sicCodes = array_pad(array_values(array_unique($sicCodes)), 4, NULL);

        $this->sicCode1 = $sicCodes[0];
        $this->sicCode2 = $sicCodes[1];
        $this->sicCode3 = $sicCodes[2];
        $this->sicCode4 = $sicCodes[3];
    }

    /**
     * @return string
     */
    public function getSicDescription()
    {
        return $this->sicDescription;
    }

    /**
     * @param string $sicDescription
     */
    public function setSicDescription($sicDescription)
    {
        $this->sicDescription = $sicDescription;
    }

    /**
     * @return string
     */
    public function getCompanyStatus()
    {
        return $this->companyStatus;
    }

    /**
     * @param string $companyStatus
     */
    public function setCompanyStatus($companyStatus)
    {
        $this->companyStatus = (string) $companyStatus;
    }

    /**
     * @return string
     */
    public function getCountryOfOrigin()
    {
        return $this->countryOfOrigin;
    }

    /**
     * @param string $countryOfOrigin
     */
    public function setCountryOfOrigin($countryOfOrigin)
    {
        $this->countryOfOrigin = (string) $countryOfOrigin;
    }

    /**
     * @return string
     */
    public function getAccountsRefDate()
    {
        return $this->accountsRefDate;
    }

    /**
     * @param string $accountsRefDate
     */
    public function setAccountsRefDate($accountsRefDate)
    {
        $this->accountsRefDate = (string) $accountsRefDate;
    }

    /**
     * @return DateTime
     */
    public function getAccountsNextDueDate()
    {
        return $this->accountsNextDueDate;
    }

    /**
     * @param DateTime $accountsNextDueDate
     */
    public function setAccountsNextDueDate($accountsNextDueDate)
    {
        $this->accountsNextDueDate = $accountsNextDueDate;
    }

    /**
     * @return DateTime
     */
    public function getAccountsLastMadeUpDate()
    {
        return $this->accountsLastMadeUpDate;
    }

    /**
     * @param DateTime $accountsLastMadeUpDate
     */
    public function setAccountsLastMadeUpDate($accountsLastMadeUpDate)
    {
        $this->accountsLastMadeUpDate = $accountsLastMadeUpDate;
    }

    /**
     * @return DateTime
     */
    public function getReturnsNextDueDate()
    {
        return $this->returnsNextDueDate;
    }

    /**
     * @param DateTime $returnsNextDueDate
     */
    public function setReturnsNextDueDate($returnsNextDueDate)
    {
        $this->returnsNextDueDate = $returnsNextDueDate;
    }

    /**
     * @return DateTime
     */
    public function getReturnsLastMadeUpDate()
    {
        return $this->returnsLastMadeUpDate;
    }

    /**
     * @param DateTime $returnsLastMadeUpDate
     */
    public function setReturnsLastMadeUpDate($returnsLastMadeUpDate)
    {
        $this->returnsLastMadeUpDate = $returnsLastMadeUpDate;
    }

    /**
     * @return integer
     */
    public function getDcaId()
    {
        return $this->dcaId;
    }

    /**
     * @param integer $dcaId
     */
    public function setDcaId($dcaId)
    {
        $this->dcaId = $dcaId;
    }

    /**
     * @return integer
     */
    public function getRegisteredOfficeId()
    {
        return $this->registeredOfficeId;
    }

    /**
     * @param integer $registeredOfficeId
     */
    public function setRegisteredOfficeId($registeredOfficeId)
    {
        $this->registeredOfficeId = $registeredOfficeId;
    }

    /**
     * @return integer
     */
    public function getServiceAddressId()
    {
        return $this->serviceAddressId;
    }

    /**
     * @param integer $serviceAddressId
     */
    public function setServiceAddressId($serviceAddressId)
    {
        $this->serviceAddressId = $serviceAddressId;
    }

    /**
     * @return integer
     */
    public function getNomineeDirectorId()
    {
        return $this->nomineeDirectorId;
    }

    /**
     * @param integer $nomineeDirectorId
     */
    public function setNomineeDirectorId($nomineeDirectorId)
    {
        $this->nomineeDirectorId = $nomineeDirectorId;
    }

    /**
     * @return integer
     */
    public function getNomineeSecretaryId()
    {
        return $this->nomineeSecretaryId;
    }

    /**
     * @param integer $nomineeSecretaryId
     */
    public function setNomineeSecretaryId($nomineeSecretaryId)
    {
        $this->nomineeSecretaryId = $nomineeSecretaryId;
    }

    /**
     * @return integer
     */
    public function getNomineeSubscriberId()
    {
        return $this->nomineeSubscriberId;
    }

    /**
     * @param integer $nomineeSubscriberId
     */
    public function setNomineeSubscriberId($nomineeSubscriberId)
    {
        $this->nomineeSubscriberId = $nomineeSubscriberId;
    }

    /**
     * @return integer
     */
    public function getAnnualReturnId()
    {
        return $this->annualReturnId;
    }

    /**
     * @param integer $annualReturnId
     */
    public function setAnnualReturnId($annualReturnId)
    {
        $this->annualReturnId = $annualReturnId;
    }

    /**
     * @return integer
     */
    public function getChangeNameId()
    {
        return $this->changeNameId;
    }

    /**
     * @param integer $changeNameId
     */
    public function setChangeNameId($changeNameId)
    {
        $this->changeNameId = $changeNameId;
    }

    /**
     * @return integer
     */
    public function getEreminderId()
    {
        return $this->ereminderId;
    }

    /**
     * @param integer $ereminderId
     */
    public function setEreminderId($ereminderId)
    {
        $this->ereminderId = $ereminderId;
    }

    /**
     * @return DateTime
     */
    public function getDocumentDate()
    {
        return $this->documentDate;
    }

    /**
     * @param DateTime $documentDate
     */
    public function setDocumentDate(DateTime $documentDate)
    {
        $this->documentDate = $documentDate;
    }

    /**
     * @return string
     */
    public function getDocumentId()
    {
        return $this->documentId;
    }

    /**
     * @param string $documentId
     */
    public function setDocumentId($documentId)
    {
        $this->documentId = $documentId;
    }

    /**
     * @return DateTime
     */
    public function getAcceptedDate()
    {
        return $this->acceptedDate;
    }

    /**
     * @param DateTime $acceptedDate
     */
    public function setAcceptedDate(DateTime $acceptedDate)
    {
        $this->acceptedDate = $acceptedDate;
    }

    /**
     * @return int
     */
    public function getCashBackAmount()
    {
        return $this->cashBackAmount;
    }

    /**
     * @param int $cashBackAmount
     */
    public function setCashBackAmount($cashBackAmount)
    {
        $this->cashBackAmount = $cashBackAmount;
    }

    /**
     * @return string
     */
    public function getNoPscReason()
    {
        return $this->noPscReason;
    }

    /**
     * @param string $noPscReason
     */
    public function setNoPscReason($noPscReason)
    {
        $this->noPscReason = $noPscReason;
    }

    /**
     * @return boolean
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * @param boolean $locked
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;
    }

    /**
     * @return boolean
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * @param boolean $hidden
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
    }

    /**
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param boolean $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * @return DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @param DateTime $dtc
     */
    public function setDtc(DateTime $dtc)
    {
        $this->dtc = $dtc;
    }

    /**
     * @return DateTime
     */
    public function getDtm()
    {
        return $this->dtm;
    }

    /**
     * @param DateTime $dtm
     */
    public function setDtm(DateTime $dtm)
    {
        $this->dtm = $dtm;
    }

    /**
     * @return Service[]|ArrayCollection
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * @return Service[]|ArrayCollection
     */
    public function getGroupedServices()
    {
        return $this->services->filter(
            function(Service $service) {
                return !$service->hasParent();
            }
        );
    }

    /**
     * @return Service[]|ArrayCollection
     */
    public function getEnabledParentServices()
    {
        return $this->getGroupedServices()->filter(
            function(Service $service) {
                return $service->isEnabled();
            }
        );
    }

    /**
     * @return Service[]|ArrayCollection
     */
    public function getEnabledActivatedParentServices()
    {
        return $this->getGroupedServices()->filter(
            function(Service $service) {
                return $service->isEnabled() && $service->hasDates();
            }
        );
    }

    /**
     * @param string $serviceTypeId
     * @return bool
     */
    public function hasActiveServiceOfType($serviceTypeId)
    {
        $services = $this->getEnabledActivatedParentServices();

        foreach ($services as $service) {
            if (!$service->isActive()) {
                continue;
            }

            if ($service->getServiceTypeId() == $serviceTypeId) {
                return TRUE;
            }

            //todo: psc - add to service entity (hasChildServiceOfType)
            foreach ($service->getChildren() as $child) {
                if ($child->getServiceTypeId() == $serviceTypeId) {
                    return TRUE;
                }
            }
        }

        return FALSE;
    }

    /**
     * @param array<Service> $services
     */
    public function setServices($services)
    {
        return $this->services = $services;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param Order $order
     */
    public function setOrder(Order $order)
    {
         $this->order = $order;
    }

    /**
     * @return ArrayCollection|FormSubmission[]
     */
    public function getFormSubmissions()
    {
        return $this->formSubmissions;
    }

    /**
     * @param FormSubmission $formSubmission
     */
    public function addFormSubmission(FormSubmission $formSubmission)
    {
        $formSubmission->setCompany($this);
        $this->formSubmissions[] = $formSubmission;
    }

    /**
     * @param array<FormSubmission> $formSubmissions
     */
    public function setFormSubmissions($formSubmissions)
    {
        return $this->formSubmissions = $formSubmissions;
    }

    /**
     * @param Company $company
     * @return bool
     */
    public function isEqual(Company $company)
    {
        return ($this->getId() === $company->getId());
    }

    /**
     * @param Service $service
     */
    public function addService(Service $service)
    {
        $service->setCompany($this);
        $this->services[] = $service;
    }

    /**
     * @return bool
     */
    public function isLimitedBySharesType()
    {
        return ($this->getCompanyCategory() === self::COMPANY_CATEGORY_BYSHR);
    }

    /**
     * @return Member[]
     */
    public function getRegisterMembers()
    {
        return $this->registerMembers;
    }

    /**
     * @param Member $registerMember
     */
    public function addRegisterMember(Member $registerMember)
    {
        $registerMember->setCompany($this);
        $this->registerMembers[] = $registerMember;
    }

    /**
     * @return bool
     */
    public function hasServices()
    {
        return $this->getServices()->isEmpty() === FALSE;
    }

    /**
     * @return boolean
     */
    public function hasActiveService()
    {
        foreach ($this->getServices() as $service) {
            if ($service->isActive()) {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * @param Service $service
     * @return bool
     */
    public function hasEqualService(Service $service)
    {
        $companyServices = $this->getServicesByType($service->getServiceTypeId());
        foreach ($companyServices as $companyService) {
            if ($companyService->isEqual($service)) {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * @param $serviceTypeId
     * @return Service[]|ArrayCollection
     */
    public function getServicesByType($serviceTypeId)
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("serviceTypeId", $serviceTypeId));

        return $this->getServices()->matching($criteria);
    }

    /**
     * @param Service $service
     * @return NULL|Service
     */
    public function getLastServiceByType($service)
    {
        $collection = $this->getServicesByType($service->getServiceTypeId());
        $lastService = NULL;
        foreach ($collection as $oldService) {
            if ($service->getId() != $oldService->getId()) {
                if (!$lastService || ($oldService->isEnabled() && $oldService->getDtExpires() > $lastService->getDtExpires())) {
                    $lastService = $oldService;
                }
            }

        }
        return $lastService;
    }

    /**
     * Returns whether there is active service after service that was given as parameter
     *
     * @param Service $service
     * @return bool
     */
    public function hasActiveRenewalServiceByType($service)
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("serviceTypeId", $service->getServiceTypeId()))
            ->andWhere(Criteria::expr()->gt('dtExpires', $service->getDtExpires()));

        return !$this->getServices()->matching($criteria)->filter(
            function (Service $service) {
                return $service->isActive();
            }
        )->isEmpty();
    }

    /**
     * @param array $types
     * @return Service[]|ArrayCollection
     */
    public function getActiveProductServices(array $types)
    {
        return $this->getGroupedServices()->filter(
            function(Service $service) use ($types) {
                return
                    $service->isProductType()
                    && $service->isActivatedAndNotExpired()
                    && in_array($service->getServiceTypeId(), $types);
            }
        );
    }

    /**
     * @param DateTime $date
     * @return ArrayCollection|Service[]
     */
    public function getNonExpiredServicesOn(DateTime $date)
    {
        return $this->getGroupedServices()->filter(
            function(Service $service) use ($date) {
                return $service->isEnabled() && $service->isExpiringAfter($date);
            }
        );
    }

    /**
     * @param array $types
     * @return Service[]|ArrayCollection
     */
    public function getProductServicesByTypes(array $types)
    {
        return $this->getGroupedServices()->filter(
            function(Service $service) use ($types) {
                return
                    $service->isProductType()
                    && $service->isEnabled()
                    && ($service->isActivatedAndNotExpired() || !$service->hasDates())
                    && in_array($service->getServiceTypeId(), $types);
            }
        );
    }

    /**
     * get last package service which includes service type or get last package by type
     * @param string $type
     * @return Service
     */
    public function getLastPackageService($type)
    {
        /** @var Service $lastService */
        $lastService = NULL;
        $collection = $this->getServicesByType($type);
        foreach ($collection as $service) {
            if ($service->isPackageType() && ($service->isActivatedAndNotExpired() || !$service->hasDates())) {
                $expectedService = $service->hasParent() ? $service->getParent() : $service;
                if (!$lastService || $expectedService->getDtExpires() > $lastService->getDtExpires()) {
                    $lastService = $expectedService;
                }
            }
        }
        return $lastService;
    }

    /**
     * @return bool
     */
    public function hasAuthenticationCode()
    {
        return (bool) $this->getAuthenticationCode();
    }

    /**
     * @param array $importedCompany
     */
    public function hydrateImportedCompanyDetails(array $importedCompany)
    {
        $this->setCompanyNumber($importedCompany['companyNumber']);
        $this->setCompanyStatus($importedCompany['companyStatus']);
        $this->setCountryOfOrigin($importedCompany['countryOfOrigin']);
        $this->setIncorporationDate($importedCompany['incorporationDate']);
        $this->setAccountsRefDate($importedCompany['accountsRefDate']);
        $this->setAccountsNextDueDate($importedCompany['accountsNextDueDate']);
        $this->setAccountsLastMadeUpDate($importedCompany['accountsLastMadeUpDate']);
        $this->setReturnsNextDueDate($importedCompany['returnsNextDueDate']);
        $this->setReturnsLastMadeUpDate($importedCompany['returnsLastMadeUpDate']);
        $this->setCareOfName($importedCompany['careOfName']);
        $this->setPoBox($importedCompany['poBox']);
        $this->setIsCertificatePrinted($importedCompany['isCertificatePrinted']);
        $this->setIsBronzeCoverLetterPrinted($importedCompany['isBronzeCoverLetterPrinted']);
    }

    /**
     * @return ArrayCollection|CompanyToolkitOffer[]
     */
    public function getSelectedToolkitOffers()
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('selected', TRUE));

        return $this->toolkitOffers->matching($criteria);
    }

    /**
     * @return bool
     */
    public function toolkitOffersAlreadyChosen()
    {
        return !$this->toolkitOffers->isEmpty();
    }

    /**
     * @param CompanyToolkitOffer $offer
     */
    public function addToolkitOffer(CompanyToolkitOffer $offer)
    {
        if (!$this->toolkitOffers->contains($offer)) {
            $offer->setCompany($this);
            $this->toolkitOffers->add($offer);
        }
    }

    public function clearToolkitOffers()
    {
        $this->toolkitOffers->clear();
    }

    /**
     * @return bool
     */
    public function isDissolved()
    {
        return $this->companyStatus == self::COMPANY_STATUS_DISSOLVED;
    }

    /**
     * @param CompanySetting $setting
     */
    public function addSetting(CompanySetting $setting)
    {
        $this->settings[] = $setting;
    }

    /**
     * @return bool
     */
    public function hasDismissedCashBackPromotion()
    {
        foreach ($this->settings as $setting) {
            if ($setting->isBankingOffer()) {
                return $setting->isEnabled();
            }
        }
        return FALSE;
    }

    /**
     * @return bool
     */
    public function belongsToUkCustomer()
    {
        return $this->customer->isUkCustomer();
    }
}
