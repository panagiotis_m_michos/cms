<?php

namespace Entities;

use Exceptions\Business\BusinessLogic as BusinessLogicException;

class CapitalShare extends Share implements EntityInterface
{

    // statuses
    const STATUS_PAID = 'paid';
    const STATUS_UNPAID = 'unpaid';
    const STATUS_PARTIALY_PAID = 'partialy_paid';

    /** @var array */
    static public $paidStatuses = Array(
        self::STATUS_PAID => 'Fully Paid',
        self::STATUS_UNPAID => 'Unpaid',
        self::STATUS_PARTIALY_PAID => 'Partially Paid',
    );

    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $prescribedParticulars;

    /**
     * @var int
     */
    protected $aggregateNominalValue;

    /**
     * @var Capital
     */
    protected $capital;

    /**
     * @var string
     */
    protected $paid;

    /**
     * @var string
     */
    protected $syncPaid = NULL;

    /**
     * @var string
     */
    protected $partialyPaid;

    /**
     * @var int
     */
    protected $shareValue;

    /**
     * Should allways validate constructor values
     * @param string $shareClass
     * @param string $prescrPartic
     * @param int $numShares
     * @param int $shareValue
     * @param string $paid
     */
    public function __construct($shareClass, $prescrPartic, $numShares, $shareValue, $paid)
    {
        $this->setShareClass($shareClass);
        $this->setPrescribedParticulars($prescrPartic);
        $this->setNumShares($numShares);
        $this->setShareValue($shareValue);
        $this->setPaid($paid);
    }

    /**
     * @param type $prescribedParticulars
     * @return CapitalShare
     * @throws InvalidArgumentException
     */
    public function setPrescribedParticulars($prescribedParticulars)
    {
        if ($prescribedParticulars == NULL) {
            throw new BusinessLogicException("The prescribedParticulars is invalid.");
        }
        $this->prescribedParticulars = $prescribedParticulars;
        return $this;
    }

    /**
     * @param int $shareValue
     * @return CapitalShare
     * @throws InvalidArgumentException
     */
    public function setShareValue($shareValue)
    {
        if ($shareValue == NULL) {
            throw new Exception("The shareValue is invalid.");
        }
        $this->shareValue = $shareValue;
        return $this;
    }

    /**
     * @param string $paid
     * @return CapitalShare
     * @throws InvalidArgumentException
     */
    public function setPaid($paid)
    {
        if ($paid == NULL) {
            throw new BusinessLogicException("The paid is invalid.");
        }
        $this->paid = $paid;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrescribedParticulars()
    {
        return $this->prescribedParticulars;
    }

    /**
     * @return string
     */
    public function getAggregateNominalValue()
    {
        return $this->aggregateNominalValue;
    }

    /**
     * @return string
     */
    public function getPaid()
    {
        return $this->paid;
    }

    /**
     * @return Capital
     */
    public function getCapital()
    {
        return $this->capital;
    }

    /**
     * @param Capital $capital
     * @return CapitalShare
     */
    public function setCapital(Capital $capital)
    {
        $this->capital = $capital;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return CapitalShare
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getShareValue()
    {
        return $this->shareValue;
    }

    /**
     * @return string
     */
    public function getSyncPaid()
    {
        return $this->syncPaid;
    }

    /**
     * @param string $syncPaid
     * @return CapitalShare
     */
    public function setSyncPaid($syncPaid)
    {
        $this->syncPaid = $syncPaid;
        return $this;
    }

    /**
     * @return string
     */
    public function getPartialyPaid()
    {
        return $this->partialyPaid;
    }

    /**
     * @param string $partialyPaid
     * @return CapitalShare
     */
    public function setPartialyPaid($partialyPaid)
    {
        $this->partialyPaid = $partialyPaid;
        return $this;
    }

    /**
     * @param string $aggregateNominalValue
     * @return CapitalShare
     */
    public function setAggregateNominalValue($aggregateNominalValue)
    {
        $this->aggregateNominalValue = $aggregateNominalValue;
        return $this;
    }

}
