<?php

namespace Entities;

use Entities\CapitalShare;
use ValueObject\Currency;
use Exceptions\Business\BusinessLogic as BusinessLogicException;

//<Capital>
//    <TotalNumberOfIssuedShares>2</TotalNumberOfIssuedShares>
//    <ShareCurrency>GBP</ShareCurrency>
//    <TotalAggregateNominalValue>4.00</TotalAggregateNominalValue>
//        <Shares>
//            <ShareClass>ORD</ShareClass>
//            <PrescribedParticulars>'One vote per share, with residual interest'.</PrescribedParticulars>
//            <NumShares>2</NumShares>
//            <AmountPaidDuePerShare>2.00</AmountPaidDuePerShare>
//            <AmountUnpaidPerShare>0</AmountUnpaidPerShare>
//            <AggregateNominalValue>4.00</AggregateNominalValue>
//        </Shares>
//</Capital>

class Capital implements EntityInterface
{

    /**
     * @var int
     */
    protected $totalNumberOfIssuedShares;

    /**
     * @var string
     */
    protected $shareCurrency;

    /**
     * @var int
     */
    protected $totalAggregateNominalValue;

    /**
     * @var array
     */
    protected $shares = array();

    /**
     * @var string
     */
    protected $id;

    /**
     * @var int
     */
    protected $formSubmissionId;

    /**
     * Should allways validate constructor values
     * @param string $shareCurrency
     * @param int $formSubmissionId
     */
    public function __construct($shareCurrency, $formSubmissionId)
    {
        $this->setShareCurrency($shareCurrency);
        $this->setFormSubmissionId($formSubmissionId);
    }

    /**
     * @param string $shareCurrency
     * @return Capital
     * @throws InvalidArgumentException
     */
    public function setShareCurrency($shareCurrency)
    {
        if (!in_array($shareCurrency, Currency::getAllCurrencies())) {
            throw new Exception("The shareCurrency is invalid.");
        }
        $this->shareCurrency = $shareCurrency;
        return $this;
    }

    /**
     * @param int $formSubmissionId
     * @return Capital
     * @throws InvalidArgumentException
     */
    public function setFormSubmissionId($formSubmissionId)
    {
        if ($formSubmissionId == NULL) {
            throw new BusinessLogicException("The formSubmissionId is invalid.");
        }
        $this->formSubmissionId = $formSubmissionId;
        return $this;
    }

    /**
     * @return int
     */
    public function getFormSubmissionId()
    {
        return $this->formSubmissionId;
    }

    /**
     * @return string
     */
    public function getShareCurrency()
    {
        return $this->shareCurrency;
    }

    /**
     * @return string
     */
    public function getTotalNumberOfIssuedShares()
    {
        return $this->totalNumberOfIssuedShares;
    }

    /**
     * @return string
     */
    public function getTotalAggregateNominalValue()
    {
        return $this->totalAggregateNominalValue;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Capital
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return array
     */
    public function getShares()
    {
        return $this->shares;
    }

    /**
     * @param CapitalShare $share
     * @return Capital
     */
    public function addShare(CapitalShare $share)
    {
        $share->setCapital($this);
        $this->shares[] = $share;
        return $this;
    }

    /**
     * @param array $shares
     * @return Capital
     */
    public function setShares($shares)
    {
        foreach ($shares as $share) {
            $this->totalAggregateNominalValue = $this->totalAggregateNominalValue + $share->getAggregateNominalValue();
            $this->totalNumberOfIssuedShares = $this->totalNumberOfIssuedShares + $share->getNumShares();
            $share->setCapital($this);
        }
        $this->shares = $shares;
        return $this;
    }

}
