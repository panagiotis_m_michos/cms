<?php

namespace Repositories;

use Mapper\CapitalMapper;
use Entities\Capital;
use Mapper\MapperInterface;

class CapitalRepository implements RepositoryInterface
{

    /**
     * @var CapitalMapper
     */
    protected $capitalMapper;

    /**
     * @param MapperInterface $capitalMapper
     */
    public function __construct(MapperInterface $capitalMapper)
    {
        $this->capitalMapper = $capitalMapper;
    }

    /**
     * @param string $id
     * @return Capital
     */
    public function findById($id)
    {
        return $this->capitalMapper->findById($id);
    }

    /**
     * create capital objects from dibi records
     * @param int $formSubmissionId
     * @return array
     */
    public function getAllCapitals($formSubmissionId)
    {
        return $this->fetch(array("form_submission_id" => $formSubmissionId));
    }

    /**
     * @param array $conditions
     * @return array
     */
    protected function fetch(array $conditions)
    {
        return $this->capitalMapper->findAll($conditions);
    }

    /**
     * @param array $capitals
     * @param Capital $capital
     * @return boolean
     */
    public function findSameCurrencyCapitals($capitals, Capital $capitalObj)
    {
        foreach ($capitals as $capital) {
            //when update don't compare same items
            if ($capital->getId() != $capitalObj->getId()) {
                if (strtolower($capital->getShareCurrency()) == strtolower($capitalObj->getShareCurrency())) {
                    return TRUE;
                }
            }
        }
        return FALSE;
    }

}
