<?php
    
namespace Repositories;

class AnswerRepository extends BaseRepository_Abstract
{
    /**
     * @return QueryBuilder
     */
    public function getListBuilder()
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('a')->from('Entities\Answer', 'a')
            ->orderBy('a.dtc', 'DESC');
        return $qb;
    }
}
