<?php

namespace Repositories;

use FEmail;
use Exceptions\Business\ItemNotFound;

class EmailRepository
{
    /**
     * @param int $nodeId
     * @return FEmail
     * @throws type
     */
    public function find($nodeId)
    {
        $node = new FEmail($nodeId);
        if (!$node->isEmail()) {
            throw ItemNotFound("The identifier $nodeId is missing for a query of FEmail");
        }
        return $node;
    }

}
