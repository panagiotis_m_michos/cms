<?php

namespace Repositories\Register\Psc;

use Entities\Company;
use Entities\Register\Psc\CompanyStatement;
use Entities\Register\Psc\CorporatePsc;
use Entities\Register\Psc\Entry;
use Entities\Register\Psc\PersonPsc;
use Entities\Register\Psc\Psc;
use EntityNotFound;
use Exceptions\Business\Forbidden;
use Repositories\BaseRepository_Abstract;

class EntryRepository extends BaseRepository_Abstract
{
    /**
     * @param Company $company
     * @return Entry[]
     */
    public function getEntriesByCompany(Company $company)
    {
        return $this->createQueryBuilder('e')
            ->where('e.company = :company')
            ->setParameter('company', $company)
            ->orderBy('e.dtc', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Company $company
     * @return Entry
     */
    public function getLastEntryByCompany(Company $company)
    {
        return $this->createQueryBuilder('e')
            ->where('e.company = :company')
            ->setParameter('company', $company)
            ->orderBy('e.dtc', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param Company $company
     * @return Psc[]
     */
    public function getActivePscsByCompany(Company $company)
    {
        return $this->createQueryBuilder()
            ->select('p')
            ->from(Psc::class, 'p')
            ->where('p.company = :company')
            ->andWhere('p.dateCessated IS NULL')
            ->setParameter('company', $company)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Company $company
     * @return CompanyStatement[]
     */
    public function getActiveCompanyStatementsByCompany(Company $company)
    {
        return $this->createQueryBuilder()
            ->select('cs')
            ->from(CompanyStatement::class, 'cs')
            ->where('cs.company = :company')
            ->andWhere('cs.dateWithdrew IS NULL')
            ->setParameter('company', $company)
            ->getQuery()
            ->getResult();
    }
}
