<?php

namespace Repositories\Register;

use Repositories\BaseRepository_Abstract;
use Entities\Company;

class MemberRepository extends BaseRepository_Abstract
{
    /**
     * @param Company $company
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getListBuilder(Company $company)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('m')->from('Entities\Register\Member', 'm')
            ->andWhere('m.company = :company')
            ->setParameter('company', $company)
            ->orderBy('m.dateEntry', 'DESC');
        return $qb;
    }
}