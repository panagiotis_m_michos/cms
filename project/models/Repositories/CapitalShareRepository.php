<?php

namespace Repositories;

use Entities\CapitalShare;
use Mapper\CapitalShareMapper;
use Mapper\MapperInterface;

class CapitalShareRepository implements RepositoryInterface
{

    /**
     * @var CapitalShareMapper
     */
    protected $capitalShareMapper;

    /**
     * @param MapperInterface $capitalShareMapper
     */
    public function __construct(MapperInterface $capitalShareMapper)
    {
        $this->capitalShareMapper = $capitalShareMapper;
    }

    /**
     * @param int $id
     * @return CapitalShare
     */
    public function findById($id)
    {
        return $this->capitalShareMapper->findById($id);
    }

    /**
     * @param type $id
     * @return array
     */
    public function getAllCapitalShares($id)
    {
        $capitalClues = explode("_", $id);
        return $this->fetch(array("form_submission_id" => $capitalClues[1], 'currency', $capitalClues[0]));
    }

    /**
     * @param int $formSubmissionId
     * @return array
     */
    public function getAllShares($formSubmissionId)
    {
        return $this->fetch(array("form_submission_id" => $formSubmissionId));
    }

    /**
     * @param array $conditions
     * @return array
     */
    protected function fetch(array $conditions)
    {
        return $this->capitalShareMapper->findAll($conditions);
    }

    /**
     * @param array $capitals
     * @param CapitalShare $shareClass
     * @return boolean
     */
    public function findSameShareClassShares($capitals, CapitalShare $capitalShare)
    {
        foreach ($capitals as $capital) {
            foreach ($capital->getShares() as $share) {
                //when update don't compare same items, and just compare shares in same capital
                if ($share->getId() != $capitalShare->getId() && $capital->getId() == $capitalShare->getCapital()->getId()) {
                    if (strtolower($share->getShareClass()) == strtolower($capitalShare->getShareClass())) {
                        return TRUE;
                    }
                }
            }
        }
        return FALSE;
    }

    /**
     * @param CapitalShare $entity
     * @return int
     */
    public function save(CapitalShare $entity)
    {
        $this->capitalShareMapper->save($entity);
        return $entity->getId();
    }

    /**
     * @param EntityInterface $entity
     */
    public function delete(CapitalShare $entity)
    {
        $this->capitalShareMapper->delete($entity);
    }

}
