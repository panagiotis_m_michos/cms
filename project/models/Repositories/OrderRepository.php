<?php

namespace Repositories;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Internal\Hydration\IterableResult;
use DateTime;
use Doctrine\ORM\QueryBuilder;

class OrderRepository extends BaseRepository_Abstract
{

    /**
     * @param DateTime $dtc
     * @return IterableResult
     */
    public function getLaterThan(DateTime $dtc)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('i')->from('Entities\Order', 'i');
        $qb->where('i.dtc > ?1')->setParameter(1, $dtc, Type::DATETIME);
        $iterator = $qb->getQuery()->iterate();
        return $iterator;
    }

    /**
     * @return QueryBuilder
     */
    public function getListBuilder()
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('o')->from('Entities\Order', 'o')
            ->join('o.transaction', 't')
            ->orderBy('o.orderId', 'DESC');
        return $qb;
    }

}
