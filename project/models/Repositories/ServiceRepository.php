<?php

namespace Repositories;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Internal\Hydration\IterableResult;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\ORM\QueryBuilder;
use Entities\Company;
use Entities\Order;
use Entities\OrderItem;
use Entities\Payment\Token;
use Entities\Service;
use Entities\ServiceSettings;
use Utils\Date;

class ServiceRepository extends BaseRepository_Abstract
{
    /**
     * @param Company $company
     * @return QueryBuilder
     */
    public function getListBuilder(Company $company = NULL)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('s')->from('Entities\Service', 's')
            ->where('s.parent IS NULL')
            ->orderBy('s.dtc', 'DESC');
        if ($company) {
            $qb->andWhere('s.company = :company')
                ->setParameter('company', $company);
        }
        return $qb;
    }

    /**
     * @param Company $company
     * @param string $serviceTypeId
     * @param Service $service
     * @return Service
     */
    public function getLastService(Company $company, $serviceTypeId, Service $service = NULL)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('s')->from('Entities\Service', 's')
            ->where('s.company = :company')
            ->setParameter('company', $company)
            ->andWhere('s.serviceTypeId = :serviceTypeId')
            ->setParameter('serviceTypeId', $serviceTypeId)
            ->orderBy('s.dtExpires', 'DESC');
        if ($service) {
            $qb->andWhere('s.serviceId != :serviceId')
                ->setParameter('serviceId', $service->getId());
        }
        $services = $qb->getQuery()->getResult();
        return !empty($services) ? $services[0] : NULL;
    }

    /**
     * @param OrderItem $orderItem
     * @return Service[]
     */
    public function getServiceByOrderItem(OrderItem $orderItem)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('s')->from('Entities\Service', 's')
            ->where('s.orderItem = :orderItem')->setParameter('orderItem', $orderItem)
            ->andWhere('s.parent IS NULL');

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param OrderItem $orderItem
     * @return Service[]
     */
    public function getOrderItemServices(OrderItem $orderItem)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('s')->from('Entities\Service', 's')
            ->where('s.order = :order')
            ->andWhere('s.productId = :productId')
            ->andWhere('s.parent IS NULL')
            ->setParameters(
                [
                    'productId' => $orderItem->getProductId(),
                    'order' => $orderItem->getOrder(),
                ]
            );
        return $qb->getQuery()->getResult();
    }

    /**
     * @param int $productId
     * @return IterableResult
     */
    public function getServicesByProductId($productId)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('s')
            ->from('Entities\Service', 's')
            ->where('s.productId = :productId')
            ->setParameter('productId', $productId);
        return $qb->getQuery()->iterate();
    }

    /**
     * @param Date $from
     * @param Date $to
     * @param string $event
     * @return IterableResult
     */
    public function getServicesDataExpiringWithinDatesForEvent(Date $from, Date $to, $event)
    {
        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata('Entities\Service', 's');
        $rsm->addJoinedEntityFromClassMetadata(
            'Entities\Company',
            'c',
            's',
            'company',
            [],
            ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT
        );

        $selectClause = $rsm->generateSelectClause(['s' => 's', 'c' => 'c']);

        $query = "SELECT {$selectClause}
            FROM cms2_services s
            INNER JOIN (
                SELECT MAX(dtExpires) AS maxDtExpires, companyId, serviceTypeId
                FROM cms2_services
                WHERE
                    parentId IS NULL
                    AND stateId = :enabledService
                    AND dtStart IS NOT NULL AND dtExpires IS NOT NULL
                GROUP BY companyId, serviceTypeId
            ) AS latestService
            ON (
              s.dtExpires = latestService.maxDtExpires
              AND s.companyId = latestService.companyId
              AND s.serviceTypeId = latestService.serviceTypeId
            )
            INNER JOIN ch_company c ON c.company_id = s.companyId
            INNER JOIN cms2_customers cu ON cu.customerId = c.customer_id
            INNER JOIN cms2_service_settings ss ON (ss.companyId = c.company_id AND ss.serviceTypeId = s.serviceTypeId)
            INNER JOIN cms2_tokens t ON t.tokenId = ss.renewalTokenId
            LEFT JOIN cms2_events e ON (e.objectId = s.serviceId AND eventKey = :eventKey)
            WHERE
                s.parentId IS NULL
                AND s.stateId = :enabledService
                AND s.dtStart IS NOT NULL AND s.dtExpires IS NOT NULL
                AND ss.isAutoRenewalEnabled = 1
                AND ss.renewalTokenId IS NOT NULL
                AND s.dtExpires BETWEEN :expiresFrom AND :expiresTo
                AND t.cardExpiryDate >= s.dtExpires
                AND t.sageStatus = :activeSageStatus
                AND e.eventId IS NULL
            ORDER BY c.customer_id, s.companyId, s.serviceId";

        $services = $this->_em->createNativeQuery($query, $rsm)
            ->setParameter('enabledService', Service::STATE_ENABLED)
            ->setParameter('eventKey', $event)
            ->setParameter('expiresFrom', $from, Type::DATE)
            ->setParameter('expiresTo', $to, Type::DATE)
            ->setParameter('activeSageStatus', Token::SAGE_STATUS_ACTIVE)
            ->iterate();

        return $services;
    }

    /**
     * @return IterableResult
     */
    public function getRefundedServices()
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('s')
            ->from('Entities\Service', 's')
            ->join('Entities\OrderItem', 'oi', 'WITH', 'oi.order = s.order')
            ->where('oi.productId = s.productId')
            ->andWhere('oi.isRefunded = 1')
            ->andWhere('s.parent IS NULL');
        return $qb->getQuery()->iterate();
    }

    /**
     * @param Order $order
     * @return array
     */
    public function getOrderServices(Order $order)
    {
        return $this->_em->createQueryBuilder()
            ->select('s')
            ->from('Entities\Service', 's')
            ->where('s.order = :order')
            ->andWhere('s.parent IS NULL')
            ->setParameter('order', $order)
            ->getQuery()
            ->iterate();
    }

    /**
     * @param ServiceSettings $settings
     * @return Service
     */
    public function getLatestServiceBySettings(ServiceSettings $settings)
    {
        return $this->_em->createQueryBuilder()
            ->select('s')
            ->from('Entities\Service', 's')
            ->where('s.company = :company')
            ->andWhere('s.serviceTypeId = :serviceTypeId')
            ->andWhere('s.parent IS NULL')
            ->andWhere('s.stateId = :enabledState')
            ->orderBy('s.dtExpires', 'DESC')
            ->setMaxResults(1)
            ->setParameters(
                [
                    'company' => $settings->getCompany(),
                    'serviceTypeId' => $settings->getServiceTypeId(),
                    'enabledState' => Service::STATE_ENABLED,
                ]
            )
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * @param Company $company
     * @param string $serviceTypeId
     * @return Service[]
     */
    public function getCompanyServicesByType(Company $company, $serviceTypeId)
    {
        return $this->_em->createQueryBuilder()
            ->select('s')
            ->from('Entities\Service', 's')
            ->where('s.company = :company')
            ->andWhere('s.serviceTypeId = :serviceTypeId')
            ->andWhere('s.parent IS NULL')
            ->andWhere('s.stateId = :enabledState')
            ->orderBy('s.dtExpires', 'DESC')
            ->setParameters(
                [
                    'company' => $company,
                    'serviceTypeId' => $serviceTypeId,
                    'enabledState' => Service::STATE_ENABLED,
                ]
            )
            ->getQuery()
            ->getResult();
    }

    /**
     * @param array $ids
     * @return Service[]
     */
    public function getServicesByIds(array $ids = [])
    {
        return $this->_em->createQueryBuilder()
            ->select('s')
            ->from($this->_entityName, 's')
            ->where('s.serviceId IN (:ids)')->setParameter('ids', $ids)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Service[]
     */
    public function getServicesToSendRemindersFor()
    {
        return $this
            ->createQueryBuilder('s')
            ->innerJoin(
                'Entities\ServiceSettings',
                'ss',
                Join::WITH,
                'ss.company = s.company AND ss.serviceTypeId = s.serviceTypeId AND ss.isAutoRenewalEnabled = FALSE AND ss.emailReminders = TRUE'
            )
            ->where('DATEDIFF(s.dtExpires, CURRENT_DATE()) IN (:days)')
            ->andWhere('s.parent IS NULL')
            ->andWhere('s.renewalProductId IS NOT NULL')
            ->setParameter('days', [-28, -15, 0, 1, 15, 28])
            ->getQuery()
            ->getResult();
    }
}
