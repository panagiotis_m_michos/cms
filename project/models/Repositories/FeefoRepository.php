<?php

namespace Repositories;

use Doctrine\ORM\Internal\Hydration\IterableResult;
use Entities\Feefo;

class FeefoRepository extends BaseRepository_Abstract
{

    /**
     * @return IterableResult
     */
    public function getWaitingOrders()
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('f')->from('Entities\Feefo', 'f')
            ->andWhere('f.statusId = :status')->setParameter('status', Feefo::STATUS_WAITING);

        return $qb->getQuery()->iterate();
    }

    /**
     * @return IterableResult
     */
    public function getEligibleOrders()
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('f')->from('Entities\Feefo', 'f')
            ->andWhere('f.statusId = :status')->setParameter('status', Feefo::STATUS_ELIGIBLE);

        return $qb->getQuery()->iterate();
    }
}
