<?php

namespace Repositories;

use dibi;
use DibiFluent;
use Doctrine\ORM\Internal\Hydration\IterableResult;
use Doctrine\ORM\QueryBuilder;
use Entities\Cashback;
use Entities\Customer;

class CashbackRepository extends BaseRepository_Abstract
{
    /**
     * @param Customer $customer
     * @return QueryBuilder
     */
    public function getListBuilder(Customer $customer)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('c')
            ->from('Entities\Cashback', 'c')
            ->where('c.customer = ?1')
            ->setParameter(1, $customer)
            ->orderBy('c.dtc', 'ASC');
        return $qb;
    }

    /**
     * @return mixed
     */
    public function getEligibleCashbacksDataSource()
    {
        return dibi::select('cb.*, CONCAT(c.firstName, " ", c.lastName) AS customerName, SUM(cb.amount) AS totalAmount')
            ->from(TBL_CASHBACKS, 'cb')
            ->leftJoin(TBL_CUSTOMERS, 'c')
            ->on('c.customerId = cb.customerId')
            ->where('cb.statusId = %s', Cashback::STATUS_ELIGIBLE)
            ->groupBy('cb.customerId, cb.packageTypeId, cb.statusId, cb.groupId')
            ->orderBy('cb.isArchived', dibi::ASC)
            ->orderBy('cb.dtc', dibi::ASC);
    }

    /**
     * @param Customer $customer
     * @param $packageTypeId
     * @return Cashback[]
     */
    public function getCustomerEligibleCashbacks(Customer $customer, $packageTypeId)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('c')
            ->from('Entities\Cashback', 'c')
            ->where('c.customer = ?1')
            ->setParameter(1, $customer)
            ->andWhere('c.packageTypeId= ?2')
            ->setParameter(2, $packageTypeId)
            ->andWhere('c.statusId = ?3')
            ->setParameter(3, Cashback::STATUS_ELIGIBLE)
            ->orderBy('c.dtc', dibi::ASC);
        return $qb->getQuery()->getResult();
    }

    /**
     * @param Customer $customer
     * @return QueryBuilder
     */
    public function getCustomerCashbacksDataSource(Customer $customer)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('c')
            ->from('Entities\Cashback', 'c')
            ->where('c.customer = :customer')->setParameter('customer', $customer)
            ->andWhere('c.statusId != :outdated')->setParameter('outdated', Cashback::STATUS_OUTDATED)
            ->orderBy('c.dtc', dibi::ASC);

        return $qb;
    }

    /**
     * @param Customer $customer
     * @return DibiFluent
     */
    public function getCashbacksDataSource(Customer $customer = NULL)
    {
        $sql = dibi::select('cb.*, CONCAT(c.firstName, " ", c.lastName) AS customerName, SUM(cb.amount) AS totalAmount')
            ->from(TBL_CASHBACKS, 'cb')
            ->leftJoin(TBL_CUSTOMERS, 'c')
            ->on('c.customerId = cb.customerId')
            ->groupBy('cb.customerId, cb.packageTypeId, cb.statusId, cb.groupId')
            ->orderBy(
                'CASE cb.statusId
                   WHEN %s THEN 1
                   WHEN %s THEN 2
                   WHEN %s THEN 3
                   ELSE 4
                END',
                Cashback::STATUS_IMPORTED,
                Cashback::STATUS_ELIGIBLE,
                Cashback::STATUS_PAID
            );
        if ($customer) {
            $sql->where('c.customerId  = %i', $customer->getId());
        }
        return $sql;
    }

    /**
     * @param Customer $customer
     * @param $packageTypeId
     * @param $statusId
     * @param string $groupId
     * @return array
     */
    public function getGroupedCashbacks(Customer $customer, $packageTypeId, $statusId, $groupId = NULL)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('c')
            ->from('Entities\Cashback', 'c')
            ->where('c.customer = ?1')
            ->setParameter(1, $customer)
            ->andWhere('c.packageTypeId= ?2')
            ->setParameter(2, $packageTypeId)
            ->andWhere('c.statusId= ?3')
            ->setParameter(3, $statusId);

        if ($groupId) {
            $qb->andWhere('c.groupId = ?4')
                ->setParameter(4, $groupId);
        }
        $qb->orderBy('c.dtc', dibi::ASC);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Customer $customer
     * @return IterableResult
     */
    public function getCustomerCashbacksToUnarchive(Customer $customer)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('c')
            ->from('Entities\Cashback', 'c')
            ->where('c.customer = ?1')
            ->setParameter(1, $customer)
            ->andWhere('c.statusId = ?2')
            ->setParameter(2, Cashback::STATUS_IMPORTED)
            ->orWhere('c.isArchived = TRUE');

        return $qb->getQuery()->iterate();
    }
}
