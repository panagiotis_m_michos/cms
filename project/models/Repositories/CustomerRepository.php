<?php
    
namespace Repositories;

use DateTime;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Internal\Hydration\IterableResult;
use DoctrineModule\SelfClearingIterator;

class CustomerRepository extends BaseRepository_Abstract
{

    /**
     * @param DateTime $dtc
     * @return IterableResult
     */
    public function getLaterThan(DateTime $dtc)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('i')->from('Entities\Customer', 'i');
        $qb->where('i.dtc > ?1')->setParameter(1, $dtc, Type::DATETIME);
        $iterator = $qb->getQuery()->iterate();
        return $iterator;
    }

    /**
     * @return SelfClearingIterator
     */
    public function getCustomersWithRequiredIdCheck()
    {
        $qb = $this->createQueryBuilder('c')
            ->where('c.isIdCheckRequired = TRUE');
        return new SelfClearingIterator($qb);
    }

    /**
     * @param string $email
     * @return bool
     */
    public function isEmailTaken($email)
    {
        return (bool) $this->findOneBy(['email' => $email]);
    }
}