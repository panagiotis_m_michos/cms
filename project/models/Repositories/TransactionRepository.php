<?php

namespace Repositories;

use Doctrine\ORM\Internal\Hydration\IterableResult;
use Entities\Customer;

class TransactionRepository extends BaseRepository_Abstract
{

    /**
     * @param \DateTime $dtc
     * @return IterableResult
     */
    public function getLaterThan(\DateTime $dtc)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('i')->from('Entities\Transaction', 'i');
        $qb->where('i.dtc > ?1')->setParameter(1, $dtc, \Doctrine\DBAL\Types\Type::DATETIME);
        $iterator = $qb->getQuery()->iterate();
        return $iterator;
    }

    /**
     * @param Customer $customer
     * @return array
     */
    public function getTransactions(Customer $customer)
    {
        $qb = $this->_em->createQueryBuilder()
            ->select('i')->from('Entities\Transaction', 'i')
            ->where('i.customer = :customer')
            ->setParameter('customer', $customer);
        $rows = $qb->getQuery()->getResult();
        return $rows;
    }
}
