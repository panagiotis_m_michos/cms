<?php

namespace Repositories;

use Entities\EntityAbstract;
use Kdyby\Doctrine\EntityRepository;
use Kdyby\Persistence\Queryable;
use OrmModule\Contracts\IRepository;

abstract class BaseRepository_Abstract extends EntityRepository implements IRepository
{
    /**
     * @param int $entityId
     */
    public function getEntityById($entityId)
    {
        $entity = $this->find($entityId);
        return $entity;
    }

    /**
     * @return array<Entities\>
     */
    public function getAllEntities()
    {
        $entities = $this->findAll();
        return $entities;
    }

    /**
     * @param EntityAbstract $entity
     * @return EntityAbstract
     */
    public function saveEntity($entity)
    {
        $this->_em->persist($entity);
        $this->_em->flush();
        return $entity;
    }

    /**
     * @return void
     */
    public function removeEntity($entity)
    {
        $this->_em->remove($entity);
        $this->_em->flush();
    }

    /**
     * @param array $entities
     * @return void
     */
    public function removeEntities(array $entities)
    {
        foreach ($entities as $entity) {
            $this->_em->remove($entity);
        }

        $this->_em->flush();
    }

    /**
     * persist entity
     * @param EntityAbstract $entity
     */
    public function persist($entity)
    {
        $this->_em->persist($entity);
    }

    /**
     * Provides flush
     *
     * @param NULL|object|array $entity
     */
    public function flush($entity = NULL)
    {
        $this->_em->flush($entity);
    }

    /**
     * Returns key and value pairs as one array
     *
     * @return array $arr
     */
    public function getPairs($key, $value)
    {
        $iterate = $this->findAll();
        $arr = [];
        foreach ($iterate as $entity) {
            $arr[$entity->$key] = $entity->$value;
        }
        return $arr;
    }

    /**
     * count all records in the database
     */
    public function countAll()
    {
        $meta = $this->getClassMetadata();
        $prKeys = $meta->identifier;
        $arr = [];
        foreach ($prKeys as $pr) {
            $arr[] = 'c.' . $pr;
        }
        $select = 'COUNT(' . implode(',', $arr) . ')';
        $qb = $this->_em->createQueryBuilder();
        $qb->select($select);
        $qb->from($this->_entityName, 'c');
        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * get results by array of values
     *
     * @param string $name
     * @param array $ids
     * @return Entities array
     */
    public function in($name, array $ids)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('c')->from($this->_entityName, 'c');
        $qb->add('where', $qb->expr()->in('c.' . $name, $ids));
        return $qb->getQuery()->getResult();
    }

    /**
     * get iterator to all existing entities
     * @return \Doctrine\ORM\Internal\Hydration\IterableResult
     */
    public function findAllIterator()
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('entity')->from($this->_entityName, 'entity');
        $query = $qb->getQuery();
        return $query->iterate();
    }

    /**
     * get iterator to all entities in a table
     * @param string $name
     * @param array $ids
     * @return \Doctrine\ORM\Internal\Hydration\IterableResult
     */
    public function findByIdsIterator($name, $ids)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('entity')->from($this->_entityName, 'entity');
        $qb->add('where', $qb->expr()->in('entity.' . $name, $ids));
        $query = $qb->getQuery();
        return $query->iterate();
    }

}
