<?php

namespace Repositories;

use Mapper\MapperInterface;
use Mapper\ShareholdingMapper;
use Entities\EntityInterface;

class ShareholdingRepository implements RepositoryInterface
{

    /**
     * @var ShareholdingMapper
     */
    protected $shareholdingMapper;

    /**
     * @param MapperInterface $shareholdingMapper
     */
    public function __construct(MapperInterface $shareholdingMapper)
    {
        $this->shareholdingMapper = $shareholdingMapper;
    }

    /**
     * @param int $id
     * @return Shareholding
     */
    public function findById($id)
    {
        return $this->shareholdingMapper->findById($id);
    }

    /**
     * @param int $formSubmissionId
     * @return array
     */
    public function getAllShareholdings($formSubmissionId)
    {
        return $this->fetch(array("form_submission_id" => $formSubmissionId));
    }

    /**
     * @param array $conditions
     * @return array
     */
    protected function fetch(array $conditions)
    {
        return $this->shareholdingMapper->findAll($conditions);
    }

    /**
     * @param EntityInterface $entity
     * @return int
     */
    public function save(EntityInterface $entity)
    {
        $this->shareholdingMapper->save($entity);
        return $entity->getId();
    }

    /**
     * @param EntityInterface $entity
     */
    public function delete(EntityInterface $entity)
    {
        $this->shareholdingMapper->delete($entity);
    }

}
