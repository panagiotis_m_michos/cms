<?php

namespace Repositories\Nodes;

use IProduct;
use Utils\Text\Parser\Filters\Interfaces\IProductRepository;

class ProductRepository extends Repository_Abstract implements IProductRepository
{
    /**
     * @param int $productId
     * @return IProduct|null
     */
    public function getProductById($productId)
    {
        $node = $this->nodeAccessor->getNodeById($productId);

        if ($node && !($node instanceof IProduct)) {
            $node = NULL;
        }

        return $node;
    }

    /**
     * @return array
     */
    public function getProductsIdsWithIdCheckRequired()
    {
        return $this->nodeAccessor->getNodesIdsByProperty('isIdCheckRequired', 1);
    }

    /**
     * @param int $productId
     * @return float|null
     */
    public function getProductPrice($productId)
    {
        $product = $this->getProductById($productId);

        return $product ? $product->getPrice() : NULL;
    }

    /**
     * @param int $productId
     * @return int
     */
    public function getProductCashBackAmount($productId)
    {
        $product = $this->getProductById($productId);

        return $product ? $product->getCashBackAmount() : 0;
    }
}
