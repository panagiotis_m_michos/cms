<?php

namespace Repositories\Nodes;

use IPackage;
use Package;

class PackageRepository extends Repository_Abstract
{

    /**
     * @param int $packageId
     * @return IPackage|Package|null
     */
    public function getPackageById($packageId)
    {
        $node = $this->nodeAccessor->getNodeById($packageId);

        if ($node && !($node instanceof IPackage)) {
            $node = NULL;
        }

        return $node;
    }
}
