<?php

namespace Repositories\Nodes;

use DibiConnection;
use Exceptions\Business\NodeException;
use FNode;
use Nette\Object;
use ReflectionClass;

class NodeAccessor extends Object
{
    /**
     * @var DibiConnection
     */
    private $connection;

    /**
     * @param DibiConnection $connection
     */
    public function __construct(DibiConnection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @return DibiConnection
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * @param int $nodeId
     * @return FNode|NULL
     */
    public function getNodeById($nodeId)
    {
        $node = NULL;

        try {
            $node = $this->tryGetNodeById($nodeId);
        } catch (NodeException $e) {
            $node = NULL;
        }

        return $node;
    }

    /**
     * @param int $nodeId
     * @return FNode|NULL
     */
    public function tryGetNodeById($nodeId)
    {
        $node = NULL;

        $controllerName = $this->getAdminController($nodeId);
        if ($controllerName) {
            $className = $this->getClassName($controllerName);
            $node = new $className($nodeId);
        }

        return $node;
    }

    /**
     * @param string $name
     * @param mixed $value
     * @return array
     */
    public function getNodesIdsByProperty($name, $value)
    {
        return $this->connection->select('nodeId')
            ->from(TBL_PROPERTIES)
            ->where('name = %s', $name)
            ->and('value = %s', $value)
            ->fetchPairs();
    }

    /**
     * @param int $nodeId
     * @return string
     */
    private function getAdminController($nodeId)
    {
        return $this->connection
            ->select('admin_controler')
            ->from(TBL_NODES)
            ->where('node_id=%i', $nodeId)
            ->fetchSingle();
    }

    /**
     * @param string $controllerName
     * @return string
     */
    private function getClassName($controllerName)
    {
        $class = new ReflectionClass($controllerName);
        $handleObjectProperty = $class->getStaticPropertyValue('handleObject', NULL);
        return $handleObjectProperty ? $handleObjectProperty : 'FNode';
    }
}