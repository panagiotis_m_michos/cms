<?php

namespace Repositories\Nodes;

use FNode;

class NodeRepository extends Repository_Abstract
{
    /**
     * @param int $nodeId
     * @return FNode
     */
    public function getNodeById($nodeId)
    {
        return $this->nodeAccessor->getNodeById($nodeId);
    }
}