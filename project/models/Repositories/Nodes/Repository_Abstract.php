<?php

namespace Repositories\Nodes;

use Nette\Object;

abstract class Repository_Abstract extends Object
{
    /**
     * @var NodeAccessor
     */
    protected $nodeAccessor;

    /**
     * @param NodeAccessor $nodeAccessor
     */
    public function __construct(NodeAccessor $nodeAccessor)
    {
        $this->nodeAccessor = $nodeAccessor;
    }
}