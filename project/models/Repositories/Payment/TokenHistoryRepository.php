<?php

namespace Repositories\Payment;

use Entities\Payment\Token;
use Repositories\BaseRepository_Abstract;

class TokenHistoryRepository extends BaseRepository_Abstract
{
    /**
     * @param Token
     * @return array
     */
    public function getTokenHistory(Token $token)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('h')
            ->from('Entities\Payment\TokenHistory', 'h');
        $qb->where('h.token = ?1');
        $qb->setParameter(1, $token->getId());
        return $qb->getQuery()->getResult();
    }
}

