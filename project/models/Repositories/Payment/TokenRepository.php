<?php

namespace Repositories\Payment;

use DateTime;
use Entities\Payment\Token;
use Utils\Date;
use Entities\Customer;
use Repositories\BaseRepository_Abstract;
use Doctrine\ORM\Internal\Hydration\IterableResult;

class TokenRepository extends BaseRepository_Abstract
{

    /**
     * @param Customer $customer
     * @return Token|NULL
     */
    public function getCurrentToken(Customer $customer)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('t')
            ->from('Entities\Payment\Token', 't')
            ->where('t.isCurrent = ?1')->setParameter(1, TRUE)
            ->andWhere('t.customer -?2')->setParameter(2, $customer->getId());
        $tokens = $qb->getQuery()->getResult();
        return reset($tokens);
    }

    /**
     * @param string $identifier
     * @return Token
     */
    public function getTokenByIdentifier($identifier)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('t')
            ->from('Entities\Payment\Token', 't')
            ->where('t.identifier = ?1')->setParameter(1, $identifier);
        $token = $qb->getQuery()->getOneOrNullResult();
        return $token;
    }

    /**
     * @param Customer $customer
     * @return Token
     */
    public function getActiveTokenByCustomer(Customer $customer)
    {
        return $this->createQueryBuilder('t')
            ->where('t.customer = :customer')
            ->andWhere('t.cardExpiryDate >= :today')
            ->andWhere('t.sageStatus = :active')
            ->setParameters(
                array(
                    'today' => new DateTime(),
                    'customer' => $customer,
                    'active' => Token::SAGE_STATUS_ACTIVE
                )
            )
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param string $tokenId
     * @return array
     */
    public function getTokenById($tokenId)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('t')
            ->from('Entities\Payment\Token', 't')
            ->where('t.tokenId = ?1')->setParameter(1, $tokenId);
        $token = $qb->getQuery()->getResult();
        return $token;
    }

    /**
     * @param Date $fromDate
     * @param Date $toDate
     * @return IterableResult 
     */
    public function getTokensForCardReminders(Date $fromDate, Date $toDate)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select("t")
            ->from('Entities\Payment\Token', "t")
            ->andWhere("t.cardExpiryDate BETWEEN :from AND :to")
            ->andWhere("t.sageStatus  = :active")
            ->setParameter('from', $fromDate)
            ->setParameter('to', $toDate)
            ->setParameter('active', Token::SAGE_STATUS_ACTIVE);
        $rows = $qb->getQuery()->iterate();
        return $rows;
    }

    /**
     * @return IterableResult
     */
    public function getUsCardsWithNoBillingState()
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select("t")
            ->from('Entities\Payment\Token', "t")
            ->where("t.billingCountry = 'US'")
            ->andWhere("t.billingState IS NULL")
            ->orWhere("t.billingState = ''");
        $rows = $qb->getQuery()->iterate();
        return $rows;
    }

    /**
     * @return IterableResult
     */
    public function getAllTokens()
    {
        return $this->_em->createQueryBuilder()
            ->select("t")
            ->from('Entities\Payment\Token', "t")
            ->getQuery()
            ->iterate();
    }
}
