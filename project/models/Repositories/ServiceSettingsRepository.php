<?php

namespace Repositories;

use Doctrine\ORM\Internal\Hydration\IterableResult;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Entities\Company;
use Entities\Customer;
use Entities\Payment\Token;
use Entities\Service;
use Entities\ServiceSettings;

class ServiceSettingsRepository extends BaseRepository_Abstract
{

    /**
     * @param Company $company
     * @param string $serviceTypeId
     * @return ServiceSettings|NULL
     */
    public function getSettingsByType(Company $company, $serviceTypeId)
    {
        return $this->findOneBy(['company' => $company, 'serviceTypeId' => $serviceTypeId]);
    }

    /**
     * @param Service $service
     * @return ServiceSettings|NULL
     */
    public function getSettingsByService(Service $service)
    {
        return $this->findOneBy(
            ['company' => $service->getCompany(), 'serviceTypeId' => $service->getServiceTypeId()]
        );
    }

    /**
     * @param Token $token
     * @return ServiceSettings[]
     */
    public function getSettingsByToken(Token $token)
    {
        return $this->findBy(['renewalToken' => $token]);
    }

    /**
     * @param Customer $customer
     * @return IterableResult
     */
    public function getSettingsWithInvalidRenewalTokenByCustomer(Customer $customer)
    {
        return $this->createSettingsWithInvalidRenewalTokenQuery($customer)
            ->getQuery()
            ->iterate();
    }

    /**
     * @param Customer $customer
     * @return IterableResult
     */
    public function hasCustomerServicesWithInvalidRenewalToken(Customer $customer)
    {
        return (bool) $this->createSettingsWithInvalidRenewalTokenQuery($customer)
            ->select('COUNT(s)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param Customer $customer
     * @return QueryBuilder
     */
    private function createSettingsWithInvalidRenewalTokenQuery(Customer $customer)
    {
        $qb = $this->_em->createQueryBuilder();

        return $qb->select('s')
            ->from('Entities\ServiceSettings', 's')
            ->join('s.company', 'c')
            ->join('c.customer', 'cu')
            ->leftJoin('s.renewalToken', 't')
            ->where('cu = :customer')->setParameter('customer', $customer)
            ->andWhere('s.renewalToken IS NOT NULL')
            ->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->isNull('t'),
                    $qb->expr()->lt('t.cardExpiryDate', 'CURRENT_DATE()'),
                    $qb->expr()->neq('t.sageStatus', ':activeStatus')
                )
            )
            ->setParameter('activeStatus', Token::SAGE_STATUS_ACTIVE);
    }

    /**
     * @return IterableResult
     */
    public function getSettingsWithEnabledAutoRenewal()
    {
        $qb = $this->_em->createQueryBuilder();

        return $qb->select('s')
            ->from('Entities\ServiceSettings', 's')
            ->where('s.isAutoRenewalEnabled = TRUE')
            ->getQuery()
            ->iterate();
    }
}
