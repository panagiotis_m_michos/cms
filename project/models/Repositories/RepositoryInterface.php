<?php

namespace Repositories;

use Mapper\MapperInterface;

interface RepositoryInterface
{

    public function __construct(MapperInterface $mapper);
}
