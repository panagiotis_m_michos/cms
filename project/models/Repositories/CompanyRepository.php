<?php

namespace Repositories;

use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Entities\Company as CompanyEntity;
use Entities\Company;
use Entities\CompanyHouse\FormSubmission;
use Entities\Customer as CustomerEntity;
use Doctrine\ORM\Internal\Hydration\IterableResult;
use Doctrine\DBAL\Types\Type;
use DateTime;
use Entities\Customer;
use Search\CompanySearchQuery;
use Search\ICompanyRepository;

class CompanyRepository extends BaseRepository_Abstract implements ICompanyRepository
{

    /**
     * @param DateTime $dtc
     * @return IterableResult
     */
    public function getLaterThan(DateTime $dtc)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('i')->from('Entities\Company', 'i');
        $qb->where('i.dtc > ?1')->setParameter(1, $dtc, Type::DATETIME);
        $iterator = $qb->getQuery()->iterate();
        return $iterator;
    }

    /**
     * @param CustomerEntity $customer
     * @return IterableResult
     */
    public function getCustomerServiceCompanies(CustomerEntity $customer)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('c')->from('Entities\Company', 'c');
        $qb->join('Entities\Service', 's', 'WITH', 's.company=c.companyId');
        $qb->where('c.customer = ?1')->setParameter(1, $customer);
        $qb->andWhere('c.companyNumber IS NOT NULL');
        $qb->having('COUNT(s.serviceId) > 0');
        $qb->groupBy('s.company');
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param CustomerEntity $customer
     * @return CompanyEntity[]
     */
    public function getUnsubmittedCompanies(CustomerEntity $customer)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('c')->from('Entities\Company', 'c');
        $qb->innerJoin('c.formSubmissions', 'f');
        $qb->where('f INSTANCE OF :identifier');
        $qb->andWhere('f.response IS NULL ');
        $qb->andWhere('c.customer = :customer');
        $qb->setParameter('identifier', FormSubmission::TYPE_COMPANY_INCORPORATION);
        $qb->setParameter('customer', $customer);
        $qb->orderBy('c.companyId', 'DESC');
        return $qb->getQuery()->getResult();

    }

    /**
     * @return Company[]
     */
    public function getCompaniesWithActiveServices()
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('c')->from('Entities\Company', 'c');
        $qb->innerJoin('c.services', 's');
        $qb->where('s.dtExpires >= ?1');
        $qb->andWhere('s.dtExpires <= ?2');
        $qb->setParameter('1', new DateTime('-29 days 00:00:00'));
        $qb->setParameter('2', new DateTime('29 days 23:59:59'));
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param Customer $customer
     * @param bool $excludeDeleted
     * @return array
     */
    public function getCustomerIncorporatedCompanies(Customer $customer, $excludeDeleted = FALSE)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('c')->from('Entities\Company', 'c');
        $qb->where('c.customer = :customer');
        $qb->andWhere('c.companyNumber IS NOT NULL AND c.companyNumber != :companyNumber');
        $qb->setParameter('customer', $customer);
        $qb->setParameter('companyNumber', '');
        if ($excludeDeleted) {
            $qb->andWhere('c.deleted = FALSE');
        }
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param CompanyEntity $company
     * @return FormSubmission|NULL
     */
    public function getLastCompanyIncorporationSubmission(CompanyEntity $company)
    {
        $qb = $this->_em->createQueryBuilder()
            ->select('f')
            ->from('Entities\CompanyHouse\FormSubmission', 'f')
            ->where('f.company = :company')->setParameter('company', $company)
            ->andWhere('f INSTANCE OF :type')->setParameter('type', FormSubmission::TYPE_COMPANY_INCORPORATION)
            ->orderBy('f.dtm', 'DESC')
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param CustomerEntity $customer
     * @param CompanySearchQuery $companySearchQuery
     * @return QueryBuilder
     */
    public function filterResults(CustomerEntity $customer, CompanySearchQuery $companySearchQuery)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('c')->from('Entities\Company', 'c');
        $qb->where('c.companyNumber IS NOT NULL AND c.companyNumber != :companyNumber');
        $qb->setParameter('companyNumber', '');
        $qb->andWhere('c.customer = :customer');
        $qb->setParameter('customer', $customer);
        $qb->andWhere('c.deleted = 0');
        $qb->andWhere('c.hidden = 0');
        if ($companySearchQuery->getTerm()) {
            $qb->andWhere('c.companyName LIKE :term OR c.companyNumber LIKE :term');
            $qb->setParameter('term', "%" . mb_convert_encoding($companySearchQuery->getTerm(), 'UTF-8', 'CP1252') . "%");
        }
        if ($companySearchQuery->isOnlyActive()) {
            $qb->andWhere('c.companyStatus = :status');
            $qb->setParameter('status', 'Active');
        }
        if ($companySearchQuery->isAccounts()) {
            if ($companySearchQuery->getDateFrom()) {
                $qb->andWhere('c.accountsNextDueDate >= :fromDate');
                $qb->setParameter('fromDate', $companySearchQuery->getDateFrom());
            }
            if ($companySearchQuery->getDateTo()) {
                $qb->andWhere('c.accountsNextDueDate <= :toDate');
                $qb->setParameter('toDate', $companySearchQuery->getDateTo());
            }
        }
        if ($companySearchQuery->isReturns()) {
            if ($companySearchQuery->getDateFrom()) {
                $qb->andWhere('c.returnsNextDueDate >= :fromDate');
                $qb->setParameter('fromDate', $companySearchQuery->getDateFrom());
            }
            if ($companySearchQuery->getDateTo()) {
                $qb->andWhere('c.returnsNextDueDate <= :toDate');
                $qb->setParameter('toDate', $companySearchQuery->getDateTo());
            }
        }
        if ($companySearchQuery->getLimit() && $companySearchQuery->getLimit() != 0) {
            $qb->setMaxResults($companySearchQuery->getLimit());
        }
        if ($companySearchQuery->getOffset() > 0) {
            $qb->setFirstResult($companySearchQuery->getOffset());
        }
        if ($companySearchQuery->getSortingDirection() && $companySearchQuery->getSortingField()) {
            $qb->orderBy("c.".$companySearchQuery->getSortingField(), $companySearchQuery->getSortingDirection());
        }
        return $qb;
    }

    /**
     * @param CustomerEntity $customer
     * @param CompanySearchQuery $companySearchQuery
     * @return QueryBuilder
     */
    public function findByQuery(CustomerEntity $customer, CompanySearchQuery $companySearchQuery)
    {
        $qb = $this->filterResults($customer, $companySearchQuery);
        return $qb->getQuery()->iterate();
    }

    /**
     * @param CustomerEntity $customer
     * @param CompanySearchQuery $companySearchQuery
     * @return int
     */
    public function countByQuery(CustomerEntity $customer, CompanySearchQuery $companySearchQuery)
    {
        $qb = $this->filterResults($customer, $companySearchQuery);
        $qb->select('count(c)');
        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param CompanyEntity $company
     */
    public function refresh(CompanyEntity $company)
    {
        $this->_em->refresh($company);
    }

    /**
     * @param Customer $customer
     * @param int $companyId
     * @return Company
     * @throws NoResultException
     */
    public function getCustomerCompany(CustomerEntity $customer, $companyId)
    {
        $company = $this->findOneBy(['customer' => $customer, 'companyId' => $companyId]);
        if (!$company) {
            throw new NoResultException();
        }
        return $company;
    }
}
