<?php

namespace Payment;

use Entities\Customer;
use Services\Payment\TokenService;
use ServiceSettingsModule\Services\ServiceSettingsService;
use SessionNamespace;

class AutoRenewalReenabler
{
    /**
     * @var ServiceSettingsService
     */
    private $serviceSettingsService;

    /**
     * @var TokenService
     */
    private $tokenService;

    /**
     * @var SessionNamespace
     */
    private $reenableSession;

    /**
     * @param ServiceSettingsService $serviceSettingsService
     * @param TokenService $tokenService
     * @param SessionNamespace $reenableSession
     */
    public function __construct(
        ServiceSettingsService $serviceSettingsService,
        TokenService $tokenService,
        SessionNamespace $reenableSession
    )
    {
        $this->serviceSettingsService = $serviceSettingsService;
        $this->tokenService = $tokenService;
        $this->reenableSession = $reenableSession;
    }

    /**
     * @param Customer $customer
     * @return bool
     */
    public function hasServicesToReenable(Customer $customer)
    {
        return $this->reenableSession->shouldReenable && $this->serviceSettingsService->hasCustomerServicesWithInvalidRenewalToken($customer);
    }

    /**
     * @param Customer $customer
     */
    public function reenable(Customer $customer)
    {
        if ($this->reenableSession->shouldReenable) {
            $this->reenableSession->remove();
            $token = $this->tokenService->getTokenByCustomer($customer);
            $this->serviceSettingsService->reenableAutoRenewalByCustomer($customer, $token);
        }
    }
}
