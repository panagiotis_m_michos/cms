<?php

/**
 * @deprecated Use PaymentModule\Responses\PaymentResponse
 */
class PaymentResponse extends Object
{

    /**
     * @var string 
     */
    private $typeId;
    
    /**
     * @var string
     */
    private $paypalTransactionId;
    
    /**
     * @var string 
     */
    private $paypalTokenId;

    /**
     * @var string
     */
    private $paymentHolderFirstName;
    
    /**
     * @var string 
     */
    private $paymentHolderLastName;
    
    /**
     * @var string 
     */
    private $paymentHolderEmail;
    
    /**
     * @var string 
     */
    private $paymentHolderAddressStreet;
    
    /**
     * @var string 
     */
    private $paymentHolderAddressCity;
    
    /**
     * @var string 
     */
    private $paymentHolderAddressState;
    
    /**
     * @var string 
     */
    private $paymentHolderAddressPostCode;
    
    /**
     * @var string 
     */
    private $paymentHolderAddressCountry;
    
    /**
     * @var string 
     */
    private $paymentTransactionTime;
    
    /**
     * @var string 
     */
    private $paymentCardNumber;
    
    /**
     * @var string 
     */
    private $paymentTransactionDetails;
    
    /**
     * @var string 
     */
    private $paymentOrderCode;
    
    /**
     * @var string 
     */
    private $paymentVpsAuthCode;
    
    /**
     * @var string 
     */
    private $paymentVendorTXCode;

    /**
     * @return string
     */
    public function getPaymentCardNumber()
    {
        return $this->paymentCardNumber;
    }

    /**
     * @param string $paymentCardNumber
     */
    public function setPaymentCardNumber($paymentCardNumber)
    {
        $this->paymentCardNumber = $paymentCardNumber;
    }

    /**
     * @return string
     */
    public function getPaymentVendorTXCode()
    {
        return $this->paymentVendorTXCode;
    }

    /**
     * @param string $paymentVendorTXCode
     */
    public function setPaymentVendorTXCode($paymentVendorTXCode)
    {
        $this->paymentVendorTXCode = $paymentVendorTXCode;
    }
    
    /**
     * @return string
     */
    public function getPaymentVpsAuthCode()
    {
        return $this->paymentVpsAuthCode;
    }

    /**
     * @param string $paymentVpsAuthCode
     */
    public function setPaymentVpsAuthCode($paymentVpsAuthCode)
    {
        $this->paymentVpsAuthCode = $paymentVpsAuthCode;
    }
    
    /**
     * @return string
     */
    public function getPaymentOrderCode()
    {
        return $this->paymentOrderCode;
    }

    /**
     * @param string $paymentOrderCode
     */
    public function setPaymentOrderCode($paymentOrderCode)
    {
        $this->paymentOrderCode = $paymentOrderCode;
    }
    
    
    /**
     * @return string
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     * @param string $typeId
     */
    public function setTypeId($typeId)
    {
        $this->typeId = $typeId;
    }
    
    /**
     * @return string
     */
    public function getPaymentTransactionDetails()
    {
        return $this->paymentTransactionDetails;
    }

    /**
     * @param string $paymentTransactionDetails
     */
    public function setPaymentTransactionDetails($paymentTransactionDetails)
    {
        $this->paymentTransactionDetails = $paymentTransactionDetails;
    }

    /**
     * @return string
     */
    public function getPaymentHolderFirstName()
    {
        return $this->paymentHolderFirstName;
    }

    /**
     * @param string $paymentHolderFirstName
     */
    public function setPaymentHolderFirstName($paymentHolderFirstName)
    {
        $this->paymentHolderFirstName = $paymentHolderFirstName;
    }
    
    /**
     * @return string
     */
    public function getPaymentHolderLastName()
    {
        return $this->paymentHolderLastName;
    }

    /**
     * @param string $paymentHolderLastName
     */
    public function setPaymentHolderLastName($paymentHolderLastName)
    {
        $this->paymentHolderLastName = $paymentHolderLastName;
    }
    
    /**
     * @return string
     */
    public function getPaymentHolderEmail()
    {
        return $this->paymentHolderEmail;
    }

    /**
     * @param string $paymentHolderEmail
     */
    public function setPaymentHolderEmail($paymentHolderEmail)
    {
        $this->paymentHolderEmail = $paymentHolderEmail;
    }
    
    /**
     * @return string
     */
    public function getPaymentHolderAddressStreet()
    {
        return $this->paymentHolderAddressStreet;
    }

    /**
     * @param string $paymentHolderAddressStreet
     */
    public function setPaymentHolderAddressStreet($paymentHolderAddressStreet)
    {
        $this->paymentHolderAddressStreet = $paymentHolderAddressStreet;
    }
    
    /**
     * @return string
     */
    public function getPaymentHolderAddressCity()
    {
        return $this->paymentHolderAddressCity;
    }

    /**
     * @param string $paymentHolderAddressCity
     */
    public function setPaymentHolderAddressCity($paymentHolderAddressCity)
    {
        $this->paymentHolderAddressCity = $paymentHolderAddressCity;
    }
    
    /**
     * @return string
     */
    public function getPaymentHolderAddressState()
    {
        return $this->paymentHolderAddressState;
    }

    /**
     * @param string $paymentHolderAddressState
     */
    public function setPaymentHolderAddressState($paymentHolderAddressState)
    {
        $this->paymentHolderAddressState = $paymentHolderAddressState;
    }
    
    /**
     * @return string
     */
    public function getPaymentHolderAddressPostCode()
    {
        return $this->paymentHolderAddressPostCode;
    }

    /**
     * @param string $paymentHolderAddressPostCode
     */
    public function setPaymentHolderAddressPostCode($paymentHolderAddressPostCode)
    {
        $this->paymentHolderAddressPostCode = $paymentHolderAddressPostCode;
    }

    /**
     * @return string
     */
    public function getPaymentHolderAddressCountry()
    {
        return $this->paymentHolderAddressCountry;
    }

    /**
     * @param string $paymentHolderAddressCountry
     */
    public function setPaymentHolderAddressCountry($paymentHolderAddressCountry)
    {
        $this->paymentHolderAddressCountry = $paymentHolderAddressCountry;
    }
    
    
    /**
     * @return string
     */
    public function getPaymentTransactionTime()
    {
        return $this->paymentTransactionTime;
    }

    /**
     * @param string $paymentTransactionTime
     */
    public function setPaymentTransactionTime($paymentTransactionTime)
    {
        $this->paymentTransactionTime = $paymentTransactionTime;
    }
    
    
    /**
     * @return string
     */
    public function getPaypalTransactionId()
    {
        return $this->paypalTransactionId;
    }

    /**
     * @param string $paypalTransactionId
     */
    public function setPaypalTransactionId($paypalTransactionId)
    {
        $this->paypalTransactionId = $paypalTransactionId;
    }
    
    /**
     * @return string
     */
    public function getPaypalTokenId()
    {
        return $this->paypalTransactionId;
    }

    /**
     * @param string $paypalTokenId
     */
    public function setPaypalTokenId($paypalTokenId)
    {
        $this->paypalTokenId = $paypalTokenId;
    }
}
