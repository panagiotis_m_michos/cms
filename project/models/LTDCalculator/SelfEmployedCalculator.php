<?php

class SelfEmployedCalculator extends Calculator 
{

    /**
     * @param float $annualProfit this is rounded to two decimals automatically
     */
    public function __construct($annualProfit)
    {
        parent::__construct($annualProfit);
        $this->tax = array(
            (string) self::$taxRateBasic => self::$taxBandBasic,
            (string) self::$taxRateHigher => self::$taxBandAdditional,
            (string) self::$taxRateAdditionalHigher => 0//self::$taxBandAdditional  <
        );

    }

    /**
     * Returns income from which we can calculate tax
     *
     * @return float
     */
    public function getTaxableIncome()
    {

        $reduction = (($this->annualProfit - self::$personalAllowanceThreshLower) > 0)
            ? ($this->annualProfit - self::$personalAllowanceThreshLower) 
            / self::$personalAllowanceReductionRatio
            : 0;

        $personalAllowance = ((self::$personalAllowance - $reduction) < 0)
                ? 0
                : ceil(self::$personalAllowance - $reduction);

        $taxableIncome = (($this->annualProfit - $personalAllowance) < 0)
                ? 0
                : $this->annualProfit - $personalAllowance;

        return $taxableIncome;
    }

    /**
     * @param bool $round
     * @return float
     */
    public function getNationalInsurance($round = self::ROUND)
    {

        $class4NIRange = self::$classNiBandUpper - self::$classNiBandLower;

        $annualProfitsSubLower = $this->annualProfit - self::$classNiBandLower;
        $annualProfitsSubHigher = $this->annualProfit - self::$classNiBandUpper;

        $class4Lower = ($annualProfitsSubLower < 0)
            ? 0
            : min($annualProfitsSubLower, $class4NIRange);

        $class4Higher = ($annualProfitsSubHigher < 0)
            ? 0
            : $annualProfitsSubHigher;

        $class4Lower *= self::$classRateBasic;
        $class4Higher *= self::$classRateAdditional;

        $totalNIPayable = $class4Lower + $class4Higher + self::$selfEmployedNIConst;

        return $round ? round($totalNIPayable, 2) : $totalNIPayable;
    }

    /**
     * Tax payable plus national insurance
     *
     * @param bool $round
     * @return float
     */
    public function getTotalPayable($round = self::ROUND)
    {
        $value = $this->getTaxPayable(!self::ROUND) + $this->getNationalInsurance(!self::ROUND);
        return $round ? round($value, 2) : $value;
    }

    /**
     * Supplied annual profit minus total payable
     *
     * @param bool $round
     * @return float
     */
    public function getNetSpendablePersonalIncome($round = self::ROUND)
    {
        $value = $this->annualProfit - $this->getTotalPayable(!self::ROUND);
        return $round ? round($value, 2) : $value;
    }
}
