<?php

class CompanyCalculator extends Calculator
{

    /**
     * @param float $annualProfit this is rounded to two decimals automatically
     */
    public function __construct($annualProfit)
    {
        parent::__construct($annualProfit);
        $this->tax = array(
            (string) self::$corporationTaxRateLower => self::$corporationTaxProfitLower, 
            (string) self::$corporationTaxRateMarginal => self::$corporationTaxProfitUpper,
            (string) self::$corporationTaxRateMain=> 0 // self::$corporationTaxProfitUpper < 
        );
    }

    /**
     * Returns income from which we can calculate tax
     *
     * @return float
     */
    public function getTaxableIncome()
    {
        if ($this->annualProfit < self::$niBandPrimary) {
            return 0;
        }
        return $this->annualProfit - self::$niBandPrimary;
    }

    /**
     * @param bool $round
     * @return float
     */
    public function getPersonalTaxPayable($round = self::ROUND)
    {
        $dividendAvailable = $this->getTaxableIncome() - $this->getTaxPayable();
        $taxOnNotationalDividend = ($dividendAvailable / (1 - self::$notationalDividendTax)) - $dividendAvailable;       
        $totalDividend = $dividendAvailable + $taxOnNotationalDividend;

        $actualSalary = min($this->annualProfit, self::$niBandPrimary);
        //pr($actualSalary);

        $companyIncome = $totalDividend + $actualSalary;
        //pr($companyIncome);
        $companyReduction = 0;
        if (($companyIncome - self::$personalAllowanceThreshLower) > 0) {
            $x = ($companyIncome - self::$personalAllowanceThreshLower);
            $companyReduction = $x / self::$personalAllowanceReductionRatio;
        }

        $companyPersonalAllowance = 0;
        if ((self::$personalAllowance - $companyReduction) > 0) {
            $x = self::$personalAllowance - $companyReduction;
            $companyPersonalAllowance = min($x, self::$personalAllowance);
        }


        //exit;
        $dividendTaxCalc1 = 0;
        if ($companyPersonalAllowance - $actualSalary > 0) {
            $x = ($companyPersonalAllowance - $actualSalary);
            $dividendTaxCalc1 = $x * self::$notationalDividendTax ;
        }
        //pr($x);
        //pr(self::$notationalDividendTax);
        $dividendTaxDeductible = 0;
        if (($taxOnNotationalDividend-$dividendTaxCalc1) > 0) {
            $dividendTaxDeductible = $taxOnNotationalDividend - $dividendTaxCalc1;
        }

        $companyTaxableIncome = 0;
        if (($companyIncome - $companyPersonalAllowance) > 0) {
            $companyTaxableIncome = $companyIncome - $companyPersonalAllowance;
        }

        $companyTaxable1 = ($actualSalary > $companyPersonalAllowance)
            ? $actualSalary - $companyPersonalAllowance
            : 0;

        $companyTaxable2 = ($companyTaxableIncome > self::$taxBandBasic)
            ? self::$taxBandBasic - $companyTaxable1
            : $companyTaxableIncome;

        $companyBalance1 = (($companyTaxableIncome - $companyTaxable1 - $companyTaxable2) < 0)
            ? 0
            : $companyTaxableIncome - $companyTaxable1 - $companyTaxable2;

        $companyTaxable3 = ((self::$taxBandAdditional - self::$taxBandBasic) < $companyBalance1)
            ? self::$taxBandAdditional - self::$taxBandBasic
            : $companyBalance1;

        $companyTaxable4 = (($companyBalance1 - $companyTaxable3) < 0)
            ? 0
            : $companyBalance1 - $companyTaxable3;

        $companyTaxable1 *= self::$taxRateBasic;
        $companyTaxable2 *= self::$notationalDividendTax;
        $companyTaxable3 *= self::$dividendRateHigher;
        $companyTaxable4 *= self::$dividendRateAdditional;

        $companyTaxPayable = $companyTaxable1 + $companyTaxable2 + $companyTaxable3 + $companyTaxable4;
        $companySubDivTax = -1 * min(array($dividendTaxDeductible, $companyTaxPayable));
        $companyTotalTax = $companyTaxPayable + $companySubDivTax;

        return $round ? round($companyTotalTax, 2) : $companyTotalTax;
    }

    /**
     * Company tax payable plus personal tax payable
     *
     * @param bool $round
     * @return float
     */
    public function getTotalPayable($round = self::ROUND)
    {
        $value = $this->getTaxPayable(!self::ROUND) + $this->getPersonalTaxPayable(!self::ROUND);
        return $round ? round($value, 2) : $value;
    }

    /**
     * Supplied annual profit minus total payable
     *
     * @param bool $round
     * @return float
     */
    public function getNetSpendablePersonalIncome($round = self::ROUND)
    {
        $value = $this->annualProfit - $this->getTotalPayable(!self::ROUND);
        return $round ? round($value, 2) : $value;
    }
}
