<?php

class LTDCalculator 
{

    /** 
     * @var SelfEmployedCalculator 
     */
    private $selfEmployed;

    /** 
     * @var CompanyCalculator 
     */
    private $company;

    /**
     * @param float $annualProfit
     */
    public function __construct($annualProfit)
    {
        $this->selfEmployed = new SelfEmployedCalculator($annualProfit);
        $this->company = new CompanyCalculator($annualProfit);
    }

    /**
     * @return SelfEmployedCalculator
     */
    public function getSelfEmployed()
    {
        return $this->selfEmployed;
    }

    /**
     * @return CompanyCalculator
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return float
     */
    public function getSaving()
    {
        $saving = $this->getCompany()->getNetSpendablePersonalIncome(!Calculator::ROUND)
            - $this->getSelfEmployed()->getNetSpendablePersonalIncome(!Calculator::ROUND);
        return round($saving, 2);	
    }
}
