<?php

abstract class Calculator
{

    const ROUND = TRUE;

    /* following constants are used for personla tax payable */
    protected static $personalAllowance = 11000;

    /**
     * @var int
     */
    protected static $personalAllowanceThreshLower = 100000;

    /**
     * @var int
     */
    protected static $personalAllowanceThreshUpper = 122000;

    /**
     * @var int
     */
    protected static $personalAllowanceReductionRatio = 2;

    /**
     * @var int
     */
    protected static $selfEmployedNIConst = 145.6;

    /**
     * @var float
     */
    protected static $notationalDividendTax = 0.075;

    /**
     * @var int
     */
    protected static $taxBandPersonalAllowance = 11000;

    /**
     * @var int
     */
    protected static $taxBandBasic = 32001;

    /**
     * @var int
     */
    protected static $taxBandAdditional = 150000;

    /**
     * @var int
     */
    protected static $niBandPrimary = 8060;

    /**
     * @var int
     */
    protected static $niBandSecondary = 8112;

    /**
     * @var int
     */
    protected static $niBandUpper = 43000;

    /**
     * @var float
     */
    protected static $taxRateBasic = 0.2;

    /**
     * @var float
     */
    protected static $taxRateHigher = 0.4;

    /**
     * @var float
     */
    protected static $taxRateAdditionalHigher = 0.45;

    /**
     * @var float
     */
    protected static $dividendRateHigher = 0.325;

    /**
     * @var float
     */
    protected static $dividendRateAdditional = 0.381;

    /**
     * @var int
     */
    protected static $classNiBandLower = 8060;

    /**
     * @var int
     */
    protected static $classNiBandUpper = 43000;

    /**
     * @var float
     */
    protected static $classRateBasic = 0.09;

    /**
     * @var float
     */
    protected static $classRateAdditional = 0.02;

    /**
     * @var float
     */
    protected static $corporationTaxRateMain = 0.2;

    /**
     * @var float
     */
    protected static $corporationTaxRateLower = 0.2;

    /**
     * @var float
     */
    protected static $corporationTaxRateMarginal = 0.2;

    /**
     * @var int
     */
    protected static $corporationTaxProfitLower = 300000;

    /**
     * @var int
     */
    protected static $corporationTaxProfitUpper = 1500000;


    /**
     * @var float
     */
    protected $annualProfit;

    /**
     *
     * @var array   key is tax rate and value is band. If band is 0, it
     *              indicates the rest.
     *              Example:
     *              if you earn 200.000 then from
     *              the first 37.400 20% will be deducted
     *              from the next 112.600 40% will be deducted
     *              and from the rest (200.000 - (37.400 + 112.600)) 50%
     */
    protected $tax;

    /**
     * @param float|string $annualProfit
     * @throws InvalidArgumentException
     */
    public function __construct($annualProfit)
    {
        if ($annualProfit < 1 || $annualProfit > 10000000000) {
            throw new InvalidArgumentException(sprintf('Profit range `%s` is not supported. Supported range is between %d and %d', $annualProfit, 1, 10000000000));
        }
        /* round to two decimals */
        $this->annualProfit = round($annualProfit, 2);
    }

    /**
     * @param bool $round
     * @return float
     */
    public function getTaxPayable($round = self::ROUND)
    {

        $balance = $this->getTaxableIncome();
        if ($balance == 0) {
            return 0;
        }

        /* key = tax rate, value = tax band; key 0 indicates the rest */
        $totalTax = 0;
        $taxableToDay = 0;
        foreach ($this->tax as $rate => $band) {

            $taxToApply = $band - $taxableToDay;
            /* no remaining balance, or band is 0 (0 = the rest) */
            if (($balance - $taxToApply) <= 0 || $band == 0) {
                /* add to total tax */
                $totalTax += $balance * $rate;
                break;
            }

            /* add to total tax */
            $totalTax += $taxToApply * $rate;
            /* decrement balance */
            $balance -= $taxToApply;
            $taxableToDay += $taxToApply;
        }

        /* round to two decimals */
        return $round ? round($totalTax, 2) : $totalTax;
    }

    abstract public function getTaxableIncome();

    /**
     * @return float
     */
    public function getAnnualProfit()
    {
        return $this->annualProfit;
    }

}
