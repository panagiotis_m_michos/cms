<?php

namespace Basket;

use DateTime;
use Entities\Company;
use Entities\Service;
use Exceptions\Technical\InvalidArgumentException;
use IPackage;
use Nette\Object;

class PackageToPackageUpgradeNotification extends Object
{
    /**
     * @var IPackage
     */
    private $package;

    /**
     * @var Service
     */
    private $currentService;

    /**
     * @var Company
     */
    private $company;

    /**
     * @param IPackage $package
     * @param Service $currentService
     * @throws InvalidArgumentException
     */
    public function __construct(IPackage $package, Service $currentService)
    {
        $this->package = $package;
        $this->currentService = $currentService;
        $this->company = $currentService->getCompany();

        if (!$this->company) {
            throw InvalidArgumentException::companyForServiceDoesNotExist($this->currentService);
        }
    }

    /**
     * @return string
     */
    public function getPackageName()
    {
        return $this->package->getLngTitle();
    }

    /**
     * @return string
     */
    public function getPackageCompanyName()
    {
        return $this->company->getCompanyName();
    }

    /**
     * @return string
     */
    public function getServiceName()
    {
        return $this->currentService->getServiceName();
    }

    /**
     * @return DateTime
     */
    public function getServiceExpiresOn()
    {
        return $this->currentService->getDtExpires();
    }

    /**
     * @return bool
     */
    public function isServiceCompanyIncorporated()
    {
        return $this->company->isIncorporated();
    }
}
