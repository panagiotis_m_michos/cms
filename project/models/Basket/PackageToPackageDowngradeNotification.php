<?php

namespace Basket;

use Entities\Company;
use Entities\Service;
use Exceptions\Technical\InvalidArgumentException;
use IPackage;
use Nette\Object;
use Product;
use DateTime;

class PackageToPackageDowngradeNotification extends Object
{
    /**
     * @var IPackage
     */
    private $package;

    /**
     * @var Service
     */
    private $currentService;

    /**
     * @var Company
     */
    private $company;

    /**
     * @var bool
     */
    private $isCompanyIncorporationPending;

    /**
     * @param IPackage $package
     * @param Service $currentService
     * @param bool $isCompanyIncorporationPending
     * @throws InvalidArgumentException
     */
    public function __construct(IPackage $package, Service $currentService, $isCompanyIncorporationPending)
    {
        $this->package = $package;
        $this->currentService = $currentService;
        $this->isCompanyIncorporationPending = $isCompanyIncorporationPending;
        $this->company = $this->currentService->getCompany();

        if (!$this->company) {
            throw InvalidArgumentException::companyForServiceDoesNotExist($this->currentService);
        }
    }

    /**
     * @return string
     */
    public function getPackageName()
    {
        return $this->package->getLngTitle();
    }

    /**
     * @return string
     */
    public function getServiceName()
    {
        return $this->currentService->getServiceName();
    }

    /**
     * @return DateTime
     */
    public function getServiceExpiresOn()
    {
        return $this->currentService->getDtExpires();
    }

    /**
     * @return int
     */
    public function getServiceCompanyId()
    {
        return $this->company->getCompanyId();
    }

    /**
     * @return string
     */
    public function getServiceCompanyName()
    {
        return $this->company->getCompanyName();
    }

    /**
     * @return bool
     */
    public function isServiceCompanyIncorporationPending()
    {
        return $this->isCompanyIncorporationPending;
    }

    /**
     * @return bool
     */
    public function isServiceCompanyIncorporated()
    {
        return $this->company->isIncorporated();
    }
}
