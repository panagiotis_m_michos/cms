<?php

namespace Basket;

use IPackage;
use Models\View\CompanyView;
use Models\View\ServiceView;
use Nette\Object;

class ProductToPackageUpgradeNotification extends Object
{
    /**
     * @var IPackage
     */
    private $package;

    /**
     * @var ServiceView[]
     */
    private $serviceViews;

    /**
     * @var CompanyView
     */
    private $companyView;

    /**
     * @param IPackage $package
     * @param CompanyView $companyView
     * @param ServiceView[] $serviceViews
     */
    public function __construct(IPackage $package, CompanyView $companyView, array $serviceViews)
    {
        $this->package = $package;
        $this->companyView = $companyView;
        $this->serviceViews = $serviceViews;
    }

    /**
     * @return int
     */
    public function getUpgradedProductsCount()
    {
        return count($this->serviceViews);
    }

    /**
     * @return string
     */
    public function getPackageName()
    {
        return $this->package->getLngTitle();
    }

    /**
     * @return array
     */
    public function getProducts()
    {
        $products = [];
        foreach ($this->serviceViews as $service) {
            $products[] = $this->package->getIncludedProduct($service->getServiceTypeId());
        }
        return $products;
    }

    /**
     * @return array
     */
    public function getServices()
    {
        return $this->serviceViews;
    }

    /**
     * @return string
     */
    public function getServicesCompanyName()
    {
        return $this->companyView->getCompanyName();
    }

    /**
     * @return bool
     */
    public function isServiceCompanyIncorporated()
    {
        return $this->companyView->isCompanyIncorporated();
    }
}
