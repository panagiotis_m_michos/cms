<?php

namespace Basket;

use Entities\Company;
use Entities\Service;
use Exceptions\Technical\InvalidArgumentException;
use Nette\Object;
use Product;
use DateTime;

class PackageToProductDowngradeNotification extends Object
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @var Service
     */
    private $currentService;

    /**
     * @var bool
     */
    private $isCompanyIncorporationPending;

    /**
     * @var Company
     */
    private $company;

    /**
     * @param Product $product
     * @param Service $currentService
     * @param bool $isCompanyIncorporationPending
     * @throws InvalidArgumentException
     */
    public function __construct(Product $product, Service $currentService, $isCompanyIncorporationPending)
    {
        $this->product = $product;
        $this->currentService = $currentService;
        $this->isCompanyIncorporationPending = $isCompanyIncorporationPending;
        $this->company = $this->currentService->getCompany();

        if (!$this->company) {
            throw InvalidArgumentException::companyForServiceDoesNotExist($this->currentService);
        }
    }

    /**
     * @return string
     */
    public function getProductName()
    {
        return $this->product->getLngTitle();
    }

    /**
     * @return string
     */
    public function getServiceName()
    {
        return $this->currentService->getServiceName();
    }

    /**
     * @return DateTime
     */
    public function getServiceExpiresOn()
    {
        return $this->currentService->getDtExpires();
    }

    /**
     * @return int
     */
    public function getServiceCompanyId()
    {
        return $this->company->getCompanyId();
    }

    /**
     * @return string
     */
    public function getServiceCompanyName()
    {
        return $this->company->getCompanyName();
    }

    /**
     * @return bool
     */
    public function isServiceCompanyIncorporationPending()
    {
        return $this->isCompanyIncorporationPending;
    }

    /**
     * @return bool
     */
    public function isServiceCompanyIncorporated()
    {
        return $this->company->isIncorporated();
    }
}
