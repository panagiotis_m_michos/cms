<?php

namespace Basket;

use DateTime;
use Models\View\ServiceView;
use Nette\Object;
use Entities\Company as CompanyEntity;

class SuspendedItem extends Object
{
    /**
     * @var CompanyEntity
     */
    private $company;

    /**
     * @var ServiceView
     */
    private $serviceView;

    /**
     * @param CompanyEntity $company
     * @param ServiceView $serviceView
     */
    public function __construct(CompanyEntity $company, ServiceView $serviceView)
    {
        $this->company = $company;
        $this->serviceView = $serviceView;
    }

    /**
     * @return string
     */
    public function getServiceName()
    {
        return $this->serviceView->getServiceType();
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->company->getCompanyName();
    }

    /**
     * @return DateTime
     */
    public function getServiceExpiresOn()
    {
        return $this->serviceView->getDtExpires();
    }
}
