<?php

namespace ValueObject;

use Nette\Object;

class VoApiConfig extends Object
{
    /**
     * @var string
     */
    private $baseUrl;

    /**
     * @var BasicAuthCredentials
     */
    private $basicAuthCredentials;

    /**
     * @param string $baseUrl
     * @param BasicAuthCredentials $basicAuthCredentials
     */
    public function __construct($baseUrl, BasicAuthCredentials $basicAuthCredentials)
    {
        $this->baseUrl = $baseUrl;
        $this->basicAuthCredentials = $basicAuthCredentials;
    }

    /**
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * @return BasicAuthCredentials
     */
    public function getBasicAuthCredentials()
    {
        return $this->basicAuthCredentials;
    }
}