<?php

namespace Console\Commands\Import\ServiceAddress;

use CHXmlGateway;
use CompaniesHouse\Data\Data;
use CSVParser\Mapper\ColumnMapper;
use CSVParser\Parser\Parser;
use Services\CustomerService;
use Doctrine\ORM\EntityManager;
use ILogger;
use Import\Products\ServiceAddress\Importer\Importer;
use Import\Products\ServiceAddress\Parser\ItemFactory;
use Loggers\NikolaiLogger;
use Services\CompanyService;
use Services\OrderService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ProductsCommand extends Command
{
    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * @var NikolaiLogger
     */
    private $logger;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var ColumnMapper
     */
    private $columnMapper;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var OrderService
     */
    private $orderService;

    /**
     * @var CustomerService
     */
    private $customerService;

    /**
     * @var ItemFactory
     */
    private $itemFactory;

    /**
     * @var Data
     */
    private $chData;

    /**
     * @param EntityManager $entityManager
     * @param NikolaiLogger $logger
     * @param ColumnMapper $columnMapper
     * @param CompanyService $companyService
     * @param OrderService $orderService
     * @param CustomerService $customerService
     * @param ItemFactory $itemFactory
     * @param Data $chData
     * @param string|NULL $name
     */
    public function __construct(
        EntityManager $entityManager,
        NikolaiLogger $logger,
        ColumnMapper $columnMapper,
        CompanyService $companyService,
        OrderService $orderService,
        CustomerService $customerService,
        ItemFactory $itemFactory,
        Data $chData,
        $name = NULL
    )
    {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
        $this->columnMapper = $columnMapper;
        $this->companyService = $companyService;
        $this->orderService = $orderService;
        $this->customerService = $customerService;
        $this->itemFactory = $itemFactory;
        $this->chData = $chData;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('import:serviceAddressProducts')
            ->setDescription('Import service address products from CSV')
            ->addArgument(
                'filePath',
                InputArgument::REQUIRED,
                'CSV file to import'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $infoMessage = '<info>Executed ' . $this->getName() . '</info>';
        $this->logMessage($infoMessage, ILogger::DEBUG);

        $parser = new Parser(
            $this->columnMapper,
            $this->itemFactory,
            $input->getArgument('filePath'),
            Parser::SKIP_HEADER
        );

        $items = $parser->getItems();
        $itemsCount = count($items);
        $importer = new Importer(
            $items,
            $this->entityManager,
            $this->companyService,
            $this->orderService,
            $this->customerService,
            $this->chData
        );

        $validationMessage = '<comment>Validating...</comment>';
        $this->logMessage($validationMessage, ILogger::DEBUG);
        $validationErrors = $importer->validate();

        if ($validationErrors) {
            $importErrorMessage = '<info>Service addresses were not imported because of errors <comment>(fix the errors and try again)</comment>';
            $this->logMessage($importErrorMessage, ILogger::WARNING);
            $this->printErrors($validationErrors);
            $doneMessage = '<info>Done with errors.</info>';
        } else {
            $validationMessage = '<comment>Validation OK. Importing now...</comment>';
            $this->logMessage($validationMessage, ILogger::DEBUG);

            $importer->import();
            $importErrors = $importer->getErrors();
            if ($importErrors) {
                $this->printErrors($importErrors);
                $doneMessage = sprintf('<info>Done with errors.</info>');
            } else {
                $doneMessage = sprintf(
                    '<info>Done.</info> <comment>%d</comment> <info>service addresses imported',
                    $itemsCount
                );
            }
        }

        $this->logMessage($doneMessage, ILogger::DEBUG);
    }

    /**
     * @param array $errors
     */
    private function printErrors($errors)
    {
        foreach ($errors as $error) {
            $errorMessage = '<error>' . $error . '</error>';
            $this->logMessage($errorMessage, ILogger::WARNING);
        }
    }

    /**
     * @param string $message
     * @param int $level
     */
    private function logMessage($message, $level = ILogger::DEBUG)
    {
        $this->output->writeln($message);
        $this->logger->logMessage($level, strip_tags($message));
    }
}
