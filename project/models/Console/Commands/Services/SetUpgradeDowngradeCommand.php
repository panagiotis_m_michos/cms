<?php

namespace Console\Commands\Services;

use DateTime;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Entities\Company;
use Entities\OrderItem;
use Doctrine\ORM\EntityManager;
use Entities\Service;
use Exception;
use ILogger;
use Loggers\NikolaiLogger;
use Services\ServiceService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SetUpgradeDowngradeCommand extends Command
{
    /**
     * @var NikolaiLogger
     */
    private $logger;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var ServiceService
     */
    private $serviceService;

    /**
     * @param EntityManager $entityManager
     * @param NikolaiLogger $logger
     * @param ServiceService $serviceService
     * @param NULL|string $name
     */
    public function __construct(
        EntityManager $entityManager,
        NikolaiLogger $logger,
        ServiceService $serviceService,
        $name = NULL
    )
    {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
        $this->serviceService = $serviceService;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('services:setUpgradeDowngrade')
            ->setDescription('Set status for upgraded/downgrade services')
            ->addOption('dry-run', NULL, InputOption::VALUE_NONE, 'Simulate execution if this command');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $infoMessage = '<info>Execution started: ' . $this->getName() . '</info>';
        $this->logMessage($output, $infoMessage);

        if ($input->getOption('dry-run')) {
            $this->logMessage($output, "<info>Dry-run flag set</info>");
        }

        //encapsulate everything in transaction
        $this->entityManager->beginTransaction();

        try {
            $this->processWrongOrders($input, $output);
            $this->processProductToPackageUpgrades($input, $output);
            $this->processPackageToPackageUpgrades($input, $output);
            $this->processPackageToPackageDowngrades($input, $output);
            $this->processPackageToProductDowngrades($input, $output);

            $this->entityManager->commit();
        } catch (Exception $e) {
            $this->entityManager->rollback();
            throw $e;
        }

        $infoMessage = '<info>Execution ended: ' . $this->getName() . '</info>';
        $this->logMessage($output, $infoMessage);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    private function processWrongOrders(InputInterface $input, OutputInterface $output)
    {
        $this->logMessage($output, "--- Processing wrong orders (package + product)", ILogger::DEBUG);

        $services = $this->entityManager->createQueryBuilder()
            ->select('s')
            ->from('\Entities\Service', 's')
            ->where('s.serviceTypeId IN (:types)')->setParameter('types', [Service::TYPE_PACKAGE_PRIVACY, Service::TYPE_PACKAGE_COMPREHENSIVE_ULTIMATE])
            ->andWhere('s.dtExpires >= :now')->setParameter('now', new DateTime, Type::DATE)
            ->orderBy('s.dtStart')
            ->getQuery()
            ->iterate();

        foreach ($services as $serviceData) {
            /** @var Service $service */
            $service = $serviceData[0];

            $limit1 = clone $service->getDtStart();
            $limit2 = clone $service->getDtStart();

            /** @var Service $previousService */
            $previousServices = $this->entityManager->createQueryBuilder()
                ->select('s')
                ->from('\Entities\Service', 's')
                ->where('s.serviceTypeId IN (:types)')->setParameter('types', [Service::TYPE_REGISTERED_OFFICE, Service::TYPE_SERVICE_ADDRESS])
                ->andWhere('s.parent IS NULL')
                ->andWhere('s.dtStart IS NOT NULL')
                ->andWhere('s.dtExpires IS NOT NULL')
                ->andWhere('s.order = :order')->setParameter('order', $service->getOrder())
                ->andWhere('s.company = :company')->setParameter('company', $service->getCompany())
                ->orderBy('s.dtStart')
                ->getQuery()
                ->getResult();

            if ($previousServices) {
                foreach ($previousServices as $previousService) {
                    $this->logMessage(
                        $output,
                        sprintf(
                            'Wrong order %d (%s + %s)',
                            $service->getOrder()->getId(),
                            $previousService->getServiceTypeId(),
                            $service->getServiceTypeId()
                        ),
                        ILogger::WARNING
                    );
                }

                if (!$input->getOption('dry-run')) {
                    $this->serviceService->markAsUpgraded($previousService);
                }
            }
        }

        $this->entityManager->clear();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    private function processProductToPackageUpgrades(InputInterface $input, OutputInterface $output)
    {
        $this->logMessage($output, "--- Processing product to package upgrades", ILogger::DEBUG);

        $services = $this->entityManager->createQueryBuilder()
            ->select('s')
            ->from('\Entities\Service', 's')
            ->where('s.serviceTypeId IN (:types)')->setParameter('types', [Service::TYPE_PACKAGE_PRIVACY, Service::TYPE_PACKAGE_COMPREHENSIVE_ULTIMATE])
            ->andWhere('s.dtExpires >= :now')->setParameter('now', new DateTime, Type::DATE)
            ->andWhere('s.dtStart >= :pivotDate')->setParameter('pivotDate', new DateTime('-2 month'), Type::DATE)
            ->orderBy('s.dtStart')
            ->getQuery()
            ->iterate();

        foreach ($services as $serviceData) {
            /** @var Service $service */
            $service = $serviceData[0];

            $limit1 = clone $service->getDtStart();
            $limit2 = clone $service->getDtStart();

            /** @var Service $previousService */
            $previousServices = $this->entityManager->createQueryBuilder()
                ->select('s')
                ->from('\Entities\Service', 's')
                ->where('s.serviceTypeId IN (:types)')->setParameter('types', [Service::TYPE_REGISTERED_OFFICE, Service::TYPE_SERVICE_ADDRESS])
                ->andWhere('s.parent IS NULL')
                ->andWhere('s.dtStart IS NOT NULL')
                ->andWhere('s.dtExpires BETWEEN :renewDtStart AND :renewDtStart2')
                ->setParameter('renewDtStart', $limit1->modify('-1 day'), Type::DATE)
                ->setParameter('renewDtStart2', $limit2->modify('+1 day'), Type::DATE)
                ->andWhere('s.company = :company')->setParameter('company', $service->getCompany())
                ->orderBy('s.dtStart')
                ->getQuery()
                ->getResult();

            if ($previousServices) {
                foreach ($previousServices as $previousService) {
                    $this->logMessage(
                        $output,
                        sprintf(
                            'Changing service %d to %s (%s -> %s, latest service id: %d)',
                            $previousService->getId(),
                            Service::STATE_UPGRADED,
                            $previousService->getServiceTypeId(),
                            $service->getServiceTypeId(),
                            $service->getId()
                        ),
                        ILogger::WARNING
                    );
                }

                if (!$input->getOption('dry-run')) {
                    $this->serviceService->markAsUpgraded($previousService);
                }
            }
        }

        $this->entityManager->clear();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    private function processPackageToPackageUpgrades(InputInterface $input, OutputInterface $output)
    {
        $this->logMessage($output, "--- Processing package to package upgrades", ILogger::DEBUG);

        $services = $this->entityManager->createQueryBuilder()
            ->select('s')
            ->from('\Entities\Service', 's')
            ->where('s.serviceTypeId = :type')->setParameter('type', Service::TYPE_PACKAGE_COMPREHENSIVE_ULTIMATE)
            ->andWhere('s.parent IS NULL')
            ->andWhere('s.dtExpires >= :now')->setParameter('now', new DateTime, Type::DATE)
            ->orderBy('s.dtStart')
            ->getQuery()
            ->iterate();

        foreach ($services as $serviceData) {
            /** @var Service $service */
            $service = $serviceData[0];

            try {
                $limit1 = clone $service->getDtStart();
                $limit2 = clone $service->getDtStart();

                /** @var Service $previousService */
                $previousService = $this->entityManager->createQueryBuilder()
                    ->select('s')
                    ->from('\Entities\Service', 's')
                    ->where('s.serviceTypeId = :type')->setParameter('type', Service::TYPE_PACKAGE_PRIVACY)
                    ->andWhere('s.parent IS NULL')
                    ->andWhere('s.dtStart IS NOT NULL')
                    ->andWhere('s.dtExpires BETWEEN :renewDtStart AND :renewDtStart2')
                    ->setParameter('renewDtStart', $limit1->modify('-1 day'), Type::DATE)
                    ->setParameter('renewDtStart2', $limit2->modify('+1 year'), Type::DATE)
                    ->andWhere('s.company = :company')->setParameter('company', $service->getCompany())
                    ->orderBy('s.dtStart')
                    ->getQuery()
                    ->getOneOrNullResult();
            } catch (NonUniqueResultException $e) {
                $this->logMessage(
                    $output,
                    "Multiple services found for type {$service->getServiceTypeId()} with dtStart {$service->getDtStart()->format('d/m/Y')} for service {$service->getId()} ",
                    ILogger::WARNING
                );

                continue;
            }

            if ($previousService) {
                $this->logMessage(
                    $output,
                    sprintf(
                        'Changing service %d to %s (%s -> %s, latest service id: %d)',
                        $previousService->getId(),
                        Service::STATE_UPGRADED,
                        $previousService->getServiceTypeId(),
                        $service->getServiceTypeId(),
                        $service->getId()
                    ),
                    ILogger::WARNING
                );

                if (!$input->getOption('dry-run')) {
                    $this->serviceService->markAsUpgraded($previousService);
                }
            }
        }

        $this->entityManager->clear();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    private function processPackageToPackageDowngrades(InputInterface $input, OutputInterface $output)
    {
        $this->logMessage($output, "--- Processing package to package downgrades", ILogger::DEBUG);

        $services = $this->entityManager->createQueryBuilder()
            ->select('s')
            ->from('\Entities\Service', 's')
            ->where('s.serviceTypeId = :type')->setParameter('type', Service::TYPE_PACKAGE_PRIVACY)
            ->andWhere('s.parent IS NULL')
            ->andWhere('s.dtExpires >= :now')->setParameter('now', new DateTime, Type::DATE)
            ->orderBy('s.dtStart')
            ->getQuery()
            ->iterate();

        foreach ($services as $serviceData) {
            /** @var Service $service */
            $service = $serviceData[0];

            try {
                $limit1 = clone $service->getDtStart();
                $limit2 = clone $service->getDtStart();

                /** @var Service $previousService */
                $previousService = $this->entityManager->createQueryBuilder()
                    ->select('s')
                    ->from('\Entities\Service', 's')
                    ->where('s.serviceTypeId = :type')->setParameter('type', Service::TYPE_PACKAGE_COMPREHENSIVE_ULTIMATE)
                    ->andWhere('s.parent IS NULL')
                    ->andWhere('s.dtStart IS NOT NULL')
                    ->andWhere('s.dtExpires BETWEEN :renewDtStart AND :renewDtStart2')
                    ->setParameter('renewDtStart', $limit1->modify('-1 day'), Type::DATE)
                    ->setParameter('renewDtStart2', $limit2->modify('+1 year'), Type::DATE)
                    ->andWhere('s.company = :company')->setParameter('company', $service->getCompany())
                    ->orderBy('s.dtStart')
                    ->getQuery()
                    ->getOneOrNullResult();
            } catch (NonUniqueResultException $e) {
                $this->logMessage(
                    $output,
                    "Multiple services found for type {$service->getServiceTypeId()} with dtStart {$service->getDtStart()->format('d/m/Y')} for service {$service->getId()} ",
                    ILogger::WARNING
                );

                continue;
            }

            if ($previousService) {
                $this->logMessage(
                    $output,
                    sprintf(
                        'Changing service %d to %s (%s -> %s, latest service id: %d)',
                        $previousService->getId(),
                        Service::STATE_DOWNGRADED,
                        $previousService->getServiceTypeId(),
                        $service->getServiceTypeId(),
                        $service->getId()
                    ),
                    ILogger::WARNING
                );

                if (!$input->getOption('dry-run')) {
                    $this->serviceService->markAsDowngraded($previousService);
                }
            }
        }

        $this->entityManager->clear();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    private function processPackageToProductDowngrades(InputInterface $input, OutputInterface $output)
    {
        $this->logMessage($output, "--- Processing package to product downgrades", ILogger::DEBUG);

        $services = $this->entityManager->createQueryBuilder()
            ->select('s')
            ->from('\Entities\Service', 's')
            ->where('s.serviceTypeId IN (:types)')->setParameter('types', [Service::TYPE_SERVICE_ADDRESS, Service::TYPE_REGISTERED_OFFICE])
            ->andWhere('s.parent IS NULL')
            ->andWhere('s.dtExpires >= :now')->setParameter('now', new DateTime, Type::DATE)
            ->orderBy('s.dtStart')
            ->getQuery()
            ->iterate();

        foreach ($services as $serviceData) {
            /** @var Service $service */
            $service = $serviceData[0];

            try {
                $limit1 = clone $service->getDtStart();
                $limit2 = clone $service->getDtStart();

                /** @var Service $previousService */
                $previousService = $this->entityManager->createQueryBuilder()
                    ->select('s')
                    ->from('\Entities\Service', 's')
                    ->where('s.serviceTypeId IN (:types)')->setParameter('types', [Service::TYPE_PACKAGE_PRIVACY, Service::TYPE_PACKAGE_COMPREHENSIVE_ULTIMATE])
                    ->andWhere('s.parent IS NULL')
                    ->andWhere('s.dtStart IS NOT NULL')
                    ->andWhere('s.dtExpires BETWEEN :renewDtStart AND :renewDtStart2')
                    ->setParameter('renewDtStart', $limit1->modify('-1 day'), Type::DATE)
                    ->setParameter('renewDtStart2', $limit2->modify('+1 year'), Type::DATE)
                    ->andWhere('s.company = :company')->setParameter('company', $service->getCompany())
                    ->orderBy('s.dtStart')
                    ->getQuery()
                    ->getOneOrNullResult();
            } catch (NonUniqueResultException $e) {
                $this->logMessage(
                    $output,
                    "Multiple services found for type {$service->getServiceTypeId()} with dtStart {$service->getDtStart()->format('d/m/Y')} for service {$service->getId()} ",
                    ILogger::WARNING
                );

                continue;
            }

            if ($previousService) {
                $this->logMessage(
                    $output,
                    sprintf(
                        'Changing service %d to %s (%s -> %s, latest service id: %d)',
                        $previousService->getId(),
                        Service::STATE_DOWNGRADED,
                        $previousService->getServiceTypeId(),
                        $service->getServiceTypeId(),
                        $service->getId()
                    ),
                    ILogger::WARNING
                );

                if (!$input->getOption('dry-run')) {
                    $this->serviceService->markAsDowngraded($previousService);
                }
            }
        }

        $this->entityManager->clear();
    }

    /**
     * @param OutputInterface $output
     * @param $message
     * @param int $level
     */
    private function logMessage(OutputInterface $output, $message, $level = ILogger::DEBUG)
    {
        $output->writeln($message);
        $this->logger->logMessage($level, strip_tags($message));
    }
}
