<?php

namespace Console\Commands\Services;

use DateTime;
use Doctrine\ORM\NoResultException;
use Entities\Company;
use Entities\OrderItem;
use Doctrine\ORM\EntityManager;
use Entities\Service;
use Exception;
use ILogger;
use Loggers\NikolaiLogger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ShortenByOneDayCommand extends Command
{
    /**
     * @var NikolaiLogger
     */
    private $logger;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @param EntityManager $entityManager
     * @param NikolaiLogger $logger
     * @param string|NULL $name
     */
    public function __construct(
        EntityManager $entityManager,
        NikolaiLogger $logger,
        $name = NULL
    )
    {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('services:shortenByOneDay')
            ->setDescription('Shorten each active and future service by one day (do not run this multiple times!)')
            ->addOption(
                'dry-run',
                NULL,
                InputOption::VALUE_NONE,
                'Simulate execution if this command'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $suspiciousServiceMessages = array();

        $infoMessage = '<info>Execution started: ' . $this->getName() . '</info>';
        $this->logMessage($output, $infoMessage);

        if ($input->getOption('dry-run')) {
            $this->logMessage($output, "<info>Dry-run flag set</info>");
        }

        //encapsulate everything in transaction
        $this->entityManager->beginTransaction();

        try {
            $services = $this->entityManager->createQueryBuilder()
                ->select('s')
                ->from('\Entities\Service', 's')
                ->where('s.dtExpires > :remindersDate')
                ->andWhere('s.parent IS NULL')
                ->orderBy('s.dtStart')
                ->setParameter('remindersDate', new DateTime('+ 28 days'))
                ->getQuery()
                ->iterate();

            foreach ($services as $serviceData) {
                /** @var Service $service */
                $service = $serviceData[0];
                $serviceDuration = $service->getDtStart()->diff($service->getDtExpires());

                $targetDtExpires = clone $service->getDtStart();
                $targetDtExpires->modify('+1 year');
                $targetDtExpires->modify('- 1 day');

                $originalDurationOk = FALSE;
                if ($targetDtExpires == $service->getDtExpires()) {
                    $originalDurationOk = TRUE;
                }

                if ($service->getDtStart() <= new DateTime() && $targetDtExpires != $service->getDtExpires()) {
                    $newDtExpires = clone $service->getDtExpires();
                    $newDtExpires->modify('- 1 day');
                    $message = "Service: {$service->getServiceId()}: DtStart < now(), changing dtExpires from {$service->getDtExpires()->format('d/m/Y')} to {$newDtExpires->format('d/m/Y')}";
                    $service->setDtExpires($newDtExpires);
                    $this->logMessage($output, $message);
                } else {
                    /** @var Service $lastService */
                    $lastService = $this->entityManager->createQueryBuilder()
                        ->select('s')
                        ->from('\Entities\Service', 's')
                        ->where('s.dtExpires > :remindersDate')
                        ->andWhere('s.company = :thisCompany')
                        ->andWhere('s.dtExpires <= :thisStart')
                        ->andWhere('s.parent IS NULL')
                        ->andWhere("DATE_ADD(s.dtExpires, 7, 'day') > :thisStart") //only look for services with expiration date max 7 days before this service start date
                        ->orderBy('s.dtExpires', 'DESC')
                        ->setMaxResults(1)
                        ->setParameter('remindersDate', new DateTime('+ 28 days'))
                        ->setParameter('thisStart', $service->getDtStart())
                        ->setParameter('thisCompany', $service->getCompany())
                        ->getQuery()
                        ->getOneOrNullResult();

                    if ($lastService) {
                        $newDtStart = clone $lastService->getDtExpires();
                        $newDtStart->modify('+ 1 day');

                        $message = "Service: {$service->getServiceId()}: changing dtStart from {$service->getDtStart()->format('d/m/Y')} to {$newDtStart->format('d/m/Y')}";
                        $this->logMessage($output, $message);
                        if ($newDtStart->diff($service->getDtStart(), TRUE)->days > 1) {
                            $suspiciousServiceMessages[] = $message;
                        }
                        $service->setDtStart($newDtStart);
                    }

                    $newTargetDtExpires = clone $service->getDtStart();
                    $newTargetDtExpires->modify('+1 year');
                    $newTargetDtExpires->modify('- 1 day');

                    if ($lastService || $newTargetDtExpires != $service->getDtExpires()) {
                        $newDtExpires = clone $service->getDtStart();
                        $newDtExpires->add($serviceDuration);

                        if (!$originalDurationOk) {
                            $newDtExpires->modify('- 1 day');
                        }

                        $message = "Service: {$service->getServiceId()}: changing dtExpires from {$service->getDtExpires()->format('d/m/Y')} to {$newDtExpires->format('d/m/Y')}";
                        $this->logMessage($output, $message);

                        if ($newDtExpires->diff($service->getDtExpires(), TRUE)->days > 1) {
                            $suspiciousServiceMessages[] = $message;
                        }

                        $service->setDtExpires($newDtExpires);
                    }
                }

                if (!$input->getOption('dry-run')) {
                    $this->entityManager->flush($service);
                }
            }

            if (!empty($suspiciousServiceMessages)) {
                $this->logMessage($output, 'Suspicious messages (more than one day difference between original and new dates):', ILogger::WARNING);
                foreach ($suspiciousServiceMessages as $suspiciousServiceMessage) {
                    $this->logMessage($output, $suspiciousServiceMessage, ILogger::WARNING);
                }
            } else {
                $this->logMessage($output, 'Executed without problems', ILogger::WARNING);
            }

            $this->entityManager->commit();
        } catch (Exception $e) {
            $this->entityManager->rollback();
            throw $e;
        }

        $infoMessage = '<info>Execution ended: ' . $this->getName() . '</info>';
        $this->logMessage($output, $infoMessage);
    }

    /**
     * @param OutputInterface $output
     * @param $message
     * @param int $level
     */
    private function logMessage(OutputInterface $output, $message, $level = ILogger::DEBUG)
    {
        $output->writeln($message);
        $this->logger->logMessage($level, strip_tags($message));
    }
}