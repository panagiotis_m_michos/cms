<?php

namespace Console\Commands\Packages;

use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityNotFoundException;
use Entities\Payment\Token;
use Entities\Service;
use Exception;
use ILogger;
use Loggers\NikolaiLogger;
use SagePay\Reporting\Request\TokenDetails;
use SagePay\Reporting\SagePayFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use UnexpectedValueException;
use Utils\Date;

class ActivateCommand extends Command
{
    /**
     * @var InputInterface
     */
    private $input;

    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var NikolaiLogger
     */
    private $logger;

    /**
     * @param EntityManager $em
     * @param NikolaiLogger $logger
     * @param string $name
     */
    public function __construct(EntityManager $em, NikolaiLogger $logger, $name = NULL)
    {
        $this->em = $em;
        $this->logger = $logger;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('packages:activate')
            ->setDescription('Activate packages for incorporated companies')
            ->addOption('dry-run', 'd', InputOption::VALUE_NONE, 'If set, changes are not flushed to database');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws UnexpectedValueException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;
        $this->logMessage("<info>Executed</info>");

        if ($input->getOption('dry-run')) {
            $this->logMessage("<info>Dry-run flag set</info>");
        }

        $qb = $this->em->createQueryBuilder();
        $services = $qb->select('s')
            ->from('Entities\Service', 's')
            ->where('s.parent IS NULL')
            ->andWhere('s.dtStart IS NULL')
            ->andWhere('s.dtExpires IS NULL')
            ->andWhere('s.company != 408182') // temporarily skip this company
            ->getQuery()
            ->iterate();

        $batchSize = 50;
        $batchCount = 0;
        $processed = 0;
        $errors = 0;

        foreach ($services as $serviceResult) {
            /** @var Service $service */
            $service = $serviceResult[0];
            try {
                $company = $service->getCompany();
                if ($company->isIncorporated() && $company->getIncorporationDate()) {
                    $dtStart = $company->getIncorporationDate();
                    $dtExpires = clone $company->getIncorporationDate();
                    $dtExpires->modify('+1 year');

                    $service->setDtStart($dtStart);
                    $service->setDtExpires($dtExpires);

                    $this->logMessage(
                        sprintf(
                            "<info>Activating service <comment>%s (%s)</comment>: Valid <comment>%s</comment> - <comment>%s</comment></info>",
                            $service->getServiceId(),
                            $service->getServiceName(),
                            $dtStart->format('d/m/Y'),
                            $dtExpires->format('d/m/Y')
                        )
                    );

                    $this->em->persist($service);
                    $processed++;
                    $batchCount++;
                }
            } catch (EntityNotFoundException $e) {
                $errors++;
                $this->logMessage("<error>Company for service {$service->getServiceId()} does not exist, deleting service!</error>");
                $this->em->remove($service);
            }

            if ($batchCount % $batchSize === 0) {
                $this->flushUpdates();
            }
        }

        $this->flushUpdates();
        $this->logMessage(
            sprintf(
                '<info>Done.</info> <comment>%s</comment> <info>services activated in total (<comment>%s</comment> errors)</info>',
                $processed,
                $errors
            )
        );
    }

    /**
     * @param string $message
     * @param int $level
     */
    private function logMessage($message, $level = ILogger::DEBUG)
    {
        $logMessage = sprintf('[%s] %s', $this->getName(), $message);
        $this->output->writeln($logMessage);
        $this->logger->logMessage($level, strip_tags($logMessage));
    }

    private function flushUpdates()
    {
        if (!$this->input->getOption('dry-run')) {
            $this->em->flush();
        }
        $this->em->clear();
        $this->logger->logMessage(ILogger::DEBUG, 'Updates have been executed');
    }
}
