<?php

namespace Console\Commands\OrderItems;

use DateTime;
use Entities\Company;
use Entities\OrderItem;
use Doctrine\ORM\EntityManager;
use ILogger;
use Loggers\NikolaiLogger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class FillCompanyReferencesCommand extends Command
{
    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * @var NikolaiLogger
     */
    private $logger;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @param EntityManager $entityManager
     * @param NikolaiLogger $logger
     * @param string|NULL $name
     */
    public function __construct(
        EntityManager $entityManager,
        NikolaiLogger $logger,
        $name = NULL
    )
    {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('orderItems:fillMissingCompanyReferences')
            ->setDescription('Fills missing company reference in order items by matching order description against companies.')
            ->addOption(
                'dry-run',
                NULL,
                InputOption::VALUE_NONE,
                'Simulate execution if this command'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $failedOrderItems = array();

        $infoMessage = '<info>Execution started: ' . $this->getName() . '</info>';
        $this->logMessage($infoMessage, ILogger::DEBUG);

        if ($input->getOption('dry-run')) {
            $this->logMessage("<info>Dry-run flag set</info>");
        }

        $orderItems = $this->entityManager->createQueryBuilder()
            ->select('oi')
            ->from('\Entities\OrderItem', 'oi')
            ->where('oi.company IS NULL')
            ->andWhere('oi.dtc > :from')
            ->andWhere('oi.productId IN (:products)')
            ->setParameter('from', new DateTime('2014-11-09'))
            ->setParameter('products', array(342, 1316, 1317))
            ->getQuery()
            ->iterate();

        foreach ($orderItems as $orderItemData) {
            /** @var OrderItem $orderItem */
            $orderItem = $orderItemData[0];

            if (preg_match('/"(.*)"/', $orderItem->getProductTitle(), $matches)) {
                $companyName = $matches[1];

                $matchedCompanies = $this->entityManager->createQueryBuilder()
                    ->select('c')
                    ->from('\Entities\Company', 'c')
                    ->where('c.companyName = :name')
                    ->setParameter('name', $companyName)
                    ->getQuery()
                    ->getResult();

                if (empty($companyName)) {
                    $infoMessage = "<info>Order Item id:{$orderItem->getOrderItemId()} from order:{$orderItem->getOrder()->getOrderId()}: Company name parsed, but empty!</info>";
                    $this->logMessage($infoMessage, ILogger::WARNING);
                    $failedOrderItems[] = $infoMessage;
                } elseif (count($matchedCompanies) > 1) {
                    $infoMessage = "<info>Order Item id:{$orderItem->getOrderItemId()} from order:{$orderItem->getOrder()->getOrderId()}: Multiple companies matching parsed company ({$companyName})!</info>";
                    $this->logMessage($infoMessage, ILogger::WARNING);
                    $failedOrderItems[] = $infoMessage;
                } elseif (count($matchedCompanies) == 0) {
                    $infoMessage = "<info>Order Item id:{$orderItem->getOrderItemId()} from order:{$orderItem->getOrder()->getOrderId()}: No company was matching parsed company ({$companyName})!</info>";
                    $this->logMessage($infoMessage, ILogger::WARNING);
                    $failedOrderItems[] = $infoMessage;
                } else {
                    /** @var Company $company */
                    $company = reset($matchedCompanies);
                    $orderItem->setCompany($company);

                    if (!$input->getOption('dry-run')) {
                        $this->entityManager->flush($orderItem);
                    }

                    $infoMessage = "<info>Order Item id:{$orderItem->getOrderItemId()} from order:{$orderItem->getOrder()->getOrderId()}: Assigned to company id:{$company->getCompanyId()}</info>";
                    $this->logMessage($infoMessage, ILogger::DEBUG);
                }
            } else {
                $infoMessage = "<info>Order Item id:{$orderItem->getOrderItemId()} from order:{$orderItem->getOrder()->getOrderId()}: Company name could not be parsed!</info>";
                $this->logMessage($infoMessage, ILogger::WARNING);
                $failedOrderItems[] = $infoMessage;
            }
        }

        if (!empty($failedOrderItems)) {
            $infoMessage = "<info>Failed order items:</info>";
            $this->logMessage($infoMessage, ILogger::WARNING);
            foreach ($failedOrderItems as $failedOrderItem) {
                $this->logMessage($failedOrderItem, ILogger::WARNING);
            }
        }

        $infoMessage = '<info>Execution ended: ' . $this->getName() . '</info>';
        $this->logMessage($infoMessage, ILogger::DEBUG);
    }

    /**
     * @param string $message
     * @param int $level
     */
    private function logMessage($message, $level = ILogger::DEBUG)
    {
        $this->output->writeln($message);
        $this->logger->logMessage($level, strip_tags($message));
    }
}
