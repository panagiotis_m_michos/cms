<?php

class FirstBoardMeetingMinutesDocument extends RTFDocument
{

    /**
     *
     * @return string 
     */
    public function getFileName()
    {
        return 'First Board Meeting Minutes.RTF';
    }

    /**
     *
     * @return array
     */
    public function getReplaceValues()
    {
        $replace = array(
            "[DATA INCORP]" => $this->getRtf()->getFormCreatetDate(),
            "[REGISTERED OFFICE ADDRESS]" => $this->getRtf()->getAddress(),
            "[SUBSCRIBERS]" => $this->getRtf()->getSubscribersName(),
            "[SUB SHARES NUMBER]" => $this->getRtf()->getSubscribersShares(),
            "[COMPANY NAME]" => $this->getRtf()->getCompanyName(),
            "[SECRETARIES]" => $this->getRtf()->getSecretaries(),
            "[DIRECTORS]" => $this->getRtf()->getDirectors(),
            "[ATTENDEES]" => "[ATTENDEES]",
            "[DATE]" => $this->getRtf()->getFormCreatetDate()
        );

        return $replace;
    }

}