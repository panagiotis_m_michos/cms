<?php

class AllBookDocument extends RTFDocument
{
	public function getFileName()
	{
		return 'all_book.rtf';
	}
	
	public function getReplaceValues()
	{
        $replace = array(
            "[DATA INCORP]" => $this->getRtf()->getCompanyIncorpDate(),
            "[REGISTERED OFFICE ADDRESS]" => $this->getRtf()->getAddress(),
            "[COMPANY NAME]" => $this->getRtf()->getCompanyName(),
			"[SECRETARIES]" => $this->getRtf()->getSecretaries(),
            "[DIRECTORS]" => $this->getRtf()->getDirectors(),
            "[DIRECTORSADDRESS]" => $this->getRtf()->getDirectorsAdress(),
            "[ATTENDEES]" => "[ATTENDEES]",
            "[COMPANYNUMBER]" => $this->getRtf()->getCompanyNumber(),
            //"[NEWDIRECTOR]" =>$this->getRtf()->getNewPerson(),
            "[DATE]" => date("m.d.y")
		);

		return $replace;
	}
}