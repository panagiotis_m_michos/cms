<?php

class BoardMinutesForTheIssueOfShares2 extends RTFDocument
{

    /**
     *
     * @return string 
     */
    public function getFileName()
    {
        return 'Board Minutes for the Issue of Shares2.RTF';
    }

    /**
     *
     * @return array 
     */
    public function getReplaceValues()
    {
        $replace = array(
            "[SHARE CLASS SPEC]" => $this->getRtf()->getCapitalClass(),
            "[SHARE VALUE SPEC]" => $this->getRtf()->getCapitalShareValue(),
            "[SHARE AMOUNT SPEC]" => $this->getRtf()->getCapitalAmount(),
            "[SHARE NUMBER SPEC]" => $this->getRtf()->getCapital_Num_Share(),
            "[DATA INCORP]" => $this->getRtf()->getCompanyIncorpDate(),
            "[REGISTERED OFFICE ADDRESS]" => $this->getRtf()->getAddress(),
            "[SHARE NAME]" => $this->getRtf()->getShareholdersName(),
            "[SHARE NUMBER]" => $this->getRtf()->getShareholdersShares(),
            // "[NEW REGISTERED OFFICE ADDRESS]" => $this->getRtf()->getNewAddress(),
            // "[SUBSCRIBERS]" => $this->getRtf()->getSubscribersName(),
            //  "[SUB SHARES NUMBER]" => $this->getRtf()->getSubscribersShares(),
            "[COMPANY NAME]" => $this->getRtf()->getCompanyName(),
            "[SECRETARIES]" => $this->getRtf()->getSecretaries(),
            "[DIRECTORS]" => $this->getRtf()->getDirectors(),
            "[ATTENDEES]" => "[ATTENDEES]",
            //  "[RESIGNSECRETARY]" => $this->getRtf()->getResignationPerson(),
            //  "[NEWSECRETARY]" =>$this->getRtf()->getNewPerson(),
            "[DATE]" => date("m.d.y")
        );

        return $replace;
    }

}