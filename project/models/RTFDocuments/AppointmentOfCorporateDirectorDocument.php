<?php

class AppointmentOfCorporateDirectorDocument extends RTFDocument
{

    /**
     *
     * @return string
     */
    public function getFileName()
    {
        return 'ABM - Appointment of Corporate Director.RTF';
    }

    /**
     *
     * @return array
     */
    public function getReplaceValues()
    {
        $replace = array(
            "[DATA INCORP]" => $this->getRtf()->getCompanyIncorpDate(),
            "[REGISTERED OFFICE ADDRESS]" => $this->getRtf()->getAddress(),
            "[COMPANY NAME]" => $this->getRtf()->getCompanyName(),
            "[SECRETARIES]" => $this->getRtf()->getSecretaries(),
            "[DIRECTORS]" => $this->getRtf()->getDirectors(),
            "[ATTENDEES]" => "[ATTENDEES]",
            "[NEWDIRECTOR]" => $this->getRtf()->getNewPerson(),
            "[DATE]" => date("d-m-Y"),
            "[APPOINTMENTDATE]" => $this->getRtf()->getAppointmentDate()
        );
        return $replace;
    }

    public function getDirectors()
    {
        // do some foreach
        $directors = $this->rtf->getDirectors();
        return $directors;
    }

}