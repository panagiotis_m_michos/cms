<?php

class ChangeOfRegisteredOfficeDocument extends RTFDocument
{

    /**
     *
     * @return string
     */
    public function getFileName()
    {
        return 'ABM - Change of Registered Office.RTF';
    }

    /**
     *
     * @return array 
     */
    public function getReplaceValues()
    {
		if($this->getRtf()->getOldAddresschanges()== NULL){
            $oldAddress = $this->getRtf()->getNewAddress();
        }else{
            $oldAddress = $this->getRtf()->getOldAddress();
        }
		
        $replace = array(
            "[DATA INCORP]" => $this->getRtf()->getCompanyIncorpDate(),
            "[REGISTERED OFFICE ADDRESS]" => $oldAddress,
            "[NEW REGISTERED OFFICE ADDRESS]" => $this->getRtf()->getNewAddress(),
            "[COMPANY NAME]" => $this->getRtf()->getCompanyName(),
            "[SECRETARIES]" => $this->getRtf()->getSecretaries(),
            "[DIRECTORS]" => $this->getRtf()->getDirectors(),
            "[ATTENDEES]" => "[ATTENDEES]",
            "[DATE]" => date("d-m-Y"),
            "[CHANGEOFFICENDATE]" => $this->getRtf()->getFormAcceptDate()
        );

        return $replace;
    }

}