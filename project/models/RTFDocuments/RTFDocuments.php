<?php

class RTFDocuments extends Object
{

    /**
     * @var Company
     */
    private $company;
    
    /**
     * @var int
     */
    private $formId;
    
    /**
     * @var int
     */
    private $dividendId;

    /**
     *
     * @var type 
     */
    private $companyData;

    /**
     * @param int $companyId
     * @param int $formId
     * @return void
     */
    public function __construct($companyId, $formId, $dividendId = NUll)
    {
        $this->company = Company::getCompany($companyId);
        $this->formId = (int) $formId;
		$this->dividendId = (int) $dividendId;
        $this->companyData = $this->company->getData();
    }

    public function getDividendId()
    {
        return $this->dividendId;
    }
    
    public function getCompanyId()
    {
       return $this->company->getCompanyId();
    }

    /**
     *  
     * @return array 
     */
    public function getResignations()
    {
        return CHFiling::getDb()->fetchAssoc('SELECT * FROM `ch_officer_resignation` WHERE `form_submission_id` = ' . $this->formId);
    }

    /**
     *
     * @return array
     */
    public function getAddresschanges()
    {
        return CHFiling::getDb()->fetchAssoc('SELECT * FROM `ch_address_change` WHERE `form_submission_id` =' . $this->formId);
    }

    /**
     *
     * @return array 
     */
    public function getAppointments()
    {
        return CHFiling::getDb()->fetchAssoc('SELECT * FROM `ch_officer_appointment` WHERE `form_submission_id` =' . $this->formId);
    }

    /**
     *
     * @return array 
     */
    public function getReturnOfAllotmentShares()
    {
        return CHFiling::getDb()->fetchAssoc('SELECT * FROM `ch_return_of_allotment_shares` WHERE `form_submission_id` =' . $this->formId);
    }

    /**
     *
     * @return string 
     */
    public function getFormSubmisionName()
    {
        return CHFiling::getDb()->fetchOne('SELECT `form_identifier` FROM `ch_form_submission` WHERE `form_submission_id` =' . $this->formId);
    }

    /**
     * 
     * @return strring
     */
    public function getDirectors()
    {
        $directors = "";
        foreach ($this->companyData['directors']as $director) {
            $directors.= $director['forename'] . ' ' . $director['middle_name'] . ' ' . $director['surname']  . '\par ';
        }
        return $directors;
    }

    /**
     *
     * @return string 
     */
    public function getDirectorsAdress()
    {
        $directorsadress = "";
        foreach ($this->companyData['directors']as $director) {
            $directorsadress .= $director['premise'] . ' ' . $director['street'] . ' ' . $director['post_town'] . ' ' . $director['postcode'] . '\par ';
        }
        return $directorsadress;
    }

    /**
     *
     * @return string 
     */
    public function getSecretaries()
    {
        $secretaries = '';
        if ($this->companyData['secretaries']) {
            $secretaries = '\b Secretaries \par \par \rtlch \ltrch\loch\f1\fs20\lang2057\i0\b0 2 \tab It was noted that ';
            foreach ($this->companyData['secretaries'] as $secretary) {
                $secretaries.= $secretary['forename'] . ' ' . $secretary['middle_name'] . ' ' . $secretary['surname'] . '\par ';
            }
            $secretaries .= ' had been appointed as Secretary pursuant to Section 12 of the Companies Act 2006.';
        }
        return $secretaries;
    }

    /**
     *
     * @return string 
     */
    public function getAddress()
    {        
		$address = "";
        $address .= isset($this->companyData['registered_office']['premise'])       ? (string) $this->companyData['registered_office']['premise'] . '\par ' : '';
        $address .= isset($this->companyData['registered_office']['street'])        ? (string)  $this->companyData['registered_office']['street'] . '\par ' : '';
        $address .= isset($this->companyData['registered_office']['thoroughfare'])  ? (string) $this->companyData['registered_office']['thoroughfare'] . '\par ' : '';
        $address .= isset($this->companyData['registered_office']['post_town'])     ? (string) $this->companyData['registered_office']['post_town'] . '\par ' : '';
        $address .= isset($this->companyData['registered_office']['country'])       ? (string) $this->companyData['registered_office']['country'] . '\par ' : '';
        $address .= isset($this->companyData['registered_office']['postcode'])      ? (string) $this->companyData['registered_office']['postcode'] . '\par ' : '';
        $address .= isset($this->companyData['registered_office']['care_of_name'])  ? (string) $this->companyData['registered_office']['care_of_name'] . '\par ' : '';
        $address .= isset($this->companyData['registered_office']['po_box'])        ? (string) $this->companyData['registered_office']['po_box'] . '\par ' : '';
        return $address;
    }    

    /**
     *
     * @return date
     */
    public function getCompanyIncorpDate()
    { 
        return date('d-m-Y', strtotime($this->companyData['company_details']['made_up_date']));
    }

    /**
     *
     * @return int 
     */
    public function getCompanyNumber()
    {
        return $this->companyData['company_details']['company_number'];
    }

    /**
     *
     * @return string 
     */
    public function getCompanyName()
    {
        return $this->companyData['company_details']['company_name'];
    }

    /**
     *
     * @return string 
     */
    public function getSubscribersName()
    {
        $subscriber = "";
        foreach ($this->companyData['subscribers']as $subscribers) {
            if (isset($subscribers['corporate_name'])) {
                $subscriber.= $subscribers['corporate_name'] . '\par ';
            } else {
                $subscriber.=  $subscribers['forename']. ' ' . $subscribers['surname'] . '\par ';
            }
        }
        return $subscriber;
    }

    /**
     *
     * @return string 
     */
    public function getSubscribersShares()
    {
        $subscriber_share = "";
        foreach ($this->companyData['subscribers']as $subscriber) {
            $subscriber_share.= $subscriber['shares'] . '/' . $subscriber['share_value'] . '\par ';
        }
        return $subscriber_share;
    }

    /**
     *
     * @return string 
     */
    public function getShareholdersName()
    {
        $shareholder = "";
        foreach ($this->companyData['shareholders']as $shareholders) {
            if (isset($shareholders['corporate_name'])) {
                $shareholder.= $shareholders['corporate_name'] . '\par ';
            } else {
                $shareholder.= $shareholders['forename'] . ' ' . $shareholders['surname'] . '\par ';
            }
        }
        return $shareholder;
    }

    /**
     *
     * @return string 
     */
    public function getShareholdersShares()
    {
        $shareholder_share = "";
        foreach ($this->companyData['shareholders']as $shareholder) {
            $shareholder_share.= $shareholder['num_shares'] . '\par ';
        }
        return $shareholder_share;
    }

    /**
     *
     * @return string 
     */
    public function getCapitalClass()
    {
        foreach ($this->companyData['capitals'] as $capitals) {
            foreach ($capitals['shares'] as $capital) {
                $capital_class_special = $capital['share_class'] . ' ';
            }
        }
        return $capital_class_special;
    }

    /**
     *
     * @return string 
     */
    public function getCapital_Num_Share()
    {
        foreach ($this->companyData['capitals'] as $capitals) {
            foreach ($capitals['shares'] as $capital) {
                $capital_num_share_special = $capital['num_shares'] . ' ';
            }
        }
        return $capital_num_share_special;
    }

    /**
     *
     * @return string 
     */
    public function getCapitalShareValue()
    {
        foreach ($this->companyData['capitals'] as $capitals) {
            foreach ($capitals['shares'] as $capital) {
                $capital_share_value_special = $capital['amount_paid'] . ' ';
            }
        }
        return $capital_share_value_special;
    }

    /**
     *
     * @return int 
     */
    public function getCapitalAmount()
    {
        foreach ($this->companyData['capitals'] as $capitals) {
            foreach ($capitals['shares'] as $capital) {
                $capital_amount_special = $capital['num_shares'] * $capital['amount_paid'];
            }
        }
        return $capital_amount_special;
    }

    /**
     *
     * @return string 
     */
    public function getNewPerson()
    {
        $appointment = $this->getAppointments();

        if ($appointment[$this->formId]['corporate']) {
            $newperson = $appointment[$this->formId]['corporate_name'];
        } else {
            $newperson = $appointment[$this->formId]['forename'] . ' ' . $appointment[$this->formId]['middle_name'] . ' ' . $appointment[$this->formId]['surname'];
        }
        return $newperson;
    }
     
    /**
     * 
     * @return string
     */
    public function getAppointmentDate()
    {
        $appointment = $this->getAppointments();
        return date('d-m-Y',strtotime($appointment[$this->formId]['appointment_date']));
    }
    
    /**
     *
     * @return string 
     */
    public function getResignationPerson()
    {
        $resignation = $this->getResignations();

        if ($resignation[$this->formId]['corporate']) {
            $resignperson = $resignation[$this->formId]['corporate_name'];
        } else {
            $resignperson = $resignation[$this->formId]['forename'] . ' ' . $resignation[$this->formId]['middle_name'] . ' ' . $resignation[$this->formId]['surname'];
        }
            return $resignperson;
    }


    /**
     *
     * @return array
     */
    public function getOldAddresschanges()
    {   $addresses = array();
        // get all submisions per company wich accept
        $submisions = CHFiling::getDb()->fetchAssoc("SELECT * FROM `ch_form_submission` WHERE `company_id` =".$this->company->getCompanyId()." 
                AND `form_identifier` LIKE 'ChangeRegisteredOfficeAddress'
                AND `response` LIKE 'ACCEPT'
                AND `date_cancelled` IS NULL
                ");
        // get all reg office address changes
        foreach ($submisions as $key => $value) {
            if ($this->formId  >= $key){
           		$addresses[] = CHFiling::getDb()->fetchAssoc('SELECT * FROM `ch_address_change` WHERE `form_submission_id` =' . $key);
        	}
		}
		
        // if address never was changed use default address
        if(count($addresses)<=1){
            return NULL;
        // else get address which was before    
        }else{
            $address = $addresses[count($addresses)-2];
        }

        return $address;
    }
    
    /**
     * 
     * 
     */
    public function getFormAcceptDate()
    {
        $date = CHFiling::getDb()->fetchOne("SELECT `date_sent` FROM `ch_envelope` WHERE `form_submission_id` =  $this->formId ORDER BY `ch_envelope`.`date_sent` DESC");
        $dateShort = date('d-m-Y',strtotime($date));
        return $dateShort;
    }
    
    /**
     * 
     * 
     */
    public function getFormCreatetDate()
    {
        $date = CHFiling::getDb()->fetchOne("SELECT `date_sent` FROM `ch_envelope` WHERE `form_submission_id` =  $this->formId ORDER BY `ch_envelope`.`date_sent` ASC");
        $dateShort = date('d-m-Y',strtotime($date));
        return $dateShort;
    }

    /**
     *
     * @return string 
     */
    public function getOldAddress()
    {
        $addresschangeS = $this->getOldAddresschanges();
        $addresschange = $addresschangeS[key($addresschangeS)];

        $oldaddress = "";
        $oldaddress .= isset($addresschange['premise'])      ? (string) $addresschange['premise'] . '\par ' : '';
        $oldaddress .= isset($addresschange['street'])       ? (string) $addresschange['street'] . '\par ' : '';
        $oldaddress .= isset($addresschange['thoroughfare']) ? (string) $addresschange['thoroughfare'] . '\par ' : '';
        $oldaddress .= isset($addresschange['post_town'])    ? (string) $addresschange['post_town'] . '\par ' : '';
        $oldaddress .= isset($addresschange['country'])      ? (string) $addresschange['country'] . '\par ' : '';
        $oldaddress .= isset($addresschange['postcode'])     ? (string) $addresschange['postcode'] . '\par ' : '';
        $oldaddress .= isset($addresschange['care_of_name']) ? (string) $addresschange['care_of_name'] . '\par ' : '';
        $oldaddress .= isset($addresschange['po_box'])       ? (string) $addresschange['po_box'] . '\par ' : '';
		return $oldaddress;
    }

    /**
     *
     * @return string 
     */
    public function getNewAddress()
    {
        $addresschange = $this->getAddresschanges();
        $newaddress = "";
        $newaddress .= isset($addresschange[$this->formId]['premise'])      ? (string) $addresschange[$this->formId]['premise'] . '\par ' : '';
        $newaddress .= isset($addresschange[$this->formId]['street'])       ? (string) $addresschange[$this->formId]['street'] . '\par ' : '';
        $newaddress .= isset($addresschange[$this->formId]['thoroughfare']) ? (string) $addresschange[$this->formId]['thoroughfare'] . '\par ' : '';
        $newaddress .= isset($addresschange[$this->formId]['post_town'])    ? (string) $addresschange[$this->formId]['post_town'] . '\par ' : '';
        $newaddress .= isset($addresschange[$this->formId]['country'])      ? (string) $addresschange[$this->formId]['country'] . '\par ' : '';
        $newaddress .= isset($addresschange[$this->formId]['postcode'])     ? (string) $addresschange[$this->formId]['postcode'] . '\par ' : '';
        $newaddress .= isset($addresschange[$this->formId]['care_of_name']) ? (string) $addresschange[$this->formId]['care_of_name'] . '\par ' : '';
        $newaddress .= isset($addresschange[$this->formId]['po_box'])       ? (string) $addresschange[$this->formId]['po_box'] . '\par ' : '';
        return $newaddress;
    }

    /**
     *
     * @param type int
     * @return boolean 
     */
    public static function getFormSubmisionStatus($formId)
    {
        $formSubmision = CHFiling::getDb()
            ->fetchOne('SELECT `response` FROM `ch_form_submission` WHERE `form_submission_id` =' . $formId);
        if ($formSubmision == 'ACCEPT') {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Give document name
     * @return string 
     */
    public function getDocumentTypeByFormId()
    {
        $appointment = $this->getAppointments();
        if (!empty($appointment) && self::getFormSubmisionStatus($this->formId)) {
            if ($appointment[$this->formId]['type'] == 'SEC' and $appointment[$this->formId]['corporate'] == 1) {
                $type = 'ABM - Appointment of Corporate Secretary.RTF';
            } elseif ($appointment[$this->formId]['type'] == 'DIR' and $appointment[$this->formId]['corporate'] == 1) {
                $type = 'ABM - Appointment of Corporate Director.RTF';
            } elseif ($appointment[$this->formId]['type'] == 'DIR') {
                $type = 'ABM - Appointment of Director.RTF';
            } elseif ($appointment[$this->formId]['type'] == 'SEC') {
                $type = 'ABM - Appointment of Secretary.RTF';
            }
        }

        $resignation = $this->getResignations();
        if (!empty($resignation) && self::getFormSubmisionStatus($this->formId)) {
            if ($resignation[$this->formId]['type'] == 'DIR') {
                $type = 'ABM - Resignation of Director.RTF';
            } elseif ($resignation[$this->formId]['type'] == 'SEC') {
                $type = 'ABM - Resignation of Secretary.RTF';
            }
        }

        $addresschanges = $this->getAddresschanges();
        if (!empty($addresschanges) && self::getFormSubmisionStatus($this->formId)) {
            $type = 'ABM - Change of Registered Office.RTF';
        }


        if ($this->getFormSubmisionName() == 'CompanyIncorporation' && self::getFormSubmisionStatus($this->formId)) {
            $type = 'First Board Meeting Minutes.RTF';
        }

        $returnOfAllotmentShares = $this->getReturnOfAllotmentShares();
        if (!empty($returnOfAllotmentShares) && self::getFormSubmisionStatus($this->formId)) {
            if ($this->getCapitalClass() == 'Ordinary') {
                $type = 'Board Minutes for the Issue of Shares3.RTF';
            } elseif ($this->getCapitalClass() == 'Special') {
                $type = 'Board Minutes for the Issue of Shares2.RTF';
            } else {
                $type = 'Board Minutes for the Issue of Shares.RTF';
            }
        }
        if(!empty($this->dividendId)){
            $type = 'ABM -Dividend Minutes.RTF';
        }
//        if ($formSubmisionIdentifier == 'CompanyIncorporation') {
//            $type = 'all_book.rtf';
//        }

        return $type;
    }

    /**
     * Return Document
     *
     * @return RTFDocument
     */
    public function getDocumentByFormId()
    {
        switch ($this->getDocumentTypeByFormId()) {
            case 'ABM - Appointment of Director.RTF':
                $doc = new AppointmentOfDirectorDocument($this);
                break;
            case 'ABM - Appointment of Secretary.RTF':
                $doc = new AppointmentOfSecretaryDocument($this);
                break;
            case 'ABM - Appointment of Corporate Secretary.RTF':
                $doc = new AppointmentOfCorporateSecretaryDocument($this);
                break;
            case 'ABM - Appointment of Corporate Director.RTF':
                $doc = new AppointmentOfCorporateDirectorDocument($this);
                break;
            case 'ABM - Resignation of Director.RTF':
                $doc = new ResignationOfDirectorDocument($this);
                break;
            case 'ABM - Resignation of Secretary.RTF':
                $doc = new ResignationOfSecretaryDocument($this);
                break;
            case 'ABM - Change of Registered Office.RTF':
                $doc = new ChangeOfRegisteredOfficeDocument($this);
                break;
            case 'Board Minutes for the Issue of Shares.RTF':
                $doc = new BoardMinutesForTheIssueOfShares($this);
                break;
            case 'Board Minutes for the Issue of Shares2.RTF':
                $doc = new BoardMinutesForTheIssueOfShares2($this);
                break;
            case 'Board Minutes for the Issue of Shares3.RTF':
                $doc = new BoardMinutesForTheIssueOfShares2($this);
                break;
            case 'First Board Meeting Minutes.RTF':
                $doc = new FirstBoardMeetingMinutesDocument($this);
                break;
            case 'all_book.rtf':
                $doc = new AllBookDocument($this);
                break;
            case 'ABM -Dividend Minutes.RTF':
                $doc = new DividendMinutesDocument($this);
                break;
        }

        return $doc;
    }

    /**
     * Generate document
     */
    public function output()
    {
        $this->getDocumentByFormId()->output();
    }

}
