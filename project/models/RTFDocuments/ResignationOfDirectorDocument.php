<?php

class ResignationOfDirectorDocument extends RTFDocument
{

    /**
     *
     * @return string
     */
    public function getFileName()
    {
        return 'ABM - Resignation of Director.RTF';
    }

    /**
     *
     * @return array
     */
    public function getReplaceValues()
    {
        $replace = array(
            "[DATA INCORP]" => $this->getRtf()->getCompanyIncorpDate(),
            "[REGISTERED OFFICE ADDRESS]" => $this->getRtf()->getAddress(),
            "[COMPANY NAME]" => $this->getRtf()->getCompanyName(),
            "[SECRETARIES]" => $this->getRtf()->getSecretaries(),
            "[DIRECTORS]" => $this->getRtf()->getDirectors(),
            "[ATTENDEES]" => "[ATTENDEES]",
            "[RESIGNDIRECTOR]" => $this->getRtf()->getResignationPerson(),
            "[DATE]" => date("d-m-Y"),
            "[RESIGNATIONDATE]" => $this->getRtf()->getFormCreatetDate()
        );

        return $replace;
    }

}