<?php

class DividendMinutesDocument extends RTFDocument
{

    /**
     *
     * @return string
     */
    public function getFileName()
    {
        return 'ABM -Dividend Minutes.RTF';
    }

    /**
     *
     * @return array
     */
    public function getReplaceValues()
    {
        $dividend = Dividend::getDividend($this->getRtf()->getCompanyId(), $this->getRtf()->getDividendId());

        $replace = array(
            "[DATA INCORP]" => $this->getRtf()->getCompanyIncorpDate(),
            "[REGISTERED OFFICE ADDRESS]" => $this->getRtf()->getAddress(),
            "[NEW REGISTERED OFFICE ADDRESS]" => $this->getRtf()->getNewAddress(),
            "[COMPANY NAME]" => $this->getRtf()->getCompanyName(),
            "[SECRETARIES]" => $this->getRtf()->getSecretaries(),
            "[DIRECTORS]" => $this->getRtf()->getDirectors(),
            "[ATTENDEES]" => "[ATTENDEES]",
            "[DATE]" => date("d.m.y"),
            "[DIVIDENT AMOUNT]" => $dividend->getNetDividend($dividend->getNumberOfShares()),
            "[SHARE VALUE]" => $dividend->getShareValue(),
            "[SHARE CURENCY]" => $dividend->getCurrency(),
            "[SHARE CLASS]" => ucwords($dividend->getShareClass()),

            "[RECORD DATE]" => date("d-m-Y", strtotime($dividend->getRecordDate())),
            "[PAYMENT DATE]" =>date("d-m-Y", strtotime($dividend->getPaidDate())),
            "[ACCOUNTING PERIOD]" =>date("d-m-Y", strtotime($dividend->getAccountingPeriod())),
            "[DIRECTORS MEETING DATE]" => date("d-m-Y", strtotime($dividend->getDirectorsMeeting())),
            "[ACCOUNT PERIOD END DATE]" => date("d-m-Y", strtotime($dividend->getAccountingPeriod())),

        );

        return $replace;
    }

}
