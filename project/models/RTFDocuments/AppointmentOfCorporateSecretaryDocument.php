<?php

class AppointmentOfCorporateSecretaryDocument extends RTFDocument
{

    /**
     *
     * @return string 
     */
    public function getFileName()
    {
        return 'ABM - Appointment of Corporate Secretary.RTF';
    }

    /**
     *
     * @return array 
     */
    public function getReplaceValues()
    {
        $replace = array(
            "[DATA INCORP]" => $this->getRtf()->getCompanyIncorpDate(),
            "[REGISTERED OFFICE ADDRESS]" => $this->getRtf()->getAddress(),
            "[COMPANY NAME]" => $this->getRtf()->getCompanyName(),
            "[SECRETARIES]" => $this->getRtf()->getSecretaries(),
            "[DIRECTORS]" => $this->getRtf()->getDirectors(),
            "[ATTENDEES]" => "[ATTENDEES]",
            "[NEWSECRETARY]" => $this->getRtf()->getNewPerson(),
            "[DATE]" => date("d-m-Y"),
            "[APPOINTMENTDATE]" => $this->getRtf()->getAppointmentDate()
        );

        return $replace;
    }

}