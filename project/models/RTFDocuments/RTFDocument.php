<?php

abstract class RTFDocument extends Object
{

    /**
     * @var RTFDocuments
     */
    private $rtf;

    /**
     *
     * @param RTFDocuments $rtf 
     */
    final public function __construct(RTFDocuments $rtf)
    {
        $this->rtf = $rtf;
    }

    /**
     *
     * @return RTFDocuments
     */
    final public function getRtf()
    {
        return $this->rtf;
    }

    /**
     * Output Document
     */
    final public function output()
    {

        $name = $this->getFileName();
        $dataw = $this->getReplaceValues();
        $filePath = CHFiling::getDocPath() . '/rtfdocs/' . $name;
        $content = file_get_contents($filePath);

        foreach ($dataw as $key => $value) {
            $content = str_replace($key, $value, $content);
        }
        header("Cache-Control: private, max-age=2");
        header("Pragma: private");
        header("Content-Description: File Transfer");
        header("Content-Transfer-Encoding: binary");
        header("Content-Type: application/rtf");
        header("Content-Length: " . strlen($content));
        header("Content-Disposition:attachment; filename=\"" . $name . "\"");
        echo($content);
        exit;
    }

    public function getFileName()
    {
        
    }

    public function getReplaceValues()
    {
        
    }

}