<?php

namespace Models\View\Front;

use DateTime;
use Entities\Company as CompanyEntity;
use EventLocator;
use Factories\ServiceViewFactory;
use Models\View\CompanyView;
use Models\View\ServiceView;
use Services\EventService;

class CompanyServicesView extends CompanyView
{
    const TYPE_EXPIRES_IN_28 = 28;
    const TYPE_EXPIRES_IN_15 = 15;
    const TYPE_EXPIRES_IN_1 = 1;
    const TYPE_EXPIRED = 0;
    const TYPE_EXPIRED_15_DAYS = -15;
    const TYPE_EXPIRED_28_DAYS = -28;

    /**
     * @var CompanyEntity
     */
    private $company;

    /**
     * @var EventService
     */
    private $eventService;

    /**
     * @param CompanyEntity $company
     * @param ServiceViewFactory $serviceViewFactory
     * @param EventService $eventService
     */
    public function __construct(CompanyEntity $company, ServiceViewFactory $serviceViewFactory, EventService $eventService)
    {
        $this->company = $company;
        $this->eventService = $eventService;
        parent::__construct($company, $serviceViewFactory);
    }

    /**
     * @return string
     */
    public function getCustomerFullName()
    {
        return $this->company->getCustomer()->getFullName();
    }

    /**
     * @param DateTime $expiryDate
     * @return int
     */
    public function getDaysRange($expiryDate)
    {
        $now = new DateTime('00:00:00');
        $expiryDate->setTime(0, 0, 0);
        $interval = $now->diff($expiryDate);
        $days = (int) $interval->format('%r%a');
        return $days;
    }

    /**
     * @param int $days
     * @param string $event
     * @return bool
     */
    public function hasServicesWithoutEventExpiringIn($days, $event)
    {
        foreach ($this->getActivatedServices() as $service) {
            $serviceDays = $this->getDaysRange($service->getDtExpires());
            if ($serviceDays == $days
                && $service->canReceiveReminders()
                && !$this->eventService->eventOccurred($event, $service->getId())
            ) {
                return TRUE;
            }
        }

        return FALSE;
    }

    /**
     * @param int $days
     * @param string $event
     * @return ServiceView[]
     */
    public function getServicesWithoutEventExpiringIn($days, $event)
    {
        $filteredServices = [];
        foreach ($this->getActivatedServices() as $service) {
            $serviceDays = $this->getDaysRange($service->getDtExpires());
            if ($serviceDays == $days
                && $service->canReceiveReminders()
                && !$this->eventService->eventOccurred($event, $service->getId())
            ) {
                $filteredServices[] = $service;
            }
        }

        return $filteredServices;
    }

    /**
     * @param int $days
     * @return bool
     */
    public function hasCompanyServicesWithoutFailedAutoRenewalExpiringIn($days)
    {
        foreach ($this->getActivatedServices() as $service) {
            $serviceDays = $this->getDaysRange($service->getDtExpires());
            if ($serviceDays == $days
                && $service->canReceiveReminders()
                && !$this->eventService->eventOccurred(EventLocator::SERVICE_EXPIRED, $service->getId())
                && !$this->eventService->eventOccurred(EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_FAILED, $service->getId())
            ) {
                return TRUE;
            }
        }

        return FALSE;
    }

    /**
     * @param int $days
     * @return ServiceView[]
     */
    public function getCompanyServicesWithoutFailedAutoRenewalExpiringIn($days)
    {
        $filteredServices = [];
        foreach ($this->getActivatedServices() as $service) {
            $serviceDays = $this->getDaysRange($service->getDtExpires());
            if ($serviceDays == $days
                && $service->canReceiveReminders()
                && !$this->eventService->eventOccurred(EventLocator::SERVICE_EXPIRED, $service->getId())
                && !$this->eventService->eventOccurred(EventLocator::COMPANY_SERVICES_AUTO_RENEWAL_FAILED, $service->getId())
            ) {
                $filteredServices[] = $service;
            }
        }

        return $filteredServices;
    }
}
