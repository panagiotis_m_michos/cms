<?php

namespace Models\View\Front;

use Models\View\Front\CompanyServicesView;
use FEmail;
use Exceptions\Business\ItemNotFound;
use Models\View\Front\NoteServiceEmailView;
use Models\View\ServiceView;

class CompanyServicesEmailView
{

    /**
     * @var array
     */
    private $serviceViews = [];

    /**
     * @var CompanyServicesView
     */
    private $companyServicesView;

    /**
     * @var FEmail
     */
    private $email;

    /**
     * @var string
     */
    private $status;

    /**
     * @param FEmail $email
     * @param CompanyServicesView $companyServicesView
     * @param array $serviceViews
     * @param string $status
     * @param NoteServiceEmailView $note
     */
    public function __construct(FEmail $email, CompanyServicesView $companyServicesView, array $serviceViews, $status = NULL)
    {
        $this->email = $email;
        $this->companyServicesView = $companyServicesView;
        $this->serviceViews = $serviceViews;
        $this->status = $status;
    }

    /**
     * @return CompanyServicesView
     */
    public function getCompanyServicesView()
    {
        return $this->companyServicesView;
    }

    /**
     * @return ServiceView[]
     */
    public function getServiceViews()
    {
        return $this->serviceViews;
    }

    /**
     * @param NoteServiceEmailView $notes
     * @return NoteServiceEmailView
     * @throws ItemNotFound
     */
    public function getNotes(NoteServiceEmailView $notes = NULL)
    {
        $notes = $notes ? : new NoteServiceEmailView();

        if (empty($this->serviceViews)) {
            throw new ItemNotFound('ServicesViews can\'t be empty');
        }

        foreach ($this->serviceViews as $service) {
            $notes->generateNotes($service, $this->status);
        }
        return $notes;
    }

    /**
     * @return string
     */
    public function getEmailTitle()
    {
        return $this->email->subject;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return bool
     */
    public function isStatusExpiresIn28()
    {
        return $this->status == CompanyServicesView::TYPE_EXPIRES_IN_28;
    }

    /**
     * @return bool
     */
    public function isStatusExpiresIn15()
    {
        return $this->status == CompanyServicesView::TYPE_EXPIRES_IN_15;
    }

    /**
     * @return bool
     */
    public function isStatusExpiresIn1()
    {
        return $this->status == CompanyServicesView::TYPE_EXPIRES_IN_1;
    }

    /**
     * @return bool
     */
    public function isStatusExpired()
    {
        return $this->status == CompanyServicesView::TYPE_EXPIRED;
    }

    /**
     * @return bool
     */
    public function isStatusExpired15Days()
    {
        return $this->status == CompanyServicesView::TYPE_EXPIRED_15_DAYS;
    }

    /**
     * @return bool
     */
    public function isStatusExpired28Days()
    {
        return $this->status == CompanyServicesView::TYPE_EXPIRED_28_DAYS;
    }

    /**
     * @param ServiceView $service
     * @param NoteServiceEmailView $notes
     * @return NoteServiceEmailView
     */
    public function getServiceNotes(ServiceView $service, NoteServiceEmailView $notes = NULL)
    {
        $notes = $notes ? : new NoteServiceEmailView();
        $notes->generateNotes($service, $this->status);
        return $notes;
    }

}
