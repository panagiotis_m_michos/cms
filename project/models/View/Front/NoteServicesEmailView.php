<?php

namespace Models\View\Front;

use Entities\Service;
use Models\View\ServiceView;

class NoteServiceEmailView
{

    /**
     * @var bool
     */
    private $note1 = FALSE;

    /**
     * @var bool
     */
    private $note2 = FALSE;

    /**
     * @var bool
     */
    private $note3 = FALSE;

    /**
     * @var bool
     */
    private $note4 = FALSE;

    /**
     * @var bool
     */
    private $note5 = FALSE;

    /**
     * @return bool
     */
    public function hasNote1()
    {
        return $this->note1;
    }

    /**
     * @return bool
     */
    public function hasNote2()
    {
        return $this->note2;
    }

    /**
     * @return bool
     */
    public function hasNote3()
    {
        return $this->note3;
    }

    /**
     * @return bool
     */
    public function hasNote4()
    {
        return $this->note4;
    }

    /**
     * @return bool
     */
    public function hasNote5()
    {
        return $this->note5;
    }

    /**
     * @param bool $note1
     */
    public function setNote1($note1)
    {
        $this->note1 = $note1;
    }

    /**
     * @param bool $note2
     */
    public function setNote2($note2)
    {
        $this->note2 = $note2;
    }

    /**
     * @param bool $note3
     */
    public function setNote3($note3)
    {
        $this->note3 = $note3;
    }

    /**
     * @param bool $note4
     */
    public function setNote4($note4)
    {
        $this->note4 = $note4;
    }

    /**
     * @param bool $note5
     */
    public function setNote5($note5)
    {
        $this->note5 = $note5;
    }

    /**
     * @return int
     */
    public function getNotesString()
    {
        //first note always 1
        $i = '(1';

        if ($this->hasNote2()) {
            $i .= ', 2';
        }
        if ($this->hasNote3()) {
            $i .= ', 3';
        }
        if ($this->hasNote4()) {
            $i .= ', 4';
        }
        if ($this->hasNote5()) {
            $i .= ', 5';
        }
        $i .= ')';

        return $i;
    }

    /**
     * @param ServiceView $serviceView
     * @param string $status
     */
    public function generateNotes($serviceView, $status = NULL)
    {
        if (empty($serviceView)) {
            throw new ItemNotFound('ServicesViews can\'t be empty');
        }

        if ($status == CompanyServicesView::TYPE_EXPIRED_28_DAYS) {
            switch ($serviceView->getServiceTypeId()) {
                case Service::TYPE_REGISTERED_OFFICE:
                    $this->setNote2(TRUE);
                    break;
                case Service::TYPE_SERVICE_ADDRESS:
                    $this->setNote4(TRUE);
                    break;
                case Service::TYPE_RESERVE_COMPANY_NAME:
                    $this->setNote3(TRUE);
                    break;
                case Service::TYPE_OFFSHORE:
                    $this->setNote5(TRUE);
                    break;
            }
        } else {
            switch ($serviceView->getServiceTypeId()) {
                case Service::TYPE_REGISTERED_OFFICE:
                    $this->setNote2(TRUE);
                    break;
                case Service::TYPE_SERVICE_ADDRESS:
                    $this->setNote2(TRUE);
                    break;
                case Service::TYPE_OFFSHORE:
                    $this->setNote3(TRUE);
                    break;
                case Service::TYPE_PACKAGE_PRIVACY:
                    $this->setNote4(TRUE);
                    break;
            }
        }
    }

}
