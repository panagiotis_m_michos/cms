<?php

namespace Models\View;

use DateTime;
use Entities\Service;
use Nette\Object;
use Product;
use Repositories\ServiceSettingsRepository;

class ServiceView extends Object
{
    /**
     * @var Service[]
     */
    private $services = [];

    /**
     * @var ServiceSettingsRepository
     */
    private $serviceSettingsRepository;

    /**
     * Just provide services with same serviceTypeId
     *
     * @param array $services
     * @param ServiceSettingsRepository $serviceSettingsRepository
     */
    public function __construct(array $services, ServiceSettingsRepository $serviceSettingsRepository)
    {
        $this->services = $services;
        $this->serviceSettingsRepository = $serviceSettingsRepository;
    }

    /**
     * @return NULL|int
     */
    public function getId()
    {
        $service = $this->getLatest();

        return $service ? $service->getId() : NULL;
    }

    /**
     * @return bool
     */
    public function isOnHold()
    {
        $now = new DateTime();
        $dtStart = $this->getDtStart();

        return !$dtStart || $dtStart > $now;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->isActiveOn(new DateTime);
    }

    /**
     * @param DateTime $date
     * @return bool
     */
    public function isActiveOn(DateTime $date)
    {
        $activeEnd = clone $date;
        $activeEnd->modify('-28 days');

        $dtStart = $this->getDtStart();
        $dtExpires = $this->getDtExpires();
        if ($this->isOneOff()) {
            $active = $dtStart && $dtStart <= $date;

            return $dtExpires ? $dtExpires > $date && $active : $active;
        }

        return $dtStart && $dtExpires && $dtStart <= $date && $dtExpires >= $activeEnd;
    }

    /**
     * @return bool
     */
    public function isDue()
    {
        if ($this->isOneOff()) {
            return FALSE;
        }
        $now = new DateTime();
        $dueEnd = new DateTime('+28 days');
        $dueEnd->setTime(23, 59, 59);
        $dtExpires = $this->getDtExpires();

        return $this->isActive() && $dtExpires >= $now && $dtExpires <= $dueEnd;
    }

    /**
     * @return int|NULL
     */
    public function getDaysToExpire()
    {
        $now = new DateTime();
        $dtExpires = $this->getDtExpires();

        return $dtExpires ? $dtExpires->diff($now)->format("%a") : NULL;
    }

    /**
     * @return bool
     */
    public function isOverdue()
    {
        if ($this->isOneOff()) {
            return FALSE;
        }
        $now = new DateTime();
        $overdueEnd = new DateTime('-28 days');
        $dtExpires = $this->getDtExpires();

        return $this->isActive() && $dtExpires < $now && $dtExpires >= $overdueEnd;
    }

    /**
     * @return bool
     */
    public function hasActiveNonOverdueServiceOfTheSameType()
    {
        $services =  $this->getService()->getCompany()->getServicesByType($this->getService()->getServiceTypeId());
        $now = new DateTime;

        foreach ($services as $service) {
            if ($service->getDtStart() <= $now && $service->getDtExpires() >= $now) {
                return TRUE;
            }
        }

        return FALSE;
    }

    /**
     * @return bool
     */
    public function isExpired()
    {
        return $this->isExpiredOn(new DateTime);
    }

    /**
     * @param DateTime $date
     * @return bool
     */
    public function isExpiredOn(DateTime $date)
    {
        $suspendStart = clone $date;
        $suspendStart->modify('-28 days');
        $dtExpires = $this->getDtExpires();

        if ($this->isOneOff()) {
            return $dtExpires && $dtExpires < new DateTime();
        }

        return $dtExpires && $dtExpires < $suspendStart;
    }

    /**
     * @param DateTime $date
     * @return bool
     */
    public function isOverdueOn(DateTime $date)
    {
        if ($this->isOneOff()) {
            return FALSE;
        }

        return $this->isActiveOn($date) && $date >= $this->getDtExpires();
    }

    /**
     * @return bool
     */
    public function canToggleAutoRenewal()
    {
        return $this->canToggleAutoRenewalOn(new DateTime);
    }

    /**
     * @param DateTime $date
     * @return bool
     */
    public function canToggleAutoRenewalOn(DateTime $date)
    {
        return $this->isEnabled() && $this->isActiveOn($date) && !$this->isLastDayActiveOn($date);
    }

    /**
     * @return bool
     */
    public function isLastDayActive()
    {
        return $this->isLastDayActiveOn(new DateTime);
    }

    /**
     * @param DateTime $date
     * @return bool
     */
    public function isLastDayActiveOn(DateTime $date)
    {
        $expiredDay = clone $date;
        $expiredDay->modify('+1 day');

        return $this->isActiveOn($date) && $this->isExpiredOn($expiredDay);
    }

    /**
     * @return bool
     */
    public function expiresToday()
    {
        if ($this->isOneOff()) {
            return FALSE;
        }
        $now = new DateTime();
        $now->setTime(23, 59, 59);
        $dtExpires = $this->getDtExpires();

        return $this->isActive() && $dtExpires == $now;
    }

    /**
     * @return bool
     */
    public function isUpgraded()
    {
        $service = $this->getService();

        return $service && $service->isUpgraded();
    }

    /**
     * @return bool
     */
    public function isDowngraded()
    {
        $service = $this->getService();

        return $service && $service->isDowngraded();
    }

    /**
     * @return string|NULL
     */
    public function getStatusId()
    {
        if ($this->isUpgraded()) {
            return Service::STATUS_UPGRADED;
        } elseif ($this->isDowngraded()) {
            return Service::STATUS_DOWNGRADED;
        } elseif ($this->isOnHold()) {
            return Service::STATUS_ON_HOLD;
        } elseif ($this->isDue() && !$this->isAutoRenewalEnabled()) {
            if (!$this->expiresToday()) {
                return Service::STATUS_SOON_DUE;
            } else {
                return Service::STATUS_EXPIRES_TODAY;
            }
        } elseif (($this->isExpired() && !$this->isOverdue()) || ($this->isOverdue() && $this->hasActiveNonOverdueServiceOfTheSameType())) {
            return Service::STATUS_EXPIRED;
        } elseif ($this->isOverdue()) {
            return Service::STATUS_OVERDUE;
        } elseif ($this->isActive()) {
            return Service::STATUS_ACTIVE;
        }

        return NULL;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        $statusId = $this->getStatusId();
        $status = (isset(Service::$statuses[$statusId])) ? Service::$statuses[$statusId] : NULL;
        if ($statusId == Service::STATUS_SOON_DUE) {
            $days = $this->getDaysToExpire();
            $daysString = "$days day" . ($days > 1 ? 's' : '');
            $status = sprintf($status, $daysString);
        }

        return $status;
    }

    /**
     * @return string
     */
    public function getStatusCSSClass()
    {
        if ($this->isDue() && !$this->isAutoRenewalEnabled()) {
            return 'fa fa-exclamation-triangle em13 yellow';
        } elseif ($this->isActive()) {
            if ($this->isOverdue()) {
                return 'fa fa-exclamation-circle em13 red';
            } else {
                return 'fa fa-check-circle em13 green1';
            }
        } elseif ($this->isExpired()) {
            return 'fa fa-times-circle em13 grey2';
        }

        return '';
    }

    /**
     * @return DateTime|NULL
     */
    public function getDtStart()
    {
        $dtStart = NULL;
        foreach ($this->services as $service) {
            $serviceDtStart = $service->getDtStart();
            if (!empty($serviceDtStart)) {
                if (empty($dtStart) || $dtStart > $serviceDtStart) {
                    $dtStart = $serviceDtStart;
                }
            }

        }

        return $dtStart;
    }

    /**
     * @return DateTime|NULL
     */
    public function getDtExpires()
    {
        $dtExpires = NULL;
        foreach ($this->services as $service) {
            $serviceDtExpires = $service->getDtExpires();
            if (!empty($serviceDtExpires)) {
                if (empty($dtExpires) || $dtExpires < $serviceDtExpires) {
                    $dtExpires = $serviceDtExpires;
                }
                $dtExpires->setTime(23, 59, 59);
            }
        }

        return $dtExpires;
    }

    /**
     * @return bool
     */
    public function isOneOff()
    {
        foreach ($this->services as $service) {
            if ($service->isOneOff()) {
                return TRUE;
            }
        }

        return FALSE;
    }

    /**
     * @return bool
     */
    public function isRenewable()
    {
        foreach ($this->services as $service) {
            if ($service->isRenewable()) {
                return TRUE;
            }
        }

        return FALSE;
    }

    /**
     * @return string|NULL
     */
    public function getServiceTypeId()
    {
        foreach ($this->services as $service) {
            return $service->getServiceTypeId();
        }

        return NULL;
    }

    /**
     * @return bool
     */
    public function hasRenewalProduct()
    {
        $service = $this->getLatest();

        return $service ? $service->hasRenewalProduct() : FALSE;
    }

    /**
     * @return NULL|Product
     */
    public function getRenewalProduct()
    {
        $service = $this->getLatest();

        return $service ? $service->getRenewalProduct() : NULL;
    }

    /**
     * @return bool
     */
    public function hasParent()
    {
        $service = $this->getLatest();

        return $service ? $service->hasParent() : FALSE;
    }

    /**
     * @return bool
     */
    public function getParent()
    {
        $service = $this->getLatest();

        return $service ? $service->getParent() : NULL;
    }

    /**
     * @return NULL|Service[]
     */
    public function getChildren()
    {
        $service = $this->getLatest();

        return $service ? $service->getChildren() : NULL;
    }

    /**
     * @return Service
     */
    public function getService()
    {
        $service = $this->getLatest();

        return $service ?: NULL;
    }

    /**
     * @return NULL|string
     */
    public function getServiceName()
    {
        $service = $this->getLatest();

        return $service ? $service->getServiceName() : NULL;
    }

    /**
     * @return string
     */
    public function getServiceType()
    {
        $latest = $this->getLatest();
        if ($latest) {
            return $latest->getServiceType();
        }
    }

    /**
     * @return NULL|Service
     */
    private function getLatest()
    {
        $dtExpires = $latestService = NULL;
        foreach ($this->services as $service) {
            $serviceDtExpires = $service->getDtExpires();
            if (!empty($serviceDtExpires)) {
                if (empty($dtExpires) || $dtExpires < $serviceDtExpires) {
                    $dtExpires = $serviceDtExpires;
                    $latestService = $service;
                }
            }
        }

        return empty($latestService) ? end($this->services) : $latestService;
    }

    /**
     * @return bool
     */
    public function isPackageType()
    {
        foreach ($this->services as $service) {
            if ($service->isPackageType()) {
                return TRUE;
            }
        }
    }

    /**
     * @return bool
     */
    public function isAutoRenewalEnabled()
    {
        $service = $this->getLatest();

        return $service && $this->serviceSettingsRepository->getSettingsByService($service)->isAutoRenewalEnabled();
    }

    /**
     * @return bool
     */
    public function hasEmailReminders()
    {
        $service = $this->getLatest();

        return $service && $this->serviceSettingsRepository->getSettingsByService($service)->hasEmailReminders();
    }

    /**
     * @return bool
     */
    public function isAutoRenewalAllowed()
    {
        $latest = $this->getLatest();

        return $latest ? $latest->getProduct()->isAutoRenewalAllowed : FALSE;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        $latest = $this->getLatest();

        return $latest ? $latest->isEnabled() : FALSE;
    }

    /**
     * @return bool
     */
    public function canReceiveReminders()
    {
        $service = $this->getLatest();

        return $service ? (!$service->isOneOff() && $this->hasEmailReminders() && !$this->isAutoRenewalEnabled()) : FALSE;
    }

    /**
     * @return bool
     */
    public function isActionRequired()
    {
        if ($this->isOverdue() || ($this->isDue() && !$this->isAutoRenewalEnabled())) {
            return TRUE;
        }

        return FALSE;
    }
}
