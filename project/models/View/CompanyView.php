<?php

namespace Models\View;

use Doctrine\Common\Collections\ArrayCollection;
use Entities\Company;
use Entities\Service;
use Factories\ServiceViewFactory;
use Nette\Object;

class CompanyView extends Object
{

    /**
     * @var Company
     */
    private $company;

    /**
     * @var ServiceViewFactory
     */
    private $serviceViewFactory;

    /**
     * @param Company $company
     * @param ServiceViewFactory $serviceViewFactory
     */
    public function __construct(Company $company, ServiceViewFactory $serviceViewFactory)
    {
        $this->company = $company;
        $this->serviceViewFactory = $serviceViewFactory;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->company->getId();
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->company->getCompanyName();
    }

    /**
     * @return bool
     */
    public function isServiceActionRequired()
    {
        foreach ($this->getServices() as $service) {
            if ($service->isActionRequired()) {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * @return ServiceView[]
     */
    public function getServices()
    {
        return $this->createServiceViews($this->company->getEnabledParentServices());
    }

    /**
     * @return ServiceView[]
     */
    public function getActivatedServices()
    {
        return $this->createServiceViews($this->company->getEnabledActivatedParentServices());
    }

    /**
     * @return bool
     */
    public function hasServices()
    {
        if ($this->company->getServices()->isEmpty()) {
            return FALSE;
        }
        return TRUE;
    }

    /**
     * @return bool
     */
    public function hasAuthenticationCode()
    {
        return $this->company->hasAuthenticationCode();
    }

    /**
     * @return bool
     */
    public function isCompanyIncorporated()
    {
        return $this->company->isIncorporated();
    }

    /**
     * @param Service[]|ArrayCollection $services
     * @return array
     */
    private function createServiceViews($services)
    {
        $serviceViews = [];
        $groupedByTypeServices = [];

        foreach ($services as $service) {
            $groupedByTypeServices[$service->getServiceTypeId()][] = $service;
        }

        foreach ($groupedByTypeServices as $serviceGroup) {
            $serviceViews[] = $this->serviceViewFactory->create($serviceGroup);
        }

        return $serviceViews;
    }
}
