<?php

abstract class Package_Abstract extends Product
{
    /** @var string */
    public $typeId;

    /** @var boolean */
    public $onlyOne = TRUE;

    /** @var string */
    public $upgradeText;

    /** @var string */
    public $upgradeProductId;

    /** @var string */
    public $upgradeProduct;

    /** @var array */
    public $includedProducts = array();

    /**
     * @var bool
     */
    protected $international = FALSE;

    /** @var array */
    private $serviceCompanyId = NULL;

    /**
     * @return boolean
     */
    public function isInternational()
    {
        return $this->international;
    }

    /**
     * @param boolean $international
     */
    public function setInternational($international)
    {
        $this->international = $international;
    }

    protected function addCustomData()
    {
        parent::addCustomData();
        $this->international = $this->getProperty('isInternational');
    }

    public function saveCustomData($action)
    {
        parent::saveCustomData($action);
        $this->saveProperty('isInternational', $this->international);
    }

    /**
     * @return int
     */
    public function getServiceCompanyId()
    {
        return $this->serviceCompanyId;
    }

    /**
     * @param int $serviceCompanyId
     */
    public function setServiceCompanyId($serviceCompanyId)
    {
        $this->serviceCompanyId = $serviceCompanyId;
    }

    /**
     * @param object Basket $basket
     * @return boolean
     */
    public function canBeUpdated(Basket $basket)
    {
        return FALSE;
    }

    /**
     * Returns includes|associated products
     * @param string $type
     * @return array
     */
    public function getPackageProducts($type)
    {
        $products = array();
        $controlers = FApplication::$db->select('node_id, admin_controler')->from(TBL_NODES)->where('node_id')->in($this->$type)->execute()->fetchPairs();
        foreach ($this->$type as $key => $val) {
            if (isset($controlers[$val])) {
                $controler = $controlers[$val];
                $products[$val] = self::getProductByAdminControler($controler, $val);
            }
        }
        return $products;
    }

    /**
     * @return array
     */
    public function getTypesForPackageProducts()
    {
        $includedProducts = $this->getPackageProducts('includedProducts');
        $types = array();
        foreach ($includedProducts as $product) {
            /* @var $product Product */
            if ($product->hasServiceType()) {
                $types[] = $product->serviceTypeId;
            }
        }
        return $types;
    }

    /**
     * @param string $serviceTypeId
     * @return NULL|Product
     */
    public function getIncludedProduct($serviceTypeId)
    {
        $includedProducts = $this->getPackageProducts('includedProducts');
        foreach ($includedProducts as $product) {
            /* @var $product Product */
            if ($product->hasServiceType() && $product->serviceTypeId == $serviceTypeId) {
                return $product;
            }
        }
        return NULL;
    }
} 
