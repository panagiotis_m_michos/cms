<?php

interface IPackage
{
    /**
     * @param string $type
     * @return BasketProduct[]
     */
    public function getPackageProducts($type);

    /**
     * @return string[]
     */
    public function getTypesForPackageProducts();

    /**
     * @param string $serviceTypeId
     * @return NULL|Product
     */
    public function getIncludedProduct($serviceTypeId);
}
