<?php

class RenewalPackage extends Package_Abstract implements IRenewalPackage
{
    /**
     * @param string $action
     */
    public function saveCustomData($action)
    {
        parent::saveCustomData($action);
        $this->saveProperty('typeId', $this->typeId);

        // save products
        $arr = array('includedProducts');
        foreach ($arr as $val) {
            $products = NULL;
            if (!empty($this->$val)) {
                $products = implode(',', $this->$val);
            }
            $this->saveProperty($val, $products);
        }
    }

    /**
     * @return boolean
     */
    public function isOk2show()
    {
        if ($this->adminControler != 'RenewalPackageAdminControler') {
            return FALSE;
        } elseif (!$this->exists()) {
            return FALSE;
            // has been deleted - it's in bin
        } elseif ($this->isDeleted()) {
            return FALSE;
            // check status pending
        } elseif ($this->getStatusId() == self::STATUS_PENDING) {
            return FALSE;
            // check status draft
        } elseif ($this->getStatusId() == self::STATUS_DRAFT && !FUser::isSignedIn()) {
            return FALSE;
            // showing date
        } elseif ($this->dtShow > date('Y-m-d G:i:s')) {
            return FALSE;
            // sensitive to date
        } elseif ($this->isDtSensitive && ($this->dtStart > date('Y-m-d G:i:s') || $this->dtEnd < date('Y-m-d'))) {
            return FALSE;
        }
        return TRUE;
    }

    /**
     * @param Basket $basket
     * @return boolean
     */
    public function canBeAdded(Basket $basket)
    {
        if ($this->isOk2show() == FALSE) {
            return FALSE;
        }

        return TRUE;
    }

    /**
     * Adding custom data in case of extend
     */
    protected function addCustomData()
    {
        parent::addCustomData();
        $this->typeId = $this->getProperty('typeId');

        // includes and associated products
        $arr = array('includedProducts');
        foreach ($arr as $val) {
            $products = $this->getProperty($val);
            $this->$val = explode(',', $products);
        }
    }

}
