<?php

class AdminFee extends Product
{
    const ADMIN_FEE_PRODUCT = 615;

    public function isOk2show()
    {
        if ($this->adminControler != 'AdminFeeeAdminControler') {
            return FALSE;
        }
        return parent::isOk2show();
    }

}
