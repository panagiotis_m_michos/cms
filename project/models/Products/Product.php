<?php

class Product extends BasketProduct
{
    const PRODUCT_FRAUD_PROTECTION = 201;
    const PRODUCT_BUSINESS_TOOLKIT = 1350;
    const PRODUCT_RENEWAL_SERVICE_ADDRESS = 806;
    const PRODUCT_RENEWAL_REGISTERED_OFFICE = 334;
    const PRODUCT_GUARANTEED_SAME_DAY = 589;

    /**
     * @var array
     */
    public static $paymantViaPhoneProductsAvailable = [
        self::PRODUCT_FRAUD_PROTECTION => 'Fraud Protection',
        self::PRODUCT_GUARANTEED_SAME_DAY => 'Guaranteed Same Day Formation',
    ];
}
