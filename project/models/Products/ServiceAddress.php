<?php

class ServiceAddress extends Product
{

    const SERVICE_ADDRESS_FOLDER = 474;

    /** @var string */
    public $premise;

    /** @var string */
    public $street;

    /** @var string */
    public $thoroughfare;

    /** @var string */
    public $post_town;

    /** @var string */
    public $county;

    /** @var string */
    public $postcode;

    /** @var string */
    public $country;

    protected function addCustomData()
    {
        parent::addCustomData();

        $this->premise = $this->getProperty('premise');
        $this->street = $this->getProperty('street');
        $this->thoroughfare = $this->getProperty('thoroughfare');
        $this->post_town = $this->getProperty('post_town');
        $this->county = $this->getProperty('county');
        $this->postcode = $this->getProperty('postcode');
        $this->country = $this->getProperty('country');
        //$this->country = isset(Address::$countries[$this->countryId]) ? Address::$countries[$this->countryId] : NULL;
    }

    public function saveCustomData($action)
    {
        parent::saveCustomData($action);
        $this->saveProperty('premise', $this->premise);
        $this->saveProperty('street', $this->street);
        $this->saveProperty('thoroughfare', $this->thoroughfare);
        $this->saveProperty('post_town', $this->post_town);
        $this->saveProperty('county', $this->county);
        $this->saveProperty('postcode', $this->postcode);
        $this->saveProperty('country', $this->country);
    }

    public function isOk2show()
    {
        if ($this->adminControler != 'ServiceAddressAdminControler') {
            return FALSE;
        }
        return parent::isOk2show();
    }

//	/**
//	 * Returns list of offices for e.g. radio buttons
//	 * @return array $offices
//	 */
//	static public function getOfficesList()
//	{
//		$ids = self::getChildsIds(RegisterOffice::REGISTER_OFFICE_FOLDER);
//		$offices = array();
//		foreach ($ids as $key=>$val) {
//			$self = new self($val);
//			if ($self->isOk2show()) {
//				$offices[$val] = $self->getLngTitle();
//			}
//		}
//		return $offices;
//	}
}
