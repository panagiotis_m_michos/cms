<?php

class Package extends Package_Abstract implements IPackage
{
    const PACKAGE_BRONZE = 109;
    const PACKAGE_BRONZE_PLUS = 110;
    const PACKAGE_SILVER = 111;
    const PACKAGE_GOLD = 112;
    const PACKAGE_PLATINUM = 113;
    const PACKAGE_DIAMOND = 279;
    const PACKAGE_RESERVE_COMPANY_NAME = 400;
    const PACKAGE_BASIC = 1313;
    const PACKAGE_DIGITAL = 1684;
    const PACKAGE_CONTRACTOR = 1694;
    const PACKAGE_PRINTED = 1314;
    const PACKAGE_PRIVACY = 1315;
    const PACKAGE_COMPREHENSIVE = 1316;
    const PACKAGE_ULTIMATE = 1317;
    const PACKAGE_FIVE_POUNDS = 1668;
    const PACKAGE_LIMITED_BY_GUARANTEE_WITHOUT_REGISTERED_OFFICE = 1175;
    const PACKAGE_LIMITED_BY_GUARANTEE = 379;
    const PACKAGE_LIMITED_LIABILITY_PARTNERSHIP = 436;

    const PACKAGE_INTERNATIONAL = 1430;
    const PACKAGE_INTERNATIONAL_BANKING = 1598;
    const PACKAGE_INTERNATIONAL_RESIDENT_DIRECTOR = 1599;

    const PACKAGE_WHOLESALE_STARTER = 1022;
    const PACKAGE_WHOLESALE_EXECUTIVE = 898;
    const PACKAGE_WHOLESALE_PROFESSIONAL = 899;

    const PACKAGE_RENEWAL_PRIVACY = 1353;
    const PACKAGE_RENEWAL_COMPREHENSIVE_ULTIMATE = 342;

    const PACKAGE_TYPE_OLD = 'OLD';
    const PACKAGE_TYPE_CRE = 'CRE';

    /**
     * @var array
     */
    public static $types = array(
        self::PACKAGE_BRONZE => 'Bronze',
        self::PACKAGE_BRONZE_PLUS => 'Bronze Plus',
        self::PACKAGE_SILVER => 'Silver',
        self::PACKAGE_GOLD => 'Gold',
        self::PACKAGE_PLATINUM => 'Platinum',
        self::PACKAGE_DIAMOND => 'Diamond',

        self::PACKAGE_BASIC => 'Basic',
        self::PACKAGE_DIGITAL => 'Digital',
        self::PACKAGE_PRINTED => 'Printed',
        self::PACKAGE_PRIVACY => 'Privacy',
        self::PACKAGE_COMPREHENSIVE => 'Comprehensive',
        self::PACKAGE_ULTIMATE => 'Ultimate',

        self::PACKAGE_FIVE_POUNDS => 'Five Pounds Formation',
        self::PACKAGE_CONTRACTOR => 'Contractor',

        self::PACKAGE_LIMITED_BY_GUARANTEE => 'Limited by Guarantee',
        self::PACKAGE_LIMITED_BY_GUARANTEE_WITHOUT_REGISTERED_OFFICE => 'Limited by Guarantee without Registered Office',
        self::PACKAGE_LIMITED_LIABILITY_PARTNERSHIP => 'Limited Liability Partnership',

        self::PACKAGE_INTERNATIONAL => 'International',
        self::PACKAGE_INTERNATIONAL_BANKING => 'International Banking',
        self::PACKAGE_INTERNATIONAL_RESIDENT_DIRECTOR => 'International Resident Director',

        self::PACKAGE_WHOLESALE_STARTER => 'Wholesale Starter',
        self::PACKAGE_WHOLESALE_PROFESSIONAL => 'Wholesale Professional',
        self::PACKAGE_WHOLESALE_EXECUTIVE => 'Wholesale Executive',
    );

    /**
     * @var array
     */
    public $associatedProducts1 = array();

    /**
     * @var array
     */
    public $associatedProducts2 = array();

    /**
     * @var array
     */
    public $associatedProducts3 = array();

    /**
     * @var int
     */
    private $offerProductId;

    /**
     * Adding custom data in case of extend
     * @return void
     */
    protected function addCustomData()
    {
        parent::addCustomData();
        $this->typeId = $this->getProperty('typeId');
        $this->upgradeText = $this->getProperty('upgradeText');
        $this->offerProductId = $this->getProperty('offerProductId');

        // includes and associated products
        $arr = array('includedProducts', 'associatedProducts1', 'associatedProducts2', 'associatedProducts3');
        foreach ($arr as $val) {
            $products = $this->getProperty($val);
            $this->$val = explode(',', $products);
        }

        // upgrade product
        $upgradeProduct = $this->getProperty('upgradeProductId');

        if ($upgradeProduct) {
            $this->upgradeProductId = $upgradeProduct;
            $this->upgradeProduct = new self($upgradeProduct);

            // change: stan, 05/11/09
            $this->upgradeProduct->isAssociated = TRUE;
        }
    }

    /**
     * @return bool
     */
    public function isPublished()
    {
        return $this->getStatusId() === self::STATUS_PUBLISHED;
    }

    public function saveCustomData($action)
    {
        parent::saveCustomData($action);
        $this->saveProperty('typeId', $this->typeId);
        $this->saveProperty('upgradeText', $this->upgradeText);
        $this->saveProperty('upgradeProductId', $this->upgradeProductId);
        $this->saveProperty('offerProductId', $this->offerProductId);

        // save products
        $arr = array('includedProducts', 'associatedProducts1', 'associatedProducts2', 'associatedProducts3');
        foreach ($arr as $key => $val) {
            $products = NULL;
            if (!empty($this->$val)) {
                $products = implode(',', $this->$val);
            }
            $this->saveProperty($val, $products);
        }
    }

    public function isOk2show()
    {
        if ($this->adminControler != 'PackageAdminControler') {
            return FALSE;
        }
        return parent::isOk2show();
    }

    /**
     * @param object Basket $basket
     * @return boolean
     */
    public function canBeAdded(Basket $basket)
    {
        if ($this->isOk2show() == FALSE || $this->isBlacklistedInBasket($basket)) {
            return FALSE;
        }

        // check only one package in basket
        foreach ($basket->items as $key => $val) {
            if ($val instanceof PackageItem) {
                return FALSE;
            }
        }

        // check only one as a quantity
        if ($this->inBasket($basket) !== FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * @param Basket $basket
     */
    public function removeExistingPackage(Basket $basket)
    {
        foreach ($basket->items as $key => $val) {
            if ($val instanceof self) {
                $basket->remove($key);
            }
        }
    }

    /**
     * @param Customer $customer
     * @return boolean
     */
    public function hasPackageFirstOrdered(Customer $customer)
    {
        $packageNew = TRUE;
        $orders = $customer->getCustomerOrderCount();
        if ($orders) {
            $packageNew = FALSE;
        }
        return $packageNew;
    }

    /**
     * @return mixed
     */
    public function getDowngradePackageId()
    {
        $array = $this->getChildsIds($this->getParentId());
        rsort($array);
        foreach ($array as $a) {
            if ($a < $this->nodeId) {
                return $a;
            }
        }
        return end($array); // or return NULL;
    }

    /**
     * @return Basket
     */
    public function addAssociatedProducts()
    {
        $basket = new Basket();
        $associatedProducts1 = $this->getPackageProducts('associatedProducts1');
        foreach ($associatedProducts1 as $key => $product) {
            $basket->add($product);
        }
        return $basket;
    }

    /**
     * @param Basket $basket
     * @return array
     */
    public function getNotInBasketAssociatedProducts3(Basket $basket)
    {
        $associatedProducts = $this->getPackageProducts('associatedProducts3');
        foreach ($associatedProducts as $key => &$product) {
            if ($product->inBasket($basket) !== FALSE) {
                unset($associatedProducts[$key]);
                continue;
            }
            $product->isAssociated = TRUE;
        }
        return $associatedProducts;
    }

    /**
     * @return int
     */
    public function getOfferProductId()
    {
        return $this->offerProductId;
    }

    /**
     * @param int $offerProductId
     */
    public function setOfferProductId($offerProductId)
    {
        $this->offerProductId = $offerProductId;
    }
}
