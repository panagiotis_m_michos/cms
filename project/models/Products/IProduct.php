<?php

interface IProduct
{
    /**
     * @return float
     */
    public function getPrice();
    
    /**
     * @return int
     */
    public function getCashBackAmount();
}
