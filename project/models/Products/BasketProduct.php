<?php

use Entities\Company;
use Entities\OrderItem;
use Entities\Payment\Token;
use Entities\Service;
use Nette\Utils\Arrays;
use OrderItem as OldOrderItem;

class BasketProduct extends FNode implements IProduct
{
    const LINK_EXISTING_SELECT = 'LINK_EXISTING_SELECT';
    const LINK_EXISTING_IMPORT = 'LINK_EXISTING_IMPORT';
    const LINK_NEW_SELECT = 'LINK_NEW_SELECT';
    const LINK_NEW_IMPORT = 'LINK_NEW_IMPORT';

    /**
     * @var array
     */
    public static $voServiceDurationsInMonths = [
        3 => '3 months',
        12 => '12 months'
    ];

    /**
     * @var array
     */
    public static $subscriptionTexts = [
        '+12 months' => 'Yearly subscription'
    ];

    /** @var integer */
    public $qty = 1;

    /** @var double */
    private $price = 0.00;

    /** @var double */
    public $productValue = 0.00;

    /** @var double */
    public $associatedPrice = 0.00;

    /** @var double */
    public $wholesalePrice = 0.00;

    /** @var int */
    private $saving = 0.00;

    /** @var boolean */
    public $onlyOneItem = FALSE;

    /** @var boolean */
    public $maxQuantityOne = FALSE;

    /** @var string */
    public $companyName;

    /** @var string */
    public $companyNumber;

    /**
     * @var int
     */
    private $companyId;

    /** @var int */
    public $conditionedById;

    /** @var object Product */
    public $conditionedBy;

    /** @var boolean */
    public $requiredCompanyNumber = FALSE;

    /** @var boolean */
    public $requiredIncorporatedCompanyNumber = FALSE;

    /** @var boolean */
    public $onlyOurCompanies = FALSE;

    /** @var boolean */
    public $saveToCompany = FALSE;

    /** @var string */
    public $responsibleEmails;

    /** @var boolean */
    public $notApplyVat = FALSE;

    /** @var int */
    public $nonVatableValue = 0;

    /** @var int */
    public $emailText;

    /** @var int */
    public $basketText;

    /** @var array */
    public $emailCmsFileAttachments = [];

    /** @var boolean */
    public $isAssociated = FALSE;

    /** @var string */
    public $additional;

    /** @var boolean */
    public $lockCompany = FALSE;

    /** @var string */
    public $upgradeDescription;

    /** @var int */
    public $upgradeImageId;

    /** @var int */
    public $associatedImageId;

    /** @var string */
    public $associatedText;

    /**
     * @var string
     */
    public $associatedDescription;

    /**
     * @var string
     */
    public $associatedIconClass;

    /** @var float   */
    public $markUp = 0.00;

    /** @var boolean */
    public $isFeefoEnabled = FALSE;

    /**
     * @var bool
     */
    public $isAutoRenewalAllowed = FALSE;

    /**
     * @var Token
     */
    public $autoRenewalToken;

    /**
     * @var string
     */
    public $linkedCompanyStatus;

    /**
     * @var OldOrderItem
     */
    public $oldOrderItem;

    /**
     * @var OrderItem
     */
    public $orderItem;

    /**
     * @var string
     */
    public $serviceTypeId;

    /**
     * @var string
     */
    private $duration;

    /**
     * @var DateTimeInterface
     */
    private $activateFrom;

    /**
     * @var int
     */
    public $renewalProductId;

    /**
     * @var Company
     */
    protected $companyEntity;

    /**
     * @var BasketProduct[]
     */
    public $blacklistedEquivalentProducts = [];

    /**
     * @var BasketProduct[]
     */
    public $blacklistedContainedProducts = [];

    /**
     * @var bool
     */
    protected $isVoServiceEligible = FALSE;

    /**
     * @var int
     */
    protected $voServiceDurationInMonths;

    /**
     * @var bool
     */
    public $isIdCheckRequired;

    /**
     * @var bool
     */
    public $bankingEnabled = FALSE;

    /**
     * @var array
     */
    public $bankingOptions = [];

    /**
     * @var bool
     */
    public $bankingRequired = FALSE;

    /**
     * @var int
     */
    private $cashBackAmount = 0;

    /**
     * @var bool
     */
    private $removableFromBasket = TRUE;

    /**
     * @var string
     */
    private $removeFromBasketConfirmation;

    /** @var double */
    private $offerPrice = 0.00;

    /** @var bool */
    private $isOffer = FALSE;

    protected function addCustomData()
    {
        parent::addCustomData();
        $this->price = $this->getProperty('price');
        $this->productValue = (double) $this->getProperty('productValue');
        $this->associatedPrice = (double) $this->getProperty('associatedPrice');
        $this->wholesalePrice = (double) $this->getProperty('wholesalePrice');
        $this->offerPrice = (double) $this->getProperty('offerPrice');
        $this->requiredCompanyNumber = $this->getProperty('requiredCompanyNumber');
        $this->requiredIncorporatedCompanyNumber = $this->getProperty('requiredIncorporatedCompanyNumber');
        $this->onlyOurCompanies = $this->getProperty('onlyOurCompanies');
        $this->saveToCompany = $this->getProperty('saveToCompany');
        $this->responsibleEmails = $this->getProperty('responsibleEmails');
        $this->nonVatableValue = $this->getProperty('nonVatableValue');
        $this->emailText = $this->getProperty('emailText');
        $this->basketText = $this->getProperty('basketText');
        $this->additional = $this->getProperty('additional');
        $this->lockCompany = (bool) $this->getProperty('lockCompany');
        $this->saving = $this->productValue - $this->getPrice();
        $this->upgradeDescription = $this->getProperty('upgradeDescription');
        $this->upgradeImageId = $this->getProperty('upgradeImageId');
        $this->markUp = $this->getProperty('markUp');
        $this->associatedImageId = $this->getProperty('associatedImageId');
        $this->associatedText = $this->getProperty('associatedText');
        $this->associatedDescription = $this->getProperty('associatedDescription');
        $this->associatedIconClass = (string) $this->getProperty('associatedIconClass');
        $this->isFeefoEnabled = $this->getProperty('isFeefoEnabled');
        $this->isAutoRenewalAllowed = $this->getProperty('isAutoRenewalAllowed');

        $arr = ['onlyOneItem', 'maxQuantityOne', 'notApplyVat'];
        foreach ($arr as $key => $property) {
            $value = $this->getProperty($property);
            if ($value != NULL) {
                $this->$property = (bool) $value;
            }
        }

        // blacklisted products
        $arr = ['blacklistedEquivalentProducts', 'blacklistedContainedProducts'];
        foreach ($arr as $val) {
            $products = $this->getProperty($val);
            $this->$val = explode(',', $products);
        }

        // conditioned by
        $this->conditionedById = $this->getProperty('conditionedBy');
        if ($this->conditionedById) {
            $this->conditionedBy = new Product($this->conditionedById);
        }

        // email attachments
        $emailCmsFileAttachments = $this->getProperty('emailCmsFileAttachments');
        if (!empty($emailCmsFileAttachments)) {
            $this->emailCmsFileAttachments = explode(',', $emailCmsFileAttachments);
        }

        $this->serviceTypeId = $this->getProperty('serviceTypeId');
        $this->setDuration($this->getProperty('duration'));
        $this->renewalProductId = $this->getProperty('renewalProductId');

        // vo eligible products
        $this->isVoServiceEligible = (bool) $this->getProperty('isVoServiceEligible');
        $this->voServiceDurationInMonths = (int) $this->getProperty('voServiceDurationInMonths');
        $this->isIdCheckRequired = $this->getProperty('isIdCheckRequired');

        $this->bankingEnabled = (bool) $this->getProperty('bankingEnabled');
        $this->bankingOptions = array_filter(explode(',', $this->getProperty('bankingOptions')));
        $this->bankingRequired = (bool) $this->getProperty('bankingRequired');
        $this->cashBackAmount = (int) $this->getProperty('cashBackAmount');

        $this->removableFromBasket = $this->getProperty('removableFromBasket') !== NULL ? $this->getProperty('removableFromBasket') : $this->removableFromBasket;
        $this->removeFromBasketConfirmation = (string) $this->getProperty('removeFromBasketConfirmation');
    }

    public function saveCustomData($action)
    {
        $this->saveProperty('price', $this->price);
        $this->saveProperty('nonVatableValue', $this->nonVatableValue);
        $this->saveProperty('associatedPrice', $this->associatedPrice);
        $this->saveProperty('wholesalePrice', $this->wholesalePrice);
        $this->saveProperty('offerPrice', $this->offerPrice);
        $this->saveProperty('productValue', $this->productValue);
        $this->saveProperty('requiredCompanyNumber', $this->requiredCompanyNumber);
        $this->saveProperty('requiredIncorporatedCompanyNumber', $this->requiredIncorporatedCompanyNumber);
        $this->saveProperty('onlyOurCompanies', $this->onlyOurCompanies);
        $this->saveProperty('saveToCompany', $this->saveToCompany);
        $this->saveProperty('onlyOneItem', $this->onlyOneItem);
        $this->saveProperty('maxQuantityOne', $this->maxQuantityOne);
        $this->saveProperty('conditionedById', $this->conditionedById);
        $this->saveProperty('responsibleEmails', $this->responsibleEmails);
        $this->saveProperty('notApplyVat', $this->notApplyVat);
        $this->saveProperty('emailText', $this->emailText);
        $this->saveProperty('basketText', $this->basketText);
        $this->saveProperty('additional', $this->additional);
        $this->saveProperty('upgradeDescription', $this->upgradeDescription);
        $this->saveProperty('upgradeImageId', $this->upgradeImageId);
        $this->saveProperty('markUp', $this->markUp);
        $this->saveProperty('associatedImageId', $this->associatedImageId);
        $this->saveProperty('associatedText', $this->associatedText);
        $this->saveProperty('associatedDescription', $this->associatedDescription);
        $this->saveProperty('associatedIconClass', $this->associatedIconClass);
        $this->saveProperty('isFeefoEnabled', $this->isFeefoEnabled);
        $this->saveProperty('isAutoRenewalAllowed', $this->isAutoRenewalAllowed);
        // email attachments
        //add
        if (!empty($this->emailCmsFileAttachments)) {
            $emailcmsFileAttachments = implode(',', $this->emailCmsFileAttachments);
            $this->saveProperty('emailCmsFileAttachments', $emailcmsFileAttachments);
            // remove
        } else {
            $this->saveProperty('emailCmsFileAttachments', '');
        }

        // save blacklisted products
        $arr = ['blacklistedEquivalentProducts', 'blacklistedContainedProducts'];
        foreach ($arr as $key => $val) {
            $products = NULL;
            if (!empty($this->$val)) {
                $products = implode(',', $this->$val);
            }
            $this->saveProperty($val, $products);
        }

        $this->saveProperty('lockCompany', $this->lockCompany);
        $this->saveProperty('serviceTypeId', $this->serviceTypeId);
        $this->saveProperty('duration', $this->getDuration());
        $this->saveProperty('renewalProductId', $this->renewalProductId);

        // vo eligible products
        $this->saveProperty('isVoServiceEligible', (bool) $this->isVoServiceEligible);
        $this->saveProperty('voServiceDurationInMonths', (int) $this->voServiceDurationInMonths);
        $this->saveProperty('isIdCheckRequired', $this->isIdCheckRequired);

        $this->saveProperty('bankingEnabled', $this->bankingEnabled);
        $this->saveProperty('bankingOptions', implode(',', $this->bankingOptions));
        $this->saveProperty('bankingRequired', $this->bankingRequired);
        $this->saveProperty('cashBackAmount', $this->cashBackAmount);

        $this->saveProperty('removableFromBasket', $this->removableFromBasket);
        $this->saveProperty('removeFromBasketConfirmation', $this->removeFromBasketConfirmation);
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function setSaving($saving)
    {
        $this->saving = $saving;
    }

    public function getPrice()
    {
        if ($this->isOffer() && !empty($this->offerPrice)) {
            return $this->offerPrice;
        } elseif ($this->isAssociated === TRUE) {
            return $this->associatedPrice;
        } elseif (Customer::isSignedIn() && Customer::getSignedIn()->isWholesale() && $this->wholesalePrice != NULL) {
            return $this->wholesalePrice;
        }
        return $this->price;
    }

    public function getSaving()
    {
        return $this->productValue - $this->getPrice();
    }

    public function getTotalPrice()
    {
        $price = $this->getPrice() * $this->qty;
        return Basket::round($price);
    }

    public function getOriginalPrice()
    {
        return $this->price;
    }

    /**
     * Returns longer title for product
     * @return string
     */
    public function getLongTitle()
    {
        $longTitle = $this->getLngTitle();
        if ($this->companyName !== NULL) {
            $longTitle .= ' for company "' . $this->companyName . '"';
        } elseif ($this->companyNumber !== NULL) {
            $longTitle .= ' for company "' . $this->companyNumber . '"';
        }
        return $longTitle;
    }

    /**
     * @param object Basket $basket
     * @return boolean
     */
    public function canBeAdded(Basket $basket)
    {
        if ($this->isOk2show() == FALSE  || $this->isBlacklistedInBasket($basket) || ($this->inBasket($basket) !== FALSE && $this->onlyOneItem)) {
            return FALSE;
        } elseif ($this->conditionedById != NULL) {
            $contitionedItem = self::getProductById($this->conditionedById);
            if ($contitionedItem->inBasket($basket) == FALSE) {
                return FALSE;
            }
        }
        return TRUE;
    }

    /**
     * @param Basket $basket
     * @return bool
     * @throws Exception
     */
    public function isBlackListedInBasket(Basket $basket)
    {
        foreach ($basket->getItems() as $item) {
            if (in_array($this->getId(), $item->blacklistedEquivalentProducts)) {
                $message = sprintf(
                    "Sorry, you are trying to purchase the %s but you already have the %s in your basket. At this time we only support the purchase of one company at a time.",
                    $this->getLngTitle(),
                    $item->getLngTitle()
                );
                throw new Exception($message);
            } elseif (in_array($this->getId(), $item->blacklistedContainedProducts)) {
                $message = sprintf(
                    'The "%s" is already included in your "%s", so we have removed the extra "%s" from your basket.',
                    $this->getLngTitle(),
                    $item->getLngTitle(),
                    $this->getLngTitle()
                );
                throw new Exception($message);
            }
        }
        return FALSE;
    }

    /**
     * @param Basket $basket
     * @return bool
     */
    public function shouldReplaceContainedBlacklistedProducts(Basket $basket)
    {
        foreach ($basket->getItems() as $item) {
            if (in_array($item->getId(), $this->blacklistedContainedProducts)) {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * @param object Basket $basket
     * @return boolean
     */
    public function canBeUpdated(Basket $basket)
    {
        if ($this->inBasket($basket) !== FALSE && $this->onlyOneItem || $this->maxQuantityOne) {
            return FALSE;
        }
        return TRUE;
    }

    /**
     * @param object Basket $basket
     * @return boolean
     */
    public function inBasket(Basket $basket)
    {
        foreach ($basket->items as $key => $item) {
            if ($item->getClass() == $this->getClass() && $this->getId() == $item->getId()) {
                return $key;
            }
        }
        return FALSE;
    }

    /**
     * @param int $qty
     * @return void
     */
    public function setQty($qty)
    {
        $this->qty = max(1, $qty);
    }

    /**
     * @return string
     */
    public function getVat()
    {
        // no VAT
        if ($this->notApplyVat == TRUE) {
            $vat = 0.00;
            // non vatable value
        } elseif ($this->nonVatableValue) {
            $price = $this->getTotalPrice() - $this->nonVatableValue;
            $vat = $price * Basket::VAT;
        } else {
            $vat = $this->getTotalPrice() * Basket::VAT;
        }
        return Basket::round($vat);
    }

    /**
     * @return string
     */
    public function getTotalVatPrice()
    {
        $price = $this->getTotalPrice() + $this->getVat();
        return Basket::round($price);
    }

    /**
     * Returns all products
     * @param boolean $returnAsObjects
     * @return array
     */
    public static function getAllProducts($returnAsObjects = FALSE)
    {
        $products = [];
        $blackList = ['FolderAdminControler', /* 'PackageAdminControler', */ 'JourneyProductAdminControler'];
        foreach (FNode::getChildsNodes(107) as $key => $val) {
            if (!in_array($val->adminControler, $blackList)) {
                $products[$key] = $val->getLngTitle();
            }
        }
        return $products;
    }

    /**
     * Returns all packages
     * @param boolean $returnAsObjects
     * @return array
     */
    public static function getAllPackages($returnAsObjects = FALSE)
    {
        $packages = [];
        $whiteList = ['PackageAdminControler', 'WholesalePackageAdminControler'];
        foreach (FNode::getChildsNodes(107) as $key => $val) {
            if (in_array($val->adminControler, $whiteList)) {
                $packages[$key] = $val->getLngTitle();
            }
        }
        return $packages;
    }

    /**
     * @return FImage
     */
    public function getUpgradeImageImage()
    {
        try {
            $image = new FImage($this->upgradeImageId);
        } catch (Exception $e) {
            $image = new FImage();
        }
        return $image;
    }

    public function getAssociatedProductImage()
    {
        try {
            $image = new FImage($this->associatedImageId);
        } catch (Exception $e) {
            $image = NULL;
        }
        return $image;
    }

    /**
     * @return bool
     */
    public function isFeefo()
    {
        return (bool) $this->isFeefoEnabled;
    }

    /**
     * @return bool
     */
    public function hasServiceType()
    {
        return (bool) $this->serviceTypeId;
    }

    /**
     * @return bool
     */
    public function hasRenewalProduct()
    {
        return (bool) $this->renewalProductId;
    }

    /**
     * @return Product
     */
    public function getRenewalProduct()
    {
        return FNode::getProductById($this->renewalProductId);
    }

    /**
     * @return Entities\Company
     */
    public function getCompanyEntity()
    {
        return $this->companyEntity;
    }

    /**
     * @param Entities\Company $companyEntity
     */
    public function setCompanyEntity(Entities\Company $companyEntity)
    {
        $this->companyEntity = $companyEntity;
    }

    /**
     * @return bool
     */
    public function isLinkedToExistingCompany()
    {
        return $this->linkedCompanyStatus == self::LINK_EXISTING_SELECT || $this->linkedCompanyStatus == self::LINK_EXISTING_IMPORT;
    }

    /**
     * @return bool
     */
    public function isLinkedToExistingImportedCompany()
    {
        return $this->linkedCompanyStatus == self::LINK_EXISTING_IMPORT;
    }

    /**
     * @return bool
     */
    public function isLinkedToNewImportedCompany()
    {
        return $this->linkedCompanyStatus == self::LINK_NEW_IMPORT;
    }

    /**
     * @return bool
     */
    public function isLinkedToNewCompany()
    {
        return $this->linkedCompanyStatus == self::LINK_NEW_IMPORT || $this->linkedCompanyStatus == self::LINK_NEW_SELECT;
    }

    /**
     * @return bool
     */
    public function isLinkedToImportedCompany()
    {
        return $this->linkedCompanyStatus == self::LINK_NEW_IMPORT || $this->linkedCompanyStatus == self::LINK_EXISTING_IMPORT;
    }

    /**
     * @return string
     */
    public function getSubscriptionPeriodText()
    {
        return Arrays::get(self::$subscriptionTexts, $this->getDuration(), '');
    }

    /**
     * @param boolean $isVoServiceEligible
     */
    public function setVoServiceEligible($isVoServiceEligible)
    {
        $this->isVoServiceEligible = (bool) $isVoServiceEligible;
    }

    /**
     * @param int $voServiceDurationInMonths
     */
    public function setVoServiceDurationInMonths($voServiceDurationInMonths)
    {
        $this->voServiceDurationInMonths = (int) $voServiceDurationInMonths;
    }

    /**
     * @return boolean
     */
    public function isVoServiceEligible()
    {
        return $this->isVoServiceEligible;
    }

    /**
     * @return int
     */
    public function getVoServiceDurationInMonths()
    {
        return $this->voServiceDurationInMonths;
    }

    /**
     * @return bool
     */
    public function isFree()
    {
        return $this->price == 0;
    }

    /**
     * @return bool
     */
    public function isPackage()
    {
        return $this instanceof IPackage;
    }

    /**
     * @return bool
     */
    public function isProduct()
    {
        return !$this->isPackage();
    }

    /**
     * @return int
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * @param int $companyId
     */
    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;
    }

    /**
     * @return string
     */
    public function getServiceTypeId()
    {
        return $this->serviceTypeId;
    }

    /**
     * @return OrderItem
     */
    public function getOrderItem()
    {
        return $this->orderItem;
    }

    /**
     * @param OrderItem $orderItem
     */
    public function setOrderItem(OrderItem $orderItem)
    {
        $this->orderItem = $orderItem;
    }

    /**
     * @return int
     */
    public function getCashBackAmount()
    {
        return $this->cashBackAmount;
    }

    /**
     * @param int $cashBackAmount
     */
    public function setCashBackAmount($cashBackAmount)
    {
        $this->cashBackAmount = $cashBackAmount;
    }

    /**
     * @param $removable
     */
    public function setRemovableFromBasket($removable)
    {
        $this->removableFromBasket = $removable;
    }

    /**
     * @return bool
     */
    public function isRemovableFromBasket()
    {
        return $this->removableFromBasket;
    }

    /**
     * @return string
     */
    public function getRemoveFromBasketConfirmation()
    {
        return $this->removeFromBasketConfirmation;
    }

    /**
     * @param string $confirmation
     */
    public function setRemoveFromBasketConfirmation($confirmation)
    {
        $this->removeFromBasketConfirmation = $confirmation;
    }

    /**
     * @return bool
     */
    public function hasRemoveFromBasketConfirmation()
    {
        return (bool) $this->removeFromBasketConfirmation;
    }

    /**
     * @return string
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param string $duration
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    }

    /**
     * @param DateTimeInterface $activateFrom
     */
    public function setActivateFrom(DateTimeInterface $activateFrom)
    {
        $this->activateFrom = $activateFrom;
    }

    /**
     * @return DateTimeInterface
     */
    public function getActivateFrom()
    {
        return $this->activateFrom;
    }

    /**
     * @return bool
     */
    public function isPackagePrivacyType()
    {
        return $this->serviceTypeId == Service::TYPE_PACKAGE_PRIVACY;
    }

    /**
     * @return bool
     */
    public function isPackageComprehensiveType()
    {
        return $this->serviceTypeId == Service::TYPE_PACKAGE_COMPREHENSIVE_ULTIMATE;
    }

    /**
     * @return bool
     */
    public function isServiceWithAutoRenewalAllowed()
    {
        return $this->serviceTypeId && $this->isAutoRenewalAllowed;
    }

    /**
     * @return float
     */
    public function getOfferPrice()
    {
        return empty($this->offerPrice) ? $this->price : $this->offerPrice;
    }

    /**
     * @param float $offerPrice
     */
    public function setOfferPrice($offerPrice)
    {
        $this->offerPrice = $offerPrice;
    }

    /**
     * @return boolean
     */
    public function isOffer()
    {
        return $this->isOffer;
    }

    public function markAsOffer()
    {
        $this->isOffer = TRUE;
    }

    public function unmarkAsOffer()
    {
        $this->isOffer = FALSE;
    }
}
