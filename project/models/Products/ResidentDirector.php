<?php

class ResidentDirector extends NomineeDirector
{
    public static $nomineeType = 'Resident Director';

    /**
     * @return bool
     */
    public function isOk2show()
    {
        if ($this->adminControler != 'ResidentDirectorAdminControler') {
            return FALSE;
        }

        return FNode::isOk2show();
    }

    /**
     * @return string
     */
    public function getNomineeType()
    {
        return self::$nomineeType;
    }
}
