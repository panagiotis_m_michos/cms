<?php

class CertificateUpgrade extends Product
{

    const CERTIFICATE_UPGRADE_PRODUCT = 870;

    public function isOk2show()
    {
        if ($this->adminControler != 'CertificateUpgradeAdminControler') {
            return FALSE;
        }
        return parent::isOk2show();
    }

}
