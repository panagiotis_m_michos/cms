<?php

class RegisterOffice extends Product
{

    const REGISTER_OFFICE_FOLDER = 164;

    /** @var string */
    public $address1;

    /** @var string */
    public $address2;

    /** @var string */
    public $address3;

    /** @var string */
    public $town;

    /** @var string */
    public $county;

    /** @var string */
    public $postcode;

    /** @var int */
    public $countryId;

    /** @var string */
    public $country;

    protected function addCustomData()
    {
        parent::addCustomData();

        $this->address1 = $this->getProperty('address1');
        $this->address2 = $this->getProperty('address2');
        $this->address3 = $this->getProperty('address3');
        $this->town = $this->getProperty('town');
        $this->county = $this->getProperty('county');
        $this->postcode = $this->getProperty('postcode');
        $this->countryId = $this->getProperty('countryId');
        $this->country = isset(Address::$countries[$this->countryId]) ? Address::$countries[$this->countryId] : NULL;
    }

    public function saveCustomData($action)
    {
        parent::saveCustomData($action);
        $this->saveProperty('address1', $this->address1);
        $this->saveProperty('address2', $this->address2);
        $this->saveProperty('address3', $this->address3);
        $this->saveProperty('town', $this->town);
        $this->saveProperty('county', $this->county);
        $this->saveProperty('postcode', $this->postcode);
        $this->saveProperty('countryId', $this->countryId);
    }

    public function isOk2show()
    {
        if ($this->adminControler != 'RegisterOfficeAdminControler') {
            return FALSE;
        }
        return parent::isOk2show();
    }

    /**
     * Returns list of offices for e.g. radio buttons
     * @return array $offices
     */
    static public function getOfficesList()
    {
        $ids = self::getChildsIds(RegisterOffice::REGISTER_OFFICE_FOLDER);
        $offices = array();
        foreach ($ids as $key => $val) {
            $self = new self($val);
            if ($self->isOk2show()) {
                $offices[$val] = $self->getLngTitle();
            }
        }
        return $offices;
    }

}
