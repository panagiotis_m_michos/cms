<?php

class CompanyNameChange extends Product
{

    const CHANGE_NAME_PRODUCT = 843;
    const CHANGE_NAME_SAME_DAY_PRODUCT = 844;

    public function isOk2show()
    {
        if ($this->adminControler != 'CompanyNameChangeAdminControler') {
            return FALSE;
        }
        return parent::isOk2show();
    }

}
