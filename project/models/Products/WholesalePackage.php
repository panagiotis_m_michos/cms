<?php

class WholesalePackage extends Package
{
    /**
     * @return bool
     */
    public function isOk2show()
    {
        if ($this->adminControler != 'WholesalePackageAdminControler') {
            return FALSE;
        }
        return FNode::isOk2show();
    }
}
