<?php

class NomineeSecretary extends Product
{

    const NOMINEE_FOLDER = 169;

    /*     * *********** corporate ************** */

    /** @var string */
    public $corporateCompanyName;

    /** @var string */
    public $corporateFirstName;

    /** @var string */
    public $corporateLastName;

    /** @var string */
    public $corporateAddress1;

    /** @var string */
    public $corporateAddress2;

    /** @var string */
    public $corporateAddress3;

    /** @var string */
    public $corporateTown;

    /** @var string */
    public $corporateCounty;

    /** @var string */
    public $corporatePostcode;

    /** @var int */
    public $corporateCountryId;

    /** @var string */
    public $corporateCountry;

    /** @var string */
    public $corporateBirtown;

    /** @var string */
    public $corporateAuthTel;

    /** @var string */
    public $corporateAuthEye;

    /** @var string */
    public $countryRegistered;

    /** @var string */
    public $registrationNumber;

    protected function addCustomData()
    {
        parent::addCustomData();
        $this->corporateCompanyName = $this->getProperty('corporateCompanyName');
        $this->corporateFirstName = $this->getProperty('corporateFirstName');
        $this->corporateLastName = $this->getProperty('corporateLastName');
        $this->corporateAddress1 = $this->getProperty('corporateAddress1');
        $this->corporateAddress2 = $this->getProperty('corporateAddress2');
        $this->corporateAddress3 = $this->getProperty('corporateAddress3');
        $this->corporateTown = $this->getProperty('corporateTown');
        $this->corporateCounty = $this->getProperty('corporateCounty');
        $this->corporatePostcode = $this->getProperty('corporatePostcode');
        $this->corporateCountryId = $this->getProperty('corporateCountryId');
        $this->corporateCountry = $this->getProperty('corporateCountry');
        $this->corporateBirtown = $this->getProperty('corporateBirtown');
        $this->corporateAuthTel = $this->getProperty('corporateAuthTel');
        $this->corporateAuthEye = $this->getProperty('corporateAuthEye');
        $this->countryRegistered = $this->getProperty('countryRegistered');
        $this->registrationNumber = $this->getProperty('registrationNumber');
    }

    public function saveCustomData($action)
    {
        parent::saveCustomData($action);
        $this->saveProperty('corporateCompanyName', $this->corporateCompanyName);
        $this->saveProperty('corporateFirstName', $this->corporateFirstName);
        $this->saveProperty('corporateLastName', $this->corporateLastName);
        $this->saveProperty('corporateAddress1', $this->corporateAddress1);
        $this->saveProperty('corporateAddress2', $this->corporateAddress2);
        $this->saveProperty('corporateAddress3', $this->corporateAddress3);
        $this->saveProperty('corporateTown', $this->corporateTown);
        $this->saveProperty('corporateCounty', $this->corporateCounty);
        $this->saveProperty('corporatePostcode', $this->corporatePostcode);
        $this->saveProperty('corporateCountryId', $this->corporateCountryId);
        $this->saveProperty('corporateAuthTel', $this->corporateAuthTel);
        $this->saveProperty('corporateBirtown', $this->corporateBirtown);
        $this->saveProperty('corporateAuthEye', $this->corporateAuthEye);
        $this->saveProperty('countryRegistered', $this->countryRegistered);
        $this->saveProperty('registrationNumber', $this->registrationNumber);
    }

    public function isOk2show()
    {
        if ($this->adminControler != 'NomineeSecretaryAdminControler') {
            return FALSE;
        }
        return parent::isOk2show();
    }

}
