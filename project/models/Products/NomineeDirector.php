<?php

class NomineeDirector extends NomineeSecretary
{
    public static $nomineeType = 'Nominee Director';

    /** @var string */
    public $personFirstname;

    /** @var string */
    public $personSurname;

    /** @var string */
    public $personNationality;

    /** @var string */
    public $personDob;

    /** @var string */
    public $personOccupation;

    /** @var string */
    public $personAddress1;

    /** @var string */
    public $personAddress2;

    /** @var string */
    public $personAddress3;

    /** @var string */
    public $personTown;

    /** @var string */
    public $personCounty;

    /** @var string */
    public $personPostcode;

    /** @var int */
    public $personCountryId;

    /** @var string */
    public $personCountry;

    /** @var string */
    public $personCountryOfResidence;

    /** @var string */
    public $personBirtown;

    /** @var string */
    public $personAuthTel;

    /** @var string */
    public $personAuthEye;

    /**
     * @var int
     */
    protected $isCorporate;

    /**
     * @var int
     */
    protected $includedByDefault;

    protected function addCustomData()
    {
        parent::addCustomData();
        $this->personFirstname = $this->getProperty('personFirstname');
        $this->personSurname = $this->getProperty('personSurname');
        $this->personNationality = $this->getProperty('personNationality');
        $this->personDob = $this->getProperty('personDob');
        $this->personOccupation = $this->getProperty('personOccupation');
        $this->personAddress1 = $this->getProperty('personAddress1');
        $this->personAddress2 = $this->getProperty('personAddress2');
        $this->personAddress3 = $this->getProperty('personAddress3');
        $this->personTown = $this->getProperty('personTown');
        $this->personCounty = $this->getProperty('personCounty');
        $this->personPostcode = $this->getProperty('personPostcode');
        $this->personCountryId = $this->getProperty('personCountryId');
        $this->personCountry = $this->getProperty('personCountry');
        $this->personCountryOfResidence = $this->getProperty('personCountryOfResidence');
        $this->personBirtown = $this->getProperty('personBirtown');
        $this->personAuthTel = $this->getProperty('personAuthTel');
        $this->personAuthEye = $this->getProperty('personAuthEye');

        $this->setIsCorporate($this->getProperty('isCorporate'));
        $this->includeByDefault($this->getProperty('includedByDefault'));
    }

    public function saveCustomData($action)
    {
        parent::saveCustomData($action);
        $this->saveProperty('personFirstname', $this->personFirstname);
        $this->saveProperty('personSurname', $this->personSurname);
        $this->saveProperty('personNationality', $this->personNationality);
        $this->saveProperty('personDob', $this->personDob);
        $this->saveProperty('personOccupation', $this->personOccupation);
        $this->saveProperty('personAddress1', $this->personAddress1);
        $this->saveProperty('personAddress2', $this->personAddress2);
        $this->saveProperty('personAddress3', $this->personAddress3);
        $this->saveProperty('personTown', $this->personTown);
        $this->saveProperty('personCounty', $this->personCounty);
        $this->saveProperty('personPostcode', $this->personPostcode);
        $this->saveProperty('personCountryId', $this->personCountryId);
        $this->saveProperty('personBirtown', $this->personBirtown);
        $this->saveProperty('personAuthTel', $this->personAuthTel);
        $this->saveProperty('personAuthEye', $this->personAuthEye);
        $this->saveProperty('personCountryOfResidence', $this->personCountryOfResidence);

        $this->saveProperty('isCorporate', $this->isCorporate);
        $this->saveProperty('includedByDefault', $this->includedByDefault);
    }

    public function isOk2show()
    {
        if ($this->adminControler != 'NomineeDirectorAdminControler') {
            return FALSE;
        }

        return FNode::isOk2show();
    }


    /**
     * @return bool
     */
    public function isCorporate()
    {
        return (bool) $this->isCorporate;
    }

    /**
     * @param bool $isCorporate
     */
    public function setIsCorporate($isCorporate = TRUE)
    {
        $this->isCorporate = $isCorporate ? 1 : 0;
    }

    /**
     * @return bool
     */
    public function isIncludedByDefault()
    {
        return (bool) $this->includedByDefault;
    }

    /**
     * @param bool $includedByDefault
     */
    public function includeByDefault($includedByDefault = TRUE)
    {
        $this->includedByDefault = $includedByDefault ? 1 : 0;
    }

    /**
     * @return string
     */
    public function getNomineeType()
    {
        return self::$nomineeType;
    }
}
