<?php

namespace Mapper;

use Entities\Shareholding;
use dibi;

class ShareholdingMapper extends AbstractDbMapper implements MapperInterface
{

    protected $tableName = 'ch_annual_return_shareholding';

    /**
     * @param int $id
     * @return Shareholding
     */
    public function findById($id)
    {
        $e = $this->getDibi()->select('*');
        $e->from($this->tableName);
        $e->where('annual_return_shareholding_id=%i', $id);
        $e->execute()->fetch();
        return $this->createShareholding($e);
    }

    /**
     * @param array $where
     * @param type $limit
     * @param type $offset
     * @return array
     */
    public function findAll(array $where = array(), $limit = NULL, $offset = NULL)
    {
        $query = $this->dibi->select('*')->from($this->tableName);

        foreach ($where as $key => $val) {
            $val = '%' . $val . '%';
            $query->where('%n', $key, ' LIKE %s', $val);
        }
        if ($limit !== NULL) {
            $query->limit($limit);
        }
        if ($offset !== NULL) {
            $query->offset($offset);
        }
        $rows = $query->execute()->fetchAll();

        return $this->createShareholdingCollection($rows);
    }

    /**
     * @param Shareholding $entity
     * @return int
     */
    public function save(Shareholding $entity)
    {
        $data = array();
        $data['form_submission_id'] = $entity->getFormSubmissionId();
        $data['share_class'] = $entity->getShareClass();
        $data['number_held'] = $entity->getNumberHeld();
        $data['sync'] = $entity->getSync();

        if ($this->isNew($entity)) {
            $id = $this->dibi->insert($this->tableName, $data)->execute(dibi::IDENTIFIER);
            $entity->setId($id);
        } else {
            $this->dibi->update($this->tableName, $data)->where('annual_return_shareholding_id=%i', $entity->getId())->execute();
        }
        return $entity->getId();
    }

    /**
     * @param array $e
     * @return Shareholding
     */
    public function createShareholding($e)
    {
        $shareholdings = new Shareholding($e->share_class, $e->number_held);
        $shareholdings->setId($e->annual_return_shareholding_id);
        $shareholdings->setSync($e->sync);
        $shareholdings->setFormSubmissionId($e->form_submission_id);
        return $shareholdings;
    }

    /**
     * @param array $rows
     * @return array
     */
    protected function createShareholdingCollection(array $rows)
    {
        $collection = array();
        if ($rows) {
            foreach ($rows as $row) {
                $collection[] = $this->createShareholding($row);
            }
        }
        return $collection;
    }

}
