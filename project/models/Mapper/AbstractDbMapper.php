<?php

namespace Mapper;

use Entities\EntityInterface;
use dibi;

abstract class AbstractDbMapper
{

    /**
     * @var dibi
     */
    protected $dibi;

    public function __construct()
    {
        $this->dibi = dibi::getConnection();
    }

    /**
     * @return bool
     */
    public function isNew(EntityInterface $entity)
    {
        return ($entity->getId() == 0);
    }

}
