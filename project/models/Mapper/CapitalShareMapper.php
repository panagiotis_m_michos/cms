<?php

namespace Mapper;

use Entities\CapitalShare;
use Mapper\CapitalMapper;
use dibi;

class CapitalShareMapper extends AbstractDbMapper implements MapperInterface
{

    protected $tableName = 'ch_annual_return_shares';

    /**
     * @var CapitalMapper
     */
    protected $capitalMapper;

    /**
     * @param int $id
     * @return CapitalShare
     */
    public function findById($id)
    {
        $e = $this->dibi->select('*');
        $e->from($this->tableName);
        $e->where('annual_return_shares_id=%i', $id);
        $share = $e->execute()->fetch();
        $capitalShare = $this->createCapitalShare($share);

        $capital = $this->capitalMapper->createCapital(array($share->currency, $share->form_submission_id));
        $capital->setShares(array($capitalShare));
        $capitalShare->setCapital($capital);
        return $capitalShare;
    }

    /**
     * @param array $where
     * @param string $limit
     * @param string $offset
     * @return array
     */
    public function findAll(array $where = array(), $limit = NULL, $offset = NULL)
    {
        $query = $this->dibi->select('*')->from($this->tableName);

        foreach ($where as $key => $val) {
            $val = '%' . $val . '%';
            $query->where('%n', $key, ' LIKE %s', $val);
        }
        if ($limit !== NULL) {
            $query->limit($limit);
        }
        if ($offset !== NULL) {
            $query->offset($offset);
        }
        $shares = $query->execute()->fetchAll();

        return $this->createCapitalShareCollection($shares);
    }

    /**
     * @param CapitalShare $entity
     * @return int
     */
    public function save(CapitalShare $entity)
    {
        $data = array();
        $data['form_submission_id'] = $entity->getCapital()->getFormSubmissionId();
        $data['share_class'] = $entity->getShareClass();
        $data['prescribed_particulars'] = $entity->getPrescribedParticulars();
        $data['num_shares'] = $entity->getNumShares();
        $data['amount_paid'] = $entity->getAmountPaidDuePerShare();
        $data['amount_unpaid'] = $entity->getAmountUnpaidPerShare();
        $data['currency'] = $entity->getCapital()->getShareCurrency();
        $data['aggregate_nom_value'] = $entity->getAggregateNominalValue();
        $data['paid'] = $entity->getPaid();

        if ($this->isNew($entity)) {
            $data['sync_paid'] = CapitalShare::STATUS_UNPAID;
            $id = $this->dibi->insert($this->tableName, $data)->execute(dibi::IDENTIFIER);
            $entity->setId($id);
        } else {
            $this->dibi->update($this->tableName, $data)->where('annual_return_shares_id=%i', $entity->getId())->execute();
        }
        return $entity->getId();
    }

    /**
     * @param CapitalShare $entity
     */
    public function delete(CapitalShare $entity)
    {
        $this->dibi->delete($this->tableName)->where('annual_return_shares_id=%i', $entity->getId())->execute();
    }

    /**
     * @param array $row
     * @return CapitalShare
     */
    public function createCapitalShare($row)
    {
        $value = $row->aggregate_nom_value / $row->num_shares;
        $capitalShare = new CapitalShare($row->share_class, $row->prescribed_particulars, $row->num_shares, $value, $row->paid);
        $capitalShare->setId($row->annual_return_shares_id);
        //$capitalShare->setShareCurrency($row->currency);
        $capitalShare->setSyncPaid($row->sync_paid);
        $capitalShare->setAmountPaidDuePerShare($row->amount_paid);
        $capitalShare->setAmountUnpaidPerShare($row->amount_unpaid);
        $capitalShare->setAggregateNominalValue($row->aggregate_nom_value);
        return $capitalShare;
    }

    /**
     * @param array $rows
     * @return array
     */
    public function createCapitalShareCollection(array $rows)
    {
        $collection = array();
        if ($rows) {
            foreach ($rows as $row) {
                $capitalShare = $this->createCapitalShare($row);
                $capital = $this->capitalMapper->createCapital(array($row->currency, $row->form_submission_id));
                $capital->setShares(array($capitalShare));
                $capitalShare->setCapital($capital);
                $collection[] = $capitalShare;
            }
        }
        return $collection;
    }

    /**
     * @param CapitalMapper  $capitalMapper
     */
    public function setCapitalMapper(CapitalMapper $capitalMapper)
    {
        $this->capitalMapper = $capitalMapper;
    }

}
