<?php

namespace Mapper;

use Entities\EntityInterface;

interface MapperInterface
{

    public function isNew(EntityInterface $entity);

    public function findById($id);

    public function findAll(array $where = array(), $limit = NULL, $offset = NULL);
}
