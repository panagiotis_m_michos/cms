<?php

namespace Mapper;

use Entities\Capital;
use Mapper\CapitalShareMapper;
use dibi;

class CapitalMapper extends AbstractDbMapper implements MapperInterface
{

    /**
     * @var CapitalShareMapper
     */
    protected $capitalShareMapper;

    /**
     * @var string
     */
    protected $tableName = 'ch_annual_return_shares';

    /**
     * @param string $id
     * @return Capital
     */
    public function findById($id)
    {
        $capitalClues = explode("_", $id);
        $capital = $this->createCapital($capitalClues);
        $rows = $this->capitalShareMapper->findAll(array("form_submission_id" => $capitalClues[1], 'currency' => $capitalClues[0]));
        $capital->setShares($rows);
        return $capital;
    }

    /**
     * @param array $where
     * @param string $limit
     * @param string $offset
     * @return array
     */
    public function findAll(array $where = array(), $limit = NULL, $offset = NULL)
    {
        $query = $this->dibi->select('*')->from($this->tableName);

        foreach ($where as $key => $val) {
            $val = '%' . $val . '%';
            $query->where('%n', $key, ' LIKE %s', $val);
        }
        if ($limit !== NULL) {
            $query->limit($limit);
        }
        if ($offset !== NULL) {
            $query->offset($offset);
        }
        $rows = $query->execute()->fetchAssoc('currency');

        return $this->createCapitalCollection($rows);
    }

    /**
     * @param array $capitalClues
     * @return Capital
     */
    public function createCapital($capitalClues)
    {
        $capital = new Capital($capitalClues[0], $capitalClues[1]);
        $capital->setId($capitalClues[0] . '_' . $capitalClues[1]);
        return $capital;
    }

    /**
     * @param array $rows
     * @return array
     */
    protected function createCapitalCollection(array $rows)
    {
        $collection = array();
        if ($rows) {
            foreach ($rows as $row) {
                $capital = $this->createCapital(array($row->currency, $row->form_submission_id));
                $shares = $this->capitalShareMapper->findAll(array("form_submission_id" => $row->form_submission_id, 'currency' => $row->currency));
                $capital->setShares($shares);

                $collection[] = $capital;
            }
        }
        return $collection;
    }

    /**
     * @param CapitalShareMapper $capitalShareMapper
     */
    public function setCapitalShareMapper($capitalShareMapper)
    {
        $this->capitalShareMapper = $capitalShareMapper;
    }

}
