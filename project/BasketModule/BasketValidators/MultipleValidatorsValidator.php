<?php

namespace BasketModule\BasketValidators;

use Basket;

class MultipleValidatorsValidator implements IValidator
{
    /**
     * @var IValidator[]
     */
    private $validators;

    /**
     * @param IValidator[] $validators
     */
    public function __construct(array $validators)
    {
        $this->validators = $validators;
    }

    /**
     * @param Basket $basket
     * @return ValidationResult
     */
    public function validate(Basket $basket)
    {
        $errors = [];

        foreach ($this->validators as $validator) {
            $errors = array_merge($errors, $validator->validate($basket)->getErrors());
        }

        return new ValidationResult($errors);
    }
}
