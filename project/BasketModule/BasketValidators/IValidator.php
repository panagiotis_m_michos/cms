<?php
namespace BasketModule\BasketValidators;

use Basket;

interface IValidator
{
    /**
     * @param Basket $basket
     * @return ValidationResult
     */
    public function validate(Basket $basket);
}
