<?php

namespace BasketModule\BasketValidators;

use Basket;
use Entities\Customer;
use Package;

class FivePoundFormationValidator implements IValidator
{
    /**
     * @var Customer
     */
    private $customer;

    /**
     * @param Customer $customer
     */
    public function __construct(Customer $customer = NULL)
    {
        $this->customer = $customer;
    }

    /**
     * @param Basket $basket
     * @return ValidationResult
     */
    public function validate(Basket $basket)
    {
        $errors = [];

        if ($this->customer) {
            if ($basket->hasPackage(Package::PACKAGE_FIVE_POUNDS) && $this->customer->hasCompanies()) {
                //todo: take this from variable provider?
                $errors[] = "Sorry, it looks like you already have a company on your account. The £5 formation package offer is only valid for first time company owners. But you can still take advantage of our <a href=\"/company-formation-name-search.html\">great value packages here</a>.";
            }
        }

        return new ValidationResult($errors);
    }
}
