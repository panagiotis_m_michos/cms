<?php

namespace BasketModule\Views;

use Basket;
use BasketModule\Deciders\GuaranteedSameDayDecider;
use BasketModule\Deciders\OfferDecider;
use BasketProduct;
use DusanKasan\Knapsack\Collection;
use Services\PackageService;
use Symfony\Component\HttpFoundation\Request;

class PackageBasketView
{
    /**
     * @var Basket
     */
    private $basket;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var GuaranteedSameDayDecider
     */
    private $guaranteedSameDayDecider;

    /**
     * @var OfferDecider
     */
    private $offerDecider;

    /**
     * @var PackageService
     */
    private $packageService;

    /**
     * @param Basket $basket
     * @param GuaranteedSameDayDecider $guaranteedSameDayDecider
     * @param Request $request
     * @param OfferDecider $offerDecider
     * @param PackageService $packageService
     */
    public function __construct(
        Basket $basket,
        GuaranteedSameDayDecider $guaranteedSameDayDecider,
        Request $request,
        OfferDecider $offerDecider,
        PackageService $packageService
    )
    {
        $this->basket = $basket;
        $this->request = $request;
        $this->guaranteedSameDayDecider = $guaranteedSameDayDecider;
        $this->offerDecider = $offerDecider;
        $this->packageService = $packageService;
    }

    /**
     * @return bool
     */
    public function canShowVoucherForm()
    {
        return !$this->basket->getVoucher()
            && ($this->request->cookies->get('upgrade-voucher-form') || $this->request->query->has('redeem'));
    }

    /**
     * @return bool
     */
    public function canAddGuaranteedSameDay()
    {
        return $this->guaranteedSameDayDecider->canBeAdded();
    }

    /**
     * @return bool
     */
    public function canAddOffer()
    {
        return $this->offerDecider->canAddOffer();
    }

    /**
     * @return BasketProduct
     */
    public function getOffer()
    {
        return $this->offerDecider->getOffer();
    }

    /**
     * @return array
     */
    public function getUpsellProducts()
    {
        $package = $this->basket->getPackageOrNull();
        $offerProductId = $package->getOfferProductId();
        $associatedProducts = $this->packageService->getAssociatedProductsNotInBasket($package, $this->basket);

        foreach ($associatedProducts as $product) {
            if ($this->canAddGuaranteedSameDay() || ($offerProductId != $product->getId())) {
                yield $product;
            }
        }
    }
}
