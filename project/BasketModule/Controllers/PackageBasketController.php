<?php

namespace BasketModule\Controllers;

use Basket;
use BasketModule\Forms\Data\VoucherFormData;
use BasketModule\Forms\VoucherForm;
use BasketModule\Views\PackageBasketView;
use BasketProduct;
use Exception;
use Exceptions\Business\BasketException;
use Package;
use Product;
use SearchControler;
use Services\ControllerHelper;
use Services\NodeService;
use Services\PackageService;
use Services\ProductService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Validator\Validator;
use TemplateModule\Renderers\IRenderer;
use VoucherModule\Services\VoucherService;

class PackageBasketController
{
    /**
     * @var IRenderer
     */
    private $renderer;

    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    /**
     * @var Basket
     */
    private $basket;

    /**
     * @var PackageService
     */
    private $packageService;

    /**
     * @var ProductService
     */
    private $nodeService;

    /**
     * @var VoucherService
     */
    private $voucherService;

    /**
     * @var PackageBasketView
     */
    private $packageBasketView;

    /**
     * @param IRenderer $renderer
     * @param ControllerHelper $controllerHelper
     * @param Basket $basket
     * @param PackageService $packageService
     * @param NodeService $nodeService
     * @param VoucherService $voucherService
     * @param PackageBasketView $packageBasketView
     */
    public function __construct(
        IRenderer $renderer,
        ControllerHelper $controllerHelper,
        Basket $basket,
        PackageService $packageService,
        NodeService $nodeService,
        VoucherService $voucherService,
        PackageBasketView $packageBasketView
    )
    {
        $this->renderer = $renderer;
        $this->controllerHelper = $controllerHelper;
        $this->basket = $basket;
        $this->packageService = $packageService;
        $this->nodeService = $nodeService;
        $this->voucherService = $voucherService;
        $this->packageBasketView = $packageBasketView;
    }

    /**
     * @return RedirectResponse
     */
    public function basket()
    {
        $request = $this->controllerHelper->getRequest();
        $package = $this->basket->getPackageOrNull();
        $packageId = $package ? $package->getId() : NULL;

        if (!$this->basket->hasPackageIncluded()) {
            return new RedirectResponse($this->controllerHelper->getLink(SearchControler::SEARCH_PAGE));
        }

        if ($packageId != $request->query->get('package_id')) {
            return new RedirectResponse($this->controllerHelper->getUrl('basket_module_package_basket_upgrade', ['package_id' => $packageId]));
        }

        $voucherForm = $this->controllerHelper->buildForm(new VoucherForm, new VoucherFormData);
        if ($voucherForm->isValid()) {
            $voucher = $this->voucherService->getVoucherByCode($voucherForm->getData()->getVoucherCode());
            $this->basket->setVoucher($voucher->getId());

            return new RedirectResponse($this->controllerHelper->getUrl('basket_module_package_basket_upgrade', ['package_id' => $packageId]));
        }

        $additionalVariables = [
            'voucherFormTop' => $voucherForm->createView(),
            'voucherFormBottom' => $voucherForm->createView(),
            'guaranteedSameDayProduct' => $this->nodeService->getProductById(Product::PRODUCT_GUARANTEED_SAME_DAY),
            'packageId' => $packageId,
            'basket' => $this->basket,
            'view' => $this->packageBasketView,
        ];

        return $this->renderer->render($additionalVariables);
    }

    /**
     * @param int $packageId
     * @return RedirectResponse
     * @throws Exception
     */
    public function addPackage($packageId)
    {
        $package = $this->packageService->getPublishedPackageById($packageId);
        if (!$package) {
            return new RedirectResponse($this->controllerHelper->getLink(SearchControler::SEARCH_PAGE));
        }

        $package->companyName = SearchControler::getCompanyName();

        if ($this->basket->hasPackageIncluded() && !$this->basket->hasPackage($packageId)) {
            $this->basket->clear(FALSE);
            $response = $this->addPackageToBasket($package);
        } elseif (!$this->basket->hasPackageIncluded()) {
            $removedProductTitles = [];
            foreach ($this->basket->getProductsInCommonByIds($package->blacklistedContainedProducts) as $product) {
                $this->basket->removeByProductId($product->getId());
                $removedProductTitles[] = $product->getLongTitle();
            }

            if (!empty($removedProductTitles)) {
                $this->controllerHelper->setInfoFlashMessage($this->createRemovedBlacklistedProductsMessage($removedProductTitles, $package), FALSE);
            }

            $response = $this->addPackageToBasket($package);
        } else {
            $response = new RedirectResponse($this->controllerHelper->getUrl('basket_module_package_basket_upgrade', ['package_id' => $packageId]));
        }

        return $response;
    }

    /**
     * @return RedirectResponse
     */
    public function addGuaranteedSameDayProduct()
    {
        if (!$this->basket->hasPackageIncluded()) {
            return new RedirectResponse($this->controllerHelper->getLink(SearchControler::SEARCH_PAGE));
        }

        if ($this->packageBasketView->canAddGuaranteedSameDay()) {
            $response = $this->addProduct($this->nodeService->getProductById(Product::PRODUCT_GUARANTEED_SAME_DAY));
        } else {
            $this->controllerHelper->setErrorFlashMessage("Guaranteed Same Day Formation can't be added to basket");
            $response = new RedirectResponse($this->controllerHelper->getUrl('basket_module_package_basket_upgrade', ['package_id' => $this->basket->getPackage()->getId()]));
        }

        return $response;
    }

    /**
     * @param int $productId
     * @return RedirectResponse
     */
    public function addOfferProduct($productId)
    {
        if (!$this->basket->hasPackageIncluded()) {
            return new RedirectResponse($this->controllerHelper->getLink(SearchControler::SEARCH_PAGE));
        }

        $package = $this->basket->getPackage();
        $product = $this->nodeService->getPublishedProductById($productId);

        if ($product && $package->getOfferProductId() == $product->getId()) {
            $product->markAsOffer();
            $response = $this->addProduct($product);
        } else {
            $this->controllerHelper->setErrorFlashMessage("Product doesn't have association to package");
            $response = new RedirectResponse($this->controllerHelper->getUrl('basket_module_package_basket_upgrade', ['package_id' => $package->getId()]));
        }
        
        return $response;
    }

    /**
     * @param int $productId
     * @return RedirectResponse
     */
    public function addAssociatedProduct($productId)
    {
        if (!$this->basket->hasPackageIncluded()) {
            return new RedirectResponse($this->controllerHelper->getLink(SearchControler::SEARCH_PAGE));
        }

        $package = $this->basket->getPackage();
        $product = $this->nodeService->getPublishedProductById($productId);
        $productsAssociated = $package->getPackageProducts('associatedProducts1') + $package->getPackageProducts('associatedProducts3');

        if ($product && isset($productsAssociated[$product->getId()])) {
            $product->isAssociated = TRUE;
            $response = $this->addProduct($product);
        } else {
            $this->controllerHelper->setErrorFlashMessage("Product doesn't have association to package");
            $response = new RedirectResponse($this->controllerHelper->getUrl('basket_module_package_basket_upgrade', ['package_id' => $package->getId()]));
        }

        return $response;
    }

    /**
     * @param int $itemId
     * @return RedirectResponse
     */
    public function removeProduct($itemId)
    {
        if (!$this->basket->hasPackageIncluded()) {
            return new RedirectResponse($this->controllerHelper->getLink(SearchControler::SEARCH_PAGE));
        }

        $packageId = $this->basket->getPackage()->getId();
        $removedProductId = $this->basket->remove($itemId);
        if ($removedProductId) {
            $productTitle = $this->nodeService->getProductById($removedProductId)->getLngTitle();
            $this->controllerHelper->setSuccessFlashMessage("<i>{$productTitle}</i> has been removed from your basket", FALSE);
        } else {
            $this->controllerHelper->setErrorFlashMessage("Product can't be removed");
        }

        return new RedirectResponse($this->controllerHelper->getUrl('basket_module_package_basket_upgrade', ['package_id' => $packageId]));
    }

    /**
     * @return RedirectResponse
     */
    public function removeVoucher()
    {
        if (!$this->basket->hasPackageIncluded()) {
            return new RedirectResponse($this->controllerHelper->getLink(SearchControler::SEARCH_PAGE));
        }

        $this->basket->removeVoucher();
        $packageId = $this->basket->getPackage()->getId();

        return new RedirectResponse($this->controllerHelper->getUrl('basket_module_package_basket_upgrade', ['package_id' => $packageId]));
    }

    /**
     * @param Package $package
     * @return RedirectResponse
     */
    private function addPackageToBasket(Package $package)
    {
        try {
            $this->basket->addPackage($package);
        } catch (BasketException $e) {
            $this->controllerHelper->setErrorFlashMessage($e->getMessage());

            return new RedirectResponse($this->controllerHelper->getLink(SearchControler::SEARCH_PAGE));
        }

        $this->controllerHelper->setSuccessFlashMessage($this->createAddedPackageMessage($package), FALSE);

        return new RedirectResponse($this->controllerHelper->getUrl('basket_module_package_basket_upgrade', ['package_id' => $package->getId()]));
    }

    /**
     * @param BasketProduct $product
     * @return RedirectResponse
     * @throws Exception
     */
    private function addProduct(BasketProduct $product)
    {
        $package = $this->basket->getPackage();

        try {
            $this->basket->add($product);
            $this->controllerHelper->setSuccessFlashMessage("<i>{$product->getLngTitle()}</i> has been added to your basket", FALSE);
        } catch (BasketException $e) {
            $this->controllerHelper->setErrorFlashMessage($e->getMessage());
        }

        return new RedirectResponse($this->controllerHelper->getUrl('basket_module_package_basket_upgrade', ['package_id' => $package->getId()]));
    }
    /**
     * @param array $removedProductTitles
     * @param Package $package
     * @return string
     */
    private function createRemovedBlacklistedProductsMessage($removedProductTitles, Package $package)
    {
        $removedProductsString = implode(', ', $removedProductTitles);
        $message = count($removedProductTitles) > 1
            ? "The items <i>{$removedProductsString}</i> are already included in your <i>{$package->getLngTitle()}</i>, so we have removed the extra <i>{$removedProductsString}</i> from your basket."
            : "The <i>{$removedProductsString}</i> is already included in your <i>{$package->getLngTitle()}</i>, so we have removed the extra <i>{$removedProductsString}</i> from your basket.";

        return $message;
    }

    /**
     * @param Package $package
     * @return string
     */
    private function createAddedPackageMessage(Package $package)
    {
        $companyName = $package->companyName ? htmlentities($package->companyName) : '';

        return "<i>{$package->getLngTitle()}</i> has been added to your basket" . ($companyName ? " for company <i>\"{$companyName}\"</i>" : '');
    }
}
