<?php

namespace BasketModule\ValueObject;

class Price
{
    /**
     * @var float
     */
    private $subtotal;

    /**
     * @var float
     */
    private $vat;

    /**
     * @var float
     */
    private $discount;

    /**
     * @var float
     */
    private $nonVatable;

    /**
     * @var float
     */
    private $total;

    /**
     * @param float $subtotal
     * @param float $vat
     * @param float $total
     * @param float $discount
     * @param float $nonVatable
     */
    public function __construct($subtotal, $vat, $total, $discount = 0.0, $nonVatable = 0.0)
    {
        $this->subtotal = $subtotal;
        $this->vat = $vat;
        $this->discount = $discount;
        $this->nonVatable = $nonVatable;
        $this->total = $total;
    }

    /**
     * @return float
     */
    public function getSubtotal()
    {
        return $this->subtotal;
    }

    /**
     * @return float
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @return float
     */
    public function getNonVatable()
    {
        return $this->nonVatable;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }
}
