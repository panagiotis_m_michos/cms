<?php

namespace BasketModule\Services;

use BasketModule\Factories\PriceFactory;
use BasketModule\ValueObject\Price;
use BasketProduct;

class PriceCalculator
{
    /**
     * @var PriceFactory
     */
    private $priceFactory;

    /**
     * @param PriceFactory $priceFactory
     */
    public function __construct(PriceFactory $priceFactory)
    {
        $this->priceFactory = $priceFactory;
    }

    /**
     * @param BasketProduct[] $products
     * @param float $discount
     * @return Price
     */
    public function calculatePrice($products, $discount = 0.0)
    {
        $nonVatable = 0;
        $subtotal = 0;

        foreach ($products as $product) {
            $subtotal += $product->getTotalPrice();

            if ($product->notApplyVat) {
                $nonVatable += $product->getTotalPrice();
            } elseif ($product->nonVatableValue) {
                $nonVatable += $product->nonVatableValue;
            }
        }

        return $this->priceFactory->create($subtotal, $nonVatable, $discount);
    }
}
