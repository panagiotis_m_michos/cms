<?php

namespace BasketModule\Services;

use Basket;
use Basket\PackageToPackageDowngradeNotification;
use Basket\PackageToPackageUpgradeNotification;
use Basket\PackageToProductDowngradeNotification;
use Basket\ProductToPackageUpgradeNotification;
use Basket\SuspendedItem;
use Entities\Customer as CustomerEntity;
use Entities\Service;
use Factories\Front\BasketNotificationFactory;
use Factories\ServiceViewFactory;
use IPackage;
use LogicException;
use Nette\Object;
use Package;
use Product;
use Services\CompanyService;
use Services\ServiceService;

class BasketService extends Object
{
    /**
     * @var Basket
     */
    private $basket;

    /**
     * @var ServiceService
     */
    private $serviceService;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var ServiceViewFactory
     */
    private $serviceViewFactory;

    /**
     * @var BasketNotificationFactory
     */
    private $basketNotificationFactory;

    /**
     * @param Basket $basket
     * @param ServiceService $serviceService
     * @param CompanyService $companyService
     * @param ServiceViewFactory $serviceViewFactory
     * @param BasketNotificationFactory $basketNotificationFactory
     */
    public function __construct(
        Basket $basket,
        ServiceService $serviceService,
        CompanyService $companyService,
        ServiceViewFactory $serviceViewFactory,
        BasketNotificationFactory $basketNotificationFactory
    )
    {
        $this->basket = $basket;
        $this->serviceService = $serviceService;
        $this->companyService = $companyService;
        $this->serviceViewFactory = $serviceViewFactory;
        $this->basketNotificationFactory = $basketNotificationFactory;
    }

    /**
     * @return ProductToPackageUpgradeNotification[]
     */
    public function getProductToPackageUpgradeNotifications()
    {
        $productUpgradeNotifications = array();
        /** @var Package $basketPackage */
        foreach ($this->getPackagesWithCompany() as $basketPackage) {
            $company = $this->companyService->getCompanyById($basketPackage->getCompanyId());
            $types = $basketPackage->getTypesForPackageProducts();
            $companyProductServices = $company->getProductServicesByTypes($types);

            if (!$companyProductServices->isEmpty()) {
                $productUpgradeNotifications[] = $this->basketNotificationFactory->createProductToPackageUpgradeNotification($basketPackage, $company, $companyProductServices->toArray());
            }
        }
        return $productUpgradeNotifications;
    }

    /**
     * @return PackageToPackageUpgradeNotification[]
     */
    public function getPackageToPackageUpgradeNotifications()
    {
        $packageUpgradeNotifications = [];
        // check if customer is buying Comprehensive when he already has Privacy active (hardcoded for now - S375)
        /** @var Package $basketPackage */
        foreach ($this->getPackagesWithCompany() as $basketPackage) {
            $company = $this->companyService->getCompanyById($basketPackage->getCompanyId());
            if ($basketPackage->isPackageComprehensiveType()) {
                $packageService = $company->getLastPackageService(Service::TYPE_PACKAGE_PRIVACY);
                if ($packageService) {
                    $packageUpgradeNotifications[] = $this->basketNotificationFactory->createPackageToPackageUpgradeNotification($basketPackage, $packageService);
                }
            }
        }

        return $packageUpgradeNotifications;
    }

    /**
     * @return PackageToProductDowngradeNotification|NULL
     */
    public function getPackageToProductDowngradeNotification()
    {
        /** @var Product $basketProduct */
        foreach ($this->getProductsWithCompany() as $basketProduct) {
            $company = $this->companyService->getCompanyById($basketProduct->getCompanyId());
            $packageService = $company->getLastPackageService($basketProduct->serviceTypeId);
            if ($packageService && $basketProduct->serviceTypeId != $packageService->getServiceTypeId()) {
                return $this->basketNotificationFactory->createPackageToProductDowngradeNotification($basketProduct, $packageService);
            }
        }

        return NULL;
    }

    /**
     * @return PackageToPackageDowngradeNotification|NULL
     */
    public function getPackageToPackageDowngradeNotification()
    {
        // check if customer is not buying Privacy when he already has Comprehensive active (hardcoded for now - S375)
        /** @var Package $basketPackage */
        foreach ($this->getPackagesWithCompany() as $basketPackage) {
            $company = $this->companyService->getCompanyById($basketPackage->getCompanyId());
            if ($basketPackage->isPackagePrivacyType()) {
                $packageService = $company->getLastPackageService(Service::TYPE_PACKAGE_COMPREHENSIVE_ULTIMATE);
                if ($packageService) {
                    return $this->basketNotificationFactory->createPackageToPackageDowngradeNotificion($basketPackage, $packageService);
                }
            }
        }

        return NULL;
    }

    /**
     * @param CustomerEntity $customer
     * @param array $services
     * @throws LogicException
     */
    public function addRenewableServices(CustomerEntity $customer, array $services)
    {
        foreach ($services as $serviceId) {
            $service = $this->serviceService->getCustomerService($customer, $serviceId);
            if (!$service->isRenewable()) {
                $message = sprintf("Service [%s (%s)] is not renewable", $service->getServiceType(), $service->getId());
                throw new LogicException($message);
            }
            $item = $service->getRenewalProduct();
            $this->basket->add($item);
        }
    }

    /**
     * @return SuspendedItem
     */
    public function getSuspendedServiceItem()
    {
        foreach ($this->basket->getItems() as $product) {
            if ($product->getCompanyId() && $product->hasServiceType()) {
                $company = $this->companyService->getCompanyById($product->getCompanyId());
                $services = $company->getServicesByType($product->serviceTypeId);
                $serviceView = $this->serviceViewFactory->create($services->toArray());
                if ($serviceView->isExpired()) {
                    return new SuspendedItem($company, $serviceView);
                }
            }
        }
    }

    /**
     * @returns IPackage[]
     */
    private function getPackagesWithCompany()
    {
        $packages = [];
        foreach ($this->basket->getItems() as $item) {
            if ($item->isPackage() && $item->getCompanyId()) {
                $packages[] = $item;
            }
        }
        return $packages;
    }

    /**
     * @returns Product[]
     */
    private function getProductsWithCompany()
    {
        $products = [];
        foreach ($this->basket->getItems() as $item) {
            if (!$item->isPackage() && $item instanceof Product && $item->getCompanyId()) {
                $products[] = $item;
            }
        }
        return $products;
    }
}
