{extends '@structure/layout.tpl'}

{block content}
 <div class="width100 bg-white" style="border-bottom: 2px solid #ddd;">
    <div id="upgrade" class="container pad025">
        <div class="row">
            <div class="col-md-12">
                <ul class="progress-indicator">
                    <li class="completed"><span>Search</span><span class="bubble"></span></li>
                    <li class="completed"><span>Select</span><span class="bubble"></span></li>
                    <li class="active"><span>Buy</span><span class="bubble"></span></li>
                    <li><span>Complete</span><span class="bubble"></span></li>
                </ul>
            </div>
        </div>

        {include '@blocks/flashMessageBt.tpl'}

        {$formHelper->setTheme($voucherFormTop, 'cms_horizontal.html.twig')}
        {$formHelper->setTheme($voucherFormBottom, 'cms_horizontal.html.twig')}

        {if $formHelper->errors($voucherFormTop['voucherCode'])}
            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {$formHelper->errors($voucherFormTop['voucherCode']) nofilter}
            </div>
        {/if}

        <div class="hidden-lg hidden-md hidden-sm">
            <div class="row narrow pad8 btm20">
                <div class="col-md-12">
                    {include './@blocks/basketBlock.tpl'}
                </div>
            </div>

            {if $view->canShowVoucherForm()}
                <div class="row narrow btm20">
                    <div class="col-md-12">
                        {include './@blocks/voucherFormBlock.tpl' form=$voucherFormTop}
                    </div>
                </div>
            {/if}
        </div>

        <div class="row pad8">
            <div class="col-md-8 left-column">
                {if $view->canAddGuaranteedSameDay()}
                    {include './@blocks/specialOfferBlock.tpl' product=$guaranteedSameDayProduct addToBasketRoute='basket_module_package_basket_add_guaranteed_same_day_product' offerTitle='Available until 2pm' offerPrice=$guaranteedSameDayProduct->associatedPrice}
                {elseif $view->canAddOffer()}
                    {include './@blocks/specialOfferBlock.tpl' product=$view->getOffer() addToBasketRoute='basket_module_package_basket_add_offer' offerTitle='Offer of the Week' offerPrice=$view->getOffer()->getOfferPrice()}
                {/if}

                {if $view->getUpsellProducts()}
                    <div class="row btm30">
                        <div class="col-md-12">
                            <div class="row narrow">
                                <div class="col-md-12 bg-grey7">
                                    <h4 class="white font-head">People who bought this package also bought: </h4>
                                </div>
                            </div>
                            {foreach from=$view->getUpsellProducts() key="productId" item="product" name="products"}
                                {if $smarty.foreach.products.iteration <= 3}
                                    <div class="row narrow greyborder">
                                        <div class="col-md-1">
                                            <i class="fa {$product->associatedIconClass} fa-2x top10"></i>
                                        </div>
                                        <div class="col-md-8">
                                            <h4>{$product->getLngTitle()}</h4>
                                            <p>{$product->associatedText}</p>
                                            <p class="small">{$product->associatedDescription}</p>
                                        </div>
                                        <div class="col-md-3 top10 text-right">
                                            <p class="black40 font-head-15"><s>{$product->productValue|currency}</s></p>
                                            <p class="black60 xsmall margin0">With your package:</p>
                                            <h3 class="font-head-15">{$product->price|currency}</h3>
                                            <a class="btn btn-success btm10" rel="nofollow" href="{url route='basket_module_package_basket_add_associated_product' productId=$product->getId()}">+ Add to basket</a>
                                        </div>
                                    </div>
                                {/if}
                            {/foreach}
                        </div>
                    </div>
                {/if}

            </div>
            <div class="col-md-4">
                <div class="row narrow btm30">
                    <div class="col-md-12">
                        {include './@blocks/basketBlock.tpl'}
                    </div>
                </div>

                {if $view->canShowVoucherForm()}
                    <div class="row narrow btm30">
                        <div class="col-md-12">
                            {include './@blocks/voucherFormBlock.tpl' form=$voucherFormBottom}
                        </div>
                    </div>
                {/if}

                <div class="row narrow btm20">
                    <div class="col-md-12">
                        <a href="https://www.feefo.com/feefo/viewvendor.jsp?logon=www.madesimplegroup.com/companiesmadesimple" onclick="window.open(this.href, 'Feefo', 'width=1000,height=600,scrollbars,resizable'); return false;">
                            <img alt="Feefo logo" border="0" src="//www.feefo.com/feefo/feefologo.jsp?logon=www.madesimplegroup.com/companiesmadesimple&amp;template=service-white-200x50_en.png" title="See what our customers say about us">
                        </a>
                    </div>
                </div>

                <div class="row narrow">
                    <div class="col-md-12">
                        <h4><i class="fa fa-gbp fa-lg black60"></i>&nbsp;&nbsp;Money Back Guarantee</h4>
                        <p>If you start the formation process and decide it's not for you, we'll give you your money back. Just as long as the company hasn't been incorporated.</p>
                    </div>
                </div>
                <hr>
                <div class="row narrow btm20">
                    <div class="col-md-12">
                        <h4><i class="fa fa-lock fa-lg black60"></i>&nbsp;&nbsp;You're safe with us!</h4>
                        <img src="{$urlImgs}cms-basket-security.png" alt="" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{/block}

{include file="@footer.tpl"}
