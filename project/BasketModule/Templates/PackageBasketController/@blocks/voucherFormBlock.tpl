<a href="javascript:void(0);" data-toggle="collapse" data-target=".voucher-toggle" title="Apply Voucher">I have a voucher</a>
<div class="voucher-toggle collapse {if $formHelper->errors($form['voucherCode'])}in{/if}">
    {$formHelper->start($form) nofilter}
        <div class="col-xs-12 col-md-8">
            <div class="form-group inlineblock valign-top">
                {$formHelper->widget($form['voucherCode']) nofilter}
            </div>
        </div>
        <div class="col-xs-12 col-md-offset-1 col-md-3">
            <div class="form-group inlineblock valign-top">
                {$formHelper->widget($form['submit']) nofilter}
            </div>
        </div>
    {$formHelper->end($form) nofilter}
</div>
