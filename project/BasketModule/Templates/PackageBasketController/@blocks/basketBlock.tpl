<div class="row">
    <div class="col-md-12 bg-blue">
        <h3 class="white font-head">My Basket</h3>
    </div>
</div>
<div class="row pad-topdown blueborder">
    <div class="col-md-12">
        {foreach from=$basket->getItems() key="itemId" item="item" name="basketItems"}
            <div class="row margin0">
                <div class="col-xs-8 col-md-7 pad0">
                    <p class="small margin0">
                        {$item->getLngTitle()|strip_tags|truncate:56:"...":1}
                    </p>
                </div>
                <div class="col-xs-1 pad0 text-right">
                    {if !empty($item->basketText)}
                        <i class="fa fa-question-circle black60" data-toggle="tooltip" data-placement="bottom" title="{$item->basketText|strip_tags}"></i>
                    {/if}
                </div>
                <div class="col-xs-2 col-md-3 pad0">
                    <p class="small font-500 margin0 text-right">
                        {if $item->isFree()}FREE{else}{$item->price|currency}{/if}
                    </p>
                </div>
                <div class="col-xs-1 pad0">
                    {if !$item->isPackage() && $item->isRemovableFromBasket()}
                        <p class="small margin0 text-right">
                            <a class="removeitem" rel="nofollow"
                               href="{url route='basket_module_package_basket_remove_product' itemId=$itemId}"
                               {if $item->hasRemoveFromBasketConfirmation()}data-confirm="{$item->getRemoveFromBasketConfirmation()}"{/if}>
                                <i class="fa fa-remove"></i>
                            </a>
                        </p>
                    {/if}
                </div>
            </div>
            {if !$smarty.foreach.basketItems.last}
                <hr>
            {/if}
        {/foreach}

        {if $basket->voucher}
            <hr>
            <div class="row margin0">
                <div class="col-xs-8 col-md-7 pad0">
                    <p class="small margin0">
                        Voucher {$basket->voucher->name}
                    </p>
                </div>
                <div class="col-xs-1 pad0 text-right">
                </div>
                <div class="col-xs-2 col-md-3 pad0">
                    <p class="small font-500 margin0 text-right">
                        {assign var="subTotal" value=$basket->getPrice('subTotal')}
                        <span class="red">-{$basket->voucher->discount($subTotal)|currency}</span>&nbsp;

                    </p>
                </div>
                <div class="col-xs-1 pad0 text-right">
                    <a class="removeitem" rel="nofollow" href="{url route='basket_module_package_basket_remove_voucher'}">
                        <i class="fa fa-remove"></i>
                    </a>
                </div>
            </div>
        {/if}

        <div class="row bg-grey1 top20">
            <div class="col-xs-12 padcard">
                <div class="row subtotal-price">
                    <div class="col-xs-8"><p class="small margin0">Subtotal</p></div>
                    <div class="col-xs-4"><p class="small font-500 margin0 text-right">{$basket->getPrice('subTotal2')|currency}</p></div>
                </div>
                <div class="row vat-price">
                    <div class="col-xs-8"><p class="small margin0">VAT</p></div>
                    <div class="col-xs-4"><p class="small font-500 margin0 text-right">{$basket->getPrice('vat')|currency}</p></div>
                </div>
                <div class="row total-price">
                    <div class="col-xs-6"><p>Total</p></div>
                    <div class="col-xs-6"><p class="text-right">{$basket->getPrice('total')|currency}</p></div>
                </div>
                <div class="row">
                    <div class="col-xs-12"><a class="btn btn-default center-block font115" href="{url route="BasketControler::PAGE_SUMMARY"}">Proceed to checkout</a></div>
                </div>
            </div>
        </div>
    </div>
</div>
