<div class="row narrow basket-special-offer">
    <div class="col-md-12">
        <div class="row">
            <div class="col-xs-8 col-sm-5 bg-grey7">
                <h4 class="white font-head">{$offerTitle}</h4>
            </div>
        </div>
        <div class="row bg-grey1">
            <div class="col-md-8">
                <h3>{$product->getLngTitle()}</h3>
                <p>{$product->associatedText}</p>
                <p class="small">{$product->associatedDescription}</p>
            </div>
            <div class="col-md-4 top10 text-right">
                <h3 class="black40 font-300 font-head-15"><s>{$product->productValue|currency}</s></h3>
                <p class="black60 small margin0">With your package:</p>
                <h2 class="font-head-15">{$offerPrice|currency}</h2>
                <a class="btn btn-success btm10" rel="nofollow" href="{url route=$addToBasketRoute productId=$product->getId()}">+ Add to basket</a>
            </div>
        </div>
    </div>
</div>
