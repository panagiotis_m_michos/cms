<?php

namespace BasketModule\Factories;

use BasketModule\ValueObject\Price;
use VatModule\Calculators\VatCalculator;

class PriceFactory
{
    /**
     * @var VatCalculator
     */
    private $vatCalculator;

    /**
     * @param VatCalculator $vatCalculator
     */
    public function __construct(VatCalculator $vatCalculator)
    {
        $this->vatCalculator = $vatCalculator;
    }

    /**
     * @param float $subtotal
     * @param float $nonVatable
     * @param float $discount
     * @return Price
     */
    public function create($subtotal, $nonVatable = 0.0, $discount = 0.0)
    {
        $vatEligible = max(0.0, $subtotal - $discount - $nonVatable);
        $vat = round($this->vatCalculator->getVatFromSubtotal($vatEligible), 2);

        return new Price(
            $subtotal,
            $vat,
            $subtotal + $vat,
            $discount,
            $nonVatable
        );
    }
}
