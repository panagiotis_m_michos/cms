<?php

namespace BasketModule\Deciders;

use Basket;
use BasketProduct;
use Services\NodeService;

class OfferDecider
{
    /**
     * @var NodeService
     */
    private $nodeService;

    /**
     * @var Basket
     */
    private $basket;
    
    /**
     * @var GuaranteedSameDayDecider
     */
    private $guaranteedSameDayDecider;

    /**
     * @param NodeService $nodeService
     * @param Basket $basket
     * @param GuaranteedSameDayDecider $guaranteedSameDayDecider
     */
    public function __construct(
        NodeService $nodeService,
        Basket $basket,
        GuaranteedSameDayDecider $guaranteedSameDayDecider
    )
    {
        $this->nodeService = $nodeService;
        $this->basket = $basket;
        $this->guaranteedSameDayDecider = $guaranteedSameDayDecider;
    }

    /**
     * @return bool
     */
    public function canAddOffer()
    {
        if ($this->guaranteedSameDayDecider->canBeAdded()) {
            return FALSE;
        }
        $offer = $this->getOffer();

        return $offer && !$this->basket->hasProduct($offer->getId());
    }

    /**
     * @return BasketProduct
     */
    public function getOffer()
    {
        $package = $this->basket->getPackageOrNull();
        if (!$package) {
            return NULL;
        }
        $productOfTheMonthId = $package ? $package->getOfferProductId() : NULL;

        return $this->nodeService->getProductById($productOfTheMonthId);
    }
}
