<?php

namespace BasketModule\Deciders;

use BankHolidayModule\Deciders\BankHolidayDecider;
use Basket;
use DateTime;
use Product;

class GuaranteedSameDayDecider
{
    /**
     * @var Basket
     */
    private $basket;

    /**
     * @var BankHolidayDecider
     */
    private $bankHolidayDecider;

    /**
     * @param Basket $basket
     * @param BankHolidayDecider $bankHolidayDecider
     */
    public function __construct(Basket $basket, BankHolidayDecider $bankHolidayDecider)
    {
        $this->basket = $basket;
        $this->bankHolidayDecider = $bankHolidayDecider;
    }

    /**
     * @return bool
     */
    public function canBeAdded()
    {
        return !$this->basket->hasProduct(Product::PRODUCT_GUARANTEED_SAME_DAY)
            && !$this->bankHolidayDecider->isHolidayNow()
            && in_array(date('N'), range(1, 5)) // work day
            && ((new DateTime) < (new DateTime('14:00'))); // before 14:00pm
    }
}
