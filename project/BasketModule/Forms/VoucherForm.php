<?php

namespace BasketModule\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class VoucherForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setMethod('POST');
        $builder->add(
            'voucherCode',
            'text',
            [
                'label' => FALSE,
                'attr' => [
                    'placeholder' => 'Voucher code',
                ],
            ]
        );

        $builder->add(
            'submit',
            'submit',
            [
                'label' => 'Apply',
                'attr' => [
                    'class' => 'btn-default',
                ],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'VoucherForm';
    }
}
