<?php

namespace BasketModule\Forms\Data;

use Symfony\Component\Validator\Constraints as Assert;
use VoucherModule\Validators as VoucherAssert;

/**
 * @Assert\GroupSequence({"VoucherFormData", "existingVoucher", "enabledVoucher", "overMinimalSpend"})
 */
class VoucherFormData
{
    /**
     * @var string
     *
     * @VoucherAssert\VoucherExists(groups={"existingVoucher"})
     * @VoucherAssert\VoucherEnabled(groups={"enabledVoucher"})
     * @VoucherAssert\MinimalSpendReached(groups={"overMinimalSpend"})
     */
    private $voucherCode;

    /**
     * @return string
     */
    public function getVoucherCode()
    {
        return $this->voucherCode;
    }

    /**
     * @param string $voucherCode
     */
    public function setVoucherCode($voucherCode)
    {
        $this->voucherCode = $voucherCode;
    }
}
