<?php

namespace PaymentModule\Factories;

use Basket;
use BasketModule\Contracts\IBasket;
use PaymentModule\Contracts\IPaymentData;
use PaymentModule\Dto\PaymentDetails;
use PaymentModule\Forms\PaymentForm;
use RouterModule\Helpers\ControllerHelper;
use SagePayDirect;
use Services\Payment\TokenService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\FormInterface;
use UserModule\Contracts\ICustomer;

class PaymentFormBuilder implements IPaymentFormBuilder
{
    /**
     * @var ParameterBagInterface
     */
    private $config;

    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    /**
     * @var TokenService
     */
    private $tokenService;

    /**
     * PaymentFormBuilder constructor.
     * @param ParameterBagInterface $config
     * @param ControllerHelper $controllerHelper
     * @param TokenService $tokenService
     */
    public function __construct(ParameterBagInterface $config, ControllerHelper $controllerHelper, TokenService $tokenService)
    {
        $this->config = $config;
        $this->controllerHelper = $controllerHelper;
        $this->tokenService = $tokenService;
    }

    /**
     * @param IBasket|Basket $basket
     * @param ICustomer|NULL $customer
     * @return FormInterface
     */
    public function buildForm(IBasket $basket, ICustomer $customer = NULL)
    {
        $customerToken = $customer ? $this->tokenService->getTokenByCustomer($customer) : NULL;

        $payment = PaymentDetails::create(
            $this->config->get('payment.currency'),
            $basket,
            NULL,
            $customer,
            $customerToken
        );

        $customerCredit = ($customer && $customer->hasCredit()) ? $customer->getCredit() : 0;

        if (empty($customerToken)) {
            $paymentTypes = [IPaymentData::TYPE_SAGE_NEW_TOKEN => "Pay using Credit/Debit Card"];
            $payment->paymentType = IPaymentData::TYPE_SAGE_NEW_TOKEN;

        } else {
            $paymentTypes = [
                IPaymentData::TYPE_SAGE_EXISTING_TOKEN => $customerToken->getCardTypeText() . " ending in " . $customerToken->getCardNumber(),
                IPaymentData::TYPE_SAGE_NO_TOKEN => "Use a different card"
            ];
            $payment->paymentType = IPaymentData::TYPE_SAGE_EXISTING_TOKEN;
        }
        $paymentTypes[IPaymentData::TYPE_PAYPAL] = "Pay using Paypal";

        $cardTypes = SagePayDirect::getCardTypes();
        $form = $this->controllerHelper->buildForm(
            new PaymentForm($cardTypes, $paymentTypes, FALSE, $customerCredit),
            $payment,
            [
                'action' => '#auth-form-block',
                'required' => FALSE,
            ]
        );
        return $form;
    }
}