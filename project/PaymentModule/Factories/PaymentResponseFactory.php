<?php

namespace PaymentModule\Factories;

use Entities\Customer;
use PaymentModule\Responses\PaymentResponse;
use PaymentResponse as OldPaymentResponse;
use Services\Payment\PaymentResponse as NewerPaymentResponse;
use Transaction;

final class PaymentResponseFactory
{
    /**
     * @param PaymentResponse $paymentResponse
     * @param Customer $customer
     * @return OldPaymentResponse
     */
    public function convert(PaymentResponse $paymentResponse, Customer $customer)
    {
        $p = new OldPaymentResponse();

        $p->setPaymentTransactionTime(date('Y m d H:i:s'));
        $p->setPaymentHolderEmail($customer->getEmail());

        $transactionType = $this->mapTransactionTypes($paymentResponse->getTypeId());
        $p->setTypeId($transactionType);

        $p->setPaymentTransactionDetails(Transaction::$types[$transactionType]);


        // sage
        if ($paymentResponse->isSagePay()) {
            $details = $paymentResponse->getSagePaymentInfo();
            $p->setPaymentCardNumber($paymentResponse->getCardNumber());

            $p->setPaymentHolderFirstName($details->getBillingFirstnames());
            $p->setPaymentHolderLastName($details->getBillingSurname());
            $p->setPaymentHolderAddressStreet($details->getBillingAddress1() . ' ' . $details->getBillingAddress2());
            $p->setPaymentHolderAddressCity($details->getBillingCity());
            $p->setPaymentHolderAddressState($details->getBillingState());
            $p->setPaymentHolderAddressPostCode($details->getBillingPostCode());
            $p->setPaymentHolderAddressCountry($details->getBillingCountry());

            $p->setPaymentTransactionDetails(serialize($details));
            $p->setPaymentOrderCode($paymentResponse->getSageVpsTxId());
            $p->setPaymentVpsAuthCode($paymentResponse->getSageTxAuthNo());
            $p->setPaymentVendorTXCode($paymentResponse->getSageVendorTxCode());

        } elseif ($paymentResponse->isPaypal()) {
            $details = $paymentResponse->getPaypalPaymentInfo();
            $payer = $details->getPayerName();
            $p->setPaymentOrderCode($paymentResponse->getPaypalTransactionId());
            $p->setPaymentHolderFirstName($payer->getFirstName());
            $p->setPaymentHolderLastName($payer->getLastName());

            // retrieve from paypal ?
            if ($address = $details->getShippingAddress()) {
                $p->setPaymentHolderAddressStreet($address->getStreet() . ' ' . $address->getStreet2());
                $p->setPaymentHolderAddressCity($address->getCity());
                $p->setPaymentHolderAddressPostCode($address->getZip());
                $p->setPaymentHolderAddressCountry($address->getCountry());
            }
            $p->setPaymentTransactionDetails(serialize($details));

        } else {
            $p->setPaymentHolderFirstName($customer->getFirstName());
            $p->setPaymentHolderLastName($customer->getLastName());

            $p->setPaymentHolderAddressStreet($customer->getAddress1());
            $p->setPaymentHolderAddressCity($customer->getCity());
            $p->setPaymentHolderAddressPostCode($customer->getPostcode());
            $p->setPaymentHolderAddressCountry($customer->getCountryIso());
        }
        return $p;
    }

    /**
     * @param string $paymentType
     * @return int
     */
    public function mapTransactionTypes($paymentType)
    {
        switch ($paymentType) {
            case PaymentResponse::TYPE_SAGE:
                return Transaction::TYPE_SAGEPAY;

            case PaymentResponse::TYPE_PAYPAL:
                return Transaction::TYPE_PAYPAL;

            case PaymentResponse::TYPE_ON_ACCOUNT:
                return Transaction::ON_ACCOUNT;

            case PaymentResponse::TYPE_FREE:
                return Transaction::TYPE_FREE;

            default:
                return Transaction::TYPE_SAGEPAY;
        }
    }

    /**
     * @param PaymentResponse $paymentResponse
     * @return OldPaymentResponse
     */
    public function convertToNewer(PaymentResponse $paymentResponse)
    {
        $p = NewerPaymentResponse::createEmpty();
        $p->setTypeId($paymentResponse->getTypeId());
        $p->setCardHolder($paymentResponse->getCardHolder());
        $p->setCardNumber($paymentResponse->getCardNumber());
        $p->setCardExpiryDate($paymentResponse->getCardExpiryDate());
        $p->setCardType($paymentResponse->getCardType());

        $p->setError($paymentResponse->getError());

        $p->setSageSecurityKey($paymentResponse->getSageSecurityKey());
        $p->setSageDetails($paymentResponse->getSagePaymentInfo());
        $p->setSageTxAuthNo($paymentResponse->getCardHolder());
        $p->setSageVendorTxCode($paymentResponse->getCardHolder());
        $p->setSageVpsTxId($paymentResponse->getCardHolder());
        $p->setSageToken($paymentResponse->getSageToken());

        $p->setPaypalTransactionId($paymentResponse->getPaypalTransactionId());
        $p->setAndroidTransactionId($paymentResponse->getAndroidTransactionId());
        $p->setItunesTransactionId($paymentResponse->getItunesTransactionId());

        $p->setRecurringPayment($paymentResponse->isRecurringPayment());
        return $p;
    }
}