<?php

namespace PaymentModule\Facades;

use DashboardCustomerControler;
use Entities\Company;
use Entities\Customer;
use Entities\Order;
use Entities\Payment\Token;
use FRouter;
use HttpModule\Responses\PdfResponse;
use OrderModule\Pdf\IInvoiceGenerator;
use PaymentModule\Forms\PurchaseConfirmationData;
use Services\CompanyService;
use Services\OrderService;
use Services\Payment\TokenService;
use ServiceSettingsModule\Services\ServiceSettingsService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use VirtualOfficeControler;

class PurchaseConfirmationFacade
{

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var OrderService
     */
    private $orderService;

    /**
     * @var TokenService
     */
    private $tokenService;

    /**
     * @var ServiceSettingsService
     */
    private $serviceSettingsService;

    /**
     * @var FRouter
     */
    private $frouter;

    /**
     * @var IInvoiceGenerator
     */
    private $invoiceGenerator;

    /**
     * @param CompanyService $companyService
     * @param OrderService $orderService
     * @param TokenService $tokenService
     * @param ServiceSettingsService $serviceSettingsService
     * @param FRouter $frouter
     */
    public function __construct(
        CompanyService $companyService,
        OrderService $orderService,
        TokenService $tokenService,
        ServiceSettingsService $serviceSettingsService,
        FRouter $frouter,
        IInvoiceGenerator $invoiceGenerator
    )
    {
        $this->companyService = $companyService;
        $this->orderService = $orderService;
        $this->tokenService = $tokenService;
        $this->serviceSettingsService = $serviceSettingsService;
        $this->frouter = $frouter;
        $this->invoiceGenerator = $invoiceGenerator;
    }

    /**
     * @param Customer $customer
     * @param int $orderId
     * @return Order|NULL
     */
    public function getCustomerOrder(Customer $customer, $orderId)
    {
        return $this->orderService->getCustomerOrder($customer, $orderId);
    }

    /**
     * @param Customer $customer
     * @return Token|NULL
     */
    public function getTokenByCustomer(Customer $customer)
    {
        return $this->tokenService->getTokenByCustomer($customer);
    }

    /**
     * @param Order $order
     * @return Company|NULL
     */
    public function getCompanyByOrder(Order $order)
    {
        return $this->companyService->getCompanyByOrder($order);
    }

    /**
     * @param Order $order
     * @param Token $customerToken
     * @return bool
     */
    public function canToggleAutoRenewal(Order $order, Token $customerToken = NULL)
    {
        return $order->isPaidBySagePay() && $order->containsRenewableServices() && $customerToken;
    }

    /**
     * @param Order $order
     * @param Token $token
     */
    public function enableAutoRenewal(Order $order, Token $token)
    {
        $this->serviceSettingsService->enableAutoRenewalByOrder($order, $token);
    }

    /**
     * @param Order $order
     */
    public function disableAutoRenewal(Order $order)
    {
        $this->serviceSettingsService->disableAutoRenewalByOrder($order);
    }

    /**
     * @param PurchaseConfirmationData $formData
     * @param Order $order
     * @param Token $customerToken
     */
    public function processConfirmationForm(PurchaseConfirmationData $formData, Order $order, Token $customerToken = NULL)
    {
        if ($formData->canToggleAutoRenewal()) {
            if ($formData->isAutoRenewalEnabled()) {
                $this->enableAutoRenewal($order, $customerToken);
            } else {
                $this->disableAutoRenewal($order);
            }
        }
    }

    /**
     * @param Company $company
     * @return RedirectResponse
     */
    public function redirectAfterConfirmation(Company $company = NULL)
    {
        if ($company) {
            $link = $this->frouter->link(
                VirtualOfficeControler::VIRTUAL_OFFICE_PAGE,
                ['company_id' => $company->getCompanyId()]
            );
        } else {
            $link = $this->frouter->link(DashboardCustomerControler::DASHBOARD_PAGE);
        }

        return new RedirectResponse($link);
    }

    /**
     * @param Order $order
     * @return PdfResponse
     */
    public function downloadInvoice(Order $order)
    {
        return new PdfResponse($this->invoiceGenerator->getContents($order), 'Invoice.pdf');
    }
}
