<?php

namespace PaymentModule\Redirections;


use BasketControler;
use BasketModule\BasketValidators\IValidator;
use BasketModule\Contracts\IBasket;
use BasketModule\Services\BasketService;
use PaymentModule\Contracts\IPaymentPermission;
use RouterModule\Helpers\ControllerHelper;
use Symfony\Component\HttpFoundation\RedirectResponse;
use UserModule\Contracts\ICustomer;

class RedirectionDecider implements IPaymentPermission
{

    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    /**
     * @var BasketService
     */
    private $basketService;

    /**
     * @var IValidator
     */
    private $validator;

    /**
     * RedirectionDecider constructor.
     * @param ControllerHelper $controllerHelper
     * @param BasketService $basketService
     * @param IValidator $validator
     */
    public function __construct(ControllerHelper $controllerHelper, BasketService $basketService, IValidator $validator)
    {
        $this->controllerHelper = $controllerHelper;
        $this->basketService = $basketService;
        $this->validator = $validator;
    }

    /**
     * @param IBasket $basket
     * @param ICustomer|NULL $customer
     * @return RedirectResponse|NULL
     */
    public function getRedirect(IBasket $basket, ICustomer $customer = NULL)
    {
        if ($basket->isEmpty()) {
            $this->controllerHelper->setFlashMessage(
                'Sorry, your basket is empty. Please add something before you checkout.',
                ControllerHelper::MESSAGE_ERROR
            );
            return new RedirectResponse(
                $this->controllerHelper->getLink(BasketControler::PAGE_BASKET)
            );
        }
        $errors = $this->validator->validate($basket)->getErrors();
        if ($errors) {
            return new RedirectResponse(
                $this->controllerHelper->getLink(BasketControler::PAGE_BASKET)
            );
        }

        $packageToProductDowngradeNotification = $this->basketService->getPackageToProductDowngradeNotification();
        $packageToPackageDowngradeNotification = $this->basketService->getPackageToPackageDowngradeNotification();
        if ($packageToProductDowngradeNotification || $packageToPackageDowngradeNotification) {
            return new RedirectResponse(
                $this->controllerHelper->getLink(BasketControler::PAGE_SUMMARY)
            );
        }
    }
}