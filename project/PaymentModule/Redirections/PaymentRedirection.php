<?php

namespace PaymentModule\Redirections;

use BasketModule\Contracts\IBasket;
use Exception;
use PaymentModel;
use PaymentModule\Contracts\IOrder;
use PaymentModule\Contracts\IPaymentRedirection;
use PaymentModule\Controllers\PaymentConfirmationController;
use RouterModule\Helpers\ControllerHelper;
use Symfony\Component\HttpFoundation\RedirectResponse;

class PaymentRedirection implements IPaymentRedirection
{
    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    /**
     * @var array
     */
    private $additionalParameters = [];

    /**
     * @param ControllerHelper $controllerHelper
     */
    public function __construct(ControllerHelper $controllerHelper)
    {
        $this->controllerHelper = $controllerHelper;
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    public function addAdditionalParameter($key, $value)
    {
        $this->additionalParameters[$key] = $value;
    }

    /**
     * @param IOrder $order
     * @param IBasket $basket
     * @return RedirectResponse
     */
    public function getConfirmation(IOrder $order, IBasket $basket)
    {
        $node = new PaymentModel();
        return $this->controllerHelper->redirectionTo(
            PaymentConfirmationController::ROUTE_PURCHASE_CONFIRMATION,
            $node->getConfirmationPageLinkParams($basket, $this->additionalParameters)

        );
    }

    /**
     * @param Exception|NULL $e
     * @return RedirectResponse
     */
    public function getFailure(Exception $e = NULL)
    {
        return $this->controllerHelper->redirectionTo('payment');
    }
}