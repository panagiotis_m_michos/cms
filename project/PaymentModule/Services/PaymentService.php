<?php

namespace PaymentModule\Services;

use BasketModule\Contracts\IBasket;
use Customer as OldCustomer;
use Exceptions\Business\CompanyImportException;
use Exceptions\Technical\CompanyException;
use Payment\AutoRenewalReenabler;
use PaymentModule\Contracts\IOrder;
use PaymentModule\Contracts\IPaymentRedirection;
use PaymentModule\Contracts\IPaymentResponse;
use PaymentModule\Exceptions\PaymentException;
use PaymentModule\Factories\PaymentResponseFactory;
use PaymentModule\Processors\IPaymentProcessor;
use PaymentService as OldPaymentService;
use SearchControler;
use Services\OrderService;
use Services\Payment\TokenService;
use Services\ProductService;
use SessionModule\ISession;
use UserModule\Contracts\ICustomer;
use UserModule\Services\IAuthenticator;

class PaymentService implements IPaymentProcessor
{
    /**
     * @var OrderService
     */
    private $orderService;

    /**
     * @var ProductService
     */
    private $productService;

    /**
     * @var PaymentResponseFactory
     */
    private $paymentResponseFactory;

    /**
     * @var TokenService
     */
    private $tokenService;

    /**
     * @var AutoRenewalReenabler
     */
    private $autoRenewalReenabler;

    /**
     * @var ISession
     */
    private $session;

    /**
     * @var IPaymentRedirection
     */
    private $paymentRedirection;

    /**
     * @param OrderService $orderService
     * @param ProductService $productService
     * @param PaymentResponseFactory $paymentResponseFactory
     * @param TokenService $tokenService
     * @param AutoRenewalReenabler $autoRenewalReenabler
     * @param ISession $session
     * @param IPaymentRedirection $paymentRedirection
     */
    public function __construct(
        OrderService $orderService,
        ProductService $productService,
        PaymentResponseFactory $paymentResponseFactory,
        TokenService $tokenService,
        AutoRenewalReenabler $autoRenewalReenabler,
        ISession $session,
        IPaymentRedirection $paymentRedirection
    )
    {
        $this->orderService = $orderService;
        $this->productService = $productService;
        $this->paymentResponseFactory = $paymentResponseFactory;
        $this->tokenService = $tokenService;
        $this->autoRenewalReenabler = $autoRenewalReenabler;
        $this->session = $session;
        $this->paymentRedirection = $paymentRedirection;
    }

    /**
     * @param ICustomer $customer
     * @param IBasket $basket
     * @param IPaymentResponse $paymentResponse
     * @return IOrder
     * @throws PaymentException
     */
    public function processPayment(ICustomer $customer, IBasket $basket, IPaymentResponse $paymentResponse)
    {
        try {
            $oldPaymentResponse = $this->paymentResponseFactory->convert($paymentResponse, $customer);
            $newerPaymentResponse = $this->paymentResponseFactory->convertToNewer($paymentResponse, $customer);
            $oldPaymentService = new OldPaymentService(new OldCustomer($customer->getId()), $basket, $customer, $this->orderService, $this->productService);
            $customer = $oldPaymentService->saveCustomerInformation($oldPaymentResponse, OldPaymentService::CUSTOMER_SIGN_IN, $customer->getEmail());
            $token = $this->tokenService->processToken($customer, $newerPaymentResponse);
            $orderId = $oldPaymentService->completeTransaction($oldPaymentResponse, $token);
            $order = $this->orderService->getOrderById($orderId);
            if ($this->autoRenewalReenabler->hasServicesToReenable($customer)) {
                $this->autoRenewalReenabler->reenable($customer);
                $this->paymentRedirection->addAdditionalParameter('renewalReenabled', TRUE);
            }
            $this->session->remove(IAuthenticator::TEMPORARY_CUSTOMER_KEY);
            SearchControler::resetCompanyName();
            return $order;
        } catch (CompanyException $e) {
            throw PaymentException::fromPrevious($e);
        } catch (CompanyImportException $e) {
            throw PaymentException::fromPrevious($e);
        }
    }
}