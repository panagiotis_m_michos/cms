<?php

namespace PaymentModule\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class PurchaseConfirmationForm extends AbstractType
{
    const NAME = 'payment_module_purchase_confirmation_form';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        $builder->setMethod('POST');
        $builder
            ->add(
                'continue',
                'submit'
            );

        $formFactory = $builder->getFormFactory();

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formFactory) {
                /** @var PurchaseConfirmationData $formData */
                $formData = $event->getData();
                $form = $event->getForm();

                if ($formData->canToggleAutoRenewal()) {
                    $form->add(
                        $formFactory->createNamed(
                            'enableAutoRenewal',
                            'checkbox',
                            NULL,
                            [
                                'required' => FALSE,
                                'label' => 'Auto-renew all subscription services (recommended)',
                                'label_attr' => [
                                    'style' => 'font-weight:300;',
                                ],
                                'auto_initialize' => FALSE
                            ]
                        )
                    );
                }
            }
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return self::NAME;
    }
}
