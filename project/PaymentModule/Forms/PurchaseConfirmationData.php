<?php

namespace PaymentModule\Forms;

class PurchaseConfirmationData
{

    /**
     * @var bool
     */
    private $canToggleAutoRenewal;

    /**
     * @var bool
     */
    private $enableAutoRenewal;

    /**
     * @param bool $canToggleAutoRenewal
     * @param bool $isEnabled
     */
    public function __construct($canToggleAutoRenewal = FALSE, $isEnabled = TRUE)
    {
        $this->canToggleAutoRenewal = $canToggleAutoRenewal;
        $this->enableAutoRenewal = $isEnabled;
    }

    /**
     * @return bool
     */
    public function getEnableAutoRenewal()
    {
        return $this->enableAutoRenewal;
    }

    /**
     * @param bool $enabledAutoRenewal
     */
    public function setEnableAutoRenewal($enabledAutoRenewal)
    {
        $this->enableAutoRenewal = $enabledAutoRenewal;
    }

    /**
     * @return bool
     */
    public function isAutoRenewalEnabled()
    {
        return $this->enableAutoRenewal;
    }

    /**
     * @return bool
     */
    public function canToggleAutoRenewal()
    {
        return $this->canToggleAutoRenewal;
    }
}
