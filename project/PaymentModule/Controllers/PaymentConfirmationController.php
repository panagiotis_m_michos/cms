<?php

namespace PaymentModule\Controllers;

use Basket;
use Entities\Customer;
use Entities\Order;
use HomeControler;
use MyServicesControler;
use Package;
use PaymentModule\Facades\PurchaseConfirmationFacade;
use PaymentModule\Forms\PurchaseConfirmationData;
use PaymentModule\Forms\PurchaseConfirmationForm;
use Services\ControllerHelper;
use Symfony\Component\HttpFoundation\RedirectResponse;
use TemplateModule\Renderers\IRenderer;

class PaymentConfirmationController
{
    const ROUTE_PURCHASE_CONFIRMATION = 'purchase_confirmation';
    const PAGE_PURCHASE_CONFIRMATION = 139;

    /**
     * @var IRenderer
     */
    private $renderer;

    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var Basket
     */
    private $basket;

    /**
     * @var PurchaseConfirmationFacade
     */
    private $purchaseConfirmationFacade;

    /**
     * @param IRenderer $renderer
     * @param ControllerHelper $controllerHelper
     * @param Customer $customer
     * @param Basket $basket
     * @param PurchaseConfirmationFacade $purchaseConfirmationFacade
     */
    public function __construct(
        IRenderer $renderer,
        ControllerHelper $controllerHelper,
        Customer $customer,
        Basket $basket,
        PurchaseConfirmationFacade $purchaseConfirmationFacade
    )
    {
        $this->renderer = $renderer;
        $this->controllerHelper = $controllerHelper;
        $this->customer = $customer;
        $this->basket = $basket;
        $this->purchaseConfirmationFacade = $purchaseConfirmationFacade;
    }

    /**
     * @param int $orderId
     * @param bool $renewalReenabled
     * @return RedirectResponse
     */
    public function confirmation($orderId, $renewalReenabled = FALSE)
    {
        $order = $this->purchaseConfirmationFacade->getCustomerOrder($this->customer, $orderId);
        if (!$order) {
            return new RedirectResponse($this->controllerHelper->getLink(HomeControler::HOME_PAGE));
        }

        $customerToken = $this->purchaseConfirmationFacade->getTokenByCustomer($this->customer);
        $canToggleAutoRenewal = $this->purchaseConfirmationFacade->canToggleAutoRenewal($order, $customerToken);

        $confirmationForm = $this->controllerHelper->buildForm(
            new PurchaseConfirmationForm(),
            new PurchaseConfirmationData($canToggleAutoRenewal, $order->isPaidBySagePayToken())
        );

        $company = $this->purchaseConfirmationFacade->getCompanyByOrder($order);
        if ($confirmationForm->isSubmitted() && $confirmationForm->isValid()) {
            $this->purchaseConfirmationFacade->processConfirmationForm($confirmationForm->getData(), $order, $customerToken);
            return $this->purchaseConfirmationFacade->redirectAfterConfirmation($company);
        }

        $variables = [
            'renewalReenabled' => $renewalReenabled,
            'myServicesLink' => $this->controllerHelper->getLink(MyServicesControler::PAGE_SERVICES),
            'confirmationForm' => $confirmationForm->createView(),
            'customerToken' => $customerToken,
            'company' => $company,
            'order' => $order,
            'basket' => clone $this->basket,
            'ecommerce' => !$this->basket->isEmpty(),
        ];

        $trackingVariables = $this->getTrackingVariables($order);

        $this->basket->clear(TRUE);

        return $this->renderer->render($variables + $trackingVariables);
    }

    /**
     * @param int $orderId
     * @return RedirectResponse
     */
    public function downloadInvoice($orderId)
    {
        $order = $this->purchaseConfirmationFacade->getCustomerOrder($this->customer, $orderId);
        if (!$order) {
            return new RedirectResponse($this->controllerHelper->getLink(HomeControler::HOME_PAGE));
        }

        return $this->purchaseConfirmationFacade->downloadInvoice($order);
    }

    /**
     * @param Order $order
     * @return array
     */
    private function getTrackingVariables(Order $order)
    {
        $standardPackageTypes = [
            Package::PACKAGE_BASIC => 'Basic',
            Package::PACKAGE_DIGITAL => 'Digital',
            Package::PACKAGE_CONTRACTOR => 'Contractor',
            Package::PACKAGE_PRINTED => 'Printed',
            Package::PACKAGE_PRIVACY => 'Privacy',
            Package::PACKAGE_COMPREHENSIVE => 'Comprehensive',
            Package::PACKAGE_ULTIMATE => 'Ultimate',
            Package::PACKAGE_FIVE_POUNDS => 'Five Pounds Formation',
        ];

        $variables = [];
        if ($this->basket->getOrderId() == $order->getId() && $this->basket->hasPackageIncluded()) {
            $package = $this->basket->getPackage();

            $variables = [
                'internationalPackage' => $package->isInternational(),
                'standardPackage' => in_array($package->getId(), array_keys($standardPackageTypes)),
            ];
        }

        return $variables;
    }
}
