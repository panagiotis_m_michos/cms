{include file="@header.tpl"}

<div id="maincontent" class="f-sterling padding20">
    {if $renewalReenabled}
        <div class="message message-green">
            <ul>
                <li>We've turned your auto renew settings back on (but only for your services where auto renew was previously on).</li>
                <li>You can confirm your settings at <a href="{$myServicesLink}" target="_blank">My Services</a>.</li>
                <li>Please note that overdue services must be renewed manually.</li>
            </ul>
        </div>
    {/if}

    {if $visibleTitle}
        <h1>{$title}</h1>
    {/if}

    <p class="largefont clearfix">
        <i class="fa fa-heart fa-fw grey5 padding5 fleft font20 txtbold"></i>
        <span class="fleft width90pc mleft10 ptop5">Thank you for your purchase. We're delighted that you have chosen Company Formation MadeSimple to be part of your business adventure!</span>
    </p>
    {if $company}
        <p class="largefont clearfix">
            <i class="fa fa-at fa-fw grey5 padding5 fleft font20 txtbold"></i>
            <span class="fleft width90pc mleft10 ptop5">We've emailed you a copy of this payment confirmation too.</span>
        </p>
    {/if}

    {$formHelper->setTheme($confirmationForm, 'cms.html.twig')}
    {$formHelper->start($confirmationForm, ['attr' => ['class' => 'purchase-confirmation-form']]) nofilter}
        {if $company}
            <div class="bg-grey1 padding30 mtop30 mbottom30 mleft-30 mright-30">
                <h2>What's next?</h2>
                <p class="largefont">Your company is not yet registered. We can guide you through the process now:</p>
                {$formHelper->widget($confirmationForm['continue'], ['label' => 'Complete my company registration', 'attr' => ['class' => 'ladda-button', 'data-style' => 'expand-right', 'data-color' => 'orange', 'data-size' => 'l']]) nofilter}
            </div>
        {else}
            <div class="bg-grey1 padding30 mtop30 mbottom30 mleft-30 mright-30">
                <h2>What's next?</h2>
                <p class="largefont">
                    <i class="fa fa-at fa-fw padding5 fleft orange2 font20 txtbold"></i>
                    <span class="fleft width90pc orange2 mleft10 font20 txtbold">Please check your email</span>
                    <span class="mleft10">Instructions and further actions are included in your purchase confirmation email.</span>
                </p>
                <div class="txtright midfont">
                    {$formHelper->widget($confirmationForm['continue'], ['label' => 'Take me to my Dashboard', 'attr' => ['class' => 'linkButton']]) nofilter}
                </div>
            </div>
        {/if}

        <h2>Order confirmation</h2>
        <p>
            Date: {$order->getDtc()|date_format:"%d %B %Y"}<br />
            Order ID: {$order->getId()}
        </p>
            {if isset($confirmationForm['enableAutoRenewal'])}
                <div class="mbottom20">
                {$formHelper->widget($confirmationForm['enableAutoRenewal']) nofilter}
                <div class="tooltip-link" rel="auto-renewal">
                    <i class="fa fa-info-circle font15 grey2" data-toggle="tooltip" data-placement="right" title="
                           By activating this option the yearly subscription services will be automatically renewed;
                           this will guarantee a disruption-free service (you can modify this later).">
                    </i>
                </div>
                <div class="purchase-confirmation-renewal-info">
                    Payment method to be used: <strong>{$customerToken->getCardTypeText()} card ending in {$customerToken->getCardNumber()}</strong><br>
                    <span class="grey2">You can change your payment method at any time from the Payment Methods page</span>
                </div>
                </div>
            {/if}
    {$formHelper->end($confirmationForm) nofilter}

    {* ORDER SUMMARY *}
    <div class="purchase-confirmation-summary">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="formtable mnone mbottom">
            <tr>
                <th width="360">Product</th>
                <th width="125" class="txtcenter" valign="top">Quantity</th>
                <th width="125">Price</th>
            </tr>

            {foreach $order->getItems() as $item}
                <tr>
                    <td>
                        {$item->getProductTitle()}
                        {product var="itemProduct" id=$item->getProductId()}
                        {if $itemProduct and $itemProduct->hasRenewalProduct()}
                            <i>({$itemProduct->getSubscriptionPeriodText()})</i>
                        {/if}
                    </td>
                    <td align="center" valign="top">{$item->getQty()}</td>
                    <td>&pound;{$item->getSubtotal()|number_format:2}</td>
                </tr>
            {/foreach}

            <tr class="bb">
                <td><strong>Subtotal</strong></td>
                <td>&nbsp;</td>
                <td><strong>&pound;{$order->getSubtotal()|number_format:2}</strong></td>
            </tr>
            {* SHOW VOUCHER *}
            {if $order->hasVoucher()}
                <tr>
                    <td class="voucher">Voucher {$order->getVoucherName()}</td>
                    <td>&nbsp;</td>
                    <td class="red">-&pound;{$order->getDiscount()|number_format:2}</td>
                </tr>
            {/if}
            <tr>
                <td>VAT</td>
                <td>&nbsp;</td>
                <td>&pound;{$order->getVat()|number_format:2}</td>
            </tr>
            {if $order->getCredit()}
                <tr>
                    <td><strong>Credit</strong></td>
                    <td>&nbsp;</td>
                    <td><strong>-&pound;{$order->getCredit()|number_format:2}</strong></td>
                </tr>
            {/if}
            <tr>
                <td><strong>Total including delivery charges and tax</strong></td>
                <td>&nbsp;</td>
                <td><strong>&pound;{$order->getTotal()|number_format:2}</strong></td>
            </tr>
        </table>
    </div>
        
    <p class="txtright midfont"><i class="fa fa-file-pdf-o"></i>&nbsp;
        <a href="{url route='purchase_confirmation_download_invoice' orderId=$order->getId()}">Download invoice</a>
    </p>
    
</div>

<!-- Google Code for CMS Purchase Conversion Page -->
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 1071747309;
    var google_conversion_language = "en";
    var google_conversion_format = "3";
    var google_conversion_color = "ffffff";
    var google_conversion_label = "3wJUCIKl9wEQ7aGG_wM";
    var google_conversion_value = 0;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt=""
             src="//www.googleadservices.com/pagead/conversion/1071747309/?label=3wJUCIKl9wEQ7aGG_wM&amp;guid=ON&amp;script=0"/>
    </div>
</noscript>

<!-- TARGETED AD SOLUTIONS Tracking pixel -->
<script type="text/javascript">
window.nx_type = 2;
window.nx_campaignid = 124;
window.nx_source = 0;
window.nx_advertiserid = 80;
window.nx_productids = "{foreach from=$order->getItems() item=item name=orderItems}{$item->getProductId()}{if !$smarty.foreach.orderItems.last},{/if}{/foreach}";
window.nx_price = "{$order->getTotal()|number_format:2}";
window.nx_currency = "GBP";
window.nx_code = "{$order->getId()}";
window.nx_text = "{$order->getVoucherName()}";
window.nx_time = "{$smarty.now}";
</script>
<script async="true" type="text/javascript"
        src="//s.targetedadsolutions.com/retarget/nr_v2.min.js">
</script>

{include file="@footer.tpl"}
