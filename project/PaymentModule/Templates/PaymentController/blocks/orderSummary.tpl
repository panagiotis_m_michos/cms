<div class="order-summary">
    <div class="col-xs-12">
        <div class="row total-to-pay">
            <div class="col-xs-12">
                <h3 class="margin0"><i class="fa fa-shopping-basket fa-fw" aria-hidden="true"></i>&nbsp;Order summary</h3>
            </div>
        </div>
        {if $basket->getItemsCount() <= 5}
            {foreach $basket->getItems() as $key => $item}
                <div class="row basket-items">
                    <div class="col-xs-8 small">
                        {$item->getLngTitle()}
                    </div>
                    <div class="col-xs-4 small text-right">{$item->getPrice()|currency}</div>
                </div>
            {/foreach}
        {else}
            <div class="row basket-items">
                <div class="col-xs-8 small">{$basket->getItemsCount()} item(s)</div>
                <div class="col-xs-4 small text-right">{$basket->getPrice('subTotal2')|currency}</div>
            </div>
        {/if}
        <div class="row basket-items subtotal">
            <div class="col-xs-7">Subtotal</div>
            <div class="col-xs-5 text-right">{$basket->getPrice('subTotal2')|currency}</div>
        </div>
        <div class="row basket-items">
            <div class="col-xs-7">VAT</div>
            <div class="col-xs-5 text-right">{$basket->getPrice('vat')|currency}</div>
        </div>

        <div class="row basket-items {if !$basket->getVoucher()} hidden{/if}">
            <div class="col-xs-7">Voucher</div>
            <div class="col-xs-5 text-right">-{$basket->getPrice('discount')|currency}</div>
        </div>

        <div class="row basket-items {if !$basket->isUsingCredit()} hidden{/if}">
            <div class="col-xs-7">Credit</div>
            <div class="col-xs-5 text-right">-{$basket->getPrice('account')|currency}</div>
        </div>

        <div class="row total-to-pay">
            <div class="col-xs-7"><strong>Total to pay</strong></div>
            <div class="col-xs-5 text-right"><strong>{$basket->getTotalPrice()|currency}</strong></div>
        </div>
    </div>
</div>