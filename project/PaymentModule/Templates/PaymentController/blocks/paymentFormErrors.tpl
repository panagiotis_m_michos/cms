{if isset($formErrorsCount) && $formErrorsCount != 0}
    <div class="alert alert-danger">Form has {$formErrorsCount} errors...</div>
{/if}