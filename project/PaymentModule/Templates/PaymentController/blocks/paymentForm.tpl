<div class="width100 bg-white" style="border-bottom: 2px solid #ddd;">
    <div class="container pad025">
        <div class="row">
            <div class="col-md-12">
                <ul class="progress-indicator">
                    <li class="completed"><span>Search</span><span class="bubble"></span></li>
                    <li class="completed"><span>Select</span><span class="bubble"></span></li>
                    <li class="active"><span>Buy</span><span class="bubble"></span></li>
                    <li><span>Complete</span><span class="bubble"></span></li>
                </ul>
            </div>
        </div>

        {include '@blocks/flashMessageBt.tpl'}
        {include file="./paymentFormErrors.tpl" formErrorsCount=$formErrorsCount}

        <div class="row">
            <div class="col-xs-12 col-md-6"><h1>Payment</h1></div>

            {if $basket->hasIncorporationCompanyName()}
            <div class="col-xs-12 col-md-6 incorporation-text-align">
                <h3 class="margin0">{$basket->getIncorporationCompanyName()}</h3>
                <p class="black60">Subject to approval by Companies House</p>
            </div>
            {/if}
        </div>
        
        <div id="payment-form-block" class="row">
            <div class="col-xs-12 col-md-8">
                {if isset($loginAvailability)}
                    <div class="col-xs-12 brick-desktop btm20">
                        {include file="../../../../UserModule/Templates/customer-availability-login.tpl" form=$loginAvailability->getFormView() emailConfirmed=$loginAvailability->isEmailConfirmed() requiresToLogin=$loginAvailability->isRequiredToLogIn() returnType='payment'}
                    </div>
                {/if}
                
                <div class="col-xs-12" style="padding: 0;">
                
                    {$formHelper->setTheme($form, 'payment.html.twig')}
                    {$formHelper->start($form, ['attr' => ['id' => 'payment_form']]) nofilter}


                    {$formHelper->errors($form) nofilter}

                    <div class="brick-desktop btm20{if empty($customer) || !$customer->hasCredit()} hidden{/if}">
                        <h3 class="top0">Pay using credit</h3>
                        <p>Available credit on your account: <span rv-text="customer.credit | price">{if !empty($customer)}{$customer->getCredit()|currency}{else}0{/if}</span></p>
                        <div class="{if !empty($customer) && !$basket->isCreditMinimumAmountReached($customer->getCredit())}hidden{/if}" rv-show="isCreditMinimumAmountReached">
                            <p><em>When using account credit towards your purchase, the remaining amount to pay by card or PayPal must be over &pound;1.</em></p>
                        </div>
                        <div class="{if !empty($customer) && $basket->isCreditMinimumAmountReached($customer->getCredit())}hidden{/if}" rv-hide="isCreditMinimumAmountReached">
                            {$formHelper->widget($form['useCredit']) nofilter}
                            <div class="top10 {if !$basket->isInsufficientCredit()}hidden{/if}" rv-show="isInsufficientCredit < paymentDetails.useCredit">
                                <div><span class="glyphicon glyphicon-exclamation-sign"></span> Remaining amount to pay: <span rv-text="basket.totalPrice | price">{$basket->getTotalPrice()|currency}</span></div>
                                <p><em>The available credit on your account is insufficient for the total amount, please enter your card details below so the remaining amount can be deducted from your card.</em></p>
                            </div>
                        </div>
                    </div>

                    <div id="payment-with-third-party" rv-hide="isPayingWithCreditOnly < paymentDetails.useCredit">
                        <div class="brick-desktop btm20">
                            <h3 class="top0">Payment method</h3>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <div id="paymentOption">
                                        <div class="form-group {if !$form['paymentType']->vars['valid']}has-error{/if}">
                                            <div class="col-sm-12">
                                                {$formHelper->widget($form['paymentType']) nofilter}
                                                {$formHelper->errors($form['paymentType']) nofilter}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="card-details-container" rv-show="isUsingCard < paymentDetails.paymentType">
                            <div class="brick-desktop btm20">
                                <h3 class="top0">Card details</h3>
                                <ul class="fa-ul black60 btm20">
                                    <li><i class="fa-li fa fa-lock fa-fw"></i>Every payment is secured by industry standard 256-bit encryption.</li>
                                    <li><i class="fa-li fa fa-fw"></i>We never store your card details on our servers.</li>
                                </ul>
                                {$formHelper->row($form['cardDetails']['cardholder']) nofilter}
                                {$formHelper->row($form['cardDetails']['cardType']) nofilter}
                                {$formHelper->row($form['cardDetails']['cardNumber']) nofilter}
                                {$formHelper->row($form['cardDetails']['expiryDate']) nofilter}
                                <div rv-show="isMaestro < paymentDetails.cardDetails.cardType">
                                    {$formHelper->row($form['cardDetails']['validFrom']) nofilter}
                                    {$formHelper->row($form['cardDetails']['issueNumber']) nofilter}
                                </div>
                                {$formHelper->row($form['cardDetails']['securityCode']) nofilter}
                            </div>
                            <div class="brick-desktop btm20">
                                <h3 class="top0">Billing address</h3>
                                {$formHelper->row($form['payerDetails']['address1']) nofilter}
                                {$formHelper->row($form['payerDetails']['address2']) nofilter}
                                {$formHelper->row($form['payerDetails']['city']) nofilter}
                                {$formHelper->row($form['payerDetails']['postcode']) nofilter}
                                {$formHelper->row($form['payerDetails']['country']) nofilter}
                                <div rv-show="isUsa < paymentDetails.payerDetails.country">
                                    {$formHelper->row($form['payerDetails']['state']) nofilter}
                                </div>
                                <div rv-show="isSpain < paymentDetails.payerDetails.country">
                                    <div class="alert-info alert">
                                        Our main payment provider, Sagepay, is currently experiencing issues with some Spanish cards. If you have
                                        trouble with your payment we suggest selecting <strong>Paypal</strong>, from the Payment Method, above.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="brick-desktop btm20">
                        <h3 class="top0">Confirm payment</h3>
                        <div id="submit-block"{if !empty($showAuth)} style="display:none" {/if}>
                            <p><strong>Total to pay: <span>{$basket->getTotalPriceWithoutCredit()|currency}</span></strong></p>

                            <button type="submit" id="payment-submit-button" class="btn btn-default button-wide-mobile btn-lg" rv-text="buttonText">Pay using Card</button>
                            <p class="black60 top10"><em>This is the final step, your payment will now be processed.</em></p>
                            <p>By making this purchase, you agree to our <a href="{url route=473}" target="_blank">Terms and conditions <i class="fa fa-external-link" aria-hidden="true"></i></a></p>
                        </div>
                    </div>
                    {if isset($loginAvailability)}
                        <div class="disabled-content{if $loginAvailability->isEmailConfirmed() && !$loginAvailability->isRequiredToLogIn()} hidden{/if}" rv-hide="loginAvailability.canUseCredentials < emailConfirmed requiresToLogin"></div>
                    {/if}

                </div>

            </div>

            {$formHelper->end($form) nofilter}

            <div class="col-xs-12 col-md-4">
                {include file="./orderSummary.tpl" basket=$basket}
                <img src="/components/ui_server/imgs/payment/payment-security.png" alt="Payment Secure" width="164" height="192" class="top20">
            </div>

        </div>
                    
        {if !empty($showAuth)}
            <div id="auth-form-block">
                <h3 class="">Please complete the 3D authentication process below</h3>
                <strong style="margin-bottom: 5px;display: block">This step is only for authentication. It's an added security step by your bank.</strong>
                <iframe name="sage-iframe" src="{url route='sage_authentication'}" width="100%" height="450"></iframe>
            </div>
        {/if}
    </div>
</div>

<script type="text/javascript">
    payment.bind(
            '#payment_form',
            new payment.PaymentDetails({$paymentDetails|json nofilter}, {$basket|json nofilter}, {$customer|json nofilter}, 'Pay using Card', window.loginAvailability),
            '{$firstError}'
    );
</script>