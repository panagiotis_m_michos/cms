<div id="free-order-block" class="width100 bg-white" style="border-bottom: 2px solid #ddd;">
    <div class="container pad054">
        <div class="row">
            <div class="col-md-12">
                <ul class="progress-indicator">
                    <li class="completed"><span>Search</span><span class="bubble"></span></li>
                    <li class="completed"><span>Select</span><span class="bubble"></span></li>
                    <li class="active"><span>Buy</span><span class="bubble"></span></li>
                    <li><span>Complete</span><span class="bubble"></span></li>
                </ul>
            </div>
        </div>

        {include '@blocks/flashMessageBt.tpl'}

        <div class="row">
            <div class="col-xs-12"><h1>Free order</h1></div>

            <div class="col-xs-12 col-md-8">
                {if isset($loginAvailability)}
                    <div class="col-xs-12 brick-desktop btm20">
                        {include file="../../../../UserModule/Templates/customer-availability-login.tpl" form=$loginAvailability->getFormView() emailConfirmed=$loginAvailability->isEmailConfirmed() requiresToLogin=$loginAvailability->isRequiredToLogIn()}
                    </div>
                {/if}
                <div id="free-order-block" class="col-xs-12 brick-desktop btm20">
                    <form action="{url route='payment'}" method="post">
                        <h3 class="top0">
                            {if isset($customer)}
                            Your order today is free, please click Confirm Order to proceed.
                            {else}
                            Your order today is free, once you have entered your email address, please click Confirm Order to proceed.
                            {/if}
                        </h3>
                        <div class="form-group">
                            <button class="btn btn-default button-wide-mobile btn-lg" type="submit" id="payment-submit-button">Confirm Order</button>
                        </div>
                        <p class="black60"><em>Your items will be added to your account.</em></p>
                    </form>
                    {if isset($loginAvailability)}
                        <div class="row disabled-content{if $loginAvailability->isEmailConfirmed()} hidden{/if}" rv-hide="canUseCredentials < emailConfirmed requiresToLogin"></div>
                    {/if}
                </div>
            </div>
            <div class="col-xs-12 col-md-4 btm20">
                {include file="./orderSummary.tpl" basket=$basket}
                <img src="/components/ui_server/imgs/payment/payment-security.png" alt="Payment Secure" width="164" height="192" class="top20">
            </div>
        </div>
    </div>
</div>

{if isset($loginAvailability)}
<script type="text/javascript">
    rivets.bind($('#free-order-block'), window.loginAvailability)
</script>
{/if}