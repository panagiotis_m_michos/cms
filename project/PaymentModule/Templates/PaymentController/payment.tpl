{extends '@structure/layout.tpl'}

{block content}

{*{block #viewport}<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">{/block}*}

    {if $basket->isFreePurchase()}
        {include file="./blocks/freePurchase.tpl"}
    {else}
        {include file="./blocks/paymentForm.tpl" form = $paymentForm}
    {/if}

    {*<script type="text/javascript">*}
        {*_gaq.push(['_trackPageview', '/page1223en.html']);*}
    {*</script>*}

{/block}