<html>
    <head>
        {scripts file='webloader_front.neon' section='responsive'}
        <script type="text/javascript">
            var AuthForm = function(contextWindow, beforeElement) {
                var context = contextWindow.document;
                this.handleSuccess = function(location) {
                    contextWindow.location = location;
                };
                this.handleError = function(error, button, form) {
                    var errorElem = '<div class="alert alert-danger">' + $('<div/>').html(error).text() + '</div>';
                    $(beforeElement, context).prepend(errorElem);
                    //hide frame						
                    $(form, context).hide();
                    //buttons
                    $(button, context).show();
                    $(context).scrollTop(1);
                };
            }
        </script>
    </head>
    <body>
        {if !empty($redirect)}
            <div style="font-family:Arial;font-size:14px">
                <h3>3D authentication successful</h3>
                <p>Redirecting you to the confirmation page ...</p>
                <p>If you are not redirected automatically please press <a target="_parent" href="{$redirect}">continue</a></p>
            </div>
        {elseif !empty($error)}
            <div style="font-family:Arial;font-size:14px">
                <h3>3D authentication failed. Please specify the correct password</h3>
                <div>{$error}</div>
            </div>
            <p>Please press <a target="_parent" href="{url route='payment'}">continue</a> to re-enter the process</p>
        {/if}
        <script type="text/javascript">
            var error = {if !empty($error)}'{$error}'{else}false{/if};
            var redirectParent = {if !empty($redirect)}'{$redirect}'{else}false{/if};
            var authForm = new AuthForm(window.parent, '#payment-form-block');
            if (redirectParent) {
                authForm.handleSuccess(redirectParent);
            } else if (error) {
                authForm.handleError(error, '#submit-block', '#auth-form-block');
            }
        </script>
    </body>
</html>


