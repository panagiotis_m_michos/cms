<?php

namespace MyServicesModule\Controllers;

use Entities\Company;
use Entities\Customer;
use Entities\Service;
use EntityNotFound;
use Exceptions\Business\CompanyException;
use MyServicesControler;
use MyServicesModule\Facades\SettingsFacade;
use MyServicesModule\Renderers\CompanyBlockRenderer;
use Services\CompanyService;
use Services\ControllerHelper;
use Services\ServiceService;
use ServiceSettingsModule\Services\ServiceSettingsService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class ServiceSettingsController
{
    const ROUTE_SET_MY_SERVICE_SETTINGS = 'set_my_service_settings';
    const ROUTE_DISABLE_REMINDERS_FOR_COMPANY = 'disable_reminders_for_company_services';
    const ROUTE_ENABLE_AUTO_RENEWAL = 'enable_auto_renewal';
    const ROUTE_DISABLE_AUTO_RENEWAL = 'disable_auto_renewal';

    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var ServiceService
     */
    private $serviceService;

    /**
     * @var ServiceSettingsService
     */
    private $serviceSettingsService;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var CompanyBlockRenderer
     */
    private $companyBlockRenderer;

    /**
     * @param ControllerHelper $controllerHelper
     * @param Customer $customer
     * @param ServiceService $serviceService
     * @param ServiceSettingsService $serviceSettingsService
     * @param CompanyService $companyService
     * @param SettingsFacade $settingsFacade
     * @param CompanyBlockRenderer $companyBlockRenderer
     */
    public function __construct(
        ControllerHelper $controllerHelper,
        Customer $customer,
        ServiceService $serviceService,
        ServiceSettingsService $serviceSettingsService,
        CompanyService $companyService,
        SettingsFacade $settingsFacade,
        CompanyBlockRenderer $companyBlockRenderer
    )
    {
        $this->controllerHelper = $controllerHelper;
        $this->customer = $customer;
        $this->serviceService = $serviceService;
        $this->serviceSettingsService = $serviceSettingsService;
        $this->companyService = $companyService;
        $this->settingsFacade = $settingsFacade;
        $this->companyBlockRenderer = $companyBlockRenderer;
    }

    /**
     * @param int $serviceId
     * @return JsonResponse
     */
    public function getServiceSettings($serviceId)
    {
        $service = $this->getCustomerServiceById($serviceId);
        $settings = $this->serviceSettingsService->getSettingsByService($service);

        return new JsonResponse($settings);
    }

    /**
     * @param int $serviceId
     * @return Response
     */
    public function setServiceSetting($serviceId)
    {
        $service = $this->getCustomerServiceById($serviceId);
        $serviceSettings = $this->serviceSettingsService->getSettingsByService($service);

        $setting = $this->getSettingFromRequest();
        $this->settingsFacade->processSettingsArray($serviceSettings, $setting);

        return $this->getServiceSettings($serviceId);
    }

    /**
     * @param int $companyId
     * @return JsonResponse
     */
    public function disableRemindersForCompany($companyId)
    {
        $company = $this->getCustomerCompanyById($companyId);
        $this->serviceSettingsService->disableEmailRemindersByCompany($company);

        $url = $this->controllerHelper->getLink(
            MyServicesControler::PAGE_SERVICES,
            ['disabledRemindersCompanyId' => $companyId]
        );

        return new RedirectResponse($url);
    }

    /**
     * @param int $serviceId
     * @return JsonResponse
     */
    public function enableAutoRenewal($serviceId)
    {
        $service = $this->getCustomerServiceById($serviceId);
        $settings = $this->serviceSettingsService->getSettingsByService($service);
        $token = $this->customer->getActiveToken();
        if ($token) {
            $this->settingsFacade->enableAutoRenewal($settings, $token);

            $checkedServiceIds = $this->controllerHelper->getRequest()->query->get('checkedServiceIds', []);
            $checkedServices = $this->serviceService->getServicesByIds($checkedServiceIds);
            $template = $this->companyBlockRenderer->getHtml($this->customer, $service->getCompany(), $checkedServices);

            return new JsonResponse(
                [
                    'error' => FALSE,
                    'template' => $template,
                    'showAdditionalInfo' => $service->expiresToday() || $service->isOverdue(),
                ]
            );
        } else {
            throw new NotFoundHttpException;
        }
    }

    /**
     * @param int $serviceId
     * @return JsonResponse
     */
    public function disableAutoRenewal($serviceId)
    {
        $service = $this->getCustomerServiceById($serviceId);
        $settings = $this->serviceSettingsService->getSettingsByService($service);
        $this->settingsFacade->disableAutoRenewal($settings);

        $checkedServiceIds = $this->controllerHelper->getRequest()->query->get('checkedServiceIds', []);
        $checkedServices = $this->serviceService->getServicesByIds($checkedServiceIds);
        $template = $this->companyBlockRenderer->getHtml($this->customer, $service->getCompany(), $checkedServices);

        return new JsonResponse(['error' => FALSE, 'template' => $template]);
    }

    /**
     * @param int $companyId
     * @return Company
     * @throws CompanyException
     */
    private function getCustomerCompanyById($companyId)
    {
        $company = $this->companyService->getCustomerCompany($this->customer, $companyId);

        if (!$company) {
            throw new NotFoundHttpException;
        }

        return $company;
    }

    /**
     * @param int $serviceId
     * @return Service
     */
    private function getCustomerServiceById($serviceId)
    {
        try {
            return $this->serviceService->getCustomerService($this->customer, $serviceId);
        } catch (EntityNotFound $e) {
            throw new NotFoundHttpException;
        }
    }

    /**
     * @return array
     */
    private function getSettingFromRequest()
    {
        $request = $this->controllerHelper->getRequest();
        $settings = json_decode($request->getContent(), TRUE);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new BadRequestHttpException;
        }

        if (!isset($settings['setting']) || !isset($settings['value'])) {
            throw new UnprocessableEntityHttpException;
        }

        return $settings;
    }
}
