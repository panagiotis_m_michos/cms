<?php

namespace MyServicesModule\Facades;

use Entities\Payment\Token;
use Entities\ServiceSettings;
use ServiceSettingsModule\Facades\ServiceSettingsFacade;

class SettingsFacade
{

    /**
     * @var ServiceSettingsFacade
     */
    private $settingsFacade;

    /**
     * @param ServiceSettingsFacade $settingsFacade
     */
    public function __construct(ServiceSettingsFacade $settingsFacade)
    {
        $this->settingsFacade = $settingsFacade;
    }

    /**
     * @param ServiceSettings $serviceSettings
     * @param array $settingData
     */
    public function processSettingsArray(ServiceSettings $serviceSettings, array $settingData)
    {
        $settingName = $settingData['setting'];
        $settingValue = $settingData['value'];

        if ($settingName == 'emailReminders') {
            if ($settingValue) {
                $this->settingsFacade->enableEmailReminders($serviceSettings);
            } else {
                $this->settingsFacade->disableEmailReminders($serviceSettings);
            }
        }
    }

    /**
     * @param ServiceSettings $settings
     * @param Token $token
     */
    public function enableAutoRenewal(ServiceSettings $settings, Token $token)
    {
        $this->settingsFacade->enableAutoRenewal($settings, $token);
    }

    /**
     * @param ServiceSettings $settings
     */
    public function disableAutoRenewal(ServiceSettings $settings)
    {
        $this->settingsFacade->disableAutoRenewal($settings);
    }
}
