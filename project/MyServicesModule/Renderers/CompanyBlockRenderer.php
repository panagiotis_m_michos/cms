<?php

namespace MyServicesModule\Renderers;

use Doctrine\ORM\Internal\Hydration\IterableResult;
use Entities\Company;
use Entities\Customer;
use Entities\Service;
use Factories\Front\CompanyViewFactory;
use FTemplate;
use Services\CompanyService;
use Services\RenewForm;
use Services\ServiceService;
use ServiceSettingsModule\Services\ServiceSettingsService;

class CompanyBlockRenderer
{

    /**
     * @var FTemplate
     */
    private $template;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var ServiceService
     */
    private $serviceService;

    /**
     * @var ServiceSettingsService
     */
    private $serviceSettingsService;

    /**
     * @var CompanyViewFactory
     */
    private $companyViewFactory;

    /**
     * @param FTemplate $template
     * @param CompanyService $companyService
     * @param ServiceService $serviceService
     * @param ServiceSettingsService $serviceSettingsService
     * @param CompanyViewFactory $companyViewFactory
     */
    public function __construct(
        FTemplate $template,
        CompanyService $companyService,
        ServiceService $serviceService,
        ServiceSettingsService $serviceSettingsService,
        CompanyViewFactory $companyViewFactory
    )
    {
        $this->template = $template;
        $this->companyService = $companyService;
        $this->serviceService = $serviceService;
        $this->serviceSettingsService = $serviceSettingsService;
        $this->companyViewFactory = $companyViewFactory;
    }

    /**
     * @param Customer $customer
     * @param Company $company
     * @param Service[] $checkedServices
     * @return string
     */
    public function getHtml(Customer $customer, Company $company, array $checkedServices)
    {
        $customerCompanies = $this->companyService->getCustomerServiceCompaniesViews($customer);

        $form = new RenewForm($customer->getId() . '_renew_services');
        $form->startup($customerCompanies, $checkedServices, $this->serviceSettingsService);

        $this->template->form = $form;
        $this->template->company = $this->companyViewFactory->create($company);
        $this->template->settingsService = $this->serviceSettingsService;
        $this->template->customer = $customer;

        return $this->template->getHtml('MyServices/@company.tpl');
    }
}
