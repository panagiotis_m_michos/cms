<?php

namespace WorkflowEngineModule\Queries;

use CustomerModule\QueryObjects\CustomerQuery as OriginalCustomerQuery;
use CustomerModule\QueryObjects\ICustomerQuery;
use CustomerModule\QueryObjects\ICustomerQueryFactory;
use DateTime;
use DateTimeInterface;
use Entities\Customer;
use Kdyby\Persistence\Queryable;
use DusanKasan\Knapsack\Collection;
use Traversable;

class CustomerQuery implements ICustomerQuery
{
    /**
     * @var callable[]
     */
    private $filters = [];

    /**
     * @var array
     */
    private $queryModifiers = [];

    /**
     * @var Queryable
     */
    private $repository;

    public function __construct(Queryable $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param $customerId
     * @return $this
     */
    public function withId($customerId)
    {
        $this->queryModifiers[] = function (ICustomerQuery $query) use ($customerId) {
            return $query->withId($customerId);
        };

        return $this;
    }

    /**
     * @param $email
     * @return $this
     */
    public function withEmail($email)
    {
        $this->queryModifiers[] = function (ICustomerQuery $query) use ($email) {
            return $query->withEmail($email);
        };

        return $this;
    }

    /**
     * @return $this
     */
    public function active()
    {
        $this->queryModifiers[] = function (ICustomerQuery $query) {
            return $query->active();
        };

        return $this;
    }

    /**
     * @param string $roleId
     * @return $this
     */
    public function byRole($roleId)
    {
        $this->queryModifiers[] = function (ICustomerQuery $query) use ($roleId) {
            return $query->byRole($roleId);
        };

        return $this;
    }

    /**
     * @param array $productIds
     * @return $this
     */
    public function withSomeProducts(array $productIds)
    {
        $this->filters[] = function (ICustomerQuery $query) use ($productIds) {
            return $query->withSomeProducts($productIds);
        };

        return $this;
    }

    /**
     * @return $this
     */
    public function withoutBankLeads()
    {
        $this->filters[] = function (ICustomerQuery $query) {
            return $query->withoutBankLeads();
        };

        return $this;
    }

    /**
     * @param DateTimeInterface $date
     * @return $this
     */
    public function withCompanyIncorporationOn(DateTimeInterface $date)
    {
        $this->queryModifiers[] = function (ICustomerQuery $query) use ($date) {
            return $query->withCompanyIncorporationOn($date);
        };

        return $this;
    }

    /**
     * @param int $emailId
     * @param DateTimeInterface $notReceivedAfter
     * @return $this
     */
    public function didNotReceiveEmailRecently($emailId, DateTimeInterface $notReceivedAfter)
    {
        $this->filters[] = function (ICustomerQuery $query) use ($emailId, $notReceivedAfter) {
            return $query->didNotReceiveEmailRecently($emailId, $notReceivedAfter);
        };

        return $this;
    }

    /**
     * @param int $emailId
     * @param DateTimeInterface $start
     * @param DateTimeInterface $end
     * @return $this
     */
    public function receivedEmailBetween($emailId, DateTimeInterface $start, DateTimeInterface $end)
    {
        $this->filters[] = function (ICustomerQuery $query) use ($emailId, $start, $end) {
            return $query->receivedEmailBetween($emailId, $start, $end);
        };

        return $this;
    }

    /**
     * @param int $countryId
     * @return $this
     */
    public function byCustomerCountry($countryId)
    {
        $this->queryModifiers[] = function (ICustomerQuery $query) use ($countryId) {
            return $query->byCustomerCountry($countryId);
        };

        return $this;
    }

    /**
     * @return Collection|Customer[]
     */
    public function iterate()
    {
        if (!$this->queryModifiers && $this->filters) {
            $this->queryModifiers[] = array_pop($this->filters);
        }

        $query = new OriginalCustomerQuery;
        foreach ($this->queryModifiers as $queryModifier) {
            $query = $queryModifier($query);
        }

        $customers = Collection::from($query->iterate($this->repository))->extract(0);

        foreach ($this->filters as $filter) {
            $customers = $customers->filter(
                function(Customer $customer) use ($filter) {
                    $query = (new OriginalCustomerQuery)->withId($customer->getId());
                    return $filter($query)->exists($this->repository);
                }
            );
        }

        return $customers;
    }
}
