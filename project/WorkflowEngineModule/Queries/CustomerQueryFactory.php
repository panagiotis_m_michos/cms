<?php

namespace WorkflowEngineModule\Queries;


use Kdyby\Persistence\Queryable;

class CustomerQueryFactory
{
    /**
     * @var Queryable
     */
    private $repository;

    /**
     * @param Queryable $repository
     */
    public function __construct(Queryable $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return CustomerQuery
     */
    public function create()
    {
        return new CustomerQuery($this->repository);
    }
}
