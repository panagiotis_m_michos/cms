@marketing_campaign:barclays_sweeper
@workflow_engine
Feature:
    Whenever a customer opts out of the business banking account
    I want to send him a email marketing campaign
    So that we can influence him to change his mind and open an business bank account

    Scenario: [Campaign: Barclays Sweeper] Send first campaign email
        Given I am an existing retail customer
        And I am from United Kingdom
        And I bought the Core Company Package Initial product
        And I am not a business bank lead
        And email 4013 was not sent to me in the last 5 days
        When 1 day passed since the company incorporation
        Then send email 4013 to me

    Scenario: [Campaign: Barclays Sweeper] Send second campaign email
        Given I am an existing retail customer
        And I am from United Kingdom
        And I am not a business bank lead
        And 15 day passed since the company incorporation
        And email 4013 was not sent to me in the last 13 days
        When 14 days passed since email 4013 was sent to me
        Then send email 4013 to me

    @test
    Scenario: [Campaign: Barclays Sweeper] Test
        Given I am an existing retail customer
        And I am not a business bank lead
        And I am 'dusank@madesimplegroup.com'
        Then send email 4013 to me
