<?php

namespace WorkflowEngineModule\LogHandlers;

use Monolog\Handler\AbstractProcessingHandler;

class UnformattedMessageOutputHandler extends AbstractProcessingHandler
{
    /**
     * Writes the record down to the log of the implementing handler
     *
     * @param  array $record
     * @return void
     */
    protected function write(array $record)
    {
        echo $record['channel'] . '.' . $record['level_name'] . ':' . $record['message'] . PHP_EOL;
    }
}
