<?php

namespace WorkflowEngineModule\Contexts;

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Behat\Hook\Scope\AfterScenarioScope;
use Behat\Behat\Hook\Scope\AfterStepScope;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use BehatModule\DryRunDecider;
use BootstrapModule\Ext\DoctrineExt;
use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManager;
use Entities\Customer;
use Exception;
use InfusionsoftModule\EmailPlaceholderReplacer;
use InfusionsoftModule\Infusionsoft;
use InvalidArgumentException;
use DusanKasan\Knapsack\Collection;
use MailgunModule\Services\MailgunApiEmailService;
use MailgunModule\Services\MailgunService;
use Monolog\Logger;
use Registry;
use Symfony\Component\Filesystem\LockHandler;
use ValueObject\Country;
use WorkflowEngineModule\Queries\CustomerQuery;
use WorkflowEngineModule\Queries\CustomerQueryFactory;

class MarketingCampaignContext implements Context, SnippetAcceptingContext
{
    /**
     * @var Collection
     */
    private $customers;

    /**
     * @var MailgunApiEmailService
     */
    private $mailgunEmailService;

    /**
     * @var Infusionsoft
     */
    private $infusionsoft;

    /**
     * @var EmailPlaceholderReplacer
     */
    private $emailPlaceholderReplacer;

    /**
     * @var Exception[]
     */
    private $exceptions;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var LockHandler
     */
    private static $lock;

    /**
     * @var string
     */
    private $scenarioName;

    /**
     * @var string
     */
    private $campaignId;


    /**
     * @var bool
     */
    private $dryRun = FALSE;

    /**
     * @var MailgunService
     */
    private $mailgunService;

    /**
     * @var CustomerQuery
     */
    private $customerQuery;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var CustomerQueryFactory
     */
    private $customerQueryFactory;

    public function __construct()
    {
        $this->em = Registry::$container->get(DoctrineExt::ENTITY_MANAGER);
        $this->customerQueryFactory = Registry::$container->get('workflow_engine_module.queries.customer_query_factory');
        $this->mailgunEmailService = Registry::$container->get('workflow_engine_module.email_service');
        $this->mailgunService = Registry::$container->get('mailgun_module.services.mailgun_service');
        $this->infusionsoft = Registry::$container->get('infusionsoft_module.infusionsoft');
        $this->emailPlaceholderReplacer = Registry::$container->get('infusionsoft_module.email_placeholder_replacer');
        $this->logger = Registry::$container->get('workflow_engine_module.loggers.workflow_engine_logger');
        $this->dryRun = DryRunDecider::$dryRun;
    }

    /**
     * @BeforeSuite
     */
    public static function lock()
    {
        self::$lock = new LockHandler('workflow_engine');
        if (!self::$lock->lock()) {
            Registry::$container
                ->get('workflow_engine_module.loggers.workflow_engine_logger')
                ->error('Can not instantiate workflow engine twice. Wait until the old instance finishes.');

            throw new Exception('Can not instantiate workflow engine twice. Wait until the old instance finishes.');
        }
    }

    /**
     * @AfterSuite
     */
    public static function releaseLock()
    {
        self::$lock->release();
    }

    /**
     * @BeforeScenario
     */
    public function prepare(BeforeScenarioScope $scope)
    {
        $this->logger->info('Syncing Mailgun events, please wait.');

        $this->customers = NULL;
        $this->exceptions = [];
        $this->scenarioName = $scope->getScenario()->getTitle();
        $this->campaignId = Collection::from($scope->getFeature()->getTags())
            ->filter(function ($tag) {
                return strpos($tag, 'marketing_campaign:') === 0;
            })
            ->map(function ($tag) {
                return str_replace('marketing_campaign:', '', $tag);
            })
            ->append('')
            ->first();

        if (!empty($this->campaignId)) {
            $this->mailgunService->assureCampaign($this->campaignId);
        }

        $this->customerQuery = $this->customerQueryFactory->create();

        Registry::$container
            ->get('mailgun_module.commands.sync_latest_mailgun_events_command')
            ->execute();
    }

    /**
     * @AfterScenario
     */
    public function afterScenario(AfterScenarioScope $scope)
    {
        $title = $scope->getScenario()->getTitle();

        if ($scope->getTestResult()->isPassed()) {
            $this->logger->info("Scenario completed: $title");
        } else {
            $this->logger->error("Scenario failed: $title", $this->exceptions);
        }

    }

    /**
     * @AfterStep
     */
    public function after(AfterStepScope $scope)
    {
        $exception = $scope->getTestResult()->getException();

        if ($exception) {
            $this->exceptions[] = $exception;
        }
    }

    /**
     * @Transform /^(.*) product$/
     */
    public function productNameToProductId($productName)
    {
        $productMatrix = [
            'LLP Package' => 436,
            'Limited by Guarantee Package' => 1175,
            'Limited by Guarantee Plus Package' => 379,
            'Reserve a Company Name Initial' => 400,
            'Reserve a Company Name Renewal' => 401,
            'Reserve a Company Name Transfer' => 403,
            'Sole Trader Package' => 921,
            'Sole Trader Plus Package' => 955,
            'International Classic Package Initial' => 1430,
            'International Banking Package Initial' => 1598,
            'International Package Renewal' => 1493,
            'Basic Package Initial' => 1313,
            'Printed Package Initial' => 1314,
            'Privacy Package Initial' => 1315,
            'Comprehensive Package Initial' => 1316,
            'Ultimate Package Initial' => 1317,
            'Privacy Package Renewal' => 1353,
            'Comprehensive Package Renewal' => 342,
            'Ultimate Package Renewal' => 342,
            'Anguilla Offshore Formation Service Initial' => 405,
            'Anguilla Offshore Formation With Bank Assistance Service' => 409,
            'Anguilla Offshore Formation Service Renewal' => 732,
            'Belize Offshore Formation Service Initial' => 406,
            'Belize Offshore Formation With Bank Assistance Service' => 410,
            'Belize Offshore Formation Service Renewal' => 733,
            'Seychelles Offshore Formation Service Initial' => 407,
            'Seychelles Offshore Formation With Bank Assistance Service' => 411,
            'Seychelles Offshore Formation Service Renewal' => 734,
            'Offshore Bank Assistance' => 413,
            'Annual Return Express Service' => 1609,
            'Annual Return Service' => 340,
            'Annual Return DIY Product' => 666,
            'Nominee Shareholder Service Initial' => 172,
            'Nominee Secretary Service Initial' => 171,
            'Nominee Shareholder Service Renewal' => 339,
            'Nominee Secretary Service Renewal' => 337,
            'Registered Office Service Initial' => 165,
            'Registered Office Service Renewal' => 334,
            'Service Address Service Initial' => 475,
            'Service Address Service Renewal' => 806,
            'Apostilled Documents Normal Product' => 115,
            'Apostilled Documents Express Product' => 287,
            'Certificate of Good Standing Normal Product' => 292,
            'Certificate of Good Standing Express Product' => 293,
            'Certificate of Good Standing Apostille Normal Product' => 1654,
            'Certificate of Good Standing Apostille Express Product' => 1655,
            'Additional Bound Memorandum and Articles of Association Product' => 317,
            'Duplicate Certificate Of Incorporation' => 318,
            'Company Formation Documents' => 319,
            'Company Name Change Service' => 843,
            'Company Name Change Same Day Service' => 844,
            'Company Dissolution Service' => 309,
            'VAT Registration Assistance' => 299,
            'PAYE Registration Assistance' => 298,
            'Dormant Company Accounts Service' => 306,
            'Dormant Company Accounts Express Service' => 882,
            'Transfer of Shares Service' => 315,
            'Issue of Shares Service' => 313,
            'Share Certificate Service' => 314,
            'Confirmation of Shareholding at Incorporation' => 311,
            'Company Register' => 301,
            'Company Seal' => 302,
            'Company Stamp' => 331,
            'All-In-One Pack' => 567,
            'Credit Product' => 609,
            'Core Company Package Initial' => [
                1313,
                1314,
                1315,
                1316,
                1317
            ],
            'Core Company Package Renewal' => [
                1353,
                342
            ],
            'Offshore Formation Service' => [
                405,
                409,
                732,
                406,
                410,
                733,
                407,
                411,
                734
            ]

        ];

        if (array_key_exists($productName, $productMatrix)) {
            return $productMatrix[$productName];
        } else {
            throw new InvalidArgumentException('Product name not recognized: ' . $productName);
        }
    }

    /**
     * @Transform /^(.*) customer/
     */
    public function customerRoleToId($customerRole)
    {
        if ($customerRole == 'retail') {
            return Customer::ROLE_NORMAL;
        } elseif ($customerRole == 'wholesale') {
            return Customer::ROLE_WHOLESALE;
        } else {
            throw new InvalidArgumentException("Customer role '$customerRole' not recognized.");
        }
    }

    /**
     * @Transform /^from (.*)/
     */
    public function countryToCountryId($country)
    {
        $countries = array_flip(Country::$countriesById);

        if (array_key_exists($country, $countries)) {
            return $countries[$country];
        } else {
            throw new InvalidArgumentException("Country '$country' not recognized.");
        }
    }

    /**
     * @Given I am :email
     */
    public function iAm($email)
    {
        $this->customerQuery->withEmail($email);
    }

    /**
     * Selects customers of specified role (normal, wholesale)
     *
     * @Given /^I am an existing (.* customer)$/
     */
    public function iAmAnExistingCustomer($customerRole)
    {
        $this->customerQuery->byRole($customerRole);
    }

    /**
     * Selects customers who bought the product specified by product name.
     *
     * @Given /^I bought the (.* product)$/
     */
    public function iBoughtTheProduct($productId)
    {
        if (!is_array($productId)) {
            $productId = [$productId];
        }

        $this->customerQuery->withSomeProducts($productId);
    }

    /**
     * Selects customers who are from specified country.
     *
     * @Given /^I am (from .*)$/
     */
    public function iAmFrom($countryId)
    {
        $this->customerQuery->byCustomerCountry($countryId);
    }

    /**
     * Selects customers who are not bank leads.
     *
     * @Given I am not a business bank lead
     */
    public function iAmNotABusinessBankLead()
    {
        $this->customerQuery->withoutBankLeads();
    }

    /**
     * Selects customers who did not received email (specified by its ID) within last X days.
     *
     * @Given /^email (\d+) was not sent to me in the last (\d+) days$/
     */
    public function emailWasNotSentToMe($emailId, $days)
    {
        $this->customerQuery->didNotReceiveEmailRecently($emailId, new DateTime("-$days days 23:59:59"));
    }

    /**
     * Selects customers with companies that were incorporated exactly X days before today.
     *
     * @When /^([0-9]+) days? passed since the company incorporation/
     */
    public function passedSinceTheCompanyIncorporation($numberOfDays)
    {
        $date = (new DateTime())->sub(new DateInterval("P{$numberOfDays}D"));

        return $this->customerQuery->withCompanyIncorporationOn($date);
    }


    /**
     * Selects customers who received email (specified by its ID) exactly X days before today.
     *
     * @When :days days passed since email :emailId was sent to me
     */
    public function daysPassedSinceEmailWasSentToMe($days, $emailId)
    {
        $start = new DateTime("- $days days 00:00:00");
        $end = new DateTime("- $days days 23:59:59");

        $this->customerQuery->receivedEmailBetween($emailId, $start, $end);
    }

    /**
     * Sends email (specified by its  ID) to customers selected beforehand.
     *
     * @Then /^send email (\d+) to me$/
     */
    public function sendEmailToMe($emailId)
    {
        $templateEmail = $this->infusionsoft->getEmailTemplateById($emailId);

        if ($this->dryRun) {
            $this->logger->info('Dry run! Nothing will be sent!');
            echo "Dry run! Nothing will be sent!\n";
        }

        gc_enable();
        $emailCount = 0;
        foreach ($this->customerQuery->iterate() as $customer) {
            $replacedEmail = $this->emailPlaceholderReplacer->replacePlaceholders(clone $templateEmail, $customer);

            $replacedEmail->setAdditionalData(
                [
                    'customerId' => $customer->getId(),
                    'emailTemplateId' => $emailId,
                    'campaign' => $this->campaignId,
                    'scenario' => $this->scenarioName,
                    'site' => 'companiesmadesimple.com'
                ]
            );

            $this->logger->info("Sending email {$emailId} to customer with ID {$customer->getId()}.");

            if (!$this->dryRun) {
                $this->mailgunEmailService->send(
                    $replacedEmail,
                    $customer
                );
            }

            $emailCount++;
            $this->em->flush();
            $this->em->clear();
            gc_collect_cycles();
        }

        $this->logger->info("Emails sent: $emailCount");
        echo "Emails sent: $emailCount";
    }
}
