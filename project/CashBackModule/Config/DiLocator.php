<?php

namespace CashBackModule\Config;

class DiLocator
{
    const LOGGER_CASH_BACK_IMPORTS = 'cash_back_module.loggers.cash_back_imports';
    const LISTENER_CREATE_BARCLAYS_CASH_BACK = 'cash_back_module.listeners.create_barclays_cash_back_listener';
    const LISTENER_CREATE_TSB_CASH_BACK = 'cash_back_module.listeners.create_tsb_cash_back_listener';
    const RESOLVER_CASH_BACK_DEFAULTS = 'cash_back_module.resolvers.cash_back_defaults_resolver';
}
