<?php

namespace CashBackModule\Resolvers;

use Entities\Cashback;
use Entities\Company;
use Entities\Customer;
use Services\NodeService;
use WholesalePackage;

class CashBackDefaultsResolver
{
    /**
     * @var NodeService
     */
    private $nodeService;

    /**
     * @param NodeService $nodeService
     */
    public function __construct(NodeService $nodeService)
    {
        $this->nodeService = $nodeService;
    }

    /**
     * @param Customer $customer
     * @return string
     */
    public function getStatusId(Customer $customer)
    {
        $isEligible = $customer->hasBankDetails() || $customer->getCashbackType() == Cashback::CASHBACK_TYPE_CREDITS;

        return $isEligible ? Cashback::STATUS_ELIGIBLE : Cashback::STATUS_IMPORTED;
    }

    /**
     * @param Company $company
     * @return string
     */
    public function getPackageTypeId(Company $company)
    {
        $package = $this->nodeService->getProductById($company->getProductId());

        return $package instanceof WholesalePackage ? Cashback::PACKAGE_TYPE_WHOLESALE : Cashback::PACKAGE_TYPE_RETAIL;
    }
}
