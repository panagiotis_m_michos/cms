<?php

namespace CashBackModule\Exceptions;

use Exceptions\Technical\TechnicalAbstract;

class CashBackAlreadyExistsException extends TechnicalAbstract
{
    /**
     * @param string $companyNumber
     * @param string $bankTypeId
     */
    public function __construct($companyNumber, $bankTypeId)
    {
        parent::__construct("{$bankTypeId} cash back for company {$companyNumber} already exists.");
    }
}
