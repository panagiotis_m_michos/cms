<?php

namespace CashBackModule\Listeners;

use BankingModule\Config\EventLocator;
use BankingModule\Events\BarclaysAccountImportedEvent;
use CashBackModule\Factories\CashBackFactory;
use CashBackModule\Exceptions\CashBackAlreadyExistsException;
use ILogger;
use Psr\Log\LoggerInterface;
use Services\CashbackService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CreateBarclaysCashBackListener implements EventSubscriberInterface
{
    /**
     * @var CashBackFactory
     */
    private $cashBackFactory;

    /**
     * @var CashbackService
     */
    private $cashBackService;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param CashBackFactory $cashBackFactory
     * @param CashbackService $cashBackService
     * @param LoggerInterface $logger
     */
    public function __construct(
        CashBackFactory $cashBackFactory,
        CashbackService $cashBackService,
        LoggerInterface $logger
    )
    {
        $this->cashBackFactory = $cashBackFactory;
        $this->cashBackService = $cashBackService;
        $this->logger = $logger;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            EventLocator::BARCLAYS_ACCOUNT_IMPORTED => 'onBarclaysAccountImported',
        ];
    }

    /**
     * @param BarclaysAccountImportedEvent $event
     */
    public function onBarclaysAccountImported(BarclaysAccountImportedEvent $event)
    {
        $company = $event->getCompany();
        if (!$this->cashBackService->getCashBackAmountForCompany($company)) {
            $this->logger->info("Company number {$company->getCompanyNumber()} has no cash back amount, cash back not created");

            return;
        }

        try {
            $cashBack = $this->cashBackFactory->createFromAccountImportedEvent($event);
        } catch (CashBackAlreadyExistsException $e) {
            $this->logger->info($e->getMessage());

            return;
        }

        if (!$this->cashBackService->isImportableForBarclays($cashBack)) {
            $this->logger->info(
                "Barclays cash back for company number {$cashBack->getCompany()->getCompanyNumber()} is older than 3 months"
            );

            return;
        }

        $this->cashBackService->saveCashback($cashBack);
        $this->logger->info(
            "Barclays cash back for company number {$cashBack->getCompany()->getCompanyNumber()} created"
        );
    }
}
