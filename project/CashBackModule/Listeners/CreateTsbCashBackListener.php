<?php

namespace CashBackModule\Listeners;

use BankingModule\Config\EventLocator;
use BankingModule\Events\TsbAccountImportedEvent;
use CashBackModule\Factories\CashBackFactory;
use CashBackModule\Exceptions\CashBackAlreadyExistsException;
use ILogger;
use Psr\Log\LoggerInterface;
use Services\CashbackService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CreateTsbCashBackListener implements EventSubscriberInterface
{
    /**
     * @var CashBackFactory
     */
    private $cashBackFactory;

    /**
     * @var CashbackService
     */
    private $cashBackService;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param CashBackFactory $cashBackFactory
     * @param CashbackService $cashBackService
     * @param LoggerInterface $logger
     */
    public function __construct(
        CashBackFactory $cashBackFactory,
        CashbackService $cashBackService,
        LoggerInterface $logger
    )
    {
        $this->cashBackFactory = $cashBackFactory;
        $this->cashBackService = $cashBackService;
        $this->logger = $logger;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            EventLocator::TSB_ACCOUNT_IMPORTED => 'onTsbAccountImported',
        ];
    }

    /**
     * @param TsbAccountImportedEvent $event
     */
    public function onTsbAccountImported(TsbAccountImportedEvent $event)
    {
        $company = $event->getCompany();
        if (!$this->cashBackService->getCashBackAmountForCompany($company)) {
            $this->logger->info("Company number {$company->getCompanyNumber()} has no cash back amount, cash back not created");

            return;
        }

        try {
            $cashBack = $this->cashBackFactory->createFromAccountImportedEvent($event);
        } catch (CashBackAlreadyExistsException $e) {
            $this->logger->info($e->getMessage());

            return;
        }

        $this->cashBackService->saveCashback($cashBack);
        $this->logger->info(
            "TSB cash back for company number {$cashBack->getCompany()->getCompanyNumber()} created"
        );
    }
}
