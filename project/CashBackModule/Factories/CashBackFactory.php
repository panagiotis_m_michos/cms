<?php

namespace CashBackModule\Factories;

use BankingModule\Events\IAccountImportedEvent;
use CashBackModule\Exceptions\CashBackAlreadyExistsException;
use CashBackModule\Resolvers\CashBackDefaultsResolver;
use DateTime;
use Entities\Cashback;
use Entities\Company;
use Services\CashbackService;
use Services\CompanyService;

class CashBackFactory
{
    /**
     * @var CashbackService
     */
    private $cashBackService;

    /**
     * @var CashBackDefaultsResolver
     */
    private $defaultsResolver;

    /**
     * @param CashbackService $cashBackService
     * @param CashBackDefaultsResolver $defaultsResolver
     */
    public function __construct(
        CashbackService $cashBackService,
        CashBackDefaultsResolver $defaultsResolver
    )
    {
        $this->cashBackService = $cashBackService;
        $this->defaultsResolver = $defaultsResolver;
    }

    /**
     * @param IAccountImportedEvent $event
     * @return Cashback
     * @throws CashBackAlreadyExistsException
     */
    public function createFromAccountImportedEvent(IAccountImportedEvent $event)
    {
        $company = $event->getCompany();
        if ($this->cashBackService->getBankCashBackByCompany($company, $event->getBankType())) {
            throw new CashBackAlreadyExistsException($company->getCompanyNumber(), $event->getBankType());
        }

        $cashBack = new Cashback(
            $company,
            $event->getBankType(),
            $this->cashBackService->getCashBackAmountForCompany($company),
            $event->getDateLeadSent(),
            $event->getDateAccountOpened()
        );

        $cashBack->setStatusId($this->defaultsResolver->getStatusId($cashBack->getCompany()->getCustomer()));
        $cashBack->setPackageTypeId($this->defaultsResolver->getPackageTypeId($cashBack->getCompany()));

        return $cashBack;
    }

    /**
     * @param Company $company
     * @param DateTime|null $dateLeadSent
     * @param DateTime|null $dateAccountOpened
     * @return Cashback
     */
    public function createBarclays(Company $company, DateTime $dateLeadSent = NULL, DateTime $dateAccountOpened = NULL)
    {
        $cashBack = new Cashback(
            $company,
            Cashback::BANK_TYPE_BARCLAYS,
            $this->cashBackService->getCashBackAmountForCompany($company),
            $dateLeadSent,
            $dateAccountOpened
        );

        $cashBack->setStatusId($this->defaultsResolver->getStatusId($company->getCustomer()));
        $cashBack->setPackageTypeId($this->defaultsResolver->getPackageTypeId($company));

        return $cashBack;
    }
}
