<?php

namespace CommonModule;

use RouterModule\Generators\UrlGenerator as RouterUrlGenerator;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Utils\Text\Parser\Filters\Interfaces\IRouter;

class UrlGenerator implements IRouter
{
    /**
     * @var RouterUrlGenerator
     */
    private $urlGenerator;

    /**
     * @param RouterUrlGenerator $urlGenerator
     */
    public function __construct(RouterUrlGenerator $urlGenerator)
    {
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * @param string $code
     * @param array $params
     * @return string
     */
    public function link($code = NULL, $params = NULL)
    {
        $params = (array) $params;

        try {
            return $this->urlGenerator->url($code, $params);
        } catch (RouteNotFoundException $e) {
            return $this->urlGenerator->url($code, $params, RouterUrlGenerator::RELATIVE | RouterUrlGenerator::OLD_LINK);
        }
    }

    /**
     * @param string $code
     * @param array $params
     * @return string
     */
    public function absoluteLink($code = NULL, $params = NULL)
    {
        $params = (array) $params;

        try {
            return $this->urlGenerator->url($code, $params, RouterUrlGenerator::ABSOLUTE);
        } catch (RouteNotFoundException $e) {
            return $this->urlGenerator->url($code, $params, RouterUrlGenerator::ABSOLUTE | RouterUrlGenerator::OLD_LINK);
        }
    }
}
