<?php

namespace CommonModule\Contracts;

use Package;
use Symfony\Component\HttpFoundation\RedirectResponse;

interface IRedirectionProvider
{
    /**
     * @param Package $package
     * @return RedirectResponse
     */
    public function getRedirection(Package $package = NULL);
}