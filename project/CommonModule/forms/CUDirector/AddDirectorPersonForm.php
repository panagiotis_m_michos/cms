<?php

namespace Form\Both\CUDirector;

use Address;
use Admin\CHForm;
use Company;
use Entities\Company as CompanyEntity;
use FControler;
use FForm;
use Person;
use Prefiller;
use Utils\Date;

class AddDirectorPersonForm extends CHForm
{

    /**
     * @var FControler
     */
    public $controller;

    /**
     * @var Company
     */
    public $company;

    /**
     * @var CompanyEntity
     */
    public $companyEntity;

    /**
     * @var Prefiller
     */
    public $prefiller;

    /**
     * @var array
     */
    public $callback;

    /**
     * @param FControler $controller
     * @param array $callback
     * @param Company $company
     * @param CompanyEntity $companyEntity
     * @param Prefiller $prefiller
     */
    public function startup(FControler $controller, array $callback, Company $company, CompanyEntity $companyEntity, Prefiller $prefiller)
    {
        $this->controller = $controller;
        $this->company = $company;
        $this->companyEntity = $companyEntity;
        $this->prefiller = $prefiller;
        $this->callback = $callback;
        $this->init();
    }

    private function init()
    {
        $prefillAddress = $this->prefiller->getPrefillAdresses();
        $prefillOfficers = $this->prefiller->getPrefillOfficers();

        // prefill
        $this->addFieldset('Prefill');
        $this->addSelect('prefillOfficers', 'Prefill Officers', $prefillOfficers['select'])
            ->setFirstOption('--- Select --');

        $incorporationDate = $this->companyEntity->getIncorporationDate();
        // apointment date
        $this->addFieldset('Appointment Date');
        $datePicker = $this->add('DatePickerNew', 'appointment_date', 'Date of Appointment *')
            ->addRule(FForm::Required, 'Please select New Date')
            ->addRule('DateFormat', 'Please enter date format as DD-MM-YYYY', 'd-m-Y')
            ->addRule('DateMin', 'Appointment date cannot be before the company\'s incorporation date %s', array($incorporationDate, 'd-m-Y'))
            ->addRule('DateMax', 'Date cannot be in the future', array(new Date(), 'd-m-Y'))
            ->setDescription('Date cannot be in the future');
        $datePicker->class('date')->setValue(date('d-m-Y'));

        // person
        $this->addFieldset('Person');
        $this->addSelect('title', 'Title', Person::$titles)
            ->setFirstOption('--- Select ---');
        $this->addText('forename', 'First name *')
            ->addRule(FForm::Required, 'Please provide first name')
            ->addRule(FForm::MAX_LENGTH, "First name can't be more than 50 characters", 50);
        $this->addText('middle_name', 'Middle name')
            ->addRule(FForm::MAX_LENGTH, "Middle name can't be more than 50 characters", 50);
        $this->addText('surname', 'Last name *')
            ->addRule(FForm::Required, 'Please provide last name')
            ->addRule(FForm::MAX_LENGTH, "Last name can't be more than 160 characters", 160);

        $this->add('DateSelect', 'dob', 'Date Of Birth *')
            ->addRule(FForm::Required, 'Please provide dob')
            ->addRule(
                ['CHValidator', 'Validator_personDOB'],
                ['DOB is not a valid date.', 'Director has to be older than %d years.'],
                16
            )
            ->setStartYear(1900)
            ->setEndYear(date('Y') - 1)
            ->setValue(date('Y-m-d', strtotime(date("Y-m-d", strtotime(date("Y-m-d"))) . " -15 year")));


        if ($this->company->getType() != 'LLP') {
            $this->addText('nationality', 'Nationality *')
                ->addRule(FForm::Required, 'Please provide nationality')
            //->addRule(FForm::REGEXP, "Only letters are allowed", '#^[A-Z]+$#i')
            ;
            $this->addText('occupation', 'Occupation *')
                ->addRule(FForm::Required, 'Please provide occupation');
            //->setDescription('This is your current business occupation. If you do not have one, please enter Company Director');
        }
        $this->addText('country_of_residence', 'Country Of Residence *')
            ->addRule(FForm::Required, 'Please provide Country of Residence')
        //->addRule(FForm::REGEXP, "Only letters are allowed", '#^[A-Z]+$#i')
        ;

        // prefill
        $this->addFieldset('Prefill');
        $this->addSelect('prefillAddress', 'Prefill Address', $prefillAddress['select'])
            ->setFirstOption('--- Select ---');

        // adddress
        $this->addFieldset('Address');
        $this->addText('premise', 'Building name/number *')
            ->addRule(FForm::Required, 'Please provide Building name/number')
            ->addRule(FForm::MAX_LENGTH, "Building name/number can't be more than 50 characters", 50);
        $this->addText('street', 'Street *')
            ->addRule(FForm::Required, 'Please provide Street')
            ->addRule(FForm::MAX_LENGTH, "Street can't be more than 50 characters", 50);
        $this->addText('thoroughfare', 'Address 3')
            ->addRule(FForm::MAX_LENGTH, "Address 3 can't be more than 50 characters", 50);
        $this->addText('post_town', 'Town *')
            ->addRule(FForm::Required, 'Please provide Town')
            ->addRule(FForm::MAX_LENGTH, "Town can't be more than 50 characters", 50);
        $this->addText('county', 'County')
            ->addRule(FForm::MAX_LENGTH, "County can't be more than 50 characters", 50);
        $this->addText('postcode', 'Postcode *')
            ->addRule(array('CHValidator', 'Validator_serviceAddress'), 'You cannot use our postcode for this address', $this->company)
            ->addRule(FForm::Required, 'Please provide Postcode')
            ->addRule(FForm::MAX_LENGTH, "Postcode can't be more than 15 characters", 15);
        $this->addSelect('country', 'Country *', Address::$countries)
            ->addRule(FForm::Required, 'Please provide Country');

        // residential address
        $this->addFieldset('Residential Address');

        $this->addCheckbox('residentialAddress', 'Different address: ', 1);

        $this->addText('residential_premise', 'Building name/number *')
            ->addRule(array('CHValidator', 'Validator_requiredResidentialAddress'), 'Please provide Building name/number', 'add')
            ->addRule(FForm::MAX_LENGTH, "Building name/number can't be more than 50 characters", 50);
        $this->addText('residential_street', 'Street *')
            ->addRule(array('CHValidator', 'Validator_requiredResidentialAddress'), 'Please provide Street', 'add')
            ->addRule(FForm::MAX_LENGTH, "Street can't be more than 50 characters", 50);
        $this->addText('residential_thoroughfare', 'Address 3')
            ->addRule(FForm::MAX_LENGTH, "Address 3 can't be more than 50 characters", 50);
        $this->addText('residential_post_town', 'Town *')
            ->addRule(array('CHValidator', 'Validator_requiredResidentialAddress'), 'Please provide Town', 'add')
            ->addRule(FForm::MAX_LENGTH, "Town can't be more than 50 characters", 50);
        $this->addText('residential_county', 'County')
            ->addRule(FForm::MAX_LENGTH, "County can't be more than 50 characters", 50);
        $this->addText('residential_postcode', 'Postcode *')
            ->addRule(array('CHValidator', 'Validator_requiredResidentialAddress'), 'Please provide Postcode', 'add')
            ->addRule(FForm::MAX_LENGTH, "Postcode can't be more than 15 characters", 15)
            ->addRule(FForm::REGEXP, 'You cannot use our postcode for this address', array('#(ec1v 4pw|ec1v4pw)#i', TRUE));
        $this->addSelect('residential_country', 'Country *', Address::$countries)
            ->addRule(array('CHValidator', 'Validator_requiredResidentialAddress'), 'Please provide Country', 'add')
            ->addRule(FForm::MAX_LENGTH, "Country can't be more than 50 characters", 50);

        $this->addFieldset('Section 243 Exemption');
        $this->addCheckbox('residentialSecureAddressInd', '243 Exemption: ', 1)
            ->setDescription('Only tick the box below if you are in the process of applying for, or have been granted, exemption by the Registrar from disclosing you usual residential address to credit reference agencies under section 243 of the Companies Act 2006.');


        if ($this->company->getType() == 'LLP') {
            $this->addFieldset('Designation');

            $this->addRadio('designated_ind', 'Designated member *', array(0 => 'No', 1 => 'Yes'))
                ->addRule(FForm::Required, 'Please provide DesignatedInd');
        }

        // security
        $this->addFieldset('Consent to act');
        if ($this->company->getType() == 'LLP') {
            $consentLabel = 'The Limited Liability Partnership (LLP) confirms that the person named has consented to act as a designated member';
        } else {
            $consentLabel = 'The company confirms that the person named has consented to act as a director';
        }

        $this->add('SingleCheckbox', 'consentToAct', $consentLabel)
            ->addRule(FForm::Required, 'Consent to act is required')
            ->{'data-label-designated_ind-0'}('The Limited Liability Partnership (LLP) confirms that the person named has consented to act as a non-designated member')
            ->{'data-label-designated_ind-1'}('The Limited Liability Partnership (LLP) confirms that the person named has consented to act as a designated member');

        $this->addFieldset('Action');
        // diff callback function
        $this->addSubmit('continue', 'Continue >')->class('btn_submit fright mbottom');

        $this->onValid = $this->callback;
        $this->start();
    }

    public function process()
    {
        $data = $this->getValues();
        $data['residential_secure_address_ind'] = $data['residentialSecureAddressInd'];

        // get director
        $director = $this->company->getNewPerson('dir');

        // set person
        $person = $director->getPerson();
        $person->setTitle($data['title']);
        $person->setForename($data['forename']);
        $person->setMiddleName($data['middle_name']);
        $person->setSurname($data['surname']);

        //don't required for llp
        if ($this->company->getType() != 'LLP') {
            $person->setNationality($data['nationality']);
            $person->setOccupation($data['occupation']);
        }

        $person->setDob($data['dob']);
        $person->setCountryOfResidence($data['country_of_residence']);
        // set appointment Date
        $person->setAppointmentDate(
            !empty($data['appointment_date']) ? Date::changeFormat($data['appointment_date'], 'd-m-Y', 'Y-m-d') : NULL
        );
        $director->setPerson($person);

        // set address
        $address = $director->getAddress();
        $address->setFields($data);
        $director->setAddress($address);

        if ($this->company->getType() == 'LLP') {
            $director->setDesignatedInd($data['designated_ind']);
        }

        // set residential address
        if ($data['residentialAddress'] == 1) {
            $address = $director->getResidentialAddress();
            $address->setFields($data, 'residential_');
            $director->setResidentialAddress($address);
        } else {
            $address->setSecureAddressInd($data['residential_secure_address_ind']);
            $director->setResidentialAddress($address);
        }

        $director->setConsentToAct($data['consentToAct']);

        $this->company->sendAppointmentOfNaturalDirector($director);
        $this->clean();
    }
}
