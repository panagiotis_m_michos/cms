<?php

namespace Form\Both\CUDirector;

use Admin\CHForm;
use Company;
use FControler;
use Feature;
use Person;
use Address;
use PersonDirector;
use FForm;
use Prefiller;

class EditDirectorPersonForm extends CHForm
{

    /**
     * @var FControler
     */
    public $controller;

    /**
     * @var Company
     */
    public $company;

    /**
     * @var PersonDirector
     */
    public $director;

    /**
     * @var Prefiller
     */
    public $prefiller;

    /**
     * @var array
     */
    public $callback;

    /**
     * @param FControler $controller
     * @param array $callback
     * @param Company $company
     * @param PersonDirector $director
     * @param Prefiller $prefiller
     */
    public function startup(FControler $controller, array $callback, Company $company, PersonDirector $director, Prefiller $prefiller)
    {
        $this->controller = $controller;
        $this->company = $company;
        $this->director = $director;
        $this->prefiller = $prefiller;
        $this->callback = $callback;
        $this->init();
    }

    private function init()
    {
        $prefillAddress = $this->prefiller->getPrefillAdresses();

        if ($this->company->getType() == 'LLP') {
            $formBlocks = array('changeName', 'changeAddress', 'changeResidentialAddress', 'changeOther', 'changeDesignation');
        } else {
            $formBlocks = array('changeName', 'changeAddress', 'changeResidentialAddress', 'changeOther');
        }

        // Name
        $this->addFieldset('Person');
        $this->addCheckbox('changeName', 'Change:', 1)
            ->addRule(array('CHValidator', 'Validator_changeChecboxRequired'), 'No changes have been made', $formBlocks);
        $this->addSelect('title', 'Title', Person::$titles)
            ->setFirstOption('--- Select ---');
        $this->addText('forename', 'First name *')
            ->addRule(array('CHValidator', 'Validator_directorEditChangeRequired'), 'Please provide first name', 'changeName')
            ->addRule(FForm::MAX_LENGTH, "First name can't be more than 50 characters", 50);
        $this->addText('middle_name', 'Middle name')
            ->addRule(FForm::MAX_LENGTH, "Middle name can't be more than 50 characters", 50)
            ->addRule(FForm::MIN_LENGTH, "Please provide full middle name.", 2);
        $this->addText('surname', 'Last name *')
            ->addRule(array('CHValidator', 'Validator_directorEditChangeRequired'), 'Please provide last name', 'changeName')
            ->addRule(FForm::MAX_LENGTH, "Last name can't be more than 160 characters", 160);

        // prefill
        $this->addFieldset('Prefill');
        $this->addSelect('prefillAddress', 'Prefill Address', $prefillAddress['select'])
            ->setFirstOption('--- Select ---');

        // adddress
        $this->addFieldset('Service Address');
        $this->addCheckbox('changeAddress', 'Change:', 1);
        $this->addText('premise', 'Building name/number *')
            ->addRule(array('CHValidator', 'Validator_directorEditChangeRequired'), 'Please provide Building name/number', 'changeAddress')
            ->addRule(FForm::MAX_LENGTH, "Building name/number can't be more than 50 characters", 50);
        $this->addText('street', 'Street *')
            ->addRule(array('CHValidator', 'Validator_directorEditChangeRequired'), 'Please provide Street', 'changeAddress')
            ->addRule(FForm::MAX_LENGTH, "Street can't be more than 50 characters", 50);
        $this->addText('thoroughfare', 'Address 3')
            ->addRule(FForm::MAX_LENGTH, "Address 3 can't be more than 50 characters", 50);
        $this->addText('post_town', 'Town *')
            ->addRule(array('CHValidator', 'Validator_directorEditChangeRequired'), 'Please provide Town', 'changeAddress')
            ->addRule(FForm::MAX_LENGTH, "Town can't be more than 50 characters", 50);
        $this->addText('county', 'County')
            ->addRule(FForm::MAX_LENGTH, "County can't be more than 50 characters", 50);
        $this->addText('postcode', 'Postcode *')
            ->addRule(array('CHValidator', 'Validator_directorEditChangeRequired'), 'Please provide Postcode', 'changeAddress')
            ->addRule(array('CHValidator', 'Validator_serviceAddress'), 'You cannot use our postcode for this address', $this->company)
            ->addRule(FForm::MAX_LENGTH, "Postcode can't be more than 15 characters", 15);
        $this->addSelect('country', 'Country *', Address::$countries)
            ->addRule(array('CHValidator', 'Validator_directorEditChangeRequired'), 'Please provide Country', 'changeAddress')
            ->addRule(FForm::MAX_LENGTH, "Country can't be more than 50 characters", 50);

        // residential address
        $this->addFieldset('Residential Address');
        $this->addCheckbox('changeResidentialAddress', 'Change:', 1);
        $this->addText('residential_premise', 'Building name/number *')
            ->addRule(array('CHValidator', 'Validator_directorEditChangeRequired'), 'Please provide Building name/number', 'changeResidentialAddress')
            ->addRule(FForm::MAX_LENGTH, "Building name/number can't be more than 50 characters", 50);
        $this->addText('residential_street', 'Street *')
            ->addRule(array('CHValidator', 'Validator_directorEditChangeRequired'), 'Please provide Street', 'changeResidentialAddress')
            ->addRule(FForm::MAX_LENGTH, "Street can't be more than 50 characters", 50);
        $this->addText('residential_thoroughfare', 'Address 3')
            ->addRule(FForm::MAX_LENGTH, "Address 3 can't be more than 50 characters", 50);
        $this->addText('residential_post_town', 'Town *')
            ->addRule(array('CHValidator', 'Validator_directorEditChangeRequired'), 'Please provide Town', 'changeResidentialAddress')
            ->addRule(FForm::MAX_LENGTH, "Town can't be more than 50 characters", 50);
        $this->addText('residential_county', 'County')
            ->addRule(FForm::MAX_LENGTH, "County can't be more than 50 characters", 50);
        $this->addText('residential_postcode', 'Postcode *')
            ->addRule(array('CHValidator', 'Validator_directorEditChangeRequired'), 'Please provide Postcode', 'changeResidentialAddress')
            ->addRule(FForm::MAX_LENGTH, "Postcode can't be more than 15 characters", 15)
            ->addRule(FForm::REGEXP, 'You cannot use our postcode for this address', array('#(ec1v 4pw|ec1v4pw)#i', TRUE));
        $this->addSelect('residential_country', 'Country *', Address::$countries)
            ->addRule(array('CHValidator', 'Validator_directorEditChangeRequired'), 'Please provide Postcode', 'changeResidentialAddress')
            ->addRule(FForm::MAX_LENGTH, "Country can't be more than 50 characters", 50);

        // other
        $this->addFieldset('Other');
        $this->addCheckbox('changeOther', 'Change:', 1);

        if ($this->company->getType() != 'LLP') {
            $this->addText('nationality', 'Nationality *')
                ->addRule(array('CHValidator', 'Validator_directorEditChangeRequired'), 'Please provide nationality', 'changeOther')
            //->addRule(FForm::REGEXP, "Only letters are allowed", '#^[A-Z]+$#i')
            ;
            $this->addText('occupation', 'Occupation *')
                ->addRule(array('CHValidator', 'Validator_directorEditChangeRequired'), 'Please provide occupation', 'changeOther');
        }
        $this->addText('country_of_residence', 'Country Of Residence *')
            ->addRule(array('CHValidator', 'Validator_directorEditChangeRequired'), 'Please provide Country of Residence', 'changeOther')
            ->addRule(FForm::REGEXP, "Only letters are allowed", '#^[A-Z ]+$#i');

        if ($this->company->getType() == 'LLP') {

            $this->addFieldset('Designation');
            $this->addCheckbox('changeDesignation', 'Change:', 1);

            $this->addRadio('designated_ind', 'Designated member *', array(0 => 'No', 1 => 'Yes'))
                ->addRule(array('CHValidator', 'Validator_directorLLPEditChangeRequired'), 'Please provide DesignatedInd', 'changeDesignation');

            $this->add('SingleCheckbox', 'consentToAct', 'The Limited Liability Partnership (LLP) confirms that the person named has consented to act as a designated member')
                ->addRule(array('CHValidator', 'Validator_directorEditChangeRequired'), 'Consent to act is required', 'changeDesignation')
                ->{'data-label-designated_ind-0'}('The Limited Liability Partnership (LLP) confirms that the person named has consented to act as a non-designated member')
                ->{'data-label-designated_ind-1'}('The Limited Liability Partnership (LLP) confirms that the person named has consented to act as a designated member');
        }

        // submit
        $this->addFieldset('Action');
        $this->addSubmit('continue', 'Save')->class('btn_submit fright mbottom');

        $fields = $this->director->getFields();
        $fields['residentialSecureAddressInd'] = $fields['residential_secure_address_ind'];
        $this->setInitValues($fields);

        if ($this->company->getType() == 'LLP') {
            $fields['designated_ind'] = $this->director->getDesignatedInd();
        }

        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();

            // change name
            $change = array();
            if ($data['changeName'] == 1) {
                $change[] = 'changeName';
            }
            if ($data['changeAddress'] == 1) {
                $change[] = 'changeAddress';
            }
            if ($data['changeResidentialAddress'] == 1) {
                $change[] = 'changeResidentialAddress';
            }
            if ($data['changeOther'] == 1) {
                $change[] = 'changeOther';
            }

            //just for llp
            if ($this->company->getType() == 'LLP') {
                if ($data['changeDesignation'] == 1) {
                    $change[] = 'changeDesignation';
                }
            }

            $this->company->sendChangeOfNaturalDirector($this->director, $data, $change);
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

}
