<?php

namespace Form\Both\CUDirector;

use Admin\CHForm;
use Company;
use FControler;
use CorporateDirector;
use Address;
use Feature;
use FForm;
use Prefiller;

class EditDirectorCorporateForm extends CHForm
{

    /**
     * @var FControler 
     */
    public $controller;

    /**
     * @var Company 
     */
    public $company;

    /**
     * @var CorporateDirector 
     */
    public $director;

    /**
     * @var Prefiller 
     */
    public $prefiller;

    /**
     * @var array 
     */
    public $callback;

    /**
     * @param FControler $controller
     * @param array $callback
     * @param Company $company
     * @param CorporateDirector $director
     * @param Prefiller $prefiller
     */
    public function startup(FControler $controller, array $callback, Company $company, CorporateDirector $director, Prefiller $prefiller)
    {
        $this->controller = $controller;
        $this->company = $company;
        $this->director = $director;
        $this->prefiller = $prefiller;
        $this->callback = $callback;
        $this->init();
    }

    private function init()
    {
        $prefillAddress = $this->prefiller->getPrefillAdresses();
        if ($this->company->getType() == 'LLP') {
            $formBlocks = array('changeName', 'changeAddress', 'changeEEA', 'changeDesignation');
        } else {
            $formBlocks = array('changeName', 'changeAddress', 'changeEEA');
        }

        // Name
        $this->addFieldset('Person');
        $this->addCheckbox('changeName', 'Change:', 1)
            ->addRule(array('CHValidator', 'Validator_changeChecboxRequired'), 'No changes have been made', $formBlocks);
        $this->addText('corporate_name', 'Company name*')
            ->addRule(array('CHValidator', 'Validator_directorEditChangeRequired'), 'Please provide company name', 'changeName');

        // prefill
        $this->addFieldset('Prefill');
        $this->addSelect('prefillAddress', 'Prefill Address', $prefillAddress['select'])
            ->setFirstOption('--- Select ---');

        // adddress
        $this->addFieldset('Address');
        $this->addCheckbox('changeAddress', 'Change:', 1);
        $this->addText('premise', 'Building name/number *')
            ->addRule(array('CHValidator', 'Validator_directorEditChangeRequired'), 'Please provide Building name/number', 'changeAddress')
            ->addRule(FForm::MAX_LENGTH, "Building name/number can't be more than 50 characters", 50);
        $this->addText('street', 'Street *')
            ->addRule(array('CHValidator', 'Validator_directorEditChangeRequired'), 'Please provide Street', 'changeAddress')
            ->addRule(FForm::MAX_LENGTH, "Street can't be more than 50 characters", 50);
        $this->addText('thoroughfare', 'Address 3')
            ->addRule(FForm::MAX_LENGTH, "Address 3 can't be more than 50 characters", 50);
        $this->addText('post_town', 'Town *')
            ->addRule(array('CHValidator', 'Validator_directorEditChangeRequired'), 'Please provide Town', 'changeAddress')
            ->addRule(FForm::MAX_LENGTH, "Town can't be more than 50 characters", 50);
        $this->addText('county', 'County')
            ->addRule(FForm::MAX_LENGTH, "County can't be more than 50 characters", 50);
        $this->addText('postcode', 'Postcode *')
            ->addRule(array('CHValidator', 'Validator_directorEditChangeRequired'), 'Please provide Postcode', 'changeAddress')
            ->addRule(FForm::MAX_LENGTH, "Postcode can't be more than 15 characters", 15);
        $this->addSelect('country', 'Country *', Address::$countries)
            ->addRule(array('CHValidator', 'Validator_directorEditChangeRequired'), 'Please provide Country', 'changeAddress')
            ->addRule(FForm::MAX_LENGTH, "Country can't be more than 50 characters", 50);


        // eea
        $this->addFieldset('EEA/ Non EEA');
        $this->addCheckbox('changeEEA', 'Change:', 1);
        $this->addRadio('eeaType', 'Type *', array(1 => 'EEA', 2 => 'Non EEA'))
            ->addRule(array('CHValidator', 'Validator_directorEditChangeRequired'), 'Please provide EEA type!', 'changeEEA');
        $this->addText('place_registered', 'Country Registered *')
            ->addRule(array('CHValidator', 'Validator_directorEditEEARequired'), 'Please provide Country registered!');
        $this->addText('registration_number', 'Registration number *')
            ->addRule(array('CHValidator', 'Validator_directorEditEEARequired'), 'Please provide Registration number!');
        $this->addText('law_governed', 'Governing law *')
            ->addRule(array('CHValidator', 'Validator_directorEditEEARequired'), 'Please provide Governing law!');
        $this->addText('legal_form', 'Legal From *')
            ->addRule(array('CHValidator', 'Validator_directorEditEEARequired'), 'Please provide Legal form!');

        if ($this->company->getType() == 'LLP') {

            $this->addFieldset('Designation');
            $this->addCheckbox('changeDesignation', 'Change:', 1);

            $this->addRadio('designated_ind', 'Designated member *', array(0 => 'No', 1 => 'Yes'))
                ->addRule(array('CHValidator', 'Validator_directorLLPEditChangeRequired'), 'Please provide DesignatedInd', 'changeDesignation');

            $this->add('SingleCheckbox', 'consentToAct', 'The Limited Liability Partnership (LLP) confirms that the corporate body named has consented to act as a designated member')
                ->addRule(array('CHValidator', 'Validator_directorEditChangeRequired'), 'Consent to act is required', 'changeDesignation')
                ->{'data-label-designated_ind-0'}('The Limited Liability Partnership (LLP) confirms that the corporate body named has consented to act as a non-designated member')
                ->{'data-label-designated_ind-1'}('The Limited Liability Partnership (LLP) confirms that the corporate body named has consented to act as a designated member');
        }

        // submit				
        $this->addFieldset('Action');
        $this->addSubmit('continue', 'Save')->class('btn_submit fright mbottom');

        //geting and seting form fields
        $fields = $this->director->getFields();
        unset($fields['consentToAct']);
        if ($this->company->getType() == 'LLP') {
            $fields['designated_ind'] = $this->director->getDesignatedInd();
        }

        $this->setInitValues($fields);

        $this->onValid = $this->callback;
        $this->start();
    }

    public function process()
    {
        $data = $this->getValues();

        // change name
        $change = array();
        if ($data['changeName'] == 1) {
            $change[] = 'changeName';
        }
        if ($data['changeAddress'] == 1) {
            $change[] = 'changeAddress';
        }
        if ($data['changeEEA'] == 1) {
            $change[] = 'changeEEA';
        }

        if ($this->company->getType() == 'LLP') {
            if ($data['changeDesignation'] == 1)
                $change[] = 'changeDesignation';
        }
        $this->company->sendChangeOfCorporateDirector($this->director, $data, $change);
        $this->clean();
    }

}
