<?php

namespace Form\Both\CUSecretary;

use Admin\CHForm;
use Company;
use FControler;
use Address;
use Feature;
use FForm;
use Prefiller;
use Entities\Company as CompanyEntity;
use Utils\Date;
use Identification;

class AddSecretaryCorporateForm extends CHForm
{

    /**
     * @var FControler 
     */
    public $controller;

    /**
     * @var Company 
     */
    public $company;

    /**
     * @var CompanyEntity 
     */
    public $companyEntity;

    /**
     * @var PersonSecretary 
     */
    public $secretary;

    /**
     * @var Prefiller 
     */
    public $prefiller;

    /**
     * @var array 
     */
    public $callback;

    /**
     * @param FControler $controller
     * @param array $callback
     * @param Company $company
     * @param CompanyEntity $companyEntity
     * @param Prefiller $prefiller
     */
    public function startup(FControler $controller, array $callback, Company $company, CompanyEntity $companyEntity, Prefiller $prefiller)
    {
        $this->controller = $controller;
        $this->company = $company;
        $this->companyEntity = $companyEntity;
        $this->prefiller = $prefiller;
        $this->callback = $callback;
        $this->init();
    }

    private function init()
    {
        $prefillAddress = $this->prefiller->getPrefillAdresses();
        $prefillOfficers = $this->prefiller->getPrefillOfficers(Prefiller::TYPE_CORPORATE);

        // prefill
        $this->addFieldset('Prefill');
        $this->addSelect('prefillOfficers', 'Prefill Officers', $prefillOfficers['select'])
            ->setFirstOption('--- Select --');

        $incorporationDate = $this->companyEntity->getIncorporationDate();
        // apointment date
        $this->addFieldset('Appointment Date');
        $datePicker = $this->add('DatePickerNew', 'appointment_date', 'Date of Appointment *')
            ->addRule(FForm::Required, 'Please select New Date')
            ->addRule('DateFormat', 'Please enter date format as DD-MM-YYYY', 'd-m-Y')
            ->addRule('DateMin', 'Appointment date cannot be before the company\'s incorporation date %s', array($incorporationDate, 'd-m-Y'))
            ->addRule('DateMax', 'Date cannot be in the future', array(new Date(), 'd-m-Y'))
            ->setDescription('Date cannot be in the future');
        $datePicker->class('date')->setValue(date('d-m-Y'));

        // person
        $this->addFieldset('Corporate');
        $this->addText('corporate_name', 'Company name *')
            ->addRule(FForm::Required, 'Please provide first name');
        $this->addText('forename', 'First name *')
            ->addRule(FForm::Required, 'Please provide first name')
            ->addRule(FForm::MAX_LENGTH, "First name can't be more than 50 characters", 50);
        $this->addText('surname', 'Last name *')
            ->addRule(FForm::Required, 'Please provide last name')
            ->addRule(FForm::MAX_LENGTH, "Last name can't be more than 160 characters", 160);

        // address
        $this->addFieldset('Prefill');
        $this->addSelect('prefillAddress', 'Prefill Address', $prefillAddress['select'])
            ->setFirstOption('--- Select ---');

        $this->addText('premise', 'Building name/number *')
            ->addRule(FForm::Required, 'Please provide Building name/number')
            ->addRule(FForm::MAX_LENGTH, "Building name/number can't be more than 50 characters", 50);
        $this->addText('street', 'Street *')
            ->addRule(FForm::Required, 'Please provide Street')
            ->addRule(FForm::MAX_LENGTH, "Street can't be more than 50 characters", 50);
        $this->addText('thoroughfare', 'Address 3')
            ->addRule(FForm::MAX_LENGTH, "Address 3 can't be more than 50 characters", 50);
        $this->addText('post_town', 'Town *')
            ->addRule(FForm::Required, 'Please provide Town')
            ->addRule(FForm::MAX_LENGTH, "Town can't be more than 50 characters", 50);
        $this->addText('county', 'County')
            ->addRule(FForm::MAX_LENGTH, "County can't be more than 50 characters", 50);
        $this->addText('postcode', 'Postcode *')
            ->addRule(FForm::Required, 'Please provide Postcode')
            ->addRule(FForm::MAX_LENGTH, "Postcode can't be more than 15 characters", 15);
        $this->addSelect('country', 'Country *', Address::$countries)
            ->addRule(FForm::Required, 'Please provide Country');


        $this->addFieldset('EEA / Non EEA');
        $this->addRadio('eeaType', 'Type *', array(1 => 'EEA', 2 => 'Non EEA'))
            ->addRule(FForm::Required, 'Please provide EEA type!');
        $this->addText('place_registered', 'Country Registered *')
            ->addRule(array('CHValidator', 'Validator_eeaRequired'), 'Please provide Country registered!');
        $this->addText('registration_number', 'Registration number *')
            ->addRule(array('CHValidator', 'Validator_eeaRequired'), 'Please provide Registration number!');
        $this->addText('law_governed', 'Governing law *')
            ->addRule(array('CHValidator', 'Validator_eeaRequired'), 'Please provide Governing law!');
        $this->addText('legal_form', 'Legal From *')
            ->addRule(array('CHValidator', 'Validator_eeaRequired'), 'Please provide Legal form!');

        $this->addFieldset('Consent to act');
        $this->add('SingleCheckbox', 'consentToAct', 'The company confirms that the corporate body named has consented to act as a secretary')
            ->addRule(FForm::Required, 'Consent to act is required');

        $this->addFieldset('Action');
        $this->addSubmit('continue', 'Continue >')->class('btn_submit fright mbottom');

        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();

            // get corporate
            $secretary = $this->company->getNewCorporate('sec');
            $corporate = $secretary->getCorporate();
            $corporate->setFields($data);
            $corporate->setAppointmentDate(
                !empty($data['appointment_date']) ? Date::changeFormat($data['appointment_date'], 'd-m-Y', 'Y-m-d') : NULL
            );
            $secretary->setCorporate($corporate);

            // address
            $address = $secretary->getAddress();
            $address->setFields($data);
            $secretary->setAddress($address);

            $secretary->setConsentToAct($data['consentToAct']);

            // identification
            if ($data['eeaType'] == 1) {
                $identification = $secretary->getIdentification(Identification::EEA);
            } else {
                $identification = $secretary->getIdentification(Identification::NonEEA);
            }

            $identification->setFields($data);
            $secretary->setIdentification($identification);
            $this->company->sendAppointmentOfCorporateSecretary($secretary);
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

}
