<?php

namespace Form\Both\CUSecretary;

use Admin\CHForm;
use Company;
use FControler;
use PersonSecretary;
use Address;
use FForm;
use Prefiller;
use Person;

class EditSecretaryPersonForm extends CHForm
{

    /**
     * @var FControler 
     */
    public $controller;

    /**
     * @var Company 
     */
    public $company;

    /**
     * @var PersonSecretary 
     */
    public $secretary;

    /**
     * @var Prefiller 
     */
    public $prefiller;

    /**
     * @var array 
     */
    public $callback;

    /**
     * @param FControler $controller
     * @param array $callback
     * @param Company $company
     * @param PersonSecretary $secretary
     * @param Prefiller $prefiller
     */
    public function startup(FControler $controller, array $callback, Company $company, PersonSecretary $secretary, Prefiller $prefiller)
    {
        $this->controller = $controller;
        $this->company = $company;
        $this->secretary = $secretary;
        $this->prefiller = $prefiller;
        $this->callback = $callback;
        $this->init();
    }

    private function init()
    {
        $prefillAddress = $this->prefiller->getPrefillAdresses();

        // Name
        $this->addFieldset('Person');
        $this->addCheckbox('changeName', 'Change:', 1)
            ->addRule(array('CHValidator', 'Validator_changeChecboxRequired'), 'No changes have been made', array('changeName', 'changeAddress'));
        $this->addSelect('title', 'Title', Person::$titles)
            ->setFirstOption('--- Select ---');
        $this->addText('forename', 'First name *')
            ->addRule(array('CHValidator', 'Validator_secretaryEditChangeRequired'), 'Please provide first name', 'changeName')
            ->addRule(FForm::MAX_LENGTH, "First name can't be more than 50 characters", 50);
        $this->addText('middle_name', 'Middle name')
            ->addRule(FForm::MAX_LENGTH, "Middle name can't be more than 50 characters", 50)
            ->addRule(FForm::MIN_LENGTH, "Please provide full middle name.", 2);
        $this->addText('surname', 'Last name *')
            ->addRule(array('CHValidator', 'Validator_secretaryEditChangeRequired'), 'Please provide last name', 'changeName')
            ->addRule(FForm::MAX_LENGTH, "Last name can't be more than 160 characters", 160);

        // prefill
        $this->addFieldset('Prefill');
        $this->addSelect('prefillAddress', 'Prefill Address', $prefillAddress['select'])
            ->setFirstOption('--- Select ---');


        // adddress
        $this->addFieldset('Service Address');
        $this->addCheckbox('changeAddress', 'Change:', 1);
        $this->addText('premise', 'Building name/number *')
            ->addRule(array('CHValidator', 'Validator_secretaryEditChangeRequired'), 'Please provide Building name/number', 'changeAddress')
            ->addRule(FForm::MAX_LENGTH, "Building name/number can't be more than 50 characters", 50);
        $this->addText('street', 'Street *')
            ->addRule(array('CHValidator', 'Validator_secretaryEditChangeRequired'), 'Please provide Street', 'changeAddress')
            ->addRule(FForm::MAX_LENGTH, "Street can't be more than 50 characters", 50);
        $this->addText('thoroughfare', 'Address 3')
            ->addRule(FForm::MAX_LENGTH, "Address 3 can't be more than 50 characters", 50);
        $this->addText('post_town', 'Town *')
            ->addRule(array('CHValidator', 'Validator_secretaryEditChangeRequired'), 'Please provide Town', 'changeAddress')
            ->addRule(FForm::MAX_LENGTH, "Town can't be more than 50 characters", 50);
        $this->addText('county', 'County')
            ->addRule(FForm::MAX_LENGTH, "County can't be more than 50 characters", 50);
        $this->addText('postcode', 'Postcode *')
            ->addRule(array('CHValidator', 'Validator_serviceAddress'), 'You cannot use our postcode for this address', $this->company)
            ->addRule(array('CHValidator', 'Validator_secretaryEditChangeRequired'), 'Please provide Postcode', 'changeAddress')
            ->addRule(FForm::MAX_LENGTH, "Postcode can't be more than 15 characters", 15);
        $this->addSelect('country', 'Country *', Address::$countries)
            ->addRule(array('CHValidator', 'Validator_secretaryEditChangeRequired'), 'Please provide Country', 'changeAddress')
            ->addRule(FForm::MAX_LENGTH, "Country can't be more than 50 characters", 50);


        // submit				
        $this->addFieldset('Action');
        $this->addSubmit('continue', 'Save')->class('btn_submit fright mbottom');

        $fields = $this->secretary->getFields();
        $this->setInitValues($fields);

        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();

            // change name
            $change = array();
            if ($data['changeName'] == 1) {
                $change[] = 'changeName';
            }
            if ($data['changeAddress'] == 1) {
                $change[] = 'changeAddress';
            }
            $this->company->sendChangeOfNaturalSecretary($this->secretary, $data, $change);
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

}
