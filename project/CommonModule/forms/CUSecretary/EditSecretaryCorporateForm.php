<?php

namespace Form\Both\CUSecretary;

use Admin\CHForm;
use Company;
use FControler;
use CorporateSecretary;
use Address;
use FForm;
use Prefiller;

class EditSecretaryCorporateForm extends CHForm
{

    /**
     * @var FControler 
     */
    public $controller;

    /**
     * @var Company 
     */
    public $company;

    /**
     * @var PersonSecretary 
     */
    public $secretary;

    /**
     * @var Prefiller 
     */
    public $prefiller;

    /**
     * @var array 
     */
    public $callback;

    /**
     * @param FControler $controller
     * @param array $callback
     * @param Company $company
     * @param CorporateSecretary $secretary
     * @param Prefiller $prefiller
     */
    public function startup(FControler $controller, array $callback, Company $company, CorporateSecretary $secretary, Prefiller $prefiller)
    {
        $this->controller = $controller;
        $this->company = $company;
        $this->secretary = $secretary;
        $this->prefiller = $prefiller;
        $this->callback = $callback;
        $this->init();
    }

    private function init()
    {
        $prefillAddress = $this->prefiller->getPrefillAdresses();

        // Name
        $this->addFieldset('Person');
        $this->addCheckbox('changeName', 'Change:', 1)
            ->addRule(array('CHValidator', 'Validator_changeChecboxRequired'), 'No changes have been made', array('changeName', 'changeAddress', 'changeEEA'));
        $this->addText('corporate_name', 'Company name*')
            ->addRule(array('CHValidator', 'Validator_secretaryEditChangeRequired'), 'Please provide company name', 'changeName');

        // prefill
        $this->addFieldset('Prefill');
        $this->addSelect('prefillAddress', 'Prefill Address', $prefillAddress['select'])
            ->setFirstOption('--- Select ---');

        // adddress
        $this->addFieldset('Service Address');
        $this->addCheckbox('changeAddress', 'Change:', 1);
        $this->addText('premise', 'Building name/number *')
            ->addRule(array('CHValidator', 'Validator_secretaryEditChangeRequired'), 'Please provide Building name/number', 'changeAddress')
            ->addRule(FForm::MAX_LENGTH, "Building name/number can't be more than 50 characters", 50);
        $this->addText('street', 'Street *')
            ->addRule(array('CHValidator', 'Validator_secretaryEditChangeRequired'), 'Please provide street', 'changeAddress')
            ->addRule(FForm::MAX_LENGTH, "Street can't be more than 50 characters", 50);
        $this->addText('thoroughfare', 'Address 3')
            ->addRule(FForm::MAX_LENGTH, "Address 3 can't be more than 50 characters", 50);
        $this->addText('post_town', 'Town *')
            ->addRule(array('CHValidator', 'Validator_secretaryEditChangeRequired'), 'Please provide Town', 'changeAddress')
            ->addRule(FForm::MAX_LENGTH, "Town can't be more than 50 characters", 50);
        $this->addText('county', 'County')
            ->addRule(FForm::MAX_LENGTH, "County can't be more than 50 characters", 50);
        $this->addText('postcode', 'Postcode *')
            ->addRule(array('CHValidator', 'Validator_secretaryEditChangeRequired'), 'Please provide Postcode', 'changeAddress')
            ->addRule(FForm::MAX_LENGTH, "Postcode can't be more than 15 characters", 15);
        $this->addSelect('country', 'Country *', Address::$countries)
            ->addRule(array('CHValidator', 'Validator_secretaryEditChangeRequired'), 'Please provide Country', 'changeAddress')
            ->addRule(FForm::MAX_LENGTH, "Country can't be more than 50 characters", 50);


        // eea
        $this->addFieldset('EEA/ Non EEA');
        $this->addCheckbox('changeEEA', 'Change:', 1);
        $this->addRadio('eeaType', 'Type *', array(1 => 'EEA', 2 => 'Non EEA'))
            ->addRule(array('CHValidator', 'Validator_secretaryEditChangeEeaRequired'), 'Please provide EEA type!', 'changeEEA');
        $this->addText('place_registered', 'Country Registered *')
            ->addRule(array('CHValidator', 'Validator_secretaryEditChangeEeaRequired'), 'Please provide Country registered!', 'changeEEA');
        $this->addText('registration_number', 'Registration number *')
            ->addRule(array('CHValidator', 'Validator_secretaryEditChangeEeaRequired'), 'Please provide Registration number!', 'changeEEA');
        $this->addText('law_governed', 'Governing law *')
            ->addRule(array('CHValidator', 'Validator_secretaryEditChangeEeaRequired'), 'Please provide Governing law!', 'changeEEA');
        $this->addText('legal_form', 'Legal From *')
            ->addRule(array('CHValidator', 'Validator_secretaryEditChangeEeaRequired'), 'Please provide Legal form!', 'changeEEA');

        // submit				
        $this->addFieldset('Action');
        $this->addSubmit('continue', 'Save')->class('btn_submit fright mbottom');

        $fields = $this->secretary->getFields();
        $this->setInitValues($fields);

        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();
            // change name
            $change = array();
            if ($data['changeName'] == 1) {
                $change[] = 'changeName';
            }
            if ($data['changeAddress'] == 1) {
                $change[] = 'changeAddress';
            }
            if ($data['changeEEA'] == 1) {
                $change[] = 'changeEEA';
            }
            $this->company->sendChangeOfCorporateSecretary($this->secretary, $data, $change);
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

}
