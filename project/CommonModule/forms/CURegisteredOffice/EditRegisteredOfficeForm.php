<?php

namespace Form\Both\CURegisteredOffice;

use Admin\CHForm;
use Company;
use FControler;
use Address;
use FForm;
use Prefiller;
use RegisterOffice;

class EditRegisteredOfficeForm extends CHForm
{

    /**
     * @var FControler 
     */
    public $controller;

    /**
     * @var Company 
     */
    public $company;

    /**
     * @var array 
     */
    public $callback;

    /**
     * @var Prefiller 
     */
    public $prefiller;

    /**
     * @param FControler $controller
     * @param array $callback
     * @param Company $company
     * @param Prefiller $prefiller
     */
    public function startup(FControler $controller, array $callback, Company $company, Prefiller $prefiller)
    {
        $this->controller = $controller;
        $this->company = $company;
        $this->callback = $callback;
        $this->prefiller = $prefiller;
        $this->init();
    }

    private function init()
    {
        // if has registered office
        $registeredOfficeId = $this->company->getRegisteredOfficeId();
        if ($registeredOfficeId) {
            // our registered office
            $this->addFieldset('Registered office');
            $this->addCheckbox('ourRegisteredOffice', 'Our registered office  ', 1)
                ->setValue(1);
        }

        $prefillAddress = $this->prefiller->getPrefillAdresses();
        // address
        $this->addFieldset('Prefill');
        $this->addSelect('prefill', 'Address', $prefillAddress['select'])
            ->setFirstOption('--- Select ---');

        $this->addFieldset('Address');
        $this->addText('premise', 'Premise *')
            ->addRule(array('CHValidator', 'Validator_officeRequired'), 'Please provide Premise')
            ->addRule(FForm::MAX_LENGTH, "Premise can't be more than 50 characters", 50);
        $this->addText('street', 'Street *')
            ->addRule(array('CHValidator', 'Validator_officeRequired'), 'Please provide Street')
            ->addRule(FForm::MAX_LENGTH, "Street can't be more than 50 characters", 50);
        $this->addText('thoroughfare', 'Address 3')
            ->addRule(FForm::MAX_LENGTH, "Address 3 can't be more than 50 characters", 50);
        $this->addText('post_town', 'Town *')
            ->addRule(array('CHValidator', 'Validator_officeRequired'), 'Please provide town')
            ->addRule(FForm::MAX_LENGTH, "Town can't be more than 50 characters", 50);
        $this->addText('county', 'County')
            ->addRule(FForm::MAX_LENGTH, "County can't be more than 50 characters", 50);
        $this->addText('postcode', 'Postcode *')
            ->addRule(array('CHValidator', 'Validator_officeRequired'), 'Please provide postcode')
            ->addRule(FForm::MAX_LENGTH, "Postcode can't be more than 15 characters", 15);
        $this->addSelect('country', 'Country *', Address::$registeredOfficeCountries)
            ->setFirstOption('--- Select ---')
            ->addRule(array('CHValidator', 'Validator_officeRequired'), 'Please provide country')
            ->addRule(FForm::MAX_LENGTH, "Country can't be more than 50 characters", 50);

        // action
        $this->addFieldset('Action');
        $this->addSubmit('save', 'Send')->class('btn');

        $this->setInitValues($this->company->getRegisteredOffice()->getFields());

        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();

            $address = new Address;
            $registeredOfficeId = $this->company->getRegisteredOfficeId();

            // has register office
            if ($registeredOfficeId != NULL && isset($this['ourRegisteredOffice']) && $this['ourRegisteredOffice']->getValue() == 1) {

                $office = new RegisterOffice($registeredOfficeId);

                $address->setPremise($office->address1);
                $address->setStreet($office->address2);
                $address->setThoroughfare($office->address3);
                $address->setPostTown($office->town);
                $address->setCounty($office->county);
                $address->setPostcode($office->postcode);
                $address->setRegisteredOfficeCountry($office->countryId);
            } else {
                $address->setFields($this->getValues());
            }

            // send request to CH
            $this->company->sendChangeRegisteredOfficeAddress($address);
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

}
