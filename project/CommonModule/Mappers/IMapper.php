<?php

namespace CommonModule\Mappers;

interface IMapper
{
    /**
     * @param mixed $details
     * @return array
     */
    public function toArray($details);
}