<?php

namespace CommonModule\Controllers;

use FTemplate;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Router;

class RouteTester
{
    /**
     * @var Router
     */
    private $router;

    /**
     * @var FTemplate
     */
    private $template;

    /**
     * @param Router $router
     * @param FTemplate $template
     */
    public function __construct(Router $router, FTemplate $template)
    {
        $this->router = $router;
        $this->template = $template;
        //comment it if you want to test it
        throw new ResourceNotFoundException();
    }

    /**
     * @param string $customerId
     * @return Response
     */
    public function test1($customerId)
    {
        return new Response(var_export(array('method' => 'test1', 'customerId' => $customerId), TRUE));
    }

    /**
     * @param int $type
     * @return array
     */
    public function test2($type)
    {
        return array('method' => 'test2', 'typeId' => $type);
    }

    public function test3()
    {
        $this->template->test1 = $this->router->generate('route_tester1', array('customerId' => 'abc'));
    }

    public function test4()
    {
        $this->template->method = 'test4';
    }

}