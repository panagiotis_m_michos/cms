<?php

namespace Remote\VirtualOffice\Api;

use GuzzleHttp\Client;
use GuzzleHttp\Message\MessageFactoryInterface;
use Nette\Object;
use ValueObject\VoApiConfig;

class ClientFactory extends Object
{
    /**
     * @param VoApiConfig $config
     * @param MessageFactoryInterface $messageFactory
     * @return Client
     */
    public static function createClient(VoApiConfig $config, MessageFactoryInterface $messageFactory)
    {
        $basicAuthCredentials = $config->getBasicAuthCredentials();
        $client = new Client(
            [
                'base_url' => [$config->getBaseUrl(), []],
                'defaults' => [
                    'auth' => [$basicAuthCredentials->getUsername(), $basicAuthCredentials->getPassword()],
                ],
                'message_factory' => $messageFactory
            ]
        );
        return $client;
    }
}