<?php

namespace Remote\VirtualOffice\Api\Message;

use GuzzleHttp\Message\Response;
use GuzzleHttp\Stream\StreamInterface;

class JsonResponse extends Response
{
    /**
     * @inheritdoc
     */
    public function __construct($statusCode, array $headers = [], StreamInterface $body = NULL, array $options = [])
    {
        parent::__construct($statusCode, $headers, $body, $options);

        $response = $this->json();
        if (!array_key_exists('success', $response)) {
            throw JsonResponseException::invalidBody();
        }
        if ($response['success'] === FALSE && $statusCode < 400) {
            throw JsonResponseException::invalidStatusCode($statusCode);
        }
    }
}