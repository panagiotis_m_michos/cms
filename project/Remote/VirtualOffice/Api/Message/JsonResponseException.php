<?php

namespace Remote\VirtualOffice\Api\Message;

use Exceptions\Technical\TechnicalAbstract;
use Symfony\Component\HttpFoundation\Response;

class JsonResponseException extends TechnicalAbstract
{
    /**
     * @return JsonResponseException
     */
    public static function invalidBody()
    {
        return new self('Response has invalid json response body', Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @param int $code
     * @return JsonResponseException
     */
    public static function invalidStatusCode($code)
    {
        return new self("Error response has invalid status code `$code`", Response::HTTP_INTERNAL_SERVER_ERROR);
    }

}