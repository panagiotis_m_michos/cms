<?php

namespace Remote\VirtualOffice\Api\Message;

use GuzzleHttp\Message\MessageFactory;
use GuzzleHttp\Stream\Stream;

class JsonMessageFactory extends MessageFactory
{
    /**
     * {@inheritdoc}
     */
    public function createResponse($statusCode, array $headers = [], $body = NULL, array $options = [])
    {
        if ($body !== NULL) {
            $body = Stream::factory($body);
        }
        return new JsonResponse($statusCode, $headers, $body, $options);
    }
}