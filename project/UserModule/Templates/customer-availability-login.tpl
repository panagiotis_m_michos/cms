<customer-availability-login>
    {$formHelper->setTheme($form, 'horizontal.html.twig')}
    {$formHelper->start($form, ['attr' => ['rv-on-submit' => 'useCredentials']]) nofilter}

    <h3 class="top0">Create Account or Log In</h3>
    <p>We'll use your email address to send your company details and purchase confirmation</p>

    {$formHelper->errors($form) nofilter}

    <div class="form-group{if !$form['email']->vars['valid']} has-error{/if}">
        {$formHelper->label($form['email']) nofilter}
        <div class="col-sm-6 has-feedback">
            {$formHelper->widget($form['email']) nofilter}
            <div class="has-success{if empty($emailConfirmed)} hidden{/if}" rv-show="isEmailLocked < emailConfirmed requiresToLogin">
                <span class="glyphicon glyphicon-ok form-control-feedback"></span>
            </div>
            <div class="{if empty($emailConfirmed) || !empty($requiresToLogin)} hidden{/if}" rv-show="canUseCredentials < emailConfirmed requiresToLogin">
                <p class="top10">Your account has been created!<br>Please enter your payment details below:</p>
            </div>
            {$formHelper->errors($form['email']) nofilter}
        </div>
        <div class="col-sm-4{if empty($emailConfirmed)} hidden{/if}" rv-show="isEmailLocked < emailConfirmed requiresToLogin"><a rv-on-click="useAnotherEmail" href="javascript:;">Use another email</a></div>
    </div>
    <div class="form-group{if !empty($requiresToLogin)} hidden{/if}" rv-hide="requiresToLogin">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default" name="check" value="1" rv-disabled="emailConfirmed">Create account or log in</button>
        </div>
    </div>
    <div class="{if empty($requiresToLogin)}hidden{/if}" rv-show="requiresToLogin">
        <div class="form-group{if !$form['password']->vars['valid']} has-error{/if}">
            {$formHelper->label($form['password']) nofilter}
            <div class="col-sm-6">
                {$formHelper->widget($form['password']) nofilter}
                {$formHelper->errors($form['password']) nofilter}
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-4">
                <button type="submit" name="login" value="1" class="btn btn-default">Create account or log in</button>
            </div>
            <div class="col-sm-6">
                <i class="fa fa-spinner fa-spin fa-2x fa-fw hidden" rv-show="sendingEmail" aria-hidden="true"></i>
                <div rv-hide="sendingEmail">
                    <div class="hidden" rv-show="emailSent">New password sent</div>
                    <div class="">
                        <a target="_blank" href="{url route="LoginControler::FORGOTTEN_PASSWORD_PAGE"}"  rv-on-click="forgottenEmail">
                            <span rv-hide="emailSent">I've forgotten my password</span>
                            <span class="hidden" rv-show="emailSent">Resend password</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {$formHelper->end($form) nofilter}
</customer-availability-login>

<script type="text/javascript">
    window.loginAvailability = customerLogin.bind(
            {$emailConfirmed|json nofilter},
            {$requiresToLogin|json nofilter},
            '{url route="login_availability"}',
            '{url route="reset_customer_password"}',
            {if isset($returnType)}'{$returnType}'{else}null{/if}
    );
</script>
