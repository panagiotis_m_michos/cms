<?php

namespace FormModule;

use Symfony\Component\Intl\Intl;

class Country
{
    const UNITED_KINGDOM = 'GB';

    const NAME_UNITED_KINGDOM = 'United Kingdom';

    /**
     * @var array
     */
    public static $euCountries = array(
        'BE', 'BG', 'CZ', 'DK', 'DE', 'EE', 'IE', 'GR', 'ES',
        'FR', 'HR', 'IT', 'CY', 'LV', 'LT', 'LU', 'HU', 'MT',
        'NL', 'AT', 'PL', 'PT', 'RO', 'SI', 'SK', 'FI', 'SE',
        'GB'
    );

    /**
     * @return array
     */
    public static function getEuCountries()
    {
        $allCountries = Intl::getRegionBundle()->getCountryNames();
        $euCountries = array();
        foreach (self::$euCountries as $countryCode) {
            $euCountries[$countryCode] = $allCountries[$countryCode];
        }
        return $euCountries;
    }

    /**
     * @return array
     */
    public static function getCountryNames()
    {
        $allCountries = Intl::getRegionBundle()->getCountryNames();
        return array_combine($allCountries, $allCountries);
    }

    /**
     * @return array
     */
    public static function getEuCountryNames()
    {
        $euCountries = self::getEuCountries();
        return array_combine($euCountries, $euCountries);
    }

    /**
     * @return array
     */
    public static function getCountries()
    {
        return Intl::getRegionBundle()->getCountryNames();
    }
}
