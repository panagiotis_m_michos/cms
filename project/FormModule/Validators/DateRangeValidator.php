<?php

namespace FormModule\Validators;

use DateTime;
use FormModule\Model\DatesRange;
use Nette\Object;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DateRangeValidator extends Object implements EventSubscriberInterface
{
    /**
     * @var array
     */
    protected $options = [];

    /**
     * @param OptionsResolverInterface $resolver
     * @param array $options
     */
    public function __construct(OptionsResolverInterface $resolver, array $options = [])
    {
        $this->setDefaultOptions($resolver);
        $this->options = $resolver->resolve($options);
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::POST_SUBMIT => 'onPostBind',
        ];
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'allow_end_in_past' => TRUE,
                'allow_single_day' => TRUE,
            ]
        );

        $resolver->setAllowedValues(
            [
                'allow_end_in_past' => [TRUE, FALSE],
                'allow_single_day' => [TRUE, FALSE],
            ]
        );
    }

    /**
     * @param FormEvent $event
     */
    public function onPostBind(FormEvent $event)
    {
        $form = $event->getForm();

        /* @var $dateRange DatesRange */
        $dateRange = $form->getNormData();
        $start = $dateRange->getStart();
        $end = $dateRange->getEnd();

        if ($start && $end && $start > $end) {
            $form->addError(new FormError('Date end has to be greater than date start'));
        }

        if (!$this->options['allow_single_day'] && $start && $end && ($start->format('Y-m-d') === $end->format('Y-m-d'))) {
            $form->addError(new FormError('Single day date range is not allowed'));
        }

        if (!$this->options['allow_end_in_past'] && ($end < new DateTime())) {
            $form->addError(new FormError('Date end cannot be in the past'));
        }
    }
}