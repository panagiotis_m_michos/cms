<?php

namespace FormModule;

class Month
{
    private static $list = [
        "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
    ];

    /**
     * @return array
     */
    public static function getNames()
    {
        return array_combine(self::$list, self::$list);
    }

}