<?php

namespace FormModule\Types;

use FormModule\Transformers\DateRangeViewTransformer;
use FormModule\Validators\DateRangeValidator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DateRangeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'start_date',
                'date',
                array_merge_recursive(
                    [
                        'property_path' => 'start',
                        'widget' => 'single_text',
                        'format' => 'yyyy-MM-dd',
                        'attr' => [
                            'data-type' => 'start',
                        ],
                        'invalid_message' => 'Please provide a valid date',
                    ],
                    $options['start_options']
                )
            )
            ->add(
                'end_date',
                'date',
                array_merge_recursive(
                    [
                        'property_path' => 'end',
                        'widget' => 'single_text',
                        'format' => 'yyyy-MM-dd',
                        'attr' => [
                            'data-type' => 'end',
                        ],
                        'invalid_message' => 'Please provide a valid date',
                    ],
                    $options['end_options']
                )
            );


        $builder->addViewTransformer($options['transformer']);
        $builder->addEventSubscriber($options['validator']);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'FormModule\Model\DatesRange',
                'end_options' => [],
                'start_options' => [],
                'transformer' => function (Options $options, $value) {
                    if (!$value) {
                        $value = new DateRangeViewTransformer(new OptionsResolver());
                    }
                    return $value;
                },
                'validator' => function (Options $options, $value) {
                    if (!$value) {
                        $value = new DateRangeValidator(new OptionsResolver());
                    }
                    return $value;
                }
            ]
        );

        $resolver->setAllowedTypes(
            [
                'transformer' => 'Symfony\Component\Form\DataTransformerInterface',
                'validator' => 'Symfony\Component\EventDispatcher\EventSubscriberInterface',
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'date_range';
    }
}