<?php

namespace FormModule\Transformers;

use DateTime;
use FormModule\Model\DatesRange;
use Nette\Object;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DateRangeViewTransformer extends Object implements DataTransformerInterface
{
    /**
     * @var array
     */
    protected $options = [];

    /**
     * @param OptionsResolverInterface $resolver
     * @param array $options
     */
    public function __construct(OptionsResolverInterface $resolver, array $options = [])
    {
        $this->setDefaultOptions($resolver);
        $this->options = $resolver->resolve($options);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'include_end' => TRUE,
            ]
        );

        $resolver->setAllowedValues(
            [
                'include_end' => [TRUE, FALSE],
            ]
        );
    }

    /**
     * @param DatesRange|NULL $value
     * @return DatesRange|NULL
     */
    public function transform($value)
    {
        if (!$value) {
            return NULL;
        }

        if (!$value instanceof DatesRange) {
            throw new UnexpectedTypeException($value, 'FormModule\Model\DatesRange');
        }

        return $value;
    }

    /**
     * @param mixed $value
     * @return mixed|null
     */
    public function reverseTransform($value)
    {
        if (!$value) {
            return NULL;
        }

        if (!$value instanceof DatesRange) {
            throw new UnexpectedTypeException($value, 'FormModule\Model\DatesRange');
        }

        if ($this->options['include_end'] && $value->getEnd() instanceof DateTime) {
            $value->getEnd()->setTime(23, 59, 59);
        }

        return $value;
    }
}