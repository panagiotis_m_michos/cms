<?php

namespace FormModule\Model;

use DateTime;
use Nette\Object;

class DatesRange extends Object
{
    /**
     * @var DateTime
     */
    private $start;

    /**
     * @var DateTime
     */
    private $end;

    /**
     * @param DateTime $start
     * @param DateTime $end
     */
    public function __construct(DateTime $start = NULL, DateTime $end = NULL)
    {
        $this->start = $start;
        $this->end = $end;
    }

    /**
     * @return DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param DateTime|NULL $start
     */
    public function setStart(DateTime $start = NULL)
    {
        $this->start = $start;
    }

    /**
     * @return DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @param DateTime|NULL $end
     */
    public function setEnd(DateTime $end = NULL)
    {
        $this->end = $end;
    }

    /**
     * @return bool
     */
    public function hasDates()
    {
        return $this->start || $this->end;
    }
}