<?php

namespace FormModule;

class Gender
{

    /**
     * @var array
     */
    private static $list = ['Male', 'Female'];

    /**
     * @return array
     */
    public static function getNames()
    {
        return array_combine(self::$list, self::$list);
    }

}