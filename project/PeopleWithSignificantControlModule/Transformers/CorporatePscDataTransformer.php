<?php

use Entities\Register\Psc\CorporatePsc;
use PeopleWithSignificantControl\Forms\Address;
use PeopleWithSignificantControl\Forms\CorporatePscData;
use PeopleWithSignificantControl\Interfaces\ICorporatePsc;

class CorporatePscDataTransformer
{
    /**
     * @param ICorporatePsc $corporatePsc
     * @return CorporatePscData
     */
    public static function from(ICorporatePsc $corporatePsc)
    {
        $identification = $corporatePsc->getIdentification();
        $natureOfControl = $corporatePsc->getNatureOfControl();

        $address = $corporatePsc->getAddress();
        $serviceAddress = (new Address)
            ->setPremise($address->getPremise())
            ->setStreet($address->getStreet())
            ->setThoroughfare($address->getThoroughfare())
            ->setPostTown($address->getPostTown())
            ->setCounty($address->getCounty())
            ->setCountry($address->getCountry())
            ->setPostcode($address->getPostcode())
            ->setCountry($address->getCountry());

        return (new CorporatePscData)
            ->setCorporateName($corporatePsc->getCorporate()->getCorporateName())
            ->setServiceAddress($serviceAddress)
            ->setCountryRegistered($identification->getCountryOrState())
            ->setGoverningLaw($identification->getLawGoverned())
            ->setLegalForm($identification->getLegalForm())
            ->setPlaceRegistered($identification->getPlaceRegistered())
            ->setRegistrationNumber($identification->getRegistrationNumber())
            ->setOwnershipOfShares($natureOfControl->getOwnershipOfShares())
            ->setOwnershipOfVotingRights($natureOfControl->getOwnershipOfVotingRights())
            ->setRightToAppointAndRemoveDirectors($natureOfControl->getRightToAppointAndRemoveDirectors())
            ->setSignificantInfluenceOrControl($natureOfControl->getSignificantInfluenceOrControl());
    }

    /**
     * @param CorporatePsc $corporatePsc
     * @return CorporatePscData
     */
    public static function fromRegister(CorporatePsc $corporatePsc)
    {
        $serviceAddress = (new Address)
            ->setPremise($corporatePsc->getPremise())
            ->setStreet($corporatePsc->getStreet())
            ->setThoroughfare($corporatePsc->getThoroughfare())
            ->setPostTown($corporatePsc->getPostTown())
            ->setCounty($corporatePsc->getCounty())
            ->setCountry($corporatePsc->getCountry())
            ->setPostcode($corporatePsc->getPostcode())
            ->setCountry($corporatePsc->getCountry());

        return (new CorporatePscData)
            ->setCorporateName($corporatePsc->getCorporateName())
            ->setServiceAddress($serviceAddress)
            ->setCountryRegistered($corporatePsc->getCountryOrState())
            ->setGoverningLaw($corporatePsc->getLawGoverned())
            ->setLegalForm($corporatePsc->getLegalForm())
            ->setPlaceRegistered($corporatePsc->getPlaceRegistered())
            ->setRegistrationNumber($corporatePsc->getRegistrationNumber())
            ->setOwnershipOfShares($corporatePsc->getOwnershipOfShares())
            ->setOwnershipOfVotingRights($corporatePsc->getOwnershipOfVotingRights())
            ->setRightToAppointAndRemoveDirectors($corporatePsc->getRightToAppointAndRemoveDirectors())
            ->setSignificantInfluenceOrControl($corporatePsc->getSignificantInfluenceOrControl());
    }

    /**
     * @param ICorporatePsc $corporatePsc
     * @param CorporatePscData $data
     * @return ICorporatePsc mutated $corporatePsc
     * @throws Exception
     */
    public static function merge(ICorporatePsc $corporatePsc, CorporatePscData $data)
    {
        $corporate = $corporatePsc->getCorporate();
        $corporate->setCorporateName($data->getCorporateName());
        $corporatePsc->setCorporate($corporate);

        $dataAddress = $data->getServiceAddress();
        $address = $corporatePsc->getAddress();
        $address->setPremise($dataAddress->getPremise());
        $address->setStreet($dataAddress->getStreet());
        $address->setThoroughfare($dataAddress->getThoroughfare());
        $address->setPostTown($dataAddress->getPostTown());
        $address->setCounty($dataAddress->getCounty());
        $address->setCountry($dataAddress->getCountry());
        $address->setPostcode($dataAddress->getPostcode());
        $address->setCountry($dataAddress->getCountry());
        $corporatePsc->setAddress($address);

        $identification = $corporatePsc->getIdentification(Identification::PSC);
        $identification->setCountryOrState($data->getCountryRegistered());
        $identification->setLawGoverned($data->getGoverningLaw());
        $identification->setLegalForm($data->getLegalForm());
        $identification->setPlaceRegistered($data->getPlaceRegistered());
        $identification->setRegistrationNumber($data->getRegistrationNumber());
        $corporatePsc->setIdentification($identification);

        $natureOfControl = $corporatePsc->getNatureOfControl();
        $natureOfControl->setOwnershipOfShares($data->getOwnershipOfShares());
        $natureOfControl->setOwnershipOfVotingRights($data->getOwnershipOfVotingRights());
        $natureOfControl->setRightToAppointAndRemoveDirectors($data->getRightToAppointAndRemoveDirectors());
        $natureOfControl->setSignificantInfluenceOrControl($data->getSignificantInfluenceOrControl());
        $corporatePsc->setNatureOfControl($natureOfControl);

        return $corporatePsc;
    }

    /**
     * @param CorporatePsc $corporatePsc
     * @param CorporatePscData $data
     * @return CorporatePsc mutated $corporatePsc
     * @throws Exception
     */
    public static function mergeRegister(CorporatePsc $corporatePsc, CorporatePscData $data)
    {
        $corporatePsc->setCorporateName($data->getCorporateName());

        $dataAddress = $data->getServiceAddress();
        $corporatePsc->setPremise($dataAddress->getPremise());
        $corporatePsc->setStreet($dataAddress->getStreet());
        $corporatePsc->setThoroughfare($dataAddress->getThoroughfare());
        $corporatePsc->setPostTown($dataAddress->getPostTown());
        $corporatePsc->setCounty($dataAddress->getCounty());
        $corporatePsc->setCountry($dataAddress->getCountry());
        $corporatePsc->setPostcode($dataAddress->getPostcode());
        $corporatePsc->setCountry($dataAddress->getCountry());

        $corporatePsc->setCountryOrState($data->getCountryRegistered());
        $corporatePsc->setLawGoverned($data->getGoverningLaw());
        $corporatePsc->setLegalForm($data->getLegalForm());
        $corporatePsc->setPlaceRegistered($data->getPlaceRegistered());
        $corporatePsc->setRegistrationNumber($data->getRegistrationNumber());

        $corporatePsc->setOwnershipOfShares($data->getOwnershipOfShares());
        $corporatePsc->setOwnershipOfVotingRights($data->getOwnershipOfVotingRights());
        $corporatePsc->setRightToAppointAndRemoveDirectors($data->getRightToAppointAndRemoveDirectors());
        $corporatePsc->setSignificantInfluenceOrControl($data->getSignificantInfluenceOrControl());

        return $corporatePsc;
    }
}
