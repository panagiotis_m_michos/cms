<?php

use Entities\Register\Psc\LegalPersonPsc;
use PeopleWithSignificantControl\Forms\Address;
use PeopleWithSignificantControl\Forms\LegalPersonPscData;
use PeopleWithSignificantControl\Interfaces\ILegalPersonPsc;

class LegalPersonPscDataTransformer
{
    /**
     * @param ILegalPersonPsc $legalPersonPsc
     * @return LegalPersonPscData
     */
    public static function from(ILegalPersonPsc $legalPersonPsc)
    {
        $identification = $legalPersonPsc->getIdentification();
        $natureOfControl = $legalPersonPsc->getNatureOfControl();

        $address = $legalPersonPsc->getAddress();
        $serviceAddress = (new Address)
            ->setPremise($address->getPremise())
            ->setStreet($address->getStreet())
            ->setThoroughfare($address->getThoroughfare())
            ->setPostTown($address->getPostTown())
            ->setCounty($address->getCounty())
            ->setCountry($address->getCountry())
            ->setPostcode($address->getPostcode())
            ->setCountry($address->getCountry());

        return (new LegalPersonPscData())
            ->setLegalPersonName($legalPersonPsc->getCorporate()->getCorporateName())
            ->setGoverningLaw($identification->getLawGoverned())
            ->setLegalForm($identification->getLegalForm())
            ->setServiceAddress($serviceAddress)
            ->setOwnershipOfShares($natureOfControl->getOwnershipOfShares())
            ->setOwnershipOfVotingRights($natureOfControl->getOwnershipOfVotingRights())
            ->setRightToAppointAndRemoveDirectors($natureOfControl->getRightToAppointAndRemoveDirectors())
            ->setSignificantInfluenceOrControl($natureOfControl->getSignificantInfluenceOrControl());
    }

    /**
     * @param LegalPersonPsc $legalPersonPsc
     * @return LegalPersonPscData
     */
    public static function fromRegister(LegalPersonPsc $legalPersonPsc)
    {
        $serviceAddress = (new Address)
            ->setPremise($legalPersonPsc->getPremise())
            ->setStreet($legalPersonPsc->getStreet())
            ->setThoroughfare($legalPersonPsc->getThoroughfare())
            ->setPostTown($legalPersonPsc->getPostTown())
            ->setCounty($legalPersonPsc->getCounty())
            ->setCountry($legalPersonPsc->getCountry())
            ->setPostcode($legalPersonPsc->getPostcode())
            ->setCountry($legalPersonPsc->getCountry());

        return (new LegalPersonPscData)
            ->setLegalPersonName($legalPersonPsc->getLegalPersonName())
            ->setServiceAddress($serviceAddress)
            ->setGoverningLaw($legalPersonPsc->getLawGoverned())
            ->setLegalForm($legalPersonPsc->getLegalForm())
            ->setOwnershipOfShares($legalPersonPsc->getOwnershipOfShares())
            ->setOwnershipOfVotingRights($legalPersonPsc->getOwnershipOfVotingRights())
            ->setRightToAppointAndRemoveDirectors($legalPersonPsc->getRightToAppointAndRemoveDirectors())
            ->setSignificantInfluenceOrControl($legalPersonPsc->getSignificantInfluenceOrControl());
    }

    /**
     * @param ILegalPersonPsc $legalPersonPsc
     * @param LegalPersonPscData $data
     * @return ILegalPersonPsc mutated $corporatePsc
     * @throws Exception
     */
    public static function merge(ILegalPersonPsc $legalPersonPsc, LegalPersonPscData $data)
    {
        $corporate = $legalPersonPsc->getCorporate();
        $corporate->setCorporateName($data->getLegalPersonName());
        $legalPersonPsc->setCorporate($corporate);

        $dataAddress = $data->getServiceAddress();
        $address = $legalPersonPsc->getAddress();
        $address->setPremise($dataAddress->getPremise());
        $address->setStreet($dataAddress->getStreet());
        $address->setThoroughfare($dataAddress->getThoroughfare());
        $address->setPostTown($dataAddress->getPostTown());
        $address->setCounty($dataAddress->getCounty());
        $address->setCountry($dataAddress->getCountry());
        $address->setPostcode($dataAddress->getPostcode());
        $address->setCountry($dataAddress->getCountry());
        $legalPersonPsc->setAddress($address);

        $identification = $legalPersonPsc->getIdentification(Identification::PSC);
        $identification->setLawGoverned($data->getGoverningLaw());
        $identification->setLegalForm($data->getLegalForm());
        $legalPersonPsc->setIdentification($identification);

        $natureOfControl = $legalPersonPsc->getNatureOfControl();
        $natureOfControl->setOwnershipOfShares($data->getOwnershipOfShares());
        $natureOfControl->setOwnershipOfVotingRights($data->getOwnershipOfVotingRights());
        $natureOfControl->setRightToAppointAndRemoveDirectors($data->getRightToAppointAndRemoveDirectors());
        $natureOfControl->setSignificantInfluenceOrControl($data->getSignificantInfluenceOrControl());
        $legalPersonPsc->setNatureOfControl($natureOfControl);

        return $legalPersonPsc;
    }

    /**
     * @param LegalPersonPsc $legalPersonPsc
     * @param LegalPersonPscData $data
     * @return LegalPersonPsc mutated $corporatePsc
     * @throws Exception
     */
    public static function mergeRegister(LegalPersonPsc $legalPersonPsc, LegalPersonPscData $data)
    {
        $legalPersonPsc->setLegalPersonName($data->getLegalPersonName());

        $dataAddress = $data->getServiceAddress();
        $legalPersonPsc->setPremise($dataAddress->getPremise());
        $legalPersonPsc->setStreet($dataAddress->getStreet());
        $legalPersonPsc->setThoroughfare($dataAddress->getThoroughfare());
        $legalPersonPsc->setPostTown($dataAddress->getPostTown());
        $legalPersonPsc->setCounty($dataAddress->getCounty());
        $legalPersonPsc->setCountry($dataAddress->getCountry());
        $legalPersonPsc->setPostcode($dataAddress->getPostcode());
        $legalPersonPsc->setCountry($dataAddress->getCountry());

        $legalPersonPsc->setLawGoverned($data->getGoverningLaw());
        $legalPersonPsc->setLegalForm($data->getLegalForm());

        $legalPersonPsc->setOwnershipOfShares($data->getOwnershipOfShares());
        $legalPersonPsc->setOwnershipOfVotingRights($data->getOwnershipOfVotingRights());
        $legalPersonPsc->setRightToAppointAndRemoveDirectors($data->getRightToAppointAndRemoveDirectors());
        $legalPersonPsc->setSignificantInfluenceOrControl($data->getSignificantInfluenceOrControl());

        return $legalPersonPsc;
    }
}
