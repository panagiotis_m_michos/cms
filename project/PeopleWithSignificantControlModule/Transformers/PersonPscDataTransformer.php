<?php


use Entities\Register\Psc\PersonPsc;
use PeopleWithSignificantControl\Forms\Address;
use PeopleWithSignificantControl\Forms\CorporatePscData;
use PeopleWithSignificantControl\Forms\PersonPscData;
use PeopleWithSignificantControl\Interfaces\ICorporatePsc;
use PeopleWithSignificantControl\Interfaces\IPersonPsc;

class PersonPscDataTransformer
{
    /**
     * @param IPersonPsc $personPsc
     * @return PersonPscData
     */
    public static function from(IPersonPsc $personPsc)
    {
        $natureOfControl = $personPsc->getNatureOfControl();

        $address = $personPsc->getAddress();
        $dataAddress = (new Address)
            ->setPremise($address->getPremise())
            ->setStreet($address->getStreet())
            ->setThoroughfare($address->getThoroughfare())
            ->setPostTown($address->getPostTown())
            ->setCounty($address->getCounty())
            ->setPostcode($address->getPostcode())
            ->setCountry($address->getCountry());

        $residentialAddress = $personPsc->getResidentialAddress();
        $dataResidentialAddress = (new Address)
            ->setPremise($residentialAddress->getPremise())
            ->setStreet($residentialAddress->getStreet())
            ->setThoroughfare($residentialAddress->getThoroughfare())
            ->setPostTown($residentialAddress->getPostTown())
            ->setCounty($residentialAddress->getCounty())
            ->setPostcode($residentialAddress->getPostcode())
            ->setCountry($residentialAddress->getCountry());

        $person = $personPsc->getPerson();
        $personPscData = (new PersonPscData)
            ->setTitle($person->getTitle())
            ->setFirstName($person->getForename())
            ->setMiddleName($person->getMiddleName())
            ->setLastName($person->getSurname())
            ->setDateOfBirth($person->getDob() ? new DateTime($person->getDob()) : NULL)
            ->setNationality($person->getNationality())
            ->setCountryOfResidence($person->getCountryOfResidence())
            ->setServiceAddress($dataAddress)
            ->setResidentialAddress($dataResidentialAddress)
            ->setOwnershipOfShares($natureOfControl->getOwnershipOfShares())
            ->setOwnershipOfVotingRights($natureOfControl->getOwnershipOfVotingRights())
            ->setRightToAppointAndRemoveDirectors($natureOfControl->getRightToAppointAndRemoveDirectors())
            ->setSignificantInfluenceOrControl($natureOfControl->getSignificantInfluenceOrControl());

        return $personPscData;
    }

    /**
     * @param PersonPsc $personPsc
     * @return PersonPscData
     */
    public static function fromRegister(PersonPsc $personPsc)
    {
        $dataAddress = (new Address)
            ->setPremise($personPsc->getPremise())
            ->setStreet($personPsc->getStreet())
            ->setThoroughfare($personPsc->getThoroughfare())
            ->setPostTown($personPsc->getPostTown())
            ->setCounty($personPsc->getCounty())
            ->setPostcode($personPsc->getPostcode())
            ->setCountry($personPsc->getCountry());

        $dataResidentialAddress = (new Address)
            ->setPremise($personPsc->getResidentialPremise())
            ->setStreet($personPsc->getResidentialStreet())
            ->setThoroughfare($personPsc->getResidentialThoroughfare())
            ->setPostTown($personPsc->getResidentialPostTown())
            ->setCounty($personPsc->getResidentialCounty())
            ->setPostcode($personPsc->getResidentialPostcode())
            ->setCountry($personPsc->getResidentialCountry());

        $personPscData = (new PersonPscData)
            ->setTitle($personPsc->getTitle())
            ->setFirstName($personPsc->getForename())
            ->setMiddleName($personPsc->getMiddleName())
            ->setLastName($personPsc->getSurname())
            ->setDateOfBirth($personPsc->getDob() ?:  NULL)
            ->setNationality($personPsc->getNationality())
            ->setCountryOfResidence($personPsc->getCountryOfResidence())
            ->setServiceAddress($dataAddress)
            ->setResidentialAddress($dataResidentialAddress)
            ->setOwnershipOfShares($personPsc->getOwnershipOfShares())
            ->setOwnershipOfVotingRights($personPsc->getOwnershipOfVotingRights())
            ->setRightToAppointAndRemoveDirectors($personPsc->getRightToAppointAndRemoveDirectors())
            ->setSignificantInfluenceOrControl($personPsc->getSignificantInfluenceOrControl());

        return $personPscData;
    }

    /**
     * @param IPersonPsc $personPsc
     * @param PersonPscData $data
     * @return IPersonPsc mutated $personPsc
     * @throws Exception
     */
    public static function merge(IPersonPsc $personPsc, PersonPscData $data)
    {
        $person = $personPsc->getPerson();
        $person->setTitle($data->getTitle());
        $person->setForename($data->getFirstName());
        $person->setMiddleName($data->getMiddleName());
        $person->setSurname($data->getLastName());
        $person->setDob($data->getDateOfBirth() ? $data->getDateOfBirth()->format('Y-m-d') : NULL);
        $person->setNationality($data->getNationality());
        $person->setCountryOfResidence($data->getCountryOfResidence());
        $personPsc->setPerson($person);

        $dataAddress = $data->getServiceAddress();
        $address = $personPsc->getAddress();
        $address->setPremise($dataAddress->getPremise());
        $address->setStreet($dataAddress->getStreet());
        $address->setThoroughfare($dataAddress->getThoroughfare());
        $address->setPostTown($dataAddress->getPostTown());
        $address->setCounty($dataAddress->getCounty());
        $address->setPostcode($dataAddress->getPostcode());
        $address->setCountry($dataAddress->getCountry());
        $personPsc->setAddress($address);

        //todo: where is different res address handled? Is this fine?
        $dataAddress = $data->getResidentialAddress()->getCountry() ? $data->getResidentialAddress() : $data->getServiceAddress();
        $residentialAddress = $personPsc->getAddress();
        $residentialAddress->setPremise($dataAddress->getPremise());
        $residentialAddress->setStreet($dataAddress->getStreet());
        $residentialAddress->setThoroughfare($dataAddress->getThoroughfare());
        $residentialAddress->setPostTown($dataAddress->getPostTown());
        $residentialAddress->setCounty($dataAddress->getCounty());
        $residentialAddress->setPostcode($dataAddress->getPostcode());
        $residentialAddress->setCountry($dataAddress->getCountry());
        $personPsc->setResidentialAddress($residentialAddress);

        $natureOfControl = $personPsc->getNatureOfControl();
        $natureOfControl->setOwnershipOfShares($data->getOwnershipOfShares());
        $natureOfControl->setOwnershipOfVotingRights($data->getOwnershipOfVotingRights());
        $natureOfControl->setRightToAppointAndRemoveDirectors($data->getRightToAppointAndRemoveDirectors());
        $natureOfControl->setSignificantInfluenceOrControl($data->getSignificantInfluenceOrControl());
        $personPsc->setNatureOfControl($natureOfControl);

        return $personPsc;
    }

    /**
     * @param PersonPsc $personPsc
     * @param PersonPscData $data
     * @return PersonPsc
     * @throws Exception
     */
    public static function mergeRegister(PersonPsc $personPsc, PersonPscData $data)
    {
        $personPsc->setTitle($data->getTitle() ?: NULL);
        $personPsc->setForename($data->getFirstName() ?: NULL);
        $personPsc->setMiddleName($data->getMiddleName() ?: NULL);
        $personPsc->setSurname($data->getLastName() ?: NULL);
        $personPsc->setDob($data->getDateOfBirth() ?: NULL);
        $personPsc->setNationality($data->getNationality() ?: NULL);
        $personPsc->setCountryOfResidence($data->getCountryOfResidence() ?: NULL);

        $dataAddress = $data->getServiceAddress();
        $personPsc->setPremise($dataAddress->getPremise() ?: NULL);
        $personPsc->setStreet($dataAddress->getStreet() ?: NULL);
        $personPsc->setThoroughfare($dataAddress->getThoroughfare() ?: NULL);
        $personPsc->setPostTown($dataAddress->getPostTown() ?: NULL);
        $personPsc->setCounty($dataAddress->getCounty() ?: NULL);
        $personPsc->setPostcode($dataAddress->getPostcode() ?: NULL);
        $personPsc->setCountry($dataAddress->getCountry() ?: NULL);

        //todo: where is different res address handled? Is this fine?
        $dataResidentialAddress = $data->getResidentialAddress()->getCountry() ? $data->getResidentialAddress() : $data->getServiceAddress();
        $personPsc->setResidentialPremise($dataResidentialAddress->getPremise() ?: NULL);
        $personPsc->setResidentialStreet($dataResidentialAddress->getStreet() ?: NULL);
        $personPsc->setResidentialThoroughfare($dataResidentialAddress->getThoroughfare() ?: NULL);
        $personPsc->setResidentialPostTown($dataResidentialAddress->getPostTown() ?: NULL);
        $personPsc->setResidentialCounty($dataResidentialAddress->getCounty() ?: NULL);
        $personPsc->setResidentialPostcode($dataResidentialAddress->getPostcode() ?: NULL);
        $personPsc->setResidentialCountry($dataResidentialAddress->getCountry() ?: NULL);

        $personPsc->setOwnershipOfShares($data->getOwnershipOfShares() ?: NULL);
        $personPsc->setOwnershipOfVotingRights($data->getOwnershipOfVotingRights() ?: NULL);
        $personPsc->setRightToAppointAndRemoveDirectors($data->getRightToAppointAndRemoveDirectors() ?: NULL);
        $personPsc->setSignificantInfluenceOrControl($data->getSignificantInfluenceOrControl() ?: NULL);

        return $personPsc;
    }
}
