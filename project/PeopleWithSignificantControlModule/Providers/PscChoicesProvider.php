<?php

namespace PeopleWithSignificantControl\Providers;

use Entities\Company;
use NatureOfControl;

class PscChoicesProvider
{
    const PERSON = 1;
    const CORPORATE = 2;
    const LEGAL_PERSON = 3;

    /**
     * @var array
     */
    public static $entityNames = [
        self::PERSON => 'person',
        self::CORPORATE => 'corporate',
        self::LEGAL_PERSON => 'legal person',
    ];

    /**
     * @param int $pscType
     * @param string $companyType
     * @return array
     */
    public function getNatureOfControlsTemplateStructure($pscType, $companyType)
    {
        $entity = PscChoicesProvider::$entityNames[$pscType];
        $sharesType = $companyType == Company::COMPANY_CATEGORY_LLP ? 'surplus assets' : 'shares';

        $groups = [
            'ownership_of_shares' => [
                'title' => "Ownership of {$sharesType}",
                'groups' => [
                    [
                        'title' => "The {$entity} holds {$sharesType}",
                        'options' => $this->getOwnershipOfShares($companyType),
                    ],
                    [
                        'title' => "The members of the firm hold {$sharesType}",
                        'description' => "The {$entity} has right to exercise, or actually exercises, significant influence or control over the activities of a firm that, under that law by which it is governed, is not a legal person, and the members of the firm",
                        'options' => $this->getOwnershipOfSharesAsFirm($companyType),
                    ],
                    [
                        'title' => "The trustees of a trust hold {$sharesType}",
                        'description' => "The {$entity} has the right to exercise, or actually exercises, significant influence or control over the activities of a trust, and the trustees of that trust",
                        'options' => $this->getOwnershipOfSharesAsTrust($companyType),
                    ],
                ]
            ],
            'ownership_of_voting_rights' => [
                'title' => 'Ownership of voting rights',
                'groups' => [
                    [
                        'title' => "The {$entity} holds voting rights",
                        'options' => $this->getOwnershipOfVotingRights(),
                    ],
                    [
                        'title' => 'The members of a firm hold voting rights',
                        'description' => "The {$entity} has right to exercise, or actually exercises, significant influence or control over the activities of a firm that, under that law by which it is governed, is not a legal person, and the members of the firm",
                        'options' => $this->getOwnershipOfVotingRightsAsFirm(),
                    ],
                    [
                        'title' => 'The trustees of a trust hold voting rights',
                        'description' => "The {$entity} has the right to exercise, or actually exercises, significant influence or control over the activities of a trust, and the trustees of that trust",
                        'options' => $this->getOwnershipOfVotingRightsAsTrust(),
                    ],
                ]
            ],
            'right_to_appoint_and_remove_directors' => [
                'title' => 'Right to appoint or remove the majority of the board of directors',
                'groups' => [
                    [
                        'options' => $this->getRightToAppointAndRemoveDirectors($pscType, $companyType),
                    ],
                ]
            ],
            'significant_influence_or_control' => [
                'formControl' => 'significant_influence_or_control',
                'title' => 'Has significant influence or control',
                'groups' => [
                    [
                        'options' => $this->getSignificantInfluenceOrControl($pscType),
                    ],
                ]
            ]
        ];

        if ($companyType == Company::COMPANY_CATEGORY_BYGUAR) {
            unset($groups['ownership_of_shares']);
        }

        return $groups;
    }

    /**
     * @param int $pscType
     * @return string
     */
    public function getNatureOfControlsTemplateDescription($pscType)
    {
        $entity = PscChoicesProvider::$entityNames[$pscType];

        return "What is the nature of the {$entity}'s control over this company?";
    }

    /**
     * @param string $companyType
     * @return array
     */
    public function getOwnershipOfShares($companyType)
    {
        if ($companyType == Company::COMPANY_CATEGORY_LLP) {
            return [
                NatureOfControl::RIGHTTOSHARESURPLUSASSETS_25_TO_50_PERCENT => 'more than 25% but not more than 50% of surplus assets',
                NatureOfControl::RIGHTTOSHARESURPLUSASSETS_50_TO_75_PERCENT => 'more than 50% but not more than 75% of surplus assets',
                NatureOfControl::RIGHTTOSHARESURPLUSASSETS_75_TO_100_PERCENT => 'more than 75% of surplus assets',
            ];
        } else {
            return [
                NatureOfControl::OWNERSHIPOFSHARES_25_TO_50_PERCENT => 'more than 25% but not more than 50% of shares',
                NatureOfControl::OWNERSHIPOFSHARES_50_TO_75_PERCENT => 'more than 50% but not more than 75% of shares',
                NatureOfControl::OWNERSHIPOFSHARES_75_TO_100_PERCENT => 'more than 75% of shares',
            ];
        }
    }

    /**
     * @param string $companyType
     * @return array
     */
    public function getOwnershipOfSharesAsFirm($companyType)
    {
        if ($companyType == Company::COMPANY_CATEGORY_LLP) {
            return [
                NatureOfControl::RIGHTTOSHARESURPLUSASSETS_25_TO_50_PERCENT_AS_FIRM => 'more than 25% but not more than 50% of surplus assets',
                NatureOfControl::RIGHTTOSHARESURPLUSASSETS_50_TO_75_PERCENT_AS_FIRM => 'more than 50% but not more than 75% of surplus assets',
                NatureOfControl::RIGHTTOSHARESURPLUSASSETS_75_TO_100_PERCENT_AS_FIRM => 'more than 75% of surplus assets',
            ];
        } else {
            return [
                NatureOfControl::OWNERSHIPOFSHARES_25_TO_50_PERCENT_AS_FIRM => 'hold more than 25% but not more than 50% of shares',
                NatureOfControl::OWNERSHIPOFSHARES_50_TO_75_PERCENT_AS_FIRM => 'hold more than 50% but not more than 75% of shares',
                NatureOfControl::OWNERSHIPOFSHARES_75_TO_100_PERCENT_AS_FIRM => 'hold more than 75% of shares',
            ];
        }
    }

    /**
     * @param string $companyType
     * @return array
     */
    public function getOwnershipOfSharesAsTrust($companyType)
    {
        if ($companyType == Company::COMPANY_CATEGORY_LLP) {
            return [
                NatureOfControl::RIGHTTOSHARESURPLUSASSETS_25_TO_50_PERCENT_AS_TRUST => 'more than 25% but not more than 50% of surplus assets',
                NatureOfControl::RIGHTTOSHARESURPLUSASSETS_50_TO_75_PERCENT_AS_TRUST => 'more than 50% but not more than 75% of surplus assets',
                NatureOfControl::RIGHTTOSHARESURPLUSASSETS_75_TO_100_PERCENT_AS_TRUST => 'more than 75% of surplus assets',
            ];
        } else {
            return [
                NatureOfControl::OWNERSHIPOFSHARES_25_TO_50_PERCENT_AS_TRUST => 'hold more than 25% but not more than 50% of shares',
                NatureOfControl::OWNERSHIPOFSHARES_50_TO_75_PERCENT_AS_TRUST => 'hold more than 50% but not more than 75% of shares',
                NatureOfControl::OWNERSHIPOFSHARES_75_TO_100_PERCENT_AS_TRUST => 'hold more than 75% of shares',
            ];
        }
    }

    /**
     * @param string $companyType
     * @return array
     */
    public function getAllOwnershipOfShares($companyType)
    {
        return $this->getOwnershipOfShares($companyType) + $this->getOwnershipOfSharesAsFirm($companyType) + $this->getOwnershipOfSharesAsTrust($companyType);
    }

    /**
     * @return array
     */
    public function getOwnershipOfVotingRights()
    {
        return [
            NatureOfControl::VOTINGRIGHTS_25_TO_50_PERCENT => 'more than 25% but not more than 50% of voting rights',
            NatureOfControl::VOTINGRIGHTS_50_TO_75_PERCENT => 'more than 50% but not more than 75% of voting rights',
            NatureOfControl::VOTINGRIGHTS_75_TO_100_PERCENT => 'more than 75% of voting rights',
        ];
    }

    /**
     * @return array
     */
    public function getOwnershipOfVotingRightsAsFirm()
    {
        return [
            NatureOfControl::VOTINGRIGHTS_25_TO_50_PERCENT_AS_FIRM => 'hold more than 25% but not more than 50% of voting rights',
            NatureOfControl::VOTINGRIGHTS_50_TO_75_PERCENT_AS_FIRM => 'hold more than 50% but not more than 75% of voting rights',
            NatureOfControl::VOTINGRIGHTS_75_TO_100_PERCENT_AS_FIRM => 'hold more than 75% of voting rights',
        ];
    }

    /**
     * @return array
     */
    public function getOwnershipOfVotingRightsAsTrust()
    {
        return [
            NatureOfControl::VOTINGRIGHTS_25_TO_50_PERCENT_AS_TRUST => 'hold more than 25% but not more than 50% of voting rights',
            NatureOfControl::VOTINGRIGHTS_50_TO_75_PERCENT_AS_TRUST => 'hold more than 50% but not more than 75% of voting rights',
            NatureOfControl::VOTINGRIGHTS_75_TO_100_PERCENT_AS_TRUST => 'hold more than 75% of voting rights',
        ];
    }

    /**
     * @return array
     */
    public function getAllOwnershipOfVotingRights()
    {
        return $this->getOwnershipOfVotingRights() + $this->getOwnershipOfVotingRightsAsFirm() + $this->getOwnershipOfVotingRightsAsTrust();
    }

    /**
     * @param int $pscType
     * @param string $companyType
     * @return array
     */
    public function getRightToAppointAndRemoveDirectors($pscType, $companyType)
    {
        $entity = PscChoicesProvider::$entityNames[$pscType];

        if ($companyType == Company::COMPANY_CATEGORY_LLP) {
            return [
                NatureOfControl::RIGHTTOAPPOINTANDREMOVEMEMBERS => "The {$entity} has the right to appoint or remove the majority of the board of directors of the company",
                NatureOfControl::RIGHTTOAPPOINTANDREMOVEMEMBERS_AS_FIRM => "The {$entity} has control over the firm and members of that firm (in their capacity as such) hold the right to appoint of remove the majority of the board of directors of the company",
                NatureOfControl::RIGHTTOAPPOINTANDREMOVEMEMBERS_AS_TRUST => "The {$entity} has control over the truste and the trustees of that trust (in their capacity as such) hold the right to appoint or remove the majority of the board of directors of the company",
            ];
        } else {
            return [
                NatureOfControl::RIGHTTOAPPOINTANDREMOVEDIRECTORS => "The {$entity} has the right to appoint or remove the majority of the board of directors of the company",
                NatureOfControl::RIGHTTOAPPOINTANDREMOVEDIRECTORS_AS_FIRM => "The {$entity} has control over the firm and members of that firm (in their capacity as such) hold the right to appoint of remove the majority of the board of directors of the company",
                NatureOfControl::RIGHTTOAPPOINTANDREMOVEDIRECTORS_AS_TRUST => "The {$entity} has control over the truste and the trustees of that trust (in their capacity as such) hold the right to appoint or remove the majority of the board of directors of the company",
            ];
        }
    }

    /**
     * @param int $pscType
     * @return array
     */
    public function getSignificantInfluenceOrControl($pscType)
    {
        $entity = PscChoicesProvider::$entityNames[$pscType];

        return [
            NatureOfControl::SIGINFLUENCECONTROL => "The {$entity} has significant influence or control over the company",
            NatureOfControl::SIGINFLUENCECONTROL_AS_FIRM => "The {$entity} has control over the firm and members of that firm (in their capacity as such) have significant influence or control over the company",
            NatureOfControl::SIGINFLUENCECONTROL_AS_TRUST => "The {$entity} has control over the trust and the trustees of that trust (in their capacity as such) have significant influence or control over the company",
        ];
    }

    /**
     * @return string
     */
    public function getNoPscReason()
    {
        return 'The company knows or has reasonable cause to believe that there is no registrable person or registrable relevant legal entity in relation to the company';
    }
}
