<?php

namespace PeopleWithSignificantControl\Forms;

class LegalPersonPscData
{
    /**
     * @var string
     */
    private $legalPersonName;

    /**
     * @var string
     */
    private $governingLaw;

    /**
     * @var string
     */
    private $legalForm;

    /**
     * @var Address
     */
    private $serviceAddress;

    /**
     * @var string
     */
    private $ownershipOfShares;

    /**
     * @var string
     */
    private $ownershipOfVotingRights;

    /**
     * @var string
     */
    private $rightToAppointDirector;

    /**
     * @var string
     */
    private $significantInfluenceOrControl;

    /**
     * @return string
     */
    public function getLegalPersonName()
    {
        return $this->legalPersonName;
    }

    /**
     * @param string $legalPersonName
     * @return LegalPersonPscData
     */
    public function setLegalPersonName($legalPersonName)
    {
        $this->legalPersonName = $legalPersonName;

        return $this;
    }

    /**
     * @return string
     */
    public function getGoverningLaw()
    {
        return $this->governingLaw;
    }

    /**
     * @param string $governingLaw
     * @return LegalPersonPscData
     */
    public function setGoverningLaw($governingLaw)
    {
        $this->governingLaw = $governingLaw;

        return $this;
    }

    /**
     * @return string
     */
    public function getLegalForm()
    {
        return $this->legalForm;
    }

    /**
     * @param string $legalForm
     * @return LegalPersonPscData
     */
    public function setLegalForm($legalForm)
    {
        $this->legalForm = $legalForm;

        return $this;
    }

    /**
     * @return Address
     */
    public function getServiceAddress()
    {
        return $this->serviceAddress ?: new Address;
    }

    /**
     * @param Address $serviceAddress
     * @return LegalPersonPscData
     */
    public function setServiceAddress($serviceAddress)
    {
        $this->serviceAddress = $serviceAddress;

        return $this;
    }

    /**
     * @return string
     */
    public function getOwnershipOfShares()
    {
        return $this->ownershipOfShares;
    }

    /**
     * @param string $ownershipOfShares
     * @return LegalPersonPscData
     */
    public function setOwnershipOfShares($ownershipOfShares)
    {
        $this->ownershipOfShares = $ownershipOfShares;

        return $this;
    }

    /**
     * @return string
     */
    public function getOwnershipOfVotingRights()
    {
        return $this->ownershipOfVotingRights;
    }

    /**
     * @param string $ownershipOfVotingRights
     * @return LegalPersonPscData
     */
    public function setOwnershipOfVotingRights($ownershipOfVotingRights)
    {
        $this->ownershipOfVotingRights = $ownershipOfVotingRights;

        return $this;
    }

    /**
     * @return string
     */
    public function getRightToAppointAndRemoveDirectors()
    {
        return $this->rightToAppointDirector;
    }

    /**
     * @param string $rightToAppointDirector
     * @return LegalPersonPscData
     */
    public function setRightToAppointAndRemoveDirectors($rightToAppointDirector)
    {
        $this->rightToAppointDirector = $rightToAppointDirector;

        return $this;
    }

    /**
     * @return string
     */
    public function getSignificantInfluenceOrControl()
    {
        return $this->significantInfluenceOrControl;
    }

    /**
     * @param string $significantInfluenceOrControl
     * @return LegalPersonPscData
     */
    public function setSignificantInfluenceOrControl($significantInfluenceOrControl)
    {
        $this->significantInfluenceOrControl = $significantInfluenceOrControl;

        return $this;
    }
}
