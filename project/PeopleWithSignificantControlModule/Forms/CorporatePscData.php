<?php

namespace PeopleWithSignificantControl\Forms;

class CorporatePscData
{
    /**
     * @var string
     */
    private $corporateName;

    /**
     * @var string
     */
    private $placeRegistered;

    /**
     * @var string
     */
    private $registrationNumber;

    /**
     * @var string
     */
    private $governingLaw;

    /**
     * @var string
     */
    private $legalForm;

    /**
     * @var string
     */
    private $countryRegistered;

    /**
     * @var Address
     */
    private $serviceAddress;

    /**
     * @var string
     */
    private $ownershipOfShares;

    /**
     * @var string
     */
    private $ownershipOfVotingRights;

    /**
     * @var string
     */
    private $rightToAppointDirector;

    /**
     * @var string
     */
    private $significantInfluenceOrControl;

    /**
     * @return string
     */
    public function getCorporateName()
    {
        return $this->corporateName;
    }

    /**
     * @param string $corporateName
     * @return CorporatePscData
     */
    public function setCorporateName($corporateName)
    {
        $this->corporateName = $corporateName;

        return $this;
    }

    /**
     * @return string
     */
    public function getPlaceRegistered()
    {
        return $this->placeRegistered;
    }

    /**
     * @param string $placeRegistered
     * @return CorporatePscData
     */
    public function setPlaceRegistered($placeRegistered)
    {
        $this->placeRegistered = $placeRegistered;

        return $this;
    }

    /**
     * @return string
     */
    public function getRegistrationNumber()
    {
        return $this->registrationNumber;
    }

    /**
     * @param string $registrationNumber
     * @return CorporatePscData
     */
    public function setRegistrationNumber($registrationNumber)
    {
        $this->registrationNumber = $registrationNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getGoverningLaw()
    {
        return $this->governingLaw;
    }

    /**
     * @param string $governingLaw
     * @return CorporatePscData
     */
    public function setGoverningLaw($governingLaw)
    {
        $this->governingLaw = $governingLaw;

        return $this;
    }

    /**
     * @return string
     */
    public function getLegalForm()
    {
        return $this->legalForm;
    }

    /**
     * @param string $legalForm
     * @return CorporatePscData
     */
    public function setLegalForm($legalForm)
    {
        $this->legalForm = $legalForm;

        return $this;
    }

    /**
     * @return string
     */
    public function getCountryRegistered()
    {
        return $this->countryRegistered;
    }

    /**
     * @param string $countryRegistered
     * @return CorporatePscData
     */
    public function setCountryRegistered($countryRegistered)
    {
        $this->countryRegistered = $countryRegistered;

        return $this;
    }

    /**
     * @return Address
     */
    public function getServiceAddress()
    {
        return $this->serviceAddress ?: new Address;
    }

    /**
     * @param Address $serviceAddress
     * @return CorporatePscData
     */
    public function setServiceAddress($serviceAddress)
    {
        $this->serviceAddress = $serviceAddress;

        return $this;
    }

    /**
     * @return string
     */
    public function getOwnershipOfShares()
    {
        return $this->ownershipOfShares;
    }

    /**
     * @param string $ownershipOfShares
     * @return CorporatePscData
     */
    public function setOwnershipOfShares($ownershipOfShares)
    {
        $this->ownershipOfShares = $ownershipOfShares;

        return $this;
    }

    /**
     * @return string
     */
    public function getOwnershipOfVotingRights()
    {
        return $this->ownershipOfVotingRights;
    }

    /**
     * @param string $ownershipOfVotingRights
     * @return CorporatePscData
     */
    public function setOwnershipOfVotingRights($ownershipOfVotingRights)
    {
        $this->ownershipOfVotingRights = $ownershipOfVotingRights;

        return $this;
    }

    /**
     * @return string
     */
    public function getRightToAppointAndRemoveDirectors()
    {
        return $this->rightToAppointDirector;
    }

    /**
     * @param string $rightToAppointDirector
     * @return CorporatePscData
     */
    public function setRightToAppointAndRemoveDirectors($rightToAppointDirector)
    {
        $this->rightToAppointDirector = $rightToAppointDirector;

        return $this;
    }

    /**
     * @return string
     */
    public function getSignificantInfluenceOrControl()
    {
        return $this->significantInfluenceOrControl;
    }

    /**
     * @param string $significantInfluenceOrControl
     * @return CorporatePscData
     */
    public function setSignificantInfluenceOrControl($significantInfluenceOrControl)
    {
        $this->significantInfluenceOrControl = $significantInfluenceOrControl;

        return $this;
    }
}
