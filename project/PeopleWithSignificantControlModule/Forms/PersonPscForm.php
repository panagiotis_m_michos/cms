<?php

namespace PeopleWithSignificantControl\Forms;

use Admin\CHForm;
use CFPscsControler;
use Company;
use CompanyIncorporation;
use DateSelect;
use DateTime;
use FApplication;
use FControl;
use FForm;
use Html;
use PeopleWithSignificantControl\Providers\PscChoicesProvider;
use Person;
use Radio;
use ServiceAddress;
use Text;

class PersonPscForm
{
    /**
     * @var Company
     */
    private $company;

    /**
     * @var PersonPscData
     */
    private $data;

    /**
     * @var callable
     */
    private $onValidCallback;

    /**
     * @var PscChoicesProvider
     */
    private $pscChoicesProvider;

    /**
     * @var array
     */
    private $prefillOfficers;

    /**
     * @var array
     */
    private $prefillAddresses;

    /**
     * @var bool
     */
    private $isAdminView;

    /**
     * @param PscChoicesProvider $pscChoicesProvider
     * @param Company $company
     * @param PersonPscData $data
     * @param callable $onValidCallback (PscData)
     * @param array $prefillOfficers
     * @param array $prefillAddresses
     * @param bool $isAdminView
     */
    public function __construct(PscChoicesProvider $pscChoicesProvider, Company $company, PersonPscData $data, callable $onValidCallback, $prefillOfficers = NULL, $prefillAddresses = NULL, $isAdminView = FALSE)
    {
        $this->company = $company;
        $this->data = $data;
        $this->onValidCallback = $onValidCallback;
        $this->pscChoicesProvider = $pscChoicesProvider;
        $this->prefillOfficers = $prefillOfficers;
        $this->prefillAddresses = $prefillAddresses;
        $this->isAdminView = $isAdminView;
    }

    /**
     * @return CHForm
     */
    public function getForm()
    {
        $serviceAddress = $this->data->getServiceAddress();
        $residentialAddress = $this->data->getResidentialAddress();
        $fieldValues = [
            'title' => $this->data->getTitle(),
            'forename' => $this->data->getFirstName(),
            'middle_name' => $this->data->getMiddleName(),
            'surname' => $this->data->getLastName(),
            'dob' => $this->data->getDateOfBirth() ? $this->data->getDateOfBirth()->format('Y-m-d') : NULL,
            'nationality' => $this->data->getNationality(),
            'country_of_residence' => $this->data->getCountryOfResidence(),

            'premise' => $serviceAddress->getPremise(),
            'street' => $serviceAddress->getStreet(),
            'thoroughfare' => $serviceAddress->getThoroughfare(),
            'post_town' => $serviceAddress->getPostTown(),
            'county' => $serviceAddress->getCounty(),
            'postcode' => $serviceAddress->getPostcode(),
            'country' => $serviceAddress->getCountry(),

            'residential_premise' => $residentialAddress->getPremise(),
            'residential_street' => $residentialAddress->getStreet(),
            'residential_thoroughfare' => $residentialAddress->getThoroughfare(),
            'residential_post_town' => $residentialAddress->getPostTown(),
            'residential_county' => $residentialAddress->getCounty(),
            'residential_postcode' => $residentialAddress->getPostcode(),
            'residential_country' => $residentialAddress->getCountry(),

            'ownership_of_shares' => $this->data->getOwnershipOfShares(),
            'ownership_of_voting_rights' => $this->data->getOwnershipOfVotingRights(),
            'right_to_appoint_and_remove_directors' => $this->data->getRightToAppointAndRemoveDirectors(),
            'significant_influence_or_control' => $this->data->getSignificantInfluenceOrControl(),
        ];

        $form = $this->buildForm($fieldValues);
        $form->onValid = [$this, 'onValid'];

        return $form;
    }

    /**
     * @param FForm $form
     */
    public function onValid(FForm $form)
    {
        $data = $form->getValues();

        $serviceAddress = new Address;
        $serviceAddress
            ->setPremise($data['premise'])
            ->setStreet($data['street'])
            ->setThoroughfare($data['thoroughfare'])
            ->setPostTown($data['post_town'])
            ->setCounty($data['county'])
            ->setPostcode($data['postcode'])
            ->setCountry($data['country']);

        if ($this->company->getServiceAddress() && $data['ourServiceAddress']) {
            $msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());
            $serviceAddress
                ->setPremise($msgServiceAdress->premise)
                ->setStreet($msgServiceAdress->street)
                ->setThoroughfare($msgServiceAdress->thoroughfare)
                ->setPostTown($msgServiceAdress->post_town)
                ->setCounty($msgServiceAdress->county)
                ->setPostcode($msgServiceAdress->postcode)
                ->setCountry($msgServiceAdress->country);
        }

        $residentialAddress = new Address;
        if (!array_key_exists('residentialAddress', $data) || $data['residentialAddress']) {
            $residentialAddress
                ->setPremise($data['residential_premise'])
                ->setStreet($data['residential_street'])
                ->setThoroughfare($data['residential_thoroughfare'])
                ->setPostTown($data['residential_post_town'])
                ->setCounty($data['residential_county'])
                ->setPostcode($data['residential_postcode'])
                ->setCountry($data['residential_country']);
        } else {
            $residentialAddress
                ->setPremise($data['premise'])
                ->setStreet($data['street'])
                ->setThoroughfare($data['thoroughfare'])
                ->setPostTown($data['post_town'])
                ->setCounty($data['county'])
                ->setPostcode($data['postcode'])
                ->setCountry($data['country']);
        }

        $personPscData = new PersonPscData();
        $personPscData
            ->setTitle($data['title'])
            ->setFirstName($data['forename'])
            ->setMiddleName($data['middle_name'])
            ->setLastName($data['surname'])
            ->setNationality($data['nationality'])
            ->setDateOfBirth(new DateTime($data['dob']))
            ->setCountryOfResidence($data['country_of_residence'])
            ->setOwnershipOfShares($data['ownership_of_shares'])
            ->setOwnershipOfVotingRights($data['ownership_of_voting_rights'])
            ->setRightToAppointAndRemoveDirectors($data['right_to_appoint_and_remove_directors'])
            ->setSignificantInfluenceOrControl($data['significant_influence_or_control'])
            ->setResidentialAddress($residentialAddress)
            ->setServiceAddress($serviceAddress);


        $cb = $this->onValidCallback;
        $cb($personPscData, $form);
    }

    /**
     * @param array $fieldValues
     * @return FForm
     */
    private function buildForm(array $fieldValues)
    {
        $companyType = $this->company->getLastIncorporationFormSubmission()
            ? $this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType()
            : $this->company->getType();

        $prefillableOfficers = $this->prefillOfficers ?: CFPscsControler::getPrefillOfficers($this->company);
        $prefillableAddresses = $this->prefillAddresses ?: CFPscsControler::getPrefillAdresses($this->company);
        $form = new CHForm('pscPerson');
        $isEmpty = empty($fieldValues['forename']);

        // prefill
        $form->addFieldset('Prefill');
        $form->addSelect('prefillOfficers', 'Select an existing appointment to autocomplete these details', $prefillableOfficers['select'])
            ->setFirstOption('--- Select --');

        // person
        $form->addFieldset('Person');
        $form->addSelect('title', 'Title', Person::$titles)
            ->setFirstOption('--- Select ---');
        $form->addText('forename', 'First name *')
            ->addRule(FForm::Required, 'Please provide first name')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "First name can't be more than 50 characters", 50);
        $form->addText('middle_name', 'Middle name')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "Middle name can't be more than 50 characters", 50)
            ->addRule('MinLength', "Please provide full middle name.", 2);
        $form->addText('surname', 'Last name *')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule(FForm::Required, 'Please provide last name')
            ->addRule('MaxLength', "Last name can't be more than 160 characters", 160);
        $form->add('DateSelect', 'dob', 'Date Of Birth *')
            ->addRule(FForm::Required, 'Please provide dob')
            ->addRule(
                [$this, 'Validator_personDOB'],
                ['DOB is not a valid date.', 'PSC has to be older than %d years.'],
                16
            )
            ->withFirstOptions()
            ->setStartYear(1901)
            ->setEndYear(date('Y') - 1);

        $form->addText('nationality', 'Nationality *')
            ->addRule(FForm::Required, 'Please provide nationality')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL);
        $form->addText('country_of_residence', 'Country Of Residence *')
            ->addRule(FForm::Required, 'Please provide Country of Residence')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL);

        if ($this->company->getServiceAddress()) {
            $form->addFieldset('Use Our Service Address service');
            $checkbox = $form->addCheckbox('ourServiceAddress', 'Service Address service  ', 1);

            if ($this->data->getServiceAddress()) {
                $msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());
                if ($this->data->getServiceAddress()->getPostcode() == $msgServiceAdress->postcode) {
                    $checkbox->setValue(1);
                }
            }
        }

        $form->addFieldset('Prefill');
        $form->addSelect('prefillAddress', 'Prefill Address', $prefillableAddresses['select'])
            ->setFirstOption('--- Select ---');

        $form->addFieldset('Address');
        $form->addText('premise', 'Building name/number *')
            ->addRule([$this, 'Validator_requiredAddress'], 'Please provide Building name/number')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "Building name/number can't be more than 50 characters", 50);
        $form->addText('street', 'Street *')
            ->addRule([$this, 'Validator_requiredAddress'], 'Please provide Street')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "Street can't be more than 50 characters", 50);
        $form->addText('thoroughfare', 'Address 3')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "Address 3 can't be more than 50 characters", 50);
        $form->addText('post_town', 'Town *')
            ->addRule([$this, 'Validator_requiredAddress'], 'Please provide Town')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "Town can't be more than 50 characters", 50);
        $form->addText('county', 'County')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "County can't be more than 50 characters", 50);

        $postcode = $form->addText('postcode', 'Postcode *')
            ->addRule([$this, 'Validator_requiredAddress'], 'Please provide Postcode')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "Postcode can't be more than 15 characters", 15);
        if (!$this->isAdminView) {
            $postcode->addRule(
                [$this, 'Validator_serviceAddressPostCode'],
                'You cannot use our postcode for this address without first purchasing the '
                . Html::el('a')
                    ->setText('Service Address Service')
                    ->href(FApplication::$router->link(476))
                    ->render() . '. NB. This is different to the registered office service.'
            );
        }
        
        $form->addSelect('country', 'Country *', \Address::$countries)
            ->addRule([$this, 'Validator_requiredAddress'], 'Please provide Country');

        $form->addFieldset('Residential Address');

        if ($isEmpty) {
            $form->addCheckbox('residentialAddress', 'Different address: ', 1);
        }

        $form->addText('residential_premise', 'Building name/number *')
            ->addRule([$this, 'Validator_requiredServiceAddress'], 'Please provide Building name/number', $isEmpty)
            ->addRule('MaxLength', "Building name/number can't be more than 50 characters", 50)
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL);
        $form->addText('residential_street', 'Street *')
            ->addRule([$this, 'Validator_requiredServiceAddress'], 'Please provide street', $isEmpty)
            ->addRule('MaxLength', "Street can't be more than 50 characters", 50)
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL);
        $form->addText('residential_thoroughfare', 'Address 3')
            ->addRule('MaxLength', "Address 3 can't be more than 50 characters", 50)
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL);
        $form->addText('residential_post_town', 'Town *')
            ->addRule([$this, 'Validator_requiredServiceAddress'], 'Please provide Town', $isEmpty)
            ->addRule('MaxLength', "Town can't be more than 50 characters", 50)
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL);
        $form->addText('residential_county', 'County')
            ->addRule('MaxLength', "County can't be more than 50 characters", 50)
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL);
        $form->addText('residential_postcode', 'Postcode *')
            ->addRule([$this, 'Validator_requiredServiceAddress'], 'Please provide Postcode', $isEmpty)
            ->addRule('MaxLength', "Postcode can't be more than 15 characters", 15)
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule(
                FForm::REGEXP,
                'You cannot use our postcode for residential address',
                ['#(ec1v 4pw|ec1v4pw)#i', TRUE]
            );
        $form->addSelect('residential_country', 'Country *', \Address::$countries)
            ->addRule([$this, 'Validator_requiredServiceAddress'], 'Please provide Country', $isEmpty)
            ->addRule('MaxLength', "Country can't be more than 50 characters", 50)
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL);

        $form->addFieldset('Nature of Control:');
        if ($companyType != CompanyIncorporation::BY_GUARANTEE) {
            $form->addRadio(
                'ownership_of_shares',
                'The person holds shares',
                $this->pscChoicesProvider->getAllOwnershipOfShares($companyType)
            );
        }

        $form->addRadio(
            'ownership_of_voting_rights',
            'The person holds voting rights',
            $this->pscChoicesProvider->getAllOwnershipOfVotingRights()
        );

        $form->addRadio(
            'right_to_appoint_and_remove_directors',
            'Right to appoint or remove the majority of the board of directors',
            $this->pscChoicesProvider->getRightToAppointAndRemoveDirectors(PscChoicesProvider::PERSON, $companyType)
        );

        $form
            ->addRadio(
                'significant_influence_or_control',
                'Has significant influence or control',
                $this->pscChoicesProvider->getSignificantInfluenceOrControl(PscChoicesProvider::PERSON)
            )
            ->addRule(
                [$this, 'Validator_natureOfControl'],
                'Please select the nature of control. At least one condition must be selected.',
                $companyType
            );

        $form->setInitValues($fieldValues);
        $form->onValid = [$this, 'onValid'];

        $form->start();
        return $form;
    }

    /**
     * @param Radio $control
     * @param string $error
     * @param string $companyType
     * @return bool
     */
    public function Validator_natureOfControl(Radio $control, $error, $companyType)
    {
        /** @var CHForm $form */
        $form = $control->owner;

        $toCheck = [
            $form['ownership_of_voting_rights']->getValue(),
            $form['right_to_appoint_and_remove_directors']->getValue(),
            $form['significant_influence_or_control']->getValue(),
        ];

        if ($companyType != CompanyIncorporation::BY_GUARANTEE) {
            $toCheck[] = $form['ownership_of_shares']->getValue();
        }

        if (empty(array_filter($toCheck))) {
            return $error;
        }

        return TRUE;
    }

    /**
     * @param DateSelect $control
     * @param array $error
     * @param int $minYears
     * @return bool|string
     */
    public function Validator_personDOB(DateSelect $control, $error, $minYears)
    {
        $value = $control->getValue();

        if (strpos($value, '?') !== FALSE) {
            return 'Please select your date of birth';
        }

        // check if dob is valid day
        $temp = explode('-', $value);
        if (checkdate($temp[1], $temp[2], $temp[0]) === FALSE) {
            return $error[0];
        }

        // check if director is older than 16 years
        $dob = date('Ymd', strtotime($value));
        $years = floor((date("Ymd") - $dob) / 10000);
        if ($years < $minYears) {
            return sprintf($error[1], $minYears);
        }
        return TRUE;
    }

    /**
     * @param FControl $control
     * @param string $error
     * @return bool|string
     */
    public function Validator_requiredAddress(FControl $control, $error)
    {
        $value = $control->getValue();
        //service address service validator
        if (isset($control->owner['ourServiceAddress'])) {
            if ($control->owner['ourServiceAddress']->getValue() != 1 && empty($value)) {
                return $error;
            }
            //without service address
        } else {
            if (empty($value)) {
                return $error;
            }
        }
        return TRUE;
    }

    /**
     * @param $control
     * @param $error
     * @param $isEmpty
     * @return bool
     */
    public function Validator_requiredServiceAddress($control, $error, $isEmpty)
    {
        $value = $control->getValue();
        if ($isEmpty) {
            if ($control->owner['residentialAddress']->getValue() == 1 && empty($value)
                || (strstr(strtoupper($control->owner['postcode']->getValue()), 'EC1V4PW')
                || strstr(strtoupper($control->owner['postcode']->getValue()), 'EC1V 4PW'))
                && empty($value)
            ) {
                return $error;
            }
        } else {
            if (empty($value)) {
                return $error;
            }
        }

        return TRUE;
    }

    /**
     * @param FControl $control
     * @param string $error
     * @return bool|string
     */
    public function Validator_serviceAddressPostCode(FControl $control, $error)
    {
        $value = $control->getValue();
        $postcode = str_replace(' ', '', mb_strtolower($value));
        $blacklistedPostcodes = array('n17gu', 'ec1v4pw');
        if (!in_array($postcode, $blacklistedPostcodes)) {
            return TRUE;
        }
        return $error;
    }
}
