<?php

namespace PeopleWithSignificantControl\Forms;

use Admin\CHForm;
use Company;
use FForm;
use PeopleWithSignificantControl\Providers\PscChoicesProvider;

class PscFormFactory
{
    /**
     * @var PscChoicesProvider
     */
    private $pscChoicesProvider;

    /**
     * @param PscChoicesProvider $pscChoicesProvider
     */
    public function __construct(PscChoicesProvider $pscChoicesProvider)
    {
        $this->pscChoicesProvider = $pscChoicesProvider;
    }

    /**
     * @param Company $company
     * @param callable $onValidCallback (PersonPscData)
     * @param array $prefillOfficers
     * @param array $prefillAddresses
     * @param bool $isAdminView
     * @return FForm
     */
    public function getPersonAddForm(Company $company, callable $onValidCallback, $prefillOfficers = NULL, $prefillAddresses = NULL, $isAdminView = FALSE)
    {
        $pscForm = new PersonPscForm($this->pscChoicesProvider, $company, new PersonPscData(), $onValidCallback, $prefillOfficers, $prefillAddresses, $isAdminView);
        $form = $pscForm->getForm();
        $form->addFieldset('Action');
        $form->addSubmit('continue', 'Continue')
            ->class('btn_submit fright mbottom');

        return $form;
    }

    /**
     * @param Company $company
     * @param PersonPscData $data
     * @param callable $onValidCallback (PersonPscData)
     * @param array $prefillOfficers
     * @param array $prefillAddresses
     * @param bool $isAdminView
     * @return CHForm
     */
    public function getPersonEditForm(Company $company, PersonPscData $data, callable $onValidCallback, $prefillOfficers = NULL, $prefillAddresses = NULL, $isAdminView = FALSE)
    {
        $pscForm = new PersonPscForm($this->pscChoicesProvider, $company, $data, $onValidCallback, $prefillOfficers, $prefillAddresses, $isAdminView);
        $form = $pscForm->getForm();
        $form->addFieldset('Action');
        $form->addSubmit('continue', 'Continue')
            ->class('btn_submit fright mbottom');

        return $form;
    }

    /**
     * @param Company $company
     * @param callable $onValidCallback (CorporatePscData)
     * @param array $prefillOfficers
     * @param array $prefillAddresses
     * @param bool $isAdminView
     * @return FForm
     */
    public function getCorporateAddForm(Company $company, callable $onValidCallback, $prefillOfficers = NULL, $prefillAddresses = NULL, $isAdminView = FALSE)
    {
        $pscForm = new CorporatePscForm($this->pscChoicesProvider, $company, new CorporatePscData(), $onValidCallback, $prefillOfficers, $prefillAddresses, $isAdminView);
        $form = $pscForm->getForm();
        $form->addFieldset('Action');
        $form->addSubmit('continue', 'Continue')
            ->class('btn_submit fright mbottom');

        return $form;
    }

    /**
     * @param Company $company
     * @param CorporatePscData $data
     * @param callable $onValidCallback (PersonPscData)
     * @param array $prefillOfficers
     * @param array $prefillAddresses
     * @param bool $isAdminView
     * @return CHForm
     */
    public function getCorporateEditForm(Company $company, CorporatePscData $data, callable $onValidCallback, $prefillOfficers = NULL, $prefillAddresses = NULL, $isAdminView = FALSE)
    {
        $pscForm = new CorporatePscForm($this->pscChoicesProvider, $company, $data, $onValidCallback, $prefillOfficers, $prefillAddresses, $isAdminView);
        $form = $pscForm->getForm();
        $form->addFieldset('Action');
        $form->addSubmit('continue', 'Continue')
            ->class('btn_submit fright mbottom');

        return $form;
    }

    /**
     * @param Company $company
     * @param callable $onValidCallback (LegalPersonPscData)
     * @param array $prefillOfficers
     * @param array $prefillAddresses
     * @param bool $isAdminView
     * @return FForm
     */
    public function getLegalPersonAddForm(Company $company, callable $onValidCallback, $prefillOfficers = NULL, $prefillAddresses = NULL, $isAdminView = FALSE)
    {
        $pscForm = new LegalPersonPscForm($this->pscChoicesProvider, $company, new LegalPersonPscData(), $onValidCallback, $prefillOfficers, $prefillAddresses, $isAdminView);
        $form = $pscForm->getForm();
        $form->addFieldset('Action');
        $form->addSubmit('continue', 'Continue')
            ->class('btn_submit fright mbottom');

        return $form;
    }

    /**
     * @param Company $company
     * @param LegalPersonPscData $data
     * @param callable $onValidCallback (LegalPersonPscData)
     * @param array $prefillOfficers
     * @param array $prefillAddresses
     * @param bool $isAdminView
     * @return CHForm
     */
    public function getLegalPersonEditForm(Company $company, LegalPersonPscData $data, callable $onValidCallback, $prefillOfficers = NULL, $prefillAddresses = NULL, $isAdminView = FALSE)
    {
        $pscForm = new LegalPersonPscForm($this->pscChoicesProvider, $company, $data, $onValidCallback, $prefillOfficers, $prefillAddresses, $isAdminView);
        $form = $pscForm->getForm();
        $form->addFieldset('Action');
        $form->addSubmit('continue', 'Continue')
            ->class('btn_submit fright mbottom');

        return $form;
    }
}
