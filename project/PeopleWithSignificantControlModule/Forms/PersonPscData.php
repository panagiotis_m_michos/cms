<?php

namespace PeopleWithSignificantControl\Forms;

use DateTime;

class PersonPscData
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $middleName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var DateTime
     */
    private $dateOfBirth;

    /**
     * @var string
     */
    private $nationality;

    /**
     * @var string
     */
    private $countryOfResidence;

    /**
     * @var Address
     */
    private $serviceAddress;

    /**
     * @var Address
     */
    private $residentialAddress;

    /**
     * @var string
     */
    private $ownershipOfShares;

    /**
     * @var string
     */
    private $ownershipOfVotingRights;

    /**
     * @var string
     */
    private $rightToAppointDirector;

    /**
     * @var string
     */
    private $significantInfluenceOrControl;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return PersonPscData
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return PersonPscData
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * @param string $middleName
     * @return PersonPscData
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return PersonPscData
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * @param DateTime $dateOfBirth
     * @return PersonPscData
     */
    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    /**
     * @return string
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * @param string $nationality
     * @return PersonPscData
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;

        return $this;
    }

    /**
     * @return string
     */
    public function getCountryOfResidence()
    {
        return $this->countryOfResidence;
    }

    /**
     * @param string $countryOfResidence
     * @return PersonPscData
     */
    public function setCountryOfResidence($countryOfResidence)
    {
        $this->countryOfResidence = $countryOfResidence;

        return $this;
    }

    /**
     * @return Address
     */
    public function getServiceAddress()
    {
        return $this->serviceAddress ?: new Address;
    }

    /**
     * @param Address $serviceAddress
     * @return PersonPscData
     */
    public function setServiceAddress($serviceAddress)
    {
        $this->serviceAddress = $serviceAddress;

        return $this;
    }

    /**
     * @return Address
     */
    public function getResidentialAddress()
    {
        return $this->residentialAddress ?: new Address;
    }

    /**
     * @param Address $residentialAddress
     * @return PersonPscData
     */
    public function setResidentialAddress($residentialAddress)
    {
        $this->residentialAddress = $residentialAddress;

        return $this;
    }

    /**
     * @return string
     */
    public function getOwnershipOfShares()
    {
        return $this->ownershipOfShares;
    }

    /**
     * @param string $ownershipOfShares
     * @return PersonPscData
     */
    public function setOwnershipOfShares($ownershipOfShares)
    {
        $this->ownershipOfShares = $ownershipOfShares;

        return $this;
    }

    /**
     * @return string
     */
    public function getOwnershipOfVotingRights()
    {
        return $this->ownershipOfVotingRights;
    }

    /**
     * @param string $ownershipOfVotingRights
     * @return PersonPscData
     */
    public function setOwnershipOfVotingRights($ownershipOfVotingRights)
    {
        $this->ownershipOfVotingRights = $ownershipOfVotingRights;

        return $this;
    }

    /**
     * @return string
     */
    public function getRightToAppointAndRemoveDirectors()
    {
        return $this->rightToAppointDirector;
    }

    /**
     * @param string $rightToAppointDirector
     * @return PersonPscData
     */
    public function setRightToAppointAndRemoveDirectors($rightToAppointDirector)
    {
        $this->rightToAppointDirector = $rightToAppointDirector;

        return $this;
    }

    /**
     * @return string
     */
    public function getSignificantInfluenceOrControl()
    {
        return $this->significantInfluenceOrControl;
    }

    /**
     * @param string $significantInfluenceOrControl
     * @return PersonPscData
     */
    public function setSignificantInfluenceOrControl($significantInfluenceOrControl)
    {
        $this->significantInfluenceOrControl = $significantInfluenceOrControl;

        return $this;
    }
}
