<?php

namespace PeopleWithSignificantControl\Forms;

use Admin\CHForm;
use CFPscsControler;
use Company;
use CompanyIncorporation;
use DateSelect;
use FApplication;
use FControl;
use FForm;
use Html;
use PeopleWithSignificantControl\Providers\PscChoicesProvider;
use Radio;
use ServiceAddress;
use Text;

class CorporatePscForm
{
    /**
     * @var Company
     */
    private $company;

    /**
     * @var CorporatePscData
     */
    private $data;

    /**
     * @var callable
     */
    private $onValidCallback;

    /**
     * @var PscChoicesProvider
     */
    private $pscChoicesProvider;

    /**
     * @var array
     */
    private $prefillOfficers;

    /**
     * @var array
     */
    private $prefillAddresses;

    /**
     * @var bool
     */
    private $isAdminView;

    /**
     * PersonPscForm constructor.
     * @param PscChoicesProvider $pscChoicesProvider
     * @param Company $company
     * @param CorporatePscData $data
     * @param callable $onValidCallback (PscData)
     * @param array $prefillOfficers
     * @param array $prefillAddresses
     * @param bool $isAdminView
     */
    public function __construct(PscChoicesProvider $pscChoicesProvider, Company $company, CorporatePscData $data, callable $onValidCallback, $prefillOfficers = NULL, $prefillAddresses = NULL, $isAdminView = FALSE)
    {
        $this->company = $company;
        $this->data = $data;
        $this->onValidCallback = $onValidCallback;
        $this->pscChoicesProvider = $pscChoicesProvider;
        $this->prefillOfficers = $prefillOfficers;
        $this->prefillAddresses = $prefillAddresses;
        $this->isAdminView = $isAdminView;
    }

    /**
     * @return CHForm
     */
    public function getForm()
    {
        $serviceAddress = $this->data->getServiceAddress();

        $fieldValues = [
            'corporate_name' => $this->data->getCorporateName(),
            'place_registered' => $this->data->getPlaceRegistered(),
            'registration_number' => $this->data->getRegistrationNumber(),
            'law_governed' => $this->data->getGoverningLaw(),
            'legal_form' => $this->data->getLegalForm(),
            'country_or_state' => $this->data->getCountryRegistered(),

            'premise' => $serviceAddress->getPremise(),
            'street' => $serviceAddress->getStreet(),
            'thoroughfare' => $serviceAddress->getThoroughfare(),
            'post_town' => $serviceAddress->getPostTown(),
            'county' => $serviceAddress->getCounty(),
            'postcode' => $serviceAddress->getPostcode(),
            'country' => $serviceAddress->getCountry(),

            'ownership_of_shares' => $this->data->getOwnershipOfShares(),
            'ownership_of_voting_rights' => $this->data->getOwnershipOfVotingRights(),
            'right_to_appoint_and_remove_directors' => $this->data->getRightToAppointAndRemoveDirectors(),
            'significant_influence_or_control' => $this->data->getSignificantInfluenceOrControl(),
        ];

        $form = $this->buildForm($fieldValues);
        $form->onValid = [$this, 'onValid'];

        return $form;
    }

    /**
     * @param FForm $form
     */
    public function onValid(FForm $form)
    {
        $data = $form->getValues();

        $serviceAddress = new Address;
        $serviceAddress
            ->setPremise($data['premise'])
            ->setStreet($data['street'])
            ->setThoroughfare($data['thoroughfare'])
            ->setPostTown($data['post_town'])
            ->setCounty($data['county'])
            ->setPostcode($data['postcode'])
            ->setCountry($data['country']);

        if ($this->company->getServiceAddress() && $data['ourServiceAddress']) {
            $msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());
            $serviceAddress
                ->setPremise($msgServiceAdress->premise)
                ->setStreet($msgServiceAdress->street)
                ->setThoroughfare($msgServiceAdress->thoroughfare)
                ->setPostTown($msgServiceAdress->post_town)
                ->setCounty($msgServiceAdress->county)
                ->setPostcode($msgServiceAdress->postcode)
                ->setCountry($msgServiceAdress->country);
        }

        $personPscData = new CorporatePscData();
        $personPscData
            ->setCorporateName($data['corporate_name'])
            ->setPlaceRegistered($data['place_registered'])
            ->setRegistrationNumber($data['registration_number'])
            ->setGoverningLaw($data['law_governed'])
            ->setLegalForm($data['legal_form'])
            ->setCountryRegistered($data['country_or_state'])
            ->setOwnershipOfShares($data['ownership_of_shares'])
            ->setOwnershipOfVotingRights($data['ownership_of_voting_rights'])
            ->setRightToAppointAndRemoveDirectors($data['right_to_appoint_and_remove_directors'])
            ->setSignificantInfluenceOrControl($data['significant_influence_or_control'])
            ->setServiceAddress($serviceAddress);

        $cb = $this->onValidCallback;
        $cb($personPscData, $form);
    }

    /**
     * @param array $fieldValues
     * @return FForm
     */
    private function buildForm(array $fieldValues)
    {
        $companyType = $this->company->getLastIncorporationFormSubmission()
            ? $this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType()
            : $this->company->getType();

        $prefillableOfficers = $this->prefillOfficers ?: CFPscsControler::getPrefillOfficers($this->company);
        $prefillableAddresses = $this->prefillAddresses ?: CFPscsControler::getPrefillAdresses($this->company);
        $form = new CHForm('pscCorporate');

        $form->addFieldset('Prefill');
        $form->addSelect('prefillOfficers', 'Select an existing appointment to autocomplete these details', $prefillableOfficers['select'])
            ->setFirstOption('--- Select --');

        $form->addFieldset('Corporate');
        $form->addText('corporate_name', 'Company name *')
            ->addRule(FForm::Required, 'Please provide company name')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL);
        $form->addText('place_registered', 'Place registered');
        $form->addText('registration_number', 'Registration number');
        $form->addText('law_governed', 'Governing law *')
            ->addRule(FForm::Required, 'Please provide Governing law');
        $form->addText('legal_form', 'Legal Form *')
            ->addRule(FForm::Required, 'Please provide Legal Form');
        $form->addText('country_or_state', 'Country registered');

        if ($this->company->getServiceAddress()) {
            $form->addFieldset('Use Our Service Address service');
            $checkbox = $form->addCheckbox('ourServiceAddress', 'Service Address service  ', 1);

            //check if service address was set for psc
            if (isset($this->psc)) {
                $fields = $this->psc->getFields();
                $msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());
                if ($fields['postcode'] == $msgServiceAdress->postcode) {
                    $checkbox->setValue(1);
                }
            }
        }

        $form->addFieldset('Prefill');
        $form->addSelect('prefillAddress', 'Prefill Address', $prefillableAddresses['select'])
            ->setFirstOption('--- Select ---');

        $form->addFieldset('Address');
        $form->addText('premise', 'Building name/number *')
            ->addRule([$this, 'Validator_requiredAddress'], 'Please provide Building name/number')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "Building name/number can't be more than 50 characters", 50);
        $form->addText('street', 'Street *')
            ->addRule([$this, 'Validator_requiredAddress'], 'Please provide Street')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "Street can't be more than 50 characters", 50);
        $form->addText('thoroughfare', 'Address 3')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "Address 3 can't be more than 50 characters", 50);
        $form->addText('post_town', 'Town *')
            ->addRule([$this, 'Validator_requiredAddress'], 'Please provide Town')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "Town can't be more than 50 characters", 50);
        $form->addText('county', 'County')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "County can't be more than 50 characters", 50);

        $postcode = $form->addText('postcode', 'Postcode *')
            ->addRule([$this, 'Validator_requiredAddress'], 'Please provide Postcode')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "Postcode can't be more than 15 characters", 15);
        if (!$this->isAdminView) {
            $postcode->addRule(
                [$this, 'Validator_serviceAddressPostCode'],
                'You cannot use our postcode for this address without first purchasing the '
                . Html::el('a')
                    ->setText('Service Address Service')
                    ->href(FApplication::$router->link(476))
                    ->render() . '. NB. This is different to the registered office service.'
            );
        }

        $form->addSelect('country', 'Country *', \Address::$countries)
            ->addRule([$this, 'Validator_requiredAddress'], 'Please provide Country');

        $form->addFieldset('Nature of Control:');
        if ($companyType != CompanyIncorporation::BY_GUARANTEE) {
            $form->addRadio(
                'ownership_of_shares',
                'The person holds shares',
                $this->pscChoicesProvider->getAllOwnershipOfShares($companyType)
            );
        }

        $form->addRadio(
            'ownership_of_voting_rights',
            'The person holds voting rights',
            $this->pscChoicesProvider->getAllOwnershipOfVotingRights()
        );

        $form->addRadio(
            'right_to_appoint_and_remove_directors',
            'Right to appoint or remove the majority of the board of directors',
            $this->pscChoicesProvider->getRightToAppointAndRemoveDirectors(PscChoicesProvider::CORPORATE, $companyType)
        );

        $form
            ->addRadio(
                'significant_influence_or_control',
                'Has significant influence or control',
                $this->pscChoicesProvider->getSignificantInfluenceOrControl(PscChoicesProvider::CORPORATE)
            )
            ->addRule(
                [$this, 'Validator_natureOfControl'],
                'Please select the nature of control. At least one condition must be selected.',
                $companyType
            );


        $form->setInitValues($fieldValues);
        $form->onValid = [$this, 'onValid'];

        $form->start();
        return $form;
    }

    /**
     * @param $control
     * @param $error
     * @param $isEmpty
     * @return bool
     */
    public function Validator_requiredServiceAddress($control, $error, $isEmpty)
    {
        $value = $control->getValue();
        if ($isEmpty) {
            if ($control->owner['residentialAddress']->getValue() == 1 && empty($value)
                || (strstr(strtoupper($control->owner['postcode']->getValue()), 'EC1V4PW')
                || strstr(strtoupper($control->owner['postcode']->getValue()), 'EC1V 4PW'))
                && empty($value)
            ) {
                return $error;
            }
        } else {
            if (empty($value)) {
                return $error;
            }
        }

        return TRUE;
    }

    /**
     * @param Radio $control
     * @param string $error
     * @param string $companyType
     * @return bool
     */
    public function Validator_natureOfControl(Radio $control, $error, $companyType)
    {
        /** @var CHForm $form */
        $form = $control->owner;

        $toCheck = [
            $form['ownership_of_voting_rights']->getValue(),
            $form['right_to_appoint_and_remove_directors']->getValue(),
            $form['significant_influence_or_control']->getValue(),
        ];

        if ($companyType != CompanyIncorporation::BY_GUARANTEE) {
            $toCheck[] = $form['ownership_of_shares']->getValue();
        }

        if (empty(array_filter($toCheck))) {
            return $error;
        }

        return TRUE;
    }

    /**
     * @param FControl $control
     * @param string $error
     * @return bool|string
     */
    public function Validator_requiredAddress(FControl $control, $error)
    {
        $value = $control->getValue();
        //service address service validator
        if (isset($control->owner['ourServiceAddress'])) {
            if ($control->owner['ourServiceAddress']->getValue() != 1 && empty($value)) {
                return $error;
            }
            //without service address
        } else {
            if (empty($value)) {
                return $error;
            }
        }
        return TRUE;
    }

    /**
     * @param FControl $control
     * @param string $error
     * @return bool|string
     */
    public function Validator_serviceAddressPostCode(FControl $control, $error)
    {
        $value = $control->getValue();
        $postcode = str_replace(' ', '', mb_strtolower($value));
        $blacklistedPostcodes = array('n17gu', 'ec1v4pw');
        if (!in_array($postcode, $blacklistedPostcodes)) {
            return TRUE;
        }
        return $error;
    }
}
