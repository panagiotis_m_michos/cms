<?php
namespace PeopleWithSignificantControl\Interfaces;

use Address;
use Authentication;
use CompanyIdentification;
use Corporate;
use Identification;
use NatureOfControl;

interface ICorporatePsc extends IPsc
{
    /**
     * @param Corporate $corporate
     * @return void
     */
    public function setCorporate(Corporate $corporate);

    /**
     * @return Corporate
     */
    public function getCorporate();

    /**
     * @param string $type
     * @return Identification
     */
    public function getIdentification($type = NULL);

    /**
     * @param Identification $identification
     * @access public
     * @return void
     */
    public function setIdentification(Identification $identification);

    /**
     * @param Address $address
     */
    public function setAddress(Address $address);

    /**
     * @return Address
     */
    public function getAddress();

    /**
     * setAuthentication
     *
     * @param Authentication $authentication
     * @access public
     * @return void
     */
    public function setAuthentication(Authentication $authentication);

    /**
     * getAuthentication
     *
     * @access public
     * @return Authentication
     */
    public function getAuthentication();

    /**
     * @param bool $consentToAct
     */
    public function setConsentToAct($consentToAct);

    /**
     * @return bool
     */
    public function hasConsentToAct();
}
