<?php

namespace PeopleWithSignificantControl\Interfaces;

use Address;
use Authentication;
use CompanyIdentification;
use Corporate;
use Identification;
use NatureOfControl;
use Person;

interface IPersonPsc extends IPsc
{
    /**
     * @param Person $person
     */
    public function setPerson(Person $person);

    /**
     * @return Person
     */
    public function getPerson();

    /**
     * @param Address $address
     */
    public function setAddress(Address $address);

    /**
     * @return Address
     */
    public function getAddress();

    /**
     * @param Address $address
     */
    public function setResidentialAddress(Address $address);

    /**
     * @return Address
     */
    public function getResidentialAddress();

    /**
     * @param Authentication $authentication
     */
    public function setAuthentication(Authentication $authentication);

    /**
     * @return Authentication
     */
    public function getAuthentication();

    /**
     * @param bool $consentToAct
     */
    public function setConsentToAct($consentToAct);

    /**
     * @return bool
     */
    public function hasConsentToAct();
}
