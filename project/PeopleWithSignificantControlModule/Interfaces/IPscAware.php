<?php

namespace PeopleWithSignificantControl\Interfaces;

use CorporatePsc;
use LegalPersonPsc;
use PersonPsc;

interface IPscAware
{
    const NO_INDIVIDUAL_OR_ENTITY_WITH_SIGNFICANT_CONTROL = 'NO_INDIVIDUAL_OR_ENTITY_WITH_SIGNFICANT_CONTROL';
    const STEPS_TO_FIND_PSC_NOT_YET_COMPLETED = 'STEPS_TO_FIND_PSC_NOT_YET_COMPLETED';

    /**
     * @return string
     */
    public function getNoPscReason();

    /**
     * @return CorporatePsc[]
     */
    public function getCorporatePscs();

    /**
     * @return PersonPsc[]
     */
    public function getPersonPscs();

    /**
     * @return LegalPersonPsc[]
     */
    public function getLegalPersonPscs();
}
