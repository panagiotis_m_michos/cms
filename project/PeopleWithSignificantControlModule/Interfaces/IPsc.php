<?php

namespace PeopleWithSignificantControl\Interfaces;

use Address;
use Authentication;
use CompanyIdentification;
use Corporate;
use Identification;
use NatureOfControl;

interface IPsc
{
    const PSC_EXISTS_BUT_NOT_IDENTIFIED = 'PSC_EXISTS_BUT_NOT_IDENTIFIED';
    const PSC_DETAILS_NOT_CONFIRMED = 'PSC_DETAILS_NOT_CONFIRMED';
    const PSC_CONTACTED_BUT_NO_RESPONSE = 'PSC_CONTACTED_BUT_NO_RESPONSE';
    const RESTRICTIONS_NOTICE_ISSUED_TO_PSC = 'RESTRICTIONS_NOTICE_ISSUED_TO_PSC';

    /**
     * @return NatureOfControl
     */
    public function getNatureOfControl();

    /**
     * @param NatureOfControl $natureOfControl
     */
    public function setNatureOfControl(NatureOfControl $natureOfControl);

    /**
     * @return string
     */
    public function getPscStatementNotification();

    /**
     * @param string $pscStatementNotification
     */
    public function setPscStatementNotification($pscStatementNotification);
}
