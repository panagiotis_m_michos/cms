<?php

namespace PeopleWithSignificantControl\Views;

use CHAnnualReturn;
use CompanyFormationModule\Views\PscView;
use PeopleWithSignificantControl\Interfaces\IPscAware;

class AnnualReturnSummaryView
{
    /**
     * @var CHAnnualReturn
     */
    private $pscAware;

    /**
     * @param CHAnnualReturn $pscAware
     */
    public function __construct(CHAnnualReturn $pscAware)
    {
        $this->pscAware = $pscAware;
    }

    /**
     * @return AnnualReturnPscView[]
     */
    public function getPscs()
    {
        return array_merge($this->getCorporatePscs(), $this->getPersonPscs(), $this->getLegalPersonPscs());
    }

    /**
     * @return AnnualReturnPscView[]
     */
    public function getPersonPscs()
    {
        $persons = [];
        foreach ($this->pscAware->getPersonPscs() as $psc) {
            $persons[] = new AnnualReturnPscView($psc);
        }

        return $persons;
    }

    /**
     * @return AnnualReturnPscView[]
     */
    public function getCorporatePscs()
    {
        $corporates = [];
        foreach ($this->pscAware->getCorporatePscs() as $psc) {
            $corporates[] = new AnnualReturnPscView($psc);
        }

        return $corporates;
    }


    /**
     * @return AnnualReturnPscView[]
     */
    public function getLegalPersonPscs()
    {
        $legalPersons = [];
        foreach ($this->pscAware->getLegalPersonPscs() as $psc) {
            $legalPersons[] = new AnnualReturnPscView($psc);
        }

        return $legalPersons;
    }

    /**
     * @return bool
     */
    public function hasPscs()
    {
        return !empty($this->getPscs());
    }

    /**
     * @return bool
     */
    public function hasNoPscReason()
    {
        return !empty($this->pscAware->getNoPscReason());
    }

    /**
     * @return string
     */
    public function getNoPscReason()
    {
        $texts = [
            IPscAware::NO_INDIVIDUAL_OR_ENTITY_WITH_SIGNFICANT_CONTROL => 'The company knows or has reasonable cause to believe that there is no registrable person or registrable relevant legal entity in relation to the company.',
        ];

        return isset($texts[$this->pscAware->getNoPscReason()]) ? $texts[$this->pscAware->getNoPscReason()] : '';
    }
}
