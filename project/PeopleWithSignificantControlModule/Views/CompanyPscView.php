<?php

namespace PeopleWithSignificantControl\Views;

use CompanyCorporatePsc;
use CompanyLegalPersonPsc;
use CompanyPersonPsc;
use CompanyFormationModule\Views\PscView;
use PeopleWithSignificantControl\Interfaces\IPscAware;

class CompanyPscView extends PscView
{
    //todo: psc - protected? lebo v PscView mam tiez $psc, problem to asi nie je, ale dump blbol
    /**
     * @var CompanyPersonPsc|CompanyCorporatePsc|CompanyLegalPersonPsc
     */
    protected $psc;

    /**
     * @param CompanyPersonPsc|CompanyCorporatePsc|CompanyLegalPersonPsc $psc
     */
    public function __construct($psc)
    {
        parent::__construct($psc);
        $this->psc = $psc;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->psc->getId();
    }

    /**
     * @return bool
     */
    public function hasPscStatementNotification()
    {
        return !empty($this->psc->getPscStatementNotification());
    }

    /**
     * @return string
     */
    public function getPscStatementNotification()
    {
        $texts = [
            IPscAware::NO_INDIVIDUAL_OR_ENTITY_WITH_SIGNFICANT_CONTROL => 'The company knows or has reasonable cause to believe that there is no registrable person or registrable relevant legal entity in relation to the company.',
            IPscAware::STEPS_TO_FIND_PSC_NOT_YET_COMPLETED => 'The company has not entered the particulars of any registrable person or registrable relevant legal entity and has not yet completed taking reasonable steps to find out if there is anyone who is a registrable person or a registrable relevant legal entity in relation to the company under section 790D of the Act',
        ];

        return isset($texts[$this->psc->getPscStatementNotification()]) ? $texts[$this->psc->getPscStatementNotification()] : '';
    }
}
