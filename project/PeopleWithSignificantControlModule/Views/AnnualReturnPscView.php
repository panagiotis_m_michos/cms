<?php

namespace PeopleWithSignificantControl\Views;

use AnnualReturnCorporatePsc;
use AnnualReturnLegalPersonPsc;
use AnnualReturnPersonPsc;
use CompanyFormationModule\Views\PscView;

class AnnualReturnPscView extends PscView
{
    /**
     * @var AnnualReturnCorporatePsc|AnnualReturnPersonPsc|AnnualReturnLegalPersonPsc
     */
    protected $psc;

    /**
     * @param AnnualReturnPersonPsc|AnnualReturnCorporatePsc|AnnualReturnLegalPersonPsc $psc
     */
    public function __construct($psc)
    {
        parent::__construct($psc);
        $this->psc = $psc;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->psc->getId();
    }

    /**
     * @return bool
     */
    public function isAlreadyExistingPsc()
    {
        return $this->psc->isAlreadyExistingPsc();
    }

    /**
     * @return AnnualReturnPersonPsc|AnnualReturnCorporatePsc|AnnualReturnLegalPersonPsc
     */
    public function unwrap()
    {
        return $this->psc;
    }
}
