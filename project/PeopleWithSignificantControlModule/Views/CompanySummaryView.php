<?php

namespace PeopleWithSignificantControl\Views;

use Company;
use PeopleWithSignificantControl\Interfaces\IPscAware;

class CompanySummaryView
{
    /**
     * @var Company
     */
    private $company;

    /**
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @return CompanyPscView[]
     */
    public function getPscs()
    {
        return array_merge($this->getPersonPscs(), $this->getCorporatePscs(), $this->getLegalPersonPscs());
    }

    /**
     * @return CompanyPscView[]
     */
    public function getPersonPscs()
    {
        $persons = [];
        foreach ($this->company->getPersonPscs() as $psc) {
            $persons[] = new CompanyPscView($psc);
        }

        return $persons;
    }

    /**
     * @return CompanyPscView[]
     */
    public function getCorporatePscs()
    {
        $corporates = [];
        foreach ($this->company->getCorporatePscs() as $psc) {
            $corporates[] = new CompanyPscView($psc);
        }

        return $corporates;
    }

    /**
     * @return CompanyPscView[]
     */
    public function getLegalPersonPscs()
    {
        $legalEntities = [];
        foreach ($this->company->getLegalPersonPscs() as $psc) {
            $legalEntities[] = new CompanyPscView($psc);
        }

        return $legalEntities;
    }

    /**
     * @return bool
     */
    public function hasPscs()
    {
        return !empty($this->getPscs());
    }

    /**
     * @return bool
     */
    public function hasNoPscReason()
    {
        return !empty($this->company->getNoPscReason());
    }

    /**
     * @return string
     */
    public function getNoPscReason()
    {
        $texts = [
            IPscAware::NO_INDIVIDUAL_OR_ENTITY_WITH_SIGNFICANT_CONTROL => 'The company knows or has reasonable cause to believe that there is no registrable person or registrable relevant legal entity in relation to the company.',
            IPscAware::STEPS_TO_FIND_PSC_NOT_YET_COMPLETED => 'The company has not entered the particulars of any registrable person or registrable relevant legal entity and has not yet completed taking reasonable steps to find out if there is anyone who is a registrable person or a registrable relevant legal entity in relation to the company under section 790D of the Act',
        ];

        return isset($texts[$this->company->getNoPscReason()]) ? $texts[$this->company->getNoPscReason()] : '';
    }
}
