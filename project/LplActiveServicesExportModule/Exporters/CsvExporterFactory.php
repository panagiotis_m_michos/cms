<?php

namespace LplActiveServicesExportModule\Exporters;

use Goodby\CSV\Export\Standard\Exporter as CsvExporter;
use Goodby\CSV\Export\Standard\ExporterConfig;
use Nette\Object;

class CsvExporterFactory extends Object
{
    /**
     * @var array
     */
    public static $header = array(
        'Company Number',
        'Has Registered Office',
        'Has Service Address'
    );

    /**
     * @return Exporter
     */
    public static function createExporter()
    {
        $config = new ExporterConfig();
        $config->setColumnHeaders(self::$header);
        return new CsvExporter($config);
    }
}
