<?php

namespace LplActiveServicesExportModule\Exporters;

use Goodby\CSV\Export\Standard\Collection\CallbackCollection;
use Goodby\CSV\Export\Standard\Exporter as CsvExporter;
use LplActiveServicesExportModule\Entities\ActiveService;
use LplActiveServicesExportModule\Repositories\ActiveServicesRepository;
use Symfony\Component\HttpFoundation\File\File;

class Exporter
{
    /**
     * @var ActiveServicesRepository
     */
    private $repository;

    /**
     * @var CsvExporter
     */
    private $exporter;

    /**
     * @var string
     */
    private $exportFilePath;

    /**
     * @param ActiveServicesRepository $repository
     * @param CsvExporter $exporter
     * @param string $exportFilePath
     */
    public function __construct(ActiveServicesRepository $repository, CsvExporter $exporter, $exportFilePath)
    {
        $this->repository = $repository;
        $this->exporter = $exporter;
        $this->exportFilePath = $exportFilePath;
    }

    /**
     * @return File
     */
    public function export()
    {
        $services = $this->repository->findAllIterator();
        $collection = new CallbackCollection(
            $services,
            function ($result) {
                /** @var ActiveService $activeService */
                $activeService = $result[0];
                $activeServiceData = $activeService->toArray();
                $activeServiceData['hasRegisteredOffice'] = $activeServiceData['hasRegisteredOffice'] ? 'true' : 'false';
                $activeServiceData['hasServiceAddress'] = $activeServiceData['hasServiceAddress'] ? 'true' : 'false';

                return $activeServiceData;
            }
        );

        $this->exporter->export($this->exportFilePath, $collection);

        return new File($this->exportFilePath, FALSE);
    }

    /**
     * @return string
     */
    public function getExportFilePath()
    {
        return $this->exportFilePath;
    }
}
