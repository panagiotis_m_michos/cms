<?php

namespace LplActiveServicesExportModule\Commands;

use Cron\Commands\CommandAbstract;
use Cron\INotifier;
use Gaufrette\Filesystem;
use LplActiveServicesExportModule\Exporters\Exporter;
use LplActiveServicesExportModule\Uploaders\Uploader;
use Psr\Log\LoggerInterface;

class Export extends CommandAbstract
{

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Exporter
     */
    private $exporter;

    /**
     * @var Uploader
     */
    private $uploader;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @param LoggerInterface $logger
     * @param INotifier $notifier
     * @param Exporter $exporter
     * @param Uploader $uploader
     * @param Filesystem $filesystem
     */
    public function __construct(LoggerInterface $logger, INotifier $notifier, Exporter $exporter, Uploader $uploader, Filesystem $filesystem)
    {
        $this->logger = $logger;
        $this->notifier = $notifier;
        $this->exporter = $exporter;
        $this->uploader = $uploader;
        $this->filesystem = $filesystem;
    }

    public function execute()
    {
        $file = $this->exporter->export();
        $this->uploader->upload($file);
        $this->filesystem->delete($file->getFilename());
        $this->logger->info('Exported CSV file has been uploaded to Google Drive.');
    }
}
