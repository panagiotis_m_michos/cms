<?php

namespace LplActiveServicesExportModule\Uploaders;

use Google_Http_MediaFileUpload;
use Google_Service_Drive;
use Google_Service_Drive_DriveFile;
use Google_Service_Drive_ParentReference;
use Symfony\Component\HttpFoundation\File\File;

class Uploader
{
    /**
     * @var Google_Service_Drive
     */
    private $drive;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $directoryId;

    /**
     * @param Google_Service_Drive $drive
     * @param string $name
     * @param string $directoryId
     */
    public function __construct(Google_Service_Drive $drive, $name = NULL, $directoryId = NULL)
    {
        $this->drive = $drive;
        $this->name = $name;
        $this->directoryId = $directoryId;
    }
    /**
     * @param File $localFile
     */
    public function upload(File $localFile)
    {
        $client = $this->drive->getClient();

        $file = new Google_Service_Drive_DriveFile();

        $file->title = $this->name ?: $localFile->getFilename();

        if ($this->directoryId) {
            $parent = new Google_Service_Drive_ParentReference();
            $parent->setId($this->directoryId);
            $file->setParents([$parent]);
        }

        $existingFileId = NULL;
        $existingFiles = $this->drive->files->listFiles(['q' => "title = '{$this->name}'", 'maxResults' => 1]);
        foreach ($existingFiles as $existingFile) {
            /** @var Google_Service_Drive_DriveFile $existingFile */
            $existingFileId = $existingFile->getId();
        }

        $client->setDefer(TRUE);
        if ($existingFileId) {
            $request = $this->drive->files->update($existingFileId, $file);
        } else {
            $request = $this->drive->files->insert($file);
        }

        $chunkSizeBytes = 1024 * 1024;
        $media = new Google_Http_MediaFileUpload(
            $client,
            $request,
            $localFile->getMimeType(),
            NULL,
            TRUE,
            $chunkSizeBytes
        );
        $media->setFileSize($localFile->getSize());

        $status = FALSE;
        $handle = fopen($localFile->getRealPath(), "rb");
        while (!$status && !feof($handle)) {
            $chunk = fread($handle, $chunkSizeBytes);
            $status = $media->nextChunk($chunk);
        }

        fclose($handle);
    }
}
