<?php

namespace LplActiveServicesExportModule\Entities;

use Doctrine\ORM\Mapping as Orm;

/**
 * @Orm\Entity(readOnly=true, repositoryClass="LplActiveServicesExportModule\Repositories\ActiveServicesRepository")
 * @Orm\Table(name="lpl_active_services_export")
 */
class ActiveService
{
    /**
     * @var string
     *
     * @Orm\Column(type="string")
     * @Orm\Id
     */
    private $companyNumber;

    /**
     * @var bool
     *
     * @Orm\Column(type="boolean")
     */
    private $hasRegisteredOffice;

    /**
     * @var bool
     *
     * @Orm\Column(type="boolean")
     */
    private $hasServiceAddress;

    /**
     * @param string $companyNumber
     * @param bool $hasRegisteredOffice
     * @param bool $hasServiceAddress
     */
    public function __construct($companyNumber, $hasRegisteredOffice, $hasServiceAddress)
    {
        $this->companyNumber = $companyNumber;
        $this->hasRegisteredOffice = $hasRegisteredOffice;
        $this->hasServiceAddress = $hasServiceAddress;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'companyNumber' => $this->getCompanyNumber(),
            'hasRegisteredOffice' => $this->hasRegisteredOffice(),
            'hasServiceAddress' => $this->hasServiceAddress(),
        ];
    }

    /**
     * @return string
     */
    public function getCompanyNumber()
    {
        return $this->companyNumber;
    }

    /**
     * @return bool
     */
    public function hasRegisteredOffice()
    {
        return $this->hasRegisteredOffice;
    }

    /**
     * @return bool
     */
    public function hasServiceAddress()
    {
        return $this->hasServiceAddress;
    }
}
