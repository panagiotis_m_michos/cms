<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    CosecAdminControler.php 2010-07-27 divak@gmail.com
 */

class CosecAdminControler extends PageAdminControler
{
	/**
	 * @var string
	 */
	static public $handleObject = 'CosecModel';
	
	/**
	 * @var CosecModel
	 */
	public $node;
	
	/** @var array */
	protected $tabs = array(
		'page' => 'Page',
		'properties' => 'Properties',
		'details' => 'Details',
		'menu' => 'Menu',
		'seo' => 'Seo'
	);
	
	public function beforePrepare()
	{
		parent::beforePrepare();
		$this->templateExtendedFrom[] = 'PageAdmin';
	}
	
	
	/******************************************* properties *******************************************/
	
	
	public function renderProperties()
	{
        $slideshowForm = new CosecAdminForm($this->node->getId() . '_slideshow');
        $slideshowForm->startup($this, $this->node, array($this, 'Form_propertiesFormSubmitted'));
        $this->template->form = $slideshowForm;
	}
	
	 /**
     * @param CosecAdminForm $form
     */
	public function Form_propertiesFormSubmitted(CosecAdminForm $form)
	{
		try {
			$form->process();
			$this->flashMessage('Data has been updated');
			$this->redirect('properties');
		} catch (Exception $e) {
		    $this->flashMessage($e->getMessage(), 'error');
		 	$this->redirect('properties');
		}
	}
}