<?php
class ImagesAdminControler extends PageAdminControler
{
    /** @var string */
    static public $icon = 'imgfolder.gif';
    
    /** @var array */
    protected $tabs = array(
        'grid' => 'Grid',
        'list' => 'List'
    );

    /** @var FImage */
    private $image;
    
    
    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'NameAdmin';
        // file
        if (isset($this->get['image_id'])) {
            try {
                $this->image = new FImage($this->get['image_id']);
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
                $this->redirect('grid');
            }
        }
    }
    
    
    /******************************* grid ***********************************/
    
    
    public function prepareGrid()
    {
        // where condtion
        $where = array('nodeId' => $this->nodeId, 'isDeleted' => 0);
        
        // paginator
        $count = FImage::getCount($where);
        $paginator = new FPaginator2($count);
        $paginator->itemsPerPage = 12;
        $paginator->htmlDisplayEnabled = FALSE;
        $this->template->paginator = $paginator;
        $this->template->images = FImage::getAll($where, $paginator->getLimit(), $paginator->getOffset());
    }
    
    /******************************* list ***********************************/
    
    
    public function prepareList()
    {
        // where condtion
        $where = array('nodeId' => $this->nodeId, 'isDeleted' => 0);
        
        // paginator
        $count = FImage::getCount($where);
        $paginator = new FPaginator2($count);
        $paginator->itemsPerPage = 25;
        $paginator->htmlDisplayEnabled = FALSE;
        $this->template->paginator = $paginator;
        $this->template->images = FImage::getAll($where, $paginator->getLimit(), $paginator->getOffset());
    }
    
    
    /******************************* insert ***********************************/
    
    
    public function prepareInsert()
    {
        // add tab
        $this->tabs['insert'] = 'Insert';
        $form = new FForm('insertImage'.$this->nodeId);
        
        //file
        $form->addFieldset('File');
        $form->addFile('image', 'Image: *')
            ->addRule(FForm::Required, 'Please provide image');
        
        // data
        $form->addFieldset('Data');
        $form->addText('name', 'Name:')
            ->size(40)
            ->setDescription('(Only plain name, without extension.)');
        $form->addSelect('nodeId', 'Node: ', FMenu::getSelect())
            ->addRule(array($this, 'Validator_checkImageNode'), 'You have to choose files page!')
            ->setValue($this->nodeId);
        $form->addText('ord', 'Ord:')
            ->addRule(FForm::NUMERIC, 'Ord have to be numeric!');
        $form->addCheckbox('isActive', 'Active', 1)->setValue(1);
        
        // action
        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Submit')
            ->class('btn');
            
        $form->onValid = array($this, 'Form_insertFormSubmitted');
        $form->start();
        $this->template->form = $form;
    }
    
    public function Form_insertFormSubmitted(FForm $form)
    {
        $data = $form->getValues();
        $file = new FImage();
        
        $file->uploadedFile = $data['image'];
        $file->name = $data['name'];
        $file->nodeId = $data['nodeId'];
        $file->ord = $data['ord'];
        $file->isActive = $data['isActive'];
        $file->save();
        
        $form->clean();
        $this->flashMessage('Image has been created');
        $this->redirect('grid');
    }
    
    
    /******************************* edit ***********************************/
    
    
    public function prepareEdit()
    {
        if (!isset($this->get['image_id'])) {
            $this->redirect('grid');
        }
        $this->tabs['edit'] = 'Edit';
    }
    
    
    public function renderEdit()
    {
        $form = new FForm('editFile');
        
        //file
        $form->addFieldset('Current file');
        $form->addImg('image', 'Image: ', FImage::getStorageDir(TRUE, 's'). DIRECTORY_SEPARATOR)
            ->setValue($this->image->getFileName());
        $form->addText('filePath', 'File path:')
            ->size(70)
            ->readonly()
            ->setValue(FApplication::$httpRequest->uri->getHostUri() . $this->image->getFilePath(TRUE));
            
            
        // data
        $form->addFieldset('Data');
        $form->addFile('newImage', 'New Image:');
        $form->addText('name', 'Name:')
            ->size(40)
            ->setDescription('(Only plain name, without extension.)');
        $form->addSelect('nodeId', 'Node: ', FMenu::getSelect($this->nodeId, TRUE))
            ->addRule(array($this, 'Validator_checkImageNode'), 'You have to choose files page!')
            ->setValue($this->nodeId)
            ->readonly();
        $form->addText('ord', 'Ord:')
            ->addRule(FForm::NUMERIC, 'Ord have to be numeric!');
        $form->addCheckbox('isActive', 'Active', 1);
        
        // action
        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Submit')
            ->class('btn');
            
        $form->setInitValues((array) $this->image);
        $form->onValid = array($this, 'Form_editFormSubmitted');
        $form->start();
        $this->template->form = $form;
        
    }
    
    
    public function Form_editFormSubmitted(FForm $form)
    {
        $data = $form->getValues();
        
        if ($data['newImage'] !== NULL) {
            $this->image->uploadedFile = $data['newImage'];
        }
        
        $this->image->name = $data['name'];
        $this->image->nodeId = $data['nodeId'];
        $this->image->ord = $data['ord'];
        $this->image->isActive = $data['isActive'];
        $this->image->save();
        
        $form->clean();
        $this->flashMessage('File has been updated');

        // redirect to page
        if (isset($this->get['page'])) {
            $this->redirect('grid', "page={$this->get['page']}");
        } else {
            $this->redirect('grid');
        }
    }
    
    
    /******************************* delete ***********************************/
    
    
    public function handleDelete()
    {
        if (!isset($this->get['image_id'])) {
            $this->redirect('grid');
        }
        
        try {
            $this->image->delete();
            $this->flashMessage('File has been deleted');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());    
        }
        
        // redirect to page
        if (isset($this->get['page'])) {
            $this->redirect('grid', "page={$this->get['page']}");
        } else {
            $this->redirect('grid');
        }
    }
    
    
    
    /******************************* other ***********************************/
    
        
    /**
     * Checks if node for file is FilesAdminControler or extended from FilesAdminControler
     * @param Select $control
     * @param string $error
     * @param array $params
     * @return mixed
     */
    public function Validator_checkImageNode($control, $error, $params)
    {
        $nodeId = $control->getValue();
        $node = new FNode($nodeId);
        
        $controler = new $node->adminControler;
        if ($controler instanceof ImagesAdminControler) {
            return TRUE;
        } else {
            return $error;
        }
    }
}
