<?php

use Admin\RenewalPackage\Package\IOptionsFormDelegate as IPackageOptionsFormDelegate;
use Admin\RenewalPackage\Package\OptionsForm as PackageOptionsForm;

class RenewalPackageAdminControler extends PackageAdminControler implements IPackageOptionsFormDelegate
{
    /**
     * @var string
     */
    public static $handleObject = 'RenewalPackage';

    /**
     * @var RenewalPackage
     */
    public $node;

    /**
     * @var array
     */
    protected $tabs = array(
        'package' => 'Package',
        'includes' => 'Includes',
        'properties' => 'Properties',
        'details' => 'Details',
        'seo' => 'Seo'
    );

    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'PackageAdmin';
    }

    /****************************************** package ******************************************/

    public function preparePackage()
    {
        $form = new PackageOptionsForm($this->nodeId . '_' . $this->action);
        $form->startup($this, $this->node);
        $this->template->form = $form;
    }

    /** *********************** IPackageOptionsFormDelegate ************************ **/

    public function renewalPackageOptionsFormSucceeded()
    {
        $this->flashMessage('Package has been updated');
        $this->redirect();
    }

    /**
     * @param Exception $e
     */
    public function renewalPackageOptionsFormFailed(Exception $e)
    {
        $this->flashMessage($e->getMessage());
        $this->redirect();
    }
}