<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    CertificatesAdminControler.php 2009-09-22 divak@gmail.com
 */
class CertificatesAdminControler extends CHAdminControler
{

    /** @var array */
    protected $tabs = array(
        'default' => 'Certificates ',
        'bronze' => 'Bronze Cover Letters',
        'memorandumArticles' => 'Memorandum & Articles',
        'customMA' => 'Custom Memorandum & Articles',
    );

    public function startup()
    {
        // add permission for staff
        $this->acl->allow('staff', 'page', array('view'));
        parent::startup();
    }

    public function beforePrepare()
    {
        parent::beforePrepare();
        ini_set('max_execution_time', 240);
        $this->templateExtendedFrom[] = 'NameAdminControler';
    }

    /*     * ************************************ list ************************************* */

    public function renderDefault()
    {
        $form = new CertificatesAdminForm($this->node->getId() . '_CertificatesAdminForm');
        $form->startup($this, array($this, 'Form_CertificatesAdminFormSubmitted'), CHFiling::getNonPrintedCompanies());
        $this->template->form = $form;
        $this->template->certificates = CHFiling::getNonPrintedCompanies();
    }

    /**
     * @param CertificatesAdminForm $form
     */
    public function Form_CertificatesAdminFormSubmitted(CertificatesAdminForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('All Certificates of incorporation marked as printed');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $this->redirect();
    }

    public function renderBronze()
    {
        $form = new BronzeCoverLetterAdminForm($this->node->getId() . '_BronzeCoverLetterAdminForm');
        $form->startup($this, array($this, 'Form_BronzeCoverLetterAdminFormSubmitted'), CHFiling::getNonPrintedBronzeCompanyCoverLetters());
        $this->template->form = $form;
        $this->template->bronzecoverletters = CHFiling::getNonPrintedBronzeCompanyCoverLetters();
    }

    /**
     * @param BronzeCoverLetterAdminForm $form
     */
    public function Form_BronzeCoverLetterAdminFormSubmitted(BronzeCoverLetterAdminForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('All Bronze Cover Letters marked as printed');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $this->redirect();
    }

    public function renderMemorandumArticles()
    {
        $form = new MemorandumArticlesAdminForm($this->node->getId() . '_MemorandumArticlesAdminForm');
        $form->startup($this, array($this, 'Form_MemorandumArticlesFormSubmitted'), CHFiling::getNonPrintedMA());
        $this->template->form = $form;
        $this->template->memorandumArticles = CHFiling::getNonPrintedMA();
    }

    /**
     * @param MemorandumArticlesAdminForm $form
     */
    public function Form_MemorandumArticlesFormSubmitted(MemorandumArticlesAdminForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('All Memorandum Articles marked as printed');
            $this->redirect();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    public function renderCustomMA()
    {

        $form = new CustomMAAdminForm($this->node->getId() . '_CustomMAAdminForm');
        $form->startup($this, array($this, 'Form_CustomMAFormSubmitted'), CHFiling::getNonPrintedMA(10, TRUE));
        $this->template->form = $form;
        $this->template->memorandumArticles = CHFiling::getNonPrintedMA(10, TRUE);
    }

    /**
     * @param MemorandumArticlesAdminForm $form
     */
    public function Form_CustomMAFormSubmitted(CustomMAAdminForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('All Custom MA marked as printed');
            $this->redirect();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    public function handleView()
    {
        try {
            if (isset($this->get['certificate_id'])) {
                $id = (array) $this->get['certificate_id'];
                CHFiling::getPdfCertificates($id);
                $this->terminate();
            }
            if (isset($this->get['bronzecoverletter_id'])) {
                $id = $this->get['bronzecoverletter_id'];
                $macover = new CoverLetter();
                $macover->createPdf($id);
                $macover->printPdf();
                $this->terminate();
            }
            if (isset($this->get['ma_id'])) {
                $id = (array) $this->get['ma_id'];
                CHFiling::getPdfMemorandumArticles($id);
                $this->terminate();
            }
            if (isset($this->get['macoverletter_id'])) {
                $id = $this->get['macoverletter_id'];
                $macover = new MACoverLetter();
                $macover->createPdf($id);
                $macover->printPdf();
                $this->terminate();
            }
            if ((!isset($this->get['bronzecoverletter_id'])) && (!isset($this->get['certificate_id']))) {
                $this->redirect('default');
            }
        } catch (Excepetion $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect('default');
        }
    }

}
