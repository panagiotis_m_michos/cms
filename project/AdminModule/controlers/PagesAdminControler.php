<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    PagesAdminControler.php 2009-11-26 divak@gmail.com
 */

class PagesAdminControler extends PageAdminControler
{
    /** @var array */
    protected $tabs = array(
        'pagesBrowser' => 'List',
    );
    
    
    public function beforePrepare()
    {
        $this->templateExtendedFrom[] = 'PageAdmin';
        parent::beforePrepare();
    }
}