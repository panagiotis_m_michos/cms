<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    ReportsAdminControler.php 2010-01-27 stan, zygi
 */

class ReportsAdminControler extends ProductAdminControler
{
	/**
	 * @var string
	 */
	static public $handleObject = 'ReportsAdminModel';
	
	/**
	 * @var ReportsAdminModel
	 */
	public $node;
	
	/**
	 * @var array
	 */
	public $tabs = array(
		'barclays' => 'Barclays',
		'orders' => 'Orders',
                'incorporations' => 'Incorporations'
	);

    
    /******************************************** barclays ********************************************/


	public function renderBarclays()
	{
        // --- filter ---
        $filter = $this->node->getComponentBarclaysForm();
		$this->template->form = $filter;

		$this->template->barclaysStats = $this->node->getBarclaysData($filter);
	}



    /******************************************** orders ********************************************/

    
	
	public function renderOrders()
	{
		// --- filter ---
		$filter = $this->node->getComponentOrdersForm();
		$this->template->filter = $filter;

        // --- export to CSV ---
        if (isset($this->get['csv'])) {
            Debug::disableProfiler();
            $this->node->outputOrdersCSV($filter);
        }
		
		// --- paginator ---
		$paginator = $this->node->getComponentOrdersPaginator($filter);
		$this->template->paginator = $paginator;
		
		// --- data ---
		$this->template->data = $this->node->getOrdersData($filter, $paginator);

	}
        
       	public function renderIncorporations()
	{
            Debug::disableProfiler();
            $filter = $this->node->getComponentBarclaysForm();
            $this->template->form = $filter;
            
            $datasource = $this->node->getIncorporationData($filter);
            $grid = new IncorporationAdminDatagrid($datasource, 'incorporated');
            
            $this->template->incorporation = $grid->getHtml();
            $this->template->total = $this->node->getIncorporationTotal($filter);
	}

}