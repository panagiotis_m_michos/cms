<?php

use Entities\Service;

class ProductAdminControler extends PageAdminControler implements IProductPropertiesFormDelegate
{
    /** @var string */
    static public $handleObject = 'Product';

    /**
     * @var Product
     */
    public $node;

    /** @var array */
    protected $tabs = array(
        'product' => 'Product',
        'properties' => 'Properties',
        'layout' => 'Layout',
        'details' => 'Details',
        'seo' => 'Seo'
    );


    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'PageAdmin';
    }


    public function prepareProduct()
    {
        $form = new FForm($this->nodeId . '_' . $this->action);

        // fieldset
        $form->addFieldset('Package');
        $form->addText('title', 'Title: * ')->addRule(FForm::Required, 'Title is required!')->size(30)->setValue($this->node->page->title);
        $form->addText('alternativeTitle', 'Alternative Title:')->size(50)->setValue($this->node->page->alternativeTitle);
        $form->add('CmsFckArea', 'abstract', 'Description:')->setSize(500, 100)->setToolbar('Basic')->setValue($this->node->page->abstract);
        $form->add('CmsFckArea', 'text', 'Page Text:')->setSize(500, 300)->setValue($this->node->page->text);

        // price
        $form->addFieldset('Price');
        $form->addText('price', 'Price: *')->size(5)->setValue($this->node->price)->addRule(FForm::Required, 'Please provide price')->addRule('Numeric', 'Price has to be number');
        $form->addText('productValue', 'Product value: *')->size(5)->setValue($this->node->productValue)->addRule(FForm::Required, 'Please provide product value')->addRule('Numeric', 'Price value has to be number');
        $form->addText('associatedPrice', 'Associated price: *')->size(5)->setValue($this->node->associatedPrice)->addRule(FForm::Required, 'Please provide associated price')->addRule('Numeric', 'Associated price value has to be number');
        $form->addText('wholesalePrice', 'Wholesale price: *')->size(5)->setValue($this->node->wholesalePrice)->addRule(FForm::Required, 'Please provide wholesale price')->addRule('Numeric', 'Wholesale price value has to be number');
        $form->addText('offerPrice', 'Offer price: *')->size(5)->setValue($this->node->getOfferPrice())->addRule(FForm::Required, 'Please provide offer price')->addRule('Numeric', 'Offer price value has to be number');


        // action
        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Submit')->onclick('changes = false')->class('btn');
        $form->onValid = array($this, 'packageFormSubmitted');
        $form->start();
        $this->template->form = $form;
    }


    public function packageFormSubmitted($form)
    {
        $data = $form->getValues();
        $this->node->page->title = $data['title'];
        $this->node->page->alternativeTitle = $data['alternativeTitle'];
        $this->node->page->abstract = $data['abstract'];
        $this->node->page->text = $data['text'];
        $this->node->price = $data['price'];
        $this->node->associatedPrice = $data['associatedPrice'];
        $this->node->wholesalePrice = $data['wholesalePrice'];
        $this->node->productValue = $data['productValue'];
        $this->node->setOfferPrice($data['offerPrice']);
        $this->node->save();

        $this->flashMessage('Product has been updated');
        $this->redirect(NULL);
    }


    /******************************** properties *******************************/


    public function prepareProperties()
    {
        $form = new ProductPropertiesForm($this->nodeId . '_properties');
        $form->startup($this, $this->node, BasketProduct::getAllProducts(), Service::$types, Service::$durations);
        $this->template->form = $form;
    }


    /************************* IProductPropertiesFormDelegate *************************/


    public function productPropertiesFormSucceeded(ProductPropertiesForm $form)
    {
        $this->flashMessage('Product has been updated');
        $this->redirect();
    }

    public function productPropertiesFormFailed(ProductPropertiesForm $form, Exception $e)
    {
        $this->flashMessage($e->getMessage(), 'error');
        $this->redirect();
    }

}
