<?php

use Admin\Order\ExportForm;
use Export\Order\CsvStream;
use Export\Order\Fetcher as OrdersFetcher;
use Export\Order\Filter;
use HttpModule\Responses\CsvStreamedResponse;
use Services\NodeService;

class OrdersExportAdminControler extends PageAdminControler
{
    /**
     * @var string
     */
    public static $handleObject = 'Export';

    /**
     * @var Export
     */
    public $node;

    /**
     * @var OrdersFetcher
     */
    public $fetcher;

    /**
     * @var array
     */
    protected $tabs = [
        'default' => 'Export',
        'settings' => 'Settings'
    ];

    /**
     * @var CsvStream
     */
    private $csvStream;

    /**
     * @var NodeService
     */
    private $nodeService;

    public function startup()
    {
        // add permission for staff
        $this->acl->allow('staff', 'page', ['view']);
        $this->csvStream = $this->getService(DiLocator::EXPORT_ORDER_CSV_STREAM);
        $this->nodeService = $this->getService(DiLocator::SERVICE_NODE);

        parent::startup();
    }

    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'PageAdminControler';
    }

    public function renderDefault()
    {
        $filter = new Filter(
            $this->node->excludedProducts,
            $this->nodeService->getProductsIdsWithIdCheckRequired(),
            FNode::getChildsIds(PackageAdminControler::PACKAGES_FOLDER),
            BarclaysBanking::BARCLAYS_BANKING_PRODUCT
        );

        $form = $this->createForm(new ExportForm(), $filter);
        if ($form->isValid()) {
            $response = new CsvStreamedResponse(
                'orders.csv', function () use ($filter) {
                    $this->csvStream->stream($filter);
                }
            );
            $response->send();
            $this->terminate();
        }
        $this->template->hasErrors = (bool) $form->getErrors()->count();
        $this->template->form = $form->createView();
    }

    /****************************************** settings ******************************************/

    public function prepareSettings()
    {
        $form = new FForm($this->nodeId . '_' . $this->action);

        $form->addFieldset('Exclude from Export');
        $form->add('AsmSelect', 'excludedProducts', 'Products:')
            ->multiple()
            ->title('--- Select --')
            ->setValue($this->node->excludedProducts)
            ->setOptions(BasketProduct::getAllProducts());

        // action
        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Submit')
            ->onclick('changes = false')
            ->class('btn');

        $form->onValid = [$this, 'settingsFormSubmitted'];
        $form->start();
        $this->template->form = $form;
    }

    /**
     * @param FForm $form
     * @throws Exception
     */
    public function settingsFormSubmitted(FForm $form)
    {
        $data = $form->getValues();

        $this->node->excludedProducts = $data['excludedProducts'];
        $this->node->save();

        $this->flashMessage('Settings have been updated');
        $this->redirect(NULL);
    }

}
