<?php

use ErrorModule\Controllers\LoggerController;
use ErrorModule\Ext\DebugExt;

class LoggerAdminControler extends BaseAdminControler
{
    const PAGE_LOGGER = 1104;

    /**
     * @var array
     */
    public $tabs = array(
        'exceptions' => 'Exceptions',
    );

    /**
     * @var LoggerController
     */
    private $loggerController;

    public function startup()
    {
        parent::startup();
        $this->loggerController = $this->getService(DebugExt::LOGGER_CONTROLLER);
    }


    public function beforePrepare()
    {
        if ($this->loggerController->hasErrorFile()) {
            $this->tabs['errorLog'] = 'Error Log';
        }
        if ($this->loggerController->hasInfoFile()) {
            $this->tabs['infoLog'] = 'Info Log';
        }
        parent::beforePrepare();
    }

    public function renderExceptions()
    {

        // --- form ---
        //if ($this->loggerController->hasExceptions()) {
        //    $form = new LoggerExceptionsAdminForm('logger');
        //    $form->startup();
        //    $this->template->form = $form;
        //    if ($form->isOk2Save()) {
        //        if ($form->isSubmitedBy('deleteAll')) {
        //            $this->loggerContoller->deleteExceptions();
        //        } elseif ($form->isSubmitedBy('deleteInfoLog')) {
        //            $this->loggerContoller->deleteInfoLog();
        //        } elseif ($form->isSubmitedBy('deleteErrorLog')) {
        //            $this->loggerContoller->deleteErroLog();
        //        }
        //        $form->clean();
        //    }
        //}
        // --- files ---
        $this->template->files = $this->loggerController->getExceptions();
    }

    public function renderViewException()
    {
        try {
            $fileId = isset($this->get['fileId']) ? $this->get['fileId'] : NULL;
            $this->template->html = $this->loggerController->getExceptionContent($fileId);
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('exceptions');
        }
    }

    public function handleDeleteException()
    {
        try {
            $fileId = isset($this->post['fileId']) ? $this->post['fileId'] : NULL;
            $this->loggerController->deleteException($fileId);
            $this->flashMessage('File has been deleted');
            $this->redirect('exceptions');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('exceptions');
        }
    }

    public function renderErrorLog()
    {
        $this->template->logContent = $this->loggerController->getErrorLogContent();
    }

    public function renderInfoLog()
    {
        $this->template->logContent = $this->loggerController->getInfoLogContent();
    }

}
