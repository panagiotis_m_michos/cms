<?php

use Admin\Order\Entity\RefundForm;
use Entities\Order as OrderEntity;
use HttpModule\Responses\PdfResponse;
use Order\Refund\Calculator\Calculator;
use Order\Refund\Data;
use OrderModule\Config\DiLocator as OrderDiLocator;
use OrderModule\Pdf\IInvoiceGenerator;
use OrdersAdminModule\Views\OrdersAdminControlerView;
use Services\Facades\OrderRefundFacade;
use Services\OrderService;

class OrdersAdminControler extends CHAdminControler
{
    const ORDERS_PAGE = 552;

    /**
     * @var array
     */
    protected $tabs = array(
        'list' => 'List',
    );

    /**
     * @var OrderService
     */
    private $orderService;

    /**
     * @var IInvoiceGenerator
     */
    private $invoiceGenerator;

    /**
     * @var Order
     */
    private $order;

    /**
     * @var OrderEntity
     */
    private $orderEntity;

    /**
     * @var OrdersAdminControlerView
     */
    private $controllerView;

    public function startup()
    {
        // add permission for staff
        $this->acl->allow('staff', 'page', array('view'));
        $this->orderService = $this->getService(DiLocator::SERVICE_ORDER);
        $this->invoiceGenerator = $this->getService(OrderDiLocator::FPDF_INVOICE_GENERATOR);

        parent::startup();
    }

    public function beforePrepare()
    {
        //for some reason orderId underscored in old system and datagrid uses camelcase
        if (isset($this->get['order_id'])) {
            $this->get['orderId'] = $this->get['order_id'];
        }

        $orderId = (int) $this->getParameter('orderId');
        if ($orderId) {
            try {
                $this->order = new Order($orderId);
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage());
                $this->redirect('list');
            }

            $this->template->order = $this->order;
            $this->template->orderId = $this->order->getId();

            $this->orderEntity = $this->orderService->getOrderById($orderId);
            $this->template->orderEntity = $this->orderEntity;
            $this->controllerView = new OrdersAdminControlerView($this->orderEntity);
        }

        parent::beforePrepare();
    }

    /****************************************** list ******************************************/

    public function prepareList()
    {
        $this->template->datagrid = new OrdersAdminDatagrid(
            $this->orderService->getListDatasource(),
            'orderId'
        );
    }

    /****************************************** view ******************************************/

    public function prepareView()
    {
        if (!$this->orderEntity) {
            $this->redirect('list');
        }

        $this->tabs['view'] = array('title' => 'Order', 'params' => "order_id={$this->orderEntity->getId()}");

        $this->template->view = $this->controllerView;
    }

    /****************************************** refund ******************************************/


    public function prepareRefund()
    {
        $orderId = $this->orderEntity->getId();
        if (!$this->controllerView->canBeRefunded()) {
            $this->redirect('view', array("order_id" => $orderId));
        }

        $this->tabs['view'] = array('title' => 'Order', 'params' => array("order_id" => $orderId));
        $this->tabs['refund'] = array('title' => 'Refund', 'params' => array('order_id' => $orderId));
    }

    public function handleRefund()
    {
        if ($this->isAjax()) {
            $do = $this->getParameter('do');
            if ($do === 'calculateTotal') {
                try {
                    $calculator = new Calculator();
                    $result = $calculator->calculate(
                        $this->getParameter('itemsTotal'),
                        $this->getParameter('adminFeeTotal'),
                        $this->orderEntity->getCredit()
                    );
                    $payload = array(
                        'error' => FALSE,
                        'price' => array(
                            'itemsTotal' => $result->getSubtotal(),
                            'adminFeeTotal' => $result->getAdminFee(),
                            'creditTotal' => $result->getTotalCreditRefund(),
                            'moneyTotal' => $result->getTotalMoneyRefund()
                        )
                    );
                    $this->sendJSONResponse($payload);
                } catch (Exception $e) {
                    $this->handleException($e);
                }
            }
        }
    }

    public function renderRefund()
    {
        $form = $this->createForm(new RefundForm($this->orderEntity), new Data());
        if ($form->isValid()) {
            /** @var Data $refundData */
            $refundData = $form->getData();

            $calculator = new Calculator();
            $result = $calculator->calculate(
                $refundData->getItemsRefundTotal(),
                $refundData->getAdminFee(),
                $this->orderEntity->getCredit()
            );

            /** @var OrderRefundFacade $orderRefundFacade */
            $orderRefundFacade = $this->getService(DiLocator::FACADE_ORDER_REFUND);
            $orderRefundFacade->refund(
                $this->orderEntity,
                $refundData,
                $result,
                FUser::getSignedIn()
            );

            $this->flashMessage('Refund has been created');
            $this->redirect('view', array("order_id" => $this->order->getId()));
        }

        $this->template->form = $form->createView();
        $this->template->view = $this->controllerView;
    }

    /****************************************** print ******************************************/

    public function handlePrint()
    {
        if (!$this->orderEntity) {
            $this->redirect('list');
        }

        $response = new PdfResponse($this->invoiceGenerator->getContents($this->orderEntity), 'Invoice.pdf');
        $response->send();
        exit(0);
    }

    /**
     * @param Exception $e
     */
    private function handleException(Exception $e)
    {
        if ($this->isAjax()) {
            $this->sendJSONResponse(array('error' => TRUE, 'message' => $e->getMessage()));
        } else {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }
}
