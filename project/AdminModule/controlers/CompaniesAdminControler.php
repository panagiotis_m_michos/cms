<?php

use AdminModule\Factories\CompaniesAdminControlerViewFactory;
use BankingModule\BankingService;
use BankingModule\Emailers\TsbApplicationEmailer;
use BankingModule\Entities\CompanyCustomer;
use BankingModule\Views\CompanyBankingView;
use CompanyFormationModule\Views\IncorporationSummaryViewFactory;
use Entities\Company as CompanyEntity;
use Entities\Customer as CustomerEntity;
use Entities\Register\Psc\CompanyStatement;
use Entities\Register\Psc\CorporatePsc;
use Entities\Register\Psc\LegalPersonPsc;
use Entities\Register\Psc\PersonPsc;
use Entities\Register\Psc\Psc;
use ErrorModule\Ext\DebugExt;
use Exceptions\Technical\FormSubmissionNotFoundException;
use Exceptions\Technical\RequestException;
use Factories\Front\CompanyViewFactory;
use FormSubmissionModule\Factories\FormSubmissionViewFactory;
use PeopleWithSignificantControl\Forms\CorporatePscData;
use PeopleWithSignificantControl\Forms\LegalPersonPscData;
use PeopleWithSignificantControl\Forms\PersonPscData;
use PeopleWithSignificantControl\Forms\PscFormFactory;
use PeopleWithSignificantControl\Interfaces\IPscAware;
use PeopleWithSignificantControl\Providers\PscChoicesProvider;
use PeopleWithSignificantControl\Views\CompanySummaryView;
use Services\Company\CompanyTransferrer;
use Services\CompanyDocumentService;
use Services\CompanyService;
use Services\CustomerService;
use Services\NodeService;
use Services\Register\Psc\EntryService;
use Services\SubmissionService;

class CompaniesAdminControler extends CUAdminControler
{

    const COMPANIES_PAGE = 551;

    /** @var array */
    protected $tabs = array(
        'list' => 'List',
    );

    /**
     * @var CustomerEntity
     */
    private $customerEntity;

    /**
     * @var CompanyEntity
     */
    private $companyEntity;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var CompanyDocumentService
     */
    private $companyDocumentService;

    /**
     * @var CustomerService
     */
    private $customerService;

    /**
     * @var SubmissionService
     */
    private $submissionService;

    /**
     * @var CompanyTransferrer
     */
    private $companyTransferrer;

    /**
     * @var CompaniesAdminControlerViewFactory
     */
    private $controllerViewFactory;

    /**
     * @var FormSubmissionViewFactory
     */
    private $formSubmissionViewFactory;

    /**
     * @var CompanyViewFactory
     */
    private $companyViewFactory;

    /**
     * @var NodeService
     */
    private $nodeService;

    /**
     * @var BankingService
     */
    private $bankingService;

    /**
     * @var CompanyBankingView
     */
    private $companyBankingView;

    /**
     * @var EntryService
     */
    private $entryService;

    /**
     * @var PscFormFactory
     */
    private $pscFormFactory;

    /**
     * @var PscChoicesProvider
     */
    private $pscChoicesProvider;

    public function startup()
    {
        // add permission for staff
        $this->acl->allow('staff', 'page', array('view'));
        $this->companyService = $this->getService(DiLocator::SERVICE_COMPANY);
        $this->companyDocumentService = $this->getService(DiLocator::SERVICE_COMPANY_DOCUMENT);
        $this->customerService = $this->getService(DiLocator::SERVICE_CUSTOMER);
        $this->submissionService = $this->getService(DiLocator::SERVICE_SUBMISSION);
        $this->logger = $this->getService(DebugExt::LOGGER);
        $this->companyTransferrer = $this->getService(DiLocator::SERVICE_COMPANY_TRANSFERRER);
        $this->controllerViewFactory = $this->getService('admin_module.factories.companies_admin_controler_view_factory');
        $this->formSubmissionViewFactory = $this->getService('form_submission_module.factories.form_submission_view_factory');
        // todo: psc - front factory? change to both?
        $this->companyViewFactory = $this->getService(DiLocator::FACTORY_FRONT_COMPANY_VIEW);
        $this->nodeService = $this->getService('services.node_service');
        $this->bankingService = $this->getService('banking_module.banking_service');
        $this->companyBankingView = $this->getService('banking_module.views.company_banking_view');
        // todo: psc - rename to onlineRegisterService?
        $this->entryService = $this->getService('services.register.psc.entry_service');
        $this->pscFormFactory = $this->getService('people_with_significant_control_module.forms.form_factory');
        $this->pscChoicesProvider = $this->getService('people_with_significant_control_module.providers.psc_choices_provider');

        parent::startup();
    }

    // @TODO: refactor!!! same things are happening in parent startup method
    public function beforePrepare()
    {
        try {
            $this->chFiling = new CHFiling();

            //checking if company_id passed exept list, and admin Actions ('insertPage', 'editPage', 'deletePage')
            if (!isset($this->get['company_id']) && !in_array($this->action, array('list', 'insertPage', 'editPage', 'deletePage'))) {
                $this->redirect('list');
            } elseif (isset($this->get['company_id'])) {
                // company
                $this->company = $this->chFiling->getCompany($this->get['company_id']);
                $this->companyEntity = $this->companyService->getCompanyById($this->get['company_id']);
                $this->companyId = $this->company->getCompanyId();
                $this->template->company = $this->company;
                $this->template->companyId = $this->company->getCompanyId();
                // customer
                $this->customer = new Customer($this->company->getCustomerId());
                $this->customerEntity = $this->companyEntity->getCustomer();
                $this->template->customer = $this->customer;
                $this->template->customerId = $this->customer->getId();
            }
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('list');
        }
        parent::beforePrepare();
    }

    /*     * ***************************** list ********************************** */

    public function prepareList()
    {
        $form = new FForm('companies', 'get');
        $form->addFieldset('Company Search');
        $form->addText('companyName', 'Company name: ');
        $form->addText('companyNumber', 'Company number: ');
        $form->addText('companyId', 'Company id: ')->size(7);
        $form->addSubmit('companySearch', 'Search')->class('btn');
        $form->start();
        $this->template->form = $form;

        // filter
        $where = array();
        if ($form->isOk2Save()) {
            $data = $form->getValues();
            if (!empty($data['companyName'])) {
                $where['company_name'] = $data['companyName'];
            }
            if (!empty($data['companyNumber'])) {
                $where['company_number'] = $data['companyNumber'];
            }
            if (!empty($data['companyId'])) {
                $where['company_id'] = $data['companyId'];
            }
            $form->clean();
        }

        // paginator
        $count = $this->chFiling->getCompaniesCount($where);
        $paginator = new FPaginator2($count);
        $paginator->itemsPerPage = 25;
        $paginator->htmlDisplayEnabled = FALSE;
        $this->template->paginator = $paginator;
        $this->template->companies = $this->chFiling->getCompanies($where, $paginator->getLimit(), $paginator->getOffset());
    }

    /*     * ***************************** view ********************************** */

    public function prepareView()
    {
        try {
            $this->template->companyStatus = $this->company->getStatus();
            $this->tabs['view'] = array('title' => $this->company->getCompanyName(), 'params' => "company_id={$this->company->getCompanyId()}");

            if ($this->company->getStatus() != 'complete') {
                $this->prepareViewCF();
            } else {
                $this->prepareViewCU();
            }
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect('view');
        }
    }

    public function Form_viewFormSubmitted()
    {
        try {
            $company = $this->chFiling->getCompany($this->get['company_id']);
            $company->getLastFormSubmission()->sendRequest();
            $this->flashMessage('Application has been sent');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $this->redirect();
    }

    /*     * ***************************** copy ********************************** */

    public function prepareCopy()
    {
        $form = new FForm('companyCopy_' . $this->companyId);
        $form->addFieldset('Company name');
        $form->addText('companyName', 'Company Name:')
            ->addRule(FForm::Required, 'Please Provide Company Name')
            ->addRule(array($this, 'Validator_companyNameChars'), 'Illegal characters detected. Only characters on the English keyboard may be used. Please retype rather than copy & paste if you are having issues');
        $form->addSubmit('submit', 'Submit')->class('btn');
        $form->onValid = array($this, 'Form_copyFormSubmitted');
        $form->start();

        $this->template->form = $form;
        $this->tabs['view'] = array('title' => $this->company->getCompanyName(), 'params' => "company_id={$this->company->getCompanyId()}");
        $this->tabs['copy'] = array('title' => 'Copy', 'params' => "company_id={$this->company->getCompanyId()}");
    }

    public function Form_copyFormSubmitted(FForm $form)
    {
        try {
            $data = $form->getValues();
            $companyCloneFacade = $this->getService(DiLocator::FACADE_CLONE_COMPANY);
            $companyCloneFacade->copyCompany($this->customerEntity, $this->companyEntity, $data['companyName']);
            //Company::copy($this->customer->getId(), $this->companyId, $data['companyName']);
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect();
        }

        $this->flashMessage('Company has been copied');
        $this->redirect('view', "company_id={$this->companyId}");
    }

    /**
     * @param Text $control
     * @param string $error
     * @param array $params
     * @return bool|string
     */
    public function Validator_companyNameChars(Text $control, $error, $params)
    {
        if (preg_match('/^([ \t\r\n\v\f\x80\x26\x40\xa3\x24\xa5\x2a\x3d\x23\x25\x2b\x91\x92\x27\x28\x29\x5b\x5d\x7b\x7d\x3c\x3e\x21\xab\xbb\x3f\x93\x94\x22\x5c\x2f‘’“”«»£€¥1-9A-Za-z]+)$/', $control->getValue())) {
            return TRUE;
        }
        return $error;
    }

    /*     * ***************************** batchCopy ********************************** */

    public function prepareBatchCopy()
    {
        try {
            $form = new CompaniesAdminBatchCopyForm('companyBatchCopy_' . $this->companyId);
            $form->startup($this);
            $this->template->form = $form;
            $this->tabs['view'] = array('title' => $this->company->getCompanyName(), 'params' => "company_id={$this->company->getCompanyId()}");
            $this->tabs['batchCopy'] = array('title' => 'Batch Copy', 'params' => "company_id={$this->company->getCompanyId()}");
            if ($form->isOk2Save()) {
                $this->processCompaniesAdminBatchCopyForm($form->getFileName());
                $form->clean();
                $this->flashMessage('Company has been copied');
                $this->redirect('view', "company_id={$this->companyId}");
            }
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect();
        }
    }

    public function processCompaniesAdminBatchCopyForm($filename)
    {
        $handle = fopen($filename, 'r');
        $companyCloneFacade = $this->getService(DiLocator::FACADE_CLONE_COMPANY);
        while (($row = fgetcsv($handle, 1000, ",")) !== FALSE) {
            $companyCloneFacade->copyFullCompany($this->customerEntity, $this->companyEntity, $row[0]);
        }
    }

    /*     * ***************************** formSubmission ********************************** */

    public function handleFormSubmission()
    {
        if (!isset($this->get['submission_id'])) {
            $this->redirect('view', "company_id={$this->company->getCompanyId()}");
        }

        // xml request
        if (isset($this->get['xml_request'])) {
            Debug::disableProfiler();
            $filename = self::getEnvelopeXmlRequestPath($this->get['xml_request']);
            header('Content-type: application/octet-stream');
            header('Content-Disposition: attachment; filename=xml_request_' . $this->get['xml_request'] . '.xml');
            header('Pragma: no-cache');
            header('Expires: 0');
            $out = file_get_contents($filename);
            $this->terminate($out);
            // xml response
        } elseif (isset($this->get['xml_response'])) {
            Debug::disableProfiler();
            $filename = self::getEnvelopeXmlResponsePath($this->get['xml_response']);
            header('Content-type: application/octet-stream');
            header('Content-Disposition: attachment; filename=xml_response_' . $this->get['xml_response'] . '.xml');
            header('Pragma: no-cache');
            header('Expires: 0');
            $out = file_get_contents($filename);
            $this->terminate($out);
        }
    }

    public function prepareFormSubmission()
    {
        if (!isset($this->get['submission_id'])) {
            $this->redirect('view', "company_id={$this->company->getCompanyId()}");
        }

        $submission = $this->submissionService->getFormSubmissionById($this->get['submission_id']);
        if (!$submission || !$submission->getCompany()->isEqual($this->companyEntity)) {
            $this->redirect('view', ['company_id' => $this->companyEntity->getId()]);
        }

        $this->template->submission = $this->formSubmissionViewFactory->createFromId($this->get['submission_id']);
        $this->template->submissionErrors = dibi::select('*')->from('ch_form_submission_error')->where('form_submission_id=%i', $this->get['submission_id'])->execute()->fetchAll();
        $this->template->envelopes = $this->getComponentEnvelopes();

        // tabs
        $this->tabs['view'] = array('title' => $this->company->getCompanyName(), 'params' => "company_id={$this->company->getCompanyId()}");
        $this->tabs['formSubmission'] = array('title' => 'Form submission', 'params' => array("company_id" => $this->company->getCompanyId(), 'submission_id' => $this->get['submission_id']));
    }

    private function getComponentEnvelopes()
    {
        $envelopes = dibi::select('*')->from('ch_envelope')->where('form_submission_id=%i', $this->get['submission_id'])->execute()->fetchAll();
        foreach ($envelopes as $envelope) {
            // xml request
            $xmlRequestPath = $this->getEnvelopeXmlRequestPath($envelope->envelope_id);
            if (is_file($xmlRequestPath)) {
                $envelope['xmlRequest'] = $this->router->link(NULL, "company_id={$this->company->getCompanyId()}", "submission_id={$this->get['submission_id']}", "xml_request={$envelope->envelope_id}");
            }
            // xml response
            $xmlResponsePath = $this->getEnvelopeXmlResponsePath($envelope->envelope_id);
            if (is_file($xmlResponsePath)) {
                $envelope['xmlResponse'] = $this->router->link(NULL, "company_id={$this->company->getCompanyId()}", "submission_id={$this->get['submission_id']}", "xml_response={$envelope->envelope_id}");
            }
        }
        return $envelopes;
    }

    private function getEnvelopeXmlRequestPath($envelopeId)
    {
        $xmlPath = CHFiling::getDocPath() . '/_submission/' . CHFiling::getDirName($envelopeId) . '/envelope-' . $envelopeId . '/request.xml';
        return $xmlPath;
    }

    private function getEnvelopeXmlResponsePath($envelopeId)
    {
        $xmlPath = CHFiling::getDocPath() . '/_submission/' . CHFiling::getDirName($envelopeId) . '/envelope-' . $envelopeId . '/response.xml';
        return $xmlPath;
    }

    /*     * ***************************** resubmitFormSubmission ********************************** */

    public function handleResubmitFormSubmission()
    {
        $submissionId = $this->getParameter('submission_id');
        if (!$submissionId) {
            $this->redirect('view', array('company_id' => $this->company->getCompanyId()));
        }

        $this->resubmitFormSubmission($submissionId);
        $this->redirect('view', array('company_id' => $this->company->getCompanyId()));
    }

    /**
     * @param int $submissionId
     */
    private function resubmitFormSubmission($submissionId)
    {
        try {
            $this->submissionService->resubmitFormSubmissionById($submissionId);
            $this->flashMessage("Form submission has been resubmitted successfully.");
        } catch (FormSubmissionNotFoundException $e) {
            $this->flashMessage($e->getMessage(), 'error');
        } catch (RequestException $e) {
            $this->flashMessage(
                'Error response received from Companies House, check form submission for more details.',
                'error'
            );
        } catch (Exception $e) {
            $message = sprintf(
                'Request for form submission could not be created: %s. Inform the Technical department about this issue',
                $e->getMessage()
            );

            $this->flashMessage($message, 'error');
            $this->logger->error($e, array('formSubmissionId' => $submissionId, 'message' => $message));
        }
    }

    /*     * ***************************** removeSubmissionError ********************************** */

    public function handleRemoveSubmissionError()
    {

        if (!isset($this->get['submission_id'])) {
            $this->redirect('view', "company_id={$this->company->getCompanyId()}");
        }

        dibi::update('ch_form_submission', array('response' => NULL))
            ->where('form_submission_id=%i', $this->get['submission_id'])
            ->execute();

        if ($this->get['type'] == 'AnnualReturn') {
            try {
                $arModel = AnnualReturnServiceModel::getByCompanyId($this->company->getCompanyId());
                $arModel->statusId = AnnualReturnServiceModel::STATUS_DRAFT;
                $arModel->save();
            } catch (Exception $e) {
                // doesn't have service
            }
        }

        $this->flashMessage('Error has been removed');
        $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
    }

    /*     * ***************************** sync ********************************** */

    public function handleSync()
    {
        try {
            $this->company->syncAllData();
            $this->flashMessage('Company has been synchronized.');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
        }
        $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
    }

    /*     * ***************************** summary PDF ********************************** */

    public function handleSummaryPdf()
    {
        try {
            $data = $this->company->getData();
            $summary = CompanySummary::getCompanySummary($data['company_details'], $data['registered_office'], $data['sail_address'], $data['directors'], $data['secretaries'], $data['shareholders'], $data['capitals']);
            $summary->outputCompanySummaryPdf();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
    }

    /*     * ***************************** transfer ********************************** */

    public function handleTransfer()
    {
        $newCustomerId = $this->getParameter('transfer_id');

        if ($newCustomerId) {
            $newCustomer = $this->customerService->getCustomerById($newCustomerId);
            $this->companyTransferrer->transfer($this->companyEntity, $newCustomer);
            $this->flashMessage('Company has been transfered.');
            $this->redirect('view', ['company_id' => $this->companyEntity->getCompanyId()]);
        }
    }

    public function prepareTransfer()
    {
        $form = new FForm('customers', 'get');
        $form->addFieldset('Customer Search');
        $form->addText('firstName', 'First name: ');
        $form->addText('lastName', 'Last name: ');
        $form->addText('email', 'Email: ');
        $form->addSubmit('customersSearch', 'Search')->class('btn');
        $form->start();
        $this->template->form = $form;

        // filter
        $where = array();
        if ($form->isOk2Save()) {
            $data = $form->getValues();
            if (!empty($data['firstName'])) {
                $where['firstName'] = $data['firstName'];
            }
            if (!empty($data['lastName'])) {
                $where['lastName'] = $data['lastName'];
            }
            if (!empty($data['email'])) {
                $where['email'] = $data['email'];
            }
            $form->clean();
        }

        // paginator
        $count = Customer::getCustomersCount($where);
        $paginator = new FPaginator2($count);
        $paginator->itemsPerPage = 25;
        $paginator->htmlDisplayEnabled = FALSE;
        $this->template->paginator = $paginator;
        $this->template->customers = Customer::getCustomers($where, $paginator->getLimit(), $paginator->getOffset());

        // tabs
        $this->tabs['view'] = array('title' => $this->company->getCompanyName(), 'params' => "company_id={$this->company->getCompanyId()}");
        $this->tabs['transfer'] = array('title' => 'Transfer', 'params' => "company_id={$this->company->getCompanyId()}");
    }

    /*     * ***************************** delete ********************************** */

    public function handleDelete()
    {
        try {
            $this->company->delete();
            $this->flashMessage('Company has been deleted');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $this->redirect('list');
    }

    /*     * ********************************************************************** */
    /*     * ***************************** BARCLAYS  ****************************** */
    /*     * ********************************************************************** */

    /*     * ***************************** view  ****************************** */

    public function prepareCUviewBarclaysData()
    {
        // tabs
        $this->tabs['view'] = array('title' => $this->company->getCompanyName(), 'params' => "company_id={$this->company->getCompanyId()}");
        $this->tabs['CUviewBarclaysData'] = array('title' => 'Barclays Data', 'params' => "company_id={$this->company->getCompanyId()}");

        $this->template->companyCustomer = $this->bankingService->getLatestBankDetailsByCompany($this->companyEntity, CompanyCustomer::BANK_TYPE_BARCLAYS);
        $this->template->readonly = CompanyCustomerModel::readonly($this->company);
    }

    /*     * ***************************** edit  ****************************** */

    public function prepareCUeditBarclaysData()
    {
        // check if company customer is not readonly
        if (CompanyCustomerModel::readonly($this->company)) {
            $this->flashMessage('Company Customer data are readonly', 'error');
            $this->redirect('CUviewBarclaysData', "company_id={$this->companyId}");
        }

        // tabs
        $this->tabs['view'] = array('title' => $this->company->getCompanyName(), 'params' => "company_id={$this->company->getCompanyId()}");
        $this->tabs['CUviewBarclaysData'] = array('title' => 'Barclays Data', 'params' => "company_id={$this->company->getCompanyId()}");
        $this->tabs['CUeditBarclaysData'] = array('title' => 'Edit', 'params' => "company_id={$this->company->getCompanyId()}");
    }

    public function renderCUeditBarclaysData()
    {
        try {
            $this->template->form = CompanyCustomerModel::getComponentAdminForm(
                [
                    $this,
                    'Form_companyCustomerFormSubmitted'
                ],
                $this->customer,
                $this->company,
                CompanyCustomer::BANK_TYPE_BARCLAYS
            );
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('CUviewBarclaysData', "company_id={$this->companyId}");
        }
    }

    public function Form_companyCustomerFormSubmitted(FForm $form)
    {
        try {
            CompanyCustomerModel::saveAdminForm($form, $this->customer, $this->company, CompanyCustomer::BANK_TYPE_BARCLAYS);
            $this->flashMessage('Data has been saved');
            $this->redirect('CUviewBarclaysData', "company_id={$this->company->getCompanyId()}");
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    /*     * ***************************** resend  ****************************** */

    public function handleCUresendBarclaysData()
    {
        try {
            // check if company customer is not readonly
            if (CompanyCustomerModel::readonly($this->company)) {
                throw new Exception('Company has been already sent to barclays');
            }
            CompanyCustomerModel::resendBarclaysData($this->company);
            $this->flashMessage('Company data has been sent to Barclays');
            $this->redirect('CUviewBarclaysData', "company_id={$this->companyId}");
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('CUviewBarclaysData', "company_id={$this->companyId}");
        }
    }

    public function prepareCUviewTsbData()
    {
        $this->tabs['view'] = ['title' => $this->company->getCompanyName(), 'params' => "company_id={$this->company->getCompanyId()}"];
        $this->tabs['CUviewTsbData'] = ['title' => 'TSB Data', 'params' => "company_id={$this->company->getCompanyId()}"];

        $this->template->tsbData = $this->bankingService->getLatestBankDetailsByCompany($this->companyEntity, CompanyCustomer::BANK_TYPE_TSB);
        if ($this->template->tsbData && !$this->bankingService->getTsbLeadByCompany($this->companyEntity)) {
            $this->flashMessage('Lead has been submitted but Date sent will be blank on our end. It will be updated tomorrow.', 'info2');
        }
    }

    public function prepareCUeditTsbData()
    {
        if (CompanyCustomerModel::readonly($this->company)) {
            $this->flashMessage('Company Customer data are readonly', 'error');
            $this->redirect('CUviewTsbData', ['company_id' => $this->companyId]);
        }

        $this->tabs['view'] = ['title' => $this->company->getCompanyName(), 'params' => "company_id={$this->company->getCompanyId()}"];
        $this->tabs['CUviewTsbData'] = ['title' => 'TSB Data', 'params' => "company_id={$this->company->getCompanyId()}"];
        $this->tabs['CUeditTsbData'] = ['title' => 'Edit', 'params' => "company_id={$this->company->getCompanyId()}"];
    }

    public function renderCUeditTsbData()
    {
        try {
            $form = new TsbContactDetailsForm('TSB');
            $form->startup([$this, 'Form_TsbFormSubmitted'], $this->customerEntity, $this->bankingService, TRUE);
            $this->template->form = $form;
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('CUviewTsbData', ['company_id' => $this->companyId]);
        }
    }

    /**
     * @param TsbContactDetailsForm $form
     */
    public function Form_TsbFormSubmitted(TsbContactDetailsForm $form)
    {
        try {
            $form->process($this->customerEntity, $this->companyEntity);
            $tsbDetails = $this->bankingService->getLatestBankDetailsByCompany($this->companyEntity, CompanyCustomer::BANK_TYPE_TSB);
            /** @var $eMailer TsbApplicationEmailer */
            $eMailer = $this->getService('banking_module.tsb_application_emailer');
            $eMailer->sendOnlineApplication($tsbDetails);
            $this->flashMessage('TSB lead has been submitted. Customer has been emailed from TSB. Please direct them to follow the email to easily apply online.');
            $this->redirect('CUviewTsbData', ['company_id' => $this->companyId]);
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    /*     * ***************************** other ********************************** */

    private function getComponentArticle()
    {
        $articles = array();
        if ($this->company->getLastFormSubmission()->getMemarts() !== NULL) {
            $articles['article'] = $this->company->getLastFormSubmission()->getMemarts();
        }
        if ($this->company->getLastFormSubmission()->getSuppnameauth() !== NULL) {
            $articles['support'] = $this->company->getLastFormSubmission()->getSuppnameauth();
        }
        return $articles;
    }

    public function handleRestoreDeletedCompany()
    {
        try {

            $companyDeletedInfo = (array) DeleteCompany::getDeletedCompanyById($this->company->getCompanyId());
            $companyInfo = DeleteCompany::isOk2Restore($companyDeletedInfo['companyNumber']);
            if ($companyInfo && $companyInfo->company_id != $companyDeletedInfo['companyId']) {
                $this->flashMessage('This company already belongs to another customer! It may have been re-imported.', 'error');
            } else {
                DeleteCompany::restoreDeletedCompany($companyDeletedInfo['companyId'], $companyDeletedInfo['companyNumber']);
                $this->company->unMarkDeleted();
                $this->flashMessage('The company was restored !');
            }
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
    }

    /* ************************** download other documents **************************** */

    public function handleDownloadOtherCompanyFile()
    {
        try {
            $documentId = $this->getParameter('document_id');
            if ($documentId) {
                $companyDocument = $this->companyDocumentService->getCompanyDocument($this->companyEntity, $documentId);
                $this->companyDocumentService->outputCompanyDocument($companyDocument);
            }
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('view', array('company_id' => $this->company->getCompanyId()));
        }
    }

    /* ************************** upload other documents ******************************* */

    public function prepareUploadOtherCompanyFiles()
    {
        $this->tabs['view'] = array('title' => $this->company->getCompanyName(), 'params' => "company_id={$this->company->getCompanyId()}");
        $this->tabs['UploadOtherCompanyFiles'] = array('title' => 'Upload Other Documents', 'params' => "company_id={$this->company->getCompanyId()}");
    }

    public function renderUploadOtherCompanyFiles()
    {
        $form = new CACOtherDocumentsUploadAdminForm($this->node->getId() . 'CACOtherDocumentsUploadAdminForm');
        $form->startup($this, array($this, 'Form_CACOtherDocumentsUploadAdminFormSubmitted'), $this->company);
        $this->template->form = $form;
    }

    /**
     * @param CACOtherDocumentsUploadAdminForm
     * @throws Exception
     */
    public function Form_CACOtherDocumentsUploadAdminFormSubmitted(CACOtherDocumentsUploadAdminForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('File(s) has been uploaded');
            $this->redirect('view', array('company_id' => $this->company->getCompanyId()));
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    /*     * ***************************** delete other documents ********************************** */

    public function handleDeleteOtherCompanyFile()
    {
        try {
            if (isset($this->get['document_id'])) {
                $companyDocument = $this->companyDocumentService->getCompanyDocument($this->companyEntity, $this->get['document_id']);
                $this->companyDocumentService->deleteCompanyDocument($companyDocument);
                $this->flashMessage('Your document was successfully deleted.');
                $this->redirect('view', array('company_id' => $this->company->getCompanyId()));
            }
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('view', array('company_id' => $this->company->getCompanyId()));
        }
    }

    /* ************************** upload other documents ******************************* */
    
    public function prepareCUPscOnlineRegister()
    {
        $this->setCompanyUpdateTabs('CUpscOnlineRegister', 'PSC Online Register');

        $this->template->entries = $this->entryService->getEntriesByCompany($this->companyEntity);
        $this->template->canAddCompanyStatement = $this->entryService->canAddCompanyStatement($this->companyEntity);
    }

    /*     * ***************************** psc ********************************** */

    public function prepareCUPscOnlineRegisterAddPersonPsc()
    {
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'person');
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $this->template->natureOfControls = $this->pscChoicesProvider->getNatureOfControlsTemplateStructure(PscChoicesProvider::PERSON, $this->company->getType());
        $this->template->natureOfControlsDescription = $this->pscChoicesProvider->getNatureOfControlsTemplateDescription(PscChoicesProvider::PERSON);
        $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());

        $this->template->form = $this->pscFormFactory
            ->getPersonAddForm(
                $this->company,
                function (PersonPscData $data, FForm $form) {
                    $psc = new PersonPsc($this->companyEntity);
                    PersonPscDataTransformer::mergeRegister($psc, $data);
                    $this->entryService->addEntry($psc);

                    $form->clean();
                    $this->flashMessage("Person PSC has been added");
                    $this->redirect('CUpscOnlineRegister', ['company_id' => $this->companyEntity->getId()]);
                },
                $prefillOfficers,
                $prefillAddress,
                TRUE
            );

        $this->setCompanyUpdateTabs('CUpscOnlineRegister', 'PSC Online Register');
        $this->setCompanyUpdateTabs('CUpscOnlineRegisterAddPersonPsc', 'Add Person PSC');
    }

    public function prepareCUPscOnlineRegisterEditPersonPsc()
    {
        $pscId = $this->getParameter('pscId');
        if (!$pscId) {
            $this->redirect('summary', ['company_id' => $this->company->getId()]);
        }

        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'person');
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $this->template->natureOfControls = $this->pscChoicesProvider->getNatureOfControlsTemplateStructure(PscChoicesProvider::PERSON, $this->company->getType());
        $this->template->natureOfControlsDescription = $this->pscChoicesProvider->getNatureOfControlsTemplateDescription(PscChoicesProvider::PERSON);
        $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());

        //todo: psc - getPscById? not entry
        /** @var PersonPsc $psc */
        $psc = $this->entryService->getCompanyEntryById($this->companyEntity, $pscId);
        $this->template->form = $this->pscFormFactory
            ->getPersonEditForm(
                $this->company,
                PersonPscDataTransformer::fromRegister($psc),
                function (PersonPscData $data, FForm $form) use ($psc) {
                    PersonPscDataTransformer::mergeRegister($psc, $data);
                    $this->entryService->updateEntry($psc);

                    $form->clean();
                    $this->flashMessage("Person PSC has been udpated");
                    $this->redirect('CUpscOnlineRegister', ['company_id' => $this->companyEntity->getId()]);
                },
                $prefillOfficers,
                $prefillAddress,
                TRUE
            );

        $this->setCompanyUpdateTabs('CUpscOnlineRegister', 'PSC Online Register');
        $this->setCompanyUpdateTabs('CUpscOnlineRegisterEditPersonPsc', 'Edit Person PSC');
    }

    public function prepareCUPscOnlineRegisterAddCorporatePsc()
    {
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'corporate');
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $this->template->natureOfControls = $this->pscChoicesProvider->getNatureOfControlsTemplateStructure(PscChoicesProvider::CORPORATE, $this->company->getType());
        $this->template->natureOfControlsDescription = $this->pscChoicesProvider->getNatureOfControlsTemplateDescription(PscChoicesProvider::CORPORATE);
        $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());

        $this->template->form = $this->pscFormFactory
            ->getCorporateAddForm(
                $this->company,
                function (CorporatePscData $data, FForm $form) {
                    $psc = new CorporatePsc($this->companyEntity);
                    CorporatePscDataTransformer::mergeRegister($psc, $data);
                    $this->entryService->addEntry($psc);

                    $form->clean();
                    $this->flashMessage("Corporate PSC has been added");
                    $this->redirect('CUpscOnlineRegister', ['company_id' => $this->companyEntity->getId()]);
                },
                $prefillOfficers,
                $prefillAddress,
                TRUE
            );

        $this->setCompanyUpdateTabs('CUpscOnlineRegister', 'PSC Online Register');
        $this->setCompanyUpdateTabs('CUpscOnlineRegisterAddCorporatePsc', 'Add Corporate PSC');
    }

    public function prepareCUPscOnlineRegisterEditCorporatePsc()
    {
        $pscId = $this->getParameter('pscId');
        if (!$pscId) {
            $this->redirect('summary', ['company_id' => $this->company->getId()]);
        }

        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'corporate');
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $this->template->natureOfControls = $this->pscChoicesProvider->getNatureOfControlsTemplateStructure(PscChoicesProvider::CORPORATE, $this->company->getType());
        $this->template->natureOfControlsDescription = $this->pscChoicesProvider->getNatureOfControlsTemplateDescription(PscChoicesProvider::CORPORATE);
        $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());

        /** @var CorporatePsc $psc */
        $psc = $this->entryService->getCompanyEntryById($this->companyEntity, $pscId);
        $this->template->form = $this->pscFormFactory
            ->getCorporateEditForm(
                $this->company,
                CorporatePscDataTransformer::fromRegister($psc),
                function (CorporatePscData $data, FForm $form) use ($psc) {
                    CorporatePscDataTransformer::mergeRegister($psc, $data);
                    $this->entryService->updateEntry($psc);

                    $form->clean();
                    $this->flashMessage("Corporate PSC has been udpated");
                    $this->redirect('CUpscOnlineRegister', ['company_id' => $this->companyEntity->getId()]);
                },
                $prefillOfficers,
                $prefillAddress,
                TRUE
            );

        $this->setCompanyUpdateTabs('CUpscOnlineRegister', 'PSC Online Register');
        $this->setCompanyUpdateTabs('CUpscOnlineRegisterEditCorporatePsc', 'Edit Corporate PSC');
    }

    public function prepareCUPscOnlineRegisterAddLegalPersonPsc()
    {
        //$prefillOfficers = self::getPrefillOfficers($this->company, $type = 'person');
        //$prefillAddress = self::getPrefillAdresses($this->company);
        //$this->template->jsPrefillOfficers = $prefillOfficers['js'];
        //$this->template->jsPrefillAdresses = $prefillAddress['js'];
        $this->template->natureOfControls = $this->pscChoicesProvider->getNatureOfControlsTemplateStructure(PscChoicesProvider::LEGAL_PERSON, $this->company->getType());
        $this->template->natureOfControlsDescription = $this->pscChoicesProvider->getNatureOfControlsTemplateDescription(PscChoicesProvider::LEGAL_PERSON);
        $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());

        $this->template->form = $this->pscFormFactory
            ->getLegalPersonAddForm(
                $this->company,
                function (LegalPersonPscData $data, FForm $form) {
                    $psc = new LegalPersonPsc($this->companyEntity);
                    LegalPersonPscDataTransformer::mergeRegister($psc, $data);
                    $this->entryService->addEntry($psc);

                    $form->clean();
                    $this->flashMessage("Legal person PSC has been added");
                    $this->redirect('CUpscOnlineRegister', ['company_id' => $this->companyEntity->getId()]);
                },
                [],
                [],
                TRUE
            );

        $this->setCompanyUpdateTabs('CUpscOnlineRegister', 'PSC Online Register');
        $this->setCompanyUpdateTabs('CUpscOnlineRegisterAddLegalPersonPsc', 'Add LegalPerson PSC');
    }

    public function prepareCUPscOnlineRegisterEditLegalPersonPsc()
    {
        $pscId = $this->getParameter('pscId');
        if (!$pscId) {
            $this->redirect('summary', ['company_id' => $this->company->getId()]);
        }

        //$prefillOfficers = self::getPrefillOfficers($this->company, $type = 'person');
        //$prefillAddress = self::getPrefillAdresses($this->company);
        //$this->template->jsPrefillOfficers = $prefillOfficers['js'];
        //$this->template->jsPrefillAdresses = $prefillAddress['js'];
        $this->template->natureOfControls = $this->pscChoicesProvider->getNatureOfControlsTemplateStructure(PscChoicesProvider::LEGAL_PERSON, $this->company->getType());
        $this->template->natureOfControlsDescription = $this->pscChoicesProvider->getNatureOfControlsTemplateDescription(PscChoicesProvider::LEGAL_PERSON);
        $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());

        //todo: psc - getPscById? not entry
        /** @var LegalPersonPsc $psc */
        $psc = $this->entryService->getCompanyEntryById($this->companyEntity, $pscId);
        $this->template->form = $this->pscFormFactory
            ->getLegalPersonEditForm(
                $this->company,
                LegalPersonPscDataTransformer::fromRegister($psc),
                function (LegalPersonPscData $data, FForm $form) use ($psc) {
                    LegalPersonPscDataTransformer::mergeRegister($psc, $data);
                    $this->entryService->updateEntry($psc);

                    $form->clean();
                    $this->flashMessage("Legal person PSC has been udpated");
                    $this->redirect('CUpscOnlineRegister', ['company_id' => $this->companyEntity->getId()]);
                },
                [],
                [],
                TRUE
            );

        $this->setCompanyUpdateTabs('CUpscOnlineRegister', 'PSC Online Register');
        $this->setCompanyUpdateTabs('CUpscOnlineRegisterEditLegalPersonPsc', 'Edit Person PSC');
    }

    public function prepareCUPscOnlineRegisterNoPscReason()
    {
        $form = new FForm('noPscs');
        $form->addCheckbox('noPscReason', $this->pscChoicesProvider->getNoPscReason(), 1);
        $form->addSubmit('save', 'Save')->class('btn btn-primary');
        $form->onValid = [$this, 'Form_CUPscOnlineRegisterNoPscReasonFormSubmitted'];
        $form->start();

        $this->template->form = $form;

        $this->setCompanyUpdateTabs('CUpscOnlineRegister', 'PSC Online Register');
        $this->setCompanyUpdateTabs('CUpscOnlineRegisterNoPscReason', 'Set no PSC reason');
    }

    /**
     * @param FForm $form
     */
    public function Form_CUPscOnlineRegisterNoPscReasonFormSubmitted(FForm $form)
    {
        $noPscReason = $form->getValue('noPscReason');

        if ($noPscReason) {
            $entry = new CompanyStatement($this->companyEntity, IPscAware::NO_INDIVIDUAL_OR_ENTITY_WITH_SIGNFICANT_CONTROL);
            $this->entryService->addEntry($entry);
            $this->flashMessage('Company statement has been saved');
        }

        $form->clean();
        $this->redirect('CUpscOnlineRegister', ['company_id' => $this->companyEntity->getId()]);
    }

    public function prepareCUPscOnlineRegisterCessatePsc()
    {
        $form = new CUCessateForm($this->node->getId() . '_cessateAdmin');
        $form->startup([$this, 'Form_CUPscOnlineRegisterCessatePscFormSubmitted']);

        $this->template->form = $form;

        $this->setCompanyUpdateTabs('CUpscOnlineRegister', 'PSC Online Register');
        $this->setCompanyUpdateTabs('CUpscOnlineRegisterCessatePsc', 'Cessate PSC');
    }

    /**
     * @param FForm $form
     */
    public function Form_CUPscOnlineRegisterCessatePscFormSubmitted(FForm $form)
    {
        /** @var Psc $psc */
        $psc = $this->entryService->getCompanyEntryById($this->companyEntity, $this->getParameter('pscId'));

        try {
            $date = new DateTime($form->getValue('date'));
            $this->entryService->cessatePsc($psc, $date);

            $form->clean();
            $this->flashMessage('PSC has been cessated.');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('CUpscOnlineRegister', ['company_id' => $this->companyEntity->getId()]);
        }

        $this->redirect('CUpscOnlineRegister', ['company_id' => $this->companyEntity->getId()]);
    }

    public function prepareCUPscOnlineRegisterWithdrawStatement()
    {
        $form = new CUWithdrawForm($this->node->getId() . '_withdrawAdmin');
        $form->startup([$this, 'Form_CUPscOnlineRegisterWithdrawStatementFormSubmitted']);

        $this->template->form = $form;

        $this->setCompanyUpdateTabs('CUpscOnlineRegisterWithdrawStatement', 'Withdraw statement');
    }

    /**
     * @param CUWithdrawForm $form
     */
    public function Form_CUPscOnlineRegisterWithdrawStatementFormSubmitted(CUWithdrawForm $form)
    {
        /** @var CompanyStatement $statement */
        $statement = $this->entryService->getCompanyEntryById($this->companyEntity, $this->getParameter('statementId'));

        try {
            $date = new DateTime($form->getValue('date'));
            $this->entryService->withdrawStatement($statement, $date);

            $form->clean();
            $this->flashMessage('Company statement has been withdrawn.');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }

        $this->redirect('CUpscOnlineRegister', ['company_id' => $this->companyEntity->getId()]);
    }

    /* ************************** private methods ******************************* */

    private function prepareViewCF()
    {
        try {
            if (!$this->company->getLastIncorporationFormSubmission()) {
                throw new Exception("Form submission for company `{$this->company->getCompanyName()}` doesn't exist.");
            }

            /** @var IncorporationSummaryViewFactory $incorporationSummaryViewFactory */
            $incorporationSummaryViewFactory = $this->getService('company_formation_module.views.incorporation_summary_view_factory');

            /** @var CompanyIncorporation $incorporation */
            $incorporation = $this->company->getLastIncorporationFormSubmission()->getForm();
            $this->template->companyName = $this->company->getCompanyName();
            $this->template->data = $incorporation->getData();
            $this->template->summaryView = $incorporationSummaryViewFactory->from($incorporation, $this->companyEntity);
            $this->template->companyType = $incorporation->getCompanyType();
            $this->template->documents = $this->getComponentArticle();
            $this->view = 'CFview';

            // show article
            if (isset($this->get['document_id'])) {
                try {
                    Debug::disableProfiler();
                    $this->company->getLastIncorporationFormSubmission()->getDocument($this->get['document_id'])->output();
                } catch (Exception $e) {
                    $this->flashMessage($e->getMessage());
                    $this->redirect(NULL, 'document_id=');
                }
            }

            $this->template->view = $this->controllerViewFactory->create($this->companyEntity);

            // submit button to companies house
            if ($this->company->getStatus() != 'pending' && $this->company->getStatus() != 'error') {
                $form = new FForm($this->action . '_' . $this->companyId);
                $form->addFieldset('Send application');
                $form->addSubmit('send', 'Submit to Companies House')->class('btn');

                // no articles - disabled buttom
                if ($incorporation->getCompanyType() != CompanyIncorporation::LIMITED_LIABILITY_PARTHENERSHIP) {
                    $articles = $this->company->getLastIncorporationFormSubmission()->getDocuments();
                    if (count($articles) == 0) {
                        $form['send']->disabled()->style('color: silver;');
                    }
                }

                $form->onValid = array($this, 'Form_viewFormSubmitted');
                $form->start();
                $this->template->form = $form;
            }

            $this->company = $incorporation;
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect('list');
        }
    }

    private function prepareViewCU()
    {
        if ($this->company->isDeleted()) {
            $companyDeletedInfo = (array) DeleteCompany::getDeletedCompanyById($this->company->getCompanyId());
            $companyInfo = DeleteCompany::isOk2Restore($companyDeletedInfo['companyNumber']);
            if ($companyInfo && $companyInfo->company_id != $companyDeletedInfo['companyId']) {
                $newCustomer = new Customer($companyInfo->customer_id);
                $this->template->newCustomer = $newCustomer;
            }
            $this->template->companyInfo = NULL; // patch to show the action submenu (added by Angelo, approved by Jared)
            $this->template->companyDeletedInfo = $companyDeletedInfo;
        }

        $this->template->summaryView = new CompanySummaryView($this->company);
        $this->template->data = $this->company->getData();
        $this->template->companyId = $this->company->getCompanyId();
        $this->template->documents = $this->company->getDocumentsList();
        $this->template->companyView = $this->companyViewFactory->create($this->companyEntity);
        $this->template->otherdocuments = CompanyOtherDocument::getAllObjects(NULL, NULL, array('companyId' => $this->company->getCompanyId()));
        $this->template->companyEntity = $this->companyEntity;
        $this->template->bankingService = $this->bankingService;
        $this->template->companyBankingView = $this->companyBankingView;
        $this->view = 'CUview';

        // CU show article
        if (isset($this->get['document_name'])) {
            Debug::disableProfiler();
            //get share certificate
            if (isset($this->get['subscriber'])) {
                $data = $this->company->getData();
                $num = $this->get['subscriber'] + 1;
                if (isset($data['subscribers'][$this->get['subscriber']]['corporate_name'])) {
                    if (FormSubmission::getCompanyIncorporationFormSubmission($this->company) != NULL) {
                        $companyIncorporation = FormSubmission::getCompanyIncorporationFormSubmission($this->company);
                        $sub = $companyIncorporation->getForm()->getIncCorporate($data['subscribers'][$this->get['subscriber']]['id'], 'SUB');
                    }
                    $corp = ShareCert::getCorporateCertificate($sub, $this->company->getCompanyName(), $this->company->getCompanyNumber(), $data['registered_office'], $num);
                    $corp->getPdfCertificate();
                } else {
                    // --- subscribers
                    if (FormSubmission::getCompanyIncorporationFormSubmission($this->company) != NULL) {
                        $companyIncorporation = FormSubmission::getCompanyIncorporationFormSubmission($this->company);
                        $sub = $companyIncorporation->getForm()->getIncPerson($data['subscribers'][$this->get['subscriber']]['id'], 'SUB');
                    }
                    $person = ShareCert::getPersonCertificate($sub, $this->company->getCompanyName(), $this->company->getCompanyNumber(), $data['registered_office'], $num);
                    $person->getPdfCertificate();
                }
            } else {
                try {
                    $this->company->getDocument($this->get['document_name']);
                } catch (Exception $e) {
                    $this->flashMessage($e->getMessage());
                    $this->redirect(NULL, array('document_name=', 'subscriber='));
                }
            }
        }
        // --- show rtf document ---
        if (isset($this->get['formid'])) {
            if (RTFDocuments::getFormSubmisionStatus($this->get['formid'])) {
                try {
                    $Rtf = new RTFDocuments($this->get['company_id'], $this->get['formid']);
                    $Rtf->output();
                } catch (Exception $e) {
                    $this->flashMessage($e->getMessage());
                    $this->redirect(NULL);
                }
            } else {
                $this->flashMessage('Your form id is not correct');
                $this->redirect(self::COMPANIES_PAGE, "company_id={$this->company->getCompanyId()}");
            }
        }

        $this->template->view = $this->controllerViewFactory->create($this->companyEntity);
    }
}
