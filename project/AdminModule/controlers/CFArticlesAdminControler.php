<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    CFArticlesAdminControler.php 2011-03-22 razvanp@madesimplegroup.com
 */

class CFArticlesAdminControler extends PageAdminControler 
{

    /** 
     * @var array 
     */
	protected $tabs = array(
		'page' => 'Page',
		'properties' => 'Properties',
		'layout' => 'Layout',
		'details' => 'Details',
		'menu' => 'Menu',
		'seo' => 'Seo'
	);
    
    /** 
     * @var string 
     */
	protected $templateExtendedFrom = array('BaseAdmin', 'PageAdmin');
    
    /** 
     * @var string 
     */
    static public $handleObject = 'CFArticlesModel';

    /**
     * @var CFArticlesModel
     */
    public $node;    
    
    public function renderProperties()
	{
         $form = new CFArticlesPropertiesAdminForm($this->node->getId() . '_CFArticlesPropertiesAdminForm');
        $form->startup($this, array($this, 'Form_CFArticlesPropertiesAdminFormSubmitted'), $this->node);
        $this->template->form = $form;
       
    }

    /**
     * @param CFArticlesPropertiesAdminForm $form
     */
    public function Form_CFArticlesPropertiesAdminFormSubmitted(CFArticlesPropertiesAdminForm $form) {
        try {
            //generate pdf from form and shareholder data
            $form->process();
            $this->flashMessage('Updated!');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $this->redirect();
    }
    
}
