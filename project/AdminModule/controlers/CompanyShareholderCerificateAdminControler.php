<?php

class CompanyShareholderCerificateAdminControler extends CompaniesAdminControler 
{
    
    /** 
     * const PAGE 
     */
    const SHAREHOLDERS_CERTIFICATE_PAGE = 1105;

    /**
     * @var string
     */
    static public $handleObject = 'CompanyShareholderCerificateModel';

    /**
     * @var CompanyShareholderCerificateModel
     */
    public $node;
    
    /**
     *
     * @var $tabs 
     */
    protected $tabs = array(
        CompaniesAdminControler::COMPANIES_PAGE => 'List',
    );

    /**
     * @var Company 
     */
    public $company;
    
    /**
     *
     * @var PersonSubscriber  
     */
    public $shareHolder;

    /**
     * @var DibiRow Object
     */
    public $shareHolderInfo;

    /**
     * @var int
     */
    public $shareHolderId;


    public function startup()
    {
        // add permission for staff
        $this->acl->allow('staff', 'page', array('view'));
        parent::startup();
    }

    public function beforePrepare()
    {
        parent::beforePrepare();

        $chFiling = new CHFiling();
        try {
            $this->company = $chFiling->getCompany($this->get['company_id']);
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), "error");
            $this->redirect(CompaniesAdminControler::COMPANIES_PAGE);
        }
        try {
            $this->shareHolder =  $this->node->getShareholderInformations($this->company, $this->get['shareHolder']);
            $this->shareHolderId = (int) $this->get['shareHolder'];
            $this->shareHolderInfo = $this->node->checkIsOkForPrint();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), "error");
            $this->redirect(CompaniesAdminControler::COMPANIES_PAGE . " view", "company_id={$this->company->getCompanyId()}");
        }

        $this->tabs['companyName'] = array('title' => $this->company->getCompanyName(), 'params' => array("company_id" => $this->company->getCompanyId(), "shareHolder" => $this->get['shareHolder']));
    }
    
    /**
     * 
     */
    public function prepareCompanyName()
    {
        $this->redirect(CompaniesAdminControler::COMPANIES_PAGE . " view", "company_id={$this->company->getCompanyId()}");
    }
    
    /**
     * Print ShareholderCerificate
     */
    public function prepareView()
    {
        $this->tabs['view'] = array('title' => 'Create certificate', 'params' => array("documentId" => $this->company->getCompanyId()));
        $address = (object) array_map("trim", $this->shareHolder->getAddress()->getFields());

        if ($address->premise && $address->street && $address->post_town && $address->country && ($address->postcode || $address->postcode == 0)) {
            $data = $this->company->getData();
            $number = !empty($data['shareholders']) ? array_search($this->shareHolderId, array_keys($data['shareholders'])) : 1;
            $number = $number > 0 ? $number + 1 : 1;
            $this->node->createPdfShareholderCerificate($this->shareHolderInfo, 'ShareholderCertificate.pdf', $number);
            exit;
        }
        $this->template->shareholder = $this->shareHolder->getFields();
        $this->template->share = $this->shareHolderInfo;
    }
    
    /**
     * render View
     */
    public function renderView()
    {
        
        $form = new CompanyAdminShareholderCerificateForm($this->node->getId() . '_printShareHolderCertificate');
        $form->startup($this, array($this, 'Form_companyShareholderFormSubmitted'), $this->shareHolderInfo, $this->company, 100);
        $this->template->form = $form;
       
    }

    /**
     * @param CompanyAdminShareholderCerificateForm $form
     */
    public function Form_companyShareholderFormSubmitted(CompanyAdminShareholderCerificateForm $form)
    {
        try {
            //generate pdf from form and shareholder data
            $form->process();
            $this->flashMessage('Address has been updated. Please download your certificate now');
            $this->redirect(CompaniesAdminControler::COMPANIES_PAGE . " view", "company_id={$this->company->getCompanyId()}");
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

}
