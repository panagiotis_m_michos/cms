<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    CHAdminControler.php 2009-03-19 divak@gmail.com
 */

abstract class CHAdminControler extends PageAdminControler
{
    public function startup()
    {        
        // CH configuration
        CHFiling::setDb(array(FApplication::$config['database']['host'], FApplication::$config['database']['username'], FApplication::$config['database']['password'], FApplication::$config['database']['database']));
        $chFiling =  FApplication::$config['chfiling'];
        CHFiling::setPackageReference($chFiling['packageReference']);
        CHFiling::setSenderId($chFiling['senderId']);
        CHFiling::setPassword($chFiling['password']);
        CHFiling::setEmailAddress($chFiling['emailAddress']);
        CHFiling::setGatewayTest($chFiling['gatewayTest']);
        
        CHFiling::setDocPath(PROJECT_DIR . '/temp/upload/ch_documents');
        parent::startup();
    }
    
    public function Validator_securityEyeColour($control, $error, $params)
    {
        $value = $control->getValue();
        if (String::upper($value) === 'BLA') {
            return $error;
        }
        return TRUE;
    }
}