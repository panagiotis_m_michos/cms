<?php
/**
 * @package     CMS
 * @subpackage     CMS 
 * @author         nikolais@madesimplegroup.com
 * @internal     project/controlers/admin/MemorandumArticlesAdminControler.php
 * @created     17/10/2012
 */

class MemorandumArticlesAdminControler extends ProductAdminControler
{
    /** @var string */
    static public $handleObject = 'MemorandumArticles';
    
    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'ProductAdmin';
    }
}