<?php
use Doctrine\Common\Cache\ApcCache;

/**
 * @TODO use cache module
 */
class CacheAdminControler extends BaseAdminControler
{
    /**
     * @var array
     */
    public $tabs = array(
        'cache' => 'Cache',
    );

    public function renderDefault()
    {
    }

    public function handleClearCache()
    {
        /** @var ApcCache $apcCache */
        $apcCache = $this->getService(DiLocator::CACHE_MEMORY);
        //$apcCache->flushAll();
        $apcCache->deleteAll();
        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator(DOCUMENT_ROOT . '/temp/cache/ui', RecursiveDirectoryIterator::SKIP_DOTS),
            RecursiveIteratorIterator::CHILD_FIRST
        );
        foreach ($files as $fileinfo) {
            $todo = ($fileinfo->isDir() ? 'rmdir' : 'unlink');
            $todo($fileinfo->getRealPath());
        }
        $this->flashMessage('Memory and ui cache cleared');
        $this->redirect('default');
    }
}