<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    PaymentTokenLogsAdminControler.php 2012-09-04 <razvanp@madesimplegroup.com>
 */

class PaymentTokenLogsAdminControler extends PageAdminControler
{
	/**
	 * @var string
	 */
	static public $handleObject = 'PaymentTokenLogsModel';
	
	/**
	 * @var PaymentTokenLogsModel
	 */
	public $node;
	
	/** @var array */
	protected $tabs = array(
		'list' => 'List',
	);
    
    protected $templateExtendedFrom = array('BaseAdmin', 'PageAdmin');
    
    /** @var array */
    public function beforePrepare()
    {
        parent::beforePrepare();
        
        try {
            $whitelist = array('list');
            if (!isset($this->get['tokenStatusId']) && !in_array($this->action, $whitelist)) {
                throw new Exception('Token Status Id is required!');
            }
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('list');
        }
    }

    
    /******************************************** List ********************************************/


	public function renderList()
	{
        $this->tabs['list'] = array('title'=>'Test');
        Debug::disableProfiler();
        $datasource = dibi::select('*')->from(TBL_TOKEN_LOGS)->orderBy('dtc', dibi::DESC);
        $grid = new PaymentTokenLogsDatagrid($datasource, 'tokenStatusId');
        $this->template->datagrid = $grid;
        
	}
    
    /******************************************** View ********************************************/
    
    /**
     * @return void
     */
    public function prepareView() {
        
        $this->tabs['view'] = array('title'=>'View', 'params'=> "tokenStatusId={$this->get['tokenStatusId']}");
        
    }
    
    public function renderView()
    {
        try {
            $error = $this->node->getTokenLogById($this->get['tokenStatusId']);
            $this->template->error = $error;
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('list');
        }
    }        
    
}