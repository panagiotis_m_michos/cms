<?php

use Entities\Company as CompanyEntity;
use PeopleWithSignificantControl\Forms\CorporatePscData;
use PeopleWithSignificantControl\Forms\LegalPersonPscData;
use PeopleWithSignificantControl\Forms\PersonPscData;
use PeopleWithSignificantControl\Forms\PscFormFactory;
use PeopleWithSignificantControl\Interfaces\IPscAware;
use PeopleWithSignificantControl\Providers\PscChoicesProvider;
use PeopleWithSignificantControl\Views\AnnualReturnSummaryView;
use Services\AnnualReturnService;
use Utils\Date;

class ARAdminControler extends CHAdminControler
{
    const ANNUAL_RETURN_PAGE = 1429;

    /**
     * @var PscFormFactory
     */
    private $pscFormFactory;

    /**
     * @var PscChoicesProvider
     */
    private $pscChoicesProvider;

    /**
     * @var array
     */
    protected $tabs = array(); // tabs are manually added in prepare methods

    /**
     * @var AnnualReturnService
     */
    protected $annualReturnService;

    /**
     * @var Company
     */
    protected $company;

    /**
     * @var Customer
     */
    protected $customer;

    /**
     * @var CHAnnualReturn
     */
    public $chAnnualReturn;

    /**
     * @var Shareholding
     */
    public $shareholding;

    /**
     * @var array
     */
    public $capitals;

    public function startup()
    {
        $this->pscChoicesProvider = $this->getService('people_with_significant_control_module.providers.psc_choices_provider');
        $this->pscFormFactory = $this->getService('people_with_significant_control_module.forms.form_factory');

        // add permission for staff
        $this->acl->allow('staff', 'page', array('view'));
        parent::startup();
    }

    public function beforePrepare()
    {
        try {
            //checking if company_id passed exept list, and admin Actions ('insertPage', 'editPage', 'deletePage')
            if (!isset($this->get['company_id']) && !in_array($this->action, array('list', 'insertPage', 'editPage', 'deletePage'))) {
                $this->redirect('CompaniesAdminControler::COMPANIES_PAGE list');
            } elseif (isset($this->get['company_id'])) {
                $this->company = Company::getCompany($this->get['company_id']);
                $this->customer = new Customer($this->company->getCustomerId());
                try {
                    // check if customer bought annual return
                    if (!$this->company->hasAnnualReturn()) {
                        throw new Exception('There is no Confirmation Statement product bought for this company');
                    }

                    $this->annualReturnService = $this->container->get(DiLocator::SERVICE_ANNUAL_RETURN);
                    $this->chAnnualReturn = $this->company->getAnnualReturn();
                    $this->shareholding = isset($this->get['shareholdingId']) ? $this->company->getAnnualReturn()->getShareholding($this->get['shareholdingId']) : $this->chAnnualReturn->getNewShareholding();

                    $this->tabs['CompaniesAdminControler::COMPANIES_PAGE list'] = 'List';
                    $this->tabs['CompaniesAdminControler::COMPANIES_PAGE view'] = array('title' => substr($this->company->getCompanyName(), 0, 30), 'params' => "company_id={$this->company->getCompanyId()}");

                } catch (Exception $e) {
                    $this->flashMessage($e->getMessage(), 'error');
                    $this->redirect('CompaniesAdminControler::COMPANIES_PAGE view', "company_id={$this->company->getCompanyId()}");
                }
            }
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect('CompaniesAdminControler::COMPANIES_PAGE list');
        }

        parent::beforePrepare();
    }

    public function beforeRender()
    {
        $this->template->showPscs = $this->chAnnualReturn->getMadeUpDate()
            && new DateTime($this->chAnnualReturn->getMadeUpDate()) >= new DateTime('2016-06-30');

        if ($this->company) {
            $this->template->company = $this->company;
            $this->template->companyId = $this->company->getCompanyId();
            // for menu
            if ($this->company->getAnnualReturnId() == AnnualReturn::ANNUAL_RETURN_SERVICE_PRODUCT) {
                $this->template->hasAnnualService = 1;
            }
        }
        if ($this->customer) {
            $this->template->customer = $this->customer;
            $this->template->customerId = $this->customer->getId();
        }

        parent::beforeRender();
    }

    public function prepareCompanyDetails()
    {
        $officers = $this->chAnnualReturn->getNotSyncedOfficers();

        $data = AnnualReturnModel::getCompanyDetailsData($this->chAnnualReturn);
        $this->template->form = AnnualReturnModel::getCompomentCompanyDetailsForm($this, $officers, $data, 'admin');
        $this->template->officers = $officers;
        $this->template->data = $data;
        $this->template->companyCategory = $this->chAnnualReturn->getCompanyCategory();
        $this->tabs['companyDetails'] = array('title' => 'Confirmation Statement', 'params' => "company_id={$this->company->getCompanyId()}");
    }

    /**
     * @param FForm $form
     */
    public function FormCompanyDetailsSubmitted(FForm $form)
    {
        try {
            $data = $form->getValues();
            $data['returnDate'] = !empty($data['returnDate']) ? Date::changeFormat($data['returnDate'], 'd-m-Y', 'Y-m-d') : NULL;
            AnnualReturnModel::saveCompanyDetails($this->chAnnualReturn, $data);
            $form->clean();
            $this->flashMessage('Company details has been saved');

            if ($this->company->getType() == CompanyEntity::COMPANY_CATEGORY_LLP) {
                $this->redirect('summary', "company_id={$this->company->getCompanyId()}");
            } else {
                $this->redirect('capitals', "company_id={$this->company->getCompanyId()}");
            }
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    public function prepareCapitals()
    {
        if ($this->company->getType() == CompanyEntity::COMPANY_CATEGORY_LLP) {
            $this->redirect('summary', "company_id={$this->company->getCompanyId()}");
        }
        $form = new CapitalsAdminForm('capital');
        $form->startup($this, $this->chAnnualReturn->getFormSubmissionId(), array($this, 'FormCapitalsSubmitted'), $this->annualReturnService);
        $this->template->form = $form;
        $this->template->capitals = $form->decorateCapitals($this->annualReturnService->getAllCapitals($this->chAnnualReturn->getFormSubmissionId()));
        $this->tabs['capitals'] = array('title' => 'Confirmation Statement', 'params' => "company_id={$this->company->getCompanyId()}");
    }

    /**
     * @param CapitalsAdminForm $form
     */
    public function FormCapitalsSubmitted(CapitalsAdminForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Capitals has been saved');
            $this->redirect('shareholdings', "company_id={$this->company->getCompanyId()}");
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    public function prepareAddCapital()
    {
        $form = new AddCapitalAdminForm('add_capital');
        $form->startup($this, $this->chAnnualReturn->getFormSubmissionId(), array($this, 'FormAddCapitalSubmitted'), $this->annualReturnService);
        $this->template->form = $form;

        $this->tabs['capitals'] = array('title' => 'Confirmation Statement', 'params' => "company_id={$this->company->getCompanyId()}");
    }

    /**
     * @param AddCapitalAdminForm $form
     */
    public function FormAddCapitalSubmitted(AddCapitalAdminForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Capital has been added');
            $this->redirect('capitals', "company_id={$this->company->getCompanyId()}");
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    public function prepareAddCapitalShare()
    {
        if (!isset($this->get['capital_id'])) {
            $this->flashMessage('Capital id must be provided');
            $this->redirect('capitals', "company_id={$this->company->getCompanyId()}");
        }

        $capital = $this->annualReturnService->findCapital($this->get['capital_id']);
        $form = new AddCapitalShareAdminForm('add_capital_share');
        $form->startup($this, $capital, array($this, 'FormAddCapitalShareSubmitted'), $this->annualReturnService);
        $this->template->form = $form;
        $this->template->capital = $capital;
        $this->tabs['capitals'] = array('title' => 'Confirmation Statement', 'params' => "company_id={$this->company->getCompanyId()}");
    }

    /**
     * @param AddCapitalShareAdminForm $form
     */
    public function FormAddCapitalShareSubmitted(AddCapitalShareAdminForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Capital Share has been saved');
            $this->redirect('capitals', "company_id={$this->company->getCompanyId()}");
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    public function renderRemoveCapital()
    {
        if (!isset($this->get['capital_id'])) {
            $this->flashMessage('Capital id must be provided');
            $this->redirect('capitals', "company_id={$this->company->getCompanyId()}");
        }
        $capital = $this->annualReturnService->findCapital($this->get['capital_id']);
        $this->annualReturnService->removeCapital($capital);
        $this->flashMessage('Capital has been removed');
        $this->redirect('capitals', "company_id={$this->company->getCompanyId()}");
    }

    public function renderRemoveCapitalShare()
    {
        if (!isset($this->get['share_id'])) {
            $this->flashMessage('Share id must be provided');
            $this->redirect('capitals', "company_id={$this->company->getCompanyId()}");
        }
        $share = $this->annualReturnService->findCapitalShare($this->get['share_id']);

        $this->annualReturnService->removeCapitalShare($share);
        $this->flashMessage('Share has been removed');
        $this->redirect('capitals', "company_id={$this->company->getCompanyId()}");
    }

    public function handleShareholdings()
    {
        if ($this->company->getType() == CompanyEntity::COMPANY_CATEGORY_LLP) {
            $this->redirect('summary', "company_id={$this->company->getCompanyId()}");
        }

        if (isset($this->get['removeAllAddresses']) && 1 == $this->get['removeAllAddresses']) {
            try {
                AnnualReturnModel::removeAllShareholderAddresses($this->chAnnualReturn->getShareholdings());
                $this->flashMessage('Shareholdings addresses has been removed!');
                $this->redirect('shareholdings', "company_id={$this->company->getCompanyId()}");
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
                $this->redirect('shareholdings', "company_id={$this->company->getCompanyId()}");
            }
        }
    }

    public function prepareShareholdings()
    {
        $shareholdings = $this->chAnnualReturn->getShareholdings();
        $this->template->shareholdings = AnnualReturnModel::getShareholdingsArray($shareholdings);
        $this->template->form = AnnualReturnModel::getCompomentShareholdingsForm($this, 'admin');

        $this->tabs['shareholdings'] = array('title' => 'Confirmation Statement', 'params' => "company_id={$this->company->getCompanyId()}");
    }

    public function FormShareholdingsSubmitted()
    {
        $this->redirect('pscs', "company_id={$this->company->getCompanyId()}");
    }

    public function prepareAddShareholding()
    {
        $this->template->form = AnnualReturnModel::getCompomentShareholdingForm($this, 'insert', array(), 'admin');

        $this->tabs['shareholdings'] = array('title' => 'Confirmation Statement', 'params' => "company_id={$this->company->getCompanyId()}");
        $this->tabs['addShareholding'] = array('title' => 'Add Shareholding', 'params' => "company_id={$this->company->getCompanyId()}");
    }

    /**
     * @param FForm $form
     */
    public function FormAddShareholdingSubmitted(FForm $form)
    {
        try {
            $data = $form->getValues();
            $data['isRemoved'] = 0;
            AnnualReturnModel::addShareholding($this->shareholding, $data);
            $form->clean();
            $this->flashMessage('Shareholdings has been created');
            $this->redirect('shareholdings', "company_id={$this->company->getCompanyId()}");
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect(NULL, "company_id={$this->company->getCompanyId()}");
        }
    }

    public function prepareEditShareholding()
    {
        if (!isset($this->get['shareholdingId'])) {
            $this->redirect('shareholdings', "company_id={$this->company->getCompanyId()}");
        }

        $data = AnnualReturnModel::getShareholdingArray($this->shareholding);
        $this->template->form = AnnualReturnModel::getCompomentShareholdingForm($this, 'edit', $data, 'admin');

        $this->tabs['shareholdings'] = array('title' => 'Confirmation Statement', 'params' => "company_id={$this->company->getCompanyId()}");
        $this->tabs['editShareholding'] = array('title' => 'Edit Shareholding', 'params' => "company_id={$this->company->getCompanyId()}");
    }

    /**
     * @param FForm $form
     */
    public function FormEditShareholdingSubmitted(FForm $form)
    {
        try {
            $data = $form->getValues();
            AnnualReturnModel::editShareholding($this->shareholding, $data);
            $form->clean();
            $this->flashMessage('Shareholdings has been updated');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $this->redirect('shareholdings', "company_id={$this->company->getCompanyId()}");
    }

    public function prepareRemoveShareholding()
    {
        try {
            AnnualReturnModel::removeShareholding($this->shareholding);
            $this->flashMessage('Shareholding has been deleted');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $this->redirect('shareholdings', "company_id={$this->company->getCompanyId()}");
    }

    public function prepareAddTransfer()
    {
        if (!isset($this->get['shareholdingId'])) {
            $this->redirect('shareholdings', "company_id={$this->company->getCompanyId()}");
        }
        $this->template->form = AnnualReturnModel::getCompomentAddTransferForm($this, 'admin');

        $this->tabs['shareholdings'] = array('title' => 'Confirmation Statement', 'params' => "company_id={$this->company->getCompanyId()}");
        $this->tabs['addTransfer'] = array('title' => 'Add Transfer', 'params' => "company_id={$this->company->getCompanyId()}");
    }

    /**
     * @param FForm $form
     */
    public function FormAddTransferSubmitted(FForm $form)
    {
        try {
            $data = $form->getValues();
            AnnualReturnModel::addTransfer($this->company, $this->shareholding, $data);
            $form->clean();
            $this->flashMessage('Shareholdings has been updated');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $this->redirect('shareholdings', "company_id={$this->company->getCompanyId()}");
    }

    public function prepareRemoveTransfer()
    {
        if (!isset($this->get['shareholdingId'], $this->get['transferId'])) {
            $this->redirect('shareholdings', "company_id={$this->company->getCompanyId()}");
        }

        try {
            AnnualReturnModel::removeTransfer($this->shareholding, $this->get['transferId']);
            $this->flashMessage('Transfer has been deleted');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $this->redirect('shareholdings', "company_id={$this->company->getCompanyId()}");
    }

    public function handleSummary()
    {
        if (isset($this->get['pdf'])) {
            $return = AnnualReturnModel::getSummaryDetails($this->chAnnualReturn, $this->company);
            try {
                CHFiling::getPdfAnnualreturnSummary($return);
                $this->terminate();
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
                $this->redirect();
            }
        }
    }

    //PSCs
    public function preparePscs()
    {
        $showPscs = $this->chAnnualReturn->getMadeUpDate()
            && new DateTime($this->chAnnualReturn->getMadeUpDate()) >= new DateTime('2016-06-30');

        if (!$showPscs) {
            $this->redirect('summary', ["company_id" => $this->company->getCompanyId()]);
        }

        $this->tabs['pscs'] = ['title' => 'Confirmation Statement', 'params' => "company_id={$this->company->getCompanyId()}"];
        $this->template->summaryView = new AnnualReturnSummaryView($this->chAnnualReturn);
        $this->template->form = AnnualReturnModel::getCompomentPscssForm($this, 'admin');
    }

    public function FormPscsSubmitted()
    {
        $this->redirect('summary', ["company_id" => $this->company->getCompanyId()]);
    }

    public function prepareAddPersonPsc()
    {
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'person');
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $this->template->natureOfControls = $this->pscChoicesProvider->getNatureOfControlsTemplateStructure(PscChoicesProvider::PERSON, $this->company->getType());
        $this->template->natureOfControlsDescription = $this->pscChoicesProvider->getNatureOfControlsTemplateDescription(PscChoicesProvider::PERSON);
        $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());

        $this->template->form = $this->pscFormFactory
            ->getPersonAddForm(
                $this->company,
                function (PersonPscData $personPscData, FForm $form) {
                    $pscPerson = new AnnualReturnPersonPsc($this->chAnnualReturn->getFormSubmissionId(), $this->get['psc_id']);
                    PersonPscDataTransformer::merge($pscPerson, $personPscData);
                    $pscPerson->save();

                    $this->chAnnualReturn->setNoPscReason(NULL);
                    $this->chAnnualReturn->save();

                    $form->clean();
                    $this->redirect('pscs', "company_id={$this->company->getCompanyId()}");
                },
                $prefillOfficers,
                $prefillAddress,
                TRUE
            );

        $this->tabs['pscs'] = [
            'title' => 'Confirmation Statement',
            'params' => "company_id={$this->company->getCompanyId()}"
        ];
        $this->tabs['addPersonPsc'] = [
            'title' => 'Add person PSC',
            'params' => "company_id={$this->company->getCompanyId()}"
        ];
    }

    public function prepareEditPersonPsc()
    {
        if (!isset($this->get['psc_id'])) {
            $this->redirect('summary', "company_id={$this->company->getCompanyId()}");
        }

        if ($this->chAnnualReturn->getCompanyCategory() === CompanyEntity::COMPANY_CATEGORY_LLP) {
            $this->redirect('summary', "company_id={$this->company->getCompanyId()}");
        }

        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'person');
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $this->template->natureOfControls = $this->pscChoicesProvider->getNatureOfControlsTemplateStructure(PscChoicesProvider::PERSON, $this->company->getType());
        $this->template->natureOfControlsDescription = $this->pscChoicesProvider->getNatureOfControlsTemplateDescription(PscChoicesProvider::PERSON);
        $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());

        $personPsc = new AnnualReturnPersonPsc($this->chAnnualReturn->getFormSubmissionId(), $this->get['psc_id']);
        $this->template->form = $this->pscFormFactory
            ->getPersonEditForm(
                $this->company,
                PersonPscDataTransformer::from($personPsc),
                function (PersonPscData $personPscData, FForm $form) {
                    $pscPerson = new AnnualReturnPersonPsc($this->chAnnualReturn->getFormSubmissionId(), $this->get['psc_id']);
                    PersonPscDataTransformer::merge($pscPerson, $personPscData);
                    $pscPerson->save();

                    $this->chAnnualReturn->setNoPscReason(NULL);
                    $this->chAnnualReturn->save();


                    $form->clean();
                    $this->redirect('pscs', "company_id={$this->company->getCompanyId()}");
                },
                $prefillOfficers,
                $prefillAddress,
                TRUE
            );

        $this->tabs['pscs'] = [
            'title' => 'Confirmation Statement',
            'params' => "company_id={$this->company->getCompanyId()}"
        ];
        $this->tabs['editPersonPsc'] = [
            'title' => 'Edit person PSC',
            'params' => "company_id={$this->company->getCompanyId()}"
        ];
    }

    public function prepareAddLegalPersonPsc()
    {
        $this->template->natureOfControls = $this->pscChoicesProvider->getNatureOfControlsTemplateStructure(PscChoicesProvider::LEGAL_PERSON, $this->company->getType());
        $this->template->natureOfControlsDescription = $this->pscChoicesProvider->getNatureOfControlsTemplateDescription(PscChoicesProvider::LEGAL_PERSON);
        $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());

        $this->template->form = $this->pscFormFactory
            ->getLegalPersonAddForm(
                $this->company,
                function (LegalPersonPscData $legalPersonPscData, FForm $form) {
                    $pscLegalPerson = new AnnualReturnLegalPersonPsc($this->chAnnualReturn->getFormSubmissionId());
                    LegalPersonPscDataTransformer::merge($pscLegalPerson, $legalPersonPscData);
                    $pscLegalPerson->save();

                    $this->chAnnualReturn->setNoPscReason(NULL);
                    $this->chAnnualReturn->save();

                    $form->clean();
                    $this->redirect('pscs', "company_id={$this->company->getCompanyId()}");
                },
                [],
                [],
                TRUE
            );

        $this->tabs['pscs'] = [
            'title' => 'Confirmation Statement',
            'params' => "company_id={$this->company->getCompanyId()}"
        ];
        $this->tabs['addLegalPersonPsc'] = [
            'title' => 'Add legal person PSC',
            'params' => "company_id={$this->company->getCompanyId()}"
        ];
    }

    public function prepareEditLegalPersonPsc()
    {
        if (!isset($this->get['psc_id'])) {
            $this->redirect('pscs', "company_id={$this->company->getCompanyId()}");
        }

        if ($this->company->getType() === CompanyEntity::COMPANY_CATEGORY_LLP) {
            $this->redirect('summary', "company_id={$this->company->getCompanyId()}");
        }

        $this->template->natureOfControls = $this->pscChoicesProvider->getNatureOfControlsTemplateStructure(PscChoicesProvider::LEGAL_PERSON, $this->company->getType());
        $this->template->natureOfControlsDescription = $this->pscChoicesProvider->getNatureOfControlsTemplateDescription(PscChoicesProvider::LEGAL_PERSON);
        $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());

        $legalPersonPsc = new AnnualReturnLegalPersonPsc($this->chAnnualReturn->getFormSubmissionId(), $this->get['psc_id']);
        $this->template->form = $this->pscFormFactory
            ->getLegalPersonEditForm(
                $this->company,
                LegalPersonPscDataTransformer::from($legalPersonPsc),
                function (LegalPersonPscData $legalPersonPscData, FForm $form) {
                    $pscLegalPerson = new AnnualReturnLegalPersonPsc($this->chAnnualReturn->getFormSubmissionId(), $this->get['psc_id']);
                    LegalPersonPscDataTransformer::merge($pscLegalPerson, $legalPersonPscData);
                    $pscLegalPerson->save();

                    $this->chAnnualReturn->setNoPscReason(NULL);
                    $this->chAnnualReturn->save();

                    $form->clean();
                    $this->redirect('pscs', "company_id={$this->company->getCompanyId()}");
                },
                [],
                [],
                TRUE
            );

        $this->tabs['pscs'] = [
            'title' => 'Confirmation Statement',
            'params' => "company_id={$this->company->getCompanyId()}"
        ];
        $this->tabs['editLegalPersonPsc'] = [
            'title' => 'Edit legal person PSC',
            'params' => "company_id={$this->company->getCompanyId()}"
        ];
    }

    public function prepareAddCorporatePsc()
    {
        if ($this->company->getType() === CompanyEntity::COMPANY_CATEGORY_LLP) {
            $this->redirect('summary', "company_id={$this->company->getCompanyId()}");
        }

        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'corporate');
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $this->template->natureOfControls = $this->pscChoicesProvider->getNatureOfControlsTemplateStructure(PscChoicesProvider::CORPORATE, $this->company->getType());
        $this->template->natureOfControlsDescription = $this->pscChoicesProvider->getNatureOfControlsTemplateDescription(PscChoicesProvider::CORPORATE);
        $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());

        $this->template->form = $this->pscFormFactory
            ->getCorporateAddForm(
                $this->company,
                function (CorporatePscData $corporatePscData, FForm $form) {
                    $pscCorporate = new AnnualReturnCorporatePsc($this->chAnnualReturn->getFormSubmissionId());
                    CorporatePscDataTransformer::merge($pscCorporate, $corporatePscData);
                    $pscCorporate->save();

                    $this->chAnnualReturn->setNoPscReason(NULL);
                    $this->chAnnualReturn->save();


                    $form->clean();
                    $this->redirect('pscs', "company_id={$this->company->getCompanyId()}");
                },
                $prefillOfficers,
                $prefillAddress,
                TRUE
            );

        $this->tabs['pscs'] = [
            'title' => 'Confirmation Statement',
            'params' => "company_id={$this->company->getCompanyId()}"
        ];
        $this->tabs['addCorporatePsc'] = [
            'title' => 'Add corporate PSC',
            'params' => "company_id={$this->company->getCompanyId()}"
        ];
    }

    public function prepareEditCorporatePsc()
    {
        if (!isset($this->get['psc_id'])) {
            $this->redirect('pscs', "company_id={$this->company->getCompanyId()}");
        }

        if ($this->chAnnualReturn->getCompanyCategory() === CompanyEntity::COMPANY_CATEGORY_LLP) {
            $this->redirect('summary', "company_id={$this->company->getCompanyId()}");
        }

        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'corporate');
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $this->template->natureOfControls = $this->pscChoicesProvider->getNatureOfControlsTemplateStructure(PscChoicesProvider::CORPORATE, $this->company->getType());
        $this->template->natureOfControlsDescription = $this->pscChoicesProvider->getNatureOfControlsTemplateDescription(PscChoicesProvider::CORPORATE);
        $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());

        $corporatePsc = new AnnualReturnCorporatePsc($this->chAnnualReturn->getFormSubmissionId(), $this->get['psc_id']);
        $this->template->form = $this->pscFormFactory
            ->getCorporateEditForm(
                $this->company,
                CorporatePscDataTransformer::from($corporatePsc),
                function (CorporatePscData $corporatePscData, FForm $form) {
                    $pscCorporate = new AnnualReturnCorporatePsc($this->chAnnualReturn->getFormSubmissionId(), $this->get['psc_id']);
                    CorporatePscDataTransformer::merge($pscCorporate, $corporatePscData);
                    $pscCorporate->save();

                    $this->chAnnualReturn->setNoPscReason(NULL);
                    $this->chAnnualReturn->save();


                    $form->clean();
                    $this->redirect('pscs', "company_id={$this->company->getCompanyId()}");
                },
                $prefillOfficers,
                $prefillAddress,
                TRUE
            );

        $this->tabs['pscs'] = [
            'title' => 'Confirmation Statement',
            'params' => "company_id={$this->company->getCompanyId()}"
        ];
        $this->tabs['editCorporatePsc'] = [
            'title' => 'Edit corporate PSC',
            'params' => "company_id={$this->company->getCompanyId()}"
        ];
    }

    public function prepareNoPscReason()
    {
        $noPscsForm = new \Admin\CHForm('noPscsReason');
        $noPscsForm->addCheckbox('noPscReason', $this->getService('people_with_significant_control_module.providers.psc_choices_provider')->getNoPscReason(), 1);
        $noPscsForm->addSubmit('save', 'Save');
        $noPscsForm->onValid = [$this, 'Form_noPscsFormSubmitted'];
        $noPscsForm->start();
        $this->template->form = $noPscsForm;

        $this->tabs['pscs'] = [
            'title' => 'Confirmation Statement',
            'params' => "company_id={$this->company->getCompanyId()}"
        ];
        $this->tabs['noPscReason'] = [
            'title' => 'No PSC reason',
            'params' => "company_id={$this->company->getCompanyId()}"
        ];
    }

    /**
     * @param FForm $form
     */
    public function Form_noPscsFormSubmitted(FForm $form)
    {
        /** @var CHAnnualReturn $annualReturn */
        $annualReturn = $this->chAnnualReturn;
        $annualReturn->setNoPscReason($form->getValue('noPscReason') ? IPscAware::NO_INDIVIDUAL_OR_ENTITY_WITH_SIGNFICANT_CONTROL : NULL);
        $annualReturn->save();

        $form->clean();
        $this->flashMessage('PSC reason has been saved');
        $this->redirect('pscs', "company_id={$this->company->getCompanyId()}");
    }

    public function handleDeletePsc()
    {
        if (!isset($this->get['psc_id'])) {
            $this->redirect('pscs', "company_id={$this->company->getCompanyId()}");
        }

        $pscs = array_merge(
            $this->chAnnualReturn->getPersonPscs(),
            $this->chAnnualReturn->getLegalPersonPscs(),
            $this->chAnnualReturn->getCorporatePscs()
        );

        /** @var $pscs AnnualReturnPersonPsc[]|AnnualReturnLegalPersonPsc[]|AnnualReturnPersonPsc[] */
        foreach ($pscs as $psc) {
            if ($psc->getId() == $this->get['psc_id'] && !$psc->isAlreadyExistingPsc()) {
                $psc->remove();
                $this->flashMessage("PSC was successfully deleted!");
            }
        }

        $this->redirect('pscs', "company_id={$this->company->getCompanyId()}");
    }

    //Summary
    public function prepareSummary()
    {
        try {
            $return = AnnualReturnModel::getSummaryDetails($this->company->getAnnualReturn(), $this->company);
            $this->template->return = $return;
            $this->template->form = AnnualReturnModel::getCompomentAdminSummaryForm($this, $this->company, $return);

            // AnnualReturnServiceModel just for service
            if ($this->company->getAnnualReturnId() == AnnualReturn::ANNUAL_RETURN_SERVICE_PRODUCT) {
                $this->template->ars = AnnualReturnServiceModel::getByCompanyId($this->company->getCompanyId());
            }

            $this->tabs['summary'] = array('title' => 'Confirmation Statement', 'params' => "company_id={$this->company->getCompanyId()}");
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('CompaniesAdminControler::COMPANIES_PAGE view', "company_id={$this->company->getCompanyId()}");
        }
    }

    /**
     * @param FForm $form
     */
    public function FormSummaryFormSubmitted(FForm $form)
    {
        try {

            $data = $form->getValues();
            $data['made_up_date'] = !empty($data['made_up_date']) ? Date::changeFormat($data['made_up_date'], 'd-m-Y', 'Y-m-d') : NULL;
            $this->chAnnualReturn = AnnualReturnModel::saveAdminSummaryData($data);

            /* do nothing, data has been saved, just redirect */
            if ($form->isSubmitedBy("save")) {
                $this->flashMessage('Confirmation Statement has been saved.');
                $form->clean();
                $this->redirect();
            }

            if ($this->company->getAnnualReturnId() == AnnualReturn::ANNUAL_RETURN_SERVICE_PRODUCT) {
                if ($form->isSubmitedBy("send_email")) {
                    AnnualReturnServiceModel::processCompletedEmail($this->company, $this->customer);
                    $this->flashMessage('Email has been sent');
                } else {
                    $arsm = AnnualReturnServiceModel::getByCompanyId($this->company->getCompanyId());
                    AnnualReturnServiceModel::processAcceptedByCustomer($this->company, $this->chAnnualReturn, $arsm, $this->customer);
                    $this->flashMessage('Confirmation Statement application has been sent.');
                }
            } else {
                AnnualReturnModel::processSendApplication($this->company, $this->chAnnualReturn, $this->customer);
                $this->flashMessage('Confirmation Statement application has been sent.');
            }

            $form->clean();
            $this->redirect('CompaniesAdminControler::COMPANIES_PAGE view', "company_id={$this->company->getCompanyId()}");
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect();
        }
    }

    public function renderMissingDataEmail()
    {
        $annualReturnsEmailer = $this->emailerFactory->get(EmailerFactory::ANNUAL_RETURN);
        $email = $annualReturnsEmailer->serviceMissingInfoEmail();

        $form = new FForm('MissingDataEmail');
        $form->addFieldset('Data');
        $form->addFckArea('body', 'Text')
            ->setSize(500, 230)
            ->addRule(FForm::Required, 'Please provide text')
            ->setValue($email->body);
        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Submit')->class('btn');
        $form->onValid = array($this, 'FormMissingDataEmailSubmitted');
        $form->start();

        $this->template->form = $form;
        $this->template->title = 'Send Missing Data Email';
        debug::disableProfiler();
    }

    /**
     * @param FForm $form
     */
    public function FormMissingDataEmailSubmitted(FForm $form)
    {
        try {
            $data = $form->getValues();
            AnnualReturnServiceModel::processMissingDataEmail($this->company, $this->customer, $data);
            $form->clean();
            $this->flashMessage('Email has been sent');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $this->reloadOpener();
    }

    public function handleServiceCompletedEmail()
    {
        try {
            AnnualReturnServiceModel::processCompletedEmail($this->company, $this->customer);
            $this->flashMessage('Email has been sent');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $this->redirect('summary', "company_id={$this->company->getCompanyId()}");
    }

    public function prepareSync()
    {
        try {
            $this->chAnnualReturn->sync();
            $this->flashMessage('Company data has now been synced with the current information held at Companies house');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $this->redirect('companyDetails', "company_id={$this->company->getCompanyId()}");
    }

    public function prepareRemove()
    {
        try {
            //check if has Anual Return
            if ($this->company->hasAnnualReturn()) {

                // uncheck AR from company record
                Company::removeAnnualReturn($this->company->getCompanyId());

                //remove form submision
                $formsubmisionId = $this->chAnnualReturn->getFormSubmissionId();
                CHFiling::getDb()->delete('ch_form_submission', "form_submission_id = $formsubmisionId");

                //remove annual return service record
                //if ($this->company->getAnnualReturnId() == AnnualReturn::ANNUAL_RETURN_SERVICE_PRODUCT){
                //     CHFiling::getDb()->delete('cms2_annual_return_service', "companyId = $this->company->getCompanyId()");
                //}
                //remove annual return recods
                if (isset($this->chAnnualReturn)) {
                    CHFiling::getDb()->delete('ch_annual_return', "form_submission_id = $formsubmisionId");

                    if ($this->chAnnualReturn->getOfficers() != NULL) {//pr('officer remove');
                        CHFiling::getDb()->delete('ch_annual_return_officer', "form_submission_id = $formsubmisionId");
                    }

                    if ($this->chAnnualReturn->getShareholdings() != NULL) {
                        CHFiling::getDb()->delete('ch_annual_return_shareholding', "form_submission_id = $formsubmisionId");
                        foreach ($this->chAnnualReturn->getShareholdings() as $shareholding) {
                            CHFiling::getDb()->delete('ch_annual_return_shareholder', "annual_return_shareholding_id = " . $shareholding->getId());
                            // check transfer record
                            $transferExist = CHFiling::getDb()->fetchOne("SELECT annual_return_transfer_id FROM ch_annual_return_transfer WHERE annual_return_shareholding_id = " . $shareholding->getId());
                            if ($transferExist != NULL) {
                                CHFiling::getDb()->delete('ch_annual_return_transfer', "annual_return_shareholding_id  = " . $shareholding->getId());
                            }
                        }
                    }
                    if ($this->chAnnualReturn->getStatementOfCapital() != NULL) {
                        CHFiling::getDb()->delete('ch_annual_return_shares', "form_submission_id = $formsubmisionId");
                    }
                }
                $this->flashMessage('Confirmation Statement removed');
            }
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('companyDetails', "company_id={$this->company->getCompanyId()}");
        }
        $this->redirect('CompaniesAdminControler::COMPANIES_PAGE view', "company_id={$this->company->getCompanyId()}");
    }

    /**
     * Returns addreses for select and javascript used for prefill address
     * @param Company $company
     * @return array $return
     */
    public static function getPrefillAdresses(Company $company)
    {
        try {
            $addresses = $company->getAddresses();
        } catch (Exception $e) {
            $addresses = array();
        }

        $addressesFields = [];
        $selectValues = [];
        foreach ($addresses as $key => $address) {
            $fields = $address->getFields();
            $addressesFields[$key] = $fields;
            $selectValues[$key] = $fields['postcode'] . ', ' . $fields['premise'] . ' ' . $fields['street'];
        }

        return [
            'select' => $selectValues,
            'js' => json_encode($addressesFields, JSON_FORCE_OBJECT),
        ];
    }

    /**
     * @param Company $company
     * @param string $type
     * @return array
     */
    public static function getPrefillOfficers(Company $company, $type = 'person')
    {
        $officersFields = [];
        $selectValues = [];

        //todo: psc - preco vrati Share Shareholder, ked ti su v ch_incorporation_member?
        //todo: psc - add psc type?
        if ($type == 'person') {
            $persons = $company->getPersons(array('dir', 'sub', 'sec'));

            foreach ($persons as $key => $person) {
                $fields = $person->getFields();
                $officersFields[$key] = $fields;
                $selectValues[$key] = $fields['surname'] . ' ' . $fields['forename'];
            }
        } elseif ($type == 'corporate') {
            $persons = $company->getCorporates(array('dir', 'sub', 'sec'));

            foreach ($persons as $key => $person) {
                $fields = $person->getFields();
                $officersFields[$key] = $fields;
                $selectValues[$key] = $fields['corporate_name'];
            }
        }

        return [
            'select' => $selectValues,
            'js' => json_encode($officersFields, JSON_FORCE_OBJECT),
        ];
    }
}
