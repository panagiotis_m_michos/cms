<?php

/**
 * CMS
 * @category   Project
 * @package    CMS
 * @author     Nikolai Senkevich
 * @version    ARSAdminControler.php 2012-03-14 
 */
class ARSAdminControler extends PageAdminControler
{
    const CASHBANK_PAGE = 1286;

    
    
    /** @var array */
    protected $tabs = array(
        'list' => 'Confirmation Statement Service',
    );
    
    public function startup()
	{
		// add permission for staff
		$this->acl->allow('staff', 'page', array('view'));
		parent::startup();
	}
    
    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'PageAdmin';
    }

    
    public function renderList()
    {
        $datasource = dibi::select('*')->from(TBL_ANNUAL_RETURN_SERVICE)->orderBy('dtm', dibi::DESC);
        $grid = new ARSAdminDatagrid($datasource, 'companyId');
        $gridWithSearch = str_replace('value="Submit"', 'value="Search"', $grid->getHtml());
        $this->template->ars = $gridWithSearch;
    }

}
