<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    CustomersReviewsAdminControler.php 2010-07-22 divak@gmail.com
 */

class CustomersReviewsAdminControler extends PageAdminControler
{
	/** @var string */
	static public $handleObject = 'CustomersReviewsAdminModel';
	
	/**
	 * @var CustomersReviewsAdminModel
	 */
	public $node;
	
	
	/** @var array */
	protected $tabs = array(
		'list' => 'List',
	);
	
	/**
	 * @var CustomerReview
	 */
	private $customerReview;
	
    
    public function startup()
	{
		// add permission for staff
		$this->acl->allow('staff', 'page', array('view'));
		parent::startup();
	}
	
	/**
	 * Checks if get customerReviewId is set for required actions
	 * 
	 * @return void
	 */
	public function beforePrepare()
	{
		try {
			$actions = array('approve', 'unapprove', 'unallow', 'edit', 'delete');
			if (in_array($this->action, $actions)) {
				if (isset($this->get['customerReviewId'])) {
					$this->customerReview = new CustomerReview($this->get['customerReviewId']);
				} else {
					throw new Exception('CustomerReviewId is required');	
				}
			}
		} catch (Exception $e) {
			$this->flashMessage($e->getMessage(), 'error');
			$this->redirect('list');
		}
		parent::beforePrepare();
	}
	
	
	/************************************************* list *************************************************/
	
	
	public function prepareList()
	{
		$this->template->filterForm = $this->node->getComponentReviewsFilter();
		$this->template->paginator = $this->node->getReviewsPaginator($this->template->filterForm);
		$this->template->reviews = $this->node->getReviews($this->template->paginator, $this->template->filterForm);
		$this->template->batchForm = $this->node->getBatchActionsForm(array($this, 'Form_batchActionsFormSubmitted'), $this->template->reviews);
	}
	
	
	public function Form_batchActionsFormSubmitted(FForm $form)
	{
		try {
			$this->node->processBatchActionsForm($form);
			$this->flashMessage('Data has been updated');
			$this->redirect();
		} catch (Exception $e) {
		    $this->flashMessage($e->getMessage(), 'error');
		 	$this->redirect();
		}
	}
	
	
	/************************************************* edit *************************************************/
	
	
	public function prepareEdit()
	{
		$this->template->form = $this->node->getComponentEditForm(array($this, 'Form_editFormSubmitted'), $this->customerReview);
	}
	
	public function Form_editFormSubmitted(FForm $form)
	{
		try {
			$this->node->processEditForm($form, $this->customerReview);
			$this->flashMessage('Data has been saved');
			$this->reloadOpener();
		} catch (Exception $e) {
		    $this->flashMessage($e->getMessage(), 'error');
		 	$this->reloadOpener();
		}
	}
	
	
	/************************************************* approve *************************************************/
	
	
	public function handleApprove()
	{
		try {
			$this->customerReview->approve();
			$this->flashMessage('Customer review has been approved');
			$this->redirect('list');
		} catch (Exception $e) {
		    $this->flashMessage($e->getMessage(), 'error');
		 	$this->redirect('list');
		}
	}
	
	
	/************************************************* unapprove *************************************************/
	
	
	public function handleUnapprove()
	{
		try {
			$this->customerReview->unapprove();
			$this->flashMessage('Customer review has been unapproved');
			$this->redirect('list');
		} catch (Exception $e) {
		    $this->flashMessage($e->getMessage(), 'error');
		 	$this->redirect('list');
		}
	}
	
	
	/************************************************* unallow *************************************************/
	
	
	public function handleUnallow()
	{
		try {
			$this->customerReview->unallow();
			$this->flashMessage('Customer review has been unallowd');
			$this->redirect('list');
		} catch (Exception $e) {
		    $this->flashMessage($e->getMessage(), 'error');
		 	$this->redirect('list');
		}
	}
	
	/************************************************* delete *************************************************/
	
	public function handleDelete()
	{
		try {
			$this->customerReview->delete();
			$this->flashMessage('Customer review has been deleted');
			$this->redirect('list');
		} catch (Exception $e) {
		    $this->flashMessage($e->getMessage(), 'error');
		 	$this->redirect('list');
		}
	}
	
	
	/************************************************* import *************************************************/
	
	
	public function prepareImport()
	{
		$this->tabs['import'] = 'Import';
		$this->template->form = $this->node->getComponentImportForm(array($this, 'Form_importFormSubmitted'));
	}

	
	public function Form_importFormSubmitted(FForm $form)
	{
		try {
			$this->node->processImportForm($form);
			$this->flashMessage('Data has been imported');
			$this->redirect('list');	
		} catch (Exception $e) {
		    $this->flashMessage($e->getMessage(), 'error');
		 	$this->redirect();
		}
	}
}