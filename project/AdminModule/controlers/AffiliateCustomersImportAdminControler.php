<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    AffiliateCustomersImportAdminControler.php 2010-04-19 divak@gmail.com
 */

class AffiliateCustomersImportAdminControler extends PageAdminControler
{
    /** @var string */
    static public $handleObject = 'AffiliateCustomersImportModel';
    
    /**
     * @var AffiliateCustomersImportModel
     */
    public $node;
    
    /** @var array */
    protected $tabs = array(
        'import' => 'Import',
    );
    
    
    /************************************************* import *************************************************/
    
    
    public function prepareImport()
    {
        $this->template->form = $this->node->getComponentImportForm($this);
    }

    
    public function Form_affiliateCustomersImportFormSubmitted(FForm $form)
    {
        try {
            $data = $form->getValues();
            $this->node->processImportData($data);
            $form->clean();
            $this->flashMessage('Data has been imported');
            $this->redirect();    
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
             $this->redirect();
        }
    }
}