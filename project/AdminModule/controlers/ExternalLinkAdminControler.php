<?php


class ExternalLinkAdminControler extends BaseAdminControler
{
    /**
     * @var string
     */
    public static $handleObject = 'ExternalLinkNode';

    /**
     * @var array
     */
    protected $tabs = [
        'data' => 'Data'
    ];

    public function handleData()
    {
        $form = new FForm($this->nodeId . '_' . $this->action);

        $form->addFieldset('Name');
        $form->addText('title', 'Title: * ')
            ->setDescription('(Link text)')
            ->addRule(FForm::Required, 'Title is required!')
            ->size(30)
            ->setValue($this->node->page->title);
        $form->addText('url', 'URL: * ')
            ->setDescription('(URL this link will point to)')
            ->addRule(FForm::Required, 'Title is required!')
            ->size(30)
            ->setValue($this->node->getUrl());

        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Submit')
            ->onclick('changes = false')
            ->class('btn');

        $form->onValid = [$this, 'Form_nameFormSubmitted'];

        $form->start();
        $this->template->form = $form;
    }

    /**
     * @param FForm $form
     */
    public function Form_nameFormSubmitted(FForm $form)
    {
        $this->node->page->title = $form->getValue('title');
        $this->node->setUrl($form->getValue('url'));
        $this->node->page->save();
        $this->node->save();
        $this->flashMessage('Data has been updated');
        $this->redirect();
    }
}
