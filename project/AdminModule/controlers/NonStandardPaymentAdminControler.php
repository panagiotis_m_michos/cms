<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    NonStandardPaymentAdminControler.php 2009-12-10 divak@gmail.com
 */

class NonStandardPaymentAdminControler extends ProductAdminControler
{
    /** @var string */
    static public $handleObject = 'NonStandardPayment';
    
    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'ProductAdmin';
    }
}