<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    AdminFeeAdminControler.php 2009-12-04 divak@gmail.com
 */

class AdminFeeAdminControler extends ProductAdminControler
{
    /** @var string */
    static public $handleObject = 'AdminFee';
    
    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'ProductAdmin';
        
        // unset properties tab
        if (isset($this->tabs['properties'])) {
            unset($this->tabs['properties']);
        }
    }
}