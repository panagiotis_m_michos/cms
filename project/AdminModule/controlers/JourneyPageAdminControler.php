<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    JourneyProductAdminControler.php 2009-03-19 divak@gmail.com
 */

class JourneyPageAdminControler extends PageAdminControler
{

    /** @var string */
    static public $handleObject = 'JourneyPage';

    /**
     * @var JourneyPage
     */
    public $node;

    
    /** @var array */
    protected $tabs = array(
        'column' => 'Column',
        'details' => 'Details',
        'seo' => 'Seo'
    );

    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'PageAdmin';
    }


    protected function prepareColumn()
    {
        $form = new FForm($this->nodeId.'_'.$this->action);

        $categories = $this->node->getCategories();

        $form->addFieldset('Financial Services');
        $form->add('AsmSelect', 'column1', 'Column1')
            ->multiple()
            ->title('--- Select --')
            ->setValue($this->node->column1)
            ->setOptions($categories)
        //->addRule(array($this, 'Validator_Category'), 'category per column has to be different!')
        ;

        // associated 1
        $form->addFieldset('Financial Services');
        $form->add('AsmSelect', 'column2', 'Column2')
            ->multiple()
            ->title('--- Select --')
            ->setValue($this->node->column2)
            ->setOptions($categories)
        //->addRule(array($this, 'Validator_Category'), 'category per column has to be different!')
        ;

        // associated 2
        $form->addFieldset('Online & Technology');
        $form->add('AsmSelect', 'column3', 'Column3')
            ->multiple()
            ->title('--- Select --')
            ->setOptions($categories)
            ->setValue($this->node->column3)
        //->addRule(array($this, 'Validator_Category'), 'category per column has to be different!')
        ;

        // action
        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Submit')->class('btn');
        $form->onValid = array($this, 'Form_ColumnFormSubmitted');
        //$form->clean();
        $form->start();
        $this->template->form = $form;
    }


    public function Form_ColumnFormSubmitted(FForm $form)
    {
        $data = $form->getValues();

        $this->node->column1 = $data['column1'];
        $this->node->column2 = $data['column2'];
        $this->node->column3 = $data['column3'];
        $this->node->save();

        $form->clean();
        $this->flashMessage('Package has been updated');
        $this->redirect();
    }

    //	public function Validator_Category($control, $error, $params)
    //	{
    //		if ($control->getValue() && $control->owner['associatedProducts1']->getValue()) {
    //			$result = array_intersect($control->getValue(), $control->owner['associatedProducts1']->getValue());
    //			if (!empty($result)) {
    //				return $error;
    //			}
    //		}
    //		return TRUE;
    //	}
}
