<?php

use Admin\Cashback\ListDataGrid;
use Admin\Customer\EditForm;
use Admin\Customer\IEditFormDelegate;
use Admin\Customer\INoteFormDelegate;
use Admin\Customer\NoteForm;
use CompanyImportModule\BulkCsv\Importer;
use Dispatcher\Events\OrderEvent;
use DusanKasan\Knapsack\Collection;
use Entities\Customer as CustomerEntity;
use Entities\User as UserEntity;
use FilesystemModule\GaufretteDatabaseStorage\CustomerFileStorage;
use FilesystemModule\GaufretteDatabaseStorage\Entities\CustomerFileInfo;
use FilesystemModule\GaufretteDatabaseStorage\Exceptions\FileException;
use HttpModule\Responses\CsvResponse;
use HttpModule\Responses\FileResponse;
use IdCheckModule\Entities\IdCheck;
use IdCheckModule\Exceptions\IdCheckException;
use IdCheckModule\Forms\IdCheckDocumentForm;
use IdCheckModule\Services\IdCheckService;
use OrderModule\Config\DiLocator as OrderDiLocator;
use OrderModule\Processors\OrderItemsProcessor;
use Services\CashbackService;
use Services\CompanyService;
use Services\CustomerService as CustomerEntityService;
use Services\OrderItemService;
use Services\OrderService as OrderEntityService;
use Services\PackageService;
use Services\ProductService;
use Services\UserService;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class CustomersAdminControler extends CHAdminControler implements INoteFormDelegate, IEditFormDelegate
{
    const CUSTOMERS_PAGE = 234;

    /** @var array */
    protected $tabs = [
        'list' => 'List',
    ];

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var CustomerEntity
     */
    private $customerEntity;

    /**
     * @var UserEntity
     */
    private $userEntity;

    /**
     * @var OrderEntityService
     */
    private $orderService;

    /**
     * @var ProductService
     */
    private $productService;

    /**
     * @var OrderItemService
     */
    private $orderItemService;

    /**
     * @var IdCheckService
     */
    private $idCheckService;

    /**
     * @var CustomerFileStorage
     */
    private $customerFileStorage;

    /**
     * @var OrderItemsProcessor
     */
    private $orderItemsProcessor;

    /**
     * @var EventDispatcher
     */
    private $eventDispatcher;

    /**
     * @var Importer
     */
    private $companyBulkCreationImporter;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var PackageService
     */
    private $packageService;

    public function startup()
    {
        //@TODO remove once orders are paginated
        ini_set('memory_limit', '512M');

        // add permission for staff
        $this->acl->allow('staff', 'page', ['view']);
        $this->orderService = $this->getService(DiLocator::SERVICE_ORDER);
        $this->productService = $this->getService(DiLocator::SERVICE_PRODUCT);
        $this->orderItemService = $this->getService(DiLocator::SERVICE_ORDER_ITEM);
        $this->idCheckService = $this->getService(DiLocator::SERVICE_ID_CHECK);
        $this->customerFileStorage = $this->getService(DiLocator::SERVICE_CUSTOMER_FILE);
        $this->orderItemsProcessor = $this->getService(OrderDiLocator::ORDER_ITEMS_PROCESSOR);
        $this->eventDispatcher = $this->getService(DiLocator::DISPATCHER);
        $this->companyBulkCreationImporter = $this->getService('company_import_module.bulk_csv.importer');
        $this->companyService = $this->getService('services.company_service');
        $this->packageService = FApplication::$container->get('services.package_service');


        parent::startup();
    }

    public function beforePrepare()
    {
        try {
            // check required customerId for actions
            if ($this->action != 'list' && !isset($this->get['customer_id'])) {
                $this->redirect('list');
            }

            // @todo: move to base controller
            /** @var UserService $userService */
            $userService = $this->getService(DiLocator::SERVICE_USER);
            $this->userEntity = $userService->getUserById(FUser::getSignedIn()->id);

            // customer
            if (isset($this->get['customer_id'])) {

                $this->customer = new Customer($this->get['customer_id']);
                $this->template->customer = $this->customer;
                $this->template->customerId = $this->customer->getId();

                /** @var CustomerEntityService $customerService */
                $customerService = $this->getService(DiLocator::SERVICE_CUSTOMER);
                $this->customerEntity = $customerService->getCustomerById($this->customer->id);
                $this->template->customerEntity = $this->customerEntity;
            }
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect('list');
        }
        parent::beforePrepare();
    }

    /*     * ***************************** list ********************************** */

    public function prepareList()
    {
        $form = new FForm('customers', 'get');
        $form->addFieldset('Customer Search');
        $form->addText('firstName', 'First name: ');
        $form->addText('lastName', 'Last name: ');
        $form->addText('email', 'Email: ');
        $form->addSelect('roleId', 'Role: ', Customer::$roles)
            ->setFirstOption('--- Select ---');
        $form->addSubmit('customersSearch', 'Search')->class('btn');
        $form->start();
        $this->template->form = $form;

        // filter
        $where = [];
        if ($form->isOk2Save()) {
            $data = $form->getValues();
            if (!empty($data['firstName'])) {
                $where['firstName'] = $data['firstName'];
            }
            if (!empty($data['lastName'])) {
                $where['lastName'] = $data['lastName'];
            }
            if (!empty($data['email'])) {
                $where['email'] = $data['email'];
            }
            if (!empty($data['roleId'])) {
                $where['roleId'] = $data['roleId'];
            }
            $form->clean();
        }

        // paginator
        $count = Customer::getCustomersCount($where);
        $paginator = new FPaginator2($count);
        $paginator->itemsPerPage = 25;
        $paginator->htmlDisplayEnabled = FALSE;
        $this->template->paginator = $paginator;
        $this->template->customers = Customer::getCustomers(
            $where,
            $paginator->getLimit(),
            $paginator->getOffset(),
            TRUE
        );
    }

    /*     * ***************************** view ********************************** */

    public function prepareView()
    {
        try {
            Debug::disableProfiler();
            $this->tabs['view'] = [
                'title' => "{$this->customer->firstName} {$this->customer->lastName}",
                'params' => "customer_id={$this->customer->getId()}"
            ];

            // companies
            $datasource = dibi::select('company_id, company_name, company_number, incorporation_date')
                ->from('ch_company')
                ->where('customer_id = %i', $this->customer->customerId)
                ->orderBy('company_id')
                ->desc();

            $grid = new CustomerCompaniesAdminDatagrid($datasource, 'company_id');
            $this->template->companies = $grid->getHtml();

            // when we use
            $this->template->tab = '';
            if (isset($this->get['datagrid-1-order']) || isset($this->get['datagrid-1-page'])) {
                $this->template->tab = 'company';
            }

            // orders
            $this->template->orders = $this->customer->getCustomerOrders();

            // notes
            $noteService = $this->getService(DiLocator::SERVICE_CUSTOMER_NOTE);

            $noteForm = new NoteForm($this->customer->id . '_note');
            $noteForm->setAction(FApplication::$router->link() . '#notes');
            $noteForm->startup($this, $this->customerEntity, $this->userEntity, $noteService);
            $this->template->noteForm = $noteForm;

            // list of notes
            $this->template->notes = $noteService->getCustomerNotes($this->customerEntity);

            // cash backs
            $cashBackService = $this->getService(DiLocator::SERVICE_CASHBACK);
            /** @var CashbackService $cashBackService */
            $grid = new ListDataGrid($cashBackService->getCashbacksDataSource($this->customerEntity), 'cashbackId');
            $grid->setRouter($this->router);
            $grid->disableFilter();
            $grid->disableExport();
            $this->template->customerHasCashbacks = $grid->getRowsCount();
            $this->template->cashbacks = $grid->getHtml();
            try {
                $idCheck = $this->idCheckService->getLastIdCheck($this->customerEntity);
            } catch (IdCheckException $e) {
                $idCheck = IdCheck::fromCustomer($this->customerEntity);
            }
            $this->template->idCheck = $idCheck;
            $form = $this->createForm(new IdCheckDocumentForm());
            $this->template->documentForm = $form->createView();
            $this->template->documentErrors = $form->getErrorsAsString();
            if ($form->isValid()) {
                $this->customerFileStorage->addFile($this->customerEntity, CustomerFileInfo::TYPE_ID, $form['document']->getData());
            }
            $this->template->idCheckFiles = iterator_to_array($this->customerFileStorage->listFiles($this->customerEntity, CustomerFileInfo::TYPE_ID));

        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('list');
        }
    }


    public function handleDownloadDocument()
    {
        try {
            $file = $this->customerFileStorage->getFile($this->customerEntity, $this->request->get('fileKey'));
            $response = new FileResponse($file->getContent(), $file->getName());
            $response->send();
            exit(0);
        } catch (FileException $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect('view', "customer_id={$this->customerEntity->getId()}");
        }
    }

    public function handleRemoveDocument()
    {
        try {
            $this->customerFileStorage->removeFile($this->customerEntity, $this->request->get('fileKey'));
            $this->flashMessage('File removed successfully');
            $this->redirect('view', "customer_id={$this->customerEntity->getId()}");
        } catch (FileException $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect('view', "customer_id={$this->customerEntity->getId()}");
        }

    }

    public function handleCompleteIdCheck()
    {
        $idCheck = IdCheck::fromCustomer($this->customerEntity);
        $this->idCheckService->completeWithDocument($idCheck);
        $this->flashMessage('Id check completed');
        $this->redirect('view', "customer_id={$this->customerEntity->getId()}");
    }

    /*     * ***************************** edit details ********************************** */

    public function prepareEditDetails()
    {
        try {
            $this->tabs['view'] = ['title' => 'Customer', 'params' => "customer_id={$this->customer->getId()}"];
            $this->tabs['editDetails'] = ['title' => 'Edit', 'params' => "customer_id={$this->customer->getId()}"];
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('default');
        }
    }

    public function renderEditDetails()
    {
        $noteService = $this->getService(DiLocator::SERVICE_CUSTOMER_NOTE);
        $customerService = $this->getService(DiLocator::SERVICE_CUSTOMER);

        $form = new EditForm($this->customer->id . '_edit');
        $form->startup($this, $this->customerEntity, $this->userEntity, $noteService, $customerService);
        $this->template->form = $form;
    }

    /*     * ***************************** add credit ********************************** */

    public function prepareAddCredit()
    {
        try {
            $this->tabs['view'] = ['title' => 'Customer', 'params' => "customer_id={$this->customer->getId()}"];
            $this->tabs['addCredit'] = [
                'title' => 'Add Credit',
                'params' => "customer_id={$this->customer->getId()}"
            ];
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('default');
        }
    }

    public function renderAddCredit()
    {
        $form = new FForm($this->nodeId . '_' . $this->action . '_' . $this->customer->getId());

        // data
        $form->addFieldset('Data');
        $form->addText('credit', 'Credit:')
            ->addRule(FForm::Required, 'Please provide credit!')
            ->addRule(FForm::NUMERIC, 'Please enter a numeric value!');
        $form->addArea('additional', 'Additional info: ')->cols(30)->rows(5);
        $form->addSelect('type', 'Type: ', Transaction::$types)
            ->setFirstOption('--- Select ---')
            ->addRule(FForm::Required, 'Please provide type!')
            ->addRule(
                function (Select $control, $error) {
                    return array_key_exists($control->getValue(), Transaction::$types) ?: $error;
                },
                'Please provide correct type!'
            );

        // action
        $form->addFieldset('Action');
        $form->addSubmit('add', 'Save Credit')->class('btn');

        $form->onValid = [$this, 'Form_addCreditFormSubmitted'];
        $form->start();
        $this->template->form = $form;
    }

    public function Form_addCreditFormSubmitted(FForm $form)
    {
        $data = $form->getValues();

        // order
        $order = new Order;
        $order->statusId = Order::STATUS_NEW;
        $order->customerId = $this->customer->getId();
        $order->realSubtotal = $data['credit'];
        $order->subtotal = $data['credit'];
        $order->vat = 0;
        $order->total = $order->subtotal + $order->vat;
        $order->customerName = $this->customer->firstName . ' ' . $this->customer->lastName;

        // item
        $item = new OrderItem;
        $item->productId = 0;
        $item->productTitle = 'Custom Credit';
        $item->qty = 1;
        $item->price = $data['credit'];
        $item->subTotal = $data['credit'];
        $item->vat = 0;
        $item->totalPrice = $data['credit'];

        if (!empty($data['additional'])) {
            $item->additional = $data['additional'] . "\n";
            $item->additional .= "=====\n";
            $item->additional .= 'by ' . FUser::getSignedIn()->login;
        } else {
            $item->additional = 'by ' . FUser::getSignedIn()->login;
        }

        $order->items[] = $item;
        $orderId = $order->save();

        // transaction
        $transaction = new Transaction();
        $transaction->customerId = $this->customer->getId();
        $transaction->typeId = $data['type'];
        $transaction->orderId = $orderId;
        $transaction->save();

        // save credit to customer
        $this->customer->credit += $data['credit'];
        $this->customer->save();

        $form->clean();
        $this->flashMessage('Credit has been added');
        $this->redirect('view', "customer_id={$this->customer->getId()}");
    }

    /*     * ***************************** add company ********************************** */

    public function prepareAddCompany()
    {
        $this->tabs['view'] = ['title' => 'Customer', 'params' => "customer_id={$this->customer->getId()}"];
        $this->tabs['addCompany'] = [
            'title' => 'Add Company',
            'params' => "customer_id={$this->customer->getId()}"
        ];
    }

    public function renderAddCompany()
    {
        $form = new FForm($this->nodeId . '_' . $this->action . '_' . $this->customer->getId());

        // data
        $form->addFieldset('Data');
        $form->addText('companyName', 'Company name:')
            ->size(40)
            ->addRule(FForm::Required, 'Please provide Company name!');
        $form->addArea('additional', 'Additional info: ')->cols(30)->rows(5);
        $form->addSelect('productId', 'Package: ', $this->packageService->getPublishedPackagesPairs())
            ->setFirstOption('--- Select ---')
            ->addRule(FForm::Required, 'Please provide package!');
        $form->addSelect('typeId', 'Type: ', CompanyIncorporation::$types)
            ->setFirstOption('--- Select ---')
            ->setDisabledOptions(['BYGUAREXEMPT'])
            ->addRule(FForm::Required, 'Please provide type!');

        // action
        $form->addFieldset('Action');
        $form->addSubmit('add', 'Add Company')->class('btn');

        $form->onValid = [$this, 'Form_addCompanyFormSubmitted'];
        $form->start();
        $this->template->form = $form;
    }

    public function Validator_companyName($control, $error)
    {
        if (preg_match('/^([ \t\r\n\v\f\x80\x26\x40\xa3\x24\xa5\x2a\x3d\x23\x25\x2b\x91\x92\x27\x28\x29\x5b\x5d\x7b\x7d\x3c\x3e\x21\xab\xbb\x3f\x93\x94\x22\x5c\x2f‘’“”«»£€¥1-9A-Za-z]+)$/', $control->getValue())) {
            return TRUE;
        }
        return $error;
    }

    public function Form_addCompanyFormSubmitted(FForm $form)
    {
        $data = $form->getValues();
        $chFiling = new CHFiling();

        $company = $chFiling->setCompany($this->customer->getId(), $data['companyName']);
        $company->getLastFormSubmission()->getForm()->setType($data['typeId']);

        // included products
        $product = FNode::getProductById($data['productId']);

        // lock company
        if ($product->lockCompany) {
            $company->lockCompany();
        }

        // don't print certificates
        if ($product->getId() == Package::PACKAGE_BRONZE) {
            $company->setCertificatePrinted(TRUE);
        } elseif ($product->getId() != Package::PACKAGE_BRONZE && !$this->customer->isWholesale()) {

            $company->setBronzeCoverLetterPrinted(TRUE);
        }

        $includedProducts = $product->getPackageProducts('includedProducts');
        $this->orderItemsProcessor->saveItemsToCompany($this->customer, $includedProducts, $company);

        // order
        $order = new Order;
        $order->statusId = Order::STATUS_NEW;
        $order->customerId = $this->customer->getId();
        $order->realSubtotal = 0;
        $order->subtotal = 0;
        $order->vat = 0;
        $order->total = 0;
        $order->customerName = $this->customer->firstName . ' ' . $this->customer->lastName;

        // package
        $package = new Package($data['productId']);
        $package->companyName = $data['companyName'];
        $package->setCompanyId($company->getId());

        // item
        $item = new OrderItem;
        $item->productId = $package->getId();
        $item->productTitle = 'Custom ' . $package->getLongTitle();
        $item->qty = 1;
        $item->price = 0;
        $item->subTotal = 0;
        $item->vat = 0;
        $item->totalPrice = 0;
        $item->incorporationRequired = TRUE;
        $item->companyId = $company->getId();

        if (!empty($data['additional'])) {
            $item->additional = $data['additional'] . "\n";
            $item->additional .= "=====\n";
            $item->additional .= 'by ' . FUser::getSignedIn()->login;
        } else {
            $item->additional = 'by ' . FUser::getSignedIn()->login;
        }

        $order->items[] = $item;
        $orderId = $order->save();

        $package->setOrderItem($this->orderItemService->getOrderItemById($item->getId()));

        $company->setOrderId($orderId);
        $company->setProductId($package->getId());
        $company->setCashBackAmount($package->getCashBackAmount());

        // transaction
        $transaction = new Transaction();
        $transaction->customerId = $this->customer->getId();
        $transaction->typeId = Transaction::ON_ACCOUNT;
        $transaction->orderId = $orderId;
        $transaction->save();

        // save services
        $this->productService->saveServices([$package]);

        $orderEntity = $this->orderService->getOrderById($orderId);
        $this->eventDispatcher->dispatch(EventLocator::ORDER_COMPLETED, new OrderEvent($orderEntity));

        $form->clean();
        $this->flashMessage('Company has been added');
        $this->redirect('view', "customer_id={$this->customer->getId()}");
    }

    /*     * ***************************** company formation ********************************** */

    public function prepareCompanyFormation()
    {
        try {
            $this->tabs['view'] = ['title' => 'Customer', 'params' => ["customer_id" => $this->customer->getId()]];
            $this->tabs['companyFormation'] = [
                'title' => 'Company Formation',
                'params' => "customer_id={$this->customer->getId()}"
            ];
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect('view');
        }
    }

    protected function renderCompanyFormation()
    {
        $bulkFormationForm = new FForm($this->nodeId . '_' . $this->action . '_' . $this->customer->getId() . '_bulk_creation');

        $bulkFormationForm->addFieldset('Data');
        $bulkFormationForm->addFile('data', 'CSV File: *')
            ->addRule(FForm::Required, 'Required!')
            ->setDescription(
                'You can download the CSV example below.'
            );

        $bulkFormationForm
            ->addSelect('package', 'Create with:', $this->packageService->getPublishedPackagesPairs())
            ->setFirstOption('--- Select ---');

        $bulkFormationForm->addFieldset('Action');
        $bulkFormationForm->addSubmit('import', 'Create companies')->class('btn');
        $bulkFormationForm->onValid = [$this, 'Form_bulkFormationSubmitted'];
        $bulkFormationForm->start();

        $this->template->bulkFormationForm = $bulkFormationForm;

        $incompleteFormSubmissionsCount = count($this->companyService->getUnsubmittedCompanies($this->customerEntity));
        $bulkSubmitForm = new FForm($this->nodeId . '_' . $this->action . '_' . $this->customer->getId() . '_bulk_submit');
        $bulkSubmitForm->addFieldset('Action');
        $bulkSubmitForm->addSubmit('submit', 'Submit all unsubmitted companies')
            ->setDescription(
                "This will submit all $incompleteFormSubmissionsCount unsubmitted companies of this customer."
            )
            ->class('btn');
        $bulkSubmitForm->onValid = [$this, 'Form_bulkSubmitSubmitted'];
        $bulkSubmitForm->start();

        if ($incompleteFormSubmissionsCount) {
            $this->template->bulkSubmitForm = $bulkSubmitForm;
        }
    }

    /**
     * @param FForm $form
     */
    public function Form_bulkSubmitSubmitted(FForm $form)
    {
        try {
            $companies = $this->companyService->getUnsubmittedCompanies($this->customerEntity);
            foreach ($companies as $company) {
                $oldCompany = Company::getCompany($company->getId());
                $oldCompany->getLastFormSubmission()->sendRequestImproved();
            }

            $this->flashMessage('Companies have been submitted');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }

        $form->clean();
        $this->redirect();
    }

    /**
     * @param FForm $form
     */
    public function Form_bulkFormationSubmitted(FForm $form)
    {
        try {
            $data = $form->getValues();

            $this->companyBulkCreationImporter->import(
                $this->customerEntity,
                $data['package'],
                new SplFileObject($data['data']['tmp_name'])
            );

            $this->flashMessage('Companies have been imported');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }

        $form->clean();
        $this->redirect();
    }

    /*     * ***************************** company import ********************************** */

    public function prepareImportCompany()
    {
        try {
            $this->tabs['view'] = ['title' => 'Customer', 'params' => "customer_id={$this->customer->getId()}"];
            $this->tabs['importCompany'] = [
                'title' => 'Company Import',
                'params' => "customer_id={$this->customer->getId()}"
            ];
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect('view');
        }
    }

    protected function renderImportCompany()
    {
        // --- single company ---
        $form = new FForm($this->nodeId . '_' . $this->action . '_' . $this->customer->getId());

        $form->addFieldset('Data');
        $form->addText('companyNumber', 'Company No.*')
            ->addRule(FForm::Required, 'Please provide Company number.');
        $form->addText('auth', 'Authentication code*')
            ->addRule(FForm::Required, 'Please provide Authentication code.');

        $form->addFieldset('Action');
        $form->addSubmit('import', 'Import Company')->class('btn');
        $form->onValid = [$this, 'Form_importCompanySubmitted'];
        $form->start();

        $this->template->form = $form;

        // --- more companies ---
        $form2 = new FForm($this->nodeId . '_' . $this->action . '_' . $this->customer->getId() . '_more');

        $form2->addFieldset('Data');
        $form2->addFile('companies', 'CSV File: *')
            ->addRule(FForm::Required, 'Required!')
            ->setDescription(
                '(File has to be in CSV format with 2 columns: "Company Number" and "Authentication Code")'
            );

        $form2->addFieldset('Action');
        $form2->addSubmit('import', 'Import Companies')->class('btn');
        $form2->onValid = [$this, 'Form_importCompaniesSubmitted'];
        $form2->start();

        $this->template->form2 = $form2;
    }

    public function Form_importCompanySubmitted(FForm $form)
    {
        $data = $form->getValues();
        $chFiling = new CHFiling();
        try {
            $chFiling->importCompany($data['companyNumber'], $data['auth'], $this->customer->getId());
            $this->flashMessage('Company has been imported.');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
        }

        $form->clean();
        $this->redirect('view', "customer_id={$this->customer->getId()}");
    }

    public function Form_importCompaniesSubmitted(FForm $form)
    {
        try {
            $errors = [];
            $chFiling = new CHFiling();
            $data = $form->getValues();
            $handle = fopen($data['companies']['tmp_name'], 'r');
            while (($row = fgetcsv($handle, 1000, ",")) !== FALSE) {
                if (count($row) != 2) {
                    throw new Exception('Wrong CSV file.');
                }
                try {
                    $chFiling->importCompany($row[0], $row[1], $this->customer->getId());
                } catch (Exception $e) {
                    $errors[] = sprintf("<b>%s</b>: %s <br />", $row[0], $e->getMessage());
                }
            }
            $form->clean();

            if (!empty($errors)) {
                $message = sprintf(
                    "<b>There was an error importing some companies (listed below).</b><br /> <br /> %s",
                    implode('<br /> ', $errors)
                );
                $this->flashMessage($message, 'error');
            } else {
                $this->flashMessage('Companies have been imported');
            }

            $this->redirect();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    /*     * ***************************** export csv ********************************** */

    public function handleExportCsv()
    {

        Debug::disableProfiler();

        $data = dibi::select('company_id as `COMPANY ID`, company_name as `COMPANY NAME`, company_number as `CO NUMBER`, incorporation_date as `INCORP DATE`, accounts_next_due_date as `ACCOUNTS`, returns_next_due_date as `AR DUE`')
            ->from("ch_company")
            ->where('customer_id = %i', $this->customer->getId())
            ->execute()
            ->fetchAll();

        if (!empty($data)) {
            $header = array_keys((array) current($data));
            array_unshift($data, $header);
        }

        Array2Csv::output($data, 'customer_' . $this->customer->getId() . '.csv');
    }

    /*     * ***************************** sync companies ********************************** */

    public function handleSyncAll()
    {

        /* only companies with company number */
        $ids = dibi::select('company_id')
            ->from("ch_company")
            ->where('customer_id = %i', $this->customer->getId())
            ->and('company_number IS NOT NULL')
            ->and(' `deleted` = 0')
            ->execute()
            ->fetchAll();

        /* sync all companies */
        $errors = [];
        foreach ($ids as $id) {
            $company = Company::getCompany($id['company_id']);
            try {
                $company->syncAllData();
            } catch (Exception $e) {
                $errors[] = "<b>" . $id['company_id'] . "</b> - " . $e->getMessage();
            }
        }
        if (empty($errors)) {
            $this->flashMessage('Companies synced');
        } else {
            $this->flashMessage(implode("<br />", $errors), 'info', FALSE);
        }
        $this->redirect('view', "customer_id={$this->customer->getId()}");
    }

    public function handlesyncMCAll()
    {
        $service = new CompanyMonitoringService();
        try {
            $service->syncCustomerCompanies($this->customer);
            $this->flashMessage('Companies has been sync.');
            $this->redirect('view', "customer_id={$this->customer->getId()}");
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('view', "customer_id={$this->customer->getId()}");
        }
    }

    /*     * **************************** INoteFormDelegate ********************************** */

    public function customerNoteSucceeded()
    {
        $this->flashMessage('Customer note has been saved');
        $this->redirect();
    }

    public function customerNoteFailed(Exception $e)
    {
        $this->flashMessage($e->getMessage(), 'error');
        $this->redirect();
    }


    /*     * **************************** IEditFormDelegate ********************************** */

    public function customerEditSucceeded()
    {
        $this->flashMessage('Customer has been updated.');
        $this->redirect('view', "customer_id={$this->customer->getId()}");
    }

    public function customerEditFailed(Exception $e)
    {
        $this->flashMessage($e->getMessage(), 'error');
        $this->redirect();
    }

}
