<?php
class CertificateUpgradeAdminControler extends ProductAdminControler
{

    /** @var string */
    static public $handleObject = 'CertificateUpgrade';

    public function beforePrepare() 
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'ProductAdmin';
    }

}