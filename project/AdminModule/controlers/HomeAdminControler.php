<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    HomeAdminControler.php 2009-03-19 divak@gmail.com
 */

class HomeAdminControler extends PageAdminControler
{
    /** @var array */
    protected $tabs = array(
        'page' => 'Page',
        'properties' => 'Properties',
        'menu' => 'Menu',
        'details' => 'Details',
        'seo' => 'Seo'
    );

    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'PageAdmin';
    }
    
    
    public function handleProperties()
    {
        
        $pageForm = new FForm($this->nodeId.'_'.$this->action);
        
        // fieldset
        $pageForm->addFieldset('Page in ' . $this->node->page->languagePk . ' Language');
        $pageForm->add('CmsFckArea', 'testimonials', 'Testimonials:')
            ->setDescription('(Main text of page)')
            ->setSize(500, 300)
            ->setValue(FProperty::getNodePropsValue('testimonials'));
            
        // action
        $pageForm->addFieldset('Action');
        $pageForm->addSubmit('submit', 'Submit')
            ->onclick('changes = false')
            ->class('btn');
        $pageForm->onValid = array($this, 'Form_propertiesFormSubmitted');
        
        $pageForm->start();

        $this->template->form = $pageForm;
    }
    
    
    public function Form_propertiesFormSubmitted(FForm $form)
    {
        FProperty::get('testimonials')->setValue($form->getValue('testimonials'))->save();
        $this->flashMessage('Data has been updated');
        $this->redirect(NULL);
    }
    
}
