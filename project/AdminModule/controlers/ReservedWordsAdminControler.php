<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    ReservedWordsAdminControler.php 2009-08-17 divak@gmail.com
 */

class ReservedWordsAdminControler extends NameAdminControler
{
    /** @var array */
    protected $tabs = array(
        'list' => 'Words',
    );
    
    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'NameAdmin';
    }

    
    
    /**************************************** list ***************************************/
    
    
    
    public function prepareList()
    {
        $this->template->words = ReservedWord::getAll();
    }
    
    
    
    /**************************************** create word ***************************************/
    
    
    
    public function prepareCreate()
    {
        $this->tabs['create'] = 'Create';
        $form = new FForm('createWord');
        
        $form->addFieldset('Reserve Word');
        $form->addRadio('typeId', 'Type: *', ReservedWord::$types)
            ->addRule(FForm::Required, 'Please provide type!')
            ->setValue(ReservedWord::TYPE_NORMAL);
        $form->addText('word', 'Word: *')->size(30)
            ->addRule(FForm::Required, 'Please provide word!');
        $form->add('CmsFckArea', 'description', 'Description:')
            ->setToolbar('Basic')
            ->setSize(400, 200);
        $form->add('CmsFile', 'fileId', 'Template:')
            ->addRule(array($this, 'Validator_requiredTemplate'), 'Please provide template!')
            ->setNodeId(613)
            ->setDescription('(Not supported yet)');
        
        $form->addFieldset('Action');
        $form->addSubmit('save', 'Save')->class('btn');
        
        $form->onValid = array($this, 'Form_createFormSubmitted');
        $form->start();
        $this->template->form = $form;
        $this->template->title = 'Add reserved word';
    }
    
    
    public function Form_createFormSubmitted(FForm $form)
    {
        $data = $form->getValues();
        
        $word = new ReservedWord;
        $word->typeId = $data['typeId'];
        $word->word = $data['word']; 
        $word->description = $data['description'];
        
        if ($data['typeId'] == ReservedWord::TYPE_TEMPLATE) {
            $word->fileId = $data['fileId'];
        }
        
        $word->save();
        $form->clean();
        
        $this->flashMessage("Reserved word has been added");
        $this->redirect('list');
    }
    
    
    /**************************************** edit word ***************************************/
    
    
    
    /** @var object ReservedWord */
    private $word;
    
    public function prepareEdit()
    {
        if (!isset($this->get['word_id'])) {
            $this->redirect('list');
        }
        
        $this->word = new ReservedWord($this->get['word_id']);
        if ($this->word->exists() == FALSE) {
            $this->redirect('list');
        }
        
        $this->tabs['edit'] = 'Edit';
    }
    
    public function renderEdit()
    {
        $form = new FForm('editWord');
        
        $form->addFieldset('Reserve Word');
        $form->addRadio('typeId', 'Type: *', ReservedWord::$types)
            ->addRule(FForm::Required, 'Please provide type!')
            ->setValue($this->word->typeId);
        $form->addText('word', 'Word: *')->size(30)
            ->addRule(FForm::Required, 'Please provide word!')
            ->setValue($this->word->word);
        $form->add('CmsFckArea', 'description', 'Description:')
            ->setToolbar('Basic')
            ->setSize(400, 200)
            ->setValue($this->word->description);
        $form->add('CmsFile', 'fileId', 'Template:')
            ->setValue($this->word->fileId)
            ->addRule(array($this, 'Validator_requiredTemplate'), 'Please provide template!')
            ->setNodeId(613);
        
        $form->addFieldset('Action');
        $form->addSubmit('save', 'Save')->class('btn');
        
        $form->onValid = array($this, 'Form_editFormSubmitted');
        //$form->clean();
        $form->start();
        $this->template->form = $form;
        $this->template->title = 'Add reserved word';
    }
    
    
    public function Form_editFormSubmitted(FForm $form)
    {
        $data = $form->getValues();
        
        $this->word->typeId = $data['typeId'];
        $this->word->word = $data['word']; 
        $this->word->description = $data['description'];
        
        if ($data['typeId'] == ReservedWord::TYPE_TEMPLATE) {
            $this->word->fileId = $data['fileId'];
        } else {
            $this->word->fileId = NULL;
        }
        
        $this->word->save();
        $form->clean();
        
        $this->flashMessage("Reserved word has been updated");
        $this->redirect(NULL);
    }
    

    
    /**************************************** delete word ***************************************/
    
    
    
    public function prepareDelete()
    {
        if (!isset($this->get['word_id'])) {
            $this->redirect('list');
        }
        
        $word = new ReservedWord($this->get['word_id']);
        if ($word->exists() == FALSE) {
            $this->redirect('list');
        }
        $word->delete();
        
        $this->flashMessage("Reserved word has been deleted");
        $this->redirect('list');
    }
    
    
    /**************************************** validators ***************************************/
    
    
    public function Validator_requiredTemplate($control, $error, $params)
    {
        $value = $control->getValue();
        if ($control->owner['typeId']->getValue() == 2 && empty($value)) {
            return $error;
        }
        return TRUE;
    }
    
}
