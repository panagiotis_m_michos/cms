<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    MatrixServicesAdminControler.php 2012-03-13 razvanp@madesimplegroup.com
 */

class MatrixServicesAdminControler extends PageAdminControler
{
    
    const MATRIX_PAKAGE_SERVICE_FOLDER = 1185;
    const MATRIX_PAKAGE_OFFERS_FOLDER = 1188;

    /**
	 * @var string
	 */
	static public $handleObject = 'MatrixServicesModel';
	
	/**
	 * @var MatrixModel
	 */
	public $node;

    /**
	 * @var array
	 */
	protected $tabs = array(
		//'list' => 'Deleted Companies',
       'page' => 'Page', 
       'properties'=>'Properties',
       // 'layout' => 'Layout', 
       //'details' => 'Details',
       //'seo' => 'Seo'
        
       
	);

    /** @var array */
    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'PageAdmin';
    }
    
    /************************************* Products *************************************/

    public function  renderProperties(){
        $form = new MatrixPakageServicesAdminForm($this->node->getId() . '_MatrixServicesAdmin');
        $form->startup($this, array($this, 'Form__MatrixPakageServicesAdminFormubmitted'),$this->node);
        $this->template->form = $form;
    }
    /**
     * @param MatrixPakageServicesAdminForm $form
     */
    public function Form__MatrixPakageServicesAdminFormubmitted(MatrixPakageServicesAdminForm $form)
    {
        try {
            $form->process();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }
}
