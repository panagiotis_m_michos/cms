<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    PackagePageAdminControler.php 2009-03-19 divak@gmail.com
 */

class PackagePageAdminControler extends PageAdminControler
{
    /** @var array */
    protected $tabs = array(
        'page' => 'Page',
        'packages' => 'Packages',
        'details' => 'Details',
        'seo' => 'Seo'
    );
    
    
    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'PageAdmin';
    }
    
    
    /********************************* properties *********************************/
    
    
    public function handlePackages()
    {
        // packages
        $packages = $this->node->getProperty('packages');
        if ($packages != NULL) {
            $packages = explode(',', $packages);
        }
        
        $form = new FForm($this->nodeId.'_'.$this->action);
        
        // packages
        $form->addFieldset('Packages');
        $form->addCheckbox('packages', 'Packages: ', BasketProduct::getAllPackages())
            ->setValue($packages);
            
        // action
        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Submit')->class('btn');
        $form->onValid = array($this, 'Form_productsFormSubmitted');
        $form->clean();
        $form->start();
        $this->template->form = $form;    
    }
    
    
    public function Form_productsFormSubmitted(FForm $form)
    {
        $data = $form->getValues();
        
        // packages
        if (!empty($data['packages'])) {
            $packages = implode(',', $data['packages']);
        } else {
            $packages = NULL;
        }
        $this->node->saveProperty('packages', $packages);
        
        
        $form->clean();
        $this->flashMessage('Data has been updated');
        $this->redirect(NULL);
    }
}
