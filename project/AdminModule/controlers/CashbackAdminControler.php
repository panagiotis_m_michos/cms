<?php

use Admin\Barclays\ImportForm;
use Admin\Cashback\AddCashbackForm;
use Admin\Cashback\EligibleDataGrid;
use Admin\Cashback\IAddCashbackDelegate;
use Admin\Cashback\ListDataGrid;
use Admin\Cashback\PaymentForm;
use BankingModule\Config\DiLocator as BankingDiLocator;
use BankingModule\Importers\BarclaysAccountsImporter;
use BankingModule\Importers\TsbAccountsImporter;
use CashBackModule\Config\DiLocator as CashBackDiLocator;
use CsvParserModule\Exceptions\ImportException;
use Dispatcher\Events\CashbackArchiveEvent;
use Doctrine\ORM\EntityManager;
use Entities\Cashback;
use Entities\Customer;
use Helpers\Admin\Template\CashbackHelper;
use Psr\Log\LoggerInterface;
use Services\CashbackService;
use Services\CompanyService;
use Services\CustomerService;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Utils\File;

class CashbackAdminControler extends PageAdminControler implements IAddCashbackDelegate
{
    const PAGE_CASHBACK = 1217;

    /**
     * @var array
     */
    protected $tabs = [
        'eligibleCashbacks' => 'Eligible Cashbacks',
        'cashbacks' => 'All Cashbacks',
        'import' => 'Import CSV',
        'addCashback' => 'Manually Add Barclays Record',
    ];

    /**
     * @var CashbackEmailer
     */
    protected $cashbackEmailer;

    /**
     * @var CashbackService
     */
    protected $cashbackService;

    /**
     * @var CustomerService
     */
    protected $customerService;

    /**
     * @var Customer
     */
    protected $customer;

    /**
     * @var Cashback[] array
     */
    protected $cashbacks;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var CompanyService
     */
    protected $companyService;

    /**
     * @var CashbackHelper
     */
    protected $cashbackHelper;

    /**
     * @var EventDispatcher
     */
    private $eventDispatcher;

    public function startup()
    {
        $this->acl->allow('staff', 'page', ['view']);
        $this->cashbackEmailer = $this->getService(DiLocator::EMAILER_CASHBACK);
        $this->cashbackService = $this->getService(DiLocator::SERVICE_CASHBACK);
        $this->customerService = $this->getService(DiLocator::SERVICE_CUSTOMER);
        $this->em = $this->getService(DiLocator::ENTITY_MANAGER);
        $this->companyService = $this->getService(DiLocator::SERVICE_COMPANY);
        $this->eventDispatcher = $this->getService(DiLocator::DISPATCHER);

        parent::startup();
    }

    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'PageAdmin';
    }

    public function beforeRender()
    {
        parent::beforeRender();
        if (in_array($this->action, ['payment', 'cashbacksDetails'])) {
            $this->cashbackHelper = new CashbackHelper($this->customer, $this->cashbacks);
        }
    }

    /****************************************** eligibleCashbacks ******************************************/


    public function renderEligibleCashbacks()
    {
        $grid = new EligibleDataGrid($this->cashbackService->getEligibleCashbacksDataSource(), 'cashbackId');
        $grid->setRouter($this->router);
        $this->template->cashbacks = $grid->getHtml();
    }

    /****************************************** payment ******************************************/


    public function preparePayment()
    {
        try {
            $this->customer = $this->customerService->getCustomerById(
                $this->getParameter('customerId'),
                TRUE
            );
            $this->cashbacks = $this->cashbackService->getCustomerEligibleCashbacks(
                $this->customer,
                $this->getParameter('packageTypeId')
            );

            if (empty($this->cashbacks)) {
                throw new LogicException("Customer `{$this->customer->getId()}` doesn't have any eligible cashbacks");
            }

            $this->tabs = [
                'eligibleCashbacks' => 'Eligible Cashbacks',
                'payment' => 'Payment',
            ];

        } catch (Exception $e) {
            $this->handleException($e->getMessage(), 'eligibleCashbacks');
        }
    }

    public function renderPayment()
    {
        $cashBackForm = new PaymentForm('cashback_payment_' . $this->customer->getId());
        $cashBackForm->startup([$this, 'processPaymentForm'], $this->cashbacks);

        $this->template->form = $cashBackForm;
        $this->template->helper = $this->cashbackHelper;
    }

    /**
     * @param PaymentForm $form
     */
    public function processPaymentForm(PaymentForm $form)
    {
        try {
            if ($form->isSubmitedBy('submit')) {
                $this->cashbackService->markCashbacksAsPaid($this->cashbacks);
                $this->cashbackEmailer->sendPaidCashbacksEmail($this->cashbackHelper);
                $this->flashMessage('Cashback confirmation email sent to customer.');
            } elseif ($form->isSubmitedBy('archive')) {
                $this->archiveCashbacks($this->cashbacks);
                $this->flashMessage('Cashback has been archived and an e-mail has been sent to the customer asking them to update their bank details.');
            } elseif ($form->isSubmitedBy('restore')) {
                $this->cashbackService->markCashbacksAsActive($this->cashbacks);
                $this->flashMessage('Cashback has been restored.');
            }

            $this->redirect('eligibleCashbacks');
        } catch (Exception $e) {
            $this->handleException($e->getMessage());
        }
    }

    /**
     * @param Cashback[] $cashbacks
     */
    private function archiveCashbacks(array $cashbacks)
    {
        $customer = $cashbacks[0]->getCustomer();

        $this->cashbackService->markCashbacksAsArchived($cashbacks);
        $this->eventDispatcher->dispatch(EventLocator::CASHBACK_ARCHIVED, new CashbackArchiveEvent($cashbacks, $customer));
    }

    /****************************************** cashbacks ******************************************/


    public function renderCashbacks()
    {
        $grid = new ListDataGrid($this->cashbackService->getCashbacksDataSource(), 'cashbackId');
        $grid->setRouter($this->router);
        $this->template->cashbacks = $grid->getHtml();
    }


    /****************************************** cashbacksDetails ******************************************/


    public function handleCashbacksDetails()
    {
        try {

            if (!isset($this->get['customerId'], $this->get['packageTypeId'], $this->get['statusId'])) {
                throw new InvalidArgumentException("Please provide all required parameters (customerId, packageTypeId, statusId)");
            }

            $this->customer = $this->customerService->getCustomerById(
                $this->getParameter('customerId'),
                TRUE
            );

            $this->cashbacks = $this->cashbackService->getGroupedCashbacks(
                $this->customer,
                $this->getParameter('packageTypeId'),
                $this->getParameter('statusId'),
                $this->getParameter('groupId')
            );

            if (!$this->cashbacks) {
                throw new LogicException("Customer `{$this->customer->getId()}` doesn't have any eligible cashbacks");
            }

            $this->tabs = [
                'cashbacks' => 'All Cashbacks',
                'cashbacksDetails' => 'Details'
            ];

        } catch (Exception $e) {
            $this->handleException($e->getMessage(), 'cashbacks');
        }
    }


    public function renderCashbacksDetails()
    {
        $this->template->helper = $this->cashbackHelper;
    }


    /****************************************** import ******************************************/

    public function renderImport()
    {
        $cashBackForm = new ImportForm($this->node->getId() . '_import');
        $cashBackForm->startup([$this, 'processImportForm']);

        $this->template->form = $cashBackForm;
    }

    /**
     * @param ImportForm $form
     */
    public function processImportForm(ImportForm $form)
    {
        set_time_limit(100000);

        /** @var LoggerInterface $logger */
        $logger = $this->getService(CashBackDiLocator::LOGGER_CASH_BACK_IMPORTS);

        $data = $form->getValues();
        $file = new File($data['import']['tmp_name']);

        if ($data['bankType'] == Cashback::BANK_TYPE_BARCLAYS) {
            $logger->info('Barclays cash back import started');
            $this->processBarclaysImport($file, $logger);
        } elseif ($data['bankType'] == Cashback::BANK_TYPE_TSB) {
            $logger->info('TSB cash back import started');
            $this->processTsbImport($file, $logger);
        }
    }

    /**
     * @param File $file
     * @param LoggerInterface $logger
     */
    private function processBarclaysImport(File $file, LoggerInterface $logger)
    {
        /** @var BarclaysAccountsImporter $barclaysImporter */
        $barclaysImporter = $this->getService(BankingDiLocator::IMPORTER_BARCLAYS_ACCOUNTS);

        try {
            $barclaysImporter->import($file);
            $logger->info('Barclays cash back import finished');
            $this->flashMessage('Barclays cash backs imported', 'success');
            $this->redirect();
        } catch (ImportException $e) {
            $logger->info('Barclays cash back import finished with errors:');
            $logger->warning(implode(', ', $e->getErrors()));
            $this->handleErrors($e->getErrors());
        }
    }

    /**
     * @param File $file
     * @param LoggerInterface $logger
     */
    private function processTsbImport(File $file, LoggerInterface $logger)
    {
        /** @var TsbAccountsImporter $tsbAccountsImporter */
        $tsbAccountsImporter = $this->getService(BankingDiLocator::IMPORTER_TSB_ACCOUNTS);

        try {
            $tsbAccountsImporter->import($file);
            $logger->info('TSB cash back import finished');
            $this->flashMessage('TSB cash backs imported', 'success');
            $this->redirect();
        } catch (ImportException $e) {
            $logger->info('TSB cash back import finished with errors:');
            $logger->warning(implode(', ', $e->getErrors()));
            $this->handleErrors($e->getErrors());
        }
    }

    /****************************************** addCashback ******************************************/

    public function renderAddCashback()
    {
        $addCashbackForm = new AddCashbackForm($this->node->getId() . '_add_cashback');
        $addCashbackForm->startup(
            $this,
            $this->companyService,
            $this->cashbackService,
            $this->getService('cash_back_module.factories.cash_back_factory')
        );
        $this->template->form = $addCashbackForm;
    }

    /****************************************** private ******************************************/


    /**
     * @param $message
     * @param NULL $action
     */
    private function handleException($message, $action = NULL)
    {
        $this->flashMessage($message, 'error');
        $this->redirect($action);
    }

    /********************************** IBarclaysImportFormDelegate *********************************/

    /**
     * @param array $errors
     */
    private function handleErrors(array $errors)
    {
        $div = Html::el('div')->setText('Some cash backs could not be imported:');
        $ul = Html::el('ul', ['style' => 'margin: 5px 0 0 20px']);
        foreach ($errors as $error) {
            $ul->add(Html::el('li')->setText($error));
        }
        $div->add($ul);

        $this->flashMessage($div, 'error');
        $this->redirect('import');
    }

    /********************************** IAddCashbackDelegate *********************************/

    public function addCashbackFormSucceeded()
    {
        $this->flashMessage('Cashback added');
        $this->redirect();
    }

    /**
     * @param Exception $e
     */
    public function addCashbackFormFailed(Exception $e)
    {
        $this->handleException($e->getMessage());
    }
}
