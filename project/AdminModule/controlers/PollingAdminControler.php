<?php

use Entities\CompanyHouse\FormSubmission;
use Services\SubmissionService;

class PollingAdminControler extends CHAdminControler
{

    /**
     * @var array
     */
    protected $tabs = [
        'default' => 'Polling',
    ];

    /** @var array */
    private $pendingSubmissions;

    public function prepareDefault()
    {
        $this->pendingSubmissions = dibi::select('form_submission_id')
            ->from('ch_form_submission')
            ->where('response IN (%s)', [FormSubmission::RESPONSE_PENDING, FormSubmission::RESPONSE_INTERNAL_FAILURE])
            ->where('date_cancelled IS NULL')
            ->execute()
            ->fetchPairs();
    }

    public function renderDefault()
    {
        // polling all
        $formAll = new FForm($this->nodeId . '-' . $this->action . '-all');
        $formAll->addFieldset('Polling All');
        $formAll->addText('pendingCount', 'Submissions count:')
            ->setValue(count($this->pendingSubmissions))
            ->readonly()
            ->style('border: 0; background: white; color: black; font-weight: bold; padding: 0;');
        $formAll->addSubmit('submit', 'Submit')->class('btn');
        $formAll->onValid = [$this, 'Form_defaultFormPollingAllSubmitted'];
        $formAll->start();
        $this->template->formAll = $formAll;

        // polling one
        $formOne = new FForm($this->nodeId . '-' . $this->action . '-one');
        $formOne->addFieldset('Polling One');
        $formOne->addText('formSubmissionId', 'Submission Id:')
            ->addRule(FForm::Required, 'Please provide Form submission id')
            ->addRule(FForm::NUMERIC, 'Please provide numeric value');
        $formOne->addSubmit('submit', 'Submit')->class('btn');
        $formOne->onValid = [$this, 'Form_defaultFormPollingOneSubmitted'];
        $formOne->start();
        $this->template->formOne = $formOne;

        // resendMultiple
        $formResend = new FForm($this->nodeId . '-' . $this->action . '-resend-new-ids');
        $formResend->addFieldset('Resend with new IDs');
        $formResend->addArea('formSubmissionIds', 'Submission Ids(separated by comma):')
            ->addRule(FForm::Required, 'Please provide Form submission ids')
            ->cols(60)->rows(10);
        $formResend->addSubmit('submit', 'Submit')->class('btn')->onclick('return confirm("Are you sure? Only do this if you are sure these submissions weren\'t sent before!")');
        $formResend->onValid = [$this, 'Form_resendWithNewIdsSubmitted'];
        $formResend->start();
        $this->template->formResend = $formResend;
    }


    /**
     * Provides process submissions - just for one submission
     *
     * @param FForm $form
     * @return void
     * @throws Exception
     */
    public function Form_defaultFormPollingAllSubmitted(FForm $form)
    {
        try {
            PollingModel::processFormSubmissionStatus($this->pendingSubmissions);
            $this->flashMessage('Pollings have been sent.');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }

        $form->clean();
        $this->redirect();
    }

    /**
     * Provides process submissions - just for one submission
     *
     * @param FForm $form
     * @return void
     */
    public function Form_defaultFormPollingOneSubmitted(FForm $form)
    {
        try {
            $data = $form->getValues();
            $ids = (array) $data['formSubmissionId'];
            PollingModel::processFormSubmissionStatus($ids);
            $this->flashMessage('Pollings have been sent.');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $form->clean();
        $this->redirect();
    }

    public function Form_resendWithNewIdsSubmitted(FForm $form)
    {
        $data = $form->getValues();
        $submissionIds = array_filter(array_map('trim', explode(',', $data['formSubmissionIds'])));
        $sentSubmissionIds = [];
        $currentSubmission = NULL;

        /** @var SubmissionService $submissionService */
        $submissionService = Registry::$container->get(DiLocator::SERVICE_SUBMISSION);
        try {
            foreach ($submissionIds as $submissionId) {
                $currentSubmission = $submissionId;
                $submissionService->resubmitFormSubmissionById($submissionId);
                $sentSubmissionIds[] = $submissionId;
            }

            $this->flashMessage("All form submissions were resubmitted successfully.");
        } catch (Exception $e) {
            $message = sprintf(
                'Error encountered while resending submission: %s. Message: %s. Submissions not sent: %s',
                $currentSubmission,
                $e->getMessage(),
                implode(',', array_diff($submissionIds, $sentSubmissionIds))
            );

            $this->flashMessage($message, 'error');
            $this->logger->error($e, ['formSubmissionId' => $currentSubmission, 'message' => $message]);
        }

        $form->clean();
        $this->redirect();
    }
}
