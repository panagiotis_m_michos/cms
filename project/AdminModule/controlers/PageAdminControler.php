<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    PageAdminControler.php 2009-03-19 divak@gmail.com
 */

class PageAdminControler extends BaseAdminControler
{
	/** @var array */
	protected $tabs = array(
		'page' => 'Page',
		'layout' => 'Layout',
		'details' => 'Details',
		'menu' => 'Menu',
		'seo' => 'Seo'
	);
	
	/** @var array */
	protected $menuItems = array(
		'menu' => 'Menu',
		'testimonials' => 'Testimonials',
		'blog' => 'Blog',
		'support' => 'Email Support',
		'htmlBlock' => 'HTML Block'
	);
	
	
	/********************************* layout *********************************/
	
	
	public function renderLayout()
	{
		$form = new FForm("layout_{$this->nodeId}");
		
		$form->addFieldset('Disable');
		$form->addCheckbox('disabledLeftColumn', 'Left Column', 1)
			->setValue(FProperty::get('disabledLeftColumn')->value);
		$form->addCheckbox('disabledRightColumn', 'Right Column', 1)
			->setValue(FProperty::get('disabledRightColumn')->value);
                $form->addCheckbox('disabledFeedback', 'Disable Feedback', 1)
			->setValue(FProperty::get('disabledFeedback')->value);
                
		$form->addFieldset('Action');
		$form->addSubmit('submit', 'Submit')
			->class('btn')
			->style('width: 200px; height: 30px;');
			
		$form->onValid = array($this, 'Form_layoutFormSubmitted');
		$form->start();
		$this->template->form = $form;
	}
	
	public function Form_layoutFormSubmitted(FForm $form)
	{
		try {
			$data = $form->getValues(); 
			FProperty::get('disabledLeftColumn')->setValue($data['disabledLeftColumn'])->save();
			FProperty::get('disabledRightColumn')->setValue($data['disabledRightColumn'])->save();
                        FProperty::get('disabledFeedback')->setValue($data['disabledFeedback'])->save();
			$form->clean();
			$this->flashMessage('Data has been saved');
			$this->redirect();	
		} catch (Exception $e) {
		    $this->flashMessage($e->getMessage(), 'error');
		 	$this->redirect();
		}
	}
	
	
	
	/********************************* menu *********************************/

	
	public function prepareMenu()
	{
		$leftMenu = FProperty::getNodePropsValue('leftMenu');
		if ($leftMenu !== NULL) {
			$items= $values = array();
			$leftMenu = unserialize($leftMenu);
			foreach ($leftMenu['items'] as $key=>$val) {
				if (isset($this->menuItems[$key])) $items[$key] = $this->menuItems[$key];
				if ($val == 1) $values[] = $key;
			}
		} else {
			$values = array_keys($this->menuItems);
			$items = $this->menuItems;
		}
		
		// form
		$form = new FForm('pageMenu');
		$form->addCheckbox('items', 'Menu Items: ', $items)
			->setValue($values);
		$form->addSubmit('submit', 'Submit')->class('btn');
		$form->addText('order', NULL)->style('display: none;');
		$form->add('CmsFckArea', 'htmlBlock', 'Html Block: ')
			->setSize(500, 200)
			//->setToolbar('Basic')
			;
        $form->addCheckbox('menuCollapser', 'Collapse all menus: ', array('1'=>'yes'));
		$form->onValid = array($this, 'Form_menuFormSubmitted');
		$form->start();
		
		// set html block value
		if ($leftMenu !== NULL) {
                    $form['htmlBlock']->setValue($leftMenu['htmlBlock']);
                    $form['menuCollapser']->setValue($leftMenu['menuCollapser']);
		}
        
		$this->template->form = $form;
		$this->template->items = $items;
	}
	
	
	public function Form_menuFormSubmitted(FForm $form)
	{
		$leftMenu = array();
		$data = $form->getValues();
		$data['items'] = (array) $data['items'];
		
		// menu items
		$menuItems = explode(' ', $data['order']);
		foreach ($menuItems as $key=>$val) {
			$leftMenu['items'][$val] = in_array($val, $data['items']) ? 1 : 0;
		}
		
		// html block
		$leftMenu['htmlBlock'] = $data['htmlBlock'];

        // menuCollapser
		$leftMenu['menuCollapser'] = (int) $data['menuCollapser'];
		
		// serialize and save
		$leftMenu = serialize($leftMenu);
		FProperty::get('leftMenu')->setValue($leftMenu)->save();
		
		$form->clean();
		$this->flashMessage('Data has been updated');
		$this->redirect();
	}
	
	
	/************************************** page **************************************/
	
	
	public function preparePage()
	{
		// check for page language to edit 
		if (isset($this->get['lang'])) {
			$langs = FLanguage::getLanguages();
			if (isset($langs[$this->get['lang']])) {
				$lang = $this->get['lang'];
			} else {
				$lang = $this->lang;
			}
		} else {
			$lang = $this->lang;
		}
		
		// node to handle with
		$this->handleNode = new FNode($this->nodeId, $lang);
		
		// check if node for this language exists
		if(!$this->handleNode->exists()) { // TODO
			trigger_error("Page for `$lang` language doesn't exists!", E_USER_ERROR);
		}
		
		$this->handleLang = strtoupper($lang);
	}
	
	
	public function handlePage()
	{
		/**** lang form ****/
		
		$langs = array();
		foreach(FLanguage::getLanguages() as $key=>$val) {
			$langs[$key] = $val['name'];
		}
		
		if (!empty($langs) && count($langs) > 1) {
			$form = new FForm('lang', 'get');
			$form->addFieldset('Language');
			$form->addSelect('lang', 'Choose language: ', $langs)
				->onchange('this.form.submit()')
				->setValue($this->handleLang);
			$form->clean();
			$form->start();
			$this->template->langForm = $form;
		}
		
		
		/**** page form ****/
		
		$pageForm = new FForm($this->nodeId.'_'.$this->action);
		
		// fieldset
		$pageForm->addFieldset('Page in ' . $this->handleNode->page->languagePk . ' Language');
		$pageForm->addText('title','Title: * ')
			->setDescription('(Page title and name in menu)')
			->addRule(FForm::Required,'Title is required!')
			->size(30)
			->setValue($this->handleNode->page->title);
		$pageForm->addText('alternativeTitle','Alternative Title:')
			->setDescription('(Title for page. Is it empty, title will be used)')
			->size(50)
			->setValue($this->handleNode->page->alternativeTitle);
        $pageForm->addCheckbox('hasVisibleTitle', 'Disable Title:',1)
            ->setDescription('(Hide or unhide the page title on front)')    
            ->setValue(FProperty::get('hasVisibleTitle')->value);
        
		$pageForm->add('CmsFckArea','text','Page Text:')
			->setDescription('(Main text of page)')
			->setSize(500,300)
			->setValue($this->handleNode->page->text);
		
		// action
		$pageForm->addFieldset('Action');
		$pageForm->addSubmit('submit','Submit')
			->onclick('changes = false')
			->class('btn');
		$pageForm->onValid = array($this, 'pageFormSubmitted');
		
		// permissions
		$perm = array('title','alternativeTitle','text');
		foreach ($perm as $key=>$val) {
			if (!$this->isUserAllowed('page',$val)) {
				$pageForm[$val]->readonly();
			}
		}
		
		$pageForm->start();
		$this->template->pageForm = $pageForm;
	}
	
	
	public function pageFormSubmitted($form)
	{
		$this->handleNode->page->title = $form->getValue('title');
		$this->handleNode->page->alternativeTitle = $form->getValue('alternativeTitle');
		$this->handleNode->page->text = $form->getValue('text');
        FProperty::get('hasVisibleTitle')->setValue($form->getValue('hasVisibleTitle'))->save();
		$this->handleNode->save();
		$this->flashMessage('Data has been updated');
		$this->redirect(NULL);
	}
	
	
	/********************************* seo *********************************/
	
	
	
	public function handleSeo()
	{
		// get nodes for all langs
		$nodes = array();
		foreach(FLanguage::getLanguages() as  $key=>$val) {
			//$nodes[$key] = FNode::create($this->nodeId, $key);
			$nodes[$key] = new FNode($this->nodeId, $key);
		}
		
		// form
		$seoForm = new FForm($this->nodeId.'_'.$this->action);

        $seoForm->addFieldset('Exclude from sitemap');
        $seoForm->addCheckbox('excludeFromSitemap', 'Exclude', 1);
		
		// seo titles
		$seoForm->addFieldset('SEO Titles');
		foreach($nodes as  $key=>$node) {
			$seoForm->addText('seo_title'.strtolower($key), 'Seo title ('.$key.')')
				->setValue($node->page->seoTitle)
				->size(50);
		}

		// descriptions
		$seoForm->addFieldset('Description');
		foreach($nodes as  $key=>$node) {
			$seoForm->addArea('description_'.strtolower($key),'Description ('.$key.')')
				->setValue($node->page->description)
				->cols(40)->rows(3);
		}

		// keywords
		$seoForm->addFieldset('Keywords');
		foreach($nodes as  $key=>$node) {
			$seoForm->addArea('keywords_'.strtolower($key), 'Keywords ('.$key.')')
				->setValue($node->page->keywords)
				->cols(40)->rows(3);
		}

		// friendly url
		$seoForm->addFieldset("Friendly URL (can have 'a-z/-' and have to end with '/' or '.html')");
		foreach($nodes as  $key=>$node) {
			$seoForm->addText('friendly_url_'.strtolower($key), 'URL ('.$key.')')
				->size(50)
				->setValue($node->page->friendlyUrl)
				->addRule(FForm::Required,'Friendly URL is required!')
				->addRule(array($this, 'Form_SeoTitle'), Array('Friendly URL is not valid!', 'Friendly URL is not unique!', 'Friendly URL has been taken!'));
		}
			

		// action
		$seoForm->addFieldset('Action');
		$seoForm->addSubmit('submit', 'Submit')
			->class('btn');
		$seoForm->onValid = array($this, 'seoFormSubmitted');
		$seoForm->start();

		$this->template->seoForm = $seoForm;
	}
	
	
	/**
	 * @param object Form $form
	 * @return void 
	 */
	public function seoFormSubmitted(FForm $form)
	{
        $this->node->excludeFromSitemap = $form->getValue('excludeFromSitemap');
        $this->node->save();

		// get lang nodes
		$nodes = array();
		foreach(FLanguage::getLanguages() as  $key=>$val) {
			//$nodes[$key] = Node::create($this->nodeId, $key);
			$nodes[$key] = new FNode($this->nodeId, $key);
		}
		
		// clear friendly url - has to be unique in database
		foreach ($nodes as $key=>$node) {
			$node->page->friendlyUrl = $key . time();
			$node->page->save();
		}
		
		// update friendly url
		foreach($nodes as  $key=>$node) {
			$node->page->seoTitle = $form->getValue('seo_title'.strtolower($key));
			$node->page->description = $form->getValue('description_'.strtolower($key));
			$node->page->keywords = $form->getValue('keywords_'.strtolower($key));
			$node->page->friendlyUrl = $form->getValue('friendly_url_'.strtolower($key));
			$node->page->save();
		}
		
		// redirect
		$this->flashMessage('Data has been updated');
		$this->redirect(NULL);
	}
	
	
	/**
	 * Validator
	 * @param object $control
	 * @param mixed $error
	 * @param mixed $params
	 * @return mixed
	 */
	public function Form_SeoTitle($control, $error, $params)
	{
		// check if it's valid - (can have "a-z/-") and have to end with "/" or ".html"
		if (!preg_match('/^[a-z0-9\/\-]{0,}([\/]|\.html){1}$/',$control->getValue())) {
			$error = $error[0];
			return $error;
		}

		// test for data from user, if they are unique
		foreach (FLanguage::getLanguages() as $key=>$val) {
			if ($control->getName() == 'friendly_url_'.strtolower($key)) {
				continue;
			}

			// it's not unique
			if ($control->owner['friendly_url_'.strtolower($key)]->getValue() == $control->getValue()) {
				$error = $error[1];
				return $error;
			}
		}
		
	    $page = $this->db->select('page_id')
	    	->from(TBL_PAGES)
	    	->where('friendly_url=%s', $control->getValue())
	    		->and('node_id!=%i', $this->nodeId)
	    	->execute()->fetchSingle();
	    	
		if (!empty($page)) {
			$error = $error[2];
			return $error;
		}
		return TRUE;
	}
	
	
	
	/********************************* details *********************************/

	
	
	public function prepareDetails()
	{
		$this->handleNode = new FNode($this->nodeId);
	}
	
	
	
	public function handleDetails()
	{
		$users = $this->db->select('user_id, login')->from(TBL_USERS)->orderBy('login')->execute()->fetchPairs();
		
		// form
		$detailsForm = new FForm($this->nodeId.'_'.$this->action);

		// page
		$detailsForm->addFieldset('Page');
		$detailsForm->addSelect('statusId','Status: ', FNode::$statuses)
			->setFirstOption('---- Select ---')
			->addRule(FForm::Required, 'Please provide status')
			->setValue($this->handleNode->statusId);

		// page info
		$detailsForm->addFieldset('Page Info');
		$detailsForm->add('DateSelect','dtc','Created:')
			->setDescription('(When was page created)')
			->disabled()
			->setValue($this->handleNode->dtc);
		$detailsForm->add('DateSelect','dtm','Modified:')
			->setDescription('(When was page edited)')
			->disabled()
			->setValue($this->handleNode->dtm);
		$detailsForm->addSelect('author_id','Author:', $users)
			->setDescription('(Who is the author of the page)')
			->disabled()
			->setValue($this->handleNode->authorId);
		$detailsForm->addSelect('editor_id','Editor:', $users)
			->setDescription('(Who is the editor of the page)')
			->disabled()
			->setValue($this->handleNode->editorId);

		// page showing
		$detailsForm->addFieldset('Page Showing:');
		$detailsForm->add('DateSelect','dt_show','Date show:')
			->setDescription('(Start of showing page without end.)')
			->setValue($this->handleNode->dtShow);

		// page start and end showing
		$detailsForm->addFieldset('Page Start and End Showing:');
		$detailsForm->addCheckbox('is_dt_sensitive','Date sensibility: ', 1)
			->setDescription('(You can set start and end time for showing page)')
			->setValue($this->handleNode->isDtSensitive);
		$detailsForm->add('DateSelect','dt_start','Start:')
			->setDescription('(Start of showing page)')
			->setValue($this->handleNode->dtStart);
		$detailsForm->add('DateSelect','dt_end','End:')
			->setDescription('(End of showing page)')
			->setValue($this->handleNode->dtEnd)
			->addRule('DtEnd', array('Date end have to be today or in the future!','Date end have to be greater than date start.'));

		// actions
		$detailsForm->addFieldset('Actions:');
		$detailsForm->addSubmit('submit','Submit')
			->class('btn');
		$detailsForm->onValid = array($this, 'detailsFormSubmitted');
		$detailsForm->start();
		$this->template->detailsForm = $detailsForm;
	}
	
	public function detailsFormSubmitted($form)
	{
		$this->handleNode->statusId = $form->getValue('statusId');
		$this->handleNode->dtShow = $form->getValue('dt_show');
		$this->handleNode->isDtSensitive = (int)$form->getValue('is_dt_sensitive');
		$this->handleNode->dtStart = $form->getValue('dt_start');
		$this->handleNode->dtEnd = $form->getValue('dt_end');
		$this->handleNode->save();
		$form->clean();
		$this->flashMessage('Data has been updated');
		$this->redirect(NULL);
	}
	
}
