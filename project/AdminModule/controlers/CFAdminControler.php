<?php

use Admin\CHForm;
use CompanyFormationModule\Views\PscView;
use PeopleWithSignificantControl\Interfaces\IPscAware;
use PeopleWithSignificantControl\Providers\PscChoicesProvider;

abstract class CFAdminControler extends CHAdminControler
{

    /** @var CHFiling */
    protected $chFiling;

    /**
     * @var Company
     */
    protected $company;

    /** @var int */
    protected $companyId;

    /** @var Customer */
    protected $customer;

    /** @var Person */
    protected $director;

    /** @var Person */
    protected $shareholder;

    /** @var IncorporationPersonPsc|IncorporationCorporatePsc */
    protected $psc;

    /** @var Person */
    protected $secretary;

    /**
     * @var PscChoicesProvider
     */
    private $pscChoicesProvider;

    public function startup()
    {
        parent::startup();

        $this->pscChoicesProvider = $this->getService('people_with_significant_control_module.providers.psc_choices_provider');
    }

    protected function prepareCFnoPscReason()
    {
        $this->setCompanyUpdateTabs('CFnoPscReason', 'Set no PSC reason');
        /** @var CompanyIncorporation $incorporation */
        $incorporation = $this->company->getLastIncorporationFormSubmission()->getForm();

        $form = new FForm('noPscs');
        $form->addFieldset('Change company name');
        $form->addCheckbox('noPscReason', $this->pscChoicesProvider->getNoPscReason(), 1);
        $form->addSubmit('save', 'Save')->class('btn btn-primary');
        $form->onValid = [$this, 'Form_CFnoPscReasonFormSubmitted'];
        $form->setInitValues(['noPscReason' => $incorporation->hasNoPscReason()]);
        $form->start();

        $this->template->form = $form;
        $this->template->companyId = $this->company->getCompanyId();
    }

    /**
     * @param FForm $form
     */
    public function Form_CFnoPscReasonFormSubmitted(FForm $form)
    {
        /** @var CompanyIncorporation $incorporation */
        $incorporation = $this->company->getLastIncorporationFormSubmission()->getForm();
        $noPscReason = $form->getValue('noPscReason');

        if ($noPscReason) {
            $incorporation->setNoPscReason(IPscAware::NO_INDIVIDUAL_OR_ENTITY_WITH_SIGNFICANT_CONTROL);
            foreach ($incorporation->getPersonPscs() as $psc) {
                $psc->remove();
            }
            foreach ($incorporation->getCorporatePscs() as $psc) {
                $psc->remove();
            }
        } else {
            $incorporation->setNoPscReason(NULL);
        }

        $form->clean();
        $this->flashMessage('PSC reason has been saved');
        $this->redirect('view', ['company_id' => $this->company->getCompanyId()]);
    }

    /*     * ***************************** CFaddPscPerson ********************************** */

    protected function prepareCFaddPscPerson()
    {
        $this->setCompanyUpdateTabs('CFaddPscPerson', 'Add person PSC');
        /** @var CompanyIncorporation $incorporation */
        $incorporation = $this->company->getLastIncorporationFormSubmission()->getForm();

        $prefillAddress = self::getPrefillAdresses($this->company);
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'person');
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];

        $this->template->form = $this->getComponentCFPscPersonForm('add', $prefillOfficers, $prefillAddress);
        $this->template->natureOfControls = $this->pscChoicesProvider->getNatureOfControlsTemplateStructure(PscChoicesProvider::PERSON, $incorporation->getCompanyType());
        $this->template->natureOfControlsDescription = $this->pscChoicesProvider->getNatureOfControlsTemplateDescription(PscChoicesProvider::PERSON);
        $this->template->companyId = $this->company->getCompanyId();
    }

    /**
     * @param FForm $form
     */
    public function Form_CFaddPscPersonFormSubmitted(FForm $form)
    {
        $data = $form->getValues();

        /** @var CompanyIncorporation $incorporation */
        $incorporation = $this->company->getLastIncorporationFormSubmission()->getForm();
        /** @var IncorporationPersonPsc $psc */
        $psc = $incorporation->getNewIncPerson('PSC');

        $person = $psc->getPerson();
        $person->setTitle($data['title']);
        $person->setForename($data['forename']);
        $person->setMiddleName($data['middle_name']);
        $person->setSurname($data['surname']);
        $person->setNationality($data['nationality']);
        $person->setDob($data['dob']);
        $person->setCountryOfResidence($data['country_of_residence']);
        $psc->setPerson($person);

        $address = $psc->getAddress();
        $address->setFields($data);
        $psc->setAddress($address);

        if ($data['residentialAddress'] == 1) {
            $address = $psc->getResidentialAddress();
            $address->setFields($data, 'residential_');
            $psc->setResidentialAddress($address);
        } else {
            $psc->setResidentialAddress($address);
        }

        $natureOfControl = new NatureOfControl;
        $natureOfControl->setFields($data);
        $psc->setNatureOfControl($natureOfControl);

        $psc->setConsentToAct(1);

        // save
        try {
            $psc->save();
            $incorporation->setNoPscReason(NULL);
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect();
        }

        $form->clean();
        $this->flashMessage('Person PSC has been added');
        $this->redirect('view', ['company_id' => $this->company->getCompanyId()]);
    }

    /*     * ***************************** CFeditPscPerson ********************************** */

    protected function prepareCFEditPscPerson()
    {
        $this->setCompanyUpdateTabs('CFeditPscPerson', 'Edit person PSC');

        if (!isset($this->get['psc_id'])) {
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }

        try {
            $this->psc = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncPerson($this->get['psc_id'], 'PSC');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }
    }

    protected function renderCFEditPscPerson()
    {
        /** @var CompanyIncorporation $incorporation */
        $incorporation = $this->company->getLastIncorporationFormSubmission()->getForm();

        $prefillAddress = self::getPrefillAdresses($this->company);
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'person');
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];

        $this->template->form = $this->getComponentCFPscPersonForm('edit', $prefillOfficers, $prefillAddress);
        $this->template->natureOfControls = $this->pscChoicesProvider->getNatureOfControlsTemplateStructure(PscChoicesProvider::PERSON, $incorporation->getCompanyType());
        $this->template->natureOfControlsDescription = $this->pscChoicesProvider->getNatureOfControlsTemplateDescription(PscChoicesProvider::PERSON);
        $this->template->companyId = $this->company->getCompanyId();
    }

    /**
     * @param FForm $form
     */
    public function Form_CFeditPscPersonFormSubmitted(FForm $form)
    {
        $data = $form->getValues();

        $person = $this->psc->getPerson();
        $person->setTitle($data['title']);
        $person->setForename($data['forename']);
        $person->setMiddleName($data['middle_name']);
        $person->setSurname($data['surname']);
        $person->setDob($data['dob']);
        $person->setNationality(isset($data['nationality']) ? $data['nationality'] : NULL);
        $person->setCountryOfResidence($data['country_of_residence']);
        $this->psc->setPerson($person);

        $address = $this->psc->getAddress();
        $address->setFields($data);
        $this->psc->setAddress($address);

        $address = $this->psc->getResidentialAddress();
        $address->setFields($data, 'residential_');
        $this->psc->setResidentialAddress($address);

        $natureOfControl = new NatureOfControl;
        $natureOfControl->setFields($data);
        $this->psc->setNatureOfControl($natureOfControl);

        $this->psc->setConsentToAct(1);

        try {
            $this->psc->save();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect();
        }

        $form->clean();
        $this->flashMessage('Person PSC has been updated');
        $this->redirect('view', ['company_id' => $this->company->getCompanyId()]);
    }

    /*     * ***************************** CFshowPscPerson ********************************** */

    public function prepareCFshowPscPerson()
    {
        $this->setCompanyUpdateTabs('CFshowPscPerson', 'Show person PSC');

        if (!isset($this->get['psc_id'])) {
            $this->redirect('view', ['company_id' => $this->company->getCompanyId()]);
        }

        try {
            /** @var IncorporationPersonPsc $person */
            $psc = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncPerson($this->get['psc_id'], 'PSC');
            $this->template->psc = new PscView($psc);
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('view', ['company_id' => $this->company->getCompanyId()]);
        }
    }

    /*     * ***************************** CFdeletePscPerson ********************************** */

    protected function handleCFdeletePscPerson()
    {
        if (isset($this->get['psc_id'])) {
            try {
                $psc = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncPerson($this->get['psc_id'], 'dir');
                $psc->remove();
                $this->flashMessage('Person PSC has been deleted');
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
            }
        }
        $this->redirect('view', ['company_id' => $this->company->getCompanyId()]);
    }

    /*     * ***************************** CFaddPscCorporate ********************************** */

    protected function prepareCFaddPscCorporate()
    {
        $this->setCompanyUpdateTabs('CFaddPscCorporate', 'Add corporate PSC');
        /** @var CompanyIncorporation $incorporation */
        $incorporation = $this->company->getLastIncorporationFormSubmission()->getForm();

        $prefillAddress = self::getPrefillAdresses($this->company);
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'corporate');
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];

        $this->template->form = $this->getComponentCFPscCorporateForm('add', $prefillOfficers, $prefillAddress);
        $this->template->natureOfControls = $this->pscChoicesProvider->getNatureOfControlsTemplateStructure(PscChoicesProvider::CORPORATE, $incorporation->getCompanyType());
        $this->template->natureOfControlsDescription = $this->pscChoicesProvider->getNatureOfControlsTemplateDescription(PscChoicesProvider::CORPORATE);
        $this->template->companyId = $this->company->getCompanyId();
    }

    /**
     * @param FForm $form
     */
    public function Form_CFaddPscCorporateFormSubmitted(FForm $form)
    {
        $data = $form->getValues();
        /** @var CompanyIncorporation $incorporation */
        $incorporation = $this->company->getLastIncorporationFormSubmission()->getForm();
        /** @var IncorporationCorporatePsc $psc */
        $psc = $incorporation->getNewIncCorporate('PSC');

        $corporate = $psc->getCorporate();
        $corporate->setFields($data);
        $psc->setCorporate($corporate);

        $address = $psc->getAddress();
        $address->setFields($data);
        $psc->setAddress($address);

        $psc->setConsentToAct(1);

        $identification = $psc->getIdentification(Identification::PSC);
        $identification->setFields($data);
        $psc->setIdentification($identification);

        $natureOfControl = new NatureOfControl;
        $natureOfControl->setFields($data);
        $psc->setNatureOfControl($natureOfControl);

        $psc->save();
        $incorporation->setNoPscReason(NULL);

        $form->clean();
        $this->flashMessage("Corporate PSC has been added");
        $this->redirect('view', ['company_id' => $this->company->getId()]);
    }

    /*     * ******************************************* edit director corporate ************************************************** */

    protected function prepareCFeditPscCorporate()
    {
        $this->setCompanyUpdateTabs('CFeditPscCorporate', 'Edit corporate PSC');

        if (!isset($this->get['psc_id'])) {
            $this->redirect('view', ['company_id' => $this->company->getId()]);
        }

        try {
            $this->psc = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncCorporate($this->get['psc_id'], 'PSC');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('view', ['company_id' => $this->company->getId()]);
        }
    }

    protected function renderCFeditPscCorporate()
    {
        /** @var CompanyIncorporation $incorporation */
        $incorporation = $this->company->getLastIncorporationFormSubmission()->getForm();

        $prefillAddress = self::getPrefillAdresses($this->company);
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'corporate');
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];

        $this->template->form = $this->getComponentCFPscCorporateForm('edit', $prefillOfficers, $prefillAddress);
        $this->template->natureOfControls = $this->pscChoicesProvider->getNatureOfControlsTemplateStructure(PscChoicesProvider::CORPORATE, $incorporation->getCompanyType());
        $this->template->natureOfControlsDescription = $this->pscChoicesProvider->getNatureOfControlsTemplateDescription(PscChoicesProvider::CORPORATE);
        $this->template->companyId = $this->company->getId();
    }

    /**
     * @param FForm $form
     */
    public function Form_CFeditPscCorporateFormSubmitted(FForm $form)
    {
        $data = $form->getValues();

        $person = $this->psc->getCorporate();
        $person->setFields($data);
        $this->psc->setCorporate($person);

        $address = $this->psc->getAddress();
        $address->setFields($data);
        $this->psc->setAddress($address);

        $this->psc->setConsentToAct(1);

        $identification = $this->psc->getIdentification();
        $identification->setFields($data);
        $this->psc->setIdentification($identification);

        $natureOfControl = new NatureOfControl;
        $natureOfControl->setFields($data);
        $this->psc->setNatureOfControl($natureOfControl);

        try {
            $this->psc->save();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect();
        }

        $form->clean();
        $this->flashMessage('Corporate PSC has been updated');
        $this->redirect('view', ['company_id' => $this->company->getId()]);
    }

    /*     * ***************************** CFshowPscCorporate ********************************** */

    public function prepareCFshowPscCorporate()
    {
        $this->setCompanyUpdateTabs('CFshowPscCorporate', 'View corporate PSC');

        if (!isset($this->get['psc_id'])) {
            $this->redirect('view', ['company_id' => $this->company->getCompanyId()]);
        }

        try {
            $psc = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncCorporate($this->get['psc_id'], 'PSC');
            $this->template->psc = new PscView($psc);
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('view', ['company_id' => $this->company->getCompanyId()]);
        }
    }

    /*     * ***************************** CFdeletePscCorporate ********************************** */

    protected function handleCFdeletePscCorporate()
    {
        if (isset($this->get['psc_id'])) {
            try {
                $director = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncCorporate($this->get['psc_id'], 'dir');
                $director->remove();
                $this->flashMessage('Corporate PSC has been deleted');
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
            }
        }
        $this->redirect('view', ['company_id' => $this->company->getCompanyId()]);
    }

    protected function handleCFaddSuggestedPscs()
    {
        /** @var CompanyIncorporation $incorporation */
        $incorporation = $this->company->getLastIncorporationFormSubmission()->getForm();
        $companyType = $incorporation->getCompanyType();

        if (!$incorporation->getPossiblePscs()) {
            switch ($companyType) {
                case CompanyIncorporation::LIMITED_BY_SHARES:
                    $message = 'Sorry, at this time the auto-suggest only works if the officers are both directors and shareholders. Please add the PSCs manually.';
                    break;
                case CompanyIncorporation::BY_GUARANTEE:
                    $message = 'The auto-suggest PSCs will only work if the company has only one person Member which is also a Director. Please add a Member and retry the auto-suggest';
                    break;
                default:
                    $message = "Auto-suggesting PSCs for company of type {$companyType} is not possible";
            }
            $this->flashMessage($message);
            $this->redirect('view', ['company_id' => $this->company->getId()]);
        }

        $suggestedPscs = $incorporation->getSuggestedPscs();
        foreach ($suggestedPscs as $psc) {
            $psc->save();
        }

        $incorporation->setPreAddedPscs(TRUE);

        //todo: psc - different error message when PSCs are alredy added and for BYGUAR?
        if (empty($suggestedPscs)) {
            $this->flashMessage('The auto-suggest feature could not add any PSC. None of the shareholders own more than 25% of the shares. You will need to add the PSCs manually');
        } else {
            $incorporation->setNoPscReason(NULL);
            $this->flashMessage('The auto-suggested PSCs have been added successfully, please double check the information and amend if necessary');
        }

        $this->redirect('view', ['company_id' => $this->company->getId()]);
    }

    /*     * ***************************** changeCompanyName ********************************** */

    protected function prepareChangeCompanyName()
    {
        $form = new FForm('changeCompanyName');
        $form->addFieldset('Change company name');
        $form->addText('companyName', 'Company name: ')
            ->addRule(FForm::Required, 'Please provide company name')
        //->addRule(array($this, 'Validator_companyNameChars'), 'Illegal characters detected.', '#[^\x20-\x7E]#')
        ;
        $form->addSubmit('change', 'Change')->class('btn');
        $form->onValid = array($this, 'Form_changeCompanyNameFormSubmitted');
        $form->start();
        $this->template->form = $form;

        // tabs
        $this->tabs['view'] = array('title' => $this->company->getCompanyName(), 'params' => "company_id={$this->company->getCompanyId()}");
        $this->tabs['changeCompanyName'] = array('title' => 'Change company name', 'params' => "company_id={$this->company->getCompanyId()}");
    }

    /**
     * @param FForm $form
     */
    public function Form_changeCompanyNameFormSubmitted(FForm $form)
    {
        try {
            $companyName = $form->getValue('companyName');
            $this->company->changeCompanyName($form->getValue('companyName'));
            $this->flashMessage('Company name has been changed.');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
        }
        $form->clean();
        $this->redirect('view', "company_id={$this->company->getCompanyId()}");
    }

    /*     * ***************************** updateOffice ********************************** */

    public function prepareCFupdateOffice()
    {
        $this->setCompanyUpdateTabs('CFupdateOffice', 'Registered office');
        $prefillAddress = CUAdminControler::getPrefillAdresses($this->company);
        $this->template->jsAdresses = $prefillAddress['js'];

        $form = new CHForm($this->action . '-' . $this->get['company_id']);

        // if has registered office
        $registeredOfficeId = $this->company->getRegisteredOfficeId();
        if ($registeredOfficeId) {
            // our registered office
            $form->addFieldset('Registered office');
            $form->addCheckbox('ourRegisteredOffice', 'Our registered office  ', 1)
                ->setValue($this->company->getLastIncorporationFormSubmission()->getForm()->hasMSGRegisteredOffice());
        }

        // address
        $form->addFieldset('Prefill');
        $form->addSelect('prefill', 'Address', $prefillAddress['select'])
            ->setFirstOption('--- Select ---');

        $form->addFieldset('Address');
        $form->addText('premise', 'Premise *')
            ->addRule(array('CHValidator', 'Validator_officeRequired'), 'Please provide Premise')
            ->addRule('MaxLength', "Premise can't be more than 50 characters", 50);
        $form->addText('street', 'Street *')
            ->addRule(array('CHValidator', 'Validator_officeRequired'), 'Please provide Street')
            ->addRule('MaxLength', "Street can't be more than 50 characters", 50);
        $form->addText('thoroughfare', 'Address 3')
            ->addRule('MaxLength', "Address 3 can't be more than 50 characters", 50);
        $form->addText('post_town', 'Town *')
            ->addRule(array('CHValidator', 'Validator_officeRequired'), 'Please provide town')
            ->addRule('MaxLength', "Town can't be more than 50 characters", 50);
        $form->addText('county', 'County')
            ->addRule('MaxLength', "County can't be more than 50 characters", 50);
        $form->addText('postcode', 'Postcode *')
            ->addRule(array('CHValidator', 'Validator_officeRequired'), 'Please provide postcode')
            ->addRule('MaxLength', "Postcode can't be more than 15 characters", 15);
        $form->addSelect('country', 'Country *', Address::$registeredOfficeCountries)
            ->setFirstOption('--- Select ---')
            ->addRule(array('CHValidator', 'Validator_officeRequired'), 'Please provide country')
            ->addRule('MaxLength', "County can't be more than 50 characters", 50);

        // action
        $form->addFieldset('Action');
        $form->addSubmit('save', 'Save')->class('btn');
        $form->onValid = array($this, 'Form_CFupdateOfficeFormSubmitted');

        $address = $this->company->getLastIncorporationFormSubmission()->getForm()->getAddress()->getFields();
        $form->setInitValues($address);
        $form->start();
        $this->template->form = $form;
    }

    /**
     * @param FForm $form
     */
    public function Form_CFupdateOfficeFormSubmitted(FForm $form)
    {
        $lastForm = $this->company->getLastIncorporationFormSubmission()->getForm();
        $address = $lastForm->getAddress();
        $registeredOfficeId = $this->company->getRegisteredOfficeId();

        // has register office
        if ($registeredOfficeId != NULL && isset($form['ourRegisteredOffice']) && $form['ourRegisteredOffice']->getValue() == 1) {
            try {
                CFRegisteredOfficeForm::saveOurRegisteredOffice($this->company, $this->company->getRegisteredOfficeId());
                $this->flashMessage('Register office has been changed');
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage());
            }
        } else {
            $address->setFields($form->getValues());
            $lastForm->setAddress($address);
            $this->company->getLastIncorporationFormSubmission()->getForm()->setMSGRegisteredOffice(FALSE);
            $this->flashMessage('Register office has been changed');
        }

        $form->clean();
        $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
    }

    /*     * ***************************** CFchangeShareCapital ********************************** */

    //todo: shares - delete?
    public function prepareCFchangeShareCapital_OLD()
    {
        $this->setCompanyUpdateTabs('CFchangeShareCapital', 'Change share capital');

        $form = new FForm('CFchangeShareCapital');

        // share capital
        $form->addFieldset('Share capital');
        $form->addSelect('currency', 'Share Currency', IncorporationCapital::$currency);
        $form->addText('share_class', 'Share Class')
            ->setValue('ORD')
            ->readonly();
        $form->addText('aggregate_nom_value', 'Value per Share')
            ->addRule(FForm::Required, 'Please provide value per shares')
            ->addRule(FForm::NUMERIC, 'Value per share has to be numeric')
            ->addRule(FForm::GREATER_THAN, 'Value per share has to be greater than #1', 0)
            ->addRule(FForm::REGEXP, 'Amount per share can be int or decimal with maximum 6 fraction digits', '#^\d+(.{1}(\d{1,6})){0,1}$#')
            ->setValue(1);

        // share capital
        $form->addFieldset('Action');
        $form->addSubmit('continueprocess', 'Save')->class('btn');


        $form->onValid = array($this, 'Form_CFchangeShareCapital');

        $capitals = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncShares();
        if (!empty($capitals)) {
            $capital = $capitals[0]->getShares()->getFields();
            $form->setInitValues($capital);
        }

        // PLC
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() == 'PLC') {
            $form['currency']->setDisabledOptions('USD');
            $form['num_shares']->addRule(array($this, 'Validator_NumberOfShares'), 'Minimum share capital is &pound;50,000 or &euro;65,600', array('GBP' => 50000, 'EUR' => 65600));
        }

        $form->start();
        $this->template->form = $form;
    }

    /**
     * @param FForm $form
     */
    public function Form_CFchangeShareCapital(FForm $form)
    {
        $companyIncorporation = $this->company->getLastIncorporationFormSubmission()->getForm();

        $authorisedCapitals = $companyIncorporation->getIncShares();
        if (!empty($authorisedCapitals)) {
            $authorisedCapital = $authorisedCapitals[0];
        } else {
            $authorisedCapital = $companyIncorporation->getNewIncShares();
        }

        $shares = $authorisedCapital->getShares();

        // fields
        $fields = $form->getValues();
        $fields['num_shares'] = 1000;
        $fields['amount_paid'] = $fields['aggregate_nom_value'] * $fields['num_shares'];
        $fields['amount_unpaid'] = 0;
        $fields['prescribed_particulars'] = 'Full voting rights';

        $shares->setFields($fields);

        $authorisedCapital->setShares($shares);
        $authorisedCapital->save();

        $form->clean();
        $this->flashMessage('Shares has been changed');
        $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
    }

    /**
     * @param FControl $control
     * @param string $error
     * @param array $params
     * @return bool
     */
    public function Validator_NumberOfShares(FControl $control, $error, $params)
    {
        $currency = $control->owner['currency']->getValue();
        if (isset($params[$currency])) {
            if ($control->getValue() < $params[$currency]) {
                return $error;
            }
        }
        return TRUE;
    }

    /*     * ***************************** CFaddDirectorPerson ********************************** */

    protected function prepareCFaddDirectorPerson()
    {
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() != 'LLP') {
            $type = 'director';
        } else {
            $type = 'member';
        }

        $this->setCompanyUpdateTabs('CFaddDirectorPerson', 'Add ' . $type . ' person');

        // prefill
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'person');
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];

        $form = $this->getComponentCFDirectorPersonForm('add', $prefillOfficers, $prefillAddress);
        $this->template->form = $form;
        $this->template->companyId = $this->company->getCompanyId();
    }

    /**
     * @param FForm $form
     */
    public function Form_CFaddDirectorPersonFormSubmitted(FForm $form)
    {
        $data = $form->getValues();
        $data['residential_secure_address_ind'] = $data['residentialSecureAddressInd'];

        // get director
        $director = $this->company->getLastIncorporationFormSubmission()->getForm()->getNewIncPerson('dir');

        // set person
        $person = $director->getPerson();
        $person->setTitle($data['title']);
        $person->setForename($data['forename']);
        $person->setMiddleName($data['middle_name']);
        $person->setSurname($data['surname']);
        $person->setNationality($data['nationality']);
        $person->setOccupation($data['occupation']);
        $person->setDob($data['dob']);
        $person->setCountryOfResidence($data['country_of_residence']);
        $director->setPerson($person);

        // set address
        $address = $director->getAddress();
        $address->setFields($data);
        $director->setAddress($address);

        // set residential address
        if ($data['residentialAddress'] == 1) {
            $address = $director->getResidentialAddress();
            $address->setFields($data, 'residential_');
            $director->setResidentialAddress($address);
        } else {
            $address->setSecureAddressInd($data['residential_secure_address_ind']);
            $director->setResidentialAddress($address);
        }

        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() == 'LLP') {
            $authetication = $director->getAuthentication();
            $authetication->setFields($data);
            $director->setAuthentication($authetication);
        }

        $director->setConsentToAct($data['consentToAct']);

        // save
        try {
            $director->save();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect();
        }

        $form->clean();
        $this->flashMessage('Director has been added');
        $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
    }

    /*     * ***************************** CFeditDirectorPerson ********************************** */

    protected function prepareCFEditDirectorPerson()
    {
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() != 'LLP') {
            $type = 'director';
        } else {
            $type = 'member';
        }

        $this->setCompanyUpdateTabs('CFeditDirectorPerson', 'Edit ' . $type . ' person');

        // check get
        if (!isset($this->get['director_id'])) {
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }

        // check existing director
        try {
            $this->director = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncPerson($this->get['director_id'], 'dir');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }
    }

    protected function renderCFEditDirectorPerson()
    {
        // prefill
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'person');
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];

        $form = $this->getComponentCFDirectorPersonForm('edit', $prefillOfficers, $prefillAddress);
        $this->template->form = $form;
        $this->template->companyId = $this->company->getCompanyId();
    }

    /**
     * @param FForm $form
     */
    public function Form_CFeditDirectorPersonFormSubmitted(FForm $form)
    {
        $data = $form->getValues();
        $data['residential_secure_address_ind'] = $data['residentialSecureAddressInd'];

        $person = $this->director->getPerson();
        $person->setTitle($data['title']);
        $person->setForename($data['forename']);
        $person->setMiddleName($data['middle_name']);
        $person->setSurname($data['surname']);
        //LLP members don't have nationality and occupation
        $person->setNationality(isset($data['nationality']) ? $data['nationality'] : NULL);
        $person->setOccupation(isset($data['occupation']) ? $data['occupation'] : NULL);
        $person->setDob($data['dob']);
        $person->setCountryOfResidence($data['country_of_residence']);
        $this->director->setPerson($person);

        // address
        $address = $this->director->getAddress();
        $address->setFields($data);
        $this->director->setAddress($address);

        // residential address
        $address = $this->director->getResidentialAddress();
        $address->setFields($data, 'residential_');
        $this->director->setResidentialAddress($address);

        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() == 'LLP') {
            $authetication = $this->director->getAuthentication();
            $authetication->setFields($data);
            $this->director->setAuthentication($authetication);
        }

        $this->director->setConsentToAct($data['consentToAct']);

        // save
        try {
            $this->director->save();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect();
        }

        $form->clean();
        $this->flashMessage('Director has been updated');
        $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
    }

    /*     * ***************************** CFshowDirectorPerson ********************************** */

    public function prepareCFshowDirectorPerson()
    {
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() != 'LLP') {
            $type = 'director';
        } else {
            $type = 'member';
        }

        $this->setCompanyUpdateTabs('CFshowDirectorPerson', 'Show ' . $type . ' person');

        // check get
        if (!isset($this->get['director_id'])) {
            $this->redirect('view', "company_id={$this->company->getCompanyId()}");
        }

        // check existing director
        try {
            $person = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncPerson($this->get['director_id'], 'dir');
            $this->template->director = $person->getFields();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('view', "company_id={$this->company->getCompanyId()}");
        }
    }

    /*     * ***************************** CFdeleteDirectorPerson ********************************** */

    protected function handleCFdeleteDirectorPerson()
    {
        // delete director
        if (isset($this->get['director_id'])) {
            try {
                $director = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncPerson($this->get['director_id'], 'dir');
                $director->remove();
                $this->flashMessage('Director has been deleted');
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
            }
        }
        $this->redirect('view', "company_id={$this->company->getCompanyId()}");
    }

    /*     * ***************************** CFaddDirectorCorporate ********************************** */

    protected function prepareCFaddDirectorCorporate()
    {
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() != 'LLP') {
            $type = 'director';
        } else {
            $type = 'member';
        }

        $this->setCompanyUpdateTabs('CFaddDirectorCorporate', 'Add ' . $type . ' corporate');

        // prefill
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'corporate');
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];

        $form = $this->getComponentCFDirectorCorporateForm('add', $prefillOfficers, $prefillAddress);
        $this->template->form = $form;
        $this->template->companyId = $this->company->getCompanyId();
    }

    /**
     * @param FForm $form
     */
    public function Form_CFaddDirectorCorporateFormSubmitted(FForm $form)
    {
        $data = $form->getValues();

        // get corporate
        $directorCorporate = $this->company->getLastIncorporationFormSubmission()->getForm()->getNewIncCorporate('dir');
        $corporate = $directorCorporate->getCorporate();
        $corporate->setFields($data);
        $directorCorporate->setCorporate($corporate);

        // address
        $address = $directorCorporate->getAddress();
        $address->setFields($data);
        $directorCorporate->setAddress($address);

        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() == 'LLP') {
            $authetication = $directorCorporate->getAuthentication();
            $authetication->setFields($data);
            $directorCorporate->setAuthentication($authetication);
        }

        $directorCorporate->setConsentToAct($data['consentToAct']);

        // identification
        if ($data['eeaType'] == 1) {
            $identification = $directorCorporate->getIdentification(Identification::EEA);
        } else {
            $identification = $directorCorporate->getIdentification(Identification::NonEEA);
        }

        $identification->setFields($data);
        $directorCorporate->setIdentification($identification);
        $directorCorporate->save();

        $form->clean();
        $this->flashMessage("Corporate director has been added");
        $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
    }

    /*     * ******************************************* edit director corporate ************************************************** */

    protected function prepareCFeditDirectorCorporate()
    {
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() != 'LLP') {
            $type = 'director';
        } else {
            $type = 'member';
        }

        $this->setCompanyUpdateTabs('CFeditDirectorCorporate', 'Edit ' . $type . ' corporate');

        // check get
        if (!isset($this->get['director_id'])) {
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }

        // check existing director
        try {
            $this->director = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncCorporate($this->get['director_id'], 'dir');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }
    }

    protected function renderCFeditDirectorCorporate()
    {
        // prefill
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'corporate');
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];

        $form = $this->getComponentCFDirectorCorporateForm('edit', $prefillOfficers, $prefillAddress);
        $this->template->form = $form;
        $this->template->companyId = $this->company->getCompanyId();
    }

    /**
     * @param FForm $form
     */
    public function Form_CFeditDirectorCorporateFormSubmitted(FForm $form)
    {
        $data = $form->getValues();

        $person = $this->director->getCorporate();
        $person->setFields($data);
        $this->director->setCorporate($person);

        // address
        $address = $this->director->getAddress();
        $address->setFields($data);
        $this->director->setAddress($address);

        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() == 'LLP') {
            $authetication = $this->director->getAuthentication();
            $authetication->setFields($data);
            $this->director->setAuthentication($authetication);
        }

        $this->director->setConsentToAct($data['consentToAct']);

        // identification
        if ($data['eeaType'] == 1) {
            $identification = $this->director->getIdentification(Identification::EEA);
        } else {
            $identification = $this->director->getIdentification(Identification::NonEEA);
        }
        $identification->setFields($data);
        $this->director->setIdentification($identification);

        // save
        try {
            $this->director->save();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect();
        }

        $form->clean();
        $this->flashMessage('Director has been updated');
        $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
    }

    /*     * ***************************** CFshowDirectorCorporate ********************************** */

    public function prepareCFshowDirectorCorporate()
    {
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() != 'LLP') {
            $type = 'director';
        } else {
            $type = 'member';
        }

        $this->setCompanyUpdateTabs('CFshowDirectorCorporate', 'View ' . $type . ' corporate');

        // check get
        if (!isset($this->get['director_id'])) {
            $this->redirect('view', "company_id={$this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyId()}");
        }

        // check existing director
        try {
            $director = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncCorporate($this->get['director_id'], 'dir');
            $this->template->director = $director->getFields();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('view', "company_id={$this->company->getCompanyId()}");
        }
    }

    /*     * ***************************** CFdeleteDirectorCorporate ********************************** */

    protected function handleCFdeleteDirectorCorporate()
    {
        // delete director
        if (isset($this->get['director_id'])) {
            try {
                $director = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncCorporate($this->get['director_id'], 'dir');
                $director->remove();
                $this->flashMessage('Director has been deleted');
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
            }
        }
        $this->redirect('view', "company_id={$this->company->getCompanyId()}");
    }

    /*     * ***************************** CFaddShareholderPerson ********************************** */

    protected function prepareCFaddShareholderPerson()
    {
        $this->setCompanyUpdateTabs('CFaddShareholderPerson', 'Add Shaholder Person');

        // prefill
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'person');
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
        $this->template->jsPrefillAdresses = $prefillAddress['js'];

        $this->template->form = $this->getComponentCFshaholderPersonForm('add', $prefillOfficers, $prefillAddress);
        $this->template->companyType = $this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType();
        $this->template->companyId = $this->company->getCompanyId();
    }

    /**
     * @param FForm $form
     */
    public function Form_CFaddShareholderPersonFormSubmitted(FForm $form)
    {
        $data = $form->getValues();
        $shareholder = $this->company->getLastIncorporationFormSubmission()->getForm()->getNewIncPerson('sub');

        $person = $shareholder->getPerson();
        if (!empty($data['title'])) {
            $person->setTitle($data['title']);
        }
        $person->setForename($data['forename']);
        $person->setSurname($data['surname']);
        $person->setMiddleName($data['middle_name']);
        $shareholder->setPerson($person);

        // shares
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() != 'BYGUAR') {
            $shares = $shareholder->getAllotmentShares();
            $shares->setShareClass('ORD');
            $shares->setNumShares($data['num_shares']);
            $shares->setCurrency($data['currency']);
            $shares->setShareValue($data['share_value']);
            $shareholder->setAllotmentShares($shares);
        }

        // address
        $address = $shareholder->getAddress();
        $address->setFields($data);
        $shareholder->setAddress($address);

        // authentication
        $authetication = $shareholder->getAuthentication();
        $authetication->setFields($data);
        $shareholder->setAuthentication($authetication);

        // save
        try {
            $shareholder->save();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect();
        }

        $form->clean();
        $this->flashMessage('Shareholder has been added');
        $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
    }

    /*     * ******************************************* CFeditShareholderPerson ************************************************** */

    protected function prepareCFeditShareholderPerson()
    {
        // check get
        if (!isset($this->get['shareholder_id'])) {
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }

        // check existing director
        try {
            $this->shareholder = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncPerson($this->get['shareholder_id'], 'sub');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }

        $this->setCompanyUpdateTabs('CFeditShareholderPerson', 'Edit Shaholder Person');
    }

    protected function renderCFeditShareholderPerson()
    {

        // prefill
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'person');
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
        $this->template->jsPrefillAdresses = $prefillAddress['js'];

        $form = $this->getComponentCFshaholderPersonForm('edit', $prefillOfficers, $prefillAddress);
        $this->template->form = $form;

        $this->template->companyType = $this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType();
    }

    /**
     * @param FForm $form
     */
    public function Form_CFeditShareholderPersonFormSubmitted(FForm $form)
    {
        $data = $form->getValues();

        $person = $this->shareholder->getPerson();
        if (!empty($data['title'])) {
            $person->setTitle($data['title']);
        }
        $person->setForename($data['forename']);
        $person->setMiddleName($data['middle_name']);
        $person->setSurname($data['surname']);
        $this->shareholder->setPerson($person);

        // address
        $address = $this->shareholder->getAddress();
        $address->setFields($data);
        $this->shareholder->setAddress($address);

        // shares
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() != 'BYGUAR') {
            $shares = $this->shareholder->getAllotmentShares();
            $shares->setShareClass($data['share_class']);
            $shares->setNumShares($data['num_shares']);
            $shares->setCurrency($data['currency']);
            $shares->setShareValue($data['share_value']);
            $this->shareholder->setAllotmentShares($shares);
        }

        // authentication
        $authetication = $this->shareholder->getAuthentication();
        $authetication->setFields($data);
        $this->shareholder->setAuthentication($authetication);

        // save
        try {
            $this->shareholder->save();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect();
        }

        $form->clean();
        $this->flashMessage('Shareholder has been updated');
        $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
    }

    /*     * ***************************** CFshowShareholderPerson ********************************** */

    public function prepareCFshowShareholderPerson()
    {
        $this->setCompanyUpdateTabs('CFshowShareholderPerson', 'Show Shareholder Person');

        // check get
        if (!isset($this->get['shareholder_id'])) {
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }

        // check existing shareholder
        try {
            $shareholder = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncPerson($this->get['shareholder_id'], 'sub');
            $this->template->shareholder = $shareholder->getFields();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }
    }

    /*     * ***************************** CFdeleteShareholderPerson ********************************** */

    protected function handleCFdeleteShareholderPerson()
    {
        // delete director
        if (isset($this->get['shareholder_id'])) {
            try {
                $director = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncPerson($this->get['shareholder_id'], 'sub');
                $director->remove();
                $this->flashMessage('Shareholder has been deleted');
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
            }
        }
        $this->redirect('view', "company_id={$this->company->getCompanyId()}");
    }

    /*     * ***************************** CFaddShareholderCorporate ********************************** */

    protected function prepareCFaddShareholderCorporate()
    {
        $this->setCompanyUpdateTabs('CFaddShareholderCorporate', 'Add Shareholder Corporate');

        // prefill
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'corporate');
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
        $this->template->jsPrefillAdresses = $prefillAddress['js'];

        $form = self::getComponentCFShareholderCorporateForm('insert', $prefillOfficers, $prefillAddress);
        $this->template->form = $form;
        $this->template->companyType = $this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType();
        $this->template->companyId = $this->company->getCompanyId();
    }

    /**
     * @param FForm $form
     */
    public function Form_CFaddShareholderCorporateFormSubmitted(FForm $form)
    {
        $data = $form->getValues();

        // get corporate
        $shareholderCorporate = $this->company->getLastIncorporationFormSubmission()->getForm()->getNewIncCorporate('sub');
        $corporate = $shareholderCorporate->getCorporate();
        $corporate->setFields($data);
        $shareholderCorporate->setCorporate($corporate);

        // address
        $address = $shareholderCorporate->getAddress();
        $address->setFields($data);
        $shareholderCorporate->setAddress($address);

        // shares
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() != 'BYGUAR') {
            $shares = $shareholderCorporate->getAllotmentShares();
            $shares->setShareClass('ORD');
            $shares->setNumShares($data['num_shares']);
            $shares->setCurrency($data['currency']);
            $shares->setShareValue($data['share_value']);
            $shareholderCorporate->setAllotmentShares($shares);
        }

        // authentication
        $authetication = $shareholderCorporate->getAuthentication();
        $authetication->setFields($data);
        $shareholderCorporate->setAuthentication($authetication);

        $shareholderCorporate->save();

        $form->clean();
        $this->flashMessage("Corporate shareholder has been added");
        $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
    }

    /*     * ******************************************* CFeditShareholderCorporate ************************************************** */

    protected function prepareCFeditShareholderCorporate()
    {
        $this->setCompanyUpdateTabs('CFeditShareholderCorporate', 'Edit Shareholder Corporate');

        // check get
        if (!isset($this->get['shareholder_id'])) {
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }

        // check existing shareholder
        try {
            $this->shareholder = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncCorporate($this->get['shareholder_id'], 'sub');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }
    }

    protected function renderCFeditShareholderCorporate()
    {
        // prefill
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'corporate');
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillAdresses = $prefillAddress['js'];

        $form = self::getComponentCFshareholderCorporateForm('edit', $prefillOfficers, $prefillAddress);
        $this->template->form = $form;
        $this->template->companyType = $this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType();
        $this->template->companyId = $this->company->getCompanyId();
    }

    /**
     * @param FForm $form
     */
    public function Form_CFeditShareholderCorporateFormSubmitted(FForm $form)
    {
        $data = $form->getValues();

        $person = $this->shareholder->getCorporate();
        $person->setFields($data);
        $this->shareholder->setCorporate($person);

        // address
        $address = $this->shareholder->getAddress();
        $address->setFields($data);
        $this->shareholder->setAddress($address);

        // authentication
        $authetication = $this->shareholder->getAuthentication();
        $authetication->setFields($data);
        $this->shareholder->setAuthentication($authetication);

        // shares
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() != 'BYGUAR') {
            $shares = $this->shareholder->getAllotmentShares();
            $shares->setShareClass($data['share_class']);
            $shares->setNumShares($data['num_shares']);
            $shares->setCurrency($data['currency']);
            $shares->setShareValue($data['share_value']);
            $this->shareholder->setAllotmentShares($shares);
        }

        // save
        try {
            $this->shareholder->save();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect();
        }

        $form->clean();
        $this->flashMessage('Shareholder has been updated');
        $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
    }

    /*     * ***************************** CFshowShareholderCorporate ********************************** */

    public function prepareCFshowShareholderCorporate()
    {
        $this->setCompanyUpdateTabs('CFshowShareholderCorporate', 'Show Shareholder Corporate');

        // check get
        if (!isset($this->get['shareholder_id'])) {
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }

        // check existing shareholder
        try {
            $shareholder = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncCorporate($this->get['shareholder_id'], 'sub');
            $this->template->shareholder = $shareholder->getFields();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }
    }

    /*     * ***************************** CFdeleteShareholderCorporate ********************************** */

    protected function handleCFdeleteShareholderCorporate()
    {
        // delete shareholder
        if (isset($this->get['shareholder_id'])) {
            try {
                $director = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncCorporate($this->get['shareholder_id'], 'sub');
                $director->remove();
                $this->flashMessage('Shareholder has been deleted');
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
            }
        }
        $this->redirect('view', "company_id={$this->company->getCompanyId()}");
    }

    /*     * ******************************************* CFaddSecretaryPerson ************************************************** */

    protected function prepareCFaddSecretaryPerson()
    {

        $this->setCompanyUpdateTabs('CFaddSecretaryPerson', 'Add Secretary Person');

        // prefill
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'person');
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];


        $form = $this->getComponentCFsecretaryPersonForm('add', $prefillOfficers, $prefillAddress);
        $this->template->form = $form;
        $this->template->companyId = $this->company->getCompanyId();
    }

    /**
     * @param FForm $form
     */
    public function Form_CFaddSecretaryPersonFormSubmitted(FForm $form)
    {
        $data = $form->getValues();

        // get secretary
        $secretary = $this->company->getLastIncorporationFormSubmission()->getForm()->getNewIncPerson('sec');

        // set person
        $person = $secretary->getPerson();
        $person->setTitle($data['title']);
        $person->setForename($data['forename']);
        $person->setMiddleName($data['middle_name']);
        $person->setSurname($data['surname']);
        $secretary->setPerson($person);

        // set address
        $address = $secretary->getAddress();
        $address->setFields($data);
        $secretary->setAddress($address);

        $secretary->setConsentToAct($data['consentToAct']);

        try {
            $secretary->save();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }

        $form->clean();
        $this->flashMessage('Secretary has been added');
        $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
    }

    /*     * ******************************************* CFeditSecretaryPerson ************************************************** */

    protected function prepareCFeditSecretaryPerson()
    {
        $this->setCompanyUpdateTabs('CFeditSecretaryPerson', 'Edit Secretary Person');

        // check get
        if (!isset($this->get['secretary_id'])) {
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }

        // check existing secretary
        try {
            $this->secretary = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncPerson($this->get['secretary_id'], 'sec');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }
    }

    protected function renderCFeditSecretaryPerson()
    {
        // prefill
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'person');
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillAdresses = $prefillAddress['js'];

        $form = $this->getComponentCFSecretaryPersonForm('edit', $prefillOfficers, $prefillAddress);
        $this->template->form = $form;
        $this->template->companyId = $this->company->getCompanyId();
    }

    /**
     * @param FForm $form
     */
    public function Form_CFeditSecretaryPersonFormSubmitted(FForm $form)
    {
        $data = $form->getValues();

        $person = $this->secretary->getPerson();
        $person->setTitle($data['title']);
        $person->setForename($data['forename']);
        $person->setMiddleName($data['middle_name']);
        $person->setSurname($data['surname']);
        $this->secretary->setPerson($person);

        // address
        $address = $this->secretary->getAddress();
        $address->setFields($data);
        $this->secretary->setAddress($address);

        $this->secretary->setConsentToAct($data['consentToAct']);

        // save
        try {
            $this->secretary->save();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect();
        }

        $form->clean();
        $this->flashMessage('Secretary has been updated');
        $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
    }

    /*     * ******************************************* CFshowSecretaryPerson ************************************************** */

    protected function prepareCFshowSecretaryPerson()
    {
        $this->setCompanyUpdateTabs('CFshowSecretaryPerson', 'Show Secretary Person');

        // check get
        if (!isset($this->get['secretary_id'])) {
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }

        // check existing secretary
        try {
            $secretary = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncPerson($this->get['secretary_id'], 'sec');
            $this->template->secretary = $secretary->getFields();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }
    }

    /*     * ******************************************* CFdeleteSecretaryPerson ************************************************** */

    protected function handleCFdeleteSecretaryPerson()
    {
        // delete secretary
        if (isset($this->get['secretary_id'])) {
            try {
                $secretary = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncPerson($this->get['secretary_id'], 'sec');
                $secretary->remove();
                $this->flashMessage('Secretary has been deleted');
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
            }
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }
    }

    /*     * ******************************************* CFaddSecretaryCorporate ************************************************** */

    protected function prepareCFaddSecretaryCorporate()
    {
        $this->setCompanyUpdateTabs('CFaddSecretaryCorporate', 'Add Secretary Corporate');

        // prefill
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'corporate');
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];

        $form = $this->getComponentCFsecretaryCorporateForm('add', $prefillOfficers, $prefillAddress);
        $this->template->form = $form;
        $this->template->companyId = $this->company->getCompanyId();
    }

    /**
     * @param FForm $form
     */
    public function Form_CFaddSecretaryCorporateFormSubmitted(FForm $form)
    {
        $data = $form->getValues();

        // get corporate
        $secretaryCorporate = $this->company->getLastIncorporationFormSubmission()->getForm()->getNewIncCorporate('sec');
        $corporate = $secretaryCorporate->getCorporate();
        $corporate->setFields($data);
        $secretaryCorporate->setCorporate($corporate);

        // address
        $address = $secretaryCorporate->getAddress();
        $address->setFields($data);
        $secretaryCorporate->setAddress($address);

        $secretaryCorporate->setConsentToAct($data['consentToAct']);

        // identification
        if ($data['eeaType'] == 1) {
            $identification = $secretaryCorporate->getIdentification(Identification::EEA);
        } else {
            $identification = $secretaryCorporate->getIdentification(Identification::NonEEA);
        }

        $identification->setFields($data);
        $secretaryCorporate->setIdentification($identification);
        $secretaryCorporate->save();

        $form->clean();
        $this->flashMessage("Corporate secretary has been added");
        $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
    }

    /*     * ******************************************* CFeditSecretaryCorporate ************************************************** */

    protected function prepareCFeditSecretaryCorporate()
    {
        $this->setCompanyUpdateTabs('CFeditSecretaryCorporate', 'Edit Secretary Corporate');

        // check get
        if (!isset($this->get['secretary_id'])) {
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }

        // check existing secretary
        try {
            $this->secretary = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncCorporate($this->get['secretary_id'], 'sec');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }
    }

    protected function renderCFeditSecretaryCorporate()
    {
        // prefill
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'corporate');
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];

        $form = $this->getComponentCFSecretaryCorporateForm('edit', $prefillOfficers, $prefillAddress);
        $this->template->form = $form;
        $this->template->companyId = $this->company->getCompanyId();
    }

    /**
     * @param FForm $form
     */
    public function Form_CFeditSecretaryCorporateFormSubmitted(FForm $form)
    {
        $data = $form->getValues();

        $person = $this->secretary->getCorporate();
        $person->setFields($data);
        $this->secretary->setCorporate($person);

        // address
        $address = $this->secretary->getAddress();
        $address->setFields($data);
        $this->secretary->setAddress($address);

        $this->secretary->setConsentToAct($data['consentToAct']);

        // identification
        if ($data['eeaType'] == 1) {
            $identification = $this->secretary->getIdentification(Identification::EEA);
            $identification->setPlaceRegistered($data['place_registered']);
            $identification->setRegistrationNumber($data['registration_number']);
            $this->secretary->setIdentification($identification);
        } else {
            $identification = $this->secretary->getIdentification(Identification::NonEEA);
            $identification->setPlaceRegistered($data['place_registered']);
            $identification->setRegistrationNumber($data['registration_number']);
            $identification->setLawGoverned($data['law_governed']);
            $identification->setLegalForm($data['legal_form']);
        }
        $this->secretary->setIdentification($identification);

        // save
        try {
            $this->secretary->save();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect();
        }

        $form->clean();
        $this->flashMessage('Secretary has been updated');
        $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
    }

    /*     * ******************************************* CFshowSecretaryCorporate ************************************************** */

    protected function prepareCFshowSecretaryCorporate()
    {
        $this->setCompanyUpdateTabs('CFshowSecretaryCorporate', 'Show Secretary Corporate');

        // check get
        if (!isset($this->get['secretary_id'])) {
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }

        // check existing secretary
        try {
            $secretary = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncCorporate($this->get['secretary_id'], 'sec');
            $this->template->secretary = $secretary->getFields();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }
    }

    /*     * ******************************************* CFdeleteSecretaryCorporate ************************************************** */

    protected function handleCFdeleteSecretaryCorporate()
    {
        // delete secretary
        if (isset($this->get['secretary_id'])) {
            try {
                $secretary = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncPerson($this->get['secretary_id'], 'sec');
                $secretary->remove();
                $this->flashMessage('Secretary has been deleted');
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
            }
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }
    }

    /*     * ******************************************* CFupdateDocuments ************************************************** */

    protected function prepareCFupdateDocuments()
    {
        $this->setCompanyUpdateTabs('CFupdateDocuments', 'Update documents');

        $form = new FForm('articles');
        $form->addFieldset('Articles');
        $form->addRadio('type', 'Type: ', array(1 => 'Use Standard Articles', 2 => 'Upload Custom Articles'))
            ->setValue(1);

        // just custom article for Section 30
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getType() == CompanyIncorporation::BY_GUARANTEE_EXEMPT) {
            $form['type']->setDisabledOptions(1)->setValue(2);
        }

        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getType() != CompanyIncorporation::LIMITED_LIABILITY_PARTHENERSHIP) {
            // article
            $form->addFieldset('Custom');
            $article = $this->company->getLastIncorporationFormSubmission()->getMemarts();
            if ($article !== NULL && $article['bespoke'] == 1) {
                $form->addText('articleName', 'Article:')
                    ->setValue($article['filename'])
                    ->setDescription($article['id']);
                $form->addSubmit('removeArticle', 'Remove')->class('btn');
                $form['type']->setDisabledOptions(1)->setValue(2);

                if ($article['bespoke']) {
                    $form['type']->setDisabledOptions(1)->setValue(2);
                } else {
                    $form['type']->setDisabledOptions(2)->setValue(1);
                }

                $this->template->article = $article;
            } else {
                $form->addFile('custom', 'Custom Article')
                    ->addRule(array($this, 'Validator_fileLenghtAfterEncoding'), 'PDF is too large. Please try to decrease the number of pages in the file by reducing the margins and font size.')
                    ->addRule(array($this, 'Validator_fileSize'), 'The total files size (including M&A, supporting doc and company info) can be no more than 5.0 mbytes')
                    ->addRule(FForm::MIME_TYPE, 'Only pdf is supported for Article', MimeType::PDF)
                    ->addRule(array($this, 'Validator_filePdfVersion'), 'PDF attachments are limited to PDF version 1.2 through 1.7 inclusive')
                    ->addRule(array($this, 'Validator_customArticle'), 'Please provide Custom Article!')
                    ->setDescription('(Must be in PDF)');
            }
        }


        // support
        $form->addFieldset('Supporting documents');
        $support = $this->company->getLastIncorporationFormSubmission()->getSuppnameauth();
        if ($support !== NULL) {
            $form->addText('supportName', 'Support:')
                ->setValue($support['filename'])
                ->setDescription($support['id']);
            $form->addSubmit('removeSupport', 'Remove')->class('btn');
            $this->template->support = $support;
        } else {
            $form->addFile('support', 'Support document ')
                ->addRule(array($this, 'Validator_fileLenghtAfterEncoding'), 'PDF is too large. Please try to decrease the number of pages in the file by reducing the margins and font size.')
                ->addRule(FForm::MIME_TYPE, 'Only pdf is supported for Support document', MimeType::PDF)
                ->addRule(array($this, 'Validator_filePdfVersion'), 'PDF attachments are limited to PDF version 1.2 through 1.7 inclusive')
                ->setDescription('(Must be in PDF)');
        }

        $form->addFieldset('Action');
        $form->addSubmit('save', 'Save')->class('btn');
        $form->onValid = array($this, 'Form_CFupdateDocumentsFormSubmitted');

        // remove documents
        if ($form->isSubmited()) {
            if ($form->isSubmitedBy('removeArticle')) {
                if (isset($article['id'])) {
                    $this->company->getLastIncorporationFormSubmission()->getDocument($article['id'])->remove();
                    $this->flashMessage('Articles has been updated');
                    $this->redirect();
                }
            }
            // remove support
            if ($form->isSubmitedBy('removeSupport')) {
                if (isset($support['id'])) {
                    $this->company->getLastIncorporationFormSubmission()->getDocument($support['id'])->remove();
                    $this->flashMessage('Articles has been updated');
                    $this->redirect();
                }
            }
        }
        $form->start();
        $this->template->form = $form;
        $this->template->type = $this->company->getLastIncorporationFormSubmission()->getForm()->getType();
    }

    /*     * ******************************************* lockCompany ************************************************** */

    public function handleLockCompany()
    {
        try {
            $this->company->lockCompany();
            $this->flashMessage('Company has been locked');
            $this->redirect('view', "company_id={$this->company->getCompanyId()}");
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('view', "company_id={$this->company->getCompanyId()}");
        }
    }

    /*     * ******************************************* unlockCompany ************************************************** */

    public function handleUnlockCompany()
    {
        try {
            $this->company->unlockCompany();
            $this->flashMessage('Company has been unlocked');
            $this->redirect('view', "company_id={$this->company->getCompanyId()}");
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('view', "company_id={$this->company->getCompanyId()}");
        }
    }

    /**
     * @param FForm $form
     * @throws Exception
     */
    public function Form_CFupdateDocumentsFormSubmitted(FForm $form)
    {
        $data = $form->getValues();
        $lastFormSubmission = $this->company->getLastIncorporationFormSubmission();

        // remove article
        if ($form->isSubmitedBy('removeArticle')) {
            $article = $this->company->getLastIncorporationFormSubmission()->getMemarts();
            if (isset($article['id'])) {
                $lastFormSubmission->getDocument($article['id'])->remove();
            }
        }

        // remove support
        if ($form->isSubmitedBy('removeSupport')) {
            $support = $this->company->getLastIncorporationFormSubmission()->getSuppnameauth();
            if (isset($support['id'])) {
                $lastFormSubmission->getDocument($support['id'])->remove();
            }
        }

        // standard
        if ($data['type'] == 1) {
            $type = $lastFormSubmission->getForm()->getType();
            $file = PROJECT_DIR . '/upload/files/' . $type . '/articles.pdf';
            $lastFormSubmission->uploadDocument('MEMARTS', $file);
        } elseif (isset($data['custom'])) {
            $article = $this->company->getLastIncorporationFormSubmission()->getMemarts();
            if ($article === NULL || $article !== NULL && $article['bespoke'] == 0) {
                $lastFormSubmission->uploadDocument('MEMARTS', $data['custom'], TRUE);
            }
        }

        // supported documents
        if (isset($data['support']) && $data['support'] != NULL) {
            $lastFormSubmission->uploadDocument('SUPPNAMEAUTH', $data['support']);
        }

        // check custom article for section 30
        $type = $this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType();
        if ($type == CompanyIncorporation::BY_GUARANTEE_EXEMPT) {
            try {
                $this->check('BESPOKE_ARTICLE');
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
                $this->redirect();
            }
        }

        $form->clean();
        $this->flashMessage('Articles has been updated');
        $this->redirect(NULL, "company_id={$this->company->getCompanyId()}");
    }

    /*     * ***************************** other ********************************** */

    /**
     * @param FControl $control
     * @param string $error
     * @return bool
     */
    public function Validator_customArticle(FControl $control, $error)
    {
        $type = $control->owner['type']->getValue();
        if ($control->getValue() == NULL && $type == 2) {
            return $error;
        }
        return TRUE;
    }

    /**
     * The total size of the message (including M&A, supporting doc and company info) can be no more than 5.0 mbytes.
     * @param FControl $control
     * @param string $error
     * @return bool
     */
    public function Validator_fileSize(FControl $control, $error)
    {
        $from = $control->owner->getElements();
        $supportFile = array_key_exists('support', $from);
        $customFile = array_key_exists('custom', $from);
        if (isset($supportFile) || isset($customFile)) {
            if ($supportFile != NULL && $customFile != NULL) {
                $supportFile = $from['support'];
                $customFile = $from['custom'];
                $customValue = $customFile->getValue();
                $supportValue = $supportFile->getValue();
                $size = $supportValue['size'] + $customValue['size'];
            }
            if ($supportFile == NULL) {
                $customFile = $from['custom'];
                $customValue = $customFile->getValue();
                $size = $customValue['size'];
            }
            if ($customFile == NULL) {
                $supportFile = $from['support'];
                $supportValue = $supportFile->getValue();
                $size = $supportValue['size'];
            }
            if ($size > '5242880') {
                return $error;
            }
        }
        return TRUE;
    }

    /**
     * PDF Maximum file size is 1.2 million characters once Base64 encoded.
     * @param FControl $control
     * @param string $error
     * @return bool
     */
    public function Validator_fileLenghtAfterEncoding(FControl $control, $error)
    {
        $value = $control->getValue();
        if ($value) {
            if (strlen(base64_encode(file_get_contents($value['tmp_name']))) > 1200000) {
                return $error;
            }
        }
        return TRUE;
    }

    /**
     * Supports: Acrobat 3.0 (PDF 1.2) > Acrobat 8.0 (PDF 1.7)
     * @param FControl $control
     * @param string $error
     * @return bool
     */
    public function Validator_filePdfVersion(FControl $control, $error)
    {
        $value = $control->getValue();
        if (!empty($value['tmp_name'])) {
            preg_match('/\d\.\d/', file_get_contents($value['tmp_name'], 20), $match);
            if (isset($match[0])) {
                if ($match[0] > 1.7 || $match[0] < 1.2) {
                    return $error;
                }
            }
        }
        return TRUE;
    }

    /**
     * Return form for adding/editing corporate psc
     *
     * @param string $action
     * @param array $prefillOfficers
     * @param array $prefillAddress
     * @return FForm
     */
    public function getComponentCFPscPersonForm($action, $prefillOfficers, $prefillAddress)
    {
        $companyType = $this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType();

        if ($action == 'add') {
            $form = new CHForm('addPscPersonAdmin');
        } else {
            $form = new CHForm('editPscPersonAdmin');
        }

        // prefill
        $form->addFieldset('Autocomplete details (optional)');
        $form->addSelect('prefillOfficers', 'Select an existing appointment to autocomplete these details', $prefillOfficers['select'])
            ->setFirstOption('--- Select --')
            ->setDescription('Or enter the details below if adding a new appointment.');

        // person
        $form->addFieldset('Person');
        $form->addSelect('title', 'Title', Person::$titles)
            ->setFirstOption('--- Select ---');
        $form->addText('forename', 'First name *')
            ->addRule(FForm::Required, 'Please provide first name')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "First name can't be more than 50 characters", 50);
        $form->addText('middle_name', 'Middle name')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "Middle name can't be more than 50 characters", 50)
            ->addRule('MinLength', "Please provide full middle name.", 2);
        $form->addText('surname', 'Last name *')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule(FForm::Required, 'Please provide last name')
            ->addRule('MaxLength', "Last name can't be more than 160 characters", 160);
        $form->add('DateSelect', 'dob', 'Date Of Birth *')
            ->addRule(FForm::Required, 'Please provide dob')
            ->addRule(
                [$this, 'Validator_personDOB'],
                ['DOB is not a valid date.', 'PSC has to be older than %d years.'],
                16
            )
            ->withFirstOptions()
            ->setStartYear(1900)
            ->setEndYear(date('Y') - 1);

        $form->addText('nationality', 'Nationality *')
            ->addRule(FForm::Required, 'Please provide nationality')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL);
        $form->addText('country_of_residence', 'Country Of Residence *')
            ->addRule(FForm::Required, 'Please provide Country of Residence')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL);

        $form->addFieldset('Prefill');
        $form->addSelect('prefillAddress', 'Prefill Address', $prefillAddress['select'])
            ->setFirstOption('--- Select ---');

        $form->addFieldset('Address');
        $form->addText('premise', 'Building name/number *')
            ->addRule(FForm::Required, 'Please provide Building name/number')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "Building name/number can't be more than 50 characters", 50);
        $form->addText('street', 'Street *')
            ->addRule(FForm::Required, 'Please provide Street')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "Street can't be more than 50 characters", 50);
        $form->addText('thoroughfare', 'Address 3')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "Address 3 can't be more than 50 characters", 50);
        $form->addText('post_town', 'Town *')
            ->addRule(FForm::Required, 'Please provide Town')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "Town can't be more than 50 characters", 50);
        $form->addText('county', 'County')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "County can't be more than 50 characters", 50);
        $form->addText('postcode', 'Postcode *')
            ->addRule(FForm::Required, 'Please provide Postcode')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "Postcode can't be more than 15 characters", 15);
        $form->addSelect('country', 'Country *', Address::$countries)
            ->addRule(FForm::Required, 'Please provide Country');

        // residential address
        $form->addFieldset('Residential Address');

        if ($action == 'add') {
            $form->addCheckbox('residentialAddress', 'Different address: ', 1);
        }

        $form->addText('residential_premise', 'Building name/number *')
            ->addRule(['CHValidator', 'Validator_requiredServiceAddress'], 'Please provide Building name/number', $action)
            ->addRule('MaxLength', "Building name/number can't be more than 50 characters", 50)
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL);
        $form->addText('residential_street', 'Street *')
            ->addRule(['CHValidator', 'Validator_requiredServiceAddress'], 'Please provide street', $action)
            ->addRule('MaxLength', "Street can't be more than 50 characters", 50)
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL);
        $form->addText('residential_thoroughfare', 'Address 3')
            ->addRule('MaxLength', "Address 3 can't be more than 50 characters", 50)
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL);
        $form->addText('residential_post_town', 'Town *')
            ->addRule(['CHValidator', 'Validator_requiredServiceAddress'], 'Please provide Town', $action)
            ->addRule('MaxLength', "Town can't be more than 50 characters", 50)
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL);
        $form->addText('residential_county', 'County')
            ->addRule('MaxLength', "County can't be more than 50 characters", 50)
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL);
        $form->addText('residential_postcode', 'Postcode *')
            ->addRule(['CHValidator', 'Validator_requiredServiceAddress'], 'Please provide Postcode', $action)
            ->addRule('MaxLength', "Postcode can't be more than 15 characters", 15)
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL);
        $form->addSelect('residential_country', 'Country *', Address::$countries)
            ->addRule(['CHValidator', 'Validator_requiredServiceAddress'], 'Please provide Country', $action)
            ->addRule('MaxLength', "Country can't be more than 50 characters", 50)
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL);

        $form->addFieldset('Nature of Control:');
        if ($companyType != CompanyIncorporation::BY_GUARANTEE) {
            $form->addRadio(
                'ownership_of_shares',
                'The person holds shares',
                $this->pscChoicesProvider->getAllOwnershipOfShares($companyType)
            );
        }

        $form->addRadio(
            'ownership_of_voting_rights',
            'The person holds voting rights',
            $this->pscChoicesProvider->getAllOwnershipOfVotingRights()
        );

        $form->addRadio(
            'right_to_appoint_and_remove_directors',
            'Right to appoint or remove the majority of the board of directors',
            $this->pscChoicesProvider->getRightToAppointAndRemoveDirectors(PscChoicesProvider::PERSON, $companyType)
        );

        $form->addRadio(
            'significant_influence_or_control',
            'Has significant influence or control',
            $this->pscChoicesProvider->getSignificantInfluenceOrControl(PscChoicesProvider::PERSON)
        )
            ->addRule(
                [$this, 'Validator_natureOfControl'],
                'Please select the nature of control. At least one condition must be selected.',
                $companyType
            );


        $form->addFieldset('Action');
        if ($action == 'add') {
            $form->addSubmit('continue', 'Continue >')->class('btn_submit fright mbottom');
            $form->onValid = array($this, 'Form_CFaddPscPersonFormSubmitted');
        } else {
            $form->addSubmit('continue', 'Save')->class('btn_submit fright mbottom');
            $form->onValid = array($this, 'Form_CFeditPscPersonFormSubmitted');
            $form->setInitValues($this->psc->getFields());
        }

        $form->start();
        return $form;
    }

    /**
     * Return form for adding/editing corporate psc
     *
     * @param string $action
     * @param array $prefillOfficers
     * @param array $prefillAddress
     * @return FForm
     */
    public function getComponentCFPscCorporateForm($action, $prefillOfficers, $prefillAddress)
    {
        $companyType = $this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType();

        if ($action == 'add') {
            $form = new FForm('addPscCorporateAdmin');
        } else {
            $form = new FForm('editPscCorporateAdmin');
        }

        $form->addFieldset('Autocomplete details (optional)');
        $form->addSelect('prefillOfficers', 'Select an existing appointment to autocomplete these details', $prefillOfficers['select'])
            ->setFirstOption('--- Select --')
            ->setDescription('Or enter the details below if adding a new appointment.');

        // person
        $form->addFieldset('Corporate');
        $form->addText('corporate_name', 'Company name *')
            ->addRule(FForm::Required, 'Please provide company name')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL);
        $form->addText('place_registered', 'Place registered');
        $form->addText('registration_number', 'Registration number');
        $form->addText('law_governed', 'Governing law *')
            ->addRule(FForm::Required, 'Please provide Governing law');
        $form->addText('legal_form', 'Legal Form *')
            ->addRule(FForm::Required, 'Please provide Legal Form');
        $form->addText('country_or_state', 'Country registered');

        // address
        $form->addFieldset('Prefill');
        $form->addSelect('prefillAddress', 'Prefill Address', $prefillAddress['select'])
            ->setFirstOption('--- Select ---');

        $form->addFieldset('Address');
        $form->addText('premise', 'Building name/number *')
            ->addRule(FForm::Required, 'Please provide Building name/number')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "Building name/number can't be more than 50 characters", 50);
        $form->addText('street', 'Street *')
            ->addRule(FForm::Required, 'Please provide Street')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "Street can't be more than 50 characters", 50);
        $form->addText('thoroughfare', 'Address 3')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "Address 3 can't be more than 50 characters", 50);
        $form->addText('post_town', 'Town *')
            ->addRule(FForm::Required, 'Please provide Town')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "Town can't be more than 50 characters", 50);
        $form->addText('county', 'County')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "County can't be more than 50 characters", 50);
        $form->addText('postcode', 'Postcode *')
            ->addRule(FForm::Required, 'Please provide Postcode')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "Postcode can't be more than 15 characters", 15);
        $form->addSelect('country', 'Country *', Address::$countries)
            ->addRule(FForm::Required, 'Please provide Country');

        $form->addFieldset('Nature of Control:');
        if ($companyType != CompanyIncorporation::BY_GUARANTEE) {
            $form->addRadio(
                'ownership_of_shares',
                'The person holds shares',
                $this->pscChoicesProvider->getAllOwnershipOfShares($companyType)
            );
        }

        $form->addRadio(
            'ownership_of_voting_rights',
            'The person holds voting rights',
            $this->pscChoicesProvider->getOwnershipOfVotingRights() + $this->pscChoicesProvider->getOwnershipOfVotingRightsAsFirm() + $this->pscChoicesProvider->getOwnershipOfVotingRightsAsTrust()
        );

        $form->addRadio(
            'right_to_appoint_and_remove_directors',
            'Right to appoint or remove the majority of the board of directors',
            $this->pscChoicesProvider->getRightToAppointAndRemoveDirectors(PscChoicesProvider::CORPORATE, $companyType)
        );

        $form->addRadio(
            'significant_influence_or_control',
            'Has significant influence or control',
            $this->pscChoicesProvider->getSignificantInfluenceOrControl(PscChoicesProvider::CORPORATE)
        )
            ->addRule(
                [$this, 'Validator_natureOfControl'],
                'Please select the nature of control. At least one condition must be selected.',
                $companyType
            );


        $form->addFieldset('Action');

        if ($action == 'add') {
            $form->addSubmit('continue', 'Save')->class('btn');
            $form->onValid = array($this, 'Form_CFaddPscCorporateFormSubmitted');
        } else {
            $form->onValid = array($this, 'Form_CFeditPscCorporateFormSubmitted');
            $form->addSubmit('continue', 'Save')->class('btn');
            $form->setInitValues($this->psc->getFields());
        }

        $form->start();
        return $form;
    }

    /**
     * Return form for adding/editin person director
     *
     * @param string $action
     * @param array $prefillOfficers
     * @param array $prefillAddress
     * @return FForm
     */
    public function getComponentCFDirectorPersonForm($action, $prefillOfficers, $prefillAddress)
    {
        // form
        if ($action == 'add') {
            $form = new CHForm('CFaddDirectorPerson222');
        } else {
            $form = new CHForm('CFeditDirectorPerson222');
        }

        // prefill
        $form->addFieldset('Autocomplete details (optional)');
        $form->addSelect('prefillOfficers', 'Select an existing appointment to autocomplete these details', $prefillOfficers['select'])
            ->setFirstOption('--- Select --')
            ->setDescription('Or enter the details below if adding a new appointment.');

        // person
        $form->addFieldset('Person');
        $form->addSelect('title', 'Title', Person::$titles)
            ->setFirstOption('--- Select ---');
        $form->addText('forename', 'First name *')
            ->addRule(FForm::Required, 'Please provide first name')
            ->addRule('MaxLength', "First name can't be more than 50 characters", 50);
        $form->addText('middle_name', 'Middle name')
            ->addRule('MaxLength', "Middle name can't be more than 50 characters", 50);
        $form->addText('surname', 'Last name *')
            ->addRule(FForm::Required, 'Please provide last name')
            ->addRule('MaxLength', "Last name can't be more than 160 characters", 160);
        $form->add('DateSelect', 'dob', 'Date Of Birth *')
            ->addRule(FForm::Required, 'Please provide dob')
            ->addRule(
                [$this, 'Validator_personDOB'],
                ['DOB is not a valid date.', 'Director has to be older than %d years.'],
                16
            )
            ->setStartYear(1900)
            ->setEndYear(date('Y') - 1)
            ->setValue(date('Y-m-d', strtotime(date("Y-m-d", strtotime(date("Y-m-d"))) . " -15 year")));

        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() != 'LLP') {
            $form->addText('nationality', 'Nationality *')
                ->addRule(FForm::Required, 'Please provide nationality')
            //->addRule(FForm::REGEXP, "Only letters are allowed", '#^[A-Z]+$#i')
            ;
            $form->addText('occupation', 'Occupation *')
                ->addRule(FForm::Required, 'Please provide occupation');
        }
        $form->addText('country_of_residence', 'Country Of Residence *')
            ->addRule(FForm::Required, 'Please provide Country of Residence')
        //->addRule(FForm::REGEXP, "Only letters are allowed", '#^[A-Z]+$#i')
        ;

        // prefill
        $form->addFieldset('Prefill');
        $form->addSelect('prefillAddress', 'Prefill Address', $prefillAddress['select'])
            ->setFirstOption('--- Select ---');

        // adddress
        $form->addFieldset('Address');
        $form->addText('premise', 'Premise *')
            ->addRule(FForm::Required, 'Please provide Premise')
            ->addRule('MaxLength', "Premise can't be more than 50 characters", 50);
        $form->addText('street', 'Street *')
            ->addRule(FForm::Required, 'Please provide Street')
            ->addRule('MaxLength', "Street can't be more than 50 characters", 50);
        $form->addText('thoroughfare', 'Address 3')
            ->addRule('MaxLength', "Address 3 can't be more than 50 characters", 50);
        $form->addText('post_town', 'Town *')
            ->addRule(FForm::Required, 'Please provide Town')
            ->addRule('MaxLength', "Town can't be more than 50 characters", 50);
        $form->addText('county', 'County')
            ->addRule('MaxLength', "County can't be more than 50 characters", 50);
        $form->addText('postcode', 'Postcode *')
            ->addRule(FForm::Required, 'Please provide Postcode')
            ->addRule('MaxLength', "Postcode can't be more than 15 characters", 15);
        $form->addSelect('country', 'Country *', Address::$countries)
            ->addRule(FForm::Required, 'Please provide Country');

        // residential address
        $form->addFieldset('Residential Address');

        if ($action == 'add') {
            $form->addCheckbox('residentialAddress', 'Different address: ', 1);
        }

        $form->addText('residential_premise', 'Premise *')
            ->addRule(array('CHValidator', 'Validator_requiredServiceAddress'), 'Please provide premise', $action)
            ->addRule('MaxLength', "Premise can't be more than 50 characters", 50);
        $form->addText('residential_street', 'Street *')
            ->addRule(array('CHValidator', 'Validator_requiredServiceAddress'), 'Please provide address 2', $action)
            ->addRule('MaxLength', "Street can't be more than 50 characters", 50);
        $form->addText('residential_thoroughfare', 'Address 3')
            ->addRule('MaxLength', "Address 3 can't be more than 50 characters", 50);
        $form->addText('residential_post_town', 'Town *')
            ->addRule(array('CHValidator', 'Validator_requiredServiceAddress'), 'Please provide Town', $action)
            ->addRule('MaxLength', "Town can't be more than 50 characters", 50);
        $form->addText('residential_county', 'County')
            ->addRule('MaxLength', "County can't be more than 50 characters", 50);
        $form->addText('residential_postcode', 'Postcode *')
            ->addRule(array('CHValidator', 'Validator_requiredServiceAddress'), 'Please provide Postcode', $action)
            ->addRule('MaxLength', "Postcode can't be more than 15 characters", 15);
        $form->addSelect('residential_country', 'Country *', Address::$countries)
            ->addRule(array('CHValidator', 'Validator_requiredServiceAddress'), 'Please provide Country', $action)
            ->addRule('MaxLength', "Country can't be more than 50 characters", 50);
        $form->addCheckbox('residentialSecureAddressInd', '243 Exemption: ', 1);

        // security
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() == 'LLP') {
            $form->addFieldset('Security');
            $form->addText('BIRTOWN', 'First three letters of Town of birth *')
                ->size(3)
                ->addRule(FForm::Required, 'Please provide Town of birth')
                ->addRule(FForm::REGEXP, 'Please enter only 3 letters for Town of birth', '#^[A-Z]{3,3}$#i');
            $form->addText('TEL', 'Last three digits of Telephone number *')
                ->size(3)
                ->addRule(FForm::Required, 'Please provide Telephone number')
                ->addRule(FForm::REGEXP, 'Please enter only 3 numeric characters for Telephone number', '#^\d{3,3}$#');
            $form->addText('EYE', 'First three letters of Eye colour *')
                ->size(3)
                ->addRule(FForm::Required, 'Please provide Eye colour')
                ->addRule(FForm::REGEXP, 'Please enter only 3 letters for Eye colour', '#^[A-Z]{3,3}$#i')
                ->addRule([$this, 'Validator_securityEyeColour'], 'Eye colour can not be black');
        }

        $form->addFieldset('Consent to act');
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() == 'LLP') {
            $consentLabel = 'The members confirm that the person named has consented to act as a designated member';
        } else {
            $consentLabel = 'The subscribers (shareholders) confirm that the person named has consented to act as a director';
        }
        $form->add('SingleCheckbox', 'consentToAct', $consentLabel)
            ->addRule(FForm::Required, 'Consent to act is required');

        $form->addFieldset('Action');

        // diff callback function
        if ($action == 'add') {
            $form->addSubmit('continue', 'Save')->class('btn');
            $form->onValid = array($this, 'Form_CFaddDirectorPersonFormSubmitted');
        } else {
            $form->onValid = array($this, 'Form_CFeditDirectorPersonFormSubmitted');
            $form->addSubmit('continue', 'Save')->class('btn');

            $fields = $this->director->getFields();
            $fields['residentialSecureAddressInd'] = $fields['residential_secure_address_ind'];
            unset($fields['consentToAct']);
            $form->setInitValues($fields);
        }

        $form->start();
        return $form;
    }

    /**
     * Return form for adding/editing corporate director
     *
     * @param string $action
     * @param array $prefillOfficers
     * @param array $prefillAddress
     * @return FForm
     */
    public function getComponentCFDirectorCorporateForm($action, $prefillOfficers, $prefillAddress)
    {
        // form
        if ($action == 'add') {
            $form = new CHForm('CFaddDirectorCorporate');
        } else {
            $form = new CHForm('CFeditDirectorCorporate');
        }

        // prefill
        $form->addFieldset('Autocomplete details (optional)');
        $form->addSelect('prefillOfficers', 'Select an existing appointment to autocomplete these details', $prefillOfficers['select'])
            ->setFirstOption('--- Select --')
            ->setDescription('Or enter the details below if adding a new appointment.');

        // person
        $form->addFieldset('Corporate');
        $form->addText('corporate_name', 'Company name *')
            ->addRule(FForm::Required, 'Please provide first name');
        $form->addText('forename', 'First name *')
            ->addRule(FForm::Required, 'Please provide first name')
            ->addRule('MaxLength', "First name can't be more than 50 characters", 50);
        $form->addText('surname', 'Last name *')
            ->addRule(FForm::Required, 'Please provide last name')
            ->addRule('MaxLength', "Last name can't be more than 160 characters", 160);

        // address
        $form->addFieldset('Prefill');
        $form->addSelect('prefillAddress', 'Prefill Address', $prefillAddress['select'])
            ->setFirstOption('--- Select ---');

        $form->addFieldset('Address');
        $form->addFieldset('Address');
        $form->addText('premise', 'Premise *')
            ->addRule(FForm::Required, 'Please provide Premise')
            ->addRule('MaxLength', "Premise can't be more than 50 characters", 50);
        $form->addText('street', 'Street *')
            ->addRule(FForm::Required, 'Please provide Street')
            ->addRule('MaxLength', "Street can't be more than 50 characters", 50);
        $form->addText('thoroughfare', 'Address 3')
            ->addRule('MaxLength', "Address 3 can't be more than 50 characters", 50);
        $form->addText('post_town', 'Town *')
            ->addRule(FForm::Required, 'Please provide Town')
            ->addRule('MaxLength', "Town can't be more than 50 characters", 50);
        $form->addText('county', 'County')
            ->addRule('MaxLength', "County can't be more than 50 characters", 50);
        $form->addText('postcode', 'Postcode *')
            ->addRule(FForm::Required, 'Please provide Postcode')
            ->addRule('MaxLength', "Postcode can't be more than 15 characters", 15);
        $form->addSelect('country', 'Country *', Address::$countries)
            ->addRule(FForm::Required, 'Please provide Country');

        $form->addFieldset('EEA/ Non EEA');
        $form->addRadio('eeaType', 'Type *', array(1 => 'EEA', 2 => 'Non EEA'))
            ->addRule(FForm::Required, 'Please provide EEA type!');
        $form->addText('place_registered', 'Country Registered *')
            ->addRule(array($this, 'Validator_eeaRequired'), 'Please provide Country registered!');
        $form->addText('registration_number', 'Registration number *')
            ->addRule(array($this, 'Validator_eeaRequired'), 'Please provide Registration number!');
        $form->addText('law_governed', 'Governing law *')
            ->addRule(array($this, 'Validator_eeaRequired'), 'Please provide Governing law!');
        $form->addText('legal_form', 'Legal Form *')
            ->addRule(array($this, 'Validator_eeaRequired'), 'Please provide Legal form!');

        // security
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() == 'LLP') {
            $form->addFieldset('Security');
            $form->addText('BIRTOWN', 'First three letters of Town of birth *')
                ->size(3)
                ->addRule(FForm::Required, 'Please provide Town of birth')
                ->addRule(FForm::REGEXP, 'Please enter only 3 letters for Town of birth', '#^[A-Z]{3,3}$#i');
            $form->addText('TEL', 'Last three digits of Telephone number *')
                ->size(3)
                ->addRule(FForm::Required, 'Please provide Telephone number')
                ->addRule(FForm::REGEXP, 'Please enter only 3 numeric characters for Telephone number', '#^\d{3,3}$#');
            $form->addText('EYE', 'First three letters of Eye colour *')
                ->size(3)
                ->addRule(FForm::Required, 'Please provide Eye colour')
                ->addRule(FForm::REGEXP, 'Please enter only 3 letters for Eye colour', '#^[A-Z]{3,3}$#i')
                ->addRule([$this, 'Validator_securityEyeColour'], 'Eye colour can not be black');
        }

        $form->addFieldset('Consent to act');
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() == 'LLP') {
            $consentLabel = 'The members confirm that the corporate body named has consented to act as a designated member';
        } else {
            $consentLabel = 'The subscribers (shareholders) confirm that the corporate body named has consented to act as a director';
        }
        $form->add('SingleCheckbox', 'consentToAct', $consentLabel)
            ->addRule(FForm::Required, 'Consent to act is required');

        $form->addFieldset('Action');

        if ($action == 'add') {
            $form->addSubmit('continue', 'Save')->class('btn');
            $form->onValid = array($this, 'Form_CFaddDirectorCorporateFormSubmitted');
        } else {
            $form->addSubmit('continue', 'Save')->class('btn');
            $form->onValid = array($this, 'Form_CFeditDirectorCorporateFormSubmitted');
            $fields = $this->director->getFields();
            unset($fields['consentToAct']);
            $form->setInitValues($fields);
        }

        $form->start();
        return $form;
    }

    /**
     * Return form for adding/editing corporate director
     *
     * @param string $action
     * @param array $prefillOfficers
     * @param array $prefillAddress
     * @return FForm
     */
    public function getComponentCFshaholderPersonForm($action, $prefillOfficers, $prefillAddress)
    {

        if ($action == 'add') {
            $form = new CHForm('CFaddShareholderPerson');
        } else {
            $form = new CHForm('CFeditShareholderPerson');
        }

        $form->addFieldset('Autocomplete details (optional)');
        $form->addSelect('prefillOfficers', 'Select an existing appointment to autocomplete these details', $prefillOfficers['select'])
            ->setFirstOption('--- Select --')
            ->setDescription('Or enter the details below if adding a new appointment.');

        // data
        $form->addFieldset('Data');
        $form->addSelect('title', 'Title', Person::$titles)
            ->setFirstOption('--- Select ---');
        $form->addText('forename', 'First name *')
            ->addRule(FForm::Required, 'Please provide first name')
            ->addRule('MaxLength', "First name can't be more than 50 characters", 50);
        $form->addText('middle_name', 'Middle name')
            ->addRule('MaxLength', "Middle name can't be more than 50 characters", 50);
        $form->addText('surname', 'Last name *')
            ->addRule(FForm::Required, 'Please provide last name')
            ->addRule('MaxLength', "Last name can't be more than 160 characters", 160);

        // prefill
        $form->addFieldset('Prefill');
        $form->addSelect('prefillAddress', 'Prefill Address', $prefillAddress['select'])
            ->setFirstOption('--- Select ---');

        // address
        $form->addFieldset('Address');
        $form->addText('premise', 'Premise *')
            ->addRule(FForm::Required, 'Please provide Premise')
            ->addRule('MaxLength', "Premise can't be more than 50 characters", 50);
        $form->addText('street', 'Street *')
            ->addRule(FForm::Required, 'Please provide Street')
            ->addRule('MaxLength', "Street can't be more than 50 characters", 50);
        $form->addText('thoroughfare', 'Address 3')
            ->addRule('MaxLength', "Address 3 can't be more than 50 characters", 50);
        $form->addText('post_town', 'Town *')
            ->addRule(FForm::Required, 'Please provide Town')
            ->addRule('MaxLength', "Town can't be more than 50 characters", 50);
        $form->addText('county', 'County')
            ->addRule('MaxLength', "County can't be more than 50 characters", 50);
        $form->addText('postcode', 'Postcode *')
            ->addRule(FForm::Required, 'Please provide Postcode')
            ->addRule('MaxLength', "Postcode can't be more than 15 characters", 15);
        $form->addSelect('country', 'Country *', Address::$countries)
            ->addRule(FForm::Required, 'Please provide Country');

        // allotment shares
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() != 'BYGUAR') {
            $form->addFieldset('Allotment of Shares');
            $form->addSelect('currency', 'Share Currency', IncorporationCapital::$currency);
            $form->addText('share_class', 'Share Class')
                ->setValue('ORD')
                ->readonly();
            $form->addText('num_shares', 'Number of shares')
                ->addRule(FForm::Required, 'Please provide number of shares')
                ->addRule(FForm::NUMERIC, 'Number of shares has to be numeric')
                ->addRule(FForm::GREATER_THAN, 'Number of shares has to be greater than #1', 0)
                ->addRule(FForm::REGEXP, 'Number of shares has to be lower than 11 digits', '#^\d{1,11}$#')
                ->setValue(1);
            $form->addText('share_value', 'Value per Share')
                ->addRule(FForm::Required, 'Please provide value per shares')
                ->addRule(FForm::NUMERIC, 'Value per share has to be numeric')
                ->addRule(FForm::GREATER_THAN, 'Value per share has to be greater than #1', 0)
                ->addRule(FForm::REGEXP, 'Amount per share can be int or decimal with maximum 6 fraction digits', '#^\d+(.{1}(\d{1,6})){0,1}$#')
                ->setValue(1);
        }

        // security
        $form->addFieldset('Security');
        $form->addText('BIRTOWN', 'First three letters of Town of birth *')
            ->size(3)
            ->addRule(FForm::Required, 'Please provide Town of birth')
            ->addRule(FForm::REGEXP, 'Please enter only 3 letters for Town of birth', '#^[A-Z]{3,3}$#i');
        $form->addText('TEL', 'Last three digits of Telephone number *')
            ->size(3)
            ->addRule(FForm::Required, 'Please provide Telephone number')
            ->addRule(FForm::REGEXP, 'Please enter only 3 numeric characters for Telephone number', '#^\d{3,3}$#');
        $form->addText('EYE', 'First three letters of Eye colour *')
            ->size(3)
            ->addRule(FForm::Required, 'Please provide Eye colour')
            ->addRule(FForm::REGEXP, 'Please enter only 3 letters for Eye colour', '#^[A-Z]{3,3}$#i')
            ->addRule(array($this, 'Validator_securityEyeColour'), 'Eye colour can not be black');

        $form->addFieldset('Action');

        if ($action == 'add') {
            $form->addSubmit('continue', 'Save')->class('btn');
            $form->onValid = array($this, 'Form_CFaddShareholderPersonFormSubmitted');
        } else {
            $form->addSubmit('continue', 'Save')->class('btn');
            $form->onValid = array($this, 'Form_CFeditShareholderPersonFormSubmitted');
            $form->setInitValues($this->shareholder->getFields());
        }

        $form->start();
        return $form;
    }

    /**
     * Return form for adding/editing corporate shareholder
     *
     * @param string $action
     * @param array $prefillOfficers
     * @param array $prefillAddress
     * @return FForm
     */
    public function getComponentCFshareholderCorporateForm($action, $prefillOfficers, $prefillAddress)
    {
        // form
        if ($action == 'add') {
            $form = new CHForm('CFaddShareholderCorporate');
        } else {
            $form = new CHForm('CFeditShareholderCorporate');
        }

        $form->addFieldset('Autocomplete details (optional)');
        $form->addSelect('prefillOfficers', 'Select an existing appointment to autocomplete these details', $prefillOfficers['select'])
            ->setFirstOption('--- Select --')
            ->setDescription('Or enter the details below if adding a new appointment.');

        // person
        $form->addFieldset('Corporate');
        $form->addText('corporate_name', 'Company name *')
            ->addRule(FForm::Required, 'Please provide first name');
        $form->addText('forename', 'First name *')
            ->addRule(FForm::Required, 'Please provide first name')
            ->addRule('MaxLength', "First name can't be more than 50 characters", 50);
        $form->addText('surname', 'Last name *')
            ->addRule(FForm::Required, 'Please provide last name')
            ->addRule('MaxLength', "Last name can't be more than 160 characters", 160);

        // prefill
        $form->addFieldset('Prefill');
        $form->addSelect('prefillAddress', 'Prefill Address', $prefillAddress['select'])
            ->setFirstOption('--- Select ---');

        // address
        $form->addFieldset('Address');
        $form->addText('premise', 'Premise *')
            ->addRule(FForm::Required, 'Please provide Premise')
            ->addRule('MaxLength', "Premise can't be more than 50 characters", 50);
        $form->addText('street', 'Street *')
            ->addRule(FForm::Required, 'Please provide Street')
            ->addRule('MaxLength', "Street can't be more than 50 characters", 50);
        $form->addText('thoroughfare', 'Address 3')
            ->addRule('MaxLength', "Address 3 can't be more than 50 characters", 50);
        $form->addText('post_town', 'Town *')
            ->addRule(FForm::Required, 'Please provide Town')
            ->addRule('MaxLength', "Town can't be more than 50 characters", 50);
        $form->addText('county', 'County')
            ->addRule('MaxLength', "County can't be more than 50 characters", 50);
        $form->addText('postcode', 'Postcode *')
            ->addRule(FForm::Required, 'Please provide Postcode')
            ->addRule('MaxLength', "Postcode can't be more than 15 characters", 15);
        $form->addSelect('country', 'Country *', Address::$countries)
            ->addRule(FForm::Required, 'Please provide Country');

        // allotment shares
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() != 'BYGUAR') {
            $form->addFieldset('Allotment of Shares');
            $form->addSelect('currency', 'Share Currency', IncorporationCapital::$currency);
            $form->addText('share_class', 'Share Class')
                ->setValue('ORD')
                ->readonly();
            $form->addText('num_shares', 'Number of shares')
                ->addRule(FForm::Required, 'Please provide number of shares')
                ->addRule(FForm::NUMERIC, 'Number of shares has to be numeric')
                ->addRule(FForm::GREATER_THAN, 'Number of shares has to be greater than #1', 0)
                ->addRule(FForm::REGEXP, 'Number of shares has to be lower than 11 digits', '#^\d{1,11}$#')
                ->setValue(1);
            $form->addText('share_value', 'Value per Share')
                ->addRule(FForm::Required, 'Please provide value per shares')
                ->addRule(FForm::NUMERIC, 'Value per share has to be numeric')
                ->addRule(FForm::GREATER_THAN, 'Value per share has to be greater than #1', 0)
                ->addRule(FForm::REGEXP, 'Amount per share can be int or decimal with maximum 6 fraction digits', '#^\d+(.{1}(\d{1,6})){0,1}$#')
                ->setValue(1);
        }

        // security
        $form->addFieldset('Security');
        $form->addText('BIRTOWN', 'First three letters of Town of birth *')
            ->size(3)
            ->addRule(FForm::Required, 'Please provide Town of birth')
            ->addRule(FForm::REGEXP, 'Please enter only 3 letters for Town of birth', '#^[A-Z]{3,3}$#i');
        $form->addText('TEL', 'Last three digits of Telephone number *')
            ->size(3)
            ->addRule(FForm::Required, 'Please provide Telephone number')
            ->addRule(FForm::REGEXP, 'Please enter only 3 numeric characters for Telephone number', '#^\d{3,3}$#');
        $form->addText('EYE', 'First three letters of Eye colour *')
            ->size(3)
            ->addRule(FForm::Required, 'Please provide Eye colour')
            ->addRule(FForm::REGEXP, 'Please enter only 3 letters for Eye colour', '#^[A-Z]{3,3}$#i')
            ->addRule(array($this, 'Validator_securityEyeColour'), 'Eye colour can not be black');

        $form->addFieldset('Action');

        if ($action == 'insert') {
            $form->addSubmit('continue', 'Save')->class('btn');
            $form->onValid = array($this, 'Form_CFaddShareholderCorporateFormSubmitted');
        } else {
            $form->addSubmit('continue', 'Save')->class('btn');
            $form->onValid = array($this, 'Form_CFeditShareholderCorporateFormSubmitted');
            $form->setInitValues($this->shareholder->getFields());
        }

        $form->start();
        return $form;
    }

    /**
     * Return form for adding/editin secretary director
     *
     * @param string $action
     * @param array $prefillOfficers
     * @param array $prefillAddress
     * @return FForm
     */
    protected function getComponentCFsecretaryPersonForm($action, $prefillOfficers, $prefillAddress)
    {
        // form
        if ($action == 'add') {
            $form = new CHForm('CFaddSecretaryPerson');
        } else {
            $form = new CHForm('CFeditSecretaryPerson');
        }

        // prefill
        $form->addFieldset('Autocomplete details (optional)');
        $form->addSelect('prefillOfficers', 'Select an existing appointment to autocomplete these details', $prefillOfficers['select'])
            ->setFirstOption('--- Select --')
            ->setDescription('Or enter the details below if adding a new appointment.');

        // person
        $form->addFieldset('Person');
        $form->addSelect('title', 'Title', Person::$titles)
            ->setFirstOption('--- Select ---');
        $form->addText('forename', 'First name *')
            ->addRule(FForm::Required, 'Please provide first name')
            ->addRule('MaxLength', "First name can't be more than 50 characters", 50);
        $form->addText('middle_name', 'Middle name')
            ->addRule('MaxLength', "Middle name can't be more than 50 characters", 50);
        $form->addText('surname', 'Last name *')
            ->addRule(FForm::Required, 'Please provide last name')
            ->addRule('MaxLength', "Last name can't be more than 160 characters", 160);

        // prefill
        $form->addFieldset('Prefill');
        $form->addSelect('prefillAddress', 'Prefill Address', $prefillAddress['select'])
            ->setFirstOption('--- Select ---');

        // adddress
        $form->addFieldset('Address');
        $form->addText('premise', 'Premise *')
            ->addRule(FForm::Required, 'Please provide Premise')
            ->addRule('MaxLength', "Premise can't be more than 50 characters", 50);
        $form->addText('street', 'Street *')
            ->addRule(FForm::Required, 'Please provide Street')
            ->addRule('MaxLength', "Street can't be more than 50 characters", 50);
        $form->addText('thoroughfare', 'Address 3')
            ->addRule('MaxLength', "Address 3 can't be more than 50 characters", 50);
        $form->addText('post_town', 'Town *')
            ->addRule(FForm::Required, 'Please provide Town')
            ->addRule('MaxLength', "Town can't be more than 50 characters", 50);
        $form->addText('county', 'County')
            ->addRule('MaxLength', "County can't be more than 50 characters", 50);
        $form->addText('postcode', 'Postcode *')
            ->addRule(FForm::Required, 'Please provide Postcode')
            ->addRule('MaxLength', "Postcode can't be more than 15 characters", 15);
        $form->addSelect('country', 'Country *', Address::$countries)
            ->addRule(FForm::Required, 'Please provide Country');

        $form->addFieldset('Consent to act');

        $form->add('SingleCheckbox', 'consentToAct', 'The subscribers (shareholders) confirm that the person named has consented to act as a secretary')
            ->addRule(FForm::Required, 'Consent to act is required');

        $form->addFieldset('Action');
        if ($action == 'add') {
            $form->addSubmit('continue', 'Save')->class('btn');
            $form->onValid = array($this, 'Form_CFaddSecretaryPersonFormSubmitted');
        } else {
            $form->addSubmit('continue', 'Save')->class('btn');
            $form->onValid = array($this, 'Form_CFeditSecretaryPersonFormSubmitted');
            $fields = $this->secretary->getFields();
            unset($fields['consentToAct']);
            $form->setInitValues($fields);
        }

        $form->start();
        return $form;
    }

    /**
     * Return form for adding/editing corporate secretary
     *
     * @param string $action
     * @param array $prefillOfficers
     * @param array $prefillAddress
     * @return FForm
     */
    public function getComponentCFsecretaryCorporateForm($action, $prefillOfficers, $prefillAddress)
    {
        // form
        if ($action == 'add') {
            $form = new CHForm('CFaddSecretaryCorporate');
        } else {
            $form = new CHForm('CFeditSecretaryCorporate');
        }

        // prefill
        $form->addFieldset('Autocomplete details (optional)');
        $form->addSelect('prefillOfficers', 'Select an existing appointment to autocomplete these details', $prefillOfficers['select'])
            ->setFirstOption('--- Select --')
            ->setDescription('Or enter the details below if adding a new appointment.');

        // person
        $form->addFieldset('Corporate');
        $form->addText('corporate_name', 'Company name *')
            ->addRule(FForm::Required, 'Please provide first name');
        $form->addText('forename', 'First name *')
            ->addRule(FForm::Required, 'Please provide first name')
            ->addRule('MaxLength', "First name can't be more than 50 characters", 50);
        $form->addText('surname', 'Last name *')
            ->addRule(FForm::Required, 'Please provide last name')
            ->addRule('MaxLength', "Last name can't be more than 160 characters", 160);

        // address
        $form->addFieldset('Prefill');
        $form->addSelect('prefillAddress', 'Prefill Address', $prefillAddress['select'])
            ->setFirstOption('--- Select ---');

        $form->addFieldset('Address');
        $form->addText('premise', 'Premise *')
            ->addRule(FForm::Required, 'Please provide Premise')
            ->addRule('MaxLength', "Premise can't be more than 50 characters", 50);
        $form->addText('street', 'Street *')
            ->addRule(FForm::Required, 'Please provide Street')
            ->addRule('MaxLength', "Street can't be more than 50 characters", 50);
        $form->addText('thoroughfare', 'Address 3')
            ->addRule('MaxLength', "Address 3 can't be more than 50 characters", 50);
        $form->addText('post_town', 'Town *')
            ->addRule(FForm::Required, 'Please provide Town')
            ->addRule('MaxLength', "Town can't be more than 50 characters", 50);
        $form->addText('county', 'County')
            ->addRule('MaxLength', "County can't be more than 50 characters", 50);
        $form->addText('postcode', 'Postcode *')
            ->addRule(FForm::Required, 'Please provide Postcode')
            ->addRule('MaxLength', "Postcode can't be more than 15 characters", 15);
        $form->addSelect('country', 'Country *', Address::$countries)
            ->addRule(FForm::Required, 'Please provide Country');

        $form->addFieldset('EEA/ Non EEA');
        $form->addRadio('eeaType', 'Type *', array(1 => 'EEA', 2 => 'Non EEA'))
            ->addRule(FForm::Required, 'Please provide EEA type!');
        $form->addText('place_registered', 'Country Registered *')
            ->addRule(array($this, 'Validator_eeaRequired'), 'Please provide Country registered!');
        $form->addText('registration_number', 'Registration number *')
            ->addRule(array($this, 'Validator_eeaRequired'), 'Please provide Registration number!');
        $form->addText('law_governed', 'Governing law *')
            ->addRule(array($this, 'Validator_eeaRequired'), 'Please provide Governing law!');
        $form->addText('legal_form', 'Legal From *')
            ->addRule(array($this, 'Validator_eeaRequired'), 'Please provide Legal form!');

        $form->addFieldset('Consent to act');
        $form->add('SingleCheckbox', 'consentToAct', 'The subscribers (shareholders) confirm that the corporate body named has consented to act as a secretary')
            ->addRule(FForm::Required, 'Consent to act is required');

        $form->addFieldset('Action');

        if ($action == 'add') {
            $form->addSubmit('continue', 'Save')->class('btn');
            $form->onValid = array($this, 'Form_CFaddSecretaryCorporateFormSubmitted');
        } else {
            $form->addSubmit('continue', 'Save')->class('btn');
            $form->onValid = array($this, 'Form_CFeditSecretaryCorporateFormSubmitted');
            $fields = $this->secretary->getFields();
            unset($fields['consentToAct']);
            $form->setInitValues($fields);
        }

        $form->start();
        return $form;
    }

    protected function setCompanyUpdateTabs($key, $title)
    {
        // tabs
        $this->tabs['view'] = array('title' => $this->company->getCompanyName(), 'params' => array("company_id" => $this->company->getCompanyId()));
        $this->tabs[$key] = array('title' => $title, 'params' => array("company_id" => $this->company->getCompanyId()));
    }

    /**
     * Returns addreses for select and javascript used for prefill address
     * @param Company $company
     * @return array
     */
    public static function getPrefillAdresses(Company $company)
    {
        if ($company->getStatus() == 'complete') {
            $addresses = $company->getAddresses();
        } else {
            $addresses = $company->getLastIncorporationFormSubmission()->getForm()->getAddresses();
        }

        $addressesFields = [];
        $selectValues = [];
        foreach ($addresses as $key => $address) {
            $fields = $address->getFields();
            $addressesFields[$key] = $fields;
            $selectValues[$key] = $fields['postcode'] . ', ' . $fields['premise'] . ' ' . $fields['street'];
        }

        return [
            'select' => $selectValues,
            'js' => json_encode($addressesFields, JSON_FORCE_OBJECT),
        ];
    }

    /**
     * Returns person details for select and javascript used for prefill person data
     * @param Company $company
     * @param string $type
     * @return array
     */
    protected static function getPrefillOfficers(Company $company, $type = NULL)
    {
        if ($company->getStatus() == 'complete') {
            $persons = $company->getPersons();
            $personscorp = $company->getCorporates();
        } else {
            $persons = $company->getLastIncorporationFormSubmission()->getForm()->getIncPersons(array('dir', 'sub', 'sec'));
            $personscorp = $company->getLastIncorporationFormSubmission()->getForm()->getIncCorporates(array('dir', 'sub', 'sec'));
        }

        $officersFields = [];
        $selectValues = [];

        if ($type == 'person') {
            foreach ($persons as $key => $person) {
                $fields = $person->getFields();
                $officersFields[$key] = $fields;
                $selectValues[$key] = $fields['surname'] . ' ' . $fields['forename'];
            }
        } elseif ($type == 'corporate') {
            foreach ($personscorp as $key => $person) {
                $fields = $person->getFields();
                $officersFields[$key] = $fields;
                $selectValues[$key] = $fields['corporate_name'];
            }
        }

        return [
            'select' => $selectValues,
            'js' => json_encode($officersFields, JSON_FORCE_OBJECT),
        ];
    }

    /**
     * Provides validation EEA/NonEAA fields
     *
     * @param Text $control
     * @param string $error
     * @param array $params
     * @return mixed
     */
    public function Validator_eeaRequired(Text $control, $error, $params)
    {
        if ($control->owner['eeaType']->getValue() !== NULL) {
            // EEA
            if ($control->owner['eeaType']->getValue() == 1) {
                if (($control->getName() == 'place_registered' || $control->getName() == 'registration_number') && $control->getValue() == '') {
                    return $error;
                }
                // non EEA
            } else {
                if (($control->getName() == 'place_registered' || $control->getName() == 'law_governed' || $control->getName() == 'legal_form') && $control->getValue() == '') {
                    return $error;
                }
            }
        }
        return TRUE;
    }

    /**
     * @param DateSelect $control
     * @param array $error
     * @param string $minYears
     * @return string|bool
     */
    public function Validator_personDOB(DateSelect $control, $error, $minYears)
    {
        $value = $control->getValue();

        if (strpos($value, '?') !== FALSE) {
            return 'Please select date of birth';
        }

        // check if dob is valid day
        $temp = explode('-', $value);
        if (checkdate($temp[1], $temp[2], $temp[0]) === FALSE) {
            return $error[0];
        }

        // check if director is older than 16 years
        $dob = date('Ymd', strtotime($value));
        $years = floor((date("Ymd") - $dob) / 10000);
        if ($years < $minYears) {
            return sprintf($error[1], $minYears);
        }
        return TRUE;
    }

    /**
     * @param FControl $control
     * @param string $error
     * @param array $params
     * @return string|bool
     */
    public function Validator_requiredResidentialAddress(FControl $control, $error, $params)
    {
        $value = $control->getValue();
        if ($params == 'add') {
            if ($control->owner['residentialAddress']->getValue() == 1 && empty($value)) {
                return $error;
            }
        } else {
            if (empty($value)) {
                return $error;
            }
        }
        return TRUE;
    }

    /**
     * @param Text $control
     * @param string $error
     * @param array $params
     * @return string|bool
     */
    public function Validator_companyNameChars(Text $control, $error, $params)
    {
        if (!preg_match('/[^\x20-\x7E]/', $control->getValue())) {
            return TRUE;
        }
        return $error;
    }

    /**
     * @param Radio $control
     * @param string $error
     * @param string $companyType
     * @return bool
     */
    public function Validator_natureOfControl(Radio $control, $error, $companyType)
    {
        /** @var CHForm $form */
        $form = $control->owner;

        $toCheck = [
            $form['ownership_of_voting_rights']->getValue(),
            $form['right_to_appoint_and_remove_directors']->getValue(),
            $form['significant_influence_or_control']->getValue(),
        ];

        if ($companyType != CompanyIncorporation::BY_GUARANTEE) {
            $toCheck[] = $form['ownership_of_shares']->getValue();
        }

        if (empty(array_filter($toCheck))) {
            return $error;
        }

        return TRUE;
    }
}
