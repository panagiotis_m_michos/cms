<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS
 * @author 		Nikolai Senkevich, Stan Bazik
 * @internal 	project/controlers/AccountantsInstituteAdminControler.php
 * @created 	28/03/2011
 */
class AccountantsInstituteAdminControler extends PageAdminControler
{

    /** @var array */
    protected $tabs = array(
        'page' => 'Page',
        'properties' => 'Properties',
        'details' => 'Details',
        'menu' => 'Menu',
        'seo' => 'Seo'
    );

    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'PageAdmin';
    }

    /**
     * @var string
     */
    static public $handleObject = 'AccountantsInstituteModel';
    /**
     * @var AccountantsInstituteModel
     */
    public $node;

    /*     * ***************************************** properties ****************************************** */

    public function renderProperties()
    {
        $slideshowForm = new AccountantsInstituteAdminForm($this->node->getId() . '_slideshow');
        $slideshowForm->startup($this, $this->node, array($this, 'Form_slideshowFormFormSubmitted'));
        $this->template->form = $slideshowForm;
    }

    /**
     * @param AccountantsInstituteAdminForm $form
     */
    public function Form_slideshowFormFormSubmitted(AccountantsInstituteAdminForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Data has been updated');
            $this->redirect('properties');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('properties');
        }
    }

}