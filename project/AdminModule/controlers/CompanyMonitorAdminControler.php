<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    DCAAdminControler.php 2009-03-19 divak@gmail.com
 */

class CompanyMonitorAdminControler extends ProductAdminControler
{
    /** @var string */
    static public $handleObject = 'CompanyMonitor';
    
    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'ProductAdmin';
    }
}