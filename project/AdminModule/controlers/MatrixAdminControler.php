<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    MatrixAdminControler.php 2012-03-13 razvanp@madesimplegroup.com
 */

class MatrixAdminControler extends PageAdminControler
{
    const MATRIX_ADMIN_PAGE =  1184;
    /**
	 * @var string
	 */
	static public $handleObject = 'MatrixModel';
	
	/**
	 * @var MatrixModel
	 */
	public $node;

    /**
	 * @var array
	 */
	protected $tabs = array(
		//'list' => 'Deleted Companies',
       'page' => 'Page', 
       'products'=>'Products',
        'layout' => 'Layout', 
       'details' => 'Details',
       'seo' => 'Seo'
        
       
	);

    /** @var array */
    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'PageAdmin';
    }
    
    /************************************* Products *************************************/

    public function  renderProducts(){
        $form = new MatrixPakagesAdminForm($this->node->getId() . '_MatrixAdmin');
        $form->startup($this, array($this, 'Form_MatrixPakagesAdminFormSubmitted'),$this->node);
        $this->template->form = $form;
    }
    /**
     * @param MatrixPakagesAdminForm $form
     */
    public function Form_MatrixPakagesAdminFormSubmitted(MatrixPakagesAdminForm $form)
    {
        try {
            $form->process();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }
}
