<?php
class NomineeSecretaryAdminControler extends ProductAdminControler
{
    /** @var string */
    static public $handleObject = 'NomineeSecretary';
    
    /** @var array */
    protected $tabs = array(
        'product' => 'Product',
        'properties' => 'Properties',
        'details' => 'Details',
        'seo' => 'Seo'
    );
    
    
    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'ProductAdmin';    
    }
    
    
    public function prepareProduct()
    {
        $form = new FForm($this->nodeId.'_'.$this->action);
        
        // product
        $form->addFieldset('Product');
        $form->addText('title', 'Title: * ')
            ->addRule(FForm::Required, 'Title is required!')
            ->size(30)
            ->setValue($this->node->page->title);
        $form->addText('alternativeTitle', 'Alternative Title:')
            ->size(50)
            ->setValue($this->node->page->alternativeTitle);
        $form->add('CmsFckArea', 'text', 'Page Text:')
            ->setSize(500, 300)
            ->setValue($this->node->page->text);
            
        // price	
        $form->addFieldset('Price');
        $form->addText('price', 'Price: *')
            ->size(5)
            ->setValue($this->node->price)
            ->addRule(FForm::Required, 'Please provide price')
            ->addRule('Numeric', 'Price has to be number');
        $form->addText('productValue', 'Product value: *')
            ->size(5)
            ->setValue($this->node->productValue)
            ->addRule(FForm::Required, 'Please provide product value')
            ->addRule('Numeric', 'Price value has to be number');
        $form->addText('associatedPrice', 'Associated price: *')
            ->size(5)
            ->setValue($this->node->associatedPrice)
            ->addRule(FForm::Required, 'Please provide associated price')
            ->addRule('Numeric', 'Associated price value has to be number');
        $form->addText('wholesalePrice', 'Wholesale price: *')
            ->size(5)
            ->setValue($this->node->wholesalePrice)
            ->addRule(FForm::Required, 'Please provide wholesale price')
            ->addRule('Numeric', 'Wholesale price value has to be number');
            
        // corporate
        $form->addFieldset('Corporate');
        $form->addText('corporateCompanyName', 'Company name *')
            ->addRule(FForm::Required, 'Please provide Company name')
            ->setValue($this->node->corporateCompanyName)
            ->size(30);
        $form->addText('corporateFirstName', 'First name *')
            ->addRule(FForm::Required, 'Please provide First name')
            ->setValue($this->node->corporateFirstName)
            ->size(30);
        $form->addText('corporateLastName', 'Last name *')
            ->addRule(FForm::Required, 'Please provide Last name')
            ->setValue($this->node->corporateLastName)
            ->size(30);
        $form->addText('corporateAddress1', 'Address 1 *')
            ->addRule(FForm::Required, 'Please provide Address 1')
            ->setValue($this->node->corporateAddress1);
        $form->addText('corporateAddress2', 'Address 3')
            ->setValue($this->node->corporateAddress2);
        $form->addText('corporateAddress3', 'Address 4')
            ->setValue($this->node->corporateAddress3);
        $form->addText('corporateTown', 'Town *')
            ->addRule(FForm::Required, 'Please provide Town')
            ->setValue($this->node->corporateTown);
        $form->addText('corporateCounty', 'County')
            ->setValue($this->node->corporateCounty);
        $form->addText('corporatePostcode', 'Postcode *')
            ->addRule(FForm::Required, 'Please provide Postcode')
            ->setValue($this->node->corporatePostcode)
            ->size(7);
        $form->addSelect('corporateCountryId', 'Country *', Address::$countries)
            ->addRule(FForm::Required, 'Please provide Country')
            ->setValue($this->node->corporateCountryId)
            ->setFirstOption('--- Select ---');
        $form->addText('corporateBirtown', 'Town of birth *')
            ->addRule(FForm::Required, 'Please provide Town of birth')
            ->setValue($this->node->corporateBirtown)
            ->size(3);
        $form->addText('corporateAuthTel', 'Telephone *')
            ->addRule(FForm::Required, 'Please provide Telephone')
            ->setValue($this->node->corporateAuthTel)
            ->size(3);
        $form->addText('corporateAuthEye', 'Eye colour *')
            ->addRule(FForm::Required, 'Please provide Eye colour')
            ->setValue($this->node->corporateAuthEye)
            ->size(3);
            
        // EEA
        $form->addFieldset('EEA');
        $form->addText('countryRegistered', 'Country registered')
            ->addRule(FForm::Required, 'Please provide Country registered')
            ->setValue($this->node->countryRegistered)
            ->size(3);
        $form->addText('registrationNumber', 'Registration Num.')
            ->addRule(FForm::Required, 'Please provide Registration Number')
            ->setValue($this->node->registrationNumber)
            ->size(5);
        
        // action
        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Submit')
            ->onclick('changes = false')
            ->class('btn');
        $form->onValid = array($this, 'packageFormSubmitted');
        $form->clean();
        $form->start();
        $this->template->form = $form;
    }
    
    
    public function packageFormSubmitted($form)
    {
        $data = $form->getValues();
        $this->node->page->title = $data['title'];
        
        $this->node->page->alternativeTitle = $data['alternativeTitle'];
        $this->node->page->text = $data['text'];
        $this->node->price = $data['price'];
        $this->node->associatedPrice = $data['associatedPrice'];
        $this->node->wholesalePrice = $data['wholesalePrice'];
        $this->node->productValue = $data['productValue'];
        
        // corporate
        $this->node->corporateCompanyName = $data['corporateCompanyName'];
        $this->node->corporateFirstName = $data['corporateFirstName'];
        $this->node->corporateLastName = $data['corporateLastName'];
        $this->node->corporateAddress1 = $data['corporateAddress1'];
        $this->node->corporateAddress2 = $data['corporateAddress2'];
        $this->node->corporateAddress3 = $data['corporateAddress3'];
        $this->node->corporateTown = $data['corporateTown'];
        $this->node->corporateCounty = $data['corporateCounty'];
        $this->node->corporatePostcode = $data['corporatePostcode'];
        $this->node->corporateCountryId = $data['corporateCountryId'];
        $this->node->corporateBirtown = $data['corporateBirtown'];
        $this->node->corporateAuthTel = $data['corporateAuthTel'];
        $this->node->corporateAuthEye = $data['corporateAuthEye'];
        $this->node->countryRegistered = $data['countryRegistered'];
        $this->node->registrationNumber = $data['registrationNumber'];
        
        $this->node->save();
        
        $this->flashMessage('Product has been updated');
        $this->redirect(NULL);
    }
}
