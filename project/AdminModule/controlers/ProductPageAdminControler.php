<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    ProductPageAdminControler.php 2009-03-19 divak@gmail.com
 */

class ProductPageAdminControler extends PageAdminControler
{
    /** @var array */
    protected $tabs = array(
        'page' => 'Page',
        'products' => 'Products',
        'layout' => 'Layout',
        'details' => 'Details',
        'seo' => 'Seo'
    );
    
    
    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'PageAdmin';
    }
    
    
    /********************************* properties *********************************/
    
    
    public function handleProducts()
    {
        // products
        $products = $this->node->getProperty('products');
        if ($products != NULL) {
            $products = explode(',', $products);
        }
        
        
        $form = new FForm($this->nodeId.'_'.$this->action);
        
        // products 
        $form->addFieldset('Products');
        $form->add('AsmSelect', 'products', 'Products:')
            ->multiple()
            ->title('--- Select --')
            ->setValue($products)
            ->setOptions(BasketProduct::getAllProducts());
        
        // action
        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Submit')->class('btn');
        $form->onValid = array($this, 'Form_productsFormSubmitted');
        $form->clean();
        $form->start();
        $this->template->form = $form;    
    }
    
    
    public function Form_productsFormSubmitted(FForm $form)
    {
        $data = $form->getValues();
        
        // products
        if (!empty($data['products'])) {
            $products = implode(',', $data['products']);
        } else {
            $products = NULL;
        }
        $this->node->saveProperty('products', $products);
        
        $form->clean();
        $this->flashMessage('Data has been updated');
        $this->redirect(NULL);
    }
}
