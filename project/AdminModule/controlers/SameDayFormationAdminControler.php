<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    SameDayFormationAdminControler.php 2009-12-14 divak@gmail.com
 */

class SameDayFormationAdminControler extends ProductAdminControler
{
    /** @var string */
    static public $handleObject = 'SameDayFormation';
    
    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'ProductAdmin';
    }
}