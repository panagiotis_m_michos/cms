<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    UsersRolesAdminControler.php 2009-10-13 divak@gmail.com
 */

class UsersRolesAdminControler extends NameAdminControler
{
    /** @var array */
    protected $tabs = array(
        'list' => 'List',
    );
    
    /** @var UserRole */
    private $role;
    
    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'NameAdmin';
    }
    
    public function handleList()
    {
        if (isset($this->get['delete_id'])) {
            try {
                $user = new FUserRole($this->get['delete_id']);
                
                // check deleting own account
                if ($user->getId() == FUser::getSignedIn()->roleId) {
                    $this->flashMessage("You can't delete own role!", 'error');
                } else {
                    $this->flashMessage('User has been deleted.');
                    $user->delete();
                }
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
            }
            $this->redirect(NULL, 'delete_id=');
        }
    }
    
    public function renderList()
    {
        $this->template->roles = FUserRole::getAll();
    }
    
    /********************************************* create *********************************************/
    
    public function prepareCreate()
    {
        $this->tabs['create'] = 'Create';
    }
    
    public function renderCreate()
    {
        $form  = new FForm($this->nodeId.'_'.$this->action);

        // data
        $form->addFieldset('Data');
        //		$form->addSelect('parentId','Parent: ', FUserRole::getDropdown())
        //			->setFirstOption('--- Select ---');
        $form->addText('key', 'Key: * ')
            ->addRule(FForm::Required, 'Key is required!');
        $form->addText('title', 'Title: *')
            ->addRule(FForm::Required, 'Title is required!');
        
        // action
        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Submit')->class('btn');
         
        $form->onValid = array($this, 'Form_createFormSubmitted');
        $form->start();
        $this->template->form = $form;
    }
    
    public function Form_createFormSubmitted(FForm $form)
    {
        $data = $form->getValues();
        
        try {
            $role = new FUserRole;
            //			$role->parentId = $data['parentId'];
            $role->key = $data['key'];
            $role->title = $data['title'];
            $role->save();
            $this->flashMessage('User role has been created');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
        }
        $form->clean();
        $this->redirect('list');
    }
    
    
    /********************************************* edit *********************************************/
    
    public function prepareEdit()
    {
        try {
            if (!isset($this->get['role_id'])) {
                $this->redirect('list');
            }
            $this->role = new FUserRole($this->get['role_id']);
            $this->tabs['edit'] = array('title'=>'Edit', 'params'=> "role_id={$this->role->getId()}");
        } catch (Exception $e) {
             $this->flashMessage($e->getMessage());
             $this->redirect('list');
        }
    }
    
    public function renderEdit()
    {
        $form  = new FForm($this->nodeId.'_'.$this->action);

        // data
        $form->addFieldset('Data');
        //		$form->addSelect('parentId','Parent: ', FUserRole::getDropdown())
        //			->setFirstOption('--- Select ---')
        //			->setDisabledOptions(array($this->role->getId()));
        $form->addText('key', 'Key: * ')
            ->addRule(FForm::Required, 'Key is required!');
        $form->addText('title', 'Title: *')
            ->addRule(FForm::Required, 'Title is required!');

        // action
        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Submit')
            ->class('btn');
         
        $form->setInitValues((array) $this->role);
        $form->onValid = array($this, 'Form_editFormSubmitted');
        $form->start();
        $this->template->form = $form;
    }
    
    
    public function Form_editFormSubmitted(FForm $form)
    {
        $data = $form->getValues();
        
        try {
            //			$this->role->parentId = $data['parentId'];
            $this->role->key = $data['key'];
            $this->role->title = $data['title'];
            $this->role->save();
            
            $this->flashMessage('Role has been updated');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        
        $form->clean();
        $this->redirect('list');
    }
    
}