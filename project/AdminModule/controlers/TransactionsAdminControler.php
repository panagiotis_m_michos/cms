<?php

class TransactionsAdminControler extends CHAdminControler
{
    /**
     * @var array 
     */
    protected $tabs = array(
        'list' => 'List',
    );

    /**
     * @var Transaction 
     */
    private $transaction;

    
    public function startup()
    {
        // add permission for staff
        $this->acl->allow('staff', 'page', array('view'));
        parent::startup();
    }

    public function beforePrepare()
    {
        try {
            if ($this->action && !in_array($this->action, array('list')) && empty($this->get['transactionId'])) {
                throw new Exception('TransactionId is Required');
            } elseif (isset($this->get['transactionId'])) {
                $this->transaction = new Transaction($this->get['transactionId']);
            }
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect('list');
        }
        parent::beforePrepare();
    }

    public function beforeRender()
    {
        parent::beforeRender();
        
        if ($this->transaction) {
            $this->template->transaction = $this->transaction;
        }
    }

    /****************************** list **********************************/
    

    public function prepareList()
    {
        $datasource = dibi::select('t.transactionId, t.statusId, t.typeId, c.email, t.dtc')
            ->from(TBL_TRANSACTIONS, 't')
            ->leftJoin(TBL_CUSTOMERS, 'c')
                ->on('t.customerId = c.customerId')
            ->orderBy('t.dtc')
                ->desc();

        $datagrid = new TransactionsAdminDatagrid($datasource, 'transactionId');
        $this->template->datagrid = $datagrid;
    }

    /****************************** view ***********************************/

    public function prepareView()
    {
        $this->tabs[] = array('title' => 'Transaction', 'params' => array('transactionId' => $this->transaction->getId()));
    }

}
