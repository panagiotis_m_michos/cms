<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    CFRegisteredOfficeAdminControler.php 2011-11-18 razvanp@madesimplegroup.com
 */

class CFRegisteredOfficeAdminControler extends PageAdminControler 
{

    /** 
     * @var array 
     */
	protected $tabs = array(
		'page' => 'Page',
		'properties' => 'Properties',
		'layout' => 'Layout',
		'details' => 'Details',
		'menu' => 'Menu',
		'seo' => 'Seo'
	);
    
    /** 
     * @var string 
     */
	protected $templateExtendedFrom = array('BaseAdmin', 'PageAdmin');
    
    /** 
     * @var string 
     */
    static public $handleObject = 'CFRegisteredOfficeModel';

    /**
     * @var CFRegisteredOfficeModel
     */
    public $node;    
    
    public function renderProperties()
	{
         $form = new CFRegisteredOfficePropertiesAdminForm($this->node->getId() . '_CFRegisteredOfficePropertiesAdminForm');
        $form->startup($this, array($this, 'Form_CFRegisteredOfficePropertiesAdminFormSubmitted'), $this->node);
        $this->template->form = $form;
       
    }

    /**
     * @param CFRegisteredOfficePropertiesAdminForm $form
     */
    public function Form_CFRegisteredOfficePropertiesAdminFormSubmitted(CFRegisteredOfficePropertiesAdminForm $form) {
        try {
            //generate pdf from form and shareholder data
            $form->process();
            $this->flashMessage('Updated!');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $this->redirect();
    }
    
}
