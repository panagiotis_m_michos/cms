<?php

use Form\Both\CUDirector\EditDirectorPersonForm;
use Form\Both\CUDirector\EditDirectorCorporateForm;
use Form\Both\CUDirector\AddDirectorCorporateForm;
use Form\Both\CUDirector\AddDirectorPersonForm;
use Form\Both\CUSecretary\AddSecretaryCorporateForm;
use Form\Both\CUSecretary\EditSecretaryCorporateForm;
use Form\Both\CUSecretary\AddSecretaryPersonForm;
use Form\Both\CUSecretary\EditSecretaryPersonForm;
use Form\Both\CURegisteredOffice\EditRegisteredOfficeForm;
use Entities\Company as CompanyEntity;
use Services\CompanyService;

abstract class CUAdminControler extends CFAdminControler
{

    /**
     * @var CompanyEmailer 
     */
    protected $companyEmailer;

    /**
     * @var CompanyEntity
     */
    private $companyEntity;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var string 
     */
    protected $directorType;

    /**
     * @var Prefiller 
     */
    protected $prefiller;

    /*     * *************************** CUsendDocumentsToCustomer ********************************** */

    public function startup()
    {
        parent::startup();
        $this->companyEmailer = $this->emailerFactory->get(EmailerFactory::COMPANY);

        // temporary fix
        // CompaniesAdminControler create company we should move company creation to lower level
        $this->directorType = 'director';
        if (isset($this->get['company_id'])) {
            try {
                $this->company = Company::getCompany($this->get['company_id']);

                if ($this->company->getType() == 'LLP') {
                    $this->directorType = 'member';
                }
                $this->companyService = $this->getService(DiLocator::SERVICE_COMPANY);
                $this->companyEntity = $this->companyService->getCompanyById($this->get['company_id']);
                $this->prefiller = new Prefiller($this->company);

            } catch (Exception $e) {
//                Debug::log(sprintf("%s | User: %s", $e->getMessage(), FUser::getSignedIn()->login));
                $this->flashMessage($e->getMessage(), 'error');
                $this->redirect('list');
            }
        }
    }

    protected function prepareCUsendDocumentsToCustomer()
    {
        $documents = $this->company->getDocumentsList();
        if (!empty($documents)) {
            $this->companyEmailer->sendIncorporatedDocuments($this->customer, $this->company, $documents);
            $this->flashMessage('Documents has been sent.');
        } else {
            $this->flashMessage('No documents to send.', 'error');
        }
        $this->redirect('view', "company_id={$this->company->getCompanyId()}");
    }

    /*     * **************************** CUsendShareCertsToCustomer ********************************** */

    protected function prepareCUsendShareCertsToCustomer()
    {
        $data = $this->company->getData();

        if (!isset($data['subscribers'])) {
            $this->flashMessage('No documents to send.', 'error');
            $this->redirect('view', "company_id={$this->company->getCompanyId()}");
        }
        if (FormSubmission::getCompanyIncorporationFormSubmission($this->company) == NULL) {
            $this->flashMessage('No documents to send.', 'error');
            $this->redirect('view', "company_id={$this->company->getCompanyId()}");
        }

        $companyIncorporation = FormSubmission::getCompanyIncorporationFormSubmission($this->company);
        $certificates = array();
        for ($x = 0; $x < count($data['subscribers']); $x++) {

            if (isset($data['subscribers'][$x]['corporate_name'])) {
                $subscriber = $companyIncorporation->getForm()->getIncCorporate($data['subscribers'][$x]['id'], 'SUB');
                $certificates[$x] = ShareCert::getCorporateCertificate($subscriber, $this->company->getCompanyName(), $this->company->getCompanyNumber(), $data['registered_office'], $x + 1);
            } else {
                $subscriber = $companyIncorporation->getForm()->getIncPerson($data['subscribers'][$x]['id'], 'SUB');
                $certificates[$x] = ShareCert::getPersonCertificate($subscriber, $this->company->getCompanyName(), $this->company->getCompanyNumber(), $data['registered_office'], $x + 1);
            }
        }

        if (!empty($certificates)) {
            $this->companyEmailer->sendShareCertificates($this->customer, $certificates);

            $this->flashMessage('Documents has been sent.');
        } else {
            $this->flashMessage('No documents to send.', 'error');
        }
        $this->redirect('view', "company_id={$this->company->getCompanyId()}");
    }

    /*     * **************************** changeAuthenticationCode ********************************** */

    protected function prepareChangeAuthenticationCode()
    {
        $form = new EditAuthenticationCode($this->company->getId() . '_editAuthenticationCode');
        $form->startup($this, array($this, 'Form_changeAuthenticationCodeFormSubmitted'), $this->company);
        $this->template->form = $form;

        // tabs
        $this->tabs['view'] = array('title' => $this->company->getCompanyName(), 'params' => "company_id={$this->company->getCompanyId()}");
        $this->tabs['changeAuthenticationCode'] = array('title' => 'Change authentication code', 'params' => "company_id={$this->company->getCompanyId()}");
    }

    public function Form_changeAuthenticationCodeFormSubmitted(EditAuthenticationCode $form)
    {
        try {
            $form->process();
            $this->flashMessage('Authentication code has been changed.');
            $form->clean();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $this->redirect('view', "company_id={$this->company->getCompanyId()}");
    }

    /*     * *************************** change accounting reference date ********************************** */

    public function prepareCUupdateAccountingReferenceDate()
    {
        $form = new CUupdateAccountingReferenceDateForm('changeDate');
        $form->startup($this, array($this, 'Form_ChangeAccountingReferenceDateFormSubmitted'), $this->company);
        $this->template->form = $form;

        $this->setCompanyUpdateTabs('CUupdateAccountingReferenceDate', 'Update Accounting Reference Date');
        $data = $this->company->getData();
        $this->template->lastAccountMadeUp = $data['company_details']['accounts_last_made_up_date'];
        $this->template->ardCurrent = $this->company->getAccountingReferenceDate();
        $this->template->ardPrevios = date('d-m-Y', strtotime('-1 year', strtotime($this->company->getAccountingReferenceDate())));
    }

    /**
     * @param CUupdateAccountingReferenceDateForm $form
     */
    public function Form_ChangeAccountingReferenceDateFormSubmitted(CUupdateAccountingReferenceDateForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('You ARD change request has been sent to Companies House. Approval should be given in approx. 3 hours.');
            $this->redirect('view', "company_id={$this->company->getCompanyId()}");
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    /*     * *************************** updateOffice ********************************** */

    public function prepareCUupdateOffice()
    {
        $form = new EditRegisteredOfficeForm($this->company->getId() . '_editRegisteredOfficeForm');
        $form->startup($this, array($this, 'Form_CUupdateOfficeFormSubmitted'), $this->company, $this->prefiller);
        $this->template->form = $form;

        $this->setCompanyUpdateTabs('CUupdateOffice', 'Registered office');
        $prefillAddress = $this->prefiller->getPrefillAdresses();
        $this->template->jsAdresses = $prefillAddress['js'];
    }

    public function Form_CUupdateOfficeFormSubmitted(EditRegisteredOfficeForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Request has been sent to Companies House.');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
        $this->redirect('view', "company_id={$this->company->getCompanyId()}");
    }

    public function prepareCUresign()
    {
        if (isset($this->get['resign']) && isset($this->get['officer_id'])) {
            try {
                $officer = $this->company->getOfficer($this->get['officer_id']);
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage());
                $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
            }
            $resignForm = new CUResignAdminForm($this->company->getId() . '_resign');
            $resignForm->startup($this, $this->company, $officer, array($this, 'Form_ResignFormSubmitted'));
            $this->template->form = $resignForm;
        } else {
            $this->flashMessage('Officer record not exist');
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }
        $this->tabs['view'] = array('title' => $this->company->getCompanyName(), 'params' => "company_id={$this->company->getCompanyId()}");
        $this->tabs['CUResign'] = array('title' => 'Resignation Date', 'params' => "company_id={$this->company->getCompanyId()}");
    }

    /**
     * @param CUResignAdminForm $form
     */
    public function Form_ResignFormSubmitted(CUResignAdminForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Application has been sent.');
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }
    }

    /*     * *************************** CUaddDirectorPerson ********************************** */

    public function prepareCUAddDirectorPerson()
    {
        $form = new AddDirectorPersonForm($this->company->getId() . '_addDirectorPerson');
        $form->beforeValidation = function ($form) {
            $form['postcode']->removeRule(array('CHValidator', 'Validator_serviceAddress'));
        };
        $form->startup($this, array($this, 'Form_CUaddDirectorPersonFormSubmitted'), $this->company, $this->companyEntity, $this->prefiller);
        $this->template->form = $form;

        $this->setCompanyUpdateTabs('CUaddDirectorPerson', 'Add ' . $this->directorType . ' person');

        $prefillAddress = $this->prefiller->getPrefillAdresses();
        $prefillOfficers = $this->prefiller->getPrefillOfficers();

        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
    }

    public function Form_CUaddDirectorPersonFormSubmitted($form)
    {
        try {
            $form->process();
            $this->flashMessage('Application has been sent to companies house');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
        }
        $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
    }

    /*     * **************************** addDirectorCorporate ********************************** */

    public function prepareCUaddDirectorCorporate()
    {
        $this->setCompanyUpdateTabs('CUaddDirectorCorporate', 'Add ' . $this->directorType . ' corporate');

        $form = new AddDirectorCorporateForm($this->company->getId() . '_addDirectorCorporate');
        $form->startup($this, array($this, 'Form_CUaddDirectorCorporateFormSubmitted'), $this->company, $this->companyEntity, $this->prefiller);
        $this->template->form = $form;

        $prefillAddress = $this->prefiller->getPrefillAdresses();
        $prefillOfficers = $this->prefiller->getPrefillOfficers(Prefiller::TYPE_CORPORATE);
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
    }

    public function Form_CUaddDirectorCorporateFormSubmitted(FForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Application has been sent to companies house');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
        }
        $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
    }

    /*     * **************************** editDirectorPerson ********************************** */

    public function prepareCUeditDirectorPerson()
    {
        $this->setCompanyUpdateTabs('CUeditDirectorPerson', 'Edit ' . $this->directorType . ' person');
        // check get
        if (!isset($this->get['director_id'])) {
            $this->redirect('view', array('company_id=' . $this->company->getCompanyId()));
        }
        // check existing director
        try {
            $this->director = $this->company->getPerson($this->get['director_id'], 'dir');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId(), 'director_id' => NULL));
        }
    }

    protected function renderCUeditDirectorPerson()
    {
        $form = new EditDirectorPersonForm($this->company->getId() . '_editDirectorPerson');
        $form->beforeValidation = function ($form) {
            $form['postcode']->removeRule(array('CHValidator', 'Validator_serviceAddress'));
        };
        $form->startup($this, array($this, 'Form_CUeditDirectorPersonFormSubmitted'), $this->company, $this->director, $this->prefiller);
        $this->template->form = $form;

        $prefillAddress = $this->prefiller->getPrefillAdresses();
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
    }

    public function Form_CUeditDirectorPersonFormSubmitted(FForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Director has been updated');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
    }

    /*     * **************************** editDirectorCorporate ********************************** */

    public function prepareCUeditDirectorCorporate()
    {
        $this->setCompanyUpdateTabs('CUeditDirectorCorporate', 'Edit ' . $this->directorType . ' corporate');

        // check get
        if (!isset($this->get['director_id'])) {
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }

        // check existing director
        try {
            $this->director = $this->company->getCorporate($this->get['director_id'], 'dir');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }
    }

    protected function renderCUeditDirectorCorporate()
    {
        $form = new EditDirectorCorporateForm($this->company->getId() . '_editDirectorCorporate');
        $form->startup($this, array($this, 'Form_CUeditDirectorCorporateFormSubmitted'), $this->company, $this->director, $this->prefiller);
        $this->template->form = $form;

        // prefill
        $prefillAddress = $this->prefiller->getPrefillAdresses();
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
    }

    public function Form_CUeditDirectorCorporateFormSubmitted($form)
    {
        try {
            $form->process();
            $this->flashMessage('Director has been updated');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
    }

    /*     * *************************** showDirectorPerson ********************************** */

    public function prepareCUshowDirectorPerson()
    {
        $this->setCompanyUpdateTabs('CUshowDirectorPerson', 'Show ' . $this->directorType . ' person');

        if (isset($this->get['director_id']) && isset($this->get['resign'])) {
            try {
                $this->company->sendTerminationOfDirector($this->get['director_id']);
                $this->flashMessage('Application has been sent.');
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage());
            }
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }
    }

    protected function renderCUshowDirectorPerson()
    {
        // check get
        if (!isset($this->get['director_id'])) {
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId()));
        }
        // check existing director
        try {
            $person = $this->company->getPerson($this->get['director_id'], 'dir');
            $this->template->director = $person->getFields();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId(), 'director_id' => NULL));
        }
    }

    /*     * **************************** showDirectorCorporate ********************************** */

    public function prepareCUshowDirectorCorporate()
    {
        $this->setCompanyUpdateTabs('CUshowDirectorCorporate', 'View ' . $this->directorType . ' corporate');
        // remove
        if (isset($this->get['director_id']) && isset($this->get['resign'])) {
            try {
                $this->company->sendTerminationOfDirector($this->get['director_id']);
                $this->flashMessage('Application has been sent.');
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
            }
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }
    }

    protected function renderCUshowDirectorCorporate()
    {
        // check get
        if (!isset($this->get['director_id'])) {
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId()));
        }
        // check existing director
        try {
            $director = $this->company->getCorporate($this->get['director_id'], 'dir');
            $this->template->director = $director->getFields();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId(), 'director_id' => NULL));
        }
    }

    /*     * **************************** printShareholderPerson ********************************** */

    public function prepareCUprintShareholderPerson()
    {
        $this->setCompanyUpdateTabs('CUshowShareholderPerson', 'Show Shareholder Person');
        // check get
        if (!isset($this->get['shareholder_id'])) {
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId()));
        }
        // check capital more than 1
        $data = $this->company->getData();
        //if(isset($data['capitals'][1])){
        //    $this->flashMessage("Sorry, we are unable to auto produce your share certificate. Please use the below share certificate template.");
        //    $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id='.$this->company->getCompanyId(),'shareholder_id'=>NULL));
        //}
        // check existing shareholder
        $shareholder = $this->company->getPerson($this->get['shareholder_id'], 'sub');
        $address = $shareholder->getAddress();
        $addressArr = $address->getFields();

        // check address
        if (empty($addressArr['street'])) {

            //show form -- passing shareholder, company and certificate number
            $form = new CompaniesAdminPersonForm($this->company->getId() . '_home');
            $form->startup($this, array($this, 'Form_shareholderPersonFormSubmitted'), $shareholder, $this->company, $this->get['num']);
            $this->template->form = $form;
        } else {
            //print pdf
            $person = ShareCert::getPersonCertificate($shareholder, $this->company->getCompanyName(), $this->company->getCompanyNumber(), $addressArr, $this->get['num']);
            $person->getPdfCertificate();
        }

        $this->template->shareholder = $shareholder->getFields();
    }

    /**
     * @param CompaniesAdminPersonForm $form
     */
    public function Form_shareholderPersonFormSubmitted(CompaniesAdminPersonForm $form)
    {
        try {
            $form->process();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId(), 'shareholder_id' => NULL));
        }
    }

    /*     * *************************** printShareholderCorporate ********************************** */

    public function prepareCUprintShareholderCorporate()
    {
        $this->setCompanyUpdateTabs('CUshowShareholderCorporate', 'Show Shareholder Corporate');
        // check get
        if (!isset($this->get['shareholder_id'])) {
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId()));
        }

        // check capital more than 1
        $data = $this->company->getData();
        //if(isset($data['capitals'][1])){
        //    $this->flashMessage("Sorry, we are unable to auto produce your share certificate. Please use the below share certificate template.");
        //    $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id='.$this->company->getCompanyId(),'shareholder_id'=>NULL));
        //}
        // check existing shareholder
        $shareholder = $this->company->getCorporate($this->get['shareholder_id'], 'sub');
        $address = $shareholder->getAddress();
        $addressArr = $address->getFields();

        // check address
        if (empty($addressArr['street'])) {

            //show form -- passing shareholder, company and certificate number
            $form = new CompaniesAdminCorporateForm($this->company->getId() . '_home');
            $form->startup($this, array($this, 'Form_shareholderCorporateFormSubmitted'), $shareholder, $this->company, $this->get['num']);
            $this->template->form = $form;
        } else {
            //print pdf
            $corporate = ShareCert::getCorporateCertificate($shareholder, $this->company->getCompanyName(), $this->company->getCompanyNumber(), $addressArr, $this->get['num']);
            $corporate->getPdfCertificate();
        }

        $this->template->shareholder = $shareholder->getFields();
    }

    /**
     * @param CompaniesAdminCorporateForm $form
     */
    public function Form_shareholderCorporateFormSubmitted(CompaniesAdminCorporateForm $form)
    {
        try {
            //generate pdf from form and shareholder data
            $form->process();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId(), 'shareholder_id' => NULL));
        }
    }

    /*     * *************************** showShareholderPerson ********************************** */

    public function prepareCUshowShareholderPerson()
    {
        $this->setCompanyUpdateTabs('CUshowShareholderPerson', 'Show Shareholder Person');
        // check get
        if (!isset($this->get['shareholder_id'])) {
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId()));
        }
        // check existing shareholder
        try {
            $shareholder = $this->company->getPerson($this->get['shareholder_id'], 'sub');
            $this->template->shareholder = $shareholder->getFields();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId(), 'shareholder_id' => NULL));
        }
    }

    /*     * ************************** showShareholderCorporate ********************************** */

    public function prepareCUshowShareholderCorporate()
    {
        $this->setCompanyUpdateTabs('CUshowShareholderCorporate', 'Show Shareholder Corporate');
        // check get
        if (!isset($this->get['shareholder_id'])) {
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId()));
        }
        // check existing shareholder
        try {
            $shareholder = $this->company->getCorporate($this->get['shareholder_id'], 'sub');
            $this->template->shareholder = $shareholder->getFields();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId(), 'shareholder_id' => NULL));
        }
    }

    public function prepareCUuploadIncorporationDocuments()
    {
        $this->setCompanyUpdateTabs('CUuploadIncorporationDocuments', 'Upload Incorparation Certificate');

        $form = new CompaniesAdminCompIncorpUploadForm($this->company->getId() . '_home');
        $form->startup($this, array($this, 'Form_uploadIncorporationDocumentsFormSubmitted'), $this->company);
        $this->template->form = $form;
    }

    /**
     * @param CompaniesAdminCorporateForm $form
     */
    public function Form_uploadIncorporationDocumentsFormSubmitted(CompaniesAdminCompIncorpUploadForm $form)
    {
        try {
            $form->process();
            $this->flashMessage("Certificate of Incorporation  an Memorandum Article has been successfully uploaded");
            $this->redirect('view', array('company_id=' . $this->company->getCompanyId()));
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId(), 'shareholder_id' => NULL));
        }
    }

    public function prepareCUsendIcoparationEmailToCustomer()
    {
        $docPath = CHFiling::getDocPath() . '/' . CHFiling::getDirName($this->company->getCompanyId()) . '/company-' . $this->company->getCompanyId() . '/documents/';
        if (!empty($docPath)) {
            $this->companyEmailer->sendIncorporatedCompany($this->customer, $this->company, $docPath);
            $this->flashMessage('Email has been sent.');
        } else {
            $this->flashMessage('Please Upload Incorporatio Certificate before send.', 'error');
        }
        $this->redirect('view', "company_id={$this->company->getCompanyId()}");
    }

    /*     * **************************** addSecretaryPerson ********************************** */

    public function prepareCUaddSecretaryPerson()
    {
        $this->setCompanyUpdateTabs('CUaddSecretaryPerson', 'Add Secretary Person');

        $form = new AddSecretaryPersonForm($this->company->getId() . '_addSecretaryPerson');
        $form->beforeValidation = function ($form) {
            $form['postcode']->removeRule(array('CHValidator', 'Validator_serviceAddress'));
        };
        $form->startup($this, array($this, 'Form_CUaddSecretaryPersonFormSubmitted'), $this->company, $this->companyEntity, $this->prefiller);
        $this->template->form = $form;

        // prefill
        $prefillAddress = $this->prefiller->getPrefillAdresses();
        $prefillOfficers = $this->prefiller->getPrefillOfficers();
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
    }

    public function Form_CUaddSecretaryPersonFormSubmitted(AddSecretaryPersonForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Application has been sent');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect();
        }
        $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
    }

    /*     * **************************** addSecretaryCorporate ********************************** */

    public function prepareCUaddSecretaryCorporate()
    {
        $this->setCompanyUpdateTabs('CUaddSecretaryCorporate', 'Add Secretary Corporate');

        $form = new AddSecretaryCorporateForm($this->company->getId() . '_addSecretaryCorporate');
        $form->startup($this, array($this, 'Form_CUaddSecretaryCorporateFormSubmitted'), $this->company, $this->companyEntity, $this->prefiller);
        $this->template->form = $form;

        // prefill 
        $prefillAddress = $this->prefiller->getPrefillAdresses();
        $prefillOfficers = $this->prefiller->getPrefillOfficers(Prefiller::TYPE_CORPORATE);

        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
    }

    public function Form_CUaddSecretaryCorporateFormSubmitted(AddSecretaryCorporateForm $form)
    {
        try {
            $form->process();
            $this->flashMessage("Application has been sent");
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect();
        }

        $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
    }

    /*     * **************************** editSecretaryPerson ********************************** */

    public function prepareCUEditSecretaryPerson()
    {
        $this->setCompanyUpdateTabs('CUeditSecretaryPerson', 'Edit Secretary Person');
        // check get
        if (!isset($this->get['secretary_id'])) {
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId()));
        }
        // check existing secretary
        try {
            $this->secretary = $this->company->getPerson($this->get['secretary_id'], 'sec');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId(), 'secretary_id' => NULL));
        }
    }

    protected function renderCUEditSecretaryPerson()
    {
        $form = new EditSecretaryPersonForm($this->company->getId() . '_editSecretaryPerson');
        $form->beforeValidation = function ($form) {
            $form['postcode']->removeRule(array('CHValidator', 'Validator_serviceAddress'));
        };
        $form->startup($this, array($this, 'Form_editSecretaryPersonFormSubmitted'), $this->company, $this->secretary, $this->prefiller);
        $this->template->form = $form;

        // prefill 
        $prefillAddress = $this->prefiller->getPrefillAdresses();
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
    }

    public function Form_editSecretaryPersonFormSubmitted(FForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Secretary has been updated');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
    }

    /*     * **************************** editSecretaryCorporate ********************************** */

    public function prepareCUEditSecretaryCorporate()
    {
        $this->setCompanyUpdateTabs('CUeditSecretaryCorporate', 'Edit Secretary Corporate');

        // check get
        if (!isset($this->get['secretary_id'])) {
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, 'company_id=' . $this->company->getCompanyId());
        }

        // check existing secretary
        try {
            $this->secretary = $this->company->getCorporate($this->get['secretary_id'], 'sec');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId(), 'secretary_id' => NULL));
        }
    }

    protected function renderCUEditSecretaryCorporate()
    {
        $form = new EditSecretaryCorporateForm($this->company->getId() . '_editSecretaryCorporate');
        $form->startup($this, array($this, 'Form_editSecretaryCorporateFormSubmitted'), $this->company, $this->secretary, $this->prefiller);
        $this->template->form = $form;

        // prefill 
        $prefillAddress = $this->prefiller->getPrefillAdresses();
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
    }

    public function Form_editSecretaryCorporateFormSubmitted(FForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('secretary has been updated');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
    }

    /*     * **************************** showSecretaryPerson ********************************** */

    public function handleCUshowSecretaryPerson()
    {
        $this->setCompanyUpdateTabs('CUshowSecretaryPerson', 'Show Secretary Person');

        if (isset($this->get['secretary_id']) && isset($this->get['resign'])) {
            try {
                $this->company->sendTerminationOfSecretary($this->get['secretary_id']);
                $this->flashMessage('Application has been sent');
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage());
            }
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }
    }

    protected function renderCUshowSecretaryPerson()
    {
        // check get
        if (!isset($this->get['secretary_id'])) {
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId()));
        }
        // check existing secretary
        try {
            $secretary = $this->company->getPerson($this->get['secretary_id'], 'sec');
            $this->template->secretary = $secretary->getFields();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId(), 'secretary_id' => NULL));
        }
    }

    /*     * **************************** showSecretaryCorporate ********************************** */

    public function prepareCUshowSecretaryCorporate()
    {
        $this->setCompanyUpdateTabs('CUshowSecretaryCorporate', 'Show Secretary Corporate');
        // remove
        if (isset($this->get['secretary_id']) && isset($this->get['resign'])) {
            try {
                $this->company->sendTerminationOfSecretary($this->get['secretary_id']);
                $this->flashMessage('Secretary has been resigned.');
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage());
            }
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }
    }

    protected function renderCUshowSecretaryCorporate()
    {
        // check get
        if (!isset($this->get['secretary_id'])) {
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId()));
        }
        // check existing secretary
        try {
            $secretary = $this->company->getCorporate($this->get['secretary_id'], 'sec');
            $this->template->secretary = $secretary->getFields();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId(), 'secretary_id' => NULL));
        }
    }

    /*     * **************************** changeDate ********************************** */

    public function prepareChangeDate()
    {
        $this->setCompanyUpdateTabs('changeDate', 'Change Date');

        $form = new EditAccountingReferenceDateForm($this->company->getId() . '_editAccountingReferenceDateForm');
        $form->startup($this, array($this, 'Form_editAccountingReferenceDateForm'), $this->company);
        $this->template->form = $form;
    }

    public function Form_editAccountingReferenceDateForm(EditAccountingReferenceDateForm $form)
    {
        try {
            $this->process();
            $this->flashMessage('Date has been changed.');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
        }

        $this->redirect(CUSummaryControler::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
    }

    /*     * **************************** filing ********************************** */

    public function prepareFiling()
    {
        $this->setCompanyUpdateTabs('filing', 'Filing');
    }

    /*     * ****************************************** return of allotment ******************************************** */

    public function prepareCUReturnOfAllotmentShares()
    {
        try {
            $model = new ReturnOfAllotmentSharesModel($this->company);
            $this->setCompanyUpdateTabs('CUreturnOfAllotmentShares', 'Return of Allotment Shares');
            $this->template->form = ReturnOfAllotmentSharesModel::getComponentSummaryForm($this, $model, 'admin');
            $this->template->shares = $model->getShares();
            $form = ReturnOfAllotmentSharesModel::getComponentSummaryForm($this, $model, 'admin');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('view', "company_id={$this->company->getCompanyId()}");
        }
    }

    public function Form_CUreturnOfAllotmentSharesFormSubmitted(FForm $form)
    {
        try {
            $data = $form->getValues();
            $model = new ReturnOfAllotmentSharesModel($this->company);
            $model->saveSummary($this->company, $data);
            $form->clean();
            $this->flashMessage('Application has been sent to companies house');
            $this->redirect('view', "company_id={$this->company->getCompanyId()}");
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    /*     * ****************************************** add annual return ******************************************** */

    public function prepareCUaddAnnualReturn()
    {
        $this->setCompanyUpdateTabs('CUaddAnnualReturn', 'Add Confirmation Statement');
    }

    public function renderCUAddAnnualReturn()
    {
        $form = new AddAnnualReturn($this->company->getId() . '_addAnnualReturn');
        $form->startup($this, array($this, 'Form_CUaddAnnualReturnFormSubmitted'), $this->company, $this->customer);
        $this->template->form = $form;
    }

    public function Form_CUaddAnnualReturnFormSubmitted(FForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Confirmation Statement has been added to company');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
        $this->redirect('view', "company_id={$this->company->getCompanyId()}");
    }

    /*     * ****************************************** hideCompany ************************************************** */

    public function handleHideCompany()
    {
        try {
            $this->company->hideCompany();
            $this->flashMessage('Company has been hidden');
            $this->redirect('view', "company_id={$this->company->getCompanyId()}");
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('view', "company_id={$this->company->getCompanyId()}");
        }
    }

    /*     * ****************************************** unhideCompany ************************************************** */

    public function handleUnhideCompany()
    {
        try {
            $this->company->unhideCompany();
            ;
            $this->flashMessage('Company has been unhidden');
            $this->redirect('view', "company_id={$this->company->getCompanyId()}");
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('view', "company_id={$this->company->getCompanyId()}");
        }
    }

    /*     * ****************************************** add company name change ******************************************** */

    public function prepareCUaddCompanyNameChange()
    {
        $this->setCompanyUpdateTabs('CUaddCompanyNameChange', 'Company Name Change');

        $service = new CompanyNameChangeServiceModel($this->company);
        try {
            $service->isOkToChangeNameAdmin();
            $this->template->hasProduct = (bool) $this->company->hasChangeName();
            $this->template->companyName = $this->company->getCompanyName();
            $this->template->companyId = $this->company->getCompanyId();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('view', "company_id={$this->company->getCompanyId()}");
        }
    }

    public function renderCUaddCompanyNameChange()
    {
        $form = new ChangeCompanyNameAdminForm($this->company->getId() . '_ChangeCompanyNameAdmin');
        $form->startup($this, array($this, 'Form_ChangeCompanyNameAdminFormSubmitted'), $this->company, $this->customer);
        $this->template->form = $form;
    }

    /**
     * @param ChangeCompanyNameAdminForm $form
     */
    public function Form_ChangeCompanyNameAdminFormSubmitted(ChangeCompanyNameAdminForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Your company name request has been sent');
            $this->redirect('view', "company_id={$this->company->getCompanyId()}");
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    /*     * ****************************************** set reminder ******************************************** */

    public function prepareCUsetCompanyEReminder()
    {
        $this->setCompanyUpdateTabs('CUsetCompanyEReminder', 'Set EReminder');
    }

    public function renderCUsetCompanyEReminder()
    {
        try {
            $addForm = new EReminderAddAdminForm($this->company->getId() . '_set');
            $addForm->startup($this, array($this, 'Form_reminderFormSubmitted'), $this->company);

            // get all reminders  which is active and set this reminders exclude one which was provided by user
            if (isset($this->get['emailToDelete'])) {
                $emailToDelete = $this->get['emailToDelete'];

                $reminder = GetEReminders::getNewReminder($this->company);
                $reminderEmails = $reminder->sendRequest();
                $xml = simplexml_load_string($reminderEmails);

                //check xml and throw exception in case error
                $this->checkXml($xml);
                $emails = array();
                foreach ($xml->Body->EReminders->Recipient as $value) {
                    if ($emailToDelete != (string) $value->EmailAddress) {
                        $emails[] = (string) $value->EmailAddress;
                    }
                }
                $reminder = SetEReminders::getNewReminder($emails, $this->company);
                $this->flashMessage('Your email has been deleted');
                $this->readFlashMessages();

                // set all reminders  by empty array
            } elseif (isset($this->get['deleteAll'])) {
                $emails = array();
                $reminder = SetEReminders::getNewReminder($emails, $this->company);
                $this->flashMessage('Your emails has been deleted');
                $this->readFlashMessages();

                // get all reminders
            } else {
                $reminder = GetEReminders::getNewReminder($this->company);
            }

            //send request and get responce
            $reminderEmails = $reminder->sendRequest();
            $xml = simplexml_load_string($reminderEmails);

            //check xml and throw exception in case error
            $this->checkXml($xml);
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('view', 'company_id=' . $this->company->getCompanyId());
        }
        $this->template->list = $xml->Body->EReminders->Recipient;
        $this->template->formAdd = $addForm;
    }

    public function checkXml($xml)
    {
        if (isset($xml->GovTalkDetails->GovTalkErrors)) {
            if (isset($xml->GovTalkDetails->GovTalkErrors->Error->Text)) {
                throw new Exception('eReminders unavailable. Usually this is because the authentication code has expired or the company is dissolved.');
            } else {
                throw new Exception('eReminders unavailable. Usually this is because the authentication code has expired or the company is dissolved.');
            }
        }
    }

    /**
     * @param EReminderAddAdminForm $form
     */
    public function Form_reminderFormSubmitted(EReminderAddAdminForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Your email has been added but it must still be activated by clicking the link in the email that Companies House send you and agreeing to the Terms of Operation.');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $this->redirect('CUsetCompanyEReminder', 'company_id=' . $this->company->getCompanyId());
    }

}
