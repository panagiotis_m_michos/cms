<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    UsersAdminControler.php 2009-09-10 divak@gmail.com
 */
class VouchersAdminControler extends NameAdminControler
{

    const PER_PAGE = 10;

    /** @var array */
    protected $tabs = array(
        'list' => 'List',
    );
    private $voucher;

    public function beforeHandle()
    {
        try {
            if (in_array($this->action, ['edit'])) {
                if (isset($this->get['voucherId'])) {
                    $this->voucher = new VoucherNew($this->get['voucherId']);
                } else {
                    throw new Exception('Parameter `voucherId` is required');
                }
            }
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect('list');
        }
        parent::beforeHandle();
    }

    public function renderList()
    {
        set_time_limit(1200);
        Debug::disableProfiler();
        $datasource = dibi::select('*')->from(TBL_VOUCHERS)
                        ->orderBy('dtc')->desc();
        $grid = new VouchersAdminDatagrid($datasource, 'voucherId');
        $this->template->datagrid = $grid->getHtml();
    }

    public function handleCreate()
    {
        //it could take a while to create 10000 vouchers
        set_time_limit(1200);
        $this->tabs['create'] = 'Add Voucher';
        $form = new VoucherAdminForm($this->nodeId . '_' . $this->action);
        $form->startup(new VoucherNew(), array($this, 'Form_createFormSubmitted'));
        $this->template->form = $form;
    }

    public function Form_createFormSubmitted(FForm $form)
    {
        try {
            $form->processCreate();
            $this->flashMessage('Voucher successfully created');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
        }
        $form->clean();
        $this->redirect('list');
    }

    public function handleEdit()
    {
        $this->tabs['edit'] = 'Edit Voucher';
        $form = new VoucherAdminForm($this->voucher->voucherId . '_' . $this->action);
        $form->startup($this->voucher, array($this, 'Form_editFormSubmitted'));
        $this->template->form = $form;
    }

    public function Form_editFormSubmitted(FForm $form)
    {
        try {
            $paginator = $this->createPaginator(VoucherNew::getCount(), self::PER_PAGE);
            $page = $paginator->getCurrentPage();
            $form->processEdit();
            $this->flashMessage('Voucher successfully updated');
            $form->clean();
            $this->redirect('list', 'page=' . $page);
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect('edit', "voucherId={$this->voucher->voucherId}", 'page=' . $page);
        }
    }

    public function handleDelete()
    {
        try {
            $page = NULL;
            if (FApplication::$httpRequest->isMethod('post')) {
                $page = 1;
                $voucherIds = !empty($this->post['voucherId']) ? (array) $this->post['voucherId'] : array();
                $deletedVouchers = 0;
                if ($voucherIds) {
                    foreach ($voucherIds as $voucherId) {
                        $voucher = new VoucherNew($voucherId);
                        $voucher->delete();
                        $deletedVouchers++;
                    }
                } else {
                    throw new Exception('Bad request format!');
                }
                $this->flashMessage('Deleted `' . $deletedVouchers . '` records');
                $this->redirect('list', 'page=' . $page);
            } else {
                throw new Exception('Request type must be post!');
            }
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect('list', 'page=' . $page);
        }
    }

}
