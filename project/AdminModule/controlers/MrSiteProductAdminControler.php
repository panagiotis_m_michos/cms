<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    MrSiteProductAdminControler.php 2010-08-17 divak@gmail.com
 */

class MrSiteProductAdminControler extends ProductAdminControler
{
	/**
	 * @var string
	 */
	static public $handleObject = 'MrSiteProduct';
	
	/**
	 * @var MrSiteProduct
	 */
	public $node;
	
	
	public function beforePrepare()
	{
		parent::beforePrepare();
		$this->templateExtendedFrom[] = 'ProductAdmin';
	}
}