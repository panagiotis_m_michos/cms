<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    ManagementJourneyProductsAdminControler.php 2009-07-17 divak@gmail.com
 */

class ManagementJourneyProductsAdminControler extends PageAdminControler
{
    /** @var array */
    protected $tabs = array(
        'list' => 'List',
    );
    
    
    /**
     * @var JourneyCustomer
     */
    private $journeyCustomer;
    
    
    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'PageAdmin';
        
        if ($this->action === 'view') {
            try {
                if (!isset($this->get['journeyCustomerId'])) { throw new Exception('JourneyCustomerId is required'); 
                }
                $this->journeyCustomer = new JourneyCustomer($this->get['journeyCustomerId']);
                $this->template->journeyCustomer = $this->journeyCustomer;
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
                $this->redirect('list');
            }
        }
    }
    
    
    /*************************************** list ***************************************/
    
    
    public function renderList()
    {
        $form = new FForm('journey_search', 'get');
        $form->addFieldset('Filter');
        $form->addText('fullName', 'Full name');
        $form->addText('email', 'Email');
        $form->addText('phone', 'Phone');
        $form->addSubmit('search', 'Search')->class('btn');
        $form->start();

        // apply filter
        $where = array();
        if ($form->isOk2Save()) {
            $data = $form->getValues();
            if (!empty($data['fullName'])) { $where['fullName'] = $data['fullName']; 
            }
            if (!empty($data['email'])) { $where['email'] = $data['email']; 
            }
            if (!empty($data['phone'])) { $where['phone'] = $data['phone']; 
            } 
        }
        
        $all = JourneyCustomer::getCount($where);
        $paginator = new FPaginator1($all);
        $paginator->itemsPerPage = 25;
        
        $this->template->form = $form;
        $this->template->paginator = $paginator;
        $this->template->customers = JourneyCustomer::getAll($paginator->getLimit(), $paginator->getOffset(), $where);
    }
    
    
    
    /*************************************** view ***************************************/ 
    
    
    
    public function prepareView()
    {
        $this->tabs['view'] = 'View';
    }
    
    
    /**
     * Returns if customer was logged or not
     * @param string $column
     * @return string
     */
    public function isCustomer($column = NULL)
    {
        return $column != NULL ? 'Yes' : 'No';
    }
}
