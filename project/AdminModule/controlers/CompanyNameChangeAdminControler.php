<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    CompanyNameChangeAdminControler.php 2009-03-19 divak@gmail.com
 */

class CompanyNameChangeAdminControler extends ProductAdminControler
{
    /** @var string */
    static public $handleObject = 'CompanyNameChange';
    
    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'ProductAdmin';
    }
}