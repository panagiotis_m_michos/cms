<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS
 * @author 		Nikolai Senkevich, Stan Bazik
 * @internal 	project/controlers/admin/GoogleVisualizationAdminControler.php
 * @created 	28/03/2011
 */
class GoogleVisualizationAdminControler extends PageAdminControler
{

    /** @var array */
    protected $tabs = array(
        'default' => 'Export',
    );

    public function startup()
    {
        // add permission for staff
        $this->acl->allow('staff', 'page', array('view'));
        parent::startup();
    }

    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'PageAdminControler';
    }

    /*     * ***************************************** properties ****************************************** */

    public function renderDefault()
    {

        $form = new FForm('ordersExport');

        $form->addFieldset('Filter');
        $form->addCheckbox('dateSensitive', 'Date sensitive: ', 1)
            ->setValue(1);
        $form->add('DateSelect', 'dtFrom', 'Date From')
            ->setValue(date('Y-m-d', mktime(0, 0, 0, date("m"), date("d")-1, date("Y"))))
            ->setStartYear(date('Y') - 10)
            ->setEndYear(date('Y'));
        $form->add('DateSelect', 'dtTo', 'Date To')
            ->setValue(date('Y-m-d'))
            ->setStartYear(date('Y') - 10)
            ->setEndYear(date('Y'));

        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Submit')->class('btn');

        $form->onValid = array($this, 'Form_defaultFormSubmitted');
        $form->start();
        
        $this->template->form = $form;

        if ($form->isOk2Save()) {
            $formData = $form->getValues();
			
            $data = Order::getExportData($formData['dtFrom'], $formData['dtTo']);
        }else{
            $data = Order::getExportData(date('Y-m-d', mktime(0, 0, 0, date("m"), date("d")-1, date("Y"))), date('Y-m-d'));
        }
        
        //preparing data for visualization
        $i = 0;
        $record = array();
        foreach ($data as $key => $value) {
            $record[] = array($i, 0, $value['productId']);//FNode::getProductById($value['productId'])->page->title);
            $y = (int) substr($value['dtc'], 0, 4);
            $m = (int) substr($value['dtc'], 5, 2);
            $d = (int) substr($value['dtc'], 8, 2);
            $record[] = array($i, 1, 'new Date('.$y.','.$m.','.$d.')');
            $record[] = array($i, 3, (int) $value['totalPrice']);
            $record[] = array($i, 2, (int) Order::getTotalPerProductPerDate(substr($value['dtc'], 0, 4) . '-' . substr($value['dtc'], 5, 2) . '-' . substr($value['dtc'], 8, 2), $value['productId']));
            $i++;
        }
        
        //running visualization
        $v = new QMotionchartGoogleGraph();
        $v->addDrawProperties(
                array(
                    "title" => 'Company Performance',
                )
            )
            ->addColumns(
                array(
                    array('string', 'Title'),
                    array('date', 'Date'),
                    array('number', 'Price'),
                    array('number', 'Qty'),
                )
            )
            ->setValues($record);
        
        $this->template->data = $v->render();
    }

    public function Form_defaultFormSubmitted(FForm $form)
    {
        Debug::disableProfiler();
        $data = $form->getValues();
        $data = Order::getExportData($data['dtFrom'], $data['dtTo']);
        //$current = (array) current($data);

    }

}