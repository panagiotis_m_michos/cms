<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    CFSecretariesAdminControler.php 2011-03-21 razvanp@madesimplegroup.com
 */

class CFSecretariesAdminControler extends PageAdminControler 
{

    /** 
     * @var array 
     */
	protected $tabs = array(
		'page' => 'Page',
		'properties' => 'Properties',
		'layout' => 'Layout',
		'details' => 'Details',
		'menu' => 'Menu',
		'seo' => 'Seo'
	);
    
    /** 
     * @var string 
     */
	protected $templateExtendedFrom = array('BaseAdmin', 'PageAdmin');
    
    /** 
     * @var string 
     */
    static public $handleObject = 'CFSecretariesModel';

    /**
     * @var CFSecretariesModel
     */
    public $node;    
    
    public function renderProperties()
	{
         $form = new CFSecretariesPropertiesAdminForm($this->node->getId() . '_CFSecretariesPropertiesAdminForm');
        $form->startup($this, array($this, 'Form_CFSecretariesPropertiesAdminFormSubmitted'), $this->node);
        $this->template->form = $form;
       
    }

    /**
     * @param CFSecretariesPropertiesAdminForm $form
     */
    public function Form_CFSecretariesPropertiesAdminFormSubmitted(CFSecretariesPropertiesAdminForm $form) {
        try {
            //generate pdf from form and shareholder data
            $form->process();
            $this->flashMessage('Updated!');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $this->redirect();
    }
    
}
