<?php

class WholesalePackageAdminControler extends PackageAdminControler
{
    /** @var string */
    static public $handleObject = 'WholesalePackage';

    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'PackageAdmin';
    }
}
