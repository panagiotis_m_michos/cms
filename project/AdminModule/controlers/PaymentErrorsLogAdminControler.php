<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    ReportsAdminControler.php 2010-01-27 stan, zygi
 */

class PaymentErrorsLogAdminControler extends PageAdminControler
{
	/**
	 * @var string
	 */
	static public $handleObject = 'PaymentModel';
	
	/**
	 * @var PaymentModel
	 */
	public $node;
	
	/** @var array */
	protected $tabs = array(
        //'page' => 'Page',
		'list' => 'List',
		'view' => 'View',
	);
    
    /** @var array */
    public function beforePrepare()
    {
        parent::beforePrepare();
        
        try {
            $this->templateExtendedFrom[] = 'PageAdmin';
            $whitelist = array('list');
            if (!isset($this->get['errorId']) && !in_array($this->action, $whitelist)) {
                throw new Exception('ErrorId is required');
            }
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('list');
        }
    }

    
    /******************************************** List ********************************************/


	public function renderList()
	{
        Debug::disableProfiler();
        $datasource = dibi::select('*')->from(PaymentErrorsLog::TABLE_PAYMENT_ERROR_LOGS)->orderBy('dtc', dibi::DESC);
        $grid = new PaymentErrorsLogDatagrid($datasource, 'errorId');
        $this->template->datagrid = $grid;
        
	}
    
    public function renderView()
    {
        $this->tabs['view'] = array('title'=>'View', 'params'=> "errorId={$this->get['errorId']}");
        try {
            $eroor = $this->node->getPaymentErrorLogById($this->get['errorId']);
            $this->template->error = $eroor;
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('list');
        }
    }        
    
}