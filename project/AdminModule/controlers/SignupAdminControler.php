<?php
/**
 * CMS
 * @category   Project
 * @package    CMS
 * @author     Nikolai Senkevich
 * @version    SignupAdminControler.php 2012-03-14 
 */
class SignupAdminControler extends PageAdminControler
{

    /** 
     * @var string 
     */
    public static $handleObject = 'SignupsModel';
    
    /**
     * @var SignupsModel
     */
    public $node;
    
    /** 
     * @var array 
     */
    protected $tabs = array(
        'page' => 'Signup',
        'startingBusiness' => 'StartingBusiness',
        'savvySurvey' => 'SavvySurvey',
    );
    
    /** 
     * @var string 
     */
    protected $templateExtendedFrom = array('PageAdmin', 'BaseAdmin');


    protected function renderStartingBusiness()
    {
        $form = new SignupsStartingBusinessAdminForm($this->node->getId() . '_startingBusiness');
        $this->node->setParentId(SignupsControler::STARTING_BUSINESS);
        $form->startup($this, $this->node, array($this, 'Form_startingBusinessFormSubmitted'));
        $this->template->form = $form;
    }

    public function Form_startingBusinessFormSubmitted(FForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Data has been updated');
            $this->redirect();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    protected function renderSavvySurvey()
    {
        $form = new SignupsSavvySurveyAdminForm($this->node->getId() . '_SavvySurvey');
        $form->startup($this, $this->node, array($this, 'Form_savvySurveyFormSubmitted'));
        $this->template->form = $form;
    }

    public function Form_savvySurveyFormSubmitted(FForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Data has been updated');
            $this->redirect();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

}
