<?php

class NomineeDirectorAdminControler extends ProductAdminControler
{
    /** @var string */
    static public $handleObject = 'NomineeDirector';

    /** @var array */
    protected $tabs = array(
        'product' => 'Product',
        'properties' => 'Properties',
        'details' => 'Details',
        'seo' => 'Seo'
    );


    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'ProductAdmin';
    }


    public function prepareProduct()
    {
        $form = new FForm($this->nodeId . '_' . $this->action);
        /**
         * @var NomineeDirector $nomineeDirector
         */
        $nomineeDirector = $this->node;
        
        // product
        $form->addFieldset('Product');
        $form->addText('title', 'Title: * ')
            ->addRule(array($this, 'requiredIfCorporate'), 'Title is required!')
            ->size(30)
            ->setValue($nomineeDirector->page->title);
        $form->addText('alternativeTitle', 'Alternative Title:')
            ->size(50)
            ->setValue($nomineeDirector->page->alternativeTitle);
        $form->add('CmsFckArea', 'text', 'Page Text:')
            ->setSize(500, 300)
            ->setValue($nomineeDirector->page->text);

        // price
        $form->addFieldset('Price');
        $form->addText('price', 'Price: *')
            ->size(5)
            ->setValue($nomineeDirector->price)
            ->addRule(FForm::Required, 'Please provide price')
            ->addRule('Numeric', 'Price has to be number');
        $form->addText('productValue', 'Product value: *')
            ->size(5)
            ->setValue($nomineeDirector->productValue)
            ->addRule(FForm::Required, 'Please provide product value')
            ->addRule('Numeric', 'Price value has to be number');
        $form->addText('associatedPrice', 'Associated price: *')
            ->size(5)
            ->setValue($nomineeDirector->associatedPrice)
            ->addRule(FForm::Required, 'Please provide associated price')
            ->addRule('Numeric', 'Associated price value has to be number');
        $form->addText('wholesalePrice', 'Wholesale price: *')
            ->size(5)
            ->setValue($nomineeDirector->wholesalePrice)
            ->addRule(FForm::Required, 'Please provide wholesale price')
            ->addRule('Numeric', 'Wholesale price value has to be number');

        $form->addFieldset('Corporate');
        $form->addCheckbox('isCorporate', 'Use director with corporate details:', 1)
            ->setValue($nomineeDirector->isCorporate() ? 1 : 0);

        if ($nomineeDirector->isCorporate()) {
            // corporate
            $form->addText('corporateCompanyName', 'Company name *')
                ->addRule(array($this, 'requiredIfCorporate'), 'Please provide Company name')
                ->setValue($nomineeDirector->corporateCompanyName)
                ->size(30);
            $form->addText('corporateFirstName', 'First name *')
                ->addRule(array($this, 'requiredIfCorporate'), 'Please provide First name')
                ->setValue($nomineeDirector->corporateFirstName)
                ->size(30);
            $form->addText('corporateLastName', 'Last name *')
                ->addRule(array($this, 'requiredIfCorporate'), 'Please provide Last name')
                ->setValue($nomineeDirector->corporateLastName)
                ->size(30);
            $form->addText('corporateAddress1', 'Address 1 *')
                ->addRule(array($this, 'requiredIfCorporate'), 'Please provide Address 1')
                ->setValue($nomineeDirector->corporateAddress1);
            $form->addText('corporateAddress2', 'Address 3')
                ->setValue($nomineeDirector->corporateAddress2);
            $form->addText('corporateAddress3', 'Address 4')
                ->setValue($nomineeDirector->corporateAddress3);
            $form->addText('corporateTown', 'Town *')
                ->addRule(array($this, 'requiredIfCorporate'), 'Please provide Town')
                ->setValue($nomineeDirector->corporateTown);
            $form->addText('corporateCounty', 'County')
                ->setValue($nomineeDirector->corporateCounty);
            $form->addText('corporatePostcode', 'Postcode *')
                ->addRule(array($this, 'requiredIfCorporate'), 'Please provide Postcode')
                ->setValue($nomineeDirector->corporatePostcode)
                ->size(7);
            $form->addSelect('corporateCountryId', 'Country *', Address::$countries)
                ->addRule(array($this, 'requiredIfCorporate'), 'Please provide Country')
                ->setValue($nomineeDirector->corporateCountryId)
                ->setFirstOption('--- Select ---');
            $form->addText('corporateBirtown', 'Town of birth *')
                ->addRule(array($this, 'requiredIfCorporate'), 'Please provide Town of birth')
                ->setValue($nomineeDirector->corporateBirtown)
                ->size(3);
            $form->addText('corporateAuthTel', 'Telephone *')
                ->addRule(array($this, 'requiredIfCorporate'), 'Please provide Telephone')
                ->setValue($nomineeDirector->corporateAuthTel)
                ->size(3);
            $form->addText('corporateAuthEye', 'Eye colour *')
                ->addRule(array($this, 'requiredIfCorporate'), 'Please provide Eye colour')
                ->setValue($nomineeDirector->corporateAuthEye)
                ->size(3);

            // EEA
            $form->addFieldset('EEA');
            $form->addText('countryRegistered', 'Country registered')
                ->addRule(array($this, 'requiredIfCorporate'), 'Please provide Country registered')
                ->setValue($nomineeDirector->countryRegistered)
                ->size(3);
            $form->addText('registrationNumber', 'Registration Num.')
                ->addRule(array($this, 'requiredIfCorporate'), 'Please provide Registration Number')
                ->setValue($nomineeDirector->registrationNumber)
                ->size(5);
        }

        // person
        $form->addFieldset('Person');
        $form->addText('personFirstname', 'Firstname *')
            ->addRule(FForm::Required, 'Please provide Firstname')
            ->setValue($nomineeDirector->personFirstname);
        $form->addText('personSurname', 'Surname *')
            ->addRule(FForm::Required, 'Please provide Surname')
            ->setValue($nomineeDirector->personSurname);
        $form->addText('personNationality', 'Nationality *')
            ->addRule(FForm::Required, 'Please provide Nationality')
            ->setValue($nomineeDirector->personNationality);
        $form->add('DateSelect', 'personDob', 'DOB *')
            ->setStartYear(1910)
            ->setEndYear(date('Y') - 1)
            ->addRule(FForm::Required, 'Please provide DOB')
            ->setValue($nomineeDirector->personDob);
        $form->addText('personOccupation', 'Occupation *')
            ->addRule(FForm::Required, 'Please provide Occupation')
            ->setValue($nomineeDirector->personOccupation);
        $form->addText('personAddress1', 'Address 1 *')
            ->addRule(FForm::Required, 'Please provide Address 1')
            ->setValue($nomineeDirector->personAddress1);
        $form->addText('personAddress2', 'Address 2')
            ->setValue($nomineeDirector->personAddress2);
        $form->addText('personAddress3', 'Address 3')
            ->setValue($nomineeDirector->personAddress3);
        $form->addText('personTown', 'Town *')
            ->addRule(FForm::Required, 'Please provide Town')
            ->setValue($nomineeDirector->personTown);
        $form->addText('personCounty', 'County')
            ->setValue($nomineeDirector->personCounty);
        $form->addText('personPostcode', 'Postcode *')
            ->addRule(FForm::Required, 'Please provide Postcode')
            ->setValue($nomineeDirector->personPostcode)
            ->size(7);
        $form->addSelect('personCountryId', 'Country *', Address::$countries)
            ->addRule(FForm::Required, 'Please provide Country')
            ->setValue($nomineeDirector->personCountryId)
            ->setFirstOption('--- Select ---');
        $form->addText('personCountryOfResidence', 'Residence country: *')
            ->addRule(FForm::Required, 'Please provide Country of residence!')
            ->setValue($nomineeDirector->personCountryOfResidence);
        $form->addText('personBirtown', 'Town of birth *')
            ->addRule(FForm::Required, 'Please provide Town of birth')
            ->setValue($nomineeDirector->personBirtown)
            ->size(3);
        $form->addText('personAuthTel', 'Telephone number *')
            ->addRule(FForm::Required, 'Please provide Telephone number')
            ->setValue($nomineeDirector->personAuthTel)
            ->size(3);
        $form->addText('personAuthEye', 'Eye colour *')
            ->addRule(FForm::Required, 'Please provide Eye colour')
            ->setValue($nomineeDirector->personAuthEye)
            ->size(3);

        $form->addFieldset('Additional settings');
        $form->addCheckbox('includedByDefault', 'Will be included by default when in packages:', 1)
            ->setValue($nomineeDirector->isIncludedByDefault() ? 1 : 0);

        // action
        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Submit')
            ->onclick('changes = false')
            ->class('btn');
        $form->onValid = array($this, 'packageFormSubmitted');
        $form->start();
        $this->template->form = $form;
    }


    /**
     * @param FForm $form
     * @throws Exception
     */
    public function packageFormSubmitted($form)
    {
        $data = $form->getValues();

        /** @var NomineeDirector $nomineeDirector */
        $nomineeDirector = $this->node;
        $nomineeDirector->page->title = $data['title'];

        $nomineeDirector->page->alternativeTitle = $data['alternativeTitle'];
        $nomineeDirector->page->text = $data['text'];
        $nomineeDirector->price = $data['price'];
        $nomineeDirector->associatedPrice = $data['associatedPrice'];
        $nomineeDirector->wholesalePrice = $data['wholesalePrice'];
        $nomineeDirector->productValue = $data['productValue'];

        $nomineeDirector->setIsCorporate((bool) $data['isCorporate']);
        $nomineeDirector->includeByDefault((bool) $data['includedByDefault']);

        if ($nomineeDirector->isCorporate()) {
            $nomineeDirector->corporateCompanyName = $data['corporateCompanyName'];
            $nomineeDirector->corporateFirstName = $data['corporateFirstName'];
            $nomineeDirector->corporateLastName = $data['corporateLastName'];
            $nomineeDirector->corporateAddress1 = $data['corporateAddress1'];
            $nomineeDirector->corporateAddress2 = $data['corporateAddress2'];
            $nomineeDirector->corporateAddress3 = $data['corporateAddress3'];
            $nomineeDirector->corporateTown = $data['corporateTown'];
            $nomineeDirector->corporateCounty = $data['corporateCounty'];
            $nomineeDirector->corporatePostcode = $data['corporatePostcode'];
            $nomineeDirector->corporateCountryId = $data['corporateCountryId'];
            $nomineeDirector->corporateBirtown = $data['corporateBirtown'];
            $nomineeDirector->corporateAuthTel = $data['corporateAuthTel'];
            $nomineeDirector->corporateAuthEye = $data['corporateAuthEye'];
            $nomineeDirector->countryRegistered = $data['countryRegistered'];
            $nomineeDirector->registrationNumber = $data['registrationNumber'];
        }

        $nomineeDirector->personFirstname = $data['personFirstname'];
        $nomineeDirector->personSurname = $data['personSurname'];
        $nomineeDirector->personNationality = $data['personNationality'];
        $nomineeDirector->personDob = $data['personDob'];
        $nomineeDirector->personOccupation = $data['personOccupation'];
        $nomineeDirector->personAddress1 = $data['personAddress1'];
        $nomineeDirector->personAddress2 = $data['personAddress2'];
        $nomineeDirector->personAddress3 = $data['personAddress3'];
        $nomineeDirector->personTown = $data['personTown'];
        $nomineeDirector->personCounty = $data['personCounty'];
        $nomineeDirector->personCountryOfResidence = $data['personCountryOfResidence'];
        $nomineeDirector->personPostcode = $data['personPostcode'];
        $nomineeDirector->personCountryId = $data['personCountryId'];
        $nomineeDirector->personBirtown = $data['personBirtown'];
        $nomineeDirector->personAuthTel = $data['personAuthTel'];
        $nomineeDirector->personAuthEye = $data['personAuthEye'];

        $nomineeDirector->save();

        $this->flashMessage('Product has been updated');
        $this->redirect(NULL);
    }

    /**
     * @param FControl $control
     * @param string $error
     * @return bool|string
     */
    public function requiredIfCorporate(FControl $control, $error)
    {
        $isCorporate = $control->owner['isCorporate']->getValue();
        $value = $control->getValue();

        if ($isCorporate && empty($value)) {
            return $error;
        }

        return TRUE;
    }
}
