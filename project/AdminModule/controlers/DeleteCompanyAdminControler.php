<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    DeleteCompanyAdminControler.php 2011-05-25 razvanp@madesimplegroup.com
 */

class DeleteCompanyAdminControler extends PageAdminControler
{

    /**
	 * @var string
	 */
	static public $handleObject = 'DeleteCompanyModel';
	
	/**
	 * @var DeleteCompanyModel
	 */
	public $node;

    /**
	 * @var array
	 */
	protected $tabs = array(
		'list' => 'Deleted Companies',
       // 'details' => 'Details',
       // 'seo' => 'Seo'
	);

    /** @var array */
    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'PageAdmin';
    }
    
    /************************************* list *************************************/

    public function  renderList(){
        //die('here');
        $form = new DeleteCompanyAdminForm($this->node->getId() . '_DeleteCompanyAdmin', 'get');
        $form->startup($this, array($this, 'Form_DeleteCompanyAdminFormSubmitted'));
        $this->template->form = $form;
	
//
//        // --- export to CSV ---
//        if (isset($this->get['csv'])) {
//            Debug::disableProfiler();
//            try {
//                $this->node->outputFeedbackToCSV($form);
//            } catch (Exception $e) {
//               $this->flashMessage($e->getMessage(), 'error');
//               $this->redirect('list');
//            }
//		}

        
        $paginator = $this->node->getDeletedCompniesPaginator($form);
        $this->template->paginator = $paginator;
        $deletedCompanies = $this->node->getDeletedCompanies($form, $paginator);
        $this->template->deletedCompanies = $deletedCompanies;

    }
//
    /**
     * @param DeleteCompanyAdminForm $form
     */
    public function Form_DeleteCompanyAdminFormSubmitted(DeleteCompanyAdminForm $form)
    {
        try {
            $form->process();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }
}
