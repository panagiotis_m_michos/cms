<?php

use Datagrid\Admin\ServicesAdminDatagrid;
use Factories\ServiceViewFactory;
use Services\CompanyService;
use Services\ServiceService;
use ServicesAdminModule\Facades\AddServiceFacade;
use ServicesAdminModule\Forms\AddServiceFormData;
use ServicesAdminModule\Forms\AddServiceFormFactory;
use ServiceSettingsModule\Config\DiLocator as ServiceSettingsDiLocator;
use ServiceSettingsModule\Facades\ServiceSettingsFacade;
use ServiceSettingsModule\Services\ServiceSettingsService;

class PscOnlineRegisterAdminControler extends PageAdminControler
{
    const PAGE_LIST = 1427;

    /**
     * @var array
     */
    protected $tabs = [
        'list' => 'Services',
    ];

    /**
     * @var CompanyService
     */
    private $companyService;
    
    private 

    /**
     * @var ServiceService
     */
    private $serviceService;

    /**
     * @var ServiceSettingsService
     */
    private $serviceSettingsService;

    /**
     * @var ServiceSettingsFacade
     */
    private $serviceSettingsFacade;

    /**
     * @var ServiceViewFactory
     */
    private $serviceViewFactory;

    /**
     * @var AddServiceFacade
     */
    private $addServiceFacade;

    /**
     * @var AddServiceFormFactory
     */
    private $addServiceFormFactory;

    public function startup()
    {
        $this->acl->allow('staff', 'page');

        $this->tabs['CompaniesAdminControler::COMPANIES_PAGE view'] = [
            'title' => $this->company->getCompanyName(),
            'params' => "company_id={$companyId}"
        ];
        $this->tabs['companyServicesList'] = [
            'title' => 'Services',
            'params' => "company_id={$companyId}"
        ];
        
        $this->serviceService = $this->getService(DiLocator::SERVICE_SERVICE);
        $this->companyService = $this->getService(DiLocator::SERVICE_COMPANY);
        $this->serviceViewFactory = $this->getService(DiLocator::FACTORY_FRONT_SERVICE_VIEW);
        $this->serviceSettingsService = $this->getService(ServiceSettingsDiLocator::SERVICES_SERVICE_SETTINGS_SERVICE);
        $this->serviceSettingsFacade = $this->getService(ServiceSettingsDiLocator::FACADES_SERVICE_SETTINGS_FACADE);
        $this->addServiceFormFactory = $this->getService('services_admin_module.forms.add_service_form_factory');
        $this->addServiceFacade = $this->getService('services_admin_module.facades.add_service_facade');

        parent::startup();

        $this->handleServiceSettings();
    }

    public function prepareList()
    {
        Debug::disableProfiler();
        $datasource = $this->serviceService->getListDataSource();
        $grid = new ServicesAdminDatagrid(
            $datasource,
            'serviceId',
            $this->serviceViewFactory
        );
        $this->template->services = $grid->getHtml();
    }

    public function prepareCompanyServicesList()
    {
        $companyId = $this->getParameter('company_id');
        if (!$companyId) {
            $this->redirect('CompaniesAdminControler::COMPANIES_PAGE list');
        }
        $company = $this->companyService->getCompanyById($companyId);
        if (!$company) {
            $this->redirect('CompaniesAdminControler::COMPANIES_PAGE list');
        }

        $this->tabs['CompaniesAdminControler::COMPANIES_PAGE view'] = [
            'title' => $company->getCompanyName(),
            'params' => "company_id={$companyId}"
        ];
        $this->tabs['companyServicesList'] = [
            'title' => 'Services',
            'params' => "company_id={$companyId}"
        ];

        $dataSource = $this->serviceService->getListDataSourceForCompany($company);
        $grid = new ServicesAdminDatagrid(
            $dataSource,
            'serviceId',
            $this->serviceViewFactory
        );

        $this->template->services = $grid->getHtml();
        $this->template->company = $company;
    }

    public function prepareAddService()
    {
        $companyId = $this->getParameter('company_id');
        if (!$companyId) {
            $this->redirect('CompaniesAdminControler::COMPANIES_PAGE list');
        }
        $company = $this->companyService->getCompanyById($companyId);
        if (!$company || $company->isDissolved()) {
            $this->redirect('CompaniesAdminControler::COMPANIES_PAGE list');
        }

        $this->tabs += [
            'CompaniesAdminControler::COMPANIES_PAGE view' => [
                'title' => $company->getCompanyName(),
                'params' => "company_id={$companyId}",
            ],
            'companyServicesList' => [
                'title' => 'Services',
                'params' => "company_id={$companyId}",
            ],
            'addService' => [
                'title' => 'Add Service',
                'params' => "company_id={$companyId}",
            ],
        ];

        $addServiceFormData = new AddServiceFormData;

        $form = $this->createForm(
            $this->addServiceFormFactory->create($company),
            $addServiceFormData
        );

        if ($form->get('add')->isClicked() && $form->isValid()) {
            $this->addServiceFacade->addService($addServiceFormData, $company);
            $this->flashMessage('Service successfully added', 'success');
            $this->redirect('companyServicesList', ['company_id' => $companyId]);
        }

        $this->template->form = $form->createView();
    }

    private function handleServiceSettings()
    {
        $reminders = $this->getParameter('reminders');
        $renewal = $this->getParameter('renewal');
        $serviceId = $this->getParameter('service_id');
        $companyId = $this->getParameter('company_id');

        if (!$serviceId || !$companyId) {
            return;
        }

        $service = $this->serviceService->getServiceById($serviceId);
        $settings = $this->serviceSettingsService->getSettingsByService($service);

        if ($reminders == 'enable') {
            $this->serviceSettingsFacade->enableEmailReminders($settings);
        } elseif ($reminders == 'disable') {
            $this->serviceSettingsFacade->disableEmailReminders($settings);
        }

        if ($renewal == 'enable') {
            $token = $service->getCompany()->getCustomer()->getActiveToken();
            $this->serviceSettingsFacade->enableAutoRenewal($settings, $token);
        } elseif ($renewal == 'disable') {
            $this->serviceSettingsFacade->disableAutoRenewal($settings);
        }

        if ($reminders || $renewal) {
            $this->redirect('companyServicesList', ['company_id' => $companyId]);
        }
    }
}
