<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    JourneyProductAdminControler.php 2009-03-19 divak@gmail.com
 */

class JourneyProductAdminControler extends PageAdminControler
{
    /** @var string */
    static public $handleObject = 'JourneyProduct';
    
    /** 
     * @var JourneyProduct 
     */
    public $node;
    
    /** @var array */
    protected $tabs = array(
        'product' => 'Product',
        'details' => 'Details',
        'seo' => 'Seo'
    );
    
    
    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'PageAdmin';    
    }
    
    
    public function prepareProduct()
    {
        $form = new FForm($this->nodeId.'_'.$this->action);
        
        // fieldset
        $form->addFieldset('Package');
        $form->addText('title', 'Title: * ')
            ->addRule(FForm::Required, 'Title is required!')
            ->size(30)
            ->setValue($this->node->page->title);
        $form->addText('alternativeTitle', 'Alternative Title:')
            ->size(50)
            ->setValue($this->node->page->alternativeTitle);
        $form->add('CmsFckArea', 'text', 'Page Text:')
            ->setSize(500, 300)
            ->setValue($this->node->page->text);
            
        // other
        $form->addFieldset('Other');
        $form->addArea('emails', 'Emails: ')->cols(50)->rows(2)
            ->setValue($this->node->emails)
            ->setDescription('(Comma separated)')
            ->addRule(array($this, 'Validator_CommaEmails'), 'Emails are not valid!');
        $form->addArea('emailText', 'Email Text: ')
            ->cols(60)->rows(25)
            ->setValue($this->node->emailText);
        $form->addCheckbox('sendEmailInstantly', 'Send Email Instantly:', 1)
            ->setDescription('(Sends an email every time a user ticks this product on the Journey Page.)')
            ->setValue($this->node->sendEmailInstantly);
        $form->addCheckbox('sendCsvDaily', 'Send CSV Daily:', 1)
            ->setDescription('(Sends 1 email per day with a CSV of all leads for that day.)')
            ->setValue($this->node->sendCsvDaily);
            
        $form->addFieldset('Related Products');
        $form->add('AsmSelect', 'relatedProducts', 'Related Products:')
            ->multiple()
            ->title('--- Select --')
            ->setValue($this->node->relatedProducts)
            ->setOptions(BasketProduct::getAllProducts());

        $form->addFieldset('Related Products');
        $form->add('AsmSelect', 'blacklistedProducts', 'Blacklisted Products:')
            ->multiple()
            ->title('--- Select --')
            ->setValue($this->node->blacklistedProducts)
            ->setOptions(BasketProduct::getAllProducts());
          
        $form->addFieldset('Questions Per Product');
        $form->add('TextAreaTextCombo', 'questions', NULL)
            ->setValue2($this->node->question);
    
        $form->addFieldset('Image');
        $form->add('CmsImg', 'imageId', 'Image')
            ->setNodeId(614)
            ->setValue($this->node->imageId);
        
        // action
        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Submit')
            ->onclick('changes = false')
            ->class('btn');
        $form->onValid = array($this, 'packageFormSubmitted');
        $form->start();
        $this->template->form = $form;
    }
    
    
    public function packageFormSubmitted($form)
    {
        $data = $form->getValues();
        
        $this->node->page->title = $data['title'];
        $this->node->page->alternativeTitle = $data['alternativeTitle'];
        $this->node->page->text = $data['text'];
        $this->node->emails = $data['emails'];
        $this->node->emailText = $data['emailText'];
        $this->node->relatedProducts = $data['relatedProducts'];
        $this->node->blacklistedProducts = $data['blacklistedProducts'];
        $this->node->sendEmailInstantly = $data['sendEmailInstantly'];
        $this->node->sendCsvDaily = $data['sendCsvDaily'];
        if ($data['imageId'] != NULL) { $this->node->imageId = $data['imageId']; 
        }
        $this->node->question = $data['questions'];
        $this->node->save();
        
        $form->clean();
        $this->flashMessage('Product has been updated');
        $this->redirect(NULL);
    }    
    
    
    
    /**
     * Validator
     * @param object $control
     * @param mixed $error
     * @param mixed $params
     * @return mixed
     */
    public function Form_validProduct($control, $error, $params)
    {
        if ($control->getValue() != NULL) {
            $product = new Product($control->getValue());
            if ($product->isOk2show() == FALSE) {
                return $error;
            }
        }
        return TRUE;
    }
    
    
    public function Validator_CommaEmails($control, $error, $params)
    {
        $value = $control->getValue();
        if (!empty($value)) {
            $validator = new Email($error, $params, $control->owner, $control);
            $emails = explode(',', $value);
            foreach ($emails as $key=>$email) {
                $isValid = $validator->isValid(trim($email));
                if ($isValid == FALSE) {
                    return $error; 
                }
            }
        }
        return TRUE;
    }
    
}
