<?php

/**
 * cms
 *
 * @license    GPL
 * @category   Project
 * @package    cms
 * @author     Razvan Preda <razvanp@madesimplegroup.com>
 * @version    PaymentNewCustomerAdminControler.php 18-Sep-2012 
 */

interface PaymentPropertiesFormDelegate 
{
    function PaymentPropertiesFormSuccess();
    function PaymentPropertiesFormCancelled();
    function PaymentPropertiesFormWithException(Exception $e);
}


class PaymentNewCustomerAdminControler extends PageAdminControler implements PaymentPropertiesFormDelegate
{
    /**
	 * @var string
	 */
	static public $handleObject = 'PaymentModel';
	
	/**
	 * @var PaymentModel
	 */
	public $node;
	
    protected $templateExtendedFrom = array('PageAdmin', 'BaseAdmin');
    
    
	/** @var array */
	protected $tabs = array(
		'page' => 'Page',
		'layout' => 'Layout',
        'properties' => 'Properties',
		'details' => 'Details',
		'seo' => 'Seo'
	);
    
    /** @var array */
    public function beforePrepare()
    {
        parent::beforePrepare();
    }
    
    /******************************************** Properties ********************************************/


	public function renderProperties()
	{
        //pr($this->node);
        $form = new PaymentPropertiesForm($this->node->getId() . '_paymentProperties');
        $form->startup($this, $this->node);
        $this->template->form = $form;
	}
    
    function PaymentPropertiesFormSuccess()
    {
        $this->flashMessage('Update successfully!');
        $this->redirect('properties');
    }
    
    /**
     * @return void
     */
    function PaymentPropertiesFormCancelled()
    {
        $this->flashMessage('Update cancelled!');
        $this->redirect('page');
    }
    
    /**
     * @return void
     */
    function PaymentPropertiesFormWithException(Exception $e)
    {
        $this->flashMessage($e->getMessage());
        $this->redirect('properties');
    }
    
    
}
