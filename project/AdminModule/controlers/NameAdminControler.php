<?php

abstract class NameAdminControler extends PageAdminControler
{
	/**
     * @var array
     */
	protected $tabs = array('name' => 'Name', 'seo' => 'Seo');


    /**
     * @return void
     */
    public function  beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'PageAdmin';
    }

    
	public function handleName()
	{
		$form = new FForm($this->nodeId.'_'.$this->action);
		
		// fieldset
		$form->addFieldset('Name');
		$form->addText('title','Title: * ')
			->setDescription('(Page title and name in menu)')
			->addRule(FForm::Required,'Title is required!')
			->size(30)
			->setValue($this->node->page->title);
		$form->addText('alternativeTitle','Alternative Title:')
			->setDescription('(Title for page. Is it empty, title will be used)')
			->size(50)
			->setValue($this->node->page->alternativeTitle);
		
		// action
		$form->addFieldset('Action');
		$form->addSubmit('submit','Submit')
			->onclick('changes = false')
			->class('btn');
		$form->onValid = array($this, 'Form_nameFormSubmitted');
		
		$form->start();
		$this->template->form = $form;
	}
	
	
	public function Form_nameFormSubmitted($form)
	{
		$this->node->page->title = $form->getValue('title');
		$this->node->page->alternativeTitle = $form->getValue('alternativeTitle');
		$this->node->page->save();
		$this->flashMessage('Data has been updated');
		$this->redirect(NULL);
	}
}
