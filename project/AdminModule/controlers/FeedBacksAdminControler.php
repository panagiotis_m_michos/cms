<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    FeedbacksForm.php 2011-03-21 razvanp@madesimplegroup.com
 */

class FeedBacksAdminControler extends PageAdminControler
{

    /**
	 * @var string
	 */
	static public $handleObject = 'FeedBacksModel';
	
	/**
	 * @var FeedBacksModel
	 */
	public $node;

    /**
	 * @var array
	 */
	protected $tabs = array(
		'list' => 'FeedBack',
        'details' => 'Details',
        'seo' => 'Seo'
	);

    /** @var array */
    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'PageAdmin';
    }
    
    /************************************* list *************************************/

    public function  renderList(){
        $form = new FeedBackAdminForm($this->node->getId() . '_Feedback', 'get');
        $form->startup($this, array($this, 'Form_FeedbackFormSubmitted'));
        $this->template->form = $form;

        // --- export to CSV ---
        if (isset($this->get['csv'])) {
            Debug::disableProfiler();
            try {
                $this->node->outputFeedbackToCSV($form);
            } catch (Exception $e) {
               $this->flashMessage($e->getMessage(), 'error');
               $this->redirect('list');
            }
		}
        $paginator = $this->node->getFeebackPaginator($form);
		$this->template->paginator = $paginator;
        $feedbacks = $this->node->getFeedbacks($form, $paginator);
        $this->template->feedbacks = $feedbacks;
    }

    /**
     * @param FeedBackAdminForm $form
     */
    public function Form_FeedbackFormSubmitted(FeedBackAdminForm $form)
    {
        try {
            $form->process();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }
}
