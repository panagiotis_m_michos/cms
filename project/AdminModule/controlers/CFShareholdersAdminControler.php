<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    CFShareholdersAdminControler.php 2011-03-21 razvanp@madesimplegroup.com
 */

class CFShareholdersAdminControler extends PageAdminControler 
{

    /** 
     * @var array 
     */
	protected $tabs = array(
		'page' => 'Page',
		'properties' => 'Properties',
		'layout' => 'Layout',
		'details' => 'Details',
		'menu' => 'Menu',
		'seo' => 'Seo'
	);
    
    /** 
     * @var string 
     */
	protected $templateExtendedFrom = array('BaseAdmin', 'PageAdmin');
    
    /** 
     * @var string 
     */
    static public $handleObject = 'CFShareholdersModel';

    /**
     * @var CFShareholdersModel
     */
    public $node;    
    
    public function renderProperties()
	{
         $form = new CFShareholdersPropertiesAdminForm($this->node->getId() . '_CFShareholdersPropertiesAdminForm');
        $form->startup($this, array($this, 'Form_CFShareholdersPropertiesAdminFormSubmitted'), $this->node);
        $this->template->form = $form;
       
    }

    /**
     * @param CFShareholdersPropertiesAdminForm $form
     */
    public function Form_CFShareholdersPropertiesAdminFormSubmitted(CFShareholdersPropertiesAdminForm $form) {
        try {
            //generate pdf from form and shareholder data
            $form->process();
            $this->flashMessage('Updated!');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $this->redirect();
    }
    
}
