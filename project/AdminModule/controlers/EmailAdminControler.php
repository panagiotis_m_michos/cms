<?php

class EmailAdminControler extends PageAdminControler
{
    const ATTACHMENTS_COUNT = 5;
    const ATTACHMENT_FILE_PAGE = 575;

    /** @var string */
    public static $handleObject = 'FEmail';

    /** @var array */
    protected $tabs = array(
        'default' => 'Email',
    );

    protected function prepareDefault()
    {
        $form = new FForm($this->nodeId . '_' . $this->action);

        $form->addFieldset('Page:');
        $form->addText('title', 'Title: *')
            ->addRule(FForm::Required, 'Please provide title.')
            ->size(40)
            ->setValue($this->node->page->title);

        $form->addFieldset('Address');
        $form->addSelect('from', 'From: ', $this->getFromOptions())
            ->setFirstOption('--- Select ---')
            ->addRule(FForm::Required, 'Please select email address')
            ->setValue($this->node->from);
        $form->addArea('to', 'To: ')->cols(50)->rows(2)
            ->setValue($this->node->to)
            ->setDescription('(Comma separated)')
            ->addRule(array($this, 'Validator_CommaEmails'), 'Emails are not valid!');
        $form->addArea('cc', 'Cc: ')->cols(50)->rows(2)
            ->setValue($this->node->cc)
            ->setDescription('(Comma separated)')
            ->addRule(array($this, 'Validator_CommaEmails'), 'Emails are not valid!');
        $form->addArea('bcc', 'Bcc: ')->cols(50)->rows(2)
            ->setValue($this->node->bcc)
            ->setDescription('(Comma separated)')
            ->addRule(array($this, 'Validator_CommaEmails'), 'Emails are not valid!');

        $form->addFieldset('Email');
        $form->addText('subject', 'Subject: ')->size(60)
            ->setValue($this->node->subject)
            ->addRule(FForm::Required, 'Please provide subject.');
        $form->add('CmsFckArea', 'body', 'Body:')
            ->setSize(500, 300)
            ->setValue($this->node->getLngText());

        $form->addFieldset('Attachment');
        for ($i = 0; $i < self::ATTACHMENTS_COUNT; $i++) {
            $form->add('CmsFile', 'attachment' . $i, 'Attachment ' . ($i + 1) . ':')
                ->setNodeId(self::ATTACHMENT_FILE_PAGE);
            //set value
            if (isset($this->node->cmsFileAttachments[$i])) {
                $fileId = $this->node->cmsFileAttachments[$i];
                $form['attachment' . $i]->setValue($fileId);
            }
        }

        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Submit')
            ->onclick('changes = false')
            ->class('btn');

        $form->onValid = array($this, 'Form_defaultFormSubmitted');
        $form->start();
        $this->template->form = $form;
    }

    /**
     * @param FForm $form
     */
    public function Form_defaultFormSubmitted(FForm $form)
    {
        $data = $form->getValues();

        $this->node->page->title = $data['title'];
        $this->node->from = $data['from'];
        $this->node->fromName = $this->getFromName($data['from']);
        $this->node->setTo($data['to']);
        $this->node->setCc($data['cc']);
        $this->node->setBcc($data['bcc']);
        $this->node->subject = $data['subject'];
        $this->node->page->text = $data['body'];

        // attachments
        $this->node->cmsFileAttachments = array();
        for ($i = 0; $i < self::ATTACHMENTS_COUNT; $i++) {
            if (!empty($data['attachment' . $i])) {
                $this->node->addCmsFileAttachment($data['attachment' . $i]);
            }
        }

        $this->node->save();
        $this->flashMessage('Data has been updated');
        $this->redirect();
    }

    /**
     * @param FControl $control
     * @param string $error
     * @param array $params
     * @return bool|string
     */
    public function Validator_CommaEmails($control, $error, $params)
    {
        $value = $control->getValue();
        if (!empty($value)) {
            $validator = new Email($error, $params, $control->owner, $control);
            $emails = explode(',', $value);
            $i = 1;
            foreach ($emails as $key => $email) {
                $isValid = $validator->isValid(trim($email));
                if ($isValid == FALSE) {
                    return $error;
                }
                if (empty($email)) {
                    return 'Email: ' . $i . ' are empty (remove comma after email)';
                }
                $i++;
            }
        }
        return TRUE;
    }

    /**
     * @return array
     */
    protected function getFromEmails()
    {
        return FApplication::$config['emails']['from'];
    }

    /**
     * @param string $email
     * @return string
     */
    private function getFromName($email)
    {
        $emails = $this->getFromEmails();

        return $emails[$email];
    }

    /**
     * @return array
     */
    private function getFromOptions()
    {
        $emails = array_keys($this->getFromEmails());

        return array_combine($emails, $emails);
    }
}
