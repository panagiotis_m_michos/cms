<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    AffiliatesAdminControler.php 2009-11-11 divak@gmail.com
 */

class AffiliatesAdminControler extends PageAdminControler
{
	
	const PAGE = 608;
	
	/**
	 * @var string
	 */
	static public $handleObject = 'AffiliatesModel';
	
	/**
	 * @var AffiliatesModel
	 */
	public $node;
	
	/**
	 * @var array
	 */
	protected $tabs = array(
		'list' => 'Affiliates',
	);
	
	/** 
     * @var Affiliate 
     */
	protected $affiliate;
	
	
	/**
	 * Checks compulsory affiliateId on some actions
	 * and will create Affiliate object
	 *
	 * @return void 
	 */
	public function beforePrepare()
	{
		parent::beforePrepare();
		try {
			if (isset($this->get['affiliateId'])) {
				$this->node->startup($this->get['affiliateId']);
				$this->template->affiliate = $this->node->affiliate;
				$this->template->affiliateId = $this->node->affiliate->getId();
			}
			else if (!in_array($this->action, array('list', 'report', 'create', 'editPage', 'importMarkUps'))) {
				throw new Exception('AffiliateId is required');
			}

		} catch (Exception $e) {
			$this->flashMessage($e->getMessage(), 'error');
			$this->redirect('list');
		}
	}
	
	
	/************************************* list *************************************/
	
	
	public function renderList()
	{       
        $datasource = dibi::select('affiliateId, name, web, email, dtc')->from(TBL_AFFILIATES)->orderBy('dtc', dibi::DESC);
        $grid = new AffiliatesAdminDatagrid($datasource, 'affiliateId');
        $this->template->affiliates2 = $grid->getHtml();
	}
	
	
	/************************************* view *************************************/

	
	public function prepareView()
	{
		try {
			$this->tabs['view'] = array('title'=>$this->node->affiliate->name, 'params'=>'affiliateId='.$this->get['affiliateId']);
		} catch (Exception $e) {
			$this->flashMessage($e->getMessage());
			$this->redirect('list');
		}
	}
	
	
	/************************************* affiliate customers *************************************/

	
	
	public function prepareAffiliateCustomers()
	{
		// --- tabs ---
		$this->tabs['view'] = array('title'=>$this->node->affiliate->name, 'params'=>'affiliateId='.$this->get['affiliateId']);
		$this->tabs['affiliateCustomers'] = array('title' => 'Customers', 'params'=> "affiliateId={$this->get['affiliateId']}");
	}
	
	public function handleAffiliateCustomers()
	{
		Debug::disableProfiler();
		if (isset($this->get['csv'])) {
			$this->node->outputAffiliateCustomersCSV();
		}
	}
	
	
	public function renderAffiliateCustomers()
	{
		try {
			$paginator = $this->node->getAffiliateCustomersPaginator();
			$this->template->paginator = $paginator;
			$this->template->customers = $this->node->getAffiliateCustomers($paginator);
		} catch (Exception $e) {
		    $this->flashMessage($e->getMessage(), 'error');
		 	$this->redirect('list');
		}
	}
	
	
	
	/************************************* affiliate orders *************************************/
	

	
	public function prepareAffiliateOrders()
	{
		// --- tabs ---
		$this->tabs['view'] = array('title'=>$this->node->affiliate->name, 'params'=>'affiliateId='.$this->get['affiliateId']);
		$this->tabs['affiliateOrders'] = array('title' => 'Orders', 'params'=> "affiliateId={$this->get['affiliateId']}");
	}
	
	
	public function renderAffiliateOrders()
	{
		try {
			$filter = $this->node->getComponentOrdersFilterForm();
			$this->template->form = $filter;
			
			// get CSV 
			if (isset($this->get['csv'])) {
				Debug::disableProfiler();
				$this->node->outputAffiliateOrdersCSV($filter);
			}
			
			$paginator = $this->node->getAffiliateOrdersPaginator($filter);
			$this->template->paginator = $paginator;
			
			$this->template->orders = $this->node->getAffiliateOrders($filter, $paginator);
		} catch (Exception $e) {
		    $this->flashMessage($e->getMessage(), 'error');
		 	$this->redirect('list');
		}
	}
	
	
	/************************************* create *************************************/
	
	
	public function prepareCreate()
	{
		Debug::disableProfiler();
		$this->template->form = $this->node->getComponentAffiliateForm(array($this, 'Form_createFormSubmitted'), 'create');
	}
	
	
	public function Form_createFormSubmitted(FForm $form)
	{
		try {
			$affiliateId = $this->node->processSaveAffiliate($form, 'create');
			$this->flashMessage('Affiliate has been created');
			
			$url = FApplication::$router->link('view', "affiliateId=$affiliateId");
			$this->reloadOpener($url);
		} catch (Exception $e) {
			$this->flashMessage($e->getMessage(), 'error');
			$this->redirect('list');
		}
	}
	
	
	
	/************************************* edit *************************************/
	
	
	public function renderEdit()
	{
		Debug::disableProfiler();
		$this->template->title = 'Affiliate Edit';
		$this->template->form = $this->node->getComponentAffiliateForm(array($this, 'Form_editFormSubmitted'), 'edit');
	}
	
	
	public function Form_editFormSubmitted(FForm $form)
	{
		try {
			$this->node->processSaveAffiliate($form, 'edit');
			$this->flashMessage('Affiliate has been modified');
			$this->reloadOpener();
		} catch (Exception $e) {
			$this->flashMessage($e->getMessage(), 'error');
			$this->reloadOpener();
		}
	}
	
	
	/************************************* delete *************************************/
	
	
	public function handleDelete()
	{
		try {
			$this->affiliate->delete();
			$this->flashMessage('Affiliate has been deleted');
		} catch (Exception $e) {
			$this->flashMessage($e->getMessage(), 'error');
		}
		$this->redirect('list');
	}
	
	
	/************************************* report *************************************/
	
	
	public function prepareReport()
	{
		// form 
		$form = $this->node->getComponentReportFilterForm();
		$report = Affiliate::getAffiliatesReport($form->getValue('dtFrom'), $form->getValue('dtTill'));
		$form->clean();
		
		// get CSV 
		if (isset($this->get['csv'])) Affiliate::getCSVAffiliatesReport($report);
		
		$this->template->form = $form;
		$this->template->report = $report;
		$this->tabs['report'] = 'Report';
	}
	
	
	
	/************************************* affiliate report *************************************/
	
	
	public function prepareAffiliateReport()
	{
		// form 
		$form = $this->node->getComponentReportFilterForm();
		$report = Affiliate::getAffiliateReport($this->node->affiliate->getId(), $form->getValue('dtFrom'), $form->getValue('dtTill'));
		$form->clean();
		
		// get CSV 
		if (isset($this->get['csv'])) Affiliate::getCSVAffiliateReport($report);
		
		// tabs
		$this->tabs['view'] = array('title'=>$this->node->affiliate->name, 'params'=>'affiliateId='.$this->get['affiliateId']);
		$this->tabs['affiliateReport'] = array('title'=>'Report', 'params'=>'affiliateId='.$this->get['affiliateId']);
		
		$this->template->report = $report;
		$this->template->form = $form;
	}
	
	public function prepareAffiliateProducts()
	{
		// --- tabs ---
		$this->tabs['view'] = array('title'=>$this->node->affiliate->name, 'params'=>'affiliateId='.$this->get['affiliateId']);
		$this->tabs['affiliateProducts'] = array('title' => 'Mark Ups', 'params'=> "affiliateId={$this->get['affiliateId']}");
		
		$form = new AffiliateProductForm('affiliateProduct');
		$form->startup($this, array($this, 'Form_productsEditFormSubmitted'), $this->node->affiliate);
		$this->template->form = $form;
	}
	
	public function renderAffiliateProducts()
	{
		$paginator = new FPaginator2(AffiliateProduct::getCount(array('affiliateId' => $this->get['affiliateId'])));
		$this->template->affiliateProducts = AffiliateProduct::getAffiliateProducts($this->get['affiliateId']);
		$this->template->affiliateId = $this->get['affiliateId'];
		$paginator->htmlDisplayEnabled = FALSE;
		$this->template->paginator = $paginator;
	
	}
	
	public function Form_productsEditFormSubmitted(FForm $form)
	{
		try {
			$form->process();
			$this->flashMessage('Affiliate mark ups updated');
			$this->redirect();
		} catch (Exception $e) {
			$this->flashMessage($e->getMessage(), 'error');
			$this->redirect();
		}
	}
	
	public function handleDeleteProduct()
	{
		try {
			if (!isset($this->get['affiliateProductId'])) {
				throw new Exception('AffiliateProductId is required!');
			}
			$affiliateProduct = new AffiliateProduct($this->get['affiliateProductId']);
			$affiliateProduct->delete();
			$this->flashMessage('Affiliate product mark up has been deleted');
		} catch (Exception $e) {
			$this->flashMessage($e->getMessage(), 'error');
		}
		$this->redirect('affiliateProducts', 'affiliateId=' . $this->get['affiliateId']);

	}
	
//	public function handleImportMarkUps() {
//		$affiliates = Affiliate::getAll();
//		foreach ($affiliates as $affiliate) {
//			$packagesIds = explode('|', $affiliate['packagesIds']);
//			$packagesIds = array_filter((array)$packagesIds);
//			if (!empty($packagesIds)) {
//				foreach ($packagesIds as $productId) {
//					$affP = new AffiliateProduct();
//					$affP->affiliateId = $affiliate->affiliateId;
//					$affP->productId = $productId;
//					$affP->markUp = 0;
//					$affP->save();
//				}
//			}
//		}
//		die;
//	}
	
}
