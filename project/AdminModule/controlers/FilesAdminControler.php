<?php
class FilesAdminControler extends PageAdminControler
{
    /** @var string */
    static public $icon = 'files.png';
    
    /** @var array */
    protected $tabs = array(
        'list' => 'List',
    );

    /** @var FFile */
    private $file;
    
    
    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'PageAdmin';
        
        // file
        if (isset($this->get['file_id'])) {
            try {
                $this->file = new FFile($this->get['file_id']);
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
                $this->redirect('list');
            }
        }
    }
    
    
    /******************************* list ***********************************/
    
    
    public function prepareList()
    {
        // where condtion
        $where = array('nodeId' => $this->nodeId, 'isDeleted' => 0);
        
        // paginator
        $count = FFile::getCount($where);
        $paginator = new FPaginator2($count);
        $paginator->itemsPerPage = 25;
        $paginator->htmlDisplayEnabled = FALSE;
        $this->template->paginator = $paginator;
        
        $this->template->files = FFile::getAll($where, $paginator->getLimit(), $paginator->getOffset());
    }
    
    
    /******************************* insert ***********************************/
    
    
    public function prepareInsert()
    {
        // add tab
        $this->tabs['insert'] = 'Insert';
        $form = new FForm('insertFile');
        
        //file
        $form->addFieldset('File');
        $form->addFile('file', 'File: *')
            ->addRule(FForm::Required, 'Please provide file');
        
        // data
        $form->addFieldset('Data');
        $form->addText('name', 'Name:')
            ->size(40)
            ->setDescription('(Only plain name, without extension.)');
        $form->addSelect('node_id', 'Node: ', FMenu::getSelect())
            ->addRule(array($this, 'Validator_checkFileNode'), 'You have to choose files page!')
            ->setValue($this->nodeId);
        $form->addText('ord', 'Ord:')
            ->addRule(FForm::NUMERIC, 'Ord have to be numeric!');
        $form->addCheckbox('is_active', 'Active', 1);
        
        // action
        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Submit')
            ->class('btn');
            
        $form->onValid = array($this, 'Form_insertFormSubmitted');
        $form->start();
        $this->template->form = $form;
    }
    
    public function Form_insertFormSubmitted(FForm $form)
    {
        $data = $form->getValues();
        $file = new FFile();
        
        $file->uploadedFile = $data['file'];
        $file->name = $data['name'];
        $file->nodeId = $data['node_id'];
        $file->ord = $data['ord'];
        $file->isActive = $data['is_active'];
        $file->save();
        
        $form->clean();
        $this->flashMessage('File has been created');
        $this->redirect('list');
    }
    
    
    /******************************* edit ***********************************/
    
    
    public function prepareEdit()
    {
        if (!isset($this->get['file_id'])) {
            $this->redirect('list');
        }
        $this->tabs['edit'] = 'Edit';
    }
    
    
    public function renderEdit()
    {
        $form = new FForm('editFile');
        
        //file
        $form->addFieldset('Current file');
        $form->addFile('file', 'File: ', FFile::getStorageDir(TRUE))
            ->setValue($this->file->getFileName());
        
        // data
        $form->addFieldset('Data');
        $form->addFile('newFile', 'new File:');
        $form->addText('name', 'Name:')
            ->size(40)
            ->setDescription('(Only plain name, without extension.)');
        $form->addSelect('nodeId', 'Node: ', FMenu::getSelect())
            ->addRule(array($this, 'Validator_checkFileNode'), 'You have to choose files page!')
            ->setValue($this->nodeId)
            ->readonly();
        $form->addText('ord', 'Ord:')
            ->addRule(FForm::NUMERIC, 'Ord have to be numeric!');
        $form->addCheckbox('isActive', 'Active', 1);
        
        // action
        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Submit')
            ->class('btn');
            
        $form->setInitValues((array) $this->file);
        $form->onValid = array($this, 'Form_editFormSubmitted');
        $form->start();
        $this->template->form = $form;
    }
    
    
    public function Form_editFormSubmitted(FForm $form)
    {
        $data = $form->getValues();
        
        if ($data['newFile'] !== NULL) {
            $this->file->uploadedFile = $data['newFile'];
        }
        
        $this->file->name = $data['name'];
        $this->file->nodeId = $data['nodeId'];
        $this->file->ord = $data['ord'];
        $this->file->isActive = $data['isActive'];
        $this->file->save();
        
        $form->clean();
        $this->flashMessage('File has been updated');
        $this->redirect('list');
    }
    
    
    /******************************* delete ***********************************/
    
    
    public function handleDelete()
    {
        if (!isset($this->get['file_id'])) {
            $this->redirect('list');
        }
        
        try {
            $this->file->delete();
            $this->flashMessage('File has been deleted');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());    
        }
        $this->redirect('list');
    }
    
    
    /******************************* other ***********************************/
    
        
    /**
     * Checks if node for file is FilesAdminControler or extended from FilesAdminControler
     * @param Select $control
     * @param string $error
     * @param array $params
     * @return mixed
     */
    public function Validator_checkFileNode($control, $error, $params)
    {
        $nodeId = $control->getValue();
        $node = new FNode($nodeId);
        
        $controler = new $node->adminControler;
        if ($controler instanceof FilesAdminControler) {
            return TRUE;
        } else {
            return $error;
        }
    }
}
