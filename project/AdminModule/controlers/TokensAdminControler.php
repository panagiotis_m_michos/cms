<?php

/**
 * CMS
 * @category   Project
 * @package    CMS
 * @author     Nikolai Senkevich
 * @version    TokensAdminControler.php 2012-06-11 
 */
class TokensAdminControler extends PageAdminControler
{
    const TOKENS_PAGE = 1239;

    /**
     * @var string
     */
    static public $handleObject = 'TokensModel';
    /**
     * @var TokensModel
     */
    public $node;
    
    /** @var array */
    protected $tabs = array(
        'list' => 'Tokens',
    );

//    public function startup()
//	{
//		// add permission for staff
//		$this->acl->allow('staff', 'page', array('view'));
//		parent::startup();
//	}

    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'PageAdmin';
    }

    public function renderList()
    {
        $datasource = dibi::select('*')->from(TBL_TOKENS)->orderBy('dtc', dibi::DESC);
        $grid = new TokensAdminDatagrid($datasource, 'tokenId');
        $this->template->tokens = $grid;
    }

    public function handleCreate()
    {
        $this->tabs['create'] = 'Generate Token';
		$form = new TokenAdminForm($this->nodeId . '_' . $this->action);
		$form->startup($this->node, $this, array($this, 'Form_createFormSubmitted'));
		$this->template->form = $form;
    }

    /**
     * @param TokenAdminForm $form
     */
    public function Form_createFormSubmitted(TokenAdminForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Token record has been created');
            $this->redirect('list');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('create');
        }
    }

    
    public function handleDelete()
    {
        try {
            if (isset($this->get['tokenId'])) {
                $tokenDB = new Tokens($this->get['tokenId']);
                $this->node->removeToken($tokenDB);
                $this->flashMessage('Token record has been deleted from DB and SagePay');
                $this->redirect('list');
            } else {
                throw new Exception('Token record not exist');
            }
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect('list');
        }
    }

}
