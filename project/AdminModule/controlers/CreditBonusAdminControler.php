<?php

class CreditBonusAdminControler extends BaseAdminControler
{
    public function prepareDefault()
 {
        Debug::disableProfiler();
        $datasource = dibi::select('c.email, ca.amount, ca.dtc')->from(TBL_CREDITS_ADDED, 'ca')
                ->innerJoin(TBL_CUSTOMERS, 'c')->on('ca.customerId=c.customerId')
                ->orderBy('dtc')->desc();
        $grid = new CreditBonusAdminDatagrid($datasource, 'c.email');
        $this->template->datagrid = $grid->getHtml();
    }
}
