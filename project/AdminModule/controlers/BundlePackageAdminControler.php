<?php

use Admin\BundlePackage\Bundle\IOptionsFormDelegate as IBundleOptionsFormDelegate;
use Admin\BundlePackage\Bundle\OptionsForm as PackageOptionsForm;

class BundlePackageAdminControler extends PackageAdminControler implements IBundleOptionsFormDelegate
{
    /**
     * @var string
     */
    public static $handleObject = 'BundlePackage';

    /**
     * @var BundlePackage
     */
    public $node;

    /**
     * @var array
     */
    protected $tabs = array(
        'package' => 'Bundle',
        'includes' => 'Includes',
        'properties' => 'Properties',
        'details' => 'Details',
        'seo' => 'Seo'
    );

    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'PackageAdmin';
    }

    /****************************************** package ******************************************/

    public function preparePackage()
    {
        $form = new PackageOptionsForm($this->nodeId . '_' . $this->action);
        $form->startup($this, $this->node);
        $this->template->form = $form;
    }

    /** *********************** IPackageOptionsFormDelegate ************************ **/

    public function bundlePackageOptionsFormSucceeded()
    {
        $this->flashMessage('Bundle has been updated');
        $this->redirect();
    }

    /**
     * @param Exception $e
     */
    public function bundlePackageOptionsFormFailed(Exception $e)
    {
        $this->flashMessage($e->getMessage());
        $this->redirect();
    }
}
