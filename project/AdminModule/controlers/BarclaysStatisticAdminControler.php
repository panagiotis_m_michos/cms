<?php

class BarclaysStatisticAdminControler extends ProductAdminControler
{
	/**
	 * @var array
	 */
	public $tabs = array(
		'default' => 'Barclays'
	);
	
	/**
	 * Display stats data for barclays
	 * 
	 * @return void
	 */
	public function renderDefault()
	{
		$form = new FForm('barclays_stats','get');
		$form->add('DatePicker','date_from', 'Date:')
			->setValue(self::getDefaultDate());
		$form->add('DatePicker', 'date_till', '-&nbsp;&nbsp;')
			->setValue(self::getDefaultDate());
		$form->addSubmit('submit','Go')			
			->class('btn');
		$form->start();
		
		// display data
		if ($form->isOk2Save()) {
			$data = $form->getValues();
			$stats = self::getStatsResult($data['date_from'], $data['date_till']);
			$form->clean();
		} else {
			$stats = self::getStatsResult(self::getDefaultDate('Y-m-d'), self::getDefaultDate('Y-m-d'));
		}
		
		$this->template->barclaysStats = $stats;
		$this->template->form = $form;
	}
	
	
	/**
	 * Rerurns default date - yesterday
	 *
	 * @return string
	 */
	static public function getDefaultDate($format = NULL)
	{
		static $return;
		if ($return === NULL || $format !== NULL) {
			$return = mktime(0, 0, 0, date("m")  , date("d")-1, date("Y"));
			$return =  ($format === NULL) ? date('d-m-Y', $return) : date($format, $return);
		}
		return $return;
	}
	
	/**
	 * Will return barclays statistics
	 *
	 * @param string $dateFrom
	 * @param string $dateTill
	 * @return array
	 */
	static public function getStatsResult($dateFrom, $dateTill)
	{
		$packages = array_keys(Package::$types);
		
		$all = dibi::select('DATE_FORMAT(o.`dtc`,"%Y-%m-%d") as date')
			->select('COUNT(DISTINCT o.orderId) as purchased')
			->from(TBL_ORDERS.' o')
			->join(TBL_ORDERS_ITEMS.' oi')
			->where('(DATE_FORMAT(o.`dtc`,"%Y-%m-%d") >= %s', $dateFrom)->and('DATE_FORMAT(o.`dtc`,"%Y-%m-%d") <= %s)', $dateTill)
				->and('oi.orderId=o.orderId')
				->and('(oi.productId IN %l)', $packages)				
			->groupBy('date');
			
			
		$barclays = dibi::select('DATE_FORMAT(o.`dtc`,"%Y-%m-%d") as date')
			->select('COUNT(DISTINCT o.orderId) as barclays')
			->from(TBL_ORDERS.' o')
			->join(TBL_ORDERS_ITEMS.' oi')
			->where('(DATE_FORMAT(o.`dtc`,"%Y-%m-%d") >= %s', $dateFrom)->and('DATE_FORMAT(o.`dtc`,"%Y-%m-%d") <= %s)', $dateTill)
				->and('oi.orderId=o.orderId')
				->and('oi.productId=%i', 619)
			->groupBy('date');
		
		$sendin = dibi::select('DATE_FORMAT(date_sent,"%Y-%m-%d") as date')
			->select('COUNT(DISTINCT barclays_id) as sent')
			->from('ch_barclays')
			->where('(DATE_FORMAT(`date_sent`,"%Y-%m-%d") >= %s', $dateFrom)
				->and('DATE_FORMAT(`date_sent`,"%Y-%m-%d") <= %s)', $dateTill)
				->and('`success`=1') 
			->groupBy('date');			
			
		$errors = dibi::select('DATE_FORMAT(date_sent,"%Y-%m-%d") as date')
			->select('COUNT(DISTINCT barclays_id) as sent')
			->from('ch_barclays')
			->where('(DATE_FORMAT(`date_sent`,"%Y-%m-%d") >= %s', $dateFrom)
				->and('DATE_FORMAT(`date_sent`,"%Y-%m-%d") <= %s)', $dateTill)
				->and('success=0') 
			->groupBy('date');
			
		$data = $all->execute()->fetchAssoc('date');
		$bar = $barclays->execute()->fetchAssoc('date');
		$sent = $sendin->execute()->fetchAssoc('date');
		$error = $errors->execute()->fetchAssoc('date');
		
		$barclaysStats = array();
		$barclaysStats['total']['all']=0;
		$barclaysStats['total']['barclays']=0;
		$barclaysStats['total']['removed']=0;
		$barclaysStats['total']['sent']=0;
		$barclaysStats['total']['error']=0;
		
		foreach ($data as $key => $value) {		
			$barclaysStats[$key]['date'] = date('D, d/m/Y', strtotime($value->date));
			$barclaysStats[$key]['all'] = $value->purchased;			
			$barclaysStats[$key]['barclays'] = isset($bar[$key])? $bar[$key]->barclays:0; 
			$barclaysStats[$key]['removed'] = isset($bar[$key])? $value->purchased-$bar[$key]->barclays:0;
			$barclaysStats[$key]['sent'] = isset($sent[$key])? $sent[$key]->sent:0;
			$barclaysStats[$key]['error'] = isset($error[$key])? $error[$key]->sent:0;
			$barclaysStats['total']['all'] += $barclaysStats[$key]['all'];
			$barclaysStats['total']['barclays'] += $barclaysStats[$key]['barclays'];
			$barclaysStats['total']['removed'] += $barclaysStats[$key]['removed'];
			$barclaysStats['total']['sent']	+= $barclaysStats[$key]['sent'];		
			$barclaysStats['total']['error']	+= $barclaysStats[$key]['error'];
		}
		
		$stats = array_reverse($barclaysStats);
		return $stats;
	}
	
	
//	static public function getStatsResult_OLD($dateFrom, $dateTill)
//	{
//		$packages = array_keys(Package::$types);
//		
//		$all = dibi::select('DATE_FORMAT(o.`dtc`,"%Y-%m-%d") as date')
//			->select('COUNT(DISTINCT o.orderId) as purchased')
//			->from(TBL_ORDERS.' o')
//			->join(TBL_ORDERS_ITEMS.' oi')
//			->where('(DATE_FORMAT(o.`dtc`,"%Y-%m-%d") >= %s', $dateFrom)->and('DATE_FORMAT(o.`dtc`,"%Y-%m-%d") <= %s)', $dateTill)
//				->and('oi.orderId=o.orderId')
//				->and('(oi.productId IN %l)', $packages)				
//			->groupBy('date');
//			
//			
//		$barclays = dibi::select('DATE_FORMAT(o.`dtc`,"%Y-%m-%d") as date')
//			->select('COUNT(DISTINCT o.orderId) as barclays')
//			->from(TBL_ORDERS.' o')
//			->join(TBL_ORDERS_ITEMS.' oi')
//			->where('(DATE_FORMAT(o.`dtc`,"%Y-%m-%d") >= %s', $dateFrom)->and('DATE_FORMAT(o.`dtc`,"%Y-%m-%d") <= %s)', $dateTill)
//				->and('oi.orderId=o.orderId')
//				->and('oi.productId=%i', 619)
//			->groupBy('date');
//		
//		$dateTill .= '02:00:00';		
//	
//		$sendin = dibi::select('DATE_FORMAT(date_sent,"%Y-%m-%d") as date')
//			->select('COUNT(DISTINCT barclays_id) as sent')
//			->from('ch_barclays')
//			->where('(DATE_FORMAT(`date_sent`,"%Y-%m-%d") >= %s', $dateFrom)->and('DATE_FORMAT(`date_sent`,"%Y-%m-%d") <= %s)', $dateTill) 
//			->GroupBy('date');			
//			
//		$errors = dibi::select('DATE_FORMAT(date_sent,"%Y-%m-%d") as date')
//			->select('COUNT(DISTINCT barclays_id) as sent')
//			->from('ch_barclays')
//			->where('(DATE_FORMAT(`date_sent`,"%Y-%m-%d") >= %s', $dateFrom)
//				->and('DATE_FORMAT(`date_sent`,"%Y-%m-%d") <= %s)', $dateTill)
//				->and('success=0') 
//			->GroupBy('date');
//			
//		$data = $all->execute()->fetchAssoc('date');
//		$bar = $barclays->execute()->fetchAssoc('date');
//		$sen = $sendin->execute()->fetchAssoc('date');
//		$err = $errors->execute()->fetchAssoc('date');
//		
//		foreach($sen as $key => $value)
//		{
//			$date = strtotime($key) - (1 * 24 * 60 * 60);
//			$k = date('Y-m-d', $date);
//			$sent[$k] = $value;
//		}
//		
//		foreach($err as $key => $value)
//		{
//			$date = strtotime($key) - (1 * 24 * 60 * 60);
//			$k = date('Y-m-d', $date);
//			$error[$k] = $value;
//		}
//		
//		$barclaysStats = array();
//		$barclaysStats['total']['all']=0;
//		$barclaysStats['total']['barclays']=0;
//		$barclaysStats['total']['removed']=0;
//		$barclaysStats['total']['sent']=0;
//		$barclaysStats['total']['error']=0;
//		
//		foreach($data as $key => $value)
//		{				
//			$barclaysStats[$key]['date'] = date('D, d/m/Y', strtotime($value->date));
//			$barclaysStats[$key]['all'] = $value->purchased;			
//			$barclaysStats[$key]['barclays'] = isset($bar[$key])? $bar[$key]->barclays:0; 
//			$barclaysStats[$key]['removed'] = isset($bar[$key])? $value->purchased-$bar[$key]->barclays:0;
//			$barclaysStats[$key]['sent'] = isset($sent[$key])? $sent[$key]->sent:0;
//			$barclaysStats[$key]['error'] = isset($error[$key])? $error[$key]->sent:0;
//			$barclaysStats['total']['all'] += $barclaysStats[$key]['all'];
//			$barclaysStats['total']['barclays'] += $barclaysStats[$key]['barclays'];
//			$barclaysStats['total']['removed'] += $barclaysStats[$key]['removed'];
//			$barclaysStats['total']['sent']	+= $barclaysStats[$key]['sent'];		
//			$barclaysStats['total']['error']	+= $barclaysStats[$key]['error'];
//		}
//		
//		$stats = array_reverse($barclaysStats);
//		return $stats;
//	}
	
}