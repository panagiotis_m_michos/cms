<?php

class AnnualReturnAdminControler extends ProductAdminControler
{
    /** @var string */
    static public $handleObject = 'AnnualReturn';
    
    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'ProductAdmin';
    }
}
