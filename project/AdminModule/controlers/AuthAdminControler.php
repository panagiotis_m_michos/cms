<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    AuthAdminControler.php 2009-03-19 divak@gmail.com
 */

class AuthAdminControler extends AdminControler
{
    
    protected $httpsRequired = TRUE;
    
    /**
     * @return void
     */
    public function startup()
    {
        parent::startup();
        // check if user is singin
        if (FUser::isSignedIn()) {
            // loging off
            if (isset($this->get['signout'])) {
                FUser::signOut();
                $this->redirect(NULL, 'signout=');
            } else {
                $this->redirect(1);
            }
        }
    }
    
    
    /**
     * @return void
     */
    protected function prepareDefault()
    {
        $form = new FForm('admin_auth');
        
        $form->addText('login', 'Login: ')
            ->addRule(FForm::Required, 'Login is required!');
        $form->addPassword('password', 'Password: ')
            ->addRule(FForm::Required, 'Password is required!')
            ->addRule(array($this, 'Form_AdminLogin'), array('User Not Found!', 'Invalid Password!', 'Account is not active'));
        $form->addSubmit('submit', 'Login');
        
        $form->onValid = array($this, 'loginFormSubmitted');
        $form->start();
        
        $this->template->form = $form->getHtml();
    }
    
    /**
     * @param object $form
     * @return void
     */
    public function loginFormSubmitted(FForm $form)
    {
        $user = FUser::doAuthenticate($form['login']->getValue(), $form['password']->getValue());
        FUser::signIn($user->getId());
        
        $backlink = $this->getBacklink();
        if ($backlink !== FALSE) {
            $this->removeBacklink();
            $this->redirect("%$backlink%");
        } else {
            $this->redirect();
        }
    }
    
    
    /**
     * Validator
     * @param object $control
     * @param mixed $error
     * @param mixed $params
     * @return mixed
     */
    public function Form_AdminLogin($control, $error, $params)
    {
        // authentificate user
        try {
            $user = FUser::doAuthenticate($control->owner['login']->getValue(), $control->owner['password']->getValue());
            return TRUE;
        } catch (Exception $e) {
        if ($e->getMessage() == 1) {
                $error = $error[0];
                return $error;
        } elseif ($e->getMessage() == 2) {
                $error = $error[1];
                return $error;
        } elseif ($e->getMessage() == 3) {
            $error = $error[2];
            return $error;
        }
        }
    }
    
}
