<?php

use Entities\Service;

/**
 * @property Package $node
 */
class PackageAdminControler extends ProductAdminControler
{
    const PACKAGES_FOLDER = 108;
    const PACKAGES_TEST_FOLDER = 1312;

    /** @var string */
    static public $handleObject = 'Package';

    /** @var array */
    protected $tabs = array(
        'package' => 'Package',
        'includes' => 'Includes',
        'blacklists' => 'Blacklists',
        'associated' => 'Associated',
        'properties' => 'Properties',
        'layout' => 'Layout',
        'details' => 'Details',
        'seo' => 'Seo'
    );

    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'ProductAdmin';
    }

    public function preparePackage()
    {
        $form = new PackageForm($this->nodeId.'_'.$this->action);
        $form->onValid = array($this, 'packageFormSubmitted');
        $form->startup($this->node);
        $this->template->form = $form;
    }

    public function packageFormSubmitted($form)
    {
        $data = $form->getValues();
        $this->node->page->title = $data['title'];
        $this->node->page->alternativeTitle = $data['alternativeTitle'];
        $this->node->page->abstract = $data['abstract'];
        $this->node->page->text = $data['text'];
        $this->node->typeId = $data['typeId'];
        $this->node->upgradeText = $data['upgradeText'];
        $this->node->price = $data['price'];
        $this->node->productValue = $data['productValue'];
        $this->node->associatedPrice = $data['associatedPrice'];
        $this->node->wholesalePrice = $data['wholesalePrice'];
        $this->node->upgradeProductId = $data['upgradeProductId'];
        $this->node->setInternational((bool) $data['isInternational']);
        $this->node->setOfferPrice($data['offerPrice']);
        $this->node->save();

        $this->flashMessage('Package has been updated');
        $this->redirect();
    }

    public function prepareProperties()
    {
        $form = new ProductPropertiesForm($this->nodeId . '_properties');
        $form->startup(
            $this,
            $this->node,
            BasketProduct::getAllProducts(),
            Service::$types,
            Service::$durations
        );
        $this->template->form = $form;
    }

    /************************************ includes ********************************/

    protected function prepareIncludes()
    {
        $form = new FForm($this->nodeId.'_'.$this->action);

        // products 
        $form->addFieldset('Products');
        $form->add('AsmSelect', 'includedProducts', 'Included:')
            ->title('--- Select --')
            ->setValue($this->node->includedProducts)
            ->setOptions(BasketProduct::getAllProducts());

        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Submit')->class('btn');
        $form->onValid = array($this, 'Form_IncludesFormSubmitted');
        $form->start();
        $this->template->form = $form;
    }

    public function Form_IncludesFormSubmitted($form)
    {
        $data = $form->getValues();
        $this->node->includedProducts = $data['includedProducts'];
        $this->node->save();

        $this->flashMessage('Package has been updated');
        $this->redirect();
    }

    /************************************ blacklists ********************************/

    protected function prepareBlacklists()
    {
        $form = new FForm($this->nodeId.'_'.$this->action);

        // equivalent blacklist
        $form->addFieldset('Equivalent Blacklist');
        $form->add('AsmSelect', 'blacklistedEquivalentProducts', 'Products:')
            ->multiple()
            ->title('--- Select --')
            ->setValue($this->node->blacklistedEquivalentProducts)
            ->setOptions(BasketProduct::getAllProducts());

        // contained blacklist
        $form->addFieldset('Contained Blacklist');
        $form->add('AsmSelect', 'blacklistedContainedProducts', 'Products:')
            ->multiple()
            ->title('--- Select --')
            ->setValue($this->node->blacklistedContainedProducts)
            ->setOptions(BasketProduct::getAllProducts());

        // action
        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Submit')->class('btn');
        $form->onValid = array($this, 'Form_BlacklistsFormSubmitted');
        //$form->clean();
        $form->start();
        $this->template->form = $form;
    }

    public function Form_BlacklistsFormSubmitted(FForm $form)
    {
        $data = $form->getValues();

        $this->node->blacklistedEquivalentProducts = $data['blacklistedEquivalentProducts'];
        $this->node->blacklistedContainedProducts = $data['blacklistedContainedProducts'];
        $this->node->save();

        $form->clean();
        $this->flashMessage('Package has been updated');
        $this->redirect();
    }


    /************************************ associated ********************************/


    protected function prepareAssociated()
    {
        $products = array();
        foreach (FNode::getChildsNodes(107) as $key=>$val) {
            if (!in_array($val->adminControler, array('FolderAdminControler','PackageAdminControler','JourneyProductAdminControler'))) {
                $products[$key] = $val->getLngTitle();
            }
        }

        $form = new FForm($this->nodeId.'_'.$this->action);

        // associated 1
        $form->addFieldset('Associated Products 1');
        $form->add('AsmSelect', 'associatedProducts1', 'Products:')
            ->multiple()
            ->title('--- Select --')
            ->setValue($this->node->associatedProducts1)
            ->setOptions(BasketProduct::getAllProducts());

        // associated 2
        $form->addFieldset('Associated Products 2');
        $form->add('AsmSelect', 'associatedProducts2', 'Products:')
            ->multiple()
            ->title('--- Select --')
            ->setOptions(BasketProduct::getAllProducts())
            ->setValue($this->node->associatedProducts2)
            ->addRule(array($this, 'Validator_AssociatedProducts'), 'Associated product 1 and 2 has to be different!');

        // associated 3
        $form->addFieldset('Associated Products 3 upgrade test');
        $form->add('AsmSelect', 'associatedProducts3', 'Products:')
            ->multiple()
            ->title('--- Select --')
            ->setOptions(BasketProduct::getAllProducts())
            ->setValue($this->node->associatedProducts3);

        $form->addFieldset('Offer of the Week');
        $form->addSelect('offerProductId', 'Products:', BasketProduct::getAllProducts())
            ->setFirstOption('--- Select ---')
            ->setValue($this->node->getOfferProductId());

        // action
        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Submit')->class('btn');
        $form->onValid = array($this, 'Form_AssociatedFormSubmitted');
        //$form->clean();
        $form->start();
        $this->template->form = $form;
    }


    public function Form_AssociatedFormSubmitted(FForm $form)
    {
        $data = $form->getValues();

        $this->node->associatedProducts1 = $data['associatedProducts1'];
        $this->node->associatedProducts2 = $data['associatedProducts2'];
        $this->node->associatedProducts3 = $data['associatedProducts3'];
        $this->node->setOfferProductId($data['offerProductId']);
        $this->node->save();

        $form->clean();
        $this->flashMessage('Package has been updated');
        $this->redirect();
    }

    public function Validator_AssociatedProducts($control, $error)
    {
        if ($control->getValue() && $control->owner['associatedProducts1']->getValue()) {
            $result = array_intersect($control->getValue(), $control->owner['associatedProducts1']->getValue());
            if (!empty($result)) {
                return $error;
            }
        }
        return TRUE;
    }

    public function productPropertiesFormSucceeded(ProductPropertiesForm $form)
    {
        $this->flashMessage('Package has been updated');
        $this->redirect();
    }

}
