<?php

use Admin\Wholesale\ISearchDelegate;
use Admin\Wholesale\SearchForm;

class WHSearchAdminControler extends PageAdminControler implements ISearchDelegate
{
    /**
     * @var string
     */
    public static $handleObject = 'WholesaleSearchNode';

    /**
     * @var WholesaleSearchNode
     */
    public $node;

    /**
     * @var array
     */
    public $tabs = array(
        'page' => 'Page',
        'packages' => 'Packages',
        'details' => 'Details',
        'menu' => 'Menu',
        'seo' => 'Seo'
    );


    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'PageAdmin';
    }


    /************************************* packages *************************************/


    public function renderPackages()
    {
        $form = new SearchForm($this->nodeId . 'wholesale_search');
        $form->startup($this, $this->node, BasketProduct::getAllProducts());
        $this->template->form = $form;
    }


    /****************************************** ISearchDelegate ******************************************/


    public function wholesaleSearchFormSucceeded()
    {
        $this->flashMessage('Data has been saved');
        $this->redirect();
    }

    /**
     * @param Exception $e
     */
    public function wholesaleSearchFormFailed(Exception $e)
    {
        $this->flashMessage($e->getMessage(), 'error');
        $this->redirect();
    }
}
