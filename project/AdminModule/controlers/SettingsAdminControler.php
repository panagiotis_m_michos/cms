<?php

class SettingsAdminControler extends PageAdminControler {

    public static $handleObject = "SettingsModel";

	/**
	 * @var SettingsModel
	 */
	public $node;

	/**
	 * @var array
	 */
	public $tabs = array(
		'default' => 'Settings'
	);

    public function renderDefault() {
		$this->template->form = $this->node->getSettingsForm(array($this, 'Form_settingsFormSubmitted'));
    }

    public function Form_settingsFormSubmitted(FForm $form) {

		try {
			$this->node->saveSettingsFormData($form);
			if ($form->isSubmitedBy('save')) {
				$this->flashMessage('Settings have been saved.');
				$this->redirect();
			}
		} catch (Exception $e) {
		    $this->flashMessage($e->getMessage(), 'error');
		 	$this->redirect();
			throw new Exception($e->getMessage());
		}
    }
}
