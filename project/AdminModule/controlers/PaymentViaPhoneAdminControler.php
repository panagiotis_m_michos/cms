<?php

use Entities\Order;
use PayByPhoneModule\Forms\PayByPhoneForm;
use PayByPhoneModule\Services\PayByPhoneService;
use Psr\Log\LoggerInterface;
use Services\CustomerService;

interface PaymentViaPhoneFormDelegate
{
    /**
     * @param Order $order
     * @param int $companyId
     */
    public function paymentViaPhoneSuccess(Order $order, $companyId);

    /**
     * @param PayByPhoneForm $form
     */
    public function paymentViaPhoneProcessForm(PayByPhoneForm $form);

    public function paymentViaPhoneFormCancelled();

    /**
     * @param PayByPhoneForm $form
     * @param Exception $e
     */
    public function paymentViaPhoneFormWithException(PayByPhoneForm $form, Exception $e);
}

class PaymentViaPhoneAdminControler extends PageAdminControler implements PaymentViaPhoneFormDelegate
{
    /**
     * @var string
     */
    public static $namespace = 'phone_payment_basket';

    /**
     * @var array
     */
    protected $tabs = [
        'payment' => 'Payment via Phone',
    ];

    /**
     * @var FUser
     */
    private $user;

    /**
     * @var CustomerService
     */
    private $customerService;

    /**
     * @var PayByPhoneService
     */
    private $payByPhoneService;

    /**
     * @var LoggerInterface
     */
    private $phoneOrdersLogger;

    public function startup()
    {
        // add permission for staff
        $this->acl->allow('staff', 'page', ['view']);
        parent::startup();

        $this->payByPhoneService = $this->getService('pay_by_phone_module.services.pay_by_phone_service');
        $this->customerService = $this->getService('services.customer_service');
        $this->phoneOrdersLogger = $this->getService('pay_by_phone_module.loggers.phone_orders_logger');
    }

    public function beforePrepare()
    {
        parent::beforePrepare();

        $this->templateExtendedFrom[] = 'PageAdmin';
        $this->user = FUser::getSignedIn();

        if (!$this->user->hasPhonePaymentAccess) {
            $this->flashMessage("You don't have permissions to set up phone payments");
            $this->redirect(1);
        }

        // CH database stupid I don't know why but we set db for CH ??? What???
        $db = FApplication::$config['database'];
        CHFiling::setDb([$db['host'], $db['username'], $db['password'], $db['database']]);
        CHFiling::setDocPath(PROJECT_DIR . '/temp/upload/ch_documents');
    }

    public function renderPayment()
    {
        $form = new PayByPhoneForm('payByPhoneAdmin');
        $form->startup($this);

        $this->template->user = $this->user;
        $this->template->form = $form;
    }

    /**
     * @param Order $order
     * @param int $companyId
     */
    public function paymentViaPhoneSuccess(Order $order, $companyId)
    {
        $orderLink = $this->router->adminLink(OrdersAdminControler::ORDERS_PAGE . ' view', ['order_id' => $order->getId()]);
        $link = Html::el('a')->href($orderLink)->setText('order page');
        $flash = Html::el('div')->setText('Payment successful. Order confirmation has been sent to the customer. You can go to the ')->add($link);

        $this->phoneOrdersLogger->info(
            'Order successful',
            [
                'orderId' => $order->getId(),
                'customerId' => $order->getCustomer()->getId(),
                'customerEmail' => $order->getCustomer()->getEmail(),
                'agent' => $order->getAgent()->getFullName(),
            ]
        );
        $this->flashMessage($flash, 'info', FALSE);
        $this->redirect(CompaniesAdminControler::COMPANIES_PAGE. ' changeCompanyName', ['company_id' => $companyId]);
    }

    /**
     * @param PayByPhoneForm $form
     */
    public function paymentViaPhoneProcessForm(PayByPhoneForm $form)
    {
        try {
            $this->payByPhoneService->processPhonePayment($this, $form, $this->get, $this->post);
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $this->readFlashMessages();
    }

    public function paymentViaPhoneFormCancelled()
    {
        $this->flashMessage('Payment was cancelled!');
        $this->redirect();
    }

    /**
     * @param PayByPhoneForm $form
     * @param Exception $e
     */
    public function paymentViaPhoneFormWithException(PayByPhoneForm $form, Exception $e)
    {
        $this->flashMessage($e->getMessage());
        $this->redirect();
    }

    public function renderPaymentBasket()
    {
        $this->template->paymentBasket = FApplication::$httpRequest->isAjax()
            ? $this->payByPhoneService->getPhonePaymentBasket($this->post)
            : NULL;
    }

    public function handlePvpCustomerEmail()
    {
        $email = $this->getParameter('emails');
        if (FApplication::$httpRequest->isAjax() && $email) {
            $customerDetails = $this->payByPhoneService->getPhonePaymentCustomerDetails($email);
            $this->sendJSONResponse($customerDetails);
        }

        $this->terminate();
    }

    public function renderPvpCustomerInformation()
    {
        $customerId = $this->getParameter('customerId');
        $this->template->customer = $customerId ? $this->customerService->getCustomerById($customerId) : NULL;
    }
}
