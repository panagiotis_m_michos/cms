<?php

class DashboardAdminControler extends NameAdminControler
{
    /**
     * @var array
     */
    protected $tabs = array(
        'default' => 'Dashboard',
    );

    public function startup()
    {
        $this->acl->allow('staff', 'page', array('view'));
        parent::startup();
    }

    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'NameAdmin';
    }

    public function renderDefault()
    {
        $this->template->customersForm = $this->getComponentCustomerForm();
        $this->template->companyForm = $this->getComponentCompanyForm();
        $this->template->orderForm = $this->getComponentFilterForm();
    }

    /**
     * @return FForm
     */
    private function getComponentCustomerForm()
    {
        $form = new FForm('customers', 'get');
        $link = $this->router->link("CustomersAdminControler::CUSTOMERS_PAGE list");
        $form->setAction($link);
        $form->addFieldset('Customer Search');
        $form->addText('firstName', 'First name: ');
        $form->addText('lastName', 'Last name: ');
        $form->addText('email', 'Email: ');
        $form->addSelect('roleId', 'Role: ', Customer::$roles)
            ->setFirstOption('--- Select ---');
        $form->addSubmit('customersSearch', 'Search')->class('btn');
        $form->start();
        return $form;
    }

    /**
     * @return FForm
     */
    private function getComponentCompanyForm()
    {
        $form = new FForm('companies', 'get');
        $link = $this->router->link(CompaniesAdminControler::COMPANIES_PAGE . ' list');
        $form->setAction($link);
        $form->addFieldset('Company Search');
        $form->addText('companyName', 'Company name: ');
        $form->addText('companyNumber', 'Company number: ');
        $form->addText('companyId', 'Company id: ')->size(7);
        $form->addSubmit('companySearch', 'Search')->class('btn');
        $form->start();
        return $form;
    }

    /**
     * @return FForm
     */
    public function getComponentFilterForm()
    {
        $form = new FForm('orders', 'get');
        $link = FApplication::$router->link(OrdersAdminControler::ORDERS_PAGE);
        $form->setAction($link);
        $form->addFieldset('Order Search');
        $form->addText('orderId', 'Order number: ')->size(4);
        $form->addText('cardNumber', 'Card Number: ')->size(4);
        $form->addText('cardHolder', 'Card Holder: ')->size(25);
        $form->addText('vendorTXCode', 'Venodor TX Code: ')->readonly();
        $form->addText('vpsAuthCode', 'VPS Auth Code: ')->readonly();
        $form->addSubmit('orderSearch', 'Search')->class('btn');
        $form->addHidden('submited_datagrid-1-filter', 1);
        $form->start();
        return $form;
    }
}
