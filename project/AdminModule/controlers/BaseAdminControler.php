<?php

abstract class BaseAdminControler extends AdminControler
{
    /**
     * Icon for JS tree menu
     * @var string
     */
    static public $icon = 'page.gif';

    /**
     * Force front controler for creating new page
     * @var string
     */
    protected $forceFrontControler = NULL;

    /**
     * Force admin controler for creating new page
     * @var string
     */
    protected $forceAdminControler = NULL;

    /**
     * List of front controlers
     * This ones will be used instead of constrolers which are in folder
     * @var array
     */
    protected $frontControlers = NULL;

    /**
     * List of admin controlers
     * This ones will be used instead of constrolers which are in folder
     * @var array
     */
    protected $adminControlers = NULL;

    /**
     * Tabs for page
     * @var array
     */
    protected $tabs = array();

    /**
     * Controls for page
     * Which controls will be used on page
     * @var array
     */
    protected $controls = array(
        'insertPage' => array(
            'width' => 600,
            'height' => 530
        ),
        'editPage' => array(
            'width' => 600,
            'height' => 400
        ),
        'deletePage' => array(
            'width' => 600,
            'height' => 300
        ),
    );

    /** @var string */
    protected $insertPageControlTitle = 'Create Subpage';

    /** @var string */
    protected $editPageControlTitle = 'Edit Page';

    /** @var string */
    protected $deletePageControlTitle = 'Delete Page';

    /** @var object Node */
    protected $handleNode;

    /** @var string */
    protected $handleLang;

    /** @var object Acl */
    protected $acl;

    /** @var array */
    protected $privileges = array(
        'view',
        'insertPage',
        'editPage',
        'deletePage',
        'frontControler',
        'adminControler',
        'order',
        'parent',
        'status',
        'title',
        'alternativeTitle',
        'abstract',
        'text',
        'pageShowing',
        'dateSensibility',
        'seoTitle',
        'description',
        'keywords',
        'friendlyUrl'
    );

    /** @var string */
    protected $templateExtendedFrom = array('BaseAdmin');

    protected $httpsRequired = TRUE;

    /**
     * @param int $nodeId
     * @return void
     */
    public function __construct($nodeId = NULL)
    {
        parent::__construct($nodeId);
        // check if user is sign in
        if (!FUser::isSignedIn()) {
            $returnUrl = urlencode(FApplication::$httpRequest->uri->getAbsoluteUri());
            $this->saveBacklink();
            $this->redirect("%ADMIN_URL_ROOT%");
        } else {
            $this->template->user = FUser::getSignedIn();

            $this->template->signoutUrl = $this->router->link('%ADMIN_URL_ROOT%', 'signout=1');
        }
        // set permissions
        $this->setPermissions();

    }


    public function startup()
    {
        // add permission
        $this->acl->allow('admin', 'page', $this->privileges);
        // check permission for view
        if ($this->isUserAllowed('page', 'view') === FALSE) {
            $this->terminate("You don't have permission for this page!");
        }
        parent::startup();
    }


    public function beforePrepare()
    {
        parent::beforePrepare();
    }

    public function beforeHandle()
    {
        // --- ajax request for menu update ---
        if ($this->isAjax()) {
            if (isset($this->post['updateMenu'], $this->post['nodeId'], $this->post['parentId'], $this->post['orderNo'])) {
                try {
                    $affectedNodes = self::updateAdminMenu($this->post['nodeId'], $this->post['parentId'], $this->post['orderNo']);
                    $this->terminate(json_encode(['affectedNodes' => $affectedNodes]));
                    die;
                } catch (\Exception $e) {
                    $this->terminate(json_encode(['error' => $e->getMessage()]), 501);
                    die;
                }
            }
        }
        parent::beforeHandle();
    }

    public function beforeRender()
    {
        // tabs and controls
        $this->template->tabs = $this->getTabs();
        $this->template->controls = $this->getControls();

        // add variables to the template
        $this->template->this = $this;
        $this->template->config = FApplication::$config;

        // urls
        $this->template->urlRoot = ADMIN_URL_ROOT;
        $this->template->urlCss = URL_ROOT . 'admin/css/';
        $this->template->urlJs = URL_ROOT . 'admin/js/';
        $this->template->urlImgs = URL_ROOT . 'admin/imgs/';
        $this->template->urlComponent = URL_ROOT . 'components/';

        // other text stuff
        $this->template->author = FApplication::$config['author'];
        $this->template->copyright = FApplication::$config['copyright'];
        $this->template->projectName = FApplication::$config['projectName'];
        $this->template->languages = FLanguage::getLanguages();
        $this->template->currLng = FApplication::$lang;
        $this->template->keywords = $this->node->getLngKeywords();
        $this->template->description = $this->node->getLngDescription();
        $this->template->seoTitle = $this->node->getLngSeoTitle();
        $this->template->text = $this->node->getLngText();
        $this->template->title = $this->node->getLngTitle();
        $this->template->formHelper = $this->getFormHelper();
        $this->readFlashMessages();
        
        // menu
        if (FUser::getSignedIn()->role->key == 'staff') {
            $this->template->jsTreeMenu = $this->getComponentStaffMenu();
        } else {
            $this->template->jsTreeMenu = FMenu::getAdminJsTreeMenu(TRUE);
        }
    }

    /**
     * Returns JS Admin menu for group staff
     * @return string $menu
     */
    private function getComponentStaffMenu()
    {
        $nodes = FApplication::$db->select(
            'n.`node_id`, n.`status_id`, n.`is_deleted`, n.`parent_id`, n.`level`, n.`admin_controler`,n.`front_controler`, n.`ord`,n.`dt_show`,n.`is_dt_sensitive`,n.`dt_start`,n.`dt_end`,p.`title`, p.`friendly_url`'
        )
            ->from(TBL_NODES . ' n')
            ->leftJoin(TBL_PAGES . ' p')
            ->on('n.node_id=p.node_id')->and('p.language_pk=%s', FApplication::$lang)
            ->where('n.is_deleted=%i', 0)
            ->and('n.node_id IN (1, 550, 234, 280, 281, 551, 552, 747, 1217, 1258)')
            ->orderBy('n.ord, n.node_id')
            ->execute()->fetchAssoc('node_id');

        // menu for javascript
        $menu = '';
        foreach ($nodes as $key => $val) {

            $class = $val['admin_controler'];
            $vars = get_class_vars($class);
            $icon = $vars['icon'];

            $params = array(
                'id' => $key,
                'pid' => $val['parent_id'],
                'name' => _e($val['title']),
                'url' => FMenu::getUrl($key, NULL /*, $controler->action*/),
                'title' => 'id: ' . $key . ', ord: ' . $val['ord'],
                'target' => '',
                'icon' => ICONS_DIR_R . $icon,
                'iconOpen' => ICONS_DIR_R . $icon,
            );
            $menu .= 'd.add(' . "'" . implode("','", $params) . "'" . ');' . "\n";
        }

        return $menu;
    }


    public function handleInsertPage()
    {
        // form
        $form = new FForm($this->nodeId . '_' . $this->action);

        // titles
        $form->addFieldset('Titles');
        foreach (FLanguage::getLanguages() as $key => $val) {
            $form->addText('title_' . $key, 'Title (' . $key . '): ');
        }

        // controlers
        $form->addFieldset('Controlers');
        $form->addSelect('frontControler', 'Front controler: ', $this->getControlersOnInsert('front'))
            ->setFirstOption('---- None ---')
            ->setValue($this->getControlerOnInsert('front'));
        $form->addSelect('adminControler', 'Admin controler: *', $this->getControlersOnInsert('admin'))
            ->setFirstOption('---- Select ---')
            ->setValue($this->getControlerOnInsert('admin'))
            ->addRule(FForm::Required, 'Please provide admin controler');

        // data
        $form->addFieldset('Other');
        $form->addText('ord', 'Order: ')
            ->setValue(FNode::getNextOrd($this->node->getId()));
        $form->addSelect('parentId', 'Parent: *', FMenu::getSelect())
            ->setFirstOption('---- Select ---')
            ->setValue($this->node->getId())
            ->addRule(FForm::Required, 'Please provide parent.')
            ->style('width: 200px;');

        //$form->addCheckbox('redirect', 'Redirect: ', 1)
        //->setValue(1);

        // status
        $form->addFieldset('Page Status');
        $form->addSelect('statusId', 'Status: *', FNode::$statuses)
            ->setFirstOption('---- Select ---')
            ->addRule(FForm::Required, 'Please provide page status')
            ->setValue(FNode::STATUS_PUBLISHED);

        // action
        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Submit')
            ->onclick('changes = false')
            ->class('btn');
        $form->onValid = array($this, 'Form_insertPageFormSubmitted');
        $form->clean();
        $form->start();
        $this->template->form = $form;
    }


    public function Form_insertPageFormSubmitted(FForm $form)
    {
        $data = $form->getValues();

        // save node
        $node = new FNode;
        $node->statusId = $data['statusId'];
        $node->parentId = $data['parentId'];
        $node->frontControler = $data['frontControler'];
        $node->adminControler = $data['adminControler'];
        $node->ord = $data['ord'];
        $nodeId = $node->save();

        // save page titles
        $pages = FPage::getNodePages($nodeId);

        foreach ($pages as $key => $val) {
            $titleKey = 'title_' . $key;
            if (isset($data[$titleKey]) && !empty($data[$titleKey])) {
                $page = FPage::create($val);
                $page->title = $data[$titleKey];
                $page->save();
            }
        }

        self::reloadOpener();
    }


    public function renderInsertPage()
    {
        $this->template->title = 'Insert new subpage';

        if (isset($this->controls['insert']['title'])) {
            $this->template->title = $this->controls['insert']['title'] . ' under "' . $this->node->getLngTitle() . '"';
        } else {
            $this->template->title = 'Insert Page "' . $this->node->getLngTitle() . '"';
        }
    }


    /********************************* edit *********************************/

    public function prepareEditPage()
    {
        // can't edit root node
        if ($this->node->nodeId == 1) {
            $this->terminate("You can't edit root page!");
        }

        // check permission
        if (!$this->isUserAllowed('page', 'editPage')) {
            $this->terminate("You don't have permission for this action!");
        }
    }


    public function handleEditPage()
    {
        // form
        $form = new FForm($this->nodeId . '_' . $this->action);

        // titles
        $form->addFieldset('Titles');
        $form->addText('title', 'Title: ')
            ->setValue($this->node->getLngTitle());

        // controlers
        $form->addFieldset('Controlers');
        $form->addSelect('frontControler', 'Front controler: ', $this->getControlersOnInsert('front'))
            ->setFirstOption('---- None ---')
            ->setValue($this->node->frontControler);
        $form->addSelect('adminControler', 'Admin controler: *', $this->getControlersOnInsert('admin'))
            ->setFirstOption('---- Select ---')
            ->setValue($this->node->adminControler)
            ->addRule(FForm::Required, 'Please provide admin controler');

        // data
        $form->addFieldset('Other');
        $form->addText('ord', 'Order: ')
            ->setValue($this->node->ord)
            ->addRule(FForm::Required, 'Please provide order');
        $form->addSelect('parentId', 'Parent: *', FMenu::getSelect())
            ->setFirstOption('---- Select ---')
            ->setValue($this->node->parentId)
            ->setDisabledOptions($this->node->nodeId)
            ->addRule(FForm::Required, 'Please provide parent.')
            ->style('width: 200px;');

        // action
        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Submit')
            ->onclick('changes = false')
            ->class('btn');
        $form->onValid = array($this, 'editPageFormSubmitted');

        // is root node
        if ($this->node->parentId == -1) {
            $form['parentId']->disabled();
            $form['parentId']->setValue(1);
        }

        //$form->clean();
        $form->start();

        $this->template->form = $form;
    }


    public function editPageFormSubmitted(FForm $form)
    {
        // form data
        $data = $form->getValues();

        // check if parent has been changed
        $changedParent = FALSE;
        if ($this->node->parentId != -1 && $this->node->parentId != $data['parentId']) {
            $changedParent = TRUE;
        }

        // check if parent is not the same as current id
        if ($data['parentId'] == $this->node->nodeId) {
            throw new Exception("Parent can't be the same as current page.");
        }

        // update node
        $this->node->frontControler = $data['frontControler'];
        $this->node->adminControler = $data['adminControler'];
        $this->node->ord = $data['ord'];
        $this->node->page->title = $data['title'];

        // is not possible change parent if current one is root
        if ($this->node->parentId != -1) {
            $this->node->parentId = $data['parentId'];
        }

        // parent has been changed
        if ($changedParent) {
            //$parent = Node::create($data['parentId']);
            $parent = new FNode($data['parentId']);
            $this->node->level = $parent->level + 1;
        }

        // save updates
        $this->node->save();

        // change level for childs
        if ($changedParent) {
            $childs = FMenu::get($this->node->nodeId);
            FNode::recountNodeLevels($childs, $this->node->level);
        }

        $url = $this->router->link($this->nodeId);
        self::reloadOpener($url);
    }


    public function renderEditPage()
    {
        if (isset($this->controls['edit']['title'])) {
            $this->template->title = $this->controls['edit']['title'] . ' "' . $this->node->getLngTitle() . '"';
        } else {
            $this->template->title = 'Edit Page "' . $this->node->getLngTitle() . '"';
        }
    }


    /********************************* delete *********************************/


    public function prepareDeletePage()
    {
        // check action delete on root node
        if ($this->node->nodeId == 1) {
            $this->terminate("You can't delete root page!");
        }

        // check permission
        if (!$this->isUserAllowed('page', 'deletePage')) {
            $this->terminate("You don't have permission for this action!");
        }
    }


    public function handleDeletePage()
    {
        // form
        $form = new FForm($this->nodeId . '_' . $this->action);

        // childs
        $childs = FNode::getChildsIds($this->node->nodeId);
        if (!empty($childs)) {
            $form->addFieldset('Page has childs');
            $form->addSelect('childs', 'Decision: ', array('Delete all', 'Move to parent'))
                ->setFirstOption('---- Select ---')
                ->addRule(FForm::Required, 'Please provide a decision.');
        }

        // action
        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Submit')
            ->onclick('changes=false')
            ->class('btn');
        $form->onValid = array($this, 'deletePageFormSubmitted');
        $form->start();
        $this->template->form = $form;
    }


    public function deletePageFormSubmitted(FForm $form)
    {
        $childs = FNode::getChildsNodes($this->node->nodeId);
        foreach ($childs as $key => $child) {
            // delete
            if ($form->getValue('childs') == 0) {
                $child->delete();
                // move to parent
            } else {
                $child->parentId = $this->node->parentId;
                $child->save();
            }
        }
        // delete node
        $this->node->delete();
        $form->clean();

        // redirect to parent
        $link = FApplication::$router->link($this->node->parentId);
        $this->reloadOpener($link);
    }


    public function renderDeletePage()
    {
        if (isset($this->controls['delete']['title'])) {
            $this->template->title = $this->controls['delete']['title'] . ' "' . $this->node->getLngTitle() . '"';
        } else {
            $this->template->title = 'Delete Page "' . $this->node->getLngTitle() . '"';
        }
    }


    /******************************* fileBrowser ***********************************/


    public function prepareFileBrowser()
    {
        if (!isset($this->get['fck']) && !isset($this->get['input_name'])) {
            $this->redirect('list');
        }

        // where condtion
        $where = array('nodeId' => $this->nodeId, 'isDeleted' => 0);

        // paginator
        $count = FFile::getCount($where);
        $paginator = new FPaginator2($count);
        $paginator->itemsPerPage = 25;
        $paginator->htmlDisplayEnabled = FALSE;
        $this->template->paginator = $paginator;

        $this->template->files = FFile::getAll($where, $paginator->getLimit(), $paginator->getOffset());

        // fck editor
        if (isset($this->get['fck'])) {
            $this->changeView('fileBrowserFck');
            $this->tabs = array(
                'fileBrowser' => 'Files',
                'pagesBrowser' => array('title' => 'Pages', 'params' => 'fck=1'),
                'productBrowser' => array('title' => 'Products', 'params' => 'fck=1')
            );
            $this->template->jsTreeFilesMenu = FMenu::getAdminJsTreeMenu(
                TRUE,
                'admin_js_tree_files_fck_menu',
                'fileBrowser',
                'fck=1'
            );
        } else {

            $jsTreeFilesMenu = FMenu::getAdminJsTreeMenu(
                TRUE,
                'admin_js_tree_files_menu',
                'fileBrowser',
                "input_name=INPUT_NAME"
            );
            $jsTreeFilesMenu = str_replace('INPUT_NAME', $this->get['input_name'], $jsTreeFilesMenu);
            $this->template->jsTreeFilesMenu = $jsTreeFilesMenu;

            $this->template->inputName = $this->get['input_name'];
        }
    }


    /******************************* imageBrowser ***********************************/


    public function prepareImageBrowser()
    {
        if (!isset($this->get['fck']) && !isset($this->get['input_name'])) {
            $this->redirect('grid');
        }

        // where condtion
        $where = array('nodeId' => $this->nodeId, 'isDeleted' => 0);

        // paginator
        $count = FImage::getCount($where);
        $paginator = new FPaginator2($count);
        $paginator->itemsPerPage = 25;
        $paginator->htmlDisplayEnabled = FALSE;
        $this->template->paginator = $paginator;

        // form
        $form = new FForm('changeNode', 'get');
        $form->addSelect('nodeId', 'Node: ', array('100' => 'Image node', 493 => 'Image'))
            ->setValue($this->nodeId)
            ->onchange('changeNode();')
            ->disabled();
        $form->start();
        $this->template->form = $form;

        $this->template->images = FImage::getAll($where, $paginator->getLimit(), $paginator->getOffset());

        // fck editor
        if (isset($this->get['fck'])) {
            $this->changeView('imageBrowserFck');
            $this->template->jsTreeFilesMenu = FMenu::getAdminJsTreeMenu(
                TRUE,
                'admin_js_tree_images_fck_menu',
                'imageBrowser',
                'fck=1'
            );
        } else {
            $this->template->inputName = $this->get['input_name'];

            $jsTreeFilesMenu = FMenu::getAdminJsTreeMenu(
                TRUE,
                'admin_js_tree_image_menu',
                'imageBrowser',
                "input_name=INPUT_NAME"
            );
            $jsTreeFilesMenu = str_replace('INPUT_NAME', $this->get['input_name'], $jsTreeFilesMenu);
            $this->template->jsTreeFilesMenu = $jsTreeFilesMenu;
        }
    }


    /******************************* pagesBrowser ***********************************/


    public function preparePagesBrowser()
    {
        // fck editor
        if (isset($this->get['fck'])) {
            $this->tabs = array(
                'fileBrowser' => array('title' => 'Files', 'params' => 'fck=1'),
                'pagesBrowser' => 'Pages',
                'productBrowser' => array('title' => 'Products', 'params' => 'fck=1')
            );
            $this->template->fck = 1;
        }
    }


    public function renderPagesBrowser()
    {
        $this->template->title = 'Pages';
        $this->template->nodes = FMenu::getSelect();
    }


    /******************************* productBrowser ***********************************/


    public function prepareProductBrowser()
    {
        // fck editor
        if (isset($this->get['fck'])) {
            $this->tabs = array(
                'fileBrowser' => array('title' => 'Files', 'params' => 'fck=1'),
                'pagesBrowser' => array('title' => 'Pages', 'params' => 'fck=1'),
                'productBrowser' => array('title' => 'Products', 'params' => 'fck=1')
            );
            $this->template->fck = 1;
        }
    }


    public function renderProductBrowser()
    {
        $this->template->title = 'Pages';
        $this->template->products = BasketProduct::getAllProducts();
    }


    /********************************* other  *********************************/


    final protected function reloadOpener($url = NULL)
    {
        if ($url === NULL) {
            echo '<script>parent.location.reload();</script>';
            exit;
        } else {
            echo '<script>parent.location.href="' . $url . '";</script>';
            exit;
        }
    }


    final protected function getControlersOnInsert($type)
    {
        switch ($type) {
            case 'front':
                if ($this->frontControlers !== NULL && !empty($this->frontControlers)) {
                    $controlers = array();
                    foreach ($this->frontControlers as $key => $val) {
                        $controlers[$val] = str_replace(array('Controler', 'Controller'), '', $val);
                    }
                } else {
                    $controlers = array();
                    foreach (FApplication::getControlers('front') as $key => $val) {
                        $controlers[$key] = $val;
                    }
                }
                break;
            case 'admin':
                if ($this->adminControlers !== NULL && !empty($this->adminControlers)) {
                    $controlers = array();
                    foreach ($this->adminControlers as $key => $val) {
                        $controlers[$val] = str_replace(array('AdminControler', 'Controller'), '', $val);
                    }
                } else {
                    $controlers = FApplication::getControlers('admin');
                }
                break;
        }

        return $controlers;
    }


    final protected function getControlerOnInsert($type)
    {
        switch ($type) {
            case 'front':
                if ($this->forceFrontControler !== NULL) {
                    $controler = $this->forceFrontControler;
                } else {
                    $controler = $this->node->frontControler;
                }
                break;
            case 'admin':
                if ($this->forceAdminControler !== NULL) {
                    $controler = $this->forceAdminControler;
                } else {
                    $controler = $this->node->adminControler;
                }
                break;
        }
        return $controler;
    }

    /**
     * Returns controls for page
     * @return array
     */
    final protected function getControls()
    {
        $controls = array();
        foreach ($this->controls as $key => $val) {

            // check if user is allowed for this action
            if ($this->isUserAllowed('page', $key) === FALSE) {
                continue;
            }

            // params for thickbox
            $params = array(
                'keepThis' => 'true',
                'TB_iframe' => 'true',
            );

            if (isset($val['width'])) {
                $params['width'] = (int) $val['width'];
            }
            if (isset($val['height'])) {
                $params['height'] = (int) $val['height'];
            }

            $controls[$key] = array(
                'link' => $this->router->link($key, $params),
                'title' => $this->{$key . 'ControlTitle'}
            );
        }
        return $controls;
    }

    /**
     * Returns tabs with links
     * @return array
     */
    final protected function getTabs()
    {
        $tabs = array();
        foreach ($this->tabs as $key => $val) {
            if (is_array($val)) {
                $tabs[$key] = array(
                    'link' => $this->router->link($key, $val['params']),
                    'title' => $val['title']
                );
            } else {
                $tabs[$key] = array(
                    'link' => $this->router->link($key),
                    'title' => $val
                );
            }
        }
        return $tabs;
    }


    /**
     * Saving data as props..
     * Key in array has to start with `props_`
     * @param array $properties
     * @return void
     */
    protected function saveProperties($properties)
    {
        foreach ($properties as $key => $val) {
            if (preg_match('#props_#', $key)) {
                $key = str_replace('props_', '', $key);
                FProperty::get($key)->setValue($val)->save();
            }
        }
    }

    /**
     * Create ACL and set roles
     * @return void
     */
    private function setPermissions()
    {
        // permissions
        $this->acl = new Permission();
        $this->acl->addResource('page');
        $roles = FUser::getRoles();

        foreach ($roles as $key => $role) {
            if ($role->parent_id) {
                $parent = new FUserRole($role->parent_id);
                if ($this->acl->hasRole($parent->key) === FALSE) {
                    $this->acl->addRole($parent->key);
                }
                $this->acl->addRole($role->key, $parent->key);
            } else {
                if ($this->acl->hasRole($role->key) === FALSE) {
                    $this->acl->addRole($role->key);
                }
            }
        }
    }

    /**
     * Returns privileges for resource and actions
     * @param string $resource
     * @param string $action
     * @return boolean
     */
    final public function isUserAllowed($resource, $action)
    {
        $role = FUser::getSignedIn()->role->key;
        $allowed = $this->acl->isAllowed($role, $resource, $action);
        return (bool) $allowed;
    }

    /**
     * create pagination
     * @param type $perPage
     * @param type $count
     * @return FPaginator2
     */
    protected function createPaginator($count, $perPage)
    {
        $paginator = new FPaginator2($count);
        $paginator->htmlDisplayEnabled = FALSE;
        $paginator->itemsPerPage = $perPage;
        return $paginator;
    }

    /**
     * output array as csv
     * @param array $arr
     * @param string $name
     */
    protected function outputToCsv($arr, $name = 'csv')
    {
        if (empty($arr)) {
            throw new Exception('No data to export !');
        }
        $header = array_keys((array) current($arr));
        array_unshift($arr, $header);
        $filename = sprintf("$name-%s.csv", date('d-m-Y'));
        Array2Csv::output($arr, $filename);
    }

    /**
     * move node from one parent to other and update its order
     * @param int $nodeId
     * @param int $parentId
     * @param int $index (order no based on parent)
     * @return nodes affected
     */
    public static function updateAdminMenu($nodeId, $parentId, $index)
    {
        $affectedNodes = 1;
        $node = new FNode((int) $nodeId);
        $node->parentId = (int) $parentId;
        if ($index > 0) {
            $node->ord = $index > 1 ? ((int) $index * 10) + 1 : 1;
        }
        $node->save();
        if ($index > 0) {
            $nodes = FNode::getChildsNodes($node->parentId, FALSE);
            FNode::recountNodeObjectOrders($nodes);
            $affectedNodes = count($nodes);
        }

        return $affectedNodes;
    }
}
