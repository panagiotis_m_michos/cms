<?php

class TsbEmailAdminControler extends EmailAdminControler
{
    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'EmailAdmin';
    }

    /**
     * @return array
     */
    protected function getFromEmails()
    {
        return FApplication::$config['tsb_banking']['emails'];
    }
}
