<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    AffiliatesAdminControler.php 2009-11-11 divak@gmail.com
 */

class BasketSummaryAdminControler extends PageAdminControler
{
    /** @var array */
    protected $tabs = array(
        'page' => 'Page',
        'properties' => 'Properties',
        'details' => 'Details',
        'menu' => 'Menu',
        'seo' => 'Seo'
    );
    
    /** @var string */
    static public $handleObject = 'BasketModel'; 
    
    /** 
      * @var BasketModel 
      */
    public $node;
    
    
    /** @var string */
    protected $templateExtendedFrom = array('BaseAdmin', 'PageAdmin');
    
    
    public function renderProperties()
    {
        $this->template->form = $this->node->getComponentAdminPropertiesForm(array($this, 'Form_propertiesFormSubmitted'));
    }
    
    public function Form_propertiesFormSubmitted(FForm $form)
    {
        try {
            $this->node->saveAdminPropertiesTabData($form);
            $this->flashMessage('Data have been updated');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
             $this->redirect();
        }
    }
    
    
}
