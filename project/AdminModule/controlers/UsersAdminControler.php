<?php

class UsersAdminControler extends NameAdminControler
{
    /**
     * @var array
     */
    protected $tabs = [
        'list' => 'List',
    ];

    /**
     * @var User
     */
    private $user;

    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'NameAdmin';
    }

    public function renderList()
    {
        $this->template->users = FUser::getAll();
    }

    /********************************************* create *********************************************/

    public function prepareCreate()
    {
        $this->tabs['create'] = 'Create';
    }

    public function renderCreate()
    {
        $form = new FForm($this->nodeId . '_' . $this->action);

        $form->addFieldset('Login Data');
        $form->addText('login', 'Login: * ')
            ->addRule(FForm::Required, 'Login is required!')
            ->addRule([$this, 'Validator_LoginAvailable'], 'Login has been taken!')
            ->addRule('MinLength', 'Login have to have min #1 chars', 4)
            ->addRule('MaxLength', 'Login have to have max #1 chars', 32)
            ->setDescription('(min 4 chars, max 32 chars)');
        $form->addPassword('password', 'Password: *')
            ->setDescription("(min 6 chars, max 32 chars)")
            ->addRule(FForm::Required, 'Password is required!')
            ->addRule('MinLength', 'Login have to have min #1 chars', 6)
            ->addRule('MaxLength', 'Login have to have max #1 chars', 32);
        $form->addPassword('password_conf', 'Confirm: *')
            ->addRule(FForm::Equal, "Passwords doesn't match!", 'password');

        $form->addFieldset('Personal Data');
        $form->addText('firstName', 'Firstname: * ')
            ->addRule(FForm::Required, 'Firstname is required!');
        $form->addText('lastName', 'Lastname: * ')
            ->addRule(FForm::Required, 'Lastname is required!');
        $form->addText('email', 'E-mail: * ')
            ->size(30)
            ->addRule(FForm::Required, 'E-mail is required!')
            ->addRule(FForm::Email, 'E-mail is not valid!');

        $form->addFieldset('Other');
        $form->addSelect('roleId', 'Role', FUserRole::getDropdown())
            ->setDescription('what is the user role')
            ->addRule(FForm::Required, 'Please provide role!')
            ->setFirstOption('--- Select ---');
        $form->addSelect('statusId', 'Status', FUser::$statuses)
            ->addRule(FForm::Required, 'Please provide role!')
            ->setFirstOption('--- Select ---');

        $form->addFieldset('Phone Payment');
        $form->addCheckbox('hasPhonePaymentAccess', 'Phone Payment Privileges', 1);

        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Submit')
            ->class('btn');

        $form->onValid = [$this, 'Form_createFormSubmitted'];
        $form->start();
        $this->template->form = $form;
    }

    /**
     * @param FForm $form
     */
    public function Form_createFormSubmitted(FForm $form)
    {
        $data = $form->getValues();

        $user = FUser::create();
        $user->login = $data['login'];
        $user->password = $data['password'];
        $user->firstName = $data['firstName'];
        $user->lastName = $data['lastName'];
        $user->email = $data['email'];
        $user->statusId = $data['statusId'];
        $user->roleId = $data['roleId'];
        $user->hasPhonePaymentAccess = $data['hasPhonePaymentAccess'];
        $user->save();

        $form->clean();
        $this->flashMessage('User has been created');
        $this->redirect('list');
    }

    /**
     * @param Text $control
     * @param string $error
     * @param array $params
     * @return bool
     */
    public function Validator_LoginAvailable(Text $control, $error, $params)
    {
        $login = $control->getValue();

        // check own login on update
        if (isset($params['login']) && $params['login'] == $login) {
            return TRUE;
        }

        try {
            $user = FUser::getCustomerByLogin($login);

            return $error;
        } catch (Exception $e) {
            return TRUE;
        }
    }

    /********************************************* edit *********************************************/

    public function prepareEdit()
    {
        try {
            if (!isset($this->get['user_id'])) {
                $this->redirect('list');
            }
            $this->user = FUser::create($this->get['user_id']);
            $this->tabs['edit'] = ['title' => 'Edit', 'params' => "user_id={$this->user->getId()}"];
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect('list');
        }
    }

    public function renderEdit()
    {
        $form = new FForm($this->nodeId . '_' . $this->action);

        $form->addFieldset('Login Data');
        $form->addText('login', 'Login: * ')
            ->addRule(FForm::Required, 'Login is required!')
            ->addRule([$this, 'Validator_LoginAvailable'], 'Login has been taken!', ['login' => $this->user->login])
            ->addRule('MinLength', 'Login have to have min #1 chars', 4)
            ->addRule('MaxLength', 'Login have to have max #1 chars', 32)
            ->setDescription('(min 4 chars, max 32 chars)');
        $form->addPassword('password', 'Password: *')
            ->setDescription("(min 6 chars, max 32 chars)")
            ->addRule('MinLength', 'Login have to have min #1 chars', 6)
            ->addRule('MaxLength', 'Login have to have max #1 chars', 32);
        $form->addPassword('password_conf', 'Confirm: *')
            ->addRule(FForm::Equal, "Passwords doesn't match!", 'password');

        $form->addFieldset('Personal Data');
        $form->addText('firstName', 'Firstname: * ')
            ->addRule(FForm::Required, 'Firstname is required!');
        $form->addText('lastName', 'Lastname: * ')
            ->addRule(FForm::Required, 'Lastname is required!');
        $form->addText('email', 'E-mail: * ')
            ->size(30)
            ->addRule(FForm::Required, 'E-mail is required!')
            ->addRule(FForm::Email, 'E-mail is not valid!');

        $form->addFieldset('Other');
        $form->addSelect('roleId', 'Role', FUserRole::getDropdown())
            ->setDescription('what is the user role')
            ->addRule(FForm::Required, 'Please provide role!')
            ->setFirstOption('--- Select ---');
        $form->addSelect('statusId', 'Status', FUser::$statuses)
            ->addRule(FForm::Required, 'Please provide role!')
            ->setFirstOption('--- Select ---');

        $form->addFieldset('Phone Payment');
        $form->addCheckbox('hasPhonePaymentAccess', 'Phone Payment Privileges', 1);

        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Submit')
            ->class('btn');

        $form->setInitValues((array) $this->user);
        $form->onValid = [$this, 'Form_editFormSubmitted'];
        $form->start();
        $this->template->form = $form;
    }

    /**
     * @param FForm $form
     */
    public function Form_editFormSubmitted(FForm $form)
    {
        $data = $form->getValues();

        try {
            $user = FUser::create($this->get['user_id']);
            $user->login = $data['login'];
            $user->password = $data['password'];
            $user->firstName = $data['firstName'];
            $user->lastName = $data['lastName'];
            $user->email = $data['email'];
            $user->statusId = $data['statusId'];
            $user->roleId = $data['roleId'];
            $user->hasPhonePaymentAccess = $data['hasPhonePaymentAccess'];
            $user->save();

            $this->flashMessage('User has been updated');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
        }

        $form->clean();
        $this->redirect('list');
    }
}
