<?php

use LoggableModule\Services\EmailLogsService;
use Services\CustomerService;

class EmailLogsAdminControler extends PageAdminControler
{

    const EMAIL_LOG_PAGE = 1459;

    /**
     * @var array
     */
    public $tabs = [
        'list' => 'List',
    ];

    /**
     * @var EmailLog
     */
    public $emailLog;

    /**
     * @var EmailLogsService
     */
    private $emailLogsService;

    /**
     * @var CustomerService
     */
    private $customerService;

    public function startup()
    {
        $this->acl->allow('staff', 'page', ['view']);

        parent::startup();
        $this->emailLogsService = $this->getService(DiLocator::SERVICE_EMAIL_LOGS);
        $this->customerService = FApplication::$container->get('services.customer_service');
    }

    public function beforePrepare()
    {
        parent::beforePrepare();

        // --- some checking ---
        if (in_array($this->action, ['view']) && !isset($this->get['emailLogId'])) {
            $this->flashMessage('Parameter emailLogId is required', 'error');
            $this->redirect('list');
        } elseif (isset($this->get['emailLogId'])) {
            try {
                $emailLog = new EmailLog($this->get['emailLogId']);
                if ($emailLog == NULL)
                    throw new Exception("EmailLog with ID: ({$this->get['emailLogId']}) not found");
                $this->emailLog = $emailLog;
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
                $this->redirect('list');
            }
        }
    }

    /*     * ***************************** list ********************************** */

    public function prepareList()
    {
        $customer = NULL;
        if (isset($this->get['customer_id'])) {
            $customer = $this->customerService->getCustomerById($this->get['customer_id']);
        }

        $dataSource = $this->emailLogsService->getEmailsLogDataSource($customer);

        $grid = new EmailLogsAdminDatagrid($dataSource, 'emailLogId');
        $this->template->datagrid = $grid;
    }

    /*     * ***************************** view ********************************** */

    public function prepareView()
    {
        $this->tabs['view'] = ['title' => 'Log Info', 'params' => "emailLogId={$this->emailLog->getId()}"];
    }

    public function renderView()
    {
        $this->template->emailLog = $this->emailLog;
    }

}
