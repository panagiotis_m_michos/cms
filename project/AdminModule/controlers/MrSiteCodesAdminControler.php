<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    MrSiteCodesAdminControler.php 2009-11-11 divak@gmail.com
 */

class MrSiteCodesAdminControler extends PageAdminControler
{
	/** @var string */
	static public $handleObject = 'MrSiteCodesModel';
	
	/**
	 * @var MrSiteCodesModel
	 */
	public $node;
	
	/** @var array */
	protected $tabs = array(
		'list' => 'Codes',
	);
	
	
	/**
	 * Checks compulsory mrSiteCodeId on some actions
	 * and will create MrSiteCode object
	 *
	 * @return void 
	 */
	public function beforePrepare()
	{
		try {
			if (isset($this->get['mrSiteCodeId'])) {
				$this->node->createMrSiteCode($this->get['mrSiteCodeId']);
				$this->template->code = $this->node->getMrSiteCode();
				$this->template->mrSiteCodeId = $this->node->getMrSiteCode()->getId();
			} elseif (in_array($this->action, array('view', 'edit', 'delete'))) {
				throw new Exception('mrSiteCodeId is required');
			}
		} catch (Exception $e) {
			$this->flashMessage($e->getMessage(), 'error');
			$this->redirect('list');
		}
		parent::beforePrepare();
	}
	
	
	/************************************* list *************************************/
	
	
	public function renderList()
	{
		$paginator = $this->node->getMrSiteCodesPaginator();
		$this->template->paginator = $paginator;
		$this->template->codes = $this->node->getMrSiteCodes($paginator);
	}
	
	
	/************************************* view *************************************/

	
	public function prepareView()
	{
		try {
			$this->tabs['view'] = array('title'=>$this->node->getMrSiteCode()->code, 'params'=>'mrSiteCodeId='.$this->get['mrSiteCodeId']);
		} catch (Exception $e) {
			$this->flashMessage($e->getMessage());
			$this->redirect('list');
		}
	}
	
	
	
	/************************************* create *************************************/
	
	
	public function prepareCreate()
	{
		Debug::disableProfiler();
		$this->template->form = $this->node->getComponentMrSiteCodeAdminForm(array($this, 'Form_createFormSubmitted'));
	}
	
	
	public function Form_createFormSubmitted(FForm $form)
	{
		try {
			$mrSiteCodeId = $this->node->processMrSiteCodeForm($form, 'create');
			$this->flashMessage('MrSite Code has been created');
			
			$url = FApplication::$router->link('view', "mrSiteCodeId=$mrSiteCodeId");
			$this->reloadOpener($url);
		} catch (Exception $e) {
			$this->flashMessage($e->getMessage(), 'error');
			$url = FApplication::$router->link('list');
			$this->reloadOpener($url);
		}
	}
	
	
	
	/************************************* edit *************************************/
	
	
	public function renderEdit()
	{
		Debug::disableProfiler();
		$this->template->title = 'MrSite Code Edit';
		$this->template->form = $this->node->getComponentMrSiteCodeAdminForm(array($this, 'Form_editFormSubmitted'));
	}
	
	
	public function Form_editFormSubmitted(FForm $form)
	{
		try {
			$this->node->processMrSiteCodeForm($form);
			$this->flashMessage('MrSite Code has been modified');
			$this->reloadOpener();
		} catch (Exception $e) {
			$this->flashMessage($e->getMessage(), 'error');
			$url = FApplication::$router->link('list');
			$this->reloadOpener($url);
		}
	}
	
	
	/************************************* delete *************************************/
	
	
	public function handleDelete()
	{
		try {
			$this->node->getMrSiteCode()->delete();
			$this->flashMessage('MrSite Code has been deleted');
		} catch (Exception $e) {
			$this->flashMessage($e->getMessage(), 'error');
		}
		$this->redirect('list');
	}
}
