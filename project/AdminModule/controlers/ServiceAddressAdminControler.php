<?php
class ServiceAddressAdminControler extends ProductAdminControler
{
    /** @var string */
    static public $handleObject = 'ServiceAddress';
    
    /** @var array */
    protected $tabs = array(
        'product' => 'Product',
        'properties' => 'Properties',
        'details' => 'Details',
        'seo' => 'Seo'
    );
    
    
    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->templateExtendedFrom[] = 'ProductAdmin';
    }
    
    
    public function prepareProduct()
    {
        $form = new FForm($this->nodeId.'_'.$this->action);
        
        // fieldset
        $form->addFieldset('Product');
        $form->addText('title', 'Title: * ')
            ->addRule(FForm::Required, 'Title is required!')
            ->size(30)
            ->setValue($this->node->page->title);
        $form->addText('alternativeTitle', 'Alternative Title:')
            ->size(50)
            ->setValue($this->node->page->alternativeTitle);
        $form->add('CmsFckArea', 'text', 'Page Text:')
            ->setSize(500, 300)
            ->setValue($this->node->page->text);
            
        // other
        $form->addFieldset('Price');
        $form->addText('price', 'Price: *')
            ->size(5)
            ->setValue($this->node->price)
            ->addRule(FForm::Required, 'Please provide price')
            ->addRule('Numeric', 'Price has to be number');
        $form->addText('productValue', 'Product value: *')
            ->size(5)
            ->setValue($this->node->productValue)
            ->addRule(FForm::Required, 'Please provide product value')
            ->addRule('Numeric', 'Price value has to be number');
        $form->addText('associatedPrice', 'Associated price: *')
            ->size(5)
            ->setValue($this->node->associatedPrice)
            ->addRule(FForm::Required, 'Please provide associated price')
            ->addRule('Numeric', 'Associated price value has to be number');
        $form->addText('wholesalePrice', 'Wholesale price: *')
            ->size(5)
            ->setValue($this->node->wholesalePrice)
            ->addRule(FForm::Required, 'Please provide wholesale price')
            ->addRule('Numeric', 'Wholesale price value has to be number');
        $form->addText('offerPrice', 'Offer price: *')
            ->size(5)
            ->setValue($this->node->getOfferPrice())
            ->addRule(FForm::Required, 'Please provide offer price')
            ->addRule('Numeric', 'Offer price value has to be number');
        
        // other
        $form->addFieldset('Other');
        $form->addText('premise', 'Address 1: *')
            ->setValue($this->node->premise)
            ->addRule(FForm::Required, 'Please provide Address 1');
        $form->addText('street', 'Address 2:')
            ->setValue($this->node->street);
        $form->addText('thoroughfare', 'Address 3:')
            ->setValue($this->node->thoroughfare);
        $form->addText('post_town', 'Town: *')
            ->setValue($this->node->post_town)
            ->size(15)
            ->addRule(FForm::Required, 'Please provide Town');
        $form->addText('county', 'County:')
            ->setValue($this->node->county)
            ->size(15);
        $form->addText('postcode', 'Postcode: *')
            ->setValue($this->node->postcode)
            ->addRule(FForm::Required, 'Please provide Postcode')
            ->size(7);
        $form->addSelect('country', 'Country: *', Address::$registeredOfficeCountries)
            ->setValue($this->node->country)
            ->addRule(FForm::Required, 'Please provide Country');
        
        
        // action
        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Submit')
            ->onclick('changes = false')
            ->class('btn');
        $form->onValid = array($this, 'packageFormSubmitted');
        $form->start();
        $this->template->form = $form;
    }
    
    
    public function packageFormSubmitted($form)
    {
        $data = $form->getValues();
        $this->node->page->title = $data['title'];
        $this->node->page->alternativeTitle = $data['alternativeTitle'];
        $this->node->page->text = $data['text'];
        $this->node->price = $data['price'];
        $this->node->productValue = $data['productValue'];
        $this->node->associatedPrice = $data['associatedPrice'];
        $this->node->wholesalePrice = $data['wholesalePrice'];
        $this->node->setOfferPrice($data['offerPrice']);

        $this->node->premise = $data['premise']; 
        $this->node->street = $data['street'];
        $this->node->thoroughfare = $data['thoroughfare'];
        $this->node->post_town = $data['post_town'];
        $this->node->county = $data['county'];
        $this->node->postcode = $data['postcode'];
        $this->node->country = $data['country'];
        $this->node->save();
        
        $this->flashMessage('Product has been updated');
        $this->redirect(NULL);
    }
    
    
    
    /**
     * Validator
     * @param object $control
     * @param mixed $error
     * @param mixed $params
     * @return mixed
     */
    public function Form_validProduct($control, $error, $params)
    {
        if ($control->getValue() != NULL) {
            $product = new Product($control->getValue());
            if ($product->isOk2show() == FALSE) {
                return $error;
            }
        }
        return TRUE;
    }
    
}
