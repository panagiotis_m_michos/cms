<?php

use Datagrid\Admin\AnswersDatagrid;

class AnswersAdminControler extends PageAdminControler
{
    /**
     * @var array
     */
    protected $tabs = array(
        'default' => 'Answers',
    );

    public function handleDefault()
    {
        Debug::disableProfiler();
        $answerService = $this->getService(DiLocator::SERVICE_ANSWER);
        $datagrid = new AnswersDatagrid($answerService->getListDatasource(), 'answerId');
        $this->template->datagrid = $datagrid->getHtml();
    }
}
