{include file="@header.tpl"}

<div style="width:800px;">
	{if !empty($ars)}
		{$ars nofilter}
	{/if}
</div>

{literal}
<style>
    table.form-table th {
        text-align: left;
        padding: 0 5px 0 0;
        width: 80px;
        font-weight: normal;
    }
    
    .submit_button {
        background-color: #E0DFF7;
        border-color: #EDEDED #B4B4B4 #B4B4B4 #EDEDED;
        border-left: 1px solid #EDEDED;
        border-style: solid;
        border-width: 1px;
        color: black;
        height: 20px;
        margin: 0;
        padding: 0 5px 2px;
    }
</style>
{/literal}

{include file="@footer.tpl"}