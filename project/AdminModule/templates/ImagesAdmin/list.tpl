{include file="@header.tpl"}

<p>[<a href="{$this->router->link("insert")}">create new]</a></p>

{if !empty($images)}
	<table class="htmltable">
	<col width="50">
	<col>
	<col width="50">
	<col width="100">
	<col width="60">
	<col width="60">
	<col width="100">
	<col width="60">
	<col width="60">
	<tr>
		<th>Id</th>
		<th>Name</th>
		<th>Ext</th>
		<th>Size</th>
		<th>Active</th>
		<th>Ord</th>
		<th>Created</th>
		<th colspan="2">Action</th>
	</tr>
	{foreach from=$images key="imageId" item="image"}
	<tr>
		<td class="right">{$image->getId()}</td>
		<td><a href="{$image->getFilePath(true)}" target="_blank">{$image->name}</a></td>
		<td class="center">{$image->ext}</td>
		<td class="right">{$image->getSize(true)}</td>
		<td class="center">{$image->isActive}</td>
		<td class="right">{$image->ord}</td>
		<td class="center">{$image->dtc|date_format:"%d/%m/%Y"}</td>
		<td class="center"><a href="{$this->router->link('edit', "image_id=$imageId")}">edit</a></td>
		<td class="center"><a href="{$this->router->link('delete', "image_id=$imageId")}" onclick="return confirm('Are you sure?');">delete</a></td>
	</tr>
	{/foreach}
	</table>

	{* PAGINATOR *}
	{$paginator nofilter}
{else}
	<p>No images</p>
{/if}


{include file="@footer.tpl"}