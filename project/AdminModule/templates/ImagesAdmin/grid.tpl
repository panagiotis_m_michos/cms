{include file="@header.tpl"}

<p></a></p>

{if !empty($images)}

	<table class="htmlgrid">
	<col width="182">
	<col width="182">
	<col width="182">
	<col width="182">
	<tr>
		<th colspan="4" class="left" style="font-style: normal; font-weight: normal;">[<a href="{$this->router->link("insert")}">add new image</a>]</th>
	</tr>
	<tr>
		{foreach from=$images key="imageId" item="image" name="f1"}
			<td align="center" class="dfimg" id="{$imageId}">
				{$image->getHtmlTag("s") nofilter} <br />
				<strong>{$image->getFileName()}</strong> <br />
				Ord: {$image->ord}, 
				{assign var="author" value=$image->getAuthor()}
				Author: {$author->login}
				<br />
				{$image->width} x {$image->height} ({$image->getSize(true)})
			</td>
	
			{* NEW ROW? *}		
			{if $smarty.foreach.f1.iteration % 4 == 0}
				</tr><tr>
			{/if}
		{/foreach}
	</tr>
	</table>
	
	{* PAGINATOR *}
	<div style="padding-left: 10px;">{$paginator nofilter}</div>
	
	{* CONTEXT MENU *}
	<div class="contextMenu" id="ImgsGrid"> 
		<ul> 
			<li id="show"><img src="{$urlImgs}icons/show.gif" alt="Show" title="Show" />Show</li> 
			<li id="edit"><img src="{$urlImgs}icons/edit2.gif" alt="Edit" title="Edit" />Edit</li> 
			<li id="delete"><img src="{$urlImgs}icons/delete2.gif" alt="Delete" title="Delete" />Delete</li> 
		</ul>
	</div>
	
	<script type="text/javascript">
	
	// url to actions
	{assign var="currentPage" value=$paginator->getCurrentPage()}
	var editUrl = "{$this->router->link("edit", "image_id=handle_id", "page=$currentPage")}";
	var deleteUrl = "{$this->router->link("delete", "image_id=handle_id", "page=$currentPage")}";
	
	// path to images
	var imgPath = new Array();
	{foreach from=$images key="imageKey" item="image"}
	imgPath[{$imageKey}] = "{$image->getFilePath(true)}";
	{/foreach}
	</script>
	
	{literal}
	<script type="text/javascript">
	 
	function getHandleUrl(url,id) {
		return(url.replace(/handle_id/,id));
	}
	
	$('.dfimg').contextMenu('ImgsGrid', {
		bindings: {
			'show': function(t) {
				tb_show('',imgPath[t.id])
			},
			'edit': function(t) { 
	    		window.location.href = getHandleUrl(editUrl, t.id);
			},
			'delete': function(t) {
				if(confirm('Are you sure') == true) { 
		    		window.location.href = getHandleUrl(deleteUrl, t.id);
		    	}
		    }
		}
	});
	</script>
	{/literal}

{else}
	<p>No images</p>
{/if}


{include file="@footer.tpl"}