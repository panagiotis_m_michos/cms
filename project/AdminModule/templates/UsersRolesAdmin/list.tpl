{include file="@header.tpl"}

<p><a href="{$this->router->link('create')}">[create new]</a></p>
<table class="htmltable">
<col width="50">
<col>
<col>
<col>
<col>
<col>
<col width="60">
<col width="60">
<tr>
	<th>Id</th>
	{*<th>Parent</th>*}
	<th>Key</th>
	<th>Title</th>
	<th>Created</th>
	<th>Edited</th>
	<th colspan="2" class="center">Action</th>
</tr>
{foreach from=$roles key="id" item="role"}
<tr>
	<td>{$role->getId()}</td>
	{*<td>{$role->parent->title}</td>*}
	<td>{$role->key}</td>
	<td>{$role->title}</td>
	<td>{$role->dtc|date_format:"%d/%m/%Y"}</td>
	<td>{$role->dtm|date_format:"%d/%m/%Y"}</td>
	<td class="center"><a href="{$this->router->link("edit", "role_id=$id")}">edit</td>
	<td class="center"><a href="{$this->router->link("list", "delete_id=$id")}" onclick="return confirm('Are you sure?')">delete</td>
</tr>
{/foreach}
</table>

{include file="@footer.tpl"}