{include file="@header.tpl"}

<ul class="submenu">
	<li><a href="{$this->router->link("print", "order_id=$orderId")}">Print</a></li>
	{if $view->canBeRefunded()}
		<li><a href="{$this->router->link("refund", "order_id=$orderId")}">Refund</a></li>
	{/if}
</ul>

{$customer = $orderEntity->getCustomer()}
<h2>Customer info</h2>
<table class="htmltable">
<col width="150">
<tr>
	<th>Customer name:</th>
	<td>
        <a href="{$this->router->link("CustomersAdminControler::CUSTOMERS_PAGE view", "customer_id={$customer->getId()}")}">
            {$customer->getFullName()}
        </a>
    </td>
</tr>
<tr>
	<th>Email:</th>
	<td>{$customer->getEmail()}</td>
</tr>
</table>

{* VOUCHER *}
{if $order->voucherId}
	<h2>Voucher</h2>
	<table class="htmltable">
	<col width="150">
	<tr>
		<th>Name:</th>
		<td>{$order->voucherName}</td>
	</tr>
	<tr>
		<th>Id:</th>
		<td>{$order->voucherId}</td>
	</tr>
	</table>
{/if}

<h2>Order info</h2>

<table class="htmltable">
<col width="150">
<tr>
	<th>Order id:</th>
	<td>#{$order->getId()}</td>
</tr>
<tr>
	<th class="left">Status</th>
	<td>{$order->status}</td>
</tr>
<tr>
	<th class="left">Order Date</th>
	<td>{$order->dtc}</td>
</tr>
<tr>
	<th class="left">Contact</th>
	<td>{$order->customerName}</td>
</tr>
<tr>
	<th class="left">Telephone</th>
	<td>{$order->customerPhone}</td>
</tr>
<tr>
	<th class="left">Description</th>
	<td>{$order->description}</td>
</tr>
<tr>
	<th class="left">Payment Medium</th>
	<td>{$orderEntity->getPaymentMedium()}</td>
</tr>
{assign 'agent' $orderEntity->getAgent()}
{if $agent}
	<tr>
		<th class="left">Agent</th>
		<td>{$agent->getFullName()} ({$agent->getLogin()})</td>
	</tr>
{/if}
{if $order->isRefunded}
    <tr>
        <th class="left">Refunded by</th>
        <td>{$order->refundCustomerSupportLogin}</td>
    </tr>
{/if}
</table>

<h2>Basket items</h2>

<table class="htmltable">
<col>
<col width="70">
<col width="60">
<col width="150">
<tr>
	<th>Product</th>
	<th>Price</th>
	<th>Qty</th>
	<th>Additional</th>
</tr>
{foreach from=$order->items item="item"}
<tr{if $item->qty < 0} style="color: red"{/if}>
	<td class="noOverflow">#{$item->orderItemId} {$item->productTitle}</td>
	<td align="center">{$item->price}</td>
	<td align="center">{$item->qty}</td>
	<td class="noOverflow" align="left">{$item->additional|nl2br}</td>
</tr>
{/foreach}
<tr>
	<td colspan="3" class="right"><b>Subtotal:</b></td>
	<td class="right">&pound;{$order->getRounded('realSubtotal')}</td>
</tr>
{if $order->discount > 0}
<tr>
	<td colspan="3" class="right"><b>Discount:</b></td>
	<td class="right">-&pound;{$order->getRounded('discount')}</td>
</tr>
{/if}
<tr>
	<td colspan="3" class="right"><b>VAT:</b></td>
	<td class="right">&pound;{$order->getRounded('vat')}</td>
</tr>
{if $order->credit > 0}
	<tr>
		<td colspan="3" class="right"><b>Credit:</b></td>
		<td class="right {if $order->isRefunded}priceRefunded{/if}">-&pound;{$order->getRounded('credit')}</td>
	</tr>
{/if}

<tr {if $order->isRefunded}class="borderSeparator"{/if}>
	<td colspan="3" class="right"><b>Total:</b></td>
	<td class="right {if $order->isRefunded}priceRefunded{/if}">&pound;{$order->getRounded('total')}</td>
</tr>
{if $order->isRefunded}
<tr>
	<td colspan="3" class="right"><b>Money Refunded:</b></td>
	<td class="right">&pound;{$order->getRounded('refundValue')}</td>
</tr>
<tr>
    <td colspan="3" class="right"><b>Credit Refunded:</b></td>
    <td class="right">&pound;{$order->getRounded('refundCreditValue')}</td>
</tr>
{if $order->hasAdminFee()}
<tr>
	<td colspan="3" class="right"><b>Admin Fee:</b></td>
	<td class="right">&pound;{$order->getAdminFee('rounded')}</td>
</tr>
{/if}
{/if}
</table>

{if !empty($order->transactions)}
	<h2>Transactions</h2>
	
	<table class="htmltable">
	<tr>
		<th>Type</th>
		<th>Cardholder</th>
		<th>Card number</th>
		<th>Date</th>
	</tr>
	{foreach from=$order->transactions item="transaction"}
	<tr>
		<td>{$transaction->type}</td>
		<td>{$transaction->cardHolder}</td>
		<td>{$transaction->cardNumber}</td>
		<td>{$transaction->dtc|date_format:"%d/%m/%Y"}</td>
	</tr>
	{/foreach}
	</table>
{/if}

{include file="@footer.tpl"}
