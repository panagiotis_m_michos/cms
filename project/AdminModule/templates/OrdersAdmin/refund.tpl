{include file="@header.tpl"}

    <h2>Refund for order #{$orderId} (Total: &pound;{$order->total})</h2>

    <div class="ajax_overlay_container">
        {$formHelper->start($form) nofilter}
        <fieldset>
            <legend>Items</legend>
            {foreach $order->items as $orderItem}
                {$itemView = $form['orderItems'][$orderItem->id]}
                {$formHelper->widget($itemView, ['attr' => ['data-refund-action' => 'calculate', 'data-refund-item-price' => $orderItem->totalPrice]]) nofilter}
                {$formHelper->label($itemView) nofilter}<br>
            {/foreach}
            <div class="errormsg1">
                {$formHelper->errors($form['orderItems']) nofilter}
            </div>
        </fieldset>
        <fieldset>
            <legend>Other</legend>
            <div>
                {$formHelper->row($form['itemsRefund'], ['attr' => ['class' => 'no-border no-background']]) nofilter}
            </div>
            <div>
                {$formHelper->label($form['adminFee']) nofilter}
                {$formHelper->widget($form['adminFee'], ['attr' => ['data-refund-action' => 'calculate']]) nofilter}
                <span class="errormsg1">
                        {$formHelper->errors($form['adminFee']) nofilter}
                    </span>
            </div>
            <div>
                {$formHelper->row($form['totalCreditRefund'], ['attr' => ['class' => 'no-border no-background']]) nofilter}
            </div>
            <div>
                {$formHelper->row($form['totalRefund'], ['attr' => ['class' => 'no-border no-background']]) nofilter}
            </div>
        </fieldset>
        <fieldset>
            <legend>Description</legend>
            {$formHelper->widget($form['description'], ['attr' => ['cols' => 80, 'rows' => 5]]) nofilter}
        </fieldset>
        <fieldset>
            <legend>Action</legend>
            {$formHelper->row($form['refund'], ['attr' => ['class' => 'btn']]) nofilter}
        </fieldset>
        {$formHelper->end($form) nofilter}
    </div>

    <script src="{$urlJs}OrderRefundForm.js"></script>
    <script type="text/javascript">
        var loader = new CMS.AjaxLoader('.ajax_overlay_container');
        var calculationUrl = '{$this->router->link()}';
        CMS.initOrderRefund(loader, calculationUrl);
    </script>

{include file="@footer.tpl"}