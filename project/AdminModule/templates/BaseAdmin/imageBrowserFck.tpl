{include file="@headerPopup.tpl"}

{include file="@blocks/jsTreeBrowserMenu.tpl"}

{if !empty($images)}

	

	<table class="htmlgrid">
	<col width="182">
	<col width="182">
	<col width="182">
	<col width="182">
	<tr>
		{foreach from=$images key="imageId" item="image" name="f1"}
		
			{capture name="onclick"}
				opener.document.getElementById('txtUrl').value = '{$image->getFilePath(TRUE)}';
				opener.document.getElementById('txtAlt').value = '{$image->name}';
				opener.document.getElementById('txtWidth').value = '{$image->width}';
				opener.document.getElementById('txtHeight').value = '{$image->height}';
				opener.UpdatePreview();
				self.close();
				return false;
			{/capture}
		
			<td align="center" onclick="{$smarty.capture.onclick}" style="cursor: pointer;">
				
				{$image->getHtmlTag("s") nofilter} <br />
				<strong>{$image->getFileName()}</strong> <br />
				Ord: {$image->ord}, 
				{assign var="author" value=$image->getAuthor()}
				Author: {$author->login}
				<br />
				{$image->width} x {$image->height} ({$image->getSize(true)})
			</td>
	
			{* NEW ROW? *}		
			{if $smarty.foreach.f1.iteration % 4 == 0}
				</tr><tr>
			{/if}
		{/foreach}
	</tr>
	</table>
	<div style="padding-left: 10px;">{$paginator nofilter}</div>
	
	
{else}
	<p>No files</p>
{/if}


{include file="@footerPopup.tpl"}