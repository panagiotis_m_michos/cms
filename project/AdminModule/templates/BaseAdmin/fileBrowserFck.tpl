{include file="@headerPopup.tpl"}

{* TABS *}
{include file="@blocks/tabs.tpl"}

{include file="@blocks/jsTreeBrowserMenu.tpl"}

{if !empty($files)}
	<table class="htmltable">
	<col width="50">
	<col>
	<col width="50">
	<col width="100">
	<col width="60">
	<col width="60">
	<col width="100">
	<tr>
		<th>Id</th>
		<th>Name</th>
		<th>Ext</th>
		<th>Size</th>
		<th>Active</th>
		<th>Ord</th>
		<th>Created</th>
	</tr>
	{foreach from=$files key="fileId" item="file"}
	
	{capture name="onclick"}
		opener.document.getElementById('txtUrl').value = '{$file->getFilePath(TRUE)}';
		opener.document.getElementById('cmbLinkProtocol').value = '';
		opener.BrowseServer();
		self.close();
		return false;			
	{/capture}
	
	<tr onclick="{$smarty.capture.onclick}" style="cursor: pointer;">
		<td class="right">{$file->getId()}</td>
		<td>{$file->name}</td>
		<td class="center">{$file->ext}</td>
		<td class="right">{$file->getSize(true)}</td>
		<td class="center">{$file->isActive}</td>
		<td class="right">{$file->ord}</td>
		<td class="center">{$file->dtc|date_format:"%d/%m/%Y"}</td>
	</tr>
	
	{/foreach}
	</table>
	
	{* PAGINATOR *}
	{$paginator nofilter}
{else}
	<p>No files</p>
{/if}


{include file="@footerPopup.tpl"}