
{* TABS JUST FOR FCK *}
{if isset($fck)}
	{include file="@headerPopup.tpl"} 
	{include file="@blocks/tabs.tpl"}
{else}
	{include file="@header.tpl"}
{/if}

{if !empty($nodes)}
	<table class="htmltable">
	<col width="70">
	<tr>
		<th class="right">Id</th>
		<th>Title</th>
	</tr>
	{foreach from=$nodes key="nodeId" item="title"}
	<tr>
		<td class="right">{$nodeId}</td>
		{if isset($fck)}
			{capture name="onclick"}
			opener.document.getElementById('txtUrl').value = '[page {$nodeId}]';
			opener.document.getElementById('cmbLinkProtocol').value = '';
			opener.BrowseServer();
			self.close();
			return false;
			{/capture}
		
			<td style="cursor: pointer;" onclick="{$smarty.capture.onclick}">{$title}</td>
		{else}
			<td style="cursor: pointer;" onclick="window.location.href='{$this->router->link($nodeId)}'">{$title}</td>
			
		{/if}
	</tr>
	{/foreach}
	</table>
{else}
	<p>No pages</p>
{/if}

{if isset($fck)}
	{include file="@footerPopup.tpl"}
{else}
	{include file="@footer.tpl"}
{/if}