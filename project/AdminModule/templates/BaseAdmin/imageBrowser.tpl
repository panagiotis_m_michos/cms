{include file="@headerPopup.tpl"}

{include file="@blocks/jsTreeBrowserMenu.tpl"}

{if !empty($images)}

	
	<table class="htmlgrid">
	<col width="182">
	<col width="182">
	<col width="182">
	<col width="182">
	<tr>
		{foreach from=$images key="imageId" item="image" name="f1"}
		
			{capture name="onclick"}
				parent.$('#{$inputName}').val({$imageId});
				parent.$('#{$inputName}_span').html('{$image->getHtmlTag("s")|h}');
				parent.$('#{$inputName}_add').attr('style', 'display: none;');
				parent.$('#{$inputName}_remove').attr('style', 'display: inline;');
				self.parent.tb_remove();
				return false;
			{/capture}
		
			<td align="center" onclick="{$smarty.capture.onclick}" style="cursor: pointer;">
				
				{$image->getHtmlTag("s") nofilter} <br />
				<strong>{$image->getFileName()}</strong> <br />
				Ord: {$image->ord}, 
				{assign var="author" value=$image->getAuthor()}
				Author: {$author->login}
				<br />
				{$image->width} x {$image->height} ({$image->getSize(true)})
			</td>
	
			{* NEW ROW? *}		
			{if $smarty.foreach.f1.iteration % 4 == 0}
				</tr><tr>
			{/if}
		{/foreach}
	</tr>
	</table>
	<div style="padding-left: 10px;">{$paginator nofilter}</div>
	
{else}
	<p>No files</p>
{/if}


{include file="@footerPopup.tpl"}