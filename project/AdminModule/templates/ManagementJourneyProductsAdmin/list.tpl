{include file="@header.tpl"}

{* SEARCH FORM *}
{$form nofilter}

<h2>List of customers who were interested about journey products</h2>
<table class="htmltable">
<col width="50" />
<col width="130" />
<col width="190" />
<col />
<col width="95" />
<col width="90" />
<col width="70" />
<tr>
	<th>Id</th>
	<th>Full Name</th>
	<th>Email</th>
	<th>Phone</th>
	<th>CustomerId</th>
	<th>Created</th>
	<th>Action</th>
</tr>
{foreach from=$customers key="id" item="customer"}
<tr>
	<td>{$customer->journeyCustomerId}</td>
	<td>{$customer->fullName}</td>
	<td>{$customer->email}</td>
	<td>{$customer->phone}</td>
	<td class="center">{$customer->customerId}</td>
	<td class="center">{$customer->dtc|date_format:"%d/%m/%y"}</td>
	<td class="center"><a href="{$this->router->link("view", "journeyCustomerId=$id")}">view</a></td>
</tr>
{/foreach}
</table>

{* PAGINATOR *}
{$paginator nofilter}

{include file="@footer.tpl"}