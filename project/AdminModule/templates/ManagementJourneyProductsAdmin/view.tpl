{include file="@header.tpl"}

<h2>Customer info</h2>

<table class="htmltable">
<col width="200">
<tr>
	<th>Id</th>
	<td>{$journeyCustomer->getId()}</td>
</tr>
	<th>Full Name</th>
	<td>{$journeyCustomer->fullName}</td>
</tr>
	<th>Email</th>
	<td>{$journeyCustomer->email}</td>
</tr>
	<th>Phone</th>
	<td>{$journeyCustomer->phone}</td>
</tr>
	<th>Post code</th>
	<td>{$journeyCustomer->postcode}</td>
</tr>
	<th>Customer id</th>
	<td>{$journeyCustomer->customerId}</td>
</tr>
	<th>Created</th>
	<td>{$journeyCustomer->dtc|date_format:"%d/%m/%Y"}</td>
</tr>
</tr>
</table>

<h2>Products</h2>
<table class="htmltable">
<col width="80">
<col>
<col width="100">
<tr>
	<th class="center">Id</th>
	<th>Product name</th>
	<th class="center">Product id</th>
</tr>
{foreach from=$journeyCustomer->getJourneyCustomerProducts() key="id" item="item"}
<tr>
	<td class="center">{$item->journeyCustomerProductId}</td>
	<td class="left">{$item->productName}</td>
	<td class="center">{$item->productId}</td>
</tr>
{/foreach}
</table>

{include file="@footer.tpl"}