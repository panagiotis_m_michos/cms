<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="{$currLng}">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="content-language"  content="{$currLng}" />
	<meta name="author" content="{$author}"/>
	<meta name="copyright" content="{$copyright}"/>
	<title>{$title}</title>

	<!-- CSS -->
	{styles file='webloader_admin.neon' section='common'}

	<!-- JAVASCRIPT -->
	{scripts file='webloader_admin.neon' section='common'}

</head>
<body>
<div id="popupEnvelope">
	<h1>{$title}</h1>
