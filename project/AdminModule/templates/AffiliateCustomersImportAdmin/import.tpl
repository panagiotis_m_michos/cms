{include file="@header.tpl"}

<div class="flash info2">
	<b>CSV STRUCTURE:</b> <br />
	<br />
	<ul style="margin-left: 30px;">
		<li>Needs to have a header row with the following rows A-L
			<ul style="margin-left: 30px; list-style-type: upper-alpha">
				<li>Email</li>
				<li>Title</li>
				<li>First Name</li>
				<li>Last Name</li>
				<li>Address 1</li>
				<li>Address 2</li>
				<li>Address 3</li>
				<li>City</li>
				<li>County</li>
				<li>Postcode</li>
				<li>Country</li>
				<li>Phone</li>
			</ul>
		<li>All are compulsory except for: Title, Address 2, Address 3, County</li>
	</ul>
</div>

{$form nofilter}

{include file="@footer.tpl"}