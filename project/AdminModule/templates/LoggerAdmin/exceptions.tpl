{include file="@header.tpl"}

    {* FORM *}
    {if isset($form)}
        {$form nofilter}
    {/if}

    {* EXEPTION *}
    <table class="htmltable">
        <col />
        <col width="180" />
        <col width="50" />
        <col width="60" />
        <tr>
            <th>Short Description</th>
            <th>Created</th>
            <th class="center" colspan="2">Action</th>
        </tr>
        {if $files}
            {foreach $files as $file}
                <tr>
                    <td class="noOverflow">
                        <div>{$file->getTitle()}</div>
                        <div>{$file->getDescription()}</div>
                    </td>
                    <td>{$file->getDtcString()}</td>
                    <td class="center"><a href="{$this->router->link("viewException", "fileId={$file->getFileId()}")}" target="_blank">View</a></td>
                    <td class="center">
                        <form action="{$this->router->link("deleteException")}" method="post">
                            <input type="hidden" name="fileId" value="{$file->getFileId()}" />
                            <input style="width:45px;" type="submit" value="Delete" onclick="return confirm('Are you sure?')" />
                        </form>
                    </td>
                </tr>
            {/foreach}
        {else}
            <tr>
                <td colspan="4">No Exceptions</td>
            </tr>
        {/if}
    </table>

{include file="@footer.tpl"}
