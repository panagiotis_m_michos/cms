{include file="@header.tpl"}

    <textarea id="log-area" cols="102" rows="30" readonly="readonly">{$logContent}</textarea>
    <script type="text/javascript">
        $('#log-area').scrollTop($('#log-area')[0].scrollHeight);
    </script>

{include file="@footer.tpl"}
