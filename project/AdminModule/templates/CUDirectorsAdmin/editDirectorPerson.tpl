{include file="@header.tpl"}

{* JS FOR PREFILL *}
<script type="text/javascript">
/* <![CDATA[ */
{$jsPrefillAdresses nofilter}
/* ]]> */
</script>

{literal}
<script>
$(document).ready(function () {
	
	// prefill address	
	$("#prefillAddress").change(function () {
		var value = $(this).val();
		address = addresses[value];
		for (var name in address) {
			$('#'+name).val(address[name]);
		}
	});
	
	
});
</script>
{/literal}
<div id="maincontent1"> 
<h1>{$title}</h1>

<p style="margin: 20px 0 25px 0; font-size: 11px;">
	<a href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}">{$companyName}</a> &gt; {$title}
</p>

{$form nofilter}
</div>
{include file="@footer.tpl"}