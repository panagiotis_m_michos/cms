{include file="@header.tpl"}

<div id="maincontent2">
	<h1>My Companies</h1>
	
	{* NOT INCOPORATED COMPANIES *}
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="my-companies">
	<col>
	<col width="180">
	<col width="120">
	<tr>
		<th colspan="3" class="bbottom">Not Incorporated</th>
	</tr>
	{if !empty($notIncorporated)}
		<tr class="odd midfont">
			<td><strong>Company Name</strong></td>
			<td align="center"><strong>Date of last submission</strong></td>
			<td><strong>Status</strong></td>
		</tr>
		{foreach from=$notIncorporated key="key" item="company" name="f1"}
			<tr{if $smarty.foreach.f1.iteration % 2 == 0} class="odd"{/if}>
				<td><a href="{$company.link}" title="Company ({$key})">{$company.company_name}</a></td>
				<td align="center">{$company.last_submission_date}</td>
				<td class="center">{$company.status|capitalize:true}</td>
			</tr>
		{/foreach} 
	{else}
		<tr>
			<td colspan="3">No companies</td>
		</tr>
	{/if}
	</table>
	
	{* INCOPORATED COMPANIES *}
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="my-companies">
	<col>
	<col width="150">
	<col width="180">
	<col width="120">
	<tr>
		<th colspan="4" class="bbottom">Incorporated</th>
	</tr>
	{if !empty($incorporated)}
		<tr class="odd midfont">
			<td><strong>Company Name</strong></td>
			<td><strong>Company Number</strong></td>
			<td align="center"><strong>Date of incorporation</strong></td>
			<td><strong>Status</strong></td>
		</tr>
		{foreach from=$incorporated key="key" item="company" name="f1"}
			<tr{if $smarty.foreach.f1.iteration % 2 == 0} class="odd"{/if}>
				<td><a href="{$company.link}" title="Company ({$key})">{$company.company_name}</a></td>
				<td align="center">{$company.company_number}</td>
				<td align="center">{$company.incorporation_date}</td>
				<td class="center">{$company.status|capitalize:true}</td>
			</tr>
		{/foreach} 
	{else}
		<tr>
			<td colspan="4">No companies</td>
		</tr>
	{/if}
	</table>

	<div class="clear"></div>
</div>
  
{include file="@footer.tpl"}
