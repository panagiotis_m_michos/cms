{include file="@header.tpl"}

<table class="htmltable" width="700">
<col width="200">
<tr>
	<th colspan="2" class="center">Person</th>
</tr>
<tr>
	<th>Title</th>
	<td>{$director.title}</td>
</tr>
<tr>
	<th>First name</th>
	<td>{$director.forename}</td>
</tr>
<tr>
	<th>Last name</th>
	<td>{$director.surname}</td>
</tr>
<tr>
	<th>DOB</th>
	<td>{$director.dob}</td>
</tr>
<tr>
	<th>Nationality</th>
	<td>{$director.nationality}</td>
</tr>
<tr>
	<th>Occupation</th>
	<td>{$director.occupation}</td>
</tr>
<tr>
	<th>Country of residence</th>
	<td>{$director.country_of_residence}</td>
</tr>
<tr>
	<th colspan="2" class="center">Service Address</th>
</tr>
<tr>
	<th>Address 1</th>
	<td>{$director.premise}</td>
</tr>
<tr>
	<th>Address 2</th>
	<td>{$director.street}</td>
</tr>
<tr>
	<th>Address 3</th>
	<td>{$director.thoroughfare}</td>
</tr>
<tr>
	<th>Town</th>
	<td>{$director.post_town}</td>
</tr>
<tr>
	<th>County</th>
	<td>{$director.county}</td>
</tr>
<tr>
	<th>Postcode</th>
	<td>{$director.postcode}</td>
</tr>
<tr>
	<th>Country</th>
	<td>{$director.country}</td>
</tr>

{*
<tr>
	<th colspan="2" class="center">Residential Address</th>
</tr>
<tr>
	<th>Address 1</th>
	<td>{$director.residential_premise}</td>
</tr>
<tr>
	<th>Address 2</th>
	<td>{$director.residential_street}</td>
</tr>
<tr>
	<th>Address 3</th>
	<td>{$director.residential_thoroughfare}</td>
</tr>
<tr>
	<th>Town</th>
	<td>{$director.residential_post_town}</td>
</tr>
<tr>
	<th>County</th>
	<td>{$director.residential_county}</td>
</tr>
<tr>
	<th>Postcode</th>
	<td>{$director.residential_postcode}</td>
</tr>
<tr>
	<th>Country</th>
	<td>{$director.residential_country}</td>
</tr>
*}
</table>

{include file="@footer.tpl"}
