{include file="@header.tpl"}

{if $view->canRemoveLink()}
    {assign var="linkRemoverForPending" value=1}
{/if}

<ul class="submenu">
    <li><a href="{$this->router->link("transfer", "company_id=$companyId")}">Transfer company</a></li>
        {if empty($linkRemoverForPending)}
        <li><a href="{$this->router->link("changeCompanyName", "company_id=$companyId")}">Change company name</a></li>
        {/if}
    <li><a href="{$this->router->link("copy", "company_id=$companyId")}">Copy</a></li>
    <li><a href="{$this->router->link("batchCopy", "company_id=$companyId")}">Batch Copy</a></li>


    {* LOCK AND UNLOCK COMPANY *}
    {if $company->isLocked()}
        <li><a href="{$this->router->link("unlockCompany", "company_id=$companyId")}">Unlock</a></li>
    {else}
        <li><a href="{$this->router->link("lockCompany", "company_id=$companyId")}">Lock</a></li>
    {/if}
</ul>

{if $view->canShowInternalFailureWarning()}
    <div class="flash error">
		This form submission received the error <b>Internal Failure</b> from Companies House.
        This means the submission has been sent to Companies House and is still pending, but something happened at their
        end that they need to resolve. The usual fix is to wait a few hours (the system will keep checking with
        Companies House to see if it's accepted or rejected). You can reduce the wait by calling Companies House
        (029&nbsp;20380929) and asking them about this specific form submission. The Operations Team has been notified
        about this error.

        You can tell the customer that their submission is fine, but Companies House have a technical issue they need
        to resolve. Will we automatically email the customer as soon as it's fixed (this will be the accepted or
        rejected email).
	</div>
{/if}

{* COMPANY *}
<h2>Customer</h2>
<table class="htmltable">
    <col width="150">
    <tr>
        <th>Customer: </th>
        <td><a href="{$this->router->link("CustomersAdminControler::CUSTOMERS_PAGE view", "customer_id=$customerId")}">{$customer->firstName} {$customer->lastName}</a></td>
    </tr>
    <tr>
        <th>Email: </th>
        <td>{$customer->email}</td>
    </tr>
</table>

<h2>Company Details</h2>
<table class="htmltable">
    <col width="250">
    <tr>
        <th>Locked: </th>
        <td>{if $company->isLocked()}<span style="color: red; font-weight: bold;">YES</span>{else}NO{/if}</td>
    </tr>
    <tr>
        <th>Company name: </th>
        <td>{$companyName}</td>
    </tr>
    <tr>
        <th>Company Category:</th>
        <td>{$companyType}</td>
    </tr>
    <tr>
        <th>Status:</th>
        <td>{$companyStatus}</td>
    </tr>
</table>

<h2>Registered Office
    {if empty($linkRemoverForPending)}<a href="{$this->router->link("CFupdateOffice", "company_id=$companyId")}" style="font-size: 10px;">[update]</a> {/if}</h2>
<table class="htmltable">
    <col width="250">
    {if !empty($data.registered_office.premise)}
        <tr>
            <th>Premise</th>
            <td>{$data.registered_office.premise}</td>
        </tr>
        <tr>
            <th>Street</th>
            <td>{$data.registered_office.street}</td>
        </tr>
        <tr>
            <th>Thoroughfare</th>
            <td>{$data.registered_office.thoroughfare}</td>
        </tr>
        <tr>
            <th>Post town</th>
            <td>{$data.registered_office.post_town}</td>
        </tr>
        <tr>
            <th>County</th>
            <td>{$data.registered_office.county}</td>
        </tr>
        <tr>
            <th>Postcode</th>
            <td>{$data.registered_office.postcode}</td>
        </tr>
        <tr>
            <th>Country</th>
            <td>{$data.registered_office.country}</td>
        </tr>
    {else}
        <tr>
            <td>No Registered Office</td>
        </tr>
    {/if}
</table>

{if $companyType != 'LLP'}
    {* STATEMENT OF CAPITAL *}
    <h2>Statement of Capital </h2>
    <table class="htmltable">
        <col width="200">
        {*
        {if !empty($data.capitals)}
        {foreach from=$data.capitals key="id" item="capitals"}
        {if (count($data.capitals) > 1)}
        <tr>
        <th>Share Capital {$id+1}</th>
        <th></th>
        </tr>
        {/if}
        <tr>
        <th>Share Currency</th>
        <td>{$capitals.currency}</td>
        </tr>
        <tr>
        <th>Total Aggregate Value</th>
        <td>{$capitals.total_aggregate_value}</td>
        </tr>
        <tr>
        <th>Total Issued</th>
        <td>{$capitals.total_issued}</td>
        </tr>

        {foreach from=$capitals.shares key="id2" item="shares"}
        {if (count($capitals.shares) > 1)}
        <tr>
        <th>Shares {$id2+1}</th>
        <td></td>
        </tr>
        {/if}
        <tr>
        <th>Share Class</th>
        <td>{$shares.share_class}</td>
        </tr>
        <tr>
        <th>Number of shares</th>
        <td>{$shares.num_shares}</td>
        </tr>
        <tr>
        <th>Amount Paid Due Per Share</th>
        <td>{$shares.amount_paid}</td>
        </tr>
        <tr>
        <th>Amount Unpaid Due Per Share</th>
        <td>{$shares.amount_unpaid}</td>
        </tr>
        <tr>
        <th>Aggregate Nominal Value</th>
        <td>{$shares.nominal_value}</td>
        </tr>

        {/foreach}
        {/foreach}
        {else} *}
        <tr>
            <td colspan="4">No Statement of Capital</td>
        </tr>
        {*{/if} *}
    </table>

{/if}

<h2>{if $companyType != 'LLP'}	Directors {else} Members {/if}
    {if empty($linkRemoverForPending)}<a href="{$this->router->link("CFaddDirectorPerson", "company_id=$companyId")}" style="font-size: 10px;">[appoint new {if $companyType != 'LLP'}director{else}member{/if}]</a>{/if}</h2>
<table class="htmltable">
    <col>
    <col width="150">
    <tr>
        <th>Name</th>
        <th class="center">Action</th>
    </tr>
    {if !empty($data.directors)}
        {foreach from=$data.directors key="id" item="director"}
            <tr>
                <td>
                    {if $director.corporate}
                        {$director.corporate_name}
                    {else}
                        {$director.title} {$director.forename} {$director.surname}
                    {/if}

                    {if $director.nominee}
                        {if empty($director.nomineeType)}
                            (Nominee Director)
                        {else}
                            ({$director.nomineeType})
                        {/if}
                    {/if}
                </td>
                <td class="center">
                    {if $director.corporate}
                        <a href="{$this->router->link("CFshowDirectorCorporate", "company_id=$companyId", "company_id=$companyId", "director_id=$id")}">View</a>
                        {if empty($linkRemoverForPending)} /
                            <a href="{$this->router->link("CFeditDirectorCorporate", "company_id=$companyId", "director_id=$id")}">Edit</a> /
                            <a href="{$this->router->link("CFdeleteDirectorPerson", "company_id=$companyId", "director_id=$id")}" onclick="return confirm('Are you sure?');">Remove</a>
                        {/if}
                    {else}
                        <a href="{$this->router->link("CFshowDirectorPerson", "company_id=$companyId", "director_id=$id")}">View</a>
                        {if empty($linkRemoverForPending)} /
                            <a href="{$this->router->link("CFeditDirectorPerson", "company_id=$companyId", "director_id=$id")}">Edit</a> /
                            <a href="{$this->router->link("CFdeleteDirectorCorporate", "company_id=$companyId", "director_id=$id")}" onclick="return confirm('Are you sure?');">Remove</a>
                        {/if}
                    {/if}
                </td>
            </tr>
        {/foreach}
    {else}
        <tr>
            <td colspan="2">No directors</td>
        </tr>
    {/if}
</table>

{if $companyType != 'LLP'}
    {* SHAREHOLDERS *}
    <h2>Shareholders
        {if empty($linkRemoverForPending)}<a href="{$this->router->link("CFaddShareholderPerson", "company_id=$companyId")}" style="font-size: 10px;">[appoint new shareholder]</a>{/if}</h2>
    <table class="htmltable">
        <col>
        <col width="150">
        <tr>
            <th>Name</th>
            <th class="center">Action</th>
        </tr>
        {if !empty($data.shareholders)}
            {foreach from=$data.shareholders key="id" item="shareholder"}
                <tr>
                    {if $shareholder.corporate}
                        <td>{$shareholder.corporate_name} {if $shareholder.nominee} (Nominee){/if}</td>
                    {else}
                        <td>{$shareholder.title} {$shareholder.forename} {$shareholder.surname} {if $shareholder.nominee} (Nominee){/if}</td>
                    {/if}

                    <td class="center">
                        {if $shareholder.corporate}
                            <a href="{$this->router->link("CFshowShareholderCorporate", "company_id=$companyId", "shareholder_id=$id")}">View</a>
                            {if empty($linkRemoverForPending)} /
                                <a href="{$this->router->link("CFeditShareholderCorporate", "company_id=$companyId", "shareholder_id=$id")}">Edit</a> /
                                <a href="{$this->router->link("CFdeleteShareholderCorporate", "company_id=$companyId", "shareholder_id=$id")}" onclick="return confirm('Are you sure?');">Remove</a>
                            {/if}
                        {else}
                            <a href="{$this->router->link("CFshowShareholderPerson", "company_id=$companyId", "shareholder_id=$id")}">View</a>
                            {if empty($linkRemoverForPending)} /
                                <a href="{$this->router->link("CFeditShareholderPerson", "company_id=$companyId", "shareholder_id=$id")}">Edit</a> /
                                <a href="{$this->router->link("CFdeleteShareholderPerson", "company_id=$companyId", "shareholder_id=$id")}" onclick="return confirm('Are you sure?');">Remove</a>
                            {/if}
                        {/if}
                    </td>
                </tr>
            {/foreach}
        {else}
            <tr>
                <td colspan="2">No shareholders</td>
            </tr>
        {/if}
    </table>
{/if}

{if Feature::featurePsc()}
    <h2>
        PSCs
        {if empty($linkRemoverForPending)}
            <a href="{url route="CFaddPscPerson" company_id=$companyId}" style="font-size: 10px;">[add person PSC]</a>
            <a href="{url route="CFaddPscCorporate" company_id=$companyId}" style="font-size: 10px;">[add corporate PSC]</a>
            <a href="{url route="CFnoPscReason" company_id=$companyId}" style="font-size: 10px;">[no registrable person notification]</a>
            {if $companyType != 'LLP'}
                <a href="{url route="CFaddSuggestedPscs" company_id=$companyId}" style="font-size: 10px;">[auto-suggest PSC]</a>
            {/if}
        {/if}
    </h2>
    <table class="htmltable">
        <tr>
            <th width="150">Name</th>
            <th>Nature of Control</th>
            <th width="129" class="center">Action</th>
        </tr>
        {if $summaryView->hasPscs()}
            {foreach $summaryView->getPscs() as $psc}
                <tr>
                    <td>
                        {$psc->getFullName()}
                    </td>
                    <td>
                        {foreach $psc->getNatureOfControlsTexts() as $text}
                            {$text}<br>
                        {/foreach}
                    </td>
                    <td class="center">
                        {if $psc->isCorporate()}
                            <a href="{url route="CFshowPscCorporate" company_id=$companyId psc_id=$psc->getId()}">View</a>
                            {if empty($linkRemoverForPending)} /
                                <a href="{url route="CFeditPscCorporate" company_id=$companyId psc_id=$psc->getId()}">Edit</a> /
                                <a href="{url route="CFdeletePscCorporate" company_id=$companyId psc_id=$psc->getId()}" onclick="return confirm('Are you sure?');">Remove</a>
                            {/if}
                        {else}
                            <a href="{url route="CFshowPscPerson" company_id=$companyId psc_id=$psc->getId()}">View</a>
                            {if empty($linkRemoverForPending)} /
                                <a href="{url route="CFeditPscPerson" company_id=$companyId psc_id=$psc->getId()}">Edit</a> /
                                <a href="{url route="CFdeletePscPerson" company_id=$companyId psc_id=$psc->getId()}" onclick="return confirm('Are you sure?');">Remove</a>
                            {/if}
                        {/if}
                    </td>
                </tr>
            {/foreach}
        {elseif $summaryView->hasNoPscReason()}
            <tr>
                <td colspan="3"><i>{$summaryView->getNoPscReason()}</i></td>
            </tr>
        {else}
            <tr>
                <td colspan="3">No PSCs</td>
            </tr>
        {/if}
    </table>
{/if}

{if $companyType != 'LLP'}
    {* SECRETARIES *}
    <h2>Secretaries
        {if empty($linkRemoverForPending)}<a href="{$this->router->link("CFaddSecretaryPerson", "company_id=$companyId")}" style="font-size: 10px;">[appoint new secretary]</a>{/if}</h2>
    <table class="htmltable">
        <col>
        <col width="150">
        <tr>
            <th>Name</th>
            <th class="center">Action</th>
        </tr>
        {if !empty($data.secretaries)}
            {foreach from=$data.secretaries key="id" item="secretary"}
                <tr>
                    <td>
                        {if $secretary.corporate}
                            {$secretary.corporate_name}
                        {else}
                            {$secretary.title} {$secretary.forename} {$secretary.surname}
                        {/if}

                        {if $secretary.nominee} (Nominee){/if}
                    </td>
                    <td class="center">
                        {if $secretary.corporate}
                            <a href="{$this->router->link("CFshowSecretaryCorporate", "company_id=$companyId", "secretary_id=$id")}">View</a>
                            {if empty($linkRemoverForPending)} /
                                <a href="{$this->router->link("CFeditSecretaryCorporate", "company_id=$companyId", "secretary_id=$id")}">Edit</a> /
                                <a href="{$this->router->link("CFdeleteSecretaryCorporate", "company_id=$companyId", "secretary_id=$id")}" onclick="return confirm('Are you sure?');">Remove</a>
                            {/if}
                        {else}
                            <a href="{$this->router->link("CFshowSecretaryPerson", "company_id=$companyId", "secretary_id=$id")}">View</a>
                            {if empty($linkRemoverForPending)} /
                                <a href="{$this->router->link("CFeditSecretaryPerson", "company_id=$companyId", "secretary_id=$id")}">Edit</a> /
                                <a href="{$this->router->link("CFdeleteSecretaryPerson", "company_id=$companyId", "secretary_id=$id")}" onclick="return confirm('Are you sure?');">Remove</a>
                            {/if}
                        {/if}
                    </td>
                </tr>
            {/foreach}
        {else}
            <tr>
                <td colspan="2">No secretaries</td>
            </tr>
        {/if}
    </table>
{/if}

<h2>Incorporation Documents
    {if empty($linkRemoverForPending)}<a href="{$this->router->link("CFupdateDocuments", "company_id=$companyId")}" style="font-size: 10px;">[update documents]</a>{/if}</h2>
<table class="htmltable" width="700">
    <col>
    <col width="100">
    <tr>
        <th>Name</th>
        <th align="center">Action</th>
    </tr>
    {if !empty($documents)}
        {foreach from=$documents item='document'}
            <tr>
                <td>{$document.filename}</td>
                {assign var="documentId" value=$document.id}
                <td align="center"><a href="{$this->router->link(null, "company_id=$companyId", "document_id=$documentId")}">download</a></td>
            </tr>
        {/foreach}
    {else}
        <tr>
            <td colspan="2">No documents</td>
        </tr>
    {/if}
</table>

{* FORM SUBMISSIONS *}
<h2>Form submissions</h2>
<table class="htmltable">
    {if $view->hasFormSubmissions()}
        <col width="70">
        <col width="170">
        <col width="130">
        <col width="160">
        <tr>
            <th>Id</th>
            <th>Form identifier</th>
            <th>Response</th>
            <th>Action</th>
        </tr>
        {foreach $view->getFormSubmissions() as $submission}
            {assign var="submissionId" value=$submission->getId()}
            <tr>
                <td>{$submissionId}</td>
                <td>{$submission->getFormIdentifier()}</td>
                <td>
                    {if $submission->hasResponse()}
                        {$submission->getResponseMessage()}
                        {if $submission->isError()}
                            &nbsp;&nbsp;<a href="{$this->router->link("removeSubmissionError", "type=$type",  "company_id=$companyId", "submission_id=$submissionId")}">(remove)</a>
                        {/if}
                    {else}
                        <span class="description">(not submitted)</span>
                    {/if}
                    {if $submission->isCancelled()}
                        <br><span class="warning">Cancelled ({$submission->getDateCancelled()|datetime})</span>
                    {/if}
                </td>
                <td>
                    <a href="{$this->router->link('formSubmission', ['company_id' => $companyId, 'submission_id' => $submissionId])}">view</a>
                    {if $view->canResubmit($user, $submission)}
                        |
                        {capture "resubmitConfirmation"}
                            This will cancel the current form submission and will resubmit the form
                            with a new ID. By cancelling this submission we won't query Companies House for updates on
                            its status. If this submission is ever accepted we won't know about it. Only proceed if you
                            know what you are doing. Are you sure you want to cancel the current submission and resubmit
                            the form with using a new ID?
                        {/capture}
                        <a data-confirmation="{$smarty.capture.resubmitConfirmation|strip:" "}" href="{$this->router->link("resubmitFormSubmission", "company_id=$companyId", "submission_id=$submissionId")}">
                            resubmit with new ID
                        </a>
                    {/if}
                </td>
            </tr>
        {/foreach}
    {else}
        <tr>
            <td colspan="4">No form submissions</td>
        </tr>
    {/if}
</table>

<div class="flash error" style="margin: 25px 0 0 0; text-align: center;">
    {if isset($form)}
        {$form->getBegin() nofilter}
        {$form->getControl("send") nofilter}
        {$form->getEnd() nofilter}
    {/if}
</div>

{include file="@footer.tpl"}
