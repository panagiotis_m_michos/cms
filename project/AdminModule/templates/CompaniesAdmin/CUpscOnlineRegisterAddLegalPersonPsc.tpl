{include file="@header.tpl"}

{literal}
    <script>
        $(function () {
            toogleServiceAddress();

            $("#serviceAddress").click(function (){
                toogleServiceAddress();
            });

            function toogleServiceAddress() {
                var disabled = !$("#serviceAddress").is(":checked");
                $("input[id^='service_'], select[id^='service_']").each(function () {
                    $(this).attr("disabled", disabled);
                });
            }

            function setCheckedState($radios) {
                $.each($radios, function (i, e) {
                    $(e).data('checked', e.checked);
                });
            }

            var $container = $('.nature-of-control-container');
            setCheckedState($container);

            $($container).on('click', ':radio', function () {
                var newState = !$(this).data('checked');
                setCheckedState($container);

                $(this).prop('checked', newState);
                $(this).data('checked', newState);
            });
        });
    </script>
{/literal}

{$form->getBegin() nofilter}
{if $form->getErrors()|@count gt 0}
    <p class="ff_err_notice ff_red_err" style="width: 940px">Form has <b> {$form->getErrors()|@count}</b> error(s). See below for more details:</p>
{/if}

<fieldset>
    <legend>Legal Person</legend>
    <table class="ff_table">
        <tbody>
        <tr>
            <th>{$form->getLabel('legal_person_name') nofilter}</th>
            <td>{$form->getControl('legal_person_name') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('legal_person_name')}</span></td>
        </tr>
        <tr>
            <th>{$form->getLabel('law_governed') nofilter}</th>
            <td>{$form->getControl('law_governed') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('law_governed')}</span></td>
        </tr>
        <tr>
            <th>{$form->getLabel('legal_form') nofilter}</th>
            <td>{$form->getControl('legal_form') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('legal_form')}</span></td>
        </tr>
        </tbody>
    </table>
</fieldset>

<fieldset>
    <legend>Address</legend>
    <table class="ff_table">
        <tbody>
        <tr>
            <th>{$form->getLabel('premise') nofilter}</th>
            <td>{$form->getControl('premise') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('premise')}</span></td>
        </tr>
        <tr>
            <th>{$form->getLabel('street') nofilter}</th>
            <td>{$form->getControl('street') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('street')}</span></td>
        </tr>
        <tr>
            <th>{$form->getLabel('thoroughfare') nofilter}</th>
            <td>{$form->getControl('thoroughfare') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('thoroughfare')}</span></td>
        </tr>
        <tr>
            <th>{$form->getLabel('post_town') nofilter}</th>
            <td>{$form->getControl('post_town') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('post_town')}</span></td>
        </tr>
        <tr>
            <th>{$form->getLabel('county') nofilter}</th>
            <td>{$form->getControl('county') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('county')}</span></td>
        </tr>
        <tr>
            <th>{$form->getLabel('postcode') nofilter}</th>
            <td>{$form->getControl('postcode') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('postcode') nofilter}</span></td>
        </tr>
        <tr>
            <th>{$form->getLabel('country') nofilter}</th>
            <td>{$form->getControl('country') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('country')}</span></td>
        </tr>
        </tbody>
    </table>
</fieldset>

<fieldset class="nature-of-control-container">
    <legend>Nature of Control:</legend>
    {$natureOfControlsDescription}
    <span class="ff_control_err">{$form->getError('significant_influence_or_control')}</span>

    {foreach $natureOfControls as $radioName => $natureOfControl}
        <fieldset>
            <legend>{$natureOfControl['title']}</legend>
            <table class="ff_table" style="width: 100%">
                <tbody>
                {foreach $natureOfControl['groups'] as $group}
                    {if isset($group['title'])}
                        <tr>
                            <td><b>{$group['title']}</b></td>
                        </tr>
                    {/if}
                    {if isset($group['description'])}
                        <tr>
                            <td><i>{$group['description']}</i></td>
                        </tr>
                    {/if}
                    {foreach $group['options'] as $optionKey => $option}
                        <tr>
                            <td>
                                {$form[$radioName]->getControl($optionKey) nofilter}</td>
                            </td>
                        </tr>
                    {/foreach}
                {/foreach}
                </tbody>
            </table>
        </fieldset>
    {/foreach}
</fieldset>

<fieldset id="fieldset_2">
    <legend>Action</legend>
    {$form->getControl('continue') nofilter}
</fieldset>

{$form->getEnd() nofilter}

{include file="@footer.tpl"}
