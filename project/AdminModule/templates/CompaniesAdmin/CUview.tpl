{include file="@header.tpl"}

{if !isset($companyInfo)}
	<ul class="submenu">
		<li><a href="{$this->router->link("sync", "company_id=$companyId")}">Sync</a></li>
		<li><a href="{$this->router->link("transfer", "company_id=$companyId")}">Transfer</a></li>
		<li><a href="{$this->router->link("changeAuthenticationCode", "company_id=$companyId")}">Change Auth Code</a></li>
		{*<li><a href="{$this->router->link("sendToBarclays", "company_id=$companyId")}">Send to Barclays</a></li>*}

		{if !$company->isDeleted()}
			{* HAS ANNUAL RETURN *}
			{if $company->hasAnnualReturn()}
				<li>
					<a href="{$this->router->link("ARAdminControler::ANNUAL_RETURN_PAGE companyDetails", "company_id=$companyId")}">Confirmation Statement</a></li>
			{else}
				<li><a href="{$this->router->link("CUaddAnnualReturn", "company_id=$companyId")}">Add Confirmation Statement</a>
				</li>
			{/if}
			{*<li><a href="*}{*$this->router->link("copy", "company_id=$companyId")*}{*">Copy</a></li>*}

			{* LOCK AND UNLOCK COMPANY *}
			{if $company->isLocked()}
				<li><a href="{$this->router->link("unlockCompany", "company_id=$companyId")}">Unlock</a></li>
			{else}
				<li><a href="{$this->router->link("lockCompany", "company_id=$companyId")}">Lock</a></li>
			{/if}

			{* HIDE AND UNHIDE COMPANY *}
			{if $company->isHidden()}
				<li><a href="{$this->router->link("unhideCompany", "company_id=$companyId")}">Unhide</a></li>
			{else}
				<li><a href="{$this->router->link("hideCompany", "company_id=$companyId")}">Hide</a></li>
			{/if}
			{* HAS CHANGE NAME *}
			<li><a href="{$this->router->link("CUaddCompanyNameChange", "company_id=$companyId")}"> Change Name</a></li>
			{* PRINT SUMMARY *}
			<li><a href="{$this->router->link("SummaryPdf", "company_id=$companyId")}">Company Statutory Info</a></li>
		{else}
			<li><a href="{$this->router->link("RestoreDeletedCompany", "company_id=$companyId")}">Restore</a></li>
		{/if}

		{* SERVICES *}
		<li>
			<a href="{$this->router->link("ServicesAdminControler::SERVICES_PAGE companyServicesList", "company_id=$companyId")}">Services</a>
		</li>
	</ul>
{/if}

{* COMPANY *}
<h2>Customer</h2>
<table class="htmltable">
	<col width="200">
	<tr>
		<th>Customer:</th>
		<td>
			<a href="{$this->router->link("CustomersAdminControler::CUSTOMERS_PAGE view", "customer_id=$customerId")}">{$customer->firstName} {$customer->lastName}</a>
		</td>
	</tr>
	<tr>
		<th>Email:</th>
		<td>{$customer->email}</td>
	</tr>
</table>

{if $company->isDeleted()}
	<h2>Deleted Details</h2>
	<table class="htmltable">
		<col width="200">
		<col>
		<col width="100">
		<tr>
			<th>Message:</th>
			<td colspan="2">{$companyDeletedInfo.rejectMessage}</td>
		</tr>
		<tr>
			<th>Date:</th>
			<td colspan="2">{$companyDeletedInfo.deletedDate|date_format:"%d/%m/%Y"}</td>
		</tr>
		{if isset($companyInfo) && $companyInfo}
			<tr>
				<th>Note:</th>
				<td>This company now belongs to: {$newCustomer->firstName} {$newCustomer->lastName}
				</td>
				<td align="center">
					{assign var="companyId" value=$companyInfo.company_id}
					<a href="{$this->router->link("CompaniesAdminControler::COMPANIES_PAGE view", "company_id=$companyId")}">View</a>
				</td>
			</tr>
		{/if}
	</table>
{/if}

<h2>Company Details</h2>
<table class="htmltable">
	<col width="200">
	<col>
	<col width="100">
	<tr>
		<th>Locked:</th>
		<td colspan="2">{if $company->isLocked()}<span style="color: red; font-weight: bold;">YES</span>{else}NO{/if}
		</td>
	</tr>
	<tr>
		<th>Hidden:</th>
		<td colspan="2">{if $company->isHidden()}<span style="color: red; font-weight: bold;">YES</span>{else}NO{/if}
		</td>
	</tr>
	<tr>
		<th>Company name:</th>
		<td colspan="2">{$data.company_details.company_name}</td>
	</tr>
	<tr>
		<th>Registration number:</th>
		<td colspan="2">{$data.company_details.company_number}</td>
	</tr>
	<tr>
		<th>Registered In:</th>
		{if $data.company_details.jurisdiction == 'EW'}
			<td colspan="2">England and Wales</td>
		{elseif $data.company_details.jurisdiction == 'SC'}
			<td colspan="2">Scotland</td>
		{elseif $data.company_details.jurisdiction == 'WA'}
			<td colspan="2">Wales</td>
		{elseif $data.company_details.jurisdiction == 'NI'}
			<td colspan="2">Northern Ireland</td>
		{elseif $data.company_details.jurisdiction == 'EU'}
			<td colspan="2">Europe</td>
		{elseif $data.company_details.jurisdiction == 'UK'}
			<td colspan="2">United Kingdom</td>
		{elseif $data.company_details.jurisdiction == 'EN'}
			<td colspan="2">England</td>
		{elseif $data.company_details.jurisdiction == 'OTHER'}
			<td colspan="2">Other</td>
		{else}
			<td colspan="2"></td>
		{/if}
	</tr>
	<tr>
	<th>Web Filing Authentication Code: </th>
	<td colspan="2">{$data.company_details.authentication_code}</td>
</tr>
<tr>
	<th>Company Category:</th>
	<td colspan="2">{$data.company_details.company_category}</td>
</tr>
	<tr>
		<th>Status:</th>
		<td colspan="2">{$companyStatus}</td>
	</tr>
	<tr>
		<th>Country of Origin:</th>
		<td colspan="2">{$data.company_details.country_of_origin}</td>
	</tr>
	<tr>
		<th>Incorporation Date:</th>
		<td colspan="2">{$data.company_details.incorporation_date|date_format:"%d/%m/%Y"}</td>
	</tr>
	<tr>
		<th>Nature of Business:</th>
		<td colspan="2">{$data.company_details.sic_codes}</td>
	</tr>
	<tr>
		<th>Accounting Reference Date:</th>
		<td>{$data.company_details.accounts_ref_date|replace:'-':'/'}</td>
		<td class="center">
			{if $data.company_details.company_category != 'LLP'}
				<a href="{$this->router->link("CUupdateAccountingReferenceDate", "company_id=$companyId")}">Change Date</a>
			{/if}
		</td>
	</tr>
	<tr>
		<th>Last Accounts Made Up To:</th>
		<td colspan="2">{$data.company_details.accounts_last_made_up_date|date_format:"%d/%m/%Y"}</td>
	</tr>
	<tr>
		<th>Next Accounts Due:</th>
		<td colspan="2">{$data.company_details.accounts_next_due_date|date_format:"%d/%m/%Y"}</td>
	</tr>
	<tr>
		<th>Last Confirmation Statement Made Up To:</th>
		<td colspan="2">{$data.company_details.returns_last_made_up_date|date_format:"%d/%m/%Y"}</td>
	</tr>
	<tr>
		<th>Next Confirmation Statement Due:</th>
		<td>{$data.company_details.returns_next_due_date|date_format:"%d/%m/%Y"}</td>
		<td class="center">
			{if $data.company_details.company_category != 'LLP'}
			<a onclick="alert('Our monkeys are working on it'); return false;"
			   href="{$this->router->link("CUFiling", "company_id=$companyId")}">File now</a></td>
		{/if}
	</tr>
	<tr>
		<th>EReminder</th>
		<td></td>
		<td><a href="{$this->router->link("CUsetCompanyEReminder", "company_id=$companyId")}">Set EReminder</a></td>
	</tr>
</table>

{* REGISTERED OFFICE *}
<h2>
	Registered Office
	<a href="{$this->router->link("CUupdateOffice", "company_id=$companyId")}" style="font-size: 10px;">[update]</a>
</h2>
<table class="htmltable">
	<col width="200">
	<tr>
		<th>Premise</th>
		<td>{$data.registered_office.premise}</td>
	</tr>
	<tr>
		<th>Street</th>
		<td>{$data.registered_office.street}</td>
	</tr>
	<tr>
		<th>Thoroughfare</th>
		<td>{$data.registered_office.thoroughfare}</td>
	</tr>
	<tr>
		<th>Post town</th>
		<td>{$data.registered_office.post_town}</td>
	</tr>
	<tr>
		<th>County</th>
		<td>{$data.registered_office.county}</td>
	</tr>
	<tr>
		<th>Postcode</th>
		<td>{$data.registered_office.postcode}</td>
	</tr>
	<tr>
		<th>Country</th>
		<td>{$data.registered_office.country}</td>
	</tr>
</table>

{if $data.company_details.company_category != 'LLP'}
	{* STATEMENT OF CAPITAL *}
	<h2>Statement of Capital </h2>
	<table class="htmltable">
		<col width="200">
		{if !empty($data.capitals)}
			{foreach from=$data.capitals key="id" item="capitals"}
				{if (count($data.capitals) > 1)}
					<tr>
						<th>Share Capital {$id+1}</th>
						<th></th>
					</tr>
				{/if}
				<tr>
					<th>Share Currency</th>
					<td>{$capitals.currency}</td>
				</tr>
				<tr>
					<th>Total Aggregate Value</th>
					<td>{$capitals.total_aggregate_value}</td>
				</tr>
				<tr>
					<th>Total Issued</th>
					<td>{$capitals.total_issued}</td>
				</tr>
				{foreach from=$capitals.shares key="id2" item="shares"}
					{if (count($capitals.shares) > 1)}
						<tr>
							<th>Shares {$id2+1}</th>
							<td></td>
						</tr>
					{/if}
					<tr>
						<th>Share Class</th>
						<td>{$shares.share_class}</td>
					</tr>
					<tr>
						<th>Number of shares</th>
						<td>{$shares.num_shares}</td>
					</tr>
					<tr>
						<th>Amount Paid Due Per Share</th>
						<td>{$shares.amount_paid}</td>
					</tr>
					<tr>
						<th>Amount Unpaid Due Per Share</th>
						<td>{$shares.amount_unpaid}</td>
					</tr>
					<tr>
						<th>Aggregate Nominal Value</th>
						<td>{$shares.nominal_value}</td>
					</tr>
				{/foreach}
			{/foreach}
		{else}
			<tr>
				<td colspan="4">No Statement of Capital</td>
			</tr>
		{/if}
	</table>
{/if}

{* DIRECTORS *}
<h2>
	{if $data.company_details.company_category != 'LLP'}
		Directors
		<a href="{$this->router->link("CUaddDirectorPerson", "company_id=$companyId")}" style="font-size: 10px;">
			[appoint new director]
		</a>
	{else}
		Members
		<a href="{$this->router->link("CUaddDirectorPerson", "company_id=$companyId")}" style="font-size: 10px;">
			[appoint new member]
		</a>
	{/if}
</h2>
<table class="htmltable">
	<col>
	<col width="50">
	<col width="50">
	<col width="70">
	<tr>
		<th>Name</th>
		<th class="center" colspan="3">Action</th>
	</tr>
	{if !empty($data.directors)}
		{foreach from=$data.directors key="id" item="director"}
			<tr>
				<td>
					{if $director.corporate}
						{$director.corporate_name}
					{else}
						{$director.title} {$director.forename} {$director.surname}
					{/if}
				</td>
				{if $director.corporate}
					<td class="center">
						<a href="{$this->router->link("CUshowDirectorCorporate", "company_id=$companyId", "company_id=$companyId", "director_id=$id")}">View</a>
					</td>
					<td class="center">
						<a href="{$this->router->link("CUeditDirectorCorporate", "company_id=$companyId", "director_id=$id")}">Edit</a>
					</td>
					<td class="center">
						<a href="{$this->router->link("CUresign", "company_id=$companyId", "officer_id=$id" , "resign=1")}">Resign</a>
					</td>
				{else}
					<td class="center">
						<a href="{$this->router->link("CUshowDirectorPerson", "company_id=$companyId", "director_id=$id")}">View</a>
					</td>
					<td class="center">
						<a href="{$this->router->link("CUeditDirectorPerson", "company_id=$companyId", "director_id=$id")}">Edit</a>
					</td>
					<td class="center">
						<a href="{$this->router->link("CUresign", "company_id=$companyId", "officer_id=$id" , "resign=1")}">Resign</a>
					</td>
				{/if}
			</tr>
		{/foreach}
	{else}
		<tr>
			<td colspan="4">No directors</td>
		</tr>
	{/if}
</table>

{if Feature::featurePsc()}
    <h2>
        People with Significant Control (PSC)
		<a href="{url route="CUpscOnlineRegister" company_id=$companyId}" style="font-size: 10px;">
			[view PSC online register]
		</a>
    </h2>
    <table class="htmltable">
		{* todo: psc - check NoPscReason first, PSCs after (everywhere) *}
		{if $summaryView->hasNoPscReason()}
			<tr>
				<td colspan="3"><i>{$summaryView->getNoPscReason()}</i></td>
			</tr>
		{elseif $summaryView->hasPscs()}
			<tr>
				<th width="150">Name</th>
				<th>Nature of Control</th>
				<th width="129" class="center">Action</th>
			</tr>
			{foreach $summaryView->getPscs() as $psc}
                <tr>
                    <td>
                        {$psc->getFullName()}
                    </td>
                    <td>
                        {foreach $psc->getNatureOfControlsTexts() as $text}
                            {$text}<br>
                        {/foreach}
                    </td>
                    <td class="center">
                        {if $psc->isCorporate()}
                            <a href="{url route="CUshowPscCorporate" company_id=$companyId psc_id=$psc->getId()}">View</a>
                        {else}
                            <a href="{url route="CUshowPscPerson" company_id=$companyId psc_id=$psc->getId()}">View</a>
                        {/if}
                    </td>
                </tr>
            {/foreach}
        {else}
            <tr>
                <td colspan="3">No PSCs</td>
            </tr>
        {/if}
    </table>
{/if}

{if $data.company_details.company_category != 'LLP'}
	{* SHAREHOLDERS *}
	<h2>
		Shareholders
		<a href="{$this->router->link("CUreturnOfAllotmentShares", "company_id=$companyId")}" style="font-size: 10px;">
			[return of allotment shares]
		</a>
	</h2>
	<table class="htmltable">
		<col>
		<col width="50">
		<col width="70">
		<tr>
			<th>Name</th>
			<th class="center" colspan="2">Action</th>
		</tr>
		{if !empty($data.shareholders)}
			{assign var=i value=1}
			{foreach from=$data.shareholders key="id" item="shareholder"}
				<tr>
					<td>
						{if $shareholder.corporate}
							{$shareholder.corporate_name}
						{else}
							{$shareholder.title} {$shareholder.forename} {$shareholder.surname}
						{/if}
					</td>

					{if $shareholder.corporate}
						<td class="center">
							<a href="{$this->router->link("CUshowShareholderCorporate", "company_id=$companyId", "shareholder_id=$id")}">View</a>
						</td>
						<td class="center">
							<a href="{$this->router->link("CompanyShareholderCerificateAdminControler::SHAREHOLDERS_CERTIFICATE_PAGE view", "company_id=$companyId", "shareHolder=$id")}">Print</a>
						</td>
					{else}
						<td class="center">
							<a href="{$this->router->link("CUshowShareholderPerson", "company_id=$companyId", "shareholder_id=$id")}">View</a>
						</td>
						<td class="center">
							<a href="{$this->router->link("CompanyShareholderCerificateAdminControler::SHAREHOLDERS_CERTIFICATE_PAGE view", "company_id=$companyId", "shareHolder=$id")}">Print</a>
						</td>
					{/if}
					{assign var=i value=$i+1}
				</tr>
			{/foreach}
		{else}
			<tr>
				<td colspan="2">No shareholders</td>
			</tr>
		{/if}
	</table>

	{* SECRETARIES *}
	<h2>
		Secretaries
		<a href="{$this->router->link("CUaddSecretaryPerson", "company_id=$companyId")}" style="font-size: 10px;">
			[appoint new secretary]
		</a>
	</h2>
	<table class="htmltable">
		<col>
		<col width="50">
		<col width="50">
		<col width="70">
		<tr>
			<th>Name</th>
			<th class="center" colspan="3">Action</th>
		</tr>
		{if !empty($data.secretaries)}
			{foreach from=$data.secretaries key="id" item="secretary"}
				<tr>
					<td>
						{if $secretary.corporate}
							{$secretary.corporate_name}
						{else}
							{$secretary.title} {$secretary.forename} {$secretary.surname}
						{/if}
					</td>
					{if $secretary.corporate}
						<td class="center">
							<a href="{$this->router->link("CUshowSecretaryCorporate", "company_id=$companyId", "secretary_id=$id")}">View</a>
						</td>
						<td class="center">
							<a href="{$this->router->link("CUeditSecretaryCorporate", "company_id=$companyId", "secretary_id=$id")}">Edit</a>
						</td>
						<td class="center">
							<a href="{$this->router->link("CUresign", "company_id=$companyId", "officer_id=$id" , "resign=1")}">Resign</a>
						</td>
					{else}
						<td class="center">
							<a href="{$this->router->link("CUshowSecretaryPerson", "company_id=$companyId", "secretary_id=$id")}">View</a>
						</td>
						<td class="center">
							<a href="{$this->router->link("CUeditSecretaryPerson", "company_id=$companyId", "secretary_id=$id")}">Edit</a>
						</td>
						<td class="center">
							<a href="{$this->router->link("CUresign", "company_id=$companyId", "officer_id=$id" , "resign=1")}">Resign</a>
						</td>
					{/if}
				</tr>
			{/foreach}
		{else}
			<tr>
				<td colspan="4">No secretaries</td>
			</tr>
		{/if}
	</table>

	{* SUBSCRIBERS *}
	<h2>
		Subscribers
		<a href="{$this->router->link("CUsendShareCertsToCustomer", "company_id=$companyId")}" style="font-size: 10px;">
			[send to customer]
		</a>
	</h2>
	<table class="htmltable">
		<col>
		<col width="90">
		<col width="90">
		<col width="100">
		<col width="70">
		<tr>
			<th>Name</th>
			<th>Shares</th>
			<th>Currency</th>
			<th>Share value</th>
			<th>Action</th>
		</tr>
		{if !empty($data.subscribers)}
			{foreach from=$data.subscribers key="id" item="subscriber"}
				<tr>
					{if isset($subscriber.corporate_name)}
						<td>{$subscriber.corporate_name}</td>
					{else}
						<td>{$subscriber.forename} {$subscriber.surname}</td>
					{/if}
					<td class="center">{$subscriber.shares}</td>
					<td class="center">{$subscriber.currency}</td>
					<td class="center">{$subscriber.share_value}</td>
					<td class="center">
						<a href="{$this->router->link(null, "company_id=$companyId", "document_name=shareCertificate", "subscriber=$id")}">View</a>
					</td>
				</tr>
			{/foreach}
		{else}
			<tr>
				<td colspan="5">No Subscribers</td>
			</tr>
		{/if}
	</table>
{/if}

{* DOCUMENTS *}
<h2>Incorporation Documents
	{if $data.company_details.company_category != 'LLP'}
		<a href="{$this->router->link("CUsendDocumentsToCustomer", "company_id=$companyId")}" style="font-size: 10px;">
			[send to customer]
		</a>
	{/if}
	{if $user->role->key == 'admin'}
		<a href="{$this->router->link("CUuploadIncorporationDocuments", "company_id=$companyId")}" style="font-size: 10px;">
			[upload inc certificate and memorandum articles]
		</a>
		<a href="{$this->router->link("CUsendIcoparationEmailToCustomer", "company_id=$companyId")}" style="font-size: 10px;">
			[send accept email]
		</a>
	{/if}
</h2>
<table class="htmltable">
	<col>
	<col width="100">
	<tr>
		<th>Name</th>
		<th class="center">Action</th>
	</tr>

	{if !empty($documents)}
		{foreach from=$documents item='document'}
			<tr>
				<td>
					{if $document == 'article.pdf'}
						Articles/Memorandum
					{else}
						{$document}
					{/if}
				</td>
				<td align="center">
					<a href="{$this->router->link(null, "company_id=$companyId", "document_name=$document")}">download</a>
				</td>
			</tr>
		{/foreach}
	{else}
		<tr>
			<td colspan="2">No Documents</td>
		</tr>
	{/if}
</table>

{* === OTHER COMPANY DOCUMENTS === *}
<h2>Other Company Documents <a href="{$this->router->link("UploadOtherCompanyFiles", "company_id=$companyId")}"
							   style="font-size: 10px;">[Upload Files]</a></h2>
<table class="htmltable">
	<col>
	<col width="100">
	<col width="100">
	<tr>
		<th>Document</th>
		<th colspan="2" class="center">Action</th>
	</tr>
	{if !empty($otherdocuments)}
		{foreach $otherdocuments as $document}
			{assign var="documentId" value=$document->getId()}
			<tr>
				<td>
					{$document->getRealFileName()}
				</td>
				<td class="center">
					<a href="{$this->router->link("DownloadOtherCompanyFile", "company_id=$companyId", "document_id=$documentId")}">view</a>
				</td>
				<td class="center">
					<a href="{$this->router->link("DeleteOtherCompanyFile", "company_id=$companyId", "document_id=$documentId")}"
					   onclick="return confirm('Are you sure');">delete</a>
				</td>
			</tr>
		{/foreach}
	{else}
		<tr>
			<td colspan="3">No Documents</td>
		</tr>
	{/if}
</table>

{* FORM SUBMISSIONS *}
<h2>Form submissions</h2>
<table class="htmltable toggle">
	{if $view->hasFormSubmissions()}
		<colgroup>
			<col width="70">
			<col width="170">
			<col width="130">
			<col width="140">
		</colgroup>
		<thead>
		<tr>
			<th>Id</th>
			<th>Form identifier</th>
			<th>Response</th>
			<th>Actions</th>
			{if $data.company_details.company_category != 'LLP'}
				<th width="60">Minutes</th>
			{/if}
		</tr>
		</thead>
		<tbody>
		{foreach $view->getFormSubmissions() as $submission}
			{assign var="submissionFormIdentifier" value=$submission->getFormIdentifier()}
			{assign var="submissionId" value=$submission->getId()}
			<tr>
				<td>{$submissionId}</td>
				<td>{$submissionFormIdentifier}</td>
				<td>
					{if $submission->hasResponse()}
						{$submission->getResponseMessage()}
						{if $submission->isError()}
							&nbsp;&nbsp;
							<a href="{$this->router->link("removeSubmissionError", "type=$submissionFormIdentifier",  "company_id=$companyId", "submission_id=$submissionId")}">(remove)</a>
						{/if}
					{else}
						<span class="description">(not submitted)</span>
					{/if}
					{if $submission->isCancelled()}
						<br>
						<span class="warning">Cancelled ({$submission->getDateCancelled()|datetime})</span>
					{/if}
				</td>
				<td>
					<a href="{$this->router->link("formSubmission", "company_id=$companyId", "submission_id=$submissionId")}">view</a>
					{if $view->canResubmit($user, $submission)}
						|
						{capture "resubmitConfirmation"}
							This will cancel the current form submission and will resubmit the form
							with a new ID. By cancelling this submission we won't query Companies House for updates on
							its status. If this submission is ever accepted we won't know about it. Only proceed if you
							know what you are doing. Are you sure you want to cancel the current submission and resubmit
							the form with using a new ID?
						{/capture}
						<a href="{$this->router->link("resubmitFormSubmission", "company_id=$companyId", "submission_id=$submissionId")}"
						   data-confirmation="{$smarty.capture.resubmitConfirmation|strip:" "}">
							resubmit with new ID
						</a>
					{/if}
				</td>
				{if $data.company_details.company_category != 'LLP'}
					<td class="center">
						{if $view->canViewMinutes($submission)}
							<a href="{$this->router->link(null)}&formid={$submissionId}">view</a>
						{/if}
					</td>
				{/if}
			</tr>
		{/foreach}
		</tbody>
	{else}
		<tr>
			<td colspan="4">No form submissions</td>
		</tr>
	{/if}
</table>

{* === BANKING  ===*}
{include "@blocks/CUviewBanking.tpl" view = $companyBankingView}

{include file="@footer.tpl"}
