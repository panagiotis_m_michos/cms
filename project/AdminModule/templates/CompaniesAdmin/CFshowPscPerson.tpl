{include file="@header.tpl"}

<table class="htmltable" width="700">
    <tr>
        <th colspan="2" class="center">Person</th>
    </tr>
    <tr>
        <th>Title</th>
        <td>{$psc->getPerson()->getTitle()}</td>
    </tr>
    <tr>
        <th>First name</th>
        <td>{$psc->getPerson()->getForename()}</td>
    </tr>
    <tr>
        <th>Middle name</th>
        <td>{$psc->getPerson()->getMiddlename()}</td>
    </tr>
    <tr>
        <th>Last name</th>
        <td>{$psc->getPerson()->getSurname()}</td>
    </tr>
    <tr>
        <th>DOB</th>
        <td>{$psc->getPerson()->getDob()}</td>
    </tr>
    <tr>
        <th>Nationality</th>
        <td>{$psc->getPerson()->getNationality()}</td>
    </tr>
    <tr>
        <th>Country of Residence</th>
        <td>{$psc->getPerson()->getCountryOfResidence()}</td>
    </tr>
    <tr>
        <th colspan="2" class="center">Address</th>
    </tr>
    <tr>
        <th>Address 1</th>
        <td>{$psc->getAddress()->getPremise()}</td>
    </tr>
    <tr>
        <th>Address 2</th>
        <td>{$psc->getAddress()->getStreet()}</td>
    </tr>
    <tr>
        <th>Address 3</th>
        <td>{$psc->getAddress()->getThoroughfare()}</td>
    </tr>
    <tr>
        <th>Town</th>
        <td>{$psc->getAddress()->getPostTown()}</td>
    </tr>
    <tr>
        <th>County</th>
        <td>{$psc->getAddress()->getCounty()}</td>
    </tr>
    <tr>
        <th>Postcode</th>
        <td>{$psc->getAddress()->getPostcode()}</td>
    </tr>
    <tr>
        <th>Country</th>
        <td>{$psc->getAddress()->getCountry()}</td>
    </tr>
    <tr>
        <th colspan="2" class="center">Residential Address</th>
    </tr>
    <tr>
        <th>Address 1</th>
        <td>{$psc->getResidentialAddress()->getPremise()}</td>
    </tr>
    <tr>
        <th>Address 2</th>
        <td>{$psc->getResidentialAddress()->getStreet()}</td>
    </tr>
    <tr>
        <th>Address 3</th>
        <td>{$psc->getResidentialAddress()->getThoroughfare()}</td>
    </tr>
    <tr>
        <th>Town</th>
        <td>{$psc->getResidentialAddress()->getPostTown()}</td>
    </tr>
    <tr>
        <th>County</th>
        <td>{$psc->getResidentialAddress()->getCounty()}</td>
    </tr>
    <tr>
        <th>Postcode</th>
        <td>{$psc->getResidentialAddress()->getPostcode()}</td>
    </tr>
    <tr>
        <th>Country</th>
        <td>{$psc->getResidentialAddress()->getCountry()}</td>
    </tr>
    <tr>
        <th colspan="2" class="center">Nature of Control</th>
    </tr>
    <tr>
        <td colspan="2">
            {foreach $psc->getNatureOfControlsTexts() as $text}
                {$text}<br>
            {/foreach}
        </td>
    </tr>
</table>

{include file="@footer.tpl"}
