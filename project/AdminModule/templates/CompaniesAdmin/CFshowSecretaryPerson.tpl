{include file="@header.tpl"}

<table class="htmltable" width="700">
<col width="200">
<tr>
	<th colspan="2" class="center">Person</th>
</tr>
<tr>
	<th>Title</th>
	<td>{$secretary.title}</td>
</tr>
<tr>
	<th>First name</th>
	<td>{$secretary.forename}</td>
</tr>
<tr>
	<th>Last name</th>
	<td>{$secretary.surname}</td>
</tr>
<tr>
	<th colspan="2" class="center">Address</th>
</tr>
<tr>
	<th>Address 1</th>
	<td>{$secretary.premise}</td>
</tr>
<tr>
	<th>Address 2</th>
	<td>{$secretary.street}</td>
</tr>
<tr>
	<th>Address 3</th>
	<td>{$secretary.thoroughfare}</td>
</tr>
<tr>
	<th>Town</th>
	<td>{$secretary.post_town}</td>
</tr>
<tr>
	<th>County</th>
	<td>{$secretary.county}</td>
</tr>
<tr>
	<th>Postcode</th>
	<td>{$secretary.postcode}</td>
</tr>
<tr>
	<th>Country</th>
	<td>{$secretary.country}</td>
</tr>
</table>

{include file="@footer.tpl"}