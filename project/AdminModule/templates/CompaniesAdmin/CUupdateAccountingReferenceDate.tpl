{include file="@header.tpl"}
<div id="maincontent1">
	<div style="width: 753px;">
		{* FORM BEGIN *}
		{$form->getBegin() nofilter}
		{$form->getHtmlErrorsInfo() nofilter}

	<fieldset>
		<legend>New Accounting Reference Date </legend>
	<table class="ff_table">
	<col width="180px"/>
	<col width="300px" />
        <tr>
            <th colspan="3"style="padding-bottom: 15px; text-align: left;">{$form->getLabel("ARDRange") nofilter}</th>
        </tr>
        <tr>
            <td colspan="3"style="padding-left: 10px;">{$form->getControl('ARDRange') nofilter}</td>
        </tr>
        <tr>
            <td colspan="3"><span class="ff_control_err">{$form->getError('ARDRange') nofilter}</span></td>
        </tr>
        <tr>
            <td colspan="3"style="padding-bottom: 15px;">Please enter the date you want the accounting period to end (dd/mm/yyyy)</td>
        </tr>
		<tr>
			<th>{$form->getLabel("date") nofilter}</th>
			<td>{$form->getControl('date') nofilter}<span class="ff_desc">Once the accounting reference date is changed, subsequent accounting periods will end on the same day and month in future years.</span></td>
			<td><span class="ff_control_err">{$form->getError('date')}</span></td>
		</tr>
		<tr>
			<th>{$form->getLabel("fiveYearExtensionDetails") nofilter}</th>
			<td>{$form->getControl('fiveYearExtensionDetails') nofilter}</td>
			<td><span class="ff_control_err">{$form->getError('fiveYearExtensionDetails')}</span></td>
		</tr>
		<tr id="message2">
			<td colspan="3"><span class="ff_desc">You may only extend a period more than once in five years if one of the following provisions apply. If this is true, please select the provision below:</span></td>
		</tr>
		<tr>
			<th>{$form->getLabel("extensionReason") nofilter}</th>
			<td>{$form->getControl('extensionReason') nofilter}</td>
			<td><span class="ff_control_err">{$form->getError('extensionReason')}</span></td>
		</tr>
		<tr>
			<th>{$form->getLabel("extensionAuthorisedCode") nofilter}</th>
			<td>{$form->getControl('extensionAuthorisedCode') nofilter}<span class="ff_desc">If you have indicated that you have approval by the Secretary of State please enter the code provided on your Secretary of State authorisation letter (4 characters).</span></td>
			<td><span class="ff_control_err">{$form->getError('extensionAuthorisedCode')}</span></td>
		</tr>
	</table>
	</fieldset>
	<fieldset>
		<legend>Action</legend>
	<table class="ff_table">
		<tr>
			<th>{$form->getLabel("send") nofilter}</th>
			<td>{$form->getControl('send') nofilter}</td>
			<td></td>
		</tr>
	</table>
	</fieldset>
	{$form->getEnd() nofilter}
	</div>
</div>
{include file="@footer.tpl"}

{literal}
<script type="text/javascript">
    <!--
    
    var current = "{/literal}{$ardCurrent}{literal}";
    var previos = "{/literal}{$ardPrevios}{literal}";
    var lastAccountMadeUp = "{/literal}{$lastAccountMadeUp}{literal}";

    function parseDate(str) {
        return $.datepicker.parseDate('dd-mm-yy', str);
    }
    
    function gettodayDate() {
        var a = new Date();
        var b = a.toISOString();
        var datapickerprefill = b.substring(8,10)+"-"+b.substring(5,7)+"-"+b.substring(0,4); 
        return datapickerprefill;
    }
    function refreshDate(accPeriod,type) {
        newDate = parseDate($(".date").val());
        //alert(accPeriod);
        //alert(newDate);
        if(accPeriod > newDate){//alert('remove addition fields')
            $('.arPeriodHide1, .arPeriodHide2').attr('disabled', 'disabled').parent().parent().hide();
            $('#message2').attr('disabled', 'disabled').hide();
            $('.arPeriod').attr('disabled', 'disabled').parent().parent().hide();
        }else{//alert('show addition fields')
            $('.arPeriod').removeAttr('disabled').parent().parent().show();
            if($('.arPeriod:checked').val()  == 1){
                $('.arPeriodHide1').removeAttr('disabled').parent().parent().show();
                $('#message2').show();
                if($('.arPeriodHide1:checked').val() == 'STATE'){
                    $('.arPeriodHide2').removeAttr('disabled').parent().parent().show();}
            }
        }
    }
    
        ///////////loading defaults//////////////////
        
        //check if we need previos period if not, remove
        if(lastAccountMadeUp == ''){
            $("#ARDRange2").attr('disabled', 'disabled').hide();
            $('label[for="ARDRange2"]').attr('disabled', 'disabled').hide();
            $('#ARDRange1').attr('checked', true);
            previos = current;
        }
        
        $(document).ready(function() {


        //check range
        if($("#ARDRange1:checked").val()== 1){
            var accPeriod  = parseDate(current);
        }
        if($("#ARDRange2:checked").val()== 2){
            var accPeriod  = parseDate(previos);
        }
        
        //check range with date
        if(accPeriod > parseDate($(".date").val())){
            $('.arPeriod').attr('disabled', 'disabled').parent().parent().hide();
            $('.arPeriodHide1, .arPeriodHide2').attr('disabled', 'disabled').parent().parent().hide();
            $('#message2').attr('disabled', 'disabled').hide();
        }else if(accPeriod < parseDate($(".date").val())){
            $('.arPeriod').removeAttr('disabled').parent().parent().show();

        }
        //check period 
        if(!$(".arPeriod").attr('checked')){
            $('.arPeriodHide1, .arPeriodHide2').attr('disabled', 'disabled').parent().parent().hide();
            $('#message2').attr('disabled', 'disabled').hide();
        }
        
        //check reson
        if($('.arPeriodHide1:checked').val() == 'STATE'){
            $('.arPeriodHide2').removeAttr('disabled').parent().parent().show();
        }else{
            $('.arPeriodHide2').attr('disabled', 'disabled').parent().parent().hide();
        }
        
        //changing range
        $(".ARDRange").change (function() {
            if($(this).val()== 1){
                var accPeriod  = parseDate(current);
                refreshDate(accPeriod); 
            }
            if($(this).val()== 2){
                var accPeriod  = parseDate(previos);
                refreshDate(accPeriod); 
            }
        });
    
        // changing date
        $(".date").change (function() {
            if($("#ARDRange1:checked").val()== 1){
                var accPeriod  = parseDate(current);
            }
            if($("#ARDRange2:checked").val()== 2){
                var accPeriod  = parseDate(previos);
            }
            refreshDate(accPeriod); 
        });
    
        //changing period
        $(".arPeriod").change (function() {
            if($(this).val()  == 1){
                $('.arPeriodHide1').removeAttr('disabled').parent().parent().show();
                $('#message2').show();
                if($('.arPeriodHide1:checked').val() == 'STATE'){
                    $('.arPeriodHide2').removeAttr('disabled').parent().parent().show();}
            }else if($(this).val()  == 0){
                $('.arPeriodHide1, .arPeriodHide2').attr('disabled', 'disabled').parent().parent().hide();
                $('#message2').attr('disabled', 'disabled').hide();
            }
        });
        
        //changing reason
        $('.arPeriodHide1').click (function(){
            if($('.arPeriodHide1:checked').val() == 'STATE'){
                $('.arPeriodHide2').removeAttr('disabled').parent().parent().show();
            }else{
                $('.arPeriodHide2').attr('disabled', 'disabled').parent().parent().hide();

            }
        });
    });
    //-->
</script>
{/literal}

