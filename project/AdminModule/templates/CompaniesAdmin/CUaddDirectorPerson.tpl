{include file="@header.tpl"}

{* JS FOR PREFILL *}
<script type="text/javascript">
/* <![CDATA[ */
var addresses = {$jsPrefillAdresses nofilter};
var officers = {$jsPrefillOfficers nofilter};
/* ]]> */
</script>

{literal}
<script>
$(document).ready(function () {

	// prefill address	
	$("#prefillAddress").change(function () {
		var value = $(this).val();
		address = addresses[value];
		for (var name in address) {
			$('#'+name).val(address[name]);
		}
	});
	
	// prefill officers	
	$("#prefillOfficers").change(function () {
		var value = $(this).val();
		address = officers[value];
		for (var name in address) {
			$('#'+name).val(address[name]);
		}
	});

	toogleResidentialAddress();
	
	// residential address
	$("#residentialAddress").click(function (){
		toogleResidentialAddress();
	});
	
	function toogleResidentialAddress() {
		disabled = !$("#residentialAddress").is(":checked");
		$("input[id^='residential_'], select[id^='residential_']").each(function () {
			$(this).attr("disabled", disabled);
		}); 
	}

	$('[name="designated_ind"]').change(function (){
		setConsentLabel();
	}).trigger('change');

	function setConsentLabel() {
		var value = $('[name="designated_ind"]:checked').val();
		var label = $('#consentToAct').data('label-designated_ind-' + value);
		$('label[for="consentToAct"]').text(label);
	}
});
</script>
{/literal}

<p style="text-align: right;">To make a corporate appointment <a href="{$this->router->link("CUaddDirectorCorporate", "company_id=$companyId")}">click here</a></p>
{$form nofilter}

{include file="@footer.tpl"}
