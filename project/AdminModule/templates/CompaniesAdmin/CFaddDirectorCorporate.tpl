{include file="@header.tpl"}

{* JS FOR PREFILL *}
<script type="text/javascript">
/* <![CDATA[ */
var addresses = {$jsPrefillAdresses nofilter};
var officers = {$jsPrefillOfficers nofilter};
/* ]]> */
</script>

{literal}
<script>
$(document).ready(function () {

	// prefill address	
	$("#prefillAddress").change(function () {
		var value = $(this).val();
		address = addresses[value];
		for (var name in address) {
			$('#'+name).val(address[name]);
		}
	});
	
	// prefill officers	
	$("#prefillOfficers").change(function () {
		var value = $(this).val();
		address = officers[value];
		for (var name in address) {
			$('#'+name).val(address[name]);
		}
	});

	toogleServiceAddress();
	
	// service address
	$("#serviceAddress").click(function (){
		toogleServiceAddress();
	});
	
	function toogleServiceAddress() {
		disabled = !$("#serviceAddress").is(":checked");
		$("input[id^='service_'], select[id^='service_']").each(function () {
			$(this).attr("disabled", disabled);
		}); 
	}
	
	// EEA
	toogleEEA();
	
	$("input[name='eeaType']").click(function (){
		toogleEEA();
	});
	
	function toogleEEA() {
		// EEA
		if ($("#eeaType1").is(":checked")) {
			$("#place_registered").attr("disabled", false);
			$("#registration_number").attr("disabled", false);
			$("#law_governed").attr("disabled", true);
			$("#legal_form").attr("disabled", true);
		// Non EEA
		} else if ($("#eeaType2").is(":checked")) {
			$("#place_registered").attr("disabled", false);
			$("#registration_number").attr("disabled", false);
			$("#law_governed").attr("disabled", false);
			$("#legal_form").attr("disabled", false);
		} else {
			$("#place_registered").attr("disabled", true);
			$("#registration_number").attr("disabled", true);
			$("#law_governed").attr("disabled", true);
			$("#legal_form").attr("disabled", true);
		}
		
	}
	
});
</script>
{/literal}

<p style="text-align: right;">To make a human appointment <a href="{$this->router->link("CFaddDirectorPerson", "company_id=$companyId")}">click here</a></p>
{$form nofilter}

{include file="@footer.tpl"}
