{include file="@header.tpl"}

{if $hasProduct === false} 
    <div style="width:730px;margin:auto;margin-bottom: 20px;padding:2px;padding-top:0;padding-left:10px;" class="flash error">
        <p>The customer didn't bought change name product for {$companyName}</p>
        
    </div>
{/if}   

<div class="change-company-name-form">
		{* FORM BEGIN *}
        
		{$form->getBegin() nofilter}
            {if $form->getErrors()|@count gt 0}
                <p class="ff_err_notice">Form has <b> {$form->getErrors()|@count}</b> error(s)</p>
            {/if}
         
            <fieldset styel="clear: both;">
            <legend>Change Name</legend>
            <table class="ff_table">
            <tbody>
            <tr>
                <th><label for="product-name">{$form->getLabel('changeNameId') nofilter}</label></th>
                <td>
                    <div class="radioSet1">
                    {$form->getControl('changeNameId') nofilter}
                    </div>
                </td>
                <td>
                    <span class="ff_control_err">{$form->getError('changeNameId')}</span>
                </td>
            </tr>    
            <tr>
                <th>{$form->getLabel('newCompanyName') nofilter}</th>
                <td>
                    {$form->getControl('newCompanyName') nofilter}
                    <span class="ff_desc">Please provide New Company Name</span>
                </td>
                <td>
                    <span class="ff_control_err">{$form->getError('newCompanyName')}</span>
                </td>
            </tr>
            <tr>
                <th> <label for="sufix">{$form->getLabel('radio') nofilter}</label></th>
                <td>
                    <div class="radioSet">
                        {$form->getControl('radio') nofilter}
                        <div class="help-button" style=" width: 300px;">
                            <em>This determines how your company name appears on the name change certificate. It is purely aesthetic and has no impact on your company.</em>
                        </div>
                    </div>
                </td>
                <td>
                    <span class="ff_control_err">{$form->getError('radio')}</span>
                </td>
            </tr>
            <tr>
                <th>New Name with Suffix:</th>
                <td>
                    <span class="company-name-clone"></span>
                </td>
            </tr>
            <tr class="ckeck-company" style="display: none;">
                <th>&nbsp</th>
                <td>
                    <span class="loading-image">
                       Loading ... Checking Company Name
                   </span>
                </td>
                <td>
                     &nbsp;
                </td>
            </tr>
            <tr class="show-ckeck-company" style="display: none;">
                <th>&nbsp</th>
                <td>
                    <span class="isOkToShow" style="display: none">
                                                
                        <span style="padding-top:10px;float: left;">Company name is available.</span>
                        <img src="/front/imgs/greentick.png" alt="company is available"/>
                        <div class="clear"></div>
                    </span>
                    <span class="isNotOkToShow error" style="display: none; ">
                        <span style="padding-top:10px;float: left;">Sorry, that name is unavailable. Please try a different name. </span>
                        <img src="/front/imgs/redcross.png" alt="company isn't available"/>
                        <div class="clear"></div>
                    </span>
                </td>
                <td>
                     &nbsp;
                </td>
            </tr>
            
            </tbody>
            </table>
            </fieldset>
            <fieldset id="fieldset_2">
            <legend>Action</legend>
            <table class="ff_table">
                <tbody>
                    <tr>
                        <th>&nbsp;</th>
                        <td>{$form->getControl('send') nofilter}</td>
                        <td>
                            <span class="isNotOkToSubmit"></span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </fieldset>    
        {$form->getEnd() nofilter}
        
	</div>

{literal}
 <style type="text/css">
     .change-company-name-form {
         margin-left: 0pt; 
         width: 753px;
     }    
     .change-company-name-form .radioSet , .change-company-name-form radioSet1{
         padding-bottom: 5px;
         overflow: hidden;
     }
     
     .change-company-name-form .radioSet input, .change-company-name-form .radioSet1 input {
         height: 15px;
         line-height:15px;
         vertical-align: top;
         margin-left:10px;
     }
     .change-company-name-form td, .change-company-name-form table.ff_table th {
         padding-top:5px;
         padding-bottom:5px;
     }
     
     .change-company-name-form table.ff_table th {
         width :150px;
         padding-right:10px;
     }
     
     .change-company-name-form .err-submit {
         color: red;
         padding-left:10px;
         padding-top:6px;
         
     } 
     .change-company-name-form .error-label {
         color : red;
     }
</style>     
<script type="text/javascript">
    $(document).ready(function(){
        window.cuchangenameform = new CUChangeNameForm();
    });
    
    var CUChangeNameForm = function() {
        
        var self  = this;
       
        this.getInputCompanyNameText = function() {
           return $('.change-company-name-form #newCompanyName').val().toUpperCase();
        }
       
        this.getCloneRadioText = function(){
            var local = '';
            (typeof($(".change-company-name-form .radioSet input[name=radio]:checked").val()) == 'undefined') ?local = '' : local = $(".change-company-name-form .radioSet input[name=radio]:checked").val();  
            return local;    
        }
            
        this.hideOkToShow = function() {
            $('.isNotOkToShow').fadeOut('slow');
            $('.isOkToShow').fadeOut('slow'); 
        }    
            
        
        $('.change-company-name-form span.company-name-clone').html(self.getInputCompanyNameText() + ' ' + self.getCloneRadioText());
        
        $('.change-company-name-form #newCompanyName').keyup(function(){
            var htm = $(this);
            self.hideOkToShow();   
           $('.change-company-name-form span.company-name-clone').html(htm.val().toUpperCase() + ' '+ self.getCloneRadioText());
        });
        
        $(".change-company-name-form .radioSet input[name=radio]:checked").live('click', function(){
            self.hideOkToShow();
            $('.change-company-name-form span.company-name-clone').html(self.getInputCompanyNameText() + ' '+ $(this).val());    
        });
    }
</script>
{/literal}          

{include file="@footer.tpl"}
