{include file="@header.tpl"}

{* JS FOR PREFILL *}
<script type="text/javascript">
/* <![CDATA[ */
var addresses = {$jsPrefillAdresses nofilter};
var officers = {$jsPrefillOfficers nofilter};
/* ]]> */
</script>

{literal}
<script>
$(document).ready(function () {

	// prefill address	
	$("#prefillAddress").change(function () {
		var value = $(this).val();
		address = addresses[value];
		for (var name in address) {
			$('#'+name).val(address[name]);
		}
	});
	
	// prefill officers	
	$("#prefillOfficers").change(function () {
		var value = $(this).val();
		address = officers[value];
		for (var name in address) {
			$('#'+name).val(address[name]);
		}
	});

	toogleServiceAddress();
	
	// service address
	$("#serviceAddress").click(function (){
		toogleServiceAddress();
	});
	
	function toogleServiceAddress() {
		disabled = !$("#serviceAddress").is(":checked");
		$("input[id^='service_'], select[id^='service_']").each(function () {
			$(this).attr("disabled", disabled);
		}); 
	}
	
});
</script>
{/literal}

<p style="text-align: right;">To make a corporate appointment <a href="{$this->router->link("CFaddSecretaryCorporate", "company_id=$companyId")}">click here</a></p>
{$form nofilter}

{include file="@footer.tpl"}
