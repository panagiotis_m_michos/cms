{include file="@header.tpl"}

<h2></h2>

<h2>Company </h2>

<table class="htmltable">
<col width="150">
<tr>
	<th>Company name</th>
	<td>{$company->getCompanyName()}</td>
</tr>
<tr>
	<th>Customer</th>
	<td>{$customer->firstName} {$customer->lastName} ({$customer->email})</td>
</tr>
</table>


<h2>Transfer to</h2>

{$form nofilter}

<h2>Customers</h2>

{if !empty($customers)}
	<table class="htmltable">
	<col>
	<col>
	<col width="80">
	<tr>
		<th>Contact name</th>
		<th>Email</th>
		<th>Action</th>
	</tr>
	{foreach from=$customers key="id" item="customer"}
	<tr>
		<td>{$customer->firstName} {$customer->lastName}</td>
		<td>{$customer->email}</td>
		<td class="center"><a href="{$this->router->link(null,"company_id=$companyId", "transfer_id=$id")}">transfer</a></td>
	</tr>
	{/foreach}
	</table>
	
	{$paginator nofilter}
	
{else}
	<p>No customers found.</p>
{/if}

{include file="@footer.tpl"}
