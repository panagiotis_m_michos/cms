{include file="@header.tpl"}

{if $user->roleId == 1 || $user->roleId == 2}
<ul class="submenu">
    <li><a href="{$this->router->link("CUeditBarclaysData", "company_id=$companyId")}">Edit Data</a></li>
    <li><a href="{$this->router->link("CUresendBarclaysData", "company_id=$companyId")}">Resend</a></li>
</ul>
{/if}


<h2>Barclays Data</h2>

<table class="htmltable">
<col width="250">
<tr>
	<th>Best Time to call</th>
	<td>{if isset($companyCustomer)}{$companyCustomer->preferredContactDate}{/if}</td>
</tr>
<tr>
	<th>Email</th>
	<td>{if isset($companyCustomer)}{$companyCustomer->email}{/if}</td>
</tr>
<tr>
	<th>Title</th>
	<td>{if isset($companyCustomer)}{$companyCustomer->titleId}{/if}</td>
</tr>
<tr>
	<th>First Name</th>
	<td>{if isset($companyCustomer)}{$companyCustomer->firstName}{/if}</td>
</tr>
<tr>
	<th>Last Name</th>
	<td>{if isset($companyCustomer)}{$companyCustomer->lastName}{/if}</td>
</tr>
<tr>
	<th>Phone 1</th>
	<td>{if isset($companyCustomer)}{$companyCustomer->phone}{/if}</td>
</tr>
<tr>
	<th>Phone 2</th>
	<td>{if isset($companyCustomer)}{$companyCustomer->additionalPhone}{/if}</td>
</tr>
<tr>
	<th>Address 1</th>
	<td>{if isset($companyCustomer)}{$companyCustomer->address1}{/if}</td>
</tr>
<tr>
	<th>Address 2</th>
	<td>{if isset($companyCustomer)}{$companyCustomer->address2}{/if}</td>
</tr>
<tr>
	<th>Address 3</th>
	<td>{if isset($companyCustomer)}{$companyCustomer->address3}{/if}</td>
</tr>
<tr>
	<th>City</th>
	<td>{if isset($companyCustomer)}{$companyCustomer->city}{/if}</td>
</tr>
<tr>
	<th>County</th>
	<td>{if isset($companyCustomer)}{$companyCustomer->county}{/if}</td>
</tr>
<tr>
	<th>Postcode</th>
	<td>{if isset($companyCustomer)}{$companyCustomer->postcode}{/if}</td>
</tr>
<tr>
	<th>Country</th>
	<td>{if isset($companyCustomer)}{$companyCustomer->countryId}{/if}</td>
</tr>
</table>


{include file="@footer.tpl"}