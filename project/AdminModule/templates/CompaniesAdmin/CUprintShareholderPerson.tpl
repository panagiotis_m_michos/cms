{include file="@header.tpl"}



<table class="htmltable" width="700">
<col width="200">
<col width="500">
<tr>
	<th colspan="2" class="center">Shareholder</th>
</tr>
<tr>
	<th>Title</th>
	<td>{$shareholder.title}</td>
</tr>
<tr>
	<th>First name</th>
	<td>{$shareholder.forename}</td>
</tr>
<tr>
	<th>Last name</th>
	<td>{$shareholder.surname}</td>
</tr>

<tr>
	<th colspan="2" class="center">Shares</th>
</tr>
{*
<tr>
	<th>Currency</th>
	<td>{$shareholder.currency}</td>
</tr>
*}
<tr>
	<th>Share class</th>
	<td>{$shareholder.share_class}</td>
</tr>
<tr>
	<th>Number of shares</th>
	<td>{$shareholder.num_shares}</td>
</tr>
{*
<tr>
	<th>Share value</th>
	<td>{$shareholder.share_value}</td>
</tr>
*}

</table>

    <p></p>
    <div class="ff_err_notice" style="margin-left: 0; width: 700px;">
        <p>Please enter the shareholder's address below and we'll automatically produce their share certificate.</p>
    </div>
{$form nofilter}
{include file="@footer.tpl"}