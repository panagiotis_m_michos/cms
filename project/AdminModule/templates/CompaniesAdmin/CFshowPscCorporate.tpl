{include file="@header.tpl"}

<table class="htmltable" width="700">
    <tr>
        <th colspan="2" class="center">Corporate</th>
    </tr>
    <tr>
        <th>Company name</th>
        <td>{$psc->getFullName()}</td>
    </tr>
    <tr>
        <th>Place registered</th>
        <td>{$psc->getIdentification()->getPlaceRegistered()}</td>
    </tr>
    <tr>
        <th>Registration number</th>
        <td>{$psc->getIdentification()->getRegistrationNumber()}</td>
    </tr>
    <tr>
        <th>Governing law</th>
        <td>{$psc->getIdentification()->getLawGoverned()}</td>
    </tr>
    <tr>
        <th>Legal form</th>
        <td>{$psc->getIdentification()->getLegalForm()}</td>
    </tr>
    <tr>
        <th>Country registered</th>
        <td>{$psc->getIdentification()->getCountryOrState()}</td>
    </tr>
    <tr>
        <th colspan="2" class="center">Address</th>
    </tr>
    <tr>
        <th>Address 1</th>
        <td>{$psc->getAddress()->getPremise()}</td>
    </tr>
    <tr>
        <th>Address 2</th>
        <td>{$psc->getAddress()->getStreet()}</td>
    </tr>
    <tr>
        <th>Address 3</th>
        <td>{$psc->getAddress()->getThoroughfare()}</td>
    </tr>
    <tr>
        <th>Town</th>
        <td>{$psc->getAddress()->getPostTown()}</td>
    </tr>
    <tr>
        <th>County</th>
        <td>{$psc->getAddress()->getCounty()}</td>
    </tr>
    <tr>
        <th>Postcode</th>
        <td>{$psc->getAddress()->getPostcode()}</td>
    </tr>
    <tr>
        <th>Country</th>
        <td>{$psc->getAddress()->getCountry()}</td>
    </tr>
    <tr>
        <th colspan="2" class="center">Nature of Control</th>
    </tr>
    <tr>
        <td colspan="2">
            {foreach $psc->getNatureOfControlsTexts() as $text}
                {$text}<br>
            {/foreach}
        </td>
    </tr>
</table>

{include file="@footer.tpl"}
