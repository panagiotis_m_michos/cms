{include file="@header.tpl"}

{if $submission->isInternalFailure()}
    <div class="flash error">
		This form submission received the error <b>Internal Failure</b> from Companies House.
        This means the submission has been sent to Companies House and is still pending, but something happened at their
        end that they need to resolve. The usual fix is to wait a few hours (the system will keep checking with
        Companies House to see if it's accepted or rejected). You can reduce the wait by calling Companies House
        (029&nbsp;20380929) and asking them about this specific form submission. The Operations Team has been notified
        about this error.

        You can tell the customer that their submission is fine, but Companies House have a technical issue they need
        to resolve. Will we automatically email the customer as soon as it's fixed (this will be the accepted or
        rejected email).
	</div>
{/if}

{* SUBMISSION *}
<h2>Form Submission</h2>
<table class="htmltable">
	<col width="170">
	<tr>
		<th>Form Submission Id</th>
		<td>{$submission->getId()}</td>
	</tr>
	<tr>
		<th>Company Id</th>
		<td>{$submission->getCompanyId()}</td>
	</tr>
	<tr>
		<th>Form Identifier</th>
		<td>{$submission->getFormIdentifier()}</td>
	</tr>
	<tr>
		<th>Language</th>
		<td>{$submission->getLanguage()}</td>
	</tr>
	<tr>
		<th>Response</th>
		<td>
			{if !empty($submission->getResponse())}
				{$submission->getResponseMessage()}
			{else}
				<span class="description">(not submitted)</span>
			{/if}
			{if $submission->isCancelled()}
				<br>
				<span class="warning">Cancelled ({$submission->getDateCancelled()|datetime})</span>
			{/if}
		</td>
	</tr>
	<tr>
		<th>Reject Reference</th>
		<td>{$submission->getRejectReference()}</td>
	</tr>
	<tr>
		<th>Examiner Telephone</th>
		<td>{$submission->getExaminerTelephone()}</td>
	</tr>
	<tr>
		<th>Examiner Comment</th>
		<td>{$submission->getExaminerComment()}</td>
	</tr>
</table>

{* SUBMISSION ERRORS *}
<h2>Submission Errors</h2>
{if !empty($submissionErrors)}
	<table class="htmltable">
	<col width="70">
	<col width="70">
	<col>
	<col width="70">
	<tr>
		<th>Id</th>
		<th>Reject code</th>
		<th>Description</th>
		<th>Instance number</th>
	</tr>
	{foreach from=$submissionErrors item="submissionError"}
	<tr>
		<td>{$submissionError->form_submission_error_id}</td>
		<td>{$submissionError->reject_code}</td>
		<td class="noOverflow">{$submissionError->description}</td>
		<td>{$submissionError->instance_number}</td>
	</tr>
	{/foreach}
	</table>
{else}
	<p>No errors</p>
{/if}

{* ENVELOPES *}
<h2>Envelopes</h2>
{if !empty($envelopes)}
	{foreach from=$envelopes item="envelope" name="f1"}
	<table class="htmltable">
	<tr>
		<th colspan="2" class="center">Envelope {$envelope->envelope_id}</th>
	</tr>
	<tr>
		<th>Id</th>
		<td>{$envelope->envelope_id}</td>
	</tr>
	<tr>
		<th>Class</th>
		<td>{$envelope->class}</td>
	</tr>
	<tr>
		<th>Gateway test</th>
		<td>{$envelope->gateway_test}</td>
	</tr>
	<tr>
		<th>Date sent</th>
		<td>{$envelope->date_sent}</td>
	</tr>
	<tr>
		<th>Success</th>
		<td>{$envelope->success}</td>
	</tr>
	<tr>
		<th>Error raised by</th>
		<td>{$envelope->error_raised_by}</td>
	</tr>
	<tr>
		<th>Error number</th>
		<td>{$envelope->error_number}</td>
	</tr>
	<tr>
		<th>Error type</th>
		<td>{$envelope->error_type}</td>
	</tr>
	<tr>
		<th>Error text</th>
		<td class="noOverflow">{$envelope->error_text}</td>
	</tr>
	<tr>
		<th>Error location</th>
		<td>{$envelope->error_location}</td>
	</tr>
	{if isset($envelope->xmlRequest)}
	<tr>
		<th>Xml Request</th>
		<td><a href="{$envelope->xmlRequest}">download</a></td>
	</tr>
	{/if}
	{if isset($envelope->xmlResponse)}
	<tr>
		<th>Xml Response</th>
		<td><a href="{$envelope->xmlResponse}">download</a></td>
	</tr>
	{/if}
	</table>
	{if !$smarty.foreach.f1.last}<br />{/if}
	{/foreach}
{else}
	<p>No envelopes</p>
{/if}


{include file="@footer.tpl"}
