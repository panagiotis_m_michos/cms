{include file="@header.tpl"}

<table class="htmltable" width="700">
<col width="200">
<col width="500">
<tr>
	<th colspan="2" class="center">Shareholder</th>
</tr>
<tr>
	<th>Title</th>
	<td>{$shareholder.title}</td>
</tr>
<tr>
	<th>First name</th>
	<td>{$shareholder.forename}</td>
</tr>
<tr>
	<th>Last name</th>
	<td>{$shareholder.surname}</td>
</tr>
<tr>
	<th colspan="2" class="center">Address</th>
</tr>
<tr>
	<th>Address 1</th>
	<td>{$shareholder.premise}</td>
</tr>
<tr>
	<th>Address 2</th>
	<td>{$shareholder.street}</td>
</tr>
<tr>
	<th>Address 3</th>
	<td>{$shareholder.thoroughfare}</td>
</tr>
<tr>
	<th>Town</th>
	<td>{$shareholder.post_town}</td>
</tr>
<tr>
	<th>County</th>
	<td>{$shareholder.county}</td>
</tr>
<tr>
	<th>Postcode</th>
	<td>{$shareholder.postcode}</td>
</tr>
<tr>
	<th>Country</th>
	<td>{$shareholder.country}</td>
</tr>
<tr>
	<th colspan="2" class="center">Shares</th>
</tr>
{*
<tr>
	<th>Currency</th>
	<td>{$shareholder.currency}</td>
</tr>
*}
<tr>
	<th>Share class</th>
	<td>{$shareholder.share_class}</td>
</tr>
<tr>
	<th>Number of shares</th>
	<td>{$shareholder.num_shares}</td>
</tr>
{*
<tr>
	<th>Share value</th>
	<td>{$shareholder.share_value}</td>
</tr>
*}
</table>

{include file="@footer.tpl"}