{include file="@header.tpl"}

{literal}
<script type="text/javascript">
/* <![CDATA[ */
$(document).ready(function () {
	
	toggleCustom();
	
	$("input[name='type']").click(function () {
		toggleCustom();
	});
	
	function toggleCustom() {
		disabled = $("#type1").is(":checked");
		$("#custom").attr("disabled", disabled);
		$("#removeMaa").attr("disabled", disabled);
	}
	
});
/* ]]> */
</script>
{/literal}

{$form->getBegin() nofilter}
{if $type != 'LLP'}
<fieldset>
	<legend>Articles</legend>
	<table class="ff_table">
	<tbody>
	<tr>
		<th>{$form->getLabel('type') nofilter}</th>
		<td>{$form->getControl('type') nofilter}</td>
	</tr>
	</tbody>
	</table>
</fieldset>

{* HAS ARTICLE *}
{if isset($article)}
	<fieldset>
		<legend>Custom</legend>
		<table class="ff_table">
		<tbody>
		<tr>
			<th>{$form->getLabel('articleName') nofilter}</th>
			<td>{assign var="articleId" value=$article.id}<a href="{$this->router->link(null, "document_id=$articleId")}">{$article.filename}</a></td>
			<td style="padding-left: 20px;">{$form->getControl('removeArticle') nofilter}</td>
		</tr>
		</tbody>
		</table>
	</fieldset>
{else}
	<fieldset>
		<legend>Custom</legend>
		<table class="ff_table">
		<tbody>
		<tr>
			<th>{$form->getLabel('custom') nofilter}</th>
			<td>{$form->getControl('custom') nofilter}<br /><i>(Must be in PDF)</i></td>
			<th>{$form->getError('custom')}</th>
		</tr>
		</tbody>
		</table>
	</fieldset>
{/if}
{/if}
{* HAS SUPPORT DOCUMENT *}
{if isset($support)}
	<fieldset>
		<legend>Supporting documents</legend>
		<table class="ff_table">
		<tbody>
		<tr>
			<th>{$form->getLabel('supportName') nofilter}</th>
			<td>{assign var="supportId" value=$support.id}<a href="{$this->router->link(null, "document_id=$supportId")}">{$support.filename}</a></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$form->getControl('removeSupport') nofilter}</td>
		</tr>
		</tbody>
		</table>
	</fieldset>
{else}
	<fieldset>
		<legend>Supporting documents</legend>
		<table class="ff_table">
		<tbody>
		<tr>
			<th>{$form->getLabel('support') nofilter}</th>
			<td>{$form->getControl('support') nofilter}<br /><i>(Must be in PDF)</i></td>
			<th>{$form->getError('support')}</th>
		</tr>
		</tbody>
		</table>
	</fieldset>
{/if}

<fieldset>
	<legend>Action</legend>
	<table>
	<tr>
		<td align="right">{$form->getControl('save') nofilter}</td>
	</tr>
	</table>
</fieldset>

{$form->getEnd() nofilter}

{include file="@footer.tpl"}