{include file="@header.tpl"}
{* JS ADDRESSES FOR PREFILL *}
<script type="text/javascript">
	var addresses = {$jsAdresses nofilter};
</script>

{literal}
<script type="text/javascript">
$(document).ready(function () {

	// start on beginning
	ourRegisterOfficeHandler();
	
	// called on type click
	$("input[name='ourRegisteredOffice']").click(function () {		
		ourRegisterOfficeHandler(); 
	});
	
	
	/**
	 * Provides disable and enable dropdown for our offices
	 * @return void
	 */
	function ourRegisterOfficeHandler()
	{
		var disable = $("#ourRegisteredOffice").is(":checked");
		$('#prefill').attr("disabled", disable);
		$('#premise').attr("disabled", disable);
		$('#street').attr("disabled", disable);
		$('#thoroughfare').attr("disabled", disable);
		$('#post_town').attr("disabled", disable);
		$('#postcode').attr("disabled", disable);
		$('#county').attr("disabled", disable);
		$('#country').attr("disabled", disable);
	}
	
	// prefill address	
	$("#prefill").change(function () {
		var value = $("#prefill").val();
		address = addresses[value];
		for (var name in address) {
			$('#'+name).val(address[name]);
		}
	});
	
});
</script>
{/literal}

{$form nofilter}

{include file="@footer.tpl"}
