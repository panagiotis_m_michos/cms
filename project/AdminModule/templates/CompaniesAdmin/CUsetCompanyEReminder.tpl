{include file="@header.tpl"}
        <div style="float: right; padding-bottom: 5px;">
            <a class="ui-widget-content ui-corner-all" style="padding: 3px;" href="{$this->router->link("CUsetCompanyEReminder", "company_id=$companyId")}">Refresh List</a>
            <a class="ui-widget-content ui-corner-all" style="padding: 3px;" href="{$this->router->link("CUsetCompanyEReminder", "company_id=$companyId","deleteAll=1")}" onclick="return confirm('Are you sure');">Delete All</a>
        </div>
        <div class="clear"></div>
        <div style="padding: 10px 0px 18px;">
 <table class="htmltable" width="650">
     <col width="430">
      <col width="110">
     <col width="110">
                     <tr>
                    <th class="left"><b>Current list of eReminder recipients</b></th>
                    <th class="left"><b>Status</b></th>
                    <th class="left"><b>Action</b></th>
                </tr>
                {if $list}
                    {assign var='counter' value=0}
                    {foreach from=$list key="key" item="recipient"}
                    {assign var=$counter value=$counter++}  
                    {assign var='email' value=$recipient->EmailAddress}
                    {assign var='activated' value=$recipient->Activated}
                    <tr>
                        <td>{$email}</td>
                        <td>
                            {if $activated == 'true'}
                            Active
                            {else}
                            Awaiting Activation
                            {/if}
                        </td>
                        <td><a href="{$this->router->link("CUsetCompanyEReminder", "company_id=$companyId", "emailToDelete=$email")}">delete</a></td>
                    </tr>
                    {/foreach}
                {else}
                    <tr>
                        <td colspan="3">eReminders not currently set up</td>
                    </tr>
                {/if}
                </table>
            </div>   
        {if !$list || $counter < 5}
            {$formAdd nofilter}
        {/if}
 
{include file="@footer.tpl"}