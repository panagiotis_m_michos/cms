{include file="@header.tpl"}

{$form->getBegin() nofilter}
<fieldset>
    <legend>No registrable person official wording</legend>
    <p>
        The PSC requirements apply whether your company has a PSC or not. If you have taken all reasonable
        steps and are confident that there are no individuals or legal entities which meet any of the
        conditions in relation to your company, please select the checkbox below:
    </p>
    {$form->getControl('noPscReason') nofilter}
    {$form->getLabel('noPscReason') nofilter}
    <p>
        Check the Guidance for Companies, Societates Europaeae and Limited Liability Partnerships for
        further information.
    </p>
    {$form->getControl('save') nofilter}
</fieldset>
{$form->getEnd() nofilter}

{include file="@footer.tpl"}
