{include file="@header.tpl"}

{$form nofilter}

<h2>Companies</h2>
{if !empty($companies)}
	<table class="htmltable">
	<col width="70">
	<col>
	<col width="90">
	<col width="100">
	<col width="90">
	<tr>
		<th>Id</th>
		<th>Name</th>
		<th>Number</th>
		<th>Date</th>
		<th>Action</th>
	</tr>
	{foreach from=$companies key="key" item="company"}
	<tr>
		<td class="center">{$company->getCompanyId()}</td>
		<td class="left">{$company->getCompanyName()}</td>
		<td class="left">{$company->getCompanyNumber()}</td>
		<td class="left">{if $company->getIncorporationDate() != null}{$company->getIncorporationDate()|date_format:"%d/%m/%Y"}{/if}</td>
		<td class="center"><a href="{$this->router->link("view","company_id=$key")}">View</a></td>
	<tr>
	{/foreach}
	</table>
	
	{$paginator nofilter}
	
{else}
	<p>No companies.</p>
{/if}

{include file="@footer.tpl"}
