{include file="@header.tpl"}

<ul class="submenu">
	<li><a href="{url route="CUpscOnlineRegisterAddPersonPsc" company_id=$companyId}">Add Person PSC</a></li>
	<li><a href="{url route="CUpscOnlineRegisterAddCorporatePsc" company_id=$companyId}">Add Corporate PSC</a></li>
	<li><a href="{url route="CUpscOnlineRegisterAddLegalPersonPsc" company_id=$companyId}">Add Legal Person PSC</a></li>
	{if $canAddCompanyStatement}
		<li><a href="{url route="CUpscOnlineRegisterNoPscReason" company_id=$companyId}">No registrable person notification</a></li>
	{/if}
</ul>

<div class="margin-bottom-20">
	<table class="htmltable" style="width: 100%">
		<colgroup>
			<col width="100">
			<col width="100">
			<col width="200">
			<col>
			<col width="100">
		</colgroup>
		<tr>
			<th>Date of entry</th>
			<th>Date cessated / withdrawn</th>
			<th>Name</th>
			<th>Nature of Controls</th>
			<th>Actions</th>
		</tr>
		{foreach $entries as $entry}
			{assign var='entryId' value=$entry->getId()}
			<tr>
				<td>{$entry->getDtc()|datetime}</td>
				<td>
					{if $entry->isPsc()}
						{$entry->getDateCessated()|datetime}
					{elseif $entry->isCompanyStatement()}
						{$entry->getDateWithdrew()|datetime}
					{/if}
				</td>
				{if $entry->isCompanyStatement()}
					<td>
						-
					</td>
					<td>
						<i>{$entry->getCompanyStatementText()}</i>
					</td>
				{elseif $entry->isPsc()}
					<td>
						{$entry->getFullName()}
					</td>
					<td>
						{foreach $entry->getNatureOfControlsTexts() as $noc}
							{$noc}<br>
						{/foreach}
					</td>
				{/if}
				<td class="center">
					{if $entry->isPerson()}
						<a href="{url route="CUpscOnlineRegisterEditPersonPsc" company_id=$companyId pscId=$entryId}">Edit</a>
					{elseif $entry->isCorporate()}
						<a href="{url route="CUpscOnlineRegisterEditCorporatePsc" company_id=$companyId pscId=$entryId}">Edit</a>
					{elseif $entry->isLegalPerson()}
						<a href="{url route="CUpscOnlineRegisterEditLegalPersonPsc" company_id=$companyId pscId=$entryId}">Edit</a>
					{/if}
					{if $entry->isPsc() && !$entry->getDateCessated()}
						<a href="{url route="CUpscOnlineRegisterCessatePsc" company_id=$companyId pscId=$entryId}">Cessate</a>
					{elseif $entry->isCompanyStatement() && !$entry->getDateWithdrew()}
						<a href="{url route="CUpscOnlineRegisterWithdrawStatement" company_id=$companyId statementId=$entryId}">Withdraw</a>
					{/if}
				</td>
			</tr>
		{/foreach}
	</table>
</div>

<p>
	<h4>Notes</h4>
    <ul class="ml15">
        <li>The PSC information listed here is manually inserted by the customer or the admin team.</li>
        <li>
            This page is for local record keeping only, it is not linked in any way with Companies House. The only way to notify Companies House of a PSC update is to file a
            <a href="{url route="AnnualReturnControler::COMPANY_DETAILS_PAGE"  company_id=$companyId}">Confirmation Statement</a>.
        </li>
    </ul>
</p>

{include file="@footer.tpl"}
