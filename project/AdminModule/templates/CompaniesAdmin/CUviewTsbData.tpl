{include file="@header.tpl"}

{if ($user->roleId == 1 || $user->roleId == 2) && !isset($tsbData)}
    <ul class="submenu">
        <li><a href="{$this->router->link("CUeditTsbData", "company_id=$companyId")}">Submit Lead</a></li>
    </ul>
{/if}

<h2>TSB Data</h2>
<table class="htmltable">
    <col width="250">
    <tr>
        <th>Email</th>
        <td>{if isset($tsbData)}{$tsbData->email}{/if}</td>
    </tr>
    <tr>
        <th>Title</th>
        <td>{if isset($tsbData)}{$tsbData->titleId}{/if}</td>
    </tr>
    <tr>
        <th>First Name</th>
        <td>{if isset($tsbData)}{$tsbData->firstName}{/if}</td>
    </tr>
    <tr>
        <th>Last Name</th>
        <td>{if isset($tsbData)}{$tsbData->lastName}{/if}</td>
    </tr>
    <tr>
        <th>Phone 1</th>
        <td>{if isset($tsbData)}{$tsbData->phone}{/if}</td>
    </tr>
    <tr>
        <th>Phone 2</th>
        <td>{if isset($tsbData)}{$tsbData->additionalPhone}{/if}</td>
    </tr>
</table>

{include file="@footer.tpl"}