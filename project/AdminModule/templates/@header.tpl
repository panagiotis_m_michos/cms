<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="{$currLng}">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="content-language"  content="{$currLng}" />
        <meta name="author" content="{$author}"/>
        <meta name="copyright" content="{$copyright}"/>
        <title>{$seoTitle} - admin</title>

        {styles file='webloader_admin.neon' section='common'}

        <script type="text/javascript">var urlRoot = "{$urlRoot}"</script>
        {scripts file='webloader_admin.neon' section='common'}

    </head>
    <body>
        <div id="envelope">
            <div id="header">
                <h1><a href="{$urlRoot}">{$projectName} - Admin</a></h1>
                <div class="user_info">{if isset($user)}Logged as {$user->login} | <a href="{$signoutUrl}">Logout</a>{/if}</div>
            </div> <!-- #header -->
            <div id="container1">

                <div id="menu">
                    <script type="text/javascript">
                        var url = '{$this->router->link()}';
                        d = new dTree('d', '{$urlImgs}icons/');
                        document.write('<div class="buttons treeHeader">{if $user->role->isAdmin()}<input type="checkbox" name="initDragAndDrop" id="initDragAndDrop" /> <label for="initDragAndDrop" title="stard/end drag and drop for the tree">Drag&amp;Drop</label> {/if}[<a href="#" onClick="d.openAll(); return false;">open all</a> ] [<a href="#" onClick="d.closeAll(); return false;">close all</a>]</div>');
                        {$jsTreeMenu nofilter}
                        document.write(d);
                        d.openTo({$this->node->getId()|escape:"javascript"}, true);
                    </script>

                    {if $user->role->isAdmin()}
                    {literal}
                    <script type="text/javascript">
                            (function(){
                                    var dragLoader = new DragAndDrop.DragLoader(d, url, 'a.node,a.nodeSel', dTreeOnDrop, dTreeOnDrag);
                                    dragLoader.afterInit = function() {
                                        $('div.dropAfter').css('height', '0.4em');
                                    }
                                    dragLoader.afterUnload= function() {
                                        $('div.dropAfter').css('height', '0');
                                    }
                                    $('#initDragAndDrop').click(function() {
                                        if ($(this).is(':checked')) {
                                            dragLoader.initDragElements();
                                        }
                                        else {
                                            dragLoader.unloadDragElements();
                                        }
                                    });
                                })();
                        </script>
                    {/literal}
                    {/if}
                </div>

                <div id="main_text">

                    <h1>{$title}</h1>

                    {* CONTROLS *}
                    {include file="@blocks/controls.tpl"}

                    {* FLASH MESSAGE *}
                    {include file="@blocks/flash_message.tpl"}

                    {* TABS *}
                    {include file="@blocks/tabs.tpl"}
