{include file="@header.tpl"}

{literal}
<style type="text/css">
#sortable { 
	list-style-type: none; 
	margin: 0; 
	padding: 0; 
	width: 60%; 
}
#sortable li { 
	margin: 0 5px 5px 5px; 
	padding: 5px 5px 5px 20px; 
	font-size: 1.2em; 
	height: 1.5em;
	border: 1px solid #464646; 
	width: 300px;
	background: #E4F2FD;
	position: relative;
}
html>body #sortable li { 
	height: 1.5em; 
	line-height: 1.2em; 
}
.ui-state-highlight { 
	height: 1.5em; 
	line-height: 1.2em;
	background: #FFFFE0; 
}
#sortable li span { 
	position: absolute; 
	margin-left: -1.3em; 
	background-image: url(http://static.jquery.com/ui/css/demo-docs-theme/images/222222_256x240_icons_icons.png);
	height: 16px;
	width: 16px;
	background-position: -128px -48px;
}
</style>
<script type="text/javascript">
$(document).ready(function() {

	function fillOrder() {
		var sum = ''; 
		$('#sortable li').each(function(item) {
			var value = $(this).attr('id');
			value = value.replace('item-', '');
			//var checked = $("input[id='items" + value + "']").is(":checked");
			//if (checked == true) {
				sum += value;
				if ($('#sortable li:last').attr('id') != $(this).attr('id')) {
					sum += ' ';
				}
			//}
		});
		$('#order').val(sum);
	}
	
	fillOrder();
	
	$("#sortable").sortable({
		placeholder: 'ui-state-highlight',
		stop: function (event, ui) {
			fillOrder();
		}
	}).disableSelection();
	
	$("input[name^='items']").click(function() {
		fillOrder();
	});
	
	
});
</script>
{/literal}

{$form->getBegin() nofilter}
<fieldset>
	<legend>Menu</legend>
	<table class="ff_table">
	<tr>
		<th>{$form->getLabel('items') nofilter}</th>
		<td>
			<ul id="sortable">
				{foreach from=$items key="key" item="item"}
					<li id="item-{$key}"><span></span>{$form->getControl("items", $key) nofilter}</li>
				{/foreach}
			</ul>
		</td>
	</tr>
	</table>
</fieldset>
<fieldset>
	<legend>HTML Block</legend>
	<table class="ff_table">
	<tr>
		<th>{$form->getLabel('htmlBlock') nofilter}</th>
		<td>{$form->getControl('htmlBlock') nofilter}</td>
	</tr>
	</table>
</fieldset>
<fieldset>
	<legend>Menu Collapser</legend>
	<table class="ff_table">
	<tr>
		<th>{$form->getLabel('menuCollapser') nofilter}</th>
		<td>{$form->getControl('menuCollapser') nofilter}</td>
	</tr>
	</table>
</fieldset>
<fieldset>
	<legend>Action</legend>
	<table class="ff_table">
	<tr>
		<th></th>
		<td>{$form->getControl('submit') nofilter}</td>
	</tr>
	</table>
</fieldset>
{$form->getControl("order") nofilter}
{$form->getEnd() nofilter}



{include file="@footer.tpl"}
