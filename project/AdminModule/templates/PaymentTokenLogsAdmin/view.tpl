{include file="@header.tpl"}

<h2>Token Info:</h2>

 <table class="htmltable">
<col width="150">
<tr>
	<th>Token Error Log Id:</th>
	<td>#{$error->getId()}</td>
</tr>
<tr>
	<th class="left">Token Status:</th>
	<td>{$error->tokenStatus}</td>
</tr>
<tr>
	<th class="left">Email:</th>
	<td>{$error->email}</td>
</tr>
<tr>
	<th class="left">Token Number:</th>
	<td>{$error->token}</td>
</tr>
<tr>
	<th class="left">Name:</th>
	<td>{$error->name}</td>
</tr>
<tr>
	<th class="left">Address:</th>
	<td>{$error->address}</td>
</tr>
<tr>
	<th class="left">Card Number:</th>
	<td>************{$error->cardNumber}</td>
</tr>
<tr>
	<th class="left">Date:</th>
	<td>{$error->dtc}</td>
</tr>
</table>
    
{include file="@footer.tpl"}