{include file="@header.tpl"}

<form action="{$this->router->link('clearCache')}" method="post">
    <fieldset>
        <legend>Clear memory and filesystem cache</legend>
        <input type="submit" value="Clear cache" />
    </fieldset>
</form>

{include file="@footer.tpl"}