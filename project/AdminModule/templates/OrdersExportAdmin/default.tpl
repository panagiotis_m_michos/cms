{include file="@header.tpl"}

{$formHelper->start($form) nofilter}
<div>
    {if !$hasErrors}
        <a href="#" class="paulo-ui-toggle advanced" data-toggle-block-selector="#block-form" data-toggle-text="Hide filter">Show filter</a>
    {/if}
</div>
<div id="block-form" class="{if !$hasErrors}display-none {/if}pt15">
    {$formHelper->errors($form) nofilter}
    <div>
        {$formHelper->widget($form['date_range']) nofilter}
    </div>
</div>
<div class="pt15">
    {$formHelper->widget($form['export']) nofilter}
</div>
{$formHelper->end($form) nofilter}

{include file="@footer.tpl"}