{include file="@header.tpl"}
{literal}
<style>
    table.ff_table th {
        text-align: left;
    }

</style>
{/literal}

{$form nofilter}

{if !empty($cashbacks)}
<table class="htmltable">
    <col width="70">
    <col width="90">
    <col>

    <col width="100">
    <col width="120">
    <tr>
        <th>Id</th>
        <th>Number</th>
        <th>Name</th>        
        <th>Customer</th>
        <th>Action</th>
    </tr>
	{foreach from=$cashbacks key="key" item="company"}
    <tr>
        <td class="center">{$company->company_id}</td>
        <td class="left">{$company->company_number}</td>
        <td class="left">{$company->company_name}</td>
        <td class="left">{$company->customer_id}</td>
        <td class="center"><a href="{$this->router->link("add","companyId=$key")}">Add Cashback</a></td>
    <tr>
	{/foreach}
</table>


{else}
<p>No companies.</p>
{/if}

{include file="@footer.tpl"}
