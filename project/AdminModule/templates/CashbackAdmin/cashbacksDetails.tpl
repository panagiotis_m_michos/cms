{include file="@header.tpl"}

<fieldset>
    <legend>Details</legend>
    <table class="htmltable" style="width: 700px;">
        <col width="170">
        <tr>
            <th>Customer Id:</th>
            <td>{$helper->customer->id}</td>
        </tr>
        <tr>
            <th>Customer Name:</th>
            <td>{$helper->customer->fullName}</td>
        </tr>
        <tr>
            <th>Status:</th>
            <td>{$helper->status}</td>
        </tr>
        {if $helper->isPaid()}
            <tr>
                <th>Amount Paid:</th>
                <td>{$helper->totalAmount|currency}</td>
            </tr>
            <tr>
                <th>Date paid:</th>
                <td>{$helper->datePaid|datetime}</td>
            </tr>
            {if $helper->isCashbackTypeBank()}
                <tr>
                    <th>Sort Code:</th>
                    <td>{$helper->customer->sortCode}</td>
                </tr>
                <tr>
                    <th>Account Number:</th>
                    <td>{$helper->customer->accountNumber}</td>
                </tr>
            {elseif $helper->isCashbackTypeCredits()}
                <tr>
                    <th>Credit on CMS account:</th>
                    <td>{$helper->customer->email}</td>
                </tr>
            {/if}
        {/if}
    </table>
</fieldset>

<fieldset>
    <legend>Accounts created</legend>
    <table class="htmltable" style="width: 700px;">
        <tr>
            <th width="80">Co. Number:</th>
            <th>Co. Name:</th>
            <th width="70">Date Sent:</th>
            <th width="70">Package:</th>
            <th width="70">Amount:</th>
        </tr>
        {foreach $helper->cashbacks as $cashback}
        <tr>
            <td>{$cashback->company->companyNumber}</td>
            <td>{$cashback->company->companyName}</td>
            <td>{$cashback->dateLeadSent|datetime}</td>
            <td>{$cashback->packageType}</td>
            <td>{$cashback->amount|currency}</td>
        </tr>
        {/foreach}
        <tr>
            <td colspan="5" style="text-align: right">Total Amount: {$helper->totalAmount|currency}</td>
        </tr>
    </table>
</fieldset>


{include file="@footer.tpl"}

