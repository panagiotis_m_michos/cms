{include file="@header.tpl"}
<fieldset>
    <legend>Barclays import notes</legend>
    <p>
        CSV must be in format: leadDate,companyNumber,openDate<br>
        companyNumber must be 7-8 characters long<br>
        leadDate, openDate must be in format dd/mm/yyyy<br>
        First row must be the headers. If unsure of the required formatting please view the <a href="/project/upload/files/barclays-cash-back-example.csv">[example CSV]</a>
    </p>
    <p>
        Import will only bring in the last 3 months of records (based on Open Date). To import an older record, please add it manually.
    </p>
</fieldset>

<fieldset>
    <legend>TSB import notes</legend>
    <p>
        CSV must be in format: companyNumber<br>
        companyNumber must be 7-8 characters long<br>
        First row must be the headers. If unsure of the required formatting please view the <a href="/project/upload/files/tsb-cash-back-example.csv">[example CSV]</a>
    </p>
</fieldset>

{$form nofilter}

{include file="@footer.tpl"}
