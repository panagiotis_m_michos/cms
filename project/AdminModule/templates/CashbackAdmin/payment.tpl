{include file="@header.tpl"}

<fieldset>
    <legend>Payment</legend>
    <table class="htmltable" style="width: 700px;">
        <col width="170">
        <tr>
            <th>Customer Id:</th>
            <td>{$helper->customer->id}</td>
        </tr>
        <tr>
            <th>Customer Name:</th>
            <td>{$helper->customer->fullName}</td>
        </tr>
        {if $helper->isCashbackTypeBank()}
            <tr>
                <th>Sort Code:</th>
                <td>{$helper->customer->sortCode}</td>
            </tr>
            <tr>
                <th>Account Number:</th>
                <td>{$helper->customer->accountNumber}</td>
            </tr>
        {elseif $helper->isCashbackTypeCredits()}
            <tr>
                <th>Credit on CMS account:</th>
                <td>{$helper->customer->email}</td>
            </tr>
        {/if}
        <tr>
            <th>Amount to pay:</th>
            <td>{$helper->totalAmount|currency}</td>
        </tr>
    </table>
</fieldset>

<fieldset>
    <legend>Accounts created</legend>
    <table class="htmltable" style="width: 700px;">
        <tr>
            <th width="80">Co. Number:</th>
            <th>Co. Name:</th>
            <th width="70">Date Sent:</th>
            <th width="70">Package:</th>
            <th width="70">Amount:</th>
        </tr>
        {foreach $helper->cashbacks as $cashback}
        <tr {if $cashback->isArchived()} class="archived" {/if}>
            <td>{$cashback->company->companyNumber}</td>
            <td>{$cashback->company->companyName}</td>
            <td>{$cashback->dateLeadSent|datetime}</td>
            <td>{$cashback->packageType}</td>
            <td>{$cashback->amount|currency}</td>
        </tr>
        {/foreach}
        <tr>
            <td colspan="5" style="text-align: right">Total Amount: {$helper->totalAmount|currency}</td>
        </tr>
    </table>
</fieldset>

<fieldset>
    <legend>Action</legend>
    {$form->begin nofilter}
    {$form->getControl('submit') nofilter}
    {if $form->hasActiveCashbacks()}
        {$form->getControl('archive') nofilter}
    {/if}

    {if $form->hasArchivedCashbacks()}
        {$form->getControl('restore') nofilter}
    {/if}
    {$form->end nofilter}
</fieldset>

<fieldset id="cashback-payment-faq">
    <legend>FAQ</legend>
    <h4>What to do when the account details are invalid and I can't pay them back?</h4>
    <ul>
        <li>You must send the customer an email asking him to update his account details on his Cash Back Claim <b>(https://www.companiesmadesimple.com/cash-back-claim.html)</b> page.</li>
        <li>You can only pay by using the account details that are showing on this page, do not pay using account details he sent you on your email. We want to make sure the audit trail is correct.</li>
    </ul>
</fieldset>

{include file="@footer.tpl"}

