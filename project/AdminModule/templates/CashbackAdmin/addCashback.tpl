{include file="@header.tpl"}

<p style="font-size: 14px; padding: 10px">
    Only manually add a record here if it does not appear in the All Cashbacks list once you have completed the latest
    import AND you have confirmed by Barclays that a bank account was opened via our referral scheme.
</p>

{$form nofilter}

{include file="@footer.tpl"}

{literal}
<style>
    table.ff_table th {
        text-align: left;
        height: 25px;
    }
</style>
{/literal}
