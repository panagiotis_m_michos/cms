{include file="@header.tpl"}


{* FILTER FORM *}
{$form->getBegin() nofilter}
	<fieldset>
		<legend>Filter</legend>
		<table border="0" cellspacing="5" cellpadding="0">
		<tr>
			<td>{$form->getLabel('dateFrom') nofilter}</td>
			<td>{$form->getControl('dateFrom') nofilter}</td>
			<td>{$form->getLabel('dateTo') nofilter}</td>
			<td>{$form->getControl('dateTo') nofilter}</td>
			<td>{$form->getControl('submit') nofilter}</td>
			<td>{$form->getError('dateFrom')} {$form->getError('dateTo')}</td>
		</tr>
		</table>
	</fieldset>
{$form->getEnd() nofilter}

<br />


{* RESULTS TABLE *}
{if isset($barclaysStats)}
	<table class="htmltable">
	<col width="130">
	<col>
	<col>
	<col>
	<col>
	<tr>
		<th>Date</th>
		<th>Companies Incorporated</th>
		<th>Sent to TSB</th>
		<th>Sent to Barclays</th>
		<th>Sent to banks total</th>
		<th>Barclays Errors</th>
	</tr>
		{foreach from=$barclaysStats item=value}
		<tr>
			{if !isset($value.date)}
			<td><b>Total</b></td>
			<td>{$value.incorporationsCount}</td>
			<td>{$value.sentTsbCount}</td>
			<td>{$value.sentBarclaysCount}</td>
			<td>{$value.sentTotal}</td>
			<td>{$value.barclaysErrorCount}</td>
			{else}
			<td>{$value.date}</td>
			<td>{$value.incorporationsCount}</td>
			<td>{$value.sentTsbCount}</td>
			<td>{$value.sentBarclaysCount}</td>
			<td>{$value.sentTotal}</td>
			<td>{$value.barclaysErrorCount}</td>
			{/if}
		</tr>
		{/foreach}
	</table>
{else}
	<p>No Data to Show</p>
{/if}
{include file="@footer.tpl"}
