{include file="@header.tpl"}


{literal}
<script type="text/javascript">
<!--
$(document).ready(function () {
    $('#csv_export').attr('href',$("form[name=form_name]").attr('action')+'&csv=1');

});
//-->
</script>
{/literal}

{* FILTER *}
{$filter nofilter}

{* DATA *}
<h2>Orders</h2>


{if !empty($data)}

    <p style="position: relative;"><a id="csv_export" href="{$this->router->link(null, 'csv=1')}" style="position: absolute; top: -20px; right: 5px;">Export to CSV</a>

	<table class="htmltable">
    <col width="70">
    <col>
    <col>
    <col width="70">
    <col width="70">
    <col width="90">
    <col width="90">
	<tr>
		<th>Id</th>
		<th>Name</th>
		<th>Email</th>
		<th>Total</th>
		<th>Refund</th>
		<th>Type</th>
		<th>Created</th>
	</tr>
	{foreach from=$data item="row"}
		<tr>
			<td>{$row.orderId}</td>
			<td>{$row.customerName}</td>
			<td>{$row.customerEmail}</td>
			<td>{$row.total}</td>
			<td>{$row.refundValue}</td>
			<td>{$row.type}</td>
			<td>{$row.dtc|df:"d/m/Y"}</td>
		</tr>
	{/foreach}
	</table>
	
	{$paginator nofilter}
{else}
	<p>No Records</p>
{/if}


{include file="@footer.tpl"}