{include file="@header.tpl"}
{$form->getBegin() nofilter}
	<fieldset>
		<legend>Filter</legend>
		<table border="0" cellspacing="5" cellpadding="0">
		<tr>
			<td>{$form->getLabel('dateFrom') nofilter}</td>
			<td>{$form->getControl('dateFrom') nofilter}</td>
			<td>{$form->getLabel('dateTo') nofilter}</td>
			<td>{$form->getControl('dateTo') nofilter}</td>
			<td>{$form->getControl('submit') nofilter}</td>
			<td>{$form->getError('dateFrom')} {$form->getError('dateTo')}</td>
		</tr>
		</table>
	</fieldset>
{$form->getEnd() nofilter}

{if !empty($incorporation)}
    {$incorporation nofilter}
{/if}


<table class="htmltable">
    <col width="365">
    <tr>
        <th>Total</th>
        <th>{$total}</th>
    </tr>
</table>


{literal}
<style>
    table.form-table th {
        text-align: left;
        padding: 0 5px 0 0;
        width: 120px;
        font-weight: normal;
    }
    
    .submit_button {
    background-color: #E0DFF7;
    border-color: #EDEDED #B4B4B4 #B4B4B4 #EDEDED;
    border-left: 1px solid #EDEDED;
    border-style: solid;
    border-width: 1px;
    color: black;
    height: 20px;
    margin: 0;
    padding: 0 5px 2px;
    }
</style>
{/literal}

{include file="@footer.tpl"}