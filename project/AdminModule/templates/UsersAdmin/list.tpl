{include file="@header.tpl"}

<p><a href="{$this->router->link('create')}">[create new]</a></p>
<table class="htmltable">
<col width="50">
<col width="80">
<col>
<col>
<col>
<col>
<col>
<col width="60">
<col width="60">
<tr>
	<th>Id</th>
	<th>Status</th>
	<th>Login</th>
	<th>Email</th>
	<th>Role</th>
	<th>Created</th>
	<th>Edited</th>
	<th>Action</th>
</tr>
{foreach from=$users key="id" item="user"}
<tr>
	<td>{$user->getId()}</td>
	<td>{$user->status}</td>
	<td>{$user->login}</td>
	<td>{$user->email}</td>
	<td>{$user->role->title}</td>
	<td>{$user->dtc|date_format:"%d/%m/%Y"}</td>
	<td>{$user->dtm|date_format:"%d/%m/%Y"}</td>
	<td class="center"><a href="{$this->router->link("edit", "user_id=$id")}">edit</td>
</tr>
{/foreach}
</table>

{include file="@footer.tpl"}
