{include file="@header.tpl"}

{* SUBMENU *}
<ul class="submenu">
	<li><a href="{$this->router->link("edit", "mrSiteCodeId=$mrSiteCodeId", "keepThis=true", "TB_iframe=true", "width=600", "height=550")}" class="thickbox">Edit</a></li>
	<li><a href="{$this->router->link("delete", "mrSiteCodeId=$mrSiteCodeId")}" onclick="return confirm('Are you sure?');">Delete</a></li>
</ul>

{* DETAILS *}
<h2>DETAILS</h2>

<table class="htmltable">
<col width="150">
<tr>
	<th>OrderId</th>
	<td>
		{if $code->orderId}
			{assign var="orderId" value=$code->orderId}
			<a href="{$this->router->link("OrdersAdminControler::ORDERS_PAGE view", "order_id=$orderId")}">{$orderId}</a>
		{else}
			N/A
		{/if}
	</td>
</tr>
<tr>
	<th>Product</th>
	<td>
		{assign var="product" value=$code->getProduct()}
		{$product->getLngTitle()}
	</td>
</tr>
<tr>
	<th>Code</th>
	<td>{$code->code}</td>
</tr>
<tr>
	<th>Dtc</th>
	<td>{$code->dtc|date_format:"%d/%m/%Y %H:%M"}</td>
</tr>
<tr>
	<th>Dtc</th>
	<td>{$code->dtm|date_format:"%d/%m/%Y %H:%M"}</td>
</tr>
</table>

{include file="@footer.tpl"}