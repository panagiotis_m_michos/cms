{include file="@header.tpl"}

<ul class="submenu">
	<li><a href="{$this->router->link("create", "keepThis=true", "TB_iframe=true", "width=600", "height=550")}" class="thickbox">Create</a></li>
</ul>

<h2>Codes</h2>

{if !empty($codes)}
	<table class="htmltable">
	<col width="80">
	<col width="80">
	<col width="130">
	<col>
	<col width="120">
	<col width="60">
	<tr>
		<th>Id</th>
		<th>OrderId</th>
		<th>ProductId</th>
		<th>Code</th>
		<th class="center">Created</th>
		<th class="center">Action</th>
	</tr>
	{foreach from=$codes key="key" item="code"}
	<tr>
		<td>{$key}</td>
		<td class="right">{if $code->orderId}{$code->orderId}{else}N/A{/if}</td>
		{assign var="product" value=$code->getProduct()}
		<td class="right">{$product->getLngTitle()}</td>
		<td class="left">{$code->code}</td>
		<td class="center">{$code->dtc|date_format:"%d/%m/%Y %H:%M"}</td>
		<td class="center"><a href="{$this->router->link("view","mrSiteCodeId=$key")}">View</a></td>
	</tr>
	{/foreach}
	</table>
	
	{$paginator nofilter}
	
{else}
	<p>No codes.</p>
{/if}


{include file="@footer.tpl"}