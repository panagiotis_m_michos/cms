{if $paymentBasket}
    <table class="htmltable" style="width:98%;">
        <col>
        <col width="70">
        <col width="100">
        <tr>
            <th>Product</th>
            <th>Quantity</th>
            <th>Price</th>
        </tr>
        {foreach $paymentBasket->items as $item}
            <tr>
                <td>{$item->getLngTitle()}</td>
                <td align="center" valign="top"> 
                    {$item->qty}
                </td>
                <td>&pound;{$item->totalPrice}</td>
            </tr>
        {/foreach}
        <tr class="bb">
            <td colspan="2"><strong>Subtotal</strong></td>
            <td><strong>&pound;{$paymentBasket->getPrice('subTotal')}</strong></td>
        </tr>
        {assign var="voucher" value=$paymentBasket->getVoucher()}
        {if $voucher}
            <tr>
                <td colspan="2">Voucher "{$voucher->name}"</td>
                <td>-{$voucher->discount($paymentBasket->getPrice('subTotal'))|currency}</td>
            </tr>
        {/if}
        <tr>
            <td colspan="2">VAT</td>
            <td >&pound;{$paymentBasket->getPrice('vat')}</td>
        </tr>
        <tr class="bb">
            <td colspan="2"><strong>Total including delivery charges and tax</strong></td>
            <td><strong>&pound;<span class="pvp-price">{$paymentBasket->getPrice('total')}</span></strong></td>
        </tr>
    </table>
{/if}
