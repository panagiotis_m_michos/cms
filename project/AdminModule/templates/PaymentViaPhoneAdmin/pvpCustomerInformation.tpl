{if $customer}
    <div id="customerPvpDetails" style="width:600px;">
        <h2>{$customer->getFullName()}</h2>

        <table class="htmltable" style="width: 580px;">
            <col width="200">
            <tr>
                <th>Email:</th>
                <td>{$customer->getEmail()}</td>
            </tr>
            <tr>
                <th>Phone:</th>
                <td>{$customer->getPhone()}</td>
            </tr>
        </table>

        <br>

        <table class="htmltable" style="width: 580px;">
            <col width="200">
            <tr>
                <th>Title:</th>
                <td>{$customer->getTitle()}</td>
            </tr>
            <tr>
                <th>First name:</th>
                <td>{$customer->getFirstName()}</td>
            </tr>
            <tr>
                <th>Last name:</th>
                <td>{$customer->getLastName()}</td>
            </tr>
        </table>

        <br>

        <table class="htmltable" style="width: 580px;">
            <col width="200">
            <tr>
                <th>Account Type:</th>
                <td>{$customer->getRole()}</td>
            </tr>
            <tr>
                <th>Available Credit:</th>
                <td>{$customer->getCredit()|string_format:"%.2f"}</td>
            </tr>
            <tr>
                <th>Subscribed to Newsletter:</th>
                <td>{if $customer->getIsSubscribed()}Yes{else}No{/if}</td>
            </tr>
        </table>

        <br/>

        <table class="htmltable" style="width: 580px;">
            <col width="200">
            <tr>
                <th>Address 1:</th>
                <td>{$customer->getAddress1()}</td>
            </tr>
            <tr>
                <th>Address 2:</th>
                <td>{$customer->getAddress2()}</td>
            </tr>
            <tr>
                <th>Address 3:</th>
                <td>{$customer->getAddress3()}</td>
            </tr>
            <tr>
                <th>County:</th>
                <td>{$customer->getCounty()}</td>
            </tr>
            <tr>
                <th>City:</th>
                <td>{$customer->getCity()}</td>
            </tr>
            <tr>
                <th>Postcode:</th>
                <td>{$customer->getPostcode()}</td>
            </tr>
            <tr>
                <th>Country:</th>
                <td>{$customer->getCountry()}</td>
            </tr>
        </table>
    </div>
{else}
    <h2>We don't have a customer with this ID</h2>
{/if}    
