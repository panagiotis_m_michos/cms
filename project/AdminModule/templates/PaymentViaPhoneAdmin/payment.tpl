{include file="@header.tpl"}
{literal}
     <style type="text/css">
        .pvp-basket-product-error, .pvp-basket-package-error {
            display: none;
        }
        .payment-via-phone #asmSelect0 {
            width: 300px;
        }
        .payment-via-phone #asmList0 {
            width: 304px;
        }
    </style>
{/literal}

<div class="payment-via-phone">
    <fieldset id="fieldset_payment_via_phone_operator">
        <legend>Operator Information</legend>
        Name: {$user->firstName} {$user->lastName} <br />
        User: {$user->login}
    </fieldset>

    {$form->getBegin() nofilter}
        {if $form->getErrors()|@count > 0}
            <p class="ff_err_notice">Form has <b> {$form->getErrors()|@count}</b> error(s). See below for more details:</p>
        {/if}

        <div class="pvp-basket-template">
            <fieldset id="fieldset_4">
                <legend>Products</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <th>{$form->getLabel('packages') nofilter} </th>
                            <td>{$form->getControl('packages') nofilter}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('products') nofilter} </th>
                            <td>{$form->getControl('products') nofilter}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('voucher') nofilter} </th>
                            <td>{$form->getControl('voucher') nofilter}</td>
                            <td></td>
                        </tr>
                      <tr>
                    </tbody>
                </table>
            </fieldset>

            <fieldset id="fieldset_5_payment_via_phone" >
                <legend>Basket</legend>
                <table class="ff_table pvp-basket-errors">
                    <tr>
                        <td>
                            <div class="pvp-basket-package-error">Please select a package!</div>
                            <div class="pvp-basket-product-error">Please select a product!</div>
                        </td>
                    </tr>
                </table>
                <div id="pvp-basket"></div>
            </fieldset>
        </div>

        <fieldset id="fieldset_check_email" >
            <legend>Check Email</legend>
            <table class="ff_table">
                <tr>
                    <td colspan="2">
                        <p>Enter the customer's email to check whether they already have an account.</p>
                    </td>
                </tr>
                <tr>
                    <th>{$form->getLabel('emails') nofilter}</th>
                    <td>
                        {$form->getControl('emails') nofilter}
                        <a href="{$this->router->link("pvpCustomerEmail")}" class="pvp-check-email-avaiability" style="text-decoration: none; ">
                            <button type="button" style="cursor: pointer;">Check email</button>
                        </a>
                    </td>
                </tr>
            </table>
        </fieldset>

        <fieldset id="fieldset_new_customer" style="display: none;">
            <legend>New Customer</legend>
            <table class="ff_table">
                <tr>
                    <td colspan="2">
                        <p>
                            Email address is not on our system. Their account will be created when the payment is taken and they will be emailed their password.
                        </p>
                    </td>
                </tr>
                <tr>
                    <th><strong>Email:</strong></th>
                    <td>
                        <span class="pvp-customer-email"></span>
                        <a href="javascript:" class="pvp-edit-email-avaiability" style="text-decoration: underline;color: #2683AE">
                            Edit
                        </a>
                    </td>
                </tr>
                <tr>
                    <th>{$form->getLabel('phone') nofilter}</th>
                    <td>
                        {$form->getControl('phone') nofilter}
                        <span class="redmsg">{$form->getError('phone')}</span>
                        <span class="ff_desc">{$form['phone']->getDescription() nofilter}</span>
                    </td>
                </tr>
            </table>
        </fieldset>

        <fieldset id="fieldset_current_customer" style="display: none;">
            <legend>Account Exists</legend>
            <table class="ff_table">
                <tr>
                    <td colspan="2">
                        <p>
                            Please confirm 3 details on their account before continuing.
                        </p>
                    </td>
                </tr>
                <tr>
                    <th><strong>Customer:</strong></th>
                    <td>
                    <a href="{$this->router->link("pvpCustomerInformation")}" class="thickbox pvp-customer-name"></a>
                    </td>
                </tr>
                <tr>
                    <th><strong>Email:</strong></th>
                    <td>
                        <span class="pvp-customer-email"></span>
                        <a href="javascript:" class="pvp-edit-email-avaiability" style="text-decoration: underline;color: #2683AE ">
                            Edit
                        </a>
                    </td>
                </tr>
            </table>
        </fieldset>

        <div class="pvp-payment-form" style="display: none">
            <fieldset id="fieldset_payment_details" >
                <legend>Payment Details</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <th>{$form->getLabel('amount') nofilter} <span style="margin-left:5px;" class="pound-style">&pound;</span></th>
                            <td >{$form->getControl('amount') nofilter}</td>
                            <td></td>
                        </tr>
                      <tr>
                            <th>{$form->getLabel('cardholder') nofilter}</th>
                            <td>{$form->getControl('cardholder') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('cardholder')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('cardType') nofilter}</th>
                            <td>{$form->getControl('cardType') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('cardType')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('cardNumber') nofilter}</th>
                            <td>{$form->getControl('cardNumber') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('cardNumber')}</span></td>
                        </tr>
                    <noscript>
                    <tr>
                        <th>{$form->getLabel('issueNumber') nofilter}</th>
                        <td>{$form->getControl('issueNumber') nofilter}</td>
                        <td>&nbsp</td>
                    </tr>
                    <tr>
                        <th>{$form->getLabel('validFrom') nofilter}</th>
                        <td>{$form->getControl('validFrom') nofilter}</td>
                        <td><span class="redmsg">{$form->getError('validFrom')}</span></td>
                    </tr>
                    </noscript>
                    <tr class="issuered" style="display:none;">
                        <th>{$form->getLabel('issueNumber') nofilter}</th>
                        <td>
                            {$form->getControl('issueNumber') nofilter}
                        </td>
                        <td><span class="redmsg">{$form->getError('issueNumber')}</span></td>
                    </tr>
                    <tr class="issuered" style="display:none;">
                        <th>{$form->getLabel('validFrom') nofilter}</th>
                        <td>{$form->getControl('validFrom') nofilter}</td>
                        <td><span class="redmsg">{$form->getError('validFrom')}</span></td>
                    </tr>
                    <tr>
                        <th>{$form->getLabel('expiryDate') nofilter}</th>
                        <td>{$form->getControl('expiryDate') nofilter}</td>
                        <td><span class="redmsg">{$form->getError('expiryDate')}</span></td>
                    </tr>
                    <tr>
                        <th>{$form->getLabel('CV2') nofilter}</th>
                        <td style="vertical-align: middle">
                            {$form->getControl('CV2') nofilter}
                            {*<a href="#" class="help-icon">help</a>*}
                            {*<div> The security code is a three-digit code printed on the back of your card.</div>*}
                        </td>
                        <td><span class="redmsg">{$form->getError('CV2')}</span></td>
                    </tr>
                    <tr>
                        <th>{$form->getLabel('address1') nofilter}</th>
                        <td>{$form->getControl('address1') nofilter}</td>
                        <td><span class="redmsg">{$form->getError('address1')}</span></td>
                    </tr>
                    <tr>
                        <th>{$form->getLabel('address2') nofilter}</th>
                        <td>{$form->getControl('address2') nofilter}</td>
                        <td><span class="redmsg">{$form->getError('address2')}</span></td>
                    </tr>
                    <tr>
                        <th>{$form->getLabel('address3') nofilter}</th>
                        <td>{$form->getControl('address3') nofilter}</td>
                        <td><span class="redmsg">{$form->getError('address3')}</span></td>
                    </tr>
                    <tr>
                        <th>{$form->getLabel('town') nofilter}</th>
                        <td>{$form->getControl('town') nofilter}</td>
                        <td><span class="redmsg">{$form->getError('town')}</span></td>
                    </tr>
                    <tr>
                        <th>{$form->getLabel('postcode') nofilter}</th>
                        <td>{$form->getControl('postcode') nofilter}</td>
                        <td><span class="redmsg">{$form->getError('postcode')}</span></td>
                    </tr>
                    <tr>
                        <th>{$form->getLabel('country') nofilter}</th>
                        <td>{$form->getControl('country') nofilter}</td>
                        <td><span class="redmsg">{$form->getError('country')}</span></td>
                    </tr>
                    <noscript>
                    <tr>
                        <th>{$form->getLabel('billingState') nofilter}</th>
                        <td>{$form->getControl('billingState') nofilter}</td>
                        <td><span class="redmsg">{$form->getError('billingState')}</span></td>
                    </tr>
                    </noscript>
                    <tr class="state" style="display:none;">
                        <th>{$form->getLabel('billingState') nofilter}</th>
                        <td>{$form->getControl('billingState') nofilter}</td>
                        <td><span class="redmsg">{$form->getError('billingState')}</span></td>
                    </tr>
                    </tbody>
                </table>
            </fieldset>

            <fieldset id="fieldset_8_payment_via_phone">
                <legend>Action</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <th>&nbsp;</th>
                            <td>{$form->getControl('submit') nofilter}</td>
                            <td>&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
             </fieldset>
            <fieldset id="fieldset_8_payment_via_phone">
                <legend>Cancel</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <th>&nbsp;</th>
                            <td>{$form->getControl('cancel') nofilter}</td>
                            <td>&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
             </fieldset>
        </div>
    {$form->getEnd() nofilter}
</div>

{literal}
    <script type="text/javascript">
    $(document).ready(function(){
        $('#emails').keypress(function(e) {
            if(e.which == 13) {
               checkPvpEmail($(".pvp-check-email-avaiability"), $("#emails").val());
               return false;
            }
        });
        if (window.CMS.Validator.validateEmail($("#emails").val())) {
            checkPvpEmail($(".pvp-check-email-avaiability"), $("#emails").val());
        }

        $(".pvp-check-email-avaiability").click(function(){
            checkPvpEmail(this, $("#emails").val());
            return false;
        });

        $(".pvp-edit-email-avaiability").click(function(){
            doReset();
        });

        $('.payment-via-phone #products, .payment-via-phone #packages, .payment-via-phone #voucher').change(function(e, data){
            calculateBasket();
        });

        $('#country option:selected').each(function () {
		if($(this).text() == "United States")
		{
	      	$('.state').show();
	    }
	    else
	    {
	       	$('.state').hide();
	    }
	});

	$('#cardType option:selected').each(function () {
		if(($(this).text() == "Maestro") || ($(this).text() == "Solo"))
		{
	       	$('.issuered').show();
	    }
	    else
	    {
	      	$('.issuered').hide();
	    }
	});

	$('#cardType').change(function () {
		$('#cardType option:selected').each(function () {
			if(($(this).text() == "Maestro") || ($(this).text() == "Solo"))
			{
	         	$('.issuered').show();
	        }
	        else
	        {
	        	$('.issuered').hide();
	        }
	     });
	});


	$('#country').change(function () {
		$('#country option:selected').each(function () {
			if($(this).text() == "United States")
			{
	         	$('.state').show();
	        }
	        else
	        {
	        	$('.state').hide();
	        }
	     });
	});

    $(".help-button2 a").hover(
        function() {
            $(this).next("em").stop(true, true).animate({opacity: "show"}, "slow");
        },
        function() {
            $(this).next("em").animate({opacity: "hide"}, "fast");
        }
    );

    });

    var calculateBasket = function()
    {
        $("#packages option:selected").each(function () {
            if ($(this).val() > 0) {
                $('.pvp-basket-package-error').hide();

                var products = [];
                $("#products option:selected").each(function () {
                       products.push($(this).val()) ;
                });

                $('.pvp-basket-product-error').hide();
                $("#pvp-basket").load(
                    '/admin/en/1258/paymentBasket/',
                    {
                        'packages': $(this).val(),
                        'products[]': products,
                        'voucher': $('#voucher').val()
                    },
                    function () {
                        var price = $('.pvp-price').text();
                        $('#amount').val(price);
                        $(".pvp-payment-form").show();
                    }
                );
            } else {
                $('.pvp-basket-package-error').show();
                $("#pvp-basket").html('&nbsp');
                $(".pvp-payment-form").hide();
            }
         });
    };

    var checkPvpEmail = function(self,email) {
        if(window.CMS.Validator.validateEmail(email)) {
            $('.new-err-msg').remove();
            producePvpEmail(self,email);
        } else {
            $('.new-err-msg').remove();
            $('.pvp-check-email-avaiability').after('<span class="ff_control_err new-err-msg" style="color:red">Email address is not valid!</span>');
        }
    };

    var producePvpEmail = function(elem,email) {

        $.ajax({
            url : $(elem).attr('href'),
            type : 'GET',
            dataType: "json",
            data : {
                'emails': encodeURIComponent(email),
            },
            success : function(data){
                if(data.success == 1) {
                    onSuccess1(data, email);
                } else if(data.success == 0){
                    onSuccess0(data, email);
                } else {
                    //console.log('email invalid');
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert('Oops...an error occurred (form error)');
            },
            timeout: function () {
                alert('Oops...an error occurred (form timeout)');
            }
        });
    };

    var onSuccess1 = function(data,email) {
        $('#fieldset_check_email').hide();
        $('#fieldset_current_customer').find('.pvp-customer-email').text(email);
        var link = $('.pvp-customer-name').attr('href');
        $('#fieldset_current_customer').find('.pvp-customer-name').attr('href',link+"?customerId="+data.customerId+"&height=520");
        $('#fieldset_current_customer').find('.pvp-customer-name').html(data.customerName);
        $('#fieldset_current_customer').show();
        $('#newCustomer').val(0);
        calculateBasket();
        $(".pvp-basket-template").show();
    };

    var onSuccess0 = function(data,email) {
        $('#fieldset_check_email').hide();
        $('#fieldset_new_customer').find('.pvp-customer-email').text(email);
        $('#fieldset_new_customer').show();
        $('#newCustomer').val(1);
        calculateBasket();
        $(".pvp-basket-template").show();
    };

    var doReset = function() {
        $('#fieldset_check_email').show();
        $('#fieldset_new_customer').hide();
        $('#fieldset_current_customer').hide();
        $('#newCustomer').val(0);
    }

    calculateBasket();
    </script>

{/literal}


{include file="@footer.tpl"}
