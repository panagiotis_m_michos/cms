 {include file="@header.tpl"}

    {if $emailLog->hasCustomer()}
        {assign var="customer" value=$emailLog->getCustomer()}
    <h2>Customer info</h2>
    <table class="htmltable">
    <col width="150">
    <tr>
        <th>Customer name:</th>
        <td><a
        href="{$this->router->link("CustomersAdminControler::CUSTOMERS_PAGE view", "customer_id={$customer->getId()}")}">{$customer->firstName} {$customer->lastName}</a></td>
    </tr>
    <tr>
        <th>Email:</th>
        <td>{$customer->email}</td>
    </tr>
    </table>
    {/if}

    <h2>Mail Log</h2>
    {*<a href="{link "resendEmail", "emailLogsId={$emailLog->getEmailLogsId()}"}"
   style="float: right" onclick="return confirm('Are you sure?')">[Resend
   Email]</a>*}
    <table class="htmltable">
    <col width="120">
    <tr>
        <th>Id:</th>
        <td>{$emailLog->getId()}</td>
    </tr>
    <tr>
        <th>Title:</th>
        <td>{$emailLog->getEmailName()}</td>
    </tr>
    <tr>
        <th>From:</th>
        <td>{$emailLog->getEmailFrom()|implode:", "}</td>
    </tr>
    <tr>
        <th>To:</th>
        <td>{!$emailLog->getEmailTo()|implode:", <br />"}</td>
    </tr>
    <tr>
        <th>Subject:</th>
        <td>{$emailLog->getEmailSubject()}</td>
    </tr>
    <tr>
        <th>Body:</th>
        <td>{$emailLog->getEmailBody() nofilter}</td>
    </tr>
    <tr>
        <th>Attachment:</th>
        <td>{$emailLog->getAttachment()}</td>
    </tr>
    <tr>
        <th>Date:</th>
        <td>{$emailLog->getDtc()->format("d/m/Y G:i")}</td>
    </tr>
    </table>
{include file="@footer.tpl"}


