{include file="@header.tpl"}

{literal}
<script type="text/javascript">
$(document).ready(function(){

    // company number
	if ($("#requiredCompanyNumber").is(":checked")) {
		disable(false);
	} else {
		disable(true);
	}
	
	$("#requiredCompanyNumber").click(function (){
		if ($(this).is(":checked")) {
			disable(false);
		} else {
			disable(true);
		} 
	});
	
	function disable(disabled) {
		$('#onlyOurCompanies').attr("disabled", disabled);
		$('#saveToCompany').attr("disabled", disabled);
	}
	
	// items in basket
	if ($("#onlyOneItem").is(":checked")) {
		disable2(true);
	} else {
		disable2(false);
	}
	
	$("#onlyOneItem").click(function (){
		if ($(this).is(":checked")) {
			disable2(true);
		} else {
			disable2(false);
		} 
	});
	
	function disable2(disabled) {
		$('#maxQuantityOne').attr("disabled", disabled);
	}
});
</script>
{/literal}

{$form nofilter}

{include file="@footer.tpl"}