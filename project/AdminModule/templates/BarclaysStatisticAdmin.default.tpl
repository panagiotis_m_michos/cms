{include file="@header.tpl"}


{* FILTER FORM *}
{$form->getBegin() nofilter}
	<fieldset>
		<legend>Filter</legend>
		<table border="0" cellspacing="5" cellpadding="0">
		<tr>
			<td>{$form->getLabel('date_from') nofilter}</td>
			<td>{$form->getControl('date_from') nofilter}</td>
			<td>{$form->getLabel('date_till') nofilter}</td>
			<td>{$form->getControl('date_till') nofilter}</td>
			<td>{$form->getControl('submit') nofilter}</td>
			<td>{$form->getError('date_from')} {$form->getError('date_till')}</td>
		</tr>
		</table>
	</fieldset>
{$form->getEnd() nofilter}

<br />


{* RESULTS TABLE *}
{if isset($barclaysStats)}
	<table class="htmltable">
	<col width="130">
	<col>
	<col>
	<col>
	<col>
	<tr>
		<th>Date</th>
		<th>Companies Purchased</th>
		<th>Barclays in Basket</th>
		<th>Removed from Basket</th>
		<th>Sent to Barclays</th>	
		<th>Errors</th>
	</tr>
		{foreach from=$barclaysStats item=value}
		<tr>
			{if !isset($value.date)}
			<td><b>Total</b></td>
			<td>{$value.all}</td>
			<td>{$value.barclays}</td>
			<td>{$value.removed}</td>
			<td>{$value.sent}</td>
			<td>{$value.error}</td>
			{else}
			<td>{$value.date}</td>
			<td>{$value.all}</td>
			<td>{$value.barclays}</td>
			<td>{$value.removed}</td>
			<td>{$value.sent}</td>
			<td>{$value.error}</td>
			{/if}		
		</tr>
		{/foreach}
	</table>
{else}
	<p>No Data to Show</p>
{/if}
{include file="@footer.tpl"}