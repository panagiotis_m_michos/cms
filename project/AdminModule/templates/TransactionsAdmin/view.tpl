{include file="@header.tpl"}

<table class="htmltable">
<col width="200">
<tr>
    <th>Customer</th>
    <td>
        {assign var="customer" value=$transaction->customer}
        {if $customer}
            <a href="{$this->router->link("CustomersAdminControler::CUSTOMERS_PAGE view", "customer_id={$customer->getId()}")}">{$customer->email}</a>
        {/if}
    </td>
</tr>
<tr>
    <th>Status</th>
    <td>
        <span {if $transaction->isFailed()}style="color: red;"{/if}>{$transaction->status}</span>
    </td>
</tr>
<tr>
    <th>Type</th>
    <td>{$transaction->type}</td>
</tr>
<tr>
    <th>Card Holder</th>
    <td>{$transaction->cardHolder}</td>
</tr>
<tr>
    <th>Card Number</th>
    <td>{$transaction->cardNumber}</td>
</tr>
<tr>
    <th>Order Id</th>
    <td>{$transaction->orderId}</td>
</tr>
<tr>
    <th>Order Code</th>
    <td>{$transaction->orderCode}</td>
</tr>
<tr>
    <th>Error</th>
    <td class="noOverflow">{$transaction->error}</td>
</tr>
<tr>
    <th>Details</th>
    <td>{$transaction->details}</td>
</tr>
<tr>
    <th>Request</th>
    <td>{$transaction->request}</td>
</tr>
<tr>
    <th>Vendor TX Code</th>
    <td>{$transaction->vendorTXCode}</td>
</tr>
<tr>
    <th>Vps Auth Code</th>
    <td>{$transaction->vpsAuthCode}</td>
</tr>
<tr>
    <th>Create</th>
    <td>{$transaction->dtc}</td>
</tr>
</table>

{include file="@footer.tpl"}
