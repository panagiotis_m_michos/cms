{include file="@header.tpl"}

<table class="htmltable">
<col width="70">
<col>
<col width="100">
<col width="100">
<col>
<col width="70">
<tr>
	<th>Id</th>
	<th>Name</th>
	<th>Number</th>
	<th>Inc. Date</th>
	<th>Customer</th>
	<th>MA</th>
    <th>MA Cover Letter</th>
</tr>
{foreach from=$memorandumArticles key="id" item="ma"}
<tr>
	<td>{$ma.company_id}</td>
	<td>{$ma.company_name}</td>
	<td>{$ma.company_number}</td>
	<td>{$ma.incorporation_date|date_format:"%d/%m/%Y"}</td>
	<td>{$ma.firstName} {$ma.lastName}</td>
    <td class="center">
        {if $ma.hasMA != 1}
            <a href="{$this->router->link("view", "ma_id=$id")}" target="_blank">view</a>
        {/if}
    </td>
    <td class="center">
        {if $ma.hasMACoverLetter != 1}
            <a href="{$this->router->link("view", "macoverletter_id=$id")}" target="_blank">view</a>
        {/if}
    </td>
	
</tr>
{/foreach}
</table>

{* FORM *}
<fieldset>
	<legend>Action</legend>
	{$form->getBegin() nofilter}
	<table>
	<col width="150">
	<col width="150">
	<col width="150">
        <col width="150">
	<tr>
	<td>{$form->getControl("printMA") nofilter}</td>
        <td>{$form->getControl("printMACovers") nofilter}</td>
	<td>{$form->getControl("markAllPrinted") nofilter}</td>
        <td>{$form->getControl("printMACoversClean") nofilter}</td>
	</tr>
	</table>
	{$form->getEnd() nofilter}
</fieldset>

{include file="@footer.tpl"}