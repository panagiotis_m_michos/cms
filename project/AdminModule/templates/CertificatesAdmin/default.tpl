{include file="@header.tpl"}

<table class="htmltable">
<col width="70">
<col>
<col width="100">
<col width="100">
<col>
<col width="70">
<tr>
	<th>Id</th>
	<th>Name</th>
	<th>Number</th>
	<th>Inc. Date</th>
	<th>Customer</th>
	<th>Action</th>
</tr>
{foreach from=$certificates key="id" item="certificate"}
<tr>
	<td>{$certificate.company_id}</td>
	<td>{$certificate.company_name}</td>
	<td>{$certificate.company_number}</td>
	<td>{$certificate.incorporation_date|date_format:"%d/%m/%Y"}</td>
	<td>{$certificate.firstName} {$certificate.lastName}</td>
	<td class="center"><a href="{$this->router->link("view", "certificate_id=$id")}" target="_blank">view</td>
</tr>
{/foreach}
</table>

{* FORM *}
<fieldset>
	<legend>Action</legend>
	{$form->getBegin() nofilter}
	<table>
	<col width="150">
	<col width="150">
	<col width="150">
        <col width="150">
	<tr>
		<td>{$form->getControl("printCertificates") nofilter}</td>
		{*<td>{$form->getControl("printCovers") nofilter}</td>*}
                <td>{$form->getControl("printShareCerts") nofilter}</td>
		<td>{$form->getControl("markAllPrinted") nofilter}</td>
                <td>{$form->getControl("printCoversClean") nofilter}</td>
	</tr>
	</table>
	{$form->getEnd() nofilter}
</fieldset>

{include file="@footer.tpl"}