{include file="@header.tpl"}

<table class="htmltable">
<col width="70">
<col>
<col width="100">
<col width="100">
<col>
<col width="70">
<tr>
	<th>Id</th>
	<th>Name</th>
	<th>Number</th>
	<th>Inc. Date</th>
	<th>Customer</th>
	<th>Action</th>
</tr>
{foreach from=$bronzecoverletters key="id" item="bronzecoverletter"}
<tr>
	<td>{$bronzecoverletter.company_id}</td>
	<td>{$bronzecoverletter.company_name}</td>
	<td>{$bronzecoverletter.company_number}</td>
	<td>{$bronzecoverletter.incorporation_date|date_format:"%d/%m/%Y"}</td>
	<td>{$bronzecoverletter.firstName} {$bronzecoverletter.lastName}</td>
	<td class="center"><a href="{$this->router->link("view", "bronzecoverletter_id=$id")}" target="_blank">view</td>
</tr>
{/foreach}
</table>

{* FORM *}
<fieldset>
	<legend>Action</legend>
	{$form->getBegin() nofilter}
	<table>
	<col width="150">
	<col width="150">
	<col width="150">
	<tr>
		<td>{$form->getControl("printCovers") nofilter}</td>
		<td>{$form->getControl("markAllPrinted") nofilter}</td>
	</tr>
	</table>
	{$form->getEnd() nofilter}
</fieldset>

{include file="@footer.tpl"}