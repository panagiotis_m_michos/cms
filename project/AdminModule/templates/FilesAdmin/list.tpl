{include file="@header.tpl"}

<p>[<a href="{$this->router->link("insert")}">create new]</a></p>

{if !empty($files)}
	<table class="htmltable">
	<col width="50">
	<col>
	<col width="50">
	<col width="100">
	<col width="60">
	<col width="60">
	<col width="100">
	<col width="60">
	<col width="60">
	<tr>
		<th>Id</th>
		<th>Name</th>
		<th>Ext</th>
		<th>Size</th>
		<th>Active</th>
		<th>Ord</th>
		<th>Created</th>
		<th colspan="2">Action</th>
	</tr>
	{foreach from=$files key="fileId" item="file"}
	<tr>
		<td class="right">{$file->getId()}</td>
		<td><a href="{$file->getFilePath(true)}" target="_blank">{$file->name}</a></td>
		<td class="center">{$file->ext}</td>
		<td class="right">{$file->getSize(true)}</td>
		<td class="center">{$file->isActive}</td>
		<td class="right">{$file->ord}</td>
		<td class="center">{$file->dtc|date_format:"%d/%m/%Y"}</td>
		<td class="center"><a href="{$this->router->link('edit', "file_id=$fileId")}">edit</a></td>
		<td class="center"><a href="{$this->router->link('delete', "file_id=$fileId")}" onclick="return confirm('Are you sure?');">delete</a></td>
	</tr>
	{/foreach}
	</table>

	{* PAGINATOR *}
	{$paginator nofilter}
{else}
	<p>No files</p>
{/if}


{include file="@footer.tpl"}