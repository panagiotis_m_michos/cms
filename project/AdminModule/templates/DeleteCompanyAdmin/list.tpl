{include file="@header.tpl"}

{*
<ul class="submenu">
	<li><a href="{$this->router->link(null,'csv=1')}">Export to CSV</a></li>
</ul>
*}


{$form nofilter}


<table class="htmltable">
	<col width="280"/>
	<col width="80"/>
	<col />
        <col width="90" />
        <col width="60" />
	<tr>
        <th>Company Name</th>
        <th>Number</th>
            
            <th>Reason</th>
            <th>Date</th>
            <th>Action</th>
        </tr>
        {if !$deletedCompanies}
           <tr>
                <td align="center" colspan="4">No data</td>
           </tr>
        {else}
            {foreach from=$deletedCompanies key="id" item="company"}
            <tr>
                    <td>{$company->companyName}</td>
                    <td>{$company->companyNumber}</td>
                    
                    <td>{$company->rejectMessage}</td>
                    <td class="center">{$company->deletedDate|date_format:"%d/%m/%Y"}</td>
                    <td class="center">
                        {assign var="companyId" value=$company->companyId}
                        <a href="{$this->router->link("CompaniesAdminControler::COMPANIES_PAGE")}view/?company_id={$companyId}">View</a>
                    </td>
            </tr>
            {/foreach}
        {/if}
    </table>


{$paginator nofilter}

{include file="@footer.tpl"}