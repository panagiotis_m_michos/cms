{include file="@header.tpl"}

{$form nofilter}

{literal}
    <script type="text/javascript">
        
        function toggleBlockedReason(status) {
            var value = status.val();
            if (value == 3) {
                $('#blockedReason').closest('tr').fadeIn();
            } else {
                $('#blockedReason').closest('tr').hide();
            }
        }
        
        $(function(){
            $('#blockedReason').closest('tr').hide();
            toggleBlockedReason($('#statusId'));
            $('#statusId').change(function(){
                toggleBlockedReason($(this));
            })
        });

    </script>
{/literal}

{include file="@footer.tpl"}
