{include file="@header.tpl"}

<ul class="submenu">
    <li><a href="{$this->router->link("editDetails", "customer_id=$customerId")}">Edit details</a></li>
    <li><a href="{$this->router->link("addCredit", "customer_id=$customerId")}">Add Credit</a></li>
    <li><a href="{$this->router->link("addCompany", "customer_id=$customerId")}">Add Company</a></li>
    <li><a href="{$this->router->link("importCompany", "customer_id=$customerId")}">Import company</a></li>
    <li><a href="{$this->router->link("exportCsv", "customer_id=$customerId")}">Export Companies as csv</a></li>
    <li><a href="{$this->router->link("syncAll", "customer_id=$customerId")}">Sync All Companies</a></li>
    <li><a href="{$this->router->link("syncMCAll", "customer_id=$customerId")}">Sync Monitored</a></li>
    <li><a href="{$this->router->link("EmailLogsAdminControler::EMAIL_LOG_PAGE", "customer_id=$customerId")}">Email History</a></li>
    <li><a href="{$this->router->link("companyFormation", "customer_id=$customerId")}">Bulk Formation</a></li>
</ul>

{literal}
    <script type="text/javascript">

        function switchTabs(name) {
            $('.tab').each(function(){
                $(this).removeClass('current');
            });
            $('.tab-pane').each(function(){
                $(this).hide();
            });
            // show blocks
            $('#' + name).fadeIn();
            $('#tab_' + name).addClass('current');
        }

        $(function(){
            var current = "{/literal}{$tab}{literal}";
            if (current) {
                switchTabs('companies');
            }

            var hash = window.location.hash.slice(1);
            if (hash) {
                switchTabs(hash);
            }

            //switchTabs('notes');

            $('.tab').click(function(e){
                e.preventDefault();
                var name = $(this).data('tab');
                switchTabs(name);
            });
        });



    </script>
{/literal}

<ul id="navlist">
    <li><a href="#" id="tab_details" class="tab current" data-tab="details">Customer Details</a></li>
    <li><a href="#" id="tab_companies" class="tab" data-tab="companies">Companies</a></li>
    <li><a href="#" id="tab_orders" class="tab" data-tab="orders">Orders</a></li>
    <li><a href="#" id="tab_notes" class="tab" data-tab="notes">Notes</a></li>
    <li><a href="#" id="tab_cashback" class="tab" data-tab="cashback">Cash Back</a></li>
</ul>

{* DETAILS *}
<div id="details" class="tab-pane">
    <h2>{$customer->firstName} {$customer->lastName}</h2>
    <table class="htmltable">
    <col width="200">
    <tr>
        <th>Status:</th>
        <td>
            {if $customer->status == Active}
                {$customer->status}
            {else}
                <span style="color: red; font-weight: bold">{$customer->status}</span>
            {/if}
        </td>
    </tr>
    <tr>
        <th>Email:</th>
        <td>{$customer->email}</td>
    </tr>
    <tr>
        <th>Phone:</th>
        <td>{$customer->phone}</td>
    </tr>
    </table>
    <br />
    <table class="htmltable">
    <col width="200">
    <tr>
        <th>Title:</th>
        <td>{if isset($customer->title)}{$customer->title}{/if}</td>
    </tr>
    <tr>
        <th>First name:</th>
        <td>{$customer->firstName}</td>
    </tr>
    <tr>
        <th>Last name:</th>
        <td>{$customer->lastName}</td>
    </tr>
    <tr>
        <th>Company name:</th>
        <td>{$customer->companyName}</td>
    </tr>
    </table>
    <br />
    <table class="htmltable">
    <col width="200">
    <tr>
        <th>Account Type:</th>
        <td>{$customer->role}</td>
    </tr>
    <tr>
        <th>Available Credit:</th>
        <td>{$customer->credit|string_format:"%.2f"}</td>
    </tr>
    <!--<tr>
        <th>Subscribed to Newsletter:</th>
        <td>{if $customer->isSubscribed}Yes{else}No{/if}</td>
    </tr>-->
    </table>
    <br />
    <table class="htmltable">
    <col width="200">
    <tr>
        <th>Address 1:</th>
        <td>{$customer->address1}</td>
    </tr>
    <tr>
        <th>Address 2:</th>
        <td>{$customer->address2}</td>
    </tr>
    <tr>
        <th>Address 3:</th>
        <td>{$customer->address3}</td>
    </tr>
    <tr>
        <th>County:</th>
        <td>{$customer->county}</td>
    </tr>
    <tr>
        <th>City:</th>
        <td>{$customer->city}</td>
    </tr>
    <tr>
        <th>Postcode:</th>
        <td>{$customer->postcode}</td>
    </tr>
    <tr>
        <th>Country:</th>
        <td>{$customer->country}</td>
    </tr>
    </table>
    <br />
    <table class="htmltable">
        <col width="200">
        <tr>
            <th>Proof of ID:</th>
            <td>
                {if $customerEntity->isIdCheckCompleted()}
                    Validated
                {elseif $customerEntity->isIdCheckRequired()}
                    <div class="fleft">Not Provided</div>
                    <div class="fleft ml15">
                        {if !empty($idCheckFiles)}
                            <form id="form-complete-id-check" method="post" action="{$this->router->link('completeIdCheck', "customer_id={$customerEntity->getId()}")}">
                                <input type="submit" value="Mark as validated" class="btn" />
                            </form>
                        {/if}
                    </div>
                {else}
                    Not Required
                {/if}
            </td>
        </tr>
        <tr>
            <th>Last validated on:</th>
            <td>{$customerEntity->getDtIdValidated()|datetime}</td>
        </tr>
        <tr>
            <th>Points:</th>
            <td>
                {if $idCheck->getPoints() !== NULL}
                    {$idCheck->getPointsMessage()}. Points: {$idCheck->getPoints()} <a style="margin-left:30px" id="show-object" href="javascript:;">More Info</a>
                    <div id="more-info-object" style="display: none;">{$idCheck->getResponse()|pr}</div>
                {/if}
            </td>
        </tr>
        <tr>
            <th>Uploaded documents:</th>
            <td>
                {if !empty($idCheckFiles)}
                    {foreach $idCheckFiles as $file}
                        <div class="clear">
                            <div class="fleft">
                                <a href="{$this->router->link('downloadDocument', "customer_id={$customerEntity->getId()}", "fileKey={$file->getKey()}")}">{$file->getName()}</a>
                            </div>
                            {if !$customerEntity->isIdCheckCompleted()}
                                <div class="fleft ml15">
                                    <form method="post" action="{$this->router->link('removeDocument', "customer_id={$customerEntity->getId()}", "fileKey={$file->getKey()}")}">
                                        <input class="confirm-remove btn" type="submit" value="Remove Document" />
                                    </form>
                                </div>
                            {/if}
                        </div>
                    {/foreach}
                {/if}
            </td>
        </tr>
        {if !$customerEntity->isIdCheckCompleted()}
            <tr>
                <th>Upload new document:</th>
                {$formHelper->setTheme($documentForm, 'cms.html.twig')}
                <td>{$formHelper->form($documentForm) nofilter}</td>
            </tr>
        {/if}
    </table>
</div>

<script type="text/javascript">
    $('#show-object').click(function(e){
        e.preventDefault();
        $('#more-info-object').dialog({
            width: "75%"
        });
    });
    $('#form-complete-id-check').on('submit', function() {
        return confirm("Are you sure you want to validate customer's documents ?")
    });
    $('.confirm-remove').on('click', function() {
        return confirm("Are you sure you want to remove this document ?");
    })
</script>

{* COMPANIES *}
<div id="companies" class="tab-pane" style="display: none;">
    <h2>Companies</h2>
    {if !empty($companies)}
        {$companies nofilter}
    {/if}
</div>

{* ORDERS *}
<div id="orders" class="tab-pane" style="display: none;">
    <h2>Order History</h2>
    {if !empty($orders)}
        <table class="htmltable orders">
            <col width="100">
            <col width="70">
            <col>
            <col>
            <col>
            <col>
            <col width="80">
            <tr>
                <th>Id</th>
                <th>Status</th>
                <th>Date</th>
                <th>Subtotal</th>
                <th>Vat</th>
                <th>Total</th>
                <th>Action</th>
            </tr>
            {foreach from=$orders key="key" item="order"}
                <tr>
                    <td>{$order->getId()}</td>
                    <td class="center">{$order->status}</td>
                    <td class="center">{$order->dtc|date_format:"%d/%m/%Y"}</td>
                    <td class="right">{$order->subtotal}</td>
                    <td class="right">{$order->vat}</td>
                    <td class="right">{$order->total}</td>
                    <td class="center"><a href="{$this->router->link("OrdersAdminControler::ORDERS_PAGE view","order_id=$key")}">View</a></td>
                </tr>
            {/foreach}
        </table>
    {else}
        <p>No orders history.</p>
    {/if}
</div>

{* notes *}
<div id="notes" class="tab-pane" style="display: none;">
    {$noteForm nofilter}

    <hr style="margin: 15px 0 15px 0;">

    {foreach $notes as $note}
        <div style="margin-bottom: 20px;">
            {if $note->hasBlocked()}
                <p style="margin-bottom: 3px"><span style="color:red">Blocked Reason: </span><span>{$note->getBlockedText()}</span></p>
            {else}
                <p style="margin-bottom: 3px">{$note->note}</p>
            {/if}
            <span style="color: darkgrey; font-size: 11px;">Posted by {$note->user->fullName} on {$note->dtc|datetime:'d/m/Y H:i'}</span>
        </div>
    {/foreach}
</div>

{* Cash Back *}
<div id="cashback" class="tab-pane" style="display: none;">
    {if $customerEntity->hasCashbackTypeBank()}
        <h4>Bank details</h4>
        <table class="htmltable">
            <colgroup><col width="150">
            </colgroup><tbody><tr>
                <th>Sort Code: </th>
                <td>{$customerEntity->getSortCode()}</td>
            </tr>
            <tr>
                <th>Account Number: </th>
                <td>{$customerEntity->getAccountNumber()}</td>
            </tr>
            </tbody>
        </table>
    {elseif $customerEntity->hasCashbackTypeCredits()}
        <strong>Credit on CMS account:</strong> {$customerEntity->getEmail()}<br/>
    {else !$customerEntity->hasBankDetails()}
        <h4>Bank details</h4>
        No cash back preference set. Direct the customer to enter their details
        <a href="{$this->router->frontLink("CashbackControler::CASHBACK_PAGE")}">here</a>
        <br/>
    {/if}
    <br/>
    <h4>All Cash Backs</h4>
    {if !$customerHasCashbacks}
        No cash back records were found. This means we are still waiting to receive confirmation from the bank.<br/>
        Please check again later.
    {else}
        {$cashbacks nofilter}
    {/if}

</div>

{include file="@footer.tpl"}
