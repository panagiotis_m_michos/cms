{include file="@header.tpl"}

<h2>Bulk company creation</h2>
<fieldset>
    <legend>Info</legend>
    <p>We can upload a spreadsheet containing many companies for a customer if they want to form in bulk.</p>

    <a href="{url route="bulk_csv_formation.download_template_csv"}">Bulk formation template.csv</a><br>
    <a href="{url route="bulk_csv_formation.download_example_csv"}">Bulk formation example.csv</a><br>


    <p>For more information visit: <a href="http://support.companiesmadesimple.com/article/811-how-to-form-companies-in-bulk-for-a-customer">How to form companies in bulk for a customer</a></p>
</fieldset>
{$bulkFormationForm nofilter}

{if isset($bulkSubmitForm)}
    <h2>Bulk company submission to Companies House</h2>
    {$bulkSubmitForm nofilter}
{/if}

{include file="@footer.tpl"}
