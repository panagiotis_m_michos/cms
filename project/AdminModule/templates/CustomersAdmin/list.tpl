{include file="@header.tpl"}

{$form nofilter}

<h2>Customers</h2>

{if !empty($customers)}
	<table class="htmltable">
	<col width="80">
	<col>
	<col>
	<col width="80">
	<tr>
		<th class="right">Id</th>
		<th>Contact name</th>
		<th>Email</th>
		<th class="center">Action</th>
	</tr>
	{foreach from=$customers key="id" item="customer"}
	<tr>
		<td class="right">{$id}</td>
		<td>{$customer->firstName} {$customer->lastName}</td>
		<td>{$customer->email}</td>
		<td class="center"><a href="{$this->router->link("view","customer_id=$id")}">view</a></td>
	</tr>
	{/foreach}
	</table>
	
	{$paginator nofilter}
	
{else}
	<p>No customers found.</p>
{/if}



{include file="@footer.tpl"}