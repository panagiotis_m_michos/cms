{include file="@header.tpl"}

{$form->getBegin() nofilter}
    {if $form->getErrors()|@count gt 0}
        <p class="ff_err_notice ff_red_err" style="width: 710px">Form has <b> {$form->getErrors()|@count}</b> error(s). See below for more details:</p>
    {/if} 
	 <fieldset id="fieldset_1">
			<legend>Card Details</legend>
			<table class="ff_table">
				<tbody>
					<tr>
						<th>{$form->getLabel('customer') nofilter}</th>
						<td>{$form->getControl('customer') nofilter}</td>
						<td><span class="redmsg">{$form->getError('customer')}</span></td>
					</tr>
                </tbody>
			</table>
		</fieldset>           
            <fieldset id="fieldset_1">
			<legend>Card Details</legend>
			<table class="ff_table">
				<tbody>
					<tr>
						<th>{$form->getLabel('cardholder') nofilter}</th>
						<td>{$form->getControl('cardholder') nofilter}</td>
						<td><span class="redmsg">{$form->getError('cardholder')}</span></td>
					</tr>
					<tr>
						<th>{$form->getLabel('cardType') nofilter}</th>
						<td>{$form->getControl('cardType') nofilter}</td>
						<td><span class="redmsg">{$form->getError('cardType')}</span></td>
					</tr>
					<tr>
						<th>{$form->getLabel('cardNumber') nofilter}</th>
						<td>{$form->getControl('cardNumber') nofilter}</td>
						<td><span class="redmsg">{$form->getError('cardNumber')}</span></td>
					</tr>
                    <noscript>
					<tr>
						<th>{$form->getLabel('issueNumber') nofilter}</th>
						<td>{$form->getControl('issueNumber') nofilter}</td>
						<td>The issue number MUST be entered EXACTLY as it appears on the card. e.g. some cards have Issue Number "4", others have "04".</td>
					</tr>
					<tr>
						<th>{$form->getLabel('validFrom') nofilter}</th>
						<td>{$form->getControl('validFrom') nofilter}</td>
						<td><span class="redmsg">{$form->getError('validFrom')}</span></td>
					</tr>
					</noscript>                       
					<tr class="issuered" style="display:none;">
						<th>{$form->getLabel('issueNumber') nofilter}</th>
						<td>
							{$form->getControl('issueNumber') nofilter}
							<div class="help-button2" style="float: right; margin: 0 15px 0 0;">
								<em>The issue number MUST be entered EXACTLY as it appears on the card. e.g. some cards have Issue Number "4", others have "04".</em>
							</div>
						</td>
						<td><span class="redmsg">{$form->getError('issueNumber')}</span></td>
					</tr>
					<tr class="issuered" style="display:none;">
						<th>{$form->getLabel('validFrom') nofilter}</th>
						<td>{$form->getControl('validFrom') nofilter}</td>
						<td><span class="redmsg">{$form->getError('validFrom')}</span></td>
					</tr>                
					<tr>
						<th>{$form->getLabel('expiryDate') nofilter}</th>
						<td>{$form->getControl('expiryDate') nofilter}</td>
						<td><span class="redmsg">{$form->getError('expiryDate')}</span></td>
					</tr>
					<tr>
						<th>{$form->getLabel('CV2') nofilter}</th>
						<td>
							{$form->getControl('CV2') nofilter}
						</td>
						<td><span class="redmsg">{$form->getError('CV2')}</span></td>
					</tr>
             				</tbody>
			</table>
		</fieldset>       
        <fieldset id="fieldset_3">
        <legend>Adress</legend>
         <table class="ff_table">
            <tbody>
                    <tr>
						<th>{$form->getLabel('firstname') nofilter}</th>
						<td>{$form->getControl('firstname') nofilter}</td>
						<td><span class="redmsg">{$form->getError('firstname')}</span></td>
					</tr>
                    <tr>
						<th>{$form->getLabel('surname') nofilter}</th>
						<td>{$form->getControl('surname') nofilter}</td>
						<td><span class="redmsg">{$form->getError('surname')}</span></td>
					</tr>
					<tr>
						<th>{$form->getLabel('address1') nofilter}</th>
						<td>{$form->getControl('address1') nofilter}</td>
						<td><span class="redmsg">{$form->getError('address1')}</span></td>
					</tr>
					<tr>
						<th>{$form->getLabel('address2') nofilter}</th>
						<td>{$form->getControl('address2') nofilter}</td>
						<td><span class="redmsg">{$form->getError('address2')}</span></td>
					</tr>

					<tr>
						<th>{$form->getLabel('town') nofilter}</th>
						<td>{$form->getControl('town') nofilter}</td>
						<td><span class="redmsg">{$form->getError('town')}</span></td>
					</tr>
					<tr>
						<th>{$form->getLabel('postcode') nofilter}</th>
						<td>{$form->getControl('postcode') nofilter}</td>
						<td><span class="redmsg">{$form->getError('postcode')}</span></td>
					</tr>                
					<tr>
						<th>{$form->getLabel('country') nofilter}</th>
						<td>{$form->getControl('country') nofilter}</td>
						<td><span class="redmsg">{$form->getError('country')}</span></td>
					</tr>
                    <noscript>
					<tr>
						<th>{$form->getLabel('billingState') nofilter}</th>
						<td>{$form->getControl('billingState') nofilter}</td>
						<td><span class="redmsg">{$form->getError('billingState')}</span></td>
					</tr>
					</noscript>
					<tr class="state" style="display:none;">
						<th>{$form->getLabel('billingState') nofilter}</th>
						<td>{$form->getControl('billingState') nofilter}</td>
						<td><span class="redmsg">{$form->getError('billingState')}</span></td>
					</tr>
				</tbody>
			</table>
		</fieldset>

    <fieldset id="fieldset_3">
        <legend>Action</legend>
        <table class="ff_table">
            <tbody>
                <tr>
                    <th>&nbsp;</th>
                    <td>{$form->getControl('submit') nofilter}</td>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </fieldset>
	
    

    {$form->getEnd() nofilter}	
    {literal}
        <script language="JavaScript" type="text/javascript">

        $(document).ready(function() {
            $('#country option:selected').each(function () {		
                if($(this).text() == "United States")
                {				
                    $('.state').show();
                }	    
                else
                {	        
                    $('.state').hide();
                }
            });

            $('#cardType option:selected').each(function () {		
                if(($(this).text() == "Maestro") || ($(this).text() == "Solo"))
                {				
                    $('.issuered').show();
                }
                else
                {	        
                    $('.issuered').hide();
                }
            });

            $('#cardType').change(function () {	
                $('#cardType option:selected').each(function () {		
                    if(($(this).text() == "Maestro") || ($(this).text() == "Solo"))
                    {				
                        $('.issuered').show();
                    }
                    else
                    {	        
                        $('.issuered').hide();
                    }
                 });
            });


            $('#country').change(function () {	
                $('#country option:selected').each(function () {		
                    if($(this).text() == "United States")
                    {				
                        $('.state').show();
                    }
                    else
                    {	        
                        $('.state').hide();
                    }
                 });
            });

        });
    </script>	
    {/literal}
{include file="@footer.tpl"}