{include file="@header.tpl"}

<h2>Error Info:</h2>

<table class="htmltable">
<col width="150">
<tr>
	<th>Error id:</th>
	<td>#{$error->getId()}</td>
</tr>
<tr>
	<th class="left">Payment Type:</th>
	<td>{$error->paymentTypeId}</td>
</tr>
<tr>
	<th class="left">Error Type:</th>
	<td>{$error->errorTypeId}</td>
</tr>
<tr>
	<th class="left">VendorTXCode:</th>
	<td>{$error->vendorTXCode}</td>
</tr>
<tr>
	<th class="left">Date:</th>
	<td>{$error->dtc}</td>
</tr>
<tr>
	<th class="left">Message:</th>
	<td>{$error->errorMessage}</td>
</tr>

{if $error->auth3D} 
<tr>
	<th class="left">3DAuth:</th>
	<td>Yes</td>
</tr>
{/if}

</table>

<h2>Customer Info:</h2>
<table class="htmltable">
<col width="150">
    <tr>
	<th class="left">Email:</th>
	<td>{$error->email}</td>
    </tr>
    <tr>
        <th class="left">Name:</th>
        <td>{$error->name}</td>
    </tr>
    <tr>
        <th class="left">Address:</th>
        <td>{$error->address1} , {$error->address2} <br /> {$error->postcode} <br /> {$error->country}</td>
    </tr>
</table>

    
{if !empty($error->basketItems)}
    <h2>Basket Items:</h2>
    <table class="htmltable">
    <col width="550">
    {assign var='basketItems' value=$error->basketItems}
    {foreach from=$basketItems item=value}
        <tr>
            <td>{$value->ProductName}</td>
            <td>&pound;{$value->ProductPrice}</td>
        </tr>
    {/foreach}
    <tr>
        <td style="text-align: right;">Basket Price (width VAT):</td>
        <td>&pound;{$error->basketPrice}</td>
    </tr>
    </table>
{/if}


{include file="@footer.tpl"}