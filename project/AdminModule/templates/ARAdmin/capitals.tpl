{include file="@header.tpl"}
    <ul class="submenu">
        {* DISPLAY ONLY IF CUSTOMER HAS SERVICE *}
        {if isset($hasAnnualService)}
            <li><a href="{$this->router->link("missingDataEmail", "company_id=$companyId", "keepThis=true", "TB_iframe=true", "width=600", "height=400")}" class="thickbox">Send Missing Information Email</a></li>
            <li><a href="{$this->router->link("sync", "company_id=$companyId")}"><b>Sync</b></a></li>
        {/if}
        <li><a href="{$this->router->link('addCapital', "company_id=$companyId")}" class="current">Add New Capital</a></li>
    </ul>


{* SUBNAV *}
<ul id="navlist">
    <li><a href="{$this->router->link('companyDetails', "company_id=$companyId")}">Company Details</a></li>
    <li><a href="{$this->router->link('capitals', "company_id=$companyId")}" class="current">Capitals</a></li>
    <li><a href="{$this->router->link('shareholdings', "company_id=$companyId")}">Shareholdings</a></li>
    {if $showPscs}
        <li><a href="{$this->router->link('pscs', "company_id=$companyId")}">PSCs</a></li>
    {/if}
    <li><a href="{$this->router->link('summary', "company_id=$companyId")}">Summary</a></li>
</ul>

{$form->getBegin() nofilter}

{if $form->getErrors()|@count gt 0}
    <p class="ff_err_notice">Form has <b> {$form->getErrors()|@count}</b> error(s). See below for more details:</p>
    {/if}

{foreach from=$capitals item = "capital" key = "cKey"}
    {assign var="capitalCurrency" value=$capital->getShareCurrency()}
    {assign var="capitalId" value=$capital->getId()}
    {assign var="capitalNominalValue" value=$capital->getTotalAggregateNominalValue()}
    {assign var="capitalIssuedShares" value=$capital->getTotalNumberOfIssuedShares()}
    <fieldset id="fieldset_{$capitalId}" style="padding-top: 0px;">
    <legend>Capital</legend>
    <a href="{$this->router->link('removeCapital', "capital_id=$capitalId", "company_id=$companyId")}"
       style="float:right;">Remove Capital</a>
    <br>
    <a href="{$this->router->link('addCapitalShare', "capital_id=$capitalId", "company_id=$companyId")}"
       style="float:left; padding-top: 10px;"><b>Add Share Class to this Capital</b></a>
    <br>
    {foreach from=$capital->getShares() item = "share" }
        {assign var="key" value=$share->getId()}
        {assign var="shareClass" value=$share->getShareClass()}
        <fieldset id="fieldset_{$cKey}_{$key}" style="padding-top: 0px; margin-top: 30px;">
        <legend>Share Class</legend>
        <a href="{$this->router->link('removeCapitalShare', "share_id=$key", "company_id=$companyId")}" style="float:right">Remove Share Class</a>
        <table class="ff_table">
        <tbody>
        <tr>
            {assign var="share_currency" value='share_currency'|cat:"_"|cat:$key}
            <th>{$form->getLabel($share_currency) nofilter}</th>
            <td>{$form->getControl($share_currency) nofilter}
                {if $form->getError($share_currency)}
                    <span class="ff_control_err">{$form->getError($share_currency)}</span>
                {/if}
            </td>
            <td></td>
        </tr>
        <tr>
            {assign var="share_class" value='share_class'|cat:"_"|cat:$key}
            <th>{$form->getLabel($share_class) nofilter}</th>
            <td>{$form->getControl($share_class) nofilter}
                {if $form->getError($share_class)}
                    <span class="ff_control_err">{$form->getError($share_class)}</span>
                {/if}
                <div><i>(Share Class should be unique)</i></div>
            </td>
            <td></td>
        </tr>
        <tr>
            {assign var="prescribed_particulars" value='prescribed_particulars'|cat:"_"|cat:$key}
            <th>{$form->getLabel($prescribed_particulars) nofilter}</th>
            <td>{$form->getControl($prescribed_particulars) nofilter}
                {if $form->getError($prescribed_particulars)}
                    <span class="ff_control_err">{$form->getError($prescribed_particulars)}</span>
                {/if}
            </td>
            <td></td>
        </tr>
        <tr>
            {assign var="num_shares" value='num_shares'|cat:"_"|cat:$key}
            <th>{$form->getLabel($num_shares) nofilter}</th>
            <td>{$form->getControl($num_shares) nofilter}
                {if $form->getError($num_shares)}
                    <span class="ff_control_err">{$form->getError($num_shares)}</span>
                {/if}
            </td>
            <td></td>
        </tr>
        <tr>
            {assign var="share_value" value='share_value'|cat:"_"|cat:$key}
            <th>{$form->getLabel($share_value) nofilter}</th>
            <td>{$form->getControl($share_value) nofilter}
                {if $form->getError($share_value)}
                    <span class="ff_control_err">{$form->getError($share_value)}</span>
                {/if}
            </td>
            <td></td>
        </tr>
        <tr>
            {assign var="paid" value='paid'|cat:"_"|cat:$key}
            <th>{$form->getLabel($paid) nofilter}</th>
            <td>{$form->getControl($paid) nofilter}
                {if $form->getError($paid)}
                    <span class="ff_control_err">{$form->getError($paid)}</span>
                {/if}
            </td>
            <td></td>
        </tr>
        {assign var="partialy_paid" value='partialy_paid'|cat:"_"|cat:$key}
        {assign var="partialyPaid" value=$share->getPartialyPaid()}
        {*{if $partialyPaid }*}
        <tr style="display: none;">
            <th></th>
            <td>{$form->getControl($partialy_paid) nofilter}
                {if $form->getError($partialy_paid)}
                    <span class="ff_control_err">{$form->getError($partialy_paid)}</span>
                {/if}
            </td>
            <td></td>
        </tr>
       {* {/if}*}
        </tbody>
        </table>
        </fieldset>
    {/foreach}
    </fieldset>
{/foreach}
<fieldset id="fieldset_submit">
	<legend>Action</legend>
    <table class="ff_table">
    <tbody>
         <tr>
            <th></th>
            <td>{$form->getControl('submit') nofilter}</td>
            <td></td>
        </tr>
    </tbody>
    </table>
</fieldset>
{$form->getEnd() nofilter}

{literal}
<script type="text/javascript">
<!--

// hide all partialy paid
$('input[id^="partialy_paid_"]').parents('tr').hide();

// show partialy paid based on checkbox
$('input[id^="paid"]:checked').each(function() {
	toggle(this);
});

// click trigger on checkbox
$(document).ready(function() {
	$('input[id^="paid"]').click(function() {
		toggle(this);
	});
});

/**
 * Will show and diplay partialy paid field
 *
 * @param jQuery _this
 * @return void
 */
function toggle(_this) {

	var id = $(_this).attr('name');
	id = '#partialy_paid_' + id.split('_')[1];
	$row = $(id).parents('tr')

	if ($(_this).val() == 'partialy_paid') {
		$row.show();
	} else {
		$row.hide();
	}
}

//-->
</script>
{/literal}

{include file="@footer.tpl"}
