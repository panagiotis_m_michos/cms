{include file="@header.tpl"}

<script type="text/javascript">
    var addresses = {$jsPrefillAdresses nofilter};
    var officers = {$jsPrefillOfficers nofilter};
</script>

{literal}
    <script>
        $(function () {
            $("#prefillAddress").change(function () {
                var value = $(this).val();
                var address = addresses[value];
                for (var name in address) {
                    $('#' + name).val(address[name]);
                }
            });

            $("#prefillOfficers").change(function () {
                var value = $(this).val();
                var officer = officers[value];
                for (var name in officer) {
                    $('#' + name).val(officer[name]);
                }
            });

            toogleServiceAddress();

            $("#serviceAddress").click(function () {
                toogleServiceAddress();
            });

            function toogleServiceAddress() {
                var disabled = !$("#serviceAddress").is(":checked");
                $("input[id^='service_'], select[id^='service_']").each(function () {
                    $(this).attr("disabled", disabled);
                });
            }

            function setCheckedState($container) {
                $container.find(':radio').each(function (i, e) {
                    $(e).data('checked', e.checked);
                });
            }

            var $container = $('.nature-of-control-container');
            setCheckedState($container);

            $($container).on('click', ':radio', function () {
                var newState = !$(this).data('checked');
                setCheckedState($container);

                $(this).prop('checked', newState);
                $(this).data('checked', newState);
            });
        });
    </script>
{/literal}

{$form->getBegin() nofilter}
{if $form->getErrors()|@count gt 0}
    <p class="ff_err_notice ff_red_err" style="width: 940px">Form has <b> {$form->getErrors()|@count}</b> error(s). See below for more details:</p>
{/if}

<fieldset style="clear: both;">
    <legend>Prefill</legend>
    <table class="ff_table">
        <tbody>
        <tr>
            <th>{$form->getLabel('prefillOfficers') nofilter}</th>
            <td>{$form->getControl('prefillOfficers') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('prefillOfficers')}</span></td>
        </tr>
        </tbody>
    </table>
</fieldset>

<fieldset>
    <legend>Person</legend>
    <table class="ff_table">
        <tbody>
        <tr>
            <th>{$form->getLabel('title') nofilter}</th>
            <td>{$form->getControl('title') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('title')}</span></td>
        </tr>
        <tr>
            <th>{$form->getLabel('forename') nofilter}</th>
            <td>{$form->getControl('forename') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('forename')}</span></td>
        </tr>
        <tr>
            <th>{$form->getLabel('middle_name') nofilter}</th>
            <td>{$form->getControl('middle_name') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('middle_name')}</span></td>
        </tr>
        <tr>
            <th>{$form->getLabel('surname') nofilter}</th>
            <td>{$form->getControl('surname') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('surname')}</span></td>
        </tr>
        <tr>
            <th>{$form->getLabel('dob') nofilter}</th>
            <td>{$form->getControl('dob') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('dob')}</span></td>
        </tr>
        <tr>
            <th>{$form->getLabel('nationality') nofilter}</th>
            <td>{$form->getControl('nationality') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('nationality')}</span></td>
        </tr>
        <tr>
            <th>{$form->getLabel('country_of_residence') nofilter}</th>
            <td>{$form->getControl('country_of_residence') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('country_of_residence')}</span></td>
        </tr>

        </tbody>
    </table>
</fieldset>

<fieldset id="service_address_prefill">
    <legend>Prefill</legend>
    <table class="ff_table">
        <tbody>
        <tr>
            <th>{$form->getLabel('prefillAddress') nofilter}</th>
            <td>{$form->getControl('prefillAddress') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('prefillAddress')}</span></td>
        </tr>
        </tbody>
    </table>
</fieldset>

<fieldset id="service_address">
    <legend>Service Address</legend>
    <table class="ff_table">
        <tbody>
        <tr>
            <th>{$form->getLabel('premise') nofilter}</th>
            <td>{$form->getControl('premise') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('premise')}</span></td>
        </tr>
        <tr>
            <th>{$form->getLabel('street') nofilter}</th>
            <td>{$form->getControl('street') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('street')}</span></td>
        </tr>
        <tr>
            <th>{$form->getLabel('thoroughfare') nofilter}</th>
            <td>{$form->getControl('thoroughfare') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('thoroughfare')}</span></td>
        </tr>
        <tr>
            <th>{$form->getLabel('post_town') nofilter}</th>
            <td>{$form->getControl('post_town') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('post_town')}</span></td>
        </tr>
        <tr>
            <th>{$form->getLabel('county') nofilter}</th>
            <td>{$form->getControl('county') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('county')}</span></td>
        </tr>
        <tr>
            <th>{$form->getLabel('postcode') nofilter}</th>
            <td>{$form->getControl('postcode') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('postcode') nofilter}</span></td>
        </tr>
        <tr>
            <th>{$form->getLabel('country') nofilter}</th>
            <td>{$form->getControl('country') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('country')}</span></td>
        </tr>
        </tbody>
    </table>
</fieldset>

<fieldset>
    <legend>Residential Address</legend>
    <table class="ff_table">
        <tbody>
        <tr>
            <th colspan="4" style="text-align: left; width: 724px;">The residential address must be the address where you live.</th>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <th>{$form->getLabel('residential_premise') nofilter}</th>
            <td>{$form->getControl('residential_premise') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('residential_premise')}</span></td>
        </tr>
        <tr>
            <th>{$form->getLabel('residential_street') nofilter}</th>
            <td>{$form->getControl('residential_street') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('residential_street')}</span></td>
        </tr>
        <tr>
            <th>{$form->getLabel('residential_thoroughfare') nofilter}</th>
            <td>{$form->getControl('residential_thoroughfare') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('residential_thoroughfare')}</span></td>
        </tr>
        <tr>
            <th>{$form->getLabel('residential_post_town') nofilter}</th>
            <td>{$form->getControl('residential_post_town') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('residential_post_town')}</span></td>
        </tr>
        <tr>
            <th>{$form->getLabel('residential_county') nofilter}</th>
            <td>{$form->getControl('residential_county') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('residential_county')}</span></td>
        </tr>
        <tr>
            <th>{$form->getLabel('residential_postcode') nofilter}</th>
            <td>{$form->getControl('residential_postcode') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('residential_postcode')}</span></td>
        </tr>
        <tr>
            <th>{$form->getLabel('residential_country') nofilter}</th>
            <td>{$form->getControl('residential_country') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('residential_country')}</span></td>
        </tr>
        </tbody>
    </table>
</fieldset>

<fieldset class="nature-of-control-container">
    <legend>Nature of Control:</legend>
    {$natureOfControlsDescription}
    <span class="ff_control_err">{$form->getError('significant_influence_or_control')}</span>

    {foreach $natureOfControls as $radioName => $natureOfControl}
        <fieldset>
            <legend>{$natureOfControl['title']}</legend>
            <table class="ff_table" style="width: 100%">
                <tbody>
                {foreach $natureOfControl['groups'] as $group}
                    {if isset($group['title'])}
                        <tr>
                            <td><b>{$group['title']}</b></td>
                        </tr>
                    {/if}
                    {if isset($group['description'])}
                        <tr>
                            <td><i>{$group['description']}</i></td>
                        </tr>
                    {/if}
                    {foreach $group['options'] as $optionKey => $option}
                        <tr>
                            <td>
                                {$form[$radioName]->getControl($optionKey) nofilter}</td>
                            </td>
                        </tr>
                    {/foreach}
                {/foreach}
                </tbody>
            </table>
        </fieldset>
    {/foreach}
</fieldset>

<fieldset id="fieldset_2">
    <legend>Action</legend>
    {$form->getControl('continue') nofilter}
</fieldset>

{$form->getEnd() nofilter}

{include file="@footer.tpl"}
