{include file="@header.tpl"}

{* DISPLAY ONLY IF CUSTOMER HAS SERVICE *}
	{if isset($hasAnnualService)}
	<ul class="submenu">
		<li><a href="{$this->router->link("missingDataEmail", "company_id=$companyId", "keepThis=true", "TB_iframe=true", "width=600", "height=400")}" class="thickbox">Send Missing Information Email</a></li>
        <li><a href="{$this->router->link("sync", "company_id=$companyId")}"><b>Sync</b></a></li>
	</ul>
{/if}


{* SUBNAV *}
<ul id="navlist">
	<li><a href="{$this->router->link('companyDetails', "company_id=$companyId")}">Company Details</a></li>
	<li><a href="{$this->router->link('capitals', "company_id=$companyId")}" class="current">Capitals</a></li>
    {assign var="capitalId" value=$capital->getId()}
    <li><a href="{$this->router->link('AddCapitalShare', "capital_id=$capitalId", "company_id=$companyId")}" class="current">Add Capital Share</a></li>
	<li><a href="{$this->router->link('shareholdings', "company_id=$companyId")}">Shareholdings</a></li>
	<li><a href="{$this->router->link('summary', "company_id=$companyId")}">Summary</a></li>
</ul>

{$form nofilter}

{literal}
<script type="text/javascript">
<!--

// hide all partialy paid
$('input[id^="partialy_paid"]').parents('tr').hide();
	
// show partialy paid based on checkbox
$('input[id^="paid"]:checked').each(function() {
	toggle(this);
});

// click trigger on checkbox
$(document).ready(function() {
	$('input[id^="paid"]').click(function() {
		toggle(this);
	});
});

/**
 * Will show and diplay partialy paid field
 * 
 * @param jQuery _this
 * @return void
 */
function toggle(_this) {

	var id = $(_this).attr('name');
	id = '#partialy_paid';
	$row = $(id).parents('tr')

	if ($(_this).val() == 'partialy_paid') {
		$row.show();
	} else {
		$row.hide();
	}
}

//-->
</script>
{/literal}
	
{include file="@footer.tpl"}