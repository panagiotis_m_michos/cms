{include file="@header.tpl"}

	{literal}
	<style>
	<!--
	table.ff_table th {
		width: 150px;
	}
	-->
	</style>
	{/literal}

	{if isset($hasAnnualService)}
	<ul class="submenu">
		<li><a href="{$this->router->link("missingDataEmail", "company_id=$companyId", "keepThis=true", "TB_iframe=true", "width=600", "height=400")}" class="thickbox">Send Missing Information Email</a></li>
        <li><a href="{$this->router->link("sync", "company_id=$companyId")}"><b>Sync</b></a></li>
		<li><a href="{$this->router->link("serviceCompletedEmail", "company_id=$companyId")}">Send Completed Email</a></li>
        <li><a href="{$this->router->link("pscs", "company_id=$companyId")}">PSCs</a></li>
        <li><a style="float:right" href="{$this->router->link('summary',"company_id=$companyId", "pdf=1")}">Save CS Final Summary as a PDF</a></li>
	</ul>
    {else}
        <ul class="submenu">
            <li><a style="float:right" href="{$this->router->link('summary',"company_id=$companyId", "pdf=1")}">Save CS Final Summary as a PDF</a></li>
        </ul>
	{/if}

	{* SUBNAV *}
	<ul id="navlist">
		<li><a href="{$this->router->link('companyDetails', "company_id=$companyId")}">Company Details</a></li>
    {if $company->getType() != 'LLP'}
		<li><a href="{$this->router->link('capitals', "company_id=$companyId")}">Capitals</a></li>
		<li><a href="{$this->router->link('shareholdings', "company_id=$companyId")}">Shareholdings</a></li>
        <li><a href="{$this->router->link("pscs", "company_id=$companyId")}">PSCs</a></li>
    {/if}
		<li><a href="{$this->router->link('summary', "company_id=$companyId")}" class="current">Summary</a></li>
	</ul>

	{$form nofilter}

{literal}
<script type="text/javascript">
<!--

$(document).ready(function() {
        /* click trigger on input type */


        function parseDate(str) {
            return $.datepicker.parseDate('dd-mm-yy', str);
        }

        if ($('#trading0').is(':checked'))
            {
                $('input[name^="dtr5"]').parents('fieldset').hide();
            }

        //hide dtr and show trading  for date lover than 01/10/2011 plc
         if( parseDate($(".date").val()) < parseDate('01-10-2011') && $(".CompanyCategory").val() == 'PLC')
            {
                $('input[name^="dtr5"]').parents('fieldset').hide();
                $('input[name^="trading"]').parents('fieldset').show();

            }
         //hide dtr and trading  for date lover than 01/10/2011 ltd, byshares
         else if( parseDate($(".date").val()) < parseDate('01-10-2011') && $(".CompanyCategory").val() != 'PLC')
            {
                $('input[name^="dtr5"]').parents('fieldset').hide();
                $('input[name^="trading"]').parents('fieldset').hide();

            }

          //show trading and dtr5 if  date biger than 01/10/2011 any company type
         else  if( parseDate($(".date").val()) >= parseDate('01-10-2011') && $(".CompanyCategory").val() != 'BYGUAR')
            {
                    $('input[name^="trading"]').parents('fieldset').show();
                    if ($('#trading1').is(':checked'))
                        {
                            $('input[name^="dtr5"]').parents('fieldset').show();
                        }


            }

          else if( parseDate($(".date").val()) >= parseDate('01-10-2011') && $(".CompanyCategory").val() == 'BYGUAR')
            {
                $('input[name^="dtr5"]').parents('fieldset').hide();
                $('input[name^="trading"]').parents('fieldset').hide();
            }

        // incase changing date
        $(".date").change (function() {
             //hide dtr and trading  for date lover than 01/10/2011 ltd, byshares
            if( parseDate($(".date").val()) < parseDate('01-10-2011') && $(".CompanyCategory").val() == 'PLC')
            {
                $('input[name^="dtr5"]').parents('fieldset').hide();
                $('input[name^="trading"]').parents('fieldset').show();

            }

            //hide dtr and trading  for date lover than 01/10/2011 ltd, byshares
            else if(parseDate($(".date").val()) < parseDate('01-10-2011') && $(".CompanyCategory").val() != 'PLC'  )
            {
                $('input[name^="dtr5"]').parents('fieldset').hide();
                $('input[name^="trading"]').parents('fieldset').hide();

            }

            //show dtr5 and trading for date biger than 01/10/2011 and any company
            else  if( parseDate($(".date").val()) >= parseDate('01-10-2011') && $(".CompanyCategory").val() != 'BYGUAR')
            {

                    $('input[name^="trading"]').parents('fieldset').show();
                    if ($('#trading1').is(':checked'))
                        {
                            $('input[name^="dtr5"]').parents('fieldset').show();
                        }
                    }
            else if( parseDate($(".date").val()) >= parseDate('01-10-2011') && $(".CompanyCategory").val() == 'BYGUAR')
            {
                $('input[name^="dtr5"]').parents('fieldset').hide();
                $('input[name^="trading"]').parents('fieldset').hide();
            }

        });



        $('input[name^="trading"]').click(function()
        {
            if ($('#trading1').is(':checked') && parseDate($(".date").val()) >= parseDate('01-10-2011'))
            {
                $('input[name^="dtr5"]').parents('fieldset').show();
            }
            // display description
            else if ($('#trading0').is(':checked'))
            {
               $('input[name^="dtr5"]').val(0).parents('fieldset').hide()
            }
        });

    $('.error').attr('style','width:730px;margin-bottom: 20px;padding-top:0;');
});
//-->
</script>
{/literal}

{include file="@footer.tpl"}
