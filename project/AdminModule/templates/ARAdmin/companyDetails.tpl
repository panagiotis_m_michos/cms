{include file="@header.tpl"}

{* DISPLAY ONLY IF CUSTOMER HAS SERVICE *}

	<ul class="submenu">
		<li><a href="{$this->router->link("missingDataEmail", "company_id=$companyId", "keepThis=true", "TB_iframe=true", "width=600", "height=400")}" class="thickbox">Send Missing Information Email</a></li>
		<li><a href="{$this->router->link("sync", "company_id=$companyId")}"><b>Sync</b></a></li>
        <li><a href="{$this->router->link("remove", "company_id=$companyId")}" onclick="alert('Are you sure?');"><b>Remove CS</b></a></li>
	</ul>


	{* SUBNAV *}
	<ul id="navlist">
		<li><a href="{$this->router->link('companyDetails', "company_id=$companyId")}" class="current">Company Details</a></li>
		{if $company->getType() != 'LLP'}
			<li><a href="{$this->router->link('capitals', "company_id=$companyId")}">Capitals</a></li>
			<li><a href="{$this->router->link('shareholdings', "company_id=$companyId")}">Shareholdings</a></li>
			{if $showPscs}
				<li><a href="{$this->router->link('pscs', "company_id=$companyId")}">PSCs</a></li>
			{/if}
		{/if}
		<li><a href="{$this->router->link('summary', "company_id=$companyId")}">Summary</a></li>
	</ul>

	{$form->getBegin() nofilter}
	<fieldset>
		<legend>Confirmation Statement Date</legend>
		<table class="ff_table">
		<col width="200">
		<col>
		<col width="300">
		<tr>
			<th>{$form->getLabel('returnDate') nofilter}</th>
			<td>{$form->getControl('returnDate') nofilter}</td>
			<td><span class="ff_control_err">{$form->getError('returnDate')}</span></td>
		</tr>
		</table>
	</fieldset>



    {if $company->getType() != 'LLP'}
	<fieldset>
		<legend>Classification codes</legend>
		<table class="ff_table">
		<col width="200">
		<col>
		<col width="200">
        <tr>
            <td colspan="3">
                <span class="ff_desc">
                    Please enter your company's principal business activity using the corresponding
                    <a href="http://www.companieshouse.gov.uk/infoAndGuide/sic/sic2007.shtml" target="_blank">trade classification code.</a>
                </span>
            </td>
		</tr>
		{section name=foo start=1 loop=5 step=1}
		{assign var="key" value="code_"|cat:$smarty.section.foo.index}
		<tr>
			<th>{$form->getLabel($key) nofilter}</th>
			<td>{$form->getControl($key) nofilter}</td>
			<td><span class="ff_control_err">{$form->getError($key)}</span></td>
		</tr>
		{/section}
		</table>
	</fieldset>




	<fieldset>
		<legend>Traded On Regulated Market</legend>
		<table class="ff_table">
		<col width="200">
		<col>
		<col>
        <tr>
            <td colspan="3">
                <span class="ff_desc"> Were any of the company's shares admitted to trading on a market at any time during the return period?<br>
                 In this context a 'market' is one established under the rules of a UK recognised investment exchange or any other regulated markets in or outside the UK, e.g. London Stock Exchange.</span>
            </td>
		</tr>
		<tr>
			<th>
                {$form->getLabel('trading') nofilter}
            </th>
			<td style="width: 130px;">
                {$form->getControl('trading') nofilter}
            </td>

			<td><span class="ff_control_err">{$form->getError('trading')}</span></td>
		</tr>
		</table>
	</fieldset>

    <fieldset>
		<legend>DTR5Applies</legend>
		<table class="ff_table">
		<col width="200">
		<col>
		<col width="200">
        <tr>
            <td colspan="3">
                <span class="ff_desc">Throughout the return period, were there shares issued to which DTR5 applies?<br>
            The term 'DTR5' refers to Vote Holder and Issuer Notification Rules contained in Chapter 5 of the Disclosure and Transparency Rules source book issued by the Financial Services Authority. </span>
           </td>
		</tr>
      	<tr>
			<th>{$form->getLabel('dtr5') nofilter}</th>
			<td>{$form->getControl('dtr5') nofilter}</td>
			<td><span class="ff_control_err">{$form->getError('dtr5')}</span></td>
		</tr>
		</table>
	</fieldset>
{/if}



{if empty($data.premise) || empty($data.street)}
    <fieldset>
		<legend>Registered Office</legend>
		<table class="ff_table">
		<col width="200">
		<col>
		<col width="200">
		<tr>
			<th>{$form->getLabel('premise') nofilter}</th>
			<th>{$form->getControl('premise') nofilter}</th>
			<th><span class="ff_control_err">{$form->getError('premise')}</span></th>
		</tr>
		<tr>
			<th>{$form->getLabel('street') nofilter}</th>
			<th>{$form->getControl('street') nofilter}</th>
			<th><span class="ff_control_err">{$form->getError('street')}</span></th>
		</tr>
		</table>
	</fieldset>
{/if}

	{* OFFICERS *}
	{foreach from=$officers key="officerId" item="officer"}
		<fieldset>
			<legend>{if $officer.type == 'DIR'}Director{elseif $officer.type == 'SEC'}Secretary{else}Member{/if} - {$form->getLabel($officerId) nofilter}</legend>
			<table class="ff_table">
			<tr>
				<td>{$form->getControl($officerId) nofilter}</td>
				<td><span class="ff_control_err">{$form->getError($officerId)}</span></td>
			</tr>
			</table>
		</fieldset>
	{/foreach}


	<fieldset>
		<legend>Action</legend>
		<table border="0" width="100%">
		<tr>
			<td>{$form->getControl('submit') nofilter}</td>
		</tr>
		</table>
	</fieldset>

	{$form->getEnd() nofilter}


{literal}
<script type="text/javascript">
<!--

$(document).ready(function() {
        /* click trigger on input type */


        function parseDate(str) {
            return $.datepicker.parseDate('dd-mm-yy', str);
        }


        if ($('#trading0').is(':checked'))
            {
                $('input[name^="dtr5"]').parents('fieldset').hide();
            }

        //hide dtr and show trading  for date lover than 01/10/2011 plc
         if( parseDate($(".date").val()) < parseDate('01-10-2011') && $(".CompanyCategory").val() == 'PLC')
            {
                $('input[name^="dtr5"]').parents('fieldset').hide();
                $('input[name^="trading"]').parents('fieldset').show();

            }
         //hide dtr and trading  for date lover than 01/10/2011 ltd, byshares
         else if( parseDate($(".date").val()) < parseDate('01-10-2011') && $(".CompanyCategory").val() != 'PLC')
            {
                $('input[name^="dtr5"]').parents('fieldset').hide();
                $('input[name^="trading"]').parents('fieldset').hide();

            }

          //show trading and dtr5 if  date biger than 01/10/2011 any company type
         else  if( parseDate($(".date").val()) >= parseDate('01-10-2011') && $(".CompanyCategory").val() != 'BYGUAR')
            {
                    $('input[name^="trading"]').parents('fieldset').show();
                    if ($('#trading1').is(':checked'))
                        {
                            $('input[name^="dtr5"]').parents('fieldset').show();
                        }


            }

          else if( parseDate($(".date").val()) >= parseDate('01-10-2011') && $(".CompanyCategory").val() == 'BYGUAR')
            {
                $('input[name^="dtr5"]').parents('fieldset').hide();
                $('input[name^="trading"]').parents('fieldset').hide();
            }

        // incase changing date
        $(".date").change (function() {
             //hide dtr and trading  for date lover than 01/10/2011 ltd, byshares
            if( parseDate($(".date").val()) < parseDate('01-10-2011') && $(".CompanyCategory").val() == 'PLC')
            {
                $('input[name^="dtr5"]').parents('fieldset').hide();
                $('input[name^="trading"]').parents('fieldset').show();

            }

            //hide dtr and trading  for date lover than 01/10/2011 ltd, byshares
            else if(parseDate($(".date").val()) < parseDate('01-10-2011') && $(".CompanyCategory").val() != 'PLC'  )
            {
                $('input[name^="dtr5"]').parents('fieldset').hide();
                $('input[name^="trading"]').parents('fieldset').hide();

            }

            //show dtr5 and trading for date biger than 01/10/2011 and any company
            else  if( parseDate($(".date").val()) >= parseDate('01-10-2011') && $(".CompanyCategory").val() != 'BYGUAR')
            {

                    $('input[name^="trading"]').parents('fieldset').show();
                    if ($('#trading1').is(':checked'))
                        {
                            $('input[name^="dtr5"]').parents('fieldset').show();
                        }
                    }
            else if( parseDate($(".date").val()) >= parseDate('01-10-2011') && $(".CompanyCategory").val() == 'BYGUAR')
            {
                $('input[name^="dtr5"]').parents('fieldset').hide();
                $('input[name^="trading"]').parents('fieldset').hide();
            }

        });



        $('input[name^="trading"]').click(function()
        {
            if ($('#trading1').is(':checked') && parseDate($(".date").val()) >= parseDate('01-10-2011'))
            {
                $('input[name^="dtr5"]').parents('fieldset').show();
            }
            // display description
            else if ($('#trading0').is(':checked'))
            {
               $('input[name^="dtr5"]').val(0).parents('fieldset').hide()
            }
        });

    $('.error').attr('style','width:730px;margin-bottom: 20px;padding-top:0;');
});
//-->
</script>
{/literal}

{include file="@footer.tpl"}
