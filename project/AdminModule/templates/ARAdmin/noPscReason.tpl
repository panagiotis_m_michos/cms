{include file="@header.tpl"}

<fieldset style="border: 1px solid #444; margin-bottom: 40px;">
    <legend>
        <span style="color:#444; font-size:18px;">Set no PSC reason</span>
    </legend>

    {$form->getBegin() nofilter}
        {if $form->getErrors()|@count gt 0}
            <p class="ff_err_notice ff_red_err" style="width: 940px">Form has <b> {$form->getErrors()|@count}</b> error(s).
                See below for more details:</p>
        {/if}

        <fieldset>
            <legend>
                <span>Choose no PSC reason</span>
            </legend>

            <table>
                <td>{$form->getControl('noPscReason') nofilter}</td>
                <td>{$form->getLabel('noPscReason') nofilter}</td>
            </table>
        </fieldset>

        <fieldset>
            <legend>
                <span>Actions</span>
            </legend>

            {$form->getControl('save') nofilter}
        </fieldset>

    {$form->getEnd() nofilter}
</fieldset>

{include file="@footer.tpl"}
