{include file="@header.tpl"}


<ul class="submenu">
    <li><a href="{$this->router->link('addPersonPsc', "company_id=$companyId")}">Add Person PSC</a></li>
    <li><a href="{$this->router->link('addLegalPersonPsc', "company_id=$companyId")}">Add Legal Person PSC</a></li>
    <li><a href="{$this->router->link('addCorporatePsc', "company_id=$companyId")}">Add Corporate PSC</a></li>
    <li><a href="{$this->router->link('noPscReason', "company_id=$companyId")}">Remove all PSCs and set no PSC reason</a></li>
</ul>


{* SUBNAV *}
<ul id="navlist">
    <li><a href="{$this->router->link('companyDetails', "company_id=$companyId")}">Company Details</a></li>
    <li><a href="{$this->router->link('capitals', "company_id=$companyId")}">Capitals</a></li>
    <li><a href="{$this->router->link('shareholdings', "company_id=$companyId")}">Shareholdings</a></li>
    <li><a href="{$this->router->link('pscs', "company_id=$companyId")}" class="current">PSCs</a></li>
    <li><a href="{$this->router->link('summary', "company_id=$companyId")}">Summary</a></li>
</ul>

<fieldset style="border: 1px solid #444; margin-bottom: 40px;">
    <legend>
        <span style="color:#444; font-size:18px;">Current PSC information</span>
    </legend>

    {if $summaryView->hasNoPscReason()}
        <i>{$summaryView->getNoPscReason()}</i>
    {elseif $summaryView->hasPscs()}
        <table class="htmltable confirmation-statement__psc-list__table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Nature of control</th>
                <th class="confirmation-statement__psc-list__action" colspan="2">Action</th>
            </tr>
            </thead>
            <tbody>
            {foreach $summaryView->getPersonPscs() as $psc}
                <tr>
                    <td>{$psc->getFullName()}</td>
                    <td>
                        {if $psc->hasStatementNotification()}
                            <p>{$psc->getStatementNotificationText()}</p>
                        {else}
                            {foreach $psc->getNatureOfControlsTexts() as $noc}
                                <p>{$noc}</p>
                            {/foreach}
                        {/if}
                    </td>
                    <td class="confirmation-statement__psc-list__action">
                        {if !$psc->hasStatementNotification()}
                            <a href="{$this->router->link("editPersonPsc", ["company_id" => $companyId, "psc_id" => $psc->getId()])}">Edit</a>
                        {/if}
                    </td>
                    <td class="confirmation-statement__psc-list__action">
                        {if !$psc->hasStatementNotification()}
                            {if $psc->isAlreadyExistingPsc()}
                                {*<a href="{$this->router->link("AnnualReturnControler::PSC_PAGE", ["company_id" => $companyId, "psc_id" => $psc->getId(), "cessate" => true])}" onclick="return confirm('Are you sure?');">Cessate</a>*}
                            {else}
                                <a href="{$this->router->link("deletePsc", ["company_id" => $companyId, "psc_id" => $psc->getId(), "delete" => true])}" onclick="return confirm('Are you sure?');">Delete</a>
                            {/if}
                        {/if}
                    </td>
                </tr>
            {/foreach}

            {foreach $summaryView->getCorporatePscs() as $psc}
                <tr>
                    <td>{$psc->getFullName()}</td>
                    <td>
                        {if $psc->hasStatementNotification()}
                            <p>{$psc->getStatementNotificationText()}</p>
                        {else}
                            {foreach $psc->getNatureOfControlsTexts() as $noc}
                                <p>{$noc}</p>
                            {/foreach}
                        {/if}
                    </td>
                    <td class="confirmation-statement__psc-list__action">
                        {if !$psc->hasStatementNotification()}
                            <a href="{$this->router->link("editCorporatePsc", ["company_id" => $companyId, "psc_id" => $psc->getId()])}">Edit</a>
                        {/if}
                    </td>
                    <td class="confirmation-statement__psc-list__action">
                        {if !$psc->hasStatementNotification()}
                            {if $psc->isAlreadyExistingPsc()}
                                {*<a href="{$this->router->link("AnnualReturnControler::PSC_PAGE", ["company_id" => $companyId, "psc_id" => $psc->getId(), "cessate" => true])}" onclick="return confirm('Are you sure?');">Cessate</a>*}
                            {else}
                                <a href="{$this->router->link("deletePsc", ["company_id" => $companyId, "psc_id" => $psc->getId(), "delete" => true])}" onclick="return confirm('Are you sure?');">Delete</a>
                            {/if}
                        {/if}
                    </td>
                </tr>
            {/foreach}

            {foreach $summaryView->getLegalPersonPscs() as $psc}
                <tr>
                    <td>{$psc->getFullName()}</td>
                    <td>
                        {if $psc->hasStatementNotification()}
                            <p>{$psc->getStatementNotificationText()}</p>
                        {else}
                            {foreach $psc->getNatureOfControlsTexts() as $noc}
                                <p>{$noc}</p>
                            {/foreach}
                        {/if}
                    </td>
                    <td class="confirmation-statement__psc-list__action">
                        {if !$psc->hasStatementNotification()}
                            <a href="{$this->router->link("editLegalPersonPsc", ["company_id" => $companyId, "psc_id" => $psc->getId()])}">Edit</a>
                        {/if}
                    </td>
                    <td class="confirmation-statement__psc-list__action">
                        {if !$psc->hasStatementNotification()}
                            {if $psc->isAlreadyExistingPsc()}
                                {*<a href="{$this->router->link("AnnualReturnControler::PSC_PAGE", ["company_id" => $companyId, "psc_id" => $psc->getId(), "cessate" => true])}" onclick="return confirm('Are you sure?');">Cessate</a>*}
                            {else}
                                <a href="{$this->router->link("deletePsc", ["company_id" => $companyId, "psc_id" => $psc->getId(), "delete" => true])}" onclick="return confirm('Are you sure?');">Delete</a>
                            {/if}
                        {/if}
                    </td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    {else}
        No PSC added yet.
    {/if}
</fieldset>

{$form->getBegin() nofilter}
<fieldset>
    <legend>Action</legend>
    <table>
        <tr>
            <td>{$form->getControl('submit') nofilter}</td>
        </tr>
    </table>
</fieldset>
{$form->getEnd() nofilter}

<style>
    table.htmltable {
        width: 100%;
        table-layout: auto;
    }
</style>

{include file="@footer.tpl"}
