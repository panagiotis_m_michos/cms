{include file="@header.tpl"}


<ul class="submenu">

	{if isset($hasAnnualService)}
		<li><a href="{$this->router->link("missingDataEmail", "company_id=$companyId", "keepThis=true", "TB_iframe=true", "width=600", "height=400")}" class="thickbox">Send Missing Information Email</a></li>
        <li><a href="{$this->router->link("sync", "company_id=$companyId")}"><b>Sync</b></a></li>
	{/if}

	<li><a href="{$this->router->link("addShareholding", "company_id=$companyId")}" style="font-size: 12px;">Add New Shareholding</a></li>
	<li><a href="{$this->router->link("shareholdings", "company_id=$companyId", "removeAllAddresses=1")}" style="font-size: 12px;" onclick="return confirm('Are You Sure?')">Remove all shareholder addresses</a></li>
</ul>


{* SUBNAV *}
<ul id="navlist">
	<li><a href="{$this->router->link('companyDetails', "company_id=$companyId")}">Company Details</a></li>
	<li><a href="{$this->router->link('capitals', "company_id=$companyId")}">Capitals</a></li>
	<li><a href="{$this->router->link('shareholdings', "company_id=$companyId")}" class="current">Shareholdings</a></li>
	{if $showPscs}
		<li><a href="{$this->router->link('pscs', "company_id=$companyId")}">PSCs</a></li>
	{/if}
	<li><a href="{$this->router->link('summary', "company_id=$companyId")}">Summary</a></li>
</ul>


{if isset($shareholdings) && $shareholdings != ''}
	{foreach from=$shareholdings key=key item=shareholding}
		<fieldset style="border: 1px solid #444; margin-bottom: 40px;">
			<legend>
				<span style="color:#444; font-size:18px;">Shareholding</span>
				<span style="font-size: 12px; font-weight: normal; font-style: italic;">
					{if $shareholding}
						[ <a href="{$this->router->link("editShareholding", "company_id=$companyId", "shareholdingId=$key")}">edit</a> /
						<a href="{$this->router->link("addTransfer", "company_id=$companyId", "shareholdingId=$key")}">add  new transfer</a> /
						<a href="{$this->router->link("removeShareholding", "company_id=$companyId", "shareholdingId=$key")}" onclick="return confirm('Are You Sure?')">remove</a> ]
					{/if}
				</span>
			</legend>

			{* SHAREHOLDINGS *}
			<table class="htmltable" style="width: 700px">
			<col>
			<col width="160">
			<tr>
				<th>Share Class</th>
				<th class="right">Number Held</th>
			</tr>
			{if isset($shareholding.share_class) && isset($shareholding.number_held)}
				<tr>
					<td>{$shareholding.share_class}</td>
					<td class="right">{$shareholding.number_held}</td>
				</tr>
			{else}
				<tr>
					<td colspan="3">No shareholdings</td>
				</tr>
			{/if}
			</table>

			{* SHAREHOLDERS *}
			<br />
			<h3>Shareholders</h3>
			<br />
			<table class="htmltable" style="width: 700px">
			<tr>
				<th>Name</th>
			</tr>
			{if isset($shareholding.shareholders)}
				{foreach from=$shareholding.shareholders item=shareholder}
					<tr>
						<td>{$shareholder.name}</td>
					</tr>
				{/foreach}
			{else}
				<tr>
					<td colspan="3">No shareholders</td>
				</tr>
			{/if}
			</table>

			{* TRANSFERS *}
			<br />
			<h3>Transfers</h3>
			<br />
			<table class="htmltable" style="width: 700px">
			<col>
			<col width="160">
			<col width="100">
			<tr>
				<th>Date of Transfer</th>
				<th class="right">No. of Shares Transfered</th>
				<th class="center">Action</th>
			</tr>
			{if isset($shareholding.transfers)}
				{foreach from=$shareholding.transfers key=k item=transfer}
					<tr>
						<td>{$transfer.date}</td>
						<td class="right">{$transfer.num}</td>
						<td class="center"><a href="{$this->router->link("removeTransfer", "company_id=$companyId", "shareholdingId=$key", "transferId=$k")}" onclick="return confirm('Are You Sure?')">remove</a></td>
					</tr>
				{/foreach}
			{else}
				<tr>
					<td colspan="3">No transfers</td>
				</tr>
			{/if}
			</table>
		</fieldset>
	{/foreach}
{/if}

{* ACTION *}
{$form->getBegin() nofilter}
	<fieldset>
		<legend>Action</legend>
		<table>
		<tr>
			<td>{$form->getControl('submit') nofilter}</td>
		</tr>
		</table>
	</fieldset>
{$form->getEnd() nofilter}

{include file="@footer.tpl"}
