{literal}
<script>
<!--
	/* @var boolean */
	var changes = false;
	
	/* @var array */
	var oEditors = new Array();
	
	/* @var boolean */
	var submited = false;

	/**
 	* Adding instances of FckEditor to array to find out if content was changed
 	* @param object FckEditor
 	* @return void
 	*/
	function FCKeditor_OnComplete( editorInstance )
	{
		oEditors.push(editorInstance);
	}

	// if form was submited, then allow leave page
	$("input[type='submit']") . click (function() {
		submited = true;
	});

	// any changes on inputs, selects
	$("input, select").change(function(){
      changes = true;
	});

	// check if there are any changes
	window.onbeforeunload = function(){
		if (submited == false) {
			// fck editors
			for(i=0;i<oEditors.length;i++) {
	    	    if (oEditors[i].IsDirty() == true) {
	        		changes = true;
	        	}
	  		}
	  	
	  		// if any changes display alert window
			if (changes == true)    {
					return 'There are unsaved data!';
			} 
		}
	}
//-->
</script>
{/literal}