{literal}
    <script>
        $(function () {
            function setCheckedState($radios) {
                $.each($radios, function(i, e) {
                    $(e).data('checked', e.checked);
                });
            }

            setCheckedState($(".nature-of-control-container :radio"));

            function toggleChevron(e) {
                $(e.target)
                        .prev()
                        .find("i.nature-of-control-indicator")
                        .toggleClass('fa-chevron-right fa-chevron-down');
            }

            var $natureOfControlContainer = $('.nature-of-control-container');
            $natureOfControlContainer
                    .on('hide.bs.collapse', toggleChevron)
                    .on('show.bs.collapse', toggleChevron)
                    .on('click', ':radio', function() {
                        var newState = !$(this).data('checked');
                        setCheckedState($('.nature-of-control-container :radio[name="' + $(this).attr('name') + '"]'));

                        $(this).prop('checked', newState);
                        $(this).data('checked', newState);
                    });

            $natureOfControlContainer.find(':radio:checked').parents('.collapse').collapse('show')
        });
    </script>
{/literal}

<fieldset class="nature-of-control-container">
    <legend>Nature of Control:</legend>
    <p class="nature-of-control-description">
        {$natureOfControlsDescription}
    </p>
    <span class="redmsg">{$form->getError('significant_influence_or_control')}</span>

    {foreach $natureOfControls as $radioName => $natureOfControl}
        <div data-toggle="collapse" data-target="#{$radioName}" aria-expanded="false">
            <i class="nature-of-control-indicator fa fa-chevron-right"></i>
            <span class="nature-of-control-title">{$natureOfControl['title']}</span>
        </div>

        <div id="{$radioName}" class="collapse" style="padding-left: 20px">
            {foreach $natureOfControl['groups'] as $groupIndex => $group}
                {if isset($group['title'])}
                    <div data-toggle="collapse" data-target="#{$radioName}_{$groupIndex}" aria-expanded="false">
                        <i class="nature-of-control-indicator fa fa-chevron-right"></i>
                        <span class="nature-of-control-group-title">{$group['title']}</span>
                    </div>
                {/if}

                <div id="{$radioName}_{$groupIndex}" {if isset($group['title'])}class="collapse" style="padding-left: 20px;"{/if}>
                    {if isset($group['description'])}
                        <p class="nature-of-control-group-description">{$group['description']}</p>
                    {/if}
                    {foreach array_keys($group['options']) as $optionKey}
                        {$form[$radioName]->getControl($optionKey) nofilter}
                    {/foreach}
                </div>
            {/foreach}
        </div>
    {/foreach}
</fieldset>
