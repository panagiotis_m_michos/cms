{if !empty($tabs)}
<ul id="navlist">
	{foreach from=$tabs key="key" item="tab"}
	<li><a href="{$tab.link}"{if $this->action == $key} class="current"{/if}>{$tab.title}</a></li>
	{/foreach}
</ul>
{/if}
