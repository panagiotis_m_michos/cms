<fieldset style="clear: both;">
	<legend>Autocomplete details (optional)</legend>
	<table class="ff_table">
		<tbody>
		<tr>
			<th>{$form->getLabel('prefillOfficers') nofilter}</th>
			<td>{$form->getControl('prefillOfficers') nofilter}</td>
		</tr>
		</tbody>
	</table>
	<p>Or enter the details below if adding a new appointment.</p>
</fieldset>