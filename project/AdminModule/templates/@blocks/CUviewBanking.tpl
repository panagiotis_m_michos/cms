<h2>Bank Accounts</h2>
<table class="htmltable">
    <tr>
        <th class="center">Bank Type</th>
        <th class="center">Date sent</th>
        <th class="center">Action</th>
        <th class="center">Error code</th>
    </tr>

    {if $view->hasToShowBarclaysEmptyRow($companyEntity, $company)}
        <tr>
            <td>Barclays</td>
            <td>&nbsp;</td>
            <td><a href="{url route="CUviewBarclaysData" company_id=$companyId}">view</a></td>
            <td>&nbsp;</td>
        </tr>
    {/if}
    {if $view->hasToShowTsbEmptyRow($companyEntity, $company)}
        <tr>
            <td>TSB</td>
            <td>&nbsp;</td>
            <td><a href="{url route="CUviewTsbData" company_id=$companyId}">view</a></td>
            <td>&nbsp;</td>
        </tr>
    {/if}

    {assign var="bankingData" value=$view->getBankingData($companyEntity, $company)}
    {foreach $bankingData as $bank => $data}
        {if $view->canShowBankingRow($companyEntity, $bank)}
            <tr>
                <td>{$view->getBankName($data)}</td>
                <td>{$view->getDateSent($data)}</td>
                <td>
                    {if $view->canShowAction($companyEntity, $bank)}
                        {if $bank == 'BARCLAYS'}
                            <a href="{url route="CUviewBarclaysData" company_id=$companyId}">view</a>
                        {elseif $bank == 'TSB'}
                            <a href="{url route="CUviewTsbData" company_id=$companyId}">view</a>
                        {/if}
                    {/if}
                </td>
                <td class="noOverflow">{$view->getResponseDescription($data)}</td>
            </tr>
        {/if}
    {/foreach}
</table>