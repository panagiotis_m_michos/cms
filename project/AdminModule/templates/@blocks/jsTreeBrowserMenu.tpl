
{if isset($jsTreeFilesMenu)}

	<div style="position: relative;  height: 18px;">
		<style>
		{literal}
		.dtree {
			width: 350px;
		}
		.buttons {
			margin: 0 0 10px 0;
		}

		#treeBrowserContainer {
			width: 730px;
			position: absolute;
			xleft: 50px;
			xtop: 15px;
			z-index: 23178;

		}


		#treeBrowser {
			font: normal normal 11px/1.4 Consolas, Arial;
			position: relative;
			padding: 1px;
			color: #000;
			background: #fff;
			border: 1px solid #D2D2D2;
		}

		#treeBrowserIcon {
			position: absolute;
			right: 15px;
			top: 0;
			line-height: 1;
			padding: 4px;
			color: #000;
			text-decoration: none;
		}


		#treeBrowser span[title] {
			border-bottom: 1px dotted gray;
			cursor: help;
		}

		#treeBrowser #browserTree {
			max-height: 300px;
			overflow: auto;
			background: white;
		}

		{/literal}
		</style>

		<div id="treeBrowserContainer">
			<div id="treeBrowser">
				<a id="treeBrowserIcon" href="#"><span>&#x25bc;</span></a>
				<div id="browserTree">
					<script type="text/javascript">
					<!--
					d = new dTree('d','{$urlImgs}icons/');
					{$jsTreeFilesMenu nofilter}
					document.write(d);
					d.openTo({$this->node->getId()}, true);
					//-->
					</script>
				</div>
			</div>
		</div>

		{literal}
		<script type="text/javascript">
		/* <![CDATA[ */
		$(document).ready(function () {

			$('#treeBrowserIcon').click(function() {
				showAndCollapseTree(this, false);
			});

			showAndCollapseTree(document.getElementById('treeBrowserIcon'), true);

			function showAndCollapseTree(_this, collaps) {
				var span = _this.getElementsByTagName('span')[0];
				var panel = document.getElementById('browserTree');

				if (collaps == false) {
					var collapsed = panel.currentStyle ? panel.currentStyle.display == 'none' : getComputedStyle(panel, null).display == 'none';
				} else {
					var collapsed = false;
				}

				span.innerHTML = collapsed ? String.fromCharCode(0x25bc) : 'Change gallery ' + String.fromCharCode(0x25ba);
				panel.style.display = collapsed ? 'block' : 'none';
				span.parentNode.style.position = collapsed ? 'absolute' : 'static';
				return false;
			}
		});
		/* ]]> */
		</script>
		{/literal}
	</div>
{/if}
