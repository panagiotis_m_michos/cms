{if !empty($controls)}
<div id="pageControls">
	<ul>
		{foreach from=$controls key="key" item="control"}
		<li><a href="{$control.link}" class="thickbox">{$control.title}</a></li>
		{/foreach}
</ul>
</div>
{/if}
