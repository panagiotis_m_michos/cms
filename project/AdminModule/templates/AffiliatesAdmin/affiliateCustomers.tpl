{include file="@header.tpl"}

{literal}
<style>
<!--
h2 {
	position: relative;
}
	h2 a {
		font-size: 11px;
		text-decoration: none;
		font-weight: normal;
		position: absolute;
		right: 0px;
		top: 12px;
	}
		h2 a.ui-widget-content.ui-corner-all {
			background: #F3F3F3;
			color: #444;
			padding: 1px 3px 1px 3px;
		}
		h2 a:hover {
			text-decoration: none;
		}
-->
</style>
{/literal}

{* CUSTOMERS *}
<h2>{$affiliate->name|upper} - CUSTOMERS <a href="{$this->router->link(NULL, "affiliateId=$affiliateId", 'csv=1')}" class="ui-widget-content ui-corner-all">Export to CSV</a></h2>

<table class="htmltable">
<col width="80">
<col>
<col>
<col width="80">
<tr>
	<th class="right">Id</th>
	<th>Contact name</th>
	<th>Email</th>
	<th class="center">Action</th>
</tr>
{if !empty($customers)}
	{foreach from=$customers key="id" item="customer"}
	<tr>
		<td class="right">{$id}</td>
		<td>{$customer->firstName} {$customer->lastName}</td>
		<td>{$customer->email}</td>
		<td class="center"><a href="{$this->router->link("CustomersAdminControler::CUSTOMERS_PAGE view","customer_id=$id")}">view</a></td>
	</tr>
	{/foreach}
{else}
	<tr>
		<td colspan="4">No customers found.</td>
	</tr>
{/if}
</table>

{* === PAGINATOR === *}
{$paginator nofilter}

{include file="@footer.tpl"}