{include file="@header.tpl"}

<ul class="submenu">
	<li><a href="{$this->router->link(null, "affiliateId=$affiliateId", 'csv=1')}">Export to CSV</a></li>
</ul>

{* FILTER FORM *}
{$form->getBegin() nofilter}
	<fieldset>
		<legend>Filter</legend>
		<table border="0" cellspacing="5" cellpadding="0">
		<tr>
			<td>{$form->getLabel('dtFrom') nofilter}</td>
			<td>{$form->getControl('dtFrom') nofilter}</td>
			<td>{$form->getLabel('dtTill') nofilter}</td>
			<td>{$form->getControl('dtTill') nofilter}</td>
			<td>{$form->getControl('submit') nofilter}</td>
		</tr>
		</table>
	</fieldset>
{$form->getEnd() nofilter}

<h2>Report</h2>

<table class="htmltable">
<col width="100">
<col>
<col width="100">
<tr>
	<th class="right">Customer Id</th>
	<th>Email</th>
	<th class="right">Count</th>
</tr>
{if !empty($report)}
	{assign var="counter" value=0}
	{foreach from=$report item="customer"}
		{assign var="counter" value=$counter+$customer->count}
		<tr>
			<td class="right">{$customer->customerId}</td>
			<td>{$customer->email}</td>
			<td class="right">{$customer->count}</td>
		</tr>
	{/foreach}
	<tr>
		<td colspan="2" class="right"><b>Total</b></td>
		<td class="right">{$counter}</td>
	</tr>
{else}
	<tr>
		<td colspan="3">No orders</td>
	</tr>
{/if}
</table>
{include file="@footer.tpl"}