{include file="@header.tpl"}

{* SUBMENU *}
<ul class="submenu">
	<li><a href="{$this->router->link("affiliateCustomers", "affiliateId=$affiliateId")}">Customers</a></li>
	<li><a href="{$this->router->link("affiliateOrders", "affiliateId=$affiliateId")}">Orders</a></li>
	<li><a href="{$this->router->link("affiliateReport", "affiliateId=$affiliateId")}">Report</a></li>
	<li><a href="{$this->router->link("affiliateProducts", "affiliateId=$affiliateId")}">Mark Ups</a></li>
	<li><a href="{$this->router->link("edit", "affiliateId=$affiliateId", "keepThis=true", "TB_iframe=true", "width=600", "height=550")}" class="thickbox">Edit</a></li>
	<li><a href="{$this->router->link("delete", "affiliateId=$affiliateId")}" onclick="return confirm('Are you sure?');">Delete</a></li>
</ul>

{* DATA *}
<h2>Data</h2>
<table class="htmltable">
<col width="150">
<tr>
	<th>Name</th>
	<td>{$affiliate->name}</td>
</tr>
<tr>
	<th>Id</th>
	<td>{$affiliate->getId()}</td>
</tr>
<tr>
	<th>Status</th>
	<td>{$affiliate->getStatus()}</td>
</tr>
<tr>
	<th>Hash</th>
	<td>{$affiliate->hash}</td>
</tr>
<tr>
	<th>Email</th>
	<td>{$affiliate->email}</td>
</tr>
<tr>
	<th>Phone</th>
	<td>{$affiliate->phone}</td>
</tr>
<tr>
	<th>Web</th>
	<td>{$affiliate->web}</td>
</tr>
</table>

{* COLOURS *}
<h2>Colours</h2>
<table class="htmltable">
<col width="150">
<tr>
	<th>Company colour</th>
	<td>
		{if $affiliate->companyColour}
			#{$affiliate->companyColour}
		{else}
			N/A
		{/if}
	</td>
</tr>
<tr>
	<th>Text Colour</th>
	<td>
		{if $affiliate->textColour}
			#{$affiliate->textColour}
		{else}
			N/A
		{/if}
	</td>
</tr>
</table>

{* SERVICES *}
<h2>Services</h2>
<table class="htmltable">
<col width="150">
<tr>
	<th valign="top">Services</th>
	<td>
		{assign var="services" value=$affiliate->getServices()}
		{if !empty($services)}
			{foreach from=$services item="service"}
				{$service}<br />
			{/foreach}
		{else}
			N/A
		{/if}
	</td>
</tr>
</table>

{* WHITE LABEL *}
<h2>White Label</h2>
<table class="htmltable">
<col width="150">
<tr>
	<th>Packages</th>
	<td>
		{foreach from=$affiliate->getPackages() item="package"}
			{$package->getLngTitle()}<br />
		{/foreach}
	</td>
</tr>
<tr>
	<th>Demo</th>
	<td>
		{assign var="hash" value=$affiliate->hash}
		{assign var="link" value=$this->router->absoluteFrontLink("WidgetWhiteLabelControler::DEMO_PAGE", "hash=$hash")}
		<a href="{$link}" target="_blank">{$link}</a>
	</td>
</tr>
<tr>
	<th>Iframe</th>
	<td>
		{assign var="hash" value=$affiliate->hash}
		{assign var="link" value=$this->router->absoluteFrontLink("WidgetWhiteLabelControler::HOME_PAGE", "hash=$hash")}
		<textarea rows="4" cols="67" readonly="readonly" onclick="this.select();"><iframe src="{$link}" width="100%" height="800" frameborder="0"><p>Your browser does not support iframes.</p></iframe></textarea>
	</td>
</tr>
{if isset($affiliate->textbox)}
<tr>
	<th>TextBox</th>
	<td>
		<textarea rows="4" cols="67" readonly="readonly" onclick="this.select();">{$affiliate->textbox}</textarea>
	</td>
</tr>
{/if}
</table>

{* LTD Calculator *}
<h2>LTD Calculator</h2>
<table class="htmltable">
<col width="150">
<tr>
	<th>Tracking Tag</th>
	<td>
		{if $affiliate->googleAnalyticsTrackingTag}
			{$affiliate->googleAnalyticsTrackingTag}
		{else}
			N/A
		{/if}
	</td>
</tr>
<tr>
	<th>Demo</th>
	<td>
		{assign var="hash" value=$affiliate->hash}
		{assign var="link" value=$this->router->absoluteFrontLink("WidgetCalculatorControler::DEMO_PAGE", "hash=$hash")}
		<a href="{$link}" target="_blank">{$link}</a>
	</td>
</tr>
<tr>
	<th>Iframe</th>
	<td>
		{assign var="hash" value=$affiliate->hash}
		{assign var="link" value=$this->router->absoluteFrontLink("WidgetCalculatorControler::HOME_PAGE", "hash=$hash")}
		<textarea rows="4" cols="67" readonly="readonly" onclick="this.select();"><iframe src="{$link}" width="100%" height="800" frameborder="0"><p>Your browser does not support iframes.</p></iframe></textarea>
	</td>
</tr>
</table>

{* COMPANY NAME SEARCH *}
<h2>Company Name Search</h2>
<table class="htmltable">
<col width="150">
<tr>
	<th>Demo</th>
	<td>
		{assign var="hash" value=$affiliate->hash}
		{assign var="link" value=$this->router->absoluteFrontLink("WidgetSearchControler::DEMO_PAGE", "hash=$hash")}
		<a href="{$link}" target="_blank">{$link}</a>
	</td>
</tr>
<tr>
	<th>Iframe</th>
	<td>
		{assign var="hash" value=$affiliate->hash}
		{assign var="link" value=$this->router->absoluteFrontLink("WidgetSearchControler::HOME_PAGE", "hash=$hash")}
		<textarea rows="4" cols="67" readonly="readonly" onclick="this.select();"><iframe src="{$link}" width="370" height="100" frameborder="0"><p>Your browser does not support iframes.</p></iframe></textarea>
	</td>
</tr>
</table>

{* STATS *}
<h2>Stats</h2>
<table class="htmltable">
<col width="150">
<tr>
	<th>Customers</th>
	<td>{$affiliate->getAffiliateCustomersCount()}</td>
</tr>
<tr>
	<th>Orders</th>
	<td>{$affiliate->getAffiliateOrdersCount()}</td>
</tr>
</table>

{include file="@footer.tpl"}
