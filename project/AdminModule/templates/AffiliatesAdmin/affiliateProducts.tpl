{include file="@header.tpl"}

{if !empty($affiliateProducts)}
{$form->getBegin() nofilter}
	<table class="htmltable">
	<col width="50" />
	<col />
	<col width="100" />
	<col width="100" />
	<col width="70" />
	<tr>
		<th>Id</th>
		<th>Product</th>
		<th>Mark Up</th>
		<th>Created</th>
		<th class="center">Action</th>
	</tr>
	{foreach from=$affiliateProducts key="id" item="affiliateProduct"}
	{$affiliateProductId = $affiliateProduct->affiliateProductId}
	{assign var="affP" value=$affiliateProduct->getProduct()}
	<tr>
		<td>{$affiliateProduct->affiliateProductId}</td>
		<td>{$affP->getLngTitle()}</td>
		<td><input type="text" name="editMarkUp[{$id}]" size="5" value="{$affiliateProduct->markUp}" /></td>
		<td class="center">{$affiliateProduct->dtc|date_format:"%d/%m/%Y"}</td>
		<td><a href="{$this->router->link("deleteProduct", "affiliateProductId=$id", "affiliateId=$affiliateId")}" onclick="return confirm('Are you sure?');">Delete</a></td>
	</tr>
	{/foreach}
	</table>
	<fieldset id="fieldset_1">
		<legend>Action</legend>
		<table class="ff_table">
			<tbody><tr>
				<th></th>
				<td>{$form->getControl('submit') nofilter}</td>
				<td></td>
			</tr>
		</tbody></table>
	</fieldset>

{$form->getEnd() nofilter}
	
	{* PAGINATOR *}
	{$paginator nofilter}
{else}
	<p>No affiliate products</p>
{/if}

{include file="@footer.tpl"}