{include file="@header.tpl"}

{literal}
<style>
<!--
fieldset {
	margin: 0 0 20px 0;
}
h2 {
	position: relative;
}
	h2 a {
		font-size: 11px;
		text-decoration: none;
		font-weight: normal;
		position: absolute;
		right: 0px;
		top: 15px;
	}
		h2 a.ui-widget-content.ui-corner-all {
			background: #F3F3F3;
			color: #444;
			padding: 1px 3px 1px 3px;
		}
		h2 a:hover {
			text-decoration: none;
		}
-->
</style>
{/literal}

<h2>{$affiliate->name|upper} - ORDERS <a href="{$this->router->link(NULL, "affiliateId=$affiliateId", 'csv=1')}" class="ui-widget-content ui-corner-all">Export to CSV</a></h2>

{* FILTER FORM *}
{$form->getBegin() nofilter}
	<fieldset>
		<legend>Filter</legend>
		<table border="0" cellspacing="5" cellpadding="0">
		<tr>
			<td>{$form->getLabel('dtFrom') nofilter}</td>
			<td>{$form->getControl('dtFrom') nofilter}</td>
			<td>{$form->getLabel('dtTill') nofilter}</td>
			<td>{$form->getControl('dtTill') nofilter}</td>
			<td>{$form->getControl('submit') nofilter}</td>
		</tr>
		</table>
	</fieldset>
{$form->getEnd() nofilter}

<table class="htmltable">
<col width="80">
<col>
<col width="80">
<col width="80">
<col width="80">
<col width="100">
<col width="70">
<tr>
	<th class="right">Id</th>
	<th>Email</th>
	<th class="right">Subtotal</th>
	<th class="right">Vat</th>
	<th class="right">Total</th>
	<th class="center">Date</th>
	<th class="center">Action</th>
</tr>

{if !empty($orders)}
	{foreach from=$orders key="key" item="order"}
	{assign var="orderId" value=$order->orderId}
	<tr>
		<td class="right">{$order->orderId}</td>
		<td>{$order->customerEmail}</td>
		<td class="right">{$order->subtotal}</td>
		<td class="right">{$order->vat}</td>
		<td class="right">{$order->total}</td>
		<td class="center">{$order->dtc|date_format:"%d/%m/%Y"}</td>
		<td class="center"><a href="{$this->router->link("OrdersAdminControler::ORDERS_PAGE view","order_id=$orderId")}">View</a></td>
	</tr>
	{/foreach}
{else}
	<tr>
		<td colspan="7">No orders</td>
	</tr>
{/if}
</table>

{* === PAGINATOR === *}
{$paginator nofilter}


{include file="@footer.tpl"}