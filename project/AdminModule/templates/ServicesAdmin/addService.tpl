{include file="@header.tpl"}

<div class="ajax_overlay_container">
    {$formHelper->start($form) nofilter}
        <div class="errormsg1">
            {$formHelper->errors($form) nofilter}
        </div>

        <fieldset>
            <legend>Service</legend>
            <div {if $formHelper->errors($form['productId'])}class="errormsg1"{/if}>
                {$formHelper->label($form['productId']) nofilter}
                {$formHelper->widget($form['productId']) nofilter}
                {$formHelper->errors($form['productId']) nofilter}
            </div>
            <div {if $formHelper->errors($form['startDate'])}class="errormsg1"{/if}>
                {$formHelper->label($form['startDate']) nofilter}
                {$formHelper->widget($form['startDate']) nofilter}
                {$formHelper->errors($form['startDate']) nofilter}
            </div>
            <div {if $formHelper->errors($form['duration'])}class="errormsg1"{/if}>
                {$formHelper->label($form['duration']) nofilter}
                {$formHelper->widget($form['duration']) nofilter}
                {$formHelper->errors($form['duration']) nofilter}
            </div>
            <div {if $formHelper->errors($form['reason'])}class="errormsg1"{/if}>
                {$formHelper->label($form['reason']) nofilter}
                {$formHelper->widget($form['reason']) nofilter}
                {$formHelper->errors($form['reason']) nofilter}
            </div>
        </fieldset>

        <fieldset class="payment-details">
            <legend>Payment Details</legend>
            <div {if $formHelper->errors($form['paymentDate'])}class="errormsg1"{/if}>
                {$formHelper->label($form['paymentDate']) nofilter}
                {$formHelper->widget($form['paymentDate']) nofilter}
                {$formHelper->errors($form['paymentDate']) nofilter}
            </div>
            <div {if $formHelper->errors($form['totalAmount'])}class="errormsg1"{/if}>
                {$formHelper->label($form['totalAmount']) nofilter}
                {$formHelper->widget($form['totalAmount']) nofilter}
                {$formHelper->errors($form['totalAmount']) nofilter}
            </div>
            <div {if $formHelper->errors($form['reference'])}class="errormsg1"{/if}>
                {$formHelper->label($form['reference']) nofilter}
                {$formHelper->widget($form['reference']) nofilter}
                {$formHelper->errors($form['reference']) nofilter}
                <span class="ff_desc">Invoice number or any other payment reference</span>
            </div>
            <div>

            </div>
        </fieldset>

        <fieldset>
            <legend>Action</legend>
            <div class="flash info">
                ATTENTION: The added service cannot be changed. Please double check all information before proceeding.
            </div>
            {$formHelper->row($form['add'], ['attr' => ['class' => 'btn']]) nofilter}
        </fieldset>
    {$formHelper->end($form) nofilter}
</div>

{literal}
    <script>
        var loader = new CMS.AjaxLoader('.ajax_overlay_container');

        $('#productId, #startDate').on('change', function () {
            loader.show();
            $(this).closest('form').submit();
        });

        $('[name="reason"]').on('change', function() {
            var selected = $(this).filter(':checked').val();
            $('.payment-details').toggle(selected == 'nonStandard');
        }).trigger('change');
    </script>
{/literal}

{include file="@footer.tpl"}

