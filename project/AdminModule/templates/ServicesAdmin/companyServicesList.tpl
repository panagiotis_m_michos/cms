{include file="@header.tpl"}

{if !$company->isDissolved()}
    <ul class="submenu">
        <li><a href="{$this->router->link('addService', ['company_id' => $company->getId()])}">Add Service</a></li>
    </ul>
{/if}

{if !empty($services)}
    {$services nofilter}
{/if}

{include file="@footer.tpl"}
