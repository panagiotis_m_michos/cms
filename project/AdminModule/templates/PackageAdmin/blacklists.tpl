{include file="@header.tpl"}

{*$form nofilter*}

{$form->getBegin() nofilter}
<fieldset>
    <legend>Equivalent Blacklist</legend>
    <div>
        The products within this list cannot be purchased with the <strong>{$this->node->getLongTitle()}</strong>.
        If the customer tries to add a product from this list to his basket he will get an error saying something like: <br/><br/>
        <i>Sorry, you are trying to purchase the <strong>[second product added]</strong> but you already have the <strong>{$this->node->getLongTitle()}</strong> in your basket. At this time we only support the purchase of one company at a time.</i>
    </div>
    <br/>
    <table class="ff_table">
        <tbody>
            <tr>
                <th>{$form->getLabel('blacklistedEquivalentProducts') nofilter}</th>
                <td>
                    {$form->getControl('blacklistedEquivalentProducts') nofilter}
                </td>
                <td>&nbsp;</td>
            </tr>
        </tbody>
    </table>
</fieldset>
<fieldset>
    <legend>Contained Blacklist</legend>
    <div>
        The products within this list cannot be purchased with the <strong>{$this->node->getLongTitle()}</strong> because it already contains these products.
        If the customer tries to add a product from this list to his basket he will get an error saying something like:<br/><br/>
        <i>The <strong>[second product added]</strong> is already included in your <strong>{$this->node->getLngTitle()}</strong>, so we have removed the extra <strong>[second product added]</strong> from your basket."
    </div>
    <br/>
    <table class="ff_table">
        <tbody>
        <tr>
            <th>{$form->getLabel('blacklistedContainedProducts') nofilter}</th>
            <td>
                {$form->getControl('blacklistedContainedProducts') nofilter}
            </td>
            <td>&nbsp;</td>
        </tr>
        </tbody>
    </table>
</fieldset>
<fieldset>
    <legend>Action</legend>
    {$form->getControl('submit') nofilter}
</fieldset>
{$form->getEnd() nofilter}

{include file="@footer.tpl"}