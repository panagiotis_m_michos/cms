{include file="@header.tpl"}

<p><a href="{$this->router->link("create")}">Add new reserved word</a></p>

{if !empty($words)}
	<table class="htmltable">
	<col>
	<col width="50">
	<col width="60">
	<tr>
		<th>Reserved word</th>
		<th colspan="2">Action</th>
	</tr>
	{foreach from=$words key="key" item="word"}
	<tr>
		<td>{$word->word}</td>
		<td><a href="{$this->router->link("edit","word_id=$key")}">edit</a></td>
		<td><a href="{$this->router->link("delete","word_id=$key")}" onclick="return confirm('Are you sure?')">delete</a></td>
	</tr>
	{/foreach}
	</table>
{else}
	<p>No words</p>
{/if}

{include file="@footer.tpl"}