{include file="@header.tpl"}

{literal}
<script type="text/javascript">
$(document).ready(function (){
	disableTemplateFile();
	$("input[name='typeId']").click(function (){
		disableTemplateFile();
	});
	
	function disableTemplateFile() {
		var disabled = $('#typeId1').is(":checked");
		$("input[name^='fileId']").attr("disabled", disabled);
		
		if (disabled) {
			if ($("#fileId").val() != '' && $("#fileId").val() != 0) {
				$("#fileId_remove").attr("style", "color: silver");
			} else {
				$("#fileId_add").attr("style", "color: silver");
			}
		} else {
			if ($("#fileId").val() != '' && $("#fileId").val() != 0) {
				$("#fileId_remove").attr("style", "color: black");
			} else {
				$("#fileId_add").attr("style", "color: black");
			}
			
		}
	}
	
});
</script>
{/literal}

{$form nofilter}
{include file="@footer.tpl"}