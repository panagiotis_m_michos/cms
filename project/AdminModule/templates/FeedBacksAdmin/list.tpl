{include file="@header.tpl"}

<ul class="submenu">
	<li><a href="{$this->router->link(null,'csv=1')}">Export to CSV</a></li>
</ul>

    {$form nofilter}

    <table class="htmltable">
	<col width="85"/>
	<col />
	<col />
        <col width="90" />
	<tr>
            <th>Rating</th>
            <th>Comment</th>
            <th>Page title</th>
            <th>Date</th>
        </tr>
        {if !$feedbacks}
           <tr>
                <td align="center" colspan="4">No data</td>
           </tr>
        {else}
            {foreach from=$feedbacks key="id" item="feedback"}
            <tr>
                    <td>{$feedback->rating}</td>
                    <td>{$feedback->text}</td>
                    <td>{$feedback->pageTitle}</td>
                    <td class="center">{$feedback->dtc|date_format:"%d/%m/%Y"}</td>
            </tr>
            {/foreach}
        {/if}
    </table>

    {* PAGINATOR *}
     {!$paginator nofilter}


{include file="@footer.tpl"}