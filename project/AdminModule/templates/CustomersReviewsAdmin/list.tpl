{include file="@header.tpl"}

{literal}
<style>
<!--
ul.comment-actions {
	margin: 10px 0 3px 0;
	visibility: hidden;
}
	ul.comment-actions li {
		display: inline;
		margin-right: 5px;
		font-size: 11px;
		color: #d2d2d2;
	}
		ul.comment-actions li a {
			color: #D54E21;
		}
fieldset {
	margin: 10px 0 10px 0;
}
	fieldset.filter {
		margin-bottom: 20px;
	}
-->
</style>

<script type="text/javascript">
<!--
$(document).ready(function() {
	
	$('table.htmltable tr').hover(
		function() {
			$('ul.comment-actions', this).css('visibility', 'visible');
			$('ul.comment-actions a', this).css('color', '#D54E21');
		},
		function() {
			$('ul.comment-actions', this).css('visibility', 'hidden');
		}
	);
	
	// check and uncheck rows
	$('#check-all').click(function() {
		$('input[type="checkbox"][name^="ids"]').attr('checked', true);
	});
	$('#check-none').click(function() {
		$('input[type="checkbox"][name^="ids"]').attr('checked', false);
	});
	$('#actionSubmit').click(function() {
		if ($('#action').val() == 'delete') {
			return confirm('Are you sure');
		}
	});
});

//-->
</script>
{/literal}

<ul class="submenu">
	<li><a href="{$this->router->link('import')}">Import</a></li>
</ul>

{* FILTER *}
{$filterForm->getBegin() nofilter}
<fieldset class="filter">
	<legend>Filter</legend>
	<table>
	<col width="50">
	<col width="120">
	<tr>
		<td>{$filterForm->getLabel('statusId') nofilter}</td>
		<td>{$filterForm->getControl('statusId') nofilter}</td>
		<td>{$filterForm->getControl('submit') nofilter}</td>
	</tr>
	</table> 
</fieldset>
{$filterForm->getEnd() nofilter}


{* SELECT ALL/NONE *}
{if !empty($reviews)}
	<p><b>Select:</b> <a href="#" id="check-all">All</a>, <a href="#" id="check-none">None</a></p>
{/if}

{* === REVIEWS === *}
{$batchForm->getBegin() nofilter}

	{* BATCH ACTIONS FORM ERROR *}
	{$batchForm->getHtmlErrorsInfo()}
	
	<table class="htmltable">
	<col width="60">
	<col>
	<col width="130">
	<tr>
		<th>Action</th>
		<th>Text</th>
		<th class="center">Comment</th>
	</tr>
	{if !empty($reviews)}
		{foreach from=$reviews key="key" item="review" name="f1"}
			<tr>
				<td class="center">{$batchForm->getControl('ids', $key) nofilter}</td>
				<td class="noOverflow">
					{if $review->name}<strong>{$review->getName()}</strong>, {/if}{if $review->getCompanyName()}<strong>{$review->getCompanyName()}</strong>, {/if} <small>{$review->getDtc()|df:"d/m/Y"}</small> <br />
					{$review->getSuggestions()}
					<ul class="comment-actions">
					
						{if $review->isApproved()}
							<li>Approve</li>
						{else}
							<li><a href="{$this->router->link("approve", "customerReviewId=$key")}">Approve</a></li>
						{/if}
						
						{if $review->isUnapproved()}
							<li>Unapprove</li>
						{else}
							<li><a href="{$this->router->link("unapprove", "customerReviewId=$key")}">Unapprove</a></li>
						{/if}
						
						{if $review->isUnallowed()}
							<li>Unallow</li>
						{else}
							<li><a href="{$this->router->link("unallow", "customerReviewId=$key")}">Unallow</a></li>
						{/if}
						
						
						<li>|</li>
						<li><a href="{$this->router->link("edit", "customerReviewId=$key", "keepThis=true", "TB_iframe=true", "width=600", "height=400")}" class="thickbox">Edit</a></li>
						<li><a href="{$this->router->link("delete", "customerReviewId=$key")}" onclick="return confirm('Are you sure?')">Delete</a></li>
					</ul>
				</td>
				<td class="center">{$review->getComment()}</td>
			</tr>
		{/foreach}
	{else}
		<tr>
			<td colspan="3">No Comments</td>
		</tr>
	{/if}
	</table>
	
	{* BATCH ACTIONS *}
	{if !empty($reviews)}
		<fieldset>
			<legend>Bulk Actions</legend>
			<table width="100%">
			<col width="60">
			<col width="110">
			<col>
			<col>
			<tr>
				<td>{$batchForm->getLabel('action') nofilter}</td>
				<td>{$batchForm->getControl('action') nofilter}</td>
				<td>{$batchForm->getControl('actionSubmit') nofilter}</td>
				<td><span class="ff_control_err">{$batchForm->getError('action')}</span></td>
				<td align="right">{$paginator nofilter}</td>
			</tr>
			</table>
		</fieldset>
	{/if}
	

{$batchForm->getEnd() nofilter}


{include file="@footer.tpl"}