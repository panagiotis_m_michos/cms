<?php

/**
 * CMS
 *
 * @license    GPL
 * @category   Project
 * @package    Form
 * @author     Tomas Jakstas tomasj@madesimplegroup.com
 * @version    2011-10-04
 */
class VoucherAdminForm extends FForm {

	/**
	 * @var array
	 */
	private $callback;

	/**
	 * @var VoucherNew
	 */
	public $voucher;

	/**
	 * @param WholesaleAdminControler $controler
	 * @param array $callback
	 */
	public function startup(VoucherNew $voucher, array $callback) {
		$this->voucher = $voucher;
		$this->callback = $callback;
		$this->init();
	}

	private function init() {
		$this->addFieldset('Voucher Details');
		if ($this->voucher->isNew()) {
			$this->addText('numberOfVouchers', 'Number of Vouchers: * ')
				->addRule(FForm::Required, 'Number of Vouchers is required!')
				->addRule('numeric', 'Number of Vouchers has to be numeric!')
				->addRule([$this, 'Callback_validRange'], 'Number of Vouchers must be between 1 and 10000')
				->size(5)
				->setValue(1)
				->setDescription('(If number is greater than 1, suffix will be added to code)');
		}
		$this->addText('name', 'Name: * ')
				->addRule(FForm::Required, 'Name is required!')
				->size(30)
				->setValue($this->voucher->name);
		$this->addText('voucherCode', 'Code: *')
				->addRule(FForm::Required, 'Code is required!')
				->setValue($this->voucher->voucherCode)
				->size(30);
		$this->addRadio('typeId', 'Type: *', VoucherNew::$types)
				->addRule(FForm::Required, 'Type is required!')
				->setValue($this->voucher->typeId ? $this->voucher->typeId : VoucherNew::TYPE_AMOUNT);

		$this->addText('typeValue', 'Type Value: *')
				->addRule(FForm::Required, 'Type Value is required!')
				->addRule('numeric', 'Type Value has to be numeric!')
				->setValue($this->voucher->typeValue)
				->size(30);
		$this->addText('minSpend', 'Min spend (&pound;): ')
				->addRule('numeric', 'Min spend has to be numeric!')
				->setValue($this->voucher->minSpend)
				->size(30);
		$this->addText('usageLimit', 'Usage Limit: ')
				->addRule('numeric', 'Usage limit has to be numeric!')
				->setValue($this->voucher->usageLimit)
				->size(30);
		$this->add('datePicker', 'dtExpiry', 'Expiry: ')
				->setValue($this->voucher->dtExpiry);

		// action
		$this->addFieldset('Action');
		$this->addSubmit('submit', 'Submit')
				->onclick('changes = false')
				->class('btn');

		$this->onValid = $this->callback;
		$this->start();
	}

	public function processCreate() {
		try {
			$data = $this->getValues();
			$noOfVouchers = (int) $data['numberOfVouchers'];
			$i = 0;
			$originalCode = $data['voucherCode'];
			while ($noOfVouchers > $i) {
				if ($noOfVouchers > 1) {
					$data['voucherCode'] = VoucherNew::generateUniqueCode($originalCode);
				}
				$this->saveVoucher(new VoucherNew(), $data);
				$i++;
			}

			$this->clean();
		} catch (Exception $e) {
			throw $e;
		}
	}

	public function processEdit() {
		try {
			$data = $this->getValues();
			if ($this->voucher->voucherCode !== $data['voucherCode']) {
				if (VoucherNew::codeExists($data['voucherCode'])) {
					throw new Exception('Voucher code `' . $data['voucherCode'] . '` exist!');
				}
			}
			$this->saveVoucher($this->voucher, $data);
			$this->clean();
		} catch (Exception $e) {
			throw $e;
		}
	}

	private function saveVoucher(VoucherNew $voucher, $data) {
		$voucher->name = $data['name'];
		$voucher->voucherCode = $data['voucherCode'];
		$voucher->typeId = $data['typeId'];
		$voucher->typeValue = $data['typeValue'];
		$voucher->minSpend = $data['minSpend'];
		$voucher->usageLimit = $data['usageLimit'];
		$voucher->dtExpiry = $data['dtExpiry'];
		$voucher->save();
	}

	/**
	 * @param Text $control
	 * @param string $error
	 * @return bool
	 */
	public function Callback_validRange(Text $control, $error)
	{
		$value = $control->getValue();
		if ($value < 1 || $value > 10000) {
			return $error;
		}

		return TRUE;
	}
}
