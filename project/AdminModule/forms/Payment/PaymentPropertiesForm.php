<?php

/**
 * cms
 *
 * @license    GPL
 * @category   Project
 * @package    cms
 * @author     Razvan Preda <razvanp@madesimplegroup.com>
 * @version    PaymentPropertiesForm.php 18-Sep-2012 
 */

class PaymentPropertiesForm  extends FForm 
{
    /**
     * @var PaymentPropertiesFormDelegate
     */
    private $delegate;
    
    /**
     * @var PaymentModel
     */
    private $node;
    
    /**
     * @param PaymentPropertiesFormDelegate $delegate
     */
    public function startup(PaymentPropertiesFormDelegate $delegate, PaymentModel $node)
    {
        $this->delegate = $delegate;
        $this->node = $node;
        $this->init();
    }
    
    
    private function init()
    {
        
        $this->addFieldset('Disable Sage');
        $this->addCheckbox('disableSage', 'Disable Sage:', 1)
             ->setDescription('Checkbox for disable SAGE for new payment, old payment and non standard payment page!')   
             ->setValue($this->node->getDisableSage());
        
        $this->addFieldset('Enable Messages');
        $this->addCheckbox('enableMessages', 'Enable Messages', 1)
             ->setDescription('Enable messages for new, old and non standard payment pages!')   
             ->setValue($this->node->getEnableMessages());
        $this->add('CmsFckArea', 'paymentPageMessages', 'Message: ')
			->setSize(500, 200)
            ->setValue($this->node->getPaymentPageMessages());

        $this->addFieldset('Action');
		$this->addSubmit('save', 'Save')->class('btn');
        
        $this->addFieldset('Cancel');
        $this->addSubmit('cancel', 'Cancel')->class('btn');
        
        $this->beforeValidation = array($this, 'beforeValidation');
        $this->onValid = array($this, 'process');
	    $this->start();
    }
    
    /**
     * @throws Exception
     */
    public function beforeValidation()
    {
        if ($this->isSubmitedBy('cancel')) {
            $this->clean();
            $this->delegate->PaymentPropertiesFormCancelled();
        }
    }
    
    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();
            
            // --- save admin properties ---
            $this->node->setDisableSage($data['disableSage']);
            $this->node->setEnableMessages($data['enableMessages']);
            $this->node->setPaymentPageMessages($data['paymentPageMessages']);
            $this->node->save();
            $this->clean();
            $this->delegate->PaymentPropertiesFormSuccess();
        } catch (Exception $e) {
            $this->delegate->PaymentPropertiesFormWithException($e);
        }
    }
    
}
