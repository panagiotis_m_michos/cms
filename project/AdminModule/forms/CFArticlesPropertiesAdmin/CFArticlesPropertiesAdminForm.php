<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    CFArticlesPropertiesAdminForm.php 2011-11-22 razvanp@madesimplegroup.com
 */

class CFArticlesPropertiesAdminForm  extends FForm
{
    /**
     * @var CFArticlesAdminControler 
     */
    private $controler;
    /**
     * @var array
     */
    private $callback;

    /**
    *  @var CFArticlesModel
    */
    private $node;
    
    /**
     * @param CFArticlesAdminControler $controler
     * @param array $callback
     */
    public function startup(CFArticlesAdminControler $controler, array $callback, CFArticlesModel $node)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->node = $node;
        $this->init();
    }

    /**
	 * @return void
	 */
    private function init()
    {
        $this->addFieldset('Html Box Under Guide');
        
        
        $this->addCheckbox('hasVisibleHtmlBox', 'Active:',1)
            ->setDescription('(Hide or unhide the box)')    
            ->setValue($this->node->getHasVisibleHtmlBox());
        
        $this->add('CmsFckArea','underGuideHtmlBox','Box Text:')
             ->setSize(500, 200)
             ->setToolbar('Basic')
             ->setValue($this->node->getUnderGuideHtmlBox());
        

        $this->addFieldset('Action');
        $this->addSubmit('submit','Submit')
	     ->class('btn');
        
        
        $this->onValid = $this->callback;
	    $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();
            
            $this->node->setHasVisibleHtmlBox($data['hasVisibleHtmlBox']);
            $this->node->setUnderGuideHtmlBox($data['underGuideHtmlBox']);
            $this->node->save();

            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }
}
