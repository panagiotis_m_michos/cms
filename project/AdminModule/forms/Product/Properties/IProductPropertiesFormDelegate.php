<?php

interface IProductPropertiesFormDelegate
{
    function productPropertiesFormSucceeded(ProductPropertiesForm $form);
    function productPropertiesFormFailed(ProductPropertiesForm $form, Exception $e);
}

