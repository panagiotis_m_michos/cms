<?php

use BankingModule\Entities\CompanyCustomer;

class ProductPropertiesForm extends FForm
{
    const ASSOCIATED_IMAGE_PAGE = 1346;
    const ATTACHMENTS_COUNT = 5;
    const ATTACHMENT_FILE_PAGE = 575;
    const UPDATE_IMAGE_PAGE = 966;
    const PAGES_FOLDER = 266;

    /**
     * @var IProductPropertiesFormDelegate
     */
    protected $delegate;

    /**
     * @var Product
     */
    protected $node;

    /**
     * @var array
     */
    protected $products;

    /**
     * @var array
     */
    protected $serviceTypes;

    /**
     * @var array
     */
    protected $serviceDurations;

    /**
     * @param IProductPropertiesFormDelegate $delegate
     * @param BasketProduct $node
     * @param array $products
     * @param array $serviceTypes
     * @param array $serviceDurations
     */
    public function startup(IProductPropertiesFormDelegate $delegate, BasketProduct $node, array $products, array $serviceTypes, array $serviceDurations)
    {
        $this->delegate = $delegate;
        $this->node = $node;
        $this->products = $products;
        $this->serviceTypes = $serviceTypes;
        $this->serviceDurations = $serviceDurations;

        $this->buildForm();
        $this->onValid = [$this, 'process'];
        $this->start();
    }

    public function process()
    {
        try {
            $node = $this->node;
            $data = $this->getValues();

            $node->onlyOneItem = $data['onlyOneItem'];
            $node->maxQuantityOne = $data['maxQuantityOne'];
            $node->conditionedById = $data['conditionedById'];
            $node->requiredCompanyNumber = $data['requiredCompanyNumber'];

            $node->requiredIncorporatedCompanyNumber = $data['requiredIncorporatedCompanyNumber'];
            $node->onlyOurCompanies = $data['onlyOurCompanies'];
            $node->responsibleEmails = $data['responsibleEmails'];
            $node->notApplyVat = $data['notApplyVat'];
            $node->nonVatableValue = $data['nonVatableValue'];
            $node->emailText = $data['emailText'];
            $node->basketText = $data['basketText'];
            $node->lockCompany = $data['lockCompany'];
            $node->upgradeDescription = $data['upgradeDescription'];
            $node->upgradeImageId = $data['upgradeImageId'];
            $node->associatedImageId = $data['associatedImageId'];
            $node->associatedText = $data['associatedText'];
            $node->associatedDescription = $data['associatedDescription'];
            $node->associatedIconClass = $data['associatedIconClass'];

            // attachments
            $node->emailCmsFileAttachments = [];
            for ($i = 0; $i < self::ATTACHMENTS_COUNT; $i++) {
                if ($data['attachment' . $i]) {
                    $node->emailCmsFileAttachments[] = $data['attachment' . $i];
                }
            }
            $node->serviceTypeId = $data['serviceTypeId'];
            $node->setDuration($data['serviceDuration']);
            $node->renewalProductId = $data['renewalProductId'];
            $node->isAutoRenewalAllowed = $data['isAutoRenewalAllowed'];
            $node->setVoServiceEligible($data['isVoServiceEligible']);
            $node->setVoServiceDurationInMonths($data['voServiceDurationInMonths']);
            $node->isIdCheckRequired = $data['isIdCheckRequired'];
            $node->bankingEnabled = $data['bankingEnabled'];
            $node->bankingOptions = $data['bankingOptions'] ?: [];
            $node->bankingRequired = $data['bankingRequired'];
            $node->setCashBackAmount($data['cashBackAmount']);
            $node->setRemovableFromBasket($data['removableFromBasket']);
            $node->setRemoveFromBasketConfirmation($data['removeFromBasketConfirmation']);
            $node->save();

            $this->clean();
            $this->delegate->productPropertiesFormSucceeded($this);
        } catch (Exception $e) {
            $this->delegate->productPropertiesFormFailed($this, $e);
        }
    }

    public function Validator_CommaEmails($control, $error, $params)
    {
        $value = $control->getValue();
        if (!empty($value)) {
            $validator = new Email($error, $params, $control->owner, $control);
            $emails = explode(',', $value);
            foreach ($emails as $key => $email) {
                $isValid = $validator->isValid(trim($email));
                if ($isValid == FALSE) {
                    return $error;
                }
            }
        }
        return TRUE;
    }

    public function Validator_serviceRequiredElement(FControl $control, $error, $params)
    {
        $value = $control->getValue();
        list($serviceDurationControl) = $params;
        $serviceDuration = $serviceDurationControl->getValue();
        if (!$value && $serviceDuration) {
            return $error;
        }
        return TRUE;
    }

    /**
     * @param Select $control
     * @param string $error
     * @param array $params
     * @return bool
     */
    public function Validator_voServiceRequired(Select $control, $error, array $params)
    {
        $durationInMonths = $control->getValue();
        /** @var Checkbox $isVoServiceEligibleControl */
        $isVoServiceEligibleControl = $params['isVoServiceEligible'];
        $isVoServiceEligible = $isVoServiceEligibleControl->getValue();
        if ($isVoServiceEligible && !$durationInMonths) {
            return $error;
        }
        return TRUE;
    }

    private function buildForm()
    {
        $node = $this->node;

        $this->addFieldset('Banking');
        $this->addCheckbox('bankingEnabled', 'Banking: ', 1)
            ->setValue($node->bankingEnabled);
        $this->addCheckbox('bankingRequired', 'Banking is required: ', 1)
            ->setValue($node->bankingRequired);
        $this->add('AsmSelect', 'bankingOptions', 'Options:')
            ->title('--- Select --')
            ->setValue($node->bankingOptions)
            ->setOptions(CompanyCustomer::$availableBankingOptions);
        $this->addText('cashBackAmount', 'Cash Back Amount')
            ->setValue($node->getCashBackAmount());

        $this->addFieldset('Basket');
        $this->addCheckbox('removableFromBasket', 'Can be removed from basket: ', 1)
            ->setValue($node->isRemovableFromBasket());
        $this->addArea('removeFromBasketConfirmation', 'Remove from basket confirmation: ')
            ->cols(50)->rows(3)
            ->setDescription('(Confirmation message will appear before removing product from basket. No confirmation popup if empty)')
            ->setValue($node->getRemoveFromBasketConfirmation());

        $this->addFieldset('Service');
        $this->addSelect('serviceTypeId', 'Service Type: ', $this->serviceTypes)
            ->setFirstOption('--- None ---')
            ->setValue($this->node->serviceTypeId);
        $this->addSelect('serviceDuration', 'Service Duration:', $this->serviceDurations)
            ->setFirstOption('--- Select ---')
            ->setValue($this->node->getDuration());
        $this->addSelect('renewalProductId', 'Renewal Product:', $this->products)
            ->setFirstOption('--- Select ---')
            ->setValue($this->node->renewalProductId)
            ->addRule([$this, 'Validator_serviceRequiredElement'], 'Required!', [$this['serviceDuration']]);
        $this->addCheckbox('isAutoRenewalAllowed', 'Auto Renewal Allowed:', 1)
            ->setDescription('(Service can be set to be renewed automatically)')
            ->setValue($node->isAutoRenewalAllowed);

        $this->addFieldset('ID check');
        $this->addCheckbox('isIdCheckRequired', 'ID check required:', 1)
            ->setValue($node->isIdCheckRequired);

        // properties
        $this->addFieldset('Properties');
        $this->addSelect('conditionedById', 'Product Required:', $this->products)
            ->setDescription('(The product selected from the drop down list must be in the basket for a user to add this product)')
            ->setFirstOption('--- Select ---')
            ->setValue($node->conditionedById);
        $this->addCheckbox('onlyOneItem', 'Only 1 Item:', 1)
            ->setDescription('(Absolutely only 1 of this product in the basket)')
            ->setValue($node->onlyOneItem);
        $this->addCheckbox('maxQuantityOne', 'Max Quantity 1:', 1)
            ->setDescription('(The product can be in the basket multiple times, but the quantity of each can only be 1)')
            ->setValue($node->maxQuantityOne);

        // company number
        $this->addFieldset('Company number');
        $this->addCheckbox('requiredCompanyNumber', 'Company Required:', 1)
            ->setDescription('(The user will be required to choose a company from their account or manually enter the company number that is to be associated with this product)')
            ->setValue($node->requiredCompanyNumber);

        $this->addCheckbox('onlyOurCompanies', 'On Account:', 1)
            ->setDescription('(This product can only be purchased if the user has the company on their account)')
            ->setValue($node->onlyOurCompanies);

        $this->addCheckbox('requiredIncorporatedCompanyNumber', 'Incorporated On Account:', 1)
            ->setDescription('(This product can only be purchased if the user incorporated the company with us.)')
            ->setValue($node->requiredIncorporatedCompanyNumber);

        // other
        $this->addFieldset('Other');
        $this->addArea('responsibleEmails', 'Email CMS: ')
            ->cols(50)->rows(3)
            ->setDescription('(Email addresses entered in this field will be sent an email notifcation of every purchase of this product. Multiple email address must be comma seperated eg. email1@address.com, email2@address.com)')
            ->setValue($node->responsibleEmails)
            ->addRule([$this, 'Validator_CommaEmails'], 'Emails are not valid!');
        $this->addCheckbox('notApplyVat', 'No VAT:', 1)
            ->setDescription('(If ticked, VAT will not be applied to this product.)')
            ->setValue($node->notApplyVat);
        $this->addText('nonVatableValue', 'Non Vatable Value:')
            ->setDescription('(VAT will not be applied to the amount entered here. The standard VAT rate will be applied to the remainder of the product price.)')
            ->size(3)
            ->addRule(FForm::NUMERIC, 'Not vatable value has to be number!')
            ->setValue($node->nonVatableValue);
        $this->add('CmsFckArea', 'emailText', 'Email Text:')
            ->setSize(500, 200)
            ->setToolbar('Basic')
            ->setValue($node->emailText);
        $this->addArea('basketText', 'Basket text:')->cols(40)->rows(5)
            ->setValue($node->basketText);
        $this->addCheckbox('lockCompany', 'Lock Company:', 1)
            ->setDescription('(Package: customer can not do anything with company)')
            ->setValue($node->lockCompany);


        $this->addFieldset('Associated Product Image');
        $this->add('CmsImg', 'associatedImageId', 'Associated Product Image')
            ->setNodeId(self::ASSOCIATED_IMAGE_PAGE)
            ->setValue($node->associatedImageId);
        $this->addArea('associatedText', 'Associated Product Text:')->cols(40)->rows(5)
            ->setValue($node->associatedText);
        $this->addArea('associatedDescription', 'Associated Product Description:')->cols(40)->rows(5)
            ->setValue($node->associatedDescription);
        $this->addText('associatedIconClass', 'Icon class')
            ->setDescription('(Font awesome class used as product icon (for example "fa-folder-open"))')
            ->setValue($node->associatedIconClass);

        // attachment
        $this->addFieldset('Email Attachments	');
        for ($i = 0; $i < self::ATTACHMENTS_COUNT; $i++) {
            $this->add('CmsFile', 'attachment' . $i, 'Attachment ' . ($i + 1) . ':')
                ->setNodeId(self::ATTACHMENT_FILE_PAGE);
            //set value
            if (isset($node->emailCmsFileAttachments[$i])) {
                $fileId = $node->emailCmsFileAttachments[$i];
                $this['attachment' . $i]->setValue($fileId);
            }
        }

        // upgrade page popup
        $this->addFieldset('Upgrade Page Popup');
        $this->add('CmsFckArea', 'upgradeDescription', 'Upgrade Description:')
            ->setSize(500, 200)
            ->setValue($node->upgradeDescription);
        $this->add('CmsImg', 'upgradeImageId', 'Upgrade Image')
            ->setNodeId(self::UPDATE_IMAGE_PAGE)
            ->setValue($node->upgradeImageId);

        $this->addFieldset('VO Service');
        $this->addCheckbox('isVoServiceEligible', 'Enabled', 1)
            ->setDescription('(Enabling this will automatically create this service on VO after purchase)')
            ->setValue($node->isVoServiceEligible());
        $this->addSelect('voServiceDurationInMonths', 'Duration:', BasketProduct::$voServiceDurationsInMonths)
            ->setFirstOption('--- Select ---')
            ->addRule([$this, 'Validator_voServiceRequired'], 'Required!', ['isVoServiceEligible' => $this['isVoServiceEligible']])
            ->setValue($node->getVoServiceDurationInMonths());

        // action
        $this->addFieldset('Action');
        $this->addSubmit('submit', 'Submit')
            ->onclick('changes = false')
            ->class('btn');
    }

    /**
     * @return array
     */
    private static function getInfoPagesDropdown()
    {
        static $dropdown = NULL;
        if ($dropdown === NULL) {
            $pages = FNode::getChildsNodes(self::PAGES_FOLDER);
            /* @var $page FNode */
            foreach ($pages as $pageId => $page) {
                $dropdown[$pageId] = $page->getLngTitle();
            }
        }
        return $dropdown;
    }

}
