<?php

class ChangeCompanyNameAdminForm  extends FForm
{
    /**
     * @var CUChangeNameControler 
     */
    private $controler;
    
    /**
     * @var array
     */
    private $callback;
    
    /**
     * @var Company
     */
    private $company;
    
    /**
     * @var Customer
     */
    private $customer;

    /**
     * @param CUAdminControler $controler
     * @param array $callback
     */
    public function startup(CUAdminControler $controler, array $callback, $company,Customer  $customer)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->company = $company;
        $this->customer = $customer;
        $this->init();
    }

    /**
	 * @return void
	 */
    private function init()
    {
        $this->addFieldset('Change of name'); 
        
        $this->add('RadioInline','changeNameId', 'Product name: ')
             ->setOptions(CompanyNameChangeServiceModel::$changeCompanyNameProducts)
             ->addRule(FForm::Required, 'Please provide type!');
        
        
        $this->addText('newCompanyName','New Company Name')
			->style('width: 300px;')
			//->addRule(array($this, 'Validator_companyName'), array('Name can\'t be empty', 'Name is already in use.'))
			->addRule(array($this, 'Validator_companyNameChars'), 'Name is invalid');
			//->addRule(array($this, 'Validator_companyNameSuffix'), array('Please add LTD or LIMITED suffix to company name','Please add PLC suffix to company name','Please add PLC or LTD suffix to company name'));
        
        $this->add('RadioInline','radio', 'Suffix: ')
            ->setOptions(CompanyNameChangeServiceModel::getSuffixOptions($this->company->getType()))
            ->addRule(array($this, 'Validator_suffix'), 'Suffix is Required');
        
        $this->addSubmit('send', 'Send Company Name Change')->class('btn')->style('width: 200px; height: 30px;')
			->onclick('return confirm("ONLY send this to Companies House if you have written approval from the client")');

        $this->onValid = $this->callback;
	    $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $fields = array();
            
            $data = $this->getValues();
            
            $fields['methodOfChange'] = CompanyNameChangeServiceModel::setCompanyChangeNameMethodOfChange($this->company);
            $fields['newCompanyName'] = htmlentities(strtoupper($data['newCompanyName']) . ' ' .$data['radio'] , ENT_NOQUOTES, 'UTF-8');
            $fields['meetingDate'] = date('Y-m-d') ;
            $fields['sameDay'] = CompanyNameChangeServiceModel::setAdminCompanyChangeNameSameDay($data['changeNameId']) ;
            $fields['noticeGiven'] = TRUE;
            //save company name change type and order
            CompanyNameChangeServiceModel::addCompanyNameChangeToCustomerCompany($this->customer, $this->company, $data);
            $this->company->sendChangeCompanyName($fields);
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    /**
     * @param FFORM RadioInLine $control
     * @param array $error
     * @param array $params
     * @return error or true
     */
    public function Validator_suffix($control, $error, $params)
	{
		if ($control->owner->isSubmitedBy('back') == FALSE) {
			try {
				if (
                    ($this->company->getType() != 'BYGUAREXUNDSEC60') 
                    && ($control->getValue() == '')
                    ) {
					return $error;
				}
			} catch (Exception $e) {
				return $e->getMessage();
			}
		}
		return TRUE;
	}
    
    
    
    /**
     * @param FFORM Text $control
     * @param array $error
     * @param array $params
     * @return error or true
     */
    public function Validator_companyName($control, $error, $params)
	{
		if ($control->owner->isSubmitedBy('back') == FALSE) {
			try {
				if ($control->getValue() == '') {
					return $error[0];
				}
				$available = CompanySearch::isAvailable($control->getValue());
				return ($available === TRUE) ? TRUE : $error[1];
			} catch (Exception $e) {
				return $e->getMessage();
			}
		}
		return TRUE;
	}

    /**
     * @param FFORM Text $control
     * @param array $error
     * @param array $params
     * @return error or true
     */
	public function Validator_companyNameChars(Text $control, $error, $params)
	{
		if (preg_match("/^[-,.:;a-zA-Z0-9\/&$\\\@£*?'\"******?!{}<>()\[\] ]{3}[-,.:;a-zA-Z0-9\/&$\\\@£*?'\"******?!{}<>()\[\]*=#%+ ]{0,157}+$/", $control->getValue())) {
			return TRUE;
		}
		return $error;
	}
    
    /**
     * @param FFORM Text $control
     * @param array $error
     * @param array $params
     * @return error or true
     */
	public function Validator_companyNameSuffix(Text $control, $error, $params)
	{
		$type = $this->company->getType();
		if (!preg_match('/( PLC| LTD| LIMITED| PLC.| LTD.| LIMITED. | CYFYNGEDIG )$/', strtoupper(trim($control->getValue())))) {

		/* by shares and by guarantee */
		if ($type == 'BYSHR' || $type == 'BYGUAR') {
			return $error[0];
		/* plc */
		} elseif ($type == 'PLC') {
			return $error[1];
		}
			return $error[2];
		}
		return TRUE;
    }    
    
}
