<?php

namespace Admin\Wholesale;

use Exception;
use FForm;
use Product;
use WholesaleSearchNode;

class SearchForm extends FForm
{
    /**
     * @var ISearchDelegate
     */
    private $delegate;

    /**
     * @var WholesaleSearchNode
     */
    private $node;

    /**
     * @var Product[] array
     */
    private $products = array();

    /**
     * @param ISearchDelegate $delegate
     * @param WholesaleSearchNode $node
     * @param array $products
     */
    public function startup(ISearchDelegate $delegate, WholesaleSearchNode $node, array $products)
    {
        $this->delegate = $delegate;
        $this->node = $node;
        $this->products = $products;

        $this->buildForm();
        $this->onValid = array($this, 'process');
        $this->start();
    }

    public function process()
    {
        try {
            $data = $this->getValues();
            $this->node->setPackagesIds($data['packagesIds']);
            $this->node->save();

            $this->clean();
            $this->delegate->wholesaleSearchFormSucceeded();

        } catch (Exception $e) {
            $this->delegate->wholesaleSearchFormFailed($e);
        }
    }

    private function buildForm()
    {
        $this->addFieldset('Packages');
        $this->add('AsmSelect', 'packagesIds', 'Included:')
            ->title('--- Select --')
            ->setOptions($this->products)
            ->addRule(self::Required, 'Please provide package')
            ->setValue($this->node->getPackagesIds());

        $this->addFieldset('Action');
        $this->addSubmit('submit', 'Submit')
            ->class('btn');

    }

}