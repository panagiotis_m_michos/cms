<?php

namespace Admin\Wholesale;

use Exception;

interface ISearchDelegate
{
    public function wholesaleSearchFormSucceeded();

    /**
     * @param Exception $e
     */
    public function wholesaleSearchFormFailed(Exception $e);
}