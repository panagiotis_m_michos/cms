<?php

class PackageForm extends FForm
{
    /**
     * @var Package
     */
    private $node;

    /**
     * @param IPackage $node
     */
    public function startup(IPackage $node)
    {
        $this->node = $node;
        $this->buildForm();
        $this->start();
    }

    public function buildForm()
    {
        // fieldset
        $this->addFieldset('Package');
        $this->addText('title', 'Title: * ')
            ->addRule(FForm::Required, 'Title is required!')
            ->size(30)
            ->setValue($this->node->page->title);
        $this->addText('alternativeTitle', 'Alternative Title:')
            ->size(50)
            ->setValue($this->node->page->alternativeTitle);


        $this->addArea('upgradeText', 'Why upgrade:')
            ->cols(59)->rows(5)
            ->setToolbar('Basic')
            ->setValue($this->node->upgradeText);
        $this->add('CmsFckArea', 'abstract', 'Description:')
            ->setSize(500, 100)
            ->setToolbar('Basic')
            ->setValue($this->node->page->abstract);
        $this->add('CmsFckArea', 'text', 'Page Text:')
            ->setSize(500, 300)
            ->setValue($this->node->page->text);

        // price	
        $this->addFieldset('Price');
        $this->addText('price', 'Price: *')
            ->size(5)
            ->setValue($this->node->price)
            ->addRule(FForm::Required, 'Please provide price')
            ->addRule('Numeric', 'Price has to be number');
        $this->addText('productValue', 'Product value: *')
            ->size(5)
            ->setValue($this->node->productValue)
            ->addRule(FForm::Required, 'Please provide product value')
            ->addRule('Numeric', 'Price value has to be number');
        $this->addText('associatedPrice', 'Upgrade price: *')
            ->size(5)
            ->setValue($this->node->associatedPrice)
            ->addRule(FForm::Required, 'Please provide associated price')
            ->addRule('Numeric', 'Associated price value has to be number');
        $this->addText('wholesalePrice', 'Wholesale price: *')
            ->size(5)
            ->setValue($this->node->wholesalePrice)
            ->addRule(FForm::Required, 'Please provide wholesale price')
            ->addRule('Numeric', 'Wholesale price value has to be number');
        $this->addText('offerPrice', 'Offer price: *')
            ->size(5)
            ->setValue($this->node->getOfferPrice())
            ->addRule(FForm::Required, 'Please provide offer price')
            ->addRule('Numeric', 'Offer price value has to be number');

        // other
        $this->addFieldset('Other');
        $this->addSelect('typeId', 'Type: *', CompanyIncorporation::$types)
            ->setFirstOption('--- Select ---')
            ->setValue($this->node->typeId)
            ->addRule(FForm::Required, 'Please provide Type!');
        $this->addSelect('upgradeProductId', 'Upgrade product:', FMenu::getSelect($this->node->parentId))
            ->setFirstOption('--- Select ---')
            ->setValue($this->node->upgradeProductId);
        $this->addCheckbox('isInternational', 'International Package:', 1)
            ->setValue($this->node->isInternational());

        // action
        $this->addFieldset('Action');
        $this->addSubmit('submit', 'Submit')
            ->onclick('changes = false')
            ->class('btn');
    }

}
