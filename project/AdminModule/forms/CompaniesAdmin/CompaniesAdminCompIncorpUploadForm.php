<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS
 * @author 		Nikolai Senkevich, Stan Bazik
 * @internal 	project/forms/Admin/CompaniesAdminCompIncorpUploadForm.php
 * @created 	10/10/2011
 */
class CompaniesAdminCompIncorpUploadForm extends FForm
{

    /**
     * @var CompaniesAdminControler
     */
    private $controler;
    /**
     * @var array
     */
    private $callback;
    /**
     *
     * @var Company 
     */
    private $company;

    /**
     * @param CompaniesAdminControler $controler
     * @param array $callback
     */
    public function startup(CompaniesAdminControler $controler, array $callback, $company)
    {

        $this->controler = $controler;
        $this->callback = $callback;
        $this->company = $company;

        $this->init();
    }

    private function init()
    {
        // adddress
        $this->addFieldset('uploaad');
        $this->addFile('incCert', 'Incorporation Certificate ')
            ->addRule(FForm::MIME_TYPE, 'Only pdf is supported for Support document', MimeType::PDF)
            ->setDescription('(Must be in PDF)');
        $this->addFile('memArticle', 'Memorandum Articles')
            ->addRule(FForm::MIME_TYPE, 'Only pdf is supported for Support document', MimeType::PDF)
            ->setDescription('(Must be in PDF)');

        // action
        $this->addFieldset('Action');
        $this->addSubmit('submit', 'Submit')
            ->class('btn')->style('width: 200px; height: 30px;');

        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();
            $incCert = $data['incCert'];
            $memArticle = $data['memArticle'];
            
            $docPath = CHFiling::getDocPath() . '/' . CHFiling::getDirName($this->company->getCompanyId()) . '/company-' . $this->company->getCompanyId() . '/documents/';

            // --- create folder if needed ---
            if (!is_dir($docPath)) {
                mkdir($docPath, 0777, true);
            }
            
            // --- incorporation certificate ---
            if (isset($incCert)) {
                $incCert['name'] = 'IncorporationCertificate.pdf';
                file_put_contents($docPath . 'IncorporationCertificate.pdf', file_get_contents($incCert['tmp_name']));
                
            }
            
            // --- mem Article ---
            if (isset($memArticle)) {
                $memArticle['name'] = 'article.pdf';
                file_put_contents($docPath . 'article.pdf', file_get_contents($memArticle['tmp_name']));
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

}