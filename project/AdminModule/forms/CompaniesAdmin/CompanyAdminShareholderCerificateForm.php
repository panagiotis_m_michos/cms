<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    CompanyAdminShareholderCerificateForm.php 2011-11-04 razvanp@madesimplegroup.com
 */
class CompanyAdminShareholderCerificateForm extends FForm
{

    /**
     * @var CompanyShareholderCerificate
     */
    private $controler;
    
    /**
     * @var array
     */
    private $callback;
    
    /**
     *
     * @var Subscriber 
     */
    private $shareholder;

    /**
     * @param CompaniesAdminControler $controler
     * @param array $callback
     */
    public function startup(CompanyShareholderCerificateAdminControler $controler, array $callback, $shareholder)
    {

        $this->controler = $controler;
        $this->callback = $callback;
        $this->shareholder = $shareholder;
        $this->init();
    }

    private function init()
    {
        // adddress
        $this->addFieldset('Address');
        $this->addText('premise', 'Building name/number *')
            ->addRule(FForm::Required, 'Please provide Building name/number')
            ->addRule('MaxLength', "Building name/number can't be more than 50 characters", 50);
        $this->addText('street', 'Street *')
            ->addRule(FForm::Required, 'Please provide Street')
            ->addRule('MaxLength', "Street can't be more than 50 characters", 50);
        $this->addText('thoroughfare', 'Address 3')
            ->addRule('MaxLength', "Address 3 can't be more than 50 characters", 50);
        $this->addText('post_town', 'Town *')
            ->addRule(FForm::Required, 'Please provide Town')
            ->addRule('MaxLength', "Town can't be more than 50 characters", 50);
        $this->addText('county', 'County')
            ->addRule('MaxLength', "County can't be more than 50 characters", 50);
        $this->addText('postcode', 'Postcode *')
            ->addRule(array($this, 'Validator_postcode'), 'You cannot use our postcode for this address')
            ->addRule(FForm::Required, 'Please provide Postcode')
            ->addRule('MaxLength', "Postcode can't be more than 15 characters", 15);
        $this->addSelect('country', 'Country *', Address::$countries)
            ->addRule(FForm::Required, 'Please provide Country');


        // action
        $this->addFieldset('Action');
        $this->addSubmit('submit', 'Submit')
            ->class('btn')->style('width: 200px; height: 30px;');
        
        $this->setInitValues($this->shareholder->shareholderInfo);
        
        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();

            $fields['premise'] = $data['premise'];
            $fields['street'] = $data['street'];
            $fields['thoroughfare'] = $data['thoroughfare'];
            $fields['post_town'] = $data['post_town'];
            $fields['county'] = $data['county'];
            $fields['postcode'] = $data['postcode'];
            $fields['country'] = $data['country'];
            
            $this->shareholder = $this->controler->node->updateShareHolderData($this->shareholder, $data);
            $this->controler->node->updateShareHolderAddress($fields,$this->shareholder->shareholderInfo->company_member_id);

            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Validate postcode for UK
     *
     * @param Text $control
     * @param string $error
     * @param array $params
     * @return boolean
     */
    static public function Validator_postcode(Text $control, $error, $params)
    {
        $value = $control->getValue();
        $countryId = $control->owner['country']->getValue();
        if ($countryId == Customer::UK_CITIZEN && (!preg_match('#^(GIR 0AA|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]|[A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y]))|[0-9][A-HJKPS-UW]) [0-9][ABD-HJLNP-UW-Z]{2})$#i', $value) || !preg_match('#^([A-Z][\dA-Z]{1,3}[\s][\d][A-Z]{2,2})$#i', $value))) {
            return $error;
        }
        return TRUE;
    }

}