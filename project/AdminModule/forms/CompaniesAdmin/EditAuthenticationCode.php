<?php

class EditAuthenticationCode extends FForm
{
    /**
     * @var FControler 
     */
    public $controller;
    
    /**
     * @var Company 
     */
    public $company;
    
    /**
     * @var array 
     */
    public $callback;
    
    /**
     * @param FControler $controller
     * @param array $callback
     * @param Company $company
     */
    public function startup(FControler $controller, array $callback, Company $company)
    {
        $this->controller = $controller;
        $this->company = $company;
        $this->callback = $callback;
        $this->init();
    }

    private function init()
    {
        $this->addFieldset('Company Details');
        $this->addText('authenticationCode', 'Authentication Code: ')
            ->addRule(FForm::Required, 'Please provide company name')
            ->addRule(FForm::MIN_LENGTH, 'Authentication code has to consist of 6 characters!', 6)
            ->setValue($this->company->getAuthenticationCode());

        $this->addText('companyNumber', 'Company Number: ')
            ->addRule(FForm::Required, 'Please provide company number')
            ->addRule(FForm::MIN_LENGTH, 'Company number has to consist of 8 characters!', 8)
            ->setValue($this->company->getCompanyNumber());

        $this->addFieldset('Action');
        $this->addSubmit('change', 'Change')->class('btn');

        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();
            $this->company->setAuthenticationCode($data['authenticationCode']);
            $this->company->setCompanyNumber($data['companyNumber']);
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

}
