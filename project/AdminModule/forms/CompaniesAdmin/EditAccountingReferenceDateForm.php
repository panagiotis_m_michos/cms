<?php

class EditAccountingReferenceDateForm extends FForm
{

    /**
     * @var FControler 
     */
    public $controller;

    /**
     * @var Company 
     */
    public $company;

    /**
     * @var array 
     */
    public $callback;

    /**
     * @param FControler $controller
     * @param array $callback
     * @param Company $company
     */
    public function startup(FControler $controller, array $callback, Company $company)
    {
        $this->controller = $controller;
        $this->company = $company;
        $this->callback = $callback;
        $this->init();
    }

    private function init()
    {
        // date
        $this->addFieldset('New Accounting Reference Date ');
        $this->add('DateSelect', 'date', 'Date *')
            ->addRule(FForm::Required, 'Please provide date!.')
            ->addRule(array('CHValidator', 'Validator_futureDateDateSelect'), 'Date has to be in future!.')
            ->setStartYear(date('Y'))
            ->setEndYear(date('Y') + 10)
            ->setValue(date('Y-m-d'));

        // action
        $this->addFieldset('Action');
        $this->addSubmit('send', 'Send to Companies house');

        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $this->company->sendChangeOfAccountingReferenceDate($this->getValue('date'));
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

}
