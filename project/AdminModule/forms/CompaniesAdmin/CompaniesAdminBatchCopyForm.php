<?php

class CompaniesAdminBatchCopyForm extends FForm
{

    public function startup()
    {
        $this->buildForm();
        $this->start();
    }

    private function buildForm()
    {
        $this->addFieldset('Companies name CSV File');
        $this->addFile('csv', 'Choose File');
        //  ->addRule(FForm::MIME_TYPE, 'Only .csv is supported', MimeType::CSV);
        $this->addSubmit('submit', 'Submit')->class('btn');
    }

    public function getFileName()
    {
        $data = $this->getValues();
        return $filename = $data['csv']['tmp_name'];
    }

}
