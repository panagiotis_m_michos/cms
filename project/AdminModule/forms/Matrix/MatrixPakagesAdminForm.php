<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    MatrixPakagesAdminForm.php 2012-03-13 razvanp@madesimplegroup.com
 */

class MatrixPakagesAdminForm  extends FForm
{
    /**
     * @var MatrixAdminControler 
     */
    private $controler;
    /**
     * @var array
     */
    private $callback;
    
    /**
     *
     * @var MatrixModel 
     */
    private $node;

    /**
     * @param FeedBacksAdminControler $controler
     * @param array $callback
     */
    public function startup(MatrixAdminControler $controler, array $callback, MatrixModel $node)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->node = $node;
        $this->init();
    }

    /**
	 * @return void
	 */
    private function init()
    {
        
        $this->addFieldset('Associated Pakages ');
		$this->add('AsmSelect', 'matrixPakages', 'Products:')
			->multiple()
			->title('--- Select --')
            ->setValue($this->node->getMatrixPakages())    
			->setOptions(MatrixModel::getAllPackages());
			//->setValue($this->node->associatedProducts2)
			//->addRule(array($this, 'Validator_AssociatedProducts'), 'Associated product 1 and 2 has to be different!');
        
        $this->addFieldset('Submit');
		$this->addSubmit('submitPakages', 'Submit')->class('btn');
        $this->onValid = $this->callback;
	    $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();
            $this->node->setMatrixPakages($data['matrixPakages']);
            $this->node->save();
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function Validator_AssociatedProducts($control, $error, $params)
	{
		if ($control->getValue() && $control->owner['associatedProducts1']->getValue()) {
			$result = array_intersect($control->getValue(), $control->owner['associatedProducts1']->getValue());
			if (!empty($result)) {
				return $error;
			}
		}
		return TRUE;
	}
    
}
