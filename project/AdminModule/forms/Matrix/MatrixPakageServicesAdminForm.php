<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    MatrixPakagesAdminForm.php 2012-03-13 razvanp@madesimplegroup.com
 */

class MatrixPakageServicesAdminForm  extends FForm
{
    /**
     * @var MatrixAdminControler 
     */
    private $controler;
    /**
     * @var array
     */
    private $callback;
    
    /**
     *
     * @var MatrixServicesModel 
     */
    private $node;

    /**
     * @param FeedBacksAdminControler $controler
     * @param array $callback
     */
    public function startup(MatrixServicesAdminControler $controler, array $callback, MatrixServicesModel $node)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->node = $node;
        $this->init();
    }

    /**
	 * @return void
	 */
    private function init()
    {
        
        $matrixPakageNrOfYears = json_decode($this->node->getMatrixPakageNrOfYears(),true);
        $firstText = json_decode($this->node->getFirstText(),true);
        
        //pr($matrixPakageNrOfYears);die;
        $this->addFieldset('Pakages ');
		$this->add('AsmSelect', 'serviceAvailableForPakage', 'Available For:')
			->multiple()
			->title('--- Select --')
            ->setValue($this->node->getServiceAvailableForPakage())    
			->setOptions(MatrixModel::getAllPackages());
			//->setValue($this->node->associatedProducts2)
			//->addRule(array($this, 'Validator_AssociatedProducts'), 'Associated product 1 and 2 has to be different!');
        
        $this->addFieldset('Matrix Pakage Properties ');
        
        $this->addText('summaryPakageName', 'Summary Name:')
             ->setDescription('(Summary Pakage Name)')
                ->size(50)
			->setValue($this->node->getSummaryPakageName());
        
        $this->addText('packageTooltipText', 'Tooltip Text:')
             ->setDescription('(Summary Pakage Name)')
                ->size(50)
			->setValue($this->node->getPackageTooltipText());
        
        
        $this->addFieldset('Pakage Number Of Years');
        $this->addText(Package::PACKAGE_BRONZE, 'Bronze:')
             ->setDescription('(Available Years)')
                ->size(10)
			->setValue($matrixPakageNrOfYears ? $matrixPakageNrOfYears[Package::PACKAGE_BRONZE] : NULL);
        
        $this->addText(Package::PACKAGE_BRONZE.'extra', '')
             ->setDescription('(Extra Text)')
                ->size(10)
			->setValue($firstText ? $firstText[Package::PACKAGE_BRONZE] : NULL);
        
        
        $this->addText(Package::PACKAGE_BRONZE_PLUS, 'Bronze Plus:')
             ->setDescription('(Available Years)')
                ->size(10)
			->setValue($matrixPakageNrOfYears ? $matrixPakageNrOfYears[Package::PACKAGE_BRONZE_PLUS] : NULL);
        $this->addText(Package::PACKAGE_BRONZE_PLUS.'extra', '')
             ->setDescription('(Extra Text)')
                ->size(10)
			->setValue($firstText ? $firstText[Package::PACKAGE_BRONZE_PLUS] : NULL);
        
        
        
          $this->addText(Package::PACKAGE_SILVER, 'Silver:')
             ->setDescription('(Available Years)')
                ->size(10)
			->setValue($matrixPakageNrOfYears ? $matrixPakageNrOfYears[Package::PACKAGE_SILVER] : NULL);
          $this->addText(Package::PACKAGE_SILVER.'extra', '')
             ->setDescription('(Extra Text)')
                ->size(10)
			->setValue($firstText ? $firstText[Package::PACKAGE_SILVER] : NULL);
          
          
           $this->addText(Package::PACKAGE_GOLD, 'Gold:')
             ->setDescription('(Available Years)')
                ->size(10)
			->setValue($matrixPakageNrOfYears ? $matrixPakageNrOfYears[Package::PACKAGE_GOLD] : NULL);
           $this->addText(Package::PACKAGE_GOLD.'extra', '')
             ->setDescription('(Extra Text)')
                ->size(10)
			->setValue($firstText ? $firstText[Package::PACKAGE_GOLD] : NULL);
           
           
            $this->addText(Package::PACKAGE_PLATINUM, 'Platinum:')
             ->setDescription('(Available Years)')
                ->size(10)
			->setValue($matrixPakageNrOfYears ? $matrixPakageNrOfYears[Package::PACKAGE_PLATINUM] : NULL);
            $this->addText(Package::PACKAGE_PLATINUM.'extra', '')
             ->setDescription('(Extra Text)')
                ->size(10)
			->setValue($firstText ? $firstText[Package::PACKAGE_PLATINUM] : NULL);
            
            $this->addText(Package::PACKAGE_DIAMOND, 'Diamond:')
             ->setDescription('(Available Years)')
                ->size(10)
			->setValue($matrixPakageNrOfYears ? $matrixPakageNrOfYears[Package::PACKAGE_DIAMOND] : NULL);
            $this->addText(Package::PACKAGE_DIAMOND.'extra', '')
             ->setDescription('(Extra Text)')
                ->size(10)
			->setValue($firstText ? $firstText[Package::PACKAGE_DIAMOND] : NULL);
        
        $this->addFieldset('Pakage Service Properties');
        
        $this->addCheckbox('tickImage', 'Tick Image: ',1)
            ->setDescription('(If is ticked display tick image)')    
            ->setValue($this->node->getTickImage());
        
//        $this->addText('firstText', 'First Text: ')
//			->setValue($this->node->getFirstText());
//			//->addRule(FForm::Required, 'Please provide product value')
//			//->addRule('Numeric','Price value has to be number');
//		$this->addText('seccondText', 'SeccondText')
//			->setValue($this->node->getSeccondText());
//			//->addRule(FForm::Required, 'Please provide associated price')
//			//->addRule('Numeric','Associated price value has to be number');
        
        $this->addFieldset('Submit');
		$this->addSubmit('submitPakages', 'Submit')->class('btn');
        $this->onValid = $this->callback;
	    $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();
            $firstText = json_encode(array(
                Package::PACKAGE_BRONZE => $data['109extra'],
                Package::PACKAGE_BRONZE_PLUS => $data['110extra'],
                Package::PACKAGE_GOLD => $data['112extra'],
                Package::PACKAGE_SILVER => $data['111extra'],
                Package::PACKAGE_PLATINUM => $data['113extra'],
                Package::PACKAGE_DIAMOND => $data['279extra'],
            ));
            
            $matrixPakageNrOfYears = json_encode( array(
                    Package::PACKAGE_BRONZE => $this->getValue(Package::PACKAGE_BRONZE),
                    Package::PACKAGE_BRONZE_PLUS => $this->getValue(Package::PACKAGE_BRONZE_PLUS),
                    Package::PACKAGE_GOLD => $this->getValue(Package::PACKAGE_GOLD),
                    Package::PACKAGE_SILVER => $this->getValue(Package::PACKAGE_SILVER),
                    Package::PACKAGE_PLATINUM => $this->getValue(Package::PACKAGE_PLATINUM),
                    Package::PACKAGE_DIAMOND => $this->getValue(Package::PACKAGE_DIAMOND),
                    ));
            $this->node->setServiceAvailableForPakage($data['serviceAvailableForPakage']);
            $this->node->setSummaryPakageName($data['summaryPakageName']);
            $this->node->setPackageTooltipText($data['packageTooltipText']);
            $this->node->setMatrixPakageNrOfYears($matrixPakageNrOfYears);
            $this->node->setTickImage($data['tickImage']);
            $this->node->setFirstText($firstText);
            $this->node->setSeccondText('test');
            $this->node->save();
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function Validator_AssociatedProducts($control, $error, $params)
	{
		if ($control->getValue() && $control->owner['associatedProducts1']->getValue()) {
			$result = array_intersect($control->getValue(), $control->owner['associatedProducts1']->getValue());
			if (!empty($result)) {
				return $error;
			}
		}
		return TRUE;
	}
    
}
