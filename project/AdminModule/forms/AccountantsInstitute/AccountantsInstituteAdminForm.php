<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS
 * @author 		Nikolai Senkevich, Stan Bazik
 * @internal 	project/forms/AccountantsInstitute/AccountantsInstituteAdminForm.php
 * @created 	28/03/2011
 */
class AccountantsInstituteAdminForm extends FForm
{

    /**
     * @var AccountantsInstituteAdminControler
     */
    private $controler;
    /**
     * @var array
     */
    private $callback;
    /**
     * @var AccountantsInstituteModel
     */
    public $node;

    /**
     * @param AccountantsInstituteAdminControler $controler
     * @param array $callback
     */
    public function startup(AccountantsInstituteAdminControler $controler, AccountantsInstituteModel $node, array $callback)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->node = $node;
        $this->init();
    }

    private function init()
    {
        $this->addFieldset('Action');
        $this->addArea('homeSlideShow', 'Slide show')
            ->cols(50)->rows(10)
            ->setValue($this->node->homeSlideShow);
        $this->add('CmsImg', 'templateRightImageId', 'Template Image')
            ->setValue($this->node->templateRightImageId);

        $this->addFieldset('Action');
        $this->addSubmit('submit', 'Submit')
            ->class('btn')->style('width: 200px; height: 30px;');

        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();

            // --- save admin properties ---
            $this->node->setHomeSlideShow($data['homeSlideShow']);
            $this->node->setTemplateRightImageId($data['templateRightImageId']);
            $this->node->save();

            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

}