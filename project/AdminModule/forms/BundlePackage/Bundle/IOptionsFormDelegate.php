<?php

namespace Admin\BundlePackage\Bundle;

use Exception;

interface IOptionsFormDelegate
{
    public function bundlePackageOptionsFormSucceeded();

    /**
     * @param Exception $e
     */
    public function bundlePackageOptionsFormFailed(Exception $e);
}

