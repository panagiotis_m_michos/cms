<?php

class AddAnnualReturn extends FForm
{

    /**
     * @var FControler 
     */
    public $controller;

    /**
     * @var Company 
     */
    public $company;

    /**
     * @var Customer 
     */
    public $customer;

    /**
     * @var array 
     */
    public $callback;

    /**
     * @param FControler $controller
     * @param array $callback
     * @param Company $company
     * @param Customer $customer
     */
    public function startup(FControler $controller, array $callback, Company $company, Customer $customer)
    {
        $this->controller = $controller;
        $this->company = $company;
        $this->customer = $customer;
        $this->callback = $callback;
        $this->init();
    }

    private function init()
    {
        $this->setRenderClass('Default2Render');
        // data
        $this->addFieldset('Data');
        $this->addArea('additional', 'Additional info: ')->cols(30)->rows(5);
        $this->addRadio('productId', 'Product: ', array(AnnualReturn::ANNUAL_RETURN_FEE_PRODUCT => 'Confirmation Statement (DIY)', AnnualReturn::ANNUAL_RETURN_SERVICE_PRODUCT => 'Confirmation Statement Service'))
            ->addRule(FForm::Required, 'Please provide type!');

        // action
        $this->addFieldset('Action');
        $this->addSubmit('add', 'Add Confirmation Statement')->class('btn')->style('width: 200px; height: 30px;');

        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();
            AnnualReturnServiceModel::addAnnualReturnToCustomerCompany($this->customer, $this->company, $data);
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

}
