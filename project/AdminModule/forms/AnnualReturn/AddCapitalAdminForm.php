<?php

use Services\AnnualReturnService;
use Entities\CapitalShare;
use Entities\Capital;
use ValueObject\Currency;

class AddCapitalAdminForm extends FForm
{

    /**
     * @var ARAdminControler
     */
    private $controler;

    /**
     * @var array
     */
    private $callback;

    /**
     * @var int
     */
    public $formSubmissionId;

    /**
     * @var AnnualReturnService
     */
    protected $arService;

    /**
     *
     * @param ARAdminControler $controler
     * @param int $formSubmissionId
     * @param array $callback
     * @param AnnualReturnService $arService
     */
    public function startup(ARAdminControler $controler, $formSubmissionId, array $callback, AnnualReturnService $arService)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->formSubmissionId = $formSubmissionId;
        $this->arService = $arService;
        $this->init();
    }

    private function init()
    {
        $this->addFieldset('Capital');
        $this->addSelect('share_currency', 'Capital Currency *', Currency::getAllCurrencies())
            ->addRule(FForm::Required, 'Please provide Capital Currency');

        $this->addFieldset('Capital Share');
        $this->addText('share_class', 'Share Class *')
            ->addRule(FForm::Required, 'Please provide Share Class')
            ->setDescription('(Share Class should be unique)');
        $this->addArea('prescribed_particulars', 'Prescribed Particulars *')
            ->addRule(FForm::Required, 'Please provide Prescribed Particulars');
        $this->addText('num_shares', 'Number of Shares *')
            ->addRule(FForm::Required, 'Please provide Number of Shares')
            ->addRule(FForm::NUMERIC, 'Only digits allowed')
            ->addRule(FForm::GREATER_THAN, 'Value must be greater than 0', 0);
        $this->addText('share_value', 'Share Value *')
            ->addRule(FForm::Required, 'Please provide Share Value')
            ->addRule(FForm::NUMERIC, 'Only digits allowed')
            ->addRule(FForm::GREATER_THAN, 'Value must be greater than 0', 0);
        $this->addRadio('paid', '&nbsp;Amount Paid <br />&nbsp;(per Share) *', CapitalShare::$paidStatuses)
            ->addRule(FForm::Required, 'Please provide');
        $this->addFieldset('Action');
        $this->addSubmit('submit', 'Save and continue >')->style('width: 200px; height: 30px;')->class('btn');
        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $d = $this->getValues();
            $share = new CapitalShare($d['share_class'], $d['prescribed_particulars'], $d['num_shares'], $d['share_value'], $d['paid']);
            $capital = new Capital($d['share_currency'], $this->formSubmissionId);
            $capital->setShares(array($share));
            $this->arService->saveCapital($capital);
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

}
