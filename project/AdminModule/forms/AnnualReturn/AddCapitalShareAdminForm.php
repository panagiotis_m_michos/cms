<?php

use Services\AnnualReturnService;
use Entities\Capital;
use Entities\CapitalShare;

class AddCapitalShareAdminForm extends FForm
{

    /**
     * @var ARAdminControler
     */
    private $controler;

    /**
     * @var array
     */
    private $callback;

    /**
     * @var ARCapital
     */
    public $capital;

    /**
     * @var AnnualReturnService
     */
    protected $arService;

    /**
     * @param ARAdminControler $controler
     * @param Capital $capital
     * @param array $callback
     * @param AnnualReturnService $arService
     */
    public function startup(ARAdminControler $controler, Capital $capital, array $callback, AnnualReturnService $arService)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->capital = $capital;
        $this->arService = $arService;
        $this->init();
    }

    private function init()
    {
        $this->addFieldset('Capital Share');
        $this->addText('share_class', 'Share Class *')
            ->addRule(FForm::Required, 'Please provide Share Class')
            ->setDescription('(Share Class should be unique)');
        $this->addArea('prescribed_particulars', 'Prescribed Particulars *')
            ->addRule(FForm::Required, 'Please provide Prescribed Particulars');
        $this->addText('num_shares', 'Number of Shares *')
            ->addRule(FForm::Required, 'Please provide Number of Shares')
            ->addRule(FForm::NUMERIC, 'Only digits allowed')
            ->addRule(FForm::GREATER_THAN, 'Value must be greater than 0', 0);
        $this->addText('share_value', 'Share Value *')
            ->addRule(FForm::Required, 'Please provide Share Value')
            ->addRule(FForm::NUMERIC, 'Only digits allowed')
            ->addRule(FForm::GREATER_THAN, 'Value must be greater than 0', 0);
        $this->addRadio('paid', '&nbsp;Amount Paid <br />&nbsp;(per Share) *', CapitalShare::$paidStatuses)
            ->addRule(FForm::Required, 'Please provide');

        $this->addText('partialy_paid', NULL)
            ->addRule(array($this, 'Validator_capitalPaidRequired'), 'Please provide value for partially paid')
            ->addRule(FForm::REGEXP, 'Please provide numeric value', '#^([\d\.])+$#');

        $this->addFieldset('Action');
        $this->addSubmit('submit', 'Save and continue >')->style('width: 200px; height: 30px;')->class('btn');
        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $d = $this->getValues();
            $share = new CapitalShare($d['share_class'], $d['prescribed_particulars'], $d['num_shares'], $d['share_value'], $d['paid']);
            if (isset($d['partialy_paid'])) {
                $share->setPartialyPaid($d['partialy_paid']);
            }

            $this->capital->addShare($share);
            $this->arService->saveCapitalShare($share);
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Validator for capital - partial paid value
     *
     * @param Text $control
     * @param string $error
     * @return mixed
     */
    public static function Validator_capitalPaidRequired(Text $control, $error)
    {
        $id = $control->getName();
        $paid = $control->owner[$id]->getValue();
        if ($paid == 'partialy_paid' && $control->getValue() == '') {
            return $error;
        }
        return TRUE;
    }

}
