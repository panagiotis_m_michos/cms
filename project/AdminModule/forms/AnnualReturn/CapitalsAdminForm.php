<?php

use Services\AnnualReturnService;
use Entities\CapitalShare;

class CapitalsAdminForm extends FForm
{

    /**
     * @var ARAdminControler
     */
    private $controler;

    /**
     * @var array
     */
    private $callback;

    /**
     * @var int
     */
    public $formSubmissionId;

    /**
     * @var AnnualReturnService
     */
    protected $arService;

    /**
     * @param ARAdminControler $controler
     * @param int $formSubmissionId
     * @param array $callback
     * @param AnnualReturnService $arService
     */
    public function startup(ARAdminControler $controler, $formSubmissionId, array $callback, AnnualReturnService $arService)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->formSubmissionId = $formSubmissionId;
        $this->arService = $arService;
        $this->init();
    }

    private function init()
    {
        $capitals = $this->decorateCapitals($this->arService->getAllCapitals($this->formSubmissionId));
        foreach ($capitals as $key => $capital) {
            foreach ($capital->getShares() as $share) {//pr($share);
                $this->addFieldset('Share For Capital ' . $capital->getShareCurrency());
                $this->addText('share_currency_' . $share->getId(), 'Share Currency ')
                    ->setValue($capital->getShareCurrency())->readonly()
                    ->style('background: white;  border: 0;');
                $this->addText('share_class_' . $share->getId(), 'Share Class *')
                    ->setValue($share->getShareClass())
                    ->addRule(FForm::Required, 'Please provide Share Class')
                    ->setDescription('Share Class should be unique');
                $this->addArea('prescribed_particulars_' . $share->getId(), 'Prescribed Particulars *')
                    ->setValue($share->getPrescribedParticulars())
                    ->addRule(FForm::Required, 'Please provide Prescribed Particulars');
                $this->addText('num_shares_' . $share->getId(), 'Number of Shares *')
                    ->setValue($share->getNumShares())
                    ->addRule(FForm::Required, 'Please provide Number of Shares')
                    ->addRule(FForm::NUMERIC, 'Only digits allowed')
                    ->addRule(FForm::GREATER_THAN, 'Value must be greater than 0', 0);
                $this->addText('share_value_' . $share->getId(), 'Share Value *')
                    ->setValue($share->getShareValue())
                    ->addRule(FForm::Required, 'Please provide Share Value')
                    ->addRule(FForm::NUMERIC, 'Only digits allowed')
                    ->addRule(FForm::GREATER_THAN, 'Value must be greater than 0', 0);
                $paid = array_intersect_key(CapitalShare::$paidStatuses, $share->getPaid());
                $this->addRadio('paid_' . $share->getId(), '&nbsp;Amount Paid <br />&nbsp;(per Share)', $paid);

                // set value for paid
                foreach ($share->getPaid() as $key2 => $val2) {
                    if ($val2 == 1) {
                        $this['paid_' . $share->getId()]->setValue($key2);
                        break;
                    }
                }

                //                if ($share->getPartialyPaid()) {
                $this->addText('partialy_paid_' . $share->getId(), NULL)
                    ->setValue($share->getPartialyPaid())
                    ->addRule(array($this, 'Validator_capitalPaidRequired'), 'Please provide value for partially paid')
                    ->addRule(FForm::REGEXP, 'Please provide numeric value', '#^([\d\.])+$#');
                //                }
            }
        }

        $this->addFieldset('Action');
        $this->addSubmit('submit', 'Save and continue >')->style('width: 200px; height: 30px;')->class('btn');
        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * decorate capitals form base on synced value from Company House
     * @param array $capitals
     * @return array
     */
    public function decorateCapitals($capitals)
    {
        foreach ($capitals as $key => $capital) {
            foreach ($capital->getShares() as $share) {
                switch ($share->getSyncPaid()) {
                    /* paid from sync */
                    /* show fully paid radio button */
                    case CapitalShare::STATUS_PAID:
                        $share->setPaid(array(CapitalShare::STATUS_PAID => 1));
                        break;
                    /* unpaid from sync
                      show all radio buttons */
                    case CapitalShare::STATUS_UNPAID:
                        /* set numeric value for partially paid */
                        $share->setPartialyPaid($share->getPaid());
                        if ($share->getPaid() == CapitalShare::STATUS_PAID || $share->getPaid() == CapitalShare::STATUS_UNPAID) {
                            $share->setPartialyPaid(0);
                        }
                        switch ($share->getPaid()) {
                            case CapitalShare::STATUS_UNPAID:
                                $share->setPaid(array(CapitalShare::STATUS_UNPAID => 1, CapitalShare::STATUS_PARTIALY_PAID => 0, CapitalShare::STATUS_PAID => 0));
                                break;
                            case CapitalShare::STATUS_PAID:
                                $share->setPaid(array(CapitalShare::STATUS_UNPAID => 0, CapitalShare::STATUS_PARTIALY_PAID => 0, CapitalShare::STATUS_PAID => 1));
                                break;
                            default:
                                $share->setPaid(array(CapitalShare::STATUS_UNPAID => 0, CapitalShare::STATUS_PARTIALY_PAID => 1, CapitalShare::STATUS_PAID => 0));
                                break;
                        }
                        break;
                    /* partialy paid from sync */
                    /* show just paid and partialy paid radio buttons */
                    default:
                        /* in this case 'paid' holds the number */
                        $share->setPartialyPaid($share->getPaid());
                        $share->setPaid(array(CapitalShare::STATUS_PARTIALY_PAID => 1, CapitalShare::STATUS_PAID => 1));
                        if ($share->getPaid() == CapitalShare::STATUS_PAID) {
                            $share->setPartialyPaid($share->getSyncPaid());
                            $share->setPaid(array(CapitalShare::STATUS_PARTIALY_PAID => 0, CapitalShare::STATUS_PAID => 1));
                        }
                        break;
                }
                //$share->setSyncPaid(NULL);
            }
        }
        return $capitals;
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();
            $capitals = $this->arService->getAllCapitals($this->formSubmissionId);
            foreach ($capitals as $capital) {
                $shares = array();
                foreach ($capital->getShares() as $share) {
                    $key = $share->getId();
                    $share = $this->arService->findCapitalShare($key);
                    $share->setShareClass($data['share_class_' . $key]);
                    $share->setPrescribedParticulars($data['prescribed_particulars_' . $key]);
                    $share->setNumShares($data['num_shares_' . $key]);
                    $share->setShareValue($data['share_value_' . $key]);
                    $share->setPaid($data['paid_' . $key]);
                    if (isset($data['partialy_paid_' . $key])) {
                        $share->setPartialyPaid($data['partialy_paid_' . $key]);
                    }
                    $shares[] = $share;
                }
                $capital->setShares($shares);
                $this->arService->saveCapital($capital);
            }
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Validator for capital - partial paid value
     * @param Text $control
     * @param string $error
     * @return mixed
     */
    public static function Validator_capitalPaidRequired(Text $control, $error)
    {
        $id = $capitalClues = explode('_', $control->getName());
        $id = 'paid_' . $id[2];
        $paid = $control->owner[$id]->getValue();
        if ($paid == 'partialy_paid' && $control->getValue() == '') {
            return $error;
        }
        return TRUE;
    }

}
