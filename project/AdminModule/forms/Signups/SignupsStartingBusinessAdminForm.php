<?php
/**
 * CMS
 * @category   Project
 * @package    CMS
 * @author     Nikolai Senkevich
 * @version    SignupsStartingBusinessAdminForm.php 2012-03-14 
 */

class SignupsStartingBusinessAdminForm  extends FForm
{
    /**
     * @var SignupAdminControler 
     */
    private $controler;
    
    /**
     * @var SignupsModel 
     */
    private $node;
    
    /**
     * @var array
     */
    private $callback;

    /**
     * @param SignupAdminControler $controler
     * @param array $callback
     */
    public function startup(SignupAdminControler $controler, SignupsModel $node, array $callback)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->node = $node;
        $this->init();
    }

    /**
	 * @return void
	 */
    private function init()
    {
		$this->addFieldset('Attachment');
		$this->add('CmsFile', 'attachment', 'Attachment :')
				->setNodeId(SignupsModel::ATTACHMENT_FILE_PAGE)
                ->setValue($this->node->getProperty('startingBusiness', SignupsControler::STARTING_BUSINESS));

		$this->addFieldset('Action');
		$this->addSubmit('submit','Submit')->class('btn');
        $this->onValid = $this->callback;
	    $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();
            $this->node->startingBusiness = $data['attachment'];
            $this->node->save();
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }
}
