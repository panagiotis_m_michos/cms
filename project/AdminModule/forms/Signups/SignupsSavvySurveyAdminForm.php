<?php
/**
 * CMS
 * @category   Project
 * @package    CMS
 * @author     Nikolai Senkevich
 * @version    SignupsSavvySurveyAdminForm.php 2012-03-14 
 */

class SignupsSavvySurveyAdminForm  extends FForm
{
    /**
     * @var SignupAdminControler 
     */
    private $controler;
    
    /**
     * @var SignupsModel 
     */
    private $node;
    
    
    /**
     * @var array
     */
    private $callback;

    /**
     * @param SignupAdminControler $controler
     * @param array $callback
     */
    public function startup(SignupAdminControler $controler, SignupsModel $node, array $callback)
    {
        $this->controler = $controler;
        $this->node = $node;
        $this->callback = $callback;
        $this->init();
    }

    /**
	 * @return void
	 */
    private function init()
    {  
		$this->addFieldset('Attachment');
		$this->add('CmsFile', 'attachment', 'Attachment :')
				->setNodeId(SignupsModel::ATTACHMENT_FILE_PAGE)
                ->setValue($this->node->getProperty('savvySurvey', SignupsControler::SAVVY_SURVEY));

		$this->addFieldset('Action');
		$this->addSubmit('submit','Submit')->class('btn');
        $this->onValid = $this->callback;
	    $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();
            $this->node->savvySurvey = $data['attachment'];
            $this->node->save();
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }
}
