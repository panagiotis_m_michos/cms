<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    CFRegisteredOfficePropertiesAdminForm.php 2011-11-18 razvanp@madesimplegroup.com
 */

class CFRegisteredOfficePropertiesAdminForm  extends FForm
{
    /**
     * @var CFRegisteredOfficeAdminControler 
     */
    private $controler;
    /**
     * @var array
     */
    private $callback;

    /**
    *  @var CFRegisteredOfficeModel
    */
    private $node;
    
    /**
     * @param CFRegisteredOfficeAdminControler $controler
     * @param array $callback
     */
    public function startup(CFRegisteredOfficeAdminControler $controler, array $callback, CFRegisteredOfficeModel $node)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->node = $node;
        $this->init();
    }

    /**
	 * @return void
	 */
    private function init()
    {
        $this->addFieldset('Html Box Under Guide');
        
        $this->addCheckbox('hasVisibleHtmlBox', 'Active:',1)
            ->setDescription('(Hide or unhide the box)')    
            ->setValue($this->node->getHasVisibleHtmlBox());
        
        $this->add('CmsFckArea','underGuideHtmlBox','Box Text:')
             ->setSize(500, 200)
             ->setToolbar('Basic')
             ->setValue($this->node->getUnderGuideHtmlBox());
        

        $this->addFieldset('Action');
        $this->addSubmit('submit','Submit')
	     ->class('btn');
        
        
        $this->onValid = $this->callback;
	    $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();
            
            $this->node->setHasVisibleHtmlBox($data['hasVisibleHtmlBox']);
            $this->node->setUnderGuideHtmlBox($data['underGuideHtmlBox']);
            $this->node->save();

            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }
}
