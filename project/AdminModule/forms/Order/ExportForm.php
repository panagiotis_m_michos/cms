<?php

namespace Admin\Order;

use FormModule\Types\DateRangeType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ExportForm extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        $builder->setMethod('POST');
        $builder->add(
            'date_range',
            new DateRangeType(),
            [
                'property_path' => 'datesRange',
                'start_options' => [
                    'required' => FALSE,
                    'label' => 'Start Date',
                ],
                'end_options' => [
                    'required' => FALSE,
                    'label' => 'End Date',
                ]
            ]
        );
        $builder->add(
            'export',
            'submit',
            [
                'label' => 'Export',
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'admin_order_export_form';
    }
}