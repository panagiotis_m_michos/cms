<?php

namespace Admin\Order\Entity;

use Entities\Order;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Validators\Constraints\Entity\RequiredChoicesConstraint;

class RefundForm extends AbstractType
{
    /**
     * @var Order
     */
    private $order;

    /**
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        $builder->setMethod('POST');
        $builder->add(
            'orderItems',
            'entity',
            array(
                'required' => TRUE,
                'class' => 'Entities\OrderItem',
                'property' => 'getProductTitleAndTotalPrice',
                'choices' => $this->order->getItems(),
                'multiple' => TRUE,
                'expanded' => TRUE,
                'constraints' => array(new RequiredChoicesConstraint(1)),
            )
        );
        $builder->add(
            'itemsRefund',
            'number',
            array(
                'label' => 'Items Refund',
                'data' => 0,
                'attr' => array(
                    'readonly' => TRUE,
                ),
                'mapped' => FALSE,
            )
        );
        $builder->add(
            'adminFee',
            'number',
            array(
                'invalid_message' => 'Please provide numeric value',
                'label' => 'Admin Fee',
                'data' => 0,
            )
        );
        $builder->add(
            'totalCreditRefund',
            'number',
            array(
                'label' => 'Total Credit Refund',
                'data' => 0,
                'attr' => array(
                    'readonly' => TRUE
                ),
                'mapped' => FALSE,
            )
        );
        $builder->add(
            'totalRefund',
            'number',
            array(
                'label' => 'Total Money Refund',
                'data' => 0,
                'attr' => array(
                    'readonly' => TRUE
                ),
                'mapped' => FALSE,
            )
        );
        $builder->add(
            'description',
            'textarea',
            array(
                'required' => FALSE,
            )
        );
        $builder->add(
            'refund',
            'submit',
            array(
                'label' => 'Refund',
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'orderRefund';
    }
}