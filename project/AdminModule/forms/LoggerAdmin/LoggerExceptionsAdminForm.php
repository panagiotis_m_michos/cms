<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    LoggerExceptionsAdminForm.php 2011-03-03 divak@gmail.com
 */

class LoggerExceptionsAdminForm extends FForm
{
    /**
     * @var LoggerAdminControler
     */
    private $controler;

    /**
     * @var array
     */
    private $callback;
    
    /**
     * @param LoggerAdminControler $controler
     * @param array $callback
     */
    public function startup(LoggerAdminControler $controler, array $callback)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->init();
    }

    /**
     * @return void
     */
    private function init()
    {
        $node = $this->controler->node;

		$this->addFieldset('Action');
        $this->addCheckbox('options', 'Options', $node->getOptions())
                ->setValue(array('errorLog', 'mailingFile', 'exceptions'));
        $this->addSubmit('submit','Delete')->style('width: 150px;');

		$this->onValid = $this->callback;
        $this->clean();
        $this->start();
    }


    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $values = $this->getValues();
            $node = $this->controler->node;

            $options = (array)$values['options'];
            foreach ($options as $option) {
                switch ($option) {
                    case 'errorLog':
                        $node->deleteErroLogFile();
                        break;
                    case 'mailingFile':
                        $node->deleteMailingFile();
                        break;
                    case 'exceptions':
                        $node->deleteExceptions();
                        break;

                }
            }

            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }
}
