<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    FeedbacksForm.php 2011-03-21 razvanp@madesimplegroup.com
 */

class FeedBackAdminForm  extends FForm
{
    /**
     * @var FeedBackAdminControler 
     */
    private $controler;
    /**
     * @var array
     */
    private $callback;

    /**
     * @param FeedBacksAdminControler $controler
     * @param array $callback
     */
    public function startup(FeedBacksAdminControler $controler, array $callback)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->init();
    }

    /**
	 * @return void
	 */
    private function init()
    {
        $this->addFieldset('Feedback Search');
        $this->add('DatePicker', 'dtcFrom',  'From');
        $this->add('DatePicker', 'dtcTo',  'To');
        $this->addSelect('nodeId','Page Name' , FMenu::getSelect(1))->style('width:200px')
             ->setFirstOption('--- Select ---');
        $this->addSelect('rating', 'Rating',FeedBacksModel::$ratings)
             ->setFirstOption('--- Select ---');
		$this->addSubmit('feedbackSearch', 'Search')->class('btn');
        $this->onValid = $this->callback;
	    $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }
}
