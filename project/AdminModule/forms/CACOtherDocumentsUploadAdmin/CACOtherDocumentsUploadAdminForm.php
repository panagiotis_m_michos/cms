<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    CACOtherDocumentsUploadAdminForm.php 2011-10-05 razvanp@madesimplegroup.com
 */
class CACOtherDocumentsUploadAdminForm extends FForm {

    /**
     * @var CompaniesAdminControler 
     */
    private $controler;

    /**
     * @var array
     */
    private $callback;

    /**
     * @var Company
     */
    private $company;

    /**
     * @param CompaniesAdminControler $controler
     * @param array $callback
     */
    public function startup(CompaniesAdminControler $controler, array $callback, $company) {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->company = $company;
        $this->init();
    }

    /**
     * @return void
     */
    private function init() {
        $this->addFieldset('Submit Feedback');

        $this->addFieldset("Document Upload");
        $this->add('MultiFile', 'CmsFile', NULL)
                ->addRule(array($this, 'Validator_DocumentRequired'), 'A file is required')
                ->addRule(array($this, 'Validator_multipleDocumentsType'), 'File type not supported')
                ->addRule(array($this, 'Validator_multipleDocumentsSize'), 'File exceeds the max file size of 3MB');
        $this->addFieldset('Action');
        $this->addSubmit('send', 'Upload')->class('btn_submit fright mbottom btn');

        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process() 
    {
        
        $data = $this->getValues();
        $data = array_filter($data['CmsFile'], 'strlen');

        foreach ($data as $fileData) {
            $info = pathinfo($fileData->getName());
            $document = new CompanyOtherDocument();
            $document->companyId = $this->company->getCompanyId();
            $document->typeId = CompanyOtherDocument::TYPE_OTHERS;
            $document->name = $info['filename'] . time();
            $document->fileTitle = $info['filename'];
            $document->size = $fileData->getSize();
            $document->ext = String::lower($info['extension']);
            $document->uploadedFile = $fileData;
            $document->save();
        }
        $this->clean();
    }

    /**
     * @param FControl $control
     * @param string $error
     * @param array $params
     */
    public function Validator_DocumentRequired(FControl $control, $error, array $params) {
        
        $values = $control->getValue();
        $firstFile = $values[1];
        ($firstFile instanceof HttpUploadedFile && $firstFile->isOk()) ? $value = TRUE : $value = $error;
        return $value;
    }
    
    /**
     * @param FControl $control
     * @param string $error
     * @param array $params
     */
    public function Validator_multipleDocumentsType(FControl $control, $error, array $params) {
        $allowed = array('doc', 'docx', 'rtf', 'pdf', 'odt', 'xls', 'xlsx', 'ods');
        $values = $control->getValue();
        /* @var $value HttpUploadedFile */
        foreach ($values as $value) {
            if ($value instanceof HttpUploadedFile && $value->isOk()) {
                $info = pathinfo($value->getName());
                if (!in_array(strtolower($info['extension']), $allowed)) {
                    return $error;
                }
            }
        }
        return TRUE;
    }

    /**
     * @param FControl $control
     * @param string $error
     * @param array $params
     */
    public function Validator_multipleDocumentsSize(FControl $control, $error, array $params) {
        $values = $control->getValue();
        /* @var $value HttpUploadedFile */
        foreach ($values as $value) {
            if ($value instanceof HttpUploadedFile && $value->isOk()) {
                if ($value->getsize() > 3138576) {
                    return $error;
                }
            }
        }
        return TRUE;
    }

}
