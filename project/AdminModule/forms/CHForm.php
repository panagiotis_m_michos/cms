<?php

namespace Admin;

use FControl;
use FForm;

class CHForm extends FForm
{
    public function start()
    {
        /* @var $element FControl */
        foreach ($this->elements as $element) {
            if (get_class($element) === 'Text' || get_class($element) === 'Area') {
                $element->addRule(array('CHValidator', 'Validator_isValidChString'), NULL);
            }
        }
        parent::start();
    }
}
