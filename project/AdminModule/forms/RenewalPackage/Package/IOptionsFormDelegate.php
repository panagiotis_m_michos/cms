<?php

namespace Admin\RenewalPackage\Package;

use Exception;

interface IOptionsFormDelegate
{
    public function renewalPackageOptionsFormSucceeded();

    /**
     * @param Exception $e
     */
    public function renewalPackageOptionsFormFailed(Exception $e);
}

