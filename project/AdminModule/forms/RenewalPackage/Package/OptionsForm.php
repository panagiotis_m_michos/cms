<?php

namespace Admin\RenewalPackage\Package;

use Exception;
use FForm;
use Package;
use RenewalPackage;

class OptionsForm extends FForm
{
    /**
     * @var IOptionsFormDelegate
     */
    public $delegate;
    
    /**
     * @var Package
     */
    private $node;

    /**
     * @param IOptionsFormDelegate $delegate
     * @param RenewalPackage $node
     */
    public function startup(IOptionsFormDelegate $delegate, RenewalPackage $node)
    {
        $this->delegate = $delegate;
        $this->node = $node;
        $this->buildForm();
        $this->onValid = array($this, 'process');
        $this->start();
    }

    public function buildForm()
    {
        // fieldset
        $this->addFieldset('Package');
        $this->addText('title', 'Title: * ')
            ->addRule(FForm::Required, 'Title is required!')
            ->size(30)
            ->setValue($this->node->page->title);
        $this->addText('alternativeTitle', 'Alternative Title:')
            ->size(50)
            ->setValue($this->node->page->alternativeTitle);
        $this->add('CmsFckArea', 'abstract', 'Description:')
            ->setSize(500, 100)
            ->setToolbar('Basic')
            ->setValue($this->node->page->abstract);
        $this->add('CmsFckArea', 'text', 'Page Text:')
            ->setSize(500, 300)
            ->setValue($this->node->page->text);

        // price	
        $this->addFieldset('Price');
        $this->addText('price', 'Price: *')
            ->size(5)
            ->setValue($this->node->getPrice())
            ->addRule(FForm::Required, 'Please provide price')
            ->addRule('Numeric', 'Price has to be number');
        $this->addText('productValue', 'Product value: *')
            ->size(5)
            ->setValue($this->node->productValue)
            ->addRule(FForm::Required, 'Please provide product value')
            ->addRule('Numeric', 'Price value has to be number');
        $this->addText('wholesalePrice', 'Wholesale price: *')
            ->size(5)
            ->setValue($this->node->wholesalePrice)
            ->addRule(FForm::Required, 'Please provide wholesale price')
            ->addRule('Numeric', 'Wholesale price value has to be number');

        // action
        $this->addFieldset('Action');
        $this->addSubmit('submit', 'Submit')
            ->onclick('changes = false')
            ->class('btn');
    }
    
    public function process()
    {
        try {
            $data = $this->getValues();
            $this->node->page->title = $data['title'];
            $this->node->page->alternativeTitle = $data['alternativeTitle'];
            $this->node->page->abstract = $data['abstract'];
            $this->node->page->text = $data['text'];
            $this->node->setPrice($data['price']);
            $this->node->productValue = $data['productValue'];
            $this->node->wholesalePrice = $data['wholesalePrice'];
            $this->node->save();

            $this->clean();
            $this->delegate->renewalPackageOptionsFormSucceeded();

        } catch (Exception $e) {
            $this->delegate->renewalPackageOptionsFormFailed($e);
        }
    }

}
