<?php

namespace Admin\Customer;

interface INoteFormDelegate
{
    function customerNoteSucceeded();
    function customerNoteFailed(\Exception $e);
}
