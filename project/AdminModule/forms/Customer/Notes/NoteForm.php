<?php

namespace Admin\Customer;

use Services\CustomerNoteService,
    Entities\Customer,
    Entities\CustomerNote,
    Entities\User;

class NoteForm extends \FForm
{
    /**
     * @var IAdminCustomerNoteFormDelegate 
     */
    private $delegate;
    
    /**
     * @var Customer 
     */
    private $customer;
    
    /**
     * @var User 
     */
    private $user;
    
    /**
     * @var CustomerNoteService 
     */
    private $service;

    /**
     * @param \Admin\Customer\INoteFormDelegate $delegate
     * @param \Entities\Customer $customer
     * @param \Entities\User $user
     * @param \Services\CustomerNoteService $service
     */
    public function startup(INoteFormDelegate $delegate, Customer $customer, User $user, CustomerNoteService $service)
    {
        $this->delegate = $delegate;
        $this->customer = $customer;
        $this->user = $user;
        $this->service = $service;
        
        $this->buildForm();
        $this->onValid = array($this, 'process');
	    $this->start();
    }

    private function buildForm()
    {
        $this->addArea('note', 'New note:')
            ->cols(50)
            ->rows(5)
            ->addRule(self::Required, 'Required!');
        $this->addSubmit('submit', 'Add Note')
            ->class('btn');
    }

    public function process()
    {
        try {
            $data = $this->getValues();
            $entity = new CustomerNote;
            $entity->customer = $this->customer;
            $entity->user = $this->user;
            $entity->note = $data['note'];
            
            $this->service->save($entity);
            
            $this->clean();
            $this->delegate->customerNoteSucceeded();
            
        } catch (Exception $e) {
            $this->delegate->customerNoteFailed($e);
        }
    }
}
