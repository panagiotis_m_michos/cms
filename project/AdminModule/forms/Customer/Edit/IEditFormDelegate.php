<?php

namespace Admin\Customer;

interface IEditFormDelegate
{
    function customerEditSucceeded();
    function customerEditFailed(\Exception $e);
}
