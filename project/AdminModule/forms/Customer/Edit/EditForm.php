<?php

namespace Admin\Customer;

use Services\CustomerNoteService;
use Entities\User;
use Entities\Customer;
use Services\CustomerService;
use Entities\CustomerNote;
use Exception;

class EditForm extends \FForm
{
    /**
     * @var IEditFormDelegate 
     */
    private $delegate;

    /**
     * @var Customer 
     */
    private $customer;

    /**
     * @var FUser 
     */
    private $user;

    /**
     * @var CustomerNoteService 
     */
    private $noteService;

    /**
     * @var CustomerService 
     */
    private $customerService;

    /**
     * @param \Admin\Customer\IEditFormDelegate $delegate
     * @param \Entities\Customer $customer
     * @param \Entities\User $user
     * @param \Services\CustomerNoteService $noteService
     * @param \Services\CustomerService $customerService
     */
    public function startup(IEditFormDelegate $delegate, Customer $customer, User $user, CustomerNoteService $noteService, CustomerService $customerService)
    {
        $this->delegate = $delegate;
        $this->customer = $customer;
        $this->user = $user;
        $this->noteService = $noteService;
        $this->customerService = $customerService;

        $this->buildForm();
        $this->onValid = array($this, 'process');
        $this->setInitValues($this->customer->toArray());
        $this->start();
    }

    public function process()
    {
        try {
            $data = $this->getValues();

            $customer = $this->customer;
            $customer->email = $data['email'];

            if ($data['password']) {
                $customer->password = $data['password'];
            }

            $customer->statusId = $data['statusId'];
            $customer->titleId = $data['titleId'];
            $customer->firstName = $data['firstName'];
            $customer->lastName = $data['lastName'];
            $customer->companyName = $data['companyName'];
            $customer->address1 = $data['address1'];
            $customer->address2 = $data['address2'];
            $customer->address3 = $data['address3'];
            $customer->city = $data['city'];
            $customer->county = $data['county'];
            $customer->postcode = $data['postcode'];
            $customer->countryId = $data['countryId'];
            $customer->phone = $data['phone'];
            $customer->tagId = $data['tagId'];
            $customer->roleId = $data['roleId'];

            $this->customerService->save($customer);

            if ($data['statusId'] == \Customer::STATUS_BLOCKED && $data['blockedReason']) {
                $entity = new CustomerNote;
                $entity->customer = $this->customer;
                $entity->user = $this->user;
                $entity->note = sprintf("%s %s", CustomerNote::BLOCKED_TEXT, $data['blockedReason']);
                $this->noteService->save($entity);
            }

            $this->clean();
            $this->delegate->customerEditSucceeded();
        } catch (Exception $e) {
            $this->delegate->customerEditFailed($e);
        }
    }

    private function buildForm()
    {
        // login details
        $this->addFieldset('Login details');
        $this->addText('email', 'Email *')
            ->addRule(self::Required, 'Please provide e-mail address!')
            ->addRule(self::Email, 'Email is not valid!')
            ->addRule(array($this, 'Validator_uniqueLogin'), 'Email address has been taken.', array('customerId' => $this->customer->getId()));
        $this->addPassword('password', 'Password *');
        $this->addPassword('passwordConf', 'Confirm Password *')
            ->addRule(self::Equal, 'Passwords are not the same!', 'password');

        // personal details
        $this->addFieldset('Personal details');
        $this->addSelect('statusId', 'Status *', \Customer::$statuses)
            ->setFirstOption('--- Select ---');
        if ($this->customer->statusId != \Customer::STATUS_BLOCKED) {
            $this->addArea('blockedReason', 'Reason *')
                ->addRule(array($this, 'Validator_blockedRequired'), 'Please provide reason for blocking this customer')
                ->cols(40)
                ->rows(5);
        }
        $this->addSelect('titleId', 'Title', \Customer::$titles)
            ->setFirstOption('--- Select ---');
        $this->addText('firstName', 'Firstname *')
            ->addRule(self::Required, 'Please provide firstname!');
        $this->addText('lastName', 'Lastname *')
            ->addRule(self::Required, 'Please provide lastname!');
        $this->addText('companyName', 'Company name');

        // address details
        $this->addFieldset('Address details');
        $this->addText('address1', 'Address 1 *')
            ->addRule(self::Required, 'Please provide address 1!');
        $this->addText('address2', 'Address 2');
        $this->addText('address3', 'Address 3');
        $this->addText('city', 'City *')
            ->addRule(self::Required, 'Please provide city!');
        $this->addText('county', 'County');
        $this->addText('postcode', 'Postcode *')
            ->addRule(self::Required, 'Please provide post code!');
        $this->addSelect('countryId', 'Country *', \Customer::$countries)
            ->setFirstOption('--- Select ---')
            ->addRule(self::Required, 'Please provide country!');
        $this->addText('phone', 'Phone');

        // other
        $this->addFieldset('Other');

        $this->addSelect('tagId', 'Tag: ', \Customer::$tags);
        $this->addSelect('roleId', 'Role: ', \Customer::$roles);

        // action
        $this->addFieldset('Action');
        $this->addSubmit('update', 'Update data')
            ->class('btn');
    }

    /**
     * @param mixed $control
     * @param mixed $error
     * @param mixed $params
     * @return bool
     */
    public function Validator_uniqueLogin($control, $error, $params)
    {
        $customerId = \dibi::select('customerId')
            ->from(TBL_CUSTOMERS)
            ->where('email = %s', $control->getValue())
            ->and('customerId != %i', $params['customerId'])
            ->fetchSingle();
        if ($customerId) {
            return $error;
        }
        return TRUE;
    }

    /**
     * @param mixed $control
     * @param mixed $error
     * @param mixed $params
     * @return bool
     */
    public function Validator_blockedRequired($control, $error, $params)
    {
        $form = $control->owner;
        $blockedReason = $control->getValue();
        $statusId = $form['statusId']->getValue();
        if ($statusId != \Customer::STATUS_BLOCKED) {
            return TRUE;
        }
        if (!empty($blockedReason)) {
            return TRUE;
        }
        return $error;
    }

}
