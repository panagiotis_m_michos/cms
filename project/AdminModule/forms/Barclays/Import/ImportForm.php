<?php

namespace Admin\Barclays;

use Entities\Cashback;
use FControl;
use FForm;

class ImportForm extends FForm
{
    /**
     * @param callable $onValid
     */
    public function startup(callable $onValid)
    {
        $this->buildForm();
        $this->onValid = $onValid;
        $this->start();
    }

    /**
     * @param FControl $control
     * @param string $error
     * @return bool|string
     */
    public function Validator_DocumentsType(FControl $control, $error)
    {
        $values = $control->getValue();
        $file = explode(".", $values['name']);
        if (strtolower($file[1]) != 'csv') {
            return $error;
        }

        return TRUE;
    }

    private function buildForm()
    {
        $this->addFieldset('Cash back import');
        $this->addRadio('bankType', 'Bank:', Cashback::$cashBackBanks)
            ->addRule(FForm::Required, 'Please select a bank')
            ->style('margin: 0 0 10px 10px;');
        $this->addFile('import', 'File:')
            ->addRule([$this, 'Validator_DocumentsType'], 'File type not supported')
            ->addRule(self::Required, 'Please select a file')
            ->style('margin: 0 0 10px 10px;');
        $this->addSubmit('submit', 'Import cash back records')
            ->class('btn')
            ->style('width: 200px; height: 30px; margin: 10px;');
    }

}
