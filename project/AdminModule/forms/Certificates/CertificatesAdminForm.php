<?php

/**
 * @package     Companies Made Simple
 * @subpackage     CMS
 * @author         Nikolai Senkevich
 * @internal     project/forms/Certificates/CertificatesAdminForm.php
 * @created     26/09/2012
 */
class CertificatesAdminForm extends FForm
{

    /**
     * @var CertificatesAdminControler
     */
    private $controler;
    /**
     * @var array
     */
    private $certificates;
    /**
     * @var array
     */
    private $callback;

    /**
     *
     * @param CertificatesAdminControler $controler
     * @param CashbackModel $node
     * @param array $callback 
     */
    public function startup(CertificatesAdminControler $controler, array $callback, array $certificates) 
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->certificates = $certificates;
        $this->init();
    }

    private function init() 
    {
        
        $this->addSubmit('printCertificates', 'Print Certificates')->class('btn');
        //$this->addSubmit('printCovers', 'Print Cover Letters')->class('btn');
        $this->addSubmit('printShareCerts', 'Print Share Certs')->class('btn');
        $this->addSubmit('markAllPrinted', 'Mark All As Printed')->class('btn')->onclick('return confirm("Are you sure?")');
        $this->addSubmit('printCoversClean', 'Print Cover Letters (clean)')->class('btn');
        
        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process() 
    {
        try {
            $ids = array_keys($this->certificates);
           
            if ($this->isSubmitedBy('printCertificates')) {
                CHFiling::getPdfCertificates($ids);
                $this->terminate();

                // output covers
                //            } elseif ($this->isSubmitedBy('printCovers')) {
                //                $cover = new CoverLetter();
                //                $cover->createCombinedPdf($ids);
                //                $cover->printPdf();
                //                $this->terminate();
                
            } elseif ($this->isSubmitedBy('printShareCerts')) {
                $sharecerts = new SubscShareCert();
                $sharecerts->createCombinedPdf($ids);
                $sharecerts->printPdf('sharecerts.pdf');
                $this->terminate();    
                
                // mark all certificates as printed
            } elseif ($this->isSubmitedBy('markAllPrinted')) {
                Company::markCertificatesAsPrinted($ids);
                
                // output covers clear for print company
            } elseif ($this->isSubmitedBy('printCoversClean')) {
                $cover = new CoverLetter();
                $cover->createCombinedPdfClear($ids);
                $cover->printPdf('CertCover.pdf');
                $this->terminate();
             }
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

}
