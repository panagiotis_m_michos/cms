<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS
 * @author 		Nikolai Senkevich
 * @internal 	project/forms/Certificates/BronzeCoverLetterAdminForm.php
 * @created 	26/09/2012
 */
class BronzeCoverLetterAdminForm extends FForm {

    /**
     * @var CertificatesAdminControler
     */
    private $controler;
    /**
     * @var array
     */
    private $bronzecoverletters;
    /**
     * @var array
     */
    private $callback;

    /**
     *
     * @param CertificatesAdminControler $controler
     * @param array $callback
     * @param array $bronzecoverletters
     */
    public function startup(CertificatesAdminControler $controler, array $callback, array $bronzecoverletters) {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->bronzecoverletters = $bronzecoverletters;
        $this->init();
    }

    private function init() {
        $this->addSubmit('printCovers', 'Print Cover Letters')->class('btn');
        $this->addSubmit('markAllPrinted', 'Mark All As Printed')
                ->class('btn')
                ->onclick('return confirm("Are you sure?")');
        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process() {
        try {
            $ids = array_keys($this->bronzecoverletters);

            if ($this->isSubmitedBy('printCovers')) {
                $cover = new CoverLetter();
                $cover->createCombinedPdf($ids);
                $cover->printPdf();
                $this->terminate();
            } elseif ($this->isSubmitedBy('markAllPrinted')) {
                Company::markBronzeCoverLetterAsPrinted($ids);
            }
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

}
