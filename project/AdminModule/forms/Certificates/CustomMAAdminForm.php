<?php

class CustomMAAdminForm extends FForm {

    /**
     * @var CertificatesAdminControler
     */
    private $controler;
    /**
     * @var array
     */
    private $ma;
    /**
     * @var array
     */
    private $callback;

    /**
     *
     * @param CertificatesAdminControler $controler
     * @param CashbackModel $node
     * @param array $callback 
     */
    public function startup(CertificatesAdminControler $controler, array $callback, array $ma) {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->ma = $ma;
        $this->init();
    }

    private function init() {
        
        $this->addSubmit('printMA', 'Print Memorandum Articles')->class('btn')->style('margin-right: 15px;');
        $this->addSubmit('printMACovers', 'Print MA Cover Letters')->class('btn')->style('margin-right: 15px;');
        $this->addSubmit('markAllPrinted', 'Mark All As Printed')->class('btn')->onclick('return confirm("Are you sure?")')->style('margin-right: 15px;');
        $this->addSubmit('printMACoversClean', 'Print MA Cover Letters (clean)')->class('btn');

        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process() {
        try {
            $ids = array_keys($this->ma);

            if ($this->isSubmitedBy('printMA')) {
                CHFiling::getPdfMemorandumArticles($ids);
                $this->terminate();

                // output covers
            } elseif ($this->isSubmitedBy('printMACovers')) {
                $macover = new MACoverLetter();
                $macover->createCombinedPdf($ids);
                $macover->printPdf();
                $this->terminate();
                
                // mark all certificates as printed
            } elseif ($this->isSubmitedBy('markAllPrinted')) {
                Company::markMAAsPrinted($ids);
                Company::markMACoverLetterAsPrinted($ids);
                
            } elseif ($this->isSubmitedBy('printMACoversClean')) {
                $macover = new MACoverLetter();
                $macover->createCombinedPdfClean($ids);
                $macover->printPdf('ArticleCover.pdf');
                
                $this->terminate();
            }
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

}
