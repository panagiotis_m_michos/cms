<?php

namespace Admin\Cashback;

use Entities\Cashback;
use FForm;

class PaymentForm extends FForm
{
    /**
     * @var bool
     */
    private $hasArchivedCashbacks;

    /**
     * @var bool
     */
    private $hasActiveCashbacks;

    /**
     * @param array $callback
     * @param Cashback[] $cashbacks
     */
    public function startup(array $callback, array $cashbacks)
    {
        $this->searchCashbacksAndSetFlags($cashbacks);
        $this->onValid = $callback;
        $this->buildForm();
        $this->start();
    }

    private function buildForm()
    {
        $this->addSubmit('submit', 'Pay')
            ->class('btn')
            ->style('width: 100px;');

        if ($this->hasActiveCashbacks) {
            $this->addSubmit('archive', 'Archive (invalid account details)')
                ->class('btn');
        }

        if ($this->hasArchivedCashbacks) {
            $this->addSubmit('restore', 'Restore cashback')
                ->class('btn');
        }
    }

    /**
     * @param Cashback[] $cashbacks
     */
    private function searchCashbacksAndSetFlags(array $cashbacks)
    {
        $this->hasActiveCashbacks = FALSE;
        $this->hasArchivedCashbacks = FALSE;

        foreach ($cashbacks as $cashback) {
            if ($cashback->isActive()) {
                $this->hasActiveCashbacks = TRUE;
            } elseif ($cashback->isArchived()) {
                $this->hasArchivedCashbacks = TRUE;
            }
        }
    }

    /**
     * @return bool
     */
    public function hasActiveCashbacks()
    {
        return $this->hasActiveCashbacks;
    }

    /**
     * @return bool
     */
    public function hasArchivedCashbacks()
    {
        return $this->hasArchivedCashbacks;
    }
}