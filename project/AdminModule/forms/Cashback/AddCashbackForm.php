<?php

namespace Admin\Cashback;

use CashBackModule\Factories\CashBackFactory;
use Doctrine\ORM\EntityManager;
use Entities\Cashback;
use Exception;
use FForm;
use InvalidArgumentException;
use Services\CashbackService;
use Services\CompanyService;
use Text;
use Utils\Date;
use UtilsDatePicker;

class AddCashbackForm extends FForm
{
    /**
     * @var IAddCashbackDelegate
     */
    private $delegate;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var CashbackService
     */
    private $cashbackService;

    /**
     * @var CashBackFactory
     */
    private $cashBackFactory;

    /**
     * @param IAddCashbackDelegate $delegate
     * @param CompanyService $companyService
     * @param CashbackService $cashbackService
     * @param CashBackFactory $cashBackFactory
     */
    public function startup(
        IAddCashbackDelegate $delegate,
        CompanyService $companyService,
        CashbackService $cashbackService,
        CashBackFactory $cashBackFactory
    )
    {
        $this->delegate = $delegate;
        $this->companyService = $companyService;
        $this->cashbackService = $cashbackService;
        $this->cashBackFactory = $cashBackFactory;

        $this->buildForm();
        $this->onValid = array($this, 'process');
        $this->start();
    }

    public function process()
    {
        try {
            $values = $this->getValues();
            $company = $this->companyService->getCompanyByCompanyNumber($values['companyNumber']);
            $cashback = $this->cashbackService->getBankCashBackByCompany($company, Cashback::BANK_TYPE_BARCLAYS);
            if (!$cashback) {
                $cashback = $this->cashBackFactory->createBarclays($company, $values['dateLeadSent'], $values['dateAccountOpened']);
                $this->cashbackService->saveCashback($cashback);
            } else {
                throw new InvalidArgumentException('Company already has cashback');
            }

            $this->clean();
            $this->delegate->addCashbackFormSucceeded();
        } catch (Exception $e) {
            $this->delegate->addCashbackFormFailed($e);
        }
    }

    /**
     * @param Text $control
     * @param string $error
     * @return bool|string
     */
    public function Validator_companyExists(Text $control, $error)
    {
        $companyNumber = $control->getValue();
        $company = $this->companyService->getCompanyByCompanyNumber($companyNumber);
        if (!$company) {
            return $error;
        }
        return TRUE;
    }

    /**
     * @param UtilsDatePicker $control
     * @param string $error
     * @return bool|string
     */
    public function Validator_futureDate(UtilsDatePicker $control, $error)
    {
        $value = $control->getValue();
        if ($value > new Date('now')) {
            return $error;
        }
        return TRUE;
    }

    private function buildForm()
    {
        $this->addFieldset('Add Record');

        $this->add('UtilsDatePicker', 'dateLeadSent', 'Lead Date')
            ->size('10')
            ->setDescription('Format dd/mm/yy')
            ->addRule(self::Required, 'Required!')
            ->addRule(array($this, 'Validator_futureDate'), "Date can't be in the future");
        $this['dateLeadSent']->setOption('dateFormat', 'dd/mm/yy');
        $this['dateLeadSent']->setOption('maxDate', 'now');

        $this->addText('companyNumber', 'Company number')
            ->setDescription('Format 01234567')
            ->addRule(self::Required, 'Required!')
            ->addRule(array($this, 'Validator_companyExists'), "Company with this number does not exist");

        $this->add('UtilsDatePicker', 'dateAccountOpened', 'Open Date')
            ->size('10')
            ->setDescription('Format dd/mm/yy')
            ->addRule(self::Required, 'Required!')
            ->addRule(array($this, 'Validator_futureDate'), "Date can't be in the future");
        $this['dateAccountOpened']->setOption('dateFormat', 'dd/mm/yy');
        $this['dateAccountOpened']->setOption('maxDate', 'now');

        $this->addFieldset('Action');

        $this->addSubmit('submit', 'Add Record')
            ->class('btn')
            ->style('width: 200px; height: 30px;');
    }

}
