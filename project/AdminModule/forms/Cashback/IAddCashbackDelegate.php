<?php

namespace Admin\Cashback;

use Exception;

interface IAddCashbackDelegate
{
    public function addCashbackFormSucceeded();

    /**
     * @param Exception $e
     * @return mixed
     */
    public function addCashbackFormFailed(Exception $e);
}
