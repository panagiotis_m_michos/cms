<?php

namespace AdminModule\Factories;

use AdminModule\Views\CompaniesAdminControlerView;
use CompanyFormationModule\Views\CFSummaryControlerView;
use Entities\Company;
use FormSubmissionModule\Factories\FormSubmissionsViewFactory;

class CompaniesAdminControlerViewFactory
{
    /**
     * @var FormSubmissionsViewFactory
     */
    private $formSubmissionsViewFactory;

    /**
     * @param FormSubmissionsViewFactory $formSubmissionsViewFactory
     */
    public function __construct(FormSubmissionsViewFactory $formSubmissionsViewFactory)
    {
        $this->formSubmissionsViewFactory = $formSubmissionsViewFactory;
    }

    /**
     * @param Company $company
     * @return CFSummaryControlerView
     */
    public function create(Company $company)
    {
        $formSubmissionsView = $this->formSubmissionsViewFactory->createFromCompany($company);

        return new CompaniesAdminControlerView($formSubmissionsView);
    }
}
