<?php

namespace AdminModule\Views;

use Entities\CompanyHouse\FormSubmission;
use FormSubmissionModule\Views\FormSubmissionsView;
use FormSubmissionModule\Views\FormSubmissionView;
use FUser;

class CompaniesAdminControlerView
{
    /**
     * @var FormSubmissionsView
     */
    private $formSubmissionsView;

    /**
     * @param FormSubmissionsView $formSubmissionsView
     */
    public function __construct(FormSubmissionsView $formSubmissionsView)
    {
        $this->formSubmissionsView = $formSubmissionsView;
    }

    /**
     * @return bool
     */
    public function hasFormSubmissions()
    {
        return (bool) $this->formSubmissionsView->getFormSubmissionViews();
    }

    /**
     * @return FormSubmissionView[]
     */
    public function getFormSubmissions()
    {
        return $this->formSubmissionsView->getFormSubmissionViews();
    }

    /**
     * @param FUser $user
     * @param FormSubmissionView $formSubmissionView
     * @return bool
     */
    public function canResubmit(FUser $user, FormSubmissionView $formSubmissionView)
    {
        //todo: psc - temporarily disabled until new CH entities are updated with PSC
        return FALSE;

        return $user->role->isAdmin() && $formSubmissionView->isResubmittable();
    }

    /**
     * @return bool
     */
    public function canShowInternalFailureWarning()
    {
        return $this->formSubmissionsView->hasInternalFailureSubmission();
    }

    /**
     * @param FormSubmissionView $formSubmissionView
     * @return bool
     */
    public function canViewMinutes(FormSubmissionView $formSubmissionView)
    {
        return $formSubmissionView->isViewable();
    }

    /**
     * @return bool
     */
    public function canRemoveLink()
    {
        foreach ($this->getFormSubmissions() as $formSubmissionView) {
            if (!$formSubmissionView->isCancelled() && $formSubmissionView->isPending() && $formSubmissionView->isCompanyIncorporationType()) {
                return TRUE;
            }
        }

        return FALSE;
    }
}
