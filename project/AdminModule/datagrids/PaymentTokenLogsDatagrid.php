<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    PaymentTokenLogsDatagrid.php 2012-09-04 <razvanp@madesimplegroup.com>
 */

class PaymentTokenLogsDatagrid extends DataGrid
{
    /**
     * @return void
     */
    protected function init()
    {
        $this->enableExport();
        $this->getPaginator()->setItemsPerPage('20');
        
        // --- column ---		
        $this->addTextColumn('token', 'Token', 300);
        $this->addTextColumn('tokenStatus', 'Status', 90);
		$this->addTextColumn('dtm', 'Date', 110);

        $router = FApplication::$router;
        $action = $this->addActionColumn('actions', 'Actions', 50);
        $action->getHeaderPrototype()->class('center');

         //view
		$label = Html::el('a')->setText('View');
        $url = $router->link('view');
        $action->addAction('view', $label, $url);
        
        $filter1 = new DataGridFilterSelect('tokenStatus', 'Status');
        $filter1->setOptions(TokenLogs::$errorStatusTypeId);
        $filter2 = new DataGridFilterDate('dtm', 'Date');
        

        $this->getFilter()->addFilter($filter1);
        $this->getFilter()->addFilter($filter2);
    }
        
}
