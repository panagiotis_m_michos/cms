<?php

class IncorporationAdminDatagrid extends DataGrid
{
    /**
     * @return void
     */
    protected function init()
    {
        //$this->enableExport();
        $this->disablePaginator();
        $this->getPaginator()->setItemsPerPage('20');
        // --- column ---		
        $this->addTextColumn('dti', 'Date', 85);
        $this->addTextColumn('incorporated', 'Incorporated', 85);
    }
}
