<?php

namespace Datagrid\Admin;


use DataGridFilterItem_Abstract;
use FControl;
use FForm;
use IDataSource;
use Tracy\Debugger;

class DatagridCallbackFilter extends DataGridFilterItem_Abstract
{
    /**
     * @var FControl
     */
    private $control;

    /**
     * @var callable
     */
    private $filter;

    public function __construct(FControl $control, callable $filter)
    {
        parent::__construct($control->getName(), $control->getLabel());
        $this->control = $control;
        $this->filter = $filter;
    }

    /**
     * @param IDataSource $dataSource
     * @param mixed $value
     */
    public function applyFilter(IDataSource $dataSource, $value)
    {
        call_user_func_array($this->filter, [$dataSource, $value]);
    }

    /**
     * @param FForm $form
     * @return FControl
     */
    public function addFormControl(FForm $form)
    {
        $this->control->owner = $form;
        $form->addControl($this->control);
    }

    public function getName()
    {
        return $this->control->getName();
    }
}
