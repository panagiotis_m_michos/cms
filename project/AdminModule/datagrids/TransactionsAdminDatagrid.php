<?php

class TransactionsAdminDatagrid extends DataGrid
{

    public function Callback_failedTransaction(DataGridColumn_Abstract $column, $text, DibiRow $row)
    {
        $text = Html::el('span')
                ->setText($text);
        if ($row->statusId == Transaction::STATUS_FAILED) {
            $text->style('color: red;');
        }
        return $text;
    }

    protected function init()
    {
        $router = FApplication::$router;

        $this->addTextColumn('transactionId', 'Id', 60)
                ->addCallback(array($this, 'Callback_failedTransaction'));
        $this->addTextColumn('statusId', 'Status', 70)
                ->addCallback(array($this, 'Callback_failedTransaction'))
                ->replacement(Transaction::$statuses);
        $this->addTextColumn('typeId', 'Type', 50)
                ->addCallback(array($this, 'Callback_failedTransaction'))
                ->replacement(Transaction::$types);
        $this->addTextColumn('email', 'Customer')
                ->addCallback(array($this, 'Callback_failedTransaction'));
        $this->addDateColumn('dtc', 'Created', 120, 'd/m/Y @ H:i')
                ->addCallback(array($this, 'Callback_failedTransaction'));

        $action = $this->addActionColumn('actions', 'Actions', 60);
        $action->getHeaderPrototype()->class('center');

        // view
        $label = Html::el('a')
                ->setText('View');
        $url = $router->link('view');
        $action->addAction('view', $label, $url);

        $this->addTextFilter('email', 'Email:');
        $this->addSelectFilter('statusId', 'Status:', Transaction::$statuses, 't');
        $this->addSelectFilter('typeId', 'Type:', Transaction::$types);
    }
}
