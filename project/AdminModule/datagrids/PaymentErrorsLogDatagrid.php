<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    PaymentErrorsLog.php 2012-07-19 razvanp@madesimplegroup.com
 */

class PaymentErrorsLogDatagrid extends DataGrid
{
    /**
     * @return void
     */
    protected function init()
    {
        $this->enableExport();
        $this->getPaginator()->setItemsPerPage('20');
        // --- column ---		
        //$this->addTextColumn('errorId', 'Id', 70);
        $this->addTextColumn('name', 'Name', 120);
        $this->addTextColumn('paymentTypeId', 'Provider', 50);
        $this->addTextColumn('errorTypeId', 'Error', 80);
		$this->addTextColumn('vendorTXCode', 'Code');
		$this->addTextColumn('dtc', 'Date', 110);
		//$this->addTextColumn('paymentSystem', 'System', 90);
//        $this->addTextColumn('address1', 'Address1', 75);
//        $this->addTextColumn('address2', 'Address2', 85);
//        $this->addTextColumn('city', 'City', 85);
//        $this->addTextColumn('postCode', 'PostCode', 85);
//        $this->addTextColumn('country', 'Country', 30);      
//        $this->addTextColumn('state', 'state', 85); 	
//        $this->addTextColumn('phone', 'phone', 85);
        
//        $this->addSelectFilter('status', 'Status: ', Cashback::$types);
//        $this->addNumericFilter('amount','Amount: ');
//        $this->addTextFilter('companyNumber', 'Co Number: ');
        
        $router = FApplication::$router;
        $action = $this->addActionColumn('actions', 'Actions', 50);
        $action->getHeaderPrototype()->class('center');

         //view
		$label = Html::el('a')->setText('View');
        $url = $router->link('view');
        $action->addAction('view', $label, $url);
        
        
        $filter = new DataGridFilterText('name', 'Name');
        $filter2 = new DataGridFilterSelect('paymentTypeId', 'Provider');
        $filter2->setOptions(PaymentErrorsLog::$paymentStatusTypeId);
        $filter3 = new DataGridFilterSelect('errorTypeId', 'Error Type');
        $filter3->setOptions(PaymentErrorsLog::$errorStatusTypeId);
        $filter4 = new DataGridFilterText('vendorTXCode', 'VendorTXCode');
        //$filter4->style('width:400px');
        $this->getFilter()->addFilter($filter);
        $this->getFilter()->addFilter($filter2);
        $this->getFilter()->addFilter($filter3);
        $this->getFilter()->addFilter($filter4);
    }
        
}
