<?php

/**
 * CMS
 *
 * @license    GPL
 * @category   Project
 * @package    Datagrid
 * @author     Nikolai Senkevich    nikolais@madesimplegroup.com
 * @version    2012-03-10
 */

class CustomerCompaniesAdminDatagrid extends DataGrid
{
    /**
     * @return void
     */
    protected function init()
    {
        $this->enableExport();
        $this->getPaginator()->setItemsPerPage('500');
        
        // --- column ---		
        $this->addTextColumn('company_id', 'Id', 70);
        $this->addTextColumn('company_name', 'Company name');
        $this->addTextColumn('company_number', 'Company number', 125);
        $this->addTextColumn('incorporation_date', 'Incorporation Date', 130);
		
		$router = FApplication::$router;
        $action = $this->addActionColumn('actions', 'Actions', 55);
        $action->getHeaderPrototype()->class('center');
        
		// edit
        $label = Html::el('a')->setText('View');
        $url = $router->link(CompaniesAdminControler::COMPANIES_PAGE);
        
        $action->addAction('view', $label, $url.'view/');
    }
    
}
