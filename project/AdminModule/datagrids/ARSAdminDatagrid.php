<?php

/**
 * CMS
 *
 * @license    GPL
 * @category   Project
 * @package    Datagrid
 * @author     Nikolai Senkevich    nikolais@madesimplegroup.com
 * @version    2012-03-24
 */

class ARSAdminDatagrid extends DataGrid
{
    /**
     * @return void
     */
    protected function init()
    {
        $this->enableExport();
        $this->getPaginator()->setItemsPerPage('20');
        // --- column ---		
        $this->addTextColumn('annualReturnServiceId', 'ARS Id', 85);
        $this->addTextColumn('statusId',   'Status', 85);
        //$this->addTextColumn('companyId',  'Company Id', 85)->addCallback(array($this, 'Callback_companyLink'));
        $this->addTextColumn('customerId', 'Customer Id', 85);
		$this->addTextColumn('productId',  'Company Id', 75)->addCallback(array($this, 'Callback_companyLink'));
        
        $this->addTextColumn('companyId',  'View', 85)->addCallback(array($this, 'Callback_viewLink'));
        
        $this->addSelectFilter('statusId',   'Status: ', AnnualReturnServiceModel::$statues);
        $this->addTextFilter('customerId', 'Custtomer: ');
        $this->addTextFilter('companyId',  'Cmpany Id: ');      
    }
    
     /**
     * @param DataGridTextColumn $column
     * @param string $text
     * @param DibiRow $row
     * @return Html
     */
    public function Callback_companyLink(DataGridTextColumn $column, $text, DibiRow $row)
    {
        $el = Html::el();      
        $el->create('a')->href(FApplication::$router->absoluteAdminLink(CompaniesAdminControler::COMPANIES_PAGE).'view/?company_id='.$row->companyId)->setText($row->companyId);
        return $el;
    }
    
    public function Callback_viewLink(DataGridTextColumn $column, $text, DibiRow $row)
    {
        $el = Html::el();      
        $el->create('a')->href(FApplication::$router->absoluteAdminLink(CompaniesAdminControler::COMPANIES_PAGE).'ARcompanyDetails/?company_id='.$row->companyId)->setText('View AR');
        return $el;
    }
    
    
}
