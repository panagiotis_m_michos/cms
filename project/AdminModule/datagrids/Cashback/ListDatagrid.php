<?php

namespace Admin\Cashback;

use CashbackAdminControler;
use DataGrid;
use DataGridAction;
use DataGridTextColumn;
use DibiRow;
use Entities\Cashback;
use FRouter;
use Html;
use Nette\Utils\Arrays;

class ListDataGrid extends DataGrid
{
    /**
     * @var FRouter
     */
    private $router;

    /**
     * @var array
     */
    private static $tooltips = [
        Cashback::STATUS_IMPORTED => 'Waiting for customer bank account details',
        Cashback::STATUS_ELIGIBLE => 'Waiting for payment to be made',
        Cashback::STATUS_PAID => 'Cash back paid to bank account details or credited on account',
    ];

    /**
     * @param FRouter $router
     */
    public function setRouter($router)
    {
        $this->router = $router;
    }

    /**
     * @param DataGridAction $action
     * @param DibiRow $row
     * @return DataGridAction|Html
     */
    public function Action_details(DataGridAction $action, DibiRow $row)
    {
        $url = $this->router->link(
            "CashbackAdminControler::PAGE_CASHBACK cashbacksDetails",
            [
                'customerId' => $row['customerId'],
                'packageTypeId' => $row['packageTypeId'],
                'statusId' => $row['statusId'],
                'groupId' => $row['groupId'],
            ]
        );

        $action = Html::el('a')
            ->setText('Details')
            ->href($url);

        return $action;
    }

    /**
     * @param DataGridTextColumn $column
     * @param mixed $text
     * @param DibiRow $row
     * @return string
     */
    public function Callback_status(DataGridTextColumn $column, $text, DibiRow $row)
    {
        $tooltip = $tooltip = Arrays::get(self::$tooltips, $row['statusId'], NULL);
        $column->getCellPrototype()->title($tooltip);
        return  Arrays::get(Cashback::$statuses, $row['statusId'], NULL);
    }

    protected function init()
    {
        $this->enableExport();
        $this->getPaginator()->setItemsPerPage('20');

        // columns
        $this->addTextColumn('customerId', 'Customer ID', 80);
        $this->addTextColumn('customerName', 'Name');
        $this->addTextColumn('packageTypeId', 'Packages', 70)
            ->replacement(Cashback::$packageTypes);
        $this->addDateColumn('datePaid', 'Paid', 70, 'd/m/Y');
        $textColumn = $this->addTextColumn('statusId', 'Status', 80)
            ->addCallback([$this, 'Callback_status']);
        $textColumn->disableOrder();
        $header = $textColumn->getHeaderPrototype();
        $text = <<<'EOD'
Imported: Waiting for customer bank account details
Eligible: Waiting for payment to be made
Paid: Cash back paid to bank account details or credited on account
Outdated: Record too old to be eligible
EOD;
        $header->create('span')->class('question-pop')->title($text)->create('i')->class('fa fa-question-circle');

        $this->addCurrencyColumn('totalAmount', 'Total', 60);

        // actions
        $actions = $this->addActionColumn('actions', 'Actions', 60);
        $actions->addAction('details')
            ->setCallback([$this, 'Action_details']);

        // filters
        $this->addSelectFilter('statusId', 'Status: ', Cashback::$statuses, 'cb');
        $this->addTextFilter('customerId', 'Customer ID: ', 'cb');
    }
}
