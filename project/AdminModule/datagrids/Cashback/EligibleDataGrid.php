<?php

namespace Admin\Cashback;

use DataGrid;
use DataGridAction;
use DibiRow;
use Entities\Cashback;
use FRouter;
use Html;

class EligibleDataGrid extends DataGrid
{
    /**
     * @var FRouter
     */
    protected $router;

    /**
     * @param FRouter $router
     */
    public function setRouter(FRouter $router)
    {
        $this->router = $router;
    }

    /**
     * @param DataGridAction $action
     * @param DibiRow $row
     * @return Html
     */
    public function Callback_paymentAction(DataGridAction $action, DibiRow $row)
    {
        $url = $this->router->link(
            'payment',
            array(
                'customerId' => $row['customerId'],
                'packageTypeId' => $row['packageTypeId'],
            )
        );

        $pay = Html::el('a')
            ->href($url)
            ->setHtml('Pay');

        return $pay;
    }

    protected function init()
    {
        $this->enableExport();
        $this->disableOrder();
        $this->getPaginator()->setItemsPerPage('20');

        // columns
        $this->addTextColumn('customerId', 'Customer ID', 80);
        $this->addTextColumn('customerName', 'Name');
        $this->addTextColumn('packageTypeId', 'Packages', 80)
            ->replacement(Cashback::$packageTypes);
        $this->addCurrencyColumn('totalAmount', 'Amount', 80);

        // actions
        $action = $this->addActionColumn('actions', 'Actions', 80);
        $action->getHeaderPrototype()->class('center');
        $action->addAction('payment')
            ->setCallback(array($this, 'Callback_paymentAction'));

        // filters
        $this->addTextFilter('firstName', 'First Name: ');
        $this->addTextFilter('lastName', 'Last Name: ');
        $this->addTextFilter('customerId', 'Customer ID: ', 'c');
        $this->addSelectFilter('packageTypeId', 'Packages', Cashback::$packageTypes);
    }

    /**
     * @param Html $row
     * @param DibiRow $cashBack
     */
    public function modifyRow(Html $row, $cashBack)
    {
        if ($cashBack->isArchived) {
            $row->class('archived-cashback');
        }
    }


}
