<?php

use Entities\Order;

class OrdersAdminDatagrid extends DataGrid
{
    /**
     * @return void
     */
    protected function init()
    {
        $this->disableOrder();
        $this->getPaginator()->setItemsPerPage('500');

        // --- columns ---
        $this->addTextColumn('orderId', 'Id', 50);
        $this->addTextColumn('customerName', 'Name', 150);
        $this->addTextColumn('customerEmail', 'Email');
        $this->addTextColumn('total', 'Total');
        $this->addDateColumn('dtc', 'Date', 120, 'd/m/Y G:i');

        // --- actions ---
        $action = $this->addActionColumn('actions', 'Actions', 70);
        $url = FApplication::$router->link(OrdersAdminControler::ORDERS_PAGE.' view');
        $action->addAction('view', 'View', $url);

        $this->addTextFilter('orderId', 'Order number:', 'o');
        $this->addTextFilter('cardNumber', 'Card Number:', 't');
        $this->addTextFilter('cardHolder', 'Card holder:', 't');
        $this->addTextFilter('vendorTXCode', 'Vendor TX Code:', 't');
        $this->addTextFilter('vpsAuthCode', 'VPS Auth Code:', 't');
    }
}
