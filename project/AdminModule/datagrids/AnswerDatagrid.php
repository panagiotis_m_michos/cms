<?php

namespace Datagrid\Admin;

use DataGrid;

class AnswersDatagrid extends DataGrid
{
    protected function init()
    {
        $this->enableExport();
        $this->addTextColumn('answerId', 'Id');
        $this->addTextColumn('text', 'Text');
    }
}
