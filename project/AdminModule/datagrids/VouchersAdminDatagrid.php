<?php

use VoucherModule\Entities\Voucher;

class VouchersAdminDatagrid extends DataGrid
{
    protected function init()
    {
        $this->enableExport();
        //$this->setMassDelete(true);
        // --- column ---
        //$this->addMassDeleteColumn('voucherId', null, 20);
        $this->addTextColumn('voucherId', 'Id', 40);
        $this->addTextColumn('name', 'Name');
        $this->addTextColumn('voucherCode', 'Code', 90);
        $this->addTextColumn('typeValue', 'Value', 40)->addCallback([$this, 'Callback_typeValue']);
        $this->addTextColumn('minSpend', 'Min Spend', 40);
        $this->addTextColumn('usageLimit', 'Usage Limit', 40);
        $this->addTextColumn('used', 'Used', 40);
        $this->addTextColumn('dtExpiry', 'Expiry', 75)->addCallback([$this, 'Callback_dtExpiry']);

        $router = FApplication::$router;
        $action = $this->addActionColumn('actions', 'Actions', 120);
        $action->getHeaderPrototype()->class('center');
        // edit
        $label = Html::el('a')->setText('Edit');
        $url = $router->link('edit');
        $action->addAction('edit', $label, $url);

        // view
        $label = Html::el('a')->setText('Delete')->class('buttonCheckSecondParent');
        $url = $router->link('delete');
        $action->addAction('delete', $label, $url);

        $this->addTextFilter('name', 'Name');
        $this->addTextFilter('voucherCode', 'Code');
    }

    /**
     * @param DataGridTextColumn $column
     * @param string $value
     * @param DibiRow $row
     * @return string
     */
    public function Callback_typeValue(DataGridTextColumn $column, $value, DibiRow $row)
    {
        if ($row['typeId'] == Voucher::TYPE_AMOUNT) {
            return '&pound;' . $value;
        } elseif ($row['typeId'] == Voucher::TYPE_PERCENT) {
            return $value . '%';
        }

        return $value;
    }

    /**
     * @param DataGridTextColumn $column
     * @param string $value
     * @param DibiRow $row
     * @return string
     */
    public function Callback_dtExpiry(DataGridTextColumn $column, $value, DibiRow $row)
    {
        return ($value && strtotime($value)) ? (new DateTime($value))->format('d/m/Y') : NULL;
    }
}
