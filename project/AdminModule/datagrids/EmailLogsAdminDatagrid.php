<?php

use Datagrid\Admin\DatagridCallbackFilter;
use LoggableModule\Entities\EmailLog;

class EmailLogsAdminDatagrid extends DataGrid
{
    /**
     * @return void
     */
    protected function init()
    {
        $this->disableOrder();

        // --- columns ---
        $this->addTextColumn('emailLogId', 'Id', 50);
        $this->addTextColumn('emailSubject', 'Email Subject')->getCellPrototype()->class='noOverflow';
        $this->addTextColumn('emailTo', 'To')->addCallback([$this, 'Callback_emailTo']);
        $this->addDateColumn('dtc', 'Created', 120, 'd/m/Y G:i');

        // --- actions ---
        $action = $this->addActionColumn('actions', 'Actions', 70);
        $url = FApplication::$router->link(EmailLogsAdminControler::EMAIL_LOG_PAGE.' view');
        $action->addAction('view', 'View', $url);

        // --- filter: company name
        $filter = new DataGridFilterText('emailTo', 'To:', 'l');
        $this->getFilter()->addFilter($filter);

        $hideInternalEmailsCheckbox = new SingleCheckbox(
            'hideInternalEmail',
            'Hide Internal Emails',
            $this
        );
        $hideInternalEmailsCheckbox->setValue(1);

        $filter = new DatagridCallbackFilter(
            $hideInternalEmailsCheckbox,
            function (DoctrineDataSource $dataSource, $value) {
                if ($value) {
                    $dataSource->where('l.emailName <> :emailName', 'emailName', 'Internal');
                }
            }
        );
        $this->getFilter()->addFilter($filter);
    }

    /**
     * @param DataGridTextColumn $column
     * @param string $text
     * @param EmailLog $row
     * @return Html
     */
    public function Callback_emailTo(DataGridTextColumn $column, $text, EmailLog $row)
    {
        return implode(', <br />', $text);
    }
}
