<?php

namespace Datagrid\Admin;

use DataGrid;
use DataGridTextColumn;
use DateTime;
use Entities\Service;
use Factories\ServiceViewFactory;
use FApplication;
use Html;
use IDataSource;

class ServicesAdminDatagrid extends DataGrid
{
    /**
     * @var ServiceViewFactory
     */
    private $serviceViewFactory;

    /**
     * @param IDataSource $datasource
     * @param string $primaryKey
     * @param ServiceViewFactory $serviceViewFactory
     */
    public function __construct(IDataSource $datasource, $primaryKey, ServiceViewFactory $serviceViewFactory)
    {
        parent::__construct($datasource, $primaryKey);

        $this->serviceViewFactory = $serviceViewFactory;
    }

    protected function init()
    {
        // --- column ---
        $this->addTextColumn('serviceId', 'Id', 55);

        $serviceColumn = $this->addTextColumn('serviceName', 'Service')->addCallback([$this, 'Callback_serviceName']);
        $serviceColumn->getCellPrototype()->class[] = 'noOverflow';

        $statusColumn = $this->addTextColumn('serviceTypeId', 'Status', 90, NULL)->addCallback([$this, 'Callback_status']);
        $statusColumn->disableOrder();

        // "dtm" is used as dummy column, we are running out of columns to use
        $this->addTextColumn('dtm', 'Auto-Renew', 75, NULL)
            ->addCallback([$this, 'Callback_autoRenew'])
            ->disableOrder();

        $text = <<<'EOD'
On hold: Service purchased, but not started
Active: Service active and payment up to date
Overdue: Service limited and payment overdue (28 days grace period)
Expired: Service expired
Downgraded: Service downgraded to different service
Upgraded: Service upgraded to different service
EOD;
        $statusColumn->getHeaderPrototype()
            ->create('span')->class('question-pop')->title($text)->create('i')->class('fa fa-question-circle');

        $this->addDateColumn('dtStart', 'Start date', 65, 'd/m/Y');
        $this->addDateColumn('dtExpires', 'End date', 65, 'd/m/Y');

        // "dtc" is used as dummy column
        $this->addTextColumn('dtc', 'Reminders', 75)
            ->addCallback([$this, 'Callback_reminders'])
            ->disableOrder();

        $this->addTextColumn('order', 'Actions', 65)
            ->addCallback([$this, 'Callback_orderLink'])
            ->disableOrder();
    }

    /**
     * @param Html $row
     * @param Service $tableRow
     */
    public function modifyRow(Html $row, $tableRow)
    {
        $serviceView = $this->serviceViewFactory->create([$tableRow]);
        if ($serviceView->isActive() && !$serviceView->isOverdue()) {
            $row->class('bold');
        } elseif ($serviceView->isExpired()) {
            $row->class('disabled');
        }
    }

    /**
     * @param DataGridTextColumn $column
     * @param DateTime $dtc
     * @param Service $service
     * @return Html
     */
    public function Callback_reminders(DataGridTextColumn $column, DateTime $dtc, Service $service)
    {
        $remindersEnabled = $this->serviceViewFactory->create([$service])->hasEmailReminders();

        $el = Html::el();
        $bold = $el->create('b');
        $link = $el->create('a');
        if ($remindersEnabled) {
            $bold->setText('On ');
            $link->href(
                '',
                [
                    'company_id' => $service->getCompany()->getId(),
                    'service_id' => $service->getId(),
                    'reminders' => 'disable',
                ]
            )->setText('Turn Off');
        } else {
            $bold->setText('Off ');
            $link->href(
                '',
                [
                    'company_id' => $service->getCompany()->getId(),
                    'service_id' => $service->getId(),
                    'reminders' => 'enable',
                ]
            )->setText('Turn On');
        }
        return $el;
    }

    /**
     * @param DataGridTextColumn $column
     * @param string $text
     * @param Service $service
     * @return string
     */
    public function Callback_autoRenew(DataGridTextColumn $column, $text, Service $service)
    {
        $services = [$service];
        $serviceView = $this->serviceViewFactory->create($services);
        $token = $service->getCompany()->getCustomer()->getActiveToken();

        $el = Html::el();
        $bold = $el->create('b');
        $link = $el->create('a');
        if ($serviceView->isAutoRenewalEnabled()) {
            $bold->setText('On ');
            if (!$serviceView->isExpired()) {
                $link
                    ->href(
                        '',
                        [
                            'company_id' => $service->getCompany()->getId(),
                            'service_id' => $service->getId(),
                            'renewal' => 'disable',
                        ]
                    )
                    ->setText('Turn Off');
            }
        } else {
            $bold->setText('Off ');
            if ($token && ($serviceView->canToggleAutoRenewal() || $serviceView->isOnHold())) {
                $confirmation = "Do you want to enable auto-renewal?\nPayment method to be used: {$token->getCardTypeText()} ending in {$token->getCardNumber()}.";
                $confirmation .= $serviceView->expiresToday() || $serviceView->isOverdue() ? "\nWe'll process this payment overnight and email the customer once confirmed." : '';

                $link
                    ->href(
                        '',
                        [
                            'company_id' => $service->getCompany()->getId(),
                            'service_id' => $service->getId(),
                            'renewal' => 'enable',
                        ]
                    )
                    ->setText('Turn On')
                    ->{'data-confirmation'} = $confirmation;
            }
        }

        return $el;
    }

    /**
     * @param DataGridTextColumn $column
     * @param mixed $text
     * @param Service $service
     * @return mixed
     */
    public function Callback_status(DataGridTextColumn $column, $text, Service $service)
    {
        $status = $this->serviceViewFactory->create([$service])->getStatus();
        $tooltips = [
            'On Hold' => 'Service purchased, but not started',
            'Active' => 'Service active and payment up to date',
            'Overdue' => 'Service limited and payment overdue (28 days grace period)',
            'Expired' => 'Service expired',
            'Downgraded' => 'Service downgraded to different service',
            'Upgraded' => 'Service upgraded to different service',
        ];

        $column->getCellPrototype()->title(isset($tooltips[$status]) ? $tooltips[$status] : '');

        return $status;
    }

    /**
     * @param DataGridTextColumn $column
     * @param mixed $text
     * @param Service $service
     * @return mixed
     */
    public function Callback_orderLink(DataGridTextColumn $column, $text, Service $service)
    {
        $orderId = $service->getOrder() ? $service->getOrder()->getId() : NULL;
        if (!$orderId) {
            return Html::el();
        }
        $el = Html::el();
        $el->create('a')
            ->href(
                FApplication::$router->link("OrdersAdminControler::ORDERS_PAGE view"),
                ["order_id" => $orderId]
            )
            ->setText('View Order');
        return $el;
    }

    /**
     * @param DataGridTextColumn $column
     * @param mixed $text
     * @param Service $service
     * @return string
     */
    public function Callback_serviceName(DataGridTextColumn $column, $text, Service $service)
    {
        $order = $service->getOrder();
        $suffix = ($order && $order->isComplimentaryOrder()) ? ' (Complimentary)' : '';

        return $service->getServiceName() . $suffix;
    }
}
