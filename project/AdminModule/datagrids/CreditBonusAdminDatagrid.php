<?php

/**
 * CMS
 *
 * @license    GPL
 * @category   Project
 * @package    Datagrid
 * @author     Tomas Jakstas tomasj@madesimplegroup.com
 * @version    2011-11-01
 */

class CreditBonusAdminDatagrid extends DataGrid
{
    /**
     * @return void
     */
    protected function init()
    {
        $this->enableExport();
        // --- column ---
        $this->addTextColumn('email', 'Email');
        $this->addTextColumn('amount', 'Amount');
        $this->addTextColumn('dtc', 'Date Created');

        $this->addTextFilter('email', 'Email');
    }
    
}
