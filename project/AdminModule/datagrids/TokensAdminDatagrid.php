<?php

/**
 * CMS
 *
 * @license    GPL
 * @category   Project
 * @package    Datagrid
 * @author     Nikolai Senkevich    nikolais@madesimplegroup.com
 * @version    2012-03-24
 */

class TokensAdminDatagrid extends DataGrid
{
    /**
     * @return void
     */
    protected function init()
    {
        $this->enableExport();
        $this->getPaginator()->setItemsPerPage('20');
        // --- column ---		
        $this->addTextColumn('customerId', 'Customer', 70);
        $this->addTextColumn('token', 'Token');
        $this->addTextColumn('cardNumber', 'card', 50);
        $this->addTextColumn('firstname', 'Firstname', 70);
		$this->addTextColumn('surname', 'Surname', 70);
//        $this->addTextColumn('address1', 'Address1', 75);
//        $this->addTextColumn('address2', 'Address2', 85);
//        $this->addTextColumn('city', 'City', 85);
//        $this->addTextColumn('postCode', 'PostCode', 85);
//        $this->addTextColumn('country', 'Country', 30);      
//        $this->addTextColumn('state', 'state', 85); 	
//        $this->addTextColumn('phone', 'phone', 85);
        
//        $this->addSelectFilter('status', 'Status: ', Cashback::$types);
//        $this->addNumericFilter('amount','Amount: ');
//        $this->addTextFilter('companyNumber', 'Co Number: ');
        
        $router = FApplication::$router;
        $action = $this->addActionColumn('actions', 'Actions', 50);
        $action->getHeaderPrototype()->class('center');

        // delete
		$label = Html::el('a',array('onclick' =>'return confirm("Are you sure?")'))->setText('Delete');
        $url = $router->link('delete');
        $action->addAction('delete', $label, $url);
        
    }
        
}
