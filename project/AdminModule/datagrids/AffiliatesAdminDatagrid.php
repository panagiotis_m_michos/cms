<?php

/**
 * CMS
 *
 * @license    GPL
 * @category   Project
 * @package    Datagrid
 * @author     Nikolai Senkevich    nikolais@madesimplegroup.com
 * @version    2012-03-22
 */

class AffiliatesAdminDatagrid extends DataGrid
{
    /**
     * @return void
     */
    protected function init()
    {
        $this->enableExport();
        $this->getPaginator()->setItemsPerPage('25');
        
        // --- column ---		
        $this->addTextColumn('affiliateId', 'Id', 45);
        $this->addTextColumn('name', 'Name');
        $this->addTextColumn('web', 'Web', 200);
        $this->addTextColumn('dtc', 'Created', 100);
		
		$router = FApplication::$router;
        $action = $this->addActionColumn('actions', 'Actions', 55);
        $action->getHeaderPrototype()->class('center');
        
		// edit
        $label = Html::el('a')->setText('View');
        $url = $router->link(AffiliatesAdminControler::PAGE);
        
        $action->addAction('view', $label, $url.'view/');
        
        
        $this->addTextFilter('name', 'Name: ');
        $this->addTextFilter('web',  'Web: ');
        $this->addTextFilter('email','Email: ');
        
    }
    
}
