<?php

namespace BankingModule\Repositories;

use BankingModule\Entities\CompanyCustomer;
use DateTime;
use Doctrine\ORM\Query\Expr\Join;
use Entities\Company;
use Repositories\BaseRepository_Abstract;

class BankingRepository extends BaseRepository_Abstract
{
    /**
     * @param Company $company
     * @return bool
     */
    public function hasDetailsFilled(Company $company)
    {
        return (bool) $this->createQueryBuilder('cc')
            ->select('COUNT(cc)')
            ->where('cc.company = :company')->setParameter('company', $company)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @return CompanyCustomer[]
     */
    public function getCompaniesToSendTsbLeads()
    {
        return $this->createQueryBuilder('cc')
            ->leftJoin('BankingModule\Entities\TsbLead', 't', Join::WITH, 't.company = cc.company')
            ->join('cc.company', 'c')
            ->where('cc.bankTypeId = :tsbBank')->setParameter('tsbBank', CompanyCustomer::BANK_TYPE_TSB)
            ->andWhere('t IS NULL')
            ->andWhere('c.companyNumber IS NOT NULL')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param DateTime $from
     * @param DateTime $to
     * @return CompanyCustomer[]
     */
    public function getTsbLeadsDetailsSentBetween(DateTime $from, DateTime $to)
    {
        return $this->createQueryBuilder('cc')
            ->innerJoin('BankingModule\Entities\TsbLead', 't', Join::WITH, 't.company = cc.company')
            ->where('t.dtc >= :from')->setParameter('from', $from)
            ->andWhere('t.dtc <= :to')->setParameter('to', $to)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Company $company
     * @return CompanyCustomer
     */
    public function getTsbBankDetailsByCompany(Company $company)
    {
        return $this->createQueryBuilder('cc')
            ->where('cc.company = :company')->setParameter('company', $company)
            ->andWhere('cc.bankTypeId = :bankType')->setParameter('bankType', CompanyCustomer::BANK_TYPE_TSB)
            ->getQuery()
            ->getOneOrNullResult();
    }
    /**
     * @param Company $company
     * @return array
     */
    public function getAppliedBanks(Company $company)
    {
        $banks = $this->createQueryBuilder('cc')
            ->select('cc.bankTypeId')
            ->where('cc.company = :company')->setParameter('company', $company)
            ->getQuery()
            ->getArrayResult();
        return array_column($banks, 'bankTypeId');
    }

    /**
     * @param Company $company
     * @param string $bank
     * @return int
     */
    public function getLatestBankDetailsByCompany(Company $company, $bank)
    {
        return $this->createQueryBuilder('cc')
            ->where('cc.company = :company')->setParameter('company', $company)
            ->andWhere('cc.bankTypeId = :bankType')->setParameter('bankType', $bank)
            ->orderBy('cc.dtc', 'DESC')
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }
}
