<?php

namespace BankingModule\Repositories;

use BankingModule\Config\EventLocator;
use BankingModule\Entities\CompanyCustomer;
use BankingModule\Entities\TsbLead;
use DateTime;
use Doctrine\ORM\Query\Expr\Join;
use Entities\Company;
use Entities\Event;
use Repositories\BaseRepository_Abstract;

class TsbLeadRepository extends BaseRepository_Abstract
{
    /**
     * @return TsbLead[]
     */
    public function getLeadsToSendFirstTsbReminder()
    {
        return $this->createQueryBuilder('t')
            ->innerJoin(CompanyCustomer::class, 'cc', Join::WITH, '(cc.company = t.company AND cc.bankTypeId = :tsbBankTypeId)')
            ->leftJoin(Event::class, 'e', Join::WITH, '(e.objectId = t.id AND e.eventKey = :event)')
            ->where('t.dtc > :from')
            ->andWhere('t.dtc <= :to')
            ->andWhere('e IS NULL')
            ->andWhere('t.accountOpened = FALSE')
            ->setParameters(
                [
                    'tsbBankTypeId' => CompanyCustomer::BANK_TYPE_TSB,
                    'event' => EventLocator::TSB_APPLICATION_FIRST_REMINDER,
                    'from' => new DateTime('-5 weeks'),
                    'to' => new DateTime('-1 week'),
                ]
            )
            ->getQuery()
            ->getResult();
    }

    /**
     * @return TsbLead[]
     */
    public function getLeadsToSendSecondTsbReminder()
    {
        return $this->createQueryBuilder('t')
            ->innerJoin(CompanyCustomer::class, 'cc', Join::WITH, '(cc.company = t.company AND cc.bankTypeId = :tsbBankTypeId)')
            ->leftJoin(Event::class, 'e', Join::WITH, '(e.objectId = t.id AND e.eventKey = :event)')
            ->where('t.dtc <= :to')
            ->andWhere('e IS NULL')
            ->andWhere('t.accountOpened = FALSE')
            ->setParameters(
                [
                    'tsbBankTypeId' => CompanyCustomer::BANK_TYPE_TSB,
                    'event' => EventLocator::TSB_APPLICATION_SECOND_REMINDER,
                    'to' => new DateTime('-5 weeks'),
                ]
            )
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Company $company
     * @return TsbLead|NULL
     */
    public function getTsbLeadByCompany(Company $company)
    {
        return $this->createQueryBuilder('t')
            ->where('t.company = :company')
            ->setParameter('company', $company)
            ->orderBy('t.dtc', 'DESC')
            ->getQuery()
            ->getOneOrNullResult();
    }
}
