<?php

namespace BankingModule\Factories;

use BankingModule\Entities\CompanyCustomer;
use Entities\Company;
use Utils\Date;

class CompanyCustomerFactory
{
    /**
     * @param Company $company
     * @param string $bankTypeId
     * @param Date $preferredContactDate
     * @param int $consent
     * @return CompanyCustomer
     */
    public function createFromCompany(
        Company $company,
        $bankTypeId,
        Date $preferredContactDate = NULL,
        $consent = CompanyCustomer::CONSENT_MY_DETAILS
    )
    {
        $companyCustomer = new CompanyCustomer;
        $customer = $company->getCustomer();

        $companyCustomer->setCompany($company);
        $companyCustomer->setWholesaler($customer);
        $companyCustomer->setBankTypeId($bankTypeId);
        $companyCustomer->setPreferredContactDate($preferredContactDate ?: new Date);
        $companyCustomer->setEmail($customer->getEmail());
        $companyCustomer->setTitleId($customer->getTitleId());
        $companyCustomer->setFirstName($customer->getFirstName());
        $companyCustomer->setLastName($customer->getLastName());
        $companyCustomer->setPhone($customer->getPhone());
        $companyCustomer->setAdditionalPhone($customer->getPhone());
        $companyCustomer->setConsent($consent);

        return $companyCustomer;
    }
}
