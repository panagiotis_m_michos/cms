<?php

namespace BankingModule\DataObjects;


use BankingModule\Entities\CompanyCustomer;
use BankingModule\Entities\TsbLead;
use DateTime;
use InvalidArgumentException;

class BankingRequestData
{

    /**
     * @var string
     */
    private $dateSent = '';

    /**
     * @var boolean
     */
    private $success = TRUE;

    /**
     * @var string
     */
    private $responseDescription = '';

    /**
     * @var string
     */
    private $bank = '';

    /**
     * @param array $requestData
     * @return BankingRequestData
     * @throws InvalidArgumentException
     */
    public static function fromDefault($requestData, $bank)
    {
        if (empty($requestData)) {
            throw new InvalidArgumentException();
        }
        $self = new BankingRequestData();
        $self->bank = $bank;
        $dateSent = new DateTime($requestData[0]['date_sent']);
        $self->dateSent = $dateSent->format('d/m/Y H:i:s');
        $self->success = $requestData[0]['success'];
        $self->responseDescription = $self->isSuccess() ? '' : $requestData[0]['response_description'];
        return $self;
    }

    /**
     * @param TsbLead $requestData
     * @return BankingRequestData
     */
    public static function fromTsb(TsbLead $requestData)
    {
        if (empty($requestData)) {
            throw new InvalidArgumentException();
        }
        $self = new BankingRequestData();
        $self->bank = CompanyCustomer::BANK_TYPE_TSB;
        $self->dateSent = $requestData->getDtc()->format('d/m/Y H:i:s');
        return $self;
    }

    /**
     * @param string $message
     * @return BankingRequestData
     */
    public static function fromTsbMultipleLeadsException($message)
    {
        $self = new BankingRequestData();
        $self->bank = CompanyCustomer::BANK_TYPE_TSB;
        $self->responseDescription = $message;
        return $self;
    }

    /**
     * @return string
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * @return string
     */
    public function getBankName()
    {
        return CompanyCustomer::$banks[$this->bank];
    }

    /**
     * @return string
     */
    public function getDateSent()
    {
        return $this->dateSent;
    }

    /**
     * @return bool
     */
    public function isSuccess()
    {
        return $this->success;
    }

    /**
     * @return string
     */
    public function getResponseDescription()
    {
        return $this->responseDescription;
    }

}