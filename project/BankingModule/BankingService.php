<?php

namespace BankingModule;

use BankingModule\DataObjects\BankingRequestData;
use BankingModule\Entities\CompanyCustomer;
use BankingModule\Entities\TsbLead;
use BankingModule\Exceptions\MultipleTsbBankDetailsForCompanyException;
use BankingModule\Exceptions\MultipleTsbLeadsForCompanyException;
use BankingModule\Exceptions\NoTsbLeadForCompanyException;
use BankingModule\Repositories\BankingRepository;
use BankingModule\Repositories\TsbLeadRepository;
use Company as OldCompany;
use DateTime;
use Doctrine\ORM\NonUniqueResultException;
use Entities\Company;
use InvalidArgumentException;

class BankingService
{
    /**
     * @var BankingRepository
     */
    private $bankingRepository;

    /**
     * @var TsbLeadRepository
     */
    private $tsbLeadRepository;

    /**
     * @param BankingRepository $bankingRepository
     * @param TsbLeadRepository $tsbLeadRepository
     */
    public function __construct(
        BankingRepository $bankingRepository, 
        TsbLeadRepository $tsbLeadRepository
    )
    {
        $this->bankingRepository = $bankingRepository;
        $this->tsbLeadRepository = $tsbLeadRepository;
    }

    /**
     * @param CompanyCustomer $details
     */
    public function saveDetails(CompanyCustomer $details)
    {
        $this->bankingRepository->saveEntity($details);
    }

    /**
     * @param TsbLead $lead
     */
    public function saveTsbLead(TsbLead $lead)
    {
        $this->tsbLeadRepository->saveEntity($lead);
    }

    /**
     * @param Company $company
     * @param string $bank
     * @return CompanyCustomer|NULL
     */
    public function getLatestBankDetailsByCompany(Company $company, $bank)
    {
        return $this->bankingRepository->getLatestBankDetailsByCompany($company, $bank);
    }

    /**
     * @param Company $company
     * @return CompanyCustomer|NULL
     * @throws MultipleTsbBankDetailsForCompanyException
     */
    public function getTsbBankDetailsByCompany(Company $company)
    {
        try {
            return $this->bankingRepository->getTsbBankDetailsByCompany($company);
        } catch (NonUniqueResultException $e) {
            throw new MultipleTsbBankDetailsForCompanyException($company->getCompanyNumber(), $e);
        }
    }

    /**
     * @return CompanyCustomer[]
     */
    public function getCompaniesToSendTsbLeads()
    {
        return $this->bankingRepository->getCompaniesToSendTsbLeads();
    }

    /**
     * @param DateTime $from
     * @param DateTime $to
     * @return CompanyCustomer[]
     */
    public function getTsbLeadsDetailsSentBetween(DateTime $from, DateTime $to)
    {
        return $this->bankingRepository->getTsbLeadsDetailsSentBetween($from, $to);
    }

    /**
     * @return TsbLead[]
     */
    public function getLeadsToSendFirstTsbReminder()
    {
        return $this->tsbLeadRepository->getLeadsToSendFirstTsbReminder();
    }

    /**
     * @return TsbLead[]
     */
    public function getLeadsToSendSecondTsbReminder()
    {
        return $this->tsbLeadRepository->getLeadsToSendSecondTsbReminder();
    }

    /**
     * @param Company $company
     * @throws MultipleTsbLeadsForCompanyException
     * @throws NoTsbLeadForCompanyException
     */
    public function markTsbAccountOpened(Company $company)
    {
        try {
            $tsbLead = $this->tsbLeadRepository->getTsbLeadByCompany($company);
        } catch (NonUniqueResultException $e) {
            throw new MultipleTsbLeadsForCompanyException($company->getCompanyNumber(), $e);
        }

        if (!$tsbLead) {
            throw new NoTsbLeadForCompanyException($company->getCompanyNumber());
        }

        $tsbLead->setAccountOpened(TRUE);
        $this->tsbLeadRepository->flush($tsbLead);
    }

    /**
     * @param Company $company
     * @return TsbLead|NULL
     * @throws MultipleTsbLeadsForCompanyException
     */
    public function getTsbLeadByCompany(Company $company)
    {
        try {
            return $this->tsbLeadRepository->getTsbLeadByCompany($company);
        } catch (NonUniqueResultException $e) {
            throw new MultipleTsbLeadsForCompanyException($company->getCompanyNumber(), $e);
        }
    }

    /**
     * @param OldCompany $company
     * @param $bankingOption
     * @param Company $companyEntity
     * @return array|NULL
     */
    public function getBankingRequestData(OldCompany $company, $bankingOption, Company $companyEntity)
    {
        switch ($bankingOption) {
            case CompanyCustomer::BANK_TYPE_BARCLAYS:
                try {
                    return BankingRequestData::fromDefault(
                        $company->getBarclaysRequestData(),
                        CompanyCustomer::BANK_TYPE_BARCLAYS
                    );
                } catch (InvalidArgumentException $e) {
                    return NULL;
                }
                break;
            case CompanyCustomer::BANK_TYPE_TSB:
                try {
                    $lead = $this->getTsbLeadByCompany($companyEntity);
                    return $lead ? BankingRequestData::fromTsb($lead) : NULL;
                } catch (MultipleTsbLeadsForCompanyException $e) {
                    return BankingRequestData::fromTsbMultipleLeadsException($e->getMessage());
                }
                break;
            case CompanyCustomer::BANK_TYPE_CARD_ONE:
                try {
                    return BankingRequestData::fromDefault(
                        $company->getCardOneRequestData(),
                        CompanyCustomer::BANK_TYPE_CARD_ONE
                    );
                } catch (InvalidArgumentException $e) {
                    return NULL;
                }
                break;
            case CompanyCustomer::BANK_TYPE_HSBC:
                try {
                    return BankingRequestData::fromDefault(
                        $company->getHSBCRequestData(),
                        CompanyCustomer::BANK_TYPE_HSBC
                    );
                } catch (InvalidArgumentException $e) {
                    return NULL;
                }
                break;
        }
    }

}
