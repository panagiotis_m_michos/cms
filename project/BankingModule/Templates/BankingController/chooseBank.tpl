{assign var=hide value=true}
{include file="@header.tpl"}

<style>
    .choose-bank-button:focus, .choose-bank-button:active {
        outline: none !important;
    }
</style>
<h1>Your Company's Bank Account</h1>
<div class="alert alert-info" role="alert">
    <strong>Note</strong> - A business bank account will help you efficiently manage your company’s finances. Bank with one of our prestigious partners and receive {$cashBackAmount|currency:0} cash back*
</div>

<div id="bank-option-list" class="hidden" rv-show="items">

    <table class="table table-bordered table-condensed choose-bank-table">
        <tbody>
            <tr rv-each-item="items" class="choose-bank-row">
                <td>
                    <div class="row padding10 vertical-center choose-bank-container">
                        <div class="inlineblock pleft20">
                            <img rv-src="item.imageSrc" width="78" height="78" rv-alt="item.imageAlt"/>
                        </div>
                        <div class="inlineblock pleft20 width660">
                            <h3 class="mnone massivefont">{ item.text }</h3>

                            <p class="largefont">
                                <span rv-if="item.amount">With { item.amount | price} Cash Back.</span>
                                <span rv-if="item.additionalText">{ item.additionalText }</span>
                                <a href="javascript:;" class="cf-more-info" rv-rel="item.name | prefix 'cf'">More Info</a>
                            </p>
                        </div>
                        <div class="inlineblock txtcenter width130">
                            <div>
                                <a href="javascript:;" rv-if="item.applied" class="btn btn-default minwidth100 disabled"> Applied</a>
                                <div rv-unless="item.applied">
                                    <a rv-hide="item.selected" rv-href="item.url" rv-on-click="select" class="btn btn-default minwidth100">Select</a>
                                    <span rv-show="item.selected" class="green3 txtbold hidden"><i class="fa fa-check"></i> Selected</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <div rv-show="items" class="row hidden">
        <div class="col-md-2 col-md-offset-10">
            <a href="javascript:;" rv-no-class-disabled="selected.url" rv-href="selected.url" class="btn btn-orange minwidth100 button-continue disabled">Continue <i class="fa fa-angle-right"></i></a>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-xs-12">
        <p class="small">*Only for Barclays and TSB</p>
    </div>
</div>

<script type="text/javascript">
    bankOptions.init($('#bank-option-list'), [{$bankOptions->getList()|json nofilter},{$bankOptionsContent|json nofilter}, {$bankUrls|json nofilter}]);
</script>


{include file="@popups/CFBarclaysPopup.tpl"}
{include file="@popups/CFOneCardPopup.tpl"}
{include file="@popups/CFHsbcPopup.tpl"}
{include file="@popups/CFTsbPopup.tpl"}

{include file="@footer.tpl"}