{assign "hide" 1}
{include file="@header.tpl"}

<div id="maincontent" class="f-sterling padding20">
    <h1>{$title}</h1>

    <p class="largefont clearfix">
        <i class="fa fa-heart fa-fw grey5 padding5 fleft font20 txtbold"></i>
        <span class="fleft width90pc mleft10 ptop5">{$thankYou}</span>
    </p>

    <div class="bg-grey1 padding30 mtop30 mbottom30 mleft-30 mright-30">
        <h2>{$nextStep.header}</h2>
        <p class="largefont">
            <i class="fa fa-at fa-fw padding5 fleft orange2 font20 txtbold"></i>
            <span class="fleft width90pc orange2 mleft10 font20 txtbold">{$nextStep.title}</span>
            <span class="mleft10">{$nextStep.description}</span>
        </p>
    </div>
</div>

{include file="@footer.tpl"}
