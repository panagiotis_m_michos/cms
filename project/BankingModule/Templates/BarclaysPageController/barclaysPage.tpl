{extends '@structure/layout.tpl'}

{block content}
    <div class="width100 bg-white padcard">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-9">
                    <h1>{$introductionStrip.title nofilter}</h1>
                    <p class="lead margin0">{$introductionStrip.lead nofilter}</p>
                </div>
                {if isset($introductionStrip.imgFeature)}
                    <div class="col-xs-12  col-md-3 hidden-xs">
                        <img class="img-responsive center-block" src="{$introductionStrip.imgFeature nofilter}" alt="{$introductionStrip.imgFeatureDescription nofilter}">
                    </div>
                {/if}
            </div>
        </div>
    </div>
    <div class="width100 bg-white pad8">
        <div class="container">
            <div class="row">
                {if isset($featuresStrip.features)}
                    {foreach $featuresStrip.features as $feature}
                        <div class="col-xs-12 col-sm-1">
                            <img class="img-responsive top20 center-block " src="{$feature.imgFeature nofilter}" alt="{$feature.title nofilter}">
                        </div>
                        <div class="col-xs-12 col-sm-11">
                            <h3>{$feature.title nofilter}</h3>
                            {$feature.description nofilter}
                        </div>
                    {/foreach}
                {/if}
            </div>
        </div>
    </div>
    <div class="width100 bg-white padcard">
        <div class="container">
            <div class="row companyservices-matrix-block">
                <div class="col-xs-12">
                    <div class="row text-center button-wide-mobile">
                        <h3 class="top0 btm20">{$matrixStrip.title nofilter}</h3>
                        <a href="{$matrixStrip.link nofilter}" class="btn btn-lg btn-default">{$matrixStrip.cta nofilter}</a>
                        <p class="top20 btm0">{$matrixStrip.description nofilter}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="width100 bg-white pad8">
        <div class="container">
            <div class="row">
                {if isset($howItWorksStrip)}
                    <div class="col-xs-12">
                        <h3>{$howItWorksStrip.title nofilter}</h3>
                        {$howItWorksStrip.description nofilter}
                    </div>
                {/if}
            </div>
        </div>
    </div>
        <div class="width100 bg-grey1 padcard">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h5>{$faqStrip.title nofilter}</h5>
                        {foreach $faqStrip.tandc as $tandc}
                            <p class="small">{$tandc.description nofilter}</p>
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
{/block}
