<?php

namespace BankingModule\Emailers;

use BankingModule\Entities\CompanyCustomer;
use EmailerAbstract;
use FEmailFactory;
use IEmailService;

class TsbApplicationEmailer
{
    const EMAIL_TSB_BANKING = 1634;
    const EMAIL_TSB_BANKING_REMINDER = 1657;

    /**
     * @var IEmailService
     */
    private $emailService;

    /**
     * @var FEmailFactory
     */
    private $emailFactory;

    /**
     * @param IEmailService $emailService
     * @param FEmailFactory $emailFactory
     */
    public function __construct(IEmailService $emailService, FEmailFactory $emailFactory)
    {
        $this->emailService = $emailService;
        $this->emailFactory = $emailFactory;
    }

    /**
     * @param CompanyCustomer $bankDetails
     */
    public function sendOnlineApplication(CompanyCustomer $bankDetails)
    {
        $email = $this->emailFactory->getById(self::EMAIL_TSB_BANKING);
        $email->addTo($bankDetails->getEmail());
        $email->replacePlaceHolders(
            [
                '[FIRST_NAME]' => $bankDetails->getFirstName(),
            ]
        );

        $this->emailService->send($email);
    }

    /**
     * @param CompanyCustomer $bankDetails
     */
    public function sendOnlineApplicationReminder(CompanyCustomer $bankDetails)
    {
        $email = $this->emailFactory->getById(self::EMAIL_TSB_BANKING_REMINDER);
        $email->addTo($bankDetails->getEmail());
        $email->replacePlaceHolders(
            [
                '[FIRST_NAME]' => $bankDetails->getFirstName(),
            ]
        );

        $this->emailService->send($email);
    }
}
