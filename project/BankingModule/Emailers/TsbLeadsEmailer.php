<?php

namespace BankingModule\Emailers;

use FEmailFactory;
use IEmailService;

class TsbLeadsEmailer
{
    const EMAIL_TSB_LEADS = 1636;
    const EMAIL_TSB_WEEKLY_SUMMARY = 1639;
    const EMAIL_TSB_MONTHLY_SUMMARY = 1640;

    /**
     * @var IEmailService
     */
    private $emailService;

    /**
     * @var FEmailFactory
     */
    private $emailFactory;

    /**
     * @param IEmailService $emailService
     * @param FEmailFactory $emailFactory
     */
    public function __construct(IEmailService $emailService, FEmailFactory $emailFactory)
    {
        $this->emailService = $emailService;
        $this->emailFactory = $emailFactory;
    }

    /**
     * @param string $xlsContents
     * @param string $attachmentName
     */
    public function sendLeads($xlsContents, $attachmentName)
    {
        $this->send(self::EMAIL_TSB_LEADS, $xlsContents, $attachmentName);
    }

    /**
     * @param string $xlsContents
     * @param string $attachmentName
     */
    public function sendWeeklySummary($xlsContents, $attachmentName)
    {
        $this->send(self::EMAIL_TSB_WEEKLY_SUMMARY, $xlsContents, $attachmentName);
    }

    /**
     * @param string $xlsContents
     * @param string $attachmentName
     */
    public function sendMonthlySummary($xlsContents, $attachmentName)
    {
        $this->send(self::EMAIL_TSB_MONTHLY_SUMMARY, $xlsContents, $attachmentName);
    }

    /**
     * @param int $emailId
     * @param string $xlsContents
     * @param string $attachmentName
     */
    private function send($emailId, $xlsContents, $attachmentName)
    {
        $email = $this->emailFactory->getById($emailId);
        $email->addAttachment($attachmentName, $xlsContents);

        $this->emailService->send($email);
    }
}
