<?php

namespace BankingModule\Events;

use Entities\Cashback;
use Entities\Company;
use Symfony\Component\EventDispatcher\Event;
use Utils\Date;

class TsbAccountImportedEvent extends Event implements IAccountImportedEvent
{

    /**
     * @var Company
     */
    private $company;

    /**
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return string
     */
    public function getBankType()
    {
        return Cashback::BANK_TYPE_TSB;
    }

    /**
     * @return Date|NULL
     */
    public function getDateLeadSent()
    {
        return NULL;
    }

    /**
     * @return Date|NULL
     */
    public function getDateAccountOpened()
    {
        return NULL;
    }
}
