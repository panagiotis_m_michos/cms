<?php

namespace BankingModule\Events;

use Entities\Company;
use Utils\Date;

interface IAccountImportedEvent
{
    /**
     * @return Company
     */
    public function getCompany();

    /**
     * @return string
     */
    public function getBankType();

    /**
     * @return Date|NULL
     */
    public function getDateLeadSent();

    /**
     * @return Date|NULL
     */
    public function getDateAccountOpened();
}
