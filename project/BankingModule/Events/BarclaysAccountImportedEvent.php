<?php

namespace BankingModule\Events;

use Entities\Cashback;
use Entities\Company;
use Symfony\Component\EventDispatcher\Event;
use Utils\Date;

class BarclaysAccountImportedEvent extends Event implements IAccountImportedEvent
{
    /**
     * @var Company
     */
    private $company;

    /**
     * @var Date
     */
    private $dateLeadSent;

    /**
     * @var Date
     */
    private $dateAccountOpened;

    /**
     * @param Company $company
     * @param string $dateLeadSent
     * @param string $dateAccountOpened
     */
    public function __construct(Company $company, $dateLeadSent, $dateAccountOpened)
    {
        $this->company = $company;
        $this->dateLeadSent = Date::createFromFormat('d/m/Y', $dateLeadSent);
        $this->dateAccountOpened = Date::createFromFormat('d/m/Y', $dateAccountOpened);
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return string
     */
    public function getBankType()
    {
        return Cashback::BANK_TYPE_BARCLAYS;
    }

    /**
     * @return Date
     */
    public function getDateLeadSent()
    {
        return $this->dateLeadSent;
    }

    /**
     * @return Date
     */
    public function getDateAccountOpened()
    {
        return $this->dateAccountOpened;
    }
}
