<?php

namespace BankingModule\Exporters;

use BankingModule\Entities\CompanyCustomer;
use Factories\PhpExcelFactory;
use Factories\PhpExcelWriterFactory;
use PHPExcel;

class TsbLeadsExporter
{
    /**
     * @var PhpExcelFactory
     */
    private $excelFactory;

    /**
     * @var PhpExcelWriterFactory
     */
    private $writerFactory;

    /**
     * @param PhpExcelFactory $excelFactory
     * @param PhpExcelWriterFactory $writerFactory
     */
    public function __construct(PhpExcelFactory $excelFactory, PhpExcelWriterFactory $writerFactory)
    {
        $this->excelFactory = $excelFactory;
        $this->writerFactory = $writerFactory;
    }

    /**
     * @param CompanyCustomer[] $companies
     * @return string
     */
    public function export(array $companies)
    {
        $xls = $this->excelFactory->create();
        $this->addHeader($xls);
        $this->addRows($xls, $companies);

        $writer = $this->writerFactory->create($xls);

        ob_start();
        $writer->save('php://output');
        $contents = ob_get_clean();

        return $contents;
    }

    /**
     * @param PHPExcel $xls
     */
    private function addHeader(PHPExcel $xls)
    {
        $xls->setActiveSheetIndex(0)
            ->setCellValueExplicitByColumnAndRow(0, 1, 'email')
            ->setCellValueExplicitByColumnAndRow(1, 1, 'title')
            ->setCellValueExplicitByColumnAndRow(2, 1, 'firstName')
            ->setCellValueExplicitByColumnAndRow(3, 1, 'lastName')
            ->setCellValueExplicitByColumnAndRow(4, 1, 'phone1')
            ->setCellValueExplicitByColumnAndRow(5, 1, 'phone2')
            ->setCellValueExplicitByColumnAndRow(6, 1, 'companyName')
            ->setCellValueExplicitByColumnAndRow(7, 1, 'companyNumber')
            ->setCellValueExplicitByColumnAndRow(8, 1, 'incorporationDate');
    }

    /**
     * @param PHPExcel $xls
     * @param array $companies
     */
    private function addRows($xls, array $companies)
    {
        $row = 2;

        foreach ($companies as $company) {
            $this->addRow($xls, $row++, $company);
        }
    }

    /**
     * @param PHPExcel $xls
     * @param int $row
     * @param CompanyCustomer $company
     */
    private function addRow(PHPExcel $xls, $row, CompanyCustomer $company)
    {
        $xls->setActiveSheetIndex(0)
            ->setCellValueExplicitByColumnAndRow(0, $row, $company->getEmail())
            ->setCellValueExplicitByColumnAndRow(1, $row, $company->getTitleId())
            ->setCellValueExplicitByColumnAndRow(2, $row, $company->getFirstName())
            ->setCellValueExplicitByColumnAndRow(3, $row, $company->getLastName())
            ->setCellValueExplicitByColumnAndRow(4, $row, $company->getPhone())
            ->setCellValueExplicitByColumnAndRow(5, $row, $company->getAdditionalPhone())
            ->setCellValueExplicitByColumnAndRow(6, $row, $company->getCompany()->getCompanyName())
            ->setCellValueExplicitByColumnAndRow(7, $row, $company->getCompany()->getCompanyNumber())
            ->setCellValueExplicitByColumnAndRow(8, $row, $company->getCompany()->getIncorporationDate()->format('d/m/Y'));
    }
}
