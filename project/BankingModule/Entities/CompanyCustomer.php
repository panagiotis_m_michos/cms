<?php

namespace BankingModule\Entities;

use DateTime;
use Doctrine\ORM\Mapping as Orm;
use Entities\Company;
use Entities\Customer;
use Entities\EntityAbstract;
use Gedmo\Mapping\Annotation as Gedmo;
use Utils\Date;

/**
 * @Orm\Entity(repositoryClass="BankingModule\Repositories\BankingRepository")
 * @Orm\Table(name="cms2_company_customer")
 */
class CompanyCustomer extends EntityAbstract
{
    const BANK_TYPE_BARCLAYS = 'BARCLAYS';
    const BANK_TYPE_TSB = 'TSB';
    const BANK_TYPE_CARD_ONE = 'CARD_ONE';
    const BANK_TYPE_HSBC = 'HSBC';

    /**
     * @var array
     */
    public static $banks = [
        self::BANK_TYPE_BARCLAYS => 'Barclays',
        self::BANK_TYPE_TSB => 'TSB',
        self::BANK_TYPE_CARD_ONE => 'CardOne',
        self::BANK_TYPE_HSBC => 'HSBC'
    ];

    const CONSENT_MY_DETAILS = 1;
    const CONSENT_MY_CLIENTS_DETAILS = 2;

    const CASH_BACK_AMOUNT = 50;

    /**
     * @var array
     */
    public static $availableBankingOptions = [
        self::BANK_TYPE_BARCLAYS => 'Barclays',
        self::BANK_TYPE_TSB => 'TSB',
        self::BANK_TYPE_CARD_ONE => 'CardOne'
    ];

    /**
     * @var array
     */
    public static $forbiddenBankingForImportedCompanies = [
        self::BANK_TYPE_BARCLAYS
    ];

    /**
     * @var int
     * @Orm\Column(type="integer", name="companyCustomerId")
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Company
     * @Orm\OneToOne(targetEntity="Entities\Company")
     * @Orm\JoinColumn(name="companyId", referencedColumnName="company_id")
     */
    private $company;

    /**
     * @var Customer
     * @Orm\OneToOne(targetEntity="Entities\Customer")
     * @Orm\JoinColumn(name="wholesalerId", referencedColumnName="customerId")
     */
    private $wholesaler;

    /**
     * @var string
     * @Orm\Column(type="string")
     */
    private $bankTypeId;

    /**
     * @var Date
     * @Orm\Column(type="date")
     */
    private $preferredContactDate;

    /**
     * @var string
     * @Orm\Column(type="string")
     */
    private $email;

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $titleId;

    /**
     * @var string
     * @Orm\Column(type="string")
     */
    private $firstName;

    /**
     * @var string
     * @Orm\Column(type="string")
     */
    private $lastName;

    /**
     * @var string
     * @Orm\Column(type="string")
     */
    private $address1 = '';

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $address2;

    /**
     * @var string
     * @Orm\Column(type="string")
     */
    private $address3 = '';

    /**
     * @var string
     * @Orm\Column(type="string")
     */
    private $city = '';

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $county;

    /**
     * @var string
     * @Orm\Column(type="string")
     */
    private $postcode = '';

    /**
     * @var int
     * @Orm\Column(type="integer")
     */
    private $countryId = 0;

    /**
     * @var string
     * @Orm\Column(type="string")
     */
    private $phone = '';

    /**
     * @var string
     * @Orm\Column(type="string", nullable=true)
     */
    private $additionalPhone;

    /**
     * @var int
     * @Orm\Column(type="smallint", nullable=true)
     */
    private $consent;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Company $company
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @return Customer
     */
    public function getWholesaler()
    {
        return $this->wholesaler;
    }

    /**
     * @param Customer $wholesaler
     */
    public function setWholesaler(Customer $wholesaler)
    {
        $this->wholesaler = $wholesaler;
    }

    /**
     * @return string
     */
    public function getBankTypeId()
    {
        return $this->bankTypeId;
    }

    /**
     * @param string $bankTypeId
     */
    public function setBankTypeId($bankTypeId)
    {
        $this->bankTypeId = $bankTypeId;
    }

    /**
     * @return Date
     */
    public function getPreferredContactDate()
    {
        return $this->preferredContactDate;
    }

    /**
     * @param Date $preferredContactDate
     */
    public function setPreferredContactDate(Date $preferredContactDate)
    {
        $this->preferredContactDate = $preferredContactDate;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getTitleId()
    {
        return $this->titleId;
    }

    /**
     * @param string $titleId
     */
    public function setTitleId($titleId)
    {
        $this->titleId = $titleId;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * @param string $address1
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;
    }

    /**
     * @return string
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * @param string $address2
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;
    }

    /**
     * @return string
     */
    public function getAddress3()
    {
        return $this->address3;
    }

    /**
     * @param string $address3
     */
    public function setAddress3($address3)
    {
        $this->address3 = $address3;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * @param string $county
     */
    public function setCounty($county)
    {
        $this->county = $county;
    }

    /**
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @param string $postcode
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    }

    /**
     * @return int
     */
    public function getCountryId()
    {
        return $this->countryId;
    }

    /**
     * @param int $countryId
     */
    public function setCountryId($countryId)
    {
        $this->countryId = $countryId;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getAdditionalPhone()
    {
        return $this->additionalPhone;
    }

    /**
     * @param string $additionalPhone
     */
    public function setAdditionalPhone($additionalPhone)
    {
        $this->additionalPhone = $additionalPhone;
    }

    /**
     * @return int
     */
    public function getConsent()
    {
        return $this->consent;
    }

    /**
     * @param int $consent
     */
    public function setConsent($consent)
    {
        $this->consent = $consent;
    }

    /**
     * @return DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @param DateTime $dtc
     */
    public function setDtc(DateTime $dtc)
    {
        $this->dtc = $dtc;
    }

    /**
     * @return bool
     */
    public function isBankTsb()
    {
        return $this->bankTypeId == self::BANK_TYPE_TSB;
    }
}
