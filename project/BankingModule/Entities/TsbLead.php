<?php

namespace BankingModule\Entities;

use DateTime;
use Doctrine\ORM\Mapping as Orm;
use Entities\Company;
use Entities\Customer;
use Entities\EntityAbstract;
use Gedmo\Mapping\Annotation as Gedmo;
use Utils\Date;

/**
 * @Orm\Entity(repositoryClass="BankingModule\Repositories\TsbLeadRepository")
 * @Orm\Table(name="ch_tsb_leads")
 */
class TsbLead extends EntityAbstract
{
    /**
     * @var int
     * @Orm\Column(type="integer", name="tsbLeadId")
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Company
     * @Orm\OneToOne(targetEntity="Entities\Company")
     * @Orm\JoinColumn(name="companyId", referencedColumnName="company_id")
     */
    private $company;

    /**
     * @var bool
     * @Orm\Column(type="boolean")
     */
    private $accountOpened = FALSE;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create", on="update")
     */
    private $dtm;

    /**
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Company $company
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @return bool
     */
    public function isAccountOpened()
    {
        return $this->accountOpened;
    }

    /**
     * @param bool $accountOpened
     */
    public function setAccountOpened($accountOpened)
    {
        $this->accountOpened = $accountOpened;
    }

    /**
     * @return DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @param DateTime $dtc
     */
    public function setDtc(DateTime $dtc)
    {
        $this->dtc = $dtc;
    }

    /**
     * @return DateTime
     */
    public function getDtm()
    {
        return $this->dtm;
    }

    /**
     * @param DateTime $dtm
     */
    public function setDtm(DateTime $dtm)
    {
        $this->dtm = $dtm;
    }
}
