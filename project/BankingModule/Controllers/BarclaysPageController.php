<?php

namespace BankingModule\Controllers;

use Symfony\Component\HttpFoundation\Response;
use TemplateModule\Renderers\IRenderer;

class BarclaysPageController
{
    /**
     * @var IRenderer
     */
    private $renderer;

    /**
     * @param IRenderer $renderer
     */
    public function __construct(IRenderer $renderer)
    {
        $this->renderer = $renderer;
    }

    /**
     * @return Response
     */
    public function barclaysPage()
    {
        return $this->renderer->render();
    }
}
