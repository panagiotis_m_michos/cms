<?php

namespace BankingModule\Controllers;

use BankingModule\BankingDecider;
use BankingModule\Entities\CompanyCustomer;
use CFCompanyCustomerControler;
use CUSummaryControler;
use DashboardCustomerControler;
use Doctrine\ORM\NoResultException;
use Entities\Company;
use Entities\Customer;
use Exceptions\Business\NodeException;
use RouterModule\Helpers\ControllerHelper;
use Services\CompanyService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use TemplateModule\Renderers\IRenderer;

class BankingController
{
    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    /**
     * @var BankingDecider
     */
    private $bankingDecider;

    /**
     * @var IRenderer
     */
    private $renderer;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @param Customer $customer
     * @param CompanyService $companyService
     * @param BankingDecider $bankingDecider
     * @param ControllerHelper $controllerHelper
     * @param IRenderer $renderer
     */
    public function __construct(
        Customer $customer,
        CompanyService $companyService,
        BankingDecider $bankingDecider,
        ControllerHelper $controllerHelper,
        IRenderer $renderer
    )
    {
        $this->controllerHelper = $controllerHelper;
        $this->bankingDecider = $bankingDecider;
        $this->renderer = $renderer;
        $this->companyService = $companyService;
        $this->customer = $customer;
    }

    /**
     * @param int $companyId
     * @return RedirectResponse|Response
     */
    public function chooseBank($companyId)
    {
        try {
            $company = $this->companyService->getCustomerCompanyById($this->customer, $companyId);
            $options = $this->bankingDecider->getCashBackOptions($company);
            if (!$options->canApplyForCashBack()) {
                $this->controllerHelper->setFlashMessage('No cash back options available!');
                return new RedirectResponse(
                    $this->controllerHelper->getLink(CUSummaryControler::SUMMARY_PAGE, ['company_id' => $company->getId()])
                );
            }
            return $this->renderer->render([
                'bankOptions' => $options,
                'company' => $company,
                'bankUrls' => $this->getBankUrls($company),
                'cashBackAmount' => $options->getCashBackAmount(),
            ]);
        } catch (NoResultException $e) {
            $this->controllerHelper->setFlashMessage('Company not found!');
            return new RedirectResponse(
                $this->controllerHelper->getLink(DashboardCustomerControler::DASHBOARD_PAGE)
            );
        } catch (NodeException $e) {
            $this->controllerHelper->setFlashMessage('No cash back options available');
            return new RedirectResponse(
                $this->controllerHelper->getLink(DashboardCustomerControler::DASHBOARD_PAGE)
            );
        }
    }

    /**
     * @param int $companyId
     * @return JsonResponse
     */
    public function dismissOffer($companyId)
    {
        try {
            $company = $this->companyService->getCustomerCompanyById($this->customer, $companyId);
            $this->companyService->dismissBankOffer($company);
            return new JsonResponse(['error' => false]);
        } catch (NoResultException $e) {
            return new JsonResponse(['error' => 'Company not found']);
        }
    }

    /**
     * @param Company $company
     * @return array
     */
    private function getBankUrls(Company $company)
    {
       return [
           CompanyCustomer::BANK_TYPE_TSB => [
               'url' => $this->controllerHelper->getLink(CFCompanyCustomerControler::PAGE_TSB, ['company_id' => $company->getId()])
           ],
           CompanyCustomer::BANK_TYPE_BARCLAYS => [
               'url' =>    $this->controllerHelper->getLink(CFCompanyCustomerControler::PAGE_BARCLAYS, ['company_id' => $company->getId()])
           ],
           CompanyCustomer::BANK_TYPE_CARD_ONE => [
               'url' => $this->controllerHelper->getLink(CFCompanyCustomerControler::PAGE_CARD_ONE, ['company_id' => $company->getId()])
           ]
        ];
    }
}