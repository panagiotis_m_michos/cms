<?php

namespace BankingModule\Controllers;

use BankingModule\Exceptions\MultipleTsbBankDetailsForCompanyException;
use BankingModule\Exceptions\TsbApplicationInvalidParametersException;
use BankingModule\Facades\TsbApplicationFacade;
use Basket;
use Entities\Company;
use Entities\Customer;
use FTemplate;
use HomeControler;
use Psr\Log\LoggerInterface;
use Services\ControllerHelper;
use Symfony\Component\HttpFoundation\RedirectResponse;
use TemplateModule\Renderers\IRenderer;

class TsbApplicationController
{
    /**
     * @var IRenderer
     */
    private $renderer;

    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    /**
     * @var TsbApplicationFacade
     */
    private $tsbApplicationFacade;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param ControllerHelper $controllerHelper
     * @param TsbApplicationFacade $tsbApplicationFacade
     * @param LoggerInterface $logger
     * @param IRenderer $renderer
     */
    public function __construct(
        IRenderer $renderer,
        ControllerHelper $controllerHelper,
        TsbApplicationFacade $tsbApplicationFacade,
        LoggerInterface $logger
    )
    {
        $this->renderer = $renderer;
        $this->controllerHelper = $controllerHelper;
        $this->tsbApplicationFacade = $tsbApplicationFacade;
        $this->logger = $logger;
    }

    /**
     * @param Customer $customer
     * @param Company $company
     * @param string $companyNumber
     * @return RedirectResponse
     */
    public function addTsbBankDetails(Customer $customer, Company $company, $companyNumber)
    {
        try {
            $this->tsbApplicationFacade->addTsbBankDetails($customer, $company, $companyNumber);
        } catch (TsbApplicationInvalidParametersException $e) {
            return new RedirectResponse($this->controllerHelper->getLink(HomeControler::HOME_PAGE));
        } catch (MultipleTsbBankDetailsForCompanyException $e) {
            $this->logger->error($e);
        }

        return $this->renderer->render();
    }
}
