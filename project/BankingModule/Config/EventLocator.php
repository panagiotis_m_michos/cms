<?php

namespace BankingModule\Config;

class EventLocator
{
    const TSB_ACCOUNT_IMPORTED = 'banking_module.events.tsb_account_imported';
    const BARCLAYS_ACCOUNT_IMPORTED = 'banking_module.events.barclays_account_imported';

    const TSB_APPLICATION_FIRST_REMINDER = 'banking_module.events.tsb_first_reminder';
    const TSB_APPLICATION_SECOND_REMINDER = 'banking_module.events.tsb_second_reminder';
}
