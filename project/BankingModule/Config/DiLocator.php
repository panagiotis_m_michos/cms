<?php

namespace BankingModule\Config;

class DiLocator
{
    const SERVICE_BANKING = 'banking_module.banking_service';
    const DECIDER_BANKING = 'banking_module.banking_decider';
    const RENDERER_CHOOSE_BANK = 'banking_module.renderers.choose_bank_renderer';
    const LISTENER_SEND_TSB_APPLICATION_EMAIL = 'banking_module.listeners.send_tsb_application_email_listener';
    const IMPORTER_BARCLAYS_ACCOUNTS = 'banking_module.importers.barclays_accounts_importer';
    const IMPORTER_TSB_ACCOUNTS = 'banking_module.importers.tsb_accounts_importer';
}
