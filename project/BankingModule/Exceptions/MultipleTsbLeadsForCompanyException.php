<?php

namespace BankingModule\Exceptions;

use Exception;
use Exceptions\Business\Business_Abstract;

class MultipleTsbLeadsForCompanyException extends Business_Abstract
{
    /**
     * @param string $companyNumber
     * @param Exception $previous
     */
    public function __construct($companyNumber, Exception $previous = NULL)
    {
        parent::__construct("Company {$companyNumber} has multiple TSB leads.", 0, $previous);
    }
}
