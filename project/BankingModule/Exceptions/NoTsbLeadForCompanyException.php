<?php

namespace BankingModule\Exceptions;

use Exceptions\Business\Business_Abstract;

class NoTsbLeadForCompanyException extends Business_Abstract
{
    /**
     * @param string $companyNumber
     */
    public function __construct($companyNumber)
    {
        parent::__construct("Company {$companyNumber} has no TSB lead.");
    }
}
