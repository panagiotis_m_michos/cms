<?php

namespace BankingModule\Exceptions;

use Exceptions\Technical\TechnicalAbstract;

class TsbApplicationInvalidParametersException extends TechnicalAbstract
{
    /**
     * @param int $customerId
     * @param int $companyId
     * @param string $companyNumber
     */
    public function __construct($customerId, $companyId, $companyNumber)
    {
        parent::__construct(
            sprintf(
                'Parameters for adding TSB bank details are not valid (customerId: %d, companyId: %d, companyNumber: %s)',
                $customerId,
                $companyId,
                $companyNumber
            )
        );
    }
}
