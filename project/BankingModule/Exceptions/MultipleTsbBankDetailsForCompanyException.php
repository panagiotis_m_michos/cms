<?php

namespace BankingModule\Exceptions;

use Exception;
use Exceptions\Technical\TechnicalAbstract;

class MultipleTsbBankDetailsForCompanyException extends TechnicalAbstract
{
    /**
     * @param string $companyNumber
     * @param Exception $previous
     */
    public function __construct($companyNumber, Exception $previous = NULL)
    {
        parent::__construct(
            "Company {$companyNumber} has multiple TSB bank details submitted.",
            $previous ? $previous->getCode() : 0,
            $previous
        );
    }
}
