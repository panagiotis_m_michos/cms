<?php

namespace BankingModule\Listeners;

use BankingModule\BankingService;
use BankingModule\Emailers\TsbApplicationEmailer;
use Dispatcher\Events\CompanyEvent;
use EventLocator;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class SendTsbApplicationEmailListener implements EventSubscriberInterface
{
    /**
     * @var BankingService
     */
    private $bankingService;

    /**
     * @var TsbApplicationEmailer
     */
    private $tsbApplicationEmailer;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param BankingService $bankingService
     * @param TsbApplicationEmailer $tsbApplicationEmailer
     * @param LoggerInterface $logger
     */
    public function __construct(
        BankingService $bankingService,
        TsbApplicationEmailer $tsbApplicationEmailer,
        LoggerInterface $logger
    )
    {
        $this->bankingService = $bankingService;
        $this->tsbApplicationEmailer = $tsbApplicationEmailer;
        $this->logger = $logger;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            EventLocator::COMPANY_INCORPORATED => 'onCompanyIncorporated',
        ];
    }

    /**
     * @param CompanyEvent $event
     */
    public function onCompanyIncorporated(CompanyEvent $event)
    {
        try {
            $details = $this->bankingService->getTsbBankDetailsByCompany($event->getCompany());
            if ($details) {
                $this->tsbApplicationEmailer->sendOnlineApplication($details);
            }
        } catch (Exception $e) {
            $this->logger->error(
                'Sending TSB online application email failed',
                ['exception' => $e->getMessage(), 'trace' => (string) $e]
            );
        }
    }
}
