<?php

namespace BankingModule\Views;

use BankingModule\BankingDecider;
use BankingModule\BankingService;
use BankingModule\DataObjects\BankingRequestData;
use BankingModule\Entities\CompanyCustomer;
use Company as OldCompany;
use Entities\Company;

class CompanyBankingView
{
    /**
     * @var BankingService
     */
    private $bankingService;

    /**
     * @var BankingDecider
     */
    private $bankingDecider;

    /**
     * @param BankingService $bankingService
     * @param BankingDecider $bankingDecider
     */
    public function __construct(BankingService $bankingService, BankingDecider $bankingDecider)
    {
        $this->bankingService = $bankingService;
        $this->bankingDecider = $bankingDecider;
    }

    /**
     * @param Company $companyEntity
     * @param OldCompany $company
     * @return array
     */
    public function getBankingData(Company $companyEntity, OldCompany $company)
    {
        $data = [];
        foreach (array_keys(CompanyCustomer::$banks) as $bank) {
            if ($this->hasData($company, $bank, $companyEntity)) {
                $data[$bank] = $this->getData($company, $bank, $companyEntity);
            }
        }
        return $data;
    }

    /**
     * @param Company $companyEntity
     * @param OldCompany $company
     * @return bool
     */
    public function hasBankingData(Company $companyEntity, OldCompany $company)
    {
        return !empty($this->getBankingData($companyEntity, $company));
    }

    /**
     * @param OldCompany $company
     * @param string $bank
     * @param Company $companyEntity
     * @return bool
     */
    public function hasData(OldCompany $company, $bank, Company $companyEntity)
    {
        return !empty($this->getData($company, $bank, $companyEntity));
    }

    /**
     * @param OldCompany $company
     * @param string $bank
     * @param Company $companyEntity
     * @return BankingRequestData
     */
    public function getData(OldCompany $company, $bank, Company $companyEntity)
    {
        return $this->bankingService->getBankingRequestData($company, $bank, $companyEntity);
    }

    /**
     * @param Company $companyEntity
     * @param string $bank
     * @return bool
     */
    public function canShowAction(Company $companyEntity, $bank)
    {
        $productId = $companyEntity->getProductId();
        if ($productId) {
            return $this->bankingDecider->hasBankInOptions($companyEntity, $bank)
            && in_array($bank, [CompanyCustomer::BANK_TYPE_BARCLAYS, CompanyCustomer::BANK_TYPE_TSB]);
        } else {
            return $bank == CompanyCustomer::BANK_TYPE_TSB;
        }
    }

    /**
     * @param Company $companyEntity
     * @param string $bank
     * @return bool
     */
    public function canShowBankingRow(Company $companyEntity, $bank)
    {
        $productId = $companyEntity->getProductId();
        return $productId
            ? TRUE
            : !in_array($bank, CompanyCustomer::$forbiddenBankingForImportedCompanies) ? TRUE : FALSE;
    }

    /**
     * @param Company $companyEntity
     * @param OldCompany $company
     * @return bool
     */
    public function hasToShowBarclaysEmptyRow(Company $companyEntity, OldCompany $company)
    {
        if (!$this->canShowBankingRow($companyEntity, CompanyCustomer::BANK_TYPE_BARCLAYS)) {
            return FALSE;
        }

        return $this->canShowAction($companyEntity, CompanyCustomer::BANK_TYPE_BARCLAYS)
            && !$this->hasData($company, CompanyCustomer::BANK_TYPE_BARCLAYS, $companyEntity);
    }

    /**
     * @param Company $companyEntity
     * @param OldCompany $company
     * @return bool
     */
    public function hasToShowTsbEmptyRow(Company $companyEntity, OldCompany $company)
    {
        if (!$this->canShowBankingRow($companyEntity, CompanyCustomer::BANK_TYPE_TSB)) {
            return FALSE;
        }

        return $this->canShowAction($companyEntity, CompanyCustomer::BANK_TYPE_TSB)
            && !$this->hasData($company, CompanyCustomer::BANK_TYPE_TSB, $companyEntity);
    }

    /**
     * @param BankingRequestData $data
     * @return string
     */
    public function getBankName(BankingRequestData $data)
    {
        return $data->getBankName();
    }

    /**
     * @param BankingRequestData $data
     * @return string
     */
    public function getDateSent(BankingRequestData $data)
    {
        if ($data->getBank() == CompanyCustomer::BANK_TYPE_TSB) {
            $value = $data->getResponseDescription() != ''
                ? $data->getResponseDescription()
                : $data->getDateSent();
        } else {
            $value = $data->isSuccess() ? $data->getDateSent() : '';
        }
        return $value;
    }

    /**
     * @param BankingRequestData $data
     * @return string
     */
    public function getResponseDescription(BankingRequestData $data)
    {
        if ($data->getBank() != CompanyCustomer::BANK_TYPE_BARCLAYS) {
            return '';
        } else {
            return $data->getResponseDescription();
        }
    }
}
