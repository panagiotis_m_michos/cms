<?php

namespace BankingModule;

use BankingModule\Domain\BankCashBack;
use BankingModule\Domain\BankCashBackList;
use BankingModule\Domain\BankCashBackOptions;
use BankingModule\Entities\CompanyCustomer;
use BankingModule\Repositories\BankingRepository;
use Entities\Company;
use Exceptions\Business\NodeException;
use Repositories\Nodes\NodeRepository;
use Services\NodeService;

class BankingDecider
{
    /**
     * @var BankingRepository
     */
    private $bankingRepository;

    /**
     * @var NodeService
     */
    private $nodeService;

    /**
     * @param BankingRepository $bankingRepository
     * @param NodeService $nodeService
     */
    public function __construct(BankingRepository $bankingRepository, NodeService $nodeService)
    {
        $this->bankingRepository = $bankingRepository;
        $this->nodeService = $nodeService;
    }

    /**
     * @param Company $company
     * @return bool
     */
    public function hasDetailsFilled(Company $company)
    {
        return $this->bankingRepository->hasDetailsFilled($company);
    }

    /**
     * @param Company $company
     * @return bool
     */
    public function hasBankingEnabled(Company $company)
    {
        $package = $this->nodeService->getProductById($company->getProductId());

        return $package->bankingEnabled;
    }

    /**
     * @param Company $company
     * @param string $bankTypeId
     * @return bool
     */
    public function hasBankInOptions(Company $company, $bankTypeId)
    {
        $package = $this->nodeService->getProductById($company->getProductId());

        return in_array($bankTypeId, $package->bankingOptions);
    }
    
    /**
     * @param Company $company
     * @return BankCashBackOptions
     * @throws NodeException
     */
    public function getCashBackOptions(Company $company)
    {
        $bankList = new BankCashBackList();
        if (!$company->getProductId()) {
            $package = NULL;
            $cashBack = CompanyCustomer::CASH_BACK_AMOUNT;
            $availableOptions = [CompanyCustomer::BANK_TYPE_TSB, CompanyCustomer::BANK_TYPE_CARD_ONE];
        } else {
            $package = $this->nodeService->getProductById($company->getProductId(), TRUE);
            $availableOptions = $package->bankingOptions;
            $cashBack = $package->getCashBackAmount();
        }
        $appliedOptions = $this->bankingRepository->getAppliedBanks($company);
        foreach ($availableOptions as $name) {
            $packageCashBack = $name === CompanyCustomer::BANK_TYPE_CARD_ONE ? 0 : $cashBack;
            $bank = new BankCashBack($name, $packageCashBack, in_array($name, $appliedOptions, TRUE));
            $bankList->addBank($bank);
        }
        return new BankCashBackOptions($bankList, $company, $cashBack, $package);
    }
}
