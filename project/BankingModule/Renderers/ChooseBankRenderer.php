<?php

namespace BankingModule\Renderers;

use Entities\Company;
use FTemplate;
use Services\CashbackService;
use Services\NodeService;

class ChooseBankRenderer
{

    /**
     * @var FTemplate
     */
    private $template;

    /**
     * @var NodeService
     */
    private $nodeService;

    /**
     * @var CashbackService
     */
    private $cashbackService;

    /**
     * @param FTemplate $template
     * @param NodeService $nodeService
     * @param CashbackService $cashbackService
     */
    public function __construct(FTemplate $template, NodeService $nodeService, CashbackService $cashbackService)
    {
        $this->template = $template;
        $this->nodeService = $nodeService;
        $this->cashbackService = $cashbackService;
    }

    /**
     * @param Company $company
     * @param string $selected
     * @return string
     */
    public function getHtml(Company $company, $selected)
    {
        $package = $this->nodeService->getProductById($company->getProductId());
        $this->template->setVariables(
            [
                'bankingRequired' => $package->bankingRequired,
                'options' => $package->bankingOptions,
                'company' => $company,
                'selectedBank' => $selected ?: reset($package->bankingOptions),
                'expanded' => (bool) $selected,
                'cashBackAmount' => $this->cashbackService->getCashBackAmountForCompany($company),
            ]
        );

        return $this->template->getHtml('Banking/chooseBank.tpl');
    }
}
