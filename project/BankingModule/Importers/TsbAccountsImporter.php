<?php

namespace BankingModule\Importers;

use BankingModule\BankingService;
use BankingModule\Config\EventLocator;
use BankingModule\Events\TsbAccountImportedEvent;
use CsvParserModule\CsvParser;
use CsvParserModule\Exceptions\ImportException;
use CsvParserModule\Exceptions\IncorrectStructureException;
use Exception;
use Services\CompanyService;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Utils\File;

class TsbAccountsImporter
{
    /**
     * @var CsvParser
     */
    private $parser;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var EventDispatcher
     */
    private $eventDispatcher;

    /**
     * @var BankingService
     */
    private $bankingService;

    /**
     * @param CsvParser $parser
     * @param CompanyService $companyService
     * @param BankingService $bankingService
     * @param EventDispatcher $eventDispatcher
     */
    public function __construct(
        CsvParser $parser,
        CompanyService $companyService,
        BankingService $bankingService,
        EventDispatcher $eventDispatcher
    )
    {
        $this->parser = $parser;
        $this->companyService = $companyService;
        $this->eventDispatcher = $eventDispatcher;
        $this->bankingService = $bankingService;
    }

    /**
     * @param File $file
     * @throws ImportException
     */
    public function import(File $file)
    {
        try {
            $csvData = $this->parser->parse($file);
        } catch (IncorrectStructureException $e) {
            throw new ImportException([$e->getMessage()]);
        }

        $errors = [];
        foreach ($csvData as $row) {
            $companyNumber = str_pad($row[0], 8, '0', STR_PAD_LEFT);
            $company = $this->companyService->getCompanyByCompanyNumber($companyNumber);
            if (!$company) {
                $errors[] = "Company {$companyNumber} does not exist.";
                continue;
            }

            try {
                $this->bankingService->markTsbAccountOpened($company);
                $this->eventDispatcher->dispatch(EventLocator::TSB_ACCOUNT_IMPORTED, new TsbAccountImportedEvent($company));
            } catch (Exception $e) {
                $errors[] = $e->getMessage();
            }
        }

        if (!empty($errors)) {
            throw new ImportException($errors);
        }
    }
}
