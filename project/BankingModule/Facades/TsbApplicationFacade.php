<?php

namespace BankingModule\Facades;

use BankingModule\BankingService;
use BankingModule\Emailers\TsbApplicationEmailer;
use BankingModule\Entities\CompanyCustomer;
use BankingModule\Exceptions\TsbApplicationInvalidParametersException;
use BankingModule\Factories\CompanyCustomerFactory;
use Basket;
use Entities\Company;
use Entities\Customer;

class TsbApplicationFacade
{
    /**
     * @var BankingService
     */
    private $bankingService;

    /**
     * @var CompanyCustomerFactory
     */
    private $companyCustomerFactory;

    /**
     * @var TsbApplicationEmailer
     */
    private $tsbApplicationEmailer;

    /**
     * @param BankingService $bankingService
     * @param CompanyCustomerFactory $companyCustomerFactory
     * @param TsbApplicationEmailer $tsbApplicationEmailer
     */
    public function __construct(
        BankingService $bankingService,
        CompanyCustomerFactory $companyCustomerFactory,
        TsbApplicationEmailer $tsbApplicationEmailer
    )
    {
        $this->bankingService = $bankingService;
        $this->companyCustomerFactory = $companyCustomerFactory;
        $this->tsbApplicationEmailer = $tsbApplicationEmailer;
    }

    /**
     * @param Customer $customer
     * @param Company $company
     * @param $companyNumber
     * @throws TsbApplicationInvalidParametersException
     */
    public function addTsbBankDetails(Customer $customer, Company $company, $companyNumber)
    {
        if (!$this->validateParameters($customer, $company, $companyNumber)) {
            throw new TsbApplicationInvalidParametersException($customer->getId(), $company->getId(), $companyNumber);
        }

        $currentBankDetails = $this->bankingService->getTsbBankDetailsByCompany($company);
        if (!$currentBankDetails) {
            $bankDetails = $this->companyCustomerFactory->createFromCompany($company, CompanyCustomer::BANK_TYPE_TSB);
            $this->bankingService->saveDetails($bankDetails);
            $this->tsbApplicationEmailer->sendOnlineApplication($bankDetails);
        }
    }

    /**
     * @param Customer $customer
     * @param Company $company
     * @param $companyNumber
     * @return bool
     */
    private function validateParameters(Customer $customer, Company $company, $companyNumber)
    {
        return ($customer->isEqual($company->getCustomer()) && $company->getCompanyNumber() == $companyNumber);
    }
}
