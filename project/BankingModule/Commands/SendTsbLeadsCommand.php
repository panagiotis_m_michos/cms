<?php

namespace BankingModule\Commands;

use BankingModule\BankingService;
use BankingModule\Emailers\TsbLeadsEmailer;
use BankingModule\Entities\TsbLead;
use BankingModule\Exporters\TsbLeadsExporter;
use Cron\Commands\CommandAbstract;
use Cron\INotifier;
use Psr\Log\LoggerInterface;
use Utils\Date;

class SendTsbLeadsCommand extends CommandAbstract
{
    /**
     * @var string
     */
    protected $timeType = self::TIME_TYPE_FIXED;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var INotifier
     */
    protected $notifier;

    /**
     * @var BankingService
     */
    private $bankingService;

    /**
     * @var TsbLeadsExporter
     */
    private $exporter;

    /**
     * @var TsbLeadsEmailer
     */
    private $emailer;

    /**
     * @var string
     */
    private $attachmentName;

    /**
     * @param LoggerInterface $logger
     * @param INotifier $notifier
     * @param BankingService $bankingService
     * @param TsbLeadsExporter $exporter
     * @param TsbLeadsEmailer $emailer
     * @param string $attachmentName
     */
    public function __construct(
        LoggerInterface $logger,
        INotifier $notifier,
        BankingService $bankingService,
        TsbLeadsExporter $exporter,
        TsbLeadsEmailer $emailer,
        $attachmentName
    )
    {
        $this->logger = $logger;
        $this->notifier = $notifier;
        $this->bankingService = $bankingService;
        $this->exporter = $exporter;
        $this->emailer = $emailer;
        $this->attachmentName = $attachmentName;
    }

    public function execute()
    {
        $companiesTsbDetails = $this->bankingService->getCompaniesToSendTsbLeads();

        if ($companiesTsbDetails) {
            $xlsContents = $this->exporter->export($companiesTsbDetails);
            $this->emailer->sendLeads($xlsContents, $this->attachmentName);

            foreach ($companiesTsbDetails as $tsbDetails) {
                $lead = new TsbLead($tsbDetails->getCompany());
                $this->bankingService->saveTsbLead($lead);
            }

            $this->logger->debug(sprintf('TSB leads sent to TSB for %d companies', count($companiesTsbDetails)));
        } else {
            $this->logger->debug('No companies with TSB bank to send');
        }

        $date = new Date();
        $this->notifier->triggerSuccess(
            $this->getName(),
            $date->getTimestamp(),
            sprintf('TSB leads sent to TSB for %d companies. Date %s', count($companiesTsbDetails), $date->format('d/m/Y'))
        );
    }
}
