<?php

namespace BankingModule\Commands;

use BankingModule\BankingService;
use BankingModule\Config\EventLocator;
use BankingModule\Emailers\TsbApplicationEmailer;
use Cron\Commands\CommandAbstract;
use Cron\INotifier;
use Psr\Log\LoggerInterface;
use Services\EventService;
use Utils\Date;

class SendTsbApplicationRemindersCommand extends CommandAbstract
{
    /**
     * @var string
     */
    protected $timeType = self::TIME_TYPE_FIXED;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var INotifier
     */
    protected $notifier;

    /**
     * @var BankingService
     */
    private $bankingService;

    /**
     * @var TsbApplicationEmailer
     */
    private $tsbApplicationEmailer;

    /**
     * @var EventService
     */
    private $eventService;

    /**
     * @param LoggerInterface $logger
     * @param INotifier $notifier
     * @param BankingService $bankingService
     * @param TsbApplicationEmailer $tsbApplicationEmailer
     * @param EventService $eventService
     */
    public function __construct(
        LoggerInterface $logger,
        INotifier $notifier,
        BankingService $bankingService,
        TsbApplicationEmailer $tsbApplicationEmailer,
        EventService $eventService
    )
    {
        $this->logger = $logger;
        $this->notifier = $notifier;
        $this->bankingService = $bankingService;
        $this->tsbApplicationEmailer = $tsbApplicationEmailer;
        $this->eventService = $eventService;
    }

    public function execute()
    {
        $this->sendFirstReminders();
        $this->sendSecondReminders();

        $date = new Date();
        $this->notifier->triggerSuccess(
            $this->getName(),
            $date->getTimestamp(),
            sprintf('TSB application reminders sent. Date %s', $date->format('d/m/Y'))
        );
    }

    private function sendFirstReminders()
    {
        $firstReminderLeads = $this->bankingService->getLeadsToSendFirstTsbReminder();
        foreach ($firstReminderLeads as $firstReminderLead) {
            $company = $firstReminderLead->getCompany();

            $bankDetails = $this->bankingService->getTsbBankDetailsByCompany($company);
            $this->tsbApplicationEmailer->sendOnlineApplicationReminder($bankDetails);
            $this->eventService->notify(EventLocator::TSB_APPLICATION_FIRST_REMINDER, $firstReminderLead->getId());

            $this->logger->debug(
                'First reminder sent',
                [
                    'tsbLeadId' => $firstReminderLead->getId(),
                    'leadSentAt' => $firstReminderLead->getDtc()->format('d/m/Y'),
                    'companyId' => $company->getId(),
                ]
            );
        }
    }

    private function sendSecondReminders()
    {
        $secondReminderLeads = $this->bankingService->getLeadsToSendSecondTsbReminder();
        foreach ($secondReminderLeads as $secondReminderLead) {
            $company = $secondReminderLead->getCompany();

            $bankDetails = $this->bankingService->getTsbBankDetailsByCompany($company);
            $this->tsbApplicationEmailer->sendOnlineApplicationReminder($bankDetails);
            $this->eventService->notify(EventLocator::TSB_APPLICATION_SECOND_REMINDER, $secondReminderLead->getId());

            $this->logger->debug(
                'Second reminder sent',
                [
                    'tsbLeadId' => $secondReminderLead->getId(),
                    'leadSentAt' => $secondReminderLead->getDtc()->format('d/m/Y'),
                    'companyId' => $company->getId(),
                ]
            );
        }
    }
}
