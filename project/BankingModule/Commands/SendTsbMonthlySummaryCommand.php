<?php

namespace BankingModule\Commands;

use BankingModule\BankingService;
use BankingModule\Emailers\TsbLeadsEmailer;
use BankingModule\Exporters\TsbLeadsExporter;
use Cron\Commands\CommandAbstract;
use Cron\INotifier;
use DateTime;
use Psr\Log\LoggerInterface;
use Utils\Date;

class SendTsbMonthlySummaryCommand extends CommandAbstract
{
    /**
     * @var string
     */
    protected $timeType = self::TIME_TYPE_FIXED;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var INotifier
     */
    protected $notifier;

    /**
     * @var BankingService
     */
    private $bankingService;

    /**
     * @var TsbLeadsExporter
     */
    private $exporter;

    /**
     * @var TsbLeadsEmailer
     */
    private $emailer;

    /**
     * @var string
     */
    private $attachmentName;

    /**
     * @param LoggerInterface $logger
     * @param INotifier $notifier
     * @param BankingService $bankingService
     * @param TsbLeadsExporter $exporter
     * @param TsbLeadsEmailer $emailer
     * @param string $attachmentName
     */
    public function __construct(
        LoggerInterface $logger,
        INotifier $notifier,
        BankingService $bankingService,
        TsbLeadsExporter $exporter,
        TsbLeadsEmailer $emailer,
        $attachmentName
    )
    {
        $this->logger = $logger;
        $this->notifier = $notifier;
        $this->bankingService = $bankingService;
        $this->exporter = $exporter;
        $this->emailer = $emailer;
        $this->attachmentName = $attachmentName;
    }

    public function execute()
    {
        $start = new DateTime('first day of previous month 00:00:00');
        $stop = new DateTime('last day of previous month 23:59:59');
        $companies = $this->bankingService->getTsbLeadsDetailsSentBetween($start, $stop);

        if ($companies) {
            $xlsContents = $this->exporter->export($companies);
            $this->emailer->sendMonthlySummary($xlsContents, $this->attachmentName);
            $logMessage = sprintf('Monthly summary with %d companies sent to TSB', count($companies));
        } else {
            $logMessage = 'No TSB leads sent previous month';
        }

        $this->logger->debug($logMessage);
        $date = new Date();
        $this->notifier->triggerSuccess(
            $this->getName(),
            $date->getTimestamp(),
            $logMessage . sprintf('Date %s', $date->format('d/m/Y'))
        );
    }
}
