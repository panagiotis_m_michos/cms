<?php

namespace BankingModule\Domain;

use JsonSerializable;

final class BankCashBack implements JsonSerializable
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $amount;

    /**
     * @var bool
     */
    private $applied;

    /**
     * @param string $name
     * @param float $amount
     * @param bool $applied
     */
    public function __construct($name, $amount, $applied)
    {
        $this->name = $name;
        $this->amount = $amount;
        $this->applied = $applied;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return boolean
     */
    public function isApplied()
    {
        return $this->applied;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'name' => $this->getName(),
            'amount' => $this->getAmount(),
            'applied' => $this->isApplied()
        ];
    }
}
