<?php

namespace BankingModule\Domain;

use BankingModule\Domain\BankCashBackList;
use BankingModule\Entities\CompanyCustomer;
use Entities\Company;
use Package;

final class BankCashBackOptions
{
    /**
     * @var BankCashBackList
     */
    private $bankCashBackList;

    /**
     * @var Company
     */
    private $company;

    /**
     * @var Package
     */
    private $package;

    /**
     * @var float
     */
    private $cashBackAmount;

    /**
     * @param BankCashBackList $bankCashBackList
     * @param Company $company
     * @param float $cashBackAmount
     * @param Package $package
     */
    public function __construct(BankCashBackList $bankCashBackList, Company $company, $cashBackAmount, Package $package = NULL)
    {
        $this->bankCashBackList = $bankCashBackList;
        $this->company = $company;
        $this->package = $package;
        $this->cashBackAmount = $cashBackAmount;
    }

    /**
     * @return bool
     */
    public function isCashBackAllowed()
    {
        if ($this->company->isIncorporated()) {
            if ($this->company->belongsToUkCustomer()) {
                if ($this->package) {
                    return $this->package->bankingEnabled;
                } else {
                    return TRUE;
                }
            }
        }
        return FALSE;
    }

    /**
     * @return bool
     */
    public function canApplyForCashBack()
    {
        return $this->isCashBackAllowed() && $this->bankCashBackList->hasAvailableBanks();
    }

    /**
     * @return bool
     */
    public function canShowBankOffer()
    {
        return !$this->company->hasDismissedCashBackPromotion() && $this->canApplyForCashBack();
    }

    /**
     * @return bool
     */
    public function canDismissBankOffer()
    {
        foreach ($this->bankCashBackList as $bank) {
            if ($bank->isApplied()) {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * @param string $bank
     * @return bool
     */
    public function canApplyForCashBackWith($bank)
    {
        return $this->isCashBackAllowed() && $this->bankCashBackList->hasAvailableBank($bank);
    }

    /**
     * @return BankCashBackList
     */
    public function getList()
    {
        return $this->bankCashBackList;
    }

    /**
     * @return float
     */
    public function getCashBackAmount()
    {
        return $this->cashBackAmount;
    }

    /**
     * @return array
     */
    public function getAppliedBankNames()
    {
        $names = [];
        foreach ($this->bankCashBackList as $bankCashBack) {
            if ($bankCashBack->isApplied()) {
                $names[] = $bankCashBack->getName();
            }
        }
        return $names;
    }

    /**
     * @TODO does not belong in domain
     *
     * @param string $implodeBy
     * @return string
     */
    public function getAppliedBankNamesAsString($implodeBy)
    {
        $names = [];
        foreach ($this->bankCashBackList as $bankCashBack) {
            if ($bankCashBack->isApplied()) {
                if (isset(CompanyCustomer::$availableBankingOptions[$bankCashBack->getName()])) {
                    $names[] = CompanyCustomer::$availableBankingOptions[$bankCashBack->getName()];
                }
            }
        }
        return implode($implodeBy, $names);
    }

    /**
     * @return bool
     */
    public function hasAppliedBankCashBacks()
    {
        return $this->bankCashBackList->hasAppliedBanks();
    }
}
