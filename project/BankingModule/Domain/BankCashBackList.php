<?php

namespace BankingModule\Domain;

use ArrayIterator;
use IteratorAggregate;
use JsonSerializable;

final class BankCashBackList implements IteratorAggregate, JsonSerializable
{
    /**
     * @var BankCashBack[]
     */
    private $bankCashBacks = [];

    public function addBank(BankCashBack $bankCashBack)
    {
        $this->bankCashBacks[$bankCashBack->getName()] = $bankCashBack;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function hasBankWithName($name)
    {
        return isset($this->bankCashBacks[$name]);
    }

    /**
     * @return ArrayIterator
     */
    public function getIterator()
    {
        return new ArrayIterator($this->bankCashBacks);
    }

    /**
     * @param string $bank
     * @return bool
     */
    public function hasAvailableBank($bank)
    {
        if (isset($this->bankCashBacks[$bank])) {
            return !$this->bankCashBacks[$bank]->isApplied();
        }
        return FALSE;
    }

    /**
     * @return bool
     */
    public function hasAvailableBanks()
    {
        foreach ($this->bankCashBacks as $bank) {
            if (!$bank->isApplied()) {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * @return bool
     */
    public function hasAppliedBanks()
    {
        foreach ($this->bankCashBacks as $bank) {
            if ($bank->isApplied()) {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * @return BankCashBack[]
     */
    public function jsonSerialize()
    {
        return $this->bankCashBacks;
    }
}
