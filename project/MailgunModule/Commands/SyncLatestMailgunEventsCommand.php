<?php

namespace MailgunModule\Commands;

use Cron\Commands\CommandAbstract;
use Cron\INotifier;
use DateTime;
use MailgunModule\Entities\MailgunEvent;
use MailgunModule\Repositories\MailgunEventRepository;
use MailgunModule\Services\MailgunService;
use Psr\Log\LoggerInterface;

class SyncLatestMailgunEventsCommand extends CommandAbstract
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var INotifier
     */
    protected $notifier;

    /**
     * @var MailgunService
     */
    private $mailgunService;

    /**
     * @var MailgunEventRepository
     */
    private $mailgunEventRepository;

    public function __construct(
        LoggerInterface $logger,
        INotifier $notifier,
        MailgunService $mailgunService,
        MailgunEventRepository $mailgunEventRepository
    ) {
        $this->logger = $logger;
        $this->notifier = $notifier;
        $this->mailgunService = $mailgunService;
        $this->mailgunEventRepository = $mailgunEventRepository;
    }

    /**
     * main logic to execute command
     */
    public function execute()
    {
        $latestEvent = $this->mailgunEventRepository->getLatestEvent();
        $findFrom = $latestEvent ? clone $latestEvent->getCreatedAt() : new DateTime('1970-02-01');

        $newEvents = $this->mailgunService->getLatestEvents($findFrom->modify('-5 minutes'));
        $latestStoredEvents = $this->mailgunEventRepository->findBetween($findFrom->modify('-1 minute'), new DateTime('+1 day'));
        $latestStoredEventsUniqueIds = array_map(
            function (MailgunEvent $event) {
                return $event->getUniqueId();
            },
            $latestStoredEvents
        );

        foreach ($newEvents as $event) {
            if (!in_array($event->getUniqueId(), $latestStoredEventsUniqueIds)) {
                $this->mailgunEventRepository->saveEntity($event);
            }
        }

        $date = new DateTime;
        $this->notifier->triggerSuccess(
            $this->getName(),
            $date->getTimestamp(),
            "Latest Mailgun events synced."
        );
    }
}
