<?php

namespace MailgunModule\Factories;

use DateTime;
use MailgunModule\Entities\MailgunEvent;
use Services\CustomerService;
use stdClass;

class MailgunResponseObjectFactory
{
    /**
     * @var CustomerService
     */
    private $customerService;

    /**
     * @param CustomerService $customerService
     */
    public function __construct(CustomerService $customerService)
    {
        $this->customerService = $customerService;
    }

    /**
     * @param array|stdClass $jsonData
     * @return MailgunEvent
     */
    public function buildMailgunEvent($jsonData)
    {
        $arrayData = json_decode(json_encode($jsonData), TRUE);
        $customVariables = isset($arrayData['user-variables']['my-custom-data']) ? json_decode($arrayData['user-variables']['my-custom-data'],
            TRUE) : [];

        $event = new MailgunEvent();
        $event->setStatus($arrayData['event']);
        $event->setTemporaryId($arrayData['id']);
        $event->setEventBody($arrayData);
        $event->setEmailTemplateId(isset($customVariables['emailTemplateId']) ? $customVariables['emailTemplateId'] : NULL);
        $event->setCampaign(isset($customVariables['campaign']) ? $customVariables['campaign'] : NULL);
        $event->setScenario(isset($customVariables['scenario']) ? $customVariables['scenario'] : NULL);
        $event->setSite(isset($customVariables['site']) ? $customVariables['site'] : NULL);
        $event->setCustomer(isset($customVariables['customerId']) ? $this->customerService->getCustomerById($customVariables['customerId']) : NULL);
        $event->setRecipient(isset($arrayData['recipient']) ? $arrayData['recipient'] : NULL);
        $event->setCreatedAt((new DateTime())->setTimestamp($arrayData['timestamp']));

        return $event;
    }
}
