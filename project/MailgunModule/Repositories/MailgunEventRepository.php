<?php

namespace MailgunModule\Repositories;

use DateTime;
use Doctrine\ORM\Internal\Hydration\IterableResult;
use Doctrine\ORM\NonUniqueResultException;
use MailgunModule\Entities\MailgunEvent;
use Repositories\BaseRepository_Abstract;

class MailgunEventRepository extends BaseRepository_Abstract
{
    /**
     * @return MailgunEvent|NULL
     * @throws NonUniqueResultException
     */
    public function getLatestEvent()
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('e')
            ->from(MailgunEvent::class, 'e')
            ->orderBy('e.createdAt', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param DateTime $from
     * @param DateTime $to
     * @return MailgunEvent[]
     */
    public function findBetween(DateTime $from, DateTime $to)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('e')
            ->from(MailgunEvent::class, 'e')
            ->where('e.createdAt BETWEEN :from AND :to')
            ->setParameters(
                [
                    'from' => $from,
                    'to' => $to,
                ]
            )
            ->getQuery()
            ->getResult();
    }
}
