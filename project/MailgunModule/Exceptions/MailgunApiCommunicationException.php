<?php

namespace MailgunModule\Exceptions;

use Exception;
use RuntimeException;

class MailgunApiCommunicationException extends RuntimeException
{
    public function __construct($httpStatusCode = 0, Exception $previous = NULL)
    {
        parent::__construct("Mailgun API responded with status code $httpStatusCode.", $httpStatusCode, $previous);
    }
}
