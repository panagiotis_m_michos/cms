<?php

namespace MailgunModule\Entities;

use DateTime;
use Doctrine\ORM\Mapping as Orm;
use Entities\Customer;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Customer
 * @Orm\Entity(repositoryClass = "MailgunModule\Repositories\MailgunEventRepository")
 * @Orm\Table(name="cms2_mailgun_events")
 * @Gedmo\Loggable(logEntryClass="LoggableModule\Entities\LogEntry")
 */
class MailgunEvent
{
    const STATUS_ACCEPTED = 'accepted';
    const STATUS_REJECTED = 'rejected';
    const STATUS_DELIVERED = 'delivered';
    const STATUS_FAILED = 'failed';
    const STATUS_OPENED = 'opened';
    const STATUS_CLICKED = 'clicked';
    const STATUS_UNSUBSCRIBED = 'unsubscribed';
    const STATUS_COMPLAINED = 'complained';
    const STATUS_STORED = 'stored';

    /**
     * @var integer
     * @Orm\Column(name="mailgunEventId", type="integer", nullable=false)
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @Orm\Column()
     */
    private $temporaryId;

    /**
     * @var string
     * @Orm\Column()
     */
    private $status;

    /**
     * @var string
     * @Orm\Column(nullable=true)
     */
    private $emailTemplateId;

    /**
     * @var Customer
     * @Orm\ManyToOne(targetEntity="\Entities\Customer", cascade={"persist"})
     * @Orm\JoinColumn(name="customerId", referencedColumnName="customerId")
     */
    private $customer;

    /**
     * @var string
     * @Orm\Column()
     */
    private $recipient;

    /**
     * @var string
     * @Orm\Column()
     */
    private $scenario;

    /**
     * @var string
     * @Orm\Column()
     */
    private $campaign;

    /**
     * @var string
     * @Orm\Column()
     */
    private $site;

    /**
     * @var array
     * @Orm\Column(type="json_array")
     */
    private $eventBody = [];

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getEmailTemplateId()
    {
        return $this->emailTemplateId;
    }

    /**
     * @param string $emailTemplateId
     */
    public function setEmailTemplateId($emailTemplateId)
    {
        $this->emailTemplateId = $emailTemplateId;
    }

    /**
     * @return array
     */
    public function getEventBody()
    {
        return $this->eventBody;
    }

    /**
     * @param array $eventBody
     */
    public function setEventBody(array $eventBody)
    {
        $this->eventBody = $eventBody;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * @param string $recipient
     */
    public function setRecipient($recipient)
    {
        $this->recipient = $recipient;
    }

    /**
     * @return string
     */
    public function getScenario()
    {
        return $this->scenario;
    }

    /**
     * @param string $scenario
     */
    public function setScenario($scenario)
    {
        $this->scenario = $scenario;
    }

    /**
     * @return string
     */
    public function getCampaign()
    {
        return $this->campaign;
    }

    /**
     * @param string $campaign
     */
    public function setCampaign($campaign)
    {
        $this->campaign = $campaign;
    }

    /**
     * @return string
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * @param string $site
     */
    public function setSite($site)
    {
        $this->site = $site;
    }

    /**
     * @return string
     */
    public function getTemporaryId()
    {
        return $this->temporaryId;
    }

    /**
     * @param string $temporaryId
     */
    public function setTemporaryId($temporaryId)
    {
        $this->temporaryId = $temporaryId;
    }

    /**
     * @return string
     */
    public function getUniqueId()
    {
        return $this->getCreatedAt()->format('U') . $this->getTemporaryId();
    }
}
