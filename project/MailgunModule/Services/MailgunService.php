<?php

namespace MailgunModule\Services;

use DateTime;
use FEmail;
use FFile;
use Generator;
use DusanKasan\Knapsack\Collection;
use Mailgun\Mailgun;
use MailgunModule\Entities\MailgunEvent;
use MailgunModule\Exceptions\MailgunApiCommunicationException;
use MailgunModule\Factories\MailgunResponseObjectFactory;
use stdClass;

class MailgunService
{
    /**
     * @var Mailgun
     */
    private $mailgun;

    /**
     * @var string
     */
    private $domain;

    /**
     * @var MailgunResponseObjectFactory
     */
    private $responseObjectFactory;

    /**
     * @param Mailgun $mailgun
     * @param string $domain
     * @param MailgunResponseObjectFactory $responseObjectFactory
     */
    public function __construct(Mailgun $mailgun, $domain, MailgunResponseObjectFactory $responseObjectFactory)
    {
        $this->mailgun = $mailgun;
        $this->domain = $domain;
        $this->responseObjectFactory = $responseObjectFactory;
    }

    /**
     * @param FEmail $email
     */
    public function send(FEmail $email)
    {
        $attachments = [];

        foreach ($email->getCmsFileAttachments() as $val) {
            $file = new FFile($val);
            $attachments[] = $file->getFilePath();
        }

        $tmpFiles = [];
        foreach ($email->getAttachments() as $val) {
            $tmpFile = tmpfile();
            fwrite($tmpFile, $val['content']);
            $attachments[] = stream_get_meta_data($tmpFile)['uri'];
        }

        $to = Collection::from($email->getToArr())
            ->concat($email->getCCArr())
            ->concat($email->getBccArr())
            ->distinct()
            ->toArray();

        $additionalData = $email->getAdditionalData();
        $messageData = [
            [
                'from' => $email->fromName ? "{$email->fromName} <{$email->from}>" : $email->from,
                'to' => implode(',', $to),
                'subject' => $email->subject,
                'html' => $email->body,
                'o:campaign' => isset($additionalData['campaign']) ? $additionalData['campaign'] : '',
                'v:my-custom-data' => json_encode($additionalData)
            ]
        ];

        if ($email->getCCArr()) {
            $messageData['cc'] = implode(',', $email->getCCArr());
        }

        if ($email->getBccArr()) {
            $messageData['bcc'] = implode(',', $email->getBccArr());
        }

        $this->mailgun->sendMessage(
            $this->domain,
            $messageData,
            [
                'attachment' => $attachments
            ]
        );

        foreach ($tmpFiles as $tmpFile) {
            fclose($tmpFile);
        }
    }

    /**
     * @param DateTime $dateTime
     * @return Generator|MailgunEvent[]
     */
    public function getLatestEvents(DateTime $dateTime)
    {
        $response = $this->mailgun->get(
            "{$this->domain}/events",
            [
                'begin' => $dateTime->format(DateTime::RFC2822),
                'ascending' => 'yes',
            ]
        );

        foreach ($this->getFullItemList($response) as $eventData) {
            yield $this->responseObjectFactory->buildMailgunEvent($eventData);
        }
    }

    /**
     * @param $campaignId
     */
    public function assureCampaign($campaignId)
    {
        $campaigns = $this->getFullItemList($this->mailgun->get("{$this->domain}/campaigns"));
        $campaignExists = Collection::from($campaigns)
            ->map(function ($campaignData) {
                return $campaignData->id;
            })
            ->contains($campaignId);

        if (!$campaignExists) {
            $this->mailgun->post("{$this->domain}/campaigns", ['name' => $campaignId, 'id' => $campaignId]);
        }
    }

    /**
     * @param stdClass $response
     * @return Generator
     */
    private function getFullItemList($response)
    {
        if ($response->http_response_code != 200) {
            throw new MailgunApiCommunicationException($response->http_response_code);
        }

        $responseBody = $response->http_response_body;

        foreach ($responseBody->items as $item) {
            yield $item;
        }

        if ($responseBody->items && property_exists($responseBody, 'paging')) {
            $nextResponse = $this->mailgun->get($responseBody->paging->next);

            foreach ($this->getFullItemList($nextResponse) as $item) {
                yield $item;
            }
        }
    }
}
