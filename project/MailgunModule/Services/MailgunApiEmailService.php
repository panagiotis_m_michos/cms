<?php

namespace MailgunModule\Services;


use Entities\Customer;
use FEmail;
use IEmailService;

class MailgunApiEmailService implements IEmailService
{
    /**
     * @var MailgunService
     */
    private $mailgunService;

    /**
     * @param MailgunService $mailgunService
     */
    public function __construct(MailgunService $mailgunService)
    {
        $this->mailgunService = $mailgunService;
    }

    /**
     * @param FEmail $email
     * @param Customer $customer
     */
    public function send(FEmail $email, $customer = NULL)
    {
        $this->mailgunService->send($email);
    }
}
