<?php

namespace ServiceRemindersModule\Commands;

use Cron\Commands\CommandAbstract;
use Cron\INotifier;
use Exception;
use Factories\Front\CompanyServicesViewFactory;
use Psr\Log\LoggerInterface;
use ServiceRemindersModule\Dispatchers\IDispatcher;
use Services\ServiceService;
use Utils\Date;

class SendRemindersCommand extends CommandAbstract
{
    /**
     * @var string
     */
    protected $timeType = self::TIME_TYPE_FIXED;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var INotifier
     */
    protected $notifier;

    /**
     * @var IDispatcher
     */
    private $dispatcher;

    /**
     * @var ServiceService
     */
    private $serviceService;

    /**
     * @var CompanyServicesViewFactory
     */
    private $viewFactory;

    /**
     * @param LoggerInterface $logger
     * @param INotifier $notifier
     * @param IDispatcher $dispatcher
     * @param ServiceService $serviceService
     * @param CompanyServicesViewFactory $viewFactory
     */
    public function __construct(
        LoggerInterface $logger,
        INotifier $notifier,
        IDispatcher $dispatcher,
        ServiceService $serviceService,
        CompanyServicesViewFactory $viewFactory
    )
    {
        $this->logger = $logger;
        $this->notifier = $notifier;
        $this->dispatcher = $dispatcher;
        $this->serviceService = $serviceService;
        $this->viewFactory = $viewFactory;
    }

    public function execute()
    {
        $services = $this->serviceService->getServicesToSendRemindersFor();
        if (!$services) {
            $this->logger->notice('No services to send reminders for');
        }

        $count = 0;
        $errorCount = 0;

        foreach ($services as $service) {
            try {
                $view = $this->viewFactory->create($service->getCompany());
                $this->dispatcher->dispatch($view);
                $count++;
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                $errorCount++;
            }
        }

        $date = new Date();
        $this->notifier->triggerSuccess(
            $this->getName(),
            $date->getTimestamp(),
            sprintf('Service reminders processed %d. Errors %d. Date %s', $count, $errorCount, $date->format('d/m/Y'))
        );
    }
}
