<?php

namespace ServiceRemindersModule\Dispatchers;

use Models\View\Front\CompanyServicesView;

interface IDispatcher
{
    /**
     * @param CompanyServicesView $companyServicesView
     */
    public function dispatch(CompanyServicesView $companyServicesView);
}
