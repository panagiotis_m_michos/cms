<?php

namespace ServiceRemindersModule\Dispatchers;

use Dispatcher\Events\ServiceEmailEventFactory;
use Emailers\ServicesEmailer;
use EventLocator;
use Models\View\Front\CompanyServicesView;
use Models\View\ServiceView;
use Repositories\EmailRepository;
use Services\EventService;
use Symfony\Component\EventDispatcher\EventDispatcher;

class ExpiresToday implements IDispatcher
{
    /**
     * @var EmailRepository
     */
    private $emailRepository;

    /**
     * @var ServiceEmailEventFactory
     */
    private $eventFactory;

    /**
     * @var EventService
     */
    private $eventService;

    /**
     * @var EventDispatcher
     */
    private $eventDispatcher;

    /**
     * @param EmailRepository $emailRepository
     * @param ServiceEmailEventFactory $eventFactory
     * @param EventService $eventService
     * @param EventDispatcher $eventDispatcher
     */
    public function __construct(
        EmailRepository $emailRepository,
        ServiceEmailEventFactory $eventFactory,
        EventService $eventService,
        EventDispatcher $eventDispatcher
    )
    {
        $this->emailRepository = $emailRepository;
        $this->eventFactory = $eventFactory;
        $this->eventService = $eventService;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param CompanyServicesView $companyServicesView
     */
    public function dispatch(CompanyServicesView $companyServicesView)
    {
        $event = EventLocator::SERVICE_EXPIRED;
        $type = CompanyServicesView::TYPE_EXPIRED;

        if ($companyServicesView->hasCompanyServicesWithoutFailedAutoRenewalExpiringIn($type)) {
            $serviceViews = $companyServicesView->getCompanyServicesWithoutFailedAutoRenewalExpiringIn($type);
            $email = $this->emailRepository->find(ServicesEmailer::SERVICES_EXPIRED_EMAIL);
            $emailEvent = $this->eventFactory->create($email, $companyServicesView, $serviceViews, $type);

            $this->eventDispatcher->dispatch(EventLocator::COMPANY_SERVICES_EXPIRED, $emailEvent);
            $this->notifyService($event, $serviceViews);
        }
    }

    /**
     * @param string $event
     * @param ServiceView[] $serviceViews
     */
    private function notifyService($event, array $serviceViews)
    {
        foreach ($serviceViews as $serviceView) {
            $this->eventService->notify($event, $serviceView->getId());
        }
    }
}
