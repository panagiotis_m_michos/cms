<?php

namespace ServiceRemindersModule\Dispatchers;

use Models\View\Front\CompanyServicesView;

class RemindersDispatcher implements IDispatcher
{

    /**
     * @var IDispatcher[]
     */
    private $dispatchers = [];

    /**
     * @param IDispatcher $dispatcher
     */
    public function addDispatcher(IDispatcher $dispatcher)
    {
        $this->dispatchers[] = $dispatcher;
    }

    /**
     * @param CompanyServicesView $view
     */
    public function dispatch(CompanyServicesView $view)
    {
        foreach ($this->dispatchers as $dispatcher) {
            $dispatcher->dispatch($view);
        }
    }
}
