<?php

namespace ServicesAdminModule\Facades;

use DateTime;
use Entities\Company;
use ServiceActivatorModule\ProductRelationshipChecker;
use ServiceActivatorModule\RelatedServiceProvider;
use Services\NodeService;
use Services\ProductService;
use ServicesAdminModule\Entities\AddServiceFormOrderFactory;
use ServicesAdminModule\Forms\AddServiceFormData;
use ServiceSettingsModule\Services\ServiceSettingsService;

class AddServiceFacade
{
    /**
     * @var NodeService
     */
    private $nodeService;

    /**
     * @var ProductService
     */
    private $productService;

    /**
     * @var AddServiceFormOrderFactory
     */
    private $orderFactory;

    /**
     * @var RelatedServiceProvider
     */
    private $relatedServiceProvider;

    /**
     * @var ProductRelationshipChecker
     */
    private $productChecker;

    /**
     * @var ServiceSettingsService
     */
    private $serviceSettingsService;

    /**
     * @param NodeService $nodeService
     * @param ProductService $productService
     * @param AddServiceFormOrderFactory $orderFactory
     * @param RelatedServiceProvider $relatedServiceProvider
     * @param ProductRelationshipChecker $productChecker
     * @param ServiceSettingsService $serviceSettingsService
     */
    public function __construct(
        NodeService $nodeService,
        ProductService $productService,
        AddServiceFormOrderFactory $orderFactory,
        RelatedServiceProvider $relatedServiceProvider,
        ProductRelationshipChecker $productChecker,
        ServiceSettingsService $serviceSettingsService
    )
    {
        $this->nodeService = $nodeService;
        $this->productService = $productService;
        $this->orderFactory = $orderFactory;
        $this->relatedServiceProvider = $relatedServiceProvider;
        $this->productChecker = $productChecker;
        $this->serviceSettingsService = $serviceSettingsService;
    }

    /**
     * @param AddServiceFormData $data
     * @param Company $company
     */
    public function addService(AddServiceFormData $data, Company $company)
    {
        $product = $this->nodeService->getProductById($data->getProductId());
        $related = $this->relatedServiceProvider->getRelatedService($company, $product, new DateTime);
        $renewalToken = $related && $this->productChecker->isEqual($related->getProduct(), $product)
            ? $this->serviceSettingsService->getSettingsByService($related)->getRenewalToken()
            : NULL;

        $orderItem = $this->orderFactory->create($company, $product, $data);

        $product->setCompanyId($company->getId());
        $product->setOrderItem($orderItem);
        $product->setDuration($data->getDuration());
        $product->setActivateFrom($data->getStartDateDateTime());

        $this->productService->saveServices([$product], $renewalToken);
    }
}
