<?php

namespace ServicesAdminModule\Forms;

use Entities\Company;
use ServicesAdminModule\Providers\AddServiceFormChoicesProvider;

class AddServiceFormSubscriberFactory
{
    /**
     * @var AddServiceFormChoicesProvider
     */
    private $formChoicesProvider;

    /**
     * @param AddServiceFormChoicesProvider $formChoicesProvider
     */
    public function __construct(AddServiceFormChoicesProvider $formChoicesProvider)
    {
        $this->formChoicesProvider = $formChoicesProvider;
    }

    /**
     * @param Company $company
     * @return AddServiceFormSubscriber
     */
    public function create(Company $company)
    {
        return new AddServiceFormSubscriber($this->formChoicesProvider, $company);
    }
}
