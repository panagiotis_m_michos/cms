<?php

namespace ServicesAdminModule\Forms;

use Entities\Company;
use ServicesAdminModule\Providers\AddServiceFormChoicesProvider;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;

class AddServiceFormSubscriber implements EventSubscriberInterface
{
    /**
     * @var AddServiceFormChoicesProvider
     */
    private $formChoicesProvider;

    /**
     * @var Company
     */
    private $company;

    /**
     * @param AddServiceFormChoicesProvider $formChoicesProvider
     * @param Company $company
     */
    public function __construct(AddServiceFormChoicesProvider $formChoicesProvider, Company $company)
    {
        $this->formChoicesProvider = $formChoicesProvider;
        $this->company = $company;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_SUBMIT => 'preSubmit',
            FormEvents::POST_SUBMIT => ['postSubmit', 900],
        ];
    }

    /**
     * @param FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
        $this->updateForm($event->getForm());
    }

    /**
     * @param FormEvent $event
     */
    public function preSubmit(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        $productId = isset($data['productId']) ? $data['productId'] : NULL;
        $startDate = isset($data['startDate']) ? $data['startDate'] : NULL;
        $productIdChanged = $data['previousProductId'] != $productId;
        $startDateChanged = $data['previousStartDate'] != $startDate;

        if ($productIdChanged && $productId) {
            $startDatesChoices = $this->formChoicesProvider->getStartDates($this->company, $productId);
            $startDate = count($startDatesChoices) == 1 ? key($startDatesChoices) : NULL;
            $durationsChoices = $this->formChoicesProvider->getDurations($startDate);

            $data['startDate'] = $startDate;
            $data['duration'] = NULL;

        } elseif ($productIdChanged && !$productId) {
            $startDatesChoices = [];
            $durationsChoices = [];
            $data['startDate'] = NULL;
            $data['duration'] = NULL;

        } elseif ($startDateChanged) {
            $startDatesChoices = $this->formChoicesProvider->getStartDates($this->company, $productId);
            $durationsChoices = $this->formChoicesProvider->getDurations($startDate);
            $data['duration'] = NULL;

        } elseif ($productId) {
            $startDatesChoices = $this->formChoicesProvider->getStartDates($this->company, $productId);
            $durationsChoices = $this->formChoicesProvider->getDurations($startDate);
        }

        $data['previousProductId'] = $productId;
        $data['previousStartDate'] = isset($data['startDate']) ? $data['startDate'] : NULL;

        $event->setData($data);

        $this->updateForm($form, $startDatesChoices, $durationsChoices);
    }

    /**
     * @param FormEvent $event
     */
    public function postSubmit(FormEvent $event)
    {
        if (!$event->getForm()->get('add')->isClicked()) {
            $event->stopPropagation();
        }
    }

    /**
     * @param FormInterface $form
     * @param array $startDates
     * @param array $durations
     */
    private function updateForm(FormInterface $form, $startDates = [], $durations = [])
    {
        $form->add(
            'startDate',
            'choice',
            [
                'label' => 'Start Date',
                'choices' => $startDates,
                'disabled' => empty($startDates),
                'placeholder' => count($startDates) > 1 ? '--- Select start date ---' : NULL,
            ]
        );

        $form->add(
            'duration',
            'choice',
            [
                'choices' => $durations,
                'disabled' => empty($durations),
                'placeholder' => count($durations) > 1 ? '--- Select duration ---' : NULL,
            ]
        );
    }
}
