<?php

namespace ServicesAdminModule\Forms;

use Entities\Company;
use ServicesAdminModule\Providers\AddServiceFormChoicesProvider;
use ServiceActivatorModule\Validators\NotDowngradeForbidden;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class AddServiceForm extends AbstractType
{
    const DURATION_1_MONTH = '+1 month';
    const DURATION_3_MONTHS = '+3 months';
    const DURATION_6_MONTHS = '+6 months';
    const DURATION_12_MONTHS = '+12 months';

    const REASON_GOODWILL = 'goodwill';
    const REASON_NON_STANDARD = 'nonStandard';

    /**
     * @var AddServiceFormSubscriber
     */
    private $addServiceFormSubscriber;

    /**
     * @var AddServiceFormChoicesProvider
     */
    private $formChoicesProvider;

    /**
     * @var Company
     */
    private $company;

    /**
     * @param AddServiceFormSubscriber $addServiceFormSubscriber
     * @param AddServiceFormChoicesProvider $formChoicesProvider
     * @param Company $company
     */
    public function __construct(
        AddServiceFormSubscriber $addServiceFormSubscriber,
        AddServiceFormChoicesProvider $formChoicesProvider,
        Company $company
    )
    {
        $this->addServiceFormSubscriber = $addServiceFormSubscriber;
        $this->formChoicesProvider = $formChoicesProvider;
        $this->company = $company;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventSubscriber($this->addServiceFormSubscriber);

        $builder
            ->add(
                'productId',
                'choice',
                [
                    'label' => 'Service Offer',
                    'choices' => $this->formChoicesProvider->getProducts($this->company),
                    'placeholder' => '--- Select product ---',
                    'constraints' => [
                        new NotDowngradeForbidden(['company' => $this->company]),
                    ]
                ]
            )
            ->add(
                'reason',
                'choice',
                [
                    'choices' => [
                        self::REASON_GOODWILL => 'Gesture of Goodwill',
                        self::REASON_NON_STANDARD => 'Non-Standard payment method was used',
                    ],
                    'expanded' => TRUE,
                ]
            )
            ->add(
                'paymentDate',
                'date',
                [
                    'label' => 'Payment Date',
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                    'invalid_message' => 'Please provide a valid date',
                    'required' => FALSE,
                ]
            )
            ->add(
                'totalAmount',
                'number',
                [
                    'label' => 'Total Amount',
                    'required' => FALSE
                ]
            )
            ->add(
                'reference',
                'text',
                [
                    'required' => FALSE
                ]
            )
            ->add(
                'previousProductId',
                'hidden',
                [
                    'mapped' => FALSE,
                ]
            )
            ->add(
                'previousStartDate',
                'hidden',
                [
                    'mapped' => FALSE,
                ]
            )
            ->add(
                'add',
                'submit',
                [
                    'label' => 'Add Service',
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'validation_groups' => function(FormInterface $form) {
                    /** @var AddServiceFormData $data */
                    $data = $form->getData();

                    return ['Default', $data->getReason()];
                }
            ]
        );
    }

    /**
     * @return null
     */
    public function getName()
    {
        return NULL;
    }
}
