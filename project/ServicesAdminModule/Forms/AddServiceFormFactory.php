<?php

namespace ServicesAdminModule\Forms;

use Entities\Company;
use ServicesAdminModule\Providers\AddServiceFormChoicesProvider;

class AddServiceFormFactory
{
    /**
     * @var AddServiceFormSubscriberFactory
     */
    private $addServiceFormSubscriberFactory;

    /**
     * @var AddServiceFormChoicesProvider
     */
    private $formChoicesProvider;

    /**
     * @param AddServiceFormSubscriberFactory $addServiceFormSubscriberFactory
     * @param AddServiceFormChoicesProvider $formChoicesProvider
     */
    public function __construct(
        AddServiceFormSubscriberFactory $addServiceFormSubscriberFactory,
        AddServiceFormChoicesProvider $formChoicesProvider
    )
    {
        $this->addServiceFormSubscriberFactory = $addServiceFormSubscriberFactory;
        $this->formChoicesProvider = $formChoicesProvider;
    }

    /**
     * @param Company $company
     * @return AddServiceForm
     */
    public function create(Company $company)
    {
        return new AddServiceForm(
            $this->addServiceFormSubscriberFactory->create($company),
            $this->formChoicesProvider,
            $company
        );
    }
}
