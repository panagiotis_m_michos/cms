<?php

namespace ServicesAdminModule\Forms;

use DateTime;
use DateTimeImmutable;

class AddServiceFormData
{
    /**
     * @var int
     */
    private $productId;

    /**
     * @var string
     */
    private $startDate;

    /**
     * @var int
     */
    private $duration;

    /**
     * @var string
     */
    private $reason = AddServiceForm::REASON_GOODWILL;

    /**
     * @var string
     */
    private $paymentDate;

    /**
     * @var float
     */
    private $totalAmount;

    /**
     * @var string
     */
    private $reference;

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * @return string
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param string $startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getStartDateDateTime()
    {
        return $this->startDate ? new DateTimeImmutable($this->startDate) : NULL;
    }

    /**
     * @return int
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    }

    /**
     * @return string
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @param string $reason
     */
    public function setReason($reason)
    {
        $this->reason = $reason;
    }

    /**
     * @return DateTime
     */
    public function getPaymentDate()
    {
        return $this->paymentDate;
    }

    /**
     * @param DateTime $paymentDate
     */
    public function setPaymentDate(DateTime $paymentDate = NULL)
    {
        $this->paymentDate = $paymentDate;
    }

    /**
     * @return float
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    /**
     * @param float $totalAmount
     */
    public function setTotalAmount($totalAmount)
    {
        $this->totalAmount = $totalAmount;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    }

    /**
     * @return bool
     */
    public function isReasonGoodwill()
    {
        return $this->reason == AddServiceForm::REASON_GOODWILL;
    }

    /**
     * @return bool
     */
    public function isReasonNonStandard()
    {
        return $this->reason == AddServiceForm::REASON_NON_STANDARD;
    }

}
