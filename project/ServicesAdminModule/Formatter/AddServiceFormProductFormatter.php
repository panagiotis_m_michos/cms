<?php

namespace ServicesAdminModule\Formatters;

use DateTime;
use Entities\Company;
use ServiceActivatorModule\ProductRelationshipChecker;
use ServiceActivatorModule\RelatedServiceProvider;
use Services\NodeService;

class AddServiceFormProductFormatter
{
    /**
     * @var NodeService
     */
    private $nodeService;

    /**
     * @var RelatedServiceProvider
     */
    private $relatedServiceProvider;

    /**
     * @var ProductRelationshipChecker
     */
    private $productChecker;

    /**
     * @param NodeService $nodeService
     * @param RelatedServiceProvider $relatedServiceProvider
     * @param ProductRelationshipChecker $productChecker
     */
    public function __construct(
        NodeService $nodeService,
        RelatedServiceProvider $relatedServiceProvider,
        ProductRelationshipChecker $productChecker
    )
    {
        $this->nodeService = $nodeService;
        $this->relatedServiceProvider = $relatedServiceProvider;
        $this->productChecker = $productChecker;
    }

    /**
     * @param Company $company
     * @param int $productId
     * @return string
     */
    public function getTitle(Company $company, $productId)
    {
        $orderDate = new DateTime;

        $product = $this->nodeService->getProductById($productId);
        $relatedService = $this->relatedServiceProvider->getRelatedService($company, $product, $orderDate);
        $suffix = '';

        if ($relatedService) {
            if ($this->productChecker->isDowngrade($relatedService->getProduct(), $product)) {
                if ($this->productChecker->isDowngradableOn($relatedService, $product, $orderDate)) {
                    $suffix = '(downgrade)';
                } else {
                    $suffix = '(downgrade not possible!)';
                }
            } elseif ($this->productChecker->isUpgrade($relatedService->getProduct(), $product)) {
                $suffix = '(upgrade)';
            } else {
                $suffix = '(current service)';
            }
        }

        return "{$product->getLngTitle()} {$suffix}";
    }
}
