<?php

namespace ServicesAdminModule\Entities;

use BasketProduct;
use Entities\Company;
use Entities\Customer;
use Entities\Order;
use Entities\OrderItem;
use Services\UserService;
use ServicesAdminModule\Forms\AddServiceFormData;
use VatModule\Calculators\VatCalculator;

class AddServiceFormOrderFactory
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var VatCalculator
     */
    private $vatCalculator;

    /**
     * @param UserService $userService
     * @param VatCalculator $vatCalculator
     */
    public function __construct(UserService $userService, VatCalculator $vatCalculator)
    {
        $this->userService = $userService;
        $this->vatCalculator = $vatCalculator;
    }

    /**
     * @param Company $company
     * @param BasketProduct $product
     * @param AddServiceFormData $data
     * @return OrderItem
     */
    public function create(Company $company, BasketProduct $product, AddServiceFormData $data)
    {
        $order = $this->createOrder($company->getCustomer(), $data);
        $orderItem = $this->createOrderItem($data, $order, $company, $product);

        $order->addItem($orderItem);

        if ($data->isReasonGoodwill()) {
            $order->setRealSubtotal($orderItem->getSubTotal());
            $order->setSubTotal(0);
            $order->setDiscount($orderItem->getSubTotal());
            $order->setVat(0);
            $order->setTotal(0);
        } else {
            $total = $data->getTotalAmount();
            $subtotal = $this->vatCalculator->getSubtotalFromTotal($total);

            $order->setRealSubtotal($subtotal);
            $order->setSubTotal($subtotal);
            $order->setDiscount(0);
            $order->setVat($this->vatCalculator->getVatFromTotal($total));
            $order->setTotal($total);
        }

        return $orderItem;
    }

    /**
     * @param Customer $customer
     * @param AddServiceFormData $data
     * @return Order
     */
    private function createOrder(Customer $customer, AddServiceFormData $data)
    {
        $order = new Order($customer);
        $order->setAgent($this->userService->getLoggedInUser());

        if ($data->isReasonGoodwill()) {
            $order->setDescription("Goodwill Gesture ({$data->getDuration()})");
            $order->setPaymentMediumId(Order::PAYMENT_MEDIUM_COMPLIMENTARY);
        } else {
            $order->setDescription("Non-standard payment, payment date: {$data->getPaymentDate()->format('d/m/Y')}, reference: {$data->getReference()}");
            $order->setPaymentMediumId(Order::PAYMENT_MEDIUM_NON_STANDARD);
        }

        return $order;
    }

    /**
     * @param AddServiceFormData $data
     * @param Order $order
     * @param Company $company
     * @param BasketProduct $product
     * @return OrderItem
     */
    private function createOrderItem(AddServiceFormData $data, Order $order, Company $company, BasketProduct $product)
    {
        $orderItem = new OrderItem($order);
        $orderItem->setCompany($company);
        $orderItem->setProductId($product->getId());
        $orderItem->setProductTitle("{$product->getLongTitle()} for company \"{$company->getCompanyName()}\"");
        $orderItem->setPrice($product->getPrice());
        $orderItem->setSubTotal($product->getPrice());
        $orderItem->setVat($product->getVat());
        $orderItem->setTotalPrice($product->getTotalVatPrice());
        $orderItem->setIsFee(FALSE);
        $orderItem->setNotApplyVat(FALSE);
        $orderItem->setNonVatableValue(0);
        $orderItem->setQty($data->isReasonGoodwill() ? 0 : 1);

        return $orderItem;
    }
}
