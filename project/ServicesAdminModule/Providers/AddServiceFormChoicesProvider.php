<?php

namespace ServicesAdminModule\Providers;

use DateTime;
use DateTimeImmutable;
use Entities\Company;
use ServiceActivatorModule\ProductRelationshipChecker;
use ServiceActivatorModule\RelatedServiceProvider;
use Services\NodeService;
use ServicesAdminModule\Formatters\AddServiceFormProductFormatter;
use ServicesAdminModule\Forms\AddServiceForm;

class AddServiceFormChoicesProvider
{
    /**
     * @var int[]
     */
    private $productIds;

    /**
     * @var NodeService
     */
    private $nodeService;

    /**
     * @var ProductRelationshipChecker
     */
    private $productChecker;

    /**
     * @var RelatedServiceProvider
     */
    private $relatedServiceProvider;

    /**
     * @var AddServiceFormProductFormatter
     */
    private $productFormatter;

    /**
     * @param int[] $productIds
     * @param NodeService $nodeService
     * @param ProductRelationshipChecker $productChecker
     * @param RelatedServiceProvider $relatedServiceProvider
     * @param AddServiceFormProductFormatter $productFormatter
     */
    public function __construct(
        $productIds,
        NodeService $nodeService,
        ProductRelationshipChecker $productChecker,
        RelatedServiceProvider $relatedServiceProvider,
        AddServiceFormProductFormatter $productFormatter
    )
    {
        $this->productIds = $productIds;
        $this->nodeService = $nodeService;
        $this->productChecker = $productChecker;
        $this->relatedServiceProvider = $relatedServiceProvider;
        $this->productFormatter = $productFormatter;
    }

    /**
     * @param Company $company
     * @return array
     */
    public function getProducts(Company $company)
    {
        $choices = [];
        foreach ($this->productIds as $productId) {
            $choices[$productId] = $this->productFormatter->getTitle($company, $productId);
        }

        return $choices;
    }

    /**
     * @param Company $company
     * @param $productId
     * @return string[]
     */
    public function getStartDates(Company $company, $productId)
    {
        $product = $this->nodeService->getProductById($productId);
        $orderDate = new DateTime;
        $relatedService = $this->relatedServiceProvider->getRelatedService($company, $product, $orderDate);

        if ($relatedService && $this->productChecker->isDowngradeForbiddenOn($relatedService, $product, $orderDate)) {
            return [];
        }

        $choices = [];
        if ($relatedService) {
            $startDate = DateTimeImmutable::createFromMutable($relatedService->getDtExpires())->modify('+1 day');
            $choices[$startDate->format('Y-m-d')] = 'End of existing active service (' . $startDate->format('d/m/Y') . ')';
        } else {
            $startDate = new DateTimeImmutable;
            $choices[$startDate->format('Y-m-d')] = 'Today (' . $startDate->format('d/m/Y') . ')';
        }

        return $choices;
    }

    /**
     * @param string $startDate
     * @return string[]
     */
    public function getDurations($startDate)
    {
        if (!$startDate) {
            return [];
        }

        $startDateDateTime = new DateTimeImmutable($startDate);
        $description = '%s (expires on %s)';

        return [
            AddServiceForm::DURATION_1_MONTH => sprintf($description, '1 month', $startDateDateTime->modify('+1 month -1 day')->format('d/m/Y')),
            AddServiceForm::DURATION_3_MONTHS => sprintf($description, '3 months', $startDateDateTime->modify('+3 months -1 day')->format('d/m/Y')),
            AddServiceForm::DURATION_6_MONTHS => sprintf($description, '6 months', $startDateDateTime->modify('+6 months -1 day')->format('d/m/Y')),
            AddServiceForm::DURATION_12_MONTHS => sprintf($description, '12 months', $startDateDateTime->modify('+12 months -1 day')->format('d/m/Y')),
        ];
    }
}
