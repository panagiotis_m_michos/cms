<?php

namespace ServiceSettingsModule\Services;

use Doctrine\ORM\Internal\Hydration\IterableResult;
use Entities\Company;
use Entities\Customer;
use Entities\Order;
use Entities\Payment\Token;
use Entities\Service;
use Entities\ServiceSettings;
use Factories\ServiceViewFactory;
use Repositories\ServiceSettingsRepository;
use Services\ServiceService;
use ServiceSettingsModule\Facades\ServiceSettingsFacade;

class ServiceSettingsService
{

    /**
     * @var ServiceSettingsRepository
     */
    private $repository;

    /**
     * @var ServiceService
     */
    private $serviceService;

    /**
     * @var ServiceSettingsFacade
     */
    private $serviceSettingsFacade;

    /**
     * @var ServiceViewFactory
     */
    private $serviceViewFactory;

    /**
     * @param ServiceSettingsRepository $repository
     * @param ServiceService $serviceService
     * @param ServiceSettingsFacade $serviceSettingsFacade
     * @param ServiceViewFactory $serviceViewFactory
     */
    public function __construct(
        ServiceSettingsRepository $repository,
        ServiceService $serviceService,
        ServiceSettingsFacade $serviceSettingsFacade,
        ServiceViewFactory $serviceViewFactory
    )
    {
        $this->repository = $repository;
        $this->serviceService = $serviceService;
        $this->serviceSettingsFacade = $serviceSettingsFacade;
        $this->serviceViewFactory = $serviceViewFactory;
    }

    /**
     * @param ServiceSettings $entity
     */
    public function save(ServiceSettings $entity)
    {
        $this->repository->saveEntity($entity);
    }

    /**
     * @param ServiceSettings $entity
     */
    public function flush(ServiceSettings $entity)
    {
        $this->repository->flush($entity);
    }

    /**
     * @param Company $company
     * @param string $serviceTypeId
     * @return ServiceSettings|NULL
     */
    public function getSettingsByType(Company $company, $serviceTypeId)
    {
        return $this->repository->getSettingsByType($company, $serviceTypeId);
    }

    /**
     * @param Service $service
     * @return ServiceSettings|NULL
     */
    public function getSettingsByService(Service $service)
    {
        return $this->repository->getSettingsByService($service);
    }

    /**
     * @param Token $token
     * @return ServiceSettings[]
     */
    public function getSettingsByToken(Token $token)
    {
        return $this->repository->getSettingsByToken($token);
    }

    /**
     * @return IterableResult
     */
    public function getSettingsWithEnabledAutoRenewal()
    {
        return $this->repository->getSettingsWithEnabledAutoRenewal();
    }

    /**
     * @param Company $company
     * @param string $serviceTypeId
     * @return ServiceSettings
     */
    public function getOrCreateSettingsByType(Company $company, $serviceTypeId)
    {
        $settings = $this->getSettingsByType($company, $serviceTypeId);
        if (!$settings) {
            $settings = new ServiceSettings($company, $serviceTypeId);
            $this->repository->saveEntity($settings);
        }

        return $settings;
    }

    /**
     * @param Company $company
     */
    public function disableEmailRemindersByCompany(Company $company)
    {
        foreach ($company->getGroupedServices() as $service) {
            $settings = $this->getSettingsByService($service);
            $this->serviceSettingsFacade->disableEmailReminders($settings);
        }
    }

    /**
     * @param Company $company
     */
    public function disableAutoRenewalByCompany(Company $company)
    {
        foreach ($company->getGroupedServices() as $service) {
            $settings = $this->getSettingsByService($service);
            $this->serviceSettingsFacade->disableAutoRenewal($settings);
        }
    }

    /**
     * @param Order $order
     * @param Token $token
     */
    public function enableAutoRenewalByOrder(Order $order, Token $token)
    {
        foreach ($order->getRenewableServices() as $service) {
            $settings = $this->getSettingsByService($service);
            if ($service->isRenewable() && $service->getProduct()->isAutoRenewalAllowed) {
                $this->serviceSettingsFacade->enableAutoRenewal($settings, $token);
            }
        }
    }

    /**
     * @param Order $order
     */
    public function disableAutoRenewalByOrder(Order $order)
    {
        foreach ($order->getRenewableServices() as $service) {
            $settings = $this->getSettingsByService($service);
            $this->serviceSettingsFacade->disableAutoRenewal($settings);
        }
    }

    /**
     * @param Token $token
     */
    public function suspendAutoRenewalByToken(Token $token)
    {
        $servicesSettings = $this->getSettingsByToken($token);
        foreach ($servicesSettings as $serviceSettings) {
            $this->serviceSettingsFacade->suspendAutoRenewal($serviceSettings);
        }
    }

    /**
     * @param Customer $customer
     * @param Token $token
     */
    public function reenableAutoRenewalByCustomer(Customer $customer, Token $token)
    {
        $servicesSettings = $this->getSettingsToReenableAutoRenewalByCustomer($customer);
        foreach ($servicesSettings as $serviceSettings) {
            $this->serviceSettingsFacade->enableAutoRenewal($serviceSettings, $token);
        }
    }

    /**
     * @param Customer $customer
     * @return ServiceSettings[]
     */
    public function getSettingsToReenableAutoRenewalByCustomer(Customer $customer)
    {
        $settingsToReenable = [];
        $servicesSettings = $this->repository->getSettingsWithInvalidRenewalTokenByCustomer($customer);
        foreach ($servicesSettings as $serviceSettingsData) {
            /** @var ServiceSettings $serviceSettings */
            $serviceSettings = $serviceSettingsData[0];
            $services = $this->serviceService->getCompanyServicesByType($serviceSettings->getCompany(), $serviceSettings->getServiceTypeId());
            $serviceView = $this->serviceViewFactory->create($services);

            if ($serviceView->canToggleAutoRenewal()) {
                $settingsToReenable[] = $serviceSettings;
            }
        }

        return $settingsToReenable;
    }

    /**
     * @param Customer $customer
     * @return bool
     */
    public function hasCustomerServicesWithInvalidRenewalToken(Customer $customer)
    {
        $servicesSettings = $this->repository->getSettingsWithInvalidRenewalTokenByCustomer($customer);
        foreach ($servicesSettings as $serviceSettingsData) {
            /** @var ServiceSettings $serviceSettings */
            $serviceSettings = $serviceSettingsData[0];
            $services = $this->serviceService->getCompanyServicesByType($serviceSettings->getCompany(), $serviceSettings->getServiceTypeId());
            $serviceView = $this->serviceViewFactory->create($services);

            if ($serviceView->canToggleAutoRenewal()) {
                return TRUE;
            }
        }

        return FALSE;
    }
}
