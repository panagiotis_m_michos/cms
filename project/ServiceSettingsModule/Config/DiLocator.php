<?php

namespace ServiceSettingsModule\Config;

class DiLocator
{
    const FACADES_SERVICE_SETTINGS_FACADE = 'service_settings_module.facades.service_settings_facade';

    const LISTENERS_DISABLE_AUTO_RENEWAL_LISTENER = 'service_settings_module.listeners.disable_auto_renewal_listener';

    const SERVICES_SERVICE_SETTINGS_SERVICE = 'service_settings_module.services.service_settings_service';

    const TOGGLERS_AUTO_RENEWAL_TOGGLER = 'service_settings_module.togglers.auto_renewal_toggler';
    const TOGGLERS_EMAIL_REMINDERS_TOGGLER = 'service_settings_module.togglers.email_reminders_toggler';

    const LISTENER_DISABLE_AUTO_RENEWAL = 'service_settings_module.listeners.disable_auto_renewal_listener';
}
