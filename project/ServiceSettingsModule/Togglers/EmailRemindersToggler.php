<?php

namespace ServiceSettingsModule\Togglers;

use Entities\ServiceSettings;
use Repositories\ServiceSettingsRepository;

class EmailRemindersToggler
{

    /**
     * @var ServiceSettingsRepository
     */
    private $serviceSettingsRepository;

    /**
     * @param ServiceSettingsRepository $serviceSettingsRepository
     */
    public function __construct(ServiceSettingsRepository $serviceSettingsRepository)
    {
        $this->serviceSettingsRepository = $serviceSettingsRepository;
    }

    /**
     * @param ServiceSettings $settings
     */
    public function enable(ServiceSettings $settings)
    {
        $settings->enableEmailReminders();
        $this->serviceSettingsRepository->flush($settings);
    }

    /**
     * @param ServiceSettings $settings
     */
    public function disable(ServiceSettings $settings)
    {
        $settings->enableEmailReminders(FALSE);
        $this->serviceSettingsRepository->flush($settings);
    }
}
