<?php

namespace ServiceSettingsModule\Togglers;

use Entities\Payment\Token;
use Entities\ServiceSettings;
use Repositories\ServiceSettingsRepository;

class AutoRenewalToggler
{

    /**
     * @var ServiceSettingsRepository
     */
    private $serviceSettingsRepository;

    /**
     * @param ServiceSettingsRepository $serviceSettingsRepository
     */
    public function __construct(ServiceSettingsRepository $serviceSettingsRepository)
    {
        $this->serviceSettingsRepository = $serviceSettingsRepository;
    }

    /**
     * @param ServiceSettings $settings
     * @param Token $token
     */
    public function enable(ServiceSettings $settings, Token $token)
    {
        $settings->setIsAutoRenewalEnabled(TRUE);
        $settings->setRenewalToken($token);
        $this->serviceSettingsRepository->flush($settings);
    }

    /**
     * @param ServiceSettings $settings
     */
    public function disable(ServiceSettings $settings)
    {
        $settings->setIsAutoRenewalEnabled(FALSE);
        $settings->setRenewalToken(NULL);
        $this->serviceSettingsRepository->flush($settings);
    }

    /**
     * @param ServiceSettings $settings
     */
    public function suspend(ServiceSettings $settings)
    {
        $settings->setIsAutoRenewalEnabled(FALSE);
        $this->serviceSettingsRepository->flush($settings);
    }
}
