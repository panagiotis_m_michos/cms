<?php

namespace ServiceSettingsModule\Listeners;

use Dispatcher\Events\CompanyTransferredEvent;
use Dispatcher\Events\Order\RefundEvent;
use EventLocator;
use Services\ServiceService;
use ServiceSettingsModule\Facades\ServiceSettingsFacade;
use ServiceSettingsModule\Services\ServiceSettingsService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class DisableAutoRenewalListener implements EventSubscriberInterface
{
    /**
     * @var ServiceService
     */
    private $serviceService;

    /**
     * @var ServiceSettingsService
     */
    private $settingsService;

    /**
     * @var ServiceSettingsFacade
     */
    private $settingsFacade;

    /**
     * @param ServiceService $serviceService
     * @param ServiceSettingsService $settingsService
     * @param ServiceSettingsFacade $settingsFacade
     */
    public function __construct(
        ServiceService $serviceService,
        ServiceSettingsService $settingsService,
        ServiceSettingsFacade $settingsFacade
    )
    {
        $this->serviceService = $serviceService;
        $this->settingsService = $settingsService;
        $this->settingsFacade = $settingsFacade;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            EventLocator::COMPANY_TRANSFERRED => 'onCompanyTransferred',
            EventLocator::ORDER_REFUNDED => ['onOrderRefunded', 25],
        ];
    }

    /**
     * @param CompanyTransferredEvent $event
     */
    public function onCompanyTransferred(CompanyTransferredEvent $event)
    {
        $this->settingsService->disableAutoRenewalByCompany($event->getCompany());
    }

    /**
     * @param RefundEvent $refundEvent
     */
    public function onOrderRefunded(RefundEvent $refundEvent)
    {
        foreach ($refundEvent->getOrderItems() as $orderItem) {
            $service = $this->serviceService->getOrderItemService($orderItem);
            if ($service) {
                $settings = $this->settingsService->getSettingsByService($service);
                $this->settingsFacade->disableAutoRenewal($settings);
            }
        }
    }
}
