<?php

namespace ServiceSettingsModule\Facades;

use Entities\Payment\Token;
use Entities\ServiceSettings;
use ServiceSettingsModule\Togglers\AutoRenewalToggler;
use ServiceSettingsModule\Togglers\EmailRemindersToggler;

class ServiceSettingsFacade
{

    /**
     * @var AutoRenewalToggler
     */
    private $autoRenewalToggler;

    /**
     * @var EmailRemindersToggler
     */
    private $emailRemindersToggler;

    /**
     * @param AutoRenewalToggler $autoRenewalToggler
     * @param EmailRemindersToggler $emailRemindersToggler
     */
    public function __construct(AutoRenewalToggler $autoRenewalToggler, EmailRemindersToggler $emailRemindersToggler)
    {
        $this->autoRenewalToggler = $autoRenewalToggler;
        $this->emailRemindersToggler = $emailRemindersToggler;
    }

    /**
     * @param ServiceSettings $settings
     * @param Token $token
     */
    public function enableAutoRenewal(ServiceSettings $settings, Token $token)
    {
        $this->autoRenewalToggler->enable($settings, $token);
        $this->emailRemindersToggler->enable($settings);
    }

    /**
     * @param ServiceSettings $settings
     */
    public function disableAutoRenewal(ServiceSettings $settings)
    {
        $this->autoRenewalToggler->disable($settings);
    }

    /**
     * @param ServiceSettings $settings
     */
    public function suspendAutoRenewal(ServiceSettings $settings)
    {
        $this->autoRenewalToggler->suspend($settings);
    }

    /**
     * @param ServiceSettings $settings
     */
    public function enableEmailReminders(ServiceSettings $settings)
    {
        $this->emailRemindersToggler->enable($settings);
    }

    /**
     * @param ServiceSettings $settings
     */
    public function disableEmailReminders(ServiceSettings $settings)
    {
        $this->emailRemindersToggler->disable($settings);
    }
}
