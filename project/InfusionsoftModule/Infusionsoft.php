<?php

namespace InfusionsoftModule;

use FEmail;
use Infusionsoft_App;
use Infusionsoft_EmailService;

class Infusionsoft
{
    /**
     * @var Infusionsoft_App
     */
    private $connection;

    /**
     * @param Infusionsoft_App $connection
     */
    public function __construct(Infusionsoft_App $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param int $templateId
     * @return FEmail
     */
    public function getEmailTemplateById($templateId)
    {
        $emailService = new Infusionsoft_EmailService();
        $templateData = $emailService->getEmailTemplate($templateId, $this->connection);

        $email = new FEmail();
        $email->subject = $templateData['subject'];
        $email->body = $templateData['htmlBody'] ?: $templateData['textBody'];
        $email->from = $templateData['fromAddress'];
        $email->setTo($templateData['toAddress']);

        if (strstr($email->from, '"') !== FALSE) {
            if (preg_match('/"(.*)"/', $email->from, $matches)) {
                $email->fromName = $matches[1];
            }
        }

        if (strstr($email->from, '<') !== FALSE) {
            if (empty($email->fromName) && preg_match('/(.*) <.*>/', $email->from, $matches)) {
                $email->fromName = $matches[1];
            }

            if (preg_match('/<(.*)>/', $email->from, $matches)) {
                $email->from = $matches[1];
            }
        }

        return $email;
    }
}
