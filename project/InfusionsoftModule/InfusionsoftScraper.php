<?php

namespace InfusionsoftModule;


use Behat\Mink\Element\DocumentElement;
use Behat\Mink\Exception\ElementNotFoundException;
use Behat\Mink\Session;
use Doctrine\Common\Cache\Cache;
use InvalidArgumentException;
use InvalidStateException;

class InfusionsoftScraper
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @var Cache
     */
    private $cache;

    /**
     * @var bool
     */
    private $isLoggedIn = FALSE;

    /**
     * @var string
     */
    private $username;

    /**
     * @var array
     */
    private $passwords;

    /**
     * @param string $username
     * @param array $passwords
     * @param Session $session
     * @param Cache $cache
     */
    public function __construct($username, array $passwords, Session $session, Cache $cache)
    {
        $this->session = $session;
        $this->cache = $cache;
        $this->username = $username;
        $this->passwords = $passwords;

        if ($this->cache->contains('password')) {
            array_unshift($this->passwords, $this->cache->fetch('password'));
        }
    }

    /**
     * @param string $id
     * @return mixed
     * @throws ElementNotFoundException
     */
    public function getLinkById($id)
    {
        if (!$this->isLoggedIn) {
            $this->logIn();
        }

        if (!$this->cache->contains($id)) {
            $this->session->visit('https://bo281.infusionsoft.com/SystemEmailLink/manageSystemEmailLink.jsp?global=true&view=edit&ID=' . $id);
            $element = $this->session->getPage()->findById('SystemEmailLink0LinkTargetHREF');

            if (!$element) {
                $header = $this->session->getPage()->find('css', '.page-header > h2');
                if ($header && $header->getText() == 'Manage Automation Link') {
                    throw new InvalidArgumentException("Link $id is not replaceable. Probably a thank you page.");
                } else {
                    throw new ElementNotFoundException($this->session,
                        "Link value element for link $id",
                        '#SystemEmailLink0LinkTargetHREF');
                }
            }

            if (empty($element->getValue())) {
                throw new InvalidArgumentException("Link $id is not replaceable because its value is empty!");
            }

            $this->cache->save($id, $element->getValue(), 60);
        }

        return $this->cache->fetch($id);
    }

    public function login()
    {
        $this->session->start();
        $this->session->visit('https://signin.infusionsoft.com/login');

        foreach ($this->passwords as $password) {
            $page = $this->session->getPage();

            $resetPasswordForm = $page->findById('resetPasswordForm');
            if ($resetPasswordForm) {
                $password = $this->resetPassword($page);
            }

            $page->findById('username')->setValue($this->username);
            $page->findById('password')->setValue($password);
            $page->findById('loginForm')->submit();

            if ($this->session->getCurrentUrl() != 'https://signin.infusionsoft.com/login') {
                $this->cache->save('password', $password, 36000);
                $this->isLoggedIn = TRUE;
                break;
            }
        }

        if (!$this->isLoggedIn) {
            throw new InvalidStateException('Unable to login or reset password.');
        }
    }

    /**
     * @param DocumentElement $page
     * @return array
     */
    private function resetPassword(DocumentElement $page)
    {
        $setPassword = '';
        foreach ($this->passwords as $password) {
            $page->findById('password1')->setValue($password);
            $page->findById('password2')->setValue($password);
            sleep(5);
            $page->findById('resetPasswordForm')->submit();

            if ($this->session->getCurrentUrl() != 'https://signin.infusionsoft.com/login') {
                $setPassword = $password;
                break;
            }
        }

        return $setPassword;
    }
}
