<?php

namespace InfusionsoftModule;

use Entities\Customer;
use FEmail;

class EmailPlaceholderReplacer
{
    /**
     * @var string
     */
    private $ownerFirstName;

    /**
     * @var string
     */
    private $ownerLastName;

    /**
     * @var string
     */
    private $ownerEmail;
    /**
     * @var InfusionsoftScraper
     */
    private $scraper;

    /**
     * @param string $ownerFirstName
     * @param string $ownerLastName
     * @param string $ownerEmail
     * @param InfusionsoftScraper $scraper
     */
    public function __construct($ownerFirstName, $ownerLastName, $ownerEmail, InfusionsoftScraper $scraper)
    {
        $this->ownerFirstName = $ownerFirstName;
        $this->ownerLastName = $ownerLastName;
        $this->ownerEmail = $ownerEmail;
        $this->scraper = $scraper;
    }

    /**
     * @param FEmail $email
     * @param Customer $customerContext
     * @return FEmail
     */
    public function replacePlaceholders(FEmail $email, Customer $customerContext)
    {
        $replacementContext = [
            '~Contact.Email~' => $customerContext->getEmail(),
            '~Contact.FirstName~' => $customerContext->getFirstName(),
            '~Contact.LastName~' => $customerContext->getLastName(),
            '~Owner.FirstName~' => $this->ownerFirstName,
            '~Owner.LastName~' => $this->ownerLastName,
            '~Owner.Email~' => $this->ownerEmail,
            '~Company.HTMLCanSpamAddressBlock~' => ''
        ];

        $email->from = $this->ownerEmail;
        $email->fromName = trim("{$this->ownerFirstName} {$this->ownerLastName}");
        $email->setTo($this->replace($email->getTo(), $replacementContext));
        $email->subject = $this->replace($email->subject, $replacementContext);
        $email->body = $this->replace($email->body, $replacementContext);
        $email->body = $this->replaceLinks($email->body);

        return $email;
    }

    /**
     * @param string $subject
     * @param array $context
     * @return string
     */
    private function replace($subject, array $context)
    {
        if (is_array($subject)) {
            $processed = [];

            foreach ($subject as $part) {
                $processed[] = $this->replace($part, $context);
            }

            return $processed;
        }

        foreach ($context as $placeholder => $value) {
            $subject = str_replace($placeholder, $value, $subject);
        }

        return $subject;
    }

    /**
     * @param string $string
     * @return mixed
     */
    private function replaceLinks($string)
    {
        preg_match_all('/~Link-([0-9]+)~/', $string, $matches);

        foreach ($matches[1] as $index => $id) {
            $string = str_replace($matches[0][$index], $this->scraper->getLinkById($id), $string);
        }

        return $string;
    }
}
