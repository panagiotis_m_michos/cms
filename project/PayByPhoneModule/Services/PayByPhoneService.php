<?php

namespace PayByPhoneModule\Services;

use Basket;
use Customer;
use Entities\Order;
use FForm;
use FUser;
use Nette\Utils\Validators;
use PaymentService;
use PaymentViaPhoneAdminControler;
use PaymentViaPhoneFormDelegate;
use Services\CompanyService;
use Services\CustomerService;
use Services\NodeService;
use Services\OrderService;
use Services\PackageService;
use Services\ProductService;
use Services\UserService;
use VoucherModule\Services\VoucherService;
use VoucherNew;

class PayByPhoneService
{
    /**
     * @var OrderService
     */
    private $orderService;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var ProductService
     */
    private $productService;

    /**
     * @var CustomerService
     */
    private $customerService;

    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var NodeService
     */
    private $nodeService;

    /**
     * @var VoucherService
     */
    private $voucherService;

    /**
     * @var PackageService
     */
    private $packageService;

    /**
     * @param CustomerService $customerService
     * @param CompanyService $companyService
     * @param OrderService $orderService
     * @param ProductService $productService
     * @param UserService $userService
     * @param NodeService $nodeService
     * @param VoucherService $voucherService
     * @param PackageService $packageService
     */
    public function __construct(
        CustomerService $customerService,
        CompanyService $companyService,
        OrderService $orderService,
        ProductService $productService,
        UserService $userService,
        NodeService $nodeService,
        VoucherService $voucherService,
        PackageService $packageService
    )
    {
        $this->customerService = $customerService;
        $this->companyService = $companyService;
        $this->orderService = $orderService;
        $this->productService = $productService;
        $this->userService = $userService;
        $this->nodeService = $nodeService;
        $this->voucherService = $voucherService;
        $this->packageService = $packageService;
    }

    /**
     * @param PaymentViaPhoneFormDelegate $delegate
     * @param FForm $form
     * @param array $get
     * @param array $post
     */
    public function processPhonePayment(PaymentViaPhoneFormDelegate $delegate, $form, $get, $post)
    {
        $agent = $this->userService->getUserById(FUser::getSignedIn()->getId());
        $customer = $this->customerService->getCustomerByEmail($post['emails']);
        $oldCustomer = new Customer($customer ? $customer->getId() : 0);

        $basket = $this->getPhonePaymentBasket($post);
        $paymentService = new PaymentService(
            $oldCustomer,
            $basket,
            $customer,
            $this->orderService,
            $this->productService
        );

        $package = $this->packageService->getPackageById($form->getValue('packages'));
        $processTokenPayment = (!$customer || !$customer->hasActiveToken()) && $package->isServiceWithAutoRenewalAllowed();
        $orderId = $paymentService->processPhonePaymentForm($form, $get, $post, $processTokenPayment);

        if (!$customer) {
            $customer = $this->customerService->getCustomerByEmail($post['emails']);
            $customer->setPhone($post['phone']);
            $this->customerService->save($customer);
        }

        $order = $this->orderService->getOrderById($orderId);
        $order->setPaymentMediumId(Order::PAYMENT_MEDIUM_PHONE);
        $order->setAgent($agent);
        $order->setCustomerPhone($customer->getPhone());
        $this->orderService->saveOrder($order);

        $company = $this->companyService->getCompanyByOrder($order);
        $delegate->paymentViaPhoneSuccess($order, $company->getId());
    }

    /**
     * @param array $post
     * @return Basket $basket
     */
    public function getPhonePaymentBasket($post)
    {
        $basket = new Basket(PaymentViaPhoneAdminControler::$namespace);
        $basket->clear();
        if (!empty($post['packages'])) {
            $item = $this->nodeService->getProductById((int) $post['packages']);
            $basket->add($item);
        }

        if (!empty($post['products'])) {
            foreach ($post['products'] as $itemId) {
                $item = $this->nodeService->getProductById((int) $itemId);
                $item->isAssociated = TRUE;
                $basket->add($item);
            }
        }

        if (!empty($post['voucher'])) {
            $voucher = $this->voucherService->getVoucherByCode($post['voucher']);
            if ($voucher) {
                $oldVoucher = new VoucherNew($voucher->getId());
                if ($oldVoucher->isAvailableForUse($basket->getPrice('subTotal'))) {
                    $basket->setVoucher($voucher->getId());
                };
            }
        }

        return $basket;
    }

    /**
     * @param string $ajaxEmail
     * @return array
     */
    public function getPhonePaymentCustomerDetails($ajaxEmail)
    {
        $email = urldecode($ajaxEmail);
        if (isset($email) && Validators::isEmail($email)) {
            $customer = $this->customerService->getCustomerByEmail($email);
            if ($customer) {
                $responseData = [
                    'success' => 1,
                    'customerId' => $customer->getId(),
                    'customerName' => $customer->getFullName(),
                ];
            } else {
                $responseData = ['success' => 0];
            }
        } else {
            $responseData = ['success' => -1];
        }

        return $responseData;
    }
}

