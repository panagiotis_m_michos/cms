<?php

namespace PayByPhoneModule\Forms;

use Exception;
use FApplication;
use FControl;
use FForm;
use Package;
use PaymentViaPhoneFormDelegate;
use Product;
use SagePayDirect;
use Services\PackageService;
use Text;
use WPDirect;

class PayByPhoneForm extends FForm
{
    /**
     * @var array
     */
    const AVAILABLE_VOUCHERS = [
        '20phonediscount' => '20% phone discount for Privacy up',
    ];

    /**
     * @var bool
     */
    protected $storable = FALSE;

    /**
     * @var PaymentViaPhoneFormDelegate
     */
    private $delegate;

    /**
     * @var PackageService
     */
    private $packageService;

    /**
     * @param PaymentViaPhoneFormDelegate $delegate
     */
    public function startup(PaymentViaPhoneFormDelegate $delegate)
    {
        $this->packageService = FApplication::$container->get('services.package_service');
        $this->delegate = $delegate;
        $this->init();
    }

    private function init()
    {
        $this->addText('emails', 'Email: *')
            ->addRule(FForm::Required, 'Please provide e-mail address!')
            ->addRule(FForm::Email, 'Email is not valid!');
        $this->addText('phone', 'Phone: *')
            ->setDescription('(Only numbers, no spaces, max 14 characters)')
            ->addRule([$this, 'Validator_phoneNumber'], 'Please provide phone number')
            ->addRule(self::REGEXP, 'Phone number is not valid.', '#^\d{1,14}$#');

        $this->addSelect('packages', 'Packages:', $this->packageService->getPublishedPackagesPairs())
            ->setFirstOption('--- Select ---')
            ->class('pvp-package-mb');
        $this->add('AsmSelect', 'products', 'Associated products:')
            ->multiple()
            ->title('--- Select ---')
            ->style('width:300px;')
            ->setOptions(Product::$paymantViaPhoneProductsAvailable)
            ->setValue([Product::PRODUCT_FRAUD_PROTECTION]);
        $this->addSelect('voucher', 'Voucher:', self::AVAILABLE_VOUCHERS)
            ->setFirstOption('None');

        $this->addText('amount', 'Amount')
            ->class('readonlyInput bold')
            ->readonly();
        $this->addText('cardholder', "Cardholder's Name: *")
            ->autocomplete('off')
            ->addRule([$this, 'Validator_requiredFields'], "Cardholder's name is required!")
            ->addRule('MaxLength', 'Name must be upto 50 characters', [50]);
        $this->addSelect('cardType', 'Card Type: *', SagePayDirect::getCardTypes())
            ->setFirstOption('--- Select ---')
            ->addRule([$this, 'Validator_requiredFields'], "Card type is required!");
        $this->addText('cardNumber', 'Card Number: *')
            ->autocomplete('off')
            ->addRule([$this, 'Validator_requiredFields'], 'Card number is required!')
            ->addRule('MaxLength', 'Too long card number. Do NOT enter spaces.', [20]);
        $this->addText('issueNumber', 'Issue Number: ')
            ->size(1)
            ->addRule([$this, 'Validator_IssueNumber'], 'Issue number or Valid from is required!')
            ->addRule('MaxLength', 'Issue number must be upto 2 characters', [2]);
        $this->addText('CV2', 'Security Code: *')
            ->size(3)
            ->autocomplete('off')
            ->addRule(
                [$this, 'Validator_securityCode'],
                ["Security code must be a three digit number!", "Security code must be a four digit number!"]
            );
        $this->add('CardExpiryDate', 'expiryDate', 'Expiry Date: *')
            ->addRule(
                [$this, 'Validator_ExpiryDate'],
                ['Expiry date is required!', "Expiry date has to be more than or equal to today's date!"]
            );
        $this->add('CardValidFromDate', 'validFrom', 'Valid from: ')
            ->addRule([$this, 'Validator_IssueNumber'], 'Issue number or Valid from is required!');
        $this->addText('address1', 'Address 1: *')
            ->autocomplete('off')
            ->addRule([$this, 'Validator_requiredFields'], 'Address is required!')
            ->addRule('MaxLength', 'Address 1 must be upto 100 characters', [100]);
        $this->addText('address2', 'Address 2: ')
            ->autocomplete('off')
            ->addRule('MaxLength', 'Address 2 must be upto 50 characters', [50]);
        $this->addText('address3', 'Address 3: ')
            ->autocomplete('off')
            ->addRule('MaxLength', 'Address 3 must be upto 50 characters', [50]);
        $this->addText('town', 'Town: *')
            ->autocomplete('off')
            ->addRule([$this, 'Validator_requiredFields'], 'Town is required!')
            ->addRule('MaxLength', 'Town must be upto 40 characters', [40]);
        $this->addText('postcode', 'Post Code: *')
            ->autocomplete('off')
            ->addRule([$this, 'Validator_requiredFields'], 'Post code is required!')
            ->addRule('MaxLength', 'Post code must be upto 10 characters', [10]);
        $this->addSelect('country', 'Country: *', WPDirect::$countries)
            ->setFirstOption('--- Select ---')
            ->style('width: 150px;')
            ->addRule([$this, 'Validator_requiredFields'], 'Country is required!');
        $this->addSelect('billingState', 'State: *', SagePayDirect::getStates())
            ->setFirstOption('--- Select ---');

        $this->addHidden('paymentType', 0);
        $this->addHidden('newCustomer', 0);

        $this->addSubmit('submit', 'Submit Payment')->class('btn');
        $this->addSubmit('cancel', 'Cancel')->class('btn');

        $this->beforeValidation = [$this, 'beforeValidation'];
        $this->onValid = [$this, 'process'];
        $this->start();
    }

    public function process()
    {
        try {
            $this->delegate->paymentViaPhoneProcessForm($this);
        } catch (Exception $e) {
            $this->delegate->paymentViaPhoneFormWithException($this, $e);
        }
    }

    /*     * ******************************** validators ******************************** */

    public function beforeValidation()
    {
        if ($this->isSubmitedBy('cancel')) {
            $this->clean();
            $this->delegate->paymentViaPhoneFormCancelled();
        }
    }

    /**
     * @param object $control
     * @param string $error
     * @return mixed
     */
    public function Validator_IssueNumber($control, $error)
    {
        $form = $control->owner;
        $cardType = strtolower($form['cardType']->getValue());
        $issue = $form['issueNumber']->getValue();
        $validFrom = $form['validFrom']->getValue();
        if (($cardType == 'maestro' || $cardType == 'solo') && empty($issue) && empty($validFrom)) {
            return $error;
        }

        return TRUE;
    }

    /**
     * @param object $control
     * @param string $error
     * @return mixed
     */
    public function Validator_ExpiryDate($control, $error)
    {
        // required
        if ($control->getValue() == NULL) {
            return $error[0];
        }
        // greater than
        $expireDate = $control->owner['expiryDate']->getValue();
        if (strtotime($expireDate) < strtotime(date("Y-m"))) {
            return $error[1];
        }

        return TRUE;
    }

    /**
     * Check required fields fo card payment
     *
     * @param object $control
     * @param string $error
     * @return mixed
     */
    public function Validator_requiredFields($control, $error)
    {
        $value = $control->getValue();
        if (empty($value)) {
            return $error;
        }

        return TRUE;
    }

    /**
     * @param FControl $control
     * @param mixed $error
     * @return mixed
     */
    public function Validator_securityCode($control, $error)
    {
        $value = $control->getValue();

        $isAmex = $control->owner['cardType']->getValue() == SagePayDirect::CARD_AMEX;
        $requiredLength = $isAmex ? SagePayDirect::SECURITY_CODE_AMEX_LENGTH : SagePayDirect::SECURITY_CODE_DEFAULT_LENGTH;

        if (empty($value) || !is_numeric($value) || strlen($value) != $requiredLength) {
            return $isAmex ? $error[1] : $error[0];
        }

        return TRUE;
    }

    /**
     * @param Text $control
     * @param string $error
     * @return mixed
     */
    public function Validator_phoneNumber(Text $control, $error)
    {
        if ($control->owner['newCustomer']->getValue() == 1 && empty($control->getValue())) {
            return $error;
        }

        return TRUE;
    }
}

