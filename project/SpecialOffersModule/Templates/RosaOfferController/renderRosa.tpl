{extends '@structure/layout.tpl'}

{block content}
    <div class="width100 london-bg padcard">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="white font-300">{$introductionStrip.title nofilter}<br><br>{$introductionStrip.titleSecondary nofilter}</h1>
                    <div class="row top45">
                        <div class="col-xs-12 col-sm-4 button-wide">
                            {ui name="buy_button" data=$introductionStrip.buyButton}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="width100 bg-white padcard">
        <div class="container">
            <div class="row">
                {foreach $matrixStrip.blocks as $block}
                    <div class="col-xs-12 col-sm-4 text-center">
                        <div class="rosa-matrix-block">
                            <h2 class="margin0 font-300">{$block.title nofilter}</h2>
                            <h2 class="margin0 orange">{$block.price nofilter}<span class="orange xsmall">+VAT</span></h2>
                            <p class="lead black80">{$block.oldPrice nofilter}<span class="xsmall">+VAT</span></p>
                            <div class="row">
                                <div class="col-xs-12 button-wide">
                                    {ui name="buy_button" data=$block.buyButton}
                                </div>
                            </div>
                            {$block.fom nofilter}
                        </div>
                    </div>
                {/foreach}
            </div>
        </div>
    </div>
    <div class="width100 bg-white padcard">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2>{$featuresStrip.title nofilter}</h2>
                    {foreach $featuresStrip.features as $feature}
                        <p class="lead">
                            <strong>{$feature.title nofilter}</strong><br>
                            {$feature.description nofilter}
                        </p>
                    {/foreach}
                </div>
            </div>
        </div>
    </div>
    {foreach $faqStrip.faqs as $faq}
    <div id="{if isset($faq.blockId)}{$faq.blockId}{/if}" class="width100 bg-white padcard rosa-faq-block">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3>{$faq.blockTitle nofilter}</h3>
                    {foreach $faq.faqBlocks as $faqBlock}
                        <h4>{$faqBlock.title nofilter}</h4>
                        <p>{$faqBlock.description nofilter}</p>
                    {/foreach}
                    <p class="xsmall top45">{if isset($faq.footNote)}{$faq.footNote}{/if}</p>
                </div>
            </div>
        </div>
    </div>
    {/foreach}
{/block}
