<?php

namespace SpecialOffersModule\Controllers;

use Symfony\Component\HttpFoundation\Response;
use TemplateModule\Renderers\IRenderer;

class RosaOfferController
{
    /**
     * @var IRenderer
     */
    private $renderer;

    /**
     * @param IRenderer $renderer
     */
    public function __construct(IRenderer $renderer)
    {
        $this->renderer = $renderer;
    }

    /**
     * @return Response
     */
    public function renderRosa()
    {
        return $this->renderer->render();
    }
}
