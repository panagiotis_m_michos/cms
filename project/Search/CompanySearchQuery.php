<?php

namespace Search;

use DateTime;
use Entities\Customer;
use Symfony\Component\HttpFoundation\Request;
use Utils\Date;

class CompanySearchQuery
{
    const TYPE_ACCOUNTS = 'accounts';
    const TYPE_RETURNS = 'returns';

    /**
     * @var array
     */
    public static $types = array(
        self::TYPE_ACCOUNTS => 'Accounts Due',
        self::TYPE_RETURNS => 'Returns Due',
    );

    /**
     * @var string
     */
    private $term;

    /**
     * @var bool
     */
    private $onlyActive;

    /**
     * @var string
     */
    private $dateFilterType;

    /**
     * @var DateTime
     */
    private $dateFrom;

    /**
     * @var DateTime
     */
    private $dateTo;

    /**
     * @var int
     */
    private $offset;

    /**
     * @var int
     */
    private $limit;

    /**
     * @var int
     */
    private $draw;

    /**
     * @var string
     */
    private $sortingDirection;

    /**
     * @var string
     */
    private $sortingFieldIndex;

    /**
     * @param int $offset
     * @param int $limit
     * @return CompanySearchQuery
     */
    public static function fromDefault($offset, $limit)
    {
        $self = new self();
        $self->offset = $offset;
        $self->limit = $limit;
        return $self;
    }

    /**
     * @param Request $request
     * @param integer $limit
     * @return CompanySearchQuery
     */
    public static function fromParams(Request $request, $limit)
    {
        $self = self::fromDefault(0, $limit);
        $self->sortingDirection = $request->get('order[0][dir]', NULL, TRUE) == 'asc' ? 'ASC' : 'DESC';
        $self->sortingFieldIndex = $request->get('order[0][column]', NULL, TRUE);
        $self->draw = (int) $request->get('draw');
        $self->offset = (int) $request->get('start');
        return $self;
    }

    /**
     * @param string $term
     */
    public function setTerm($term)
    {
        $this->term = $term;
    }

    /**
     * @return string
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * @return string
     */
    public function getDateFilterType()
    {
        return $this->dateFilterType;
    }

    /**
     * @param string $dateFilterType
     */
    public function setDateFilterType($dateFilterType)
    {
        $this->dateFilterType = $dateFilterType;
    }

    /**
     * @return DateTime
     */
    public function getDateFrom()
    {
        return $this->dateFrom;
    }

    /**
     * @param DateTime $dateFrom
     */
    public function setDateFrom($dateFrom)
    {
        $this->dateFrom = $dateFrom;
    }

    /**
     * @return DateTime
     */
    public function getDateTo()
    {
        return $this->dateTo;
    }

    /**
     * @param DateTime $dateTo
     */
    public function setDateTo($dateTo)
    {
        $this->dateTo = $dateTo;
    }

    /**
     * @return boolean
     */
    public function isOnlyActive()
    {
        return $this->onlyActive;
    }

    /**
     * @param boolean $onlyActive
     */
    public function setOnlyActive($onlyActive)
    {
        $this->onlyActive = $onlyActive;
    }

    /**
     * @return bool
     */
    public function isAccounts()
    {
        return $this->getDateFilterType() === self::TYPE_ACCOUNTS;
    }

    /**
     * @return bool
     */
    public function isReturns()
    {
        return $this->getDateFilterType() === self::TYPE_RETURNS;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     * @return CompanySearchQuery
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
        return $this;
    }

    /**
     * @return int
     */
    public function getDraw()
    {
        return $this->draw;
    }

    /**
     * @param int $draw
     */
    public function setDraw($draw)
    {
        $this->draw = $draw;
    }

    /**
     * @return string
     */
    public function getSortingDirection()
    {
        return $this->sortingDirection;
    }

    /**
     * @param string $sortingDirection
     */
    public function setSortingDirection($sortingDirection)
    {
        $this->sortingDirection = $sortingDirection;
    }

    /**
     * @return string
     */
    public function getSortingFieldIndex()
    {
        return $this->sortingFieldIndex;
    }

    /**
     * @return string
     */
    public function getSortingField()
    {
        switch ($this->getSortingFieldIndex()) {
            case 0:
                return 'companyName';
            case 1:
                return 'companyNumber';
            case 2:
                return 'incorporationDate';
            case 3:
                return 'companyStatus';
            case 4:
                return 'accountsNextDueDate';
            case 5:
                return 'returnsNextDueDate';
            default:
                return NULL;
        }
    }

    /**
     * @param string $sortingFieldIndex
     */
    public function setSortingFieldIndex($sortingFieldIndex)
    {
        $this->sortingFieldIndex = $sortingFieldIndex;
    }

}
