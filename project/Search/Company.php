<?php

namespace Search;

use Entities\Company as CompanyEntity;
use Factories\Front\CompanyViewFactory;
use Models\View\CompanyView;
use Utils\Date;

class Company
{
    const NO_ACTION_REQUIRED = 'No action needed';
    const ACTION_REQUIRED = 'Action required';

    /**
     * @var CompanyView
     */
    public $view;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $number;

    /**
     * @var Date
     */
    private $incorporationDate;

    /**
     * @var string
     */
    private $status;

    /**
     * @var Date
     */
    private $accountsDue;

    /**
     * @var Date
     */
    private $returnsDue;

    /**
     * @param array $companyFields
     * @return Company
     */
    public static function fromArray(array $companyFields)
    {
        $company = new Company();
        if (!empty($companyFields['companyName'])) {
            $company->name = $companyFields['companyName'];
        }

        return $company;
    }

    /**
     * @param CompanyEntity $companyEntity
     * @param CompanyViewFactory $companyViewFactory
     * @return Company
     */
    public static function fromEntity(CompanyEntity $companyEntity, CompanyViewFactory $companyViewFactory)
    {
        $company = new self();
        $company->id = $companyEntity->getId();
        $company->name = $companyEntity->getCompanyName();
        $company->number = $companyEntity->getCompanyNumber();
        $company->status = $companyEntity->getCompanyStatus();
        $company->incorporationDate = $companyEntity->getIncorporationDate();
        $company->accountsDue = $companyEntity->getAccountsNextDueDate();
        $company->returnsDue = $companyEntity->getReturnsNextDueDate();
        $company->view = $companyViewFactory->create($companyEntity);
        return $company;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return Date
     */
    public function getAccountsDue()
    {
        return $this->accountsDue;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @return Date
     */
    public function getIncorporationDate()
    {
        return $this->incorporationDate;
    }

    /**
     * @return Date
     */
    public function getReturnsDue()
    {
        return $this->returnsDue;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return bool
     */
    public function hasServices()
    {
        return $this->view->hasServices();
    }

    /**
     * @return bool
     */
    public function isCustomerActionRequired()
    {
        return $this->view->isServiceActionRequired();
    }

    /**
     * @return string
     */
    public function getServicesActionStatus()
    {
        return $this->isCustomerActionRequired() ? self::ACTION_REQUIRED : self::NO_ACTION_REQUIRED;
    }

    /**
     * @return mixed data which can be serialized by <b>json_encode</b>,
     */
    public function jsonSerialize()
    {
        return [
            $this->getName(),
            $this->getNumber(),
            (string) $this->getIncorporationDate(),
            $this->getStatus(),
            (string) $this->getAccountsDue(),
            (string) $this->getReturnsDue(),
            $this->getId(),
            $this->hasServices(),
            $this->isCustomerActionRequired(),
            $this->getServicesActionStatus()
        ];
    }


}
