<?php

namespace Search;

use Doctrine\ORM\QueryBuilder;
use Entities\Customer;

interface ICompanyRepository
{

    /**
     * @param Customer $customer
     * @param CompanySearchQuery $companySearchQuery
     * @return QueryBuilder
     */
    public function findByQuery(Customer $customer, CompanySearchQuery $companySearchQuery);
}