<?php

namespace Search;

use Entities\Customer;
use Factories\Front\CompanyViewFactory;

class CompanyFinder
{
    /**
     * @var ICompanyRepository
     */
    private $companyRepository;

    /**
     * @var CompanyViewFactory
     */
    private $companyViewFactory;

    /**
     * @param ICompanyRepository $companyRepository
     * @param CompanyViewFactory $companyViewFactory
     */
    public function __construct(ICompanyRepository $companyRepository, CompanyViewFactory $companyViewFactory)
    {
        $this->companyRepository = $companyRepository;
        $this->companyViewFactory = $companyViewFactory;
    }

    /**
     * @param Customer $customer
     * @param CompanySearchQuery $companySearchQuery
     * @return CompanyIterator
     */
    public function findCustomerCompanies(Customer $customer, CompanySearchQuery $companySearchQuery)
    {
        $iterator = $this->companyRepository->findByQuery($customer, $companySearchQuery);
        return new CompanyIterator($iterator, $this->companyViewFactory);
    }

    /**
     * @param Customer $customer
     * @param CompanySearchQuery $companySearchQuery
     * @return int
     */
    public function countCustomerCompanies(Customer $customer, CompanySearchQuery $companySearchQuery)
    {
        $companySearchQuery = clone $companySearchQuery;
        $companySearchQuery->setOffset(0);
        return $this->companyRepository->countByQuery($customer, $companySearchQuery);

    }
}
