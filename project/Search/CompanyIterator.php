<?php

namespace Search;

use Doctrine\ORM\Internal\Hydration\IterableResult;
use Factories\Front\CompanyViewFactory;
use Iterator;

class CompanyIterator implements Iterator
{

    /**
     * @var IterableResult
     */
    private $iterableResult;

    /**
     * @var CompanyViewFactory
     */
    private $companyViewFactory;

    /**
     * @param IterableResult $iterableResult
     * @param CompanyViewFactory $companyViewFactory
     */
    public function __construct(IterableResult $iterableResult, CompanyViewFactory $companyViewFactory)
    {
        $this->iterableResult = $iterableResult;
        $this->companyViewFactory = $companyViewFactory;
    }

    /**
     * @return bool|Company
     */
    public function current()
    {
        $current = $this->iterableResult->current();
        return $current ? Company::fromEntity($current[0], $this->companyViewFactory) : FALSE;
    }

    /**
     * Move forward to next element
     * @return bool|Company
     */
    public function next()
    {
        $this->iterableResult->next();
        return $this->current();
    }

    /**
     * Return the key of the current element
     */
    public function key()
    {
        return $this->iterableResult->key();
    }

    /**
     * Checks if current position is valid
     * @return boolean The return value will be casted to boolean and then evaluated.
     */
    public function valid()
    {
        return $this->iterableResult->valid();
    }

    /**
     * Rewind the Iterator to the first element
     */
    public function rewind()
    {
        $this->iterableResult->rewind();
    }

    /**
     * @param CompanyIterator $companies
     * @return Company
     */
    public static function getJsonSerialized(CompanyIterator $companies)
    {
        $arr = [];

        foreach ($companies as $key) {
            $arr[] = $key->jsonSerialize();
        }

        return $arr;
    }
}
