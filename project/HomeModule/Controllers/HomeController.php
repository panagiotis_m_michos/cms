<?php

namespace HomeModule\Controllers;

use CsmsModule\ICsms;
use FeefoModule\IFeefo;
use Symfony\Component\HttpFoundation\Response;
use TemplateModule\Renderers\IRenderer;
use Utils\FeedReader\FeedReader;

class HomeController
{
    /**
     * @var FeedReader
     */
    private $blogReader;

    /**
     * @var IFeefo
     */
    private $feefo;

    /**
     * @var ICsms
     */
    private $csms;

    /**
     * @var IRenderer
     */
    private $renderer;

    /**
     * @param FeedReader $blogReader
     * @param IFeefo $feefo
     * @param ICsms $csms
     * @param IRenderer $renderer
     */
    public function __construct(FeedReader $blogReader, IFeefo $feefo, ICsms $csms, IRenderer $renderer)
    {
        $this->blogReader = $blogReader;
        $this->feefo = $feefo;
        $this->csms = $csms;
        $this->renderer = $renderer;
    }

    /**
     * @return Response
     */
    public function renderHomepage()
    {
        $variables = [
            'incorporatedThisYear' => $this->csms->getIncorporationStats()->getIncorporationCountThisYearToNow(),
            'feefoAverage' => $this->feefo->getSummary()->getAverageRating(),
            'feefoReviewCount' => $this->feefo->getSummary()->getReviewCount(),
            'homeBlogPosts' => $this->blogReader->getPosts(2),
        ];

        return $this->renderer->render($variables);
    }

    /**
     * @return Response
     */
    public function renderAlternate1()
    {
        return $this->renderHomepage();
    }

    /**
     * @return Response
     */
    public function renderAlternate2()
    {
        return $this->renderHomepage();
    }

    /**
     * @return Response
     */
    public function renderAlternate3()
    {
        return $this->renderHomepage();
    }

    /**
     * @return Response
     */
    public function renderAlternate4()
    {
        return $this->renderHomepage();
    }
}
