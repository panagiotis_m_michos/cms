{extends '@structure/layout.tpl'}

{block content}
    <script>
        function popupCenter(url, title, w, h) {
        var left = (screen.width/2)-(w/2);
        var top = (screen.height/2)-(h/2);
        return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
        }
    </script>
    <div class="width100 home-banner1 padcard">
        <div class="container">
            <div class="row btm30">
                <div class="col-xs-12 col-md-6">
                    <h1>{$introductionStrip.title nofilter}</h1>
                    <h2 class="font-300">{$introductionStrip.description nofilter}</h2>
                </div>
            </div>
            <div class="row btm30">
                <div class="col-xs-12">
                    <form id="home2016-search" class="form-horizontal" action="/company-formation-name-search-old.html" method="post" name="search">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-12 col-md-10">
                                    <input class="width100 form-control" id="q" name="q" type="text" placeholder="{$introductionStrip.searchPlaceholder nofilter}">
                                </div>
                                <div class="col-xs-12 col-md-2">
                                    <button class="width100 btn btn-lg btn-default" type="submit" name="send"><span><i class="fa fa-search"></i>&nbsp;Search</span></button>
                                </div>
                            </div>
                            <input type="hidden" value="1" id="submited_search" name="submited_search">
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-6 text-left btm20 text-center-xs">
                    <h3><a class="grey7" href="{$introductionStrip.packagesLinkURL nofilter}">{$introductionStrip.packagesLink nofilter}</a></h3>
                </div>
                <div class="hidden-xs hidden-sm col-md-2">
                    <img src="/components/ui_server/imgs/best100.png" alt="Top100" style="height: 85px;" class="pull-right img-responsive">
                </div>
                <div class="col-xs-12 col-md-4 btm30 pad0">
                    <div id="home2016-feefo">
                        <div class="feefo2016-top">
                            <span><strong>GOLD</strong> TRUSTED MERCHANT</span>
                        </div>
                        <div class="feefo2016-central">
                            <div class="feefo2016-left">
                                <img src="{$urlImgs}home2016/feefo.png" alt="" />
                            </div>
                            <div class="feefo2016-right">
                                <span class="yellow2 lead"><strong>{$feefoAverage}%</strong></span>
                                <span class="display-inline white">
                                    <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                </span>
                                <p class="margin0">
                                    <a href="http://www.feefo.com/feefo/viewvendor.jsp?logon=www.madesimplegroup.com/companiesmadesimple" onclick="window.open(this.href, 'Feefo', 'width=1000,height=600,scrollbars,resizable'); return false;">
                                        {$feefoReviewCount} reviews
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="home2016-progress" class="width100">
        <div class="progress-indicator-bg-left hidden-xs hidden-sm"></div>
        <div class="progress-indicator-bg-right hidden-xs hidden-sm"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 pad0">
                    <ul class="progress-indicator">
                        <li class="active"><span class="bubble">1</span><span>Search</span></li>
                        <li><span class="bubble">2</span><span>Select</span></li>
                        <li><span class="bubble">3</span><span>Buy</span></li>
                        <li><span class="bubble">4</span><span>Complete</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="width100 bg-grey-blue padcard">
        <div class="container">
            <div class="row">
                {foreach $featuresStrip.features as $feature}
                    <div class="col-xs-12 col-md-4 text-center white">
                        <img class="padcard" src="{$feature.icon nofilter}" alt="" />
                        <p class="lead font-200">{$feature.title nofilter}</p>
                        <p class="font-200">{$feature.description nofilter}</p>
                    </div>
                {/foreach}
            </div>
        </div>
    </div>
    <div class="width100 bg-white padcard">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h2>{$cashbackStrip.title nofilter}</h2>
                    <p class="lead">{$cashbackStrip.description nofilter}</p>
                </div>
            </div>
            <div class="row btm30">
                {foreach $cashbackStrip.banks as $bank}
                    <div class="col-xs-12 col-md-6 text-center btm10">
                        <div class="padcard blueborder">
                            <a href="{$bank.iconLink nofilter}"><img class="padcard" src="{$bank.icon nofilter}" alt="" /></a>
                            <p>{$bank.description nofilter}</p>
                        </div>
                    </div>
                {/foreach}
            </div>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p><strong>{$cashbackStrip.footer nofilter}</strong></p>
                </div>
            </div>
        </div>
    </div>
    <div class="width100 home-banner3 padcard">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h2>{$counterStrip.title nofilter}</h2>
                </div>
            </div>

            {if $incorporatedThisYear != 0}
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p style="font-size: 4.6em;"><strong>{$incorporatedThisYear}</strong></p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="lead">{$counterStrip.footer nofilter}</p>
                </div>
            </div>
            {/if}
        </div>
    </div>

    {include file="@structure/feefoStrip.tpl"}

    <div class="width100 bg-white padcard">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h2>{$servicesStrip.title}</h2>
                </div>
            </div>
            <div class="row">
                {foreach $servicesStrip.services as $service}
                    <div class="col-xs-12 col-md-4 text-center">
                        <img class="" src="{$service.icon nofilter}" alt="" />
                        <h3>{$service.title nofilter}</h3>
                        <p>{$service.description nofilter}</p>
                    </div>
                {/foreach}
            </div>
        </div>
    </div>
    <div class="width100 home-banner2 padcard">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h2 class="white">{$professionalsStrip.title nofilter}</h2>
                    <p class="white lead">{$professionalsStrip.description nofilter}</p>
                    <a href="{$professionalsStrip.link nofilter}" class="btn btn-lg btn-secondary top20 btm20">{$professionalsStrip.cta nofilter}</a>
                </div>
            </div>
        </div>
    </div>
    <div class="width100 bg-white padcard">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h2>{$partnersStrip.title nofilter}</h2>
                    <p class="lead">{$partnersStrip.description nofilter}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-center">
                    {foreach $partnersStrip.partners as $partner}
                        <img class="padcard" src="{$partner.icon nofilter}" alt="" />
                    {/foreach}
                </div>
            </div>
        </div>
    </div>
    <div class="width100 bg-grey1 padcard">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h2>{$internationalStrip.title nofilter}</h2>
                    <p class="lead">{$internationalStrip.description nofilter}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-center">
                    {foreach $internationalStrip.flags as $flag}
                        <img class="padcard" src="{$flag.icon nofilter}" alt="" />
                    {/foreach}
                </div>
            </div>
        </div>
    </div>
    <div class="width100 bg-white padcard">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-2">
                    <img class="padcard top20 center-block" src="{$ebookStrip.icon nofilter}" alt="" style="width: 137px;"/>
                </div>
                <div class="col-xs-12 col-md-10">
                    <h2>{$ebookStrip.title nofilter}</h2>
                    <p class="lead">{$ebookStrip.description nofilter}</p>
                    <p>{$ebookStrip.content nofilter}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <a onclick="popupCenter('https://themadesimplegroup.leadpages.co/leadbox/1445b2c73f72a2%3A123192981346dc/5725851488354304/', 'Guide',800,600);" class="btn btn-lg btn-secondary top20 btm20" href="javascript:void(0);">{$ebookStrip.cta nofilter}</a>
                </div>
            </div>
        </div>
    </div>
    <div class="width100 home-banner4 padcard">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h2 class="white">{$blogStrip.title nofilter}</h2>
                </div>
            </div>
            <div class="row btm20">
                {if !empty($homeBlogPosts)}
                    {foreach $homeBlogPosts as $post}
                        <div class="col-xs-12 col-md-6 text-center white">
                            <h4 class="white">{$post->getTitle()}</h4>
                            <span>{$post->getLimitedDescription(160)|strip_tags nofilter}</span>
                            <a href="{$post->getLink()}">Continue Reading</a>
                        </div>
                    {/foreach}
                {else}
                    <p><strong>No blog posts.</strong></p>
                {/if}
            </div>
        </div>
    </div>
    <div class="width100 bg-white padcard">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h2 class="top10">{$seoCopy1.title nofilter}</h2>
                    <p>{$seoCopy1.description nofilter}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="width100 bg-grey1 padcard">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="text-center top10">{$seoCopy2.title nofilter}</h2>
                    {$seoCopy2.description nofilter}
                    <ul class="fa-ul">
                        {foreach $seoCopy2.blocks as $block}
                            <li><i class="green fa-li fa fa-check-circle"></i>{$block.description nofilter}</li>
                        {/foreach}
                    </ul>
                    <p>{$seoCopy2.footer nofilter}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="width100 bg-white padcard">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h2 class="top10">{$seoCopy3.title nofilter}</h2>
                    {foreach $seoCopy3.blocks as $block}
                        {if isset($block.title)}<h4>{$block.title nofilter}</h4>{/if}
                        <p>{$block.description nofilter}</p>
                    {/foreach}
                </div>
            </div>
        </div>
    </div>
    <div id="home2016-bottomlinks" class="width100 padcard">
        <div class="container">
            <div class="row">
                {foreach $footerLinks as $column}
                    <div class="col-xs-12 col-md-3">
                        {foreach $column as $group}
                            <h2>{$group.title nofilter}</a></h2>
                            <ul>
                                {foreach $group.links as $link}
                                    <li>{$link.link nofilter}</li>
                                {/foreach}
                            </ul>
                        {/foreach}
                    </div>
                {/foreach}
            </div>
        </div>
    </div>
{/block}

{include file="@footer.tpl"}
