<?php

namespace OfferModule\Mappers;

use CommonModule\Mappers\IMapper;
use OfferModule\Entities\CustomerDetails;

class InsuranceMapper implements IMapper
{
    /**
     * @param CustomerDetails $details
     * @return array
     */
    public function toArray($details)
    {
        return [
            'First Name' => $details->getFirstName(),
            'Last Name' => $details->getLastName(),
            'Telephone' => $details->getPhone(),
            'Email address' => $details->getEmail(),
            'Company name' => $details->getCompanyName(),
            'Switch month' => $details->getInsuranceReturnMonth() ? : 'NULL'
        ];
    }
}
