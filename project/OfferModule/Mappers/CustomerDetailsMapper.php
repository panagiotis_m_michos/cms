<?php

namespace OfferModule\Mappers;

use CommonModule\Mappers\IMapper;
use OfferModule\Entities\CustomerDetails;

class CustomerDetailsMapper implements IMapper
{
    /**
     * @param CustomerDetails $details
     * @return array
     */
    public function toArray($details)
    {
        return [
            'firstName' => $details->getFirstName(),
            'lastName' => $details->getLastName(),
            'email' => $details->getEmail(),
            'phone' => $details->getPhone(),
            'postcode' => $details->getPostcode(),
            'companyName' => $details->getCompanyName(),
        ];
    }
}
