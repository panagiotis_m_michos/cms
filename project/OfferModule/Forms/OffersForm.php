<?php

namespace OfferModule\Forms;

use FormModule\Month;
use OfferModule\Entities\OffersFormData;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class OffersForm extends AbstractType
{
    const OFFER_DIGITAL_RISKS = 'digitalRisks';
    const OFFER_TAX_ASSIST = 'taxAssist';
    const OFFER_WORLDPAY = 'worldPay';

    const LOAN_BUSINESS_CENTRE = 'businessCentre';
    const LOAN_VIRGIN_START_UP = 'virginStartUp';

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'offers',
                'choice',
                [
                    'required' => TRUE,
                    'choices' => [
                        self::OFFER_DIGITAL_RISKS => 'Digital Risks',
                        self::OFFER_TAX_ASSIST => 'Tax Assist',
                        self::OFFER_WORLDPAY => 'Worldpay',
                    ],
                    'expanded' => TRUE,
                    'multiple' => TRUE,
                ]
            )
            ->add(
                'loans',
                'choice',
                [
                    'choices' => [
                        self::LOAN_BUSINESS_CENTRE => 'Business Centre',
                        self::LOAN_VIRGIN_START_UP => 'Virgin Start Up',
                    ],
                    'expanded' => TRUE,
                    'multiple' => TRUE,
                ]
            )
            ->add(
                'firstName',
                'text',
                [
                    'label' => 'First Name',
                    'attr' => [
                        'placeholder' => 'First Name'
                    ]
                ]
            )
            ->add(
                'lastName',
                'text',
                [
                    'label' => 'Last Name',
                    'attr' => [
                        'placeholder' => 'Last Name'
                    ]
                ]
            )
            ->add(
                'phone',
                'text',
                [
                    'label' => 'Phone Number',
                    'attr' => [
                        'placeholder' => 'Phone'
                    ]
                ]
            )
            ->add(
                'email',
                'text',
                [
                    'label' => 'Email Address',
                    'attr' => [
                        'placeholder' => 'E-mail'
                    ]
                ]
            )
            ->add(
                'hasInsurance',
                'checkbox',
                [
                    'label' => 'I have business insurance',
                    'required' => FALSE,
                    'attr' => [
                        'class' => 'has-insurance'
                    ]
                ]
            )
            ->add(
                'insuranceReturnMonth',
                'choice',
                [
                    'placeholder' => 'Select',
                    'label' => 'My insurance renewal is in',
                    'choices' => Month::getNames(),
                    'required' => FALSE,
                    'attr' => [
                        'class' => 'insurance-month-select'
                    ]
                ]
            )
            ->add(
                'save',
                'submit',
                [
                    'label' => 'Submit',
                    'attr' => [
                        'class' => 'orange-submit'
                    ]
                ]
            );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'attr' => [
                    'data-label-ticked' => "I'm interested",
                    'data-label-unticked' => 'Tick if interested',
                ],
                'validation_groups' => function (FormInterface $form) {
                    $groups = ['Default'];

                    /** @var OffersFormData $data */
                    $data = $form->getData();
                    if ($data->hasDigitalRisks() && $data->hasInsurance()) {
                        $groups[] = 'hasInsurance';
                    }

                    return $groups;
                },
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'offerForm';
    }
}
