<?php

namespace OfferModule\Repositories;

use Doctrine\ORM\Internal\Hydration\IterableResult;
use JourneyProduct;
use Repositories\BaseRepository_Abstract;
use Utils\Date;

class OfferRepository extends BaseRepository_Abstract
{
    /**
     * @param JourneyProduct $product
     * @param Date $date
     * @return IterableResult
     */
    public function getCustomerDetailsForDate(JourneyProduct $product, Date $date)
    {
        $toDate = clone $date;
        $toDate->modify('+1 day');
        $qb = $this->createQueryBuilder('o');
        $qb->where('o.productId = :productId')
            ->andWhere('o.dtc >= :fromDate')
            ->andWhere('o.dtc < :toDate')
            ->setParameter('productId', $product->getId())
            ->setParameter('fromDate', $date)
            ->setParameter('toDate', $toDate);
        return $qb->getQuery()->iterate();
    }
}