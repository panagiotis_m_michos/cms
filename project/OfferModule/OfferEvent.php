<?php

namespace OfferModule;

use OfferModule\Entities\CustomerDetails;
use Symfony\Component\EventDispatcher\Event;

class OfferEvent extends Event
{
    const PRODUCT_ADDED = 'offers.product_added';
    const CUSTOMER_DETAILS_ADDED = 'offers.customer_details_added';

    /**
     * @var CustomerDetails
     */
    private $customerDetails;

    /**
     * @param CustomerDetails $customerDetails
     */
    public function __construct(CustomerDetails $customerDetails)
    {
        $this->customerDetails = $customerDetails;
    }

    /**
     * @return CustomerDetails
     */
    public function getCustomerDetails()
    {
        return $this->customerDetails;
    }

    /**
     * @return bool
     */
    public function canSendEmailInstantly()
    {
        $product = $this->customerDetails->getProduct();
        return $product && $product->sendEmailInstantly && $product->emails;
    }

    /**
     * @return bool
     */
    public function canSendEmailToCustomer()
    {
        return trim($this->customerDetails->getProduct()->emailText) != '';
    }
}