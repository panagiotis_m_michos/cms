<?php

namespace OfferModule\Entities;

use Doctrine\ORM\Mapping as Orm;
use Gedmo\Mapping\Annotation as Gedmo;
use DateTime;
use Entities\Customer;
use JourneyProduct;
use Nette\Object;
use Utils\Date;

/**
 * Class CustomerDetails
 * @Orm\Entity(repositoryClass="OfferModule\Repositories\OfferRepository")
 * @Orm\Table(name="cms2_offer_customer_details")
 */
class CustomerDetails extends Object
{
    const PRODUCT_SIMPLY_BUSINESS = 617;
    const PRODUCT_TAX_ASSIST = 544;
    const PRODUCT_SMARTA = 1469;
    const PRODUCT_WORLDPAY = 1458;
    const PRODUCT_BUSINESS_CENTRE = 1642;
    const PRODUCT_VIRGIN_START_UP = 1671;
    const PRODUCT_DIGITAL_RISKS = 1676;

    /**
     * @var int
     *
     * @Orm\Column(type="integer", nullable=false)
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $customerDetailsId;

    /**
     * @var string
     * @Orm\Column(type="string")
     */
    private $firstName;

    /**
     * @var string
     * @Orm\Column(type="string")
     */
    private $lastName;

    /**
     * @var string
     * @Orm\Column(type="string")
     */
    private $email;

    /**
     * @var string
     * @Orm\Column(type="string")
     */
    private $phone;

    /**
     * @var string
     * @Orm\Column(type="string")
     */
    private $postcode;

    /**
     * @var string
     * @Orm\Column(type="string")
     */
    private $companyName;

    /**
     * @var Date
     * @Orm\Column(type="string")
     */
    private $insuranceReturnMonth;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;

    /**
     * @var DateTime
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create", on="update")
     */
    private $dtm;

    /**
     * @var Customer
     * @Orm\ManyToOne(targetEntity="Entities\Customer", inversedBy="companies")
     * @Orm\JoinColumn(name="customerId", referencedColumnName="customerId")
     */
    private $customer;

    /**
     * @var int
     * @Orm\Column(type="integer")
     */
    private $productId;

    /**
     * @var JourneyProduct
     */
    private $product;

    /**
     * @param OffersFormData $formData
     * @param JourneyProduct $product
     * @return CustomerDetails
     */
    public static function fromFormAndProduct(OffersFormData $formData, JourneyProduct $product)
    {
        $self = new self();
        $self->customer = $formData->getCustomer();
        $self->product = $product;
        $self->productId = $product->getId();
        $self->lastName = $formData->getLastName();
        $self->firstName = $formData->getFirstName();
        $self->email = $formData->getEmail();
        $self->postcode = $formData->getPostcode();
        $self->companyName = $formData->getCompanyName();
        $self->phone = $formData->getPhone();
        $self->insuranceReturnMonth = $formData->getInsuranceReturnMonth();

        return $self;
    }

    /**
     * @param Customer $customer
     * @param JourneyProduct $product
     * @param null|string $companyName
     * @return CustomerDetails
     */
    public static function fromProduct(Customer $customer, JourneyProduct $product, $companyName = NULL)
    {
        $self = new self();
        $self->customer = $customer;
        $self->product = $product;
        $self->productId = $product->getId();
        $self->lastName = $customer->getLastName();
        $self->firstName = $customer->getFirstName();
        $self->email = $customer->getEmail();
        $self->postcode = $customer->getPostcode();
        $self->companyName = $companyName;
        $self->phone = $customer->getPhone();

        return $self;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->customerDetailsId;
    }

    /**
     * @return int
     */
    public function getCustomerDetailsId()
    {
        return $this->customerDetailsId;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @param string $postcode
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    }

    /**
     * @param string $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     * @param string $insuranceReturnMonth
     */
    public function setInsuranceReturnMonth($insuranceReturnMonth)
    {
        $this->insuranceReturnMonth = $insuranceReturnMonth;
    }

    /**
     * @return JourneyProduct
     */
    public function getProduct()
    {
        return $this->product ? $this->product : $this->product = new JourneyProduct($this->productId);
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        $names = [];
        if ($this->firstName) {
            $names[] = $this->firstName;
        }
        if ($this->lastName) {
            $names[] = $this->lastName;
        }
        return implode(' ', $names);
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }


    /**
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @return string
     */
    public function getInsuranceReturnMonth()
    {
        return $this->insuranceReturnMonth;
    }

    /**
     * @return DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @return DateTime
     */
    public function getDtm()
    {
        return $this->dtm;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }
}
