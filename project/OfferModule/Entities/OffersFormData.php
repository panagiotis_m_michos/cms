<?php

namespace OfferModule\Entities;

use Doctrine\ORM\Mapping as Orm;
use Entities\Customer;
use Gedmo\Mapping\Annotation as Gedmo;
use Nette\Object;
use OfferModule\Forms\OffersForm;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Utils\Date;

class OffersFormData extends Object
{
    /**
     * @var array
     */
    private $offers = [];

    /**
     * @var array
     */
    private $loans = [];

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $postcode;

    /**
     * @var string
     */
    private $companyName;

    /**
     * @var bool
     */
    private $hasInsurance = FALSE;

    /**
     * @var Date
     */
    private $insuranceReturnMonth;

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @param Customer $customer
     * @param null|string $companyName
     */
    public function __construct(Customer $customer, $companyName = NULL)
    {
        $this->customer = $customer;
        $this->lastName = $customer->getLastName();
        $this->firstName = $customer->getFirstName();
        $this->email = $customer->getEmail();
        $this->postcode = $customer->getPostcode();
        $this->companyName = $companyName;
        $this->phone = $customer->getPhone();
    }

    /**
     * @param ExecutionContextInterface $context
     */
    public function validateSelections(ExecutionContextInterface $context)
    {
        $offersCount = count($this->getOffers());
        $loansCount = count($this->getLoans());

        if ($loansCount > 1) {
            $context->buildViolation('Sorry, you can only select one loan offer')->addViolation();
        }

        if (($offersCount + $loansCount) < 1) {
            $context->buildViolation('Please select at least one offer, or skip this step using the link below.')->addViolation();
        }
    }

    /**
     * @return array
     */
    public function getOffers()
    {
        return $this->offers;
    }

    /**
     * @param array $offers
     */
    public function setOffers($offers)
    {
        $this->offers = $offers;
    }

    /**
     * @return array
     */
    public function getLoans()
    {
        return $this->loans;
    }

    /**
     * @param array $loans
     */
    public function setLoans($loans)
    {
        $this->loans = $loans;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @param string $postcode
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    }

    /**
     * @param string $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     * @param string $insuranceReturnMonth
     */
    public function setInsuranceReturnMonth($insuranceReturnMonth)
    {
        $this->insuranceReturnMonth = $insuranceReturnMonth;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @return string
     */
    public function getInsuranceReturnMonth()
    {
        return $this->insuranceReturnMonth;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @return boolean
     */
    public function hasTaxAssist()
    {
        return in_array(OffersForm::OFFER_TAX_ASSIST, $this->offers);
    }

    /**
     * @return boolean
     */
    public function hasDigitalRisks()
    {
        return in_array(OffersForm::OFFER_DIGITAL_RISKS, $this->offers);
    }

    /**
     * @return boolean
     */
    public function hasWorldpay()
    {
        return in_array(OffersForm::OFFER_WORLDPAY, $this->offers);
    }

    /**
     * @return boolean
     */
    public function hasBusinessCentre()
    {
        return in_array(OffersForm::LOAN_BUSINESS_CENTRE, $this->loans);
    }

    /**
     * @return boolean
     */
    public function hasVirginStartUp()
    {
        return in_array(OffersForm::LOAN_VIRGIN_START_UP, $this->loans);
    }

    /**
     * @return boolean
     */
    public function hasInsurance()
    {
        return $this->hasInsurance;
    }

    /**
     * @param boolean $hasInsurance
     */
    public function setHasInsurance($hasInsurance)
    {
        $this->hasInsurance = $hasInsurance;
    }
}
