{capture "digitalRisks"}
    {assign 'popupContent' $view->getDigitalRisksPopupContent()}
    <div class="offer digitalrisks mbottom20 posrelative">
        <h3 class="blue4 massivefont">Flexible business insurance for startups</h3>
        <p>Protect your new business with the experts in insurance for growing startups and small businesses.</p>
        <div class="posabsolute width380 bottom5">
            <a data-toggle="modal" data-target="#digital-risks-modal" href="#">More Info</a>
                    <span class="offer-checkbox-container fright">
                        <label class="interested-label valign-top"></label>
                        {$formHelper->widget($form['offers'][0], ['attr' => ['data-needs-insurance' => 'data-needs-insurance']]) nofilter}
                    </span>
        </div>
        <div class="modal fade" id="digital-risks-modal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">{$popupContent->title}</h4>
                    </div>
                    <div class="modal-body">
                        {$popupContent->text nofilter}
                    </div>
                </div>
            </div>
        </div>
    </div>
{/capture}

{capture "taxAssist"}
    {assign 'popupContent' $view->getTaxAssistPopupContent()}
    <div class="offer taxassist mbottom20 posrelative">
        <h3 class="blue4 massivefont">Free accountancy consultation</h3>
        <p>Get expert advice from a TaxAssist small business consultant, local to you.</p>
        <div class="posabsolute width380 bottom5">
            <a data-toggle="modal" data-target="#taxassist-modal" href="#">More Info</a>
                <span class="offer-checkbox-container fright">
                    <label class="interested-label valign-top"></label>
                    {$formHelper->widget($form['offers'][1]) nofilter}
                </span>
        </div>

        <div class="modal fade" id="taxassist-modal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">{$popupContent->title}</h4>
                    </div>
                    <div class="modal-body">
                        {$popupContent->text nofilter}
                    </div>
                </div>
            </div>
        </div>
    </div>
{/capture}

{capture "worldpay"}
    {assign 'popupContent' $view->getWorldpayPopupContent()}
    <div class="offer worldpay mbottom20 posrelative">
        <h3 class="blue4 massivefont">Take card payments with Worldpay</h3>
        <p>Get help from Worldpay, taking payments in person, online or over the phone.</p>
        <div class="posabsolute width380 bottom5">
            <a data-toggle="modal" data-target="#worldpay-modal" href="#">More Info</a>
                <span class="offer-checkbox-container fright">
                    <label class="interested-label valign-top"></label>
                    {$formHelper->widget($form['offers'][2]) nofilter}
                </span>
        </div>

        <div class="modal fade" id="worldpay-modal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">{$popupContent->title}</h4>
                    </div>
                    <div class="modal-body">
                        {$popupContent->text nofilter}
                    </div>
                </div>
            </div>
        </div>
    </div>
{/capture}

{capture "businessCentre"}
    {assign 'popupContent' $view->getBusinessCentrePopupContent()}
    <div class="offer loan business-centre mbottom20 posrelative">
        <h3 class="blue4 massivefont">Business advice and up to £25,000 for your busines</h3>
        <p>Exclusive fast track application as a Made Simple customer. <b>London based businesses only.</b></p>
        <div class="posabsolute width380 bottom5">
            <a data-toggle="modal" data-target="#business-centre-modal" href="#">More Info</a>
                <span class="offer-checkbox-container fright">
                    <label class="interested-label valign-top"></label>
                    {$formHelper->widget($form['loans'][0]) nofilter}
                </span>
        </div>

        <div class="modal fade" id="business-centre-modal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">{$popupContent->title}</h4>
                    </div>
                    <div class="modal-body largefont">
                        {$popupContent->text nofilter}
                    </div>
                </div>
            </div>
        </div>
    </div>
{/capture}

{capture "virginStartUp"}
    {assign 'popupContent' $view->getVirginStartUpPopupContent()}
    <div class="offer loan virgin-start-up mbottom20 posrelative">
        <h3 class="blue4 massivefont">Advice, a loan and a mentor to get your business going</h3>
        <p>A Start-Up Loan of up to &pound;25,000 per partner in the business. <b>No location restrictions.</b></p>
        <div class="posabsolute width380 bottom5">
            <a data-toggle="modal" data-target="#virgin-start-up-modal" href="#">More Info</a>
                <span class="offer-checkbox-container fright">
                    <label class="interested-label valign-top"></label>
                    {$formHelper->widget($form['loans'][1]) nofilter}
                </span>
        </div>

        <div class="modal fade" id="virgin-start-up-modal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">{$popupContent->title}</h4>
                    </div>
                    <div class="modal-body largefont">
                        {$popupContent->text nofilter}
                    </div>
                </div>
            </div>
        </div>
    </div>
{/capture}

{include file="@header.tpl"}

<div class="padding20 f-sterling">
    <div id="incorporation-notice" class="row bg-grey2 padding20" style="margin:0;">
        <div class="col-md-1 txtcenter bigfont">
            <i class="fa fa-clock-o fa-5x grey14"></i>
        </div>
        <div class="col-md-10">
            <h3 class="blue4 massivefont">Thank you for completing your company registration details</h3>
            <p class="largefont txtbold">Your application has now been submitted to Companies House and your new company should be incorporated within 3 hours.</p>
            <p class="midfont">However, in rare cases it can take longer. Once we have a response from Companies House, we will notify you via email.</p>
        </div>
    </div>
    {if !$view->canShowOffers()}
        <span class="txtcenter padding20 displayblock">
            <a href="{$this->router->link("DashboardCustomerControler::DASHBOARD_PAGE")}" class="orange-submit btn">Go to my Dashboard</a>
        </span>
    {else}
        <div class="mtop30">
            <h1>What’s next?</h1>
            <p class="hugefont">
                Now that your company is almost formed, take a look at some of the support and services we've arranged for you via our trusted partners below.
            </p>
            <p class="hugefont blue4">
                Simply select the services you’re interested in and click ‘Submit’, we’ll then contact you with information on how to get your business off to a great start.
            </p>
        </div>

        <div id="exclusive-offers" class="mtop30">
            {$formHelper->setTheme($form, 'cms.html.twig')}
            {$formHelper->start($form) nofilter}
            <div class="fleft" style="width: 590px;">
                {foreach $view->getOffersOrder() as $offer}
                    {$smarty.capture.$offer nofilter}
                {/foreach}

                <h3>StartUp Loans <span class="grey2">(Please select only one)</span></h3>
                {foreach $view->getLoansOrder() as $loan}
                    {$smarty.capture.$loan nofilter}
                {/foreach}
            </div>
            <div class="fright" style="width: 320px;">
                <div class="bg-grey2 padding10">
                    {$formHelper->errors($form) nofilter}
                    <h3 class="blue4">Please check the information below is correct</h3>
                    {$formHelper->row($form['firstName']) nofilter}
                    {$formHelper->row($form['lastName']) nofilter}
                    {$formHelper->row($form['phone']) nofilter}
                    {$formHelper->row($form['email']) nofilter}
                    {$formHelper->row($form['hasInsurance']) nofilter}
                    {$formHelper->row($form['insuranceReturnMonth']) nofilter}
                    <span class="txtcenter padding5 displayblock">{$formHelper->row($form['save']) nofilter}</span>
                </div>
                <div class="txtright ptop10">
                    <a href="{$this->router->link("DashboardCustomerControler::DASHBOARD_PAGE")}">Skip this step and continue to dashboard&nbsp;<i class="fa fa-angle-right"></i></a>
                </div>
            </div>

            {$formHelper->end($form) nofilter}
            <div class="clear"></div>

            <script>
                function resolveLoans($selectedLoan) {
                    if ($selectedLoan.is(':checked')) {
                        $selectedLoan.closest('form').find('.loan :checkbox:checked').not($selectedLoan).prop('checked', false);

                        resolveLabels();
                    }
                }

                function resolveLabels() {
                    $('.offer input').each(function () {
                        var $this = $(this);
                        var $form = $this.closest('form');
                        var label = $this.closest('.offer-checkbox-container').find('.interested-label');

                        if ($this.is(':checked')) {
                            label.text($form.data('label-ticked'));
                            label.removeClass('blue4');
                            label.addClass('orange');
                        } else {
                            label.text($form.data('label-unticked'));
                            label.removeClass('orange');
                            label.addClass('blue4');
                        }
                    });
                }

                function resolveDisplayInsurance() {
                    var needInsuranceDetails = $('.offer input[data-needs-insurance="data-needs-insurance"]').is(':checked');
                    var checkboxContainer = $('.has-insurance').closest('.form-group');
                    var checkbox = checkboxContainer.find('input[type="checkbox"]');
                    var checkboxLabel = checkboxContainer.find('label:first-child');

                    if (needInsuranceDetails) {
                        checkboxLabel.find('span').remove();
                        checkboxContainer.show();
                    } else {
                        checkboxLabel.find('span').remove();
                        checkboxContainer.hide();
                    }

                    var needMonthSelect = checkbox.is(':checked');
                    var selectContainer = $('.insurance-month-select').closest('.form-group');
                    var select = selectContainer.find('select');
                    var selectLabel = selectContainer.find('label:first-child');

                    if (needMonthSelect && needInsuranceDetails) {
                        select.attr('required', 'required');
                        selectLabel.find('span').remove();
                        selectLabel.append($('<span class="red">*</span>'));
                        selectContainer.show();
                    } else {
                        select.removeAttr('required');
                        selectLabel.find('span').remove();
                        selectContainer.hide();
                    }
                }

                $('body')
                    .ready(function () {
                        resolveLabels();
                        resolveDisplayInsurance();
                    })
                    .on('change', '.loan input', function () {
                        resolveLoans($(this));
                    })
                    .on('change', '.offer input', function () {
                        resolveLabels();
                    })
                    .on('change', '.offer input[data-needs-insurance="data-needs-insurance"], .has-insurance', function () {
                        resolveDisplayInsurance();
                    })
                    .on('click', '.offer label.interested-label', function() {
                        $(this).closest('.offer').find('input').click();
                    });
            </script>
        </div>
    {/if}
</div>

{include file="@footer.tpl"}
