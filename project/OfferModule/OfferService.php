<?php

namespace OfferModule;

use DibiDriverException;
use Doctrine\ORM\Internal\Hydration\IterableResult;
use JourneyProduct;
use OfferModule\Entities\CustomerDetails;
use OfferModule\Repositories\OfferRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Utils\Date;

class OfferService
{
    /**
     * @var OfferRepository
     */
    private $repository;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @param OfferRepository $repository
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(OfferRepository $repository, EventDispatcherInterface $eventDispatcher)
    {
        $this->repository = $repository;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param CustomerDetails $customerDetails
     */
    public function saveNewCustomer(CustomerDetails $customerDetails)
    {
        $this->repository->saveEntity($customerDetails);
        $this->eventDispatcher->dispatch(OfferEvent::CUSTOMER_DETAILS_ADDED, new OfferEvent($customerDetails));
        $this->eventDispatcher->dispatch(OfferEvent::PRODUCT_ADDED . $customerDetails->getProductId(), new OfferEvent($customerDetails));
    }

    /**
     * @param JourneyProduct $product
     * @param Date $date
     * @return IterableResult
     */
    public function getCustomerDetailsForDate(JourneyProduct $product, Date $date)
    {
        return $this->repository->getCustomerDetailsForDate($product, $date);
    }

    /**
     * @return JourneyProduct[]
     */
    public function getAvailableProducts()
    {
        return [
            new JourneyProduct(CustomerDetails::PRODUCT_TAX_ASSIST),
            new JourneyProduct(CustomerDetails::PRODUCT_SIMPLY_BUSINESS),
            new JourneyProduct(CustomerDetails::PRODUCT_SMARTA),
            new JourneyProduct(CustomerDetails::PRODUCT_WORLDPAY),
            new JourneyProduct(CustomerDetails::PRODUCT_BUSINESS_CENTRE),
            new JourneyProduct(CustomerDetails::PRODUCT_VIRGIN_START_UP),
            new JourneyProduct(CustomerDetails::PRODUCT_DIGITAL_RISKS),
        ];
    }

    /**
     * @param int $id
     * @return JourneyProduct|NULL
     */
    public function getJourneyProduct($id)
    {
        $product = new JourneyProduct($id);

        return in_array($product, $this->getAvailableProducts()) ? $product : NULL;
    }
}
