<?php

namespace OfferModule\Views;

use Entities\Customer;
use OfferModule\Deciders\OffersPositionDecider;
use OfferModule\Entities\CustomerDetails;
use OfferModule\OfferService;
use Services\CompanyService;
use Services\PackageService;

class JourneyOfferControllerView
{
    /**
     * @var OffersPositionDecider
     */
    private $offersPositionDecider;

    /**
     * @var OfferService
     */
    private $offerService;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var PackageService
     */
    private $packageService;

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var string|null
     */
    private $companyName;

    /**
     * @param OffersPositionDecider $offersPositionDecider
     * @param OfferService $offerService
     * @param CompanyService $companyService
     * @param PackageService $packageService
     * @param Customer $customer
     * @param string|null $companyName
     */
    public function __construct(
        OffersPositionDecider $offersPositionDecider,
        OfferService $offerService,
        CompanyService $companyService,
        PackageService $packageService,
        Customer $customer,
        $companyName = NULL
    )
    {
        $this->offersPositionDecider = $offersPositionDecider;
        $this->offerService = $offerService;
        $this->companyService = $companyService;
        $this->packageService = $packageService;
        $this->customer = $customer;
        $this->companyName = $companyName;
    }

    /**
     * @return bool
     */
    public function canShowOffers()
    {
        return !$this->customer->isWholesale() && !$this->isInternationalPackage();
    }

    /**
     * @return string|null
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @return array
     */
    public function getOffersOrder()
    {
        return $this->offersPositionDecider->getOffersPosition();
    }

    /**
     * @return array
     */
    public function getLoansOrder()
    {
        return $this->offersPositionDecider->getLoansPosition();
    }

    /**
     * @return string
     */
    public function getTaxAssistPopupContent()
    {
        return $this->offerService->getJourneyProduct(CustomerDetails::PRODUCT_TAX_ASSIST)->page;
    }

    /**
     * @return string
     */
    public function getDigitalRisksPopupContent()
    {
        return $this->offerService->getJourneyProduct(CustomerDetails::PRODUCT_DIGITAL_RISKS)->page;
    }

    /**
     * @return string
     */
    public function getWorldpayPopupContent()
    {
        return $this->offerService->getJourneyProduct(CustomerDetails::PRODUCT_WORLDPAY)->page;
    }

    /**
     * @return string
     */
    public function getBusinessCentrePopupContent()
    {
        return $this->offerService->getJourneyProduct(CustomerDetails::PRODUCT_BUSINESS_CENTRE)->page;
    }

    /**
     * @return string
     */
    public function getVirginStartUpPopupContent()
    {
        return $this->offerService->getJourneyProduct(CustomerDetails::PRODUCT_VIRGIN_START_UP)->page;
    }

    /**
     * @return bool
     */
    private function isInternationalPackage()
    {
        $isInternational = FALSE;
        $company = $this->companyService->getCompanyByCompanyName($this->companyName);
        if ($company) {
            $package = $this->packageService->getPackageById($company->getProductId());
            $isInternational = $package && $package->isInternational();
        }

        return $isInternational;
    }
}
