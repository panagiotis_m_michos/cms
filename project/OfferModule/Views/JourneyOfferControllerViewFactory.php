<?php

namespace OfferModule\Views;

use Entities\Customer;
use OfferModule\Deciders\OffersPositionDecider;
use OfferModule\OfferService;
use Services\CompanyService;
use Services\PackageService;

class JourneyOfferControllerViewFactory
{
    /**
     * @var OffersPositionDecider
     */
    private $offersPositionDecider;

    /**
     * @var OfferService
     */
    private $offerService;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var PackageService
     */
    private $packageService;

    /**
     * @param OffersPositionDecider $offersPositionDecider
     * @param OfferService $offerService
     * @param CompanyService $companyService
     * @param PackageService $packageService
     */
    public function __construct(
        OffersPositionDecider $offersPositionDecider,
        OfferService $offerService,
        CompanyService $companyService,
        PackageService $packageService
    )
    {
        $this->offersPositionDecider = $offersPositionDecider;
        $this->offerService = $offerService;
        $this->companyService = $companyService;
        $this->packageService = $packageService;
    }

    /**
     * @param Customer $customer
     * @param string|null $companyName
     * @return JourneyOfferControllerView
     */
    public function create(Customer $customer, $companyName = NULL)
    {
        return new JourneyOfferControllerView(
            $this->offersPositionDecider,
            $this->offerService,
            $this->companyService,
            $this->packageService,
            $customer,
            $companyName
        );
    }
}
