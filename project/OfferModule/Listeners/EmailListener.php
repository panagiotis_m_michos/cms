<?php

namespace OfferModule\Listeners;

use JourneyEmailer;
use OfferModule\OfferEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EmailListener implements EventSubscriberInterface
{
    /**
     * @var JourneyEmailer
     */
    private $emailer;

    public static function getSubscribedEvents()
    {
        return [
            OfferEvent::CUSTOMER_DETAILS_ADDED => 'handleOffer'
        ];
    }

    /**
     * @param JourneyEmailer $emailer
     */
    public function __construct(JourneyEmailer $emailer)
    {
        $this->emailer = $emailer;
    }

    /**
     * @param OfferEvent $event
     */
    public function handleOffer(OfferEvent $event)
    {
        if ($event->canSendEmailInstantly()) {
            $this->emailer->sendEmailToThirdParties($event->getCustomerDetails());
        }
        if ($event->canSendEmailToCustomer()) {
            $this->emailer->sendEmailToCustomer($event->getCustomerDetails());
        }
    }
}