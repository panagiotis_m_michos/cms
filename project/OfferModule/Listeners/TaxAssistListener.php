<?php

namespace OfferModule\Listeners;

use OfferModule\Entities\CustomerDetails;
use OfferModule\OfferEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use TaxAssistLeadModel;

class TaxAssistListener implements EventSubscriberInterface
{
    /**
     * @var TaxAssistLeadModel
     */
    private $taxAssistLeadModel;

    public static function getSubscribedEvents()
    {
        return [
            OfferEvent::PRODUCT_ADDED . CustomerDetails::PRODUCT_TAX_ASSIST => 'handleOffer'
        ];
    }

    /**
     * @param TaxAssistLeadModel $taxAssistLeadModel
     */
    public function __construct(TaxAssistLeadModel $taxAssistLeadModel)
    {
        $this->taxAssistLeadModel = $taxAssistLeadModel;
    }

    /**
     * @param OfferEvent $event
     */
    public function handleOffer(OfferEvent $event)
    {
        $this->taxAssistLeadModel->saveFromCustomerDetails($event->getCustomerDetails());
    }

}
