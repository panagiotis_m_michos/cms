<?php

namespace OfferModule\Tests;

use EntityHelper;
use OfferModule\Entities\CustomerDetails;
use OfferModule\Repositories\OfferRepository;
use PHPUnit_Framework_TestCase;
use Tests\Helpers\ObjectHelper;
use Tests\ProductHelper;
use Utils\Date;

class OfferRepositoryTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var OfferRepository
     */
    private $object;

    /**
     * @var array
     */
    private $customerDetails;

    public function setUp()
    {
        EntityHelper::emptyTables([TBL_CUSTOMERS, TBL_CUSTOMER_DETAILS]);
        $this->object = EntityHelper::getService('offer_module.offer_repository');
        $customer = ObjectHelper::createCustomer();
        $this->product1 = ProductHelper::createJourney(CustomerDetails::PRODUCT_SIMPLY_BUSINESS);
        $this->product2 = ProductHelper::createJourney(CustomerDetails::PRODUCT_TAX_ASSIST);
        $customerDetails[] = CustomerDetails::fromProduct($customer, $this->product1);
        $customerDetails[] = CustomerDetails::fromProduct($customer, $this->product2);
        $customerDetails[] = CustomerDetails::fromProduct($customer, $this->product1);
        EntityHelper::save(array_merge([$customer], $customerDetails));
        $this->customerDetails = $customerDetails;
    }

    /**
     * @covers OfferModule\Repositories\OfferRepository::getCustomerDetailsForDate
     */
    public function testGetCustomerDetailsForDate()
    {
        $iterator1 = $this->object->getCustomerDetailsForDate($this->product1, new Date('today'));
        $iterator2 = $this->object->getCustomerDetailsForDate($this->product2, new Date('today'));
        $result1 = EntityHelper::iteratorToArray($iterator1);
        $result2 = EntityHelper::iteratorToArray($iterator2);
        $this->assertEquals($this->customerDetails[0]->getId(), $result1[0]->getId());
        $this->assertEquals($this->customerDetails[2]->getId(), $result1[1]->getId());
        $this->assertEquals($this->customerDetails[1]->getId(), $result2[0]->getId());
    }

    /**
     * @covers OfferModule\Repositories\OfferRepository::getCustomerDetailsForDate
     */
    public function testGetCustomerDetailsForWrongDate()
    {
        $iterator1 = $this->object->getCustomerDetailsForDate($this->product1, new Date('-1 day'));
        $result1 = EntityHelper::iteratorToArray($iterator1);
        $this->assertEmpty($result1);
    }

}
