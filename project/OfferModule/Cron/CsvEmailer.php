<?php

namespace OfferModule\Cron;

use Array2Csv;
use CommonModule\Mappers\IMapper;
use Cron\Commands\CommandAbstract;
use Cron\INotifier;
use ILogger;
use JourneyEmailer;
use JourneyProduct;
use OfferModule\Entities\CustomerDetails;
use OfferModule\Mappers\CustomerDetailsMapper;
use OfferModule\Mappers\InsuranceMapper;
use OfferModule\OfferService;
use Utils\Date;

class CsvEmailer extends CommandAbstract
{
    /**
     * @var string
     */
    protected $timeType = self::TIME_TYPE_FIXED;

    /**
     * @var JourneyEmailer
     */
    private $emailer;

    /**
     * @var OfferService
     */
    private $offerService;

    /**
     * @param ILogger $logger
     * @param INotifier $notifier
     * @param OfferService $offerService
     * @param JourneyEmailer $emailer
     */
    public function __construct(ILogger $logger, INotifier $notifier, OfferService $offerService, JourneyEmailer $emailer)
    {
        parent::__construct($logger, $notifier);
        $this->emailer = $emailer;
        $this->offerService = $offerService;
    }

    /**
     * @return int
     */
    public function execute()
    {
        $emailsSent = 0;
        $forDate = $this->runDate ? new Date($this->runDate->format('Y-m-d')): new Date('-1 day');
        foreach ($this->offerService->getAvailableProducts() as $product) {
            if ($product->sendCsvDaily) {
                $this->process($forDate, $product);
                $emailsSent++;
            }
        }
        $this->notifier->triggerSuccess($this->getName(), $forDate->getTimestamp(), sprintf('Sent %d emails for date %s', $emailsSent, $forDate->format('d/m/Y')));
        return $emailsSent;
    }

    /**
     * @param Date $forDate
     * @param JourneyProduct $product
     * @return bool
     */
    public function process(Date $forDate, JourneyProduct $product)
    {
        $iterator = $this->offerService->getCustomerDetailsForDate($product, $forDate);
        $csv = Array2Csv::getContentFromDoctrineIterator($iterator, $this->getMapper($product));
        if ($csv) {
            $this->emailer->midnightCsvEmail($product, $csv);
        }
    }

    /**
     * @param JourneyProduct $product
     * @return IMapper
     */
    private function getMapper(JourneyProduct $product)
    {
        $insuranceProducts = [CustomerDetails::PRODUCT_SIMPLY_BUSINESS, CustomerDetails::PRODUCT_DIGITAL_RISKS];

        if (in_array($product->getId(), $insuranceProducts)) {
            return new InsuranceMapper();
        } else {
            return new CustomerDetailsMapper();
        }
    }
}
