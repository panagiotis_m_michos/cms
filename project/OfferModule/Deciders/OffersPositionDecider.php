<?php

namespace OfferModule\Deciders;

use Doctrine\Common\Cache\CacheProvider;

class OffersPositionDecider
{
    /**
     * @var CacheProvider
     */
    private $cache;

    /**
     * @var array
     */
    private $defaultOffersOrder;

    /**
     * @var array
     */
    private $defaultLoansOrder;

    /**
     * @param array $defaultOffersOrder
     * @param array $defaultLoansOrder
     * @param CacheProvider $cache
     */
    public function __construct(array $defaultOffersOrder, array $defaultLoansOrder, CacheProvider $cache)
    {
        $this->defaultOffersOrder = $defaultOffersOrder;
        $this->defaultLoansOrder = $defaultLoansOrder;
        $this->cache = $cache;
    }

    /**
     * @return array
     */
    public function getDefaultOffersOrder()
    {
        return $this->defaultOffersOrder;
    }

    /**
     * @return array
     */
    public function getDefaultLoansOrder()
    {
        return $this->defaultLoansOrder;
    }

    /**
     * @return array
     */
    public function getOffersPosition()
    {
        return $this->getPosition('offersOrder', $this->defaultOffersOrder);
    }

    /**
     * @return array
     */
    public function getLoansPosition()
    {
        return $this->getPosition('loansOrder', $this->defaultLoansOrder);
    }

    /**
     * @param string $type
     * @param array $defaultOrder
     * @return array
     */
    private function getPosition($type, array $defaultOrder)
    {
        $order = $this->cache->contains($type) ?
            $this->cache->fetch($type) :
            $defaultOrder;

        array_unshift($order, array_pop($order));

        if (count($order) != count($defaultOrder)|| count(array_intersect($order, $defaultOrder)) != count($defaultOrder)) {
            $order = $defaultOrder;
        }

        $this->cache->save($type, $order);

        return $order;
    }
}
