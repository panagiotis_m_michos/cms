<?php

namespace OfferModule\Factories;

use JourneyProduct;
use OfferModule\Entities\CustomerDetails;
use OfferModule\Entities\OffersFormData;

class CustomerDetailsFactory
{
    /**
     * @param OffersFormData $formData
     * @return CustomerDetails[]
     */
    public function buildFromFormData(OffersFormData $formData)
    {
        $productIds = [
            CustomerDetails::PRODUCT_DIGITAL_RISKS => $formData->hasDigitalRisks(),
            CustomerDetails::PRODUCT_WORLDPAY => $formData->hasWorldpay(),
            CustomerDetails::PRODUCT_TAX_ASSIST => $formData->hasTaxAssist(),
            CustomerDetails::PRODUCT_BUSINESS_CENTRE => $formData->hasBusinessCentre(),
            CustomerDetails::PRODUCT_VIRGIN_START_UP => $formData->hasVirginStartUp(),
        ];

        $productIds = array_keys(array_filter($productIds));

        $customerDetails = [];
        foreach ($productIds as $productId) {
            $customerDetails[] = CustomerDetails::fromFormAndProduct($formData, new JourneyProduct($productId));
        }

        return $customerDetails;
    }
}
