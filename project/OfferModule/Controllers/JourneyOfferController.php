<?php

namespace OfferModule\Controlers;

use DashboardCustomerControler;
use DibiDriverException;
use Entities\Customer;
use Exception;
use FTemplate;
use OfferModule\Entities\OffersFormData;
use OfferModule\Factories\CustomerDetailsFactory;
use OfferModule\Forms\OffersForm;
use OfferModule\OfferService;
use OfferModule\Views\JourneyOfferControllerViewFactory;
use Psr\Log\LoggerInterface;
use Services\ControllerHelper;
use Symfony\Component\HttpFoundation\RedirectResponse;

class JourneyOfferController
{
    /**
     * @var FTemplate
     */
    private $template;

    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    /**
     * @var OfferService
     */
    private $offerService;

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var CustomerDetailsFactory
     */
    private $customerDetailsFactory;

    /**
     * @var JourneyOfferControllerViewFactory
     */
    private $controllerViewFactory;

    /**
     * @param FTemplate $template
     * @param ControllerHelper $controllerHelper
     * @param OfferService $offerService
     * @param Customer $customer
     * @param LoggerInterface $logger
     * @param CustomerDetailsFactory $customerDetailsFactory
     * @param JourneyOfferControllerViewFactory $controllerViewFactory
     */
    public function __construct(
        FTemplate $template,
        ControllerHelper $controllerHelper,
        OfferService $offerService,
        Customer $customer,
        LoggerInterface $logger,
        CustomerDetailsFactory $customerDetailsFactory,
        JourneyOfferControllerViewFactory $controllerViewFactory
    )
    {
        $this->template = $template;
        $this->controllerHelper = $controllerHelper;
        $this->offerService = $offerService;
        $this->customer = $customer;
        $this->logger = $logger;
        $this->customerDetailsFactory = $customerDetailsFactory;
        $this->controllerViewFactory = $controllerViewFactory;
    }

    /**
     * @param string $companyName
     * @return RedirectResponse
     */
    public function showOffers($companyName = NULL)
    {
        //todo: check if customer went through this step already and redirect

        $offersFormData = new OffersFormData($this->customer, $companyName);
        $form = $this->controllerHelper->buildForm(new OffersForm, $offersFormData);

        $this->template->form = $form->createView();
        $this->template->view = $this->controllerViewFactory->create($this->customer, $companyName);

        if ($form->isValid()) {
            $customersDetails = $this->customerDetailsFactory->buildFromFormData($offersFormData);

            $errors = [];
            foreach ($customersDetails as $customerDetails) {
                try {
                    $this->offerService->saveNewCustomer($customerDetails);
                } catch (DibiDriverException $e) {
                    //tax assist can throw dibi execption
                    $this->logger->error($e);
                    $errors[] = 'There was a problem submitting your details to TaxAssist for your Free Accountancy Consultation. Please call us on  020 3217 0999 if you wish for us to arrange this for you.';
                } catch (Exception $e) {
                    $this->logger->error($e);
                    $errors[] = 'There was a problem with activating ' . $customerDetails->getProduct()->getLngTitle() . '. Please call us on  020 3217 0999 if you wish for us to arrange this for you.';
                }
            }

            if ($errors) {
                $this->controllerHelper->setFlashMessage(implode("\n", $errors));
            } elseif ($customersDetails) {
                $this->controllerHelper->setFlashMessage("Thank you for showing your interest in your selected offers, you will be contacted shortly.");
            }

            return new RedirectResponse($this->controllerHelper->getLink(DashboardCustomerControler::DASHBOARD_PAGE));
        }
    }
}
