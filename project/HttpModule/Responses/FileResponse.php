<?php

namespace HttpModule\Responses;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class FileResponse extends Response
{
    /**
     * @param string $content
     * @param string $fileName
     * @param int $status
     * @param array $headers
     */
    public function __construct($content, $fileName, $status = 200, $headers = array())
    {
        parent::__construct($content, $status, $headers);
        $d = $this->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $fileName
        );
        $this->headers->set('Content-Disposition', $d);
        $this->headers->set('Content-Type', 'application/octet-stream');
    }
}
