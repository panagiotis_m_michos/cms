<?php

namespace HttpModule\Responses;

use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;

class CsvStreamedResponse extends StreamedResponse
{
    /**
     * @param string $filename
     * @param callable $callback
     */
    public function __construct($filename, callable $callback)
    {
        parent::__construct();

        $this->setCallback($callback);

        $headers = $this->headers;
        $headers->set('Content-Type', 'text/csv');
        $headers->set(
            'Content-Disposition',
            $headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $filename)
        );
    }
}
