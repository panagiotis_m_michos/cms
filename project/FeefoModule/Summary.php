<?php

namespace FeefoModule;


class Summary
{
    /**
     * @var string
     */
    private $reviewCount;

    /**
     * @var string
     */
    private $averageRating;

    /**
     * @param string $reviewCount
     * @param string $averageRating
     */
    public function __construct($reviewCount, $averageRating)
    {
        $this->reviewCount = $reviewCount;
        $this->averageRating = $averageRating;
    }

    /**
     * @return string
     */
    public function getReviewCount()
    {
        return $this->reviewCount;
    }

    /**
     * @return string
     */
    public function getAverageRating()
    {
        return $this->averageRating;
    }
}
