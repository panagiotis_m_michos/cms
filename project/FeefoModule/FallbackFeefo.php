<?php

namespace FeefoModule;


use Doctrine\Common\Cache\Cache;
use Exception;

class FallbackFeefo implements IFeefo
{
    /**
     * @var IFeefo
     */
    private $feefo;

    /**
     * @var Cache
     */
    private $cache;

    /**
     * @param IFeefo $feefo
     * @param Cache $cache
     */
    public function __construct(IFeefo $feefo, Cache $cache)
    {
        $this->feefo = $feefo;
        $this->cache = $cache;
    }

    /**
     * @return Summary
     */
    public function getSummary()
    {
        try {
            $summary = $this->feefo->getSummary();
            $this->cache->save('feefo.summary.fallback', $summary);

            return $summary;
        } catch (Exception $e) {
            return $this->cache->contains('feefo.summary.fallback')
                ? $this->cache->fetch('feefo.summary.fallback')
                : new Summary(1997, 98);
        }
    }
}
