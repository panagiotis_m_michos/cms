<?php

namespace FeefoModule;


use Doctrine\Common\Cache\Cache;
use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;

class FeefoFactory
{
    /**
     * @param $merchantId
     * @param Client $client
     * @param LoggerInterface $logger
     * @param Cache $cache
     * @param int $lifetime
     * @return IFeefo
     */
    public static function create(
        $merchantId,
        Client $client,
        LoggerInterface $logger,
        Cache $cache,
        $lifetime = 1800
    )
    {
        $feefo = new Feefo($client, $merchantId);
        $cached = new CachedFeefo($feefo, $cache, $lifetime);
        $logged = new LoggedFeefo($cached, $logger);

        return new FallbackFeefo($logged, $cache);
    }
}
