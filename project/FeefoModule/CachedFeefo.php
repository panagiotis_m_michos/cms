<?php

namespace FeefoModule;


use Doctrine\Common\Cache\Cache;

class CachedFeefo implements IFeefo
{
    /**
     * @var IFeefo
     */
    private $feefo;

    /**
     * @var Cache
     */
    private $cache;

    /**
     * @var int
     */
    private $lifetime;

    /**
     * @param IFeefo $feefo
     * @param Cache $cache
     * @param int $lifetime
     */
    public function __construct(IFeefo $feefo, Cache $cache, $lifetime = 1800)
    {
        $this->feefo = $feefo;
        $this->cache = $cache;
        $this->lifetime = $lifetime;
    }

    /**
     * @return Summary
     */
    public function getSummary()
    {
        $key = 'feefo.summary';

        if (!$this->cache->contains($key)) {
            $summary = $this->feefo->getSummary();
            $this->cache->save($key, $summary, $this->lifetime);
        }

        return $this->cache->fetch($key);

    }
}
