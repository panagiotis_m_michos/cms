<?php
namespace FeefoModule;

interface IFeefo
{
    /**
     * @return Summary
     */
    public function getSummary();
}
