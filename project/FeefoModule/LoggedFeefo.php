<?php

namespace FeefoModule;


use Exception;
use Psr\Log\LoggerInterface;

class LoggedFeefo implements IFeefo
{
    /**
     * @var IFeefo
     */
    private $feefo;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param IFeefo $feefo
     * @param LoggerInterface $logger
     */
    public function __construct(IFeefo $feefo, LoggerInterface $logger)
    {
        $this->feefo = $feefo;
        $this->logger = $logger;
    }

    /**
     * @return Summary
     */
    public function getSummary()
    {
        try {
            return $this->feefo->getSummary();
        } catch (Exception $e) {
            $this->logger->error($e);
            throw $e;
        }
    }
}
