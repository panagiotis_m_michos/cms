<?php

namespace FeefoModule;

use GuzzleHttp\Client;

class Feefo implements IFeefo
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $merchantId;

    /**
     * @param Client $client
     * @param string $merchantId
     */
    public function __construct(Client $client, $merchantId)
    {
        $this->client = $client;
        $this->merchantId = $merchantId;
    }

    /**
     * @return Summary
     */
    public function getSummary()
    {
        $url = 'http://cdn2.feefo.com/api/xmlfeedback?merchantidentifier=' . $this->merchantId;
        $summaryData = $this->client->get($url)->xml()->SUMMARY;

        return new Summary(
            (int) $summaryData->TOTALRESPONSES,
            (int) $summaryData->AVERAGE
        );
    }
}
