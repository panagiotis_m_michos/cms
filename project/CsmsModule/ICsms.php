<?php
namespace CsmsModule;

interface ICsms
{
    /**
     * @return IncorporationsStats
     */
    public function getIncorporationStats();
}
