<?php

namespace CsmsModule;

use DateTime;
use GuzzleHttp\Client;

class Csms implements ICsms
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $apikey;

    /**
     * @param Client $client
     * @param string $apikey
     */
    public function __construct(Client $client, $apikey)
    {
        $this->client = $client;
        $this->apikey = $apikey;
    }

    /**
     * @return IncorporationsStats
     */
    public function getIncorporationStats()
    {
        $url = 'https://www.companysearchesmadesimple.com/webservices/v2/staticStats/incorporatedStartUpBritain/?apiKey=' . $this->apikey;
        $data = $this->client->get($url)->json()['results'];

        return new IncorporationsStats(
            $data['daily'],
            DateTime::createFromFormat('d/m/Y H:i:s', $data['dailyDate'] . ' 00:00:00'),
            $data['thisMonthToNow'],
            $data['thisYearToNow']
        );
    }
}
