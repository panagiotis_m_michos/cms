<?php

namespace CsmsModule;

use Exception;
use Psr\Log\LoggerInterface;

class LoggedCsms implements ICsms
{
    /**
     * @var ICsms
     */
    private $csms;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param ICsms $csms
     * @param LoggerInterface $logger
     */
    public function __construct(ICsms $csms, LoggerInterface $logger)
    {
        $this->csms = $csms;
        $this->logger = $logger;
    }

    /**
     * @return IncorporationsStats
     * @throws Exception
     */
    public function getIncorporationStats()
    {
        try {
            return $this->csms->getIncorporationStats();
        } catch (Exception $e) {
            $this->logger->error($e);
            throw $e;
        }
    }
}
