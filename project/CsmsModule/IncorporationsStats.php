<?php

namespace CsmsModule;


use DateTime;

class IncorporationsStats
{
    /**
     * @var int
     */
    private $daily;

    /**
     * @var DateTime
     */
    private $dailyMeasuredDate;

    /**
     * @var int
     */
    private $thisMonthToNow;

    /**
     * @var int
     */
    private $thisYearToNow;

    /**
     * @param int $daily
     * @param DateTime $dailyMeasuredDate
     * @param int $thisMonthToNow
     * @param int $thisYearToNow
     */
    public function __construct(
        $daily,
        $dailyMeasuredDate,
        $thisMonthToNow,
        $thisYearToNow
    )
    {
        $this->daily = $daily;
        $this->dailyMeasuredDate = $dailyMeasuredDate;
        $this->thisMonthToNow = $thisMonthToNow;
        $this->thisYearToNow = $thisYearToNow;
    }

    /**
     * @return int
     */
    public function getDailyIncorporationCount()
    {
        return $this->daily;
    }

    /**
     * @return DateTime
     */
    public function getDailyMeasuredDate()
    {
        return $this->dailyMeasuredDate;
    }

    /**
     * @return int
     */
    public function getIncorporationCountThisMonthToNow()
    {
        return $this->thisMonthToNow;
    }

    /**
     * @return int
     */
    public function getIncorporationCountThisYearToNow()
    {
        return $this->thisYearToNow;
    }
}
