<?php

namespace CsmsModule;

use Doctrine\Common\Cache\Cache;
use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;

class CsmsFactory
{
    public static function create(
        $apiKey,
        Client $client,
        LoggerInterface $logger,
        Cache $cache,
        $lifetime = 1800
    )
    {
        $csms = new Csms($client, $apiKey);
        $cached = new CachedCsms($csms, $cache, $lifetime);
        $logged = new LoggedCsms($cached, $logger);

        return new FallbackCsms($logged, $cache);
    }
}
