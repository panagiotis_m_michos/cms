<?php

namespace CsmsModule;

use DateTime;
use Doctrine\Common\Cache\Cache;
use Exception;

class FallbackCsms implements ICsms
{
    /**
     * @var ICsms
     */
    private $csms;

    /**
     * @var Cache
     */
    private $cache;

    /**
     * @param ICsms $csms
     * @param Cache $cache
     */
    public function __construct(ICsms $csms, Cache $cache)
    {
        $this->csms = $csms;
        $this->cache = $cache;
    }

    /**
     * @return IncorporationsStats
     */
    public function getIncorporationStats()
    {
        $key = 'csms.incorporation_stats.fallback';

        try {
            $stats = $this->csms->getIncorporationStats();
            $this->cache->save($key, $stats);

            return $stats;
        } catch (Exception $e) {
            return $this->cache->contains($key)
                ? $this->cache->fetch($key)
                : new IncorporationsStats(
                    2680,
                    new Datetime(),
                    50399,
                    98603
                );
        }
    }
}
