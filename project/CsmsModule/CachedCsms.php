<?php

namespace CsmsModule;

use Doctrine\Common\Cache\Cache;

class CachedCsms implements ICsms
{
    /**
     * @var ICsms
     */
    private $csms;

    /**
     * @var Cache
     */
    private $cache;

    /**
     * @var int
     */
    private $lifetime;

    /**
     * @param ICsms $csms
     * @param Cache $cache
     * @param int $lifetime
     */
    public function __construct(ICsms $csms, Cache $cache, $lifetime = 3600)
    {
        $this->csms = $csms;
        $this->cache = $cache;
        $this->lifetime = $lifetime;
    }

    /**
     * @return IncorporationsStats
     */
    public function getIncorporationStats()
    {
        $key = 'csms.incorporation_stats';

        if (!$this->cache->contains($key)) {
            $stats = $this->csms->getIncorporationStats();
            $this->cache->save($key, $stats, $this->lifetime);
        }

        return $this->cache->fetch($key);
    }
}
