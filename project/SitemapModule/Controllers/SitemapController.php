<?php

namespace SitemapModule\Controllers;

use Doctrine\Common\Cache\Cache;
use FSitemap;
use Services\ControllerHelper;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\Router;
use TemplateModule\Renderers\IRenderer;

class SitemapController
{
    const NODE_MENU = 100;
    const NODE_PAGES = 266;

    /**
     * @var IRenderer
     */
    private $renderer;

    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var Cache
     */
    private $cache;

    /**
     * @param IRenderer $renderer
     * @param ControllerHelper $controllerHelper
     * @param Router $router
     * @param Cache $cache
     */
    public function __construct(
        IRenderer $renderer,
        ControllerHelper $controllerHelper,
        Router $router,
        Cache $cache
    )
    {
        $this->renderer = $renderer;
        $this->controllerHelper = $controllerHelper;
        $this->router = $router;
        $this->cache = $cache;
    }

    public function render()
    {
        $this->renderer->render(['sitemap' => $this->getSitemapDefinition()]);
    }

    /**
     * @return Response
     */
    public function renderXml()
    {
        $xmlData = $this->getXmlData($this->getSitemapDefinition());

        return $this->renderer->render(['xmlData' => $xmlData], new Response('', Response::HTTP_OK, ['Content-Type' => 'text/xml']));
    }

    /**
     * @return array
     */
    private function getSitemapDefinition()
    {
        $newRoutes = $this->router->getRouteCollection()->all();
        $normalizedNewRoutes = $this->normalizeNewRoutes($newRoutes);

        $key = 'normalized-old-routes';
        if ($this->cache->contains($key)) {
            $normalizedOldRoutes = $this->cache->fetch($key);
        } else {
            $oldRoutes = [FSitemap::get(self::NODE_MENU), FSitemap::get(self::NODE_PAGES)];
            $normalizedOldRoutes = $this->normalizeOldRoutes($oldRoutes);
            $this->cache->save($key, $normalizedOldRoutes, 3600);
        }

        return array_merge($normalizedNewRoutes, $normalizedOldRoutes);
    }

    /**
     * @param Route[] $routes
     * @return array
     */
    private function normalizeNewRoutes(array $routes)
    {
        $normalized = [];

        foreach ($routes as $name => $route) {
            $compiledRoute = $route->compile();
            $methods = $route->getMethods();
            $isGet = empty($methods) || $methods == ['GET'];
            $pathVariables = $compiledRoute->getPathVariables();

            if (!(empty($pathVariables))
                || $route->getOption('excludeFromSitemap') === TRUE
                || !$isGet
            ) {
                continue;
            }

            $normalized[] = [
                'title' => ucwords(str_replace('_', ' ', $name)),
                'url' => $compiledRoute->getStaticPrefix(),
                'children' => [],
            ];
        }

        return $normalized;
    }

    /**
     * @param array $routes
     * @return array
     */
    private function normalizeOldRoutes(array $routes)
    {
        $normalized = [];

        foreach ($routes as $route) {
            $normalized[] = [
                'title' => $route['title'],
                'url' => $route['url'],
                'children' => $this->normalizeOldRoutes($route['childs']),
            ];
        }

        return $normalized;
    }

    /**
     * @param array $routes
     * @return array
     */
    private function getXmlData(array $routes)
    {
        $flattened = [];

        foreach ($routes as $route) {
            $url = "https://{$_SERVER['HTTP_HOST']}{$route['url']}";

            $flattened[$url] = [
                'title' => $route['title'],
                'url' => $url,
                'priority' => ($route['url'] == '/' ? 1 : 0.85)

            ];

            if (!empty($route['children'])) {
                $flattened = array_merge($flattened, $this->getXmlData($route['children']));
            }
        }

        return $flattened;
    }
}
