<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
    {foreach $xmlData as $data}
        <url>
            <loc>{$data['url']}</loc>
            <changefreq>daily</changefreq>
            <priority>{$data['priority']}</priority>
        </url>
    {/foreach}
</urlset>
