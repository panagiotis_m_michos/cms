{extends '@structure/layout.tpl'}

{block content}
    {function name=printSitemap}
        <ul>
            {foreach $items as $item}
                <li>
                    {if $item['url'] == NULL}
                        <strong>{$item['title']}</strong>
                    {else}
                        <a href="{$item['url']}" title="{$item['title']}">{$item['title']}</a>
                    {/if}

                    {if $item['children']}
                        {call name=printSitemap items=$item['children']}
                    {/if}
                </li>
            {/foreach}
        </ul>
    {/function}

    <div class="container bg-white padding20">
        <h1>Sitemap</h1>
        {call name=printSitemap items=$sitemap}
    </div>
{/block}
