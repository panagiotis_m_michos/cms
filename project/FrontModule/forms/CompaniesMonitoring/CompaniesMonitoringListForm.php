<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS
 * @author 		Nikolai Senkevich, Stan Bazik
 * @internal 	project/forms/CompaniesMonitoring/CompaniesMonitoringListForm.php
 * @created 	21/03/2011
 */
class CompaniesMonitoringListForm extends FForm
{

    /**
     * @var CompaniesMonitoringControler
     */
    private $controler;
    /**
     * @var array
     */
    private $callback;

    /**
     * @param CompaniesMonitoringControler $controler
     * @param array $callback
     */
    public function startup(CompaniesMonitoringControler $controler, array $callback)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->init();
    }

    private function init()
    {
        $this->addFieldset('Return Filter');
        $this->add('RadioInline', 'selector', 'Type')
            ->setOptions(array('Accounts' => 'Accounts', 'Return' => 'Return'));

        $this->add('DatePicker', 'dateFrom', 'From')
            ->addRule(FForm::Required, 'Please provide date!.')
            ->setOption('yearRange', '-111:+2');

        $this->add('DatePicker', 'dateTo', 'To')
            ->addRule(FForm::Required, 'Please provide date!.')
            ->setOption('yearRange', '-111:+2');

        $this->addselect('sorter', 'Sort by:', array('1' => 'Accounts Due', '2' => 'Returns Due'))
            ->style('width: 100px;')
            ->setFirstOption('--- Sort by ---');

        $this->addSubmit('companyFilter', 'Filter')->class('btn')->style('width:60px');
        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

}