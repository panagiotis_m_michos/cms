<?php
/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS
 * @author 		Nikolai Senkevich, Stan Bazik
 * @internal 	project/forms/CompaniesMonitoring/CompaniesMonitoringAddForm.php
 * @created 	21/03/2011
 */
class CompaniesMonitoringAddForm extends FForm{

    /**
     * @var CompaniesMonitoringControler
     */
    private $controler;

    /**
     * @var array
     */
    private $callback;

    /**
     * @param CompaniesMonitoringControler $controler
     * @param array $callback
     */
    public function startup(CompaniesMonitoringControler $controler, array $callback)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->init();
    }

    private function init()
    {
        $this->addFieldset('Data');
        $this->addText('companyNumber', 'Company Number: *')
            ->style('width: 100px;')
                ->addRule(FForm::Required, 'Required!')
                ->addRule(FForm::REGEXP, 'Company number must always have 8 characters. If it has 7 please add a leading zero.', '#^\w{8,8}$#');
        $this->addSubmit('submit', 'Add Company')
                ->style('width: 100px; height: 25px;');
        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();

            // check if company already in our database
            $customerId = Customer::getSignedIn()->customerId;
            $companyMonitoredId = dibi::select('companyMonitoredId')
                    ->from(TBL_COMPANIES_MONITORING)
                    ->where('customerId=%i', $customerId)
                    ->and('companyNumber=%s', $data['companyNumber'])->execute()->fetch();
            if (!empty($companyMonitoredId)) {
                throw new Exception('Company is already being monitored.');
            }
            $companyData = CompanyMonitoringService::getCompanyData($data['companyNumber']);

            $company = new CompanyMonitoring();
            $company->customerId = Customer::getSignedIn()->customerId;
            $company->companyName = $companyData['companyName'];
            $company->companyNumber = $companyData['companyNumber'];
            $company->companyStatus = $companyData['companyStatus'];
            $company->incorporationDate = $companyData['incorporationDate'];
            (isset($companyData['accountsNextDueDate'])) ? $company->accountsNextDueDate = $companyData['accountsNextDueDate'] : $company->accountsNextDueDate = NULL;
            (isset($companyData['returnsNextDueDate'])) ? $company->returnsNextDueDate = $companyData['returnsNextDueDate'] : $company->returnsNextDueDate = NULL;
            // save
            

            $company->save();
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }
}