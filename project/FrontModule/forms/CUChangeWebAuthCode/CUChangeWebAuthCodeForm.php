<?php

use Entities\Company;

class CUChangeWebAuthCodeForm extends FForm
{
    /**
     * @var CUChangeWebAuthCodeControler
     */
    private $delegate;

    /**
     * @var Company
     */
    private $company;

    /**
     * @param CUChangeWebAuthCodeControler $delegate
     * @param Company $company
     */
    public function startup(CUChangeWebAuthCodeControler $delegate, Company $company)
    {
        $this->delegate = $delegate;
        $this->company = $company;
        $this->init();
    }

    private function init()
    {
        $this->addFieldset('Company Details');

        $this->addText('authenticationCode', 'Authentication Code: ')
            ->addRule(FForm::Required, 'Please provide company auth code')
            ->addRule('MinLength', 'Authentication code has to consist of 6 characters!', 6)
            ->setValue($this->company->getAuthenticationCode());

        $this->addFieldset('Action');
        $this->addSubmit('update', 'Update')->class('orange-submit');

        $this->onValid = array($this, 'process');
        $this->start();
    }

    public function process()
    {
        $data = $this->getValues();
        $this->clean();
        $this->delegate->authCodeFormSuccess($data['authenticationCode']);
    }

}
