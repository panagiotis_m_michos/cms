<?php

namespace Front\CUChangeWebAuthCode;

interface ICUChangeWebAuthCodeForm
{
    /**
     * @param string $authCode
     */
    public function authCodeFormSuccess($authCode);
}
