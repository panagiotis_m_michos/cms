<?php

class ContactForm extends FForm
{
    /**
     * @param array $callback
     */
    public function startup(array $callback)
    {
        $this->buildForm();
        $this->onValid = $callback;
        $this->start();
    }

    private function buildForm()
    {
        $this->addText('name', 'Name:');
        $this->addText('email', 'Email:*')
            ->addRule(self::Required, 'Please provide email!')
            ->addRule(self::Email, 'Incorrect email format');
        $this->addText('phone', 'Telephone:');
        $this->addText('companyName', 'Company:');
        $this->addArea('message', 'Message:*')
            ->rows(5)->cols(30)
            ->addRule(self::Required, 'Message is required!');
        $this->add('Captcha', 'captcha', NULL)
            ->addRule('ReqCaptcha', array('Required!', 'Incorrect!'));
        $this->addSubmit('send', 'Send')
            ->class('btn_submit');
    }

}