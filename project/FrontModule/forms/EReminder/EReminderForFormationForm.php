<?php

/**
 * @package    CMS
 * @author     Nikolai Senkevich
 */

class EReminderForFormationForm extends FForm
{

    /**
     * @var CFERemindersControler 
     */
    private $controler;
    /**
     * @var array
     */
    private $callback;

    /**
     * @var Company
     */
    private $company;
    
    /**
     * @param CFERemindersControler $controler
     * @param array $callback
     */
    public function startup(CFERemindersControler $controler, array $callback, $company)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->company = $company;
        $this->init();
    }

    private function init()
    {
        $this->addFieldset('Set eReminder');
        $this->addRadio('ereminder', 'eReminder', array(1=>'Yes',0=>'No'));
        // action
        $this->addFieldset('Action');
        $this->addSubmit('submit', 'Save')
            ->style('width: 120px; height: 22px;');

        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();
            $this->company->setEreminderId($data['ereminder']);
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }
}