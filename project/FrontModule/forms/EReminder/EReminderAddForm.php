<?php

/**
 * @package    CMS
 * @author     Nikolai Senkevich
 */

class EReminderAddForm extends FForm
{

    /**
     * @var CUEReminderControler 
     */
    private $controler;
    /**
     * @var array
     */
    private $callback;

    /**
     * @var Company
     */
    private $company;
    
    /**
     * @param CUEReminderControler $controler
     * @param array $callback
     */
    public function startup(CUEReminderControler $controler, array $callback, $company)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->company = $company;
        $this->init();
    }

    private function init()
    {

        $this->addFieldset('Add an email address');
        $this->addText('email', 'Email address: *')
            ->style('width: 200px; height: 22px;')
            ->addRule(FForm::Required, 'Required!')
            ->addRule(FForm::Email, 'Email is not valid!');
        // action
        $this->addFieldset('Action');
        $this->addSubmit('submit', 'Add Email')
            ->style('width: 100px; height: 22px;');

        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {

        //get existing ones
        $reminder = GetEReminders::getNewReminder($this->company);
        $reminderEmails = $reminder->sendRequest();
        $xml = simplexml_load_string($reminderEmails);
        $this->checkXml($xml);
        foreach ($xml->Body->EReminders->Recipient as $value) {
            //if ($value->Activated == 'Y') {
                $emails[] = (string) $value->EmailAddress;
            //}
        }

        // added by customer
        $data = $this->getValues();
        $emails[] = $data['email'];

        //seting all together
        $reminder = SetEReminders::getNewReminder($emails, $this->company);

        $xml = simplexml_load_string($reminder->sendRequest());
        $this->checkXml($xml);         
    }
    
    public function checkXml($xml){
         if (isset($xml->GovTalkDetails->GovTalkErrors)) {
                if (isset($xml->GovTalkDetails->GovTalkErrors->Error->Text)) {
                    throw new Exception($xml->GovTalkDetails->GovTalkErrors->Error->Text);
                } else {
                    throw new Exception('Error');
                }
            }
    }

}