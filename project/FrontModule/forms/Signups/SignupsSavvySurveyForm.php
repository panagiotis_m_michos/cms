<?php
/**
 * CMS
 * @category   Project
 * @package    CMS
 * @author     Nikolai Senkevich
 * @version    SignupsSavvySurveyForm.php 2012-03-14 
 */

class SignupsSavvySurveyForm  extends FForm
{
    /**
     * @var SignupsControler 
     */
    private $controler;
    /**
     * @var array
     */
    private $callback;

    /**
     * @param SignupsControler $controler
     * @param array $callback
     */
    public function startup(SignupsControler $controler, array $callback)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->init();
    }

    /**
	 * @return void
	 */
    private function init()
    {
        $this->addFieldset('Submit Feedback');
        $this->addFieldset('Download');
        $this->addText('name', 'Name:')
            ->addRule(FForm::Required, 'Please fill field ');
        $this->addText('email', 'Email:')
            ->addRule(FForm::Required, 'Please fill field ');
        $this->addSubmit('submit', 'Submit')->style('width: 155px; height: 35px;');
        $this->onValid = $this->callback;
	    $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();
            $signups = new Signup();
            $signups->name = $data['name'];
            $signups->email = $data['email'];
            $signups->typeId = Signup::TYPE_SAVVYSURVEY;
            $signups = $signups->save();
            $this->clean();

            $nameSpace = FApplication::$session->getNamespace('whitePaperPdf');
            $nameSpace->outputSavvySurveyPdf = TRUE;
        } catch (Exception $e) {
            throw $e;
        }
    }
}
