<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS
 * @author 		Nikolai Senkevich, Stan Bazik
 * @internal 	project/forms/AccountantsInstitute/AccountantsInstituteContactForm.php
 * @created 	28/03/2011
 */
class AccountantsInstituteContactForm extends FForm
{

    /**
     * @var AccountantsInstituteControler
     */
    private $controler;
    /**
     * @var array
     */
    private $callback;

        /**
     * @var AccountantsInstituteEmailer
     */
    protected $accountantsInstituteEmailer;
    
    /**
     * @param AccountantsInstituteControler $controler
     * @param array $callback
     */
    public function startup(AccountantsInstituteControler $controler, array $callback, AccountantsInstituteEmailer $accountantsInstituteEmailer)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->accountantsInstituteEmailer = $accountantsInstituteEmailer;
        $this->init();
    }

    private function init()
    {
        $this->addFieldset('Contact Form');
        $this->addText('customer_name', 'Name:');
        $this->addText('customer_email', 'Email:*')->addRule(FForm::Required, 'Please provide email!')
            ->addRule(FForm::Email, 'Incorrect email format');
        $this->addText('contact_phone', 'Telephone:');
        $this->addText('company_name', 'Company:');
        $this->addArea('message', 'Message:*')->rows(5)->cols(30)->addRule(FForm::Required, 'Message is required!');

        $this->addFieldset('Action');
        $this->addSubmit('send', 'Send')->style('width: 200px; height: 30px;');

        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();
            $this->accountantsInstituteEmailer->contactUsEmail($data);
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

}