<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS
 * @author 		Nikolai Senkevich, Stan Bazik
 * @internal 	project/forms/AccountantsInstitute/AccountantsInstituteLoginForm.php
 * @created 	28/03/2011
 */
class AccountantsInstituteLoginForm extends FForm
{

    /**
     * @var AccountantsInstituteControler
     */
    private $controler;
    
    /**
     * @var array
     */
    private $callback;
    
    /**
     * @param AccountantsInstituteControler $controler
     * @param array $callback
     */
    public function startup(AccountantsInstituteControler $controler,array $callback)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->init();
    }

    private function init()
    {
        //$this->setAction(FApplication::$router->link(LoginControler::LOGIN_PAGE));
        $this->addFieldset('Login Details');
        $this->addText('email', 'Email address:')
             ->addRule(FForm::Required, 'Please provide e-mail address!');
        $this->addPassword('password', 'Password:')
            ->addRule(FForm::Required, 'Please provide password!')
            ->addRule('LoginCredentials',NULL, NULL, $this);

        $this->addFieldset('Action');
        $this->addSubmit('login', 'Login')
            ->style('width: 200px; height: 30px;')->onClick("javascript:pageTracker._trackPageview('/login/cosec/icaew')");

        $this->onValid = $this->callback;
        $this->start();
    }
    
    public function process(){
        try {
            $data = $this->getValues();
            $customer = Customer::doAuthenticate($data['email'], $data['password']);
            CustomerActionLogger::log($customer, CustomerActionLogger::LOG_IN);
            Customer::signIn($customer->getId());
            $this->clean();
         } catch (Exception $e) {
            throw $e;
        }
    }
}