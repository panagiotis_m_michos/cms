<?php

/**
 * @package     Companies Made Simple
 * @subpackage     CMS
 * @author         Nikolai Senkevich, Stan Bazik
 * @internal     project/forms/AccountantsInstitute/AccountantsInstituteSignupForm.php
 * @created     28/03/2011
 */
class AccountantsInstituteSignupForm extends FForm
{

    /**
     * @var AccountantsInstituteControler
     */
    private $controler;
    /**
     * @var array
     */
    private $callback;

    /**
     * @var AccountantsInstituteEmailer
     */
    protected $accountantsInstituteEmailer;
    
    /**
     * @param AccountantsInstituteControler $controler
     * @param array $callback
     */
    public function startup(AccountantsInstituteControler $controler, array $callback, AccountantsInstituteEmailer $accountantsInstituteEmailer)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->accountantsInstituteEmailer = $accountantsInstituteEmailer;
        $this->init();
    }

    private function init()
    {
        // login details
        $this->addFieldset('Login details');
        $this->addText('email', 'Email *')
            ->class('field220')
            ->addRule(FForm::Required, 'Please provide e-mail address!')
            ->addRule(FForm::Email, 'Email is not valid!')
            ->addRule(array($this, 'Validator_uniqueLogin'), 'Email address has been taken.');
        $this->addPassword('password', 'Password *')
            ->class('field220');
        $this->addPassword('passwordConf', 'Confirm Password *')
            ->class('field220')
            ->addRule(FForm::Equal, 'Passwords are not the same!', 'password');

        // ICAEW Id
        //        $this->addFieldset('ICAEW details');
        //        $this->addText('icaewId', 'ICAEW Id *')
        //            ->class('field220')
        //            ->addRule(FForm::Required, 'Please provide ICAEW Id!');

        // personal details
        $this->addFieldset('Personal details');
        $this->addSelect('titleId', 'Title', Customer::$titles)
            ->class('field220')
            ->setFirstOption('--- Select ---');
        $this->addText('firstName', 'First Name *')
            ->class('field220')
            ->addRule(FForm::Required, 'Please provide firstname!');
        $this->addText('lastName', 'Last Name  *')
            ->class('field220')
            ->addRule(FForm::Required, 'Please provide lastname!');

        // address details
        $this->addFieldset('Address details');
        $this->addText('address1', 'Address 1  *')
            ->class('field220')
            ->addRule(FForm::Required, 'Please provide address 1!');
        $this->addText('address2', 'Address 2 &nbsp;&nbsp;')
            ->class('field220');
        $this->addText('address3', 'Address 3 &nbsp;&nbsp;')
            ->class('field220');
        $this->addText('city', 'City  *')
            ->class('field220')
            ->addRule(FForm::Required, 'Please provide city!');
        $this->addText('county', 'County &nbsp;&nbsp;')
            ->class('field220');
        $this->addText('postcode', 'Postcode  *')
            ->class('field220')
            ->addRule(FForm::Required, 'Please provide post code!')
            ->addRule(array($this, 'Validator_postcode'), 'Postcode is not valid')
            ->setDescription('If UK postcode please add 1 space in the middle');
        $this->addSelect('countryId', 'Country  *', Customer::$countries)
            ->class('field220')
            ->setFirstOption('--- Select ---')
            ->addRule(FForm::Required, 'Please provide country!');
        $this->addText('phone', 'Phone *')
            ->class('field220')
            ->setDescription('(Only numbers, no spaces, max 14 characters)')
            ->addRule(FForm::Required, 'Please provide phone!')
            ->addRule(FForm::REGEXP, 'Phone is not valid.', '#^\d{1,14}$#');
        $this->addText('additionalPhone', 'Mobile / Other Phone')
            ->class('field220')
            ->setDescription('(Only numbers, no spaces, max 14 characters)')
            ->addRule(FForm::REGEXP, 'Phone is not valid.', '#^\d{1,14}$#');

        // action
        $this->addFieldset('Action');
        $this->addSubmit('login', 'Register')
            ->setDescription("By registering you agree to the <a href=\"[page 770]\">Terms and Conditions</a>")
            ->style('width: 200px; height: 30px;');

        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();

            // save customer
            $customer = new Customer();
            $customer->roleId = Customer::ROLE_WHOLESALE;
            $customer->tagId = Customer::TAG_ICAEW;
            $customer->icaewId = NULL;
            $customer->email = $data['email'];
            $customer->password = $data['password'];
            $customer->titleId = $data['titleId'];
            $customer->firstName = $data['firstName'];
            $customer->lastName = $data['lastName'];
            $customer->address1 = $data['address1'];
            $customer->address2 = $data['address2'];
            $customer->address3 = $data['address3'];
            $customer->city = $data['city'];
            $customer->county = $data['county'];
            $customer->postcode = $data['postcode'];
            $customer->countryId = $data['countryId'];
            $customer->phone = $data['phone'];
            $customer->additionalPhone = $data['additionalPhone'];
            $customerId = $customer->save();

            $this->accountantsInstituteEmailer->signupEmail($data['password'], $customer);
            // sign user
            Customer::signIn($customerId);

            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Validate postcode for UK
     *
     * @param Text $control
     * @param string $error
     * @param array $params
     * @return boolean
     */
    public static function Validator_postcode(Text $control, $error, $params)
    {
        $value = $control->getValue();
        $countryId = $control->owner['countryId']->getValue();
        if ($countryId == Customer::UK_CITIZEN && (!preg_match('#^(GIR 0AA|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]|[A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y]))|[0-9][A-HJKPS-UW]) [0-9][ABD-HJLNP-UW-Z]{2})$#i', $value) || !preg_match('#^([A-Z][\dA-Z]{1,3}[\s][\d][A-Z]{2,2})$#i', $value))) {
            return $error;
        }
        return TRUE;
    }

    /**
     * Validator
     *
     * @param object $control
     * @param mixed $error
     * @param mixed $params
     * @return mixed
     */
    public static function Validator_uniqueLogin($control, $error, $params)
    {
        $customerId = dibi::select('customerId')->from(TBL_CUSTOMERS)->where('email=%s', $control->getValue())->execute()->fetchSingle();
        if ($customerId) {
            return $error; 
        }
        return TRUE;
    }

}