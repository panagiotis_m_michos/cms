<?php

namespace TaxAssistAccountsConsultation;

use Exception;

interface ISoletraderFormDelegate
{
    public function soletraderFormSucceeded();

    public function soletraderFormFailed(Exception $e);
}
