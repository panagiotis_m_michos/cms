<?php

namespace TaxAssistAccountsConsultation;

use FForm;
use Exception;
use Services\TaxAssistLeadService;

class SoletraderForm extends FForm
{
    /**
     * @var ISoletraderFormDelegate
     */
    private $delegate;

    /**
     * @var TaxAssistLeadService
     */
    private $service;

    /**
     * @param ISoletraderFormDelegate $delegate
     * @param TaxAssistLeadService $service
     */
    public function startup(ISoletraderFormDelegate $delegate, TaxAssistLeadService $service)
    {
        $this->delegate = $delegate;
        $this->service = $service;

        $this->buildForm();
        $this->onValid = array($this, 'process');
        $this->start();
    }

    public function process()
    {
        try {
            $data = $this->getValues();
            $this->service->processSoletraderData($data);

            $this->clean();
            $this->delegate->soletraderFormSucceeded();

        } catch (Exception $e) {
            $this->delegate->soletraderFormFailed($e);
        }
    }

    private function buildForm()
    {
        $this->addText('email', 'Email: *')
            ->addRule(FForm::Required, 'Required!')
            ->addRule(FForm::Email, 'Not valid!')
            ->class('field220');
        $this->addText('fullName', 'Full name: *')
            ->addRule(FForm::Required, 'Required!')
            ->class('field220');
        $this->addText('phone', 'Contact number: *')
            ->addRule(FForm::Required, 'Required!')
            ->class('field220');
        $this->addText('postcode', 'Postcode: *')
            ->addRule(FForm::Required, 'Required!')
            ->class('field220');
        $this->addText('companyName', 'Business name:*')
            ->addRule(FForm::Required, 'Required!')
            ->class('field220');
        $this->addSubmit('send', 'Submit')
            ->class('btn_submit fleft mbottom mright');

    }

}
