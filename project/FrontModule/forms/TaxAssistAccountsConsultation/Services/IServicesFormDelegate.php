<?php

namespace TaxAssistAccountsConsultation;

use Exception;

interface IServicesFormDelegate
{
    public function servicesFormSucceeded();

    public function servicesFormFailed(Exception $e);
}
