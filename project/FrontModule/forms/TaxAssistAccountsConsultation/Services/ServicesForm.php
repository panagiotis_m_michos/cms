<?php

namespace TaxAssistAccountsConsultation;

use FForm;
use Services\TaxAssistLeadService;
use Exception;

class ServicesForm extends FForm
{
    /**
     * @var IServicesFormDelegate
     */
    private $delegate;

    /**
     * @var TaxAssistLeadService
     */
    private $service;

    /**
     * @param IServicesFormDelegate $delegate
     * @param TaxAssistLeadService $service
     */
    public function startup(IServicesFormDelegate $delegate, TaxAssistLeadService $service)
    {
        $this->delegate = $delegate;
        $this->service = $service;

        $this->buildForm();
        $this->onValid = array($this, 'process');
        $this->start();
    }

    public function process()
    {
        try {
            $data = $this->getValues();
            $this->service->processServiceData($data);

            $this->clean();
            $this->delegate->servicesFormSucceeded();

        } catch (Exception $e) {
            $this->delegate->servicesFormFailed($e);
        }
    }

    private function buildForm()
    {
        $this->addText('email', 'Email: *')
            ->addRule(FForm::Required, 'Required!')
            ->addRule(FForm::Email, 'Not valid!')
            ->class('field180');
        $this->addText('fullName', 'Full name: *')
            ->addRule(FForm::Required, 'Required!')
            ->class('field180');
        $this->addText('phone', 'Contact number: *')
            ->addRule(FForm::Required, 'Required!')
            ->class('field180');
        $this->addText('postcode', 'Postcode: *')
            ->addRule(FForm::Required, 'Required!')
            ->class('field180');
        $this->addText('companyName', 'Company name:*')
            ->addRule(FForm::Required, 'Required!')
            ->class('field180');
        $this->add('Captcha', 'captcha', NULL)
            ->addRule('ReqCaptcha', array('Required!', 'Incorrect!'));
        $this->addSubmit('send', 'Submit')
            ->class('btn_submit fleft mbottom mright');
    }

}