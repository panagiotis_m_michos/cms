<?php

namespace TaxAssistAccountsConsultation;

use FForm;
use Services\TaxAssistLeadService;
use Exception;

class AccountsForm extends FForm
{
    /**
     * @var IAccountFormDelegate
     */
    private $delegate;

    /**
     * @var TaxAssistLeadService
     */
    private $service;

    /**
     * @param IAccountFormDelegate $delegate
     * @param TaxAssistLeadService $service
     */
    public function startup(IAccountFormDelegate $delegate, TaxAssistLeadService $service)
    {
        $this->delegate = $delegate;
        $this->service = $service;

        $this->buildForm();
        $this->onValid = array($this, 'process');
        $this->start();
    }

    public function process()
    {
        try {
            $data = $this->getValues();
            $this->service->processAccountData($data);

            $this->clean();
            $this->delegate->accountFormSucceeded();

        } catch (Exception $e) {
            $this->delegate->accountFormFailed($e);
        }
    }

    private function buildForm()
    {
        $this->addText('email', 'Email: *')
            ->addRule(FForm::Required, 'Required!')
            ->addRule(FForm::Email, 'Not valid!')
            ->class('field220');
        $this->addText('fullName', 'Full name: *')
            ->addRule(FForm::Required, 'Required!')
            ->class('field220');
        $this->addText('phone', 'Contact number: *')
            ->addRule(FForm::Required, 'Required!')
            ->class('field220');
        $this->addText('postcode', 'Postcode: *')
            ->addRule(FForm::Required, 'Required!')
            ->class('field220');
        $this->addText('companyName', 'Company name: *')
            ->addRule(FForm::Required, 'Required!')
            ->class('field220');
        $this->addArea('details', 'Details: ')
            ->cols(40)
            ->rows(7);
        $this->addSubmit('send', 'Submit')
            ->class('btn_submit fleft mbottom mright');
    }

}