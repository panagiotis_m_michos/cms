<?php

namespace TaxAssistAccountsConsultation;

use Exception;

interface IAccountFormDelegate
{
    public function accountFormSucceeded();
    public function accountFormFailed(Exception $e);
}
