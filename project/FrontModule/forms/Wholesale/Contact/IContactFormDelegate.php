<?php

namespace Wholesale;

use Exception;

interface IContactFormDelegate
{
    public function wholeSaleContactFormSucceeded();

    /**
     * @param Exception $e
     */
    public function wholeSaleContactFormFailed(Exception $e);
}