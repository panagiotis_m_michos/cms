<?php

namespace Wholesale;

use Exception;
use FForm;

class ContactForm extends FForm
{
    /**
     * @var IContactFormDelegate
     */
    private $delegate;

    /**
     * @var IEmailer
     */
    private $emailer;

    /**
     * @param IContactFormDelegate $delegate
     * @param IEmailer $generalWholesaleEmailer
     */
    public function startup(IContactFormDelegate $delegate, IEmailer $generalWholesaleEmailer)
    {
        $this->delegate = $delegate;
        $this->emailer = $generalWholesaleEmailer;

        $this->buildForm();
        $this->onValid = array($this, 'process');
        $this->start();
    }

    public function process()
    {
        try {
            $this->emailer->sendContactUsEmail($this->getValues());
            $this->clean();
            $this->delegate->wholeSaleContactFormSucceeded();

        } catch (Exception $e) {
            $this->delegate->wholeSaleContactFormFailed($e);
        }
    }

    private function buildForm()
    {
        $this->addFieldset('Contact Form');
        $this->addText('customer_name', 'Name:');
        $this->addText('customer_email', 'Email:*')
            ->addRule(self::Required, 'Please provide email!')
            ->addRule(self::Email, 'Incorrect email format');
        $this->addText('contact_phone', 'Telephone:');
        $this->addText('company_name', 'Company:');
        $this->addArea('message', 'Message:*')
            ->rows(5)->cols(30)
            ->addRule(self::Required, 'Message is required!');

        $this->addFieldset('Action');
        $this->addSubmit('send', 'Send')
            ->style('width: 200px; height: 30px;');
    }

}