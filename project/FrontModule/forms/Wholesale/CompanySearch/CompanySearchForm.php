<?php

namespace Wholesale;

use FForm;

class CompanySearchForm extends FForm
{
    public function startup()
    {
        $this->buildForm();
        $this->start();
    }

    private function buildForm()
    {
        $this->setRenderClass('Default2Render');

        $this->addFieldset('Data');
        $this->addText('companyName', 'Company name:')
            ->addRule(array('CompanyNameValidator', 'isValid'), NULL);

        $this->addFieldset('Action');
        $this->addSubmit('search', 'Search')
            ->style('width: 200px; height: 30px;');
    }
}