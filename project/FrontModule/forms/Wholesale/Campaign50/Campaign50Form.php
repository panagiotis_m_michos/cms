<?php

namespace Wholesale;

use Exception;
use FForm;

class Campaign50Form extends FForm
{
    /**
     * @var Campaign50FormDelegate
     */
    private $delegate;

    /**
     * @param ISignUpFormDelegate $delegate
     */
    public function startup(
        ICampaign50FormDelegate $delegate
    )
    {
        $this->delegate = $delegate;
        $this->buildForm();
        $this->onValid = array($this, 'process');
        $this->start();
    }

    public function process()
    {
        try {
            $data = $this->getValues();
            
            $to = 'mathewa@madesimplegroup.com;grantb@madesimplegroup.com;panosm@madesimplegroup.com'; 
            $email_subject = "£50 Campaign CMS";
            $email_body = "£50 Campaign CMS - Form submission:" . "\n\n";
            $email_body.= "Name: " . $data['name'] . "\n";
            $email_body.= "Company Name: " . $data['companyName'] . "\n";
            $email_body.= "Email Address: " . $data['email']; 

            $headers = "From: noreply@companiesmadesimple.com\n"; 
            $headers .= "Reply-To: noreply@companiesmadesimple.com";

            mail($to, $email_subject, $email_body, $headers);
        
            $this->clean();
            $this->delegate->wholeSaleCampaign50FormSucceeded();

        } catch (Exception $e) {
            $this->delegate->wholeSaleCampaign50FormFailed($e);
        }
    }

    private function buildForm()
    {
        $this->addText('name', 'Name*')
            ->class('field240')
            ->addRule(self::Required, 'Required');
        $this->addText('companyName', 'Company Name*')
            ->class('field240')
            ->addRule(self::Required, 'Required');
        $this->addText('email', 'Email Address*')
            ->class('field240')
            ->placeholder('The address we have on your account')
            ->addRule(self::Required, 'Required')
            ->addRule(self::Email, 'Not valid');

        $this->addSubmit('submit', 'Submit')
            ->class('btn_submit fright');
    }

}