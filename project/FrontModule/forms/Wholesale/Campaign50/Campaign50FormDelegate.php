<?php

namespace Wholesale;

use Exception;

interface ICampaign50FormDelegate
{
    public function wholeSaleCampaign50FormSucceeded();

    /**
     * @param Exception $e
     */
    public function wholeSaleCampaign50FormFailed(Exception $e);
}