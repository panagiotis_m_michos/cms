<?php

namespace Wholesale;

use Exception;
use Package;

interface IPackagesFormDelegate
{
    /**
     * @param Package $package
     */
    public function wholeSalePackagesFormSucceeded(Package $package);

    /**
     * @param Exception $e
     */
    public function wholeSalePackagesFormFailed(Exception $e);
}