<?php

namespace Wholesale;

use Basket;
use Exception;
use FForm;
use InvalidArgumentException;
use Package;

class PackagesForm extends FForm
{
    /**
     * @var IPackagesFormDelegate
     */
    private $delegate;

    /**
     * @var Package[] array
     */
    private $packages = array();

    /**
     * @param IPackagesFormDelegate $delegate
     * @param array $packages
     */
    public function startup(IPackagesFormDelegate $delegate, array $packages)
    {
        $this->delegate = $delegate;
        $this->packages = $packages;

        $this->buildForm();
        $this->onValid = array($this, 'process');
        $this->start();
    }

    public function process()
    {
        try {
            foreach ($this->packages as $packageId => $package) {
                if ($this->isSubmitedBy("submit_$packageId")) {
                    $this->clean();
                    $this->delegate->wholeSalePackagesFormSucceeded($package);
                }
            }

            throw new InvalidArgumentException("Package not found");

        } catch (Exception $e) {
            $this->delegate->wholeSalePackagesFormFailed($e);
        }
    }


    private function buildForm()
    {
        foreach ($this->packages as $packageId => $package) {
            $this->addSubmit("submit_$packageId", 'BUY NOW');
        }
    }
}