<?php

namespace Wholesale;

use Exception;

interface ISignUpFormDelegate
{
    public function wholeSaleSignUpFormSucceeded();

    /**
     * @param Exception $e
     */
    public function wholeSaleSignUpFormFailed(Exception $e);
}