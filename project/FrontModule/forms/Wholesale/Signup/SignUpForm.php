<?php

namespace Wholesale;

use Customer;
use dibi;
use Dispatcher\Events\SignUpEvent;
use EventLocator;
use Exception;
use FForm;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Text;

class SignUpForm extends FForm
{
    /**
     * @var ISignUpFormDelegate
     */
    private $delegate;

    /**
     * @var IEmailer
     */
    private $emailer;

    /**
     * @var string
     */
    private $tag;

    /**
     * @var EventDispatcher
     */
    private $eventDispatcher;

    /**
     * @param ISignUpFormDelegate $delegate
     * @param IEmailer $generalWholesaleEmailer
     * @param $tag
     * @param EventDispatcher $eventDispatcher
     */
    public function startup(
        ISignUpFormDelegate $delegate,
        IEmailer $generalWholesaleEmailer,
        $tag,
        EventDispatcher $eventDispatcher
    )
    {
        $this->delegate = $delegate;
        $this->emailer = $generalWholesaleEmailer;
        $this->tag = $tag;
        $this->eventDispatcher = $eventDispatcher;

        $this->buildForm();
        $this->onValid = array($this, 'process');
        $this->start();
    }

    public function process()
    {
        try {
            $data = $this->getValues();

            // save customer
            $customer = new Customer();
            $customer->roleId = Customer::ROLE_WHOLESALE;
            $customer->tagId = $this->tag;
            $customer->icaewId = NULL;
            $customer->email = $data['email'];
            $customer->password = $data['password'];
            $customer->titleId = $data['titleId'];
            $customer->firstName = $data['firstName'];
            $customer->lastName = $data['lastName'];
            $customer->companyName = $data['companyName'];
            $customer->address1 = $data['address1'];
            $customer->address2 = $data['address2'];
            $customer->address3 = $data['address3'];
            $customer->city = $data['city'];
            $customer->county = $data['county'];
            $customer->postcode = $data['postcode'];
            $customer->countryId = $data['countryId'];
            $customer->phone = $data['phone'];
            $customer->additionalPhone = $data['additionalPhone'];
            $customer->save();

            $this->eventDispatcher->dispatch(
                EventLocator::CUSTOMER_SIGN_UP,
                new SignUpEvent($this->emailer, $customer, $data['password'])
            );

            $this->clean();
            $this->delegate->wholeSaleSignUpFormSucceeded();

        } catch (Exception $e) {
            $this->delegate->wholeSaleSignUpFormFailed($e);
        }
    }

    /**
     * @param Text $control
     * @param $error
     * @return bool
     */
    public static function Validator_postcode(Text $control, $error)
    {
        $value = $control->getValue();
        $countryId = $control->owner['countryId']->getValue();
        if ($countryId == Customer::UK_CITIZEN
            && (!preg_match('#^(GIR 0AA|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]|[A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y]))|[0-9][A-HJKPS-UW]) [0-9][ABD-HJLNP-UW-Z]{2})$#i', $value)
            || !preg_match('#^([A-Z][\dA-Z]{1,3}[\s][\d][A-Z]{2,2})$#i', $value))
        ) {
            return $error;
        }
        return TRUE;
    }

    /**
     * @param object $control
     * @param mixed $error
     * @return mixed
     */
    public static function Validator_uniqueLogin($control, $error)
    {
        $customerId = dibi::select('customerId')
            ->from(TBL_CUSTOMERS)
            ->where('email=%s', $control->getValue())
            ->fetchSingle();
        if ($customerId) {
            return $error;
        }
        return TRUE;
    }


    private function buildForm()
    {
        $this->addFieldset('Login details');
        $this->addText('email', 'Email *')
            ->class('field220')
            ->addRule(self::Required, 'Please provide e-mail address!')
            ->addRule(self::Email, 'Email is not valid!')
            ->addRule(array($this, 'Validator_uniqueLogin'), 'Email address has been taken.');
        $this->addPassword('password', 'Password *')
            ->addRule(self::Required, 'Please provide password!')
            ->class('field220');
        $this->addPassword('passwordConf', 'Confirm Password *')
            ->addRule(self::Required, 'Please provide password!')
            ->addRule(self::Equal, 'Passwords are not the same!', 'password')
            ->class('field220');

        $this->addFieldset('Personal details');
        $this->addSelect('titleId', 'Title', Customer::$titles)
            ->class('field220')
            ->setFirstOption('--- Select ---');
        $this->addText('firstName', 'First Name *')
            ->class('field220')
            ->addRule(self::Required, 'Please provide firstname!');
        $this->addText('lastName', 'Last Name  *')
            ->class('field220')
            ->addRule(self::Required, 'Please provide lastname!');
        $this->addText('companyName', 'Company name')
            ->class('field220');

        $this->addFieldset('Address details');
        $this->addText('address1', 'Address 1  *')
            ->class('field220')
            ->addRule(self::Required, 'Please provide address 1!');
        $this->addText('address2', 'Address 2 &nbsp;&nbsp;')
            ->class('field220');
        $this->addText('address3', 'Address 3 &nbsp;&nbsp;')
            ->class('field220');
        $this->addText('city', 'City  *')
            ->class('field220')
            ->addRule(self::Required, 'Please provide city!');
        $this->addText('county', 'County &nbsp;&nbsp;')
            ->class('field220');
        $this->addText('postcode', 'Postcode  *')
            ->class('field220')
            ->addRule(self::Required, 'Please provide post code!')
            ->addRule(array($this, 'Validator_postcode'), 'Postcode is not valid')
            ->setDescription('If UK postcode please add 1 space in the middle');
        $this->addSelect('countryId', 'Country  *', Customer::$countries)
            ->class('field220')
            ->setFirstOption('--- Select ---')
            ->addRule(self::Required, 'Please provide country!');
        $this->addText('phone', 'Phone *')
            ->class('field220')
            ->setDescription('(Only numbers, no spaces, max 14 characters)')
            ->addRule(self::Required, 'Please provide phone!')
            ->addRule(self::REGEXP, 'Phone is not valid.', '#^\d{1,14}$#');
        $this->addText('additionalPhone', 'Mobile / Other Phone')
            ->class('field220')
            ->setDescription('(Only numbers, no spaces, max 14 characters)')
            ->addRule(self::REGEXP, 'Phone is not valid.', '#^\d{1,14}$#');

        $this->addFieldset('Action');
        $this->addSubmit('login', 'Register')
            ->style('width: 200px; height: 30px;');
    }

}