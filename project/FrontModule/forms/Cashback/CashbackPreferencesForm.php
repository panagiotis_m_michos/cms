<?php

use Entities\Cashback;
use Entities\Customer as CustomerEntity;

class CashbackPreferencesForm extends FForm
{
    /**
     * @var CustomerEntity
     */
    private $customer;

    /**
     * @param CustomerEntity $customer
     */
    public function startup(CustomerEntity $customer)
    {
        $this->customer = $customer;
        $this->buildForm();
        $this->start();
    }

    /**
     * Check required fields for Cashback on Bank Account
     *
     * @param FControl $control
     * @param string $error
     * @param array $params
     * @return bool|string
     */
    public function Validator_requiredFields(FControl $control, $error, array $params)
    {
        /* @var $cashbackType FControl */
        list($cashbackType) = $params;
        if ($cashbackType->getValue() == CustomerEntity::CASHBACK_ON_BANK_ACCOUNT) {
            $value = $control->getValue();
            if (empty($value)) {
                return $error;
            }
        }
        return TRUE;
    }

    /**
     * @param FControl $control
     * @param string $error
     * @param array $params
     * @return bool|string
     */
    public function Validator_regexp(FControl $control, $error, array $params)
    {
        $value = $control->getValue();
        /* @var $cashbackType FControl */
        list($cashbackType, $regexp) = $params;
        if ($cashbackType->getValue() === CustomerEntity::CASHBACK_ON_BANK_ACCOUNT) {
            if (!preg_match($regexp, $value)) {
                return $error;
            }
        }
        return TRUE;
    }

    private function buildForm()
    {
        $this->addRadio(
            'cashbackTypeId',
            '<h3 class="cashback">How do you want to receive your cash back?</h3>',
            array(
                Cashback::CASHBACK_TYPE_CREDITS => 'As Credit on my Companies Made Simple Account <span class="cashbackTypeDescription">You can use your credit to pay for new purchases on this site</span>',
                Cashback::CASHBACK_TYPE_BANK => 'As money in my bank account <span class="cashbackTypeDescription">We deposit the money in any UK bank account you provide</span>'
            )
        )->class('cashback')
            ->setValue($this->customer->getCashbackType());
        $this->addText('sortCode', 'Sort Code:')
            ->setDescription('Use format XXXXXX')
            ->setValue($this->customer->getSortCode())
            ->addRule(array($this, 'Validator_requiredFields'), "Sort Code is required", array($this['cashbackTypeId']))
            ->addRule(
                array($this, 'Validator_regexp'),
                'For sort code use format 000000',
                array($this['cashbackTypeId'], '/^[0-9]{6}$/')
            );
        $this->addText('accountNumber', 'Account Number:')
            ->setDescription('UK accounts only')
            ->setValue($this->customer->getAccountNumber())
            ->addRule(
                array($this, 'Validator_requiredFields'),
                "Account Number is required",
                array($this['cashbackTypeId'])
            )
            ->addRule(
                array($this, 'Validator_regexp'),
                'Please provide 6-8 digit account number',
                array($this['cashbackTypeId'], '/^[0-9]{6,8}$/')
            );
        $this->addSubmit('submit', 'Save')
            ->class('orange-submit')
            ->addAtrib('data-toggleElement', 'true');
    }


}
