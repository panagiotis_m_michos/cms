<?php

class ProductsForm extends FForm
{
    /**
     * @var array
     */
    private $callback;

    /**
     * @var array
     */
    private $products;

    /**
     * @param array $callback
     * @param array $products
     */
    public function startup(array $callback, array $products)
    {
        $this->callback = $callback;
        $this->products = $products;

        $this->buildForm();
        $this->onValid = $this->callback;
        $this->start();
    }

    private function buildForm()
    {
        $this->addRadio(
            'productType',
            'Select a package and get your prestigious Virtual Office with one click!',
            $this->products
        )->addRule(FForm::Required, 'Please select a package');
        $this->addSubmit('submit', 'Get Virtual Office Service');
    }
}
