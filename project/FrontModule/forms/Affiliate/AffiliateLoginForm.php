<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    AffiliateLoginForm.php 2010-12-16 divak@gmail.com
 */
class AffiliateLoginForm extends FForm
{

    /**
     * @var AffiliateControler 
     */
    private $controler;
    /**
     * @var array
     */
    private $callback;

    /**
     * @param AffiliateControler $controler
     * @param array $callback
     */
    public function startup(AffiliateControler $controler, array $callback)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->init();
    }

    private function init()
    {

        $this->addFieldset('Login');
        $this->addText('email', 'Email: *')
            ->addRule(FForm::Required, 'Required!')
            ->addRule(FForm::Email, 'Email is not valid!');
        $this->addPassword('password', 'Password: *')
            ->addRule(FForm::Required, 'Required!')
            ->addRule('MinLength', 'Minimum 6 characters', 6)
            ->addRule(array($this, 'Validator_loginCredentials'), array(1 => "Email not found!", "Password doesn't match!", "Your account haven't been validated!", "Your account is blocked. Please contact our customer supoort line on 0207 608 5500."))
            ->setDescription('(Min 6 characters)');
        $this->addFieldset('Action');
        $this->addSubmit('submit', 'Submit')
            ->style('width: 200px; height: 30px;');

        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();

            // sign in
            $affiliate = Affiliate::doAuthenticate($data['email'], $data['password']);
            //pr($affiliate);exit;
            Affiliate::signIn($affiliate->getId());
            //pr($affiliate);exit;
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Validator
     * @param object $control
     * @param mixed $error
     * @param mixed $params
     * @return mixed
     */
    public function Validator_loginCredentials($control, $error, $params)
    {
        // authentificate user
        try {
            $user = Affiliate::doAuthenticate($control->owner['email']->getValue(), $control->owner['password']->getValue());
            return TRUE;
        } catch (Exception $e) {
            if ($e->getMessage() == 1) {
                $error = $error[1];
                return $error;
            } elseif ($e->getMessage() == 2) {
                $error = $error[2];
                return $error;
            } elseif ($e->getMessage() == 3) {
                $error = $error[3];
                return $error;
            } else {
                $error = $error[4];
                return $error;
            }
        }
    }

}