<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    AffiliateMyDetailsForm.php 2010-12-16 divak@gmail.com
 */

class AffiliateProductForm extends FForm
{
    /**
     * @var AffiliateProductsAdminControler
     */
    private $controler;

    /**
     * @var array
     */
    private $callback;

     /**
     * @var Affiliate
     */
    private $affiliate;


    /**
     * @param AffiliateProductsAdminControler $controler
     * @param array $callback
     */
    public function startup(AffiliatesAdminControler $controler, array $callback, Affiliate $affiliate)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->affiliate = $affiliate;
        $this->init();
    }

    private function init()
    {
        $this->addFieldset('Product');
//        $this->addText('markUp', 'Mark: *')
//                ->size(5);
//		$this->addSelect('productId', 'Product: *', BasketProduct::getAllProducts())
//			->setFirstOption('--- Select ---');

        $this->addFieldset('Action');
        $this->addSubmit('submit', 'Update')
				->class('btn submitAction');

        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
            $data = $this->getValues();
			//edit old ones
			if (!empty($this->controler->post['editMarkUp'])) {
				foreach ($this->controler->post['editMarkUp'] as $id => $value) {
					$affiliateProduct = new AffiliateProduct($id);
					$affiliateProduct->markUp = $value;
					$affiliateProduct->save();
				}
			}
			//create new 
//			if (!empty($data['productId'])) {
//				$affiliateProduct = new AffiliateProduct();
//				$affiliateProduct->affiliateId = $this->affiliate->affiliateId;
//				$affiliateProduct->productId = $data['productId'];
//				$affiliateProduct->markUp = $data['markUp'];
//				$affiliateProduct->save();
//			}
            $this->clean();
    }

}