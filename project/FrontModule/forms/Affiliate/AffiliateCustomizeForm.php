<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    AffiliateCustomizeForm.php 2010-12-16 divak@gmail.com
 */
class AffiliateCustomizeForm extends FForm
{

    /**
     * @var AffiliateLoggedControler
     */
    private $controler;
    /**
     * @var array
     */
    private $callback;
    /**
     * @var Affiliate
     */
    private $affiliate;

    /**
     * @param AffiliateLoggedControler $controler
     * @param array $callback
     */
    public function startup(AffiliateLoggedControler $controler, array $callback)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $session = FApplication::$session->getNameSpace('affiliate');
        $this->affiliate = new Affiliate($session->affiliateId);
        $this->init();
    }

    private function init()
    {
        $this->addFieldset('Data');
        $this->addText('companyColour', 'Company Colour: *')
            ->setValue($this->affiliate->companyColour)->Class('color')
            ->addRule(FForm::Required, 'Required!')
            ->addRule(FForm::REGEXP, 'Not valid', '/^([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?$/')
            ->setDescription('(without #)');
        $this->addText('textColour', 'Text Colour: *')
            ->setValue($this->affiliate->textColour)->Class('color')
            ->addRule(FForm::Required, 'Required!')
            ->addRule(FForm::REGEXP, 'Not valid', '/^([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?$/')
            ->setDescription('(without #)');
        $this->addFieldset('Action');
        $this->addSubmit('submit', 'Save Colours')
            ->style('width: 100px; height: 30px;');

        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();

            $affiliate = $this->affiliate;
            $affiliate->companyColour = $data['companyColour'];
            $affiliate->textColour = $data['textColour'];
            // --- default produts ---
            $affiliate->saveCustomize();
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

}