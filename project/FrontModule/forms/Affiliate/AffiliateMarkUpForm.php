<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    AffiliateMyDetailsForm.php 2010-12-16 divak@gmail.com
 */
class AffiliateMarkUpForm extends FForm
{

    /**
     * @var AffiliateLoggedControler
     */
    private $controler;
    /**
     * @var array
     */
    private $callback;
    /**
     * @var Affiliate
     */
    public $affiliate;

    /**
     * @param AffiliateLoggedControler $controler
     * @param array $callback
     */
    public function startup(AffiliateLoggedControler $controler, array $callback)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $session = FApplication::$session->getNameSpace('affiliate');
        $this->affiliate = new Affiliate($session->affiliateId);
        $this->init();
    }

    private function init()
    {
        $this->addFieldset('Product');
        $this->addFieldset('Action');
        $this->addSubmit('submit', 'Update Mark Up')
            ->class('btn submitAction');

        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();
            //edit old ones
            if (!empty($this->controler->post['editMarkUp'])) {
                foreach ($this->controler->post['editMarkUp'] as $id => $value) {
                    $affiliateProduct = new AffiliateProduct($id);
                    $affiliateProduct->markUp = $value;
                    $affiliateProduct->save();
                }
            }
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

}