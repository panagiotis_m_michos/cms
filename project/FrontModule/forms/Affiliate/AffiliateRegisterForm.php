<?php

class AffiliateRegisterForm extends FForm
{
    /**
     * @var AffiliateControler
     */
    private $controler;

    /**
     * @var array
     */
    private $callback;

    /**
     * @var array
     */
    public static $servicesIds = [
        Affiliate::SERVICE_WIDGET_CALCULATOR,
        Affiliate::SERVICE_WHITE_LABEL,
        Affiliate::SERVICE_WIDGET_SEARCH
    ];

    /**
     * @var array
     */
    public static $packagesIds = [1022, 898, 899];

    /**
     * @param AffiliateControler $controler
     * @param array $callback
     */
    public function startup(AffiliateControler $controler, array $callback)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->init();
    }

    private function init()
    {
        $this->addFieldset('Company Details');
        $this->addText('companyName', 'Company Name: *')
            ->addRule(FForm::Required, 'Required!');
        $this->addText('firstName', 'First Name: *')
            ->addRule(FForm::Required, 'Required!');
        $this->addText('lastName', 'Last Name: *')
            ->addRule(FForm::Required, 'Required!');
        $this->addText('web', 'Website: *')
            ->addRule(array($this, 'Validator_uniqueWeb'), 'Website url is already taken')
            ->addRule(FForm::REGEXP, 'Domain Not Valid', '/^((http\:\/\/|https\:\/\/|)([0-9a-z][\-0-9a-z]*\.)+[a-z\/]{2,6})?$/')
            ->addRule(FForm::Required, 'Required!');

        $this->addFieldset('Customise');
        $this->addText('companyColour', 'Company Colour: *')->Class('color')
            ->setValue('006699')
            ->addRule(FForm::Required, 'Required!')
            ->addRule(FForm::REGEXP, 'Not valid', '/^([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?$/')
            ->setDescription('(You can always edit this later)');
        $this->addText('textColour', 'Text Colour: *')->Class('color')
            ->addRule(FForm::Required, 'Required!')
            ->addRule(FForm::REGEXP, 'Not valid', '/^([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?$/')
            ->setDescription('(without #)');

        $this->addFieldset('Login Details');
        $this->addText('email', 'Email: *')
            ->addRule(FForm::Required, 'Required!')
            ->addRule(FForm::Email, 'Email is not valid!')
            ->addRule(array($this, 'Validator_uniqueEmail'), 'Email address is already taken');
        $this->addPassword('password', 'Password: *')
            ->addRule(FForm::Required, 'Required!')
            ->addRule('MinLength', 'Minimum 6 characters', 6)
            ->setDescription('(Min 6 characters)');
        $this->addPassword('passwordConfirmation', 'Confirmation: *')
            ->addRule(FForm::Required, 'Required!')
            ->addRule(FForm::Equal, 'Not the same!', 'password')
            ->setDescription('(Min 6 characters)');

        $this->addFieldset('Terms and Conditions');
        $link = FApplication::$router->link(AffiliateControler::TERMS_AND_CONDITIONS_PAGE);
        $termsLabel = Html::el('a')->href($link)->style('font-size: 12px;')->class('popup_page')->setHtml('Terms&nbsp;and&nbsp;Conditions');
        $termsLabel = Html::el('span')->style('font-size: 11px; color: #333333;')->setHtml($termsLabel . '<br />(Opens&nbsp;in&nbsp;new&nbsp;window)');
        $this->addCheckbox('terms', $termsLabel, 1)
            ->addRule(FForm::Required, 'Required!');

        $this->addFieldset('Action');
        $this->addSubmit('submit', 'Sign Up Now')
            ->class('btn_submit fright mbottom mright')
            ->style('width: 200px; height: 30px;');

        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        $data = $this->getValues();
        $affiliate = new Affiliate;
        $affiliate->hash = $this->Validator_hash();
        $affiliate->name = $data['companyName'];
        $affiliate->firstName = $data['firstName'];
        $affiliate->lastName = $data['lastName'];
        $affiliate->email = $data['email'];
        //$affiliate->phone = $data['phone'];
        $affiliate->web = preg_replace("/(http\:\/\/|https\:\/\/|\/)/i", " ", $data['web']);
        $affiliate->packagesIds = self::$packagesIds;
        $affiliate->companyColour = $data['companyColour'];
        $affiliate->textColour = $data['textColour'];
        $affiliate->servicesIds = self::$servicesIds;
        // setup default products
        // google tracking code - generate from name
        $affiliate->googleAnalyticsTrackingTag = String::webalize($affiliate->name);
        $affiliate->password = $data['password'];
        $affiliateId = $affiliate->save();

        Affiliate::signIn($affiliateId);

        $this->clean();
    }

    /**
     * @return string
     */
    public function Validator_hash()
    {
        $affiliatehash = TRUE;
        while (!empty($affiliatehash)) {
            $hash = FTools::generPwd(24);
            $result = dibi::select('affiliateId')
                ->from(TBL_AFFILIATES)
                ->where('hash=%s', $hash);
            $affiliatehash = $result->execute()->fetchSingle();
        }

        return $hash;
    }

    /**
     * @param Text $control
     * @param string $error
     * @param array $params
     * @return bool|string
     */
    public function Validator_uniqueEmail(Text $control, $error, array $params)
    {
        try {
            $affiliate = Affiliate::getAffiliateByEmail($control->getValue());
            return $error;
        } catch (Exception $e) {
            return TRUE;
        }
    }

    /**
     * @param Text $control
     * @param string $error
     * @param array $params
     * @return bool|string
     */
    public function Validator_uniqueWeb(Text $control, $error, array $params)
    {
        try {
            $affiliate = Affiliate::getAffiliateByWeb($control->getValue());
            return $error;
        } catch (Exception $e) {
            return TRUE;
        }
    }
}
