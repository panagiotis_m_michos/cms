<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    AffiliateMyDetailsForm.php 2010-12-16 divak@gmail.com
 */

class AffiliateMyDetailsForm extends FForm
{
    /**
     * @var AffiliateLoggedControler
     */
    private $controler;

    /**
     * @var array
     */
    private $callback;

     /**
     * @var Affiliate
     */
    public $affiliate;

    /**
     * @param AffiliateLoggedControler $controler
     * @param array $callback
     */
    public function startup(AffiliateLoggedControler $controler, array $callback)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $session = FApplication::$session->getNameSpace('affiliate');
        $this->affiliate = new Affiliate($session->affiliateId);
        $this->init();
    }

    private function init()
    {
        $this->addFieldset('Data');
        $this->addText('companyName', 'Company Name: *')
                ->setValue($this->affiliate->name)
                ->addRule(FForm::Required, 'Required!');
        $this->addText('firstName', 'First Name: *')
                ->setValue($this->affiliate->firstName)
                ->addRule(FForm::Required, 'Required!');
        $this->addText('lastName', 'Last Name: *')
                ->setValue($this->affiliate->lastName)
                ->addRule(FForm::Required, 'Required!');
//        $this->addText('web', 'Web: *')
//                ->setValue($this->affiliate->web)
//                ->addRule(FForm::Required, 'Required!');
        $this->addFieldset('Login');
        $this->addText('email', 'Email: *')
                ->setValue($this->affiliate->email)
                ->addRule(FForm::Required, 'Required!')
                ->addRule(FForm::Email, 'Email is not valid!')
                ->addRule(array(__CLASS__, 'Validator_uniqueLogin'), 'Email has been taken!', array('affiliate' =>  $this->affiliate));
        $this->addPassword('password', 'Password: *')
                ->addRule(FForm::Required, 'Required!')
                ->addRule('MinLength', 'Minimum 6 characters', 6)
                ->setDescription('(Min 6 characters)');
        $this->addPassword('passwordConfirmation', 'Confirmation: *')
                ->addRule(FForm::Required, 'Required!')
                ->addRule(FForm::Equal, 'Not the same!', 'password')
                ->setDescription('(Min 6 characters)');

        $this->addFieldset('Action');
        $this->addSubmit('submit', 'Submit')
                ->style('width: 200px; height: 30px;');

        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();
            $affiliate = $this->affiliate;
            $affiliate->name = $data['companyName'];
            $affiliate->firstName = $data['firstName'];
            $affiliate->lastName = $data['lastName'];
            $affiliate->web = $this->affiliate->web;
            $affiliate->email = $data['email'];
            $affiliate->password = $data['password'];
            //pr($affiliate);exit;
            $affiliate->save();
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }


    /**
	 * Validator for unique email address
	 *
	 * @param FControl $control
	 * @param string $error
	 * @param array $params
	 * @return mixed
	 */
	static public function Validator_uniqueLogin(FControl $control, $error, array $params)
	{
		$affiliate = $params['affiliate'];
		$result = dibi::select('affiliateId')
			->from(TBL_AFFILIATES)
			->where('email=%s', $control->getValue());

		if ($affiliate !== NULL) $result->and('affiliateId!=%s', $affiliate->affiliateId);
		$affiliateId = $result->execute()->fetchSingle();

		if ($affiliateId) return $error;
		return TRUE;
	}
}