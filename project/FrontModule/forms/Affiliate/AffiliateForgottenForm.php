<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    AffiliateForgottenForm.php 2010-12-16 divak@gmail.com
 */
class AffiliateForgottenForm extends FForm
{

    /**
     * @var AffiliateControler 
     */
    private $controler;
    /**
     * @var array
     */
    private $callback;

    /**
     * @var AffiliateEmailer 
     */
    private $affiliateEmailer;
    
    /**
     * @param AffiliateControler $controler
     * @param array $callback
     */
    public function startup(AffiliateControler $controler, array $callback, AffiliateEmailer $affiliateEmailer)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->affiliateEmailer = $affiliateEmailer;
        $this->init();
    }

    private function init()
    {

        $this->addFieldset('Your email');
        $this->addText('email', 'Email: *')
            ->addRule(FForm::Required, 'Required!')
            ->addRule(FForm::Email, 'Email is not valid!')
            ->addRule(array($this, 'Validator_forgetEmail'), 'Your email address could not be found. Please check the spelling and try again.');
        // action
        $this->addFieldset('Action');
        $this->addSubmit('submit', 'Submit')
            ->style('width: 200px; height: 30px;');

        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();
            $affiliate = Affiliate::getAffiliateByEmail($data['email']);
            $affiliate->password = FTools::generPwd(8);
            $affiliate->save();

            $link = FApplication::$httpRequest->uri->getHostUri() . $this->controler->router->link(AffiliateControler::LOGIN_PAGE);
            $this->affiliateEmailer->forgottenPasswordEmail($link, $affiliate);
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Checking if customer email is in database
     *
     * @param object $control
     * @param mixed $error
     * @param mixed $params
     * @return mixed
     */
    public function Validator_forgetEmail($control, $error, $params)
    {
        try {
            Affiliate::getAffiliateByEmail($control->getValue());
            return TRUE;
        } catch (Exception $e) {
            return $error;
        }
    }

}