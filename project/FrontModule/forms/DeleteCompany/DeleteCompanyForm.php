<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    FeedbacksForm.php 2011-05-24 razvanp@madesimplegroup.com
 */
class DeleteCompanyForm extends FForm
{

    /**
     * @var DeleteCompanyControler
     */
    private $controler;

    /**
     * @var array
     */
    private $callback;

    /**
     * @param DeleteCompanyControler $controler
     * @param array $callback
     */
    public function startup(DeleteCompanyControler $controler, array $callback)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->init();
    }

    /**
     * @return void
     */
    private function init()
    {
        $this->addFieldset('Delete Company');

        $this->addArea('message', 'Delete reason (optional):')->rows(2)->cols(35);

        //$this->addFieldset('');
        $this->addCheckbox('removeConfirm', 'I confirm I want to remove all records of this company from the Companies Made Simple system.', 1)
            ->addRule(FForm::Required, 'required!!');

        //$this->addFieldset('');
        $this->addCheckbox('removeObligation', 'I understand the statutory obligations regarding this company.', 1)
            ->addRule(FForm::Required, 'required!!');

        $this->addFieldset('Submit');
        $this->addSubmit('deleteCompany', 'Delete')->onclick('changes = false');
        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {

            $data = $this->getValues();
            $deletedCompanies = new DeleteCompany();
            $deletedCompanies->customerId = $this->controler->customer->customerId;
            $deletedCompanies->companyId = $this->controler->company->getCompanyId();
            $deletedCompanies->customerName = $this->controler->customer->firstName . ' ' . $this->controler->customer->lastName;
            $deletedCompanies->companyNumber = $this->controler->company->getCompanyNumber();
            $deletedCompanies->companyName = $this->controler->company->getCompanyName();
            $deletedCompanies->rejectMessage = $data['message'];
            $id = $deletedCompanies->save();

            if ($id) {
                $this->controler->company->setDeletedCompanyNumber($this->controler->company->getCompanyNumber());
                $this->controler->company->markDeleted();
            }

            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }
}
