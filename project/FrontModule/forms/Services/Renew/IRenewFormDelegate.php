<?php

namespace Services;

use Exception;

interface IRenewFormDelegate
{
    public function renewFormWithServices(array $services);
}
