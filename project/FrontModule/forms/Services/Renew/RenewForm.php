<?php

namespace Services;

use Checkbox;
use Entities\Company;
use FForm;
use Models\View\CompanyView;
use Models\View\ServiceView;
use ServiceSettingsModule\Services\ServiceSettingsService;
use Submit;

class RenewForm extends FForm
{

    /**
     * @var CompanyView[]
     */
    private $companies;

    /**
     * @var ServiceView[]
     */
    private $checkedServices = [];

    /**
     * @var ServiceSettingsService
     */
    private $settingsService;

    /**
     * @param CompanyView[] $companies
     * @param ServiceView[] $checkedServices
     * @param ServiceSettingsService $settingsService
     */
    public function startup(array $companies, array $checkedServices, ServiceSettingsService $settingsService)
    {
        $this->companies = $companies;
        $this->checkedServices = $checkedServices;
        $this->settingsService = $settingsService;

        $this->buildForm();
        $this->start();
    }

    private function buildForm()
    {
        foreach ($this->companies as $company) {
            $services = $company->getServices();
            foreach ($services as $service) {
                if ($service->hasRenewalProduct()) {
                    $renewalProduct = $service->getRenewalProduct();
                    $checkboxLabel = sprintf('&pound;%.2f / year', $renewalProduct->getPrice());
                    $renewCheckbox = $this->addCheckbox($service->getId(), $checkboxLabel, $renewalProduct->getId())
                        ->setGroup('services')
                        ->{"data-companyId"}($company->getId())
                        ->{"data-renewal-selector"}('service')
                        ->{"data-renewal-action"}('calculatePrice')
                        ->{"data-renewal-toggable"}('calculatePrice');

                    $settings = $this->settingsService->getSettingsByType($company->getCompany(), $service->getServiceTypeId());
                    if ($settings->isAutoRenewalEnabled()) {
                        $renewCheckbox->disabled('disabled');
                    }
                    $this->checkForm($renewCheckbox, $company->getCompany());
                }
            }
        }
        $this->addSubmit('submit', 'Continue >')
            ->addRule([$this, 'Validator_checked'], 'Please select at least one service to renew')
            ->class('button renew-submit')
            ->{"data-renewal-selector"}('submit')
            ->{"data-renewal-toggable"}('calculatePrice');
    }

    /**
     * @param Submit $submit
     * @param string $error
     * @return bool
     */
    public function Validator_checked(Submit $submit, $error)
    {
        $values = $submit->owner->getValues();
        $serviceIds = array_keys(array_filter($values['services']));

        if (!$serviceIds) {
            return $error;
        }

        return TRUE;
    }

    /**
     * @param Checkbox $checkbox
     * @param Company $company
     */
    private function checkForm($checkbox, Company $company)
    {
        foreach ($this->checkedServices as $service) {
            if ($checkbox->getName() != $service->getId()) {
                continue;
            }

            $settings = $this->settingsService->getSettingsByType($company, $service->getServiceTypeId());
            if (!$settings->isAutoRenewalEnabled()) {
                $checkbox->checked('checked');
            }
        }
    }

}
