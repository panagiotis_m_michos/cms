<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS
 * @author 		Nikolai Senkevich
 * @internal 	project/forms/Token/TokenDetailsForm.php
 * @created 	14/06/2012
 */
class TokenDetailsForm extends FForm
{

    /**
     * @var Tokens
     */
    private $token;
    /**
     * @var TokensModel
     */
    private $node;
    /**
     * @var Customer
     */
    private $customer;
    /**
     * @var TokenControler
     */
    private $controler;
    /**
     * @var array
     */
    private $callback;

    /**
     * @param TokensControler $controler
     * @param array $callback
     */
    public function startup(Tokens $token, TokensModel $node, Customer $customer, TokensControler $controler, array $callback)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->token = $token;
        $this->node = $node;
        $this->customer = $customer;

        $this->init();
    }

    private function init()
    {

        $this->addFieldset('Card Details');

        $cardholder = $this->addText('cardholder', "Cardholder's Name: *");
        
        if(isset($this->token->firstname) && isset($this->token->firstname))
        {
            $cardholder->setValue($this->token->firstname . ' ' . $this->token->surname);
        }
            
        $cardholder->addRule(FForm::Required, "Cardholder's name is required!")
        ->addRule('MaxLength', 'Name must be upto 50 characters', array(50));

        $this->addSelect('cardType', 'Card Type: *', SagePayDirect::getCardTypes())
            ->setFirstOption('--- Select ---')
            ->addRule(FForm::Required, "Card type is required!");

        $this->addText('cardNumber', 'Card Number: *')
            ->setValue(isset($this->token->cardNumber) ? '**** **** **** ' . $this->token->cardNumber : '')
            ->addRule(FForm::NUMERIC, 'Invalid Card Number!')
            ->addRule('MaxLength', 'Too long Card Number. Do NOT enter spaces.', array(20));

        $this->addText('issueNumber', 'Issue Number: ')
            ->size(1)
            ->addRule(array($this, 'Validator_IssueNumber'), 'Issue number or Valid from is required!')
            ->addRule('MaxLength', 'Issue number must be upto 2 characters', array(2));

        $this->add('CardValidFromDate', 'validFrom', 'Valid from: ');
            //->addRule(array($this, 'Validator_IssueNumber'), 'Issue number or Valid from is required!');

        $this->add('CardExpiryDate', 'expiryDate', 'Expiry Date: *')
            ->addRule(array($this, 'Validator_ExpiryDate'), array('Expiry date is required!', "Expiry date has to be more than or equal to today's date!"));

        $this->addText('CV2', 'Security Code: *')
            ->size(1)
            ->addRule(array($this, 'Validator_securityCode'), "Security code must be a three digit number!");


        $this->addFieldset('Address');

        $this->addText('firstname', 'Firstname: *')
            ->setValue($this->token->firstname)
            ->addRule(FForm::Required, 'Firstname is required!')
            ->addRule('MaxLength', 'Firstname must be upto 20 characters', array(20));

        $this->addText('surname', 'Surname: *')
            ->setValue($this->token->surname)
            ->addRule(FForm::Required, 'Surname is required!')
            ->addRule('MaxLength', 'Surname must be upto 20 characters', array(20));

        $this->addText('address1', 'Address 1: *')
            ->setValue($this->token->address1)
            ->addRule(FForm::Required, 'Address is required!')
            ->addRule('MaxLength', 'Address 1 must be upto 100 characters', array(100));

        $this->addText('address2', 'Address 2: ')
            ->setValue($this->token->address2)
            ->addRule('MaxLength', 'Address 2 must be upto 50 characters', array(50));


        $this->addText('town', 'Town: *')
            ->setValue($this->token->city)
            ->addRule(FForm::Required, 'Town is required!')
            ->addRule('MaxLength', 'Town must be upto 40 characters', array(40));

        $this->addText('postcode', 'Post Code: *')
            ->setValue($this->token->postCode)
            ->addRule(FForm::Required, 'Post code is required!')
            ->addRule('MaxLength', 'Post code must be upto 10 characters', array(10));

        $this->addSelect('country', 'Country: *', WPDirect::$countries)
            ->setValue($this->token->country)
            ->setFirstOption('--- Select ---')
            ->style('width: 150px;')
            ->addRule(FForm::Required, 'Country is required!');

        $this->addSelect('billingState', 'State:', SagePayDirect::getStates())
            ->setValue($this->token->state)
            ->setFirstOption('--- Select ---');

        $this->addFieldset('Action');
        if ($this->node->getCustomerToken($this->customer) == NULL) {
            $this->addSubmit('submit', 'Generate')
                ->class('btn')->style('width: 200px; height: 30px;');
        } else {
            $this->addSubmit('submit', 'Update')
                ->class('btn')->style('width: 200px; height: 30px;');
        }
        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();

            // expiry date
            $explode = explode('-', $data['expiryDate']);
            $explode[0] = substr($explode[0], -2);
            if (strlen($explode[1]) < 2) {
                $explode[1] = str_replace($explode[1], '0' . $explode[1], $explode[1]);
            }
            $expiryDate = $explode[1] . $explode[0];

            if (isset($data['cardType']) && $data['cardType'] == SagePayDirect::CARD_MAESTRO) {
                if (isset($data['validFrom'])) {pr($data['validFrom']);
                    $explode = explode('-', $data['validFrom']);
                    //adding 1 becouse array starting from 0
                    $explode[1] = $explode[1]+1;
                    $explode[0] = substr($explode[0], -2);
                    if (strlen($explode[1]) < 2) {
                        $explode[1] = str_replace($explode[1], '0' . $explode[1], $explode[1]);
                    }
                    $validFrom = $explode[1] . $explode[0];
                }
            }

            $card = new SagePayTokenCard();
            $card->setCardDetails($data['cardholder'], $data['cardType'], $data['cardNumber'], $expiryDate, $data['CV2']);

            if (isset($data['issueNumber'])) {
                $card->setIssueNumber(trim($data['issueNumber']));
            }
            if (isset($validFrom)) {
                $card->setStartDate(trim($validFrom));
            }

            $address = new SagePayAddress();
            $address->setAddressDetails($data['firstname'], $data['surname'], $data['address1'], $data['town'], $data['postcode'], $data['country']);

            if (isset($data['address2'])) {
                $address->setLine2($data['address2']);
            }

            if (isset($data['billingState']) && trim($data['country']) == 'US') {
                $address->setState(trim($data['billingState']));
            }

            $token = $this->node->getToken($this->customer, $card, $address);
            $token->save();

            //check if token already exist and remove for token update
            if ($this->node->getCustomerToken($this->customer) != NULL && !empty($this->token->token)) {
                $this->node->removeToken($this->token);
            }
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

    /*     * ******************************** validators ******************************** */

    /**
     * Check required issue number for maestro and solo
     * @param object $control
     * @param string $error
     * @param mixed $params
     * @return mixed
     */
    public function Validator_IssueNumber($control, $error, $params)
    {
        $form = $control->owner;
        $cardType = strtolower($form['cardType']->getValue());
        $issue = $form['issueNumber']->getValue();
        $validFrom = $form['validFrom']->getValue();
        if (($cardType == 'maestro' || $cardType == 'solo') && empty($issue) && empty($validFrom)) {
            return $error;
        }
        return TRUE;
    }

    /**
     * Check required issue number for maestro and solo
     * @param object $control
     * @param string $error
     * @param mixed $params
     * @return mixed
     */
    public function Validator_ExpiryDate($control, $error, $params)
    {
        if ($control->owner->isSubmitedBy('submit') && !isset($control->owner['credit']) || $control->owner->isSubmitedBy('submit') && isset($control->owner['credit']) && $control->owner['credit']->getValue() != 1) {
            // required
            if ($control->getValue() == NULL) {
                return $error[0];
            }
            // greater than
            $expireDate = $control->owner['expiryDate']->getValue();
            if (strtotime($expireDate) < strtotime(date("Y-m"))) {
                return $error[1];
            }
        }
        return TRUE;
    }

    public function Validator_securityCode($control, $error, $params)
    {
        if ($control->owner->isSubmitedBy('submit') && !isset($control->owner['credit']) || $control->owner->isSubmitedBy('submit') && isset($control->owner['credit']) && $control->owner['credit']->getValue() != 1) {
            $value = $control->getValue();
            if (empty($value) || !is_numeric($value) || strlen($value) != 3) {
                return $error;
            }
        }
        return true;
    }

}