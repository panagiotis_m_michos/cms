<?php

use Entities\Answer;

class CFRegisteredOfficeForm extends FForm
{

    /**
     * @var CFRegisteredOfficeControler
     */
    private $controler;

    /**
     * @var array
     */
    private $callback;

    /**
     *
     * @var object
     */
    private $company;

    /**
     *
     * @var array
     */
    private $prefillAddress;

    /**
     * @var Answer
     */
    private $answer;

    /**
     * @var bool
     */
    private $isRetail;


    /**
     * @param CFRegisteredOfficeControler $controler
     * @param array $callback
     */
    public function startup(
        CFRegisteredOfficeControler $controler,
        array $callback,
        $company,
        $prefillAddress,
        $isRetail = FALSE,
        Answer $answer = NULL
    )
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->company = $company;
        $this->prefillAddress = $prefillAddress;
        $this->answer = $answer;
        $this->isRetail = $isRetail;

        $this->init();
    }

    private function init()
    {
        if ($this->canShowFirstQuestion() || $this->canShowSecondQuestion()) {
            $this->addFieldset('Your details');

            if ($this->canShowFirstQuestion()) {
                $this->addSelect('answer', 'Which best describes you? *', Answer::$customerTypes)
                    ->addRule(FForm::Required, 'Please select which best describes you')
                    ->setFirstOption('--- Select ---')
                    ->style('width:150px');
                $this->addArea('additionalAnswer', 'Tell us more')
                    ->rows(3);
            }

            if ($this->canShowSecondQuestion()) {
                $this->addSelect('tradingStart', 'When are you looking to trade? *', Answer::$tradingPeriods)
                    ->addRule(FForm::Required, 'Please select when you are looking to trade')
                    ->setFirstOption('--- Select ---')
                    ->style('width:150px');
            }
        }
        // if has registered office
        $registeredOfficeId = $this->company->getRegisteredOfficeId();
        if ($registeredOfficeId) {
            // our registered office
            $this->addFieldset('Use Our Registered office service');
            $this->addCheckbox('ourRegisteredOffice', 'Registered office service  ', 1)
                ->setValue($this->company->getLastIncorporationFormSubmission()->getForm()->hasMSGRegisteredOffice());
        }

        // address
        $this->addFieldset('Prefill Address');
        $this->addSelect('prefill', 'Prefill', $this->prefillAddress['select'])
            ->setFirstOption('--- Select ---');

        $this->addFieldset('Address');
        $this->addText('premise', 'Building name/number *')
            ->addRule([$this, 'Validator_officeRequired'], 'Please provide Building name/number')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "Building name/number can't be more than 50 characters", 50);
        $this->addText('street', 'Street *')
            ->addRule([$this, 'Validator_officeRequired'], 'Please provide address 2')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL);
        $this->addText('thoroughfare', 'Address 3')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "Address 3 can't be more than 50 characters", 50);
        $this->addText('post_town', 'Town *')
            ->addRule([$this, 'Validator_officeRequired'], 'Please provide town')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "Town can't be more than 50 characters", 50);
        $this->addText('county', 'County')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "County can't be more than 50 characters", 50);
        $this->addText('postcode', 'Postcode *')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule([$this, 'Validator_officeRequired'], 'Please provide postcode')
            ->addRule([$this, 'Validator_serviceAddress'], 'You cannot use our postcode for this address')
            ->addRule('MaxLength', "Postcode can't be more than 15 characters", 15);
        $this->addSelect('country', 'Country *', Address::$registeredOfficeCountries)
            ->setFirstOption('--- Select ---')
            ->addRule([$this, 'Validator_officeRequired'], 'Please provide country')
            ->addRule('MaxLength', "Country can't be more than 50 characters", 50);

        // action
        $this->addFieldset('Action');
        $this->addSubmit('continue', 'Continue >')->class('btn_submit fright mbottom');
        $this->onValid = $this->callback;

        $address = $this->company->getLastIncorporationFormSubmission()->getForm()->getAddress()->getFields();
        $this->setInitValues($address);
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $lastForm = $this->company->getLastIncorporationFormSubmission()->getForm();
            $address = $lastForm->getAddress();
            $registeredOfficeId = $this->company->getRegisteredOfficeId();

            // has register office
            if ($registeredOfficeId != NULL && isset($this['ourRegisteredOffice']) && $this['ourRegisteredOffice']->getValue() == 1) {
                self::saveOurRegisteredOffice($this->company, $this->company->getRegisteredOfficeId());
            } else {
                $address->setFields($this->getValues());
                $lastForm->setAddress($address);
                $this->company->getLastIncorporationFormSubmission()->getForm()->setMSGRegisteredOffice(FALSE);
            }
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Saving register office to company
     *
     * @param Company $company
     * @param int $registeredOfficeId
     */
    public static function saveOurRegisteredOffice($company, $registeredOfficeId)
    {
        $office = new RegisterOffice($registeredOfficeId);

        // there is something wrong with registered office
        if ($office->isOk2show() == FALSE) {
            throw new Exception('Registered office can not be added!');

            // we can save registered office
        } else{//if ($company->getLastFormSubmission()->getForm()->hasMSGRegisteredOffice() == FALSE) {

            $address = $company->getLastIncorporationFormSubmission()->getForm()->getAddress();
            $address->setPremise($office->address1);
            $address->setStreet($office->address2);
            $address->setThoroughfare($office->address3);
            $address->setPostTown($office->town);
            $address->setCounty($office->county);
            $address->setPostcode($office->postcode);
            $address->setRegisteredOfficeCountry($office->countryId);

            $company->getLastIncorporationFormSubmission()->getForm()->setMSGRegisteredOffice(TRUE);
            $company->getLastIncorporationFormSubmission()->getForm()->setAddress($address);
        }
    }

    /**
     * Validate if registered office is required
     *
     * @param FControl $control
     * @param string $error
     * @param array $params
     * @return mixed
     */
    public function Validator_officeRequired($control, $error)
    {
        $value = $control->getValue();
        if (isset($control->owner['ourRegisteredOffice']) && $control->owner['ourRegisteredOffice']->getValue() == 1) {
            return TRUE;
        } else {
            if (empty($value)) {
                return $error;
            }
        }
        return TRUE;
    }

    public function Validator_serviceAddress($control, $error)
    {
        if (strstr(strtoupper($control->owner['postcode']->getValue()), 'EC1V4PW')
            ||strstr(strtoupper($control->owner['postcode']->getValue()), 'EC1V 4PW')
        ) {
            $registeredOfficeId = $this->company->getRegisteredOfficeId();
            if (empty($registeredOfficeId)) {
                return $error;
            }
        }
        return TRUE;
    }

    /**
     * @return bool
     */
    public function canShowFirstQuestion()
    {
        if ($this->answer && $this->answer->getText()) {
            return FALSE;
        }

        return $this->isRetail;
    }

    /**
     * @return bool
     */
    public function canShowSecondQuestion()
    {
        $tradingStart = NULL;
        if ($this->answer) {
            $tradingStart = $this->answer->getTradingStart();
        }

        return $this->isRetail && empty($tradingStart);
    }
}
