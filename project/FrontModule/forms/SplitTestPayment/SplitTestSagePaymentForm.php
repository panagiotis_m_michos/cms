<?php

class SplitTestSagePaymentForm extends FForm
{

    /**
     * @var CFCompanyCustomerControler 
     */
    private $controler;

    /**
     * @var array
     */
    private $callback;

    /**
     * @var Basket
     */
    public $basket;

    /**
     * @var Customer
     */
    public $customer;
    protected $storable = FALSE;

    /**
     * @var PaymentModel 
     */
    protected $node;

    /**
     * @var array
     */
    static public $types = array(
        Transaction::ON_ACCOUNT => 'Pay by Account',
        Transaction::TYPE_SAGEPAY => 'Pay by Credit / Debit',
        Transaction::TYPE_PAYPAL => 'Pay via PayPal',
    );

    /**
     * @param PaymentControler $controler
     * @param array $callback
     */
    public function startup(SplitTestPaymentControler $controler, array $callback, Basket $basket, Customer $customer, PaymentModel $node)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->basket = $basket;
        $this->customer = $customer;
        $this->node = $node;
        $this->init();
    }

    private function init()
    {

        $this->setAction($this->controler->router->link('PaymentNewCustomerControler::SAGE_NEW_CUSTOMER_PAGE') . '#authFormBlock');
        $this->addFieldset('Preferred contact details');

        //if for new customers
        if ($this->customer->isNew()) {
            $this->addText('emails', 'Email: *')
                    ->addRule(FForm::Required, 'Please provide e-mail address')
                    ->addRule(FForm::Email, 'Email is not valid')
                    ->addRule(array($this, 'Validator_uniqueLogin'), 1)
                    ->class('text250');
        }

        //if is not a free basket 
        if (!$this->basket->isFreeBasket()) {
            $this->add('SplitTestRadio', 'paymentOption', '')
                    ->setOptions($this->getPaymentOptions(self::$types))
                    ->setValue(Transaction::TYPE_SAGEPAY)
                    ->addRule(array($this, 'Validator_wholesalerCredit'), 'You do not have enough credit to make this purchase. Please contact your wholesale agent.', array('amount' => $this->basket->getPrice('total'), 'credit' => $this->customer->credit));

            $this->addText('cardNumber', 'Card Number: *')
                    ->addRule(array($this, 'Validator_requiredFields'), 'Card number is required')
                    ->addRule('MaxLength', 'Too long card number. Do NOT enter spaces.', array(20));
            $this->addText('cardholder', "Name on Card: *")
                    ->addRule(array($this, 'Validator_requiredFields'), "Cardholder's name is required")
                    ->addRule('MaxLength', 'Name must be upto 50 characters', array(50));
            $this->addSelect('cardType', 'Card Type: *', SagePayDirect::getCardTypes())
                    ->setFirstOption('--- Select ---')
                    ->addRule(array($this, 'Validator_requiredFields'), "Card type is required");
            $this->addText('issueNumber', 'Issue Number: ')
                    ->size(1)
                    ->addRule(array($this, 'Validator_IssueNumber'), 'Issue number or Valid from is required')
                    ->addRule('MaxLength', 'Issue number must be upto 2 characters', array(2));
            $this->addText('CV2', '3 Digit Security Code: *')
                    ->size(3)
                    #->addRule(array($this, 'Validator_requiredFields'), 'Security code is required!')
                    ->addRule(array($this, 'Validator_securityCode'), "Please enter a valid security code");
            $this->add('CardExpiryDateSplitTest', 'expiryDate', 'Expiry Date: *')
                    ->addRule(array($this, 'Validator_ExpiryDate'), array('Expiry date is required', "Expiry date has to be more than or equal to today's date!"));
            $this->add('CardValidFromDate', 'validFrom', 'Valid from: ')
                    ->addRule(array($this, 'Validator_IssueNumber'), 'Issue number or Valid from is required!');
            $this->addText('address1', 'Address: *')
                    ->addRule(array($this, 'Validator_requiredFields'), 'Address is required')
                    ->addRule('MaxLength', 'Address 1 must be upto 100 characters', array(100))
                    ->setValue(!$this->customer->isNew() ? $this->customer->address1 : NULL);
            $this->addText('address2', 'Address 2:')
                    ->addRule('MaxLength', 'Address 2 must be upto 50 characters', array(50))
                    ->setValue(!$this->customer->isNew() ? $this->customer->address2 : NULL);
            $this->addText('address3', 'Address 3:')
                    ->addRule('MaxLength', 'Address 3 must be upto 50 characters', array(50))
                    ->setValue(!$this->customer->isNew() ? $this->customer->address3 : NULL);
            $this->addText('town', 'Town/City: *')
                    ->addRule(array($this, 'Validator_requiredFields'), 'Town is required')
                    ->addRule('MaxLength', 'Town must be upto 40 characters', array(40))
                    ->setValue(!$this->customer->isNew() ? $this->customer->city : NULL);
            $this->addText('postcode', 'Post Code: *')
                    ->size(12)
                    ->addRule(array($this, 'Validator_requiredFields'), 'Post code is required')
                    ->addRule('MaxLength', 'Post code must be upto 10 characters', array(10))
                    ->setValue(!$this->customer->isNew() ? $this->customer->postcode : NULL);
            $this->addSelect('country', 'Country: *', WPDirect::$countries)
                    //->setFirstOption('--- Select ---')
                    ->style('width: 150px;')
                    ->addRule(array($this, 'Validator_requiredFields'), 'Country is required')
                    ->setValue(!$this->customer->isNew() ? $this->controler->node->getCustomerPaymentCountry($this->customer) : NULL);
            $this->addSelect('billingState', 'State: *', SagePayDirect::getStates())
                    ->setFirstOption('--- Select ---');
        }

        $this->addHidden('paymentType', 0);
        $this->addHidden('onAccountPayment', Transaction::ON_ACCOUNT);
        $this->addCheckbox('tokenPayment', 'Use Token payment', 1);


        $this->addFieldset('Or pay using');
        $this->addSubmit('paypal', 'Make Secure Payment')->class('btn_paynow')->onClick('doPaypal(' . Transaction::TYPE_PAYPAL . ')');

        $this->addFieldset('Action');
        $paymentType = $this->basket->isFreeBasket() ? Transaction::TYPE_FREE : Transaction::TYPE_SAGEPAY;
        $this->addSubmit('submit', 'Make Secure Payment')->class('btn_paynow')->onClick('doPayment(' . $paymentType . ')');


        //$this->onValid = $this->callback;
        $this->start();
        $this->savePaymentFormErrors($this);
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();
            die;
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param array $types
     * @return array
     */
    private function getPaymentOptions(array $types)
    {
        $options = array();
        // --- account ---
        if ($this->node->isOkToShowOnAccountPayment($this->customer, $this->basket)) {
            if (isset($types[Transaction::ON_ACCOUNT])) {
                $options[Transaction::ON_ACCOUNT] = 
                        Html::el('span')->class('pRadioLabelSpan_n')
                        ->setText('Pay by Account');
                $options[Transaction::ON_ACCOUNT] .= 
                        Html::el('span')->class('onaccount-payment-text_n')
                        ->setText('Avaliable Credit £' . $this->customer->credit);
            }
        }
        // --- sage ---
        if (isset($types[Transaction::TYPE_SAGEPAY])) {
            $options[Transaction::TYPE_SAGEPAY] = 
                    Html::el('span')->class('pRadioLabelSpan_n')
                    ->setText('Pay via Credit / Debit card');
            $options[Transaction::TYPE_SAGEPAY] .= 
                    Html::el('span')->setHtml("
                        <ul class='cards valign-middle ie7cards'>
                        <li class='VISA'>Visa</li>
                        <li class='UKE'>Visa Electron</li>
                        <li class='MC'>Mastercard</li>
                        <li class='MAESTRO'>Maestro</li>
                        </ul>");
        }
        // --- paypal ---
        if (isset($types[Transaction::TYPE_PAYPAL])) {
            $options[Transaction::TYPE_PAYPAL] = Html::el('span')->class('pRadioLabelSpan_n')->setText('Pay via PayPal');
            $options[Transaction::TYPE_PAYPAL] .=
                    Html::el('span')->setHtml("
                        <ul class='paypalCards valign-middle ie7cards'>
                        <li class='PAYPAL'>PayPal</li>
                        <li class='AMEX'>Amex</li>
                        </ul>");
            $options[Transaction::TYPE_PAYPAL] .= Html::el('span')
                    ->class('normalfont txtitalic grey11 txtnormal')
                    ->setText('We also accept AMEX when paying by PayPal');
        }

        return $options;
    }

    /**
     * Validate postcode for UK
     *
     * @param Text $control
     * @param string $error
     * @param array $params
     * @return boolean
     */
    static public function Validator_postcode(Text $control, $error)
    {
        $value = $control->getValue();
        if ((!preg_match('#^(GIR 0AA|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]|[A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y]))|[0-9][A-HJKPS-UW]) [0-9][ABD-HJLNP-UW-Z]{2})$#i', $value) || !preg_match('#^([A-Z][\dA-Z]{1,3}[\s][\d][A-Z]{2,2})$#i', $value))) {
            return $error;
        }
        return TRUE;
    }

    /*     * ******************************** validators ******************************** */

    /**
     * Check required issue number for maestro and solo
     * @param object $control
     * @param string $error
     * @param mixed $params
     * @return mixed
     */
    public function Validator_IssueNumber($control, $error)
    {
        $form = $control->owner;
        $cardType = strtolower($form['cardType']->getValue());
        $issue = $form['issueNumber']->getValue();
        $validFrom = $form['validFrom']->getValue();
        if (($cardType == 'maestro' || $cardType == 'solo') && empty($issue) && empty($validFrom)) {
            return $error;
        }
        return TRUE;
    }

    /**
     * Check required issue number for maestro and solo
     * @param object $control
     * @param string $error
     * @param mixed $params
     * @return mixed
     */
    public function Validator_ExpiryDate($control, $error)
    {
        if ($control->owner['paymentOption']->getValue() == Transaction::TYPE_SAGEPAY) {
            // required
            if ($control->getValue() == NULL) {
                return $error[0];
            }
            // greater than
            $expireDate = $control->owner['expiryDate']->getValue();
            if (strtotime($expireDate) < strtotime(date("Y-m"))) {
                return $error[1];
            }
        }
        return TRUE;
    }

    /**
     * Check required fields fo card payment
     *
     * @param object $control
     * @param string $error
     * @param array $params
     * @return mixed
     */
    public function Validator_requiredFields($control, $error)
    {
        if ($control->owner['paymentOption']->getValue() == Transaction::TYPE_SAGEPAY) {
            $value = $control->getValue();
            if (empty($value)) {
                return $error;
            }
        }
        return TRUE;
    }

    /**
     * Check enough credit for pay by credit
     *
     * @param Radio $control
     * @param String $error
     * @param array $params
     * @return mixed
     */
    public function Validator_wholesalerCredit(Radio $control, $error, $params)
    {
        if ($control->getValue() == Transaction::ON_ACCOUNT && $params['amount'] > $params['credit']) {
            return $error;
        }
        return TRUE;
    }

    /**
     *
     * @param type $control
     * @param type $error
     * @param type $params
     * @return boolean 
     */
    public function Validator_securityCode($control, $error, $params)
    {
        if ($control->owner['paymentOption']->getValue() == Transaction::TYPE_SAGEPAY) {
            $value = $control->getValue();
            if (empty($value) || !is_numeric($value) || strlen($value) != 3) {
                return $error;
            }
        }
        return true;
    }

    /**
     * Validator
     * @param object $control
     * @param mixed $error
     * @param mixed $params
     * @return mixed
     */
    public function Validator_uniqueLogin($control, $error, $params)
    {
        $customerId = FApplication::$db->select('customerId')->from(TBL_CUSTOMERS)->where('email=%s', $control->getValue())->execute()->fetchSingle();
        if ($customerId)
            return $error;
        return TRUE;
    }

    public function savePaymentFormErrors($form)
    {
        $errors = $form->getErrors();
        if (!empty($errors)) {
            $paymentFormLog = new PaymentFormLogs();
            $values = $this->getValues();
            $paymentFormLog->email = isset($values['emails']) ? $values['emails'] : Null;
            $paymentFormLog->customerId = $this->customer->getId();
            $paymentFormLog->cardHolder = isset($errors['cardholder']) ? $errors['cardholder'] : Null;
            $paymentFormLog->cardNumber = isset($errors['cardNumber']) ? $errors['cardNumber'] : Null;
            $paymentFormLog->cardType = isset($errors['cardType']) ? $errors['cardType'] : Null;
            $paymentFormLog->expiryDate = isset($errors['expiryDate']) ? $errors['expiryDate'] : Null;
            $paymentFormLog->securityCode = isset($errors['CV2']) ? $errors['CV2'] : Null;
            $paymentFormLog->address1 = isset($errors['address1']) ? $errors['address1'] : Null;
            $paymentFormLog->town = isset($errors['town']) ? $errors['town'] : Null;
            $paymentFormLog->postCode = isset($errors['postcode']) ? $errors['postcode'] : Null;
            $paymentFormLog->country = isset($errors['country']) ? $errors['country'] : Null;
            $paymentFormLog->termscond = isset($errors['terms']) ? $errors['terms'] : Null;
            $paymentFormLog->save();
        }
    }

}