<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    ChangeCompanyNameForm.php 2011-09-28 razvanp@madesimplegroup.com
 */

class ChangeCompanyNameForm  extends FForm
{
    /**
     * @var CUChangeNameControler 
     */
    private $controler;
    
    /**
     * @var array
     */
    private $callback;
    
    /**
     * @var Company
     */
    private $company;

    /**
     * @param CUChangeNameControler $controler
     * @param array $callback
     */
    public function startup(CUChangeNameControler $controler, array $callback, $company)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->company = $company;
        $this->init();
    }

    /**
	 * @return void
	 */
    private function init()
    {
        $this->addFieldset('Submit Feedback');
        
        $this->addText('newCompanyName','New Company Name')
            ->style('width: 300px;')
            ->addRule(array($this, 'Validator_companyName'), array('Name can\'t be empty', 'Name is already in use.'))
            ->addRule(array('CompanyNameValidator', 'isValid'), NULL);
			//->addRule(array($this, 'Validator_companyNameSuffix'), array('Please add LTD or LIMITED suffix to company name','Please add PLC suffix to company name','Please add PLC or LTD suffix to company name'));
        
        $this->add('RadioInline','radio', 'Suffix: ')
            ->setOptions(CompanyNameChangeServiceModel::getSuffixOptions($this->company->getType()))
            ->addRule(array($this, 'Validator_suffix'), 'Suffix is Required');
        
        //$this->addHidden('changeNameId', $this->company->getChangeNameId());
        
        $this->addSubmit('send', 'Change')->class('btn_submit fright mbottom');
			//->onclick('return confirm("Please double check your company name before submitting.")');

        $this->onValid = $this->callback;
	    $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $fields = array();
            
            $data = $this->getValues();
            
            $fields['methodOfChange'] = CompanyNameChangeServiceModel::setCompanyChangeNameMethodOfChange($this->company);
            $fields['newCompanyName'] = htmlentities(strtoupper($data['newCompanyName']) . ' ' .$data['radio'] , ENT_NOQUOTES, 'UTF-8');
            $fields['meetingDate'] = date('Y-m-d') ;
            $fields['sameDay'] = CompanyNameChangeServiceModel::setCompanyChangeNameSameDay($this->company) ;
            $fields['noticeGiven'] = TRUE;

            $this->company->sendChangeCompanyName($fields);
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }
     
    /**
     * @param FFORM RadioInLine $control
     * @param array $error
     * @param array $params
     * @return error or true
     */
    public function Validator_suffix($control, $error, $params)
	{
		if ($control->owner->isSubmitedBy('back') == FALSE) {
			try {
				if (
                    ($this->company->getType() != 'BYGUAREXUNDSEC60') 
                    && ($control->getValue() == '')
                    ) {
					return $error;
				}
			} catch (Exception $e) {
				return $e->getMessage();
			}
		}
		return TRUE;
	}
    
    /**
     * @param FFORM Text $control
     * @param array $error
     * @param array $params
     * @return error or true
     */
    public function Validator_companyName($control, $error, $params)
	{
		if ($control->owner->isSubmitedBy('back') == FALSE) {
			try {
				if ($control->getValue() == '') {
					return $error[0];
				}
				$available = CompanySearch::isAvailable($control->getValue());
				return ($available === TRUE) ? TRUE : $error[1];
			} catch (Exception $e) {
				return $e->getMessage();
			}
		}
		return TRUE;
	}


    /**
     * @param FFORM Text $control
     * @param array $error
     * @param array $params
     * @return error or true
     */
	public function Validator_companyNameSuffix(Text $control, $error, $params)
	{
		$type = $this->company->getType();
		if (!preg_match('/( PLC| LTD| LIMITED| PLC.| LTD.| LIMITED. | CYFYNGEDIG )$/', strtoupper(trim($control->getValue())))) {

		/* by shares and by guarantee */
		if ($type == 'BYSHR' || $type == 'BYGUAR') {
			return $error[0];
		/* plc */
		} elseif ($type == 'PLC') {
			return $error[1];
		}
			return $error[2];
		}
		return TRUE;
    }    
    
}
