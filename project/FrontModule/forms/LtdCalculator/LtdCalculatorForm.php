<?php

class LtdCalculatorForm extends FForm
{
    public function startup()
    {
        $this->builForm();
        $this->start();
    }

    protected function builForm()
    {
        $this->addFieldset('Should I form a limited company?');
        $this->addText('annualProfit', 'Enter your annual profits:')
            ->size(7)
            ->placeholder('e.g. 35000')    
            ->addRule(FForm::Required, 'Please provide your annual profit')
            ->addRule(FForm::NUMERIC, 'Please provide a numeric value')
            ->addRule(array($this, 'Callback_validRange'), 'Please provide values from 1 to 10000000000');
        $this->addSubmit('submit', 'Calculate')->style('width: 100px; height: 30px;');
    }

    /**
     * @param mixed $control
     * @param mixed $error
     * @param mixed $params
     * @return bool
     */
    public function Callback_validRange($control, $error, $params)
    {
        $form = $control->owner;
        $annualProfit = $form->getValue('annualProfit');
        if ($annualProfit >= 1 && $annualProfit < 10000000000) {
            return TRUE;
        }
        return $error;
    }
}

