<?php

namespace Forms\Front;

use Entities\Customer;
use FForm;
use FormModule\Country;
use Nette\Application\UI\Control;
use SagePayDirect;

class EditCardForm extends FForm
{

    /**
     * @var Customer 
     */
    private $customer;

    /**
     * @var bool
     */
    protected $storable = FALSE;

    /**
     * @param Customer $customer
     */
    public function startup(Customer $customer)
    {
        $this->customer = $customer;
        $this->buildForm();
        $this->start();
    }

    protected function buildForm()
    {
        $this->addText('cardHolder', "Cardholder's Name: *")
            ->addRule(array($this, 'Validator_requiredFields'), "Cardholder's name is required!")
            ->addRule('MaxLength', 'Name must be upto 50 characters', array(50));
        $this->addSelect('cardType', 'Card Type: *', SagePayDirect::getCardTypes())
            ->setFirstOption('--- Select ---')
            ->addRule(array($this, 'Validator_requiredFields'), "Card type is required!");
        $this->addText('cardNumber', 'Card Number: *')
            ->addRule(array($this, 'Validator_requiredFields'), 'Card number is required!')
            ->addRule('MaxLength', 'Too long card number. Do NOT enter spaces.', array(20));
        $this->addText('issueNumber', 'Issue Number: ')
            ->size(1)
            ->addRule(array($this, 'Validator_IssueNumber'), 'Issue number or Valid from is required!')
            ->addRule('MaxLength', 'Issue number must be upto 2 characters', array(2));
        $this->addText('CV2', 'Security Code: *')
            ->size(4)
            ->addRule(array($this, 'Validator_securityCode'), "Security code must be a three digit number!")
            ->addRule(array($this, 'Validator_securityCodeAmex'), "Security code must be a four digit number!");
        $this->add('CardExpiryDateNew', 'expiryDate', 'Expiry Date: *')
            ->addRule('Required', 'Expiry date is required!')
            ->addRule('DateMin', "Expiry date has to be more than or equal to today's date!", array('today', 'my'));
        $this->add('CardValidFromDateNew', 'validFrom', 'Valid from: ')
            ->addRule(array($this, 'Validator_IssueNumber'), 'Issue number or Valid from is required!');
        $this->addText('firstName', 'Firsname: *')
            ->addRule(array($this, 'Validator_requiredFields'), 'Firstname is required!')
            ->addRule('MaxLength', 'Firstname must be upto 20 characters', array(20))
            ->setValue($this->customer ? $this->customer->firstName : NULL);
        $this->addText('lastName', 'Lastname: *')
            ->addRule(array($this, 'Validator_requiredFields'), 'Lastname is required!')
            ->addRule('MaxLength', 'Lastname must be upto 20 characters', array(20))
            ->setValue($this->customer ? $this->customer->lastName : NULL);
        $this->addText('address1', 'Address 1: *')
            ->addRule(array($this, 'Validator_requiredFields'), 'Address is required!')
            ->addRule('MaxLength', 'Address 1 must be upto 100 characters', array(100))
            ->setValue($this->customer ? $this->customer->address1 : NULL);
        $this->addText('address2', 'Address 2: ')
            ->addRule('MaxLength', 'Address 2 must be upto 50 characters', array(50))
            ->setValue($this->customer ? $this->customer->address2 : NULL);
        $this->addText('city', 'City/Town: *')
            ->addRule(array($this, 'Validator_requiredFields'), 'City/Town is required!')
            ->addRule('MaxLength', 'City/Town must be upto 40 characters', array(40))
            ->setValue($this->customer ? $this->customer->city : NULL);
        $this->addText('postcode', 'Post Code: *')
            ->addRule(array($this, 'Validator_requiredFields'), 'Post code is required!')
            ->addRule('MaxLength', 'Post code must be upto 10 characters', array(10))
            ->setValue($this->customer ? $this->customer->postcode : NULL);
        $this->addSelect('country', 'Country: *', Country::getCountries())
            ->setFirstOption('--- Select ---')
            ->style('width: 150px;')
            ->addRule(array($this, 'Validator_requiredFields'), 'Country is required!')
            ->setValue($this->customer ? $this->customer->getCountry2Letter() : NULL);
        $this->addSelect('billingState', 'State: *', SagePayDirect::getStates())
            ->setFirstOption('--- Select ---');
        $this->addSubmit('cancel', 'Cancel')
            ->class('cancel');
        $this->addSubmit('submit', 'Save')
            ->class('save');
    }

    /**
     * Check required issue number for maestro and solo
     * @param object $control
     * @param string $error
     * @param mixed $params
     * @return mixed
     */
    public function Validator_IssueNumber($control, $error, $params)
    {
        $form = $control->owner;
        $cardType = strtolower($form['cardType']->getValue());
        $issue = $form['issueNumber']->getValue();
        $validFrom = $form['validFrom']->getValue();
        if (($cardType == 'maestro' || $cardType == 'solo') && empty($issue) && empty($validFrom)) {
            return $error;
        }
        return TRUE;
    }


    /**
     * Check required fields fo card payment
     *
     * @param object $control
     * @param string $error
     * @param array $params
     * @return mixed
     */
    public function Validator_requiredFields($control, $error, $params)
    {
        if ($control->owner->isSubmitedBy('submit') && !isset($control->owner['credit']) || $control->owner->isSubmitedBy('submit') && isset($control->owner['credit']) && $control->owner['credit']->getValue() != 1) {
            $value = $control->getValue();
            if (empty($value)) {
                return $error;
            }
        }
        return TRUE;
    }

    /**
     *
     * @param object $control
     * @param string $error
     * @param array $params
     * @return boolean 
     */
    public function Validator_securityCode($control, $error, $params)
    {
        if ($control->owner->isSubmitedBy('submit') && !isset($control->owner['credit']) || $control->owner->isSubmitedBy('submit') && isset($control->owner['credit']) && $control->owner['credit']->getValue() != 1) {
            $value = $control->getValue();
            $cardType = $control->owner['cardType']->getValue();
            if ((empty($value)
                || !is_numeric($value)
                || strlen($value) != 3 && $cardType != 'AMEX')
            ) {
                return $error;
            }
        }
        return TRUE;
    }

    /**
     * @param Control $control
     * @param string $error
     * @param array $params
     * @return boolean
     */
    public function Validator_securityCodeAmex($control, $error, $params)
    {
        if ($control->owner->isSubmitedBy('submit') && !isset($control->owner['credit']) || $control->owner->isSubmitedBy('submit') && isset($control->owner['credit']) && $control->owner['credit']->getValue() != 1) {
            $cardType = $control->owner['cardType']->getValue();
            $value = $control->getValue();
            if (empty($value)
                || !is_numeric($value)
                || (strlen($value) != 4 && $cardType == 'AMEX')
            ) {
                return $error;
            }
        }
        return TRUE;
    }
}
