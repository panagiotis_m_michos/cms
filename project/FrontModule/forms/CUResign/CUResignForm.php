<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS
 * @author 		Nikolai Senkevich
 * @internal 	project\forms\Front\CUResign\CUResignForm.php
 * @created 	03/05/2012
 */
class CUResignForm extends FForm
{

    /**
     * @var CUSummaryControler
     */
    private $controler;
    
    /**
     * @var array
     */
    private $callback;
    
    /**
     * @var Company
     */
    private $company;
    
    /**
     * @var Object
     */
    private $officer;

    /**
     * @param CUSummaryControler $controler
     * @param array $callback
     */
    public function startup(CUSummaryControler $controler, Company $company, $officer, array $callback)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->company = $company;
        $this->officer = $officer;
        $this->init();
    }

    private function init()
    {
        $this->addFieldset('Date');
        $this->add('DatePicker', 'date', 'Resignation Date:')
            ->addRule(FForm::Required, 'Please provide date!.')
            ->addRule(array($this, 'Validator_futureDate'), 'Cannot be a future date.')
            ->class('date')->setValue(date('Y-m-d'));
        
        $this->addFieldset('Action');
        $this->addSubmit('submit', 'Submit')
            ->class('btn_submit fright mbottom');
            //->onclick('return confirm("Are you sure?")');

        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();
            $this->company->sendOfficerResignationNew($this->officer, $data['date']);
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    /**
     *
     * @param DatePicker $control
     * @param type $error
     * @param type $params
     * @return type 
     */
    public function Validator_futureDate($control, $error, $params)
    {
        if(strtotime ($control->owner['date']->getValue()) > strtotime (date('Y-m-d'))){
            return $error;
        }
        return TRUE;
    }
}