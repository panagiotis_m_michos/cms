<?php

namespace Front\Payment;

use Exception;

interface IOrderSummaryFormDelegate
{
    /**
     * @param $productKey
     * @param OrderSummaryForm $form
     * @return mixed
     */
    public function summaryFormImport($productKey, OrderSummaryForm $form);

    /**
     * @param $productKey
     * @param OrderSummaryForm $form
     * @return mixed
     */
    public function summaryFormEditImport($productKey, OrderSummaryForm $form);

    public function summaryFormRemovedVoucher();

    /**
     * @param array $linkPackageVariable
     */
    public function summaryFormPayment(array $linkPackageVariable);

    public function summaryFormAddVoucher();

    public function summaryFormBasketPage();

    /**
     * @param Exception $e
     */
    public function summaryFormError(Exception $e);
}
