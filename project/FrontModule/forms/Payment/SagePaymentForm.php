<?php

use Entities\Payment\Token;
use Exceptions\Business\CompanyImportException;
use Services\CompanyService;
use ValueObject\Country;

class SagePaymentForm extends FForm
{

    /**
     * @var array
     */
    public static $types = array(
        Transaction::TYPE_SAGEPAY => 'Visa, Mastercard, Maestro, Solo',
        Transaction::TYPE_PAYPAL => 'Paypal, Amex',
    );

    /**
     * @var Basket
     */
    public $basket;

    /**
     * @var Token
     */
    public $customerToken;

    /**
     * @var bool
     */
    protected $storable = FALSE;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var CFCompanyCustomerControler
     */
    private $controler;

    /**
     * @var array
     */
    private $callback;

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @param PaymentControler $controler
     * @param array $callback
     * @param Basket $basket
     * @param Customer $customer
     * @param CompanyService $companyService
     * @param Token $customerToken
     */
    public function startup(
        PaymentControler $controler,
        array $callback,
        Basket $basket,
        Customer $customer,
        CompanyService $companyService,
        Token $customerToken = NULL
    )
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->basket = $basket;
        $this->customer = $customer;
        $this->companyService = $companyService;
        $this->customerToken = $customerToken;
        $this->init();
    }

    private function init()
    {

        $this->setAction($this->controler->router->link('PaymentNewCustomerControler::SAGE_NEW_CUSTOMER_PAGE') . '#authFormBlock');
        $this->addFieldset('Preferred contact details');

        //if for new customers
        if ($this->customer->isNew()) {
            $this->addText('emails', 'Email: *')
                ->addRule(FForm::Required, 'Please provide e-mail address!')
                ->addRule(FForm::Email, 'Email is not valid!')
                ->addRule(array($this, 'Validator_uniqueLogin'), 1)
                ->class('text250');
        }

        //if is not a free basket 
        if (!$this->basket->isFreeBasket()) {
            //on account payment
            $this->addFieldset('On Account');
            $creditCheckBox = $this->addCheckbox('credit', 'Use Credit', 1)
                ->class('on-account-payment')
                ->{'data-price'}($this->basket->getPrice('total'))
                ->{'data-credit'}($this->customer->credit);
            if ($this->basket->isUsingCredit()) {
                $creditCheckBox->checked("checked")
                    ->{'data-price'}($this->basket->getPrice('total') + $this->basket->getPrice('account'));
            }
                //->addRule(
                //    array($this, 'Validator_wholesalerCredit'),
                //    'You do not have enough credit to make this purchase. Please contact your wholesale agent.',
                //    array(
                //        'amount' => $this->basket->getPrice('total'),
                //        'credit' => $this->customer->credit
                //    )
                //);

            //payment methods
            $this->addFieldset('Payment Methods');

            $paymentOptions = array();
            if (empty($this->customerToken)) {
                $paymentOptions["Credit or Debit Cards"] = array("0" => "Pay using Credit/Debit Card");
            } else {
                $paymentOptions["Credit or Debit Cards"] = array(
                    $this->customerToken->getId() => $this->customerToken->getCardType() . " ending in " . $this->customerToken->getCardNumber(), 
                    "one_off_card" => "Use a different card"
                );
            }
            $paymentOptions["Other Payment Methods"] = array("paypal" => "Pay using Paypal");

            $this->addSelect('selectedMethod', '', $paymentOptions);

            //amount
            $this->addText('amount', 'Amount')
                ->setValue($this->basket->getPrice('total'))
                ->class('readonlyInput bold')
                ->readonly();
            $this->addText('cardholder', "Cardholder's Name: *")
                ->addRule(array($this, 'Validator_requiredFields'), "Cardholder's name is required!")
                ->addRule('MaxLength', 'Name must be upto 50 characters', array(50));
            $this->addSelect('cardType', 'Card Type: *', SagePayDirect::getCardTypes())
                ->setFirstOption('--- Select ---')
                ->addRule(array($this, 'Validator_requiredFields'), "Card type is required!");
            $this->addText('cardNumber', 'Card Number: *')
                ->addRule(array($this, 'Validator_requiredFields'), 'Card number is required!')
                ->addRule('MaxLength', 'Too long card number. Do NOT enter spaces.', array(20));
            $this->addText('issueNumber', 'Issue Number: ')
                ->size(1)
                ->addRule(array($this, 'Validator_IssueNumber'), 'Issue number or Valid from is required!')
                ->addRule('MaxLength', 'Issue number must be upto 2 characters', array(2));
            $this->addText('CV2', 'Security Code: *')
                ->size(3)
                ->addRule(array($this, 'Validator_securityCode'), "Security code must be a three digit number!")
                ->addRule(array($this, 'Validator_securityCodeAmex'), "Security code must be a four digit number!");
            $this->add('CardExpiryDate', 'expiryDate', 'Expiry Date: *')
                ->addRule(array($this, 'Validator_ExpiryDate'), array('Expiry date is required!', "Expiry date has to be more than or equal to today's date!"));
            $this->add('CardValidFromDate', 'validFrom', 'Valid from: ')
                ->addRule(array($this, 'Validator_IssueNumber'), 'Issue number or Valid from is required!');
            $this->addText('address1', 'Address 1: *')
                ->addRule(array($this, 'Validator_requiredFields'), 'Address is required!')
                ->addRule('MaxLength', 'Address 1 must be upto 100 characters', array(100))
                ->setValue(!$this->customer->isNew() ? $this->customer->address1 : NULL);
            $this->addText('address2', 'Address 2: ')
                ->addRule('MaxLength', 'Address 2 must be upto 50 characters', array(50))
                ->setValue(!$this->customer->isNew() ? $this->customer->address2 : NULL);
            $this->addText('address3', 'Address 3: ')
                ->addRule('MaxLength', 'Address 3 must be upto 50 characters', array(50))
                ->setValue(!$this->customer->isNew() ? $this->customer->address3 : NULL);
            $this->addText('town', 'Town: *')
                ->addRule(array($this, 'Validator_requiredFields'), 'Town is required!')
                ->addRule('MaxLength', 'Town must be upto 40 characters', array(40))
                ->setValue(!$this->customer->isNew() ? $this->customer->city : NULL);
            $this->addText('postcode', 'Post Code: *')
                ->addRule(array($this, 'Validator_requiredFields'), 'Post code is required!')
                ->addRule('MaxLength', 'Post code must be upto 10 characters', array(10))
                ->setValue(!$this->customer->isNew() ? $this->customer->postcode : NULL);
            $this->addSelect('country', 'Country: *', WPDirect::$countries)
                ->setFirstOption('--- Select ---')
                ->style('width: 150px;')
                ->addRule(array($this, 'Validator_requiredFields'), 'Country is required!')
                ->setValue(!$this->customer->isNew() ? $this->controler->node->getCustomerPaymentCountry($this->customer) : NULL);
            $this->addSelect('billingState', 'State: *', SagePayDirect::getStates())
                ->setFirstOption('--- Select ---');
            $defaultPaymentType = 0;
        } else {
            $defaultPaymentType = Transaction::TYPE_FREE;
        }

        $this->addHidden('paymentType', $defaultPaymentType);
        $this->addHidden('onAccountPayment', Transaction::ON_ACCOUNT);
        $this->addCheckbox('tokenPayment', 'Use Token payment', 1);

        $this->addFieldset('Terms and Conditions');
        $this->addCheckbox('terms', 'Terms and Conditions', 1)
            ->addRule(FForm::Required, 'Please accept Terms and Conditions!');

        $this->addFieldset('Action');
        $this->addSubmit('submit', 'Submit')->class('btn_submit');

        $this->beforeValidation = array($this, 'beforeValidation');
        //$this->onValid = $this->callback;
        $this->start();
        $this->savePaymentFormErrors($this);
    }

    /**
     * @throws CompanyImportException
     */
    public function beforeValidation()
    {
        $conflictCompanies = array();
        /** @var BasketProduct $item */
        foreach ($this->basket->getItems() as $item) {
            if ($item->isLinkedToNewCompany()) {
                $company = $this->companyService->getCompanyByCompanyNumber($item->companyNumber);
                if ($company && $company->getCustomer()->getCustomerId() != $this->customer->getId()) {
                    $conflictCompanies[] = $company;
                }
            }
        }

        if (!empty($conflictCompanies)) {
            throw CompanyImportException::companiesAlreadyExist($conflictCompanies);
        }
    }

    /**
     * Validate postcode for UK
     *
     * @param Text $control
     * @param string $error
     * @param array $params
     * @return boolean
     */
    public static function Validator_postcode(Text $control, $error, $params)
    {
        $value = $control->getValue();
        if ((!preg_match('#^(GIR 0AA|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]|[A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y]))|[0-9][A-HJKPS-UW]) [0-9][ABD-HJLNP-UW-Z]{2})$#i', $value) || !preg_match('#^([A-Z][\dA-Z]{1,3}[\s][\d][A-Z]{2,2})$#i', $value))) {
            return $error;
        }
        return TRUE;
    }

    /*	 * ******************************** validators ******************************** */

    /**
     * Check required issue number for maestro and solo
     * @param object $control
     * @param string $error
     * @param mixed $params
     * @return mixed
     */
    public function Validator_IssueNumber($control, $error, $params)
    {
        $form = $control->owner;
        $cardType = strtolower($form['cardType']->getValue());
        $issue = $form['issueNumber']->getValue();
        $validFrom = $form['validFrom']->getValue();
        if (($cardType == 'maestro' || $cardType == 'solo') && empty($issue) && empty($validFrom)) {
            return $error;
        }
        return TRUE;
    }

    /**
     * Check required issue number for maestro and solo
     * @param object $control
     * @param string $error
     * @param mixed $params
     * @return mixed
     */
    public function Validator_ExpiryDate($control, $error, $params)
    {
        if ($control->owner->isSubmited() && (!isset($control->owner['credit']) || isset($control->owner['credit']) && $control->owner['credit']->getValue() != 1)) {
            if ($control->owner->isSubmited() && ($control->owner['selectedMethod']->getValue() == '0' || $control->owner['selectedMethod']->getValue() == 'one_off_card')) {
                // required
                if ($control->getValue() == NULL) {
                    return $error[0];
                }
                // greater than
                $expireDate = $control->owner['expiryDate']->getValue();
                if (strtotime($expireDate) < strtotime(date("Y-m"))) {
                    return $error[1];
                }
            }
        }
        return TRUE;
    }

    /**
     * Check required fields fo card payment
     *
     * @param object $control
     * @param string $error
     * @param array $params
     * @return mixed
     */
    public function Validator_requiredFields($control, $error, $params)
    {
        if ($control->owner->isSubmited() && (!isset($control->owner['credit']) ||  isset($control->owner['credit']) && $control->owner['credit']->getValue() != 1)) {
            if ($control->owner->isSubmited() && ($control->owner['selectedMethod']->getValue() == '0' || $control->owner['selectedMethod']->getValue() == 'one_off_card')) {
                $value = $control->getValue(); 
                if (empty($value)) {
                    return $error;
                }
            }
        }
        return TRUE;
    }

    /**
     * Check enough credit for pay by credit
     *
     * @param object $control
     * @param Checkbox $error
     * @param array $params
     * @return mixed
     */
    //public function Validator_wholesalerCredit(Checkbox $control, $error, $params)
    //{
    //    if ($control->owner->isSubmitedBy('submit') && $control->getValue() == 1 && $params['amount'] > $params['credit']) {
    //        return $error;
    //    }
    //    return TRUE;
    //}

    /**
     *
     * @param type $control
     * @param type $error
     * @param type $params
     * @return boolean
     */
    public function Validator_securityCode($control, $error, $params)
    {
        if ($control->owner->isSubmited() && (!isset($control->owner['credit']) || isset($control->owner['credit']) && $control->owner['credit']->getValue() != 1)) {
            if ($control->owner->isSubmited() && ($control->owner['selectedMethod']->getValue() == '0' || $control->owner['selectedMethod']->getValue() == 'one_off_card')) {
                $cardType = $control->owner['cardType']->getValue();
                $value = $control->getValue();
                if ($cardType != 'AMEX'
                    && (empty($value)
                        || !is_numeric($value)
                        || (strlen($value) != 3))
                ) {
                    return $error;
                }
            }
        }
        return TRUE;
    }

    /**
     *
     * @param Control $control
     * @param string $error
     * @param array $params
     * @return boolean
     */
    public function Validator_securityCodeAmex($control, $error, $params)
    {
        if ($control->owner->isSubmited() && (!isset($control->owner['credit']) || isset($control->owner['credit']) && $control->owner['credit']->getValue() != 1)) {
            if ($control->owner->isSubmited() && ($control->owner['selectedMethod']->getValue() == '0' || $control->owner['selectedMethod']->getValue() == 'one_off_card')) {
                $cardType = $control->owner['cardType']->getValue();
                $value = $control->getValue();
                if (empty($value)
                    || !is_numeric($value)
                    || (strlen($value) != 4 && $cardType == 'AMEX')
                ) {
                    return $error;
                }
            }
        }
        return TRUE;
    }

    /**
     * Validator
     * @param object $control
     * @param mixed $error
     * @param mixed $params
     * @return mixed
     */
    public function Validator_uniqueLogin($control, $error, $params)
    {
        $customerId = FApplication::$db->select('customerId')->from(TBL_CUSTOMERS)->where('email=%s', $control->getValue())->execute()->fetchSingle();
        if ($customerId) return $error;
        return TRUE;
    }

    public function savePaymentFormErrors($form)
    {
        $errors = $form->getErrors();
        if (!empty($errors)) {
            $paymentFormLog = new PaymentFormLogs();
            $values = $this->getValues();
            $paymentFormLog->email =  isset($values['emails']) ? $values['emails']: NULL;
            $paymentFormLog->customerId = $this->customer->getId();
            $paymentFormLog->cardHolder = isset($errors['cardholder']) ? $errors['cardholder'] : NULL;
            $paymentFormLog->cardNumber = isset($errors['cardNumber']) ? $errors['cardNumber'] : NULL;
            $paymentFormLog->cardType = isset($errors['cardType']) ? $errors['cardType'] : NULL;
            $paymentFormLog->expiryDate = isset($errors['expiryDate']) ? $errors['expiryDate'] : NULL;
            $paymentFormLog->securityCode = isset($errors['CV2']) ? $errors['CV2'] : NULL;
            $paymentFormLog->address1 = isset($errors['address1']) ? $errors['address1'] : NULL;
            $paymentFormLog->town = isset($errors['town']) ? $errors['town'] : NULL;
            $paymentFormLog->postCode = isset($errors['postcode']) ? $errors['postcode'] : NULL;
            $paymentFormLog->country = isset($errors['country']) ? $errors['country'] : NULL;
            $paymentFormLog->termscond = isset($errors['terms']) ? $errors['terms'] : NULL;
            $paymentFormLog->save();
        }
    }

}
