<?php

namespace Front\Payment;

use Basket;
use Basket\PackageToPackageDowngradeNotification;
use Basket\PackageToProductDowngradeNotification;
use BasketProduct;
use Exception;
use FForm;
use Package;
use VoucherNew;

class OrderSummaryForm extends FForm
{

    /**
     * @var IOrderSummaryFormDelegate
     */
    private $delegate;

    /**
     * @var Basket
     */
    private $basket;

    /**
     * @var array
     */
    private $customerCompanies;

    /**
     * @var array
     */
    private $customerIncCompanies;

    /**
     * @var PackageToProductDowngradeNotification
     */
    private $packageToProductDowngradeNotification;

    /**
     * @var PackageToPackageDowngradeNotification
     */
    private $packageToPackageDowngradeNotification;

    /**
     * @param IOrderSummaryFormDelegate $delegate
     * @param Basket $basket
     * @param array $customerCompanies
     * @param array $customerIncCompanies
     * @param PackageToProductDowngradeNotification|NULL $packageToProductDowngradeNotification
     * @param PackageToPackageDowngradeNotification|NULL $packageToPackageDowngradeNotification
     */
    public function startup(
        IOrderSummaryFormDelegate $delegate,
        Basket $basket,
        array $customerCompanies,
        array $customerIncCompanies,
        PackageToProductDowngradeNotification $packageToProductDowngradeNotification = NULL,
        PackageToPackageDowngradeNotification $packageToPackageDowngradeNotification = NULL
    )
    {
        $this->delegate = $delegate;
        $this->basket = $basket;
        $this->customerCompanies = $customerCompanies;
        $this->customerIncCompanies = $customerIncCompanies;
        $this->packageToProductDowngradeNotification = $packageToProductDowngradeNotification;
        $this->packageToPackageDowngradeNotification = $packageToPackageDowngradeNotification;

        $formValues = array();
        /** @var BasketProduct $item */
        foreach ($this->basket->getItems() as $key => $item) {
            $selectValue = NULL;

            if ($item->isLinkedToExistingCompany()) {
                $selectValue = $item->getCompanyId();
            } elseif ($item->isLinkedToNewCompany()) {
                $selectValue = 'imported_' . $item->companyNumber;
            }

            $formValues[$key] = array(
                "radio" => ($item->isLinkedToImportedCompany() ? 'import' : 'existing'),
                "select" => $selectValue,
                "text" => $item->isLinkedToImportedCompany() ? $item->companyNumber : NULL,
            );
        }

        $this->buildForm();
        $this->onValid = array($this, 'process');
        $this->setInitValues($formValues);
        $this->start();
    }

    public function process()
    {
        try {
            $this->saveSummaryFormData();

            if ($this->isSubmitedByMatch('^import_')) {
                $submitName = $this->getSubmitNameByMatch('^import_');
                list(, $productKey) = explode('_', $submitName);
                $this->delegate->summaryFormImport($productKey, $this);
            } elseif ($this->isSubmitedByMatch('^edit_')) {
                $submitName = $this->getSubmitNameByMatch('^edit_');
                list(, $productKey) = explode('_', $submitName);
                $this->delegate->summaryFormEditImport($productKey, $this);
            } elseif ($this->isSubmitedBy('removeVoucher')) {
                $this->delegate->summaryFormRemovedVoucher();
            } elseif ($this->isSubmitedBy('addVoucher')) {
                $this->delegate->summaryFormAddVoucher();
            } elseif ($this->isSubmitedBy('payment')) {
                $linkPackageVariable = NULL;
                foreach ($this->basket->items as $item) {
                    if ($item instanceof Package && isset(Package::$types[$item->getId()])) {
                        $linkPackageVariable = array('package' => str_replace(' ', '', strtolower(Package::$types[$item->getId()])));
                    }
                }
                $this->clean();
                $this->delegate->summaryFormPayment($linkPackageVariable);
            } elseif ($this->isSubmitedBy('basket')) {
                $this->delegate->summaryFormBasketPage();
            }
        } catch (Exception $e) {
            $this->delegate->summaryFormError($e);
        }
    }

    public function saveSummaryFormData()
    {
        $data = $this->getValues();

        if ($this->isSubmitedBy('removeVoucher')) {
            $this->basket->removeVoucher();

        } elseif ($this->isSubmitedBy('addVoucher')
            || $this->isSubmitedBy('payment')
            || $this->isSubmitedBy('companySelectChanged')
            || $this->isSubmitedByMatch('^import_')
        ) {
            if (isset($data['companyNumbers'])) {
                $this->basket->updateCompanyNumbers($data['companyNumbers'], $this->customerCompanies);
            }

            // voucher
            if (isset($data['voucherCode']) && !empty($data['voucherCode'])) {
                $voucher = VoucherNew::getByCode($data['voucherCode']);
                if ($voucher) {
                    $this->basket->setVoucher($voucher->getId());
                }
            }
        }
    }

    private function buildForm()
    {
        // show just if package is not included
        if (!$this->basket->hasPackageIncluded()) {
            if ($this->basket->getItemsCount() > 0 && $this->basket->hasItemsRequiringCompanyNumber()) {
                /** @var BasketProduct $item */
                foreach ($this->basket->items as $key => $item) {
                    if ($item->requiredCompanyNumber) {
                        $this->add('SummaryProductControl', $key, 'Choose company name or enter company number manually: ')
                            ->setGroup('companyNumbers')
                            ->setOptions($item->requiredIncorporatedCompanyNumber ? $this->customerIncCompanies : $this->customerCompanies)
                            ->setProduct($item)
                            ->addRule(
                                array('SummaryProductControl', 'Validator_summaryReq'),
                                array('Please select company', 'Please import company.'),
                                array($this->basket->items)
                            );
                    }
                }
            }
        }

        // voucher
        if ($this->basket->voucher == NULL) {
            $this->addText('voucherCode', 'Enter voucher code:')
                ->addRule(array('VoucherNew', 'Validator_VoucherCode'), array('Voucher code is not valid', 'Minimum spend must be greater than £%s!'), array('basket' => $this->basket));
        } else {
            $this->addSubmit('removeVoucher', 'Remove')
                ->class('linkButton');
        }

        $this->addSubmit('addVoucher', 'add voucher')
            ->class('linkButton');
        $this->addSubmit('basket', 'Back to Basket')
            ->class('btn_submit mtop fleft clear');

        if ($this->packageToProductDowngradeNotification || $this->packageToPackageDowngradeNotification) {
            $this->addSubmit('payment', 'Checkout')
                ->disabled();
        } else {
            $this->addSubmit('payment', 'Checkout')
                ->class('btn_submit');
        }
    }
}
