<?php

namespace Front;

use Customer;
use dibi;
use Entities\Customer as CustomerEntity;
use FControl;
use FForm;
use Text;

class MyDetailsForm extends FForm
{
    /**
     * @var CustomerEntity
     */
    private $customer;

    /**
     * @var array
     */
    private $countries;

    /**
     * @var array
     */
    private $titles;

    /**
     * @var array
     */
    private $howHeards;

    /**
     * @var array
     */
    private $industries;

    /**
     * @param CustomerEntity $customer
     * @param array $titles
     * @param array $countries
     * @param array $howHeards
     * @param array $industries
     */
    public function startup(CustomerEntity $customer, array $titles, array $countries, array $howHeards, array $industries)
    {
        $this->customer = $customer;
        $this->titles = $titles;
        $this->countries = $countries;
        $this->howHeards = $howHeards;
        $this->industries = $industries;

        $this->buildForm();
        $this->start();
    }

    /**
     * @param Text $control
     * @param string $error
     * @return bool
     */
    public function Validator_postcode(Text $control, $error)
    {
        $value = $control->getValue();
        $countryId = $control->owner['countryId']->getValue();
        if ($countryId == Customer::UK_CITIZEN && (!preg_match('#^(GIR 0AA|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]|[A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y]))|[0-9][A-HJKPS-UW]) [0-9][ABD-HJLNP-UW-Z]{2})$#i', $value) || !preg_match('#^([A-Z][\dA-Z]{1,3}[\s][\d][A-Z]{2,2})$#i', $value))) {
            return $error;
        }
        return TRUE;

    }

    /**
     * @param FControl $control
     * @param string $error
     * @return bool
     */
    public function Validator_uniqueLogin(FControl $control, $error)
    {
        $customerId = dibi::select('customerId')
            ->from(TBL_CUSTOMERS)
            ->where('email=%s', $control->getValue())
            ->and('customerId!=%s', $this->customer->getId())
            ->fetchSingle();

        if ($customerId) {
            return $error;
        }
        return TRUE;
    }

    private function buildForm()
    {
        if ($this->customer->hasCompletedRegistration()) {
            $this->addFieldset('Login details');
            $this->addText('email', 'Email <span class="orange">*</span>')
                ->class('field220')
                ->addRule(self::Required, 'Please provide e-mail address!')
                ->addRule(self::Email, 'Email is not valid!')
                ->addRule(array($this, 'Validator_uniqueLogin'), 'Email address has been taken.')
                ->setValue($this->customer->getEmail());
            $this->addPassword('password', 'New Password <span class="orange">*</span>')
                ->setDescription('Passwords are case sensitive and must be a minimum of 6 characters. Leave blank to keep your existing password.')
                ->class('field220');
            $this->addPassword('passwordConf', 'Confirm New Password <span class="orange">*</span>')
                ->class('field220')
                ->addRule(self::Equal, 'Passwords are not the same!', 'password');
        }

        $this->addFieldset('Personal details');
        $this->addSelect('titleId', 'Title &nbsp;&nbsp;', $this->titles)
            ->class('field220')
            ->setFirstOption('--- Select ---')
            ->setValue($this->customer->getTitleId());
        $this->addText('firstName', 'First Name <span class="orange">*</span>')
            ->class('field220')
            ->addRule(self::Required, 'Please provide firstname!')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->setValue($this->customer->getFirstName());
        $this->addText('lastName', 'Last Name  <span class="orange">*</span>')
            ->class('field220')
            ->addRule(self::Required, 'Please provide lastname!')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->setValue($this->customer->getLastName());
        $this->addText('companyName', 'Company name')
            ->class('field220')
            ->setDescription('The name of the company you work for. Leave blank if not applicable.')
            ->setValue($this->customer->getCompanyName());

        $this->addFieldset('Postal Address');
        $this->addText('address1', 'Address 1  <span class="orange">*</span>')
            ->class('field220')
            ->addRule(self::Required, 'Please provide address 1!')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule(self::MAX_LENGTH, 'Address 1 cannot be longer than #1 characters', 50)
            ->setValue($this->customer->getAddress1());
        $this->addText('address2', 'Address 2 &nbsp;&nbsp;')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule(self::MAX_LENGTH, 'Address 2 cannot be longer than #1 characters', 50)
            ->class('field220')
            ->setValue($this->customer->getAddress2());
        $this->addText('address3', 'Address 3 &nbsp;&nbsp;')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule(self::MAX_LENGTH, 'Address 3 cannot be longer than #1 characters', 50)
            ->class('field220')
            ->setValue($this->customer->getAddress3());
        $this->addText('city', 'City  <span class="orange">*</span>')
            ->class('field220')
            ->addRule(self::Required, 'Please provide city!')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule(self::MAX_LENGTH, 'City cannot be longer than #1 characters', 50)
            ->setValue($this->customer->getCity());
        $this->addText('county', 'County &nbsp;&nbsp;')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule(self::MAX_LENGTH, 'County cannot be longer than #1 characters', 50)
            ->class('field220')
            ->setValue($this->customer->getCounty());
        $this->addText('postcode', 'Postcode  <span class="orange">*</span>')
            ->class('field220')
            ->addRule(self::Required, 'Please provide post code!')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule(array($this, 'Validator_postcode'), 'Postcode is not valid')
            ->addRule(self::REGEXP, 'You cannot use our postcode for this address', array('#(ec1v 4pw|ec1v4pw)#i', TRUE))
            ->addRule(self::MAX_LENGTH, 'Postcode cannot be longer than #1 characters', 50)
            ->setDescription('If UK postcode please add 1 space in the middle')
            ->setValue($this->customer->getPostcode());
        $this->addSelect('countryId', 'Country  <span class="orange">*</span>', $this->countries)
            ->class('field220')
            ->setFirstOption('--- Select ---')
            ->addRule(self::Required, 'Please provide country!')
            ->setValue($this->customer->getCountryId());
        $this->addText('phone', 'Phone <span class="orange">*</span>')
            ->class('field220')
            ->setDescription('(Only numbers, no spaces, max 14 characters)')
            ->addRule(self::Required, 'Please provide phone!')
            ->addRule(self::REGEXP, 'Phone is not valid.', '#^\d{1,14}$#')
            ->setValue($this->customer->getPhone());
        $this->addText('additionalPhone', 'Mobile / Other Phone')
            ->class('field220')
            ->setDescription('(Only numbers, no spaces, max 14 characters)')
            ->addRule(self::REGEXP, 'Phone is not valid.', '#^\d{1,14}$#')
            ->setValue($this->customer->getAdditionalPhone());

        if ($this->customer->hasWholesaleQuestion()) {
            $this->addFieldset('About me');
            $this->addRadio('userType', 'This company is for <span class="orange">*</span>', array(Customer::ROLE_NORMAL => 'Myself or a friend', Customer::ROLE_WHOLESALE => 'I\'m a professional forming companies on behalf of others'))
                ->setDescription('Your answer helps us optimise your account for your requirements')
                ->setValue('normal')->class('userType')
                ->addRule(self::Required, 'Required!');

            $this->addFieldset('ICAEW member?');
            $this->addRadio('memberICAEW', 'Are you a member of ICAEW? <span class="orange">*</span>', array('yes' => 'Yes', 'no' => 'No'))
                ->class('memberICAEW');
        }

        // other details
        $this->addFieldset('Other details');
        $this->addSelect('howHeardId', 'How did you hear about us?', $this->howHeards)
            ->class('field220')
            ->setFirstOption('--- Select ---')
            ->setValue($this->customer->getHowHeardId());
        $this->addSelect('industryId', 'Industry &nbsp;&nbsp;', $this->industries)
            ->class('field220')
            ->setFirstOption('--- Select ---')
            ->setValue($this->customer->getIndustryId());
        $this->addCheckbox('isSubscribed', 'Email me special offers', 1);

        // action
        $this->addFieldset('Personal');
        $this->addSubmit('login', 'Save')
            ->setDescription('(Please double check all your details before saving them!)')
            ->class('btn_submit');
    }
}
