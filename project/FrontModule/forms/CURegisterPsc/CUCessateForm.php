<?php

class CUCessateForm extends FForm
{
    /**
     * @var callable
     */
    private $callback;

    /**
     * @param callable $callback
     */
    public function startup(callable $callback)
    {
        $this->callback = $callback;
        $this->init();
    }

    private function init()
    {
        $this->addFieldset('Date');
        $this->add('DatePicker', 'date', 'Cessation Date:')
            ->addRule(FForm::Required, 'Please provide date.')
            ->addRule([$this, 'Validator_futureDate'], 'Cannot be a future date.')
            ->addRule([$this, 'Validator_pscDate'], 'The PSC register was introduced on 6 April 2016, therefore cessations cannot be before this date.')
            ->class('date')->setValue(date('Y-m-d'));

        $this->addFieldset('Action');
        $this->addSubmit('submit', 'Submit')
            ->class('btn_submit fright mbottom');

        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @param DatePicker $control
     * @param string $error
     * @return string|bool
     */
    public function Validator_futureDate(DatePicker $control, $error)
    {
        if (strtotime($control->owner['date']->getValue()) > strtotime(date('Y-m-d'))) {
            return $error;
        }

        return TRUE;
    }

    /**
     * @param DatePicker $control
     * @param string $error
     * @return string|bool
     */
    public function Validator_pscDate(DatePicker $control, $error)
    {
        if (strtotime($control->owner['date']->getValue()) < strtotime(date('2016-04-07 00:00:00'))) {
            return $error;
        }

        return TRUE;
    }
}
