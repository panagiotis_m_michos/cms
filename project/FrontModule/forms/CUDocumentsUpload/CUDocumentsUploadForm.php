<?php

class CUDocumentsUploadForm extends FForm
{

    /**
     * @var CUChangeNameControler 
     */
    private $controler;

    /**
     * @var array
     */
    private $callback;

    /**
     * @var Company
     */
    private $company;

    /**
     * @param CUDocumentsControler $controler
     * @param array $callback
     * @param Company $company
     */
    public function startup(CUDocumentsControler $controler, array $callback, Company $company)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->company = $company;
        $this->init();
    }

    /**
     * @return void
     */
    private function init()
    {
        $this->addFieldset("Document Upload");
        $this->add('MultiFile', 'CmsFile', NULL)
            ->addRule(array($this, 'Validator_DocumentRequired'), 'A file is required')
            ->addRule(array($this, 'Validator_multipleDocumentsType'), 'File type not supported')
            ->addRule(array($this, 'Validator_multipleDocumentsSize'), 'File exceeds the max file size of 3MB')
            ->setMaximumRows(CompanyOtherDocument::TYPE_OTHERS_MAX_NUMBER - $this->controler->node->getNumberOfFiles($this->company));
        $this->addFieldset('Action');
        $this->addSubmit('send', 'Upload')->class('btn_submit fright mbottom');

        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process() 
    {
        $data = $this->getValues();
        $data = array_filter($data['CmsFile'], 'strlen');

        foreach ($data as $fileData) {
            $info = pathinfo($fileData->getName());
            $document = new CompanyOtherDocument();
            $document->companyId = $this->company->getCompanyId();
            $document->typeId = CompanyOtherDocument::TYPE_OTHERS;
            $document->name = $info['filename'] . time();
            $document->fileTitle = $info['filename'];
            $document->size = $fileData->getSize();
            $document->ext = String::lower($info['extension']);
            $document->uploadedFile = $fileData;
            $document->save();
        }
        $this->clean();
    }

    /**
     * @param FControl $control
     * @param string $error
     */
    public function Validator_DocumentRequired(FControl $control, $error)
    {
        $values = $control->getValue();
        
        if (!isset($values)) {
            return $error;
        }
        
        $firstFile = $values[1];
        if ($firstFile instanceof HttpUploadedFile && $firstFile->isOk()) {
            return TRUE;
        } else {
            return $error;
        }
    }
    
    
    /**
     * @param FControl $control
     * @param string $error
     */
    public function Validator_multipleDocumentsType(FControl $control, $error)
    {
        $allowed = array('doc', 'docx', 'rtf', 'pdf', 'odt', 'xls', 'xlsx', 'ods');
        $values = $control->getValue();
        /* @var $value HttpUploadedFile */
        foreach ($values as $value) {
            if ($value instanceof HttpUploadedFile && $value->isOk()) {
                $info = pathinfo($value->getName());
                if (!in_array($info['extension'], $allowed)) {
                    return $error;
                }
            }
        }
        return TRUE;
    }

    /**
     * @param FControl $control
     * @param string $error
     */
    public function Validator_multipleDocumentsSize(FControl $control, $error)
    {
        $values = $control->getValue();
        /* @var $value HttpUploadedFile */
        foreach ($values as $value) {
            if ($value instanceof HttpUploadedFile && $value->isOk()) {
                if ($value->getsize() > 3138576) {
                    return $error;
                }
            }
        }
        return TRUE;
    }

}
