<?php

use BankingModule\BankingService;
use BankingModule\Entities\CompanyCustomer;
use Customer as OldCustomer;
use Entities\Company;
use Entities\Customer;
use Utils\Date;

class TsbContactDetailsForm extends FForm
{
    /**
     * @var array
     */
    private $callback;

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var BankingService
     */
    private $bankingService;

    /**
     * @param array $callback
     * @param Customer $customer
     * @param BankingService $bankingService
     * @param bool $isAdmin
     */
    public function startup(array $callback, Customer $customer, BankingService $bankingService, $isAdmin = FALSE)
    {
        $this->callback = $callback;
        $this->customer = $customer;
        $this->bankingService = $bankingService;
        $this->init($isAdmin);
    }

    /**
     * @param bool $isAdmin
     */
    private function init($isAdmin)
    {
        $this->addFieldset('What are the best details for TSB to use to contact you?');
        $this->addText('email', 'Email *')
            ->addRule(FForm::Required, 'Please provide e-mail address!')
            ->addRule(FForm::Email, 'Email is not valid!');

        $this->addSelect('titleId', 'Title &nbsp;&nbsp;', OldCustomer::$titles)
            ->setFirstOption('--- Select ---');

        $this->addText('firstName', 'First Name *')
            ->addRule(FForm::Required, 'Please provide firstname!');

        $this->addText('lastName', 'Last Name  *')
            ->addRule(FForm::Required, 'Please provide lastname!');

        $this->addText('phone', 'Daytime Number *')
            ->setDescription('(The following formats are allowed: 01234567890, +441234567890, 00441234567890)')
            ->addRule(FForm::Required, 'Please provide phone!')
            ->addRule(['CompanyCustomerModel', 'Validator_phonenumber'], 'Phone is not valid.');

        $this->addText('additionalPhone', 'Alternative Number *')
            ->setDescription('(The following formats are allowed: 01234567890, +441234567890, 00441234567890)')
            ->addRule(FForm::Required, 'Please provide phone!')
            ->addRule(['CompanyCustomerModel', 'Validator_phonenumber'], 'Phone is not valid.');

        $this->addSelect('countryId', 'Country *', ['UK' => 'United Kingdom'])
            ->setDescription('(TSB available only for UK residents.)')
            ->addRule(['CompanyCustomerModel', 'Validator_country'], 'TSB available only for UK residents.');;

        if (!$isAdmin) {
            $this->addFieldset('Consent');
            $this->addRadio(
                'consent',
                'Consent',
                [1 => "I consent to my details being sent to TSB", 2 => "I am sending my client's details to TSB and they have given me consent"]
            )
                ->addRule(FForm::Required, 'Please provide consent!');
        } else {
            $this->addHidden('consent', 1);
        }

        $this->addFieldset('Action');
        $this->addSubmit('login', 'Submit')
            ->setDescription('(Please double check all your details before saving them!)')
            ->style('width: 200px; height: 30px');

        $this->setInitValues(
            [
                'email' => $this->customer->getEmail(),
                'titleId' => $this->customer->getTitleId(),
                'firstName' => $this->customer->getFirstName(),
                'lastName' => $this->customer->getLastName(),
                'phone' => $this->customer->getPhone(),
                'additionalPhone' => $this->customer->getAdditionalPhone()
            ]
        );

        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @param Customer $wholesaler
     * @param Company $company
     * @return CompanyCustomer
     */
    public function process(Customer $wholesaler, Company $company)
    {
        $data = $this->getValues();

        $details = new CompanyCustomer();
        $details->setBankTypeId(CompanyCustomer::BANK_TYPE_TSB);
        $details->setPreferredContactDate(new Date());
        $details->setWholesaler($wholesaler);
        $details->setCompany($company);
        $details->setEmail($data['email']);
        $details->setTitleId(OldCustomer::$titles[$data['titleId']]);
        $details->setFirstName($data['firstName']);
        $details->setLastName($data['lastName']);
        $details->setPhone($data['phone']);
        $details->setAdditionalPhone($data['additionalPhone']);
        $details->setConsent($data['consent']);

        $this->bankingService->saveDetails($details);
        $this->clean();
        return $details;
    }
}
