<?php

use BankingModule\Entities\CompanyCustomer as CompanyCustomerEntity;

class CardOneContactDetailsForm extends FForm
{
    /**
     * @var array
     */
    private $callback;

    /**
     * @var Customer
     */
    public $customer;

    /**
     * @param array $callback
     * @param Customer $customer
     */
    public function startup(array $callback, Customer $customer)
    {
        $this->callback = $callback;
        $this->customer = $customer;
        $this->init();
    }

    private function init()
    {
        $this->addFieldset('Preferred contact details');
        $this->addText('email', 'Email: *')
            ->addRule(FForm::Required, 'Please provide e-mail address!')
            ->addRule(FForm::Email, 'Email is not valid!');

        $this->addText('firstName', 'First Name: *')
            ->addRule(FForm::Required, 'Required!');

        $this->addText('lastName', 'Last Name: *')
            ->addRule(FForm::Required, 'Required!');

        $this->addText('phone', 'Daytime Phone *')
            ->setDescription('(Only numbers, no spaces, max 14 characters)')
            ->addRule(FForm::Required, 'Please provide phone!')
            ->addRule(FForm::REGEXP, 'Phone is not valid.', '#^\d{1,14}$#');

        $this->addFieldset('Action');
        $this->addSubmit('submit', 'Submit')
            ->class('btn_submit fright mbottom mright')
            ->style('width: 200px; height: 30px;');

        $data = (array) $this->customer;
        $this->setInitValues($data);

        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @param int $wholesalerId
     * @param int $companyId
     */
    public function process($wholesalerId, $companyId)
    {
        $data = $this->getValues();
        $customer = new CompanyCustomer();
        $customer->bankTypeId = CompanyCustomerEntity::BANK_TYPE_CARD_ONE;
        $customer->preferredContactDate = 0;
        $customer->wholesalerId = $wholesalerId;
        $customer->companyId = $companyId;
        $customer->email = $data['email'];
        $customer->firstName = $data['firstName'];
        $customer->lastName = $data['lastName'];
        $customer->phone = $data['phone'];

        $customer->postcode = 'NULL';
        $customer->address1 = 'NULL';
        $customer->address3 = 'NULL';
        $customer->city = 'NULL';
        $customer->countryId = 'NULL';
        $customer->consent = 0;

        $customer->save();
        $this->clean();
    }
}
