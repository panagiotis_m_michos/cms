<?php

namespace FrontModule\Forms\CompaniesCustomer;

use Search\CompanySearchQuery;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FilterForm extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod('GET')
            ->add(
                'term', 'text', array(
                    'attr' => array(
                        'placeholder' => 'Search by company name or number',
                    ),
                'label' => FALSE,
                'required'  => FALSE,
                )
            )
            ->add(
                'search', 'submit', array(
                    'label' => 'Search'
                )
            )
            ->add(
                'onlyActive', 'checkbox', array(
                    'label'     => 'Display active companies only',
                    'required'  => FALSE,
                )
            )
            ->add(
                'dateFilterType', 'choice', array(
                    'label' => 'Search by date:',
                    'choices' => CompanySearchQuery::$types,
                    'expanded' => FALSE,
                    'multiple' => FALSE,
                    'required'  => FALSE
                )
            )
            ->add(
                'dateFrom', 'date', array(
                    'label' => 'From:',
                    'input'  => 'datetime',
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'required'  => FALSE,
                )
            )
            ->add(
                'dateTo', 'date', array(
                    'label' => 'To:',
                    'input'  => 'datetime',
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'required'  => FALSE,
                )
            )
            ->add(
                'exportCsv', 'submit', array(
                    'label' => 'Export results to CSV'
                )
            );

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'allow_extra_fields' => TRUE
            )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return NULL;
    }
} 