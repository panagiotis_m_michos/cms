<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS
 * @author 		Nikolai Senkevich, Stan Bazik
 * @internal 	project/forms/CompaniesCustomer/CompaniesCustomerListForm.php
 * @created 	21/03/2011
 */
class CompaniesCustomerListForm extends FForm
{

    /**
     * @var CompaniesCustomerControler
     */
    private $controler;
    /**
     * @var array
     */
    private $callback;

    /**
     * @param CompaniesCustomerControler $controler
     * @param array $callback
     */
    public function startup(CompaniesCustomerControler $controler, array $callback)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->init();
    }

    private function init()
    {
        $this->addFieldset('Return Filter');
        $this->add('RadioInline', 'selector', 'Type')
            ->setOptions(array('Accounts' => 'Accounts', 'Return' => 'Return'));

        $this->add('DatePicker', 'dateFrom', 'From')
            ->addRule(FForm::Required, 'Please provide from date!.')
            ->setOption('yearRange', '-111:+2');

        $this->add('DatePicker', 'dateTo', 'To')
            ->addRule(FForm::Required, 'Please provide to date!.')
            ->setOption('yearRange', '-111:+2');

        $this->addSubmit('companyFilter', 'Filter')->class('btn')->style('width:60px');
        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

}