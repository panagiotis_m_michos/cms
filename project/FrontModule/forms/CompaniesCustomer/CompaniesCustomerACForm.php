<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS
 * @author 		Nikolai Senkevich
 * @internal 	project/forms/CompaniesCustomer/CompaniesCustomerACForm.php
 * @created 	16/10/2012
 */
class CompaniesCustomerACForm extends FForm
{

    /**
     * @var CompaniesCustomerControler
     */
    private $controler;
    /**
     * @var array
     */
    private $callback;
    
    /**
     * @var Customer 
     */
    private $customer;

    /**
     * @param CompaniesCustomerControler $controler
     * @param array $callback
     */
    public function startup(CompaniesCustomerControler $controler, array $callback, Customer $customer)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->customer = $customer;
        $this->init();
    }

    private function init()
    {
        $this->addFieldset('Return Filter');
        $activeCompanies = $this->addCheckbox('active', 'Display active companies only', 1);

        if ($this->customer->activeCompanies == 1){
            $activeCompanies->setValue(1);
        }
        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

}