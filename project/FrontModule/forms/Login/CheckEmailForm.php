<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    CheckEmailForm.php 2012-04-12 razvanp@madesimplegroup.com
 */

class CheckEmailForm  extends FForm
{
    /**
     * @var DeleteCompanyControler
     */
    private $controler;
    /**
     * @var array
     */
    private $callback;

    /**
     * @param DeleteCompanyControler $controler
     * @param array $callback
     */
    public function startup(PaymentControler $controler, array $callback)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->init();
    }

    /**
	 * @return void
	 */
    private function init()
    {
        
		$this->addText('email', 'Email address:')
            ->class('field220')
			->addRule(FForm::Required, 'Please provide e-mail address!')
			->addRule('Email', 'Email address is not valid!')
            ->addRule(array($this, 'Validator_uniqueLogin'), 1);
		$this->addSubmit('check', ' Check');
        
        $this->onValid = $this->callback;
	    $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try{
            $this->clean();
            return $this->getValue('email');
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    /**
	 * Validator
	 * @param object $control
	 * @param mixed $error
	 * @param mixed $params
	 * @return mixed
	 */
	public function Validator_uniqueLogin($control, $error, $params)
	{
        $customerId = FApplication::$db->select('customerId')->from(TBL_CUSTOMERS)->where('email=%s', $control->getValue())->execute()->fetchSingle();
		if ($customerId) return $error;
		return TRUE;
	}
    
}
