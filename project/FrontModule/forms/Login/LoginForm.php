<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    LoginForm.php 2012-04-12 razvanp@madesimplegroup.com
 */

class LoginForm  extends FForm
{
    
    /**
     * @var array
     */
    private $callback;

    /**
     * @param DeleteCompanyControler $controler
     * @param array $callback
     */
    public function startup( array $callback)
    {
        
        $this->callback = $callback;
        $this->init();
    }

    /**
	 * @return void
	 */
    private function init()
    {
        
		$this->addText('email', 'Email address:')
            ->class('field220')
			->addRule(FForm::Required, 'Please provide e-mail address!')
			->addRule('Email', 'Email address is not valid!');
		$this->addPassword('password', 'Password:')
            ->class('field220')
			->addRule(FForm::Required, 'Please provide password!')
			->addRule(array($this, 'Validator_loginCredentials'), array(1=>"Email not found!","Password doesn't match!","Your account haven't been validated!","Your account is blocked. Please contact our customer supoort line on 0207 608 5500."));
		$this->addSubmit('login', 'Login')
            ->class('btn_submit');
		
        $this->onValid = $this->callback;
	    $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();
            $deletedCompanies = new DeleteCompany();
            $deletedCompanies->customerId  =  $this->controler->customer->customerId;
            $deletedCompanies->companyId  =  $this->controler->company->getCompanyId();
            $deletedCompanies->customerName = $this->controler->customer->firstName . ' ' . $this->controler->customer->lastName;
            $deletedCompanies->companyNumber = $this->controler->company->getCompanyNumber();
            $deletedCompanies->companyName = $this->controler->company->getCompanyName();
            $deletedCompanies->rejectMessage = $data['message'];
            $id = $deletedCompanies->save();
            
            if ($id){
                $this->controler->company->setDeletedCompanyNumber($this->controler->company->getCompanyNumber());
                $this->controler->company->markDeleted();
            }
            
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    /**
	 * Validator
	 * @param object $control
	 * @param mixed $error
	 * @param mixed $params
	 * @return mixed
	 */
	public function Validator_loginCredentials($control, $error, $params)
	{
		// authentificate user
		try {
		  $user = Customer::doAuthenticate($control->owner['email']->getValue(), $control->owner['password']->getValue());
		  return TRUE;
		} catch (Exception $e) {
            if ($e->getMessage() == 1) {
            	$error = $error[1];
            	return $error;
            } elseif ($e->getMessage() == 2) {
            	$error = $error[2];
            	return $error;
            } elseif ($e->getMessage() == 3) {
            	$error = $error[3];
            	return $error;
            } else {
            	$error = $error[4];
            	return $error;
            }
		}
	}
    
    /**
	 * Validator
	 * @param object $control
	 * @param mixed $error
	 * @param mixed $params
	 * @return mixed
	 */
	public function Validator_uniqueLogin($control, $error, $params)
	{
		$customerId = $this->db->select('customerId')->from(TBL_CUSTOMERS)->where('email=%s', $control->getValue())->execute()->fetchSingle();
		if ($customerId) return $error;
		return TRUE;
	}
    
}
