<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    FeedbacksForm.php 2011-03-21 razvanp@madesimplegroup.com
 */

class FeedBackForm  extends FForm
{
    /**
     * @var FeedBackControler 
     */
    private $controler;
    /**
     * @var array
     */
    private $callback;
    
    /**
     * @var FNode
     */
    private $node;
    
    /**
     * @param FeedBacksControler $controler
     * @param array $callback
     */
    public function startup(FeedbacksControler $controler, array $callback, FNode $node)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->node = $node;
        $this->init();
    }

    /**
	 * @return void
	 */
    private function init()
    {
        $this->addFieldset('Submit Feedback');
        $this->add('RadioInline','radio', 'My feedback is: ')
             ->setOptions(FeedBacksModel::$ratings)->setValue('Neutral')
             ->addRule(FForm::Required, 'Please chose feedback!');
        $this->addArea('message', 'Give feedback:')->rows(6)->cols(37)
             ->addRule(FForm::Required, 'Please enter your feedback!');
		$this->addSubmit('feedbackSubmit', 'Send Feedback')->class('btn_submit fright')->onclick('changes = false');
        $this->onValid = $this->callback;
	    $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();
            $feedbacks = new FeedBack();
            $feedbacks->text = $data['message'];
            $feedbacks->rating = $data['radio'];
            $feedbacks->nodeId = $this->node->getId();
            $feedbacks->pageTitle = $this->node->getLngTitle();
            $feedbacks->url = $this->node->getLink();
            $feedbacks->dtc = new DibiDateTime();
		    $feedbacks->dtm = new DibiDateTime();
            $id = $feedbacks->save();
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }
}
