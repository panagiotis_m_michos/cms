<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS
 * @author 		Nikolai Senkevich, Stan Bazik
 * @internal 	project/forms/Front/CorporateForm.php
 * @created 	10/10/2011
 */
class CorporateForm extends FForm
{

    /**
     * @var CUShareholdersControler
     */
    private $controler;
    /**
     * @var array
     */
    private $callback;
    /**
     *
     * @var int 
     */
    private $num;
    /**
     *
     * @var object 
     */
    private $company;
    /**
     *
     * @var object 
     */
    private $corporate;

    /**
     * @param CUShareholdersControler $controler
     * @param array $callback
     */
    public function startup(CUShareholdersControler $controler, array $callback, $corporate, $company, $num)
    {

        $this->controler = $controler;
        $this->callback = $callback;
        $this->num = $num;
        $this->company = $company;
        $this->corporate = $corporate;
        $this->init();
    }

    private function init()
    {
        // adddress
        $this->addFieldset('Address');
        $this->addText('premise', 'Premise *')
            ->addRule(FForm::Required, 'Please provide Premise')
            ->addRule('MaxLength', "Premise can't be more than 50 characters", 50);
        $this->addText('street', 'Street *')
            ->addRule(FForm::Required, 'Please provide Street')
            ->addRule('MaxLength', "Street can't be more than 50 characters", 50);
        $this->addText('thoroughfare', 'Address 3')
            ->addRule('MaxLength', "Address 3 can't be more than 50 characters", 50);
        $this->addText('post_town', 'Town *')
            ->addRule(FForm::Required, 'Please provide Town')
            ->addRule('MaxLength', "Town can't be more than 50 characters", 50);
        $this->addText('county', 'County')
            ->addRule('MaxLength', "County can't be more than 50 characters", 50);
        $this->addText('postcode', 'Postcode *')
            ->addRule(array($this, 'Validator_postcode'), 'You cannot use our postcode for this address')
            ->addRule(FForm::Required, 'Please provide Postcode')
            ->addRule('MaxLength', "Postcode can't be more than 15 characters", 15);
        $this->addSelect('country', 'Country *', Address::$countries)
            ->addRule(FForm::Required, 'Please provide Country');


        // action
        $this->addFieldset('Action');
        $this->addSubmit('submit', 'Submit')
            ->class('btn')->style('width: 200px; height: 30px;');

        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();

            // save shareholder
            $shareholder = $this->corporate;
            $address = new Address();
            $address->setPremise($data['premise']);
            $address->setStreet($data['street']);
            $address->setThoroughfare($data['thoroughfare']);
            $address->setPostTown($data['post_town']);
            $address->setCounty($data['county']);
            $address->setPostcode($data['postcode']);
            $address->setCountry($data['country']);
            $shareholder->setAddress($address);
            $this->clean();

            $corporate = ShareCert::getCorporateCertificate($shareholder, $this->company->getCompanyName(), $this->company->getCompanyNumber(), $shareholder->getAddress()->getFields(), $this->num);

            $corporate->getPdfCertificate();
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Validate postcode for UK
     *
     * @param Text $control
     * @param string $error
     * @param array $params
     * @return boolean
     */
    static public function Validator_postcode(Text $control, $error, $params)
    {
        $value = $control->getValue();
        $countryId = $control->owner['country']->getValue();
        if ($countryId == Customer::UK_CITIZEN && (!preg_match('#^(GIR 0AA|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]|[A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y]))|[0-9][A-HJKPS-UW]) [0-9][ABD-HJLNP-UW-Z]{2})$#i', $value) || !preg_match('#^([A-Z][\dA-Z]{1,3}[\s][\d][A-Z]{2,2})$#i', $value))) {
            return $error;
        }
        return TRUE;
    }

}