<?php

use Utils\Date;

/**
 * @TODO update when documentation is present
 */
class ChangeAccountingReferenceDateForm extends FForm
{

    /**
     * @var CUChangeDateControler 
     */
    private $controler;
    /**
     * @var array
     */
    private $callback;
    /**
     * @var Company
     */
    private $company;

    /**
     * @param CUChangeDateControler $controler
     * @param array $callback
     */
    public function startup(CUChangeDateControler $controler, array $callback, $company)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->company = $company;
        $this->init();
    }

    private function init()
    {
        $ardCurrent = $this->company->getAccountingReferenceDate();
        $ardPrevios = date('d-m-Y', strtotime('-1 year', strtotime($this->company->getAccountingReferenceDate())));

        $this->addFieldset('New Accounting Reference Date ');      
        $this->add('DatePickerNew', 'date', 'New Date *')
            ->addRule(FForm::Required, 'Please provide date!.')
            ->addRule('DateFormat', 'Please enter date format as DD-MM-YYYY', 'd-m-Y')
            ->addRule(array($this, 'Validator_futureDate'), 'New accounting period cannot be greater than 18 months.')
            ->class('date')
            ->setValue(date('d-m-Y'));
        $this->addRadio(
            'ARDRange',
            'Please select which accounting reference date you want to change?',
            array(
                1 => "Current accounting period <span style=\"padding-left:100px;\"> $ardCurrent</span>" , 
                2 => "Immediately previous accounting period <span style=\"padding-left:24px;\"> $ardPrevios"
            )
        )->class('ARDRange')->addRule(FForm::Required, 'Please check accounting period');

        $this->addRadio(
            'fiveYearExtensionDetails',
            'Are you extending the AR period more than once in 5 years?',
            array(1 => 'Yes', 0 => 'No')
        )->class('arPeriod');

        $this->addradio(
            'extensionReason',
            'Extend reason:',
            array(
                'ADMIN' => 'The company is in administration',
                'STATE' => 'You have specific approval of the Secretary of State',
                'EEAPAR' => 'You are extending the company\'s accounting reference period to align with that of a parent or subsidiary undertaking established in the European Economic Area (EEA)'
            )
        )->class('arPeriodHide1')->addRule(array($this, 'Validator_period'), 'Please select reasone.');
        //->addRule(FForm::Required, 'Please select reason');
        $this->addText('extensionAuthorisedCode', 'Extension Authorised Code')
            ->class('arPeriodHide2')->addRule(array($this, 'Validator_extension'), 'Please provide 4 character code.')
            ->setDescription('If you have indicated that you have approval by the Secretary of State please enter the code provided on your Secretary of State authorisation letter (4 characters).');
        $this->addFieldset('Action');
        $this->addSubmit('send', 'Send')->class('btn_submit fright mbottom');
        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        $data = $this->getValues();
        try {
            $data['date'] = !empty($data['date']) ? Date::changeFormat($data['date'], 'd-m-Y', 'Y-m-d') : NULL;
            $this->company->sendChangeAccountingReferenceDate($data);
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     *
     * @param DatePicker $control
     * @param type $error
     * @param type $params
     * @return type 
     */
    public function Validator_futureDate($control, $error, $params)
    {
        $value = $control->getValue();

        //@TODO strtotime does not work with format d-m-Y
        if ($value) {
            $dateObject = new Date($value);
            $value = $dateObject->format('Y-m-d'); 
        }
        $companyData = $this->company->getData();

        if (empty($companyData['company_details']['accounts_last_made_up_date'])) {
            $date = strtotime($companyData['company_details']['incorporation_date']);
        } else {
            $date = strtotime($this->company->getAccountingReferenceDate());
            $date = strtotime('-1 year', $date);
        }

        if ($control->owner['ARDRange']->getValue() == 2) {
            $date = strtotime('-1 year', $date);
        }

        $date = strtotime('+18 month', $date);
        $userDate = strtotime($value);

        if ($userDate >= $date) {
            return $error;
        }
        return TRUE;
    }

    /**
     *
     * @param FControl $control
     * @param type $error
     * @param type $params
     * @return type 
     */
    public function Validator_period(FControl $control, $error, $params)
    {
        $value = $control->owner['fiveYearExtensionDetails']->getValue();
        $value2 = $control->owner['extensionReason']->getValue();
        if ($value == 0 || empty($value) || ($value == 1 && !empty($value2))) {
            return TRUE;
        }return $error;
    }

    /**
     *
     * @param FControl $control
     * @param type $error
     * @param type $params
     * @return type 
     */
    public function Validator_extension(FControl $control, $error, $params)
    {
        $value = $control->owner['fiveYearExtensionDetails']->getValue();
        $value2 = $control->owner['extensionReason']->getValue();
        $value3 = $control->owner['extensionAuthorisedCode']->getValue();
        if ($value2 == 'STATE' && empty($value3)) {
            return $error;
        }
        return TRUE;
    }

}
