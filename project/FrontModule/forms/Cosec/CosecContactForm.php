<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS
 * @author 		Nikolai Senkevich, Stan Bazik
 * @internal 	project/forms/Cosec/CosecContactForm.php
 * @created 	28/03/2011
 */
class CosecContactForm extends FForm
{

    /**
     * @var CosecControler
     */
    private $controler;
    
    /**
     * @var CosecEmailer 
     */
    private $cosecEmailer;
    
    /**
     * @var array
     */
    private $callback;

    /**
     * @param CosecControler $controler
     * @param array $callback
     */
    public function startup(CosecControler $controler, array $callback, CosecEmailer $cosecEmailer)
    {
        $this->controler = $controler;
        $this->cosecEmailer = $cosecEmailer;
        $this->callback = $callback;
        $this->init();
    }

    private function init()
    {
        $this->addFieldset('Contact Form');
        $this->addText('customer_name', 'Name:');
        $this->addText('customer_email', 'Email:*')->addRule(FForm::Required, 'Please provide email!')
            ->addRule(FForm::Email, 'Incorrect email format');
        $this->addText('contact_phone', 'Telephone:');
        $this->addText('company_name', 'Company:');
        $this->addArea('message', 'Message:*')->rows(5)->cols(30)->addRule(FForm::Required, 'Message is required!');

        $this->addFieldset('Action');
        $this->addSubmit('send', 'Send')->style('width: 200px; height: 30px;');

        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();
            $this->cosecEmailer->contactUsEmail($data);
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

}