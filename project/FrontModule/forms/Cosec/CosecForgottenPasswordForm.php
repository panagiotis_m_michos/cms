<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    FeedbacksForm.php 2011-11-11 razvanp@madesimplegroup.com
 */

class CosecForgottenPasswordForm extends FForm
{

    /**
     * @var CosecControler
     */
    private $controler;
    
    /**
     * @var CosecEmailer 
     */
    private $cosecEmailer;
    
    /**
     * @var array
     */
    private $callback;
    
    /**
     * @param CosecControler $controler
     * @param array $callback
     */
    public function startup(CosecControler $controler, array $callback, CosecEmailer $cosecEmailer)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->cosecEmailer = $cosecEmailer;
        $this->init();
    }

    private function init()
    {
        $this->addFieldset('Your email');
        $this->addText('email', 'Email:')
            ->addRule(FForm::Required, 'Email is required!')
            ->addRule(FForm::Email, 'Email is not valid!')
            ->addRule(array($this, 'Validator_forgetEmail'), 'Your email address could not be found. Please check the spelling and try again.')
            ->size(30);
            
        // action
        $this->addFieldset('Action');
        $this->addSubmit('submit', 'Send')->class('btn_submit');
        

        $this->onValid = $this->callback;
        $this->start();
    }
    
    public function process()
    {
        try {
            $customerEmail = $this->getValue('email');
            // re-save customer
           
            $customer = Customer::getCustomerByEmail($customerEmail);
            $customer->password = FTools::generPwd(8);
            $customer->save();

            $link = FApplication::$httpRequest->uri->getHostUri() . $this->controler->router->link(CosecControler::LOGIN_PAGE);
            /** @var ForgottenPasswordEmailer $forgottenPasswordEmailer */
            $forgottenPasswordEmailer = Registry::$container->get(DiLocator::EMAILER_FORGOTTEN_PASSWORD);
            $forgottenPasswordEmailer->forgottenPasswordEmail($link, $customer);

            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    /**
     * Checking if customer email is in database
     * 
     * @param object $control
     * @param mixed $error
     * @param mixed $params
     * @return mixed
     */
    public function Validator_forgetEmail($control, $error, $params)
    {
        try {
            Customer::getCustomerByEmail($control->getValue());
            return TRUE;
        } catch (Exception $e) {
            return $error;
        }
    }
    
    
    
}
