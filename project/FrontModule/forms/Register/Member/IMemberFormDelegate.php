<?php

namespace Front\Register;

use Entities\Register\Member;
use Exception;

interface IMemberFormDelegate
{
    public function memberFormCreated(Member $member);
    public function memberFormUpdated(Member $member);
    public function memberFormDeleted();
    public function memberFormFailed(Exception $e);
}
