<?php

namespace Front\Register;

use Entities\Register\Member;
use Exception;
use FForm;
use Services\Register\MemberService;
use UtilsDatePicker;

class MemberForm extends FForm
{

    /**
     * @var string
     */
    protected $render_class = 'InlineSubmitRenderer';

    /**
     * @var IMemberFormDelegate
     */
    private $delegate;

    /**
     * @var Member
     */
    private $member;

    /**
     * @var MemberService
     */
    private $registerMemberService;

    /**
     * @param IMemberFormDelegate $delegate
     * @param Member $member
     * @param MemberService $registerMemberService
     */
    public function startup(IMemberFormDelegate $delegate, Member $member, MemberService $registerMemberService)
    {
        $this->delegate = $delegate;
        $this->member = $member;
        $this->registerMemberService = $registerMemberService;

        $this->buildForm();
        $this->beforeValidation = array($this, 'beforeValidation');
        $this->onValid = array($this, 'process');
        $this->start();
    }

    public function beforeValidation()
    {
        if ($this->isSubmitedBy('delete')) {
            $this->registerMemberService->deleteMember($this->member);
            $this->clean();
            $this->delegate->memberFormDeleted();
        }
    }

    public function process()
    {
        try {
            $isNew = $this->member->isNew();
            list(, $name, $dateEntry, $dateCeased, $address) = array_values($this->getValues());

            $this->member->setName($name);
            $this->member->setDateEntry($dateEntry);
            $this->member->setDateCeased($dateCeased);
            $this->member->setAddress($address);

            $this->registerMemberService->saveMember($this->member);
            $this->clean();

            if ($isNew) {
                $this->delegate->memberFormCreated($this->member);
            } else {
                $this->delegate->memberFormUpdated($this->member);
            }
        } catch (Exception $e) {
            $this->delegate->memberFormFailed($e);
        }
    }

    /**
     * @param UtilsDatePicker $control
     * @param string $error
     * @return bool
     */
    public function Validator_date(UtilsDatePicker $control, $error)
    {
        $value = $control->getValue();
        if ($value === FALSE) {
            return $error;
        }
        return TRUE;
    }

    private function buildForm()
    {
        $this->addFieldset('Personal Information');
        $this->addText('name', 'Name')
            ->addRule(self::Required, 'Required')
            ->setValue($this->member->getName());
        $this->add('UtilsDatePicker', 'dateEntry', 'Date of Entry')
            ->size(14)
            ->setValue($this->member->getDateEntry())
            ->addRule(array($this, 'Validator_date'), 'Please provide a valid date');
        $this->add('UtilsDatePicker', 'dateCeased', 'Date Ceased')
            ->size(14)
            ->setValue($this->member->getDateCeased())
            ->addRule(array($this, 'Validator_date'), 'Please provide a valid date');
        $this->addArea('address', 'Address')
            ->cols(40)
            ->rows(5)
            ->setValue($this->member->getAddress());

        $this->addSubmit('submit', 'Save')
            ->class('pull-right green-submit');

        if (!$this->member->isNew()) {
            $this->addSubmit('delete', 'Remove this member')
                ->class('remove-link-submit margin-right-10 pull-right')
                ->onclick('return confirm("Are you sure you want to remove this Member?")');
        }
    }

}
