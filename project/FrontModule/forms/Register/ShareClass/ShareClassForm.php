<?php

namespace Front\Register\ShareClassEvent;

use Entities\Register\ShareClass;
use Exception;
use FForm;
use Front\Register\ShareClass\IShareClassFormDelegate;
use Services\Register\ShareClassService;
use UtilsDatePicker;
use Helpers\Currency;

class ShareClassForm extends FForm
{
    /**
     * @var string
     */
    protected $render_class = 'InlineSubmitRenderer';

    /**
     * @var IShareClassFormDelegate
     */
    private $delegate;

    /**
     * @var ShareClass
     */
    private $shareClass;

    /**
     * @var ShareClassService
     */
    private $registerShareClassService;

    /**
     * @param IShareClassFormDelegate $delegate
     * @param ShareClass              $shareClass
     * @param ShareClassService       $registerShareClassService
     */
    public function startup(IShareClassFormDelegate $delegate, ShareClass $shareClass, ShareClassService $registerShareClassService)
    {
        $this->delegate = $delegate;
        $this->shareClass = $shareClass;
        $this->registerShareClassService = $registerShareClassService;

        $this->buildForm();
        $this->onValid = array($this, 'process');
        $this->start();
    }

    public function process()
    {
        try {
            $isNew = $this->shareClass->isNew();

            if ($this->isSubmitedBy('delete')) {
                $this->registerShareClassService->deleteShareClass($this->shareClass);
                $this->clean();
                $this->delegate->shareClassFormDeleted();

            } else {
                $values = $this->getValues();

                $this->shareClass->setClassType($values['classType']);
                $this->shareClass->setCurrencyIso(Currency::ISO_GBP);
                $this->shareClass->setPrice($values['price']);

                $this->registerShareClassService->saveShareClass($this->shareClass);
                $this->clean();

                if ($isNew) {
                    $this->delegate->shareClassFormCreated($this->shareClass);
                } else {
                    $this->delegate->shareClassFormUpdated($this->shareClass);
                }
            }

        } catch (Exception $e) {
            $this->delegate->shareClassFormFailed($e);
        }
    }

    /**
     * @param UtilsDatePicker $control
     * @param string $error
     * @return bool
     */
    public function Validator_date(UtilsDatePicker $control, $error)
    {
        $value = $control->getValue();
        if ($value === FALSE) {
            return $error;
        }
        return TRUE;
    }

    private function buildForm()
    {
        $this->addFieldset('Share Class');
        $this->addText('classType', 'Class Type')
            ->size(30)
            ->setDescription('Type of the class (e.g. Ordinary)')
            ->addRule(self::Required, 'Required')
            ->setValue($this->shareClass->getClassType());
        $this->addText('price', 'Share Price')
            ->size(30)
            ->setDescription('Price of the share in Pounds (GBP)')
            ->addRule(self::Required, 'Required')
            ->setValue($this->shareClass->getPrice());
        $this->addSubmit('submit', 'Save')
            ->class('pull-right green-submit');

        if (!$this->shareClass->isNew()) {
            $this->addSubmit('delete', 'Remove')
                ->class('remove-link-submit margin-right-10 pull-right')
                ->onclick('return confirm("Are you sure you want to remove this Share Class?")');
        }
    }
}
