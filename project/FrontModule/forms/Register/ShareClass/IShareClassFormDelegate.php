<?php

namespace Front\Register\ShareClass;

use Entities\Register\ShareClass;
use Exception;

interface IShareClassFormDelegate
{
    public function shareClassFormCreated(ShareClass $shareClass);
    public function shareClassFormUpdated(ShareClass $shareClass);
    public function shareClassFormDeleted();
    public function shareClassFormFailed(Exception $e);
}
