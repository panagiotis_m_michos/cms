<?php

namespace Front\Register\ShareClassEvent;

use Entities\Register\Member;
use Entities\Register\ShareClassEvent;
use Exception;
use FForm;
use Services\Register\ShareClassEventService;
use UtilsDatePicker;

class ShareClassEventForm extends FForm
{
    /**
     * @var IShareClassEventFormDelegate
     */
    private $delegate;

    /**
     * @var ShareClassEvent
     */
    private $shareClassEvent;

    /**
     * @var ShareClassEventService
     */
    private $registerShareClassEventService;

    /**
     * @param IShareClassEventFormDelegate $delegate
     * @param ShareClassEvent $shareClassEvent
     * @param ShareClassEventService $registerShareClassEventService
     */
    public function startup(IShareClassEventFormDelegate $delegate, ShareClassEvent $shareClassEvent, ShareClassEventService $registerShareClassEventService)
    {
        $this->delegate = $delegate;
        $this->shareClassEvent = $shareClassEvent;
        $this->registerShareClassEventService = $registerShareClassEventService;

        $this->buildForm();
        $this->onValid = array($this, 'process');
        $this->start();
    }

    public function process()
    {
        try {
            $values = $this->getValues();

            $this->shareClassEvent->setDtEntry($values['dtEntry']);
            $this->shareClassEvent->setTypeId($values['typeId']);
            $this->shareClassEvent->setQuantity($values['quantity']);
            $this->shareClassEvent->setCertificateNumber($values['certificateNumber']);
            $this->shareClassEvent->setAcquired($values['acquired']);
            $this->shareClassEvent->setDisposed($values['disposed']);
            $this->shareClassEvent->setBalance($values['balance']);
            $this->shareClassEvent->setPricePerShare($values['pricePerShare']);
            $this->shareClassEvent->setTotalAmount($values['totalAmount']);
            $this->shareClassEvent->setNotes($values['notes']);

            $this->registerShareClassEventService->saveShareClassEvent($this->shareClassEvent);
            $this->clean();

            $this->delegate->shareClassEventFormCreated($this->shareClassEvent);
        } catch (Exception $e) {
            $this->delegate->shareClassEventFormFailed($e);
        }
    }

    /**
     * @param UtilsDatePicker $control
     * @param string $error
     * @return bool
     */
    public function Validator_date(UtilsDatePicker $control, $error)
    {
        $value = $control->getValue();
        if ($value === FALSE) {
            return $error;
        }
        return TRUE;
    }

    private function buildForm()
    {
        $this->add('UtilsDatePicker', 'dtEntry', 'Date of Entry')
            ->size(14)
            ->setValue($this->shareClassEvent->getDtEntry())
            ->addRule(array($this, 'Validator_date'), 'Please provide a valid date')
            ->addRule(self::Required, 'Required');
        $this->addFieldset('Entry Number');
        $this->addRadio('typeId', 'Type', ShareClassEvent::$types)
            ->setValue($this->shareClassEvent->getTypeId());
        $this->addText('quantity', 'Quantity')
            ->setValue($this->shareClassEvent->getQuantity());
        $this->endFieldset();
        $this->addText('certificateNumber', 'Certificate Number')
            ->setValue($this->shareClassEvent->getCertificateNumber());
        $this->addFieldset('Number of Shares');
        $this->addText('acquired', 'Acquired')
            ->setValue($this->shareClassEvent->getAcquired());
        $this->addText('disposed', 'Disposed')
            ->setValue($this->shareClassEvent->getDisposed());
        $this->addText('balance', 'Balance')
            ->setValue($this->shareClassEvent->getBalance());
        $this->endFieldset();
        $this->addText('pricePerShare', 'Price per Share')
            ->setDescription('Price of the share in Pounds (GBP)')
            ->setValue($this->shareClassEvent->getPricePerShare());
        $this->addText('totalAmount', "Total Amount <br> &nbsp; Agreed to be Paid")
            ->setDescription('In Pounds (GBP)')
            ->setValue($this->shareClassEvent->getTotalAmount());
        $this->addArea('notes', 'Notes')
            ->cols(40)
            ->rows(5)
            ->setValue($this->shareClassEvent->getNotes());
        $this->addSubmit('submit', 'Save')
            ->class('green-submit');
    }
}
