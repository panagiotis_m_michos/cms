<?php

namespace Front\Register\ShareClassEvent;

use Entities\Register\ShareClassEvent;
use Exception;

interface IShareClassEventFormDelegate
{
    public function shareClassEventFormCreated(ShareClassEvent $shareClassEvent);
    public function shareClassEventFormFailed(Exception $e);
}
