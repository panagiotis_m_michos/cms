<?php

namespace Front\Register\ShareClassEvent;

use Entities\Register\Member;
use Exception;

interface IDeleteFormDelegate
{
    public function deleteShareClassEventFormDeleted();
    public function deleteShareClassEventFormFailed(Exception $e);
}
