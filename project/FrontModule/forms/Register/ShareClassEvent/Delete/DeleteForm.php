<?php

namespace Front\Register\ShareClassEvent;

use Entities\Register\ShareClass;
use Entities\Register\ShareClassEvent;
use Exception;
use FForm;
use Services\Register\MemberService;
use Services\Register\ShareClassEventService;
use Services\Register\ShareClassService;

class DeleteForm extends FForm
{
    /**
     * @var IDeleteFormDelegate
     */
    private $delegate;

    /**
     * @var ShareClass
     */
    private $shareClass;

    /**
     * @var ShareClassService
     */
    private $registerShareClassService;

    /**
     * @param IDeleteFormDelegate $delegate
     * @param ShareClass $shareClass
     * @param ShareClassService $registerShareClassService
     */
    public function startup(IDeleteFormDelegate $delegate, ShareClass $shareClass, ShareClassService $registerShareClassService)
    {
        $this->delegate = $delegate;
        $this->shareClass = $shareClass;
        $this->registerShareClassService = $registerShareClassService;

        $this->buildForm();
        $this->onValid = array($this, 'process');
        $this->start();
    }

    public function process()
    {
        try {
            foreach ($this->shareClass->getShareClassEvents() as $shareClassEvent) {
                if ($this->isSubmitedBy('delete_' . $shareClassEvent->getId())) {
                    $this->shareClass->getShareClassEvents()->removeElement($shareClassEvent);
                    $this->registerShareClassService->saveShareClass($this->shareClass);
                    $this->clean();
                    $this->delegate->deleteShareClassEventFormDeleted();
                }
            }

        } catch (Exception $e) {
            $this->delegate->deleteShareClassEventFormFailed($e);
        }
    }

    private function buildForm()
    {
        foreach ($this->shareClass->getShareClassEvents() as $shareClassEvent) {
            $this->addSubmit('delete_' . $shareClassEvent->getId(), 'Remove')
                ->class('linkButton')
                ->onclick("return confirm('Are you sure you want to remove this Entry?')");
        }
    }
}
