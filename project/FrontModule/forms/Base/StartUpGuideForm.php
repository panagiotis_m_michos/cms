<?php

class StartUpGuideForm extends FForm
{
    const STARTUP_FORM_ID = "UPTml140384";
    const STARTUP_ACTION = "http://madesimplegroup.us7.list-manage.com/subscribe/post";    
    const STARTUP_U = "73193eaef1f438e262d2e464d";
    const STARTUP_ID = "ce65016327";

    /**
     * @var BaseControler 
     */
    private $controler;

    /**
     * @var array
     */
    private $callback;

    /**
     * @param BaseControler $controler
     * @param array $callback
     */
    public function startup(BaseControler $controler, array $callback)
    {
        $this->controler = $controler;
        $this->callback = $callback;
        $this->init();
    }

    /**
     * @return void
     */
    private function init()
    {
        $this->addHidden("u", self::STARTUP_U);
        $this->addHidden("id", self::STARTUP_ID);
        $this->addText('MERGE1', 'Name:')
            ->setValue('Name')
            ->onClick('this.value=""')
            ->addRule(FForm::Required, 'Please enter a name');
        $this->addText('MERGE0', 'Email:')
            ->setValue('Email')
            ->onClick('this.value=""')
            ->addRule(FForm::Required, 'Please enter a valid email')
            ->addRule(FForm::Email, 'Please enter a valid email');
        $this->addSubmit('continue', '')->class('btn_submit fright start-up-button');
        $this->onValid = $this->callback;
        $this->start();
    }
}
