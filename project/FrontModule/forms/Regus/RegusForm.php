<?php

class RegusForm extends FForm
{

    /**
     * @var RegusControler
     */
    protected $controler;

    /**
     * @var array
     */
    protected $callback;

    /**
     * @var RegusEmailer
     */
    protected $regusEmailer;

    /**
     * @var RegusModel
     */
    protected $regusModel;

    /**
     * @var Regus
     */
    protected $regus;

    /**
     * @param RegusControler $controler
     * @param array $callback
     * @param RegusEmailer $regusEmailer
     * @param Regus $regus
     */
    public function startup(RegusControler $controler, array $callback, RegusEmailer $regusEmailer, Regus $regus)
    {

        $this->controler = $controler;
        $this->callback = $callback;
        $this->regusEmailer = $regusEmailer;
        $this->regusModel = new RegusModel($controler->db);
        $this->regus = $regus;
        $this->init();
    }

    private function init()
    {
        $this->addText('firstName', 'First Name')
            ->setValue($this->regus->getFirstName())
            ->addRule(FForm::Required, 'Please enter your First Name')
            ->addRule(FForm::REGEXP, 'Invalid characters. Please use a-z', '#^[A-Za-z ]{1,255}$#');
        $this->addText('lastName', 'Last Name')
            ->setValue($this->regus->getLastName())
            ->addRule(FForm::Required, 'Please enter your Last Name')
            ->addRule(FForm::REGEXP, 'Invalid characters. Please use a-z', '#^[A-Za-z ]{1,255}$#');
        $this->addText('email', 'Email')
            ->setValue($this->regus->getEmail())
            ->addRule(FForm::Required, 'Please enter your Email')
            ->addRule(FForm::Email, 'Invalid email. Please check the format');
        $this->addSelect('dialCode', '', DialCode::getCodeAndCountry())
            ->style("width: 45px");
        $this->addText('phone', 'Phone Number')
            ->setValue($this->regus->getPhone())
            ->addRule(FForm::Required, 'Please enter your phone number')
            ->addRule(FForm::REGEXP, 'Invalid phone number. Please use 0-9 only', '#^[0-9 ]{1,20}$#');
        $this->addSubmit('enquire', 'Enquire')->style("cursor: pointer");
        $this->onValid = $this->callback;
        $this->start();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $data = $this->getValues();
            $this->regus->setFirstName($data['firstName']);
            $this->regus->setLastName($data['lastName']);
            $this->regus->setEmail($data['email']);
            $this->regus->setDialCode($data['dialCode']);
            $this->regus->setPhone($data['phone']);
            $this->regusEmailer->enquireEmail($this->regus);

            $this->regus->setSend(TRUE);
            $this->regusModel->save($this->regus);
            $this->clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

}
