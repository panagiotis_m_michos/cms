{if isset($popup) && $popup == 1}
    {include file="@headerPopUp.tpl"}
{else}
    {include file="@header.tpl"}
{/if}
{capture name="content"}
	{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
	{$text nofilter}
{/capture}

{include file='@blocks/defaultTemplateBlock.tpl' content=$smarty.capture.content}
{if isset($popup) && $popup == 1}
    {include file="@footerPopUp.tpl"}
{else}
    {include file='@blocks/rightColumn.tpl'}
    {include file="@footer.tpl"}
{/if}