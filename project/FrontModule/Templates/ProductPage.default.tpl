{include file="@header.tpl"}

{capture name="content"}
	{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
	{$text nofilter}
	{if !empty($products)}
		<table class="grid">
		<col>
		<col width="150">
		{foreach from=$products key="productId" item="product"}
		<tr>
			<th><b>{$product->getLngTitle()}</b></th>
			<td rowspan="2" align="center">
				{if $product->price}
                                    <strong>&pound;{$product->price}</strong><br />
				{else}
                                    <strong>FREE</strong><br />
				{/if}
				{ui name="buy_button"
					loading="expand-right"
					text="Buy Now"
					actionUrl="{$this->router->link("BasketControler::PAGE_BASKET")}"
					inputName="productId"
					productId="{$productId}"}
			</td>
		</tr>
		<tr>
			<td>{$product->getLngText()|cms nofilter}</td>
		</tr>
		{/foreach}
		</table>
	{/if}
{/capture}

{include file='@blocks/defaultTemplateBlock.tpl' content=$smarty.capture.content}
{include file='@blocks/rightColumn.tpl'}
{include file="@footer.tpl"}
