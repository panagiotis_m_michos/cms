{if $visibleTitle}
    <h1>{$title}</h1>
{/if}

<p>To be able to synchronize your company information with Companies House you must provide the company Authentication Code.</p>

{$form->getBegin() nofilter}
    {if $form->hasErrors()}
        <p class="ff_err_notice">Form has <b> {$form->getErrors()|@count}</b> error(s)</p>
    {/if}

    <table class="ff_table">
        <tbody>
            <tr>
                <th width="20%" align="right">{$form->getLabel('authenticationCode') nofilter}</th>
                <td width="25%" align="left">{$form->getControl('authenticationCode') nofilter}</td>
                <td width="55%" align="left" class="padding-top-10"><span class="ff_control_err">{$form->getError('authenticationCode')}</span></td>
            </tr>
            <tr>
                <th>&nbsp;</th>
                <td align="left">{$form->getControl('update') nofilter}</td>
            </tr>
        </tbody>
    </table>
    <p>&nbsp;</p>
{$form->getEnd() nofilter}

<div id="auth-code-problems">
    <h4>Problems with Authentication Code?</h4>
    <p>
        Here are some articles that may help you...
    </p>
    <p>
        <a href="/project/blog/4-steps-to-requesting-a-lost-or-expired-companies-house-authentication-code/" target="_blank">4 steps to requesting a lost or expired Companies House Authentication Code</a><br>
        <a href="/project/blog/everything-you-need-to-know-about-the-companies-house-webfiling-authentication-code/" target="_blank">Everything you need to know about the Companies House WebFiling Authentication Code</a>
    </p>
</div>


{*{$form nofilter}*}
