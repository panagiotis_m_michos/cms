{include file="@header.tpl"}

<div id="maincontent2" class="cu-change-auth-code-page" style="padding: 0 0 30px 0;">
    <p style="margin: 0 0 15px 0; font-size: 11px;"><a href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}">{$companyName}</a> &gt; {$title}</p>

    {include file="CUChangeWebAuthCode/@content.tpl"}
</div>

{include file="@footer.tpl"}
