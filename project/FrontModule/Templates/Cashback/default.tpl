{include file="@header.tpl"}

<div id="maincontent">

    <h1 class="cashback">{$title}</h1>
    <h2 class="cashback">Cash Back Account Details</h2>

    {include 'Cashback/@form.tpl'}

    <h2 class="cashback">Cash Back History</h2>
    {$cashbacks nofilter}
    <p>&nbsp;</p>

    <h2 class="cashback">Cash Back FAQs</h2>
    
    <h3 class="cashback">How long does it take to get my cash back?</h3>
    <p class="cashback">You cash back payment may take between 4 – 8 weeks to arrive in you account.</p>
    
    <h3 class="cashback">Why does it take so long to receive my cash back?</h3>
    <p class="cashback">We endeavour to process all cash backs as soon as possible; however, there are strict verification processes that must happen in order for us to process the payment. The bank updates us with all new account information one month in arrears and then internally we have to verify the account before cash back can be released.</p>
    
    <h3 class="cashback">Will you notify me once you have sent my cash back?</h3>
    <p class="cashback">Yes, we will send you an email as soon as your cash back is paid.</p>
    
</div>
        
{include file="@footer.tpl"}
