<form action="{$form->getAction()}" method="post" name="{$form->getName()}" class="togglableSubmitForm">
    {if $form->hasErrors()}
        <p class="ff_err_notice">Form has <b> {$form->getErrors()|@count}</b> error(s)</p>
    {/if}

    {$form->getLabel('cashbackTypeId') nofilter}<br/>
    {$form->getControl('cashbackTypeId') nofilter}<br/>
    <span class="ff_control_err">{$form->getError('cashbackTypeId')}</span>

    <div class="conditionalShown" data-master="cashbackTypeId" data-value-for-show="BANK">
        <p class="cashback">Insert the bank account details where you want to receive your cash back.</p>
        <table class="ff_table cashback">
            <tbody>
                <tr>
                    <th width="20%" align="right">{$form->getLabel('sortCode') nofilter}</th>
                    <td width="40%" align="left">{$form->getControl('sortCode') nofilter}<span class="cashback">{$form['sortCode']->getDescription() nofilter}</span></td>
                    <td width="40%" align="left" class="padding-top-10"><span class="ff_control_err">{$form->getError('sortCode')}</span></td>
                </tr>
                <tr>
                    <th width="20%" align="right">{$form->getLabel('accountNumber') nofilter}</th>
                    <td width="40%" align="left">{$form->getControl('accountNumber') nofilter}<span class="cashback">{$form['accountNumber']->getDescription() nofilter}</span></td>
                    <td width="40%" align="left" class="padding-top-10"><span class="ff_control_err">{$form->getError('accountNumber')}</span></td>
                </tr>
            </tbody>
        </table>
    </div>
    <table class="ff_table cashback">
        <tbody>
            <tr>
                <th style="width:41%;">&nbsp;</th>
                <td width="37%" align="left">{$form->getControl('submit') nofilter}</td>
            </tr>
        </tbody>
    </table>
    <p>&nbsp;</p>
{$form->getEnd() nofilter}
{literal}
    <script>
        $(document).ready(function () {
            toggleConditionalVisibility('.conditionalShown');
        });
    </script>
{/literal}