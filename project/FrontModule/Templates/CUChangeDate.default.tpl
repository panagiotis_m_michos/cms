{include file="@header.tpl"}
<div id="maincontent1"> 
	{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
    <p style="margin: 20px 0 25px 0; font-size: 11px;">
        <a href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}">{$companyName}</a> &gt; {$title}
    </p>
    {* S234 Temporarily showing message and disabling possibility to update ARD *}
    <div style="margin-left: 0pt; width: 753px;" class="flash info2">
        <ul>
            <li>
                Sorry, this service is not currently available.
            </li>
            <li>
                To change your ARD you can file a <a href="http://www.companieshouse.gov.uk/forms/generalForms/AA01_change_of_accounting_reference_date.pdf" target="_blank">paper form AA01</a>
                (please follow <a href="http://www.companieshouse.gov.uk/infoAndGuide/faq/AA01Checklist.shtml" target="_blank">this guidance</a>)
            </li>
            <li>
                or make the change directly with Companies house <a href="http://www.companieshouse.gov.uk/infoAndGuide/faq/webFiling.shtml" target="_blank">WebFiling</a>
            </li>
        </ul>
    </div>
    {*
    <div style="margin-left: 0pt; width: 753px;" class="flash info2">
        <p>This page can be used to change the accounting reference date (form AA01) relating to either the current or immediately previous accounting period.</p>
        <ul>
            <li>You may not change a period for which the accounts are already overdue.</li>
            <li>You may not extend a period beyond 18 months unless the company is in administration.</li>
            <li>You may not extend any period more than once in five years (five accounting periods) unless you have provision to do so.</li>
        </ul>
    </div>
    <div style="margin-left: 0pt; width: 753px;">
        {$form->getBegin() nofilter}
        {if $form->getErrors()|@count gt 0}
            <p class="ff_err_notice ff_red_err" style="width: 735px">Form has <b> {$form->getErrors()|@count}</b> error(s). See below for more details:</p>
        {/if} 
            <fieldset>
                <legend>New Accounting Reference Date </legend>
                <table class="ff_table">
                    <col width="180px"/>
                    <col width="300px" />        
                    <tr>
                        <th colspan="3"style="padding-bottom: 15px;">{$form->getLabel("ARDRange") nofilter}</th>
                    </tr>
                    <tr>
                        <td colspan="3"style="padding-left: 10px;">{$form->getControl('ARDRange') nofilter}</td>
                    </tr>
                    <tr>
                        <td colspan="3"><span class="redmsg">{$form->getError('ARDRange')}</span></td>
                    </tr>
                    <tr>
                        <td colspan="3"style="padding-bottom: 15px;">Please enter the date you want the accounting period to end (dd-mm-yyyy)</td>
                    </tr>
                    <tr>
                        <th style="padding-left: 10px;">{$form->getLabel("date") nofilter}</th>
                        <td>{$form->getControl('date') nofilter}<span class="ff_desc">Once the accounting reference date is changed, subsequent accounting periods will end on the same day and month in future years.</span></td>
                        <td><span class="redmsg">{$form->getError('date')}</span></td>
                    </tr>

                    <tr>
                        <th>{$form->getLabel("fiveYearExtensionDetails") nofilter}</th>
                        <td>{$form->getControl('fiveYearExtensionDetails') nofilter}</td>
                        <td><span class="redmsg">{$form->getError('fiveYearExtensionDetails')}</span></td>
                    </tr>
                    <tr id="message2">
                        <td colspan="3"><span class="ff_desc">You may only extend a period more than once in five years if one of the following provisions apply. If this is true, please select the provision below:</span></td>
                    </tr>
                    <tr>
                        <th>{$form->getLabel("extensionReason") nofilter}</th>
                        <td>{$form->getControl('extensionReason') nofilter}</td>
                        <td><span class="redmsg">{$form->getError('extensionReason')}</span></td>
                    </tr>
                    <tr>
                        <th>{$form->getLabel("extensionAuthorisedCode") nofilter}</th>
                        <td>{$form->getControl('extensionAuthorisedCode') nofilter}<span class="ff_desc">If you have indicated that you have approval by the Secretary of State please enter the code provided on your Secretary of State authorisation letter (4 characters).</span></td>
                        <td><span class="redmsg">{$form->getError('extensionAuthorisedCode')}</span></td>
                    </tr>
                </table>
            </fieldset>
            <fieldset>
                <legend>Action</legend>
                <table class="ff_table">
                    <tr>
                        <th>{$form->getLabel("send") nofilter}</th>
                        <td>{$form->getControl('send') nofilter}</td>
                        <td></td>
                    </tr>
                </table>
            </fieldset>
        {$form->getEnd() nofilter}
    </div>
    *}
</div>
{include file="@footer.tpl"}

{literal}
<script type="text/javascript">
    <!--
    
    var current = "{/literal}{$ardCurrent}{literal}";
    var previos = "{/literal}{$ardPrevios}{literal}";
    var lastAccountMadeUp = "{/literal}{$lastAccountMadeUp}{literal}";
    function parseDate(str) {
        return $.datepicker.parseDate('dd-mm-yy', str);
    }
    function gettodayDate() {
        var a = new Date();
        var b = a.toISOString();
        var datapickerprefill = b.substring(8,10)+"-"+b.substring(5,7)+"-"+b.substring(0,4); 
        return datapickerprefill;
    }
    function refreshDate(accPeriod,type) {
        newDate = parseDate($(".date").val());
        if (accPeriod > newDate) {
            $('.arPeriodHide1, .arPeriodHide2').attr('disabled', 'disabled').parent().parent().hide();
            $('#message2').attr('disabled', 'disabled').hide();
            $('.arPeriod').attr('disabled', 'disabled').parent().parent().hide();
        } else {//alert('show addition fields')
            $('.arPeriod').removeAttr('disabled').parent().parent().show();
            if ($('.arPeriod:checked').val()  == 1) {
                $('.arPeriodHide1').removeAttr('disabled').parent().parent().show();
                $('#message2').show();
                if ($('.arPeriodHide1:checked').val() == 'STATE') {
                    $('.arPeriodHide2').removeAttr('disabled').parent().parent().show();}
            }
        }
    }
   
    //loading defaults
    //check if we need previos period if not, remove
    if (lastAccountMadeUp == '') {
        $("#ARDRange2").attr('disabled', 'disabled').hide();
        $('label[for="ARDRange2"]').attr('disabled', 'disabled').hide();
        $('#ARDRange1').attr('checked', true);
        previos = current;
    }

    $(document).ready(function() {
        
        //changing range
        $(".ARDRange").change(function() {
            if($(this).val()== 1){
                var accPeriod  = parseDate(current);
                refreshDate(accPeriod); 
            }
            if($(this).val()== 2){
                var accPeriod  = parseDate(previos);
                refreshDate(accPeriod); 
            }
        });
            //check range
        if ($("#ARDRange1:checked").val()== 1) {
            var accPeriod  = parseDate(current);
        }
        if ($("#ARDRange2:checked").val()== 2) {
            var accPeriod  = parseDate(previos);
        }
        
        //check range with date
        if (accPeriod > parseDate($(".date").val())) {
            $('.arPeriod').attr('disabled', 'disabled').parent().parent().hide();
            $('.arPeriodHide1, .arPeriodHide2').attr('disabled', 'disabled').parent().parent().hide();
            $('#message2').attr('disabled', 'disabled').hide();
        } else if (accPeriod < parseDate($(".date").val())) {
            $('.arPeriod').removeAttr('disabled').parent().parent().show();

        }
        //check period 
        if (!$(".arPeriod").attr('checked')) {
            $('.arPeriodHide1, .arPeriodHide2').attr('disabled', 'disabled').parent().parent().hide();
            $('#message2').attr('disabled', 'disabled').hide();
        }
        
        //check reson
        if ($('.arPeriodHide1:checked').val() == 'STATE') {
            $('.arPeriodHide2').removeAttr('disabled').parent().parent().show();
        } else {
            $('.arPeriodHide2').attr('disabled', 'disabled').parent().parent().hide();
        }
        
        
        // changing date
        $(".date").change(function() {
            if ($("#ARDRange1:checked").val()== 1) {
                var accPeriod  = parseDate(current);
            }
            if ($("#ARDRange2:checked").val()== 2) {
                var accPeriod  = parseDate(previos);
            }
            refreshDate(accPeriod); 
        });
    
        //changing period
        $(".arPeriod").change(function() {
            if ($(this).val()  == 1) {
                $('.arPeriodHide1').removeAttr('disabled').parent().parent().show();
                $('#message2').show();
                if ($('.arPeriodHide1:checked').val() == 'STATE') {
                    $('.arPeriodHide2').removeAttr('disabled').parent().parent().show();
                }
            } else if ($(this).val()  == 0) {
                $('.arPeriodHide1, .arPeriodHide2').attr('disabled', 'disabled').parent().parent().hide();
                $('#message2').attr('disabled', 'disabled').hide();
            }
        });
        
        //changing reason
        $('.arPeriodHide1').click(function() {
            if ($('.arPeriodHide1:checked').val() == 'STATE'){
                $('.arPeriodHide2').removeAttr('disabled').parent().parent().show();
            } else {
                $('.arPeriodHide2').attr('disabled', 'disabled').parent().parent().hide();

            }
        });
    });
    //-->
</script>
{/literal}
