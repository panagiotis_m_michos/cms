{include file="@headerPopUp.tpl"}

<div class="update_info_page">
    
    <div>
    {if $image->getHtmlTag()}
    <div style="float: left;width:360px;">
    {else}
    <div>
    {/if}
        <h1 class="update_page_h1">{$node->getLngTitle()}</h1>
        <h3>Price if bought today: &pound;{$node->getProperty('associatedPrice')}</h3>
        <h4 style="padding-top: 10px;" >Actual price: &pound;{$node->getProperty('productValue')}</h4>
    </div>
    
    {if $image->getHtmlTag()}
    <div style="float: right;height: 113px; width: 150px;">
        {$image->getHtmlTag('s') nofilter}
    </div>
    {/if}
        <div style="clear: both"></div>
    </div>
    <p>{$node->getProperty('upgradeDescription') nofilter}</p>
    <a class="updateaddbasket" href="{url route='basket_module_package_basket_add_associated_product' productId=$nodeId}">Add to basket</a>

{literal}
   <script type="text/javascript">
    $('.updateaddbasket').click(function(){
        parent.location.href = $(this).attr('href');
        return false;
    });
    </script>
   {/literal}
</div>

{include file="@footerPopUp.tpl"}



