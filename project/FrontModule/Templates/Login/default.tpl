{include file="@header.tpl"}
<div id="maincontent1">
{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
    <div class="progressbar step3"></div>
    <div style="position: relative">
        {if $loginForm->hasErrors()}
        <ul style="float: right; margin-right: 70px; margin-left: 280px;">
            {foreach from=$loginForm->getErrors() item='error'}
                <li>{$error}</li>
            {/foreach}
        </ul>
        {/if}
        {if $registerForm->hasErrors()}
            <ul>
                {foreach from=$registerForm->getErrors() item='error'}
                    <li>{$error}</li>
                {/foreach}
            </ul>
        {/if}
    </div>
    <div class="box-dblwhite">
        <div class="box-dblwhite-top">
            <div class="box-dblwhite-bottom">
                <div class="lefthalf">
                    <p>I am a</p>
                    <h4><span class="orange txtbold">New Customer</span><span class="mleft required font13" style="font-weight:normal">(<span class="orange">*</span> Required field)</span></h4>
                    {$registerForm->getBegin() nofilter}
                        <div class="customer">
                            {$registerForm->getLabel('email') nofilter}
                            {$registerForm->getControl('email') nofilter}
                            <div id="hint" style="color: #B94A48; display: none; font-size: 0.9em;margin: -10px 0 10px;float: left;">test</div>

                            {$registerForm->getLabel('password') nofilter}
                            {$registerForm->getControl('password') nofilter}

                            {$registerForm->getLabel('passwordConf') nofilter}
                            {$registerForm->getControl('passwordConf') nofilter}

                            {$registerForm->getControl('register') nofilter}
                        </div><!-- .customer -->
                    {$registerForm->getEnd() nofilter}
                </div>
  <script type="text/javascript">
    var $email = $('#email');
    var $hint = $("#hint");

    $email.blur(function() {
      $hint.css('display', 'none').empty();
      $(this).mailcheck({
        suggested: function(element, suggestion) {
          if(!$hint.html()) {
            // First error - fill in/show entire hint element
            var suggestion = "Did you mean <span class='suggestion'>" +
                              "<span class='address'>" + suggestion.address + "</span>"
                              + "@<a href='#' class='domain'>" + suggestion.domain + 
                              "</a></span>?";
                              
            $hint.html(suggestion).fadeIn(150);
          } else {
            // Subsequent Errors/
            $(".address").html(suggestion.address);
            $(".domain").html(suggestion.domain);
          }
        }
      });
    });

    $hint.click(function() {
      // On click, fill in the field with the suggestion and remove the hint
      $email.val($(".suggestion").text());
      $hint.fadeOut(200, function() {
        $(this).empty();
      });
      return false;
    });
  </script>
                <div class="righthalf">
                    <p>I am an</p>
                    <h4 class="orange txtbold">Existing Customer</h4>

                    {$loginForm->getBegin() nofilter}
                        <div class="customer">
                            {$loginForm->getLabel('email-login') nofilter}
                            {$loginForm->getControl('email-login') nofilter}

                            {$loginForm->getLabel('password-login') nofilter}
                            {$loginForm->getControl('password-login') nofilter}

                            {$loginForm->getControl('login') nofilter}
                        </div><!-- .customer -->
                    {$loginForm->getEnd() nofilter}

                    <div class="clear"></div>
                    <a href="{$this->router->link("LoginControler::FORGOTTEN_PASSWORD_PAGE")}">Forgotten Password? Click here</a>
                    <img src="{$urlImgs}questionmark.png" width="22" height="22" alt="?" class="mleft" />
                </div>

                <div class="clear"></div>

            </div>
        </div>
    </div>
</div>

<!-- ============================================= RIGHT COLUMN ======================================================== -->

{include file='@blocks/rightColumn.tpl'}

<!-- ================================================ FOOTER =========================================================== -->

{include file="@footer.tpl"}