{include file="@header.tpl"}
{literal}
<script text="javascript">
$(function(){
  $('#submit').click(function(){  
    $('input:submit').hide();
    $('#sending_message').show();    
  });
});
</script>
{/literal}
{capture name="content"}
	{$form->getBegin() nofilter}
	{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formtable">
	<tr>
	    <td width="20%" align="right" valign="top">{$form->getLabel('email') nofilter}</td>
	    <td width="40%" align="left" valign="top">{$form->getControl('email') nofilter}</td>
	    <td width="40%" align="left" valign="top">{$form->getError('email')}</td>
	</tr>
	<tr>
	    <td align="right" valign="middle">&nbsp;</td>
	    <td align="left" valign="top">
	    <span id="sending_message" style="display:none;">Sending new password...</span>
	    {$form->getControl('submit') nofilter}</td>
  	</tr>
	</table>
	{$form->getEnd() nofilter}
{/capture}

{include file='@blocks/defaultTemplateBlock.tpl' content=$smarty.capture.content}
{include file='@blocks/rightColumn.tpl'}
{include file="@footer.tpl"}