{include file="@header.tpl"}

{literal}
<style>
    .registeredofficeservice {
        margin-bottom: 40px;
    }
    .registeredofficeservice h2 {
        color: #000;
        font-size: 18px;
        margin-bottom: 0;
        margin-top: 0;
    }
    .registeredofficeservice p {
        font-size: 14px;
        line-height: 20px;
        padding-top: 10px;
    }

    .roleftcol {
        width: 560px;
        float: left;
    }
    .roimage {
        float: right;
        margin-left: 20px;
        width: 400px;
        text-align: center;
    }
    .roimage p {
        font-size: 13px;
        line-height: 19px;
    }
    .roimage span {
        color: #666;
    }
    .green_button_block {
        /*        padding: 10px;*/
        height: 53px;
        width: 532px;
        background: url('front/imgs/green-btn.png') no-repeat scroll left top transparent;
        margin: 3px 0;
        text-align: center;
        color: #fff;
        font-size: 20px;
        font-weight: bold;
    }

    .roleftcol ul {
        padding-left: 0;
        margin-top: 20px;
    }
    .roleftcol li.greentick32 {
        list-style-type: none;
        background: url('front/imgs/green_tick.png') no-repeat top left;
        height: 32px;
        line-height: 32px;
        padding-left: 40px;
        font-size: 14px;
        padding-bottom: 10px;
    }
    .roleftcol li.greenarrow32 {
        list-style-type: none;
        background: url('front/imgs/img763.png') no-repeat top left;
        height: 32px;
        line-height: 32px;
        padding-left: 40px;
        font-size: 14px;
        padding-bottom: 5px;
    }
    .roleftcol li.greenarrow32 em{
        line-height: 20px;
    }
    .virtualofficeoneclick {
        border: 2px dashed #000;
        padding: 10px;
        margin-top: 20px;
        background-color: #fff9e6;
    }
    .guarantee {
        margin: 20px 0;
    }
    .testimonials {
        color: #666;
        text-align: left;
        font-style: italic;
    }
    .testimonialname {
        font-weight: bold;
        text-align: right;
    }
    h2.arrow48 {
        background: url('front/imgs/img761.png') no-repeat top left;
        height: 48px;
        line-height: 48px;
        padding-left: 48px;
    }
    .questionicon {
        background: url('front/imgs/img765.png') no-repeat center right;
        height: 24px;
        line-height: 24px;
        padding-right: 30px;
    }
    .questionicontest {
        background: url('front/imgs/img764.png') no-repeat top left;
        height: 32px;
        line-height: 32px;
        padding-right: 32px;
    }
    .list_item .help-button {
        padding-top: 8px;
        margin-left: 5px;
        margin-right: 5px;
    }

    label {
        font-size: 14px;
    }

    li #productType1270{
        vertical-align: middle;
        margin-bottom: 4px;
        height: 31px;
    }
    li #productType1271{
        vertical-align: middle;
        margin-bottom: 4px;
        height: 10px;
    }
</style>
<script type="text/javascript">

    $(document).ready(function () {

        $('form[name$="virtual_office"]').live('submit',function(){

            _gaq.push(['_trackEvent', 'Links', 'Click', 'Get Virtual Office Service']);

            var self = $(this);
            var dataForm = $(self).serialize();
            $.ajax({
                type: "POST",
                url: self.attr('action'),
                data: dataForm,
                dataType: "json",
                success: function(data){
                    if(data.error ==1) {

                        var $submitButton =  $('#submit');
                        if ($submitButton.hasClass('ladda-button')) {
                            $submitButton.ladda().ladda('stop');
                        }

                        if(data.message) {
                            $(".redmsg").html(data.message.productType);
                        }
                    }
                    if(data.popup == 1) {
                        payment(data.link);
                        return false;
                    }

                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                   $(".redmsg").html('Oops...an error occurred (form error)');
                },
                timeout: function () {
                    $(".redmsg").html('Oops...an error occurred (form timeout)');
                }
            });
            return false;
        });

        $('.meeting_room').click(function(){
            var url = $(this).attr('href');
            meeting_room(url);
            return false;
        });

        $('.mail_answering').click(function(){
            var url = $(this).attr('href');
            mail_answering(url);
            return false;
        });
        function meeting_room(url){
            $overlay = $('<div class="virtual_office_mail_answering virtual_office_meeting_room" ><iframe id="overlay_iframe" src="' + url + '" style="margin-left: 20px; margin-top: 20px; " frameborder="0" width="780" height="570"></iframe></div>');
            $('body').append($overlay);
            $overlay.overlay({
                effect: 'apple',
                load: true,
                left:"center",
                mask: '#000',
                oneInstance: false,
                api : true,
                top : "0px"
            });
        };

        function mail_answering(url){
            $overlay = $('<div class="virtual_office_mail_answering" ><iframe id="overlay_iframe" src="' + url + '" style="margin-left: 20px; margin-top: 10px; " frameborder="0" width="570" height="460"></iframe></div>');
            $('body').append($overlay);
            $overlay.overlay({
                effect: 'apple',
                load: true,
                left:"center",
                mask: '#000',
                oneInstance: false,
                api : true,
                top : "0px"
            });
        };

        function payment(url){
            $overlay = $('<div class="virtual_office_mail_answering payment_virtual_office" ><iframe id="overlay_iframe" src="' + url + '" style="margin-left: 20px; margin-top: 10px; " frameborder="0" width="700" height="570"></iframe></div>');
            $('body').append($overlay);
            $overlay.overlay({
                effect: 'apple',
                load: true,
                left:"center",
                mask: '#000',
                oneInstance: false,
                api : true,
                top : "0px",
                height: "500px",
                onClose: function(){
                    window.location.reload(true);
                },
                onBeforeLoad: function() {
                    // grab wrapper element inside content
                    var wrap = this.getOverlay().find(".contentWrap");

                    // load the page specified in the trigger
                    wrap.attr('src', this.getTrigger().attr("href"));
                }

            });
        };

    });
</script>
{/literal}


{* GUIDE *}
<div class="registeredofficeservice">
    <h2 style="font-size:29px;">One-Time Offer: Get up to 50% off Virtual Office services for 3 months</h2>
    <h2 style="font-weight: normal;margin-bottom: 15px;font-size: 18px;">Get a prestigious business address with same-day mail forwarding for only &pound;10 per month</h2>
    <div class="roimage">
        <img width="400" height="300" alt="Companies Made Simple Offices" src="{$urlImgs}register_office_photo.jpg" style="border: 2px solid #ddd;">
        <p><strong>Over 19,000 companies have used our prestigious address as their Virtual Office since 2002.</strong><br><span>20-22 Wenlock Road, London, N1 7GU</span>
            <a target="_blank"href="http://maps.google.com/?q=20-22+Wenlock+Road,+London,+N1+7GU">Map</a></p>
        <div style="margin-top: 190px;">
            <p style="text-align: left;"><strong>Testimonials</strong></p>
            <p class="testimonials">"If you are looking for a mail forwarding service at an affordable price, look no further! This is the best virtual office package I have tried so far!"</p>
            <p style="padding-top: 0;" class="testimonialname">Vidas, Lithuania</p>

            <p class="testimonials">"Outstanding service, great value, flexibility and people who are prepared to go beyond the call of duty"</p>
            <p style="padding-top: 0;" class="testimonialname">Chris, United Kingdom</p>

            <p class="testimonials">"Excellent service, always courteous and helpful... worth double the money!"</p>
            <p style="padding-top: 0;" class="testimonialname">Nav, France</p>
        </div>
    </div>
    <div class="roleftcol">
        <h2>What is the Virtual Office service?</h2>
        <p>Our Virtual Office service gives you a prestigious business address without the high costs of renting a physical office. The service gives you:</p>
        <ul style="margin-top: 10px;">
            <li class="greenarrow32">Prestigious business address in central London</li>
            {assign var="companyId" value=$company->getCompanyId()}
            <li class="greenarrow32">
                <div  class="list_item">
                    <div style="float:left;">
                        Same-day <a href="{$this->router->link("VirtualOfficeOldControler::MAIL_FORWARDING_PAGE", "company_id=$companyId")}" class="mail_answering">mail forwarding</a>
                        by first class post
                    </div>
                    <div class="help-button">
                        <a href="#" class="help-icon-white-bg">help</a>
                        <em>All mail will be forwarded to you by 1st class post (we&#39;ll also block junk mail)</em>
                    </div>
                </div>
            </li>
            <li class="greenarrow32">
                <div class="list_item">
                    <div style="float:left;">
                        A <a href="{$this->router->link("VirtualOfficeOldControler::TELEPHONE_ANSWERING_PAGE", "company_id=$companyId")}" class="mail_answering">professional receptionist</a>
                        to answer your calls using your company's name
                    </div>
                    <div class="help-button">
                        <a href="#" class="help-icon-white-bg">help</a>
                        <em>We&#39;ll answer your calls when you&#39;re busy and email (or text) your messages right away!</em>
                    </div>
                </div>
            </li>
            <li class="greenarrow32">
                <div  class="list_item">
                    <div style="float:left;">
                        <a href="{$this->router->link("VirtualOfficeOldControler::MEETING_ROOM_PAGE", "company_id=$companyId")}" class="meeting_room">Meeting room</a> facilities
                    </div>
                    <div class="help-button">
                        <a href="#" class="help-icon-white-bg">help</a>
                        <em>Professional meeting rooms at your virtual office are available to hire on short notice.</em>
                    </div>
                </div>
            </li>
        </ul>
        <h2 style="margin-top: 15px; ">Top 4 reasons to get a Virtual Office service</h2>
        <ul style="margin-top: 15px;">
            <li class="greentick32">Get <strong>more business</strong> by looking professional and established</li>
            <li class="greentick32"><strong>Protect the privacy</strong> of your personal address</li>
            <li class="greentick32">Get <strong>more sale leads</strong> as you'll never miss a call while you're busy</li>
            <li class="greentick32"><strong>Ideal if you're overseas</strong> and require a UK trading address</li>
        </ul>
        <h2>Why are we making this offer?</h2>
        <p>We want to make it easier for new businesses to try our Virtual Office service and see for themselves how well it works. It's a one-time offer as we can't afford to offer it to everyone all the time!</p>
        <div class="virtualofficeoneclick">
            <h2>Select a package and get your prestigious Virtual Office with one click!</h2>
            <div>
                {$form->getBegin() nofilter}
                <ul>
                    <span class="redmsg" style="font-size: 14px; font-weight: bold; color: #c00000;">{$form->getError('productType')}</span>
                    <li>
                        {$form->getControl('productType') nofilter}
                        <p style="padding-top: 0px; margin-left: 18px;">
                            <span style="font-size: 12px; font-weight: bold; color: #c00000;">Comes with free 0845 Telephone Number!</span> <span style="color: #999;">(usually <span style="text-decoration: line-through;">£24.99/mth</span> save 20%)</span>
                        </p>
                    </li>
                </ul>
                <div class="txtcenter" style="margin:20px 0;">
                    {ui name="button" text="Get Virtual Office Service" size="m" idAttr="submit" loading="expand-right"}
                    {$form->getEnd() nofilter}
                </div>
            </div>
            <div>
                <div style="float: right;margin-top: 3px;">
                    <img width="100" height="23" alt="Sagepay" src="{$urlImgs}sagepay-logo.png">
                </div>
                <div style="float: left;">
                    <p style="padding-top: 0; font-size: 14px; color: #000; font-weight: bold;">Easy one-click payment using card ending {$token->cardNumber}</p>
                    <p style="font-size: 12px;  padding-top: 0; color: #666;">You'll confirm your purchase on the next screen.</p>
                </div>
            </div>
            <div style="clear: both;"></div>
            <p style="color: #999; border-bottom: 1px solid #eee; font-size: 12px;">One-Time Offer Terms</p>
            <p style="font-size: 11px; color: #999; line-height: 17px;">* You'll be billed for three months of service plus VAT and a £20 deposit
                from which postage and handling fees will be deducted (10p per letter &amp; 50p per parcel).
                Telephone answering service includes 10 calls a month.
                After three months, if you choose to continue using the service, you will be charged the standard month rates (mail forwarding: £19.99/mth +VAT,
                mail forwarding &amp; telephone answering: £24.99/mth +VAT).
                You can cancel at any time by providing just one months' notice.
                Proof of identity and address will be required to activate your service (e.g. driver's license and a utility bill).</p>
        </div>
        <div class="guarantee">
            <div style="float: left; width: 100px;">
                <img width="100" height="70" alt="guarantee" src="{$urlImgs}guarantee.png">
            </div>
            <div style="float: left; width: 430px; margin-left: 10px;">
                <p style="padding-top: 12px;"><strong>We guarantee you won't find an equivalent Virtual Office service for less... or we'll give you £50!</strong></p>
            </div>
            <div style="clear: both;"></div>
            <div>
                <p style="text-align: right; padding-top: 0;"><strong>Howard Graham</strong> Founder &amp; Chartered Accountant</p>
            </div>
        </div>
        <p style="text-align: right; margin-top: 40px; margin-right: 60px; font-size: 14px;">
            <span style="font-size: 18px; font-weight: bold;">Questions? Call our experts on 0207 608 5500</span><br>Mon to Fri - 9am to 5.30pm</p>
        <p style="font-size: 14px; margin-top: 20px; color: #666; padding-left: 55px;text-align: center;width: 420px;">
            <a href="{$this->router->link("CFRegisteredOfficeControler::REGISTER_OFFICE_PAGE", "company_id=$companyId")}"><strong>No thanks, Continue to the Company Registration process</strong></a>
            <br>I know it's a one-time offer and I won't be offered it again!
        </p>
        <div style="clear: both;"></div>
    </div>
    <div style="clear: both;"></div>
</div>

{include file="@footer.tpl"}
