{include file="@headerPopUp.tpl"}
{literal}
    <style type="text/css">
        div#maincontent1{
            width: 500px;
        }
        .box-white2{
            width: 365px;
        }
        li.greentick32 {
            list-style-type: none;
            background: url('front/imgs/green_tick.png') no-repeat top left;
            line-height: 32px;
            padding-left: 40px;
            font-size: 14px;
            padding-bottom: 10px;
        }
        ul.ro-stat {
            padding: 0;
            margin:0;
        }
        .green_button_buy {
            margin:0px auto;
            padding: 10px;
            padding-bottom: 5px;
            width: 300px;
            background: url("front/imgs/oneclick_button.png") no-repeat scroll left top transparent;
        }
        .green_button_buy a {
            text-decoration: none;
        }
        .green_button_buy h3 {
            margin: 3px 0;
            text-align: center;
            color: #fff;
            font-size: 20px;
        }
        .green_button_buy p {
            margin-top: 4px;
            text-align: center;
            color: #fff;
            font-size: 14px;
            padding-top: 0;
        }

        .help-button {
            padding-top: 8px;
            margin-left: 5px;
            margin-right: 5px;
        }

    </style>
{/literal}
<div style="margin:0px auto; width:530px;">
    <h1 style="color: #000; text-align: center; ">{$title}</h1>
    <h2 style="color: #000; text-align: center; ">Virtual Office - {$product->page->title}</h2>
    <div style="margin-left: 60px;">
        <ul class="ro-stat">
            <li class="greentick32">{$product->page->abstract nofilter}</li>
            <li class="greentick32">Your purchase will be completed with a single click!</li>
            <li class="greentick32">Your Virtual Office Service will be ready to use in just 48 hours.</li>
        </ul>
        <div style=" margin: 0px 0 10px 10px; font-size: 14px; line-height: 20px;">
            <div style="float: left;width: 350px;"><p>{$product->page->title}</p>
                <p style="font-size: 12px;  padding-top: 0; color: #666;">3 months service x £{$product->price/3} per month</p>
            </div>
            <div style="float: left"><p>{$product->price|string_format:"%.2f"}</p></div>
            <div style="float: left; width: 350px;"><p>VAT (20%)</p></div>
            {assign var="vat" value=$product->price*0.2}
            <div style="float: left; text-decoration:underline"><p>{$vat|string_format:"%.2f"}</p></div>
            <div style="float: left; width: 350px;"><p>Total</p></div>
            <div style="float: left;"><p>{($product->price+$vat)|string_format:"%.2f"}</p></div>
            <div style="float: left; width: 350px;">
                <div style="float: left;">
                    <p>Postage Deposit</p>
                </div>
                <div class="help-button">
                    <a href="#" class="help-icon-white-bg">help</a>
                    <em>Postage and handling fees will be deducted from your postage deposit
                        (10p per letter & 50p per parcel).
                        We'll email you whenever it needs to be topped up.</em>
                </div>
            </div>
            <div style="float: left; text-decoration:underline;"><p>{($product->associatedPrice-$product->price)|string_format:"%.2f"}</p></div>
            <div style="float: left;width: 350px;"><p>Total Payable (&pound;)</p></div>
            <div style="float: left;"><p style="font-weight: bold; text-decoration:underline;">{($product->associatedPrice+$vat)|string_format:"%.2f"}</p></div>
            <div style="clear: both;"></div>
        </div>
    </div>


    <form id="token_form" action="{$this->router->link("TokensControler::TOKEN")}" method="get" class="txtcenter" style="margin: 20px 0;">
        {ui name="button" size="s" text="Buy Instantly*" loading="expand-right"}
        <input type="hidden" name="add" value="{$product->nodeId}">
        <input type="hidden" name="company" value="{$company->getCompanyId()}">
    </form>

    {* Alberto: can be deleted when you are finished with styling *}
    {*<div class="green_button_buy">
        {assign var="productId" value=$product->nodeId}
        {assign var="companyId" value=$company->getCompanyId()}
        <a href="{$this->router->link("TokensControler::TOKEN", "add=$productId", "company=$companyId")}" onclick="_gaq.push(['_trackEvent', 'Links', 'Click', 'Buy {$product->page->title}']);">
            <h3>Buy Instantly</h3>
            <p>(One-Click Ordering)</p>
        </a>
        <p style="padding-top: 13px; font-size: 12px; color: #666;">One click ordering using card ending x{$token->cardNumber}</p>
    </div>
    *}

    <div style="margin-left: 70px; width: 400px; margin-top: 10px;">
        <div style="float: right;">
            <img src="{$urlImgs}sagepay-logo.png" alt="Sagepay" width="80" height="18" />
        </div>
        <div style="float: left;">
            <p style="padding-top: 0; font-size: 12px; color: #666;">*One-Click Ordering using card ending {$token->cardNumber}</p>
	</div>
    </div>
    <div style="clear: both;"></div>
    <div style="margin-left: 70px; width: 400px;">
        {$product->page->text nofilter}
    </div>
</div>


<script>
    $('#token_form').on('click', function(){
        _gaq.push(['_trackEvent', 'Links', 'Click', 'Buy {$product->page->title}']);
    });
</script>

{include file="@footerPopUp.tpl"}
