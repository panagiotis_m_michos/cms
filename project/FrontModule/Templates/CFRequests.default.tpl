{include file="@header.tpl"}

{if $visibleTitle}
    <h1>{$title}</h1>
{/if}

<h2>Company Information</h2>

<table class="grid" width="100%">
	<col width="150">
	<tr>
		<th>Company Name</th>
		<td>{$company->getCompanyName()}</td>
	</tr>
	<tr>
		<th>Company Number</th>
		<td>{$company->getCompanyNumber()}</td>
	</tr>
	<tr>
		<th>Request type</th>
		<td>{$request->getFormIdentifier()}</td>
	</tr>
	<tr>
		<th>Date Submitted</th>
		<td>{$request->getSubmissionDate()|date_format:"%d/%m/%Y %H:%M"}</td>
	</tr>
	<tr>
		<th>Status</th>
		<td>{$company->getStatus()}</td>
	</tr>
</table>

{include file="@footer.tpl"}
