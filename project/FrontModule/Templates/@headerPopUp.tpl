<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <meta http-equiv="expires" content="-1"/>
    <meta http-equiv="content-language" content="{$currLng}"/>
    <meta name="robots" content="all,index,follow"/>
    <meta name="googlebot" content="index,follow,archive"/>
    <meta name="google-site-verification" content="-1-p8maOYMvZm3W9KBdEl_u-kRSHsehAw4OrVCZG2eA"/>
    <meta name="author" content="{$author}"/>
    <meta name="copyright" content="{$copyright}"/>
    <meta name="allow-search" content="yes"/>
    <meta name="rating" content="general"/>
    <meta name="distribution" content="global"/>
    <meta http-equiv="revisit-after" content="5 days"/>
    <meta name="Abstract" content="Company Formation Services"/>

    {if !empty($company4head)}
        <meta name="description" content="{$company4head.CompanyName}, {if !empty($address.AddressLine.0)}{$address.AddressLine.0}{/if} {if !empty($address.AddressLine.1)}{$address.AddressLine.1}{/if} {if !empty($address.AddressLine.2)}{$address.AddressLine.2}{/if} {if !empty($address.AddressLine.3)}{$address.AddressLine.3}{/if}"/>
        <meta name="keywords" content="{$company4head.CompanyName}, company report"/>
        <title>{$company4head.CompanyName} - {$seoTitle}</title>
    {else}
        <meta name="description" content="{$description}"/>
        <meta name="keywords" content="{$keywords}"/>
        <title>{$seoTitle}</title>
    {/if}

    <link rel="stylesheet" href="{$urlComponent}jquery-ui/themes/base/minified/jquery-ui.min.css" type="text/css"/>
    <link rel="stylesheet" href="{$urlCss}popup.css" type="text/css"/>
    <link rel="stylesheet" href="{$urlCss}htmltable.css" type="text/css"/>
    <link rel="stylesheet" href="{$urlCss}accordion.css" type="text/css"/>
    <link rel="stylesheet" href="{$urlCss}stylesheet.css" type="text/css"/>
    <link rel="stylesheet" href="{$urlCss}forms.css" type="text/css"/>
    <link rel="stylesheet" href="{$urlCss}project.css" type="text/css"/>
    <link rel="stylesheet" href="{$urlCss}bootstrap_fix.css" type="text/css"/>
    <link rel="shortcut icon" href="{$urlImgs}favicon.ico" type="image/x-icon"/>
    <link rel="icon" href="{$urlImgs}favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="{$urlComponent}ladda/dist/ladda-themeless.min.css" type="text/css"/>
    <link rel="stylesheet" href="{$urlComponent}ui_server/css/button.css" type="text/css"/>

    <script type="text/javascript" src="{$urlComponent}jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="{$urlComponent}jquery-ui/ui/minified/jquery-ui.custom.min.js"></script>
    <script type="text/javascript" src="{$urlComponent}jquery-migrate/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="{$urlJs}jquery.tools.min.js"></script>
    <script language="JavaScript" src="{$urlJs}plugins/fancycombo.js" type="text/javascript"></script>
    <script language="JavaScript" src="{$urlJs}project.js" type="text/javascript"></script>
    <script language="JavaScript" src="{$urlJs}payment.js" type="text/javascript"></script>
    <script type="text/javascript" src="{$urlComponent}ladda/js/spin.js"></script>
    <script type="text/javascript" src="{$urlComponent}ladda/js/ladda.js"></script>
    <script type="text/javascript" src="{$urlComponent}ladda/js/ladda.jquery.js"></script>
    <script type="text/javascript" src="{$urlComponent}ui_server/js/ui.js"></script>

    {literal}
    <style type="text/css">
        .box-white2,.box-white2-bottom,.box-white2-top {background: none;}
    </style>
    {/literal}
    {if $this->node->getId() == 746}
        {literal}
        <style type="text/css">
            .box-white2,.box-white2-bottom,.box-white2-top {background: none;}
            div#maincontent1 {
                width: 571px;
            }
            .box-white, .box-white2 {
                width: 571px;
            }
        </style>
        {/literal}
    {/if}
    
    {* ECOMERCE *}
	{if isset($ecommerce)}
        
        <script type="text/javascript">
       try{ldelim}
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-543198-9']);
        _gaq.push(["_setCustomVar", 1, "user_type", "customer", 1]);
        _gaq.push(['_trackPageview']);

        _gaq.push(['_addTrans',
          "{$basket->orderId}", // order ID - required
          "Companies Made Simple", // affiliation or store name
          "{$basket->getPrice('total')}", // total - required
          "{$basket->getPrice('vat')}", // tax
          "", // shipping
          "", // city
          "", // state or province
          "" // country
        ]);

        {foreach from=$basket->getItems() item="item"} 
            _gaq.push(['_addItem',
              "{$basket->orderId}", // order ID - necessary to associate item with transaction
              "{$item->getId()}", // SKU/code - required
              "{$item->getLngTitle()}", // product name
              "{$item->parentId}", // category or variation
              "{$item->price}", // unit price - required
              "{$item->qty}" // quantity - required
            ]);
        {/foreach}

        _gaq.push(['_trackTrans']); //submits transaction to the Analytics servers
        {rdelim} catch(err) {ldelim}{rdelim}
        {literal}
        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
        </script>
        
        {/literal}
    {else}
        {literal}
        <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-543198-9']);
        _gaq.push(['_trackPageview']);    

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
        </script>
        {/literal}
	{/if}
    
</head>
    
<body>

<!-- Google Tag Manager -->
{literal}<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5RVST5"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5RVST5');</script>{/literal}
<!-- End Google Tag Manager -->

<div id="content" class="popup">
 {include file='@blocks/popupFlashMessage.tpl'}
