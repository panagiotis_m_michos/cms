{include file="@AffiliateHeader.tpl"}

{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}

{$form->getBegin() nofilter}
{$form->getHtmlErrorsInfo() nofilter}
<fieldset id="fieldset_0">
<legend>Login</legend>
<table class="ff_table">
    <tbody>
        <tr>
            <th>{$form->getLabel('email') nofilter}</th>
            <td>{$form->getControl('email') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('email')}</span></td>
        </tr>
        <tr>
            <th>{$form->getLabel('password') nofilter}</th>
            <td>{$form->getControl('password') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('password')}</span></td>
        </tr>
    </tbody>
</table>
</fieldset>
<fieldset id="fieldset_0">
<legend>Action</legend>
<table class="ff_table">
    <tbody>
        <tr>
            <th>{$form->getLabel('submit') nofilter}</th>
            <td>{$form->getControl('submit') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('submit')}</span></td>
        </tr>
        <tr>
            <th></th>
            <td rowspan="2">
                <a href="{$this->router->link("AffiliateControler::FORGOTTEN_PASSWORD_PAGE")}">Forgotten Password? Click here</a>
            </td>
        </tr>
    </tbody>
</table>
</fieldset>
{$form->getEnd() nofilter}

{include file="@AffiliateFooter.tpl"}
