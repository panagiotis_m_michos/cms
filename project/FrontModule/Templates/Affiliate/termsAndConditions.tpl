{if isset($popup) && $popup == 1}
    {include file="@AffiliateHeaderPopUp.tpl"}
{else}
    {include file="@AffiliateHeader.tpl"}
{/if}

{if $visibleTitle}
<h1>{$title}</h1>
{/if}

{$text nofilter}

{if isset($popup) && $popup == 1}
    {include file="@AffiliateFooterPopUp.tpl"}
{else}
    {include file="@AffiliateFooter.tpl"}
{/if}

