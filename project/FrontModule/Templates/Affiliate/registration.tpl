{include file="@AffiliateHeader.tpl"}

{if $visibleTitle}
<h1>{$title}</h1>
{/if}
{$text nofilter}
<div style="float:left;width:500px;">
{$form nofilter}
</div>
<div style="float:left; padding-left: 20px; margin-top: 215px;">
    <h2>Example</h2>
    <div class="block" style="width:340px; padding:10px">
        <form name="test" action="#">
            <label for="companyColour">Company Name: *</label>
            <input type="text" name="text">
            <input type="submit" name="submit" value="Search" style="height:25px; width:55px;">
        </form>
    </div>
</div>


{literal}
<script type="text/javascript">
    <!--
    $(document).ready(function() {
        $(".block").css('background-color', "#" + $("#companyColour").val());
        $(".block").css('color', "#" + $("#textColour").val());
        $("#companyColour").focusout(function() {
            $(".block").css('background-color', "#" + $("#companyColour").val());
        })
        $("#textColour").focusout(function() {
            $(".block").css('color', "#" + $("#textColour").val());
        })
    });
    //-->
</script>
{/literal}
{include file="@AffiliateFooter.tpl"}