<div class="overlay" id="cf-barclays">
        <p style="text-align: center; padding-top: 24px;"><img alt="Barclays" width="353" height="60" src="{$urlImgs}/img510.png" /></p>
        <p style="font-weight: bold; font-size:18px; text-align: center; padding-top: 24px;">
            Open a Barclays Business Bank Account and benefit from:
        </p>
        <ul style="font-size: 14px; color: #666; line-height: 20px;">
            <li style="padding-top: 14px;">{$cashBackAmount|currency:0} cash back when your account is opened</li>
            <li style="padding-top: 14px;">Your own Local Business Manager dedicated to helping you reach your goals</li>
            <li style="padding-top: 14px;">A debit card, cheque book and card payment facilities</li>
            <li style="padding-top: 14px;">Access to free seminars and workshops with business mentors</li>
            <li style="padding-top: 14px;">Award-winning credit management tools to help control your cash flow</li>
        </ul>
        <p style="font-weight: bold; font-size:14px; padding: 20px 24px;">
            Join more than 40,000 other business owners who have opened a business bank account with us this way.
        </p>
</div>     
