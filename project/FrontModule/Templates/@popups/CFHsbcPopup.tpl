<div class="overlay" id="cf-hsbc">
        <p style="text-align: center;">
            <img alt="hsbc" width="269" height="60" src="{$urlImgs}/img410.png" />
        </p>
        <p style="font-weight: bold; font-size:18px; text-align: center; padding-top: 24px;">Open a HSBC Small Business Bank Account and get:</p>
        <ul style="font-size: 14px; color: #666; line-height: 20px;">
            <li style="padding-top: 16px;">Traditional branch services, plus the flexibility of internet and telephone banking.</li>
            <li style="padding-top: 16px;">Up to <span style="font-weight: bold; color: #000;">18 months free business banking</span> for businesses with a turnover below £2 million.</li>
            <li style="padding-top: 16px;">Ideal if you prefer to bank using cash and cheques and want access to a local in-branch Business Specialist.</li>
            <li style="padding-top: 16px;">Plus, free business support and resources through our online Knowledge Centre and Business Network.</li>
        </ul>
</div>     