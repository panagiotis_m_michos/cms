<div class="overlay" id="cf-tsb">
    <p style="text-align: center; padding-top: 24px;"><img alt="TSB" width="129" height="76" src="{$urlImgs}/tsbbank129.png" /></p>
    <p style="font-weight: bold; font-size:18px; text-align: center; padding-top: 24px;">
        Open a TSB Business Account and benefit from: 
    </p>
    <ul style="font-size: 14px; color: #666; line-height: 20px;">
        <li style="padding-top: 14px;">Online application process</li>
        <li style="padding-top: 14px;">18 months' free day-to-day banking</li>
        <li style="padding-top: 14px;">Dedicated UK based customer service and relationship support teams</li>
        <li style="padding-top: 14px;">No minimum account balance during the free banking period</li>
    </ul>
</div>
