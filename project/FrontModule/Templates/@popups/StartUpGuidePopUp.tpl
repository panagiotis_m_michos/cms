{literal}
    <style>
        div#mainguide {
            width:550px;
            float:left;
            margin: 0px 0px 0px 0px;
        }
        #bodyguide {
            height:310px;
            margin:10px 30px 0 10px;
            background: url("/project/upload/imgs/img972.jpg") no-repeat;
        }
        .line-height-20 {
            line-height: 20px;
        }
        .padding-top-16 {
            padding-top: 16px;
        }
        .font-size-14 {
            font-size: 14px;
        }
        .font-size-34 {
            font-size: 34px;
        }
        .grey {
            color: #3a3a3a;
        }
        .float-right {
            float: right;
        }
    </style>
{/literal}
<div class="overlay" id="mainguide">
    <div id="bodyguide">                  
        <div class="float-right" style="width:350px;">
            <p class="h2 grey font-size-34">Your guide is on its way!</p>
            <p class="h3 grey line-height-20 padding-top-16 font-size-14" style="padding-top:25px;">Thanks for signing up for the Ultimate Small Business Start Up Guide.</p>
            <p class="h3 grey line-height-20 padding-top-16 font-size-14">Keep an eye on your inbox, an email should be with you in the next minute or two.  If you don't receive it, please check your SPAM filter.</p>
            <p class="h3 grey line-height-20 padding-top-16 font-size-14">If you have any questions, feel free to <a href="/support/?/Tickets/Submit/RenderForm/22" target="_top">contact us.</a></p>

        </div>
    </div>
    <div class="clear"></div>
</div>
