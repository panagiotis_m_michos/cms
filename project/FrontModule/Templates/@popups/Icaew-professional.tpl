<div class="overlay" id="icaew-professional">
    <h2 style="font-size: 24px;font-weight: bold;padding-bottom: 10px;color: #666">
        Professional Wholesale Company Formation Loyalty Scheme
    </h2>
    <p style="font-size: 14px;color: #666">
        If you are registered with a ‘wholesale’ company formation user account, then you are eligible to take advantage of our Professional Wholesale 
        Company Formation Loyalty Scheme. Effectively, for every 10 companies you purchase, you receive a £20 credit to your account. 
        The £20 is automatically credited to your account. No alternative benefits are available. Companies purchased, must be purchased through 
        the ‘wholesale’ Dashboard in your professional wholesale account rather than through the Companies Made Simple Retail website. 
        Retail site packages are not included within the Loyalty Scheme.
    </p>
</div>    