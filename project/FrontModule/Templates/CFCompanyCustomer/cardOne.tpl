{include file="@header.tpl"}
<div id="maincontent2">
{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
	{if $customer->isWholesale() && $customer->tagId !== 'AFFILIATE'}
        <div style="margin-left: 0pt; width: 753px; " class="flash info2">
		You have opted to include a <b>Card One Business Account</b> with this company formation. <br>
		<br>
		Please enter your preferred contact details below. If you would prefer Card One to contact your client directly, please enter their details below. Once the company is formed a member of the Card One team will be in touch within 24 hours.<br>
		<br>
		If you do not want to be contacted by Card One you can remove this service.<br>
			<a href="{if $company->isIncorporated()}
				    {url route="banking_module.choose_bank" companyId=$company->getId()}
			    {else}
			        {$this->router->link(CFCompanyCustomerControler::PAGE_CHOOSE_BANK_ACCOUNT, ['company_id' => $company->getId(), 'selected' => 'CARD_ONE'])}
			    {/if}">Go back and choose different option</a>.
	</div>
	{else}
        <div style="margin-left: 0pt; width: 753px; " class="flash info2">
		You have opted to include a <b>Card One Business Account</b> with this company formation. <br>
		<br>
        Please enter your preferred contact details below. A member of the Card One team will be in touch with you in the next 24 hours to discuss your account requirements.<br>
		<br>
		If you do not want to be contacted by Card One you can remove this service.
		<a href="{if $company->isIncorporated()}
				    {url route="banking_module.choose_bank" companyId=$company->getId()}
			    {else}
			        {$this->router->link(CFCompanyCustomerControler::PAGE_CHOOSE_BANK_ACCOUNT, ['company_id' => $company->getId(), 'selected' => 'CARD_ONE'])}
			    {/if}">go back and choose different option</a>.
    </div>
	<div style="clear:both;"></div>
	{/if}
{$form nofilter}
</div>
{include file="@footer.tpl"}
