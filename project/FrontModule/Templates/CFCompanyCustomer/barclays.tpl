{include file="@header.tpl"}

{* INLINE CSS *}
<style type="text/css">/*<![CDATA[[*/{literal}

.barclays-hp-banner{
	float:left;
	margin-left:10px;
	margin-top:0px;
}
/*]]>*/</style>{/literal}


<div id="maincontent">
	{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
	{if $customer->isWholesale()}
        <div style="margin: 0; width: auto" class="flash info2">
			You have opted to include a <b>Fast Track Barclays Bank Account</b> with
			<span style="font-size:14px; font-weight: bold; font-style: italic; color: #ff6600;">{$cashBackAmount|currency:0} cash back</span> with this company formation. <br>
			<br>
			Please enter your clients contact details below. This is so Barclays will contact them directly once their company has been formed.
			If you would like Barclays to contact you instead, please enter your details.<br>
			<br>
			If you do not want {$cashBackAmount|currency:0} cash back via a Barclays Business Bank Account, you can
			<a href="{if $company->isIncorporated()}
				    {url route="banking_module.choose_bank" companyId=$company->getId()}
			    {else}
			        {$this->router->link(CFCompanyCustomerControler::PAGE_CHOOSE_BANK_ACCOUNT, ['company_id' => $company->getId(), 'selected' => 'BARCLAYS'])}
			    {/if}">
                go back and choose different option</a>.<br><br>
			NB. The {$cashBackAmount|currency:0} cash back payment is only made when your client successfully opens a Barclays Bank Account. Once successful account opens are reconciled, {$cashBackAmount|currency:0} will be added as credit to your companiesmadesimple.com account.
		</div>
	{else}
        <div style="margin: 0; width: auto" class="flash info2">
			You have opted to apply for a <b>Fast Track Barclays Business Bank Account</b> with
			<span style="color: #ff6600; font-style: italic; font-weight: bold; font-size: 14px;">{$cashBackAmount|currency:0} cash back</span>
			Barclays will contact you within 48 hours -<b> please ensure your contact details are correct.</b><br>
			<br> If you do not want a Barclays Bank Account with {$cashBackAmount|currency:0} cash back, you can <a href="{if $company->isIncorporated()}
				    {url route="banking_module.choose_bank" companyId=$company->getId()}
			    {else}
			        {$this->router->link(CFCompanyCustomerControler::PAGE_CHOOSE_BANK_ACCOUNT, ['company_id' => $company->getId(), 'selected' => 'BARCLAYS'])}
			{/if}">go back and choose different option</a>.<br>
			<br> If you would like to change the contact information that Barclays will use, please do so below:
        </div>
	{*
    <div  class="barclays-hp-banner">
		<span>with all our <strong>Company Formation</strong> packages when you open a Barclays <strong>Business Bank Account!</strong></span>
	</div>
    *}
	<div style="clear:both;"></div>
	{/if}
	{$form->getBegin() nofilter}
	{$form->getHtmlErrorsInfo() nofilter}
	<fieldset id="fieldset_1">
		<legend>What are the best details for Barclays to use to contact you?</legend>
		<table class="ff_table">
			<tbody>
				<tr>
					<th>{$form->getLabel('email') nofilter}</th>
					<td>{$form->getControl('email') nofilter}</td>
					<td><span class="ff_control_err">{$form->getError('email')}</span></td>
				</tr>
				<tr>
					<th>{$form->getLabel('titleId') nofilter}</th>
					<td>{$form->getControl('titleId') nofilter}</td>
					<td><span class="ff_control_err">{$form->getError('titleId')}</span></td>
				</tr>
				<tr>
					<th>{$form->getLabel('firstName') nofilter}</th>
					<td>{$form->getControl('firstName') nofilter}</td>
					<td><span class="ff_control_err">{$form->getError('firstName')}</span></td>
				</tr>
				<tr>
					<th>{$form->getLabel('lastName') nofilter}</th>
					<td>{$form->getControl('lastName') nofilter}</td>
					<td><span class="ff_control_err">{$form->getError('lastName')}</span></td>
				</tr>
				<tr>
					<th>{$form->getLabel('phone') nofilter}</th>
					<td>{$form->getControl('phone') nofilter}
                        <span style="color:#006699" class="ff_desc"><b>Mobile</b> preferred. Barclays will contact you Mon-Fri 9am-5pm within 48 hours from your company formation.</span>
                        <span class="ff_desc">(The following formats are allowed: 01234567890, +441234567890, 00441234567890)</span></td>
					<td><span class="ff_control_err">{$form->getError('phone')}</span></td>
				</tr>
				<tr>
					<th>{$form->getLabel('additionalPhone') nofilter}</th>
					<td>{$form->getControl('additionalPhone') nofilter}<span style="color:#006699" class="ff_desc">A second number makes it even easier to get in touch (mobile/work/home etc).</span>
										<span class="ff_desc">(The following formats are allowed: 01234567890, +441234567890, 00441234567890)</span></td>
					<td><span class="ff_control_err">{$form->getError('additionalPhone')}</span></td>
				</tr>
			</tbody>
		</table>
	</fieldset>
	<fieldset id="fieldset_2">
		<legend>Where would you like to have your Barclays branch close to?</legend>
		<table class="ff_table">
			<tbody>
				<tr>
					<th>{$form->getLabel('address1') nofilter}</th>
					<td>{$form->getControl('address1') nofilter}</td>
					<td><span class="ff_control_err">{$form->getError('address1')}</span></td>
				</tr>
				<tr>
					<th>{$form->getLabel('address2') nofilter}</th>
					<td>{$form->getControl('address2') nofilter}</td>
					<td><span class="ff_control_err">{$form->getError('address2')}</span></td>
				</tr>
				<tr>
					<th>{$form->getLabel('address3') nofilter}</th>
					<td>{$form->getControl('address3') nofilter}</td>
					<td><span class="ff_control_err">{$form->getError('address3')}</span></td>
				</tr>
				<tr>
					<th>{$form->getLabel('city') nofilter}</th>
					<td>{$form->getControl('city') nofilter}</td>
					<td><span class="ff_control_err">{$form->getError('city')}</span></td>
				</tr>
				<tr>
					<th>{$form->getLabel('county') nofilter}</th>
					<td>{$form->getControl('county') nofilter}</td>
					<td><span class="ff_control_err">{$form->getError('county')}</span></td>
				</tr>
				<tr>
					<th>{$form->getLabel('postcode') nofilter}</th>
					<td>{$form->getControl('postcode') nofilter}<span class="ff_desc">Please add 1 space in the middle</span></td>
					<td><span class="ff_control_err">{$form->getError('postcode')}</span></td>
				</tr>
				<tr>
					<th>{$form->getLabel('countryId') nofilter}</th>
					<td>{$form->getControl('countryId') nofilter}</td>
					<td><span class="ff_control_err">{$form->getError('countryId')}</span></td>
				</tr>
			</tbody>
		</table>
	</fieldset>
    
	<fieldset id="fieldset_3">
		<legend>Consent</legend>
		<table class="ff_table">
			<tbody>
				<tr>
					<td>{$form->getControl('consent') nofilter}</td>
					<td><span class="ff_control_err">{$form->getError('consent')}</span></td>
				</tr>
			</tbody>
		</table>
		<table>
			<tbody>
				<tr>
					<th colspan="2"><b>Privacy:</b> Your contact details are sent to barclays so they can arrange your appointment. They are not used by us or Barclays for any other selling/cross
					 marketing. We do not send, sell or use these details for any other purpose.<br><br>Company Formation MadeSimple and Barclays have entered into a referral arrangement whereby a fee will be paid by Barclays to Company Formation MadeSimple on the successful opening of a Barclays Business Account</th>
				</tr>
			</tbody>
		</table>
	</fieldset>
    
	<fieldset id="fieldset_4">
		<legend>Save and continue</legend>
		<table class="ff_table">
			<tbody>
				<tr>
					<th></th>
					<td>{$form->getControl('login') nofilter}<span class="ff_desc">(Please double check all your details before saving them!)</span></td>
					<td></td>
				</tr>
			</tbody>
		</table>
	</fieldset>
	{$form->getEnd() nofilter}
</div>

{include file="@footer.tpl"}

