{include file="@header.tpl"}

<div id="maincontent">
    {if $visibleTitle}
        <h1>{$title}</h1>
    {/if}
    <div style="margin: 0; width: auto" class="flash info2">
        You've chosen to apply for a TSB Business Current Account.<br /><br />
        Please confirm the details below to enable TSB to send you an email with a link to the online application process and to contact you about your application.<br /><br />
        If you do not want a TSB Business Current Account (with {$cashBackAmount|currency:0} cash back from MadeSimple), you can <a href="{if $company->isIncorporated()}
				    {url route="banking_module.choose_bank" companyId=$company->getId()}
			    {else}
                    {$this->router->link(CFCompanyCustomerControler::PAGE_CHOOSE_BANK_ACCOUNT, ['company_id' => $company->getId(), 'selected' => 'TSB'])}
                {/if}">go back and choose a different option</a>.
    </div>
    <div style="clear:both;"></div>
    {$form->getBegin() nofilter}
    {$form->getHtmlErrorsInfo() nofilter}
    <fieldset id="fieldset_1">
        <legend>What are the best details for TSB to use to contact you?</legend>
        <table class="ff_table">
            <tbody>
                <tr>
                    <th>{$form->getLabel('email') nofilter}</th>
                    <td>{$form->getControl('email') nofilter}</td>
                    <td><span class="ff_control_err">{$form->getError('email')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('titleId') nofilter}</th>
                    <td>{$form->getControl('titleId') nofilter}</td>
                    <td><span class="ff_control_err">{$form->getError('titleId')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('firstName') nofilter}</th>
                    <td>{$form->getControl('firstName') nofilter}</td>
                    <td><span class="ff_control_err">{$form->getError('firstName')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('lastName') nofilter}</th>
                    <td>{$form->getControl('lastName') nofilter}</td>
                    <td><span class="ff_control_err">{$form->getError('lastName')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('phone') nofilter}</th>
                    <td>{$form->getControl('phone') nofilter}
                        <span style="color:#006699" class="ff_desc"><b>Mobile</b> preferred. TSB will contact you Mon-Fri 9am-5pm within 48 hours from your company formation.</span>
                        <span class="ff_desc">(The following formats are allowed: 01234567890, +441234567890, 00441234567890)</span></td>
                    <td><span class="ff_control_err">{$form->getError('phone')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('additionalPhone') nofilter}</th>
                    <td>{$form->getControl('additionalPhone') nofilter}<span style="color:#006699" class="ff_desc">A second number makes it even easier to get in touch (mobile/work/home etc).</span>
                        <span class="ff_desc">(The following formats are allowed: 01234567890, +441234567890, 00441234567890)</span></td>
                    <td><span class="ff_control_err">{$form->getError('additionalPhone')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('countryId') nofilter}</th>
                    <td>{$form->getControl('countryId') nofilter}
                        <span style="color:#006699" class="ff_desc">TSB available only for UK residents</span></td>
                    <td><span class="ff_control_err">{$form->getError('countryId')}</span></td>
                </tr>
            </tbody>
        </table>
    </fieldset>

    <fieldset id="fieldset_3">
        <legend>Consent</legend>
        <table class="ff_table">
            <tbody>
                <tr>
                    <td>{$form->getControl('consent') nofilter}</td>
                    <td><span class="ff_control_err">{$form->getError('consent')}</span></td>
                </tr>
            </tbody>
        </table>
        <table>
            <tbody>
                <tr>
                    <th colspan="2">
                        <b>Data privacy:</b> Your contact details are being sent to TSB so they can make contact with you.
                        They are not used by us for any other selling/cross marketing.
                        We do not send, sell or use these details for any other purpose.
                    </th>
                </tr>
            </tbody>
        </table>
    </fieldset>

    <fieldset id="fieldset_4">
        <legend>Save and continue</legend>
        <table class="ff_table">
            <tbody>
                <tr>
                    <th></th>
                    <td>{$form->getControl('login') nofilter}<span class="ff_desc">(Please double check all your details before saving them!)</span></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    {$form->getEnd() nofilter}
</div>

{include file="@footer.tpl"}

