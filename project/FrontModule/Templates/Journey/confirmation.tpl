{include file="@header.tpl"}

<style>
    {literal}
    .ff_control_err2 {
        color: red;
        font-style: italic;
        font-size: 11px;
    }
    table.formtable2 tr {
        padding: 0;
        margin: 0;
        height: 10px;
    }
    .mainbox{
        padding-top:55px;
        width:590px;
        clear:left;
    }
    h1{
        margin-bottom: 0px;
    }
    .services2 {
        border:1px solid #CCCCCC;
        margin-bottom:4px;
        padding:10px 0 0 5px;
        background: none repeat scroll 0 0 #F8F8F8;
    }
    .services2 .leftpart {
        width:578px;
    }
    .services2 .leftpart .thumb {
        border:1px solid #CCCCCC;
        float:left;
        width:78px;
        margin-right:5px;
    }
    .services2 .leftpart .text {
        width:570px;
        padding-bottom: 15px;
        padding-top: 10px;
    }
    .services2 .leftpart .text h3{
        float: left;
        width: 378px;
        font-size: 15px;
    }
    .services2 .leftpart .text a{
        padding-right: 20px;
    }
    .services2 .leftpart .text input{
        position:absolute;
    }
    .services2 .leftpart .text b{
        padding-right: 10px;
    }
    .services2 .leftpart .sort_question{
        padding-bottom: 10px;
    }
    #content #maincontent2{
        width:980px;
    }
    .journey-container{
        float:left;
        width:600px;
        margin-right:6px;
    }
    .container2{
        float:left;
        width:300px;
    }
    .box-white3 {
        width:280px;
        height:350px;
        margin-bottom:10px;
        background: url("front/imgs/journey-page-confirmation.jpg") top left repeat-y;
        padding:20px 40px 40px 40px;
    }
    .box-white3 .field220 {
        margin-bottom: 15px;
    }
    {/literal}
</style>
<script>
    {literal}
            $(document).ready( function(){
             $('a.home-free-guide').click(function(){
                 var url = $(this).attr('href');
                 $overlay = $('<div class="overlay"><iframe id="overlay_iframe" src="' + url + '?popup=1' + '" frameborder="0" width="600" height="480"></iframe></div>');
                 $('body').append($overlay);
                     $($overlay).overlay({
                         effect: 'apple',
                         load: true,
                         mask: '#000'
                     });
                 return false;
             });
            })
    {/literal}
</script>
<div id="maincontent2">
    {$form->getBegin() nofilter}

    <h1>You have selected the following offers and support</h1>
    <div class="clear"></div>

    {* PRODUCTS ERROR *}
    {if $form->getError("products")}
    	<p class="ff_err_notice">{$form->getError("products")}</p>
    {/if}
    <div class="journey-container">
        <div class="mainbox">

            {foreach from=$products item="product" name="f1"}
                <div class="services2{if $smarty.foreach.f1.last} noborder{/if}">
                <div class="leftpart">
                    {*<div class="thumb">

                    {if !empty($product->imageId)}
                        {$product->image->getHtmlTag("journey") nofilter}
                    {else}
                        <img src="{$urlImgs}blank.png" width="78" height="74" alt="blank" />
                    {/if}
                    </div>*}
                    <div class="text" >
                        <h3>{$product->getLngTitle()}</h3>
                        {*<p>{$product->getLngText()|strip_tags|truncate:250}</p>*}
                        <a href="{$product->getLink()}" class="home-free-guide">More information</a>
                        <b>Remove</b> {$form->getControl($product->getId()) nofilter}
                    </div>
                        <div class="sort_question">
                        <table >
                        <col width="300">
                            {foreach from=$product->getQuestionsArray() key ="questionKey" item="question"}
                                {if !empty($question.question) && !empty($question.helper) && !empty($question.answers)}

                                {assign var="select_element" value=$product->getId()|cat:"_"|cat:$questionKey}
                                        <tr>
                                            <td>{$form->getLabel($select_element) nofilter}</td>
                                            <td>
                                                {$form->getControl($select_element) nofilter}
                                                {if $form->getError($select_element)}
                                                    <span class="ff_control_err2">{$form->getError($select_element)}</span>
                                                {/if}
                                            </td>
                                            <td>
                                                <div class="help-button">
                                                    <a class="help-icon2" href="#">help</a>
                                                    <em style="display: none;">{$question->text2}</em>
                                                </div>
                                            </td>
                                        </tr>
                                {/if}
                            {/foreach}
                            </table>
                        </div>

                </div>
            </div>
            {/foreach}
        </div>
    </div>
    <div class="containera2">
    <b><p style="color: #666666; font-size:14px; text-align:center;">Please confirm your details below.</p></b>
    <p  style="color: #666666; font-size:14px; text-align:center;">Further information will be emailed to you.</p>
            <div class="box-white3 fright mleft" style="margin-bottom: 0;">
            	<table class="formtable2">
            	<col width="150">
            	<tr>
            		<td align="center" height="40px"><b style="font-size:16px;">Contact Details</b></td>
            	</tr>
            	<tr>
            		<td>{$form->getLabel("email") nofilter}
                        {if $form->getError("email")}
            			<span class="ff_control_err2">{$form->getError("email")}</span>
            			{/if}
            			{$form->getControl("email") nofilter}
            		</td>
            	</tr>
            	<tr>
            		<td>{$form->getLabel("fullName") nofilter}
            			{if $form->getError("fullName")}
            			<span class="ff_control_err2">{$form->getError("fullName")}</span>
            			{/if}
            			{$form->getControl("fullName") nofilter}
            		</td>
            	</tr>
            	<tr>
            		<td>{$form->getLabel("phone") nofilter}
            			{if $form->getError("phone")}
            			<span class="ff_control_err2">{$form->getError("phone")}</span>
            			{/if}
            			{$form->getControl("phone") nofilter}
            		</td>
            	</tr>
            	<tr>
            		<td>{$form->getLabel("postcode") nofilter}
            			{if $form->getError("postcode")}
            			<span class="ff_control_err2">{$form->getError("postcode")}</span>
            			{/if}
            			{$form->getControl("postcode") nofilter}
            		</td>
            	</tr>
            	<tr>
            		<td>{$form->getLabel("company_name") nofilter}
            			{if $form->getError("company_name")}
            			<span class="ff_control_err2">{$form->getError("company_name")}</span>
            			{/if}
            			{$form->getControl("company_name") nofilter}
            		</td>
            	</tr>
            	<tr>
                    <td><a href="{$this->router->link(473)}">Terms &amp; Conditions Apply</a>
                    </td>
                </tr>
            	</table>
            </div>
    </div>

</div>
{$form->getControl("send") nofilter}
 
<p class="mleft">No services required. Please <a href="{$this->router->link("DashboardCustomerControler::DASHBOARD_PAGE")}">continue</a></p>

    {$form->getEnd() nofilter}
{include file="@footer.tpl"}
