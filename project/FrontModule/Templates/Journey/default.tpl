{include file="@header.tpl"}
<style>
    {literal}
    .ff_control_err2 {
        color: red;
        font-style: italic;
        font-size: 11px;
    }
    .services2 {
        height:50px;
        margin-bottom:4px;

    }
    .services2 img {
        border:none;
        border-right:1px solid #CCCCCC;
    }
    .services2 .leftpart {
        width:310px;

    }
    .services2 .leftpart .text {
        position: absolute;
        width:225px;
    }
    .services2 .leftpart .text p {
        font-size: 15px;
        padding-bottom: 5px;
        color: #000;
        font-style: normal;
    }
    .services2 .leftpart .moreInfo{
        position:absolute;
        padding-top: 30px;
    }
    .services2 .leftpart .moreInfo a {
        padding-right:100px;
    }
    .services2 .leftpart .moreInfo label {
        padding-right:10px;
        color: #666;
    }
    .services2 .leftpart .moreInfo input {
        position:absolute;
        margin:0;
        padding:0;
    }
    #content #maincontent2{
        width:975px;
        padding-left: 5px;
    }
    .journey-products-container{
        float:left;
        width:233px;
        padding-right: 10px;
    }

    .header{
        font-size: 20px;    
        padding-bottom: 10px; 
        padding-left: 33px;
        background-position:0px 0px;
        background-repeat:no-repeat;
  
    }
    .header span{  
        padding-bottom: 5px; 
    }
    .mainbox{
        border-top: 1px solid #CCCCCC 
    }

    .overlay{
        padding:26px;
        width:615px;
        overflow:hidden;
    }

    .submitted-notice {
        padding: 2em 2em 2em 9em;
        border-radius: 5px;
        background: url('/front/imgs/green_tick.png') #DFF0D8 no-repeat 3em 3em;
        font-size: 1.1em;
        color: #46885F;
        margin-bottom: 1em;
    }

    .submitted-notice > h5 {
        color: #46885F;
    }

    .btn-continue {
        display: block;
        background: url('/front/imgs/btn_more.gif') center center no-repeat;
        width: 155px;
        height: 24px;
        margin: auto;
        color: #ffffff;
        text-decoration: none;
        font-size: 15px;
        font-weight: bold;
        text-align: center;
        padding: 5px 5px 0 0;
    }
    {/literal}
</style>
<script>
    {literal}
    $(document).ready( function(){
        $('a.home-free-guide').click(function(){
            var url = $(this).attr('href');
            $overlay = $('<div class="overlay"><iframe id="overlay_iframe" src="' + url + '?popup=1' + '" frameborder="0" width="617" height="477"></iframe></div>');
            $('body').append($overlay);
            $($overlay).overlay({
                effect: 'apple',
                load: true,
                mask: '#000'
            });
            return false;
        });
    })
    {/literal}
</script>
<div id="maincontent2">
    {if !empty($isSubmitted)}
        <div class="submitted-notice">
            <h5>Company information submitted to Companies House!</h5>
            Your company is now pending with Companies House. The usual incorporation time is about 3 working hours (in rare cases it can be more). We will email you as soon as your company has been formed.
        </div>
    {/if}

    {if !$isWholesale}
        {$form->getBegin() nofilter}
        <h1>Free Business Advice and Support</h1>
        <p class="midfont noptop" style="color: #666666">To take advantage of free offers and support services, simply tick the offers of interest below. Details will be emailed to you directly.</p>
        <div class="clear"></div>

        {* PRODUCTS ERROR *}
        {if $form->getError("products")}
        <p class="ff_err_notice">{$form->getError("products")}</p>
        {/if}
        <div style="float:left; margin-top:24px;">
            {foreach from=$columns  item="categories"}

            {foreach from=$categories key="key2" item="products"}
            {assign var="key3" value=$key2}


            <div class="journey-products-container {$key3|replace:' ':'_'|replace:'&_':''}">
                {if !empty($products)}
                <div class="header"><span>{$key2}</span>
                </div>
                {/if}
                <div class="mainbox">
                    {foreach from=$products item="product" name="f1"}
                    <div class="services2{if $smarty.foreach.f1.last} noborder{/if}">
                        <div class="leftpart">
                            <div class="text">
                                <p>{$product->getLngTitle()}</p>
                                {*<p>{$product->getLngText()|strip_tags|truncate:250}</p>*}
                            </div>
                            <div class="moreInfo">
                                <a class="home-free-guide" href="{$product->getLink()}">More</a>
                                {$form->getlabel($product->getId()) nofilter}{$form->getControl($product->getId()) nofilter}
                            </div>
                        </div>
                    </div>
                    {/foreach}
                </div>
            </div>
            {/foreach}

            {/foreach}
        </div>
        <div style="float:right;">
            {$form->getControl("send") nofilter}
            <p class="mleft fright" style="clear: right; padding: 10px 0; font-size: 14px;"> <a href="{$this->router->link("DashboardCustomerControler::DASHBOARD_PAGE")}">No thanks, continue</a></p>
        </div>
        {$form->getEnd() nofilter}
    {else}
        <a class="btn-continue" href="{$this->router->link("DashboardCustomerControler::DASHBOARD_PAGE")}">Continue</a>
    {/if}
</div>
{include file="@footer.tpl"}
