<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"> 
<head> 
	<title>{$title}</title>
	
	<script>
	var url = "{$url}"
	{literal}
	if(self != top) {
		top.location = self.location
	} else {
		document.location.href = url 
	}
	{/literal}
	</script>
	 
</head> 
<body>
<p>redirecting ...</p>
</body>
</html>