{include file="@header.tpl"}

<div id="maincontent1" style="padding-bottom: 25px;">

    {if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
    {$text nofilter}
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="grid2">
	<tr>
		<th width="310">Product</th>
		<th width="75" align="center" valign="top">Quantity</th>
		<th width="75">Price</th>
		<th width="75">VAT</th>
		<th width="75">Total</th>
	</tr>
	
	{foreach from=$basket->items key="key" item="item"}
	<tr>
		<td>{$item->getLngTitle()}</td>
		<td align="center" valign="top">{$item->qty}</td>
		<td>&pound;{$item->totalPrice}</td>
		<td>&pound;{$item->vat}</td>
		<td>&pound;{$item->totalVatPrice}</td>
	</tr>
	{/foreach}
	
	{* SHOW VOUCHER *} 
	{if $basket->voucher}
	<tr>
		<td>Voucher: {$basket->voucher->name}</td>
		<td align="center" valign="top">1</td>
		{assign var="subTotal" value=$basket->getPrice('subTotal')}
		<td style="color: red;">-&pound;{$basket->voucher->discount($subTotal)}</td>
		<td>&pound;0.00</td>
		<td>-&pound;{$basket->voucher->discount($subTotal)}</td>
	</tr>
	{/if}

	<tr class="midfont">
		<td><strong>Total including delivery charges and tax</strong></td>
		<td align="left" valign="top">&nbsp;</td>
		
		{if $basket->voucher}
			<td><strong>&pound;{$basket->getPrice('subTotal2')}</strong></td>
		{else}
			<td><strong>&pound;{$basket->getPrice('subTotal')}</strong></td>
		{/if}
		
		<td><strong>&pound;{$basket->getPrice('vat')}</strong></td>
		<td><strong>&pound;{$basket->getPrice('total')}</strong></td>
	</tr>
	</table>
	
	<p><a href="#" onclick="window.print(); return false;">print summary</a></p>
	<p>At Companies Made Simple we value your feedback and suggestions - so, tell us what you think by taking this very quick customer service survey. Only 5 questions - so will take just a couple of minutes to complete. Many thanks. <a href="http://www.surveymonkey.com/s.aspx?sm=XvCHnaZgKdU2B4DLAF48iw_3d_3d" target="_blank">Please click here to take the survey</a>.</p>
</div>
<div class="clear"></div>
{include file="@footer.tpl"}