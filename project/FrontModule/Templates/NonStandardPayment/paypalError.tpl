{include file="@header.tpl"}

{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}

{if isset($errorMessage)}
	<p style="padding: 0 0 25px 0;">
		<b style="color: red;">Error message:</b> <br /> 
		{$errorMessage}
	</p>
{/if}

{include file="@footer.tpl"}
