{include file="@header.tpl"}

<div style="padding: 0 0 25px 0;">
    
{if $enableMessages}
    <div class="info" style="margin-bottom: 15px;">{$paymentPageMessages nofilter}</div>
{/if} 
    
{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}

 	{$form->getBegin() nofilter}

	<fieldset id="fieldset_0">
        <legend>Details</legend>
        <table class="ff_table">
		<tr>
        	<th>{$form->getLabel('amount') nofilter}</th>
            <td>{$form->getControl('amount') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('amount')}</span></td>
        </tr>
        <tr>
        	<th>{$form->getLabel('email') nofilter}</th>
            <td>{$form->getControl('email') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('email')}</span></td>
        </tr>
        <tr>
        	<th>{$form->getLabel('reference') nofilter}</th>
            <td>{$form->getControl('reference') nofilter}</td>
            <td><span class="ff_control_err">{$form->getError('reference')}</span></td>
        </tr>
        </table>
    </fieldset>
    {if !$disableSage}    
    <fieldset id="fieldset_1">
        <legend>Payment</legend>
        <table class="ff_table">
            <tbody>
                <tr>
                    <th>{$form->getLabel('cardholder') nofilter}</th>
                    <td>{$form->getControl('cardholder') nofilter}</td>
                    <td><span class="ff_control_err">{$form->getError('cardholder')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('cardType') nofilter}</th>
                    <td>{$form->getControl('cardType') nofilter}</td>
                    <td><span class="ff_control_err">{$form->getError('cardType')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('cardNumber') nofilter}</th>
                    <td>{$form->getControl('cardNumber') nofilter}</td>
                    <td><span class="ff_control_err">{$form->getError('cardNumber')}</span></td>
                </tr> 
                <noscript>
                <tr>
                    <th>{$form->getLabel('issueNumber') nofilter}</th>
                    <td>{$form->getControl('issueNumber') nofilter}</td>
                    <td>The issue number MUST be entered EXACTLY as it appears on the card. e.g. some cards have Issue Number "4", others have "04".</td>
                </tr>
                <tr>
                    <th>{$form->getLabel('validFrom') nofilter}</th>
                    <td>{$form->getControl('validFrom') nofilter}</td>
                    <td><span class="ff_control_err">{$form->getError('validFrom')}</span></td>
                </tr>
                </noscript>                       
                <tr class="issuered" style="display:none;">
                    <th>{$form->getLabel('issueNumber') nofilter}</th>
                    <td>{$form->getControl('issueNumber') nofilter}</td>
                    <td>The issue number MUST be entered EXACTLY as it appears on the card. e.g. some cards have Issue Number "4", others have "04".</td>
                </tr>
                <tr class="issuered" style="display:none;">
                    <th>{$form->getLabel('validFrom') nofilter}</th>
                    <td>{$form->getControl('validFrom') nofilter}</td>
                    <td><span class="ff_control_err">{$form->getError('validFrom')}</span></td>
                </tr>                
                <tr>
                    <th>{$form->getLabel('expiryDate') nofilter}</th>
                    <td>{$form->getControl('expiryDate') nofilter}</td>
                    <td><span class="ff_control_err">{$form->getError('expiryDate')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('CV2') nofilter}</th>
                    <td>
                    	{$form->getControl('CV2') nofilter}
                    	<div class="help-button2" style="float: right; margin: 0 15px 0 0;">
        					<a href="#" class="help-icon">help</a>
        					<em>The security code is a three-digit code printed on the back of your card.
        					<img src="{$urlImgs}CVC2SampleVisaNew.png" alt="security code" />
        					</em>
    					</div>
                    </td>
                    <td><span class="ff_control_err">{$form->getError('CV2')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('address1') nofilter}</th>
                    <td>{$form->getControl('address1') nofilter}</td>
                    <td><span class="ff_control_err">{$form->getError('address1')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('address2') nofilter}</th>
                    <td>{$form->getControl('address2') nofilter}</td>
                    <td><span class="ff_control_err">{$form->getError('address2')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('address3') nofilter}</th>
                    <td>{$form->getControl('address3') nofilter}</td>
                    <td><span class="ff_control_err">{$form->getError('address3')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('town') nofilter}</th>
                    <td>{$form->getControl('town') nofilter}</td>
                    <td><span class="ff_control_err">{$form->getError('town')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('postcode') nofilter}</th>
                    <td>{$form->getControl('postcode') nofilter}</td>
                    <td><span class="ff_control_err">{$form->getError('postcode')}</span></td>
                </tr>                
                <tr>
                    <th>{$form->getLabel('country') nofilter}</th>
                    <td>{$form->getControl('country') nofilter}</td>
                    <td><span class="ff_control_err">{$form->getError('country')}</span></td>
                </tr>
                <noscript>
                <tr>
                    <th>{$form->getLabel('billingState') nofilter}</th>
                    <td>{$form->getControl('billingState') nofilter}</td>
                    <td><span class="ff_control_err">{$form->getError('billingState')}</span></td>
                </tr>
                </noscript>
                <tr class="state" style="display:none;">
                    <th>{$form->getLabel('billingState') nofilter}</th>
                    <td>{$form->getControl('billingState') nofilter}</td>
                    <td><span class="ff_control_err">{$form->getError('billingState')}</span></td>
                </tr>
            </tbody>
        </table>
    </fieldset>
   {/if}  
	<fieldset id="fieldset_2">
        <legend>Terms and Conditions</legend>
        <table class="ff_table">
            <tbody>
                <tr>
                    <th><a href="/general-terms-conditions.html" target="_blank">Terms and Conditions</a></th>
                    <td>{$form->getControl('terms') nofilter}</td>
                    <td><span class="ff_control_err">{$form->getError('terms')}</span></td>
                </tr>
            </tbody>
        </table>
    </fieldset>

	{if !empty($showAuth)}
    <div id="authFormBlock">
        <fieldset id="fieldset_3">
            <legend>3D authentication</legend>
            <a name="authFormBlock" href="#" style="visibility:hidden"></a>
            <h3 class="mnone pbottom10">Please complete the 3D authentication process below</h3>
            <iframe src="{$authFormUrl}" width="100%" height="450"></iframe>
         </fieldset>
    </div>
    {/if}
    
    <div id="submitBlock" {if !empty($showAuth)} style="display:none" {/if}>
    {if !$disableSage}
    <fieldset id="fieldset_3">
        <legend>Action</legend>
        <table class="ff_table">
            <tbody>
                <tr>
                    <th>&nbsp;</th>
                    <td>{$form->getControl('submit') nofilter}</td>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    {/if}
    <fieldset id="fieldset_4">
        <legend>Or pay using</legend>
        <table class="ff_table">
            <tbody>
                <tr>
                    <th>&nbsp;</th>
                    <td>{$form->getControl('paypal') nofilter}</td>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
        </table>        
    </fieldset>
	</div>

    {$form->getEnd() nofilter}	
		
	<p class="required"><span class="orange">*</span> Required Fields</p>
</div>
{include file="@footer.tpl"}
