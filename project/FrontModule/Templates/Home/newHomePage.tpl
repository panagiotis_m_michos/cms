{include file="@header.tpl"}

<div id="maincontent" class="home-page">
    {* TOP *}
    <div id="home-page-top">
        <div class="home-page-top-left">
            {*<h1>Get your company name today in 4 easy steps and protect it from others</h1>  
            <span>- or reserve it for the future.</span>
             *}
            <div class="home-page-top-left-headers">
                <h1>{$title}</h1>
                <h2>
                    Get Your Company Name Today &amp; Start Trading in the Next 3 Hours or Reserve if for the Future
                </h2>
            </div> 
            <div class="home-page-search-top-text">
                Secure Your UK Limited Company Name and Start Your New Business!
            </div>
            <div class="home-page-search-top-form">
                {$searchForm->getBegin() nofilter}
                <div class="inputbox fleft mright">
	    	       	{$searchForm->getControl("q") nofilter}
	            </div>
	            <input name="send" type="image" src="{$urlImgs}home-page/home-page-search-btn.png" class="fleft home-page-search-top-form-submit" />
	            {$searchForm->getEnd() nofilter}
            </div>
            <div class="clear"></div>
        </div>
        <div class="home-page-top-right">
            <div class="home-page-top-right-info">
                Great service and <b>incredible value for money</b> compared to your competitors. Thank you for the smooth and swift registration and all of the additional benefits!
            </div>
            <div class="home-page-top-right-author">
                <div class="home-page-top-author-left">
                </div>
                <div class="home-page-top-author-right">
                    <span style="color: #2b7cac">John Duggan</span> <br />
                    <span>Events Protocol Ltd.</span> <br />
                    <span>21/09/2012</span>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
            
    <div id="home-page-left-side">
        <p style="font-size: 18px;margin-bottom: 5px;">Included in All of Our Packages:</p>
        <h4>Free Business Start Up Toolkit - Everything You Need to Start, Run &amp; Grow Your New Company.</h4>
        <div class="home-page-pakages-include">
            <ul>
                <li>
                    <span style="float: left;"><b>FREE Fast-track Barclays Business Bank Account</b> with &pound;50.00 Cash Back (Optional)</span>
                    <span style="float: right;">
                        <a href="javascript:;" class="home-page-tooltip home-page-tooltip-image" title="" rel="1">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </a>
                        <span id="tooltip1" style="display: none" class="tooltip">
                            <b>Barclays Startup Business Account (Optional)</b> <br />
                                -You get £50 cash deposit* & 12 months FREE banking! <br />
                                -Dedicated local business manager to provide support. <br />
                                -Business debit card, cheque book, online & telephone banking. <br />
                                -Fast track service.  As soon as your company is formed, Barclays will contact you directly.<br />
                                -UK customers only.
                        </span>
                    </span> <div class="clear"></div>
                </li>
                <li>
                    <span style="float: left;"><b>FREE .co.uk Domain Name</b> to Get Your Business Online with a Professional Email Address</span>
                    <span style="float: right;">
                        <a href="javascript:;" class="home-page-tooltip home-page-tooltip-image" rel="2">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </a>
                        <span id="tooltip2" style="display: none" class="tooltip">
                            <b>FREE .co.uk Domain Name (worth £XXX)</b> <br />
                            -Get a free .co.uk domain name for your new company worth £6.99. <br />
                            -Get your business online with a professional email address.
                        </span>
                    </span> <div class="clear"></div>
                </li>
                <li>
                    <span style="float: left;"><b>Google Adwords Voucher</b> to Get Your Business in Front of Millions of Potential Customers</span>
                    <span style="float: right;">
                        <a href="javascript:;" class="home-page-tooltip home-page-tooltip-image" rel="3">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </a>
                        <span id="tooltip3" style="display: none" class="tooltip">
                            <b>Google AdWords Voucher (worth £75)</b> <br />
                            -Advertise your products & services to millions of potential customers using Google's online advertising service.<br />
                            -Find more customers by advertising your site with Google.<br />
                            -Spend £25 and get an additional £75.<br />
                            -Use Google's phone setup service to get started now.
                        </span>
                    </span> <div class="clear"></div>
                </li>
                <li>
                    <span style="float: left;"><b>FREE Accountancy &amp; Tax Consultation</b> (UK Customers Only)</span>
                    <span style="float: right;">
                        <a href="javascript:;" class="home-page-tooltip home-page-tooltip-image" rel="4">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </a>
                        <span id="tooltip4" style="display: none" class="tooltip">
                            <b> FREE Accountancy & Tax Consultation (worth £XXX</b> <br />
                            -Find out if you're paying too much tax.<br />
                            -Free review of your affairs by a qualified accountant.<br />
                            -Branches nationwide for convenient appointments.<br />
                            -UK customers only.
                        </span>
                    </span> <div class="clear"></div>
                </li>
                <li>
                    <span style="float: left;"><b>Exclusive 2-Month FREE Trial to FreeAgent's</b> Award Winning Online Accounting Tool for </span>
                    <span style="float: right;">
                        <a href="javascript:;" class="home-page-tooltip home-page-tooltip-image" rel="5">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </a>
                        <span id="tooltip5" style="display: none" class="tooltip">
                            <b> FREE 2-Month trial to FreeAgent (worth £XX)</b> <br />
                            -Exclusive discount for Companies Made Simple customers - you won't find this offer anywhere else.<br />
                            - Plus get a 10% discount on the monthly subscription for 12-months after your free trial ends.<br />
                            -Keep track of all your company's finances. <br />
                            -FreeAgent is super easy-to-use, you don't need any accounting or bookkeeping experience to start using it.<br />
                        </span>
                    </span> <div class="clear"></div> Small Businesses. Plus you get 10% discount on the monthly subscription for 12 months after the free trial ends. FreeAgent is easy-to-use and you won't need any previuos accounts experience to start using it.
                </li>
                <li>
                    <span style="float: left;"><b>FREE How To Make A Profit eBook</b> - Everything you need to know to help your business make a profit</span>
                    <span style="float: right;">
                        <a href="javascript:;" class="home-page-tooltip home-page-tooltip-image" rel="6">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </a>
                        <span id="tooltip6" style="display: none" class="tooltip">
                            <b> FFREE How To Make A Profit eBook</b> <br />
                            -Receive an expert’s guide to making profit<br />
                            -Easy to understand and jargon-free<br />
                            -Avoid the common mistakes many new businesses make<br />
                            -Perfect if you've never started a business before
                        </span>
                    </span>  <div class="clear"></div>
                </li>
                <li>
                    <span style="float: left;"><b>3 FREE Company Credit Checks</b> (worth £XXX) - Avoid losing money to bad payers or </span>
                    <span style="float: right;">
                        <a href="javascript:;" class="home-page-tooltip home-page-tooltip-image" rel="7">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </a>
                        <span id="tooltip7" style="display: none" class="tooltip">
                            <b> FREE Company Credit Checks (worth £XXX</b> <br />
                            -Run a free credit check on your first 3 business customers to make sure you'll get paid<br />
                            -Easy to use service from our sister site Company Searches Made Simple (credit checks available on UK companies only).
                        </span>
                    </span> <div class="clear"></div> suppliers
                </li>
            </ul>
        </div>
        <div class="home-page-compare-packages">
            <img src="{$urlImgs}home-page/home-page-compare-packages.png" alt="click here to compare all the packages" usemap="#compare-packages" />
            <map id="compare-packages" name="compare-packages">
                <area title="Click here to compare all the packages" alt="Compare all the packages" href="{$this->router->link("SearchControler::SEARCH_PAGE")}" class="sample" coords="120,100,485,150" shape="rect">
            </map>
        </div>
        <h3>Why Choose Us?</h3>
        <div class="home-page-why-choose-us">
            <div class="home-page-why-choose-us-left"></div>
            <div class="home-page-why-choose-us-right">
                <p><b>Free Lifetime Customer Support for every customer</b> - call, email, tweet or chat; <br /> we're here for you!</p>
                <p><b>No hidden extras</b> <br /> - all our formation packages include Companies House filing fees.</p>
                <p><b>We form a company every 8-minuts and we're formed 247,532 companies <br />since 2002</b> - so you can rest assured.</p>
                <p><b>3-hours company formation</b> - almost all the companies we form are registered within <br /> 3hours. That's fast...</p>
                <p><b>Innovative online companiy management portal</b> - enabling you to easly manage your <br />company details, directors and shareholders online.</p>
            </div>
            <div class="clear"></div>
        </div>
        <h3>See What Customres Have To Say About Us</h3>
        <div class="home-page-about-us">
            <div class="home-page-about-us-left">
                <div  style="height: 234px;" id="videoHomePage">
                    <img id="homeImage" src="{$urlImgs}cover1.jpg" name="butplayln" alt="play" align="right"  border="0" onmouseover="document.butplayln.src='{$urlImgs}cover2.jpg'" onmouseout="document.butplayln.src='{$urlImgs}cover1.jpg'" onclick="createEmbed();" /> 
                    <div class="embed-video" style="display: none;"></div>
                </div>
                    
            </div>
            <div class="home-page-about-us-right"></div>
            <div class="clear"></div>
            <p>95% of customers rate our services as excellent and more than 96% will recommend our services to others. See more reviews</p>
        </div>
        <div class="home-page-bottom-form" >
            <div class="home-page-search-top-text" style="margin-top: 0px; padding-top: 30px;">
                Secure Your UK Limited Company Name and Start Your New Business!
            </div>
            <div class="home-page-search-top-form">
                {$searchForm->getBegin() nofilter}
                <div class="inputbox fleft mright">
	    	       	{$searchForm->getControl("q") nofilter}
	            </div>
	            <input name="send" type="image" src="{$urlImgs}home-page/home-page-search-btn.png" class="fleft home-page-search-top-form-submit" />
	            {$searchForm->getEnd() nofilter}
            </div>
        </div>
    </div>
    <div id="home-page-right-side">
        <div class="home-page-right-side-partners">
            <img src="{$urlImgs}home-page/home-page-partners.png" alt="partners" />
        </div>
        
        <div class="home-page-right-side-testimonials">
            <div class="home-page-right-side-testimonials-top">
                <p>More Testimonials</p>
            </div>
            <div class="home-page-right-side-testimonials-midd">
                <div class="home-page-testimonial">
                    <div class="left-home-page-testimonial"></div>
                    <div class="right-home-page-testimonial">
                    My first time setting up my 
                    own company, was intially
                    quoted by a local accountant
                    but after visting your website 
                    had the confidence to do it
                    myself! Really good 
                    information and support
                    feedback rapid and <b>much
                    easier then I had anticipated.</b>
                    </div>
                    <div class="clear"></div>
                    <div class="home-page-top-right-author">
                        <div class="home-page-top-author-right">
                            <span style="color: #2b7cac">Steve Nicholson</span> <br />
                            <span>WebVet Services Ltd.</span> <br />
                            <span>26/07/2012</span>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                
                <div class="home-page-testimonial">
                    <div class="left-home-page-testimonial"></div>
                    <div class="right-home-page-testimonial">
                        <b>Excellent service, very quick response</b> and the business plan tips are really useful provided with the company formation.
                    </div>
                    <div class="clear"></div>
                    <div class="home-page-top-right-author">
                        <div class="home-page-top-author-right">
                            <span style="color: #2b7cac">Zahid Chaudhry</span> <br />
                            <span>Sentinel Technologies Ltd.</span> <br />
                            <span>19/09/2012</span>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                
                <div class="home-page-testimonial">
                    <div class="left-home-page-testimonial"></div>
                    <div class="right-home-page-testimonial">
                    As a nervous user to set up 
                    a Company can find no fault
                    with your service. <b>You gave
                    me confidence that I was
                    doing everything correctly.</b>
                    </div>
                    <div class="clear"></div>
                    <div class="home-page-top-right-author">
                        <div class="home-page-top-author-right">
                            <span style="color: #2b7cac">Mrs P.Fee</span> <br />
                            <span>Able Glass Flowline Ltd.</span> <br />
                            <span>12/09/2012</span>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                
            </div>
            <div class="home-page-right-side-testimonials-bottom"></div>
            </div>
    </div>
    <div class="clear"></div>
    <p class="bottom-home-page-info">
        *Subject to Companies House workload.
    </p>
</div>

                
{literal}

<script type="text/javascript">
$(document).ready( function(){
    
    $(".home-page-search-top-form input#q").attr('placeholder', 'Check Your Company Name\'s Availabilty Now...');
    
    $(function() {
        var placeHolders = $.Placeholders();
        placeHolders.init();    
    });
    
    $('.home-page-tooltip').each(function() {
            var self = $(this);
            self.tooltip({
            tip : "#tooltip" +  $(this).attr('rel'),   
            position: 'center right',
            offset: [0, 10],
            effect: 'slide',
            delay: 0,    
            opacity: 1    
        });    
    });
});
</script>
<style>
    .tooltip {
        width: 300px;
        line-height: 20px;
    }
</style>
{/literal}                
                
{include file="@footer.tpl"}
