{include file="@header.tpl"}
<div id="main">
    <div id="headerBanner">
        <h1>Simple UK company formation</h1>
        <p>The company formation process was so easy. I woke up to an email saying my company was registered, and the tips on each page were so easy to follow.
            <img src="{$urlImgs}home2014/QuotationMark4.PNG" alt="" />
            <br /><span style="padding: 0px;">Glenn Bennett, Hussky International</span>
        </p>
        <img id="hQuoteStart" src="{$urlImgs}home2014/QuotationMark3.png" alt="" />
        <div id="rating">
            <a href="http://www.feefo.com/feefo/viewvendor.jsp?logon=www.madesimplegroup.com/companiesmadesimple" onclick="window.open(this.href, 'Feefo', 'width=1000,height=600,scrollbars,resizable'); return false;">
                <img alt="Feefo logo" border="0" src="//www.feefo.com/feefo/feefologo.jsp?logon=www.madesimplegroup.com/companiesmadesimple&template=service-white-200x50_en.png" title="See what our customers say about us">
            </a>
        </div>
    </div>
    <div id="headerSearch">
        <img class="search" src="{$urlImgs}home2014/search.png" alt="" />
        <h2>Is your company name available? Search now</h2>
        <form action="/company-formation-name-search.html" method="post" name="search">
            <input class="textInput" id="q" name="q" type="text" />
            <input class="searchButton" type="submit" name="send" value="SEARCH" />
            <input type="hidden" value="1" id="submited_search" name="submited_search" />
        </form>
        <p>or <a href="/company-formation-name-search.html">view packages</a></p>
    </div>
    <div id="logos">
        <span>We work with the best around to give your company a great start.</span>
        <div>
            <a class="logo" id="google"></a>
            <a class="logo" id="barclays"></a>
            <a class="logo" id="freeagent"></a>
            <a class="logo" id="acra"></a>
            <a class="logo" id="icaew"></a>
        </div>
    </div>
    <div id="leftColumn">
        <div id="populars">
            <h2>Popular services & renewals</h2>
            <div class="pcolumn">
                <img src="{$urlImgs}home2014/calendar.png" />
                <a href="{$this->router->link("672")}">Annual return</a>
                <p>Your company’s annual<br />return filed for you</p>
            </div>
            <div class="pcolumn">
                <img src="{$urlImgs}home2014/skyscraper.png" alt="" />
                <a href="{$this->router->link("259")}">Registered office</a>
                <p>Use our prestigious London<br />N1 registered office address</p>
            </div>
            <div class="pcolumn">
                <img src="{$urlImgs}home2014/refresh.png" alt="" />
                <a href="{$this->router->link("82")}">Renewals</a>
                <p>Renew your services, quickly<br />and easily</p>
            </div>
        </div>
        <div id="whyUs">
            <h2>Why choose us?</h2>
            <img class="whyArrow" src="{$urlImgs}home2014/blackarrow.png" alt="" />
            <div id="service">
                <div class="whyus-left">
                    <h3>Fast Service</h3>
                    <div class="imgBox">&#160;</div>
                </div>
                <div class="whyus-right">
                    <ul>
                        <li>
                            <h4>3 hour formation</h4>
                            Almost all of the companies we form are registered within 3 hours. That's fast.
                        </li>
                        <li>
                            <h4>4 simple steps</h4>
                            Register your UK company in 4 simple steps. Search, select, buy, form. Learn <a href="/how-it-works.html">how it works</a>.
                        </li>
                    </ul>
                </div>
            </div>
            <div id="value">
                <div class="whyus-left">
                    <h3>Great value</h3>
                    <div class="imgBox">&#160;</div>
                </div>
                <div class="whyus-right">
                    <ul>
                        <li>
                            <h4>Free startup toolkit</h4>
                            With every formation. All you need to start, run &amp; grow your new company.
                        </li>
                        <li>
                            <h4>No hidden extras</h4>
                            Prices start from £13.99   VAT and include the Companies House filing fee.
                        </li>
                    </ul>
                </div>
            </div>
            <div id="safe">
                <div class="whyus-left">
                    <h3>You're in safe hands</h3>
                    <div class="imgBox">&#160;</div>
                </div>
                <div class="whyus-right">
                    <ul>
                        <li>
                            <h4>Over 350,000 companies formed</h4>
                            We've been forming companies for over a decade. We know what we're doing.
                        </li>
                        <li>
                            <h4>Free lifetime customer support</h4>
                            Call, email, tweet, chat; we're here for you.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div id="whatOurCustomersSay">
            <h2>See What Our Customers Have to Say</h2>
            <div id="videoHolder">
                <div style="height: 234px;" id="videoHomePage">
                    <img id="homeImage" src="{$urlImgs}cover1.jpg" name="butplayln" alt="play" align="right" width="416" height="234" border="0" onmouseover="document.butplayln.src = '{$urlImgs}cover2.jpg'" onmouseout="document.butplayln.src = '{$urlImgs}cover1.jpg'" onclick="createEmbed();" />
                    <div class="embed-video" style="display: none;">&#160;</div>
                </div>
            </div>
            <div id="reasons">
                <ul>
                    <li>
                        Award winning company formation agent
                    </li>
                    <li>
                        Leading online company formation agent in the<br />
                        UK - 7 years running!
                    </li>
                    <li>
                        5 Star customer feedback
                    </li>
                    <li>
                        Free company secretarial management, allowing you to manage all your companies in one place.
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div id="rightColumn">
        <div id="startToday">
            <p>Start Your New<br />
                Company Today From<br />
                <strong>£13.99<span>  +VAT</span></strong><br />
                <a href="/company-formation-name-search.html">Click here</a> to view packages</p>
        </div>
        <div id="barclaysCashBack">
            <p><span>£50 CASH BACK</span></p>
            <img src="{$urlImgs}home2014/barclaysColour.png" alt="Barclays" />
            <p>Every company needs a business bank account to start trading.</p>
            <p>Through our relationship with Barclays we can offer a simple account opening process with <strong>£50 cash back.</strong></p>
            <p><a href="/fast-track-business-banking.html">Click here for more information</a></p>
        </div>
        <div id="testimonials">
            <h3>We're the professionals' choice too...</h3>
            <ul>
                <li>
                    <img src="{$urlImgs}home2014/ss-accountants.PNG" alt="" />
                    <p>I regularly form companies for my clients. It really is very simple. The service is great too. A must for all practices.</p>
                    <span class="quote">Southside Accountants</span>
                </li>
                <li>
                    <img src="{$urlImgs}home2014/1stcontact.png" alt="" />
                    <p>The most attractive part of what they offer is the simplicity of using their systems.</p>
                    <span class="quote">1st Contact</span>
                </li>
                <li>
                    <img src="{$urlImgs}home2014/taxassist.png" alt="" />
                    <p>It does what it says on the tin! We have been using them for a number of years for company formations for clients and the service is excellent. Thoroughly recommended.</p>
                    <span class="quote">TaxAssist Wolverhampton</span>
            </ul>
        </div>
    </div>
</div>
<div class="clear"></div>
<div id="blogfeed">
    <div id="blogfeedbox">
        <div class="reviewsblog">
            <img src="{$urlImgs}home2014/fpen.png" />
            <span>Latest blog posts</span>
            <p>
                <strong>I want to change my shareholder</strong><br />
                Unfortunately changing a shareholder in a UK limited...
                <a href="/project/blog/i-want-to-change-my-shareholder/">Read more</a>
            </p>
            <p>
                <strong>Got your own Limited Company? Manage it from...</strong><br />
                Storing a company on our admin portal means that...
                <a href="/project/blog/got-your-own-limited-company-manage-it-from-our-portal-for-free/">Read more</a>
            </p>
        </div>
        <div class="reviewsblog">
            <img src="{$urlImgs}home2014/customers.png" />
            <span>What our customers say</span>
            <p>
                'Have been using Companies Made Simple for several years now,
                and cannot fault them. An excellent company to do business with,
                and Customer Service second to none. A FIVE STAR company'
                <span>Independent Feefo Review</span>
                <a href="http://www.feefo.com/feefo/viewvendor.jsp?logon=www.madesimplegroup.com/companiesmadesimple" onclick="window.open(this.href, 'Feefo', 'width=1000,height=600,scrollbars,resizable');
                        return false;">Read more reviews</a>
            </p>
        </div>
    </div>
</div>
<div class="clear"></div>
<div id="bottomlinks">
    <div id="homemenu">
        <div class="mcol1">
            <h2><a href="{$this->router->link("102")}">Company Formation</a></h2>
            <div>
                <div>
                    <div>
                        <div>
                            <ul>
                                <li><a href="{$this->router->link("102")}">Company Formation Packages</a></li>
                            </ul>
                        </div>
                        <div>
                            <h3>Company Types</h3>
                            <ul>
                                <li><a href="{$this->router->link("72")}">UK Limited Company Formation</a></li>
                                <li><a href="{$this->router->link("74")}">Limited by Guarantee Company</a></li>
                                <li><a href="{$this->router->link("73")}">Limited by Shares Company</a></li>
                                <li><a href="{$this->router->link("76")}">Limited Liability Partnership</a></li>
                                <li><a href="{$this->router->link("75")}">Public Limited Company</a></li>
                                <li><a href="{$this->router->link("237")}">Flat Management Company</a></li>
                            </ul>
                        </div>
                    </div>
                    <div>
                        <div>
                            <h3>Other company services</h3>
                            <ul>
                                <li><a href="{$this->router->link("62")}">Offshore Formation</a></li>
                                <li><a href="{$this->router->link("239")}">Reserve a Company Name</a></li>
                                <li><a href="{$this->router->link("1288")}">Shelf Companies</a></li>
                                <li><a href="{$this->router->link("819")}">Sole Trader Start Up Pack</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mcol1">
            <h2><a href="{$this->router->link("67")}">How It Works</a></h2>
            <ul>
                <li>
                    <div>
                        <div>
                            <ul>
                                <li><a href="{$this->router->link("67")}">Overview</a></li>
                                <li><a href="{$this->router->link("972")}">Why Choose us?</a></li>
                                <li><a href="{$this->router->link("749")}">Reviews and Testimonials</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
            <h2><a href="{$this->router->link("1423")}">Additional Services</a></h2>
            <ul>
                <li>
                    <div>
                        <ul>
                            <li><a href="{$this->router->link("82")}">Renewals</a></li>
                        </ul>
                    </div>
                    <div>
                        <h3>Accountancy and Legal</h3>
                        <ul>
                            <li><a href="{$this->router->link("251")}">Accountancy Consultation</a></li>
                            <li><a href="{$this->router->link("672")}">Annual Return Service</a></li>
                            <li><a href="{$this->router->link("368")}">Change of Accounting Reference Date</a></li>
                            <li><a href="{$this->router->link("845")}">Company Name Change Service</a></li>
                            <li><a href="{$this->router->link("257")}">Company Dissolution</a></li>
                            <li><a href="{$this->router->link("255")}">Company Tax Registration</a></li>
                            <li><a href="{$this->router->link("785")}">Dormant Company Accounts Service</a></li>
                            <li><a href="{$this->router->link("261")}">Share Services</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
        <div class="mcol1">
            <div>
                <div>
                    <h3>Office & Address Services</h3>
                    <ul>
                        <li><a href="{$this->router->link("557")}">Mail Forwarding</a></li>
                        <li><a href="{$this->router->link("259")}">Registered Office Service</a></li>
                        <li><a href="{$this->router->link("476")}">Service Address Service</a></li>
                        <li><a href="{$this->router->link("250")}">Virtual Office</a></li>
                    </ul>
                </div>
                <div>
                    <h3>Documents</h3>
                    <ul>
                        <li><a href="{$this->router->link("83")}">Apostilled Documents Service</a></li>
                        <li><a href="{$this->router->link("85")}">Certificate of Good Standing</a></li>
                        <li><a href="{$this->router->link("262")}">Supplementary Company Documents</a></li>
                    </ul>
                </div>
            </div>
            <div>
                <h3>Official Company Products</h3>
                <ul>
                    <li><a href="{$this->router->link("256")}">Company Pack (Register, Seal & Stamp)</a></li>
                    <li><a href="{$this->router->link("256")}">Company Register</a></li>
                    <li><a href="{$this->router->link("256")}">Company Seal</a></li>
                    <li><a href="{$this->router->link("256")}">Company Stamp</a></li>
                </ul>
            </div>
        </div>
        <div class="mcol1">
            <div>
                <h3>Partners and Affiliates</h3>
                <ul>
                    <li><a href="{$this->router->link("908")}">Affiliate Program</a></li>
                    <li><a href="{$this->router->link("1283")}">White Label Company</a></li>
                    <li><a href="/professional/">Formation</a></li>
                    <li><a href="{$this->router->link("240")}">Wholesale</a></li>
                </ul>
            </div>
            <div>
                <div>
                    <h3>Other</h3>
                    <ul>
                        <li><a href="{$this->router->link("928")}">Business Apps</a></li>
                        <li><a href="{$this->router->link("555")}">Guides</a></li>
                        <li><a href="{$this->router->link("573")}">Business Templates/Forms</a></li>
                    </ul>
                </div>
            </div>
            <h2><a href="http://support.companiesmadesimple.com">Help and Advice</a></h2>
            <ul>
                <li>
                    <div>
                        <ul>
                            <li><a href="http://support.companiesmadesimple.com/category/132-frequently-asked-questions">FAQs</a></li>
                            <li><a href="{$this->router->link("88")}">Small Business Advisor</a></li>
                            <li><a href="{$this->router->link("726")}">Sole Trader v Limited Company Calculator</a></li>
                            <li><a href="http://support.companiesmadesimple.com/article/286-glossary">Glossary</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

{include file="@footer.tpl"}
