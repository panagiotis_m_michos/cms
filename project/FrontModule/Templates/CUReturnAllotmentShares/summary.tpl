{include file="@header.tpl"}


<div id="maincontent2">
	<p style="margin: 0 0 15px 0; font-size: 11px;"><a href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}">{$companyName}</a> &gt; {$title}</p> 
	{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}

    <div style="background-color: rgb(204, 229, 255); border: 1px solid rgb(204, 204, 204); margin-bottom: 15px; padding: 10px;">
        A company may increase its share capital by allotting additional shares using the form below. Please note, you will only 
        notify Companies House of the name of the shareholder(s) at the time of your next Annual Return. NB: You can only allot 
        additional shares to a share class that already exists. If you wish to allot a new class of shares you will need to file 
        the paper form <a href="http://www.companieshouse.gov.uk/forms/generalForms/SH01_return_of_allotment_of_shares.pdf">SH01</a>.
    </div>

	
	{if !empty($shares)}

		{$form->getBegin() nofilter}
		
		{* ERRORS *}
		{if $form->hasErrors()}
			<ul class="ff_err_notice">
			{foreach from=$form->getErrors() item="error"}
				<li>{$error}</li>
			{/foreach}
			</ul>
		{/if}

		<table class="grid2">
		<col width="423">
		<col>
		<col>
		<col>
		<col width="50">
		<tr>
			<th>Share Class</th>
			<th class="center">No. of Shares</th>
			<th class="center">Value Per Share</th>
			<th class="center">Currency</th>
			<th class="center">No. of New Shares</th>
		</tr>
		{foreach from=$shares key="key" item="share"}
		<tr>
			<td>{$share.share_class}</td>
			<td style="text-align: right;">{$share.num_shares}</td>
			<td style="text-align: right;">{$share.amount_paid+$share.amount_unpaid}</td>
			<td>{$share.currency}</td>
			<td>{$form->getControl("new_shares_add_$key") nofilter}</td>
		</tr>
		{/foreach}
		</table>
		
		<br />

        {* SUBMIT *}
        {$form->getControl('submit') nofilter}
        
        {$form->getEnd() nofilter}
	{else}
		<p>No Shares</p>
	{/if}
</div>
{include file="@footer.tpl"}
