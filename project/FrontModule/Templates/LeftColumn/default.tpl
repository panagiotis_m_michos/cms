{include file="@header.tpl"}

<div id="maincontent2">
	{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
	
	{$text nofilter}
	
</div> <!-- .maincontent2 -->

{include file="@footer.tpl"}