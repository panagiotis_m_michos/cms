{include file="@header.tpl"}
<div id="wholemicro">
    <div id="wrapper">
        {if $visibleTitle}
            <h1  class="mtop20">{$title}</h1>
        {/if}
        {$text nofilter}
        <div id="text_content">
            {$form nofilter}
        </div>
    </div>
</div>
{include file="@footer.tpl"}