{include file="@header.tpl"}

<div id="wholemicro">

<div id="wrapper">
    <!--
    <div class="campaign50">
        <h3 class="blue3 massivefont mnone">Time limited offer</h3>
        <p class="midfont grey12 lheight20 noptop">
            Spend £50 with Company Formation MadeSimple before May 31st and we’ll add £50 credit to your account.
            <br /><a href="#campaign50">To claim your £50, or to find out more - click here</a>
        </p>
    </div>
    -->
    <div id="box1">
        <h1>{$title}</h1>
        <ul>
            <li>Save time with hassle-free company formations from &pound;13.99</li>
            <li>Earn &pound;50 cash back for every Barclays bank account opened</li>
            <li>Manage companies with ease using our ICAEW accredited software</li>
        </ul>
        <span class="fwdbutton"><a href="{$this->router->link('#signUp#')}">Sign Up</a></span>
        <span>Or <a href="{$this->router->link('#products#')}">view our formation packages</a></span>
    </div>

    <div id="box2">
        <span>We're the professional's choice:</span>
    </div>

    <div id="box3">
        <h2>Why form companies with us?</h2>
        <div id="box3container">
            <div class="w-left colborder">
                <h3 class="img1550">Great value packages with full documentation</h3>
                <div class="w-qmark">
                    <i class="fa fa-question-circle grey2 font20 mleft10" data-toggle="tooltip" data-placement="right" title="
                       Our competitively priced company packages start at just &pound;13.99 and include the &pound;13 Companies House filing fee.
                    <br /><br />With every company formation package, you also receive a printed Certificate of Incorporation to pass on to your clients, as well as digital Memorandum & Articles of Association and digital shareholder certificates. In more detail:
                    <br /><br />Starter &pound;13.99 + VAT
                    <br />Limited by shares company including printed certificate of incorporation and electronic company documents. This package is the equivalent to our &pound;29.99 Printed package (for non-professional customers) so you save even more money.
                    <br /><br />Executive &pound;23.99 + VAT
                    <br />All the benefits of the Starter package plus a printed copy of the Memorandum and Articles of Association.
                    <br /><br />Professional &pound;39.99 + VAT
                    <br />All the benefits of the Executive package plus a Registered Office Service for 1 year, a Service Address for all directors for 1 year and an unbranded company register.
                    "></i>
                </div>
                <p class="pbtm40">Form companies from just &pound;13.99 and receive a printed Certificate of Incorporation, digital M&amp;As and digital shareholder certificates.<br />Includes &pound;13 Companies House filing fee.</p>
                <h3 class="img1551">3 hour formation</h3>
                <div class="w-qmark">
                    <i class="fa fa-question-circle grey2 font20 mleft10" data-toggle="tooltip" data-placement="right" title="
                       99% of all our company formations are incorporated within 3 hours of being submitted, so neither you or your client have to wait long.
                    "></i>
                </div>
                <p class="pbtm40">Almost all the companies we form are registered within 3 hours - that's fast!</p>
                <h3 class="img1552">We reward your loyalty</h3>
                <div class="w-qmark">
                    <i class="fa fa-question-circle grey2 font20 mleft10" data-toggle="tooltip" data-placement="right" title="
                       For every 10 companies formed with Executive or Professional packages, we'll add &pound;20 credit to your account. That's one free company formation (Starter package) or, alternatively, can be used against any other service that we offer on Companies Made Simple.
                    "></i>
                </div>
                <p>For every 10 companies you form with our Executive or Professional packages, we add &pound;20 credit to your account.<br />&nbsp</p>
            </div>
            <div class="w-right">
                <h3 class="img1553">Barclays Bank Account with &pound;50 cash back</h3>
                <div class="w-qmark">
                    <i class="fa fa-question-circle grey2 font20 mleft10" data-toggle="tooltip" data-placement="left" title="
                       We have an excellent partnership with Barclays and offer &pound;50 cash back for every business bank account opened as part of the formation process.
                    <br /><br />Shortly after the company formation, Barclays will contact your client to arrange a face to face meeting (they will not mention us). Once the Business Bank Account has been opened, you will receive &pound;50 cash back. This can be added as credit to your Companies Made Simple account or, alternatively, transferred to your bank account. Please note we will require proof of ID and proof of address before adding credit to your Companies Made Simple account or transferring the &pound;50 cash back to your bank account. What you then do with this cash back is up to you; give it to your client, keep it for yourself or share it with them.
                    "></i>
                </div>
                <p class="pbtm40">Provide your clients with a 'fast track' Barclays Bank Account and receive &pound;50 cash back when accounts are opened.</p>
                <h3 class="img1554">Easily manage companies</h3>
                <div class="w-qmark">
                    <i class="fa fa-question-circle grey2 font20 mleft10" data-toggle="tooltip" data-placement="left" title="
                       With our free Company Management system, the only company secretarial system accredited by ICAEW,  you can manage all your companies from one secure place - you can bulk import your existing companies, even if they weren't formed with us.
                    <br /><br />The system is accessible from any computer, at any time. There's no installation, and no subscription charges. You can sync your companies with Companies House to receive up to date information, view and sort your companies by statutory filing dates, track dividends, create vouchers and more:
                    <br /><br />Prepare and file Annual Returns for &pound;13
                    <br />Yes that's right! For the same price as Companies House, you can prepare and file your annual returns electronically.
                    "></i>
                </div>
                <p class="pbtm40">Free online company secretarial software, accessible 24/7 wherever you are in the world.</p>
                <h3 class="img1555">Over 350,000 companies formed</h3>
                <div class="w-qmark">
                    <i class="fa fa-question-circle grey2 font20 mleft10" data-toggle="tooltip" data-placement="left" title="
                       We've been forming companies since 2003, we know what we're doing. We're the leading UK company formation agent and every 3 minutes, a new company is registered via Companies Made Simple.
                    "></i>
                </div>
                <p>We've been forming companies for over a decade and we are ICAEW's exclusively appointed company formation partner. You're in safe hands.</p>
            </div>
        </div>
    </div>

    <div id="box4">
        <h2>How it works</h2>
        <p>Forming companies on behalf of your clients has never been easier. Simply sign up as a professional customer to form and manage your clients' companies from one central place. Our standard formation packages are still available to you, however, if you wish to buy one of our discounted professional packages, select the 'form a company' option from your dashboard.</p>
    </div>

    <div id="box4acontainer">
        <div class="ball1">
            <h4>1. Search</h4>
            <p>Instantly check if the company name you require is available.</p>
        </div>
        <div class="ball2">
            <h4>2. Select</h4>
            <p>Choose one of our 3 company formation packages. View our comparison matrix to see what's included in each.</p>
        </div>
        <div class="ball3">
            <h4>3. Buy</h4>
            <p>Pay via online card payment with our secure payment system.</p>
        </div>
        <div class="ball4">
            <h4>4. Complete</h4>
            <p>Complete and submit the company details. Once the company has been formed we'll send you all the documentation.</p>
        </div>
    </div>

    <div id="box5">
        <h2>Manage your clients' companies - the simple way</h2>
        <p>Our company management system is the only company secretarial software accredited by ICAEW. It is a  simple to use system packed full of useful features that make your life easier.</p>
        <div id="box5container">
            <div class="w-left">
                <h3 class="img1556">Free</h3>
                <p>No setup or subscription charges, our management system is free.</p>
                <h3 class="img1557">Manage Centrally</h3>
                <p>Manage all companies from one place. You can even bulk import all your existing companies.</p>
                <h3 class="img1558">Never miss a deadline</h3>
                <p>View and sort companies by statutory filing dates.</p>
                <h3 class="img1559">Exclusively for ICAEW members</h3>
                <p>100 free company credit reports (valued at &pound;150)*.</p>
            </div>
            <div class="w-right">
                <h3 class="img1560">Access anywhere</h3>
                <p>Access from any computer with an internet connection 24/7. No installation required.</p>
                <h3 class="img1561">File Annual Returns</h3>
                <p>Prepare and file annual returns electronically for just &pound;13 (that's the same fee Companies House charge).</p>
                <h3 class="img1562">Keep up to date</h3>
                <p>Sync your companies directly with Companies House.</p>
                <h3 class="img1563">Dividends</h3>
                <p>Track dividends and create vouchers.</p>
            </div>
        </div>
        <span class="fwdbutton"><a href="{$this->router->link('#signUp#')}">Sign Up, it's free</a></span>
    </div>

    <div id="box6">
        <h2>What other professionals have to say</h2>
        <div id="box6container">
            <div class="w-left">
                <p>"The most attractive part is the simplicity of using their systems. Everything can be performed online and is very user friendly"<span>Raymond Ridgeway - 1st Contact</span></p>
            </div>
            <div class="w-right">
                <p>"Companies Made Simple always provide an efficient and reliable service which is easy to use. They now handle all of my company formation requirements"<span>Maximilian Hamilton - Director Courtlands Hotel</span></p>
            </div>
        </div>
    </div>

    <div id="box7">
        <h2>Frequently Asked Questions</h2>
        <div id="box7container">
            <div class="w-left">
                <h3>How much does signing up cost?</h3>
                <p>Nothing. It's absolutely free.</p>
                <h3>How much does forming a company cost?</h3>
                <p>We offer 3 different packages:<br /><br />
                    Starter = <strong>&pound;13.99</strong> +VAT<br />
                    Executive = <strong>&pound;23.99</strong> +VAT<br />
                    Professional = <strong>&pound;39.99</strong> +VAT<br /><br />
                    All prices include the Companies House &pound;13 incorporation fee. Of course, you are then welcome to charge your clients your usual company formation fees.</p>
                <h3>What are the payment options?</h3>
                <p>All services, including company formations, are paid for up-front via online card payments. If you would rather not enter your card information every time that you wish to buy a service, you can add an amount of credit of your choosing to your account via a one-off payment. Future payments are then knocked off of this credit.</p>
                <h3>Do my clients know that Companies Made Simple assisted with the formation?</h3>
                <p>No. All documentation is sent on to you  - we will not contact your clients.</p>
                <h3>*I am an ICAEW member, how do I claim my 100 free company credit reports?</h3>
                <p>To claim your exclusive benefits you simply need to be signed up to the system and have imported at least 5 companies. Then contact us and we'll add the credits.</p>
            </div>
            <div class="w-right">
                <h3>Barclays Bank Account FAQ's<br /><br />How does it work?</h3>
                <ol>
                    <li>You provide your client's contact information just before the company formation process.</li>
                    <li>Within 2 days of the company formation, Barclays make the initial contact to arrange a face-to-face meeting with your client. Barclays will let us know when the account has been opened.</li>
                    <li>If you haven’t already, please provide us with your proof of ID and proof of address (please see the below FAQ) and your preferred payment method; account credit or cash back to your bank account. You must provide ID and payment method details no later than 45 days after the account opens to be eligible for cashback.</li>
                    <li>Once a month Barclays will notify us of how many of your clients have opened a bank account, we will then add credit to your Companies Made Simple account or transfer the money to your bank account. </li>
                </ol>
                <h3>Why do I need to submit proof of ID and proof of address?</h3>
                <p>These are required to comply with anti money-laundering regulations.</p>
                <h3>What forms of proof ID and proof of address are acceptable and how do I submit them?</h3>
                <p>To submit your proof of ID and proof of address, please attach .jpeg .png or pdf files in an email and send them to <a href="mailto:cosec@madesimplegroup.com">cosec@madesimplegroup.com</a> with the subject line ‘Proof of ID - Professional’. If you have not submitted ID before the first cash back or credit payment is due to be made, our team will send you a reminder email. Acceptable forms of identification include passport, driving license (with photocard), National Identity card, HM Forces Identity card, Employment Identification card and Disabled drivers blue pass. Acceptable forms of proof of address include gas or electricity bill, telephone bill (excluding mobile phone bill), water bill, mortgage statement, council tax bill, bank statement or a TV licence. All proof of address documents must show your name and address and have been issued within the last 3 months.</p>
                <h3>How do I notify you of my preferred method for receiving cash back?</h3>
                <p>Please state your preference <a href="/cash-back-claim.html">here</a> (log in required).</p>
                <h3>If my client successfully opens a the Barclays Bank Account, who gets the cash back, them or me?</h3>
                <p>It's up to you. We can either add the cash back to your account as credit for you to use in the future, or we can transfer it to your account. You're then free to do with it as you wish; keep it, share with your client, or give it all to your client.</p>
                <h3>Will Barclays mention Companies Made Simple or the cash back to my client in any step of the process?</h3>
                <p>No, it's between you and us.</p>
                <h3>Is the Barclays Bank Account still available for clients not based in the UK?</h3>
                <p>Unfortunately no. We can only offer this service to your UK based clients.</p>
                <h3>Can I meet with Barclays on my clients' behalf? </h3>
                <p>No, your client must attend the face-to-face with Barclays.</p>
                <h3>Can you transfer the cash to my clients' bank account?</h3>
                <p>No, we can only transfer to your account.</p>
                <h3>How long does it take to get my cash back?</h3>
                <p>The whole process from the initial contact with Barclays, the account being opened and Barclays confirming to us that we can issue you with a cash back is about 4-6 weeks. </p>
                <h3>When I enter the initial contact information, shall I put down my details or my clients'?</h3>
                <p>For simplicity's sake, we recommend providing your clients contact information.</p>
            </div>
        </div>
    </div>
    <!--
    <div id="campaign50">
        <div id="box8container">
            <div class="w-left">
                <h2 class="mnone grey12">Spend £50. <strong>Get £50.</strong></h2>
                <p class="midfont grey12">When you spend <strong>£50</strong> with <strong>Company Formation MadeSimple</strong> we will credit your account with another <strong>£50.</strong></p>
                <p class="midfont grey12">To qualify for the £50 credit, spend £50 (exclusive of VAT) or more in a single purchase or multiple purchases between 16/05/2016 and 31/05/2016.</p>
                <p class="littlefont grey12">
                    Terms and Conditions: Only Company Formation MadeSimple customers with a ‘Professional’ account qualify. 
                    If you would like to upgrade your account please email cosec@madesimplegroup.com. Only one £50 credit per 
                    account (accounts that have already claimed £50 through the same offer this year do not qualify).  
                    The credit on your account is non-refundable. If you credit your account to the sum of £50 (or more), or 
                    make up your order value with a credit payment to claim the special £50 credit offer - please note that all 
                    credit is non-refundable. Offer expires on 31/05/2016 (we reserve the right to withdraw the offer at our discretion). 
                    Last chance to claim your £50 credit is 08/06/2016. Credit will be issued within 5 working days of the claim form being completed. 
                </p>
            </div>
            <div class="w-right">
                <h2 class="mnone grey12">Claim your £50 credit.</h2>
                <p class="midfont grey12">
                    Please fill in the form below (once you have spent £50 or more) and one of our team will add the credit to your account within 5 working days.
                    We will notify you via email once credit has been added.
                </p>
                <div class="txtright padding20">
                    {$form->getBegin() nofilter}
                    <table class="ff_table">
                        <tbody>
                            <tr>
                                <th>{$form->getLabel('name') nofilter}</th>
                                <td>{$form->getControl('name') nofilter}</td>
                                <td><span class="ff_control_err">{$form->getError('name')}</span></td>
                            </tr>
                            <tr>
                                <th>{$form->getLabel('companyName') nofilter}</th>
                                <td>{$form->getControl('companyName') nofilter}</td>
                                <td><span class="ff_control_err">{$form->getError('companyName')}</span></td>
                            </tr>
                            <tr>
                                <th>{$form->getLabel('email') nofilter}</th>
                                <td>{$form->getControl('email') nofilter}</td>
                                <td><span class="ff_control_err">{$form->getError('email')}</span></td>
                            </tr>
                            <tr>
                                <th colspan="3">{$form->getControl('submit') nofilter}</th>
                            </tr>
                        </tbody>
                    </table>
                    {$form->getEnd() nofilter}
                </div>
            </div>
        </div>
    </div>
    -->
</div>

</div>
<div class="clear"></div>

{include file="@footer.tpl"}
