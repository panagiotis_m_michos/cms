{include file="@header.tpl"}

<div id="wholemicro">
    <div id="wrapper">
        <h1 class="mtop20">Company Formation Packages</h1>
        <div class="w-greybox">
            <span class="fleft">To form a company, simply <a href="{$this->router->link('#signUp#')}">sign up</a> for a free account or <a href="{$this->router->link(LoginControler::LOGIN_PAGE)}">log in</a> and<br />select one of our great value packages.</span>
            <span class="fright fwdbutton"><a href="{$this->router->link('#signUp#')}">Sign Up</a></span>
        </div>
            
        <div class="w-matrix-wrap barclays-feat">
            <div class="">
                <div class="w-matrix-e"></div>
                <div class="w-matrix-packname"> 
                    Starter
                </div>
                <div class="w-matrix-packname"> 
                    Executive
                </div>
                <div class="w-matrix-packname"> 
                    Professional
                </div>
            </div>
            <div class="">
                <div class="w-matrix-e"></div>
                <div class="w-matrix-price"> 
                    &pound;13.99 <span class="w-matrix-vat">+VAT</span>
                    <div class="w-matrix-price-cross"> 
                        <span class="w-matrix-pcross">&pound;29.99</span> <span class="w-matrix-vat">+VAT</span>
                    </div>
                </div>
                <div class="w-matrix-price"> 
                    &pound;23.99 <span class="w-matrix-vat">+VAT</span>
                    <div class="w-matrix-price-cross"> 
                        <span class="w-matrix-pcross">&pound;33.99</span> <span class="w-matrix-vat">+VAT</span>
                    </div>
                </div>
                <div class="w-matrix-price"> 
                    &pound;39.99 <span class="w-matrix-vat">+VAT</span>
                    <div class="w-matrix-price-cross"> 
                        <span class="w-matrix-pcross">&pound;59.99</span> <span class="w-matrix-vat">+VAT</span>
                    </div>
                </div>
            </div>
            <div class="w-matrix-row">
                <div class="w-matrix1">  
                    <div class="matrix1-column-left matrix-open-image"></div>
                    <span class="w-matrix-descr1">Ready to Trade Company -<br />Limited by Shares</span>
                    <div class="w-qmark mtop">
                        <i class="fa fa-question-circle grey2 font20 mleft10" data-toggle="tooltip" data-placement="right" title="
                           All the necessary legal work completed, your client could start trading today!
                    <br /><br />All companies are provided with a Companies House WebFiling Authentication Code upon incorporation. This allows the company to securely use the Companies House WebFiling System after incorporation.
                        "></i>
                    </div>
                </div>
                <div class="w-matrix2 w-matrix-tick"></div>
                <div class="w-matrix2 w-matrix-tick"></div>
                <div class="w-matrix2 w-matrix-tick"></div>
            </div>
            <div class="w-matrix-row">
                <div class="w-matrix1">  
                    <div class="matrix1-column-left matrix-pound-image"></div>
                    <span class="w-matrix-descr1">&pound;50 Cash Back with every<br />Barclays Bank Account</span>
                    <div class="w-qmark mtop">
                        <i class="fa fa-question-circle grey2 font20 mleft10" data-toggle="tooltip" data-placement="right" title="
                           For every Barclays Bank Account opened as part of the formation process, we’ll give you &pound;50 cash back. This can be used as credit on your account for future purchases, or transferred into your bank account. What you do with the &pound;50 is completely up to you - keep it and boost your profits, give it to your client, or share it! 
                    <br /><br />Please note we will require proof of ID before transferring the &pound;50 cash back to your bank account. 
                        "></i>
                    </div>
                </div>
                <div class="w-matrix3 w-matrix-tick"></div>
                <div class="w-matrix3 w-matrix-tick"></div>
                <div class="w-matrix3 w-matrix-tick"></div>
            </div>
            <div class="w-matrix-row">
                <div class="w-matrix1">  
                    <div class="matrix1-column-left matrix-gold-image"></div>
                    <span class="w-matrix-descr1">Companies House &pound;13<br />Fee Included in Price</span>
                    <div class="w-qmark mtop">
                        <i class="fa fa-question-circle grey2 font20 mleft10" data-toggle="tooltip" data-placement="right" title="
                           There are no hidden extras or surprise costs, the package price already includes the Companies House fee.
                        "></i>
                    </div>
                </div>
                <div class="w-matrix2 w-matrix-tick"></div>
                <div class="w-matrix2 w-matrix-tick"></div>
                <div class="w-matrix2 w-matrix-tick"></div>
            </div>
            <div class="w-matrix-row">
                <div class="w-matrix1">  
                    <div class="matrix1-column-left matrix-email-image"></div>
                    <span class="w-matrix-descr1">Official Company<br />Documents (Electronic)</span>
                    <div class="w-qmark mtop">
                        <i class="fa fa-question-circle grey2 font20 mleft10" data-toggle="tooltip" data-placement="right" title="
                           Including Shareholder Certificates, Articles of Association, Memorandum of Association.
                        "></i>
                    </div>
                </div>
                <div class="w-matrix3 w-matrix-tick"></div>
                <div class="w-matrix3 w-matrix-tick"></div>
                <div class="w-matrix3 w-matrix-tick"></div>
            </div>
            <div class="w-matrix-row">
                <div class="w-matrix1">  
                    <div class="matrix1-column-left matrix-monitor-image"></div>
                    <span class="w-matrix-descr1">Company Secretarial Portal<br />to manage your companies</span>
                    <div class="w-qmark mtop">
                        <i class="fa fa-question-circle grey2 font20 mleft10" data-toggle="tooltip" data-placement="right" title="
                           Our free management portal is the the only company secretarial software accredited by ICAEW. You can easily manage all your companies from one central place 
                    <br />
                    <br />- Access from any computer 24/7, no installation required
                    <br />- Easily bulk import all your existing companies
                    <br />- Prepare and file Annual Returns electronically for &pound;13 - the same fee as Companies House
                    <br />- View and sort companies by statutory filing dates for all your companies
                    <br />- Sync your companies with Companies House to receive up to date information
                    <br />- Track dividends and create vouchers
                        "></i>
                    </div>
                </div>
                <div class="w-matrix2 w-matrix-tick"></div>
                <div class="w-matrix2 w-matrix-tick"></div>
                <div class="w-matrix2 w-matrix-tick"></div>
            </div>
            <div class="w-matrix-row">
                <div class="w-matrix1">  
                    <div class="matrix1-column-left matrix-print-image"></div>
                    <span class="w-matrix-descr1">Printed Certificate of<br />Incorporation</span>
                    <div class="w-qmark mtop">
                        <i class="fa fa-question-circle grey2 font20 mleft10" data-toggle="tooltip" data-placement="right" title="
                           The company’s birth certificate!
                    <br /><br />We’ll send you a hard copy printed on Companies House approved paper, which you can pass on to your client.
                        "></i>
                    </div>
                </div>
                <div class="w-matrix3 w-matrix-tick"></div>
                <div class="w-matrix3 w-matrix-tick"></div>
                <div class="w-matrix3 w-matrix-tick"></div>
            </div>
            <div class="w-matrix-row">
                <div class="w-matrix1">  
                    <div class="matrix1-column-left matrix-rim-image"></div>
                    <span class="w-matrix-descr1">Dedicated Support<br />Staff</span>
                    <div class="w-qmark mtop">
                        <i class="fa fa-question-circle grey2 font20 mleft10" data-toggle="tooltip" data-placement="right" title="
                           We want your experience to be as simple and efficient as possible, so we’re here to help every step of the way. 
                        "></i>
                    </div>
                </div>
                <div class="w-matrix2 w-matrix-tick"></div>
                <div class="w-matrix2 w-matrix-tick"></div>
                <div class="w-matrix2 w-matrix-tick"></div>
            </div>
            <div class="w-matrix-row">
                <div class="w-matrix1">  
                    <div class="matrix1-column-left matrix-ma-image"></div>
                    <span class="w-matrix-descr1">Bound Memorandum<br />&amp; Articles of Association</span>
                    <div class="w-qmark mtop">
                        <i class="fa fa-question-circle grey2 font20 mleft10" data-toggle="tooltip" data-placement="right" title="
                           We’ll send you a bound copy of the Memorandum & Articles of Association immediately after incorporation.
                    <br /><br />The document is essentially a statement confirming that the subscribers wish to form a company under the 2006 Act, have agreed to become members, and to take at least one share each. 
                    <br /><br />It is often needed by insurance companies or for finance purposes (e.g. when leasing equipment or applying for a loan).
                        "></i>
                    </div>
                </div>
                <div class="w-matrix3 w-matrix-cross"></div>
                <div class="w-matrix3 w-matrix-tick"></div>
                <div class="w-matrix3 w-matrix-tick"></div>
            </div>
            <div class="w-matrix-row">
                <div class="w-matrix1">  
                    <div class="matrix1-column-left matrix-cr-image"></div>
                    <span class="w-matrix-descr1">Company Register<br />(Unbranded)</span>
                    <div class="w-qmark mtop">
                        <i class="fa fa-question-circle grey2 font20 mleft10" data-toggle="tooltip" data-placement="right" title="
                           A formal file for securely recording and storing company information.
                        "></i>
                    </div>
                </div>
                <div class="w-matrix2 w-matrix-cross"></div>
                <div class="w-matrix2 w-matrix-cross"></div>
                <div class="w-matrix2 w-matrix-tick"></div>
            </div>
            <div class="w-matrix-row">
                <div class="w-matrix1">  
                    <div class="matrix1-column-left matrix-ec1-image"></div>
                    <span class="w-matrix-descr1">Prestigious Registered<br />Office Address (Optional)</span>
                    <div class="w-qmark mtop">
                        <i class="fa fa-question-circle grey2 font20 mleft10" data-toggle="tooltip" data-placement="right" title="
                           Use our address as your client's Registered Office and we'll forward you official government mail (from Companies House, HMRC & Government Gateway) and block junk mail!
                    <br />
                    <br />- Prestigious Central London address (N1 postcode)
                    <br />- Cut out junk mail
                    <br />- Registered Offices must be in the UK - use ours if your client is located outside the UK
                    <br />- Your clients can avoid the inconvenience of changing address (and stationery) if they regularly move
                    <br />- We forward mail within one working day of receiving it
                    <br />- There are no postage or handling charges even if we forward the mail outside the UK
                    <br />- This service is renewable annually at &pound;49.99 + VAT
                        "></i>
                    </div>
                </div>
                <div class="w-matrix3 w-matrix-cross"></div>
                <div class="w-matrix3 w-matrix-cross"></div>
                <div class="w-matrix3 w-matrix-tick"></div>
            </div>
            <div class="w-matrix-row">
                <div class="w-matrix1">  
                    <div class="matrix1-column-left matrix-sa-image"></div>
                    <span class="w-matrix-descr1">Service Address for all<br />company directors (Optional)</span>
                    <div class="w-qmark mtop">
                        <i class="fa fa-question-circle grey2 font20 mleft10" data-toggle="tooltip" data-placement="right" title="
                           This service is for all directors, shareholders and company secretaries who want to keep their residential address off the Companies House public register (and therefore keep it confidential)
                    <br />
                    <br />- Use of our London N1 address as your clients service address
                    <br />- Official mail (HMRC, Companies House) is forwarded to the client for one year - with no charge for postage
                    <br />- This service is renewable annually at &pound;49.99 + VAT
                        "></i>
                    </div>
                </div>
                <div class="w-matrix2 w-matrix-cross"></div>
                <div class="w-matrix2 w-matrix-cross"></div>
                <div class="w-matrix2 w-matrix-tick"></div>
            </div>
            <div class="">
                <div class="w-matrix-e"></div>
                <a href="{$this->router->link('#signUp#')}">
                    <div class="w-matrix-widebox w-matrix-widebutton"> 
                        Sign up to form a company
                    </div>
                </a>
            </div>
            <div class="">
                <div class="w-matrix-e"></div>
                <div class="w-matrix-widebox w-matrix-widelogin"> 
                        Already registered? <a href="{$this->router->link(LoginControler::LOGIN_PAGE)}">log in here</a>
                </div>
            </div>
            <div class="w-greybox1">
                <span class="fleft w-leftspan">
                    <strong>Limited by Guarantee</strong>
                    <br />3 hour online formation, Barclays Bank Account, printed Certificate of
                    <br />Incorporation, electronic Memorandum & Articles of Association
                </span>
                <span class="fleft fwdbutton w-rightspan">
                    <span class="w-matrix-vat">From</span> &pound;59 <span class="w-matrix-vat">+VAT</span>
                    <a href="{$this->router->link('#signUp#')}">Sign Up</a>
                </span>
            </div>
            <div class="w-greybox1">
                <span class="fleft w-leftspan">
                    <strong>Limited Liability Partnership (LLP)</strong>
                    <br />3 hour online formation, free LLP Agreement, printed Certificate of
                    <br />Incorporation
                </span>
                <span class="fleft fwdbutton w-rightspan">
                    <span class="w-matrix-vat">From</span> &pound;59.99 <span class="w-matrix-vat">+VAT</span>
                    <a href="{$this->router->link('#signUp#')}">Sign Up</a>
                </span>
            </div>
        </div>
        
    </div>        
</div>

{include file="@footer.tpl"}