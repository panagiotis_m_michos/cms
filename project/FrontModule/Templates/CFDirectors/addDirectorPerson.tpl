{include file="@header.tpl"}
<div class="formprocesstitle">
    {if $visibleTitle}
    <h1>{$title}</h1>
    {/if}

{*ERRORS*}
{$form->getBegin() nofilter}
{if $form->getErrors()|@count gt 0}
    <p class="ff_err_notice ff_red_err" style="width: 940px">Form has <b> {$form->getErrors()|@count}</b> error(s). See below for more details:</p>
{/if}

</div>

{* <div class="livesuprt"><script language="javascript" src="https://support.madesimplegroup.com/visitor/index.php?_m=livesupport&_a=htmlcode&departmentid=2"></script></div> *}

{* TABS *}
{include file="@blocks/navlist.tpl" currentTab="directors"}



<div>
    <p style="float: right; text-align: right;">
        To make a corporate appointment
        <a href="{$this->router->link("CFDirectorsControler::ADD_DIRECTOR_CORPORATE_PAGE", "company_id=$companyId")}">click here</a>
    </p>
    <div class="help-button" style="float: right; margin: 0 15px 0 0;">
        <a href="#" class="help-icon">help</a>
        {if $companyType != 'LLP'}
            <em>This option is available to you should you wish to nominate a company in the position of director. Please note that you cannot nominate the company you are forming to be a director of itself.</em>
        {else}
            <em>This option is available to you should you wish to nominate a company as a member. Please note that you cannot nominate the company you are forming to be a member of itself.</em>
        {/if}
    </div>
</div>

<div style="clear: both;">&nbsp;</div>


{* JS FOR PREFILL *}
<script type="text/javascript">
    /* <![CDATA[ */
    var addresses = {$jsPrefillAdresses nofilter};
    var officers = {$jsPrefillOfficers nofilter};
    /* ]]> */
</script>

{literal}
<script>
    $(document).ready(function () {

        // start on beginning
        ourRegisterOfficeHandler();

        // called on type click
        $("input[name='ourServiceAddress']").click(function () {
            ourRegisterOfficeHandler();
        });


        /**
         * Provides disable and enable dropdown for our offices
         * @return void
         */
        function ourRegisterOfficeHandler()
        {
            disabled = $("#ourServiceAddress").is(":checked");

            $('#residentialAddress').attr("checked", disabled);

            $('#prefillAddress').attr("disabled", disabled);
            $('#premise').attr("disabled", disabled);
            $('#street').attr("disabled", disabled);
            $('#thoroughfare').attr("disabled", disabled);
            $('#post_town').attr("disabled", disabled);
            $('#postcode').attr("disabled", disabled);
            $('#county').attr("disabled", disabled);
            $('#country').attr("disabled", disabled);
            if(disabled == true){
                toogleServiceAddress();
                $('#residentialAddress').parent().parent().hide();
                $("#service_address, #service_address_prefill").hide();
            }else{
                $("#service_address, #service_address_prefill").show();
                toogleServiceAddress();
                $('#residentialAddress').parent().parent().show();
            }
        }

        //load
        checkPostcode();
        toogleServiceAddress();

        // prefill address
        $("#prefillAddress").change(function () {
            var value = $(this).val();
            address = addresses[value];
            for (var name in address) {
                $('#'+name).val(address[name]);
            }
            checkPostcode();
        });

        // prefill officers
        $("#prefillOfficers").change(function () {
            var value = $(this).val();
            address = officers[value];
            for (var name in address) {
                $('#'+name).val(address[name]);
            }
            checkPostcode();
        });


        // service address
        $("#residentialAddress").click(function (){
            toogleServiceAddress();
        });

        function toogleServiceAddress() {
                disabled = !$("#residentialAddress").is(":checked");
                $("input[id^='residential_'], select[id^='residential_']").each(function () {
                    $(this).attr("disabled", disabled);
                });
            }
	    function checkPostcode() {
            if($('#postcode').val() == 'ec1v 4pw'  || $('#postcode').val() == 'ec1v4pw'
                || $("#residentialAddress").is(":checked")
                || $("#ourServiceAddress").is(":checked"))
            {
                $("#residentialAddress").attr('checked','checked');
                toogleServiceAddress();
            }else{
                $("#residentialAddress").attr('checked', false);
                toogleServiceAddress();
            }
        }
    });
</script>
{/literal}

<div>
    <div style="border:0px solid blue;float: left">
        <div style="width: 550px;">

            {include '../@blocks/officersPrefill.tpl'}

            <fieldset>
                <legend>Person</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <th>{$form->getLabel('title') nofilter}</th>
                            <td>{$form->getControl('title') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('title')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('forename') nofilter}</th>
                            <td>{$form->getControl('forename') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('forename')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('middle_name') nofilter}</th>
                            <td>{$form->getControl('middle_name') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('middle_name')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('surname') nofilter}</th>
                            <td>{$form->getControl('surname') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('surname')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('dob') nofilter}</th>
                            <td>
                                {$form->getControl('dob') nofilter}
                            </td>
                            <td><span class="redmsg">{$form->getError('dob')}</span></td>
                        </tr>
                        {if $companyType != 'LLP'}
                        <tr>
                            <th>{$form->getLabel('nationality') nofilter}</th>
                            <td>{$form->getControl('nationality') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('nationality')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('occupation') nofilter}</th>
                            <td>{$form->getControl('occupation') nofilter}</td>
                            <td>
                                <div class="help-button">
                                    <a href="#" class="help-icon">help</a>
                                    <em>This is your current business occupation. If you do not have one, please enter Company Director</em>
                                </div>
                                <span class="redmsg">{$form->getError('occupation')}</span>
                            </td>
                        </tr>
                        {/if}
                        <tr>
                            <th>{$form->getLabel('country_of_residence') nofilter}</th>
                            <td>{$form->getControl('country_of_residence') nofilter}</td>
                            <td>
                                <div class="help-button">
                                    <a href="#" class="help-icon">help</a>
                                    <em>This is the country the appointment is either currently living in or where they consider their main country of residence to be.</em>
                                </div>
                                <span class="redmsg">{$form->getError('country_of_residence')}</span>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </fieldset>

            {if isset($form.ourServiceAddress)}
            <fieldset id="fieldset_0">
                <legend>Use Our Service Address</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <th>{$form->getLabel('ourServiceAddress') nofilter}</th>
                            <td>{$form->getControl('ourServiceAddress') nofilter}</td>
                            <td>{$msgServiceAdress->premise}
                                {$msgServiceAdress->street},
                                {$msgServiceAdress->post_town},
                                {$msgServiceAdress->postcode}</td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <ul style="margin: 0px; color: #999999;">
                                    <li><i>Your officer&apos;s address will remain private; ours will show on the public register.</i></li>
                                    <li><i>Prevent junk mail; only receive your company's statutory mail.</i></li>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>
            {/if}

            <fieldset id="service_address_prefill">
                <legend>Prefill</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <th>{$form->getLabel('prefillAddress') nofilter}</th>
                            <td>{$form->getControl('prefillAddress') nofilter}</td>
                            <td>
                                <div class="help-button">
                                    <a href="#" class="help-icon">help</a>
                                    <em>You can use this prefill option to select an address you’ve entered previously.</em>
                                </div>
                                <span class="redmsg">{$form->getError('prefillAddress')}</span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>

            <fieldset id="service_address">
                <legend>Service Address</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <th>{$form->getLabel('premise') nofilter}</th>
                            <td>{$form->getControl('premise') nofilter}</td>
                            <td>
                                <div class="help-button">
                                    <a href="#" class="help-icon">help</a>
                                    <em>A service address is the contact address Companies House will use for their records. It does not have to be your residential address.</em>
                                </div>
                                <span class="redmsg">{$form->getError('premise')}</span>
                            </td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('street') nofilter}</th>
                            <td>{$form->getControl('street') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('street')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('thoroughfare') nofilter}</th>
                            <td>{$form->getControl('thoroughfare') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('thoroughfare')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('post_town') nofilter}</th>
                            <td>{$form->getControl('post_town') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('post_town')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('county') nofilter}</th>
                            <td>{$form->getControl('county') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('county')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('postcode') nofilter}</th>
                            <td>{$form->getControl('postcode') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('postcode') nofilter}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('country') nofilter}</th>
                            <td>{$form->getControl('country') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('country')}</span></td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>

            <fieldset>
                <legend>Residential Address</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <th colspan="3" style="padding-bottom: 10px;">The residential address must be the address where you live.</th>

                        </tr>
                        <tr>
                            <th>{$form->getLabel('residentialAddress') nofilter}</th>
                            <td>{$form->getControl('residentialAddress') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('residentialAddress')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('residential_premise') nofilter}</th>
                            <td>{$form->getControl('residential_premise') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('residential_premise')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('residential_street') nofilter}</th>
                            <td>{$form->getControl('residential_street') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('residential_street')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('residential_thoroughfare') nofilter}</th>
                            <td>{$form->getControl('residential_thoroughfare') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('residential_thoroughfare')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('residential_post_town') nofilter}</th>
                            <td>{$form->getControl('residential_post_town') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('residential_post_town')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('residential_county') nofilter}</th>
                            <td>{$form->getControl('residential_county') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('residential_county')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('residential_postcode') nofilter}</th>
                            <td>{$form->getControl('residential_postcode') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('residential_postcode')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('residential_country') nofilter}</th>
                            <td>{$form->getControl('residential_country') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('residential_country')}</span></td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>

            {if $companyType == 'LLP'}
                <fieldset>
                    <legend>Security</legend>
                    <table class="ff_table">
                        <tbody>
                        <tr>
                            <th>{$form->getLabel('BIRTOWN') nofilter}</th>
                            <td>{$form->getControl('BIRTOWN') nofilter}</td>
                            <td>
                                <div class="help-button">
                                    <a href="#" class="help-icon">help</a>
                                    <em>Security questions are used as online signatures by Companies House.</em>
                                </div>
                                <span class="redmsg">{$form->getError('BIRTOWN')}</span>
                            </td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('TEL') nofilter}</th>
                            <td>{$form->getControl('TEL') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('TEL')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('EYE') nofilter}</th>
                            <td>{$form->getControl('EYE') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('EYE')}</span></td>
                        </tr>
                        </tbody>
                    </table>
                </fieldset>
            {/if}

            <fieldset>
                <legend>Consent to act</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <td colspan="2">
                                <span class="redmsg">{$form->getError('consentToAct')}</span>
                                {$form->getControl('consentToAct') nofilter}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>

            <fieldset id="fieldset_2">
                <legend>Action</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <td><a href="{$this->router->link("CFDirectorsControler::DIRECTORS_PAGE", "company_id=$companyId")}" class="btn_back2 fleft clear">Back</a></td>
                            <td width="33%">&nbsp;</td>
                            <td>{$form->getControl('continue') nofilter}</td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>

            {$form->getEnd() nofilter}
        </div>
    </div>
    <div style="float: right; width: 285px;">

        {* GUIDE *}
        {if $companyType != 'LLP'}
        <div class="box1 fright">
            <h3>Guide:</h3>
            <div class="box1-foot pbottom15">
                <p>Your director cannot be younger than 16 years old.</p>
                <p>The new Companies Act 2006 requires you to state both a residential and service address. The service address will be shown on the public register as your contact address.</p>
                <p>Corporate directors cannot be the sole director. If you have a corporate director, you will need at least one natural (person) director as well.</p>
                <p>If you are forming a PLC you need a minimum of 2 directors.</p>
            </div>
        </div>
        {else}
        <div class="box1 fright">
            <h3>Guide:</h3>
            <div class="box1-foot pbottom15">
                <p>You must have at least 2 members to form an LLP.</p>
                <p>Each member is required to state both their residential address and a service address. The service address will be shown on the public register. It can be the same as the residential address or it can be another address where the member can be reached.</p>
                <p>All members will be set as Designated Members (this can be changed after incorporation, however, there must always be at least 2 designated members).</p>
                <p>A Designated Member has the same rights and duties as other (non-designated) members, but has extra responsibilities, such as authorising the submission of documents to Companies House.</p>
                <p>NB. The first member appointed will be the authorised member.</p>
            </div>
        </div>
        {/if}


        <div class="clear"></div>

        {* HTML INFO BOX *}
        {include file="@blocks/CFHtmlBox.tpl"}
    </div>
    <div class="clear"></div>
</div>

{include file="@footer.tpl"}
