{include file="@header.tpl"}
<div class="formprocesstitle">
    {if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
</div>

{* <div class="livesuprt"><script language="javascript" src="https://support.madesimplegroup.com/visitor/index.php?_m=livesupport&_a=htmlcode&departmentid=2"></script></div> *}

{* TABS *}
{include file="@blocks/navlist.tpl" currentTab=$this->action}

<div>
    <div style="border:0px solid blue;float: left">

        {* NOMINEE DIRECTOR *}
        {if isset($form)}
        <div style="width:69%">{$form nofilter}</div>
        {/if}

        <div class="box-blue">
            <div class="box-blue-bottom">
                {if $companyType != 'LLP'}
                <p class="hugefont">Director Summary</p>
                <p>Below is a list of the directors you have added so far:</p>
                {else}
                <p class="hugefont">Member Summary</p>
                <p>Below is a list of the members you have added so far:</p>
                {/if}
                <br />
                {* PERSONS *}
                <p class="hugefont">Persons</p>

                {if !empty($persons)}
                <table class="formtable mnone" width="550">
                    <col>
                    <col width="60">
                    <col width="70">
                    <tr>
                        <th>Full name</th>
                        <th class="center" colspan="2">Action</th>
                    </tr>
                    {foreach from=$persons key="key" item="person"}
                    <tr>
                        <td>{$key+1}. {$person.fullName}</td>
                        <td class="center"><a href="{$person.editLink}">Edit</a></td>
                        <td class="center"><a href="{$person.deleteLink}" onclick="return confirm('Are you sure?');">Delete</a></td>
                    </tr>
                    {/foreach}
                </table>
                {else}
                <p>No persons yet.</p>
                {/if}

                <br />

                {* CORPORATES *}
                <p class="hugefont">Corporates</p>

                {if !empty($corporates)}
                <table class="formtable mnone" width="550">
                    <col>
                    <col width="60">
                    <col width="70">
                    <tr>
                        <th>Full name</th>
                        <th class="center" colspan="2">Action</th>
                    </tr>
                    {foreach from=$corporates key="key" item="corporate"}
                    <tr>
                        <td>{$key+1}. {$corporate.fullName}</td>
                        <td class="center"><a href="{$corporate.editLink}">Edit</a></td>
                        <td class="center"><a href="{$corporate.deleteLink}" onclick="return confirm('Are you sure?');">Delete</a></td>
                    </tr>
                    {/foreach}
                </table>
                {else}
                <p>No corporates yet.</p>
                {/if}

                <br />

                {* NOMINEES *}
                {if !empty($nominees)}
                    <p class="hugefont">Nominees</p>
                    <table class="formtable mnone" width="550">
                        <col>
                        <tr>
                            <th>Full name</th>
                        </tr>
                        {foreach from=$nominees key="key" item="nominee"}
                            <tr>
                                <td>{$key+1}. {$nominee.fullName}</td>
                            </tr>
                        {/foreach}
                    </table>
                {/if}

                {* RESIDENTS *}
                {if !empty($residents)}
                    <p class="hugefont">Resident Director</p>
                    <table class="formtable mnone" width="550">
                        <col>
                        <tr>
                            <th>Full name</th>
                        </tr>
                        {foreach from=$residents key="key" item="resident"}
                            <tr>
                                <td>{$key+1}. {$resident.fullName}</td>
                            </tr>
                        {/foreach}
                    </table>
                {/if}

            </div>
        </div>


        <div class="box-pink">
            <div class="box-pink-bottom">
                <p class="hugefont noptop">What do you want to do next?</p>

                <table width="100%">
                    <col width="33%">
                    <col width="34%">
                    <col width="33%">
                    <tr>
                        <td><a class="btn_back2 mtop fleft clear" href="{$this->router->link("CFRegisteredOfficeControler::REGISTER_OFFICE_PAGE", "company_id=$companyId")}">Back</a></td>
                        <td><a href="{$this->router->link("CFDirectorsControler::ADD_DIRECTOR_PERSON_PAGE", "company_id=$companyId")}">
                                {if $companyType != 'LLP'}Add Another Director {else} Add Another Member {/if}</a></td>
                        <td align="right">{$continue nofilter}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>    
    <div style="float: right; width: 285px;">

        {* GUIDE *}
        {if $companyType != 'LLP'}
        <div class="box1 fright">	
            <h3>Guide:</h3>
            <div class="box1-foot pbottom15">
                <p>Your director cannot be younger than 16 years old.<p>
                <p>The new Companies Act 2006 requires you to state both a residential and service address. The service address will be shown on the public register as your contact address.</p>
                <p>Corporate directors cannot be the sole director. If you have a corporate director, you will need at least one natural (person) director as well.</p>
                <p>If you are forming a PLC you need a minimum of 2 directors.</p>		
            </div>
        </div>
        {else}
        <div class="box1 fright">	
            <h3>Guide:</h3>
            <div class="box1-foot pbottom15">
                <p>You must have at least 2 members to form an LLP.</p>
                <p>Each member is required to state both their residential address and a service address. The service address will be shown on the public register. It can be the same as the residential address or it can be another address where the member can be reached.</p>
                <p>All members will be set as Designated Members (this can be changed after incorporation, however, there must always be at least 2 designated members).</p>
                <p>A Designated Member has the same rights and duties as other (non-designated) members, but has extra responsibilities, such as authorising the submission of documents to Companies House.</p>
                <p>NB. The first member appointed will be the authorised member.</p>
            </div>
        </div>
        {/if}

        <div class="clear"></div>

        {* HTML INFO BOX *}
        {include file="@blocks/CFHtmlBox.tpl"}
    </div>
    <div class="clear"></div>
</div>        


{include file="@footer.tpl"}