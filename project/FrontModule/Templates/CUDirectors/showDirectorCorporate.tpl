{include file="@header.tpl"}
<div id="maincontent1">
	<p style="margin: 0 0 15px 0; font-size: 11px;"><a href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}">{$companyName}</a> &gt; {$title}</p> 
	{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
	<table class="grid2" width="780">
	<col width="200">
	<tr>
		<th colspan="2" class="center">Corporate</th>
	</tr>
	<tr>
		<th>Company name</th>
		<td>{$director.corporate_name}</td>
	</tr>
	<tr>
		<th>First name</th>
		<td>{$director.forename}</td>
	</tr>
	<tr>
		<th>Last name</th>
		<td>{$director.surname}</td>
	</tr>
    
    {assign var="type" value=$company->getType()}

    {if $type == 'LLP'}
    <tr>
		<th>Designated member</th>
		<td>{if $director.designated_ind == 1} Yes {else} No {/if}</td>
	</tr>
	{/if}
	
    <tr>
		<th colspan="2" class="center">Address</th>
	</tr>
	<tr>
		<th>Address 1</th>
		<td>{$director.premise}</td>
	</tr>
	<tr>
		<th>Address 2</th>
		<td>{$director.street}</td>
	</tr>
	<tr>
		<th>Address 3</th>
		<td>{$director.thoroughfare}</td>
	</tr>
	<tr>
		<th>Town</th>
		<td>{$director.post_town}</td>
	</tr>
	<tr>
		<th>County</th>
		<td>{$director.county}</td>
	</tr>
	<tr>
		<th>Postcode</th>
		<td>{$director.postcode}</td>
	</tr>
	<tr>
		<th>Country</th>
		<td>{$director.country}</td>
	</tr>
	</table>
</div>
{include file="@footer.tpl"}
