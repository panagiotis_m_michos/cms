{include file="@header.tpl"}

{* JS FOR PREFILL *}
<script type="text/javascript">
/* <![CDATA[ */
var addresses = {$jsPrefillAdresses nofilter};

/* ]]> */
</script>

{literal}
<script>
$(document).ready(function () {

	// prefill address	
	$("#prefillAddress").change(function () {
		var value = $(this).val();
		address = addresses[value];
		for (var name in address) {
			$('#'+name).val(address[name]);
		}
	});
	
	// change name 
	toogleChangeName();
	$("#changeName").click(function (){
		toogleChangeName();
	});
	function toogleChangeName() {
		disabled = !$("#changeName").is(":checked");
		$('#corporate_name').attr("disabled", disabled);
	}
	
	// change designation
	toogleDesignation();
	$("#changeDesignation").click(function (){
		toogleDesignation();
	});
	function toogleDesignation() {
		disabled = !$("#changeDesignation").is(":checked");
		$('#designated_ind0').attr("disabled", disabled);
        $('#designated_ind1').attr("disabled", disabled);
		$('#consentToAct').attr("disabled", disabled);
		$('#BIRTOWN').attr("disabled", disabled);
		$('#TEL').attr("disabled", disabled);
		$('#EYE').attr("disabled", disabled);
	}
    
	$('[name="designated_ind"]').change(function (){
		setConsentLabel();
	}).trigger('change');

	function setConsentLabel() {
		var value = $('[name="designated_ind"]:checked').val();
		var label = $('#consentToAct').data('label-designated_ind-' + value);
		$('label[for="consentToAct"]').text(label);
	}

    	// change address 
	toogleChangeAddress();
	$("#changeAddress").click(function (){
		toogleChangeAddress();
	});
	function toogleChangeAddress() {
		disabled = !$("#changeAddress").is(":checked");
		$('#prefillAddress').attr("disabled", disabled);
		$('#premise').attr("disabled", disabled);
		$('#street').attr("disabled", disabled);
		$('#thoroughfare').attr("disabled", disabled);
		$('#post_town').attr("disabled", disabled);
		$('#county').attr("disabled", disabled);
		$('#postcode').attr("disabled", disabled);
		$('#country').attr("disabled", disabled);
	}
	
	// EEA
	toogleEEA();
	$("#changeEEA").click(function (){
		toogleEEA();
	});
	
	$("input[name='eeaType']").click(function (){
		toogleEEA();
	});
	function toogleEEA() {
		if (!$("#changeEEA").is(":checked")) {
			$("#place_registered").attr("disabled", true);
			$("#registration_number").attr("disabled", true);
			$("#law_governed").attr("disabled", true);
			$("#legal_form").attr("disabled", true);
			$("#eeaType1").attr("disabled", true);
			$("#eeaType2").attr("disabled", true);
		} else {
			$("#eeaType1").attr("disabled", false);
			$("#eeaType2").attr("disabled", false);
		
			// EEA
			if ($("#eeaType1").is(":checked")) {
				$("#place_registered").attr("disabled", false);
				$("#registration_number").attr("disabled", false);
				$("#law_governed").attr("disabled", true);
				$("#legal_form").attr("disabled", true);
			// Non EEA
			} else if ($("#eeaType2").is(":checked")) {
				$("#place_registered").attr("disabled", false);
				$("#registration_number").attr("disabled", false);
				$("#law_governed").attr("disabled", false);
				$("#legal_form").attr("disabled", false);
			} else {
				$("#place_registered").attr("disabled", true);
				$("#registration_number").attr("disabled", true);
				$("#law_governed").attr("disabled", true);
				$("#legal_form").attr("disabled", true);
			}
		}
	}
	
});
</script>
{/literal}

<div id="maincontent2">
	<p style="margin: 0 0 15px 0; font-size: 11px;"><a href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}">{$companyName}</a> &gt; Edit Corporate{if $companyType == 'LLP'} Member{else} Director{/if}</p> 
	{if $visibleTitle}
    <h1>Edit Corporate{if $companyType == 'LLP'} Member{else} Director{/if}</h1>
    {/if}
	{$form nofilter}
</div>

{include file="@footer.tpl"}
