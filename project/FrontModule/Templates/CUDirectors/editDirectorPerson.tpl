{include file="@header.tpl"}

{* JS FOR PREFILL *}
<script type="text/javascript">
/* <![CDATA[ */
var addresses = {$jsPrefillAdresses nofilter};
/* ]]> */
</script>

{literal}
<script>
$(document).ready(function () {
	
	// prefill address	
	$("#prefillAddress").change(function () {
		var value = $(this).val();
		address = addresses[value];
		for (var name in address) {
			$('#'+name).val(address[name]);
		}
	});
	
	// change name 
	toogleChangeName();
	$("#changeName").click(function (){
		toogleChangeName();
	});
	function toogleChangeName() {
		disabled = !$("#changeName").is(":checked");
		$('#title').attr("disabled", disabled);
		$('#forename').attr("disabled", disabled);
		$('#middle_name').attr("disabled", disabled);
		$('#surname').attr("disabled", disabled);
	}
	
    // change designation 
        toogleDesignation();
	$("#changeDesignation").click(function (){
		toogleDesignation();
	});
	function toogleDesignation() {
		disabled = !$("#changeDesignation").is(":checked");
		$('#designated_ind0').attr("disabled", disabled);
        $('#designated_ind1').attr("disabled", disabled);
		$('#BIRTOWN').attr("disabled", disabled);
		$('#TEL').attr("disabled", disabled);
		$('#EYE').attr("disabled", disabled);
		$('#consentToAct').attr("disabled", disabled);
	}
    
	$('[name="designated_ind"]').change(function (){
		setConsentLabel();
	}).trigger('change');

	function setConsentLabel() {
		var value = $('[name="designated_ind"]:checked').val();
		var label = $('#consentToAct').data('label-designated_ind-' + value);
		$('label[for="consentToAct"]').text(label);
	}

	// change address
	toogleChangeAddress();
	$("#changeAddress").click(function (){
		toogleChangeAddress();
	});
	function toogleChangeAddress() {
		disabled = !$("#changeAddress").is(":checked");
		$('#prefillAddress').attr("disabled", disabled);
		$('#premise').attr("disabled", disabled);
		$('#street').attr("disabled", disabled);
		$('#thoroughfare').attr("disabled", disabled);
		$('#post_town').attr("disabled", disabled);
		$('#county').attr("disabled", disabled);
		$('#postcode').attr("disabled", disabled);
		$('#country').attr("disabled", disabled);
	}
	
	// change residential address
	toogleResidentialAddress();
	$("#changeResidentialAddress").click(function (){
		toogleResidentialAddress();
	});
	function toogleResidentialAddress() {
		disabled = !$("#changeResidentialAddress").is(":checked");
		$("input[id^='residential_'], select[id^='residential_']").each(function () {
			$(this).attr("disabled", disabled);
		}); 
		$('#residentialSecureAddressInd').attr("disabled", disabled);
	}
	
	// change other 
	toogleChangeOther();
	$("#changeOther").click(function (){
		toogleChangeOther();
	});
	function toogleChangeOther() {
		disabled = !$("#changeOther").is(":checked");
		$('#nationality').attr("disabled", disabled);
		$('#occupation').attr("disabled", disabled);
		$('#country_of_residence').attr("disabled", disabled);
	}
	
});
</script>
{/literal}
<div id="maincontent2">
	<p style="margin: 0px 0 15px 0; font-size: 11px;"><a href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}">{$companyName}</a> &gt; Edit Person{if $companyType == 'LLP'} Member{else} Director{/if}</p>
	{if $visibleTitle}
    <h1>Edit Person{if $companyType == 'LLP'} Member{else} Director{/if}</h1>
    {/if}
	{$form nofilter}
</div>
{include file="@footer.tpl"}
