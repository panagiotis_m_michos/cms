<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"> 
<head> 
<meta name="description" content="{$description}" />
<meta name="keywords" content="{$keywords}" />
<title>{$seoTitle}</title>
	<link rel="stylesheet" href="{$urlCss}thickbox.css" type="text/css" /> 
	<link rel="stylesheet" href="{$urlCss}forms.css" type="text/css" />
	<link rel="stylesheet" href="{$urlCss}cosec.css" type="text/css" />
	<link rel="stylesheet" href="{$urlCss}icaew.css" type="text/css" />

	<script type="text/javascript">var urlRoot = "{$urlRoot}"</script>
    <script type="text/javascript" src="{$urlComponent}jquery/dist/jquery.min.js" ></script>
    <script type="text/javascript" src="{$urlComponent}jquery-migrate/jquery-migrate.min.js" ></script>
	<script type="text/javascript" src="{$urlJs}thickbox.js"></script>
    {literal}
    <script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-543198-9']);
    _gaq.push(['_trackPageview']);    

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
    </script>
    {/literal}
</head>

<body>

<div id="main_container">
<div id="header">
    <div id="header_logo"> <a href="#"></a></div>
    <div id="header_text"> <p id="header_big_text">Simple Company Secretarial Management</p>
    <p id="header_bold_text">Free online system to manage your companies for ICAEW members</p>
    </div>

     <div id="header_navigation">
      <ul id="nav">
	{foreach from=$topMenu item="item"}
		{if $item.is_selected}
			<li><p>{$item.title}</p></li>
		{else}
		<li><a href="{$item.url}">{$item.title}</a></li>
		{/if}
	{/foreach}
	</ul>
    </div>
  </div>
