
{if isset($showAdminBar) && $showAdminBar == 1}
	{literal}
	<style>
	div#adminBar {
		position: fixed;
		_position: absolute;
		z-index: 23178;
		bottom: 0;
		left: 0;
		font-family: Georgia, Verdana, Sans-serif;
		font-size: 12px;
		margin: 10px;
		padding: 10px;
		text-align: left;
		background: #ffffe0;
		color: #000;
		border: 1px dotted orange;
		cursor: move;
		opacity: .80;
		=filter: alpha(opacity=80);
	}
		#adminBar:hover {
			opacity: 1;
			=filter: none;
		}
		#adminBar a {
			color: #fff;
		}
		#adminBar strong {
			color: black;
		}
	</style>
	{/literal}
	
	<div id="adminBar">
		<div>
		<table>
		<tr>
			<th>Id:</th>
			<td>{$this->node->getId()}</td>
		</tr>
		<tr>
			<th>Front:</th>
			<td>{$this->node->frontControler}</td>
		</tr>
		<tr>
			<th>Admin:</th>
			<td>{$this->node->adminControler}</td>
		</tr>
		<tr>
			<th>Action:</th>
			<td>{$this->action}</td>
		</tr>
		<tr>
			<th>Created:</th>
			<td>{$this->node->dtc|df:"j M Y G:i"}</td>
		</tr>
		<tr>
			<th>Edited:</th>
			<td>{$this->node->dtm|df:"j M Y G:i"} </td>
		</tr>
		<tr>
			<th>Status:</th>
			<td>{$this->node->status}</td>
		</tr>
		</table>
		</div>
	</div>
	
	{literal}
	<script type="text/javascript"> 
	/* <![CDATA[ */
	document.getElementById('adminBar').onmousedown = function(e) {
		e = e || event;
		this.posX = parseInt(this.style.left + '0');
		this.posY = parseInt(this.style.top + '0');
		this.mouseX = e.clientX;
		this.mouseY = e.clientY;
	 
		var thisObj = this;
	 
		document.documentElement.onmousemove = function(e) {
			e = e || event;
			thisObj.style.left = (e.clientX - thisObj.mouseX + thisObj.posX) + "px";
			thisObj.style.top = (e.clientY - thisObj.mouseY + thisObj.posY) + "px";
			return false;
		};
	 
		document.documentElement.onmouseup = function(e) {
			document.documentElement.onmousemove = null;
			document.documentElement.onmouseup = null;
			return false;
		};
	};
	/* ]]> */
	</script>
	{/literal}
{/if}
