{include file="@header.tpl"}

<div id="maincontent2" class="reviews-satisfaction-page">
	{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
	
	<div class="text-box">
		<div class="text">{$text nofilter}</div>
		<div class="image">
			<span class="value">{$customerSatisfaction->getFinalPercentageValue()}</span>
			<img src="{$urlImgs}satisfaction-button.png" width="187" height="172" alt="customer satisfaction" />
		</div>
		<div class="clear"></div>
	</div> <!-- .text-box -->
	
	<h2 style="margin-bottom: 0; padding-bottom: 0;">{$customerSatisfaction->getFinalPercentageValue()}% Customer Satisfaction</h2>
	<p style="margin: 0 0 10px 0; padding-top: 0; font-size: 16px; color: #666;">Please score the following aspects of our company formation service. Where 5 = Excellent and 1 = Poor</p>
	
	<div class="box-chart">
		<div class="left">
			<table class="grid2">
			<col width="30">
			<col width="200">
			<col width="80">
			<col width="80">
			<tr>
				<th colspan="2"></th>
				<th class="center">Response Average</th>
				<th class="center">No. of Responses</th>
			</tr>
			<tr>
				<td>A</td>
				<td>Our online company formation process</td>
				<td class="center">{$customerSatisfaction->getFormationProcess('avg') nofilter}</td>
				<td class="center">{$customerSatisfaction->getFormationProcess('count') nofilter}</td>
			</tr>
			<tr>
				<td>B</td>
				<td>Communicating our products clearly and effectively</td>
				<td class="center">{$customerSatisfaction->getCommunicationOurProducts('avg') nofilter}</td>
				<td class="center">{$customerSatisfaction->getCommunicationOurProducts('count') nofilter}</td>
			</tr>
			<tr>
				<td>C</td>
				<td>Customer service support team</td>
				<td class="center">{$customerSatisfaction->getCustomerService('avg')}</td>
				<td class="center">{$customerSatisfaction->getCustomerService('count')}</td>
			</tr>
			<tr>
				<td>D</td>
				<td>Providing value for money</td>
				<td class="center">{$customerSatisfaction->getProvidingValueForMoney('avg')}</td>
				<td class="center">{$customerSatisfaction->getProvidingValueForMoney('count')}</td>
			</tr>
			<tr>
				<td>E</td>
				<td>Responding promptly to queries</td>
				<td class="center">{$customerSatisfaction->getRespondingPromptlyToQueries('avg')}</td>
				<td class="center">{$customerSatisfaction->getRespondingPromptlyToQueries('count')}</td>
			</tr>
			</table>
		</div>
		<div class="right">
			{$customerSatisfaction->getBarChart() nofilter}
		</div>
		<div class="clear"></div>
	</div>
	
	
	<h2 style="margin-bottom: 0; padding-bottom: 0;">{$serviceValue->getFinalPercentageValue()}% of customers rate our service as excellent</h2>
	<p style="margin: 0 0 10px 0; padding-top: 0; font-size: 16px; color: #666;">Overall, how do you rate the quality of products and services we provide? Where 5 = Excellent and 1 = Poor.</p>
	
	<div class="box-chart">
		<div class="left">
			<table class="grid2">
			<col width="30">
			<col width="200">
			<col width="80">
			<col width="80">
			<tr>
				<th colspan="2"></th>
				<th class="center">Response Percent</th>
				<th class="center">No. of Responses</th>
			</tr>
			<tr>
				<td>5</td>
				<td><div style="float: left; height: 12px; width: {$serviceValue->getGradeFive('width')}px; background: #A0C55F"></div></td>
				<td class="center">{$serviceValue->getGradeFive('percent')}</td>
				<td class="center">{$serviceValue->getGradeFive('count')}</td>
			</tr>
			<tr>
				<td>4</td>
				<td><div style="float: left; height: 12px; width: {$serviceValue->getGradeFour('width')}px; background: #7AB317"></div></td>
				<td class="center">{$serviceValue->getGradeFour('percent')}</td>
				<td class="center">{$serviceValue->getGradeFour('count')}</td>
			</tr>
			<tr>
				<td>3</td>
				<td><div style="float: left; height: 12px; width: {$serviceValue->getGradeThree('width')}px; background: #0D6759"></div></td>
				<td class="center">{$serviceValue->getGradeThree('percent')}</td>
				<td class="center">{$serviceValue->getGradeThree('count')}</td>
			</tr>
			<tr>
				<td>2</td>
				<td><div style="float: left; height: 12px; width: {$serviceValue->getGradeTwo('width')}px; background: #0B2E59"></div></td>
				<td class="center">{$serviceValue->getGradeTwo('percent')}</td>
				<td class="center">{$serviceValue->getGradeTwo('count')}</td>
			</tr>
			<tr>
				<td>1</td>
				<td><div style="float: left; height: 12px; width: {$serviceValue->getGradeOne('width')}px; background: #2A044A"></div></td>
				<td class="center">{$serviceValue->getGradeOne('percent')}</td>
				<td class="center">{$serviceValue->getGradeOne('count')}</td>
			</tr>
			</table>
		</div>
		<div class="right">
			{$serviceValue->getPieChart() nofilter}
		</div>
		<div class="clear"></div>
	</div>
	
	
	<h2 style="margin-bottom: 0; padding-bottom: 0;">{$recomendation->getFinalPercentageValue()}% will recommend our services to others</h2>
	<p style="margin: 0 0 10px 0; padding-top: 0; font-size: 16px; color: #666;">How likely is it that you will recommend our services in the future? Where 5 = Certainly and 1 = Definitely not</p>
	
	<div class="box-chart">
		<div class="left">
			<table class="grid2">
			<col width="30">
			<col width="200">
			<col width="80">
			<col width="80">
			<tr>
				<th colspan="2"></th>
				<th class="center">Response Percent</th>
				<th class="center">No. of Responses</th>
			</tr>
			<tr>
				<td>5</td>
				<td><div style="float: left; height: 12px; width: {$recomendation->getGradeFive('width')}px; background: #A0C55F"></div></td>
				<td class="center">{$recomendation->getGradeFive('percent')}</td>
				<td class="center">{$recomendation->getGradeFive('count')}</td>
			</tr>
			<tr>
				<td>4</td>
				<td><div style="float: left; height: 12px; width: {$recomendation->getGradeFour('width')}px; background: #7AB317"></div></td>
				<td class="center">{$recomendation->getGradeFour('percent')}</td>
				<td class="center">{$recomendation->getGradeFour('count')}</td>
			</tr>
			<tr>
				<td>3</td>
				<td><div style="float: left; height: 12px; width: {$recomendation->getGradeThree('width')}px; background: #0D6759"></div></td>
				<td class="center">{$recomendation->getGradeThree('percent')}</td>
				<td class="center">{$recomendation->getGradeThree('count')}</td>
			</tr>
			<tr>
				<td>2</td>
				<td><div style="float: left; height: 12px; width: {$recomendation->getGradeTwo('width')}px; background: #0B2E59"></div></td>
				<td class="center">{$recomendation->getGradeTwo('percent')}</td>
				<td class="center">{$recomendation->getGradeTwo('count')}</td>
			</tr>
			<tr>
				<td>1</td>
				<td><div style="float: left; height: 12px; width: {$recomendation->getGradeOne('width')}px; background: #2A044A"></div></td>
				<td class="center">{$recomendation->getGradeOne('percent')}</td>
				<td class="center">{$recomendation->getGradeOne('count')}</td>
			</tr>
			</table>
		</div>
		<div class="right">
			{$recomendation->getPieChart() nofilter}
		</div>
		<div class="clear"></div>
	</div>
	
	
	
	
</div>
  
{include file="@footer.tpl"}