{include file="@header.tpl"}

{literal}
    <style type="text/css">
        table.grid2 {
            margin-bottom: 10px;
            width: 100%;
        }
    </style>
{/literal}

<div id="maincontent">
    {* BREADCRUMBS *}
    <p style="margin: 0 0 15px 0; font-size: 11px;"><a href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}">{$companyName}</a> &gt; {$title}</p>

    {* SUBMISSION *}
    {assign var="submissionId" value=$submission->getId()}
	{if $visibleTitle}
        <h1>
            {$title}
            <span style="font-size: 12px; font-weight: 400;">
                <a href="{$this->router->link("CUHistoryViewerControler::PAGE_INCORPORATION", "company_id=$companyId","submission_id=$submissionId","pdf=1")}">
                    Print
                </a>
            </span>
        </h1>
    {/if}
    <table class="grid2">
        <col width="200">
        <tr>
            <th>Form Submission Id</th>
            <td>{$submissionId}</td>
        </tr>
        <tr>
            <th>Company Id</th>
            <td>{$submission->getCompanyId()}</td>
        </tr>
        <tr>
            <th>Form Identifier</th>
            <td>{$submission->getFormIdentifier()}</td>
        </tr>
        <tr>
            <th>Language</th>
            <td>{$submission->getLanguage()}</td>
        </tr>
        <tr>
            <th>Response</th>
            <td>{$submission->getResponseMessage()}</td>
        </tr>
        <tr>
            <th>Reject Reference</th>
            <td>{$submission->getRejectReference()}</td>
        </tr>
        <tr>
            <th>Examiner Telephone</th>
            <td>{$submission->getExaminerTelephone()}</td>
        </tr>
        <tr>
            <th>Examiner Comment</th>
            <td>{$submission->getExaminerComment()}</td>
        </tr>
        <tr>
            <th>Request Date</th>
            <td>{$submission->getFirstSubmissionDate()}</td>
        </tr>
        <tr>
            <th>Response Date</th>
            <td>{$submission->getLastSubmissionDate()}</td>
        </tr>
    </table>

    {if !empty($submissionErrors)}
        <h1>Submission Errors</h1>
        <table class="grid2">
            <col width="70">
            <col width="70">
            <col>
            <col width="70">
            <tr>
                <th>Id</th>
                <th>Reject code</th>
                <th>Description</th>
                <th>Instance number</th>
            </tr>
            {foreach $submissionErrors as $error}
            <tr>
                <td>{$error->form_submission_error_id}</td>
                <td>{$error->reject_code}</td>
                <td class="noOverflow">{$error->description}</td>
                <td>{$error->instance_number}</td>
            </tr>
            {/foreach}
        </table>
    {/if}

    {*COMPANY*}
    <h1>Company</h1>
    <table class="grid2">
        <col width="250">
        <tr>
            <th>Name</th>
            <td>{$companyName}</td>
        </tr>
        <tr>
            <th>Type</th>
            <td>{$companyType}</td>
        </tr>
    </table>

	{* REGISTERED OFFICE *}
    <h1>Registered Office</h1>

    <table class="grid2">
        <col width="250">
        <tr>
            <th>Address 1</th>
            <td>{$office->premise}</td>
        </tr>
        <tr>
            <th>Address 2</th>
            <td>{$office->street}</td>
        </tr>
        <tr>
            <th>Address 3</th>
            <td>{$office->thoroughfare}</td>
        </tr>
        <tr>
            <th>Town</th>
            <td>{$office->post_town}</td>
        </tr>
        <tr>
            <th>County</th>
            <td>{$office->county}</td>
        </tr>
        <tr>
            <th>Postcode</th>
            <td>{$office->postcode}</td>
        </tr>
        <tr>
            <th>Country</th>
            <td>{$office->country}</td>
        </tr>
    </table>

	{* DIRECTORS *}
    {if $origType != 'LLP'}
        <h1>Directors</h1>
    {else}
        <h1>Members</h1>
    {/if}

    <table class="grid2">
        <col width="250">
        {foreach from=$directors key="key" item="director"}
            <tr>
                {if $origType != 'LLP'}
                <th colspan="2" class="center">Director {$key+1}</th>
                {else}
                <th colspan="2" class="center">Member {$key+1}</th>
                {/if}
            </tr>

            {* CORPORATE DIRECTOR *}
            {if isset($director->corporate_name)}
            <tr>
                <th>Company Name</th>
                <td>{$director->corporate_name}</td>
            </tr>
            <tr>
                <th>Authorizing Person First Name</th>
                <td>{$director->forename}</td>
            </tr>
            <tr>
                <th>Authorizing Person Last Name</th>
                <td>{$director->surname}</td>
            </tr>
            {* PERSON DIRECTOR *}
            {else}

            <tr>
                <th>Title</th>
                <td>{$director->title}</td>
            </tr>
            <tr>
                <th>First Name</th>
                <td>{$director->forename}</td>
            </tr>
            <tr>
                <th>Middle Name</th>
                <td>{$director->middle_name}</td>
            </tr>
            <tr>
                <th>Last Name</th>
                <td>{$director->surname}</td>
            </tr>
            <tr>
                <th>DOB</th>
                <td>{$director->dob}</td>
            </tr>
            <tr>
                <th>Nationality</th>
                <td>{$director->nationality}</td>
            </tr>
            <tr>
                <th>Occupation</th>
                <td>{$director->occupation}</td>
            </tr>
            <tr>
                <th><b>Residential Address</b></th>
                <td></td>
            </tr>
            <tr>
                <th>Address 1</th>
                <td>{$director->residential_premise}</td>
            </tr>
            <tr>
                <th>Address 2</th>
                <td>{$director->residential_street}</td>
            </tr>
            <tr>
                <th>Address 3</th>
                <td>{$director->residential_thoroughfare}</td>
            </tr>
            <tr>
                <th>Town</th>
                <td>{$director->residential_post_town}</td>
            </tr>
            <tr>
                <th>County</th>
                <td>{$director->residential_county}</td>
            </tr>
            <tr>
                <th>Postcode</th>
                <td>{$director->residential_postcode}</td>
            </tr>
            <tr>
                <th>Country</th>
                <td>{$director->residential_country}</td>
            </tr>
            <tr>
                <th><b>Service Address</b></th>
                <td></td>
            </tr>
            {/if}
            <tr>
                <th>Address 1</th>
                <td>{$director->premise}</td>
            </tr>
            <tr>
                <th>Address 2</th>
                <td>{$director->street}</td>
            </tr>
            <tr>
                <th>Address 3</th>
                <td>{$director->thoroughfare}</td>
            </tr>
            <tr>
                <th>Town</th>
                <td>{$director->post_town}</td>
            </tr>
            <tr>
                <th>County</th>
                <td>{$director->county}</td>
            </tr>
            <tr>
                <th>Postcode</th>
                <td>{$director->postcode}</td>
            </tr>
            <tr>
                <th>Country</th>
                <td>{$director->country}</td>
            </tr>

            {* CORPORATE DIRECTOR *}
            {if isset($director->corporate_name)}
                {* EEA *}
                {if $director->eeaType}
            <tr>
                <th>Type</th>
                <td>EEA</td>
            </tr>
            <tr>
                <th>Country Registered</th>
                <td>{$director->place_registered}</td>
            </tr>
            <tr>
                <th>Registration number</th>
                <td>{$director->registration_number}</td>
            </tr>
                {* NON EEA *}
                {else}
            <tr>
                <th>Type</th>
                <td>Non EEA</td>
            </tr>
            <tr>
                <th>Country Registered</th>
                <td>{$director->place_registered}</td>
            </tr>
            <tr>
                <th>Registration numbers</th>
                <td>{$director->registration_number}</td>
            </tr>
            <tr>
                <th>Governing law</th>
                <td>{$director->law_governed}</td>
            </tr>
            <tr>
                <th>Legal Form</th>
                <td>{$director->legal_form}</td>
            </tr>
                {/if}
            {/if}

            <tr>
                <th>First three letters of Town of birth</th>
                <td>{$director->BIRTOWN}</td>
            </tr>
            <tr>
                <th>Last three digits of Telephone number</th>
                <td>{$director->TEL}</td>
            </tr>
            <tr>
                <th>First three letters of Eye colour</th>
                <td>{$director->EYE}</td>
            </tr>
        {/foreach}
    </table>

    {if $origType != 'LLP'}
    {* SHAREHOLDERS *}
    {if $origType != 'BYGUAR'}
    <h1>Shareholders</h1>
    {else}
    <h1>Members</h1>
    {/if}

    <table class="grid2">
        <col width="250">
        {foreach from=$shareholders key="key" item="shareholder"}
        <tr>
            {if $origType != 'BYGUAR'}
            <th colspan="2" class="center">Shareholder {$key+1}</th>
            {else}
            <th colspan="2" class="center">Member {$key+1}</th>
            {/if}
        </tr>
        {if isset($shareholder->corporate_name)}
        <tr>
            <th>Company Name</th>
            <td>{$shareholder->corporate_name}</td>
        </tr>
        <tr>
            <th>Authorizing Person First Name</th>
            <td>{$shareholder->forename}</td>
        </tr>
        <tr>
            <th>Authorizing Person Last Name</th>
            <td>{$shareholder->surname}</td>
        </tr>
        {else}
        <tr>
            <th>Title</th>
            <td>{$shareholder->title}</td>
        </tr>
        <tr>
            <th>First Name</th>
            <td>{$shareholder->forename}</td>
        </tr>
        <tr>
            <th>Middle Name</th>
            <td>{$shareholder->middle_name}</td>
        </tr>
        <tr>
            <th>Last Name</th>
            <td>{$shareholder->surname}</td>
        </tr>
        {/if}
        <tr>
            <th>Address 1</th>
            <td>{$shareholder->premise}</td>
        </tr>
        <tr>
            <th>Address 2</th>
            <td>{$shareholder->street}</td>
        </tr>
        <tr>
            <th>Address 3</th>
            <td>{$shareholder->thoroughfare}</td>
        </tr>
        <tr>
            <th>Town</th>
            <td>{$shareholder->post_town}</td>
        </tr>
        <tr>
            <th>County</th>
            <td>{$shareholder->county}</td>
        </tr>
        <tr>
            <th>Postcode</th>
            <td>{$shareholder->postcode}</td>
        </tr>
        <tr>
            <th>Country</th>
            <td>{$shareholder->country}</td>
        </tr>
        <tr>
            <th>First three letters of Town of birth</th>
            <td>{$shareholder->BIRTOWN}</td>
        </tr>
        <tr>
            <th>Last three digits of Telephone number</th>
            <td>{$shareholder->TEL}</td>
        </tr>
        <tr>
            <th>First three letters of Eye colour</th>
            <td>{$shareholder->EYE}</td>
        </tr>
        {if $origType != 'BYGUAR'}
        <tr>
            <th>Share Class</th>
            <td>{$shareholder->share_class}</td>
        </tr>
        <tr>
            <th>Shares</th>
            <td>{$shareholder->num_shares}</td>
        </tr>
        <tr>
            <th>Share Currency</th>
            <td>{$shareholder->currency}</td>
        </tr>
        <tr>
            <th>Share Value</th>
            <td>{$shareholder->share_value}</td>
        </tr>
        {/if}
        {/foreach}
    </table>
    {/if}
	{* SECRETARIES *}
    <h1>Secretaries</h1>
	{if !empty($secretaries)}
    <table class="grid2" width="780">
        <col width="250">
		{foreach from=$secretaries key="key" item="secretary"}
        <tr>
            <th colspan="2" class="center">Secretary {$key+1}</th>
        </tr>
	    {if isset($secretary->corporate_name)}
        <tr>
            <th>Company Name</th>
            <td>{$secretary->corporate_name}</td>
        </tr>
        <tr>
            <th>Authorizing Person First Name</th>
            <td>{$secretary->forename}</td>
        </tr>
        <tr>
            <th>Authorizing Person Last Name</th>
            <td>{$secretary->surname}</td>
        </tr>
	    {else}
        <tr>
            <th>Title</th>
            <td>{$secretary->title}</td>
        </tr>
        <tr>
            <th>First Name</th>
            <td>{$secretary->forename}</td>
        </tr>
        <tr>
            <th>Middle Name</th>
            <td>{$secretary->middle_name}</td>
        </tr>
        <tr>
            <th>Last Name</th>
            <td>{$secretary->surname}</td>
        </tr>
		    {/if}
        <tr>
            <th>Address 1</th>
            <td>{$secretary->premise}</td>
        </tr>
        <tr>
            <th>Address 2</th>
            <td>{$secretary->street}</td>
        </tr>
        <tr>
            <th>Address 3</th>
            <td>{$secretary->thoroughfare}</td>
        </tr>
        <tr>
            <th>Town</th>
            <td>{$secretary->post_town}</td>
        </tr>
        <tr>
            <th>County</th>
            <td>{$secretary->county}</td>
        </tr>
        <tr>
            <th>Postcode</th>
            <td>{$secretary->postcode}</td>
        </tr>
        <tr>
            <th>Country</th>
            <td>{$secretary->country}</td>
        </tr>


			{* CORPORATE SECRETARY *}
			{if isset($secretary->corporate_name)}
				{* EEA *}
				{if $secretary->eeaType}
        <tr>
            <th>Type</th>
            <td>EEA</td>
        </tr>
        <tr>
            <th>Country Registered</th>
            <td>{$secretary->place_registered}</td>
        </tr>
        <tr>
            <th>Registration number</th>
            <td>{$secretary->registration_number}</td>
        </tr>
				{* NON EEA *}
				{else}
        <tr>
            <th>Type</th>
            <td>Non EEA</td>
        </tr>
        <tr>
            <th>Country Registered</th>
            <td>{$secretary->place_registered}</td>
        </tr>
        <tr>
            <th>Registration numbers</th>
            <td>{$secretary->registration_number}</td>
        </tr>
        <tr>
            <th>Governing law</th>
            <td>{$secretary->law_governed}</td>
        </tr>
        <tr>
            <th>Legal Form</th>
            <td>{$secretary->legal_form}</td>
        </tr>
				{/if}
			{/if}

        <tr>
            <th>First three letters of Town of birth</th>
            <td>{$secretary->BIRTOWN}</td>
        </tr>
        <tr>
            <th>Last three digits of Telephone number</th>
            <td>{$secretary->TEL}</td>
        </tr>
        <tr>
            <th>First three letters of Eye colour</th>
            <td>{$secretary->EYE}</td>
        </tr>
		{/foreach}
    </table>
	{else}
    <table class="grid2">
        <tr>
            <th><p>No secretaries</p><th>
        <tr>
    </table>
	{/if}

    {if $origType != 'LLP'}
    {* ARTICLES *}
    <h1>Articles</h1>
    <table class="grid2">
        <col width="250">
        <tr>
            <th>Article</th>
            <td>{if !empty($articles.article)}{$articles.article.filename}{/if}</td>
        </tr>
        {if !empty($articles.support)}
        <tr>
            <th>Support document</th>
            <td>{$articles.support.filename}</td>
        </tr>
        {/if}
    </table>
    <p></p>
    {/if}
</div>


{include file="@footer.tpl"} 
