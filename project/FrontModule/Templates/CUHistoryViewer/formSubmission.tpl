{include file="@header.tpl"}

{literal}
    <style type="text/css">
        table.grid2 {
            margin-bottom: 10px;
            width: 100%;
        }
    </style>
{/literal}

<div id="maincontent">
    {* BREADCRUMBS *}
    <p style="margin: 0 0 15px 0; font-size: 11px;"><a href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}">{$companyName}</a> &gt; {$title}</p>
    
    {* SUBMISSION *}
    {assign var="submissionId" value=$submission->getId()}
	{if $visibleTitle}
        <h1>
            {$title}
            <span style="font-size: 12px;font-weight: 400;">
                <a href="{$this->router->link("CUHistoryViewerControler::PAGE_OTHER", "company_id=$companyId","submission_id=$submissionId","pdf=1")}">
                     Print
                </a>
            </span>
        </h1>
    {/if}	
    <table class="grid2">
        <col width="200">
        <tr>
            <th>Form Submission Id</th>
            <td>{$submissionId}</td>
        </tr>
        <tr>
            <th>Company Id</th>
            <td>{$submission->getCompanyId()}</td>
        </tr>
        <tr>
            <th>Form Identifier</th>
            <td>{$submission->getFormIdentifier()}</td>
        </tr>
        <tr>
            <th>Language</th>
            <td>{$submission->getLanguage()}</td>
        </tr>
        <tr>
            <th>Response</th>
            <td>{$submission->getResponseMessage()}</td>
        </tr>
        <tr>
            <th>Reject Reference</th>
            <td>{$submission->getRejectReference()}</td>
        </tr>
        <tr>
            <th>Examiner Telephone</th>
            <td>{$submission->getExaminerTelephone()}</td>
        </tr>
        <tr>
            <th>Examiner Comment</th>
            <td>{$submission->getExaminerComment()}</td>
        </tr>
        <tr>
            <th>Request Date</th>
            <td>{$submission->getFirstSubmissionDate()}</td>
        </tr>
        <tr>
            <th>Response Date</th>
            <td>{$submission->getLastSubmissionDate()}</td>
        </tr>
    </table>

    {if !empty($submissionErrors)}
        <h1>Submission Errors</h1>
        <table class="grid2">
            <col width="70">
            <col width="70">
            <col>
            <col width="70">
            <tr>
                <th>Id</th>
                <th>Reject code</th>
                <th>Description</th>
                <th>Instance number</th>
            </tr>
            {foreach from=$submissionErrors item="submissionError"}
                <tr>
                    <td>{$submissionError->form_submission_error_id}</td>
                    <td>{$submissionError->reject_code}</td>
                    <td class="noOverflow">{$submissionError->description}</td>
                    <td>{$submissionError->instance_number}</td>
                </tr>
            {/foreach}
        </table>
    {/if}

    <h1>Submission details</h1>
    {if !empty($data)}
        <table class="grid2">
        <col width="200">
            {foreach from=$data key="key" item="item"}
                <tr>
                    <th>{$key}</th>
                    <td>{$item}</td>
                </tr>
            {/foreach}
        </table>
    {else}
        <p>No data</p>
    {/if}

</div>

{include file="@footer.tpl"}
