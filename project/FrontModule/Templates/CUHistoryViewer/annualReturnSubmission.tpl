{include file="@header.tpl"}

{literal}
    <style type="text/css">
        table.grid2 {
            margin-bottom: 10px;
            width: 100%;
        }
    </style>
{/literal}

<div id="maincontent">
    {* BREADCRUMBS *}
    <p style="margin: 0 0 15px 0; font-size: 11px;">
        <a href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}">{$companyName}</a>&gt; {$title}
        {* <a style="float:right" href="{$this->router->link(null, "pdf=1")}">Save AR History as a PDF</a>*}
    </p>

    {* SUBMISSION *}
    {assign var="submissionId" value=$submission->getId()}
    {if $visibleTitle}
        <h1>
            {$title}
            <span style="font-size: 12px; font-weight: 400;">
                <a href="{$this->router->link("CUHistoryViewerControler::PAGE_ANNUAL_RETURN", "company_id=$companyId","submission_id=$submissionId","pdf=1")}">
                    Print
                </a>
            </span>
        </h1>
    {/if}
    <table class="grid2">
        <col width="200">
        <tr>
            <th>Form Submission Id</th>
            <td>{$submissionId}</td>
        </tr>
        <tr>
            <th>Company Id</th>
            <td>{$submission->getCompanyId()}</td>
        </tr>
        <tr>
            <th>Form Identifier</th>
            <td>{$submission->getFormIdentifier()}</td>
        </tr>
        <tr>
            <th>Language</th>
            <td>{$submission->getLanguage()}</td>
        </tr>
        <tr>
            <th>Response</th>
            <td>{$submission->getResponseMessage()}</td>
        </tr>
        <tr>
            <th>Reject Reference</th>
            <td>{$submission->getRejectReference()}</td>
        </tr>
        <tr>
            <th>Examiner Telephone</th>
            <td>{$submission->getExaminerTelephone()}</td>
        </tr>
        <tr>
            <th>Examiner Comment</th>
            <td>{$submission->getExaminerComment()}</td>
        </tr>
        <tr>
            <th>Request Date</th>
            <td>{$submission->getFirstSubmissionDate()}</td>
        </tr>
        <tr>
            <th>Response Date</th>
            <td>{$submission->getLastSubmissionDate()}</td>
        </tr>
    </table>

{if !empty($submissionErrors)}
    {* SUBMISSION ERRORS *}
    <h1>Submission Errors</h1>
        <table class="grid2">
        <col width="70">
        <col width="70">
        <col>
        <col width="70">
        <tr>
            <th>Id</th>
            <th>Reject code</th>
            <th>Description</th>
            <th>Instance number</th>
        </tr>
        {foreach from=$submissionErrors item="submissionError"}
        <tr>
            <td>{$submissionError->form_submission_error_id}</td>
            <td>{$submissionError->reject_code}</td>
            <td class="noOverflow">{$submissionError->description}</td>
            <td>{$submissionError->instance_number}</td>
        </tr>
        {/foreach}
        </table>
    {/if}

    {if isset($return)}
        <h1>Company Details</h1>
        <table class="grid2">
        <col width="250">
        <tr>
            <th>Company Name: </th>
            <td>{$return.company_name}</td>
        </tr>
        <tr>
            <th>Registration Number: </th>
            <td>{$return.company_number}</td>
        </tr>    
        <tr>
            <th>Company Category:</th>
            <td>{$return.company_category}</td>
        </tr>
        <tr>
            <th>Made Up Date:</th>
            <td>{$return.made_up_date}</td>
        </tr>
{*
        <tr>
            <th>Sic Code Type:</th>
            <td>{$return.sic_code_type}</td>
        </tr>
*}
        {foreach from=$return.sic_code key=key item=code}
        <tr>
            <th>Sic Code {$key+1}:</th>
            <td>{$code}</td>
        </tr>
        {/foreach}
        <tr>
            <th>Registered Office:</th>
            <td>
            {if isset($return.reg_office.premise) && $return.reg_office.premise != ''}
            {$return.reg_office.premise}<br />
            {/if}
            {if isset($return.reg_office.street) && $return.reg_office.street != ''}
            {$return.reg_office.street}<br />
            {/if}
            {if isset($return.reg_office.thoroughfare) && $return.reg_office.thoroughfare != ''}
            {$return.reg_office.thoroughfare}<br />
            {/if}
            {if isset($return.reg_office.post_town) && $return.reg_office.post_town != ''}
            {$return.reg_office.post_town}<br />
            {/if}
            {if isset($return.reg_office.county) && $return.reg_office.county != ''}
            {$return.reg_office.county}<br />
            {/if}
            {if isset($return.reg_office.country) && $return.reg_office.country != ''}
            {$return.reg_office.country}<br />
            {/if}
            {if isset($return.reg_office.postcode) && $return.reg_office.postcode != ''}
            {$return.reg_office.postcode}<br />
            {/if}
            {if isset($return.reg_office.care_of_name) && $return.reg_office.care_of_name != ''}
            {$return.reg_office.care_of_name}<br />
            {/if}
            {if isset($return.reg_office.po_box) && $return.reg_office.po_box != ''}
            {$return.reg_office.po_box}<br />
            {/if}
            {if isset($return.reg_office.secure_address_ind) && $return.reg_office.secure_address_ind != ''}
            {$return.reg_office.secure_address_ind}<br />
            {/if}
            </td>
        </tr>
        {if isset($return.sail_address.country) && ($return.sail_address.country != 'UNDEF' && $return.sail_address.country != '')}
        <tr>
            <th>Sail Address:</th>
            <td>
            {if isset($return.sail_address.premise) && $return.sail_address.premise != ''}
            {$return.sail_address.premise}<br />
            {/if}
            {if isset($return.sail_address.street) && $return.sail_address.street != ''}
            {$return.sail_address.street}<br />
            {/if}
            {if isset($return.sail_address.thoroughfare) && $return.sail_address.thoroughfare != ''}
            {$return.sail_address.thoroughfare}<br />
            {/if}
            {if isset($return.sail_address.post_town) && $return.sail_address.post_town != ''}
            {$return.sail_address.post_town}<br />
            {/if}
            {if isset($return.sail_address.county) && $return.sail_address.county != ''}
            {$return.sail_address.county}<br />
            {/if}
            {if isset($return.sail_address.country) && $return.sail_address.country != ''}
            {$return.sail_address.country}<br />
            {/if}
            {if isset($return.sail_address.postcode) && $return.sail_address.postcode != ''}
            {$return.sail_address.postcode}<br />
            {/if}
            {if isset($return.sail_address.care_of_name) && $return.sail_address.care_of_name != ''}
            {$return.sail_address.care_of_name}<br />
            {/if}
            {if isset($return.sail_address.po_box) && $return.sail_address.po_box != ''}
            {$return.sail_address.po_box}<br />
            {/if}
            {if isset($return.sail_address.secure_address_ind) && $return.sail_address.secure_address_ind != ''}
            {$return.sail_address.secure_address_ind}<br />
            {/if}
            </td>
        </tr>
        {/if}
{*
        {if isset($return.members_list) && $return.members_list}
        <tr>
            <th>Members List:</th>
            <td>{$return.members_list}</td>
        </tr>
        {/if}
*}
        </table>
        
        <h1>Officers</h1>
        {if isset($return.officers) && $return.officers != ''}
        {foreach from=$return.officers key=k item=officer}
        {if $k!=0}<br /><br />{/if}
        <table class="grid2">
        <col width="250">
        {if isset($officer.title) && $officer.title != ''}
        <tr>
            <th>Officer Title:</th>
            <td>{$officer.title}</td>
        </tr>
        {/if}
        {if isset($officer.forename) && $officer.forename != ''}
        <tr>
            <th>Officer Forename:</th>
            <td>{$officer.forename}</td>
        </tr>
        {/if}
        {if isset($officer.middle_name) && $officer.middle_name}
        <tr>
            <th>Officer Middle Name:</th>
            <td>{$officer.middle_name}</td>
        </tr>
        {/if}
        {if isset($officer.surname) && $officer.surname}
        <tr>
            <th>Officer Surname:</th>
            <td>{$officer.surname}</td>
        </tr>
        {/if}
        {if isset($officer.corporate_name) && $officer.corporate_name}
        <tr>
            <th>Corporate Name:</th>
            <td>{$officer.corporate_name}</td>
        </tr>
        {/if}
        {if isset($officer.type) && $officer.type}
        <tr>
            <th>Officer Type:</th>
            <td>{$officer.type}</td>
        </tr>
        {/if}    
        <tr>
            <th>Address:</th>        
            <td>
            {if isset($officer.premise) && $officer.premise != ''}
            {$officer.premise}<br />
            {/if}
            {if isset($officer.street) && $officer.street != ''}
            {$officer.street}<br />
            {/if}
            {if isset($officer.thoroughfare) && $officer.thoroughfare != ''}
            {$officer.thoroughfare}<br />
            {/if}
            {if isset($officer.post_town) && $officer.post_town != ''}
            {$officer.post_town}<br />
            {/if}
            {if isset($officer.county) && $officer.county != ''}
            {$officer.county}<br />
            {/if}
            {if isset($officer.country) && $officer.country != ''}
            {$officer.country}<br />
            {/if}
            {if isset($officer.postcode) && $officer.postcode != ''}
            {$officer.postcode}<br />
            {/if}
            {if isset($officer.care_of_name) && $officer.care_of_name != ''}
            {$officer.care_of_name}<br />
            {/if}
            {if isset($officer.po_box) && $officer.po_box != ''}
            {$officer.po_box}<br />
            {/if}
            </td>
        </tr>
        {if isset($officer.previous_names) && isset($officer.typrevious_namespe)}
        <tr>
            <th>Previous Names:</th>
            <td>{$officer.previous_names}</td>
        </tr>
        {/if}
        {if isset($officer.type) && $officer.type == 'Director'}
            {if isset($officer.dob) && $officer.dob}
            <tr>
                <th>Date of Birth:</th>
                <td>{$officer.dob}</td>
            </tr>
            {/if}
            {if isset($officer.nationality) && $officer.nationality}
            <tr>
                <th>Nationality:</th>
                <td>{$officer.nationality}</td>
            </tr>
            {/if}
            {if isset($officer.occupation) && $officer.occupation}
            <tr>
                <th>Occupation:</th>
                <td>{$officer.occupation}</td>
            </tr>
            {/if}
        {/if}
        {if isset($officer.country_of_residence) && isset($officer.country_of_residence)}
        <tr>
            <th>Country of Residence:</th>
            <td>{$officer.country_of_residence}</td>
        </tr>
        {/if}
        {if isset($officer.corporate) && $officer.corporate}
            {if isset($officer.identification_type) && $officer.identification_type}
            <tr>
                <th>Identification Type:</th>
                <td>{$officer.identification_type}</td>
            </tr>
            {/if}
            {if isset($officer.place_registered) && $officer.place_registered}
            <tr>
                <th>Place Registered:</th>
                <td>{$officer.place_registered}</td>
            </tr>
            {/if}
            {if isset($officer.registration_number) && $officer.registration_number}
            <tr>
                <th>Registration Number:</th>
                <td>{$officer.registration_number}</td>
            </tr>
            {/if}
            {if isset($officer.law_governed) && $officer.law_governed}
            <tr>
                <th>Law Governed:</th>
                <td>{$officer.law_governed}</td>
            </tr>
            {/if}
            {if isset($officer.legal_form) && $officer.legal_form}
            <tr>
                <th>Legal Form:</th>
                <td>{$officer.legal_form}</td>
            </tr>
            {/if}
        {/if}
        </table>
        {/foreach}
        {/if}
    
        {if isset($return.shares) && $return.shares != ''}
        <h1>Shares</h1>
            {foreach from=$return.shares key=k item=share}
            
                {if $k!=0}

                {/if}
                
                <table class="grid2">
                <col width="250">
                {if isset($share.share_class) && $share.share_class!= ''}
                    <tr>
                        <th>Share Class:</th>
                        <td>{$share.share_class}</td>
                    </tr>
                {/if}
                {if isset($share.prescribed_particulars) && $share.prescribed_particulars!= ''}
                    <tr>
                        <th>Prescribed Particulars:</th>
                        <td>{$share.prescribed_particulars}</td>
                    </tr>
                {/if}
                {if isset($share.num_shares) && $share.num_shares!= ''}
                    <tr>
                        <th>Number of Shares:</th>
                        <td>{$share.num_shares}</td>
                    </tr>
                {/if}
                {if isset($share.amount_paid) && $share.amount_paid!= ''}
                    <tr>
                        <th>Paid Per Share:</th>
                        <td>{$share.amount_paid}</td>
                    </tr>
                {/if}
                {if isset($share.amount_unpaid) && $share.amount_unpaid!= ''}
                    <tr>
                        <th>Unpaid Per Share:</th>
                        <td>{$share.amount_unpaid}</td>
                    </tr>
                {/if}
                {if isset($share.currency) && $share.currency!= ''}
                    <tr>
                        <th>Currency:</th>
                        <td>{$share.currency}</td>
                    </tr>
                {/if}
                {if isset($share.aggregate_nom_value) && isset($share.currenaggregate_nom_valuecy) && $share.currenaggregate_nom_valuecy!= ''}
                    <tr>
                        <th>Aggregate Nom Value:</th>
                        <td>{$share.aggregate_nom_value}</td>
                    </tr>
                {/if}
                </table>
            {/foreach}
        {/if}

        {if isset($return.shareholdings) && $return.shareholdings != ''}
        <h1>Shareholdings</h1>
            {foreach from=$return.shareholdings key=k item=shareholding}
                <table class="grid2">
                <col width="250">
                {if isset($shareholding.share_class) && $shareholding.share_class!= ''}
                <tr>
                    <th>Share Class:</th>
                    <td>{$shareholding.share_class}</td>
                </tr>
                {/if}
                {if isset($shareholding.number_held) && $shareholding.number_held!= ''}
                <tr>
                    <th>Number Held:</th>
                    <td>{$shareholding.number_held}</td>
                </tr>
                {/if}
            {foreach from=$shareholding.shareholder key=k item=shareholder}
                {if isset($shareholder.forename) && $shareholder.forename!= ''}
                <tr>
                    <th>Forename:</th>
                    <td>{$shareholder.forename}</td>
                </tr>
                {/if}
                {if isset($shareholder.middle_name) && $shareholder.middle_name!= ''}
                <tr>
                    <th>Middle Name:</th>
                    <td>{$shareholder.middle_name}</td>
                </tr>
                {/if}
                {if isset($shareholder.surname) && $shareholder.surname!= ''}
                <tr>
                    <th>Surname:</th>
                    <td>{$shareholder.surname}</td>
                </tr>
                {/if}    
                <tr>
                    <th>Address:</th>        
                    <td>
                    {if isset($shareholder.premise) && $shareholder.premise!= ''}
                    {$shareholder.premise}<br />
                    {/if}
                    {if isset($shareholder.street) && $shareholder.street!= ''}
                    {$shareholder.street}<br />
                    {/if}
                    {if isset($shareholder.thoroughfare) && $shareholder.thoroughfare!= ''}
                    {$shareholder.thoroughfare}<br />
                    {/if}
                    {if isset($shareholder.post_town) && $shareholder.post_town!= ''}
                    {$shareholder.post_town}<br />
                    {/if}
                    {if isset($shareholder.county) && $shareholder.county!= ''}
                    {$shareholder.county}<br />
                    {/if}
                    {if isset($shareholder.country) && $shareholder.country!= ''}
                    {$shareholder.country}<br />
                    {/if}
                    {if isset($shareholder.postcode) && $shareholder.postcode!= ''}
                    {$shareholder.postcode}<br />
                    {/if}
                    </td>
                </tr>    
            {/foreach}
                </table>
            {/foreach}
        {/if}
    {/if}
    <p></p>
</div>
{include file="@footer.tpl"}
