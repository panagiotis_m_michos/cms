{include file="@headerPopUp.tpl"}

<div class="feedback" >
    <h1>We <span class="heart">&hearts;</span> Feedback</h1>
    {$form->getBegin() nofilter}
            <table width="387px" border="0">
                <col width="1"/>
                <tr>
                    <td style="padding-bottom: 3px;">
                        <div style="padding-bottom:5px;">
                             <span class="number_format">1.</span>
                             <span style="margin-left: -5px;" class="lbl_format">{$form->getLabel('message') nofilter}</span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <div >{$form->getControl('message') nofilter}</div>
                        <span class="ff_control_err" style="margin-left: 100px;">{$form->getError('message')}</span>
                    </td>
                </tr>
                <tr>
                     <td>
                        <div>
                             <span class="number_format">2.</span>
                             <span  class="lbl_format">{$form->getLabel('radio') nofilter}</span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <div  class="radioSet">{$form->getControl('radio') nofilter}</div>
                        <span class="ff_control_err" style="margin-left: 100px;">{$form->getError('message')}</span>
                    </td>
                </tr>
                <tr>
                     <td>
                        <div>
                             <span class="number_format" style="float:left;padding-right: 16px;" >3.</span>
                             <span  class="lbl_format">{$form->getControl('feedbackSubmit') nofilter}</span>
                        </div>
                    </td>
		</tr>
            </table>
    {$form->getEnd() nofilter}
</div>
 <div class="feedback_text">
    <p>We use this feedback to improve your experience, please be as honest as you like.</p>
    <p>All feedback is completely anonymous.</p>
</div>
{literal}
<script type="text/javascript">
    $(document).ready(function(){
            updateRadioLabels()
            $('input[type=radio]').change(updateRadioLabels)
    })
    function updateRadioLabels(){
        $('input[type=radio]').each(function(){
                $('input[name=' + this.name +'] + label').removeClass('selected');
                $('input[name=' + this.name +']:checked + label').addClass('selected');
        })
    }
</script>
{/literal}
{include file="@footerPopUp.tpl"}



