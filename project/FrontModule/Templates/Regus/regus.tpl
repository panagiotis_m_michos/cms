{include file="@header.tpl"}

<div class="formprocesstitle">
    {if $visibleTitle}
        <h1>{$title}</h1>
    {/if}

    {*ERRORS*}
    {$form->getBegin() nofilter}
    {if $form->getErrors()|@count gt 0}
        <p class="ff_err_notice ff_red_err" style="width: 940px">Form has <b> {$form->getErrors()|@count}</b> error(s). See below for more details:</p>
    {/if}
</div>

{* REGUS *}
<div id="regwrap">
    <div id="regmain">
        <div id="regleft">
            <div id="regtitle">
                <h2>A professional image <strong>without the cost<br />of a physical office</strong>, <span class="pink2">from &pound;39</span></h2>
            </div>
            <div id="regimg"></div>
            <div>
                <ul id ="reglist">
                    <li>Build your reputation and win over new customers with a prestigious address.</li>
                    <li>Never miss a call - our professional teams will handle your telephone calls.</li>
                    <li>Leave the post to us so you can focus on your new business.</li>
                    <li>Get an immediate business presence at a fraction of the cost of traditional office.</li>
                    <li>Choose from 220 locations in the UK, for space wherever you need to be.</li>
                </ul>
            </div>
            <div id="regtestim">
                <div class="mbottom">
                    <span class="txtbold mright">What others are saying</span><span class="normalfont">Independent Trust Pilot reviews</span>
                </div>
                <div id="regqleft">&ldquo;</div>
                <div class="mleft20 lheight15 mtop-55">
                    <span>Turned out to be just what I needed to make my<br />small business happen. Thank you guys!</span>
                    <span id="regqright">&rdquo;</span>
                    <span class="largefont">- Steve</span>
                </div>
            </div>
        </div>
        <div id="regright">
            <div id="regformwrap">
                <div id="reg50off">50&#37; off<span class="hugefont txtnormal posrelative top-15">&#8224;</span></div>
                <div id="reglogo">
                    <div>In partnership with</div>
                    <img src="{$urlImgs}reg-logo.png" alt="Regus" width="118px" height="51px"/>
                    <div>Fill in your details and we'll get back to you</div>
                </div>
                <div id="regformtable">
                    <div>{$form->getLabel('firstName') nofilter}<span class="pink">*</span></div>
                    <div>{$form->getControl('firstName') nofilter}</div>
                    <div class="mbottom"><span class="redmsg">{$form->getError('firstName')}</span></div>
                    <div>{$form->getLabel('lastName') nofilter}<span class="pink">*</span></div>
                    <div>{$form->getControl('lastName') nofilter}</div>
                    <div class="mbottom"><span class="redmsg">{$form->getError('lastName')}</span></div>
                    <div>{$form->getLabel('email') nofilter}<span class="pink">*</span></div>
                    <div>{$form->getControl('email') nofilter}</div>
                    <div class="mbottom"><span class="redmsg">{$form->getError('email')}</span></div>
                    <div class="displaynone">{$form->getLabel('dialCode') nofilter}</div>
                    <div class="displaynone"><span class="redmsg">{$form->getError('dialCode')}</span></div>
                    <div>{$form->getLabel('phone') nofilter}<span class="pink">*</span></div>
                    <div>
                        <span>{$form->getControl('dialCode') nofilter}</span>
                        <span>{$form->getControl('phone') nofilter}</span>
                    </div>
                    <div class="mbottom"><span class="redmsg">{$form->getError('phone')}</span></div>
                    <div class="mtop disclosure">
                        <span class="pink">*</span><span>required fields</span>
                    </div>
                    <div class="mtop mbottom disclosure">
                        &#8224; Offer is available on purchases of virtual office products for up to 30 days after you enquire and cannot be used in conjunction with any other promotion (subject to a 12 month service agreement)
                    </div>
                    <div class="txtcenter mbottom">{$form->getControl('enquire') nofilter}</div>
                </div>
                <div id="regformnote">
                    <span>Note: By clicking "Enquire" you are giving approval for Regus to contact you with information about their service. When the form is submitted you will be taken to the next step in the incorporation process.</span>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
    <div class="topskip">
        <div class="skipright"><a href="{$this->router->link("JourneyControler::JOURNEY_PAGE")}">Skip and Continue</a></span> 
        </div>
    </div>
    {$form->getEnd() nofilter}

    {include file="@footer.tpl"}