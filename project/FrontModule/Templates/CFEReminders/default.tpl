{include file="@headerPopUp.tpl"}
    {if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
    <div style="background-color: #cce5ff; border: 1px solid #cccccc; margin-bottom: 15px; padding: 10px;">
        The eReminder Service will issue emails when the accounts and Confirmation Statement are due for this company. 
        Once your company is formed you will receive an activation email which you must click to enable the service. 
        If you would prefer paper reminders click 'change'.
    </div>
{$form nofilter}

{include file="@footerPopUp.tpl"}


