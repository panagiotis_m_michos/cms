<div id="callback-form-container" class="bg-grey4 btm20">
    <div class="col-xs-12 col-sm-6 col-md-6 btm20">
        <h3 class="top10 pad50 btm10">{$callbackRequest.title}</h3>
        <h4 class="margin0 btm10">Call us:</h4>
        <h1 class="margin0 btm20 font-300 pos-relative">
            <i class="fa fa-phone"></i>
            <span class="lead font-500 margin0 pos-absolute top5">{$callbackRequest.phone}</span>
        </h1>
        {$callbackRequest.description nofilter}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 btm20">
        <p class="lead font-500 top10 pad50 btm10">Request a call:</p>
        {if $callbackMailSent}
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                Your callback request was submitted successfully.
            </div>
        {/if}
        {$formHelper->setTheme($callbackForm, 'cms.html.twig')}
        {$formHelper->start($callbackForm) nofilter}
        {$formHelper->errors($callbackForm) nofilter}
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="form-group">
                    {$formHelper->row($callbackForm['firstName']) nofilter}
                </div>
                <div class="form-group">
                    {$formHelper->row($callbackForm['lastName']) nofilter}
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="form-group">
                    {$formHelper->row($callbackForm['phoneNumber']) nofilter}
                </div>
                <div class="form-group">
                    {$formHelper->row($callbackForm['country']) nofilter}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                    {$formHelper->row($callbackForm['message']) nofilter}
                </div>
                <div class="form-group">
                    {$formHelper->row($callbackForm['save'], ['attr' => ['class' => 'btn-default']]) nofilter}
                </div>
                {$formHelper->end($callbackForm) nofilter}
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
