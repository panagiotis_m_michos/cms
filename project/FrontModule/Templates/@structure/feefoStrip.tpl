<div class="width100 bg-grey8 padcard-mobile">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <h2 class="white top0">{$feefoStrip.title}</h2>
                <p class="lead yellow2">Our customers rate us <span class="display-inline"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span> with {$feefoAverage}% positive service rating </p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-4 text-center">
                <p class="white">{$feefoStrip.iconDescription}</p>
                <a href="http://www.feefo.com/feefo/viewvendor.jsp?logon=www.madesimplegroup.com/companiesmadesimple" onclick="window.open(this.href, 'Feefo', 'width=1000,height=600,scrollbars,resizable'); return false;">
                    <img class="padcard-mobile" src="{$feefoStrip.icon}" alt="" />
                </a>
            </div>
            <div class="col-xs-12 col-md-8">
                {foreach $feefoStrip.reviews as $review}
                    <p class="white btm30">
                        <strong><span class="yellow2">{$review.title}</span> <em>{$review.author}</em></strong><br>{$review.description}
                    </p>
                {/foreach}
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 text-center">
                <p class="white margin0">With <span class="yellow2"><strong>{$feefoReviewCount}</strong></span> reviews and counting, <a class="whitelink" href="http://www.feefo.com/feefo/viewvendor.jsp?logon=www.madesimplegroup.com/companiesmadesimple" onclick="window.open(this.href, 'Feefo', 'width=1000,height=600,scrollbars,resizable'); return false;"><strong>click here</strong></a> to see what our customers say about us.</p>
            </div>
        </div>
    </div>
</div>
