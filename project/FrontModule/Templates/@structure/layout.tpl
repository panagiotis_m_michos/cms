<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        {block head}
            <title>{$seo.title}</title>
            <meta name="description" content="{$seo.description}">
            <meta name="keywords" content="{$seo.keywords}">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="robots" content="{if isset($seo.index) && !$seo.index}noindex{else}all,index,follow{/if}">
            <link rel="shortcut icon" href="{$urlImgs}favicon.ico" type="image/x-icon">
            <link rel="icon" href="{$urlImgs}favicon.ico" type="image/x-icon">
            <meta charset="UTF-8">

            {styles file='webloader_front.neon' section='responsive'}

            {* Google Analytics *}
            <script type="text/javascript">
                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-543198-9']);
                if (document.location.pathname == '/payment/') {
                    _gaq.push(['_trackPageview', '/page1223en.html']);
                } else {
                    _gaq.push(['_trackPageview']);
                }

                (function() {
                    var ga = document.createElement('script');
                    ga.type = 'text/javascript';
                    ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(ga, s);
                })();
            </script>

            {scripts file='webloader_front.neon' section='responsive'}

            {* Helpscout Beacon activation *}
            <script type="text/javascript">
                var helpscout = new CMS.Helpscout();

                helpscout.init('What is the subject of your query?');

                {if isset($customer) && $customer->getId()}
                helpscout.identify("{$customer->getFullName()}", "{$customer->email}", {$customer->getId()});
                {/if}
            </script>
        {/block}
    </head>
    <body>
        {* Google Tag Manager *}
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5RVST5" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>
            (function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({
                    'gtm.start': new Date().getTime(),
                    event: 'gtm.js'
                });
                var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src = '//www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-5RVST5');
        </script>
        {block body}
            {ui name="global_header_bt" data=$gbrandlinks}
            {ui name="header_bt" data=$header}

            {ui name="megamenu_cms_mobile" data=$megamenu}
            {ui name="megamenu_cms_desktop" data=$megamenu}

            {block content}

            {/block}

            {ui name="footer_bt" data=$footer}
            {ui name="global_footer_bt" data=$gbrandlinks}
        {/block}
    </body>
</html>
