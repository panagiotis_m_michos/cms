{include file="@AccountantsInstituteHeader.tpl"}

{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}

{$text nofilter}
<div id="text_content">
    {$form->getBegin() nofilter}
    {if $form->getErrors()|@count gt 0}
        <p class="ff_err_notice">Form has <b> {$form->getErrors()|@count}</b> error(s)</p>
    {/if}
    <fieldset id="fieldset_0">
    <legend>Login Details</legend>
    <table class="ff_table">
    <tbody>
        <tr>
            <th width="20%" align="right" valign="top">{$form->getLabel('email') nofilter}</th>
            <td width="40%" align="left" valign="top">{$form->getControl('email') nofilter}</td>
            <td width="40%" align="left" valign="top" style="color:#FF8135">{$form->getError('email')}</td>
        </tr>
        <tr>
            <th width="20%" align="right" valign="top">{$form->getLabel('password') nofilter}</th>
            <td width="40%" align="left" valign="top">{$form->getControl('password') nofilter}</td>
            <td width="40%" align="left" valign="top" style="color:#FF8135">{$form->getError('password')}</td>
        </tr>
    </tbody>    
    </table>
    </fieldset>
    <fieldset id="fieldset_1">
    <legend>Action</legend>
    <table class="ff_table">
        <tbody>
            <tr>
                <th></th>
                <td>{$form->getControl('login') nofilter}</td>
                <td>
                    <p style="padding-top: 5px; margin-left: 20px;">
                        <a href="{$this->router->link("AccountantsInstituteControler::FORGOTTEN_PASSWORD")}" style="color: #006699">Forgotten Password? Click here.</a>   
                    </p>
                </td>
            </tr>
        </tbody>
    </table>
    </fieldset>    
    {$form->getEnd() nofilter}
</div>
{include file="@AccountantsInstituteFooter.tpl"}