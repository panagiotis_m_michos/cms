{include file="@AccountantsInstituteHeader.tpl"}
    {if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
    
    <div id="text_content">
    {$text nofilter}    
    {$form nofilter}
    </div>
{include file="@AccountantsInstituteFooter.tpl"}