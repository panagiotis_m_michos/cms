{include file="@AccountantsInstituteHeader.tpl"}

{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
<div id="content" class="popup">
    {include file='@blocks/flashMessage.tpl'}
</div>
{$text nofilter}
<div id="text_content">
{$form nofilter}
</div>
{include file="@AccountantsInstituteFooter.tpl"}