{include file="@AccountantsInstituteHeader.tpl"}

    <div id="bluebox">
        <div class="ic-head-blue-left">
            <a href="#TB_inline?height=490&amp;width=600&amp;inlineId=slideShow2" class="thickbox"></a>
        </div>
        <div id="slideShow2" style="display: none;">{$slideShow nofilter}</div>
        <div class="ic-head-blue-right">
            <div class="ic-head-blue-right-text"></div>
            <div style="margin-top: 20px;" class="ic-buttons" >
                <a rel="nofollow" class="red-btn fleft" title="" href="{$this->router->link("951")}" style="margin-right: 10px;">MANAGE COMPANIES</a>
                    <a rel="nofollow" class="blue-btn fright" title="" href="{$this->router->link("1009")}">COMPANY FORMATION</a>
                <div class="clear"></div>
            </div>
           
        </div>
        <div class="clear"></div>
    </div>   

     {*<div id="redbox">
        <div id="left_red_box">
            <div id="left_red_box_text">
                <h2>The <span style="font-size: 20px; font-weight: bold;">only</span> company secretarial management software <br/>accredited by ICAEW</h2>
            </div>
            <div id="left_red_box_button">
                <a class="buttonclickable" href="{$this->router->link("#signup#")}"><span>START</span></a>
            </div>
        </div>
        <div id="right_red_box">
            <a href="#TB_inline?height=490&amp;width=600&amp;inlineId=slideShow2" class="thickbox"></a>
        </div>
        <div id="slideShow2" style="display: none;">{$slideShow nofilter}</div>
    </div> *}

    <div id="content">
        <div id="partners-banner"></div>
        <div id="content_top_part" style="padding: 0">
            <div class="content_top_part-left">
                <div id="content_left_part_top_wholesale" style="margin-top: 10px;">
                <h2>Exclusive ICAEW Member Benefits</h2>
                <ul>
                    <li><span>100 free company credit reports<br />(valued at &pound;150)*<span/></li>
                </ul>
                <h2>Plus</h2>
                <ul>
                    <li><span>Free mobile business apps<span/></li>
                    <li><span>Free sole trader v limited company web app<span/></li>
                    <li><span>Statutory documents templates<span/></li>
                    <li><span>Free support on hand<span/></li>
                </ul>
				<p style="padding-top: 14px;"><em>*To claim your exclusive benefits you simply need to be signed up to the system and have imported at least 5 companies. Then contact us and we'll add the credits.</em></p>
               </div>
                <div class="ip-content_right_part_bottom_wholesale" style="border: 0px solid blue;">
                     <h2>**The <span style="font-size: 20px; font-weight: bold;">only</span> company secretarial software <br/>accredited by ICAEW</h2>
                </div>

            </div>
             
            <div id="content_right_part" style="margin-top:15px;">
                <p id="content_text">Why you should use our company secretarial system:</p>
                <div id="features">
                    <div id="left_features">
                        <div><h2 class="img" id="img1">Free</h2><p>No setup or subscription charges, our management system is free.</p></div>
                        <div><h2 class="img" id="img2">Access anywhere</h2><p>Access from any computer 24/7. No installation required.</p></div>
                        <div><h2 class="img" id="img3">Annual return</h2><p>Prepare and file annual returns electronically.</p></div>
                        <div><h2 class="img" id="img4">Up to date</h2><p>Sync your companies with Companies House to receive up to date information.</p></div>
                    </div>
                    <div id="right_features">
                        <div><h2 class="img" id="img5">Manage centrally</h2><p>Manage all your companies from one place.</p></div>
                        <div><h2 class="img" id="img6">Simple import</h2><p>Easily bulk import all your existing companies.</p></div>
                        <div><h2 class="img" id="img7">Stat filing dates</h2><p>View and sort by statutory filing dates for all your companies.</p></div>
                        <div><h2 class="img" id="img8">Dividends</h2><p>Track dividends and create vouchers.</p></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="content_bottom_part">
            <h2>What our Customers have to say:</h2>
            <div id="content_bottom_part_left">
                <div class="custamer_say">
                  <div class="example-obtuse">
                    <p>We have been using the Made Simple Group to form companies for us for almost a year now and we are very impressed with the wholesale service.  Companies are formed very promptly with the formation documents sent by email to us, which we then forward on to our clients so that they can set up bank accounts etc.  The service is highly efficient and offers great value for money.  We also receive regular newsletters highlighting forthcoming changes (eg, director service addresses) which helps us to keep up to date and to offer a proactive service to our clients.<br><b>Keith Witchell ACA FCCA CTA KRW Accountants</b></p>
                  </div>
                </div>
                <!--
                <div class="custamer_say2">
                    <div class="triangle-obtuse">
                        <p>"Companies Made Simple always provide an efficient and reliable service which is easy to use. They now handle all my Company formation requirements and I can confidently recommend them."</br><b>Maximilian R. Hamilton, Director Courtlands Hotel</b></p>
                    </div>
                </div>
                -->
            </div>
            <div id="content_bottom_part_right">
                <div class="custamer_say2">
                    <div class="triangle-obtuse">
                        <p>"Companies Made Simple always provide an efficient and reliable service which is easy to use. They now handle all my Company formation requirements and I can confidently recommend them."</br><b>Maximilian R. Hamilton, Director Courtlands Hotel</b></p>
                    </div>
                </div>
                <div class="custamer_say">
                    <div class="example-obtuse">
                        <p>1st Contact has made use of Companies Made Simple for well over 5 years and cannot fault their service. The most attractive part to what they offer is the simplicity of using their systems. The fact that everything can be performed online and very user friendly makes you appreciate the service you pay for. The customer service and account management have always been fantastic and should there ever be any problems or questions for CMS we are contacted within the hour with a proactive response.</br><b>Raymond Ridgeway Commercial Manager, 1st Contact</b></p>
                    </div>
                </div>
            </div>
        </div>
        <div id="content_disclaimer">
            <b>Disclaimer:</b> The Institute of Chartered Accountants in England and Wales has accredited this software under the terms of its Accreditation Scheme. Purchasers should ensure that the software, its security features and related support meet their own specific requirements, as the Institute will not be liable for any damage whatsoever, incurred by any person, occasioned by the use of the software, or any related products or services howsoever caused.<br/>
        </div>
    </div>
    <div class="clear"></div>
{* SLIDESHOW *}


{include file="@AccountantsInstituteFooter.tpl"}
