{include file="@header.tpl"}

<div id="maincontent2">

	{* BREADCRUMBS *}
	<p style="margin: 0 0 15px 0; font-size: 11px;"><a href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}">{$companyName}</a> &gt; {$title}</p>
	
{$form->getBegin() nofilter}
<fieldset id="fieldset_0">
    <legend>Data</legend>

    <table class="ff_table">
        <tbody>
            <tr>
                <th>{$form->getLabel('isActive') nofilter}</th>
                <td>{$form->getControl('isActive') nofilter}</td>
                <td>{$form->getError('isActive')}</td>
            </tr>
            <tr>
                <th>{$form->getLabel('email') nofilter}</th>
                <td>{$form->getControl('email') nofilter}</td>
                <td>{$form->getError('active')}</td>
            </tr>
        </tbody>
    </table>
</fieldset>

<fieldset>
    <legend>Info</legend>
    <table class="ff_table">
        <thead>
            <tr>
                <th>&nbsp;</th>
                <th style="text-align: center;">Due</th>
                <th style="text-align: center;">Last Reminder Sent</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Returns</td>
                <td style="text-align: center;">
                    {if is_null($reminder->getReturnsDueDate())}
                        <b>n/a</b>
                    {else}
                        {$reminder->getReturnsDueDate()}
                    {/if}
                </td>
                <td style="text-align: center;">
                    {if is_null($reminder->getReturnsLastSentDate())}
                        <b>n/a</b>
                    {else}
                        {$reminder->getReturnsLastSentDate()}
                    {/if}
                </td>
            </tr>
            <tr>
                <td>Accounts</td>
                <td style="text-align: center;">
                    {if is_null($reminder->getAccountsDueDate())}
                        <b>n/a</b>
                    {else}
                        {$reminder->getAccountsDueDate()}
                    {/if}
                </td>
                <td style="text-align: center;">
                    {if is_null($reminder->getAccountsLastSentDate())}
                        <b>n/a</b>
                    {else}
                        {$reminder->getAccountsLastSentDate()}
                    {/if}
                </td>
            </tr>
            <tr>
                <td colspan="3" style="color: #888888; font-size: 12px; padding-top: 10px;">
                    <span style="color: red;">*</span> Reminders are sent 1 month prior to the due date.
                </td>
            </tr>
        </tbody>
    </table>
</fieldset>

<fieldset id="fieldset_1">
    <legend>Action</legend>

    <table class="ff_table">
        <tbody>
            <tr>
                <td>&nbsp;</td>
                <td>{$form->getControl('save') nofilter}</td>
                <td>&nbsp;</td>
            </tr>
        </tbody>
    </table>
</fieldset>
{$form->getEnd() nofilter}

{*
	{$form nofilter}
*}
	
</div>

{include file="@footer.tpl"}
