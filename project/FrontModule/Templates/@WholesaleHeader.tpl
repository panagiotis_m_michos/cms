<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="cache-control"  content="no-cache" />
        <meta http-equiv="pragma"  content="no-cache" />
        <meta http-equiv="expires"  content="-1" />
        <meta http-equiv="content-language"  content="{$currLng}" />
        <meta name="robots" content="all,index,follow" />
        <meta name="description" content="{$description}" />
        <meta name="keywords" content="{$keywords}" />
        <title>{$seoTitle}</title>

        <link rel="shortcut icon" href="{$urlImgs}favicon.ico" type="image/x-icon" />
        <link rel="icon" href="{$urlImgs}favicon.ico" type="image/x-icon" />

        {styles file='webloader_front.neon' section='common'}

        <script type="text/javascript">var dateTimeNow = "{$dateTimeNow}"</script>
        {scripts file='webloader_front.neon' section='common'}
        
        {if $this->node->getId() == 102 || $this->node->getId() == 1207 || $this->node->getId()== 1310 || $this->node->getId()== 1231 || $this->node->getId()== 1244 || $this->node->getId()== 1278}
            {literal}
                <script type="text/javascript">
                    $(function() {
                        $(".info-z-index").show();
                        $(window).scroll(function() {
                            if ($(window).scrollTop() == 0) {
                                if ($("#clearselect").is(":visible")) {
                                } else {
                                    $(".info-z-index").show();
                                }

                            } else {
                                $(".info-z-index").hide();
                            }
                        });
                    });

                </script>
            {/literal}    
        {/if}

        {literal}
            <script type="text/javascript">
                $(document).ready(function() {
                    $('#cse-search-box input[name="q"]').focus(function() {
                        this.value = '';
                    });

                    $('.home-page-tooltip-icq').each(function() {
                        var self = $(this);
                        self.tooltip({
                            tip: "#tooltip" + $(this).attr('rel'),
                            position: 'center left',
                            offset: [-15, -15],
                            opacity: 1
                        });
                    });

                });
                $(document).ready(function() {
                    $('#cse-search-box input[name="q"]').focus(function() {
                        this.value = '';
                    });

                    $('.home-page-tooltip-right').each(function() {
                        var self = $(this);
                        self.tooltip({
                            tip: "#tooltip" + $(this).attr('rel'),
                            position: 'center right',
                            offset: [-15, 0],
                            opacity: 1
                        });
                    });

                });
            </script>
            <!-- Optimizely code -->
            <script src="//cdn.optimizely.com/js/138942235.js"></script> 
        {/literal}
        {if isset($ecommerce)}
            <script>
                try{ldelim}
                        window.optimizely = window.optimizely || [];
                        window.optimizely.push(['trackEvent',
                            "revenue",
                            "{$basket->getPrice('subTotal')*100}"
                        ]);


                {rdelim} catch (err) {ldelim}{rdelim}
            </script>
        {/if}
        {* ECOMERCE *}
        {if isset($ecommerce)}

            <script type="text/javascript">
                try{ldelim}
                        var _gaq = _gaq || [];
                        _gaq.push(['_setAccount', 'UA-543198-9']);
                        _gaq.push(["_setCustomVar", 1, "user_type", "customer", 1]);
                        _gaq.push(['_trackPageview']);

                        _gaq.push(['_addTrans',
                            "{$basket->orderId}", // order ID - required
                            "Companies Made Simple", // affiliation or store name
                            "{$basket->getPrice('total')}", // total - required
                            "{$basket->getPrice('vat')}", // tax
                            "", // shipping
                            "", // city
                            "", // state or province
                            "" // country
                        ]);

                {foreach from=$basket->getItems() item="item"}
                        _gaq.push(['_addItem',
                            "{$basket->orderId}", // order ID - necessary to associate item with transaction
                            "{$item->getId()}", // SKU/code - required
                            "{$item->getLngTitle()}", // product name
                            "{$item->parentId}", // category or variation
                            "{$item->price}", // unit price - required
                            "{$item->qty}" // quantity - required
                        ]);
                {/foreach}

                        _gaq.push(['_trackTrans']); //submits transaction to the Analytics servers
                {rdelim} catch (err) {ldelim}{rdelim}
                {literal}
                    (function() {
                        var ga = document.createElement('script');
                        ga.type = 'text/javascript';
                        ga.async = true;
                        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                        var s = document.getElementsByTagName('script')[0];
                        s.parentNode.insertBefore(ga, s);
                    })();
                </script>

            {/literal}
        {else}
            {literal}
                <script type="text/javascript">
                    var _gaq = _gaq || [];
                    _gaq.push(['_setAccount', 'UA-543198-9']);
                    _gaq.push(['_trackPageview']);

                    (function() {
                        var ga = document.createElement('script');
                        ga.type = 'text/javascript';
                        ga.async = true;
                        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                        var s = document.getElementsByTagName('script')[0];
                        s.parentNode.insertBefore(ga, s);
                    })();
                </script>
            {/literal}
        {/if}
        <!-- Analytics code -->
        {literal}
            <script type="text/javascript">
                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-543198-9']);
                _gaq.push(['_trackPageview']);

                (function() {
                    var ga = document.createElement('script');
                    ga.type = 'text/javascript';
                    ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(ga, s);
                })();
            </script>
        {/literal}
        <!-- Start Javascript for Split test 
        <script type="text/javascript">
            var isCustomerLogged = {if isset($customer)}1{else}0{/if};
        </script>
        Stop Javascript for Split test -->
    </head>
    <body>
        <div id="header2014">
            <div id="headwrap">
                <span><a id="msglogo" href="/"></a></span>
                <span id="facebook">
                    <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FMadeSimpleGroup&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;" allowTransparency="true"></iframe>
                </span>
                <span id="googleplus">
                    <div class="g-plusone" data-size="medium" data-href="/"></div>
                </span>
                <div id="contact" style="left:550px;">
                    <span class="number">0207 608 5500</span>
                    <span class="open">Monday - Friday 9am - 5.30pm</span>
                </div>
                                </div>
                                </div>
                                <div id="megamenu" class="container">
                                    <div id="menu1" class="menu_container green_glass full_width">
                                        <div class="mobile_collapser">
                                            <label for="hidden_menu_collapser">
                                                <span class="mobile_menu_icon"></span> Menu
                                            </label>
                                        </div>
                                        <input id="hidden_menu_collapser" type="checkbox">
                                            <ul>
                                                {foreach $menu as $item}
                                                    <li style="width:190px;">
                                                        <a href="{$item->link}">{$item->page->title}</a>
                                                    </li>
                                                {/foreach}
                                            </ul>
                                    </div>
                                </div>
                                <div class="clear"></div>
                                {if $this->node->getId() != 1424}
                                    <div id="wrapper">
                                 {/if}
                                {* LEFT COLUMN *}
{*                                {if !isset($hide) || $hide != 1}
                                    {include file="@blocks/leftColumn.tpl"}
                                {/if}*}

                                {include file='@blocks/flashMessage.tpl'}
