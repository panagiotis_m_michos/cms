{include file="@headerPopUp.tpl"}
{literal}        
<style>
 div.flash {
    width: 300px;
    margin:0px;
}
<style>
{/literal} 
        {if !empty($showAuth)}
            <div id="authFormBlock">
                <fieldset id="fieldset_3">
                    <legend>3D authentication</legend>
                    <a name="authFormBlock" href="#" style="visibility:hidden"></a>
                    <h3 class="mnone pbottom10">Please complete the 3D authentication process below</h3>
                    <iframe src="{$authFormUrl}" width="100%" height="450"></iframe>
                </fieldset>
            </div>
            {/if}
            
            
{include file="@footerPopUp.tpl"}
