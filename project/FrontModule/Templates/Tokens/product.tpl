{include file="@header.tpl"}

{capture name="content"}
	{if $visibleTitle}
    <h1>{$title}</h1>
{/if}
	{$text nofilter}

	{if !empty($products)}
		<table class="grid">
		<col>
		<col width="100">
            {foreach from=$products key="productId" item="product"}
            <tr>
			<th><b>{$product->getLngTitle()}</b></th>
			<td rowspan="2" align="center">
				{if $product->price}
					&pound;{$product->price} <br />
				{else}
					FREE <br />
				{/if}
				<a href="{$this->router->link("TokensControler::TOKEN", "add=$productId")}" onclick="return confirm('Are you sure?')">Buy now</a>
                </td>
            </tr>
		<tr>
			<td>{$product->getLngText()|cms}</td>
		</tr>
            {/foreach}
    </table>
	{/if}
{/capture}

{include file='@blocks/defaultTemplateBlock.tpl' content=$smarty.capture.content}
{include file='@blocks/rightColumn.tpl'}
{include file="@footer.tpl"}