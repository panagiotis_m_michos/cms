{include file="@headerPopUp.tpl"}


 <div style="width: 420px;margin:0px auto;">
    <div>
        <div style="float:right; ">
             <img src="{$urlImgs}sagepay-logo.png">
        </div>
        
    </div>
    <div class="clear"></div>
        {if $visibleTitle}
            <h1 style="text-align:center;  margin-top:30px; color: #000000; font-size:36px;">{$title}</h1>
        {/if}
        <div id="contentpop">
       <p class="middleSilver" style="font-size:20px;"> Order id: {$orderid}</p>
    </div>
    <div id="footerpop">
        {if isset($continueLink)}
            <p class="middleSilver" style="font-size:28px; padding-top: 25px;"><a target="_parent" href="{$continueLink}" onclick="_gaq.push(['_trackEvent', 'Links', 'Click', 'Continue Registration Process']);">Continue Registration Process</a></p>
        {/if}
    </div>
</div>

{include file="@footerPopUp.tpl"}
