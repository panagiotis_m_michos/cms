{include file="@header.tpl"}
<div id="maincontent1">
	<div class="box-white2">
		<div class="box-white2-top">
			<div class="box-white2-bottom">
                
{if $token != NULL && !isset($form)}
<h1>Edit or Delete a Payment Method</h1>
    <table class="grid">
        <col width="200">
        <col width="200">
        <tr>
            <td>Card Ending:</td>
            <td>{$token->cardNumber}</td>
            <td rowspan="9" align="center">
<!--                <a class="ui-widget-content ui-corner-all buttons" style="padding:3px 6px;"  href="{$this->router->link("TokensControler::TOKEN_DETAILS", "update=1")}">Update</a>-->
                
                <a class="ui-widget-content ui-corner-all buttons"  style="padding:3px 6px;"  href="{$this->router->link("TokensControler::TOKEN_DETAILS","delete=1")}" onclick="return confirm('Are you sure?')">Delete</a>
            </td>
        </tr>
        <tr>
            <td>First Name:</td>
            <td>{$token->firstname}</td>
        </tr>
        <tr>
            <td>Surname:</td>
            <td>{$token->surname}</td>
        </tr>
        <tr>
            <td>Address1:</td>
            <td>{$token->address1}</td>
        </tr>
        {if !empty($token->address2)}
        <tr>
            <td>Address2:</td>
            <td>{$token->address2}</td>
        </tr>
        {/if}
        <tr>
            <td>City:</td>
            <td>{$token->city}</td>
        </tr>
        <tr>
            <td>PostCode:</td>
            <td>{$token->postCode}</td>
        </tr>
        <tr>
            <td>Country:</td>
            <td>{$token->country}</td>
        </tr>
        {if !empty($token->state)}
        <tr>
            <td>State:</td>
            <td>{$token->state}</td>
        </tr>
        {/if}
    </table>
{else}
    <h1>Token</h1>
    <p class="ff_err_notice ff_red_err" style="width: 540px">No token saved on your account.</p>
    {/if}
			</div>
		</div>
	</div>
</div>

{include file='@blocks/rightColumn.tpl'}

            
            
{include file="@footer.tpl"}

