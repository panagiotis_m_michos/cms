<!DOCTYPE html>
<html>
    <head>
    <title>Chat - MadeSimple</title>
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
    {literal}
        <style type="text/css">
            body {margin:0; padding:0; text-align:center; color:#4d4d4d; font-family:Helvetica, Arial, sans-serif;}
            #header {background-color:#eeebe9; padding:40px 10px;}
            #main h1 {font-size: 20px;}
            #main p {font-size: 14px;}
        </style>
    {/literal}
    {literal}
        <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-543198-9']);
        _gaq.push(['_trackPageview']);    

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
        </script>
    {/literal}
    </head>
    <body>
        <div id="header">
            <img src="{$urlImgs}cms-chat.png" alt="MadeSimple">
        </div>
        <div id="main">
            <h1>{$title}</h1>
            {$text nofilter}
        </div>
    </body>
</html>