{include file="@header.tpl"}

{literal}
    <style type="text/css">
    <!--
        div#maincontent2 {
            float: left;
            width: 730px;
            padding-left:10px;
        }
    -->
    
    .sage-disabled { display: none;}
    </style>
{/literal}

{* RIGHT COLUMN *}
<div style=" padding-top:40px; float: right; width:230px;">
	<div style="font-size: 12px; padding: 8px; color: #666666;">
		<img style="float: left;" alt="lock" width="29" height="40" src="{$urlImgs}img8.gif">
		<p style="height: 40px; line-height: 40px; margin-left: 5px; margin-left: 29px; padding-left: 10px; font-weight: bold; font-size: 14px;">You're safe with us!</p>
		<p style="padding-top: 16px;">Every payment is secured by industry standard 256-bit encryption. Companies Made Simple never stores your card details on our servers.</p>
		<img style="margin-top: 16px;" alt="secured by sagepay" width="150" height="52" src="{$urlImgs}img10.gif">
	</div>
	<script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=www.companiesmadesimple.com&amp;size=L&amp;use_flash=YES&amp;use_transparent=YES&amp;lang=en"></script>
	<!--
	<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="https://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0" id="s_l" width="130" height="88" align="">
		<param name="movie" value="https://seal.verisign.com/getseal?at=1&amp;sealid=0&amp;dn=www.companiesmadesimple.com&amp;lang=en">
		<param name="loop" value="false">
		<param name="menu" value="false">
		<param name="quality" value="best">
		<param name="wmode" value="transparent">
		<param name="allowScriptAccess" value="always">
		<embed src="https://seal.verisign.com/getseal?at=1&amp;sealid=0&amp;dn=www.companiesmadesimple.com&amp;lang=en" loop="false" menu="false" quality="best" wmode="transparent" swliveconnect="FALSE" width="130" height="88" name="s_l" align="" type="application/x-shockwave-flash" pluginspage="https://www.macromedia.com/go/getflashplayer" allowscriptaccess="always">
	</object>
	<br>
	<a href="http://www.verisign.com/ssl-certificate/" target="_blank" style="color:#000000; text-decoration:none; font:bold 7px verdana,sans-serif; letter-spacing:.5px; text-align:center; margin:0px; padding:0px;">ABOUT SSL CERTIFICATES</a>
	-->
	<div style="margin-top: 16px;">
		<a href="https://www.securitymetrics.com/site_certificate.adp?s=94%2e236%2e66%2e34&amp;i=374287" target="_blank">
		<img src="https://www.securitymetrics.com/images/sm_ccsafe_check1.gif" alt="SecurityMetrics for PCI Compliance, QSA, IDS, Penetration Testing, Forensics, and Vulnerability Assessment" border="0"></a>
	</div>
</div>

<div id="maincontent2">
    
	{literal}
	<script type="text/javascript">
	$(document).ready(function(){
		toggleFields();
		$("#credit").click(function (){
			toggleFields();
		});
      		
		function toggleFields() {
			disabled = $("#credit").is(":checked"); 
			$("form input[type='text'], form select").each(function (){
				$(this).attr("disabled", disabled);
			});
			$('#amount').attr("disabled", false);
			$('#paypal').attr("disabled", disabled);
			$('#google').attr("disabled", disabled);
		}
   $('#country').change(function () {	     	
        $('#country option:selected').each(function () {		
            if($(this).text() == "Spain")
            {				
                $('#spaintext').show(400);
            }else{
                $('#spaintext').hide(400);
            }
        });
	});
    {/literal}  
    {ldelim}
        {if $disableSage}
            {literal}
        var xx ='test';
        if ($('.on-account-payment').is(":checked") ) {
            $(".submit-on-account").show();
            $(".sage-disabled-paypal").hide();
        } else {
            $(".submit-on-account").hide();
            $(".sage-disabled-paypal").show();    
        }
        $('.on-account-payment').click(function(){
            if ($(this).is(":checked")) {
                $(".submit-on-account").show();
                $(".sage-disabled-paypal").hide();
            }
            else
            {
                $(".submit-on-account").hide();
                $(".sage-disabled-paypal").show();     
            }
        });
        {/literal}    
            
        {/if}    
    {rdelim} 
    {literal}    
    });
	</script>
	{/literal}
    {if $enableMessages}
        <div class="info" style="margin-bottom: 15px;">{$paymentPageMessages nofilter}</div>
    {/if}   
    
	{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}

	
	{*<div class="flash info2" style="margin-left: 0; width: 700px;">
        <strong>Paying with Paypal</strong>. NB. You do not need an account to use Paypal, once you've agreed to our Ts & Cs and clicked 'Paypal' just click the 'Don't have a Paypal account?' link on the Paypal page.
    </div>*}
    
	
	{* PAYMENT ERROR *}
	{if isset($error)}<p>{$error}</p>{/if}
		
    {$form->getBegin() nofilter}
    {if $form->getErrors()|@count gt 0}
        <p class="ff_err_notice ff_red_err" style="width: 710px">Form has <b> {$form->getErrors()|@count}</b> error(s). See below for more details:</p>
    {/if} 
	{if !empty($freeBasket)}
		<fieldset id="fieldset_1">
			<legend>Details</legend>
			<div id="freeOrder">
				<h3>Your order today is free, please tick the terms and conditions and click submit to confirm your order.</h3>
			</div>
		</fieldset>
	{else}
		{if isset($useCredit)}
           <fieldset id="fieldset_0" >
			<legend>On Account</legend>
			<table class="ff_table">
				<tbody>
					<tr>
						<th>Available credit</th>
						<td>&pound;{$customer->credit}</td>  
					</tr>
					<tr>
						<th>{$form->getLabel('credit') nofilter}</th>
						<td>{$form->getControl('credit') nofilter}</td>  
						<td><span class="redmsg">{$form->getError('credit')}</span></td>                  
					</tr>
				</tbody>
			</table>
		</fieldset>
		{/if}
        
        
		<fieldset id="fieldset_1">
			<legend>Details</legend>
			<table class="ff_table">
				<tbody>
					<tr>
						<th>{$form->getLabel('amount') nofilter}</th>
						<td>{$form->getControl('amount') nofilter}</td>
						<td></td>
					</tr>
                    {if !$disableSage}
					<tr>
						<th>{$form->getLabel('cardholder') nofilter}</th>
						<td>{$form->getControl('cardholder') nofilter}</td>
						<td><span class="redmsg">{$form->getError('cardholder')}</span></td>
					</tr>
					<tr>
						<th>{$form->getLabel('cardType') nofilter}</th>
						<td>{$form->getControl('cardType') nofilter}</td>
						<td><span class="redmsg">{$form->getError('cardType')}</span></td>
					</tr>
					<tr>
						<th>{$form->getLabel('cardNumber') nofilter}</th>
						<td>{$form->getControl('cardNumber') nofilter}</td>
						<td><span class="redmsg">{$form->getError('cardNumber')}</span></td>
					</tr>
                    <noscript>
					<tr>
						<th>{$form->getLabel('issueNumber') nofilter}</th>
						<td>{$form->getControl('issueNumber') nofilter}</td>
						<td>The issue number MUST be entered EXACTLY as it appears on the card. e.g. some cards have Issue Number "4", others have "04".</td>
					</tr>
					<tr>
						<th>{$form->getLabel('validFrom') nofilter}</th>
						<td>{$form->getControl('validFrom') nofilter}</td>
						<td><span class="redmsg">{$form->getError('validFrom')}</span></td>
					</tr>
					</noscript>                       
					<tr class="issuered" style="display:none;">
						<th>{$form->getLabel('issueNumber') nofilter}</th>
						<td>
							{$form->getControl('issueNumber') nofilter}
							<div class="help-button2" style="float: right; margin: 0 15px 0 0;">
								<a href="#" class="help-icon">help</a>
								<em>The issue number MUST be entered EXACTLY as it appears on the card. e.g. some cards have Issue Number "4", others have "04".</em>
							</div>
						</td>
						<td><span class="redmsg">{$form->getError('issueNumber')}</span></td>
					</tr>
					<tr class="issuered" style="display:none;">
						<th>{$form->getLabel('validFrom') nofilter}</th>
						<td>{$form->getControl('validFrom') nofilter}</td>
						<td><span class="redmsg">{$form->getError('validFrom')}</span></td>
					</tr>                
					<tr>
						<th>{$form->getLabel('expiryDate') nofilter}</th>
						<td>{$form->getControl('expiryDate') nofilter}</td>
						<td><span class="redmsg">{$form->getError('expiryDate')}</span></td>
					</tr>
					<tr>
						<th>{$form->getLabel('CV2') nofilter}</th>
						<td>
							{$form->getControl('CV2') nofilter}
							<div class="help-button2" style="float: right; margin: 0 15px 0 0;">
								<a href="#" class="help-icon">help</a>
								<em>The security code is a three-digit code printed on the back of your card.
								<img src="{$urlImgs}CVC2SampleVisaNew.png" />
								</em>
							</div>
						</td>
						<td><span class="redmsg">{$form->getError('CV2')}</span></td>
					</tr>
					<tr>
						<th>{$form->getLabel('address1') nofilter}</th>
						<td>{$form->getControl('address1') nofilter}</td>
						<td><span class="redmsg">{$form->getError('address1')}</span></td>
					</tr>
					<tr>
						<th>{$form->getLabel('address2') nofilter}</th>
						<td>{$form->getControl('address2') nofilter}</td>
						<td><span class="redmsg">{$form->getError('address2')}</span></td>
					</tr>
					<tr>
						<th>{$form->getLabel('address3') nofilter}</th>
						<td>{$form->getControl('address3') nofilter}</td>
						<td><span class="redmsg">{$form->getError('address3')}</span></td>
					</tr>
					<tr>
						<th>{$form->getLabel('town') nofilter}</th>
						<td>{$form->getControl('town') nofilter}</td>
						<td><span class="redmsg">{$form->getError('town')}</span></td>
					</tr>
					<tr>
						<th>{$form->getLabel('postcode') nofilter}</th>
						<td>{$form->getControl('postcode') nofilter}</td>
						<td><span class="redmsg">{$form->getError('postcode')}</span></td>
					</tr>                
					<tr>
						<th>{$form->getLabel('country') nofilter}</th>
						<td>{$form->getControl('country') nofilter}</td>
						<td><span class="redmsg">{$form->getError('country')}</span></td>
					</tr>
                    <noscript>
					<tr>
						<th>{$form->getLabel('billingState') nofilter}</th>
						<td>{$form->getControl('billingState') nofilter}</td>
						<td><span class="redmsg">{$form->getError('billingState')}</span></td>
					</tr>
					</noscript>
					<tr class="state" style="display:none;">
						<th>{$form->getLabel('billingState') nofilter}</th>
						<td>{$form->getControl('billingState') nofilter}</td>
						<td><span class="redmsg">{$form->getError('billingState')}</span></td>
					</tr>
                    {/if}
				</tbody>
			</table>
            <div id="spaintext" style="border: 1px solid #069;padding: 10px;background-color: #cce5ff;line-height: 18px; display: none;">
                Our main payment provider, Sagepay, is currently experiencing issues with some Spanish cards. If you have
                trouble with your payment we suggest selecting <strong>Paypal</strong>, from the Payment Method, above.
            </div>
        </fieldset>
                    
	{/if}

	<fieldset id="fieldset_2">
        <legend>Terms and Conditions</legend>
        <table class="ff_table">
            <tbody>
                <tr>
                    <th><a href="/general-terms-conditions.html" target="_blank">Terms and Conditions</a>
                    <br /><span style="font-size: 11px; color: #333333;">(Opens in new window)</span></th>
                    <td>{$form->getControl('terms') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('terms')}</span></td>
                </tr>               
            </tbody>
        </table>
    </fieldset>

    {if !empty($showAuth)}
    <div id="authFormBlock">
        <fieldset id="fieldset_3">
            <legend>3D authentication</legend>
            <a name="authFormBlock" href="#" style="visibility:hidden"></a>
            <h3 class="mnone pbottom10">Please complete the 3D authentication process below</h3>
            <iframe src="{$authFormUrl}" width="100%" height="450"></iframe>
         </fieldset>
    </div>
    {/if}
    
    <div id="submitBlock" {if !empty($showAuth)} style="display:none" {/if}>
    
    <fieldset id="fieldset_3" class="submit-on-account {if $disableSage} submit-on-account{/if}">
        <legend>Action</legend>
        <table class="ff_table">
            <tbody>
                <tr>
                    <th>&nbsp;</th>
                    <td>{$form->getControl('submit') nofilter}</td>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </fieldset> 
    
	{if empty($freeBasket)}
    <fieldset id="fieldset_4" {if $disableSage} class="sage-disabled-paypal"{/if} >
        <legend>Pay using</legend>
        <table class="ff_table">
            <tbody>
                <tr>
                    <th>&nbsp;</th>
                    <td>{$form->getControl('paypal') nofilter}</td>
                    <td>&nbsp;</td>
                </tr>
                {*
                <tr>
                	<th>&nbsp;</th>
                	<td>{$form->getControl('google') nofilter}</td>
                	<td>&nbsp;</td>
                </tr>
                *}
            </tbody>
        </table>        
    </fieldset>
	{/if}
    </div>

    {$form->getEnd() nofilter}	
		
	<p class="required"><span class="orange">*</span>Required Fields</p>

</div>

		
{include file="@footer.tpl"}
