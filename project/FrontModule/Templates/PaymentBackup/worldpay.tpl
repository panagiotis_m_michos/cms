{include file="@header.tpl"}

{literal}
<script type="text/javascript">
$(document).ready(function(){
	toggleFields();
	$("#credit").click(function (){
		toggleFields();
	});
	
	function toggleFields() {
		disabled = $("#credit").is(":checked"); 
		$("form input[type='text'], form select").each(function (){
			$(this).attr("disabled", disabled);
		});
		$('#amount').attr("disabled", false);
		$('#paypal').attr("disabled", disabled);
		$('#google').attr("disabled", disabled);
	}
	
});
</script>
{/literal}



{capture name="content"}
	{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
	{* PAYMENT ERROR *}
	{if isset($error)}<p>{$error}</p>{/if}
	{$form nofilter}
	<p class="required"><span class="orange">*</span>Required Fields</p>
{/capture}
			
{include file='@blocks/defaultTemplateBlock.tpl' content=$smarty.capture.content}
{include file='@blocks/rightColumn.tpl'}
{include file="@footer.tpl"}