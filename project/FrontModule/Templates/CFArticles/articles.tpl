{include file="@header.tpl"}
<div class="formprocesstitle">
    {if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
{*ERRORS*}
{$form->getBegin() nofilter}
{if $form->getErrors()|@count gt 0}
    <p class="ff_err_notice ff_red_err" style="width: 940px">Form has <b> {$form->getErrors()|@count}</b> error(s). See below for more details:</p>
{/if} 

</div>

{* <div class="livesuprt"><script language="javascript" src="https://support.madesimplegroup.com/visitor/index.php?_m=livesupport&_a=htmlcode&departmentid=2"></script></div> *}


{* TABS *}
{include file="@blocks/navlist.tpl" currentTab=$this->action}

{literal}
<script type="text/javascript">
    /* <![CDATA[ */
    $(document).ready(function () {
	
        toggleCustom();
	
        $("input[name='type']").click(function () {
            toggleCustom();
        });
	
        function toggleCustom() {
            disabled = $("#type1").is(":checked");
            $("#custom").attr("disabled", disabled);
            $("#removeMaa").attr("disabled", disabled);
        }
	
    });
    /* ]]> */
</script>
{/literal}

<div>
    <div style="border:0px solid blue;float: left">
        <div style="width: 550px;">
            {$form->getBegin() nofilter}

            {if $type != 'LLP'}
            <fieldset>
                <legend>Memorandum &amp; Articles</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <th>{$form->getLabel('type') nofilter}</th>
                            <td>{$form->getControl('type') nofilter}</td>
                            <td>
                                <div class="help-button">
                                    <a href="#" class="help-icon">help</a>
                                    <em>
                                        - Our standard articles are the 2006 Model Articles adopted from Companies House. Please leave this option selected should you not have your own custom Articles.
                                        <br />
                                        <br />
                                        - Use 'Upload Custom Articles' should you have your own Articles that you wish to use instead of ours.
                                    </em>
                                </div>
                                <span class="redmsg">{$form->getError('type')}</span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>


            {if isset($article)}
            <fieldset>
                <legend>Custom Memorandum &amp; Articles</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <th>{$form->getLabel('articleName') nofilter}</th>
                            <td>{assign var="articleId" value=$article.id}<a href="{$this->router->link(null, "document_id=$articleId")}">{$article.filename}</a></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$form->getControl('removeArticle') nofilter}</td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>
            {else}
            <fieldset>
                <legend>Custom Memorandum &amp; Articles</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <th>{$form->getLabel('custom') nofilter}</th>
                            <td>{$form->getControl('custom') nofilter}<br /><i>(Must be in PDF)</i></td>
                            <td>
                                <div class="help-button">
                                    <a href="#" class="help-icon">help</a>
                                    <em>This is where your own custom Articles are displayed. Your custom Articles will be shown here if you have uploaded them correctly.</em>
                                </div>
                                <span class="redmsg">{$form->getError('custom')}</span>
                            </td>
                        </tr>
                        <tr>
                             <td colspan="3"><i>Please try to reduce the number of pages in your M&A to approx. 13. You may need to reduce the margins and font size.</i></td>
                            
                        </tr>
                    </tbody>
                </table>
            </fieldset>
            {/if}
            {/if}

            {if isset($support)}
            <fieldset>
                <legend>Supporting documents</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <th>{$form->getLabel('supportName') nofilter}</th>
                            <td>{assign var="supportId" value=$support.id}<a href="{$this->router->link(null, "document_id=$supportId")}">{$support.filename}</a></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$form->getControl('removeSupport') nofilter}</td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>
            {else}
            {if !empty($reservedWords)}
            <fieldset>
                <legend>Reserved words</legend>

                <div class="flash error" style="margin: 0 0 10px 0; width: 500px;">
                    <p style="margin: 0 0 10px 0;">We have detected a reserved word in your company name. You will need to provide supporting documentation for the use of this word. Please download the template provided, fill it in with your details and upload it as a PDF below.</p>

				{foreach from=$reservedWords item="reservedWord"}
                    <table class="grid2" width="500">
                        <col>
                        <col width="80">
                        <tr>
                            <th>Reserved Word</th>
                            <th class="center">Action</th>
                        </tr>
                        <tr>
                            <td>{$reservedWord->word}</td>
                            <td class="center"><a href="{$reservedWord->file->getFilePath(true)}">download</a></td>
                        </tr>
                    </table>
				{/foreach}
                </div>
		{/if}
            </fieldset>

            <fieldset>
                <legend>Supporting documents</legend>
                <table class="ff_table" border="00">
                    <tbody>
                        <tr>
                            <th>{$form->getLabel('support') nofilter}</th>
                            <td>{$form->getControl('support') nofilter}<br /><i>(Must be in PDF)</i></td>
                            <td>
                                <div class="help-button">
                                    <a href="#" class="help-icon">help</a>
                                    <em>Supporting documents are needed in some cases for certain reasons like sensitive words in the company name.</em>
                                </div>
                                <span class="redmsg">{$form->getError('support')}</span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>
            {/if}
        </div>

        <div class="box-pink">
            <div class="box-pink-bottom">
                <p class="hugefont noptop">What do you want to do next?</p>

                <table width="100%">
                    <col width="33%">
                    <col width="34%">
                    <col width="33%">
                    <tr>
                        <td><a class="btn_back2 mtop fleft clear" href="{$this->router->link("CFSecretariesControler::SECRETARIES_PAGE", "company_id=$companyId")}">Back</a></td>
                        <td>&nbsp;</td>
                        <td align="right">{$form->getControl('continueprocess') nofilter}</td>
                    </tr>
                </table>
            </div>
        </div>
        {$form->getEnd() nofilter}
    </div>    
    <div style="float: right; width: 285px;">

        {if $companyType == 'BYSHR'}
        <div class="box1 fright">	
            <h3>Guide:</h3>
            <div class="box1-foot pbottom15">
                <p>The Memorandum &amp; Articles define the relationships between the shareholders and the directors of the company. It also defines how the company operates.</p>
                <p>Our <a href="/project/upload/files/ma-shares-model-template.doc">Standard Memorandum &amp; Articles</a> use the Model Articles provided by Companies House and are suitable in most cases.</p>
                <p>You can upload your own custom Memorandum &amp; Articles if you wish. NB. If you upload your own custom M&amp;A you need to upload both the Memorandum &amp; the Articles as 1 document.</p>
                <p>If you need to upload a supporting document you can do so on this page (eg. Sensitive word(s) in the company name).</p>
            </div>
        </div>
        {elseif $companyType == 'LLP'}
        <div class="box1 fright">	
            <h3>Guide:</h3>
            <div class="box1-foot pbottom15">
                <p>Only use this section if your proposed LLP name contains sensitive or restricted words or expressions that require you to seek comments of a government or other specified body.</p>
                <p>NB. Our system attempts to detect if your name includes a sensitive word and if it can't find one it will suggest you skip this step. The majority of company names do not contain sensitive words.</p>
                <p>For a full list you can <a href="http://www.companieshouse.gov.uk/about/gbhtml/gp1.shtml#appA">click here</a></p>
            </div>
        </div>
        {elseif $companyType == 'BYGUAR'}
        <div class="box1 fright">	
            <h3>Guide:</h3>
            <div class="box1-foot pbottom15">
                <p>The Memorandum &amp; Articles define the relationships between the shareholders and the directors of the company. It also defines how the company operates.</p>
                <p>Our <a href="/project/upload/files/ma-guarantee-model-template.doc">Standard Memorandum &amp; Articles</a>  use the Model Articles provided by Companies House and are suitable in most cases.</p>
                <p>You can upload your own custom Memorandum &amp; Articles if you wish. NB. If you upload your own custom M&amp;A you need to upload both the Memorandum &amp; the Articles as 1 document.</p>
                <p>If you need to upload a supporting document you can do so on this page (eg. Sensitive word(s) in the company name).</p>
                <p><strong>Non-profit distribution clause</strong>
                    <br />In some cases Companies House may require a non-profit distribution clause in the articles. 
                    If you need this you can <a href="/project/upload/files/ma-guarantee-nonprofit-distribution-objects-template.doc">download our M&amp;A template</a> which includes this clause. NB. 
                    You will need to complete the sections marked in red on the last 2 pages of the document and then upload it as 'Custom'.</p>
            </div>
        </div>
        
        {/if}
        <div class="clear"></div>

        {* HTML INFO BOX *}
        {include file="@blocks/CFHtmlBox.tpl"}
    </div>
    <div class="clear"></div>
</div>

{include file="@footer.tpl"}
