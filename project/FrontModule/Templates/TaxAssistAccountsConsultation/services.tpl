{include file="@header.tpl"}

{capture name="content"}

	{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
<div style="float: right; margin-left: 20px; width: 400px;">
	<p style="text-align: center; padding-top:0;"><img width="300" height="74" alt="tax assist free accountancy consultation" src="/project/upload/imgs/img41.gif"></p>
	<p style="margin-bottom: 10px; font-weight: bold; text-align: center; font-size: 16px;">Interested in a free consultation?<br><span style="color: #666;">Please fill out the form below:</span></p>
	{* FORM *}	
	{$form nofilter}
</div>   
    	{$text nofilter}
{/capture}


{include file='@blocks/defaultTemplateBlock.tpl' content=$smarty.capture.content}
{include file='@blocks/rightColumn.tpl'}
{include file="@footer.tpl"}