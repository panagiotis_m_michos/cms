{include file="@header.tpl"}

<div id="maincontent-full">

    {include file="@blocks/breadCrumbs.tpl"}
    <h1>{$title}</h1>

    {$form nofilter}

</div>

{include file="@footer.tpl"}