{include file="@header.tpl"}

<!--<style>
    div.datagrid ul.top-action {
        position: absolute;
        top: 35px;
        left: 280px;
    }
</style>-->

<div id="maincontent-full" style="position: relative;">

    {include file="@blocks/breadCrumbs.tpl"}
    <h1>{$title}</h1>

    {$datagrid nofilter}

    <div>

        <h4>Notes</h4>
        <ul class="midfont lheight20">
            <li>Shareholder information is manually inserted by you.</li>
            <li>This page is for record keeping only, it is not linked in any way with Companies House. The only way to notify Companies House of a shareholder update is to file an Annual Return.</li>
            <li>See <a href="/project/blog/adding-new-shares-and-shareholders-to-a-uk-limited-company/">adding new shares and shareholders</a> for more information.</li>
        </ul>

    </div>

</div>

{include file="@footer.tpl"}
