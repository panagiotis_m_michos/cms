{include file="@header.tpl"}

    <div style="border:0px solid blue;float: left">
         <div style="width: 650px; margin-left: 20px; padding-bottom: 10px;">
        <p style="margin-bottom: 20px; font-size: 11px;">
            <a href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}">{$companyName}</a> &gt; {$title}
        </p>
        {if $visibleTitle}
        <h1>{$title}</h1>
        {/if}
        <div style="background-color: #cce5ff; border: 1px solid #9cccfe; margin-bottom: 15px; padding: 10px;">
            The eReminder Service will issue emails when the accounts and Confirmation Statement are due for this company. These emails will replace the paper reminder letters.
        </div>
        <div style="float: right; padding-bottom: 5px;">
            <a class="ui-widget-content ui-corner-all" style="padding: 3px;" href="{$this->router->link("CUEReminderControler::EREMINDER_PAGE", "company_id=$companyId")}">Refresh List</a>
            <a class="ui-widget-content ui-corner-all" style="padding: 3px;" href="{$this->router->link("CUEReminderControler::EREMINDER_PAGE", "company_id=$companyId","deleteAll=1")}" onclick="return confirm('Are you sure');">Delete All</a>
        </div>
        <div class="clear"></div>
        <div style="padding: 10px 0px 18px;">
            <table class="grid2" width="650">
                <tr>
                    <th class="left"><b>Current list of eReminder recipients</b></th>
                    <th class="left"><b>Status</b></th>
                    <th class="left"><b>Action</b></th>
                </tr>
				{assign var='counter' value=0}
                {if isset($list[0])}
                    {foreach from=$list key="key" item="recipient"}
                    {assign var=$counter value=$counter++}  
                    {assign var='email' value=$recipient->EmailAddress}
                    {assign var='activated' value=$recipient->Activated}
                    <tr>
                        <td>{$email}</td>
                        <td>
                            {if $activated == 'true'}
                            Active
                            {else}
                            Awaiting Activation
                            {/if}
                        </td>
                        <td><a href="{$this->router->link("CUEReminderControler::EREMINDER_PAGE", "company_id=$companyId", "emailToDelete=$email")}">delete</a></td>
                    </tr>
                    {/foreach}
                {else}
                    <tr>
                        <td colspan="3">eReminders not currently set up</td>
                    </tr>
                {/if}
                </table>
            </div>
            {if $counter < 4}
            {$formAdd->getBegin() nofilter}
                <fieldset>
                    <legend>Add an email address</legend>
                    <table class="ff_table">
                        <tbody>
                            <tr>
                                <th>{$formAdd->getLabel('email') nofilter}</th>
                                <td>{$formAdd->getControl('email') nofilter}</td>
                                <td>{$formAdd->getControl('submit') nofilter}</td>
                            </tr>
                            <tr>
                                <td><span class="redmsg">{$formAdd->getError('email')}</span></td>
                            </tr>
                        </tbody>
                    </table>
                </fieldset>       
            {$formAdd->getEnd() nofilter}
            {/if}
        
        <h4>eReminder FAQs</h4>
        
        <p><b>Are there a maximum number of email addresses I can add?</b></p>
        <p style="padding-top: 2px">Yes, there is a maximum of 4 email recipients.</p>
        
         <p><b>I’ve added 4 email recipients, how do I change one?</b></p>
         <p style="padding-top: 2px">Simply delete an email address and add another.</p>
        
         <p><b>How do I unsubscribe from the service?</b></p>
         <p style="padding-top: 2px">Delete your email from the list (or click ‘unsubscribe’ within the email) or ‘Delete All’ to remove the eReminder service completely.</p>
        
         <p><b>If I delete the eReminder service completely will I still be reminded?</b></p>
         <p style="padding-top: 2px">Yes, Companies House will revert to sending paper reminder letters.</p>

         <p><b>Where are email reminders sent from?</b></p>
         <p style="padding-top: 2px">All emails are sent direct from Companies House.</p>

         <p><b>How do I ensure that my eReminders are not treated as spam?</b></p>
         <p style="padding-top: 2px">The email address from which Companies House will send the eReminders will end with “@companies-house.gov.uk”. You should ensure that such emails will not be removed by your spam filter.</p>
        
         <p><b>What are the activation email details?</b></p>
         <p style="padding-top: 2px">The activation email is sent FROM: ereminders@companies-house.gov.uk with SUBJECT: Co No. 01234567 - Companies House eReminder Service – Activation</p>
        
        </div>
    </div>    
    <div style="float: right; width: 250px;">
        <div class="box1 fright" style="margin-bottom: 10px; margin-top: 85px;">	
            <h3>How It Works</h3>
            <div class="box1-foot pbottom15">
                <p><b> 1. Add email addresses</b></p>
                <p> Enter up to 4 email addresses where you wish eReminders to be sent.</p>
                <p> <b> 2. Activate email addresses</b>  </p>
                <p> An email is sent from Companies House to each new email address and the recipient must agree to the Terms of Operation to join the service. Each email address is then activated to receive eReminders.</p>
            </div>
        </div>
    </div>
    <div class="clear"></div>

{include file="@footer.tpl"}



