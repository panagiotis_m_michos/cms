{include file="@header.tpl"}

<div id="maincontent2">
	{* BREADCRUMBS *}
	<p style="margin: 0 0 15px 0; font-size: 11px;"><a href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}">{$companyName}</a> &gt; <a href="{$this->router->link("#dividends#", "company_id=$companyId")}">Dividends</a> &gt; {$title}</p>

	{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
	
	<div style="text-align: right; padding: 0 0 5px 0;"><a href="{$this->router->link(null, "print=1")}">Print Vouchers</a></div>
	
	<table class="grid2" width="780">
	<col>
	<col width="100">
	<col width="90">
	<tr>
		<th>Name</th>
		<th class="center">Number Held</th>
		<th class="center">Action</th>
	</tr>
	{if !empty($shareholders)}
		{foreach from=$shareholders item="shareholder"}
		<tr>
			<td>{$shareholder->getName()}</td>
			<td class="center">{$shareholder->getNumberOfShares()}</td>
{assign var="shareholderId" value=$shareholder->getShareholderId()}
			<td class="center"><a href="{$this->router->link('#shareholder#', "company_id=$companyId", "shareholdersId=$shareholdersId", "shareholderId=$shareholderId")}">Details</a></td>
		</tr>
		{/foreach}
	{else}
		<tr>
			<td colspan="3">No shareholders</td>
		</tr>
	{/if}
	</table>
	
</div>

{include file="@footer.tpl"}
