{include file="@header.tpl"}

<div id="maincontent2">

	{* BREADCRUMBS *}
	<p style="margin: 0 0 15px 0; font-size: 11px;"><a href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}">{$companyName}</a> &gt; <a href="{$this->router->link("#dividends#", "company_id=$companyId")}">Dividends</a> &gt; {$title}</p>

	{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}

	{* FORM *}
	<div id="form">{$form nofilter}</div>
	{* JS FOR PREFILL *}

<script type="text/javascript">
/* <![CDATA[ */
{$jsPrefillShares nofilter}
{$jsPrefillOfficers nofilter}
{$jsPrefillShareholders nofilter}
/* ]]> */
</script>

	<script type="text/javascript">
	<!--
	var ajaxAddShareholderLink = "{$this->router->link(null, 'ajaxAddShareholder=1') nofilter}"
	var ajaxRemoveShareholderLink = "{$this->router->link(null, 'ajaxRemoveShareholder=1') nofilter}"
	var Dividend = new Dividend(ajaxAddShareholderLink, ajaxRemoveShareholderLink)
	//Dividend.toggleShare();
	//Dividend.toggleOfficer();
	//-->
	</script>

	{literal}
	<script type="text/javascript">
	<!--
	/**
	 * When document is ready
	 */
	$(document).ready(function() {
		Dividend.onDocumentReady();

	// prefill shares
	$("#prefilledShareClass").change(function () {
		var value = $(this).val();
		share = shares[value];
		for (var name in share) {
			$('#'+name).val(share[name]);
		}
	});

	// prefill officers
	$("#prefilledOfficer").change(function () {
		var value = $(this).val();
		officer = officers[value];
		for (var name in officer) {
			$('#'+name).val(officer[name]);
		}
	});

    // prefill shareholders

	$(".shareholderSelect").change(function () {
		var value = $(this).val();
        var value2 = $(this).attr("name").split('_')[1];
		shareholder = shareholders[value];
		for (var name2 in shareholder) {
			$('#'+name2+'_'+value2).val(shareholder[name2]);
		}
	});

	});
	//-->
	</script>
	{/literal}
    
</div>

{include file="@footer.tpl"}
