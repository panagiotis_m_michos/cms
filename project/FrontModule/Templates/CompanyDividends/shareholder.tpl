{include file="@header.tpl"}

<div id="maincontent2">

	{* BREADCRUMBS *}
	<p style="margin: 0 0 15px 0; font-size: 11px;"><a href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}">{$companyName}</a> &gt; <a href="{$this->router->link("#dividends#", "company_id=$companyId")}">Dividends</a> &gt; <a href="{$this->router->link("#shareholders#", "company_id=$companyId", "shareholdersId=$shareholdersId")}">Shareholders</a> &gt; {$title}</p>
	
	{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
	
	<div style="text-align: right; padding: 0 0 5px 0;"><a href="{$this->router->link(null, "print=1")}">Print Voucher</a></div>
	
	<table class="grid2" width="780">
	<col width="150">
	<col>
	<tr>
		<th>Name</th>
		<td>{$shareholder->getName()}</td>
	</tr>
	<tr>
		<th>Number Held:</th>
		<td>{$shareholder->getNumberOfShares()}</td>
	</tr>
	<tr>
		<th colspan="2" style="text-align: center;">Address</th>
	</tr>
	<tr>
		<th>Premise:</th>
		<td>{$shareholder->getPremise()}</td>
	</tr>
	<tr>
		<th>Street:</th>
		<td>{$shareholder->getStreet()}</td>
	</tr>
	<tr>
		<th>Thoroughfare:</th>
		<td>{$shareholder->getThoroughfare()}</td>
	</tr>
	<tr>
		<th>Post Town:</th>
		<td>{$shareholder->getPostTown()}</td>
	</tr>
	<tr>
		<th>County:</th>
		<td>{$shareholder->getCounty()}</td>
	</tr>
	<tr>
		<th>Country:</th>
		<td>{$shareholder->getCountry()}</td>
	</tr>
	<tr>
		<th>Postcode:</th>
		<td>{$shareholder->getPostcode()}</td>
	</tr>
	</table>
	
</div>

{include file="@footer.tpl"}
