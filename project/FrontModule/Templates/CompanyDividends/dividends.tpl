{include file="@header.tpl"}

<div id="maincontent2" style="float:left; padding: 0 0 10px 5px; width: 960px;">

	{* BREADCRUMBS *}
	<p style="margin: 0 0 15px 0; font-size: 11px;"><a href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}">{$companyName}</a> &gt; {$title}</p>
	
	{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
	
	<div style="text-align: right; padding: 0 0 5px 0;"><a href="{$this->router->link('#addDividend#', "company_id=$companyId")}">Add Dividend</a></div>
	
	<table class="grid2" width="960">
	<col width="85">
	<col width="100">
	<col width="100">
	<col width="75">
	<col width="75">
	<col width="75">
	<col width="75">
	<col width="85">
	<col width="100">
	<tr>
		<th>Acc. period</th>
		<th>Type</th>
		<th>Share Class</th>
		<th>Shares</th>
		<th>Net</th>
		<th>Tax</th>
		<th>Gross</th>
		<th>Rec. Date</th>
		<th class="center" colspan="5">Action</th>
	</tr>
	
	{if !empty($dividends)}
		{foreach from=$dividends item="dividend"}
		<tr>
			<td>{$dividend->getAccountingPeriod()|date_format:"%d-%m-%Y"}</td>
			<td>{$dividend->getType()}</td>
			<td>{$dividend->getShareClass()}</td>
			<td>{$dividend->getNumberOfShares()}</td>
			<td>{$dividend->getNetDividend()}</td>
			<td>{$dividend->getTax()}</td>
			<td>{$dividend->getGrossDividend()}</td>
			<td>{$dividend->getRecordDate()|date_format:"%d-%m-%Y"}</td>
{assign var="shareholdersId" value=$dividend->getDividendId()}
			<td class="center">
                <a href="{$this->router->link('#shareholders#', "company_id=$companyId", "shareholdersId=$shareholdersId")}">View Shareholders</a>
            </td>
			<td class="center">
                <a href="{$this->router->link(null, "company_id=$companyId", "printReport=$shareholdersId")}">Print Report</a>
            </td>
			<td class="center">
                <a href="{$this->router->link(null, "company_id=$companyId", "RtfMinutes=$shareholdersId")}">Print Minutes</a>
            </td>
            <td class="center">
                <a href="{$this->router->link('#addDividend#', "company_id=$companyId","cloneShareholdersId=$shareholdersId")}">Copy</a>
            </td>
			<td class="center">
                <a href="{$this->router->link(null, "company_id=$companyId", "removeShareholdersId=$shareholdersId")}">Remove</a>
            </td>
		</tr>
		{/foreach}
	{else}
		<tr>
			<td colspan="11">No dividends</td>
		</tr>
	{/if}
	</table>
	
</div>

{include file="@footer.tpl"}
