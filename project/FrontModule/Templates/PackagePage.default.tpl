{include file="@header.tpl"}

{capture name="content"}
	{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
	{$text nofilter}
	
	{if !empty($packages)}
		{foreach from=$packages key="productId" item="product"}
				<a href="{$this->router->link(null, "buy=$productId")}" class="btn_more mleft">Buy now (&pound;{$product->price})</a>
		{/foreach}
	{/if}
{/capture}

{include file='@blocks/defaultTemplateBlock.tpl' content=$smarty.capture.content}
{include file='@blocks/rightColumn.tpl'}
{include file="@footer.tpl"}