{include file="@header.tpl"}

{*
{literal}
<script>
$(document).ready(function () {
	$("#num_shares, #aggregate_nom_value").keyup(function (e) {
		var value = $("#num_shares").val() * $("#aggregate_nom_value").val();
		$('#shareCapital').html(value);
	});
});
</script>
{/literal}
*}
<div class="formprocesstitle">
{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
</div>
<div class="livesuprt">
<script language="javascript" src="http://support.madesimplegroup.com/visitor/index.php?_m=livesupport&_a=htmlcode&departmentid=2"></script>
</div>
{* TABS *}
{include file="@blocks/navlist.tpl" currentTab=$this->action}


{* GUIDE *}
<div class="box1 fright">	
		<h3>Guide:</h3>
		<div class="box1-foot pbottom15">
		<p>A company limited by shares requires a share capital with a minimum of one share.</p>
		<p>A standard share capital is usually 1000 shares at &pound;1 each. We have entered these values as default; however, you can change them if you wish.</p>
		<p>A company limited by guarantee will not have a share capital but it does require member/subscriber details similar to the company limited by shares. These details will be entered in the step below.</p>
		<p>You are allowed to choose a share currency besides that of GBP (&pound;) for your company. You have the choice of GBP (&pound;), EUR (&euro;) and USD ($).</p>
		<p>There are many types of share classes but only Ordinary shares are accepted via online applications.</p>
		</div>
</div>


<div style="width: 550px;">

{$form->getBegin() nofilter}
<fieldset>
<legend>Share capital</legend>
<table class="ff_table">
<tbody>
<tr>
	<th>{$form->getLabel('currency') nofilter}</th>
	<td>{$form->getControl('currency') nofilter}</td>
	<td>
        <div class="help-button">
            <a href="#" class="help-icon">help</a>
            <em>This is the currency your shares will be nominated in. Companies House allows for other currencies besides GBP so choose which ever you prefer.</em>
        </div>
        {$form->getError('currency')}
    </td>
</tr>
<tr>
	<th>{$form->getLabel('share_class') nofilter}</th>
	<td>{$form->getControl('share_class') nofilter}</td>
	<td>
        <div class="help-button">
            <a href="#" class="help-icon">help</a>
            <em>This is your share class. There are many types of share classes but only ordinary shares can be done via online applications. </em>
        </div>
        {$form->getError('share_class')}
    </td>
</tr>
{*
<tr>
	<th>{$form->getLabel('num_shares') nofilter}</th>
	<td>{$form->getControl('num_shares') nofilter}</td>
	<td><span class="redmsg">{$form->getError('num_shares')}</span></td>
</tr>
*}
<tr>
	<th>{$form->getLabel('aggregate_nom_value') nofilter}</th>
	<td>{$form->getControl('aggregate_nom_value') nofilter}</td>
	<td><span class="redmsg">{$form->getError('aggregate_nom_value')}</span></td>
</tr>

</tbody>
</table>	
</fieldset>


{*
<p style="border: 1px solid #cccccc; padding: 5px 15px;"><strong style="font-size: 18px;">Share capital: <span id="shareCapital">{$shareCapital}</span></strong></p><br />
*}
</div>


<div class="box-pink">
<div class="box-pink-bottom">
<p class="hugefont noptop">What do you want to do next?</p>

<table width="100%">
<col width="33%">
<col width="34%">
<col width="33%">
<tr>
	<td><a class="btn_back2 mtop fleft clear" href="{$this->router->link("CFRegisteredOfficeControler::REGISTER_OFFICE_PAGE", "company_id=$companyId")}">Back</a></td>
	<td>&nbsp;</td>
	<td align="right">{$form->getControl('continueprocess') nofilter}</td>
</tr>
</table>
</div>
</div>
{$form->getEnd() nofilter}
{include file="@footer.tpl"}