<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="cache-control"  content="no-cache" />
	<meta http-equiv="pragma"  content="no-cache" />
	<meta http-equiv="expires"  content="-1" />
	<meta http-equiv="content-language"  content="en" />
	<meta name="robots" content="all,index,follow" />
	<meta name="googlebot"  content="index,follow,archive" />
	<meta name="author" content="{$author}"/>
	<meta name="copyright" content="{$copyright}"/>
	<meta name="description" content="{$description}" />
	<meta name="keywords" content="{$keywords}" />
	
    <script type="text/javascript" src="{$urlComponent}jquery/dist/jquery.min.js" ></script>
    <script type="text/javascript" src="{$urlComponent}jquery-migrate/jquery-migrate.min.js" ></script>	
	
	<title>{$title}</title>
	
	<style type="text/css">
	<!--
	body {ldelim}
		font-family: Arial, Verdana, sans-serif;
		font-size: 12px;
	{rdelim}
	
	fieldset {ldelim}
		background-color: #{$affiliate->companyColour};
		color: #{$affiliate->textColour};
		padding: 5px 10px 5px 10px;
		margin: 0;
		border: 0;
	{rdelim}
	
	table.grid {ldelim}
		table-layout: fixed;
		border-bottom: 1px solid #{$affiliate->companyColour};
		border-left: 1px solid #{$affiliate->companyColour};
		border-collapse: collapse;
		width: 100%;
		margin: 0 0 0 0px;
	{rdelim}
	
		table.grid th, table.grid td {ldelim}
			border-right: 1px solid #{$affiliate->companyColour};
			border-top: 1px solid #{$affiliate->companyColour};
			padding: 4px 10px 4px 10px;
		{rdelim}
		
		table.grid th {ldelim}
			color: #{$affiliate->textColour};
			font-size: 12px;
			background-color: #{$affiliate->companyColour};
			text-align: left;
		{rdelim}
		
		table.grid td {ldelim}
			text-overflow: ellipsis;
			overflow: hidden;
			white-space: nowrap;
		{rdelim}
		
		table.grid tr.saving_row th, table.grid tr.saving_row td {ldelim}
			font-size: 14px;
			font-weight: bold;
		{rdelim}
		
	div.form_link {ldelim}
		border: 3px solid #{$affiliate->companyColour}; 
		width: 200px; 
		height: 35px; 
		margin-top: 12px; 
		margin-left: auto; 
		margin-right: auto;
	{rdelim}
	
		div.form_link p {ldelim}
			text-align: center; 
			font-weight: bold; 
			padding: 0; 
			margin: 0; 
			line-height: 35px; 
			font-size: 14px; 
			background: #{$affiliate->companyColour};
		{rdelim}
		
			div.form_link p a {ldelim}
				color: #{$affiliate->textColour}; 
				text-decoration: none;
			{rdelim}
			
				div.form_link p a:hover {ldelim}
					color: #000000;
				{rdelim}
	-->
	</style>
	
	{literal}
	<script type="text/javascript">
	<!--
	$(document).ready(function() {
		$('p.form_link').click(function() {
			var url = $('a', this).attr('href');
			parent.document.location = url;
			return false;
		});
	});
	//-->
	</script>
	{/literal}

</head>
<body>



{* === FORM === *}
{$form->getBegin() nofilter}
	<fieldset>
		<table border="0">
		<tr>
			<th>{$form->getLabel('annualProfit') nofilter}</th>
			<td>{$form->getControl('annualProfit') nofilter}</td>
			<td>{$form->getControl('submit') nofilter}</td>
		</tr>
		{if $form->hasErrors()}
			<tr>
				<td></td>
				<td colspan="2">{$form->getError('annualProfit')}</td>
			</tr>
		{/if}
		</table>
	</fieldset>
{$form->getEnd() nofilter}


{* === RESULT === *}
{if isset($submitted)}
	<table class="grid">
	<col>
	<col width="125">
	<tr>
		<td>Annual Profits</td>
		<td>&pound;{$selfEmployed->getAnnualProfit()|number_format:2:".":","}</td>
	</tr>
	<tr>
		<th colspan="2">Self Employed</th>
	</tr>
	<tr>
		<td>Tax Payable</td>
		<td>{$selfEmployed->getTaxPayable()|number_format:2:".":","}</td>
	</tr>
	<tr>
		<td>National Insurance</td>
		<td>{$selfEmployed->getNationalInsurance()|number_format:2:".":","}</td>
	</tr>
	<tr>
		<td>Total Payable</td>
		<td>{$selfEmployed->getTotalPayable()|number_format:2:".":","}</td>
	</tr>
	<tr>
		<td>Net Spendable Personal Income</td>
		<td>{$selfEmployed->getNetSpendablePersonalIncome()|number_format:2:".":","}</td>
	</tr>
	<tr>
		<th colspan="2">Limited Company</th>
	</tr>
	<tr>
		<td>Company Tax Payable</td>
		<td>{$company->getTaxPayable()|number_format:2:".":","}</td>
	</tr>
	<tr>
		<td>Personal Tax Payable</td>
		<td>{$company->getPersonalTaxPayable()|number_format:2:".":","}</td>
	</tr>
	<tr>
		<td>Total Payable</td>
		<td>{$company->getTotalPayable()|number_format:2:".":","}</td>
	</tr>
	<tr>
		<td>Net Spendable Personal Income</td>
		<td>{$company->getNetSpendablePersonalIncome()|number_format:2:".":","}</td>
	</tr>
	<tr class="saving_row">
		<th colspan="2">Saving</th>
	</tr>
	<tr class="saving_row">
		<td>Saved By Registering As A Limited Company</td>
		<td><b>&pound;{$saving|number_format:2:".":","}</b></td>
	</tr>
	</table>
	
	{* LINK TO FORM COMPANY *}
	<div class="form_link">
		<p><a href="/?utm_source={$affiliate->googleAnalyticsTrackingTag}&utm_medium=affiliate-solevltd&utm_campaign=solevltd">Form Your Company Today</a></p>
	</div>
	
	<script type="text/javascript">
	<!--
	$('.form_link a').click(function() {ldelim}
		var url = $(this).attr('href');
		top.location = url;
		return false;
	{rdelim});
	//-->
	</script>
	
{/if}

{* <p style="color: #999999; font-size: 12px;">Powered by Companies Made Simple - The Simplest <a href="/" target="_blank">Company Formation</a> Service</p>*}

</body>
</html>
