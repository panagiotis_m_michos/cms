{include file="@header.tpl"}
<div id="maincontent2">
	<p style="margin: 0 0 15px 0; font-size: 11px;"><a href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}">{$companyName}</a> &gt; {$title}</p>
	{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
	<table class="grid2" width="780">
	<col width="200">
	<tr>
		<th colspan="2" class="center">Person</th>
	</tr>
	<tr>
		<th>Title</th>
		<td>{$shareholder.title}</td>
	</tr>
	<tr>
		<th>First name</th>
		<td>{$shareholder.forename}</td>
	</tr>
	<tr>
		<th>Last name</th>
		<td>{$shareholder.surname}</td>
	</tr>
	<tr>
		<th colspan="2" class="center">Shares</th>
	</tr>
	<tr>
		<th>Share class</th>
		<td>{$shareholder.share_class}</td>
	</tr>
	<tr>
		<th>Number of shares</th>
		<td>{$shareholder.num_shares}</td>
	</tr>
	<tr>
	</table>
    <p></p>
    <div class="ff_err_notice" style="margin-left: 0; width: 755px;">
        <p>Please enter the shareholder's address below and we'll automatically produce their share certificate.</p>
    </div>
    {$form nofilter}
</div>
{include file="@footer.tpl"}