{$form->getBegin() nofilter}

<div class="payment-button" style="float: right;">
    {$form->getControl("payment") nofilter}
</div>

<h1>Order Summary</h1>

{* FORM ERRORS *}
{$form->getHtmlErrorsInfo() nofilter}

<div class="container-msg">
    <div class="left-block">

        {* LINKS PRODUCTS TO COMPANY *}
        {if !$basket->hasPackageIncluded() && !empty($basket->items) && $basket->hasItemsRequiringCompanyNumber()}
            <div class="summary-container">
                <h4 class="txtbold">Link each product to a company</h4>

                {foreach $basket->items as $key => $item}
                    {if $item->requiredCompanyNumber}
                        <div class="summary-service">
                            <h6 class="txtbold">{$item->getLngTitle()}</h6>
                            {$form->getControl($key) nofilter}
                            <span class="ff_control_err">{$form->getError($key) nofilter}</span>
                        </div>
                    {/if}
                {/foreach}
            </div>
        {/if}

        {* Package to Product downgrade *}
        {if $packageToProductDowngradeNotification}
            <div class="flash error" style="margin: 0 0 15px 0; width: 450px;">
                {if $packageToProductDowngradeNotification->isServiceCompanyIncorporated()}
                    {* Incorporated *}
                    <h4>You already have this service</h4>
                    <p>
                        You are trying to buy the service <b>{$packageToProductDowngradeNotification->getProductName()}</b>, but
                        <b>{$packageToProductDowngradeNotification->getServiceCompanyName()}</b>
                        already has the <b>{$packageToProductDowngradeNotification->getServiceName()}</b>
                        that includes this service. Your current subscription will expire on
                        <b>{$packageToProductDowngradeNotification->getServiceExpiresOn()|datetime}</b>.
                    </p>
                    <p>
                        If you want to downgrade your current subscription you will need to purchase the new service once
                        the current subscription expires.
                    </p>
                {else}
                    {* Not incorporated *}
                    <h4>You already have this service</h4>
                    <p>
                        You are trying to buy the service <b>{$packageToProductDowngradeNotification->getProductName()}</b>, but
                        <b>{$packageToProductDowngradeNotification->getServiceCompanyName()}</b> already has the
                        <b>{$packageToProductDowngradeNotification->getServiceName()}</b> that includes this service.
                        Your package will be valid for 12 months once your company is formed.
                        {if !$packageToProductDowngradeNotification->isServiceCompanyIncorporationPending()}
                            You can complete the formation process
                            <a href="{$this->router->link("CFRegisteredOfficeControler::REGISTER_OFFICE_PAGE", "company_id={$packageToProductDowngradeNotification->getServiceCompanyId()}")}" target="_blank">here</a>.
                        {/if}
                    </p>
                {/if}
            </div>
        {/if}

        {* Package to Package downgrade *}
        {if $packageToPackageDowngradeNotification}
            <div class="flash error" style="margin: 0 0 15px 0; width: 450px;">
                {if $packageToPackageDowngradeNotification->isServiceCompanyIncorporated()}
                    {* Incorporated *}
                    <h4>You already have this service</h4>
                    <p>
                        You are trying to buy the <b>{$packageToPackageDowngradeNotification->getPackageName()}</b>, but
                        <b>{$packageToPackageDowngradeNotification->getServiceCompanyName()}</b> already has the
                        <b>{$packageToPackageDowngradeNotification->getServiceName()}</b>. Your current subscription will
                        expire on <b>{$packageToPackageDowngradeNotification->getServiceExpiresOn()|datetime}</b>.
                    </p>
                    <p>
                        If you want to downgrade your current subscription you will need to purchase the new package once
                        the current subscription expires.
                    </p>
                {else}
                    {* Not incorporated *}
                    <h4>You already have this service</h4>
                    <p>
                        You are trying to buy the <b>{$packageToPackageDowngradeNotification->getPackageName()}</b>, but
                        <b>{$packageToPackageDowngradeNotification->getServiceCompanyName()}</b> already has the
                        <b>{$packageToPackageDowngradeNotification->getServiceName()}</b>. Your current package will be
                        valid for 12 months once your company is formed.
                        {if !$packageToPackageDowngradeNotification->isServiceCompanyIncorporationPending()}
                            You can complete the formation process
                            <a href="{$this->router->link("CFRegisteredOfficeControler::REGISTER_OFFICE_PAGE", "company_id={$packageToPackageDowngradeNotification->getServiceCompanyId()}")}" target="_blank">here</a>.
                        {/if}
                    </p>
                {/if}
            </div>
        {/if}

        {* Product to Package upgrade *}
        {if $productToPackageUpgradeNotifications}
            {foreach $productToPackageUpgradeNotifications as $productToPackageUpgradeNotification}
                {if $productToPackageUpgradeNotification->isServiceCompanyIncorporated()}
                    {* Incorporated *}
                    <div class="flash info" style="margin: 0 0 15px 0; width: 450px;">
                        <h4>Attention!</h4>
                        <p>
                            You are trying to buy the package <b>{$productToPackageUpgradeNotification->packageName}</b> which
                            contains the service{if $productToPackageUpgradeNotification->getUpgradedProductsCount() > 1}s:{/if}
                            <br>
                            {foreach $productToPackageUpgradeNotification->getProducts() as $product}
                                - <b>{$product->getLngTitle()}</b><br>
                            {/foreach}
                        </p>
                        <p>
                            <b>{$productToPackageUpgradeNotification->getServicesCompanyName()}</b> already has a subscription
                            with the service{if $productToPackageUpgradeNotification->getUpgradedProductsCount() > 1}s:{/if}
                            <br>
                            {foreach $productToPackageUpgradeNotification->getServices() as $service}
                                - <b>{$service->getServiceName()}</b> which expires on <b>{$service->getDtExpires()|datetime}</b><br>
                            {/foreach}
                        </p>
                        <p>
                            If you purchase this new subscription your current
                            subscription{if $productToPackageUpgradeNotification->getUpgradedProductsCount() > 1}s{/if} will be
                            removed and the new service will start today.
                        </p>
                    </div>
                {else}
                    {* Not incorporated *}
                    <div class="flash info" style="margin: 0 0 15px 0; width: 450px;">
                        <h4>Attention!</h4>
                        <p>
                            You are trying to buy the package <b>{$productToPackageUpgradeNotification->packageName}</b> which
                            contains the service{if $productToPackageUpgradeNotification->getUpgradedProductsCount() > 1}s:{/if}
                            <br>
                            {foreach $productToPackageUpgradeNotification->getProducts() as $product}
                                - <b>{$product->getLngTitle()}</b><br>
                            {/foreach}
                        </p>
                        <p>
                            <b>{$productToPackageUpgradeNotification->getServicesCompanyName()}</b> already has a subscription
                            with the service{if $productToPackageUpgradeNotification->getUpgradedProductsCount() > 1}s:{/if}
                            <br>
                            {foreach $productToPackageUpgradeNotification->getServices() as $service}
                                - <b>{$service->getServiceName()}</b><br>
                            {/foreach}
                        </p>
                        <p>
                            If you purchase this new subscription your current
                            subscription{if $productToPackageUpgradeNotification->getUpgradedProductsCount() > 1}s{/if} will be
                            removed.
                        </p>
                    </div>
                {/if}
            {/foreach}
        {/if}

        {* Package to Package upgrade *}
        {if $packageToPackageUpgradeNotifications}
            {foreach $packageToPackageUpgradeNotifications as $packageToPackageUpgradeNotification}
                {if $packageToPackageUpgradeNotification->isServiceCompanyIncorporated()}
                    {* Incorporated *}
                    <div class="flash info" style="margin: 0 0 15px 0; width: 450px;">
                        <h4>Attention!</h4>
                        <p>
                            You are trying to buy the <b>{$packageToPackageUpgradeNotification->getPackageName()}</b>, but
                            <b>{$packageToPackageUpgradeNotification->getPackageCompanyName()}</b> already has a subscription with the
                            <b>{$packageToPackageUpgradeNotification->getServiceName()}</b>. Your current subscription will expire on
                            <b>{$packageToPackageUpgradeNotification->getServiceExpiresOn()|datetime}</b>.
                        </p>
                        <p>
                            If you purchase this new subscription your current package subscription will be removed.
                        </p>
                    </div>
                {else}
                    {* Not incorporated *}
                    <div class="flash info" style="margin: 0 0 15px 0; width: 450px;">
                        <h4>Attention!</h4>
                        <p>
                            You are trying to buy the <b>{$packageToPackageUpgradeNotification->getPackageName()}</b>, but
                            <b>{$packageToPackageUpgradeNotification->getPackageCompanyName()}</b> already has a subscription with the
                            <b>{$packageToPackageUpgradeNotification->getServiceName()}</b>.
                        </p>
                        <p>
                            If you purchase this new subscription your current package subscription will be removed.
                        </p>
                    </div>
                {/if}
            {/foreach}
        {/if}

        {if $suspendedServiceItem}
            <div class="flash info" style="margin: 0 0 15px 0; width: 450px;">
                <h4>Important!</h4>
                <p>Some of the services you selected have been suspended: <br>
                    • {$suspendedServiceItem->serviceName} to {$suspendedServiceItem->companyName} was suspended on {$suspendedServiceItem->serviceExpiresOn|datetime}</p>
                <p>These services have been suspended and no service activities have occurred during this time.<br>
                    By reactivating these services, these previous activities will not be performed.<br>
                    The services will re-start afresh from today.</p>
            </div>
        {/if}


        {* BASKET SUMMARY *}
        <div class="box">

            <table border="0" cellspacing="0" cellpadding="0" class="formtable mnone mbottom">
                <tr>
                    <th width="280">Product</th>
                    <th width="70">Qty</th>
                    <th width="90">Price</th>
                </tr>
                {foreach from=$basket->items key="key" item="item"}
                    <tr>
                        <td>{$item->getLngTitle()}</td>
                        <td align="center" valign="top">{$item->qty}</td>
                        <td>&pound;{$item->totalPrice}</td>
                    </tr>
                {/foreach}


                <tr class="bb">
                    <td><strong>Subtotal</strong></td>
                    <td>&nbsp;</td>
                    <td><strong>&pound;{$basket->getPrice('subTotal')}</strong></td>
                </tr>
                {* SHOW VOUCHER *}
                {if $basket->voucher}
                    <tr>
                        <td class="voucher">
                            Voucher `{$basket->voucher->name}` <span>{$form->getControl("removeVoucher") nofilter}</span>
                        </td>
                        <td>&nbsp;</td>
                        {assign var="subTotal" value=$basket->getPrice('subTotal')}
                        <td style="color: red;">-&pound;{$basket->voucher->discount($subTotal)}</td>
                    </tr>
                {/if}
                <tr class="">
                    <td>VAT</td>
                    <td>&nbsp;</td>
                    <td>&pound;{$basket->getPrice('vat')}</td>
                </tr>
                <tr class="bb">
                    <td><strong>Total including delivery charges and tax</strong></td>
                    <td>&nbsp;</td>
                    <td><strong>&pound;{$basket->getPrice('total')}</strong></td>
                </tr>
            </table>

            {if $this->node->hasBarclaysBankingInBasket()}
                <div class="barclays-box">
                    <b>A Fast Track Barclays Business bank account application with <span style="font-weight: bold; font-style: italic; font-size: 14px; color: rgb(255, 102, 0);">&pound;50 cash back</span> has been added to your basket.</b><br />
                    <span>We recommend that all new businesses set up a Barclays Business bank account. If you do not want to be contacted by Barclays, simply <a href="{$this->router->link(130)}">click here</a>.</span>
                </div>
            {/if}

        </div>
    </div> <!-- .left-block -->


    {* ASSOCIATED PRODUCTS *}
    <div class="right-block">
        <table width="470" cellpadding="0" cellspacing="0" border="0">
            <col>
            <col width="50">
            <col width="50">
            <col width="50">
            <tr>
                <th class="first">Customers who bought this item also bought</th>
                <th class="center dark">Actual Price</th>
                <th class="center">Price if bought today</th>
                <th class="center dark">Add to Basket</th>
            </tr>
            {if !empty($associatedProducts2)}
                {foreach from=$associatedProducts2 key="key" item="product"}
                    <tr>
                        <td class="first">
                            <span style="position: relative;">
                                {$product->getLngTitle()}
                            </span>
                            <span>
                                <i class="fa fa-question-circle grey2 font15" data-toggle="tooltip" data-placement="bottom" title="{$product->getLngText()|strip_tags}"></i>
                            </span>
                        </td>
                        <td class="center dark">&pound;{$product->productValue}</td>
                        <td class="center"><strong>&pound;{$product->price}</strong></td>
                        <td class="center dark"><a href="{$this->router->link(null,"add=$key")}">Add</a></td>
                    </tr>
                {/foreach}
            {else}
                <tr>
                    <td colspan="4" class="first">No products</td>
                </tr>
            {/if}
        </table>
    </div> <!-- .right-block -->

    <div class="clear"></div>
</div> <!-- .container -->

<div class="container2 mtop20">
    <div class="basket-button fleft">
        <a href="{$this->router->link("BasketControler::PAGE_BASKET")}" class="btn_back2" style="margin-bottom: 0px;">Basket</a>
    </div>
    <div class="add-voucher fleft mleft">
        {if !$basket->voucher}
            {$form->getControl('voucherCode') nofilter}
            {$form->getControl("addVoucher") nofilter}
            <span class="ff_control_err">{$form->getError('voucherCode')}</span>
            <div class="clear"></div>
        {/if}
    </div>
    <div class="payment-button fright">
        {$form->getControl("payment") nofilter}
    </div>
    <div class="clear"></div>
</div>

{$form->getEnd() nofilter}
