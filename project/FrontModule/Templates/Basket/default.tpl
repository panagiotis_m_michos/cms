{include file="@header.tpl"}


{$form->getBegin() nofilter}
	<div id="maincontent-full">
		{if !$basket->isEmpty()}
		    {if $visibleTitle}
            <h1>{$title}</h1>
            {/if}
		    <h3>Summary</h3>
		    {$form->getHtmlErrorsInfo() nofilter}
		    
		    {* BASKET SUMMARY *}
		    <div class="box-lrgwhite">
		        <div class="box-lrgwhite-top">
		            <div class="box-lrgwhite-bottom">
		                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="formtable mnone mbottom">
						<tr>
							<th width="310">Product</th>
							<th width="75" align="center" valign="top">Quantity</th>
							<th width="75">Price</th>
							<th width="90">&nbsp;</th>
						</tr>					
						
						{foreach from=$basket->items key="key" item="item"}							
						<tr>
							<td>
                                {$item->getLngTitle()}
                                {if $item->hasRenewalProduct()}
                                    <i>({$item->getSubscriptionPeriodText()})</i>
                                {/if}
                            </td>
							<td align="center" valign="top">
								{if $item->canBeUpdated($basket) == FALSE}
									{$item->qty}
								{else}
									{$form->getControl($key) nofilter}
									
								{/if}
							</td>
							<td>&pound;{$item->totalPrice}</td>
							<td><a href="{$this->router->link(null, "remove_id=$key")}">Remove</a></td>
						</tr>
						{/foreach}
					
						<tr class="bb">
							<td><strong>Subtotal</strong></td>
							<td>&nbsp;</td>
							<td><strong>&pound;{$basket->getPrice('subTotal')}</strong></td>
							<td>&nbsp;</td>
						</tr>
						{* SHOW VOUCHER *}
						{if $basket->voucher}
							<tr>
								<td>
									Voucher `{$basket->voucher->name}`
								</td>
								<td>&nbsp;</td>
								{assign var="subTotal" value=$basket->getPrice('subTotal')}
								<td style="color: red;">-&pound;{$basket->voucher->discount($subTotal)}</td>
								<td>{$form->getControl("removeVoucher") nofilter}</td>
							</tr>
						{/if}
						<tr class="">
							<td>VAT</td>
							<td>&nbsp;</td>
							<td>&pound;{$basket->getPrice('vat')}</td>
							<td>&nbsp;</td>
						</tr>
						<tr class="bb">
							<td><strong>Total including delivery charges and tax</strong></td>
							<td>&nbsp;</td>
							<td><strong>&pound;{$basket->getPrice('total')}</strong></td>
							<td>
								<a href="{$this->router->link(null, 'remove_all=1')}" class="normalfont" onclick="return confirm('Are you sure?')">Remove All</a>
							</td>
						</tr>
						</table>
						
						{* NAVIGATION *}
						<table width="100%" style="margin: 30px 0 0 0;">
						<col>
						<col>
						<col width="200">
						<tr>
							<td><a href="{$this->router->link("HomeControler::HOME_PAGE")}" class="btn_back mtop fleft clear">Shopping</a></td>
							<td align="right">{$form->getControl("update") nofilter}</td>
							<td>{$form->getControl("checkout") nofilter}</td>
						</tr>
						</table>
		
		                <div class="clear"></div>
		            </div>
		        </div>
		    </div>
		{* EMPTY BASKET *}
		{else}
		 	{if $visibleTitle}
            <h1>{$title}</h1>
            {/if}
		 	<div class="box-lrgwhite">
		        <div class="box-lrgwhite-top">
		            <div class="box-lrgwhite-bottom">
						<h><strong>Your basket is empty</strong></p>
						<p><a href="{$this->router->link("HomeControler::HOME_PAGE")}" class="btn_back mtop fleft clear">Shopping</a></p>
						<div class="clear"></div>
					</div>
				</div>
			</div>
		{/if}
	    <div class="clear"></div>
	</div>
	<div class="clear"></div>
{$form->getEnd() nofilter}

{include file="@footer.tpl"}
