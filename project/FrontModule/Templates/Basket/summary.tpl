{include file="@header.tpl"}
{scripts file='webloader_front.neon' section='orderSummary'}

<div id="order-summary-page">
    {include 'Basket/@form.tpl'}
</div>

{literal}
    <script type="text/javascript">
        $(function(){
            cms.summaryForm.init();
        });
    </script>

    <!--[if lt IE 9]>
    <script type="text/javascript">
        $(document).ready(function(){
            // Set the width via the plugin.
            $('select').ieSelectWidth
            ({
                width : 200,
                containerClassName : 'select-container',
                overlayClassName : 'select-overlay'
            });
        });
    </script>
    <![endif]-->
{/literal}

{include file="@footer.tpl"}
