{include file="@header.tpl"}

<div id="maincontent1">	
			{if $visibleTitle}
            <h1>{$title}</h1>
            {/if}
			
		    <div style="width:733px; background-color: rgb(204, 229, 255); border: 1px solid rgb(204, 204, 204); margin-bottom: 15px; padding: 10px;">
		        To import a company on to our management system you will need your company number 
		        and your web filing authentication code. If you are unsure of where to find 
		        these details please see below: 
		
		        <br />
		        <br />
		
		        <strong>Company Number: </strong> 
		        <a href=http://www.companieshouse.gov.uk/info target="_blank"> Companies House WebCHeck</a>
		
		        <br />
		        <br />
		
		        <strong>Web Filing Authentication Code:</strong> This is sent to the company's registered 
		        office address. You can reorder this code from Companies House - please allow 5 working days. 
		        You will need to:
		        <ol>
		            <li>Register for <a href="https://ewf.companieshouse.gov.uk/seclogin" target="_blank">Web Filing</a></li>
		            <li>Sign in to <a href="https://ewf.companieshouse.gov.uk/seclogin" target="_blank">Web Filing</a></li>
		            <li>Click 'Forgotten your authentication code'</li>
		            <li>Request it to be resent.</li>
		        </ol>
		    </div>
			<div style="margin-left: 0pt; width: 753px;">
				{* FORM BEGIN *}
				{$form nofilter}
			</div>
    		
			</div>
	

	
{include file="@footer.tpl"}