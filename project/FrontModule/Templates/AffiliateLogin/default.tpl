{include file="@AffiliateHeader.tpl"}

<div style="width: 730px;">
	<div style="float: left; width: 360px;">
		<h2>Register</h2>
		{$registrationForm}
	</div>
	<div style="float: right; width: 360px;">
		<h2>Login</h2>
		{$loginForm}		
	</div>
	<div style="clear: both;"></div>
</div>

{include file="@AffiliateFooter.tpl"}