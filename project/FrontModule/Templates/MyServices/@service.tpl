{assign 'settings' $settingsService->getSettingsByService($service->getService())}
{assign 'serviceId' $service->getId()}

<tr class="service-row">
    <td>{$service->getServiceName()}</td>
    <td title="{$service->getStatus()}" ><i class="{$service->getStatusCSSClass()}"></i>{$service->getStatus()}</td>
    {if $service->isRenewable()}
        <td class="txtcenter">{$service->dtExpires|datetime}</td>
        <td class="txtcenter">
            <div class="disabled-autorenewal-container inlineblock {if $settings->isAutoRenewalEnabled()}hidden{/if}">
                <i class="fa fa-times-circle em13 grey2"></i><span class="grey2">Disabled</span>
            </div>
            <div class="enabled-autorenewal-container inlineblock {if !$settings->isAutoRenewalEnabled()}hidden{/if}">
                <i class="fa fa-check-circle em13 green1"></i>Active
            </div>

            {if $service->isAutoRenewalAllowed() && $customer->hasActiveToken() && $service->canToggleAutoRenewal()}
                <a href="javascript://" class="autorenewal-popover-link" data-toggle="autorenewal-popover" data-service-id="{$serviceId}">
                    edit
                </a>

                <div class="autorenewal-popover-title-{$serviceId} hidden">
                    Auto-Renew<i class="loading hidden"></i><i class="close fa fa-times grey3" data-service-id="{$serviceId}"></i>
                </div>
                <div class="autorenewal-popover-content-{$serviceId} hidden">
                    <div>
                        <label>
                            <input type="radio" class="radio-enable-autorenewal"
                                   name="radio-autorenewal-{$serviceId}"
                                   data-url="{url route='enable_auto_renewal' serviceId=$serviceId}"
                                   data-service-id="{$serviceId}"
                                   {if $settings->isAutoRenewalEnabled()}checked="checked"{/if}>
                            Enabled
                        </label>
                        <label class="fright">
                            <input type="radio" class="radio-disable-autorenewal"
                                   name="radio-autorenewal-{$serviceId}"
                                   data-url="{url route='disable_auto_renewal' serviceId=$serviceId}"
                                   data-service-id="{$serviceId}"
                                   {if !$settings->isAutoRenewalEnabled()}checked="checked"{/if}>
                            Disabled
                        </label>
                    </div>
                    <div class="alert alert-success alert-dismissible autorenewal-popover-flash hidden" role="alert">
                        Saved. <span class="autorenewal-popover-flash-overdue hidden">We'll process this payment overnight and email you to confirm.</span>
                    </div>
                    <p class="font13">
                        <strong>Payment method to be used:</strong><br>
                        {$customer->getActiveToken()->getCardTypeText()} ending in {$customer->getActiveToken()->getCardNumber()}
                    </p>
                </div>
            {/if}
        </td>
        <td class="txtcenter">
            <a href="javascript://" data-toggle="settings-popover" data-service-id="{$serviceId}">
                <i class="fa fa-cog em13 grey3"></i>
            </a>
        </td>
        <td class="txtcenter renewal-price {if $settings->isAutoRenewalEnabled()}disabled{/if}">
            {$form->getControl($service->id) nofilter}
            {$form->getLabel($service->id) nofilter}
        </td>
    {else}
        <td class="txtcenter"></td>
        <td class="txtcenter"></td>
        <td class="txtcenter">
            <a href="#" data-toggle="settings-popover" data-service-id="{$serviceId}">
                <i class="fa fa-cog em13 grey3"></i>
            </a>
        </td>
        <td class="txtcenter"></td>
    {/if}
</tr>
