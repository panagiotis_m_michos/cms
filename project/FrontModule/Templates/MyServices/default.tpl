{include file="@header.tpl"}

<script src="{$urlJs}RenewalForm.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var serviceSettingsPopovers = new CMS.ServiceSettingsPopovers('{$myServicesSettingUrl}');
        serviceSettingsPopovers.init();

        var renewalForm = new CMS.RenewalForm('form[name$="renew_services"]', serviceSettingsPopovers);
        renewalForm.init();
    });
</script>

<div id="maincontent" class="my-services-page">
    {if $customerCompanies}
        {$form->begin nofilter}
        <div>
            <h1>My Services</h1>
            <span class="midfont grey2">Check the status of all your services and renew them when necessary.</span>
            <span class="txtcenter width70 fright toggle-company-hide hidden">
                <a href="#" data-renewal-action="toggleAllCompanyServices">
                    <i class="fa fa-angle-up"></i>
                    Hide All
                </a>
            </span>
            <span class="txtcenter width70 fright toggle-company-show">
                <a href="#" data-renewal-action="toggleAllCompanyServices">
                    <i class="fa fa-angle-down"></i>
                    Expand All
                </a>
            </span>
        </div>
        {if !$customer->hasActiveToken()}
            <div class="flash info2 payment-method-flash">
                To enable automatic renewals you must have a payment method linked to your account.
                <a href="{$this->router->link("ManagePaymentControler::MANAGE_PAYMENT_PAGE")}">Add new payment method</a>
            </div>
        {/if}
        <div id="sstatusgrid">
            {foreach $customerCompanies as $company}
                {if $disabledRemindersCompanyId && $company->getId() == $disabledRemindersCompanyId}
                    <div class="alert alert-success alert-dismissible reminders-disabled-flash" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4>All payment reminders have been turned off for this company</h4>
                        <ul>
                            <li>You can turn reminders back on from each service's settings</li>
                            <li>Services set to auto-renew cannot have reminders turned off (auto-renew must be disabled first)</li>
                            <li>If you do decide to renew a service, we'll turn reminders back on for you (so you are notified for next year)</li>
                        </ul>
                    </div>
                {/if}
                {include 'MyServices/@company.tpl'}
            {/foreach}
        </div>
        <div id="summary">
            <div class="mleft30 mtop20 mright8 mbottom20 fleft txtright">
                <div>Subtotal</div>
                <div>VAT</div>
                <div><strong>Total</strong></div>
            </div>
            <div class="mtop20 fleft">
                <div class="price-subtotal" data-renewal-selector="subtotal">£0.00</div>
                <div class="price-vat" data-renewal-selector="vat">£0.00</div>
                <div class="price-total" data-renewal-selector="total" style="font-weight: bold">£0.00</div>
            </div>
            <div class="m20">
                {$form->getControl('submit') nofilter}
            </div>
        </div>
        {$form->end nofilter}
    {else}
        <div>
            <h1>My Services</h1>
            <span class="midfont grey2">No services.</span>
        </div>
    {/if}
</div>

{include file="@footer.tpl"}
