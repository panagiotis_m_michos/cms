{if $company->hasServices()}
    <table id="c_{$company->getId()}" class="company-block" data-company-id="{$company->getId()}">
        <thead>
            <tr class="company-header-expanded">
                <th colspan="5" data-renewal-toggable="toggleCompany" class="company-name4">
                    <a name="company={$company->getId()}"></a>
                    {$company->getCompanyName()}
                </th>
                <th class="txtcenter width70">
                    <a href="#" class="toggle-company" data-renewal-action="toggleCompanyServices">
                        <i class="fa fa-angle-up"></i>
                        Hide
                    </a>
                </th>
            </tr>
            <tr class="company-header-collapsed hidden">
                <th colspan="3" data-renewal-toggable="toggleCompany" class="company-name2">
                    {$company->getCompanyName()}
                </th>
                <th colspan="2" data-renewal-toggable="toggleCompany" style="width: 50%;" class="action-required">
                    {if $company->isServiceActionRequired()}
                        <i class="fa fa-exclamation-circle largefont red"></i>
                        <span class="midfont">Action required</span>
                    {else}
                        <i class="fa fa-check-circle largefont green1"></i>
                        <span class="midfont">No action needed</span>
                    {/if}
                </th>
                <th class="txtcenter width70" data-renewal-toggable="toggleCompany">
                    <a href="#" class="toggle-company expand-{$company->getId()}" data-renewal-action="toggleCompanyServices">
                        <i class="fa fa-angle-down"></i>
                        Expand
                    </a>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr class="heading" data-renewal-toggable="toggleCompany">
                <td class="width300">SERVICE</td>
                <td class="width130">STATUS</td>
                <td class="txtcenter width70">EXPIRES ON</td>
                <td class="txtcenter width130">
                    AUTO-RENEW
                    <div class="tooltip-link" rel="auto-renewal">
                        <i class="fa fa-info-circle em15 grey2" data-toggle="tooltip" data-placement="top"
                           title="Don't risk losing your services. Set them to automatically renew and you won't have to worry about them expiring again.
                            Expired services cannot be set to auto renew, they must be manually renewed first."></i>
                    </div>
                </td>
                <td class="txtcenter width70">SETTINGS</td>
                <td class="txtcenter width100">RENEW?</td>
            </tr>
            {$services = $company->getServices()}
            {foreach $services as $service}
                {include 'MyServices/@service.tpl'}
            {/foreach}
        </tbody>
    </table>
{/if}
