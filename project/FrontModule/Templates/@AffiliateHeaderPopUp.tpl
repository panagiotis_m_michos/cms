<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta name="description" content="{$description}" />
        <meta name="keywords" content="{$keywords}" />
        <title>{$seoTitle}</title>	

        <link rel="stylesheet" href="{$urlComponent}jquery-ui/themes/base/minified/jquery-ui.min.css" type="text/css" />
        <link rel="stylesheet" href="{$urlCss}project.css" type="text/css" />
        <link rel="stylesheet" href="{$urlCss}htmltable.css" type="text/css" />
        <link rel="stylesheet" href="{$urlCss}stylesheet.css"  type="text/css" />
        <link rel="stylesheet" href="{$urlCss}forms.css" type="text/css" />
        <link rel="stylesheet" href="{$urlCss}affiliate.css" type="text/css" />
        <link rel="shortcut icon" href="{$urlImgs}favicon.ico" type="image/x-icon" />
        <link rel="icon" href="{$urlImgs}favicon.ico" type="image/x-icon" />

        <script type="text/javascript" src="{$urlComponent}jquery/dist/jquery.min.js" ></script>
        <script type="text/javascript" src="{$urlComponent}jquery-ui/ui/minified/jquery-ui.custom.min.js"></script>
        <script type="text/javascript" src="{$urlComponent}jquery-migrate/jquery-migrate.min.js" ></script>
        <script type="text/javascript" src="{$urlJs}jquery.tools.min.js"></script> 
        <script language="JavaScript" src="{$urlJs}project.js" type="text/javascript"></script>
    </head>

    <body>
        <div id="wrapperPopUp">
 
            <div id="content">

	    {include file='@blocks/flashMessage.tpl'}
