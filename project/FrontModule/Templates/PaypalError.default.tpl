{include file="@header.tpl"}

{capture name="content"}
	{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
	{if isset($message)}
	<br />
	<p style="color:red;font-size:14px;">
	{$message}
	</p>
	{/if}
{/capture}

{include file='@blocks/defaultTemplateBlock.tpl' content=$smarty.capture.content}
{include file='@blocks/rightColumn.tpl'}
{include file="@footer.tpl"}