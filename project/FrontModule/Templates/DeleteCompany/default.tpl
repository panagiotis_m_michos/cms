{include file="@header.tpl"}
{literal}
<style type="text/css">
    .delete-page-ul li{
        padding-bottom:8px;
    }

    #deleteCompany{
        height:33px;
        line-height:33px;
        width:150px;
    }
</style>

{/literal}

<div id="maincontent2">
    <p id="breadcrumbs">
        <a href="{$this->router->link("DashboardCustomerControler::DASHBOARD_PAGE")}">Dashboard</a> &gt;
        <a href="{$this->router->link("CompaniesCustomerControler::COMPANIES_PAGE")}">My Companies</a> &gt;
        <a href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}">{$companyName}</a> &gt;
        Delete Company 
	</p> <!-- #breadcrumbs -->
    {if $visibleTitle}
    <h1>{$title}</h1>
    {/if}

    <div class="flash error" style="width:730px;margin:auto;margin-bottom: 20px;padding:10px;padding-top:0;">
        <p>This deletion process will remove the company from the Companies Made Simple (CMS) system.</p>
        <p>
           <strong>This process will NOT dissolve this company.</strong>. You are still legally required to file all statutory documents.
        </p>
    </div>


    <div class="flash info2" style="width:750px;padding: 0;margin:auto;">
        <ul class="delete-page-ul" style="list-style-type: square;">
            <li>Are you sure you want to delete this company?</li>
            <li>Deleting this company will completely remove all records of it from the CMS system.</li>
            <li>This includes all company documents provided (certificate, M&A, share certs) and any company documents you have uploaded yourself. Please check and download these before deleting.</li>
            <li>The company’s history of submissions will also be deleted.</li>
            <li>If you need to use this company elsewhere please save the company number and authentication code.</li>
            <li>IMPORTANT: All details related to this company will be completely deleted from the CMS system.</li>
        </ul>
    </div>

    <div style="width:750px;margin:auto;padding-top:15px;">
        {$form->getHtmlErrorsInfo() nofilter}
        {$form->getBegin() nofilter}
        <fieldset id="fieldset_0">
            <legend>Delete Company</legend>
            <table class="ff_table">
                <tbody>
                    <tr>
                        <th>{$form->getLabel('message') nofilter}</th>
                        <td>
                            {$form->getControl('message') nofilter}
                        <td>
                            <span class="ff_control_err">{$form->getError('message')}</span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </fieldset>
        
        <fieldset id="fieldset_1">
            <table class="ff_table">
                <col  />
                <col />
                <tbody>
                    
                    <tr>
                        <th style="text-align: right;width: 20px;">{$form->getControl('removeConfirm') nofilter}</th>
                        <td>
                            {$form->getLabel('removeConfirm') nofilter}
                        </td>
                        <td>
                            <span class="ff_control_err">{$form->getError('removeConfirm')}</span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </fieldset>
        
        <fieldset id="fieldset_2">
           <table class="ff_table">
                <col />
                <col />
                <tbody>
                    <tr>
                        <th style="text-align: right; width: 20px;">{$form->getControl('removeObligation') nofilter}</th>
                        <td>
                            {$form->getLabel('removeObligation') nofilter}
                        </td>
                        <td>
                            <span class="ff_control_err">{$form->getError('removeObligation')}</span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </fieldset>

        <fieldset id="fieldset_3">
            <legend>Submit</legend>
            <table class="ff_table">
                <tbody>
                    <tr>
                        <th>&nbsp;</th>
                        <td>
                            {$form->getControl('deleteCompany') nofilter}
                            {$form->getEnd() nofilter}
                        </td>
                        <td>
                            <button style="width:150px; margin-right: 10px;text-decoration: none;line-height: 33px;height: 33px;" type="button" onclick="window.location.href='{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}'">Cancel</button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </fieldset>
        
        {$form->getEnd() nofilter}



    </div>
    

</div>


{include file="@footer.tpl"}