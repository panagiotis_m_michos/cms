{capture name="warning"}
	<div class="flash error" style="margin: 25px 0 25px 0; width: 960px; clear: both;">
		{* HAS DEFAULT COMPANY NAME *}
        {if isset($defaultCompanyName)}
 			<p><strong>WARNING:</strong> You have not yet entered your company name. Please click <a href="{$this->router->link("CFCompanyNameControler::COMPANY_NAME_PAGE","company_id=$companyId", "backlink=$backLink")}" style="color: white;">here</a> to search for your name. Once complete you will be able to submit your application to Companies House</p>
		{else}
	    	<p><strong>WARNING:</strong> This is the final step before incorporation, once you click 'Submit' your incorporation will be sent straight to Companies House. This <strong>cannot</strong> be undone.</p>
	    	<ul>
	    		<li>Please review all the information you have input, checking for spelling errors.</li>
	        	<li>Please do not forget to double check your company name!</li>
	        </ul>
        {/if}
	</div>
{/capture}

{include file="@header.tpl"}
<div class="formprocesstitle">{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}</div>

{* <div class="livesuprt"><script language="javascript" src="https://support.madesimplegroup.com/visitor/index.php?_m=livesupport&_a=htmlcode&departmentid=2"></script></div> *}

{* TABS *}
{include file="@blocks/navlist.tpl" currentTab=$this->action}

<div id="maincontent">
	{* WARNING MESSAGE *}
	{$smarty.capture.warning nofilter}

	<h2>Company</h2>

	<table class="grid2" width="980" style="margin-bottom:30px;">
	<col width="250">
	<tr>
		<th>Name</th>
		<td>
			{$companyName}
			{if $company->getStatus() != "pending"}
				<a href="{$this->router->link("CFCompanyNameControler::COMPANY_NAME_PAGE","company_id=$companyId", "backlink=$backLink")}" style="font-weight: bold;">[change]</a>
			{/if}
		</td>
	</tr>
	<tr>
		<th>Type</th>
		<td>{$companyType}</td>
	</tr>
	{*<tr>*}
		{*<th>Status</th>*}
		{*<td>{$company->getStatus()|strtoupper}</td>*}
	{*</tr>*}
    {if Feature::featurePsc() && $origType != 'LLP'}
        <tr>
            <th>SIC Codes</th>
            <td>
                {foreach $sicCodes as $sicCode}
                    {$sicCode}
                    {if $sicCode == 82990}Other business support service activities n.e.c.{/if}
                    <br>
                {/foreach}
                {if $company->getStatus() != "pending"}
                    <a href="#" data-toggle="modal" data-target="#company-formation-summary__sic-code-change__modal" style="font-weight: bold">
                        [change]
                    </a>
                {/if}
            </td>
        </tr>
    {/if}

	{* REJECTED DESCRIPTION *}
	{if isset($rejectedDesc)}
	<tr>
		<th>Rejected description:</th>
		<td style="color: red; font-weight: bold;">{$rejectedDesc}</td>
	</tr>
	{/if}
	</table>


	{* REGISTERED OFFICE *}
	{if $company->getStatus() == "pending"}
		<h2>Registered Office</h2>
	{else}
		<h2>Registered Office <span><a href="{$this->router->link("CFRegisteredOfficeControler::REGISTER_OFFICE_PAGE", "company_id=$companyId")}" style="font-size: 11px;">[change]</a></span></h2>
	{/if}

	<table class="grid2" width="980" style="margin-bottom:30px;">
	<col width="250">
	<tr>
		<th>Address 1</th>
		<td>{$office->premise}</td>
	</tr>
	<tr>
		<th>Address 2</th>
		<td>{$office->street}</td>
	</tr>
	<tr>
		<th>Address 3</th>
		<td>{$office->thoroughfare}</td>
	</tr>
	<tr>
		<th>Town</th>
		<td>{$office->post_town}</td>
	</tr>
	<tr>
		<th>County</th>
		<td>{$office->county}</td>
	</tr>
	<tr>
		<th>Postcode</th>
		<td>{$office->postcode}</td>
	</tr>
	<tr>
		<th>Country</th>
		<td>{$office->country}</td>
	</tr>
	</table>

	{* DIRECTORS *}

        {if $company->getStatus() == "pending"}
            {if $origType != 'LLP'}<h2>Directors</h2>{else}<h2>Members</h2>{/if}
        {else}
            {if $origType != 'LLP'}
                <h2>Directors <span><a href="{$this->router->link("CFDirectorsControler::DIRECTORS_PAGE", "company_id=$companyId")}" style="font-size: 11px;">[change]</a></span></h2>
            {else}
                <h2>Members <span><a href="{$this->router->link("CFDirectorsControler::DIRECTORS_PAGE", "company_id=$companyId")}" style="font-size: 11px;">[change]</a></span></h2>
            {/if}
        {/if}

        <table class="grid2" width="980" style="margin-bottom:30px;">
        <col width="250">
        {foreach from=$directors key="key" item="director"}
            <tr>
                <th colspan="2" class="center">{if $origType != 'LLP'}Director {$key+1}{else} Member {$key+1}{/if}</th>
            </tr>

            {* CORPORATE DIRECTOR *}
            {if isset($director->corporate_name)}
                <tr>
                    <th>Company Name</th>
                    <td>{$director->corporate_name}</td>
                </tr>
                <tr>
                    <th>Authorizing Person First Name</th>
                    <td>{$director->forename}</td>
                </tr>
                <tr>
                    <th>Authorizing Person Last Name</th>
                    <td>{$director->surname}</td>
                </tr>
            {* PERSON DIRECTOR *}
            {else}

                <tr>
                    <th>Title</th>
                    <td>{$director->title}</td>
                </tr>
                <tr>
                    <th>First Name</th>
                    <td>{$director->forename}</td>
                </tr>
                <tr>
                    <th>Middle Name</th>
                    <td>{$director->middle_name}</td>
                </tr>
                <tr>
                    <th>Last Name</th>
                    <td>{$director->surname}</td>
                </tr>
                <tr>
                    <th>DOB</th>
                    <td>{$director->dob}</td>
                </tr>
                <tr>
                    <th>Nationality</th>
                    <td>{$director->nationality}</td>
                </tr>
                <tr>
                    <th>Occupation</th>
                    <td>{$director->occupation}</td>
                </tr>
                <tr>
                <th><b>Residential Address</b></th>
                <td></td>
                </tr>
                <tr>
                    <th>Address 1</th>
                    <td>{$director->residential_premise}</td>
                </tr>
                <tr>
                    <th>Address 2</th>
                    <td>{$director->residential_street}</td>
                </tr>
                <tr>
                    <th>Address 3</th>
                    <td>{$director->residential_thoroughfare}</td>
                </tr>
                <tr>
                    <th>Town</th>
                    <td>{$director->residential_post_town}</td>
                </tr>
                <tr>
                    <th>County</th>
                    <td>{$director->residential_county}</td>
                </tr>
                <tr>
                    <th>Postcode</th>
                    <td>{$director->residential_postcode}</td>
                </tr>
                <tr>
                    <th>Country</th>
                    <td>{$director->residential_country}</td>
                </tr>
                <tr>
                    <th><b>Service Address</b></th>
                    <td></td>
                </tr>
            {/if}
            <tr>
                <th>Address 1</th>
                <td>{$director->premise}</td>
            </tr>
            <tr>
                <th>Address 2</th>
                <td>{$director->street}</td>
            </tr>
            <tr>
                <th>Address 3</th>
                <td>{$director->thoroughfare}</td>
            </tr>
            <tr>
                <th>Town</th>
                <td>{$director->post_town}</td>
            </tr>
            <tr>
                <th>County</th>
                <td>{$director->county}</td>
            </tr>
            <tr>
                <th>Postcode</th>
                <td>{$director->postcode}</td>
            </tr>
            <tr>
                <th>Country</th>
                <td>{$director->country}</td>
            </tr>

            {* CORPORATE DIRECTOR *}
            {if isset($director->corporate_name)}
                {* EEA *}
                {if $director->eeaType}
                    <tr>
                        <th>Type</th>
                        <td>EEA</td>
                    </tr>
                    <tr>
                        <th>Country Registered</th>
                        <td>{$director->place_registered}</td>
                    </tr>
                    <tr>
                        <th>Registration number</th>
                        <td>{$director->registration_number}</td>
                    </tr>
                {* NON EEA *}
                {else}
                    <tr>
                        <th>Type</th>
                        <td>Non EEA</td>
                    </tr>
                    <tr>
                        <th>Country Registered</th>
                        <td>{$director->place_registered}</td>
                    </tr>
                    <tr>
                        <th>Registration number</th>
                        <td>{$director->registration_number}</td>
                    </tr>
                    <tr>
                        <th>Governing law</th>
                        <td>{$director->law_governed}</td>
                    </tr>
                    <tr>
                        <th>Legal Form</th>
                        <td>{$director->legal_form}</td>
                    </tr>
                {/if}
            {/if}

            {if $origType == 'LLP'}
                <tr>
                    <th>First three letters of Town of birth</th>
                    <td>{$director->BIRTOWN}</td>
                </tr>
                <tr>
                    <th>Last three digits of Telephone number</th>
                    <td>{$director->TEL}</td>
                </tr>
                <tr>
                    <th>First three letters of Eye colour</th>
                    <td>{$director->EYE}</td>
                </tr>
            {/if}

            <tr>
                <th>Consent to act</th>
                <td>{if $director->consentToAct}Yes{else}No{/if}</td>
            </tr>
        {/foreach}
        </table>

	{* SHAREHOLDERS *}
    {if $origType != 'LLP'}
        {if $origType != 'BYGUAR'}
            {if $company->getStatus() == "pending"}
                <h2>Shareholders</h2>
            {else}
                <h2>Shareholders <span><a href="{$this->router->link("CFShareholdersControler::SHAREHOLDERS_PAGE", "company_id=$companyId")}" style="font-size: 11px;">[change]</a></span></h2>
            {/if}
        {else}
            {if $company->getStatus() == "pending"}
                <h2>Members</h2>
            {else}
                <h2>Members <span><a href="{$this->router->link("CFShareholdersControler::SHAREHOLDERS_PAGE", "company_id=$companyId")}" style="font-size: 11px;">[change]</a></span></h2>
            {/if}
        {/if}

        <table class="grid2" width="980" style="margin-bottom:30px;">
        <col width="250">
        {foreach from=$shareholders key="key" item="shareholder"}
        <tr>
            {if $origType != 'BYGUAR'}
                <th colspan="2" class="center">Shareholder {$key+1}</th>
            {else}
                <th colspan="2" class="center">Member {$key+1}</th>
            {/if}
        </tr>
        {if isset($shareholder->corporate_name)}
        <tr>
            <th>Company Name</th>
            <td>{$shareholder->corporate_name}</td>
        </tr>
        <tr>
            <th>Authorizing Person First Name</th>
            <td>{$shareholder->forename}</td>
        </tr>
        <tr>
            <th>Authorizing Person Last Name</th>
            <td>{$shareholder->surname}</td>
        </tr>
        {else}
        <tr>
            <th>Title</th>
            <td>{$shareholder->title}</td>
        </tr>
        <tr>
            <th>First Name</th>
            <td>{$shareholder->forename}</td>
        </tr>
        <tr>
            <th>Middle Name</th>
            <td>{$shareholder->middle_name}</td>
        </tr>
        <tr>
            <th>Last Name</th>
            <td>{$shareholder->surname}</td>
        </tr>
        {/if}
        <tr>
            <th>Address 1</th>
            <td>{$shareholder->premise}</td>
        </tr>
        <tr>
            <th>Address 2</th>
            <td>{$shareholder->street}</td>
        </tr>
        <tr>
            <th>Address 3</th>
            <td>{$shareholder->thoroughfare}</td>
        </tr>
        <tr>
            <th>Town</th>
            <td>{$shareholder->post_town}</td>
        </tr>
        <tr>
            <th>County</th>
            <td>{$shareholder->county}</td>
        </tr>
        <tr>
            <th>Postcode</th>
            <td>{$shareholder->postcode}</td>
        </tr>
        <tr>
            <th>Country</th>
            <td>{$shareholder->country}</td>
        </tr>
        <tr>
            <th>First three letters of Town of birth</th>
            <td>{$shareholder->BIRTOWN}</td>
        </tr>
        <tr>
            <th>Last three digits of Telephone number</th>
            <td>{$shareholder->TEL}</td>
        </tr>
        <tr>
            <th>First three letters of Eye colour</th>
            <td>{$shareholder->EYE}</td>
        </tr>
            {if $origType != 'BYGUAR'}
                <tr>
                    <th>Share Class</th>
                    <td>{$shareholder->share_class}</td>
                </tr>
                <tr>
                    <th>Shares</th>
                    <td>{$shareholder->num_shares}</td>
                </tr>
                <tr>
                    <th>Share Currency</th>
                    <td>{$shareholder->currency}</td>
                </tr>
                <tr>
                    <th>Share Value</th>
                    <td>{$shareholder->share_value}</td>
                </tr>
            {/if}
        {/foreach}
        </table>
    {/if}

    {if Feature::featurePsc()}
        {* PSCS *}
        {if $company->getStatus() == "pending"}
           <h2>People with Significant Control (PSC)</h2>
        {else}
           <h2>People with Significant Control (PSC) <span><a href="{url route="CFPscsControler::PSCS_PAGE" company_id=$companyId}" style="font-size: 11px;">[change]</a></span></h2>
        {/if}

        {if $summaryView->hasPscs()}
           <table class="grid2" width="980" style="margin-bottom:30px;">
           <col width="250">
           {foreach from=$summaryView->getPscs() key="key" item="psc"}
               <tr>
                   <th colspan="2" class="center">PSC {$key+1}</th>
               </tr>
               {if $psc->isCorporate()}
                   <tr>
                       <th>Company Name</th>
                       <td>{$psc->getCorporate()->getCorporateName()}</td>
                   </tr>
               {else}
                   <tr>
                       <th>Title</th>
                       <td>{$psc->getPerson()->getTitle()}</td>
                   </tr>
                   <tr>
                       <th>First Name</th>
                       <td>{$psc->getPerson()->getForename()}</td>
                   </tr>
                   <tr>
                       <th>Middle Name</th>
                       <td>{$psc->getPerson()->getMiddleName()}</td>
                   </tr>
                   <tr>
                       <th>Last Name</th>
                       <td>{$psc->getPerson()->getSurname()}</td>
                   </tr>
                   <tr>
                       <th>DOB</th>
                       <td>{$psc->getPerson()->getDob()}</td>
                   </tr>
                   <tr>
                       <th>Nationality</th>
                       <td>{$psc->getPerson()->getNationality()}</td>
                   </tr>
                   <tr>
                       <th>Country of Residence</th>
                       <td>{$psc->getPerson()->getCountryOfResidence()}</td>
                   </tr>
                   <tr>
                       <th><b>Residential Address</b></th>
                       <td></td>
                   </tr>
                   <tr>
                       <th>Address 1</th>
                       <td>{$psc->getResidentialAddress()->getPremise()}</td>
                   </tr>
                   <tr>
                       <th>Address 2</th>
                       <td>{$psc->getResidentialAddress()->getStreet()}</td>
                   </tr>
                   <tr>
                       <th>Address 3</th>
                       <td>{$psc->getResidentialAddress()->getThoroughfare()}</td>
                   </tr>
                   <tr>
                       <th>Town</th>
                       <td>{$psc->getResidentialAddress()->getPostTown()}</td>
                   </tr>
                   <tr>
                       <th>County</th>
                       <td>{$psc->getResidentialAddress()->getCounty()}</td>
                   </tr>
                   <tr>
                       <th>Postcode</th>
                       <td>{$psc->getResidentialAddress()->getPostcode()}</td>
                   </tr>
                   <tr>
                       <th>Country</th>
                       <td>{$psc->getResidentialAddress()->getCountry()}</td>
                   </tr>
                   <tr>
                       <th><b>Service Address</b></th>
                       <td></td>
                   </tr>
               {/if}
               <tr>
                   <th>Address 1</th>
                   <td>{$psc->getAddress()->getPremise()}</td>
               </tr>
               <tr>
                   <th>Address 2</th>
                   <td>{$psc->getAddress()->getStreet()}</td>
               </tr>
               <tr>
                   <th>Address 3</th>
                   <td>{$psc->getAddress()->getThoroughfare()}</td>
               </tr>
               <tr>
                   <th>Town</th>
                   <td>{$psc->getAddress()->getPostTown()}</td>
               </tr>
               <tr>
                   <th>County</th>
                   <td>{$psc->getAddress()->getCounty()}</td>
               </tr>
               <tr>
                   <th>Postcode</th>
                   <td>{$psc->getAddress()->getPostcode()}</td>
               </tr>
               <tr>
                   <th>Country</th>
                   <td>{$psc->getAddress()->getCountry()}</td>
               </tr>
               {if $psc->isCorporate()}
                   <tr>
                       <th>Place Registered</th>
                       <td>{$psc->getIdentification()->getPlaceRegistered()}</td>
                   </tr>
                   <tr>
                       <th>Registration number</th>
                       <td>{$psc->getIdentification()->getRegistrationNumber()}</td>
                   </tr>
                   <tr>
                       <th>Governing law</th>
                       <td>{$psc->getIdentification()->getLawGoverned()}</td>
                   </tr>
                   <tr>
                       <th>Legal Form</th>
                       <td>{$psc->getIdentification()->getLegalForm()}</td>
                   </tr>
                   <tr>
                       <th>Country Registered</th>
                       <td>{$psc->getIdentification()->getCountryOrState()}</td>
                   </tr>
               {/if}
               <tr>
                   <th><b>Nature of Controls</b></th>
                   <td>
                       {foreach $psc->getNatureOfControlsTexts() as $nocText}
                           {$nocText}<br>
                       {/foreach}
                   </td>
               </tr>
           {/foreach}
           </table>
        {elseif $summaryView->hasNoPscReason()}
            <table class="grid2" width="980" style="margin-bottom:30px;">
                <col width="250">
                <tr>
                    <th colspan="2">
                        <i>{$summaryView->getNoPscReason()}</i>
                    </th>
                </tr>
            </table>
        {/if}
    {/if}

	{* SECRETARIES *}
    {if $origType != 'LLP'}
        {if $company->getStatus() == "pending"}
            <h2>Secretaries</h2>
        {else}
            <h2>Secretaries <span><a href="{$this->router->link("CFSecretariesControler::SECRETARIES_PAGE", "company_id=$companyId")}" style="font-size: 11px;">[change]</a></span></h2>
        {/if}

        {if !empty($secretaries)}
            <table class="grid2" width="980" style="margin-bottom:30px;">
            <col width="250">
            {foreach from=$secretaries key="key" item="secretary"}
            <tr>
                <th colspan="2" class="center">Secretary {$key+1}</th>
            </tr>
            {if isset($secretary->corporate_name)}
                <tr>
                    <th>Company Name</th>
                    <td>{$secretary->corporate_name}</td>
                </tr>
                <tr>
                    <th>Authorizing Person First Name</th>
                    <td>{$secretary->forename}</td>
                </tr>
                <tr>
                    <th>Authorizing Person Last Name</th>
                    <td>{$secretary->surname}</td>
                </tr>
            {else}
                <tr>
                    <th>Title</th>
                    <td>{$secretary->title}</td>
                </tr>
                <tr>
                    <th>First Name</th>
                    <td>{$secretary->forename}</td>
                </tr>
                <tr>
                    <th>Middle Name</th>
                    <td>{$secretary->middle_name}</td>
                </tr>
                <tr>
                    <th>Last Name</th>
                    <td>{$secretary->surname}</td>
                </tr>
                {/if}
                <tr>
                    <th>Address 1</th>
                    <td>{$secretary->premise}</td>
                </tr>
                <tr>
                    <th>Address 2</th>
                    <td>{$secretary->street}</td>
                </tr>
                <tr>
                    <th>Address 3</th>
                    <td>{$secretary->thoroughfare}</td>
                </tr>
                <tr>
                    <th>Town</th>
                    <td>{$secretary->post_town}</td>
                </tr>
                <tr>
                    <th>County</th>
                    <td>{$secretary->county}</td>
                </tr>
                <tr>
                    <th>Postcode</th>
                    <td>{$secretary->postcode}</td>
                </tr>
                <tr>
                    <th>Country</th>
                    <td>{$secretary->country}</td>
                </tr>


                {* CORPORATE DIRECTOR *}
                {if isset($secretary->corporate_name)}
                    {* EEA *}
                    {if $secretary->eeaType}
                        <tr>
                            <th>Type</th>
                            <td>EEA</td>
                        </tr>
                        <tr>
                            <th>Country Registered</th>
                            <td>{$secretary->place_registered}</td>
                        </tr>
                        <tr>
                            <th>Registration number</th>
                            <td>{$secretary->registration_number}</td>
                        </tr>
                    {* NON EEA *}
                    {else}
                        <tr>
                            <th>Type</th>
                            <td>Non EEA</td>
                        </tr>
                        <tr>
                            <th>Country Registered</th>
                            <td>{$secretary->place_registered}</td>
                        </tr>
                        <tr>
                            <th>Registration number</th>
                            <td>{$secretary->registration_number}</td>
                        </tr>
                        <tr>
                            <th>Governing law</th>
                            <td>{$secretary->law_governed}</td>
                        </tr>
                        <tr>
                            <th>Legal Form</th>
                            <td>{$secretary->legal_form}</td>
                        </tr>
                    {/if}
                {/if}

                <tr>
                    <th>Consent to act</th>
                    <td>{if $secretary->consentToAct}Yes{else}No{/if}</td>
                </tr>
            {/foreach}
            </table>
        {else}
            <p>No secretaries</p>
        {/if}
    {/if}

	{* ARTICLES *}
    {if ($origType == 'LLP' && !empty($articles.support))}
        {if $company->getStatus() == "pending"}
            <h2>Support Documents</h2>
        {else}
            <h2>Support Documents <span><a href="{$this->router->link("CFArticlesControler::ARTICLES_PAGE", "company_id=$companyId")}" style="font-size: 11px;">[change]</a></span></h2>
        {/if}

        <table class="grid2" width="980" style="margin-bottom:30px;">
        <col width="250">
        {if !empty($articles.support)}
            <tr>
                <th>Support document</th>
                <td>{$articles.support.filename}</td>
            </tr>
        {/if}
        </table>
    {elseif $origType != 'LLP'}
        {if $company->getStatus() == "pending"}
            <h2>Articles</h2>
        {else}
            <h2>Articles <span><a href="{$this->router->link("CFArticlesControler::ARTICLES_PAGE", "company_id=$companyId")}" style="font-size: 11px;">[change]</a></span></h2>
        {/if}

        <table class="grid2" width="980" style="margin-bottom:30px;">
        <col width="250">
        <tr>
            <th>Article</th>
            <td>{if !empty($articles.article)}{$articles.article.filename}{/if}</td>
        </tr>
        {if !empty($articles.support)}
            <tr>
                <th>Support document</th>
                <td>{$articles.support.filename}</td>
            </tr>
        {/if}
        </table>
    {/if}

	{* eReminders *}
	{if $company->getStatus() == "pending"}
		<h2>eReminders</h2>
	{else}
		<h2 style="float: left;">eReminders <span><a class="change-company-auth-code" href="{$this->router->link("CFERemindersControler::EREMINDER_PAGE","company_id=$companyId", "popup=1")}" style="font-size: 11px;">[change]</a></span></h2>
        <div class="help-button" style="margin-top: 10px;">
            <a href="#" class="help-icon">help</a>
            <em>
                This service can be enabled or disabled at any time.
            </em>
        </div>
	{/if}

	<table class="grid2" width="980" style="clear:both; margin-bottom:30px;">
	<col width="250">
	<tr>
        <th colspan="2">
           The eReminder Service will issue emails when the accounts and Confirmation Statement are due for this company.
           Once your company is formed you will receive an activation email which you must click to enable the service.
           If you would prefer paper reminders click 'change'.
        </th>
	</tr>
	<tr>
		<th>eReminders</th>
		<td>{if $company->getEreminderId() == 1}
                Yes
            {else}
                No
            {/if}
        </td>
	</tr>
	</table>

    {* COMPANY NAME *}
	{if $company->getStatus() == "pending"}
		<h2>Company Name</h2>
	{else}
		<h2 style="float: left;">Company Name <span><a href="{$this->router->link("CFCompanyNameControler::COMPANY_NAME_PAGE","company_id=$companyId")}" style="font-size: 11px;">[change]</a></span></h2>
        <div class="help-button" style="margin-top: 10px;">
            <a href="#" class="help-icon">help</a>
            <em>
                {if $origType != 'LLP'}
                If you see a duplicate LTD of LIMITED please click change to
                update the name. Please retype rather than copy and paste if
                you are having issues; or call us +44 (0) 207 608 5500
                {else}
                If you see a duplicate LLP of LIMITED LIABILITY PARTNERSHIP
                please click change to update the name. Please retype rather
                than copy and paste if you are having issues;
                or call us +44 (0) 207 608 5500
                {/if}
            </em>
        </div>
	{/if}

	<table class="grid2" width="980" style="clear: both;margin-bottom:30px;">
	<col width="250">
	<tr>
        <th colspan="2">
            {if $origType != 'LLP'}
            Our system attempts to detect if you have added LTD or LIMITED to
            your name and if it does not think you have, it will append LTD to
            the name. Please check below as this is <b>exactly</b> how your name
            will be sent to Companies House.
            {else}
            Our system attempts to detect if you have added LLP or LIMITED LIABILITY PARTNERSHIP to
            your name and if it does not think you have, it will append LLP to
            the name. Please check below as this is <b>exactly</b> how your name
            will be sent to Companies House.
            {/if}
        </th>
	</tr>
	<tr>
		<th>Company Name</th>
		<td>{$companyName}</td>
	</tr>
	</table>

	{* REQUESTS *}
	{if $view->getFormSubmissions()}
	 	<h2>Requests history</h2>
	 	<table class="grid2" width="980" style="margin-bottom:30px;">
	 	<col width="50">
	 	<col>
	 	<col width="100">
	 	<col width="100">
	 	<col width="50">
	 	<tr>
	 		<th>Id</th>
	 		<th>Type</th>
	 		<th>Status</th>
	 		<th>Date</th>
	 		<th>Action</th>
	 	</tr>
	 	{foreach $view->getFormSubmissions() as $submission}
	 	<tr>
	 		<td class="center">{$submission->getId()}</td>
	 		<td>{$submission->getFormIdentifier()}</td>
	 		<td>{$submission->getResponseMessage()}</td>
	 		<td>{$submission->getLastSubmissionDate()|date_format:"%d/%m/%Y"}</td>
	 		<td class="center"><a href="{$this->router->link("CFRequests::REQUEST_PAGE", ['company_id' => $companyId, 'request_id' => $submission->getId()])}">view</a>
	 	</tr>
	 	{/foreach}
	 	</table>
	{/if}


	{* SUBMIT FORM *}
	{if !isset($defaultCompanyName)}

		{* WARNING MESSAGE *}
		{$smarty.capture.warning nofilter}

		{if isset($form)}
			{$form->getBegin() nofilter}
			<p>{$form->getControl('submit') nofilter}</p>
			{$form->getEnd() nofilter}
		{/if}
	{/if}
	</div>
	<div class="clear"></div>

</div>
<div class="clear"></div>
<br />
<br />
<br />

{if Feature::featurePsc() && $origType != 'LLP' && $company->getStatus() != "pending"}
    <div class="modal fade" id="company-formation-summary__sic-code-change__modal" tabindex="-1" role="dialog" aria-labelledby="company-formation-summary__sic-code-change__label">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="company-formation-summary__sic-code-change__label">Standard industrial classification of economic activities (SIC)</h4>
                </div>
                {$formHelper->start($sicCodesForm) nofilter}

                <div class="modal-body">
                    <p>Use the <a href="https://www.gov.uk/government/publications/standard-industrial-classification-of-economic-activities-sic" target="_blank" title="Companies House SIC codes list">Companies House SIC codes list</a> to select the company's nature of business.</p>
                    <p>You can enter up to 4 SIC codes. We have defaulted to the most used and generic code (82990 Other business support service activities n.e.c.) you can update this at any time in the future.</p>

                    {$formHelper->setTheme($sicCodesForm, 'cms.html.twig')}
                    {$formHelper->row($sicCodesForm['sicCode0']) nofilter}
                    {$formHelper->row($sicCodesForm['sicCode1']) nofilter}
                    {$formHelper->row($sicCodesForm['sicCode2']) nofilter}
                    {$formHelper->row($sicCodesForm['sicCode3']) nofilter}
                </div>
                <div class="modal-footer">
                    {$formHelper->widget($sicCodesForm['save']) nofilter}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                {$formHelper->end($sicCodesForm) nofilter}
            </div>
        </div>
    </div>
{/if}

{include file="@footer.tpl"}
