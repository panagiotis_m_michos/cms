{include file="@header.tpl"}

<style>
	{literal}
    input {
        height: 22px;
    }
	{/literal}
</style>

<div id="maincontent2">
	<p style="margin: 0 0 15px 0; font-size: 11px;"><a href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}">{$companyName}</a> &gt; {$title}</p> 
	{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
 <div style="margin-left: 0pt; width: 753px; " class="flash info2">
		Upload and store any additional company documents so they're all together in one place!
<ul>
    <li>Max filesize: 3 MB</li>
    <li>Max number of files: 10</li>
    <li>Supported file types: doc, docx, rtf, pdf, odt, xls, xlsx, ods</li>
</ul>
        Work with a file type not listed? Email us: <a href="mailto:info@madesimplegroup.com">info@madesimplegroup.com</a>
	</div>
{$form nofilter}
</div>
{include file="@footer.tpl"}