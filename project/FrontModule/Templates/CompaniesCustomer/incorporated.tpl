<h2 class="mtop30">Incorporated</h2>
{if !empty($companies)}
{* COMPANY FILTER FORM *}
{$formHelper->start($form, ['attr' => ['id' => 'customerCompaniesFilterForm']]) nofilter}
    <div class="mtop20 mbottom">
        <div id="sboxinc">
            <span>
                {$formHelper->widget($form['term']) nofilter}
                {$formHelper->widget($form['search']) nofilter}
            </span>
            <span id="advbtn" class="posrelative" style="right: -95px;">
                <a class="cspointer">advanced <i class="fa fa-angle-down"></i></a>
            </span>
        </div>
        <div id="advinc" class="padding20 displaynone" style="border-bottom: 1px solid #ccc;">
            <div class="">
                {$formHelper->widget($form['onlyActive']) nofilter}{$formHelper->label($form['onlyActive']) nofilter}<br/>
            </div>
            <div class="">
                {$formHelper->label($form['dateFilterType']) nofilter}{$formHelper->widget($form['dateFilterType']) nofilter}
                {$formHelper->label($form['dateFrom']) nofilter}{$formHelper->widget($form['dateFrom']) nofilter}
                {$formHelper->errors($form['dateFrom']) nofilter}
                {$formHelper->label($form['dateTo']) nofilter}{$formHelper->widget($form['dateTo']) nofilter}
                {$formHelper->errors($form['dateTo']) nofilter}
            </div>
        </div>
        <div id="currentFilter" style="border: 0px; display:none;" class="padding10">
            <span id="filterText">&nbsp;</span>
            <span  id="clearFilter">
                <a href="{$this->router->link("CompaniesCustomerControler::COMPANIES_PAGE")}" class="ui-widget-content ui-corner-all buttons">Clear Filter</a>
            </span>
        </div>
    </div>

    <table id="incorporated-companies" class="display data-table">
        <thead>
        <tr>
            <th colspan="6">&nbsp;</th>
            <th>
                <a id="syncAllLink" href="{$this->router->link("CompaniesCustomerControler::PAGE_SYNC")}"
                   class="ui-widget-content ui-corner-all buttons syncAllComMsg" style="padding: 3px;">Update All
                </a>
            </th>
        </tr>
        <tr>
            <th>Company Name</th>
            <th>Company Number</th>
            <th>Incorporation Date</th>
            <th>Status</th>
            <th>Accounts Due</th>
            <th>Returns Due</th>
            <th>My Services</th>
        </tr>
        </thead>
        <tbody>
            {foreach $companies as $company}
            <tr>
                <td>
                    <a href="{$this->router->link(CUSummaryControler::SUMMARY_PAGE)}?company_id={$company->getId()}">{$company->getName()}</a>
                </td>
                <td>
                    <a href="{$this->router->link(CUSummaryControler::SUMMARY_PAGE)}?company_id={$company->getId()}">{$company->getNumber()}</a>
                </td>
                <td>{$company->getIncorporationDate()}</td>
                <td>{$company->getStatus()}</td>
                <td>{$company->getAccountsDue()}</td>
                <td>{$company->getReturnsDue()}</td>
                <td>
                    {if !$company->hasServices()}
                    <span>---</span>
                    {else}
                        {if $company->isCustomerActionRequired()}
                            <i class="fa fa-exclamation-circle midfont red"></i>
                        {else}
                            <i class="fa fa-check-circle midfont green1"></i>
                        {/if}
                        <span class="font13"><a href="{$this->router->link(MyServicesControler::PAGE_SERVICES)}?companyId={$company->getId()}#company={$company->getId()}">{$company->getServicesActionStatus()}</a></span>
                    {/if}
                </td>
            </tr>
            {/foreach}

        </tbody>
    </table>
    {$formHelper->widget($form['exportCsv']) nofilter}
{$formHelper->end($form) nofilter}
<p class="txtright txtbold">Key: <span class="red3">Overdue</span> / <span class="orange">Due in 2 months</span></p>
<script type="text/javascript">
    cms.customerCompanies.init({$perPage}, {$companiesCount}, '{$this->router->link(CompaniesCustomerControler::PAGE_FILTER)}', '{$this->router->link(CUSummaryControler::SUMMARY_PAGE)}', '{$this->router->link(MyServicesControler::PAGE_SERVICES)}');
</script>

{else}
    <div>No incorporated companies found</div>
{/if}