{include file="@header.tpl"}

<div>
    {if isset($syncAllCompaniesErrorMessages)}
        <div class="flash error" style="margin:10px 0px;width:auto;">
            <p>Companies updated but the following companies could not be synced:</p>
            <ul>
                {foreach from=$syncAllCompaniesErrorMessages key="key" item="company" name="f1"}
                    <li>{$company.companyName} - {$company.error}</li> 
                {/foreach}
            </ul>
        </div>
    {/if}
    <h1>My Companies</h1>
    {include file="CompaniesCustomer/notincorporated.tpl"}
    {include file="CompaniesCustomer/incorporated.tpl"}
    <div class="clear"></div>
</div>

{include file="@footer.tpl"}
