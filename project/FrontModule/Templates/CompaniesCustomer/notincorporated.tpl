<h2 class="mtop20">Not Incorporated</h2>
{if !empty($notIncorporated)}
    <table id="not-incorporated-companies" class="display data-table">
        <thead>
        <tr>
            <td><strong>Company Name</strong></td>
            <td align="center"><strong>Date of last submission</strong></td>
            <td><strong>Status</strong></td>
        </tr>
        </thead>
        <tbody>
        {foreach from=$notIncorporated key="key" item="company" name="f1"}
            <tr>
                <td><a href="{$company.link}" title="Company ({$key})">{$company.company_name}</a></td>
                <td align="center">{$company.last_submission_date}</td>
                <td class="center">{$company.submission_status}</td>
            </tr>
        {/foreach}
        </tbody>
    </table>
    <script type="text/javascript">
        $('#not-incorporated-companies').dataTable({
            "lengthChange": false,
            "paginate": false
        });
    </script>
{else}
    No not incorporated companies found
{/if}
