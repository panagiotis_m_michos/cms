{include file="@header.tpl"}

{capture name="content"}

	{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
	{$text nofilter}

	{* FORM *}	
	{$form nofilter}
{/capture}


{include file='@blocks/defaultTemplateBlock.tpl' content=$smarty.capture.content}
{include file='@blocks/rightColumn.tpl'}
{include file="@footer.tpl"}