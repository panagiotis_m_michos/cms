{include file="@header.tpl"}

<div class="mleft20 mright20">
    {*
    <div class="flash info2" style="margin-left: 0; width: 750px;">
    Important Update 03/11/2010 @ 10:30 - Companies House's systems have temporarily gone down. This means that at this time we are unable to submit your company for incorporation. They have advised us that the problem should be resolved shortly and we will update you as soon as we hear more. We sincerely apologise for the inconvenience but unfortunately this is out of our control.</div>
    *}

    {if $visibleTitle}
        <h1>{$title}</h1>
    {/if}


    {* REQUIRED ACTIONS *}
    {if !empty($lastSubmissions)}  
        <div class="nwflash nwerror">
            <strong>Your action is required:</strong> Click your company names below to complete your setup
        </div>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="display data-table dataTable no-footer" style="margin-bottom: 20px; border:1px solid #a94442; border-top: 0px;">
            <col>
            <col width="180">
            <col width="80">
            <col width="70">
            <tr class="odd">
                <td><strong>Company Name</strong></td>
                <td><strong>Type</strong></td>
                <td align="center"><strong>Status</strong></td>
                <td align="center"><strong>Action</strong></td>
            </tr>
            {foreach from=$lastSubmissions item="submission" name="f1"}
                {if !$submission.deleted}
                    <tr{if $smarty.foreach.f1.iteration is even} class="odd"{/if}>
                        <td>
                            {if $submission.link != false}
                                {if $submission.form_identifier == 'AnnualReturn'}
                                    <a href="{$submission.link}">{$submission.company_name}</a>
                                {elseif $submission.form_identifier == 'ChangeOfName'}
                                    <a href="{$submission.link}">{$submission.company_name}</a>
                                {else}
                                    <a href="{$submission.link}">{$submission.company_name}</a>
                                {/if}
                            {/if}
                        </td>
                        <td>{$submission.form_identifier}</td>
                        <td align="center">{$submission.response}</td>
                        <td align="center">
                            {if $submission.link != false}
                                {if $submission.form_identifier == 'AnnualReturn'}
                                    <a href="{$submission.link}">Continue</a>
                                {elseif $submission.form_identifier == 'ChangeOfName'}
                                    <a href="{$submission.link}">Continue</a>
                                {else}
                                    <a href="{$submission.link}">Continue</a>
                                {/if}
                            {/if}
                        </td>
                    </tr>
                {/if}
            {/foreach}
        </table>
    {/if}


    {* ACTIONS *}
    <div class="dashbd">
        <a href="{$this->router->link("CompaniesCustomerControler::COMPANIES_PAGE")}" class="bg-grey1 inlineblock mbottom20">
            <h3>My Companies</h3>
            <p class="">Get access to (and change) your company information.</p>
        </a>

        {if $customer->isWholesale()}
            <a href="{url route="professional_landing_page"}" class="bg-grey1 inlineblock mbottom20">
                <h3>Form a Company</h3>
                <p class="">Purchase and Form a Limited Company.<br />&nbsp;</p>
            </a>
        {/if}

        <a href="{$this->router->link("MyDetailsControler::MY_DETAILS_PAGE")}" class="bg-grey1 inlineblock mbottom20">
            <h3>My Details</h3>
            <p class="">Update your personal details.<br />&nbsp;</p>
        </a>
            
        <a href="{$this->router->link("CompanyImportControler::COMPANY_IMPORT_PAGE")}" class="bg-grey1 inlineblock mbottom20">
            <h3>Import a company</h3>
            <p class="">Import your existing company into our management portal.</p>
        </a>

        <a href="{$this->router->link("WHCreditControler::ADD_CREDIT_PAGE")}" class="bg-grey1 inlineblock mbottom20">
            <h3>My Credit</h3>
            <p class="">Add credit to your account.<br />
                Available credit: &pound;{$customer->credit|string_format:"%.2f"}
            </p>
        </a>

        {if $customer->isWholesale()}
            <a href="{$this->router->link(130, 'add=666')}" class="bg-grey1 inlineblock mbottom20">
                <h3>Confirmation Statement - DIY</h3>
                <p class="">Purchase & file your CS for any of the companies in your account</p>
            </a>
        {/if}

        <a href="{$this->router->link("CustomerOrdersControler::ORDERS_PAGE")}" class="bg-grey1 inlineblock mbottom20">
            <h3>My Order History</h3>
            <p class="">View your order history and access invoices for all your payments.</p>
        </a>
            
        <a href="{$this->router->link("ManagePaymentControler::MANAGE_PAYMENT_PAGE")}" class="bg-grey1 inlineblock mbottom20">
            <h3>Payment Methods</h3>
            <p class="">Manage your Credit and Debit card<br />details.</p>
        </a>

        <a href="{$this->router->link("DashboardCustomerControler::STATUTORY_FORMS_PAGE")}" class="bg-grey1 inlineblock mbottom20">
            <h3>Statutory Forms</h3>
            <p class="">Useful templates and forms for your statutory requirements.</p>
        </a>

        <a href="{$this->router->link("DashboardCustomerControler::BUSINESS_LIBRARY_PAGE")}" class="bg-grey1 inlineblock mbottom20">
            <h3>Business Library</h3>
            <p class="">Glossary, FAQs & Post Formation help.<br />&nbsp;</p>
        </a>
    </div>
            
    {if $customer->isRetail()}
        <a href="{$this->router->link(140)}" class="txtdnone displayblock dashbd-banner mbottom20">
            <h2 class="blue4 txtbold extrafont ptop20">Get your business adventure<br />off to a great start</h2>
            <p class="grey4 txtnormal font15 pbtm20">Special offers and support from our trusted partners, carefully selected to<br />help start and grow your business.</p>
            <span class="dashbd-moreinfo orange-submit">Learn More</span>
        </a>
    {/if}
</div>

<div class="clear"></div>

{include file="@footer.tpl"}
