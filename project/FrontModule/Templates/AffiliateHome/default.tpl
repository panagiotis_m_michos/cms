{include file="@AffiliateHeader.tpl"}

{* SEARCH FORM *}
<h2>Is your company name available? Search NOW!</h2>
{$searchForm}

{* LOGIN FORM *}
{if !isset($customer)}
	<h2>or Login</h2>
	{$loginForm}
{/if}

{include file="@AffiliateFooter.tpl"}