  {assign var="topText" value=[109 =>"Basic Company <br />Package",110=>"Printed Certificate of Incorporation",111=>"Prestigious <br />Registered Office",112=>"Professionally prepared Annual Return",113=>"Professionally prepared Annual Return",279=>"Company Register, <br /> VAT Registration"]}
  {assign var="midText" value=[109 =>"Company <br /> Administration Portal",110=>"Done-For-You <br />Company Support",111=>"No Junk Mail",112=>"Prestigious <br />Registered Office",113=>"Prestigious <br />Registered Office",279=>"Annual Return & Registered Office"]}
  {assign var="bottomText" value=[109 =>NULL,110=>NULL,111=>"Protects home <br /> address privacy",112=>NULL,113=>2,279=>3]}
  {assign var="vouchersPrice" value=[109 =>30,110=>30,111=>40,112=>50,113=>50,279=>50]}

<div id="matrix-page"> 
    <div style="width: 890px;margin: auto;border: 0px solid blue;margin-top: 10px;">
        <table class="matrx-table">
            {* MATRIX HEAD *}
            <tr>
                {section name=pakage loop=$availablePakages}
                    {if $availablePakages[pakage.index] == 111}
                        <td class="matrix-table-head-up-silver pakageId{$availablePakages[pakage.index]}">
                            <div class="ribbon"></div>
                        </td>
                    {else}
                        <td class="matrix-table-head-up pakageId{$availablePakages[pakage.index]}"></td>
                    {/if}
                {/section}    
            </tr>
            <tr>
                {section name=pakage loop=$availablePakages}
                    <td class="{if $availablePakages[pakage.index] == 111} matrix-table-head-silver {else} matrix-table-head{/if} {if $smarty.section.pakage.last && $availablePakages[pakage.index]<>111} matrix-last-pakage  {/if} pakageId{$availablePakages[pakage.index]} wizard-border{$availablePakages[pakage.index]} wizard-border-top{$availablePakages[pakage.index]}" {if isset($availablePakages[pakage.index_prev]) && $availablePakages[pakage.index_prev] == 111} style="border-left:none;"{/if}>
                        {assign  var=pakageId  value=$availablePakages[pakage.index]}
                        <div class="matrix-package-name {if $availablePakages[pakage.index] == 111}silver-black-color silver-top-shadow{/if}"> {$packages.$pakageId->page->title nofilter}</div>
                        <div class="matrix-package-text {if $availablePakages[pakage.index] == 111}silver-black-color{/if}" > {$topText.$pakageId nofilter} </div>
                        <div class="matrix-package-text matrix-last-top {if $availablePakages[pakage.index] == 111}silver-black-color{/if}"> {$midText.$pakageId nofilter} </div>
                        <div class="matrix-package-text-price" style="padding-bottom: 10px;">
                            {if $bottomText.$pakageId <> NULL && $bottomText.$pakageId > 0} 
                                <div class="matrix-year-package" style="">{$bottomText.$pakageId nofilter} {if $bottomText.$pakageId >1}YEARS{else}YEAR{/if}</div>
                            {else}
                                <div class="matrix-year-text-package">{$bottomText.$pakageId nofilter}</div> 
                            {/if} 
                        </div>
                        {if $availablePakages[pakage.index] == 111}
                            <div class="matrix-package-price matrix-price-size" style="color: #000;margin-top: 25px;padding-left:13px;">Value: <span style="text-decoration:line-through;">&pound;{$packages.$pakageId->productValue}</span></div>
                            <div class="matrix-package-price matrix-price-size" style="color: #000; padding-left:13px;">Price: <span style="color:#e41d1d; font-size: 17px;">&pound;{$packages.$pakageId->price}</span> <span style="font-size: 10px;">+VAT</span></div>
                            <div class="matrix-package-price matrix-price-size" style="color: #000; padding-left:13px;margin-bottom: 10px;">You Save: <span style="color: #e41d1d;font-weight: normal;">&pound;{$packages.$pakageId->saving}.00</span></div>
                            <a href="{$packages.$pakageId->getLink()}{if $noCompany}&no=1{else}&no=2{/if}" class="matrix-btn-silver padding-bottom">More Info</a>

                        {else}    
                            <div class="package-separator"></div>
                            <div class="matrix-package-price matrix-price-size" >Value: <span style="text-decoration:line-through;">&pound;{$packages.$pakageId->productValue}</span></div>
                            <div class="matrix-package-price matrix-price-size" >Price: <span style="color:#e41d1d; font-size: 17px;">&pound;{$packages.$pakageId->price}</span> <span style="font-size: 10px;">+VAT</span></div>
                            <div class="matrix-package-price matrix-price-size" style="margin-bottom: 10px;">You Save: <span style="color: #e41d1d;font-weight: normal;">&pound;{$packages.$pakageId->saving}.00</span></div>
                            <a href="{$packages.$pakageId->getLink()}{if $noCompany}&no=1{else}&no=2{/if}" class="matrix-btn padding-bottom">More Info</a>
                        {/if}
                    </td>
                {/section}    
            </tr>
            <tr>
                <td colspan="6" class="pakage-line-separator">
                    <p>Summary of Package Features <span>(rollover or click for details)</span></p>
                </td>
            </tr>

            {* MATRIX MIDDLE*}
            {if $packageService}
                {foreach from=$packageService name="packageService" key="packageServiceId" item="packageServiceItem"}
                    {assign var="availableForView" value=$packageServiceItem.serviceAvailableForPakage|@json_decode}
                    {assign var="matrixPakageNrOfYears" value=$packageServiceItem.matrixPakageNrOfYears|@json_decode:true}
                    <tr>

                        {section name=pakage loop=$availablePakages}
                            {assign var=pakageNameId value=$availablePakages[pakage.index]}
                            {if $availablePakages[pakage.index] == 111}
                                <td class="matrix-table-midd-silver pakageId{$availablePakages[pakage.index]} wizard-border{$availablePakages[pakage.index]}" >
                                    {if isset($availableForView) && in_array($availablePakages[pakage.index],$availableForView)}
                                        <div  class="pakage-info-p-silver"><a href="#{$packageServiceId}"  title="{$packageServiceItem.packageTooltipText}" {if $packageServiceItem.packageTooltipText}rel=""{/if}>{$packageServiceItem.summaryPakageName nofilter}</a> </div>
                                        {if isset($matrixPakageNrOfYears.$pakageNameId) && $matrixPakageNrOfYears.$pakageNameId >0}
                                            <div class="matrix-year-package" >{$matrixPakageNrOfYears.$pakageNameId nofilter} {if $matrixPakageNrOfYears.$pakageNameId >1}YEARS{else}YEAR{/if}</div>
                                        {/if}
                                    {else}
                                        <div > &nbsp;</div>
                                    {/if}
                                </td>
                            {else}
                                <td class="matrix-head-info-pakages {if $smarty.section.pakage.last } matrix-last-pakage{/if} pakageId{$availablePakages[pakage.index]} wizard-border{$availablePakages[pakage.index]}" {if isset($availablePakages[pakage.index_prev]) && $availablePakages[pakage.index_prev] == 111 } style="border: none;" {/if}>
                                    {if isset($availableForView) && in_array($availablePakages[pakage.index],$availableForView)}
                                        <div class="pakage-info-p"  {if $smarty.foreach.packageService.last} style="padding-bottom: 10px;"{/if}> <a href="#{$packageServiceId}" title="{$packageServiceItem.packageTooltipText}" {if $packageServiceItem.packageTooltipText}rel=""{/if}>{$packageServiceItem.summaryPakageName nofilter nofilter}</a> </div>
                                        {if isset($matrixPakageNrOfYears.$pakageNameId) && $matrixPakageNrOfYears.$pakageNameId >0}
                                            <div class="matrix-year-package">{$matrixPakageNrOfYears.$pakageNameId nofilter} {if $matrixPakageNrOfYears.$pakageNameId >1}YEARS{else}YEAR{/if}</div>
                                        {/if}
                                    {else}
                                        <div > &nbsp;</div>
                                    {/if}
                                </td>
                            {/if}    

                        {/section}

                    </tr>

                {/foreach}        
            {/if}
            {* MATRIX PACKAGES SEPARATOR*}
            <tr>
                <td colspan="6" class="pakage-line-separator">
                    <div class="matrix-separator">
                        <div class="matrix-left-separator"></div>
                        <div class="matrix-separator-text" style=""><strong>FREE OFFERS & VOUCHERS TO HELP YOUR COMPANY SUCCEED</strong> (ROLLOVER OR CLICK FOR DETAILS)</div>
                        <div class="matrix-right-separator"></div>
                        <div class="clear"></div>
                    </div>
                </td>
            </tr>
            <tr>

                {foreach from=$availablePakages name=pakage key="id" item="pakageId"}
                    <td style="background: #34A0EB{if $smarty.foreach.pakage.first}; border-left: 1px solid #34A0EB {/if}{if $smarty.foreach.pakage.last};border-right: 1px solid #34A0EB{/if}"><p style="text-align: center; color: white;font-size: 17px; padding: 4px;font-weight: normal;">{$packages.$pakageId->page->title}</p></td>
                {/foreach}    
            </tr>

            {* MATRIX OFFERS*}
            {if $allPackageOffers}
                {foreach from=$allPackageOffers name="packageService" key="packageServiceId" item="packageServiceItem"}
                    {assign var="availableForView" value=$packageServiceItem.serviceAvailableForPakage|@json_decode}
                    {assign var="matrixPakageNrOfYears" value=$packageServiceItem.matrixPakageNrOfYears|@json_decode:true}
                    <tr>

                        {section name=pakage loop=$availablePakages}
                            {assign var=pakageNameId value=$availablePakages[pakage.index]}
                            {if $availablePakages[pakage.index] == 111}
                                <td class="matrix-table-midd-silver pakageId{$availablePakages[pakage.index]} wizard-border{$availablePakages[pakage.index]}"  >
                                    {if isset($availableForView) && in_array($availablePakages[pakage.index],$availableForView)}
                                        <div  class="pakage-info-p-silver"><a href="#{$packageServiceId}"  title="{$packageServiceItem.packageTooltipText}" {if $packageServiceItem.packageTooltipText}rel=""{/if}>{$packageServiceItem.summaryPakageName nofilter}</a> </div>
                                        {if isset($matrixPakageNrOfYears.$pakageNameId) && $matrixPakageNrOfYears.$pakageNameId >0}
                                            <div class="matrix-year-package">{$matrixPakageNrOfYears.$pakageNameId nofilter} {if $matrixPakageNrOfYears.$pakageNameId >1}YEARS{else}YEAR{/if}</div>
                                        {/if}
                                    {else}
                                        <div > &nbsp;</div>
                                    {/if}
                                </td>
                                </div>
                            {else}
                                <td class="matrix-head-info-pakages {if $smarty.section.pakage.last} matrix-last-pakage{/if} pakageId{$availablePakages[pakage.index]} wizard-border{$availablePakages[pakage.index]} " {if isset($availablePakages[pakage.index_prev]) && $availablePakages[pakage.index_prev] == 111 } style="border-left:none;" {/if}>
                                    {if isset($availableForView) && in_array($availablePakages[pakage.index],$availableForView)}
                                        <div class="pakage-info-p"  {if $smarty.foreach.packageService.last} style="padding-bottom: 10px;"{/if}> <a href="#{$packageServiceId}" title="{$packageServiceItem.packageTooltipText}" {if $packageServiceItem.packageTooltipText}rel=""{/if}>{$packageServiceItem.summaryPakageName nofilter}</a> </div>
                                        {if isset($matrixPakageNrOfYears.$pakageNameId) && $matrixPakageNrOfYears.$pakageNameId >0}
                                            <div class="matrix-year-package">{$matrixPakageNrOfYears.$pakageNameId nofilter} {if $matrixPakageNrOfYears.$pakageNameId >1}YEARS{else}YEAR{/if}</div>
                                        {/if}
                                    {else}
                                        <div > &nbsp;</div>
                                    {/if}
                                </td>
                            {/if}    

                        {/section}

                    </tr>

                {/foreach}     
            {/if}


            {*MATRIX BOTTOM*}

            <tr>
                {section name=pakage loop=$availablePakages}
                    <td class="{if $availablePakages[pakage.index] == 111} matrix-table-bottom-silver {else} matrix-table-bottom{/if} {if $smarty.section.pakage.last && $availablePakages[pakage.index]<>111} matrix-last-pakage{/if} pakageId{$availablePakages[pakage.index]} wizard-border{$availablePakages[pakage.index]} wizard-border-bottom{$availablePakages[pakage.index]}" {if isset($availablePakages[pakage.index_prev]) && $availablePakages[pakage.index_prev] == 111} style="border-left:none;"{/if}>
                        {assign  var=pakageId  value=$availablePakages[pakage.index]}

                        {if $availablePakages[pakage.index] == 111}
                            <div style="width: 146px; border-top:1px solid #bce2f6;height: 1px;margin: auto;margin-bottom: 5px;margin-top: -2px;"></div>
                            <div class="matrix-package-price matrix-price-size" style="color: #000;padding-left:13px;">Value: <span style="text-decoration:line-through;">&pound;{$packages.$pakageId->productValue}</span></div>
                            <div class="matrix-package-price matrix-price-size" style="color: #000; padding-left:13px;">Price: <span style="color:#e41d1d; font-size: 17px;">&pound;{$packages.$pakageId->price}</span> <span style="font-size: 10px;">+VAT</span></div>
                            <div class="matrix-package-price matrix-price-size" style="color: #000; padding-left:13px;margin-bottom: 10px;">You Save: <span style="color: #e41d1d;font-weight: normal;">&pound;{$packages.$pakageId->saving}.00</span></div>
                            <a href="{$packages.$pakageId->getLink()}{if $noCompany}&no=1{else}&no=2{/if}" class="matrix-btn-silver padding-bottom">More Info</a>

                        {else}
                            <div style="width: 146px; border-top: 1px solid #bce2f6;height: 1px;margin: auto;margin-bottom: 5px;"></div>
                            <div class="matrix-package-price matrix-price-size" >Value: <span style="text-decoration:line-through;">&pound;{$packages.$pakageId->productValue}</span></div>
                            <div class="matrix-package-price matrix-price-size" >Price: <span style="color:#e41d1d; font-size: 17px;">&pound;{$packages.$pakageId->price}</span> <span style="font-size: 10px;">+VAT</span></div>
                            <div class="matrix-package-price matrix-price-size" style="margin-bottom: 10px;">You Save: <span style="color: #e41d1d;font-weight: normal;">&pound;{$packages.$pakageId->saving}.00</span></div>
                            <a href="{$packages.$pakageId->getLink()}{if $noCompany}&no=1{else}&no=2{/if}" class="matrix-btn padding-bottom">More Info</a>
                        {/if}    
                    </td>
                {/section}
            </tr>

            <tr>
                {section name=pakage loop=$availablePakages}
                    <td class=" {if $availablePakages[pakage.index] == 111} matrix-table-bottom-down-silver {else} matrix-table-bottom-down {/if} pakageId{$availablePakages[pakage.index]}"></td>
                {/section}    
            </tr>
        </table>
    </div>
    <div  class="text-separator-up" style="">We hate spam too and hold your contact details in strict confidence.<br />We only share your details with partners when you request their offer or voucher.</div>
    <div style="width: 892px;text-align: right;margin: auto;margin-bottom: 5px;color:#666666; font-size: 14px; "><strong>Any questions?</strong> Get free, friendly advice from our <br /> experts. Call us now at 0207 608 5500 </div>
        {* PAKAGE DETAILS*}
    <div style="width: 920px;margin: auto;margin-bottom: 30px;">
        <table class="matrx-table-offers persist-area" >
            <thead class="persist-header">
                <tr>
                    <td class="matrx-table-offers-info-head ">&nbsp;<a name="1199" id="1199"></a></td>
                    {section name=pakage loop=$availablePakages}
                        <td class=" {if $availablePakages[pakage.index] == 111} matrx-table-offers-silver-head {else} matrx-table-offers-head {/if} "></td>
                    {/section}    
                </tr>
                <tr >
                    <td class="matrx-table-offers-info-body">Package Feature Details</td>
                    {section name=pakage loop=$availablePakages}
                        <td class="{if $availablePakages[pakage.index] == 111} matrx-table-offers-body-silver {else} matrx-table-offers-body{/if} {if $smarty.section.pakage.last && $availablePakages[pakage.index]<>111} matrix-last-pakage{/if}" {if isset($availablePakages[pakage.index_prev]) && $availablePakages[pakage.index_prev] == 111} style="border-left:none;"{/if}>
                            {assign  var=pakageId  value=$availablePakages[pakage.index]}
                            {if $availablePakages[pakage.index] == 111}
                                <div class="matrix-package-name-offer matrix-package-name-offer-silver " > {$packages.$pakageId->page->title|strtoupper} {if $availablePakages[pakage.index] == 111}<span style="font-size: 12px;color:#818283;text-shadow: none;">Recommended</span>{/if}</div>

                            {else}
                                <div class="matrix-package-name-offer" > {$packages.$pakageId->page->title}</div>
                            {/if}
                        </td>
                    {/section}
                </tr>
            </thead>
            {* OFFER CONTENT*}

           {if $packageService}
                {foreach from=$packageService name="packageService" key="packageServiceId" item="packageServiceItem"}
                    {assign var="availableForView" value=$packageServiceItem.serviceAvailableForPakage|@json_decode}
                    {assign var="matrixPakageNrOfYears" value=$packageServiceItem.matrixPakageNrOfYears|@json_decode:true}
                    {assign var="firstText" value=$packageServiceItem.firstText|@json_decode:true}
                    {if $smarty.foreach.packageService.index%2}
                        {assign var="bgColor" value=0}
                    {else}
                        {assign var="bgColor" value=1}
                    {/if}
                    <tr>

                        <td class="matrx-table-offers-info-content {if $smarty.foreach.packageService.index%2}evenRowColumn{else}oddRowColumn{/if}">
                            <div class="go-to-top"><div>{*<a name="{$packageServiceId}" id="{$packageServiceId}" style='position:relative; top:-90px;'></a> *}</div><a href="#top">Back to top</a></div>
                            <div style="padding: 10px;">{$packageServiceItem.page->getLngText() nofilter}</div>
                        </td>
                        {section name=pakage loop=$availablePakages}

                            {assign var=pakageNameId value=$availablePakages[pakage.index]}
                            {if isset($availableForView) && in_array($availablePakages[pakage.index],$availableForView)}

                                {if $availablePakages[pakage.index] == 111}
                                    <td class="  {if $bgColor} matrx-table-offers-content-silver{else} matrx-table-offers-content-silver-odd{/if}" > 

                                        {if $packageServiceItem.tickImage}
                                            <div class="{if $bgColor}matrix-pakage-tick-silver{else} matrix-pakage-tick-silver-odd{/if}">&nbsp</div>
                                        {else}
                                            <div style="text-align: center;font-size: 16px;">{if $matrixPakageNrOfYears.$pakageNameId > 0} <strong> {$matrixPakageNrOfYears.$pakageNameId} {if $matrixPakageNrOfYears.$pakageNameId >1} years{else}year{/if}</strong>{/if}</div> 
                                        {if $firstText.$pakageNameId}<div style="text-align: center;color: #666; font-size: 12px;margin-top: 10px;">{$firstText.$pakageNameId}</div>{/if}
                                    {/if}
                                </td>
                            {else}
                                <td class="matrx-table-offers-content {if $bgColor} oddRow{else} evenRow{/if} {if $smarty.section.pakage.last} matrix-last-pakage{/if}" {if isset($availablePakages[pakage.index_prev]) && $availablePakages[pakage.index_prev] == 111 } style="border-left:none;" {/if}> 
                                    {if $packageServiceItem.tickImage}
                                        <div class="{if $bgColor} matrix-pakage-tick{else} matrix-pakage-tick-odd{/if}">&nbsp</div>
                                    {else}
                                        <div style="text-align: center;font-size: 16px;">{if $matrixPakageNrOfYears.$pakageNameId > 0} <strong> {$matrixPakageNrOfYears.$pakageNameId} {if $matrixPakageNrOfYears.$pakageNameId >1} years{else}year{/if}</strong>{/if}</div> 
                                    {if $firstText.$pakageNameId}<div style="text-align: center;color: #666; font-size: 12px;margin-top: 10px;">{$firstText.$pakageNameId}</div>{/if}
                                {/if}
                            </td>
                        {/if}
                    {else}
                        {if $availablePakages[pakage.index] == 111}
                            <td class="{if $bgColor} matrx-table-offers-content-silver{else} matrx-table-offers-content-silver-odd{/if}"> &nbsp;</td>
                        {else}
                            <td class="matrx-table-offers-content {if $bgColor} oddRow{else} evenRow{/if} {if $smarty.section.pakage.last} matrix-last-pakage{/if}" {if $availablePakages[pakage.index_prev] == 111 } style="border-left:none;" {/if}> &nbsp;</td>
                        {/if}
                    {/if}
                {/section}
            </tr>
        {/foreach} 

    {/if}


    {*PACKAGE BOTTOM*}

    <tr>
        <td class="matrx-offers-info-bottom-body">&nbsp;</td>
        {section name=pakage loop=$availablePakages}
            <td class="{if $availablePakages[pakage.index] == 111} matrx-table-offers-body-bottom-silver {else} matrx-table-offers-body-bottom{/if} {if $smarty.section.pakage.last && $availablePakages[pakage.index]<>111} matrix-last-pakage{/if}" {if isset($availablePakages[pakage.index_prev]) && $availablePakages[pakage.index_prev] == 111} style="border-left:none;"{/if}>
                {assign  var=pakageId  value=$availablePakages[pakage.index]}

                {if $availablePakages[pakage.index] == 111}

                    <div class="matrix-value-silver-small" >Value: <span style="text-decoration:line-through;">&pound;{$packages.$pakageId->productValue}</span></div>
                    <div class="matrix-price-silver-small" ><span style="color:#e41d1d; font-size: 17px;">&pound;{$packages.$pakageId->price}</span> <span style="font-size: 10px;">+VAT</span></div>
                    <div class="matrix-save-silver-small" >Save: &nbsp;<span style="color: #e41d1d;font-weight: normal;">&pound;{$packages.$pakageId->saving}.00</span></div>
                    <a href="{$packages.$pakageId->getLink()}{if $noCompany}&no=1{else}&no=2{/if}" class="matrix-btn-silver-small-up" style="margin-top: 3px;">More Info</a>

                {else}

                    <div class="matrix-value-small" >Value: <span style="text-decoration:line-through;">&pound;{$packages.$pakageId->productValue}</span></div>
                    <div class="matrix-price-small" ><span style="color:#e41d1d; font-size: 17px;">&pound;{$packages.$pakageId->price}</span> <span style="font-size: 10px;">+VAT</span></div>
                    <div class="matrix-save-small" style="margin-bottom: 2px;">Save: &nbsp;<span style="color: #e41d1d;font-weight: normal;">&pound;{$packages.$pakageId->saving}.00</span></div>
                    <a href="{$packages.$pakageId->getLink()}{if $noCompany}&no=1{else}&no=2{/if}" class="matrix-btn-small-up" style="margin-bottom: 5px;">More Info</a>
                {/if}    
            </td>
        {/section}
    </tr>


    <tr>
        <td class="matrx-offers-info-bottom">&nbsp;</td>
        {section name=pakage loop=$availablePakages}
            <td class=" {if $availablePakages[pakage.index] == 111} matrx-offers-bottom-silver {else} matrx-offers-bottom {/if} "></td>
        {/section}    
    </tr>
</table>
</div>

{* OFFERS *}
<div style="width: 920px;margin: auto;margin-top: 30px;margin-bottom: 30px;">
    <table class="matrx-table-offers persist-area" >
        <thead class="persist-header">
            <tr>
                <td class="matrx-table-offers-info-head">&nbsp;<a name="1190" id="1190"></a></td>
                {section name=pakage loop=$availablePakages}
                    <td class=" {if $availablePakages[pakage.index] == 111} matrx-table-offers-silver-head {else} matrx-table-offers-head {/if} "></td>
                {/section}    
            </tr>
            <tr>
                <td class="matrx-table-offers-info-body">Offers & Voucher Details</td>
                {section name=pakage loop=$availablePakages}
                    <td class="{if $availablePakages[pakage.index] == 111} matrx-table-offers-body-silver {else} matrx-table-offers-body{/if} {if $smarty.section.pakage.last && $availablePakages[pakage.index]<>111} matrix-last-pakage{/if}" {if isset($availablePakages[pakage.index_prev]) && $availablePakages[pakage.index_prev] == 111} style="border-left:none;"{/if}>
                        {assign  var=pakageId  value=$availablePakages[pakage.index]}
                        {if $availablePakages[pakage.index] == 111}
                            <div class="matrix-package-name-offer matrix-package-name-offer-silver" > {$packages.$pakageId->page->title|strtoupper} {if $availablePakages[pakage.index] == 111}<span style="font-size: 12px;color:#818283">Recommended</span>{/if}</div>

                        {else}
                            <div class="matrix-package-name-offer" > {$packages.$pakageId->page->title}</div>
                        {/if}
                    </td>
                {/section}
            </tr>
        </thead>
        <tr>
            <td colspan="7" class="pakage-line-separator">
                <p>Offers & Vouchers to support your business</p>
            </td>
        </tr>


        {* OFFER CONTENT*}

        {if $allPackageOffers}
            {foreach from=$allPackageOffers name="packageService" key="packageServiceId" item="packageServiceItem"}
                {assign var="availableForView" value=$packageServiceItem.serviceAvailableForPakage|@json_decode}
                {assign var="matrixPakageNrOfYears" value=$packageServiceItem.matrixPakageNrOfYears|@json_decode:true}
                {assign var="firstText" value=$packageServiceItem.firstText|@json_decode:true}
                {if $smarty.foreach.packageService.index%2}
                    {assign var="bgColor" value=0}
                {else}
                    {assign var="bgColor" value=1}
                {/if}
                <tr>

                    <td class="matrx-table-offers-info-content {if $smarty.foreach.packageService.index%2}evenRowColumn{else}oddRowColumn{/if}">
                        <div class="go-to-top"><div>{*<a name="{$packageServiceId}" id="{$packageServiceId}" style='position:relative; top:-90px;'></a>*}</div><a href="#top">Back to top</a></div>
                        <div style="padding: 10px;">{$packageServiceItem.page->getLngText() nofilter}</div>
                    </td>
                    {section name=pakage loop=$availablePakages}

                        {assign var=pakageNameId value=$availablePakages[pakage.index]}
                        {if isset($availableForView) && in_array($availablePakages[pakage.index],$availableForView)}

                            {if $availablePakages[pakage.index] == 111}
                                <td class="  {if $bgColor} matrx-table-offers-content-silver{else} matrx-table-offers-content-silver-odd{/if}" > 

                                    {if $packageServiceItem.tickImage}
                                        <div class="{if $bgColor}matrix-pakage-tick-silver{else} matrix-pakage-tick-silver-odd{/if}">&nbsp</div>
                                    {else}
                                        {if $packageServiceId == 1191}
                                            <div style="text-align: center;font-size: 16px;"><strong>&pound;{$vouchersPrice.$pakageNameId}</strong><div style="text-align: center;color: #666; font-size: 12px;margin-top: 5px;">Voucher</div></div>
                                        {else}    
                                            <div style="text-align: center;font-size: 16px;">{if $matrixPakageNrOfYears.$pakageNameId > 0} <strong> {$matrixPakageNrOfYears.$pakageNameId} {if $matrixPakageNrOfYears.$pakageNameId >1} years{else}year{/if}</strong>{/if}</div> 
                                        {if $firstText.$pakageNameId}<div style="text-align: center;color: #666; font-size: 12px;margin-top: 10px;">{$firstText.$pakageNameId}</div>{/if}
                                    {/if}
                                {/if}
                            </td>
                        {else}
                            <td class="matrx-table-offers-content {if $bgColor} oddRow{else} evenRow{/if} {if $smarty.section.pakage.last} matrix-last-pakage{/if}" {if isset($availablePakages[pakage.index_prev]) && $availablePakages[pakage.index_prev] == 111 } style="border-left:none;" {/if}> 
                                {if $packageServiceItem.tickImage}
                                    <div class="{if $bgColor} matrix-pakage-tick{else} matrix-pakage-tick-odd{/if}">&nbsp</div>
                                {else}
                                    {if $packageServiceId == 1191}
                                        <div style="text-align: center;font-size: 16px;"><strong>&pound;{$vouchersPrice.$pakageNameId}</strong><div style="text-align: center;color: #666; font-size: 12px;margin-top: 5px;">Voucher</div></div>
                                    {else}    
                                        <div style="text-align: center;font-size: 16px;">{if $matrixPakageNrOfYears.$pakageNameId > 0} <strong> {$matrixPakageNrOfYears.$pakageNameId} {if $matrixPakageNrOfYears.$pakageNameId >1} years{else}year{/if}</strong>{/if}</div> 
                                    {if $firstText.$pakageNameId}<div style="text-align: center;color: #666; font-size: 12px;margin-top: 10px;">{$firstText.$pakageNameId}</div>{/if}
                                {/if}
                            {/if}
                        </td>
                    {/if}
                {else}
                    {if $availablePakages[pakage.index] == 111}
                        <td class="{if $bgColor} matrx-table-offers-content-silver{else} matrx-table-offers-content-silver-odd{/if}"> &nbsp;</td>
                    {else}
                        <td class="matrx-table-offers-content {if $bgColor} oddRow{else} evenRow{/if} {if $smarty.section.pakage.last} matrix-last-pakage{/if}" {if $availablePakages[pakage.index_prev] == 111 } style="border-left:none;" {/if}> &nbsp;</td>
                    {/if}
                {/if}
            {/section}
        </tr>
    {/foreach} 

{/if}    


{*OFFER BOTTOM*}

<tr>
    <td class="matrx-offers-info-bottom-body" style="vertical-align: top;"><p style="color: #666;font-size: 12px;">* for terms & conditions <a href="{$this->router->link("BusinessBankingControler::FAST_TRACKING_PAGE")}" >click here</a></p></td>
    {section name=pakage loop=$availablePakages}
        <td class="{if $availablePakages[pakage.index] == 111} matrx-table-offers-body-bottom-silver {else} matrx-table-offers-body-bottom{/if} {if $smarty.section.pakage.last && $availablePakages[pakage.index]<>111} matrix-last-pakage{/if}" {if isset($availablePakages[pakage.index_prev]) && $availablePakages[pakage.index_prev] == 111} style="border-left:none;"{/if}>
            {assign  var=pakageId  value=$availablePakages[pakage.index]}

            {if $availablePakages[pakage.index] == 111}

                    <div class="matrix-value-silver-small" >Value: <span style="text-decoration:line-through;">&pound;{$packages.$pakageId->productValue}</span></div>
                    <div class="matrix-price-silver-small" ><span style="color:#e41d1d; font-size: 17px;">&pound;{$packages.$pakageId->price}</span> <span style="font-size: 10px;">+VAT</span></div>
                    <div class="matrix-save-silver-small" >Save: &nbsp;<span style="color: #e41d1d;font-weight: normal;">&pound;{$packages.$pakageId->saving}.00</span></div>
                    <a href="{$packages.$pakageId->getLink()}{if $noCompany}&no=1{else}&no=2{/if}" class="matrix-btn-silver-small-up" style="margin-top: 3px;">More Info</a>

                {else}

                    <div class="matrix-value-small" >Value: <span style="text-decoration:line-through;">&pound;{$packages.$pakageId->productValue}</span></div>
                    <div class="matrix-price-small" ><span style="color:#e41d1d; font-size: 17px;">&pound;{$packages.$pakageId->price}</span> <span style="font-size: 10px;">+VAT</span></div>
                    <div class="matrix-save-small" style="margin-bottom: 2px;">Save: &nbsp;<span style="color: #e41d1d;font-weight: normal;">&pound;{$packages.$pakageId->saving}.00</span></div>
                    <a href="{$packages.$pakageId->getLink()}{if $noCompany}&no=1{else}&no=2{/if}" class="matrix-btn-small-up" style="margin-bottom: 5px;">More Info</a>
                {/if} 
        </td>
    {/section}
</tr>
<tr>
    <td class="matrx-offers-info-bottom">&nbsp;</td>
    {section name=pakage loop=$availablePakages}
        <td class=" {if $availablePakages[pakage.index] == 111} matrx-offers-bottom-silver {else} matrx-offers-bottom {/if} "></td>
    {/section}    
</tr>
</table>
</div>    
<div class="info-z-index">
    <img src="{$urlImgs}matrix/scroll-down.png" />
</div>
</div>

