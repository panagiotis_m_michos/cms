{literal}
<style>
    .html-box-under-guide {
        margin-bottom: 10px;
        height: auto; 
        border: 0px solid blue;
        background: none;
    }
    .html-box-under-guide p {
        margin:0;
        padding:0
    }
    
</style>
{/literal}
{if $this->node->getHasVisibleHtmlBox()}
    <div class="html-box-under-guide">	    
        {$this->node->getUnderGuideHtmlBox()}
    </div>     
{/if}