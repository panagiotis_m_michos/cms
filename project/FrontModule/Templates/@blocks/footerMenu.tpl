<div id="footer2014">
    <div id="fcolwrap">
        <div class="fcolumn">
            <strong>Made Simple Websites</strong><br/>
            <a href="https://www.madesimplegroup.com/" rel="nofollow">Made Simple Group</a><br/>
            <a href="https://www.londonpresence.com/" rel="nofollow">London Virtual Office</a><br/>
            <a href="https://www.companysearchesmadesimple.com/" rel="nofollow">Company Credit Check</a><br/>
            <a href="http://www.taxreturnsmadesimple.com/" rel="nofollow">Tax Returns</a><br/>
            <a href="https://www.businesstrainingmadesimple.co.uk/" rel="nofollow">Business Training</a>
        </div>
        <div class="fcolumn">
            <strong>Information & Assistance</strong><br/>
            <a href="{$this->router->link(242)}">About Us</a><br />
            <a href="{$this->router->link(473)}">Terms and Conditions</a><br/>
            <a href="{$this->router->link(772)}">Privacy Policy</a><br/>
            <a href="/support/?/Tickets/Submit/RenderForm/22">Contact Us</a><br/>
            <a href="/support/?/Knowledgebase/List">Knowledge Base</a><br/>
            <a href="/blog/">Blog</a><br/>
            <a href="/sitemap.html">Site Map</a>
        </div>
        <div class="fcolumn">
            <strong>Contact Us</strong><br/>
            <span>0207 608 5500</span><br/>
            <a href="mailto:theteam@companiesmadesimple.com">Email Us</a><br/>
            <span>145-157 St. John Street</span><br/>
            <span>London</span><br/>
            <span>EC1V 4PW</span><br/>
            <span>United Kingdom</span>
        </div>
        <div class="fcolumn">
            <strong>Connect With Us</strong>

            <div id="fsocialwrap">
                <span id="i-fb"><a href="http://www.facebook.com/MadeSimpleGroup"><img src="{$urlImgs}home2014/i-fb.png"
                                                                                       alt=""/></a></span>
                <span id="i-twitter"><a href="http://twitter.com/MadeSimpleGroup"><img
                                src="{$urlImgs}home2014/i-twitter.png" alt=""/></a></span>
                <span id="i-gplus"><a href="https://plus.google.com/112476242712086821280"><img
                                src="{$urlImgs}home2014/i-gplus.png" alt=""/></a></span>
            </div>
            <div>
                <a href="http://www.feefo.com/feefo/viewvendor.jsp?logon=www.madesimplegroup.com/companiesmadesimple"
                   onclick="window.open(this.href, 'Feefo', 'width=1000,height=600,scrollbars,resizable');
                        return false;">
                    <img alt="Feefo logo" border="0"
                         src="//www.feefo.com/feefo/feefologo.jsp?logon=www.madesimplegroup.com/companiesmadesimple&template=service-percentage-grey-100x100_en.png"
                         title="See what our customers say about us">
                </a>
            </div>
        </div>
    </div>
    <div id="fcopyright">
        <p>Companies Made Simple provides company formation services and is a division of the Made Simple Group Ltd</p>

        <p>Copyright © 2014 Made Simple Group - all rights reserved Company Number: 05104525 Vat Number: GB820956327</p>
    </div>
    <div id="fdisclaimer">
        <p>
            Companiesmadesimple.com is an online specialist in limited company formations covering everything necessary
            to facilitate the formation and/or registration of a company in the UK. All types of UK company formations
            can be catered for and everything necessary to set up a limited company in the UK, including electronic
            filing and all required documents, including the memorandum and articles of association are
            provided.Guaranteed same day formations, company registers, and other company formation ancillary products
            are available as well as a vast number of readymade companies. Our automated on-line formation is quick,
            simple and affordable enabling the immediate check of company names with Companies House. All formations are
            done electronically with our incorporation system. All costs include all necessary documents and filing fees
            at Companies House and the company formation process is usually complete within 3 hours.
        </p>
    </div>
</div>
