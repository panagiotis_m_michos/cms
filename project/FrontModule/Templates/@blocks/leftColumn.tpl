
{* DISABLE LEFT COLUMN *}
{if !$disabledLeftColumn}

    <div id="leftCol">

		{* TESTIMONIALS *}
		{capture name="testimonials"}

		<div class="box1">
        	<h3><a href="{url route="CustomersReviewsControler::REVIEWS_PAGE"}" style="color:white;text-decoration:none;">What our clients say...</a></h3>
			<div class="box1-foot nopright noptop">
            	<div class="h180 auto-over">
           			{$testimonials nofilter}
				</div>
          	</div>
		</div>
        {/capture}

        {* BLOG *}
        {capture name="blog"}
        	<div class="box1">
          		<h3>Blog - Latest articles</h3>
          		<div class="box1-foot nopright noptop">
            		<div class="h180 auto-over">
                        {if !empty($leftBlogPosts)}
                            {foreach $leftBlogPosts as $post}
                                <h4 class="blog">{$post->getTitle()}</h4>
                                <p class="blog">{$post->getLimitedDescription(200)|strip_tags} <a href="{$post->getLink()}">Read more...</a></p>
                            {/foreach}
                        {else}
                            <p><strong>No blog posts.</strong></p>
                        {/if}
					</div>
				</div>
			</div>
        {/capture}


        {* EMAIL SUPPORT *}
        {capture name="support"}

        <div class="free-support-homepage">
            <p><strong>FREE</strong> unlimited telephone and email support</p>
            <a href="http://support.companiesmadesimple.com/article/288-contacting-us" style="">Contact Us</a>
        </div>
        {/capture}

        {* HTML BLOCK *}
        {capture name="htmlBlock"}
        {if $leftMenuItems != false}
	        <div class="htmlBlock" style="width: 181px; margin-bottom: 10px;">
	       		{$leftMenuItems.htmlBlock nofilter}
	        </div>
        {/if}
        {/capture}

        {* DISPLAY MENU BASED ON PROPERTIES FOR LEFT MENU *}
        {if $leftMenuItems != false}
        	{foreach from=$leftMenuItems.items key="key" item="value"}
        		{if $value}
        			{$smarty.capture.$key nofilter}
        		{/if}
        	{/foreach}
        {else}
            {if $node->getId() != 919}
                {$smarty.capture.testimonials nofilter}
                {$smarty.capture.blog nofilter}
                {$smarty.capture.support nofilter}
                {$smarty.capture.htmlBlock nofilter}
            {/if}
        {/if}
      </div>
{/if}
