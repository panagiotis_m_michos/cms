<br /><br /><br /><br />
<div id="maincontent-full">

<div class="help-button" style="float: left; margin: 0 15px 0 0">
    <a href="#" class="help-icon">help</a>
    <em>This is your company name. Click on the change button to change your proposed company name.</em>
</div>

{if $company->getStatus() == "pending"}
<h5>{$companyName} {*{$companyType}*}</h5>
{else}
<h3>{$companyName} {*{$companyType}*}
    <a href="{$this->router->link("CFCompanyNameControler::COMPANY_NAME_PAGE","company_id=$companyId", "backlink=$backLink")}" style="font-weight: normal; font-size: 10px;">
        [change company name]
    </a>
</h3>
{/if}

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="dashtable"> 
  <tr> 
 	{foreach from=$tabs key="key" item="tab"}
	{if $tab.link} 
  	<td>
  		{if $currentTab==$key}
  		<div class="orangeBorder"></div>
  		<div class="orangetriangle"></div>
  		<strong class="orange">{$tab.title nofilter}</strong>
  		{else}
  		<div class="blackBorder"></div>
  		<div class="emptyhr"></div>  		  		
  		<a href="{$tab.link}">{$tab.title nofilter}</a>  		
  		{/if}
  	</td>
  	</td> 
  	{else}
  	<td>
	  	{if $currentTab==$key}
	  	<div class="orangeBorder"></div>
	  	<div class="orangetriangle"></div>
	  	<strong class="orange">{$tab.title nofilter}</strong>
	  	{else}
	  	<div class="greyBorder"></div>
  		<div class="emptyhr"></div>  		  		
  		<font style="color:#ccc;">{$tab.title nofilter}</font>
	  	{/if}
	 </td>
    {/if} 
    {/foreach}
  </tr> 
</table> 

{*
<ul id="navlist">
	{foreach from=$tabs key="key" item="tab"}
	{if $tab.link}
		<li{if $currentTab==$key} class="current"{/if}><a href="{$tab.link}">{$tab.title}</a></li>
	{else}
		<li{if $currentTab==$key} class="current"{/if}>{$tab.title}</li>
	{/if}
	{/foreach}
</ul>
*}

</div>