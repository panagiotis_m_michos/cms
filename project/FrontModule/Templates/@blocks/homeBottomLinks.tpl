<div id="bottomlinks">
    <div id="homemenu">
        <div class="mcol1">
            <h2><a href="{$this->router->link("102")}">Company Formation</a></h2>
            <div>
                <div>
                    <div>
                        <div>
                            <ul>
                                <li><a href="{$this->router->link("102")}">Company Formation Packages</a></li>
                            </ul>
                        </div>
                        <div>
                            <h3>Company Types</h3>
                            <ul>
                                <li><a href="{$this->router->link("72")}">UK Limited Company Formation</a></li>
                                <li><a href="{$this->router->link("74")}">Limited by Guarantee Company</a></li>
                                <li><a href="{$this->router->link("73")}">Limited by Shares Company</a></li>
                                <li><a href="{$this->router->link("76")}">Limited Liability Partnership</a></li>
                                <li><a href="{$this->router->link("75")}">Public Limited Company</a></li>
                                <li><a href="{$this->router->link("237")}">Flat Management Company</a></li>
                            </ul>
                        </div>
                    </div>
                    <div>
                        <div>
                            <h3>Other company services</h3>
                            <ul>
                                <li><a href="{$this->router->link("62")}">Offshore Formation</a></li>
                                <li><a href="{$this->router->link("239")}">Reserve a Company Name</a></li>
                                <li><a href="{$this->router->link("1288")}">Shelf Companies</a></li>
                                <li><a href="{$this->router->link("819")}">Sole Trader Start Up Pack</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mcol1">
            <h2><a href="{$this->router->link("67")}">How It Works</a></h2>
            <ul>
                <li>
                    <div>
                        <div>
                            <ul>
                                <li><a href="{$this->router->link("67")}">Overview</a></li>
                                <li><a href="{$this->router->link("972")}">Why Choose us?</a></li>
                                <li><a href="{$this->router->link("749")}">Reviews and Testimonials</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
            <h2><a href="{$this->router->link("1423")}">Additional Services</a></h2>
            <ul>
                <li>
                    <div>
                        <ul>
                            <li><a href="{$this->router->link("82")}">Renewals</a></li>
                        </ul>
                    </div>
                    <div>
                        <h3>Accountancy and Legal</h3>
                        <ul>
                            <li><a href="{$this->router->link("251")}">Accountancy Consultation</a></li>
                            <li><a href="{$this->router->link("672")}">Confirmation Statement</a></li>
                            <li><a href="{$this->router->link("368")}">Change of Accounting Reference Date</a></li>
                            <li><a href="{$this->router->link("845")}">Company Name Change Service</a></li>
                            <li><a href="{$this->router->link("257")}">Company Dissolution</a></li>
                            <li><a href="{$this->router->link("255")}">Company Tax Registration</a></li>
                            <li><a href="{$this->router->link("785")}">Dormant Company Accounts Service</a></li>
                            <li><a href="{$this->router->link("261")}">Share Services</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
        <div class="mcol1">
            <div>
                <div>
                    <h3>Office & Address Services</h3>
                    <ul>
                        <li><a href="{$this->router->link("557")}">Mail Forwarding</a></li>
                        <li><a href="{$this->router->link("259")}">Registered Office Service</a></li>
                        <li><a href="{$this->router->link("476")}">Service Address Service</a></li>
                        <li><a href="{$this->router->link("250")}">Virtual Office</a></li>
                    </ul>
                </div>
                <div>
                    <h3>Documents</h3>
                    <ul>
                        <li><a href="{$this->router->link("83")}">Apostilled Documents Service</a></li>
                        <li><a href="{$this->router->link("85")}">Certificate of Good Standing</a></li>
                        <li><a href="{$this->router->link("262")}">Supplementary Company Documents</a></li>
                    </ul>
                </div>
            </div>
            <div>
                <h3>Official Company Products</h3>
                <ul>
                    <li><a href="{$this->router->link("256")}">Company Pack (Register, Seal & Stamp)</a></li>
                    <li><a href="{$this->router->link("256")}">Company Register</a></li>
                    <li><a href="{$this->router->link("256")}">Company Seal</a></li>
                    <li><a href="{$this->router->link("256")}">Company Stamp</a></li>
                </ul>
            </div>
        </div>
        <div class="mcol1">
            <div>
                <h3>Partners and Affiliates</h3>
                <ul>
                    <li><a href="{$this->router->link("908")}">Affiliate Program</a></li>
                    <li><a href="{$this->router->link("1283")}">White Label Company</a></li>
                    <li><a href="/professional/">Formation</a></li>
                    <li><a href="{$this->router->link("240")}">Wholesale</a></li>
                </ul>
            </div>
            <div>
                <div>
                    <h3>Other</h3>
                    <ul>
                        <li><a href="{$this->router->link("928")}">Business Apps</a></li>
                        <li><a href="{$this->router->link("555")}">Guides</a></li>
                        <li><a href="{$this->router->link("573")}">Business Templates/Forms</a></li>
                    </ul>
                </div>
            </div>
            <h2><a href="http://support.companiesmadesimple.com">Help and Advice</a></h2>
            <ul>
                <li>
                    <div>
                        <ul>
                            <li><a href="http://support.companiesmadesimple.com/category/132-frequently-asked-questions">FAQs</a></li>
                            <li><a href="#open-zopim">Chat with us</a></li>
                            <li><a href="#open-beacon">Email us</a></li>
                            <li><a href="{$this->router->link("1614")}">Registering a company</a></li>
                            <li><a href="{$this->router->link("1612")}">Setting up a business</a></li>
                            <li><a href="{$this->router->link("1610")}">Register a business name</a></li>
                            <li><a href="{$this->router->link("88")}">Small Business Advisor</a></li>
                            <li><a href="{$this->router->link("726")}">Sole Trader v Limited Company Calculator</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
