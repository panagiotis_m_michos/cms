<table width="100%" border="0" cellspacing="0" cellpadding="0" class="clearr matrix">
  <tr>
    <td colspan="2" valign="top" class="noborder"><h2 class="mnone" style="font-size: 19px">Company Formation Packages</h2></td>
    <td width="9%" align="center" valign="middle" class="bbottom b-bronze" ><strong><a href="{$packages.109->getLink()}">{$packages.109->page->title}</a></strong></td>
    <td width="9%" align="center" valign="middle" class="bbottom b-bronze-plus"><strong><a href="{$packages.110->getLink()}">{$packages.110->page->title}</a></strong></td>
    <td width="9%" align="center" valign="middle" class="bbottom b-silver"><strong><a href="{$packages.111->getLink()}">{$packages.111->page->title}</a></strong></td>
    <td width="9%" align="center" valign="middle" class="bbottom b-gold"><strong><a href="{$packages.112->getLink()}">{$packages.112->page->title}</a></strong></td>
    <td width="9%" align="center" valign="middle" class="bbottom b-platinum"><strong><a href="{$packages.113->getLink()}">{$packages.113->page->title}</a></strong></td>
    <td width="9%" align="center" valign="middle" class="bbottom b-diamant"><strong><a href="{$packages.279->getLink()}">{$packages.279->page->title}</a></strong></td>
  </tr>
  <tr>
    <td width="19%" valign="top" class="noborder">&nbsp;</td>
    <td width="15%" align="right" valign="top"><strong>Price</strong><span style="font-size: 10px;"> (+VAT)</span></td>
    <td align="center" class="bbottom midfont"><strong>&pound;{$packages.109->price}</strong></td>
    <td align="center" class="bbottom midfont"><strong>&pound;{$packages.110->price}</strong></td>
    <td align="center" class="bbottom midfont"><strong>&pound;{$packages.111->price}</strong></td>
    <td align="center" class="bbottom midfont"><strong>&pound;{$packages.112->price}</strong></td>
    <td align="center" class="bbottom midfont"><strong>&pound;{$packages.113->price}</strong></td>
    <td align="center" class="bbottom midfont"><strong>&pound;{$packages.279->price}</strong></td>
  </tr>
  <tr class="">
    <td width="19%" valign="top" class="noborder">&nbsp;</td>
    <td width="15%" align="right" valign="top"><strong>Package Value</strong></td>
    <td align="center" class="bbottom strike">&pound;{$packages.109->productValue}</td>
    <td align="center" class="bbottom strike">&pound;{$packages.110->productValue}</td>
    <td align="center" class="bbottom strike">&pound;{$packages.111->productValue}</td>
    <td align="center" class="bbottom strike">&pound;{$packages.112->productValue}</td>
    <td align="center" class="bbottom strike">&pound;{$packages.113->productValue}</td>
    <td align="center" class="bbottom strike">&pound;{$packages.279->productValue}</td>
  </tr>
  <tr class="">
    <td width="19%" valign="top" class="noborder">&nbsp;</td>
    <td width="15%" align="right" valign="top"><strong>Save</strong></td>
    <td align="center" class="bbottom odd orange"><strong>&pound;{$packages.109->saving}</strong></td>
    <td align="center" class="bbottom odd orange"><strong>&pound;{$packages.110->saving}</strong></td>
    <td align="center" class="bbottom odd orange"><strong>&pound;{$packages.111->saving}</strong></td>
    <td align="center" class="bbottom odd orange"><strong>&pound;{$packages.112->saving}</strong></td>
    <td align="center" class="bbottom odd orange"><strong>&pound;{$packages.113->saving}</strong></td>
    <td align="center" class="bbottom odd orange"><strong>&pound;{$packages.279->saving}</strong></td>
  </tr>
  
{if $showBuyLink}

	<tr>
    	<td width="19%" valign="top" class="noborder">&nbsp;</td>
    	<td width="15%" valign="top">&nbsp;</td>
    	<td align="center"><strong><a href="{url route='basket_module_package_basket_add_package' packageId=109}">Buy now</a></strong></td>
    	<td align="center"><strong><a href="{url route='basket_module_package_basket_add_package' packageId=110}">Buy now</a></strong></td>
    	<td align="center"><strong><a href="{url route='basket_module_package_basket_add_package' packageId=111}">Buy now</a></strong></td>
    	<td align="center"><strong><a href="{url route='basket_module_package_basket_add_package' packageId=112}">Buy now</a></strong></td>
    	<td align="center"><strong><a href="{url route='basket_module_package_basket_add_package' packageId=113}">Buy now</a></strong></td>
    	<td align="center"><strong><a href="{url route='basket_module_package_basket_add_package' packageId=279}">Buy now</a></strong></td>
	</tr>
	
{/if}
  
  <tr>
    <td width="19%" valign="top" class="noborder">&nbsp;</td>
    <td width="15%" valign="top">&nbsp;</td>
    <td align="center"><strong><a href="{$packages.109->getLink()}">More info</a></strong></td>
    <td align="center"><strong><a href="{$packages.110->getLink()}">More info</a></strong></td>
    <td align="center"><strong><a href="{$packages.111->getLink()}">More info</a></strong></td>
    <td align="center"><strong><a href="{$packages.112->getLink()}">More info</a></strong></td>
    <td align="center"><strong><a href="{$packages.113->getLink()}">More info</a></strong></td>
    <td align="center"><strong><a href="{$packages.279->getLink()}">More info</a></strong></td>
  </tr>
  <tr class="odd">
    <td colspan="2"><div style="float:left;">3 hour Online Formation</div>
    	<div class="help-button-matrix">
        	<a href="#" class="help-icon-grey-bg">help</a>
            <em>
               Your company formed in 3 working hours.                  
            </em>
        </div>        
    </td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><div style="float:left;">Full Trading Company - Limited by Shares</div>
    	<div class="help-button-matrix">
        	<a href="#" class="help-icon-white-bg">help</a>
            <em>
               This is a UK Private company, limited by shares.                  
            </em>
        </div>
    </td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
  </tr>
  <tr class="odd">
    <td colspan="2"><div style="float:left;">Barclays Business Banking <b>&pound;35 Cash Back</b></div>
    	<div class="help-button-matrix">
        	<a href="#" class="help-icon-grey-bg">help</a>
            <em>
                Fast Track service. As soon as your company is formed Barclays will contact you directly. &pound;35 cash back* once your account is open. UK customers only. NB. This is an optional service.</em>
        </div>
    </td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
  </tr>
  <tr>
  <tr>
    <td colspan="2"><div style="float:left;">Digital Documents by Email</div>
    	<div class="help-button-matrix">
        	<a href="#" class="help-icon-white-bg">help</a>
            <em>
               PDF documents including the Certificate of Incorporation and the Memorandum &amp; Articles of Association.                  
            </em>
        </div>
    </td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
  </tr>
  <tr class="odd">
    <td colspan="2"><div style="float:left;">Business Start Up Pack</div>
    	<div class="help-button-matrix">
        	<a href="#" class="help-icon-grey-bg">help</a>
            <em>
               Mamut Start Up Pack - includes free business software to help you run your company.                  
            </em>
        </div>
    </td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><div style="float:left;">Memorandum &amp; Articles of Association</div>
    	<div class="help-button-matrix">
        	<a href="#" class="help-icon-white-bg">help</a>
            <em>
               Accessible as a PDF on your account 24/7.                  
            </em>
        </div>
    </td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
  </tr>
  <tr class="odd">
    <td colspan="2"><div style="float:left;">Companies House &pound;15 Fee included in price</div>
    	<div class="help-button-matrix">
        	<a href="#" class="help-icon-grey-bg">help</a>
            <em>
               Included in the package price, never added as an additional fee.                  
            </em>
        </div>
    </td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><div style="float:left;">Free Online Company Name Search</div>
    	<div class="help-button-matrix">
        	<a href="#" class="help-icon-white-bg">help</a>
            <em>
                Free name check to see if your company name is available to register.                  
            </em>
        </div>
    </td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
  </tr>
  <tr class="odd">
    <td colspan="2"><div style="float:left;">Online Admin Portal to Manage Your Companies</div>
    	<div class="help-button-matrix">
        	<a href="#" class="help-icon-grey-bg">help</a>
            <em>
               Appoint &amp; Resign directors &amp; secretaries, update the registered office address, issue shares and file Annual Return.                  
            </em>
        </div>
    </td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><div style="float:left;">Free Accountancy Consultation</div>
    	<div class="help-button-matrix">
        	<a href="#" class="help-icon-white-bg">help</a>
            <em>
                Free accountancy and tax services consultation. UK customers only.
            </em>
        </div>
    </td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
  </tr>
  <tr class="odd">
    <td colspan="2"><div style="float:left;">Google Adwords Voucher</div>
    	<div class="help-button-matrix">
        	<a href="#" class="help-icon-grey-bg">help</a>
            <em>
               Advertise your products &amp; services to millions of potential customers using Google's online advertising product.                  
            </em>
        </div>
    </td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><div style="float:left;">Web Filing Authentication Code</div>
    	<div class="help-button-matrix">
        	<a href="#" class="help-icon-white-bg">help</a>
            <em>
               Needed to make changes to your company after incorporation.                 
            </em>
        </div>
    </td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
  </tr>
  <tr class="odd">
	<td colspan="2"><div style="float:left;">Printed Certificate of Incorporation</div>
    	<div class="help-button-matrix">
        	<a href="#" class="help-icon-grey-bg">help</a>
            <em>
               Printed Certificate on official paper. Received within 5 working days.                  
            </em>
        </div>
    </td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><div style="float:left;">Share Certificates (Electronic)</div>
    	<div class="help-button-matrix">
        	<a href="#" class="help-icon-white-bg">help</a>
            <em>
                PDF Share Certificates for each subscriber.                  
            </em>
        </div>
    </td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
  </tr>
  <tr class="odd">
    <td colspan="2"><div style="float:left;">Business Start Up Guide</div>
    	<div class="help-button-matrix">
        	<a href="#" class="help-icon-grey-bg">help</a>
            <em>
                Starting in Business Guide - Everything you need to know to Start, Run and Grow your new business.                  
            </em>
        </div>
    </td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><div style="float:left;">20 Company Credit Check Reports</div>
    	<div class="help-button-matrix">
        	<a href="#" class="help-icon-white-bg">help</a>
            <em>
               Through our sister site Company Searches Made Simple, credit check 20 competitors, suppliers or new business partners. Valid for 1 year.
            </em>
        </div>
    </td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
  </tr>
  <tr class="odd">
    <td colspan="2"><div style="float:left;">Registered Office - London EC1 - 1 Year</div>
    	<div class="help-button-matrix">
        	<a href="#" class="help-icon-grey-bg">help</a>
            <em>
                 You are required by law to have a UK registered office. This service forwards on all of your company's statutory post for 1 year.                  
            </em>
        </div>
    </td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><div style="float:left;">Bound Memorandum &amp; Articles of Association</div>
    	<div class="help-button-matrix">
        	<a href="#" class="help-icon-white-bg">help</a>
            <em>
               Your company's Memorandum &amp; Articles in a bound case.                  
            </em>
        </div>
    </td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
  </tr>
  <tr class="odd">
    <td colspan="2"><div style="float:left;">Maintenance of Statutory Books &amp; Annual Return</div>
    	<div class="help-button-matrix">
        	<a href="#" class="help-icon-grey-bg">help</a>
            <em>
                 We maintain your statutory books and prepare and file your company's annual return.                  
            </em>
        </div>
    </td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><div style="float:left;">Ebooks Package Tax and Start Up Guide</div>
    	<div class="help-button-matrix">
        	<a href="#" class="help-icon-white-bg">help</a>
            <em>
               Includes our Tax &amp; Financial Strategies Guide and our Business Start Up Guide to help you with your new business.                  
            </em>
        </div>
    </td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
  </tr>
  <tr class="odd">
    <td colspan="2"><div style="float:left;">Business Forms Pack</div>
    	<div class="help-button-matrix">
        	<a href="#" class="help-icon-grey-bg">help</a>
            <em>
                 19 business forms and templates to help you with your new company.                  
            </em>
        </div>
    </td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><div style="float:left;">Company Register</div>
    	<div class="help-button-matrix">
        	<a href="#" class="help-icon-white-bg">help</a>
            <em>
               A stylish register to keep your company's statutory books organised.                  
            </em>
        </div>
    </td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
  </tr>
  <tr class="odd">
    <td colspan="2"><div style="float:left;">Registered Office &amp; Annual Return - 2 Years</div>
    	<div class="help-button-matrix">
        	<a href="#" class="help-icon-grey-bg">help</a>
            <em>
                 You are required by law to have a UK registered office &amp; to file an Annual Return each year. This service forwards on all of your company's statutory post and prepares &amp; files your Annual Return for 2 years.                  
            </em>
        </div>
    </td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><div style="float:left;">Registered Office &amp Annual Return - 3 Years</div>
    	<div class="help-button-matrix">
        	<a href="#" class="help-icon-white-bg">help</a>
            <em>
               You are required by law to have a UK registered office &amp; to file an Annual Return each year. This service forwards on all of your company's statutory post and prepares &amp; files your Annual Return for 3 years.                  
            </em>
        </div>
    </td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
  </tr>
  <tr class="odd">
    <td colspan="2"><div style="float:left;">VAT Application</div>
    	<div class="help-button-matrix">
        	<a href="#" class="help-icon-grey-bg">help</a>
            <em>
                 Value Added Tax (VAT) Registration with HM Revenue &amp; Customs.                  
            </em>
        </div>
    </td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><div style="float:left;"><a href="https://www.londonpresence.com/mail-forwarding/" title="More Info" target="_blank">Virtual Office / Mail Forwarding for 1 Year</a></div>
    	<div class="help-button-matrix">
        	<a href="#" class="help-icon-white-bg">help</a>
            <em>
               Your own central London address (EC1 postcode). Full mail forwarding service for 1 year.                  
            </em>
        </div>
    </td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
  </tr>
  {* <tr class="odd">
    <td colspan="2"><div style="float:left;">Company Seal</div>
    	<div class="help-button-matrix">
        	<a href="#" class="help-icon-grey-bg">help</a>
            <em>
                 Creates an embossed image on paper or card with your company name and number.                  
            </em>
        </div>
    </td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><div style="float:left;">Company Stamp</div>
    	<div class="help-button-matrix">
        	<a href="#" class="help-icon-white-bg">help</a>
            <em>
               Self-inking company stamp. Custom made to your own requirements.                  
            </em>
        </div>
    </td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle" class="check">&nbsp;</td>
  </tr>
  *}
  <tr>
    <td colspan="2" valign="top" class="noborder"><h2 class="mnone">&nbsp;</h2></td>
    <td width="9%" align="center" valign="middle" class="bbottom b-bronze"><strong><strong><a href="{$packages.109->getLink()}">{$packages.109->page->title}</a></strong></td>
    <td width="9%" align="center" valign="middle" class="bbottom b-bronze-plus"><strong><strong><a href="{$packages.110->getLink()}">{$packages.110->page->title}</a></strong></td>
    <td width="9%" align="center" valign="middle" class="bbottom b-silver"><strong><strong><a href="{$packages.111->getLink()}">{$packages.111->page->title}</a></strong></td>
    <td width="9%" align="center" valign="middle" class="bbottom b-gold"><strong><strong><a href="{$packages.112->getLink()}">{$packages.112->page->title}</a></strong></td>
    <td width="9%" align="center" valign="middle" class="bbottom b-platinum"><strong><strong><a href="{$packages.113->getLink()}">{$packages.113->page->title}</a></strong></td>
    <td width="9%" align="center" valign="middle" class="bbottom b-diamant"><strong><strong><a href="{$packages.279->getLink()}">{$packages.279->page->title}</a></strong></td>
  </tr>
 <tr>
    <td width="19%" valign="top" class="noborder">&nbsp;</td>
    <td width="15%" align="right" valign="top"><strong>Price</strong><span style="font-size: 10px;"> (+VAT)</span></td>
    <td align="center" class="bbottom midfont"><strong>&pound;{$packages.109->price}</strong></td>
    <td align="center" class="bbottom midfont"><strong>&pound;{$packages.110->price}</strong></td>
    <td align="center" class="bbottom midfont"><strong>&pound;{$packages.111->price}</strong></td>
    <td align="center" class="bbottom midfont"><strong>&pound;{$packages.112->price}</strong></td>
    <td align="center" class="bbottom midfont"><strong>&pound;{$packages.113->price}</strong></td>
    <td align="center" class="bbottom midfont"><strong>&pound;{$packages.279->price}</strong></td>
  </tr>
  <tr class="">
    <td width="19%" valign="top" class="noborder">&nbsp;</td>
    <td width="15%" align="right" valign="top"><strong>Package Value</strong></td>
    <td align="center" class="bbottom strike">&pound;{$packages.109->productValue}</td>
    <td align="center" class="bbottom strike">&pound;{$packages.110->productValue}</td>
    <td align="center" class="bbottom strike">&pound;{$packages.111->productValue}</td>
    <td align="center" class="bbottom strike">&pound;{$packages.112->productValue}</td>
    <td align="center" class="bbottom strike">&pound;{$packages.113->productValue}</td>
    <td align="center" class="bbottom strike">&pound;{$packages.279->productValue}</td>
  </tr>
  </tr>
  <tr class="">
    <td width="19%" valign="top" class="noborder">&nbsp;</td>
    <td width="15%" align="right" valign="top"><strong>Save</strong></td>
    <td align="center" class="bbottom odd orange"><strong>&pound;{$packages.109->saving}</strong></td>
    <td align="center" class="bbottom odd orange"><strong>&pound;{$packages.110->saving}</strong></td>
    <td align="center" class="bbottom odd orange"><strong>&pound;{$packages.111->saving}</strong></td>
    <td align="center" class="bbottom odd orange"><strong>&pound;{$packages.112->saving}</strong></td>
    <td align="center" class="bbottom odd orange"><strong>&pound;{$packages.113->saving}</strong></td>
    <td align="center" class="bbottom odd orange"><strong>&pound;{$packages.279->saving}</strong></td>
  </tr>
  
 {if $showBuyLink}
 <tr>
    <td width="19%" valign="top" class="noborder">&nbsp;</td>
    <td width="15%" valign="top">&nbsp;</td>
    <td align="center"><strong><a href="{url route='basket_module_package_basket_add_package' packageId=109}">Buy now</a></strong></td>
    <td align="center"><strong><a href="{url route='basket_module_package_basket_add_package' packageId=110}">Buy now</a></strong></td>
    <td align="center"><strong><a href="{url route='basket_module_package_basket_add_package' packageId=111}">Buy now</a></strong></td>
    <td align="center"><strong><a href="{url route='basket_module_package_basket_add_package' packageId=112}">Buy now</a></strong></td>
    <td align="center"><strong><a href="{url route='basket_module_package_basket_add_package' packageId=113}">Buy now</a></strong></td>
    <td align="center"><strong><a href="{url route='basket_module_package_basket_add_package' packageId=279}">Buy now</a></strong></td>
  </tr>
  {/if}
  
  <tr>
    <td width="19%" valign="top" class="noborder">&nbsp;</td>
    <td width="15%" valign="top">&nbsp;</td>
    <td align="center"><strong><a href="{$packages.109->getLink()}">More info</a></strong></td>
    <td align="center"><strong><a href="{$packages.110->getLink()}">More info</a></strong></td>
    <td align="center"><strong><a href="{$packages.111->getLink()}">More info</a></strong></td>
    <td align="center"><strong><a href="{$packages.112->getLink()}">More info</a></strong></td>
    <td align="center"><strong><a href="{$packages.113->getLink()}">More info</a></strong></td>
    <td align="center"><strong><a href="{$packages.279->getLink()}">More info</a></strong></td>
  </tr>
</table>
