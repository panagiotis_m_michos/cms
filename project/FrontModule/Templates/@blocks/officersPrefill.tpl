<fieldset style="clear: both;">
    <legend>Autocomplete details (optional)</legend>
    <table class="ff_table">
        <tbody>
        <tr>
            <th>{$form->getLabel('prefillOfficers') nofilter}</th>
            <td>{$form->getControl('prefillOfficers') nofilter}</td>
            <td>
                <div class="help-button">
                    <a href="#" class="help-icon">help</a>
                    <em>You can use this prefill option to appoint previously nominated appointments.</em>
                </div>
                <span class="redmsg">{$form->getError('prefillOfficers')}</span>
            </td>
        </tr>
        </tbody>
    </table>
    <p>Or enter the details below if adding a new appointment.</p>
</fieldset>