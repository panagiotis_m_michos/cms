{literal}
    <script type="text/javascript">
    /* <![CDATA[ */
    $(function() {
        var input = document.createElement("input");
        if(('placeholder' in input)==false) { 
                    $('[placeholder]').focus(function() {
                            var i = $(this);
                            if(i.val() == i.attr('placeholder')) {
                                    i.val('').removeClass('placeholder');
                                    if(i.hasClass('password')) {
                                            i.removeClass('password');
                                            this.type='password';
                                    }			
                            }
                    }).blur(function() {
                            var i = $(this);	
                            if(i.val() == '' || i.val() == i.attr('placeholder')) {
                                    if(this.type=='password') {
                                            i.addClass('password');
                                            this.type='text';
                                    }
                                    i.addClass('placeholder').val(i.attr('placeholder'));
                            }
                    }).blur().parents('form').submit(function() {
                            $(this).find('[placeholder]').each(function() {
                                    var i = $(this);
                                    if(i.val() == i.attr('placeholder'))
                                            i.val('');
                            })
                    });
            }
    });
    /* ]]> */
    </script>
    <style>
        .my-error-class-special {
            color: #FF0000;
        }
        .margin-left-22 input[type='text'] {
            /* css 3 */
            border-radius:3px;
            /* mozilla */
            -moz-border-radius:3px;
            /* webkit */
            -webkit-border-radius:3px;
            padding: 5px;
        }
        .placeholder {
            color: #aaa;
        }
        .noborder {
            border: 0px solid;
        }
        .wrap {
            height:605px;
            width:187px;
            background-image:url('/front/imgs/img961.png');
        }
        div.width-80{
            width:80px;
        }
        div.height-60{
            height:60px;
        }
        div.float-left{
            float:left;
        }
        div.float-right{
            float:right;
        }
        .margin-top-2{
            margin-top:2px;
        }
        .margin-top-10{
            margin-top:10px;
        }
        .margin-bottom-6{
            margin-bottom:6px;
        }
        .margin-left-7{
            margin-left:-7px;
        }
        .margin-left-17{
            margin-left:17px;
        }
        .margin-left-22{
            margin-left:22px;
        }
        .margin-left-35{
            margin-left:35px;
        }
        .margin-right-10{
            margin-right:10px;
        }
        .arial-10{
            font-family:Arial, Helvetica, sans-serif;
            font-size:10px;   
        }
        .arial-13{
            font-family:Arial, Helvetica, sans-serif;
            font-size:13px;
            line-height:19px;
        }
        .arial-16-bold{
            font-weight:bold;
            font-family:Arial, Helvetica, sans-serif;
            font-size:16px;
            line-height:22px;
        }
        .white{
            color:#fff;     
        }
        .black{
            color:#000;     
        }
        .red{
            color:#FF0000;  
        }
        .grey{
            color:#808080;  
        }
        .lightblue{
            color:#81c4e9;
        }
        .display-none{
            display: none;
        }
        input:focus{
            outline:none;
        }
        .my-error-class {
            display: block;
            color:#FF0000;  /* red */

        }
        .my-valid-class {
            color:#000; /* black */

        }
       
        .start-up-button {
        background: url("/front/imgs/img968.png") no-repeat scroll center center transparent;
    color: #FFFFFF;
        }
        
        #startup-overlay{
            background: url("/front/imgs/img976.png") no-repeat scroll center center transparent;
            height: 320px;
        }
    </style>
{/literal}
{literal}
    <!--[if IE 7]>
    <style type="text/css">
        .margin-right-10{
            width:145px;
        }
        .margin-top-18{
            margin-top:16px;
        }
    </style>
{/literal}
 <![endif]-->   
<!--<div class="wrap">
    <div class="float-right">
        <div class="width-80">
            <p class="arial-16-bold white">Get Your FREE <span class="orange3">&#8216;Ultimate Small Business Startup Guide&#8217;</span></p>
        </div>
    </div>
    <div class="float-left margin-top-2 margin-left-35 margin-right-10 margin-bottom-6">
        <div>
            <p class="arial-13 white" style="padding: 0;line-height: 18px;">How you can bridge the gap between where you are now and owning a profitable business.</p>
        </div> 
        <div>
            <p class="arial-13 white" style="padding: 0;line-height: 18px;">Discover the top 10 mistakes most new business owners make in the first critical year.</p>
        </div>
        <div>
            <p class="arial-13 white" style="padding: 0;line-height: 18px;">How to build your company &#8216;the right way&#8217; for fast-track profits and longterm stability.</p>
        </div>
    </div>
    <div>
        <div class="margin-left-22">
            {$rightBlockStartUpGuide->getBegin() nofilter}
            <input class="noborder arial-14 margin-left-7" style="width: 150px;" type="text" name="MERGE1" id="MERGE1" value="" placeholder="Name">
            <div class="clear"></div>
            <input class="noborder margin-top-10 arial-14 margin-left-7" style="width: 150px;" type="text" name="MERGE0" id="MERGE0" value="" placeholder="Email">

            <input class="margin-top-10 margin-left-7" type="image" name="submit" id="submit" value="Submit" src="{$urlImgs}img968.png" style="width: 157px; height: 42px"/>
            {$rightBlockStartUpGuide->getEnd() nofilter}
        </div>
    </div>
    <div class="margin-left-17">
        <p class="arial-10 lightblue">We hate SPAM, your details will never be sold or rented to anyone</p>
    </div>
</div>-->
{literal}
<script>
$(document).ready(function(){
    $('form[name="UPTml140384"]').live('submit',function(){
        //validatiion affter submit
        $('form[name="UPTml140384"]' ).validate({
            errorClass: "my-error-class-special",
            validClass: "my-error-class-special",
            rules: {
                MERGE1:{required: true },
                MERGE0:{required: true, email: true} 					   
            },
            messages: {
                MERGE1: {required: "Please enter a name"},
                MERGE0: {required: "Please enter a valid email"}
                }
        }).form();
        
        //ajax handler send form data and get error or redirect back
        var self = $(this);
        var dataForm = $(self).serialize();    
        $.ajax({
            type: "POST",
            url: self.attr('action'),
            data: dataForm,
            dataType: "json",
            success: function(data){
                if(data.error ==1) {
                    if(data.message) {
                        alert(data.message);
                    }
                }
                if(data.redirect == 1) {
                    $('#startUp').click();
                    $("form[name='UPTml140384'] #MERGE0").val('');   
                    $("form[name='UPTml140384'] #MERGE1").val(''); 
                    _gaq.push(['_trackEvent', 'Overlay', 'Pageview', 'Startup guide success overlay']);
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert('Oops...an error occurred (form error)');
            },
            timeout: function () {
                alert('Oops...an error occurred (form timeout)');
            }
        }); 
        return false;
    });
});
</script>
{/literal}
<span class="startup-overlay-link">
    <a href="#" id="startUp" rel="mainguide"></a>
</span>
{include file="@popups/StartUpGuidePopUp.tpl"}
