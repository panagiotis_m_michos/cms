{foreach $flashes as $flash}
    <div class="alert alert-{if $flash.type === 'error'}danger{else}{$flash.type}{/if}">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {if isset($flash.escape) && !$flash.escape}
            {$flash.text nofilter}
        {else}
            {$flash.text}
        {/if}
    </div>
{/foreach}
