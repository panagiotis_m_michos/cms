<div class="matrix1-table1-top"></div>
    {*GRADIENT 6*}
    <div class="matrix1-table-gradient">
        <div class="first-matrix1-column border-mark">
            <div class="matrix1-column-left matrix-bank-image" ></div>
            <div class="matrix1-column-center two-rows-column">
                {$packages[1313]->getCashBackAmount()|currency:0}-{$packages[1317]->getCashBackAmount()|currency:0} Cash Back with<br />Business Bank Account
            </div>
            <i class="fa fa-question-circle grey2 font20 lheight60" data-toggle="tooltip" data-placement="right" title="
               <ul style='padding-left:15px;'>
               <li>Choose between Barclays and TSB</li>
               <li>You get {$packages[1313]->getCashBackAmount()|currency:0}-{$packages[1317]->getCashBackAmount()|currency:0} cash back from us when you open your account</li>
               <li>12-18 months' free business banking</li>
               <li>Available to UK customers only</li>
               </ul>
               ">
            </i>
            <div class="clear"></div>
        </div>
        <div class="matrix1-package-column border-mark matrix1-cash-back-value">{$packages[1313]->getCashBackAmount()|currency:0}</div>
        <div class="matrix1-package-column border-mark matrix1-cash-back-value">{$packages[1314]->getCashBackAmount()|currency:0}</div>
        <div class="matrix1-package-column border-mark matrix1-diff matrix1-cash-back-value">{$packages[1315]->getCashBackAmount()|currency:0}</div>
        <div class="matrix1-package-column border-mark matrix1-diff1 matrix1-cash-back-value">{$packages[1316]->getCashBackAmount()|currency:0}</div>
        <div class="matrix1-package-column border-mark matrix1-cash-back-value">{$packages[1317]->getCashBackAmount()|currency:0}</div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    {*GRADIENT 1 1*}
    <div class="matrix1-table-gradient1">
        <div class="first-matrix1-column border-mark">
            <div class="matrix1-column-left matrix-rim-image" ></div>
            <div class="matrix1-column-center two-rows-column">
                FREE Lifetime Company <br /> Support
            </div>
            <i class="fa fa-question-circle grey2 font20 lheight60" data-toggle="tooltip" data-placement="right" title="
               Got a question about your company?  Unlike other company formation services we're here to help before, during and after your company setup.
               ">
            </i>
            <div class="clear"></div>
        </div>
        <div class="matrix1-package-column border-mark matrix1-tick-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-diff matrix1-tick-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-diff1 matrix1-tick-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img1"></div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    {*GRADIENT 6*}
    <div class="matrix1-table-gradient">
        <div class="first-matrix1-column border-mark">
            <div class="matrix1-column-left matrix-monitor-image" ></div>
            <div class="matrix1-column-center two-rows-column">
                Online Admin Portal to<br />Manage Your Companies
            </div>
            <i class="fa fa-question-circle grey2 font20 lheight60" data-toggle="tooltip" data-placement="right" title="
               Online Admin Portal<br />
               Easily manage your company details, directors and shareholders online saving you &pound;&pound;&pound;'s on your accountancy fees.
               Easily fulfil your LEGAL responsibilities with our tools:
               <ul style='padding-left:15px;'>
               <li>Email alerts of key dates - avoid late filing penalties</li>
               <li>Intuitive & easy to use</li>
               <li>Easily manage multiple companies</li>
               <li>Secure online access to your company's documents</li>
               <li>Upload and store your company's documents in one place</li>
               <li>Valuable document templates for adding shareholders, issuing dividends & much more!</li>
               </ul>
               ">
            </i>
            <div class="clear"></div>
        </div>
        <div class="matrix1-package-column border-mark matrix1-tick-img"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img"></div>
        <div class="matrix1-package-column border-mark matrix1-diff matrix1-tick-img"></div>
        <div class="matrix1-package-column border-mark matrix1-diff1 matrix1-tick-img"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img"></div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    {*GRADIENT 1 7*}
    <div class="matrix1-table-gradient1">
        <div class="first-matrix1-column border-mark">
            <div class="matrix1-column-left matrix-note-image" ></div>
            <div class="matrix1-column-center two-rows-column">
                No Complicated Paperwork<br /> to Complete
            </div>
            <i class="fa fa-question-circle grey2 font20 lheight60" data-toggle="tooltip" data-placement="right" title="
               Start trading in the next few hours, after you've completed your purchase simply complete your registration details and click submit.
               ">
            </i>
            <div class="clear"></div>
        </div>
        <div class="matrix1-package-column border-mark matrix1-tick-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-diff matrix1-tick-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-diff1 matrix1-tick-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img1"></div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    {*GRADIENT 8*}
    <div class="matrix1-table-gradient">
        <div class="first-matrix1-column border-mark">
            <div class="matrix1-column-left matrix-clock-image" ></div>
            <div class="matrix1-column-center one-rows-column">
                3-hour Online Formation*
            </div>
            <i class="fa fa-question-circle grey2 font20 lheight60" data-toggle="tooltip" data-placement="right" title="
               Submit your details straightaway and you could be trading in the next 3-hours (subject to Companies House workload and operating hours).
               ">
            </i>
            <div class="clear"></div>
        </div>
        <div class="matrix1-package-column border-mark matrix1-tick-img"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img"></div>
        <div class="matrix1-package-column border-mark matrix1-diff matrix1-tick-img"></div>
        <div class="matrix1-package-column border-mark matrix1-diff1 matrix1-tick-img"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img"></div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    {*GRADIENT 1 9*}
    <div class="matrix1-table-gradient1">
        <div class="first-matrix1-column border-mark">
            <div class="matrix1-column-left matrix-open-image" ></div>
            <div class="matrix1-column-center two-rows-column">
                Ready to Trade Company -<br />Limited by Shares
            </div>
            <i class="fa fa-question-circle grey2 font20 lheight60" data-toggle="tooltip" data-placement="right" title="
               All the necessary legal work completed - start trading TODAY!  A limited company is owned by its shareholders and under the limited company structure, your company and personal finances are kept separate, unlike the sole trader structure.
               ">
            </i>
            <div class="clear"></div>
        </div>
        <div class="matrix1-package-column border-mark matrix1-tick-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-diff matrix1-tick-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-diff1 matrix1-tick-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img1"></div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    {*GRADIENT 10*}
    <div class="matrix1-table-gradient">
        <div class="first-matrix1-column border-mark">
            <div class="matrix1-column-left matrix-gold-image" ></div>
            <div class="matrix1-column-center two-rows-column">
                Companies House &pound;15<br /> Fee Included in Price
            </div>
            <i class="fa fa-question-circle grey2 font20 lheight60" data-toggle="tooltip" data-placement="right" title="
               There are no hidden extras or surprise costs, the price you pay today includes the fee that has to be paid to Companies House to officially register your new company.
               ">
            </i>
            <div class="clear"></div>
        </div>
        <div class="matrix1-package-column border-mark matrix1-tick-img"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img"></div>
        <div class="matrix1-package-column border-mark matrix1-diff1 matrix1-tick-img"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img"></div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    {*GRADIENT 1 11*}
    <div class="matrix1-table-gradient1">
        <div class="first-matrix1-column border-mark">
            <div class="matrix1-column-left matrix-email-image" ></div>
            <div class="matrix1-column-center two-rows-column">
                All Your Official Company <br /> Setup Documents by Email
            </div>
            <i class="fa fa-question-circle grey2 font20 lheight60" data-toggle="tooltip" data-placement="right" title="
               We send you everything you need via email; Articles of Association, Memorandum of Association, Share Certificates and your Web Filing Authentication Code (to use Companies House's WebFiling system securely).
               ">
            </i>
            <div class="clear"></div>
        </div>
        <div class="matrix1-package-column border-mark matrix1-tick-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-diff matrix1-tick-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-diff1 matrix1-tick-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img1"></div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>

    {*GRADIENT LAST*}
    <div class="matrix1-table-gradient">
        <div class="first-matrix1-column">
            <div class="matrix1-column-left matrix-star-image"></div>
            <div class="matrix1-column-center two-rows-column">
                PLUS FREE Business<br /> Startup Toolkit (see below)
            </div>
            <i class="fa fa-question-circle grey2 font20 lheight60" data-toggle="tooltip" data-placement="right" title="
               Your Business Tool Kit includes:
               <ul style='padding-left:15px;'>
               <li>FREE .co.uk Domain Name, email address and 1-page website</li>
               <li>Google Adwords Voucher</li>
               <li>FREE Accountancy &amp; Tax Consultation (UK customers only)</li>
               <li>Free Trial of FreeAgent's cloud accounting software</li>
               <li>FREE Ultimate Small Business Startup eBook</li>
               </ul>">
            </i>
            <div class="clear"></div>
        </div>
        <div class="matrix1-package-column matrix1-tick-img"></div>
        <div class="matrix1-package-column matrix1-tick-img"></div>
        <div class="matrix1-package-column matrix1-diff matrix1-tick-img"></div>
        <div class="matrix1-package-column matrix1-diff1 matrix1-tick-img"></div>
        <div class="matrix1-package-column matrix1-tick-img"></div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    <div class="matrix1-table-bottom"></div>
</div>
<div style="padding-bottom: 5px; padding-top: 10px;">
    <h5 class="txtbold">What You Get In Each Of Our Packages</h5>
</div>

{* TABLE 2*}
<div style="border: 0px solid blue; margin-top: 20px; margin-bottom: 20px;">
    <div class="matrix1-table-top"></div>
    {*GRADIENT 1 1 *}
    <div class="matrix1-table-gradient1">
        <div class="first-matrix1-column border-mark">
            <div class="matrix1-column-left matrix-acrobat-image" ></div>
            <div class="matrix1-column-center two-rows-column">
                Digital Certificate of<br />Incorporation
            </div>
            <i class="fa fa-question-circle grey2 font20 lheight60" data-toggle="tooltip" data-placement="right" title="
               The Certificate of Incorporation is the main document which states your limited company is in existence. It includes your company name and number.<br /><br />
               In the UK, a certificate of incorporation is usually a simple certificate issued by the relevant government registry as confirmation of the due incorporation and valid existence of the company.
               <br /><br />A printed copy of the certificate is often required by various banks when opening a business bank account.
               ">
            </i>
            <div class="clear"></div>
        </div>
        <div class="matrix1-package-column border-mark matrix1-tick-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-diff matrix1-tick-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-diff1 matrix1-tick-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img1"></div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    {*GRADIENT 2*}
    <div class="matrix1-table-gradient">
        <div class="first-matrix1-column border-mark">
            <div class="matrix1-column-left matrix-acrobat-image" ></div>
            <div class="matrix1-column-center one-rows-column">
                Digital Share Certificates
            </div>
            <i class="fa fa-question-circle grey2 font20 lheight60" data-toggle="tooltip" data-placement="right" title="
               The share certificate is a legal document that certifies ownership of shares within a limited company.
               ">
            </i>
            <div class="clear"></div>
        </div>
        <div class="matrix1-package-column border-mark matrix1-tick-img"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img"></div>
        <div class="matrix1-package-column border-mark matrix1-diff matrix1-tick-img"></div>
        <div class="matrix1-package-column border-mark matrix1-diff1 matrix1-tick-img"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img"></div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    {*GRADIENT 1 3*}
    <div class="matrix1-table-gradient1">
        <div class="first-matrix1-column border-mark">
            <div class="matrix1-column-left matrix-acrobat-image" ></div>
            <div class="matrix1-column-center two-rows-column">
                Digital Memorandum<br />&amp; Articles of Association
            </div>
            <i class="fa fa-question-circle grey2 font20 lheight60" data-toggle="tooltip" data-placement="right" title="
               This is one of the documents required to incorporate a company and is often required by insurance companies or providers of finance (e.g. when leasing equipment of applying for a loan).
               <br /><br />It is basically a statement that the subscribers wish to form a company under the 2006 Act, have agreed to become members and to take at least one share each.
               ">
            </i>
            <div class="clear"></div>
        </div>
        <div class="matrix1-package-column border-mark matrix1-tick-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-diff matrix1-tick-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-diff1 matrix1-tick-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img1"></div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    {*GRADIENT 4*}
    <div class="matrix1-table-gradient">
        <div class="first-matrix1-column border-mark">
            <div class="matrix1-column-left matrix-print-image" ></div>
            <div class="matrix1-column-center two-rows-column">
                Printed Certificate of<br />Incorporation
            </div>
            <i class="fa fa-question-circle grey2 font20 lheight60" data-toggle="tooltip" data-placement="right" title="
               Equivalent to a birth certificate for your company!
               <ul style='padding-left:15px;'>
               <li>We'll immediately send you a hard copy certificate printed on special Companies House approved paper</li>
               <li>Often required by insurance companies or providers of finance (e.g. when leasing equipment or applying for a loan)</li>
               </ul>
               ">
            </i>
            <div class="clear"></div>
        </div>
        <div class="matrix1-package-column border-mark matrix1-cross-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img"></div>
        <div class="matrix1-package-column border-mark matrix1-diff matrix1-tick-img"></div>
        <div class="matrix1-package-column border-mark matrix1-diff1 matrix1-tick-img"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img"></div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    {*GRADIENT 1 5*}
    <div class="matrix1-table-gradient1">
        <div class="first-matrix1-column border-mark">
            <div class="matrix1-column-left matrix-print-image" ></div>
            <div class="matrix1-column-center one-rows-column">
                Printed Share Certificates
            </div>
            <i class="fa fa-question-circle grey2 font20 lheight60" data-toggle="tooltip" data-placement="right" title="
               We'll immediately send you a hard copy of the share certificate.  These are a legal document that certifies ownership of shares within a limited company.
               ">
            </i>
            <div class="clear"></div>
        </div>
        <div class="matrix1-package-column border-mark matrix1-cross-img"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-diff matrix1-tick-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-diff1 matrix1-tick-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img1"></div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>

    {*GRADIENT 1 7*}
    <div class="matrix1-table-gradient">
        <div class="first-matrix1-column border-mark">
            <div class="matrix1-column-left matrix-book-image" ></div>
            <div class="matrix1-column-center two-rows-column">
                Maintenance of Statutory <br /> Books
            </div>
            <i class="fa fa-question-circle grey2 font20 lheight60" data-toggle="tooltip" data-placement="right" title="
               Save &pound;'s on your accountancy fees, we'll maintain these for you.
               The Maintenance of Statutory Books is a digital record of all your company's statutory information. This includes:
               <ul style='padding-left:15px;'>
               <li>Company Name, Company Number & Registered Office</li>
               <li>Accounting Reference Date and other Important Dates</li>
               <li>List of Company Directors & their Service Address</li>
               <li>List of Company Secretaries & their address</li>
               <li>List of Shareholders & how many shares they hold</li>
               <li>Company Share Capital</li>
               </ul>
               ">
            </i>
            <div class="clear"></div>
        </div>
        <div class="matrix1-package-column border-mark matrix1-cross-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img"></div>
        <div class="matrix1-package-column border-mark matrix1-diff matrix1-tick-img"></div>
        <div class="matrix1-package-column border-mark matrix1-diff1 matrix1-tick-img"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img"></div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    {*GRADIENT 8*}
    <div class="matrix1-table-gradient1">
        <div class="first-matrix1-column border-mark">
            <div class="matrix1-column-left matrix-ec1-image" ></div>
            <div class="matrix1-column-center two-rows-column">
                Prestigious Registered Office<br /> Address - London N1
            </div>
            <i class="fa fa-question-circle grey2 font20 lheight60" data-toggle="tooltip" data-placement="right" title="
               Use our address as your Registered Office and we'll forward your official government mail (from Companies House, HMRC & Government Gateway) and block junk mail!
               <ul style='padding-left:15px;'>
               <li>Prestigious Central London address (N1 postcode)</li>
               <li>Protect the privacy of your home address</li>
               <li>No junk mail (there's lots of it!)</li>
               <li>Registered Offices must be in the UK - use ours if you are located outside the UK</li>
               <li>Avoid the inconvenience of changing your address (and stationery) if you regularly move</li>
               <li>We forward your mail within one working day of receiving it</li>
               <li>No postage or handling charges even if we forward your mail outside of the UK</li>
               </ul>
               ">
            </i>
            <div class="clear"></div>
        </div>
        <div class="matrix1-package-column border-mark matrix1-cross-img"></div>
        <div class="matrix1-package-column border-mark matrix1-cross-img"></div>
        <div class="matrix1-package-column border-mark matrix1-diff matrix1-tick-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-diff1 matrix1-tick-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img1"></div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    {*GRADIENT ?*}
    <div class="matrix1-table-gradient">
        <div class="first-matrix1-column border-mark">
            <div class="matrix1-column-left matrix-sa-image" ></div>
            <div class="matrix1-column-center two-rows-column">
                Service Address – keep your residential address private
            </div>
            <i class="fa fa-question-circle grey2 font20 lheight60" data-toggle="tooltip" data-placement="right" title="
               This service is for all directors, shareholders and company secretaries who want to keep their residential address confidential and off the Companies House public register.
               <ul style='padding-left:15px;'>
               <li>Use of our London N1 address as your service address</li>
               <li>Official Mail (HMRC, Companies House) sent to your service address and forwarded to you for one year – with no charge for postage</li>
               </ul>
               ">
            </i>
            <div class="clear"></div>
        </div>
        <div class="matrix1-package-column border-mark matrix1-cross-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-cross-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-diff1 matrix1-tick-img"></div>
        <div class="matrix1-package-column border-mark matrix1-diff1 matrix1-tick-img"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img"></div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    {*GRADIENT 1 9*}
    <div class="matrix1-table-gradient1">
        <div class="first-matrix1-column border-mark">
            <div class="matrix1-column-left matrix-ar-image" ></div>
            <div class="matrix1-column-center two-rows-column">
                Annual Return<br />(Preparation &amp; Filing)
            </div>
            <i class="fa fa-question-circle grey2 font20 lheight60" data-toggle="tooltip" data-placement="right" title="
               Our experts will accurately prepare and submit your Annual Return on time to Companies House
               <ul style='padding-left:15px;'>
               <li>Every year, companies must update Companies House with their latest details using an Annual Return</li>
               <li>Our experts will prepare and submit your company's Annual Return to Companies House</li>
               <li>Guarantees you file your Annual return on time and 100% accurately</li>
               <li>Includes Companies House's Annual Return fee (&pound;13 pa)</li>
               <li>This service is renewable annually</li>
               </ul>
               NB. It is a criminal offence not to deliver an Annual Return within 28 days of its due date, for which Companies House may strike-off and prosecute the company and its officers.
               ">
            </i>
            <div class="clear"></div>
        </div>
        <div class="matrix1-package-column border-mark matrix1-cross-img"></div>
        <div class="matrix1-package-column border-mark matrix1-cross-img"></div>
        <div class="matrix1-package-column border-mark matrix1-diff matrix1-cross-img"></div>
        <div class="matrix1-package-column border-mark matrix1-diff1 matrix1-tick-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img1"></div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    {*GRADIENT 6*}
    <div class="matrix1-table-gradient">
        <div class="first-matrix1-column border-mark">
            <div class="matrix1-column-left matrix-ma-image" ></div>
            <div class="matrix1-column-center two-rows-column">
                Bound Memorandum <br /> &amp; Article of Association
            </div>
            <i class="fa fa-question-circle grey2 font20 lheight60" data-toggle="tooltip" data-placement="right" title="
               We'll immediately send you a hard copy of your Mems & Arts.<br /><br />
               This is one of the documents required to incorporate a company and is often required by insurance companies or providers of finance (e.g. when leasing equipment of applying for a loan).
               <br /><br />It is basically a statement that the subscribers wish to form a company under the 2006 Act, have agreed to become members and to take at least one share each.
               ">
            </i>
            <div class="clear"></div>
        </div>
        <div class="matrix1-package-column border-mark matrix1-cross-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-cross-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-diff matrix1-cross-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-diff1 matrix1-tick-img"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img"></div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    {*GRADIENT 10*}
    {*<div class="matrix1-table-gradient">
    <div class="first-matrix1-column border-mark">
    <div class="matrix1-column-left matrix-freeTr-image" ></div>
    <div class="matrix1-column-center two-rows-column">
    FREE Training Workshop:<br /> <span style="font-size: 10px;">Understanding Your Ltd Company</span>
    </div>
    <div class="matrix1-column-right" style="text-align: right;">
    <a href="javascript:;" class="matrix1-tooltip" title="" rel="35">
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </a>
    <span id="tooltip35" class="tooltip" style="display: none; text-align: left; width: 200px;"  >
    Attend one of our popular FREE workshops to understand your Limited Company and your responsibilities as a director.
    </span>
    </div>
    <div class="clear"></div>
    </div>
    <div class="matrix1-package-column border-mark matrix1-cross-img1"></div>
    <div class="matrix1-package-column border-mark matrix1-cross-img1"></div>
    <div class="matrix1-package-column border-mark matrix1-cross-img1"></div>
    <div class="matrix1-package-column border-mark matrix1-diff1 matrix1-tick-img"></div>
    <div class="matrix1-package-column border-mark matrix1-tick-img"></div>
    <div class="clear"></div>
    </div>
    <div class="clear"></div> *}
    {*GRADIENT 1 11*}
    <div class="matrix1-table-gradient1">
        <div class="first-matrix1-column border-mark">
            <div class="matrix1-column-left matrix-tax-image" ></div>
            <div class="matrix1-column-center two-rows-column">
                Tax and Financial<br />Strategies Guide
            </div>
            <i class="fa fa-question-circle grey2 font20 lheight60" data-toggle="tooltip" data-placement="right" title="
               Understanding tax and finance is vital for start ups. This guide provides you with valuable tax tips and advice that's designed to get your business off to the perfect start. Chapters include:
               <ul style='padding-left:15px;'>
               <li>Strategies for your business</li>
               <li>Planning for yourself and your family</li>
               <li>Tax and employment</li>
               <li>Saving and investments</li>
               </ul>
               ">
            </i>
            <div class="clear"></div>
        </div>
        <div class="matrix1-package-column border-mark matrix1-cross-img"></div>
        <div class="matrix1-package-column border-mark matrix1-cross-img"></div>
        <div class="matrix1-package-column border-mark matrix1-diff matrix1-cross-img"></div>
        <div class="matrix1-package-column border-mark matrix1-diff1 matrix1-cross-img"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img1"></div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    {*GRADIENT 12*}
    <div class="matrix1-table-gradient">
        <div class="first-matrix1-column border-mark">
            <div class="matrix1-column-left matrix-vo-image" ></div>
            <div class="matrix1-column-center two-rows-column">
                Virtual Office Mail<br /> Forwarding
            </div>
            <i class="fa fa-question-circle grey2 font20 lheight60" data-toggle="tooltip" data-placement="right" title="
               Use our prestigious address as your virtual office for credibility, privacy and a UK-based trading address
               <ul style='padding-left:15px;'>
               <li>All your mail will be forwarded by 1st class post (we'll also block junk mail)</li>
               <li>Choose to collect your mail or have it posted to you</li>
               <li>Ideal for startups and those requiring a UK-based trading address</li>
               <li>This service is renewable after 12 months at &pound;19.99pm</li>
               </ul>
               ">
            </i>
            <div class="clear"></div>
        </div>
        <div class="matrix1-package-column border-mark matrix1-cross-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-cross-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-cross-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-diff1 matrix1-cross-img1"></div>
        <div class="matrix1-package-column border-mark matrix1-tick-img"></div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    {*GRADIENT LAST*}
    <div class="matrix1-table-gradient1">
        <div class="first-matrix1-column">
            <div class="matrix1-column-left matrix-hr-image" style="width: 35px;"></div>
            <div class="matrix1-column-center two-rows-column" style="width: 175px;">
                VAT Registration with<br /> HMRC
            </div>
            <i class="fa fa-question-circle grey2 font20 lheight60" data-toggle="tooltip" data-placement="right" title="
               Our experts will register your new company for VAT avoiding any errors. We'll email you the registration form to complete. Once posted back to us, it is thoroughly checked and approved before being submitted to HMRC.
               <ul style='padding-left:15px;'>
               <li>Recover VAT on pre-incorporation business expenditure</li>
               <li>Recover VAT on future business expenditure</li>
               <li>Make your company look more credible</li>
               </ul>
               ">
            </i>
            <div class="clear"></div>
        </div>
        <div class="matrix1-package-column matrix1-cross-img" ></div>
        <div class="matrix1-package-column matrix1-cross-img"></div>
        <div class="matrix1-package-column matrix1-diff matrix1-cross-img"></div>
        <div class="matrix1-package-column matrix1-diff1 matrix1-cross-img"></div>
        <div class="matrix1-package-column matrix1-tick-img1"></div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
