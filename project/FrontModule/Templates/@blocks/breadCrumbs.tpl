{if isSet($breadcrumbs) && !empty($breadcrumbs)}
	<p id="breadcrumbs">
		{foreach from=$breadcrumbs item="val" name="f1"}
			{if !$smarty.foreach.f1.last}
				<a href="{$val.url}">{$val.title}</a> &gt;
			{else}
				{$val.title}
			{/if}
		{/foreach}
	</p> <!-- #breadcrumbs -->
{/if}