{if isset($flashes)}
    {foreach $flashes as $flash}
        <div class="flash {$flash.type}">
            {if isset($flash.escape) && !$flash.escape}
                {$flash.text nofilter}
            {else}
                {$flash.text}
            {/if}
        </div>
    {/foreach}
{/if}
