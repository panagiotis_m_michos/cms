<html>
	<head>
        <script type="text/javascript" src="{$urlComponent}jquery/dist/jquery.min.js" ></script>
        <script type="text/javascript" src="{$urlComponent}jquery-migrate/jquery-migrate.min.js" ></script>
        <script type="text/javascript">
			{literal}
            var AuthForm = function(contextWindow, beforeElement) {
          
				var context = contextWindow.document;
                this.handleSuccess = function(location) {
                    contextWindow.location = location;
                }
                this.handleError = function(error, button, form) {
                    var errorElem = '<div class="flash error">' + error + '</div>';
                    $(beforeElement, context).prepend(errorElem);
                    //hide frame						
                    $(form, context).hide();
                    //buttons
                    $(button, context).show();
                    $(context).scrollTop(1);
                }
            }
			{/literal}
        </script>
	</head>
	<body>
		{if !empty($success)}
			<div style="font-family:Arial;font-size:14px">
				<h3>3D authentication successful</h3>
				<p>Redirecting you to the confirmation page ...</p>
                <p>If you are not redirected automatically please press <a target="_parent" href="{$location}">continue</a></p>
			</div>
		{elseif !empty($error)}
			<div style="font-family:Arial;font-size:14px">
				<h3>3D authentication failed. Please specify the correct password</h3>
				<p>{$error}</p>
                <p>Please press <a target="_parent" href="{$failedLocation}">continue</a> to re-enter the process</p>
			</div>
		{/if}
		<script type="text/javascript">
			var error = {if !empty($error)}"{$error}"{else}false{/if};
			var redirectParent = {if !empty($location)}"{$location}"{else}false{/if};
			{literal}
			var authForm = new AuthForm(window.parent, '#content');
			if (redirectParent) {
				authForm.handleSuccess(redirectParent);
			} else if (error) {
				authForm.handleError(error, '#submitBlock', '#authFormBlock');
			}
			{/literal}
		</script>
	</body>
</html>


