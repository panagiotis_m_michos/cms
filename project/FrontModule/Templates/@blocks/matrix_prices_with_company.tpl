<div class="matrix1-top-table-gradient">
    <div class="matrix1-package-column-price">
        <div class="matrix1-price">
            <span class="matrix1-price-up">{$packages[1313]->getPrice()|currency}</span>
            <span class="matrix1-vat">+VAT</span>
            <div class="clear"></div>
        </div>
        <div>
            <span class="matrix1-real-price-up">{$packages[1313]->productValue|currency}</span>
            <span class="matrix1-real-price-vat">+VAT</span>
            <div class="clear"></div>
        </div>
        <div>
            <span class="matrix1-save-text-up">You save {$packages[1313]->saving|currency}</span>
            <i class="fa fa-question-circle grey2" data-toggle="tooltip" data-placement="left" title="
               The Basic Package has the essentials for getting your company registered and ready to trade quickly. It includes digital copies of all the necessary documents
               PLUS you also get our FREE Business Startup Toolkit worth over £200.
               ">
            </i>
            <div class="clear"></div>
        </div>
        <div class="matrix1-button-link">
            <a href="{url route='basket_module_package_basket_add_package' packageId=1313}" class="matrix1-btn">BUY NOW</a>
        </div>
        <div class="matrix1-more-info-link">
            <a href="{$packages[1313]->getLink()}{if $noCompany}&no=1{/if}">More Info</a>
        </div>
    </div>
    <div class="matrix1-package-column-price">
        <div class="matrix1-price">
            <span class="matrix1-price-up">{$packages[1314]->getPrice()|currency}</span>
            <span class="matrix1-vat">+VAT</span>
            <div class="clear"></div>
        </div>
        <div>
            <span class="matrix1-real-price-up">{$packages[1314]->productValue|currency}</span>
            <span class="matrix1-real-price-vat">+VAT</span>
            <div class="clear"></div>
        </div>
        <div>
            <span class="matrix1-save-text-up">You save {$packages[1314]->saving|currency}</span>
            <i class="fa fa-question-circle grey2" data-toggle="tooltip" data-placement="left" title="
               The Printed Package would cost {$packages[1314]->productValue|currency} if bought separately. We've bundled everything together to save you money and get your company registered and ready to trade.
               It includes printed copies of all the documents you'll need to arrange a bank account, finance and business insurance. PLUS you also get our FREE Business Startup Toolkit worth over £200.
               ">
            </i>
            <div class="clear"></div>
        </div>
        <div class="matrix1-button-link">
            <a href="{url route='basket_module_package_basket_add_package' packageId=1314}" class="matrix1-btn">BUY NOW</a>
        </div>
        <div class="matrix1-more-info-link">
            <a href="{$packages[1314]->getLink()}{if $noCompany}&no=1{/if}">More Info</a>
        </div>
    </div>
    <div class="matrix1-package-column-price">
        <div class="matrix1-price">
            <span class="matrix1-price-up">{$packages[1315]->getPrice()|currency}</span>
            <span class="matrix1-vat">+VAT</span>
            <div class="clear"></div>
        </div>
        <div>
            <span class="matrix1-real-price-up">{$packages[1315]->productValue|currency}</span>
            <span class="matrix1-real-price-vat">+VAT</span>
            <div class="clear"></div>
        </div>
        <div>
            <span class="matrix1-save-text-up">You save {$packages[1315]->saving|currency}</span>
            <i class="fa fa-question-circle grey2" data-toggle="tooltip" data-placement="left" title="
               The Privacy Package would cost {$packages[1315]->productValue|currency} if bought separately. We've bundled everything together to save you money and get your company start trading immediately.
               It includes printed copies of all the documents you'll need to arrange a bank account, finance and business insurance. On top of that you can use of our prestigious
               London address as your Registered Office for a year. This means you can keep your home address private and avoid junk mail.  PLUS you also get our FREE Business
               Startup Toolkit worth over £200.
               ">
            </i>
            <div class="clear"></div>
        </div>
        <div class="matrix1-button-link">
            <a href="{url route='basket_module_package_basket_add_package' packageId=1315}" class="matrix1-btn">BUY NOW</a>
        </div>
        <div class="matrix1-more-info-link">
            <a href="{$packages[1315]->getLink()}{if $noCompany}&no=1{/if}">More Info</a>
        </div>
    </div>
    <div class="matrix1-package-column-price">
        <div class="matrix1-price">
            <span class="matrix1-price-up">{$packages[1316]->getPrice()|currency}</span>
            <span class="matrix1-vat">+VAT</span>
            <div class="clear"></div>
        </div>
        <div>
            <span class="matrix1-real-price-up">{$packages[1316]->productValue|currency}</span>
            <span class="matrix1-real-price-vat">+VAT</span>
            <div class="clear"></div>
        </div>
        <div>
            <span class="matrix1-save-text-up">You save {$packages[1316]->saving|currency}</span>
            <i class="fa fa-question-circle grey2" data-toggle="tooltip" data-placement="left" title="
               The Comprehensive Package would cost {$packages[1316]->productValue|currency} if bought separately. We've bundled everything together to save you money and get your company start trading immediately.
               It includes printed copies of all the documents you'll need to arrange a bank account, finance and business insurance. It includes the use of our prestigious London address as your
               Registered Office for a year. This means you can keep your home address private and avoid junk mail.  On top of this, we'll prepare and send your Annual Return to Companies House
               saving you hundreds on your accountants fees (you will still need an accountant to submit your company accounts to HMRC.)  PLUS you also get our FREE Business Startup Toolkit worth over £200.
               ">
            </i>
            <div class="clear"></div>
        </div>
        <div class="matrix1-button-link">
            <a href="{url route='basket_module_package_basket_add_package' packageId=1316}" class="matrix1-btn">BUY NOW</a>
        </div>
        <div class="matrix1-more-info-link">
            <a href="{$packages[1316]->getLink()}{if $noCompany}&no=1{/if}">More Info</a>
        </div>
    </div>
    <div class="matrix1-package-column-price">
        <div class="matrix1-price">
            <span class="matrix1-price-up">{$packages[1317]->getPrice()|currency}</span>
            <span class="matrix1-vat">+VAT</span>
            <div class="clear"></div>
        </div>
        <div>
            <span class="matrix1-real-price-up">{$packages[1317]->productValue|currency}</span>
            <span class="matrix1-real-price-vat">+VAT</span>
            <div class="clear"></div>
        </div>
        <div>
            <span class="matrix1-save-text-up">You save {$packages[1317]->saving|currency}</span>
            <i class="fa fa-question-circle grey2" data-toggle="tooltip" data-placement="left" title="
               The Ultimate Package would cost {$packages[1317]->productValue|currency} if bought separately. We've bundled everything together to save you money and help your company start trading immediately.
               You get full use of our prestigious London office address for a year. This means you can impress clients with a central London address and keep your home address
               totally private. We'll also forward all of your post to you and block annoying junk mail. On top of that you get printed copies of all the documents you'll need
               to arrange a bank account, finance and business insurance. Not only that but we'll prepare and send your Annual Return to Companies House saving you hundreds on
               your accountant's fees (you will still need an accountant to submit your company accounts to HMRC.)  PLUS you also get our FREE Business Startup Toolkit worth over £200.
               ">
            </i>
            <div class="clear"></div>
        </div>
        <div class="matrix1-button-link">
            <a href="{url route='basket_module_package_basket_add_package' packageId=1317}" class="matrix1-btn">BUY NOW</a>
        </div>
        <div class="matrix1-more-info-link">
            <a href="{$packages[1317]->getLink()}{if $noCompany}&no=1{/if}">More Info</a>
        </div>
    </div>
    <div class="clear"></div>
</div>
