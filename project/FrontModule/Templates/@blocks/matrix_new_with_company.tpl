{* TABLE 2*}
<div style="border: 0px solid blue; margin-top: 20px; margin-bottom: 20px;">

    <div class="up-matrix1-table">
        <div class="left-up-matrix1-table" style="float: left; height: 335px; width: 240px; margin-top: 0px;">
            <div class="matrix1-arrow-inf" style="margin-top: 0px;"></div>
            <div class="matrix1-top-table-text">
                <h5 class="txtbold">Included in ALL Our Packages</h5>
            </div>
        </div>
        <div class="left-up-matrix1-table " style="width: 730px; border: 0px solid blue; float: right">
            <div class="matrix1-top-up-header">
                <div class="matrix1-package-column-top">Basic <br /> Package</div>
                <div class="matrix1-package-column-top">Printed <br /> Package</div>
                <div class="matrix1-package-column-top " style="padding-top: 15px;">Privacy <br /> Package</div>
                <div class="matrix1-package-column-top ">Comprehensive <br /> Package</div>
                <div class="matrix1-package-column-top">Ultimate <br /> Package</div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
            <div class="matrix1-top-table-gradient2">
                <div class="matrix1-package-column-middle">"Great choice if you <br />want to reserve a <br />company name for <br />future use or form a<br /> dormant company"</div>
                <div class="matrix1-package-column-middle">"Perfect to get you<br />trading quickly-<br />everything  you need <br />to maintain  your<br />statutory records by<br />email and in the post"</div>
                <div class="matrix1-package-column-middle" style="font-weight: bold;">"When your business needs a prestigious address and you want to protect the privacy of your home address"</div>
                <div class="matrix1-package-column-middle">"Every feature you'll<br /> need to start trading<br /> immediately and<br /> ensure your statutory <br />requirements are <br />covered"</div>
                <div class="matrix1-package-column-middle">"Every feature you'll<br /> need for a truly virtual<br /> presence"</div>
                <div class="clear"></div>
            </div>
            <div class="matrix1-top-table-middle"></div>
            {include "@blocks/matrix_prices_with_company.tpl"}
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    {include "@blocks/matrix_block.tpl"}
    <div class="matrix1-table1-bottom"></div>
    <div class="up-matrix1-table">
        <div class="left-up-matrix1-table" style="float: left; height: 235px; width: 240px;"></div>
        <div class="left-up-matrix1-table " style="width: 730px; border: 0px solid blue; float: right">
            {include "@blocks/matrix_prices_with_company.tpl"}
            <div class="matrix1-top-up-header1">
                <div class="matrix1-package-column-top" style="padding-top: 10px;">Basic <br /> Package</div>
                <div class="matrix1-package-column-top" style="padding-top: 10px;">Printed <br /> Package</div>
                <div class="matrix1-package-column-top " style="padding-top: 3px;">
                    <div style="font-size: 11px; font-style: italic; margin-bottom: 10px;">Most Popular</div>
                    Privacy <br /> Package
                </div>
                <div class="matrix1-package-column-top " style="padding-top: 10px;">Comprehensive <br /> Package</div>
                <div class="matrix1-package-column-top" style="padding-top: 10px;">Ultimate <br /> Package</div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="info-z-index">
        <img src="{$urlImgs}matrix/scroll-down.png" />
    </div>
</div>
