{if isset($flashes)}
    {foreach from=$flashes item="flash"}
        <div class="flash {$flash->type} popup">{$flash->text nofilter}</div>
    {/foreach}
{/if}
