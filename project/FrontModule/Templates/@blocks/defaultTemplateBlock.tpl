
{* DISABLED BOTH COLUMN *}
{if $disabledLeftColumn && $disabledRightColumn}
	<div id="maincontent1" style="width: 955px;">
		{$content nofilter}
	</div>

{* DISABLED ONE COLUMN *}
{elseif $disabledLeftColumn || $disabledRightColumn}
	<div id="maincontent1" style="width: 770px;">
		{$content nofilter}
	</div>
	
{* BOTH COLUMN *}
{else}
	<div id="maincontent1">
		<div class="box-white2">
			<div class="box-white2-top">
				<div class="box-white2-bottom">
					{$content nofilter}
				</div>
			</div>
		</div>
	</div>
{/if}