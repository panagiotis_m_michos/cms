{literal}
    <style type="text/css">
<!--
table.clearr.matrix {
	border-left: 0;
}
	table.clearr.matrix .bleft {
		border-left: 1px solid #ccc;
	}
	table.clearr.matrix .bLeftRight2px {
		border-left: 2px solid #ccc;
		border-right: 3px solid #ccc;
	}
    
-->
* {margin:0}

a.package-link {
    text-decoration: none;
    color: #000;
}
a.package-link:hover {
    text-decoration: underline;
    color: #069;
}

.tooltip {
    background-color: #ffffff;
    border: 1px solid #cccccc;
    padding: 5px;
    width: 240px;
    font-style: italic;
    display: none;        
}



</style>

{/literal}
<div style="width: 785px; margin: auto;">

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="clearr new-matrix" style="border:0px solid black;margin-top: 15px;">
    <tr class="top-up-head white-row"><!-- up matrix -->
        <td rowspan="{if $showBuyLink}5{else}4{/if}" class="th-left-head" style="background-image: none;">
            <div style="border:0px solid red;padding-bottom: 35px;"><h2 class="mnone" style="font-size: 19px">Company Formation Packages</h2></div>
            <div style="border:0px solid red;color: #000;text-align: right;margin-bottom: 7px;">
                <strong>Price</strong><span style="font-size: 10px;"> (+VAT)</span>
            </div>
            <div style="border:0px solid red;color: #000;text-align: right;margin-bottom: 5px;">
                <strong>Package Value</strong></span>
            </div>
        </td>
        <td style="padding: 0;"><a href="{$packages.109->getLink()}"><img src="{$urlImgs}boxes/top/bronze-top.png" width="78" height="30"/></a></td>
        <td style="padding: 0;"><a href="{$packages.110->getLink()}"><img src="{$urlImgs}boxes/top/bronze-plus-top.png" width="78" height="30"/></a></td>
        <td style="padding: 0;"><a href="{$packages.111->getLink()}"><img src="{$urlImgs}boxes/top/silver-top.png" width="78" height="30"/></a></td>
        <td style="padding: 0;"><a href="{$packages.112->getLink()}"><img src="{$urlImgs}boxes/top/gold-top.png" width="78" height="30"/></a></td>
        <td style="padding: 0;"><a href="{$packages.113->getLink()}"><img src="{$urlImgs}boxes/top/platinum-top.png" width="78" height="30"/></a></td>
        <td style="padding: 0;"><a href="{$packages.279->getLink()}"><img src="{$urlImgs}boxes/top/diamond-top.png" width="78" height="30"/></a></td>
    </tr>
    <tr class="top-midd-head">
        <td class="p-bronze-midd"><strong>&nbsp;</strong></td>
        <td class="p-bronze-plus-midd"><strong>&nbsp;</strong></td>
        <td class="p-silver-midd"><strong>&nbsp;</strong></td>
        <td class="p-gold-midd"><strong>&nbsp;</strong></td>
        <td class="p-platinum-midd"><strong>&nbsp;</strong></td>
        <td class="p-diamond-midd"><strong>&nbsp;</strong></td>
    </tr>
    <tr class="white-row">
        <td style="color: #999;">&pound;{$packages.109->productValue}</td>
        <td style="color: #999;">&pound;{$packages.110->productValue}</td>
        <td style="color: #999;">&pound;{$packages.111->productValue}</td>
        <td style="color: #999;">&pound;{$packages.112->productValue}</td>
        <td style="color: #999;">&pound;{$packages.113->productValue}</td>
        <td style="color: #999;">&pound;{$packages.279->productValue}</td>
    {if $showBuyLink}
    </tr>
        <tr class="white-row">
    	<td><strong><a href="{url route='basket_module_package_basket_add_package' packageId=109}">Buy now</a></strong></td>
    	<td><strong><a href="{url route='basket_module_package_basket_add_package' packageId=110}">Buy now</a></strong></td>
    	<td><strong><a href="{url route='basket_module_package_basket_add_package' packageId=111}">Buy now</a></strong></td>
    	<td><strong><a href="{url route='basket_module_package_basket_add_package' packageId=112}">Buy now</a></strong></td>
    	<td><strong><a href="{url route='basket_module_package_basket_add_package' packageId=113}">Buy now</a></strong></td>
    	<td><strong><a href="{url route='basket_module_package_basket_add_package' packageId=279}">Buy now</a></strong></td>
	</tr>
    {/if}
    <tr class="white-row">
    	<td><strong><a href="{$packages.109->getLink()}">More info</a></strong></td>
    	<td><strong><a href="{$packages.110->getLink()}">More info</a></strong></td>
    	<td><strong><a href="{$packages.111->getLink()}">More info</a></strong></td>
    	<td><strong><a href="{$packages.112->getLink()}">More info</a></strong></td>
    	<td><strong><a href="{$packages.113->getLink()}">More info</a></strong></td>
    	<td><strong><a href="{$packages.279->getLink()}">More info</a></strong></td>
	</tr><!--end up matrix -->
    
    <tr class="even">
        <td class="tdleft">
          <a href="javascript:;" class="package-link" title="Your company formed in 3 working hours.">
              3 hour Online Formation
          </a>
        </td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
    </tr>
    <tr>
        <td class="tdleft">
            <a href="javascript:;" class="package-link" title="This is a UK Private company, limited by shares.">
                Full Trading Company - Limited by Shares
            </a>
        </td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
    </tr>
    <tr class="even">
        <td class="tdleft">
            <a href="javascript:;" class="package-link" title="Fast Track service. As soon as your company is formed Barclays will contact you directly. &pound;50 cash back* once your account is open. UK customers only. NB. This is an optional service.">
                Barclays Business Banking <b>&pound;50 Cash Back</b>
            </a>
        </td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
    </tr>
    <tr>
        <td class="tdleft">
            <a href="javascript:;" class="package-link" title="PDF documents including the Certificate of Incorporation and the Memorandum &amp; Articles of Association.">
                Digital Documents by Email
            </a>
        </td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
    </tr>
    {*<tr>
        <td class="tdleft">
            <a href="javascript:;" class="package-link" title="Mamut Start Up Pack - includes free business software to help you run your company.">
                Business Start Up Pack
            </a>
        </td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
    </tr>
    *}
    <tr class="even">
        <td class="tdleft ">
            <a href="javascript:;" class="package-link" title="Accessible as a PDF on your account 24/7.">
                Memorandum &amp; Articles of Association
            </a>
        </td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
    </tr>
    <tr>
        <td class="tdleft">
            <a href="javascript:;" class="package-link" title="Included in the package price, never added as an additional fee.">
                Companies House &pound;13 Fee included in price
            </a>
        </td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
    </tr>
    {*<tr>
        <td class="tdleft">
            <a href="javascript:;" class="package-link" title="Free name check to see if your company name is available to register.">
                Free Online Company Name Search
            </a>
        </td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
    </tr>
    *}
    <tr class="even">
        <td class="tdleft">
            <a href="javascript:;" class="package-link" title="Appoint &amp; Resign directors &amp; secretaries, update the registered office address, issue shares and file Annual Return.">
                Online Admin Portal to Manage Your Companies
            </a>
        </td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
    </tr>
    <tr>
        <td class="tdleft">
            <a href="javascript:;" class="package-link" title="Free accountancy and tax services consultation. UK customers only.">
                Free Accountancy Consultation
            </a>
        </td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
    </tr>
    <tr class="even">
        <td class="tdleft">
           <a href="javascript:;" class="package-link" title="Advertise your products &amp; services to millions of potential customers using Google's online advertising product.">
                Google Adwords Voucher
            </a>
        </td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
    </tr>
    <tr>
        <td class="tdleft">
           <a href="javascript:;" class="package-link" title="Needed to make changes to your company after incorporation.">
                Web Filing Authentication Code
            </a>
        </td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
    </tr>
    <tr class="free-domain-name" >
        <td class="tdleft">
           <a href="{$this->router->link("1117")}" class="popupnew package-link" title="Free Domain Name Registration">
                Free Domain Name Registration
            </a>
        </td>
        
        <td class="check5">&nbsp;</td>
        <td class="check5">&nbsp;</td>
        <td class="check5">&nbsp;</td>
        <td class="check5">&nbsp;</td>
        <td class="check5">&nbsp;</td>
        <td class="check5">&nbsp;</td>
    </tr>
    <tr >
        <td class="tdleft">
           <a href="javascript:;" class="package-link" title="Printed Certificate on official paper. Received within 5 working days. Usually required to open a bank account.">
                Printed Certificate of Incorporation
            </a>
        </td>
        <td>&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
    </tr>
    
    <tr class="even">
        <td class="tdleft">
           <a href="javascript:;" class="package-link" title="PDF Share Certificates for each subscriber. Usually required to open a bank account.">
                Share Certificates (Electronic)
            </a>
        </td>
        <td>&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
    </tr>
    <tr>
        <td class="tdleft">
           <a href="javascript:;" class="package-link" title="Starting in Business Guide - Everything you need to know to Start, Run and Grow your new business.">
                Business Start Up Guide
            </a>
        </td>
        <td>&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
    </tr>
    <tr class="even">
        <td class="tdleft">
           <a href="javascript:;" class="package-link" title="A digital record of all your company's statutory information.">
                Maintenance of Statutory Books
            </a>
        </td>
        <td>&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
    </tr>
    <tr>
        <td class="tdleft">
            <a href="javascript:;" class="package-link" title="&pound;20 off business training courses with Business Training Made Simple.">
                &pound;20 off Business Training
            </a>
        </td>
        <td>&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
    </tr>
    <tr class="even">
        <td class="tdleft">
            <a href="javascript:;" class="package-link" title="Through our sister site Company Searches Made Simple, credit check 20 competitors, supplies or new business partners. Valid for 1 year.">
                20 Company Credit Check Reports
            </a>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
    </tr>
    <tr>
        <td class="tdleft">
            <a href="javascript:;" class="package-link" title="You are required by law to have a UK registered office. This service forwards on all of your company's statutory post for 1 year.">
                Registered Office - London EC1
            </a>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>1 year</td>
        <td>1 year</td>
        <td>2 years</td>
        <td>3 years</td>
    </tr>
    <tr class="even">
        <td class="tdleft">
            <a href="javascript:;" class="package-link" title="Your company's Memorandum &amp; Articles in a bound case.">
                Bound Memorandum &amp; Articles of Association
            </a>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
    </tr>
    <tr>
        <td class="tdleft">
            <a href="javascript:;" class="package-link" title="Includes preparation and filing of your first year's Annual Return.">
                Annual Return (Preparation &amp; Filing)
            </a>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>1 year</td>
        <td>2 years</td>
        <td>3 years</td>
    </tr>
    <tr class="even">
        <td class="tdleft">
            <a href="javascript:;" class="package-link" title="Free half day training course on Understanding Your Limited Company by Business training Made Simple. Must be redeemed within 3 months of purchase. 1 seat per package.">
                Training Course: Understanding Your Ltd Co.
            </a>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
    </tr>
    <tr>
        <td class="tdleft">
            <a href="javascript:;" class="package-link" title="Includes our Tax &amp; Financial Strategies Guide and our Business Start Up Guide to help you with your new business. ">
               Ebooks Package Tax and Start Up Guide
            </a>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td class="check">&nbsp;</td>
    </tr>
    <tr class="even">
        <td class="tdleft">
            <a href="javascript:;" class="package-link" title="A stylish register to keep your company's statutory books organised.">
               Company Register
            </a>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td class="check">&nbsp;</td>
    </tr>
    {*
    <tr>
        <td class="tdleft">
            <a href="javascript:;" class="package-link" title="You are required by law to have a UK registered office &amp; to file an Annual Return each year. This service forwards on all of your company's statutory post and prepares &amp; files your Annual Return for 2 years.">
               Registered Office &amp; Annual Return - 2 Years
            </a>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td class="check">&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="tdleft">
            <a href="javascript:;" class="package-link" title="You are required by law to have a UK registered office &amp; to file an Annual Return each year. This service forwards on all of your company's statutory post and prepares &amp; files your Annual Return for 3 years.">
               Registered Office &amp Annual Return - 3 Years
            </a>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td class="check">&nbsp;</td>
    </tr>
    *}
    <tr>
        <td class="tdleft">
            <a href="http://www.thisisyouroffice.com/mail-forwarding/" class="package-link" title="Your own central London address (EC1 postcode). Full mail forwarding service." target="_blank">
               Virtual Office / Mail Forwarding
            </a>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>1 year</td>
    </tr>
    <tr class="even">
        <td class="tdleft">
            <a href="javascript:;" class="package-link" title="Value Added Tax (VAT) Registration with HM Revenue &amp; Customs.">
               VAT Application
            </a>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td class="check">&nbsp;</td>
    </tr>
    {* <tr>
        <td class="tdleft">
            <a href="javascript:;" class="package-link" title="Self-inking company stamp. Custom made with your company name and number.">
               Company Stamp
            </a>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td class="check">&nbsp;</td>
    </tr>
    <tr class="even">
        <td class="tdleft">
            <a href="javascript:;" class="package-link" title="Creates an embossed image on paper or card with your company name and number.">
               Company Seal
            </a>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td class="check">&nbsp;</td>
    </tr>
    *}
 
    <tr class="top-up-head white-row"><!-- bottom matrix -->
        <td rowspan="{if $showBuyLink}5{else}4{/if}">
            &nbsp;
        </td>
        <td style="padding: 0;"><a href="{$packages.109->getLink()}"><img src="{$urlImgs}boxes/top/bronze-top.png" width="78" height="30"/></a></td>
        <td style="padding: 0;"><a href="{$packages.110->getLink()}"><img src="{$urlImgs}boxes/top/bronze-plus-top.png" width="78" height="30"/></a></td>
        <td style="padding: 0;"><a href="{$packages.111->getLink()}"><img src="{$urlImgs}boxes/top/silver-top.png" width="78" height="30"/></a></td>
        <td style="padding: 0;"><a href="{$packages.112->getLink()}"><img src="{$urlImgs}boxes/top/gold-top.png" width="78" height="30"/></a></td>
        <td style="padding: 0;"><a href="{$packages.113->getLink()}"><img src="{$urlImgs}boxes/top/platinum-top.png" width="78" height="30"/></a></td>
        <td style="padding: 0;"><a href="{$packages.279->getLink()}"><img src="{$urlImgs}boxes/top/diamond-top.png" width="78" height="30"/></a></td>
    
    </tr>
    <tr class="top-midd-head">
        <td class="p-bronze-midd"><strong>&nbsp;</strong></td>
        <td class="p-bronze-plus-midd"><strong>&nbsp;</strong></td>
        <td class="p-silver-midd"><strong>&nbsp;</strong></td>
        <td class="p-gold-midd"><strong>&nbsp;</strong></td>
        <td class="p-platinum-midd"><strong>&nbsp;</strong></td>
        <td class="p-diamond-midd"><strong>&nbsp;</strong></td>
    </tr>
    <tr class="white-row">
        <td style="color: #999;">&pound;{$packages.109->productValue}</td>
        <td style="color: #999;">&pound;{$packages.110->productValue}</td>
        <td style="color: #999;">&pound;{$packages.111->productValue}</td>
        <td style="color: #999;">&pound;{$packages.112->productValue}</td>
        <td style="color: #999;">&pound;{$packages.113->productValue}</td>
        <td style="color: #999;">&pound;{$packages.279->productValue}</td>
    </tr>
    {if $showBuyLink}
    <tr class="white-row">
    	<td><strong><a href="{url route='basket_module_package_basket_add_package' packageId=109}">Buy now</a></strong></td>
    	<td><strong><a href="{url route='basket_module_package_basket_add_package' packageId=110}">Buy now</a></strong></td>
    	<td><strong><a href="{url route='basket_module_package_basket_add_package' packageId=111}">Buy now</a></strong></td>
    	<td><strong><a href="{url route='basket_module_package_basket_add_package' packageId=112}">Buy now</a></strong></td>
    	<td><strong><a href="{url route='basket_module_package_basket_add_package' packageId=113}">Buy now</a></strong></td>
    	<td><strong><a href="{url route='basket_module_package_basket_add_package' packageId=279}">Buy now</a></strong></td>
	</tr>
    {/if}
    <tr class="white-row">
    	<td><strong><a href="{$packages.109->getLink()}">More info</a></strong></td>
    	<td><strong><a href="{$packages.110->getLink()}">More info</a></strong></td>
    	<td><strong><a href="{$packages.111->getLink()}">More info</a></strong></td>
    	<td><strong><a href="{$packages.112->getLink()}">More info</a></strong></td>
    	<td><strong><a href="{$packages.113->getLink()}">More info</a></strong></td>
    	<td><strong><a href="{$packages.279->getLink()}">More info</a></strong></td>
	</tr><!--end up matrix -->
    
    
</table>

</div>



{literal}

<script type="text/javascript">
$(document).ready( function(){
        $('.package-link').tooltip({
    		position: 'bottom right',
    		offset: [0, 15],
    		effect: 'slide',
            delay: 0,    
            opacity: 0.9    
        });
        
});

</script>
{/literal}
