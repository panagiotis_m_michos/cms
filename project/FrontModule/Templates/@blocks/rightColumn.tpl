
{* DISABLE RIGHT COLUMN *}
{if !$disabledRightColumn}

	<!-- ================================================ RIGHT COLUMN =========================================================== -->
	<div id="rightCol">	
			<a href="/company-formation-name-search.html"><img src="{$urlImgs}banner1.png" width="187" height="172" alt="Click here to form your company for just" /></a>
		
			
			{* The Facts - Homepage only *}
		    {if $this->node->getId() == 44}
				<div style="margin-top: 10px;">
					<a href="{$this->router->link(746)}">
						<img src="{$urlImgs}the-facts-banner.png" alt="the facts forming a uk limited company" width="187" height="121" />
					</a>
				</div>
			{/if}
			
			{* OLD BARCLAYS BOX 35 BACK
			<div class="barclays-hp-banner">
				<a href="{$this->router->link(447)}"><span>with all our <strong>Company Formation</strong> packages when you open a Barclays <strong>Business Bank Account!*</strong></span></a>
			</div>
			*}
			
			{* BARCLAYS 50 CASH BACK *}
            <div class="barclays-home-sprite">
                <a href="{$this->router->link(447)}" style="text-decoration: none;">
                    <p style="color: #ff6600; font-weight: bold; font-size: 42px; line-height: 42px; padding-top: 0;">&pound;50</p>
                    <p style="color: #ff6600; font-weight: bold; font-size: 26px; line-height: 8px; padding-top: 0;">CASH BACK</p>
                    <p style="font-size: 11px; padding:0; color: #00adef;">With every company formation <br />when you open a free:</p>
                    <p style="font-size: 11px; color: #00adef; line-height: 11px; padding-top: 25px; text-align: right; padding-right: 14px;">Business Bank Account</p>
                    <p style="font-size: 11px; line-height: 0px;"><a href="{$this->router->link(447)}">Click here for more info</a></p>
               </a>
            </div>

            {*if isset($rightBlockStartUpGuide)}
                {include file="@blocks/startUpGuide.tpl"}
             {/if*}

            {*<div class="need-help-home-new">
                <h3>Need Help?</h3>
                <div>
                    <p><a href="mailto:info@madesimplegroup.com"><u>Email us</u></a></p>
                    <p><a href="callto:+44 207 608 5500">+44 (0) 207 608 5500</a></p>
                    <li><a href="#" class="support_link"><img src="{$urlImgs}chat_icon.png" alt="Live Support" /><u>Live Support</u></a></li>
                    <p style="padding-top: 10px;"><a href="{$this->router->link(92)}"><u>FAQs</u></a></p>
                    <p><a href="http://twitter.com/madesimplegroup"><u>Twitter</u></a></p>
                    <p><a href="http://www.facebook.com/MadeSimpleGroup"><u>Facebook</u></a></p>	
                    <p><a href="https://plus.google.com/112476242712086821280"><u>Google+</u></a></p>	
                </div>
            </div>
			<div class="box1 mtop">
				<h3>All our products include:</h3>
				<div class="box1-foot pbottom15">
					<ul class="special-list mbottom">
					{foreach from=$rightBlock1 item="item" name="f11"}
						<li{if $smarty.foreach.f11.last} class="last"{/if}><a href="{$item.link}">{$item.title|replace:"&":"&amp;"}</a></li>
					{/foreach}
					</ul>
					<a href="{$this->router->link("SearchControler::SEARCH_PAGE")}" class="btn_more mtop_small">Plus more</a>
				</div>
			</div>*}
			
			{* 
			<div class="box1 mtop">
				<h3>For existing customers</h3>
				<div class="box1-foot">
					<ul class="special-list">
					{foreach from=$rightBlock2 item="item" name="f12"}
						<li{if $smarty.foreach.f12.last} class="last"{/if}><a href="{$item.link}">{$item.title|replace:"&":"&amp;"}</a></li>
					{/foreach}
					</ul>
				</div>
			</div>
			
			<div class="box-grey">
				<div class="box-grey_top">
					<div class="box-grey_foot">
						<p>Small Business Advisors<br />
						<strong>FREE</strong> Resources</p>
						<a href="{$this->router->link("88")}" class="btn_more mtop_small">Click here for more</a>
					</div>
				</div>
			</div>
			
			<div class="box-grey">
				<div class="box-grey_top">
					<div class="box-grey_foot">
						<p>Our Promise</p>
						<a href="{$this->router->link("68")}" class="btn_more mtop_small">Click here for more</a>
					</div>
				</div>
			</div>
			<div class="box-grey">
				<div class="box-grey_top">
					<div class="box-grey_foot">
						<p>Terms &amp; Conditions</p>
						<a href="{$this->router->link("473")}" class="btn_more mtop_small">Click here for more</a>
					</div>
				</div>
			</div>	
			*}
	</div>
{/if}
	