<div id="megamenu" class="container">
    <div id="menu1" class="menu_container green_glass full_width">
        <div class="mobile_collapser">
            <label for="hidden_menu_collapser">
                <span class="mobile_menu_icon"></span> Menu
            </label>
        </div>
        <input id="hidden_menu_collapser" type="checkbox">
        <ul>
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="{$this->router->link("102")}">Company Formation</a>
                <div class="menu_dropdown_block half_width">
                    <div class="container">
                        <div class="column span2 bordered">
                            <div class="content">
                                <ul class="menu_submenu">
                                    <li><a href="{$this->router->link("102")}">Company Formation Packages</a></li>
                                </ul>
                            </div>
                            <div class="content">
                                <h2>Company Types</h2>
                                <ul class="menu_submenu">
                                    <li><a href="{$this->router->link("72")}">UK Limited Company Formation</a></li>
                                    <li><a href="{$this->router->link("74")}">Limited by Guarantee Company</a></li>
                                    <li><a href="{$this->router->link("73")}">Limited by Shares Company</a></li>
                                    <li><a href="{$this->router->link("76")}">Limited Liability Partnership</a></li>
                                    <li><a href="{$this->router->link("75")}">Public Limited Company</a></li>
                                    <li><a href="{$this->router->link("237")}">Flat Management Company</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="column span2">
                            <div class="content">
                                <h2>Other company services</h2>
                                <ul class="menu_submenu">
                                    <li><a href="{$this->router->link("1435")}">International Package</a></li>
                                    <li><a href="{$this->router->link("62")}">Offshore Formation</a></li>
                                    <li><a href="{$this->router->link("239")}">Reserve a Company Name</a></li>
                                    <li><a href="{$this->router->link("1288")}">Shelf Companies</a></li>
                                    <li><a href="{$this->router->link("819")}">Sole Trader Start Up Pack</a></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </li>
            <li class="container">
                <a href="{$this->router->link("67")}">How It Works</a>
                <div class="menu_dropdown_block ">
                    <div class="container">
                        <ul class="menu_submenu">
                            <li><a href="{$this->router->link("67")}">Overview</a></li>
                            <li><a href="{$this->router->link("972")}">Why Choose us?</a></li>
                            <li><a href="{$this->router->link("749")}">Reviews and Testimonials</a></li>
                        </ul>
                    </div>
                </div>
            </li>
            <li>
                <a href="{$this->router->link("1423")}">Additional Services</a>
                <div class="menu_dropdown_block full_width">
                    <div class="container">
                        <div class="column span4 bordered">
                            <div class="content">
                                <ul class="menu_submenu">
                                    <li><a href="{$this->router->link("82")}">Renewals</a></li>
                                </ul>
                            </div>
                            <div class="content">
                                <h2>Accountancy and Legal</h2>
                                <ul class="menu_submenu">
                                    <li><a href="{$this->router->link("251")}">Accountancy Consultation</a></li>
                                    <li><a href="{$this->router->link("672")}">Confirmation Statement</a></li>
                                    <li><a href="{$this->router->link("368")}">Change of Accounting Reference Date</a></li>
                                    <li><a href="{$this->router->link("845")}">Company Name Change Service</a></li>
                                    <li><a href="{$this->router->link("257")}">Company Dissolution</a></li>
                                    <li><a href="{$this->router->link("255")}">VAT and PAYE Registration Assistance</a></li>
                                    <li><a href="{$this->router->link("785")}">Dormant Company Accounts Service</a></li>
                                    <li><a href="{$this->router->link("261")}">Share Services</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="column span4 bordered">
                            <div class="content">
                                <h2>Office & Address Services</h2>
                                <ul class="menu_submenu">
                                    <li><a href="{$this->router->link("557")}">Mail Forwarding</a></li>
                                    <li><a href="{$this->router->link("259")}">Registered Office Service</a></li>
                                    <li><a href="{$this->router->link("476")}">Service Address Service</a></li>
                                    <li><a href="{$this->router->link("250")}">Virtual Office</a></li>
                                </ul>
                            </div>
                            <div class="content">
                                <h2>Documents</h2>
                                <ul class="menu_submenu">
                                    <li><a href="{$this->router->link("83")}">Apostilled Documents Service</a></li>
                                    <li><a href="{$this->router->link("85")}">Certificate of Good Standing</a></li>
                                    <li><a href="{$this->router->link("262")}">Supplementary Company Documents</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="column span4 bordered">
                            <div class="content">
                                <h2>Official Company Products</h2>
                                <ul class="menu_submenu">
                                    <li><a href="{$this->router->link("256")}">Company Pack (Register, Seal & Stamp)</a></li>
                                    <li><a href="{$this->router->link("256")}">Company Register</a></li>
                                    <li><a href="{$this->router->link("256")}">Company Seal</a></li>
                                    <li><a href="{$this->router->link("256")}">Company Stamp</a></li>
                                </ul>
                            </div>
                            <div class="content">
                                <h2>Partners and Affiliates</h2>
                                <ul class="menu_submenu">
                                    <li><a href="{$this->router->link("908")}">Affiliate Program</a></li>
                                    <li><a href="{$this->router->link("1283")}">White Label Company Formation</a></li>
                                    <li><a href="{$this->router->link("1110")}">Wholesale</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="column span4">
                            <div class="content">
                                <h2>Other</h2>
                                <ul class="menu_submenu">
                                    <li><a href="{$this->router->link("928")}">Business Apps</a></li>
                                    <li><a href="{$this->router->link("555")}">Guides</a></li>
                                    <li><a href="{$this->router->link("573")}">Business Templates/Forms</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li class="container">
                <a href="http://support.companiesmadesimple.com">Help and Advice</a>
                <div class="menu_dropdown_block ">
                    <div class="container">
                        <ul class="menu_submenu">
                            <li><a href="http://support.companiesmadesimple.com/category/132-frequently-asked-questions">FAQs</a></li>
                            <li><a href="{$this->router->link("88")}">Small Business Advisor</a></li>
                            <li><a href="{$this->router->link("726")}">Sole Trader v Limited Company Calculator</a></li>
                            <li><a href="http://support.companiesmadesimple.com/article/286-glossary">Glossary</a></li>
                        </ul>
                    </div>
                </div>
            </li>
            <li class="right last">
                <a href="{$this->router->link("65")}">My Account</a>
                <div class="menu_dropdown_block">
                    <div class="container">
                        <ul class="menu_submenu">
                            <li><a href="{$this->router->link("93")}">Dashboard</a></li>
                            <li><a href="{$this->router->link("95")}">My Companies</a></li>
                            <li><a href="{$this->router->link("96")}">My Details</a></li>
                            <li><a href="{$this->router->link("97")}">Order History</a></li>
                            <li><a href="{$this->router->link("98")}">Business Library</a></li>
                            <li><a href="{$this->router->link("677")}">Company Import</a></li>
                            <li><a href="{$this->router->link("945")}">My Monitored Companies</a></li>
                        </ul>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>
