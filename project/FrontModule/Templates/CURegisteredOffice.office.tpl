{include file="@header.tpl"}

{* JS ADDRESSES FOR PREFILL *}
<script type="text/javascript">
	var addresses = {$jsAdresses nofilter};
</script>

{literal}
<script type="text/javascript">
$(document).ready(function () {

	// start on beginning
	ourRegisterOfficeHandler();
	
	// called on type click
	$("input[name='ourRegisteredOffice']").click(function () {
		ourRegisterOfficeHandler(); 
	});
	
	
	/**
	 * Provides disable and enable dropdown for our offices
	 * @return void
	 */
	function ourRegisterOfficeHandler()
	{
		var disable = $("#ourRegisteredOffice").is(":checked");
		$('#prefill').attr("disabled", disable);
		$('#premise').attr("disabled", disable);
		$('#street').attr("disabled", disable);
		$('#thoroughfare').attr("disabled", disable);
		$('#post_town').attr("disabled", disable);
		$('#postcode').attr("disabled", disable);
		$('#county').attr("disabled", disable);
		$('#country').attr("disabled", disable);
	}
	
	// prefill address	
	$("#prefill").change(function () {
		var value = $("#prefill").val();
		address = addresses[value];
		for (var name in address) {
			$('#'+name).val(address[name]);
		}
	});
	
});
</script>
{/literal}
<div id="maincontent2">
	<p style="margin: 0 0 15px 0; font-size: 11px;"><a href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}">{$companyName}</a> &gt; {$title}</p> 
	{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
	{$form nofilter}
</div>
{include file="@footer.tpl"}
