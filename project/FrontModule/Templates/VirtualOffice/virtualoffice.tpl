{include file="@header.tpl"}

<script type="text/javascript">

    $(document).ready(function () {

        $('#virtual-office-offer').on('click', function() {
            _gaq.push(['_trackEvent', 'Links', 'Click', 'Get Virtual Office Service']);

            payment();
            return false;
        });

        function payment()
        {
            var oneClickPage = "{$this->router->link("VirtualOfficeControler::ONE_CLICK_PAYMENT_PAGE", ['company_id' => $companyId])}";
            var $overlay = $('<div class="virtual_office_mail_answering payment_virtual_office" ><iframe id="overlay_iframe" src="' + oneClickPage + '" style="margin-left: 20px; margin-top: 10px; " frameborder="0" width="700" height="570"></iframe></div>');
            $('body').append($overlay);
            $overlay.overlay({
                effect: 'apple',
                load: true,
                left:"center",
                mask: '#000',
                oneInstance: false,
                api : true,
                top : "0px",
                height: "500px",
                onClose: function(){
                    window.location.reload(true);
                },
                onBeforeLoad: function() {
                    // grab wrapper element inside content
                    var wrap = this.getOverlay().find(".contentWrap");

                    // load the page specified in the trigger
                    wrap.attr('src', this.getTrigger().attr("href"));
                }
            });
        }
    });
</script>

<div class="f-sterling">
    <div class="bg-grey1 hp-mfoffer posrelative">
        <div class="ptop10">
            <h2 class="white massivefont fweight300 bg-blue1 inlineblock pleft40 pright30 ptop10 pbtm10">Exclusive one-time offer!</h2>
        </div>
        <div class="pleft40 pright30 pbtm30 ptop15">
            <h1 class="grey12 font48 txtbold">Try our Mail Forwarding service<br />FREE for 3 months</h1>
            <h2 class="grey12 massivefont fweight300">Get a <span class="txtbold">prestigious business address in London</span>
                with 1st Class mail<br />forwarding for 3 months - just pay £20 to cover your postage fees.
            </h2>
            <div class="mtop20">
                {ui name="button" text="Yes please, I’ll try it" size="m" idAttr="virtual-office-offer" loading="expand-right"}
            </div>
            <div>
                <p class="grey12 largefont fweight300 pbtm20">
                    Easy and secure one-click payment using card ending {$token->cardNumber}<br />
                    You'll confirm your purchase on the next screen.
                </p>
                <a class="massivefont fweight300 mtop20" href="{$this->router->link("CFRegisteredOfficeControler::REGISTER_OFFICE_PAGE", "company_id=$companyId")}">No thanks, continue to company registration details</a>
            </div>
        </div>
    </div>
    <div class="padding30">
        <h2 class="grey10 massivefont txtbold mtop0 mbottom20">Why use our Mail Forwarding service?</h2>
        <ul class="grey10 massivefont fweight300 facheck">
            <li class="facheck">Keep your home address private from customers and suppliers</li>
            <li class="facheck">Make a great impression with a professional and established London address</li>
            <li class="facheck">Save on the costly overheads of a physical office location</li>
        </ul>
    </div>
    <div class="hp-mfquote posrelative bg-grey1 padding30 txtcenter">
        <h2 class="grey8 massivefont txtbold mbottom0">“Having a London address changed the perception of our business”</h2>
        <span class="grey14 largefont fweight300">Oli Chowdry, Webimpulse</span>
    </div>
    <div class="padding30">
        <h2 class="grey14 massivefont fweight300 mbottom0">How it works</h2>
        <p class="grey14 largefont fweight300 pbottom15">A non-refundable £20 deposit will be taken, from which postage (1st class) and handling fees (10p per letter) are deducted. If this runs out while the service is active we will ask you to top-up your postage deposit. There is no auto renewal after taking up this one-time offer. After three months, if you choose to continue the service, you will be asked to sign up to one of our mail forwarding packages starting from £19.99/ month + VAT. You can cancel at any time by providing just one month’s notice. Proof of identity and address will be required to activate your service (e.g. driver’s license and utility bill).</p>
        <h2 class="grey14 massivefont fweight300 mbottom0">Proof of ID</h2>
        <p class="grey14 largefont fweight300">We have a legal obligation to check proof of ID and proof of address for all our customers, prior to activating your service. This is to ensure we comply with Anti-Money Laundering (AML) regulations and Know Your Customer (KYC) requirements. All you need to do is send us two scanned documents and we’ll do the rest - you can check what documents you must provide <a href="/proof-of-id.html" class="popupnew">here</a>. </p>
    </div>
</div>


{include file="@footer.tpl"}
