{include file="@header.tpl"}
{literal}
    <style>
        <!--
        .midfont {
            font-size: 12px;
        }
        .btn {
            height: 22px;
        }
        .buttons{
            font-size: 12px;
            padding: 1px 3px 4px 3px;
            text-decoration: none;
        }
        label{
            padding-right:10px;
            font-weight:bold;
        }
        input{
           vertical-align: top;
        }
        .my-companies td span.order {
            position: relative;
        }
            .my-companies td span.order a {
                font-size: 9px;
            }
                .my-companies td span.order a:hover {
                    color: black;
                }
                .my-companies td span.order a.active {
                    color: black;
                }
                .my-companies td  span.order a.asc {
                    position: absolute;
                    right: -11px;
                    top: -2px;
                    color: silver;
                    text-decoration: none;
                }
                .my-companies td span.order a.desc {
                    position: absolute;
                    right: -11px;
                    top: 6px;
                    color: silver;
                    text-decoration: none;
                }
                label[for=active]{
                    padding-right: 0px;
                }

        -->
    </style>
{/literal}

<div id="maincontent">
	{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}

        {* FORM BEGIN *}
        {$formList->getBegin() nofilter}
        {$formList->getHtmlErrorsInfo() nofilter}
        <fieldset>
        <legend>Filter/Export</legend>
            <table>
                <col width="100">
                <col width="200">
                <col width="100">
                <col width="100">
                <col width="120">
                <col width="80">
                <col width="80">
                <col width="80">
                <tr>
                    <td><b>Filter by:</b></td>
                    <td>{$formList->getControl('selector') nofilter}</td>
                    <td>{$formList->getLabel("dateFrom") nofilter} {$formList->getControl('dateFrom') nofilter}</td>
                    <td>{$formList->getLabel("dateTo") nofilter} {$formList->getControl('dateTo') nofilter}</td>
                    <td>{$formList->getControl('companyFilter') nofilter}</td>
                    <td><a class="ui-widget-content ui-corner-all buttons"href="{$this->router->link("CompaniesMonitoringControler::COMPANIES_MONITORING_PAGE")}">Clear Filter</a></td>
                    <td><a href="{$this->router->link(NULL, 'csv=1')}" class="ui-widget-content ui-corner-all buttons">CSV Export</a></td>
                    <td><a href="{$this->router->link(NULL, 'syncAll=1')}" class="ui-widget-content ui-corner-all buttons">Sync All</a></td>
                </tr>
            </table>
        </fieldset>
        {$formList->getEnd() nofilter}
    {* FORM BEGIN *}
    {$formAdd->getBegin() nofilter}
	{if $formAdd->getError("companyNumber")}
		<p class="ff_err_notice">{$formAdd->getError("companyNumber")}</p>
	{/if}
	{* INCOPORATED COMPANIES *}
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="my-companies">
	<col width="210">
	<col width="100">
	<col width="100">
	<col width="100">
	<col width="100">
    <col width="100">
    <col width="60">

	<tr>
		<th colspan="3" class="bbottom">Monitored Companies</th>
        <th colspan="4" class="bbottom">{$formAdd->getLabel("companyNumber") nofilter}{$formAdd->getControl("companyNumber") nofilter} {$formAdd->getControl("submit") nofilter}</th>
        
	</tr>
        {if !empty($customerCompaniesList)}
            <tr class="odd midfont">
                <td>
                    <span class="order"><strong>Company Name</strong>
                        <a class="asc" title="ASC" href="{$this->router->link(NULL, "order=companyNameA")}">▲</a>
                        <a class="desc" title="DESC" href="{$this->router->link(NULL, "order=companyNameD")}">▼</a>
                    </span>
                </td>
                <td>
                    <span class="order"><strong>Company No.</strong>
                        <a class="asc" title="ASC" href="{$this->router->link(NULL, "order=companyNumberA")}">▲</a>
                        <a class="desc" title="DESC" href="{$this->router->link(NULL, "order=companyNumberD")}">▼</a>
                    </span>
                </td>
                <td align="center">
                    <span class="order"><strong>Incorp Date</strong>
                        <a class="asc" title="ASC" href="{$this->router->link(NULL, "order=incorporationDateA")}">▲</a>
                        <a class="desc" title="DESC" href="{$this->router->link(NULL, "order=incorporationDateD")}">▼</a>
                    </span>
                </td>
                <td align="center">
                    <span class="order"><strong>Status</strong>
                        <a class="asc" title="ASC" href="{$this->router->link(NULL, "order=companyStatusA")}">▲</a>
                        <a class="desc" title="DESC" href="{$this->router->link(NULL, "order=companyStatusD")}">▼</a>
                    </span>
                </td>
                <td align="center">
                    <span class="order"><strong>Accounts Due</strong>
                        <a class="asc" title="ASC" href="{$this->router->link(NULL, "order=accountsNextDueDateA")}">▲</a>
                        <a class="desc" title="DESC" href="{$this->router->link(NULL, "order=accountsNextDueDateD")}">▼</a>
                    </span>
                </td>
                <td align="center">
                    <span class="order"><strong>Returns Due</strong>
                        <a class="asc" title="ASC" href="{$this->router->link(NULL, "order=returnsNextDueDateA")}">▲</a>
                        <a class="desc" title="DESC" href="{$this->router->link(NULL, "order=returnsNextDueDateD")}">▼</a>
                    </span>
                </td>
                <td align="center"></td>
            </tr>

            {foreach from=$customerCompaniesList key="key" item="company" name="f1"}
                <tr{if $smarty.foreach.f1.iteration % 2 == 0} class="odd"{/if}>
                    <td>{$company.companyName}</td>
                    <td align="center">{$company.companyNumber}</td>
                    <td align="center">{$company.incorporationDate|date_format:"%d-%m-%Y"}</td>
                    <td align="center">{$company.companyStatus}</td>

                    {if $company.accountsNextDueDate|date_format:"%Y/%m/%d" <= $smarty.now|date_format:"%Y/%m/%d" }
                        <td align="center"><b style="color:red;">{$company.accountsNextDueDate|date_format:"%d-%m-%Y"}</b></td>
                    {elseif $company.accountsNextDueDate|date_format:"%Y/%m/%d" <= $reminderDate }
                        <td align="center"><b style="color:#FF6600;">{$company.accountsNextDueDate|date_format:"%d-%m-%Y"}</b></td>
                    {else}
                        <td align="center">{$company.accountsNextDueDate|date_format:"%d-%m-%Y"}</td>
                    {/if}

                    {if $company.returnsNextDueDate|date_format:"%Y/%m/%d" <= $smarty.now|date_format:"%Y/%m/%d" }
                        <td align="center"><b style="color:red;">{$company.returnsNextDueDate|date_format:"%d-%m-%Y"}</b></td>
                    {elseif  $company.returnsNextDueDate|date_format:"%Y/%m/%d" <= $reminderDate }
                        <td align="center"><b style="color:#FF6600;">{$company.returnsNextDueDate|date_format:"%d-%m-%Y"}</b></td>
                    {else}
                        <td align="center">{$company.returnsNextDueDate|date_format:"%d-%m-%Y"}</td>
                    {/if}
                    <td><a onclick="return confirm('Are you sure?');" class="ui-widget-content ui-corner-all buttons" href="{$this->router->link(NULL, "delete=$key")}">Delete</a></td>
                </tr>
            {/foreach}

    {/if}
    {$formAdd->getEnd() nofilter}
                <tr>
                <td>{$paginator nofilter}</td>
                <td colspan="2">
                    {$activeform->getBegin() nofilter} {$activeform->getControl('active') nofilter}{$activeform->getLabel('active') nofilter} 
                </td> 
                <td style="padding-left: 0px;">  
                    <div class="help-button"  style="margin-left: 0px; margin-right: 0px;">
                        <a class="help-icon" href="#">help</a>
                        <em style="display: none;">Will only show companies with an Active status. All others will be hidden until unticked.</em>
                    </div>                   
                    {$activeform->getEnd() nofilter}</td>
                <td colspan="3"><p style="float: right; font-weight: bold;">Key: <span style="color: #ff0000;">Overdue</span> / <span style="color: #ff6600;">Due in 2 months</span></p></td>
            </tr>
     </table>
	<div class="clear"></div>
</div>
{literal}

<script>
$(document).ready(function () {
	$('#active').live('click',function() {
    $(this).closest('form').submit();
    });
});
</script>
{/literal}

{include file="@footer.tpl"}
