<div class="clear"></div>
<div id="footer">
    <a href="/" rel="nofollow" target="_blank">Company Formation</a> |
    <a href="https://www.companysearchesmadesimple.com" rel="nofollow" target="_blank">Company Search</a> |
    <a href="https://www.businesstrainingmadesimple.co.uk" rel="nofollow" target="_blank">Business Training</a> |
    <a href="https://www.londonpresence.com" rel="nofollow" target="_blank">Virtual Office</a> |
    <a href="https://www.madesimplegroup.com" rel="nofollow" target="_blank">MadeSimple Group</a>
    <br />
    <p>Company Formation MadeSimple provides company formation services and is a division of the Made Simple Group Ltd<br />
    Registered Office address: 20-22 Wenlock Road, London, N1 7GU, UK<br />
    Company Number: 04214713 Vat Number: GB820956327</p>
    <br />
</div>
</div>
{literal}
<!-- Google Plus --> 
<script type="text/javascript">
    (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
})();
</script>
{/literal}
</body>
</html>
