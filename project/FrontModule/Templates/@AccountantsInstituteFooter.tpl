  <div class="fotter"> 
    <p>
        <a href="https://www.madesimplegroup.com">Made Simple Group</a> |
        <a href="http://www.westbury.co.uk/">London Chartered Accountants</a> |
        <a href="https://www.londonpresence.com">London Virtual Office</a> |
        <a href="https://www.businesstrainingmadesimple.co.uk/">Business Training</a> |
        <a href="https://www.londonpresence.com/">Serviced Offices</a> |
		<a href="https://www.companysearchesmadesimple.com/">Company Credit Check</a> |
		<a href="http://www.taxreturnsmadesimple.com/">Tax Returns</a>
        <br/>Companies Made Simple provides company formation services and is a division of the Made Simple Group Ltd
        <br/>Registered Office address: 145 - 157 St John St, London, EC1V 4PY
        <br/>Company Number: 05104525 Vat Number: GB820956327
        <br/>Copyright &copy; 2011 Made Simple Group - all rights reserved
    </p>
</div>
</div>
</div> <!-- #envelope -->

</body>
</html> 
