{include file="WidgetWhiteLabel/@header.tpl"}

<h1>IMPORTANT NOTICE:</h1>
<p>Please note you are about to be re-directed away from this website to <strong>Companies Made Simple</strong> where you will be able to pay for your company formation and complete your application before submission to Companies House.</p> 
<p>A confirmation of what you have chosen is below:</p>

{$table nofilter}

<p><strong>Reminder</strong>: You have not yet completed this formation. Once payment is made you will still need to appoint your registered office and company officers for this company via the wizard.</p> 
<p>If you require any assistance please contact our support on 0207 608 5500.</p>


{$form->getBegin() nofilter}
	<p>{$form->getControl('continue') nofilter}</p>
{$form->getEnd() nofilter}

{include file="WidgetWhiteLabel/@footer.tpl"}