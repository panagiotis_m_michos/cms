{include file="WidgetWhiteLabel/@header.tpl"}

{* SEARCH FORM *}
<h2>Is your company name available? Search NOW!</h2>
{$searchForm nofilter}

{* LOGIN FORM *}
{if !isset($customer)}
	<h2>or Login</h2>
	{$loginForm nofilter}
{/if}

{include file="WidgetWhiteLabel/@footer.tpl"}
