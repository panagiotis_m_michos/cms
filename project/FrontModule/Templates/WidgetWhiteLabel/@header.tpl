<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"> 
<head> 
	<title>{$title}</title>
	<link rel="stylesheet" href="{$urlCss}forms.css" type="text/css" /> 
	<link rel="stylesheet" href="{$urlCss}affiliate-general.css" type="text/css" />
	<link rel="stylesheet" href="{$urlCss}jquery-ui-1.8.1.custom.css" type="text/css" />
	
	
	<style>
	<!--
	.company-color {ldelim}
		color: #{$companyColour};
	{rdelim}
	h1 {ldelim}
		color: #{$companyColour};
	{rdelim}
	fieldset {ldelim}
		background: #{$companyColour};
		border: 1px solid #{$companyColour};
	{rdelim}
	fieldset label {ldelim}
		color: #{$textColour};
	{rdelim}
	table.htmltable th {ldelim}
		background: #{$companyColour};
		color: #{$textColour};
	{rdelim}
	-->
	</style>
	
	
</head>
<body>
	<div id="envelope">
		{include file='@blocks/flashMessage.tpl'}
		
		{if isset($customer)}
			<p style="text-align: right;">Logged as {$customer->email}, <a href="{$logoutLink}">Logout</a></p>
		{/if}