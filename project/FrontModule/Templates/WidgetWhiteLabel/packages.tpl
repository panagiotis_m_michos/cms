{include file="WidgetWhiteLabel/@header.tpl"}

{$form->getBegin() nofilter}

	<h1>Congratulations! Your company name "{$companyName|h}" is available.</h1>
	
	{* === FORM ERRORS === *}
	{if $form->hasErrors()}
		{$form->getHtmlErrors()}
	{/if}
	
	{* === PACKAGES === *}
	<h2>Company Packages</h2>
	
	<table class="htmltable" width="100%">
	<col>
	<col width="100">
	{if !empty($packages)}
		{foreach from=$packages key="id" item="package"}
			<tr>
				<th colspan="2">{$package->getLngTitle() nofilter}</th>
			</tr>
			<tr>
				<td>
					{$package->getLngAbstract() nofilter}
				</td>
				<td class="center">
					<strong>&pound;{$package->price}</strong> + VAT<br />
					{$form->getControl('packageId', $id) nofilter}
				</td>
			</tr>
		{/foreach}
	{else}
		<tr>
			<td colspan="2">No packages</td>
		</tr>
	{/if}
	</table>

	{if isset($affiliate->textbox)}
	    {$affiliate->textbox}
	{/if}

	{* === ACTION === *}
	<fieldset>
		<table class="package-action">
			<tr>
				<td colspan="2">{$form->getControl('submit') nofilter}</td>
			</tr>
		</table>
	</fieldset>

{$form->getEnd() nofilter}


{include file="WidgetWhiteLabel/@footer.tpl"}
