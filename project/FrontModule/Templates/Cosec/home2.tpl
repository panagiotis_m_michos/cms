{include file="@CosecHeader.tpl"}

 <div id="bluebox">
    <h1>Simple Company Secretarial Management</h1>
    <div id="left_blue_box">
      <div id="left_blue_box_text">
        <h2>Free online company secretarial management system - it's really, really simple</h2>
        <p>Track dividends, create vouchers, file annual returns, set up alerts and more...</p>
      </div>
   
      <div id="left_blue_box_button">
      	<a href="{$this->router->link("#signup#")}">
				<p><b>SIGN UP</b>  - It's Free!</p>
        </a>
      </div>
	  
	  <!--
      <div style="clear:both;"></div>
	  <div id="left_blue_box_grey_text">
		<p>Sign up before 31 October 2010 and receive<br /><strong>100 Company Credit Reports*</strong></p>
	  </div>
	  -->
    </div>
    <div id="right_blue_box"><a href="#TB_inline?height=490&amp;width=600&amp;inlineId=slideShow2" class="thickbox"></a></div>
<div id="slideShow2" style="display: none;">{$slideShow nofilter}</div>
  </div>


  <div id="content">
    <div id="content_top_part">
      <div id="content_top_part_left">
        <h2>Our Products</h2>
        <div class="image_text img2">
            <a href="{$this->router->link("CosecControler::WHITE_PAGE")}">
              <br />
			  <h4>White Label Company Formation</h4>
              <p> Create your very own branded company formation service</p>
            </a>
        </div>
        <div class="image_text img1">
            <a href="{$this->router->link("CosecControler::AFFILIATE_PAGE")}">
              <br />
			  <br />
			  <h4>Affiliate Wholesale </h4>
              <p> Earn revenue via simple links on your website</p>
            </a>
        </div>
        <div class="image_text img3">
            <a href="{$this->router->link("CosecControler::ACCOUNTANTS_PAGE")}">
              <br />
			  <h4>Accountants &amp; Solicitors</h4>
              <p> Let us form your companies for you via our Professional Services Wholesale Company Formation service </p>
            </a>
</div>
      </div>
      <div id="content_top_part_right">
        <h2>Features</h2>
        <ul>
          <li>All companies are in one place</li>
          <li>Dividend tracker and vouchers</li>
          <li>Prepare and file annual return electronically - in just 4 simple steps</li>
          <li>Real time data sync with Companies House</li>
          <li>Print company documents</li>
          <li>Statutory documents templates</li>
          <li>Ability to bulk upload companies</li>
        </ul>
        <h2>Benefits</h2>
        <ul>
          <li>Saves time</li>
          <li>Saves cost</li>
          <li>Makes company secretarial management SIMPLE</li>
          <li>True Cloud Computing - data backed up automatically, daily.</li>
          <li>Trusted brand - powered by The Made Simple Group (Over 150,000 companies formed)</li>
          <li>Free to use the portal - only Annual Return service chargeable</li>
          <li>Accessed 24/7 from any internet connection</li>
          <li>No minimum number of clients required - whether 1 or 100 you only pay &pound;5 per client per Annual return</li>
          <li>Free support team on hand</li>
        </ul>
		<p style="padding-top: 12px;"><em>* NB. To be eligible for the 100 company credit reports, 5 companies must be imported on to your account.</em></p>
      </div>
    </div>
    <div id="content_bottom_part">
      <h2>What our Customers have to say:</h2>
      <div id="content_bottom_part_left">
        <div class="custamer_say">
          <div class="example-obtuse">
            <p>We have been using the Made Simple Group to form companies for us for almost a year now and we are very impressed with the wholesale service.  Companies are formed very promptly with the formation documents sent by email to us, which we then forward on to our clients so that they can set up bank accounts etc.  The service is highly efficient and offers great value for money.  We also receive regular newsletters highlighting forthcoming changes (eg, director service addresses) which helps us to keep up to date and to offer a proactive service to our clients.<br>
				<b>Keith Witchell ACA FCCA CTA KRW Accountants</b></p>
          </div>
        </div>
		<!--
        <div class="custamer_say2">
          <div class="triangle-obtuse">
            <p>"Companies Made Simple always provide an efficient and reliable service which is easy to use. They now handle all my Company formation requirements and I can confidently recommend them."</br>
<b>
Maximilian R. Hamilton, Director
Courtlands Hotel</b></p>
          </div>
        </div>
		-->
      </div>
      <div id="content_bottom_part_right">
        <div class="custamer_say2">
          <div class="triangle-obtuse">
            <p>"Companies Made Simple always provide an efficient and reliable service which is easy to use. They now handle all my Company formation requirements and I can confidently recommend them."</br>
<b>
Maximilian R. Hamilton, Director
Courtlands Hotel</b></p>
          </div>
        </div>
        <div class="custamer_say">
          <div class="example-obtuse">
            <p>1st Contact has made use of Companies Made Simple for well over 5 years and cannot fault their service. The most attractive part to what they offer is the simplicity of using their systems. The fact that everything can be performed online and very user friendly makes you appreciate the service you pay for. The customer service and account management have always been fantastic and should there ever be any problems or questions for CMS we are contacted within the hour with a proactive response.</br>
<b>Raymond Ridgeway
Commercial Manager, 1st Contact</b></p>
          </div>
        </div>
      </div>
    </div>
  </div>
  
{* SLIDESHOW *}


{include file="@CosecFooter.tpl"}
