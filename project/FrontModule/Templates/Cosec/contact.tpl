{include file="@CosecHeader.tpl"}

{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}

{$text nofilter}
<div id="text_content">
{$form nofilter}
</div>
{include file="@CosecFooter.tpl"}