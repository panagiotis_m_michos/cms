{include file="@CosecHeader.tpl"}
{if $visibleTitle}
<h1>{$title}</h1>
{/if}
{$text nofilter}

    {* GOOGLE TRACKING CODE *}
    {literal}
<script type="text/javascript">
    $(document).ready(function(){
    	$('input#login').click(function() {
    		pageTracker._trackPageview('/click/wholesale-registration');
    	});
    });
    </script>
{/literal}

 	{* FORM *}
{*{$form nofilter}*}
<div id="text_content"> {$form->getBegin() nofilter}
  <fieldset id="fieldset_0">
    <legend>Login details</legend>
    <table class="ff_table">
      <tbody>
        <tr>
          <th>{$form->getLabel('email') nofilter}</th>
          <td>{$form->getControl('email') nofilter}</td>
          <td><span class="ff_control_err">{$form->getError('email')}</span></td>
        </tr>
        <tr>
          <th>{$form->getLabel('password') nofilter}</th>
          <td>{$form->getControl('password') nofilter}</td>
          <td><span class="ff_control_err">{$form->getError('password')}</span></td>
        </tr>
        <tr>
          <th>{$form->getLabel('passwordConf') nofilter}</th>
          <td>{$form->getControl('passwordConf') nofilter}</td>
          <td><span class="ff_control_err">{$form->getError('passwordConf')}</span></td>
        </tr>
      </tbody>
    </table>
  </fieldset>
  <fieldset id="fieldset_1">
    <legend>Personal details</legend>
    <table class="ff_table">
      <tbody>
        <tr>
          <th>{$form->getLabel('titleId') nofilter}</th>
          <td>{$form->getControl('titleId') nofilter}</td>
        </tr>
        <tr>
          <th>{$form->getLabel('firstName') nofilter}</th>
          <td>{$form->getControl('firstName') nofilter}</td>
          <td><span class="ff_control_err">{$form->getError('firstName')}</span></td>
        </tr>
        <tr>
          <th>{$form->getLabel('lastName') nofilter}</th>
          <td>{$form->getControl('lastName') nofilter}</td>
          <td><span class="ff_control_err">{$form->getError('lastName')}</span></td>
        </tr>
      </tbody>
    </table>
  </fieldset>
  <fieldset id="fieldset_2">
    <legend>Address details</legend>
    <table class="ff_table">
      <tbody>
        <tr>
          <th>{$form->getLabel('address1') nofilter}</th>
          <td>{$form->getControl('address1') nofilter}</td>
          <td><span class="ff_control_err">{$form->getError('address1')}</span></td>
        </tr>
        <tr>
          <th>{$form->getLabel('address2') nofilter}</th>
          <td>{$form->getControl('address2') nofilter}</td>
          <td></td>
        </tr>
        <tr>
          <th>{$form->getLabel('address3') nofilter}</th>
          <td>{$form->getControl('address3') nofilter}</td>
          <td></td>
        </tr>
        <tr>
          <th>{$form->getLabel('city') nofilter}</th>
          <td>{$form->getControl('city') nofilter}</td>
          <td><span class="ff_control_err">{$form->getError('city')}</span></td>
        </tr>
        <tr>
          <th>{$form->getLabel('county') nofilter}</th>
          <td>{$form->getControl('county') nofilter}</td>
          <td></td>
        </tr>
        <tr>
          <th>{$form->getLabel('postcode') nofilter}</th>
          <td>{$form->getControl('postcode') nofilter} <span class="ff_desc">(If UK postcode please add 1 space in the middle)</span></td>
          <td><span class="ff_control_err">{$form->getError('postcode')}</span></td>
        </tr>
        <tr>
          <th>{$form->getLabel('countryId') nofilter}</th>
          <td>{$form->getControl('countryId') nofilter}</td>
          <td><span class="ff_control_err">{$form->getError('countryId')}</span></td>
        </tr>
        <tr>
          <th>{$form->getLabel('phone') nofilter}</th>
          <td>{$form->getControl('phone') nofilter} <span class="ff_desc">(Only numbers, no spaces, max 14 characters)</span></td>
          <td><span class="ff_control_err">{$form->getError('phone')}</span></td>
        </tr>
        <tr>
          <th>{$form->getLabel('additionalPhone') nofilter}</th>
          <td>{$form->getControl('additionalPhone') nofilter} <span class="ff_desc">(Only numbers, no spaces, max 14 characters) </span></td>
          <td></td>
        </tr>
      </tbody>
    </table>
  </fieldset>
  <fieldset id="fieldset_3">
    <legend>Action</legend>
    <table class="ff_table">
      <tbody>
        <tr>
          <th>{$form->getControl('login') nofilter}</th>
          <td><span class="ff_desc">By registering you agree to the <a href="cosec-terms-and-conditions.html">Terms and Conditions</a></span></td>
        </tr>
      </tbody>
    </table>
  </fieldset>
  {$form->getEnd() nofilter} </div>
{include file="@CosecFooter.tpl"}