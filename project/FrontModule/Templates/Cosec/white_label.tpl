{include file="@CosecHeader.tpl"}

{if $visibleTitle}
<h1>{$title}</h1>
{/if}
<div id="text_content">
<div id="left_doc_part">
{$text nofilter}
</div>
<div id="right_img_part">
{$templateRightImage->getHtmlTag(null,$title) nofilter}
</div>
</div>


{include file="@CosecFooter.tpl"}