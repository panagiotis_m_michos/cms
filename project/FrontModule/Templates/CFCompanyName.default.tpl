{include file="@header.tpl"}


<div id="maincontent2" style="width: 750px; margin: 0 20px 0 0;">
	<h1>Company name</h1>
	
	{* RESERVED WORD *}
	{if !empty($reserveWords)}
		<div class="flash error" style="margin: 15px 0 30px 0; width: 750px;">
			<h3 style="color: black; margin: 0; padding: 0;">Warning! You have used a reserved word(s) in your company name.</h3>
			{foreach from=$reserveWords item="reserveWord"} 
				<div style="margin: 20px 0 0px 0;">{$reserveWord->description nofilter}</div>
			{/foreach}
		</div>
	{/if}
	
	{$form->getBegin() nofilter}
	<fieldset>
		<legend>Enter your new company name</legend>
		<table>
            <col width="110">
		<tr>
			<td>{$form->getLabel("companyName") nofilter}</td>
			<td>{$form->getControl("companyName") nofilter}</td>
			<td>
				{if isset($available)}
					<span style="color: green; font-size: 11px; font-style: italic;">Company name is available</span>
				{else}
					<span class="ff_control_err">{$form->getError("companyName")}</span>
				{/if}
			</td>
		</tr>
		<tr>
			<td></td>
			<td>{$form->getControl("search") nofilter}</td>
		</tr>
		</table>
	</fieldset>
	
	<fieldset>
		<legend>Action</legend>
		<table>
		<tr>
			<td>{$form->getControl("back") nofilter}</td>
			{if isset($available)}
			<td>{$form->getControl("change") nofilter}</td>
			{/if}		
		</tr>
		</table>
	</fieldset>
	{$form->getEnd() nofilter}
</div>

{include file="@footer.tpl"}