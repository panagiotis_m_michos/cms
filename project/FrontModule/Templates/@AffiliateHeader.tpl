<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta name="description" content="{$description}" />
        <meta name="keywords" content="{$keywords}" />
        <title>{$seoTitle}</title>	

        <link rel="stylesheet" href="{$urlComponent}jquery-ui/themes/base/minified/jquery-ui.min.css" type="text/css" />
        <link rel="stylesheet" href="{$urlCss}project.css" type="text/css" />
        <link rel="stylesheet" href="{$urlCss}htmltable.css" type="text/css" />
        <link rel="stylesheet" href="{$urlCss}stylesheet.css"  type="text/css" />
        <link rel="stylesheet" href="{$urlCss}forms.css" type="text/css" />
        <link rel="stylesheet" href="{$urlCss}affiliate.css" type="text/css" />
        <link rel="shortcut icon" href="{$urlImgs}favicon.ico" type="image/x-icon" />
        <link rel="icon" href="{$urlImgs}favicon.ico" type="image/x-icon" />

        <script type="text/javascript" src="{$urlComponent}jquery/dist/jquery.min.js" ></script>
        <script type="text/javascript" src="{$urlComponent}jquery-ui/ui/minified/jquery-ui.custom.min.js"></script>
        <script type="text/javascript" src="{$urlComponent}jquery-migrate/jquery-migrate.min.js" ></script>
        <script type="text/javascript" src="{$urlJs}jquery.tools.min.js"></script> 
        <script language="JavaScript" src="{$urlJs}project.js" type="text/javascript"></script>
        <script language="JavaScript" src="{$urlJs}payment.js" type="text/javascript"></script>	

        {literal}
        <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-543198-9']);
        _gaq.push(['_trackPageview']);    

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
        </script>
        {/literal}
    </head>

    <body style="background-color: #eeebe9;">
        <div id="wrapper" style="padding: 0px 0px 10px 0px;">
            <div id="header">  
                <a href="/affiliate/" style="position:absolute; display:block;	height:114px; width:500px;"></a>
                <div id="topnav" >
                    {if isset($sign) && $sign == true} 
                    <a href="{$this->router->link("AffiliateControler::REGISTRATION_PAGE", "l=1")}">Logout</a> 
                    {else}
                    <a href="{$this->router->link("AffiliateControler::LOGIN_PAGE")}">Login</a> 
                    {/if}
                </div>		
            </div> <!-- #header -->
            <div id="content" style="border-top: 4px solid #009fda;padding: 10px;">

	    {include file='@blocks/flashMessage.tpl'}
