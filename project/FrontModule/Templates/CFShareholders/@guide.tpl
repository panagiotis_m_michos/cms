<div class="box1 fright">	
    <h3>Guide:</h3>
    <div class="box1-foot pbottom15">
        <p>A subscriber (shareholder) or member can be a company director or secretary or a new appointment.</p>
        <p>Shareholders and shareholding determine the ownership of a company.</p>		
        <p>For each shareholder the currency, number of shares issued and value per share needs to be specified.</p>
        <p>By default we enter 1 share per shareholder with a value per share of £1. This is suitable in most cases, but you may change it if you wish.</p>
        <p>
            <strong>Shares determine 2 things:</strong>
        <ul style="padding-left: 15px; margin: 2px 0 0 0;">
            <li>The amount the shareholder is liable to pay should the company ‘go under’.</li>
            <li>The ownership of the company (eg. If 2 shareholders. Shareholder X holding 51 shares and Shareholder Y holding 49 shares. Then shareholder X owns 51% of the company and Shareholder Y owns 49% of the company)</li>
        </ul>
        </p>
        <p><em>NB. The information provided above is correct based on our standard company setup (ie. Using the Standard Memorandum & Articles of Association). If you choose to upload your own, custom, Memorandum & Articles the above may not necessarily be true.</em></p>
        <p>
            <strong>The Technical Details:</strong>
        <ul style="padding-left: 15px; margin: 2px 0 0 0;">
            <li>The share class for all shares is: ‘Ordinary’.</li>
            <li>The prescribed particulars for all shares are: Ordinary shares have full rights in the company with respect to voting, dividends and distributions.</li>
            <li>All shares are Fully Paid.</li>
        </ul>
        </p>
        <strong>Shareholder address</strong>
        <p>The shareholder address will appear on the public record. This does not need to be the subscriber's usual residential address.</p>
    </div>
</div>