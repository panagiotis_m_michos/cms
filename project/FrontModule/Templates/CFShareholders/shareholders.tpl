{include file="@header.tpl"}
<div class="formprocesstitle">
    {if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
</div>

{* <div class="livesuprt"><script language="javascript" src="https://support.madesimplegroup.com/visitor/index.php?_m=livesupport&_a=htmlcode&departmentid=2"></script></div> *}


{* TABS *}
{include file="@blocks/navlist.tpl" currentTab=$this->action}

<div>
    <div style="border:0px solid blue;float: left">
        {* NOMINEE SHAREHOLDER *}
        {if isset($form)}
            <div style="width:69%">{$form nofilter}</div>
        {/if}


        <div class="box-blue">
            <div class="box-blue-bottom">

                {if $companyType != 'BYGUAR'}
                    <p class="hugefont">Shareholders Summary</p>
                    <p>Below is a list of the shareholders you have added so far:</p>
                {else}
                    <p class="hugefont">Members Summary</p>
                    <p>Below is a list of the members you have added so far:</p>
                {/if}

                <br />

                {* PERSONS *}
                <p class="hugefont">Persons</p>

                {if !empty($persons)}
                    <table class="formtable mnone" width="550">
                        <col width="700">
                        {if $companyType != 'BYGUAR'} 
                            <col width="150">
                        {/if}
                        <col width="50">
                        <tr>
                            <th>Full name</th>
                            {if $companyType != 'BYGUAR'} 
                                <th class="center" style="text-align: center">Shares</th>
                            {/if}
                            <th class="center" colspan="2">Action</th>
                        </tr>
                        {foreach from=$persons key="key" item="person"}
                            <tr>
                                <td>{$key+1}. {$person.fullName}</td>
                                {if $companyType != 'BYGUAR'} 
                                    <td class="center" style="text-align: center"> {$person.num_shares}&nbsp;@&nbsp;{$person.share_value}&nbsp;{$person.currency}</td>
                                {/if}

                                <td class="center"><a href="{$person.editLink}">Edit</a></td>
                                <td class="center"><a href="{$person.deleteLink}" onclick="return confirm('Are you sure?');">Delete</a></td>
                            </tr>
                        {/foreach}
                    </table>
                {else}
                    <p>No persons yet.</p>
                {/if}

                <br />

                {* CORPORATES *}
                <p class="hugefont">Corporates</p>

                {if !empty($corporates)}
                    <table class="formtable mnone" width="550">
                        <col width="700">
                        {if $companyType != 'BYGUAR'}
                            <col width="150">
                        {/if}
                        <col width="50">
                        <tr>
                            <th>Full name</th>
                            {if $companyType != 'BYGUAR'}
                                <th class="center" style="text-align: center">Shares</th>
                            {/if}
                            <th class="center" colspan="2">Action</th>
                        </tr>
                        {foreach from=$corporates key="key" item="corporate"}
                            <tr>
                                <td>{$key+1}. {$corporate.fullName}</td>
                                {if $companyType != 'BYGUAR'}
                                    <td class="center" style="text-align: center">{$corporate.num_shares}&nbsp;@&nbsp;{$corporate.share_value}&nbsp;{$corporate.currency}</td>
                                {/if}
                                <td class="center"><a href="{$corporate.editLink}">Edit</a></td>
                                <td class="center"><a href="{$corporate.deleteLink}" onclick="return confirm('Are you sure?');">Delete</a></td>
                            </tr>
                        {/foreach}
                    </table>
                {else}
                    <p>No corporates yet.</p>
                {/if}

                <br />

                {* NOMINEES *}
                {if !empty($nominees)}
                    <p class="hugefont">Nominees</p>
                    <table class="formtable mnone" width="550">
                        <col>
                        <tr>
                            <th>Full name</th>
                        </tr>
                        {foreach from=$nominees key="key" item="nominee"}
                            <tr>
                                <td>{$key+1}. {$nominee.fullName}</td>
                            </tr>
                        {/foreach}
                    </table>
                {/if}



            </div>
        </div>

        <div class="box-pink">
            <div class="box-pink-bottom">
                <p class="hugefont noptop">What do you want to do next?</p>

                <table width="100%">
                    <col width="33%">
                    <col width="34%">
                    <col width="33%">
                    <tr>
                        <td><a class="btn_back2 mtop fleft clear" href="{$this->router->link("CFDirectorsControler::DIRECTORS_PAGE", "company_id=$companyId")}">Back</a></td>
                        {if $companyType != 'BYGUAR'}
                            <td><a href="{$this->router->link("CFShareholdersControler::ADD_SHAREHOLDER_PERSON_PAGE", "company_id=$companyId")}">Add Another Shareholder</a></td>
                        {else}
                            <td><a href="{$this->router->link("CFShareholdersControler::ADD_SHAREHOLDER_PERSON_PAGE", "company_id=$companyId")}">Add Another Member</a></td>
                        {/if}
                        <td align="right">{$continue nofilter}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>    
    <div style="float: right; width: 285px;">

        {* GUIDE *}
        <div class="box1 fright" style="margin-bottom: 10px;">	
            <h3>Guide:</h3>
            <div class="box1-foot pbottom15">
                <p>A subscriber (shareholder) or member can be a company director or secretary or a new appointment.</p>
                <p>Shareholders and shareholding determine the ownership of a company.</p>		
                <p>For companies limited by guarantee the owners of the company are called Members or Guarantors.</p>
            </div>
        </div>

        <div class="clear"></div>
        
        {* HTML INFO BOX *}
        {include file="@blocks/CFHtmlBox.tpl"}
    </div>
    <div class="clear"></div>
</div>        



{include file="@footer.tpl"}
