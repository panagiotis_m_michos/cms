{include file="@header.tpl"}
<div class="formprocesstitle">
    {if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
    
{*ERRORS*}
{$form->getBegin() nofilter}
{if $form->getErrors()|@count gt 0}
    <p class="ff_err_notice ff_red_err" style="width: 940px">Form has <b> {$form->getErrors()|@count}</b> error(s). See below for more details:</p>
{/if} 

</div>

{* <div class="livesuprt"><script language="javascript" src="https://support.madesimplegroup.com/visitor/index.php?_m=livesupport&_a=htmlcode&departmentid=2"></script></div> *}


{* TABS *}
{include file="@blocks/navlist.tpl" currentTab="shareholders"}

<div>
    <p style="float: right; text-align: right;">
        To make a human appointment
        <a href="{$this->router->link("CFShareholdersControler::ADD_SHAREHOLDER_PERSON_PAGE", "company_id=$companyId")}">click here</a>
    </p>
    <div class="help-button" style="float: right; margin: 0 15px 0 0;">
        <a href="#" class="help-icon">help</a>
        <em>Click on this option should you wish to nominate a person in the position of shareholder/member rather than a company.</em>
    </div>
</div>

<div style="clear: both;">&nbsp;</div>

{* JS FOR PREFILL *}
<script type="text/javascript">
/* <![CDATA[ */
var addresses = {$jsPrefillAdresses nofilter};
var officers = {$jsPrefillOfficers nofilter};
/* ]]> */
</script>

{literal}
    <script>
    $(document).ready(function () {

        // prefill officers	
        $("#prefillOfficers").change(function () {
            var value = $(this).val();
            address = officers[value];
            for (var name in address) {
                $('#'+name).val(address[name]);
            }
        });

        // prefill address	
        $("#prefillAddress").change(function () {
            var value = $(this).val();
            address = addresses[value];
            for (var name in address) {
                $('#'+name).val(address[name]);
            }
        });
	
	
    });
    </script>
{/literal}

<div>
    <div style="border:0px solid blue;float: left">
        <div style="width: 550px;">

            {$form->getBegin() nofilter}

            {include '../@blocks/officersPrefill.tpl'}

            <fieldset>
                <legend>Corporate</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <th>{$form->getLabel('corporate_name') nofilter}</th>
                            <td>{$form->getControl('corporate_name') nofilter}</td>
                            <td>
                                <div class="help-button">
                                    <a href="#" class="help-icon">help</a>
                                    <em>This is the company name of the company you want to stand in the position of shareholder/member. Please note that you cannot nominate the company you are currently forming to be a shareholder/member of itself.</em>
                                </div>
                                <span class="redmsg">{$form->getError('corporate_name')}</span>
                            </td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('forename') nofilter}</th>
                            <td>{$form->getControl('forename') nofilter}</td>
                            <td>
                                <div class="help-button">
                                    <a href="#" class="help-icon">help</a>
                                    <em>The authorising person is the director of the company standing in the position of shareholder/member who has given permission for it to be appointed.</em>
                                </div>
                                <span class="redmsg">{$form->getError('forename')}</span>
                            </td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('surname') nofilter}</th>
                            <td>{$form->getControl('surname') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('surname')}</span></td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>

            <fieldset>
                <legend>Prefill</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <th>{$form->getLabel('prefillAddress') nofilter}</th>
                            <td>{$form->getControl('prefillAddress') nofilter}</td>
                            <td>
                                <div class="help-button">
                                    <a href="#" class="help-icon">help</a>
                                    <em>You can use this prefill option to select an address you&apos;ve entered previously.</em>
                                </div>
                                <span class="redmsg">{$form->getError('prefillAddress')}</span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>

            <fieldset>
                <legend>Address</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <th>{$form->getLabel('premise') nofilter}</th>
                            <td>{$form->getControl('premise') nofilter}</td>
                            <td>
                                <div class="help-button">
                                    <a href="#" class="help-icon">help</a>
                                    <em>This is the registered office of the company you are appointing as shareholder/member.</em>
                                </div>
                                <span class="redmsg">{$form->getError('premise')}</span>
                            </td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('street') nofilter}</th>
                            <td>{$form->getControl('street') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('street')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('thoroughfare') nofilter}</th>
                            <td>{$form->getControl('thoroughfare') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('thoroughfare')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('post_town') nofilter}</th>
                            <td>{$form->getControl('post_town') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('post_town')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('county') nofilter}</th>
                            <td>{$form->getControl('county') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('county')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('postcode') nofilter}</th>
                            <td>{$form->getControl('postcode') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('postcode') nofilter}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('country') nofilter}</th>
                            <td>{$form->getControl('country') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('country')}</span></td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>

            {if $companyType != 'BYGUAR'}
                <fieldset>
                    <legend>Allotment of Shares</legend>
                    <table class="ff_table">
                        <tbody>
                            <tr>
                                <th>{$form->getLabel('currency') nofilter}</th>
                                <td>{$form->getControl('currency') nofilter}</td>
                                <td>
                                    <div class="help-button">
                                        <a href="#" class="help-icon">help</a>
                                        <em>Only 3 currencies can be used for online incorporations. GBP (£), USD ($) and EUR (€).</em>
                                    </div>
                                    <span class="redmsg">{$form->getError('currency')}</span>
                                </td>
                            </tr>
                            <tr>
                                <th>{$form->getLabel('share_class') nofilter}</th>
                                <td>{$form->getControl('share_class') nofilter}</td>
                                <td>
                                    <div class="help-button">
                                        <a href="#" class="help-icon">help</a>
                                        <em>Ordinary shares are the default for online incorporations. This cannot be changed.</em>
                                    </div>
                                    <span class="redmsg">{$form->getError('share_class')}</span>
                                </td>
                            </tr>
                            <tr>
                                <th>{$form->getLabel('num_shares') nofilter}</th>
                                <td>{$form->getControl('num_shares') nofilter}<br /><i>By default we recommend 1 share per shareholder. You can easily allocate extra shares after incorporation.</i></td>
                                <td>
                                    <div class="help-button">
                                        <a href="#" class="help-icon">help</a>
                                        <em>This is the number of shares you would like to issue to this shareholder.</em>
                                    </div>
                                    <span class="redmsg">{$form->getError('num_shares')}</span>
                                </td>
                            </tr>
                            <tr>
                                <th>{$form->getLabel('share_value') nofilter}</th>
                                <td>{$form->getControl('share_value') nofilter}<br /><i>By default we recommend a share value of 1. The number of shares and value per share limits your company's liability.</i></td>
                                <td>
                                    <div class="help-button">
                                        <a href="#" class="help-icon">help</a>
                                        <em>This is the value of each share for the nominated currency.</em>
                                    </div>
                                    <span class="redmsg">{$form->getError('share_value')}</span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </fieldset>
            {/if}

            <fieldset>
                <legend>Security</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <th>{$form->getLabel('BIRTOWN') nofilter}</th>
                            <td>{$form->getControl('BIRTOWN') nofilter}</td>
                            <td>
                                <div class="help-button">
                                    <a href="#" class="help-icon">help</a>
                                    <em>Security questions are used as online signatures by Companies House. These questions relate to the Authorising person.</em>
                                </div>
                                <span class="redmsg">{$form->getError('BIRTOWN')}</span>
                            </td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('TEL') nofilter}</th>
                            <td>{$form->getControl('TEL') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('TEL')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('EYE') nofilter}</th>
                            <td>{$form->getControl('EYE') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('EYE')}</span></td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>

            <fieldset id="fieldset_2">
                <legend>Action</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <td><a href="{$this->router->link("CFShareholdersControler::SHAREHOLDERS_PAGE", "company_id=$companyId")}" class="btn_back2 fleft clear">Back</a></td>
                            <td width="33%">&nbsp;</td>
                            <td>{$form->getControl('continue') nofilter}</td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>

            {$form->getEnd() nofilter}

        </div>
    </div>    
    <div style="float: right; width: 285px;">

        {* GUIDE *}
        {include file="CFShareholders/@guide.tpl"}

        <div class="clear"></div>
        
        {* HTML INFO BOX *}
        {include file="@blocks/CFHtmlBox.tpl"}
    </div>
    <div class="clear"></div>
</div>

{include file="@footer.tpl"}
