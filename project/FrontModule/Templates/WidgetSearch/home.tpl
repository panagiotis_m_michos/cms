<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"> 
<head> 
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
	<meta http-equiv="content-language"  content="en" /> 
	<title>{$title}</title>
	
	<style type="text/css">
	<!--
	body {ldelim}
		font-family: Arial, Verdana, sans-serif;
		font-size: 12px;	
	{rdelim}
	
	#envelope {ldelim}
		width: 350px;
	{rdelim}
	
		#envelope #search fieldset {ldelim}
			background: #{$companyColour};
			border: 1px solid #{$companyColour};
			margin: 0;
			padding: 3px 0 3px 3px;
		{rdelim}
		
			#envelope #search fieldset label {ldelim}
				color: #{$textColour};
			{rdelim}
	
				#envelope #search fieldset table {ldelim}
					border-collapse:collapse;	
				{rdelim}
				
					#envelope #search fieldset table input#submit {ldelim}
						height: 30px;
						width: 70px;
					{rdelim}
					
		#envelope #respond {ldelim}
			padding: 3px 0 0 113px;
			font-size: 11px;
			font-style: italic;
		{rdelim}
		
			#envelope #respond span.available {ldelim}
				color: green;
			{rdelim}
			
			#envelope #respond span.not-available {ldelim}
				color: red;
			{rdelim}
			
		#envelope #powered {ldelim}
			font-size: 11px;
			text-align: right;
			color: silver;
			padding: 10px 0 0 0;
		{rdelim}
		
			#envelope #powered a {ldelim}
				color: silver;
			{rdelim}
	-->
	</style>
	
</head>
<body> 
<div id="envelope">
	
	{* SEARCH FORM *}
	<div id="search">
		{$form->getBegin() nofilter}
		<fieldset>
			<table border="0">
			<col width="105">
			<tr>
				<td>{$form->getLabel('companyName') nofilter}</td>
				<td>{$form->getControl('companyName') nofilter}</td>
				<td>{$form->getControl('submit') nofilter}</td>
			</tr>
			</table>
		</fieldset>
		{$form->getEnd() nofilter}
	</div> <!-- #search -->
	
	{* RESPOND *}
	{if isset($available)}
		<div id="respond">
			{if $available}
				<span class="available">Company name is available</span>
			{else}
				<span class="not-available">Company name is not available</span>
			{/if}
		</div> <!-- #respond -->
	{/if}
	
	{* POWERED *}
	<div id="powered">
		powered by <a href="http://madesimplegroup.com" target="_blank">The Made Simple Group</a>
	</div> <!-- #powered -->

</div> <!-- #envelope -->
</body>
</html>