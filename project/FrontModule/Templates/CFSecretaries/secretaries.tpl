{include file="@header.tpl"}
<div class="formprocesstitle">
    {if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
</div>

{* <div class="livesuprt"><script language="javascript" src="https://support.madesimplegroup.com/visitor/index.php?_m=livesupport&_a=htmlcode&departmentid=2"></script></div> *}

{* TABS *}
{include file="@blocks/navlist.tpl" currentTab=$this->action}

{* GUIDE *}

<div>
    <div style="border:0px solid blue;float: left">
        {* NOMINEE DIRECTOR *}
        {if isset($form)}
        <div style="width:69%">{$form nofilter}</div>
        {/if}

        <div class="box-blue"  style="border: 0px solid blue;">
            <div class="box-blue-bottom">
                <div style="position: relative;">
                    <div class="hugefont" style="float: left;">Secretaries Summary</div>
                    <div class="help-button">
                        <a href="#" class="help-icon">help</a>
                        <em>Secretaries are not compulsory appointments and you can skip this step if you like. </em>
                    </div>
                </div>

                <div style="clear: both;">&nbsp;</div>
                <p>Below is a list of the secretaries you have added so far:</p>

                <br />

                {* PERSONS *}
                <p class="hugefont">Persons</p>

                {if !empty($persons)}
                    <table class="formtable mnone" width="550">
                        <col>
                        <col width="60">
                        <col width="70">
                        <tr>
                            <th>Full name</th>
                            <th class="center" colspan="2">Action</th>
                        </tr>
                        {foreach from=$persons key="key" item="person"}
                            <tr>
                                <td>{$key+1}. {$person.fullName}</td>
                                <td class="center"><a href="{$person.editLink}">Edit</a></td>
                                <td class="center"><a href="{$person.deleteLink}" onclick="return confirm('Are you sure?');">Delete</a></td>
                            </tr>
                        {/foreach}
                    </table>
                {else}
                    <p>No persons yet.</p>
                {/if}

                <br />

                {* CORPORATES *}
                <p class="hugefont">Corporates</p>

                {if !empty($corporates)}
                    <table class="formtable mnone" width="550">
                        <col>
                        <col width="60">
                        <col width="70">
                        <tr>
                            <th>Full name</th>
                            <th class="center" colspan="2">Action</th>
                        </tr>
                        {foreach from=$corporates key="key" item="corporate"}
                            <tr>
                                <td>{$key+1}. {$corporate.fullName}</td>
                                <td class="center"><a href="{$corporate.editLink}">Edit</a></td>
                                <td class="center"><a href="{$corporate.deleteLink}" onclick="return confirm('Are you sure?');">Delete</a></td>
                            </tr>
                        {/foreach}
                    </table>
                {else}
                    <p>No corporates yet.</p>
                {/if}

                <br />

                {* NOMINEES *}
                {if !empty($nominees)}
                    <p class="hugefont">Nominees</p>
                    <table class="formtable mnone" width="550">
                        <col>
                        <tr>
                            <th>Full name</th>
                        </tr>
                        {foreach from=$nominees key="key" item="nominee"}
                            <tr>
                                <td>{$key+1}. {$nominee.fullName}</td>
                            </tr>
                        {/foreach}
                    </table>
                {/if}
            </div>

        </div>

        <div class="box-pink" style="border: 0px solid blue;">
            <div class="box-pink-bottom">
                <p class="hugefont noptop">What do you want to do next?</p>

                <table width="100%">
                    <col width="33%">
                    <col width="34%">
                    <col width="33%">
                    <tr>
                        <td><a class="btn_back2 mtop fleft clear" href="{$this->router->link("CFShareholdersControler::SHAREHOLDERS_PAGE", "company_id=$companyId")}">Back</a></td>
                        <td><a href="{$this->router->link("CFSecretariesControler::ADD_SECRETARY_PERSON_PAGE", "company_id=$companyId")}">Add Secretary</a></td>
                        <td align="right">{$continue nofilter}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>            
    <div style="float: right; width: 285px;">

        {* GUIDE *}
        {include file="CFSecretaries/@guide.tpl"}
        <div class="clear"></div>
        
        {* HTML INFO BOX *}
        {include file="@blocks/CFHtmlBox.tpl"}
    </div>
    <div class="clear"></div>
</div>


{include file="@footer.tpl"}