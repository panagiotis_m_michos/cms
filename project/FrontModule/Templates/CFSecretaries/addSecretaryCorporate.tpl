{include file="@header.tpl"}
<div class="formprocesstitle">
    {if $visibleTitle}
        <h1>{$title}</h1>
    {/if}

    {*ERRORS*}
    {$form->getBegin() nofilter}
    {if $form->getErrors()|@count gt 0}
        <p class="ff_err_notice ff_red_err" style="width: 940px">Form has <b> {$form->getErrors()|@count}</b> error(s). See below for more details:</p>
    {/if} 

</div>

{* <div class="livesuprt"><script language="javascript" src="https://support.madesimplegroup.com/visitor/index.php?_m=livesupport&_a=htmlcode&departmentid=2"></script></div> *}

{* TABS *}
{include file="@blocks/navlist.tpl" currentTab="secretaries"}

<div>
    <p style="float: right; text-align: right;">
        To make a human appointment
        <a href="{$this->router->link("CFSecretariesControler::ADD_SECRETARY_PERSON_PAGE", "company_id=$companyId")}">click here</a>
    </p>
    <div class="help-button" style="float: right; margin: 0 15px 0 0;">
        <a href="#" class="help-icon">help</a>
        <em>Click on this option should you wish to nominate a person in the position of secretary rather than a company.</em>
    </div>
</div>

<div style="clear: both;">&nbsp;</div>

{* JS FOR PREFILL *}
<script type="text/javascript">
    /* <![CDATA[ */
    var addresses = {$jsPrefillAdresses nofilter};
    var officers = {$jsPrefillOfficers nofilter};
    /* ]]> */
</script>

{literal}
    <script>
        $(document).ready(function() {

            // prefill officers	
            $("#prefillOfficers").change(function() {
                var value = $(this).val();
                officer = officers[value];
                for (var name in officer) {
                    $('#' + name).val(officer[name]);
                    if ($('#' + name).is(':radio')) {
                        $("#place_registered, #registration_number, #law_governed, #legal_form ").attr("disabled", false);
                        if (name == "eeaType1") {
                            $("#law_governed, #legal_form").val('');
                            $("#law_governed, #legal_form").attr("disabled", true);
                        }
                        $('#' + name).attr('checked', officer[name]);
                    }
                }
            });

            // prefill address	
            $("#prefillAddress").change(function() {
                var value = $(this).val();
                address = addresses[value];
                for (var name in address) {
                    $('#' + name).val(address[name]);
                }
            });

            // EEA
            toogleEEA();

            $("input[name='eeaType']").click(function() {
                toogleEEA();
            });

            function toogleEEA() {
                // EEA
                if ($("#eeaType1").is(":checked")) {
                    $("#place_registered, #registration_number").attr("disabled", false);
                    $("#law_governed, #legal_form").val('');
                    $("#law_governed, #legal_form").attr("disabled", true);
                    // Non EEA
                } else if ($("#eeaType2").is(":checked")) {
                    $("#place_registered, #registration_number").attr("disabled", false);
                    $("#law_governed, #legal_form").attr("disabled", false);
                } else {
                    $("#place_registered, #registration_number, #law_governed, #legal_form ").attr("disabled", true);
                }

            }




        });
    </script>
{/literal}

<div>
    <div style="border:0px solid blue;float: left">
        <div style="width: 550px;">

            {$form->getBegin() nofilter}

            {include '../@blocks/officersPrefill.tpl'}

            <fieldset>
                <legend>Corporate</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <th>{$form->getLabel('corporate_name') nofilter}</th>
                            <td>{$form->getControl('corporate_name') nofilter}</td>
                            <td>
                                <div class="help-button">
                                    <a href="#" class="help-icon">help</a>
                                    <em>This is the company name of the company you want to stand in the position of secretary. Please note that you cannot nominate the company you are currently forming to be a secretary of itself.</em>
                                </div>
                                <span class="redmsg">{$form->getError('corporate_name')}</span>
                            </td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('forename') nofilter}</th>
                            <td>{$form->getControl('forename') nofilter}</td>
                            <td>
                                <div class="help-button">
                                    <a href="#" class="help-icon">help</a>
                                    <em>The authorising person is the director of the company standing in the position of secretary who has given permission for it to be appointed.</em>
                                </div>
                                <span class="redmsg">{$form->getError('forename')}</span>
                            </td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('surname') nofilter}</th>
                            <td>{$form->getControl('surname') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('surname')}</span></td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>

            <fieldset>
                <legend>Prefill</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <th>{$form->getLabel('prefillAddress') nofilter}</th>
                            <td>{$form->getControl('prefillAddress') nofilter}</td>
                            <td>
                                <div class="help-button">
                                    <a href="#" class="help-icon">help</a>
                                    <em>You can use this prefill option to select an address you&apos;ve entered previously.</em>
                                </div>
                                <span class="redmsg">{$form->getError('prefillAddress')}</span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>

            <fieldset>
                <legend>Address</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <th>{$form->getLabel('premise') nofilter}</th>
                            <td>{$form->getControl('premise') nofilter}</td>
                            <td>
                                <div class="help-button">
                                    <a href="#" class="help-icon">help</a>
                                    <em>This is the registered office of the company you are appointing as secretary.</em>
                                </div>
                                <span class="redmsg">{$form->getError('premise')}</span>
                            </td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('street') nofilter}</th>
                            <td>{$form->getControl('street') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('street')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('thoroughfare') nofilter}</th>
                            <td>{$form->getControl('thoroughfare') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('thoroughfare')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('post_town') nofilter}</th>
                            <td>{$form->getControl('post_town') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('post_town')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('county') nofilter}</th>
                            <td>{$form->getControl('county') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('county')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('postcode') nofilter}</th>
                            <td>{$form->getControl('postcode') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('postcode') nofilter}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('country') nofilter}</th>
                            <td>{$form->getControl('country') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('country')}</span></td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>

            <fieldset>
                <legend>EEA/Non EEA</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <th>{$form->getLabel('eeaType') nofilter}</th>
                            <td>{$form->getControl('eeaType') nofilter}</td>
                            <td>
                                <div class="help-button">
                                    <a href="#" class="help-icon">help</a>
                                    <em>Please state the whether this company is registered in either the EEA (European Union) or not.</em>
                                </div>
                                <span class="redmsg">{$form->getError('eeaType')}</span>
                            </td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('place_registered') nofilter}</th>
                            <td>{$form->getControl('place_registered') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('place_registered')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('registration_number') nofilter}</th>
                            <td>{$form->getControl('registration_number') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('registration_number')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('law_governed') nofilter}</th>
                            <td>{$form->getControl('law_governed') nofilter}</td>
                            <td>
                                <div class="help-button">
                                    <a href="#" class="help-icon">help</a>
                                    <em>Enter which law governs the company you are appointing. Eg: Common, Civil etc.</em>
                                </div>
                                <span class="redmsg">{$form->getError('law_governed')}</span>
                            </td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('legal_form') nofilter}</th>
                            <td>{$form->getControl('legal_form') nofilter}</td>
                            <td>
                                <div class="help-button">
                                    <a href="#" class="help-icon">help</a>
                                    <em>The legal form of the company as defined in its country of registration.</em>
                                </div>
                                <span class="redmsg">{$form->getError('legal_form')}</span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>

            <fieldset>
                <legend>Consent to act</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <td colspan="2">
                                <span class="redmsg">{$form->getError('consentToAct')}</span>
                                {$form->getControl('consentToAct') nofilter}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>

            <fieldset id="fieldset_2">
                <legend>Action</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <td><a href="{$this->router->link("CFSecretariesControler::SECRETARIES_PAGE", "company_id=$companyId")}" class="btn_back2 fleft clear">Back</a></td>
                            <td width="33%">&nbsp;</td>
                            <td>{$form->getControl('continue') nofilter}</td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>

            {$form->getEnd() nofilter}

        </div>
    </div>            
    <div style="float: right; width: 285px;">

        {* GUIDE *}
        {include file="CFSecretaries/@guide.tpl"}

        <div class="clear"></div>

        {* HTML INFO BOX *}
        {include file="@blocks/CFHtmlBox.tpl"}
    </div>
    <div class="clear"></div>
</div>

{include file="@footer.tpl"}
