{include file="@header.tpl"}
<div class="formprocesstitle">
    {if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
    
{*ERRORS*}
{$form->getBegin() nofilter}
{if $form->getErrors()|@count gt 0}
    <p class="ff_err_notice ff_red_err" style="width: 940px">Form has <b> {$form->getErrors()|@count}</b> error(s). See below for more details:</p>
{/if} 

</div>

{* <div class="livesuprt"><script language="javascript" src="https://support.madesimplegroup.com/visitor/index.php?_m=livesupport&_a=htmlcode&departmentid=2"></script></div> *}

{* TABS *}
{include file="@blocks/navlist.tpl" currentTab="secretaries"}


<div>
    <p style="float: right; text-align: right;">
        To make a corporate appointment
        <a href="{$this->router->link("CFSecretariesControler::ADD_SECRETARY_CORPORATE_PAGE", "company_id=$companyId")}">click here</a>
    </p>
    <div class="help-button" style="float: right; margin: 0 15px 0 0;">
        <a href="#" class="help-icon">help</a>
        <em>This option is available to you should you wish to nominate a company in the position of secretary. Please note that you cannot nominate the company you are forming to be a secretary of itself.</em>
    </div>
</div>

<div style="clear: both;">&nbsp;</div>


{* JS FOR PREFILL *}
<script type="text/javascript">
/* <![CDATA[ */
var addresses = {$jsPrefillAdresses nofilter};
var officers = {$jsPrefillOfficers nofilter};
/* ]]> */
</script>

{literal}
    <script>
    $(document).ready(function () {

        // start on beginning
        ourRegisterOfficeHandler();
	
        // called on type click
        $("input[name='ourServiceAddress']").click(function () {		
            ourRegisterOfficeHandler(); 
        });
	
	
        /**
         * Provides disable and enable dropdown for our offices
         * @return void
         */
        function ourRegisterOfficeHandler()
        {
            disabled = $("#ourServiceAddress").is(":checked");
            
            $('#prefillAddress').attr("disabled", disabled);
            $('#premise').attr("disabled", disabled);
            $('#street').attr("disabled", disabled);
            $('#thoroughfare').attr("disabled", disabled);
            $('#post_town').attr("disabled", disabled);
            $('#postcode').attr("disabled", disabled);
            $('#county').attr("disabled", disabled);
            $('#country').attr("disabled", disabled);
            if(disabled == true){
                $("#service_address, #service_address_prefill").hide();
            }else{
                $("#service_address, #service_address_prefill").show();
            }
        }
        // prefill address	
        $("#prefillAddress").change(function () {
            var value = $(this).val();
            address = addresses[value];
            for (var name in address) {
                $('#'+name).val(address[name]);
            }
        });
	
        // prefill officers	
        $("#prefillOfficers").change(function () {
            var value = $(this).val();
            address = officers[value];
            for (var name in address) {
                $('#'+name).val(address[name]);
            }
        });
	
    });
    </script>
{/literal}

<div>
    <div style="border:0px solid blue;float: left">
        <div style="width: 550px;">

            {$form->getBegin() nofilter}

            {include '../@blocks/officersPrefill.tpl'}

            <fieldset>
                <legend>Person</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <th>{$form->getLabel('title') nofilter}</th>
                            <td>{$form->getControl('title') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('title')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('forename') nofilter}</th>
                            <td>{$form->getControl('forename') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('forename')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('middle_name') nofilter}</th>
                            <td>{$form->getControl('middle_name') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('middle_name')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('surname') nofilter}</th>
                            <td>{$form->getControl('surname') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('surname')}</span></td>
                        </tr>

                    </tbody>
                </table>
            </fieldset>

            {if isset($form.ourServiceAddress)}
            <fieldset id="fieldset_0">
                <legend>Use Our Service Address</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <th>{$form->getLabel('ourServiceAddress') nofilter}</th>
                            <td>{$form->getControl('ourServiceAddress') nofilter}</td>
                            <td>{$msgServiceAdress->premise} 
                                {$msgServiceAdress->street}, 
                                {$msgServiceAdress->post_town}, 
                                {$msgServiceAdress->postcode}</td>
                        </tr>
                        <tr>
                            <td colspan="3"> 
                                <ul style="margin: 0px; color: #999999;">
                                    <li><i>Your officer&apos;s address will remain private; ours will show on the public register.</i></li>
                                    <li><i>Prevent junk mail; only receive your company's statutory mail.</i></li>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>
            {/if}
            
            <fieldset id="service_address_prefill">
                <legend>Prefill</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <th>{$form->getLabel('prefillAddress') nofilter}</th>
                            <td>{$form->getControl('prefillAddress') nofilter}</td>
                            <td>
                                <div class="help-button">
                                    <a href="#" class="help-icon">help</a>
                                    <em>You can use this prefill option to select an address you&apos;ve entered previously.</em>
                                </div>
                                <span class="redmsg">{$form->getError('prefillAddress')}</span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>

            <fieldset id="service_address">
                <legend>Service Address</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <th>{$form->getLabel('premise') nofilter}</th>
                            <td>{$form->getControl('premise') nofilter}</td>
                            <td><div class="help-button">
                                    <a href="#" class="help-icon">help</a>
                                    <em>Eg. "28A" or "Flat 2, 36" or "Crusader House"</em>
                                </div>
                                <span class="redmsg">{$form->getError('premise')}</span>
                            </td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('street') nofilter}</th>
                            <td>{$form->getControl('street') nofilter}</td>
                            <td>
                                <div class="help-button">
                                    <a href="#" class="help-icon">help</a>
                                    <em>Street refers to the actual street, road, lane etc.</em>
                                </div>
                                <span class="redmsg">{$form->getError('street')}</span>
                            </td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('thoroughfare') nofilter}</th>
                            <td>{$form->getControl('thoroughfare') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('thoroughfare')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('post_town') nofilter}</th>
                            <td>{$form->getControl('post_town') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('post_town')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('county') nofilter}</th>
                            <td>{$form->getControl('county') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('county')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('postcode') nofilter}</th>
                            <td>{$form->getControl('postcode') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('postcode') nofilter}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('country') nofilter}</th>
                            <td>{$form->getControl('country') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('country')}</span></td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>

            <fieldset>
                <legend>Consent to act</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <td colspan="2">
                                <span class="redmsg">{$form->getError('consentToAct')}</span>
                                {$form->getControl('consentToAct') nofilter}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>

            <fieldset id="fieldset_2">
                <legend>Action</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <td><a href="{$this->router->link("CFSecretariesControler::SECRETARIES_PAGE", "company_id=$companyId")}" class="btn_back2 fleft clear">Back</a></td>
                            <td width="33%">&nbsp;</td>
                            <td>{$form->getControl('continue') nofilter}</td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>

            {$form->getEnd() nofilter}
        </div>
    </div>            
    <div style="float: right; width: 285px;">

        {* GUIDE *}
        {include file="CFSecretaries/@guide.tpl"}

        <div class="clear"></div>
        
        {* HTML INFO BOX *}
        {include file="@blocks/CFHtmlBox.tpl"}
    </div>
    <div class="clear"></div>
</div>


{include file="@footer.tpl"}
