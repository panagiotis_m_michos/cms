{include file="@header.tpl"}

{assign var="id" value=$package->id}

{capture name="description"}
    {if $id == 1313} {* basic package *}
        A great choice if you want to reserve a company name for future use or form a dormant company.
    {elseif $id == 1684} {* digital package *}
        Get your business off to a great start with access to useful statutory forms plus tax and financial tips to help you along the way. 
    {elseif $id == 1314} {* printed package *}
        Perfect to get you trading quickly - everything you need to maintain your statutory records by email and in the post.
    {elseif $id == 1315} {* privacy package *}
        When your business needs a prestigious address and you want to protect the privacy of your home address.
    {elseif $id == 1316} {* comprehensive package *}
        Ideal if you not only want to start trading immediately, but also want to be assured that all your statutory requirements are covered.
    {elseif $id == 1317} {* ultimate package *}
        Ideal for those who want to make sure they have everything in place to both enhance their company image and start trading with peace of mind.
    {/if}
{/capture}

{capture name="buy_now_link"}
    {if isset($company)}
        {url route='basket_module_package_basket_add_package' packageId=$id}
    {else}
        {if $noCompany}
            {$this->router->link("SearchControler::COMPANY_NAME", "package_id=$id")}
        {else}
            {$this->router->link("SearchControler::SEARCH_PAGE")}
        {/if}
    {/if}
{/capture}

{capture name="content"}
    <div id="newprod">
        <div class="col100 padding40">
            <div class="col80">
                <h1 class="mbottom20">
                    {$package->page->title}<br>
                    Ltd Company Formation
                </h1>
                <h5>{$smarty.capture.description|trim}</h5>
            </div>
            <div class="col20">
                <a href="http://www.feefo.com/feefo/viewvendor.jsp?logon=www.madesimplegroup.com/companiesmadesimple" onclick="window.open(this.href, 'Feefo', 'width=1000,height=600,scrollbars,resizable');
                        return false;">
                    <img alt="Feefo logo" border="0" src="http://www.feefo.com/feefo/feefologo.jsp?logon=www.madesimplegroup.com/companiesmadesimple&amp;template=service-percentage-white-150x150_en.png" title="See what our customers say about us">
                </a>
            </div>
            <div>
                <div><a class="btn-orange mtop20" href="{$smarty.capture.buy_now_link|trim}">BUY NOW</a></div>
                <div class="mleft20 mtop20"><span class="blueprice">&pound;{$package->price}</span><span> +VAT</span></div>
                <div class="mleft20 mtop20 posrelative" style="top:-5px;"><img src="{$urlImgs}50-cash-back-bubble.png" alt="&pound;50 Cashback"></div>
            </div>
        </div>
        {$text nofilter}
    </div>
{/capture}

{include file='@blocks/defaultTemplateBlock.tpl' content=$smarty.capture.content}
{include file='@blocks/rightColumn.tpl'}
{include file="@footer.tpl"}
