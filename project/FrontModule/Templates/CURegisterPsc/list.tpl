{include file="@header.tpl"}

<div id="maincontent-full" style="position: relative;">

    {include file="@blocks/breadCrumbs.tpl"}
    <h1>
        {$title}
        <span style="font-size: 12px; font-weight: 400;">
            <a href="{url route="CURegisterPscControler::PAGE_ADD_PERSON" company_id=$companyId}">Add Person PSC</a>
            | <a href="{url route="CURegisterPscControler::PAGE_ADD_CORPORATE" company_id=$companyId}">Add Corporate PSC</a>
            | <a href="{url route="CURegisterPscControler::PAGE_ADD_LEGAL_PERSON" company_id=$companyId}">Add Legal Person PSC</a>
            {if $canAddCompanyStatement}
                | <a data-toggle="modal" data-target="#no-psc-modal" href="#">No registrable person notification</a>
            {/if}
        </span>
    </h1>

    <div class="margin-bottom-20">
        <table class="grid" style="width: 100%">
            <tr>
                <th>Date of entry</th>
                <th>Date cessated / withdrawn</th>
                <th>Name</th>
                <th>Nature of Controls</th>
                <th colspan="2">Actions</th>
            </tr>
            {foreach $entries as $entry}
                {assign var='entryId' value=$entry->getId()}
                <tr>
                    <td>{$entry->getDtc()|datetime}</td>
                    <td>
                        {if $entry->isPsc()}
                            {$entry->getDateCessated()|datetime}
                        {elseif $entry->isCompanyStatement()}
                            {$entry->getDateWithdrew()|datetime}
                        {/if}
                    </td>
                    {if $entry->isCompanyStatement()}
                        <td>
                            -
                        </td>
                        <td>
                            <i>{$entry->getCompanyStatementText()}</i>
                        </td>
                    {elseif $entry->isPsc()}
                        <td>
                            {$entry->getFullName()}
                        </td>
                        <td>
                            {foreach $entry->getNatureOfControlsTexts() as $noc}
                                {$noc}<br>
                            {/foreach}
                        </td>
                    {/if}
                    <td >
                        {if $entry->isPerson()}
                            <a href="{url route="CURegisterPscControler::PAGE_EDIT_PERSON" company_id=$companyId pscId=$entryId}">Edit</a>
                        {elseif $entry->isCorporate()}
                            <a href="{url route="CURegisterPscControler::PAGE_EDIT_CORPORATE" company_id=$companyId pscId=$entryId}">Edit</a>
                        {elseif $entry->isLegalPerson()}
                            <a href="{url route="CURegisterPscControler::PAGE_EDIT_LEGAL_PERSON" company_id=$companyId pscId=$entryId}">Edit</a>
                        {/if}
                    </td>
                    <td class="center">
                        {if $entry->isPsc() && !$entry->getDateCessated()}
                            <a class="popupnew" href="{url route="CURegisterPscControler::PAGE_CESSATE_PSC" company_id=$companyId pscId=$entryId}">Cessate</a>
                        {elseif $entry->isCompanyStatement() && !$entry->getDateWithdrew()}
                            <a class="popupnew" href="{url route="CURegisterPscControler::PAGE_WITHDRAW_STATEMENT" company_id=$companyId statementId=$entryId}">Withdraw</a>
                        {/if}
                    </td>
                </tr>
            {/foreach}
        </table>
    </div>

    <div>
        <h4>Notes</h4>
        <ul class="midfont lheight20">
            <li>The PSC information listed here is manually inserted by you.</li>
            <li>
                This page is for record keeping only, it is not linked in any way with Companies House. The only way to notify Companies House of a PSC update is to file a
                <a href="{url route="AnnualReturnControler::COMPANY_DETAILS_PAGE"  company_id=$companyId}">Confirmation Statement</a>.
            </li>
        </ul>
    </div>
</div>

<div class="modal fade" id="no-psc-modal" role="dialog">
    <div class="modal-dialog">
        {$noPscsForm->getBegin() nofilter}
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">No registrable person official wording</h4>
            </div>
            <div class="modal-body">
                <p>
                    The PSC requirements apply whether your company has a PSC or not. If you have taken all reasonable
                    steps and are confident that there are no individuals or legal entities which meet any of the
                    conditions in relation to your company, please select the checkbox below:
                </p>
                {$noPscsForm->getControl('noPscReason') nofilter}
                {$noPscsForm->getLabel('noPscReason') nofilter}
                <p>
                    Check the Guidance for Companies, Societates Europaeae and Limited Liability Partnerships for
                    further information.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {$noPscsForm->getControl('save') nofilter}
            </div>
        </div>
        {$noPscsForm->getEnd() nofilter}
    </div>
</div>

{include file="@footer.tpl"}
