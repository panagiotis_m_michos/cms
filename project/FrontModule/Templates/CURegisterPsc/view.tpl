{include file="@header.tpl"}

<div id="maincontent-full">

    {include file="@blocks/breadCrumbs.tpl"}
    <h1>{$title}</h1>

    <fieldset>
        <legend>Personal Information</legend>
        <table class="grid" style="width: 100%;">
        <col width="120">
        <tr>
            <th>Name</th>
            <td>{$member->name}</td>
        </tr>
        <tr>
            <th>Date of Entry</th>
            <td>{$member->dateEntry|datetime}</td>
        </tr>
        <tr>
            <th>Date ceased</th>
            <td>{$member->dateCeased|datetime}</td>
        </tr>
        <tr>
            <th>Address</th>
            <td>{$member->address|escape:"htmlall"|nl2br nofilter}</td>
        </tr>
        </table>
        <p><a href="{$this->router->link(CURegisterMemberControler::PAGE_EDIT, "company_id={$companyEntity->id}", "registerMemberId={$member->id}")}">Edit personal information</a></p>
    </fieldset>

    <h2 class="margin-bottom-20">
        Share classes
        <span style="font-size: 12px; font-weight: 400;">
            <a href="{$this->router->link("CURegisterShareClassControler::PAGE_ADD", "company_id=$companyId", "registerMemberId={$member->id}")}">Add new Class</a>
        </span>
    </h2>

    {foreach $member->shareClasses as $shareClass}
        <div class="margin-bottom-20">
            <h4>
                {$shareClass->price|currency:2:$shareClass->currencyIso} {$shareClass->classType} shares
                <span style="font-size: 12px; font-weight: 400;">
                    <a href="{$this->router->link("CURegisterShareClassControler::PAGE_EDIT", "company_id=$companyId", "registerShareClassId={$shareClass->id}")}">Edit</a>
                </span>
            </h4>
            {$deleteShareClassForms[$shareClass->id]->begin nofilter}
            <table class="grid" style="width: 100%">
                <tr>
                    <th rowspan="2">Date of entry</th>
                    <th colspan="2">Entry number</th>
                    <th rowspan="2">Certificate number</th>
                    <th colspan="3">Number of shares</th>
                    <th rowspan="2">Price per share</th>
                    <th rowspan="2">Amount to be paid</th>
                    <th rowspan="2">Notes</th>
                    <th rowspan="2">Actions</th>
                </tr>
                <tr>
                    <th>Allotment</th>
                    <th>Transfer</th>
                    <th>Acquired</th>
                    <th>Disposed</th>
                    <th>Balance</th>
                </tr>
                {foreach $shareClass->shareClassEvents as $shareClassEvent}
                    <tr>
                        <td>{$shareClassEvent->dtEntry|datetime}</td>
                        <td>
                            {if $shareClassEvent->isAllotmentType()}
                                {$shareClassEvent->quantity}
                            {/if}
                        </td>
                        <td>
                            {if $shareClassEvent->isTransferType()}
                                {$shareClassEvent->quantity}
                            {/if}
                        </td>
                        <td>{$shareClassEvent->certificateNumber}</td>
                        <td>{$shareClassEvent->acquired}</td>
                        <td>{$shareClassEvent->disposed}</td>
                        <td>{$shareClassEvent->balance}</td>
                        <td>{$shareClassEvent->pricePerShare|currency:2:$shareClass->currencyIso}</td>
                        <td>{$shareClassEvent->totalAmount|currency:2:$shareClass->currencyIso}</td>
                        <td>{$shareClassEvent->notes|escape:"htmlall"|nl2br nofilter}</td>
                        <td>
                            {$deleteShareClassForms[$shareClass->id]->getControl("delete_{$shareClassEvent->id}") nofilter}
                        </td>
                    </tr>
                {/foreach}
                <tr>
                    <td colspan="11">
                        <a href="{$this->router->link(CURegisterShareClassEventControler::PAGE_ADD, "company_id={$companyEntity->id}", "registerShareClassId={$shareClass->id}")}">Add new entry</a>
                    </td>
                </tr>
            </table>
            {$deleteShareClassForms[$shareClass->id]->end nofilter}
        </div>
    {/foreach}
</div>

{include file="@footer.tpl"}
