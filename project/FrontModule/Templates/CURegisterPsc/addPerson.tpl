{include file="@header.tpl"}

<script type="text/javascript">
    var addresses = {$jsPrefillAdresses nofilter};
    var officers = {$jsPrefillOfficers nofilter};
</script>

{literal}
    <script>
        $(function () {
            ourRegisterOfficeHandler();

            $("input[name='ourServiceAddress']").click(function () {
                ourRegisterOfficeHandler();
            });

            /**
             * Provides disable and enable dropdown for our offices
             */
            function ourRegisterOfficeHandler()
            {
                var disabled = $("#ourServiceAddress").is(":checked");

                $('#residentialAddress').attr("checked", disabled);
                $('#prefillAddress').attr("disabled", disabled);
                $('#premise').attr("disabled", disabled);
                $('#street').attr("disabled", disabled);
                $('#thoroughfare').attr("disabled", disabled);
                $('#post_town').attr("disabled", disabled);
                $('#postcode').attr("disabled", disabled);
                $('#county').attr("disabled", disabled);
                $('#country').attr("disabled", disabled);
                if (disabled){
                    toogleServiceAddress();
                    $('#residentialAddress').parent().parent().hide();
                    $("#service_address, #service_address_prefill").hide();
                } else {
                    $("#service_address, #service_address_prefill").show();
                    toogleServiceAddress();
                    $('#residentialAddress').parent().parent().show();
                }
            }

            checkPostcode();
            toogleServiceAddress();

            $("#prefillAddress").change(function () {
                var value = $(this).val();
                var address = addresses[value];
                for (var name in address) {
                    $('#'+name).val(address[name]);
                }
                checkPostcode();
            });

            $("#prefillOfficers").change(function () {
                var value = $(this).val();
                var officer = officers[value];
                for (var name in officer) {
                    $('#'+name).val(officer[name]);
                }
                checkPostcode();
            });

            // service address
            $("#residentialAddress").click(function (){
                toogleServiceAddress();
            });

            function toogleServiceAddress() {
                disabled = !$("#residentialAddress").is(":checked");
                $("input[id^='residential_'], select[id^='residential_']").each(function () {
                    $(this).attr("disabled", disabled);
                });
            }

            function checkPostcode() {
                if ($('#postcode').val() == 'ec1v 4pw' || $('#postcode').val() == 'ec1v4pw'
                        || $("#residentialAddress").is(":checked")
                        || $("#ourServiceAddress").is(":checked")) {
                    $("#residentialAddress").attr('checked', 'checked');
                    toogleServiceAddress();
                } else {
                    $("#residentialAddress").attr('checked', false);
                    toogleServiceAddress();
                }
            }

            function setCheckedState($radios) {
                $.each($radios, function(i, e) {
                    $(e).data('checked', e.checked);
                });
            }

            setCheckedState($(".nature-of-control-container :radio"));

            function toggleChevron(e) {
                $(e.target)
                    .prev()
                    .find("i.nature-of-control-indicator")
                    .toggleClass('fa-chevron-right fa-chevron-down');
            }

            var $natureOfControlContainer = $('.nature-of-control-container');
            $natureOfControlContainer
                .on('hide.bs.collapse', toggleChevron)
                .on('show.bs.collapse', toggleChevron)
                .on('click', ':radio', function() {
                    var newState = !$(this).data('checked');
                    setCheckedState($('.nature-of-control-container :radio[name="' + $(this).attr('name') + '"]'));

                    $(this).prop('checked', newState);
                    $(this).data('checked', newState);
                });

            $natureOfControlContainer.find(':radio:checked').parents('.collapse').collapse('show')
        });
    </script>
{/literal}

<div id="maincontent-full">

    {include file="@blocks/breadCrumbs.tpl"}
    <h1>{$title}</h1>

    {$form->getBegin() nofilter}
    {if $form->getErrors()|@count gt 0}
        <p class="ff_err_notice ff_red_err" style="width: 940px">Form has <b> {$form->getErrors()|@count}</b> error(s).
            See below for more details:</p>
    {/if}

    <fieldset style="clear: both;">
        <legend>Prefill</legend>
        <table class="ff_table">
            <tbody>
                <tr>
                    <th>{$form->getLabel('prefillOfficers') nofilter}</th>
                    <td>{$form->getControl('prefillOfficers') nofilter}</td>
                    <td>
                        <div class="help-button">
                            <a href="#" class="help-icon">help</a>
                            <em>You can use this prefill option to appoint previously nominated appointments.</em>
                        </div>
                        <span class="redmsg">{$form->getError('prefillOfficers')}</span>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>

    <fieldset>
        <legend>Person</legend>
        <table class="ff_table">
            <tbody>
                <tr>
                    <th>{$form->getLabel('title') nofilter}</th>
                    <td>{$form->getControl('title') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('title')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('forename') nofilter}</th>
                    <td>{$form->getControl('forename') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('forename')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('middle_name') nofilter}</th>
                    <td>{$form->getControl('middle_name') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('middle_name')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('surname') nofilter}</th>
                    <td>{$form->getControl('surname') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('surname')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('dob') nofilter}</th>
                    <td>
                        {$form->getControl('dob') nofilter}
                    </td>
                    <td><span class="redmsg">{$form->getError('dob')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('nationality') nofilter}</th>
                    <td>{$form->getControl('nationality') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('nationality')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('country_of_residence') nofilter}</th>
                    <td>{$form->getControl('country_of_residence') nofilter}</td>
                    <td>
                        <div class="help-button">
                            <a href="#" class="help-icon">help</a>
                            <em>This is the country the appointment is either currently living in or where they consider their main country of residence to be.</em>
                        </div>
                        <span class="redmsg">{$form->getError('country_of_residence')}</span>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>

    {if isset($form.ourServiceAddress)}
    <fieldset id="fieldset_0">
        <legend>Use Our Service Address</legend>
        <table class="ff_table">
            <tbody>
                <tr>
                    <th>{$form->getLabel('ourServiceAddress') nofilter}</th>
                    <td>{$form->getControl('ourServiceAddress') nofilter}</td>
                    <td>{$msgServiceAdress->premise}
                        {$msgServiceAdress->street},
                        {$msgServiceAdress->post_town},
                        {$msgServiceAdress->postcode}</td>
                </tr>
                <tr>
                    <td colspan="3">
                        <ul style="margin: 0px; color: #999999;">
                            <li><i>Your officer&apos;s address will remain private; ours will show on the public register.</i></li>
                            <li><i>Prevent junk mail; only receive your company's statutory mail.</i></li>
                        </ul>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    {/if}

    <fieldset id="service_address_prefill">
        <legend>Prefill</legend>
        <table class="ff_table">
            <tbody>
                <tr>
                    <th>{$form->getLabel('prefillAddress') nofilter}</th>
                    <td>{$form->getControl('prefillAddress') nofilter}</td>
                    <td>
                        <div class="help-button">
                            <a href="#" class="help-icon">help</a>
                            <em>You can use this prefill option to select an address you’ve entered previously.</em>
                        </div>
                        <span class="redmsg">{$form->getError('prefillAddress')}</span>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>

    <fieldset id="service_address">
        <legend>Service Address</legend>
        <table class="ff_table">
            <tbody>
                <tr>
                    <th>{$form->getLabel('premise') nofilter}</th>
                    <td>{$form->getControl('premise') nofilter}</td>
                    <td>
                        <div class="help-button">
                            <a href="#" class="help-icon">help</a>
                            <em>A service address is the contact address Companies House will use for their records. It does not have to be your residential address.</em>
                        </div>
                        <span class="redmsg">{$form->getError('premise')}</span>
                    </td>
                </tr>
                <tr>
                    <th>{$form->getLabel('street') nofilter}</th>
                    <td>{$form->getControl('street') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('street')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('thoroughfare') nofilter}</th>
                    <td>{$form->getControl('thoroughfare') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('thoroughfare')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('post_town') nofilter}</th>
                    <td>{$form->getControl('post_town') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('post_town')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('county') nofilter}</th>
                    <td>{$form->getControl('county') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('county')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('postcode') nofilter}</th>
                    <td>{$form->getControl('postcode') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('postcode') nofilter}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('country') nofilter}</th>
                    <td>{$form->getControl('country') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('country')}</span></td>
                </tr>
            </tbody>
        </table>
    </fieldset>

    <fieldset>
        <legend>Residential Address</legend>
        <table class="ff_table">
            <tbody>
                <tr>
                    <th colspan="3" style="padding-bottom: 10px;">The residential address must be the address where you live.</th>
                </tr>
                <tr>
                    <th>{$form->getLabel('residentialAddress') nofilter}</th>
                    <td>{$form->getControl('residentialAddress') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('residentialAddress')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('residential_premise') nofilter}</th>
                    <td>{$form->getControl('residential_premise') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('residential_premise')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('residential_street') nofilter}</th>
                    <td>{$form->getControl('residential_street') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('residential_street')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('residential_thoroughfare') nofilter}</th>
                    <td>{$form->getControl('residential_thoroughfare') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('residential_thoroughfare')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('residential_post_town') nofilter}</th>
                    <td>{$form->getControl('residential_post_town') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('residential_post_town')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('residential_county') nofilter}</th>
                    <td>{$form->getControl('residential_county') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('residential_county')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('residential_postcode') nofilter}</th>
                    <td>{$form->getControl('residential_postcode') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('residential_postcode')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('residential_country') nofilter}</th>
                    <td>{$form->getControl('residential_country') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('residential_country')}</span></td>
                </tr>
            </tbody>
        </table>
    </fieldset>

    <fieldset class="nature-of-control-container">
        <legend>Nature of Control:</legend>
        <p class="nature-of-control-description">
            {$natureOfControlsDescription}
        </p>
        <span class="redmsg">{$form->getError('significant_influence_or_control')}</span>

        {foreach $natureOfControls as $radioName => $natureOfControl}
            <div data-toggle="collapse" data-target="#{$radioName}" aria-expanded="false">
                <i class="nature-of-control-indicator fa fa-chevron-right"></i>
                <span class="nature-of-control-title">{$natureOfControl['title']}</span>
            </div>

            <div id="{$radioName}" class="collapse" style="padding-left: 20px">
                {foreach $natureOfControl['groups'] as $groupIndex => $group}
                    {if isset($group['title'])}
                        <div data-toggle="collapse" data-target="#{$radioName}_{$groupIndex}" aria-expanded="false">
                            <i class="nature-of-control-indicator fa fa-chevron-right"></i>
                            <span class="nature-of-control-group-title">{$group['title']}</span>
                        </div>
                    {/if}

                    <div id="{$radioName}_{$groupIndex}" {if isset($group['title'])}class="collapse" style="padding-left: 20px;"{/if}>
                        {if isset($group['description'])}
                            <p class="nature-of-control-group-description">{$group['description']}</p>
                        {/if}
                        {foreach array_keys($group['options']) as $optionKey}
                            {$form[$radioName]->getControl($optionKey) nofilter}
                        {/foreach}
                    </div>
                {/foreach}
            </div>
        {/foreach}
    </fieldset>

    <fieldset id="fieldset_2">
        <legend>Action</legend>
        {$form->getControl('continue') nofilter}
    </fieldset>

    {$form->getEnd() nofilter}
</div>

{include file="@footer.tpl"}
