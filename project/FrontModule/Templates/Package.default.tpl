{include file="@header.tpl"}

{assign var="id" value=$package->id}

{assign var='newPakages' value=','|explode:"1313,1314,1315,1316,1317"}

<div id="maincontent1">
	<div class="box-white2">
		<div class="box-white2-top">
			<div class="box-white2-bottom">
				<p class="package" style="font-size: 26px;">{$package->getLngTitle() nofilter}</p>
				<p class="package orange">&pound;{$package->price}</p>
				{if isset($company)}
					<a href="{url route='basket_module_package_basket_add_package' packageId=$id}" class="btn_more fright">Buy Now</a>
				{else}
                    {if $noCompany}
                        <a href="{$this->router->link("SearchControler::COMPANY_NAME", "package_id=$id")}" class="btn_more fright">Buy Now</a>
                    {else}
                        <a href="{$this->router->link("SearchControler::SEARCH_PAGE")}" class="btn_more fright">Buy Now</a>
                    {/if}
				{/if}
				<p class="midfont">Package Value &pound;{$package->productValue}</p>
				<div class="clear"></div>
				<div class="description">
					{$text nofilter}
					<br />
					<a href="{$this->router->link("SearchControler::SEARCH_PAGE")}" class="btn_back2 fleft clear">Back</a>					
					{if isset($company)}
						<a href="{url route='basket_module_package_basket_add_package' packageId=$id}" class="btn_more fright">Buy Now</a>
					{else}
                        {if $noCompany}
                            <a href="{$this->router->link("SearchControler::COMPANY_NAME", "package_id=$id")}" class="btn_more fright">Buy Now</a>
                        {else}
                            <a href="{$this->router->link("SearchControler::SEARCH_PAGE")}" class="btn_more fright">Buy Now</a>
                        {/if}
					{/if}
					<div class="clear"></div>
				</div>    
                {if !in_array($this->node->getId(), $newPakages)}
				<h4>View more details of our other packages</h4>
				<a href="{$packages.109.link}" class="pck bronze">{$packages.109.title}</a>
				<a href="{$packages.110.link}" class="pck bronzeplus">{$packages.110.title}</a>
				<a href="{$packages.111.link}" class="pck silver">{$packages.111.title}</a>
				<a href="{$packages.112.link}" class="pck gold">{$packages.112.title}</a>
				<a href="{$packages.113.link}" class="pck platinum">{$packages.113.title}</a>
				<a href="{$packages.279.link}" class="pck diamond">{$packages.279.title}</a>
                {/if}
				<div class="clear"></div>
			</div>
		</div>
	</div>
</div>

{include file='@blocks/rightColumn.tpl'}
{include file="@footer.tpl"}
