{include file="@header.tpl"}

<script>
    $(function() {
        resolveAnnualReturnOfficersVisibility();

        $('body').on('change', "input[name='returnDate']", function () {
            resolveAnnualReturnOfficersVisibility();
        });
    });


    function resolveAnnualReturnOfficersVisibility() {
        var parts = $("input[name='returnDate']").val().split("-");
        var returnDate = new Date(parts[2], parts[1]-1, parts[0]);
        var confirmationStatementRollout = new Date(2016, 5, 30);
        var shouldHide = returnDate >= confirmationStatementRollout;

        $('.confirmation-statement-officers').toggleClass('hidden', shouldHide);
    }
</script>


<div id="maincontent" style="position: relative;">

	<div style="position: absolute; right: 0; top: 0;">
		<a href="{$this->router->link(null, "sync=1")}" onclick="return confirm('Warning if you have already filled in your Confirmation Statement - The update process will refresh your company details and remove any information you have filled in. Are you sure?')">Sync Your Company</a>
	</div>

	<p style="margin: 0 0 15px 0; font-size: 11px;"><a href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}">{$companyName}</a> &gt; {$title}</p>

 	{if $visibleTitle}
    <h1>{$title}
        {if $companyCategory == 'BYSHR'}
            <a style="float: right; font-size: 20px;font-weight: bold;" href="{$this->router->link(1170)}#step6">HELP</a>
        {/if}
    </h1>
    {/if}

    <div style="background-color: #cce5ff; border: 1px solid #cccccc; margin-bottom: 15px; padding: 10px;">
        <b style="color: red; font-size: 14px;">Important Notice:</b> The annual return has been replaced by a Confirmation Statement where you must confirm that your basic company information has been delivered and is up to date. It is NOT used to make changes to the company in ANY regard. This means that you cannot change your directors, share capital or shareholders. Should you want to do this you should follow the correct procedure and submit the relevant forms to Companies House separately.
		<br/>
        Made a mistake? Click 'Sync Your Company' to refresh your company details and start again.
    </div>

	{$form->getBegin() nofilter}

	<fieldset>
		<legend>Update Company Information</legend>
		<table class="ff_table">
		<tr>
            <td colspan="3">
                <span style="font-size: 12px; color: #333333;">
                    <b>If you have not already done so, please <a href="{$this->router->link(null, "sync=1")}" onclick="return confirm('Warning if you have already filled in your Confirmation Statement - The update process will refresh your company details and remove any information you have filled in. Are you sure?')">click here</a> to update your company information.</b>
                </span>
            </td>

		</tr>
        <tr>
            <td colspan="3">
                <span style="font-size: 11px; color: #333333;">
                    This ensures the confirmation statement is completely up to date.
                </span>
            </td>

		</tr>
		</table>
	</fieldset>

    <fieldset>
		<legend>Confirmation Statement Date</legend>
		<table class="ff_table">
		<col width="200">
		<col>
		<col width="300">
		<tr>
			<th>{$form->getLabel('returnDate') nofilter}</th>
			<td>{$form->getControl('returnDate') nofilter}</td>
			<td><span class="ff_control_err">{$form->getError('returnDate')}</span>
                <div style="position: relative; float: left;" class="help-button2">
        			<a class="help-icon" href="#">help</a>
        			<em>
                        Your company's Confirmation Statement date is usually the anniversary of the incorporation or the last Confirmation Statement filed at Companies House. You may choose an earlier Confirmation Statement date but it must not be a future date.
                    </em>
    			</div>
            </td>
		</tr>
		</table>
	</fieldset>
    {if $companyCategory != 'LLP'}
        <fieldset>
            <legend>Traded On Regulated Market?</legend>
            <table class="ff_table">
            <col width="200">
            <col>
            <col>
            <!--col width="200"-->
            <tr>
                <td colspan="3">
                    <span style="font-size: 11px; color: #333333;">
                        Were any of the company's shares admitted to trading on a market at any time during the return period?
                    </span>
                </td>
            </tr>
            <tr>
                <th>
                    {$form->getLabel('trading') nofilter}
                </th>
                <td style="width: 100px;">
                    <div style="position: relative; float: right;" class="help-button2">
                        <a class="help-icon" href="#">help</a>
                        <em>
                           In this context a 'market' is one established under the rules of a UK recognised investment exchange or any other regulated markets in or outside the UK, e.g. London Stock Exchange.
                        </em>
                    </div>
                    {$form->getControl('trading') nofilter}
                </td>
                <td><span class="ff_control_err">{$form->getError('trading')}</span></td>
            </tr>
            </table>
        </fieldset>

        <fieldset>
            <legend>DTR5 Applies</legend>
            <table class="ff_table">
            <col width="200">
            <col>
            <col width="200">
            <tr>
                <td colspan="3">
                    <span style="font-size: 11px; color: #333333;">
                        Throughout the return period, were there shares issued to which DTR5 applies?
                    </span>
                </td>
            </tr>
            <tr>
                <th>
                    {$form->getLabel('dtr5') nofilter}
                </th>
                <td style="width: 130px;">
                    <div style="position: relative; float: right;" class="help-button2">
                        <a class="help-icon" href="#">help</a>
                        <em>
                           The term 'DTR5' refers to Vote Holder and Issuer Notification Rules contained in Chapter 5 of the Disclosure and Transparency Rules source book issued by the Financial Services Authority.
                        </em>
                    </div>
                    {$form->getControl('dtr5') nofilter}
                </td>
                <td><span class="ff_control_err">{$form->getError('dtr5')}</span></td>
            </tr>
            </table>
        </fieldset>


        <fieldset>
            <legend>Classification codes</legend>
            <table class="ff_table">
            <col width="200">
            <col>
            <col width="200">
            <tr>
                <td colspan="3">
                    <span style="font-size: 11px; color: #333333;">Please enter your company's principal business activity using the corresponding
                        <a href="http://www.companieshouse.gov.uk/infoAndGuide/sic/sic2007.shtml" target="_blank">trade classification code.</a>
                    </span>
                </td>
            </tr>
            {section name=foo start=1 loop=5 step=1}
            {assign var="key" value="code_"|cat:$smarty.section.foo.index}
            <tr>
                <th>{$form->getLabel($key) nofilter}</th>
                <td>{$form->getControl($key) nofilter}</td>
                <td><span class="ff_control_err">{$form->getError($key)}</span></td>
            </tr>
            {/section}
            </table>
        </fieldset>
    {/if}
	{if empty($data.premise) || empty($data.street)}
        <fieldset>
            <legend>Registered Office</legend>
            <table class="ff_table">
            <col width="200">
            <col>
            <col width="200">
            <tr>
                <th>{$form->getLabel('premise') nofilter}</th>
                <th>{$form->getControl('premise') nofilter}</th>
                <th><span class="ff_control_err">{$form->getError('premise')}</span></th>
            </tr>
            <tr>
                <th>{$form->getLabel('street') nofilter}</th>
                <th>{$form->getControl('street') nofilter}</th>
                <th><span class="ff_control_err">{$form->getError('street')}</span></th>
            </tr>
            </table>
        </fieldset>
    {/if}
	{* OFFICERS *}
    <div class="confirmation-statement-officers hidden">
	{foreach from=$officers key="officerId" item="officer"}
		<fieldset>
			<legend>{if $officer.type == 'DIR'}Director{elseif $officer.type == 'SEC'}Secretary{else}Member{/if} - {$form->getLabel($officerId) nofilter}</legend>
			<table class="ff_table">

			{if $form->getError($officerId)}
			<tr>
				<td><span class="ff_control_err">{$form->getError($officerId)}</span></td>
			</tr>
			{/if}

			<tr>
				<td>{$form->getControl($officerId) nofilter}</td>
			</tr>
			</table>
		</fieldset>
	{/foreach}
    </div>

	<fieldset>
		<legend>Action</legend>
		<table border="0" width="100%">
		<tr>
			<td>
				{$form->getControl('submit') nofilter}
			</td>
		</tr>
		</table>
	</fieldset>

	{$form->getEnd() nofilter}

</div>

{literal}
<script type="text/javascript">
<!--

$(document).ready(function() {
        /* click trigger on input type */


        function parseDate(str) {
        var date = str.split('-');
        return new Date(date[2], date[1]-1, date[0]);
        }

        function parseDate2(str) {
        var date = str.split('-');
        return new Date( date[0], date[1]-1, date[2] );
        }

        if ($('#trading0').is(':checked'))
            {
                $('input[name^="dtr5"]').parents('fieldset').hide();
            }

        //hide dtr and show trading  for date lover than 01/10/2011 plc
         if( parseDate($(".date").val()) < parseDate('01-10-2011') && $(".CompanyCategory").val() == 'PLC')
            {
                $('input[name^="dtr5"]').parents('fieldset').hide();
                $('input[name^="trading"]').parents('fieldset').show();

            }
         //hide dtr and trading  for date lover than 01/10/2011 ltd, byshares
         else if( parseDate($(".date").val()) < parseDate('01-10-2011') && $(".CompanyCategory").val() != 'PLC')
            {
                $('input[name^="dtr5"]').parents('fieldset').hide();
                $('input[name^="trading"]').parents('fieldset').hide();

            }

          //show trading and dtr5 if  date biger than 01/10/2011 any company type
         else  if( parseDate($(".date").val()) >= parseDate('01-10-2011') && $(".CompanyCategory").val() != 'BYGUAR')
            {
                    $('input[name^="trading"]').parents('fieldset').show();
                    if ($('#trading1').is(':checked'))
                        {
                            $('input[name^="dtr5"]').parents('fieldset').show();
                        }


            }

          else if( parseDate($(".date").val()) >= parseDate('01-10-2011') && $(".CompanyCategory").val() == 'BYGUAR')
            {
                $('input[name^="dtr5"]').parents('fieldset').hide();
                $('input[name^="trading"]').parents('fieldset').hide();
            }

        // incase changing date
        $(".date").change (function() {
             //hide dtr and trading  for date lover than 01/10/2011 ltd, byshares
            if( parseDate($(".date").val()) < parseDate('01-10-2011') && $(".CompanyCategory").val() == 'PLC')
            {
                $('input[name^="dtr5"]').parents('fieldset').hide();
                $('input[name^="trading"]').parents('fieldset').show();

            }

            //hide dtr and trading  for date lover than 01/10/2011 ltd, byshares
            else if(parseDate($(".date").val()) < parseDate('01-10-2011') && $(".CompanyCategory").val() != 'PLC'  )
            {
                $('input[name^="dtr5"]').parents('fieldset').hide();
                $('input[name^="trading"]').parents('fieldset').hide();

            }

            //show dtr5 and trading for date biger than 01/10/2011 and any company
            else  if( parseDate($(".date").val()) >= parseDate('01-10-2011') && $(".CompanyCategory").val() != 'BYGUAR')
            {

                    $('input[name^="trading"]').parents('fieldset').show();
                    if ($('#trading1').is(':checked'))
                        {
                            $('input[name^="dtr5"]').parents('fieldset').show();
                        }
                    }
            else if( parseDate($(".date").val()) >= parseDate('01-10-2011') && $(".CompanyCategory").val() == 'BYGUAR')
            {
                $('input[name^="dtr5"]').parents('fieldset').hide();
                $('input[name^="trading"]').parents('fieldset').hide();
            }

        });



        $('input[name^="trading"]').click(function()
        {
            if ($('#trading1').is(':checked') && parseDate($(".date").val()) >= parseDate('01-10-2011'))
            {
                $('input[name^="dtr5"]').parents('fieldset').show();
            }
            // display description
            else if ($('#trading0').is(':checked'))
            {
               $('input[name^="dtr5"]').val(0).parents('fieldset').hide()
            }
        });

    $('.error').attr('style','width:730px;margin-bottom: 20px;');
});
//-->
</script>
{/literal}


{include file="@footer.tpl"}
