{include file="@header.tpl"}
<div id="maincontent">

    {* BREADCRUMBS *}
    {if isset($isService) || $return.company_category == 'BYGUAR'}
        <p style="margin: 0 0 15px 0; font-size: 11px;"><a href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}">{$companyName}</a> &gt; {$title}</p>
    {else}

        <p style="margin: 0 0 15px 0; font-size: 11px;"><a href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}">{$companyName}</a> &gt; <a href="{$this->router->link("#companyDetails#", "company_id=$companyId")}">Company Details</a>
            {if $return.company_category != 'LLP'}

                &gt; <a href="{$this->router->link("#capital#", "company_id=$companyId")}">Statement of Capital</a>
                &gt; <a href="{$this->router->link("#shareholdings#", "company_id=$companyId")}">Shareholdings</a>
                &gt; <a href="{$this->router->link("#pscs#", "company_id=$companyId")}">PSCs</a>
                &gt; {$title}
            {/if}
        </p>
    {/if}

    {if $visibleTitle}
        <h1>{$title}
            {if $companyCategory == 'BYSHR'}
                <a style="float: right; font-size: 20px;font-weight: bold;" href="{$this->router->link(1170)}#step26">HELP</a>
            {/if}
        </h1>
    {/if}

    {* SERVICE *}
    {if isset($isService)}
        <div style="background-color: #cce5ff; border: 1px solid #cccccc; margin-bottom: 15px; padding: 10px;">
            {if $status == 'draft'}
                <ul>
                    <li>Your Confirmation Statement is currently being prepared by our team.</li>
                    <li>If we require any additional information we will contact you via email to request it.</li>
                    <li>This will be sent to the email on your account.</li>
                    <li>Once we have completed the Confirmation Statement we will notify you via email.</li>
                    <li>You will need to log back in to this page, approve and submit the Confirmation Statement.</li>
                </ul>
            {elseif $status == 'completed'}
                Your Confirmation Statement has been prepared and is shown below. Please review all the information and submit it to Companies House
            {elseif $status == 'rejected_by_user'}
                Your Confirmation Statement is currently being reviewed by our team. Any comments you have made with regards to the Confirmation Statement will be addressed and when these have been corrected we will notify you by email. You will need to log back in to this page, approve and submit the Confirmation Statement.
            {elseif $status == 'pending' || $status == 'error' || $status == 'rejected'}
                Your Confirmation Statement has been submitted to Companies House and is currently pending. It can take up to 3 working hours to be accepted. We will email you as soon as it's been accepted.
            {/if}
        </div>
        {if $status == 'completed'}<a style="float:right" href="{$this->router->link(null, "pdf=1")}">Save CS Final Summary as a PDF</a>{/if}
        {* DIY *}
    {else}
        <div style="background-color: #cce5ff; border: 1px solid #cccccc; margin-bottom: 15px; padding: 10px;">
            {if $status == 'pending'}
                Your Confirmation Statement has been submitted to Companies House and is currently pending. It can take up to 3 working hours to be accepted. We will email you as soon as it's been accepted.
            {else}
                <b style="color: red;">WARNING:</b> This is the final step before submission; once you click 'Send' your Confirmation Statement will be sent straight to Companies House. This cannot be undone.
                Please review all the information you have input, checking for any errors.
            {/if}
        </div>
        {if $status != 'pending'}<a style="float:right" href="{$this->router->link(null, "pdf=1")}">Save CS Final Summary as a PDF</a>{/if}
    {/if}


    {* DONT DISPLAY FOR SERVICE IF IS DRAFT *}
    {if isset($return)}

        {if isset($form) && $form->hasErrors()}
            {$form->getHtmlErrorsInfo() nofilter}
        {/if}

        <h2>Company Details</h2>
        <table class="grid2" width="780">
            <col width="250">
            <tr>
                <th>Company Name: </th>
                <td>{$return.company_name}</td>
            </tr>
            <tr>
                <th>Registration Number: </th>
                <td>{$return.company_number}</td>
            </tr>
            <tr>
                <th>Company Category:</th>
                <td>{$return.company_category}</td>
            </tr>
            <tr>
                <th>Made Up Date:</th>
                <td>{$return.made_up_date}</td>
            </tr>
            {if $return.company_category != 'LLP'}
                {if isset($return.trading)}
                    <tr>
                        <th>Trading On Regulated Market:</th>
                        <td>{if $return.trading == 1}Yes{else}No{/if}</td>
                    </tr>
                {/if}
                {if isset($return.dtr5)}
                    <tr>
                        <th>Dtr 5:</th>
                        <td>{if $return.dtr5 == 1}Yes{else}No{/if}</td>
                    </tr>
                {/if}

                {foreach from=$return.sic_code key=key item=code}
                    <tr>
                        <th>Sic Code {$key+1}:</th>
                        <td>{$code}</td>
                    </tr>
                {/foreach}
                <tr>
                    <th>Registered Office:</th>
                    <td>
                        {if isset($return.reg_office.premise) && $return.reg_office.premise != ''}
                            {$return.reg_office.premise}<br />
                        {/if}
                        {if isset($return.reg_office.street) && $return.reg_office.street != ''}
                            {$return.reg_office.street}<br />
                        {/if}
                        {if isset($return.reg_office.thoroughfare) && $return.reg_office.thoroughfare != ''}
                            {$return.reg_office.thoroughfare}<br />
                        {/if}
                        {if isset($return.reg_office.post_town) && $return.reg_office.post_town != ''}
                            {$return.reg_office.post_town}<br />
                        {/if}
                        {if isset($return.reg_office.county) && $return.reg_office.county != ''}
                            {$return.reg_office.county}<br />
                        {/if}
                        {if isset($return.reg_office.country) && $return.reg_office.country != ''}
                            {$return.reg_office.country}<br />
                        {/if}
                        {if isset($return.reg_office.postcode) && $return.reg_office.postcode != ''}
                            {$return.reg_office.postcode}<br />
                        {/if}
                        {if isset($return.reg_office.care_of_name) && $return.reg_office.care_of_name != ''}
                            {$return.reg_office.care_of_name}<br />
                        {/if}
                        {if isset($return.reg_office.po_box) && $return.reg_office.po_box != ''}
                            {$return.reg_office.po_box}<br />
                        {/if}
                        {if isset($return.reg_office.secure_address_ind) && $return.reg_office.secure_address_ind != ''}
                            {$return.reg_office.secure_address_ind}<br />
                        {/if}
                    </td>
                </tr>
            {/if}
            {if isset($return.sail_address.country) && ($return.sail_address.country != 'UNDEF' && $return.sail_address.country != '')}
                <tr>
                    <th>Sail Address:</th>
                    <td>
                        {if isset($return.sail_address.premise) && $return.sail_address.premise != ''}
                            {$return.sail_address.premise}<br />
                        {/if}
                        {if isset($return.sail_address.street) && $return.sail_address.street != ''}
                            {$return.sail_address.street}<br />
                        {/if}
                        {if isset($return.sail_address.thoroughfare) && $return.sail_address.thoroughfare != ''}
                            {$return.sail_address.thoroughfare}<br />
                        {/if}
                        {if isset($return.sail_address.post_town) && $return.sail_address.post_town != ''}
                            {$return.sail_address.post_town}<br />
                        {/if}
                        {if isset($return.sail_address.county) && $return.sail_address.county != ''}
                            {$return.sail_address.county}<br />
                        {/if}
                        {if isset($return.sail_address.country) && $return.sail_address.country != ''}
                            {$return.sail_address.country}<br />
                        {/if}
                        {if isset($return.sail_address.postcode) && $return.sail_address.postcode != ''}
                            {$return.sail_address.postcode}<br />
                        {/if}
                        {if isset($return.sail_address.care_of_name) && $return.sail_address.care_of_name != ''}
                            {$return.sail_address.care_of_name}<br />
                        {/if}
                        {if isset($return.sail_address.po_box) && $return.sail_address.po_box != ''}
                            {$return.sail_address.po_box}<br />
                        {/if}
                        {if isset($return.sail_address.secure_address_ind) && $return.sail_address.secure_address_ind != ''}
                            {$return.sail_address.secure_address_ind}<br />
                        {/if}
                    </td>
                </tr>
            {/if}
            {*
            {if isset($return.members_list) && $return.members_list}
            <tr>
            <th>Members List:</th>
            <td>{$return.members_list}</td>
            </tr>
            {/if}
            *}
        </table>
        {if !empty($summaryView) && $summaryView->getPscs()}
            <h2>PSCs</h2>
        {foreach $summaryView->getPersonPscs() as $psc}
            <table class="grid2" width="780">
                <tr>
                    <th width="200px">Type:</th>
                    <td>Person PSC</td>
                </tr>
                <tr>
                    <th width="200px">Name:</th>
                    <td>{$psc->getFullName()}</td>
                </tr>
                <tr>
                    <th width="200px">Date of birth:</th>
                    <td>{$psc->getPerson()->getDob()}</td>
                </tr>
                <tr>
                    <th width="200px">Nationality:</th>
                    <td>{$psc->getPerson()->getNationality()}</td>
                </tr>
                <tr>
                    <th width="200px">Country of residence:</th>
                    <td>{$psc->getPerson()->getCountryOfResidence()}</td>
                </tr>
                <tr>
                    <th width="200px">Service Address:</th>
                    <td>
                        {if $psc->unwrap()->getAddress()}
                            {$psc->unwrap()->getAddress()->getAddressAsString() nofilter}
                        {else}
                            No address given
                        {/if}
                    </td>
                </tr>
                <tr>
                    <th width="200px">Residential Address:</th>
                    <td>
                        {if $psc->unwrap()->getResidentialAddress()}
                            {$psc->unwrap()->getResidentialAddress()->getAddressAsString() nofilter}
                        {else}
                            No address given
                        {/if}
                    </td>
                </tr>
                <tr>
                    <th>Nature of Control:</th>
                    <td>
                        {if $psc->hasStatementNotification()}
                            <p>{$psc->getStatementNotificationText()}</p>
                        {else}
                            {foreach $psc->getNatureOfControlsTexts() as $noc}
                                <p>{$noc}</p>
                            {/foreach}
                        {/if}
                    </td>
                </tr>
            </table>
            <br>
        {/foreach}

        {foreach $summaryView->getLegalPersonPscs() as $psc}
            <table class="grid2" width="780">
                <tr>
                    <th width="200px">Type:</th>
                    <td>Legal Person PSC</td>
                </tr>
                <tr>
                    <th width="200px">Name:</th>
                    <td>{$psc->getFullName()}</td>
                </tr>
                <tr>
                    <th width="200px">Law Gowerned:</th>
                    <td>{$psc->unwrap()->getIdentification()->getLawGoverned()}</td>
                </tr>
                <tr>
                    <th width="200px">Legal Form:</th>
                    <td>{$psc->unwrap()->getIdentification()->getLegalForm()}</td>
                </tr>
                <tr>
                    <th width="200px">Address:</th>
                    <td>
                        {if $psc->unwrap()->getAddress()}
                            {$psc->unwrap()->getAddress()->getAddressAsString() nofilter}
                        {else}
                            No address given
                        {/if}
                    </td>
                </tr>
                <tr>

                    <th>Nature of Control:</th>
                    <td>
                        {if $psc->hasStatementNotification()}
                            <p>{$psc->getStatementNotificationText()}</p>
                        {else}
                            {foreach $psc->getNatureOfControlsTexts() as $noc}
                                <p>{$noc}</p>
                            {/foreach}
                        {/if}
                    </td>
                </tr>
            </table>
            <br>
        {/foreach}

        {foreach $summaryView->getCorporatePscs() as $psc}
            <table class="grid2" width="780">
                <tr>
                    <th width="200px">Type:</th>
                    <td>Corporate PSC</td>
                </tr>
                <tr>
                    <th width="200px">Name:</th>
                    <td>{$psc->getFullName()}</td>
                </tr>
                <tr>
                    <th width="200px">Law Gowerned:</th>
                    <td>{$psc->unwrap()->getIdentification()->getLawGoverned()}</td>
                </tr>
                <tr>
                    <th width="200px">Legal Form:</th>
                    <td>{$psc->unwrap()->getIdentification()->getLegalForm()}</td>
                </tr>
                <tr>
                    <th width="200px">Place Registered:</th>
                    <td>{$psc->unwrap()->getIdentification()->getPlaceRegistered()}</td>
                </tr>
                <tr>
                    <th width="200px">Registration Number:</th>
                    <td>{$psc->unwrap()->getIdentification()->getRegistrationNumber()}</td>
                </tr>
                <tr>
                    <th width="200px">Country or State:</th>
                    <td>{$psc->unwrap()->getIdentification()->getCountryOrState()}</td>
                </tr>
                <tr>
                    <th width="200px">Address:</th>
                    <td>
                        {if $psc->unwrap()->getAddress()}
                            {$psc->unwrap()->getAddress()->getAddressAsString() nofilter}
                        {else}
                            No address given
                        {/if}
                    </td>
                </tr>
                <tr>
                    <th>Nature of Control:</th>
                    <td>
                        {if $psc->hasStatementNotification()}
                            <p>{$psc->getStatementNotificationText()}</p>
                        {else}
                            {foreach $psc->getNatureOfControlsTexts() as $noc}
                                <p>{$noc}</p>
                            {/foreach}
                        {/if}
                    </td>
                </tr>
            </table>
            <br>
        {/foreach}
        {/if}

        <h2>Officers</h2>
        {if isset($return.officers) && $return.officers != ''}
            {foreach from=$return.officers key=k item=officer}
                {if $officer.type == 'PSC'}
                    {continue}
                {/if}
                <table class="grid2" width="780">
                    <col width="250">
                    {if isset($officer.title) && $officer.title != ''}
                        <tr>
                            <th>Officer Title:</th>
                            <td>{$officer.title}</td>
                        </tr>
                    {/if}
                    {if isset($officer.forename) && $officer.forename != ''}
                        <tr>
                            <th>Officer Forename:</th>
                            <td>{$officer.forename}</td>
                        </tr>
                    {/if}
                    {if isset($officer.middle_name) && $officer.middle_name}
                        <tr>
                            <th>Officer Middle Name:</th>
                            <td>{$officer.middle_name}</td>
                        </tr>
                    {/if}
                    {if isset($officer.surname) && $officer.surname}
                        <tr>
                            <th>Officer Surname:</th>
                            <td>{$officer.surname}</td>
                        </tr>
                    {/if}
                    {if isset($officer.corporate_name) && $officer.corporate_name}
                        <tr>
                            <th>Corporate Name:</th>
                            <td>{$officer.corporate_name}</td>
                        </tr>
                    {/if}
                    {if isset($officer.type) && $officer.type}
                        <tr>
                            <th>Officer Type:</th>
                            <td>{$officer.type}</td>
                        </tr>
                    {/if}
                    <tr>
                        {if isset($officer.corporate_name) && $officer.corporate_name}
                            <th>Address:</th>
                            {else}
                            <th>Service Address:</th>
                            {/if}
                        <td>
                            {if isset($officer.premise) && $officer.premise != ''}
                                {$officer.premise}<br />
                            {/if}
                            {if isset($officer.street) && $officer.street != ''}
                                {$officer.street}<br />
                            {/if}
                            {if isset($officer.thoroughfare) && $officer.thoroughfare != ''}
                                {$officer.thoroughfare}<br />
                            {/if}
                            {if isset($officer.post_town) && $officer.post_town != ''}
                                {$officer.post_town}<br />
                            {/if}
                            {if isset($officer.county) && $officer.county != ''}
                                {$officer.county}<br />
                            {/if}
                            {if isset($officer.country) && $officer.country != ''}
                                {$officer.country}<br />
                            {/if}
                            {if isset($officer.postcode) && $officer.postcode != ''}
                                {$officer.postcode}<br />
                            {/if}
                            {if isset($officer.care_of_name) && $officer.care_of_name != ''}
                                {$officer.care_of_name}<br />
                            {/if}
                            {if isset($officer.po_box) && $officer.po_box != ''}
                                {$officer.po_box}<br />
                            {/if}
                        </td>
                    </tr>
                    {if isset($officer.previous_names) && isset($officer.typrevious_namespe)}
                        <tr>
                            <th>Previous Names:</th>
                            <td>{$officer.previous_names}</td>
                        </tr>
                    {/if}
                    {if isset($officer.type) && $officer.type == 'Director'}
                        {if isset($officer.dob) && $officer.dob}
                            <tr>
                                <th>Date of Birth:</th>
                                <td>{$officer.dob}</td>
                            </tr>
                        {/if}
                        {if isset($officer.nationality) && $officer.nationality}
                            <tr>
                                <th>Nationality:</th>
                                <td>{$officer.nationality}</td>
                            </tr>
                        {/if}
                        {if isset($officer.occupation) && $officer.occupation}
                            <tr>
                                <th>Occupation:</th>
                                <td>{$officer.occupation}</td>
                            </tr>
                        {/if}
                    {/if}
                    {if isset($officer.type) && $officer.type == 'Member'}
                        {if isset($officer.designated_ind) }
                            <tr>
                                <th>Designated Member:</th>
                                <td>{if $officer.designated_ind == '1'}Yes{else}No{/if}</td>
                            </tr>
                        {/if}
                    {/if}
                    {if isset($officer.country_of_residence) && $officer.country_of_residence}
                        <tr>
                            <th>Country of Residence:</th>
                            <td>{$officer.country_of_residence}</td>
                        </tr>
                    {/if}
                    {if isset($officer.corporate) && $officer.corporate}
                        {if isset($officer.identification_type) && $officer.identification_type}
                            <tr>
                                <th>Identification Type:</th>
                                <td>{$officer.identification_type}</td>
                            </tr>
                        {/if}
                        {if isset($officer.place_registered) && $officer.place_registered}
                            <tr>
                                <th>Place Registered:</th>
                                <td>{$officer.place_registered}</td>
                            </tr>
                        {/if}
                        {if isset($officer.registration_number) && $officer.registration_number}
                            <tr>
                                <th>Registration Number:</th>
                                <td>{$officer.registration_number}</td>
                            </tr>
                        {/if}
                        {if isset($officer.law_governed) && $officer.law_governed}
                            <tr>
                                <th>Law Governed:</th>
                                <td>{$officer.law_governed}</td>
                            </tr>
                        {/if}
                        {if isset($officer.legal_form) && $officer.legal_form}
                            <tr>
                                <th>Legal Form:</th>
                                <td>{$officer.legal_form}</td>
                            </tr>
                        {/if}
                    {/if}
                </table>
                {if $return.company_category == 'LLP'}
                    <br />
                    <br />
                {/if}
                <br>
            {/foreach}
        {/if}

        {if isset($return.shares) && $return.shares != ''}
            <h2>Shares</h2>
            {assign var=i value=0}
            {foreach from=$return.shares key=k item=share}
                {if $i!=0}<br /><br />{/if}
                {assign var=i value=$i+1}
                <table class="grid2" width="780">
                    <col width="250">
                    {if isset($share.share_class) && $share.share_class!= ''}
                        <tr>
                            <th>Share Class:</th>
                            <td>{$share.share_class}</td>
                        </tr>
                    {/if}
                    {if isset($share.prescribed_particulars) && $share.prescribed_particulars!= ''}
                        <tr>
                            <th>Prescribed Particulars:</th>
                            <td>{$share.prescribed_particulars}</td>
                        </tr>
                    {/if}
                    {if isset($share.num_shares) && $share.num_shares!= ''}
                        <tr>
                            <th>Number of Shares:</th>
                            <td>{$share.num_shares}</td>
                        </tr>
                    {/if}
                    {if isset($share.amount_paid) && $share.amount_paid!= ''}
                        <tr>
                            <th>Paid Per Share:</th>
                            <td>{$share.amount_paid}</td>
                        </tr>
                    {/if}
                    {if isset($share.amount_unpaid) && $share.amount_unpaid!= ''}
                        <tr>
                            <th>Unpaid Per Share:</th>
                            <td>{$share.amount_unpaid}</td>
                        </tr>
                    {/if}
                    {if isset($share.currency) && $share.currency!= ''}
                        <tr>
                            <th>Currency:</th>
                            <td>{$share.currency}</td>
                        </tr>
                    {/if}
                    {if isset($share.aggregate_nom_value) && isset($share.currenaggregate_nom_valuecy) && $share.currenaggregate_nom_valuecy!= ''}
                        <tr>
                            <th>Aggregate Nom Value:</th>
                            <td>{$share.aggregate_nom_value}</td>
                        </tr>
                    {/if}
                </table>
            {/foreach}
        {/if}

        {if isset($return.shareholdings) && $return.shareholdings != ''}
            <h2>Shareholdings</h2>
            {foreach from=$return.shareholdings key=k item=shareholding}
                <table class="grid2" width="780">
                    <col width="250">
                    {if isset($shareholding.share_class) && $shareholding.share_class!= ''}
                        <tr>
                            <th>Share Class:</th>
                            <td>{$shareholding.share_class}</td>
                        </tr>
                    {/if}
                    {if isset($shareholding.number_held) && $shareholding.number_held!= ''}
                        <tr>
                            <th>Number Held:</th>
                            <td>{$shareholding.number_held}</td>
                        </tr>
                    {/if}
                    {foreach from=$shareholding.shareholder key=k item=shareholder}
                        {if isset($shareholder.forename) && $shareholder.forename!= ''}
                            <tr>
                                <th>Forename:</th>
                                <td>{$shareholder.forename}</td>
                            </tr>
                        {/if}
                        {if isset($shareholder.middle_name) && $shareholder.middle_name!= ''}
                            <tr>
                                <th>Middle Name:</th>
                                <td>{$shareholder.middle_name}</td>
                            </tr>
                        {/if}
                        {if isset($shareholder.surname) && $shareholder.surname!= ''}
                            <tr>
                                <th>Surname:</th>
                                <td>{$shareholder.surname}</td>
                            </tr>
                        {/if}
                        {if isset($shareholder.isRemoved) && $shareholder.isRemoved==0}
                            <tr>
                                <th>Address:</th>
                                <td>
                                    {if isset($shareholder.premise) && $shareholder.premise!= ''}
                                        {$shareholder.premise}<br />
                                    {/if}
                                    {if isset($shareholder.street) && $shareholder.street!= ''}
                                        {$shareholder.street}<br />
                                    {/if}
                                    {if isset($shareholder.thoroughfare) && $shareholder.thoroughfare!= ''}
                                        {$shareholder.thoroughfare}<br />
                                    {/if}
                                    {if isset($shareholder.post_town) && $shareholder.post_town!= ''}
                                        {$shareholder.post_town}<br />
                                    {/if}
                                    {if isset($shareholder.county) && $shareholder.county!= ''}
                                        {$shareholder.county}<br />
                                    {/if}
                                    {if isset($shareholder.country) && $shareholder.country!= ''}
                                        {$shareholder.country}<br />
                                    {/if}
                                    {if isset($shareholder.postcode) && $shareholder.postcode!= ''}
                                        {$shareholder.postcode}<br />
                                    {/if}
                                </td>
                            </tr>
                        {/if}
                    {/foreach}
                </table>
                <br />
                <br />
            {/foreach}
        {/if}

        {* DOWNLOAD-PDF *}
        {if isset($isService)}
            {if $status == 'completed'}
                <a style="float:right" href="{$this->router->link(null, "pdf=1")}">Save CS Final Summary as a PDF</a><br />
            {/if}
        {else}
            {if $status != 'pending'}
                <a style="float:right" href="{$this->router->link(null, "pdf=1")}">Save CS Final Summary as a PDF</a><br />
            {/if}
        {/if}

        {* FORM *}

        {literal}
            <script type="text/javascript">
                <!--
            $(document).ready(function() {
                    $('#reject').click(function() {
                        $('#confirmationFieldset').hide();
                        $('#rejectionFieldset').fadeIn();
                        $('#actionFieldset').hide();
                        return false;
                    })

                    $('#closeReject').click(function() {
                        $('#confirmationFieldset').fadeIn();
                        $('#rejectionFieldset').hide();
                        $('#actionFieldset').fadeIn();
                        return false;
                    });

                    $('#rejectSubmit').click(function() {
                        var value = $('#rejectionText').val();
                        if (value == '') {
                            alert('Please provide rejection text');
                            return false;
                        }
                    });
                });
                //-->
            </script>
        {/literal}


        {* FORM *}
        {if isset($form)}

            {$form->getBegin() nofilter}

            <fieldset id="confirmationFieldset">
                <legend>Confirmation</legend>
                <table>
                    <tr>
                        <td>{$form->getControl('confirm') nofilter}</td>
                        <td>{$form->getLabel('confirm') nofilter}</td>
                        <td><span class="ff_control_err">{$form->getError('confirm')}</span></td>
                    </tr>
                </table>
            </fieldset>

            <div style="background-color: #cce5ff; border: 1px solid #cccccc; margin-bottom: 15px; padding: 10px;">
                <b style="color: red;">WARNING:</b> This is the final step before submission; once you click 'Send' your Confirmation Statement will be sent straight to Companies House. This cannot be undone.
                Please review all the information you have input, checking for any errors.
            </div>

            {* SERVICE *}
            {if isset($isService)}
                <fieldset id="actionFieldset">
                    <legend>Action</legend>
                    <table>
                        <col width="220">
                        <tr>
                            <td>{$form->getControl('submit') nofilter}</td>
                            <td>{$form->getControl('reject') nofilter}</td>
                        </tr>
                    </table>
                </fieldset>

                <fieldset id="rejectionFieldset" style="display: none;">
                    <legend>Reject</legend>
                    <table border="0">
                        <tr>
                            <td valign="center">{$form->getLabel('rejectionText') nofilter}</td>
                            <td valign="top">{$form->getControl('rejectionText') nofilter}</td>
                            <td><span class="ff_control_err">{$form->getError('rejectionText')}</span></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td valign="bottom">{$form->getControl('rejectSubmit') nofilter} <a href="#" id="closeReject">Close</a></td>
                        </tr>
                    </table>
                </fieldset>
            {else}
                <fieldset>
                    <legend>Action</legend>
                    <table>
                        <tr>
                            <td>{$form->getControl('submit') nofilter}</td>
                        </tr>
                    </table>
                </fieldset>
            {/if}

            {$form->getEnd() nofilter}
        {/if}

    {/if}

</div>

<style>
    h2 {
        margin-top: 1em;
    }
</style>

{include file="@footer.tpl"}
