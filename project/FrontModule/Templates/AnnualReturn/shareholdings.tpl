{include file="@header.tpl"}
<div id="maincontent">

	<p style="margin: 0 0 15px 0; font-size: 11px;"><a href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}">{$companyName}</a> &gt; <a href="{$this->router->link("#companyDetails#", "company_id=$companyId")}">Company Details</a> &gt; <a href="{$this->router->link("#capital#", "company_id=$companyId")}">Statement of Capital</a> &gt; {$title}</p>

	<h1>{$title} 
        <a href="{$this->router->link("#addShareholding#", "company_id=$companyId")}" style="font-size: 12px;">[add new shareholder]</a>
        {if $companyCategory == 'BYSHR'}
            <a style="float: right; font-size: 20px;font-weight: bold;" href="http://support.companiesmadesimple.com/article/128-how-to-complete-your-annual-return-diy#step8" target="_blank">HELP</a>
        {/if}
    </h1>

    <div style="background-color: #cce5ff; border: 1px solid #cccccc; margin-bottom: 15px; padding: 10px;">
        This section represents each individual shareholder for the company. Here you will be required to update the shareholder information 
        in regards to any transfers that were made either to or from each shareholder. Any shares that were allocated to an existing or new 
        shareholder (using form SH01) should also be represented here. All updates are for the period since the last Annual Return was filed.
        <p style="margin: 5px 0; padding: 0;">
        <b style="color: red; font-size: 14px;">Important:</b> Any transfers or allocations listed below must have already occurred since 
        the last Return and the relevant paperwork filed.
        </p>
        <b>Reminder:</b> This is a record of all share transactions that have taken place since the last Annual Return. The Annual Return is a 
        snapshot of the company at the date of the return. It is NOT used to make changes to the company in ANY regard.
    </div>

	{if isset($shareholdings) && $shareholdings != ''}
		{foreach from=$shareholdings key=key item=shareholding}	
			<fieldset style="border: 2px solid #FF6600">	
				<legend>
					<span style="color:#FF6600; font-size:18px;">Shareholder</span> 
					<span style="font-size: 12px; font-weight: normal; font-style: italic;">
						{if $shareholding}
							[ <a href="{$this->router->link("#editShareholding#", "company_id=$companyId", "shareholdingId=$key")}">edit</a> /
							<a href="{$this->router->link("#addTransfer#", "company_id=$companyId", "shareholdingId=$key")}">add  new transfer</a> ]
							<a href="{$this->router->link(null, "company_id=$companyId", "deleteShareholding=$key")}" onclick="return confirm('Are You Sure?')">remove</a> ]
						{/if}
					</span>
				</legend>
				
				{* SHAREHOLDINGS *}		
				<table class="grid2" width="100%">		
                    <col>
                    <col width="160">
                    <col width="100">
                    <tr>
                        <th>Shareholder's  Name</th>
                        <th>Share Class</th>
                        <th>Number Held</th>
                    </tr>
                    <tr>
                        <td>
                            {if isset($shareholding.shareholders)}
                            {foreach from=$shareholding.shareholders item=shareholder}
                                {$shareholder.name} <br />
                            {/foreach}
                            {else}
                                No shareholders
                            {/if}
                        </td>
                        <td>
                            {if isset($shareholding.share_class)}
                                {if isset($shareholding.share_class)}{$shareholding.share_class}{/if}
                            {else}		
                                <span style="color: red;"><b>Share Class undefined, click edit to update.</b></span>
                            {/if}
                        </td>
                        <td>
                            {if isset($shareholding.number_held)}
                                {if isset($shareholding.number_held)}{$shareholding.number_held}{/if}
                            {else}		
                                0
                            {/if}
                        </td>
                    </tr>
                </table>
				
				{* TRANSFERS *}
				<br />
				<h6>Transfer History</h6>
				<br />
				<table class="grid2" width="100%">
				<col>
				<col width="160">
				<col width="100">
				<tr>
					<th>Date of Transfer</th>
					<th class="right">No. of Shares Transfered</th>
					<th class="center">Action</th>
				</tr>
				{if isset($shareholding.transfers)}
				{foreach from=$shareholding.transfers key=k item=transfer}
				<tr>
					<td>{$transfer.date}</td>
					<td class="right">{$transfer.num}</td>
					<td class="center"><a href="{$this->router->link(null, "company_id=$companyId", "shareholdingId=$key", "deleteTransfer=$k")}" onclick="return confirm('Are You Sure?')">remove</a></td>
				</tr>
				{/foreach}
				{else}
				<tr>
					<td colspan="3">No transfers</td>
				</tr>
				{/if}
				</table>
			</fieldset>
		{/foreach}
	{/if}
	
	{$form nofilter}
	
</div>
{include file="@footer.tpl"}
