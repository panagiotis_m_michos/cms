{include file="@header.tpl"}
<div id="maincontent">

	<p style="margin: 0 0 15px 0; font-size: 11px;"><a href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}">{$companyName}</a> &gt; <a href="{$this->router->link("#companyDetails#", "company_id=$companyId")}">Company Details</a> &gt; {$title}</p>

	{if $visibleTitle}
    <h1>{$title}        
        {if $companyCategory == 'BYSHR'}
            <a style="float: right; font-size: 20px;font-weight: bold;" href="{$this->router->link(1170)}#step15">HELP</a>
        {/if}
    </h1>
    {/if}
	
    <div style="background-color: #cce5ff; border: 1px solid #cccccc; margin-bottom: 15px; padding: 10px;">
        This is the total issued share capital in the company. The share capital is divided by currency and 
        then divided by the share class within each currency. Individual shareholding details are completed in the next step.
        <br />
        <br />
        <span style="font-size: 11px; color: #333333;">
            <b>Share Class:</b> Sometimes Companies House will return a share class of 'Other'. In this instance you will need to update your 
            share class manually. If you do not know your share class, reference to it can be found in the company's 
            M&amp;A - although in most cases it will be Ordinary (Ord). If you formed the company with us the share class will be Ordinary (Ord).
        </span>
        <br />
        <br />
        <span style="font-size: 11px; color: #333333;">
            <ul>
                <li><b>Fully Paid</b> - All issued shares have been paid for in full into the company’s bank account.</li>
                <li><b>Unpaid</b> - This means that the company has not received any money to date for the issued shares.</li>
                <li><b>Partially Paid</b> - This means the shares have been paid for in part.</li>
            </ul>
        </span>
    </div>

	{$form nofilter}
	
	{literal}
	<script type="text/javascript">
	<!--
	
	// hide all partialy paid
	$('input[id^="partialy_paid_"]').parents('tr').hide();
		
	// show partialy paid based on checkbox
	$('input[id^="paid"]:checked').each(function() {
		toggle(this);
	});
	
	// click trigger on checkbox
	$(document).ready(function() {
		$('input[id^="paid"]').click(function() {
			toggle(this);
		});
	});
	
	/**
	 * Will show and diplay partialy paid field
	 * 
	 * @param jQuery _this
	 * @return void
	 */
	function toggle(_this) {
	
		var id = $(_this).attr('name');
		id = '#partialy_paid_' + id.split('_')[1];
		$row = $(id).parents('tr')
	
		if ($(_this).val() == 'partialy_paid') {
			$row.show();
		} else {
			$row.hide();
		}
	}
	
	//-->
	</script>
	{/literal}
	
</div>
{include file="@footer.tpl"}
