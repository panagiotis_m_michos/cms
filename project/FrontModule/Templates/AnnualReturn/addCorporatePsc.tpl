{include file="@header.tpl"}

<script type="text/javascript">
    var addresses = {$jsPrefillAdresses nofilter};
    var officers = {$jsPrefillOfficers nofilter};
</script>

{literal}
    <script>
        $(function() {
            $("#prefillAddress").change(function() {
                var value = $(this).val();
                var address = addresses[value];
                for (var name in address) {
                    $('#' + name).val(address[name]);
                }
            });

            $("#prefillOfficers").change(function() {
                var value = $(this).val();
                var officer = officers[value];
                for (var name in officer) {
                    $('#' + name).val(officer[name]);
                    if ($('#' + name).is(':radio')) {
                        $('#' + name).attr('checked', officer[name]);
                    }
                }
            });

            function setCheckedState($radios) {
                $.each($radios, function(i, e) {
                    $(e).data('checked', e.checked);
                });
            }

            setCheckedState($(".nature-of-control-container :radio"));

            function toggleChevron(e) {
                $(e.target)
                        .prev()
                        .find("i.nature-of-control-indicator")
                        .toggleClass('fa-chevron-right fa-chevron-down');
            }

            var $natureOfControlContainer = $('.nature-of-control-container');
            $natureOfControlContainer
                    .on('hide.bs.collapse', toggleChevron)
                    .on('show.bs.collapse', toggleChevron)
                    .on('click', ':radio', function() {
                        var newState = !$(this).data('checked');
                        setCheckedState($('.nature-of-control-container :radio[name="' + $(this).attr('name') + '"]'));

                        $(this).prop('checked', newState);
                        $(this).data('checked', newState);
                    });

            $natureOfControlContainer.find(':radio:checked').parents('.collapse').collapse('show')
        });
    </script>
{/literal}

<div id="maincontent">

    <p style="margin: 0 0 15px 0; font-size: 11px;">
        <a href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}">{$companyName}</a>
        &gt; <a href="{$this->router->link("#companyDetails#", "company_id=$companyId")}">Company Details</a>
        &gt; <a href="{$this->router->link("#pscs#", "company_id=$companyId")}">Confirmation Statement - PSCs</a>
        &gt; {$title}
    </p>

    <h2>Add corporate PSC</h2>

    {$form->getBegin() nofilter}
    {if $form->getErrors()|@count gt 0}
        <p class="ff_err_notice ff_red_err" style="width: 940px">Form has <b> {$form->getErrors()|@count}</b> error(s).
            See below for more details:</p>
    {/if}

    <fieldset style="clear: both;">
        <legend>Prefill</legend>
        <table class="ff_table">
            <tbody>
            <tr>
                <th>{$form->getLabel('prefillOfficers') nofilter}</th>
                <td>{$form->getControl('prefillOfficers') nofilter}</td>
                <td>
                    <div class="help-button">
                        <a href="#" class="help-icon">help</a>
                        <em>You can use this prefill option to appoint a previously nominated appointment.</em>
                    </div>
                    <span class="redmsg">{$form->getError('prefillOfficers')}</span>
                </td>
            </tr>
            </tbody>
        </table>
    </fieldset>
    <fieldset>
        <legend>Corporate</legend>
        <table class="ff_table">
            <tbody>
            <tr>
                <th>{$form->getLabel('corporate_name') nofilter}</th>
                <td>{$form->getControl('corporate_name') nofilter}</td>
                <td>
                    <div class="help-button">
                        <a href="#" class="help-icon">help</a>
                        {if $companyType != 'LLP'}
                            <em>This is the company name of the company you want to stand in the position of director. Please note that you cannot nominate the company you are currently forming to be a director of itself.</em>
                        {else}
                            <em>This is the name of the company you want to appoint as a member. Please note that you cannot nominate the company you are currently forming to be a member of itself.</em>
                        {/if}
                    </div>
                    <span class="redmsg">{$form->getError('corporate_name')}</span>
                </td>
            </tr>
            <tr>
                <th>{$form->getLabel('place_registered') nofilter}</th>
                <td>{$form->getControl('place_registered') nofilter}</td>
                <td><span class="redmsg">{$form->getError('place_registered')}</span></td>
            </tr>
            <tr>
                <th>{$form->getLabel('registration_number') nofilter}</th>
                <td>{$form->getControl('registration_number') nofilter}</td>
                <td><span class="redmsg">{$form->getError('registration_number')}</span></td>
            </tr>
            <tr>
                <th>{$form->getLabel('law_governed') nofilter}</th>
                <td>{$form->getControl('law_governed') nofilter}</td>
                <td>
                    <div class="help-button">
                        <a href="#" class="help-icon">help</a>
                        <em>Enter which law governs the company you are appointing. Eg: Common, Civil etc.</em>
                    </div>
                    <span class="redmsg">{$form->getError('law_governed')}</span>
                </td>
            </tr>
            <tr>
                <th>{$form->getLabel('legal_form') nofilter}</th>
                <td>{$form->getControl('legal_form') nofilter}</td>
                <td>
                    <div class="help-button">
                        <a href="#" class="help-icon">help</a>
                        <em>The legal form of the company as defined in its country of registration.</em>
                    </div>
                    <span class="redmsg">{$form->getError('legal_form')}</span>
                </td>
            </tr>
            <tr>
                <th>{$form->getLabel('country_or_state') nofilter}</th>
                <td>{$form->getControl('country_or_state') nofilter}</td>
                <td>
                    <div class="help-button">
                        <a href="#" class="help-icon">help</a>
                        <em>The country or state where the company is registered.</em>
                    </div>
                    <span class="redmsg">{$form->getError('country_or_state')}</span>
                </td>
            </tr>
            </tbody>
        </table>
    </fieldset>

    <fieldset>
        <legend>Prefill</legend>
        <table class="ff_table">
            <tbody>
            <tr>
                <th>{$form->getLabel('prefillAddress') nofilter}</th>
                <td>{$form->getControl('prefillAddress') nofilter}</td>
                <td>
                    <div class="help-button">
                        <a href="#" class="help-icon">help</a>
                        <em>You can use this prefill option to select an address you’ve entered previously.</em>
                    </div>
                    <span class="redmsg">{$form->getError('prefillAddress')}</span>
                </td>
            </tr>
            </tbody>
        </table>
    </fieldset>

    <fieldset>
        <legend>Address</legend>
        <table class="ff_table">
            <tbody>
            <tr>
                <th>{$form->getLabel('premise') nofilter}</th>
                <td>{$form->getControl('premise') nofilter}</td>
                <td>
                    <div class="help-button">
                        <a href="#" class="help-icon">help</a>
                        {if $companyType != 'LLP'}
                            <em>This is the registered office of the company you are appointing as director.</em>
                        {else}
                            <em>This is the registered office of the company you are appointing as a member.</em>
                        {/if}
                    </div>
                    <span class="redmsg">{$form->getError('premise')}</span>
                </td>
            </tr>
            <tr>
                <th>{$form->getLabel('street') nofilter}</th>
                <td>{$form->getControl('street') nofilter}</td>
                <td><span class="redmsg">{$form->getError('street')}</span></td>
            </tr>
            <tr>
                <th>{$form->getLabel('thoroughfare') nofilter}</th>
                <td>{$form->getControl('thoroughfare') nofilter}</td>
                <td><span class="redmsg">{$form->getError('thoroughfare')}</span></td>
            </tr>
            <tr>
                <th>{$form->getLabel('post_town') nofilter}</th>
                <td>{$form->getControl('post_town') nofilter}</td>
                <td><span class="redmsg">{$form->getError('post_town')}</span></td>
            </tr>
            <tr>
                <th>{$form->getLabel('county') nofilter}</th>
                <td>{$form->getControl('county') nofilter}</td>
                <td><span class="redmsg">{$form->getError('county')}</span></td>
            </tr>
            <tr>
                <th>{$form->getLabel('postcode') nofilter}</th>
                <td>{$form->getControl('postcode') nofilter}</td>
                <td><span class="redmsg">{$form->getError('postcode') nofilter}</span></td>
            </tr>
            <tr>
                <th>{$form->getLabel('country') nofilter}</th>
                <td>{$form->getControl('country') nofilter}</td>
                <td><span class="redmsg">{$form->getError('country')}</span></td>
            </tr>
            </tbody>
        </table>
    </fieldset>

    <fieldset class="nature-of-control-container">
        <legend>Nature of Control:</legend>
        <p class="nature-of-control-description">
            {$natureOfControlsDescription}
        </p>
        <span class="redmsg">{$form->getError('significant_influence_or_control')}</span>

        {foreach $natureOfControls as $radioName => $natureOfControl}
            <div data-toggle="collapse" data-target="#{$radioName}" aria-expanded="false">
                <i class="nature-of-control-indicator fa fa-chevron-right"></i>
                <span class="nature-of-control-title">{$natureOfControl['title']}</span>
            </div>

            <div id="{$radioName}" class="collapse" style="padding-left: 20px">
                {foreach $natureOfControl['groups'] as $groupIndex => $group}
                    {if isset($group['title'])}
                        <div data-toggle="collapse" data-target="#{$radioName}_{$groupIndex}" aria-expanded="false">
                            <i class="nature-of-control-indicator fa fa-chevron-right"></i>
                            <span class="nature-of-control-group-title">{$group['title']}</span>
                        </div>
                    {/if}

                    <div id="{$radioName}_{$groupIndex}" {if isset($group['title'])}class="collapse" style="padding-left: 20px;"{/if}>
                        {if isset($group['description'])}
                            <p class="nature-of-control-group-description">{$group['description']}</p>
                        {/if}
                        {foreach array_keys($group['options']) as $optionKey}
                            {$form[$radioName]->getControl($optionKey) nofilter}
                        {/foreach}
                    </div>
                {/foreach}
            </div>
        {/foreach}
    </fieldset>

    <fieldset id="fieldset_2">
        <legend>Action</legend>
        <table class="ff_table">
            <tbody>
            <tr>
                <td><a href="{url route="AnnualReturnControler::PSCS_PAGE" company_id=$companyId}" class="btn_back2 fleft clear">Back</a></td>
                <td width="33%">&nbsp;</td>
                <td>{$form->getControl('continue') nofilter}</td>
            </tr>
            </tbody>
        </table>
    </fieldset>

    {$form->getEnd() nofilter}

</div>
{include file="@footer.tpl"}
