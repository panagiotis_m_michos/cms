{include file="@header.tpl"}
<div id="maincontent">

	{* BREADCRUMBS *}
	<p style="margin: 0 0 15px 0; font-size: 11px;"><a href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}">{$companyName}</a> &gt; <a href="{$this->router->link("#companyDetails#", "company_id=$companyId")}">Company Details</a> &gt; <a href="{$this->router->link("#shareholdings#", "company_id=$companyId")}">Shareholdings</a> &gt; {$title}</p>

	{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
	
    <div style="background-color: #cce5ff; border: 1px solid #cccccc; margin-bottom: 15px; padding: 10px;">
        Please enter the date of the transfer and the number of shares transferred from this shareholder.
        NB: Once you have saved this transfer you will still need to click ‘edit’ next to the shareholder 
        and manually update the number of shares they hold (ie. Number held minus number transferred).
    </div>

	{$form nofilter}
	
</div>
{include file="@footer.tpl"}
