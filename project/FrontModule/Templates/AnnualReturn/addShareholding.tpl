{include file="@header.tpl"}
<div id="maincontent">

	{* BREADCRUMBS *}
	<p style="margin: 0 0 15px 0; font-size: 11px;"><a href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}">{$companyName}</a> &gt; <a href="{$this->router->link("#companyDetails#", "company_id=$companyId")}">Company Details</a> &gt; <a href="{$this->router->link("#capital#", "company_id=$companyId")}">Statement of Capital</a> &gt; <a href="{$this->router->link("#shareholdings#", "company_id=$companyId")}">Shareholdings</a> &gt; {$title}</p>

	{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}

    <div style="background-color: #cce5ff; border: 1px solid #cccccc; margin-bottom: 15px; padding: 10px;">
        If you are adding a corporate shareholder please enter the full company name in the Surname field.
    </div>
	
	{$form nofilter}
	
</div>
{include file="@footer.tpl"}
