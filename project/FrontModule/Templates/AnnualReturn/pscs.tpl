{include file="@header.tpl"}
<div id="maincontent">

    <p style="margin: 0 0 15px 0; font-size: 11px;">
        <a href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}">{$companyName}</a>
        &gt; <a href="{$this->router->link("#companyDetails#", "company_id=$companyId")}">Company Details</a>
        &gt; {$title}
    </p>
    <h1>{$title}</h1>


    <div style="background-color: #cce5ff; border: 1px solid #cccccc; margin-bottom: 15px; padding: 10px;">
        <p>Companies need to keep a register of people with significant control (‘PSC register’) from 6 April 2016.</p>
        <p>A PSC is anyone in a company, LLP or SE who meets one or more of the conditions listed in the legislation.
            Thisis someone who:</p>
        <ul>
            <li>owns more than 25% of the company’s shares</li>
            <li>holds more than 25% of the company’s voting rights</li>
            <li>holds the right to appoint or remove the majority of directors</li>
            <li>has the right to, or actually exercises significant influence or control</li>
            <li>holds the right to exercise or actually exercises significant control over a trust or company that
                meetsone of the first 4 conditions.
            </li>
        </ul>
        <a href="#">Learn More about PSC</a>
    </div>

    <h4 class="confirmation-statement__subsection-title">What do you want to do?</h4>
    <a href="{$this->router->link("AnnualReturnControler::PSC_PERSON_ADD", "company_id=$companyId")}">Add a person as PSC</a><br>
    <a href="{$this->router->link("AnnualReturnControler::PSC_LEGAL_PERSON_ADD", "company_id=$companyId")}">Add a legal person as PSC</a><br>
    <a href="{$this->router->link("AnnualReturnControler::PSC_CORPORATE_ADD", "company_id=$companyId")}">Add a corporate as PSC</a><br>
    <a href="#" data-toggle="modal" data-target="#no-psc-modal">I believe that there is no registrable person or my company is under a different situation</a>

    <h4 class="confirmation-statement__subsection-title">Current PSC information</h4>
    {if $summaryView->hasNoPscReason()}
        <i>{$summaryView->getNoPscReason()}</i>
    {elseif $summaryView->hasPscs()}
        <table class="confirmation-statement__psc-list__table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Nature of control</th>
                <th class="confirmation-statement__psc-list__action" colspan="2">Action</th>
            </tr>
            </thead>
            <tbody>
            {foreach $summaryView->getPersonPscs() as $psc}
                <tr>
                    <td>{$psc->getFullName()}</td>
                    <td>
                        {if $psc->hasStatementNotification()}
                            <p>{$psc->getStatementNotificationText()}</p>
                        {else}
                            {foreach $psc->getNatureOfControlsTexts() as $noc}
                                <p>{$noc}</p>
                            {/foreach}
                        {/if}
                    </td>
                    <td class="confirmation-statement__psc-list__action">
                        {if !$psc->hasStatementNotification()}
                            <a href="{$this->router->link("AnnualReturnControler::PSC_PERSON_EDIT", ["company_id" => $companyId, "psc_id" => $psc->getId()])}">Edit</a>
                        {/if}
                    </td>
                    <td class="confirmation-statement__psc-list__action">
                        {if !$psc->hasStatementNotification()}
                            {if $psc->isAlreadyExistingPsc()}
                                {*<a href="{$this->router->link("AnnualReturnControler::PSC_PAGE", ["company_id" => $companyId, "psc_id" => $psc->getId(), "cessate" => true])}" onclick="return confirm('Are you sure?');">Cessate</a>*}
                            {else}
                                <a href="{$this->router->link("AnnualReturnControler::PSC_PAGE", ["company_id" => $companyId, "psc_id" => $psc->getId(), "delete" => true])}" onclick="return confirm('Are you sure?');">Delete</a>
                            {/if}
                        {/if}
                    </td>
                </tr>
            {/foreach}

            {foreach $summaryView->getCorporatePscs() as $psc}
                <tr>
                    <td>{$psc->getFullName()}</td>
                    <td>
                        {if $psc->hasStatementNotification()}
                            <p>{$psc->getStatementNotificationText()}</p>
                        {else}
                            {foreach $psc->getNatureOfControlsTexts() as $noc}
                                <p>{$noc}</p>
                            {/foreach}
                        {/if}
                    </td>
                    <td class="confirmation-statement__psc-list__action">
                        {if !$psc->hasStatementNotification()}
                          <a href="{$this->router->link("AnnualReturnControler::PSC_CORPORATE_EDIT", ["company_id" => $companyId, "psc_id" => $psc->getId()])}">Edit</a>
                        {/if}
                    </td>
                    <td class="confirmation-statement__psc-list__action">
                        {if !$psc->hasStatementNotification()}
                            {if $psc->isAlreadyExistingPsc()}
                                {*<a href="{$this->router->link("AnnualReturnControler::PSC_PAGE", ["company_id" => $companyId, "psc_id" => $psc->getId(), "cessate" => true])}" onclick="return confirm('Are you sure?');">Cessate</a>*}
                            {else}
                                <a href="{$this->router->link("AnnualReturnControler::PSC_PAGE", ["company_id" => $companyId, "psc_id" => $psc->getId(), "delete" => true])}" onclick="return confirm('Are you sure?');">Delete</a>
                            {/if}
                        {/if}
                    </td>
                </tr>
            {/foreach}

            {foreach $summaryView->getLegalPersonPscs() as $psc}
                <tr>
                    <td>{$psc->getFullName()}</td>
                    <td>
                        {if $psc->hasStatementNotification()}
                            <p>{$psc->getStatementNotificationText()}</p>
                        {else}
                            {foreach $psc->getNatureOfControlsTexts() as $noc}
                                <p>{$noc}</p>
                            {/foreach}
                        {/if}
                    </td>
                    <td class="confirmation-statement__psc-list__action">
                        {if !$psc->hasStatementNotification()}
                            <a href="{$this->router->link("AnnualReturnControler::PSC_LEGAL_PERSON_EDIT", ["company_id" => $companyId, "psc_id" => $psc->getId()])}">Edit</a>
                        {/if}
                    </td>
                    <td class="confirmation-statement__psc-list__action">
                        {if !$psc->hasStatementNotification()}
                            {if $psc->isAlreadyExistingPsc()}
                                {*<a href="{$this->router->link("AnnualReturnControler::PSC_PAGE", ["company_id" => $companyId, "psc_id" => $psc->getId(), "cessate" => true])}" onclick="return confirm('Are you sure?');">Cessate</a>*}
                            {else}
                                <a href="{$this->router->link("AnnualReturnControler::PSC_PAGE", ["company_id" => $companyId, "psc_id" => $psc->getId(), "delete" => true])}" onclick="return confirm('Are you sure?');">Delete</a>
                            {/if}
                        {/if}
                    </td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    {else}
        No PSC added yet.
    {/if}

    {$form nofilter}
</div>

<div class="modal fade" id="no-psc-modal" role="dialog">
    <div class="modal-dialog">
        {$noPscsForm->getBegin() nofilter}
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">No registrable person official wording</h4>
            </div>
            <div class="modal-body">
                <p>
                    The PSC requirements apply whether your company has a PSC or not. If you have taken all reasonable
                    steps and are confident that there are no individuals or legal entities which meet any of the
                    conditions in relation to your company, please select the checkbox below:
                </p>
                {$noPscsForm->getControl('noPscReason') nofilter}
                {$noPscsForm->getLabel('noPscReason') nofilter}
                <p>
                    Check the Guidance for Companies, Societates Europaeae and Limited Liability Partnerships for
                    further information.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {$noPscsForm->getControl('save') nofilter}
            </div>
        </div>
        {$noPscsForm->getEnd() nofilter}
    </div>
</div>

<style>
    .confirmation-statement__psc-list__table td, .confirmation-statement__psc-list__table th {
        padding: 0.5em 1em;
        border: 1px solid grey;

    }

    .confirmation-statement__psc-list__table thead {
        background: lightgrey;
    }

    .confirmation-statement__subsection-title {
        display: block;
        color: #FF6600;
        font-size: 18px;
        border-bottom: 1px solid #a9a9a9;
        padding-bottom: 5px;
    }

    .confirmation-statement__psc-list__action {
        width: 1%;
        white-space: nowrap;
    }
</style>
{include file="@footer.tpl"}
