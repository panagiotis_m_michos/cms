<div class="row padding10 vertical-center choose-bank-container" data-next-page="{$this->router->link("CFCompanyCustomerControler::PAGE_CARD_ONE", ['company_id' => $company->getId()])}">
    <div class="inlineblock pleft20">
        <img src="/front/imgs/one-logo.png" width="78" height="78" alt="oneCard logo"/>
    </div>
    <div class="inlineblock pleft20 width660">
        <h3 class="mnone massivefont">Guaranteed Business Account</h3>

        <p class="largefont">
            No credit checks. From CardOne Banking.
            <a href="javascript:;" class="cf-more-info" rel="cf-card-one">More Info</a>
        </p>
    </div>
    <div class="inlineblock txtcenter width130">
        <button type="button" class="btn btn-default minwidth100 choose-bank-button {if $selected}hidden{/if}">Select</button>
        <span class="green3 txtbold choose-bank-selected {if !$selected}hidden{/if}">
            <i class="fa fa-check"></i> SELECTED
        </span>
    </div>
</div>
