<div class="row padding10 vertical-center choose-bank-container" data-next-page="{$this->router->link("CFCompanyCustomerControler::PAGE_TSB", ['company_id' => $company->getId()])}">
    <div class="inlineblock pleft20">
        <img src="/front/imgs/tsbbank.png" width="78" height="78" alt="TSB"/>
    </div>
    <div class="inlineblock pleft20 width660">
        <h3 class="mnone massivefont">TSB Business Current Account</h3>

        <p class="largefont">
            With {$cashBackAmount|currency:0} Cash Back.
            <a href="javascript:;" class="cf-more-info" rel="cf-tsb">More Info</a>
        </p>
    </div>
    <div class="inlineblock txtcenter width130">
        <button type="button" class="btn btn-default minwidth100 choose-bank-button {if $selected}hidden{/if}">Select</button>
        <span class="green3 txtbold choose-bank-selected {if !$selected}hidden{/if}">
            <i class="fa fa-check"></i> SELECTED
        </span>
    </div>
</div>
