<style>
    .choose-bank-button:focus, .choose-bank-button:active {
        outline: none !important;
    }
</style>
<div style="font-size:16px;" class="alert alert-info" role="alert">
    <strong>Note - </strong>A business bank account will help you efficiently manage your company’s finances. Bank with one of our prestigious partners and receive {$cashBackAmount|currency:0} cash back*
</div>
<table class="table table-bordered table-condensed choose-bank-table">
    <tbody>
    {foreach from=$options item=bank name=bankOptions}
        {assign var=selected value=($bank == $selectedBank)}
        <tr class="choose-bank-row {if !$smarty.foreach.bankOptions.first && !$expanded}hidden{/if}">
            <td>
                {if $bank == "TSB"}
                    {include "./@tsb.tpl"}
                {elseif $bank == "BARCLAYS"}
                    {include "./@barclays.tpl"}
                {elseif $bank == "CARD_ONE"}
                    {include "./@cardOne.tpl"}
                {/if}
            </td>
        </tr>
    {/foreach}

    {if !$bankingRequired}
        <tr class="choose-bank-row {if !$expanded}hidden{/if}">
            <td>
                <div class="row padding10 vertical-center choose-bank-container" data-next-page="{$this->router->link("CFRegisteredOfficeControler::REGISTER_OFFICE_PAGE", ['company_id' => $company->getId(), 'noBank' => 1])}">
                    <div class="inlineblock pleft20">
                        <img src="/front/imgs/no-account.png" width="78" height="78" alt="no acctoun"/>
                    </div>
                    <div class="inlineblock pleft20 width660">
                        <h3 class="mnone massivefont">No Thanks</h3>

                        <p class="largefont">I will arrange my own Business Banking</p>
                    </div>
                    <div class="inlineblock txtcenter width130">
                        <button type="button" class="btn btn-default minwidth100 choose-bank-button">Select</button>
                        <span class="green3 txtbold choose-bank-selected hidden">
                            <i class="fa fa-check"></i> SELECTED
                        </span>
                    </div>
                </div>
            </td>
        </tr>
    {/if}
    </tbody>
</table>
<div class="row more-options {if $expanded}hidden{/if}">
    <div class="col-md-3">
        <a href="#" class="font20" onclick="_gaq.push(['_trackEvent', 'Links', 'Click', 'Banking options View more options']);">View more options <i class="fa fa-angle-down"></i></a>
    </div>
</div>
<div class="row">
    <div class="col-md-2 col-md-offset-10">
        <a href="#" role="button" class="btn btn-orange minwidth100 button-continue">Continue <i class="fa fa-angle-right"></i></a>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <p class="small">*Only for Barclays and TSB</p>
    </div>
</div>

<script>
    $(function() {
        $('.choose-bank-button').on('click', function() {
            var $bank = $(this).closest('.choose-bank-container');

            $('.choose-bank-button').removeClass('hidden');
            $('.choose-bank-selected').addClass('hidden');
            $bank.find('.choose-bank-button').addClass('hidden');
            $bank.find('.choose-bank-selected').removeClass('hidden');

            $('.button-continue').attr('href', $bank.data('next-page'));

            return false;
        });

        $('.more-options').on('click', function() {
            $('.choose-bank-table .choose-bank-row').removeClass('hidden');
            $(this).addClass('hidden');

            return false;
        });

        $('.button-continue').attr('href', $('.choose-bank-selected').filter(':visible').closest('.choose-bank-container').data('next-page'));
    });
</script>

{include file="@popups/CFBarclaysPopup.tpl"}
{include file="@popups/CFOneCardPopup.tpl"}
{include file="@popups/CFHsbcPopup.tpl"}
{include file="@popups/CFTsbPopup.tpl"}
