{assign "hide" 1} {* hide left column in header.tpl *}
{include file="@header.tpl"}
	<div id="maincontent1" style="width: 955px;">
            <div id="body404">                    
                <div style="width:560px; height:595px; float:right;">
                    <p class="h1" style="margin-top:25px;">Well, you<br>found Wally!</p>
                    <p class="h2">But not the page you were <br>looking for… sorry.</p>
                    <p class="h3" style="padding-top:25px;">Why not search our site?</p>
                    <script>
                            (function() {
                            var cx = '004293338795185150162:j-metbixcs0';
                            var gcse = document.createElement('script');
                            gcse.type = 'text/javascript';
                            gcse.async = true;
                            gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') + '//www.google.com/cse/cse.js?cx=' + cx;
                            var s = document.getElementsByTagName('script')[0];
                            s.parentNode.insertBefore(gcse, s); })();
                    </script>
                    <gcse:searchbox-only></gcse:searchbox-only>
                    <p class="h3">Or try one of the following links:</p>
                    <div style="width:560px; height:auto; float:right;">
                        <div style="width:184px; height:70px; float:left;">
                            <a href="/">
                                <img width="184px" height="70px" src="{$urlImgs}404/homeoff.jpg" alt="Go to Homepage" onmouseover="this.src='{$urlImgs}404/homeon.jpg'" onmouseout="this.src='{$urlImgs}404/homeoff.jpg'"/>
                            </a>
                            <a href="/"><p class="link1">Go to<br>Homepage</p></a>
                        </div>
                        <div style="width:184px; height:70px; float:left;">
                            <a href="http://support.companiesmadesimple.com/">
                                <img width="184px" height="70px" src="{$urlImgs}404/knowoff.jpg" alt="View Knowledgebase" onmouseover="this.src='{$urlImgs}404/knowon.jpg'" onmouseout="this.src='{$urlImgs}404/knowoff.jpg'"/>
                            </a>
                            <a href="http://support.companiesmadesimple.com/"><p class="link1">View<br>Knowledgebase</p></a>
                        </div>
                        <div style="width:184px; height:70px; float:left;">
                            <a href="http://support.companiesmadesimple.com/article/288-contacting-us">
                                <img width="184px" height="70px" src="{$urlImgs}404/contactoff.jpg" alt="Contact Us" onmouseover="this.src='{$urlImgs}404/contacton.jpg'" onmouseout="this.src='{$urlImgs}404/contactoff.jpg'"/>
                            </a>
                            <a href="http://support.companiesmadesimple.com/article/288-contacting-us"><p class="link1">Contact<br>Us</p></a>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
                            
                            			
                <div style="width:960px; height:425px; background-color:#f2f2f2;">
                    <div style="width:920px; height:auto; padding: 20px 20px 20px 20px">
                        <p class="parah">Hang on, why am I here?</p>
                        <p class="para">Hello and welcome to our 404 page, you are here because the page you have requested either doesn’t exist anymore or has been moved to a new cosier home (and you will be glad to hear that it’s very happy there).</p><br>
                        <p class="parah">We hope you find what you’re looking for!</p>
                        <p class="para">Like most of the cool kids these days, we like hanging out on social media. Why don’t you join us? We’re more than happy to answer any questions you may have… or have a chat. Just remember to bring some biscuits.</p>
                        <div style="width:184px; height:70px; float:left; padding-top:50px;">
                            <a href="https://www.facebook.com/MadeSimpleGroup"><img width="184px" height="70px" src="{$urlImgs}404/img931.jpg" alt="Facebook"/></a>
                            <a href="https://www.facebook.com/MadeSimpleGroup"><p class="link1">Facebook</p></a>
                        </div>
                        <div style="width:184px; height:70px; float:left;  padding-top:50px;">
                            <a href="https://twitter.com/MadeSimpleGroup"><img width="184px" height="70px" src="{$urlImgs}404/img932.jpg" alt="Twitter"/></a>
                            <a href="https://twitter.com/MadeSimpleGroup"><p class="link1">Twitter</p></a>
                        </div>		
                        <div style="width:184px; height:70px; float:left;  padding-top:50px;">
                            <a href="https://plus.google.com/u/0/b/117583399482954442830/117583399482954442830/posts"><img width="184px" height="70px" src="{$urlImgs}404/img933.jpg" alt="Google Plus"/></a>
                            <a href="https://plus.google.com/u/0/b/117583399482954442830/117583399482954442830/posts"><p class="link1">Google  </p></a>
                        </div>
                        <div style="width:184px; height:70px; float:left;  padding-top:50px;">
                            <a href="http://www.youtube.com/user/TheMadeSimpleGroup"><img width="184px" height="70px" src="{$urlImgs}404/img934.jpg" alt="You Tube"/></a>
                            <a href="http://www.youtube.com/user/TheMadeSimpleGroup"><p class="link1">You Tube</p></a>
                        </div>
                        <div style="width:184px; height:70px; float:left;  padding-top:50px;">
                            <a href="mailto:theteam@madesimplegroup.com"><img width="184px" height="70px" src="{$urlImgs}404/img935.jpg" alt="Email Us"/></a>
                            <a href="mailto:theteam@madesimplegroup.com"><p class="link1">Email</p></a>
                        </div>		
                    </div>
                </div>

            </div>
            <div class="clear"></div>
        </div>
{include file="@footer.tpl"}
