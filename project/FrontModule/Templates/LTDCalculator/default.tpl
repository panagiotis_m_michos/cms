{include file="@header.tpl"}
{literal}
<style>
<!--
#calc-container{
    height: 180px;
    width: 700px;
    margin: 0 auto;
}
#bluest{
    background-color: #009fd9;
    width: 300px;
    height: 90px;
    float: left;
    top: 13px;
    position: relative;
    padding: 10px 10px 10px 20px;
}
#bluest h2{
    font-size: 18px;
    color: #fff;
    font-weight: normal;
}
#bluest p{
    font-size: 13px;
    color: #fff;
    font-weight: normal;
    font-style: italic;
    padding: 0;
}
.ltd-calculator-page-default .form{
    width: 271px;
    float: left;
    position: relative;
    left: -15px;
}
#calc-up{
    background-image: url("/front/imgs/calc_up.png");
    height:84px;
    margin: 0;
}
#calc-mid{
    background-image: url("/front/imgs/calc_mid.png");
    height:39px;
    margin: 0;
}
#calc-down{
    background-image: url("/front/imgs/calc_down.png");
    height:25px;
    margin: 0;
}
#annualProfit{
    border: 0;
    background-color: #f5f5f5;
    padding: 5px;
    font-size: 28px;
    margin: 0 auto;
    display: block;
    width: 130px;
    top: 22px;
    position: relative;
    outline: none;
    height: 30px;
}
#submit{
    width: 100px;
    height: 33px !important;
    left: 136px;
    top: 3px;
    position: relative;
    background-color: #ff7900;
    border: 0;
    color: #fff;
    font-size: 16px;
    cursor: pointer;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px;
}
-->
</style>
{/literal}
<div id="maincontent-full" class="ltd-calculator-page-default">
    <h1 style="color: #000; font-size: 36px;">
        Should you trade as a Limited Company or <br />as a Sole Trader?
    </h1>
    <p style="color: #000; font-size: 18px; margin-bottom: 30px;">
        See how much you could save by registering as a Limited Company, simply enter your annual profits below:
    </p>
    <div id="calc-container">
        <div id="bluest">
            <h2>Enter your projected annual profits:</h2>
            <p>Tip: This figure should exclude all business expenses i.e. rent, travel, utilities.</p>
        </div>
        <div class="form">
            {$form->getBegin() nofilter}
            <div>
                <div id="calc-up">{$form->getControl('annualProfit') nofilter}</div>
                <div id="calc-mid">{$form->getControl('submit') nofilter}</div>
                <div id="calc-down"></div>
            </div>
            <div class="txtcenter">
                <span class="ff_control_err">{$form->getError('annualProfit')}</span>
            </div>
            {$form->getEnd() nofilter}
        </div>
    </div>
    {$text nofilter}
   
</div>

{include file='@blocks/rightColumn.tpl'}

{include file="@footer.tpl"}