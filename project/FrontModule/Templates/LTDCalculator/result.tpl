{include file="@header.tpl"}
{literal}
<style>
<!--
#calc-container{
    height: 150px;
    width: 610px;
    margin: 0 auto;
}
#bluest{
    background-color: #009fd9;
    width: 300px;
    height: 45px;
    float: left;
    top: 13px;
    position: relative;
    padding: 10px 10px 10px 20px;
}
#bluest h2{
    font-size: 18px;
    color: #fff;
    font-weight: normal;
    margin: 0;
}
.ltd-calculator-page-default .form{
    width: 275px;
    float: left;
    position: relative;
    left: -18px;
}
#calc-result{
    background-image: url("/front/imgs/calc-result.png");
    height:103px;
    margin: 0;
}
#calc-result p{
    color: #009fda;
    font-size: 24px;
    position: relative;
    top: 22px;
    left: 60px;
}
table#profits{
    border: 2px solid #e6e7e9;
    font-size: 16px;
    margin: 0 auto;
    width: 700px;
}
table#profits th{
    background-color: #e6e7e9;
    padding: 10px;
}
table#profits td{
    line-height: 30px;
    padding: 0 10px;
}
.view-orange{
    color: #fff;
    background-color: #ff7900;
    padding: 15px 20px;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    font-size: 16px;
    text-decoration: none;
    display: inline-block;
}
-->
</style>
{/literal}
<div id="maincontent-full" class="ltd-calculator-page-default">
    <h1 style="color: #000; font-size: 36px;">
        Self employed or limited company?
    </h1>
    <div id="calc-container">
        <div id="bluest">
            <h2>By registering as a limited company, you could save:</h2>
        </div>
        <div class="form">
            <div>
                <div id="calc-result">
                    <p>{$saving|number_format:2:".":","}</p>
                </div>
                <div class="txtcenter">
                    <p><a href="#" id="toggle1">Show me the calculations</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="toggle1 mbottom20" style="display:none;">
	<table id="profits">
            <tr>
		<th>Annual Profits</th>
                <th></th>
		<th class="txtright">&pound;{$selfEmployed->getAnnualProfit()|number_format:2:".":","}</th>
            </tr>

            <tr>
                <td style="color:#009fda">Self Employed</td>
                <td>Tax Payable</td>
                <td class="txtright">&pound;{$selfEmployed->getTaxPayable()|number_format:2:".":","}</td>
            </tr>
            <tr>
                <td></td>
                <td>National Insurance</td>
                <td class="txtright">&pound;{$selfEmployed->getNationalInsurance()|number_format:2:".":","}</td>
            </tr>
            <tr>
                <td></td>
                <td>Total Tax and NI Payable</td>
                <td class="txtright">&pound;{$selfEmployed->getTotalPayable()|number_format:2:".":","}</td>
            </tr>
            <tr>
                <td></td>
                <td class="txtbold">Net spendable personal income</td>
                <td class="txtright txtbold txtunderline">&pound;{$selfEmployed->getNetSpendablePersonalIncome()|number_format:2:".":","}</td>
            </tr>
            
            <tr style="border-top: 2px solid #e6e7e9;">
		<td style="color:#009fda">Limited Company</td>
                <td>Corporation Tax Payable</td>
		<td class="txtright">&pound;{$company->getTaxPayable()|number_format:2:".":","}</td>
            </tr>
            <tr>
                <td></td>
		<td>Personal Tax Payable</td>
                <td class="txtright">&pound;{$company->getPersonalTaxPayable()|number_format:2:".":","}</td>
            </tr>
            <tr>
                <td></td>
                <td>Total Tax Payable</td>
                <td class="txtright">&pound;{$company->getTotalPayable()|number_format:2:".":","}</td>
            </tr>
            <tr>
                <td></td>
                <td class="txtbold">Net spendable personal income</td>
                <td class="txtright txtbold txtunderline">&pound;{$company->getNetSpendablePersonalIncome()|number_format:2:".":","}</td>
            </tr>
	</table>	
    </div>
    {$text nofilter}
</div>
{literal}
    <script type="text/javascript">
    $('#toggle1').click(function() {
        $('.toggle1').toggle("slow");
        return false;
    });
    </script>
{/literal}
{include file="@footer.tpl"}
