{include file="@header.tpl"}

{literal}
    <style type="text/css">
        <!--
        div#maincontent2 {
            float: left;
            width: 730px;
            padding-left:10px;
        }
        .new-payment-setps{
            margin-bottom: 10px;
        }
        -->
    </style>
{/literal}

{if empty($showAuth)}
    {* TITLE *}
    {if isset($customer)}
        {include file="SplitTestPayment/@paymentBlocks/title.tpl" visibleTitle = $visibleTitle companyName = $companyName customer = $customer}
    {/if}

    {* RIGHT COLUMN *}
    {include file="SplitTestPayment/@paymentBlocks/rightColumn.tpl"} 

    <div id="maincontent2">
        <div class="new-payment-setps">
            <img src="{$urlImgs}progress_bar_buy.png" alt="new payment steps" width="730" height="44"/>
        </div>

        {* MESSAGES *}
        {include file="SplitTestPayment/@paymentBlocks/messages.tpl" node=$node} 

        <div class="payment-page">

            {* PAYMENT LOGIN FORM*}
            {if isset($formLogin) && $formLogin}
                {include file="SplitTestPayment/@paymentBlocks/paymentLoginForm.tpl" formLogin = $formLogin} 
            {/if}


            <div class="sage-payment-form">
                {$form->getBegin() nofilter}

                 

                {* NEW CUSTOMER EMAIL *}
                {if isset($formLogin) && $formLogin}
                    {include file="SplitTestPayment/@paymentBlocks/sageFormEmailFieldset.tpl" form = $form} 
                {/if} 
                
                {if $form->getErrors()|@count gt 0}
                    <p class="ff_err_notice ff_red_err" style="width: 710px">Form has <b> {$form->getErrors()|@count}</b> error(s). See below for more details:</p>
                {/if}

                {* FORM DETAILS*}
                {include file="SplitTestPayment/@paymentBlocks/sageFormBodyFieldset.tpl" form = $form node = $node} 

                <div id="submitBlock">
                    {if !$node->getDisableSage()}
                        {include file="SplitTestPayment/@paymentBlocks/sageSubmitButton.tpl" form = $form basket = $basket type = ''} 
                    {/if}                
                    {include file="SplitTestPayment/@paymentBlocks/paypalSubmitButton.tpl" form = $form basket = $basket} 
                </div>           
                {$form->getEnd() nofilter}
            </div>
        </div>
    </div>  
{else}
    
    <div class="mbottom20 mleft30 width450 payment3dsTitle">
         <span><h1 class="grey7 massivefont txtbold mbottom_small inlineblock">Secure Checkout</h1></span>
         <span class="mleft10 valign-10"><img src="{$urlImgs}lock.png" alt="safebuy" width="32px" height="37px" ></span>
         <h6 class="grey6 normalfont txtnormal">This is an additional security check required by your card provider.</h6>
    </div>
    <div id="authFormBlock">
        <fieldset id="fieldset_3" class="center width550">
            <!--<legend>3D authentication</legend>-->
            <a name="authFormBlock" href="#" style="visibility:hidden"></a>
            <!--<h3 class="mnone pbottom10">Please complete the 3D authentication process below</h3>-->
            <iframe src="{$authFormUrl}" width="500px" height="500px"></iframe>
        </fieldset>
    </div>
    <div class = "payment3dsText grey6 mbottom20 mtop20 mleft30">
        <h4>Registered Cards</h4>
        <span>To complete you order you must provide the requested information from you card provider.</span>
        <h4 class="mtop">Not seen this before?</h4>
        <span>You card provider will ask you to register for this online security service for you protection.</span>
    </div>
{/if}


{include file="@footer.tpl"}
