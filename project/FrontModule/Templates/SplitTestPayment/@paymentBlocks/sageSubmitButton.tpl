<fieldset id="fieldset_3"  class="fieldset3 sageButton {$type}">
    <!-- <legend>Action</legend> -->
    <table class="ff_table2 pleft40">
        <tbody class="valign-top">
            <tr>
                <td>
                    <span>{$form->getControl('submit') nofilter}</span>
                    <p class="lheight20"><span class="normalfont grey8">By clicking 'Make Secure Payment' you are<br />agreeeing to our </span>
                    <a href="{$this->router->link(473,"popup=1")}" class="popupTermsAndCond blue2">Terms and Conditions</a>
                    <br /><span class="littlefont grey">(Opens in a new window)</span></p>
                </td>
                <td>  
                    <div class="mleft20">
                    <div class="grey9 midfont txtbold">Your card will be billed:</div>
                    <span class="extrafont txtbold orange2">&pound;{$basket->getPrice('total')}</span> 
                    <span class="grey9 normalfont"> including VAT</span>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</fieldset>