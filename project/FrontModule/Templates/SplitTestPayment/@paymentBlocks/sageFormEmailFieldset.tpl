<!--[if IE 7]>
<style type="text/css">
.inlineblock {
display: inline-block;
*display: inline;
zoom: 1;
}
</style>
<![endif]-->
<fieldset style="clear: both;color: #000; " class="email-form-check fieldset2">
            <legend class="black bigfont txtbold top10 left18 posabsolute">New &amp; Returning Customers</legend>
            <table class="ff_table2">
                <tbody>
                    <tr>
                        <th>
                            {$form->getLabel('emails') nofilter}
                        </th>
                    </tr>
                    <tr>
                        <td><div class="inlineblock mright8 width30"></div>
                        <div class="inlineblock valign-top">{$form->getControl('emails') nofilter}</div></td>
                        <td style="width: 300px;">
                            <span class="show-ckeck-company" style="display: none;float: left;margin-left: 5px;">
                                <span class="isOkToShow" style="display: none; ">
                                    <img src="{$urlImgs}email-valid.png" alt="email is available" style="float: left;"/>
                                    {*<span style="color: #339900; float: left; padding-left: 3px;padding-top: 7px;">Email is valid</span> *}
                                    <div class="clear"></div>
                                </span>
                                <span class="isNotOkToShow error" style="display: none; ">
                                    <img src="{$urlImgs}mistake.png" alt="email isn't available"/>
                                    <div class="clear"></div>
                                </span>
                            </span>     
                            {* <span class="ff_control_err">{$checkEmail->getError('email')}</span> *}
                            <span class="editEmailLink" style="display: none;float: left; padding-left: 10px;padding-top:13px;">
                                <a href="javascript:;" class="editEmailFields blue2">Edit</a> 
                                <div class="clear"></div>
                            </span>
                            <div class="clear"></div>
                        </td>
                      </tr>
                        <tr>
                        <td><span class="pleft40 normalfont txtitalic grey5 displayblock mtop-5">We use your email to send your company details</span>
                        </td>
                        </tr>
                    
                    <tr>
                        <td><div class="inlineblock mright8 height30 width30"></div>
                            <div class="inlineblock valign-top">
                            <a href="{$this->router->link("PaymentControler::SAGE_CHECK_NEW_CUSTOMER_PAGE")}" class="check-email-avaiability" style="text-decoration: none; color: #000;">
                                <button type="button" class="email-login" onclick="_gaq.push(['_trackEvent', 'Links', 'Click', 'Clicked Create Account or Sign In button on payment page']);">Create account or log in</button>    
                            </a>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </fieldset>   
        <span class="payment-email-ok greenbgr green inlineblock width720 ptop10 pright0 pbtm10 pleft10 mflash">Please enter your payment details below</span>   