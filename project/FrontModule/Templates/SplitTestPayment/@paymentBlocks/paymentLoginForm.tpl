<!--[if IE 7]>
<style type="text/css">
.inlineblock {
display: inline-block;
*display: inline;
zoom: 1;
}
</style>
<![endif]-->
<div class="check-email-login-form">    
    {$formLogin->getBegin() nofilter}
    <fieldset style="clear: both;" class="fieldset2">
        <legend class="black bigfont txtbold top10 left18 posabsolute">New &amp; Returning Customers</legend>
        <table class="ff_table2">
            <tbody>
                <tr>
                    <th>{$formLogin->getLabel('email') nofilter}</th>
                </tr>
                <tr>
                    <td><div class="inlineblock mright8 height30 width30"></div>
                    <div class="inlineblock valign-top">
                        {$formLogin->getControl('email') nofilter}
                    </div>
                    
                        <span class="valign-middle height30 inlineblock editEmailLinkLogin" style="display: none;">
                            <a href="javascript:;"  class="blue2" style="padding-left:10px;">Use another email</a> 
                            <div class="clear"></div>
                        </span>
                        <span class="ff_control_err">{$formLogin->getError('email')}</span>
                    </td>
                </tr>
                <tr>
                    <th>{$formLogin->getLabel('password') nofilter}</th>
                </tr>
                <tr>
                    <td><div class="inlineblock mright8 height30 width30"></div>
                    <div class="inlineblock valign-top">
                        {$formLogin->getControl('password') nofilter}
                        {*<span class="ff_desc">Please provide New Company Name</span>*}
                    </div>
                    </td>
                    <td>
                        <span class="ff_control_err new-err-msg">{$formLogin->getError('password')}</span>
                    </td>
                </tr>
                <tr>
                    <td><div class="inlineblock mright8 height30 width30"></div>
                    <div class="inlineblock valign-top">
                        <span>
                        {$formLogin->getControl('login') nofilter}
                        </span>
                    </div>
                    
                        <span class="valign-middle height30 inlineblock updateForgottenPassword">
                            <a href="{$this->router->link("PaymentControler::NEW_PASSWORD_PAGE")}" class="forgotpassword-email blue2" style="padding-left: 10px;" >Forgotten password</a>
                            <span class="loading-image" style="display: none;">
                                Emailing new password!
                            </span>
                            <span class="newPaswwordSent" style="display: none;">
                                New password sent. <a href="{$this->router->link("PaymentControler::NEW_PASSWORD_PAGE")}" class="forgotpassword-email after-emailing" style="color: #006699;" >Resend password.</a>
                            </span>
                        </span>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>     
    {$formLogin->getEnd() nofilter}
</div>
