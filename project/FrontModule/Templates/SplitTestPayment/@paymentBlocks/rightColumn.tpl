<div class="fright width210">
    <div class="boxn center">
        <h3>Order Summary</h3>
        <div class="nopright noptop boxn_gradient padding15">
            <div class="paymentBasket grey3">
                {foreach from=$basket->items key="key" item="item"}
                    <div class="paymentBasketItem mbottom20">
                        <span>{$item->qty}x </span>
                        <span>{$item->getLngTitle()}</span>
                    </div>
                {/foreach}
                <div class="paymentBasketDetails border1 bordergrey bordertopsolid">
                    <p class="midfont txtbold grey4"><span class="fleft width100">Subtotal:</span><span class="blue">&pound;{$basket->getPrice('subTotal')}</span></p>
                    <p class="normalfont txtbold grey4"><span class="fleft width100">VAT:</span><span class="blue">&pound;{$basket->getPrice('vat')}</span></p>
                </div>
                <div class="paymentBasketTotal mtop border2 bordergrey bordertopsolid">
                    <p class="bigfont txtbold grey4"><span class="fleft width100">Total:</span><span class="orange2">&pound;{$basket->getPrice('total')}</span></p>
                </div>
            </div>
        </div>
    </div>
    <div class="width210 center">
    <div class="mtop16">
        <img src="{$urlImgs}lock-secure.png" alt="lock" width="210px" height="48px" >
    </div>
    <div class="mtop16">
        <img src="{$urlImgs}SafeBuy-Seal-V01-100x114.png" alt="safebuy" width="210px" height="114px" >
    </div>
    <div class="mtop16">
        <a href="#" title="norton" onclick="openNortonSecure();">
            <img src="{$urlImgs}norton.png" alt="norton secured" width="210px" height="61px" >
        </a>
    </div>
    <div class="mtop16">
        <img src="{$urlImgs}sagepay.png" alt="sagepay" width="210px" height="34px" >
    </div>
    <div class="mtop16">
        <a href="#" onclick="creditCardSafeLink()">
            <img src="{$urlImgs}metrics.png" alt="SecurityMetrics" width="210px" height="51px">
        </a>
    </div>
    <div class="mtop16">
        <img src="{$urlImgs}trust-elements.png" alt="lock" width="210" height="413" >
    </div>
    </div>
</div>