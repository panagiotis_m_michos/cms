<!--[if IE 7]>
<style type="text/css">
.inlineblock {
    display: inline-block;
    *display: inline;
    zoom: 1;
    }
.ie7cards {
    display: inline-block;
    *display: inline;
    zoom: 1;
    margin-left:40px;
    }
</style>
<![endif]-->
<fieldset id="fieldset_1a" class="fieldset2">
    <legend class="black bigfont txtbold top10 left18 posabsolute">Payment Method</legend>
    <table class="ff_table2 mleft40">
        <tbody>
            <tr>
                <td>
                    {$form->getLabel('paymentOption') nofilter}
                    <div>{$form->getControl('paymentOption') nofilter}</div>
                </td>
            </tr>
            {if $form->getError('paymentOption')}
            <tr>
                <td>
                    <span class="errormsg">{$form->getError('paymentOption')}</span>
                </td>
            </tr>
            {/if}
        </tbody>
    </table>  
                
</fieldset>
<fieldset id="fieldset_1" class="paymentDetails fieldset2">
    <legend class="top10 left18 posabsolute width690">
        <div class="fleft txtbold black bigfont">Payment Details</div>
        <div class="fright txtitalic txtnormal grey5 normalfont">Fields marked with a * must be filled in</div>
    </legend>
    <table class="ff_table2">
        <tbody>
            {if !$node->getDisableSage()}  
                <tr>
                    <th>{$form->getLabel('cardNumber') nofilter}</th>
                </tr>
                <tr>{assign var="ecardNumber" value=$form->getError('cardNumber')}
                    <td><div class="inlineblock mright8 height30 width30 {if isset($ecardNumber)}fielderror{/if}"></div>
                        <div{if isset($ecardNumber)} class="redfield inlineblock valign-top"{/if} class="inlineblock valign-top">{$form->getControl('cardNumber') nofilter}</div>
                    </td>
                    <td><span class="errormsg">{$form->getError('cardNumber')}</span></td>
                </tr>
                <tr>
                    <td><span class="pleft40 normalfont txtitalic grey11 displayblock mtop-5">Long number on the front of your card</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('cardholder') nofilter}</th>
                </tr>
                <tr>{assign var="ecardholder" value=$form->getError('cardholder')}
                    <td><div class="inlineblock mright8 height30 width30 {if isset($ecardholder)}fielderror{/if}"></div>
                        <div{if isset($ecardholder)} class="redfield inlineblock valign-top"{/if} class="inlineblock valign-top">{$form->getControl('cardholder') nofilter}</div></td>
                    <td><span class="errormsg">{$form->getError('cardholder')}</span></td>
                </tr>
                <tr>
                    <td><span class="pleft40 normalfont txtitalic grey11 displayblock mtop-5">Your name as printed on the front of your card</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('cardType') nofilter}</th>
                </tr>
                <tr>
                    <td><div class="inlineblock mright8 height30 width30"></div>
                    <div class="inlineblock valign-top">{$form->getControl('cardType') nofilter}</div></td>
                    <td><span class="errormsg">{$form->getError('cardType')}</span></td>
                </tr>
                
            <noscript>
            <tr>
                <th>{$form->getLabel('issueNumber') nofilter}</th>
                <td><div>{$form->getControl('issueNumber') nofilter}</div></td>
                <td>The issue number MUST be entered EXACTLY as it appears on the card. e.g. some cards have Issue Number "4", others have "04".</td>
            </tr>
            <tr>
                <th>{$form->getLabel('validFrom') nofilter}</th>
                <td><div>{$form->getControl('validFrom') nofilter}</div></td>
                <td><span class="errormsg">{$form->getError('validFrom')}</span></td>
            </tr>
            </noscript>                       
            <tr class="issuered" style="display:none;">
                <th>{$form->getLabel('issueNumber') nofilter}</th>
            </tr>
            <tr class="issuered" style="display:none;">
                <td><div class="inlineblock mright8 height30 width30"></div>
                <div class="inlineblock valign-top">{$form->getControl('issueNumber') nofilter}</div></td>
                <td><span class="errormsg">{$form->getError('issueNumber')}</span></td>
            </tr>
            <tr class="issuered" style="display:none;">
                <th>{$form->getLabel('validFrom') nofilter}</th>
            </tr>
            <tr class="issuered" style="display:none;">
                <td><div class="inlineblock mright8 height30 width30"></div>
                <div class="inlineblock valign-top">{$form->getControl('validFrom') nofilter}</div></td>
                <td><span class="errormsg">{$form->getError('validFrom')}</span></td>
            </tr>                
            <tr>
                <th>{$form->getLabel('expiryDate') nofilter}</th>
            </tr>
            <tr>{assign var="eexpiryDate" value=$form->getError('expiryDate')}
                <td><div class="inlineblock mright8 height30 width30 {if isset($eexpiryDate)}fielderror{/if}"></div>
                <div class="inlineblock valign-top">{$form->getControl('expiryDate') nofilter}</div></td>
                <td><span class="errormsg">{$form->getError('expiryDate')}</span></td>
            </tr>
            <tr>
                <td><span class="pleft40 normalfont txtitalic grey11 displayblock mtop-5">Find this on the front of your card</span></td>
            </tr>
            <tr>
                <th>{$form->getLabel('CV2') nofilter}</th>
            </tr>
            <tr>{assign var="eCV2" value=$form->getError('CV2')}
                <td><div class="inlineblock mright8 height30 width30 {if isset($eCV2)}fielderror{/if}"></div>
                <div{if isset($eCV2)} class="redfield inlineblock valign-top"{/if} class="inlineblock valign-top">{$form->getControl('CV2') nofilter}</div></td>
                <td><img src="{$urlImgs}cv2.png" alt="CV2" width="47" height="31"></td>
                <td><span class="errormsg">{$form->getError('CV2')}</span></td>
            </tr>
            <tr>
                <td><span class="pleft40 normalfont txtitalic grey11 displayblock mtop-5">The last 3 digits displayed on the back of your card<br />(also known as “CVC” or “CVV” or “CV2”)</span></td>
            </tr>
            </tbody>
        </table>   
    </fieldset>
    <fieldset id="fieldset_1b" class="billingAddress fieldset2">
        <legend class="top10 left18 posabsolute width690">
        <div class="fleft txtbold black bigfont">Billing Address</div>
        <div class="fright txtitalic txtnormal grey5 normalfont">Fields marked with a * must be filled in</div>
    </legend>
        <table class="ff_table2">
            <tbody>
                <tr>
                    <th>{$form->getLabel('postcode') nofilter}</th>
                </tr>
                <tr>{assign var="epostcode" value=$form->getError('postcode')}
                    <td><div class="inlineblock mright8 height30 width30 {if isset($epostcode)}fielderror{/if}"></div>
                    <div{if isset($epostcode)} class="redfield inlineblock valign-top"{/if} class="inlineblock valign-top">{$form->getControl('postcode') nofilter}</div></td>
                    <td>
                        <input type="button" id="find" value="Find My Address" class="btn_postcode"
                               onclick="Javascript: PostcodeAnywhere_Interactive_Find_v1_10Begin(document.getElementById('postcode').value)">
                    </td>
                    <td><span class="errormsg">{$form->getError('postcode')}</span></td>
                </tr>
                <tr>
                    <td class="pleft40">
                        <select id="building" style="display:none;" onchange="Javascript: PostcodeAnywhere_Interactive_RetrieveById_v1_30Begin(getElementById('building').value)">
                    </td>
                </tr>
                <td><span id="buildingMessage" style="display:none;" class="pleft40 normalfont txtitalic grey11 displayblock mtop-5">Please select your address from the drop down above,<br />or enter your address manually below.</span></td>
                <tr>
                    <th>{$form->getLabel('address1') nofilter}</th>
                </tr>
                <tr>{assign var="eaddress1" value=$form->getError('address1')}
                    <td><div class="inlineblock mright8 height30 width30 {if isset($eaddress1)}fielderror{/if}"></div>
                    <div{if isset($eaddress1)} class="redfield inlineblock valign-top"{/if} class="inlineblock valign-top">{$form->getControl('address1') nofilter}</div></td>
                    <td><span class="errormsg">{$form->getError('address1')}</span></td>
                </tr>
                <tr>{assign var="eaddress2" value=$form->getError('address2')}
                    <td><div class="inlineblock mright8 height30 width30 {if isset($eaddress2)}fielderror{/if}"></div>
                    <div class="inlineblock valign-top">{$form->getControl('address2') nofilter}</div></td>
                    <td><span class="errormsg">{$form->getError('address2')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('town') nofilter}</th>
                </tr>
                <tr>{assign var="etown" value=$form->getError('town')}
                    <td><div class="inlineblock mright8 height30 width30 {if isset($etown)}fielderror{/if}"></div>
                    <div{if isset($etown)} class="redfield inlineblock valign-top"{/if} class="inlineblock valign-top">{$form->getControl('town') nofilter}</div></td>
                    <td><span class="errormsg">{$form->getError('town')}</span></td>
                </tr>              
                <tr>
                    <th>{$form->getLabel('country') nofilter}</th>
                </tr>
                <tr>{assign var="ecountry" value=$form->getError('country')}
                    <td><div class="inlineblock mright8 height30 width30 {if isset($ecountry)}fielderror{/if}"></div>
                    <div class="inlineblock valign-top">{$form->getControl('country') nofilter}</div></td>
                    <td><span class="errormsg">{$form->getError('country')}</span></td>
                </tr>
            <noscript>
            <tr>
                <th>{$form->getLabel('billingState') nofilter}</th>
            </tr>
            <tr class="state" style="display:none;">
                <td><div class="mright8 height30 width30"></div></td>
                <td>{$form->getControl('billingState') nofilter}</td>
                <td><span class="errormsg">{$form->getError('billingState')}</span></td>
            </tr>
            </noscript>
            <tr class="state" style="display:none;">
                <th>{$form->getLabel('billingState') nofilter}</th>
            </tr>
            <tr class="state" style="display:none;">
                <td><div class="mright8 height30 width30"></div></td>
                <td>{$form->getControl('billingState') nofilter}</td>
                <td><span class="errormsg">{$form->getError('billingState')}</span></td>
            </tr>
        {/if} 
        </tbody>
    </table>
    <div id="spaintext" style="border: 1px solid #069;padding: 10px;background-color: #cce5ff;line-height: 18px; display: none;">
        Our main payment provider, Sagepay, is currently experiencing issues with some Spanish cards. If you have
        trouble with your payment we suggest selecting <strong>Paypal</strong>, from the Payment Method, above.
    </div>
</fieldset>               
