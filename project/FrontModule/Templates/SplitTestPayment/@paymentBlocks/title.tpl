{if $visibleTitle}
{assign var="signedIn" value=$customer->isSignedIn()}
        <div class="mbottom mtop25">
            {if empty($companyName) or $companyName|h == "CHANGE YOUR COMPANY NAME LTD"}
                <div class="fleft mleft30 width450">
                    <span><h1 class="grey7 massivefont txtbold mbottom_small inlineblock">Secure Checkout</h1></span>
                    <span class="mleft10 valign-10"><img src="{$urlImgs}lock.png" alt="safebuy" width="32px" height="37px" ></span>
                    {if $signedIn}
                        <h6 class="grey6 normalfont txtnormal">Select payment method and enter your payment details</h6>
                    {else}
                        <h6 class="grey6 normalfont txtnormal">Create account or login, select payment method and enter your payment details</h6>
                    {/if}
                </div>
                {*<h6 style="color: #999; font-weight: normal; float: left; margin-left: 10px;color: #555; ">You will be prompted to enter your company name after payment.</h6>*}
            {else}
                <div class="fleft mleft30 width450">
                    <div>
                    <span><h1 class="grey7 massivefont txtbold mbottom_small inlineblock">Secure Checkout</h1></span> 
                    <span class="mleft10 valign-10"><img src="{$urlImgs}lock.png" alt="safebuy" width="32px" height="37px" ></span>
                    </div>
                    <div>                   
                    {if $signedIn}
                        <h6 class="grey6 normalfont txtnormal">Select payment method and enter your payment details</h6>
                    {else}
                        <h6 class="grey6 normalfont txtnormal">Create account or login, select payment method and enter your payment details</h6>
                    {/if}
                    </div>
                </div>
                {if $basket->hasPackageInBasket() != NULL}
                    <div class="fright txtright pright5 mbottom">
                        <h1 class="massivefont orange2 txtbold">{$companyName}</h1>
                    </div>
                 {/if}
            {/if}
            
            <div class="clear"></div>
        </div>
    {/if}