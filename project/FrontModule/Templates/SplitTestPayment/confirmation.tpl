{include file="@header.tpl"}


<div id="maincontent2">
	{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
    <div class="progressbar step4"></div>
    
    {if isset($hasPackageIncluded) && $hasPackageIncluded}
    <div class="box-blue">
    	<div class="box-blue-bottom">
    		<p style="padding:0;font-size: 14px;"><b>IMPORTANT:</b> Your company is <b>NOT</b> yet registered. Please continue to complete your registration.</p>
    	</div>
    </div>
    {/if}
    
    <h3>Summary</h3>
    
    {* BASKET SUMMARY *}
    <div class="box-lrgwhite">
        <div class="box-lrgwhite-top">
            <div class="box-lrgwhite-bottom">        
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="formtable mnone mbottom">
					<tr>
						<th width="360">Product</th>
						<th width="125" align="center" valign="top">Quantity</th>
						<th width="125">Price</th>
					</tr>

					{foreach from=$basket->items key="key" item="item"}
					<tr>
						<td>{$item->getLngTitle()}</td>
						<td align="center" valign="top">{$item->qty}</td>
						<td>&pound;{$item->totalPrice}</td>
					</tr>
					{/foreach}

					<tr class="bb">
						<td><strong>Subtotal</strong></td>
						<td>&nbsp;</td>
						<td><strong>&pound;{$basket->getPrice('subTotal')}</strong></td>
					</tr>
					{* SHOW VOUCHER *}
					{if $basket->voucher}
						<tr>
							<td class="voucher">Voucher {$basket->voucher->voucherCode}</td>
							<td>&nbsp;</td>
							{assign var="subTotal" value=$basket->getPrice('subTotal')}
							<td style="color: red;">-&pound;{$basket->voucher->discount($subTotal)}</td>
						</tr>
					{/if}
					<tr class="">
						<td>VAT</td>
						<td>&nbsp;</td>
						<td>&pound;{$basket->getPrice('vat')}</td>
					</tr>
					<tr class="">
						<td><strong>Total including delivery charges and tax</strong></td>
						<td>&nbsp;</td>
						<td><strong>&pound;{$basket->getPrice('total')}</strong></td>
					</tr>
				</table>
				
                <div class="clear"></div>
            </div>
        </div>
    </div>
    
    {* NAVIGATION *}
    <a href="#" class="mleft" onclick="window.print(); return false;">Print Summary</a>
    {if isset($hasPackageIncluded, $companyId) && $hasPackageIncluded && $companyId}
        <a href="{$this->router->link("RegusControler::VIRTUAL_OFFICE_REGUS", "company_id=$companyId")}" class="btn_more mtop fright clear">Continue</a>
    {else}
        <a href="{url route="incorporation_submitted"}" class="btn_more mtop fright clear">Continue</a>
    {/if}
    <div class="clear"></div>
	
	{* CERTIFICATE UPGRADE *}
	{if  isset($hasBronzePackage)}
		<div style="padding: 8px 16px 16px; border: 1px solid rgb(204, 204, 204); margin: 24px 0pt 0pt 2px;">
			<p style="background: url('/project/upload/imgs/img228.jpg') no-repeat scroll 0pt 0pt transparent; padding: 10px 0pt 11px 50px; font-size: 14px; font-weight: bold;">Exclusive Certificate Upgrade Offer - £4.99</p>
			<p>The Bronze package you purchased does <u>not</u> include the printed certificate of incorporation or share certificates. <strong>These documents are usually required by banks to open an account</strong>, so we are offering you an exclusive offer for just £4.99 (regularly priced at £20). <a href="/certificate-upgrade-offer.html" onclick="_gaq.push(['_trackEvent', 'Links', 'Click', 'Certificate upgrade from purchase confirmation page']);">More Info</a></p>
		</div>
	{/if}

</div>


<!-- Google Code for CMS Purchase Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1071747309;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "3wJUCIKl9wEQ7aGG_wM";
var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1071747309/?label=3wJUCIKl9wEQ7aGG_wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

	<!--  OLD HTTP CODE
		<!-- Google Code for Purchase Confirmation Page Conversion Page
		<!-- Jared's shit code, but I LOVE HIS MOM - HA HA HA
		<script type="text/javascript">
		<!--
		var google_conversion_id = 1071747309;
		var google_conversion_language = "en";
		var google_conversion_format = "1";
		var google_conversion_color = "ffffff";
		var google_conversion_label = "purchase";
		var google_conversion_value = 0;
		//
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1071747309/?label=purchase&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
	-->

<!-- mrkvosoft -->
<script>
 microsoft_adcenterconversion_domainid = 482591;
 microsoft_adcenterconversion_cp = 5050;
</script>
<script src="//0.r.msn.com/scripts/microsoft_adcenterconversion.js"></script>
<noscript><img width=1 height=1 src="//482591.r.msn.com/?type=1&cp=1"/></noscript>


{include file="@footer.tpl"}
