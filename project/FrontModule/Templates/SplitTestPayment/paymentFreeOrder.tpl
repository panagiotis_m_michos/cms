{include file="@header.tpl"}

{literal}
    <style type="text/css">
        <!--
        div#maincontent2 {
            float: left;
            width: 730px;
            padding-left:10px;
        }
        .new-payment-setps{
            margin-bottom: 10px;
        }
        -->
    </style>
{/literal}

{* TITLE *}
{include file="SplitTestPayment/@paymentBlocks/title.tpl" visibleTitle = $visibleTitle companyName = $companyName customer = $customer} 

{* RIGHT COLUMN *}
{include file="SplitTestPayment/@paymentBlocks/rightColumn.tpl"} 

<div id="maincontent2">
    <div class="new-payment-setps">
        <img src="{$urlImgs}progress_bar_buy.png" alt="new payment steps" width="730" height="44"/>
    </div>
    {* MESSAGES *}
    {include file="SplitTestPayment/@paymentBlocks/messages.tpl" node=$node} 

    <div {if $customer->isNew()} class="payment-page" {/if}>

        {* PAYMENT LOGIN FORM*}
        {if isset($formLogin) && $formLogin}
            {include file="SplitTestPayment/@paymentBlocks/paymentLoginForm.tpl" formLogin = $formLogin} 
        {/if}    

        {$form->getBegin() nofilter}

        {if $form->getErrors()|@count gt 0}
            <p class="ff_err_notice ff_red_err" style="width: 710px">Form has <b> {$form->getErrors()|@count}</b> error(s). See below for more details:</p>
        {/if} 


        {* NEW CUSTOMER EMAIL *}
        {if isset($formLogin) && $formLogin}
            {include file="SplitTestPayment/@paymentBlocks/sageFormEmailFieldset.tpl" form = $form} 
        {/if}                        


        <fieldset id="fieldset_1">
            <legend>Details</legend>
            <div id="freeOrder">
                <h3>Your order today is free, please tick the terms and conditions and click submit to confirm your order.</h3>
            </div>
        </fieldset>

        {include file="SplitTestPayment/@paymentBlocks/sageSubmitButton.tpl" form = $form basket = $basket type = 'submit-on-account'}    
        
        {$form->getEnd() nofilter}
    </div>
</div>        
{include file="@footer.tpl"}
