<html>
    <head>
        <script type="text/javascript" src="{$urlComponent}jquery/dist/jquery.min.js"></script>
        <script type="text/javascript">
            function show_error(context, error, submitBlock, authFormBlock, flashMessageElement) {
                var errorElem = '<div class="flash error">' + error + '</div>';
                $(flashMessageElement, context).prepend(errorElem);
                $(authFormBlock, context).hide();
                $(submitBlock, context).show();
                $(context).scrollTop(1);
            }
            function redirect(context, location) {
                context.location = location;
            }
        </script>
    </head>
    <body>
        {if !empty($success)}
            <div style="font-family:Arial;font-size:14px">
                <h3>3D authentication successful</h3>
                <p>Redirecting you to the manage card page ...</p>
                <p>If you are not redirected automatically please press <a target="_parent" href="{$this->router->link('ManagePaymentControler::MANAGE_PAYMENT_PAGE')}">continue</a></p>
            </div>
        {elseif !empty($error)}
            <div style="font-family:Arial;font-size:14px">
                <h3>3D authentication failed. Please specify the correct password</h3>
                <div>{$error}</div>
                <p>Please press <a target="_parent" href="{$this->router->link('ManagePaymentControler::MANAGE_PAYMENT_PAGE')}">continue</a> to re-enter the process</p>
            </div>
        {/if}
        <script type="text/javascript">
            var error = {if !empty($error)}"{$error}"{else}false{/if};
            if (error) {
                show_error(window.parent.document, error, '#submit-block', '#auth-form-block', '#content');
            } else {
                redirect(window.parent, "{$this->router->link('ManagePaymentControler::MANAGE_PAYMENT_PAGE')}");
            }
        </script>
    </body>
</html>


