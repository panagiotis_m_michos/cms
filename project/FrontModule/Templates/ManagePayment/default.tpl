{include file="@header.tpl"}

<div id="maincontent" style="padding-left: 200px;">
    <div>
        <h1 class="grey12 extrafont txtnormal">Payment Methods</h1>
    </div>

{if isset($form)}
    <h2 class="grey12 bigfont txtnormal">Add new Credit &amp; Debit cards</h2>
    {if $form->hasErrors()}<p class="bg-warning">{$form->getDefaultErrorMessage() nofilter}</p>{/if}
    {$form->getBegin() nofilter}
    <div class="dropdown-toggle fsetcard2" data-toggle="card-details">
        <span>Add New Card</span>
        <span class="fright"><a title="Add New Card" href="javascript:;" class="fa fa-chevron-down"></a></span>
    </div>
    <div id="card-details" class="{if $form->hasErrors() || !empty($showAuthenticationForm) || !empty($error)}show-on-error{else}paulo-js-hidden{/if}">
        <fieldset>
            <legend>1. Card Information</legend>
            <div class="form-group{if $form->hasError('cardType')} has-error{/if}">
                {$form->getLabel('cardType') nofilter}
                {$form->getControl('cardType') nofilter}
                <span class="help-block">{$form->getError('cardType')}</span>
            </div>
            <div class="form-group{if $form->hasError('cardHolder')} has-error{/if}">
                {$form->getLabel('cardHolder') nofilter}
                {$form->getControl('cardHolder') nofilter}
                <span class="help-block">{$form->getError('cardHolder')}</span>
            </div>
            <div class="form-group{if $form->hasError('cardNumber')} has-error{/if}">
                {$form->getLabel('cardNumber') nofilter}
                {$form->getControl('cardNumber') nofilter}
                <span class="help-block">{$form->getError('cardNumber')}</span>
            </div>
            <div class="card-optional form-group{if $form->hasError('issueNumber')} has-error{/if}">
                {$form->getLabel('issueNumber') nofilter}
                {$form->getControl('issueNumber') nofilter}
                <span class="help-block">{$form->getError('issueNumber')}</span>
            </div>
            <div class="card-optional form-group{if $form->hasError('validFrom')} has-error{/if}">
                {$form->getLabel('validFrom') nofilter}
                {$form->getControl('validFrom') nofilter}
                <span class="help-block">{$form->getError('validFrom')}</span>
            </div>
            <div class="form-group{if $form->hasError('expiryDate')} has-error{/if}">
                {$form->getLabel('expiryDate') nofilter}
                {$form->getControl('expiryDate') nofilter}
                <span class="help-block">{$form->getError('expiryDate')}</span>
            </div>
            <div class="form-group{if $form->hasError('CV2')} has-error{/if}">
                {$form->getLabel('CV2') nofilter}
                {$form->getControl('CV2') nofilter}
                <div class="help-button2 help-button-cvv">
                    <a href="#" class="help-icon">help</a>
                    <em>
                            <span id="general-cvv-hint">
                                The security code is a three-digit code printed on the back of your card.
                                <img src="{$urlImgs}CVC2SampleVisaNew.png" />
                            </span>

                            <span id="amex-cvv-hint">
                                The security code is a four-digit code printed on the front of your card.
                                <img src="{$urlImgs}csc_amex.jpg" />
                            </span>
                    </em>
                </div>
                <span class="help-block">{$form->getError('CV2')}</span>
            </div>
        </fieldset>
        <fieldset>
            <legend>2. Billing address</legend>
            <div class="form-group{if $form->hasError('firstName')} has-error{/if}">
                {$form->getLabel('firstName') nofilter}
                {$form->getControl('firstName') nofilter}
                <span class="help-block">{$form->getError('firstName')}</span>
            </div>
            <div class="form-group{if $form->hasError('lastName')} has-error{/if}">
                {$form->getLabel('lastName') nofilter}
                {$form->getControl('lastName') nofilter}
                <span class="help-block">{$form->getError('lastName')}</span>
            </div>
            <div class="form-group{if $form->hasError('address1')} has-error{/if}">
                {$form->getLabel('address1') nofilter}
                {$form->getControl('address1') nofilter}
                <span class="help-block">{$form->getError('address1')}</span>
            </div>
            <div>
                {$form->getLabel('address2') nofilter}
                {$form->getControl('address2') nofilter}
                <span class="help-block">{$form->getError('address2')}</span>
            </div>
            <div class="form-group{if $form->hasError('city')} has-error{/if}">
                {$form->getLabel('city') nofilter}
                {$form->getControl('city') nofilter}
                <span class="help-block">{$form->getError('city')}</span>
            </div>
            <div class="form-group{if $form->hasError('postcode')} has-error{/if}">
                {$form->getLabel('postcode') nofilter}
                {$form->getControl('postcode') nofilter}
                <span class="help-block">{$form->getError('postcode')}</span>
            </div>
            <div class="state-optional form-group{if $form->hasError('billingState')} has-error{/if}">
                {$form->getLabel('billingState') nofilter}
                {$form->getControl('billingState') nofilter}
                <span class="help-block">{$form->getError('billingState')}</span>
            </div>
            <div class="form-group{if $form->hasError('country')} has-error{/if}">
                {$form->getLabel('country') nofilter}
                {$form->getControl('country') nofilter}
                <span class="help-block">{$form->getError('country')}</span>
            </div>
        </fieldset>
        {if !empty($showAuthenticationForm)}
            <div id="auth-form-block">
                <a name="authFormBlock" href="#" style="visibility:hidden"></a>
                <h3 class="">Please complete the 3D authentication process below</h3>
                <strong style="margin-bottom: 5px;display: block">This step is only for authentication, no payment will be taken from your account</strong>
                <iframe name="sage-iframe" src="{$this->router->link('ManagePaymentControler::MANAGE_PAYMENT_AUTHENTICATION_FORM')}" width="100%" height="450"></iframe>
            </div>
        {/if}
        <div id="submit-block" class="txtright" {if !empty($showAuthenticationForm)}style="display:none"{/if}>
            <span class="inlineblock midfont m10"><a class="cancel-select" href="javascript:;">Cancel</a></span>
            {$form->getControl('submit') nofilter}
        </div>
    </div>
    {$form->getEnd() nofilter}
{elseif isset($token)}
    <h2 class="grey12 bigfont txtnormal">Your Credit &amp; Debit cards</h2>
    <div></div>
    <div id="fsetcard1" class="dropdown-toggle midfont" data-toggle="address-info">
        <span class="card32 {$token->getCardType()|lower}"></span>
        <span class="width330">{$token->getCardTypeText()} ending in {$token->getCardNumber()}</span>
        <span class="width150">Exp. {$token->getCardExpiryDate()|datetime:'m/Y'}</span>
        <span class="width20"><a title="Show card details" href="javascript:;" class="fa fa-chevron-down"></a></span>
    </div>
    <div id="address-info" class="paulo-js-hidden midfont posabsolute">
        <div class="col-md-5 inlineblock valign-top">
            <h4>Name on card</h4>
            <p>{$token->getCardHolder()}</p>
        </div>
        <div class="col-md-5 inlineblock valign-top">
            <h4>Billing address</h4>
            <p>{$token->getBillingAddress()|escape|replace:"\n":'<br/>' nofilter}</p>
        </div>
        <div class="col-md-10 inlineblock"></div>
        <div class="fright mtop30 inlineblock">
            <form action="{$this->router->link(ManagePaymentControler::DELETE_CARD_PAGE)}" method="post">
                <input type="hidden" name="tokenId" value="{$token->getId()}" />
                <input id="delete-token" type="submit" class="redbutton" value="Delete" data-toggle="warning-dialog" />
            </form>
        </div>
        <div id="warning-dialog" class="overlay small-overlay">
            <h2>WARNING!</h2>
            <p>Deleting your card details will stop your recurring payments.<br />
                Your service will stop working if payments are not kept up to date.</p>
            <p>Are you sure you want to delete this card?</p>
            <div class="mtop mbottom">
                <div class="card32 inlineblock valign-middle {$token->getCardType()|lower}"></div>
                <span class="width330 valign-middle">{$token->getCardTypeText()} ending in {$token->getCardNumber()}</span>
            </div>
            <div class="fright">
                <form id="delete-token-form" action="{$this->router->link(ManagePaymentControler::DELETE_CARD_PAGE)}" method="post">
                    <input type="hidden" name="tokenId" value="{$token->getId()}" />
                    <span class="inlineblock m10"><a class="cancel-additional" href="javascript:;">Cancel</a></span>
                    <span class="inlineblock"><input type="submit" class="redbutton" value="Delete card" /></span>
                </form>
            </div>
        </div>
    </div>
{/if}
</div>

{literal}
<script type="text/javascript">
    cms.form.selectOption('#cardType', {'MAESTRO':1}, 'div.card-optional', 'hidden');
    cms.form.selectOption('#country', {'US':1}, 'div.state-optional', 'hidden');
    cms.form.largeSelect('div.dropdown-toggle', 'a.cancel-select');
    cms.popup.overlay('#delete-token', 'a.cancel-additional');
</script>
{/literal}

{include file="@footer.tpl"}
