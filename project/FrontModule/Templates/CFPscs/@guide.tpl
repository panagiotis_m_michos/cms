<div class="box1 fright">	
    <h3>Guide:</h3>
    <div class="box1-foot pbottom15">
        <p>UK companies and limited liability partnerships (LLPs) are required to identify and record the people who own or control their company. Formally, these are the 'People with Significant Control (PSCs)' and Companies and LLPs need to keep a PSC register.</p>

        <p>The PSC register will help to increase transparency over who owns and controls UK companies, especially for those that have many layers of ownership.</p>
        
        <p>A PSC is anyone in a company who:</p>
        <ul>
            {if $companyType == 'LLP'}
                <li>owns more than 25% of the company’s surplus assets</li>
            {else if $companyType == 'BYSHR'}
                <li>owns more than 25% of the company’s shares</li>
            {/if}
            <li>holds more than 25% of the company’s voting rights</li>
            <li>holds the right to appoint or remove the majority of directors</li>
            <li>has the right to, or actually exercises significant influence or control</li>
            <li>holds the right to exercise or actually exercises significant control over a trust or company that meets one of the first 4 conditions.</li>
        </ul>
        <p><a href="https://www.gov.uk/government/news/the-small-business-enterprise-and-employment-bill-is-coming#psc-info" target="_blank">Learn more about PSCs <i class="fa fa-external-link" aria-hidden="true"></i></a></p>
        <p><a href="https://www.youtube.com/watch?v=6bpdpX3ceQ8&feature=youtu.be" target="_blank">Watch: How to identify PSCs <i class="fa fa-external-link" aria-hidden="true"></i></a></p>
        {if $companyType == 'BYGUAR'}
            <p><Strong>In most cases the PSCs are the Members, as the most common criteria for a PSC in a company limited by guarantee is "holds more than 25% of the company’s voting rights".</strong></p>
        {else if $companyType == 'BYSHR'}
            <p><Strong>In most cases the PSCs are the shareholders, as the most common criteria for a PSC is "owns more than 25% of the company’s shares".</strong></p>
        {/if}
    </div>
</div>
