{include file="@header.tpl"}
<div class="formprocesstitle">
    {if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
</div>

{include file="@blocks/navlist.tpl" currentTab=$this->action}

<div>
    <div style="float: left">

        {if isset($showPreAddedInfo) && $showPreAddedInfo}
            <div class="alert alert-warning" style="background-color: #FFD; border-radius: 5px; padding: 15px; margin-bottom: 15px; width: 681px">
                According to your company information we have selected the People with Significant Control that match what is required by the legislation.<br>
                Please check the information below and amend if necessary.
            </div>
        {/if}
        <div style="background-color: rgb(203, 229, 255); border-radius: 5px; padding: 15px; margin-bottom: 15px; width: 681px">
            <p class="hugefont">People with Significant Control Summary</p>
            {if $summaryView->hasNoPscReason()}
                <p><i>{$summaryView->getNoPscReason()}</i></p>
            {else}
                <p>Below is the list of PSCs you have added so far:</p>
                <br>

                {* PERSONS *}
                <p class="hugefont">Persons</p>

                {if !empty($summaryView->getPersonPscs())}
                    <table class="formtable mnone" width="550">
                        <col>
                        <col width="60">
                        <col width="70">
                        <tr>
                            <th>Full name</th>
                            <th class="center" colspan="2">Action</th>
                        </tr>
                        {foreach from=$summaryView->getPersonPscs() key="key" item="person"}
                        <tr>
                            <td>{$key+1}. {$person->getFullName()}</td>
                            <td class="center"><a href="{$person->getEditLink()}">Edit</a></td>
                            <td class="center"><a href="{$person->getDeleteLink()}" onclick="return confirm('Are you sure?');">Delete</a></td>
                        </tr>
                        {/foreach}
                    </table>
                {else}
                    <p>No persons yet.</p>
                {/if}

                <br>

                {* CORPORATES *}
                <p class="hugefont">Corporates</p>

                {if !empty($summaryView->getCorporatePscs())}
                    <table class="formtable mnone" width="550">
                        <col>
                        <col width="60">
                        <col width="70">
                        <tr>
                            <th>Full name</th>
                            <th class="center" colspan="2">Action</th>
                        </tr>
                        {foreach from=$summaryView->getCorporatePscs() key="key" item="corporate"}
                            <tr>
                                <td>{$key+1}. {$corporate->getFullName()}</td>
                                <td class="center"><a href="{$corporate->getEditLink()}">Edit</a></td>
                                <td class="center"><a href="{$corporate->getDeleteLink()}" onclick="return confirm('Are you sure?');">Delete</a></td>
                            </tr>
                        {/foreach}
                    </table>
                {else}
                    <p>No corporates yet.</p>
                {/if}
            {/if}
        </div>

        <div style="background-color: #ffd1b2; border-radius: 5px; padding: 15px; width: 681px;">
            <p class="hugefont noptop">What do you want to do next?</p>

            <table width="100%">
                <col width="33%">
                <col width="34%">
                <col width="33%">
                <tr>
                    <td><a class="btn_back2 mtop fleft clear" href="{url route="CFShareholdersControler::SHAREHOLDERS_PAGE" company_id=$companyId}">Back</a></td>
                    <td><a href="{url route="CFPscsControler::ADD_PSC_PERSON_PAGE" company_id=$companyId}">Add another PSC</a></td>
                    <td align="right">{$continue nofilter}</td>
                </tr>
            </table>
        </div>
    </div>
    <div style="float: right; width: 260px;">

        {* GUIDE *}
        {include file="CFPscs/@guide.tpl"}
        
        <p>
            <a data-toggle="modal" data-target="#no-psc-modal" href="#">Click here if you believe that there is no registrable person or you company is under a different situation</a>
        </p>

        <div class="clear"></div>

        {* HTML INFO BOX *}
        {include file="@blocks/CFHtmlBox.tpl"}
    </div>
    <div class="clear"></div>
</div>

<div class="modal fade" id="no-psc-modal" role="dialog">
    <div class="modal-dialog">
        {$noPscsForm->getBegin() nofilter}
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">No registrable person official wording</h4>
            </div>
            <div class="modal-body">
                <p>
                    The PSC requirements apply whether your company has a PSC or not. If you have taken all reasonable
                    steps and are confident that there are no individuals or legal entities which meet any of the
                    conditions in relation to your company, please select the checkbox below:
                </p>
                {$noPscsForm->getControl('noPscReason') nofilter}
                {$noPscsForm->getLabel('noPscReason') nofilter}
                <p>
                    Check the Guidance for Companies, Societates Europaeae and Limited Liability Partnerships for
                    further information.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {$noPscsForm->getControl('save') nofilter}
            </div>
        </div>
        {$noPscsForm->getEnd() nofilter}
    </div>
</div>

{include file="@footer.tpl"}
