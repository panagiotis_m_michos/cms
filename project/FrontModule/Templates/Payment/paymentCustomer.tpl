{include file="@header.tpl"}

{literal}
    <style type="text/css">
        <!--
        div#maincontent2 {
            float: left;
            width: 730px;
            padding-left:10px;
        }
        -->

        .sage-disabled { display: none;}
    </style>
{/literal}

{* RIGHT COLUMN *}
{include file="Payment/@paymentBlocks/rightColumn.tpl"}

<div id="maincontent2">

    <script type="text/javascript">
        $(document).ready(function(){
            {if $node->getDisableSage()}
                if ($('.on-account-payment').is(":checked") ) {
                    $(".submit-on-account").show();
                    $(".sage-disabled-paypal").hide();
                } else {
                    $(".submit-on-account").hide();
                    $(".sage-disabled-paypal").show();
                }
                $('.on-account-payment').click(function(){
                    if ($(this).is(":checked")) {
                        $(".submit-on-account").show();
                        $(".sage-disabled-paypal").hide();
                    }
                    else
                    {
                        $(".submit-on-account").hide();
                        $(".sage-disabled-paypal").show();
                    }
                });
            {/if}
        });
    </script>

    {* MESSAGES *}
    {include file="Payment/@paymentBlocks/messages.tpl" node=$node}

    {* TITLE *}
    {include file="Payment/@paymentBlocks/title.tpl" visibleTitle = $visibleTitle companyName = $companyName}

    <div class="payment-page-customer">
    {* PAYMENT ERROR *}
    {if isset($error)}<p>{$error}</p>{/if}

    {$form->getBegin() nofilter}
    {if $form->getErrors()|@count gt 0}
        <p class="ff_err_notice ff_red_err" style="width: 710px">Form has <b> {$form->getErrors()|@count}</b> error(s). See below for more details:</p>
    {/if}

    {if $node->isOkToShowOnAccountPayment($customer,$basket)}
        <fieldset id="fieldset_0" class="payment-page-fieldset" style="margin-bottom:10px;">
            <legend class="payment-page-fieldset-legend">On Account</legend>
            <table class="ff_table">
                <tbody>
                    <tr>
                        <th>Available credit</th>
                        <td>&pound;{$customer->credit}</td>
                    </tr>
                    <tr>
                        <th>{$form->getLabel('credit') nofilter}</th>
                        {if !$node->isAbleToUseCredits($customer, $basket)}
                            <td><input type="checkbox" id="credit" name="credit" disabled="disabled" /></td>
                            <td>
                                <span class="redmsg">To be able to use your credits the remaining amount to be paid by card/paypal must be over &pound;1</span>
                            </td>
                        {else}
                            <td>{$form->getControl('credit') nofilter}</td>
                            <td>
                                <span id="credit-error" class="displaynone redmsg">
                                    The available credit on your account is insufficient for the total amount, insert your card details below so the remaining amount can be deducted from your card.
                                </span>
                                <span class="redmsg">{$form->getError('credit')}</span>
                            </td>
                        {/if}
                    </tr>
                </tbody>
            </table>
        </fieldset>
    {/if}

    {* FORM DETAILS*}
    {include file="Payment/@paymentBlocks/sageFormBodyFieldset.tpl" form = $form node = $node}

    <fieldset id="fieldset_2" style="margin-bottom:10px;" class="payment-page-fieldset">
        <legend class="payment-page-fieldset-legend">Terms and Conditions</legend>
        <table class="ff_table">
            <tbody>
                <tr>
                    <th><a href="/general-terms-conditions.html" target="_blank">Terms and Conditions</a>
                        <br /><span style="font-size: 11px; color: #333333;">(Opens in new window)</span></th>
                    <td>{$form->getControl('terms') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('terms')}</span></td>
                </tr>
            </tbody>
        </table>
    </fieldset>

    {if !empty($showAuth)}
        <div id="authFormBlock">
            <fieldset id="fieldset_3"class="payment-page-fieldset">
                <legend class="payment-page-fieldset-legend">3D authentication</legend>
                <a name="authFormBlock" href="#" style="visibility:hidden"></a>
                <h3 class="mnone pbottom10">Please complete the 3D authentication process below</h3>
                <iframe name="sage-iframe" src="{$authFormUrl}" width="100%" height="450"></iframe>
            </fieldset>
        </div>
    {/if}

    <div id="submitBlock" {if !empty($showAuth)} style="display:none" {/if}>

        <fieldset id="fieldset_3" class="payment-page-fieldset submit-on-account {if $node->getDisableSage()} submit-on-account{/if}">
            <legend class="payment-page-fieldset-legend">Summary</legend>
            <div id="summarybasket">
            <div class="mright8 fleft txtright">
                <div>Subtotal</div>
                <div>VAT</div>
                <div class="credit-price{if !$basket->isUsingCredit()} hidden{/if}">Credit</div>
                <div class="mtop"><strong>Total</strong></div>
            </div>
            <div class="fleft">
                <div class="price-subtotal" data-renewal-selector="subtotal">&pound;&nbsp;{$basket->getPrice('subTotal2')}</div>
                <div class="price-vat" data-renewal-selector="vat">&pound;&nbsp;{$basket->getPrice('vat')}</div>
                <div id="credit-price" class="credit-price{if !$basket->isUsingCredit()} hidden{/if}">-&pound;&nbsp;{$basket->getPrice('account')}</div>
                <div class="mtop price-total" data-renewal-selector="total"><strong>&pound;&nbsp;{$basket->getPrice('total')}</strong></div>
            </div>
            </div>
            <div class="m20">
                {ui name="button" loading="expand-right" text="Pay using card" idAttr="submit"}
            </div>
        </fieldset>

{$form->getEnd() nofilter}

<p class="required"><span class="orange">*</span>Required Fields</p>

    </div>

</div>

</div>

{include file="@footer.tpl"}
