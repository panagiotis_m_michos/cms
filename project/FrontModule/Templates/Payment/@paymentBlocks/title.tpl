{if $visibleTitle}
        <div style="margin-top: 25px;margin-bottom: 10px;">
            {if empty($companyName) or $companyName|h == "CHANGE YOUR COMPANY NAME LTD"}
                <div>
                    <h1>Payment</h1>
                </div>
                {*<h6 style="color: #999; font-weight: normal; float: left; margin-left: 10px;color: #555; ">You will be prompted to enter your company name after payment.</h6>*}
            {else}
                <div style="float: left;">
                    <h1>Payment</h1>
                </div>
                {if $basket->hasPackageInBasket() != NULL}
                    <div style="float: right; text-align: right;padding-right: 5px;margin-bottom: 10px;">
                        <h3>{$companyName}</h3>
                        <h6 style="color: #999; margin-top: -10px; font-weight: normal;">Subject to approval by Companies House</h6>
                    </div>
                 {/if}
            {/if}
            
            <div class="clear"></div>
        </div>
    {/if}