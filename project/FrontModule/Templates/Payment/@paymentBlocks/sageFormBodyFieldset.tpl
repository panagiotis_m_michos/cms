<style>
    #amex-cvv-hint {
        display: none;
    }

    #amex-cvv-hint > img {
        width: 100%;
    }
</style>

<fieldset id="paymentMethods" style="margin-bottom:10px;" class="payment-page-fieldset">
    <legend class="payment-page-fieldset-legend">Payment Method</legend>
    {$form->getControl('selectedMethod') nofilter}
    
    {* Payment methods combobox*}
    {include file="Payment/@paymentBlocks/paymentMethodsCombo.tpl"} 
</fieldset>

<fieldset id="fieldset_1" style="margin-bottom:10px;" class="payment-page-fieldset">
    <legend class="payment-page-fieldset-legend">Payment Details</legend>
    <table class="ff_table">
        <tbody>
            <!--
            <tr>
                <th>{$form->getLabel('amount') nofilter} <span style="margin-left:5px;" class="pound-style">&pound;</span></th>
                <td >{$form->getControl('amount') nofilter}</td>
                <td></td>
            </tr>
            -->
            {if !$node->getDisableSage()}  
                <tr>
                    <th>{$form->getLabel('cardholder') nofilter}</th>
                    <td>{$form->getControl('cardholder') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('cardholder')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('cardType') nofilter}</th>
                    <td>{$form->getControl('cardType') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('cardType')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('cardNumber') nofilter}</th>
                    <td>{$form->getControl('cardNumber') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('cardNumber')}</span></td>
                </tr>
            <noscript>
            <tr>
                <th>{$form->getLabel('issueNumber') nofilter}</th>
                <td>{$form->getControl('issueNumber') nofilter}</td>
                <td>The issue number MUST be entered EXACTLY as it appears on the card. e.g. some cards have Issue Number "4", others have "04".</td>
            </tr>
            <tr>
                <th>{$form->getLabel('validFrom') nofilter}</th>
                <td>{$form->getControl('validFrom') nofilter}</td>
                <td><span class="redmsg">{$form->getError('validFrom')}</span></td>
            </tr>
            </noscript>                       
            <tr class="issuered" style="display:none;">
                <th>{$form->getLabel('issueNumber') nofilter}</th>
                <td>
                    {$form->getControl('issueNumber') nofilter}
                    <div class="help-button2" style="float: right; margin: 0 15px 0 0;">
                        <a href="#" class="help-icon">help</a>
                        <em>The issue number MUST be entered EXACTLY as it appears on the card. e.g. some cards have Issue Number "4", others have "04".</em>
                    </div>
                </td>
                <td><span class="redmsg">{$form->getError('issueNumber')}</span></td>
            </tr>
            <tr class="issuered" style="display:none;">
                <th>{$form->getLabel('validFrom') nofilter}</th>
                <td>{$form->getControl('validFrom') nofilter}</td>
                <td><span class="redmsg">{$form->getError('validFrom')}</span></td>
            </tr>                
            <tr>
                <th>{$form->getLabel('expiryDate') nofilter}</th>
                <td>{$form->getControl('expiryDate') nofilter}</td>
                <td><span class="redmsg">{$form->getError('expiryDate')}</span></td>
            </tr>
            <tr>
                <th>{$form->getLabel('CV2') nofilter}</th>
                <td>
                    {$form->getControl('CV2') nofilter}
                    <div class="help-button2" style="float: right; margin: 0 15px 0 0;">
                        <a href="#" class="help-icon">help</a>
                        <em>
                            <span id="general-cvv-hint">
                                The security code is a three-digit code printed on the back of your card.
                                <img src="{$urlImgs}CVC2SampleVisaNew.png" />
                            </span>

                            <span id="amex-cvv-hint">
                                The security code is a four-digit code printed on the front of your card.
                                <img src="{$urlImgs}csc_amex.jpg" />
                            </span>
                        </em>
                    </div>
                </td>
                <td><span class="redmsg">{$form->getError('CV2')}</span></td>
            </tr>
            <tr>
                <th>{$form->getLabel('address1') nofilter}</th>
                <td>{$form->getControl('address1') nofilter}</td>
                <td><span class="redmsg">{$form->getError('address1')}</span></td>
            </tr>
            <tr>
                <th>{$form->getLabel('address2') nofilter}</th>
                <td>{$form->getControl('address2') nofilter}</td>
                <td><span class="redmsg">{$form->getError('address2')}</span></td>
            </tr>
            <tr>
                <th>{$form->getLabel('address3') nofilter}</th>
                <td>{$form->getControl('address3') nofilter}</td>
                <td><span class="redmsg">{$form->getError('address3')}</span></td>
            </tr>
            <tr>
                <th>{$form->getLabel('town') nofilter}</th>
                <td>{$form->getControl('town') nofilter}</td>
                <td><span class="redmsg">{$form->getError('town')}</span></td>
            </tr>
            <tr>
                <th>{$form->getLabel('postcode') nofilter}</th>
                <td>{$form->getControl('postcode') nofilter}</td>
                <td><span class="redmsg">{$form->getError('postcode')}</span></td>
            </tr>                
            <tr>
                <th>{$form->getLabel('country') nofilter}</th>
                <td>{$form->getControl('country') nofilter}</td>
                <td><span class="redmsg">{$form->getError('country')}</span></td>
            </tr>
            <noscript>
            <tr>
                <th>{$form->getLabel('billingState') nofilter}</th>
                <td>{$form->getControl('billingState') nofilter}</td>
                <td><span class="redmsg">{$form->getError('billingState')}</span></td>
            </tr>
            </noscript>
            <tr class="state" style="display:none;">
                <th>{$form->getLabel('billingState') nofilter}</th>
                <td>{$form->getControl('billingState') nofilter}</td>
                <td><span class="redmsg">{$form->getError('billingState')}</span></td>
            </tr>
        {/if} 
        </tbody>
    </table>
    <div id="spaintext" style="border: 1px solid #069;padding: 10px;background-color: #cce5ff;line-height: 18px; display: none;">
        Our main payment provider, Sagepay, is currently experiencing issues with some Spanish cards. If you have
        trouble with your payment we suggest selecting <strong>Paypal</strong>, from the Payment Method, above.
    </div>
</fieldset>               
