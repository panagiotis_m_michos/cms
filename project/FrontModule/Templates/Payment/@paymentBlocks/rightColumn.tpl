<div style=" padding-top:40px; float: right; width:230px;text-align: center;">
    <div style="font-size: 12px; padding: 8px; color: #666666;">
        <img style="display: inline-block;" alt="lock" width="29" height="40" src="{$urlImgs}img8.gif">
        <p style="display: inline-block; height: 40px; line-height: 40px; margin: 0 0 0 5px; font-weight: bold; font-size: 14px;">You're safe with us!</p>
        <p style="padding-top: 16px;">Every payment is secured by industry standard 256-bit encryption. Companies Made Simple never stores your card details on our servers.</p>
        <img style="margin-top: 16px;" alt="secured by sagepay" width="150" height="52" src="{$urlImgs}img10.gif">
    </div>
    <div style="margin-top: 16px;">
        <script type="text/javascript" src="https://seal.thawte.com/getthawteseal?host_name=www.companiesmadesimple.com&amp;size=M&amp;lang=en"></script>
    </div>
    <div style="margin-top: 16px;">
		<a href="#" onclick="creditCardSafeLink()">
		    <img src="{$urlImgs}credit-card-safe.gif" alt="SecurityMetrics for PCI Compliance, QSA, IDS, Penetration Testing, Forensics, and Vulnerability Assessment" border="0">
		</a>
    </div>
    <div style="margin-top: 16px;">
		<img src="{$urlImgs}bdg_secured_by_pp_2line.png" alt="PayPal" border="0" style="margin-bottom:20px;">
    </div>
</div>