<fieldset style="clear: both;color: #000; " class="email-form-check payment-page-fieldset">
            <legend class="payment-page-fieldset-legend">New and Returning Customer Details</legend>
            <table class="ff_table">
                <tbody>
                    <tr>
                        <th>
                            {$form->getLabel('emails') nofilter}
                        </th>
                        <td >
                            {$form->getControl('emails') nofilter}
                            <span class="ff_desc dec_new_cust">We use your email to send your company details!</span>
                        </td>
                        <td style="width: 300px;">
                            <span class="show-ckeck-company" style="display: none;float: left;margin-top: -4px;margin-left: 5px;">
                                <span class="isOkToShow" style="display: none; ">
                                    <img src="{$urlImgs}greentick24.png" alt="email is available" style="float: left;"/>
                                    {*<span style="color: #339900; float: left; padding-left: 3px;padding-top: 7px;">Email is valid</span> *}
                                    <div class="clear"></div>
                                </span>
                                <span class="isNotOkToShow error" style="display: none; ">
                                    <img src="{$urlImgs}redcross.png" alt="email isn't available"/>
                                    <div class="clear"></div>
                                </span>
                            </span>     
                            {* <span class="ff_control_err">{$checkEmail->getError('email')}</span> *}
                            <span class="editEmailLink" style="display: none;float: left; padding-left: 10px;padding-top:3px;">
                                <a href="javascript:;" class="editEmailFields" style="color: #006699">Edit</a> 
                                <div class="clear"></div>
                            </span>
                            <div class="clear"></div>
                        </td>
                    </tr>
                    <tr>
                        <th>&nbsp;</th>
                        <td colspan="2">
                            <a href="{$this->router->link("PaymentControler::SAGE_CHECK_NEW_CUSTOMER_PAGE")}" class="check-email-avaiability" style="text-decoration: none; color: #000;">
                                <button type="button" style="cursor: pointer;" onclick="_gaq.push(['_trackEvent', 'Links', 'Click', 'Clicked Create Account or Sign In button on payment page']);">Create Account or Sign In</button>    
                            </a>
                            <span class="payment-email-ok" style="font-weight: bold">Please enter your payment details below:</span>    
                        </td>
                    </tr>
                </tbody>
            </table>
        </fieldset>      
