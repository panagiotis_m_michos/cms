<div class="check-email-login-form">    
    {$formLogin->getBegin() nofilter}
    <fieldset styel="clear: both;" class="payment-page-fieldset">
        <legend class="payment-page-fieldset-legend">Customer Login</legend>
        <table class="ff_table">
            <tbody>
                <tr>
                    <th>{$formLogin->getLabel('email') nofilter}</th>
                    <td>
                        {$formLogin->getControl('email') nofilter}
                    </td>
                    <td>
                        <span class="editEmailLinkLogin" style="display: none;">
                            <a href="javascript:;"  style="color: #006699;padding-left:5px;">Use another email</a> 
                            <div class="clear"></div>
                        </span>
                        <span class="ff_control_err">{$formLogin->getError('email')}</span>
                    </td>

                </tr>
                <tr>
                    <th>{$formLogin->getLabel('password') nofilter}</th>
                    <td>
                        {$formLogin->getControl('password') nofilter}
                        {*<span class="ff_desc">Please provide New Company Name</span>*}
                    </td>
                    <td>
                        <span class="ff_control_err new-err-msg">{$formLogin->getError('password')}</span>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        {$formLogin->getControl('login') nofilter}
                    </td>
                    <td  style="vertical-align: middle;"> 
                        <div class="updateForgottenPassword">
                            <a href="{$this->router->link("PaymentControler::NEW_PASSWORD_PAGE")}" class="forgotpassword-email" style="color: #006699; padding-left: 10px;" >I have forgotten my password</a>
                            <span class="loading-image" style="display: none;">
                                Emailing new password!
                            </span>
                            <span class="newPaswwordSent" style="display: none;">
                                New password sent. <a href="{$this->router->link("PaymentControler::NEW_PASSWORD_PAGE")}" class="forgotpassword-email after-emailing" style="color: #006699;" >Resend password.</a>
                            </span>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>     
    {$formLogin->getEnd() nofilter}
</div>
