<div class="fancy-combo" data-combo="#selectedMethod">
    <div class="selected-value">
        <ul class="selected-content fleft">
        {if !empty($customerToken)}
            <li class="option" data-option="{$customerToken->getId()}">
                <span class="card25 {$customerToken->getCardType()|lower}">&nbsp;</span>
                {$customerToken->getCardTypeText()} ending in {$customerToken->getCardNumber()}
            </li>
        {else}
            <li class="option" data-option="0">
                <span class="card25 generic">&nbsp;</span>
                Pay using Credit/Debit Card
            </li>
        {/if}
        </ul>
        <div class="fright"><a href="javascript:;" class="combo-arrow fa fa-chevron-down">&nbsp;</a></div>
        <div class="clear"></div>
    </div>
    <ul class="combo-options hidden"> 
        <li class="optgroup">
            Credit or Debit Cards
        </li>
        <ul>
            {if !empty($customerToken)}
                <li class="option" data-option="{$customerToken->getId()}">
                    <span class="card25 {$customerToken->getCardType()|lower}" >&nbsp;</span>
                    {$customerToken->getCardTypeText()} ending in {$customerToken->getCardNumber()}
                </li>
                <li class="option" data-option="one_off_card">
                    <span class="card25 generic" >&nbsp;</span>
                    Use a different card
                </li>
            {else}
                <li class="option" data-option="0">
                    <span class="card25 generic" >&nbsp;</span>
                    Pay using Credit/Debit Card
                </li>
            {/if}
        </ul>
        <li class="optgroup">
            Other Payment Methods
        </li>
        <ul>
            <li class="option" data-option="paypal">
                <span class="card25 paypal" >&nbsp;</span>
                Pay with PayPal
            </li>
        </ul>
    </ul>
</div>
