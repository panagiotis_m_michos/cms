{include file="@header.tpl"}

{literal}
    <style type="text/css">
        <!--
        div#maincontent2 {
            float: left;
            width: 730px;
            padding-left:10px;
        }
        -->
    </style>
{/literal}

{* RIGHT COLUMN *}
{include file="Payment/@paymentBlocks/rightColumn.tpl"}

<div id="maincontent2">

    <div class="new-payment-setps">
        <img src="{$urlImgs}progress_bar_buy.png" alt="new payment steps" width="730" height="44"/>
    </div>

    {* MESSAGES *}
    {include file="Payment/@paymentBlocks/messages.tpl" node=$node}

    {* TITLE *}
    {include file="Payment/@paymentBlocks/title.tpl" visibleTitle = $visibleTitle companyName = $companyName}

    <div class="payment-page">

    {* PAYMENT LOGIN FORM*}
    {if isset($formLogin) && $formLogin}
        {include file="Payment/@paymentBlocks/paymentLoginForm.tpl" formLogin = $formLogin}
    {/if}


    <div class="sage-payment-form">
        {$form->getBegin() nofilter}

        {if $form->getErrors()|@count gt 0}
            <p class="ff_err_notice ff_red_err" style="width: 710px">Form has <b> {$form->getErrors()|@count}</b> error(s). See below for more details:</p>
        {/if}

        {* NEW CUSTOMER EMAIL *}
        {if isset($formLogin) && $formLogin}
            {include file="Payment/@paymentBlocks/sageFormEmailFieldset.tpl" form = $form}
        {/if}

        {* FORM DETAILS*}
        {include file="Payment/@paymentBlocks/sageFormBodyFieldset.tpl" form = $form node = $node}



        <fieldset id="fieldset_2" style="margin-bottom:10px;" class="payment-page-fieldset">
            <legend class="payment-page-fieldset-legend">Terms and Conditions</legend>
            <table class="ff_table">
                <tbody>
                    <tr>
                        <th><a href="/general-terms-conditions.html" target="_blank">Terms and Conditions</a>
                            <br /><span style="font-size: 11px; color: #333333;">(Opens in new window)</span></th>
                        <td>{$form->getControl('terms') nofilter}</td>
                        <td><span class="redmsg">{$form->getError('terms')}</span></td>
                    </tr>
                </tr>
                </tbody>
            </table>
        </fieldset>

        {if !empty($showAuth)}
            <div id="authFormBlock">
                <fieldset id="fieldset_3" class="payment-page-fieldset">
                    <legend class="payment-page-fieldset-legend">3D authentication</legend>
                    <a name="authFormBlock" href="#" style="visibility:hidden"></a>
                    <h3 class="mnone pbottom10">Please complete the 3D authentication process below</h3>
                    <iframe name="sage-iframe" src="{$authFormUrl}" width="100%" height="450"></iframe>
                </fieldset>
            </div>
        {/if}

        <div id="submitBlock" {if !empty($showAuth)} style="display:none" {/if}>
            {if !$node->getDisableSage()}
            <fieldset id="fieldset_3" class="payment-page-fieldset">
                <legend class="payment-page-fieldset-legend">Summary</legend>
                <div id="summarybasket">
                    <div class="mright8 fleft txtright">
                        <div>Subtotal</div>
                        <div>VAT</div>
                        <div class="mtop"><strong>Total</strong></div>
                    </div>
                    <div class="fleft">
                        <div class="price-subtotal" data-renewal-selector="subtotal">&pound;&nbsp;{$basket->getPrice('subTotal2')}</div>
                        <div class="price-vat" data-renewal-selector="vat">&pound;&nbsp;{$basket->getPrice('vat')}</div>
                        <div class="mtop price-total" data-renewal-selector="total"><strong>&pound;&nbsp;{$basket->getPrice('total')}</strong></div>
                    </div>
                </div>
            <div class="m20">
                {ui name="button" loading="expand-right" text="Pay using card" idAttr="submit"}
            </div>
            </fieldset>
            {/if}
        </div>
        {$form->getEnd() nofilter}
        <p class="required"><span class="orange">*</span>Required Fields</p>
        </div>
    </div>
</div>
{include file="@footer.tpl"}
