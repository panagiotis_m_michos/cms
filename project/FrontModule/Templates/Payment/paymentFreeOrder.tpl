{include file="@header.tpl"}

{literal}
    <style type="text/css">
        <!--
        div#maincontent2 {
            float: left;
            width: 730px;
            padding-left:10px;
        }
        -->
    </style>
{/literal}


{* RIGHT COLUMN *}
{include file="Payment/@paymentBlocks/rightColumn.tpl"} 


<div id="maincontent2">
    
    {* MESSAGES *}
    {include file="Payment/@paymentBlocks/messages.tpl" node=$node} 
    
    {* TITLE *}
    {include file="Payment/@paymentBlocks/title.tpl" visibleTitle = $visibleTitle companyName = $companyName} 
    
    
    <div {if $customer->isNew()} class="payment-page" {/if}>
    
    {* PAYMENT LOGIN FORM*}
    {if isset($formLogin) && $formLogin}
        {include file="Payment/@paymentBlocks/paymentLoginForm.tpl" formLogin = $formLogin} 
    {/if}    
    
    {$form->getBegin() nofilter}
    
    {if $form->getErrors()|@count gt 0}
        <p class="ff_err_notice ff_red_err" style="width: 710px">Form has <b> {$form->getErrors()|@count}</b> error(s). See below for more details:</p>
    {/if} 
    
    
    {* NEW CUSTOMER EMAIL *}
    {if isset($formLogin) && $formLogin}
        {include file="Payment/@paymentBlocks/sageFormEmailFieldset.tpl" form = $form} 
    {/if}                        
    
    <fieldset id="fieldset_1" class="payment-page-fieldset" style="margin-bottom:10px;">
        <legend class="payment-page-fieldset-legend">Details</legend>
        <div id="freeOrder">
            {if isset($formLogin)}
                <h4>Your order today is free, once you have entered your email address, please tick the terms and conditions and click submit to confirm your order.</h4>
            {else}
                <h4>Your order today is free, please tick the terms and conditions and click submit to confirm your order.</h4>
            {/if}
        </div>
    </fieldset>
    
    <fieldset id="fieldset_2" style="margin-bottom:10px;" class="payment-page-fieldset">
        <legend class="payment-page-fieldset-legend">Terms and Conditions</legend>
        <table class="ff_table">
            <tbody>
                <tr>
                    <th><a href="/general-terms-conditions.html" target="_blank">Terms and Conditions</a>
                    <br /><span style="font-size: 11px; color: #333333;">(Opens in new window)</span></th>
                    <td>{$form->getControl('terms') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('terms')}</span></td>
                </tr>               
            </tbody>
        </table>
    </fieldset>
                
    <fieldset id="fieldset_3" class="submit-on-account payment-page-fieldset">
        <legend class="payment-page-fieldset-legend">Summary</legend>
        <table class="ff_table">
            <tbody>
                <tr>
                    <th>&nbsp;</th>
                    <td>{$form->getControl('submit') nofilter}</td>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </fieldset>             
                
    {$form->getEnd() nofilter}	
		
	<p class="required"><span class="orange">*</span>Required Fields</p>
    
    </div>
    
</div>        
{include file="@footer.tpl"}
