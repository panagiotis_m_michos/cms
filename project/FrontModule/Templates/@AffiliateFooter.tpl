<div class="clear"></div>

</div>
<div id="footer">
    <a href="/" rel="nofollow" target="_blank">Company Formation</a> |
    <a href="https://www.companysearchesmadesimple.com" rel="nofollow" target="_blank">Company Search</a> |
    <a href="https://www.businesstrainingmadesimple.co.uk" rel="nofollow" target="_blank">Business Training</a> |
    <a href="https://www.londonpresence.com" rel="nofollow" target="_blank">Virtual Office</a> |
    <a href="https://www.madesimplegroup.com" rel="nofollow" target="_blank">MadeSimple Group</a>
    <br />
    <p>Company Formation MadeSimple provides company formation services and is a division of the Made Simple Group Ltd<br />
    Registered Office address: 20-22 Wenlock Road, London, N1 7GU, UK<br />
    Company Number: 04214713 Vat Number: GB820956327</p>
    <br />
</div>
</div>

<script>
    {literal}
    $(document).ready( function(){
        $('a.feedback_show').click(function(){
            var url = $(this).attr('href');
            $overlay = $('<div class="overlay"><iframe id="overlay_iframe" src="' + url + '?popup=1' + '" frameborder="0" width="600" height="480"></iframe></div>');
            $('body').append($overlay);
            $($overlay).overlay({
                effect: 'apple',
                load: true,
                mask: '#000'
            });
            return false;
        });
    })
    {/literal}
</script>

<script>
    {literal}
            $(document).ready( function(){
             $('a.popup_page').click(function(){
                 var url = $(this).attr('href');
                 $overlay = $('<div class="overlay"><iframe id="overlay_iframe" src="' + url + '?popup=1' + '" frameborder="0" width="600" height="470"></iframe></div>');
                 $('body').append($overlay);
                     $($overlay).overlay({
                         effect: 'apple',
                         load: true,
                         mask: '#000'
                     });
                 return false;
             });
            })
    {/literal}
</script>
</body>
</html>
