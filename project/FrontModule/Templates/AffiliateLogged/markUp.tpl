{include file="@AffiliateHeader.tpl"}
{include file="@blocks/affiliateLeftMenu.tpl"}

{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
<div style="margin-left:200px;">
    {if !empty($affiliateProducts)}
    {$form->getBegin() nofilter}
    <table class="affiliateTable">

        <col>
        <col width="100" />
        <col width="100" />
        <col width="100" />
        <tr>
            <th>Product</th>
            <th>Base Price</th>
            <th>Mark Up</th>
            <th>Total Price</th>
        </tr>
	{foreach from=$affiliateProducts key="id" item="affiliateProduct"}
	{$affiliateProductId = $affiliateProduct->affiliateProductId}
	{assign var="affP" value=$affiliateProduct->getProduct()}
    {assign var="affTotalPrice" value=$affP->getPrice()+$affiliateProduct->markUp}
        <tr>
            <td>{$affP->getLngTitle()}</td>
            <td>&pound;{$affP->getPrice()}</td>
            <td>&pound;<input type="text" name="editMarkUp[{$id}]" size="5" value="{$affiliateProduct->markUp}" /></td>
            <td class="center"><b>&pound;{$affTotalPrice}</b></td>
        </tr>
	{/foreach}
    </table>
    <p></p>

    <table class="ff_table">
    <col width="500">
        <tbody><tr>
                <th>{$form->getControl('submit') nofilter}</th>
                <td></td>
                <td></td>
                <td style="color:#666">All prices are exclusive of VAT.</td>
            </tr>
        </tbody>
    </table>

    {$form->getEnd() nofilter}

	{* PAGINATOR *}
	{$paginator nofilter}
    {else}
    <p>No affiliate products</p>
    {/if}
</div>
{include file="@AffiliateFooter.tpl"}
