{include file="@AffiliateHeader.tpl"}
{include file="@blocks/affiliateLeftMenu.tpl"}
{if $visibleTitle}
<h1>{$title}</h1>
{/if}
<div style="margin-left:200px;">
{$form->getBegin() nofilter}
{$form->getHtmlErrorsInfo() nofilter}
<fieldset id="fieldset_0">
	<legend>Data</legend>
    <table class="ff_table">
        <tbody>
            <tr>
                <th style="padding-left: 4px;"><b>Website:</b></th>
                <td>{$affiliate->web}</td>
                <td></td>
            </tr>
            <tr>
                <th>{$form->getLabel('companyName') nofilter}</th>
                <td>{$form->getControl('companyName') nofilter}</td>
                <td><span class="redmsg">{$form->getError('companyName')}</span></td>
            </tr>
            <tr>
                <th>{$form->getLabel('firstName') nofilter}</th>
                <td>{$form->getControl('firstName') nofilter}</td>
                <td><span class="redmsg">{$form->getError('firstName')}</span></td>
            </tr>
            <tr>
                <th>{$form->getLabel('lastName') nofilter}</th>
                <td>{$form->getControl('lastName') nofilter}</td>
                <td><span class="redmsg">{$form->getError('lastName')}</span></td>
            </tr>
        </tbody>
    </table>
</fieldset>

<fieldset id="fieldset_1">
	<legend>Login</legend>
    <table class="ff_table">
        <tbody>
            <tr>
                <th>{$form->getLabel('email') nofilter}</th>
                <td>{$form->getControl('email') nofilter}</td>
                <td><span class="redmsg">{$form->getError('email')}</span></td>
            </tr>
            <tr>
                <th>{$form->getLabel('password') nofilter}</th>
                <td>{$form->getControl('password') nofilter}<span class="ff_desc">(Min 6 characters)</span></td>
                <td><span class="redmsg">{$form->getError('password')}</span></td>
            </tr>
            <tr>
                <th>{$form->getLabel('passwordConfirmation') nofilter}</th>
                <td>{$form->getControl('passwordConfirmation') nofilter}<span class="ff_desc">(Min 6 characters)</span></td>
                <td><span class="redmsg">{$form->getError('passwordConfirmation')}</span></td>
            </tr>
        </tbody>
    </table>
</fieldset>

<fieldset id="fieldset_2">
	<legend>Action</legend>
    <table class="ff_table">
        <tbody>
            <tr>
                <th></th>
                <td>{$form->getControl('submit') nofilter}</td>
                <td></td>
            </tr>
        </tbody>
    </table>
</fieldset>
{$form->getEnd() nofilter}
</div>
{include file="@AffiliateFooter.tpl"}