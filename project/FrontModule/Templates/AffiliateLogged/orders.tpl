{include file="@AffiliateHeader.tpl"}
{include file="@blocks/affiliateLeftMenu.tpl"}
{literal}
<style>
<!--
h2 {
	position: relative;
}
	h2 a {
		font-size: 11px;
		text-decoration: none;
		font-weight: normal;
		position: absolute;
		right: 60px;
		top: 1px;
	}
		h2 a.ui-widget-content.ui-corner-all {
			background: #F3F3F3;
			color: #444;
			padding: 1px 3px 1px 3px;
		}
		h2 a:hover {
			text-decoration: none;
		}
-->
</style>
{/literal}
	{* BREADCRUMBS *}
	<p style="font-size: 12px;"><a href="{$this->router->link("AffiliateLoggedControler::MY_REPORTS")}">My reports</a> &gt; {$title}</p>
    
{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
<div style="margin-left:200px;">
{* FILTER FORM *}
{$form->getBegin() nofilter}
	<fieldset style="width:705px;">
		<legend>Filter</legend>
		<table border="0" cellspacing="5" cellpadding="0" class="affiliateFilter">
		<tr>
			<td>{$form->getLabel('dtFrom') nofilter}</td>
			<td>{$form->getControl('dtFrom') nofilter}</td>
			<td>-</td>
			<td>{$form->getControl('dtTill') nofilter}</td>
			<td>{$form->getControl('submit') nofilter}</td>
		</tr>
		</table>
	</fieldset>
{$form->getEnd() nofilter}

<h2>Orders<a href="{$this->router->link(NULL, "affiliateId=$affiliateId", "csv=1")}" class="ui-widget-content ui-corner-all">Export to CSV</a></h2>

<table class="affiliateTable">
<col width="80">
<col>
<col width="80">
<col width="80">
<col width="80">
<col width="100">
<tr>
	<th>Id</th>
	<th>Email</th>
	<th>Subtotal</th>
	<th>Vat</th>
	<th>Total</th>
    <th>markUp</th>
	<th>Date</th>
</tr>

{if !empty($orders)}
	{foreach from=$orders key="key" item="order"}
	{assign var="orderId" value=$order->orderId}
	<tr>
		<td>{$order->orderId}</td>
		<td>{$order->customerEmail}</td>
		<td>{$order->subtotal}</td>
		<td>{$order->vat}</td>
		<td>{$order->total}</td>
        <td>{$order->markUp}</td>
		<td>{$order->dtc|date_format:"%d/%m/%Y"}</td>
	</tr>
	{/foreach}
{else}
	<tr>
		<td colspan="7">No orders</td>
	</tr>
{/if}
</table>

{* === PAGINATOR === *}
{$paginator nofilter}
</div>

{include file="@AffiliateFooter.tpl"}