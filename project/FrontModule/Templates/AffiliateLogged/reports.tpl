{include file="@AffiliateHeader.tpl"}
{include file="@blocks/affiliateLeftMenu.tpl"}
{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
<div style="margin-left:200px;">
{$text nofilter}
<h2> White Label reporting</h2>
<ul style="list-style-type: none; padding-left: 10px;">
     <li><a href="{$this->router->link("AffiliateLoggedControler::ORDERS_PAGE")}">Orders</a></li>
     <li><a href="{$this->router->link("AffiliateLoggedControler::CUSTOMERS_PAGE")}">Customers</a></li>
</ul>

<h2> Sole v Ltd and Affiliate Link reporting</h2>
<ul style="list-style-type: none; padding-left: 10px;">
     <li><a href="{$this->router->link("AffiliateLoggedControler::ANALYTICS_PAGE")}">Affiliate Links Report</a></li>
     <li><a href="{$this->router->link("AffiliateLoggedControler::SOLE_V_LTD_REPORT_PAGE")}">Sole v Ltd Report</a></li>
</ul>
</div>
{include file="@AffiliateFooter.tpl"}