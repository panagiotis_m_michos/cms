{include file="@AffiliateHeader.tpl"}
{include file="@blocks/affiliateLeftMenu.tpl"}
{literal}
<style>
<!--
h2 {
	position: relative;
}
	h2 a {
		font-size: 11px;
		text-decoration: none;
		font-weight: normal;
		position: absolute;
		right: 50px;
		top: 1px;
	}
		h2 a.ui-widget-content.ui-corner-all {
			background: #F3F3F3;
			color: #444;
			padding: 1px 3px 1px 3px;
		}
		h2 a:hover {
			text-decoration: none;
		}
-->
</style>
{/literal}
	{* BREADCRUMBS *}
	<p style="font-size: 12px;"><a href="{$this->router->link("AffiliateLoggedControler::MY_REPORTS")}">My reports</a> &gt; {$title}</p>
    
{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}

<div style="margin-left:200px;">
<h2>Customers<a href="{$this->router->link(NULL, "affiliateId=$affiliateId", "csv=1")}" class="ui-widget-content ui-corner-all">Export to CSV</a></h2>

<table class="affiliateTable">
<col width="130">
<col>
<col>
<tr>
	<th class="right">Id</th>
	<th>Contact name</th>
	<th>Email</th>
</tr>
{if !empty($customers)}
	{foreach from=$customers key="id" item="customer"}
	<tr>
		<td class="right">{$id}</td>
		<td>{$customer->firstName} {$customer->lastName}</td>
		<td>{$customer->email}</td>
	</tr>
	{/foreach}
{else}
	<tr>
		<td colspan="3">No customers found.</td>
	</tr>
{/if}


</table>

        {* === PAGINATOR === *}
        {$paginator nofilter}
</div>
{include file="@AffiliateFooter.tpl"}