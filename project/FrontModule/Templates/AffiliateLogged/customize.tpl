{include file="@AffiliateHeader.tpl"}
{include file="@blocks/affiliateLeftMenu.tpl"}

{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
{$text nofilter}
{$form->getBegin() nofilter}
<div id="rightPart">
    <div id="formText">
        <div class="input">
            {$form->getLabel("companyColour") nofilter}
            {$form->getControl('companyColour') nofilter}
        </div>
        <div class="input">
            {$form->getLabel("textColour") nofilter}
            {$form->getControl('textColour') nofilter}
        </div>  
    </div>
    <div id="formSubmit">
        {$form->getControl('submit') nofilter}
    </div>
</div>
{$form->getEnd() nofilter}
<div style="float:left">
    <h2>Example</h2>
    <div class="block">
        <form name="test" action="#">
            <label for="companyColour">Company Name: *</label>
            <input type="text" name="text">
            <input type="submit" name="submit" value="Search" style="height:25px; width:55px;">
        </form>
    </div>
</div>


{literal}
<script type="text/javascript">
    <!--
    $(document).ready(function() {
        $(".block").css('background-color', "#" + $("#companyColour").val());
        $(".block").css('color', "#" + $("#textColour").val());
        $("#companyColour").focusout(function() {
            $(".block").css('background-color', "#" + $("#companyColour").val());
        })
        $("#textColour").focusout(function() {
            $(".block").css('color', "#" + $("#textColour").val());
        })
    });
    //-->
</script>
{/literal}
{include file="@AffiliateFooter.tpl"}