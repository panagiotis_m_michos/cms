{include file="@AffiliateHeader.tpl"}
{include file="@blocks/affiliateLeftMenu.tpl"}
{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
<div style="margin-left:200px;">
    <div class="dashMainBlock">
        <div class="dashBlock">
            <h2><a href="{$this->router->link("AffiliateLoggedControler::SOLE_V_LTD_PAGE")}">Sole v Limited</a></h2>
            <p>Are you better off as a sole trader or a limited company? Let your customers enter their annual profits and we'll calculate it for you.</p>
        </div>
        <div class="dashBlock">
            <h2><a href="{$this->router->link("AffiliateLoggedControler::AFFILIATE_LINKS_PAGE")}">Affiliate Links</a></h2>
            <p>Tell your friends about us. Link to our site using the code we provide. Commission from sales.</p>
        </div>
    </div>
    <div id="dashText">
        <p>10% commission on all sales!</p>
    </div>
    <div class="dashMainBlock">
        <div class="dashBlock">
            <h2><a href="{$this->router->link("AffiliateLoggedControler::MY_REPORTS")}">My Reports</a></h2>
            <p>View orders and affiliate revenue.</p>
        </div>
        <div class="dashBlock">
            <h2><a href="{$this->router->link("AffiliateLoggedControler::CUSTOMIZE_PAGE")}">Customise</a></h2>
            <p>Change the colours of the Sole v Limited widget to suit your site.</p>
        </div>
        <div class="dashBlock">
            <h2><a href="{$this->router->link("AffiliateLoggedControler::MY_DETAILS_PAGE")}">My Details</a></h2>
            <p>Update your personal details including your login and password</p>
        </div>
    </div>
</div>
{include file="@AffiliateFooter.tpl"}