{include file="@AffiliateHeader.tpl"}
{include file="@blocks/affiliateLeftMenu.tpl"}
{if $visibleTitle}
<h1>{$title}</h1>
{/if}
<div style="margin-left:200px;">
    {* WHITE LABEL *}
	{$text nofilter}

    <div id="frameCode">
		{assign var="hash" value=$affiliate->hash}
		{assign var="link" value=$this->router->absoluteFrontLink("WidgetWhiteLabelControler::HOME_PAGE", "hash=$hash")}
        <textarea rows="7" cols="67" readonly="readonly" onclick="this.select();"><iframe src="{$link}" width="100%" height="1000" frameborder="0"><p>Your browser does not support iframes.</p></iframe><p style="font-size: 10px; color: #666666;">Powered by Companies Made Simple - The simplest <a href="/" style="color: #666666; text-decoration: none;">company formation</a> service</p> </textarea>
    </div>

    <em>Adjust the width to suit your site. To change the colours click <a href="{$this->router->absoluteFrontLink("AffiliateLoggedControler::CUSTOMIZE_PAGE")}">customise</a>.</em>

    <h2 id="demoTitle">View the demo</h2>
    <p>We've embeded the above code directly in to this page so you can see exactly how it works.</p>
    <div id="frameDemo">
        <div id="frameForm">
            <iframe src="{$link}" width="100%" height="400" frameborder="0">
                <p>Your browser does not support iframes.</p>
            </iframe>
        </div>
        <div id="frameText">
            <p>NB. The border is shown here to demonstrate where the iframe ends. When placed on your site the iframe will blend seamlessly into your own.</p>
        </div>
    </div>
</div>
{include file="@AffiliateFooter.tpl"}
