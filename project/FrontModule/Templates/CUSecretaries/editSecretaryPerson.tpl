{include file="@header.tpl"}

{* JS FOR PREFILL *}
<script type="text/javascript">
/* <![CDATA[ */
var addresses = {$jsPrefillAdresses nofilter};
/* ]]> */
</script>

{literal}
<script>
$(document).ready(function () {
	
	// prefill address	
	$("#prefillAddress").change(function () {
		var value = $(this).val();
		address = addresses[value];
		for (var name in address) {
			$('#'+name).val(address[name]);
		}
	});
	
	// change name 
	toogleChangeName();
	$("#changeName").click(function (){
		toogleChangeName();
	});
	function toogleChangeName() {
		disabled = !$("#changeName").is(":checked");
		$('#title').attr("disabled", disabled);
		$('#forename').attr("disabled", disabled);
		$('#middle_name').attr("disabled", disabled);
		$('#surname').attr("disabled", disabled);
	}
	
	// change address 
	toogleChangeAddress();
	$("#changeAddress").click(function (){
		toogleChangeAddress();
	});
	function toogleChangeAddress() {
		disabled = !$("#changeAddress").is(":checked");
		$('#prefillAddress').attr("disabled", disabled);
		$('#premise').attr("disabled", disabled);
		$('#street').attr("disabled", disabled);
		$('#thoroughfare').attr("disabled", disabled);
		$('#post_town').attr("disabled", disabled);
		$('#county').attr("disabled", disabled);
		$('#postcode').attr("disabled", disabled);
		$('#country').attr("disabled", disabled);
	}
});
</script>
{/literal}
<div id="maincontent2">
	<p style="margin: 0 0 15px 0; font-size: 11px;"><a href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}">{$companyName}</a> &gt; {$title}</p> 
	{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
	{$form nofilter}
</div>
{include file="@footer.tpl"}
