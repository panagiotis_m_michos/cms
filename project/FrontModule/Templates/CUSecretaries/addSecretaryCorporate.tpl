{include file="@header.tpl"}

{* JS FOR PREFILL *}
<script type="text/javascript">
/* <![CDATA[ */
var addresses = {$jsPrefillAdresses nofilter};
var officers = {$jsPrefillOfficers nofilter};
/* ]]> */
</script>

{literal}
<script>
$(document).ready(function () {

	// prefill address	
	$("#prefillAddress").change(function () {
		var value = $(this).val();
		address = addresses[value];
		for (var name in address) {
			$('#'+name).val(address[name]);
		}
	});
	
	// prefill officers	
	$("#prefillOfficers").change(function () {
		var value = $(this).val();
		address = officers[value];
		for (var name in address) {
			$('#'+name).val(address[name]);
		}
	});

	// EEA
	toogleEEA();
	
	$("input[name='eeaType']").click(function (){
		toogleEEA();
	});
	
	function toogleEEA() {
		// EEA
		if ($("#eeaType1").is(":checked")) {
			$("#place_registered").attr("disabled", false);
			$("#registration_number").attr("disabled", false);
			$("#law_governed").attr("disabled", true);
			$("#legal_form").attr("disabled", true);
		// Non EEA
		} else if ($("#eeaType2").is(":checked")) {
			$("#place_registered").attr("disabled", false);
			$("#registration_number").attr("disabled", false);
			$("#law_governed").attr("disabled", false);
			$("#legal_form").attr("disabled", false);
		} else {
			$("#place_registered").attr("disabled", true);
			$("#registration_number").attr("disabled", true);
			$("#law_governed").attr("disabled", true);
			$("#legal_form").attr("disabled", true);
		}
		
	}
	
});
</script>
{/literal}

<div id="maincontent2">
	<p style="margin: 0 0 15px 0; font-size: 11px;"><a href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}">{$companyName}</a> &gt; {$title}</p> 
	{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
	<p style="text-align: right;">To make a human appointment <a href="{$this->router->link("CUSecretariesControler::ADD_SECRETARY_PERSON_PAGE", "company_id=$companyId")}">click here</a></p>
	{$form nofilter}
</div>
{include file="@footer.tpl"}
