{include file="@header.tpl"}

{literal}
    <style type="text/css">
        <!--
        div#maincontent2 {
            float: left;
            width: 730px;
            padding-left:10px;
        }
        -->
    </style>
{/literal}

{* RIGHT COLUMN *}
<div style=" padding-top:40px; float: right; width:230px;">
    <div style="font-size: 12px; padding: 8px; color: #666666;">
        <img style="float: left;" alt="lock" width="29" height="40" src="{$urlImgs}img8.gif">
        <p style="height: 40px; line-height: 40px; margin-left: 5px; margin-left: 29px; padding-left: 10px; font-weight: bold; font-size: 14px;">You're safe with us!</p>
        <p style="padding-top: 16px;">Every payment is secured by industry standard 256-bit encryption. Companies Made Simple never stores your card details on our servers.</p>
        <img style="margin-top: 16px;" alt="secured by sagepay" width="150" height="52" src="{$urlImgs}img10.gif">
    </div>
    <script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=www.companiesmadesimple.com&amp;size=L&amp;use_flash=YES&amp;use_transparent=YES&amp;lang=en"></script>
    <div style="margin-top: 16px;">
        <a href="https://www.securitymetrics.com/site_certificate.adp?s=94%2e236%2e66%2e34&amp;i=374287" target="_blank">
            <img src="https://www.securitymetrics.com/images/sm_ccsafe_check1.gif" alt="SecurityMetrics for PCI Compliance, QSA, IDS, Penetration Testing, Forensics, and Vulnerability Assessment" border="0"></a>
    </div>
</div>

<div id="maincontent2">
    <script type="text/javascript">var access = 9</script>
    <script type="text/javascript">var loginUrl = "{$this->router->link("LoginControler::LOGIN_PAGE")}"</script>
    {literal}

        <script type="text/javascript">
        $(document).ready(function(){
           $('#country').change(function () {
                $('#country option:selected').each(function () {
                    if($(this).text() == "Spain")
                    {
                        $('#spaintext').show(400);
                    }else{
                        $('#spaintext').hide(400);
                    }
                });
            });

            toggleFields();
            $("#credit").click(function (){
                toggleFields();
            });
            doDisable();
            $("form[name='1223_sage_payment'] #emails").removeAttr('disabled');
            $("form[name='1223_sage_payment'] label[for='emails']").css('color',"#000");
            $(".pound-style").css('color',"#D3D3D3");
            $(".check-email-login-form").hide();
            $(".payment-email-ok").hide();
            if(typeof $("form[name='1223_sage_payment'] #emails").val() !== "undefined" && $("form[name='1223_sage_payment'] #emails").val() != 0) {
                $('.editEmailLink').show();
                $('.check-email-avaiability').hide();
                doEnable();
                $("form[name='1223_sage_payment'] #emails").attr('readonly',true).css('background-color','#D3D3D3');

            };


            $(".editEmailFields").click(function(){
                doDisable();
                $("form[name='1223_sage_payment'] #emails").removeAttr('disabled');
                $("form[name='1223_sage_payment'] #emails").removeAttr('readonly',true).removeAttr('style');
                $("form[name='1223_sage_payment'] label[for='emails']").css('color',"#000");
                $(".pound-style").css('color',"#D3D3D3");
                $('.isOkToShow').hide();
                $('.isNotOkToShow').hide();
                $(".editEmailLink").hide();
                $(".check-email-avaiability").show();
                $(".dec_new_cust").show();
                $(".payment-email-ok").hide();

            });

            $('.editEmailLinkLogin').click(function(){
                $(".check-email-login-form").hide();
                $(".email-form-check").show();
            });


            $('.forgotpassword-email').click(function(){
                if(!confirm('We will email you a new password. Are you sure?')){
                    return false;
                }
                $('.loading-image').show();
                $('.forgotpassword-email').hide();
                $('.newPaswwordSent').hide();
                var self = $(this);
                    $.ajax({
                        url : self.attr('href') + "&email=" + encodeURIComponent($("#email").val()),
                        type : 'GET',
                        dataType: "json",
                        data : {},
                        success : function(data){
                           if(data.succes == 1) {
                               $('.loading-image').hide();
                               $('.newPaswwordSent').show();
                               $('.after-emailing').show();
                           }
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) {
                            alert('Oops...an error occurred (form error)');
                        },
                        timeout: function () {
                            alert('Oops...an error occurred (form timeout)');
                        }
                    });

                return false;
            });

            $('form[name="1223_login"]').live('submit',function(){
                    var self = $(this);
                    var dataForm = $(self).serialize();
                    $.ajax({
                        type: "POST",
                        url: self.attr('action'),
                        data: dataForm,
                        dataType: "json",
                        success: function(data){
                            if(data.error ==1) {
                                if(data.message) {
                                    $(".new-err-msg").html(data.message.password);
                                }
                            }
                            if(data.redirect == 1) {
                                window.location.href = data.link;
                            }

                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) {
                            alert('Oops...an error occurred (form error)');
                        },
                        timeout: function () {
                            alert('Oops...an error occurred (form timeout)');
                    }
                    });
                    return false;
                });


            //check if email exists
            $('.check-email-avaiability').click(function(){
                var email = $("#emails").val();
                checkEmail(this,email);
                return false;
            });

            $('#emails').keypress(function(e) {
                if(e.which == 13) {
                   checkEmail($('.check-email-avaiability'),$("#emails").val());
                }
            });

        });

        var checkEmail = function(self,email) {
            if(window.CMS.Validator.validateEmail(email)) {
                $('.email-form-check .ff_control_err').remove();
                produceEmail(self,email);

            } else {
                $('.email-form-check .ff_control_err').remove();
                $('.editEmailLink').after('<span class="ff_control_err new-err-msg" style="color:red">Email address is not valid!</span>');
            }
        };

        var produceEmail = function(elem,email) {
			$.ajax({
				url : $(elem).attr('href') + "&email=" + encodeURIComponent(email),
				type : 'GET',
                dataType: "json",
				data : {},
				success : function(data){
                    $('.show-ckeck-company').fadeIn('slow');
					if(data.succes == 1) {
                        if(data.showLogin ==1){
                            $(".email-form-check").hide();
                            $('.check-email-login-form').fadeIn();
                            $("form[name='1223_login'] #email").val($("form[name='1223_sage_payment'] #emails").val());
                            $("form[name='1223_login'] #password").focus().val(null);
                            $("form[name='1223_login'] #email").attr('readonly',true).css('background-color','#D3D3D3');
                            $('.editEmailLinkLogin').fadeIn();

                        } else {
                            $('.isOkToShow').show();
                            $('.isNotOkToShow').hide();
                            $('.check-email-avaiability').hide();
                            doEnable();
                            $("form[name='1223_sage_payment'] #emails").attr('readonly',true).css('background-color','#D3D3D3');
                            $(".pound-style").css('color',"#000");
                            $(".editEmailLink").show();
                            $(".dec_new_cust").hide();
                            $(".payment-email-ok").show();
                        }
                    } else {
                        $('.isNotOkToShow').show();
                        $('.isOkToShow').hide();
                    }
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alert('Oops...an error occurred (form error)');
                },
                timeout: function () {
                    alert('Oops...an error occurred (form timeout)');
                }
			});
        }

        var doDisable = function() {
            $("form[name='1223_sage_payment'] input[type='text']").attr('disabled','disabled');
            $("form[name='1223_sage_payment'] select").attr('disabled','disabled');
            $("form[name='1223_sage_payment'] input[type='checkbox']").attr('disabled','disabled');
            $("form[name='1223_sage_payment'] input[type='submit']").attr('disabled','disabled');
            $("form[name='1223_sage_payment'] label").css('color','#CCCCCC');
        }

        var doEnable = function() {
            $("form[name='1223_sage_payment'] input[type='text']").removeAttr('disabled');
            $("form[name='1223_sage_payment'] select").removeAttr('disabled');
            $("form[name='1223_sage_payment'] input[type='checkbox']").removeAttr('disabled');
            $("form[name='1223_sage_payment'] input[type='submit']").removeAttr('disabled');
            $("form[name='1223_sage_payment'] label").css('color',"#000");
        }

        function toggleFields() {
                disabled = $("#credit").is(":checked");
                $("form input[type='text'], form select").each(function (){
                    $(this).attr("disabled", disabled);
                });
                $('#amount').attr("disabled", false);
                $('#paypal').attr("disabled", disabled);
                $('#google').attr("disabled", disabled);
            }
       var doSage = function(value) {
           $("form[name='1223_sage_payment'] input[name='paymentType']").val(value);
       }

       var doPaypal = function(value) {
           $("form[name='1223_sage_payment'] input[name='paymentType']").val(value);
       }

        </script>
    {/literal}
    <div class="new-payment-setps">
        <img src="{$urlImgs}progress_bar_buy.png" alt="new payment steps" width="730" height="44"/>

    </div>

    {if $enableMessages}
        <div class="info" style="padding-top: 10px;">{$paymentPageMessages nofilter}</div>
    {/if}

    {if $visibleTitle}
        <div style="margin-top: 25px;margin-bottom: 10px;">
            {if empty($companyName) or $companyName|h == "CHANGE YOUR COMPANY NAME LTD"}
                <div>
                    <h1>Payment</h1>
                </div>
                {*<h6 style="color: #999; font-weight: normal; float: left; margin-left: 10px;color: #555; ">You will be prompted to enter your company name after payment.</h6>*}
            {else}
                <div class="fleft">
                    <h1>Payment</h1>
                </div>
                <div style="float: right; text-align: right;padding-right: 5px;margin-bottom: 10px;">
                    <h3>{$companyName}</h3>
                    <h6 style="color: #999; margin-top: -10px; font-weight: normal;">Subject to approval by Companies House</h6>
                </div>
            {/if}

            <div class="clear"></div>
        </div>
    {/if}
    {*<div class="flash info2" style="margin-left: 0; width: 700px;">
		<strong>Paying with Paypal</strong>. NB. You do not need an account to use Paypal, once you've agreed to our Ts & Cs and clicked 'Paypal' just click the 'Don't have a Paypal account?' link on the Paypal page.
    </div>*}


    <div class="check-email-login-form">
        {$formLogin->getBegin() nofilter}
            <fieldset styel="clear: both;">
            <legend>Customer Login</legend>
            <table class="ff_table">
            <tbody>
            <tr>
                <th>{$formLogin->getLabel('email') nofilter}</th>
                <td>
                    {$formLogin->getControl('email') nofilter}
                </td>
                <td>
                    <span class="editEmailLinkLogin" style="display: none;">
                        <a href="javascript:;"  style="color: #006699;padding-left:5px;">Use another email</a>
                        <div class="clear"></div>
                    </span>
                    <span class="ff_control_err">{$formLogin->getError('email')}</span>
                </td>

            </tr>
            <tr>
                <th>{$formLogin->getLabel('password') nofilter}</th>
                <td>
                    {$formLogin->getControl('password') nofilter}
                    {*<span class="ff_desc">Please provide New Company Name</span>*}
                </td>
                <td>
                    <span class="ff_control_err new-err-msg">{$formLogin->getError('password')}</span>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    {$formLogin->getControl('login') nofilter}
                </td>
                <td  style="vertical-align: middle;">
                    <div class="updateForgottenPassword">
                        <a href="{$this->router->link("PaymentNewCustomerControler::NEW_PASSWORD_PAGE")}" class="forgotpassword-email" style="color: #006699; padding-left: 10px;" >I have forgotten my password</a>
                        <span class="loading-image" style="display: none;">
                            Emailing new password!
                        </span>
                        <span class="newPaswwordSent" style="display: none;">
                            New password sent. <a href="{$this->router->link("PaymentNewCustomerControler::NEW_PASSWORD_PAGE")}" class="forgotpassword-email after-emailing" style="color: #006699;" >Resend password.</a>
                        </span>
                    </div>
                </td>
            </tr>
            </tbody>
            </table>
            </fieldset>
        {$formLogin->getEnd() nofilter}
        </div>



    <div class="sage-payment-form">
        {$form->getBegin() nofilter}
        {*<fieldset styel="clear: both;">
        <legend>Payment Method</legend>
        <table class="ff_table">
        <tbody>
        <tr>
        <th>{$form->getLabel('paymentOption') nofilter}</th>
        <td colspan="2"> {$form->getControl('paymentOption') nofilter}</td>
        </tr>
        </tbody>
        </table>
        </fieldset>
        *}
        <fieldset style="clear: both;color: #000; " class="email-form-check">
            <legend>New and Returning Customer Details</legend>
            <table class="ff_table">
                <tbody>
                    <tr>
                        <th>
                            {$form->getLabel('emails') nofilter}
                        </th>
                        <td >
                            {$form->getControl('emails') nofilter}
                            <span class="ff_desc dec_new_cust">We use your email to send your company details!</span>
                        </td>
                        <td style="width: 300px;">
                            <span class="show-ckeck-company" style="display: none;float: left;margin-top: -4px;margin-left: 5px;">
                                <span class="isOkToShow" style="display: none; ">
                                    <img src="{$urlImgs}greentick24.png" alt="email is available" style="float: left;"/>
                                    {*<span style="color: #339900; float: left; padding-left: 3px;padding-top: 7px;">Email is valid</span> *}
                                    <div class="clear"></div>
                                </span>
                                <span class="isNotOkToShow error" style="display: none; ">
                                    <img src="{$urlImgs}redcross.png" alt="email isn't available"/>
                                    <div class="clear"></div>
                                </span>
                            </span>
                            {* <span class="ff_control_err">{$checkEmail->getError('email')}</span> *}
                            <span class="editEmailLink" style="display: none;float: left; padding-left: 10px;padding-top:3px;">
                                <a href="javascript:;" class="editEmailFields" style="color: #006699">Edit</a>
                                <div class="clear"></div>
                            </span>
                            <div class="clear"></div>
                        </td>
                    </tr>
                    <tr>
                        <th>&nbsp;</th>
                        <td colspan="2">
                            <a href="{$this->router->link("PaymentNewCustomerControler::SAGE_CHECK_NEW_CUSTOMER_PAGE")}" class="check-email-avaiability" style="text-decoration: none; color: #000;">
                                <button type="button" style="cursor: pointer;" onclick="_gaq.push(['_trackEvent', 'Links', 'Click', 'Clicked Create Account or Sign In button on payment page']);">Create Account or Sign In</button>
                            </a>
                            <span class="payment-email-ok" style="font-weight: bold">Please enter your payment details below:</span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </fieldset>


        <fieldset id="fieldset_1">
            <legend>Payment Details</legend>
            <table class="ff_table">
                <tbody>
                    <tr>
                        <th>{$form->getLabel('amount') nofilter} <span style="margin-left:5px;" class="pound-style">&pound;</span></th>
                        <td >{$form->getControl('amount') nofilter}</td>
                        <td></td>
                    </tr>
                  {if !$disableSage}
                  <tr>
                        <th>{$form->getLabel('cardholder') nofilter}</th>
                        <td>{$form->getControl('cardholder') nofilter}</td>
                        <td><span class="redmsg">{$form->getError('cardholder')}</span></td>
                    </tr>
                    <tr>
                        <th>{$form->getLabel('cardType') nofilter}</th>
                        <td>{$form->getControl('cardType') nofilter}</td>
                        <td><span class="redmsg">{$form->getError('cardType')}</span></td>
                    </tr>
                    <tr>
                        <th>{$form->getLabel('cardNumber') nofilter}</th>
                        <td>{$form->getControl('cardNumber') nofilter}</td>
                        <td><span class="redmsg">{$form->getError('cardNumber')}</span></td>
                    </tr>
                <noscript>
                <tr>
                    <th>{$form->getLabel('issueNumber') nofilter}</th>
                    <td>{$form->getControl('issueNumber') nofilter}</td>
                    <td>The issue number MUST be entered EXACTLY as it appears on the card. e.g. some cards have Issue Number "4", others have "04".</td>
                </tr>
                <tr>
                    <th>{$form->getLabel('validFrom') nofilter}</th>
                    <td>{$form->getControl('validFrom') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('validFrom')}</span></td>
                </tr>
                </noscript>
                <tr class="issuered" style="display:none;">
                    <th>{$form->getLabel('issueNumber') nofilter}</th>
                    <td>
                        {$form->getControl('issueNumber') nofilter}
                        <div class="help-button2" style="float: right; margin: 0 15px 0 0;">
                            <a href="#" class="help-icon">help</a>
                            <em>The issue number MUST be entered EXACTLY as it appears on the card. e.g. some cards have Issue Number "4", others have "04".</em>
                        </div>
                    </td>
                    <td><span class="redmsg">{$form->getError('issueNumber')}</span></td>
                </tr>
                <tr class="issuered" style="display:none;">
                    <th>{$form->getLabel('validFrom') nofilter}</th>
                    <td>{$form->getControl('validFrom') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('validFrom')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('expiryDate') nofilter}</th>
                    <td>{$form->getControl('expiryDate') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('expiryDate')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('CV2') nofilter}</th>
                    <td>
                        {$form->getControl('CV2') nofilter}
                        <div class="help-button2" style="float: right; margin: 0 15px 0 0;">
                            <a href="#" class="help-icon">help</a>
                            <em>The security code is a three-digit code printed on the back of your card.
                                <img src="{$urlImgs}CVC2SampleVisaNew.png" />
                            </em>
                        </div>
                    </td>
                    <td><span class="redmsg">{$form->getError('CV2')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('address1') nofilter}</th>
                    <td>{$form->getControl('address1') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('address1')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('address2') nofilter}</th>
                    <td>{$form->getControl('address2') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('address2')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('address3') nofilter}</th>
                    <td>{$form->getControl('address3') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('address3')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('town') nofilter}</th>
                    <td>{$form->getControl('town') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('town')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('postcode') nofilter}</th>
                    <td>{$form->getControl('postcode') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('postcode')}</span></td>
                </tr>
                <tr>
                    <th>{$form->getLabel('country') nofilter}</th>
                    <td>{$form->getControl('country') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('country')}</span></td>
                </tr>
                <noscript>
                <tr>
                    <th>{$form->getLabel('billingState') nofilter}</th>
                    <td>{$form->getControl('billingState') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('billingState')}</span></td>
                </tr>
                </noscript>
                <tr class="state" style="display:none;">
                    <th>{$form->getLabel('billingState') nofilter}</th>
                    <td>{$form->getControl('billingState') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('billingState')}</span></td>
                </tr>
                {/if}
                </tbody>
            </table>
            <div id="spaintext" style="border: 1px solid #069;padding: 10px;background-color: #cce5ff;line-height: 18px; display: none;">
                Our main payment provider, Sagepay, is currently experiencing issues with some Spanish cards. If you have
                trouble with your payment we suggest selecting <strong>Paypal</strong>, from the Payment Method, above.
            </div>
        </fieldset>



        <fieldset id="fieldset_2" class="payment-page-fieldset">
            <legend class="payment-page-fieldset-legend">Terms and Conditions</legend>
            <table class="ff_table">
                <tbody>
                    <tr>
                        <th><a href="/general-terms-conditions.html" target="_blank">Terms and Conditions</a>
                            <br /><span style="font-size: 11px; color: #333333;">(Opens in new window)</span></th>
                        <td>{$form->getControl('terms') nofilter}</td>
                        <td><span class="redmsg">{$form->getError('terms')}</span></td>
                    </tr>
                    {*<tr>
                    <th>{$form->getLabel('tokenPayment') nofilter}</th>
                    <td>{$form->getControl('tokenPayment') nofilter}</td>
                    <td><span class="redmsg">{$form->getError('tokenPayment')}</span></td>
                    *}
                </tr>
                </tbody>
            </table>
        </fieldset>

        {if !empty($showAuth)}
            <div id="authFormBlock">
                <fieldset id="fieldset_3" class="payment-page-fieldset">
                    <legend class="payment-page-fieldset-legend">3D authentication</legend>
                    <a name="authFormBlock" href="#" style="visibility:hidden"></a>
                    <h3 class="mnone pbottom10">Please complete the 3D authentication process below</h3>
                    <iframe name="sage-iframe" src="{$authFormUrl}" width="100%" height="450"></iframe>
                </fieldset>
            </div>
            {/if}

        <div id="submitBlock" {if !empty($showAuth)} style="display:none" {/if}>
            {if !$disableSage}
            <fieldset id="fieldset_3" class="payment-page-fieldset">
                <legend class="payment-page-fieldset-legend">Summary</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <th>&nbsp;</th>
                            <td>{$form->getControl('submit') nofilter}</td>
                            <td>&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>
            {/if}
            <fieldset id="fieldset_4" class="payment-page-fieldset">
                <legend class="payment-page-fieldset-legend">Pay using</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <th>&nbsp;</th>
                            <td>{$form->getControl('paypal') nofilter}</td>
                            <td>&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>
        </div>
        {$form->getEnd() nofilter}
        <p class="required"><span class="orange">*</span>Required Fields</p>
    </div>
</div>
{include file="@footer.tpl"}
