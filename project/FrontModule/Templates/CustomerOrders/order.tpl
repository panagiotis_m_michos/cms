{include file="@header.tpl"}
<div id="maincontent1">

	<p style="margin: 0 0 15px 0; font-size: 11px;">
		{if isset($companyId)}
			<a href="{$this->router->link("CustomerOrdersControler::ORDERS_PAGE", "company_id=$companyId")}">Orders</a> &gt; {$title} #{$order->getId()}
		{else}
			Orders &gt; {$title} #{$order->getId()}
		{/if}
	</p>

	<h1>{$title} #{$order->getId()}</h1>
	<div style="text-align: right; width: 780px; font-size: 14px; font-weight: bold; margin-bottom: 10px;"><a href="{$this->router->link(null, "print=1")}">Download invoice</a></div>
	
	<table class="grid2" width="780">
	<col width="150">
	<tr>
		<th class="left">Order id</th>
		<td>{$order->getId()}</td>
	</tr>
	<tr>
		<th class="left">Status</th>
		<td>{$order->status}</td>
	</tr>
	<tr>
		<th class="left">Order Date</th>
		<td>{$order->dtc}</td>
	</tr>
	<tr>
		<th class="left">Contact</th>
		<td>{$order->customerName}</td>
	</tr>
	<tr>
		<th class="left">Telephone</th>
		<td>{$order->customerPhone}</td>
	</tr>
	</table>
	
	<h3 class="mtop20">Basket items</h3>
	<table class="grid2" width="780">
	<col>
	<col width="70">
	<col width="140">
	<tr>
		<th>Product</th>
		<th class="right">Qty</th>
		<th class="right">Price</th>
	</tr>
	{foreach from=$order->items item="item"}
	<tr>
		<td>{$item->productTitle}</td>
		<td align="right">{$item->qty}</td>
		<td align="right">{$item->price}</td>
	</tr>
	{/foreach}
	<tr>
		<td colspan="2" class="right"><b>Subtotal:</b></td>
		<td class="right">&pound;{$order->getRounded('realSubtotal')}</td>
	</tr>
	{if $order->discount > 0}
	<tr>
		<td colspan="2" class="right"><b>Discount:</b></td>
		<td class="right">-&pound;{$order->getRounded('discount')}</td>
	</tr>
	{/if}
	<tr>
		<td colspan="2" class="right"><b>VAT:</b></td>
		<td class="right">&pound;{$order->getRounded('vat')}</td>
	</tr>
    {if $order->credit > 0}
        <tr>
            <td colspan="2" class="right"><b>Credit:</b></td>
            <td class="right {if $order->isRefunded}priceRefunded{/if}">-&pound;{$order->getRounded('credit')}</td>
        </tr>
    {/if}
	<tr {if $order->isRefunded}class="borderSeparator"{/if}>
		<td colspan="2" class="right"><b>Total:</b></td>
		<td class="right {if $order->isRefunded}priceRefunded{/if}">&pound;{$order->getRounded('total')}</td>
	</tr>
	{if $order->isRefunded}
	<tr>
		<td colspan="2" class="right"><b>Money Refunded:</b></td>
		<td class="right">&pound;{$order->getRounded('refundValue')}</td>
	</tr>
    <tr>
        <td colspan="2" class="right"><b>Credit Refunded:</b></td>
        <td class="right">&pound;{$order->getRounded('refundCreditValue')}</td>
    </tr>
	{if $order->hasAdminFee()}
	<tr>
		<td colspan="2" class="right"><b>Admin Fee:</b></td>
		<td class="right">&pound;{$order->getAdminFee('rounded')}</td>
	</tr>
	{/if}
	{/if}
	</table>
	
	{if !empty($order->transactions)}
		<h3 class="mtop20">Transactions</h3>
		<table class="grid2" width="780">
		<col width="100">
		<col>
		<col width="100">
		<col width="90">
		<tr>
			<th>Type</th>
			<th>Cardholder</th>
			<th>Card number</th>
			<th>Date</th>
		</tr>
		{foreach from=$order->transactions item="transaction"}
		<tr>
			<td>{$transaction->type}</td>
			<td>{$transaction->cardHolder}</td>
			<td>{$transaction->cardNumber}</td>
			<td>{$transaction->dtc|date_format:"%d/%m/%Y"}</td>
		</tr>
		{/foreach}
		</table>
	{/if}
	
	<h3 class="mtop20">Made Simple Group Ltd trading as Companies Made Simple</b></h3>
	<table class="grid2" width="780">
	<col width="220">
	<tr>
		<th>Address:</th>
		<td>
			20-22 Wenlock Road<br />
			London N1 7GU
		</td>
		
	</tr>
	<tr>
		<th>Tel:</th>
		<td>020 7608 5500</td>
	</tr>
	<tr>
		<th>Email:</th>
		<td>theteam@madesimplegroup.com</td>
	</tr>
	<tr>
		<th>Company registered in England No:</th>
		<td>04214713</td>
	</tr>
	<tr>
		<th>VAT Number:</th>
		<td>GB820956327</td>
	</tr>
	</table>

	<div style="text-align: right; width: 780px; font-size: 14px; font-weight: bold; margin-top: 10px;"><a href="{$this->router->link(null, "print=1")}">Download invoice</a></div>

</div>
{include file="@footer.tpl"}
