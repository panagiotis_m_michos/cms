{include file="@header.tpl"}
<div id="maincontent1">
{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}

{if !empty($orders)}
	<table class="grid2" width="780">
	<col width="80">
	<col>
	<col width="80">
	<col width="80">
	<col width="80">
	<col width="100">
	<col width="70">
	<tr>
		<th class="right">Id</th>
		<th>Status</th>
		<th class="right">Subtotal</th>
		<th class="right">Vat</th>
		<th class="right">Total</th>
		<th class="center">Date</th>
		<th class="center">Action</th>
	</tr>
	{foreach from=$orders key="key" item="order"}
	<tr>
		<td class="right">{$order->getId()}</td>
		<td>{$order->status}</td>
		<td class="right">{$order->getRounded('realSubtotal')}</td>
		<td class="right">{$order->getRounded('vat')}</td>
		<td class="right">{$order->getRounded('total')}</td>
		<td class="center">{$order->dtc|date_format:"%d/%m/%Y"}</td>
		<td class="center"><a href="{$this->router->link("CustomerOrdersControler::ORDER_PAGE","order_id=$key")}">View</a></td>
	<tr>
	{/foreach}
	</table>
{else}
	<p>No orders</p>
{/if}
<br /><br />
</div>
{include file="@footer.tpl"}
