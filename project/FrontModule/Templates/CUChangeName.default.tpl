{include file="@header.tpl"}

<div id="maincontent1">
    {if $visibleTitle}
        <h1>{$title}</h1>
    {/if}
    <p style="margin: 20px 0 25px 0; font-size: 11px;">
        <a href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId")}">{$companyName}</a> &gt; {$title}
    </p>
    <div style="margin-left: 0pt; width: 727px;" class="flash info2">
        <p style="padding-top: 0;">Please enter your new company name below.</p>
        <p>Please select which suffix you prefer.</p>
        <p><strong>Please double check all spelling and grammar. The name will be sent to Companies House EXACTLY as typed in.</strong></p>
        <p><em>NB. If you need to provide supporting documentation for the new name (eg. Sensitive words in the name) we will need to file paperwork forms. Please <a href="/contact-us.html">contact us</a> if this is the case.</em></p>
    </div>
    <div class="change-company-name-form">
        {* FORM BEGIN *}

        {$form->getBegin() nofilter}
        {if $form->getErrors()|@count gt 0}
            <p class="ff_err_notice">Form has <b> {$form->getErrors()|@count}</b> error(s)</p>
        {/if}

        <fieldset styel="clear: both;">
            <legend>{$companyName} Name Change</legend>
            <table class="ff_table">
                <tbody>
                    <tr>
                        <th>{$form->getLabel('newCompanyName') nofilter}</th>
                        <td>
                            {$form->getControl('newCompanyName') nofilter}
                            <span class="ff_desc">Please provide New Company Name</span>
                        </td>
                        <td>
                            <span class="ff_control_err">{$form->getError('newCompanyName')}</span>
                        </td>
                    </tr>
                    <tr>
                        <th> <label for="sufix">{$form->getLabel('radio') nofilter} </label></th>
                        <td>
                            <div class="radioSet">
                                <div style="float:left;">{$form->getControl('radio') nofilter}</div>
                                <div class="help-button" style="float:right;">
                                    <a href="#" class="help-icon">help</a>
                                    <em>This determines how your company name appears on the name change certificate. It is purely aesthetic and has no impact on your company.</em>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </td>
                        <td>

                            <span class="ff_control_err">{$form->getError('radio')}</span>
                        </td>
                    </tr>
                    <tr class="check-if-available" style="vertical-align: middle;">
                        <th>New Name with Suffix:</th>
                        <td>
                            <span class="company-name-clone"></span>
                        </td>
                        <td>
                            <a href="{$this->router->link("CUChangeNameControler::CHANGE_NAME_PAGE", "company_id=$companyId")}" class="check-avaiability" style="text-decoration: none; ">
                                <button type="button" style="cursor: pointer;">Check Name Availability</button>
                            </a>
                        </td>
                    </tr>
                    <tr class="ckeck-company" style="display: none;">
                        <th>&nbsp</th>
                        <td>
                            <span class="loading-image">
                                Loading ... Checking Company Name
                            </span>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr class="show-ckeck-company" style="display: none;">
                        <th>&nbsp</th>
                        <td>
                            <span class="isOkToShow" style="display: none">

                                <span style="padding-top:10px;float: left;">Company name is available.</span>
                                <img src="{$urlImgs}greentick.png" alt="company is available"/>
                                <div class="clear"></div>
                            </span>
                            <span class="isNotOkToShow error" style="display: none; ">
                                <span style="padding-top:10px;float: left;">Sorry, that name is unavailable. Please try a different name. </span>
                                <img src="{$urlImgs}redcross.png" alt="company isn't available"/>
                                <div class="clear"></div>
                            </span>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </tbody>
            </table>
        </fieldset>
        <fieldset id="fieldset_2">
            <legend>Action</legend>
            <table class="ff_table">
                <tbody>
                    <tr>
                        <th>&nbsp;</th>
                        <td>{$form->getControl('send') nofilter}</td>
                        <td>
                            <span class="isNotOkToSubmit"></span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </fieldset>
        {$form->getEnd() nofilter}

    </div>
</div>

{literal}
    <style type="text/css">
        .change-company-name-form {
            margin-left: 0pt;
            width: 753px;
        }
        .change-company-name-form .radioSet{
            padding-bottom: 5px;
            overflow: hidden;
        }

        .change-company-name-form .radioSet input {
            height: 15px;
            line-height:15px;
            vertical-align: top;
            margin-left:30px;
        }
        .change-company-name-form .err-submit {
            color: red;
            padding-left:10px;
            padding-top:6px;

        }
        .change-company-name-form .error-label {
            color : red;
        }

    </style>
    <script type="text/javascript">
        $(document).ready(function() {

            window.cuchangenameform = new CUChangeNameForm();

            $('.change-company-name-form #send').click(function() {
                if ($('.isNotOkToShow').css('display') == 'none' && $('.isOkToShow').css('display') == 'none') {
                    $('.isNotOkToSubmit').html('Please check company name is available before submitting').parent('td').addClass('err-submit');
                    return false;
                }
                if ($('.isNotOkToShow').is(':visible')) {
                    $('.isNotOkToSubmit').html('Please check company name is available before submitting').parent('td').addClass('err-submit');
                    return false;
                }
            });



            $('.check-avaiability').click(function() {
                $('.show-ckeck-company').hide();
                $('.isNotOkToSubmit').html('');
                //$('td').removeClass('err-submit');

                if (!validateName() || !validateSufix()) {
                    return false
                }
                ;


                var self = $(this);
                $('.ckeck-company').fadeIn('slow');
                $.ajax({
                    url: self.attr('href') + '&companyName=' + encodeURIComponent($('.company-name-clone').html()),
                    type: "GET",
                    dataType: "json",
                    success: function(data) {
                        $('.ckeck-company').hide();
                        $('.show-ckeck-company').fadeIn('slow');

                        if (data.succes == 0) {
                            $('.isNotOkToShow').show();
                        } else {
                            $('.isOkToShow').show();
                        }
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        $('.loading-image').html('Oops...an error occurred');
                    },
                    timeout: function() {
                        $('.loading-image').html('Oops...an error occurred');
                    }
                });
                return false;
            });

        });

        var CUChangeNameForm = function() {
            var self = this;
            this.getInputCompanyNameText = function() {
                return $('.change-company-name-form #newCompanyName').val().toUpperCase();
            }

            this.getCloneRadioText = function() {
                var local = '';
                (typeof ($(".change-company-name-form input[name=radio]:checked").val()) == 'undefined') ? local = '' : local = $(".change-company-name-form input[name=radio]:checked").val();
                return local;
            }

            this.hideOkToShow = function() {
                $('.isNotOkToShow').fadeOut('slow');
                $('.isOkToShow').fadeOut('slow');
            }

            $('.change-company-name-form span.company-name-clone').html(self.getInputCompanyNameText() + ' ' + self.getCloneRadioText());

            $('.change-company-name-form #newCompanyName').keyup(function() {
                var htm = $(this);
                self.hideOkToShow();
                $('.change-company-name-form span.company-name-clone').html(htm.val().toUpperCase() + ' ' + self.getCloneRadioText());
            });

            $(".change-company-name-form input[name=radio]:checked").live('click', function() {
                self.hideOkToShow();
                $('.change-company-name-form span.company-name-clone').html(self.getInputCompanyNameText() + ' ' + $(this).val());
            });
        }

        var validateName = function() {
            var name = $('#newCompanyName');
            //if it's NOT valid
            if (name.val().length < 4) {
                $("label[for='newCompanyName']").addClass("error-label");
                $($('#newCompanyName').parent().next()).find('.ff_control_err').html('Name can\'t be empty');
                return false;

            }
            $("label[for='newCompanyName']").removeClass("error-label");
            $($('#newCompanyName').parent().next()).find('.ff_control_err').html('');
            return true;
        }
    {/literal}
        var companyType = '{$company->getType()}';
    {literal}
        var validateSufix = function() {

            if (companyType != "BYGUAREXUNDSEC60" && !$('input[name=radio]:checked', '.change-company-name-form form').val()) {
                $("label[for='sufix']").addClass("error-label");
                $($('.radioSet').parent().next()).find('.ff_control_err').html('Choose a suffix');
                return false;
            }

            $("label[for='sufix']").removeClass("error-label");
            $($('.radioSet').parent().next()).find('.ff_control_err').html('');
            return true;
        }


    </script>
{/literal}
{include file="@footer.tpl"}
