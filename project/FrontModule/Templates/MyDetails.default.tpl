{include file="@header.tpl"}
{literal}
    <style>
        #fieldset_3 span.ff_desc {
            padding-top: 10px;
            color: gray;
        }
        .error {
            color: #ff0000;
            font-style: italic;
            font-weight: normal;
            padding: 1px 10px 1px 10px;
            font-size: 11px;
        }
    </style>
{/literal}
{if $hasWholesaleQuestion}
    {literal}
    <style>
        #fieldset_4 {
        background:url(/front/imgs/ICAEW.png) #f2f3f5 right no-repeat;
        }
    </style>
    {/literal}
{/if}
<div id="maincontent1">
	<div>
		<div>
			<div>
				{if $visibleTitle}
                <h1>{$title}</h1>
                {/if}
                <p class="margin-bottom-20">Edit your personal and account details:</p>

				{* FINISH REGISTRATION MESSAGE *}
				{if $finishedRegistration == false}
					<div class="flash info2" style="margin: 0 0 20px 0; width: 530px">Please complete your registration details to access your account.</div>
				{/if}
				{if $finishedRegistration == true && $hasSubscribed == Customer::SUBSCRIBE_DEFAULT}
					<div class="flash info2" style="margin: 0 0 20px 0; width: 530px">In order for us to fully process your order, please finish your registration details.</div>
				{/if}
				{* DISPLAY FORM *} 
				{$form nofilter}
				
				<p><span class="orange">*</span> required fields</p>
			</div>
		</div>
	</div>
</div>
 
 {if $hasWholesaleQuestion}
{literal}
    <script type="text/javascript">
    <!--
    $(document).ready(function() {
        $('#fieldset_4').hide();
        if($('#userTypewholesale').is(':checked')){
            $('#fieldset_4').show();
            
        }

        $('.userType').click(function () {
            if ($(this).val() == 'wholesale') {
                $('#fieldset_4').show();
                $('form[name="registration"]').validate({
                errorPlacement: function(error, element) {
                error.insertBefore( element.parent("td").next("td") );
                },
                rules: {memberICAEW: {required: true}}});
        
            }else{
                $('#fieldset_4').hide(); 
                $('.memberICAEW').attr('checked', false);
            }
        });
    });
    //-->
    </script>
{/literal}
{/if}
{include file='@blocks/rightColumn.tpl'}
{include file="@footer.tpl"}
