{include file="@header.tpl"}

<div id="maincontent2">
	
	{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
	
	{* FORM *}
	{$form nofilter}
	
</div>
	
{include file="@footer.tpl"}