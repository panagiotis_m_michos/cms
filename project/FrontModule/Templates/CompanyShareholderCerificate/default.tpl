{include file="@header.tpl"}
<div id="maincontent2">
    {if $visibleTitle}
    <h1>{$title}</h1>
    {/if}
    <table class="grid2" width="780">
        <col width="200">
        <col width="500">
        <tr>
            <th colspan="2" class="center">Shareholder</th>
        </tr>

        {if $share->shareholderInfo->corporate}
            <tr>
                <th>Corporate Nme</th>
                <td>{$share->shareholderInfo->corporate_name}</td>
            </tr>
        {else}
            <tr>
                <th>Title</th>
                <td>{if isset($share->shareholderInfo->title)} {$share->shareholderInfo->title} {/if}</td>
            </tr>
            <tr>
                <th>First name</th>
                <td>{$share->shareholderInfo->forename}</td>
            </tr>
            <tr>
                <th>Last name</th>
                <td>{$share->shareholderInfo->surname}</td>
            </tr>
        {/if}
        <tr>
            <th colspan="2" class="center">Shares</th>
        </tr>
        <tr>
            <th>Currency</th>
            <td>{$share->shareholderInfo->currency}</td>
        </tr>
        <tr>
            <th>Share class</th>
            <td>{$share->shareholderInfo->MemberShare}</td>
        </tr>
        <tr>
            <th>Number of shares</th>
            <td>{$share->shareholderInfo->MemberNrShares}</td>
        </tr>
        <tr>
            <th>Share value</th>
            <td>{$share->shareholderInfo->share_value}</td>
        </tr>

    </table>
    <br />
    <div class="flash info2" style="margin-left: 0pt; width: 753px; ">
        Please enter the shareholder's address below and we'll automatically produce their share certificate.
    </div>

    {$form nofilter}

</div>
{include file="@footer.tpl"}
