{include file="@headerPopUp.tpl"}
{literal}
<style type="text/css">
div#maincontent1{
width: 500px;
}
.box-white2{
width: 365px;
}
li.greentick32 {
	list-style-type: none;
	background: url('front/imgs/green_tick.png') no-repeat top left;
	line-height: 32px;
	padding-left: 40px;
	font-size: 14px;
	padding-bottom: 10px;
}
ul.ro-stat {
	padding: 0;
    margin:0;
}
.green_button_buy {
	padding: 10px;
	height: 55px;
	width: 300px;
	background: url("front/imgs/oneclick_button.png") no-repeat scroll left top transparent;
}
.green_button_buy a {
    text-decoration: none;
}
.green_button_buy h3 {
    margin: 3px 0;
    text-align: center;
    color: #fff;
    font-size: 20px;
}
.green_button_buy p {
    margin-top: 4px;
    text-align: center;
    color: #fff;
    font-size: 14px;
	padding-top: 0;
}
</style>
{/literal}
	<h2 style="color: #000; text-align: center; ">{$title}</h2>
	<div style="margin: 0 auto;width: 400px;">
		<ul class="ro-stat">
			<li class="greentick32">&pound;{$product->associatedPrice} (+VAT) will be charged to your Credit/Debit Card</li>
			<li class="greentick32">Instant access to Registered Office Service</li>
			<li class="greentick32">Purchase is completed with a single click!</li>
		</ul>
        <div style=" margin: 0 0 0 10px;">
            <div style="float: left;width: 250px;"><p>Registered Office Service</p></div>
            <div style="float: left"><p>&pound;{$product->price}</p></div>
            <div style="float: left;width: 250px;"><p>Less: Discount</p></div>
            {assign var="key3" value=$product->price-$product->associatedPrice}
            <div style="float: left; text-decoration:underline"><p>&pound;{$key3}</p></div>
            <div style="float: left;width: 250px;"><p>Net price</p></div>
            <div style="float: left"><p>&pound;{$product->associatedPrice}</p></div>

            <div style="float: left; width: 250px;"><p>VAT</p></div>
            <div style="float: left; text-decoration:underline"><p>&pound;{$product_vat}</p></div>

            <div style="float: left; width: 250px;"><p style="font-weight: bold;">Total</p></div>
            <div style="float: left;"><p style="font-weight: bold;">&pound;{$product_gross}</p></div>
            <div style="clear: both;"></div>
        </div>
        <div style="clear: both;"></div>

		<form id="token_form" action="{$this->router->link("TokensControler::TOKEN")}" method="get" class="txtcenter" style="margin: 20px 0;">
			{ui name="button" size="s" text="Buy Instantly*" loading="expand-right"}
			<input type="hidden" name="add" value="{$product->nodeId}">
			<input type="hidden" name="company" value="{$companyId}">
		</form>

		{* Alberto: can be deleted when you are finished with styling *}
		{*<div class="green_button_buy">
            {assign var="productId" value=$product->nodeId}
			<a href="{$this->router->link("TokensControler::TOKEN", "add=$productId", "company=$companyId")}" onclick="_gaq.push(['_trackEvent', 'Links', 'Click', 'Buy Registered Office Service']);">
				<h3>Buy Instantly</h3>
				<p>(One-Click Ordering)</p>
			</a>
		</div>*}

		<div>
			<div style="float: right;">
				<img src="{$urlImgs}sagepay-logo.png" alt="Sagepay" width="80" height="18" />
			</div>
			<div style="float: left;">
				<p style="padding-top: 0; font-size: 12px; color: #666;">*One-Click Ordering using card ending {$token->cardNumber}</p>
			</div>
		</div>
		<div style="clear: both;"></div>
        <p style=" margin-top: 2px; width: 350px;color: #666;">Your company will automatically use Companies Made Simple's address as its Registered Office once payment is completed.</p>
	</div>

    {include file="@footerPopUp.tpl"}
