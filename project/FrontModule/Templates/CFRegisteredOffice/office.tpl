{include file="@header.tpl"}

{* JS ADDRESSES FOR PREFILL *}
<script type="text/javascript">
    var addresses = {$jsAdresses nofilter};
</script>

{literal}
<style>        
    .registeredofficeservice {
        margin-bottom: 40px;
        background-color: #f7f7f7;
        border: 1px solid #ddd;
        padding: 20px;
    }
    .registeredofficeservice h2 {
        color: #000;
        font-size: 18px;
        margin-bottom: 0;
        margin-top: 0;
    }
    .registeredofficeservice p {
        font-size: 14px;
        line-height: 20px;
        padding-top: 10px;
    }

    .roleftcol {
        width: 510px;
        float: left;
    }
    .roimage {
        float: right;
        margin-left: 20px;
        width: 400px;
        text-align: center;
    }
    .roimage p {
        font-size: 13px;
        line-height: 19px;
    }
    .roimage span {
        color: #666;
    }
    .green_button_block {
/*        padding: 10px;*/
        height: 53px;
        width: 532px;
        background: url("front/imgs/green-btn.png") no-repeat scroll left top transparent;
    }
    .green_button_block a {
        text-decoration: none;
    }
    .green_button_block h3 {
        margin: 3px 0;
        text-align: center;
        color: #fff;
        font-size: 20px;
        padding-top: 11px;
    }
    .green_button_block p {
        margin-top: 4px;
        text-align: center;
        color: #fff;
        font-size: 14px;
        padding-top: 0;
    }
    .roleftcol ul {
        padding-left: 0;
        margin-top: 20px;
    }
    .roleftcol li.greentick32 {
        list-style-type: none;
        background: url('front/imgs/green_tick.png') no-repeat top left;
        height: 32px;
        line-height: 32px;
        padding-left: 40px;
        font-size: 14px;
        padding-bottom: 10px;
    }
</style>
<script type="text/javascript">

    $(document).ready(function () {

        $('.popupnew1').click(function(){
            var url = $(this).attr('href');
            popoverlay1(url);
            return false;    
        });
        
        $('.green_button_block a').click(function(){
            var url = $(this).attr('href');
            popoverlay2(url);
            return false;    
        });
    
    
        function popoverlay1(url){
            $overlay = $('<div class="payment_register_office" ><iframe id="overlay_iframe" src="' + url + '" style="margin-left: 20px; margin-top: 10px; " frameborder="0" width="500" height="410"></iframe></div>');
            $('body').append($overlay);
            $overlay.overlay({
                effect: 'apple',
                load: true,
                left:"center",
                mask: '#000',
                oneInstance: false, 
                api : true,
                top : "0px"
            });
        };
        
        function popoverlay2(url){
            $overlay = $('<div class="payment_overlay" ><iframe id="overlay_iframe" src="' + url + '" style="margin-left: 20px; margin-top: 10px; " frameborder="0" width="530" height="460"></iframe></div>');
            $('body').append($overlay);
            $overlay.overlay({
                effect: 'apple',
                load: true,
                left:"center",
                mask: '#000',
                oneInstance: false, 
                api : true,
                top : "0px",
                height: "500px",
                onClose: function(){
                    window.location.reload(true);
                },
                onBeforeLoad: function() {
                    // grab wrapper element inside content
                    var wrap = this.getOverlay().find(".contentWrap");

                    // load the page specified in the trigger
                    wrap.attr('src', this.getTrigger().attr("href"));
                }

            });
        };
        // start on beginning
        ourRegisterOfficeHandler();
	
        // called on type click
        $("input[name='ourRegisteredOffice']").click(function () {		
            ourRegisterOfficeHandler(); 
        });
	
	
        /**
         * Provides disable and enable dropdown for our offices
         * @return void
         */
        function ourRegisterOfficeHandler()
        {
            var disable = $("#ourRegisteredOffice").is(":checked");
            $('#prefill').attr("disabled", disable);
            $('#premise').attr("disabled", disable);
            $('#street').attr("disabled", disable);
            $('#thoroughfare').attr("disabled", disable);
            $('#post_town').attr("disabled", disable);
            $('#postcode').attr("disabled", disable);
            $('#county').attr("disabled", disable);
            $('#country').attr("disabled", disable);
            if(disable == true){
                $("#fieldset_1, #fieldset_2").hide();
            }else{
                $("#fieldset_1, #fieldset_2").show();
            }
        }
	
        // prefill address	
        $("#prefill").change(function () {
            var value = $("#prefill").val();
            address = addresses[value];
            for (var name in address) {
                $('#'+name).val(address[name]);
            }
        });
	
    });
</script>
{/literal}

<div class="formprocesstitle">
    {if $visibleTitle}
    <h1>{$title}</h1>
    {/if}

    {*ERRORS*}
    {$form->getBegin() nofilter}
    {if $form->getErrors()|@count gt 0}
    <p class="ff_err_notice ff_red_err" style="width: 940px">Form has <b> {$form->getErrors()|@count}</b> error(s). See below for more details:</p>
    {/if} 

</div>

{* <div class="livesuprt"><script language="javascript" src="https://support.madesimplegroup.com/visitor/index.php?_m=livesupport&_a=htmlcode&departmentid=2"></script></div> *}


{* TABS *}
{include file="@blocks/navlist.tpl" currentTab=$this->action}

{* GUIDE *}
<div>
    {if $one_click_register_office == 1}
    <div class="registeredofficeservice">
        <h2 style="font-size:30px;">One-Time Offer: Get 55% off Registered Office Service</h2>
        <h2 style="font-weight: normal;margin-bottom: 15px;font-size: 18px;">We're offering new customers our Registered Office Service for just &pound;{$product->associatedPrice} (usually &pound;{$product->price})</h2>

        <div class="roimage">
            <img width="400" height="300" alt="Companies Made Simple Offices" src="{$urlImgs}register_office_photo.jpg" style="border: 2px solid #ddd;">
            <p><strong>Protect your privacy by using our prestigious London address as your Registered Office.</strong><br><span>(20-22 Wenlock Road, London, N1 7GU)</span></p>
            <p style="text-align: right; margin-top: 40px; margin-right: 30px; font-size: 14px;"><strong>Questions? Call our experts on 0207 608 5500</strong><br>Mon to Fri - 9am to 5.30pm</p>
        </div>
        <div class="roleftcol">
            <div class="green_button_block">
                <a onclick="_gaq.push(['_trackEvent', 'Links', 'Click', 'Add Registered Office Service Top']);" href="{$this->router->link("CFRegisteredOfficeControler::REGISTER_OFFICE_ONECLICK_PAGE", "company_id=$companyId", "noBank=1", "popup=1")}">
                   <h3>Add Registered Office Service</h3>

                </a>
            </div>
            <div>
                <div style="float: right;margin-top: 3px;">
                    <img width="100" height="23" src="{$urlImgs}sagepay-logo.png" alt="Sagepay">
                </div>
                <div style="float: left;">
                    <p style="padding-top: 0; font-size: 14px; color: #000; font-weight: bold;">Easy one-click payment using card ending {$token->cardNumber}</p>
                    <p style="font-size: 12px;  padding-top: 0; color: #666;">You'll confirm your purchase on the next screen.</p>
                </div>
            </div>
            <div style="clear: both;"></div>
            <h2 style="margin-top: 15px;">What is the Registered Office service?</h2>
            <p>As Registered Office addresses are made public, this service allows you to use our prestigious address as you company's Registered Office instead of your own.</p>

            <h2 style="margin-top: 15px; ">Top 5 reasons to use the Registered Office service</h2>
            <ul style="margin-top: 15px;">
                <li class="greentick32">Protect the <strong>privacy of your home address</strong></li>
                <li class="greentick32"><strong>Block Junk Mail</strong> to your address</li>
                <li class="greentick32"><strong>Look more established</strong> by having a prestigious Registered Office</li>
                <li class="greentick32"><strong>FREE mail forwarding</strong> of <a href="{$this->router->link("CFRegisteredOfficeControler::REGISTER_OFFICE_MAIL", "company_id=$companyId", "noBank=1", "popup=1")}" class="popupnew1">Official Mail</a></li>
                <li class="greentick32"><strong>Over 57,000 companies</strong> have used our service <strong>since 2002</strong></li>

            </ul>
            <h2>Why are we making this offer?</h2><p>As we are expanding our Registered Office service, we're passing cost savings on to our customers through this offer.</p>
            <p style="font-weight:bold;font-size:18px;padding: 15px 0 2px 0;">Protect your privacy for just &pound;{$product->associatedPrice}  
                <span style="font-size: 10px;">+VAT</span> (usually &pound;{$product->price} <span style="font-size: 10px;">+VAT</span>) *
            </p>
            <div class="green_button_block">
                <a onclick="_gaq.push(['_trackEvent', 'Links', 'Click', 'Add Registered Office Service Bottom']);" href="{$this->router->link("CFRegisteredOfficeControler::REGISTER_OFFICE_ONECLICK_PAGE", "company_id=$companyId", "noBank=1", "popup=1")}">
                   <h3>Add Registered Office Service</h3>
                </a>
            </div>
            <div>
                <div style="float: right;margin-top: 3px;">
                    <img width="100" height="23" src="{$urlImgs}sagepay-logo.png" alt="Sagepay">
                </div>
                <div style="float: left;">
                    <p style="padding-top: 0; font-size: 14px; color: #000; font-weight: bold;">Easy one-click payment using card ending {$token->cardNumber}</p>
                    <p style="font-size: 12px;  padding-top: 0; color: #666;">You'll confirm your purchase on the next screen.</p>
                </div>
            </div>
            <div style="clear: both;"></div>
            <p style="font-size: 12px; color: #666; line-height: 18px; text-align: center;">* You'll be billed only &pound;{$product->associatedPrice} +VAT for the first year of service. After the first year, if you choose to continue the sevice, you will be charged the standard annual rate (currently &pound;{$product->price} +VAT).</p>
            <p style="font-size: 14px; margin-top: 20px; color: #666; padding-left: 55px;text-align: center;width: 420px;">
                <a href="#nothanks"><strong>No thanks, I'm happy to use my own address</strong></a>
                <br> I understand my Registered Office will be made public information and I won't be shown this offer again!
            </p>
            <div style="clear: both;"></div>

        </div>
        <div style="clear: both;"></div>
    </div>
    {/if} 
    <div style="border:0px solid blue;float: left">
        <div style="width: 500px;">
            <a name="nothanks"></a>
            {if $form->canShowFirstQuestion() || $form->canShowSecondQuestion()}
            <fieldset id="fieldset_1343">
                <legend>Your Details</legend>
                <table class="ff_table">
                    <tbody>
                        {if $form->canShowFirstQuestion()}
                            <tr>
                                <th>{$form->getLabel('answer') nofilter}</th>
                                <td id="questionAnswer">{$form->getControl('answer') nofilter}</td>
                                <td>
                                    <div class="help-button">
                                        <a href="javascript:;" class="help-icon">help</a>
                                        <em>So that we can better understand your needs and continuously improve your experience with us, please tell us why you are forming this company.</em>
                                    </div>
                                    <span class="redmsg">{$form->getError('answer')}</span>
                                </td>
                            </tr>
                            <tr id="additionalAnswer" class="hidden" data-show-on-option="{$optionTrigger}">
                                <th>{$form->getLabel('additionalAnswer') nofilter}</th>
                                <td>{$form->getControl('additionalAnswer') nofilter}</td>
                            </tr>
                        {/if}

                        {if $form->canShowSecondQuestion()}
                            <tr>
                                <th>{$form->getLabel('tradingStart') nofilter}</th>
                                <td id="questionAnswer">{$form->getControl('tradingStart') nofilter}</td>
                                <td>
                                    <div class="help-button">
                                        <a href="javascript:;" class="help-icon">help</a>
                                        <em>So that we can better understand your needs and continuously improve your experience with us, please tell us when you are looking to trade with this company.</em>
                                    </div>
                                    <span class="redmsg">{$form->getError('tradingStart')}</span>
                                </td>
                            </tr>
                        {/if}
                    </tbody>
                </table>
            </fieldset>
            <script type="text/javascript">
                var elementToShow = $('#additionalAnswer');
                $("#questionAnswer select").change(function() {
                    if ($(this).find(':selected').val() === elementToShow.attr('data-show-on-option')) {
                        elementToShow.show();
                    } else {
                        elementToShow.hide();
                    }
                });
            </script>
            {/if}
            {if isset($form.ourRegisteredOffice)}
            <fieldset id="fieldset_0">
                <legend>Use Our Registered office service</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <th>{$form->getLabel('ourRegisteredOffice') nofilter}</th>
                            <td>{$form->getControl('ourRegisteredOffice') nofilter}</td>
                            <td>20-22 Wenlock Road, London, N1 7GU</td>
                        </tr>
                        <tr>
                            <td colspan="3"> 
                                <ul style="margin: 0px; color: #999999;">
                                    <li><i>Your address will remain private; ours will show on the public register.</i></li>
                                    <li><i>Prevent junk mail; only receive your company's statutory mail.</i></li>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>
            {/if}
            <fieldset id="fieldset_1">
                <legend>Prefill Address</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <th>{$form->getLabel('prefill') nofilter}</th>
                            <td>{$form->getControl('prefill') nofilter}</td>
                            <td>
                                <div class="help-button">
                                    <a href="#" class="help-icon">help</a>
                                    <em>You can use this prefill option to select an address you&#39;ve entered previously.</em>
                                </div>
                                {$form->getError('prefill')}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>

            <fieldset id="fieldset_2">
                <legend>Address</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <th>{$form->getLabel('premise') nofilter}</th>
                            <td>{$form->getControl('premise') nofilter}</td>
                            <td>
                                <div class="help-button">
                                    <a href="#" class="help-icon">help</a>
                                    <em>Eg. "28A" or "Flat 2, 36" or "Crusader House"</em>
                                </div>
                                <span class="redmsg">{$form->getError('premise')}</span>
                            </td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('street') nofilter}</th>
                            <td>{$form->getControl('street') nofilter}</td>
                            <td>
                                <div class="help-button">
                                    <a href="#" class="help-icon">help</a>
                                    <em>Street refers to the actual street, road, lane etc.</em>
                                </div>
                                <span class="redmsg">{$form->getError('street')}</span>
                            </td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('thoroughfare') nofilter}</th>
                            <td>{$form->getControl('thoroughfare') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('thoroughfare')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('post_town') nofilter}</th>
                            <td>{$form->getControl('post_town') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('post_town')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('county') nofilter}</th>
                            <td>{$form->getControl('county') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('county')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('postcode') nofilter}</th>
                            <td>{$form->getControl('postcode') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('postcode')}</span></td>
                        </tr>
                        <tr>
                            <th>{$form->getLabel('country') nofilter}</th>
                            <td>{$form->getControl('country') nofilter}</td>
                            <td><span class="redmsg">{$form->getError('country')}</span></td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>

            <fieldset id="fieldset_3">
                <legend>Action</legend>
                <table class="ff_table">
                    <tbody>
                        <tr>
                            <th>&nbsp;</th>
                            <td>{$form->getControl('continue') nofilter}</td>
                            <td>&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>

            {$form->getEnd() nofilter}
        </div>
    </div>    
    <div style="float: right; padding-right: 15px; margin-top:6px;">
        <div class="box1 fright" style="margin-bottom: 10px;">	
            <h3>Guide:</h3>
            <div class="box1-foot pbottom15">
                <p>This address will be used for statutory mail from Companies House and HM Revenue &amp; Customs.</p>
                <p>Your registered office is required to be in England & Wales, Scotland or Northern Ireland.</p>
                <p>The location of the registered office at incorporation determines where the company is registered. Eg. If registered in England the registered office can never be changed to Scotland or Northern Ireland.</p>
            </div>
        </div>
        <div class="clear"></div>

        {* HTML INFO BOX *}
        {include file="@blocks/CFHtmlBox.tpl"} 

    </div>
    <div class="clear"></div>
</div>

{include file="@footer.tpl"}
