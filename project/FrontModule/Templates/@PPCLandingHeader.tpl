<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="cache-control"  content="no-cache" />
        <meta http-equiv="pragma"  content="no-cache" />
        <meta http-equiv="expires"  content="-1" />
        <meta http-equiv="content-language"  content="{$currLng}" />
        <meta name="robots" content="noindex" />
        <meta name="description" content="{$description}" />
        <meta name="keywords" content="{$keywords}" />
        <title>{$seoTitle}</title>
        <link rel="stylesheet" type="text/css" href="{$urlCss}ppc-landing.css" />
        <script type="text/javascript" src="{$urlComponent}jquery/dist/jquery.min.js" ></script>
        <script type="text/javascript" src="{$urlComponent}jquery-migrate/jquery-migrate.min.js" ></script>
        <script type="text/javascript" src="{$urlJs}jquery.tools.min.js"></script>
        <script language="JavaScript" src="{$urlJs}project.js" type="text/javascript"></script>
        <script type="text/javascript" src="//apis.google.com/js/plusone.js"></script>
        <!-- Optimizely code -->
        <script src="//cdn.optimizely.com/js/138942235.js"></script>
        <!-- Analytics code -->
        {literal}
        <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-543198-9']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
        </script>
        {/literal}
    </head>
    <body>
        <div id="wrapper">
            <div id="header">
                <div id="headerTop">
                    <span><a id="msglogo" href="/"></a></span>
                    <span id="facebook">
                        <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FMadeSimpleGroup&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;" allowTransparency="true"></iframe>
                    </span>
                    <span id="googleplus">
                        <div class="g-plusone" data-size="medium" data-href="/"></div>
                    </span>
                    <div id="contact">
                        <span class="number">0207 608 5500</span>
                        <span class="open">Monday - Friday 9am - 5.30pm</span>
                    </div>
                    <ul>
                        <li><a href="../company-formation-name-search.html">Our Packages</a></li>
                        <li><a href="http://support.companiesmadesimple.com/article/288-contacting-us">Contact Us</a></li>
                        <li><a href="../login.html">Login</a></li>
                    </ul>
                </div>
