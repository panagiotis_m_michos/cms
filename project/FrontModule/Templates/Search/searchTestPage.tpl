{include file="@header.tpl"}

{if $disabledLeftColumn} 
    <div id="slidercontainer" class="closeContainer">
	      <div id="slider" class="closed wizslider">
		  	   <div id="closebutton" class="closebutton">
			   		<img src="{$urlImgs}close.png" />
			   </div>
    	  	   <div id="helpbutton" >
    		   	  	<img src="{$urlImgs}help.png" />
    		   </div>
               <div id="question0" class="question" >
                   <div class="q0text" style="width: 350px; text-align: center; margin-top: 40px; margin-left: 40px;">
                   <b>Let's find the package that's right for you...</b> <br /> <br />   
                   <img src="{$urlImgs}loading.gif" />
                   </div>
                    
               </div>
			   <!--Question1-->
			   <div class="question" id="question1">
    			   <div class="question_text">
    					Are you planning to open a bank account for your company?
    			   </div>	
    			   <div class="yesno_container">
                       <div class="check_yes">
                           <input class="real_check Yes Q1" id="Q1CheckYes" type="radio" name="Q1" />
							 <label for="Q1CheckYes"><span>Yes</span></label>
    		<!--<label for="Q1CheckYes"><div class="check_box unticked"></div></label>-->
    					</div>
    					<div class="check_no">
    						 <input class="real_check No Q1" id="Q1CheckNo" type="radio" name="Q1" />
							 <label for="Q1CheckNo"><span>No</span></label>
    		<!--<label for="Q1CheckNo"><div class="check_box unticked"></div></label>-->
    					</div>
                       <div class="clear"></div>
    			   </div>
				   <div id="qno1" class="qnumber">
				   		Question 1 of 5
				   </div>
			   </div>
			   <!--Question2-->
			   <div class="question" id="question2">
    			   <div class="question_text">
    					Do you have a UK address for your company?
    			   </div>	
    			   <div class="yesno_container">
    					<div class="check_yes">
    						 <input class="real_check Yes Q2" id="Q2CheckYes" type="radio" name="Q2" />
							 <label for="Q2CheckYes"><span>Yes</span></label>
    		<!--<label for="Q1CheckYes"><div class="check_box unticked"></div></label>-->
    					</div>
    					<div class="check_no">
    						 <input class="real_check No Q2" id="Q2CheckNo" type="radio" name="Q2" />
							 <label for="Q2CheckNo"><span>No</span></label>
    		<!--<label for="Q1CheckNo"><div class="check_box unticked"></div></label>-->
    					</div>
                       <div class="clear"></div>
    			   </div>
				   <div id="qno2" class="qnumber">
				   		Question 2 of 5
				   </div>
			   </div>
			   
			   <!--Question3-->
			   <div class="question" id="question3">
    			   <div class="question_text">
    					Will your company operate from a home address?
    			   </div>	
    			   <div class="yesno_container">
    					<div class="check_yes">
    						 <input class="real_check Yes Q3" id="Q3CheckYes" type="radio" name="Q3" />
							 <label for="Q3CheckYes"><span>Yes</span></label>
    		<!--<label for="Q1CheckYes"><div class="check_box unticked"></div></label>-->
    					</div>
    					<div class="check_no">
    						 <input class="real_check No Q3" id="Q3CheckNo" type="radio" name="Q3" />
							 <label for="Q3CheckNo"><span>No</span></label>
    		<!--<label for="Q1CheckNo"><div class="check_box unticked"></div></label>-->
    					</div>
                       <div class="clear"></div>
    			   </div>
				   <div id="qno3" class="qnumber">
				   		Question 3 of 5
				   </div>
			   </div>
			   
			  <!--Question4-->
			   <div class="question" id="question4">
    			   <div class="question_text">
    					Does your company have an Accountant?
    			   </div>	
    			   <div class="yesno_container">
    					<div class="check_yes">
    						 <input class="real_check Yes Q4" id="Q4CheckYes" type="radio" value="1" class="yes" name="Q4" />
							 <label for="Q4CheckYes"><span>Yes</span></label>
    		<!--<label for="Q1CheckYes"><div class="check_box unticked"></div></label>-->
    					</div>
    					<div class="check_no">
    						 <input class="real_check No Q4" id="Q4CheckNo" type="radio" value="1" class="no" name="Q4" />
							 <label for="Q4CheckNo"><span>No</span></label>
    		<!--<label for="Q1CheckNo"><div class="check_box unticked"></div></label>-->
    					</div>
                       <div class="clear"></div>
    			   </div>
				   <div id="qno4" class="qnumber">
				   		Question 4 of 5
				   </div>
			   </div>
              
              <!--Question5-->
			   <div class="question" id="question5">
    			   <div class="question_text">
    					Would you like a prestigious London virtual business address?
    			   </div>
    			   <div class="yesno_container">
    					<div class="check_yes">
    						 <input class="real_check Yes Q5" id="Q5CheckYes" type="radio" value="1" class="yes" name="Q5" />
							 <label for="Q5CheckYes"><span>Yes</span></label>
    		<!--<label for="Q1CheckYes"><div class="check_box unticked"></div></label>-->
    					</div>
    					<div class="check_no">
    						 <input class="real_check No Q5" id="Q5CheckNo" type="radio" value="1" class="no" name="Q5" />
							 <label for="Q5CheckNo"><span>No</span></label>
    		<!--<label for="Q1CheckNo"><div class="check_box unticked"></div></label>-->
    					</div>
                       <div class="clear"></div>
    			   </div>
				   <div id="qno5" class="qnumber">
				   		Question 5 of 5
				   </div>
			   </div>		   
			   <!--Package Recommendation-->
			   <div class="question" id="recommend">
                                <div id="package_title">We recommend the  <span id="package_name"></span> package because of the…</div>
                                <div id="package_text">
                        
                    </div>
					<!--<div id="package_price">
						 <div style="float:left; padding:0 10px">
						 	  <div id="package_value">Value: <span style="text-decoration:line-through;">&pound;</span><span id="package_value_text" style="text-decoration:line-through;"></span></div>
                         	  <div id="package_saving">You Save: <span style="color: #e41d1d;font-weight: normal;">&pound;</span><span id="package_saving_text" style="color: #e41d1d;font-weight: normal;"></span></div>
						 </div>
						 <div id="package_price">Price: <span style="color:#e41d1d; font-size: 17px;">&pound;</span><span id="package_price_text" style="color:#e41d1d; font-size: 17px;"></span> <span style="font-size: 10px;">+VAT</span></div>
					</div>-->
					
					<div id="startagain" class="qnumber" style="font-size:14px;">
				   		Start Again
				   </div>
			   </div>
			   
			   <!--Back/Next Buttons-->
			   <div id="buy_now" style="display: none;">Take a Look</div>
               <div id="back">
					<span>Back</span>
			   </div>
			   <div id="next">
					<span style="color:white;">Next</span>
			   </div>
			   
    	  </div> 
	  </div>
    <!--clearselection-->
		
    <div class="new-matrix-search" style="height: auto;">
        
        <div class="search-result-name">
            <div class="result-name-text">
                {if empty($companyName) || $companyName|h == "CHANGE YOUR COMPANY NAME LTD"}
                    New text!
                {else}
                    <div style="float: left;"> <p>Good News!</p> </div><div class="result-name-tick"></div>
                    <div class="clear"></div>
                    <p><strong>{$companyName|h  nofilter}</strong> is available! </p>
                    <p>To claim it, select a package &amp; sign up today!</p>    
                {/if}
                <div style="padding-top: 10px; font-size: 16px; ">
                    <img src="{$urlImgs}wizard-button.png" class="help-to-choose"/>
                </div>
            </div>
           {* <div class="result-name-time">
                <p class="same-day-txt">Time left for SAME-DAY registration</p>
                <p class="same-day-time" id="countdownSecs">Time</p>
            </div> *}
             <div class="result-name-time-off" style="float: right;margin-right: 30px;">
                 <p style="">Why use Companies Made Simple</p>
                 <div style="margin-top: 15px;" class="why-use-cms-text">We've registered over 250,000 companies since 2002</div>
                 <div class="separator-li">&nbsp;</div>
                 <div class="why-use-cms-text">Approved Partner of the Institute of Chartered Accountants in England &amp; Wales</div>
                 <div class="separator-li">&nbsp;</div>
                 <div  class="why-use-cms-text">95% of customers rate our service as excellent</div>
                 <div style="color: #666; text-align: right;padding-right: 16px;margin-top: 10px;">
                    <a href="{$this->router->link("SearchControler::BYGUAR_PAKAGE_PAGE")}">Non-profit</a>, 
                    <a href="{$this->router->link("SearchControler::LLP_PAKAGE_PAGE")}">LLP</a> and 
                    <a href="{$this->router->link("SearchControler::SOLE_TRADER_START_UP_PAGE")}">Sole Trader</a> packages
                </div>
            </div>
            <div class="clear"></div>
        </div>

        {include file="@blocks/matrix_silver_new.tpl"} 
    </div>

{else}
    <div id="maincontent2">

        <h1>Company Formation Packages</h1>

        <div class="progressbar step2"></div>

        <p class="midfont" style="font-size: 23px;">Congratulations <strong>{$companyName|h  nofilter}</strong> is available</p>



        {if !empty($reserveWords)}
            <div class="flash error" style="margin: 15px 0 30px 0; width: 750px;">
                <h3 style="color: black; margin: 0; padding: 0;">Warning! You have used a reserved word(s) in your company name.</h3>
                {foreach from=$reserveWords item="reserveWord"} 
                    <div style="margin: 20px 0 0px 0;">{$reserveWord->description}</div>
                {/foreach}
            </div>
        {/if}

        {include file="@blocks/matrix_silver.tpl" showBuyLink=true}

    </div>
{/if}   

{include file="@footer.tpl"}
