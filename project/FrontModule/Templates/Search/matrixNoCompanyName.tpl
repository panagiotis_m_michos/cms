{include file="@header.tpl"}

    <div class="new-matrix-search" style="height: auto;">
        <div class="search-result-name">
            <div class="result-name-text">
                <div style="float: left;"> <p style="font-weight: bold; font-size: 22px;">Select a package & sign up today!</p> </div>
            </div>
             <div class="result-name-time-off" style="float: right;margin-right: 30px;">
                 <p style="font-weight: normal;">Why use Companies Made Simple</p>
                 <div style="margin-top: 15px;" class="why-use-cms-text">We've registered over 250,000 companies since 2002</div>
                 <div class="separator-li">&nbsp;</div>
                 <div class="why-use-cms-text">Approved Partner of the Institute of Chartered Accountants in England & Wales</div>
                 <div class="separator-li">&nbsp;</div>
                 <div  class="why-use-cms-text">95% of customers rate our service as excellent</div>
                 <div style="color: #666; text-align: right;padding-right: 16px;margin-top: 10px;">
                    <a href="{$this->router->link("SearchControler::BYGUAR_PAKAGE_PAGE")}">Non-profit</a>, 
                    <a href="{$this->router->link("SearchControler::LLP_PAKAGE_PAGE")}">LLP</a> and 
                    <a href="{$this->router->link("SearchControler::SOLE_TRADER_START_UP_PAGE")}">Sole Trader</a> packages
                </div>
            </div>
            <div class="clear"></div>
        </div>

        {include file="@blocks/matrix_silver_new_without_company_name_more_info.tpl" noCompany=0} 
    </div>


{include file="@footer.tpl"}