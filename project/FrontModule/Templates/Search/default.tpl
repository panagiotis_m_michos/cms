{include file="@header.tpl"}
{literal}
    <style>
        div.matrix1-page-top-left h1 {
            color: #272727; font-size: 26pt;
            margin-top: 15px;
            font-weight: bold;
            margin-bottom: 0;
        }
        div.matrix1-page-top-left h2 {
            color: #ff6e00; font-size: 26pt;
            margin-top: 0px;
            margin-bottom: 15px;
        }
        div.matrix1-page-top-left h4 {
            color: #282828; font-size: 15pt;
            font-weight: normal;

        }

        div.matrix-new-page-faq h5 {
            font-size: 15px;
            color: #272727;
            margin-top: 15px;
            margin-bottom: 15px;
        }
        div.matrix-new-page-faq p {
            font-size: 14px;
            color: #272727;
            line-height: 17px;
            padding-top: 0;
            padding-bottom: 12px;
        }
        div.matrix1-column-center{
            color: #3a3a3a;
            font-size: 13px;
        }

        div.two-rows-column {
            padding-top: 15px;
        }

        div.one-rows-column {
            padding-top: 25px;
        }

        div#content {
            padding: 0 0 0;
        }

        div.home-page-pakages-include ul li{
            font-size: 14px;
        }

        div.tooltip  span ul li{
            margin-left: 0px;
        }

        div.tooltip span ul {
            margin-left: 0px;
            padding-left: 0px;
        }

        .hp-toolkit-text-style {
            float: left;
            font-size: 13px;
            color: #3a3a3a;
            line-height: 19px;
            padding-top: 0;
            margin-top: 4px;
        }

        .tooltip_qmark {
            float: left;
            margin-left: 10px;
        }

        li.textblock {
            line-height: 23px!important;
            margin-bottom: 15px!important;
        }

        div.flash {
            margin: 20px 0 0;
            width: auto;
        }
    </style>
    {/literal}

<a name="top"></a>
<div id="maincontent" class="home-page matrix-split-test">
    {* TOP *}
    <div id="matrix1-page-top">
        <div class="matrix1-page-top-left" style="margin-top: 10px;">
            {*<h1>Get your company name today in 4 easy steps and protect it from others</h1>
            <span>- or reserve it for the future.</span>
             *}
             <div class="matrix1-page-left-headers">
                {if isset($companyName) && $companyName && !isset($companyUnavaliable)}
                <h1>Good News!</h1>
                <h2>
                    {$companyName}<span style="color: #272727"> is available!</span>
                </h2>
                 <h4>What would you like to do now?</h4>
                {elseif isset($companyUnavaliable)}
                    <h1 style="margin-bottom: 15px;">
                        Choose the Perfect Package for You and <br />Start Your New Company Today
                    </h1>
                    <h4 style="font-size: 19px;"> Simply choose from the packages below or use these resources to help you decide:</h4>
                {else}
                    <h1 style="margin-bottom: 15px;">
                        Choose the Perfect Package for You and <br />Start Your New Company Today
                    </h1>
                    <h4 style="font-size: 19px;"> Simply choose from the packages below or use these resources to help you decide:</h4>
                {/if}
                <ul>
                    <li><span class="span1">&nbsp;</span>Discover <a href="{$this->router->link(1318)}" class="popupnew">why you should register via us</a>.</li>
                    <li><span class="span2">&nbsp;</span>Learn about registering a company: <a href="{$this->router->link(746)}" class="popupnew">See this guide</a></li>
                    <li><span class="span3">&nbsp;</span>Do you still have complicated questions?: <a href="#faq">See our FAQs</a>,<div style="margin-left: 27px;"><a href="javascript:$zopim.livechat.window.show()">Click here for live chat</a>&nbsp;or call us on 0207 608 5500</div></li>
                    <li><span class="span4">&nbsp;</span>If you're ready to proceed, scroll down to choose the perfect package for you, then continue to order. </li>
                </ul>
            </div>

            <div class="clear"></div>
        </div>
        <div class="matrix1-page-top-right">
        </div>
        <div class="clear"></div>
    </div>

    {if !empty($error)}
    {literal}
    <style>
        div.flash.error
        {
            background: none repeat scroll 0 0 #CCE5FF;
            border: 1px solid #006699;
        }
        div.flash
        {
            width: auto;
        }
    </style>
    {/literal}
    <div class="flash error" style="margin: 15px 0px">
    {$error}
    </div>
    {/if}

    {* Reserve Words Toggle Message                *}
    {if !empty($reserveWords)}
    {literal}
    <style>
        div.flash.error
        {
            background: none repeat scroll 0 0 #CCE5FF;
            border: 1px solid #006699;
        }
        div.flash
        {
            width: auto;
        }
    </style>
    <script>
        $(document).ready(function() {
        $('#slick-toggle1').click(function () {
            $('#slickbox1').toggle(400);
            $('#slick-toggle1').text($('#slick-toggle1').text() == "More Info" ? "Less Info" : "More Info");
            //return flase;
        });
        $('#slick-toggle2').click(function () {
            $('#slickbox2').toggle(400);
            $('#slick-toggle2').text($('#slick-toggle2').text() == "Less Info" ? "More Info" : "Less Info");
        });
        });
    </script>
    {/literal}
        <div class="flash error" style="margin: 15px 0px">
<!--                <h3 style="color: black; margin: 0; padding: 0;">Warning! You have used a reserved word(s) in your company name.</h3>-->
            {foreach from=$reserveWords item="reserveWord"}
                <div> Reserved word detected: <b>{$reserveWord->word|upper}</b> Use of this word may require
                {if $reserveWord->typeId == 2}
                supporting documentation. We will provide you with a template to use. <a id="slick-toggle1" href="#">More Info</a>
                <div id="slickbox1" style="display: none">{$reserveWord->description nofilter}</div>
                {else}
                you to provide supporting documentation. <a id="slick-toggle2" href="#">More Info</a>
                <div id="slickbox2" style="display: none">{$reserveWord->description nofilter}</div>
                {/if}

                </div>
            {/foreach}
        </div>
    {/if}

    {if isset($companyName) && $companyName}
        {include file="@blocks/matrix_new_with_company.tpl" noCompany=0}
    {else}
        {include file="@blocks/matrix_new_no_company.tpl" noCompany=1}
    {/if}
    <div id="international-banner">
        <div class="fleft">
            <h2>UK Company Formation for Overseas Residents</h2>
            <p class="largefont">
                Our international packages include everything you need to form a UK limited company, plus<br />
                the option of a UK bank account for your business.
            </p>
        </div>
        <div class="fright mnone txtright">
            <div>
                <span class="blue3 txtnormal bigfont">from</span>
                <span class="blue3 txtbold massivefont">&pound;199</span>
                <span class="black normalfont">+VAT</span>
                <div class="clear"></div>
            </div>
            <div class="mtop">
                <form action="company-formation-non-uk-residents.html">
                    <input type="submit" value="More Info" class="orange-submit" onclick="_gaq.push(['_trackEvent', 'Links', 'Click', 'International packages More info from Packages page']);">
                </form>
            </div>
        </div>
    </div>
    <div id="home-page-left-side">
        <p style="font-size: 18px;margin-bottom: 5px;">Included in All of Our Packages:</p>
        <h4>Free Business Start Up Toolkit - Everything You <br /> Need to Start, Run &amp; Grow Your New Company.</h4>
        <div class="home-page-pakages-include">
            <ul>
                <div style="width: 390px; float:left;">
                <li class="textblock">
                    <span class="hp-toolkit-text-style"><b>FREE Business Bank Account</b></span>
                    <i class="fa fa-question-circle grey2 font20 mleft10" data-toggle="tooltip" data-placement="bottom" title="
                       <b>FREE Business Bank Account</b><br />
                        - Choose between Barclays and TSB<br />
                        - You get {$packages[1313]->getCashBackAmount()|currency:0}-{$packages[1317]->getCashBackAmount()|currency:0} cash back from us when you open your account<br />
                        - 12-18 months' free business banking<br />
                        - Available to UK customers only<br />
                        ">
                    </i>
                    <div class="clear"></div>
                    <span class="hp-toolkit-text-style">with {$packages[1313]->getCashBackAmount()|currency:0}-{$packages[1317]->getCashBackAmount()|currency:0} Cash Back (Optional)</span>
                    <div class="clear"></div>
                </li>
                <li class="textblock">
                    <span class="hp-toolkit-text-style"><b>FREE .co.uk website plus more...</b></span>
                    <i class="fa fa-question-circle grey2 font20 mleft10" data-toggle="tooltip" data-placement="bottom" title="
                    <b>FREE .co.uk website plus more</b><br />
                        Save &pound;s by getting your online presence started with our MadeSimple solution - Websites MadeSimple.<br />
                        <br />
                        Your free package includes:<br />
                        &bullet; Free .co.uk  Website for 1 year<br />
                        &bullet; Free Personalised Email Address<br />
                        &bullet; Free 1-page Website<br />
                        &bullet; Free Professional Stock Images<br />
                    ">
                    </i>
                    <div class="clear"></div>
                    <span class="hp-toolkit-text-style">Free 1 year domain name includes free email address, 1-page website and professional stock images.</span>
                    <div class="clear"></div>
                </li>
                <li class="textblock">
                    <span class="hp-toolkit-text-style"><b>Google Adwords Voucher</b></span>
                    <i class="fa fa-question-circle grey2 font20 mleft10" data-toggle="tooltip" data-placement="bottom" title="
                    <b>Google AdWords Voucher</b> <br />
                    &bullet; Advertise your products & services to millions of potential customers using Google's online advertising service.<br />
                    &bullet; Find more customers by advertising your site with Google.<br />
                    &bullet; Spend &pound;25 and get an additional &pound;75.<br />
                    &bullet; Use Google's phone setup service to get started now.
                    ">
                    </i>
                    <div class="clear"></div>
                    <span class="hp-toolkit-text-style">Get your business in front of millions of potential customers.</span>
                    <div class="clear"></div>
                </li>
                <li class="textblock">
                    <span class="hp-toolkit-text-style"><b>FREE Accountancy &amp; Tax Consultation</b></span>
                    <i class="fa fa-question-circle grey2 font20 mleft10" data-toggle="tooltip" data-placement="bottom" title="
                    <b> FREE Accountancy & Tax Consultation</b> <br />
                    &bullet; Find out if you're paying too much tax.<br />
                    &bullet; Free review of your affairs by a qualified accountant.<br />
                    &bullet; Branches nationwide for convenient appointments.<br />
                    &bullet; UK customers only.
                    ">
                    </i>
                    <div class="clear"></div>
                    <span class="hp-toolkit-text-style">(UK Customers Only)</span>
                    <div class="clear"></div>
                </li>
                </div>
                <div style="width: 275px;float:left;"><img width="235" height="240" style="padding-left: 30px; margin-top: 20px;" alt="startup toolkit" src="/project/upload/imgs/startup-toolkit.png" /></div>
                <div style="clear: left; height: 0px;">&#160;</div>
                <li class="textblock">
                    <span class="hp-toolkit-text-style"><b>Exclusive 2-Month FREE Trial to FreeAgent's</b></span>
                    <i class="fa fa-question-circle grey2 font20 mleft10" data-toggle="tooltip" data-placement="bottom" title="
                    <b> FREE 2-Month trial to FreeAgent</b> <br />
                    &bullet; Exclusive discount for Companies Made Simple customers - you won't find this offer anywhere else.<br />
                    &bullet; Plus get a 10% discount on the monthly subscription for 12-months after your free trial ends.<br />
                    &bullet; Keep track of all your company's finances. <br />
                    &bullet; FreeAgent is super easy-to-use, you don't need any accounting or bookkeeping experience to start using it.<br />
                    ">
                    </i>
                <div class="clear"></div>
                <span class="hp-toolkit-text-style">Award Winning Online Accounting Tool for Small Businesses. Plus you get 10% discount on the monthly subscription for 12 months after the free trial ends. FreeAgent is easy-to-use and you won't need any previous accounts experience to start using it.</span>
                <div class="clear"></div>
                </li>
                <li class="textblock">
                    <span class="hp-toolkit-text-style"><b>FREE Ultimate Small Business Startup eBook</b></span>
                    <i class="fa fa-question-circle grey2 font20 mleft10" data-toggle="tooltip" data-placement="bottom" title="
                    <strong>FREE Ultimate Small Business Startup eBook</strong><br>
                    All the key information you need to know to start, run and grow your business, including:<br>
                    &bullet; Creating a business plan<br>
                    &bullet; Funding options<br>
                    &bullet; Marketing<br>
                    &bullet; Accounting<br>
                    &bullet; And more
                    ">
                    </i>
                    <div class="clear"></div>
                    <span class="hp-toolkit-text-style">Everything you need to know to start, run and grow your new business</span>
                    <div class="clear"></div>
                </li>
            </ul>
        </div>
        <div class="home-page-compare-packages" style="height: 60px; width: 690px; overflow: hidden;">
            <img src="{$urlImgs}home-page/home-page-compare-packages2.png" alt="Our partners" usemap="#compare-packages" />
            <map id="compare-packages" name="compare-packages">
                <area title="Click here to compare all the packages" alt="Compare all the packages" href="{$this->router->link("SearchControler::SEARCH_PAGE")}" class="sample" coords="120,100,485,150" shape="rect">
            </map>
        </div>
        <a name="faq"></a>
        <div class="matrix-new-page-faq">
            <div>
                <div style="float: left; width: 510px;"><h3>Frequently Asked Questions</h3></div>
                <div style="float: left; width: 180px;margin-top: 15px;"><a href="javascript:$zopim.livechat.window.show()">we're here to help - chat live</a></div>
                <div class="clear"></div>
            </div>

            <h5 class="txtbold">Do I need to provide proof of ID?</h5>
            <p>
                No if you are buying the Basic or Printed company formation packages.
                <br /><br />
                Yes if you are buying the registered office/service address service (Privacy, Comprehensive and Ultimate company formation packages).
                In this case we are required by law to hold proof of ID and proof of address.
                <br /><br />
                We have a legal obligation to check proof of ID and proof of address for all customers who use our address services.
                This is to ensure we comply with Anti-Money Laundering (AML) regulations and Know Your Customer (KYC) requirements.
                For which documents you need to provide us please check the <a href="{$this->router->link(1611)}" class="popupnew">FAQs</a>.
            </p>
            <h5 class="txtbold">Will I still need an accountant?</h5>
            <p>
                For certain tasks we do recommend that you use an accountant.
                This is why we offer a free consultation with a local TaxAssist accountant once your company has been formed.
                <br /><br />
                We provide you with lots of inclusive services and tools to keep accounting costs down.
                <br /><br />
                All of our packages include a free 2-month trial to FreeAgent,
                easy-to-use small business accounting software that helps keep you on top of all your
                business finances. Our Comprehensive and Ultimate Packages take care of some of
                the things that your accountant would normally do,
                such as preparing and filing your company's Annual Return. With the Ultimate Package we'll also register
                your company for VAT with HMRC.
                <br /><br />
                Less work for your accountant equals lower fees for you.
            </p>
            <h5 class="txtbold">Is there a renewal cost annually?</h5>
            <p>
                This depends on the package that you choose.
                <br /><br />
                The Privacy Package has an annual renewal cost of &pound;{$privacyPackageRenewal->getPrice()}, only applicable if you want
                to continue using our Registered Office and Service Address to keep your residential
                address confidential and off Companies House public register.
                <br /><br />
                The Comprehensive and Ultimate packages have an annual renewal cost of &pound;{$compUltiPackageRenewal->getPrice()},
                only applicable if you want to continue using our Registered Office and Service Address
                to keep your residential address confidential and off Companies House public register.
                Also included is our Maintenance of Statutory Books Service and Preparation/Filing of
                Annual Return service.
            </p>
            <h5 class="txtbold">What if I only need the company name and none of the other parts?</h5>
            <p>
                If you're looking to start trading immediately or very soon, then our Basic Package is the
                cheapest way to get up and running quickly and easily. However,
                if you want to open a bank account we recommend the printed package as
                most bank managers require this official documentation.
                <br /><br />
                On, the other hand, if you simply want to secure your chosen company name for the future,
                then you can reserve it <a href="{$this->router->link(239)}">here</a>.
            </p>
            <h5 class="txtbold">I'm not sure what I need right now, can I buy the Basic Package and add other services to it later?</h5>
            <p>
                Absolutely, however the services will not be available at the discounted package rates.
            </p>
            <h5 class="txtbold">I don't live in the UK, can I still open a UK company & bank account?</h5>
            <p>
                You can form a UK limited company regardless of your nationality or location.
                <br /><br />
                A UK limited company must have a registered office based in the UK.
                Our Privacy, Comprehensive and Ultimate Packages all include use of our <a href="{$this->router->link(259)}">Registered Office Service</a>
                for one year (you have the option to renew after this period),
                perfect for customers who do not have access to their own UK address.
                <br /><br />
                Our International package is the ideal solution for overseas customers looking to form a UK Limited company.
                It also includes a 1 hour tax consultation with our UK Tax Experts who can also assist you with other professional
                services such as assistance with opening a UK bank account and online accountancy services (please note the cost
                for these services is to be arranged separately with our tax and banking partner).
                <a href="{$this->router->link(1435)}">More information</a>
            </p>
            <h5 class="txtbold">What are my statutory and legal requirements once I start a Limited Company?</h5>
            <p>
                Limited companies are required to file annual returns and annual accounts with Companies House.
                If you choose the Comprehensive or Ultimate Package, we take care of the annual return for you.
                <br /><br />
                You or your accountant will need to submit annual tax returns and company accounts to HMRC.
                <br /><br >
                As a director of a limited company you must act in the best interests of the
                company and not necessarily yourself.
            </p>
            <h5 class="txtbold">How much tax will I save by starting a Limited Company vs. being a Sole Trader?</h5>
            <p>
                This depends on your circumstances. You may find this <a href="{$this->router->link(726)}">Sole Trader v Limited Calculator</a> helpful.
            </p>
            <h5 class="txtbold">How do I get funding for my new business?</h5>
            <p>
                There are a number of options available when it comes to funding a business.
                These can be split into two categories; debt financing sources and equity financing sources.
                Debt financing sources would include banks, lease financing and trade credit.
                Equity financing sources would include venture capitalist companies and private individuals.<a href="/blog/how-do-i-get-funding-for-my-new-business/">More info</a>
            </p>
        </div>
        <br />
        <div>
            <div style="float: left;">
                <a href="http://support.companiesmadesimple.com/category/132-frequently-asked-questions" class="matrix1-normal-btn" target="_blank">SEE MORE FAQs</a>
            </div>
            <div style="float: right; margin-top: 10px;">
                <a href="#top">Back to the top</a>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div id="home-page-right-side">
        <div class="home-page-right-side-partners">
            <img src="{$urlImgs}home-page/home-page-partners.png" alt="partners" />
        </div>

        <div class="home-page-right-side-testimonials">
            <div class="home-page-right-side-testimonials-top">
                <p>More Testimonials</p>
            </div>
            <div class="home-page-right-side-testimonials-midd">
                <div class="home-page-testimonial">
                    <div class="left-home-page-testimonial"></div>
                    <div class="right-home-page-testimonial">
                    <b>Outstanding service.</b> The process was simple,
                    provided detailed information, and our company
                    has been incorporated in under 3 working hours.
                    Very impressed, will definitely be using
                    again, and will recommend to others.
                    </div>
                    <div class="clear"></div>
                    <div class="home-page-top-right-author">
                        <div class="home-page-top-author-left1">
                        </div>
                        <div class="home-page-top-author-right">
                            <span style="color: #2b7cac">Tim Durden</span> <br />
                            <span>Ever FX Ltd.</span> <br />
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>

                <div class="home-page-testimonial">
                    <div class="left-home-page-testimonial"></div>
                    <div class="right-home-page-testimonial">
                        This is the third company
                        I've set up, but the first
                        time I have used you guys.
                        So much easier, <b>I'll never
                        bother trying to do it myself
                        again - so simple.</b>
                    </div>
                    <div class="clear"></div>
                    <div class="home-page-top-right-author">
                        <div class="home-page-top-author-left2">
                        </div>
                        <div class="home-page-top-author-right">
                            <span style="color: #2b7cac">Chris Moore</span> <br />
                            <span>Hexchem Imports Ltd.</span> <br />
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>

                <div class="home-page-testimonial">
                    <div class="left-home-page-testimonial"></div>
                    <div class="right-home-page-testimonial">
                    You are my preferred supplier when forming
                    companies for my clients. Very <b>quick
                    and simple</b> which means I am able to
                    offer a good value service to my
                    clients. Thank you
                    </div>
                    <div class="clear"></div>
                    <div class="home-page-top-right-author">
                        <div class="home-page-top-author-left3">
                        </div>
                        <div class="home-page-top-author-right">
                            <span style="color: #2b7cac">Richard Wilson</span> <br />
                            <span>RAK Accountancy <br />Solutions Ltd.</span> <br />
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>

            </div>
            <div class="home-page-right-side-testimonials-bottom"></div>
            </div>
    </div>
    <div class="clear"></div>
</div>


{literal}

<script type="text/javascript">
$(document).ready( function(){
    $('.home-page-tooltip').each(function() {
            var self = $(this);
            self.tooltip({
            tip : "#tooltip" +  $(this).attr('rel'),
            position: 'center right',
            offset: [0, 10],
            effect: 'slide',
            delay: 0,
            opacity: 1
        });
    });

    $('.matrix1-tooltip').each(function() {
            var self = $(this);
            self.tooltip({
            tip : "#tooltip" +  $(this).attr('rel'),
            position: 'center right',
            offset: [0, 10],
            delay: 0,
            opacity: 1
        });
    });

    $('.matrix1-tooltip1').each(function() {
            var self = $(this);
            self.tooltip({
            tip : "#tooltip" +  $(this).attr('rel'),
            position: 'center left',
            offset: [0, -10],
            delay: 0,
            opacity: 1
        });
    });
});
</script>
{/literal}

{include file="@footer.tpl"}
