{include file="@header.tpl"}
    <div class="new-matrix-search" style="height: auto;">
        <div class="search-result-name">
            <div class="result-name-text">
                {if isset($companyName) && $companyName}
                    <div style="float: left;"> 
                        <p>Good News!</p> 
                    </div>
                    <div class="result-name-tick"></div>
                    <div class="clear"></div>
                    <p><strong>{$companyName|h nofilter}</strong> is available! </p>
                    <p>To claim it, select a package & sign up today!</p>    
                {else}
                     <h1 style="margin-bottom: 15px; font-size: 24px; color:black; font-weight: bold;">
                        Select a package & sign up today!
                    </h1>
                {/if}   
            </div>
            
            <div class="result-name-time-off" style="float: right;margin-right: 30px;">
                 <p style="font-weight: normal;">Why use Companies Made Simple</p>
                 <div style="margin-top: 15px;" class="why-use-cms-text">We've registered over 250,000 companies since 2002</div>
                 <div class="separator-li">&nbsp;</div>
                 <div class="why-use-cms-text">Approved Partner of the Institute of Chartered Accountants in England & Wales</div>
                 <div class="separator-li">&nbsp;</div>
                 <div  class="why-use-cms-text">95% of customers rate our service as excellent</div>
                 <div style="color: #666; text-align: right;padding-right: 16px;margin-top: 10px;">
                    <a href="{$this->router->link("SearchControler::BYGUAR_PAKAGE_PAGE")}">Non-profit</a>, 
                    <a href="{$this->router->link("SearchControler::LLP_PAKAGE_PAGE")}">LLP</a> and 
                    <a href="{$this->router->link("SearchControler::SOLE_TRADER_START_UP_PAGE")}">Sole Trader</a> packages
                </div>
            </div>
            <div class="clear"></div>
        </div>
        
        {if !empty($error)}
        {literal}
        <style>
            div.flash.error 
            {
                background: none repeat scroll 0 0 #CCE5FF;
                border: 1px solid #006699;
            }
            div.flash 
            {
                width: auto;
            }
        </style>
        {/literal}
            <div class="flash error" style="margin: 15px 45px">
            {$error}
            </div>
        {/if}
        
        {* Reserve Words Toggle Message                *}
        {if !empty($reserveWords)}
        {literal}
        <style>
            div.flash.error 
            {
                background: none repeat scroll 0 0 #CCE5FF;
                border: 1px solid #006699;
            }
            div.flash 
            {
                width: auto;
            }
        </style>
        <script>
            $(document).ready(function() {          
            $('#slick-toggle1').click(function () {
                $('#slickbox1').toggle(400);
                $('#slick-toggle1').text($('#slick-toggle1').text() == "More Info" ? "Less Info" : "More Info");
                //return flase;
            });
            $('#slick-toggle2').click(function () {
                $('#slickbox2').toggle(400);
                $('#slick-toggle2').text($('#slick-toggle2').text() == "Less Info" ? "More Info" : "Less Info");
            });
            });
        </script>
        {/literal}
            <div class="flash error" style="margin: 15px 45px">
<!--                <h3 style="color: black; margin: 0; padding: 0;">Warning! You have used a reserved word(s) in your company name.</h3>-->
                {foreach from=$reserveWords item="reserveWord"} 
                    <div> Reserved word detected: <b>{$reserveWord->word|upper}</b> Use of this word may require
                    {if $reserveWord->typeId == 2}
                    supporting documentation. We will provide you with a template to use. <a id="slick-toggle1" href="#">More Info</a>
                    <div id="slickbox1" style="display: none">{$reserveWord->description nofilter}</div>
                    {else}
                    you to provide supporting documentation. <a id="slick-toggle2" href="#">More Info</a>
                    <div id="slickbox2" style="display: none">{$reserveWord->description nofilter}</div>
                    {/if}
                    
                    </div>
                {/foreach}
            </div>
        {/if}
        
        
        {if isset($companyName) && $companyName}     
              {include file="@blocks/matrix_silver_new.tpl"}
        {else}
             {include file="@blocks/matrix_silver_new_no_company.tpl" noCompany=1}              
        {/if}
    </div>


{include file="@footer.tpl"}