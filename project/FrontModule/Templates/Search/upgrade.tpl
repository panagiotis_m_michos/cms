{include file="@header.tpl"}


<div id="maincontent3">
    
    <div class="margin-bottom-20">
        <img src="{$urlImgs}progress_bar_buy.png" alt="Progress Bar" width="757" height="44">
    </div>

    <h1 class="left">Your Basket</h1>
    
    {$form->getBegin() nofilter}
        <div class="buttonsmall">
            {$form->getControl("continue") nofilter}
        </div>
    {$form->getEnd() nofilter}

    <div class="clear"></div>

    <table class="basket" align="left" border="0" cellpadding="0" cellspacing="0" width="755">
    {foreach from=$basket->getItems() key="key" item="item"}
        <tr class="odd">
            <td class="img">
                {if $item->getAssociatedProductImage() != NULL}
                    {$item->getAssociatedProductImage()->getHtmlTag() nofilter}
                {else}
                    <img src="" width="73" heigth="65" alt="default"/>
                {/if}
            </td>
            <td class="desc">
                {$item->getLngTitle()|strip_tags|truncate:56:"...":1} 
            </td>
            <td class="actions">
                <div>
                    {if !empty($item->basketText)}
                        <div class="help-button2" style="float: left; margin: 0 3px 0 0;">
                            <a class="help-icon3"
                                data-toggle="tooltip" 
                                data-placement="left" 
                                title="" 
                                data-original-title="{$item->basketText|strip_tags}">
                            </a>
                        </div>
                    {/if}
                </div>
                <div>
                    {if $item->getClass() != "Package"}
                        {if $item->getClass() == "BarclaysBanking"}
                            <a class="delete" href="{$this->router->link(null, array("removeItemId=$key","removedProductId="))}" data-confirm="You will need a business bank account for your limited company. This free service is designed to help you on your way. Are you sure you want to remove it?"></a>
                        {elseif $item->getId() ==  Product::PRODUCT_FRAUD_PROTECTION}
                            <a class="delete" href="{$this->router->link(null, array("removeItemId=$key","removedProductId="))}" data-confirm="Company fraud protection notifies you of any changes to your company (director resignations etc.), even if you didn't make them. OK = remove, Cancel = keep."></a>
                        {elseif $item->getId() ==  1350}
                            <a class="popupnew font13" href="{$item->getLink()}?packageId={$package->nodeId}&popup=1" >More Info</a>
                        {else} 
                            <a class="delete" href="{$this->router->link(null, array("removeItemId=$key","removedProductId="))}"></a>
                        {/if}
                    {/if}
                </div>
            </td>
            <td class="price">{if $item->price == 0}FREE{else}&pound;{$item->price|string_format:"%.2f"}&nbsp;{/if}</td>
        </tr>
    {/foreach}

    {* voucher *}
    {if isset($voucherForm) && $basket->voucher}
        <tr class="odd">
            {$voucherForm->getBegin() nofilter}
            <td class="img"><img src="{$urlImgs}voucher.png" width="86px" heigth="60px" alt="Voucher"/></td>
            <td class="desc">Voucher {$basket->voucher->name}</td>
            <td class="actions">{$voucherForm->getControl("removeVoucher") nofilter}</td>
            <td class="price" style="color: red;">
                {assign var="subTotal" value=$basket->getPrice('subTotal')}
                -&pound;{$basket->voucher->discount($subTotal)}
            </td>
            {$voucherForm->getEnd() nofilter}
        </tr>
    {/if}

    <tr class="totals odd">
        <td colspan="3">
            <div class="imgwrap">
                <img src="{$urlImgs}see-popular.png" alt="See popular add-on products" width="358" height="127" />
            </div>
            Subtotal:<br />
            VAT:<br />
            <span class="total">Total:</span>
        </td>
        <td>
            &pound;{$basket->getPrice("subTotal")}<br />
            &pound;{$basket->getPrice("vat")}<br />
            <span class="total">
                &pound;{$basket->getPrice("total")}
            </span>
        </td>
    </tr>
    </table>
        
    <div class="clear"></div>
    
    {$form->getBegin() nofilter}
        <span class="buttonbig">
            {$form->getControl("continue") nofilter}
        </span>
    {$form->getEnd() nofilter}
    
    <div class="clear"></div>
    
    {if isset($voucherForm) && !$basket->voucher}
        <div class="container2" style="margin: 30px 0 0 10px;">
            <div class="add-voucher" style="margin-top: -50px;">
                {$voucherForm->getBegin() nofilter}
                {$voucherForm->getControl('voucherCode') nofilter}
                {$voucherForm->getControl("addVoucher") nofilter}
                <span class="ff_control_err">{$voucherForm->getError('voucherCode')}</span>
                {$voucherForm->getEnd() nofilter}
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
    {/if}

    <h2>Other people who bought this package also bought:</h2>
    <table class="basket" align="left" border="0" cellpadding="0" cellspacing="0" width="755">
    <tr>
        <th colspan="3"></th>
        <th class="was">Usual Price</th>
        <th class="offer">Special Price When You Buy Today</th>
        <th class="add">Add to Basket</th>
    </tr>
    {foreach from=$associatedProducts key="key" item="product" name="products"}
        {if $smarty.foreach.products.iteration <= 3}
            <tr>
                <td class="img">
                    {if $product->getAssociatedProductImage() != NULL}
                        {$product->getAssociatedProductImage()->getHtmlTag() nofilter}
                    {else}
                        <img src="" width="73" heigth="65" alt="default"/>
                    {/if}
                </td>
                <td class="desc1">
                    <b>{$product->getLngTitle()}</b>&nbsp;
                    <p>{$product->associatedText}&nbsp;
                </td>
                <td class="actions">
                    <a class="popupnew" href="{$product->getLink()}?packageId={$package->nodeId}&popup=1" >More</a>
                </td>
                <td class="was">
                    <span style="text-decoration: line-through;">&pound;{$product->productValue}</span>
                </td>
                <td class="offer">
                    <b>&pound;{$product->price}</b>
                </td>
                <td class="add">
                    <a href="{$this->router->link(null, array("addProductId=$key","removedProductId="))}">Add</a>
                </td>
            </tr>
        {/if}
    {/foreach}
    </table>
</div>
    
<div id="rightCol">
    <div class="sidebox">
        <h3 class="heading">We work with</h3>
        <div class="content">
            <img src="{$urlImgs}partners.png" alt="Our Partners" width="131" height="330" />
        </div>
    </div>
    <div id="quotebox" class="sidebox">
        <h3 class="heading">Satisfied Clients...</h3>
        <div class="content">
            <div class="quote">
                Great service and <strong>incredible value for money</strong> compared to your competitors. Thank you for the smooth and swift registration and all of the additional benefits!
            </div>
            <div class="name">
                <img src="{$urlImgs}events-protocol-logo.png" alt="Events Protocol" width="49" height="39" />
                <strong>John Duggan</strong><br />
                Events Protocol Ltd.<br />
                21/09/2012
            </div>
        </div>
    </div>
    <div id="chatbox" class="sidebox">
        <h3 class="heading">Got a Question?</h3>
        <div class="content">
            <img src="{$urlImgs}live-chat-sarah.png" alt="Chat" width="162" height="187" />
            <div class="chat_text"><span>Let's chat now</span></div>
            <div><a href="javascript:$zopim.livechat.window.show()">Click here</a></div>
            <div><span class="chat_time">Mon-Fri 9am-5:30pm</span></div>
        </div>
    </div>
</div>
<div class="clear"></div>
<div style="width:1000px;">
    <div style="width:712px; float:left"><img src="{$urlImgs}card-logos.png" alt="You're safe with us!" width="712" height="117" /></div>
    <div style="width:220px; float:right; margin-top:40px">Every payment is secured by industry standard 256-bit encryption.<br />Companies Made Simple never stores your card details on our servers.</div>
</div>
    
<script type="text/javascript">
    window.nx_type = 1;
    window.nx_campaignid = 124;
    window.nx_source = 0;
    window.nx_productid = "{$package->nodeId}";
</script>
<script async="true" type="text/javascript"
        src="//s.targetedadsolutions.com/retarget/nr_v2.min.js">
</script>

{include file="@footer.tpl"}
