{include file="@header.tpl"}
{literal}
    <style type="text/css">
        div.flash{
            width:755px;
        }
    </style>
{/literal}

<div id="maincontent2">
{if $visibleTitle}
    <h1>{$title}</h1>
{/if}
	
	{* SEARCH FORM *}
	<div class="box-lrgwhite">
		<div class="box-lrgwhite-top">
			<div class="box-lrgwhite-bottom">
				{$form->getBegin() nofilter}
				<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formtable">
				<col width="150">
				<tr>
					<td align="left" valign="middle">{$form->getLabel('q') nofilter}<span class="orange">*</span></td>
					<td align="left" valign="top">{$form->getControl('q') nofilter}</td>
				</tr>
				<tr>
					<td></td>
					<td>{$form->getControl('send') nofilter}</td>
				</tr>
				</table>
				{$form->getEnd() nofilter}
			</div>
		</div>
	</div>
	        
	{* LIST OF COMPANIES *}
	{if !empty($companies)}
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="my-companies">
		<col>
		<col width="150">
		<col width="100">
		<tr>
			<th colspan="3" class="bbottom">List of similar names that are also unavailable</th>
		</tr>
		<tr class="odd midfont">
			<td><strong>Company Name</strong></td>
			<td><strong>Company Number</strong></td>
			<td><strong>Status</strong></td>
		</tr>
		{foreach from=$companies item="company"}
		<tr>
			<td>{$company.CompanyName}</td>
			<td>{$company.CompanyNumber}</td>
			<td>{$company.DataSet}</td>
		</tr>
		{/foreach}
		</table>
	{/if}
	<div class="clear"></div>
</div>

<script type="text/javascript">
    _gaq.push(['_trackEvent', 'Search', 'Unavailable']);
</script>


{include file="@footer.tpl"}