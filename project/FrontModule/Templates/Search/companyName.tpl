{include file="@header.tpl"}

<div id="maincontent2" style="width: 960px; margin: 0 10px 15px 10px;">
    <h1>Your Company's Name</h1>
    {literal}
    <style>
        div.flash.error 
        {
            background: none repeat scroll 0 0 #CCE5FF;
            border: 1px solid #006699;
            margin: 0 0 20px 10px;
        }
        div.flash 
        {
            width: auto;
        }
        #companySearch label 
        {
            font-size: 24px;
            color: black;
            font-weight: bold;
            padding-left: 0px;
            padding-bottom: 8px;
        }
        .cf-choose-bank-account
        {
            padding-bottom: 20px;
 
        }
        .cf-choose-bank-account .product-img{
            border: none;
        }
        #companySearch #search{
            margin-left: 6px;
        }
    </style>
    {/literal}
	{if !empty($reserveWords)}

    {literal}
    <script>
        $(document).ready(function() {          
            $('#slick-toggle1').click(function () {
                $('#slickbox1').toggle(400);
                $('#slick-toggle1').text($('#slick-toggle1').text() == "More Info" ? "Less Info" : "More Info");
                //return flase;
            });
            $('#slick-toggle2').click(function () {
                $('#slickbox2').toggle(400);
                $('#slick-toggle2').text($('#slick-toggle2').text() == "Less Info" ? "More Info" : "Less Info");
            });
        });
    </script>
    {/literal}
    <div class="flash error" style="margin: 15px 0; padding-bottom: 10px;">
        {foreach from=$reserveWords item="reserveWord"} 
        <div> Reserved word detected: <b>{$reserveWord->word|upper}</b> Use of this word may require
            {if $reserveWord->typeId == 2}
            supporting documentation. We will provide you with a template to use. <a id="slick-toggle1" href="#">More Info</a>
            <div id="slickbox1" style="display: none">{$reserveWord->description nofilter}</div>
            {else}
            you to provide supporting documentation. <a id="slick-toggle2" href="#">More Info</a>
            <div id="slickbox2" style="display: none">{$reserveWord->description nofilter}</div>
            {/if}

        </div>
        {/foreach}
    </div>
    {/if}
    {$form->getBegin() nofilter}
    <div class="cf-choose-bank-account">
        <p class="cf-p">Please enter your preferred company name (we'll make sure it's available).</p>
        <div class="cf-barcklays-account">
            <div class="product-img">
                <img src="{$urlImgs}magnifying-glass-78.png" width="78" height="78" alt="barclays logo" />
            </div>
            <div id="companySearch">
                    <table>
                        <col width="110">
                        <tr>
                            <td style="height: 40px;">{$form->getLabel("companyName") nofilter}</td>
                        </tr>
                        <tr>
                            <td>{$form->getControl("companyName") nofilter}</td>
                            <td>{$form->getControl("search") nofilter}</td>
                            <td>
                            {if isset($available)}
                               
                            {else}
                                <span class="ff_control_err">{$form->getError("companyName")}</span>
                            {/if}
                            </td>
                        </tr>
                    </table>       
            </div>
            <div class="clear"></div>
        </div>
            {assign var="error" value= $form->getError("companyName")}                  
            {if isset($error) }
               {literal}
               <style>
                   div.flash.error 
                   {
                       background: none repeat scroll 0 0 #CCE5FF;
                       border: 1px solid #006699;
                   }
                   div.flash 
                   {
                       width: auto;
                   }
               </style>
               {/literal}   
                   {* <div class="flash error" style="margin: 15px 0px">
                     We are unable to check the availability of this company name at the moment because the Companies House name checking service is unavailable. You may still make this purchase, however you may need to supply an alternative name when the Companies House name checking service becomes available again.
                    </div>
                    <div style="margin-top:10px;float:left;">
                     <a class="btn_more" href="{url route='basket_module_package_basket_add_package' packageId=$packageId}"> Procced</a>
                    </div>*}
            {/if}
    </div>
    <div style="float:left">
    {if isset($available)}
        {if !empty($available)}
            <table>
                <tr>
                    <td>
                        <div style="margin-bottom: 20px;">
                            <p style="font-size: 16px;">
                                <span style="color: #0B0; font-weight: bold;">Good News!</span> 
                                <strong>{if isset($companyName)} {$companyName} {/if}is available!</strong> Please proceed below:
                            </p>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>{$form->getControl("change") nofilter}</td>
                </tr>
            </table>
            <input type="hidden" name="proceed" value="1" />
        {else}
            <table>
                <tr>
                    <td>
                        <div style="margin-bottom: 20px;">
                            <p style="font-size: 16px;">
                                <span style="font-weight: bold;">Sorry, {$companyName} is not available.</span> 
                                <span>Please search again above</span>
                            </p>
                        </div>
                    </td>
                </tr>
            </table>
        {/if}
    {/if}
    </div>
    {$form->getEnd() nofilter}
</div>

{include file="@footer.tpl"}
