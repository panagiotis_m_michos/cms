{include file="@header.tpl"}
<div class="formprocesstitle">{if $visibleTitle}
    <h1>{$title}</h1>
    {/if}</div>

{* TABS *}
{include file="@blocks/navlist.tpl" currentTab=$this->action}

<div id="maincontent">
    {$formHelper->setTheme($form, 'cms.html.twig')}
    {$formHelper->start($form, ['attr' => ['class' => 'toolkit-offers-form']]) nofilter}

    <fieldset id="fieldset_1343">
        <legend>Our Business StartUp Toolkit helps get you started</legend>
        <p>
            A <b>Business StartUp Toolkit</b> was included in your package free of charge, please untick the boxes below if you do not wish to receive
            any of these free offers.
        </p>
        <div id="block-form">
            {foreach $form['offers'] as $item}
                {assign 'index' $item->vars['value']}

                {$formHelper->widget($item) nofilter}
                <i class="fa fa-question-circle grey2" data-toggle="tooltip" data-placement="right" title="{$choices[$index]->getDescription()}"></i><br>
            {/foreach}
        </div>
    </fieldset>

    <div id="cf-toolkit-actions">
        <p class="hugefont noptop">What do you want to do next?</p>

        <table width="100%">
            <col width="33%">
            <col width="34%">
            <col width="33%">
            <tr>
                <td><a class="mtop" href="{$this->router->link("CFSecretariesControler::SECRETARIES_PAGE", "company_id=$companyId")}">Back</a></td>
                <td>&nbsp;</td>
                <td align="right">{$formHelper->widget($form['continue'], ['attr' => ['class' => 'btn_submit clear']]) nofilter}</td>
            </tr>
        </table>
    </div>

    {$formHelper->end($form) nofilter}
</div>

{include file="@footer.tpl"}
