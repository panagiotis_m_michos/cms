<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="cache-control"  content="no-cache" />
        <meta http-equiv="pragma"  content="no-cache" />
        <meta http-equiv="expires"  content="-1" />
        <meta http-equiv="content-language"  content="{$currLng}" />
        {if isset($noindex) && $noindex == true }
            <meta name="robots" content="noindex" />
        {else}
            <meta name="robots" content="all,index,follow" />
        {/if}
        <meta name="description" content="{$description}" />
        <meta name="keywords" content="{$keywords}" />

        <title>{$seoTitle}</title>

        <link rel="shortcut icon" href="{$urlImgs}favicon.ico" type="image/x-icon" />
        <link rel="icon" href="{$urlImgs}favicon.ico" type="image/x-icon" />

        {styles file='webloader_front.neon' section='common'}
        {styles file='webloader_front.neon' section='transition'}

        <script type="text/javascript">var dateTimeNow = "{$dateTimeNow}"</script>
        {scripts file='webloader_front.neon' section='common'}

        {literal}
            <script type="text/javascript">
                $(function() {
                    var $infoBar = $('.info-z-index');
                    if ($infoBar.length) {
                        $infoBar.show();
                        $(window).scroll(function() {
                            if ($(window).scrollTop() == 0) {
                                if ($("#clearselect").is(":visible")) {
                                } else {
                                    $infoBar.show();
                                }

                            } else {
                                $infoBar.hide();
                            }
                        });
                    }
                });
            </script>
        {/literal}

        {literal}
            <script type="text/javascript">
                $(document).ready(function() {
                    $('#cse-search-box input[name="q"]').focus(function() {
                        this.value = '';
                    });

                    $('.home-page-tooltip-icq').each(function() {
                        var self = $(this);
                        self.tooltip({
                            tip: "#tooltip" + $(this).attr('rel'),
                            position: 'center left',
                            offset: [-15, -15],
                            opacity: 1
                        });
                    });

                });
                $(document).ready(function() {
                    $('#cse-search-box input[name="q"]').focus(function() {
                        this.value = '';
                    });

                    $('.home-page-tooltip-right').each(function() {
                        var self = $(this);
                        self.tooltip({
                            tip: "#tooltip" + $(this).attr('rel'),
                            position: 'center right',
                            offset: [-15, 0],
                            opacity: 1
                        });
                    });

                });
            </script>
            <!-- Optimizely code -->
            <script src="//cdn.optimizely.com/js/138942235.js"></script>
        {/literal}
        {if isset($ecommerce)}
            <script>
                try{ldelim}
                        window.optimizely = window.optimizely || [];
                        window.optimizely.push(['trackEvent',
                            "revenue",
                            "{$basket->getPrice('subTotal')*100}"
                        ]);


                {rdelim} catch (err) {ldelim}{rdelim}
            </script>
        {/if}
        {* ECOMERCE *}
        {if isset($ecommerce)}

            <script type="text/javascript">
                try{ldelim}
                        var _gaq = _gaq || [];
                        _gaq.push(['_setAccount', 'UA-543198-9']);
                        _gaq.push(["_setCustomVar", 1, "user_type", "customer", 1]);
                        _gaq.push(['_trackPageview']);

                        _gaq.push(['_addTrans',
                            "{$basket->orderId}", // order ID - required
                            "Companies Made Simple", // affiliation or store name
                            "{$basket->getPrice('total')}", // total - required
                            "{$basket->getPrice('vat')}", // tax
                            "", // shipping
                            "", // city
                            "", // state or province
                            "" // country
                        ]);

                {foreach from=$basket->getItems() item="item"}
                        _gaq.push(['_addItem',
                            "{$basket->orderId}", // order ID - necessary to associate item with transaction
                            "{$item->getId()}", // SKU/code - required
                            "{$item->getLngTitle()}", // product name
                            "{$item->parentId}", // category or variation
                            "{$item->price}", // unit price - required
                            "{$item->qty}" // quantity - required
                        ]);
                {/foreach}

                        _gaq.push(['_trackTrans']); //submits transaction to the Analytics servers
                {rdelim} catch (err) {ldelim}{rdelim}
                {literal}
                    (function() {
                        var ga = document.createElement('script');
                            ga.type = 'text/javascript';
                        ga.async = true;
                        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                        var s = document.getElementsByTagName('script')[0];
                        s.parentNode.insertBefore(ga, s);
                    })();
                </script>

            {/literal}
        {else}
            {literal}
                <script type="text/javascript">
                    var _gaq = _gaq || [];
                    _gaq.push(['_setAccount', 'UA-543198-9']);
                    _gaq.push(['_trackPageview']);

                    (function() {
                        var ga = document.createElement('script');
                        ga.type = 'text/javascript';
                        ga.async = true;
                        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                        var s = document.getElementsByTagName('script')[0];
                        s.parentNode.insertBefore(ga, s);
                    })();
                </script>
            {/literal}
        {/if}

        <!-- Start Javascript for Split test
        <script type="text/javascript">
            var isCustomerLogged = {if isset($customer)}1{else}0{/if};
        </script>
        Stop Javascript for Split test -->
		<script type="text/javascript" src="{$urlJs}project.js"></script>

        {assign var=isCustomerLoggedIn value=!empty($customer)}

        <!-- Google Tag Manager DataLayer -->
        <script type="text/javascript">
            dataLayer = [];
            {if $isCustomerLoggedIn}
                dataLayer.push({
                    'customerId': {$customer->getId()}
                });
            {/if}

            //if basket is set, but ONLY on confirmation!
            {if isset($basket) && isset($ecommerce)}
                dataLayer.push({
                    'subtotal': {$basket->getPrice('subTotal2')},
                    'total': {$basket->getPrice('total')}
                });
            {/if}
        </script>
        <script type="text/javascript">
            uiserver.addCss('/components/ui_server/css/js.css');
        </script>

        <script type="text/javascript">
            var helpscout = new CMS.Helpscout();
            helpscout.init('What is the subject of your query?');

            {if $isCustomerLoggedIn}
                helpscout.identify("{$customer->getFullName()}", "{$customer->email}", {$customer->getId()});
            {/if}
        </script>
    </head>
    <body>
        {literal}
            <!-- Google Tag Manager -->
                <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5RVST5"
                height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
                <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                })(window,document,'script','dataLayer','GTM-5RVST5');</script>
            <!-- End Google Tag Manager -->
        {/literal}

        {ui name="global_header_bt" data=$gbrandlinks}
        {ui name="header_bt" data=$header}
        {if !isset($ecommerce)}
            {ui name="megamenu_cms_mobile" data=$megamenu}
            {ui name="megamenu_cms_desktop" data=$megamenu}
        {else}
            {ui name="megamenu_cms_mobile" data=$megamenu basketCount=0}
            {ui name="megamenu_cms_desktop" data=$megamenu basketCount=0}
        {/if}

        <div class="clear"></div>
            <div class="clear"></div>
                <div id="wrapper">
                    <div id="content">
                        {* LEFT COLUMN *}
                        {if !isset($hide) || $hide != 1}
                            {include file="@blocks/leftColumn.tpl"}
                        {/if}

                        {include file='@blocks/flashMessage.tpl'}
