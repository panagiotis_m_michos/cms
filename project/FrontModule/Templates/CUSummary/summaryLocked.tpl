{include file="@header.tpl"}

<div id="maincontent" style="position: relative;">

	<div class="flash error" style="margin-left: 0; width: 755px;">
		Your company has been locked by the system - If you purchased a reserve a company name, this is normal and no action is required until you purchase the <a href="{$this->router->link(239)}">Company Transfer Fee</a>. <br />
		<br />
		If you feel this lock has been done in error please contact support on 0207 608 5500
	</div>

	<h1 style="border: 0px solid red; width: 530px;">{$title}</h1>


	<h2>Company Details</h2>

	{* BARCLAYS FORM *}
	{if isset($barclaysForm) && 1==2}
		<div class="cu-summary-barclays">
			<div class="header">
				<b>Barclays Bussiness Banking</b>
			</div>
			<div class="content">
				Free Fast Track Bank <br />
				Account Application
				{$barclaysForm}
			</div>
		</div>
	{/if}



	<table class="grid2 full-width">
	<col width="250">
	<tr>
		<th>Company Name: </th>
		<td>{$data.company_details.company_name}</td>
	</tr>
	<tr>
		<th>Registration Number: </th>
		<td>{$data.company_details.company_number}</td>
	</tr>
	<tr>
		<th>Web Filing Authentication Code: </th>
		<td></td>
	</tr>
	<tr>
		<th>Company Category:</th>
		<td>{$data.company_details.company_category}</td>
	</tr>
	<tr>
		<th>Company Status:</th>
		<td>{$data.company_details.company_status}</td>
	</tr>
	<tr>
		<th>Country of Origin: </th>
		<td>{$data.company_details.country_of_origin}</td>
	</tr>
	<tr>
		<th>Incorporation Date: </th>
		<td>{$data.company_details.incorporation_date|date_format:"%d/%m/%Y"}</td>
	</tr>
	<tr>
		<th>Nature of Business: </th>
		<td>{$data.company_details.sic_codes}</td>
	</tr>
	<tr>
		<th>Accounting Reference Date: </th>
		<td>{$data.company_details.accounts_ref_date|replace:'-':'/'}</td>
	</tr>
	<tr>
		<th>Last Accounts Made Up Date: </th>
		<td>{$data.company_details.accounts_last_made_up_date|date_format:"%d/%m/%Y"}</td>
	</tr>
	<tr>
		<th>Next Accounts Due: </th>
		<td>{$data.company_details.accounts_next_due_date|date_format:"%d/%m/%Y"}</td>
	</tr>
	<tr>
		<th>Last Return Made Up To: </th>
		<td>{$data.company_details.returns_last_made_up_date|date_format:"%d/%m/%Y"}</td>
	</tr>
	<tr>
		<th>Next Confirmation Statement Due: </th>
		<td>{$data.company_details.returns_next_due_date|date_format:"%d/%m/%Y"}</td>
	</tr>
	{*
	<tr>
		<th>Last Members List: </th>
		<td></td>
	</tr>
	<tr>
		<th>Previous Names: </th>
		<td></td>
	</tr>
	*}
	</table>

	{* REGISTERED OFFICE *}
	<h2>Registered Office </h2>
	<table class="grid2 full-width">
	<col width="250">
	<tr>
		<th>Address</td>
	</tr>
	<tr>
		<td>
			{$data.registered_office.premise} {$data.registered_office.street} <br />
			{if !empty($data.registered_office.thoroughfare)}{$data.registered_office.thoroughfare} <br />{/if}
			{if !empty($data.registered_office.post_town)}{$data.registered_office.post_town} <br />{/if}
			{if !empty($data.registered_office.county)}{$data.registered_office.county} <br />{/if}
			{$data.registered_office.postcode} <br />
			{$data.registered_office.country}
		</td>
	</tr>
	</table>

{* SHARE CAPITAL *}
{if $data.company_details.company_category != 'BYGUAR'}
	<h2>Share Capital</h2>
	{foreach from=$data.capitals key="key" item="capital"}
	<table class="grid2 full-width">
	<col width="250">
	<tr>
		<th>Total issued:</th>
		<td>{$capital.total_issued}</td>
	</tr>
	<tr>
		<th>Currency:</th>
		<td>{$capital.currency}</td>
	</tr>
	<tr>
		<th>Total Aggregate Value:</th>
		<td>{$capital.total_aggregate_value}</td>
	</tr>
	</table>
	{* SHARES *}
    {*
	{foreach from=$capital.shares key="key2" item="share"}
		<h3>Share {$key+1}.{$key2+1}:</h3>
		<table class="grid2 full-width">
		<tr>
			<th>share_class</th>
			<td>{$share.share_class}</td>
		</tr>
		<tr>
			<th>prescribed_particulars</th>
			<td>{$share.prescribed_particulars}</td>
		</tr>
		<tr>
			<th>num_shares</th>
			<td>{$share.num_shares}</td>
		</tr>
		<tr>
			<th>amount_paid</th>
			<td>{$share.amount_paid}</td>
		</tr>
		<tr>
			<th>amount_unpaid</th>
			<td>{$share.amount_unpaid}</td>
		</tr>
		<tr>
			<th>nominal_value</th>
			<td>{$share.nominal_value}</td>
		</tr>
		</table>
	{/foreach}
	*}
	{/foreach}
{/if}

	{* DIRECTORS *}
	{if !empty($data.directors)}
		<h2>Directors</h2>
		<table class="grid2 full-width">
		<col>
		<tr>
			<th>Name</th>
		</tr>
		{foreach from=$data.directors key="id" item="director"}
		<tr>
			<td>
				{if $director.corporate}
					{$director.corporate_name}
				{else}
					{$director.title} {$director.forename} {$director.surname}
				{/if}
			</td>
		</tr>
		{/foreach}
		</table>
	{/if}


	{* SHAREHOLDERS *}
	{if !empty($data.shareholders)}
        {if $data.company_details.company_category == 'BYGUAR'}
		    <h2>Members</h2>
        {else}
            <h2>Shareholders</h2>
        {/if}
		<table class="grid2 full-width">
		<col>
		<tr>
			<th>Name</th>
		</tr>
		{foreach from=$data.shareholders key="id" item="shareholder"}
		<tr>
			<td>
				{if $shareholder.corporate}
					{$shareholder.corporate_name}
				{else}
					{$shareholder.title} {$shareholder.forename} {$shareholder.surname}
				{/if}
			</td>
		</tr>
		{/foreach}
		</table>
	{/if}

	{* SECRETARIES *}
	<h2>Secretaries</h2>
	{if !empty($data.secretaries)}
		<table class="grid2 full-width">
		<col>
		<tr>
			<th>Name</th>
		</tr>
		{foreach from=$data.secretaries key="id" item="secretary"}
		<tr>
			<td>
				{if $secretary.corporate}
					{$secretary.corporate_name}
				{else}
					{$secretary.title} {$secretary.forename} {$secretary.surname}
				{/if}
			</td>
		</tr>
		{/foreach}
		</table>
	{else}
		<p>No secretaries</p>
	{/if}


	{* SUBSCRIBERS *}
	{if !empty($data.subscribers)}
        {if $data.company_details.company_category != 'BYGUAR'}
		<div style="position:relative; float:left;margin: 10px 15px 10px 5px;"><h2 style="display:inline">Subscribers</h2></div>
				<div class="help-button2" style="position:relative; float:left; margin: 10px 15px 10px 5px;">
        			<a href="#" class="help-icon">help</a>
        			<em>Subscribers are shareholders at incorporation.</em>
    			</div>
    			<h2>&nbsp;</h2>
        {else}
		<h2>Members at Incorporation</h2>
        {/if}
		<table class="grid2 full-width">
        {if $data.company_details.company_category != 'BYGUAR'}
		<col>
		<col width="90">
		<col width="90">
		<col width="100">
			{if !isset($doNotShowSubsLink)}
			<col width="128">
			{/if}
        {/if}
		<tr>
			<th>Name</th>
            {if $data.company_details.company_category != 'BYGUAR'}
			<th>Shares</th>
			<th>Currency</th>
			<th>Share value</th>
				{if !isset($doNotShowSubsLink)}
				<th>Share Certificate(s)</th>
				{/if}
            {/if}
		</tr>
		{foreach from=$data.subscribers key="id" item="subscriber"}
		<tr>
			{if isset($subscriber.corporate_name)}
				<td>{$subscriber.corporate_name}</td>
			{else}
				<td>{$subscriber.forename} {$subscriber.surname}</td>
			{/if}
            {if $data.company_details.company_category != 'BYGUAR'}
			<td class="center">{$subscriber.shares}</td>
			<td class="center">{$subscriber.currency}</td>
			<td class="center">{$subscriber.share_value}</td>
            {/if}
		</tr>
		{/foreach}
		</table>
	{/if}

	{* INCORPORATION DOCUMENTS *}
	<h2>Incorporation Documents</h2>
	<table class="grid2 full-width">
	<col width="250">
	<tr>
		<td>
	        <ul>
	        {foreach from=$documents item='document'}
	            <li>
                    {if $document == 'article.pdf'}
                        Articles/Memorandum
                    {else}
                        {$document}
                    {/if}
	            </li>
	        {/foreach}
	        </ul>
	    </td>
	</tr>
	</table>

	{* REQUEST HISTORY *}
	<h2>Request History</h2>
	<table class="grid2 full-width">
	<col width="100">
	<col>
	<col width="100">
	<col width="100">
	<tr>
		<th class="center">Id</th>
		<th>Type</th>
		<th class="center">Status</th>
		<th class="center">Date Submitted</th>
	</tr>
	{foreach $view->getFormSubmissions() as $submission}
		<tr>
			<td class="center">{$submission->getId()}</td>
			<td>{$submission->getFormIdentifier()}</td>
			<td class="center">{$submission->getResponseMessage()}</td>
			<td class="center">{$submission->getLastSubmissionDate()|date_format:"%d/%m/%Y"}</td>
		</tr>
	{/foreach}
	</table>
	<br />
	<br />
</div>

{include file="@footer.tpl"}
