{include file="@header.tpl"}

<div id="maincontent" class="cu-summary-page" style="padding: 0 0 30px 0;">

	{literal}
    <script type="text/javascript">
        <!--
        $(function(){
            $('.cu-summary-container .menu .buttons')
            .show()
            .buttonset();

            $(".text")
            .button().attr('style', 'cursor: text')
            .unbind('mouseover');

            $(".select")
            .button({
                text: false,
                icons: {
                    primary: 'ui-icon-triangle-1-s'
                }
            })
            .click(function() {
                $ul = $('ul.cu-summary-container-links');
                if ($ul.is(':visible')) {
                    $(this).removeClass('ui-state-highlight')
                } else {
                    $(this).addClass('ui-state-highlight')
                }
                $ul.toggle();
            });

          $('.error').attr('style', 'margin-bottom: 20px;');
        });
        //-->
    </script>
	{/literal}

    <script>
        {if $companyView->hasAuthenticationCode()}
            companyHasAuthenticationCode = true;
        {else}
            companyHasAuthenticationCode = false;
        {/if}
    </script>

    <div class="cu-summary-container">
        {if $visibleTitle}
        <h1 style="width: 830px">{$title}
        {if $data.company_details.company_category == 'BYSHR'}
            <a style="float: right; font-size: 20px;font-weight: bold;" href="{$this->router->link(1174)}">HELP</a>
        {/if}
        </h1>
        {/if}
        <div class="menu">

            <div class="buttons">
                <a class="text">Quick Links</a>
                <a class="select">Select an action</a>
            </div>
            <ul class="cu-summary-container-links ui-widget-content ui-corner-all">
                <li><a class="auth-code-required" href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId", "sync=1")}">Sync Data with Companies House</a></li>
                <li><a class="auth-code-required" href="{$this->router->link("CURegisteredOfficeControler::REGISTER_OFFICE_PAGE", "company_id=$companyId")}">Update Registered Office</a></li>

                {if $data.company_details.company_category != 'LLP'}
                    <li><a class="auth-code-required" href="{$this->router->link("CUDirectorsControler::ADD_DIRECTOR_PERSON_PAGE", "company_id=$companyId")}">Appoint New Director</a></li>
                    <li><a class="auth-code-required" href="{$this->router->link("CUSecretariesControler::ADD_SECRETARY_PERSON_PAGE", "company_id=$companyId")}">Appoint New Secretary</a></li>
                    <li><a class="auth-code-required" href="{$this->router->link("CUReturnAllotmentSharesControler::SUMMARY_PAGE", "company_id=$companyId")}">Return of Allotment of Shares</a></li>
                {/if}
                {if $data.company_details.company_category == 'LLP'}
                    <li><a class="auth-code-required" href="{$this->router->link("CUDirectorsControler::ADD_DIRECTOR_PERSON_PAGE", "company_id=$companyId")}">Appoint New Member</a></li>
                {/if}
                <li><a class="auth-code-required" href="{$this->router->link("AnnualReturnControler::COMPANY_DETAILS_PAGE", "company_id=$companyId")}">File Confirmation Statement</a></li>
                {if $customer->roleId == 'wholesale'}
                    {if $data.company_details.company_category != 'LLP'}
                        <li><a href="{$this->router->link("CompanyDividendsControler::DIVIDENDS_PAGE", "company_id=$companyId")}">Dividends</a></li>
                    {/if}
                {/if}

                <li><a href="{$this->router->link("CUEReminderControler::EREMINDER_PAGE", "company_id=$companyId")}">eReminders (Accounts & Returns)</a></li>
                <!--<li><a href="{$this->router->link("CURemindersControler::REMINDERS_PAGE", "company_id=$companyId")}">Reminder: Annual Return/Accounts</a></li> -->

                <li><a href="{$this->router->link("DeleteCompanyControler::DELETE_COMPANY_PAGE", "company_id=$companyId")}">Delete company from system</a></li>
            </ul>

        </div>
    </div> <!-- .cu-summary-container -->

    {if $data.company_details.company_category != 'LLP'}
        {if $bankingView->canShowBankOffer()}
            <div class="alert alert-info {if $bankingView->canDismissBankOffer()}alert-dismissible fade in{/if}" role="alert">
                {if $bankingView->canDismissBankOffer()}
                    <button id="dismiss-bank-option" type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {/if}
                <div class="row">
                    <div class="col-sm-9">
                        <i class="fa fa-university fa-fw fa-3x pull-left" aria-hidden="true"></i>
                        <span class="largefont f-sterling">Every limited company requires a bank account to start trading. Bank with one of our partners and receive <strong>{$bankingView->getCashBackAmount()|currency:0} cash back</strong>.</span>
                    </div>
                    <div class="col-sm-1">
                        <a class="btn btn-orange mtop_small f-sterling" href="{url route='banking_module.choose_bank' companyId=$company->getId()}">Apply Now</a>
                    </div>
                </div>
            </div>
            {if $bankingView->canDismissBankOffer()}
            <script type="text/javascript">
                bankOptions.dismissBankOffer('#dismiss-bank-option', '{url route='banking_module.dismiss_offer' companyId=$company->getId()}');
            </script>
            {/if}
        {/if}
    {/if}

	{* AUTHCODE *}
    {if !$companyView->hasAuthenticationCode()}
        <div class="flash info2 auth-code-flash">
            <b>Missing Companies House Authentication Code</b><br>

            <p>To be able to synchronize your company information with Companies House you must provide the company
                Authentication Code.</p>

            <p>
                <a class="popupnew" href="{$this->router->link("CUChangeWebAuthCodeControler::PAGE_CHANGE_AUTH_CODE", "company_id=$companyId")}">Enter Authentication Code</a>
                <a href="/project/blog/everything-you-need-to-know-about-the-companies-house-webfiling-authentication-code/" target="_blank">Learn more</a>
            </p>
        </div>
    {/if}

	{* === COMPANY DETAILS === *}
    <h2>
		Company Details
        <span style="font-size: 12px; font-weight: 400;"> <a class="auth-code-required" href="{$this->router->link("CUSummaryControler::SUMMARY_PAGE", "company_id=$companyId", "sync=1")}" >Sync Data with Companies House</a></span>
        {*{if $bronze != 1}*}
        <span style="padding-left:170px; font-size: 12px;font-weight: 400;float:right;"><a href="{$this->router->link(null, "summary=1")}" >Download Company Statutory Info (PDF)</a></span>
        {*{/if}*}
    </h2>
    <table class="grid2 full-width" style="margin-bottom: 20px;">

        <col width="250">
        <tr>
            <th>Company Name: </th>
            <td>{$data.company_details.company_name}
                    <a class="auth-code-required" href="{$this->router->link("CUChangeNameControler::CHANGE_NAME_PAGE","company_id=$companyId")}" >Change Company Name</a>
            </td>
        </tr>
        <tr>
            <th>Registration Number: </th>
            <td>{$data.company_details.company_number}</td>
        </tr>
        <tr>
            <th>Registered In: </th>
	{if $data.company_details.jurisdiction == 'EW'}
            <td>England and Wales</td>
	{elseif $data.company_details.jurisdiction == 'SC'}
            <td>Scotland</td>
	{elseif $data.company_details.jurisdiction == 'WA'}
            <td>Wales</td>
	{elseif $data.company_details.jurisdiction == 'NI'}
            <td>Northern Ireland</td>
	{elseif $data.company_details.jurisdiction == 'EU'}
            <td>Europe</td>
	{elseif $data.company_details.jurisdiction == 'UK'}
            <td>United Kingdom</td>
	{elseif $data.company_details.jurisdiction == 'EN'}
            <td>England</td>
	{elseif $data.company_details.jurisdiction == 'OTHER'}
            <td>Other</td>
	{else}
            <td></td>
	{/if}
        </tr>
        <tr>
            <th>Web Filing Authentication Code: </th>
            <td>
                {$data.company_details.authentication_code} &nbsp; &nbsp;
                <a class="popupnew" href="{$this->router->link("CUChangeWebAuthCodeControler::PAGE_CHANGE_AUTH_CODE", "company_id=$companyId")}">Update Code</a>
            </td>
        </tr>
        <tr>
            <th>Company Category:</th>
            <td>{$data.company_details.company_category}</td>
        </tr>
        <tr>
            <th>Company Status:</th>
            <td>{$data.company_details.company_status}</td>
        </tr>
        <tr>
            <th>Country of Origin: </th>
            <td>{$data.company_details.country_of_origin}</td>
        </tr>
        <tr>
            <th>Incorporation Date: </th>
            <td>{$data.company_details.incorporation_date|date_format:"%d/%m/%Y"}</td>
        </tr>
        <tr>
            <th>Bank Account Application: </th>
            <td>
                {$bankingView->getAppliedBankNamesAsString('/')}
                {if $bankingView->canApplyForCashBack()}
                <a href="{url route='banking_module.choose_bank' companyId=$company->getId()}">
                    {if $bankingView->hasAppliedBankCashBacks()}
                        More Options
                    {else}
                        Apply Now
                    {/if}
                    </a>
                {/if}
                {if !$bankingView->hasAppliedBankCashBacks()}
                    for your company's bank account with {$bankingView->getCashBackAmount()|currency:0} cash back
                {/if}
            </td>
        </tr>
        <tr>
            <th>Nature of Business: </th>
            <td>{$data.company_details.sic_codes}</td>
        </tr>
        <tr>
            <th>Accounting Reference Date: </th>
            <td>{$data.company_details.accounts_ref_date|replace:'-':'/'}
                <!--{if $data.company_details.company_category != 'LLP'}
                    <a href="{$this->router->link("CUChangeDateControler::CHANGE_DATE_PAGE","company_id=$companyId")}" >Change Date</a></td>
                {/if}-->
        </tr>
        <tr>
            <th>Last Accounts Made Up Date: </th>
            <td>{$data.company_details.accounts_last_made_up_date|date_format:"%d/%m/%Y"}</td>
        </tr>
        <tr>
            <th>Next Accounts Due: </th>
            <td>{$data.company_details.accounts_next_due_date|date_format:"%d/%m/%Y"}</td>
        </tr>
        <tr>
            <th>Last Confirmation Statement Made Up To: </th>
            <td>{$data.company_details.returns_last_made_up_date|date_format:"%d/%m/%Y"}</td>
        </tr>
        <tr>
            <th>Next Confirmation Statement Due: </th>
            <td>{$data.company_details.returns_next_due_date|date_format:"%d/%m/%Y"}
                <a class="auth-code-required" href="{$this->router->link("AnnualReturnControler::COMPANY_DETAILS_PAGE", "company_id=$companyId")}" >File Confirmation Statement</a>
            </td>
        </tr>
	{*
        <tr>
            <th>Last Members List: </th>
            <td></td>
        </tr>
        <tr>
            <th>Previous Names: </th>
            <td></td>
        </tr>
	*}
    </table>

	{* === REGISTERED OFFICE === *}
    <h2>
		Registered Office
        <span style="font-size: 12px; font-weight: 400;">
            <a class="auth-code-required" href="{$this->router->link("CURegisteredOfficeControler::REGISTER_OFFICE_PAGE", "company_id=$companyId")}" >Update</a>
        </span>
    </h2>

    <table class="grid2 full-width" style="margin-bottom: 20px;">
        <col width="250">
        <tr>
            <th>Address</th>
        </tr>

        <tr>
            <td>
			{$data.registered_office.premise} {$data.registered_office.street} <br />
			{if !empty($data.registered_office.thoroughfare)}{$data.registered_office.thoroughfare} <br />{/if}
			{if !empty($data.registered_office.post_town)}{$data.registered_office.post_town} <br />{/if}
			{if !empty($data.registered_office.county)}{$data.registered_office.county} <br />{/if}
			{$data.registered_office.postcode} <br />
			{$data.registered_office.country}
            </td>
        </tr>
    </table>

    {if $data.company_details.company_category != 'LLP'}
    {* === SHARE CAPITAL === *}
    {if $data.company_details.company_category != 'BYGUAR'}
    <h2>
		Share Capital
		{if !empty($data.company_details.company_number) and !empty($data.company_details.company_name)}
        <span style="font-size: 12px; font-weight: 400;">
            <a class="auth-code-required" href="{$this->router->link("CUReturnAllotmentSharesControler::SUMMARY_PAGE", "company_id=$companyId")}" >Return of Allotment of Shares</a>
        </span>
		{/if}
    </h2>

	{foreach from=$data.capitals key="key" item="capital"}
    <table class="grid2 full-width" style="margin-bottom: 20px;">
        <col width="250">
        <tr>
            <th>Total issued:</th>
            <td>{$capital.total_issued}</td>
        </tr>
        <tr>
            <th>Currency:</th>
            <td>{$capital.currency}</td>
        </tr>
        <tr>
            <th>Total Aggregate Value:</th>
            <td>{$capital.total_aggregate_value}</td>
        </tr>
    </table>
	{* SHARES *}
    {*
	{foreach from=$capital.shares key="key2" item="share"}
    <h3>Share {$key+1}.{$key2+1}:</h3>
    <table class="grid2 full-width">
        <tr>
            <th>share_class</th>
            <td>{$share.share_class}</td>
        </tr>
        <tr>
            <th>prescribed_particulars</th>
            <td>{$share.prescribed_particulars}</td>
        </tr>
        <tr>
            <th>num_shares</th>
            <td>{$share.num_shares}</td>
        </tr>
        <tr>
            <th>amount_paid</th>
            <td>{$share.amount_paid}</td>
        </tr>
        <tr>
            <th>amount_unpaid</th>
            <td>{$share.amount_unpaid}</td>
        </tr>
        <tr>
            <th>nominal_value</th>
            <td>{$share.nominal_value}</td>
        </tr>
    </table>
	{/foreach}
	*}
	{/foreach}
    {/if}
{/if}
	{* === DIRECTORS === *}
    <h2>
        {if $data.company_details.company_category != 'LLP'}
		Directors
        <span style="font-size: 12px; font-weight: 400;">
            <a class="auth-code-required" href="{$this->router->link("CUDirectorsControler::ADD_DIRECTOR_PERSON_PAGE", "company_id=$companyId")}" >Appoint New Director</a>
            {else}
            Members
            <span style="font-size: 12px; font-weight: 400;">
            <a class="auth-code-required" href="{$this->router->link("CUDirectorsControler::ADD_DIRECTOR_PERSON_PAGE", "company_id=$companyId")}" >Appoint New Member</a>
        {/if}
        </span>
    </h2>

    <table class="grid2 full-width" style="margin-bottom: 20px;">
        <col width="600">
        <col width="60">
        <col width="60">
        <col width="60">
        <tr>
            <th>Name</th>
                <th colspan="3" class="center">Action</th>
        </tr>
	{if !empty($data.directors)}
		{foreach from=$data.directors key="id" item="director"}
        <tr>
            <td>
				{if $director.corporate}
					{$director.corporate_name}
				{else}
					{$director.title} {$director.forename} {$director.middle_name} {$director.surname}
				{/if}
            </td>
			{if $director.corporate}
                    <td class="center"><a href="{$this->router->link("CUDirectorsControler::SHOW_DIRECTOR_CORPORATE_PAGE", "company_id=$companyId", "director_id=$id")}" >View</a></td>
                    <td class="center"><a class="auth-code-required" href="{$this->router->link("CUDirectorsControler::EDIT_DIRECTOR_CORPORATE_PAGE", "company_id=$companyId", "director_id=$id")}" >Edit</a></td>
                    <td class="center"><a class="auth-code-required-popup" href="{$this->router->link("CUSummaryControler::RESIGNATION_PAGE", "company_id=$companyId", "officer_id=$id" , "resign=1")}">Resign</a></td>
            {else}
                    <td class="center"><a href="{$this->router->link("CUDirectorsControler::SHOW_DIRECTOR_PERSON_PAGE", "company_id=$companyId", "director_id=$id")}" >View</a></td>
                    <td class="center"><a class="auth-code-required" href="{$this->router->link("CUDirectorsControler::EDIT_DIRECTOR_PERSON_PAGE", "company_id=$companyId", "director_id=$id")}" >Edit</a></td>
                    <td class="center"><a class="auth-code-required-popup" href="{$this->router->link("CUSummaryControler::RESIGNATION_PAGE", "company_id=$companyId", "officer_id=$id" , "resign=1")}">Resign</a></td>

            {/if}
        </tr>
		{/foreach}
	{else}
        <tr>
            <td colspan="4">No Directors</td>
        </tr>
	{/if}
    </table>

    {if $data.company_details.company_category != 'LLP'}

	    {* ==== SHAREHOLDERS === *}
	    {if $data.company_details.company_category == 'BYGUAR'}
            <h2>Members</h2>
	    {else}
            <h2>
                Shareholders
                {if $data.company_details.company_category == 'BYSHR'}
                    <span style="font-size: 12px; font-weight: 400;">
                        <a href="{$this->router->link("CURegisterMemberControler::PAGE_LIST", "company_id=$companyId")}">View Register of Members List</a>
                    </span>
                {/if}
            </h2>
	    {/if}

        <table class="grid2 full-width" style="margin-bottom: 20px;">
        <col>
        <col width="90">
        <col width="100">
        {if !$hasBronzeProduct}
        <col width="140">
        {/if}
        <tr>
            <th>Name</th>
            <th>Shares</th>
            <th>ShareClass</th>
             {if !$hasBronzeProduct}
            <th class="center">Share Certificate(s)</th>
            {/if}
        </tr>
    {if !empty($data.shareholders)}
        {assign var=i value=1}
		{foreach from=$data.shareholders key="id" item="shareholder"}
        <tr>
            <td>
					{if $shareholder.corporate}
						{$shareholder.corporate_name}
					{else}
						{$shareholder.title} {$shareholder.forename} {$shareholder.surname}
					{/if}
            </td>
            <td class="center"> {$shareholder.num_shares}</td>
            <td class="center"> {$shareholder.share_class}</td>
             {*{if !$hasBronzeProduct} *}
            <td class="center">

                <a href="{$this->router->link("CompanyShareholderCerificateControler::SHAREHOLDERS_CERTIFICATE_PAGE", "company_id=$companyId", "shareholderId=$id")}" >Download</a>
               </td>
           {* {/if} *}

            {assign var=i value=$i+1}
        </tr>
		{/foreach}
	{else}
        <tr>
            <td colspan="4">No shareholders</td>
        </tr>
	{/if}
    </table>


	{* === SECRETARIES === *}
    <h2>Secretaries
        <span style="font-size: 12px;font-weight: 400;">
            <a class="auth-code-required" href="{$this->router->link("CUSecretariesControler::ADD_SECRETARY_PERSON_PAGE", "company_id=$companyId")}" >Appoint New Secretary</a>
        </span>
    </h2>

    <table class="grid2 full-width" style="margin-bottom: 20px;">
        <col width="600">
        <col width="60">
        <col width="60">
        <col width="60">
        <tr>
            <th>Name</th>
            <th colspan="3" class="center">Action</th>
        </tr>
	{if !empty($data.secretaries)}
		{foreach from=$data.secretaries key="id" item="secretary"}
        <tr>
            <td>
					{if $secretary.corporate}
						{$secretary.corporate_name}
					{else}
						{$secretary.title} {$secretary.forename} {$secretary.middle_name} {$secretary.surname}
					{/if}
            </td>
				{if $secretary.corporate}
            <td class="center"><a href="{$this->router->link("CUSecretariesControler::SHOW_SECRETARY_CORPORATE_PAGE", "company_id=$companyId", "secretary_id=$id")}" >View</a></td>
            <td class="center"><a class="auth-code-required" href="{$this->router->link("CUSecretariesControler::EDIT_SECRETARY_CORPORATE_PAGE", "company_id=$companyId", "secretary_id=$id")}" >Edit</a></td>
            <td class="center"><a class="auth-code-required-popup" href="{$this->router->link("CUSummaryControler::RESIGNATION_PAGE", "company_id=$companyId", "officer_id=$id" , "resign=1")}">Resign</a></td>
				{else}
            <td class="center"><a href="{$this->router->link("CUSecretariesControler::SHOW_SECRETARY_PERSON_PAGE", "company_id=$companyId", "secretary_id=$id")}" >View</a></td>
            <td class="center"><a class="auth-code-required" href="{$this->router->link("CUSecretariesControler::EDIT_SECRETARY_PERSON_PAGE", "company_id=$companyId", "secretary_id=$id")}" >Edit</a></td>
            <td class="center"><a class="auth-code-required-popup" href="{$this->router->link("CUSummaryControler::RESIGNATION_PAGE", "company_id=$companyId", "officer_id=$id" , "resign=1")}">Resign</a></td>
				{/if}
        </tr>
		{/foreach}
	{else}
        <tr>
            <td colspan="4">No Secretaries</td>
        </tr>
	{/if}
    </table>

    {if Feature::featurePsc()}
        {* todo: psc - refactor, summaryView? PscAware? *}
        <h2>
            People with Significant Control (PSC)
            <span style="font-size: 12px; font-weight: 400;">
                <a href="{url route="CURegisterPscControler::PAGE_LIST" company_id=$companyId}">View PSC Online Register</a>
            </span>
        </h2>
        <p>
            This is the PSC information that is held on Companies House. This information may be different from what is in the <a href="{url route="CURegisterPscControler::PAGE_LIST" company_id=$companyId}">PSC Online Register</a>.
            <br>
            To update the Companies House PSC information please file a <a href="{url route="AnnualReturnControler::COMPANY_DETAILS_PAGE"  company_id=$companyId}">Confirmation Statement</a>.
        </p>
        <table class="grid2 full-width" style="margin-bottom: 20px;">
            {if $summaryView->hasNoPscReason()}
                <tr>
                    <td><i>{$summaryView->getNoPscReason()}</i></td>
                </tr>
            {elseif $summaryView->hasPscs()}
                <tr>
                    <th width="150">Name</th>
                    <th>Details</th>
                </tr>
                {foreach $summaryView->getPscs() as $psc}
                    <tr>
                        <td>
                            {$psc->getFullName()}
                        </td>
                        <td>
                            {if $psc->hasStatementNotification()}
                                <i>{$psc->getStatementNotificationText()}</i>
                            {else}
                                {foreach $psc->getNatureOfControlsTexts() as $noc}
                                    {$noc}<br>
                                {/foreach}
                            {/if}
                        </td>
                    </tr>
                {/foreach}
            {else}
                <tr>
                    <td>No PSCs</td>
                </tr>
            {/if}
        </table>
    {/if}

    {if $companyView->hasServices()}
        {* === COMPANY SERVICES === *}
        <h2>My Services
            <span style="font-size: 12px; font-weight: 400;">
                <a href="{$this->router->link("MyServicesControler::PAGE_SERVICES", "companyId=$companyId")}#company={$companyView->getId()}" >Manage services</a>
            </span>
        </h2>
        <table class="grid2 full-width" style="margin-bottom: 20px;">
            <col>
            <col width="130">
            <col width="100">
            <col width="100">
            <tr>
                <th>Service</th>
                <th class="center">Status</th>
                <th class="center">Expires on</th>
                <th class="center">Manage</th>
            </tr>
            {$services = $companyView->getServices()}
            {foreach $services as $service}
                <tr>
                    <td>{$service->getServiceName()}</td>
                    <td title="{$service->getStatus()}"><i style="width:20px;" class="{$service->getStatusCSSClass()}"></i>{$service->getStatus()}</td>

                    {if $service->isRenewable()}
                        <td class="txtcenter">{$service->dtExpires|datetime}</td>
                        {assign var="serviceId" value=$service->getId()}
                        <td class="center"><a href="{$this->router->link("MyServicesControler::PAGE_SERVICES", "serviceId=$serviceId", "companyId={$companyView->getId()}")}#company={$companyView->getId()}" >Manage</a></td>
                    {else}
                        <td class="txtcenter" colspan="2"></td>
                    {/if}
                </tr>
            {/foreach}
        </table>
    {/if}
	{* === SUBSCRIBERS === *}
	{if !empty($data.subscribers)}
    {if $data.company_details.company_category != 'BYGUAR'}
    <div style="position:relative; float:left;margin: 10px 15px 10px 5px;"><h2 style="display:inline">Subscribers</h2></div>
    <div class="help-button" style="position:relative; float:left; margin: 10px 15px 10px 5px;">
        <a href="#" class="help-icon">help</a>
        <em>Subscribers are shareholders at incorporation.</em>
    </div>
    {else}
    <h2>Members at Incorporation</h2>
    {/if}

    <table class="grid2 full-width" style="margin-bottom: 20px;">
        {if $data.company_details.company_category != 'BYGUAR'}
        <col>
        <col width="90">
        <col width="90">
        <col width="100">
			{if !isset($doNotShowSubsLink)}<col width="128">{/if}
        {/if}
        <tr>
            <th>Name</th>
            {if $data.company_details.company_category != 'BYGUAR'}
            <th>Shares</th>
            <th>Currency</th>
            <th>Share value</th>
			{if !isset($doNotShowSubsLink)}<th>Share Certificate(s)</th>{/if}
            {/if}
        </tr>
		{foreach from=$data.subscribers key="id" item="subscriber"}
        <tr>
			{if isset($subscriber.corporate_name)}
            <td>{$subscriber.corporate_name}</td>
			{else}
            <td>{$subscriber.forename} {$subscriber.surname}</td>
			{/if}
            {if $data.company_details.company_category != 'BYGUAR'}
            <td class="center">{$subscriber.shares}</td>
            <td class="center">{$subscriber.currency}</td>
            <td class="center">{$subscriber.share_value}</td>
				{if !isset($doNotShowSubsLink)}<td class="center"><a href="{$this->router->link(null, "subscriberCertificate=$id")}" >Download</a></td>{/if}
            {/if}
        </tr>
		{/foreach}
    </table>
	{/if}
{/if}
	{* === INCORPORATION DOCUMENTS === *}
    <h2>My Company Documents</h2>

    <table class="grid2 full-width" style="margin-bottom: 20px;">
        <col>
        <col width="190">
        <tr>
            <th>Document</th>
            <th class="center">Action</th>
        </tr>
	{if !empty($documents)}

		{foreach from=$documents item='document'}
        <tr>
            <td>
					{if $document == 'article.pdf'}
		            	Articles/Memorandum
		            {else}
		            	{$document}
					{/if}
            </td>
            <td class="center">
                <a href="{$this->router->link(null, "document_name=$document")}" >View</a>
            </td>
        </tr>
        {/foreach}

	{else}
        <tr>
            <td colspan="2">No Documents</td>
        </tr>
	{/if}
    </table>

	{* === OTHER COMPANY DOCUMENTS === *}
    <h2>Other Company Documents
        <span style="font-size: 12px; font-weight: 400;">
            <a href="{$this->router->link("CUDocumentsControler::PAGE_ADD_DOCUMENT", "company_id=$companyId")}" >Upload Document</a>
        </span>
    </h2>
    <table class="grid2 full-width" style="margin-bottom: 20px;">
        <col>
        <col width="100">
        <col width="100">
        <tr>
            <th>Document</th>
            <th colspan="2" class="center">Action</th>
        </tr>
	{if !empty($otherdocuments)}
		{foreach from=$otherdocuments item='document'}
        {assign var="documentId" value=$document->getId()}
            <tr>
                <td>
                    {$document->getRealFileName()}
                </td>
                <td class="center">
                    <a href="{$this->router->link("CUDocumentsControler::PAGE_ADD_DOCUMENT", "do={CUDocumentsControler::ACTION_DOWNLOAD}", "company_id=$companyId", "document_id=$documentId")}">view</a>
                </td>
                <td class="center">
                    <a href="{$this->router->link("CUDocumentsControler::PAGE_ADD_DOCUMENT", "do={CUDocumentsControler::ACTION_DELETE}", "company_id=$companyId", "document_id=$documentId")}" onclick="return confirm('Are you sure');">delete</a>
                </td>
            </tr>
        {/foreach}
	{else}
        <tr>
            <td colspan="3">No Documents</td>
        </tr>
	{/if}
    </table>

	{* === REQUEST HISTORY === *}
    <h2>Request History</h2>

    <table class="grid2 full-width">
        <col width="100">
        <col>
        <col width="100">
        <col width="100">
        <tr>
            <th class="center">Id</th>
            <th>Type</th>
            <th class="center">Status</th>
            <th class="center">Date Submitted</th>
            <th class="center">Submissions</th>
            {if $data.company_details.company_category != 'LLP'}
                <th class="center">Minutes</th>
            {/if}
        </tr>
    	{if $view->hasFormSubmissions()}
            {foreach $view->getFormSubmissions() as $submission}
                {assign var="submissionId" value=$submission->getId()}
                <tr>
                    <td class="center">{$submissionId}</td>
                    <td>{$submission->getFormIdentifier()}</td>
                    <td class="center">{$submission->getResponseMessage()}</td>
                    <td class="center">
                        {$submission->getLastSubmissionDate()|date_format:"%d/%m/%Y"}
                    </td>
                    <td class="center">
                        {if !$submission->isPending()}
                            <a href="{$view->getViewLink($companyId, $submission)}">View</a>
                        {/if}
                    </td>
                    {if $data.company_details.company_category != 'LLP'}
                        <td class="center">
                            {if $view->canViewMinutes($submission)}
                                <a href="{$this->router->link(NULL, ['formid' => $submissionId])}">View</a>
                            {/if}
                        </td>
                    {/if}
                </tr>
            {/foreach}
        {else}
            <tr>
                <td colspan="6">No Requests</td>
            </tr>
        {/if}
    </table>

	{* === BARCLAYS EXPLANATION
	{if isset($barclays)}
    <p class="barclays-explanation">
			{if $customer->isWholesale()}
				*) The &pound;50 cashback payment is only made when your client (UK only) successfully opens a Barclays Bank Account. Once successful account opens are reconciled, &pound;50 will be added as credit to your companiesmadesimple.com account.
			{else}
				*) Cash back only applies to UK customers who form their company through Companies Made Simple and open a Barclays Business Bank Account. Offer valid for all new company formation purchases after 17 March 2010. &pound;50 cash back will be transferred into your account once you have filled in the cash back form, emailed it to us and we have verified the details with Barclays.
			{/if}
    </p>
	{/if}
	*}

</div>

{include file="@footer.tpl"}
