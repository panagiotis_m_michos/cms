<?php

class LTDCalculatorControler extends DefaultControler
{
    /* pages */
    const DEFAULT_PAGE = 726;
    const RESULT_PAGE = 727;

    /** @var array */
    public $possibleActions = array(
        self::DEFAULT_PAGE => 'default',
        self::RESULT_PAGE => 'result',
    );

    /**
     * @var LTDCalculatorModel
     */
    public $node;

    public function renderDefault()
    {
        try {
            $form = new LtdCalculatorForm('LtdCalculator');
            $form->startup();
            $this->template->form = $form;
            if ($form->isOk2Save()) {
                $annualProfit = $form->getValue('annualProfit');
                $form->clean();
                $this->redirect(self::RESULT_PAGE, "annualProfit=$annualProfit");	
            }
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    public function renderResult()
    {
        try {
            if (empty($this->get['annualProfit'])) {
                throw new InvalidArgumentException('Annual Profit is required!');
            }

            $ltdCalculator = new LTDCalculator($this->get['annualProfit']);
            $this->template->selfEmployed = $ltdCalculator->getSelfEmployed();
            $this->template->company = $ltdCalculator->getCompany();
            $this->template->saving = $ltdCalculator->getSaving();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect(self::DEFAULT_PAGE);
        }
    }
}
