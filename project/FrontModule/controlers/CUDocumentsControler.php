<?php

use Exceptions\Business\Forbidden;
use Services\CompanyDocumentService;

class CUDocumentsControler extends CUControler
{
    const PAGE_ADD_DOCUMENT = 894;

    /**
     * @var array
     */
    public $possibleActions = array(
        self::PAGE_ADD_DOCUMENT => 'default',
    );

    const ACTION_DOWNLOAD = 'download';
    const ACTION_DELETE = 'delete';

    /**
     * @var string
     */
    static public $handleObject = 'CompanyOtherDocumentsModel';
    
    /**
     * @var CompanyOtherDocumentsModel
     */
    public $node;

    /**
     * @var CompanyDocumentService
     */
    private $companyDocumentService;

    public function startup()
    {
        parent::startup();
        $this->companyDocumentService = $this->getService(DiLocator::SERVICE_COMPANY_DOCUMENT);
    }

    /*     * ******************************************* add documents ************************************************** */

    public function beforePrepare()
    {
        parent::beforePrepare();
        try {
            $action = $this->getParameter('do');
            $documentId = $this->getParameter('document_id');

            if ($documentId) {
                try {
                    $companyDocument = $this->companyDocumentService->getCompanyDocument($this->companyEntity, $documentId);
                } catch (Forbidden $e) {
                    throw new Forbidden('Document does not exist or does not belong to you');
                }

                if ($action == self::ACTION_DELETE) {
                    $this->companyDocumentService->deleteCompanyDocument($companyDocument);
                    $this->flashMessage('Your document was successfully deleted.');
                    $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id' => $this->company->getCompanyId()));
                } elseif ($action == self::ACTION_DOWNLOAD) {
                    $this->companyDocumentService->outputCompanyDocument($companyDocument);
                }
            }
            $this->node->checkMaxFilesUpload($this->company);
            
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id' => $this->company->getCompanyId()));
        }

        
    }
    
    public function renderDefault()
    {
        $form = new CUDocumentsUploadForm($this->node->getId() . '_CUDocumentsUpload');
        $form->startup($this, array($this, 'Form_CUDocumentsUploadFormSubmitted'), $this->company);
        $this->template->form = $form;
    }

    /**
     * @param CUDocumentsUploadForm $form
     */
    public function Form_CUDocumentsUploadFormSubmitted(CUDocumentsUploadForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('File(s) has been uploaded');
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

}
