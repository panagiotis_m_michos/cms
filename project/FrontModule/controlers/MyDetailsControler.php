<?php

use Front\MyDetailsForm;

class MyDetailsControler extends LoggedControler
{
    const MY_DETAILS_PAGE = 96;

    public function renderDefault()
    {
        $form = new MyDetailsForm('registration_' . $this->customerEntity->getId());
        $form->startup(
            $this->customerEntity,
            Customer::$titles,
            Customer::$countries,
            Customer::$howHeards,
            Customer::$industries
        );
        if ($form->isOk2Save()) {
            $this->processForm($form);
        }
        $this->template->form = $form;

        $this->template->finishedRegistration = $this->customer->hasCompletedRegistration();
        $this->template->hasSubscribed = $this->customer->isSubscribed;
        $this->template->hasWholesaleQuestion = $this->customer->hasWholesaleQuestion();
    }

    /**
     * @param MyDetailsForm $form
     */
    private function processForm(MyDetailsForm $form)
    {
        try {
            $this->customerService->update($this->customerEntity, $form->getValues());
            $form->clean();

            $backLink = $this->getBacklink();
            if ($backLink !== FALSE) {
                $this->removeBacklink();
                $this->redirect("%$backLink%");
            } else {
                $this->flashMessage('Your details have been changed');
                $this->redirect(DashboardCustomerControler::DASHBOARD_PAGE);
            }
        } catch (Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * @param Exception $e
     */
    private function handleException(Exception $e)
    {
        $this->flashMessage($e->getMessage(), 'error');
        $this->redirect();
    }
}
