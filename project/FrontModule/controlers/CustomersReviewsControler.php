<?php

class CustomersReviewsControler extends DefaultControler
{
    const REVIEWS_PAGE = 749;
    const SATISFACTION_PAGE = 750;

    /**
     * @var array
     */
    public $possibleActions = [
        self::REVIEWS_PAGE => 'reviews',
        self::SATISFACTION_PAGE => 'satisfaction',
    ];

    /**
     * @var string
     */
    public static $handleObject = 'CustomersReviewsModel';

    /**
     * @var CustomersReviewsModel
     */
    public $node;

    /************************************** reviews **************************************/

    public function renderReviews()
    {
        $this->template->replacedText = $this->node->getReplacesReviewsText();
        $this->template->satisfactionValue = $this->node->getSatisfactionFinalPercentage();

        $paginator = $this->node->getReviewsPaginator();
        $reviewsRows = $this->node->getReviews($paginator);

        //get company name from first row
        $array = array_slice($reviewsRows, 0, 1);
        $firstRewiew = array_shift($array);
        $companyName = $firstRewiew['companyName'];

        $this->template->description = str_replace("[CompanyName]", $companyName, $this->node->page->description);
        $this->template->seoTitle = str_replace("[CompanyName]", $companyName, $this->node->page->seoTitle);
        $this->template->reviews = $reviewsRows;
        $this->template->paginator = $paginator;
    }

    /************************************** satisfaction **************************************/

    public function renderSatisfaction()
    {
        $this->template->customerSatisfaction = new CustomerReviewsSatisfaction();
        $this->template->serviceValue = new CustomerReviewsServiceValue();
        $this->template->recomendation = new CustomerReviewsRecommendation();
    }
}
