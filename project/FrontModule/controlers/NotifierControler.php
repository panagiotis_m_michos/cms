<?php

class NotifierControler extends DefaultControler
{
    public function renderDefault()
    {
        Debug::disableProfiler();
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
       
//        if (!isset($this->get['hash']) && $this->get['hash'] != CRON_HASH) {
//            $this->terminate(json_encode(array('error' => TRUE,'message' => 'No hash')));
//        }
        
        if (!isset($this->get['event'])) {
            header('HTTP/1.0 404 Not Found');
            $this->terminate(json_encode(array('error' => TRUE,'message' => 'No Event')));
            exit;
        }
        
        $cache = Notifier::getStatusObject($this->get['event']);
        if (!$cache) {
            header('HTTP/1.0 404 Not Found');
            $this->terminate(json_encode(array('error' => TRUE,'message' => 'No Event Status')));
            exit;
        }
        
        $this->terminate(json_encode(
                array('error'   => FALSE,
                    'eventKey' => $cache->eventKey,
                    'statusId'    => $cache->statusId,
                    'objectId'  => $cache->objectId,
                    'message'   => $cache->message, 
                    'dtc'   => $cache->dtc, 
                )));
        exit;
    }
}