<?php

use Services\CompanyService;

// this was used for testing new payment page, keeping it for future use
class SplitTestPaymentControler extends DefaultControler
{
    /* CONST */
    const SAGE_PAGE = 1391;
    const SAGE_CHECK_NEW_CUSTOMER_PAGE = 1224;
    const SAGE_AUTH = 1226;
    const SAGE_AUTH_VALIDATION = 1227;
    const NEW_PASSWORD_PAGE = 1228;
    const CONFIRMATION_PAGE = 139;
    const PAYPAL_NEW_CUSTOMER_CANCEL_PAGE = 1225;
    const PAYPAL_ERROR_EMAIL = 233;
    const TRUST_ELEMENTS_PAGE = 1309;

    /**
     * @var array
     */
    public $possibleActions = array(
        self::SAGE_PAGE => 'sage',
        self::SAGE_CHECK_NEW_CUSTOMER_PAGE => 'checkSageNewCustomer',
        self::PAYPAL_NEW_CUSTOMER_CANCEL_PAGE => 'paypalCanceled',
        self::SAGE_AUTH => 'sageAuthentication',
        self::SAGE_AUTH_VALIDATION => 'sageAuthValidation',
        self::NEW_PASSWORD_PAGE => 'sageNewCustomerPassword',
        self::CONFIRMATION_PAGE => 'confirmation',
        self::TRUST_ELEMENTS_PAGE => 'trustElementsContent',
    );

    /**
     * @var string
     */
    public static $handleObject = 'PaymentModel';

    /**
     * @var string Session
     */
    public static $nameSpace = 'paymentRequestDetails';

    /**
     * @var PaymentModel
     */
    public $node;

    /**
     * @var boolean
     */
    protected $httpsRequired = TRUE;

    /**
     * @var CompanyService
     */
    private $companyService;

    public function startup()
    {
        parent::startup();

        $this->companyService = $this->getService(DiLocator::SERVICE_COMPANY);
    }

    /**
     * @return void
     */
    public function beforePrepare()
    {
        // check is basket is not empty
        if ($this->basket->isEmpty()) {
            $this->redirect("%{$this->getHomeLink()}%");
        }

        $this->template->node = $this->node;
        $this->template->companyName = SearchControler::getCompanyName();

        parent::beforePrepare();
    }

    public function prepareSage()
    {
        try {
            $view = $this->node->getPaymentView($this->customer, $this->basket);

            $this->changeView($view);

            if ($this->customer->isNew()) {
                $formLogin = new LoginFormNew($this->node->getId() . '_login');
                $formLogin->startup(array($this, 'Form_loginFormSubmitted'));
                $this->template->formLogin = $formLogin;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function renderSage()
    {
        $form = new SplitTestSagePaymentForm($this->node->getId() . '_sage_payment');
        $form->startup($this, array($this, 'Form_sagePaymentFormSubmitted'), $this->basket, $this->customer, $this->node);
        $this->template->form = $form;
        //processPayment
        try {
            $service = new PaymentService($this->customer, $this->basket, $this->customerEntity, $this->getService(DiLocator::SERVICE_ORDER), $this->getService(DiLocator::SERVICE_PRODUCT));
            $service->setOnFinish(array($this, 'Service_paymentFinished'));
            $service->processPaymentForm($form, $this->get, $this->post);
        } catch (PaypalNoResponseException $e) {
            $this->flashMessage($e->getMessage(), 'error');
            //send mail ??
            $this->redirect(self::SAGE_PAGE);
        } catch (SageAuthenticationException $e) {
            $this->template->authFormUrl = FApplication::$router->secureLink(self::SAGE_AUTH, array('sagepay-3d' => 1));
            $this->template->showAuth = TRUE;
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');

            if (Customer::isSignedIn() === TRUE) {
                $this->redirect(self::SAGE_PAGE, array('sagepay-3d' => 1));
            }
            $this->readFlashMessages();
        }
    }

    /**
     * @param PaymentService $service
     * @return void
     */
    public function Service_paymentFinished(PaymentService $service)
    {
        $this->template->success = TRUE;
        $this->redirect(self::CONFIRMATION_PAGE, $this->node->getConfirmationPageLinkParams($this->basket));
    }

    /**
     * @param LoginForm $form
     */
    public function Form_loginFormSubmitted(LoginFormNew $form)
    {
        if (FApplication::$httpRequest->isAjax()) {
            try {
                $customer = Customer::doAuthenticate($form['email']->getValue(), $form['password']->getValue());
                CustomerActionLogger::log($customer, CustomerActionLogger::LOG_IN);
                Customer::signIn($customer->getId());
                // analitics url start
                $param = '';
                if ($this->basket->hasPackageInBasket()) {
                    $package = $this->basket->getPackage();
                    $param = str_replace(' ', '', strtolower(Package::$types[$package->getId()]));
                }
                // analitics url end

                $json = array('redirect' => 1, 'link' => FApplication::$router->secureLink(self::SAGE_PAGE, array('package' => $param)));
                echo json_encode($json);
                exit;
            } catch (Exception $e) {
                $json = array('error' => 1, 'message' => $e->getMessage());
                echo json_encode($json);
                die;
            }
        }
        die;
    }

    /*     * *********************************************** PaypalCanceled ************************************************ */

    /**
     * @return void
     */
    public function handlePaypalCanceled()
    {
        if (!isset($this->get['token']) || empty($this->get['token'])) {
            $this->redirect(DashboardCustomerControler::DASHBOARD_PAGE);
        }
    }

    /**
     * sage authorization form in iframe (loaded when 3dauth is necessary)
     */
    public function renderSageAuthentication()
    {
        Debug::disableProfiler();
        if (isset($_SESSION['sageAuthForm'])) {
            $this->template->form = $_SESSION['sageAuthForm'];
        }
    }

    /**
     * sage auth form validation, sage pay sends post to this method
     */
    public function renderSageAuthValidation()
    {
        try {
            Debug::disableProfiler();
            $form = new SagePaymentForm($this->node->getId() . '_sage_payment');
            $form->startup(
                $this,
                array($this, 'Form_sagePaymentFormSubmitted'),
                $this->basket,
                $this->customer,
                $this->companyService
            );
            $this->template->form = $form;

            //processPayment
            $service = new PaymentService($this->customer, $this->basket, $this->customerEntity, $this->getService(DiLocator::SERVICE_ORDER), $this->getService(DiLocator::SERVICE_PRODUCT));
            $service->setOnFinish(array($this, 'Service_AuthSagepaymentFinished'));
            $service->processPaymentForm($form, $this->get, $this->post);
        } catch (Exception $e) {
            $this->template->failedLocation = FApplication::$router->link(self::SAGE_PAGE);
            $this->template->error = $e->getMessage();
        }
    }

    /**
     * @param PaymentService $service
     * @return void
     */
    public function Service_AuthSagepaymentFinished(PaymentService $service)
    {
        $this->template->success = TRUE;
        $this->template->location = FApplication::$router->secureLink(self::CONFIRMATION_PAGE, $this->node->getConfirmationPageLinkParams($this->basket));
    }

    /*     * ******************************** ajax SageNewCustomerPassword ******************************** */

    /**
     * @return void
     */
    public function handleSageNewCustomerPassword()
    {
        if (FApplication::$httpRequest->isAjax() && isset($this->get['email'])) {
            try {
                $json = $this->node->generateNewCustomerPassord($this->get['email']);
            } catch (Exception $e) {
                $json = array('error' => 1);
                echo json_encode($json);
                die;
            }
            echo json_encode($json);
            exit;
        }
    }

    /*     * ******************************** ajax CheckSageNewCustomer ******************************** */

    /**
     * @return void
     */
    public function handleCheckSageNewCustomer()
    {
        Debug::disableProfiler();
        if (FApplication::$httpRequest->isAjax() && !isset($this->get['password'])) {
            if (isset($this->get['email']) && $this->node->checkEmail($this->get['email'])) {
                if ($this->node->validateUniqueLogin($this->get['email'])) {
                    $json = array('succes' => 1, 'showLogin' => 1);
                } else {
                    $json = array('succes' => 1, 'showLogin' => 0);
                }
            } else {
                $json = array('error' => 1);
            }

            echo json_encode($json);
            exit;
        }
    }

    /**
     * @return void
     */
    public function renderTrustElementsContent()
    {
        Debug::disableProfiler();
        if (FApplication::$httpRequest->isAjax()) {
            sleep(2);
            echo $this->getHtml();
        }
        $this->terminate();
    }

    /*     * ******************************** confirmation ******************************** */

    protected function prepareConfirmation()
    {
        if ($this->basket->hasPackageIncluded() === TRUE) {
            $this->template->hasPackageIncluded = 1;
            //if you have package you definitely have company
            $company = Company::getCompanyByOrderId($this->basket->getOrderId());
            if ($company instanceof Company) {
                $this->template->companyId = $company->getCompanyId();
            }
        }

        // --- checking bronze package
        try {
            $package = $this->basket->getPackage();
            if ($package->getId() == Package::PACKAGE_BRONZE) {
                $this->template->hasBronzePackage = TRUE;
            }
        } catch (Exception $e) {

        }

        $this->template->basket = clone $this->basket;
        $this->template->ecommerce = 1;

        // save backlink - because of journey page
        $backlink = $this->router->link(DashboardCustomerControler::DASHBOARD_PAGE);
        $this->saveBacklink($backlink);
    }

    protected function renderConfirmation()
    {
        // get and save basket ids for journey page
        $ids = array();
        $items = $this->basket->getItems();
        foreach ($items as $key => $item) {
            $ids[] = $item->getId();
        }
        $namespace = $this->getSession('journeyBasketIds');
        $namespace->ids = $ids;
        $this->basket->clear();
    }

}
