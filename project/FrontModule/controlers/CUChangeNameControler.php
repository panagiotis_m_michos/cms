<?php

class CUChangeNameControler extends CUControler
{
    const CHANGE_NAME_PAGE = 846;
    const COMPANY_NAME_CHANGE_PRODUCT_PAGE = 845;

    /**
     * @var array
     */
    public $possibleActions = array(
        self::CHANGE_NAME_PAGE => 'default',
    );

    /**
     * @var CompanyNameChangeServiceModel
     */
    protected $service;

    public function beforePrepare()
    {
        if ($this->action == $this->possibleActions[self::CHANGE_NAME_PAGE]) {
            $this->authCodeRequired = TRUE;
        }

        parent::beforePrepare();
        $this->service = new CompanyNameChangeServiceModel($this->company);

        try {
            $this->service->isOkToChangeName();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error', FALSE);
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
        }
    }

    public function renderDefault()
    {
        if (FApplication::$httpRequest->isAjax()) {
            $this->service->ajaxCheckCompanyAvailability($this->get['companyName']);
        }

        $form = new ChangeCompanyNameForm($this->node->getId() . '_ChangeCompanyname');
        $form->startup($this, array($this, 'Form_ChangeCompanynameFormSubmitted'), $this->company);
        $this->template->form = $form;
    }

    /**
     * @param ChangeCompanyNameForm $form
     */
    public function Form_ChangeCompanynameFormSubmitted(ChangeCompanyNameForm $form)
    {
        try {
            $form->process();
            $this->flashMessage(
                'Your company name change request has been sent to Companies House. Approval should be given in approx. 3 hours.'
            );
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }
}
