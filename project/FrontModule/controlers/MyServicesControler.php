<?php

use BasketModule\Services\BasketService;
use Factories\Front\CompanyViewFactory;
use MyServicesModule\Controllers\ServiceSettingsController;
use Services\CompanyService;
use Services\IRenewFormDelegate;
use Services\RenewForm;
use Services\ServiceService;
use ServiceSettingsModule\Config\DiLocator as ServiceSettingsDiLocator;
use ServiceSettingsModule\Services\ServiceSettingsService;

class MyServicesControler extends LoggedControler implements IRenewFormDelegate
{

    const PAGE_SERVICES = 1364;

    /** @var array */
    public $possibleActions = [
        self::PAGE_SERVICES => 'default',
    ];

    /**
     * @var ServiceService
     */
    private $serviceService;

    /**
     * @var BasketService
     */
    private $basketService;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var ServiceSettingsService
     */
    private $serviceSettingsService;

    /**
     * @var CompanyViewFactory
     */
    private $companyViewFactory;

    public function startup()
    {
        parent::startup();

        $this->serviceService = $this->getService(DiLocator::SERVICE_SERVICE);
        $this->basketService = $this->getService('basket_module.services.basket_service');
        $this->companyService = $this->getService(DiLocator::SERVICE_COMPANY);
        $this->serviceSettingsService = $this->getService(ServiceSettingsDiLocator::SERVICES_SERVICE_SETTINGS_SERVICE);
        $this->companyViewFactory = $this->getService(DiLocator::FACTORY_FRONT_COMPANY_VIEW);
    }

    public function handleDefault()
    {
        if ($this->isAjax() && isset($this->get['do']) && $this->get['do'] === 'calculateTotal') {
            try {
                $basket = new Basket('my-services');
                $basket->clear(TRUE);
                if (isset($this->get['ids'])) {
                    foreach ($this->get['ids'] as $productId) {
                        $item = Product::getProductById($productId);
                        $basket->add($item);
                    }
                }
                $price = $basket->getPrice();
                $payload = [
                    'error' => FALSE,
                    'price' => [
                        'subtotal' => $price->subTotal,
                        'vat' => $price->vat,
                        'total' => $price->total
                    ]
                ];
                $this->sendJSONResponse($payload);
            } catch (Exception $e) {
                $this->handleException($e);
            }
        }
    }

    public function renderDefault()
    {
        $checkedServices = [];
        if (isset($this->get['serviceId'])) {
            $service = $this->serviceService->getServiceById($this->get['serviceId']);
            if ($service) {
                $checkedServices[] = $service;
            }
        }
        if (isset($this->get['companyId'])) {
            $company = $this->companyService->getCompanyById($this->get['companyId']);
            if ($company) {
                $companyView = $this->companyViewFactory->create($company);
                $checkedServices = $companyView->getServices();
            }
        }

        $customerCompanies = $this->companyService->getCustomerServiceCompaniesViews($this->customerEntity);
        $this->template->customerCompanies = $customerCompanies;
        $this->template->customer = $this->customerEntity;
        $this->template->settingsService = $this->serviceSettingsService;
        $this->template->disabledRemindersCompanyId = $this->getParameter('disabledRemindersCompanyId');
        $this->template->myServicesSettingUrl = $this->newRouter->getRouteCollection()->get(ServiceSettingsController::ROUTE_SET_MY_SERVICE_SETTINGS)->getPath();

        $form = new RenewForm($this->customerEntity->id . '_renew_services');
        $form->startup($customerCompanies, $checkedServices, $this->serviceSettingsService);

        if ($form->isOk2Save()) {
            $data = $form->getValues();
            $serviceIds = array_keys(array_filter($data['services']));

            $form->clean();
            $this->renewFormWithServices($serviceIds);
        }

        $this->template->form = $form;
    }

    /*     * ****************************** IRenewFormDelegate ******************************** */

    /**
     * @param array $serviceIds
     */
    public function renewFormWithServices(array $serviceIds)
    {
        try {
            $this->basketService->addRenewableServices($this->customerEntity, $serviceIds);
            if ($this->isAjax()) {
                $payload = ['error' => FALSE, 'redirect' => FApplication::$router->link(BasketControler::PAGE_SUMMARY)];
                $this->sendJSONResponse($payload);
            } else {
                $this->redirect(BasketControler::PAGE_SUMMARY);
            }
        } catch (Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * @param Exception $e
     */
    private function handleException(Exception $e)
    {
        if ($this->isAjax()) {
            $payload = ['error' => TRUE, 'message' => $e->getMessage()];
            $this->sendJSONResponse($payload);
        } else {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

}
