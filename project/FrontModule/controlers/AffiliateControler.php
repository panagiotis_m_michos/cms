<?php


class AffiliateControler extends DefaultControler
{
    /* pages */
    const REGISTRATION_PAGE = 974;
    const LOGIN_PAGE = 973;
    const TERMS_AND_CONDITIONS_PAGE = 975;
    const PRIVACY_POLICY_PAGE = 993;
    const LOGOUT_PAGE = 983;
    const FORGOTTEN_PASSWORD_PAGE = 976;

    /**
     * @var array
     */
    public $possibleActions = array(
        self::REGISTRATION_PAGE => 'registration',
        self::LOGIN_PAGE => 'login',
        self::TERMS_AND_CONDITIONS_PAGE => 'termsAndConditions',
        self::LOGOUT_PAGE => 'logout',
        self::FORGOTTEN_PASSWORD_PAGE => 'forgotten',
    );
    
    protected $httpsRequired = TRUE;

    /*     * ******************************************** home register********************************************* */

    public function startup()
    {  
        parent::startup();
        if (Affiliate::isSignedIn()) {
            $this->template->sign = TRUE;
        }
    }

    public function handleRegistration()
    {
        parent::startup();
        if (Affiliate::isSignedIn()) {
            if (isset($this->get['l'])) {
                Affiliate::signOut();
                $this->redirect(self::LOGOUT_PAGE);
            }
            $this->redirect(AffiliateLoggedControler::DASHBOARD_PAGE);
        }
    }

    public function renderRegistration()
    {
        $form = new AffiliateRegisterForm($this->node->getId() . '_home');
        $form->startup($this, array($this, 'Form_homeFormSubmitted'));
        $this->template->form = $form;
    }

    /**
     * @param AffiliateRegisterForm $form
     */
    public function Form_homeFormSubmitted(AffiliateRegisterForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Your Affiliate Account has been created');
            $this->redirect(AffiliateLoggedControler::DASHBOARD_PAGE);
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    /*     * ********************************************login ********************************************* */
     public function handleLogin()
     {
        parent::startup();
        if (Affiliate::isSignedIn()) {
            if (isset($this->get['l'])) {
                Affiliate::signOut();
                $this->redirect(self::LOGOUT_PAGE);
            }
            $this->redirect(AffiliateLoggedControler::DASHBOARD_PAGE);
        }
     }


        public function renderLogin()
        {
            $form = new AffiliateLoginForm($this->node->getId() . '_login');
            $form->startup($this, array($this, 'Form_loginFormSubmitted'));
            $this->template->form = $form;
        }

        /**
     * @param AffiliateRegisterForm $form
     */
        public function Form_loginFormSubmitted(AffiliateLoginForm $form)
        {
            try {
                $form->process();
                $this->redirect(AffiliateLoggedControler::DASHBOARD_PAGE);
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
                $this->redirect();
            }
        }

        /*     * ******************************************** terms and conditions ********************************************* */

        public function renderTermsAndConditions()
        {
            $this->template->test = 'test';
        }

        /*     * ******************************************** forgotten ********************************************* */

        public function renderForgotten()
        {
            $form = new AffiliateForgottenForm($this->node->getId() . '_forgotten');
            $affiliateEmailer = $this->emailerFactory->get(EmailerFactory::AFFILIATE);
            $form->startup($this, array($this, 'Form_forgottenFormSubmitted'), $affiliateEmailer);
            $this->template->form = $form;
        }

        /**
     * @param AffiliateForgottenForm $form
     */
        public function Form_forgottenFormSubmitted(AffiliateForgottenForm $form)
        {
            try {
                $form->process();
                $this->flashMessage('New password has been sent to your email.');
                $this->redirect(AffiliateControler::LOGIN_PAGE);
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
                $this->redirect();
            }
        }

}
