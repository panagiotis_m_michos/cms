<?php


/**
 * @package    CMS
 * @author     Nikolai Senkevich
 */

class CUEReminderControler extends CUControler
{
    const EREMINDER_PAGE = 1147;

    public $possibleActions = array(
        self::EREMINDER_PAGE => 'default',
    );


    public function renderDefault()
    {
        try {
            $addForm = new EReminderAddForm($this->node->getId() . '_add');
            $addForm->startup($this, array($this, 'Form_signupFormSubmitted'), $this->company);

            // get all reminders  which is active and set this reminders exclude one which was provided by user 
            if (isset($this->get['emailToDelete'])) {
                $emailToDelete = $this->get['emailToDelete'];

                $reminder = GetEReminders::getNewReminder($this->company);
                $reminderEmails = $reminder->sendRequest();
                $xml = simplexml_load_string($reminderEmails);

                //check xml and throw exception in case error
                $this->checkXml($xml);
                $emails = array();
                foreach ($xml->Body->EReminders->Recipient as $value) {
                    if ($emailToDelete != (string) $value->EmailAddress) {
                        $emails[] = (string) $value->EmailAddress;
                    }
                }//pr($emails);exit;
                $reminder = SetEReminders::getNewReminder($emails, $this->company);
                $this->flashMessage('Your email has been deleted');
                $this->readFlashMessages();
                
            // set all reminders  by empty array               
            } elseif (isset($this->get['deleteAll'])) {
                $emails = array();
                $reminder = SetEReminders::getNewReminder($emails, $this->company);
                $this->flashMessage('Your emails has been deleted');
                $this->readFlashMessages();
            
            // get all reminders    
            } else {
                $reminder = GetEReminders::getNewReminder($this->company);
            }

            //send request and get responce
            $reminderEmails = $reminder->sendRequest();
            $xml = simplexml_load_string($reminderEmails);

            //check xml and throw exception in case error
            $this->checkXml($xml);
            
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, 'company_id='.$this->company->getCompanyId());
        }
        $this->template->list = $xml->Body->EReminders->Recipient;
        $this->template->formAdd = $addForm;
    }

    public function checkXml($xml)
    {
        if (isset($xml->GovTalkDetails->GovTalkErrors)) {
            if (isset($xml->GovTalkDetails->GovTalkErrors->Error->Text)) {
                throw new Exception('eReminders unavailable. Usually this is because the authentication code has expired or the company is dissolved.');
            } else {
                throw new Exception('eReminders unavailable. Usually this is because the authentication code has expired or the company is dissolved.');
            }
        }
    }

    /**
     * @param EReminderAddForm $form
     */
    public function Form_signupFormSubmitted(EReminderAddForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Your email has been added but it must still be activated by clicking the link in the email that Companies House send you and agreeing to the Terms of Operation.');
            $this->redirect('default', 'company_id='.$this->company->getCompanyId());
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, 'company_id='.$this->company->getCompanyId());
        }
    }

}