<?php

use Entities\Company as CompanyEntity;
use Services\CompanyService;

abstract class CFControler extends LoggedControler
{
    const REGISTER_OFFICES_FOLDER = 164;

    /**
     * @var Company
     */
    protected $company;

    /**
     * @var CompanyService
     */
    protected $companyService;

    /**
     * @var CompanyEntity
     */
    protected $companyEntity;

    /**
     * @var CFControllerModel
     */
    public $service;

    public function startup()
    {
        //@TODO remove once memory issues conserning directors are fixed S333
        //ini_set('memory_limit', '512M');
        parent::startup();
        $this->companyService = $this->getService(DiLocator::SERVICE_COMPANY);
    }

    /**
     * Provide checking if company belongs to logged customer
     *
     * @return void
     */
    public function beforePrepare()
    {
        try {
            // check get parameter
            if (!isset($this->get['company_id'])) {
                $this->redirect(CompaniesCustomerControler::COMPANIES_PAGE);
            }

            // get company
            $chFiling = new CHFiling();
            $this->company = $chFiling->getCustomerCompany($this->get['company_id'], $this->customer->getId());
            $this->companyEntity = $this->companyService->getCustomerCompany(
                $this->customerEntity,
                $this->company->getId()
            );

            $this->service = new CFControllerModel($this->company, $this->customer);

            // check if company is locked
            if ($this->company->isLocked()) {
                throw new Exception(
                    'Your company has been locked by the system - If you purchased a reserve a company name, this is normal and no action is required until you purchase the <a href="' . $this->router->link(
                        239
                    ) . '">Company Transfer Fee</a>. <br /><br />If you feel this lock has been done in error please contact support on 0207 608 5500'
                );
            }


            // check for form submission
            if ($this->company->getLastIncorporationFormSubmission() == NULL) {
                throw new Exception(
                    "Form submission for company `{$this->company->getCompanyName()}` doesn't exist."
                );
            }

            // check if company is incorporated
            if ($this->company->getCompanyNumber() != NULL) {
                $this->redirect(CompaniesCustomerControler::COMPANIES_PAGE);
            }


            $this->template->company = $this->company;
            $this->template->companyId = $this->company->getCompanyId();
            $this->template->companyName = $this->company->getCompanyName();
            $this->template->companyType = $this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType();
            $this->template->tabs = $this->getComponentCFTabs();

        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect(CompaniesCustomerControler::COMPANIES_PAGE);
        }
        parent::beforePrepare();
    }

    /**
     * Returns company type
     *
     * @return string
     */
    public function getCompanyType()
    {
        $companyType = $this->company->getLastIncorporationFormSubmission()->getForm()->getType();
        $type = isset(CompanyIncorporation::$types[$companyType]) ? CompanyIncorporation::$types[$companyType] : 'N/A';
        return $type;
    }


    /**
     * Returns addreses for select and javascript used for prefill address
     *
     * @param Company $company
     * @return array $return
     */
    public static function getPrefillAdresses(Company $company)
    {
        $addresses = $company->getLastIncorporationFormSubmission()->getForm()->getAddresses();

        // add customer address to prefill array just for UK customers
        $customer = new Customer($company->getCustomerId());
        if ($customer->countryId == 223) {
            try {
                $customerAddress = new Address();
                $customerAddress->setPremise($customer->address1);
                $customerAddress->setStreet($customer->address2);
                $customerAddress->setThoroughfare($customer->address3);
                $customerAddress->setPostTown($customer->city);
                $customerAddress->setCounty($customer->county);
                $customerAddress->setCountry('');
                $customerAddress->setPostcode($customer->postcode);
                $addresses[] = $customerAddress;
            } catch (Exception $e) {
                //                Debug::log($e);
                // if exception is throw we don't add address to prefill array
            }
        }

        $addressesFields = [];
        $selectValues = [];
        foreach ($addresses as $key => $address) {
            $fields = $address->getFields();
            $addressesFields[$key] = $fields;
            $selectValues[$key] = $fields['postcode'] . ', ' . $fields['premise'];
        }

        return [
            'select' => $selectValues,
            'js' => json_encode($addressesFields, JSON_FORCE_OBJECT),
        ];
    }


    /**
     * Returns person details for select and javascript used for prefill person data
     *
     * @param Company $company
     * @param string $type
     * @return array $return
     */
    public static function getPrefillOfficers(Company $company, $type = 'person')
    {
        $officersFields = [];
        $selectValues = [];

        if ($type == 'person') {
            $persons = $company->getLastIncorporationFormSubmission()->getForm()->getIncPersons(array('dir', 'sub', 'sec', 'psc'));

            foreach ($persons as $key => $person) {
                // skip nominees
                if ($person->isNominee()) {
                    continue;
                }

                $fields = $person->getFields();
                $officersFields[$key] = $fields;
                $selectValues[$key] = $fields['surname'] . ' ' . $fields['forename'];
            }
        } elseif ($type == 'corporate') {
            $persons = $company->getLastIncorporationFormSubmission()->getForm()->getIncCorporates(array('dir', 'sub', 'sec', 'psc'));

            foreach ($persons as $key => $person) {
                // skip nominees
                if ($person->isNominee()) {
                    continue;
                }

                $fields = $person->getFields();
                $officersFields[$key] = $fields;
                $selectValues[$key] = $fields['corporate_name'];
            }
        }

        return [
            'select' => $selectValues,
            'js' => json_encode($officersFields, JSON_FORCE_OBJECT),
        ];
    }


    /**
     * Provides validation EEA/NonEAA fields
     *
     * @param Text $control
     * @param string $error
     * @return mixed
     */
    public function Validator_eeaRequired(Text $control, $error)
    {
        if ($control->owner['eeaType']->getValue() !== NULL) {
            // EEA
            if ($control->owner['eeaType']->getValue() == 1) {
                if (($control->getName() == 'place_registered' || $control->getName() == 'registration_number')
                    && $control->getValue() == ''
                ) {
                    return $error;
                }
                // non EEA
            } else {
                if (($control->getName() == 'place_registered' || $control->getName() == 'law_governed'
                    || $control->getName() == 'legal_form') && $control->getValue() == ''
                ) {
                    return $error;
                }
            }
        }
        return TRUE;
    }

    /**
     * Returns tabs for company formation
     *
     * @return array
     */
    protected function getComponentCFTabs()
    {
        $tabs = [
            'office' => [
                'title' => 'Registered',
                'link' => $this->router->link(
                    CFRegisteredOfficeControler::REGISTER_OFFICE_PAGE,
                    "company_id={$this->company->getCompanyId()}"
                ),
            ],
            'directors' => [
                'title' => 'Directors',
                'link' => $this->router->link(
                    CFDirectorsControler::DIRECTORS_PAGE,
                    "company_id={$this->company->getCompanyId()}"
                ),
            ],
            'shareholders' => [
                'title' => 'Shareholders',
                'link' => $this->router->link(
                    CFShareholdersControler::SHAREHOLDERS_PAGE,
                    "company_id={$this->company->getCompanyId()}"
                ),
            ],
            'pscs' => [
                'title' => 'PSCs',
                'link' => $this->router->link(
                    CFPscsControler::PSCS_PAGE,
                    ['company_id' => $this->company->getId()]
                ),
            ],
            'secretaries' => [
                'title' => 'Secretaries',
                'link' => $this->router->link(
                    CFSecretariesControler::SECRETARIES_PAGE,
                    "company_id={$this->company->getCompanyId()}"
                ),
            ],
            'articles' => [
                'title' => 'M &amp; A',
                'link' => $this->router->link(
                    CFArticlesControler::ARTICLES_PAGE,
                    "company_id={$this->company->getCompanyId()}"
                ),
            ],
            'toolkit' => [
                'title' => 'StartUp Toolkit',
                'link' => $this->router->link(
                    CFToolkitControler::PAGE_TOOLKIT,
                    ['company_id' => $this->company->getCompanyId()]
                ),
            ],
            'summary' => [
                'title' => 'Summary',
                'link' => $this->router->link(
                    CFSummaryControler::SUMMARY_PAGE,
                    "company_id={$this->company->getCompanyId()}"
                ),
            ],
        ];

        // some changes by types
        switch ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType()) {
            case 'BYGUAR':
                $tabs['shareholders']['title'] = 'Members';
                break;
            case CompanyIncorporation::LIMITED_LIABILITY_PARTHENERSHIP:
                $tabs['directors']['title'] = 'Members';
                $tabs['articles']['title'] = 'Name Restrictions';
                unset($tabs['shareholders']);
                unset($tabs['secretaries']);
                break;
        }

        if (!$this->service->checkOrderHasItem(Product::PRODUCT_BUSINESS_TOOLKIT)) {
            unset($tabs['toolkit']);
        }

        if (!Feature::featurePsc()) {
            unset($tabs['pscs']);
        }

        // permissions
        foreach ($tabs as $key => &$tab) {
            if ($this->checkStepPermission($key) === FALSE) {
                $tab['link'] = NULL;
            }
        }
        unset($tab);

        return $tabs;
    }


    /**
     * Check permission for steps
     *
     * @param string $step
     * @throws Exception
     * @return boolean
     */
    protected function checkStepPermission($step)
    {
        if ($this->company->getStatus() == 'pending' && $step != 'summary') {
            return FALSE;
        }

        $type = $this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType();
        switch (String::upper($step)) {

            case 'OFFICE':
                // check permission
                if ($this->company->getStatus() == 'pending') {
                    return FALSE;
                }
                break;

            case 'DIRECTORS':
                // check previous step
                if ($this->checkStepPermission('office') === FALSE) {
                    return FALSE;
                }

                // is filled
                if ($this->company->getLastIncorporationFormSubmission()->getForm()->hasRegisteredOffice() == FALSE) {
                    return FALSE;
                }
                break;

            case 'SHAREHOLDERS':
                // check previous step
                if ($this->checkStepPermission('directors') === FALSE) {
                    return FALSE;
                }

                // minimum 2 directors for PLC
                if ($type == CompanyIncorporation::PUBLIC_LIMITED) {
                    try {
                        $this->check('MINIMUM_2_DIRECTORS');
                    } catch (Exception $e) {
                        return FALSE;
                    }

                    //llp need at list 2 members(directors)
                } elseif ($type == CompanyIncorporation::LIMITED_LIABILITY_PARTHENERSHIP) {
                    try {
                        $this->check('MINIMUM_2_MEMBERS');
                    } catch (Exception $e) {
                        return FALSE;
                    }

                    // at least one director
                } elseif ($this->company->getLastIncorporationFormSubmission()->getForm()->directorsCount() == 0) {
                    return FALSE;
                }
                break;

            case 'PSCS':
                // check previous step
                if ($this->checkStepPermission('shareholders') === FALSE) {
                    return FALSE;
                }

                try {
                    if ($type == CompanyIncorporation::LIMITED_LIABILITY_PARTHENERSHIP) {
                        $this->check('MINIMUM_2_MEMBERS');
                    } else {
                        $this->check('HAS_SHAREHOLDER');
                    }
                } catch (Exception $e) {
                    return FALSE;
                }
                break;

            case 'SECRETARIES':
                // check previous step
                if ($this->checkStepPermission('pscs') === FALSE) {
                    return FALSE;
                }

                if (!Feature::featurePsc()) {
                    return TRUE;
                }

                try {
                    $this->check('HAS_PSC_OR_NO_PSC_REASEON');
                } catch (Exception $e) {
                    return FALSE;
                }
                break;

            case 'ARTICLES':
                // minimum 2 shareholders for LLP
                if ($type == CompanyIncorporation::LIMITED_LIABILITY_PARTHENERSHIP) {
                    try {
                        $this->check('MINIMUM_2_MEMBERS');
                    } catch (Exception $e) {
                        return FALSE;
                    }
                }

                // check previous step
                if ($this->checkStepPermission('secretaries') === FALSE) {
                    return FALSE;
                }

                // minimum 1 secretary for PLC
                if ($type == CompanyIncorporation::PUBLIC_LIMITED) {
                    try {
                        $this->check('HAS_SECRETARY');
                    } catch (Exception $e) {
                        return FALSE;
                    }
                }

                // same officer
                //if ($type == CompanyIncorporation::LIMITED_BY_SHARES) {
                //	try {
                //		$this->check('SAME_DIR_SEC');
                //	} catch (Exception $e){
                //		return FALSE;
                //	}
                //}

                break;

            case 'TOOLKIT':
                // check previous step
                if ($this->checkStepPermission('articles') === FALSE) {
                    return FALSE;
                }

                if ($type == CompanyIncorporation::LIMITED_LIABILITY_PARTHENERSHIP) {
                    try {
                        $this->check('MINIMUM_2_MEMBERS');
                    } catch (Exception $e) {
                        return FALSE;
                    }

                    return TRUE;
                }

                try {
                    $this->check('HAS_ARTICLE');
                } catch (Exception $e) {
                    return FALSE;
                }

                // section 30 own article
                if ($type == CompanyIncorporation::BY_GUARANTEE_EXEMPT) {
                    try {
                        $this->check('BESPOKE_ARTICLE');
                    } catch (Exception $e) {
                        return FALSE;
                    }
                }

                break;

            case 'SUMMARY':
                // check toolkit
                if ($this->service->checkOrderHasItem(Product::PRODUCT_BUSINESS_TOOLKIT)) {
                    try {
                        $this->check('TOOLKIT');
                    } catch (Exception $e) {
                        return FALSE;
                    }
                }

                // check previous step
                if ($this->company->getStatus() != 'pending' && $this->checkStepPermission('toolkit') === FALSE) {
                    return FALSE;
                }

                break;

            default:
                throw new Exception("Step `$step` doesn't exists!");
        }
        return TRUE;
    }


    /**
     * Provides checking company requirements
     *
     * @param string $category
     * @throws Exception
     */
    protected function check($category)
    {
        switch ($category) {
            case 'SAME_DIR_SEC':
                $same = $this->company->getLastIncorporationFormSubmission()->getForm()->isSameOfficer();
                if ($same === TRUE) {
                    throw new Exception(
                        "Sorry, it looks like you've appointed the same person as both director and secretary. In order to continue you need to either remove the secretary (as this role is no longer compulsory) or add another director."
                    );
                }
                break;
            case 'MINIMUM_2_DIRECTORS':
                // is there at least 2 directors
                $count = $this->company->getLastIncorporationFormSubmission()->getForm()->directorsCount();
                if ($count < 2) {
                    throw new Exception(
                        'Public Limited Companies require a minimum of 2 directors. Please make sure you have appointed 2 directors before you continue.'
                    );
                }
                break;
            case 'MINIMUM_2_MEMBERS':
                // is there at least 2 LLP members
                $count = $this->company->getLastIncorporationFormSubmission()->getForm()->directorsCount();
                if ($count < 2) {
                    throw new Exception(
                        'Limited Liability Partnerships require a minimum of 2 members. Please make sure you have appointed 2 members before you continue.'
                    );
                }
                break;
            case 'MINIMUM_1_PERSON_DIRECTOR':
                $persons = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncPersons(array('dir'));
                if (count($persons) == 0) {
                    throw new Exception('You need to appoint at least one natural (human) director');
                }
                break;
            case 'HAS_SHAREHOLDER':
                // at least 1 secretary
                $count = $this->company->getLastIncorporationFormSubmission()->getForm()->shareholdersCount();
                if ($count < 1) {
                    throw new Exception(
                        'Public Limited Companies require a minimum of 1 shareholder. Please make sure you have appointed 1 shareholder before you continue.'
                    );
                }
                break;
            case 'MINIMUM_2_SHAREHOLDERS':
                // is there at least 2 shareholders
                $count = $this->company->getLastIncorporationFormSubmission()->getForm()->shareholdersCount();
                if ($count < 2) {

                    throw new Exception(
                        'Public Limited Companies require a minimum of 2 shareholders. Please make sure you have appointed 2 shareholders before you continue.'
                    );
                }
                break;
            case 'HAS_SECRETARY':
                // at least 1 secretary
                $count = $this->company->getLastIncorporationFormSubmission()->getForm()->secretariesCount();
                if ($count < 1) {
                    throw new Exception(
                        'Public Limited Companies require a minimum of 1 secretary. Please make sure you have appointed 1 secretary before you continue.'
                    );
                }
                break;
            case 'HAS_ARTICLE':
                $articles = $this->company->getLastIncorporationFormSubmission()->getDocuments();
                if (count($articles) == 0) {
                    throw new Exception(
                        'There are no Articles linked to this application. Please upload them before you submit your incorporation.'
                    );
                }
                break;
            case 'BESPOKE_ARTICLE':
                $article = $this->company->getLastIncorporationFormSubmission()->getMemarts();
                if ($article['bespoke'] === FALSE) {
                    throw new Exception(
                        'Your Section 30 company requires custom Articles. Please upload your own or download ours, edit the appropriate sections and reupload.'
                    );
                }
                break;
            case 'TOOLKIT':
                if (!$this->companyEntity->toolkitOffersAlreadyChosen()) {
                    throw new Exception(
                        'Please choose some of the toolkit offers (you can choose none)'
                    );
                }
                break;
            case 'HAS_PSC_OR_NO_PSC_REASEON':
                /** @var CompanyIncorporation $incorporation */
                $incorporation = $this->company->getLastIncorporationFormSubmission()->getForm();
                if ($incorporation->pscsCount() < 1 && !$incorporation->getNoPscReason()) {
                    throw new Exception('You have to have at least 1 PSC or exceptional case');
                }
                break;
        }
    }


    /**
     * Validator for DOB
     *
     * @param DateSelect $control
     * @param array $error
     * @param int $minYears
     * @return bool|string
     */
    public function Validator_personDOB(DateSelect $control, $error, $minYears)
    {
        $value = $control->getValue();

        // check if dob is valid day
        $temp = explode('-', $value);
        if (checkdate($temp[1], $temp[2], $temp[0]) === FALSE) {
            return $error[0];
        }

        // check if director is older than 16 years
        $dob = date('Ymd', strtotime($value));
        $years = floor((date("Ymd") - $dob) / 10000);
        if ($years < $minYears) {
            return sprintf($error[1], $minYears);
        }
        return TRUE;
    }

    /**
     * Validator for security question - eye colour
     *
     * @param FControl $control
     * @param string $error
     * @return bool|string
     */
    public function Validator_securityEyeColour(FControl $control, $error)
    {
        $value = $control->getValue();
        if (String::upper($value) === 'BLA') {
            return $error;
        }
        return TRUE;
    }

    /**
     * @param FControl $control
     * @param string $error
     * @return bool|string
     */
    public function Validator_serviceAddressPostCode(FControl $control, $error)
    {
        $value = $control->getValue();
        $postcode = str_replace(' ', '', mb_strtolower($value));
        $blacklistedPostcodes = array('n17gu', 'ec1v4pw');
        if (!in_array($postcode, $blacklistedPostcodes)) {
            return TRUE;
        }
        return $error;
    }

    /**
     * @param FControl $control
     * @param string $error
     * @return bool|string
     */
    public function Validator_requiredAddress(FControl $control, $error)
    {
        $value = $control->getValue();
        //service address service validator
        if (isset($control->owner['ourServiceAddress'])) {
            if ($control->owner['ourServiceAddress']->getValue() != 1 && empty($value)) {
                return $error;
            }
            //without service address
        } else {
            if (empty($value)) {
                return $error;
            }
        }
        return TRUE;
    }

}
