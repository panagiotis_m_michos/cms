<?php

class CFCompanyNameControler extends CFControler
{
    const COMPANY_NAME_PAGE = 178;

    /** @var array */
    public $possibleActions = array(
        self::COMPANY_NAME_PAGE => 'default',
    );

    public function beforePrepare()
    {
        parent::beforePrepare();
        if ($this->company->getStatus() == 'pending') {
            $this->redirect(CFSummaryControler::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
        }
    }

    public function renderDefault()
    {
        $form = new FForm('CFCompanyName');

        $form->addFieldset('Change company name');
        $form->addText('companyName', 'Company Name:')
            ->addRule(array('CompanyNameValidator', 'isValid'), NULL)
            ->addRule(array($this, 'Validator_companyName'), array('Please provide company name!', 'Sorry, that name is taken. Please try searching for a new name or adding additional words to your current name.'));

        $form->addFieldset('Action');
        $form->addSubmit('search', 'Search')->class('btn_submit fright mbottom');
        $form->addSubmit('back', '< Back')->class('btn_back2')->style('border: 0; height: 47px; width: 150px; margin: -10px 0 0 0; padding: 0;');
        $form->addSubmit('change', 'Continue >')->class('btn_submit fright mbottom');

        $form->onValid = array($this, 'Form_defaultFormSubmitted');
        $form->start();
        $this->template->form = $form;

        // reserved words
        $this->template->reserveWords = ReservedWord::match($form['companyName']->getValue());
    }

    
    public function Form_defaultFormSubmitted(FForm $form)
    {
        // back
        if ($form->isSubmitedBy('back')) {
            if (isset($this->get['backlink'])) {
                $backLink = $this->get['backlink'];
                $backLink = urldecode($backLink);
                $this->redirect("%$backLink%");
            } elseif($this->getBacklink()) {
                $backlink = $this->getBacklink();
                $this->removeBacklink();
                $this->redirect("%$backlink%");
            } else {
                $this->redirect(CFRegisteredOfficeControler::REGISTER_OFFICE_PAGE, 'company_id='.$this->company->getCompanyId());
            }

            // change company name
        } elseif ($form->isSubmitedBy('change')) {
            //pr('TODO Pete'); exit;
            $this->company->changeCompanyName($form->getValue('companyName'));
            $this->flashMessage('Your company name has been changed');

            if (isset($this->get['backlink'])) {
                $backLink = $this->get['backlink'];
                $backLink = urldecode($backLink);
                $this->redirect("%$backLink%");
            } elseif($this->getBacklink()) {
                $backlink = $this->getBacklink();
                $this->removeBacklink();
                $this->redirect("%$backlink%");
            } else {
                $this->redirect(CFRegisteredOfficeControler::REGISTER_OFFICE_PAGE, 'company_id='.$this->company->getCompanyId());
            }

            // search
        } elseif ($form->isSubmitedBy('search')) {
            $this->template->available = 1;
        }
    }

    
    public function Validator_companyName($control, $error, $params)
    {
        if ($control->owner->isSubmitedBy('back') == FALSE) {
            try {
                if ($control->getValue() == '') {
                    return $error[0];
                }
                $available = CompanySearch::isAvailable($control->getValue());
                return ($available === TRUE) ? TRUE : $error[1];
            } catch (Exception $e) {
                return $e->getMessage();
            }
        }
        return TRUE;
    }

}