<?php

class PackagePageControler extends BaseControler
{
    protected function handleDefault()
    {
        if (isset($this->get['buy'])) {
            try {
                $basket = new Basket();
                $companyName = 'Please Choose a Company Name Limited';

                $productId = (int) $this->get['buy'];
                $item = FNode::getProductById($productId);

                $item->companyName = $companyName;
                $item->removeExistingPackage($basket);

                $session = FApplication::$session->getNameSpace(SearchControler::$nameSpace);
                $session->companyName = $companyName;

                $basket->add($item);

            } catch (Exception $e) {
                $this->flashMessage($e->getMessage());
            }

            $this->flashMessage('Package has been added to your basket');
            $this->redirect(BasketControler::PAGE_BASKET);
        }
    }

    protected function renderDefault()
    {
        $packages = array();
        $included = $this->node->getProperty('packages');
        if ($included != NULL) {
            $included = explode(',', $included);
            foreach ($included as $key => $val) {
                $packages[$val] = $this->node->getProductById($val);
            }
        }

        $this->template->packages = $packages;
    }
}
