<?php

use CompanyFormationModule\Factories\CFSummaryControlerViewFactory;
use CompanyFormationModule\Forms\SicCodesChangeData;
use CompanyFormationModule\Forms\SicCodesChangeForm;
use CompanyFormationModule\Views\IncorporationSummaryViewFactory;
use Dispatcher\Events\CompanyEvent;
use Symfony\Component\EventDispatcher\EventDispatcher;

class CFSummaryControler extends CFControler
{
    const SUMMARY_PAGE = 168;

    /**
     * @var array
     */
    public $possibleActions = [
        self::SUMMARY_PAGE => 'summary',
    ];

    /**
     * @var CFSummaryControlerViewFactory
     */
    private $controllerViewFactory;

    /**
     * @var IncorporationSummaryViewFactory
     */
    private $incorporationSummaryViewFactory;

    /**
     * @var CompanyIncorporation
     */
    private $incorporation;

    public function startup()
    {
        parent::startup();

        $this->controllerViewFactory = $this->getService('company_formation_module.factories.cfsummary_controler_view_factory');
        $this->incorporationSummaryViewFactory = $this->getService('company_formation_module.views.incorporation_summary_view_factory');
    }

    public function beforePrepare()
    {
        parent::beforePrepare();
        // check permission
        if ($this->checkStepPermission('summary') == FALSE) {
            $this->redirect(CFArticlesControler::ARTICLES_PAGE, "company_id={$this->company->getCompanyId()}");
        }

        $this->incorporation = $this->company->getLastIncorporationFormSubmission()->getForm();
    }

    public function renderSummary()
    {
        // has default company name
        if ($this->incorporation->getCompanyType() == 'BYGUAR' && $this->company->getCompanyName() == 'Please Choose a Company Name Limited') {
            $this->template->defaultCompanyName = 1;
        }

        if ($this->company->getStatus() != 'pending') {
            $form = new FForm('summary');
            $form->addSubmit('submit', 'Submit')->class('btn_submit')
                ->onclick('return confirm("Are you sure?")');
            $form->onValid = array($this, 'Form_summaryFormSubmitted');
            $form->start();
            $this->template->form = $form;
        }

        $this->template->view = $this->controllerViewFactory->create($this->companyEntity);
        $this->template->hide = 1;
        $this->template->companyType = $this->getCompanyType();
        /* check if we need to display memebers or subscribers */
        $this->template->origType = $this->incorporation->getCompanyType();
        $this->template->office = $this->getComponentOffice();

        $this->template->directors = $this->getComponentPersons('dir');
        $this->template->shareholders = $this->getComponentPersons('sub');
        $this->template->secretaries = $this->getComponentPersons('sec');

        $this->template->summaryView = $this->incorporationSummaryViewFactory->from($this->incorporation, $this->companyEntity);
        $this->template->articles = $this->getComponentArticle();
        $this->template->companyEntity = $this->companyEntity;


        if (Feature::featurePsc()) {
            $sicCodesChangeData = new SicCodesChangeData();
            $sicCodesChangeData->setSicCodes($this->incorporation->getSicCodes());

            $sicCodesForm = $this->createForm(new SicCodesChangeForm(), $sicCodesChangeData);
            if ($sicCodesForm->isValid()) {
                $this->incorporation->setSicCodes($sicCodesChangeData->getSicCodes());
                $this->redirect();
            }

            $this->template->sicCodes = $sicCodesChangeData->getSicCodes();
            $this->template->sicCodesForm = $sicCodesForm->createView();
        }

        if ($this->company->getStatus() == 'rejected') {
            $rejectedDesc = $this->company->getLastFormSubmission()->getForm()->getRejectDesc();
            if (preg_match('#credit limit#', $rejectedDesc)) {
                $this->template->rejectedDesc = 'There has been an unexpected error sending your request to Companies House. We apologise for the inconvenience. Please resubmit your application and it should be formed in 3 working hours';
            } else {
                $this->template->rejectedDesc = $rejectedDesc;
            }
        }
    }

    /**
     * @param FForm $form
     * @throws Exception
     */
    public function Form_summaryFormSubmitted(FForm $form)
    {
        /** @var $eventDispatcher EventDispatcher */
        $eventDispatcher = $this->getService(DiLocator::DISPATCHER);

        try {
            $this->company->getLastFormSubmission()->sendRequestImproved();
            $eventDispatcher->dispatch(EventLocator::COMPANY_INCORPORATION_PENDING, new CompanyEvent($this->companyEntity));
            $form->clean();
            $this->redirectRoute('exclusive_offers', ['companyName' => $this->company->getCompanyName()]);
        } catch (Exception $e) {
            $eventDispatcher->dispatch(EventLocator::COMPANY_INCORPORATION_ERROR, new CompanyEvent($this->companyEntity));
            $this->flashMessage('There has been an error with your submission to Companies House. Our staff have been alerted to the problem and are actively investigating. We expect to have the issue resolved shortly and will email you as soon as it is done.', 'error');
            $this->redirect();
        }
    }


    /****************************************** components ******************************************/

    /**
     * @return object
     */
    private function getComponentOffice()
    {
        $countries = ['EW' => 'England and Wales', 'SC' => 'Scotland'];
        $office = (object) $this->incorporation->getAddress()->getFields();
        $office->country = isset($countries[$office->country]) ? $countries[$office->country] : $office->country;

        return $office;
    }


    /**
     * @param string $type
     * @return array
     */
    private function getComponentPersons($type)
    {
        $persons = $this->incorporation->getIncPersons([$type]);
        $corporates = $this->incorporation->getIncCorporates([$type]);
        $persons = array_merge($persons, $corporates);

        $return = [];
        foreach ($persons as $key => $val) {
            $return[] = (object) $val->getFields();
        }

        return $return;
    }

    /**
     * @return array
     */
    private function getComponentArticle()
    {
        $articles = [
            'article' => $this->company->getLastIncorporationFormSubmission()->getMemarts(),
            'support' => $this->company->getLastIncorporationFormSubmission()->getSuppnameauth(),
        ];

        return $articles;
    }
}
