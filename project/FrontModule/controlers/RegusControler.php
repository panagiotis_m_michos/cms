<?php
class RegusControler extends CFControler
{
    const VIRTUAL_OFFICE_REGUS = 1420;

    public $possibleActions = array(
        self::VIRTUAL_OFFICE_REGUS => 'regus',
    );


    public function prepareRegus()
    {
        //check if wholesale
        if($this->customer->isWholesale()) {
            $this->redirect(JourneyControler::JOURNEY_PAGE);
        }
        
        //check if virtual office in orders already
        if ($this->service->checkOrderItemsHasProduct(VirtualOfficeControler::MAIL_FORWARDING_PRODUCT) 
            || $this->service->checkOrderItemsHasProduct(VirtualOfficeControler::MAIL_FORWARDING_TELEPHONE_ANSWERING_PRODUCT)
            || $this->service->checkOrderItemsHasProduct(VirtualOfficeControler::MAIL_FORWARDING_2015)
        ) {
            $this->redirect(JourneyControler::JOURNEY_PAGE);
        }
        
        $c = $this->customer;
        $regus = new Regus($c->customerId, $c->firstName, $c->lastName, $c->email, $c->phone);
        $form = new RegusForm($this->node->getId() . '_office');
        $form->startup($this, array($this, 'Form_officeRegusFormSubmitted'), $this->emailerFactory->get(EmailerFactory::REGUS_OFFICE), $regus);
        $this->template->form = $form;
    }
    
    public function Form_officeRegusFormSubmitted(RegusForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('We have passed your details to Regus. They will contact you shortly to discuss you needs');
            $this->redirect(JourneyControler::JOURNEY_PAGE);
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect(RegusControler::VIRTUAL_OFFICE_REGUS);
           }
    }
}
