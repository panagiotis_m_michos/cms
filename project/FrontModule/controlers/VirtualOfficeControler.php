<?php

class VirtualOfficeControler extends CFControler
{
    /* pages */
    const VIRTUAL_OFFICE_PAGE = 1276;
    const ONE_CLICK_PAYMENT_PAGE = 1277;

    /** @var array */
    public $possibleActions = [
        self::VIRTUAL_OFFICE_PAGE => 'virtualoffice',
        self::ONE_CLICK_PAYMENT_PAGE => 'oneclickpayment',
    ];

    /* products */
    const MAIL_FORWARDING_PRODUCT = 1270;
    const MAIL_FORWARDING_TELEPHONE_ANSWERING_PRODUCT = 1271;
    const MAIL_FORWARDING_2015 = 1625;
    const MAIL_FORWARDING_PRODUCT_3_MONTHS = 443;

    /**
     * @var string
     */
    static public $handleObject = 'TokensModel';

    /**
     * @var TokensModel
     */
    public $node;


    /****************************************** virtualoffice ******************************************/


    public function renderVirtualoffice()
    {
        $token = $this->node->getCustomerToken($this->customer);

        if ($this->hasMailForwardingAlreadyIncluded()
            || $this->customer->isWholesale()
            || !$token
            || $this->hasCompanyMailForwardingAlreadyIncluded()
            || $this->hasInternationalPackage()
        ) {
            $this->redirect(DashboardCustomerControler::DASHBOARD_PAGE);
        }

        $this->template->token = $token;
        $this->template->confirmationLink = $this->router->link(
            self::ONE_CLICK_PAYMENT_PAGE,
            ["company_id" => $this->company->getCompanyId()]
        );
    }

    /****************************************** oneclickpayment ******************************************/

    public function renderOneclickpayment()
    {
        if ($this->node->getCustomerToken($this->customer) == NULL) {
            $this->redirect(self::VIRTUAL_OFFICE_PAGE);
        }

        Debug::disableProfiler();
        $backlink = $this->router->link(
            self::VIRTUAL_OFFICE_PAGE,
            ["company_id" => $this->company->getCompanyId()]
        );
        $this->saveBacklink($backlink);

        $this->template->token = $this->node->getCustomerToken($this->customer);
        $this->template->product = Product::getProductById(self::MAIL_FORWARDING_2015);
    }

    /****************************************** private ******************************************/

    /**
     * @return bool
     */
    private function hasMailForwardingAlreadyIncluded()
    {
        $items = [
            Package::PACKAGE_DIAMOND,
            Package::PACKAGE_ULTIMATE,
            self::MAIL_FORWARDING_PRODUCT_3_MONTHS
        ];
        foreach ($items as $item) {
            if ($this->service->checkOrderHasItem($item)) {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * @return bool
     */
    private function hasCompanyMailForwardingAlreadyIncluded()
    {
        $items = [self::MAIL_FORWARDING_PRODUCT, self::MAIL_FORWARDING_TELEPHONE_ANSWERING_PRODUCT, self::MAIL_FORWARDING_2015];
        foreach ($items as $item) {
            if ($this->service->checkOrderItemsHasProduct($item)) {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * @return bool
     */
    private function hasInternationalPackage()
    {
        return $this->service->checkOrderHasItem(Package::PACKAGE_INTERNATIONAL);
    }

}
