<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    ErrorControler.php 2009-01-08 divak@gmail.com
 */


class ErrorControler extends BaseControler
{
	public function startup()
	{
		header("HTTP/1.1 404 Not Found");
        FApplication::$lang = FApplication::$config['default_lng'];
	}
}
