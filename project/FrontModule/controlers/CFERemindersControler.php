<?php

class CFERemindersControler extends CFControler
{
    
    const EREMINDER_PAGE = 1176;

    public $possibleActions = array(
        self::EREMINDER_PAGE => 'default',
    );
    
    public function renderDefault(){
        $reminderForm = new EReminderForFormationForm('Ereminder');
        $reminderForm->startup($this, array($this, 'Form_ereminderFormFormSubmitted'),$this->company);
        $this->template->form = $reminderForm;
       
    }
    
    /**
     * @param EReminderForFormationForm $form
     */
    public function Form_ereminderFormFormSubmitted(EReminderForFormationForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Ereminder has been set');
            $this->reloadOpener();
            //$this->redirect('CFSummaryControler::SUMMARY_PAGE',"company_id={$this->company->getCompanyId()}");
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('default',"company_id={$this->company->getCompanyId()}");
        }
    }
	
}
