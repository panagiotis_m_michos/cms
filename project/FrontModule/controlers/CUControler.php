<?php

use Entities\Company as CompanyEntity;
use Services\CompanyService;

abstract class CUControler extends LoggedControler
{
    /**
     * @var Company
     */
    protected $company;

    /**
     * @var CompanyService
     */
    protected $companyService;

    /**
     * @var CompanyEntity
     */
    protected $companyEntity;

    /**
     * @var bool
     */
    protected $authCodeRequired = FALSE;

    /**
     * @var Prefiller 
     */
    protected $prefiller;
    
    public function startup()
    {
        parent::startup();
        $this->companyService = $this->getService(DiLocator::SERVICE_COMPANY);
    }

    /**
     * Provide checking if company belongs to logged customer
     * @return void
     */
    public function beforePrepare()
    {
        // check get parameter
        if (!isset($this->get['company_id'])) {
            $this->redirect(CompaniesCustomerControler::COMPANIES_PAGE);
        }

        // get company
        try {
            $chFiling = new CHFiling();
            $this->company = $chFiling->getCustomerIncorporatedCompany((int) $this->get['company_id'], $this->customer->getId());
            // @todo: consider return incoporated company
            $this->companyEntity = $this->companyService->getCustomerCompany($this->customerEntity, $this->company->getId());

            // locked company
            if ($this->company->isLocked() && $this->getClass() != 'CUSummaryControler') {
                $this->redirect(CUSummaryControler::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
            }

            // company is hidden
            if ($this->company->isHidden()) {
                $this->flashMessage("Company `{$this->company->getCompanyId()}` doesn't exists", 'error');
                $this->redirect(CompaniesCustomerControler::COMPANIES_PAGE);
            }

            // company is deleted -- added: Razvan Preda 25/05/2011
            if ($this->company->isDeleted()) {
                $this->flashMessage("Company `{$this->company->getCompanyId()}` doesn't exists !", 'error');
                $this->redirect(CompaniesCustomerControler::COMPANIES_PAGE);
            }

            $this->prefiller = new Prefiller($this->company);
                    
            $this->template->company = $this->company;
            $this->template->companyId = $this->company->getCompanyId();
            $this->template->companyName = $this->company->getCompanyName();
            $this->template->companyType = $this->company->getType();

            $this->template->companyEntity = $this->companyEntity;

        } catch (Exception $e) {
            $this->flashMessage($e->getMessage() . "<br/>To complete the Change of Name or the Annual Return, please  <a href='" . FApplication::$router->link(CFRegisteredOfficeControler::REGISTER_OFFICE_PAGE, "company_id={$this->get['company_id']}") . "'>click here</a> complete the formation process.", 'error', FALSE);
            $this->redirect(CompaniesCustomerControler::COMPANIES_PAGE);
        }

        if ($this->authCodeRequired && !$this->companyEntity->hasAuthenticationCode()) {
            $this->redirectToChangeAuthCode();
        }

        parent::beforePrepare();
    }

    /**
     * Returns addreses for select and javascript used for prefill address
     * @param object $company
     * @return array $return
     */
    public static function getPrefillAdresses($company)
    {
        try {
            $addresses = $company->getAddresses();
        } catch (Exception $e) {
            $addresses = array();
        }

        $addressesFields = [];
        $selectValues = [];
        foreach ($addresses as $key => $address) {
            $fields = $address->getFields();
            $addressesFields[$key] = $fields;
            $selectValues[$key] = $fields['postcode'] . ', ' . $fields['premise'] . ' ' . $fields['street'];
        }

        return [
            'select' => $selectValues,
            'js' => json_encode($addressesFields, JSON_FORCE_OBJECT),
        ];
    }

    /**
     * @param $company
     * @param string $type
     * @return array
     */
    public static function getPrefillOfficers($company, $type = 'person')
    {
        $officersFields = [];
        $selectValues = [];

        //todo: psc - preco vrati Share Shareholder, ked ti su v ch_incorporation_member?
        //todo: psc - add psc type?
        if ($type == 'person') {
            $persons = $company->getPersons(array('dir', 'sub', 'sec'));

            foreach ($persons as $key => $person) {
                $fields = $person->getFields();
                $officersFields[$key] = $fields;
                $selectValues[$key] = $fields['surname'] . ' ' . $fields['forename'];
            }
        } elseif ($type == 'corporate') {
            $persons = $company->getCorporates(array('dir', 'sub', 'sec'));

            foreach ($persons as $key => $person) {
                $fields = $person->getFields();
                $officersFields[$key] = $fields;
                $selectValues[$key] = $fields['corporate_name'];
            }
        }

        return [
            'select' => $selectValues,
            'js' => json_encode($officersFields, JSON_FORCE_OBJECT),
        ];
    }

    /**
     * Provides validation EEA/NonEAA fields
     *
     * @param Text $control
     * @param string $error
     * @internal param array $params
     * @return mixed
     */
    public function Validator_eeaRequired(Text $control, $error)
    {
        if ($control->owner['eeaType']->getValue() !== NULL) {

            // EEA
            if ($control->owner['eeaType']->getValue() == 1) {
                if (($control->getName() == 'place_registered' || $control->getName() == 'registration_number') && $control->getValue() == '') {
                    return $error;
                }
                // non EEA
            } else {
                if (($control->getName() == 'place_registered' || $control->getName() == 'law_governed' || $control->getName() == 'legal_form') && $control->getValue() == '') {
                    return $error;
                }
            }
        }
        return TRUE;
    }

    /**
     * @param DateSelect $control
     * @param array $error
     * @param string $minYears
     * @return bool
     */
    public function Validator_personDOB(DateSelect $control, $error, $minYears)
    {
        $value = $control->getValue();

        // check if dob is valid day
        $temp = explode('-', $value);
        if (checkdate($temp[1], $temp[2], $temp[0]) === FALSE) {
            return $error[0];
        }

        // check if director is older than 16 years
        $dob = date('Ymd', strtotime($value));
        $years = floor((date("Ymd") - $dob) / 10000);
        if ($years < $minYears) {
            return sprintf($error[1], $minYears);
        }
        return TRUE;
    }

    /**
     * @param $control
     * @param $error
     * @return bool
     */
    public function Validator_securityEyeColour($control, $error)
    {
        $value = $control->getValue();
        if (String::upper($value) === 'BLA') {
            return $error;
        }
        return TRUE;
    }

    public function redirectToChangeAuthCode()
    {
        $params = array(
            'company_id' => $this->companyEntity->getCompanyId(),
        );

        if ($this->getParameter('authCodePopup')) {
            $params['popup'] = 1;
        }

        $this->saveBacklink();
        $this->redirect(CUChangeWebAuthCodeControler::PAGE_CHANGE_AUTH_CODE, $params);
    }

}
