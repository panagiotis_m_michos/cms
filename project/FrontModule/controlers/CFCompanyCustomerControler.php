<?php

use BankingModule\BankingDecider;
use BankingModule\BankingService;
use BankingModule\Config\DiLocator as BankingDiLocator;
use BankingModule\Domain\BankCashBackOptions;
use BankingModule\Emailers\TsbApplicationEmailer;
use BankingModule\Entities\CompanyCustomer;
use BankingModule\Renderers\ChooseBankRenderer;
use Entities\Company as CompanyEntity;
use Services\CashbackService;
use Services\CompanyService;

class CFCompanyCustomerControler extends LoggedControler
{
    const PAGE_BARCLAYS = 660;
    const PAGE_CARD_ONE = 1406;
    const PAGE_TSB = 1637;
    const PAGE_CHOOSE_BANK_ACCOUNT = 1091;

    /**
     * @var array
     */
    public $possibleActions = [
        self::PAGE_BARCLAYS => 'barclays',
        self::PAGE_CARD_ONE => 'cardOne',
        self::PAGE_TSB => 'tsb',
        self::PAGE_CHOOSE_BANK_ACCOUNT => 'chooseBankAcount',
    ];

    /**
     * @var Company
     */
    protected $company;

    /**
     * @var CFControllerModel
     */
    protected $service;

    /**
     * @var CompanyEntity
     */
    protected $companyEntity;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var BankingDecider
     */
    private $bankingDecider;

    /**
     * @var BankingService
     */
    private $bankingService;

    /**
     * @var ChooseBankRenderer
     */
    private $chooseBankRenderer;

    /**
     * @var CashbackService
     */
    private $cashBackService;

    /**
     * @var BankCashBackOptions
     */
    private $bankCashBackOptions;

    /**
     * @var TsbApplicationEmailer
     */
    private $tsbApplicationEmailer;

    public function beforePrepare()
    {
        parent::beforePrepare();

        $this->companyService = $this->getService(DiLocator::SERVICE_COMPANY);
        $this->bankingDecider = $this->getService(BankingDiLocator::DECIDER_BANKING);
        $this->bankingService = $this->getService(BankingDiLocator::SERVICE_BANKING);
        $this->chooseBankRenderer = $this->getService(BankingDiLocator::RENDERER_CHOOSE_BANK);
        $this->cashBackService = $this->getService('services.cashback_service');
        $this->tsbApplicationEmailer = $this->getService('banking_module.tsb_application_emailer');

        try {
            $chFiling = new CHFiling();
            if (!isset($this->get['company_id'])) {
                throw new Exception('Company Id is required!');
            }
            $this->company = $chFiling->getCustomerCompany($this->get['company_id'], $this->customer->getId());
            $this->companyEntity = $this->companyService->getCustomerCompany($this->customerEntity, $this->get['company_id']);
            $this->bankCashBackOptions = $this->bankingDecider->getCashBackOptions($this->companyEntity);
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect(DashboardCustomerControler::DASHBOARD_PAGE);
        }

        if ($this->companyEntity->isIncorporated()) {
            if (!$this->bankCashBackOptions->canApplyForCashBack()) {
                $this->redirectContinue();
            }
        } else {
            if (!$this->bankingDecider->hasBankingEnabled($this->companyEntity) || $this->bankingDecider->hasDetailsFilled($this->companyEntity)) {
                $this->redirectContinue();
            }
        }

        $this->service = new CFControllerModel($this->company, $this->customer);
        $this->template->company = $this->companyEntity;
    }

    /*     * ********************************************* CHOOSE COMPANY BANK ACCOUNT ********************************************** */

    public function renderChooseBankAcount()
    {
        $selected = $this->getParameter('selected');
        $this->template->bankSelector = $this->chooseBankRenderer->getHtml($this->companyEntity, $selected);
    }

    /*     * ********************************************* Card One ********************************************** */

    public function prepareCardOne()
    {
        if ($this->companyEntity->isIncorporated()) {
            if (!$this->bankCashBackOptions->canApplyForCashBackWith(CompanyCustomer::BANK_TYPE_CARD_ONE)) {
                $this->redirectContinue();
            }
        } else {
            if (!$this->bankingDecider->hasBankInOptions($this->companyEntity, CompanyCustomer::BANK_TYPE_CARD_ONE)) {
                $this->redirectContinue();
            }
        }
    }

    public function renderCardOne()
    {
        $form = new CardOneContactDetailsForm('CardOne');
        $form->startup([$this, 'Form_CardOneFormSubmitted'], $this->customer);
        $this->template->form = $form;
    }

    /**
     * @param CardOneContactDetailsForm $form
     */
    public function Form_CardOneFormSubmitted(CardOneContactDetailsForm $form)
    {
        try {
            $form->process($this->customer->getId(), $this->company->getCompanyId());
            $this->flashMessage('Your details have been submitted to Card One who will be in touch soon.');
            $this->redirectContinue();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    /*     * ********************************************* TSB ********************************************** */

    public function prepareTsb()
    {
        if ($this->companyEntity->isIncorporated()) {
            if (!$this->bankCashBackOptions->canApplyForCashBackWith(CompanyCustomer::BANK_TYPE_TSB)) {
                $this->redirectContinue();
            }
        } else {
            if (!$this->bankingDecider->hasBankInOptions($this->companyEntity, CompanyCustomer::BANK_TYPE_TSB)) {
                $this->redirectContinue();
            }
        }
    }

    public function renderTsb()
    {
        $form = new TsbContactDetailsForm('TSB');
        $form->startup([$this, 'Form_TsbFormSubmitted'], $this->customerEntity, $this->bankingService);
        $this->template->form = $form;
        $this->template->cashBackAmount = $this->cashBackService->getCashBackAmountForCompany($this->companyEntity) ? : $this->bankCashBackOptions->getCashBackAmount();
    }

    /**
     * @param TsbContactDetailsForm $form
     */
    public function Form_TsbFormSubmitted(TsbContactDetailsForm $form)
    {
        try {
            $details = $form->process($this->customerEntity, $this->companyEntity);
            $this->tsbApplicationEmailer->sendOnlineApplication($details);
            $this->flashMessage('Your details have been submitted to TSB. Please follow the email you receive from them shortly to apply directly online.');
            $this->redirectContinue();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    /*     * ********************************************* BARCLAYS ********************************************** */

    public function prepareBarclays()
    {
        if ($this->companyEntity->isIncorporated()) {
            if (!$this->bankCashBackOptions->canApplyForCashBackWith(CompanyCustomer::BANK_TYPE_BARCLAYS)) {
                $this->redirectContinue();
            }
        } else {
            if (!$this->bankingDecider->hasBankInOptions($this->companyEntity, CompanyCustomer::BANK_TYPE_BARCLAYS)) {
                $this->redirectContinue();
            }
        }
    }

    public function renderBarclays()
    {
        $this->template->form = CompanyCustomerModel::getComponentFrontForm([$this, 'Form_companyCustomerFormSubmitted'], $this->customer);
        $this->template->cashBackAmount = $this->cashBackService->getCashBackAmountForCompany($this->companyEntity) ? : $this->bankCashBackOptions->getCashBackAmount();
    }

    /**
     * @param FForm $form
     */
    public function Form_companyCustomerFormSubmitted(FForm $form)
    {
        try {
            $data = $form->getValues();
            CompanyCustomerModel::saveFrontForm($this->customer->getId(), $this->company->getCompanyId(), $data);
            $form->clean();
            $this->flashMessage('Your details have been submitted to barclays. They will call you with 48 hours to arrange a meeting to open your account.');
            $this->redirectContinue();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    private function redirectContinue()
    {
        if ($this->companyEntity->isIncorporated()) {
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, ['company_id' => $this->companyEntity->getId()]);
        } else {
            $this->redirect(CFRegisteredOfficeControler::REGISTER_OFFICE_PAGE, ['company_id' => $this->companyEntity->getId()]);
        }
    }
}
