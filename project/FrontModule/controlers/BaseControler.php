<?php

use Entities\Customer as CustomerEntity;
use Mailgun\Mailgun;
use Services\CustomerService as CustomerEntityService;
use Utils\FeedReader\FeedReader;

abstract class BaseControler extends FControler
{

    const TOP_MENU_FOLDER = 246;
    const BOTTOM_MENU_FOLDER = 235;
    const LEFT_MENU_FOLDER = 185;
    const MY_ACCOUNT_FOLDER = 65;

    /**
     * @var Customer
     */
    protected $customer;

    /**
     * @var CustomerEntity
     */
    protected $customerEntity;

    /**
     * @var Basket
     */
    protected $basket;

    public function startup()
    {
        // @todo: it will create empty Customer object
        $this->customer = Customer::getSignedIn();

        if (!$this->customer->isNew()) {

            /* @var $customerService CustomerEntityService */
            $customerService = $this->getService(DiLocator::SERVICE_CUSTOMER);
            $this->customerEntity = $customerService->getCustomerById($this->customer->getId());

            // @todo: move $this->template to render method (needs test)
            $this->template->customer = $this->customer;

            $submenu = FMenu::get(65);
            if (Customer::getSignedIn()->roleId != Customer::ROLE_WHOLESALE) {
                if (isset($submenu[783])) {
                    unset($submenu[783]);
                }
            }

            $this->template->submenu = $submenu;
        }

        $this->basket = new Basket();
        // @todo: move $this->template to render method (needs test)
        $this->template->basket = $this->basket;

        // CH configuration
        $chFiling = FApplication::$config['chfiling'];
        CHFiling::setPackageReference($chFiling['packageReference']);
        CHFiling::setSenderId($chFiling['senderId']);
        CHFiling::setPassword($chFiling['password']);
        CHFiling::setEmailAddress($chFiling['emailAddress']);
        CHFiling::setGatewayTest($chFiling['gatewayTest']);

        // CH database
        $db = FApplication::$config['database'];
        CHFiling::setDb(array($db['host'], $db['username'], $db['password'], $db['database']));
        CHFiling::setDocPath(PROJECT_DIR . '/temp/upload/ch_documents');

        // enable upgrade page voucher form
        if (isset($this->get['redeem'])) {
            VoucherService::enableUpgradeVoucherForm();
        }
        parent::startup();
    }

    /**
     *
     */
    public function beforeRender()
    {
        parent::beforeRender();

        $this->template->node = $this->node;
        $this->template->topLoginForm = $this->getComponent('login');
        //$this->template->bottomMenu = FMenu::get(self::BOTTOM_MENU_FOLDER);
        $this->template->rightBlock1 = $this->getComponentRightBlock1();
        $this->template->rightBlock2 = $this->getComponentRightBlock2();
        $this->template->rightBlockStartUpGuide = $this->getComponent('startUpGuide');
        $this->template->testimonials = FProperty::getNodePropsValue('testimonials', HomeControler::HOME_PAGE);

        // top menu
        //$this->template->topMenu = $this->getComponentTopMenu();

        // left menu
        $leftMenu = $this->getCompomentLefMenu();
        $this->template->leftMenu = $leftMenu;
        $this->template->currentLeftMenu = $this->getComponentCurrentLeftMenu($leftMenu);

        // back link
        $backlink = FApplication::$httpRequest->uri->getAbsoluteUri();
        $this->template->backLink = urlencode($backlink);


        /** @var FeedReader $postReader */
        $postReader = $this->getService(DiLocator::BLOG_READER);
        $blogPosts = $postReader->getPosts(5);
        $this->template->homeBlogPosts = array_slice($blogPosts, 0, 2);
        $this->template->leftBlogPosts = $blogPosts;

        // left menu items
        $this->template->leftMenuItems = self::getComponentLeftMenuItems();

        $this->template->disabledLeftColumn = FProperty::get('disabledLeftColumn')->value;
        $this->template->disabledRightColumn = FProperty::get('disabledRightColumn')->value;

        $this->template->httpsEnabled = $this->httpsRequired;
        $this->template->dateTimeNow = date('M j, Y H:i:s O');

        if ($this->isSecured()) {
            $this->template->https = 1;
        }
    }

    private function getCompomentLefMenu()
    {
        $cache = FApplication::getCache();
        if (isset($cache['componentLeftMenu'])) {
            return $cache['componentLeftMenu'];
        } else {
            $leftMenu = FMenu::get(self::LEFT_MENU_FOLDER);
            $cache->save('componentLeftMenu', $leftMenu, array('expire' => time() + 60 * 10));
            return $leftMenu;
        }
    }

    private function getComponentCurrentLeftMenu($leftMenu)
    {
        foreach (array_values($leftMenu) as $key => $val) {
            if ($this->node->parentId == $val['id']) {
                return $key;
            }
        }
    }

    private function getComponentTopMenu()
    {
        $signIn = Customer::isSignedIn();
        $cacheKey = 'componentTopMenu_' . (int) $signIn;
        $cache = FApplication::getCache();

        if (isset($cache[$cacheKey])) {
            return $cache[$cacheKey];
        } else {

            $topIds = array_merge(array(92), FNode::getChildsIds(self::TOP_MENU_FOLDER, FALSE));
            $topMenu = FNode::getTitleAndLink($topIds);

            // change login to logout
            if ($signIn === TRUE) {
                $link = $topMenu[LoginControler::LOGIN_PAGE]['link'];
                $topMenu[LoginControler::LOGIN_PAGE] = array('title' => 'Logout', 'link' => $this->router->link("%$link%", 'l=1'));
            }

            // cache for 10 minutes
            $cache->save($cacheKey, $topMenu, array('expire' => time() + 60 * 10));
            return $topMenu;
        }
    }

    private function getComponentRightBlock1()
    {
        $cache = FApplication::getCache();

        // it's cached
        if (isset($cache['componentRightBlock1'])) {
            return $cache['componentRightBlock1'];
        } else {
            $ids = array(267, 447, 268, 269, 270, 271, 272, 273, 274);
            $ret = FNode::getTitleAndLink($ids);

            // cache for 10 minutes
            $cache->save('componentRightBlock1', $ret, array('expire' => time() + 60 * 10));
            return $ret;
        }
    }

    private function getComponentRightBlock2()
    {
        $cache = FApplication::getCache();

        // it's cached
        if (isset($cache['componentRightBlock2'])) {
            return $cache['componentRightBlock2'];
        } else {
            $ids = array(236, 127, 88);
            $ret = FNode::getTitleAndLink($ids);

            //change link names
            $ret[236]['title'] = "Help & Advice";
            $ret[88]['title'] = "Resources";

            // cache for 10 minutes
            $cache->save('componentRightBlock2', $ret, array('expire' => time() + 60 * 10));
            return $ret;
        }
    }

    /**
     * @param int $folder
     * @return array
     */
    protected function getComponentPackages($folder = PackageAdminControler::PACKAGES_FOLDER)
    {
        $cache = $this->getService(DiLocator::CACHE_MEMORY);
        $key = sprintf('component_packages_%d', $folder);
        if ($cache->contains($key)) {
            return $cache->fetch($key);
        }
        $packages = array();
        $ids = FNode::getChildsIds($folder);
        foreach ($ids as $val) {
            $packages[$val] = FNode::getProductById($val);
        }
        $cache->save($key, $packages, 1800);
        return $packages;
    }

    /**
     * @param string $action
     * @return FForm
     */
    public static function getSearchForm($action = NULL)
    {
        $form = new FForm('search', 'post');
        if ($action) {
            $form->setAction($action);
        }
        $form->addText('q', 'Company name:')
            //->addRule(array($this, 'Validator_companyNameChars'), 'Illegal characters detected. Only characters on the English keyboard may be used. Please retype rather than copy & paste if you are having issues.', '#[^\x20-\x7E]#')
            //->addRule(array($this, 'Validator_companyName'), array('Please provide company name!', 'Company name is taken!'))
            ->class('searchbox');
        $form->addSubmit('send', 'Search')
            ->class('btn_submit');
        $form->clean();
        $form->start();
        return $form;
    }

    /**
     * @param int|null $packageId
     * @return FForm
     */
    public static function getCompanyNameForm($packageId = NULL, $proceed = NULL)
    {
        $form = new FForm('CFCompanyName');
        $form->addText('companyName', 'Search For Your Company Name')
            ->style(array('height' => '28px', 'width' => '380px'))
            ->addRule(array('CompanyNameValidator', 'isValid'), NULL)
            ->addRule(FForm::Required, 'Company Name is required');

        $form->addFieldset('Action');
        if ($packageId) {
            $form->createElement('HiddenWithValue', 'package')->setValue($packageId);
        }
        $form->addSubmit('search', 'Search')->style(array('height' => '30px', 'width' => '100px'));
        $form->addSubmit('change', 'Proceed')->class('btn_submit fright mbottom');
        $form->start();
        return $form;
    }

    protected function getComponent($name)
    {
        switch ($name) {
            case 'startUpGuide':
                $startUpGuideForm = new StartUpGuideForm(StartUpGuideForm::STARTUP_FORM_ID);
                $startUpGuideForm->startup($this, array($this, 'FormStartUpGuideSubmitted'));
                return $startUpGuideForm;
                break;
            case 'search':
                return self::getSearchForm($this->router->link(102));
                break;
            case 'login':
                $form = new FForm('login');
                $action = $this->router->link(127);
                $form->setAction($action);
                $form->addText('email', 'Email:')
                    ->onClick('this.value=""')
                    ->setValue('Email...');
                $form->addPassword('password', 'Password:')
                    ->setValue('Password...')
                    ->onClick('this.value=""');
                $form->addSubmit('login', 'Login');
                $form->clean();
                $form->start();
                return $form;
                break;
        }
    }

    /**
     * Returns component for left menu
     * @return array
     */
    public static function getComponentLeftMenuItems()
    {
        $leftMenu = FProperty::getNodePropsValue('leftMenu');
        if ($leftMenu !== NULL) {
            $leftMenu = unserialize($leftMenu);
            return $leftMenu;
        }
        return FALSE;
    }

    public function FormStartUpGuideSubmitted(StartUpGuideForm $form)
    {
        try {
            if (FApplication::$httpRequest->isAjax()) {
                $ch = curl_init(StartUpGuideForm::STARTUP_ACTION);
                curl_setopt($ch, CURLOPT_POST, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, FApplication::$httpRequest->getPost());
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                $returndata = curl_exec($ch);
                if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == 404) {
                    throw new Exception('404: Page Not Found');
                }

                //todo create beter validator to validate form mailchimp errors
                if (strpos($returndata, 'There are errors below') !== FALSE) {
                    throw new Exception('Email is already subscribed to list Made Simple Group');
                }
            }
        } catch (Exception $e) {
            echo json_encode(array('error' => 1, 'message' => $e->getMessage()));
            die;
        }
        echo json_encode(array('redirect' => 1));
        die;
    }

}
