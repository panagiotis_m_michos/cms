<?php


class CosecControler extends FControler
{
    /* folder */
    const COSEC_FOLDER = 757;

    /* pages */
    const HOME_PAGE = 758;
    const ABOUT_US_PAGE = 759;
    const SIGN_UP_PAGE = 760;
    const FAQ_PAGE = 761;
    const CONTACT_US_PAGE = 762;
    const LOGIN_PAGE = 766;
    const AFFILIATE_PAGE = 763;
    const WHITE_PAGE = 764;
    const ACCOUNTANTS_PAGE = 765;
    const FORGOTTEN_PASSWORD = 1122;

    /**
     * @var array 
     */
    public $possibleActions = array(
        self::HOME_PAGE => 'home',
        self::ABOUT_US_PAGE => 'default',
        self::SIGN_UP_PAGE => 'signup',
        self::FAQ_PAGE => 'faq',
        self::CONTACT_US_PAGE => 'contact',
        self::LOGIN_PAGE => 'login',
        self::AFFILIATE_PAGE => 'affiliate_wholesale',
        self::WHITE_PAGE => 'white_label',
        self::ACCOUNTANTS_PAGE => 'accountants_and_solicitors',
        self::FORGOTTEN_PASSWORD => 'forgottenPassword'
    );

    /**
     * @var string
     */
    static public $handleObject = 'CosecModel';

    /**
     * @var CosecModel
     */
    public $node;

    /**
     * If https is reuquired, than redirect
     *
     * @var Boolean
     */
    protected $httpsRequired;

    /**
     * @var CosecEmailer; 
     */
    protected $cosecEmailer;
    
    public function startup()
    {
        if ($this->nodeId == self::LOGIN_PAGE || $this->nodeId == self::SIGN_UP_PAGE) {
            $this->httpsRequired = TRUE;
        }
        parent::startup();
        $this->cosecEmailer = $this->emailerFactory->get(EmailerFactory::COSEC);
    }

    
    public function beforeRender()
    {
        $this->template->topMenu = $this->node->getTopMenu();
        $this->template->templateRightImage = $this->node->getTemplateRightImage();
        parent::beforeRender();
    }


    
    /*********************************************** home ***********************************************/
     
    
    
    public function renderHome()
    {
        $this->template->slideShow = $this->node->getHomeSlideShow();
    }


    
    /*********************************************** signup ***********************************************/
    
    public function renderSignup()
    {
        $signupForm = new CosecSignupForm($this->node->getId() . '_signup');
        $signupForm->startup($this, array($this, 'Form_signupFormSubmitted'), $this->cosecEmailer);
        $this->template->form = $signupForm;
    }

    /**
     * @param CosecSignupForm $form
     */
    public function Form_signupFormSubmitted(CosecSignupForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Your account has been created');
            $this->redirect(DashboardCustomerControler::DASHBOARD_PAGE);
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('signup');
        }
    }


    /*********************************************** login ***********************************************/
    
    
    public function renderLogin()
    {
        $loginForm = new CosecLoginForm('login');
        $loginForm->startup($this, array($this, 'Form_loginFormSubmitted'));
        $this->template->form = $loginForm;
    }

    /**
     * @param CosecLoginForm $form
     */
    public function Form_loginFormSubmitted(CosecLoginForm $form)
    {
        try {
            $form->process();
            $this->redirect(DashboardCustomerControler::DASHBOARD_PAGE);
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('login');
        }
    }

    /*********************************************** contact ***********************************************/

    public function renderContact()
    {
        $contactForm = new CosecContactForm($this->node->getId() . '_contact');
        $contactForm->startup($this, array($this, 'Form_contactFormSubmitted'), $this->cosecEmailer);
        $this->template->form = $contactForm;
    }

    /**
     * @param CosecContactForm $form
     */
    public function Form_contactFormSubmitted(CosecContactForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Message has been sent');
            $this->redirect('contact');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('contact');
        }
    }

    public function renderforgottenPassword()
    {
        $form = new CosecForgottenPasswordForm($this->node->getId() . '_ForgottenPassword');
        $form->startup($this, array($this, 'Form_ForgottenPasswordFormSubmitted'), $this->cosecEmailer);
        $this->template->form = $form;
    }

    /**
     * @param CosecForgottenPasswordForm $form
     */
    public function Form_ForgottenPasswordFormSubmitted(CosecForgottenPasswordForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('New password has been sent to your email.');
            $this->redirect(CosecControler::LOGIN_PAGE);
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    
}