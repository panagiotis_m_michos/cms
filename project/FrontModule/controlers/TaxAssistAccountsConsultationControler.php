<?php

use TaxAssistAccountsConsultation\AccountsForm;
use TaxAssistAccountsConsultation\IAccountFormDelegate;
use TaxAssistAccountsConsultation\SoletraderForm;
use TaxAssistAccountsConsultation\ISoletraderFormDelegate;
use TaxAssistAccountsConsultation\ServicesForm;
use TaxAssistAccountsConsultation\IServicesFormDelegate;

class TaxAssistAccountsConsultationControler extends DefaultControler implements IAccountFormDelegate, ISoletraderFormDelegate, IServicesFormDelegate
{
    // pages
    const ACCOUNTS_PAGE = 801;
    const SOLETRADER_PAGE = 1163;
    const SERVICES_PAGE = 251;

    /**
     * @var array
     */
    public $possibleActions = array(
        self::ACCOUNTS_PAGE => 'accounts',
        self::SOLETRADER_PAGE => 'soletrader',
        self::SERVICES_PAGE => 'services',
    );


    /*********************************************** accounts ***********************************************/

    public function renderAccounts()
    {
        $form = new AccountsForm($this->node->getId() . '_accounts');
        $form->startup(
            $this,
            $this->getService(DiLocator::SERVICE_TAX_ASSIST_LEAD)
        );
        $this->template->form = $form;
    }


    /*********************************************** soletrader ***********************************************/

    public function renderSoletrader()
    {
        $form = new SoletraderForm($this->node->getId() . '_soletrader');
        $form->startup(
            $this,
            $this->getService(DiLocator::SERVICE_TAX_ASSIST_LEAD)
        );
        $this->template->form = $form;
    }

    /*********************************************** services ***********************************************/

    public function renderServices()
    {
        $form = new ServicesForm($this->node->getId() . '_services');
        $form->startup(
            $this,
            $this->getService(DiLocator::SERVICE_TAX_ASSIST_LEAD)
        );
        $this->template->form = $form;
    }

    /*********************************************** IAccountFormDelegate ***********************************************/


    public function accountFormSucceeded()
    {
        $this->flashMessage('Your details have been passed directly to TaxAssist. They will contact you shortly to arrange a consultation.');
        $this->redirect('accounts');
    }

    public function accountFormFailed(\Exception $e)
    {
        $this->flashMessage($e->getMessage(), 'error');
        $this->redirect('accounts');
    }

    /*********************************************** ISoletraderFormDelegate ***********************************************/

    public function soletraderFormSucceeded()
    {
        $this->flashMessage('Your details have been passed directly to TaxAssist. They will contact you shortly to arrange a consultation.');
        $this->redirect('soletrader');
    }

    public function soletraderFormFailed(Exception $e)
    {
        $this->flashMessage($e->getMessage(), 'error');
        $this->redirect('soletrader');
    }

    /*********************************************** IServicesFormDelegate ***********************************************/


    public function servicesFormSucceeded()
    {
        $this->flashMessage('Your details have been passed directly to TaxAssist. They will contact you shortly to arrange a consultation.');
        $this->redirect('accounts');
    }

    public function servicesFormFailed(Exception $e)
    {
        $this->flashMessage($e->getMessage(), 'error');
        $this->redirect('accounts');
    }

}
