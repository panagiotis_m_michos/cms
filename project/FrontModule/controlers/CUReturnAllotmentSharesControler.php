<?php

class CUReturnAllotmentSharesControler extends CUControler
{
    const SUMMARY_PAGE = 639;

    /**
     * @var array
     */
    public $possibleActions = array(
        self::SUMMARY_PAGE => 'summary',
    );

    /**
     * @var ReturnOfAllotmentSharesModel
     */
    private $model;

    
    public function beforePrepare()
    {
        if ($this->action == $this->possibleActions[self::SUMMARY_PAGE]) {
            $this->authCodeRequired = TRUE;
        }

        parent::beforePrepare();
        try {
            $this->model = new ReturnOfAllotmentSharesModel($this->company);
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
        }
    }

    /******************************** summary ******************************/

    public function renderSummary()
    {
        try {
            $this->template->form = ReturnOfAllotmentSharesModel::getComponentSummaryForm($this, $this->model);
            $this->template->shares = $this->model->getShares();

        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
        }
    }

    /**
     * @param FForm $form
     */
    public function Form_summaryFormSubmitted(FForm $form)
    {
        try {
            $data = $form->getValues();
            $this->model->saveSummary($this->company, $data);
            $form->clean();
            $this->flashMessage('Application has been sent to companies house');
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }
}
