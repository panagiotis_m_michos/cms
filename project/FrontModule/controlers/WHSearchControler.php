<?php

use Exceptions\Business\CompaniesHouseException;
use Services\Facades\Wholesale\SearchFacade;
use Wholesale\CompanySearchForm;
use Wholesale\IPackagesFormDelegate;
use Wholesale\PackagesForm;

/**
 * @TODO make sure its not used create redirections and remove
 */
class WHSearchControler extends LoggedControler implements IPackagesFormDelegate
{
    const PAGE_SEARCH = 605;
    const PAGE_PACKAGES = 607;

    /**
     * @var array
     */
    public $possibleActions = array(
        self::PAGE_SEARCH => 'default',
        self::PAGE_PACKAGES => 'packages',
    );

    /**
     * @var string
     */
    private $companyName;

    /**
     * @var string
     */
    static public $handleObject = 'WholesaleSearchNode';

    /**
     * @var WholesaleSearchNode
     */
    public $node;

    /**
     * @var SearchFacade
     */
    public $wholesaleSearchFacade;


    public function startup()
    {
        parent::startup();
        $this->wholesaleSearchFacade = $this->getService(DiLocator::FACADE_WHOLESALE_SEARCH);
    }


    public function beforePrepare()
    {
        parent::beforePrepare();
        if (!$this->customer->isWholesale()) {
            $this->redirect(DashboardCustomerControler::DASHBOARD_PAGE);
        }
    }


    /******************************* default *****************************************/


    public function handleDefault()
    {
        $companyName = NULL;

        try {
            $form = new CompanySearchForm($this->customerEntity->getId() . 'wh_company_search');
            $form->startup($this);

            if ($form->isOk2Save()) {
                $companyName = $form->getValue('companyName');
                $available = CompanySearch::isAvailable($companyName);
                if (!$available) {
                    $this->flashMessage('Company name has been taken', 'error');
                    $this->redirect();
                } else {
                    $this->wholesaleSearchFacade->saveCompanyName($companyName);
                    $this->redirectRoute('professional_matrix');
                }
            }

            $this->template->searchForm = $form;

        } catch (Exception $e) {
            if (!($e instanceof CompaniesHouseException)) {
                $e = CompaniesHouseException::companiesHouseServiceUnreachable();
            }

            $this->flashMessage($e->getMessage(), 'error');
            $this->wholesaleSearchFacade->saveCompanyName($companyName);
            $this->redirectRoute('professional_matrix');
        }
    }


    /******************************* packages *****************************************/


    public function preparePackages()
    {
        $this->companyName = $this->wholesaleSearchFacade->getCompanyName();
        if (!$this->companyName) {
            $this->flashMessage('Please search for company name first', 'error');
            $this->redirect(WHSearchControler::PAGE_SEARCH);
        }
    }

    public function renderPackages()
    {
        $packages = $this->node->getPackages();

        $form = new PackagesForm($this->customer->getId() . '_wh_packages');
        $form->startup($this, $packages, $this->wholesaleSearchFacade);
        $this->template->form = $form;

        $this->template->packages = $packages;
        $this->template->companyName = $this->companyName;

        $this->template->companyAvaliable = TRUE;
        if (isset($this->get['namesearch']) && $this->get['namesearch'] == 'unavailable') {
            $this->template->companyAvaliable = FALSE;
        }
    }

    /****************************************** IPackagesFormDelegate ******************************************/


    public function wholeSalePackagesFormSucceeded(Package $package)
    {
        $this->wholesaleSearchFacade->addPackage($package, $this->wholesaleSearchFacade->getCompanyName());
        $this->redirectRoute('basket_module_package_basket_upgrade', ['package_id' => $package->getId()]);
    }

    /**
     * @param Exception $e
     */
    public function wholeSalePackagesFormFailed(Exception $e)
    {
        $this->flashMessage($e->getMessage(), 'error');
        $this->redirect();
    }
}
