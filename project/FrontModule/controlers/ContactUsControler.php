<?php

class ContactUsControler extends DefaultControler
{
    /**
     * @var ContactUsEmailer
     */
    public $emailer;

    public function startup()
    {
        parent::startup();
        $this->emailer = $this->getService(DiLocator::EMAILER_CONTACT_US);
    }

    public function renderDefault()
    {
        $form = new ContactForm('contact');
        $form->startup(array($this, 'Form_defaultFormSubmitted'));
        $this->template->form = $form;
    }

    /**
     * @param FForm $form
     */
    public function Form_defaultFormSubmitted(FForm $form)
    {
        $this->emailer->contactUsEmail($form->getValues());
        $form->clean();
        $this->flashMessage('Your query has been sent successfully.');
        $this->redirect();
    }
}
