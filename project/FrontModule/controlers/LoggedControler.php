<?php

use Entities\Customer as CustomerEntity;
use Services\CustomerService;

abstract class LoggedControler extends DefaultControler
{
    /**
     * @var Customer
     */
    protected $customer;

    /**
     * @var CustomerEntity
     */
    protected $customerEntity;

    /**
     * @var CustomerService
     */
    protected $customerService;

    /**
     * @var bool
     */
    protected $httpsRequired = TRUE;
    
    /**
     * @var boolean
     */
    protected $finishedRegistrationRequired = FALSE;


    public function startup()
    {
        if (Customer::isSignedIn() === FALSE) {
            $this->saveBacklink();
            $this->redirect(LoginControler::LOGIN_PAGE);
        }

        $this->customer = Customer::getSignedIn();
        $this->customerService = $this->getService(DiLocator::SERVICE_CUSTOMER);
        $this->customerEntity = $this->customerService->getCustomerById($this->customer->getId());

        // if his registration is not completed
        if ($this->finishedRegistrationRequired && !$this->customer->hasCompletedRegistration()) {
            $this->saveBacklink();
            $this->redirect(MyDetailsControler::MY_DETAILS_PAGE);
        }

        parent::startup();
    }
}
