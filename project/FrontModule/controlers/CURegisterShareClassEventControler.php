<?php

use Entities\Register\ShareClass;
use Entities\Register\ShareClassEvent;
use Front\Register\ShareClassEvent\IShareClassEventFormDelegate;
use Front\Register\ShareClassEvent\ShareClassEventForm;
use Services\Register\ShareClassEventService;
use Services\Register\ShareClassService;

class CURegisterShareClassEventControler extends CURegisterControler_Abstract implements IShareClassEventFormDelegate
{
    const PAGE_ADD = 1446;

    /**
     * @var array
     */
    public $possibleActions = array(
        self::PAGE_ADD => 'add',
    );

    /**
     * @var ShareClassEventService
     */
    private $registerShareClassEventService;

    /**
     * @var ShareClassService
     */
    private $registerShareClassService;

    /**
     * @var ShareClass
     */
    private $shareClass;

    public function startup()
    {
        parent::startup();
        $this->registerShareClassService = $this->getService(DiLocator::SERVICE_REGISTER_SHARE_CLASS);
        $this->registerShareClassEventService = $this->getService(DiLocator::SERVICE_REGISTER_SHARE_CLASS_EVENT);
    }

    public function beforePrepare()
    {
        parent::beforePrepare();

        try {
            $registerShareClassId = $this->getParameter('registerShareClassId');
            if (!$registerShareClassId) {
                throw new InvalidArgumentException('Missing share class id');
            }
            $this->shareClass = $this->registerShareClassService->getCompanyShareClass($this->companyEntity, $registerShareClassId);

        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect(CURegisterMemberControler::PAGE_LIST, array('company_id' => $this->companyEntity->getId()));
        }
    }

    /********************************** add **********************************/

    public function renderAdd()
    {
        $shareClassEvent = new ShareClassEvent($this->shareClass);

        $form = new ShareClassEventForm('share_class_event_create');
        $form->startup($this, $shareClassEvent, $this->registerShareClassEventService);
        $this->template->form = $form;

        $this->addBreadCrumbs(array(
            $this->getCompanyBreadCrumb(),
            $this->getListBreadCrumb(),
            $this->getViewBreadCrumb(),
            $this->node->getLngTitle(),
        ));
    }

    /********************************** IShareClassEventFormDelegate **********************************/

    public function shareClassEventFormCreated(ShareClassEvent $shareClassEvent)
    {
        $this->flashMessage('Share class event has been created');
        $this->redirect(CURegisterMemberControler::PAGE_VIEW, array(
            'company_id' => $this->companyEntity->getId(),
            'registerMemberId' => $shareClassEvent->getShareClass()->getMember()->getRegisterMemberId()
        ));
    }

    public function shareClassEventFormFailed(Exception $e)
    {
        $this->flashMessage($e->getMessage(), 'error');
        $this->redirect();
    }


    /**
     * @return array
     */
    private function getCompanyBreadCrumb()
    {
        return array('node_id' => CUSummaryControler::SUMMARY_PAGE, 'params' => array('company_id' => $this->companyEntity->getId()));
    }

    /**
     * @return array
     */
    private function getListBreadCrumb()
    {
        return array('node_id' => CURegisterMemberControler::PAGE_LIST, 'params' => array('company_id' => $this->companyEntity->getId()));
    }

    /**
     * @return array
     */
    private function getViewBreadCrumb()
    {
        return array('node_id' => CURegisterMemberControler::PAGE_VIEW, 'params' => array('company_id' => $this->companyEntity->getId(), 'registerMemberId' => $this->shareClass->getMember()->getId()));
    }
}
