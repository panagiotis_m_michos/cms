<?php

use Exceptions\Business\ItemNotFound;
use Forms\Front\EditCardForm;
use SagePay\Token\Exception\AuthenticationRequired;
use SagePay\Token\Exception\ConnectionError;
use SagePay\Token\Exception\DataMismatch;
use SagePay\Token\Exception\FailedResponse;
use SagePay\Token\Exception\Transaction;
use SagePay\Token\Exception\ValidationError;
use Services\Payment\SageService;
use Services\Payment\TokenService;
use Services\TransactionService;
use ServiceSettingsModule\Config\DiLocator as ServiceSettingsDiLocator;
use ServiceSettingsModule\Services\ServiceSettingsService;

class ManagePaymentControler extends LoggedControler
{
    /* pages */

    const MANAGE_PAYMENT_PAGE = 1455;
    const DELETE_CARD_PAGE = 1456;
    const MANAGE_PAYMENT_AUTHENTICATION_FORM = 1607;
    const MANAGE_PAYMENT_AUTHENTICATION_PAGE = 1608;

    const AMOUNT = 0.01;

    /**
     * @param array
     */
    public $possibleActions = [
        self::MANAGE_PAYMENT_PAGE => 'default',
        self::MANAGE_PAYMENT_AUTHENTICATION_FORM => 'authenticationForm',
        self::MANAGE_PAYMENT_AUTHENTICATION_PAGE => 'authorization',
        self::DELETE_CARD_PAGE => 'deleteCard',
    ];

    /**
     * @var TokenService
     */
    private $tokenService;

    /**
     * @var SageService
     */
    private $sageService;

    /**
     * @var TransactionService
     */
    private $transactionService;

    /**
     * @var ServiceSettingsService
     */
    private $serviceSettingsService;

    public function startup()
    {
        parent::startup();
        $this->tokenService = $this->getService(DiLocator::SERVICE_TOKEN);
        $this->sageService = $this->getService(DiLocator::SERVICE_SAGE);
        $this->transactionService = $this->getService(DiLocator::SERVICE_TRANSACTION);
        $this->serviceSettingsService = $this->getService(ServiceSettingsDiLocator::SERVICES_SERVICE_SETTINGS_SERVICE);
    }

    public function renderDefault()
    {
        try {
            $token = $this->tokenService->getTokenByCustomer($this->customerEntity);
            if (!$token) {
                $form = new EditCardForm('add_card');
                $form->startup($this->customerEntity);
                $form->setAction($this->router->link(self::MANAGE_PAYMENT_PAGE) . '#authFormBlock');
                $this->template->form = $form;
                if ($form->isOk2Save()) {
                    $data = $form->getValues();
                    $data['email'] = $this->customerEntity->getEmail();
                    $paymentConfig = FApplication::$config['payment'];
                    try {
                        $response = $this->sageService->makePayment(
                            $this->sageService->mapRegistration($data, $paymentConfig['currency']),
                            $this->sageService->mapAuthenticateDetails($data, self::AMOUNT, $paymentConfig['currency'], $paymentConfig['description'], TRUE)
                        );
                        $flashMessage = $this->getCardAddedFlashMessage();
                        $token = $this->tokenService->saveTokenFromResponse($this->customerEntity, $response);
                        $this->serviceSettingsService->reenableAutoRenewalByCustomer($this->customerEntity, $token);
                        $response->setToken($token);
                        $this->transactionService->saveTransactionFromResponse($this->customerEntity, $response);
                        $this->flashMessage($flashMessage, 'info', FALSE);
                        $this->redirect();
                    } catch (AuthenticationRequired $e) {
                        $this->template->showAuthenticationForm = TRUE;
                    }
                }
            } else {
                $this->template->token = $token;
            }
        } catch (Exception $e) {
            $this->flashMessage($this->getError($e), 'error');
            $this->readFlashMessages();
            $this->template->error = TRUE;
        }
    }

    public function handleDeleteCard()
    {
        try {
            if (empty($this->post['tokenId'])) {
                $this->flashMessage('Token id is required', 'error');
                $this->redirect(self::MANAGE_PAYMENT_PAGE);
            }
            $token = $this->tokenService->getCustomerToken($this->customerEntity, $this->post['tokenId']);
            try {
                $this->sageService->deleteToken($token->getIdentifier());
            } catch (FailedResponse $e) {
                $this->logger->info(
                    sprintf(
                        'Customer %s token %s does not exist. Message: %s',
                        $this->customerEntity->getId(), $token->getId(), (string) $e
                    )
                );
            }
            $this->tokenService->removeToken($token, $this->customerEntity->getEmail());
            $this->flashMessage('Your card details have been deleted.');
        } catch (Exception $e) {
            $this->flashMessage($this->getError($e), 'error');
        }
        $this->redirect(self::MANAGE_PAYMENT_PAGE);
    }

    public function renderAuthenticationForm()
    {
        try {
            $url = $this->router->secureLink(self::MANAGE_PAYMENT_AUTHENTICATION_PAGE, 'sagepay-3d=1');
            $this->template->form = $this->sageService->getAuthenticationForm($url);
        } catch (ValidationError $e) {
            $this->template->form = $e->getMessage();
        }
    }

    public function renderAuthorization()
    {
        try {
            $response = $this->sageService->authorizePayment($_POST, self::AMOUNT);
            $flashMessage = $this->getCardAddedFlashMessage();
            $token = $this->tokenService->saveTokenFromResponse($this->customerEntity, $response);
            $this->serviceSettingsService->reenableAutoRenewalByCustomer($this->customerEntity, $token);
            $response->setToken($token);
            $this->transactionService->saveTransactionFromResponse($this->customerEntity, $response);
            $this->template->success = TRUE;
            $this->flashMessage($flashMessage, 'info', FALSE);
        } catch (Exception $e) {
            $this->template->error = $this->getError($e);
        }
    }

    /**
     * @param Exception $e
     * @return string
     * @throws Exception
     */
    public function getError(Exception $e)
    {
        if ($e instanceof FailedResponse || $e instanceof ValidationError || $e instanceof DataMismatch || $e instanceof ItemNotFound) {
            return $e->getMessage();
        } elseif ($e instanceof ConnectionError) {
            $this->logger->error((string) $e);
            return 'Unable to communicate with the payment provider. Please try again later';
        } elseif ($e instanceof Transaction) {
            return 'Please resubmit your details.';
        }
        throw $e;
    }

    /**
     * @return string
     */
    private function getCardAddedFlashMessage()
    {
        $flashMessage = 'Your card details have been updated.';
        if ($this->serviceSettingsService->hasCustomerServicesWithInvalidRenewalToken($this->customerEntity)) {
            $flashMessage .=
                "<ul>
                    <li>We've turned your auto renew settings back on (but only for your services where auto renew was previously on).</li>
                    <li>You can confirm your settings at <a href=\"{$this->router->link(MyServicesControler::PAGE_SERVICES)}\">My Services</a>.</li>
                    <li>Please note that overdue services must be renewed manually.</li>
                </ul>";
        }

        return $flashMessage;
    }
}
