<?php

use FrontModule\Forms\CompaniesCustomer\FilterForm;
use Nette\Utils\Json;
use Search\Company;
use Search\CompanyFinder;
use Search\CompanyIterator;
use Search\CompanySearchQuery;
use Services\CompanyService;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Utils\Date;

class CompaniesCustomerControler extends LoggedControler
{
    const COMPANIES_PAGE = 95;
    const PAGE_FILTER = 1514;
    const PAGE_CSV = 1515;
    const PAGE_SYNC = 1516;

    const COMPANIES_PER_PAGE = 200;

    /**
     * @var CompanyFinder
     */
     private $companyFinder;

    /**
     * @var array
     */
    public $possibleActions = [
        self::COMPANIES_PAGE => 'default',
        self::PAGE_FILTER => 'filtered',
        self::PAGE_CSV => 'exportToCsv',
        self::PAGE_SYNC => 'syncAllCompanies',
    ];

    /**
     * @var CompanyCustomerService
     */
    private $companyCustomerService;

    public function startUp()
    {
        parent::startup();
        $this->companyFinder = $this->getService(DiLocator::SERVICE_COMPANY_SEARCH);
        $this->companyCustomerService = $this->getService(DiLocator::SERVICE_COMPANY_CUSTOMER);
    }

    public function renderDefault()
    {
        $filter = CompanySearchQuery::fromDefault(0, self::COMPANIES_PER_PAGE);
        $filter->setSortingFieldIndex(0);
        $filter->setSortingDirection('asc');
        $form = $this->createForm(new FilterForm(), $filter, ['csrf_protection' => FALSE]);
        $this->template->form = $form->createView();
        $this->template->companiesCount = $companiesCount = $this->companyFinder->countCustomerCompanies($this->customerEntity, $filter);
        $companies = $this->companyFinder->findCustomerCompanies($this->customerEntity, $filter);

        $this->template->perPage = self::COMPANIES_PER_PAGE;
        $this->template->companies = iterator_to_array($companies);

        $this->template->notIncorporated = $this->companyCustomerService->getNotIncorporateCompanies($this->customer);

        if ($form->get('exportCsv')->isClicked()) {
            $this->exportCompanies($filter);
        }
    }

    public function renderFiltered()
    {
        $filter = CompanySearchQuery::fromParams(Request::createFromGlobals(), self::COMPANIES_PER_PAGE);
        $form = $this->createForm(new FilterForm(), $filter, ['csrf_protection' => FALSE]);
        if (!$form->isValid()) {
            $response = new JsonResponse(
                [
                    'draw' => (int) $filter->getDraw(),
                    'error' => $this->formatFormErrors($form)
                ],
                JsonResponse::HTTP_INTERNAL_SERVER_ERROR
            );
            $response->send();
            exit(1);
        }

        $companiesCount = $this->companyFinder->countCustomerCompanies($this->customerEntity, $filter);
        $companies = $this->companyFinder->findCustomerCompanies($this->customerEntity, $filter);
        $response = new JsonResponse(
            [
                'draw' => (int) $filter->getDraw(),
                'recordsTotal' => (int) $companiesCount,
                'recordsFiltered' => (int) $companiesCount,
                'data' => CompanyIterator::getJsonSerialized($companies)
            ]
        );
        $response->send();
        exit(0);
    }

    public function handleSyncAllCompanies()
    {
        try {
            /** @var CompanyService $companyService */
            $companyService = $this->getService(DiLocator::SERVICE_COMPANY);
            $excludeDeleted = TRUE;
            $companies = $companyService->getCustomerIncorporatedCompanies($this->customerEntity, $excludeDeleted);
            $err = $this->companyCustomerService->syncCompanies($companies);
            //@TODO testing if timeout is causing S388
            //there is no good way to check if it was timeout
            $connection = dibi::getConnection();
            $connection->disconnect();
            $connection->connect();

            if (isset($err) && count($err) > 0) {
                foreach ($err as $error) {
                    $this->flashMessage(sprintf('<strong>%s</strong> %s', $error['companyName'], $error['error']), 'info', FALSE);
                }
                $this->redirect(self::COMPANIES_PAGE);
            } else {
                $this->flashMessage(
                    'All companies have been successfully synchronised. NB. This has updated all due dates but not company officers. To sync these details you must Sync the individual company.'
                );
                $this->redirect('default');
            }
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('default');
        }
    }

    /**
     * @param Form $form
     * @return array
     */
    private function formatFormErrors(Form $form)
    {
        $errors = [];
        foreach ($form->getErrors() as $key => $error) {
            $errors[] = $error->getMessage();
        }
        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->formatFormErrors($child);
            }
        }
        return $errors;
    }

    /**
     * @param CompanySearchQuery $filter
     */
    private function exportCompanies(CompanySearchQuery $filter)
    {
        $filter->setLimit(0);
        $companies = iterator_to_array($this->companyFinder->findCustomerCompanies($this->customerEntity, $filter));

        $arr[] = ['Company Name', 'Company Number', 'Incorporation Date', 'Status', 'Accounts Due', 'Returns Due'];
        foreach ($companies as $company) {
            /** @var Company $company */
            $arr[] = $company->jsonSerialize();
        }

        $filename = sprintf("customerCompaniesList.csv", date('d-m-Y'));
        Array2Csv::output($arr, $filename);
    }
}
