<?php

class DeleteCompanyControler extends CUControler
{
    /* live */
    const DELETE_COMPANY_PAGE = 992;

    /**
     * @var Company
     */
    public $company;

    /**
     * @var Customer
     */
    public $customer;

    /**
     * Check if a company is in pending and if the customer is Wholesale
     * @return void
     */
    public function beforePrepare()
    {
        parent::beforePrepare();
        $request = $this->company->getLastFormSubmission();
        if ($request && ($request->isPending() || $request->isInternalFailure())) {
            $this->flashMessage('Sorry, you cannot delete this company as there is still a PENDING request with Companies House', 'error');
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array("company_id" => "{$this->company->getCompanyId()}"));
        }
    }

    /***************************************** default *****************************************/
    protected function renderDefault()
    {
        $this->template->test = 'test';
        $form = new DeleteCompanyForm($this->node->getId() . '_DeleteCompany');
        $form->startup($this, array($this, 'Form_DeleteCompanyFormSubmitted'));
        $this->template->form = $form;

    }

    /**
     * @param DeleteCompanyForm $form
     */
    public function Form_DeleteCompanyFormSubmitted(DeleteCompanyForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Your company was successfully deleted from the Companies Made Simple system.');
            $this->redirect(CompaniesCustomerControler::COMPANIES_PAGE);
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

}
