<?php

class XmlMenuExportControler extends FControler
{

    protected function handleDefault()
    {
        Debug::disableProfiler();
        if (isset($this->get['footer'])) {
            $this->template->footer = TRUE;
        } elseif (isset($this->get['header'])) {
            $this->template->header = TRUE;
        } else {
            //old content 
            $ids = FNode::getChildsIds(BaseControler::BOTTOM_MENU_FOLDER, FALSE);
            $menu = FNode::getTitleAndLink($ids);

            $x = 0;
            $xml = '<menu></menu>';
            $xml = simplexml_load_string($xml);

            foreach ($menu as $key => $value) {
                $xml->addChild('tab');
                $xml->tab[$x]->addChild('id', $key);
                foreach ($value as $key2 => $val) {
                    $xml->tab[$x]->addChild($key2, $val);
                }
                $x++;
            }
            $this->terminate($xml->asXml());
        }
    }

}
