<?php

use Entities\Register\Psc\CompanyStatement;
use Entities\Register\Psc\CorporatePsc;
use Entities\Register\Psc\LegalPersonPsc;
use Entities\Register\Psc\PersonPsc;
use Entities\Service;
use PeopleWithSignificantControl\Forms\CorporatePscData;
use PeopleWithSignificantControl\Forms\LegalPersonPscData;
use PeopleWithSignificantControl\Forms\PersonPscData;
use PeopleWithSignificantControl\Forms\PscFormFactory;
use PeopleWithSignificantControl\Interfaces\IPscAware;
use PeopleWithSignificantControl\Providers\PscChoicesProvider;
use Services\Register\Psc\EntryService;

class CURegisterPscControler extends CUControler
{
    const PAGE_LIST = 1710;
    //const PAGE_VIEW = 170;
    const PAGE_ADD_PERSON = 1711;
    const PAGE_ADD_CORPORATE = 1712;
    const PAGE_ADD_LEGAL_PERSON = 1721;
    const PAGE_EDIT_PERSON = 1713;
    const PAGE_EDIT_CORPORATE = 1714;
    const PAGE_EDIT_LEGAL_PERSON = 1722;
    const PAGE_CESSATE_PSC = 1715;
    const PAGE_WITHDRAW_STATEMENT = 1716;

    /**
     * @var array
     */
    public $possibleActions = array(
        self::PAGE_LIST => 'list',
        //self::PAGE_VIEW => 'view',
        self::PAGE_ADD_PERSON => 'addPerson',
        self::PAGE_ADD_CORPORATE => 'addCorporate',
        self::PAGE_ADD_LEGAL_PERSON => 'addLegalPerson',
        self::PAGE_EDIT_PERSON => 'editPerson',
        self::PAGE_EDIT_CORPORATE => 'editCorporate',
        self::PAGE_EDIT_LEGAL_PERSON => 'editLegalPerson',
        self::PAGE_CESSATE_PSC => 'cessatePsc',
        self::PAGE_WITHDRAW_STATEMENT => 'withdrawStatement',
    );

    //todo: psc - how? musi to byt konkretne kvoli transformerom alebo aj spolocne
    /**
     * @var PersonPsc|CorporatePsc|LegalPersonPsc|CompanyStatement
     */
    private $psc;

    /**
     * @var EntryService
     */
    private $entryService;

    /**
     * @var PscFormFactory
     */
    private $pscFormFactory;

    /**
     * @var PscChoicesProvider
     */
    private $pscChoicesProvider;

    /**
     * @var string
     */
    private $companyType;

    public function startup()
    {
        parent::startup();
        $this->entryService = $this->getService('services.register.psc.entry_service');
        $this->pscFormFactory = $this->getService('people_with_significant_control_module.forms.form_factory');
        $this->pscChoicesProvider = $this->getService('people_with_significant_control_module.providers.psc_choices_provider');
    }

    public function beforePrepare()
    {
        parent::beforePrepare();

        if (!$this->companyEntity->hasActiveServiceOfType(Service::TYPE_PSC_ONLINE_REGISTER)) {
            $message = "Sorry, you need to purchase the service to access the online register. <a href='{$this->newRouter->generate('product_module.psc_services')}'>More info</a>";
            $this->flashMessage($message, 'error', FALSE);
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, ['company_id' => $this->companyEntity->getId()]);
        }

        $this->companyType = $this->company->getType();
    }

    /********************************** list **********************************/

    public function renderList()
    {
        $noPscsForm = new FForm('noPscs');
        $noPscsForm->addCheckbox('noPscReason', $this->pscChoicesProvider->getNoPscReason(), 1);
        $noPscsForm->addSubmit('save', 'Save')->class('btn btn-primary');
        $noPscsForm->onValid = [$this, 'Form_noPscsFormSubmitted'];
        //todo: psc - set according to last entry
        //$noPscsForm->setInitValues(['noPscReason' => $this->companyEntity->hasNoPscReason()]);
        $noPscsForm->start();

        $this->template->noPscsForm = $noPscsForm;
        $this->template->entries = $this->entryService->getEntriesByCompany($this->companyEntity);
        $this->template->canAddCompanyStatement = $this->entryService->canAddCompanyStatement($this->companyEntity);

        $this->addBreadCrumbs(
            [
                $this->getCompanyBreadCrumb(),
                $this->node->getLngTitle(),
            ]
        );
    }

    /**
     * @param FForm $form
     */
    public function Form_noPscsFormSubmitted(FForm $form)
    {
        $noPscReason = $form->getValue('noPscReason');

        if ($noPscReason) {
            $entry = new CompanyStatement($this->companyEntity, IPscAware::NO_INDIVIDUAL_OR_ENTITY_WITH_SIGNFICANT_CONTROL);
            $this->entryService->addEntry($entry);
            $this->flashMessage('Company statement has been saved');
        }

        $form->clean();
        $this->redirect();
    }

    /********************************** view **********************************/

    //public function renderView()
    //{
    //    try {
    //        $id = $this->getParameter('registerMemberId');
    //        $member = $this->getPsc($id);
    //
    //        $deleteShareClassForms = array();
    //        foreach ($member->getShareClasses() as $shareClass) {
    //            $form = new DeleteForm($shareClass->getId() . 'share_class_event_delete');
    //            $form->startup($this, $shareClass, $this->registerShareClassService);
    //            $deleteShareClassForms[$shareClass->getId()] = $form;
    //        }
    //
    //        $this->addBreadCrumbs(array(
    //            $this->getCompanyBreadCrumb(),
    //            $this->getListBreadCrumb(),
    //            $this->node->getLngTitle(),
    //        ));
    //
    //        $this->template->member = $member;
    //        $this->template->deleteShareClassForms = $deleteShareClassForms;
    //
    //    } catch (Exception $e) {
    //        $this->processException($e);
    //    }
    //}
    
    /********************************** add **********************************/

    public function renderAddPerson()
    {
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'person');
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
        $this->template->jsPrefillAdresses = $prefillAddress['js'];

        $form = $this->pscFormFactory->getPersonAddForm(
            $this->company,
            function (PersonPscData $data, FForm $form) {
                $psc = new PersonPsc($this->companyEntity);
                PersonPscDataTransformer::mergeRegister($psc, $data);

                $this->entryService->addEntry($psc);
                $form->clean();
                $this->flashMessage("Person PSC has been added");
                $this->redirect(self::PAGE_LIST, ['company_id' => $this->companyEntity->getId()]);
            },
            $prefillOfficers,
            $prefillAddress
        );

        $this->template->form = $form;
        $this->template->natureOfControls = $this->pscChoicesProvider->getNatureOfControlsTemplateStructure(PscChoicesProvider::PERSON, $this->companyType);
        $this->template->natureOfControlsDescription = $this->pscChoicesProvider->getNatureOfControlsTemplateDescription(PscChoicesProvider::PERSON);
        $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());

        $this->addBreadCrumbs(
            [
                $this->getCompanyBreadCrumb(),
                $this->getListBreadCrumb(),
                $this->node->getLngTitle(),
            ]
        );
    }

    public function renderAddCorporate()
    {
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'corporate');
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
        $this->template->jsPrefillAdresses = $prefillAddress['js'];

        $form = $this->pscFormFactory->getCorporateAddForm(
            $this->company,
            function (CorporatePscData $data, FForm $form) {
                $psc = new CorporatePsc($this->companyEntity);
                CorporatePscDataTransformer::mergeRegister($psc, $data);

                $this->entryService->addEntry($psc);
                $form->clean();
                $this->flashMessage("Corporate PSC has been added");
                $this->redirect(self::PAGE_LIST, ['company_id' => $this->companyEntity->getId()]);
            },
            $prefillOfficers,
            $prefillAddress
        );

        $this->template->form = $form;
        $this->template->natureOfControls = $this->pscChoicesProvider->getNatureOfControlsTemplateStructure(PscChoicesProvider::CORPORATE, $this->companyType);
        $this->template->natureOfControlsDescription = $this->pscChoicesProvider->getNatureOfControlsTemplateDescription(PscChoicesProvider::CORPORATE);
        $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());

        $this->addBreadCrumbs(
            [
                $this->getCompanyBreadCrumb(),
                $this->getListBreadCrumb(),
                $this->node->getLngTitle(),
            ]
        );
    }

    public function renderAddLegalperson()
    {
        //$prefillOfficers = self::getPrefillOfficers($this->company, $type = 'corporate');
        //$prefillAddress = self::getPrefillAdresses($this->company);
        //$this->template->jsPrefillOfficers = $prefillOfficers['js'];
        //$this->template->jsPrefillAdresses = $prefillAddress['js'];

        $form = $this->pscFormFactory->getLegalPersonAddForm(
            $this->company,
            function (LegalPersonPscData $data, FForm $form) {
                $psc = new LegalPersonPsc($this->companyEntity);
                LegalPersonPscDataTransformer::mergeRegister($psc, $data);

                $this->entryService->addEntry($psc);
                $form->clean();
                $this->flashMessage("Legal Person PSC has been added");
                $this->redirect(self::PAGE_LIST, ['company_id' => $this->companyEntity->getId()]);
            },
            [],
            []
        );

        $this->template->form = $form;
        $this->template->natureOfControls = $this->pscChoicesProvider->getNatureOfControlsTemplateStructure(PscChoicesProvider::LEGAL_PERSON, $this->companyType);
        $this->template->natureOfControlsDescription = $this->pscChoicesProvider->getNatureOfControlsTemplateDescription(PscChoicesProvider::LEGAL_PERSON);
        $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());

        $this->addBreadCrumbs(
            [
                $this->getCompanyBreadCrumb(),
                $this->getListBreadCrumb(),
                $this->node->getLngTitle(),
            ]
        );
    }

    /********************************** edit **********************************/

    public function renderEditPerson()
    {
        try {
            $id = $this->getParameter('pscId');
            $this->psc = $this->entryService->getCompanyEntryById($this->companyEntity, $id);

            $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'person');
            $prefillAddress = self::getPrefillAdresses($this->company);
            $this->template->jsPrefillOfficers = $prefillOfficers['js'];
            $this->template->jsPrefillAdresses = $prefillAddress['js'];
    
            $personPscData = PersonPscDataTransformer::fromRegister($this->psc);
            $form = $this->pscFormFactory->getPersonEditForm(
                $this->company,
                $personPscData,
                function (PersonPscData $data, FForm $form) {
                    PersonPscDataTransformer::mergeRegister($this->psc, $data);
                    $this->entryService->updateEntry($this->psc);

                    $form->clean();
                    $this->flashMessage("Person PSC has been updated");
                    $this->redirect(self::PAGE_LIST, 'company_id='.$this->company->getCompanyId());
                },
                $prefillOfficers,
                $prefillAddress
            );
    
            $this->template->form = $form;
            $this->template->natureOfControls = $this->pscChoicesProvider->getNatureOfControlsTemplateStructure(PscChoicesProvider::PERSON, $this->companyType);
            $this->template->natureOfControlsDescription = $this->pscChoicesProvider->getNatureOfControlsTemplateDescription(PscChoicesProvider::PERSON);
            $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());

            $this->addBreadCrumbs(
                [
                    $this->getCompanyBreadCrumb(),
                    $this->getListBreadCrumb(),
                    $this->node->getLngTitle(),
                ]
            );
        } catch (Exception $e) {
            $this->processException($e);
        }
    }

    public function renderEditCorporate()
    {
        try {
            $id = $this->getParameter('pscId');
            $this->psc = $this->entryService->getCompanyEntryById($this->companyEntity, $id);

            $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'corporate');
            $prefillAddress = self::getPrefillAdresses($this->company);
            $this->template->jsPrefillOfficers = $prefillOfficers['js'];
            $this->template->jsPrefillAdresses = $prefillAddress['js'];

            $corporatePscData = CorporatePscDataTransformer::fromRegister($this->psc);
            $form = $this->pscFormFactory->getCorporateEditForm(
                $this->company,
                $corporatePscData,
                function (CorporatePscData $data, FForm $form) {
                    CorporatePscDataTransformer::mergeRegister($this->psc, $data);
                    $this->entryService->updateEntry($this->psc);

                    $form->clean();
                    $this->flashMessage("Corporate PSC has been updated");
                    $this->redirect(self::PAGE_LIST, 'company_id='.$this->company->getCompanyId());
                },
                $prefillOfficers,
                $prefillAddress
            );

            $this->template->form = $form;
            $this->template->natureOfControls = $this->pscChoicesProvider->getNatureOfControlsTemplateStructure(PscChoicesProvider::CORPORATE, $this->companyType);
            $this->template->natureOfControlsDescription = $this->pscChoicesProvider->getNatureOfControlsTemplateDescription(PscChoicesProvider::CORPORATE);
            $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());

            $this->addBreadCrumbs(
                [
                    $this->getCompanyBreadCrumb(),
                    $this->getListBreadCrumb(),
                    $this->node->getLngTitle(),
                ]
            );
        } catch (Exception $e) {
            $this->processException($e);
        }
    }

    public function renderEditLegalPerson()
    {
        try {
            $id = $this->getParameter('pscId');
            $this->psc = $this->entryService->getCompanyEntryById($this->companyEntity, $id);

            //$prefillOfficers = self::getPrefillOfficers($this->company, $type = 'person');
            //$prefillAddress = self::getPrefillAdresses($this->company);
            //$this->template->jsPrefillOfficers = $prefillOfficers['js'];
            //$this->template->jsPrefillAdresses = $prefillAddress['js'];

            $legalPersonPscData = LegalPersonPscDataTransformer::fromRegister($this->psc);
            $form = $this->pscFormFactory->getLegalPersonEditForm(
                $this->company,
                $legalPersonPscData,
                function (LegalPersonPscData $data, FForm $form) {
                    LegalPersonPscDataTransformer::mergeRegister($this->psc, $data);
                    $this->entryService->updateEntry($this->psc);

                    $form->clean();
                    $this->flashMessage("Legal Person PSC has been updated");
                    $this->redirect(self::PAGE_LIST, 'company_id='.$this->company->getCompanyId());
                },
                [],
                []
            );

            $this->template->form = $form;
            $this->template->natureOfControls = $this->pscChoicesProvider->getNatureOfControlsTemplateStructure(PscChoicesProvider::LEGAL_PERSON, $this->companyType);
            $this->template->natureOfControlsDescription = $this->pscChoicesProvider->getNatureOfControlsTemplateDescription(PscChoicesProvider::LEGAL_PERSON);
            $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());

            $this->addBreadCrumbs(
                [
                    $this->getCompanyBreadCrumb(),
                    $this->getListBreadCrumb(),
                    $this->node->getLngTitle(),
                ]
            );
        } catch (Exception $e) {
            $this->processException($e);
        }
    }

    /********************************** cessate **********************************/

    public function renderCessatePsc()
    {
        $this->psc = $this->entryService->getCompanyEntryById($this->companyEntity, $this->getParameter('pscId'));

        $form = new CUCessateForm($this->node->getId() . '_cessate');
        $form->startup([$this, 'Form_CessateFormSubmitted']);

        $this->template->form = $form;

        $this->addBreadCrumbs(
            [
                $this->getCompanyBreadCrumb(),
                $this->getListBreadCrumb(),
                $this->node->getLngTitle(),
            ]
        );
    }

    /**
     * @param CUCessateForm $form
     */
    public function Form_CessateFormSubmitted(CUCessateForm $form)
    {
        try {
            $date = new DateTime($form->getValue('date'));
            $this->entryService->cessatePsc($this->psc, $date);

            $form->clean();
            $this->flashMessage('PSC has been cessated.');
            $this->reloadOpener();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect(self::PAGE_LIST, ['company_id' => $this->companyEntity->getId()]);
        }
    }

    /********************************** withdraw **********************************/

    public function renderWithdrawStatement()
    {
        $this->psc = $this->entryService->getCompanyEntryById($this->companyEntity, $this->getParameter('statementId'));

        $form = new CUWithdrawForm($this->node->getId() . '_withdraw');
        $form->startup([$this, 'Form_WithdrawFormSubmitted']);

        $this->template->form = $form;

        $this->addBreadCrumbs(
            [
                $this->getCompanyBreadCrumb(),
                $this->getListBreadCrumb(),
                $this->node->getLngTitle(),
            ]
        );
    }

    /**
     * @param CUWithdrawForm $form
     */
    public function Form_WithdrawFormSubmitted(CUWithdrawForm $form)
    {
        try {
            $date = new DateTime($form->getValue('date'));
            $this->entryService->withdrawStatement($this->psc, $date);

            $form->clean();
            $this->flashMessage('Company statement has been withdrawn.');
            $this->reloadOpener();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect(self::PAGE_LIST, ['company_id' => $this->companyEntity->getId()]);
        }
    }

    /********************************** Private methods **********************************/

    /**
     * @param Exception $e
     */
    private function processException(Exception $e)
    {
        if ($e instanceof InvalidArgumentException || $e instanceof EntityNotFound) {
            $message = $e->getMessage();
        } else {
            Debug::log($e);
            $message = 'An error occurred';
        }
        $this->flashMessage($message, 'error');
        $this->redirect(self::PAGE_LIST, "company_id={$this->companyEntity->id}");
    }

    /**
     * @return array
     */
    private function getCompanyBreadCrumb()
    {
        return [
            'title' => $this->companyEntity->getCompanyName(),
            'url' => $this->router->link(CUSummaryControler::SUMMARY_PAGE, ['company_id' => $this->companyEntity->getId()]),
        ];
    }

    /**
     * @return array
     */
    private function getListBreadCrumb()
    {
        return [
            'node_id' => self::PAGE_LIST,
            'params' => [
                'company_id' => $this->companyEntity->getId(),
            ],
        ];
    }
}
