<?php

use CHFiling\Exceptions\AuthenticationCodeExpiredException;
use CHFiling\Exceptions\UnknownCHException;
use DusanKasan\Knapsack\Collection;
use ErrorModule\Ext\DebugExt;
use PeopleWithSignificantControl\Forms\CorporatePscData;
use PeopleWithSignificantControl\Forms\LegalPersonPscData;
use PeopleWithSignificantControl\Forms\PersonPscData;
use PeopleWithSignificantControl\Forms\PscFormFactory;
use PeopleWithSignificantControl\Interfaces\IPscAware;
use PeopleWithSignificantControl\Providers\PscChoicesProvider;
use PeopleWithSignificantControl\Views\AnnualReturnSummaryView;
use Psr\Log\LoggerInterface;
use Services\SubmissionService;
use Utils\Date;

class AnnualReturnControler extends CUControler
{
    const COMPANY_DETAILS_PAGE = 652;
    const CAPITAL_PAGE = 659;
    const SHAREHOLDINGS_PAGE = 653;
    const ADD_SHAREHOLDING_PAGE = 655;
    const EDIT_SHAREHOLDING_PAGE = 657;
    const ADD_TRANSFER_PAGE = 656;
    const SUMMARY_PAGE = 654;

    const PSCS_PAGE = 1695;
    const PSC_PERSON_ADD = 1696;
    const PSC_PERSON_EDIT = 1697;
    const PSC_PERSON_DELETE = 1699;
    const PSC_CORPORATE_ADD = 1700;
    const PSC_CORPORATE_EDIT = 1701;
    const PSC_CORPORATE_DELETE = 1702;
    const PSC_LEGAL_PERSON_ADD = 1718;
    const PSC_LEGAL_PERSON_EDIT = 1719;
    const PSC_LEGAL_PERSON_DELETE = 1720;

    /**
     * @var array
     */
    public $possibleActions = [
        self::COMPANY_DETAILS_PAGE => 'companyDetails',
        self::CAPITAL_PAGE => 'capital',
        self::SHAREHOLDINGS_PAGE => 'shareholdings',
        self::ADD_SHAREHOLDING_PAGE => 'addShareholding',
        self::EDIT_SHAREHOLDING_PAGE => 'editShareholding',
        self::ADD_TRANSFER_PAGE => 'addTransfer',
        self::SUMMARY_PAGE => 'summary',
        self::PSCS_PAGE => 'pscs',
        self::PSC_PERSON_ADD => 'addPersonPsc',
        self::PSC_PERSON_EDIT => 'editPersonPsc',
        self::PSC_CORPORATE_ADD => 'addCorporatePsc',
        self::PSC_CORPORATE_EDIT => 'editCorporatePsc',
        self::PSC_LEGAL_PERSON_ADD => 'addLegalPersonPsc',
        self::PSC_LEGAL_PERSON_EDIT => 'editLegalPersonPsc',
    ];

    /**
     * @var CHAnnualReturn
     */
    public $chAnnualReturn;

    /**
     * @var Shareholding
     */
    public $shareholding;

    /**
     * @var array
     */
    public $capitals;

    /**
     * @var AnnualReturnServiceModel
     */
    public $arsm;

    /**
     * @var LoggerInterface
     */
    public $logger;

    /**
     * @var PscChoicesProvider
     */
    private $pscChoicesProvider;

    /**
     * @var PscFormFactory
     */
    private $pscFormFactory;

    /**
     * @var string
     */
    private $companyType;

    public function startup()
    {
        parent::startup();
        $this->logger = $this->getService(DebugExt::LOGGER);
        $this->pscChoicesProvider = $this->getService('people_with_significant_control_module.providers.psc_choices_provider');
        $this->pscFormFactory = $this->getService('people_with_significant_control_module.forms.form_factory');
    }


    public function beforePrepare()
    {
        if ($this->action == $this->possibleActions[self::COMPANY_DETAILS_PAGE]) {
            $this->authCodeRequired = TRUE;
        }

        parent::beforePrepare();
        $this->companyType = $this->company->getType();

        // check if has annual return
        if ($this->company->hasAnnualReturn() === FALSE) {
            $this->flashMessage(
                "You need to purchase the Annual Return (DIY) or Annual Return Service (If you would like us to prepare and file it for you). <a href=\"{$this->router->link(672)}\">Please click here to purchase</a>
                </br></br>If your formation package includes an annual return service, we shall prepare this for you roughly 1-3 weeks before the due date. You shall receive an email once prepared with further instructions.",
                'error',
                FALSE
            );
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
        }



        // get annual return
        try {
            $this->chAnnualReturn = $this->company->getAnnualReturn(FALSE);
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
        }



        // redirect to summary
        if ($this->action !== 'summary') {

            // if customer bought service - redirect to summary page
            $annualReturnId = $this->company->getAnnualReturnId();
            if ($annualReturnId == AnnualReturn::ANNUAL_RETURN_SERVICE_PRODUCT) {
                $this->redirect(self::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
            }

            // if it's pending
            if ($this->chAnnualReturn->isSubmissionPending()) {
                $this->redirect(self::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
            }
        }


        // get shareholding
        if (isset($this->get['shareholdingId'])) {
            try {
                $this->shareholding = $this->chAnnualReturn->getShareholding($this->get['shareholdingId']);
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
                $this->redirect(NULL, 'shareholdingId=');
            }
        }
    }

    /*     * **************************** company details ******************************** */

    public function handleCompanyDetails()
    {
        if (!isset($this->get['auth'])) {
            try {
                $this->chAnnualReturn->checkAuthenticationCode();
                $this->redirect(NULL, 'auth=1');
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
                $this->redirect(CUSummaryControler::SUMMARY_PAGE, ["company_id" => "{$this->company->getCompanyId()}"]);
            }
            //;
        }
        if (isset($this->get['sync'])) {
            try {
                FForm::cleanForm('companyDetails');
                $this->chAnnualReturn->sync();
                $this->flashMessage(
                    'Your company data has now been synced with the current information held at Companies house'
                );
            } catch (Exception $e) {
                $this->handleException($e);
            }
            $this->redirect(NULL, 'sync=');
        }

        /** @var SubmissionService $submissionService */
        $submissionService = $this->getService(DiLocator::SERVICE_SUBMISSION);
        $formSubmissionEntity = $submissionService->companyHasUnsyncedAnualReturn($this->companyEntity);

        if (!empty($formSubmissionEntity)) {
            $formSubmission = $submissionService->getOldFormSubmission($this->companyEntity, $formSubmissionEntity->getId());
            $ar = $formSubmission->getForm();
            $type = $ar->getCompanyCategory();
            if (empty($type)) {
                try {
                    $this->chAnnualReturn->sync();
                } catch (Exception $e) {
                    $this->handleException($e);
                }
            }
        }
    }

    /**
     * @param Exception $e
     */
    private function handleException(Exception $e)
    {
        if ($e instanceof AuthenticationCodeExpiredException) {
            $this->flashMessage("{$e->getMessage()}. Please try a new authentication code or contact our Customer Service for more information.", 'error');
        } elseif ($e instanceof UnknownCHException) {
            $this->flashMessage('We are currently experiencing technical difficulties contacting Companies House. Please try again in a few minutes.', 'error');
        } else {
            $this->flashMessage($e->getMessage(), 'error');
        }
    }

    public function renderCompanyDetails()
    {
        $officers = Collection::from($this->chAnnualReturn->getNotSyncedOfficers())
            ->reject(
                function (array $officerData) {
                    return $officerData['type'] == '' || $officerData['type'] == 'PSC';
                }
            )
            ->toArray();

        $data = AnnualReturnModel::getCompanyDetailsData($this->chAnnualReturn);
        $this->template->form = AnnualReturnModel::getCompomentCompanyDetailsForm($this, $officers, $data);
        $this->template->data = $data;
        $this->template->officers = $officers;
        $this->template->companyCategory = $this->chAnnualReturn->getCompanyCategory();
    }

    /**
     * @param FForm $form
     */
    public function Form_companyDetailsFormSubmitted(FForm $form)
    {
        try {
            $data = $form->getValues();
            //change format from 01-01-2001 to 2001-01-01
            $data['returnDate'] = !empty($data['returnDate']) ? Date::changeFormat($data['returnDate'], 'd-m-Y', 'Y-m-d') : NULL;
            AnnualReturnModel::saveCompanyDetails($this->chAnnualReturn, $data);
            $form->clean();
            $this->flashMessage('Company details have been saved');
            $return = AnnualReturnModel::getSummaryDetails($this->chAnnualReturn, $this->company);
            if ($return['company_category'] == 'BYGUAR' || $return['company_category'] == 'LLP') {
                $this->redirect(self::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
            } else {
                $this->redirect(self::CAPITAL_PAGE, "company_id={$this->company->getCompanyId()}");
            }
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    /*     * **************************** capital ******************************** */

    public function renderCapital()
    {
        $return = AnnualReturnModel::getSummaryDetails($this->chAnnualReturn, $this->company);
        if ($return['company_category'] == 'BYGUAR' || $return['company_category'] == 'LLP') {
            $this->redirect(self::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
        }

        $this->capitals = $this->chAnnualReturn->getStatementOfCapital();
        $form = AnnualReturnModel::getComponentCapitalForm($this, $this->capitals);
        $this->template->form = $form;
        $this->template->companyCategory = $this->chAnnualReturn->getCompanyCategory();
    }

    public function Form_capitalSubmitted(FForm $form)
    {
        try {
            $data = $form->getValues();
            AnnualReturnModel::saveCapital($this->chAnnualReturn, $this->capitals, $data);
            $form->clean();
            $this->flashMessage('Capital has been saved');
            $this->redirect(self::SHAREHOLDINGS_PAGE, "company_id={$this->company->getCompanyId()}");
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    /*     * **************************** shareholdings ******************************** */

    public function handleShareholdings()
    {
        $return = AnnualReturnModel::getSummaryDetails($this->chAnnualReturn, $this->company);
        if ($return['company_category'] == 'BYGUAR' || $return['company_category'] == 'LLP') {
            $this->redirect(self::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
        }
        // remove shareholding
        if (isset($this->get['deleteShareholding'])) {
            try {
                $this->shareholding = $this->chAnnualReturn->getShareholding($this->get['deleteShareholding']);
                AnnualReturnModel::removeShareholding($this->shareholding);
                $this->flashMessage('Shareholding has been deleted');
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
            }
            $this->redirect(NULL, 'deleteShareholding=');

            // remove tranfer
        } elseif (isset($this->get['deleteTransfer'])) {
            try {
                AnnualReturnModel::removeTransfer($this->shareholding, $this->get['deleteTransfer']);
                $this->flashMessage('Transfer has been deleted');
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
            }
            $this->redirect(NULL, 'deleteTransfer=');
        }
    }

    public function renderShareholdings()
    {
        $shareholdings = $this->chAnnualReturn->getShareholdings();
        $this->template->shareholdings = AnnualReturnModel::getShareholdingsArray($shareholdings);
        $this->template->form = AnnualReturnModel::getCompomentShareholdingsForm($this);
        $this->template->companyCategory = $this->chAnnualReturn->getCompanyCategory();
    }

    public function Form_shareholdingsSubmitted()
    {
        foreach ($this->chAnnualReturn->getShareholdings() as $shareholding) {
            if (!$shareholding->isComplete()) {
                $this->flashMessage("Shareholdings are not complete, your share class does not match your statement of capital. Please click 'edit' next to the blank shareholding and choose the matching share class from the drop-down.", "error");
                $this->redirect();
            }
        }

        if (Feature::featurePsc() && $this->chAnnualReturn->getMadeUpDate() && new DateTime($this->chAnnualReturn->getMadeUpDate()) >= new DateTime('2016-06-30')) {
            $this->redirect(self::PSCS_PAGE, "company_id={$this->company->getCompanyId()}");
        } else {
            $this->redirect(self::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
        }
    }

    /*     * **************************** add shareholding ******************************** */

    public function renderAddShareholding()
    {
        $return = AnnualReturnModel::getSummaryDetails($this->chAnnualReturn, $this->company);
        if ($return['company_category'] == 'BYGUAR' || $return['company_category'] == 'LLP') {
            $this->redirect(self::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
        }
        $this->shareholding = $this->chAnnualReturn->getNewShareholding();
        $this->template->form = AnnualReturnModel::getCompomentShareholdingForm($this, 'insert');
    }

    public function Form_addShareholdingSubmitted(FForm $form)
    {
        try {
            $data = $form->getValues();
            $data['isRemoved'] = 0;
            AnnualReturnModel::addShareholding($this->shareholding, $data);
            $form->clean();
            $this->flashMessage('Shareholdings have been created');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $this->redirect(self::SHAREHOLDINGS_PAGE, "company_id={$this->company->getCompanyId()}");
    }

    /*     * **************************** edit shareholding ******************************** */

    public function renderEditShareholding()
    {
        $return = AnnualReturnModel::getSummaryDetails($this->chAnnualReturn, $this->company);
        if ($return['company_category'] == 'BYGUAR' || $return['company_category'] == 'LLP') {
            $this->redirect(self::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
        }

        if (!$this->shareholding) {
            $message = "Missing shareholding for company `{$this->company->getCompanyName()}`";

            if ($this->chAnnualReturn->getMadeUpDate()) {
                $message .= " and annual return made up to `{$this->chAnnualReturn->getMadeUpDate()}`";
            }

            $this->logger->notice($message);
            $this->flashMessage($message, 'error');
            $this->redirect(self::COMPANY_DETAILS_PAGE, ['companyId' => $this->companyEntity->getId()]);
        }

        $data = AnnualReturnModel::getShareholdingArray($this->shareholding);
        $this->template->form = AnnualReturnModel::getCompomentShareholdingForm($this, 'edit', $data);
    }

    public function Form_editShareholdingSubmitted(FForm $form)
    {
        try {
            $data = $form->getValues();
            AnnualReturnModel::editShareholding($this->shareholding, $data);
            $form->clean();
            $this->flashMessage('Shareholdings have been updated');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $this->redirect(self::SHAREHOLDINGS_PAGE, "company_id={$this->company->getCompanyId()}");
    }

    /********************************* Pscs **********************************/
    public function handlePscs()
    {
        if (isset($this->get['psc_id']) && isset($this->get['cessate'])) {
            //todo: cessate
            //filter only already existing, no pre-added as they might not be in CH at this point

            $pscs = array_merge($this->chAnnualReturn->getPersonPscs(), $this->chAnnualReturn->getCorporatePscs());
            foreach ($pscs as $psc) {
                /** @var $psc AnnualReturnPersonPsc|AnnualReturnPersonPsc */

                if ($psc->getId() == $this->get['psc_id']) {
                    //cessate
                    $this->flashMessage("PSC was successfully cessated!");
                }
            }

            $this->redirect(self::PSCS_PAGE, "company_id={$this->company->getCompanyId()}");
        }

        if (isset($this->get['psc_id']) && isset($this->get['delete'])) {
            $pscs = array_merge($this->chAnnualReturn->getPersonPscs(), $this->chAnnualReturn->getCorporatePscs());
            foreach ($pscs as $psc) {
                /** @var $psc AnnualReturnPersonPsc|AnnualReturnPersonPsc */

                if ($psc->getId() == $this->get['psc_id'] && !$psc->isAlreadyExistingPsc()) {
                    $psc->remove();
                    $this->flashMessage("PSC was successfully deleted!");
                }
            }

            $this->redirect(self::PSCS_PAGE, "company_id={$this->company->getCompanyId()}");
        }

        if (!$this->chAnnualReturn->hasPreAddedPscs()) {
            //todo: pre add PSCs, how can we know the shares here?

            $this->chAnnualReturn->setPreAddedPscs(TRUE);
            $this->chAnnualReturn->save();
            $this->flashMessage("According to your company information we have selected the People with Significant Control who match what is required by the legislation. Please check the information below and amend if necessary.");
        }
    }

    public function renderPscs()
    {
        if ($this->chAnnualReturn->getCompanyCategory() === 'LLP') {
            $this->redirect(self::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
        }

        $annualReturnSummaryView = new AnnualReturnSummaryView($this->chAnnualReturn);

        $noPscsForm = new FForm('noPscs');
        $noPscsForm->addCheckbox('noPscReason', $this->getService('people_with_significant_control_module.providers.psc_choices_provider')->getNoPscReason(), 1);
        $noPscsForm->addSubmit('save', 'Save')->class('btn btn-primary');
        $noPscsForm->onValid = [$this, 'Form_noPscsFormSubmitted'];
        $noPscsForm->setInitValues(['noPscReason' => $annualReturnSummaryView->hasNoPscReason()]);
        $noPscsForm->start();

        $this->template->noPscsForm = $noPscsForm;
        $this->template->summaryView = $annualReturnSummaryView;
        $this->template->form = AnnualReturnModel::getCompomentPscssForm($this);
    }

    /**
     * @param FForm $form
     */
    public function Form_noPscsFormSubmitted(FForm $form)
    {
        /** @var CHAnnualReturn $annualReturn */
        $annualReturn = $this->chAnnualReturn;
        $annualReturn->setNoPscReason($form->getValue('noPscReason') ? IPscAware::NO_INDIVIDUAL_OR_ENTITY_WITH_SIGNFICANT_CONTROL : NULL);
        $annualReturn->save();

        $form->clean();
        $this->flashMessage('PSC reason has been saved');
        $this->redirect();
    }

    public function Form_pscsSubmitted()
    {
        $this->redirect(self::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
    }

    public function renderAddPersonPsc()
    {
        if ($this->chAnnualReturn->getCompanyCategory() === 'LLP') {
            $this->redirect(self::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
        }

        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'person');
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $this->template->natureOfControls = $this->pscChoicesProvider->getNatureOfControlsTemplateStructure(PscChoicesProvider::PERSON, $this->companyType);
        $this->template->natureOfControlsDescription = $this->pscChoicesProvider->getNatureOfControlsTemplateDescription(PscChoicesProvider::PERSON);
        $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());


        $form = $this->pscFormFactory
            ->getPersonAddForm(
                $this->company,
                function (PersonPscData $personPscData, FForm $form) {
                    $pscPerson = new AnnualReturnPersonPsc($this->chAnnualReturn->getFormSubmissionId());
                    PersonPscDataTransformer::merge($pscPerson, $personPscData);
                    $pscPerson->save();

                    $this->chAnnualReturn->setNoPscReason(NULL);
                    $this->chAnnualReturn->save();

                    $form->clean();
                    $this->redirect(self::PSCS_PAGE, "company_id={$this->company->getCompanyId()}");
                },
                $prefillOfficers,
                $prefillAddress
            );

        $this->template->form = $form;
    }

    public function renderEditPersonPsc()
    {
        if (!isset($this->get['psc_id'])) {
            $this->redirect(self::PSCS_PAGE, "company_id={$this->company->getCompanyId()}");
        }

        if ($this->chAnnualReturn->getCompanyCategory() === 'LLP') {
            $this->redirect(self::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
        }

        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'person');
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $this->template->natureOfControls = $this->pscChoicesProvider->getNatureOfControlsTemplateStructure(PscChoicesProvider::PERSON, $this->companyType);
        $this->template->natureOfControlsDescription = $this->pscChoicesProvider->getNatureOfControlsTemplateDescription(PscChoicesProvider::PERSON);
        $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());

        $personPsc = new AnnualReturnPersonPsc($this->chAnnualReturn->getFormSubmissionId(), $this->get['psc_id']);
        $this->template->form = $this->pscFormFactory
            ->getPersonEditForm(
                $this->company,
                PersonPscDataTransformer::from($personPsc),
                function (PersonPscData $personPscData, FForm $form) {
                    $pscPerson = new AnnualReturnPersonPsc($this->chAnnualReturn->getFormSubmissionId(), $this->get['psc_id']);
                    PersonPscDataTransformer::merge($pscPerson, $personPscData);
                    $pscPerson->save();

                    $this->chAnnualReturn->setNoPscReason(NULL);
                    $this->chAnnualReturn->save();


                    $form->clean();
                    $this->redirect(self::PSCS_PAGE, "company_id={$this->company->getCompanyId()}");
                },
                $prefillOfficers,
                $prefillAddress
            );
    }

    public function renderAddCorporatePsc()
    {
        if ($this->chAnnualReturn->getCompanyCategory() === 'LLP') {
            $this->redirect(self::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
        }

        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'corporate');
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $this->template->natureOfControls = $this->pscChoicesProvider->getNatureOfControlsTemplateStructure(PscChoicesProvider::CORPORATE, $this->companyType);
        $this->template->natureOfControlsDescription = $this->pscChoicesProvider->getNatureOfControlsTemplateDescription(PscChoicesProvider::CORPORATE);
        $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());

        $this->template->form = $this->pscFormFactory
            ->getCorporateAddForm(
                $this->company,
                function (CorporatePscData $corporatePscData, FForm $form) {
                    $pscCorporate = new AnnualReturnCorporatePsc($this->chAnnualReturn->getFormSubmissionId());
                    CorporatePscDataTransformer::merge($pscCorporate, $corporatePscData);
                    $pscCorporate->save();

                    $this->chAnnualReturn->setNoPscReason(NULL);
                    $this->chAnnualReturn->save();


                    $form->clean();
                    $this->redirect(self::PSCS_PAGE, "company_id={$this->company->getCompanyId()}");
                },
                $prefillOfficers,
                $prefillAddress
            );
    }

    public function renderEditCorporatePsc()
    {
        if (!isset($this->get['psc_id'])) {
            $this->redirect(self::PSCS_PAGE, "company_id={$this->company->getCompanyId()}");
        }

        if ($this->chAnnualReturn->getCompanyCategory() === 'LLP') {
            $this->redirect(self::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
        }

        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'corporate');
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $this->template->natureOfControls = $this->pscChoicesProvider->getNatureOfControlsTemplateStructure(PscChoicesProvider::CORPORATE, $this->companyType);
        $this->template->natureOfControlsDescription = $this->pscChoicesProvider->getNatureOfControlsTemplateDescription(PscChoicesProvider::CORPORATE);
        $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());

        $corporatePsc = new AnnualReturnCorporatePsc($this->chAnnualReturn->getFormSubmissionId(), $this->get['psc_id']);
        $this->template->form = $this->pscFormFactory
            ->getCorporateEditForm(
                $this->company,
                CorporatePscDataTransformer::from($corporatePsc),
                function (CorporatePscData $corporatePscData, FForm $form) {
                    $pscCorporate = new AnnualReturnCorporatePsc($this->chAnnualReturn->getFormSubmissionId(), $this->get['psc_id']);
                    CorporatePscDataTransformer::merge($pscCorporate, $corporatePscData);
                    $pscCorporate->save();

                    $this->chAnnualReturn->setNoPscReason(NULL);
                    $this->chAnnualReturn->save();


                    $form->clean();
                    $this->redirect(self::PSCS_PAGE, "company_id={$this->company->getCompanyId()}");
                },
                $prefillOfficers,
                $prefillAddress
            );
    }

    public function renderAddLegalPersonPsc()
    {
        if ($this->chAnnualReturn->getCompanyCategory() === 'LLP') {
            $this->redirect(self::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
        }

        $this->template->natureOfControls = $this->pscChoicesProvider->getNatureOfControlsTemplateStructure(PscChoicesProvider::LEGAL_PERSON, $this->companyType);
        $this->template->natureOfControlsDescription = $this->pscChoicesProvider->getNatureOfControlsTemplateDescription(PscChoicesProvider::LEGAL_PERSON);
        $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());

        $this->template->form = $this->pscFormFactory
            ->getLegalPersonAddForm(
                $this->company,
                function (LegalPersonPscData $legalPersonPscData, FForm $form) {
                    $pscLegalPerson = new AnnualReturnLegalPersonPsc($this->chAnnualReturn->getFormSubmissionId());
                    LegalPersonPscDataTransformer::merge($pscLegalPerson, $legalPersonPscData);
                    $pscLegalPerson->save();

                    $this->chAnnualReturn->setNoPscReason(NULL);
                    $this->chAnnualReturn->save();

                    $form->clean();
                    $this->redirect(self::PSCS_PAGE, "company_id={$this->company->getCompanyId()}");
                }
            );
    }

    public function renderEditLegalPersonPsc()
    {
        if (!isset($this->get['psc_id'])) {
            $this->redirect(self::PSCS_PAGE, "company_id={$this->company->getCompanyId()}");
        }

        if ($this->chAnnualReturn->getCompanyCategory() === 'LLP') {
            $this->redirect(self::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
        }

        $this->template->natureOfControls = $this->pscChoicesProvider->getNatureOfControlsTemplateStructure(PscChoicesProvider::LEGAL_PERSON, $this->companyType);
        $this->template->natureOfControlsDescription = $this->pscChoicesProvider->getNatureOfControlsTemplateDescription(PscChoicesProvider::LEGAL_PERSON);
        $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());

        $legalPersonPsc = new AnnualReturnLegalPersonPsc($this->chAnnualReturn->getFormSubmissionId(), $this->get['psc_id']);
        $this->template->form = $this->pscFormFactory
            ->getLegalPersonEditForm(
                $this->company,
                LegalPersonPscDataTransformer::from($legalPersonPsc),
                function (LegalPersonPscData $legalPersonPscData, FForm $form) {
                    $pscLegalPerson = new AnnualReturnLegalPersonPsc($this->chAnnualReturn->getFormSubmissionId(), $this->get['psc_id']);
                    LegalPersonPscDataTransformer::merge($pscLegalPerson, $legalPersonPscData);
                    $pscLegalPerson->save();

                    $this->chAnnualReturn->setNoPscReason(NULL);
                    $this->chAnnualReturn->save();

                    $form->clean();
                    $this->redirect(self::PSCS_PAGE, "company_id={$this->company->getCompanyId()}");
                }
            );
    }


    /*     * **************************** add transfer ******************************** */

    public function renderAddTransfer()
    {
        $return = AnnualReturnModel::getSummaryDetails($this->chAnnualReturn, $this->company);
        if ($return['company_category'] == 'BYGUAR' || $return['company_category'] == 'LLP') {
            $this->redirect(self::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
        }
        $this->template->form = AnnualReturnModel::getCompomentAddTransferForm($this);
    }

    public function Form_addTransferSubmitted(FForm $form)
    {
        try {
            $data = $form->getValues();
            AnnualReturnModel::addTransfer($this->company, $this->shareholding, $data);
            $form->clean();
            $this->flashMessage('Shareholdings have been updated');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $this->redirect(self::SHAREHOLDINGS_PAGE, "company_id={$this->company->getCompanyId()}");
    }

    /*     * **************************** summary ******************************** */

    public function handleSummary()
    {
        if (isset($this->get['pdf'])) {
            $return = AnnualReturnModel::getSummaryDetails($this->chAnnualReturn, $this->company);
            //pr($return);exit;
            try {
                CHFiling::getPdfAnnualreturnSummary($return);
                $this->terminate();
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
                $this->redirect();
            }
        }
    }

    public function renderSummary()
    {
        try {

            // service
            if ($this->company->getAnnualReturnId() == AnnualReturn::ANNUAL_RETURN_SERVICE_PRODUCT) {

                // display form only if it's service completed
                $this->arsm = AnnualReturnServiceModel::getByCompanyId($this->company->getCompanyId());
                if ($this->arsm->statusId == AnnualReturnServiceModel::STATUS_COMPLETED) {
                    $this->template->form = AnnualReturnServiceModel::getCompomentSummaryForm($this);
                }

                $this->template->isService = 1;
                $this->template->status = $this->arsm->statusId;

                // don't display when is draft
                if ($this->arsm->statusId != AnnualReturnServiceModel::STATUS_DRAFT) {
                    $this->template->return = AnnualReturnModel::getSummaryDetails($this->chAnnualReturn, $this->company);
                }
                // DIY
            } else {
                if ($this->chAnnualReturn->isSubmissionPending()) {
                    $this->template->status = 'pending';
                } else {
                    $this->template->form = AnnualReturnModel::getCompomentSummaryForm($this, 'front');
                    $this->template->status = 'completed';
                }
                $this->template->return = AnnualReturnModel::getSummaryDetails($this->chAnnualReturn, $this->company);
            }

            $annualReturnSummaryView = new AnnualReturnSummaryView($this->chAnnualReturn);
            $this->template->summaryView = $annualReturnSummaryView;
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('CUSummaryControler::SUMMARY_PAGE', "company_id={$this->company->getCompanyId()}");
        }
        $this->template->companyCategory = $this->chAnnualReturn->getCompanyCategory();
    }

    /**
     * Provides send application DIY
     *
     * @param FForm $form
     */
    public function Form_summaryFormSubmitted(FForm $form)
    {
        try {
            AnnualReturnModel::processSendApplication($this->company, $this->chAnnualReturn, $this->customer);
            $this->flashMessage('Your annual return application has been sent to Companies House and is currently pending. It usually takes them approx. 3 working hours to approve it and we will email you as soon as it\'s done.');
            $form->clean();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $this->redirect();
    }

    /**
     * Provides send application service
     *
     * @param FForm $form
     * @return void
     */
    public function Form_summaryServiceFormSubmitted(FForm $form)
    {
        try {
            // accept
            if ($form->isSubmitedBy('submit')) {
                AnnualReturnServiceModel::processAcceptedByCustomer($this->company, $this->chAnnualReturn, $this->arsm, $this->customer);
                $this->flashMessage('Your annual return application has been sent.');
                // reject
            } elseif ($form->isSubmitedBy('rejectSubmit')) {
                $data = $form->getValues();
                AnnualReturnServiceModel::processRejectedByCustomer($this->company, $this->arsm, $data, $this->customer);
                $this->flashMessage('Rejection has been sent');
            }

            $form->clean();
            $this->redirect();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('#summary#', "company_id={$this->company->getCompanyId()}");
        }
    }

}
