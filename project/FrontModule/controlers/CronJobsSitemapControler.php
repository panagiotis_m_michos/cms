<?php

class CronJobsSitemapControler extends BaseControler
{

    /**
     * @var string
     */
    private static $host;

    public function startup()
    {
        parent::startup();
        Debug::disableProfiler();
    }

    public function beforePrepare()
    {
        self::$host = FApplication::$httpRequest->getUri()->getHostUri();
        // check for hash
        if (isset($this->get['hash']) && $this->get['hash'] == '6a256241ea0b7a305ba21473a7667543') {
            try {
                $this->generateSitemap();
            } catch (Exception $e) {
                Debug::log($e);
            }
        }
        $this->terminate('done');
        parent::beforePrepare();
    }

    private function generateSitemap()
    {
        $root = <<< EOD
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
</urlset>
EOD;
        $file = WWW_DIR . '/sitemap.xml';
        $xml = new SimpleXMLElement($root);
        $sitemap = FSitemap::get(100);
        self::recursiveXml($sitemap, $xml);
        $sitemap = FSitemap::get(266);
        self::recursiveXml($sitemap, $xml);
        $str = $xml->saveXML();

        $domxml = new DOMDocument();
        $domxml->preserveWhiteSpace = FALSE;
        $domxml->formatOutput = TRUE;
        $domxml->loadXML($str);
        $domxml->save($file);
    }

    /**
     * @param string $url
     * @return string
     */
    private static function escapeUrl($url)
    {
        $segments = (array) @explode('/', $url);
        $segments = array_map('rawurlencode', $segments);
        $url = @implode('/', $segments);
        $url = htmlspecialchars($url, ENT_QUOTES);
        return $url;
    }

    /**
     * @param array $arr
     * @param SimpleXMLElement $xml
     */
    private static function recursiveXml($arr, $xml)
    {
        if (is_array($arr)) {
            foreach ($arr as $item) {
                if (!empty($item['url'])) {
                    $url = $xml->addChild('url');
                    $url->addChild('loc', self::$host . self::escapeUrl($item['url']));
                    $url->addChild('changefreq', 'daily');
                    $priority = $item['url'] == '/' ? 1 : 0.85;
                    $url->addChild('priority', $priority);
                }
                if (!empty($item['childs'])) {
                    self::recursiveXml($item['childs'], $xml);
                }
            }
        }
    }
}
