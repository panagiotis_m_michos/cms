<?php

class DoNotTouchControler extends DefaultControler
{
	public function startup() {
        parent::startup();

        // --- creating new form submission for specific errror ---
        // --- companies house says they don't have form submission, 
        // --- but we already sent one, and we cannot resend the same form 
        // --- submission coz we get error saying that it's duplicate form 
        // --- submission - companies from the 7th and 8th of October ---
        
        // 100259,100556,100616
        /*
        $formSubmissionId = 100616;

        $companyId = CHFiling::getDb()->fetchOne('SELECT company_id FROM ch_form_submission WHERE form_submission_id = '.$formSubmissionId);

        // --- copy the old form submission and create new one ---
        $sql = "
            INSERT INTO `ch_form_submission`
            (`company_id`, `form_identifier`, `language`)
            SELECT `company_id`, `form_identifier`, `language`
            FROM `ch_form_submission`
            WHERE `form_submission_id` = $formSubmissionId";

        // --- get id of the newly create form submission ---
        CHFiling::getDb()->query($sql);
        $newFormSubmissionId = CHFiling::getDb()->lastInsertId();

        // --- create file path for documents ---
        $fromPath   = CHFiling::getDocPath()."/company-$companyId/formSubmission-$formSubmissionId/";
        $toPath     = CHFiling::getDocPath()."/company-$companyId/formSubmission-$newFormSubmissionId/";

        // --- create folder if needed ---
        if (!is_dir($toPath)) {
            mkdir($toPath, 0777, true);
        }

        // --- copy all the files into new form submission folder ---
        if ($handle = opendir($fromPath)) {
            while (false !== ($file = readdir($handle))) {
                if ($file != '.' && $file != '..') {
                    copy($fromPath.$file, $toPath.$file);
                }
            }
        }

        // --- get reject reference and reject description ---
        $rejectReference    = null;
        $rejectDescription  = 'Companies house error';

        // --- create new company incorporation ---
        CompanyIncorporation::createRejectCompanyIncorporation
            ($newFormSubmissionId, $formSubmissionId, $rejectReference, $rejectDescription);
    
        pr('done');
        exit;
         */

        // ------------------------------------------------------------------------------------------------------- 

        /*
        $session = FApplication::$session;
        pr($session->generateVerificationKey());
        pr(Environment::getHttpRequest());
        pr(Environment::getHttpResponse());
        pr($session);exit;
         */

        /*
        $result = CHFiling::getDb()->fetchAssoc('
            SELECT DISTINCT customer_id FROM ch_form_submission 
            LEFT JOIN ch_company ON ch_form_submission.company_id = ch_company.company_id WHERE form_identifier = \'OfficerResignation\'');
        pr($result);exit;
         */

        /* --- sending company incorporation request ---
        $company = Company::getCompany('95435');
        $formSubmission = $company->getLastFormSubmission();
        $formSubmission->sendRequest();
         */

        // --- moving envelopes ---

        /*
        $path = CHFiling::getDocPath();
        $dirs = scandir($path);

        foreach ($dirs as $d) {
            if(strstr($d, 'company-'))
            {
                $num = (int) str_replace('company-', '', $d);
                $dir = floor($num/10000) + 1;
                $dir = (string) $dir . '0000';

                $old_dir = $path . '/' . $d;
				$new_dir = $path . '/' . $dir . '/' . $d;

                if (!is_dir($path.'/'.$dir)) {
                    mkdir($path.'/'.$dir, 0777, true);
                }
				rename($old_dir, $new_dir);
            }
        }
        echo 'done';
        exit;
         */
         
        // ------------------------
        
        // --- this is calling submission ---
        /*
	    CHFiling::setDb(array('192.168.100.35', 'cms', 'namornik', 'cms'));
	    CHFiling::setDocPath(PROJECT_DIR . '/temp/upload/ch_documents');
        $chFiling = new CHFiling();
         */

        /*
        CompanyIncorporation::createRejectCompanyIncorporation(100258, 100035, 'PEN53303', "Subscribers share total does not equal the Total shares shown in the Capital Statement.;The Memorandum absent.;The total for the subscribers' shares does not equal the total of taken-up shares.");

        exit;
         */

        //$company = $chFiling->getCompany(95636);
        //$company = $chFiling->getCompany(95635);
        //$company = $chFiling->getCompany(95634); -- rejected - no memorandum
        //$company = $chFiling->getCompany(95649);
        //$company = $chFiling->getCompany(95644);
        //$company = $chFiling->getCompany(95642);
        //$company = $chFiling->getCompany(95636);
        /*
        $company = $chFiling->getCompany(95635);
        $formSubmissionId = $company->getLastFormSubmission()->getFormSubmissionId();

        CHFiling::getFormSubmissionStatus($formSubmissionId);
        exit;
         */

        //$company->getLastFormSubmission()->sendRequest();

        //$company->getLastFormSubmission()->sendRequest();
        
        // --- this is for companies that have been bought on the old site but ---
        // --- haven't been incorporated yet. So calling the code bellow will create ---
        // --- form submission and company incorporation tables ---
        // --- adding form submission for company that's been bought on the old site ---
        
        /*
		$companyIds = array(
           2089
        );

		foreach ($companyIds as $value) {

			CHFiling::setDb(array('192.168.100.35', 'cms', 'namornik', 'cms'));
			//CHFiling::setDb(array('localhost', 'cms', 'namornik', 'cms'));

            $result = CHFiling::getDb()->fetchOne('SELECT company_id FROM ch_company WHERE company_id = '.$value);
            if (!isset($result) || empty($result)) {
                throw new Exception('company does not exist');
            }

            $result = CHFiling::getDb()->fetchOne('SELECT form_submission_id FROM ch_form_submission WHERE company_id = '.$value);
            if (!isset($result) || empty($result)) {
                CHFiling::getDb()->insert('ch_form_submission', array('form_identifier' => 'CompanyIncorporation', 'company_id' => $value));
                $lastInsertId = CHFiling::getDb()->lastInsertId();
                CHFiling::getDb()->insert('ch_company_incorporation', array('form_submission_id' => $lastInsertId, 'type' => 'BYSHR'));
            }
		}
        */

        // --- sync ---
        /*
	    CHFiling::setDb(array('192.168.100.35', 'cms', 'namornik', 'cms'));
	    CHFiling::setDocPath(PROJECT_DIR . '/temp/upload/ch_documents');
        $company = CHFiling::getCompany(70524);
        echo $company->updateCompanyGetXml();exit;
         */

	}
	
}

?>
