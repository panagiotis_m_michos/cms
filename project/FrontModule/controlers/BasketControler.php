<?php

use BasketModule\Config\DiLocator as BasketModuleDiLocator;
use BasketModule\Services\BasketService;
use CompaniesHouse\Data\Data;
use CompaniesHouse\Data\Exceptions\CompanyNotFoundException;
use CompaniesHouse\Data\Exceptions\DataException;
use Doctrine\ORM\EntityManager;
use Exceptions\Business\CompanyException;
use Exceptions\Business\CompanyImportException;
use Front\Payment\IOrderSummaryFormDelegate;
use Front\Payment\OrderSummaryForm;
use Nette\Utils\Strings;
use RouterModule\Helpers\ControllerHelper;
use Services\CompanyService;
use Services\NodeService;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;

class BasketControler extends DefaultControler implements IOrderSummaryFormDelegate
{
    const PAGE_BASKET = 130;
    const PAGE_SUMMARY = 131;

    /**
     * @var array
     */
    public $possibleActions = [
        self::PAGE_BASKET => 'default',
        self::PAGE_SUMMARY => 'summary',
    ];

    /**
     * @var BasketModel
     */
    public static $handleObject = 'BasketModel';

    /**
     * @var BasketModel
     */
    public $node;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var SessionNamespace
     */
    private $orderSummarySession;

    /**
     * @var NodeService
     */
    private $nodeService;

    /**
     * @var EventDispatcher
     */
    private $eventDispatcher;

    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    public function startup()
    {
        $this->companyService = $this->getService(DiLocator::SERVICE_COMPANY);
        $this->em = $this->getService(DiLocator::ENTITY_MANAGER);
        $this->orderSummarySession = $this->getService(DiLocator::SESSION)->getNamespace('orderSummary');
        $this->nodeService = $this->getService(DiLocator::SERVICE_NODE);
        $this->eventDispatcher = $this->getService(DiLocator::DISPATCHER);
        $this->controllerHelper = $this->getService('router_module.helpers.controller_helper');

        if (!isset($this->orderSummarySession->importedCompanies)) {
            $this->orderSummarySession->importedCompanies = [];
        }

        parent::startup();
    }

    public function beforePrepare()
    {
        $this->node->startup($this->basket, $this->customer, $this->eventDispatcher);
        parent::beforePrepare();
    }

    /**
     * Handle with basket: add, remove_id, remove_all
     */
    public function beforeHandle()
    {
        try {
            if (isset($this->get['add'])) {
                $this->node->addBasketItem($this->get['add']);
                $this->flashMessage('Product has been added to your basket');
                $this->redirect(NULL, 'add=');
            } elseif (isset($this->get['remove_id'])) {
                $this->node->removeBasketItem($this->get['remove_id']);
                $this->flashMessage('Basket has been updated');
                $this->redirect(NULL, 'remove_id=');
            } elseif (isset($this->get['remove_all'])) {
                $this->node->removeAllBasketItems();
                $this->flashMessage('Basket has been updated');
                $this->redirect(NULL, 'remove_all=');
            }
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect(NULL, ['add' => NULL, 'remove_id' => NULL, 'remove_all' => NULL]);
        }
        parent::beforeHandle();
    }

    /*     * *********************************************** basket ************************************************ */

    public function handleDefault()
    {
        try {
            $request = Request::createFromGlobals();
            if ($request->isMethod('POST')) {
                $do = $request->request->get('do');
                if ($do === 'add') {
                    $productId = $request->request->get('productId');
                    if ($productId) {
                        $product = $this->nodeService->getProductById($productId, TRUE);
                        $this->node->addProduct($product);
                        $this->flashMessage('Product has been added to your basket');
                        $this->redirect();
                    }
                }
            }

            $errors = FApplication::$container->get(BasketModuleDiLocator::BASKET_VALIDATOR)->validate($this->basket)->getErrors();
            if ($errors) {
                $this->flashMessage(implode('<br>', $errors), 'error', FALSE);
                $this->readFlashMessages();
            }

        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    public function renderDefault()
    {
        $this->template->form = $this->node->getComponentBasketForm([$this, 'Form_defaultFormSubmitted']);
    }

    /**
     * @param FForm $form
     */
    public function Form_defaultFormSubmitted(FForm $form)
    {
        try {
            $this->node->saveBasketFormData($form);
            if ($form->isSubmitedBy('removeVoucher')) {
                $this->flashMessage('Voucher code has been removed');
                $this->redirect();

            } elseif ($form->isSubmitedBy('update')) {
                $this->flashMessage('Basket has been updated');
                $this->redirect();

            } elseif ($form->isSubmitedBy('checkout')) {
                // analitics url start
                if ($this->basket->hasPackageInBasket()) {
                    $package = $this->basket->getPackage();
                    $param = str_replace(' ', '', strtolower(Package::$types[$package->getId()]));
                    $this->redirect(self::PAGE_SUMMARY, ['package' => $param]);
                }
                // analitics url end
                $this->redirect(self::PAGE_SUMMARY);

            } elseif ($form->isSubmitedBy('continue')) {
                $this->redirect($this->getHomeLink());
            }
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    /*     * *********************************************** summary ************************************************ */

    /**
     * Check if customer is logged
     */
    protected function prepareSummary()
    {
        if ($this->basket->isEmpty() === TRUE) {
            $this->redirect(self::PAGE_BASKET);
        }

        $this->routeFromSummary();
    }

    protected function renderSummary()
    {
        /* @var $basketService BasketService */
        $basketService = $this->getService('basket_module.services.basket_service');
        $packageToProductDowngradeNotification = $basketService->getPackageToProductDowngradeNotification();
        $packageToPackageDowngradeNotification = $basketService->getPackageToPackageDowngradeNotification();

        $customerCompanies = $this->node->getCustomerCompaniesDropdown();
        $customerIncCompanies = $this->node->getIncCustomerCompaniesDropdown();

        $form = new OrderSummaryForm('summary');
        $form->startup(
            $this,
            $this->basket,
            $customerCompanies,
            $customerIncCompanies,
            $packageToProductDowngradeNotification,
            $packageToPackageDowngradeNotification
        );

        $this->template->form = $form;
        $this->template->associatedProducts2 = $this->node->getSummaryAssociatedProducts($this->basket);

        $this->template->productToPackageUpgradeNotifications = $basketService->getProductToPackageUpgradeNotifications();
        $this->template->packageToPackageUpgradeNotifications = $basketService->getPackageToPackageUpgradeNotifications();
        $this->template->packageToProductDowngradeNotification = $packageToProductDowngradeNotification;
        $this->template->packageToPackageDowngradeNotification = $packageToPackageDowngradeNotification;
        $this->template->suspendedServiceItem = $basketService->getSuspendedServiceItem();
        $this->template->hide = 1;

        if ($this->isAjax()) {
            $this->sendJSONResponse(
                ['template' => $this->template->getHtml('Basket/@form.tpl')]
            );
        }
    }

    /* ****************************** IOrderSummaryFormDelegate ****************************** */

    /**
     * @param $productKey
     * @param OrderSummaryForm $form
     */
    public function summaryFormImport($productKey, OrderSummaryForm $form)
    {
        try {
            /** @var BasketProduct $basketItem */
            $basketItem = $this->basket->items[$productKey];
            $companyNumber = Strings::padLeft($basketItem->companyNumber, 8, '0');
            $company = $this->companyService->getCompanyByCompanyNumber($companyNumber);

            if ($company) {
                $basketItem->linkedCompanyStatus = BasketProduct::LINK_EXISTING_IMPORT;
                $basketItem->setCompanyId($company->getCompanyId());
                $basketItem->companyName = $company->getCompanyName();
            } else {
                $importedCompanyData = $this->getImportedCompanyData($companyNumber);
                $basketItem->linkedCompanyStatus = BasketProduct::LINK_NEW_IMPORT;
                $basketItem->setCompanyId(NULL);
                $basketItem->companyName = $importedCompanyData['companyName'];
                $this->orderSummarySession->importedCompanies[$companyNumber] = $importedCompanyData;
            }

            $basketItem->companyNumber = $companyNumber;
        } catch (CompanyImportException $e) {
            $form->addErr($productKey, $e->getMessage());
            $form->saveToSession();
        } catch (Exception $e) {
            $form->addErr($productKey, 'Error has occurred. Please try again later.');
            $form->saveToSession();
        }

        if (!$this->isAjax()) {
            $this->redirect();
        }
    }

    /**
     * @param $productKey
     * @param OrderSummaryForm $form
     */
    public function summaryFormEditImport($productKey, OrderSummaryForm $form)
    {
        try {
            /** @var BasketProduct $basketItem */
            $basketItem = $this->basket->items[$productKey];
            $companyNumber = $basketItem->companyNumber;

            if ($companyNumber) {
                $basketItem->setCompanyId(NULL);
                $basketItem->companyNumber = NULL;
                $basketItem->companyName = NULL;
                $basketItem->linkedCompanyStatus = NULL;

                $this->removeImportedCompany($companyNumber);
            }
        } catch (CompanyException $e) {
            $form->addErr($productKey, $e->getMessage());
            $form->saveToSession();
        } catch (Exception $e) {
            $form->addErr($productKey, 'Error has occurred. Please try again later.');
            $form->saveToSession();
        }

        if (!$this->isAjax()) {
            $this->redirect();
        }
    }

    public function summaryFormRemovedVoucher()
    {
        $this->flashMessage('Voucher code has been removed');
        $this->redirect();
    }

    /**
     * @param array $linkPackageVariable
     */
    public function summaryFormPayment(array $linkPackageVariable = NULL)
    {
        $this->redirectRoute('payment', $linkPackageVariable);
    }

    public function summaryFormAddVoucher()
    {
        $this->flashMessage('Voucher code has been applied');
        $this->redirect();
    }

    public function summaryFormBasketPage()
    {
        $this->redirect(self::PAGE_BASKET);
    }

    /**
     * @param Exception $e
     */
    public function summaryFormError(Exception $e)
    {
        $this->flashMessage($e->getMessage(), 'error');
        $this->redirect();
    }

    /**
     * @param string $companyNumber
     * @throws CompanyImportException
     * @return array
     */
    private function getImportedCompanyData($companyNumber)
    {
        /** @var Data $companiesHouse */
        $companiesHouse = $this->getService(DiLocator::CH_DATA);

        try {
            $companyDetails = $companiesHouse->getCompanyDetails($companyNumber);
        } catch (CompanyNotFoundException $e) {
            throw CompanyImportException::companyNotFound();
        } catch (DataException $e) {
            $this->logger->error(
                "There was an error while importing company during purchase process",
                [
                    'customerId' => $this->customerEntity->getCustomerId(),
                    'companyNumber' => $companyNumber,
                    'exception' => $e,
                ]
            );
            throw CompanyImportException::companiesHouseNotResponding();
        }

        return [
            'companyName' => $companyDetails->getCompanyName(),
            'companyNumber' => $companyDetails->getCompanyNumber(),
            'companyStatus' => $companyDetails->getCompanyStatus(),
            'countryOfOrigin' => $companyDetails->getCountryOfOrigin(),
            'incorporationDate' => $companyDetails->getIncorporationDate(),
            'accountsRefDate' => $companyDetails->getAccounts()->getAccountRefDay() . '-' . $companyDetails->getAccounts()->getAccountRefMonth(),
            'accountsNextDueDate' => $companyDetails->getAccounts()->getNextDueDate(),
            'accountsLastMadeUpDate' => $companyDetails->getAccounts()->getLastMadeUpDate(),
            'returnsNextDueDate' => $companyDetails->getReturns()->getNextDueDate(),
            'returnsLastMadeUpDate' => $companyDetails->getReturns()->getLastMadeUpDate(),
            'careOfName' => $companyDetails->getRegAddress()->getCareOf(),
            'poBox' => $companyDetails->getRegAddress()->getPobox(),
            'isCertificatePrinted' => TRUE,
            'isBronzeCoverLetterPrinted' => TRUE,
        ];
    }

    /**
     * @param string $companyNumber
     * @return bool
     */
    private function removeImportedCompany($companyNumber)
    {
        foreach ($this->basket->getItems() as $item) {
            if ($item->companyNumber == $companyNumber) {
                return FALSE;
            }
        }

        unset($this->orderSummarySession->importedCompanies[$companyNumber]);
        return TRUE;
    }

    private function routeFromSummary()
    {
        $isSignedIn = Customer::isSignedIn();
        $hasPackage = FALSE;
        foreach ($this->basket->getItems() as $item) {
            if (array_key_exists($item->getId(), Package::$types)) {
                $hasPackage = TRUE;
                break;
            }
        }
        if ($this->basket->hasWholesalePackage()) {
            $this->redirectRoute('payment');
        } elseif (!$isSignedIn && !$hasPackage) {
            $this->saveBacklink();
            $this->redirect(LoginControler::LOGIN_PAGE);
        } elseif (!$isSignedIn && $hasPackage) {
            $linkPackageVariable = NULL;
            foreach ($this->basket->items as $item) {
                if ($item instanceof Package && isset(Package::$types[$item->getId()])) {
                    $linkPackageVariable = ['package' => str_replace(' ', '', strtolower(Package::$types[$item->getId()]))];
                }
            }

            $this->redirectRoute('payment', $linkPackageVariable);
        } elseif ($isSignedIn && $hasPackage) {
            $this->redirectRoute('payment');
        }
    }
}
