<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    PackageControler.php 2009-07-08 divak@gmail.com
 */


class PackageControler extends DefaultControler
{
	public function renderDefault()
	{
		$company = SearchControler::getCompanyName();
		if($company != FALSE)
		{
			$this->template->company = $company;
		}
		
        //test for page 1244 - clean this code after all test are done
        if ( isset($this->get['no']) && $this->get['no'] == 1) {
            $this->template->noCompany = 1;
        } else {
            $this->template->noCompany = 0;
        }
        
        $this->template->package = new Package($this->nodeId);
		
		// other packages
		$ids = FNode::getChildsIds(PackageAdminControler::PACKAGES_FOLDER);
		$this->template->packages = FNode::getTitleAndLink($ids);
	}
}