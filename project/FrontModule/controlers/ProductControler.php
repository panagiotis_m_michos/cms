<?php

class ProductControler extends DefaultControler
{

    /**
     * @var string
     */
    public static $handleObject = 'BasketProduct';

    public function handleDefault()
    {
        if (!isset($this->get['popup'])) {
            $this->redirect(BasketControler::PAGE_BASKET);
        }

        try {
            if (!isset($this->get['packageId'])) {
                throw new Exception('Package Id is required');
            }
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            self::reloadOpener();
        }
    }

    protected function renderDefault()
    {
        $this->template->image = $this->node->getUpgradeImageImage();
        $this->template->node = $this->node;
        $this->template->packageId = (int) $this->getParameter('packageId');
        $this->template->nodeId = $this->node->getId();
    }

}
