<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS
 * @author 		Nikolai Senkevich, Stan Bazik
 * @internal 	project/controlers/front/CompaniesMonitoringControler.php
 * @created 	21/03/2011
 */
class CompaniesMonitoringControler extends LoggedControler
{
    const COMPANIES_MONITORING_PAGE = 945;

    /** @var array */
    public $possibleActions = array(
        self::COMPANIES_MONITORING_PAGE => 'default',
    );

    public function beforePrepare()
    {
        // no journey page for wholesalers
        if (Customer::isSignedIn() === TRUE && Customer::getSignedIn()->roleId != Customer::ROLE_WHOLESALE) {
            $this->redirect(DashboardCustomerControler::DASHBOARD_PAGE);
        }
        parent::beforePrepare();
    }

    protected function renderDefault()
    {
        $addForm = new CompaniesMonitoringAddForm($this->node->getId() . '_add');
        $addForm->startup($this, array($this, 'Form_addFormSubmitted'));
        $this->template->formAdd = $addForm;


        $listForm = new CompaniesMonitoringListForm($this->node->getId() . '_list', 'get');
        $listForm->startup($this, array($this, 'Form_listFormSubmitted'));
        $this->template->formList = $listForm;
        
        $activeCompanyform = new CompaniesMonitoringACForm($this->node->getId() . '_active');
        $activeCompanyform->startup($this, array($this, 'Form_ACFormSubmitted'),$this->customer);
        $this->template->activeform = $activeCompanyform;

        $service = new CompanyMonitoringService();
        $customer = $this->customer;

        // --- export to CSV ---
        if (isset($this->get['csv'])) {
            Debug::disableProfiler();
            try {
                $service->outputCustmerCompaniesListToCSV($customer, $listForm);
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
                $this->redirect('default');
            }
        }

        // --- sync all companies ---
        if (isset($this->get['syncAll'])) {
            try {
                $service->syncCustomerCompanies($customer);
                $this->flashMessage('All monitored companies have now been synchronised with Companies House.');
                $this->redirect('default');
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
                $this->redirect('default');
            }
        }

        // --- removing company monitoring ---
        if (isset($this->get['delete'])) {
            try {
                $companyMonitoring = new CompanyMonitoring($this->get['delete']);
                $companyMonitoring->delete();
                $this->flashMessage('Company has been deleted.');
                $this->redirect('default');
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
                $this->redirect('default');
            }
        }

        // --- ordering company monitoring ---
        if (isset($this->get['order'])) {
            try {
                $service->getCustmerCompaniesOrder($this->get['order']);
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
                $this->redirect('default');
            }
        }
        $service->saveActiveCompaniesStatus($customer, $activeCompanyform);
        $paginator = $service->getCustmerCompaniesPaginator($customer, $listForm);
        $this->template->paginator = $paginator;
        $custmerCompaniesList = $service->getCustmerCompaniesList($customer, $listForm, $paginator);

        $this->template->customerCompaniesList = $custmerCompaniesList;
        $this->template->customer = $customer;
        $this->template->reminderDate = date('Y/m/d', strtotime("+61 day"));
    }

    /**
     * @param CompaniesMonitoringAddForm $form
     */
    public function Form_addFormSubmitted(CompaniesMonitoringAddForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Company has been added.');
            $this->redirect('default');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('default');
        }
    }

    /**
     * @param CompaniesMonitoringListForm $form
     */
    public function Form_listFormSubmitted(CompaniesMonitoringListForm $form)
    {
        try {
            $form->process();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('default');
        }
    }
    
    /**
     * @param CompaniesCustomerACForm $form
     */
    public function Form_ACFormSubmitted(CompaniesMonitoringACForm $form)
    {
        try {
            $form->process();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('default');
        }
    }

}
