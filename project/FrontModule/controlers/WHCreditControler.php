<?php

class WHCreditControler extends LoggedControler
{

    const CREDIT_PRODUCT = 609;
    const ADD_CREDIT_PAGE = 606;

    /** @var array */
    public $possibleActions = array(
        self::ADD_CREDIT_PAGE => 'add',
    );

    public function renderAdd()
    {
        $form = new FForm('WHaddCredit');
        $form->setRenderClass('Default2Render');

        $form->addFieldset('Data');
        $form->addText('available', 'Available Credit: ')
            ->readonly()
            ->style('bordeR: 0;')
            ->setValue('£' . number_format($this->customer->credit, 2, '.', ''));
        $form->addText('amount', 'Add Credit: ')
            ->addRule(FForm::Required, 'Please provide amount')
            ->addRule(FForm::NUMERIC, 'Please provide number')
            ->addRule(FForm::GREATER_THAN, 'Please provide number greater than #1', 0);

        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Make payment')->style('width: 200px; height: 30px;');
        $form->onValid = array($this, 'Form_defaultFormSubmitted');
        $form->start();
        $this->template->form = $form;
    }

    public function Form_defaultFormSubmitted(FForm $form)
    {
        $data = $form->getValues();

        $product = new CreditProduct(self::CREDIT_PRODUCT);
        $product->price = $data['amount'];

        $basket = new Basket();
        $basket->clear(FALSE);
        $basket->add($product);

        $form->clean();
        $this->redirectRoute('payment');
    }

}
