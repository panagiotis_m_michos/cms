<?php

class CUShareholdersControler extends CUControler
{
	const SHOW_SHAREHOLDER_PERSON_PAGE = 214;
	const SHOW_SHAREHOLDER_CORPORATE_PAGE = 222;
	
	/** @var array */
	public $possibleActions = array(
		self::SHOW_SHAREHOLDER_PERSON_PAGE => 'showShareholderPerson',
		self::SHOW_SHAREHOLDER_CORPORATE_PAGE => 'showShareholderCorporate',
	);
	
	/** @var Person */
	private $shareholder;
	
	
	public function renderShowShareholderPerson()
	{
    	// check get
		if (!isset($this->get['shareholder_id'])) {
			$this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id='.$this->company->getCompanyId()));
		}
		
        // check capital more than 1
        $data = $this->company->getData();
        //if(isset($data['capitals'][1])){
        //    $this->flashMessage("Sorry, we are unable to auto produce your share certificate. Please use the below share certificate template.");
        //    $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id='.$this->company->getCompanyId(),'shareholder_id'=>NULL));
        //}
        
		// check existing shareholder
		    $shareholder = $this->company->getPerson($this->get['shareholder_id'], 'sub');
            $address = $shareholder->getAddress();
            $addressArr = $address->getFields();

            // check address
            if(empty($addressArr['street'])){
                
                //show form -- passing shareholder, company and certificate number
                $form = new PersonForm($this->node->getId() . '_home');
                $form->startup($this, array($this, 'Form_shareholderPersonFormSubmitted'), $shareholder, $this->company, $this->get['num']);
                $this->template->form = $form;

            }else{
                //print pdf
                $person = ShareCert::getPersonCertificate($shareholder, $this->company->getCompanyName(), $this->company->getCompanyNumber(), $addressArr, $this->get['num']);							
                $person->getPdfCertificate();
            }
             
			$this->template->shareholder = $shareholder->getFields();
	}
	
	
    public function renderShowShareholderCorporate()
	{
        // check get
		if (!isset($this->get['shareholder_id'])) {
			$this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id='.$this->company->getCompanyId()));
		}

        // check capital more than 1 
        $data = $this->company->getData();
        //if(isset($data['capitals'][1])){
        //    $this->flashMessage("Sorry, we are unable to auto produce your share certificate. Please use the below share certificate template.");
        //    $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id='.$this->company->getCompanyId(),'shareholder_id'=>NULL));
        //}
            // check existing shareholder
		    $shareholder = $this->company->getCorporate($this->get['shareholder_id'], 'sub');
            $address = $shareholder->getAddress();
            $addressArr = $address->getFields();

            // check address
            if(empty($addressArr['street'])){
                
                //show form -- passing shareholder, company and certificate number 
                $form = new CorporateForm($this->node->getId() . '_home');
                $form->startup($this, array($this, 'Form_shareholderCorporateFormSubmitted'), $shareholder, $this->company, $this->get['num']);
                $this->template->form = $form;

            }else{
                //print pdf
                $corporate = ShareCert::getCorporateCertificate($shareholder, $this->company->getCompanyName(), $this->company->getCompanyNumber(), $addressArr, $this->get['num']);							
                $corporate->getPdfCertificate();
            }
             
			$this->template->shareholder = $shareholder->getFields();
	}


/**
     * @param PersonForm $form
     */
    public function Form_shareholderPersonFormSubmitted(PersonForm $form)
    {
        try {
            //generate pdf from form and shareholder data
            $form->process();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id='.$this->company->getCompanyId(),'shareholder_id'=>NULL));
        }
    }
	
        /**
     * @param CorporateForm $form
     */
    public function Form_shareholderCorporateFormSubmitted(CorporateForm $form)
    {
        try {
            //generate pdf from form and shareholder data
            $form->process();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id='.$this->company->getCompanyId(),'shareholder_id'=>NULL));
        }
    }
}