<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    CURemindersControler.php 2010-05-14 divak@gmail.com
 */


class CURemindersControler extends CUControler
{
	const REMINDERS_PAGE = 717;
	
	/** @var string */
	public static $handleObject = 'RemindersModel';
	
	/**
	 * @var RemindersModel
	 */
	public $node;
	
	
	public function beforePrepare()
	{
		parent::beforePrepare();
		$this->node->startup($this->company, $this->customer);
	}
	
	
	public function renderDefault()
	{
        $this->template->reminder = $this->node->getReminder();
		$this->template->form = $this->node->getComponentRemindersForm(array($this, 'Form_DefaultFormSubmitted'));
	}
	
	public function Form_DefaultFormSubmitted(FForm $form)
	{
		try {
			$this->node->saveReminderData($form, $this->company);
			$this->flashMessage('Reminders have been updated');
			$this->redirect(CUSummaryControler::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
		} catch (Exception $e) {
			$this->flashMessage($e->getMessage(), 'error');
			$this->redirect();
		}
	}
	
}
