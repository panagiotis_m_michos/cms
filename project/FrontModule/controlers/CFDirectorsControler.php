<?php

use Admin\CHForm;
use Services\CompanyService;

class CFDirectorsControler extends CFControler
{
    const DIRECTORS_PAGE = 1129;
    const ADD_DIRECTOR_PERSON_PAGE = 156;
    const EDIT_DIRECTOR_PERSON_PAGE = 157;
    const ADD_DIRECTOR_CORPORATE_PAGE = 175;
    const EDIT_DIRECTOR_CORPORATE_PAGE = 176;

    /**
     * @var array
     */
    public $possibleActions = [
        self::DIRECTORS_PAGE => 'directors',
        self::ADD_DIRECTOR_PERSON_PAGE => 'addDirectorPerson',
        self::EDIT_DIRECTOR_PERSON_PAGE => 'editDirectorPerson',
        self::ADD_DIRECTOR_CORPORATE_PAGE => 'addDirectorCorporate',
        self::EDIT_DIRECTOR_CORPORATE_PAGE => 'editDirectorCorporate',
    ];

    /**
     * @var string
     */
    static public $handleObject = 'CFDirectorsModel';

    /**
     * @var CFDirectorsModel
     */
    public $node;

    /**
     * @var IncorporationPersonDirector|IncorporationCorporateDirector
     */
    private $director;

    public function beforePrepare()
    {
        parent::beforePrepare();
        if ($this->checkStepPermission('directors') == FALSE) {
            $this->redirect(
                CFRegisteredOfficeControler::REGISTER_OFFICE_PAGE,
                "company_id={$this->company->getCompanyId()}"
            );
        }
        $this->template->hide = 1;
    }

    /********************************************* list of directors ***************************************************/

    protected function handleDirectors()
    {
        // delete director
        if (isset($this->get['delete_person_id']) || isset($this->get['delete_corporate_id'])) {
            try {
                if (isset($this->get['delete_person_id'])) {
                    $director = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncPerson(
                        $this->get['delete_person_id'],
                        'dir'
                    );
                } else {
                    $director = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncCorporate(
                        $this->get['delete_corporate_id'],
                        'dir'
                    );
                }
                $director->remove();
                $this->flashMessage('Director has been deleted');
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage());
            }
            $this->redirect(NULL, ['delete_person_id' => NULL, 'delete_corporate_id' => NULL]);
        }
    }

    protected function prepareDirectors()
    {
        // redirect to add - if there is no director
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->directorsCount() == 0
            && $this->company->getNomineeDirectorId() == NULL
        ) {
            $this->redirect(self::ADD_DIRECTOR_PERSON_PAGE, 'company_id=' . $this->company->getCompanyId());
        }

        // nominee director
        $nomineeDirectorId = $this->company->getNomineeDirectorId();
        if ($nomineeDirectorId !== NULL) {
            $nomineeDirector = new NomineeDirector($nomineeDirectorId);
            $this->template->form = $this->getComponentNomineeDirectorForm($nomineeDirector);
        }

        // add persons and corporates
        $this->template->persons = $this->getComponentPersons();
        $this->template->corporates = $this->getComponentCorporates();
        $this->template->nominees = $this->getComponentNominees();
        $this->template->residents = $this->getComponentResidents();

        // continue form
        $continue = new FForm('continueDirectors');
        $continue->addSubmit('continueprocess', 'Continue >')->class('btn_submit fright mbottom');
        $continue->onValid = [$this, 'Form_continueDirectorsFormSubmitted'];
        $continue->start();
        $this->template->continue = $continue;
        $this->template->hide = 1;
    }

    protected function renderDirectors()
    {
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() == 'LLP') {
            $this->template->title = 'Members';
            $this->template->seoTitle = 'Members';
        }
    }

    /**
     * @param FForm $form
     */
    public function Form_continueDirectorsFormSubmitted(FForm $form)
    {
        $type = $this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType();
        if ($type == CompanyIncorporation::PUBLIC_LIMITED) {
            try {
                $this->check('MINIMUM_2_DIRECTORS');
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
                $this->redirect();
            }
        } elseif ($type == CompanyIncorporation::LIMITED_LIABILITY_PARTHENERSHIP) {
            try {
                $this->check('MINIMUM_2_MEMBERS');
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
                $this->redirect();
            }
        } elseif ($this->company->getLastIncorporationFormSubmission()->getForm()->directorsCount() == 0) {
            $this->flashMessage('You need to appoint at least one director', 'error');
            $this->redirect();
        } else {
            try {
                $this->check('MINIMUM_1_PERSON_DIRECTOR');
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
                $this->redirect();
            }
        }
        if ($type == CompanyIncorporation::LIMITED_LIABILITY_PARTHENERSHIP) {
            //llp don't have secretaries and shareholder and don't need articles if doesn't include reserved words
            $this->redirect(CFPscsControler::PSCS_PAGE, "company_id={$this->company->getCompanyId()}");
        } else {
            // for ltd by shares and by garantee
            $this->redirect(CFShareholdersControler::SHAREHOLDERS_PAGE, "company_id={$this->company->getCompanyId()}");
        }
    }

    /**
     * @param FForm $form
     */
    public function Form_DirectorsFormSubmitted(FForm $form)
    {
        $nomineeDirector = FNode::getProductById($this->company->getNomineeDirectorId());

        if ($form->getValue('nominee') == 1) {
            try {
                if (!$this->submissionContainsNomineeDirector()) {
                    /** @var CompanyService $companyService */
                    $companyService = $this->getService(DiLocator::SERVICE_COMPANY);
                    $companyService->addNomineeDirectorToCompany($this->company, $nomineeDirector);
                    $this->flashMessage("{$nomineeDirector->getLngTitle()} has been added!");
                }
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage());
            }
        } else {
            $this->company->getLastIncorporationFormSubmission()->getForm()->removeNomineeDirector();
            $this->flashMessage("{$nomineeDirector->getLngTitle()} has been removed!");
        }

        $form->clean();
        $this->redirect();
    }

    /**********************************************************************************/
    /************************** DIRECTOR PERSON ***************************************/
    /**********************************************************************************/

    /********************************************* add director person ***************************************************/

    protected function renderAddDirectorPerson()
    {
        // prefill
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'person');
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];

        $form = $this->getComponentPersonForm('add', $prefillOfficers, $prefillAddress);
        $this->template->form = $form;
        $this->template->companyId = $this->company->getCompanyId();
        $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() == 'LLP') {
            $this->template->title = 'Add Member';
            $this->template->seoTitle = 'Add Member';
        }
    }

    /**
     * @param $form
     */
    public function Form_addDirectorPersonFormSubmitted(CHForm $form)
    {
        try {
            $data = $form->getValues();

            /** @var IncorporationPersonDirector $director */
            $director = $this->company->getLastIncorporationFormSubmission()->getForm()->getNewIncPerson('dir');

            // set person
            $person = $director->getPerson();
            $person->setTitle($data['title']);
            $person->setForename($data['forename']);
            $person->setMiddleName($data['middle_name']);
            $person->setSurname($data['surname']);
            if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() != 'LLP') {
                $person->setNationality($data['nationality']);
                $person->setOccupation($data['occupation']);
            }
            $person->setDob($data['dob']);
            $person->setCountryOfResidence($data['country_of_residence']);
            $director->setPerson($person);

            // set address
            if ($this->company->getServiceAddress() && $data['ourServiceAddress']) {
                $msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());
                $address = $director->getAddress();
                $address->setFields((array) $msgServiceAdress);
                //pr($address);exit;
                $director->setAddress($address);
            } else {
                $address = $director->getAddress();
                $address->setFields($data);
                $director->setAddress($address);
            }
            // set residential address
            if ($data['residentialAddress'] == 1) {
                $address = $director->getResidentialAddress();
                $address->setFields($data, 'residential_');
                $director->setResidentialAddress($address);
            } else {
                $address->setSecureAddressInd($data['residential_secure_address_ind']);
                $director->setResidentialAddress($address);
            }

            if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() == 'LLP') {
                // set authentication
                $authetication = $director->getAuthentication();
                $authetication->setFields($data);
                $director->setAuthentication($authetication);
            }

            $director->setConsentToAct($data['consentToAct']);

            $director->save();
            $form->clean();
            if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() == 'LLP') {
                $this->flashMessage('Member has been added');
            } else {
                $this->flashMessage('Director has been added');
            }
            $this->redirect(self::DIRECTORS_PAGE, 'company_id=' . $this->company->getCompanyId());

        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    /********************************************* edit director person ***************************************************/

    protected function prepareEditDirectorPerson()
    {
        // check get
        if (!isset($this->get['director_id'])) {
            $this->redirect(self::DIRECTORS_PAGE, 'company_id=' . $this->company->getCompanyId());
        }

        // check existing director
        try {
            $this->director = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncPerson(
                $this->get['director_id'],
                'dir'
            );
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect(
                self::DIRECTORS_PAGE,
                ['company_id=' . $this->company->getCompanyId(), 'director_id' => NULL]
            );
        }
    }

    protected function renderEditDirectorPerson()
    {
        // prefill
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'person');
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];

        $form = $this->getComponentPersonForm('edit', $prefillOfficers, $prefillAddress);
        $this->template->form = $form;
        $this->template->companyId = $this->company->getCompanyId();
        $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() == 'LLP') {
            $this->template->title = 'Amend Member';
            $this->template->seoTitle = 'Amend Member';
        }
    }

    /**
     * @param $form
     */
    public function Form_editDirectorPersonFormSubmitted(CHForm $form)
    {
        try {
            $data = $form->getValues();

            $person = $this->director->getPerson();
            $person->setTitle($data['title']);
            $person->setForename($data['forename']);
            $person->setMiddleName($data['middle_name']);
            $person->setSurname($data['surname']);
            if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() != 'LLP') {
                $person->setNationality($data['nationality']);
                $person->setOccupation($data['occupation']);
            }
            $person->setCountryOfResidence($data['country_of_residence']);
            $person->setDob($data['dob']);
            $this->director->setPerson($person);

            // set address
            if ($this->company->getServiceAddress() && $data['ourServiceAddress']) {
                $msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());
                $address = $this->director->getAddress();
                $address->setFields((array) $msgServiceAdress);
                //pr($address);exit;
                $this->director->setAddress($address);
            } else {
                $address = $this->director->getAddress();
                $address->setFields($data);
                $this->director->setAddress($address);
            }

            // residential address
            $address = $this->director->getResidentialAddress();
            $address->setFields($data, 'residential_');
            $this->director->setResidentialAddress($address);

            if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() == 'LLP') {
                $authetication = $this->director->getAuthentication();
                $authetication->setFields($data);
                $this->director->setAuthentication($authetication);
            }

            $this->director->setConsentToAct($data['consentToAct']);

            // save
            try {
                $this->director->save();
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage());
                $this->redirect();
            }

            $form->clean();
            if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() == 'LLP') {
                $this->flashMessage('Member has been updated');
            } else {
                $this->flashMessage('Director has been updated');
            }

            $this->redirect(self::DIRECTORS_PAGE, 'company_id=' . $this->company->getCompanyId());
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    /**********************************************************************************/
    /************************** DIRECTOR CORPORATE ************************************/
    /**********************************************************************************/

    /********************************************* add director corporate ***************************************************/

    protected function renderAddDirectorCorporate()
    {
        // prefill
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'corporate');
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];

        $form = $this->getComponentCorporateForm('add', $prefillOfficers, $prefillAddress);
        $this->template->form = $form;
        $this->template->companyId = $this->company->getCompanyId();

        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() == 'LLP') {
            $this->template->title = 'Add Corporate Member';
            $this->template->seoTitle = 'Add Corporate Member';
        }
    }

    /**
     * @param FForm $form
     */
    public function Form_addDirectorCorporateFormSubmitted(FForm $form)
    {
        try {
            $data = $form->getValues();

            /** @var IncorporationCorporateDirector $directorCorporate */
            $directorCorporate = $this->company->getLastIncorporationFormSubmission()->getForm()->getNewIncCorporate('dir');
            $corporate = $directorCorporate->getCorporate();
            $corporate->setFields($data);
            $directorCorporate->setCorporate($corporate);

            // address
            $address = $directorCorporate->getAddress();
            $address->setFields($data);
            $directorCorporate->setAddress($address);

            if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() == 'LLP') {
                // authentication
                $authetication = $directorCorporate->getAuthentication();
                $authetication->setFields($data);
                $directorCorporate->setAuthentication($authetication);
            }

            $directorCorporate->setConsentToAct($data['consentToAct']);

            // identification
            if ($data['eeaType'] == 1) {
                $identification = $directorCorporate->getIdentification(Identification::EEA);
            } else {
                $identification = $directorCorporate->getIdentification(Identification::NonEEA);
            }

            $identification->setFields($data);
            $directorCorporate->setIdentification($identification);
            $directorCorporate->save();

            $form->clean();
            if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() == 'LLP') {
                $this->flashMessage('Corporate member has been added');
            } else {
                $this->flashMessage("Corporate director has been added");
            }

            $this->redirect(self::DIRECTORS_PAGE, 'company_id=' . $this->company->getCompanyId());
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    /********************************************* edit director corporate ***************************************************/

    protected function prepareEditDirectorCorporate()
    {
        // check get
        if (!isset($this->get['director_id'])) {
            $this->redirect(self::DIRECTORS_PAGE, 'company_id=' . $this->company->getCompanyId());
        }

        // check existing director
        try {
            $this->director = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncCorporate(
                $this->get['director_id'],
                'dir'
            );
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect(
                self::DIRECTORS_PAGE,
                ['company_id=' . $this->company->getCompanyId(), 'director_id' => NULL]
            );
        }
    }

    protected function renderEditDirectorCorporate()
    {
        // prefill
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'corporate');
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];

        $form = $this->getComponentCorporateForm('edit', $prefillOfficers, $prefillAddress);
        $this->template->form = $form;
        $this->template->companyId = $this->company->getCompanyId();
        $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() == 'LLP') {
            $this->template->title = 'Amend Corporate Member';
            $this->template->seoTitle = 'Amend Corporate Member';
        }
    }

    /**
     * @param $form
     */
    public function Form_editDirectorCorporateFormSubmitted(CHForm $form)
    {
        try {
            $data = $form->getValues();

            $person = $this->director->getCorporate();
            $person->setFields($data);
            $this->director->setCorporate($person);

            // address
            $address = $this->director->getAddress();
            $address->setFields($data);
            $this->director->setAddress($address);

            if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() == 'LLP') {
                // authentication
                $authetication = $this->director->getAuthentication();
                $authetication->setFields($data);
                $this->director->setAuthentication($authetication);
            }

            $this->director->setConsentToAct($data['consentToAct']);

            // identification
            if ($data['eeaType'] == 1) {
                $identification = $this->director->getIdentification(Identification::EEA);
            } else {
                $identification = $this->director->getIdentification(Identification::NonEEA);
            }
            $identification->setFields($data);
            $this->director->setIdentification($identification);

            // save
            try {
                $this->director->save();
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage());
                $this->redirect();
            }

            $form->clean();
            if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() == 'LLP') {
                $this->flashMessage('Corporate member has been updated');
            } else {
                $this->flashMessage('Corporate director has been updated');
            }


            $this->redirect(self::DIRECTORS_PAGE, 'company_id=' . $this->company->getCompanyId());

        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    /***************************************** components ****************************************************/

    /**
     * Return form for adding/editin person director
     *
     * @param string $action
     * @param array $prefillOfficers
     * @param array $prefillAddress
     * @return FForm
     */
    public function getComponentPersonForm($action, $prefillOfficers, $prefillAddress)
    {
        // form
        if ($action == 'add') {
            $form = new CHForm('addDirectorPerson');
        } else {
            $form = new CHForm('editDirectorPerson');
        }

        // prefill
        $form->addFieldset('Prefill');
        $form->addSelect('prefillOfficers', 'Select an existing appointment to autocomplete these details', $prefillOfficers['select'])
            ->setFirstOption('--- Select --');

        // person
        $form->addFieldset('Person');
        $form->addSelect('title', 'Title', Person::$titles)
            ->setFirstOption('--- Select ---');
        $form->addText('forename', 'First name *')
            ->addRule(FForm::Required, 'Please provide first name')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "First name can't be more than 50 characters", 50);
        $form->addText('middle_name', 'Middle name')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "Middle name can't be more than 50 characters", 50)
            ->addRule('MinLength', "Please provide full middle name.", 2);
        $form->addText('surname', 'Last name *')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule(FForm::Required, 'Please provide last name')
            ->addRule('MaxLength', "Last name can't be more than 160 characters", 160);
        $form->add('DateSelect', 'dob', 'Date Of Birth *')
            ->addRule(FForm::Required, 'Please provide dob')
            ->addRule(
                [$this, 'Validator_personDOB'],
                ['DOB is not a valid date.', 'Director has to be older than %d years.'],
                16
            )
            ->setStartYear(1900)
            ->setEndYear(date('Y') - 1)
            ->setValue(date('Y-m-d', strtotime(date("Y-m-d", strtotime(date("Y-m-d"))) . " -15 year")));

        //llp member don't need this fields

        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() != 'LLP') {
            $form->addText('nationality', 'Nationality *')
                ->addRule(FForm::Required, 'Please provide nationality')
                ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL);
            $form->addText('occupation', 'Occupation *')
                ->addRule(FForm::Required, 'Please provide occupation')
                ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL);
        }
        $form->addText('country_of_residence', 'Country Of Residence *')
            ->addRule(FForm::Required, 'Please provide Country of Residence')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL);

        // if has registered office
        if ($this->company->getServiceAddress()) {
            // our registered office
            $form->addFieldset('Use Our Service Address service');

            $checkbox = $form->addCheckbox('ourServiceAddress', 'Service Address service  ', 1);

            //check if service address was set for director
            if (isset($this->director)) {
                $fields = $this->director->getFields();
                $msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());
                if ($fields['postcode'] == $msgServiceAdress->postcode) {
                    $checkbox->setValue(1);
                }
            }
        }


        // prefill
        $form->addFieldset('Prefill');
        $form->addSelect('prefillAddress', 'Prefill Address', $prefillAddress['select'])
            ->setFirstOption('--- Select ---');

        // adddress
        $form->addFieldset('Address');
        $form->addText('premise', 'Building name/number *')
            ->addRule([$this, 'Validator_requiredAddress'], 'Please provide Building name/number')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "Building name/number can't be more than 50 characters", 50);
        $form->addText('street', 'Street *')
            ->addRule([$this, 'Validator_requiredAddress'], 'Please provide Street')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "Street can't be more than 50 characters", 50);
        $form->addText('thoroughfare', 'Address 3')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "Address 3 can't be more than 50 characters", 50);
        $form->addText('post_town', 'Town *')
            ->addRule([$this, 'Validator_requiredAddress'], 'Please provide Town')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "Town can't be more than 50 characters", 50);
        $form->addText('county', 'County')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "County can't be more than 50 characters", 50);
        $form->addText('postcode', 'Postcode *')
            ->addRule(
                [$this, 'Validator_serviceAddressPostCode'],
                'You cannot use our postcode for this address without first purchasing the '
                . Html::el('a')
                    ->setText('Service Address Service')
                    ->href(FApplication::$router->link(476))
                    ->render() . '. NB. This is different to the registered office service.'
            )
            ->addRule([$this, 'Validator_requiredAddress'], 'Please provide Postcode')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "Postcode can't be more than 15 characters", 15);
        $form->addSelect('country', 'Country *', Address::$countries)
            ->addRule([$this, 'Validator_requiredAddress'], 'Please provide Country');

        // residential address
        $form->addFieldset('Residential Address');

        if ($action == 'add') {
            $form->addCheckbox('residentialAddress', 'Different address: ', 1);
        }

        $form->addText('residential_premise', 'Building name/number *')
            ->addRule([$this, 'Validator_requiredServiceAddress'], 'Please provide Building name/number', $action)
            ->addRule('MaxLength', "Building name/number can't be more than 50 characters", 50)
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL);
        $form->addText('residential_street', 'Street *')
            ->addRule([$this, 'Validator_requiredServiceAddress'], 'Please provide street', $action)
            ->addRule('MaxLength', "Street can't be more than 50 characters", 50)
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL);
        $form->addText('residential_thoroughfare', 'Address 3')
            ->addRule('MaxLength', "Address 3 can't be more than 50 characters", 50)
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL);
        $form->addText('residential_post_town', 'Town *')
            ->addRule([$this, 'Validator_requiredServiceAddress'], 'Please provide Town', $action)
            ->addRule('MaxLength', "Town can't be more than 50 characters", 50)
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL);
        $form->addText('residential_county', 'County')
            ->addRule('MaxLength', "County can't be more than 50 characters", 50)
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL);
        $form->addText('residential_postcode', 'Postcode *')
            ->addRule([$this, 'Validator_requiredServiceAddress'], 'Please provide Postcode', $action)
            ->addRule('MaxLength', "Postcode can't be more than 15 characters", 15)
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule(
                FForm::REGEXP,
                'You cannot use our postcode for residential address',
                ['#(ec1v 4pw|ec1v4pw)#i', TRUE]
            );
        $form->addSelect('residential_country', 'Country *', Address::$countries)
            ->addRule([$this, 'Validator_requiredServiceAddress'], 'Please provide Country', $action)
            ->addRule('MaxLength', "Country can't be more than 50 characters", 50)
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL);

        // security
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() == 'LLP') {
            $form->addFieldset('Security');
            $form->addText('BIRTOWN', 'First three letters of Town of birth *')
                ->size(3)
                ->addRule(FForm::Required, 'Please provide Town of birth')
                ->addRule(FForm::REGEXP, 'Please enter only 3 letters for Town of birth', '#^[A-Z]{3,3}$#i');
            $form->addText('TEL', 'Last three digits of Telephone number *')
                ->size(3)
                ->addRule(FForm::Required, 'Please provide Telephone number')
                ->addRule(FForm::REGEXP, 'Please enter only 3 numeric characters for Telephone number', '#^\d{3,3}$#');
            $form->addText('EYE', 'First three letters of Eye colour *')
                ->size(3)
                ->addRule(FForm::Required, 'Please provide Eye colour')
                ->addRule(FForm::REGEXP, 'Please enter only 3 letters for Eye colour', '#^[A-Z]{3,3}$#i')
                ->addRule(
                    [$this, 'Validator_securityEyeColour'],
                    'Eye colour cannot be black. Please enter a valid eye colour (eg. brown, blue, green etc.)'
                );
        }

        $form->addFieldset('Consent to act');
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() == 'LLP') {
            $consentLabel = 'The members confirm that the person named has consented to act as a designated member';
        } else {
            $consentLabel = 'The subscribers (shareholders) confirm that the person named has consented to act as a director';
        }
        $form->add('SingleCheckbox', 'consentToAct', $consentLabel)
            ->addRule(FForm::Required, 'Consent to act is required');

        $form->addFieldset('Action');

        // diff callback function
        if ($action == 'add') {
            $form->addSubmit('continue', 'Continue >')->class('btn_submit fright mbottom');
            $form->onValid = [$this, 'Form_addDirectorPersonFormSubmitted'];
        } else {
            $form->onValid = [$this, 'Form_editDirectorPersonFormSubmitted'];
            $form->addSubmit('continue', 'Save')->class('btn_submit fright mbottom');

            $fields = $this->director->getFields();
            unset($fields['consentToAct']);
            $form->setInitValues($fields);
        }

        $form->start();

        return $form;
    }

    /**
     * Return form for adding/editing corporate director
     *
     * @param string $action
     * @param array $prefillOfficers
     * @param array $prefillAddress
     * @return FForm
     */
    public function getComponentCorporateForm($action, $prefillOfficers, $prefillAddress)
    {
        // form
        if ($action == 'add') {
            $form = new CHForm('addDirectorCorporate');
        } else {
            $form = new CHForm('editDirectorCorporate');
        }

        // prefill
        $form->addFieldset('Prefill');
        $form->addSelect('prefillOfficers', 'Select an existing appointment to autocomplete these details', $prefillOfficers['select'])
            ->setFirstOption('--- Select --');

        // person
        $form->addFieldset('Corporate');
        $form->addText('corporate_name', 'Company name *')
            ->addRule(FForm::Required, 'Please provide first name')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL);
        $form->addText('forename', 'First name *')
            ->addRule(FForm::Required, 'Please provide first name')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "First name can't be more than 50 characters", 50);
        $form->addText('surname', 'Last name *')
            ->addRule(FForm::Required, 'Please provide last name')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "Last name can't be more than 160 characters", 160);

        // prefill address
        $form->addFieldset('Prefill');
        $form->addSelect('prefillAddress', 'Prefill Address', $prefillAddress['select'])
            ->setFirstOption('--- Select ---');

        // address
        $form->addFieldset('Address');
        $form->addText('premise', 'Building name/number *')
            ->addRule(FForm::Required, 'Please provide Building name/number')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "Building name/number can't be more than 50 characters", 50);
        $form->addText('street', 'Street *')
            ->addRule(FForm::Required, 'Please provide Street')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "Street can't be more than 50 characters", 50);
        $form->addText('thoroughfare', 'Address 3')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "Address 3 can't be more than 50 characters", 50);
        $form->addText('post_town', 'Town *')
            ->addRule(FForm::Required, 'Please provide Town')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "Town can't be more than 50 characters", 50);
        $form->addText('county', 'County')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule('MaxLength', "County can't be more than 50 characters", 50);
        $form->addText('postcode', 'Postcode *')
            ->addRule(['EnglishKeyboardValidator', 'isValid'], NULL)
            ->addRule(
                [$this, 'Validator_serviceAddressPostCode'],
                'You cannot use our postcode for this address without first purchasing the '
                . Html::el('a')->setText('Service Address Service')->href(FApplication::$router->link(476))->render()
                . '. NB. This is different to the registered office service.'
            )
            ->addRule(FForm::Required, 'Please provide Postcode')
            ->addRule('MaxLength', "Postcode can't be more than 15 characters", 15);
        $form->addSelect('country', 'Country *', Address::$countries)
            ->addRule(FForm::Required, 'Please provide Country');

        // authenfication
        $form->addFieldset('EEA/ Non EEA');
        $form->addRadio('eeaType', 'Type *', [1 => 'EEA', 2 => 'Non EEA'])
            ->addRule(FForm::Required, 'Please provide EEA type!');
        $form->addText('place_registered', 'Country Registered *')
            ->addRule([$this, 'Validator_eeaRequired'], 'Please provide Country registered!');
        $form->addText('registration_number', 'Registration number *')
            ->addRule([$this, 'Validator_eeaRequired'], 'Please provide Registration number!');
        $form->addText('law_governed', 'Governing law *')
            ->addRule([$this, 'Validator_eeaRequired'], 'Please provide Governing law!');
        $form->addText('legal_form', 'Legal Form *')
            ->addRule([$this, 'Validator_eeaRequired'], 'Please provide Legal form!');

        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() == 'LLP') {
            $form->addFieldset('Security');
            $form->addText('BIRTOWN', 'First three letters of Town of birth *')
                ->size(3)
                ->addRule(FForm::Required, 'Please provide Town of birth')
                ->addRule(FForm::REGEXP, 'Please enter only 3 letters for Town of birth', '#^[A-Z]{3,3}$#i');
            $form->addText('TEL', 'Last three digits of Telephone number *')
                ->size(3)
                ->addRule(FForm::Required, 'Please provide Telephone number')
                ->addRule(FForm::REGEXP, 'Please enter only 3 numeric characters for Telephone number', '#^\d{3,3}$#');
            $form->addText('EYE', 'First three letters of Eye colour *')
                ->size(3)
                ->addRule(FForm::Required, 'Please provide Eye colour')
                ->addRule(FForm::REGEXP, 'Please enter only 3 letters for Eye colour', '#^[A-Z]{3,3}$#i')
                ->addRule(
                    [$this, 'Validator_securityEyeColour'],
                    'Eye colour cannot be black. Please enter a valid eye colour (eg. brown, blue, green etc.)'
                );
        }

        $form->addFieldset('Consent to act');
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() == 'LLP') {
            $consentLabel = 'The members confirm that the corporate body named has consented to act as a designated member';
        } else {
            $consentLabel = 'The subscribers (shareholders) confirm that the corporate body named has consented to act as a director';
        }
        $form->add('SingleCheckbox', 'consentToAct', $consentLabel)
            ->addRule(FForm::Required, 'Consent to act is required');

        $form->addFieldset('Action');

        if ($action == 'add') {
            $form->addSubmit('continue', 'Continue >')->class('btn_submit fright mbottom');
            $form->onValid = [$this, 'Form_addDirectorCorporateFormSubmitted'];
        } else {
            $form->addSubmit('continue', 'Save')->class('btn_submit fright mbottom');
            $form->onValid = [$this, 'Form_editDirectorCorporateFormSubmitted'];

            $fields = $this->director->getFields();
            unset($fields['consentToAct']);
            $form->setInitValues($fields);
        }

        $form->start();

        return $form;
    }

    /**
     * Returns list of persons
     * @return array
     */
    private function getComponentPersons()
    {
        /** @var IncorporationPersonDirector[] $persons */
        $persons = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncPersons(['dir']);

        // persons
        foreach ($persons as $key => &$person) {
            if ($person->isNominee() == TRUE) {
                unset($persons[$key]);
                continue;
            }
            $person = [
                'fullName' => $person->getPerson()->getForename() . ' ' .
                    $person->getPerson()->getMiddleName() . ' ' .
                    $person->getPerson()->getSurname(),
                'editLink' => $this->router->link(
                    self::EDIT_DIRECTOR_PERSON_PAGE,
                    'company_id=' . $this->company->getCompanyId(),
                    'director_id=' . $person->getId()
                ),
                'deleteLink' => $this->router->link(
                    self::DIRECTORS_PAGE,
                    'company_id=' . $this->company->getCompanyId(),
                    'delete_person_id=' . $person->getId()
                )
            ];
        }
        unset($person);

        return $persons;
    }

    /**
     * Returns list of persons
     * @return array
     */
    private function getComponentCorporates()
    {
        /** @var IncorporationCorporateDirector[] $corporates */
        $corporates = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncCorporates(['dir']);

        // corporates
        foreach ($corporates as $key => &$corporate) {
            if ($corporate->isNominee() == TRUE) {
                unset($corporates[$key]);
                continue;
            }
            $corporate = [
                'fullName' => $corporate->getCorporate()->getCorporateName(),
                'editLink' => $this->router->link(
                    self::EDIT_DIRECTOR_CORPORATE_PAGE,
                    'company_id=' . $this->company->getCompanyId(),
                    'director_id=' . $corporate->getId()
                ),
                'deleteLink' => $this->router->link(
                    self::DIRECTORS_PAGE,
                    'company_id=' . $this->company->getCompanyId(),
                    'delete_corporate_id=' . $corporate->getId()
                )
            ];
        }
        unset($corporate);

        return $corporates;
    }

    /**
     * Returns list of persons
     * @return array
     */
    private function getComponentNominees()
    {
        $persons = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncPersons(['dir']);
        $corporates = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncCorporates(['dir']);
        $directors = array_merge($persons, $corporates);

        // corporates
        foreach ($directors as $key => &$director) {
            if (!$this->isNomineeDirector($director)) {
                unset($directors[$key]);
                continue;
            }

            if ($director instanceof IncorporationPersonDirector) {
                $director = [
                    'fullName' => $director->getPerson()->getForename() . ' ' . $director->getPerson()->getSurname()
                ];
            } else {
                $director = ['fullName' => $director->getCorporate()->getCorporateName()];
            }
        }
        unset($director);

        return $directors;
    }

    /**
     * @return array
     */
    private function getComponentResidents()
    {
        $persons = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncPersons(['dir']);
        $corporates = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncCorporates(['dir']);
        $directors = array_merge($persons, $corporates);

        foreach ($directors as $key => &$director) {
            if (!$this->isResidentDirector($director)) {
                unset($directors[$key]);
                continue;
            }

            if ($director instanceof IncorporationPersonDirector) {
                $director = [
                    'fullName' => $director->getPerson()->getForename() . ' ' . $director->getPerson()->getSurname()
                ];
            } else {
                $director = ['fullName' => $director->getCorporate()->getCorporateName()];
            }
        }
        unset($director);

        return $directors;
    }

    /**
     * @param IncorporationCorporateDirector|IncorporationPersonDirector $director
     * @return bool
     */
    private function isNomineeDirector($director)
    {
        //NULL for backwards compatibility, the previous incorporations will not have no nominee type.
        return $director->isNominee() && (in_array($director->getNomineeType(), [NomineeDirector::$nomineeType, NULL]));
    }

    /**
     * @param IncorporationCorporateDirector|IncorporationPersonDirector $director
     * @return bool
     */
    private function isResidentDirector($director)
    {
        return $director->isNominee() && $director->getNomineeType() == ResidentDirector::$nomineeType;
    }

    /**
     * @param NomineeDirector $nomineeDirector
     * @return FForm
     */
    private function getComponentNomineeDirectorForm(NomineeDirector $nomineeDirector)
    {
        $nomineeDirectorTitle = $nomineeDirector->getLngTitle();

        $form = new FForm('nomineeDirector');
        $form->addFieldset($nomineeDirectorTitle);
        $form->addCheckbox('nominee', "Use {$nomineeDirectorTitle} ", 1);
        $form->addSubmit('update', 'Update')->class('btn_submit fright mbottom');
        $form->onValid = [$this, 'Form_DirectorsFormSubmitted'];

        if ($this->submissionContainsNomineeDirector()) {
            $form['nominee']->setValue(1);
        }

        $form->start();

        return $form;
    }

    /***************************************** validators ****************************************************/

    /**
     * @param $control
     * @param $error
     * @param $params
     * @return bool
     */
    public function Validator_requiredServiceAddress($control, $error, $params)
    {
        $value = $control->getValue();
        if ($params == 'add') {
            if ($control->owner['residentialAddress']->getValue() == 1 && empty($value)
                || (strstr(strtoupper($control->owner['postcode']->getValue()), 'EC1V4PW')
                    || strstr(strtoupper($control->owner['postcode']->getValue()), 'EC1V 4PW'))
                && empty($value)
            ) {
                return $error;
            }
        } else {
            if (empty($value)) {
                return $error;
            }
        }

        return TRUE;
    }

    /**
     * @param FControl $control
     * @param string $error
     * @return bool
     */
    public function Validator_serviceAddress($control, $error)
    {
        if (strstr(strtoupper($control->owner['postcode']->getValue()), 'EC1V4PW')
            || strstr(strtoupper($control->owner['postcode']->getValue()), 'EC1V 4PW')
        ) {
            $registeredOfficeId = $this->company->getRegisteredOfficeId();
            if (empty($registeredOfficeId)) {
                return $error;
            }
        }

        return TRUE;
    }

    /**
     * @return bool
     */
    private function submissionContainsNomineeDirector()
    {
        return $this->company->getLastIncorporationFormSubmission()->getForm()->hasNomineeDirector();
    }
}
