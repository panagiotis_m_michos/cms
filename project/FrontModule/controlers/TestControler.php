<?php

class TestControler extends BaseControler
{

    public function prepareDefault()
    {
        //        if (isset($this->get['hash']) && $this->get['hash'] == '6a256241ea0b7a305ba21473a7667543') {
        //            $result = dibi::select('*')->from(TBL_TAX_ASSIST_LEAD_TRACKING)
        //                ->where('leadId>%i', 3701)
        //                ->execute()
        //                ->fetchAll();
        //
        //            try {
        //                $taxAssistModel = $this->getService(KeyLocator::MODEL_TAX_ASSIST_LEAD);
        //            } catch (Exception $exc) {
        //                echo 'wrong credentials';
        //                exit;
        //            }
        //
        //            foreach ($result as $record) {
        //                $taxAssistModel->saveTALead(TaxAssistLead::TYPE_ANNUAL_ACCOUNTS, $record->email, $record->name, $record->phone, $record->postcode, $record->companyName);
        //                echo 'Submited' . $record->leadId . '<br>';
        //            }
        //        }
    }

    public function sendPollingErrorMessageEmail($error)
    {
        $replace = array();
        $replace['[DATE]'] = date('d-m-Y, H:i:s');
        $replace['[MESSAGE]'] = $error;

        $email = $email = new FEmail(1127);
        $email->addTo(TEST_EMAIL);
        $email->replacePlaceHolders($replace);
        $email->send();
    }

    public function showReportStats($ids, $response)
    {
        $x = json_encode($ids);
        $y = json_encode($response);

        $xcount = count($ids);
        $ycount = count($response);

        $html = "Form submission ids : {$x} <br /> Nr of form submission: {$xcount} <br /> Response: {$y} <br /> Nr of response: {$ycount}";
        return $html;
    }

    public function checkSendIncorporationEmail()
    {




        $customer = new Customer(86206);
        $companyName = 'MILLENNIUM STADIUM PLC';
        $companyId = 110914;

        // this company form submision id
        $incorporationFormSubmissionId = 125503;

        //incorporation document
        $IncorporationDocumenName = "IncorporationCertificate.pdf";
        $IncorporationDocumentpatdh = CHFiling::getDocPath() . '/' . CHFiling::getDirName($companyId) . "/company-$companyId/documents/";

        $data = array();
        $data['formSubmissionId'] = $incorporationFormSubmissionId;
        $data['requestKey'] = 'sdjklfjgfksdghf8347y597834yolhbsdfuo';
        $data['incorporationDate'] = date('Y-m-d');
        $data['authenticationCode'] = 'TESTAU';
        $data['companyNumber'] = '03176906';
        $data['attachmentPath'] = NULL;
        //$data['attachmentPath']      = $IncorporationDocumentpatdh.$IncorporationDocumenName;
        $data['companyName'] = $companyName;
        $data['companyId'] = $companyId;

        $this->sendIncEmail($customer, $data);
    }

    private function sendIncEmail(Customer $customer, array $data)
    {
        $replace = $this->replaceIncEmailplaceholders($customer, $data);
        $email = new FEmail(PollingModel::ACCEPTED_EMAIL);
        $email->addTo($customer->email);
        $email->addTo(TEST_EMAIL);
        $email->replacePlaceHolders($replace);
        $email->addAttachment($data['attachmentPath']);
        $email->send();
    }

    private function replaceIncEmailplaceholders(Customer $customer, $data)
    {
        $replace = array();
        $replace['[FIRSTNAME]'] = $customer->firstName;
        $replace['[COMPANY_NAME]'] = $data['companyName'];
        $replace['[COMPANY_ID]'] = $data['companyId'];
        $replace['[COMPANY_NUMBER]'] = isset($data['companyNumber']) ? $data['companyNumber'] : 0;
        if (isset($data['incorporationDate'])) {
            $replace['[DATE]'] = $data['incorporationDate'];
        }

        return $replace;
    }

    private function checkRemovedBarclaysByCompany($companyId, $productId)
    {
        $company = Company::getCompany($companyId);
        $customer = new Customer($company->getCustomerId());
        $orderId = $company->getOrderId();
        foreach (OrderItem::getOrderItems($orderId) as $key => $item) {
            if ($item->productId == $productId) {
                $i = 1;
            }
        }
        foreach (OrderItem::getOrderItems($orderId) as $key => $item) {
            if ($i != 1 && !$customer->isWholesale() && ($item->productId == 109 || $item->productId == 110 || $item->productId == 111 || $item->productId == 112 || $item->productId == 113 || $item->productId == 279)) {
                return TRUE;
            }
        }
        return FALSE;
    }

    private function customersFrom2010()
    {
        Debug::disableProfiler();
        Debug::enable(Debug::DEVELOPMENT);

        $customers = dibi::select('email, industryId')->from(TBL_CUSTOMERS)
                ->where('DATE_FORMAT(`dtc`,"%Y") = %s', '2010')
                ->and('industryId IS NOT NULL')
                ->orderBy('dtc')->desc()
                ->execute()->fetchAll();
        $new = array();
        foreach ($customers as $customer) {
            $industries = Customer::$industries;
            $customer['industry'] = isset($industries[$customer['industryId']]) ? $industries[$customer['industryId']] : NULL;
            $new[] = (array) $customer;
        }

        $header = array_keys((array) current($new));
        array_unshift($new, $header);
        Array2Csv::output($new, 'Customers2010.csv');
    }

    private function companiesFormedWithin30Days()
    {
        $customers = dibi::select('co.company_name, co.accepted_date, c.firstName, c.lastName, c.email, c.roleId')
            ->from('ch_company', 'co')
            ->leftJoin(TBL_CUSTOMERS, 'c')
            ->on('co.customer_id=c.customerId')
            ->where('DATE_SUB(CURDATE(), INTERVAL 30 DAY) <= accepted_date')
            ->orderBy('co.accepted_date')->asc()
            ->execute()
            ->fetchAll();

        $header = array_keys((array) current($customers));
        array_unshift($customers, $header);
        Array2Csv::output($customers, 'customersCompany30Days.csv');
    }

    private function companyNameSearchJSON()
    {
        Debug::disableProfiler();

        try {
            // --- check for g.companyName ---
            if (!isset($this->get['companyName']))
                throw new Exception("Missing company name parameter");

            // --- check if company name is avaiable
            $companyName = $this->get['companyName'];
            $available = CompanySearch::isAvailable($companyName);
            if ($available) {
                $arr = array(
                    'error' => FALSE,
                    'companyNameAvailable' => TRUE,
                    'errorMessage' => NULL
                );
            } else {
                $arr = array(
                    'error' => FALSE,
                    'companyNameAvailable' => FALSE,
                    'errorMessage' => NULL
                );
            }
        } catch (Exception $e) {
            $arr = array(
                'error' => TRUE,
                'companyNameAvailable' => FALSE,
                'errorMessage' => $e->getMessage()
            );
        }

        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        $json = json_encode($arr);
        $this->terminate($json);
    }

    private function wholesalersWithLessThanFiveCompaniesCsv()
    {
        $customers = dibi::select('customerId')->select('firstName')->select('lastName')->select('email')
                ->from(TBL_CUSTOMERS)
                ->where('roleId=%s', Customer::ROLE_WHOLESALE)
                ->execute()->fetchAll();
        foreach ($customers as &$customer) {
            $customer['companiesCount'] = dibi::select('COUNT(company_id)')->from('ch_company')->where('customer_id=%i', $customer->customerId)->execute()->fetchSingle();
        }
        unset($customer);

        Debug::disableProfiler();
        Array2Csv::output($customers);
    }

    private function oneCorporateDirectorCSV()
    {
        $result = dibi::select('cm.company_id')
            ->select('cm.corporate_name')
            ->select('c.company_name')
            ->select('cm.corporate')
            ->select('COUNT(*)')->as('pocet')
            ->from('ch_company_member', 'cm')
            ->leftJoin('ch_company', 'c')
            ->on('c.company_id=cm.company_id')
            ->where('type=%s', 'DIR')
            ->groupBy('cm.company_id')
            ->having('pocet=1')
            ->execute()
            ->fetchAll();


        $final = array();
        $final[] = array('company_id', 'corporate_name', 'company_name', 'corporate', 'count');
        foreach ($result as $row) {
            if ($row->corporate) {
                $final[] = $row;
            }
        }

        Debug::disableProfiler();
        Array2Csv::output($final);
    }

    private function barclays()
    {
        $ids = dibi::select('company_id')
            ->from('ch_barclays')
            ->where('`date_sent` LIKE %s', '2010-08-26%')
            ->and('success=0')
            ->execute()
            ->fetchPairs();

        foreach ($ids as $id) {
            try {
                pr($id);
                Barclays::sendCompany($id, FALSE, TRUE, TRUE);
            } catch (Exception $e) {
                pr($e->getMessage());
            }
        }
    }

    public static function responsePaypalPayment()
    {

        $paypal = 1;
        //       $sage = 6;

        $paymentResponse = new PaymentResponse();
        $paymentResponse->setTypeId(Transaction::TYPE_PAYPAL);
        $paymentResponse->setPaypalTransactionId('EC7BB71387UB9127726');
        $paymentResponse->setPaymentTransactionTime(date("Ymd"));
        $paymentResponse->setPaymentHolderEmail('diviak_1234458372_per@gmail.com');
        $paymentResponse->setPaymentHolderFirstName('Tomas');
        $paymentResponse->setPaymentHolderLastName('Test');
        $paymentResponse->setPaymentHolderAddressStreet('8 durham road');
        $paymentResponse->setPaymentHolderAddressCity('London');
        $paymentResponse->setPaymentHolderAddressState(NULL);
        $paymentResponse->setPaymentHolderAddressPostCode('HA2 6PQ');
        $paymentResponse->setPaymentHolderAddressCountry('GBR');
        $paymentResponse->setPaymentTransactionDetails(serialize('Test'));

        return $paymentResponse;
    }

    public function beforeRender()
    {
        $this->terminate('done');
    }

}
