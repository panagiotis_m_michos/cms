<?php

class BusinessBankingControler extends DefaultControler
{
    const FAST_TRACKING_PAGE = 447;
    const HSBC_BANKING_PAGE = 448;
    const NATWEST_BANKING_PAGE = 450;
    const ABBEY_BANKING_PAGE = 449;

    public $possibleActions = array(
        self::FAST_TRACKING_PAGE => 'default',
        self::HSBC_BANKING_PAGE => 'HsbcBanking',
        self::NATWEST_BANKING_PAGE => 'NatWestBanking',
        self::ABBEY_BANKING_PAGE => 'AbbeyBanking'
    );

    
    public function beforePrepare()
    {
        parent::beforePrepare();
    }

    
    //============= HSBC =================//

    public function handleHsbcBanking()
    {
        $form = new FForm('hsbc_form');

        $form->addFieldset('HSBC Banking Query Form');
        $form->addText('customer_name', "Name: *")
            ->addRule(array($this, 'Validator_requiredFields'), "Name is required!");
        $form->addText('customer_email', "Email: *")
            ->addRule(array($this, 'Validator_requiredFields'), "Email is required!");
        $form->addText('customer_phone', "Phone: *")
            ->addRule(array($this, 'Validator_requiredFields'), "Phone is required!");
        $form->addText('customer_address1', "Address 1: *")
            ->addRule(array($this, 'Validator_requiredFields'), "Address 1 is required!");
        $form->addText('customer_address2', "Address 2: ");
        $form->addText('customer_town', "Town: *")
            ->addRule(array($this, 'Validator_requiredFields'), "Town is required!");
        $form->addText('customer_postcode', "Postcode: *")
            ->addRule(array($this, 'Validator_requiredFields'), "Postcode is required!");
        $form->addText('company_name', "Company Name: *")
            ->addRule(array($this, 'Validator_requiredFields'), "Company name is required!");

        $form->addSubmit('submit', 'Send')->class('btn_submit');

        $form->onValid = array($this, 'Form_HsbcFormSubmitted');
        $form->start();
        $this->template->form = $form;
    }

    
    public function Form_HsbcFormSubmitted($form)
    {
        // get data from form
        $data = $form->getValues();
        $businessBanking = $this->emailerFactory->get(EmailerFactory::BUSINESS_BANKING);
        $businessBanking->hsbcEmail($data);
        $this->flashMessage("Your query has been sent succesfully.");
        $form->clean();
        $this->redirect();
    }

    //============= NatWest =================//

    public function handleNatWestBanking()
    {
        $form = new FForm('natwest_form');

        $form->addFieldset('NatWest Banking Query Form');
        $form->addText('customer_name', "Name: *")
            ->addRule(array($this, 'Validator_requiredFields'), "Name is required!");
        $form->addText('customer_email', "Email: *")
            ->addRule(array($this, 'Validator_requiredFields'), "Email is required!");
        $form->addText('customer_phone', "Phone: *")
            ->addRule(array($this, 'Validator_requiredFields'), "Phone is required!");
        $form->addText('customer_address1', "Address 1: *")
            ->addRule(array($this, 'Validator_requiredFields'), "Address 1 is required!");
        $form->addText('customer_address2', "Address 2: ");
        $form->addText('customer_town', "Town: *")
            ->addRule(array($this, 'Validator_requiredFields'), "Town is required!");
        $form->addText('customer_postcode', "Postcode: *")
            ->addRule(array($this, 'Validator_requiredFields'), "Postcode is required!");
        $form->addText('company_name', "Company Name: *")
            ->addRule(array($this, 'Validator_requiredFields'), "Company name is required!");

        $form->addSubmit('submit', 'Send')->class('btn_submit');

        $form->onValid = array($this, 'Form_NatWestFormSubmitted');
        $form->start();
        $this->template->form = $form;
    }

    
    public function Form_NatWestFormSubmitted($form)
    {
        // get data from form
        $data = $form->getValues();
        $businessBanking = $this->emailerFactory->get(EmailerFactory::BUSINESS_BANKING);
        $businessBanking->natwestEmail($data);
        $this->flashMessage("Your query has been sent succesfully.");
        $form->clean();
        $this->redirect();
    }

    
    //============= ABBEY =================//

    public function handleAbbeyBanking()
    {
        $form = new FForm('abbey_form');

        $form->addFieldset('Abbey Banking Query Form');
        $form->addText('customer_name', "Name: *")
            ->addRule(array($this, 'Validator_requiredFields'), "Name is required!");
        $form->addText('customer_email', "Email: *")
            ->addRule(array($this, 'Validator_requiredFields'), "Email is required!");
        $form->addText('customer_phone', "Phone: *")
            ->addRule(array($this, 'Validator_requiredFields'), "Phone is required!");
        $form->addText('company_name', "Company Name: *")
            ->addRule(array($this, 'Validator_requiredFields'), "Company name is required!");

        $form->addSubmit('submit', 'Send')->class('btn_submit');

        $form->onValid = array($this, 'Form_AbbeyFormSubmitted');
        $form->start();
        $this->template->form = $form;
    }

    
    public function Form_AbbeyFormSubmitted($form)
    {
        // get data from form
        $data = $form->getValues();
        $businessBanking = $this->emailerFactory->get(EmailerFactory::BUSINESS_BANKING);
        $businessBanking->abbeyEmail($data);
        $this->flashMessage("Your query has been sent succesfully.");
        $form->clean();
        $this->redirect();
    }

    //===================================== Validators =========================================//

    /**
     * Check required fields fo card payment
     *
     * @param object $control
     * @param string $error
     * @param array $params
     * @return mixed
     */
    public function Validator_requiredFields($control, $error, $params)
    {
        if ($control->owner->isSubmitedBy('submit') && !isset($control->owner['credit']) || $control->owner->isSubmitedBy('submit') && isset($control->owner['credit']) && $control->owner['credit']->getValue() != 1) {
            $value = $control->getValue();
            if (empty($value)) {
                return $error;
            }
        }
        return TRUE;
    }

}
