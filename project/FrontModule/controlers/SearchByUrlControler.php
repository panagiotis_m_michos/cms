<?php

class SearchByUrlControler extends DefaultControler
{
	const COMPANY_LIST_PAGE = 583;
	
	const COMPANY_DETAILS_PAGE = 587;
	
	public function handleDefault()
	{	
				
		//debug::enable(FALSE);		
		
		$alphabet = array('A','B','C','D','E','F','G','H','I','J','K','L','M',
                     'N','O','P','Q','R','S','T','U','V','W','X','Y','Z');		
		foreach ($alphabet as $value)
		{
			foreach($alphabet as $val)
			{
				$alphabetLinks[$value][$val] = $value.$val; 
			}
		}					
		$this->template->alphabetLinks = $alphabetLinks;
		
		/*
		//----------------------- RSS FEED --------------------//
		if($xml = simplexml_load_file('https://www.companiesmadesimple.com/blog/SyndicationService.asmx/GetRss'))
		{				
   			$result["title"]   		= $xml->xpath("/rss/channel/item/title");
       		$result["link"]    		= $xml->xpath("/rss/channel/item/link");
       		$result["description"]  = $xml->xpath("/rss/channel/item/description");        		
				        		
       		foreach($result as $key => $attribute)
       		{
       			$i=0;
       			foreach($attribute as $element) 
           		{
           			if($key == "description")
           			{
           				$str = strip_tags((string)$element);
           				$str = preg_split("/[.]+/", $str);            				       				
           				$ret[$i][$key] = $str[0].'...';
           			}
           			else
           			{
           				$ret[$i][$key] = (string)$element;
           			}
           			$i++;
           			if($i==3) break;            			
           		}            		
	       	}
		}
		*/
		$ret = array();
		$this->template->rssfeed = $ret;	
		//------------------------- END RSS FEED -------------------------//		
		
		if(isset($_GET['q']) && $_GET['q']!='')
		{		
			
			if(isset($_GET['next']))
			{				
				$searchByUrl = new SearchByUrl();
				$searchByUrl->setXmlGateway();
				try{
					if(!isset($_SESSION['continuationKey']))
					{
						$_SESSION['continuationKey'] = $_GET['next'];
					}
					$response = $searchByUrl->searchNextByName($_GET['q'], 100, $_SESSION['continuationKey']);
				}
				catch (Exception $e)
				{
					if(isset($_GET['try']))
					{
						$this->flashMessage("Server temperory unavailable. Please try later.");
					}
					else
					{
						$url = $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'&try=1';
						$this->redirect($url);
					}
				}
				//$response = $searchByUrl->searchNextByName($_GET['q'], 100, $_SESSION['continuationKey']);
				$array = SearchByUrl::list2Array($response);			
				$_SESSION['continuationKey'] = SearchByUrl::getContinuationKey($response);
				$_SESSION['regressionKey'] = SearchByUrl::getRegressionKey($response);
				$nextLink = '&next='.SearchByUrl::getContinuationKey($response);
				$previousLink = '&previous='.SearchByUrl::getRegressionKey($response);
				$this->template->letter = $_GET['q'];				
				$this->template->nextLink = $nextLink;
				$this->template->previousLink = $previousLink;		
				$this->template->list = $array;				
				
			}
			elseif(isset($_GET['previous']))
			{
								
				$searchByUrl = new SearchByUrl();
				$searchByUrl->setXmlGateway();
				try{
					if(!isset($_SESSION['regressionKey']))
					{
						$_SESSION['regressionKey'] = $_GET['previous'];
					}
					$response = $searchByUrl->searchPreviousByName($_GET['q'], 100, $_SESSION['regressionKey']);
				}
				catch (Exception $e)
				{
					if(isset($_GET['try']))
					{
						$this->flashMessage("Server temperory unavailable. Please try later.");
					}
					else
					{
						$url = $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'&try=1';
						$this->redirect($url);
					}
				}
				//$response = $searchByUrl->searchPreviousByName($_GET['q'], 100, $_SESSION['regressionKey']);				
				$array = SearchByUrl::list2Array($response);			
				$_SESSION['continuationKey'] = SearchByUrl::getContinuationKey($response);
				$_SESSION['regressionKey'] = SearchByUrl::getRegressionKey($response);
				$nextLink = '&next='.SearchByUrl::getContinuationKey($response);
				$previousLink = '&previous='.SearchByUrl::getRegressionKey($response);
				$this->template->letter = $_GET['q'];
				$this->template->nextLink = $nextLink;
				$this->template->previousLink = $previousLink;		
				$this->template->list = $array;
								
			}
			else
			{
				$q = $_GET['q'];				
				if($q == 'CO/')
				{
					$q = '@C@O';				
				}
				$searchByUrl = new SearchByUrl();
				$searchByUrl->setXmlGateway();
				try{
					$response = $searchByUrl->searchList($q, 100);
				}
				catch (Exception $e)
				{
					if(isset($_GET['try']))
					{
						$this->flashMessage("Server temperory unavailable. Please try later.");
					}
					else
					{
						$url = $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'&try=1';
						$this->redirect($url);
					}
				}
				//$response = $searchByUrl->searchList($q, 100);
				$array = SearchByUrl::list2Array($response);
				$_SESSION['continuationKey'] = SearchByUrl::getContinuationKey($response);
				$_SESSION['regressionKey'] = SearchByUrl::getRegressionKey($response);
				$nextLink = '&next='.SearchByUrl::getContinuationKey($response);
				$previousLink = '&previous='.SearchByUrl::getRegressionKey($response);
				$this->template->letter = $_GET['q'];
				$this->template->nextLink = $nextLink;
				$this->template->previousLink = $previousLink;
				$this->template->list = $array;
								
			}
		}
		
		
		elseif(isset($_GET["c"]) && $_GET["c"]!='') 
		{
			$companyName = $_GET["c"];
			
			$url = $_SERVER["REQUEST_URI"];
			$urlArr = explode('/', $url);			
			$flag = false;
			foreach($urlArr as $key => $value)
			{
				if($value == $companyName)
				{
					$flag = true;	
				}				
			}
			
			if(!$flag)
			{
				if($urlArr[$key] != '')
				{
					$companyName2 = $urlArr[$key];
				}
				else
				{
					$companyName2 = $urlArr[$key-1];
				}
			}			
			
			$searchByUrl = new SearchByUrl();
			$searchByUrl->setXmlGateway();
			/*
			if(isset($companyName2))
			{					
				$companyDetails = $searchByUrl->doSearch($companyName2);					
			}
			else
			{
				$companyDetails = $searchByUrl->doSearch($companyName);
			}
			*/
			try{
				if(isset($companyName2))
				{					
					$companyDetails = $searchByUrl->doSearch($companyName2);					
				}
				else
				{
					$companyDetails = $searchByUrl->doSearch($companyName);
				}	
			}
			catch (Exception $e)
			{
				if(isset($_GET['try']))
				{
					$this->flashMessage("Server temperory unavailable. Please try later.");
				}
				else
				{
					$url = $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'&try=1';					
					$this->redirect($url);
				}
			}
			if(!$companyDetails)
			{				
				$this->redirect(269, array('q' => $companyName));	
			}
			$array = SearchByUrl::companyDetails2Array($companyDetails);
			//do not show this company
			if(isset($array['CompanyNumber']) && $array['CompanyNumber'] == '06173026') $this->redirect("/company/");
			
			if(isset($array['RegAddress']))
				$address = (array)$array['RegAddress'];
			if(isset($array['SICCodes']))
				$industry = (array)$array['SICCodes'];		
			
			$filing = $searchByUrl->searchFiles($array['CompanyNumber']);			
			
			$counter = 0;			
			foreach($filing as $key => $value)
			{
				if($key == "FHistItem")
				{					
					foreach($value as $k => $v)
					{
						if($counter == 10)
						{
							break;
						}
						$files[$k] = (array) $v;
						$counter++;
					}					
				}
			}
			
			if(isset($files))
			{
				foreach($files as $key => $value)
				{
					foreach($value as $k => $v)
					{
						if($k == "DocumentDesc" && is_array($v))
						{
							$name = '';
							foreach($v as $val)
							{
								$name .= $val.'; '; 
							}
							$files[$key]["DocumentDesc"] = SearchByUrl::cutString($name, 100).'...';
						}					
					}
				}
			}
			else
			{
				$files[] = "No documents"; 
			}			
			
			$this->template->keywords = $array.', '.$address;
			$this->template->files = $files;			
			$this->template->industry = $industry;
			$this->template->address = $address;
			$this->template->company4head = $array;
			$this->template->company = $array;
					
			$buyNowLink = "https://www.companysearchesmadesimple.com/company-credit-report-search/continue/?company_number=".$array['CompanyNumber']."&ltd=1&hash=".self::searchHash($array['CompanyNumber']);
			$this->template->buyNowLink = $buyNowLink;
		}
		
	}
	
	static public function searchHash($companyNumber)  
	{
		return md5($companyNumber . 'secretword');
	}
		
	
	public function handleList()
	{
		if(isset($_GET['q']))
		{
			$searchByUrl = new SearchByUrl();
			$searchByUrl->setXmlGateway();
			$response = $searchByUrl->searchList($_GET['q'], 20);
			pr($response);exit;			
			$array = SearchByUrl::list2Array($response);			
			$this->template->list = $array;
		}
	}
	
}
?>
