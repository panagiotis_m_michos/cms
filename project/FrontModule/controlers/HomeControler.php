<?php

use CsmsModule\ICsms;
use FeefoModule\IFeefo;

class HomeControler extends DefaultControler
{
    const HOME_PAGE = 44;
    const HOME_NEW_PAGE = 1290;
    const HOME_NEW_PAGE_2014 = 1424;

    /** @var array */
    public $possibleActions = [
        self::HOME_PAGE => 'default',
        self::HOME_NEW_PAGE => 'newHomePage',
        self::HOME_NEW_PAGE_2014 => 'newHomePage2014',
    ];

    protected function prepareDefault()
    {
        $this->template->searchForm = $this->getComponent('search');
        $this->template->blogs = 1;
        $this->template->hide = 1;
        
        /** @var ICsms $csms */
        $csms = $this->getService('csms_module.csms');

        /** @var IFeefo $feefo */
        $feefo = $this->getService('feefo_module.feefo');
        $summary = $feefo->getSummary();

        $this->template->incorporatedThisYear = $csms->getIncorporationStats()->getIncorporationCountThisYearToNow();
        $this->template->feefoAverage = $summary->getAverageRating();
        $this->template->feefoReviewCount = $feefo->getSummary()->getReviewCount();

        $this->template->searchForm = $this->getComponent('search');
    }

    public function renderNewHomePage()
    {
        $this->template->searchForm = $this->getComponent('search');
    }

    public function renderNewHomePage2014()
    {
        $this->template->searchForm = $this->getComponent('search');
    }

    public function renderHome2016()
    {
        /** @var ICsms $csms */
        $csms = $this->getService('csms_module.csms');

        /** @var IFeefo $feefo */
        $feefo = $this->getService('feefo_module.feefo');
        $summary = $feefo->getSummary();

        $this->template->incorporatedThisYear = $csms->getIncorporationStats()->getIncorporationCountThisYearToNow();
        $this->template->feefoAverage = $summary->getAverageRating();
        $this->template->feefoReviewCount = $feefo->getSummary()->getReviewCount();
    }
}
