<?php


class CFSharesControler_OLD extends CFControler
{
	const SHARES_PAGE = 154;
	
	/** @var array */
	public $possibleActions = array(
		self::SHARES_PAGE => 'shares',
	);
	
	public function beforePrepare()
	{
		parent::beforePrepare();
		// check permission
		if ($this->checkStepPermission('shares') == FALSE) {
			$this->redirect(CFRegisteredOfficeControler::REGISTER_OFFICE_PAGE, "company_id={$this->company->getCompanyId()}");
		}
		$this->template->hide = 1;
	}
	
	
	protected function prepareShares()
	{
		$form = new FForm('shares');
		
		// share capital
		$form->addFieldset('Share capital');
		$form->addSelect('currency', 'Share Currency', IncorporationCapital::$currency);
		$form->addText('share_class', 'Share Class')
			->setValue('ORD')
			->readonly();
        /*
		$form->addText('num_shares', 'Number of shares')
			->addRule(FForm::Required, 'Please provide number of shares')
			->addRule(FForm::NUMERIC, 'Number of shares has to be numeric')
			->addRule(FForm::GREATER_THAN, 'Number of shares has to be greater than #1', 0)
			->setValue(1000);
        */
		$form->addText('aggregate_nom_value', 'Value per Share')
			->addRule(FForm::Required, 'Please provide value per shares')
			->addRule(FForm::NUMERIC, 'Value per share has to be numeric')
			->addRule(FForm::GREATER_THAN, 'Value per share has to be greater than #1', 0)
			->addRule(FForm::REGEXP, 'Amount per share can be int or decimal with maximum 6 fraction digits', '#^\d+(.{1}(\d{1,6})){0,1}$#')
			->setValue(1);
			
		// share capital
		$form->addFieldset('Action');
		$form->addSubmit('continueprocess', 'Continue >')->class('btn_submit fright mbottom');
		
		
		$form->onValid = array($this, 'Form_sharesFormSubmitted');
		
		$capitals = $this->company->getLastFormSubmission()->getForm()->getIncShares();
		if(!empty($capitals)) {
			$capital = $capitals[0]->getShares()->getFields();
			$form->setInitValues($capital);
		}
		
		// PLC
		if ($this->company->getLastFormSubmission()->getForm()->getCompanyType() == 'PLC') {
			$form['currency']->setDisabledOptions('USD');
			$form['num_shares']->addRule(array($this, 'Validator_NumberOfShares'), 'Minimum share capital is &pound;50,000 or &euro;65,600', array('GBP' => 50000, 'EUR' => 65600));
		}
		
		//$form->clean();
		$form->start();
		$this->template->form = $form;
		//$this->template->shareCapital = $form['num_shares']->getValue() * $form['aggregate_nom_value']->getValue();
	}
	
	
	public function Validator_NumberOfShares($control, $error, $params)
	{
		$currency = $control->owner['currency']->getValue();
		if (isset($params[$currency])) { 
			if ($control->getValue() < $params[$currency]) {
				return $error; 
			}
		}
		return TRUE;
	}
	
	public function Form_sharesFormSubmitted($form)
	{
		$companyIncorporation = $this->company->getLastFormSubmission()->getForm();
		
		$authorisedCapitals = $companyIncorporation->getIncShares();
		if(!empty($authorisedCapitals)) {
			$authorisedCapital = $authorisedCapitals[0];
		} else {
			$authorisedCapital = $companyIncorporation->getNewIncShares();
		}
		
		$shares = $authorisedCapital->getShares();
		
		// fields
		$fields = $form->getValues();
        $fields['num_shares'] = 1000;
		$fields['amount_paid'] = $fields['aggregate_nom_value'] * $fields['num_shares'];
		$fields['amount_unpaid'] = 0;
		$fields['prescribed_particulars'] = 'Full voting rights';

		$shares->setFields($fields);
		
		$authorisedCapital->setShares($shares);
		$authorisedCapital->save();
		
	    $form->clean();
		$this->flashMessage('Your shares has been saved');
		$this->redirect(CFDirectorsControler::DIRECTORS_PAGE, 'company_id='.$this->company->getCompanyId());
	}
}
