<?php

namespace FrontModule\Controllers;

use Repositories\UserRepository;
use Symfony\Component\HttpFoundation\Response;

class ProjectStatus
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * check if project with its dependencies is working correctly
     * @return Response
     */
    public function renderDefault()
    {
        if ($this->userRepository->countAll() < 1) {
            return new Response('Db error', Response::HTTP_SERVICE_UNAVAILABLE);
        }
        return new Response('OK');
    }

}
