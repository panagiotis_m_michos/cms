<?php

use BankingModule\BankingDecider;
use BankingModule\Config\DiLocator as BankingDiLocator;
use Entities\Answer;
use Services\AnswerService;

class CFRegisteredOfficeControler extends CFControler
{
    const REGISTER_OFFICE_PAGE = 153;
    const REGISTER_OFFICE_ONECLICK_PAGE = 1261;
    const REGISTER_OFFICE_PRODUCT = 1257;
    const REGISTER_OFFICE_MAIL = 1259;

    /**
     * @var array
     */
    public $possibleActions = array(
        self::REGISTER_OFFICE_PAGE => 'office',
        self::REGISTER_OFFICE_ONECLICK_PAGE => 'oneclickpayment',
        self::REGISTER_OFFICE_MAIL => 'officemail',
    );

    /**
     * @var string
     */
    static public $handleObject = 'CFRegisteredOfficeModel';

    /**
     * @var CFRegisteredOfficeModel
     */
    public $node;

    /**
     * @var AnswerService
     */
    private $answerService;

    /**
     * @var BankingDecider
     */
    private $bankingDecider;

    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->answerService = $this->getService(DiLocator::SERVICE_ANSWER);
        $this->bankingDecider = $this->getService(BankingDiLocator::DECIDER_BANKING);

        if ($this->checkStepPermission('office') == FALSE) {
            $this->redirect(CompaniesCustomerControler::COMPANIES_PAGE);
        }

        if (String::upper($this->company->getCompanyName() == Company::RANDOM_LTD_COMPANY_NAME) || String::upper($this->company->getCompanyName()) == Company::RANDOM_LLP_COMPANY_NAME) {
            $this->saveBacklink();
            $this->redirect(CFCompanyNameControler::COMPANY_NAME_PAGE, array('company_id' => $this->company->getCompanyId()));
        }

        if ($this->hasToChooseBankAccount()) {
            $this->redirect(CFCompanyCustomerControler::PAGE_CHOOSE_BANK_ACCOUNT, "company_id={$this->company->getCompanyId()}");
        }

        $tokensModel = new TokensModel();
        if (!$this->company->getRegisteredOfficeId()
            && !isset($this->get['token_offer'])
            && !isset($this->get['popup'])
            && $tokensModel->getCustomerToken($this->customer) != NULL
        ) {
            $this->redirect(self::REGISTER_OFFICE_PAGE, array("company_id" => $this->company->getCompanyId(), "noBank" => "1", "token_offer=1"));
        }

        $this->template->hide = 1;
    }

    protected function prepareOffice()
    {
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsAdresses = $prefillAddress['js'];

        $registeredOfficeId = $this->company->getRegisteredOfficeId();
        if ($registeredOfficeId) {
            $this->company->getLastIncorporationFormSubmission()->getForm()->setMSGRegisteredOffice(TRUE);
            $this->template->registeredOffice = new RegisterOffice($registeredOfficeId);
        }

        $answer = $this->answerService->getAnswerByCustomer($this->customerEntity);
        $this->template->optionTrigger = Answer::TYPE_NONE;

        $form = new CFRegisteredOfficeForm($this->node->getId() . '_office');
        $form->startup($this, array($this, 'Form_officeFormSubmitted'), $this->company, $prefillAddress, $this->customerEntity->isRetail(), $answer);
        $this->template->form = $form;
        $tokenMogel = new TokensModel();
        $this->template->token = $tokenMogel->getCustomerToken($this->customer);
        $product = Product::getProductById(self::REGISTER_OFFICE_PRODUCT);
        $this->template->product = $product;

        //check package token and reg office
        $this->template->one_click_register_office = 0;
        $hasEligiblePackage = $this->service->checkOrderHasItem(Package::PACKAGE_BRONZE)
            || $this->service->checkOrderHasItem(Package::PACKAGE_BRONZE_PLUS)
            || $this->service->checkOrderHasItem(Package::PACKAGE_BASIC)
            || $this->service->checkOrderHasItem(Package::PACKAGE_CONTRACTOR)
            || $this->service->checkOrderHasItem(Package::PACKAGE_DIGITAL)
            || $this->service->checkOrderHasItem(Package::PACKAGE_PRINTED);

        if ($hasEligiblePackage
            && $tokenMogel->getCustomerToken($this->customer) != NULL
            && $this->company->getRegisteredOfficeId() == NULL
        ) {
            $this->template->one_click_register_office = 1;
        }
        $this->template->customer = $this->customer;
    }

    /**
     * @param CFRegisteredOfficeForm $form
     */
    public function Form_officeFormSubmitted(CFRegisteredOfficeForm $form)
    {
        try {
            $form->process();
            $data = $form->getValues();

            if (array_key_exists('answer', $data) || array_key_exists('tradingStart', $data)) {
                $answer = $this->answerService->getAnswerByCustomer($this->customerEntity) ?: new Answer;

                if (!empty($data['answer'])) {
                    $answer->setTextById($data['answer']);
                }

                if (!empty($data['additionalAnswer'])) {
                    $answer->setAdditionalText($data['additionalAnswer']);
                }

                if (array_key_exists('tradingStart', $data)) {
                    $answer->setTradingStartById($data['tradingStart']);
                }

                if (!empty($data['answer']) || !empty($data['tradingStart'])) {
                    $answer->setCompany($this->companyEntity);
                }

                $answer->setCustomer($this->customerEntity);
                $this->answerService->saveAnswer($answer);
            }
            $this->flashMessage('Your registered office details have been saved');
            $this->redirect(CFDirectorsControler::DIRECTORS_PAGE, 'company_id=' . $this->company->getCompanyId());
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    public function renderOneclickpayment()
    {
        Debug::disableProfiler();

        $backlink = $this->router->link(self::REGISTER_OFFICE_PAGE, array("company_id" => $this->company->getCompanyId(), "noBank" => "1"));
        $this->saveBacklink($backlink);

        $tokensModel = new TokensModel();
        $this->template->token = $tokensModel->getCustomerToken($this->customer);
        $this->template->companyId = $this->company->getCompanyId();
        $product = Product::getProductById(self::REGISTER_OFFICE_PRODUCT);
        $this->template->product = $product;
        $this->template->product_vat = number_format($product->associatedPrice * (Basket::VAT), 2, '.', ',');
        $this->template->product_gross = number_format($product->associatedPrice * (Basket::VAT + 1), 2, '.', ',');
    }

    public function renderOfficemail()
    {
        Debug::disableProfiler();
    }

    /**
     * @return bool
     */
    private function hasToChooseBankAccount()
    {
        return ($this->bankingDecider->hasBankingEnabled($this->companyEntity)
            && !$this->bankingDecider->hasDetailsFilled($this->companyEntity)
            && $this->service->checkUKCitizen()
            && !isset($this->get['noBank']));
    }
}
