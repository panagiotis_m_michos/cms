<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    ProductControler.php 2009-09-24 divak@gmail.com
 */


class ProductPageControler extends BaseControler
{
	protected function renderDefault()
	{
		$cache = FApplication::getCache();
		$cacheKey = 'productPageProducts' . $this->nodeId;
		if (isset($cache[$cacheKey])) {
			$products = $cache[$cacheKey];
		} else {
			$products = array();
			$included = $this->node->getProperty('products');
			if ($included != NULL) {
				$included = explode(',', $included);
				foreach ($included as $key=>$val) {
					$product = $this->node->getProductById($val);
					if ($product->isOk2Show()) {
						$products[$val] = $this->node->getProductById($val);
					}				
				}
			}
			$cache->save($cacheKey, $products, array('expire' => time() + 60 * 10));
		}
		$this->template->products = $products;
	}
}