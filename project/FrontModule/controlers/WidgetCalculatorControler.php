<?php

class WidgetCalculatorControler extends FControler
{
    /* vevericka */
    //	const HOME_PAGE = 737;
    //	const DEMO_PAGE = 749;

    /* live */
    const HOME_PAGE = 834;
    const DEMO_PAGE = 865;

    /**
     * @var array
     */
    public $possibleActions = array(
        self::HOME_PAGE => 'home',
        self::DEMO_PAGE => 'demo',
    );

    /**
     * @var string
     */
    static public $handleObject = 'WidgetCalculatorModel';

    /**
     * @var WidgetCalculatorModel
     */
    public $node;


    public function beforePrepare()
    {
        parent::beforePrepare();
        Debug::disableProfiler();

        try {
            if (!isset($this->get['hash'])) throw new Exception("Hash code is required.");
            $this->node->startup($this->get['hash']);
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect("%{$this->getHomeLink()}%");
        }
    }


    /******************************************* home *******************************************/


    public function renderHome()
    {
        try {
            // --- affiliate ---
            $this->template->affiliate = $this->node->getAffiliate();

            // --- form ---
            $form = new LtdCalculatorForm('WidgetLtdCalculator');
            $form->startup(); 
            $this->template->form = $form;

            if ($form->isSubmited() && $form->isOk2Save()) {
                $annualProfit = $form->getValue('annualProfit');

                // --- calculator ---
                $calculator = new LTDCalculator($annualProfit);
                $this->template->selfEmployed = $calculator->getSelfEmployed();
                $this->template->company = $calculator->getCompany();
                $this->template->saving = $calculator->getSaving();
                $this->template->submitted = TRUE;
            }
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
        }
    }


    /******************************************* home *******************************************/ 


    public function renderDemo()
    {
        $this->template->affiliate = $this->node->getAffiliate();
        $this->template->iframeUrl = FApplication::$router->link(self::HOME_PAGE, array('hash' => $this->node->getAffiliate()->hash));
    }
}
