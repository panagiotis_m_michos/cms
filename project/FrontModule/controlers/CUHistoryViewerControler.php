<?php

use CompanyUpdateModule\Views\CUHistoryViewerControlerView;
use FormSubmissionModule\Factories\FormSubmissionViewFactory;
use FormSubmissionModule\Views\FormSubmissionView;

class CUHistoryViewerControler extends CUControler
{
    const PAGE_OTHER = 917;
    const PAGE_ANNUAL_RETURN = 919;
    const PAGE_INCORPORATION = 920;
    
    /**
     * @var array
     */
    public $possibleActions = array(
        self::PAGE_OTHER => 'formSubmission',
        self::PAGE_ANNUAL_RETURN => 'annualReturnSubmission',
        self::PAGE_INCORPORATION => 'incorporationSubmission'
    );

    /**
     * @var string
     */
    public static $handleObject = 'HistoryViewerModel';

    /**
     * @var HistoryViewerModel
     */
    public $node;

    /**
     * @var ChAnnualReturn
     */
    public $chAnnualReturn;

    /**
     * @var CUHistoryViewerControlerView
     */
    private $controllerView;

    /**
     * @var FormSubmissionView
     */
    private $submissionView;

    /**
     * @var FormSubmissionViewFactory
     */
    private $formSubmissionViewFactory;

    public function startup()
    {
        parent::startup();

        $this->formSubmissionViewFactory = $this->getService('form_submission_module.factories.form_submission_view_factory');
    }
    
    public function beforePrepare()
    {
        parent::beforePrepare();
        $this->node->setCompany($this->company);

        try {
            if (!isset($this->get['submission_id'])) {
                throw new Exception('submission_id has to be set');
            }

            $this->controllerView = new CUHistoryViewerControlerView($this->formSubmissionViewFactory->createFromId($this->get['submission_id']));
            $this->submissionView = $this->controllerView->getFormSubmission();
            if ($this->submissionView == FALSE) {
                throw new Exception('Record with given submission_id does not exist');
            }
        } catch (Exception $e) {
            $this->processException($e);
        }

    }

    public function beforeRender()
    {
        parent::beforeRender();

        $this->template->submission = $this->submissionView;
        $this->template->submissionErrors = $this->node->getErrors($this->get['submission_id']);
    }

    /* ****************************************** FormSubmission ************************************************** */

    public function handleFormSubmission()
    {
        if (isset($this->get['pdf'])) {
            try {
                $pdf = new FormSubmisionPdf($this->submissionView, $this->node->getErrors($this->get['submission_id']));
                $pdf->addOtherSubmisionData(
                    $this->node->getOtherDecoratedData($this->get['submission_id'], $this->submissionView->getFormIdentifier())
                );
                $pdf->renderFormSubmisionPdf();
            } catch (Exception $e) {
                $this->processException($e);
            }
        }
    }

    public function renderFormSubmission()
    {
        $this->template->data = $this->node->getOtherDecoratedData($this->get['submission_id'], $this->submissionView->getFormIdentifier());
    }

    /* ****************************************** AnnualReturnSubmission ************************************************** */

    public function prepareAnnualReturnSubmission()
    {
        try {
            $this->chAnnualReturn = CHAnnualReturn::getAnnualReturn($this->get['submission_id']);
        } catch (Exception $e) {
            $this->processException($e);
        }
    }

    public function handleAnnualReturnSubmission()
    {
        if (isset($this->get['pdf'])) {
            try {
                $pdf = new FormSubmisionPdf($this->submissionView, $this->node->getErrors($this->get['submission_id']));
                $pdf->addAnnualReturnSubmisionData(AnnualReturnModel::getSummaryDetails($this->chAnnualReturn, $this->company));
                $pdf->renderFormSubmisionPdf();
            } catch (Exception $e) {
                $this->processException($e);
            }
        }
    }

    public function renderAnnualReturnSubmission()
    {
        try {
            $this->template->return = AnnualReturnModel::getSummaryDetails($this->chAnnualReturn, $this->company);
        } catch (Exception $e) {
            $this->processException($e);
        }
    }

    /* ****************************************** IncorporationSubmission ************************************************** */

    public function handleIncorporationSubmission()
    {
        if (isset($this->get['pdf'])) {
            try {
                $pdf = new FormSubmisionPdf($this->submissionView, $this->node->getErrors($this->get['submission_id']));
                $this->node->setFormSubmision($this->get['submission_id']);
                $this->node->setCompany($this->company);
                $pdf->addIncorporationSubmisionData($this->node, $this->company);
                $pdf->renderFormSubmisionPdf();
            } catch (Exception $e) {
                $this->processException($e);
            }
        }
    }

    public function renderIncorporationSubmission()
    {
        try {
            $this->node->setFormSubmision($this->get['submission_id']);
            $this->template->origType = $this->company->getFormSubmission($this->get['submission_id'])->getForm()->getType();
            $this->template->office = $this->node->getComponentOffice();
            $this->template->directors = $this->node->getComponentPersons('dir');
            $this->template->shareholders = $this->node->getComponentPersons('sub');
            $this->template->secretaries = $this->node->getComponentPersons('sec');
            $this->template->articles = $this->node->getComponentArticle();
        } catch (Exception $e) {
            $this->processException($e);
        }
    }

    /**
     * @param Exception $exception
     */
    private function processException($exception)
    {
        $this->flashMessage($exception->getMessage(), 'error');
        $this->redirect(CUSummaryControler::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
    }
}
