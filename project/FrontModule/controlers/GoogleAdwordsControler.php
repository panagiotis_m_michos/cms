<?php

class GoogleAdwordsControler extends DefaultControler
{

    public function beforePrepare()
    {
        parent::beforePrepare();
    }

    public function handleDefault()
    {
        $form = new FForm('GoogleAdwordsFormation');

        // contact form		
        $form->addText('fullName', 'Full name:*')
            ->addRule(FForm::Required, 'Please provide full name.')
            ->class('field220');
        $form->addText('company_name', 'Company name:*')
            ->addRule(FForm::Required, 'Please provide company name.')
            ->class('field220');
        $form->addText('email', 'Email:*')
            ->addRule(FForm::Required, 'Please provide email.')
            ->addRule(FForm::Email, 'Please provide valid email')
            ->class('field220');
        $form->addText('phone', 'Contact number:*')
            ->addRule(FForm::Required, 'Please provide phone number.')
            ->class('field220');

        $form->addSubmit('send', 'Submit')->class('btn_submit fright mbottom mright');
        $form->onValid = array($this, 'Form_GoogleAdwordsFormSubmitted');
        $form->start();

        $this->template->form = $form;
    }

    public function Form_GoogleAdwordsFormSubmitted($form)
    {
        $data = $form->getValues();
        $googleEmailer = $this->emailerFactory->get(EmailerFactory::GOOGLE);
        $googleEmailer->googleAdwordsEmail($data);
        $this->flashMessage("Thank you for your interest in our Google AdWords Account Set Up. We will be in contact shortly.");
        $form->clean();
        $this->redirect();
    }

}