<?php

use Admin\CHForm;
use CompanyFormationModule\Views\IncorporationSummaryViewFactory;
use PeopleWithSignificantControl\Forms\CorporatePscData;
use PeopleWithSignificantControl\Forms\PersonPscData;
use PeopleWithSignificantControl\Forms\PscFormFactory;
use PeopleWithSignificantControl\Interfaces\IPscAware;
use PeopleWithSignificantControl\Providers\PscChoicesProvider;

class CFPscsControler extends CFControler
{
    const PSCS_PAGE = 1690;
    const ADD_PSC_PERSON_PAGE = 1686;
    const EDIT_PSC_PERSON_PAGE = 1687;
    const ADD_PSC_CORPORATE_PAGE = 1688;
    const EDIT_PSC_CORPORATE_PAGE = 1689;

    /**
     * @var string
     */
    public static $handleObject = 'CFPscsModel';

    /** @var array */
    public $possibleActions = array(
        self::PSCS_PAGE => 'pscs',
        self::ADD_PSC_PERSON_PAGE => 'addPscPerson',
        self::EDIT_PSC_PERSON_PAGE => 'editPscPerson',
        self::ADD_PSC_CORPORATE_PAGE => 'addPscCorporate',
        self::EDIT_PSC_CORPORATE_PAGE => 'editPscCorporate',
    );

    /**
    * @var CFPscsModel
    */
    public $node;

    /**
     * @var IncorporationPersonPsc|IncorporationCorporatePsc
     */
    private $psc;

    /**
     * @var PscChoicesProvider
     */
    private $pscChoicesProvider;

    /**
     * @var IncorporationSummaryViewFactory
     */
    private $incorporationSummaryViewFactory;

    /**
     * @var CompanyIncorporation
     */
    private $incorporation;

    /**
     * @var string
     */
    private $companyType;

    /**
     * @var PscFormFactory
     */
    private $pscFormFactory;

    public function startup()
    {
        parent::startup();

        $this->pscChoicesProvider = $this->getService('people_with_significant_control_module.providers.psc_choices_provider');
        $this->incorporationSummaryViewFactory = $this->getService('company_formation_module.views.incorporation_summary_view_factory');
        $this->pscFormFactory = $this->getService('people_with_significant_control_module.forms.form_factory');
    }

    public function beforePrepare()
    {
        parent::beforePrepare();

        if (!Feature::featurePsc() || $this->checkStepPermission('pscs') == FALSE) {
            $this->redirect(CFShareholdersControler::SHAREHOLDERS_PAGE, ['company_id' => $this->company->getId()]);
        }

        $this->incorporation = $this->company->getLastIncorporationFormSubmission()->getForm();
        $this->companyType = $this->incorporation->getCompanyType();
        $this->template->hide = 1;
    }

    protected function handlePscs()
    {
        if (isset($this->get['delete_person_id']) || isset($this->get['delete_corporate_id'])) {
            try {
                if (isset($this->get['delete_person_id'])) {
                    $psc = $this->incorporation->getIncPerson($this->get['delete_person_id'], 'SUB');
                } else {
                    $psc = $this->incorporation->getIncCorporate($this->get['delete_corporate_id'], 'SUB');
                }
                $psc->remove();
                $this->flashMessage('PSC has been deleted');
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage());
            }
            $this->redirect(NULL, ['delete_person_id' => NULL, 'delete_corporate_id' => NULL]);
        }
    }

    protected function preparePscs()
    {
        $incorporation = $this->incorporation;

        if (!$incorporation->isPreAddedPscs() && $incorporation->pscsCount() == 0) {
            $suggestedPscs = $incorporation->getSuggestedPscs();

            foreach ($suggestedPscs as $psc) {
                $psc->save();
            }

            $incorporation->setPreAddedPscs(TRUE);

            if (!empty($suggestedPscs)) {
                $this->template->showPreAddedInfo = TRUE;
                $this->template->tabs = $this->getComponentCFTabs();
            }
        }

        $continue = new FForm('continuePscs');
        $continue->addSubmit('continueprocess', 'Continue >')->class('btn_submit fright mbottom');
        $continue->onValid = [$this, 'Form_continuePscsFormSubmitted'];
        $continue->start();

        $this->template->continue = $continue;
    }

    public function Form_continuePscsFormSubmitted()
    {
        try {
            $this->check('HAS_PSC_OR_NO_PSC_REASEON');
        } catch (Exception $e) {
            $addPscLink = "<a href='{$this->router->link(self::ADD_PSC_PERSON_PAGE, ['company_id' => $this->company->getId()])}'>Add another PSC</a>";
            $this->flashMessage("Please add the company's PSCs.<br>You can add a PSC by clicking {$addPscLink} at the bottom of the page", 'error', FALSE);
            $this->redirect();
        }

        if ($this->companyType == CompanyIncorporation::LIMITED_LIABILITY_PARTHENERSHIP) {
            $reservedWord = ReservedWord::match($this->company->getCompanyName(), ReservedWord::TYPE_TEMPLATE);
            if (!empty($reservedWord)) {
                $this->redirect(CFArticlesControler::ARTICLES_PAGE, "company_id={$this->company->getCompanyId()}");
            }
        }

        if ($this->service->checkOrderHasItem(Product::PRODUCT_BUSINESS_TOOLKIT)) {
            $this->redirect(CFToolkitControler::PAGE_TOOLKIT, ['company_id' => $this->company->getCompanyId()]);
        } else {
            $this->redirect(CFSummaryControler::SUMMARY_PAGE, ['company_id' => $this->company->getCompanyId()]);
        }
    }

    public function renderPscs()
    {
        $noPscsForm = new FForm('noPscs');
        $noPscsForm->addCheckbox('noPscReason', $this->pscChoicesProvider->getNoPscReason(), 1);
        $noPscsForm->addSubmit('save', 'Save')->class('btn btn-primary');
        $noPscsForm->onValid = [$this, 'Form_noPscsFormSubmitted'];
        $noPscsForm->setInitValues(['noPscReason' => $this->incorporation->hasNoPscReason()]);
        $noPscsForm->start();

        $this->template->noPscsForm = $noPscsForm;
        $this->template->summaryView = $this->incorporationSummaryViewFactory->from($this->incorporation, $this->companyEntity);
    }

    /**
     * @param FForm $form
     */
    public function Form_noPscsFormSubmitted(FForm $form)
    {
        /** @var CompanyIncorporation $incorporation */
        $incorporation = $this->incorporation;
        $noPscReason = $form->getValue('noPscReason');

        if ($noPscReason) {
            $incorporation->setNoPscReason(IPscAware::NO_INDIVIDUAL_OR_ENTITY_WITH_SIGNFICANT_CONTROL);
            foreach ($incorporation->getPersonPscs() as $psc) {
                $psc->remove();
            }
            foreach ($incorporation->getCorporatePscs() as $psc) {
                $psc->remove();
            }
        } else {
            $incorporation->setNoPscReason(NULL);
        }

        $form->clean();
        $this->flashMessage('PSC reason has been saved');
        $this->redirect();
    }

    /**********************************************************************************/
    /************************** PSC PERSON ************************************/
    /**********************************************************************************/

    /********************************************* add psc person ***************************************************/
    protected function renderAddPscPerson()
    {
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'person');
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
        $this->template->jsPrefillAdresses = $prefillAddress['js'];

        $this->template->form = $this->pscFormFactory->getPersonAddForm(
            $this->company,
            function (PersonPscData $data, FForm $form) {
                /** @var IncorporationPersonPsc $psc */
                $psc = $this->incorporation->getNewIncPerson('PSC');
                PersonPscDataTransformer::merge($psc, $data);
                $psc->setConsentToAct(1);
                $psc->save();
                $this->incorporation->setNoPscReason(NULL);
                $form->clean();
                $this->flashMessage("Person PSC has been added");
                $this->redirect(self::PSCS_PAGE, 'company_id='.$this->company->getCompanyId());
            }
        );

        $this->template->natureOfControls = $this->pscChoicesProvider->getNatureOfControlsTemplateStructure(PscChoicesProvider::PERSON, $this->companyType);
        $this->template->natureOfControlsDescription = $this->pscChoicesProvider->getNatureOfControlsTemplateDescription(PscChoicesProvider::PERSON);
        $this->template->companyType = $this->companyType;
        $this->template->companyId = $this->company->getId();
        $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());
    }

    /********************************************* edit psc person ***************************************************/
    protected function prepareEditPscPerson()
    {
        if (!isset($this->get['psc_id'])) {
            $this->redirect(self::PSCS_PAGE, 'company_id='.$this->company->getCompanyId());
        }

        try {
            $this->psc = $this->incorporation->getIncPerson($this->get['psc_id'], 'PSC');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect(self::PSCS_PAGE, array('company_id='.$this->company->getCompanyId(),'psc_id'=>NULL));
        }
        $this->template->companyId = $this->company->getCompanyId();
    }

    protected function renderEditPscPerson()
    {
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'person');
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
        $this->template->jsPrefillAdresses = $prefillAddress['js'];

        $personPscData = PersonPscDataTransformer::from($this->psc);
        $form = $this->pscFormFactory->getPersonEditForm(
            $this->company,
            $personPscData,
            function (PersonPscData $data, FForm $form) {
                PersonPscDataTransformer::merge($this->psc, $data);
                $this->psc->save();
                $this->incorporation->setNoPscReason(NULL);
                $form->clean();
                $this->flashMessage("Person PSC has been added");
                $this->redirect(self::PSCS_PAGE, 'company_id='.$this->company->getCompanyId());
            }
        );

        $this->template->form = $form;
        $this->template->natureOfControls = $this->pscChoicesProvider->getNatureOfControlsTemplateStructure(PscChoicesProvider::PERSON, $this->companyType);
        $this->template->natureOfControlsDescription = $this->pscChoicesProvider->getNatureOfControlsTemplateDescription(PscChoicesProvider::PERSON);
        $this->template->companyType = $this->companyType;
        $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());
    }

    /**********************************************************************************/
    /************************** PSC CORPORATE *********************************/
    /**********************************************************************************/

    /********************************************* add psc corporate ***************************************************/
    protected function renderAddPscCorporate()
    {
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'corporate');
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
        $this->template->jsPrefillAdresses = $prefillAddress['js'];

        $form = $this->pscFormFactory->getCorporateAddForm(
            $this->company,
            function (CorporatePscData $data, FForm $form) {
                /** @var IncorporationCorporatePsc $pscCorporate */
                $pscCorporate = $this->incorporation->getNewIncCorporate('PSC');
                CorporatePscDataTransformer::merge($pscCorporate, $data);
                $pscCorporate->setConsentToAct(1);
                $pscCorporate->save();
                $this->incorporation->setNoPscReason(NULL);
                $form->clean();
                $this->flashMessage("Corporate PSC has been added");
                $this->redirect(self::PSCS_PAGE, 'company_id='.$this->company->getCompanyId());
            }
        );

        $this->template->form = $form;

        $this->template->natureOfControls = $this->pscChoicesProvider->getNatureOfControlsTemplateStructure(PscChoicesProvider::CORPORATE, $this->companyType);
        $this->template->natureOfControlsDescription = $this->pscChoicesProvider->getNatureOfControlsTemplateDescription(PscChoicesProvider::CORPORATE);
        $this->template->companyType = $this->companyType;
        $this->template->companyId = $this->company->getCompanyId();
    }

    /********************************************* edit psc corporate ***************************************************/
    protected function prepareEditPscCorporate()
    {
        if (!isset($this->get['psc_id'])) {
            $this->redirect(self::PSCS_PAGE, 'company_id='.$this->company->getCompanyId());
        }

        try {
            $this->psc = $this->incorporation->getIncCorporate($this->get['psc_id'], 'PSC');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect(self::PSCS_PAGE, array('company_id='.$this->company->getCompanyId(),'psc_id'=>NULL));
        }
    }


    protected function renderEditPscCorporate()
    {
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'corporate');
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillAdresses = $prefillAddress['js'];

        $corporatePscData = CorporatePscDataTransformer::from($this->psc);
        $form = $this->pscFormFactory->getCorporateEditForm(
            $this->company,
            $corporatePscData,
            function (CorporatePscData $data, FForm $form) {
                CorporatePscDataTransformer::merge($this->psc, $data);
                $this->psc->save();
                $this->incorporation->setNoPscReason(NULL);
                $form->clean();
                $this->flashMessage("Corporate PSC has been added");
                $this->redirect(self::PSCS_PAGE, 'company_id='.$this->company->getCompanyId());
            }
        );

        $this->template->form = $form;
        $this->template->natureOfControls = $this->pscChoicesProvider->getNatureOfControlsTemplateStructure(PscChoicesProvider::CORPORATE, $this->companyType);
        $this->template->natureOfControlsDescription = $this->pscChoicesProvider->getNatureOfControlsTemplateDescription(PscChoicesProvider::CORPORATE);
        $this->template->companyType = $this->companyType;
        $this->template->companyId = $this->company->getCompanyId();
    }
}
