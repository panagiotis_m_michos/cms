<?php

use ErrorModule\Ext\DebugExt;

class CFArticlesControler extends CFControler
{

    const ARTICLES_PAGE = 167;

    /** @var array */
    public $possibleActions = array(
        self::ARTICLES_PAGE => 'articles',
    );

    /**
     * @var string
     */
    static public $handleObject = 'CFArticlesModel';

    /**
     * @var CFDirectorsModel
     */
    public $node;

    public function beforePrepare()
    {
        parent::beforePrepare();
        // check permission
        if ($this->checkStepPermission('articles') == FALSE) {
            //llp don't have secretaries and directors
            if (CompanyIncorporation::LIMITED_LIABILITY_PARTHENERSHIP == $this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType()) {
                $this->redirect(CFDirectorsControler::DIRECTORS_PAGE, "company_id={$this->company->getCompanyId()}");
            } else {
                $this->redirect(CFSecretariesControler::SECRETARIES_PAGE, "company_id={$this->company->getCompanyId()}");
            }
        }
        $this->template->hide = 1;
    }

    protected function handleArticles()
    {
        // show article
        if (isset($this->get['document_id'])) {
            try {
                $this->company->getLastIncorporationFormSubmission()->getDocument($this->get['document_id'])->output();
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage());
                $this->redirect(NULL, 'document_id=');
            }
        }
    }

    protected function prepareArticles()
    {
        $form = new FForm('articles');

        $form->addFieldset('Articles');
        $form->addRadio('type', 'Type: ', array(1 => 'Use Standard Memorandum &amp; Articles', 2 => 'Upload Custom Memorandum &amp; Articles'))
            ->setValue(1);

        // just custom article for Section 30
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getType() == CompanyIncorporation::BY_GUARANTEE_EXEMPT) {
            $form['type']->setDisabledOptions(1)->setValue(2);
        }

        //show this section just for not llp companies
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getType() != CompanyIncorporation::LIMITED_LIABILITY_PARTHENERSHIP) {
            // article
            $form->addFieldset('Custom');
            $article = $this->company->getLastIncorporationFormSubmission()->getMemarts();
            if ($article !== NULL && $article['bespoke'] == 1) {
                $form->addText('articleName', 'M&A:')
                    ->setValue($article['filename'])
                    ->setDescription($article['id']);
                $form->addSubmit('removeArticle', 'Remove')->class('linkButton');

                if ($article['bespoke']) {
                    $form['type']->setDisabledOptions(1)->setValue(2);
                } else {
                    $form['type']->setDisabledOptions(2)->setValue(1);
                }
                $this->template->article = $article;
            } else {
                $form->addFile('custom', 'Custom M &amp; A')
                    ->addRule(array($this, 'Validator_fileLenghtAfterEncoding'), 'PDF is too large. Please try to decrease the number of pages in the file by reducing the margins and font size.')
                    ->addRule(array($this, 'Validator_fileSize'), 'The total files size (including M&A, supporting doc and company info) can be no more than 5.0 mbytes')
                    ->addRule(FForm::MIME_TYPE, 'Only pdf is supported for Article', MimeType::PDF)
                    ->addRule(array($this, 'Validator_filePdfVersion'), 'PDF attachments are limited to PDF version 1.2 through 1.7 inclusive')
                    ->addRule(array($this, 'Validator_customArticle'), 'Please provide Custom Article!')
                    ->setDescription('(Must be in PDF)');
            }
        }


        // support
        $form->addFieldset('Supporting documents');
        $support = $this->company->getLastIncorporationFormSubmission()->getSuppnameauth();
        //pr($support);exit;
        if ($support !== NULL) {
            $form->addText('supportName', 'Support:')
                ->setValue($support['filename'])
                ->setDescription($support['id']);
            $form->addSubmit('removeSupport', 'Remove')->class('linkButton');
            $this->template->support = $support;
        } else {
            $supportFile = $form->addFile('support', 'Support document ')
                ->addRule(array($this, 'Validator_fileLenghtAfterEncoding'), 'PDF is too large. Please try to decrease the number of pages in the file by reducing the margins and font size.')
                ->addRule(FForm::MIME_TYPE, 'Only pdf is supported for Support document', MimeType::PDF)
                ->addRule(array($this, 'Validator_filePdfVersion'), 'PDF attachments are limited to PDF version 1.2 through 1.7 inclusive')
                ->setDescription('(Must be in PDF)');

            // if reserved word detected make the supporting document upload compulsory
            $reservedWord = ReservedWord::match($this->company->getCompanyName(), ReservedWord::TYPE_TEMPLATE);
            if (!empty($reservedWord)) {
                $supportFile->addRule(FForm::Required, 'Please upload supporting documentation in order to submit you application. Please follow the Reserved Words guidance.');
            }
        }

        $form->addFieldset('Action');
        $form->addSubmit('continueprocess', 'Continue >')->class('btn_submit fright mbottom');
        $form->onValid = array($this, 'Form_maaFormSubmitted');

        // remove documents
        if ($form->isSubmitedBy('removeArticle') && isset($article['id'])) {
            $this->company->getLastIncorporationFormSubmission()->getDocument($article['id'])->remove();
            $this->flashMessage('Articles have been updated');
            $this->redirect();
        }
        // remove support
        if ($form->isSubmitedBy('removeSupport')) {
            $this->removeSupportDocument($support, $form);
        }

        $form->start();
        $this->template->form = $form;

        $this->template->type = $this->company->getLastIncorporationFormSubmission()->getForm()->getType();
        // reserved word
        $this->template->reservedWords = ReservedWord::match($this->company->getCompanyName(), ReservedWord::TYPE_TEMPLATE);
    }

    public function Form_maaFormSubmitted(FForm $form)
    {
        $updated = FALSE;
        $data = $form->getValues();
        $lastFormSubmission = $this->company->getLastIncorporationFormSubmission();
        $article = $this->company->getLastIncorporationFormSubmission()->getMemarts();
        $support = $this->company->getLastIncorporationFormSubmission()->getSuppnameauth();

        // remove article
        if ($form->isSubmitedBy('removeArticle')) {
            if (isset($article['id'])) {
                $lastFormSubmission->getDocument($article['id'])->remove();
                $updated = TRUE;
            }
        }

        // remove support
        if ($form->isSubmitedBy('removeSupport')) {
            if (isset($support['id'])) {
                $lastFormSubmission->getDocument($support['id'])->remove();
                $updated = TRUE;
            }
        }

        // standard
        if ($data['type'] == 1) {
            if ($article === NULL || $article !== NULL && $article['bespoke'] == 1) {
                $type = $lastFormSubmission->getForm()->getType();
                $file = FILES_DIR . '/' . $type . '/articles.pdf';
                $lastFormSubmission->uploadDocument('MEMARTS', $file);
                $updated = TRUE;
            }
            // custom (bespoke)
        } elseif (isset($data['custom'])) {
            if ($article === NULL || $article !== NULL && $article['bespoke'] == 0) {
                $lastFormSubmission->uploadDocument('MEMARTS', $data['custom'], TRUE);
                $updated = TRUE;
            }
        }

        // supported documents
        if (isset($data['support']) && $data['support'] != NULL) {
            $lastFormSubmission->uploadDocument('SUPPNAMEAUTH', $data['support']);
            $updated = TRUE;
        }

        // check custom article for section 30
        $type = $this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType();
        if ($type == CompanyIncorporation::BY_GUARANTEE_EXEMPT) {
            try {
                $this->check('BESPOKE_ARTICLE');
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
                $this->redirect();
            }
        }


        if ($updated === TRUE) {
            $this->flashMessage('Articles have been updated');
        }

        $form->clean();

        if ($this->service->checkOrderHasItem(Product::PRODUCT_BUSINESS_TOOLKIT)) {
            $this->redirect(CFToolkitControler::PAGE_TOOLKIT, ['company_id' => $this->company->getCompanyId()]);
        } else {
            $this->redirect(CFSummaryControler::SUMMARY_PAGE, ['company_id' => $this->company->getCompanyId()]);
        }
    }

    public function Validator_customArticle($control, $error, $params)
    {
        $type = $control->owner['type']->getValue();
        if ($control->getValue() == NULL && $type == 2) {
            return $error;
        }
        return TRUE;
    }

    /**
     * The total size of the message (including M&A, supporting doc and company info) can be no more than 5.0 mbytes.
     * @param FControl $control
     * @param string $error
     */
    public function Validator_fileSize(FControl $control, $error)
    {
        $from = $control->owner->getElements();
        $supportFile = array_key_exists('support', $from);
        $customFile = array_key_exists('custom', $from);
        if (isset($supportFile) || isset($customFile)) {
            if ($supportFile != NULL && $customFile != NULL) {
                $supportFile = $from['support'];
                $customFile = $from['custom'];
                $customValue = $customFile->getValue();
                $supportValue = $supportFile->getValue();
                $size = $supportValue['size'] + $customValue['size'];
            }
            if ($supportFile == NULL) {
                $customFile = $from['custom'];
                $customValue = $customFile->getValue();
                $size = $customValue['size'];
            }
            if ($customFile == NULL) {
                $supportFile = $from['support'];
                $supportValue = $supportFile->getValue();
                $size = $supportValue['size'];
            }
            if ($size > '5242880') {
                return $error;
            }
        }
        return TRUE;
    }

    /**
     * PDF Maximum file size is 1.2 million characters once Base64 encoded.
     * @param FControl $control
     * @param string $error
     */
    public function Validator_fileLenghtAfterEncoding(FControl $control, $error)
    {
        $value = $control->getValue();
        if ($value) {
            if (strlen(base64_encode(file_get_contents($value['tmp_name']))) > 1200000) {
                return $error;
            }
        }
        return TRUE;
    }

    /**
     * Supports: Acrobat 3.0 (PDF 1.2) > Acrobat 8.0 (PDF 1.7)
     * @param FControl $control
     * @param string $error
     */
    public function Validator_filePdfVersion(FControl $control, $error)
    {
        $value = $control->getValue();
        if (!empty($value['tmp_name'])) {
            preg_match('/\d\.\d/', file_get_contents($value['tmp_name'], 20), $match);
            if (isset($match[0])) {
                if ($match[0] > 1.7 || $match[0] < 1.2) {
                    return $error;
                }
            }
        }
        return TRUE;
    }

    /**
     * @param $support
     * @param $form
     * @throws Exception
     */
    protected function removeSupportDocument($support, $form)
    {
        if (is_array($support) && isset($support['id'])) {
            $this->company->getLastIncorporationFormSubmission()->getDocument($support['id'])->remove();
            $this->flashMessage('Articles have been updated');
            $this->redirect();
        } else {
            //@TODO remove once its clear why this happens
            $logger = $this->getService(DebugExt::LOGGER);
            $logger->warning(
                sprintf(
                    '[S213] Trying to remove document that does not have SUPPNAMEAUTH info. Company id %s. Form %s. Support %s',
                    $this->company->getId(),
                    var_export($form, TRUE),
                    var_export($support, TRUE)
                )
            );
        }
    }

}
