<?php

class GoogleVoucherApplicationControler extends DefaultControler
{

    public function beforePrepare()
    {
        // debug::enable(FALSE);
        parent::beforePrepare();
    }

    
    public function handleDefault()
    {
        $form = new FForm('google_voucher');

        $form->addFieldset('Google Voucher Application Form');
        $form->addText('customer_name', "Name: *")
            ->addRule(FForm::Required, "Name is required!");
        $form->addText('customer_email', "Email: *")
            ->addRule(FForm::Required, 'Please provide e-mail address!')
            ->addRule('Email', 'Email address is not valid!');
        $form->addText('company_name', "Company Name: *")
            ->addRule(FForm::Required, "Name is required!");

        $form->addSubmit('submit', 'Send')->class('btn_submit');

        $form->onValid = array($this, 'Form_GoogleVoucherFormSubmitted');
        $form->start();
        $this->template->form = $form;
    }

    
    public function Form_GoogleVoucherFormSubmitted($form)
    {
        // get data from form
        $data = $form->getValues();
        $googleEmailer = $this->emailerFactory->get(EmailerFactory::GOOGLE);
        $googleEmailer->googleVoucherEmail($data);
        $this->flashMessage("Your query has been sent succesfully.");
        $form->clean();
        $this->redirect();
    }

}
