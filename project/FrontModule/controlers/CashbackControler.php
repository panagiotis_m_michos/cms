<?php

use Dispatcher\Events\CustomerEvent;
use Entities\Cashback;
use Front\CashbacksDataGrid;
use Services\CashbackService;
use Symfony\Component\EventDispatcher\EventDispatcher;

class CashbackControler extends LoggedControler
{
    const CASHBACK_PAGE = 1218;

    /**
     * @var CashBackService
     */
    public $cashBackService;

    /**
     * @var EventDispatcher
     */
    public $dispatcher;


    public function startup()
    {
        parent::startup();
        $this->cashBackService = $this->getService(DiLocator::SERVICE_CASHBACK);
        $this->dispatcher = $this->getService(DiLocator::DISPATCHER);
    }

    public function renderDefault()
    {
        $form = new CashbackPreferencesForm($this->node->getId() . '_default');
        $form->startup($this->customerEntity);
        if ($form->isOk2Save()) {
            try {
                $data = $form->getValues();
                $this->customerEntity->setCashbackType($data['cashbackTypeId']);
                switch ($data['cashbackTypeId']) {
                    case 'BANK':
                        $this->customerEntity->setSortCode($data['sortCode']);
                        $this->customerEntity->setAccountNumber($data['accountNumber']);
                        break;
                    case 'CREDITS':
                        $this->customerEntity->setSortCode(NULL);
                        $this->customerEntity->setAccountNumber(NULL);
                        break;
                }
                $this->customerService->save($this->customerEntity);

                $this->dispatcher->dispatch(
                    EventLocator::CUSTOMER_BANK_DETAILS_UPDATED,
                    new CustomerEvent($this->customerEntity)
                );
                $form->clean();

                $this->flashMessage('Your details have been successfully stored.', 'info');
                $this->redirect();

            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
                $this->redirect();
            }
        }

        $this->template->form = $form;
        $this->template->cashbacks = new CashbacksDataGrid(
            $this->cashBackService->getCustomerCashbacksDataSource($this->customerEntity),
            'cashbackId'
        );
    }
}
