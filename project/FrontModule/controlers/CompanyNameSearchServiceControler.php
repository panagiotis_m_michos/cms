<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    CompanyNameSearchServiceControler.php 2010-11-09 divak@gmail.com
 */


class CompanyNameSearchServiceControler extends BaseControler
{
	/**
	 * @var string
	 */
	static public $handleObject = 'CompanyNameSearchServiceModel';
	
	/**
	 * @var CompanyNameSearchServiceModel
	 */
	public $node;
	
	/**
	 * @return void
	 */
	public function beforePrepare()
	{
		Debug::disableProfiler();
			
		try {
			// --- check for g.hash
			if (!isset($this->get['hash']) || $this->get['hash'] != 'bc041032ef8912f1d9c6460c60b1bbb4') throw new Exception("Missing or Invalid hash parameter");
			 
    		// --- check for g.companyName ---
    		if (!isset($this->get['companyName']))  throw new Exception("Missing company name parameter");
    		
    		// --- check if company name is avaiable
    		$companyName = $this->get['companyName'];
    		$available = CompanySearch::isAvailable($companyName);
    		if ($available) {
    			$arr = array (
			    	'error' => FALSE,
			    	'companyNameAvailable' => TRUE,
    				'errorMessage' => NULL
    			);
    		} else {
    			$arr = array (
			    	'error' => FALSE,
			    	'companyNameAvailable' => FALSE,
    				'errorMessage' => NULL
    			);
    		}
    	} catch (Exception $e) {
    		$arr = array (
			    'error' => TRUE,
			    'companyNameAvailable' => FALSE,
    			'errorMessage' => $e->getMessage()
    		);
    	}
		
		$this->node->outputJSON($arr);
	}
}