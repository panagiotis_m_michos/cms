<?php

abstract class CURegisterControler_Abstract extends CUControler
{
    public function beforePrepare()
    {
        parent::beforePrepare();

        if (!$this->companyEntity->isLimitedBySharesType()) {
            $this->flashMessage("Sorry, you can't use the register of members for this company", 'error');
            $this->redirect(CompaniesCustomerControler::COMPANIES_PAGE);
        }
    }
}
