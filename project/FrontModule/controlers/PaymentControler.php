<?php

use BasketModule\Config\DiLocator as BasketModuleDiLocator;
use BasketModule\Services\BasketService;
use Entities\Payment\Token;
use Payment\AutoRenewalReenabler;
use PaymentModule\Controllers\PaymentConfirmationController;
use Services\CompanyService;
use Services\OrderService;
use Services\Payment\TokenService;
use Services\ServiceService;
use ServiceSettingsModule\Config\DiLocator as ServiceSettingsDiLocator;
use ServiceSettingsModule\Services\ServiceSettingsService;
use ValidationModule\IValidator;

class PaymentControler extends DefaultControler
{
    /* CONST */
    const SAGE_PAGE = 1223;
    const SAGE_CHECK_NEW_CUSTOMER_PAGE = 1224;
    const SAGE_AUTH = 1226;
    const SAGE_AUTH_VALIDATION = 1227;
    const NEW_PASSWORD_PAGE = 1228;
    const PAYPAL_NEW_CUSTOMER_CANCEL_PAGE = 1225;
    const PAYPAL_ERROR_EMAIL = 233;
    const TRUST_ELEMENTS_PAGE = 1309;

    /**
     * @var array
     */
    public $possibleActions = [
        self::SAGE_PAGE => 'sage',
        self::SAGE_CHECK_NEW_CUSTOMER_PAGE => 'checkSageNewCustomer',
        self::PAYPAL_NEW_CUSTOMER_CANCEL_PAGE => 'paypalCanceled',
        self::SAGE_AUTH => 'sageAuthentication',
        self::SAGE_AUTH_VALIDATION => 'sageAuthValidation',
        self::NEW_PASSWORD_PAGE => 'sageNewCustomerPassword',
        self::TRUST_ELEMENTS_PAGE => 'trustElementsContent',
    ];

    /**
     * @var string
     */
    public static $handleObject = 'PaymentModel';

    /**
     * @var PaymentModel
     */
    public $node;

    /**
     * @var string Session
     */
    public static $nameSpace = 'paymentRequestDetails';

    /**
     * @var Token
     */
    public $customerToken;

    /**
     * @var bool
     */
    protected $httpsRequired = TRUE;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var ServiceService
     */
    private $serviceService;

    /**
     * @var OrderService
     */
    private $orderService;

    /**
     * @var TokenService
     */
    private $tokenService;

    /**
     * @var AutoRenewalReenabler
     */
    private $autoRenewalReenabler;

    /**
     * @var ServiceSettingsService
     */
    private $serviceSettingsService;

    /**
     * @var IValidator
     */
    private $emailValidator;

    public function startup()
    {
        parent::startup();

        $this->companyService = $this->getService(DiLocator::SERVICE_COMPANY);
        $this->serviceService = $this->getService(DiLocator::SERVICE_SERVICE);
        $this->orderService = $this->getService(DiLocator::SERVICE_ORDER);
        $this->tokenService = $this->getService(DiLocator::SERVICE_TOKEN);
        $this->autoRenewalReenabler = $this->getService(DiLocator::MODEL_PAYMENT_AUTO_RENEWAL_REENABLER);
        $this->serviceSettingsService = $this->getService(ServiceSettingsDiLocator::SERVICES_SERVICE_SETTINGS_SERVICE);
        $this->emailValidator = $this->getService('validation_module.email_validator');
    }

    public function beforePrepare()
    {
        // check is basket is not empty
        if ($this->basket->isEmpty() === TRUE) {
            $this->redirect("%{$this->getHomeLink()}%");
        }

        parent::beforePrepare();
    }

    public function beforeRender()
    {
        parent::beforeRender();
        $this->template->companyName = SearchControler::getCompanyName();
    }

    /* **************************** SAGE *********************** */
    public function prepareSage()
    {
        /*
        // temp
        $response = new PaymentResponse();
        $response->setTypeId(Transaction::TYPE_CREDIT);
        $paymentService = new PaymentService($this->customer, $this->basket, $this->customerEntity, $this->getService(DiLocator::SERVICE_ORDER), $this->getService(DiLocator::SERVICE_PRODUCT));
        $paymentService->completeTransaction($response);

        $this->Service_paymentFinished($paymentService);
        pr('done');
        exit;
        */

        try {
            $view = $this->node->getPaymentView($this->customer, $this->basket);

            // Quick fix. Can be removed once paymentFreeOrder template is fixed
            if ($view === PaymentModel::PAYMNET_PAGE_FREE_ORDER_VIEW) {
                $this->template->customer = $this->customer;
            }

            $this->changeView($view);

            if ($this->customerEntity) {
                $this->customerToken = $this->tokenService->getTokenByCustomer($this->customerEntity);
            }

            if ($this->customer->isNew()) {
                $formLogin = new LoginForm($this->node->getId() . '_login');
                $formLogin->startup([$this, 'Form_loginFormSubmitted']);
                $this->template->formLogin = $formLogin;
            }

            $errors = FApplication::$container->get(BasketModuleDiLocator::BASKET_VALIDATOR)->validate($this->basket)->getErrors();
            if ($errors) {
                $this->redirect(BasketControler::PAGE_BASKET);
            }
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->readFlashMessages();
        }
    }

    public function renderSage()
    {
        // redirect to summary page because of package downgrade
        /** @var BasketService $basketService */
        $basketService = $this->getService('basket_module.services.basket_service');
        $packageToProductDowngradeNotification = $basketService->getPackageToProductDowngradeNotification();
        $packageToPackageDowngradeNotification = $basketService->getPackageToPackageDowngradeNotification();
        if ($packageToProductDowngradeNotification || $packageToPackageDowngradeNotification) {
            $this->redirect(BasketControler::PAGE_SUMMARY);
        }

        try {
            $form = new SagePaymentForm($this->node->getId() . '_sage_payment');
            $form->startup(
                $this,
                [$this, 'Form_sagePaymentFormSubmitted'],
                $this->basket,
                $this->customer,
                $this->companyService,
                $this->customerToken
            );
            $this->template->form = $form;
            $this->template->customerToken = $this->customerToken;

            $service = new PaymentService($this->customer, $this->basket, $this->customerEntity, $this->getService(DiLocator::SERVICE_ORDER), $this->getService(DiLocator::SERVICE_PRODUCT), new TokensModel());
            $service->setOnFinish([$this, 'Service_paymentFinished']);
            $service->processPaymentForm($form, $this->get, $this->post);

            // sage 3D
        } catch (SageAuthenticationException $e) {
            $this->template->authFormUrl = FApplication::$router->secureLink(self::SAGE_AUTH, ['sagepay-3d' => 1]);
            $this->template->showAuth = TRUE;
        } catch (SageException $e) {
            TransactionService::saveFailedSageTransaction($this->customer, $e->getMessage());
            $this->flashMessage($e->getMessage(), 'error');
            $this->readFlashMessages();
        } catch (PaypalException $e) {
            TransactionService::saveFailedPaypalTransaction($this->customer, $e->getMessage());
            $this->flashMessage($e->getMessage(), 'error');
            $this->readFlashMessages();
        } catch (Exception $e) {
            $this->logger->error($e);
            $this->flashMessage($e->getMessage(), 'error');
            $this->readFlashMessages();
        }
    }

    public function Service_paymentFinished()
    {
        $this->template->success = TRUE;
        $additionalParams = [];

        if ($this->customerEntity && $this->autoRenewalReenabler->hasServicesToReenable($this->customerEntity)) {
            $this->autoRenewalReenabler->reenable($this->customerEntity);
            $additionalParams = ['renewalReenabled' => TRUE];
        }

        SearchControler::resetCompanyName();

        $this->redirectRoute(
            PaymentConfirmationController::ROUTE_PURCHASE_CONFIRMATION,
            $this->node->getConfirmationPageLinkParams($this->basket, $additionalParams)
        );
    }

    /**
     * @param LoginForm $form
     */
    public function Form_loginFormSubmitted(LoginForm $form)
    {
        if (FApplication::$httpRequest->isAjax()) {
            try {
                $customer = Customer::doAuthenticate($form['email']->getValue(), $form['password']->getValue());
                CustomerActionLogger::log($customer, CustomerActionLogger::LOG_IN);
                Customer::signIn($customer->getId());
                $json = ['redirect' => 1, 'link' => FApplication::$router->secureLink(PaymentControler::SAGE_PAGE)];
                echo json_encode($json);
                exit;
            } catch (Exception $e) {
                $json = ['error' => 1, 'message' => $e->getMessage()];
                echo json_encode($json);
                die;
            }
        }
        die;
    }

    /*     * *********************************************** PaypalCanceled ************************************************ */

    public function handlePaypalCanceled()
    {
        if (!isset($this->get['token']) || empty($this->get['token'])) {
            $this->redirect(DashboardCustomerControler::DASHBOARD_PAGE);
        }
    }

    /**
     * sage authorization form in iframe (loaded when 3dauth is necessary)
     */
    public function renderSageAuthentication()
    {
        Debug::disableProfiler();
        if (isset($_SESSION['sageAuthForm'])) {
            $this->template->form = $_SESSION['sageAuthForm'];
        }
    }

    /**
     * sage auth form validation, sage pay sends post to this method
     */
    public function renderSageAuthValidation()
    {
        try {
            Debug::disableProfiler();
            $form = new SagePaymentForm($this->node->getId() . '_sage_payment');
            $form->startup(
                $this,
                [$this, 'Form_sagePaymentFormSubmitted'],
                $this->basket,
                $this->customer,
                $this->companyService
            );
            $this->template->form = $form;

            // processPayment
            $service = new PaymentService($this->customer, $this->basket, $this->customerEntity, $this->getService(DiLocator::SERVICE_ORDER), $this->getService(DiLocator::SERVICE_PRODUCT));
            $service->setOnFinish([$this, 'Service_AuthSagepaymentFinished']);
            $service->processPaymentForm($form, $this->get, $this->post);
        } catch (Exception $e) {
            $this->template->failedLocation = FApplication::$router->link(self::SAGE_PAGE);
            $this->template->error = $e->getMessage();
        }
    }

    public function Service_AuthSagepaymentFinished()
    {
        $this->template->success = TRUE;
        $additionalParams = [];

        if ($this->customerEntity && $this->autoRenewalReenabler->hasServicesToReenable($this->customerEntity)) {
            $this->autoRenewalReenabler->reenable($this->customerEntity);
            $additionalParams = ['renewalReenabled' => TRUE];
        }

        $this->template->location = $this->newRouter->generate(
            PaymentConfirmationController::ROUTE_PURCHASE_CONFIRMATION,
            $this->node->getConfirmationPageLinkParams($this->basket, $additionalParams)
        );
    }

    /*     * ******************************** ajax SageNewCustomerPassword ******************************** */

    public function handleSageNewCustomerPassword()
    {
        if (FApplication::$httpRequest->isAjax() && isset($this->get['email'])) {
            try {
                $json = $this->node->generateNewCustomerPassord($this->get['email']);
            } catch (Exception $e) {
                $json = ['error' => 1];
                echo json_encode($json);
                die;
            }
            echo json_encode($json);
            exit;
        }
    }

    /*     * ******************************** ajax CheckSageNewCustomer ******************************** */

    public function handleCheckSageNewCustomer()
    {
        Debug::disableProfiler();

        if (FApplication::$httpRequest->isAjax() && !isset($this->get['password'])) {
            if (isset($this->get['email']) && empty($this->emailValidator->validate($this->get['email']))) {
                if ($this->node->validateUniqueLogin($this->get['email'])) {
                    $json = ['succes' => 1, 'showLogin' => 1];
                } else {
                    $json = ['succes' => 1, 'showLogin' => 0];
                }
            } else {
                $json = ['error' => 1];
            }
            $this->sendJSONResponse($json);
        }
    }

    public function renderTrustElementsContent()
    {
        Debug::disableProfiler();
        if (FApplication::$httpRequest->isAjax()) {
            sleep(2);
            echo $this->getHtml();
        }
        $this->terminate();
    }
}
