<?php

class NonStandardPaymentControler extends BaseControler {
//	const PAYMENT_PAGE = 500;
//	const PAYPAL_PAGE = 503;
//	const PAYPAL_CANCEL_PAGE = 505;
//	const PAYPAL_ERROR_PAGE = 506;
//	const CONFIRMATION_PAGE = 504;

	const PAYMENT_PAGE = 623;
	const PAYPAL_PAGE = 624;
	const PAYPAL_CANCEL_PAGE = 626;
	const PAYPAL_ERROR_PAGE = 627;
	const CONFIRMATION_PAGE = 625;
	const SAGE_AUTH = 1178;
	const SAGE_VALIDATION = 1181;

	//const NON_STANDARD_PAYMENT_PRODUCT = 502;
	const NON_STANDARD_PAYMENT_PRODUCT = 628;
	const SESSION_NAMESPACE = 'nonStandardPayments';

	/** @var array */
	public $possibleActions = array(
		self::PAYMENT_PAGE => 'default',
		self::PAYPAL_PAGE => 'paypal',
		self::PAYPAL_CANCEL_PAGE => 'paypalCancel',
		self::PAYPAL_ERROR_PAGE => 'paypalError',
		self::CONFIRMATION_PAGE => 'confirmation',
		self::SAGE_AUTH => 'sageAuthentication',
		self::SAGE_VALIDATION => 'sageValidation',
	);

	/** @var Basket */
	protected $basket;

	/** @var boolean */
	protected $httpsRequired = TRUE;
        /**
         * @var PaymentEmailer 
         */
        public $paymentEmailer;

        public function startup(){
            parent::startup();
            $this->paymentEmailer = $this->getService(DiLocator::EMAILER_PAYMENT);
        }
	public function beforePrepare() {
            // hide left column
            $this->template->hide = 1;
            $this->basket = new Basket('nonStandardPayment');
            parent::beforePrepare();
            $this->template->enableMessages = FProperty::get('enableMessages', PaymentControler::SAGE_PAGE)->value;
            $this->template->paymentPageMessages = FProperty::get('paymentPageMessages', PaymentControler::SAGE_PAGE)->value;
            $this->template->disableSage = FProperty::get('disableSage', PaymentControler::SAGE_PAGE)->value;
	}

	/*	 * *********************************** default ************************************ */

	public function prepareDefault() {
            if ($this->basket->hasOrderId() === TRUE) {
                    $this->redirect(self::CONFIRMATION_PAGE);
            }
	}

	/**
	 * sage auth form validation, sage pay sends post to this method
	 */
	public function renderSageValidation() {
            try {
                           Debug::disableProfiler();
                   //if 3D secure is on - all data will save here
                   if (isset($_POST["PaRes"]) && isset($_SESSION['SagePayDirect'])) {
                       $sage = unserialize($_SESSION['SagePayDirect']);
                       $response = $sage->finish3DSecure($_POST);
                       // process order
                       if (!empty($response)) {
                           if (trim($response['Status']) == 'OK') {
                               $this->saveSagePayment($sage, $response);
                               FForm::cleanForm('nonStandardPaymentForm');
                               SagePayDirect::cleanAuthForm();
                               unset($_SESSION['SagePayDirect']);
                               unset($_SESSION[self::SESSION_NAMESPACE]);
                               $this->template->success = true;
                               $this->template->location = FApplication::$router->secureLink(self::CONFIRMATION_PAGE);
                           } else {
                               SagePayDirect::cleanAuthForm();
                               $msg = trim($response['Status']) . '. ' . trim($response['StatusDetail']);
                               throw new Exception($msg . " If you have any questions regarding this message please contact us +44 (0) 207 608 5500");
                           }
                       } else {
                           SagePayDirect::cleanAuthForm();
                           throw new Exception('ERROR: MSG01. Please contact us on +44 (0) 207 608 5500 or info@madesimplegroup.com');
                       }
                   }
               } catch (Exception $e) {
                   $this->template->failedLocation = FApplication::$router->link(self::PAYMENT_PAGE);
                   $this->template->error = $e->getMessage();
               }
	}

	/**
	 * sage authorization form in iframe (loaded when 3dauth is necessary)
	 */
	public function renderSageAuthentication() {
            Debug::disableProfiler();
            if (isset($_SESSION['sageAuthForm'])) {
                    $this->template->form = $_SESSION['sageAuthForm'];
            }
	}

	public function renderDefault() {
                    $form = new FForm('nonStandardPaymentForm');
            $form->setAction($this->router->link('NonStandardPaymentControler::SAGE_PAGE') . '#authFormBlock');
            $form->setStorable(false);

            // details
            $form->addFieldset('Details');
            $form->addText('amount', 'Amount: *')->size(10)
                    ->addRule(FForm::Required, 'Please Provide Amount')
                    ->addRule(FForm::NUMERIC, 'Please Provide Numeric Amount');
            $form->addText('email', 'Email: *')->size(30)
                    ->addRule(FForm::Required, 'Please Provide Email')
                    ->addRule(FForm::Email, 'Email is not valid')
                    ->addRule(array($this, 'Validator_customerEmailAddress'), 'Email address in not in our database');
            $form->addText('reference', 'Reference: *')->size(50)
                    ->addRule(FForm::Required, 'Please Provide Reference')
                    ->addRule(FForm::MAX_LENGTH, 'Reference has to be long max #1 characters', 255);

            // payment
            $form->addFieldset('Payment');
            $form->addText('cardholder', "Cardholder's Name: *")
                    ->addRule(array($this, 'Validator_requiredFields'), "Cardholder's name is required!")
                    ->addRule('MaxLength', 'Name must be upto 50 characters', array(50));
            $form->addSelect('cardType', 'Card Type: *', SagePayDirect::getCardTypes())
                    ->setFirstOption('--- Select ---')
                    ->addRule(array($this, 'Validator_requiredFields'), "Card type is required!");
            $form->addText('cardNumber', 'Card Number: *')
                    ->addRule(array($this, 'Validator_requiredFields'), 'Card number is required!')
                    ->addRule('MaxLength', 'Card number must be upto 20 characters', array(20));
            $form->addText('issueNumber', 'Issue Number: ')
                    ->size(1)
                    ->addRule(array($this, 'Validator_IssueNumber'), 'Issue number is required!')
                    ->addRule('MaxLength', 'Issue number must be upto 2 characters', array(2));
            $form->addText('CV2', 'Security Code: *')
                    ->size(1)
                    ->addRule(array($this, 'Validator_securityCode'), "Security code must be a three digit number!");
            $form->add('CardExpiryDate', 'expiryDate', 'Expiry Date: *')
                    ->addRule(array($this, 'Validator_ExpiryDate'), array('Expiry date is required!', "Expiry date has to be more than or equal to today's date!"));
            $form->add('CardValidFromDate', 'validFrom', 'Valid from: ');
            $form->addText('address1', 'Address 1: *')
                    ->addRule(array($this, 'Validator_requiredFields'), 'Address is required!')
                    ->addRule('MaxLength', 'Address 1 must be upto 100 characters', array(100));
            $form->addText('address2', 'Address 2: ')
                    ->addRule('MaxLength', 'Address 2 must be upto 50 characters', array(50));
            $form->addText('address3', 'Address 3: ')
                    ->addRule('MaxLength', 'Address 3 must be upto 50 characters', array(50));
            $form->addText('town', 'Town: *')
                    ->addRule(array($this, 'Validator_requiredFields'), 'Town is required!')
                    ->addRule('MaxLength', 'Town must be upto 40 characters', array(40));
            $form->addText('postcode', 'Post Code: *')
                    ->addRule(array($this, 'Validator_requiredFields'), 'Post code is required!')
                    ->addRule('MaxLength', 'Post code must be upto 10 characters', array(10));
            $form->addSelect('country', 'Country: *', WPDirect::$countries)
                    ->setFirstOption('--- Select ---')
                    ->style('width: 150px;')
                    ->addRule(array($this, 'Validator_requiredFields'), 'Country is required!');
            $form->addSelect('billingState', 'State: *', SagePayDirect::getStates());

            // terms
            $form->addFieldset('Terms and Conditions');
            $form->addCheckbox('terms', 'Terms and Conditions', 1)
                    ->addRule(FForm::Required, 'Please accept Terms and Conditions!');

            // action 
            $form->addFieldset('Action');
            $form->addSubmit('submit', 'Submit')->class('btn_submit');

            // paypal
            $form->addFieldset('Or pay using');
            $form->addSubmit('paypal', 'Paypal');

            $form->onValid = array($this, 'Form_detaultFormSubmitted');
            $form->start();
            $this->template->form = $form;
	}

	/**
	 * Provides finish payment  
	 * @param object FForm $form
	 */
	public function Form_detaultFormSubmitted(FForm $form) {
		// get data from form
		$data = $form->getValues();
		$product = FNode::getProductById(self::NON_STANDARD_PAYMENT_PRODUCT);

		// add product to basket
		if ($product->inBasket($this->basket) === FALSE) {
				$product->price = $data['amount'];
				$product->additional = $data['reference'];
				$this->basket->add($product);
		}

		// save form data
		$_SESSION[self::SESSION_NAMESPACE] = array('email' => $data['email']);

		// paypal
		if ($form->isSubmitedBy('paypal')) {
			$this->redirect(self::PAYPAL_PAGE, 'checkout=1');
		}

		// expiry date
		$explode = explode('-', $data['expiryDate']);
		$explode[0] = substr($explode[0], -2);
		if (strlen($explode[1]) < 2) {
			$explode[1] = str_replace($explode[1], '0' . $explode[1], $explode[1]);
		}

		$expiryDate = $explode[1] . $explode[0];
		if (isset($data['cardType']) && $data['cardType'] == SagePayDirect::CARD_MAESTRO) {
			if (isset($data['validFrom'])) {
				$explode = explode('-', $data['validFrom']);
				$explode[0] = substr($explode[0], -2);
				if (strlen($explode[1]) < 2) {
					$explode[1] = str_replace($explode[1], '0' . $explode[1], $explode[1]);
				}
				$validFrom = $explode[1] . $explode[0];
			}
		}

		// Names
		$names = explode(' ', trim($data['cardholder']));
		$firstnames = '';
		foreach ($names as $value) {
			$firstnames .= ' ' . $value;
		}

		$firstnames = str_replace($value, '', $firstnames);
		$temp = str_replace(' ', '', $firstnames);
		if (trim($firstnames) == '' or trim($temp) == '') {
			$firstnames = 'M';
		}
		$surname = $value;
		$firstnames = SagePayDirect::cutString($firstnames, 20);
		$surname = SagePayDirect::cutString($surname, 20);

		if (isset($data['address2']) || isset($data['address3'])) {
			$address2 = '';
			$address2 .= $data['address2'] ? $data['address2'] : '';
			$address2 .= $data['address3'] ? $data['address3'] : '';
		}

		// sage pay direct
		$sage = new SagePayDirect();
        $sageConfig = FApplication::$config['sage'];
        $sage->setVendor($sageConfig['vendor']);
		$sage->setSystem($sageConfig['server']);
        
		$payment = $sage->getPayment();
		$payment->setCard(trim($data['cardholder']), trim($data['cardType']), trim($data['cardNumber']), trim($expiryDate));
		$payment->getCard()->setCVV(trim($data['CV2']));
		$payment->setBillingAddress(trim($firstnames), trim($surname), trim($data['address1']), trim($data['town']), trim($data['postcode']), trim($data['country']));
		$payment->setDeliveryAddressObject();

		if (isset($address2)) {
			$payment->getCard()->getBillingAddress()->setLine2(trim($address2));
			$payment->getDeliveryAddress()->setLine2(trim($address2));
		}

		if (isset($data['issueNumber'])) {
			$payment->getCard()->setIssueNumber(trim($data['issueNumber']));
		}

		if (isset($validFrom)) {
			$payment->getCard()->setStartDate(trim($validFrom));
		}

		if (isset($data['billingState']) && trim($data['country']) == 'US') {
			$payment->getCard()->getBillingAddress()->setState(trim($data['billingState']));
			$payment->getDeliveryAddress()->setState(trim($data['billingState']));
		}

		$payment->setPaymentDetails("Companies Made Simple", $data['amount']);
		$sage->setAction($payment);
			
		try {
			$backUrl = FApplication::$router->secureLink(self::SAGE_VALIDATION);
			$response = $sage->doAction($backUrl);
		} catch (SageAuthenticationException $e) {
			$this->template->authFormUrl = FApplication::$router->secureLink(self::SAGE_AUTH, 'sagepay-3d=1');
			$this->template->showAuth = true;
		} catch (Exception $e) {
			$this->flashMessage($e->getMessage(), 'error');
			$this->readFlashMessages();
		}


		// process order
		if (!empty($response)) {
			if (trim($response['Status']) == 'OK') {
				try {
					$this->saveSagePayment($sage, $response);
					FForm::cleanForm('nonStandardPaymentForm');
					SagePayDirect::cleanAuthForm();
					unset($_SESSION[self::SESSION_NAMESPACE]);
					$this->redirect(self::CONFIRMATION_PAGE);
				} catch (Exception $e) {
					SagePayDirect::cleanAuthForm();
					$this->flashMessage($e->getMessage(), 'error');
					$this->readFlashMessages();
				}
			} else {
				SagePayDirect::cleanAuthForm();
				$msg = trim($response['Status']) . '. ' . trim($response['StatusDetail']);
				$this->flashMessage($msg . " If you have any questions regarding this message please contact us +44 (0) 207 608 5500.", 'error');
				$this->readFlashMessages();
			}
		}
	}

	/*	 * *********************************** paypal ************************************ */

	public function preparePaypal() {
		require_once PROJECT_DIR . '/libs/Paypal/paypal.php';

		$cfg = FApplication::$config['paypal'];

		// do checkout
		if (isSet($this->get['checkout']) && !isSet($this->get['token'])) {

			$returnUrl = FApplication::$httpRequest->uri->scheme . '://' . FApplication::$httpRequest->uri->host . $this->router->link(self::PAYPAL_PAGE);
			$cancelUrl = FApplication::$httpRequest->uri->scheme . '://' . FApplication::$httpRequest->uri->host . $this->router->link(self::PAYPAL_CANCEL_PAGE);

			$paypal = new SetExpressCheckout($this->basket->getPrice('total'));
			$paypal->setPayPalNVP("API_UserName", $cfg['apiUsername']);
			$paypal->setPayPalNVP("API_Password", $cfg['apiPassword']);
			$paypal->setPayPalNVP("API_Signature", $cfg['apiSignature']);
			$paypal->setPayPalNVP("environment", $cfg['environment']);
			$paypal->setNVP("RETURNURL", $returnUrl);
			$paypal->setNVP("CANCELURL", $cancelUrl);
			$paypal->setNVP("L_NAME0", 'Companies Made Simple');
			$paypal->setNVP("L_AMT0", $this->basket->getPrice('total'));
			$paypal->setNVP("L_QTY0", 1);
			$paypal->setNVP("CURRENCYCODE", 'GBP');
            $paypal->setNVP("CUSTOM", 'COMPANIESMADESIMPLE');
			$paypal->getResponse();
		} elseif (isSet($this->get['token'], $this->get['PayerID'])) {

			// DoExpressCheckoutPayment
			$paypal = new DoExpressCheckoutPayment($this->basket->getPrice('total'));
			$paypal->setPayPalNVP("API_UserName", $cfg['apiUsername']);
			$paypal->setPayPalNVP("API_Password", $cfg['apiPassword']);
			$paypal->setPayPalNVP("API_Signature", $cfg['apiSignature']);
			$paypal->setPayPalNVP("environment", $cfg['environment']);
			$paypal->setNVP("CURRENCYCODE", 'GBP');
            $paypal->setNVP("CUSTOM", 'COMPANIESMADESIMPLE');
			$res = $paypal->getResponse();

			$namespace = $_SESSION[self::SESSION_NAMESPACE];
			$customer = Customer::getCustomerByEmail($namespace['email']);

			// save transaction
			$transaction = new Transaction();
			$transaction->customerId = $customer->getId();
			$transaction->typeId = Transaction::TYPE_PAYPAL;

			// error page
			if ($res['ACK'] == 'Success' && isset($res['TRANSACTIONID'])) {
				try {
					$orderService = new OrderService();
					$orderId = $orderService->saveOrder($customer, $this->basket);
					$transaction->orderId = $orderId;
					$transaction->orderCode = $res['TRANSACTIONID'];
					$transaction->save();
					$this->basket->orderId = $orderId;

					$this->paymentEmailer->orderConfirmationEmail($customer, $this->basket, $orderId);
					$this->paymentEmailer->orderConfirmationInternalEmail($customer, $this->basket, $orderId);
                                        
					FForm::cleanForm('nonStandardPaymentForm');
					$this->redirect(self::CONFIRMATION_PAGE);
				} catch (Exception $e) {
					$this->flashMessage($e->getMessage());
					$this->redirect();
				}
			} else {
                                $this->paymentEmailer->paypalErrorEmail($customer, $res);
				// save transaction
				$transaction->error = isSet($res['L_LONGMESSAGE0']) ? $res['L_LONGMESSAGE0'] : 'Unknown';
				$transaction->save();

				// redirect to error page
				$message = isSet($res['L_LONGMESSAGE0']) ? urlencode($res['L_LONGMESSAGE0']) : 'Unknown';
				$message = urldecode($message);
				$this->redirect(self::PAYPAL_ERROR_PAGE, 'errorMessage=' . urlencode($message));
			}
		}

		// redirect to home link
		$this->redirect("%{$this->getHomeLink()}%");
	}

	/*	 * *********************************** paypal error ************************************ */

	public function renderPaypalError() {
		if (isset($this->get['errorMessage'])) {
			$this->template->errorMessage = urldecode($this->get['errorMessage']);
		}
	}

	/*	 * *********************************** confirmation ************************************ */

	public function prepareConfirmation() {
		if ($this->basket->isEmpty() || $this->basket->hasOrderId() === FALSE) {
			$this->redirect("%{$this->getHomeLink()}%");
		}
		$this->template->basket = clone $this->basket;
		$this->template->ecommerce = 1;
		$this->basket->clear();
	}

	/*	 * *********************************** other ************************************ */

	private function saveSagePayment($sage, $response) {
		if (isset($_SESSION[self::SESSION_NAMESPACE])) {
//			try {
				$namespace = $_SESSION[self::SESSION_NAMESPACE];
				$customer = Customer::getCustomerByEmail($namespace['email']);

				// save order
                $orderService = new OrderService();
                $orderId = $orderService->saveOrder($customer, $this->basket);

				// save transaction
				$transaction = new Transaction();
				$transaction->customerId = $customer->getId();
				$transaction->typeId = Transaction::TYPE_SAGEPAY;
				$transaction->cardHolder = $sage->getAction()->getCard()->getCardholderName();
				$transaction->cardNumber = substr($sage->getAction()->getCard()->getCardNumber(), -4);
				$transaction->orderCode = trim($response['VPSTxId']);
				$transaction->details = serialize($response);
				$transaction->orderId = $orderId;
				$transaction->vendorTXCode = $_SESSION['VendorTxCode'];
				$transaction->save();

				// send confirmation email
                                $this->paymentEmailer->orderConfirmationEmail($customer, $this->basket, $orderId);
                                $this->paymentEmailer->orderConfirmationInternalEmail($customer, $this->basket, $orderId);

				$this->basket->setOrderId($orderId);
		} else {
			$this->terminate('ERROR: MSG02. Please contact us on +44 (0) 207 608 5500 or info@madesimplegroup.com');
		}
	}

	/*	 * ******************************** validators ******************************** */

	/**
	 * Check required issue number for maestro and solo
	 * @param object $control
	 * @param string $error
	 * @param mixed $params
	 * @return mixed
	 */
	public function Validator_IssueNumber($control, $error, $params) {
		$cardType = $control->owner['cardType']->getValue();
		if ($cardType == 'maestro' || $cardType == 'solo') {
			if ($control->getValue() == '') {
				return $error;
			}
		}
		return TRUE;
	}

	/**
	 * Check required issue number for maestro and solo
	 * @param object $control
	 * @param string $error
	 * @param mixed $params
	 * @return mixed
	 */
	public function Validator_ExpiryDate($control, $error, $params) {
		if ($control->owner->isSubmitedBy('submit')) {
			// required
			if ($control->getValue() == NULL) {
				return $error[0];
			}
			// greater than
			$expireDate = $control->owner['expiryDate']->getValue();
			if (strtotime($expireDate) < strtotime(date("Y-m"))) {
				return $error[1];
			}
		}
		return TRUE;
	}

	/**
	 * Check required fields fo card payment
	 *
	 * @param object $control
	 * @param string $error
	 * @param array $params
	 * @return mixed
	 */
	public function Validator_requiredFields($control, $error, $params) {
		if ($control->owner->isSubmitedBy('submit')) {
			$value = $control->getValue();
			if (empty($value)) {
				return $error;
			}
		}
		return TRUE;
	}

	public function Validator_securityCode($control, $error, $params) {
		if ($control->owner->isSubmitedBy('submit')) {
			$value = $control->getValue();
			if (empty($value) || !is_numeric($value) || strlen($value) != 3) {
				return $error;
			}
		}
		return true;
	}

	public function Validator_customerEmailAddress($control, $error, $params) {
		try {
			$email = $control->getValue();
			Customer::getCustomerByEmail($email);
			return TRUE;
		} catch (Exception $e) {
			return $error;
		}
	}
}
