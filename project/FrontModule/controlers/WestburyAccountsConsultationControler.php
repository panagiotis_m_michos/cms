<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    WestburyAccountsConsultationControler.php 2010-09-20 divak@gmail.com
 */

class WestburyAccountsConsultationControler extends DefaultControler
{
	/**
	 * @var string
	 */
	static public $handleObject = 'WestburyAccountsConsultationModel';

	/**
	 * @var WestburyAccountsConsultationModel
	 */
	public $node;
	
	
	public function handleDefault()
	{
		$this->template->form = $this->node->getComponentDefaultForm(array($this, 'Form_defaultFormSubmitted'));
	}
	
	public function Form_defaultFormSubmitted($form)
	{
		try {
			$this->node->processDefaultForm($form);
			$this->flashMessage('Your query has been sent succesfully.');
			$this->redirect();
		} catch (Exception $e) {
		    $this->flashMessage($e->getMessage(), 'error');
		 	$this->redirect();
		}
	}	
}