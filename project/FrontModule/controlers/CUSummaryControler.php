<?php

use BankingModule\BankingDecider;
use CHFiling\Exceptions\InvalidCompanyAuthenticationCodeException;
use CompanyUpdateModule\Factories\CUSummaryControlerViewFactory;
use Factories\Front\CompanyViewFactory;
use PeopleWithSignificantControl\Views\CompanySummaryView;

class CUSummaryControler extends CUControler
{
    const SUMMARY_PAGE = 205;
    const BARCLAYS_PAGE = 343;
    const RESIGNATION_PAGE = 1229;

    /** 
     * @var array 
     */
    public $possibleActions = [
        self::SUMMARY_PAGE => 'summary',
        self::BARCLAYS_PAGE => 'barclays',
        self::RESIGNATION_PAGE => 'resign'
    ];

    /** 
     * @var string 
     */
    static public $handleObject = 'CUSummaryModel';

    /**
     * @var CUSummaryModel
     */
    public $node;

    /**
     * @var CompanyViewFactory
     */
    private $companyViewFactory;

    /**
     * @var CUSummaryControlerViewFactory
     */
    private $controllerViewFactory;

    /**
     * @var BankingDecider
     */
    private $bankingDecider;

    public function startup()
    {
        // fix for IE
        if (isset($this->get['document_name'])) {
            $this->httpsRequired = FALSE; 
        }
        parent::startup();

        $this->companyViewFactory = $this->getService(DiLocator::FACTORY_FRONT_COMPANY_VIEW);
        $this->controllerViewFactory = $this->getService('company_update_module.factories.cusummary_controler_view_factory');
        $this->bankingDecider = $this->getService('banking_module.banking_decider');
    }

    public function beforePrepare()
    {
        if ($this->action == $this->possibleActions[self::RESIGNATION_PAGE] 
            || $this->action == $this->possibleActions[self::SUMMARY_PAGE] && isset($this->get['sync'])
        ) {
            $this->authCodeRequired = TRUE;
        }

        parent::beforePrepare();
        $this->node->startup($this->company, $this->customer);
    }

    /*     * ********************************** summary *********************************** */

    /**
     * Handle with: 
     * - display document
     * - sync company
     * - display subscribe certificate
     *
     * @return void
     */
    public function handleSummary()
    {
        // --- show document ---
        if (isset($this->get['document_name'])) {
            try {
                $this->node->outputDocument($this->get['document_name']);
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage());
                $this->redirect(NULL, 'document_name=');
            }
        }
        // --- show rtf document ---
        if (isset($this->get['formid'])) {
            if (RTFDocuments::getFormSubmisionStatus($this->get['formid'])) {
                try {
                    $Rtf = new RTFDocuments($this->get['company_id'], $this->get['formid']);
                    $Rtf->output();
                } catch (Exception $e) {
                    $this->flashMessage($e->getMessage());
                    $this->redirect(NULL);
                }
            } else {
                $this->flashMessage('Your form id is not correct');
                $this->redirect(self::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
            }
        }
        // --- user clicked sync button ---
        if (isset($this->get['sync'])) {
            try {
                $this->node->syncCompany();
                $this->flashMessage('Your company has been synchronised with data from Companies House.');
                $this->redirect(NULL, 'sync=');
            } catch (InvalidCompanyAuthenticationCodeException $e) {
                $this->flashMessage('The company sync was unsuccessful because the authentication code we have is invalid - see <a href="http://support.companiesmadesimple.com/article/88-authentication-code#invalid" target="_blank">here</a> for information on getting a new one.', 'error', FALSE);
                $this->redirect(NULL, 'sync=');
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
                $this->redirect(NULL, 'sync=');
            }
        }

        // --- show subscriber certificate ---
        if (isset($this->get['subscriberCertificate'])) {
            try {
                $this->node->outputSubscriberCertificate($this->get['subscriberCertificate']);
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
                $this->redirect(NULL, 'subscriberCertificate=');
            }
        }
        // --- show company summary ---
        if (isset($this->get['summary'])) {
            try {
                $this->node->outputCompanySummary();
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
                $this->redirect(NULL, 'summary=');
            }
        }
    }

    protected function prepareSummary()
    {
        // --- change view for locked company ---
        if ($this->company->isLocked()) {
            $this->changeView('summaryLocked');
        }
    }

    protected function renderSummary()
    {
        // --- check if package is bronze = don't show subsribers link ---
        if ($this->node->hasAllowedSubscriberCertificates() === FALSE) {
            $this->template->doNotShowSubsLink = 1;
        }

        // --- display barclays link if haven't submitted
        if ($this->node->displayBarclaysBox()) {
            $this->template->barclays = 1;
        }

        $this->template->view = $this->controllerViewFactory->create($this->companyEntity);
        $this->template->companyView = $this->companyViewFactory->create($this->companyEntity);
        $this->template->summaryView = new CompanySummaryView($this->company);
        $this->template->bronze = Order::hasItem($this->company->getOrderId(), Package::PACKAGE_BRONZE);
        $this->template->title = $this->company->getCompanyName();
        $this->template->data = $this->company->getData();
        $this->template->documents = $this->company->getDocumentsList();
        $this->template->otherdocuments = CompanyOtherDocument::getAllObjects(NULL, NULL, ['companyId' => $this->company->getCompanyId()]);
        $this->template->hasBronzeProduct = Order::hasItem($this->company->getOrderId(), Package::PACKAGE_BRONZE);
        $this->template->bankingView = $this->bankingDecider->getCashBackOptions($this->companyEntity);
    }

    /*     * ********************************** barclays *********************************** */

    public function prepareBarclays()
    {
        try {
            if ($this->customer->countryId != 223) {
                throw new Exception('We are unable to process your Barclays application at this time. Barclays can only open bank accounts for UK customers and your current contact address is international. If you reside in the UK, please update your <a href="' . $this->router->link(MyDetailsControler::MY_DETAILS_PAGE) . '">My Details</a> and retry.');
            }
            $this->node->checkBarclaysPage();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect(self::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
        }
    }

    public function renderBarclays()
    {
        $this->template->form = $this->node->getComponentBarclaysForm([$this, 'Form_barclaysFormSubmitted']);
    }

    /**
     * @param FForm $form
     */
    public function Form_barclaysFormSubmitted(FForm $form)
    {
        try {
            $this->node->processBarclaysForm($form);
            $this->flashMessage('Request has been sent');
            $this->redirect(self::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    public function renderResign()
    {
        if (isset($this->get['resign']) && isset($this->get['officer_id'])) {
            try {
                $officer = $this->company->getOfficer($this->get['officer_id']);
            } catch (Exception $e) {
                $this->flashMessage('Officer record not exist');
                $this->reloadOpener();
            }
            $resignForm = new CUResignForm($this->node->getId() . '_resign');
            $resignForm->startup($this, $this->company, $officer, [$this, 'Form_ResignFormSubmitted']);
            $this->template->form = $resignForm;
            $this->template->companyType = $this->company->getType();
        } else {
            $this->flashMessage('Officer record not exist');
            $this->redirect(self::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
        }
    }

    /**
     * @param CUResignForm $form
     */
    public function Form_ResignFormSubmitted(CUResignForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Application has been sent.');
            $this->reloadOpener();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect(self::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
        }
    }

}
