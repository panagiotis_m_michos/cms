<?php


class CompanyDividendsControler extends CUControler
{
	
    /* live */
	const DIVIDENDS_PAGE = 699;
	const ADD_DIVIDEND_PAGE = 700;
	const SHAREHOLDERS_PAGE = 701;
	const SHAREHOLDER_PAGE = 702;

	/** @var array */
	public $possibleActions = array(
		self::DIVIDENDS_PAGE => 'dividends',
		self::ADD_DIVIDEND_PAGE => 'addDividend',
		self::SHAREHOLDERS_PAGE => 'shareholders',
		self::SHAREHOLDER_PAGE => 'shareholder',
	);


	/**
	 * @var CompanyDividendsModel
	 */
	private $model;

	/** @var int */ 
	private $shareholdersId;

	/** @var int */ 
	private $shareholderId;


	public function beforePrepare()
	{
        parent::beforePrepare();

        try {
	        // only for wholesale
	        if ($this->customer->roleId != Customer::ROLE_WHOLESALE) {
	        	throw new Exception('Dividends are available only for Wholesale customers.');
	        }
        	
        	$this->model = new CompanyDividendsModel($this->company);
        	$this->shareholdersId = isset($this->get['shareholdersId']) ? $this->get['shareholdersId'] : NULL;
        	$this->shareholderId = isset($this->get['shareholderId']) ? $this->get['shareholderId'] : NULL;
        } catch (Exception $e) {
        	$this->flashMessage($e->getMessage(), 'error');
        	$this->redirect(CUSummaryControler::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
        }
	}



	/*********************************************** dividends ******************************************************/


	public function handleDividends()
	{
		if (isset($this->get['removeShareholdersId'])) {
			$this->model->removeDividend($this->get['removeShareholdersId']);
			$this->redirect(null, "removeShareholdersId=");
		}
		if (isset($this->get['printReport'])) {
			$this->model->getPdfReport($this->get['printReport']);
			$this->redirect(null, "printReport=");
		}
        if(isset($this->get['RtfMinutes'])){
            $Rtf = new RTFDocuments($this->get['company_id'], NULL, $this->get['RtfMinutes']);
            $Rtf->output();
        }
	}

	public function renderDividends()
	{
		try {
            $sessionNr = FApplication::$session->getNamespace('companyDividendsShareholders');
            if(isset($sessionNr->shareholdersCount)) {
                $sessionNr->shareholdersCount = NULL;
            }
			$this->template->dividends = $this->model->getDividends();
		} catch (Exception $e) {
			$this->flashMessage($e->getMessage(), 'error');
			$this->redirect(CUSummaryControler::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
		}
	}


	/*********************************************** add dividend ******************************************************/

	
	public function handleAddDividend()
	{
        $prefillShareholders = CompanyDividendsModel::getPrefillShareholders($this->company);
        $prefillShares = CompanyDividendsModel::getPrefillShares($this->company);
        $prefillOfficers = CompanyDividendsModel::getPrefillOfficers($this->company);

		/* AJAX  - add shareholder */
		if (isset($this->get['ajaxAddShareholder'])) {
			$this->model->addShareholderField();
			$form  = $this->model->getAddDividendComponentForm($this, $prefillShareholders, $prefillShares, $prefillOfficers);
			$this->terminate($form);
		}
		
		/* AJAX  - remove shareholder */
		if (isset($this->get['ajaxRemoveShareholder'])) {
			$this->model->removeShareholderField();
			$form  = $this->model->getAddDividendComponentForm($this, $prefillShareholders, $prefillShares, $prefillOfficers);
			$this->terminate($form);
		}
	}

	public function renderAddDividend()
	{
		try {
            
            
			$prefillShareholders = CompanyDividendsModel::getPrefillShareholders($this->company);
            $prefillShares = CompanyDividendsModel::getPrefillShares($this->company);
            $prefillOfficers = CompanyDividendsModel::getPrefillOfficers($this->company);
            $this->template->jsPrefillShareholders = $prefillShareholders['js'];
            $this->template->jsPrefillShares = $prefillShares['js'];
            $this->template->jsPrefillOfficers = $prefillOfficers['js'];
            
            if(isset($this->get['cloneShareholdersId'])){
                $form = $this->model->getAddDividendComponentForm($this, $prefillShareholders, $prefillShares, $prefillOfficers, $this->get['cloneShareholdersId']);
            }else{
                $sessionNr = FApplication::$session->getNamespace('companyDividendsShareholders');
                if(!isset($sessionNr->shareholdersCount)) {
                    $sessionNr->shareholdersCount = 1;
                }
                $form = $this->model->getAddDividendComponentForm($this, $prefillShareholders, $prefillShares, $prefillOfficers);
            }
            
            $this->template->form = $form;
		} catch (Exception $e) {
			$this->flashMessage($e->getMessage(), 'error');
			$this->redirect(self::DIVIDENDS_PAGE, "company_id={$this->company->getCompanyId()}");
		}
	}
    

	public function Form_addDividendSubmitted(FForm $form)
	{
		try {
			$data = $form->getValues();
			$this->model->saveDividendData($data);
			$this->flashMessage('Dividend has been created');
			$this->redirect(self::DIVIDENDS_PAGE, "company_id={$this->company->getCompanyId()}");
		} catch (Exception $e) {
			$this->flashMessage($e->getMessage(), 'error');
			$this->redirect();
		}
	}
    


	/*********************************************** shareholders ******************************************************/


	public function handleShareholders()
	{
		if (isset($this->get['print'])) {
            debug::disableProfiler();
			$this->model->outputPdfShareholdersVoucher($this->shareholdersId);
		}
	}


	public function renderShareholders()
	{
		try {
			$this->template->shareholdersId = $this->shareholdersId;
			$this->template->shareholders = $this->model->getShareholders($this->shareholdersId);
		} catch (Exception $e) {
			$this->flashMessage($e->getMessage(), 'error');
			$this->redirect(self::DIVIDENDS_PAGE, "company_id={$this->company->getCompanyId()}");
		}
	}


	/*********************************************** shareholder ******************************************************/

	public function handleShareholder()
	{
		if (isset($this->get['print'])) {
            debug::disableProfiler();
			$this->model->outputPdfShareholderVoucher($this->shareholderId, $this->shareholdersId);
		}
	}


	public function renderShareholder()
	{
		try {
			$this->template->shareholdersId = $this->shareholdersId;
			$this->template->shareholder = $this->model->getShareholder($this->shareholderId);
		} catch (Exception $e) {
			$this->flashMessage($e->getMessage(), 'error');
			$this->redirect(self::SHAREHOLDERS_PAGE, "company_id={$this->company->getCompanyId()}");
		}
	}







}
