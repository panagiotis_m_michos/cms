<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    ContactControler.php 2009-01-08 divak@gmail.com
 */

class ContactControler extends DefaultControler
{
	/**
	 * @return void
	 */
	protected function handleDefault()
	{
		$paginator = new Paginator2(100);
		$this->template->paginator = $paginator->render();
	}
}
?>