<?php

class AccountantsInstituteControler extends FControler
{
    /* folder */
    const ACCOUNTANTS_INSTITUTE_FOLDER = 949;

    /* pages */
    const HOME_PAGE = 950;
    const SIGN_UP_PAGE = 951;
    const CONTACT_US_PAGE = 952;
    const LOGIN_PAGE = 953;
    const FORGOTTEN_PASSWORD = 1121;
    //    const AFFILIATE_PAGE = 723;
    //    const WHITE_PAGE = 724;
    //    const ACCOUNTANTS_PAGE = 725;
    //    const ABOUT_US_PAGE = 719;
    //    const FAQ_PAGE = 720;
    /**
     * @var array 
     */
    public $possibleActions = array(
        self::HOME_PAGE => 'home',
        self::SIGN_UP_PAGE => 'signup',
        self::CONTACT_US_PAGE => 'contact',
        self::LOGIN_PAGE => 'login',
        self::FORGOTTEN_PASSWORD => 'forgottenPassword',
    //		self::AFFILIATE_PAGE => 'affiliate_wholesale',
    //		self::WHITE_PAGE => 'white_label',
    //		self::ACCOUNTANTS_PAGE => 'accountants_and_solicitors',
    //      self::ABOUT_US_PAGE => 'default',
    //      self::FAQ_PAGE => 'faq'
    );
    /**
     * @var string
     */
    static public $handleObject = 'AccountantsInstituteModel';
    /**
     * @var AccountantsInstituteModel
     */
    public $node;
    /**
     * If https is reuquired, than redirect
     *
     * @var Boolean
     */
    protected $httpsRequired;
    
    /**
     * @var AccountantsInstituteEmailer
     */
    protected $accountantsInstituteEmailer;

    public function startup()
    {
        if ($this->nodeId == self::LOGIN_PAGE || $this->nodeId == self::SIGN_UP_PAGE) {
            $this->httpsRequired = TRUE;
        }
        parent::startup();
        $this->accountantsInstituteEmailer = $this->emailerFactory->get(EmailerFactory::ACCOUNTS_INSTITUTE);
    }

    public function beforeRender()
    {
        $this->template->topMenu = $this->node->getTopMenu();
        $this->template->templateRightImage = $this->node->getTemplateRightImage();
        parent::beforeRender();
    }

    /*     * ********************************************* home ********************************************** */

    public function renderHome()
    {
        $this->template->slideShow = $this->node->getHomeSlideShow();
    }

    /*     * ********************************************* signup ********************************************** */

    public function renderSignup()
    {
        $signupForm = new AccountantsInstituteSignupForm($this->node->getId() . '_signup');
        $signupForm->startup($this, array($this, 'Form_signupFormSubmitted'), $this->accountantsInstituteEmailer);
        $this->template->form = $signupForm;
    }

    /**
     * @param AccountantsInstituteSignupForm $form
     */
    public function Form_signupFormSubmitted(AccountantsInstituteSignupForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Your account has been created');
            $this->redirect(DashboardCustomerControler::DASHBOARD_PAGE);
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('signup');
        }
    }

    /*     * ********************************************* login ********************************************** */

    public function renderLogin()
    {
        $loginForm = new AccountantsInstituteLoginForm('login');
        $loginForm->startup($this, array($this, 'Form_loginFormSubmitted'));
        $this->template->form = $loginForm;
    }

    /**
     * @param ICAEWWholesaleLoginForm $form
     */
    public function Form_loginFormSubmitted(AccountantsInstituteLoginForm $form)
    {
        try {
            $form->process();
            $this->redirect(DashboardCustomerControler::DASHBOARD_PAGE);
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('login');
        }
    }

    /*     * ********************************************* contact ********************************************** */

    public function renderContact()
    {
        $contactForm = new AccountantsInstituteContactForm($this->node->getId() . '_contact');
        $contactForm->startup($this, array($this, 'Form_contactFormSubmitted'), $this->accountantsInstituteEmailer);
        $this->template->form = $contactForm;
    }

    /**
     * @param AccountantsInstituteContactForm $form
     */
    public function Form_contactFormSubmitted(AccountantsInstituteContactForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Message has been sent');
            $this->redirect('contact');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('contact');
        }
    }

    public function renderforgottenPassword()
    {
        $form = new AccountantsInstituteForgottenPasswordForm($this->node->getId() . '_ForgottenPassword');
        $form->startup($this, array($this, 'Form_ForgottenPasswordFormSubmitted'), $this->accountantsInstituteEmailer);
        $this->template->form = $form;
    }

    /**
     * @param AccountantsInstituteForgottenPasswordForm $form
     */
    public function Form_ForgottenPasswordFormSubmitted(AccountantsInstituteForgottenPasswordForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('New password has been sent to your email.');
            $this->redirect(AccountantsInstituteControler::LOGIN_PAGE);
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }


}
