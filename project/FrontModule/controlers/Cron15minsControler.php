<?php

class Cron15minsControler extends DefaultControler
{

    public function prepareDefault()
    {
        if (isset($this->get['hash']) && $this->get['hash'] == CRON_HASH) {
            $this->handleSubmissions(); // polling
            $this->terminate();
        }
    }

    /**
     * checks all submissions. This is used by cron job
     */
    private function handleSubmissions()
    {

        /* check if autopolling is set to true */
        $settings = SettingsModel::getSettingsByKey('autopolling');
        if (!$settings->getValue()) {
            echo 'Autopolling is not allowed';
            return;
        }

        try {
            PollingModel::processFormSubmissionStatus();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        echo 'ok';
    }

}
