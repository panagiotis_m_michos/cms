<?php


class CronJobsControler extends BaseControler
{

    const SOLE_TRADER_PLUS = 400;
    const RACN = 955;

    /**
     * @var CronJobEmailer
     */
    protected $cronJobEmailer;

    public function startup()
    {
        parent::startup();
        $this->cronJobEmailer = $this->emailerFactory->get(EmailerFactory::CRON_JOB);
        Debug::disableProfiler();
    }

    public function beforePrepare()
    {
        // check for hash
        if (isset($this->get['hash']) && $this->get['hash'] == CRON_HASH) {
            try {
                $this->welcomeEmail();
            } catch (Exception $e) {
            }
            try {
                $this->automatedCompanyResponseEmail();
            } catch (Exception $e) {
            }
            try {
                // @TODO: should be removed
                $this->barclays();
            } catch (Exception $e) {
            }
            try {
                JourneyModel::sendMidnightCsv();
            } catch (Exception $e) {
            }
        }
        $this->terminate('done');
        parent::beforePrepare();
    }



    /************************************* Barclays *************************************/


    /**
     * Sending xml to Barclays bank
     * @return void
     */
    private function barclays()
    {
        /* when changing test to false, delete all test transactions from database */
        /* so getCompaniesIds will return also ids that has been sent as a test */
        $test = FALSE;
        $ids = Barclays::getCompaniesIds();
        foreach ($ids as $id) {
            $return = Barclays::sendCompany($id, $test);
        }
    }



    /************************************* welcome email *************************************/


    /**
     * Sending welcome email from HG to all new Company Formation customers (except wholesalers)
     * @return void
     */
    private function welcomeEmail()
    {
        $arr = dibi::select('c.firstName, c.email, co.company_name')
            ->from(TBL_ORDERS . ' o')
            ->join('ch_company co')->on('o.orderId=co.order_id')
            ->join(TBL_CUSTOMERS . ' c')->on('c.customerId=o.customerId')
            ->where('co.company_number IS NOT NULL')
            ->and('TO_DAYS(NOW()) - TO_DAYS(o.dtc) = 7')
            ->and('c.roleId=%s', Customer::ROLE_NORMAL)
            ->execute()->fetchAll();

        // send email to customer
        foreach ($arr as $key => $row) {
            $this->cronJobEmailer->welcomeEmail($row);
        }
        pr('Welcome Emails Sent: ' . count($arr));
    }


    /************************************* new company response *************************************/

    /**
     * Sending email from the team to all customers who have not incorporated their company
     * @return void
     */
    private function automatedCompanyResponseEmail()
    {
        $arr = dibi::select('c.firstName, c.email, co.company_name, co.order_id')
            ->from(TBL_ORDERS . ' o')
            ->join('ch_company co')->on('o.orderId=co.order_id')
            ->join(TBL_CUSTOMERS . ' c')->on('c.customerId=o.customerId')
            ->where('co.company_number IS NULL')
            ->and('c.roleId=%s', Customer::ROLE_NORMAL)
            ->and('TO_DAYS(NOW()) - TO_DAYS(o.dtc) = 7')
            ->execute()->fetchAll();


        // send email to customer
        foreach ($arr as $key => $row) {
            // don't send email for 'Sole Trader Plus'  and  'Reserve a Company Name' products
            if (!Order::hasItem($row->order_id, self::SOLE_TRADER_PLUS) || !Order::hasItem($row->order_id, self::RACN)) {
                $this->cronJobEmailer->notIncorporatedCompanyEmail($row);
            }
        }
        pr('Automated Company Reponse Emails Sent: ' . count($arr));
    }
}