<?php
class PaypalErrorControler extends DefaultControler
{
	
	public function beforePrepare()
	{
		parent::beforePrepare();
	}
	
	public function handleDefault()
	{		
		if (isset($this->get['message']))
		{
			$this->template->message = urldecode($this->get['message']);
		}	
	}
	
}
?>