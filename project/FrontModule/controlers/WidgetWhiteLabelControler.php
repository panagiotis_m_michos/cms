<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    WidgetWhiteLabelControler.php 2010-11-12 divak@gmail.com
 */


class WidgetWhiteLabelControler extends BaseControler
{
	/* vevericka */
//	const HOME_PAGE = 600;
//	const PACKAGES_PAGE = 601;
//	const LOGIN_PAGE = 602;
//	const SUMMARY_PAGE = 603;
//	const REDIRECT_PAGE = 604;
//	const DEMO_PAGE = 747;
	
	/* live */
	const HOME_PAGE = 858;
	const PACKAGES_PAGE = 859;
	const LOGIN_PAGE = 860;
	const SUMMARY_PAGE = 861;
	const REDIRECT_PAGE = 862;
	const DEMO_PAGE = 863;
	
	/**
	 * @var array
	 */
	public $possibleActions = array(
		self::HOME_PAGE => 'home',
		self::PACKAGES_PAGE => 'packages',
		self::LOGIN_PAGE => 'login',
		self::SUMMARY_PAGE => 'summary',
		self::REDIRECT_PAGE => 'redirect',
		self::DEMO_PAGE => 'demo',
	);
	
	/**
	 * @var string
	 */
	static public $handleObject = 'WidgetWhiteLabelModel';
	
	/**
	 * @var WidgetWhiteLabelModel
	 */
	public $node;
	
	/**
	 * @var Boolean
	 */
	protected $httpsRequired = TRUE;
	
    /**
     * @var PaymentEmailer 
     */
    public $paymentEmailer;

	public function startup()
	{
		parent::startup();
		$this->paymentEmailer = $this->getService(DiLocator::EMAILER_PAYMENT);
	}
    
	/**
	 * Some checking and startup
	 */
	public function beforePrepare()
	{
		parent::beforePrepare();
		
		// --- for IE (Blocking iFrame Cookies) ---
		Header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
		
		try {
			if (isset($this->get['hash'])) {
				$this->node->startup($this->get['hash']);
			} else {
				throw new Exception("Missing Hash Parameter");
			}	
		} catch (Exception $e) {
		    $this->flashMessage($e->getMessage(), 'error');
		 	$this->redirect("%{$this->getHomeLink()}%");
		}
	}
	
	/**
	 * @return void
	 */
	public function beforeRender()
	{
		$affiliate = $this->node->getAffiliate();
		$this->template->affiliate = $affiliate;
		$this->template->companyColour = $affiliate->companyColour;
		$this->template->textColour = $affiliate->textColour;
		$this->template->companyName = $this->node->getCompanyName();
		$this->template->logoutLink = FApplication::$router->link(self::LOGIN_PAGE, 'l=1', "hash={$this->node->getAffiliate()->hash}");
		parent::beforeRender();
	}
	
	
	
	/********************************************** home **********************************************/

	
	public function renderHome()
	{
		$this->template->searchForm = $this->node->getComponentHomeSearchForm(array($this, 'Form_homeSearchFormSubmitted'));
		if (Customer::isSignedIn() === FALSE) {
			$this->template->loginForm = $this->node->getComponentLoginForm(array($this, 'Form_homeLoginFormSubmitted'));
		}
	}
	
	/**
	 * @param FForm $form
	 * @throws Exception
	 */
	public function Form_homeSearchFormSubmitted(FForm $form)
	{
		try {
			$data = $form->getValues();
			$form->clean();
			$this->redirect(self::PACKAGES_PAGE, array('hash' => $this->node->getAffiliate()->hash, 'companyName' => $data['companyName']));
		} catch (Exception $e) {
		    $this->flashMessage($e->getMessage(), 'error');
		 	$this->redirect();
		}
	}
	
	/**
	 * @param FForm $form
	 * @throws Exception
	 */
	public function Form_homeLoginFormSubmitted(FForm $form)
	{
		try {
			$this->node->processLoginForm($form);
			$this->redirect(self::REDIRECT_PAGE, array('hash' => $this->node->getAffiliate()->hash));
		} catch (Exception $e) {
		    $this->flashMessage($e->getMessage(), 'error');
		 	$this->redirect();
		}
	}
	
	
	/********************************************** packages **********************************************/

	
	public function preparePackages()
	{
		// --- check if has searched company name ---
		if (!isset($this->get['companyName']) || empty($this->get['companyName'])) {
			$this->redirect(self::HOME_PAGE, array('hash' => $this->node->getAffiliate()->hash));
		} else {
			$this->node->setCompanyName($this->get['companyName']);
		}
		// --- clear basket ---
		$this->node->getBasket()->clear(FALSE);
	}
	
	
	public function renderPackages()
	{
		$this->template->form = $this->node->getComponentPackagesForm(array($this, 'Form_packagesFormSubmitted'));
		$this->template->packages = $this->node->getAffiliate()->getPackages();
	}
	
	/**
	 * @param FForm $form
	 */
	public function Form_packagesFormSubmitted(FForm $form)
	{
		try {
			$this->node->processPackagesForm($form);
			if (Customer::isSignedIn() === TRUE) {
				$this->redirect(self::SUMMARY_PAGE, array('hash' => $this->node->getAffiliate()->hash));
			} else {
				$this->redirect(self::LOGIN_PAGE, array('hash' => $this->node->getAffiliate()->hash));	
			}
		} catch (Exception $e) {
			$this->flashMessage($e->getMessage(), 'error');
			$this->redirect();
		}
	}
	
	
	/********************************************** login **********************************************/
	
	
	public function prepareLogin()
	{
		// --- logout ---
		if (isset($this->get['l']) && Customer::isSignedIn() === TRUE) {
			Customer::signOut();
			$this->redirect(self::HOME_PAGE, array('hash' => $this->node->getAffiliate()->hash));
		}
		// signed and not empty basket
		if (Customer::isSignedIn() || $this->node->getBasket()->isEmpty()) {
			$this->redirect(self::HOME_PAGE, array('hash' => $this->node->getAffiliate()->hash));
		}
	}
	
	
	public function renderLogin()
	{
		$this->template->loginForm = $this->node->getComponentLoginForm(array($this, 'Form_loginLoginFormSubmitted'));
		$this->template->registrationForm = $this->node->getComponentRegistrationForm(array($this, 'Form_loginRegistrationFormSubmitted'));
	}
	
	/**
	 * @param FForm $form
	 */
	public function Form_loginLoginFormSubmitted(FForm $form)
	{
		try {
			$this->node->processLoginForm($form);
			$this->redirect(self::SUMMARY_PAGE, array('hash' => $this->node->getAffiliate()->hash));
		} catch (Exception $e) {
		    $this->flashMessage($e->getMessage(), 'error');
		 	$this->redirect();
		}
	}
	
	/**
	 * @param FForm $form
	 */
	public function Form_loginRegistrationFormSubmitted(FForm $form)
	{
		try {
			$this->node->processRegistrationForm($form);			
			$this->redirect(self::SUMMARY_PAGE, array('hash' => $this->node->getAffiliate()->hash));
		} catch (Exception $e) {
		    $this->flashMessage($e->getMessage(), 'error');
		 	$this->redirect();
		}
	}
	
	
	/********************************************** summary **********************************************/
	
	
	public function prepareSummary()
	{
		// signed and not empty basket
		if (!Customer::isSignedIn() || $this->node->getBasket()->isEmpty()) {
			$this->redirect(self::HOME_PAGE, array('hash' => $this->node->getAffiliate()->hash));
		}
	}
	
	
	public function renderSummary()
	{
		$this->template->table = $this->paymentEmailer->invoiceTable($this->node->getBasket(), 'htmltable');
		$this->template->form = $this->node->getComponentSummaryForm();
	}
	
	
	/********************************************** redirect **********************************************/
	
	
	public function renderRedirect()
	{
		$this->template->url = $this->getService('router_module.helpers.controller_helper')->getUrl('payment');
	}
	
	
	/********************************************** demo **********************************************/
	
	
	public function renderDemo()
	{
		Debug::disableProfiler();
		$this->template->iframeUrl = FApplication::$router->link(self::HOME_PAGE, array('hash' => $this->node->getAffiliate()->hash));
	}
}
