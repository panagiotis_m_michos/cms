<?php

use Wholesale\Campaign50Form;
use Wholesale\ICampaign50FormDelegate;
use Wholesale\ContactForm;
use Wholesale\IContactFormDelegate;
use Wholesale\IEmailer;
use Wholesale\ISignUpFormDelegate;
use Wholesale\SignUpForm;

abstract class WholesaleControler_Abstract extends BaseControler implements ISignUpFormDelegate, IContactFormDelegate, ICampaign50FormDelegate
{

    public function beforeRender()
    {
        parent::beforeRender();
        $this->template->menu = $this->getMenu();
    }
    
    /****************************************** home ******************************************/
    public function handleHome()
    {
        $form = new Campaign50Form($this->node->getId() . '_campaign');
        $form->startup(
            $this
        );
        $this->template->form = $form;
    }

    /****************************************** signUp ******************************************/

    public function renderSignUp()
    {
        $form = new SignUpForm($this->node->getId() . '_signUp');
        $form->startup(
            $this,
            $this->getEmailer(),
            $this->getTag(),
            $this->getService(DiLocator::DISPATCHER)
        );
        $this->template->form = $form;
    }

    /****************************************** contact ******************************************/

    public function renderContact()
    {
        $form = new ContactForm($this->node->getId() . '_contact');
        $form->startup($this, $this->getEmailer());
        $this->template->form = $form;
    }

    /****************************************** ISignUpFormDelegate ******************************************/

    public function wholeSaleSignUpFormSucceeded()
    {
        $this->flashMessage('Your account has been created');
        $this->redirect(DashboardCustomerControler::DASHBOARD_PAGE);
    }

    /**
     * @param Exception $e
     */
    public function wholeSaleSignUpFormFailed(Exception $e)
    {
        $this->flashMessage($e->getMessage(), 'error');
        $this->redirect();
    }

    /****************************************** IContactFormDelegate ******************************************/

    public function wholeSaleContactFormSucceeded()
    {
        $this->flashMessage('Message has been sent');
        $this->redirect();
    }

    /**
     * @param Exception $e
     */
    public function wholeSaleContactFormFailed(Exception $e)
    {
        $this->flashMessage($e->getMessage(), 'error');
        $this->redirect();
    }
    
    /****************************************** ICampaign50FormDelegate ******************************************/

    public function wholeSaleCampaign50FormSucceeded()
    {
        $this->flashMessage('Thank you for completing our form. If you qualify your credit will be added within 5 working days. We will notify you via email once it has been added.');
        $this->redirect();
    }

    /**
     * @param Exception $e
     */
    public function wholeSaleCampaign50FormFailed(Exception $e)
    {
        $this->flashMessage($e->getMessage(), 'error');
        $this->redirect();
    }

    /****************************************** protected ******************************************/

    /**
     * @return array
     */
    abstract protected function getMenuItems();

    /**
     * @return string
     */
    abstract protected function getTag();

    /**
     * @return IEmailer
     */
    abstract protected function getEmailer();

    /**
     * @return array
     */
    protected function getMenu()
    {
        $menu = array();
        foreach ($this->getMenuItems() as $id) {
            $menu[] = new FNode($id);
        }
        return $menu;
    }
}