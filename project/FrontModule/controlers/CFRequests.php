<?php


class CFRequests extends CFControler
{
	const REQUEST_PAGE = 203;

	public function prepareDefault()
	{
		parent::beforePrepare();
		if (!isset($this->get['request_id'])) {
			$this->redirect(CFSummaryControler::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
		}
		
		try {
			$request = $this->company->getFormSubmission($this->get['request_id']);
			$this->template->request = $request;
		} catch(Exception $e) {
			$this->flashMessage($e->getMessage());
			$this->redirect(CFSummaryControler::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
		}
	}
}
