<?php

use HttpModule\Responses\PdfResponse;
use OrderModule\Config\DiLocator as OrderDiLocator;
use OrderModule\Converters\OrderConverter;
use OrderModule\Pdf\IInvoiceGenerator;

class CustomerOrdersControler extends LoggedControler
{
    const ORDERS_PAGE = 97;
    const ORDER_PAGE = 173;

    /**
     * @var IInvoiceGenerator
     */
    private $invoiceGenerator;

    /**
     * @var OrderConverter
     */
    private $orderConverter;

    /**
     * @var array
     */
    public $possibleActions = [
        self::ORDERS_PAGE => 'default',
        self::ORDER_PAGE => 'order',
    ];

    public function startup()
    {
        parent::startup();
        //@TODO remove once paginator is added
        ini_set('memory_limit', '512M');

        $this->invoiceGenerator = $this->getService(OrderDiLocator::FPDF_INVOICE_GENERATOR);
        $this->orderConverter = $this->getService(OrderDiLocator::CONVERTERS_ORDER_CONVERTER);
    }

    /********************************** orders **********************************/

    protected function renderDefault()
    {
        $this->template->orders = Order::getOrders($this->customer->getId());
    }

    /********************************** order **********************************/

    protected function prepareOrder()
    {
        if (!isset($this->get['order_id'])) {
            $this->flashMessage("The order can not be displayed!");
            $this->redirect(self::ORDERS_PAGE);
        }

        $oldOrder = new Order($this->get['order_id']);
        if ($oldOrder->exists() == FALSE || $oldOrder->customerId != $this->customer->getId()) {
            $this->flashMessage("The order can not be displayed!");
            $this->redirect(self::ORDERS_PAGE);
        }
//		$order->recountOrder();

        // print pdf
        if (isset($this->get['print'])) {
            $order = $this->orderConverter->toEntity($oldOrder);
            $response = new PdfResponse($this->invoiceGenerator->getContents($order), 'Invoice.pdf');
            $response->send();
            exit(0);
        }

        $this->template->order = $oldOrder;

        $this->addBreadCrumbs([self::ORDERS_PAGE, $this->node->getLngTitle()]);
    }
}
