<?php

use Form\Both\CUDirector\EditDirectorPersonForm;
use Form\Both\CUDirector\EditDirectorCorporateForm;
use Form\Both\CUDirector\AddDirectorCorporateForm;
use Form\Both\CUDirector\AddDirectorPersonForm;

class CUDirectorsControler extends CUControler
{

    const ADD_DIRECTOR_PERSON_PAGE = 207;
    const ADD_DIRECTOR_CORPORATE_PAGE = 211;
    const EDIT_DIRECTOR_PERSON_PAGE = 212;
    const EDIT_DIRECTOR_CORPORATE_PAGE = 213;
    const SHOW_DIRECTOR_PERSON_PAGE = 220;
    const SHOW_DIRECTOR_CORPORATE_PAGE = 221;

    /** @var array */
    public $possibleActions = array(
        self::ADD_DIRECTOR_PERSON_PAGE => 'addDirectorPerson',
        self::ADD_DIRECTOR_CORPORATE_PAGE => 'addDirectorCorporate',
        self::EDIT_DIRECTOR_PERSON_PAGE => 'editDirectorPerson',
        self::EDIT_DIRECTOR_CORPORATE_PAGE => 'editDirectorCorporate',
        self::SHOW_DIRECTOR_PERSON_PAGE => 'showDirectorPerson',
        self::SHOW_DIRECTOR_CORPORATE_PAGE => 'showDirectorCorporate',
    );

    /**
     * @var Person 
     */
    private $director;

    public function beforePrepare()
    {
        $authCodeRequiredActions = array(
            $this->possibleActions[self::ADD_DIRECTOR_PERSON_PAGE],
            $this->possibleActions[self::EDIT_DIRECTOR_CORPORATE_PAGE],
            $this->possibleActions[self::EDIT_DIRECTOR_PERSON_PAGE],
        );

        if (in_array($this->action, $authCodeRequiredActions)) {
            $this->authCodeRequired = TRUE;
        }

        parent::beforePrepare();
    }

    /*     * ******************************************* add director person ************************************************** */

    protected function renderAddDirectorPerson()
    {
        $form = new AddDirectorPersonForm($this->node->getId() . '_addDirectorPerson');
        $form->startup($this, array($this, 'Form_addDirectorPersonFormSubmitted'), $this->company, $this->companyEntity, $this->prefiller);
        $this->template->form = $form;

        // prefill 
        $prefillAddress = $this->prefiller->getPrefillAdresses();
        $prefillOfficers = $this->prefiller->getPrefillOfficers();

        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
    }

    public function Form_addDirectorPersonFormSubmitted(AddDirectorPersonForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Your Director appointment has been sent to Companies House and will be processed in approximately 3 hours.');
            if ($this->company->getType() == 'LLP') {
                $this->flashMessage('Your Member appointment has been sent to Companies House and will be processed in approximately 3 hours.');
            }
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
        $this->redirect(CUSummaryControler::SUMMARY_PAGE, 'company_id=' . $this->company->getCompanyId());
    }

    /*     * ****************************************** show director person ************************************************** */

    protected function handleShowDirectorPerson()
    {
        // remove
        if (isset($this->get['director_id']) && isset($this->get['resign'])) {
            try {
                $this->company->sendTerminationOfDirector($this->get['director_id']);
                $this->flashMessage('Your Director resignation has been sent to Companies House and will be processed in approximately 3 hours.');
                if ($this->company->getType() == 'LLP') {
                    $this->flashMessage('Your Member resignation has been sent to Companies House and will be processed in approximately 3 hours.');
                }
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage());
            }
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId(), 'director_id' => NULL, 'resign' => NULL));
        }
    }

    protected function renderShowDirectorPerson()
    {
        // check get
        if (!isset($this->get['director_id'])) {
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId()));
        }

        // check existing director
        try {
            $person = $this->company->getPerson($this->get['director_id'], 'dir');
            $fields = $person->getFields();
            if ($this->company->getType() == 'LLP') {
                $fields['designated_ind'] = $person->getDesignatedInd();
            }

            $this->template->director = $fields;
            $this->template->company = $this->company;
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId(), 'director_id' => NULL));
        }
    }

    /*     * ****************************************** edit director person ************************************************** */

    protected function prepareEditDirectorPerson()
    {
        // check get
        if (!isset($this->get['director_id'])) {
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId()));
        }

        // check existing director
        try {
            $this->director = $this->company->getPerson($this->get['director_id'], 'dir');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId(), 'director_id' => NULL));
        }
    }

    protected function renderEditDirectorPerson()
    {
        $form = new EditDirectorPersonForm($this->node->getId() . '_editDirectorPerson');
        $form->startup($this, array($this, 'Form_editDirectorPersonFormSubmitted'), $this->company, $this->director, $this->prefiller);
        $this->template->form = $form;

        $prefillAddress = $this->prefiller->getPrefillAdresses();
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
    }

    public function Form_editDirectorPersonFormSubmitted(EditDirectorPersonForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Your Director change has been sent to Companies House and will be processed in approximately 3 hours.');
            if ($this->company->getType() == 'LLP') {
                $this->flashMessage('Your Member change has been sent to Companies House and will be processed in approximately 3 hours.');
            }
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $this->redirect(CUSummaryControler::SUMMARY_PAGE, 'company_id=' . $this->company->getCompanyId());
    }

    /*     * ****************************************** add director corporate ************************************************** */

    protected function renderAddDirectorCorporate()
    {
        $form = new AddDirectorCorporateForm($this->node->getId() . '_addDirectorCorporate');
        $form->startup($this, array($this, 'Form_addDirectorCorporateFormSubmitted'), $this->company, $this->companyEntity, $this->prefiller);
        $this->template->form = $form;

        // prefill
        $prefillAddress = $this->prefiller->getPrefillAdresses();
        $prefillOfficers = $this->prefiller->getPrefillOfficers(Prefiller::TYPE_CORPORATE);

        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
    }

    public function Form_addDirectorCorporateFormSubmitted(AddDirectorCorporateForm $form)
    {
        try {
            $form->process();
            $this->flashMessage("Corporate Director appointment has been submitted to Companies House and should be approved within 3 hours.");
            if ($this->company->getType() == 'LLP') {
                $this->flashMessage('Corporate Member appointment has been submitted to Companies House and should be approved within 3 hours.');
            }
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }

        $this->redirect(CUSummaryControler::SUMMARY_PAGE, 'company_id=' . $this->company->getCompanyId());
    }

    /*     * ****************************************** show director corporate ************************************************** */

    protected function handleShowDirectorCorporate()
    {
        // remove
        if (isset($this->get['director_id']) && isset($this->get['resign'])) {
            try {
                $this->company->sendTerminationOfDirector($this->get['director_id']);
                $this->flashMessage('Corporate Director resignation has been sent to Companies House and will be processed in approximately 3 hours.');
                if ($this->company->getType() == 'LLP') {
                    $this->flashMessage('Corporate Member resignation has been sent to Companies House and will be processed in approximately 3 hours.');
                }
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage());
            }
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId(), 'director_id' => NULL, 'resign' => NULL));
        }
    }

    protected function renderShowDirectorCorporate()
    {
        // check get
        if (!isset($this->get['director_id'])) {
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId()));
        }

        // check existing director
        try {
            $director = $this->company->getCorporate($this->get['director_id'], 'dir');
            $fields = $director->getFields();
            if ($this->company->getType() == 'LLP') {
                $fields['designated_ind'] = $director->getDesignatedInd();
            }

            $this->template->director = $fields;
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId(), 'director_id' => NULL));
        }
    }

    /*     * ****************************************** edit director corporate ************************************************** */

    protected function prepareEditDirectorCorporate()
    {        
        // check get
        if (!isset($this->get['director_id'])) {
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, 'company_id=' . $this->company->getCompanyId());
        }

        // check existing director
        try {
            $this->director = $this->company->getCorporate($this->get['director_id'], 'dir');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId(), 'director_id' => NULL));
        }
    }

    protected function renderEditDirectorCorporate()
    {
        $form = new EditDirectorCorporateForm($this->node->getId() . '_editDirectorCorporate');
        $form->startup($this, array($this, 'Form_editDirectorCorporateFormSubmitted'), $this->company, $this->director, $this->prefiller);
        $this->template->form = $form;

        // prefill 
        $prefillAddress = $this->prefiller->getPrefillAdresses();
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
    }

    public function Form_editDirectorCorporateFormSubmitted(EditDirectorCorporateForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Corporate Director change has been sent to Companies House and will be processed in approximately 3 hours.');
            if ($this->company->getType() == 'LLP') {
                $this->flashMessage('Corporate Member change has been sent to Companies House and will be processed in approximately 3 hours.');
            }
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $this->redirect(CUSummaryControler::SUMMARY_PAGE, 'company_id=' . $this->company->getCompanyId());
    }

}
