<?php

use Entities\Register\Member;
use Entities\Register\ShareClass;
use Front\Register\ShareClass\IShareClassFormDelegate;
use Front\Register\ShareClassEvent\ShareClassForm;
use Services\Register\MemberService;
use Services\Register\ShareClassService;

class CURegisterShareClassControler extends CURegisterControler_Abstract implements IShareClassFormDelegate
{
    const PAGE_ADD = 1444;
    const PAGE_EDIT = 1445;

    /**
     * @var array
     */
    public $possibleActions = array(
        self::PAGE_ADD => 'add',
        self::PAGE_EDIT => 'edit',
    );

    /**
     * @var ShareClassService
     */
    private $registerShareClassService;

    /**
     * @var ShareClass
     */
    private $shareClass;

    /**
     * @var MemberService
     */
    private $registerMemberService;

    /**
     * @var Member
     */
    private $member;

    public function startup()
    {
        parent::startup();
        $this->registerShareClassService = $this->getService(DiLocator::SERVICE_REGISTER_SHARE_CLASS);
        $this->registerMemberService = $this->getService(DiLocator::SERVICE_REGISTER_MEMBER);
    }

    public function beforeRender()
    {
        parent::beforeRender();

        $this->template->shareClass = $this->shareClass;
    }

    /********************************** add **********************************/

    public function prepareAdd()
    {
        try {
            $registerMemberId = $this->getParameter('registerMemberId');
            if (!$registerMemberId) {
                throw new InvalidArgumentException('Missing member id');
            }
            $this->member = $this->registerMemberService->getCompanyMemberById($this->companyEntity, $registerMemberId);
            $this->shareClass = new ShareClass($this->member);

        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect(CURegisterMemberControler::PAGE_LIST, array('company_id' => $this->companyEntity->getId()));
        }
    }

    public function renderAdd()
    {
        $form = new ShareClassForm('share_class_create');
        $form->startup($this, $this->shareClass, $this->registerShareClassService);
        $this->template->form = $form;

        $this->addBreadCrumbs(array(
            $this->getCompanyBreadCrumb(),
            $this->getListBreadCrumb(),
            $this->getViewBreadCrumb(),
            $this->node->getLngTitle(),
        ));
    }

    /********************************** edit **********************************/

    public function prepareEdit()
    {
        try {
            $registerShareClassId = $this->getParameter('registerShareClassId');
            if (!$registerShareClassId) {
                throw new InvalidArgumentException('Missing share class id');
            }
            $this->shareClass = $this->registerShareClassService->getCompanyShareClass($this->companyEntity, $registerShareClassId);

        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect(CURegisterMemberControler::PAGE_LIST, array('company_id' => $this->companyEntity->getId()));
        }
    }

    public function renderEdit()
    {
        $form = new ShareClassForm($this->shareClass->getId() . 'share_class_edit');
        $form->startup($this, $this->shareClass, $this->registerShareClassService);
        $this->template->form = $form;

        $this->addBreadCrumbs(array(
            $this->getCompanyBreadCrumb(),
            $this->getListBreadCrumb(),
            $this->getViewBreadCrumb(),
            $this->node->getLngTitle(),
        ));
    }

    /********************************** IShareClassFormDelegate **********************************/

    public function shareClassFormCreated(ShareClass $shareClass)
    {
        $this->flashMessage('Share class has been created');
        $this->redirect(CURegisterMemberControler::PAGE_VIEW, array(
            'company_id' => $this->companyEntity->getId(),
            'registerMemberId' => $shareClass->getMember()->getRegisterMemberId()
        ));
    }

    public function shareClassFormUpdated(ShareClass $shareClass)
    {
        $this->flashMessage('Share class has been updated');
        $this->redirect(CURegisterMemberControler::PAGE_VIEW, array(
            'company_id' => $this->companyEntity->getId(),
            'registerMemberId' => $shareClass->getMember()->getRegisterMemberId()
        ));
    }

    public function shareClassFormDeleted()
    {
        $this->flashMessage('Share class has been deleted');
        $this->redirect(CURegisterMemberControler::PAGE_VIEW, array(
            'company_id' => $this->companyEntity->getId(),
            'registerMemberId' => $this->shareClass->getMember()->getRegisterMemberId()
        ));
    }

    public function shareClassFormFailed(Exception $e)
    {
        $this->flashMessage($e->getMessage(), 'error');
        $this->redirect();
    }

    /**
     * @return array
     */
    private function getCompanyBreadCrumb()
    {
        return array('node_id' => CUSummaryControler::SUMMARY_PAGE, 'params' => array('company_id' => $this->companyEntity->getId()));
    }

    /**
     * @return array
     */
    private function getListBreadCrumb()
    {
        return array('node_id' => CURegisterMemberControler::PAGE_LIST, 'params' => array('company_id' => $this->companyEntity->getId()));
    }

    /**
     * @return array
     */
    private function getViewBreadCrumb()
    {
        return array('node_id' => CURegisterMemberControler::PAGE_VIEW, 'params' => array('company_id' => $this->companyEntity->getId(), 'registerMemberId' => $this->shareClass->getMember()->getId()));
    }
}
