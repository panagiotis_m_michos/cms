<?php

use Admin\CHForm;

class CFSecretariesControler extends CFControler
{
    const SECRETARIES_PAGE = 1131;
    const ADD_SECRETARY_PERSON_PAGE = 162;
    const EDIT_SECRETARY_PERSON_PAGE = 163;
    const ADD_SECRETARY_CORPORATE_PAGE = 183;
    const EDIT_SECRETARY_CORPORATE_PAGE = 184;
    
    /** @var array */
    public $possibleActions = array(
        self::SECRETARIES_PAGE => 'secretaries',
        self::ADD_SECRETARY_PERSON_PAGE => 'addSecretaryPerson',
        self::EDIT_SECRETARY_PERSON_PAGE => 'editSecretaryPerson',
        self::ADD_SECRETARY_CORPORATE_PAGE => 'addSecretaryCorporate',
        self::EDIT_SECRETARY_CORPORATE_PAGE => 'editSecretaryCorporate',
    );
    
    /** 
    * @var string 
    */
    static public $handleObject = 'CFSecretariesModel';

    /**
    * @var CFSecretariesModel
    */
    public $node;    
    
    /** @var object */
    private $secretary;
    
    public function beforePrepare()
    {
        parent::beforePrepare();
        // check permission
        if ($this->checkStepPermission('secretaries') == FALSE) {
            $this->redirect(CFShareholdersControler::SHAREHOLDERS_PAGE, "company_id={$this->company->getCompanyId()}");
        }
        $this->template->hide = 1;
    }
    
    
    /********************************************* list of secretaries ***************************************************/
    
    
    protected function handleSecretaries()
    {
        // delete secretary
        if (isset($this->get['delete_person_id']) || isset($this->get['delete_corporate_id'])) {
            try {
                if (isset($this->get['delete_person_id'])) {
                    $secretary = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncPerson($this->get['delete_person_id'], 'sec');
                } else {
                    $secretary = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncCorporate($this->get['delete_corporate_id'], 'sec');
                }
                $secretary->remove();
                $this->flashMessage('Secretary has been deleted');
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage());
            }
            $this->redirect(NULL, array('delete_person_id' => NULL, 'delete_corporate_id' => NULL));
        }
    }
    
    protected function prepareSecretaries()
    {
        // redirect to add - if it's PLC - 1 secretary required
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->secretariesCount() == 0 && $this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() == CompanyIncorporation::PUBLIC_LIMITED) {
            $this->redirect(self::ADD_SECRETARY_PERSON_PAGE, 'company_id='.$this->company->getCompanyId());
        }
        
        // nominee secretary
        if ($this->company->getNomineeSecretaryId() !== NULL) {
            $this->template->form = $this->getComponentNomineeSecretaryForm();
        }
        
        // add persons and corporates
        $this->template->persons = $this->getComponentPersons();
        $this->template->corporates = $this->getComponentCorporates();
        $this->template->nominees = $this->getComponentNominees();
        
        // continue form 
        $continue = new FForm('continueSecretaries');
        $continue->addSubmit('continueprocess', 'Continue >')->class('btn_submit fright mbottom');
        $continue->onValid = array($this, 'Form_continueShareholdersFormSubmitted');
        $continue->start();
        $this->template->continue = $continue;
    }
    
    
    public function Form_continueShareholdersFormSubmitted(FForm $form)
    {
        $type = $this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType();
        if ($type == CompanyIncorporation::PUBLIC_LIMITED) {
            try {
                $this->check('HAS_SECRETARY');
            } catch (Exception $e){
                $this->flashMessage($e->getMessage(), 'error');
                $this->redirect();
            }
        } 
        
        // check same officer
        //		if ($type == CompanyIncorporation::LIMITED_BY_SHARES) {
        //			try {
        //				$this->check('SAME_DIR_SEC');
        //			} catch (Exception $e){
        //				$this->flashMessage($e->getMessage(), 'error');
        //				$this->redirect();
        //			}
        //		}
        
        $this->redirect(CFArticlesControler::ARTICLES_PAGE, "company_id={$this->company->getCompanyId()}");
    }
    
    
    public function Form_SecretariesFormSubmitted(FForm $form)
    {
        // add nominee secretary
        if ($form->getValue('nominee') == 1) {
            try {
                self::saveNomineeSecretary($this->company, $this->company->getNomineeSecretaryId());
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage());
            }
            // remove nominee
        } else {
            $this->company->getLastIncorporationFormSubmission()->getForm()->removeNomineeSecretary();
            $this->flashMessage('Nominee secretary has been removed!');
        }
        $form->clean();
        $this->redirect();
    }
    
    
    
    /**********************************************************************************/
    /************************** SECRETARY PERSON ***************************************/
    /**********************************************************************************/
    
    
    /********************************************* add secretary person ***************************************************/
    
    
    protected function renderAddSecretaryPerson()
    {
        // prefill 
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'person');
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
        

        $form = $this->getComponentPersonForm('add', $prefillOfficers, $prefillAddress);
        $this->template->form = $form;
        $this->template->companyId = $this->company->getCompanyId();
        $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());
    }
    
    
    public function Form_addSecretaryPersonFormSubmitted($form)
    {
        $data = $form->getValues();
        
        // get secretary
        $secretary = $this->company->getLastIncorporationFormSubmission()->getForm()->getNewIncPerson('sec');
        
        // set person
        $person = $secretary->getPerson();
        $person->setTitle($data['title']);
        $person->setForename($data['forename']);
        $person->setMiddleName($data['middle_name']);
        $person->setSurname($data['surname']);
        //        $person->setNationality($data['nationality']);
        //        $person->setOccupation($data['occupation']);
        //        $person->setDob($data['dob']);
        //        $person->setCountryOfResidence($data['country_of_residence']);
        $secretary->setPerson($person);

         // set address
        if($this->company->getServiceAddress() && $data['ourServiceAddress']) {
            $msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());
            $address = $secretary->getAddress();
            $address->setFields((array) $msgServiceAdress);
            $secretary->setAddress($address);
        }else{
            $address = $secretary->getAddress();
            $address->setFields($data);
            $secretary->setAddress($address);
        }
        
        $secretary->setConsentToAct($data['consentToAct']);

        // save
        try {
            $secretary->save();
        } catch(Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect();
        }
        
        $form->clean();
        $this->flashMessage('Secretary has been added');
        $this->redirect(self::SECRETARIES_PAGE, 'company_id='.$this->company->getCompanyId());
    }
    
    /********************************************* edit secretary person ***************************************************/
    
    
    protected function prepareEditSecretaryPerson()
    {
        // check get
        if (!isset($this->get['secretary_id'])) {
            $this->redirect(self::SECRETARIES_PAGE, 'company_id='.$this->company->getCompanyId());
        }
        
        // check existing secretary
        try {
            $this->secretary = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncPerson($this->get['secretary_id'], 'sec');
            $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect(self::SECRETARIES_PAGE, array('company_id='.$this->company->getCompanyId(),'secretary_id'=>NULL));
        }
        
    }
    
    
    protected function renderEditSecretaryPerson()
    {
        // prefill
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'person');
        $this->template->jsPrefillOfficers = $prefillOfficers['js']; 
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        
        $form = $this->getComponentPersonForm('edit', $prefillOfficers, $prefillAddress);
        $this->template->form = $form;
        $this->template->companyId = $this->company->getCompanyId();
    }
    
    
    public function Form_editSecretaryPersonFormSubmitted($form)
    {
        $data = $form->getValues();
        
        $person = $this->secretary->getPerson();
        $person->setTitle($data['title']);
        $person->setForename($data['forename']);
        $person->setMiddleName($data['middle_name']);
        $person->setSurname($data['surname']);
        //		$person->setNationality($data['nationality']);
        //		$person->setOccupation($data['occupation']);
        //		$person->setDob($data['dob']);
        $this->secretary->setPerson($person);
        
         // set address
        if($this->company->getServiceAddress() && $data['ourServiceAddress']) {
            $msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());
            $address = $this->secretary->getAddress();
            $address->setFields((array) $msgServiceAdress);
            $this->secretary->setAddress($address);
        }else{
            $address = $this->secretary->getAddress();
            $address->setFields($data);
            $this->secretary->setAddress($address);
        }
        
        $this->secretary->setConsentToAct($data['consentToAct']);

        // save
        try {
            $this->secretary->save();
        } catch(Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect();
        }
        
        $form->clean();
        $this->flashMessage('Secretary has been updated');
        $this->redirect(self::SECRETARIES_PAGE, 'company_id='.$this->company->getCompanyId());
    }    
    
    
    
    /**********************************************************************************/
    /************************** SECRETARY CORPORATE ***********************************/
    /**********************************************************************************/
    
    
    
    /********************************************* add secretary corporate ***************************************************/
    
    
    protected function renderAddSecretaryCorporate()
    {
        // prefill 
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'corporate');
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
        
        $form = $this->getComponentCorporateForm('add', $prefillOfficers, $prefillAddress);
        $this->template->form = $form;
        $this->template->companyId = $this->company->getCompanyId();
    }
    
    
    public function Form_addSecretaryCorporateFormSubmitted(FForm $form)
    {
        $data = $form->getValues();
        
        // get corporate
        $secretaryCorporate = $this->company->getLastIncorporationFormSubmission()->getForm()->getNewIncCorporate('sec');
        $corporate = $secretaryCorporate->getCorporate();
        $corporate->setFields($data);
        $secretaryCorporate->setCorporate($corporate);
        
        // address
        $address = $secretaryCorporate->getAddress();
        $address->setFields($data);
        $secretaryCorporate->setAddress($address);
        
        $secretaryCorporate->setConsentToAct($data['consentToAct']);

        // identification
        if ($data['eeaType'] == 1) {
            $identification = $secretaryCorporate->getIdentification(Identification::EEA);
        } else {
            $identification = $secretaryCorporate->getIdentification(Identification::NonEEA);
        }

        $identification->setFields($data);
        $secretaryCorporate->setIdentification($identification);
        $secretaryCorporate->save();
        
        $form->clean();
        $this->flashMessage("Corporate secretary has been added");
        $this->redirect(self::SECRETARIES_PAGE, 'company_id='.$this->company->getCompanyId());
    }
    
    
    
    /********************************************* edit director corporate ***************************************************/
    
    
    protected function prepareEditSecretaryCorporate()
    {
        // check get
        if (!isset($this->get['secretary_id'])) {
            $this->redirect(self::SECRETARIES_PAGE, 'company_id='.$this->company->getCompanyId());
        }
        
        // check existing secretary
        try {
            $this->secretary = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncCorporate($this->get['secretary_id'], 'sec');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect(self::SECRETARIES_PAGE, array('company_id='.$this->company->getCompanyId(),'secretary_id'=>NULL));
        }
    }
    
    
    protected function renderEditSecretaryCorporate()
    {
        // prefill 
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'corporate');
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
        
        $form = $this->getComponentCorporateForm('edit', $prefillOfficers, $prefillAddress);
        $this->template->form = $form;
        $this->template->companyId = $this->company->getCompanyId();
    }
    
    
    public function Form_editSecretaryCorporateFormSubmitted($form)
    {
        $data = $form->getValues();
        
        $person = $this->secretary->getCorporate();
        $person->setFields($data);
        $this->secretary->setCorporate($person);
        
        // address
        $address = $this->secretary->getAddress();
        $address->setFields($data);
        $this->secretary->setAddress($address);
        
        $this->secretary->setConsentToAct($data['consentToAct']);

        // identification
        if ($data['eeaType'] == 1) {
            $identification = $this->secretary->getIdentification(Identification::EEA);
            $identification->setPlaceRegistered($data['place_registered']);
            $identification->setRegistrationNumber($data['registration_number']);
            $this->secretary->setIdentification($identification);
        } else {
            $identification = $this->secretary->getIdentification(Identification::NonEEA);
            $identification->setPlaceRegistered($data['place_registered']);
            $identification->setRegistrationNumber($data['registration_number']);
            $identification->setLawGoverned($data['law_governed']);
            $identification->setLegalForm($data['legal_form']);
        }
        $this->secretary->setIdentification($identification);
        
        // save
        try {
            $this->secretary->save();
        } catch(Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect();
        }
        
        $form->clean();
        $this->flashMessage('Secretary has been updated');
        $this->redirect(self::SECRETARIES_PAGE, 'company_id='.$this->company->getCompanyId());
    }

    
    /********************************************* components ***************************************************/
    
    /**
     * Provide saving nominee secretary to customer company
     *
     * @param Company $company
     * @param int $nomineeSecretaryId
     * @return void
     * @throws Exception
     */
    public static function saveNomineeSecretary($company, $nomineeSecretaryId)
    {
        $secretary = new NomineeSecretary($nomineeSecretaryId);
            
        // there is something wrong with nominee secretary
        if ($secretary->isOk2show() == FALSE) {
            throw new Exception('Nominee secretary can not be added!');
            
            // we can save nominee secretary
        } elseif ($company->getLastIncorporationFormSubmission()->getForm()->hasNomineeSecretary() == FALSE) {
            
            /************************ corporate ******************************/
            
            $secretaryCorporate = $company->getLastIncorporationFormSubmission()->getForm()->getNewIncCorporate('sec');
            $secretaryCorporate->setNominee(TRUE);
            
            $corporate = $secretaryCorporate->getCorporate();
            $corporate->setCorporateName($secretary->corporateCompanyName);
            $corporate->setForename($secretary->corporateFirstName);
            $corporate->setSurname($secretary->corporateLastName);
            $secretaryCorporate->setCorporate($corporate);
            
            // address
            $address = $secretaryCorporate->getAddress();
            $address->setPremise($secretary->corporateAddress1);
            $address->setStreet($secretary->corporateAddress2);
            $address->setThoroughfare($secretary->corporateAddress3);
            $address->setPostTown($secretary->corporateTown);
            $address->setCounty($secretary->corporateCounty);
            $address->setPostcode($secretary->corporatePostcode);
            $address->setCountry($secretary->corporateCountryId);
            $secretaryCorporate->setAddress($address);

            $secretaryCorporate->setConsentToAct(TRUE);

            // identification
            $identification = $secretaryCorporate->getIdentification(Identification::EEA);
            $identification->setPlaceRegistered($secretary->countryRegistered);
            $identification->setRegistrationNumber($secretary->registrationNumber);
            $secretaryCorporate->setIdentification($identification);
            
            $secretaryCorporate->save();
        }
    }
    
    
    /**
     * Return form for adding/editin secretary director
     *
     * @param string $action
     * @param array $prefillOfficers
     * @param array $prefillAddress
     * @return FForm
     */
    protected function getComponentPersonForm($action, $prefillOfficers, $prefillAddress)
    {
        // form
        if ($action == 'add') {
            $form = new FForm('addSecretaryPerson');
        } else {
            $form = new FForm('editSecretaryPerson');
        }

        // prefill
        $form->addFieldset('Prefill');
        $form->addSelect('prefillOfficers', 'Select an existing appointment to autocomplete these details', $prefillOfficers['select'])
            ->setFirstOption('--- Select --');

        // person
        $form->addFieldset('Person');
        $form->addSelect('title', 'Title', Person::$titles)
            ->setFirstOption('--- Select ---');
        $form->addText('forename', 'First name *')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule(FForm::Required, 'Please provide first name')
            ->addRule('MaxLength', "First name can't be more than 50 characters", 50);
        $form->addText('middle_name', 'Middle name')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "Middle name can't be more than 50 characters", 50)
            ->addRule('MinLength', "Please provide full middle name.", 2);
        $form->addText('surname', 'Last name *')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule(FForm::Required, 'Please provide last name')
            ->addRule('MaxLength', "Last name can't be more than 160 characters", 160);

        // if has registered office
        if ($this->company->getServiceAddress()) {
            // our registered office
            $form->addFieldset('Use Our Service Address service');

            $checkbox = $form->addCheckbox('ourServiceAddress', 'Service Address service  ', 1);

            //check if service address was set for director
            if (isset($this->secretary)) {
                $fields = $this->secretary->getFields();
                $msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());
                if ($fields['postcode'] == $msgServiceAdress->postcode) {
                    $checkbox->setValue(1);
                }
            }
        }

        // prefill
        $form->addFieldset('Prefill');
        $form->addSelect('prefillAddress', 'Prefill Address', $prefillAddress['select'])
            ->setFirstOption('--- Select ---');

        // adddress
        $form->addFieldset('Address');
        $form->addText('premise', 'Building name/number *')
            ->addRule(array($this, 'Validator_requiredAddress'), 'Please provide Building name/number')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "Building name/number can't be more than 50 characters", 50);
        $form->addText('street', 'Street *')
            ->addRule(array($this, 'Validator_requiredAddress'), 'Please provide Street')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "Street can't be more than 50 characters", 50);
        $form->addText('thoroughfare', 'Address 3')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "Address 3 can't be more than 50 characters", 50);
        $form->addText('post_town', 'Town *')
            ->addRule(array($this, 'Validator_requiredAddress'), 'Please provide Town')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "Town can't be more than 50 characters", 50);
        $form->addText('county', 'County')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "County can't be more than 50 characters", 50);
        $form->addText('postcode', 'Postcode *')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule(
                array($this, 'Validator_serviceAddressPostCode'), 'You cannot use our postcode for this address without first purchasing the '
                . Html::el('a')->setText('Service Address Service')->href(FApplication::$router->link(476))->render()
                . '. NB. This is different to the registered office service.'
            )
            ->addRule(array($this, 'Validator_requiredAddress'), 'Please provide Postcode')
            ->addRule('MaxLength', "Postcode can't be more than 15 characters", 15);
        $form->addSelect('country', 'Country *', Address::$countries)
            ->addRule(array($this, 'Validator_requiredAddress'), 'Please provide Country');

        $form->addFieldset('Consent to act');
        $form->add('SingleCheckbox', 'consentToAct', 'The subscribers (shareholders) confirm that the person named has consented to act as a secretary')
            ->addRule(FForm::Required, 'Consent to act is required');

        $form->addFieldset('Action');
        if ($action == 'add') {
            $form->addSubmit('continue', 'Continue >')->class('btn_submit fright mbottom');
            $form->onValid = array($this, 'Form_addSecretaryPersonFormSubmitted');
        } else {
            $form->addSubmit('continue', 'Save')->class('btn_submit fright mbottom');
            $form->onValid = array($this, 'Form_editSecretaryPersonFormSubmitted');

            $fields = $this->secretary->getFields();
            unset($fields['consentToAct']);
            $form->setInitValues($fields);
        }

        $form->start();
        return $form;
    }
    
    /**
     * Return form for adding/editing corporate secretary
     *
     * @param string $action
     * @param array $prefillOfficers
     * @param array $prefillAddress
     * @return FForm
     */
    public function getComponentCorporateForm($action, $prefillOfficers, $prefillAddress)
    {
        // form
        if ($action == 'add') {
            $form = new CHForm('addSecretaryCorporate');
        } else {
            $form = new CHForm('editSecretaryCorporate');
        }

        // prefill
        $form->addFieldset('Prefill');
        $form->addSelect('prefillOfficers', 'Select an existing appointment to autocomplete these details', $prefillOfficers['select'])
            ->setFirstOption('--- Select --');

        // person
        $form->addFieldset('Corporate');
        $form->addText('corporate_name', 'Company name *')
            ->addRule(FForm::Required, 'Please provide first name')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL);
        $form->addText('forename', 'First name *')
            ->addRule(FForm::Required, 'Please provide first name')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "First name can't be more than 50 characters", 50);
        $form->addText('surname', 'Last name *')
            ->addRule(FForm::Required, 'Please provide last name')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "Last name can't be more than 160 characters", 160);

        // address
        $form->addFieldset('Prefill');
        $form->addSelect('prefillAddress', 'Prefill Address', $prefillAddress['select'])
            ->setFirstOption('--- Select ---');

        // adddress
        $form->addFieldset('Address');
        $form->addText('premise', 'Building name/number *')
            ->addRule(FForm::Required, 'Please provide Building name/number')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "Building name/number can't be more than 50 characters", 50);
        $form->addText('street', 'Street *')
            ->addRule(FForm::Required, 'Please provide Street')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "Street can't be more than 50 characters", 50);
        $form->addText('thoroughfare', 'Address 3')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "Address 3 can't be more than 50 characters", 50);
        $form->addText('post_town', 'Town *')
            ->addRule(FForm::Required, 'Please provide Town')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "Town can't be more than 50 characters", 50);
        $form->addText('county', 'County')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "County can't be more than 50 characters", 50);
        $form->addText('postcode', 'Postcode *')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule(
                array($this, 'Validator_serviceAddressPostCode'), 'You cannot use our postcode for this address without first purchasing the '
                . Html::el('a')->setText('Service Address Service')->href(FApplication::$router->link(476))->render() .
                '. NB. This is different to the registered office service.'
            )
            ->addRule(FForm::Required, 'Please provide Postcode')
            ->addRule('MaxLength', "Postcode can't be more than 15 characters", 15);
        $form->addSelect('country', 'Country *', Address::$countries)
            ->addRule(FForm::Required, 'Please provide Country');


        $form->addFieldset('EEA/ Non EEA');
        $form->addRadio('eeaType', 'Type *', array(1=>'EEA', 2=>'Non EEA'))
            ->addRule(FForm::Required, 'Please provide EEA type!');
        $form->addText('place_registered', 'Country Registered *')
            ->addRule(array($this, 'Validator_eeaRequired'), 'Please provide Country registered!');
        $form->addText('registration_number', 'Registration number *')
            ->addRule(array($this, 'Validator_eeaRequired'), 'Please provide Registration number!');
        $form->addText('law_governed', 'Governing law *')
            ->addRule(array($this, 'Validator_eeaRequired'), 'Please provide Governing law!');
        $form->addText('legal_form', 'Legal Form *')
            ->addRule(array($this, 'Validator_eeaRequired'), 'Please provide Legal form!');

        $form->addFieldset('Consent to act');
        $form->add('SingleCheckbox', 'consentToAct', 'The subscribers (shareholders) confirm that the corporate body named has consented to act as a secretary')
            ->addRule(FForm::Required, 'Consent to act is required');

        $form->addFieldset('Action');

        if ($action == 'add') {
            $form->addSubmit('continue', 'Continue >')->class('btn_submit fright mbottom');
            $form->onValid = array($this, 'Form_addSecretaryCorporateFormSubmitted');
        } else {
            $form->addSubmit('continue', 'Save')->class('btn_submit fright mbottom');
            $form->onValid = array($this, 'Form_editSecretaryCorporateFormSubmitted');

            $fields = $this->secretary->getFields();
            unset($fields['consentToAct']);
            $form->setInitValues($fields);
        }

        $form->start();
        return $form;
    }
    
    /**
     * Returns list of persons
     * @return array
     */
    private function getComponentPersons()
    {
        $persons = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncPersons(array('sec'));
        
        // persons
        foreach ($persons as $key=>&$person) {
            if ($person->isNominee() == TRUE) {
                unset($persons[$key]);
                continue;
            }
            $person = array(
                'fullName' => $person->getPerson()->getForename() . ' ' .
                    $person->getPerson()->getMiddleName() . ' ' .
                    $person->getPerson()->getSurname(),
                'editLink' => $this->router->link(self::EDIT_SECRETARY_PERSON_PAGE, 'company_id='.$this->company->getCompanyId(), 'secretary_id='.$person->getId()),
                'deleteLink' => $this->router->link(self::SECRETARIES_PAGE, 'company_id='.$this->company->getCompanyId(), 'delete_person_id='.$person->getId())
            );
        }
        unset($person);
        return $persons;
    }
    
    /**
     * Returns list of persons
     * @return array
     */
    private function getComponentCorporates()
    {
        $corporates = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncCorporates(array('sec'));
        // corporates
        foreach ($corporates as $key=>&$corporate) {
            if ($corporate->isNominee() == TRUE) {
                unset($corporates[$key]);
                continue;
            }
            $corporate = array(
                'fullName' => $corporate->getCorporate()->getCorporateName(),
                'editLink' => $this->router->link(self::EDIT_SECRETARY_CORPORATE_PAGE, 'company_id='.$this->company->getCompanyId(), 'secretary_id='.$corporate->getId()),
                'deleteLink' => $this->router->link(self::SECRETARIES_PAGE, 'company_id='.$this->company->getCompanyId(), 'delete_corporate_id='.$corporate->getId())
            );
        }
        unset($corporate);
        return $corporates;
    }
    
    
    /**
     * Returns list of persons
     * @return array
     */
    private function getComponentNominees()
    {
        // get secretaries
        $secretaries = array();
        $persons = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncPersons(array('sec'));
        $corporates = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncCorporates(array('sec'));
        $secretaries = array_merge($persons, $corporates);
        
        // corporates
        foreach ($secretaries as $key=>&$secretary) {
            if ($secretary->isNominee() == FALSE) {
                unset($secretaries[$key]);
                continue;
            }
            
            if ($secretary instanceof IncorporationPersonSecretary) {
                $secretary = array('fullName' => $secretary->getPerson()->getForename() . ' ' . $secretary->getPerson()->getSurname());
            } else {
                $secretary = array('fullName' => $secretary->getCorporate()->getCorporateName());
            }
        }
        unset($secretary);
        return $secretaries;
    }
    
    
    /**
     * returns form for nominee secretary
     * @return unknown
     */
    private function getComponentNomineeSecretaryForm()
    {
        $form = new CHForm('nomineeSecretary');
        $form->addFieldset('Nominee Secretary');
        $form->addCheckbox('nominee', 'Use Nominee secretary ', 1);
        $form->addSubmit('update', 'Update')->class('btn_submit fright mbottom');
        $form->onValid = array($this, 'Form_SecretariesFormSubmitted');
        
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->hasNomineeSecretary()) {
            $form['nominee']->setValue(1);
        }
        
        $form->start();
        return $form;
    }
}
