<?php

use Front\CUChangeWebAuthCode\ICUChangeWebAuthCodeForm;

class CUChangeWebAuthCodeControler extends CUControler implements ICUChangeWebAuthCodeForm
{
    const PAGE_CHANGE_AUTH_CODE = 1089;

    public function renderDefault()
    {
        $form = new CUChangeWebAuthCodeForm($this->node->getId() . '_CUChangeWebAuthCode');
        $form->startup($this, $this->companyEntity);
        $this->template->form = $form;

        if ($this->isPopup()) {
            $this->changeView('popup');
        }
    }

    /* ******************************** ICUChangeWebAuthCodeForm ******************************** */

    /**
     * @param string $authCode
     */
    public function authCodeFormSuccess($authCode)
    {
        try {
            $this->companyEntity->setAuthenticationCode($authCode);
            $this->companyService->saveCompany($this->companyEntity);
            $this->flashMessage('Thank you, your web filling auth code has been changed!');

            $backlink = $this->getBacklink();
            if ($backlink !== FALSE) {
                $this->removeBacklink();

                // remove authCodePopup parameter and check if original request has popup parameter set
                parse_str(parse_url($backlink, PHP_URL_QUERY), $backlinkParameters);
                unset($backlinkParameters['authCodePopup']);

                if (isset($backlinkParameters['popup'])) {
                    $this->redirect("%$backlink%");
                } else {
                    $this->reloadOpener($backlink);
                }
            } else {
                $this->reloadOpener();
            }
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }
}
