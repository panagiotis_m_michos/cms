<?php

use Form\Both\CUSecretary\AddSecretaryCorporateForm;
use Form\Both\CUSecretary\EditSecretaryCorporateForm;
use Form\Both\CUSecretary\AddSecretaryPersonForm;
use Form\Both\CUSecretary\EditSecretaryPersonForm;

class CUSecretariesControler extends CUControler
{

    const ADD_SECRETARY_PERSON_PAGE = 223;
    const ADD_SECRETARY_CORPORATE_PAGE = 224;
    const EDIT_SECRETARY_PERSON_PAGE = 227;
    const EDIT_SECRETARY_CORPORATE_PAGE = 228;
    const SHOW_SECRETARY_PERSON_PAGE = 225;
    const SHOW_SECRETARY_CORPORATE_PAGE = 226;

    /**
     * @var array
     */
    public $possibleActions = array(
        self::ADD_SECRETARY_PERSON_PAGE => 'addSecretaryPerson',
        self::ADD_SECRETARY_CORPORATE_PAGE => 'addSecretaryCorporate',
        self::EDIT_SECRETARY_PERSON_PAGE => 'editSecretaryPerson',
        self::EDIT_SECRETARY_CORPORATE_PAGE => 'editSecretaryCorporate',
        self::SHOW_SECRETARY_PERSON_PAGE => 'showSecretaryPerson',
        self::SHOW_SECRETARY_CORPORATE_PAGE => 'showSecretaryCorporate',
    );

    /**
     * @var Person
     */
    private $secretary;

    public function beforePrepare()
    {
        $authCodeRequiredActions = array(
            $this->possibleActions[self::ADD_SECRETARY_PERSON_PAGE],
            $this->possibleActions[self::ADD_SECRETARY_CORPORATE_PAGE],
            $this->possibleActions[self::EDIT_SECRETARY_PERSON_PAGE],
            $this->possibleActions[self::EDIT_SECRETARY_CORPORATE_PAGE],
        );

        if (in_array($this->action, $authCodeRequiredActions)) {
            $this->authCodeRequired = TRUE;
        }

        parent::beforePrepare();
    }

    protected function renderAddSecretaryPerson()
    {
        $form = new AddSecretaryPersonForm($this->node->getId() . '_addSecretaryPerson');
        $form->startup($this, array($this, 'Form_addSecretaryPersonFormSubmitted'), $this->company, $this->companyEntity, $this->prefiller);
        $this->template->form = $form;

        $prefillAddress = $this->prefiller->getPrefillAdresses();
        $prefillOfficers = $this->prefiller->getPrefillOfficers();
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
    }

    public function Form_addSecretaryPersonFormSubmitted(AddSecretaryPersonForm $form)
    {
        try {
            $form->process();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect();
        }
        $this->flashMessage('Your Secretary appointment has been sent to Companies House and will be processed in approximately 3 hours.');
        $this->redirect(CUSummaryControler::SUMMARY_PAGE, 'company_id=' . $this->company->getCompanyId());
    }

    protected function handleShowSecretaryPerson()
    {
        // remove
        if (isset($this->get['secretary_id']) && isset($this->get['resign'])) {
            try {
                $this->company->sendTerminationOfSecretary($this->get['secretary_id']);
                $this->flashMessage('Your Secretary resignation has been sent to Companies House and will be processed in approximately 3 hours.');
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage());
            }
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId(), 'secretary_id' => NULL, 'resign' => NULL));
        }
    }

    protected function renderShowSecretaryPerson()
    {
        // check get
        if (!isset($this->get['secretary_id'])) {
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId()));
        }

        // check existing secretary
        try {
            $secretary = $this->company->getPerson($this->get['secretary_id'], 'sec');
            $this->template->secretary = $secretary->getFields();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId(), 'secretary_id' => NULL));
        }
    }

    protected function prepareEditSecretaryPerson()
    {        
        // check get
        if (!isset($this->get['secretary_id'])) {
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId()));
        }

        // check existing secretary
        try {
            $this->secretary = $this->company->getPerson($this->get['secretary_id'], 'sec');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId(), 'secretary_id' => NULL));
        }
    }

    protected function renderEditSecretaryPerson()
    {
        $form = new EditSecretaryPersonForm($this->node->getId() . '_editSecretaryPerson');
        $form->startup($this, array($this, 'Form_editSecretaryPersonFormSubmitted'), $this->company, $this->secretary, $this->prefiller);
        $this->template->form = $form;

        // prefill 
        $prefillAddress = $this->prefiller->getPrefillAdresses();
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
    }

    public function Form_editSecretaryPersonFormSubmitted(EditSecretaryPersonForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Your Secretary change has been sent to Companies House and will be processed in approximately 3 hours.');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $this->redirect(CUSummaryControler::SUMMARY_PAGE, 'company_id=' . $this->company->getCompanyId());
    }

    protected function renderAddSecretaryCorporate()
    {
        $form = new AddSecretaryCorporateForm($this->node->getId() . '_addSecretaryCorporate');
        $form->startup($this, array($this, 'Form_addSecretaryCorporateFormSubmitted'), $this->company, $this->companyEntity, $this->prefiller);
        $this->template->form = $form;

        // prefill 
        $prefillAddress = $this->prefiller->getPrefillAdresses();
        $prefillOfficers = $this->prefiller->getPrefillOfficers(Prefiller::TYPE_CORPORATE);

        $this->template->jsPrefillAdresses = $prefillAddress['js'];
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
    }

    public function Form_addSecretaryCorporateFormSubmitted(AddSecretaryCorporateForm $form)
    {
        try {
            $form->process();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect();
        }
        $this->flashMessage("Corporate Secretary appointment has been submitted to Companies House and should be approved within 3 hours.");
        $this->redirect(CUSummaryControler::SUMMARY_PAGE, 'company_id=' . $this->company->getCompanyId());
    }

    protected function handleShowSecretaryCorporate()
    {
        // remove
        if (isset($this->get['secretary_id']) && isset($this->get['resign'])) {
            try {
                $this->company->sendTerminationOfSecretary($this->get['secretary_id']);
                $this->flashMessage('Your Secretary resignation has been sent to Companies House and will be processed in approximately 3 hours.');
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage());
            }
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId(), 'secretary_id' => NULL, 'resign' => NULL));
        }
    }

    protected function renderShowSecretaryCorporate()
    {
        // check get
        if (!isset($this->get['secretary_id'])) {
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId()));
        }

        // check existing secretary
        try {
            $secretary = $this->company->getCorporate($this->get['secretary_id'], 'sec');
            $this->template->secretary = $secretary->getFields();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId(), 'secretary_id' => NULL));
        }
    }

    protected function prepareEditSecretaryCorporate()
    {        
        // check get
        if (!isset($this->get['secretary_id'])) {
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, 'company_id=' . $this->company->getCompanyId());
        }

        // check existing secretary
        try {
            $this->secretary = $this->company->getCorporate($this->get['secretary_id'], 'sec');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, array('company_id=' . $this->company->getCompanyId(), 'secretary_id' => NULL));
        }
    }

    protected function renderEditSecretaryCorporate()
    {
        $form = new EditSecretaryCorporateForm($this->node->getId() . '_editSecretaryCorporate');
        $form->startup($this, array($this, 'Form_editSecretaryCorporateFormSubmitted'), $this->company, $this->secretary, $this->prefiller);
        $this->template->form = $form;

        // prefill 
        $prefillAddress = $this->prefiller->getPrefillAdresses();
        $this->template->jsPrefillAdresses = $prefillAddress['js'];
    }

    public function Form_editSecretaryCorporateFormSubmitted(EditSecretaryCorporateForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Your Secretary change has been sent to Companies House and will be processed in approximately 3 hours.');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }

        $this->redirect(CUSummaryControler::SUMMARY_PAGE, 'company_id=' . $this->company->getCompanyId());
    }

}
