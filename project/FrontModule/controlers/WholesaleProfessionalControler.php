<?php

use Wholesale\IEmailer;

class WholesaleProfessionalControler extends WholesaleControler_Abstract
{
    const TAG = 'GENERALWHOLESALE';

    /* pages */
    const PAGE_HOME_ = 1110;
    const PAGE_SIGN_UP = 1114;
    const PAGE_PRODUCTS = 1118;
    const PAGE_CONTACT = 1115;
    const PAGE_MY_ACCOUNT = DashboardCustomerControler::MYACCOUNT_PAGE;

    /**
     * @var array
     */
    public $possibleActions = array(
        self::PAGE_HOME_ => 'home',
        self::PAGE_SIGN_UP => 'signUp',
        self::PAGE_PRODUCTS => 'products',
        self::PAGE_CONTACT => 'contact',
        self::PAGE_MY_ACCOUNT => 'myDetails',
    );

    /**
     * @var array
     */
    protected $templateExtendedFrom = array('WholesaleAbstract');

    /**
     * @var bool
     */
    protected $httpsRequired = TRUE;

    /**
     * @return array
     */
    protected function getMenuItems()
    {
        return array_keys($this->possibleActions);
    }

    /**
     * @return string
     */
    protected function getTag()
    {
        return self::TAG;
    }

    /**
     * @return IEmailer
     */
    protected function getEmailer()
    {
        return $this->getService(DiLocator::EMAILER_WHOLESALE_PROFESSIONAL);
    }
}
