<?php

use Form\Both\CURegisteredOffice\EditRegisteredOfficeForm;

class CURegisteredOfficeControler extends CUControler
{

    const REGISTER_OFFICE_PAGE = 206;

    /** @var array */
    public $possibleActions = array(
        self::REGISTER_OFFICE_PAGE => 'office',
    );

    public function beforePrepare()
    {
        if ($this->action == $this->possibleActions[self::REGISTER_OFFICE_PAGE]) {
            $this->authCodeRequired = TRUE;
        }

        parent::beforePrepare();
    }

    protected function prepareOffice()
    {
        $form = new EditRegisteredOfficeForm($this->company->getId() . '_editAuthenticationCode');
        $form->startup($this, array($this, 'Form_defaultFormSubmitted'), $this->company, $this->prefiller);
        $this->template->form = $form;

        // prefill address
        $prefillAddress = $this->prefiller->getPrefillAdresses();
        $this->template->jsAdresses = $prefillAddress['js'];
    }

    /**
     * @param FForm $form
     */
    public function Form_defaultFormSubmitted(FForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Request has been sent to Companies House.');
            $form->clean();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        $this->redirect(CUSummaryControler::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
    }

}
