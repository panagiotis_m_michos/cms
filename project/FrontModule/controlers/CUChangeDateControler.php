<?php

class CUChangeDateControler extends CUControler
{
    const CHANGE_DATE_PAGE = 229;

    /** @var array */
    public $possibleActions = array(
        self::CHANGE_DATE_PAGE => 'default',
    );

    public function beforePrepare()
    {
        parent::beforePrepare();     
        try {
            foreach ($this->company->getFormSubmissions() as $formSubmission) {
                if ($formSubmission->isChangeAccountingReferenceDateType()) {
                    if ($formSubmission->isPending() || $formSubmission->isInternalFailure()) {
                        throw new Exception('Your Accounting Reference Date Change is currently pending. We will email you as soon as it\'s done!');
                    }
                }
            }
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
        }
    }

    public function renderDefault()
    {
        $form = new ChangeAccountingReferenceDateForm('changeDate');
        $form->startup($this, array($this, 'Form_ChangeAccountingReferenceDateFormSubmitted'), $this->company);
        $this->template->form = $form;
        $data = $this->company->getData();
        $this->template->lastAccountMadeUp = $data['company_details']['accounts_last_made_up_date'];    
        $this->template->ardCurrent = $this->company->getAccountingReferenceDate();
        $this->template->ardPrevios = date('d-m-Y', strtotime('-1 year', strtotime($this->company->getAccountingReferenceDate())));
    }

    /**
     * @param ChangeAccountingReferenceDateForm $form
     */
    public function Form_ChangeAccountingReferenceDateFormSubmitted(ChangeAccountingReferenceDateForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('You ARD change request has been sent to Companies House. Approval should be given in approx. 3 hours.');
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }
}
