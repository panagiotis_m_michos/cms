<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version   BarclaysSoletraderControler.php 2010-11-12 divak@gmail.com
 */

class BarclaysSoletraderControler  extends BaseControler
{

	const BARCLAYS_PAGE_SOLETRADER = 873;
    const BARCLAYS_PACKAGE = 820;

	public $possibleActions = array(
        self::BARCLAYS_PAGE_SOLETRADER => 'default'
	);

	/**
	 * @var string
	 */
	static public $handleObject = 'BarclaysSoletraderModel';

	/**
	 * @var BarclaysSoletraderModel
	 */
	public $node;

        /**************************************barclayssoletrader************************************/
    public function renderDefault()
        {
            $this->template->form = $this->node->getComponentBarclaysSoletraderForm(array($this, 'Form_barclaysSoletraderFormSubmitted'));
        }



    public function Form_barclaysSoletraderFormSubmitted(FForm $form)
    {
    	try {
    		$this->node->processBarclaysSoletraderForm($form);
    		$this->flashMessage('Your application has been submitted to Barclays. They will be in touch shortly.');
    		$this->redirect(self::BARCLAYS_PACKAGE);
    	} catch (Exception $e) {
    	    $this->flashMessage($e->getMessage(), 'error');
    	 	$this->redirect();
    	}
    }
   /**************************************barclayssoletrader************************************/
}
