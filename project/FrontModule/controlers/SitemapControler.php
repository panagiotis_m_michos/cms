<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    SitemapControler.php 2011-05-13 divak@gmail.com
 */


class SitemapControler extends DefaultControler
{
    public function renderDefault()
    {
        $cache = FApplication::getCache();
		if (isset($cache['sitemapHtml'])) {
			$sitemapHtml = $cache['sitemapHtml'];
		} else {
            $folders = array();
            $sitemap = FSitemap::get(1);
            $folders[100] = $sitemap['100'];
            $folders[266] = $sitemap['266'];
            $sitemapHtml = FSitemap::getHtml($folders);
			$cache->save('sitemapHtml', $sitemapHtml, array('expire' => time() + 60 * 60 * 24)); // one day
		}
        // --- generated menu ---
        $this->template->sitemap = $sitemapHtml;
    }
}
