<?php

class VirtualOfficeOldControler extends CFControler
{
    /* pages */
    const VIRTUAL_OFFICE_PAGE = 1276;
    const ONE_CLICK_PAYMENT_PAGE = 1277;
    const MEETING_ROOM_PAGE = 1273;
    const MAIL_FORWARDING_PAGE = 1274;
    const TELEPHONE_ANSWERING_PAGE = 1275;

    /** @var array */
    public $possibleActions = array(
        self::VIRTUAL_OFFICE_PAGE => 'virtualoffice',
        self::ONE_CLICK_PAYMENT_PAGE => 'oneclickpayment',
        self::MEETING_ROOM_PAGE => 'meetingroom',
        self::MAIL_FORWARDING_PAGE => 'mailforwarding',
        self::TELEPHONE_ANSWERING_PAGE => 'telephoneanswering'
    );

    /* products */
    const MAIL_FORWARDING_PRODUCT = 1270;
    const MAIL_FORWARDING_TELEPHONE_ANSWERING_PRODUCT = 1271;
    const MAIL_FORWARDING_PRODUCT_3_MONTHS = 443;

    /**
     * @var array
     */
    public static $products = array(
        self::MAIL_FORWARDING_PRODUCT => '<b>Mail Forwarding:</b> £10/mth for 3 months <span style = "color:grey;">(usually £19.99/mth save 50%)</span>',
        self::MAIL_FORWARDING_TELEPHONE_ANSWERING_PRODUCT => '<b>Mail Forwarding & Telephone Answering:</b> £20/mth for 3 months',
    );

    /**
     * @var string
     */
    static public $handleObject = 'TokensModel';

    /**
     * @var TokensModel
     */
    public $node;


    /****************************************** virtualoffice ******************************************/


    public function renderVirtualoffice()
    {
        $token = $this->node->getCustomerToken($this->customer);

        if ($this->hasMailForwardingAlreadyIncluded()
            || $this->customer->isWholesale()
            || !$token
            || $this->hasCompanyMailForwardingAlreadyIncluded()
            || $this->hasInternationalPackage()
        ) {
            $this->redirect(DashboardCustomerControler::DASHBOARD_PAGE);
        }

        $form = new ProductsForm($this->node->getId() . '_virtual_office');
        $form->startup(array($this, 'Form_productTypeFormSubmitted'), self::$products);
        $this->template->form = $form;

        $this->template->token = $token;
    }

    /**
     * @param ProductsForm $form
     */
    public function Form_productTypeFormSubmitted(ProductsForm $form)
    {
        if (FApplication::$httpRequest->isAjax()) {
            try {
                $product = $form['productType']->getValue();
                $payload = array(
                    'popup' => 1,
                    'link' => $this->router->link(
                        self::ONE_CLICK_PAYMENT_PAGE,
                        array("product" => $product, "company_id" => $this->company->getCompanyId())
                    ));
                    $this->sendJSONResponse($payload);

            } catch (Exception $e) {
                $this->sendJSONResponse(
                    array(
                        'error' => TRUE,
                        'message' => $e->getMessage()
                    )
                );
            }
        }
        exit;
    }


    /****************************************** oneclickpayment ******************************************/

    public function renderOneclickpayment()
    {
        // check if pasing product and token exist
        if (!isset($this->get['product']) || empty($this->get['product'])
            || $this->node->getCustomerToken($this->customer) == NULL
        ) {
            $this->redirect(self::VIRTUAL_OFFICE_PAGE);
        }

        Debug::disableProfiler();
        $backlink = $this->router->link(
            self::VIRTUAL_OFFICE_PAGE,
            array("company_id" => $this->company->getCompanyId())
        );
        $this->saveBacklink($backlink);

        $this->template->token = $this->node->getCustomerToken($this->customer);
        $this->template->product = Product::getProductById($this->get['product']);
    }

    /****************************************** meetingroom ******************************************/

    public function renderMeetingroom()
    {
        Debug::disableProfiler();
    }


    /****************************************** mailforwarding ******************************************/

    public function renderMailforwarding()
    {
        Debug::disableProfiler();
    }


    /****************************************** telephoneanswering ******************************************/

    public function renderTelephoneanswering()
    {
        Debug::disableProfiler();
    }

    /****************************************** private ******************************************/


    /**
     * @return bool
     */
    private function hasMailForwardingAlreadyIncluded()
    {
        $items = array(
            Package::PACKAGE_DIAMOND,
            Package::PACKAGE_ULTIMATE,
            self::MAIL_FORWARDING_PRODUCT_3_MONTHS
        );
        foreach ($items as $item) {
            if ($this->service->checkOrderHasItem($item)) {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * @return bool
     */
    private function hasCompanyMailForwardingAlreadyIncluded()
    {
        $items = array(self::MAIL_FORWARDING_PRODUCT, self::MAIL_FORWARDING_TELEPHONE_ANSWERING_PRODUCT);
        foreach ($items as $item) {
            if ($this->service->checkOrderItemsHasProduct($item)) {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * @return bool
     */
    private function hasInternationalPackage()
    {
        return $this->service->checkOrderHasItem(Package::PACKAGE_INTERNATIONAL);
    }

}
