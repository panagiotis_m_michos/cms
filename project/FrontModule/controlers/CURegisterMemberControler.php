<?php

use Entities\Register\Member;
use Front\Register\MemberForm;
use Front\Register\ShareClassEvent\DeleteForm;
use Front\Register\ShareClassEvent\IDeleteFormDelegate;
use Services\Register\MemberService;
use Front\Register\MembersDatagrid;
use Front\Register\IMemberFormDelegate;
use Services\Register\ShareClassService;

class CURegisterMemberControler extends CURegisterControler_Abstract implements IMemberFormDelegate, IDeleteFormDelegate
{
    const PAGE_LIST = 1440;
    const PAGE_VIEW = 1441;
    const PAGE_ADD = 1442;
    const PAGE_EDIT = 1443;

    /**
     * @var array
     */
    public $possibleActions = array(
        self::PAGE_LIST => 'list',
        self::PAGE_VIEW => 'view',
        self::PAGE_ADD => 'add',
        self::PAGE_EDIT => 'edit',
    );

    /**
     * @var MemberService
     */
    private $registerMemberService;

    /**
     * @var Member
     */
    private $member;

    /**
     * @var ShareClassService
     */
    private $registerShareClassService;


    public function startup()
    {
        parent::startup();
        $this->registerMemberService = $this->getService(DiLocator::SERVICE_REGISTER_MEMBER);
        $this->registerShareClassService = $this->getService(DiLocator::SERVICE_REGISTER_SHARE_CLASS);
    }


    /********************************** list **********************************/

    public function renderList()
    {
        $datasource = $this->registerMemberService->getListDataSource($this->companyEntity);

        $grid = new MembersDataGrid($datasource, 'registerMemberId');
        $grid->setCompany($this->companyEntity);
        $grid->setRouter($this->router);
        $this->template->datagrid = $grid;

        $this->addBreadCrumbs(array(
            $this->getCompanyBreadCrumb(),
            $this->node->getLngTitle(),
        ));

    }

    /********************************** view **********************************/


    public function renderView()
    {
        try {
            $id = $this->getParameter('registerMemberId');
            $member = $this->getMember($id);

            $deleteShareClassForms = array();
            foreach ($member->getShareClasses() as $shareClass) {
                $form = new DeleteForm($shareClass->getId() . 'share_class_event_delete');
                $form->startup($this, $shareClass, $this->registerShareClassService);
                $deleteShareClassForms[$shareClass->getId()] = $form;
            }

            $this->addBreadCrumbs(array(
                $this->getCompanyBreadCrumb(),
                $this->getListBreadCrumb(),
                $this->node->getLngTitle(),
            ));

            $this->template->member = $member;
            $this->template->deleteShareClassForms = $deleteShareClassForms;

        } catch (Exception $e) {
            $this->processException($e);
        }
    }


    /********************************** add **********************************/


    public function renderAdd()
    {
        $member = new Member($this->companyEntity);

        $form = new MemberForm('member_create');
        $form->startup($this, $member, $this->registerMemberService);
        $this->template->form = $form;

        $this->addBreadCrumbs(array(
            $this->getCompanyBreadCrumb(),
            $this->getListBreadCrumb(),
            $this->node->getLngTitle(),
        ));
    }


    /********************************** edit **********************************/


    public function renderEdit()
    {
        try {
            $id = $this->getParameter('registerMemberId');
            $this->member = $this->getMember($id);

            $form = new MemberForm($this->member->getId() . '_member_edit');
            $form->startup($this, $this->member, $this->registerMemberService);
            $this->template->form = $form;

            $this->addBreadCrumbs(array(
                $this->getCompanyBreadCrumb(),
                $this->getListBreadCrumb(),
                $this->getViewBreadCrumb(),
                $this->node->getLngTitle(),
            ));

        } catch (Exception $e) {
            $this->processException($e);
        }
    }


    /********************************** IMemberFormDelegate **********************************/


    public function memberFormCreated(Member $member)
    {
        $this->flashMessage('Member has been created');
        $this->redirect(CURegisterMemberControler::PAGE_VIEW, array('company_id' => $this->companyEntity->getId(), 'registerMemberId' => $member->getId()));
    }

    public function memberFormUpdated(Member $member)
    {
        $this->flashMessage('Member has been updated');
        $this->redirect(CURegisterMemberControler::PAGE_VIEW, array('company_id' => $this->companyEntity->getId(), 'registerMemberId' => $member->getId()));
    }

    public function memberFormDeleted()
    {
        $this->flashMessage('Member has been deleted');
        $this->redirect(CURegisterMemberControler::PAGE_LIST, array('company_id' => $this->companyEntity->getId()));
    }

    public function memberFormFailed(Exception $e)
    {
        $this->flashMessage($e->getMessage(), 'error');
        $this->redirect();
    }


    /********************************** IDeleteFormDelegate **********************************/

    public function deleteShareClassEventFormDeleted()
    {
        $this->flashMessage('Share class event has been deleted');
        $this->redirect();
    }

    public function deleteShareClassEventFormFailed(Exception $e)
    {
        $this->flashMessage($e->getMessage(), 'error');
        $this->redirect();
    }

    /********************************** Private methods **********************************/


    private function getMember($id)
    {
        $member = $this->registerMemberService->getCompanyMemberById($this->companyEntity, $id);
        if (!$member) {
            throw new EntityNotFound("Member with id `$id` not found");
        }
        return $member;
    }

    /**
     * @param Exception $e
     */
    private function processException(Exception $e)
    {
        if ($e instanceof InvalidArgumentException || $e instanceof EntityNotFound) {
            $message = $e->getMessage();
        } else {
            Debug::log($e);
            $message = 'An error occurred';
        }
        $this->flashMessage($message, 'error');
        $this->redirect(self::PAGE_LIST, "company_id={$this->companyEntity->id}");
    }

    /**
     * @return array
     */
    private function getCompanyBreadCrumb()
    {
        return array('node_id' => CUSummaryControler::SUMMARY_PAGE, 'params' => array('company_id' => $this->companyEntity->getId()));
    }

    /**
     * @return array
     */
    private function getListBreadCrumb()
    {
        return array('node_id' => self::PAGE_LIST, 'params' => array('company_id' => $this->companyEntity->getId()));
    }

    /**
     * @return array
     */
    private function getViewBreadCrumb()
    {
        return array('node_id' => self::PAGE_VIEW, 'params' => array('company_id' => $this->companyEntity->getId(), 'registerMemberId' => $this->member->getId()));
    }

}
