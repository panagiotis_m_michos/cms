<?php

use CsmsModule\ICsms;
use FeefoModule\IFeefo;

class PPCLandingControler extends DefaultControler
{
    public function renderDefault()
    {
        /** @var ICsms $csms */
        $csms = $this->getService('csms_module.csms');

        /** @var IFeefo $feefo */
        $feefo = $this->getService('feefo_module.feefo');
        $summary = $feefo->getSummary();

        $this->template->incorporatedThisYear = $csms->getIncorporationStats()->getIncorporationCountThisYearToNow();
        $this->template->feefoAverage = $summary->getAverageRating();
        $this->template->feefoReviewCount = $feefo->getSummary()->getReviewCount();

        $this->template->searchForm = $this->getComponent('search');
    }
}
