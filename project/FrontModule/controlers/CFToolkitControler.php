<?php

use ToolkitOfferModule\Forms\OffersData;
use ToolkitOfferModule\Forms\OffersForm;
use ToolkitOfferModule\Services\ToolkitOfferService;

class CFToolkitControler extends CFControler
{
    const PAGE_TOOLKIT = 1630;

    /**
     * @var ToolkitOfferService
     */
    private $toolkitOfferService;

    /** @var array */
    public $possibleActions = [
        self::PAGE_TOOLKIT => 'toolkit'
    ];

    public function beforePrepare()
    {
        parent::beforePrepare();

        if ($this->checkStepPermission('toolkit') == FALSE) {
            $this->redirect(CFArticlesControler::ARTICLES_PAGE, ['company_id' => $this->company->getCompanyId()]);
        }

        $this->toolkitOfferService = $this->getService('toolkit_offer_module.services.toolkit_offer_service');
    }

    public function renderToolkit()
    {
        $this->template->hide = 1;
        $this->template->companyType = $this->getCompanyType();

        $allOffers = $this->toolkitOfferService->getAvailableOffers();
        $companyOffers = $this->companyEntity->getSelectedToolkitOffers();

        if ($this->companyEntity->toolkitOffersAlreadyChosen()) {
            $offersData = OffersData::fromCompanyToolkitOffers($companyOffers);
        } else {
            $offersData = new OffersData($allOffers);
        }

        $form = $this->createForm(new OffersForm(), $offersData);

        if ($form->isValid()) {
            /** @var OffersData $data */
            $data = $form->getData();
            $this->toolkitOfferService->assignOffersToCompany($this->companyEntity, $data->getOffers());
            $this->redirect(CFSummaryControler::SUMMARY_PAGE, ['company_id' => $this->companyEntity->getId()]);
        }

        // get all choices (entities) from form to make them available in template to custom rendering
        $options = $form->get('offers')->getConfig()->getOptions();
        $choices = $options['choice_list']->getChoices();

        $this->template->form = $form->createView();
        $this->template->choices = $choices;
    }
}
