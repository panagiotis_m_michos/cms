<?php

class TokensControler extends LoggedControler
{
    const TOKEN = 1234;
    const TOKEN_SAGEPAY_3DRESPONCE = 1236;
    const SAGE_AUTH = 1235;
    const TOKEN_DETAILS = 1237;
//    const TOKEN_PRODUCT = 1238;
    const TOKEN_CONFIRMATION = 1260;

    public $possibleActions = array(
        self::TOKEN => 'default',
        self::SAGE_AUTH => 'sageAuthentication',
        self::TOKEN_SAGEPAY_3DRESPONCE => 'sagepay3dResponce',
        self::TOKEN_DETAILS => 'tokenDetails',
//        self::TOKEN_PRODUCT => 'product',
        self::TOKEN_CONFIRMATION => 'confirmation',
    );

    /**
     * @var string
     */
    static public $handleObject = 'TokensModel';

    /**
     * @var TokensModel
     */
    public $node;

//    public function renderProduct()
//    {
//        $this->template->products = array(1240 => Product::getProductById(1240));
//    }

    public function handleDefault()
    {
        // check token
        if ($this->node->getCustomerToken($this->customer) == NULL) {
            $this->flashMessage('Card not exist');
            $this->redirect(TokensControler::TOKEN_DETAILS);
        }

        // check product id
        if (!isset($this->get['add'])) {
            $this->flashMessage('You don\'t have any products in your basket, please choose product and try again.');
            self::reloadOpener($this->getBacklink());
        }

        if (isset($this->get['add'])) {
            try {
                $itemId = $this->get['add'];
                $item = FNode::getProductById($itemId);
                if (CFRegisteredOfficeControler::REGISTER_OFFICE_PRODUCT == $itemId ||
                    VirtualOfficeControler::MAIL_FORWARDING_PRODUCT == $itemId ||
                    VirtualOfficeControler::MAIL_FORWARDING_TELEPHONE_ANSWERING_PRODUCT == $itemId ||
                    VirtualOfficeControler::MAIL_FORWARDING_2015 == $itemId) {
                    $item->isAssociated = TRUE;
                }

                if (isset($this->get['company'])) {
                    $company = Company::getCompany($this->get['company']);
                    $item->setCompanyId($company->getCompanyId());
                    $item->companyName = $company->getCompanyName();
                    $item->companyNumber = $company->getCompanyNumber();
                    $item->requiredCompanyNumber = TRUE;
                }

                $this->basket->clear($hardClean = FALSE);
                $this->basket->add($item);

                if (isset($this->get['topayment'])) {
                    $this->redirect(PaymentControler::SAGE_PAGE);
                }
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
                $this->readFlashMessages();
            }
        }
    }

    public function renderDefault()
    {
        Debug::disableProfiler();
        try {
            //'sending data to sage'
            $response = $this->node->useToken($this->customer, $this->basket);

            $this->node->getTokenAnswer($this->customer, $this->basket, $response, $this->customerEntity, $this->getService(DiLocator::SERVICE_ORDER), $this->getService(DiLocator::SERVICE_PRODUCT));
            $this->redirect(self::TOKEN_CONFIRMATION, $this->node->getConfirmationPageLinkParams($this->basket, $this->customer));
        } catch (SageAuthenticationException $e) {
            //pr('3dform on');
            $this->template->authFormUrl = FApplication::$router->secureLink(self::SAGE_AUTH, array('sagepay-3d' => 1));
            $this->template->showAuth = true;
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->readFlashMessages();
            //$this->redirect(BasketControler::BASKET_PAGE, array('add'=>$this->get['add']));
        }
    }

    /**
     * sage authorization form in iframe (loaded when 3dauth is necessary)
     */
    public function renderSageAuthentication()
    {
        Debug::disableProfiler();
        if (isset($_SESSION['sageAuthForm'])) {
            $this->template->form = $_SESSION['sageAuthForm'];
        }
    }

    public function renderSagepay3dResponce()
    {
        Debug::disableProfiler();
        try {
            $response = $this->node->useToken($this->customer, $this->basket);

            $this->node->getTokenAnswer($this->customer, $this->basket, $response, $this->customerEntity, $this->getService(DiLocator::SERVICE_ORDER), $this->getService(DiLocator::SERVICE_PRODUCT));

            $this->template->success = true;
            $this->template->location = FApplication::$router->secureLink(self::TOKEN_CONFIRMATION, $this->node->getConfirmationPageLinkParams($this->basket, $this->customer));
        } catch (Exception $e) {
            $this->template->failedLocation = FApplication::$router->link(self::TOKEN);
            $this->template->error = $e->getMessage();
        }
    }

    public function renderConfirmation()
    {
        Debug::disableProfiler();
        if ($this->basket->isEmpty() === TRUE) {
            $this->redirect(HomeControler::HOME_PAGE);
            self::reloadOpener();
        }

        $this->template->basket = clone $this->basket;
        $this->template->orderid = $this->basket->getOrderId();
        $backlink = $this->getBacklink();
        if ($backlink !== FALSE) {
            $this->template->continueLink = $backlink;
        }
        $this->basket->clear();
        $this->template->ecommerce = 1;
    }

    public function renderTokenDetails()
    {
        $token = $this->node->getCustomerToken($this->customer);

        if (!$token instanceof Tokens) {
            // show new form
//            $form = new TokenDetailsForm($this->nodeId . '_' . $this->action);
//            $form->startup(new Tokens(), $this->node, $this->customer, $this, array($this, 'Form_tokenDetailsFormSubmitted'));
//            $this->template->form = $form;
        } else {
            ////update token
//            if (isset($this->get['update'])) {
//                $form = new TokenDetailsForm($this->nodeId . '_' . $this->action);
//                $form->startup($token, $this->node, $this->customer, $this,  array($this, 'Form_tokenDetailsFormSubmitted'));
//                $this->template->form = $form;
//            }
            //delete token
            if (isset($this->get['delete'])) {
                try {
                    $this->node->removeToken($token);
                } catch (Exception $e) {
                    $this->flashMessage($e->getMessage());
                    $this->redirect('tokenDetails');
                }
                $this->flashMessage('Token record has been deleted from DB and SagePay');
                $this->redirect('tokenDetails');
            }
        }
        $this->template->token = $token;
    }

    /**
     * @param TokenDetailsForm $form
     */
    public function Form_tokenDetailsFormSubmitted(TokenDetailsForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Token record has been created');
            $this->redirect('tokenDetails');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            if ($this->node->getCustomerToken($this->customer) != NULL) {
                $this->redirect('tokenDetails', array('update' => 1));
            }
            $this->redirect('tokenDetails');
        }
    }

}
