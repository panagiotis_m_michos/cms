<?php

use Admin\CHForm;

class CFShareholdersControler extends CFControler
{
    const SHAREHOLDERS_PAGE = 1130;
    const ADD_SHAREHOLDER_PERSON_PAGE = 159;
    const EDIT_SHAREHOLDER_PERSON_PAGE = 160;
    const ADD_SHAREHOLDER_CORPORATE_PAGE = 181;
    const EDIT_SHAREHOLDER_CORPORATE_PAGE = 182;

    /** @var array */
    public $possibleActions = array(
        self::SHAREHOLDERS_PAGE => 'shareholders',
        self::ADD_SHAREHOLDER_PERSON_PAGE => 'addShareholderPerson',
        self::EDIT_SHAREHOLDER_PERSON_PAGE => 'editShareholderPerson',
        self::ADD_SHAREHOLDER_CORPORATE_PAGE => 'addShareholderCorporate',
        self::EDIT_SHAREHOLDER_CORPORATE_PAGE => 'editShareholderCorporate',
    );

    /**
     * @var string
     */
    static public $handleObject = 'CFShareholdersModel';

    /**
    * @var CFShareholdersModel
    */
    public $node;

    /** @var object */
    private $shareholder;

    public function beforePrepare()
    {
        parent::beforePrepare();
        // check permission
        if ($this->checkStepPermission('shareholders') == FALSE) {
            $this->redirect(CFDirectorsControler::DIRECTORS_PAGE, "company_id={$this->company->getCompanyId()}");
        }
        $this->template->hide = 1;
    }

    protected function handleShareholders()
    {
        // delete shareholder
        if (isset($this->get['delete_person_id']) || isset($this->get['delete_corporate_id'])) {
            try {
                if (isset($this->get['delete_person_id'])) {
                    $shareholder = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncPerson($this->get['delete_person_id'], 'sub');
                } else {
                    $shareholder = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncCorporate($this->get['delete_corporate_id'], 'sub');
                }
                $shareholder->remove();
                $this->flashMessage('Shareholder has been deleted');
            } catch (Exception $e) {
                $this->flashMessage($e->getMessage());
            }
            $this->redirect(NULL, array('delete_person_id' => NULL, 'delete_corporate_id' => NULL));
        }
    }



    protected function prepareShareholders()
    {
        // redirect to add on empty
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->shareholdersCount() == 0 && $this->company->getNomineeSubscriberId() == NULL) {
            $this->redirect(self::ADD_SHAREHOLDER_PERSON_PAGE, 'company_id='.$this->company->getCompanyId());
        }

        // nominee shareholder
        if ($this->company->getNomineeSubscriberId() !== NULL) {
            $this->template->form = $this->getComponentNomineeShareholderForm();
        }

        // add persons and corporates
        $this->template->persons = $this->getComponentPersons();
        $this->template->corporates = $this->getComponentCorporates();
        $this->template->nominees = $this->getComponentNominees();

        // continue form 
        $continue = new FForm('continueShareholders');
        $continue->addSubmit('continueprocess', 'Continue >')->class('btn_submit fright mbottom');
        $continue->onValid = array($this, 'Form_continueShareholdersFormSubmitted');
        $continue->start();
        $this->template->continue = $continue;
    }

    public function renderShareholders()
    {
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() == 'BYGUAR') {
            $this->template->title = 'Members';
        }
    }

    public function Form_continueShareholdersFormSubmitted(FForm $form)
    {
        $type = $this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType();

        try {
            // check if for PLC
            if ($type == CompanyIncorporation::PUBLIC_LIMITED) {
                $this->check('MINIMUM_2_SHAREHOLDERS');
            } elseif ($this->company->getLastIncorporationFormSubmission()->getForm()->shareholdersCount() == 0) {
                $this->flashMessage('You need to appoint at least one shareholder', 'error');
                $this->redirect();
            }
        } catch (Exception $e){
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }

        if (Feature::featurePsc()) {
            $this->redirect(CFPscsControler::PSCS_PAGE, ['company_id' => $this->company->getCompanyId()]);
        } else {
            $this->redirect(CFSecretariesControler::SECRETARIES_PAGE, ['company_id' => $this->company->getCompanyId()]);
        }
    }


    public function Form_shareholdersFormSubmitted(FForm $form)
    {
        // add nominee shareholder
        if ($form->getValue('nominee') == 1) {
            self::saveNomineeShareholder($this->company, $this->company->getNomineeSubscriberId());
            $this->flashMessage('Nominee shareholder has been added');
            // remove nominee
        } else {
            $this->company->getLastIncorporationFormSubmission()->getForm()->removeNomineeSubscriber();
            $this->flashMessage('Nominee shareholder has been removed!');
        }

        $form->clean();
        $this->redirect();
    }


    /**********************************************************************************/
    /************************** SHAREHOLDER PERSON ************************************/
    /**********************************************************************************/


    /********************************************* add shareholder person ***************************************************/


    protected function renderAddShareholderPerson()
    {
        // prefill 
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'person');
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
        $this->template->jsPrefillAdresses = $prefillAddress['js'];

        $this->template->form = $this->getComponentPersonForm('add', $prefillOfficers, $prefillAddress);
        $this->template->companyType = $this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType();
        $this->template->companyId = $this->company->getCompanyId();
        $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() == 'BYGUAR') {
            $this->template->title = 'Add Member';
        }
    }


    public function Form_addShareholderPersonFormSubmitted($form)
    {
        try {
            $data = $form->getValues();
            $shareholder = $this->company->getLastIncorporationFormSubmission()->getForm()->getNewIncPerson('sub');

            // person
            $person = $shareholder->getPerson();
            if (!empty($data['title'])) {    $person->setTitle($data['title']);
            }
            $person->setForename($data['forename']);
            $person->setMiddleName($data['middle_name']);
            $person->setSurname($data['surname']);
            $shareholder->setPerson($person);

            // shares
            if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() != 'BYGUAR') {
                $shares = $shareholder->getAllotmentShares();
                $shares->setShareClass('ORD');
                $shares->setNumShares($data['num_shares']);
                $shares->setCurrency($data['currency']);
                $shares->setShareValue($data['share_value']);
                $shareholder->setAllotmentShares($shares);
            }

            // address
            if($this->company->getServiceAddress() && $data['ourServiceAddress']) {
                $msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());
                $address = $shareholder->getAddress();
                $address->setFields((array) $msgServiceAdress);
                $shareholder->setAddress($address);
            }else{
                $address = $shareholder->getAddress();
                $address->setFields($data);
                $shareholder->setAddress($address);
            }

            // authentication
            $authetication = $shareholder->getAuthentication();
            $authetication->setFields($data);
            $shareholder->setAuthentication($authetication);

            $shareholder->save();
            $form->clean();
            $this->flashMessage('Shareholder has been added');
            $this->redirect(self::SHAREHOLDERS_PAGE, 'company_id='.$this->company->getCompanyId());

        } catch(Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }


    /********************************************* edit shareholder person ***************************************************/


    protected function prepareEditShareholderPerson()
    {
        // check get
        if (!isset($this->get['shareholder_id'])) {
            $this->redirect(self::SHAREHOLDERS_PAGE, 'company_id='.$this->company->getCompanyId());
        }

        // check existing director
        try {
            $this->shareholder = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncPerson($this->get['shareholder_id'], 'sub');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect(self::SHAREHOLDERS_PAGE, array('company_id='.$this->company->getCompanyId(),'shareholder_id'=>NULL));
        }
        $this->template->companyId = $this->company->getCompanyId();
    }

    protected function renderEditShareholderPerson()
    {
        // prefill
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'person');
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
        $this->template->jsPrefillAdresses = $prefillAddress['js'];

        $form = $this->getComponentPersonForm('edit', $prefillOfficers, $prefillAddress);
        $this->template->form = $form;
        $this->template->msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());

        $this->template->companyType = $this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType();
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() == 'BYGUAR') {
            $this->template->title = 'Amend Member';
        }
    }


    public function Form_editShareholderPersonFormSubmitted(FForm $form)
    {
        $data = $form->getValues();

        $person = $this->shareholder->getPerson();
        if (!empty($data['title'])) { $person->setTitle($data['title']);
        }
        $person->setForename($data['forename']);
        $person->setMiddleName($data['middle_name']);
        $person->setSurname($data['surname']);
        $this->shareholder->setPerson($person);

        // address
        if($this->company->getServiceAddress() && $data['ourServiceAddress']) {
            $msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());
            $address = $this->shareholder->getAddress();
            $address->setFields((array) $msgServiceAdress);
            $this->shareholder->setAddress($address);
        }else{
            $address = $this->shareholder->getAddress();
            $address->setFields($data);
            $this->shareholder->setAddress($address);
        }

        // get company capital
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() != 'BYGUAR') {

            $shares = $this->shareholder->getAllotmentShares();
            $shares->setShareClass($data['share_class']);
            $shares->setNumShares($data['num_shares']);
            $shares->setCurrency($data['currency']);
            $shares->setShareValue($data['share_value']);
            $this->shareholder->setAllotmentShares($shares);
        }

        // authentication
        $authetication = $this->shareholder->getAuthentication();
        $authetication->setFields($data);
        $this->shareholder->setAuthentication($authetication);

        // save
        try {
            $this->shareholder->save();
        } catch(Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect();
        }

        $form->clean();
        $this->flashMessage('Shareholder has been updated');
        $this->redirect(self::SHAREHOLDERS_PAGE, 'company_id='.$this->company->getCompanyId());
    }


    /**********************************************************************************/
    /************************** SHAREHOLDER CORPORATE *********************************/
    /**********************************************************************************/


    /********************************************* add shareholder corporate ***************************************************/


    protected function renderAddShareholderCorporate()
    {
        // prefill
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'corporate');
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
        $this->template->jsPrefillAdresses = $prefillAddress['js'];

        $form = self::getComponentCorporateForm('insert', $prefillOfficers, $prefillAddress);
        $this->template->form = $form;
        $this->template->companyType = $this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType();
        $this->template->companyId = $this->company->getCompanyId();
    }


    public function Form_addShareholderCorporateFormSubmitted(FForm $form)
    {
        $data = $form->getValues();

        // get corporate
        $shareholderCorporate = $this->company->getLastIncorporationFormSubmission()->getForm()->getNewIncCorporate('sub');
        $corporate = $shareholderCorporate->getCorporate();
        $corporate->setFields($data);
        $shareholderCorporate->setCorporate($corporate);

        // address
        $address = $shareholderCorporate->getAddress();
        $address->setFields($data);
        $shareholderCorporate->setAddress($address);

        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() != 'BYGUAR') {
            $shares = $shareholderCorporate->getAllotmentShares();
            $shares->setShareClass('ORD');
            $shares->setNumShares($data['num_shares']);
            $shares->setCurrency($data['currency']);
            $shares->setShareValue($data['share_value']);
            $shareholderCorporate->setAllotmentShares($shares);
        }

        // authentication
        $authetication = $shareholderCorporate->getAuthentication();
        $authetication->setFields($data);
        $shareholderCorporate->setAuthentication($authetication);

        $shareholderCorporate->save();

        $form->clean();
        $this->flashMessage("Corporate shareholder has been added");
        $this->redirect(self::SHAREHOLDERS_PAGE, 'company_id='.$this->company->getCompanyId());
    }


    /********************************************* edit shareholder corporate ***************************************************/


    protected function prepareEditShareholderCorporate()
    {
        // check get
        if (!isset($this->get['shareholder_id'])) {
            $this->redirect(self::SHAREHOLDERS_PAGE, 'company_id='.$this->company->getCompanyId());
        }

        // check existing shareholder
        try {
            $this->shareholder = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncCorporate($this->get['shareholder_id'], 'sub');
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect(self::SHAREHOLDERS_PAGE, array('company_id='.$this->company->getCompanyId(),'shareholder_id'=>NULL));
        }
    }


    protected function renderEditShareholderCorporate()
    {
        // prefill
        $prefillOfficers = self::getPrefillOfficers($this->company, $type = 'corporate');
        $this->template->jsPrefillOfficers = $prefillOfficers['js'];
        $prefillAddress = self::getPrefillAdresses($this->company);
        $this->template->jsPrefillAdresses = $prefillAddress['js'];

        $form = self::getComponentCorporateForm('edit', $prefillOfficers, $prefillAddress);
        $this->template->form = $form;
        $this->template->companyType = $this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType();
        $this->template->companyId = $this->company->getCompanyId();
    }


    public function Form_editShareholderCorporateFormSubmitted($form)
    {
        $data = $form->getValues();

        $person = $this->shareholder->getCorporate();
        $person->setFields($data);
        $this->shareholder->setCorporate($person);

        // address
        $address = $this->shareholder->getAddress();
        $address->setFields($data);
        $this->shareholder->setAddress($address);

        // authentication
        $authetication = $this->shareholder->getAuthentication();
        $authetication->setFields($data);
        $this->shareholder->setAuthentication($authetication);

        // 
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() != 'BYGUAR') {
            $shares = $this->shareholder->getAllotmentShares();
            $shares->setShareClass($data['share_class']);
            $shares->setNumShares($data['num_shares']);
            $shares->setCurrency($data['currency']);
            $shares->setShareValue($data['share_value']);
            $this->shareholder->setAllotmentShares($shares);
        }
        // save
        try {
            $this->shareholder->save();
        } catch(Exception $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect();
        }

        $form->clean();
        $this->flashMessage('Shareholder has been updated');
        $this->redirect(self::SHAREHOLDERS_PAGE, 'company_id='.$this->company->getCompanyId());
    }


    /********************************************* components ***************************************************/

    /**
     * Provide saving nominee shareholder to customer company
     *
     * @param Company $company
     * @param int $nomineeShareholderId
     * @return void
     * @throws Exception
     */
    public static function saveNomineeShareholder($company, $nomineeShareholderId)
    {
        $shareholder = new NomineeShareholder($nomineeShareholderId);

        // there is something wrong with nominee shareholder
        if ($shareholder->isOk2show() == FALSE) {
            throw new Exception('Nominee shareholder can not be added!');

            // we can save nominee shareholder
        } elseif ($company->getLastIncorporationFormSubmission()->getForm()->hasNomineeSubscriber() == FALSE) {

            /************************ corporate ******************************/

            $shareholderCorporate = $company->getLastIncorporationFormSubmission()->getForm()->getNewIncCorporate('sub');
            $shareholderCorporate->setNominee(TRUE);

            $corporate = $shareholderCorporate->getCorporate();
            $corporate->setCorporateName($shareholder->corporateCompanyName);
            $corporate->setForename($shareholder->corporateFirstName);
            $corporate->setSurname($shareholder->corporateLastName);
            $shareholderCorporate->setCorporate($corporate);

            // address
            $address = $shareholderCorporate->getAddress();
            $address->setPremise($shareholder->corporateAddress1);
            $address->setStreet($shareholder->corporateAddress2);
            $address->setThoroughfare($shareholder->corporateAddress3);
            $address->setPostTown($shareholder->corporateTown);
            $address->setCounty($shareholder->corporateCounty);
            $address->setPostcode($shareholder->corporatePostcode);
            $address->setCountry($shareholder->corporateCountryId);
            $shareholderCorporate->setAddress($address);

            // shares
            $shares = $shareholderCorporate->getAllotmentShares();
            $shares->setShareClass('ORD');
            $shares->setNumShares(1);
            $shares->setCurrency('GBP');
            $shares->setShareValue(1);
            $shareholderCorporate->setAllotmentShares($shares);

            // authentication
            $authetication = $shareholderCorporate->getAuthentication();
            $data = array(
                'BIRTOWN' => $shareholder->corporateBirtown,
                'TEL' => $shareholder->corporateAuthTel,
                'EYE' => $shareholder->corporateAuthEye,
            );
            $authetication->setFields($data);
            $shareholderCorporate->setAuthentication($authetication);

            $shareholderCorporate->save();
        }
    }

    /**
     * Return form for adding/editing corporate director
     *
     * @param string $action
     * @param array $prefillOfficers
     * @param array $prefillAddress
     * @return FForm
     */
    public function getComponentPersonForm($action, $prefillOfficers, $prefillAddress)
    {

        if ($action == 'add') {
            $form = new CHForm('addShareholderPerson');
        } else {
            $form = new CHForm('editShareholderPerson');
        }

        $form->addFieldset('Prefill');
        $form->addSelect('prefillOfficers', 'Select an existing appointment to autocomplete these details', $prefillOfficers['select'])
            ->setFirstOption('--- Select --');

        // data
        $form->addFieldset('Data');
        $form->addSelect('title', 'Title', Person::$titles)
            ->setFirstOption('--- Select ---');
        $form->addText('forename', 'First name *')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule(FForm::Required, 'Please provide first name')
            ->addRule('MaxLength', "First name can't be more than 50 characters", 50);
        $form->addText('middle_name', 'Middle name')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MinLength', "Please provide full middle name.", 2)
            ->addRule('MaxLength', "First name can't be more than 50 characters", 50);
        $form->addText('surname', 'Last name *')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule(FForm::Required, 'Please provide last name')
            ->addRule('MaxLength', "Last name can't be more than 160 characters", 160);

        // if has registered office
        if ($this->company->getServiceAddress()) {
            // our registered office
            $form->addFieldset('Use Our Service Address service');

            $checkbox = $form->addCheckbox('ourServiceAddress', 'Service Address service  ', 1);

            //check if service address was set for director
            if (isset($this->shareholder)) {
                $fields = $this->shareholder->getFields();
                $msgServiceAdress = new ServiceAddress($this->company->getServiceAddress());
                if ($fields['postcode'] == $msgServiceAdress->postcode) {
                    $checkbox->setValue(1);
                }
            }
        }

        $form->addFieldset('Prefill');
        $form->addSelect('prefillAddress', 'Prefill Address', $prefillAddress['select'])
            ->setFirstOption('--- Select ---');

        // adddress
        $form->addFieldset('Address');
        $form->addText('premise', 'Building name/number *')
            ->addRule(array($this, 'Validator_requiredAddress'), 'Please provide Building name/number')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "Building name/number can't be more than 50 characters", 50);
        $form->addText('street', 'Street *')
            ->addRule(array($this, 'Validator_requiredAddress'), 'Please provide Street')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "Street can't be more than 50 characters", 50);
        $form->addText('thoroughfare', 'Address 3')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "Address 3 can't be more than 50 characters", 50);
        $form->addText('post_town', 'Town *')
            ->addRule(array($this, 'Validator_requiredAddress'), 'Please provide Town')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "Town can't be more than 50 characters", 50);
        $form->addText('county', 'County')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "County can't be more than 50 characters", 50);
        $form->addText('postcode', 'Postcode *')
            ->addRule(array($this, 'Validator_requiredAddress'), 'Please provide Postcode')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "Postcode can't be more than 15 characters", 15)
            ->addRule(
                array($this, 'Validator_serviceAddressPostCode'), 'You cannot use our postcode for this address without first purchasing the '
                . Html::el('a')->setText('Service Address Service')->href(FApplication::$router->link(476))->render()
                . '. NB. This is different to the registered office service.'
            );
        $form->addSelect('country', 'Country *', Address::$countries)
            ->addRule(array($this, 'Validator_requiredAddress'), 'Please provide Country');

        // allotment shares
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() != 'BYGUAR') {
            $form->addFieldset('Allotment of Shares');
            $form->addSelect('currency', 'Share Currency', IncorporationCapital::$currency);
            $form->addText('share_class', 'Share Class')
                ->setValue('ORD')
                ->readonly();
            $form->addText('num_shares', 'Number of shares')
                ->addRule(FForm::Required, 'Please provide number of shares')
                ->addRule(FForm::NUMERIC, 'Number of shares has to be numeric')
                ->addRule(FForm::GREATER_THAN, 'Number of shares has to be greater than #1', 0)
                ->addRule(FForm::REGEXP, 'Number of shares has to be lower than 11 digits', '#^\d{1,11}$#')
                ->setValue(1);
            $form->addText('share_value', 'Value per Share')
                ->addRule(FForm::Required, 'Please provide value per shares')
                ->addRule(FForm::NUMERIC, 'Value per share has to be numeric')
                ->addRule(FForm::GREATER_THAN, 'Value per share has to be greater than #1', 0)
                ->addRule(FForm::REGEXP, 'Amount per share can be int or decimal with maximum 6 fraction digits', '#^\d+(.{1}(\d{1,6})){0,1}$#')
                ->setValue(1);
        }

        // security
        $form->addFieldset('Security');
        $form->addText('BIRTOWN', 'First three letters of Town of birth *')
            ->size(3)
            ->addRule(FForm::Required, 'Please provide Town of birth')
            ->addRule(FForm::REGEXP, 'Please enter only 3 letters for Town of birth', '#^[A-Z]{3,3}$#i');
        $form->addText('TEL', 'Last three digits of Telephone number *')
            ->size(3)
            ->addRule(FForm::Required, 'Please provide Telephone number')
            ->addRule(FForm::REGEXP, 'Please enter only 3 numeric characters for Telephone number', '#^\d{3,3}$#');
        $form->addText('EYE', 'First three letters of Eye colour *')
            ->size(3)
            ->addRule(FForm::Required, 'Please provide Eye colour')
            ->addRule(FForm::REGEXP, 'Please enter only 3 letters for Eye colour', '#^[A-Z]{3,3}$#i')
            ->addRule(array($this, 'Validator_securityEyeColour'), 'Eye colour cannot be black. Please enter a valid eye colour (eg. brown, blue, green etc.)');

        $form->addFieldset('Action');

        if ($action == 'add') {
            $form->addSubmit('continue', 'Continue >')->class('btn_submit fright mbottom');
            $form->onValid = array($this, 'Form_addShareholderPersonFormSubmitted');
        } else {
            $form->addSubmit('continue', 'Save')->class('btn_submit fright mbottom');
            $form->onValid = array($this, 'Form_editShareholderPersonFormSubmitted');
            $form->setInitValues($this->shareholder->getFields());
        }

        $form->start();
        return $form;
    }

    /**
     * Return form for adding/editing corporate shareholder
     *
     * @param string $action
     * @param array $prefillOfficers
     * @param array $prefillAddress
     * @return FForm
     */
    public function getComponentCorporateForm($action, $prefillOfficers, $prefillAddress)
    {
        // form
        if ($action == 'add') {
            $form = new FForm('addShareholderCorporate');
        } else {
            $form = new FForm('editShareholderCorporate');
        }

        $form->addFieldset('Prefill');
        $form->addSelect('prefillOfficers', 'Select an existing appointment to autocomplete these details', $prefillOfficers['select'])
            ->setFirstOption('--- Select --');

        // person
        $form->addFieldset('Corporate');
        $form->addText('corporate_name', 'Company name *')
            ->addRule(FForm::Required, 'Please provide first name')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL);
        $form->addText('forename', 'First name *')
            ->addRule(FForm::Required, 'Please provide first name')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "First name can't be more than 50 characters", 50);
        $form->addText('surname', 'Last name *')
            ->addRule(FForm::Required, 'Please provide last name')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "Last name can't be more than 160 characters", 160);

        // address
        $form->addFieldset('Prefill');
        $form->addSelect('prefillAddress', 'Prefill Address', $prefillAddress['select'])
            ->setFirstOption('--- Select ---');

        $form->addFieldset('Address');
        $form->addText('premise', 'Building name/number *')
            ->addRule(FForm::Required, 'Please provide Building name/number')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "Building name/number can't be more than 50 characters", 50);
        $form->addText('street', 'Street *')
            ->addRule(FForm::Required, 'Please provide Street')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "Street can't be more than 50 characters", 50);
        $form->addText('thoroughfare', 'Address 3')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "Address 3 can't be more than 50 characters", 50);
        $form->addText('post_town', 'Town *')
            ->addRule(FForm::Required, 'Please provide Town')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "Town can't be more than 50 characters", 50);
        $form->addText('county', 'County')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "County can't be more than 50 characters", 50);
        $form->addText('postcode', 'Postcode *')
            ->addRule(FForm::Required, 'Please provide Postcode')
            ->addRule(array('EnglishKeyboardValidator', 'isValid'), NULL)
            ->addRule('MaxLength', "Postcode can't be more than 15 characters", 15)
            ->addRule(
                array($this, 'Validator_serviceAddressPostCode'),
                'You cannot use our postcode for this address without first purchasing the '
                . Html::el('a')->setText('Service Address Service')->href(FApplication::$router->link(476))->render() .
                '. NB. This is different to the registered office service.'
            );
        $form->addSelect('country', 'Country *', Address::$countries)
            ->addRule(FForm::Required, 'Please provide Country');

        // allotment shares
        if ($this->company->getLastIncorporationFormSubmission()->getForm()->getCompanyType() != 'BYGUAR') {
            $form->addFieldset('Allotment of Shares');
            $form->addSelect('currency', 'Share Currency', IncorporationCapital::$currency);
            $form->addText('share_class', 'Share Class')
                ->setValue('ORD')
                ->readonly();
            $form->addText('num_shares', 'Number of shares')
                ->addRule(FForm::Required, 'Please provide number of shares')
                ->addRule(FForm::NUMERIC, 'Number of shares has to be numeric')
                ->addRule(FForm::GREATER_THAN, 'Number of shares has to be greater than #1', 0)
                ->addRule(FForm::REGEXP, 'Number of shares must be less than 11 digits', '#^\d{1,11}$#')
                ->setValue(1);
            $form->addText('share_value', 'Value per Share')
                ->addRule(FForm::Required, 'Please provide value per shares')
                ->addRule(FForm::NUMERIC, 'Value per share has to be numeric')
                ->addRule(FForm::GREATER_THAN, 'Value per share has to be greater than #1', 0)
                ->addRule(FForm::REGEXP, 'Amount per share can be int or decimal with maximum 6 fraction digits', '#^\d+(.{1}(\d{1,6})){0,1}$#')
                ->setValue(1);
        }

        // security
        $form->addFieldset('Security');
        $form->addText('BIRTOWN', 'First three letters of Town of birth *')
            ->size(3)
            ->addRule(FForm::Required, 'Please provide Town of birth')
            ->addRule(FForm::REGEXP, 'Please enter only 3 letters for Town of birth', '#^[A-Z]{3,3}$#i');
        $form->addText('TEL', 'Last three digits of Telephone number *')
            ->size(3)
            ->addRule(FForm::Required, 'Please provide Telephone number')
            ->addRule(FForm::REGEXP, 'Please enter only 3 numeric characters for Telephone number', '#^\d{3,3}$#');
        $form->addText('EYE', 'First three letters of Eye colour *')
            ->size(3)
            ->addRule(FForm::Required, 'Please provide Eye colour')
            ->addRule(FForm::REGEXP, 'Please enter only 3 letters for Eye colour', '#^[A-Z]{3,3}$#i')
            ->addRule(array($this, 'Validator_securityEyeColour'), 'Eye colour cannot be black. Please enter a valid eye colour (eg. brown, blue, green etc.)');

        $form->addFieldset('Action');

        if ($action == 'insert') {
            $form->addSubmit('continue', 'Continue >')->class('btn_submit fright mbottom');
            $form->onValid = array($this, 'Form_addShareholderCorporateFormSubmitted');
        } else {
            $form->addSubmit('continue', 'Save')->class('btn_submit fright mbottom');
            $form->onValid = array($this, 'Form_editShareholderCorporateFormSubmitted');
            $form->setInitValues($this->shareholder->getFields());
        }

        $form->start();
        return $form;
    }


    /**
     * Returns list of persons
     * @return array
     */
    private function getComponentPersons()
    {
        // get shareholders
        $persons = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncPersons(array('sub'));

        // persons
        foreach ($persons as $key=>&$person) {
            if ($person->isNominee() == TRUE) {
                unset($persons[$key]);
                continue;
            }
            $share = $person->getAllotmentShares()->getFields();
            $person = array(
                'fullName' => $person->getPerson()->getForename() . ' ' .
                $person->getPerson()->getMiddleName() . ' ' .
                $person->getPerson()->getSurname(),
                'editLink' => $this->router->link(self::EDIT_SHAREHOLDER_PERSON_PAGE, 'company_id='.$this->company->getCompanyId(), 'shareholder_id='.$person->getId()),
                'deleteLink' => $this->router->link(self::SHAREHOLDERS_PAGE, 'company_id='.$this->company->getCompanyId(), 'delete_person_id='.$person->getId()),
                'num_shares' => $share['num_shares'],
                'share_value' => $share['share_value'],
                'currency' => $share['currency'],
            );
        }
        unset($person);
        return $persons;
    }

    /**
     * Returns list of persons
     * @return array
     */
    private function getComponentCorporates()
    {
        // get shareholders
        $corporates = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncCorporates(array('sub'));

        // corporates
        foreach ($corporates as $key=>&$corporate) {
            if ($corporate->isNominee() == TRUE) {
                unset($corporates[$key]);
                continue;
            }
            $share = $corporate->getAllotmentShares()->getFields();
            $corporate = array(
                'fullName' => $corporate->getCorporate()->getCorporateName(),
                'editLink' => $this->router->link(self::EDIT_SHAREHOLDER_CORPORATE_PAGE, 'company_id='.$this->company->getCompanyId(), 'shareholder_id='.$corporate->getId()),
                'deleteLink' => $this->router->link(self::SHAREHOLDERS_PAGE, 'company_id='.$this->company->getCompanyId(), 'delete_corporate_id='.$corporate->getId()),
                'num_shares' => $share['num_shares'],
                'share_value' => $share['share_value'],
                'currency' => $share['currency'],
            );
        }
        unset($corporate);
        return $corporates;
    }

    /**
     * Returns list of persons
     * @return array
     */
    private function getComponentNominees()
    {
        // get directors
        $shareholders = array();
        $persons = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncPersons(array('sub'));
        $corporates = $this->company->getLastIncorporationFormSubmission()->getForm()->getIncCorporates(array('sub'));
        $shareholders = array_merge($persons, $corporates);

        // corporates
        foreach ($shareholders as $key=>&$shareholder) {
            if ($shareholder->isNominee() == FALSE) {
                unset($shareholders[$key]);
                continue;
            }

            if ($shareholder instanceof IncorporationPersonubscriber) {
                $shareholder = array('fullName' => $shareholder->getPerson()->getForename() . ' ' . $shareholder->getPerson()->getSurname());
            } else {
                $shareholder = array('fullName' => $shareholder->getCorporate()->getCorporateName());
            }
        }
        unset($shareholder);
        return $shareholders;
    }

    /**
     * returns form for nominee shareholder
     * @return unknown
     */
    private function getComponentNomineeShareholderForm()
    {
        $form = new FForm('nomineeShareholder');
        $form->addFieldset('Nominee');
        $form->addCheckbox('nominee', 'Use Nominee', 1);
        $form->addSubmit('update', 'Update')->class('btn_submit fright mbottom');
        $form->onValid = array($this, 'Form_shareholdersFormSubmitted');

        if ($this->company->getLastIncorporationFormSubmission()->getForm()->hasNomineeSubscriber()) {
            $form['nominee']->setValue(1);
        }

        $form->start();
        return $form;
    }


}
