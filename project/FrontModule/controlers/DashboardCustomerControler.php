<?php

use IdCheckModule\Controllers\IdCheckController;
use PaymentModule\Controllers\PaymentConfirmationController;
use Services\ControllerHelper;


class DashboardCustomerControler extends LoggedControler
{
    // pages
    const MYACCOUNT_PAGE = 65;
    const DASHBOARD_PAGE = 93;
    const BUSINESS_LIBRARY_PAGE = 236;
    const STATUTORY_FORMS_PAGE = 676;

    public function beforePrepare()
    {
        if ($this->customer->isSubscribed == Customer::SUBSCRIBE_DEFAULT) {
            $this->redirect(MyDetailsControler::MY_DETAILS_PAGE);
        }

        if ($this->nodeId == self::MYACCOUNT_PAGE) {
            $this->redirect(self::DASHBOARD_PAGE);
        }

        $sessionNamespace = $this->session->getNamespace(ControllerHelper::SESSION_DEFAULT);

        if (empty($sessionNamespace->skipIdCheckTemporary) && !$this->hasIncompleteSubmission()) {
            if ($this->customerEntity
                && $this->customerEntity->isIdCheckRequired()
                && $this->nodeId != IdCheckController::NODE_DEFAULT
                && $this->nodeId != PaymentConfirmationController::PAGE_PURCHASE_CONFIRMATION) {
                $this->saveBacklink();
                $this->redirectRoute(IdCheckController::PAGE_DEFAULT);
            }
        }

        parent::beforePrepare();
    }

    public function renderDefault()
    {
        $this->template->lastSubmissions = self::getLastSubmissions($this->customer);
    }

    /**
     * @param Customer $customer
     * @return mixed
     */
    public static function getLastSubmissions(Customer $customer)
    {
        $submissions = CHFiling::getIncompleteFormSubmissions($customer->getId());
        foreach ($submissions as $key => $submission) {

            // on error no action
            if ($submission['response'] === 'ERROR') {
                $submissions[$key]['link'] = FALSE;
                continue;
            }

            switch ($submission['form_identifier']) {

                case 'CompanyIncorporation':
                    $submissions[$key]['link'] = FApplication::$router->link(
                        CFRegisteredOfficeControler::REGISTER_OFFICE_PAGE,
                        "company_id={$submission['company_id']}"
                    );
                    break;

                case 'AnnualReturn':
                    $submissions[$key]['link'] = FApplication::$router->link(
                        AnnualReturnControler::COMPANY_DETAILS_PAGE,
                        "company_id={$submission['company_id']}"
                    );
                    break;

                case 'ChangeOfName':
                    $submissions[$key]['link'] = FApplication::$router->link(
                        CUChangeNameControler::CHANGE_NAME_PAGE,
                        "company_id={$submission['company_id']}"
                    );
                    break;

                default:
                    $submissions[$key]['link'] = '#';
                    break;
            }
        }
        return $submissions;
    }

    /**
     * @return bool
     */
    private function hasIncompleteSubmission()
    {
        $submissions = self::getLastSubmissions($this->customer);
        foreach ($submissions as $submission) {
            if ($submission['response'] == 'INCOMPLETE') {
                return TRUE;
            }
        }

        return FALSE;
    }
}
