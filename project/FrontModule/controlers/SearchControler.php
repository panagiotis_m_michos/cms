<?php

use Services\PackageService;

class SearchControler extends DefaultControler
{
    const PACKAGE_FOLDER = 108;

    /** search pages */
    const SEARCH_PAGE = 102;

    const UNAVAILABLE_PAGE = 104;
    const MATRIX_TEST_PAGE = 1207;
    const MATRIX_NEW_TEST_PAGE = 1310;
    const SEARCH_AVAILABLE_TEST_PAGE = 1231;
    const COMPANY_NAME = 1245;
    const SOLE_TRADER_START_UP_PAGE = 819;
    const BYGUAR_PAKAGE_PAGE = 74;
    const LLP_PAKAGE_PAGE = 76;
    /** @var array */
    public $possibleActions = array(
        self::SEARCH_PAGE => 'default',
        self::UNAVAILABLE_PAGE => 'unavailable',
        self::MATRIX_TEST_PAGE => 'matrixTestPage',
        self::SEARCH_AVAILABLE_TEST_PAGE => 'searchTestPage',
        self::MATRIX_NEW_TEST_PAGE => 'matrixNewTestPage',
        self::COMPANY_NAME => 'companyName',
    );

    /** @var string */
    static public $nameSpace = 'searched_company_name';

    /** @var string */
    private $companyName;

    /** @var object Package */
    private $package;

    /**
     * @var PackageService
     */
    private $packageService;

    public function startup()
    {
        parent::startup();

        $this->packageService = $this->getService('services.package_service');
    }

    public function beforePrepare()
    {
        parent::beforePrepare();
        if ($this->nodeId == self::SEARCH_PAGE || $this->nodeId == self::MATRIX_TEST_PAGE || $this->nodeId == self::SEARCH_AVAILABLE_TEST_PAGE) {
            // check if company name is in session - needed for whole controler
            $this->companyName = SearchControler::getCompanyName();

            $this->template->companyName = $this->companyName;
        }
    }

    public function renderDefault()
    {
        $session = FApplication::$session->getNameSpace(self::$nameSpace);
        if (isset($this->post['q'])) {
            $companyName = $this->post['q'];
            $error = CompanyNameValidator::checkName($companyName);

            if ($error !== TRUE) {
                $this->flashMessage($error, 'error');
                $this->redirect(self::UNAVAILABLE_PAGE, 'q=' . $companyName);
            }

            try {
                $available = CompanySearch::isAvailable($companyName);
            } catch (Exception $e) {
                $this->flashMessage(
                    'We are unable to check the availability of this company name at the moment because the Companies House name checking service is unavailable. You may still make this purchase, however you may need to supply an alternative name when the Companies House name checking service becomes available again.',
                    'warning'
                );
                $session = FApplication::$session->getNameSpace(self::$nameSpace);
                $session->companyName = Company::RANDOM_LTD_COMPANY_NAME;
                $this->redirectRoute('package_module.matrix', ['namesearch' => 'unavailable']);
            }

            if ($available == FALSE) {
                $this->redirect(self::UNAVAILABLE_PAGE, 'q=' . $companyName);
            }
            $session->companyName = $companyName;
            $this->redirectRoute('package_module.matrix');
        }

        $this->companyName = SearchControler::getCompanyName();
        $this->template->companyName = $this->companyName;
        $this->template->packages = $this->getComponentPackages(PackageAdminControler::PACKAGES_TEST_FOLDER);
        $this->template->reserveWords = ReservedWord::match($this->companyName);
        $this->template->privacyPackageRenewal = $this->packageService->getPackageById(Package::PACKAGE_RENEWAL_PRIVACY);
        $this->template->compUltiPackageRenewal = $this->packageService->getPackageById(Package::PACKAGE_RENEWAL_COMPREHENSIVE_ULTIMATE);

        if (isset($this->get['namesearch'])) {
            if ($this->get['namesearch'] == 'unavailable') {
                $this->template->companyUnavaliable = 1;
            }
        }

        //redirection for jared remove after testing
        if (!isset($this->get['namesearch'])) {
            if (SearchControler::getCompanyName()) {
                $this->redirect(self::SEARCH_PAGE, 'namesearch=yes');
            } else {
                $this->redirect(self::SEARCH_PAGE, 'namesearch=no');
            }
        }
    }

    public function renderCompanyName()
    {
        if (!isset($this->get['package_id'])) {
            $this->redirect(self::SEARCH_PAGE);
        }

        $form = BaseControler::getCompanyNameForm($this->get['package_id']);
        $this->template->form = $form;
        $this->template->package_id = $this->get['package_id'];
        // reserved words
        $this->template->reserveWords = ReservedWord::match($form['companyName']->getValue());
        if ($form->isOk2Save()) {
            $this->Form_companyNameFormSubmitted($form);
        }
    }

    public function Form_companyNameFormSubmitted(FForm $form)
    {
        // change company name
        if ($form->isSubmitedBy('change')) {
            $session = FApplication::$session->getNameSpace(self::$nameSpace);
            $session->companyName = $form->getValue('companyName');
            $this->redirectRoute('basket_module_package_basket_add_package', ['packageId' => $form->getValue('package')]);
            // search
        } elseif ($form->isSubmitedBy('search')) {
            try {
                $available = CompanySearch::isAvailable($form->getValue('companyName'));
            } catch (Exception $e) {
                $available = TRUE;
                $this->flashMessage(
                    'We are unable to check the availability of this company name at the moment because the Companies House name checking service is unavailable. You may still make this purchase, however you may need to supply an alternative name when the Companies House name checking service becomes available again.'
                );
            }
            $this->template->available = $available;
            $this->template->companyName = $form->getValue('companyName');
        }
    }

    public function renderMatrixNewTestPage()
    {
        $session = FApplication::$session->getNameSpace(self::$nameSpace);
        try {
            $matrix = new MatrixModel();
            $matrixServices = new MatrixServicesModel();
            $this->template->packageService = $matrixServices->getAllPackageServices();
            $this->template->availablePakages = $matrix->getMatrixProperties();
            $this->template->allPackageOffers = $matrixServices->getAllPackageOffers();
            $this->template->packages = $this->getComponentPackages();
            $this->template->reserveWords = ReservedWord::match(SearchControler::getCompanyName());
            $this->template->error = SearchControler::getCHError();
            unset($session->error);
        } catch (Exception $e) {
            $session->error = 'We are unable to check the availability of this company name at the moment because the Companies House name checking service is unavailable. You may still make this purchase, however you may need to supply an alternative name when the Companies House name checking service becomes available again.';
            $this->redirect(self::MATRIX_NEW_TEST_PAGE, 'namesearch=yes');
        }

        //redirection for jared remove after testing
        if (!isset($this->get['namesearch'])) {
            if (SearchControler::getCompanyName()) {
                $this->redirect(self::MATRIX_NEW_TEST_PAGE, 'namesearch=yes');
            } else {
                $this->redirect(self::MATRIX_NEW_TEST_PAGE, 'namesearch=no');
            }
        }
    }

    /*     * ******************************** search test page ******************************* */

    public function prepareSearchTestPage()
    {
        if ($this->companyName == FALSE) {
            $session = FApplication::$session->getNameSpace(self::$nameSpace);
            $session->companyName = Company::RANDOM_LTD_COMPANY_NAME;
        }

        try {
            $matrix = new MatrixModel();
            $matrixServices = new MatrixServicesModel();
            $this->template->packageService = $matrixServices->getAllPackageServices();
            $this->template->availablePakages = $matrix->getMatrixProperties();
            $this->template->allPackageOffers = $matrixServices->getAllPackageOffers();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect(SearchControler::SEARCH_PAGE);
        }
    }

    public function renderSearchTestPage()
    {
        $this->template->packages = $this->getComponentPackages();
        $this->template->reserveWords = ReservedWord::match($this->companyName);
    }

    /*     * **************************** unavailable *************************** */

    protected function prepareUnavailable()
    {
        if (!isset($this->get['q']) && !isset($this->post['q'])) {
            $this->redirect(self::SEARCH_PAGE);
        }
    }

    protected function renderUnavailable()
    {
        $companies = [];

        if (!empty($this->get['q'])) {
            $xmlGateway = new CHXmlGateway();
            $nameSearch = $xmlGateway->getNameSearch($this->get['q'], 'LIVE');
            $nameSearch->setSearchRows(10);

            $res = $xmlGateway->getResponse($nameSearch);
            $xml = simplexml_load_string($res);

            if (isset($xml->Body->NameSearch->CoSearchItem)) {
                foreach ($xml->Body->NameSearch->CoSearchItem as $key => $val) {
                    $companies[] = (array) $val;
                }
            }
        }

        $this->template->form = $this->getComponent('search');
        $this->template->form->onValid[] = function () {
            $this->redirect(self::SEARCH_PAGE);
        };
        $this->template->companies = $companies;
    }

    public function handleMatrixTestPage()
    {

        try {
            $matrix = new MatrixModel();
            $matrixServices = new MatrixServicesModel();
            $this->template->packageService = $matrixServices->getAllPackageServices();
            $this->template->availablePakages = $matrix->getMatrixProperties();
            $this->template->allPackageOffers = $matrixServices->getAllPackageOffers();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect(SearchControler::SEARCH_PAGE);
        }
    }

    public function renderMatrixTestPage()
    {
        $this->companyName = SearchControler::getCompanyName();
        $this->template->packages = $this->getComponentPackages();
        $this->template->reserveWords = ReservedWord::match($this->companyName);
    }

    /*     * **************************** other *************************** */

    /**
     * Returns searched company name
     * @return string
     */
    public static function getCompanyName()
    {
        $session = FApplication::$session->getNameSpace(self::$nameSpace);
        if (isset($session->companyName)) {
            $companyName = String::upper(trim($session->companyName));
            if (!preg_match('#(LTD|LIMITED|CYF|CIC|CYFYNGEDIG)\.?$#', $companyName)) {
                return sprintf("%s LTD", $companyName);
            }
            return $companyName;
        }
        return FALSE;
    }

    public static function resetCompanyName()
    {
        $session = FApplication::$session->getNameSpace(self::$nameSpace);
        if ($session->offsetExists('companyName')) {
            $session->offsetUnset('companyName');
        }
    }

    /**
     * @param string $companyName
     */
    public static function setCompanyName($companyName)
    {
        $session = FApplication::$session->getNameSpace(self::$nameSpace);
        $session->companyName = $companyName;
    }

    /**
     * @return string
     */
    public static function getCHError()
    {
        $session = FApplication::$session->getNameSpace(self::$nameSpace);
        if (isset($session->error)) {
            return $session->error;
        }
        return FALSE;
    }

    public function Validator_companyNameChars(Text $control, $error)
    {
        if (preg_match(
            "/^[-,.:;a-zA-Z0-9\/&$\\\@Â£*?'\"******?!{}<>()\[\] ]{3}[-,.:;a-zA-Z0-9\/&$\\\@Â£*?'\"******?!{}<>()\[\]*=#%+ ]{0,157}+$/",
            $control->getValue()
        )
        ) {
            return TRUE;
        }
        return $error;
    }

    public function Validator_companyName($control, $error)
    {
        if ($control->owner->isSubmitedBy('back') == FALSE) {
            try {
                if ($control->getValue() == '') {
                    return $error[0];
                }
                $available = CompanySearch::isAvailable($control->getValue());
                return ($available === TRUE) ? TRUE : $error[1];
            } catch (Exception $e) {
                return $e->getMessage();
            }
        }
        return TRUE;
    }

}
