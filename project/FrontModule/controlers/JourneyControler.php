<?php

class JourneyControler extends BaseControler
{
    // pages
    const JOURNEY_PAGE = 140;
    const CONFIRMATION_PAGE = 146;
    const TAX_ASSIST_PRODUCT = 544;

    /**
     * @var string
     */
    public static $handleNode = 'JourneyPageModel';

    /**
     * @var array
     */
    public $possibleActions = array(
        self::JOURNEY_PAGE => 'default',
        self::CONFIRMATION_PAGE => 'confirmation',
    );

    /**
     * @var JourneyEmailer
     */
    public $journeyEmailer;

    public function startup()
    {
        parent::startup();
        $this->journeyEmailer = $this->getService(DiLocator::EMAILER_JOURNEY);
    }

    protected function renderDefault()
    {
        $this->template->isSubmitted = $this->getParameter('submitted');

        if (Customer::isSignedIn() === TRUE && Customer::getSignedIn()->isWholesale() || Customer::getSignedIn()->isAffiliate()) {
            $this->template->isWholesale = TRUE;
        } else {
            $this->template->isWholesale = FALSE;
        }

        // form
        $form = new FForm('journey');

        // basket ids
        $namespace = $this->getSession('journeyBasketIds');
        $basketIds = isset($namespace->ids) ? $namespace->ids : array();

        // products
        $arr = array('column1', 'column2', 'column3');
        foreach ($arr as $key => $val4) {
            $categories = $this->node->getProperty($val4, JourneyPage::JOURNEY_PAGE);
            $products = array();
            foreach (explode(',', $categories) as $key2 => $category) {
                $ids = JourneyProduct::getChildsIds($category, FALSE);
                foreach ($ids as $key3 => $val3) {
                    $product = new JourneyProduct($val3);
                    $intersect = array_intersect($product->blacklistedProducts, $basketIds);
                    if (empty($intersect)) {
                        if ($product->isOk2show()) {
                            $products[$val3] = $product;
                            // products
                            foreach ($products as $key => $val) {
                                $form->addCheckbox($key, 'Please tick', 1)->setGroup('products')->class('fright')->addRule(array($this, 'Validator_Products'), "Please select at least one tick box. Or select 'No thanks, continue' at the bottom of this page to skip.");
                            }
                        }
                    }
                }
                if (isset($products)) {
                    $cat[JourneyProduct::getNodeTitle($category)] = $products;
                }
                unset($products);
            }
            $columns[$val4] = $cat;
            unset($cat);
        }

        $form->addSubmit('send', 'Submit')->class('btn_submit fright mbottom mright');
        $form->onValid = array($this, 'Form_defaultFormSubmitted');
        $form->start();

        $this->template->form = $form;
        $this->template->hide = 1;
        $this->template->columns = $columns;
        //        continue
        //        $backlink = $this->getBacklink();
        //        if ($backlink !== FALSE) {
        //            $this->template->continueLink = $backlink;
        //        }
    }

    public function Form_defaultFormSubmitted(FForm $form)
    {
        $products = array();
        $data = $form->getValues();
        $namespace = $this->getSession('productIds');
        $namespace->products = $data['products'];
        $form->clean();
        $this->redirect(self::CONFIRMATION_PAGE);
    }

    protected function renderConfirmation()
    {
        $products = array();
        // get and check choosen product if none redirect to previos page
        $ids = $this->getSession('productIds')->products;
        if (!isset($ids)) {
            $this->redirect(self::JOURNEY_PAGE);
        }
        foreach ($ids as $key => $val) {
            if ($val == 1) {
                $product = new JourneyProduct($key);
                if ($product->isOk2show()) {
                    $products[$key] = $product;
                }
            }
        }
        // form
        $form = new FForm('journey2');
        foreach ($products as $key => $val) {
            // adding checkbox
            $form->addCheckbox($key, $val->getLngTitle(), 1)->setGroup('products')->class('fright');
            $jorney = new JourneyProduct($key);
            $questionsArray = $jorney->getQuestionsArray();
            foreach ($questionsArray as $questionKey => $question) {
                $form->addSelect($key . '_' . $questionKey, $question['question'], $question['answers'])->setFirstOption('--- Select --')->addRule(FForm::Required, 'Required!');
            }
        }
        // contact form
        $form->addText('email', 'Email:')->addRule(FForm::Required, 'Please provide email.')->addRule(FForm::Email, 'Email is not valid.')->class('field220');
        $form->addText('fullName', 'Full name:')->addRule(FForm::Required, 'Please provide full name.')->class('field220');
        $form->addText('phone', 'Contact number:')->addRule(FForm::Required, 'Please provide contact number.')->class('field220');
        $form->addText('postcode', 'Postcode:')->addRule(FForm::Required, 'Please provide postcode.')->class('field220');
        $form->addText('company_name', 'Company name:')->addRule(FForm::Required, 'Please provide company name.')->class('field220');

        if (Customer::isSignedIn()) {
            $customer = Customer::getSignedIn();
            $form['email']->setValue($customer->email);
            $form['fullName']->setValue($customer->firstName . ' ' . $customer->lastName);
            $form['phone']->setValue($customer->phone);
            $form['postcode']->setValue($customer->postcode);
        }

        $form->addSubmit('send', 'Submit')->class('btn_submit fright mbottom mright');
        $form->onValid = array($this, 'Form_confirmationFormSubmitted');
        $form->start();

        $this->template->form = $form;
        $this->template->products = $products;
        $this->template->hide = 1;

        //        continue
        //        $backlink = $this->getBacklink();
        //        if ($backlink !== FALSE) {
        //           $this->template->continueLink = $backlink;
        //        }
    }

    public function Form_confirmationFormSubmitted(FForm $form)
    {
        $products = array();
        $data = $form->getValues();
        if (isset($data['1153_0'])) {
            if ($data['1153_0'] == 0) {
                $data['products']['1153'] = 1;
            }
        }
        foreach ($data['products'] as $key => $val) {
            if ($val == 0) {
                $product = new JourneyProduct($key);
                if ($product->isOk2show()) {
                    $products[$key] = $product;
                }
            }
        }
        $journeyCustomer = $this->saveJourneyProducts($data, $products);
        $products = JourneyCustomerProduct::getAllObjects(NULL, NULL, array('journeyCustomerId' => $journeyCustomer->getId()));
        foreach ($products as $product) {
            $this->journeyEmailer->journeyProductEmail($product, $journeyCustomer);
            $this->journeyEmailer->internalJorneyProductEmail($product, $journeyCustomer);
        }
        // clear session with basket ids
        $namespace = $this->getSession('journeyBasketIds');
        $namespace->remove();
        $form->clean();

        $this->flashMessage('Thank you for your interest in these support services. Further details will be emailed to you.');
        $this->redirect(DashboardCustomerControler::DASHBOARD_PAGE);
    }

    private function saveJourneyProducts($data, $products)
    {
        //get products which customer chosen and save them
        $journeyCustomer = new JourneyCustomer;
        $journeyCustomer->fullName = $data['fullName'];
        $journeyCustomer->email = $data['email'];
        $journeyCustomer->phone = $data['phone'];
        $journeyCustomer->postcode = $data['postcode'];
        $journeyCustomer->companyName = $data['company_name'];
        if (Customer::isSignedIn()) {
            $journeyCustomer->customerId = Customer::getSignedIn()->getId();
        }
        $journeyCustomerId = $journeyCustomer->save();

        //get products which customer chosen and save them
        foreach ($products as $key => $val) {
            $product = new JourneyCustomerProduct();
            $product->journeyCustomerId = $journeyCustomerId;
            $product->productId = $val->getId();
            $product->productName = $val->getLngTitle();
            $product->save();

            // generate products array
            $products = $data['products'];
            $justProducts = array();
            foreach ($products as $productKey => $productValue) {
                $justProducts[] = $productKey;
            }

            //get question and answers and save them
            foreach ($data as $key2 => $val2) {
                $clearKey = substr($key2, 0, -2);
                $checker = in_array($clearKey, $justProducts);
                if ($checker == 1) {
                    $productInfo = explode('_', $key2);
                    $productId = $productInfo[0];
                    $questionId = $productInfo[1];
                    $journeyProduct = new JourneyProduct($productId);
                    $jorneyQuestions = $journeyProduct->getQuestionsArray();
                    $jorneyQuestion = $jorneyQuestions[$questionId];
                    $question = new JourneyCustomerProductQuestion();
                    $question->answer = $jorneyQuestion['answers'][$val2];
                    $question->question = $jorneyQuestion['question'];
                    $question->journeyCustomerProductId = $product->getId();
                    $question->save();
                }
            }
            // save lead for tax assists
            if ($val->getId() == self::TAX_ASSIST_PRODUCT) {
                try {

                    $service = $this->getService(DiLocator::SERVICE_TAX_ASSIST_LEAD);
                    $service->processJourneyPageData($data);

                } catch (Exception $e) {
                    $this->journeyEmailer->errorEmail($e);
                }
            }
        }
        return $journeyCustomer;
    }

    /*     * ********************** confirmation ******************* */

    public function renderConfirmation2()
    {
        $backlink = $this->getBacklink();
        if ($backlink !== FALSE) {
            $this->removeBacklink();
            $this->template->continueLink = $backlink;
        }
    }

    /*     * ********************** validators ******************* */

    public function Validator_Products($control, $error)
    {
        $data = $control->owner->getValues();
        foreach ($data['products'] as $val) {
            if ($val != 0) {
                return TRUE;
            }
        }
        $control->owner->addErr('products', $error);
        return TRUE;
    }

}
