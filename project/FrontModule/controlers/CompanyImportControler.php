<?php

class CompanyImportControler extends LoggedControler
{
	/* vevericka */
//	const COMPANY_IMPORT_PAGE = 544;
	
	/* live */
	const COMPANY_IMPORT_PAGE = 677;
	
	
    protected function renderDefault()
	{
		$form = new FForm('companyImport');
		$form->setRenderClass('Default2Render');

		$form->addFieldset('Company data');
		$form->addText('companyNumber', 'Company number: *')
			->addRule(FForm::Required, 'Please provide Company number.');
		$form->addText('auth', 'Authentication code: *')
			->addRule(FForm::Required, 'Please provide Authentication code.');
			
		$form->addFieldset('Action');
		$form->addSubmit('import', 'Import Company')->style('width: 200px; height: 30px;');
		
		$form->onValid = array($this, 'Form_companyImportSubmitted');
		$form->start();
		$this->template->form = $form;
	}
	
	
	/**
	 * Provide import new company
	 *
	 * @param FForm $form
	 * @return void
	 */
    public function Form_companyImportSubmitted(FForm $form)
	{
		try {
			$chFiling = new CHFiling();
			$data = $form->getValues();
		    $xmlResponse = $chFiling->importCompany($data['companyNumber'], $data['auth'], $this->customer->getId());
		    $form->clean();
		    $this->flashMessage('Company has been imported.');
		} catch(Exception $e) {
		    $this->flashMessage($e->getMessage(), 'error');
		}
		$this->redirect(DashboardCustomerControler::DASHBOARD_PAGE);
	}
}
