<?php

/**
 * CMS
 * @category   Project
 * @package    CMS
 * @author     Nikolai Senkevich
 * @version    SignupsControler.php 2012-03-14 
 */
class SignupsControler extends BaseControler
{
    const STARTING_BUSINESS = 868;
    const SAVVY_SURVEY = 1183;

    public $possibleActions = array(
        self::STARTING_BUSINESS => 'startingBusiness',
        self::SAVVY_SURVEY => 'savvySurvey',
    );
    /**
     * @var string
     */
    static public $handleObject = 'SignupsModel';
    /**
     * @var SignupsModel
     */
    public $node;

    public function beforePrepare()
    {
        $this->template->hide = TRUE;
    }

    public function handleStartingBusiness()
    {
        if (isset($this->get['pdf'])) {
            $nameSpace = FApplication::$session->getNamespace('whitePaperPdf');
            if (isset($nameSpace->outputStartingBusinessPdf)) {
                unset($nameSpace->outputStartingBusinessPdf);
                try {
                    $this->node->outputStartingBusinessPdf();
                } catch (Exception $e) {
                    $this->flashMessage($e->getMessage(), 'error');
                    $this->redirect();
                }
            }
        }
    }

    public function renderStartingBusiness()
    {
        $nameSpace = FApplication::$session->getNamespace('whitePaperPdf');
        if (isset($nameSpace->outputStartingBusinessPdf)) {
            $this->template->displayLink = TRUE;
        }

        $form = new SignupsStartingBusinessForm($this->node->getId() . '_SignupSignup');
        $form->startup($this, array($this, 'Form_signupSignupFormSubmitted'));
        $this->template->form = $form;
    }

    /**
     * @param FForm $form
     */
    public function Form_signupSignupFormSubmitted(FForm $form)
    {
        try {
            $form->process();
            $this->redirect();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    public function handleSavvySurvey()
    {
        if (isset($this->get['pdf'])) {
            $nameSpace = FApplication::$session->getNamespace('whitePaperPdf');
            if (isset($nameSpace->outputSavvySurveyPdf)) {
                unset($nameSpace->outputSavvySurveyPdf);
                try {
                    $this->node->outputSavvySurveyPdf();
                } catch (Exception $e) {
                    $this->flashMessage($e->getMessage(), 'error');
                    $this->redirect();
                }
            }
        }
    }

    public function renderSavvySurvey()
    {
        $nameSpace = FApplication::$session->getNamespace('whitePaperPdf');
        if (isset($nameSpace->outputSavvySurveyPdf)) {
            $this->template->displayLink = TRUE;
        }

        $form = new SignupsSavvySurveyForm($this->node->getId() . '_SavvySurvey');
        $form->startup($this, array($this, 'Form_SavvySurveyFormSubmitted'));
        $this->template->form = $form;
    }

    /**
     * @param FForm $form
     */
    public function Form_SavvySurveyFormSubmitted(FForm $form)
    {
        try {
            $form->process();
            $this->redirect();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

}
