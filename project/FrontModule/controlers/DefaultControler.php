<?php

class DefaultControler extends BaseControler
{

    public function beforePrepare()
    {
        parent::beforePrepare();
        if ($this->isPopup()) {
            $this->template->popup = TRUE;
            Debug::disableProfiler();
        }
    }

    /**
     * @return bool
     */
    public function isPopup()
    {
        return isset($this->get['popup']);
    }

    /* ******************************** other ******************************** */

    /**
     * @param string $url
     */
    final protected function reloadOpener($url = NULL)
    {
        if ($url === NULL) {
            echo '<script>parent.location.reload();</script>';
            exit;
        } else {
            echo '<script>parent.location.href="' . $url . '";</script>';
            exit;
        }
    }

}
