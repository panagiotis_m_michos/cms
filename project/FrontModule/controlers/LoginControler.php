<?php

class LoginControler extends DefaultControler
{

    const LOGIN_PAGE = 127;
    const FORGOTTEN_PASSWORD_PAGE = 247;
    const LOGOUT_PAGE = 568;

    /** @var array */
    public $possibleActions = array(
        self::LOGIN_PAGE => 'default',
        self::FORGOTTEN_PASSWORD_PAGE => 'forgotten',
        self::LOGOUT_PAGE => 'logout'
    );

    /** @var boolean */
    protected $httpsRequired = TRUE;

    public function startup() 
    {
        parent::startup();
        if (Customer::isSignedIn()) {
            if (isset($this->get['l'])) {
                CustomerActionLogger::log(Customer::getSignedIn(), CustomerActionLogger::LOG_OUT);
                Customer::signOut();
                $this->redirect(self::LOGOUT_PAGE);
            }
            $this->redirect(DashboardCustomerControler::DASHBOARD_PAGE);
        }
    }

    /*     * *************************************** login and registration **************************************** */

    protected function prepareDefault() 
    {
        // login form
        $loginForm = new FForm('login');
        $loginForm->addText('email-login', 'Email address:')
            ->class('field220')
            ->addRule(FForm::Required, 'Please provide e-mail address!')
            ->addRule('Email', 'Email address is not valid!');
        $loginForm->addPassword('password-login', 'Password:')
            ->class('field220')
            ->addRule(FForm::Required, 'Please provide password!')
            ->addRule(array($this, 'Validator_loginCredentials'), array(1 => "Email not found!", "Password doesn't match!", "Your account haven't been validated!", "Your account is blocked. Please contact our customer supoort line on 0207 608 5500."));
        $loginForm->addSubmit('login', 'Login')
            ->class('btn_submit');
        $loginForm->onValid = array($this, 'Form_loginFormSubmitted');
        $loginForm->start();
        $this->template->loginForm = $loginForm;

        // register form
        $registerForm = new FForm('register');
        $registerForm->addText('email', 'Email *')
            ->class('field220')
            ->addRule(FForm::Required, 'Please provide e-mail address!')
            ->addRule(FForm::Email, 'Email is not valid!')
            ->addRule(array($this, 'Validator_uniqueLogin'), 'Email address has been taken.');
        $registerForm->addPassword('password', 'Password *')
            ->class('field220')
            ->addRule(FForm::Required, 'Please provide password!');
        $registerForm->addPassword('passwordConf', 'Confirm Password *')
            ->class('field220')
            ->addRule(FForm::Required, 'Please provide password!')
            ->addRule(FForm::Equal, 'Passwords are not the same!', 'password');
        $registerForm->addSubmit('register', 'Create Account ')
            ->class('btn_submit');
        $registerForm->onValid = array($this, 'Form_registerFormSubmitted');
        $registerForm->start();
        $this->template->registerForm = $registerForm;
    }

    public function Form_loginFormSubmitted($form) 
    {
        $customer = Customer::doAuthenticate($form['email-login']->getValue(), $form['password-login']->getValue());
        CustomerActionLogger::log($customer, CustomerActionLogger::LOG_IN);
        Customer::signIn($customer->getId());

        // redirect
        $backlink = $this->getBacklink();
        if ($backlink !== FALSE) {
            $this->removeBacklink();
            $this->redirect("%$backlink%");
        } else {
            $this->redirect(DashboardCustomerControler::DASHBOARD_PAGE);
        }
    }

    public function Form_registerFormSubmitted($form) 
    {
        $data = $form->getValues();

        $customer = new Customer;
        $customer->email = $data['email'];
        $customer->password = $data['password'];
        //$customer->isSubscribed = 1;
        $customerId = $customer->save();

        // sign in
        Customer::signIn($customerId);
        $this->flashMessage('Your account has been created.');

        // redirect
        $backlink = $this->getBacklink();
        if ($backlink !== FALSE) {
            $this->removeBacklink();
            $this->redirect("%$backlink%");
        } else {
            $this->redirect(DashboardCustomerControler::DASHBOARD_PAGE);
        }
    }

    /*     * *************************************** forgotten password **************************************** */

    protected function prepareForgotten() 
    {
        $form = new FForm('forgottenPassword');

        // action
        $form->addFieldset('Your email');
        $form->addText('email', 'Email:')
            ->addRule(FForm::Required, 'Email is required!')
            ->addRule(FForm::Email, 'Email is not valid!')
            ->addRule(array($this, 'Validator_forgetEmail'), 'Your email address could not be found. Please check the spelling and try again.')
            ->size(30);

        // action
        $form->addFieldset('Action');
        $form->addSubmit('submit', 'Send')->class('btn_submit');

        $form->onValid = array($this, 'Form_forgottenFormSubmitted');
        $form->start();
        $this->template->form = $form;
    }

    public function Form_forgottenFormSubmitted(FForm $form) 
    {
        // re-save customer
        $customer = Customer::getCustomerByEmail($form->getValue('email'));
        $customer->password = FTools::generPwd(8);
        $customer->save();

        $link = FApplication::$httpRequest->uri->getHostUri() . $this->router->link(self::LOGIN_PAGE);
        /** @var ForgottenPasswordEmailer $forgottenPasswordEmailer */
        $forgottenPasswordEmailer = $this->getService(DiLocator::EMAILER_FORGOTTEN_PASSWORD);
        $forgottenPasswordEmailer->forgottenPasswordEmail($link, $customer);

        $form->clean();
        $this->flashMessage('New password has been sent to your email.');
        $this->redirect();
    }

    /**
     * Checking if customer email is in database
     *
     * @param object $control
     * @param mixed $error
     * @param mixed $params
     * @return mixed
     */
    public function Validator_forgetEmail($control, $error, $params) 
    {
        try {
            Customer::getCustomerByEmail($control->getValue());
            return TRUE;
        } catch (Exception $e) {
            return $error;
        }
    }

    /*     * **************************** validators ***************************** */

    /**
     * Validator
     * @param object $control
     * @param mixed $error
     * @param mixed $params
     * @return mixed
     */
    public function Validator_uniqueLogin($control, $error, $params) 
    {
        $customerId = $this->db->select('customerId')->from(TBL_CUSTOMERS)->where('email=%s', $control->getValue())->execute()->fetchSingle();
        if ($customerId) {
            return $error; 
        }
        return TRUE;
    }

    /**
     * Validator
     * @param object $control
     * @param mixed $error
     * @param mixed $params
     * @return mixed
     */
    public function Validator_loginCredentials($control, $error, $params) 
    {
        // authentificate user
        try {
            $user = Customer::doAuthenticate($control->owner['email-login']->getValue(), $control->owner['password-login']->getValue());
            return TRUE;
        } catch (Exception $e) {
            if ($e->getMessage() == 1) {
                $error = $error[1];
                return $error;
            } elseif ($e->getMessage() == 2) {
                $error = $error[2];
                return $error;
            } elseif ($e->getMessage() == 3) {
                $error = $error[3];
                return $error;
            } else {
                $error = $error[4];
                return $error;
            }
        }
    }

}
