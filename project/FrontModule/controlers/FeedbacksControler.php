<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    FeedbacksForm.php 2011-03-21 razvanp@madesimplegroup.com
 */

class FeedbacksControler extends DefaultControler
{
    /* pages */
    const FEEDBACK_PAGE = 944;

    
    private $nodeObject;
    
    public function  beforePrepare() {
        parent::beforePrepare();
        Debug::disableProfiler();
        if (!isset($this->get['popup'])) {
            $this->redirect(HomeControler::HOME_PAGE);
        }
        try {
            $nodeId = isset($this->get['nodeId']) ? $this->get['nodeId'] : NULL;
            if ( !isset($nodeId) || !$nodeId)
                throw new Exception('You can\'t leave feedback for this page');
                 
            $this->nodeObject = new FNode( (int) $nodeId);
            if ($this->nodeObject->getId() == 0 || $this->nodeObject->getId() < 0) {
                throw new Exception('You can\'t leave feedback for this page!');
            }
            
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->reloadOpener();
        }
            
    }

    /********************************************* Default ********************************************* */

    public function renderDefault() {
        $this->template->err = '';

        $form = new FeedBackForm($this->node->getId() . '_FeedbackFront');
        $form->startup($this, array($this, 'Form_FeedbackFrontFormSubmitted'), $this->nodeObject);
        $this->template->form = $form;     
     }

     /**
     * @param FeedBackForm $form
     */
    public function Form_FeedbackFrontFormSubmitted(FeedBackForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Thank you, your feedback has been received!');
            $this->redirect();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }
}