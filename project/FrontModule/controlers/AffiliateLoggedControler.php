<?php

class AffiliateLoggedControler extends DefaultControler
{
    const DASHBOARD_PAGE = 977;
    const WHITE_LABEL_PAGE = 978;
    const SOLE_V_LTD_PAGE = 979;
    const AFFILIATE_LINKS_PAGE = 981;
    const CUSTOMIZE_PAGE = 980;
    const MY_DETAILS_PAGE = 982;
    const MY_REPORTS = 986;
    const CUSTOMERS_PAGE = 999;
    const ORDERS_PAGE = 998;
    const MY_MARKUP = 1000;
    const ANALYTICS_PAGE = 1006;
    const SOLE_V_LTD_REPORT_PAGE = 1393;

    /**
     * @var array
     */
    public $possibleActions = [
        self::DASHBOARD_PAGE => 'dashboard',
        self::WHITE_LABEL_PAGE => 'whiteLabel',
        self::SOLE_V_LTD_PAGE => 'soleVLtd',
        self::AFFILIATE_LINKS_PAGE => 'affiliateLinks',
        self::CUSTOMIZE_PAGE => 'customize',
        self::MY_DETAILS_PAGE => 'myDetails',
        self::MY_REPORTS => 'reports',
        self::ORDERS_PAGE => 'orders',
        self::CUSTOMERS_PAGE => 'customers',
        self::MY_MARKUP => 'markUp',
        self::ANALYTICS_PAGE => 'analytics',
        self::SOLE_V_LTD_REPORT_PAGE => 'soleVLtdReport',
    ];

    /**
     * @var Affiliate
     */
    protected $affiliate;

    /**
     * @var string
     */
    static public $handleObject = 'AffiliatesModel';

    /**
     * @var AffiliatesModel
     */
    public $node;

    public function startup()
    {
        if (!Affiliate::isSignedIn()) {
            $this->redirect(AffiliateControler::LOGIN_PAGE);
        }
        parent::startup();
        $session = FApplication::$session->getNameSpace('affiliate');
        $this->affiliate = new Affiliate($session->affiliateId);
        $this->template->sign = TRUE;
    }

    /*     * ******************************************** Dashboard ********************************************* */

    public function renderDashboard()
    {
        $this->template->affiliate = $this->affiliate;
        $this->template->affiliateId = $this->affiliate->affiliateId;
    }

    /*     * ******************************************** whiteLabel ********************************************* */

    public function renderWhiteLabel()
    {
        $this->template->affiliate = $this->affiliate;
        $this->template->affiliateId = $this->affiliate->affiliateId;
    }

    /*     * ******************************************** soleVLtd ********************************************* */

    public function renderSoleVLtd()
    {
        $this->template->affiliate = $this->affiliate;
        $this->template->affiliateId = $this->affiliate->affiliateId;
    }

    /*     * ******************************************** affiliateLinks ********************************************* */

    public function renderAffiliateLinks()
    {
        $this->template->affiliate = $this->affiliate;
        $this->template->affiliateId = $this->affiliate->affiliateId;
    }

    /*     * ******************************************** customize ********************************************* */

    public function renderCustomize()
    {
        $form = new AffiliateCustomizeForm($this->node->getId() . '_customize');
        $form->startup($this, [$this, 'Form_customizeFormSubmitted']);
        $this->template->form = $form;
    }

    /**
     * @param AffiliateCustomizeForm $form
     */
    public function Form_customizeFormSubmitted(AffiliateCustomizeForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Your colours have been updated.');
            $this->redirect(self::CUSTOMIZE_PAGE);
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    /*     * ******************************************** myDetails ********************************************* */

    public function renderMyDetails()
    {
        $form = new AffiliateMyDetailsForm($this->node->getId() . '_myDetails');
        $form->startup($this, [$this, 'Form_myDetailsFormSubmitted']);
        $this->template->form = $form;
        $this->template->affiliate = $this->affiliate;
    }

    /**
     * @param AffiliateMyDetailsForm $form
     */
    public function Form_myDetailsFormSubmitted(AffiliateMyDetailsForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('Details has been changed');
            $this->redirect(self::DASHBOARD_PAGE);
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    public function renderOrders()
    {
        try {
            $this->node->startup($this->affiliate->affiliateId);

            $filter = $this->node->getComponentOrdersFilterForm();
            $this->template->form = $filter;
            $this->template->affiliateId = $this->affiliate->affiliateId;
            // get CSV
            if (isset($this->get['csv'])) {
                Debug::disableProfiler();
                $this->node->outputAffiliateOrdersPublicCSV($filter);
            }

            $paginator = $this->node->getAffiliateOrdersPaginator($filter);
            $this->template->paginator = $paginator;
            $this->template->orders = $this->node->getAffiliateOrders($filter, $paginator);
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('list');
        }
    }

    public function renderCustomers()
    {
        try {
            $this->node->startup($this->affiliate->affiliateId);
            $paginator = $this->node->getAffiliateCustomersPaginator();
            $this->template->paginator = $paginator;
            $this->template->customers = $this->node->getAffiliateCustomers($paginator);
            $this->template->affiliateId = $this->affiliate->affiliateId;
            // get CSV
            if (isset($this->get['csv'])) {
                Debug::disableProfiler();
                $this->node->outputAffiliateCustomersPublicCSV();
            }
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('list');
        }
    }

    /*     * ******************************************** Mark Up ********************************************* */

    public function prepareMarkUp()
    {
        $form = new AffiliateMarkUpForm('affiliateMarkUp');
        $session = FApplication::$session->getNameSpace('affiliate');
        $form->startup($this, [$this, 'Form_markUpFormSubmitted']);
        $this->template->form = $form;
    }

    public function renderMarkUp()
    {
        $session = FApplication::$session->getNameSpace('affiliate');
        $paginator = new FPaginator2(AffiliateProduct::getCount(['affiliateId' => $this->affiliate->affiliateId]));
        $this->template->affiliateProducts = AffiliateProduct::getAffiliateProducts($this->affiliate->affiliateId);
        $this->template->affiliateId = $this->affiliate->affiliateId;
        $paginator->htmlDisplayEnabled = FALSE;
        $this->template->paginator = $paginator;
    }

    /**
     * @param FForm $form
     */
    public function Form_markUpFormSubmitted(FForm $form)
    {
        try {
            $form->process();
            $this->flashMessage('White label mark up prices updated.');
            $this->redirect();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

    public function renderAnalytics()
    {
        try {
            $this->node->startup($this->affiliate->affiliateId);

            $filter = $this->node->getComponentAnalyticsFilterForm();
            $this->template->form = $filter;
            $this->template->affiliateId = $this->affiliate->affiliateId;
            // get CSV
            if (isset($this->get['csv'])) {
                Debug::disableProfiler();
                $this->node->outputAffiliateAnalyticsCSV($filter);
            }

            $this->template->reports = $this->node->getAffiliateAnalytics($filter);
        } catch (Exception $e) {
            $this->logger->error($e);
            $this->flashMessage('An error occurred during report creation. Our staff has been notified.', 'error');
            $this->redirect(AffiliateLoggedControler::MY_REPORTS);
        }
    }

    public function renderSoleVLtdReport()
    {
        try {
            $this->node->startup($this->affiliate->affiliateId);

            $filter = $this->node->getComponentAnalyticsFilterForm();
            $this->template->form = $filter;
            $this->template->affiliateId = $this->affiliate->affiliateId;
            // get CSV
            if (isset($this->get['csv'])) {
                Debug::disableProfiler();
                $this->node->outputAffiliateAnalyticsCSV($filter);
            }

            $this->template->reports = $this->node->getAffiliateSoleVLtdReport($filter);
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect('list');
        }
    }

}
