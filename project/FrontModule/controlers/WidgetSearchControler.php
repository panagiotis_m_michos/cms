<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    WidgetSearchControler.php 2010-02-18 divak@gmail.com
 */


class WidgetSearchControler extends BaseControler
{
	/* vevericka */
//	const HOME_PAGE = 644;
//	const DEMO_PAGE = 751;
	
	/* live */
	const HOME_PAGE = 644;
	const DEMO_PAGE = 867;
	
	/**
	 * @var array
	 */
	public $possibleActions = array(
		self::HOME_PAGE => 'home',
		self::DEMO_PAGE => 'demo',
	);
	
	/**
	 * @var string
	 */
	static public $handleObject = 'WidgetSearchModel';
	
	/**
	 * @var WidgetSearchModel
	 */
	public $node;
	
	
	public function beforePrepare()
	{
		parent::beforePrepare();
		Debug::disableProfiler();
		
		try {
			if (isset($this->get['hash'])) {
				$this->node->startup($this->get['hash']);
			} else {
				throw new Exception("Missing Hash Parameter");
			}	
		} catch (Exception $e) {
		    $this->flashMessage($e->getMessage(), 'error');
		 	$this->redirect("%{$this->getHomeLink()}%");
		}
	}
	
	/**
	 * @return void
	 */
	public function beforeRender()
	{
		$affiliate = $this->node->getAffiliate();
		$this->template->affiliate = $affiliate;
		$this->template->companyColour = $affiliate->companyColour;
		$this->template->textColour = $affiliate->textColour;
		parent::beforeRender();
	}
	
	
	/********************************************* home *********************************************/ 
	
	
	public function renderHome()
	{
		$this->template->form = $this->node->getComponentSearchForm(array($this, 'Form_defaultFormSubmitted'));
	}
	
	
	/**
	 * Handle with search
	 * 
	 * @param FForm $form
	 * @return void
	 */
	public function Form_defaultFormSubmitted(FForm $form)
	{
		try {
			$companyName = $form->getValue('companyName');
			$this->template->available = CompanySearch::isAvailable($companyName);
			$form->clean();
		} catch (Exception $e) {
			$this->template->respond = 'Unable to check';
		}
	}
	
	
	/********************************************* demo *********************************************/
	
	public function renderDemo()
	{
		Debug::disableProfiler();
		$this->template->iframeUrl = FApplication::$router->link(self::HOME_PAGE, array('hash' => $this->node->getAffiliate()->hash));
	}
	
}