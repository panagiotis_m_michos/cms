<?php

class CompanyShareholderCerificateControler extends CUControler
{
    
    /** 
     * const PAGE 
     */
    const SHAREHOLDERS_CERTIFICATE_PAGE = 1105;

    /**
     * @var CompanyShareholderCerificateModel
     */
    static public $handleObject = 'CompanyShareholderCerificateModel';

    /**
     * @var CompanyShareholderCerificateModel
     */
    public $node;
    
    /**
     *
     * @var Company 
     */
    public $company;

    /**
     *
     * @var PersonSubscriber  
     */
    public $shareHolder;

    /**
     * @var Object $shareHolderInfo
     */
    public $shareHolderInfo;

    /**
     * @var int
     */
    public $shareHolderId;

    
    public function beforePrepare() 
    {
        parent::beforePrepare();
        
        try {
            if (!isset($this->get['shareholderId'])) {
                throw new Exception('Sorry, we are unable to auto produce your share certificate. Please download this <a href="/project/upload/files/share-certificate-template.doc" title="" style="color: blue;"> share certificate template</a>.');
            }
            $this->shareHolder = $this->node->getShareholderInformations($this->company, $this->get['shareholderId']);
            $this->shareHolderId = (int) $this->get['shareholderId'];
            $this->shareHolderInfo = $this->node->checkIsOkForPrint();
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), "error", FALSE);
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
        }
    }

    public function prepareDefault()
    {
        
        $address = (object) array_map("trim", $this->shareHolder->getAddress()->getFields());

        if ($address->premise && $address->street && $address->post_town && $address->country && $address->postcode) {
            $data = $this->company->getData();
            $number = !empty($data['shareholders']) ? array_search($this->shareHolderId, array_keys($data['shareholders'])) : 1;
            $number = $number > 0 ? $number + 1 : 1;
            $this->node->createPdfShareholderCerificate($this->shareHolderInfo, 'ShareholderCertificate.pdf', $number);
            exit;
        }
        $this->template->shareholder = $this->shareHolder->getFields();
        $this->template->share = $this->shareHolderInfo;
    }

    public function renderDefault()
    {
        
        $form = new CompanyShareholderCerificateForm($this->node->getId() . '_printShareHolderCertificateFront');
        $form->startup($this, array($this, 'Form_companyShareholderFrontFormSubmitted'), $this->shareHolderInfo, $this->company, 100);
        $this->template->form = $form;
    }

    /**
     * @param CompanyShareholderCerificateForm $form
     */
    public function Form_companyShareholderFrontFormSubmitted(CompanyShareholderCerificateForm $form)
    {
        try {
            //generate pdf from form and shareholder data
            $form->process();
            $this->flashMessage('Address has been updated. Please download your certificate now');
            $this->redirect(CUSummaryControler::SUMMARY_PAGE, "company_id={$this->company->getCompanyId()}");
        } catch (Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            $this->redirect();
        }
    }

}
