<?php

namespace Front;

use DataGrid;
use DataGridTextColumn;
use Entities\Cashback;
use Entities\Company;

class CashbacksDataGrid extends DataGrid
{
    /**
     * @param DataGridTextColumn $column
     * @param Company $company
     * @param Cashback $cashback
     * @return string
     */
    public function Callback_companyName(DataGridTextColumn $column, Company $company, Cashback $cashback)
    {
        return $company->getCompanyName();
    }

    /**
     * @param DataGridTextColumn $column
     * @param string $value
     * @param Cashback $cashback
     * @return mixed
     */
    public function Callback_status(DataGridTextColumn $column, $value, Cashback $cashback)
    {
        return $cashback->getStatus();
    }

    /**
     * @param DataGridTextColumn $column
     * @param string $value
     * @param Cashback $cashback
     * @return mixed
     */
    public function Callback_bankType(DataGridTextColumn $column, $value, Cashback $cashback)
    {
        return $cashback->getBankType();
    }

    protected  function init()
    {
        $this->disableOrder();
        $this->disablePaginator();

        $this->addTextColumn('company', 'Company Name', 300)
            ->addCallback(array($this, 'Callback_companyName'));
        $this->addTextColumn('cashbackId', 'Status', 120)
            ->addCallback(array($this, 'Callback_status'));
        $this->addTextColumn('bankTypeId', 'Bank', 120)
            ->addCallback(array($this, 'Callback_bankType'));
        $this->addDateColumn('datePaid', 'Date Paid', 120, 'd/m/Y');
        $this->addNumericColumn('amount', 'Amount', 120);
    }
}
