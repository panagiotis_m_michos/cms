<?php

namespace Front\Register;

use DataGrid;
use CURegisterMemberControler;
use Entities\Company as CompanyEntity;
use FRouter;

class MembersDataGrid extends DataGrid
{
    /**
     * @var CompanyEntity
     */
    private $company;

    /**
     * @var FRouter
     */
    private $router;

    /**
     * @param CompanyEntity $company
     */
    public function setCompany(CompanyEntity $company)
    {
        $this->company = $company;
    }

    /**
     * @param FRouter $router
     */
    public function setRouter(FRouter $router)
    {
        $this->router = $router;
    }

    protected function init()
    {
        $this->disableOrder();
        $this->disablePaginator();

        $this->addDateColumn('dateEntry', 'Date of entry', 70, 'd/m/Y');
        $this->addDateColumn('dateCeased', 'Date ceased', 70, 'd/m/Y');
        $this->addTextColumn('name', 'Name', 460);

        $action = $this->addActionColumn('actions', 'Register', 100);

        $url = $this->router->link(CURegisterMemberControler::PAGE_ADD, "company_id={$this->company->getId()}");
        $action->addGlobalAction('create', 'Add new member', $url);

        $url = $this->router->link(CURegisterMemberControler::PAGE_VIEW, "company_id={$this->company->getId()}");
        $action->addAction('view', 'View Register', $url);
    }
    
}
