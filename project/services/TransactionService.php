<?php

namespace Services;

use Entities\Customer;
use Entities\Order;
use Entities\Transaction;
use Nette\Object;
use Repositories\TransactionRepository;
use Services\Payment\PaymentResponse;
use Services\Payment\SageService;

class TransactionService extends Object
{
    /**
     * @var TransactionRepository
     */
    private $repository;

    /**
     * @param TransactionRepository $repository
     */
    public function __construct(TransactionRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Transaction $transaction
     */
    public function save(Transaction $transaction)
    {
        $this->repository->saveEntity($transaction);
    }

    /**
     * @param Customer $customer
     * @return array
     */
    public function getTransactions(Customer $customer)
    {
        return $this->repository->getTransactions($customer);
    }

    /**
     * @param Customer $customer
     * @param string $error
     */
    public function saveFailedSageTransaction(Customer $customer, $error)
    {
        $transaction = new Transaction($customer, Transaction::TYPE_SAGEPAY);
        $transaction->setStatusId(Transaction::STATUS_FAILED);
        $transaction->setError($error);
        $this->save($transaction);
    }

    /**
     * @param Customer $customer
     * @param PaymentResponse $paymentResponse
     * @param Order $order
     * @return Transaction
     */
    public function saveTransactionFromResponse(Customer $customer, PaymentResponse $paymentResponse, Order $order = NULL)
    {
        $transaction = new Transaction($customer, Transaction::TYPE_SAGEPAY);
        $transaction->setOrderCode($paymentResponse->getSageVpsTxId());
        $transaction->setCardHolder($paymentResponse->getCardHolder());
        $transaction->setDetails(serialize($paymentResponse->getSageDetails()));
        $transaction->setCardNumber($paymentResponse->getCardNumber());
        $transaction->setVendorTXCode($paymentResponse->getSageVendorTxCode());
        if ($paymentResponse->getToken()) {
            $transaction->setToken($paymentResponse->getToken());
        }
        if ($order) {
            $transaction->setOrder($order);
        }
        $this->repository->saveEntity($transaction);
        return $transaction;
    }
}
