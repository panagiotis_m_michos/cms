<?php

namespace Services;

use Company;
use CompanyIncorporation;
use CompanyModule\Entities\CompanySetting;
use CompanyModule\Repositories\CompanySettingsRepository;
use CompanySummary;
use Dispatcher\Events\CompanyDeletedEvent;
use Doctrine\ORM\NoResultException;
use Entities\Company as CompanyEntity;
use Entities\CompanyHouse\FormSubmission;
use Entities\Customer as CustomerEntity;
use Entities\Customer;
use Entities\EntityAbstract;
use Entities\Order;
use EventLocator;
use Exception;
use Exceptions\Business\CompanyException;
use Factories\Front\CompanyViewFactory;
use Feature;
use Identification;
use IncorporationCorporateDirector;
use IncorporationPersonDirector;
use Nette\Object;
use NomineeDirector;
use Repositories\CompanyRepository;
use Symfony\Component\EventDispatcher\EventDispatcher;

class CompanyService extends Object
{
    /**
     * @var CompanyRepository
     */
    private $repository;

    /**
     * @var CompanyViewFactory
     */
    private $companyViewFactory;

    /**
     * @var EventDispatcher
     */
    private $eventDispatcher;

    /**
     * @var CompanySettingsRepository
     */
    private $companySettingsRepository;

    /**
     * @param CompanyRepository $repository
     * @param CompanyViewFactory $companyViewFactory
     * @param EventDispatcher $eventDispatcher
     * @param CompanySettingsRepository $companySettings
     */
    public function __construct(
        CompanyRepository $repository,
        CompanyViewFactory $companyViewFactory,
        EventDispatcher $eventDispatcher,
        CompanySettingsRepository $companySettings
    )
    {
        $this->repository = $repository;
        $this->companyViewFactory = $companyViewFactory;
        $this->eventDispatcher = $eventDispatcher;
        $this->companySettingsRepository = $companySettings;
    }

    /**
     * @param EntityAbstract|EntityAbstract[]|NULL $entity
     */
    public function flush($entity = NULL)
    {
        $this->repository->flush($entity);
    }

    /**
     * @param CompanyEntity $company
     */
    public function saveCompany(CompanyEntity $company)
    {
        $this->repository->saveEntity($company);
    }

    /**
     * @param int $id
     * @return CompanyEntity
     */
    public function getCompanyById($id)
    {
        return $this->repository->find($id);
    }

    /**
     * @param string $companyNumber
     * @return CompanyEntity
     */
    public function getCompanyByCompanyNumber($companyNumber)
    {
        return $this->repository->findOneBy(['companyNumber' => $companyNumber]);
    }

    /**
     * @param string $name
     * @return CompanyEntity|null
     */
    public function getCompanyByCompanyName($name)
    {
        return $this->repository->findOneBy(['companyName' => $name]);
    }

    /**
     * @param CustomerEntity $customer
     * @return array CompanyEntity[]
     */
    public function getCustomerCompanies(CustomerEntity $customer)
    {
        return $this->repository->findBy(['customer' => $customer]);
    }

    /**
     * @param CustomerEntity $customer
     * @return array CompanyView[]
     */
    public function getCustomerCompaniesViews(CustomerEntity $customer)
    {
        $companies = $this->getCustomerCompanies($customer);
        $companyViews = [];
        foreach ($companies as $company) {
            $companyViews[] = $this->companyViewFactory->create($company);
        }
        return $companyViews;
    }

    /**
     * @param CustomerEntity $customer
     * @return array CompanyView[]
     */
    public function getCustomerServiceCompaniesViews(CustomerEntity $customer)
    {
        $companies = $this->repository->getCustomerServiceCompanies($customer);
        $companyViews = [];
        foreach ($companies as $company) {
            $companyViews[] = $this->companyViewFactory->create($company);
        }
        return $companyViews;
    }

    /**
     * @param CustomerEntity $customer
     * @return CompanyEntity[]
     */
    public function getUnsubmittedCompanies(CustomerEntity $customer)
    {
        return $this->repository->getUnsubmittedCompanies($customer);
    }

    /**
     * @return CompanyEntity[]
     */
    public function getCompaniesWithActiveServices()
    {
        return $this->repository->getCompaniesWithActiveServices();
    }

    /**
     * @param CustomerEntity $customer
     * @param int $id
     * @throws CompanyException
     * @return CompanyEntity
     */
    public function getCustomerCompany(CustomerEntity $customer, $id)
    {
        $company = $this->getCompanyById($id);
        if (!$company->getCustomer()->isEqual($customer)) {
            throw CompanyException::companyDoesNotBelongToCustomer($company, $customer);
        }
        return $company;
    }

    /**
     * @param CustomerEntity $customer
     * @param string $companyNumber
     * @throws CompanyException
     * @return CompanyEntity
     */
    public function getCustomerCompanyByCompanyNumber(CustomerEntity $customer, $companyNumber)
    {
        $company = $this->getCompanyByCompanyNumber($companyNumber);
        if (!$company->getCustomer()->isEqual($customer)) {
            throw CompanyException::companyDoesNotBelongToCustomer($company, $customer);
        }
        return $company;
    }

    /**
     * @param CompanyEntity $company
     * @param CustomerEntity $customer
     * @return bool
     */
    public function doesCompanyBelongToCustomer(CompanyEntity $company, CustomerEntity $customer)
    {
        return $company->getCustomer()->isEqual($customer);
    }

    /**
     * @param Order $order
     * @return CompanyEntity|NULL
     */
    public function getCompanyByOrder(Order $order)
    {
        return $this->repository->findOneBy(['order' => $order]);
    }

    /**
     * @param CompanyEntity $company
     */
    public function deleteCompany(CompanyEntity $company)
    {
        // @todo: change to use company repository
        $oldCompany = Company::getCompany($company->getId());
        $oldCompany->delete();

        $this->eventDispatcher->dispatch(EventLocator::COMPANY_DELETED, new CompanyDeletedEvent($company));
    }

    /**
     * @param CustomerEntity $customer
     * @param bool $excludeDeleted
     * @return array
     */
    public function getCustomerIncorporatedCompanies(Customer $customer,  $excludeDeleted = FALSE)
    {
        return $this->repository->getCustomerIncorporatedCompanies($customer, $excludeDeleted);
    }

    /**
     * Provide saving nominee director to customer company
     *
     * @param Company $company
     * @param NomineeDirector $director
     * @throws Exception
     */
    public function addNomineeDirectorToCompany(Company $company, NomineeDirector $director)
    {
        /** @var CompanyIncorporation $companyIncorporation */
        $companyIncorporation = $company->getLastIncorporationFormSubmission()->getForm();

        if ($director->isOk2show() == FALSE) {
            throw new Exception("{$director->getLngTitle()} can not be added!");
        } elseif ($companyIncorporation->hasNomineeDirector() == FALSE) {

            /************************ person ******************************/
            /** @var IncorporationPersonDirector $directorPerson */
            $directorPerson = $companyIncorporation->getNewIncPerson('dir');
            $directorPerson->setNominee(TRUE);

            $address = $directorPerson->getAddress();
            $address->setPremise($director->personAddress1);
            $address->setStreet($director->personAddress2);
            $address->setThoroughfare($director->personAddress3);
            $address->setPostTown($director->personTown);
            $address->setCounty($director->personCounty);
            $address->setPostcode($director->personPostcode);
            $address->setCountry($director->personCountryId);
            $directorPerson->setAddress($address);

            $person = $directorPerson->getPerson();
            $person->setForename($director->personFirstname);
            $person->setSurname($director->personSurname);
            $person->setNationality($director->personNationality);
            $person->setOccupation($director->personOccupation);
            $person->setCountryOfResidence($director->personCountryOfResidence);
            $person->setDob($director->personDob);

            $directorPerson->setResidentialAddress($address);
            $directorPerson->setPerson($person);
            $directorPerson->setConsentToAct(TRUE);
            $directorPerson->setNomineeType($director->getNomineeType());

            $directorPerson->save();


            /************************ corporate ******************************/
            if ($director->isCorporate()) {
                /** @var IncorporationCorporateDirector $directorCorporate */
                $directorCorporate = $companyIncorporation->getNewIncCorporate(
                    'dir'
                );
                $directorCorporate->setNominee(TRUE);

                $corporate = $directorCorporate->getCorporate();
                $corporate->setCorporateName($director->corporateCompanyName);
                $corporate->setForename($director->corporateFirstName);
                $corporate->setSurname($director->corporateLastName);
                $directorCorporate->setCorporate($corporate);

                $address = $directorCorporate->getAddress();
                $address->setPremise($director->corporateAddress1);
                $address->setStreet($director->corporateAddress2);
                $address->setThoroughfare($director->corporateAddress3);
                $address->setPostTown($director->corporateTown);
                $address->setCounty($director->corporateCounty);
                $address->setPostcode($director->corporatePostcode);
                $address->setCountry($director->corporateCountryId);
                $directorCorporate->setAddress($address);

                $directorCorporate->setConsentToAct(TRUE);

                $identification = $directorCorporate->getIdentification(Identification::EEA);
                $identification->setPlaceRegistered($director->countryRegistered);
                $identification->setRegistrationNumber($director->registrationNumber);
                $directorCorporate->setIdentification($identification);
                $directorCorporate->setNomineeType($director->getNomineeType());

                $directorCorporate->save();
            }
        }
    }

    /**
     * @param CompanyEntity $company
     * @return CompanySummary
     */
    public function getCompanySummaryByCompany(CompanyEntity $company)
    {
        $companyId = $company->getCompanyId();
        $oldCompany = Company::getCompany($companyId);
        $data = $oldCompany->getData();
        return CompanySummary::getCompanySummary($data['company_details'], $data['registered_office'], $data['sail_address'], $data['directors'], $data['secretaries'], $data['shareholders'], $data['capitals']);
    }

    /**
     * @param CompanyEntity $company
     * @return FormSubmission
     */
    public function getLastCompanyIncorporationSubmission(CompanyEntity $company)
    {
        return $this->repository->getLastCompanyIncorporationSubmission($company);
    }

    /**
     * @param CompanyEntity $company
     * @return bool
     */
    public function isCompanyIncorporationPending(CompanyEntity $company)
    {
        $formSubmission = $this->getLastCompanyIncorporationSubmission($company);

        return $formSubmission && $formSubmission->isPending();
    }

    /**
     * @param CompanyEntity $company
     */
    public function refresh(CompanyEntity $company)
    {
        $this->repository->refresh($company);
    }

    /**
     * @param CustomerEntity $customer
     * @param int $id
     * @throws NoResultException
     * @return CompanyEntity
     */
    public function getCustomerCompanyById(CustomerEntity $customer, $id)
    {
        $company = $this->repository->getCustomerCompany($customer, $id);
        return $company;
    }

    /**
     * @param CompanyEntity $company
     * @return CompanySetting
     */
    public function dismissBankOffer(CompanyEntity $company)
    {
        $setting = CompanySetting::doNotShowBankOffer($company);
        $this->companySettingsRepository->saveEntity($setting);
        return $setting;
    }
}
