<?php

namespace Services;

use Repositories\CustomerNoteRepository;
use Entities\CustomerNote;
use Entities\Customer;

class CustomerNoteService extends \Object
{
    /**
     * @var CustomerNoteRepository
     */
    private $repository;

    /**
     * @param \Repositories\CustomerNoteRepository $repository
     */
    public function __construct(CustomerNoteRepository $repository)
    {
        $this->repository = $repository;
    }
    
    /**
     * @param \Entities\CustomerNote $entity
     */
    public function save(CustomerNote $entity)
    {
        $this->repository->saveEntity($entity);
    }

    /**
     * @param \Entities\Customer $customer
     * @return \Entities\CustomerNote
     */
    public function getCustomerNotes(Customer $customer)
    {
        return $this->repository->findBy(array('customer' => $customer), array('dtc' => 'DESC'));
    }
}