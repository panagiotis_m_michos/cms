<?php

namespace Services;

use FUser;
use Repositories\UserRepository;
use Entities\User;

class UserService
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }
    
    /**
     * @param User $entity
     */
    public function save(User $entity)
    {
        $this->repository->saveEntity($entity);
    }
    
    /**
     * @param int $id
     * @return User
     */
    public function getUserById($id)
    {
        return $this->repository->find($id);
    }

    /**
     * @return User|null
     */
    public function getLoggedInUser()
    {
        return $this->repository->find(FUser::getSignedIn()->getId());
    }
}
