<?php

class EmailLogService
{
    /**
     * @TODO we need to use it with both Customer and Entities\Customer (used IIdentifier for now) -> refactor
     * @param FEmail $email
     * @param IIdentifier $customer
     * @return EmailLog
     */
    public function logEmail(FEmail $email, IIdentifier $customer = NULL)
    {
        $emailLog = new EmailLog();
        $emailLog->setEmailName($email->page->title);
        $emailLog->setEmailNodeId($email->getId());
        $emailLog->setEmailFrom($email->getFromArr());
        if ($customer) {
            $emailLog->setCustomerId($customer->getId());
        }
        $emailsTo = array_merge($email->getToArr(), $email->getCcArr(), $email->getBccArr());
        $emailLog->setEmailTo($emailsTo);
        $emailLog->setEmailSubject($email->subject);
        $emailLog->setEmailBody($email->body);
        $emailLog->setAttachment(!empty($email->attachments) ? 1 : 0);
        $emailLog->save();
        return $emailLog;
    }
}
