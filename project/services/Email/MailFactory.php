<?php

class MailFactory implements IMailFactory
{
    /**
     * @var IMailer
     */
    private $mailer;

    /**
     * @param IMailer $mailer
     */
    public function __construct(IMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @return Mail
     */
    public function create()
    {
        $mail = new Mail();
        $mail->setMailer($this->mailer);

        return $mail;
    }
}
