<?php

interface IMailFactory
{

    /**
     * @return Mail
     */
    public function create();
}
