<?php

use Exceptions\Technical\EmailException;

class EmailService implements IEmailService
{

    /**
     * @var EmailLogService
     **/
    private $emailLogService;

    /**
     * @var IMailFactory
     */
    private $mailFactory;

    /**
     * @param EmailLogService $emailLogService
     * @param IMailFactory $mailFactory
     */
    public function __construct(EmailLogService $emailLogService, IMailFactory $mailFactory)
    {
        $this->emailLogService = $emailLogService;
        $this->mailFactory = $mailFactory;
    }

    /**
     * @TODO we need to use it with both Customer and Entities\Customer refactor
     * @param FEmail $email
     * @param Customer|Entities\Customer $customer
     * @param bool $isHtml
     */
    public function send(FEmail $email, $customer = NULL, $isHtml = TRUE)
    {
        try {
            if ($email->fromName) {
                $fromName = $email->fromName;
            } else {
                $fromName = (isset(FApplication::$config['emails']['from_name']) ? FApplication::$config['emails']['from_name'] : NULL);
            }

            $mail = $this->mailFactory->create();
            $mail->setFrom($email->from, $fromName);

            foreach ($email->getToArr() as $key => $val) {
                $mail->addTo(trim($val));
            }

            foreach ($email->getCcArr() as $key => $val) {
                $mail->addCc(trim($val));
            }

            foreach ($email->getBccArr() as $key => $val) {
                $mail->addBcc(trim($val));
            }

            foreach ($email->getAttachments() as $key => $val) {
                $mail->addAttachment($val['file'], $val['content'], $val['contentType']);
            }

            foreach ($email->getCmsFileAttachments() as $key => $val) {
                try {
                    $file = new FFile($val);
                    $filePath = $file->getFilePath();
                    $mail->addAttachment($filePath);
                } catch (Exception $e) {
                    Debug::log($e);
                }
            }

            $mail->setSubject($email->subject);
            if ($isHtml) {
                $mail->setHtmlBody($email->body);
            } else {
                $mail->setBody($email->body);
            }

            $mail->send();
            $this->emailLogService->logEmail($email, $customer);
        } catch(Exception $e) {
            throw new EmailException($e->getMessage(), 0, $e);
        }
    }
}
