<?php

class EmailerFactory
{
    const BUSINESS_BANKING = 'businessBanking';
    const COSEC = 'cosec';
    const COMPANY = 'company';
    const ANNUAL_RETURN = 'annualReturn';
    const ORDER = 'order';
    const USER = 'user';
    const PAYMENT = 'payment';
    const GENERAL_WHOLESALE = 'generalWholesale';
    const TAX_ASSIST_WHOLESALE = 'taxAssistWholesale';
    const TAX_ASSIST_ACCOUNTS = 'taxAssistAccounts';
    const ICAEW_WHOLESALE = 'icaewWholesaleEmailer';
    const WHOLESALE = 'wholesale';
    const ACCOUNTS_INSTITUTE = 'wholesale';
    const AFFILIATE = 'affiliate';
    const WESTBURY = 'westbury';
    const GOOGLE = 'google';
    const JOURNEY = 'journey';
    const CRON_JOB = 'cronJob';
    const CASH_BACK = 'cashBack';
    const CONTACT_US = 'contactUs';
    const COMPANY_NAME = 'companyName';
    const ARD = 'ard';
    const EREMINDER = 'ereminder';
    const ERROR_TESTING = 'errorTesting';
    const REGUS_OFFICE = 'regusOffice';

    /**
     * @var IEmailService
     */
    private $emailService;

    /**
     * @param IEmailService $emailService
     */
    public function __construct(IEmailService $emailService)
    {
        $this->emailService = $emailService;
    }

    /**
     * @param string $emailer
     * @return EmailerAbstract
     */
    public function get($emailer)
    {
        switch ($emailer) {
            case self::BUSINESS_BANKING: return new BusinessBankingEmailer($this->emailService);
            case self::COSEC: return new CosecEmailer($this->emailService);
            case self::COMPANY: return new CompanyEmailer($this->emailService);
            case self::ANNUAL_RETURN: return new AnnualReturnsEmailer($this->emailService);
            case self::ORDER: return new OrderEmailer($this->emailService);
            case self::USER: return new UserEmailer($this->emailService);
            case self::TAX_ASSIST_ACCOUNTS: return new TaxAssistAccountsEmailer($this->emailService);
            case self::ACCOUNTS_INSTITUTE: return new AccountantsInstituteEmailer($this->emailService);
            case self::WHOLESALE: return new WholesaleEmailer($this->emailService);
            case self::AFFILIATE: return new AffiliateEmailer($this->emailService);
            case self::WESTBURY: return new WestburyEmailer($this->emailService);
            case self::GOOGLE: return new GoogleEmailer($this->emailService);
            case self::JOURNEY: return new JourneyEmailer($this->emailService);
            case self::CRON_JOB: return new CronJobEmailer($this->emailService);
            case self::CASH_BACK: return new CashBackEmailer($this->emailService);
            case self::CONTACT_US: return new ContactUsEmailer($this->emailService);
            case self::COMPANY_NAME: return new CompanyNameEmailer($this->emailService);
            case self::ARD: return new ArdEmailer($this->emailService);
            case self::EREMINDER: return new EreminderEmailer($this->emailService);
            case self::ERROR_TESTING: return new ErrorTestingEmailer($this->emailService);
            case self::REGUS_OFFICE: return new RegusEmailer($this->emailService);
            default: throw new InvalidArgumentException("Emailer $emailer not found!");
        }
    }
}
