<?php

class FEmailFactory
{
    /**
     * @param int $id
     * @return FEmail
     */
    public function getById($id)
    {
        return new FEmail($id);
    }
}
