<?php

interface IEmailService
{

    /**
     * @param FEmail $email
     * @param Customer|Entities\Customer $customer
     */
    public function send(FEmail $email, $customer = NULL);
}
