<?php

class ContainerBuilder {

    const EMAILER = 'emailer';

    public function get($class) {
        switch ($class) {
        case self::EMAILER: return new EmailerFactory(new EmailService(new EmailLogService()));
        default: throw new InvalidArgumentException("Container $class not found!");
        }
    }
}
