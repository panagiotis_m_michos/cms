<?php

namespace Services;

use Repositories\CapitalRepository;
use Repositories\CapitalShareRepository;
use Repositories\ShareholdingRepository;
use Entities\CapitalShare;
use Entities\Capital;
use Exceptions\Business\ItemExists as ItemExistsException;
use Exceptions\Business\BusinessLogic as BusinessLogicException;

class AnnualReturnService extends \Object
{

    /**
     * @var CapitalShareRepository
     */
    protected $capitalShareRepository;

    /**
     * @var CapitalRepository
     */
    protected $capitalRepository;

    /**
     * @var ShareholdingRepository
     */
    protected $shareholdingRepository;

    /**
     * @param CapitalRepository $capitalRepository
     * @param CapitalShareRepository $capitalShareRepository
     * @param ShareholdingRepository $shareholdingRepository
     */
    public function __construct(CapitalRepository $capitalRepository, CapitalShareRepository $capitalShareRepository, ShareholdingRepository $shareholdingRepository)
    {
        $this->capitalShareRepository = $capitalShareRepository;
        $this->capitalRepository = $capitalRepository;
        $this->shareholdingRepository = $shareholdingRepository;
    }

    //-----------------------------CAPITAL----------------------------------------//
    /**
     * @param Capital $capital
     */
    public function saveCapital(Capital $capital)
    {
        $this->validateCapital($capital);
        foreach ($capital->getShares() as $share) {
            $this->saveCapitalShare($share);
        }
        //unset shareholding if capital share change
        $this->unsetShareholdingShareClasses($share);
    }

    /**
     * @param Capital $capital
     */
    public function removeCapital(Capital $capital)
    {

        foreach ($capital->getShares() as $share) {
            $this->removeCapitalShare($share);
        }
    }

    /**
     * @param string $id
     * @return Capital
     */
    public function findCapital($id)
    {
        return $this->capitalRepository->findById($id);
    }

    /**
     * @param int $formSubmissionId
     * @return array
     */
    public function getAllCapitals($formSubmissionId)
    {
        return $this->capitalRepository->getAllCapitals($formSubmissionId);
    }

    //-----------------------CAPITAL SHARE----------------------------------------//
    /**
     * @param CapitalShare $capitalShare
     */
    public function saveCapitalShare(CapitalShare $capitalShare)
    {
        $this->validateCapitalShare($capitalShare);
        $this->calculatePaid($capitalShare);
        $this->capitalShareRepository->save($capitalShare);
    }

    /**
     * @param CapitalShare $capitalShare
     */
    public function removeCapitalShare(CapitalShare $capitalShare)
    {
        $this->capitalShareRepository->delete($capitalShare);

        //unset shareholding if capital share change
        $this->unsetShareholdingShareClasses($capitalShare);
    }

    /**
     * @param int $id
     * @return CapitalShare
     */
    public function findCapitalShare($id)
    {
        return $this->capitalShareRepository->findById($id);
    }

    /**
     * @param string $id = $shareCurrency_$formSubmissionId
     * @return array
     */
    public function getAllCapitalShares($id)
    {
        return $this->capitalShareRepository->getAllCapitalShares($id);
    }

    //------------------------SHAREHOLDING----------------------------------------//
    /**
     * @param int $id
     * @return Shareholding
     */
    public function findShareholding($id)
    {
        return $this->shareholdingRepository->findById($id);
    }

    /**
     * @param Shareholding $shareholding
     * @return int
     */
    public function saveShareholding(Shareholding $shareholding)
    {
        return $this->shareholdingRepository->save($shareholding);
    }

    /**
     * @param int $formSubmissionId
     * @return array
     */
    public function getAllShareholdings($formSubmissionId)
    {
        return $this->shareholdingRepository->getAllShareholdings($formSubmissionId);
    }

    /**
     * @param CapitalShare $capitalShare
     */
    public function unsetShareholdingShareClasses(CapitalShare $capitalShare)
    {
        $formSubmissionId = $capitalShare->getCapital()->getFormSubmissionId();
        $shares = $this->capitalShareRepository->getAllShares($formSubmissionId);
        $shareholdings = $this->getAllShareholdings($formSubmissionId);

        $shareClass = array();
        foreach ($shares as $share) {
            $shareClass[] = $share->getShareClass();
        }
        foreach ($shareholdings as $shareholding) {
            if (!in_array($shareholding->getShareClass(), $shareClass)) {
                $shareholding->setShareClass(NULL);
                $this->shareholdingRepository->save($shareholding);
            }
        }
    }

    //--------------------------VALIDATORS----------------------------------------//
    /**
     * todo need to be remove to helper or validator class
     * @param CapitalShare $capitalShare
     * @throws Exception
     */
    public function validateCapitalShare(CapitalShare $capitalShare)
    {
        //$shares = $this->getAllCapitalShares($capitalShare->getCapital()->getId());
        // Share "share_class" must unique for all capitals
        $capitals = $this->getAllCapitals($capitalShare->getCapital()->getFormSubmissionId());
        if ($this->capitalShareRepository->findSameShareClassShares($capitals, $capitalShare)) {
            throw new ItemExistsException('Share Class: ' . $capitalShare->getShareClass() . ' already exist.');
        }
        if ($capitalShare->getPaid() == CapitalShare::STATUS_PARTIALY_PAID) {
            /* paid */
            if ($capitalShare->getSyncPaid() == CapitalShare::STATUS_PAID) {
                throw new BusinessLogicException('Your shares need to be fully paid');
            }
            /* partialy paid */
            if ($capitalShare->getSyncPaid() != CapitalShare::STATUS_UNPAID && $capitalShare->getSyncPaid() != CapitalShare::STATUS_PAID) {
                /* cannot have less paid than before */
                if ($capitalShare->getPartialyPaid() < $capitalShare->getSyncPaid()) {
                    throw new BusinessLogicException('The amount partially paid per share cannot be less than the original amount (' . $capitalShare->getSyncPaid() . ' ' . $capitalShare->getCapital()->getShareCurrency() . ')');
                }
            }
            if ($capitalShare->getPartialyPaid() > ($capitalShare->getShareValue())) {
                throw new BusinessLogicException('Share: ' . $capitalShare->getShareClass() . ' Amount paid per share cannot be more than the share value.');
            }
        } else {
            if ($capitalShare->getPaid() == CapitalShare::STATUS_UNPAID && $capitalShare->getSyncPaid() == CapitalShare::STATUS_PAID) {
                throw new BusinessLogicException('You cannot \'unpaid shares\'');
            }
        }
    }

    /**
     * todo need to be remove to helper or validator class
     * @param Capital $capital
     * @throws Exception
     */
    public function validateCapital(Capital $capital)
    {
        $capitals = $this->capitalRepository->getAllCapitals($capital->getFormSubmissionId());
        if ($this->capitalRepository->findSameCurrencyCapitals($capitals, $capital)) {
            throw new ItemExistsException('Capital with provided currency already exist.');
        }
    }

    /**
     * business logic which calculate capital share
     * generate amountPaidDuePerShare, amountUnpaidPerShare
     * @param CapitalShare $capitalShare
     */
    public function calculatePaid(CapitalShare $capitalShare)
    {
        if ($capitalShare->getPaid() == CapitalShare::STATUS_PARTIALY_PAID) {
            /* the rest */
            $capitalShare->setAmountUnpaidPerShare($capitalShare->getShareValue() - $capitalShare->getPartialyPaid());
            /* part paid */
            $capitalShare->setAmountPaidDuePerShare($capitalShare->getPartialyPaid());
            $capitalShare->setPaid($capitalShare->getPartialyPaid());
        } else {
            if ($capitalShare->getPaid() == CapitalShare::STATUS_PAID) {
                $capitalShare->setAmountPaidDuePerShare($capitalShare->getShareValue());
                $capitalShare->setAmountUnpaidPerShare(0);
                $capitalShare->setPaid(CapitalShare::STATUS_PAID);
            } else {
                $capitalShare->setAmountUnpaidPerShare($capitalShare->getShareValue());
                $capitalShare->setAmountPaidDuePerShare(0);
                $capitalShare->setPaid(CapitalShare::STATUS_UNPAID);
            }
        }
        $capitalShare->setAggregateNominalValue($capitalShare->getNumShares() * $capitalShare->getShareValue());
    }

}
