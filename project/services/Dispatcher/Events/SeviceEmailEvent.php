<?php

namespace Dispatcher\Events;

use Symfony\Component\EventDispatcher\Event;
use FEmail;
use Models\View\Front\CompanyServicesEmailView;

class ServicesEmailEvent extends Event
{
    /**
     * @var FEmail 
     */
    protected $email;
    
    /**
     * @var CompanyServicesEmailView 
     */
    protected $emailView;
    
    /**
     * 
     * @param FEmail $email
     * @param CompanyServicesEmailView $emailView
     */
    public function __construct(FEmail $email, CompanyServicesEmailView $emailView)
    {
        $this->email = $email;
        $this->emailView = $emailView;
    }
    
    /**
     * @return FEmail
     */
    public function getEmail()
    {
        return $this->email;
    }
    
    /**
     * @return CompanyServicesEmailView
     */
    public function getEmailView()
    {
        return $this->emailView;
    }
}
