<?php

namespace Dispatcher\Events;

use Entities\Company;
use Entities\Payment\Token;
use Product;
use Symfony\Component\EventDispatcher\Event;
use Entities\Service;

class ServicePurchasedEvent extends Event
{
    /**
     * @var Service 
     */
    protected $service;

    /**
     * @var Token
     */
    private $renewalToken;

    /**
     * @param Service $service
     * @param Token $renewalToken
     */
    public function __construct(Service $service, Token $renewalToken = NULL)
    {
        $this->service = $service;
        $this->renewalToken = $renewalToken;
    }
    
    /**
     * @return Service
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->service->getCompany();
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->service->getProduct();
    }

    /**
     * @return bool
     */
    public function isCompanyIncorporated()
    {
        return $this->getCompany()->isIncorporated();
    }

    /**
     * @return Token
     */
    public function getRenewalToken()
    {
        return $this->renewalToken;
    }
}
