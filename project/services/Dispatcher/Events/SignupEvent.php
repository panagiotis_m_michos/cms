<?php

namespace Dispatcher\Events;

use Entities\Customer;
use Symfony\Component\EventDispatcher\Event;
use Wholesale\IEmailer;

class SignUpEvent extends Event
{
    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var string
     */
    private $password;
    /**
     * @var IEmailer
     */
    private $emailer;

    /**
     * @param IEmailer $emailer
     * @param $customer
     * @param $password
     */
    public function __construct(IEmailer $emailer, $customer, $password)
    {
        $this->customer = $customer;
        $this->password = $password;
        $this->emailer = $emailer;
    }

    /**
     * @return IEmailer
     */
    public function getEmailer()
    {
        return $this->emailer;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
}