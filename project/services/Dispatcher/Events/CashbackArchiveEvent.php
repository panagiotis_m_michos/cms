<?php

namespace Dispatcher\Events;

use Entities\Cashback;
use Entities\Customer;
use Symfony\Component\EventDispatcher\Event;

class CashbackArchiveEvent extends Event
{
    /**
     * @var Cashback[]
     */
    private $cashbacks;

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @param Cashback[] $cashbacks
     * @param Customer $customer
     */
    public function __construct(array $cashbacks, Customer $customer)
    {
        $this->cashbacks = $cashbacks;
        $this->customer = $customer;
    }

    /**
     * @return Cashback[]
     */
    public function getCashbacks()
    {
        return $this->cashbacks;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }
}
