<?php

namespace Dispatcher\Events;

use FEmail;
use Models\View\Front\CompanyServicesEmailView;
use Models\View\Front\CompanyServicesView;
use Nette\Object;

class ServiceEmailEventFactory extends Object
{

    /**
     * @param FEmail $email
     * @param CompanyServicesView $companyServicesView
     * @param array $serviceViews
     * @param string $type
     * @return ServicesEmailEvent
     */
    public function create(FEmail $email, CompanyServicesView $companyServicesView, array $serviceViews, $type)
    {
        return new ServicesEmailEvent(
            $email,
            new CompanyServicesEmailView($email, $companyServicesView, $serviceViews, $type)
        );
    }
}
