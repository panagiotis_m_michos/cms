<?php

namespace Dispatcher\Events;

use Entities\Payment\Token;
use Symfony\Component\EventDispatcher\Event;

class TokenRemovedEvent extends Event
{

    /**
     * @var Token
     */
    protected $token;

    /**
     * @param Token $token
     */
    public function __construct(Token $token)
    {
        $this->token = $token;
    }

    /**
     * @return Token
     */
    public function getToken()
    {
        return $this->token;
    }
}
