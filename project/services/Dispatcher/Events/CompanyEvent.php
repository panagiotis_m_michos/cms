<?php

namespace Dispatcher\Events;

use Symfony\Component\EventDispatcher\Event;
use Entities\Company;

class CompanyEvent extends Event
{
    /**
     * @var Company 
     */
    protected $company;
    
    /**
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        $this->company = $company;
    }
    
    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return int
     */
    public function getCompanyProductId()
    {
        return $this->company->getProductId();
    }

    /**
     * @return array<Service>
     */
    public function getGroupedServices()
    {
        return $this->company->getGroupedServices();
    } 
}
