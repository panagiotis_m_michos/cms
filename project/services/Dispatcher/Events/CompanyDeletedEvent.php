<?php

namespace Dispatcher\Events;

use Entities\Company;
use Symfony\Component\EventDispatcher\Event;

class CompanyDeletedEvent extends Event
{

    /**
     * @var Company
     */
    private $company;

    /**
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }
}
