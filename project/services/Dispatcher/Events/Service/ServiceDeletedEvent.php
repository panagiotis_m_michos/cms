<?php

namespace Dispatcher\Events\Service;

use Entities\Service;
use Symfony\Component\EventDispatcher\Event;

class ServiceDeletedEvent extends Event
{
    /**
     * @var Service
     */
    private $service;

    /**
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * @return Service
     */
    public function getService()
    {
        return $this->service;
    }
}