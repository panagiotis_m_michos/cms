<?php

namespace Dispatcher\Events\Basket;

use BasketProduct;
use Symfony\Component\EventDispatcher\Event;

class ItemRemovedEvent extends Event
{
    /**
     * @var int
     */
    private $itemId;

    /**
     * @var BasketProduct
     */
    private $item;

    /**
     * @param int $itemId
     * @param BasketProduct $item
     */
    public function __construct($itemId, BasketProduct $item)
    {
        $this->itemId = $itemId;
        $this->item = $item;
    }

    /**
     * @return int
     */
    public function getItemId()
    {
        return $this->itemId;
    }

    /**
     * @return BasketProduct
     */
    public function getItem()
    {
        return $this->item;
    }
}
