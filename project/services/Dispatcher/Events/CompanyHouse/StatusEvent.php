<?php

namespace Dispatcher\Events\CompanyHouse;

use Symfony\Component\EventDispatcher\Event;
use Entities\CompanyHouse\FormSubmission;
use CompanyHouse\Filling\Responses\SubmissionStatus;

class StatusEvent extends Event
{
    /**
     * @var FormSubmission 
     */
    protected $formSubmission;

    /**
     * @var SubmissionStatus 
     */
    protected $submissionStatus;

    /**
     * @var array
     */
    protected $options = array();
    

    /**
     * @param FormSubmission $formSubmission
     * @param SubmissionStatus $submissionStatus
     */
    public function __construct(FormSubmission $formSubmission, SubmissionStatus $submissionStatus)
    {
        $this->formSubmission = $formSubmission;
        $this->submissionStatus = $submissionStatus;
    }
    
    /**
     * @return FormSubmission
     */
    public function getFormSubmission()
    {
        return $this->formSubmission;
    }

    /**
     * @return SubmissionStatus 
     */
    public function getSubmissionStatus()
    {
        return $this->submissionStatus;
    } 

    /**
     * @return bool
     */
    public function hasEqualStatus()
    {
        return $this->formSubmission->getResponse() === $this->submissionStatus->getStatus();
    }

    /**
     * @return int
     */
    public function getCompanyId()
    {
        return $this->formSubmission->getCompany()->getCompanyId();
    }

    /**
     * @return string
     */
    public function getFormSubmissionId()
    {
        return $this->submissionStatus->getSubmissionNumber();
    }

    /**
     * @return array
     */
    public function getResponseRejections()
    {
        return $this->submissionStatus->getRejections();
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    public function setOption($key, $value)
    {
        $this->options[$key] = $value;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }
}
