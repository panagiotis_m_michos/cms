<?php

namespace Dispatcher\Events\Order;

use Entities\Order;
use Entities\OrderItem;
use Symfony\Component\EventDispatcher\Event;

class RefundEvent extends Event
{
    /**
     * @var Order
     */
    private $order;

    /**
     * @var OrderItem[]
     */
    private $orderItems;

    /**
     * @var float
     */
    private $totalCreditRefund;

    /**
     * @var float
     */
    private $totalMoneyRefund;

    /**
     * @param Order $order
     * @param OrderItem[] $orderItems
     * @param float $totalCreditRefund
     * @param float $totalMoneyRefund
     */
    public function __construct(Order $order, array $orderItems, $totalCreditRefund, $totalMoneyRefund)
    {
        $this->order = $order;
        $this->orderItems = $orderItems;
        $this->totalCreditRefund = $totalCreditRefund;
        $this->totalMoneyRefund = $totalMoneyRefund;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @return OrderItem[]
     */
    public function getOrderItems()
    {
        return $this->orderItems;
    }

    /**
     * @return float
     */
    public function getTotalCreditRefund()
    {
        return $this->totalCreditRefund;
    }

    /**
     * @return float
     */
    public function getTotalMoneyRefund()
    {
        return $this->totalMoneyRefund;
    }
}