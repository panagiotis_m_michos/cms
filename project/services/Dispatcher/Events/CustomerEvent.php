<?php

namespace Dispatcher\Events;

use Entities\Customer;
use Symfony\Component\EventDispatcher\Event;

class CustomerEvent extends Event
{
    /**
     * @var Customer
     */
    private $customer;

    /**
     * @param Customer $customer
     */
    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }
}
