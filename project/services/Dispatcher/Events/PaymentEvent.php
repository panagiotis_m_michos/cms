<?php

namespace Dispatcher\Events;

use Basket;
use Entities\Company;
use Entities\Customer;
use Symfony\Component\EventDispatcher\Event;

class PaymentEvent extends Event
{
    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var Basket
     */
    private $basket;

    /**
     * @var Company[]
     */
    private $notImportedCompanies = array();

    /**
     * @param Customer $customer
     * @param Basket $basket
     */
    public function __construct(Customer $customer, Basket $basket)
    {
        $this->customer = $customer;
        $this->basket = $basket;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @return Basket
     */
    public function getBasket()
    {
        return $this->basket;
    }

    /**
     * @return Company[]
     */
    public function getNotImportedCompanies()
    {
        return $this->notImportedCompanies;
    }

    /**
     * @param Company[] $notImportedCompanies
     */
    public function setNotImportedCompanies(array $notImportedCompanies)
    {
        $this->notImportedCompanies = $notImportedCompanies;
    }

    /**
     * @param int $basketItemId
     * @param Company $notImportedCompany
     */
    public function addNotImportedCompany($basketItemId, Company $notImportedCompany)
    {
        $this->notImportedCompanies[$basketItemId] = $notImportedCompany;
    }

    /**
     * @return bool
     */
    public function isIdCheckRequiredToBeCompleted()
    {
        return !$this->customer->isIdCheckCompleted() && $this->basket->hasItemWithIdCheckRequired();
    }

    /**
     * @return bool
     */
    public function isIdCheckRequiredForCustomer()
    {
        return $this->customer->isIdCheckRequired();
    }

    /**
     * @return bool
     */
    public function isUkCustomer()
    {
        return $this->customer->isUkCustomer();
    }
}
