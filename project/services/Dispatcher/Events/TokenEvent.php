<?php

namespace Dispatcher\Events;

use ILogger;
use Symfony\Component\EventDispatcher\Event;
use Entities\Payment\Token;

class TokenEvent extends Event
{
    /**
     * @var Token 
     */
    protected $token;

    /**
     * @var ILogger
     */
    private $logger;

    /**
     * @param Token $token
     * @param ILogger $logger
     */
    public function __construct(Token $token, ILogger $logger = NULL)
    {
        $this->token = $token;
        $this->logger = $logger;
    }
    
    /**
     * @return Token
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return ILogger
     */
    public function getLogger()
    {
        return $this->logger;
    }
}
