<?php

namespace Dispatcher\Listeners;

use Dispatcher\Events\CompanyEvent;
use Dispatcher\Events\Order\RefundEvent;
use Exceptions\Technical\OrderRefundException;
use Nette\Object;
use ServiceActivatorModule\ServiceActivator;
use Dispatcher\Events\ServicePurchasedEvent;
use Services\ServiceService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use EventLocator;

class ServiceListener extends Object implements EventSubscriberInterface
{
    /**
     * @var ServiceService
     */
    private $serviceService;

    /**
     * @var ServiceActivator
     */
    private $serviceActivator;

    /**
     * @param ServiceService $serviceService
     * @param ServiceActivator $serviceActivator
     */
    public function __construct(ServiceService $serviceService, ServiceActivator $serviceActivator)
    {
        $this->serviceService = $serviceService;
        $this->serviceActivator = $serviceActivator;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            EventLocator::SERVICE_PURCHASED => ['onServicePurchased', 10],
            EventLocator::COMPANY_INCORPORATED => 'onIncorporated',
            EventLocator::ORDER_REFUNDED => ['onOrderRefunded', 20],
        ];
    }

    /**
     * @param ServicePurchasedEvent $serviceEvent
     */
    public function onServicePurchased(ServicePurchasedEvent $serviceEvent)
    {
        if ($serviceEvent->isCompanyIncorporated()) {
            $this->serviceActivator->activate($serviceEvent->getService());
        }
    }

    /**
     * @param CompanyEvent $companyEvent
     */
    public function onIncorporated(CompanyEvent $companyEvent)
    {
        foreach ($companyEvent->getCompany()->getGroupedServices() as $service) {
            $this->serviceActivator->activate($service);
        }
    }

    /**
     * @param RefundEvent $refundEvent
     * @throws OrderRefundException
     */
    public function onOrderRefunded(RefundEvent $refundEvent)
    {
        foreach ($refundEvent->getOrderItems() as $orderItem) {
            $service = $this->serviceService->getOrderItemService($orderItem);
            if ($service) {
                $this->serviceService->deleteService($service);
            }
        }
    }
}
