<?php

namespace Dispatcher\Listeners;

use CompanyEmailer;
use EventLocator;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Dispatcher\Events\CompanyEvent;
use Dispatcher\Events\ServicePurchasedEvent;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Nette\Object;

class IncorporationListener extends Object implements EventSubscriberInterface
{

    /**
     * @var CompanyEmailer
     */
    private $companyEmailer;

    /**
     * @param CompanyEmailer $companyEmailer
     */
    public function __construct(CompanyEmailer $companyEmailer)
    {
        $this->companyEmailer = $companyEmailer;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            EventLocator::COMPANY_INCORPORATION_PENDING => 'onIncorporationPending',
            EventLocator::COMPANY_INCORPORATION_ERROR => 'onIncorporationError',
        );
    }

    /**
     * @param CompanyEvent $companyEvent
     */
    public function onIncorporationPending(CompanyEvent $companyEvent)
    {
        $this->companyEmailer->sendSubmisionSuccess($companyEvent->getCompany()->getCustomer(), $companyEvent->getCompany());
    }

    /**
     * @param CompanyEvent $companyEvent
     */
    public function onIncorporationError(CompanyEvent $companyEvent)
    {
        $this->companyEmailer->sendSubmisionError($companyEvent->getCompany()->getCustomer(), $companyEvent->getCompany());
    }
}
