<?php

namespace Dispatcher\Listeners\Feefo;

use EventLocator;
use Services\FeefoService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Dispatcher\Events\CompanyEvent;
use Nette\Object;

class UpdateStatusListener extends Object implements EventSubscriberInterface
{

    /**
     * @var FeefoService
     */
    private $feefoService;

    /**
     * @param FeefoService $feefoService
     */
    public function __construct(FeefoService $feefoService)
    {
        $this->feefoService = $feefoService;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            EventLocator::COMPANY_INCORPORATED => 'onIncorporated',
        );
    }

    /**
     * @param CompanyEvent $companyEvent
     */
    public function onIncorporated(CompanyEvent $companyEvent)
    {
        $order = $companyEvent->getCompany()->getOrder();
        if ($order) {
            $feefo = $this->feefoService->getFeefoByOrder($order);
            if ($feefo) {
                $feefo->markAsEligible();
                $this->feefoService->saveFeefo($feefo);
            }
        }
    }
}
