<?php

namespace Dispatcher\Listeners;

use Company;
use Dispatcher\Events\OrderEvent;
use Entities\OrderItem;
use EventLocator;
use Exception;
use Nette\Object;
use NomineeDirector;
use Package;
use Services\CompanyService;
use Services\PackageService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class NomineeDirectorListener extends Object implements EventSubscriberInterface
{
    /**
     * @var PackageService
     */
    private $packageService;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @param PackageService $packageService
     * @param CompanyService $companyService
     */
    public function __construct(PackageService $packageService, CompanyService $companyService)
    {
        $this->packageService = $packageService;
        $this->companyService = $companyService;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            EventLocator::ORDER_COMPLETED => 'onOrderCompleted',
        );
    }

    /**
     * @param OrderEvent $event
     * @throws Exception
     */
    public function onOrderCompleted(OrderEvent $event)
    {
        $orderItems = $event->getOrder()->getItems();
        foreach ($orderItems as $orderItem) {
            $this->processOrderItem($orderItem);
        }
    }

    /**
     * @param OrderItem $orderItem
     */
    private function processOrderItem(OrderItem $orderItem)
    {
        $package = $this->packageService->getPackageById($orderItem->getProductId());
        if ($package && $package->isInternational()) {
            $company = Company::getCompanyByOrderId($orderItem->getOrder()->getId());
            $this->processInternationalPackage($package, $company);
        }
    }

    /**
     * @param Package $package
     * @param Company $company
     * @throws Exception
     */
    private function processInternationalPackage(Package $package, Company $company)
    {
        $packageProducts = $package->getPackageProducts('includedProducts');
        foreach ($packageProducts as $product) {
            if ($product instanceof NomineeDirector && $product->isIncludedByDefault()) {
                $this->companyService->addNomineeDirectorToCompany($company, $product);
            }
        }
    }
}
