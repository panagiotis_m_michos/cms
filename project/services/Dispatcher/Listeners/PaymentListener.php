<?php

namespace Dispatcher\Listeners;

use BasketProduct;
use Dispatcher\Events\PaymentEvent;
use Entities\Company;
use EventLocator;
use Exceptions\Technical\CompanyException;
use Nette\Object;
use PaymentEmailer;
use Services\CompanyService;
use SessionNamespace;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class PaymentListener extends Object implements EventSubscriberInterface
{

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var PaymentEmailer
     */
    private $paymentEmailer;

    /**
     * @var SessionNamespace
     */
    private $orderSummarySession;

    /**
     * @param CompanyService $companyService
     * @param PaymentEmailer $paymentEmailer
     * @param SessionNamespace $orderSummarySession
     */
    public function __construct(
        CompanyService $companyService,
        PaymentEmailer $paymentEmailer,
        SessionNamespace $orderSummarySession
    )
    {
        $this->companyService = $companyService;
        $this->paymentEmailer = $paymentEmailer;
        $this->orderSummarySession = $orderSummarySession;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            EventLocator::PAYMENT_SUCCEEDED => 'onPaymentSucceeded',
        ];
    }

    /**
     * @param PaymentEvent $paymentEvent
     * @throws CompanyException
     */
    public function onPaymentSucceeded(PaymentEvent $paymentEvent)
    {
        $this->createImportedCompanies($paymentEvent);

    }

    /**
     * @param PaymentEvent $paymentEvent
     * @throws CompanyException
     */
    private function createImportedCompanies(PaymentEvent $paymentEvent)
    {
        /** @var BasketProduct $item */
        foreach ($paymentEvent->getBasket()->getItems() as $itemId => $item) {
            if ($item->isLinkedToNewCompany()) {
                $company = $this->companyService->getCompanyByCompanyNumber($item->companyNumber);

                if ($company) {
                    if ($company->getCustomer()->isEqual($paymentEvent->getCustomer())) {
                        $item->setCompanyId($company->getCompanyId());
                    } else {
                        $paymentEvent->addNotImportedCompany($itemId, $company);
                    }
                } else {
                    $companyDetails = $this->orderSummarySession->importedCompanies[$item->companyNumber];
                    if (!empty($companyDetails) && !empty($companyDetails['companyName'])) {
                        $importedCompany = new Company($paymentEvent->getCustomer(), $companyDetails['companyName']);
                        $importedCompany->hydrateImportedCompanyDetails($companyDetails);
                        $this->companyService->saveCompany($importedCompany);
                        $item->setCompanyId($importedCompany->getCompanyId());
                    } else {
                        throw CompanyException::importedCompanyDetailsMissing($item->companyNumber);
                    }
                }
            }
        }
    }
}
