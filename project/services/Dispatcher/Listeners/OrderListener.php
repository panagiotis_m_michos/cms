<?php

namespace Dispatcher\Listeners;

use Nette\Object;
use Session;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use EventLocator;

class OrderListener extends Object implements EventSubscriberInterface
{

    /**
     * @var Session
     */
    private $session;

    /**
     * @param Session $session
     */
    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            EventLocator::ORDER_COMPLETED => 'onOrderCompleted',
        );
    }

    public function onOrderCompleted()
    {
        $orderSummarySession = $this->session->getNamespace('orderSummary');
        $orderSummarySession->remove();
    }
}
