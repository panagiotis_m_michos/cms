<?php

namespace Dispatcher\Listeners\CompanyHouse;

use Nette\Object;
use Services\SubmissionService as FormSubmissionService;
use Dispatcher\Events\CompanyHouse\StatusEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use EventLocator;
use Company;
use ErrorTestingEmailer;

class SubmissionStatusListener extends Object implements EventSubscriberInterface
{
    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            EventLocator::FORM_SUBMISSION_REJECT => 'onReject',
            EventLocator::FORM_SUBMISSION_FAILURE => 'onFailure',
            EventLocator::FORM_SUBMISSION_ACCEPT => 'onAccept',
        );
    }

    /**
     * @var FormSubmissionService 
     */
    private $formSubmissionService;

    /**
     * @var ErrorTestingEmailer 
     */
    private $errorTestingEmailer;

    /**
     * @param FormSubmissionService $formSubmissionService
     * @param ErrorTestingEmailer $errorTestingEmailer
     */
    public function __construct(FormSubmissionService $formSubmissionService, ErrorTestingEmailer $errorTestingEmailer)
    {
        $this->formSubmissionService = $formSubmissionService;
        $this->errorTestingEmailer = $errorTestingEmailer;
    }

    /**
     * @param StatusEvent $statusEvent
     */
    public function onAccept(StatusEvent $statusEvent)
    {
        $company = Company::getCompany($statusEvent->getCompanyId());
        $company->syncAllData();
    }

    /**
     * @param StatusEvent $statusEvent
     */
    public function onFailure(StatusEvent $statusEvent)
    {
        $this->errorTestingEmailer->sendFormSubmisionError($statusEvent->getCompanyId(), $statusEvent->getFormSubmissionId());
    }

    /**
     * @param StatusEvent $statusEvent
     */
    public function onReject(StatusEvent $statusEvent)
    {
        foreach ($statusEvent->getResponseRejections() as $rejection) {
            $rejectionEntity = $this->formSubmissionService->createFormSubmissionError($statusEvent->getFormSubmission(), $rejection);
            $this->formSubmissionService->saveFormSubmissionError($rejectionEntity);
        }
    }

}
