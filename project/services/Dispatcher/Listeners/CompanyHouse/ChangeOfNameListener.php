<?php

namespace Dispatcher\Listeners\CompanyHouse;

use Nette\Object;
use Services\SubmissionService as FormSubmissionService;
use Dispatcher\Events\CompanyHouse\StatusEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use EventLocator;

class ChangeOfNameListener extends Object implements EventSubscriberInterface
{
    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            EventLocator::FORM_SUBMISSION_ACCEPT_CHANGE_OF_NAME => 'onAccept',
        );
    }

    /**
     * @var FormSubmissionService 
     */
    private $formSubmisionService;

    /**
     * @param FormSubmissionService $formSubmisionService
     */
    public function __construct(FormSubmissionService $formSubmisionService)
    {
        $this->formSubmisionService = $formSubmisionService;
    }

    /**
     * @TODO remove set option and arrays
     * @param StatusEvent $statusEvent
     */
    public function onAccept(StatusEvent $statusEvent)
    {
        $data = $this->formSubmisionService->acceptChangeOfName($statusEvent->getFormSubmission(), $statusEvent->getSubmissionStatus());
        $statusEvent->setOption('companyNumber', $data['companyNumber']);
        $statusEvent->setOption('attachmentPath', $data['attachmentPath']);
    }
}
