<?php

namespace Dispatcher\Listeners\CompanyHouse;

use Nette\Object;
use Services\SubmissionService as FormSubmissionService;
use Dispatcher\Events\CompanyHouse\StatusEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use EventLocator;

class IncorporationListener extends Object implements EventSubscriberInterface
{
    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            EventLocator::FORM_SUBMISSION_ACCEPT_INCORPORATION => 'onAccept',
            EventLocator::FORM_SUBMISSION_REJECT_INCORPORATION => 'onReject',
        );
    }

    /**
     * @var FormSubmissionService 
     */
    private $formSubmisionService;

    /**
     * @param FormSubmissionService $formSubmisionService
     */
    public function __construct(FormSubmissionService $formSubmisionService)
    {
        $this->formSubmisionService = $formSubmisionService;
    }

    /**
     * @param StatusEvent $statusEvent
     */
    public function onAccept(StatusEvent $statusEvent)
    {
        $data = $this->formSubmisionService->acceptCompanyIncorporation($statusEvent->getFormSubmission(), $statusEvent->getSubmissionStatus());
        $statusEvent->setOption('date', $data['incorporationDate']);
        $statusEvent->setOption('companyNumber', $data['companyNumber']);
        $statusEvent->setOption('attachmentPath', $data['attachmentPath']);
    }

    /**
     * @param StatusEvent $statusEvent
     */
    public function onReject(StatusEvent $statusEvent)
    {
        $this->formSubmisionService->rejectCompanyIncorporation($statusEvent->getFormSubmission(), $statusEvent->getSubmissionStatus());
    }
}
