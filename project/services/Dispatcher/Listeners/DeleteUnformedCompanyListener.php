<?php

namespace Dispatcher\Listeners;

use Dispatcher\Events\Order\RefundEvent;
use Entities\Company;
use Entities\OrderItem;
use EventLocator;
use Nette\Object;
use Services\CompanyService;
use Services\PackageService;
use Services\ServiceService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class DeleteUnformedCompanyListener extends Object implements EventSubscriberInterface
{
    /**
     * @var ServiceService
     */
    private $serviceService;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var PackageService
     */
    private $packageService;

    /**
     * @param ServiceService $serviceService
     * @param CompanyService $companyService
     * @param PackageService $packageService
     */
    public function __construct(
        ServiceService $serviceService,
        CompanyService $companyService,
        PackageService $packageService
    )
    {
        $this->serviceService = $serviceService;
        $this->companyService = $companyService;
        $this->packageService = $packageService;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            EventLocator::ORDER_REFUNDED => ['onOrderRefunded', 15],
        ];
    }

    /**
     * @param RefundEvent $refundEvent
     */
    public function onOrderRefunded(RefundEvent $refundEvent)
    {
        foreach ($refundEvent->getOrderItems() as $orderItem) {
            $company = $this->companyService->getCompanyByOrder($orderItem->getOrder());
            if (!$this->canDeleteRefundedCompany($orderItem, $company)) {
                return;
            }

            $service = $this->serviceService->getOrderItemService($orderItem);
            if ($service) {
                $this->serviceService->deleteService($service);
            }

            $this->companyService->deleteCompany($company);
        }
    }

    /**
     * @param OrderItem $orderItem
     * @param Company|NULL $company
     * @return bool
     */
    private function canDeleteRefundedCompany(OrderItem $orderItem, Company $company = NULL)
    {
        $package = $this->packageService->getPackageById($orderItem->getProductId());

        return $package && $company && !$company->isIncorporated() && !$this->companyService->isCompanyIncorporationPending($company);
    }
}
