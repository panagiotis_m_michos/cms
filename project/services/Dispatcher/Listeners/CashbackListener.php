<?php

namespace Dispatcher\Listeners;

use CashBackEmailer;
use Dispatcher\Events\CashbackArchiveEvent;
use Dispatcher\Events\CustomerEvent;
use Entities\Cashback;
use Nette\Object;
use Psr\Log\LoggerInterface;
use Services\CashbackService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use EventLocator;

class CashbackListener extends Object implements EventSubscriberInterface
{
    /**
     * @var CashbackService
     */
    private $cashbackService;

    /**
     * @var CashBackEmailer
     */
    private $cashBackEmailer;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param CashbackService $cashbackService
     * @param CashBackEmailer $cashBackEmailer
     * @param LoggerInterface $logger
     */
    public function __construct(
        CashbackService $cashbackService,
        CashBackEmailer $cashBackEmailer,
        LoggerInterface $logger
    )
    {
        $this->cashbackService = $cashbackService;
        $this->cashBackEmailer = $cashBackEmailer;
        $this->logger = $logger;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            EventLocator::CUSTOMER_BANK_DETAILS_UPDATED => 'onCustomerBankDetailsUpdated',
            EventLocator::CASHBACK_ARCHIVED => 'onCashbackArchived',
        ];
    }

    /**
     * @param CustomerEvent $customerEvent
     */
    public function onCustomerBankDetailsUpdated(CustomerEvent $customerEvent)
    {
        $cashbacks = $this->cashbackService->getCustomerCashbacksToUnarchive($customerEvent->getCustomer());

        foreach ($cashbacks as $cashbackData) {
            /** @var Cashback $cashback */
            $cashback = $cashbackData[0];

            $cashback->setIsArchived(FALSE);
            $cashback->setStatusId(Cashback::STATUS_ELIGIBLE);
            $this->cashbackService->saveCashback($cashback);
        }
    }

    /**
     * @param CashbackArchiveEvent $cashbackEvent
     */
    public function onCashbackArchived(CashbackArchiveEvent $cashbackEvent)
    {
        $this->cashBackEmailer->sendInvalidCashbackDetailEmail($cashbackEvent->getCustomer());
    }
}
