<?php

namespace Dispatcher\Listeners;

use Customer;
use Dispatcher\Events\SignUpEvent;
use EventLocator;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Nette\Object;

class SignUpListener extends Object implements EventSubscriberInterface
{
    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            EventLocator::CUSTOMER_SIGN_UP => 'onSignUp',
        );
    }

    /**
     * @param SignUpEvent $signUpEvent
     */
    public function onSignUp(SignUpEvent $signUpEvent)
    {
        $signUpEvent->getEmailer()->sendSignUpEmail(
            $signUpEvent->getPassword(),
            $signUpEvent->getCustomer()
        );

        Customer::signIn($signUpEvent->getCustomer()->getId());
    }
}
