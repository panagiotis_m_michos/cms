<?php

namespace Dispatcher\Listeners;

use Company as OldCompany;
use Dispatcher\Events\CompanyEvent;
use Entities\Company;
use EventLocator;
use Exception;
use Nette\Object;
use Package;
use PremierBusinessEmailer;
use Psr\Log\LoggerInterface;
use Services\CompanyService;
use Services\PackageService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class NotifyPremierBusinessListener extends Object implements EventSubscriberInterface
{

    /**
     * @var PackageService
     */
    private $packageService;

    /**
     * @var PremierBusinessEmailer
     */
    private $premierBusinessEmailer;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @param PackageService $packageService
     * @param CompanyService $companyService
     * @param PremierBusinessEmailer $premierBusinessEmailer
     * @param LoggerInterface $logger
     */
    public function __construct(
        PackageService $packageService,
        CompanyService $companyService,
        PremierBusinessEmailer $premierBusinessEmailer,
        LoggerInterface $logger
    )
    {
        $this->premierBusinessEmailer = $premierBusinessEmailer;
        $this->packageService = $packageService;
        $this->logger = $logger;
        $this->companyService = $companyService;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            EventLocator::COMPANY_INCORPORATED => 'onIncorporated',
        ];
    }

    /**
     * @param CompanyEvent $companyEvent
     */
    public function onIncorporated(CompanyEvent $companyEvent)
    {
        $package = $this->packageService->getPackageById($companyEvent->getCompanyProductId());
        if ($package && $package->isInternational()) {
            $this->notifyPremierBusiness($companyEvent->getCompany(), $package);
        }
    }

    /**
     * @param Company $company
     * @param Package $package
     */
    private function notifyPremierBusiness(Company $company, Package $package)
    {
        $this->sendMail($company, $package);
        $this->logSuccess($company, $package);
    }

    /**
     * @param Company $company
     * @param Package $package
     */
    private function sendMail(Company $company, Package $package)
    {
        try {
            $oldCompany = OldCompany::getCompany($company->getId());
            $oldCompany->syncAllData();
        } catch (Exception $e) {
            $this->logger->error($e);
            // we want to send email to Premier Business even if sync failed
        }

        $companySummary = $this->companyService->getCompanySummaryByCompany($company);
        $summaryPdfString = $companySummary->getCompanySummaryPdfString();
        $this->premierBusinessEmailer->sendInternationalCompanyIncorporated($company, $package, $summaryPdfString);
    }

    /**
     * @param Company $company
     * @param Package $package
     */
    private function logSuccess(Company $company, Package $package)
    {
        $companyNumber = $company->getCompanyNumber();
        $companyName = $company->getCompanyName();
        $packageName = $package->getLngTitle();
        $this->logger->info(
            "Premier Business was notified that company $companyName (company number: $companyNumber) was incorporated with $packageName"
        );
    }
}
