<?php

namespace Dispatcher\Listeners;

use EventLocator;
use ILogger;
use SagePay\Reporting\Interfaces\ISagePay;
use Services\Payment\TokenService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Dispatcher\Events\TokenEvent;
use Services\Payment\SageService;

class TokenListener implements EventSubscriberInterface
{
    /**
     * @var TokenService
     */
    private $tokenService;

    /**
     * @var SageService
     */
    private $sageService;

    /**
     * @var ISagePay
     */
    private $sagePay;

    /**
     * @param TokenService $tokenService
     * @param SageService $sageService
     * @param ISagePay $sagePay
     */
    public function __construct(TokenService $tokenService, SageService $sageService, ISagePay $sagePay)
    {
        $this->tokenService = $tokenService;
        $this->sageService = $sageService;
        $this->sagePay = $sagePay;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            EventLocator::TOKEN_EMAIL_EXPIRED => 'onExpired',
        ];
    }

    /**
     * @param TokenEvent $tokenEvent
     */
    public function onExpired(TokenEvent $tokenEvent)
    {
        $token = $tokenEvent->getToken();
        $tokenId = $token->getId();
        $customer = $token->getCustomer();

        $this->tokenService->removeToken($token, $customer->getEmail());
        $tokenEvent->getLogger()->logMessage(ILogger::INFO, sprintf("Token `%d` deleted from DB", $tokenId));

        $tokenIdentifier = $token->getIdentifier();
        if ($this->sagePay->isTokenActive($tokenIdentifier)) {
            $this->sageService->deleteToken($tokenIdentifier);
            $tokenEvent->getLogger()->logMessage(ILogger::INFO, sprintf("Token `%d` deleted from sagepay", $tokenId));
        }
    }
}
