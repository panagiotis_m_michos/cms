<?php

namespace Dispatcher\Listeners;

use EventLocator;
use ILogger;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Dispatcher\Events\TokenEvent;
use Emailers\TokenEmailer;

class TokenEmailListener implements EventSubscriberInterface
{

    /**
     * @var TokenEmailer 
     */
    protected $emailer;

    /**
     * @param TokenEmailer $emailer
     */
    public function __construct(TokenEmailer $emailer)
    {
        $this->emailer = $emailer;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            EventLocator::TOKEN_EMAIL_SOON_EXPIRES => 'onExpires',
            EventLocator::TOKEN_EMAIL_EXPIRED => 'onExpired',
        );
    }

    /**
     * @param TokenEvent $tokenEvent 
     */
    public function onExpires(TokenEvent $tokenEvent)
    {
        $token = $tokenEvent->getToken();
        $customer = $token->getCustomer();

        $this->emailer->sendEmailSoonExpires($token);

        $tokenEvent->getLogger()->logMessage(
            ILogger::INFO, sprintf(
                "Token `%d`, soon expires email sent to `%s`",
                $token->getId(),
                $customer->getEmail()
            )
        );
    }

    /**
     * @param TokenEvent $tokenEvent 
     */
    public function onExpired(TokenEvent $tokenEvent)
    {
        $token = $tokenEvent->getToken();
        $customer = $token->getCustomer();

        $this->emailer->sendEmailExpired($token);

        $tokenEvent->getLogger()->logMessage(
            ILogger::INFO, sprintf(
                "Token `%d`, expired email sent to `%s`",
                $token->getId(),
                $customer->getEmail()
            )
        );
    }
}
