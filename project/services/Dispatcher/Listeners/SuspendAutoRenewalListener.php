<?php

namespace Dispatcher\Listeners;

use Dispatcher\Events\TokenRemovedEvent;
use EventLocator;
use ServiceSettingsModule\Services\ServiceSettingsService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class SuspendAutoRenewalListener implements EventSubscriberInterface
{

    /**
     * @var ServiceSettingsService
     */
    private $serviceSettingsService;

    /**
     * @param ServiceSettingsService $serviceSettingsService
     */
    public function __construct(ServiceSettingsService $serviceSettingsService)
    {
        $this->serviceSettingsService = $serviceSettingsService;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            EventLocator::TOKEN_REMOVED => 'onTokenRemoved',
        ];
    }

    /**
     * @param TokenRemovedEvent $event
     */
    public function onTokenRemoved(TokenRemovedEvent $event)
    {
        $this->serviceSettingsService->suspendAutoRenewalByToken($event->getToken());
    }
}
