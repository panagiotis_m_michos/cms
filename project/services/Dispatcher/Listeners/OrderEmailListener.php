<?php

namespace Dispatcher\Listeners;

use Dispatcher\Events\Order\RefundEvent;
use Nette\Object;
use OrderEmailer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use EventLocator;

class OrderEmailListener extends Object implements EventSubscriberInterface
{
    /**
     * @var OrderEmailer
     */
    private $orderEmailer;

    /**
     * @param OrderEmailer $orderEmailer
     */
    public function __construct(OrderEmailer $orderEmailer)
    {
        $this->orderEmailer = $orderEmailer;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            EventLocator::ORDER_REFUNDED => array('onOrderRefunded', 5),
        );
    }

    /**
     * @param RefundEvent $refundEvent
     */
    public function onOrderRefunded(RefundEvent $refundEvent)
    {
        $this->orderEmailer->sendCustomerRefundEmail(
            $refundEvent->getOrder(),
            $refundEvent->getTotalCreditRefund(),
            $refundEvent->getTotalMoneyRefund()
        );
    }

}
