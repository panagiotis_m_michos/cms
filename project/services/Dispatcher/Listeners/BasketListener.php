<?php

namespace Dispatcher\Listeners;

use Dispatcher\Events\Basket\ItemRemovedEvent;
use Nette\Object;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use EventLocator;

class BasketListener extends Object implements EventSubscriberInterface
{

    /**
     * @var string
     */
    private $orderSummaryFormClass;

    /**
     * @param $orderSummaryFormClass
     */
    public function __construct($orderSummaryFormClass)
    {
        $this->orderSummaryFormClass = $orderSummaryFormClass;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            EventLocator::BASKET_ITEM_REMOVED => 'onItemRemoved',
        );
    }

    /**
     * @param ItemRemovedEvent $itemRemovedEvent
     */
    public function onItemRemoved(ItemRemovedEvent $itemRemovedEvent)
    {
        $itemId = $itemRemovedEvent->getItemId();

        if (isset($_SESSION)) {
            unset($_SESSION[$this->orderSummaryFormClass]['summary']['values']['companyNumbers'][$itemId]);
            unset($_SESSION[$this->orderSummaryFormClass]['summary']['errors'][$itemId]);
        }
    }
}
