<?php

namespace Dispatcher\Listeners;

use CustomerActionLogger;
use Dispatcher\Events\CustomerEvent;
use Dispatcher\Events\Order\RefundEvent;
use Nette\Object;
use Services\CustomerService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use EventLocator;

class CustomerListener extends Object implements EventSubscriberInterface
{
    /**
     * @var CustomerService
     */
    private $customerService;

    /**
     * @param $customerService
     */
    public function __construct($customerService)
    {
        $this->customerService = $customerService;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            EventLocator::CUSTOMER_UPDATED => 'onUpdate',
            EventLocator::ORDER_REFUNDED => array('onOrderRefunded', 10),
        );
    }

    /**
     * TODO: create customer logger entity
     * @param CustomerEvent $customerEvent
     */
    public function onUpdate(CustomerEvent $customerEvent)
    {
        CustomerActionLogger::logEntity($customerEvent->getCustomer(), CustomerActionLogger::DETAILS_CHANGED);
    }

    /**
     * @param RefundEvent $refundEvent
     */
    public function onOrderRefunded(RefundEvent $refundEvent)
    {
        $order = $refundEvent->getOrder();
        $customer = $order->getCustomer();
        $totalCreditRefund = $refundEvent->getTotalCreditRefund();

        if ($totalCreditRefund > 0) {
            $this->customerService->addCredit($customer, $totalCreditRefund);
        }
    }


}
