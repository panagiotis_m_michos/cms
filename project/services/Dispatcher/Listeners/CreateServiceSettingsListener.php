<?php

namespace Dispatcher\Listeners;

use Dispatcher\Events\ServicePurchasedEvent;
use Entities\Payment\Token;
use Entities\ServiceSettings;
use EventLocator;
use Services\ServiceService;
use ServiceSettingsModule\Facades\ServiceSettingsFacade;
use ServiceSettingsModule\Services\ServiceSettingsService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CreateServiceSettingsListener implements EventSubscriberInterface
{

    /**
     * @var ServiceService
     */
    private $serviceService;

    /**
     * @var ServiceSettingsService
     */
    private $serviceSettingsService;

    /**
     * @var ServiceSettingsFacade
     */
    private $serviceSettingsFacade;

    /**
     * @param ServiceService $serviceService
     * @param ServiceSettingsService $serviceSettingsService
     * @param ServiceSettingsFacade $serviceSettingsFacade
     */
    public function __construct(
        ServiceService $serviceService,
        ServiceSettingsService $serviceSettingsService,
        ServiceSettingsFacade $serviceSettingsFacade
    )
    {
        $this->serviceService = $serviceService;
        $this->serviceSettingsService = $serviceSettingsService;
        $this->serviceSettingsFacade = $serviceSettingsFacade;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            EventLocator::SERVICE_PURCHASED => ['onServicePurchased', 20],
        ];
    }

    /**
     * @param ServicePurchasedEvent $event
     */
    public function onServicePurchased(ServicePurchasedEvent $event)
    {
        $settings = $this->serviceSettingsService->getOrCreateSettingsByType(
            $event->getCompany(),
            $event->getService()->getServiceTypeId()
        );

        $this->setDefaultServiceSettings($settings, $event->getRenewalToken());
    }

    /**
     * @param ServiceSettings $settings
     * @param Token $renewalToken
     */
    private function setDefaultServiceSettings(ServiceSettings $settings, Token $renewalToken = NULL)
    {
        $service = $this->serviceService->getLatestServiceBySettings($settings);
        if ($renewalToken && $service->isAutoRenewalAllowed()) {
            $this->serviceSettingsFacade->enableAutoRenewal($settings, $renewalToken);
        } else {
            $this->serviceSettingsFacade->disableAutoRenewal($settings);
        }
    }
}
