<?php

namespace Dispatcher\Listeners;

use Dispatcher\Events\ServicesEmailEvent;
use Emailers\ServicesEmailer;
use EventLocator;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ServicesEmailListener implements EventSubscriberInterface
{

    /**
     * @var ServicesEmailer
     */
    protected $emailer;

    /**
     * @param ServicesEmailer $emailer
     */
    public function __construct(ServicesEmailer $emailer)
    {
        $this->emailer = $emailer;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            EventLocator::COMPANY_SERVICES_EXPIRES_IN_28 => 'sendExpirationEmail',
            EventLocator::COMPANY_SERVICES_EXPIRES_IN_15 => 'sendExpirationEmail',
            EventLocator::COMPANY_SERVICES_EXPIRES_IN_1 => 'sendExpirationEmail',
            EventLocator::COMPANY_SERVICES_EXPIRED => 'sendExpirationEmail',
            EventLocator::COMPANY_SERVICES_EXPIRED_15 => 'sendExpirationEmail',
            EventLocator::COMPANY_SERVICES_EXPIRED_28 => 'sendExpirationEmail',
        ];
    }

    /**
     * @param ServicesEmailEvent $servicesEmailEvent
     */
    public function sendExpirationEmail(ServicesEmailEvent $servicesEmailEvent)
    {
        $emailView = $servicesEmailEvent->getEmailView();
        $email = $servicesEmailEvent->getEmail();
        $this->emailer->sendEmailExpiration($emailView, $email);
    }
}
