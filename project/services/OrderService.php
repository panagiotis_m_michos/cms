<?php

namespace Services;

use Entities\Customer;
use Repositories\OrderRepository;
use Entities\Order;
use Nette\Object;
use DoctrineDataSource;

class OrderService extends Object
{

    /**
     * @var OrderRepository
     */
    private $repository;

    /**
     * @param OrderRepository $repository
     */
    public function __construct(OrderRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Order $entity
     */
    public function saveOrder(Order $entity)
    {
        $this->repository->saveEntity($entity);
    }

    /**
     * @param int $id
     * @return Order
     */
    public function getOrderById($id)
    {
        return $this->repository->find($id);
    }

    /**
     * @return DoctrineDataSource
     */
    public function getListDatasource()
    {
        $builder = $this->repository->getListBuilder();
        return new DoctrineDataSource($builder);
    }

    /**
     * @param Customer $customer
     * @param int $orderId
     * @return Order|NULL
     */
    public function getCustomerOrder(Customer $customer, $orderId)
    {
        $order = $this->getOrderById((int) $orderId);
        if (!$order || !$order->getCustomer()->isEqual($customer)) {
            return NULL;
        }
        return $order;
    }
}
