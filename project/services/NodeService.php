<?php

namespace Services;

use BasketProduct;
use Exceptions\Business\NodeException;
use FNode;
use Nette\Object;
use Repositories\Nodes\NodeRepository;
use Repositories\Nodes\ProductRepository;

class NodeService extends Object
{
    /**
     * @var NodeRepository
     */
    private $repository;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @todo: create ProductService (issue: ProductService already exists but it's not "our" service)
     *
     * @param NodeRepository $repository
     * @param ProductRepository $productRepository
     */
    public function __construct(NodeRepository $repository, ProductRepository $productRepository)
    {
        $this->repository = $repository;
        $this->productRepository = $productRepository;
    }

    /**
     * @param int $nodeId
     * @param bool $need
     * @return FNode
     * @throws NodeException
     */
    public function getNodeById($nodeId, $need = FALSE)
    {
        $node = $this->repository->getNodeById($nodeId);
        if (!$node && $need) {
            throw NodeException::nodeDoesNotExist($nodeId);
        }
        return $node;
    }

    /****************************************** SHOULD BE MOVED TO PRODUCT_SERVICE ******************************************/

    /**
     * @param int $nodeId
     * @param bool $need
     * @return BasketProduct
     * @throws NodeException
     */
    public function getProductById($nodeId, $need = FALSE)
    {
        $product = $this->productRepository->getProductById($nodeId);
        if (!$product && $need) {
            throw NodeException::nodeIsNotAProduct($nodeId);
        }
        return $product;
    }

    /**
     * @param int $productId
     * @return BasketProduct|null
     */
    public function getPublishedProductById($productId)
    {
        $product = $this->getProductById($productId);

        return $product->isOk2show() ? $product : NULL;
    }

    /**
     * @return array
     */
    public function getProductsIdsWithIdCheckRequired()
    {
        return $this->productRepository->getProductsIdsWithIdCheckRequired();
    }
}
