<?php

namespace Services\Payment;

use Entities\Customer;
use Entities\Payment\Token;
use Exception;
use FApplication;
use Nette\Object;
use SagePay\Token\Exception\AuthenticationRequired;
use SagePay\Token\Exception\ConnectionError;
use SagePay\Token\Exception\DataMismatch;
use SagePay\Token\Exception\FailedResponse;
use SagePay\Token\Exception\Transaction;
use SagePay\Token\Exception\ValidationError;
use SagePay\Token\Request\Details as RequestDetails;
use SagePay\Token\Request\Registration as RequestRegistration;
use SagePay\Token\Request\Remove as RequestRemove;
use SagePay\Token\Request\RequestAbstract;
use SagePay\Token\Response;
use SagePay\Token\Response\Registration as ResponseRegistration;
use SagePay\Token\Response\Summary as ResponseSummary;
use SagePay\Token\SageFactory;
use SagePay\Token\SagePay;
use SagePay\Token\Storage;
use Utils\Date;

class SageService extends Object
{
    const STORAGE_PAYMENT = 'requestDetails';

    /**
     * @var array
     */
    public static $cardTypes = [
        "MAESTRO" => "Maestro",
        "MC" => "MasterCard",
        "VISA" => "Visa",
        "DELTA" => "Visa Debit/Delta",
        "UKE" => "Visa Electron"
    ];

    /**
     * @var array
     */
    public static $states = [
        'AL' => "Alabama",
        'AK' => "Alaska",
        'AZ' => "Arizona",
        'AR' => "Arkansas",
        'CA' => "California",
        'CO' => "Colorado",
        'CT' => "Connecticut",
        'DE' => "Delaware",
        'DC' => "District Of Columbia",
        'FL' => "Florida",
        'GA' => "Georgia",
        'HI' => "Hawaii",
        'ID' => "Idaho",
        'IL' => "Illinois",
        'IN' => "Indiana",
        'IA' => "Iowa",
        'KS' => "Kansas",
        'KY' => "Kentucky",
        'LA' => "Louisiana",
        'ME' => "Maine",
        'MD' => "Maryland",
        'MA' => "Massachusetts",
        'MI' => "Michigan",
        'MN' => "Minnesota",
        'MS' => "Mississippi",
        'MO' => "Missouri",
        'MT' => "Montana",
        'NE' => "Nebraska",
        'NV' => "Nevada",
        'NH' => "New Hampshire",
        'NJ' => "New Jersey",
        'NM' => "New Mexico",
        'NY' => "New York",
        'NC' => "North Carolina",
        'ND' => "North Dakota",
        'OH' => "Ohio",
        'OK' => "Oklahoma",
        'OR' => "Oregon",
        'PA' => "Pennsylvania",
        'RI' => "Rhode Island",
        'SC' => "South Carolina",
        'SD' => "South Dakota",
        'TN' => "Tennessee",
        'TX' => "Texas",
        'UT' => "Utah",
        'VT' => "Vermont",
        'VA' => "Virginia",
        'WA' => "Washington",
        'WV' => "West Virginia",
        'WI' => "Wisconsin",
        'WY' => "Wyoming"
    ];

    /**
     * @var Storage
     */
    private $storage;

    /**
     * @var SagePay
     */
    private $sagePayment;

    /**
     * @param SagePay $sagePayment
     * @param Storage $storage
     */
    public function __construct(SagePay $sagePayment, Storage $storage)
    {
        $this->sagePayment = $sagePayment;
        $this->storage = $storage;
    }

    /**
     * @param RequestRegistration $registration
     * @param RequestDetails $details
     * @return PaymentResponse
     * @throws AuthenticationRequired
     * @throws FailedResponse
     * @throws ConnectionError
     * @throws ValidationError
     * @throws Transaction
     */
    public function makePayment(RequestRegistration $registration, RequestDetails $details)
    {
        try {
            $responseRegistration = $this->sagePayment->registerToken($registration);
            $responseSummary = $this->sagePayment->makePayment($details, $responseRegistration->getToken());
            $response = $this->createPaymentResponse($details, $responseSummary);
            return $response;
        } catch (AuthenticationRequired $e) {
            $this->saveDetails($details);
            throw $e;
        } catch (Exception $e) {
            $this->sagePayment->finishTransaction();
            if ($details->getStoreToken() && $details->getToken()) {
                $this->deleteToken($details->getToken());
            }
            throw $e;
        }
    }

    /**
     * @param array $sagePayResponseData
     * @param float $amount
     * @return PaymentResponse
     * @throws ConnectionError
     * @throws DataMismatch
     * @throws FailedResponse
     * @throws ValidationError
     */
    public function authorizePayment($sagePayResponseData, $amount)
    {
        $details = $this->getDetails();
        if (!$details || !($details instanceof RequestDetails)) {
            throw new ValidationError('No saved payment details found!');
        }
        $responseSummary = $this->sagePayment->authorizePayment($sagePayResponseData, $amount);
        $response = $this->createPaymentResponse($details, $responseSummary);
        return $response;
    }

    /**
     * @param string $url
     * @return string
     * @throws ValidationError
     */
    public function getAuthenticationForm($url)
    {
        return $this->sagePayment->getAuthenticationForm($url);
    }

    /**
     * @param $tokenIdentifier
     * @return Response
     * @throws FailedResponse
     * @throws ConnectionError
     * @throws ValidationError
     */
    public function deleteToken($tokenIdentifier)
    {
        $tokenIdentifier  = '{' . trim($tokenIdentifier, '{}') . '}';
        $remReq = new RequestRemove();
        $remReq->setToken($tokenIdentifier);
        $response = $this->sagePayment->removeToken($remReq);
        return $response;
    }

    /**
     * @param RequestRegistration
     * @return ResponseRegistration
     * @throws FailedResponse
     * @throws ConnectionError
     * @throws ValidationError
     */
    public function registerToken(RequestRegistration $registration)
    {
        $responseRegistration = $this->sagePayment->registerToken($registration);
        return $responseRegistration;
    }

    /**
     * @param RequestDetails $details
     * @param Token $token
     * @return PaymentResponse
     * @throws Exception
     */
    public function processRecurringPayment(RequestDetails $details, Token $token = NULL)
    {
        try {
            $response = $this->sagePayment->makePayment($details, $details->getToken());
            $paymentResponse = $this->createPaymentResponse($details, $response);

            if ($token) {
                $paymentResponse->setCardHolder($token->getCardHolder());
                $paymentResponse->setCardNumber($token->getCardNumber());
                $paymentResponse->setCardType($token->getCardType());
                $paymentResponse->setToken($token);
            }
            return $paymentResponse;
        } catch (Exception $e) {
            $this->sagePayment->finishTransaction();
            throw $e;
        }
    }


    /**
     * @return RequestDetails
     */
    public function getDetails()
    {
        return $this->storage->get(self::STORAGE_PAYMENT);
    }

    /**
     * @param RequestDetails $details
     */
    public function saveDetails(RequestDetails $details)
    {
        $clone = clone $details;
        $clone->setCv2(NULL);
        $this->storage->save(self::STORAGE_PAYMENT, $clone);
    }

    /**
     * @param array $data
     * @param string $currency
     * @return RequestRegistration
     */
    public function mapRegistration(array $data, $currency)
    {
        $req = new RequestRegistration();
        $req->cardHolder = isset($data['cardHolder']) ? $data['cardHolder'] : NULL;
        $req->cardNumber = isset($data['cardNumber']) ? $data['cardNumber'] : NULL;
        $req->expiryDate = isset($data['expiryDate']) ? $data['expiryDate'] : NULL;
        $req->startDate = isset($data['validFrom']) ? $data['validFrom'] : NULL;
        $req->issueNumber = isset($data['issueNumber']) ? $data['issueNumber'] : NULL;
        $req->cardType = isset($data['cardType']) ? $data['cardType'] : NULL;
        $req->cv2 = isset($data['CV2']) ? $data['CV2'] : NULL;
        $req->currency = $currency;
        return $req;
    }

    /**
     * @param array $data
     * @param float $amount
     * @param string $currency
     * @param string $description
     * @param boolean $isRecurring
     * @return RequestDetails 
     */
    public function mapDetails(array $data, $amount, $currency, $description, $isRecurring)
    {
        $det = new RequestDetails();
        $det->currency = $currency;
        $det->amount = $amount;
        $det->description = $description;
        $det->setCustomerEmail(isset($data['email']) ? $data['email'] : NULL);
        $det->billingSurname = isset($data['lastName']) ? $data['lastName'] : NULL;
        $det->billingFirstnames = isset($data['firstName']) ? $data['firstName'] : NULL;
        $det->billingSurname = isset($data['lastName']) ? $data['lastName'] : NULL;
        $det->billingFirstnames = isset($data['firstName']) ? $data['firstName'] : NULL;

        $det->billingAddress1 = isset($data['address1']) ? $data['address1'] : NULL;
        $det->billingAddress2 = isset($data['address2']) ? $data['address2'] : NULL;
        $det->billingCity = isset($data['city']) ? $data['city'] : NULL;
        $det->billingPostCode = isset($data['postcode']) ? $data['postcode'] : NULL;
        $det->billingState = isset($data['billingState']) ? $data['billingState'] : NULL;
        $det->billingCountry = isset($data['country']) ? $data['country'] : NULL;

        $det->deliverySurname = $det->billingSurname;
        $det->deliveryFirstnames = $det->billingFirstnames;
        $det->deliveryAddress1 = $det->billingAddress1;
        $det->deliveryAddress2 = $det->billingAddress2;
        $det->deliveryCity = $det->billingCity;
        $det->deliveryPostCode = $det->billingPostCode;
        $det->deliveryState = $det->billingState;
        $det->deliveryCountry = $det->billingCountry;

        if ($isRecurring) {
            $det->setStoreToken(1);
        }

        return $det;
    }

    /**
     * @param array $data
     * @param float $amount
     * @param string $currency
     * @param string $description
     * @param boolean $isRecurring
     * @return RequestDetails
     */
    public function mapAuthenticateDetails(array $data, $amount, $currency, $description, $isRecurring)
    {
        $request = $this->mapDetails($data, $amount, $currency, $description, $isRecurring);
        $request->setTxType(RequestAbstract::TYPE_AUTHENTICATE);
        return $request;
    }

    /**
     * @param RequestAbstract $request
     * @return array
     */
    public static function requestToArray(RequestAbstract $request)
    {
        $mapper = SageFactory::createAnnotationMapper();
        return $mapper->objectToArray($request);
    }

    /**
     * @param array $arr
     * @param RequestAbstract $request
     * @return RequestAbstract|ResponseAbstract
     */
    public static function arrayToRequest($arr, RequestAbstract $request)
    {
        $mapper = SageFactory::createAnnotationMapper();
        return $mapper->arrayToObject($arr, $request);
    }

    /**
     * @param Token $token
     * @param int $amount
     * @return RequestDetails
     */
    public static function createRecurringRequest(Token $token, $amount)
    {
        $config = FApplication::$config['payment'];
        $details = new RequestDetails();
        TokenService::mapTokenToRequest($token, $details);
        $tokenIdentifier  = '{' . trim($token->getIdentifier(), '{}') . '}';
        $details->setToken($tokenIdentifier);
        $details->setDescription($config['description']);
        $details->setCurrency($config['currency']);

        $details->setAmount($amount);
        $details->setApplyAvsCv2(RequestDetails::CV2_OFF);
        $details->setApply3dSecure(RequestDetails::SECURE3D_OFF);
        $details->setAccountType(RequestDetails::CONTINUOUS_AUTHORITY);
        $details->setStoreToken(1);
        return $details;
    }

    /**
     * @param RequestDetails $details
     * @param ResponseSummary $response
     * @return PaymentResponse
     */
    private function createPaymentResponse(RequestDetails $details, ResponseSummary $response)
    {
        $paymentResponse = PaymentResponse::fromDetails($details, $response);
        return $paymentResponse;
    }

}
