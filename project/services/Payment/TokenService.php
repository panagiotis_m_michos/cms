<?php

namespace Services\Payment;

use Dispatcher\Events\TokenEvent;
use Dispatcher\Events\TokenRemovedEvent;
use Entities\Customer;
use Entities\Payment\Token;
use Entities\Payment\TokenHistory;
use EventLocator;
use Exceptions\Business\ItemNotFound as ItemNotFoundException;
use Repositories\Payment\TokenRepository;
use Repositories\Payment\TokenHistoryRepository;
use SagePay\Reporting\Interfaces\IToken;
use SagePay\Reporting\Interfaces\ITokenService;
use SagePay\Token\Request\Details as RequestDetails;
use SagePay\Token\Request\Registration as RequestRegistration;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Utils\Date;
use Nette\Object;
use Doctrine\ORM\Internal\Hydration\IterableResult;

class TokenService extends Object implements ITokenService
{
    /**
     * @var TokenRepository
     */
    protected $tokenRepository;

    /**
     * @var TokenHistoryRepository
     */
    protected $tokenHistoryRepository;

    /**
     * @var EventDispatcher
     */
    private $eventDispatcher;

    /**
     * @param TokenRepository $tokenRepository
     * @param TokenHistoryRepository $tokenHistoryRepository
     * @param EventDispatcher $eventDispatcher
     */
    public function __construct(TokenRepository $tokenRepository, TokenHistoryRepository $tokenHistoryRepository, EventDispatcher $eventDispatcher)
    {
        $this->tokenRepository = $tokenRepository;
        $this->tokenHistoryRepository = $tokenHistoryRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param Token $token
     * @param string $statusId
     * @param string $actionBy
     */
    public function saveTokenWithHistory(Token $token, $statusId, $actionBy)
    {
        $this->tokenRepository->saveEntity($token);
        $this->saveHistory($token, $statusId, $actionBy);
    }

    /**
     * @param IToken|Token $token
     * @param string $actionBy
     * @return NULL
     */
    public function removeToken(IToken $token, $actionBy)
    {
        $this->eventDispatcher->dispatch(EventLocator::TOKEN_REMOVED, new TokenRemovedEvent($token));
        $token->setSageStatus(Token::SAGE_STATUS_DELETED);
        $token->setIsCurrent(FALSE);
        $this->saveTokenWithHistory($token, TokenHistory::STATUS_REMOVED, $actionBy);
    }

    /**
     * @param Token $token
     * @param string $statusId
     * @param string $actionBy
     * @return TokenHistory
     */
    public function saveHistory(Token $token, $statusId, $actionBy)
    {
        $tokenHistory = $this->createHistory($token, $statusId, $actionBy);
        $this->tokenHistoryRepository->saveEntity($tokenHistory);
    }

    /**
     * @param Token $token
     * @param string $statusId
     * @param string $actionBy
     * @return TokenHistory
     */
    public function createHistory(Token $token, $statusId, $actionBy)
    {
        $tokenHistory = new TokenHistory($statusId, $token->getIdentifier(), $token->getCardNumber(), $token->getCardHolder(), $actionBy);
        $tokenHistory->setAddress($token->getBillingAddress());
        return $tokenHistory;
    }

    /**
     * @param Date $fromDate
     * @param Date $toDate
     * @return IterableResult
     */
    public function getTokensForCardReminders(Date $fromDate, Date $toDate)
    {
        return $this->tokenRepository->getTokensForCardReminders($fromDate, $toDate);
    }

    /**
     * @param int $tokenId
     * @return Token
     */
    public function getTokenById($tokenId)
    {
        return $this->tokenRepository->find($tokenId);
    }

    /**
     * @param string $identifier
     * @return Token
     */
    public function getTokenByIdentifier($identifier)
    {
        return $this->tokenRepository->getTokenByIdentifier($identifier);
    }

    /**
     * @param Customer $customer
     * @return Token
     */
    public function getTokenByCustomer(Customer $customer)
    {
        return $this->tokenRepository->getActiveTokenByCustomer($customer);
    }

    /**
     * @param Token $token
     * @return TokenHistory[]
     */
    public function getHistory(Token $token)
    {
        return $this->tokenHistoryRepository->findBy(['token' => $token->getIdentifier()]);
    }

    /**
     *
     * @param Customer $customer
     * @param int $tokenId
     * @return Token
     * @throws ItemNotFoundException
     */
    public function getCustomerToken(Customer $customer, $tokenId)
    {
        $token = $this->tokenRepository->findOneBy(['customer' => $customer, 'tokenId' => $tokenId]);
        if (!$token) {
            throw new ItemNotFoundException("Customer `{$customer->getId()}` does not have token `{$tokenId}`");
        }
        return $token;
    }

    /**
     *
     * @param Customer $customer
     * @param string $cardNumber
     * @return null | Token
     */
    //public function getToken(Customer $customer, $cardNumber)
    //{
    //$token = $this->tokenRepository->findOneBy(array('customer' => $customer->getId(), 'cardNumber' => $cardNumber));
    //return $token;
    //}

    /**
     *
     * @param Customer $customer
     * @return Token
     * @throws ItemNotFoundException
     * @throws NoTokenException
     */
    //public function getCurrentToken(Customer $customer)
    //{
    //$tokens = $this->tokenRepository->findBy(array('customer' => $customer->getId()));
    //if (count($tokens) === 0) {
    //throw new NoTokenException("Customer `{$customer->getId()}` does not have any tokens");
    //}
    //foreach ($tokens as $token) {
    //if ($token->isCurrent()) {
    //return $token;
    //}
    //}
    //throw new ItemNotFoundException("Customer `{$customer->getId()}` does not have usable tokens");
    //}

    /**
     *
     * @param Customer $customer
     * @param string $cardNumber
     * @return bool
     */
    //public function hasToken(Customer $customer, $cardNumber)
    //{
    //$token = $this->getToken($customer, $cardNumber);
    //return $token ? true : false;
    //}

    /**
     * @param Customer $customer
     * @param RequestRegistration $registration
     * @param RequestDetails $details
     * @param string $token
     * @return Token
     */
    public function mapToken(Customer $customer, RequestRegistration $registration, RequestDetails $details, $token)
    {
        $cardExpiryDate = Date::create('my', $registration->getExpiryDate());
        $tokenEntity = new Token(
            $customer,
            $token,
            $registration->getCardType(),
            $registration->getCardHolder(),
            $registration->getCardNumber(),
            $cardExpiryDate
        );
        $tokenEntity->setIsCurrent(TRUE);
        $this->mapRequestToToken($details, $tokenEntity);
        return $tokenEntity;
    }

    /**
     * @param RequestDetails $details
     * @param Token $token
     */
    public static function mapRequestToToken(RequestDetails $details, Token $token)
    {
        $token->setBillingFirstnames($details->getBillingFirstnames());
        $token->setBillingSurname($details->getBillingSurname());
        $token->setBillingAddress1($details->getBillingAddress1());
        $token->setBillingAddress2($details->getBillingAddress2());
        $token->setBillingCity($details->getBillingCity());
        $token->setBillingCountry($details->getBillingCountry());
        $token->setBillingPostCode($details->getBillingPostCode());
        $token->setBillingState($details->getBillingState());
        $token->setBillingPhone($details->getBillingPhone());
        $token->setDeliveryFirstnames($details->getDeliveryFirstnames());
        $token->setDeliverySurname($details->getDeliverySurname());
        $token->setDeliveryAddress1($details->getDeliveryAddress1());
        $token->setDeliveryAddress2($details->getDeliveryAddress2());
        $token->setDeliveryCity($details->getDeliveryCity());
        $token->setDeliveryCountry($details->getDeliveryCountry());
        $token->setDeliveryPostCode($details->getDeliveryPostCode());
        $token->setDeliveryState($details->getDeliveryState());
        $token->setDeliveryPhone($details->getDeliveryPhone());
    }

    /**
     * @param Token $token
     * @param RequestDetails $details
     */
    public static function mapTokenToRequest(Token $token, RequestDetails $details)
    {
        $details->setBillingFirstnames($token->getBillingFirstnames());
        $details->setBillingSurname($token->getBillingSurname());
        $details->setBillingAddress1($token->getBillingAddress1());
        $details->setBillingAddress2(substr($token->getBillingAddress2(), 0, 40));
        $details->setBillingCity($token->getBillingCity());
        $details->setBillingCountry($token->getBillingCountry());
        $details->setBillingPostCode($token->getBillingPostCode());
        $details->setBillingState($token->getBillingState());
        $details->setBillingPhone($token->getBillingPhone());
        $details->setDeliveryFirstnames($token->getDeliveryFirstnames());
        $details->setDeliverySurname($token->getDeliverySurname());
        $details->setDeliveryAddress1($token->getDeliveryAddress1());
        $details->setDeliveryAddress2($token->getDeliveryAddress2());
        $details->setDeliveryCity($token->getDeliveryCity());
        $details->setDeliveryCountry($token->getDeliveryCountry());
        $details->setDeliveryPostCode($token->getDeliveryPostCode());
        $details->setDeliveryState($token->getDeliveryState());
        $details->setDeliveryPhone($token->getDeliveryPhone());
    }

    /**
     * @return IterableResult
     */
    public function getUsCardsWithNoBillingState()
    {
        return $this->tokenRepository->getUsCardsWithNoBillingState();
    }

    /**
     * @return IterableResult
     */
    public function getAllTokens()
    {
        return $this->tokenRepository->getAllTokens();
    }

    /**
     * @param Customer $customer
     * @param PaymentResponse $paymentResponse
     * @return Token
     */
    public function saveTokenFromResponse(Customer $customer, PaymentResponse $paymentResponse)
    {
        $tokenEntity = new Token(
            $customer,
            $paymentResponse->getSageToken(),
            $paymentResponse->getCardType(),
            $paymentResponse->getCardHolder(),
            $paymentResponse->getCardNumber(),
            $paymentResponse->getCardExpiryDate()
        );
        $tokenEntity->setIsCurrent(TRUE);
        self::mapRequestToToken($paymentResponse->getSageDetails(), $tokenEntity);
        $this->saveTokenWithHistory($tokenEntity, TokenHistory::STATUS_COMPLETE, $customer->getEmail());
        return $tokenEntity;
    }

    /**
     * @param Customer $customer
     * @param PaymentResponse $paymentResponse
     * @return Token|null
     */
    public function processToken(Customer $customer, PaymentResponse $paymentResponse)
    {
        $token = NULL;
        if ($identifier = $paymentResponse->getSageToken()) {
            $token = $this->getTokenByIdentifier($identifier);
        }
        if ($paymentResponse->isRecurring()) {
            if (!$token) {
                return $this->saveTokenFromResponse($customer, $paymentResponse);
            }
        }
        return $token;
    }

    /**
     * @param Token $token
     */
    public function removeEntity(Token $token)
    {
        $this->tokenRepository->removeEntity($token);
    }
}
