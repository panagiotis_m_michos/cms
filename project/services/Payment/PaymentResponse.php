<?php

namespace Services\Payment;

use Entities\Customer;
use Entities\Payment\Token;
use Nette\Object;
use PayPalNVP\Response\GetExpressCheckoutDetailsResponse;
use SagePay\Token\Request\Details;
use SagePay\Token\Response\Summary;
use Utils\Date;
use Utils\Exceptions\InvalidDateException;

/**
 * @deprecated Use PaymentModule\Responses\PaymentResponse
 */
class PaymentResponse extends Object
{
    const RECURRING = TRUE;

    const TYPE_SAGE = 'SAGE';

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var string
     */
    private $typeId;

    /**
     * @var string
     */
    private $cardHolder;

    /**
     * @var string
     */
    private $cardNumber;

    /**
     * @var string
     */
    private $sageVendorTxCode;

    /**
     * @var string
     */
    private $sageVpsTxId;

    /**
     * @var string
     */
    private $sageSecurityKey;

    /**
     * @var string
     */
    private $sageTxAuthNo;

    /**
     * @var string
     */
    private $sageToken;

    /**
     * @var Details;
     */
    private $sageDetails;

    /**
     * @var string
     */
    private $paypalDetails;

    /**
     * @var string
     */
    private $paypalTransactionId;

    /**
     * @var string
     */
    private $androidTransactionId;

    /**
     * @var string
     */
    private $itunesTransactionId;

    /**
     * @var string
     */
    private $error;

    /**
     * is payment recurring
     * @var bool
     */
    private $recurringPayment;

    /**
     * @var string
     */
    private $worldPayTransactionId;

    /**
     * @var Token
     */
    private $token;

    private function __construct()
    {
    }

    /**
     * @param Details $details
     * @param Summary $summary
     * @return PaymentResponse
     * @throws InvalidDateException
     */
    public static function fromDetails(Details $details, Summary $summary)
    {
        $paymentResponse = new self();
        $paymentResponse->setTypeId(self::TYPE_SAGE);
        $paymentResponse->setCardNumber($summary->getCardNumber());
        $paymentResponse->setCardHolder($summary->getCardHolder());
        $paymentResponse->setCardType($summary->getCardType());
        $expiryDate = $summary->getCardExpiryDate();
        if (!empty($expiryDate)) {
            $date = Date::create('my', $expiryDate);
            $date->modify('last day of this month');
            $paymentResponse->setCardExpiryDate($date);
        }
        $paymentResponse->setSageVpsTxId($summary->getVpsTxId());
        $paymentResponse->setSageSecurityKey($summary->getSecurityKey());
        $paymentResponse->setSageTxAuthNo($summary->getTxAuthNo());
        $paymentResponse->setSageVendorTxCode($summary->getVendorTxCode());
        $paymentResponse->setSageToken($summary->getToken());
        $paymentResponse->setSageDetails($details);
        if ($details->getStoreToken() == 1) {
            $paymentResponse->setRecurringPayment(self::RECURRING);
        }
        return $paymentResponse;
    }

    /**
     * @return PaymentResponse
     */
    public static function createEmpty()
    {
        return new self();
    }


    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @return string
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     * @param string $typeId
     */
    public function setTypeId($typeId)
    {
        $this->typeId = $typeId;
    }

    /**
     * @return string
     */
    public function getCardHolder()
    {
        return $this->cardHolder;
    }

    /**
     * @param string $typeId
     */
    public function setCardHolder($cardHolder)
    {
        $this->cardHolder = $cardHolder;
    }

    /**
     * @return Date
     */
    public function getCardExpiryDate() 
    {
        return $this->cardExpiryDate;
    }

    /**
     * @param Date $cardExpiryDate
     */
    public function setCardExpiryDate($cardExpiryDate) 
    {
        $this->cardExpiryDate = $cardExpiryDate;
    }
    
    /**
     * @return string
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * @param string $typeId
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;
    }

    /**
     * @return string
     */
    public function getCardType()
    {
        return $this->cardType;
    }

    /**
     * @param string $cardType
     */
    public function setCardType($cardType)
    {
        $this->cardType = $cardType;
    }

    /**
     * @return string
     */
    public function getSageVendorTxCode()
    {
        return $this->sageVendorTxCode;
    }

    /**
     * @param string $typeId
     */
    public function setSageVendorTxCode($sageVendorTxCode)
    {
        $this->sageVendorTxCode = $sageVendorTxCode;
    }

    /**
     * @return string
     */
    public function getSageVpsTxId()
    {
        return $this->sageVpsTxId;
    }

    /**
     * @param string $typeId
     */
    public function setSageVpsTxId($sageVpsTxId)
    {
        $this->sageVpsTxId = $sageVpsTxId;
    }

    /**
     * @return string
     */
    public function getSageSecurityKey()
    {
        return $this->sageSecurityKey;
    }

    /**
     * @param string $typeId
     */
    public function setSageSecurityKey($sageSecurityKey)
    {
        $this->sageSecurityKey = $sageSecurityKey;
    }

    /**
     * @return string
     */
    public function getSageTxAuthNo()
    {
        return $this->sageTxAuthNo;
    }

    /**
     * @param string $typeId
     */
    public function setSageTxAuthNo($sageTxAuthNo)
    {
        $this->sageTxAuthNo = $sageTxAuthNo;
    }

    /**
     * @return string
     */
    public function getSageToken()
    {
        return $this->sageToken;
    }

    /**
     * @param string $typeId
     */
    public function setSageToken($sageToken)
    {
        $this->sageToken = $sageToken;
    }

    /**
     * @return Details
     */
    public function getSageDetails()
    {
        return $this->sageDetails;
    }

    /**
     * @param Details $sageDetails
     */
    public function setSageDetails($sageDetails)
    {
        $this->sageDetails = $sageDetails;
    }

    /**
     * @return string
     */
    public function getPaypalDetails()
    {
        return $this->paypalDetails;
    }

    /**
     * @param string $paypalDetails
     */
    public function setPaypalDetails($paypalDetails)
    {
        $this->paypalDetails = $paypalDetails;
    }

    /**
     * @return string
     */
    public function getPaypalTransactionId()
    {
        return $this->paypalTransactionId;
    }

    /**
     * @param string $typeId
     */
    public function setPaypalTransactionId($paypalTransactionId)
    {
        $this->paypalTransactionId = $paypalTransactionId;
    }

    public function getItunesTransactionId()
    {
        return $this->itunesTransactionId;
    }

    /**
     * @param string $itunesTransactionId
     */
    public function setItunesTransactionId($itunesTransactionId)
    {
        $this->itunesTransactionId = $itunesTransactionId;
    }

    public function getAndroidTransactionId()
    {
        return $this->androidTransactionId;
    }

    /**
     * @param string $androidTransactionId
     */
    public function setAndroidTransactionId($androidTransactionId)
    {
        $this->androidTransactionId = $androidTransactionId;
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param string $typeId
     */
    public function setError($error)
    {
        $this->error = $error;
    }

    /**
     * @param bool $recurringPayment
     */
    public function setRecurringPayment($recurringPayment)
    {
        $this->recurringPayment = $recurringPayment;
    }

    /**
     * @return boolean
     */
    public function isRecurring()
    {
        return $this->recurringPayment === self::RECURRING;
    }

    /**
     * @return Token
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param Token $token
     */
    public function setToken(Token $token)
    {
        $this->token = $token;
    }
}
