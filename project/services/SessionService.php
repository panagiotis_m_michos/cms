<?php

namespace Services;

use Customer;
use Exception;
use InvalidArgumentException;
use Repositories\CustomerRepository;
use Entities\Customer as CustomerEntity;
use SessionModule\ISession;
use UserModule\Contracts\ICustomer;
use UserModule\Domain\Credentials;
use UserModule\Exceptions\CustomerAvailabilityException;
use UserModule\Exceptions\LoginException;
use UserModule\Services\IAuthenticator;

class SessionService implements IAuthenticator
{
    /**
     * @var CustomerRepository
     */
    private $customerRepository;

    /**
     * @var ISession
     */
    private $session;

    /**
     * @param CustomerRepository $customerRepository
     * @param ISession $session
     */
    public function __construct(CustomerRepository $customerRepository, ISession $session)
    {
        $this->customerRepository = $customerRepository;
        $this->session = $session;
    }

    /**
     * @return CustomerEntity
     * @throws InvalidArgumentException
     */
    public function getLoggedInCustomer()
    {
        $customer = $this->getLoggedInCustomerOrNull();
        if (!$customer) {
            throw new InvalidArgumentException(sprintf('Customer is not logged in'));
        }
        return $customer;
    }

    /**
     * @return NULL|CustomerEntity
     */
    public function getLoggedInCustomerOrNull()
    {
        $oldCustomer = Customer::getSignedIn();
        return $this->customerRepository->find($oldCustomer->getId());
    }

    /**
     * login customer or use email to save temporary customer
     *
     * @param Credentials $credentials
     * @return ICustomer
     * @throws CustomerAvailabilityException
     * @throws LoginException
     */
    public function useCredentials(Credentials $credentials)
    {
        $this->session->remove(IAuthenticator::TEMPORARY_CUSTOMER_KEY);
        if (!$this->customerRepository->isEmailTaken($credentials->getUsername())) {
            $customer = CustomerEntity::temporary($credentials->getUsername());
            $this->session->set(IAuthenticator::TEMPORARY_CUSTOMER_KEY, $customer);
            return $customer;
        }
        if (!$credentials->getPassword()) {
            throw CustomerAvailabilityException::emailTaken($credentials->getUsername());
        }
        
        try {
            $oldCustomer = Customer::doAuthenticate($credentials->getUsername(), $credentials->getPassword());
        } catch (Exception $e) {
            throw LoginException::fromNumeric($e);
        }
        Customer::signIn($oldCustomer->getId());
        return $this->customerRepository->find($oldCustomer->getId());

    }
}
