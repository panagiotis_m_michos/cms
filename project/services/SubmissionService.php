<?php

namespace Services;

use CHFillingFactory;
use Company;
use CompanyHouse\Filling\Responses\Internal\Rejection;
use CompanyHouse\Filling\Responses\SubmissionStatus;
use CompanyIncorporation;
use DiLocator;
use Dispatcher\Events\CompanyEvent;
use Dispatcher\Events\CompanyHouse\StatusEvent;
use Doctrine\ORM\EntityManager;
use Entities\Company as CompanyEntity;
use Entities\CompanyHouse\FormSubmission;
use Entities\CompanyHouse\FormSubmissionError;
use EventLocator;
use Exceptions\Business\CompanyIncorporationException;
use Exceptions\Business\DocumentKeyMissingException;
use Exceptions\Technical\FormSubmissionNotFoundException;
use Exceptions\Technical\FormSubmissionNotResubmittableException;
use FormSubmission as OldFormSubmission;
use Nette\Object;
use Registry;
use Repositories\CompanyHouse\FormSubmission\AnnualReturnRepository;
use Repositories\CompanyHouse\FormSubmissionErrorRepository;
use Repositories\CompanyHouse\FormSubmissionRepository;
use Symfony\Component\EventDispatcher\EventDispatcher;

class SubmissionService extends Object
{

    /**
     * @var FormSubmissionRepository
     */
    private $formSubmissionRepository;

    /**
     * @var FormSubmissionErrorRepository
     */
    private $formSubmissionErrorRepository;

    /**
     * @var AnnualReturnRepository
     */
    private $annualReturnRepository;

    /**
     * @var EventDispatcher
     */
    private $eventDispatcher;

    /**
     * @param FormSubmissionRepository $formSubmissionRepository
     * @param FormSubmissionErrorRepository $formSubmissionErrorRepository
     * @param AnnualReturnRepository $annualReturnRepository
     * @param EventDispatcher $eventDispatcher
     */
    public function __construct(
        FormSubmissionRepository $formSubmissionRepository,
        FormSubmissionErrorRepository $formSubmissionErrorRepository,
        AnnualReturnRepository $annualReturnRepository,
        EventDispatcher $eventDispatcher
    )
    {
        // we need to set static chfilling
        CHFillingFactory::setCHFilling();
        $this->formSubmissionRepository = $formSubmissionRepository;
        $this->formSubmissionErrorRepository = $formSubmissionErrorRepository;
        $this->annualReturnRepository = $annualReturnRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param Company $companyEntity
     * @return array
     */
    public function getCompanyFormSubmissions(CompanyEntity $companyEntity)
    {
        //get old Company from CompanyEntity
        $company = Company::getCompany($companyEntity->getCompanyId());

        $formSubmissionIds = OldFormSubmission::getCompanyFormSubmissionsIds($company->getCompanyId());
        if (count($formSubmissionIds) == 0) {
            return [];
        }

        $formSubmision = [];
        foreach ($formSubmissionIds as $formSubmissionId) {
            $formSubmision[] = OldFormSubmission::getFormSubmission($company, $formSubmissionId);
        }

        return $formSubmision;
    }

    /**
     * @param OldFormSubmission $formSubmision
     */
    public function sendFormSubmission(OldFormSubmission $formSubmision)
    {
        $formSubmision->sendRequestImproved();
    }

    /**
     * @param CompanyEntity $companyEntity
     * @param int $formSubmissionId
     * @return OldFormSubmission
     */
    public function getOldFormSubmission(CompanyEntity $companyEntity, $formSubmissionId)
    {
        $company = Company::getCompany($companyEntity->getCompanyId());

        return OldFormSubmission::getFormSubmission($company, $formSubmissionId);
    }

    /**
     * @param int $formSubmissionId
     * @return FormSubmission
     */
    public function getFormSubmissionById($formSubmissionId)
    {
        return $this->formSubmissionRepository->findOneBy(['formSubmissionId' => $formSubmissionId]);
    }

    /**
     * @param CompanyEntity $companyEntity
     * @return NULL|FormSubmission
     */
    public function companyHasUnsyncedAnualReturn(CompanyEntity $companyEntity)
    {
        return $this->annualReturnRepository->findOneBy(
            [
                'company' => $companyEntity,
                'response' => NULL,
            ]
        );
    }

    /**
     * @param FormSubmission $formSubmission
     */
    public function saveFormSubmission(FormSubmission $formSubmission)
    {
        $this->formSubmissionRepository->saveEntity($formSubmission);
    }

    /**
     * @TODO remove options
     * @param SubmissionStatus $submissionStatus
     * @return array
     */
    public function processSubmissionStatus(SubmissionStatus $submissionStatus)
    {
        $formSubmission = $this->getFormSubmissionById($submissionStatus->getSubmissionNumber());

        $statusEvent = new StatusEvent($formSubmission, $submissionStatus);
        if ($statusEvent->hasEqualStatus()) {
            return;
        }

        //compatability for returned array
        $company = $formSubmission->getCompany();
        $statusEvent->setOption('customerId', $company->getCustomer()->getCustomerId());
        $statusEvent->setOption('companyId', $company->getId());
        $statusEvent->setOption('companyName', $company->getCompanyName());
        $statusEvent->setOption('status', $submissionStatus->getStatus());
        $statusEvent->setOption('attachementPath', NULL);
        $statusEvent->setOption('type', $formSubmission->getFormIdentifier());
        $statusEvent->setOption('description', implode(' ', $submissionStatus->getRejectionDescriptions()));

        $this->eventDispatcher->dispatch(
            strtolower(
                'events.companyhouse.' . $submissionStatus->getStatus() . '.' . $formSubmission->getFormIdentifier()
            ),
            $statusEvent
        );

        // update form submission
        $examiner = $submissionStatus->getExaminer();
        if ($examiner) {
            $formSubmission->setExaminerTelephone($examiner->getTelephone());
            $formSubmission->setExaminerComment($examiner->getComment());
        }
        $formSubmission->setResponse($submissionStatus->getStatus());
        $this->saveFormSubmission($formSubmission);

        $this->eventDispatcher->dispatch(
            strtolower('events.companyhouse.' . $submissionStatus->getStatus()),
            $statusEvent
        );

        return $statusEvent->getOptions();
    }

    /****** Company house response handling ****/

    /**
     * @TODO remove arrays
     * @param FormSubmission $formSubmision
     * @param SubmissionStatus $submissionStatus
     * @return array
     * @throws DocumentKeyMissingException
     * @throws CompanyIncorporationException
     */
    public function acceptChangeOfName(
        FormSubmission $formSubmision,
        SubmissionStatus $submissionStatus,
        $getDocument = NULL
    )
    {
        $changeOfNameDetails = $submissionStatus->getChangeOfNameDetails();
        /* no document */
        if (!$changeOfNameDetails || !$changeOfNameDetails->getDocRequestKey()) {
            throw new DocumentKeyMissingException(
                sprintf('Document key is not provided for company number `%s`', $submissionStatus->getCompanyNumber())
            );
        }

        $data = [];
        $data['formSubmissionId'] = $formSubmision->getId();
        $data['requestKey'] = $changeOfNameDetails->getDocRequestKey();
        $data['companyNumber'] = $submissionStatus->getCompanyNumber();

        if ($getDocument == NULL) {
            $getDocument = OldFormSubmission::setAcceptChangeOfName($data);
        }

        /* no attachement path */
        if (empty($getDocument)) {
            throw new CompanyIncorporationException(
                sprintf('Company `%s` incorporation failed', $submissionStatus->getCompanyNumber())
            );
        }

        return array_merge($data, $getDocument);
    }

    /**
     * @TODO remove arrays
     * @param FormSubmission $formSubmision
     * @param SubmissionStatus $submissionStatus
     * @return array
     * @throws DocumentKeyMissingException
     * @throws CompanyIncorporationException
     */
    public function acceptCompanyIncorporation(
        FormSubmission $formSubmision,
        SubmissionStatus $submissionStatus,
        $getDocument = NULL
    )
    {
        $incorporationDetails = $submissionStatus->getIncorporationDetails();
        /* no document */
        if (!$incorporationDetails || !$incorporationDetails->getDocRequestKey()) {
            throw new DocumentKeyMissingException(
                sprintf('Document key is not provided for company number `%s`', $submissionStatus->getCompanyNumber())
            );
        }

        $data = [];
        $data['formSubmissionId'] = $formSubmision->getId();
        $data['requestKey'] = $incorporationDetails->getDocRequestKey();
        $data['incorporationDate'] = $incorporationDetails->getIncorporationDate();
        $data['authenticationCode'] = $incorporationDetails->getAuthenticationCode();
        $data['companyNumber'] = $submissionStatus->getCompanyNumber();

        if ($getDocument == NULL) {
            $getDocument = OldFormSubmission::setAcceptCompanyIncorporation($data);
        }

        /* no attachement path */
        if (empty($getDocument)) {
            throw new CompanyIncorporationException(
                sprintf('Company `%s` incorporation failed', $submissionStatus->getCompanyNumber())
            );
        }

        $company = $formSubmision->getCompany();
        /** @var EntityManager $entityManager */
        $entityManager = Registry::$container->get(DiLocator::ENTITY_MANAGER);
        $entityManager->refresh($company);

        $this->eventDispatcher->dispatch(
            EventLocator::COMPANY_INCORPORATED,
            new CompanyEvent($company)
        );

        return array_merge($data, $getDocument);
    }

    /**
     * @TODO remove arrays
     * @param FormSubmission $formSubmission
     * @param SubmissionStatus $submissionStatus
     * @return array
     * @throws DocumentKeyMissingException
     * @throws CompanyIncorporationException
     */
    public function rejectCompanyIncorporation(FormSubmission $formSubmission, SubmissionStatus $submissionStatus)
    {
        $newFormSubmissionId = OldFormSubmission::setRejectCompanyIncorporation(
            $formSubmission->getId(),
            $formSubmission->getCompany()->getCompanyId()
        );
        $rejectReference = $submissionStatus->getRejectReference();
        $rejectDescription = implode('; ', $submissionStatus->getRejectionDescriptions());

        // --- create new company incorporation ---
        CompanyIncorporation::createRejectCompanyIncorporation(
            $newFormSubmissionId,
            $formSubmission->getId(),
            $rejectReference,
            $rejectDescription
        );
    }

    /**
     * @param FormSubmission $formSubmission
     * @param Rejection $rejection
     * @return FormSubmissionError
     */
    public function createFormSubmissionError(FormSubmission $formSubmission, Rejection $rejection)
    {
        $entity = new FormSubmissionError($formSubmission, $rejection->getCode(), $rejection->getDescription());
        $entity->setInstanceNumber($rejection->getInstanceNumber());

        return $entity;
    }

    /**
     * @param FormSubmissionError $formSubmissionError
     */
    public function saveFormSubmissionError(FormSubmissionError $formSubmissionError)
    {
        $this->formSubmissionErrorRepository->saveEntity($formSubmissionError);
    }

    /**
     * @param int $formSubmissionId
     * @return FormSubmission
     * @throws FormSubmissionNotFoundException
     * @throws FormSubmissionNotResubmittableException
     */
    public function resubmitFormSubmissionById($formSubmissionId)
    {
        $formSubmission = $this->getFormSubmissionById($formSubmissionId);
        if (!$formSubmission) {
            throw new FormSubmissionNotFoundException($formSubmissionId);
        }

        if ($this->isFormSubmissionResubmittable($formSubmission)) {
            $this->resubmitFormSubmission($formSubmission);
        } else {
            throw FormSubmissionNotResubmittableException::generic($formSubmission);
        }
    }

    /**
     * @param FormSubmission $formSubmission
     * @return bool
     */
    private function isFormSubmissionResubmittable(FormSubmission $formSubmission)
    {
        if ($formSubmission->isCancelled()) {
            return FALSE;
        }

        return ($formSubmission->isPending() || $formSubmission->isInternalFailure() || $formSubmission->isError());
    }

    /**
     * @param FormSubmission $formSubmission
     */
    private function resubmitFormSubmission(FormSubmission $formSubmission)
    {
        $clonedSubmission = $this->getClonedFormSubmissionToResubmit($formSubmission);
        $this->saveFormSubmission($clonedSubmission);
        $this->markFormSubmissionAsCancelled($formSubmission);
        $this->sendFormSubmissionEntity($clonedSubmission);
    }

    /**
     * @param FormSubmission $formSubmission
     * @return FormSubmission
     */
    private function getClonedFormSubmissionToResubmit(FormSubmission $formSubmission)
    {
        $clonedSubmission = clone $formSubmission;
        $clonedSubmission->unsetResponse();

        return $clonedSubmission;
    }

    /**
     * @param FormSubmission $formSubmission
     */
    private function markFormSubmissionAsCancelled(FormSubmission $formSubmission)
    {
        $formSubmission->markAsCancelled();
        $this->formSubmissionRepository->flush($formSubmission);
    }

    /**
     * @param FormSubmission $clonedSubmission
     */
    private function sendFormSubmissionEntity(FormSubmission $clonedSubmission)
    {
        $formRequest = $this->getOldFormSubmission($clonedSubmission->getCompany(), $clonedSubmission->getId());
        $formRequest->sendRequestImproved();
    }
}
