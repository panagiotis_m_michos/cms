<?php

namespace Services;

use CompanyDocumentStorage;
use Entities\Company;
use Entities\CompanyDocument;
use EntityNotFound;
use Exceptions\Business\Forbidden;
use InvalidArgumentException;
use Nette\Object;
use Repositories\CompanyDocumentRepository;

class CompanyDocumentService extends Object
{
    /**
     * @var CompanyDocumentRepository
     */
    private $repository;

    /**
     * @var CompanyDocumentStorage
     */
    private $companyDocumentStorage;

    /**
     * @param CompanyDocumentRepository $repository
     * @param CompanyDocumentStorage $companyDocumentStorage
     */
    public function __construct(CompanyDocumentRepository $repository, CompanyDocumentStorage $companyDocumentStorage)
    {
        $this->repository = $repository;
        $this->companyDocumentStorage = $companyDocumentStorage;
    }

    /**
     * @param $id
     * @return CompanyDocument
     * @throws InvalidArgumentException
     */
    public function getCompanyDocumentById($id)
    {
        if (!$id) {
            throw new InvalidArgumentException("Missing company document id");
        }
        return $this->repository->find($id);
    }

    /**
     * @param Company $company
     * @param $companyDocumentId
     * @return CompanyDocument
     * @throws EntityNotFound
     * @throws Forbidden
     */
    public function getCompanyDocument(Company $company, $companyDocumentId)
    {
        $companyDocument = $this->getCompanyDocumentById($companyDocumentId);
        if (!$companyDocument) {
            throw new EntityNotFound("Company document `$companyDocumentId` doesn't exist");
        }
        if (!$companyDocument->getCompany()->isEqual($company)) {
            throw new Forbidden(sprintf("Company document entity `%s` doesn't belong to company id `%s`", $companyDocumentId, $company->getId()));
        }
        return $companyDocument;
    }

    /**
     * @param CompanyDocument $companyDocument
     */
    public function deleteCompanyDocument(CompanyDocument $companyDocument)
    {
        $this->companyDocumentStorage->deleteDocument($companyDocument);
        $this->repository->removeEntity($companyDocument);
    }

    /**
     * @param CompanyDocument $entity
     */
    public function saveCompanyDocument(CompanyDocument $entity)
    {
        $this->repository->saveEntity($entity);
    }

    /**
     * @param CompanyDocument $companyDocument
     */
    public function outputCompanyDocument(CompanyDocument $companyDocument)
    {
        $content = $this->companyDocumentStorage->getDocumentContent($companyDocument);

        header("Cache-Control: private, max-age=2");
        header("Pragma: private");
        header("Content-Description: File Transfer");
        header("Content-Transfer-Encoding: binary");
        header("Content-Type: application/octet-stream");
        header("Content-Length: " . strlen($content));
        header("Content-Disposition:attachment; filename=\"" . $companyDocument->getRealFileName() . "\"");
        echo($content);
        exit;
    }
}
