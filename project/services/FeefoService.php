<?php

namespace Services;

use Doctrine\ORM\Internal\Hydration\IterableResult;
use Entities\Feefo;
use Entities\Order;
use Nette\Object;
use Repositories\FeefoRepository;

class FeefoService extends Object
{
    /**
     * @var FeefoRepository
     */
    private $repository;

    /**
     * @param FeefoRepository $repository
     */
    public function __construct(FeefoRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Order $order
     */
    public function addFeefo(Order $order)
    {
        $feefo = new Feefo();
        $feefo->setOrder($order);
        $this->saveFeefo($feefo);
    }

    /**
     * @param Feefo $feefo
     */
    public function saveFeefo(Feefo $feefo)
    {
        $this->repository->saveEntity($feefo);
    }

    /**
     * @return IterableResult
     */
    public function getWaitingOrders()
    {
        return $this->repository->getWaitingOrders();
    }

    /**
     * @return IterableResult
     */
    public function getEligibleOrders()
    {
        return $this->repository->getEligibleOrders();
    }

    /**
     * @param Order $order
     * @return Feefo
     */
    public function getFeefoByOrder(Order $order)
    {
        return $this->repository->findOneBy(array('order' => $order));
    }
}
