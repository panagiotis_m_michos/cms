<?php

use Entities\Customer as CustomerEntity;
use Entities\Company as CompanyEntity;

class CompanyEmailer extends EmailerAbstract
{

    const EMAIL_INCORPORATED_DOCUMENTS = 577;
    const EMAIL_SHARE_CERTIFICATES = 730;
    const EMAIL_SUBMISSION_SUCCESS = 496;
    const EMAIL_SUBMISION_ERROR = 680;
    const ACCEPTED_EMAIL_LLP = 1144;
    const ACCEPTED_EMAIL = 501;
    const ACCEPTED_EMAIL_LP_ADVERTY = 1365;
    const RESERVE_ACCEPTED_EMAIL = 891;
    const RESERVE_REJECTED_EMAIL = 890;
    const REJECTED_EMAIL = 500;

    /**
     * @param Customer $customer
     * @param type $certificates
     */
    public function sendShareCertificates(Customer $customer, $certificates)
    {
        // email
        $email = new FEmail(self::EMAIL_SHARE_CERTIFICATES);
        $email->setTo($customer->email);
        // replace
        $replace = array();
        $replace['[FIRSTNAME]'] = $customer->firstName;
        $email->replacePlaceHolders($replace);

        $x = 1;
        foreach ($certificates as $v) {
            $email->addAttachment('share_cert' . $x++ . '.pdf', $v->getPdfCertificate('cert', 'S'));
        }
        $this->emailService->send($email, $customer);
    }

    /**
     * @param Customer $customer
     * @param Company $company
     * @param type $documents
     */
    public function sendIncorporatedDocuments(Customer $customer, Company $company, $documents)
    {
        // email
        $email = new FEmail(self::EMAIL_INCORPORATED_DOCUMENTS);
        $email->setTo($customer->email);
        // replace
        $replace = array();
        $replace['[FIRSTNAME]'] = $customer->firstName;
        $email->replacePlaceHolders($replace);
        // attach documents
        foreach ($documents as $key => $document) {
            // get and change file name
            if ($document == 'ModelArticles') {
                $file = 'model_articles.pdf';
            } elseif ($document == 'article.pdf' && in_array('ModelArticles', $documents)) {
                $file = 'memorandum.pdf';
            } else {
                $file = $document;
            }
            $content = $company->getDocument($document, FALSE);
            $email->addAttachment($file, $content);
        }
        $this->emailService->send($email, $customer);
    }

    /**
     * @param Customer $customer
     * @param Company $company
     * @param type $docPath
     */
    public function sendIncorporatedCompany(Customer $customer, Company $company, $docPath)
    {
        // email
        if ($company->getType() == 'LLP') {
            $email = new FEmail(self::ACCEPTED_EMAIL_LLP);
        } else {
            $email = new FEmail(self::ACCEPTED_EMAIL);
        }
        $email->setTo($customer->email);
        // replace
        $replace = array();
        $replace['[FIRSTNAME]'] = $customer->firstName;
        $replace['[COMPANY_NAME]'] = $company->getCompanyName();
        $replace['[COMPANY_NUMBER]'] = $company->getCompanyNumber();
        $replace['[DATE]'] = $company->getIncorporationDate();
        $email->replacePlaceHolders($replace);
        $email->addAttachment($docPath . 'IncorporationCertificate.pdf');
        $this->emailService->send($email, $customer);
    }

    /**
     * @param CustomerEntity $customer
     * @param CompanyEntity $company
     */
    public function sendSubmisionError(CustomerEntity $customer, CompanyEntity $company)
    {
        // temporary logging to investigate receiving CH Error emails for old company
        if ($company->getId() == 440562) {
            /** @var \Psr\Log\LoggerInterface $logger */
            $logger = Registry::$container->get(\ErrorModule\Ext\DebugExt::LOGGER);
            $logger->error('TEMP LOGGING: sendSubmissionError sent', ['backtrace' => debug_backtrace(), 'customer' => $customer->getId(), 'company' => $company->getId()]);
        }

        $email = new FEmail(self::EMAIL_SUBMISION_ERROR);
        $email->replacePlaceHolders(
            array(
                '[FIRSTNAME]' => $customer->getFirstName(),
                '[COMPANY_NAME]' => $company->getCompanyName(),
                '[COMPANY_ID]' => $company->getCompanyId()
            )
        );
        $this->emailService->send($email, $customer);
    }

    /**
     * @param CustomerEntity $customer
     * @param CompanyEntity $company
     */
    public function sendSubmisionSuccess(CustomerEntity $customer, CompanyEntity $company)
    {
        $email = new FEmail(self::EMAIL_SUBMISSION_SUCCESS);
        $email->addTo($customer->getEmail());
        $email->replacePlaceHolders(
            array(
                '[FIRSTNAME]' => $customer->getFirstName(),
                '[COMPANY_NAME]' => $company->getCompanyName()
            )
        );
        $this->emailService->send($email, $customer);
    }

    /**
     * @param Customer $customer
     * @param Company $company
     * @param array $emailPlaceholders
     * @param array $chData
     * @param type $LPAdd
     * @param type $reserv
     */
    public function sendCompanyIncorporationAccepted(Customer $customer, Company $company, array $emailPlaceholders, array $chData, $LPAdd, $reserv)
    {
        //LLP check
        if ($company->getType() == 'LLP') {
            $email = new FEmail(self::ACCEPTED_EMAIL_LLP);
        } elseif ($LPAdd == Package::PACKAGE_ULTIMATE || $LPAdd == PollingModel::MAIL_FORWARDING) {
            $email = new FEmail(self::ACCEPTED_EMAIL);
        } else {
            $email = new FEmail(self::ACCEPTED_EMAIL_LP_ADVERTY);
        }

        $email->addTo($customer->email);
        $email->replacePlaceHolders($emailPlaceholders);

        // don't add attachment for reserve package
        if ($reserv != Package::PACKAGE_RESERVE_COMPANY_NAME) {
            $email->addAttachment($chData['attachmentPath']);
        }
        $this->emailService->send($email, $customer);
    }

    /**
     * @param Customer $customer
     * @param array $replace
     */
    public function sendCompanyIncorporationRejected(Customer $customer, array $replace)
    {
        $email = new FEmail(self::REJECTED_EMAIL);
        $email->addTo($customer->email);
        $email->replacePlaceHolders($replace);
        $this->emailService->send($email, $customer);
    }

    /**
     * @param Customer $customer
     * @param array $replace
     */
    public function sendCompanyIncorporationReservedAccepted(Customer $customer, array $replace)
    {
        $email = new FEmail(self::RESERVE_ACCEPTED_EMAIL);
        $email->addTo($customer->email);
        $email->replacePlaceHolders($replace);
        $this->emailService->send($email, $customer);
    }

    /**
     * @param Customer $customer
     * @param array $replace
     */
    public function sendCompanyIncorporationReservedRejected(Customer $customer, array $replace)
    {
        $email = new FEmail(self::RESERVE_REJECTED_EMAIL);
        $email->addTo($customer->email);
        $email->replacePlaceHolders($replace);
        $this->emailService->send($email, $customer);
    }

}
