<?php

class GoogleEmailer extends EmailerAbstract
{

    const GOOGLE_ADWORDS_EMAIL = 896;
    const GOOGLE_VOUCHER_EMAIL = 564;

    /**
     * @param array $data
     */
    public function googleVoucherEmail(array $data)
    {
        $email = new FEmail(self::GOOGLE_VOUCHER_EMAIL);
        $email->from = trim($data['customer_email']);
        $replace = array();
        $replace['[NAME]'] = $data['customer_name'];
        $replace['[EMAIL]'] = $data['customer_email'];
        $replace['[COMPANY_NAME]'] = $data['company_name'];

        $email->replacePlaceHolders($replace);

        $this->emailService->send($email);
    }

    /**
     * @param array $data
     */
    public function googleAdwordsEmail(array $data)
    {
        $replace = array();
        $replace['[FULLNAME]'] = $data['fullName'];
        $replace['[COMPANY_NAME]'] = $data['company_name'];
        $replace['[EMAIL]'] = $data['email'];
        $replace['[PHONE]'] = $data['phone'];

        $email = new FEmail(self::GOOGLE_ADWORDS_EMAIL);
        $email->replacePlaceHolders($replace);

        $this->emailService->send($email);
    }

}

