<?php

class TaxAssistAccountsEmailer extends EmailerAbstract
{
    const SERVICE_EMAIL = 611;
    const ACCOUNT_EMAIL = 804;
    
    /**
     * @param array $data
     */
    public function serviceConsultationEmail(array $data)
    {
        $email = new FEmail(self::SERVICE_EMAIL);
        $email->replacePlaceHolders(
            array(
            '[FULLNAME]' => $data['fullName'],
            '[EMAIL]' => $data['email'],
            '[PHONE]' => $data['phone'],
            '[POSTCODE]' => $data['postcode'],
            '[COMPANY_NAME]' => $data['companyName']
            )
        );
        $this->emailService->send($email);
    }

    /**
     * @param array $data
     */
    public function accountsConsultationEmail(array $data)
    {
        $email = new FEmail(self::ACCOUNT_EMAIL);
        $email->replacePlaceHolders(
            array(
            '[FULLNAME]' => $data['fullName'],
            '[EMAIL]' => $data['email'],
            '[PHONE]' => $data['phone'],
            '[POSTCODE]' => $data['postcode'],
            '[COMPANY_NAME]' => $data['companyName'],
            '[DETAILS]' => $data['details'],
            )
        );

        $this->emailService->send($email);
    }

}

