<?php

class CosecEmailer extends EmailerAbstract
{
    const WHOLESALE_REGISTRATION_EMAIL = 711;
    const CONTACT_EMAIL = 768; 
    
    /**
     * @param array $data
     */
    public function contactUsEmail(array $data)
    {
        $email = new FEmail(self::CONTACT_EMAIL);
        $email->from = $data['customer_email'];
        $email->setTo('support@companiesmadesimple.com');
        $email->replacePlaceHolders(
            array(
            '[CUSTOMER_NAME]' => $data['customer_name'],
            '[EMAIL]' => $data['customer_email'],
            '[PHONE]' => $data['contact_phone'],
            '[COMPANY]' => $data['company_name'],
            '[MESSAGE]' => $data['message'],
            )
        );
        $this->emailService->send($email);
    }

    /**
     * @param String $link
     * @param Customer $customer
     */
    public function forgottenPasswordEmail($link, Customer $customer)
    {
        $replace = array();
        $replace['[FIRSTNAME]'] = $customer->firstName;
        $replace['[EMAIL]'] = $customer->email;
        $replace['[PASSWORD]'] = $customer->password;

        $replace['[URL]'] = $link;

        $email = new FEmail(UserEmailer::FORGOTTEN_PASSWORD_EMAIL);
        $email->replacePlaceHolders($replace);
        $email->addTo($customer->email);

        $this->emailService->send($email, $customer);
    }
    
    /**
     * @param String $password
     * @param Customer $customer
     */
    public function signupEmail($password, Customer $customer)
    {

        $email = new FEmail(self::WHOLESALE_REGISTRATION_EMAIL);
        $email->addTo($customer->email);
        $email->replacePlaceHolders(
            array(
            '[FIRST_NAME]' => $customer->firstName,
            '[EMAIL]' => $customer->email,
            '[PASSWORD]' => $password,
            )
        );

        $this->emailService->send($email, $customer);
    }

}

