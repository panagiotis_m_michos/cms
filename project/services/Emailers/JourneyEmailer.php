<?php

use OfferModule\Entities\CustomerDetails;

class JourneyEmailer extends EmailerAbstract
{

    const ERROR_EMAIL = 572;
    const INTERNAL_JOURNEY_EMAIL = 145;
    const JOURNEY_EMAIL = 534;
    const CSV_EMAIL = 724;

    /**
     * @param JourneyCustomerProduct $product
     * @param JourneyCustomer $journeyCustomer
     */
    public function journeyProductEmail(JourneyCustomerProduct $product, JourneyCustomer $journeyCustomer)
    {
        // replace place holders
        $replace = array();
        $replace['[FULLNAME]'] = $journeyCustomer->fullName;
        $replace['[EMAIL]'] = $journeyCustomer->email;
        $replace['[PHONE]'] = $journeyCustomer->phone;
        $replace['[POSTCODE]'] = $journeyCustomer->postcode;
        $replace['[COMPANY_NAME]'] = $journeyCustomer->companyName;

        //get question per product
        $questions = JourneyCustomerProductQuestion::getAllObjects(NULL, NULL, array('journeyCustomerProductId' => $product->getId()));
        $questionsText = '';
        $htmlProduct = Html::el('p');
        foreach ($questions as $question) {
            $questionsText = $questionsText . $htmlProduct->create('p')->setHtml($question->question . ': ' . $question->answer);
        }
        $replace['[QUESTIONS]'] = $questionsText;

        $jorneyproduct = new JourneyProduct($product->productId);
        // product
        $htmlProduct = Html::el('p');
        $htmlProduct->create('h4')->setText($jorneyproduct->getLngTitle());
        if (!empty($jorneyproduct->emailText)) {
            $htmlProduct->create('p')->setHtml($jorneyproduct->emailText);
        }
        $replace['[PRODUCTS]'] = $htmlProduct;
        $title = substr($jorneyproduct->getLngSeoTitle(), 0, 40);
        $replaceSubject['[TITLE]'] = $title;

        $email = new FEmail(self::JOURNEY_EMAIL);
        $email->addTo($journeyCustomer->email);
        $email->subject = str_replace(array_keys($replaceSubject), $replaceSubject, $email->subject);
        $email->replacePlaceHolders($replace);

        $this->emailService->send($email);
    }

    /**
     * @param JourneyCustomerProduct $product
     * @param JourneyCustomer $journeyCustomer
     */
    public function internalJorneyProductEmail(JourneyCustomerProduct $product, JourneyCustomer $journeyCustomer)
    {
        // replace place holders
        $replace = array();
        $replace['[FULLNAME]'] = $journeyCustomer->fullName;
        $replace['[EMAIL]'] = $journeyCustomer->email;
        $replace['[PHONE]'] = $journeyCustomer->phone;
        $replace['[POSTCODE]'] = $journeyCustomer->postcode;
        $replace['[COMPANY_NAME]'] = $journeyCustomer->companyName;

        //get question per product
        $questions = JourneyCustomerProductQuestion::getAllObjects(NULL, NULL, array('journeyCustomerProductId' => $product->getId()));
        $questionsText = '';
        $htmlProduct = Html::el('p');
        foreach ($questions as $question) {
            $questionsText = $questionsText . $htmlProduct->create('p')->setHtml($question->question . ': ' . $question->answer);
        }
        $replace['[QUESTIONS]'] = $questionsText;

        // send mail if jorney product is chosen andif selected in admin
        $jorneyproduct = new JourneyProduct($product->productId);
        if ($jorneyproduct->sendEmailInstantly == TRUE && $jorneyproduct->emails != NULL) {
            //pr($jorneyproduct->emails);exit;
            $email = new FEmail(self::INTERNAL_JOURNEY_EMAIL);
            $replace['[PRODUCT]'] = $jorneyproduct->getLngTitle();
            $email->addTo($jorneyproduct->emails);
            $email->replacePlaceHolders($replace);
            $this->emailService->send($email);
        }
    }

    /**
     * @param type $product
     * @param type $csv
     */
    public function midnightCsvEmail($product, $csv)
    {
        $email = new FEmail(self::CSV_EMAIL);
        $email->addTo($product->emails);
        $email->replacePlaceHolders(
            array(
            '[PRODUCT]' => $product->getLngTitle()
            )
        );
        $email->addAttachment('customers.csv', $csv);
        $this->emailService->send($email);
    }

    /**
     * @param Exception $e
     */
    public function errorEmail(Exception $e)
    {
        $email = new FEmail(self::ERROR_EMAIL);
        $email->replacePlaceHolders(
            array(
            '[ERROR]' => $e->getMessage()
            )
        );
        $this->emailService->send($email);
    }

    /**
     * @param CustomerDetails $customerDetails
     */
    public function sendEmailToThirdParties(CustomerDetails $customerDetails)
    {
        $replace = [
            '[FULLNAME]' => $customerDetails->getFullName(),
            '[EMAIL]' => $customerDetails->getEmail(),
            '[PHONE]' => $customerDetails->getPhone(),
            '[POSTCODE]' => $customerDetails->getPostcode(),
            '[COMPANY_NAME]' => $customerDetails->getCompanyName(),
        ];

        $product = $customerDetails->getProduct();
        $email = new FEmail(self::INTERNAL_JOURNEY_EMAIL);
        $replace['[PRODUCT]'] = $product ->getLngTitle();
        $email->addTo($product->emails);
        $email->replacePlaceHolders($replace);
        $this->emailService->send($email);
    }

    /**
     * @param CustomerDetails $customerDetails
     */
    public function sendEmailToCustomer(CustomerDetails $customerDetails)
    {
        $replace = [
            '[FULLNAME]' => $customerDetails->getFullName(),
            '[EMAIL]' => $customerDetails->getEmail(),
            '[PHONE]' => $customerDetails->getPhone(),
            '[POSTCODE]' => $customerDetails->getPostcode(),
            '[COMPANY_NAME]' => $customerDetails->getCompanyName(),
        ];

        $product = $customerDetails->getProduct();
        $htmlProduct = Html::el('p');
        $htmlProduct->create('h4')->setText($product->getLngTitle());
        if (!empty($product->emailText)) {
            $htmlProduct->create('p')->setHtml($product->emailText);
        }
        $replace['[PRODUCTS]'] = $htmlProduct;
        $title = substr($product->getLngSeoTitle(), 0, 40);
        $replaceSubject['[TITLE]'] = $title;

        $email = new FEmail(self::JOURNEY_EMAIL);
        $email->addTo($customerDetails->getEmail());
        $email->subject = str_replace(array_keys($replaceSubject), $replaceSubject, $email->subject);
        $email->replacePlaceHolders($replace);
        $this->emailService->send($email);
    }

}

