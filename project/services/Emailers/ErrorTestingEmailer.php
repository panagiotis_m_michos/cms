<?php

class ErrorTestingEmailer extends EmailerAbstract
{

    const ERROR_EMAIL = 1127;
    const FORM_SUBMISION_ERROR_EMAIL = 1053;
    const FORM_SUBMISSION_EMPTY_INCORPORATION_CERTIFICATE_EMAIL = 1682;

    /**
     * @param string $error
     */
    public function sendPollingErrorMessage($error)
    {
        $replace = array();
        $replace['[DATE]'] = date('d-m-Y, H:i:s');
        $replace['[MESSAGE]'] = $error;

        $email = $email = new FEmail(self::ERROR_EMAIL);
        $email->replacePlaceHolders($replace);
        $this->emailService->send($email);
    }

    /**
     * @param int $companyId
     * @param int $formSubmissionId
     */
    public function sendFormSubmisionError($companyId, $formSubmissionId)
    {
        $email = new FEmail(self::FORM_SUBMISION_ERROR_EMAIL);
        $data = array('[COMPANY_ID]' => $companyId, '[FORM_SUBMISSION_ID]' => $formSubmissionId);
        $email->replacePlaceHolders($data);
        $this->emailService->send($email);
    }

    /**
     * @param int $companyId
     * @param int $formSubmissionId
     */
    public function sendEmptyIncorporationCertificateError($companyId, $formSubmissionId)
    {
        $email = new FEmail(self::FORM_SUBMISSION_EMPTY_INCORPORATION_CERTIFICATE_EMAIL);
        $email->replacePlaceHolders(
            [
                '[COMPANY_ID]' => $companyId,
                '[FORM_SUBMISSION_ID]' => $formSubmissionId
            ]
        );

        $this->emailService->send($email);
    }
}
