<?php

use Entities\Cashback;
use Entities\Customer;
use Helpers\Admin\Template\CashbackHelper;
use Helpers\Currency;
use Nette\Utils\Html;

class CashBackEmailer extends EmailerAbstract
{
    const EMAIL_CASHBACKS_PAID = 1461;
    const EMAIL_INVALID_CASHBACK_DETAILS = 1617;

    /**
     * @var CashbackHelper
     */
    private $cashbackHelper;

    /**
     * @param CashbackHelper $cashbackHelper
     */
    public function sendPaidCashbacksEmail(CashbackHelper $cashbackHelper)
    {
        $this->cashbackHelper = $cashbackHelper;

        $table = $this->createCompaniesTable();
        $cashbackInfo = $this->createCashbackInfo();
        $customer = $cashbackHelper->getCustomer();

        $email = new FEmail(self::EMAIL_CASHBACKS_PAID);
        $email->addTo($customer->getEmail());
        $email->replacePlaceHolders(
            [
                '[FIRST_NAME]' => $customer->getFirstName(),
                '[COMPANIES_TABLE]' => $table,
                '[CASHBACK_INFO]' => $cashbackInfo,
            ]
        );
        $this->emailService->send($email, $customer);
    }

    /**
     * @param Customer $customer
     */
    public function sendInvalidCashbackDetailEmail(Customer $customer)
    {
        $email = new FEmail(self::EMAIL_INVALID_CASHBACK_DETAILS);
        $email->addTo($customer->getEmail());
        $email->replacePlaceHolders(
            [
                '[FIRST_NAME]' => $customer->getFirstName(),
            ]
        );
        $this->emailService->send($email, $customer);
    }

    /**
     * @return Html
     */
    private function createCashbackInfo()
    {
        $customer = $this->cashbackHelper->getCustomer();
        $info = Html::el('p');

        if ($customer->getCashbackType() == Cashback::CASHBACK_TYPE_BANK) {
            $info->setHtml(
                sprintf(
                    'The cash back will be paid as money in this bank account:<br>
                    Sort Code: %s<br>
                    Account Number: %s<br>',
                    $customer->getSortCode(),
                    $customer->getAccountNumber()
                )
            );
        } elseif ($customer->getCashbackType() == Cashback::CASHBACK_TYPE_CREDITS) {
            $info->setHtml(
                sprintf(
                    'The cash back will be paid as credit on your Companies Made Simple account:<br>
                    Email address: %s<br>',
                    $customer->getEmail()
                )
            );
            $info->create('a')->href(FApplication::$router->absoluteFrontLink(WHCreditControler::ADD_CREDIT_PAGE))->setText('See your available credit');
        }

        return $info;
    }

    /**
     * @return Html
     */
    private function createCompaniesTable()
    {
        $table = Html::el('table', ['width' => '700', 'cellpadding' => '8', 'border' => '1', 'rules' => 'all']);
        $header = $table->create('tr', ['style' => 'background: #eee']);
        $header->create('th')->setText('Co. Number');
        $header->create('th')->setText('Co. Name');
        $header->create('th')->setText('Amount');

        foreach ($this->cashbackHelper->getCashbacks() as $cashback) {
            $cashbackRow = $table->create('tr');
            $cashbackRow->create('td', ['width' => '80', 'style' => 'text-align: center'])->setText($cashback->getCompany()->getCompanyNumber());
            $cashbackRow->create('td')->setText($cashback->getCompany()->getCompanyName());
            $cashbackRow->create('td', ['width' => '80', 'style' => 'text-align: right'])->setText(Currency::format($cashback->getAmount()));
        }

        $table->create('tr')
            ->create('td', ['colspan' => '3', 'style' => 'text-align: right'])
            ->setText('Total amount: ' . Currency::format($this->cashbackHelper->getTotalAmount()));

        return $table;
    }
}
