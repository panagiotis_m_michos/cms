<?php

use Entities\Customer as CustomerEntity;
use Entities\Company as CompanyEntity;
use OrderModule\Pdf\IInvoiceGenerator;
use Services\OrderService;

class PaymentEmailer extends EmailerAbstract
{

    const PAYPAL_ERROR_EMAIL = 233;
    const ORDER_CONFIRMATION_EMAIL = 138;
    const INTERNAL_ORDER_CONFIRMATION_EMAIL = 200;
    const EMAIL_CREDIT_ADDED = 1095;
    const INTERNAL_PURCHASE_COMPANY_CONFLICT_EMAIL = 1507;
    const INTERNAL_PACKAGE_TO_PACKAGE_UPGRADE_EMAIL = 1592;

    /**
     * @var OrderService
     */
    private $orderService;

    /**
     * @var IInvoiceGenerator
     */
    private $invoiceGenerator;

    /**
     * @param IEmailService $emailService
     * @param OrderService $orderService
     * @param IInvoiceGenerator $invoiceGenerator
     */
    public function __construct(IEmailService $emailService, OrderService $orderService, IInvoiceGenerator $invoiceGenerator)
    {
        parent::__construct($emailService);
        $this->orderService = $orderService;
        $this->invoiceGenerator = $invoiceGenerator;
    }

    /**
     * @param Customer $customer
     * @param array $res
     */
    public function paypalErrorEmail(Customer $customer, $res)
    {
        // replace placeholders in email text
        $replace = array();
        $replace['[FIRSTNAME]'] = $customer->firstName;
        $replace['[LASTNAME]'] = $customer->lastName;
        $replace['[EMAIL]'] = $customer->email;
        $replace['[DATE]'] = date('j.m.Y G:i');
        $replace['[TIMESTAMP]'] = isSet($res['TIMESTAMP']) ? urldecode($res['TIMESTAMP']) : 'N/A';
        $replace['[CORRELATIONID]'] = isSet($res['CORRELATIONID']) ? urldecode($res['CORRELATIONID']) : 'N/A';
        $replace['[VERSION]'] = isSet($res['VERSION']) ? urldecode($res['VERSION']) : 'N/A';
        $replace['[BUILD]'] = isSet($res['BUILD']) ? urldecode($res['BUILD']) : 'N/A';
        $replace['[ERROR_CODE]'] = isSet($res['L_ERRORCODE0']) ? urldecode($res['L_ERRORCODE0']) : 'N/A';
        $replace['[LONG_MESSAGE]'] = isSet($res['L_SHORTMESSAGE0']) ? urldecode($res['L_SHORTMESSAGE0']) : 'N/A';
        $replace['[SHORT_MESSAGE]'] = isSet($res['L_LONGMESSAGE0']) ? urldecode($res['L_LONGMESSAGE0']) : 'N/A';
        $replace['[SEVERITY_CODE]'] = isSet($res['L_SEVERITYCODE0']) ? urldecode($res['L_SEVERITYCODE0']) : 'N/A';

        // send email
        $email = new FEmail(self::PAYPAL_ERROR_EMAIL);
        $email->replacePlaceHolders($replace);

        $this->emailService->send($email, $customer);
    }

    /**
     * Provides send confirmation email to customer
     *
     * @param Customer $customer
     * @param Basket $basket
     * @param string $orderId
     */
    public function orderConfirmationEmail(Customer $customer, Basket $basket, $orderId)
    {
        // replace placeholders in email text
        $replace = array();
        $replace['[FIRSTNAME]'] = $customer->firstName;
        $replace['[LASTNAME]'] = $customer->lastName;
        $replace['[DATE]'] = date('j/m/Y');
        $replace['[PRODUCTS_EMAIL_TEXT]'] = '';

        // send email to customer
        $email = new FEmail(self::ORDER_CONFIRMATION_EMAIL);

        // adding email text for products
        /* @var $product Product */
        foreach ($basket->getItems() as $key => $product) {
            switch ($product->getReflection()->getName()) {
                case 'CompanyNameChange':
                    $code = FApplication::$router->absoluteFrontLink(
                        CUChangeNameControler::CHANGE_NAME_PAGE,
                        array('company_id' => $product->getCompanyId())
                    );
                    $product->emailText = str_replace('[NAME_CHANGE_LINK]', $code, $product->emailText);
                    break;
                case 'MrSiteProduct':
                    try {
                        $code = MrSiteCode::getAndUseAvailableCode($product->getId(), $orderId);
                    } catch (Exception $e) {
                        $code = 'There has been an error creating your activation code. Please email theteam@madesimplegroup.com to get your code';
                    }
                    $product->emailText = str_replace('[CODE]', $code, $product->emailText);
                    break;
            }
            if (!empty($product->emailText)) {
                $replace['[PRODUCTS_EMAIL_TEXT]'] .= Html::el('p')->setHtml($product->emailText);
            }
            // add attachments
            if (!empty($product->emailCmsFileAttachments)) {
                foreach ($product->emailCmsFileAttachments as $key2 => $attachment) {
                    $email->addCmsFileAttachment($attachment);
                }
            }
        }

        $order = $this->orderService->getOrderById($orderId);
        $email->addAttachment('Invoice.pdf', $this->invoiceGenerator->getContents($order));

        $email->addTo($customer->email);
        $email->replacePlaceHolders($replace);
        $this->emailService->send($email, $customer);
    }

    /**
     * @param Customer $customer
     * @param Basket $basket
     * @param int $orderId
     */
    public function orderConfirmationInternalEmail(Customer $customer, Basket $basket, $orderId)
    {
        // send internal emails
        foreach ($basket->getItems() as $key => $product) {
            if ($product->responsibleEmails != NULL) {
                // replace placeholders in email text
                $replace = array();
                $replace['[ORDER_ID]'] = $orderId;
                $replace['[CUSTOMER_NAME]'] = $customer->firstName . ' ' . $customer->lastName;
                $replace['[CUSTOMER_EMAIL]'] = $customer->email;
                $replace['[CUSTOMER_PHONE]'] = $customer->phone;
                $replace['[ADDRESS]'] = $customer->getAddress('<br>');
                $replace['[PRODUCT_NAME]'] = $product->getLongTitle();
                $replace['[ADDITIONAL]'] = $product->additional;
                $replace['[AMOUNT]'] = '&pound;' . $product->getTotalVatPrice();

                $email = new FEmail(self::INTERNAL_ORDER_CONFIRMATION_EMAIL);
                $email->subject = str_replace('[PRODUCT_NAME]', $product->getLongTitle(), $email->subject);
                $email->addTo($product->responsibleEmails);
                $email->replacePlaceHolders($replace);
                $this->emailService->send($email, $customer);
            }
        }
    }

    /**
     * Returns invoice table for email
     *
     * @param Basket $basket
     * @param string $cssClass
     * @return string
     */
    public function invoiceTable(Basket $basket, $cssClass = NULL)
    {
        $table = Html::el('table')->border(1)->cellpadding(5)->cellspacing(0);

        if ($cssClass !== NULL) {
            $table->class($cssClass);
        }

        // cols
        $table->create('col')->width(440);
        $table->create('col')->width(50);
        $table->create('col')->width(120);

        // header
        $tr = $table->create('tr');
        $tr->create('th')->setText('Description');
        $tr->create('th')->setText('Qty');
        $tr->create('th')->setText('Price');

        // basket
        foreach ($basket->items as $key => $val) {
            $tr = $table->create('tr');
            $tr->create('td')->setText($val->getLngTitle());
            $tr->create('td')->align('right')->setText($val->qty);
            $tr->create('td')->align('right')->setHtml('&pound;' . $val->price);
        }

        // subtotal
        $tr = $table->create('tr');
        $tr->create('td')->colspan(2)->align('right')->setText('Subtotal:');
        $tr->create('td')->align('right')->setHtml('&pound;' . $basket->getPrice('subTotal'));

        // voucher
        if ($basket->voucher) {
            $tr = $table->create('tr');
            $tr->create('td')->colspan(2)->align('right')->setText('Voucher `' . $basket->voucher->name . '`');
            $tr->create('td')->align('right')->setHtml('-&pound;' . $basket->getPrice('discount'));

            //$tr = $table->create('tr');
            //$tr->create('td')->colspan(3)->align('right')->setText('Discounted subtotal:');
            //$tr->create('td')->align('right')->setHtml('&pound;'.$basket->getPrice('subTotal2'));
        }


        // vat
        $tr = $table->create('tr');
        $tr->create('td')->colspan(2)->align('right')->setText('VAT:');
        $tr->create('td')->align('right')->setHtml('&pound;' . $basket->getPrice('vat'));

        if ($basket->isUsingCredit()) {
            $tr = $table->create('tr');
            $tr->create('td')->colspan(2)->align('right')->setText('Credit:');
            $tr->create('td')->align('right')->setHtml('-&pound;' . $basket->getPrice('account'));
        }
        // total
        $tr = $table->create('tr');
        $tr->create('td')->colspan(2)->align('right')->setText('Total:');
        $tr->create('td')->align('right')->setHtml('&pound;' . $basket->getPrice('total'));

        return $table->render();
    }

    /**
     * send email to the customer
     *
     * @param int $amount
     * @param Customer $customer
     */
    public function sendEmailCreditAdded($amount, Customer $customer)
    {
        // text placeholders
        $replace = array();
        $replace['[FIRST_NAME]'] = $customer->firstName;
        //$replace['[AMOUNT]'] = $amount;
        //$replace['[COMPANIES_BOUGHT]'] = self::COMPANIES_BOUGHT_LIMIT;

        // send email
        $email = new FEmail(self::EMAIL_CREDIT_ADDED);
        $email->addTo($customer->email);
        $email->replacePlaceHolders($replace);
        $this->emailService->send($email, $customer);
    }

    /**
     * @param CustomerEntity $customer
     * @param BasketProduct $product
     * @param CompanyEntity $conflictCompany
     */
    public function sendInternalPurchaseCompanyConflictEmail(
        CustomerEntity $customer,
        BasketProduct $product,
        CompanyEntity $conflictCompany
    )
    {
        $email = new FEmail(self::INTERNAL_PURCHASE_COMPANY_CONFLICT_EMAIL);
        $placeholders = array(
            '[CUSTOMER_NAME]' => $customer->getFullName(),
            '[CUSTOMER_EMAIL]' => $customer->getEmail(),
            '[PRODUCT_NAME]' => $product->getLngTitle(),
            '[IMPORT_COMPANY_NAME]' => $conflictCompany->getCompanyName(),
            '[IMPORT_COMPANY_NUMBER]' => $conflictCompany->getCompanyNumber(),
            '[IMPORT_CUSTOMER_NAME]' => $conflictCompany->getCustomer()->getFullName(),
            '[IMPORT_CUSTOMER_EMAIL]' => $conflictCompany->getCustomer()->getEmail(),
        );

        $email->replacePlaceHolders($placeholders);
        $this->emailService->send($email);
    }
}
