<?php

namespace Emailers;

use Company\Service\RenewalData;
use EmailerAbstract;
use IEmailService;
use Models\View\Front\CompanyServicesEmailView;
use FEmail;
use MyServicesModule\Controllers\ServiceSettingsController;
use Nette\Templating\FileTemplate;
use Symfony\Component\Routing\RouterInterface;

class ServicesEmailer extends EmailerAbstract
{

    const SERVICES_EXPIRES_IN_28_EMAIL = 1377;
    const SERVICES_EXPIRES_IN_15_EMAIL = 1372;
    const SERVICES_EXPIRES_IN_1_EMAIL = 1371;
    const SERVICES_EXPIRED_EMAIL = 1370;
    const SERVICES_EXPIRED_15_AGO_EMAIL = 1433;
    const SERVICES_EXPIRED_28_AGO_EMAIL = 1432;
    const SERVICES_AUTO_RENEWABLE_EXPIRES_IN_7_EMAIL = 1601;
    const SERVICES_AUTO_RENEWAL_SUCCEEDED = 1603;
    const SERVICES_AUTO_RENEWAL_FAILED = 1604;

    /**
     * @var FileTemplate
     */
    private $template;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @param IEmailService $emailService
     * @param FileTemplate $template
     * @param RouterInterface $router
     */
    public function __construct(IEmailService $emailService, FileTemplate $template, RouterInterface $router)
    {
        parent::__construct($emailService);
        $this->template = $template;
        $this->router = $router;
    }

    /**
     * @param CompanyServicesEmailView $emailView
     * @param FEmail $email
     * @param string $path
     */
    public function sendEmailExpiration(CompanyServicesEmailView $emailView, FEmail $email, $path = '/@emails/Services/main.latte')
    {
        $company = $emailView->getCompanyServicesView()->getCompany();
        $customer = $company->getCustomer();
        $this->template->setFile(FRONT_TEMPLATES_DIR . $path);
        $this->template->emailView = $emailView;
        $this->template->disableRemindersLink = $this->router->generate(
            ServiceSettingsController::ROUTE_DISABLE_REMINDERS_FOR_COMPANY,
            ['companyId' => $company->getId()],
            RouterInterface::ABSOLUTE_URL
        );
        $email->body = $this->template;
        $email->addTo($customer->getEmail());

        $this->emailService->send($email, $customer);
    }

    /**
     * @param RenewalData $customerRenewalData
     */
    public function sendEmailServiceAutoRenewalSucceeded(RenewalData $customerRenewalData)
    {
        $email = new FEmail(self::SERVICES_AUTO_RENEWAL_SUCCEEDED);

        $customer = $customerRenewalData->getCustomer();
        $token = $customer->getActiveToken();

        $this->template->setFile(FRONT_TEMPLATES_DIR . '/@emails/Services/autoRenewalSucceeded.latte');
        $this->template->emailTitle = $email->subject;
        $this->template->customer = $customer;
        $this->template->token = $token;
        $this->template->renewalData = $customerRenewalData;
        $this->template->earliestServiceExpiryDate = $customerRenewalData->getEarliestServiceExpiryDate();

        $email->replaceSubjectPlaceHolders(
            array(
                '[SERVICE_WORD]' => $customerRenewalData->getServiceCount() == 1 ? 'Service' : 'Services',
            )
        );

        $email->body = $this->template;
        $email->addTo($customer->getEmail());

        $this->emailService->send($email, $customer);
    }

    /**
     * @param RenewalData $customerRenewalData
     */
    public function sendEmailServiceAutoRenewalFailed(RenewalData $customerRenewalData)
    {
        $email = new FEmail(self::SERVICES_AUTO_RENEWAL_FAILED);

        $customer = $customerRenewalData->getCustomer();
        $token = $customer->getActiveToken();

        $this->template->setFile(FRONT_TEMPLATES_DIR . '/@emails/Services/autoRenewalFailed.latte');
        $this->template->emailTitle = $email->subject;
        $this->template->customer = $customer;
        $this->template->token = $token;
        $this->template->renewalData = $customerRenewalData;
        $this->template->earliestServiceExpiryDate = $customerRenewalData->getEarliestServiceExpiryDate();

        $email->replaceSubjectPlaceHolders(
            array(
                '[SERVICE_WORD]' => $customerRenewalData->getServiceCount() == 1 ? 'Service' : 'Services',
            )
        );

        $email->body = $this->template;
        $email->addTo($customer->getEmail());

        $this->emailService->send($email, $customer);
    }

    /**
     * @param RenewalData $servicesRenewalData
     */
    public function sendEmailServiceAutoRenewalIn7(RenewalData $servicesRenewalData)
    {
        $email = new FEmail(self::SERVICES_AUTO_RENEWABLE_EXPIRES_IN_7_EMAIL);

        $customer = $servicesRenewalData->getCustomer();
        $token = $customer->getActiveToken();

        $this->template->setFile(FRONT_TEMPLATES_DIR . '/@emails/Reminders/autoRenewal.latte');
        $this->template->emailTitle = $email->subject;
        $this->template->customer = $customer;
        $this->template->token = $token;
        $this->template->renewalData = $servicesRenewalData;
        $this->template->earliestServiceExpiryDate = $servicesRenewalData->getEarliestServiceExpiryDate();

        $email->replaceSubjectPlaceHolders(
            array(
                '[LOWEST_DAYS_TO_EXPIRE]' => $servicesRenewalData->getLowestServiceDaysToExpire(),
                '[SERVICE_WORD]' => $servicesRenewalData->getServiceCount() == 1 ? 'service' : 'services',
            )
        );

        $email->body = $this->template;
        $email->addTo($customer->getEmail());

        $this->emailService->send($email, $customer);
    }
}
