<?php

class WestburyEmailer extends EmailerAbstract
{
    const EMAIL_NODE = 805;
    
    /**
     * @param array $data
     */
    public function consultationEmail(array $data)
    {
                                        
        $email = new FEmail(self::EMAIL_NODE);        
        $email->replacePlaceHolders(
            array(
                '[FULLNAME]' => $data['fullName'],
                '[EMAIL]' => $data['email'],
                '[PHONE]' => $data['phone'],
                '[POSTCODE]' => $data['postcode'],
                '[COMPANY_NAME]' => $data['companyName'],
                '[DETAILS]' => $data['details'],
            )
        );

        $this->emailService->send($email);
    }


}

