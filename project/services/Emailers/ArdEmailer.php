<?php

class ArdEmailer extends EmailerAbstract
{

    const ARD_ACCEPT_EMAIL = 1155;
    const ARD_REJECT_EMAIL = 1156;

    /**
     * @param Customer $customer
     * @param array $replace
     */
    public function ardChangeAcceptEmail(Customer $customer, array $replace)
    {
        $email = new FEmail(self::ARD_ACCEPT_EMAIL);
        $email->addTo($customer->email);
        $email->replacePlaceHolders($replace);
        $this->emailService->send($email);
    }

    /**
     * @param Customer $customer
     * @param array $replace
     */
    public function ardChangeRejectEmail(Customer $customer, array $replace)
    {
        $email = new FEmail(self::ARD_REJECT_EMAIL);
        $email->addTo($customer->email);
        $email->replacePlaceHolders($replace);
        $this->emailService->send($email, $customer);
    }

}

