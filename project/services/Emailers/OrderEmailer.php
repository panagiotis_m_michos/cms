<?php

use Entities\Order;

class OrderEmailer extends EmailerAbstract
{
    const CUSTOMER_REFUND_EMAIL = 621;

    /**
     * @param Order $order
     * @param float $creditRefundValue
     * @param float $moneyRefundValue
     */
    public function sendCustomerRefundEmail(Order $order, $creditRefundValue, $moneyRefundValue)
    {
        $customer = $order->getCustomer();

        $email = new FEmail(self::CUSTOMER_REFUND_EMAIL);
        $email->addTo($customer->getEmail());
        $email->replacePlaceHolders(
            array(
                '[FIRST_NAME]' => $customer->getFirstName(),
                '[ORDER_ID]' => $order->getId(),
                '[REFUND_AMOUNT]' => self::getRefundAmount($creditRefundValue, $moneyRefundValue),
            )
        );
        $this->emailService->send($email, $customer);
    }

    /**
     * @param float $creditRefundValue
     * @param float $moneyRefundValue
     * @return Html
     */
    private static function getRefundAmount($creditRefundValue, $moneyRefundValue)
    {
        $el = Html::el();

        if ($creditRefundValue) {
            $el->add(sprintf('Credit refund amount: £%s', $creditRefundValue));
            $el->create('br');
        }

        if ($moneyRefundValue) {
            $el->add(sprintf('Money refund amount: £%s', $moneyRefundValue));
        }

        return $el;
    }

}
