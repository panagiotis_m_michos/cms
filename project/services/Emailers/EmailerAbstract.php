<?php

class EmailerAbstract
{
    /**
     * @var IEmailService
     */
    protected $emailService;

    /**
     * @param IEmailService $emailService
     */
    public function __construct(IEmailService $emailService)
    {
        $this->emailService = $emailService;
    }

    /**
     * @param FEmail $email
     * @param Customer $customer
     * @param mixed $replace
     */
    protected function sendEmail(FEmail $email, Customer $customer, $replace)
    {
        $email->addTo($customer->email);
        $email->replacePlaceHolders($replace);
        $this->emailService->send($email, $customer);
    }
}

