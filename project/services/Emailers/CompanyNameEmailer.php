<?php

class CompanyNameEmailer extends EmailerAbstract
{

    const CNCH_REJECTED_EMAIL = 849;
    const CNCH_ACCEPTED_EMAIL = 848;

    /**
     * @param Customer $customer
     * @param array $replace
     * @param array $return
     */
    public function nameChangeAcceptEmail(Customer $customer, array $replace, array $return)
    {
        $email = new FEmail(self::CNCH_ACCEPTED_EMAIL);
        $email->addTo($customer->email);
        $email->replacePlaceHolders($replace);
        $email->addAttachment($return['attachmentPath']);
        $this->emailService->send($email);
    }

    /**
     * @param Customer $customer
     * @param array $replace
     */
    public function nameChangerejectEmail(Customer $customer, array $replace)
    {
        $email = new FEmail(self::CNCH_REJECTED_EMAIL);
        $email->addTo($customer->email);
        $email->replacePlaceHolders($replace);
        $this->emailService->send($email, $customer);
    }

}

