<?php

class CronJobEmailer extends EmailerAbstract
{

    const WELCOME_HG_EMAIL = 631;
    const AUTOMATED_RESPOND_EMAIL = 632;

    /**
     * @param array $row
     */
    public function welcomeEmail($row)
    {
        $email = new FEmail(self::WELCOME_HG_EMAIL);
        $email->replacePlaceHolders(array('[FIRST_NAME]' => $row->firstName));
        $email->setTo($row->email);
        $this->emailService->send($email);
    }

    /**
     * @param array $row
     */
    public function notIncorporatedCompanyEmail($row)
    {
        $replace = array();
        $replace['[FIRST_NAME]'] = $row->firstName;
        $replace['[COMPANY_NAME]'] = $row->company_name;
        $replace['[EMAIL]'] = $row->email;

        $email = new FEmail(self::AUTOMATED_RESPOND_EMAIL);
        $email->replacePlaceHolders($replace);
        $email->setTo($row->email);
        $this->emailService->send($email);
    }

}
