<?php

use UserModule\Contracts\ICustomer;
use UserModule\Contracts\ILoginEmailer;

class ForgottenPasswordEmailer implements ILoginEmailer
{
    const FORGOTTEN_PASSWORD_EMAIL = 278;

    /**
     * @var IEmailService
     */
    private $emailService;

    /**
     * @var FEmailFactory
     */
    private $emailFactory;

    /**
     * @param IEmailService $emailService
     * @param FEmailFactory $emailFactory
     */
    public function __construct(IEmailService $emailService, FEmailFactory $emailFactory)
    {
        $this->emailService = $emailService;
        $this->emailFactory = $emailFactory;
    }

    /**
     * @param string $link
     * @param Customer $customer
     */
    public function forgottenPasswordEmail($link, Customer $customer)
    {
        $replace = [
            '[FIRSTNAME]' => $customer->firstName,
            '[EMAIL]' => $customer->email,
            '[PASSWORD]' => $customer->password,
            '[URL]' => $link,
        ];

        $email = $this->emailFactory->getById(self::FORGOTTEN_PASSWORD_EMAIL);
        $email->replacePlaceHolders($replace);
        $email->addTo($customer->email);

        $this->emailService->send($email, $customer);
    }

    /**
     * @param ICustomer $customer
     * @param string $returnUrl
     * @return bool
     */
    public function sendForgottenPasswordEmail(ICustomer $customer, $returnUrl)
    {
        $replace = [
            '[FIRSTNAME]' => $customer->getFirstName(),
            '[EMAIL]' => $customer->getEmail(),
            '[PASSWORD]' => $customer->getTemporaryPassword(),
            '[URL]' => $returnUrl,
        ];

        $email = $this->emailFactory->getById(self::FORGOTTEN_PASSWORD_EMAIL);
        $email->replacePlaceHolders($replace);
        $email->addTo($customer->getEmail());

        $this->emailService->send($email, $customer);
    }
}
