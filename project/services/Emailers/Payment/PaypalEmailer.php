<?php

namespace Payment;

use Basket;
use BasketProduct;
use Customer;
use EmailerAbstract;
use FEmail;

class PaypalEmailer extends EmailerAbstract
{
    const EMAIL_DIFFERENT_PRICE = 1491;

    /**
     * @param Customer $customer
     * @param Basket $basket
     * @param float $responsePrice
     * @param array $response
     */
    public function sendPriceDifferenceEmail(Customer $customer, Basket $basket, $responsePrice, array $response)
    {
        $email = new FEmail(self::EMAIL_DIFFERENT_PRICE);
        $email->replacePlaceHolders(
            array(
                '[CUSTOMER_NAME]' => $customer->getFullName(),
                '[CUSTOMER_EMAIL]' => $customer->email,
                '[BASKET_PRICE]' => $basket->getPrice('total'),
                '[PAYPAL_PRICE]' => $responsePrice,
                '[PAYPAL_RESPONSE]' => self::prettifyResponse($response),
                '[BASKET_ITEMS]' => self::prettifyBasketItems($basket)
            )
        );

        $this->emailService->send($email);
    }

    /**
     * @param array $response
     * @return string
     */
    private static function prettifyResponse(array $response)
    {
        array_walk(
            $response,
            function (&$item) {
                $item = urldecode($item);
            }
        );
        return var_export($response, TRUE);
    }

    /**
     * @param Basket $basket
     * @return string
     */
    private static function prettifyBasketItems(Basket $basket)
    {
        $items = array();
        /** @var BasketProduct $item  */
        foreach ($basket->getItems() as $item) {
            $items[] = sprintf(
                "[%s] %s (%s)",
                $item->getId(),
                $item->getLngTitle(),
                number_format($item->getPrice(), 2)
            );
        }
        return var_export($items, TRUE);
    }

}

