<?php

namespace Wholesale;

use Customer;
use EmailerAbstract;
use FEmail;

class ICAEWEmailer extends EmailerAbstract implements IEmailer
{
    /* emails */
    const EMAIL_SIGN_UP = 1034;
    const EMAIL_CONTACT_EMAIL = 948;

    /**
     * @param array $data
     * @return mixed|void
     */
    public function sendContactUsEmail(array $data)
    {
        $email = new FEmail(self::EMAIL_CONTACT_EMAIL);
        $email->from = $data['customer_email'];
        $email->setTo('support@companiesmadesimple.com');
        $email->replacePlaceHolders(
            array(
                '[CUSTOMER_NAME]' => $data['customer_name'],
                '[EMAIL]' => $data['customer_email'],
                '[PHONE]' => $data['contact_phone'],
                '[COMPANY]' => $data['company_name'],
                '[MESSAGE]' => $data['message'],
            )
        );
        $this->emailService->send($email);
    }

    /**
     * @param $password
     * @param Customer $customer
     * @return mixed|void
     */
    public function sendSignUpEmail($password, Customer $customer)
    {
        $email = new FEmail(self::EMAIL_SIGN_UP);
        $email->addTo($customer->email);
        $email->replacePlaceHolders(
            array(
                '[FIRST_NAME]' => $customer->firstName,
                '[EMAIL]' => $customer->email,
                '[PASSWORD]' => $password,
            )
        );
        $this->emailService->send($email, $customer);
    }
}

