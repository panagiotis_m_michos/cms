<?php

namespace Wholesale;

use Customer;

interface IEmailer
{
    /**
     * @param array $data
     * @return mixed
     */
    public function sendContactUsEmail(array $data);

    /**
     * @param $password
     * @param Customer $customer
     * @return mixed
     */
    public function sendSignUpEmail($password, Customer $customer);
}

