<?php

class UserEmailer extends EmailerAbstract
{

    const FORGOTTEN_PASSWORD_EMAIL = 278;
    const NEW_ACCOUNT_WELCOME_EMAIL = 1219;

    /**
     * @param Customer $customer
     */
    public function forgottenPasswordEmail($link, Customer $customer)
    {
        $replace = array();
        $replace['[FIRSTNAME]'] = $customer->firstName;
        $replace['[EMAIL]'] = $customer->email;
        $replace['[PASSWORD]'] = $customer->password;

        $replace['[URL]'] = $link;

        $email = new FEmail(self::FORGOTTEN_PASSWORD_EMAIL);
        $email->replacePlaceHolders($replace);
        $email->addTo($customer->email);

        $this->emailService->send($email, $customer);
    }

    /**
     * @param Customer $customer
     */
    public function sendWelcomeNewCustomerEmail(Customer $customer)
    {
        $replace = array();
        $replace['[FULLNAME]'] = $customer->firstName . ' ' . $customer->lastName;
        $replace['[USERNAME]'] = $customer->email;
        $replace['[PASSWORD]'] = $customer->password;
        $replace['[URL]'] = FApplication::$router->absoluteFrontLink(LoginControler::LOGIN_PAGE);

        $email = new FEmail(self::NEW_ACCOUNT_WELCOME_EMAIL);
        $email->replacePlaceHolders($replace);
        $email->addTo($customer->email);

        $this->emailService->send($email, $customer);
    }

}

