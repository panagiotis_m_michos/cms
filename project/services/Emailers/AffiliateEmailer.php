<?php

class AffiliateEmailer extends EmailerAbstract
{
    const AFFILIATE_CUSTOMERS_IMPORT_EMAIL = 707;
    const FORGOTTEN_PASSWORD_EMAIL = 984;
        
    /**
     * @param String $link
     * @param Affiliate $customer
     */
    public function forgottenPasswordEmail($link, Affiliate $customer)
    {
            $replace = array();
            $replace['[FIRSTNAME]'] = $customer->firstName;
            $replace['[EMAIL]'] = $customer->email;
            $replace['[PASSWORD]'] = $customer->password;
            $replace['[URL]'] = $link;

            $email = new FEmail(self::FORGOTTEN_PASSWORD_EMAIL);
            $email->replacePlaceHolders($replace);
            $email->addTo($customer->email);

            $this->emailService->send($email);
    }

    /**
     * @param Customer $customer
     */
    public function customerImportEmail(Customer $customer)
    {
        $placeHolders = array(
                '[FIRST_NAME]' => $customer->firstName,
                '[EMAIL]' => $customer->email,
                '[PASSWORD]' => $customer->password,
        );
        $email = new FEmail(self::AFFILIATE_CUSTOMERS_IMPORT_EMAIL);
        $email->addTo($customer->email);
        $email->replacePlaceHolders($placeHolders);
        $this->emailService->send($email);
    }

}

