<?php

class BusinessBankingEmailer extends EmailerAbstract
{
    const HSBC_EMAIL = 1027;
    const HSBC_BANKING_EMAIL = 527;
    const NATWEST_BANKING_EMAIL = 528;
    const ABBEY_BANKING_EMAIL = 529;
    const HSBC_ERROR_EMAIL = 1028;
    const BARCLAYS_ERROR_EMAIL = 641;
    const CARD_ONE_EMAIL = 1395;
    const CARD_ONE_ERROR_EMAIL = 1407;

    /**
     * @param array $data
     */
    public function hsbcEmail(array $data)
    {
        $replace = array();
        $replace['[CUSTOMER_NAME]'] = $data['customer_name'];
        $replace['[CUSTOMER_EMAIL]'] = $data['customer_email'];
        $replace['[CUSTOMER_PHONE]'] = $data['customer_phone'];
        $replace['[CUSTOMER_ADDRESS1]'] = $data['customer_address1'];
        if (isset($data['customer_address2'])) {
            $replace['[CUSTOMER_ADDRESS2]'] = $data['customer_address2'];
        } else {
            $replace['[CUSTOMER_ADDRESS2]'] = '';
        }
        $replace['[CUSTOMER_TOWN]'] = $data['customer_town'];
        $replace['[CUSTOMER_POSTCODE]'] = $data['customer_postcode'];
        $replace['[COMPANY_NAME]'] = $data['company_name'];

        $email = new FEmail(self::HSBC_BANKING_EMAIL);
        $email->replacePlaceHolders($replace);
        $this->emailService->send($email);
    }

    /**
     * @param array $data
     */
    public function natwestEmail(array $data)
    {
        $replace = array();
        $replace['[CUSTOMER_NAME]'] = $data['customer_name'];
        $replace['[CUSTOMER_EMAIL]'] = $data['customer_email'];
        $replace['[CUSTOMER_PHONE]'] = $data['customer_phone'];
        $replace['[CUSTOMER_ADDRESS1]'] = $data['customer_address1'];
        if (isset($data['customer_address2'])) {
            $replace['[CUSTOMER_ADDRESS2]'] = $data['customer_address2'];
        } else {
            $replace['[CUSTOMER_ADDRESS2]'] = '';
        }
        $replace['[CUSTOMER_TOWN]'] = $data['customer_town'];
        $replace['[CUSTOMER_POSTCODE]'] = $data['customer_postcode'];
        $replace['[COMPANY_NAME]'] = $data['company_name'];

        $email = new FEmail(self::NATWEST_BANKING_EMAIL);
        $email->replacePlaceHolders($replace);
        $this->emailService->send($email);
    }

    /**
     * @param array $data
     */
    public function abbeyEmail(array $data)
    {
        $replace = array();
        $replace['[CUSTOMER_NAME]'] = $data['customer_name'];
        $replace['[CUSTOMER_EMAIL]'] = $data['customer_email'];
        $replace['[CUSTOMER_PHONE]'] = $data['customer_phone'];
        $replace['[COMPANY_NAME]'] = $data['company_name'];

        $email = new FEmail(self::ABBEY_BANKING_EMAIL);
        $email->replacePlaceHolders($replace);
        $this->emailService->send($email);
    }

    /**
     * @param Exception $e
     * @param int $companyId
     */
    public function hsbcErrorEmail(Exception $e, $companyId)
    {
        $email = new FEmail(self::HSBC_ERROR_EMAIL);
        $email->replacePlaceHolders(
            array(
                '[COMPANY_ID]' => $companyId,
                '[ERROR]' => $e->getMessage()
            )
        );
        $this->emailService->send($email);
    }

    /**
     * @param Exception $e
     * @param int $companyId
     */
    public function barclaysErrorEmail(Exception $e, $companyId)
    {
        $email = new FEmail(self::BARCLAYS_ERROR_EMAIL);
        $email->replacePlaceHolders(
            array(
                '[COMPANY_ID]' => $companyId,
                '[ERROR]' => $e->getMessage()
            )
        );
        $this->emailService->send($email);
    }

    /**
     * @param Exception $e
     * @param type $companyId
     */
    public function cardOneErrorEmail(Exception $e, $companyId)
    {
        $email = new FEmail(self::CARD_ONE_ERROR_EMAIL);
        $email->replacePlaceHolders(
            array(
                '[COMPANY_ID]' => $companyId,
                '[ERROR]' => $e->getMessage()
            )
        );
        $this->emailService->send($email);
    }

    /**
     * @param Company $company
     * @param Customer $customer
     */
    public function sendCompanyToHsbc(Company $company, Customer $customer)
    {
        $email = new FEmail(self::HSBC_EMAIL);
        $email->replacePlaceHolders(
            array(
                '[COMPANY_ID]' => $company->getCompanyId(),
                '[COMPANYNAME]' => $company->getCompanyName(),
                '[COMPANY_NUMBER]' => $company->getCompanyNumber(),
                '[COMPANY_TYPE]' => $company->getCompanyCategory(),
                '[EMAIL]' => $customer->email,
                '[FIRST_NAME]' => $customer->firstName,
                '[LAST_NAME]' => $customer->lastName,
                '[PHONE]' => $customer->phone,
                '[POSTCODE]' => $customer->postcode
            )
        );
        $this->emailService->send($email, $customer);
    }

    /**
     * @param Company $company
     * @param CompanyCustomer $companyCustomer
     */
    public function sendCompanyToCardOne(Company $company, CompanyCustomer $companyCustomer)
    {
        $email = new FEmail(self::CARD_ONE_EMAIL);
        $address = $company->getRegisteredOffice()->getAddressAsString();
        $email->replacePlaceHolders(
            array(
                '[COMPANY_ID]' => $company->getCompanyId(),
                '[COMPANYNAME]' => $company->getCompanyName(),
                '[COMPANY_NUMBER]' => $company->getCompanyNumber(),
                '[COMPANY_TYPE]' => $company->getCompanyCategory(),
                '[EMAIL]' => $companyCustomer->email,
                '[FIRST_NAME]' => $companyCustomer->firstName,
                '[LAST_NAME]' => $companyCustomer->lastName,
                '[PHONE]' => $companyCustomer->phone,
                '[REGISTERED_OFFICE_ADDRESS]' => $address
            )
        );
        $customer = new Customer($company->getCustomerId());
        $this->emailService->send($email, $customer);
    }

}

