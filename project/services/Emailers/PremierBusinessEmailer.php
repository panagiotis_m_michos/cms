<?php

use Entities\Company;

class PremierBusinessEmailer extends EmailerAbstract
{

    const EMAIL_INTERNATIONAL_COMPANY_INCORPORATED = 1600;

    /**
     * @param Company $company
     * @param Package $package
     * @param string $companySummaryPdfString
     */
    public function sendInternationalCompanyIncorporated(Company $company, Package $package, $companySummaryPdfString)
    {
        $email = new FEmail(self::EMAIL_INTERNATIONAL_COMPANY_INCORPORATED);
        $customer = $company->getCustomer();

        $replace = array(
            '[ORDER_ID]' => $company->getOrder()->getOrderId(),
            '[PRODUCT_TITLE]' => $package->getLngTitle(),
            '[FIRST_NAME]' => $customer->getFirstName(),
            '[LAST_NAME]' => $customer->getLastName(),
            '[PHONE]' => $customer->getPhone(),
            '[EMAIL]' => $customer->getEmail(),
            '[COMPANY_NAME]' => $company->getCompanyName(),
            '[COMPANY_NUMBER]' => $company->getCompanyNumber(),
            '[BANKING_NOTICE]' => ''
        );

        if ($package->getId() == Package::PACKAGE_INTERNATIONAL_BANKING) {
            $replace['[BANKING_NOTICE]'] = 'The customer also requested a Business Bank Account.';
        }

        $email->replaceAllPlaceHolders($replace);

        $attachmentName = sprintf(
            '%s-%s-%s.pdf',
            $company->getCompanyNumber(),
            str_replace(' ', '-', $company->getCompanyName()),
            date('Y-m-d')
        );

        $email->addAttachment($attachmentName, $companySummaryPdfString, 'application/pdf');
        $this->emailService->send($email, $customer);
    }
}
