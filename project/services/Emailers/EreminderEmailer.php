<?php

class EreminderEmailer extends EmailerAbstract
{

    const EREMINDER_ERROR_EMAIL = 1177;

    /**
     * @param Exception $e
     * @param int $id
     */
    public function sendEreminderErrorEmail(Exception $e, $id)
    {
        $email = new FEmail(self::EREMINDER_ERROR_EMAIL);
        $email->replacePlaceHolders(array(
            '[COMPANY_ID]' => $id,
            '[ERROR]' => $e->getMessage()
        ));
        $this->emailService->send($email);
    }

}
