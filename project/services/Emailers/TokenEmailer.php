<?php

namespace Emailers;

use Entities\Payment\Token;
use EmailerAbstract;
use FEmail;
use FApplication;
use ManagePaymentControler;
use Utils\Date;

class TokenEmailer extends EmailerAbstract
{
    const EMAIL_EXPIRES = 1453;
    const EMAIL_EXPIRED = 1454;

    /**
     * @param Token $token 
     */
    public function sendEmailSoonExpires(Token $token)
    {
        $customer = $token->getCustomer();
        $expiryDate = clone $token->getCardExpiryDate();
        $expiresInDate = $expiryDate->modify('+1 days')->diff(new Date());
        $email = new FEmail(self::EMAIL_EXPIRES);
        $email->replacePlaceHolders(
            array(
                '[FIRST_NAME]' => $customer->getFirstName(),
                '[EXPIRES_IN]' => $expiresInDate->format('%d'),
                '[LAST4DIGITS]' => $token->getCardNumber(),
                '[EXPIRY]' => $token->getCardExpiryDate()->format('d/m/Y'),
                '[MANAGE_CARD_URL]' => FApplication::$router->absoluteFrontLink(ManagePaymentControler::MANAGE_PAYMENT_PAGE),
            )
        );
        $email->addTo($customer->getEmail());
        $this->emailService->send($email, $customer);
    }

    /**
     * @param Token $token 
     */
    public function sendEmailExpired(Token $token)
    {
        $customer = $token->getCustomer();
        $email = new FEmail(self::EMAIL_EXPIRED);
        $email->replacePlaceHolders(
            array(
                '[FIRST_NAME]' => $customer->getFirstName(),
                '[LAST4DIGITS]' => $token->getCardNumber(),
                '[EXPIRY]' => $token->getCardExpiryDate()->format('d/m/Y'),
                '[MANAGE_CARD_URL]' => FApplication::$router->absoluteFrontLink(ManagePaymentControler::MANAGE_PAYMENT_PAGE),
            )
        );
        $email->addTo($customer->getEmail());
        $this->emailService->send($email, $customer);
    }
}
