<?php

class RegusEmailer extends EmailerAbstract
{
    const REGISTER_OFFICE_REGUS_MAIL = 1421;
    
    /**
     * @param array $data
     */
    public function enquireEmail(Regus $entity)
    {
        $email = new FEmail(self::REGISTER_OFFICE_REGUS_MAIL);
        $email->replacePlaceHolders(
            array(
            '[FIRST_NAME]' => $entity->getFirstName(),
            '[LAST_NAME]' => $entity->getLastName(),
            '[EMAIL]' => $entity->getEmail(),
            '[DIAL_CODE]' => $entity->getDialCode(),
            '[PHONE]' => $entity->getPhone(),
            )
        );
        $email->body = strip_tags($email->body);
        $this->emailService->send($email, NULL, FALSE);
        
    }

}

