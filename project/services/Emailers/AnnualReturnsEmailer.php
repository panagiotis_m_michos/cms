<?php

class AnnualReturnsEmailer extends EmailerAbstract
{
    const SERVICE_MISSING_INFO_EMAIL = 662;
    const SERVICE_COMPLETED_EMAIL = 663;
    const SERVICE_CUSTOMER_ACCEPTED_EMAIL = 661;
    const SERVICE_CUSTOMER_REJECTED_EMAIL = 664;
    const SERVICE_CH_ACCEPTED_EMAIL = 669;
    const SERVICE_CH_REJECTED_EMAIL = 665;
    const SERVICE_CH_ERROR_EMAIL = 773;

    const ACCEPTED_EMAIL = 649;
    const REJECTED_EMAIL = 650;
    const DIY_CH_ERROR_EMAIL = 773;
    
    public function sendManualServiceAccepted(Customer $customer, $replace)
    {
        $email = new FEmail(self::ACCEPTED_EMAIL);
        $this->sendEmail($email, $customer, $replace);
    }

    public function sendManualServiceRejected(Customer $customer, $replace)
    {
        $email = new FEmail(self::REJECTED_EMAIL);
        $this->sendEmail($email, $customer, $replace);
    }

    public function sendCompanyHouseServiceAccepted(Customer $customer, $replace)
    {
        $email = new FEmail(self::SERVICE_CH_ACCEPTED_EMAIL);
        $this->sendEmail($email, $customer, $replace);
    }

    public function sendCompanyHouseServiceRejected(Customer $customer, $replace)
    {
        $email = new FEmail(self::SERVICE_CH_REJECTED_EMAIL);
        $this->sendEmail($email, $customer, $replace);
    }

    public function sendManualServiceError(Customer $customer, $company)
    {
        $email = new FEmail(self::DIY_CH_ERROR_EMAIL);
        $email->replacePlaceHolders(array(
            '[FIRSTNAME]' => $customer->firstName,
            '[COMPANY_NAME]' => $company->getCompanyName()
        ));
        $this->emailService->send($email, $customer);
    }

    public function sendServiceCompleted(Customer $customer, $company)
    {
        // send email to customer
        $email = new FEmail(self::SERVICE_COMPLETED_EMAIL);
        $email->addTo($customer->email);
        $email->replacePlaceHolders(array(
            '[FIRST_NAME]' => $customer->firstName,
            '[COMPANY_NUMBER]' => $company->getCompanyNumber(),
            '[COMPANY_NAME]' => $company->getCompanyName(),
        ));
        $this->emailService->send($email, $customer);
    }

    public function sendServiceAccepted(Customer $customer, $company)
    {
        $email = new FEmail(self::SERVICE_CUSTOMER_ACCEPTED_EMAIL);
        $email->addTo($customer->email);
        $email->replacePlaceHolders(array(
            '[FIRST_NAME]' => $customer->firstName,
            '[COMPANY_NUMBER]' => $company->getCompanyNumber(),
            '[COMPANY_NAME]' => $company->getCompanyName(),
        ));
        $this->emailService->send($email, $customer);
    }

    public function sendServiceError(Customer $customer, $company)
    {
        $email = new FEmail(self::SERVICE_CH_ERROR_EMAIL);
        $email->replacePlaceHolders(array(
            '[FIRSTNAME]' => $customer->firstName,
            '[COMPANY_NAME]' => $company->getCompanyName()
        ));
        $this->emailService->send($email, $customer);
    }

    public function sendServiceMissingInfo(Customer $customer, $company, $body)
    {
        $email = new FEmail(self::SERVICE_MISSING_INFO_EMAIL);
        $email->addTo($customer->email);
        $email->body = $body;
        $email->replacePlaceHolders(array(
            '[FIRST_NAME]' => $customer->firstName,
            '[COMPANY_NAME]' => $company->getCompanyName(),
        ));
        $this->emailService->send($email, $customer);
    }

    public function sendServiceRejected(Customer $customer, $company, $text)
    {
        $email = new FEmail(self::SERVICE_CUSTOMER_REJECTED_EMAIL);
        $email->replacePlaceHolders(array(
            '[COMPANY_NUMBER]' => $company->getCompanyNumber(),
            '[COMPANY_NAME]' => $company->getCompanyName(),
            '[MESSAGE]' => $text,
        ));
        $this->emailService->send($email, $customer);
    }
    
    public function serviceMissingInfoEmail()
    {
        return new FEmail(self::SERVICE_MISSING_INFO_EMAIL);
    }
}
