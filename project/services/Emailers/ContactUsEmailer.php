<?php

class ContactUsEmailer extends EmailerAbstract
{
    const EMAIL_CONTACT = 751;

    /**
     * @param array $data
     */
    public function contactUsEmail(array $data)
    {
        $email = new FEmail(self::EMAIL_CONTACT);
        $email->replacePlaceHolders(
            array(
                '[CUSTOMER_NAME]' => $data['name'],
                '[EMAIL]' => $data['email'],
                '[PHONE]' => $data['phone'],
                '[COMPANY]' => $data['companyName'],
                '[MESSAGE]' => $data['message'],
            )
        );

        $this->emailService->send($email);
    }
}
