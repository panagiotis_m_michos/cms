<?php

namespace Services;

use TaxAssistAccountsEmailer;
use TaxAssistLeadModel;
use TaxAssistLeadTracking;

class TaxAssistLeadService
{

    /**
     * @var TaxAssistAccountsEmailer
     */
    private $emailer;

    /**
     * @var TaxAssistLeadModel
     */
    private $taxAssistModel;

    /**
     * @param TaxAssistAccountsEmailer $emailer
     * @param TaxAssistLeadModel $taxAssistModel
     */
    public function __construct(TaxAssistAccountsEmailer $emailer, TaxAssistLeadModel $taxAssistModel)
    {
        $this->emailer = $emailer;
        $this->taxAssistModel = $taxAssistModel;
    }

    /**
     * @param array $data
     */
    public function processAccountData(array $data)
    {
        // save lead to local db
        // @todo: change to entity
        $tracking = new TaxAssistLeadTracking();
        $tracking->type = 'ACCOUNTS_PAGE';
        $tracking->name = $data['fullName'];
        $tracking->email = $data['email'];
        $tracking->postcode = $data['postcode'];
        $tracking->phone = $data['phone'];
        $tracking->companyName = $data['companyName'];
        $tracking->save();

        // send lead
        $this->taxAssistModel->saveTALead(TaxAssistLeadModel::TYPE_GENERAL, $data['email'], $data['fullName'], $data['phone'], $data['postcode'], $data['companyName']);
    }

    /**
     * @param array $data
     */
    public function processSoletraderData(array $data)
    {
        // save lead to local db
        // @todo: change to entity
        $tracking = new TaxAssistLeadTracking();
        $tracking->type = 'SOLETRADER_PAGE';
        $tracking->name = $data['fullName'];
        $tracking->email = $data['email'];
        $tracking->postcode = $data['postcode'];
        $tracking->phone = $data['phone'];
        $tracking->companyName = $data['companyName'];
        $tracking->save();

        // send lead
        $this->taxAssistModel->saveTALead(TaxAssistLeadModel::TYPE_SOLE_TRADER, $data['email'], $data['fullName'], $data['phone'], $data['postcode'], $data['companyName']);
    }

    /**
     * @param array $data
     */
    public function processServiceData(array $data)
    {
        // save lead to local db
        // @todo: change to entity
        $tracking = new TaxAssistLeadTracking();
        $tracking->type = 'GENERAL_PAGE';
        $tracking->name = $data['fullName'];
        $tracking->email = $data['email'];
        $tracking->postcode = $data['postcode'];
        $tracking->phone = $data['phone'];
        $tracking->companyName = $data['companyName'];
        $tracking->save();

        // send lead
        $this->taxAssistModel->saveTALead(TaxAssistLeadModel::TYPE_ANNUAL_ACCOUNTS, $data['email'], $data['fullName'], $data['phone'], $data['postcode'], $data['companyName']);
    }

    /**
     * @param array $data
     */
    public function processJourneyPageData(array $data)
    {
        // send lead
        $this->taxAssistModel->saveTALead(TaxAssistLeadModel::TYPE_JOURNEY, $data['email'], $data['fullName'], $data['phone'], $data['postcode'], $data['company_name']);
    }

}
