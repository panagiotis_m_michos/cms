<?php

namespace Services;

use Exception;
use FRouter;
use Session;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;
use Symfony\Component\Routing\RouterInterface;

class ControllerHelper
{
    const SESSION_FLASH = 'flash_messages';
    const SESSION_BACKLINK = 'backlink';
    const SESSION_DEFAULT = 'controller_helper';

    /**
     * @var Session
     */
    private $session;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var FRouter
     */
    private $frouter;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @param Session $session
     * @param RouterInterface $router
     * @param FRouter $frouter
     * @param FormFactory $formFactory
     * @param Request $request
     */
    public function __construct(
        Session $session,
        RouterInterface $router,
        FRouter $frouter,
        FormFactory $formFactory,
        Request $request
    )
    {
        $this->session = $session;
        $this->router = $router;
        $this->frouter = $frouter;
        $this->formFactory = $formFactory;
        $this->request = $request;
    }

    /**
     * @param string $name
     * @param array $params
     * @param bool $absolute
     * @return string
     */
    public function getUrl($name, $params = array(), $absolute = Router::ABSOLUTE_PATH)
    {
        return $this->router->generate($name, $params, $absolute);
    }

    /**
     * @param string $code
     * @param array $params
     * @return string
     */
    public function getLink($code, $params = NULL)
    {
        return $this->frouter->link($code, $params);
    }

    /**
     * @param string $message
     * @param string $type
     * @param bool $escape
     */
    public function setFlashMessage($message, $type = 'info', $escape = TRUE)
    {
        $session = $this->session->getNameSpace(self::SESSION_FLASH);
        $session->messages[] = ['text' => $message, 'type' => $type, 'escape' => $escape];
    }

    /**
     * @param string $message
     * @param bool $escape
     */
    public function setSuccessFlashMessage($message, $escape = TRUE)
    {
        $this->setFlashMessage($message, 'success', $escape);
    }

    /**
     * @param string $message
     * @param bool $escape
     */
    public function setInfoFlashMessage($message, $escape = TRUE)
    {
        $this->setFlashMessage($message, 'info', $escape);
    }

    /**
     * @param string $message
     * @param bool $escape
     */
    public function setErrorFlashMessage($message, $escape = TRUE)
    {
        $this->setFlashMessage($message, 'error', $escape);
    }

    /**
     * @return array
     */
    public function readFlashMessages()
    {
        $session = $this->session->getNameSpace(self::SESSION_FLASH);
        $flashes = $session->messages;
        $session->remove();
        return $flashes;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getSessionValue($key)
    {
        $session = $this->session->getNameSpace(self::SESSION_DEFAULT);
        return $session->$key;
    }

    /**
     * @param string $key
     * @param string $value
     */
    public function setSessionValue($key, $value)
    {
        $session = $this->session->getNameSpace(self::SESSION_DEFAULT);
        $session->$key = $value;
    }

    /**
     * @param mixed $type
     * @param mixed $data
     * @param array $options
     * @return FormInterface
     */
    public function buildForm($type, $data = NULL, $options = array())
    {
        $form = $this->formFactory->create($type, $data, $options);
        $form->handleRequest($this->request);
        return $form;
    }

    /**
     * @param string|Exception $message
     * @param string $url
     * @return JsonResponse|RedirectResponse
     */
    public function getAjaxResponse($message, $url)
    {
        if ($this->request->isXmlHttpRequest()) {
            $response = $message instanceof Exception
                ? new JsonResponse(array('error' => $message->getMessage()), JsonResponse::HTTP_SERVICE_UNAVAILABLE)
                : new JsonResponse(array('success' => TRUE, 'message' => $message));
        } else {
            $this->setFlashMessage($message instanceof Exception ? $message->getMessage() : $message);
            return new RedirectResponse($url);
        }
        return $response;
    }

    /**
     * @return mixed
     */
    public function getBacklink()
    {
        $namespace = $this->session->getNamespace(self::SESSION_BACKLINK);
        return isset($namespace->backlink) ? $namespace->backlink : FALSE;
    }

    public function removeBacklink()
    {
        $namespace = $this->session->getNamespace(self::SESSION_BACKLINK);
        $namespace->remove();
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }
}
