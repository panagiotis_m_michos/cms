<?php

namespace Services;

use Basket;
use DusanKasan\Knapsack\Collection;
use Generator;
use IPackage;
use Package;
use Repositories\Nodes\PackageRepository;

class PackageService
{
    /**
     * @var PackageRepository
     */
    private $packageRepository;

    /**
     * @param PackageRepository $packageRepository
     */
    public function __construct(PackageRepository $packageRepository)
    {
        $this->packageRepository = $packageRepository;
    }

    /**
     * @param $packageId
     * @return NULL|Package
     */
    public function getPackageById($packageId)
    {
        return $this->packageRepository->getPackageById($packageId);
    }

    /**
     * @param $packageId
     * @return IPackage|Package|null
     */
    public function getPublishedPackageById($packageId)
    {
        $package = $this->packageRepository->getPackageById($packageId);

        return ($package && $package->isOk2show()) ? $package : NULL;
    }

    /**
     * @return Generator|Package[]
     */
    public function getPublishedPackages()
    {
        foreach (array_keys(Package::$types) as $id) {
            $package = $this->getPublishedPackageById($id);

            if ($package && $package->isPublished()) {
                yield $package;
            }
        }
    }

    /**
     * @return array packageId => packageLngTitle
     */
    public function getPublishedPackagesPairs()
    {
        $pairs = [];

        foreach ($this->getPublishedPackages() as $package) {
            $pairs[$package->getId()] = $package->getLngTitle();
        }

        return $pairs;
    }

    /**
     * @param Package $package
     * @param Basket $basket
     * @return array
     */
    public function getAssociatedProductsNotInBasket(Package $package, Basket $basket)
    {
        $missingProducts = [];
        foreach ($package->getPackageProducts('associatedProducts3') as &$product) {
            if (!$basket->hasProduct($product->getId())) {
                $product->isAssociated = TRUE;
                $missingProducts[$product->getId()] = $product;
            }
        }

        return $missingProducts;
    }
}
