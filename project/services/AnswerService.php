<?php
    
namespace Services;

use Repositories\AnswerRepository;
use Entities\Answer;
use Entities\Customer;
use Nette\Object;
use DoctrineDataSource;

class AnswerService extends Object
{

    /**
     * @var AnswerRepository
     */
    private $answerRepository;

    /**
     * @param AnswerRepository $repository
     */
    public function __construct(AnswerRepository $answerRepository)
    {
        $this->answerRepository = $answerRepository;
    }

    /**
     * @param Customer $customer
     * @return Answer
     */
    public function getAnswerByCustomer(Customer $customer)
    {
        return $this->answerRepository->findOneBy(array('customer' => $customer));
    }

    /**
     * @param Answer $answer
     */
    public function saveAnswer(Answer $answer)
    {
        $this->answerRepository->saveEntity($answer);
    }

    /**
     * @return DoctrineDataSource
     */
    public function getListDatasource()
    {
        return new DoctrineDataSource($this->answerRepository->getListBuilder());
    }
}
