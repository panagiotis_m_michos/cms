<?php

namespace Services;

use Basket;
use DateTime;
use DibiConnection;
use Entities\Company;
use Entities\Order;
use Entities\OrderItem;
use Nette\Object;
use Repositories\OrderItemRepository;

class OrderItemService extends Object
{
    /**
     * @var OrderItemRepository
     */
    private $orderItemRepository;

    /**
     * @var DibiConnection
     */
    private $connection;

    /**
     * @param OrderItemRepository $orderItemRepository
     * @param DibiConnection $connection
     */
    public function __construct(OrderItemRepository $orderItemRepository, DibiConnection $connection)
    {
        $this->orderItemRepository = $orderItemRepository;
        $this->connection = $connection;
    }

    /**
     * @param int $id
     * @return OrderItem|NULL
     */
    public function getOrderItemById($id)
    {
        return $this->orderItemRepository->find($id);
    }

    /**
     * @param Company $company
     * @return OrderItem[]
     */
    public function getOrderItemsByCompany(Company $company)
    {
        return $this->orderItemRepository->findBy(['company' => $company]);
    }

    /**
     * @param OrderItem $orderItem
     */
    public function save(OrderItem $orderItem)
    {
        $this->orderItemRepository->saveEntity($orderItem);
    }

    /**
     * @param DateTime $dateTo
     * @return int
     */
    public function markItemsAsExported(DateTime $dateTo)
    {
        $affectedRows = 0;
        $affectedRows += $this->connection->query(
            'UPDATE cms2_orders_items oi
            LEFT JOIN ch_company co ON (oi.companyId = co.company_id)
            SET oi.dtExported = NOW()
            WHERE DATE(oi.dtc) <= %s
            AND (
                oi.incorporationRequired = FALSE
                OR (
                    oi.incorporationRequired = TRUE
                    AND co.company_number IS NOT NULL
                )
            )', $dateTo->format('Y-m-d')
        );

        $affectedRows += $this->connection->query(
            'UPDATE cms2_orders_items oi
            LEFT JOIN ch_company co ON (oi.orderId = co.order_id)
            SET oi.dtExported = NOW()
            WHERE DATE(oi.dtc) <= %s
            AND (
                oi.incorporationRequired = FALSE
                OR (
                    oi.incorporationRequired = TRUE
                    AND co.company_number IS NOT NULL
                )
            )', $dateTo->format('Y-m-d')
        );

        return $affectedRows;
    }
}
