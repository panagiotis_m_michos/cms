<?php

namespace Services\Facades\Wholesale;

use BarclaysBanking;
use Basket;
use Package;
use SessionNamespace;

class SearchFacade
{
    /**
     * @var SessionNamespace
     */
    private $sessionNamespace;

    /**
     * @var Basket
     */
    private $basket;

    /**
     * @param SessionNamespace $sessionNamespace
     * @param Basket $basket
     */
    public function __construct(SessionNamespace $sessionNamespace, Basket $basket)
    {
        $this->sessionNamespace = $sessionNamespace;
        $this->basket = $basket;
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->sessionNamespace->companyName;
    }

    /**
     * @param $companyName
     */
    public function saveCompanyName($companyName)
    {
        $this->sessionNamespace->companyName = $companyName;
    }

    /**
     * @param Package $package
     * @param $companyName
     */
    public function addPackage(Package $package, $companyName)
    {
        $package->companyName = $companyName;

        $this->basket->clear(FALSE);
        $this->basket->addPackage($package);
    }

} 
