<?php

namespace Services\Facades;

use AdminFee;
use Dispatcher\Events\Order\RefundEvent;
use Entities\Order;
use Entities\OrderItem;
use EventLocator;
use FUser;
use InvalidArgumentException;
use Nette\Object;
use Order\Refund\Calculator\Result as CalculatorResult;
use Order\Refund\Data;
use Services\OrderService;
use Symfony\Component\EventDispatcher\EventDispatcher;

class OrderRefundFacade extends Object
{
    /**
     * @var OrderService
     */
    private $orderService;

    /**
     * @var EventDispatcher
     */
    private $eventDispatcher;

    /**
     * @param OrderService $orderService
     * @param EventDispatcher $eventDispatcher
     */
    public function __construct(OrderService $orderService, EventDispatcher $eventDispatcher)
    {
        $this->orderService = $orderService;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param Order $order
     * @param Data $refundData
     * @param CalculatorResult $result
     * @param FUser $adminUser
     */
    public function refund(Order $order, Data $refundData, CalculatorResult $result, FUser $adminUser)
    {
        $totalCreditRefund = $result->getTotalCreditRefund();
        $totalMoneyRefund = $result->getTotalMoneyRefund();
        $orderItems = $refundData->getOrderItems()->toArray();

        $order->setIsRefunded(TRUE);
        $order->setStatusId(Order::STATUS_CANCELLED);
        $order->setRefundCustomerSupportLogin($adminUser->login);
        $order->setDescription($refundData->getDescription());
        $order->setRefundCreditValue($totalCreditRefund);
        $order->setRefundValue($totalMoneyRefund);

        $this->addAdminFeeItem($order, $result->getAdminFee());

        $this->addRefundItems($order, $orderItems);

        $this->orderService->saveOrder($order);

        // dispatch event
        $refundEvent = new RefundEvent($order, $orderItems, $totalCreditRefund, $totalMoneyRefund);
        $this->eventDispatcher->dispatch(EventLocator::ORDER_REFUNDED, $refundEvent);
    }

    /**
     * @param Order $order
     * @param OrderItem[] $refundItems
     * @throws InvalidArgumentException
     */
    private function addRefundItems(Order $order, array $refundItems)
    {
        foreach ($refundItems as $refundedItem) {
            $refundItem = clone $refundedItem;
            $refundItem->setIsRefund(TRUE);
            $refundItem->setQty(-1 * $refundItem->getQty());
            $refundItem->setPrice(-1 * $refundItem->getPrice());
            $refundItem->setSubTotal(-1 * $refundItem->getSubTotal());
            $refundItem->setVat(-1 * $refundItem->getVat());
            $refundItem->setTotalPrice(-1 * $refundItem->getTotalPrice());
            $refundItem->setRefundedOrderItem($refundedItem);
            $order->addItem($refundItem);

            // mark refunded item
            $refundedItem->setIsRefunded(TRUE);
        }
    }

    /**
     * @param Order $order
     * @param float $adminFee
     */
    private function addAdminFeeItem(Order $order, $adminFee)
    {
        if ($adminFee > 0) {
            $item = new OrderItem($order);
            $item->setProduct(new AdminFee(AdminFee::ADMIN_FEE_PRODUCT));
            $item->setQty(1);
            $item->setPrice($adminFee);
            $item->setSubTotal($adminFee);
            $item->setVat(0.00);
            $item->setTotalPrice($adminFee);
            $item->setIsFee(TRUE);
            $item->setNotApplyVat(FALSE);
            $item->setNonVatableValue(0);
            $order->addItem($item);
        }
    }
}
