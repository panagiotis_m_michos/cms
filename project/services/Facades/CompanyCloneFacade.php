<?php

namespace Services\Facades;

use Services\CompanyService;
use Services\OrderService;
use Entities\Company;
use dibi;
use Exceptions\Business\MissingSubmission;
use Nette\Object;
use Entities\Customer;
use Entities\CompanyHouse\FormSubmission;
use Entities\Order;
use Document;

class CompanyCloneFacade extends Object
{

    const COPY_DOCUMENT = TRUE;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var OrderService
     */
    private $orderService;

    /**
     *
     * @param CompanyService $companyService
     * @param OrderService $orderService
     */
    public function __construct(CompanyService $companyService, OrderService $orderService)
    {
        $this->companyService = $companyService;
        $this->orderService = $orderService;
    }

    /**
     * @param Customer $customer
     * @param Company $company
     * @param string $newCompanyName
     * @return Company
     *
     * @throws MissingSubmission
     */
    public function copyCompany(Customer $customer, Company $oldCompany, $newCompanyName)
    {
        return $this->handleCopyCompany($customer, $oldCompany, $newCompanyName, !self::COPY_DOCUMENT);
    }

    /**
     * @param Customer $customer
     * @param Company $company
     * @param string $newCompanyName
     * @return Company
     *
     * @throws MissingSubmission
     */
    public function copyFullCompany(Customer $customer, Company $oldCompany, $newCompanyName)
    {
        $company = $this->handleCopyCompany($customer, $oldCompany, $newCompanyName, self::COPY_DOCUMENT);
        if ($company->getOrder()) {
            $this->copyOrder($company->getOrder(), $company);
        }
        return $company;
    }

    /**
     * copy order with its items to new company
     *
     * @param Order $order
     * @param Company $newCompany
     *
     * @return Order
     */
    public function copyOrder(Order $order, Company $newCompany)
    {
        // clone construct creates a shallow copy (references remain). using custom method
        $newOrder = $order->cloneObject();
        foreach ($newOrder->getItems() as $orderItem) {
            $orderItem->setCompany($newCompany);
        }
        $this->orderService->saveOrder($newOrder);
        $newCompany->setOrder($newOrder);
        $this->companyService->saveCompany($newCompany);
        return $newOrder;
    }

    /**
     * @param mixed $formSubmission
     * @param Company $company
     *
     * @return mixed $newFormSubmission
     */
    public function copyFormSubmission($formSubmission, Company $company)
    {
        $newFormSubmission = clone $formSubmission;
        $newFormSubmission->company_id = $company->getId();
        unset($newFormSubmission->form_submission_id);
        $newFormSubmission->form_submission_id = dibi::insert(TBL_FORM_SUBMISSIONS, $newFormSubmission)->execute(dibi::IDENTIFIER);
        return $newFormSubmission;
    }

    /**
     * @param mixed $companyIncorporation
     * @param integer $newFormSubmissionId
     */
    public function copyIncorporation($companyIncorporation, $newFormSubmissionId)
    {
        $newIncorporation = clone $companyIncorporation;
        $newIncorporation->form_submission_id = $newFormSubmissionId;
        $newIncorporation->reject_reference = NULL; // make sure that we don't copy these
        $newIncorporation->reject_description = NULL;
        dibi::insert(TBL_COMPANY_INCORPORATIONS, $newIncorporation)->execute();
    }

    /**
     * @param mixed $member
     * @param integer $newFormSubmissionId
     */
    public function copyMember($member, $newFormSubmissionId)
    {
        $newMember = clone $member;
        $newMember->form_submission_id = $newFormSubmissionId;
        unset($newMember->incorporation_member_id);
        dibi::insert(TBL_INCORPORATION_MEMBERS, $newMember)->execute();
    }

    /**
     *
     * @param mixed $document
     * @param integer $formSubmissionId
     * @param Company $oldCompany
     * @param Company $newCompany
     */
    public function copyDocument($document, $formSubmissionId, Company $oldCompany, Company $newCompany)
    {
        $newDocument = clone $document;
        // --- insert new form submission id ---
        $newDocument->form_submission_id = $formSubmissionId;
        $newDocument->file_path = NULL;
        unset($newDocument->document_id);
        $newDocument->document_id = dibi::insert(TBL_DOCUMENTS, $newDocument)->execute(dibi::IDENTIFIER);
        $oldDocumentModel = Document::getDocument($document->document_id, $oldCompany->getId());
        $newDocumentModel = Document::getDocument($newDocument->document_id, $newCompany->getId());
        if (file_exists($oldDocumentModel->getFullPath())) {
            // --- create folder if needed ---
            if (!is_dir($newDocumentModel->getFilePath())) {
                mkdir($newDocumentModel->getFilePath(), 0777, TRUE);
            }
            copy($oldDocumentModel->getFullPath(), $newDocumentModel->getFullPath());
        }
    }

    /**
     * @param Customer $customer
     * @param Company $company
     * @param string $newCompanyName
     * @return Company
     *
     * @throws MissingSubmission
     */
    private function handleCopyCompany(Customer $customer, Company $oldCompany, $newCompanyName, $copyDocuments = TRUE)
    {
        $lastFormSubmission = dibi::select('*')->from(TBL_FORM_SUBMISSIONS)
                ->where('form_identifier = %s', FormSubmission::TYPE_COMPANY_INCORPORATION)
                ->where('company_id = %i', $oldCompany->getId())
                ->where('date_cancelled IS NULL')
                ->orderBy('form_submission_id', 'DESC')->execute()->fetch();

        if (empty($lastFormSubmission)) {
            throw new MissingSubmission("Copied company doesn't have incorporation submission - nothing to be copied");
        }

        //copy company
        $newCompany = clone $oldCompany;
        $newCompany->setCompanyName($newCompanyName);
        $this->companyService->saveCompany($newCompany);

        $newFormSubmission = $this->copyFormSubmission($lastFormSubmission, $newCompany);

        // copy incorporation
        $oldIncorporation = dibi::select('*')->from(TBL_COMPANY_INCORPORATIONS)
                ->where('form_submission_id = %s', $lastFormSubmission->form_submission_id)
                ->limit(1)->execute()->fetch();
        $this->copyIncorporation($oldIncorporation, $newFormSubmission->form_submission_id);


        // copy members
        $incMembers = dibi::select('*')->from(TBL_INCORPORATION_MEMBERS)
                ->where('form_submission_id = %s', $lastFormSubmission->form_submission_id)
                ->execute()->fetchAll();
        foreach ($incMembers as $member) {
            $this->copyMember($member, $newFormSubmission->form_submission_id);
        }

        if ($copyDocuments) {
            // copy documents
            $documents = dibi::select('*')->from(TBL_DOCUMENTS)
                    ->where('form_submission_id = %s', $lastFormSubmission->form_submission_id)
                    ->execute()->fetchAll();
            foreach ($documents as $oldDocument) {
                $this->copyDocument($oldDocument, $newFormSubmission->form_submission_id, $oldCompany, $newCompany);
            }
        }

        return $newCompany;
    }

}
