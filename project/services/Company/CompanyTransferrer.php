<?php

namespace Services\Company;

use Dispatcher\Events\CompanyTransferredEvent;
use Entities\Company;
use Entities\Customer;
use EventLocator;
use Services\CompanyService;
use Symfony\Component\EventDispatcher\EventDispatcher;

class CompanyTransferrer
{
    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var EventDispatcher
     */
    private $eventDispatcher;

    /**
     * @param CompanyService $companyService
     * @param EventDispatcher $eventDispatcher
     */
    public function __construct(
        CompanyService $companyService,
        EventDispatcher $eventDispatcher
    )
    {
        $this->companyService = $companyService;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param Company $company
     * @param Customer $newCustomer
     */
    public function transfer(Company $company, Customer $newCustomer)
    {
        $company->setCustomer($newCustomer);
        $order = $company->getOrder();
        if ($order) {
            $order->setCustomer($newCustomer);
        }

        $this->companyService->flush();
        $this->eventDispatcher->dispatch(EventLocator::COMPANY_TRANSFERRED, new CompanyTransferredEvent($company));
    }
}
