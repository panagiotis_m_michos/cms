<?php

namespace Services;

use BasketProduct;
use Entities\Company;
use Entities\Payment\Token;
use Object;
use Entities\Service;
use Product;
use Package;
use IPackage;
use Symfony\Component\EventDispatcher\EventDispatcher;
use EventLocator;
use Dispatcher\Events\ServicePurchasedEvent;
use DateTime;

class ProductService extends Object
{
    /**
     * @var ServiceService
     */
    private $serviceService;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var EventDispatcher
     */
    private $eventDispatcher;

    /**
     * @param ServiceService $serviceService
     * @param CompanyService $companyService
     * @param EventDispatcher $eventDispatcher
     */
    public function __construct(
        ServiceService $serviceService,
        CompanyService $companyService,
        EventDispatcher $eventDispatcher
    )
    {
        $this->serviceService = $serviceService;
        $this->companyService = $companyService;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param array $items
     * @param Token $renewalToken
     */
    public function saveServices(array $items, Token $renewalToken = NULL)
    {
        /** @var Product $product */
        foreach ($items as $product) {
            if ($product instanceof IPackage) {
                /** @var Package $package */
                $package = $product;
                if ($package->hasServiceType()) {
                    $company = $this->companyService->getCompanyById($package->getCompanyId());
                    $this->savePackage($package, $company, $renewalToken);
                }
            } elseif ($product->hasServiceType()) {
                $company = $this->companyService->getCompanyById($product->getCompanyId());
                $this->saveProduct($product, $company, $renewalToken);
            }
        }
    }

    /**
     * @param IPackage $package
     * @param Company $company
     * @param Token|NULL $renewalToken
     */
    private function savePackage(IPackage $package, Company $company, Token $renewalToken = NULL)
    {
        $parentService = $this->createService($package, $company);
        $included = $package->getPackageProducts('includedProducts');
        /** @var BasketProduct $product */
        foreach ($included as $product) {
            if ($product->hasServiceType()) {
                $childService = new Service($product->serviceTypeId, $product, $company, $package->orderItem);
                $parentService->addChild($childService);
            }
        }
        $this->serviceService->saveService($parentService);
        $this->eventDispatcher->dispatch(EventLocator::SERVICE_PURCHASED, new ServicePurchasedEvent($parentService, $renewalToken));
    }

    /**
     * @param Product $product
     * @param Company $company
     * @param Token|NULL $renewalToken
     */
    private function saveProduct(Product $product, Company $company, Token $renewalToken = NULL)
    {
        $service = $this->createService($product, $company);
        $this->serviceService->saveService($service);
        $this->eventDispatcher->dispatch(EventLocator::SERVICE_PURCHASED, new ServicePurchasedEvent($service, $renewalToken));
    }

    /**
     * @param Product $product
     * @param Company $company
     * @return Service
     */
    private function createService(Product $product, Company $company)
    {
        $service = new Service($product->serviceTypeId, $product, $company, $product->orderItem);
        $company->addService($service);
        // if renewal product does not exist we assume its one off service
        if ($product->hasRenewalProduct()) {
            $service->setRenewalProduct($product->getRenewalProduct());
        } else {
            $service->setDtStart(new DateTime());
        }

        return $service;
    }

}
