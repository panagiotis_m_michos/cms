<?php

namespace Services;

use Entities\Event;
use Repositories\EventRepository;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\Event as DispatcherEvent;
use DateInterval;
use DateTime;

class EventService
{
    /**
     * @var EventRepository
     */
    private $eventRepository;

    /**
     * @var EventDispatcher
     */
    private $eventDispatcher;

    /**
     * @param EventRepository $eventRepository
     * @param EventDispatcher $eventDispatcher
     */
    public function __construct(EventRepository $eventRepository, EventDispatcher $eventDispatcher)
    {
        $this->eventRepository = $eventRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param string $eventKey
     * @param int $objectId
     * @return Event
     */
    public function getEvent($eventKey, $objectId)
    {
        $event = $this->eventRepository->findOneBy(
            array(
                'eventKey' => $eventKey,
                'objectId' => $objectId
            ),
            array('eventId' => 'DESC')
        );
        return $event;
    }

    /**
     * @param Event $event
     */
    private function save(Event $event)
    {
        $this->eventRepository->saveEntity($event);
    }

    /**
     * @param string $eventKey
     * @param int $objectId
     */
    public function notify($eventKey, $objectId)
    {
        $event = new Event();
        $event->setEventKey($eventKey);
        $event->setObjectId($objectId);
        $this->save($event);
    }

    /**
     * did particular event occurred
     * @param string $eventKey
     * @param int $objectId
     * @return bool
     */
    public function eventOccurred($eventKey, $objectId)
    {
        $event = $this->getEvent($eventKey, $objectId);
        return $event ? TRUE : FALSE;
    }

    /**
     * did particular event occurred in a specified date interval
     * @param string $eventKey
     * @param int $objectId
     * @param DateInterval $dateInterval
     * @return boolean
     */
    public function eventOccurredIn($eventKey, $objectId, DateInterval $dateInterval)
    {
        $event = $this->getEvent($eventKey, $objectId);
        if (!$event) {
            return FALSE;
        }
        $now = new DateTime();
        $now->setTime(0, 0, 0);
        $dateOccurred = clone $event->getDtc();
        $dateOccurred->setTime(0, 0, 0);
        return $now->sub($dateInterval) <= $dateOccurred;
    }

    /**
     * dispatch event and save it to events
     *
     * @param string $eventKey
     * @param int $objectId
     * @param DispatcherEvent $event
     */
    public function dispatch($eventKey, $objectId, DispatcherEvent $event = NULL)
    {
        if (!$this->eventOccurred($eventKey, $objectId)) {
            $this->eventDispatcher->dispatch($eventKey, $event);
            $this->notify($eventKey, $objectId);
        }
    }

}
