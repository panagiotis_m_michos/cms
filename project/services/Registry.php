<?php

use Symfony\Component\DependencyInjection\Container;

/**
 * holds statically accessible properties
 * */
class Registry
{
    public static $emailerFactory;

    /**
     * @var Container
     */
    public static $container;

    /**
     * @param string $name
     * @return mixed
     */
    public static function getService($name)
    {
        return self::$container->get($name);
    }

}
