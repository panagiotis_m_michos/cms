<?php

namespace Services;

use DateTime;
use DibiFluent;
use Doctrine\ORM\Internal\Hydration\IterableResult;
use DoctrineDataSource;
use Entities\Cashback;
use Entities\Company;
use Entities\Customer;
use LogicException;
use Nette\Object;
use Repositories\CashbackRepository;

class CashbackService extends Object
{
    /**
     * @var CashbackRepository
     */
    private $cashbackRepository;

    /**
     * @param CashbackRepository $cashbackRepository
     */
    public function __construct(CashbackRepository $cashbackRepository)
    {
        $this->cashbackRepository = $cashbackRepository;
    }

    /**
     * @param Cashback $entity
     */
    public function saveCashback(Cashback $entity)
    {
        $this->cashbackRepository->saveEntity($entity);
    }

    /**
     * @return mixed
     */
    public function getEligibleCashbacksDataSource()
    {
        return $this->cashbackRepository->getEligibleCashbacksDataSource();
    }

    /**
     * @param Customer $customer
     * @param $packageTypeId
     * @return Cashback[]
     */
    public function getCustomerEligibleCashbacks(Customer $customer, $packageTypeId)
    {
        return $this->cashbackRepository->getCustomerEligibleCashbacks($customer, $packageTypeId);
    }

    /**
     * @param Customer $customer
     * @return DoctrineDataSource
     */
    public function getCustomerCashbacksDataSource(Customer $customer)
    {
        return new DoctrineDataSource($this->cashbackRepository->getCustomerCashbacksDataSource($customer));
    }

    /**
     * @param Customer $customer
     * @return Cashback[] array
     */
    public function getCustomerImportedCashbacks(Customer $customer)
    {
        return $this->cashbackRepository->findBy(
            array(
                'customer' => $customer,
                'statusId' => Cashback::STATUS_IMPORTED,
            )
        );
    }

    /**
     * @param Customer $customer
     * @return IterableResult
     */
    public function getCustomerCashbacksToUnarchive(Customer $customer)
    {
        return $this->cashbackRepository->getCustomerCashbacksToUnarchive($customer);
    }

    /**
     * @param Customer $customer
     * @return DibiFluent
     */
    public function getCashbacksDataSource(Customer $customer = NULL)
    {
        return $this->cashbackRepository->getCashbacksDataSource($customer);
    }

    /**
     * @param Company $company
     * @return Cashback[]
     */
    public function getCashbackByCompany(Company $company)
    {
        return $this->cashbackRepository->findBy(['company' => $company]);
    }

    /**
     * @param Company $company
     * @param string $bankTypeId
     * @return Cashback
     */
    public function getBankCashBackByCompany(Company $company, $bankTypeId)
    {
        return $this->cashbackRepository->findOneBy(['company' => $company, 'bankTypeId' => $bankTypeId]);
    }

    /**
     * @param int $id
     * @return Cashback
     */
    public function getCashbackById($id)
    {
        return $this->cashbackRepository->find($id);
    }

    /**
     * @param Company $company
     * @return int
     */
    public function getCashBackAmountForCompany(Company $company)
    {
        return $company->getCashBackAmount();
    }

    /**
     * @param Cashback[] $eligibleCashbacks
     * @throws LogicException
     */
    public function markCashbacksAsPaid(array $eligibleCashbacks)
    {
        $groupId = uniqid(NULL, TRUE);
        foreach ($eligibleCashbacks as $cashback) {
            if (!$cashback->isEligible()) {
                throw new LogicException("Cashback `{$cashback->getId()}` is not eligible for payment`");
            }
            $customer = $cashback->getCustomer();
            $cashback->setStatusId(Cashback::STATUS_PAID);
            $cashback->setCashbackTypeId($customer->getCashbackType());
            $cashback->setAccountNumber($customer->getAccountNumber());
            $cashback->setSortCode($customer->getSortCode());
            $cashback->setGroupId($groupId);
            $cashback->setDatePaid(new DateTime);
        }
        $this->saveCashbacks($eligibleCashbacks);
    }

    /**
     * @param Cashback[] $cashbacks
     * @throws LogicException
     */
    public function markCashbacksAsArchived(array $cashbacks)
    {
        foreach ($cashbacks as $cashback) {
            $cashback->setIsArchived(TRUE);
        }
        $this->saveCashbacks($cashbacks);
    }

    /**
     * @param Cashback[] $cashbacks
     * @throws LogicException
     */
    public function markCashbacksAsActive(array $cashbacks)
    {
        foreach ($cashbacks as $cashback) {
            $cashback->setIsArchived(FALSE);
        }
        $this->saveCashbacks($cashbacks);
    }

    /**
     * @param Customer $customer
     * @param $packageTypeId
     * @param $statusId
     * @param NULL $groupId
     * @return array
     */
    public function getGroupedCashbacks(Customer $customer, $packageTypeId, $statusId, $groupId = NULL)
    {
        return $this->cashbackRepository->getGroupedCashbacks($customer, $packageTypeId, $statusId, $groupId);
    }

    /**
     * @param Cashback $cashBack
     * @return bool
     */
    public function isImportableForBarclays(Cashback $cashBack)
    {
        $oldDate = strtotime('-3 months');

        return ($cashBack->getDateAccountOpened()->getTimestamp() > $oldDate);
    }

    /**
     * @param array $cashbacks
     */
    private function saveCashbacks(array $cashbacks)
    {
        foreach ($cashbacks as $cashback) {
            $this->saveCashback($cashback);
        }
    }
}
