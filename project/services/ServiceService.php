<?php

namespace Services;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Internal\Hydration\IterableResult;
use Entities\Company;
use Entities\Order;
use Entities\OrderItem;
use Entities\ServiceSettings;
use Exceptions\Technical\OrderRefundException;
use Object;
use Repositories\ServiceRepository;
use Entities\Service;
use DoctrineDataSource;
use Entities\Customer as CustomerEntity;
use EntityNotFound;
use Utils\Date;

class ServiceService extends Object
{
    /**
     * @var ServiceRepository
     */
    private $repository;

    /**
     * @param ServiceRepository $repository
     */
    public function __construct(ServiceRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param int $id
     * @return Service
     */
    public function getServiceById($id)
    {
        return $this->repository->find($id);
    }

    /**
     * @param array $ids
     * @return Service[]
     */
    public function getServicesByIds(array $ids = [])
    {
        return $this->repository->getServicesByIds($ids);
    }

    /**
     * @param Service $entity
     */
    public function deleteService(Service $entity)
    {
        $this->repository->removeEntity($entity);
    }

    /**
     * @param Service $entity
     */
    public function saveService(Service $entity)
    {
        $this->repository->saveEntity($entity);
    }

    /**
     * @param Service $service
     */
    public function flush(Service $service = NULL)
    {
        $this->repository->flush($service);
    }

    /**
     * @param Company $company
     * @param $serviceTypeId
     * @param Service $service (if provided this service will be ignored)
     * @return Service
     */
    public function getLastService(Company $company, $serviceTypeId, Service $service = NULL)
    {
        return $this->repository->getLastService($company, $serviceTypeId, $service);
    }

    /**
     * @return DoctrineDataSource
     */
    public function getListDatasource()
    {
        return new DoctrineDataSource($this->repository->getListBuilder());
    }

    /**
     * @param Company $company
     * @return DoctrineDataSource
     */
    public function getListDatasourceForCompany(Company $company)
    {
        return new DoctrineDataSource($this->repository->getListBuilder($company));
    }

    /**
     * @param CustomerEntity $customer
     * @param $serviceId
     * @return Service
     * @throws EntityNotFound
     */
    public function getCustomerService(CustomerEntity $customer, $serviceId)
    {
        $service = $this->getServiceById($serviceId);
        if (!$service) {
            throw new EntityNotFound("Service $serviceId doesn't exist");
        }
        $serviceCustomer = $service->getCompany()->getCustomer();
        if (!$serviceCustomer->isEqual($customer)) {
            throw new EntityNotFound("Service $serviceId doesn't exist");
        }
        return $service;
    }

    /**
     * @param OrderItem $orderItem
     * @return Service
     * @throws OrderRefundException
     */
    public function getOrderItemService(OrderItem $orderItem)
    {
        $service = $this->repository->getServiceByOrderItem($orderItem);
        if ($service) {
            return $service;
        }

        // S546 - kept old behaviour to handle old services without orderItemId
        $services = $this->repository->getOrderItemServices($orderItem);
        if ($services) {
            if (count($services) > 1) {
                // case one order: 2x SA for different companies
                foreach ($services as $service) {
                    $orderCompany = $orderItem->getCompany();
                    if ($orderCompany && $orderCompany->isEqual($service->getCompany())) {
                        return $service;
                    }
                }

                throw OrderRefundException::multipleServicesDetected($orderItem);

            } else {
                return $services[0];
            }
        }
    }

    /**
     * @param Order $order
     * @return IterableResult
     */
    public function getOrderServices(Order $order)
    {
        return $this->repository->getOrderServices($order);
    }

    /**
     * @param int $productId
     * @return IterableResult
     */
    public function getServicesByProductId($productId)
    {
        return $this->repository->getServicesByProductId($productId);
    }

    /**
     * @param Date $from
     * @param Date $to
     * @param string $event
     * @return IterableResult
     */
    public function getServicesDataExpiringWithinDatesForEvent(Date $from, Date $to, $event)
    {
        return $this->repository->getServicesDataExpiringWithinDatesForEvent($from, $to, $event);
    }

    /**
     * @return IterableResult
     */
    public function getRefundedServices()
    {
        return $this->repository->getRefundedServices();
    }

    /**
     * @param Service $service
     * @param Date|DateTime $dtStart
     * @param Date|DateTime $dtExpires
     */
    public function setServiceDates(Service $service, $dtStart, $dtExpires)
    {
        $service->setDtStart($dtStart->setTime(0, 0, 0));
        $service->setDtExpires($dtExpires->setTime(0, 0, 0));
        $this->repository->flush($service);
    }

    /**
     * @param Company $company
     * @param DateTime $date
     * @return Service[]|ArrayCollection
     */
    public function getCompanyNonExpiredServicesOn(Company $company, DateTime $date)
    {
        return $company->getNonExpiredServicesOn($date);
    }

    /**
     * @param Service $service
     * @return bool
     */
    public function isFormationServiceForCompany(Service $service)
    {
        $companyOrder = $service->getCompany()->getOrder();

        return $companyOrder && $service->getOrder()->isEqual($companyOrder);
    }

    /**
     * @param Service $service
     */
    public function markAsDowngraded(Service $service)
    {
        $service->setStateId(Service::STATE_DOWNGRADED);
        foreach ($service->getChildren() as $childService) {
            $childService->setStateId(Service::STATE_DOWNGRADED);
            $this->repository->flush($childService);
        }
        $this->repository->flush($service);
    }

    /**
     * @param Service $service
     */
    public function markAsUpgraded(Service $service)
    {
        $service->setStateId(Service::STATE_UPGRADED);
        foreach ($service->getChildren() as $childService) {
            $childService->setStateId(Service::STATE_UPGRADED);
            $this->repository->flush($childService);
        }
        $this->repository->flush($service);
    }

    /**
     * @param ServiceSettings $settings
     * @return Service
     */
    public function getLatestServiceBySettings(ServiceSettings $settings)
    {
        return $this->repository->getLatestServiceBySettings($settings);
    }

    /**
     * @param Company $company
     * @param $serviceTypeId
     * @return Service[]
     */
    public function getCompanyServicesByType(Company $company, $serviceTypeId)
    {
        return $this->repository->getCompanyServicesByType($company, $serviceTypeId);
    }

    /**
     * @return Service[]
     */
    public function getServicesToSendRemindersFor()
    {
        return $this->repository->getServicesToSendRemindersFor();
    }
}
