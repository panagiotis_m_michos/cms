<?php

namespace Services;

use EntityNotFound;
use Object;
use Repositories\CustomerRepository;
use Entities\Customer;
use Dispatcher\Events\CustomerEvent;
use EventLocator;
use Symfony\Component\EventDispatcher\EventDispatcher;

class CustomerService extends Object
{
    /**
     * @var CustomerRepository
     */
    private $repository;

    /**
     * @var EventDispatcher
     */
    private $dispatcher;

    /**
     * @param CustomerRepository $repository
     * @param EventDispatcher $dispatcher
     */
    public function __construct(CustomerRepository $repository, EventDispatcher $dispatcher)
    {
        $this->repository = $repository;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param Customer $entity
     */
    public function save(Customer $entity)
    {
        $this->repository->saveEntity($entity);
    }

    /**
     * @param int $id
     * @param bool $need
     * @throws EntityNotFound
     * @return Customer
     */
    public function getCustomerById($id, $need = FALSE)
    {
        $entity = $this->repository->find($id);
        if (!$entity && $need) {
            throw new EntityNotFound("Customer `$id`` not found");
        }
        return $entity;
    }

    /**
     * @param string $email
     * @param bool $need
     * @throws EntityNotFound
     * @return Customer
     */
    public function getCustomerByEmail($email, $need = FALSE)
    {
        $entity = $this->repository->findOneBy(array('email' => $email));
        if (!$entity && $need) {
            throw new EntityNotFound("Customer `$email`` not found");
        }
        return $entity;
    }

    /**
     * @param Customer $customer
     * @param array $data
     */
    public function update(Customer $customer, array $data)
    {
        if (!empty($data['email'])) {
            $customer->setEmail($data['email']);
        }
        if (!empty($data['password'])) {
            $customer->setPassword($data['password']);
        }

        $customer->setTitleId($data['titleId']);
        $customer->setFirstName($data['firstName']);
        $customer->setLastName($data['lastName']);
        $customer->setCompanyName($data['companyName']);
        $customer->setAddress1($data['address1']);
        $customer->setAddress2($data['address2']);
        $customer->setAddress3($data['address3']);
        $customer->setCity($data['city']);
        $customer->setCounty($data['county']);
        $customer->setPostcode($data['postcode']);
        $customer->setCountryId($data['countryId']);
        $customer->setPhone($data['phone']);
        $customer->setAdditionalPhone($data['additionalPhone']);
        $customer->setHowHeardId($data['howHeardId']);
        $customer->setIndustryId($data['industryId']);
        $customer->setIsSubscribed($data['isSubscribed']);

        // save wholesale questions data
        if (isset($data['userType'])) {
            $customer->setRoleId($data['userType']);
            $customer->setMyDetailsQuestion(1);
        }
        if (isset($data['memberICAEW'])) {
            if ($data['memberICAEW'] == 'yes') {
                $customer->setTagId(Customer::TAG_ICAEW);
            } elseif ($data['memberICAEW'] == 'no') {
                $customer->setTagId(Customer::TAG_GENERALWHOLESALE);
            }
        }
        $this->save($customer);
        $this->dispatcher->dispatch(EventLocator::CUSTOMER_UPDATED, new CustomerEvent($customer));
    }

    /**
     * @param Customer $customer
     * @param float $credit
     */
    public function addCredit(Customer $customer, $credit)
    {
        $customer->addCredit($credit);
        $this->save($customer);
    }

}
