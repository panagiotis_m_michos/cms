<?php

use Dispatcher\Events\OrderEvent;
use Dispatcher\Events\PaymentEvent;
use Entities\Customer as CustomerEntity;
use Entities\Payment\Token;
use Entities\Transaction as TransactionEntity;
use ErrorModule\Ext\DebugExt;
use Exceptions\Business\CompanyImportException;
use Exceptions\Technical\ServiceActivationException;
use OrderModule\Config\DiLocator as OrderDiLocator;
use OrderModule\Processors\OrderItemsProcessor;
use PayByPhoneModule\Forms\PayByPhoneForm;
use PaymentModule\Exceptions\PaymentException;
use Psr\Log\LoggerInterface;
use Services\CustomerService as DoctrineCustomerService;
use Services\FeefoService;
use Services\OrderService as OrderEntityService;
use Services\ProductService;
use Symfony\Component\EventDispatcher\EventDispatcher;

/**
 * @deprecated  use PaymentModule\Services\PaymentService
 */
class PaymentService extends Object
{
    const CUSTOMER_SIGN_IN = TRUE;

    /**
     * @var Basket
     */
    private $basket;

    /**
     * Customer
     */
    private $customer;

    /**
     * Callbacks on finished
     *
     * @var array
     */
    private $onFinish;

    /**
     * @var array
     */
    private $query;

    /**
     * @var array
     */
    private $post;

    /**
     * @var PaymentEmailer
     */
    private $paymentEmailer;

    /**
     * @var Customer
     */
    private $customerEntity;

    /**
     * @var OrderEntityService
     */
    private $orderEntityService;

    /**
     * @var ProductService
     */
    private $productService;

    /**
     * @var TokensModel
     */
    private $tokenModel;

    /**
     * @var FeefoService
     */
    private $feefoService;

    /**
     * @var EventDispatcher
     */
    private $eventDispatcher;

    /**
     * @var DoctrineCustomerService
     */
    private $customerService;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var OrderItemsProcessor
     */
    private $orderItemsProcessor;

    /**
     * @param Customer $customer
     * @param Basket $basket
     * @param CustomerEntity $customerEntity
     * @param OrderEntityService $orderEntityService
     * @param ProductService $productService
     * @param TokensModel $tokenModel
     */
    public function __construct(
        Customer $customer,
        Basket $basket,
        CustomerEntity $customerEntity = NULL,
        OrderEntityService $orderEntityService = NULL,
        ProductService $productService = NULL,
        TokensModel $tokenModel = NULL
    )
    {
        $this->basket = $basket;
        $this->customer = $customer;
        $this->customerEntity = $customerEntity;
        $this->orderEntityService = $orderEntityService;
        $this->productService = $productService;
        $this->tokenModel = $tokenModel;

        // @todo: move to constructor
        $this->paymentEmailer = Registry::$container->get(DiLocator::EMAILER_PAYMENT);
        $this->eventDispatcher = Registry::$container->get(DiLocator::DISPATCHER);
        $this->feefoService = Registry::$container->get(DiLocator::SERVICE_FEEFO);
        $this->customerService = Registry::$container->get(DiLocator::SERVICE_CUSTOMER);
        $this->orderItemsProcessor = Registry::$container->get(OrderDiLocator::ORDER_ITEMS_PROCESSOR);
        $this->logger = Registry::$container->get(DebugExt::LOGGER);
    }

    /**
     * @param PaymentResponse $paymentResponse
     * @param Token $token
     * @return int
     * @throws CompanyImportException
     * @throws Exception
     */
    public function completeTransaction(PaymentResponse $paymentResponse, Token $token = NULL)
    {
        try {
            $paymentEvent = new PaymentEvent($this->customerEntity, $this->basket);
            $this->eventDispatcher->dispatch(
                EventLocator::PAYMENT_SUCCEEDED,
                $paymentEvent
            );

            $notImportedCompanies = $paymentEvent->getNotImportedCompanies();
            if (!empty($notImportedCompanies)) {
                foreach ($notImportedCompanies as $basketItemid => $notImportedCompany) {
                    /** @var BasketProduct $product */
                    $product = $this->basket->items[$basketItemid];

                    $this->paymentEmailer->sendInternalPurchaseCompanyConflictEmail(
                        $this->customerEntity,
                        $product,
                        $notImportedCompany
                    );
                }

                throw CompanyImportException::companiesAlreadyExistContact($notImportedCompanies);
            }

            $orderService = new OrderService();
            $orderId = $orderService->saveOrder($this->customer, $this->basket);

            $this->orderItemsProcessor->saveItems($this->customer, $this->basket->getItems(), $orderId);

            $transactionService = new TransactionService();
            $transactionService->saveTransaction($paymentResponse, $this->customer, $orderId, $token);

            try {
                //save to service table
                $this->productService->saveServices($this->basket->getItems(), $token);
            } catch (ServiceActivationException $e) {
                $this->logger->error($e);
            }

            $orderEntity = $this->orderEntityService->getOrderById($orderId);
            $this->paymentSuccess();
            $this->eventDispatcher->dispatch(EventLocator::ORDER_COMPLETED, new OrderEvent($orderEntity));

            //send emails
            $this->paymentEmailer->orderConfirmationEmail($this->customer, $this->basket, $orderId);
            $this->paymentEmailer->orderConfirmationInternalEmail($this->customer, $this->basket, $orderId);

            // for ecommerce
            $this->basket->orderId = $orderId;

            // feefo
            foreach ($this->basket->getItems() as $item) {
                if ($item->isFeefo()) {
                    $this->feefoService->addFeefo($orderEntity);
                    break;
                }
            }

            // disable upgrade voucher service
            VoucherService::disableUpgradeVoucherForm();
            return $orderId;
        } catch (CompanyImportException $e) {
            // just rethrow to show this message to customer + internal mail has been sent
            throw $e;
        } catch (Exception $e) {
            // otherwise send more details and neutral message
            $exceptionDetails = [
                'customer' => [
                    'customerId' => $this->customerEntity->getCustomerId(),
                    'email' => $this->customerEntity->getEmail(),
                ],
                'basket' => $this->createExceptionBasketDetails(),
                'order' => isset($orderId) ? $orderId : 'No order created',
                'isFeefo' => isset($isFeefo) ? $isFeefo : NULL,
                'token' => $token ? $this->createExceptionTokenDetails($token) : NULL,
                'paymentResponse' => $this->createExceptionPaymentResponseDetails($paymentResponse),
            ];
            $this->logger->error($e, $exceptionDetails);
            Debug::log("Payment completion failed with message: {$e->getMessage()}. Additional information: " . var_export($paymentResponse, TRUE));
            throw PaymentException::failedOrder($e);
        }
    }

    /**
     * @return Basket
     */
    public function getBasket()
    {
        return $this->basket;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     *
     * @param Customer $customer
     * @return Customer
     */
    public function setCustomer($customer)
    {
        return $this->customer = $customer;
    }

    /**
     * @param array $onFinish
     */
    public function setOnFinish(array $onFinish)
    {
        $this->onFinish = $onFinish;
    }

    /*     * *********************************************** Front Payment ************************************************ */

    /**
     * @param FForm $form
     * @param array $query
     * @param array $post
     * @throws Exception
     * @throws PaypalException
     * @throws Exception
     * @throws SageException
     * @throws SageAuthenticationException
     */
    public function processPaymentForm(FForm $form, array $query, array $post)
    {
        $this->query = $query;
        $this->post = $post;
        if (!isset($this->post['emails'])) {
            $this->post['emails'] = !$this->customer->isNew() ? $this->customer->email : NULL;
        }

        //handle partial payment
        if ($form->isOk2Save()) {
            if (isset($form['credit']) && $form->getValue('credit')) {
                if ($this->basket->getPrice('total') > $this->customer->credit) {
                    $this->basket->setCredit($this->customer->credit);
                } else {
                    $this->basket->setCredit(0);
                    $form['paymentType']->setValue(Transaction::ON_ACCOUNT);
                }
            } else {
                $this->basket->setCredit(0);
            }
        }

        //paypal
        if ($form->isOk2Save() && $form->getValue('paymentType') == Transaction::TYPE_PAYPAL || isset($this->query['token'])) {
            try {
                $service = new PaypalService($this->basket, $form, $this->query, $this->post);
                $service->process($this);
                $form->clean();
                if ($this->onFinish) {
                    call_user_func($this->onFinish, $this);
                }
            } catch (PaypalException $e) {
                throw $e;
            } catch (Exception $e) {
                throw new PaypalException($e->getMessage());
            }
        }

        //sage
        if ($form->isOk2Save() && $form->getValue('paymentType') == Transaction::TYPE_SAGEPAY || isset($this->query['sagepay-3d'])) {
            try {
                // selectedMethod = 0 - new customer/new card
                // token is stored
                if ($form->getValue('selectedMethod') == '0' || (isset($this->query['sagepay-3d']) && isset($this->query['storeToken']) && $this->query['storeToken'] == 1)) {
                    $this->newCardPayment($form);
                } elseif ($form->getValue('selectedMethod') == 'one_off_card' || (isset($this->query['sagepay-3d']) && isset($this->query['storeToken']) && $this->query['storeToken'] == 0)) {
                    // selectedMethod = one_off_card - new customer w/token uses different card
                    // token is not stored
                    $this->oneOffCardPayment($form);
                } else {
                    $this->tokenPayment($form);
                }
            } catch (SageAuthenticationException $e) {
                $this->save3DAuthDetails($this->post);
                throw $e;
            } catch (SageException $e) {
                throw $e;
            } catch (Exception $e) {
                throw new SageException($e->getMessage());
            }
        }

        //free order
        if ($form->isOk2Save() && $form->getValue('paymentType') == Transaction::TYPE_FREE) {
            try {
                $this->processFreeOrderPayment();
                $form->clean();
                if ($this->onFinish) {
                    call_user_func($this->onFinish, $this);
                }
            } catch (Exception $e) {
                throw $e;
            }
        }

        //on account payment
        if ($form->isOk2Save() && $form->getValue('paymentType') == Transaction::ON_ACCOUNT) {
            try {
                $this->processOnAccountPayment();
                $form->clean();
                if ($this->onFinish) {
                    call_user_func($this->onFinish, $this);
                }
            } catch (Exception $e) {
                throw $e;
            }
        }
    }

    /**
     * @param FForm $form
     */
    public function newCardPayment(FForm $form)
    {
        $this->check3DAuthPost($this->post);
        $service = new SageService($this->basket, $form, $this->query, $this->post);
        //token payment or direct payment
        $service->processTokenPayments($this);

        if ($this->onFinish) {
            call_user_func($this->onFinish, $this);
        }
    }

    /**
     * @param FForm $form
     */
    public function oneOffCardPayment(FForm $form)
    {
        $this->check3DAuthPost($this->post);
        $service = new SageService($this->basket, $form, $this->query, $this->post);
        // card payment no token is stored
        $service->processCardPayments($this);

        if ($this->onFinish) {
            call_user_func($this->onFinish, $this);
        }
    }

    /**
     * @param FForm $form
     */
    public function tokenPayment(FForm $form)
    {
        $service = new SageService($this->basket, $form, $this->query, $this->post);
        //  token is reused and kept (reuse of oneclick code!)
        $service->processReusedTokenPayments($this, $this->tokenModel, $this->getCustomer(), $this->customerEntity, $this->orderEntityService, $this->productService);

        if ($this->onFinish) {
            call_user_func($this->onFinish, $this);
        }
    }

    /*     * *********************************************** Admin Phone Payment ************************************************ */

    public function processPhonePaymentForm(PayByPhoneForm $form, array $query, array $post, $processTokenPayment = FALSE)
    {
        $this->query = $query;
        $this->post = $post;

        //sage
        try {
            $service = new SageService($this->basket, $form, $this->query, $this->post);

            if ($processTokenPayment) {
                $service->processTokenPayments($this, TRUE);
            } else {
                $service->processPaymentViaPhone($this);
            }

            $form->clean();
            return $this->basket->orderId;
        } catch (SageAuthenticationException $e) {
            $this->save3DAuthDetails($this->post);
            throw $e;
        } catch (Exception $e) {
            throw new SageException($e->getMessage());
        }
    }

    /**
     * @throws Exception
     */
    public function processOnAccountPayment()
    {
        // subtract customer credit
        if ($this->customer->isNew()) {
            throw new Exception("You can't use on account payment!");
        }

        try {
            $this->customer->subtractCredit($this->basket->getPrice('total'));
        } catch (Exception $e) {
            $this->logger->error(
                $e,
                [
                    'customerId' => $this->customerEntity->getCustomerId(),
                    'customerCredit' => $this->customerEntity->getCredit(),
                    'basket' => $this->basket->getItems(),
                    'price' => $this->basket->getPrice(),
                    'paymentType' => $this->post['paymentType'],
                ]
            );
            throw $e;
        }

        $paymentResponse = $this->customPaymentResponse(TransactionEntity::ON_ACCOUNT);
        $this->completeTransaction($paymentResponse);
    }

    /**
     * @throws Exception
     */
    public function processFreeOrderPayment()
    {
        //confirm that we have items and voucher
        if (!$this->basket->isFreeBasket()) {
            throw new Exception('You can buy items for free!');
        }

        $this->processCustomerInformations(new PaymentResponse(), self::CUSTOMER_SIGN_IN, $this->post['emails']);
        $paymentResponse = $this->customPaymentResponse(TransactionEntity::TYPE_FREE);
        $this->completeTransaction($paymentResponse);
    }

    /**
     * @param PaymentResponse $paymentResponse
     * @param bool $customerSignIn
     * @param string|NULL $email
     * @return CustomerEntity
     */
    public function saveCustomerInformation(PaymentResponse $paymentResponse, $customerSignIn = self::CUSTOMER_SIGN_IN, $email = NULL)
    {
        $this->processCustomerInformations($paymentResponse, $customerSignIn, $email);
        return $this->customerEntity;
    }

    /**
     * @param PaymentResponse $paymentResponse
     * @param bool $customerSignIn
     * @param string|NULL $email
     * @return Customer
     */
    public function processCustomerInformations(PaymentResponse $paymentResponse, $customerSignIn = self::CUSTOMER_SIGN_IN, $email = NULL)
    {
        if ($this->customer->isNew()) {
            $cutsomerService = new CustomerService();
            $roleId = $this->basket->hasWholesalePackage() ? Customer::ROLE_WHOLESALE : NULL;
            $this->customer = $cutsomerService->createPaymentCustomer($paymentResponse, $email, $roleId);
            $this->customerEntity = $this->customerService->getCustomerById($this->customer->getId());
            //sign in customer
            if ($customerSignIn) {
                Customer::signIn($this->customer->getId());
            }
        } else {
            //fix for Registered customer without filling customer details form.
            $cutsomerService = new CustomerService();
            $this->customer = $cutsomerService->verifyCutomerData($paymentResponse, $this->customer);
            $this->customer->save();
        }
        return $this->customer;
    }

    /**
     * @param int $transactionType
     * @return \PaymentResponse
     */
    public function customPaymentResponse($transactionType)
    {
        $paymentResponse = new PaymentResponse();
        $paymentResponse->setTypeId($transactionType);
        $paymentResponse->setPaymentTransactionTime(date('Y m d H:i:s'));
        $paymentResponse->setPaymentHolderFirstName($this->customer->firstName);
        $paymentResponse->setPaymentHolderLastName($this->customer->lastName);
        $paymentResponse->setPaymentTransactionDetails(Transaction::$types[$transactionType]);
        return $paymentResponse;
    }

    public function check3DAuthPost()
    {
        if (isset($_SESSION['sageAuthPostDetails'])) {
            $this->post = array_merge($this->post, $_SESSION['sageAuthPostDetails']);
            unset($_SESSION['sageAuthPostDetails']);
        }
    }

    /**
     * @param array $post
     */
    protected function save3DAuthDetails($post)
    {
        isset($post['cardNumber']) ? $post['cardShortNumber'] = substr($post['cardNumber'], -4, 4) : NULL;
        isset($post['cardNumber']) ? $post['cardNumber'] = NULL : NULL;
        isset($post['CV2']) ? $post['CV2'] = NULL : NULL;
        isset($post['issueNumber']) ? $post['issueNumber'] = NULL : NULL;
        isset($post['validFrom']) ? $post['validFrom'] = NULL : NULL;
        //isset($post['expiryDate']) ? $post['expiryDate'] = NULL : NULL;
        $post['auth3D'] = 1;
        $_SESSION['sageAuthPostDetails'] = $post;
    }

    /**
     * called when payment was successfull
     */
    private function paymentSuccess()
    {
        $creditDiscount = new CreditDiscount($this->customer, $this->paymentEmailer);
        $creditDiscount->applyCreditBonus();
        if (!empty($this->basket->voucher)) {
            $voucher = new VoucherNew($this->basket->voucher->getId());
            $voucher->used += 1;
            $voucher->save();
        }
    }

    /**
     * @return array
     */
    private function createExceptionBasketDetails()
    {
        $itemDescriptions = [];
        /** @var Product $basketItem */
        foreach ($this->basket->getItems() as $basketItem) {
            $itemDescriptions[] = [
                'productId' => $basketItem->getId(),
                'name' => $basketItem->getLngTitle(),
                'price' => $basketItem->getPrice(),
                'linkedCompanyStatus' => $basketItem->linkedCompanyStatus,
                'linkedCompanyId' => $basketItem->getCompanyId(),
                'linkedCompanyNumber' => $basketItem->companyNumber,
            ];
        }

        return $itemDescriptions;
    }

    /**
     * @param PaymentResponse $paymentResponse
     * @return array
     */
    private function createExceptionPaymentResponseDetails(PaymentResponse $paymentResponse)
    {
        return [
            'cardholder' => sprintf(
                '%s (%s %s)',
                $paymentResponse->getPaymentHolderEmail(),
                $paymentResponse->getPaymentHolderFirstName(),
                $paymentResponse->getPaymentHolderLastName()
            ),
            'type' => $paymentResponse->getTypeId(),
            'details' => $paymentResponse->getPaymentTransactionDetails(),
        ];
    }

    /**
     * @param Token $token
     * @return array
     */
    private function createExceptionTokenDetails(Token $token)
    {
        return [
            'tokenId' => $token->getTokenId(),
            'identifier' => $token->getIdentifier()
        ];
    }
}
