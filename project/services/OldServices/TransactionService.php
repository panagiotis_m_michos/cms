<?php

use Entities\Payment\Token;
use PaymentModule\Contracts\IPaymentData;
use PaymentModule\Loggers\IPaymentLogger;
use UserModule\Contracts\ICustomer;

class TransactionService extends Object implements IPaymentLogger
{
    /**
     * @param PaymentResponse $paymentResponse
     * @param Customer $customer
     * @param int $orderId
     * @param Token $token
     */
    public function saveTransaction(PaymentResponse $paymentResponse, Customer $customer, $orderId, Token $token = NULL)
    {
        $transaction = new Transaction();
        $transaction->customerId = $customer->getId();
        $transaction->typeId = $paymentResponse->getTypeId();
        $transaction->orderId = $orderId;
        $transaction->orderCode = $paymentResponse->getPaymentOrderCode();
        $transaction->cardHolder = $paymentResponse->getPaymentHolderFirstName() . " " . $paymentResponse->getPaymentHolderLastName();
        $transaction->details = $paymentResponse->getPaymentTransactionDetails();
        $transaction->cardNumber = $paymentResponse->getPaymentCardNumber();
        $transaction->vpsAuthCode = $paymentResponse->getPaymentVpsAuthCode();
        $transaction->vendorTXCode = $paymentResponse->getPaymentVendorTXCode();
        $transaction->request = NULL;
        if (!empty($token)) {
            $transaction->tokenId = $token->getId();
        }
        $transaction->save();
    }
    
    /**
     * @param Customer $customer
     * @param string $error 
     */
    public static function saveFailedPaypalTransaction(Customer $customer, $error)
    {
        $transaction = new Transaction();
        $transaction->statusId = Transaction::STATUS_FAILED;
        $transaction->typeId = Transaction::TYPE_PAYPAL;
        $transaction->customerId = $customer->getId();
        $transaction->error = $error;
        $transaction->save();
    }
    
    /**
     * @param Customer $customer
     * @param string $error 
     */
    public static function saveFailedSageTransaction(Customer $customer, $error)
    {
        $transaction = new Transaction();
        $transaction->statusId = Transaction::STATUS_FAILED;
        $transaction->typeId = Transaction::TYPE_SAGEPAY;
        $transaction->customerId = $customer->getId();
        $transaction->error = $error;
        $transaction->save();
    }


    /**
     * @param Exception $e
     * @param ICustomer $customer
     * @param IPaymentData $paymentData
     * @param string $message
     */
    public function error(Exception $e, ICustomer $customer, IPaymentData $paymentData, $message = NULL)
    {
        if (!$customer->getId()) {
            return;
        }
        $transaction = new Transaction();
        $transaction->statusId = Transaction::STATUS_FAILED;
        $transaction->typeId = $paymentData->getRealPaymentType();
        $transaction->customerId = $customer->getId();
        $transaction->error = $e->getMessage();
        $transaction->save();
    }
}