<?php

class VoucherService extends Object
{
    const UPGRADE_VOUCHER_COOKIE_NAME = 'upgrade-voucher-form';
    
    public static function enableUpgradeVoucherForm()
    {
        FApplication::$httpResponse->setCookie(self::UPGRADE_VOUCHER_COOKIE_NAME, TRUE, 0);
    }

    public static function disableUpgradeVoucherForm()
    {
        FApplication::$httpResponse->deleteCookie(self::UPGRADE_VOUCHER_COOKIE_NAME);
    }
}
