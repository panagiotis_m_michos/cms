<?php

use Services\OrderItemService;

class OrderService extends Object
{
    /**
     * @var OrderItemService
     */
    private $orderItemService;

    public function __construct()
    {
        $this->orderItemService = Registry::$container->get(DiLocator::SERVICE_ORDER_ITEM);
    }

    /**
     * @param Customer $customer
     * @param Basket $basket
     * @return int
     */
    public function saveOrder(Customer $customer, Basket $basket)
    {
        // new order
        $order = new Order;
        $order->statusId = Order::STATUS_NEW;
        $order->customerId = $customer->getId();
        $order->subtotal = $basket->getPrice('subTotal2');
        $order->realSubtotal = $basket->getPrice('subTotal');
        $order->discount = $basket->getPrice('discount');
        $order->vat = $basket->getPrice('vat');
        $order->credit = $basket->getPrice('account');
        $order->total = $basket->getPrice('total');
        $order->customerName = $customer->firstName . ' ' . $customer->lastName;
        $order->customerAddress = $customer->getAddress('-');
        $order->customerEmail = $customer->email;
        $order->customerPhone = $customer->phone;
        $order->description = 'Companies Made Simple';

        // voucher
        if ($basket->voucher != NULL) {
            $order->voucherId = $basket->voucher->getId();
            $order->voucherName = $basket->voucher->name;
        }

        // save order items
        foreach ($basket->getItems() as $basketItem) {
            $orderItem = new OrderItem;
            $orderItem->productId = $basketItem->getId();
            $orderItem->productTitle = $basketItem->getLongTitle();
            $orderItem->qty = $basketItem->qty;
            $orderItem->price = $basketItem->getPrice();
            $orderItem->notApplyVat = $basketItem->notApplyVat;
            $orderItem->nonVatableValue = $basketItem->nonVatableValue ? $basketItem->nonVatableValue : '';
            $orderItem->subTotal = $basketItem->getTotalPrice();
            $orderItem->vat = $basketItem->getVat();
            $orderItem->totalPrice = $basketItem->getTotalVatPrice();
            $orderItem->additional = $basketItem->additional;
            $orderItem->markUp = $basketItem->markUp;

            // package
            if ($basketItem instanceof Package) {
                $orderItem->incorporationRequired = TRUE;
            } elseif ($basketItem->requiredCompanyNumber == TRUE) {
                if ($basketItem->companyNumber != NULL) {
                    $orderItem->companyNumber = $basketItem->companyNumber;
                    $orderItem->companyId = $basketItem->getCompanyId();
                    $orderItem->companyName = $basketItem->companyName;
                } elseif ($basketItem->getCompanyId() !== NULL) {
                    $chFilling = new CHFiling();

                    $company = $chFilling->getCustomerCompany($basketItem->getCompanyId(), $customer->getId());

                    $orderItem->companyId = $basketItem->getCompanyId();
                    $orderItem->companyName = $company->getCompanyName();

                    $number = $company->getCompanyNumber();
                    if ($number != NULL) {
                        $orderItem->companyNumber = $number;
                    } else {
                        $orderItem->incorporationRequired = TRUE;
                    }
                } else {
                    $orderItem->incorporationRequired = TRUE;
                }
            }

            $basketItem->oldOrderItem = $orderItem;
            $order->items[] = $orderItem;
        }
        if ($basket->isUsingCredit()) {
            $customer->subtractCredit($basket->getCredit());
        }
        $orderId = $order->save();

        /** @var BasketProduct $basketItem */
        foreach ($basket->items as $basketItem) {
            if ($basketItem->oldOrderItem) {
                $basketItem->setOrderItem($this->orderItemService->getOrderItemById($basketItem->oldOrderItem->getId()));
            }
        }

        return $orderId;
    }

}
