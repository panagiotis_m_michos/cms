<?php
/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    CustomerService.php 2012-10-24 <razvanp@madesimplegroup.com>
 */

class CustomerService extends Object
{
    /**
     * @param PaymentResponse $paymentResponse
     * @param string|null $email
     * @param string|null $roleId
     * @return Customer
     */
    public function createPaymentCustomer(PaymentResponse $paymentResponse, $email = NULL, $roleId = NULL)
    {
        $newCustomer = new Customer;
        $newCustomer->email = $email ? $email : $paymentResponse->getPaymentHolderEmail();
        $newCustomer->password = FTools::generPwd(8);
        $newCustomer->firstName = $paymentResponse->getPaymentHolderFirstName();
        $newCustomer->lastName = $paymentResponse->getPaymentHolderLastName();
        $newCustomer->address1 = $paymentResponse->getPaymentHolderAddressStreet();
        //$newCustomer->address2 = $newCustomerAddress->getLine2() ? $newCustomerAddress->getLine2() : NULL;
        $newCustomer->city = $paymentResponse->getPaymentHolderAddressCity();
        $newCustomer->postcode = $paymentResponse->getPaymentHolderAddressPostCode();
        $newCustomer->countryId = $this->getTranslateCountry($paymentResponse->getPaymentHolderAddressCountry());
        $newCustomer->country = $paymentResponse->getPaymentHolderAddressState();
        if ($roleId) {
            $newCustomer->roleId = $roleId;
        }
        $customerId = $newCustomer->save();
        
        //send welcome email
        $emailer = Registry::$emailerFactory->get(EmailerFactory::USER);
        $emailer->sendWelcomeNewCustomerEmail($newCustomer);
        
        return new Customer($customerId);
    }
    
    /**
     *
     * @param string $country code
     * @return boolean 
     */
    public function getTranslateCountry($country)
    {
        if (isset(WPDirect::$countries[$country])) {
            $country = array_search(WPDirect::$countries[$country], Customer::$countries);
            if($country) {
                return $country;
            } 
        }
        return false;
    }
    
    /**
     * @param PaymentResponse $paymentResponse
     * @param Customer $customer
     * @return \Customer
     */
    public function verifyCutomerData(PaymentResponse $paymentResponse, Customer $customer)
    {
        if (empty($customer->firstName)) $customer->firstName = $paymentResponse->getPaymentHolderFirstName();
        if (empty($customer->lastName)) $customer->lastName = $paymentResponse->getPaymentHolderLastName();
        if (empty($customer->address1)) $customer->address1 = $paymentResponse->getPaymentHolderAddressStreet();
        if (empty($customer->city))     $customer->city = $paymentResponse->getPaymentHolderAddressCity();
        if (empty($customer->postcode)) $customer->postcode = $paymentResponse->getPaymentHolderAddressPostCode();
        if (empty($customer->countryId)) $customer->countryId = $this->getTranslateCountry($paymentResponse->getPaymentHolderAddressCountry());

        return $customer;
    }
    
}