<?php

namespace Services\Register;

use Entities\Company as CompanyEntity;
use Entities\Register\ShareClassEvent;
use EntityNotFound;
use Exceptions\Business\Forbidden;
use InvalidArgumentException;
use Repositories\Register\ShareClassEventRepository;
use Nette\Object;
use Entities\Register\Member;
use DoctrineDataSource;

class ShareClassEventService extends Object
{
    /**
     * @var ShareClassEventRepository
     */
    private $repository;

    /**
     * @param ShareClassEventRepository $repository
     */
    public function __construct(ShareClassEventRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param $id
     * @return ShareClassEvent
     * @throws InvalidArgumentException
     */
    public function getShareClassEventById($id)
    {
        if (!$id) {
            throw new InvalidArgumentException("Missing share class event id");
        }
        return $this->repository->find($id);
    }

    /**
     * @param ShareClassEvent $entity
     */
    public function saveShareClassEvent(ShareClassEvent $entity)
    {
        $this->repository->saveEntity($entity);
    }

    /**
     * @param ShareClassEvent $entity
     */
    public function deleteShareClassEvent(ShareClassEvent $entity)
    {
        $this->repository->removeEntity($entity);
    }
}
