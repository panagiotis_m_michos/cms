<?php

namespace Services\Register;

use Entities\Company;
use Entities\Register\ShareClass;
use Entities\Register\ShareClassEvent;
use EntityNotFound;
use Exceptions\Business\Forbidden;
use InvalidArgumentException;
use Nette\Object;
use Repositories\Register\ShareClassRepository;

class ShareClassService extends Object
{
    /**
     * @var ShareClassRepository
     */
    private $repository;

    /**
     * @param ShareClassRepository $repository
     */
    public function __construct(ShareClassRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param $id
     * @return ShareClass
     * @throws InvalidArgumentException
     */
    public function getShareClassById($id)
    {
        if (!$id) {
            throw new InvalidArgumentException("Missing share class id");
        }
        return $this->repository->find($id);
    }

    /**
     * @param Company $company
     * @param $id
     * @return ShareClass
     * @throws EntityNotFound
     * @throws Forbidden
     */
    public function getCompanyShareClass(Company $company, $id)
    {
        $shareClass = $this->getShareClassById($id);
        if (!$shareClass) {
            throw new EntityNotFound("Share class `$id` doesn't exist");
        }
        if (!$shareClass->getMember()->getCompany()->isEqual($company)) {
            throw new Forbidden(sprintf("Share class entity `%s` doesn't belong to company id `%s`", $id, $company->getId()));
        }
        return $shareClass;
    }

    /**
     * @param ShareClass $entity
     */
    public function deleteShareClass(ShareClass $entity)
    {
        $this->repository->removeEntity($entity);
    }

    /**
     * @param ShareClass $entity
     */
    public function saveShareClass(ShareClass $entity)
    {
        $this->repository->saveEntity($entity);
    }
}
