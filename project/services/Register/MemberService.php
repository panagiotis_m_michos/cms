<?php

namespace Services\Register;

use Entities\Company as CompanyEntity;
use EntityNotFound;
use Exceptions\Business\Forbidden;
use InvalidArgumentException;
use Repositories\Register\MemberRepository;
use Nette\Object;
use Entities\Register\Member;
use DoctrineDataSource;

class MemberService extends Object
{
    /**
     * @var MemberRepository
     */
    private $repository;

    /**
     * @param MemberRepository $repository
     */
    public function __construct(MemberRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param $id
     * @return Member
     * @throws InvalidArgumentException
     */
    public function getMemberById($id)
    {
        if (!$id) {
            throw new InvalidArgumentException("Missing member id");
        }
        return $this->repository->find($id);
    }

    /**
     * @param Member $entity
     */
    public function saveMember(Member $entity)
    {
        $this->repository->saveEntity($entity);
    }

    /**
     * @param Member $entity
     */
    public function deleteMember(Member $entity)
    {
        $this->repository->removeEntity($entity);
    }

    /**
     * @param CompanyEntity $company
     * @return DoctrineDataSource
     */
    public function getListDatasource(CompanyEntity $company)
    {
        return new DoctrineDataSource($this->repository->getListBuilder($company));
    }

    /**
     * @param CompanyEntity $company
     * @param int $id
     * @throws Forbidden
     * @throws EntityNotFound
     * @return Member
     */
    public function getCompanyMemberById(CompanyEntity $company, $id)
    {
        $member = $this->getMemberById($id);
        if (!$member) {
            throw new EntityNotFound("Member `$id` doesn't exist");
        }
        if (!$member->getCompany()->isEqual($company)) {
            throw new Forbidden(sprintf("Member entity `%s` doesn't belong to company id `%s`", $id, $company->getId()));
        }
        return $member;
    }
    
}
