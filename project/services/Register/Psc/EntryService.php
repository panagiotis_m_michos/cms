<?php

namespace Services\Register\Psc;

use DateTime;
use Entities\Company;
use Entities\Register\Psc\CompanyStatement;
use Entities\Register\Psc\Entry;
use Entities\Register\Psc\Psc;
use EntityNotFound;
use Exceptions\Business\Forbidden;
use Nette\Object;
use Repositories\Register\Psc\EntryRepository;

class EntryService extends Object
{
    /**
     * @var EntryRepository
     */
    private $repository;

    /**
     * @param EntryRepository $repository
     */
    public function __construct(EntryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Entry $entry
     */
    public function addEntry(Entry $entry)
    {
        if ($entry->isCompanyStatement()) {
            $pscs = $this->repository->getActivePscsByCompany($entry->getCompany());
            foreach ($pscs as $psc) {
                $psc->setDateCessated(new DateTime);
            }
        } elseif ($entry->isPsc()) {
            $companyStatements = $this->repository->getActiveCompanyStatementsByCompany($entry->getCompany());
            foreach ($companyStatements as $statement) {
                $statement->setDateWithdrew(new DateTime);
            }
        }

        $this->repository->persist($entry);
        $this->repository->flush();
    }

    /**
     * @param Entry $entry
     */
    public function updateEntry(Entry $entry)
    {
        //todo: psc - ako toto robit? ked chcem mat v service/repository metodu, ktoru budem volat, ked chcem updatnut entitu? ale bez persistu
        $this->repository->flush($entry);
    }

    /**
     * @param Entry $entry
     */
    public function deleteEntry(Entry $entry)
    {
        $this->repository->removeEntity($entry);
    }

    /**
     * @param Psc $psc
     * @param DateTime $date
     */
    public function cessatePsc(Psc $psc, DateTime $date)
    {
        $psc->setDateCessated($date);
        $this->repository->flush();
    }

    /**
     * @param CompanyStatement $statement
     * @param DateTime $date
     */
    public function withdrawStatement(CompanyStatement $statement, DateTime $date)
    {
        $statement->setDateWithdrew($date);
        $this->repository->flush();
    }

    /**
     * @param Company $company
     * @return Entry[]
     */
    public function getEntriesByCompany(Company $company)
    {
        return $this->repository->getEntriesByCompany($company);
    }

    /**
     * @param Company $company
     * @param int $entryId
     * @return Entry
     * @throws EntityNotFound
     * @throws Forbidden
     */
    public function getCompanyEntryById(Company $company, $entryId)
    {
        //todo: psc - get by entryType
        $psc = $this->repository->find($entryId);
        if (!$psc) {
            throw new EntityNotFound("PSC Online Register entry `$entryId` doesn't exist");
        }
        if (!$psc->getCompany()->isEqual($company)) {
            throw new Forbidden(sprintf("PSC Online Register `%d` doesn't belong to company id `%d`", $entryId, $company->getId()));
        }

        return $psc;
    }

    /**
     * @param Company $company
     * @return bool
     */
    public function canAddCompanyStatement(Company $company)
    {
        $last = $this->repository->getLastEntryByCompany($company);

        //todo: psc - how to do (get..Date) for unknown class
        return !$last || !$last->isCompanyStatement() || $last->getDateWithdrew();
    }
}
