<?php

class StatCollector extends Datadogstatsd
{
    static protected $__server = 'localhost';
    static protected $__datadogHost;
    static protected $__eventUrl = '/api/v1/events';
    static protected $__apiKey;
    static protected $__applicationKey;

    /**
     * @var string
     */
    static private $host;

    /**
     * provide true to collect the stats
     * @var bool
     */
    static private $available;

    const PAGE_VIEWS = 'pageViews';
    const PAYMENT_SUCCESS = 'payments';
    const PAYMENT_FAILED = 'failedPayments';

    public static function setup($host, $available)
    {
        self::$host = $host;
        self::$available = $available;
    }

    public static function decrement($stat, $sampleRate = 1, array $tags = null)
    {
        if (!self::$available) {
            return;
        }
        return parent::decrement(self::$host . '.' . $stat, $sampleRate, $tags);
    }

    public static function gauge($stat, $value, $sampleRate = 1, array $tags = null)
    {
        if (!self::$available) {
            return;
        }
        parent::gauge(self::$host . '.' . $stat, $value, $sampleRate, $tags);
    }

    public static function histogram($stat, $value, $sampleRate = 1, array $tags = null)
    {
        if (!self::$available) {
            return;
        }
        parent::histogram(self::$host . '.' . $stat, $value, $sampleRate, $tags);
    }

    public static function increment($stat, $sampleRate = 1, array $tags = null)
    {
        if (!self::$available) {
            return;
        }
        return parent::increment(self::$host . '.' . $stat, $sampleRate, $tags);
    }

    public static function timing($stat, $time, $sampleRate = 1, array $tags = null)
    {
        if (!self::$available) {
            return;
        }
        parent::timing(self::$host . '.' . $stat, $time, $sampleRate, $tags);
    }

}