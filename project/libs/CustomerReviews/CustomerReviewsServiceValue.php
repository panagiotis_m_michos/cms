<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS 
 * @author 		Stan (diviak@gmail.com)
 * @internal 	project/libs/Reviews/CustomerReviewsServiceValue.php
 * @created 	16/07/2010
 */


class CustomerReviewsServiceValue extends ACustomerReviewsGrades
{
	/**
	 * Specify column in database
	 *
	 * @return string
	 */
	protected function getColumnName()
	{
		return 'rateQualityOfProducts';
	}
}