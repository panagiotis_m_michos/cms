<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS 
 * @author 		Stan (diviak@gmail.com)
 * @internal 	project/libs/Reviews/CustomerReviewsRecommendation.php
 * @created 	16/07/2010
 */


class CustomerReviewsRecommendation extends ACustomerReviewsGrades
{
	/**
	 * Specify column in database
	 *
	 * @return string
	 */
	protected function getColumnName()
	{
		return 'recomendOurServices';
	}
}