<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS 
 * @author 		Stan (diviak@gmail.com)
 * @internal 	project/libs/Reviews/CustomerReviewsCSVImport.php
 * @created 	16/07/2010
 */


class CustomerReviewsCSVImport extends Object
{
	/**
	 * Provides import customer reviews from csv
	 *
	 * @param string $filename
	 * @return void
	 */
	static public function import($filename)
	{		
		
		// check if file exists
		
		$poc = 0;
		$handle = fopen($filename, 'r');
		
		while (($row = fgetcsv($handle, 1000, ",")) !== FALSE) {
			
			// skip header and check
			$poc++;
			if ($poc <= 2 || count($row) < 19) { 
				continue;
			}
			
			try {
				$customerReview = new CustomerReview();
				$customerReview->setRespondentId(self::getValueFromRow($row, 0));
				$customerReview->setFormationProcess(self::getValueFromRow($row, 15));
				$customerReview->setCommunicationOurProducts(self::getValueFromRow($row, 16));
				$customerReview->setCustomerService(self::getValueFromRow($row, 17));
				$customerReview->setProvidingValueForMoney(self::getValueFromRow($row, 18));
				$customerReview->setRespondingPromptlyToQueries(self::getValueFromRow($row, 19));
				$customerReview->setRateQualityOfProducts(self::getValueFromRow($row, 20));
				$customerReview->setRecomendOurServices(self::getValueFromRow($row, 21));
				$customerReview->setSuggestions(self::getValueFromRow($row, 30));
				$customerReview->setAllowed(self::getValueFromRow($row, 31));
				$customerReview->setName(self::getValueFromRow($row, 32));
				$customerReview->setCompanyName(self::getValueFromRow($row, 33));
				$customerReview->setEmail(self::getValueFromRow($row, 34));
				$customerReview->setDtc(date('Y-m-d G:i', strtotime(self::getValueFromRow($row, 3))));
				$customerReview->save();
			} catch (Exception $e) {
			}
		}
	}
	
	/**
	 * Returns value from row - check if exists or is empty
	 *
	 * @param array $row
	 * @param int $index
	 * @return mixed
	 */
	static private function getValueFromRow(array $row, $index)
	{
		if (isset($row[$index]) && !empty($row[$index])) {
			// allowed
			if ($index == 17) {
				return ($row[$index] == 'Yes') ? 1 : 0;
			}
			return $row[$index];
		} 
		return NULL;
	}
}