<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS 
 * @author 		Stan (diviak@gmail.com)
 * @internal 	project/libs/Reviews/CustomerReviewsSatisfaction.php
 * @created 	16/07/2010
 */


class CustomerReviewsSatisfaction extends Object
{
	/**
	 * @param string $key
	 * @return number
	 */
	public function getFormationProcess($key)
	{
		static $data;
		if ($data === NULL) {
			$data = self::getColumnData('formationProcess');
		}
		return $data->$key;
	}
	
	/**
	 * @param string $key
	 * @return number
	 */
	public function getCommunicationOurProducts($key)
	{
		static $data;
		if ($data === NULL) {
			$data = self::getColumnData('communicationOurProducts');
		}
		return $data->$key;
	}
	
	/**
	 * @param string $key
	 * @return number
	 */
	public function getCustomerService($key)
	{
		static $data;
		if ($data === NULL) {
			$data = self::getColumnData('customerService');
		}
		return $data->$key;
	}
	
	/**
	 * @param string $key
	 * @return number
	 */
	public function getProvidingValueForMoney($key)
	{
		static $data;
		if ($data === NULL) {
			$data = self::getColumnData('providingValueForMoney');
		}
		return $data->$key;
	}
	
	/**
	 * @param string $key
	 * @return number
	 */
	public function getRespondingPromptlyToQueries($key)
	{
		static $data;
		if ($data === NULL) {
			$data = self::getColumnData('respondingPromptlyToQueries');
		}
		return $data->$key;
	}
	
	/**
	 * Returns avg and count for Our online company formation process
	 *
	 * @return array
	 */
	static private function getColumnData($key)
	{
		$data = dibi::select('ROUND(AVG(%n), 2)', $key)->as('`avg`')
			->select('COUNT(formationProcess)')->as('`count`')
			->from(CustomerReview::TABLE_NAME)
			->where('%n!=0', $key)
			->execute()->fetch();
		return $data;
	}
	
	
	/**
	 * Returns final value in percentage
	 *
	 * @return int
	 */
	public function getFinalPercentageValue()
	{
		static $avg;
		if ($avg === NULL) {
			$sum = $this->getFormationProcess('avg');
			$sum += $this->getCommunicationOurProducts('avg');
			$sum += $this->getCustomerService('avg');
			$sum += $this->getProvidingValueForMoney('avg');
			$sum += $this->getRespondingPromptlyToQueries('avg');
			
			$avg = $sum / 5 / 5 * 100;
			$avg = round($avg);
		}
		return $avg;
	}
	
	
	/**
	 * Returns google chart
	 *
	 * @return Html
	 */
	public function getBarChart()
	{
		$lineChart = new GBarChart(300,225,'g');
		
        //set max
        $lineChart->setDataRange(0,5);
        $lineChart->setAutoBarWidth();
        
        //set rows
        $lineChart->addDataSet(array($this->getFormationProcess('avg')));
        $lineChart->addDataSet(array($this->getCommunicationOurProducts('avg')));
        $lineChart->addDataSet(array($this->getCustomerService('avg')));
        $lineChart->addDataSet(array($this->getProvidingValueForMoney('avg')));
        $lineChart->addDataSet(array($this->getRespondingPromptlyToQueries('avg')));
        
        //set legend and colors
        $lineChart->setColors(array("A0C55F","7AB317","0D6759","0B2E59","2A044A"));
        
        //set x and y
        $lineChart->setVisibleAxes(array('x','y'));
        $lineChart->addAxisRange(0, 1, 7, 1);
        $lineChart->addAxisRange(1, 0, 5);
        $lineChart->addAxisLabel(0, array("", "A", "B", "C", "D", "E"," "));
        
        //set grid
        $lineChart->setGridLines(0,20);
        
        $img = Html::el('img')->src($lineChart->getUrl())->alt('Customer Satisfaction')->width(300)->height(225);
		return $img;
	}
}