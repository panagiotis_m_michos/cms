<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS 
 * @author 		Stan (diviak@gmail.com)
 * @internal 	project/libs/Reviews/CustomerReview.php
 * @created 	16/07/2010
 */


class CustomerReview extends Object
{
	// statuses
	const STATUS_UNAPPROVED = 'unapproved';
	const STATUS_APPROVED = 'approved';
	const STATUS_UNALLOWED = 'unallowed';
	
	/** @var array */
	static public $statuses = Array(
		self::STATUS_UNAPPROVED => 'Unapproved',
		self::STATUS_APPROVED => 'Approved',
		self::STATUS_UNALLOWED => 'Unallowed',
	);
	
	/* table name */
	const TABLE_NAME = 'cms2_customer_reviews';
	
	/**
	 * @var int
	 */
	private $customerReviewId;
	
	/**
	 * @var int
	 */
	private $respondentId;
	
	/**
	 * @var string
	 */
	private $statusId = self::STATUS_UNAPPROVED;
	
	/**
	 * @var int
	 */
	private $formationProcess;
	
	/**
	 * @var int
	 */
	private $communicationOurProducts;
	
	/**
	 * @var int
	 */
	private $customerService;
	
	/**
	 * @var int
	 */
	private $providingValueForMoney;
	
	/**
	 * @var int
	 */
	private $respondingPromptlyToQueries;
	
	/**
	 * @var int
	 */
	private $rateQualityOfProducts;
	
	/**
	 * @var int
	 */
	private $recomendOurServices;
	
	/**
	 * @var string
	 */
	private $suggestions;
	
	/**
	 * @var string
	 */
	private $comment;
	
	/**
	 * @var boolean
	 */
	private $allowed;
	
	/**
	 * @var string
	 */
	private $name;
	
	/**
	 * @var string
	 */
	private $companyName;
	
	/**
	 * @var string
	 */
	private $email;
	
	/**
	 * @var string
	 */
	private $dtc;
	
	/**
	 * @var string
	 */
	private $dtm;
	
	/**
	 * @param int $customerReviewId
	 * @throws exception
	 */
	public function __construct($customerReviewId = 0)
	{
		$this->customerReviewId = (int) $customerReviewId; 
		if ($this->customerReviewId) {
			$w = dibi::select('*')->from(self::TABLE_NAME)->where('customerReviewId=%i', $this->customerReviewId)->execute()->fetch();
			if ($w) {
				$this->customerReviewId = $w['customerReviewId'];
				$this->respondentId = $w['respondentId'];
				$this->statusId = $w['statusId'];
				$this->formationProcess = $w['formationProcess'];
				$this->communicationOurProducts = $w['communicationOurProducts'];
				$this->customerService = $w['customerService'];
				$this->providingValueForMoney = $w['providingValueForMoney'];
				$this->respondingPromptlyToQueries = $w['respondingPromptlyToQueries'];
				$this->rateQualityOfProducts = $w['rateQualityOfProducts'];
				$this->recomendOurServices = $w['recomendOurServices'];
				$this->suggestions = $w['suggestions'];
				$this->comment = $w['comment'];
				$this->allowed = $w['allowed'];
				$this->name = $w['name'];
				$this->companyName = $w['companyName'];
				$this->email = $w['email'];
				$this->dtc = $w['dtc'];
				$this->dtm = $w['dtm'];
			} else {
			    throw new Exception("CustomerReview with id `$customerReviewId` doesn't exist.");
			}
		} 
	}
	
	/**
	 * @param int $customerReviewId
	 * @return CustomerReview
	 * @throws Exception
	 */
	static public function getCustomerReview($customerReviewId = 0)
	{
		return new self($customerReviewId);
	}
	
	/**
	 * @return int
	 */
	public function getId() 
	{ 
		return $this->customerReviewId;
	}
	
	
	/**
	 * @return int
	 */
	public function getStatusId()
	{
		return $this->statusId;
	}
	
	
	/**
	 * @return int
	 */
	public function getFormationProcess()
	{
		return $this->formationProcess;
	}
	
	/**
	 * @return int
	 */
	public function getCommunicationOurProducts()
	{
		return $this->communicationOurProducts;
	}
	
	/**
	 * @return int
	 */
	public function getCustomerService()
	{
		return $this->customerService;
	}
	

	/**
	 * @return int
	 */
	public function getProvidingValueForMoney()
	{
		return $this->providingValueForMoney;
	}
	
	
	/**
	 * @return int
	 */
	public function getRespondingPromptlyToQueries()
	{
		return $this->respondingPromptlyToQueries;
	}
	
	/**
	 * @return int
	 */
	public function getRateQualityOfProducts()
	{
		return $this->rateQualityOfProducts;
	}
	
	
	/**
	 * @return int
	 */
	public function getRecomendOurServices()
	{
		return $this->recomendOurServices;
	}
	
	
	/**
	 * @return int
	 */
	public function getSuggestions()
	{
		return $this->suggestions;
	}
	
	
	/**
	 * @return int
	 */
	public function getComment()
	{
		return $this->comment;
	}
	
	
	/**
	 * @return int
	 */
	public function getAllowed()
	{
		return $this->allowed;
	}
	
	/**
	 * @return int
	 */
	public function getName()
	{
		return $this->name;
	}
	
	
	/**
	 * @return int
	 */
	public function getCompanyName()
	{
		return $this->companyName;
	}
	
	/**
	 * @return int
	 */
	public function getDtc()
	{
		return $this->dtc;
	}
	
	/**
	 * @return int
	 */
	public function getDtm()
	{
		return $this->dtm;
	}
	
	
	
	
	/**
	 * @param int $respondentId
	 */
	public function setRespondentId($respondentId)
	{
		$this->respondentId = $respondentId;
	}
	
	/**
	 * @param int $statusId
	 */
	public function setStatusId($statusId)
	{
		$this->statusId = (string) $statusId;
	}
	
	/**
	 * @param int $formationProcess
	 */
	public function setFormationProcess($formationProcess)
	{
		if ($formationProcess > 5) $formationProcess = 5;
		$this->formationProcess = (int) $formationProcess;
	}
	
	/**
	 * @param int $communicationOurProducts
	 */
	public function setCommunicationOurProducts($communicationOurProducts)
	{
		if ($communicationOurProducts > 5) $communicationOurProducts = 5;
		$this->communicationOurProducts = (int) $communicationOurProducts;
	}
	
	/**
	 * @param int $customerService
	 */
	public function setCustomerService($customerService)
	{
		if ($customerService > 5) $customerService = 5;
		$this->customerService = (int) $customerService;
	}
	
	/**
	 * @param int $providingValueForMoney
	 */
	public function setProvidingValueForMoney($providingValueForMoney)
	{
		if ($providingValueForMoney > 5) $providingValueForMoney = 5;
		$this->providingValueForMoney = (int) $providingValueForMoney;
	}
	
	/**
	 * @param int $respondingPromptlyToQueries
	 */
	public function setRespondingPromptlyToQueries($respondingPromptlyToQueries)
	{
		if ($respondingPromptlyToQueries > 5) $respondingPromptlyToQueries = 5;
		$this->respondingPromptlyToQueries = (int) $respondingPromptlyToQueries;
	}
	
	/**
	 * @param int $rateQualityOfProducts
	 */
	public function setRateQualityOfProducts($rateQualityOfProducts)
	{
		if ($rateQualityOfProducts > 5) $rateQualityOfProducts = 5;
		$this->rateQualityOfProducts = (int) $rateQualityOfProducts;
	}
	
	
	/**
	 * @param int $recomendOurServices
	 */
	public function setRecomendOurServices($recomendOurServices)
	{
		if ($recomendOurServices > 5) $recomendOurServices = 5;
		$this->recomendOurServices = (int) $recomendOurServices;
	}
	
	
	/**
	 * @param string $suggestions
	 */
	public function setSuggestions($suggestions)
	{
		if ($suggestions !== NULL) $this->suggestions = (string) $suggestions;
	}
	
	/**
	 * @param string $comment
	 */
	public function setComment($comment)
	{
		if ($comment !== NULL) $this->comment = (string) $comment;
	}
	
	/**
	 * @param boolean $allowed
	 */
	public function setAllowed($allowed)
	{
		$this->allowed = (bool) $allowed;
	}
	
	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		if ($name !== NULL) $this->name = (string) $name;
	}
	
	/**
	 * @param string $companyName
	 */
	public function setCompanyName($companyName)
	{
		if ($companyName !== NULL) $this->companyName = (string) $companyName;
	}
	
	public function getEmail() {
		return $this->email;
	}

	public function setEmail($email) {
		$this->email = $email;
	}

		
	/**
	 * @param string $dtc
	 */
	public function setDtc($dtc)
	{
		$this->dtc = (string) $dtc;
	}
	
	/**
	 * @return int
	 */
	public function save()
	{
		// data
		$data['customerReviewId'] = $this->customerReviewId;
		$data['respondentId'] = $this->respondentId;
		$data['statusId'] = $this->statusId;
		$data['formationProcess'] = $this->formationProcess;
		$data['communicationOurProducts'] = $this->communicationOurProducts;
		$data['customerService'] = $this->customerService;
		$data['providingValueForMoney'] = $this->providingValueForMoney;
		$data['respondingPromptlyToQueries'] = $this->respondingPromptlyToQueries;
		$data['rateQualityOfProducts'] = $this->rateQualityOfProducts;
		$data['recomendOurServices'] = $this->recomendOurServices;
		$data['suggestions'] = $this->suggestions;
		$data['comment'] = $this->comment;
		$data['allowed'] = $this->allowed;
		$data['name'] = $this->name;
		$data['companyName'] = $this->companyName;
		$data['email'] = $this->email;
		$data['dtc'] = $this->dtc;
		$data['dtm'] = new DibiDateTime();
		
		// insert
		if ($this->isNew()){
			if (!$this->allowed) $data['statusId'] = self::STATUS_UNALLOWED; 
			$this->customerReviewId = dibi::insert(self::TABLE_NAME, $data)->execute(dibi::IDENTIFIER);
		// update
		} else {
			dibi::update(self::TABLE_NAME, $data)->where('customerReviewId=%i', $this->customerReviewId)->execute();
		}
		return $this->getId();
	}
	
	/**
	 * @return void
	 */
	public function delete()
	{
		dibi::delete(self::TABLE_NAME)->where('customerReviewId=%i', $this->getId())->execute();
	}
	
	/**
	 * @return bool
	 */
	public function isNew() 
	{ 
		return $this->customerReviewId == 0; 
	}
	
	/**
	 * Returns customer reviews
	 *
	 * @param array $where
	 * @param int $limit
	 * @param int $offset
	 * @return array<DibiRow>
	 */
	static public function getCustomerReviews(array $where = array(), $limit = NULL, $offset = NULL)
	{
		$result = dibi::select('*')->from(self::TABLE_NAME)->orderBy('dtc')->desc();
		
		$result->where('suggestions IS NOT NULL');
		foreach ($where as $key => $val) $result->where('%n', $key, ' = %s', $val);
		
		if ($limit !== NULL) $result->limit($limit);
		if ($offset !== NULL) $result->offset($offset);
		
		$reviews = $result->execute()->fetchAssoc('customerReviewId');
		return $reviews;
	}
	
	/**
	 * Returns customer reviews as objects
	 *
	 * @param array $where
	 * @param int $limit
	 * @param int $offset
	 * @return array<CustomerReview>
	 */
	static public function getCustomerReviewsObjects(array $where = array(), $limit = NULL, $offset = NULL)
	{
		$return = array();
		$all = self::getCustomerReviews($where, $limit, $offset);
		foreach ($all as $key => $val) {
			$return[$key] = new self($key);
		}
		return $return;
	}
	
	/**
	 * Returns count of reviews
	 *
	 * @param array $where
	 * @return int
	 */
	static public function getCustomerReviewsCount(array $where = array())
	{
		$result = dibi::select('COUNT(*)')->from(self::TABLE_NAME)->orderBy('dtc')->desc();
		$result->where('suggestions IS NOT NULL'); 
		foreach ($where as $key => $val) $result->where('%n', $key, ' = %s', $val);
		$count = $result->execute()->fetchSingle();
		return $count;
	}
	
	/**
	 * Returns allowed reviews
	 *
	 * @param array $where
	 * @param int $limit
	 * @param int $offset
	 * @return array<DibiRow>
	 */
	static public function getAllowedAndApprovedCustomerReviews(array $where = array(), $limit = NULL, $offset = NULL)
	{
		$where['allowed'] = 1;
		$where['statusId'] = self::STATUS_APPROVED;
		$testimonials = self::getCustomerReviews($where, $limit, $offset);
		return $testimonials;
	}
	
	/**	
	 * Return count of companies whic are allowed and approved
	 *
	 * @param array $where
	 * @return int
	 */
	static public function getCustomerAllowedAndApproveReviewsCount(array $where = array())
	{
		$where['allowed'] = 1;
		$where['statusId'] = self::STATUS_APPROVED;
		$count = self::getCustomerReviewsCount($where);
		return $count;
	}
	
	/**
	 * Returns if review is approved
	 *
	 * @return boolean
	 */
	public function isApproved()
	{
		return (bool) ($this->getStatusId() == self::STATUS_APPROVED);
	}
	
	/**
	 * Returns if review is unapproved
	 *
	 * @return boolean
	 */
	public function isUnapproved()
	{
		return (bool) ($this->getStatusId() == self::STATUS_UNAPPROVED);
	}
	
	/**
	 * Returns if review is unallowed
	 *
	 * @return boolean
	 */
	public function isUnallowed()
	{
		return (bool) ($this->getStatusId() == self::STATUS_UNALLOWED);
	}
	
	/**
	 * Provides approving customer review
	 *
	 * @return void
	 */
	public function approve()
	{
		$this->setStatusId(self::STATUS_APPROVED);
		$this->save();
	}
	
	/**
	 * Provides unapproving customer review
	 *
	 * @return void
	 */
	public function unapprove()
	{
		$this->setStatusId(self::STATUS_UNAPPROVED);
		$this->save();
	}
	
	/**
	 * Provides unallowing customer review
	 *
	 * @return void
	 */
	public function unallow()
	{
		$this->setStatusId(self::STATUS_UNALLOWED);
		$this->save();
	}
}