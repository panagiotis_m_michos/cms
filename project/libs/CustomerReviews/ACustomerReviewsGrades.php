<?php

/**
 * @package 	Companies Made Simple
 * @subpackage 	CMS 
 * @author 		Stan (diviak@gmail.com)
 * @internal 	project/libs/Reviews/ACustomerReviewsGrades.php
 * @created 	16/07/2010
 */


abstract class ACustomerReviewsGrades extends Object
{
	/**
	 * Specify column in database
	 *
	 * @return string
	 */
	abstract protected function getColumnName();
	
	/**
	 * @param string $key
	 * @return mixed
	 */
	public function getGradeOne($key)
	{
		static $data;
		if ($data === NULL) {
			$data = $this->getGradeData(1);
		}
		return $data->$key;
	}
	
	/**
	 * @param string $key
	 * @return mixed
	 */
	public function getGradeTwo($key)
	{
		static $data;
		if ($data === NULL) {
			$data = $this->getGradeData(2);
		}
		return $data->$key;
	}
	
	
	/**
	 * @param string $key
	 * @return mixed
	 */
	public function getGradeThree($key)
	{
		static $data;
		if ($data === NULL) {
			$data = $this->getGradeData(3);
		}
		return $data->$key;
	}
	
	/**
	 * @param string $key
	 * @return mixed
	 */
	public function getGradeFour($key)
	{
		static $data;
		if ($data === NULL) {
			$data = $this->getGradeData(4);
		}
		return $data->$key;
	}
	
	/**
	 * @param string $key
	 * @return mixed
	 */
	public function getGradeFive($key)
	{
		static $data;
		if ($data === NULL) {
			$data = $this->getGradeData(5);
		}
		return $data->$key;
	}
	
	
	/**
	 * Returns array for specific mark
	 *
	 * @param int $mark
	 * @return array
	 */
	private function getGradeData($mark)
	{
		static $all;
		if ($all === NULL) {
			$all = dibi::select('COUNT(%n)', $this->getColumnName())->as('count')
				->from('cms2_customer_reviews')
				->where('%n!=0', $this->getColumnName())
				->execute()
				->fetchSingle();
		}
		
		$count = dibi::select('COUNT(%n)', $this->getColumnName())->as('count')
			->from('cms2_customer_reviews')
			->where('%n=%i', $this->getColumnName(), $mark)
			->execute()
			->fetchSingle();
			
		$return = array();
		$return['percent'] = round(($count / $all) * 100, 1);
		$return['count'] = $count;
		$return['width'] = 170 * $return['percent'] / 100;
			
		return (object) $return;
	}
	
	
	/**
	 * Returns final percentage value
	 *
	 * @return int
	 */
	public function getFinalPercentageValue()
	{
		static $finalValue;
		if ($finalValue === NULL) {
			$all = dibi::select('COUNT(%n)', $this->getColumnName())->as('count')
					->from('cms2_customer_reviews')
					->where('%n!=0', $this->getColumnName())
					->execute()
					->fetchSingle();
					
			$positive = dibi::select('COUNT(%n)', $this->getColumnName())->as('count')
					->from('cms2_customer_reviews')
					->where('%n=5', $this->getColumnName())->or('%n=4', $this->getColumnName())
					->execute()
					->fetchSingle();
           
                $finalValue = ( $positive / $all ) * 100;
                $finalValue = round($finalValue);
  
		}
		return $finalValue;
	}
	
	
	/**
	 * Returns google chart
	 *
	 * @return Html
	 */
	public function getPieChart()
	{
		$piChart = new GPieChart(300, 225);
        $piChart->addDataSet(array($this->getGradeFive('count'), $this->getGradeFour('count'), $this->getGradeThree('count'), $this->getGradeTwo('count'), $this->getGradeOne('count')));
        $piChart->setLegend(array(5, 4, 3, 2, 1));
        $piChart->setColors(array("A0C55F","7AB317","0D6759","0B2E59","2A044A"));
		return Html::el('img')->src($piChart->getUrl())->width(300)->height(225);
	}
	
	
}