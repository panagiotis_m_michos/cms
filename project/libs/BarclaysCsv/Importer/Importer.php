<?php

namespace BarcleysCsv\Importer;

use BarclaysCsv\ImportException;
use BarclaysCsv\Parser\Item;
use Doctrine\ORM\EntityManager;
use Entities\Cashback;
use Exception;
use ILogger;
use Nette\Utils\Strings;
use Object;
use Psr\Log\LoggerInterface;
use Services\CashbackService;
use Services\CompanyService;

class Importer extends Object
{

    /**
     * @var Item[]
     */
    private $items;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var CashbackService
     */
    private $cashbackService;

    /**
     * @var array
     */
    private $errors = array();

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param Item[] $items
     * @param EntityManager $em
     * @param CompanyService $companyService
     * @param CashbackService $cashbackService
     * @param LoggerInterface $logger
     */
    public function __construct(array $items, EntityManager $em, CompanyService $companyService, CashbackService $cashbackService, LoggerInterface $logger)
    {
        $this->items = $items;
        $this->em = $em;
        $this->companyService = $companyService;
        $this->cashbackService = $cashbackService;
        $this->logger = $logger;
    }

    public function import()
    {
        $batchCount = 0;
        $batchSize = 50;

        foreach ($this->items as $item) {
            if ($item->getCompanyNumber() && $item->getOpenDate()) {
                $itemCompanyNumber = Strings::padLeft($item->getCompanyNumber(), 8, '0');
                try {
                    if (!$item->isImportable()) {
                        $this->logger->logMessage(ILogger::INFO, "Cashback for company number {$itemCompanyNumber} is older then 3 months");
                        continue;
                    }

                    $company = $this->companyService->getCompanyByCompanyNumber($itemCompanyNumber);
                    if ($company) {
                        $cashback = $this->cashbackService->getBankCashBackByCompany($company, Cashback::BANK_TYPE_BARCLAYS);
                        if (!$cashback) {
                            $cashback = new Cashback(
                                $company,
                                Cashback::BANK_TYPE_BARCLAYS,
                                50,
                                $item->getLeadDate(),
                                $item->getOpenDate() // @TODO: move to config?
                            );
                            $this->em->persist($cashback);
                            $this->logger->logMessage(ILogger::INFO, "Cashback for company number {$itemCompanyNumber} imported");
                        } else {
                            $this->logger->logMessage(ILogger::INFO, "Company number {$itemCompanyNumber} has cashback already");
                        }
                    } else {
                        throw new ImportException("Company number {$itemCompanyNumber} not found");
                    }
                } catch (Exception $e) {
                    $this->errors[] = $e->getMessage();
                    $this->logger->logMessage(ILogger::WARNING, $e->getMessage());
                }
            }

            $batchCount++;
            if ($batchCount % $batchSize === 0) {
                $batchCount = 0;
                $this->flushUpdates();
            }
        }

        if ($batchCount > 0) {
            $this->flushUpdates();
        }
    }

    private function flushUpdates()
    {
        $this->em->flush();
        $this->em->clear();
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }
}
