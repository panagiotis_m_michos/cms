<?php

namespace BarclaysCsv\Parser;

use Object;

class Parser extends Object
{

    /**
     * @var string
     */
    private $csvFilePath;

    /**
     * @param string $csvFilePath
     */
    public function __construct($csvFilePath)
    {
        $this->csvFilePath = $csvFilePath;
    }

    /**
     * @return Item[]
     */
    public function getItems()
    {
        $items = array();
        foreach ($this->getRows() as $row) {
            $items[] = new Item($row);
        }
        return $items;
    }

    /**
     * @return array
     */
    public function getRows()
    {
        $rows = array_map('str_getcsv', file($this->csvFilePath, FILE_SKIP_EMPTY_LINES));
        array_shift($rows); // remove header on first line

        return $rows;
    }
}
