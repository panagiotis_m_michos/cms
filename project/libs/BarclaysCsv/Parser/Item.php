<?php

namespace BarclaysCsv\Parser;

use BarclaysCsv\ColumnNotFoundException;
use DateTime;
use Nette\Object;
use Utils\Date;

class Item extends Object
{
    /**
     * @var Date
     */
    private $leadDate;

    /**
     * @var string
     */
    private $companyNumber;
    
    /**
     * @var Date
     */
    private $openDate;
    
    /**
     * @var array
     */
    private static $columns = array(
        0 => array(
            'name' => 'leadDate',
            'type' => 'date',
        ),
        1 => array(
            'name' => 'companyNumber',
            'type' => 'string',
        ),
        2 => array(
            'name' => 'openDate',
            'type' => 'date',
        ),
    );

    /**
     * @param array $row
     * @throws ColumnNotFoundException
     */
    public function __construct(array $row)
    {
        foreach (self::$columns as $index => $column) {
            $name = $column['name'];
            $method = 'set' . ucfirst($name);
            if (method_exists($this, $method)) {
                $type = $column['type'];
                $value = isset($row[$index]) ? $row[$index] : NULL;
                $this->$method($this->processValue($value, $type));
            } else {
                throw new ColumnNotFoundException("Setter for column '{$name}' does not exist");
            }
        }
    }
    
    /**
     * @return Date
     */
    public function getLeadDate()
    {
        return $this->leadDate;
    }

    /**
     * @param Date $leadDate
     */
    public function setLeadDate(Date $leadDate)
    {
        $this->leadDate = $leadDate;
    }

    /**
     * @return string
     */
    public function getCompanyNumber()
    {
        return $this->companyNumber;
    }

    /**
     * @param string $companyNumber
     */
    public function setCompanyNumber($companyNumber)
    {
        $this->companyNumber = $companyNumber;
    }

    /**
     * @return Date
     */
    public function getOpenDate()
    {
        return $this->openDate;
    }

    /**
     * @param Date $openDate
     */
    public function setOpenDate(Date $openDate)
    {
        $this->openDate = $openDate;
    }

    /**
     * We restrict the import to the last 3 month (based on the Open Date) so we don't re-import
     * the very old records that were once Ineligible
     *
     * @return bool
     */
    public function isImportable()
    {
        $now = new DateTime('00:00:00');
        $diff = $this->openDate->diff($now);
        $monthsDiff = ($diff->y * 12) + $diff->m;
        return ($monthsDiff < 3);
    }

    /**
     * @param string $value
     * @param string $type
     * @return mixed
     */
    private function processValue($value, $type)
    {
        switch ($type) {
            case 'date':
                return new Date(Date::changeFormat($value, 'd/m/Y', 'Y-m-d'));
        }

        return $value;
    }
}
