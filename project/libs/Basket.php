<?php

use BasketModule\Contracts\IBasket;
use CompanyModule\Domain\Company\CompanyName;
use Exceptions\Business\BasketException;
use Nette\Utils\Strings;

class Basket extends Object implements IBasket, JsonSerializable
{
    const VAT = 0.20;

    const PRODUCT_ITEM = 'Product';
    const PACKAGE_ITEM = 'Package';

    /**
     * @var string
     */
    private static $namespace = 'cms_basket';

    /**
     * @var BasketProduct[]
     */
    protected $items = [];

    /**
     * @var VoucherNew
     */
    protected $voucher = FALSE;

    /**
     * @var int
     */
    protected $orderId;

    /**
     * @var float
     */
    protected $credit;

    /**
     * @var CompanyName
     */
    protected $companyName;

    /**
     * @param string $namespace
     */
    public function __construct($namespace = NULL)
    {
        if ($namespace) {
            self::$namespace = $namespace;
        }
        $session = FApplication::$session->getNameSpace(self::$namespace);
        if (!isset($session->items)) {
            $session->items = [];
        }
        if (!isset($session->voucher)) {
            $session->voucher = FALSE;
        }
        if (!isset($session->orderId)) {
            $session->orderId = FALSE;
        }
        if (!isset($session->credit)) {
            $session->credit = 0;
        }

        $this->items =& $session->items;
        $this->voucher =& $session->voucher;
        $this->orderId =& $session->orderId;
        $this->credit =& $session->credit;
        $this->companyName =& $session->companyName;
    }


    /**
     * @return BasketProduct[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param $itemId
     * @return BasketProduct|NULL
     */
    public function getItem($itemId)
    {
        return !empty($this->items[$itemId]) ? $this->items[$itemId] : NULL;
    }

    /**
     * @return VoucherNew|bool
     */
    public function getVoucher()
    {
        return $this->voucher;
    }

    /**
     * @param $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return string
     */
    public function getOrderId()
    {
        return $this->orderId;
    }


    /**
     * @param BasketProduct $product
     * @return bool
     * @throws Exception
     */
    public function add(BasketProduct $product)
    {
        if ($product->canBeAdded($this)) {
            $itemKeyInBasket = $product->inBasket($this);
            // update
            if ($itemKeyInBasket !== FALSE && $product->canBeUpdated($this) == TRUE) {
                $this->items[$itemKeyInBasket]->qty += $product->qty;
                return TRUE;
            } else {
                $this->items[] = $product;
                return TRUE;
            }
        } else {
            throw BasketException::itemCouldNotBeAdded($product);
        }
    }

    /**
     * @param BasketProduct[] $products
     */
    public function addProducts($products)
    {
        foreach ($products as $product) {
            $this->add($product);
        }
    }

    /**
     * @param Package $package
     */
    public function addPackage(Package $package)
    {
        $this->add($package);
        $this->addProducts($package->getPackageProducts('associatedProducts1'));
        // @TODO call setIncorporationCompanyName when searching
        if (!$this->companyName && SearchControler::getCompanyName()) {
            $this->companyName = CompanyName::uppercased(SearchControler::getCompanyName());
        }
        if ($this->companyName) {
            $package->companyName = (string) $this->companyName;
        }
    }

    /**
     * @param $item
     * @param $key
     * @return bool
     */
    public function replace($item, $key)
    {
        if (isset($this->items[$key])) {
            $this->items[$key] = $item;
            return TRUE;
        }
        return FALSE;
    }


    /**
     * @param int $key
     * @param int $qty
     */
    public function update($key, $qty)
    {
        if (isset($this->items[$key]) && $this->items[$key]->canBeUpdated($this)) {
            $this->items[$key]->qty = max(1, (int) $qty);
        }
    }


    /**
     * @param int $key
     * @return int
     */
    public function remove($key)
    {
        if (isset($this->items[$key])) {
            // checking if was removed package then remove entire basket
            if ($this->items[$key]->getClass() == 'Package' || $this->items[$key] instanceof Package) {
                $this->clear(FALSE);
            } else {
                $removedId = $this->items[$key]->getId();
                unset($this->items[$key]);
                return $removedId;
            }
        }
    }

    /**
     * @param int $voucherId
     */
    public function setVoucher($voucherId)
    {
        $this->voucher = new VoucherNew($voucherId);
    }

    /**
     * Provides removing voucher from basket
     * @return void
     */
    public function removeVoucher()
    {
        $this->voucher = NULL;
    }

    /**
     * Using for basket form - inputs
     * @return array $qty
     */
    public function getFormQty()
    {
        $qty = [];
        foreach ($this->items as $key => $val) {
            if (!$val->onlyOne) {
                $qty[$key] = $val->qty;
            }
        }
        return $qty;
    }


    /**
     * @return int $count
     */
    public function getItemsCount()
    {
        $count = 0;
        foreach ($this->items as $key => $val) {
            $count += $val->qty;
        }
        return $count;
    }


    /**
     * @return boolean
     */
    public function isEmpty()
    {
        return empty($this->items) ? TRUE : FALSE;
    }


    /**
     * @return boolean
     */
    public function hasOrderId()
    {
        return $this->orderId == NULL ? FALSE : TRUE;
    }


    /**
     * @param bool $hardClean
     */
    public function clear($hardClean = TRUE)
    {
        $session = FApplication::$session->getNameSpace(self::$namespace);

        if ($hardClean === TRUE) {
            $session->remove();
        } else {
            $session->items = [];
            $session->voucher = FALSE;
            $session->orderId = FALSE;
        }
    }


    /**
     * @param string $part
     * @param boolean $round
     * @return mixed
     */
    public function getPrice($part = NULL, $round = TRUE)
    {
        $price = ['subTotal' => 0, 'nonVatable' => 0, 'subTotal2' => 0, 'discount' => 0, 'vat' => 0, 'totalVat' => 0, 'total' => 0, 'account' => 0, 'totalWithoutCredit' => 0];

        // if there are any items
        if (!empty($this->items)) {

            // count sub total price
            foreach ($this->items as $key => $item) {
                $price['subTotal'] += $item->totalPrice;
                $price['vat'] += $item->getVat();
                // no VAT
                if ($item->notApplyVat == TRUE) {
                    $price['nonVatable'] += $item->totalPrice;
                    // non vatable value
                } elseif ($item->nonVatableValue) {
                    $price['nonVatable'] += $item->nonVatableValue;
                }
            }



            // voucher
            if ($this->voucher != NULL) {
                $price['discount'] = $this->voucher->discount($price['subTotal']);
                $price['subTotal2'] = $price['subTotal'] - $price['discount'];
            } else {
                $price['subTotal2'] = $price['subTotal'];
            }
            //add vat and count total price
            //we are left with greater non vatable then the price itself
            if ($price['nonVatable'] > $price['subTotal2']) {
                $price['vat'] = 0;
            } else {
                $price['vat'] = ($price['subTotal2'] - $price['nonVatable']) * self::VAT;
            }

            $price['total'] = $price['subTotal2'] + $price['vat'];
            $price['totalWithoutCredit'] = $price['total'];

            if ($this->isUsingCredit()) {
                if ($price['total'] >= $this->getCredit()) {
                    $price['account'] = $this->getCredit();
                    $price['total'] -= $this->getCredit();
                }
            }

            // round
            if ($round === TRUE) {
                foreach ($price as &$val) {
                    $val = self::round($val);
                }
                unset($val);
            }
        }

        // return part, or whole price
        if ($part && isset($price[$part])) {
            return $price[$part];
        } else {
            return (object) $price;
        }
    }


    /**
     * @param string $price
     * @return double
     */
    public static function round($price)
    {
        $round = round($price, 2);
        return number_format($round, 2, '.', '');
    }

    /**
     * @param $type
     * @param $itemId
     * @param string $companyName
     * @return mixed
     * @throws Exception
     */
    public function inBasket($type, $itemId, $companyName = NULL)
    {
        // what kind of item is it
        switch ($type) {
            case self::PRODUCT_ITEM:
                $item = new ProductItem($single);
                return $item->inBasket($this);
                break;
            case self::PACKAGE_ITEM:
                $item = new PackageItem($itemId, $companyName);
                return $item->inBasket($this);
                break;
            default:
                throw new Exception("Type couldn't be found!");
                break;
        }
    }

    /**
     * @return bool
     */
    public function hasItemsRequiringCompanyNumber()
    {
        foreach ($this->getItems() as $item) {
            if ($item->requiredCompanyNumber) {
                return TRUE;
            }
        }

        return FALSE;
    }

    /**
     * Provide update company numbers for basket items
     *
     * @param array $data
     * @param array $companies
     */
    public function updateCompanyNumbers($data, $companies)
    {
        foreach ($data as $key => $val) {
            /** @var BasketProduct $item */
            $item = $this->items[$key];

            // from select
            if (($val['radio'] == 'existing') && ($val['select'] != '?')) {
                if (isset($companies['Incorporated'][$val['select']])) {
                    $item->companyName = $companies['Incorporated'][$val['select']];
                } elseif (isset($companies['Not Incorporated'][$val['select']])) {
                    $item->companyName = $companies['Not Incorporated'][$val['select']];
                } else {
                    $item->companyName = 'N/A';
                }

                if (Strings::startsWith($val['select'], 'imported_')) {
                    $item->linkedCompanyStatus = BasketProduct::LINK_NEW_SELECT;
                    list(, $item->companyNumber) = explode('_', $val['select']);
                } else {
                    $item->linkedCompanyStatus = BasketProduct::LINK_EXISTING_SELECT;
                    $item->setCompanyId($val['select']);
                }
            } elseif (($val['radio'] == 'import') && (!empty($val['text']))) {
                // linkedCompanyStatus is set by Import method in controller
                if ($item->isLinkedToNewImportedCompany()) {
                    $item->setCompanyId(NULL);
                }
                $item->companyNumber = Strings::padLeft($val['text'], 8, '0');
            } elseif (($val['radio'] == 'existing') && ($val['select'] == '?')) {
                $item->linkedCompanyStatus = NULL;
                $item->setCompanyId(NULL);
                $item->companyNumber = NULL;
                $item->companyName = '';
            }
        }
    }


    /**
     * Returns if package is included
     * @return boolean
     */
    public function hasPackageIncluded()
    {
        foreach ($this->items as $key => $item) {
            if ($item instanceof Package) {
                return TRUE;
            }
        }
        return FALSE;
    }


    /**
     * Check if Barclays banking is included in basket
     *
     * @return boolean
     */
    public function hasBarlaysBankingIncluded()
    {
        foreach ($this->items as $key => $item) {
            if ($item->getClass() == 'BarclaysBanking') {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * @return Package
     * @throws Exception
     */
    public function getPackage()
    {
        $package = $this->getPackageOrNull();
        if (!$package) {
            throw new Exception("Basket doesn't include Package");
        }

        return $package;
    }

    /**
     * @return Package|null
     */
    public function getPackageOrNull()
    {
        foreach ($this->items as $key => $item) {
            if ($item instanceof Package) {
                return $item;
            }
        }

        return NULL;
    }

    /**
     * @param int $packageId
     * @return NULL|Package
     */
    public function getPackageById($packageId)
    {
        foreach ($this->items as $item) {
            if ($item instanceof Package && $item->getId() == $packageId) {
                return $item;
            }
        }
    }

    /**
     * @param int $packageId
     * @return bool
     */
    public function hasPackage($packageId)
    {
        return (bool) $this->getPackageById($packageId);
    }

    /**
     * @param int $productId
     * @return bool
     */
    public function hasProduct($productId)
    {
        foreach ($this->items as $item) {
            if ($item->getId() == $productId) {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * is our basket free
     * @return bool
     */
    public function isFreeBasket()
    {
        //do we have items in the basket
        if ($this->getItemsCount() > 0) {
            //do we have products that are not free
            $nonFreeItems = 0;
            foreach ($this->getItems() as $item) {
                if ($item->getPrice() > 0) {
                    $nonFreeItems++;
                }
            }
            $subtotal = $this->getPrice('subTotal');
            //do we have voucher applied
            if ($this->voucher && $this->voucher->isAvailableForUse($subtotal) && $nonFreeItems > 0) {
                if ($this->getPrice('total') === '0.00') {
                    return TRUE;
                }
            }
        }
        return FALSE;
    }

    /**
     * @return Package
     */
    public function hasPackageInBasket()
    {
        $package = NULL;
        foreach ($this->items as $item) {
            if ($item instanceof Package && isset(Package::$types[$item->getId()])) {
                $package = $item;
            }
        }
        return $package;
    }

    /**
     * @return bool
     */
    public function hasWholesalePackage()
    {
        foreach ($this->items as $item) {
            if ($item instanceof WholesalePackage) {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * @return bool
     */
    public function isUsingCredit()
    {
        return $this->credit > 0;
    }

    /**
     * @param float $amount
     */
    public function setCredit($amount)
    {
        $this->credit = $amount;
    }

    /**
     * @return float
     */
    public function getCredit()
    {
        return $this->credit;
    }

    /**
     * @return bool
     */
    public function isInsufficientCredit()
    {
        return $this->isUsingCredit()
            && ($this->getPrice('totalWithoutCredit') > $this->getCredit());
    }

    /**
     * @return bool
     */
    public function hasItemWithIdCheckRequired()
    {
        foreach ($this->items as $item) {
            if ($item->isIdCheckRequired) {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * @param int $productId
     */
    public function removeByProductId($productId)
    {
        foreach ($this->getItems() as $itemId => $item) {
            if ($item->getId() == $productId) {
                $this->remove($itemId);
            }
        }
    }

    /**
     * @param array $productIds
     * @return BasketProduct[]
     */
    public function getProductsInCommonByIds(array $productIds)
    {
        $containedBlacklistProducts = [];
        foreach ($this->getItems() as $itemId => $product) {
            if (in_array($product->getId(), $productIds)) {
                $containedBlacklistProducts[$itemId] = $product;
            }
        }

        return $containedBlacklistProducts;
    }

    /**
     * @param CompanyName $companyName
     */
    public function setIncorporationCompanyName(CompanyName $companyName)
    {
        $this->companyName = $companyName;
        $package = $this->getPackageOrNull();
        if ($package) {
            $package->companyName = (string) $companyName;
        }
    }

    /**
     * @return CompanyName
     */
    public function getIncorporationCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @return bool
     */
    public function hasIncorporationCompanyName()
    {
        return (bool) $this->companyName;
    }

    /**
     * @return bool
     */
    public function isFreePurchase()
    {
        return $this->isFreeBasket();
    }

    /**
     * @return float
     */
    public function getTotalPrice()
    {
        return $this->getPrice('total');
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $items = $basket = [];
        foreach ($this->getItems() as $item) {
            $items[] = [
                'title' => $item->getLngTitle(),
                'qty' => (int) $item->qty,
                'price' => (float) $item->getPrice()
            ];
        }
        return [
            'items' => $items,
            'subTotalPrice' => (float) $this->getPrice('subTotal2'),
            'vat' => (float) $this->getPrice('vat'),
            'creditPrice' => $this->isUsingCredit() ? (float) $this->getPrice('account') : NULL,
            'totalPrice' => (float) $this->getPrice('total'),
            'totalPriceWithoutCredit' => (float) $this->getPrice('totalWithoutCredit'),
            'useCredit' => $this->isUsingCredit()
        ];
    }

    /**
     * @param int $credits
     * @return bool
     */
    public function isCreditMinimumAmountReached($credits)
    {
        $totalPriceWithoutCredit = $this->getPrice('totalWithoutCredit');
        return $totalPriceWithoutCredit - $credits > 0 && $totalPriceWithoutCredit - $credits <= 1;
    }

    /**
     * @return float
     */
    public function getTotalPriceWithoutCredit()
    {
        return $this->getPrice('totalWithoutCredit');
    }
}
