<?php

use Entities\CompanyDocument;

class CompanyDocumentStorage
{
    /**
     * @var string
     */
    private $absoluteStoragePath;

    /**
     * @var string
     */
    private $relativeStoragePath;

    /**
     * @param string $absoluteStoragePath
     * @param string $relativeStoragePath
     */
    public function __construct($absoluteStoragePath, $relativeStoragePath)
    {
        $this->absoluteStoragePath = $absoluteStoragePath;
        $this->relativeStoragePath = $relativeStoragePath;
    }

    /**
     * @param CompanyDocument $companyDocument
     * @param boolean $relative
     * @return string
     */
    public function getDocumentDir(CompanyDocument $companyDocument, $relative = FALSE)
    {
        $dir = sprintf("%s0000", floor($companyDocument->getCompany()->getCompanyId() / 10000) + 1);
        $path = $relative ? $this->relativeStoragePath : $this->absoluteStoragePath;
        return sprintf("%s%s/", $path, $dir . '/' . $companyDocument->getCompany()->getCompanyId());
    }

    /**
     * @param CompanyDocument $companyDocument
     * @param boolean $relative
     * @return string
     */
    public function getDocumentPath(CompanyDocument $companyDocument, $relative = FALSE)
    {
        return sprintf("%s%s", $this->getDocumentDir($companyDocument, $relative), $companyDocument->getFileName());
    }

    /**
     * @param CompanyDocument $companyDocument
     * @throws FileNotFoundException
     * @return string
     */
    public function getDocumentContent(CompanyDocument $companyDocument)
    {
        if ($this->documentExists($companyDocument)) {
            return file_get_contents($this->getDocumentPath($companyDocument));
        } else {
            throw new FileNotFoundException("File '{$companyDocument->getFileName()}' with id `{$companyDocument->getId()}` does not exist");
        }
    }

    /**
     * @param CompanyDocument $companyDocument
     * @throws Exception
     */
    public function deleteDocument(CompanyDocument $companyDocument)
    {
        if ($this->documentExists($companyDocument)) {
            if (unlink($this->getDocumentPath($companyDocument)) === FALSE) {
                throw new Exception("Something is wrong, we may not have enough permission to delete `{$companyDocument->getFileName()}` with id `{$companyDocument->getId()}`");
            }
        } else {
            throw new Exception("File `{$companyDocument->getFileName()}` with id `{$companyDocument->getId()}` does not exist");
        }
    }

    /**
     * @param CompanyDocument $companyDocument
     * @return bool
     */
    public function documentExists(CompanyDocument $companyDocument)
    {
        return file_exists($this->getDocumentPath($companyDocument));
    }
}
