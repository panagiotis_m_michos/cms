<?php

use BankingModule\Entities\CompanyCustomer as CompanyCustomerEntity;

class CardOne
{

    const CARD_ONE_PRODUCT = 1405;

    /**
     *
     * @var BusinessBankingEmailer
     */
    private $businessBankingEmailer;

    public function __construct(BusinessBankingEmailer $businessBankingEmailer)
    {
        $this->businessBankingEmailer = $businessBankingEmailer;
    }

    /**
     * retrieves ids of companies that are supposed to be sent to barclays
     * 
     * @return array ids - returns ids of the companies that haven't been sent to barclays
     */
    public function getCompaniesIds()
    {
        return CHFiling::getDb()->fetchCol(
            'SELECT com.company_id
                FROM cms2_company_customer AS bank
                LEFT JOIN ch_card_one AS cardOne ON cardOne.company_id = bank.companyId
                LEFT JOIN ch_company AS com ON com.company_id = bank.companyId
                LEFT JOIN cms2_customers c ON com.customer_id = c.customerId
                WHERE
                  bank.bankTypeId = "' . CompanyCustomerEntity::BANK_TYPE_CARD_ONE. '"
                  AND cardOne.company_id IS NULL
                  AND com.company_number IS NOT NULL
                  AND c.countryId = 223
                GROUP BY com.company_id'
        );
    }

    public function sendCardOneCompanies(array $ids)
    {
        foreach ($ids as $id) {
            try {
                if (CompanyCustomer::hasCustomerCompanyFilled($id) === TRUE) {
                    $this->sendCompany($id);
                }
            } catch (Exception $e) {
                $this->businessBankingEmailer->cardOneErrorEmail($e, $id);
            }
        }
    }

    /**
     * sends company information to CardOne bank
     * @throws exception
     */
    private function sendCompany($id)
    {

        $data = array('company_id' => $id);
        CHFiling::getDb()->insert('ch_card_one', $data);
        $cardOneId = CHFiling::getDb()->lastInsertId();

        //just for wholesale
        $companyDetails = Company::getCompany($id);
        $customerDetails = CompanyCustomer::getCompanyCustomerByCompanyId($id, CompanyCustomerEntity::BANK_TYPE_CARD_ONE);

        $this->businessBankingEmailer->sendCompanyToCardOne($companyDetails, $customerDetails);

        $this->saveResponse($cardOneId);
    }

    /**
     * Saves response to the db.
     *
     * @param int $cardOneId - primary key from db
     */
    private function saveResponse($cardOneId)
    {
        /* update database */
        $data = array(
            'date_sent' => new Zend_Db_Expr('NOW()'),
            'success' => 1);
        CHFiling::getDb()->update(
            'ch_card_one', $data, 'card_one_id = ' . $cardOneId
        );
    }

    /**
     * Returns data by company id
     *
     * @param int $companyId
     * @return array
     */
    public static function getBankingData($companyId)
    {
        $data = CHFiling::getDb()->fetchAll("SELECT * FROM `ch_card_one` WHERE `company_id`=$companyId ORDER BY `date_sent` DESC");
        return $data;
    }

}
