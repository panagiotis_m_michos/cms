<?php

require_once dirname(__FILE__).'/CHFiling.php';

/**
 * Description of Envelope
 *
 * @author pete
 */
final class Envelope {
    /**
     * enveloep id from database,
     * used as transaction id tag as well
     * @var int
     */
    private $envelopeId;

    /**
     * set when envelope contains form submission
     * @var int
     */
    private $formSubmissionId;

    /**
     * envelope class tag
     *
     * @var string
     */
    private $class;

    /**
     *
     * @var boolean
     */
    private $success;

    /**
     * indicates if this is test transaction|submission
     *
     * @var boolean
     */
    private $gatewayTest;

    /**
     * this is when the envelope was created,
     * so it is advised to create envelopes just before submission
     *
     * @var string YYYY-MM-DD
     */
    private $dateSent;

    /**
     * name of the table holding all envelope data
     *
     * @var string
     */
    private static $tableName = 'ch_envelope';

    /**
     *
     * @param int $envelopeId
     */
    private function __construct($envelopeId)
    {
        // --- check that envelopeId is int ---
        if (!preg_match('/^\d+$/', $envelopeId)) {
            throw new Exception('envelopeId has to be integer');
        }

        $sql = "
			SELECT * FROM `".self::$tableName."`
			WHERE `envelope_id` = $envelopeId";
        if (!$result = CHFiling::getDb()->fetchRow($sql)) {
            throw new Exception('Record doesn\'t exist');
        }

        // --- set instance variables ---
        $this->envelopeId   = $result['envelope_id'];
        $this->formSubmissionId = $result['form_submission_id'];
        $this->class        = $result['class'];
        $this->gatewayTest  = $result['gateway_test'];
        $this->dateSent     = $result['date_sent'];
        $this->success      = $result['success'];
    }

    /**
     * return instance of existing envelope
     *
     * @param int $envelopeId
     * @return Envelope
     */
    public static function getEnvelope($envelopeId) {
        return new Envelope($envelopeId);
    }

    /**
     * creates and return new envelope - ready for submission
     * if this envelope is for form submission - supply id
     * call this method just before sending it to companies house
     *
     * @param int $formSubmissionId
     * @return Envelope
     */
    public static function getNewEnvelope($formSubmissionId = null) {

        // --- check that formSubmissionId is int ---
        if (!is_null($formSubmissionId)) {
            if (!preg_match('/^\d+$/', $formSubmissionId)) {
                throw new Exception('formSubmissionId has to be integer or null');
            }
        }

        // --- insert data into database ---
        $data = array(
            'form_submission_id' => $formSubmissionId,
            'gateway_test'  => CHFiling::getGatewayTest()
        );
        CHFiling::getDb()->insert(self::$tableName, $data);

        return new Envelope(CHFiling::getDb()->lastInsertId());
    }

    /**
     * return all envelopes belonging to specific form submission
     * envelopes are ordered descending so first envelope is at index 0
     *
     * @param int $formSubmissionId
     * @return array<Envelope>
     */
    public static function getFormSubmissionEnvelopes($formSubmissionId) {
        // --- query ---
        $sql = "
			SELECT `envelope_id` FROM `".self::$tableName."`
			WHERE `form_submission_id` = $formSubmissionId ORDER BY `envelope_id` DESC";
        $result = CHFiling::getDb()->fetchCol($sql);

        $envelopes = array();
        foreach ($result as $id) {
            $envelopes[] = new Envelope($id);
        }

        return $envelopes;
    }

    /**
     * returns this envelope's id
     *
     * @return int
     */
    public function getEnvelopeId() {
        return $this->envelopeId;
    }

    /**
     *
     * @return string
     */
    public function getDateSent() {
        return $this->dateSent;
    }

    /**
     * set to true when the response is received
     * false is default, so no need to set when nothing's received
     *
     * @param boolean $success
     */
    private function setSuccess($success) {
        $success = ($success) ? 1 : 0;
        CHFiling::getDb()->update(self::$tableName, array('success' => $success), 'envelope_id = ' . $this->envelopeId);
        $this->success = $success;
    }

    /**
     * returns true if this envelope was successfuly sent
     *
     * @return boolean
     */
    public function getSuccess() {
        return $this->success;
    }

    /**
     *
     * @param string $class
     */
    private function setClass($class) {
        CHFiling::getDb()->update(
            self::$tableName,
            array('class' => $class),
            'envelope_id = ' . $this->envelopeId);
    }

    /**
     *
     */
    private function setDateSent() {
        CHFiling::getDb()->update(
            self::$tableName,
            array('date_sent' => new Zend_Db_Expr('NOW()')),
            'envelope_id = ' . $this->envelopeId);
    }

    /**
     * if response is xml error or some error, set it using this method
     *
     * @param array $errors
     */
    private function setErrors(array $errors) {
        $errorValues = array(
            'error_raised_by'   => 1,
            'error_number'      => 1,
            'error_type'        => 1,
            'error_text'        => 1,
            'error_location'    => 1);

        // --- save only allowed values ---
        $data = array();
        foreach ($errors as $key => $value) {
            if (isset($errorValues[$key])) {
                $data[$key] = $value;
            }
        }

        // --- update database ---
        if (!empty($data)) {
            CHFiling::getDb()->update(
                self::$tableName,
                $data,
                'envelope_id = ' . $this->envelopeId);
        }
    }

    /**
     * returns xml ready for submission
     * call this method just before submission, because it generates
     * date sent value automaticaly
     *
     * @param Request $request
     * @return xml
     */
    private function getXml(Request $request) {
        // --- set database ---
        $this->setClass($request->getClass());

        // --- create xml ---
        $test   = (CHFiling::getGatewayTest()) ? '' : '';
        $email  = (is_null(CHFiling::getEmailAddress())) ? '' :
            '<EmailAddress>'.CHFiling::getEmailAddress().'</EmailAddress>';

        return '<?xml version="1.0" encoding="UTF-8" ?>
        <GovTalkMessage
            xsi:schemaLocation="http://www.govtalk.gov.uk/CM/envelope
                http://xml'.$test.'gw.companieshouse.gov.uk/v1-0/schema/Egov_ch-v2-0.xsd"
            xmlns="http://www.govtalk.gov.uk/CM/envelope"
            xmlns:dsig="http://www.w3.org/2000/09/xmldsig#"
            xmlns:gt="http://www.govtalk.gov.uk/schemas/govtalk/core"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >
            <EnvelopeVersion>1.0</EnvelopeVersion>
            <Header>
                <MessageDetails>
                    <Class>'.$request->getClass().'</Class>
                    <Qualifier>request</Qualifier>
                    <TransactionID>'.$this->envelopeId.'</TransactionID>
                    <GatewayTest>'.CHFiling::getGatewayTest().'</GatewayTest>
                </MessageDetails>
                <SenderDetails>
                    <IDAuthentication>
                        <SenderID>'.md5(CHFiling::getSenderId()).'</SenderID>
                        <Authentication>
                            <Method>clear</Method>
                            <Value>'.md5(CHFiling::getPassword()).'</Value>
                        </Authentication>
                    </IDAuthentication>
                    '.$email.'
                </SenderDetails>
            </Header>
            <GovTalkDetails>
                <Keys/>
            </GovTalkDetails>
            <Body>'.
                $request->getXml()
            .'</Body>
        </GovTalkMessage>';
    }

    /**
     * sends submission to companies house and returns response
     *
     * @param Request $request
     * @return string
     */
    public function sendRequest(Request $request) {
        // --- get class ---
        $class = $request->getClass();

        // --- get xml ---
        $request = $this->getXml($request);

        // echo $request;exit;

        // --- set date sent ---
        $this->setDateSent();

        //---
        // send xml using https
        //---
        if (Feature::featurePsc()) {
            if (Feature::featurePscTesting()) {
                $server = 'xmlbeta.companieshouse.gov.uk';
            } else {
                $server = 'xmlgw.companieshouse.gov.uk';
            }
        } else {
            $server = (CHFiling::getGatewayTest()) ? 'xmlgw.companieshouse.gov.uk' : 'xmlgw.companieshouse.gov.uk';
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://'.$server.'/v1-0/xmlgw/Gateway');
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1) ;
        curl_setopt($ch, CURLOPT_TIMEOUT, 60); // how long to wait to receive a completely buffered output from the server
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5); //how long to wait to make a successful connection to the server before starting to buffer the output.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $response = curl_exec($ch);
        $curlinfo = curl_getinfo($ch);
        curl_close($ch);

        StatCollector::timing('CHFilingConnectionTime', $curlinfo['connect_time']);
        StatCollector::timing('CHFilingTotalTime', $curlinfo['total_time']);

        if ($response) {
            Notifier::triggerSuccess('cms-chfilling-connectioncheck', time(), $curlinfo['connect_time']);
        } else {
            Notifier::triggerFailure('cms-chfilling-connectioncheck', time(), $curlinfo['connect_time']);
        }

        // --- there was a problem - nothing received ---
        if (!isset($response) || empty($response)) {
            return $response;
        }

        // --- check if there are any errors returned ---
        /* need to convert to utf-8 just to make sure, because companies house */
        /* sends sometimes data that are not utf-8 even though that put utf-8 head in xml */
        $response = mb_convert_encoding($response, 'UTF-8', 'UTF-8');
        $xmlResponse = simplexml_load_string($response);

        // --- response returned - it is xml, so set success ---
        $this->setSuccess(1);

        // --- ||| loging all request into the folders ||| ---
        $submissionFile = CHFiling::getDocPath().'/_submission/'.CHFiling::getDirName($this->envelopeId).'/';
        // --- create folder if needed ---
        if (!is_dir($submissionFile)) {
            mkdir($submissionFile, 0777, true);
        }
        $submissionFile .= 'envelope-'.$this->envelopeId.'/';

        // --- create folder if needed ---
        if (!is_dir($submissionFile)) {
            mkdir($submissionFile, 0777, true);
        }
        // --- --- ---

        // --- save ---
        file_put_contents($submissionFile . 'request.xml', $request);
        file_put_contents($submissionFile . 'response.xml', $response);
        // --- ||| delete this ||| ---

        // --- check if there are any errors ---
        if (isset($xmlResponse->GovTalkDetails->GovTalkErrors->Error)) {
            // --- save xml that has been sent, so we can check it ---
            // --- but not for form submission status - coz it produces errror ---
            // --- when no form submissions found ---
            if ($class != 'GetSubmissionStatus') {
                $errorFile = CHFiling::getDocPath().'/_error/'.CHFiling::getDirName($this->envelopeId).'/';
                // --- create folder if needed ---
                if (!is_dir($errorFile)) {
                    mkdir($errorFile, 0777, true);
                }
                $errorFile .= 'envelope-'.$this->envelopeId.'/';

                // --- create folder if needed ---
                if (!is_dir($errorFile)) {
                    mkdir($errorFile, 0777, true);
                }

                // --- save ---
                file_put_contents($errorFile . 'request.xml', $request);

                /* delete old one !!!
                $errorFile = CHFiling::getDocPath().'/_error/envelope-'.$this->envelopeId.'/';

                // --- create folder if needed ---
                if (!is_dir($errorFile)) {
                    mkdir($errorFile, 0777, true);
                }
                // --- save ---
                file_put_contents($errorFile . 'request.xml', $request);
                 */
            }

            $error  = $xmlResponse->GovTalkDetails->GovTalkErrors->Error;
            $errors = array();

            // --- get error values from response ---
            if (isset($error->RaisedBy)) {
                $errors['error_raised_by'] = (string) $error->RaisedBy;
            }
            if (isset($error->Number)) {
                $errors['error_number'] = (string) $error->Number;
            }
            if (isset($error->Type)) {
                $errors['error_type'] = (string) $error->Type;
            }
            if (isset($error->Text)) {
                $errors['error_text'] = (string) $error->Text;
            }
            if (isset($error->RaisedBy)) {
                $errors['error_location'] = (string) $error->Location;
            }

            // --- set errors ---
            $this->setErrors($errors);
        }
        unset($xmlResponse);

        return $response;
    }
}
