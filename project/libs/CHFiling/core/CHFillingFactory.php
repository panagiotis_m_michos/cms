<?php

class CHFillingFactory
{

    public static function setCHFilling()
    {
        $chFiling = FApplication::$config['chfiling'];
        CHFiling::setPackageReference($chFiling['packageReference']);
        CHFiling::setSenderId($chFiling['senderId']);
        CHFiling::setPassword($chFiling['password']);
        CHFiling::setEmailAddress($chFiling['emailAddress']);
        CHFiling::setGatewayTest($chFiling['gatewayTest']);
        CHFiling::setDocPath(PROJECT_DIR . '/temp/upload/ch_documents');
    }

}
