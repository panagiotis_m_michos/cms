<?php

class Shares extends Element
{

    /**
     * __construct 
     * 
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct($this->getDBFields());
    }

    /**
     * getDBFields 
     *
     * allowed fields - fields from database
     * true indicates that the field is compulsory
     * 
     * @access public
     * @return array
     */
    public function getDBFields()
    {
        return array(
            'share_class'               => 1,
            'prescribed_particulars'    => 1,
            'num_shares'                => 1,
            'amount_paid'               => 1,
            'amount_unpaid'             => 1,
            'currency'                  => 1,
            'aggregate_nom_value'       => 1 
        );
    }

    /**
     * setShareClass 
     *
     * Pre-contition: $shareClass cannot be empty and 
     * can be maximum 50 characters long
     * 
     * @param string $shareClass 
     * @access public
     * @return void
     */
    public function setShareClass($shareClass)
    {
        $shareClass = trim($shareClass);
        if (strlen($shareClass) > 50) {
            throw new Exception('Share class cannot be longer than 50 characters');
        }
        if ($this->isEmpty($shareClass)) {
            throw new Exception('Share class cannot be empty');
        }
        $this->fields['share_class'] = $shareClass;
    }

    /**
     * getShareClass 
     * 
     * @access public
     * @return string
     */
    public function getShareClass()
    {
        return $this->fields['share_class'];
    }

    /**
     * setPrescribedParticulars 
     *
     * Pre-condition: $particulars cannot be empty and 
     * can be max 2000 characters long
     * 
     * @param string $particulars 
     * @access public
     * @return void
     */
    public function setPrescribedParticulars($particulars)
    {
        $particulars = (string) $particulars;

        $particulars = trim($particulars);
        if (strlen($particulars) < 1) {
            throw new Exception('Value cannot be empty');
        }

        if (strlen($particulars) > 2000) {
            throw new Exception('Max allowed characters is 2000');
        }

        $this->fields['prescribed_particulars'] = $particulars;
    }

    /**
     * getPrescribedParticulars 
     * 
     * @access public
     * @return string
     */
    public function getPrescribedParticulars()
    {
        return $this->fields['prescribed_particulars'];
    }

    /**
     * setNumShares 
     *
     * Pre-condition: $numShares has to be positive integer 
     * min 1 and mzx 11 digits
     * 
     * @param int $numShares 
     * @access public
     * @return void
     */
    public function setNumShares($numShares)
    {
        // --- number of shares ---
        $numShares = trim($numShares);
        if ($numShares <= 0) {
            throw new Exception('number of shares has to be more than 0');
        }
        if (!preg_match('/^\d{1,11}$/', $numShares)) {
            throw new Exception('The number of shares has to be a whole number between 1 and 11 digits long.');
        }
        $this->fields['num_shares'] = $numShares;
    }

    /**
     * getNumShares 
     * 
     * @access public
     * @return int
     */
    public function getNumShares()
    {
        return $this->fields['num_shares'];
    }

    /**
     * setAmountPaid
     *
     * Pre-condition: $amountPerShare can be only positive integer or 
     * positive decimal with maximum 3 digits
     * 
     * @param int|decimal $amountPerShare 
     * @access public
     * @return void
     */
    public function setAmountPaid($amount)
    {
        $amount = trim($amount);
        // --- number of shares ---
        /*
        if ($amount <= 0) {
            throw new Exception('amount per share has to be more than 0');
        }
        if (!preg_match('/^\d+(.{1}(\d{1,6})){0,1}$/', $amount)) {
            throw new Exception('Amount per share can be int or decimal with maximum 6 fraction digits');
        }
        */
        $this->fields['amount_paid'] = $amount;
    }

    /**
     * getAmountPaid
     * 
     * @access public
     * @return decimal
     */
    public function getAmountPaid()
    {
        return $this->fields['amount_paid'];
    }

    /**
     * setAmountUnpaid 
     *
     * Pre-condition: $amountPerShare can be only positive integer or 
     * positive decimal with maximum 3 digits
     * 
     * @param int|decimal $amountPerShare 
     * @access public
     * @return void
     */
    public function setAmountUnpaid($amount)
    {
        $amount = trim($amount);
        // --- number of shares ---
        /* companies house returns .5 0.5 and similar bullshit
        if (!preg_match('/^\d+(.{1}(\d{1,6})){0,1}$/', $amount)) {
            throw new Exception('Amount per share can be int or decimal with maximum 6 fraction digits');
        }
         */
        $this->fields['amount_unpaid'] = $amount;
    }

    /**
     * getAmountUnpaid 
     * 
     * @access public
     * @return int|decimal
     */
    public function getAmountUnpaid()
    {
        return $this->fields['amount_unpaid'];
    }

    /**
     * setCurrency 
     *
     * Pre-condition: $currency can be only ISO 4217 code
     * 
     * @param string $currency 
     * @access public
     * @return void
     */
    public function setCurrency($currency)
    {
        // --- currency ---
        $currency = trim($currency);
        $currency = strtoupper($currency);
        if (strlen($currency) > 3) {
            throw new Exception('Currency can be maximum 3 characters - use ISO 4217 codes');
        }
        $this->fields['currency'] = $currency;
    }

    /**
     * getCurrency 
     * 
     * @access public
     * @return string
     */
    public function getCurrency()
    {
        return $this->fields['currency'];
    }

    /**
     * setAggregateNomValue
     *
     * Pre-condition: $nomValue can only be positive decimal 
     * with max 6 fraction digits and 19 total digits
     * 
     * @param decimal $shareValue decimal
     * @access public
     * @return void
     */
    public function setAggregateNomValue($nomValue)
    {
        // --- share value ---
        $nomValue = trim($nomValue);
        if ($nomValue <= 0) {
            throw new Exception('share value has to be more than 0');
        }
        if (!preg_match('/^(\d+|\d+\.(\d){1,3}){1,19}$/', $nomValue)) {
            throw new Exception('Aggregate nominal value has to be decimal max 6 fraction digits and 19 total digits');
        }
        $this->fields['aggregate_nom_value'] = $nomValue;
    }

    /**
     * getAggregateNomValue
     * 
     * @access public
     * @return decimal
     */
    public function getAggregateNomValue()
    {
        return $this->fields['aggregate_nom_value'];
    }
}
