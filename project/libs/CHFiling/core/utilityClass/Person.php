<?php

final class Person extends Element
{
    // --- titles ----
    static public $titles = array('Mr' => 'Mr', 'Miss'=>'Miss', 'Mrs'=>'Mrs', 'Ms'=>'Ms', 'Dr'=>'Dr', 'Prof'=>'Prof', 'Master'=>'Master', 'Rev'=>'Rev', 'Sir'=>'Sir', 'Lord'=>'Lord');

    /**
     * __construct 
     * 
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct($this->getDBFields());
    }

    /**
     * getDBFields 
     *
     * allowed fields - fields from database
     * true indicates that the field is compulsory
     * 
     * @access public
     * @return array
     */
    public function getDBFields()
    {
        return array(
            'title'         => 1,
            'forename'      => 1, 
            'middle_name'   => 1,
            'surname'       => 1,
            'dob'           => 1, 
            'nationality'   => 1, 
            'occupation'    => 1, 
            'country_of_residence'  => 1
        );
    }

    /**
     * setTitle 
     * 
     * @param mixed $title 
     * @access public
     * @return void
     */
    public function setTitle($title)
    {
        $title = trim($title);
        if (strlen($title) > 50) {
            throw new Exception('Title cannot be longer than 50 characters');
        }
        if (empty($title)) { $title = null; }
        $this->fields['title'] = $title;
    }

    /**
     * getTitle 
     * 
     * @access public
     * @return string
     */
    public function getTitle()
    {
        return $this->fields['title'];
    }

    /**
     * setForename 
     *
     * Pre-condition: $forename cannot be longer than 50 characters
     * 
     * @param string $forename 
     * @access public
     * @return void
     */
    public function setForename($forename)
    {
        $forename = trim($forename);
        if (strlen($forename) > 50) {
            throw new Exception('Forename cannot be longer than 50 characters');
        }
        if (empty($forename)) { $forename = null; }
        $this->fields['forename'] = $forename;
    }

    /**
     * getForename 
     * 
     * @access public
     * @return string
     */
    public function getForename()
    {
        return $this->fields['forename'];
    }

    /**
     * setMiddleName
     *
     * Pre-condition: $middleName cannot be longer than 50 characters
     *
     * @param string $middleName
     * @access public
     * @return void
     */
    public function setMiddleName($middleName)
    {
        $middleName = trim($middleName);
        if (strlen($middleName) > 50) {
            throw new Exception('Forename cannot be longer than 50 characters');
        }
        if (empty($middleName)) { $middleName = null; }
        $this->fields['middle_name'] = $middleName;
    }

    /**
     * getMiddleName
     *
     * @access public
     * @return string
     */
    public function getMiddleName()
    {
        return $this->fields['middle_name'];
    }

    /**
     * setSurname 
     *
     * Pre-condition: $surname cannot be empty and cannot exceed 160 characters
     * 
     * @param mixed $surname 
     * @access public
     * @return void
     */
    public function setSurname($surname)
    {
        $surname = trim($surname);
        if (strlen($surname) > 160) {
            throw new Exception('Surname cannot be longer than 160 characters');
        }
        if (empty($surname) && $surname != 0) {
            throw new Exception('Surname cannot be empty');
        }
        $this->fields['surname'] = $surname;
    }

    /**
     * getSurname 
     * 
     * @access public
     * @return string
     */
    public function getSurname()
    {
        return $this->fields['surname'];
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return implode(' ', array_filter([$this->getTitle(), $this->getForename(), $this->getMiddleName(), $this->getSurname()]));
    }

    /**
     * @return string
     */
    public function getFullNameWithoutTitle()
    {
        return implode(' ', array_filter([$this->getForename(), $this->getMiddleName(), $this->getSurname()]));
    }

    /**
     * setDob
     *
     * Pre-condition: $dob has to be in date format YYYY-MM-DD
     * and has to be more or equal to today's date
     *
     * @param string $dob
     * @throws Exception
     * @access public
     */
    public function setDob($dob)
    {
        // --- date of birth ---
        // check format
        if (!preg_match('/^[\d]{4}-[\d]{1,2}-[\d]{1,2}$/', $dob)) {
            throw new Exception('Date of birth has to be in YYYY-MM-DD format');
        }

        $date   = explode('-', $dob);
        $month  = $date[1];
        $day    = $date[2];
        $year   = $date[0];

        // check if it's correct
        if (!checkdate($month, $day, $year)) {
            throw new Exception('Date of birth has to be correct date');
        }

        // check if date of birth is more than today's day
        if (strtotime($dob) > time()) {
            throw new Exception('Date of birth is less than today\'s date');
        }
        $this->fields['dob'] = $dob;
    }

    /**
     * getDob 
     *
     * Post-condition: returns date of birth in format: YYYY-MM-DD
     * 
     * @access public
     * @return date
     */
    public function getDob()
    {
        return $this->fields['dob'];
    }

    /**
     * setNationality 
     *
     * Pre-condition: $nationality cannot exceed 50 characters
     * 
     * @param string $nationality 
     * @access public
     * @return void
     */
    public function setNationality($nationality)
    {
        $nationality = trim($nationality);
        if (strlen($nationality) > 50) {
            throw new Exception('Nationality cannot be longer than 50 characters');
        }
        if (empty($nationality)) { $nationality = null; }
        $this->fields['nationality'] = $nationality;
    }

    /**
     * getNationality 
     * 
     * @access public
     * @return string
     */
    public function getNationality()
    {
        return $this->fields['nationality'];
    }

    /**
     * setOccupation 
     *
     * Pre-condition: $occupation cannot exceed 50 characters
     * 
     * @param string $occupation 
     * @access public
     * @return void
     */
    public function setOccupation($occupation)
    {
        $occupation = trim($occupation);
        if (strlen($occupation) > 50) {
            throw new Exception('Nationality cannot be longer than 50 characters');
        }
        if (empty($occupation)) { $occupation = null; }
        $this->fields['occupation'] = $occupation;
    }

    /**
     * getOccupation 
     * 
     * @access public
     * @return string
     */
    public function getOccupation()
    {
        return $this->fields['occupation'];
    }

    /**
     * setAppointmentDate 
     * 
     * @access public
     * @return string
     */
    public function setAppointmentDate($appointmentDate)
    {
        $this->fields['appointment_date'] = $appointmentDate;
    }

     /**
     * getAppointmentDate
     *
     * @access public
     * @return string
     */
    public function getAppointmentDate()
    {
        return $this->fields['appointment_date'];
    }

    /**
     * setCountryOfResidence 
     * 
     * @param string $countryOfResidence 
     * @access public
     * @return void
     */
    public function setCountryOfResidence($countryOfResidence)
    {
        $countryOfResidence = trim($countryOfResidence);
        if (strlen($countryOfResidence) > 50) {
            throw new Exception('Nationality cannot be longer than 50 characters');
        }
        if (empty($countryOfResidence)) { $countryOfResidence = null; }
        $this->fields['country_of_residence'] = $countryOfResidence;
    }

    /**
     * getCountryOfResidence 
     * 
     * @access public
     * @return string
     */
    public function getCountryOfResidence()
    {
        return $this->fields['country_of_residence'];
    }
}
