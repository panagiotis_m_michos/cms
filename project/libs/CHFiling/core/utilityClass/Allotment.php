<?php

/**
 * used for allotmentOfShares request
 */
class Allotment extends AllotmentShares {

    /**
     * __construct
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct($this->getDBFields());
    }

    /**
     * getDBFields
     *
     * allowed fields - fields from database
     * true indicates that the field is compulsory
     *
     * @access public
     * @return array
     */
    public function getDBFields()
    {
        $fields =  parent::getDBFields();
        $fields['consideration'] = 1;
        return $fields;
    }

    /**
     * Pre-condition: String cannot be longer than 2000 characters
     * ohterwise exception is thrown
     *
     * @param String $consideration 
     */
    public function setConsideration($consideration) {
        $consideration = trim($consideration);
        if (strlen($consideration) > 2000) {
            throw new Exception('Consideration cannot be longer than 2000 characters');
        }
        if (empty($consideration)) { $consideration = null; }
        $this->fields['consideration'] = $consideration;
    }

    /**
     *
     * @return String - max 2000 characters
     */
    public function getConsideration() {
        return $this->fields['consideration'];
    }
}
