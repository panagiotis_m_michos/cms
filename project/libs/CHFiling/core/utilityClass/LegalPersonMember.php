<?php

abstract Class LegalPersonMember extends Member
{
    /**
     * @var Corporate
     */
    protected $corp;

    /**
     * @var Identification
     */
    protected $identification;

    protected function __construct()
    {
        parent::__construct();
        $this->corp = new Corporate();
        $this->identification = new Identification(Identification::PSC);
        $this->setCorporateType(2);
    }

    /**
     * @param array $data
     */
    public function setFields(array $data)
    {
        parent::setFields($data);
        $this->corp->setFields($data);
        // --- changed because on summary page for incorporated company ---
        // --- we don't get identification - after sync ---
        if (isset($data['identification_type'])) {
            $identificationType = array_search($data['identification_type'], Identification::$types);
        } else {
            $identificationType = Identification::PSC;
        }
        $this->identification = new Identification($identificationType);
        $this->identification->setFields($data);
    }

    /**
     * @return array
     */
    public function getFields()
    {
        $fields = array_merge(
            $this->corp->getFields(),
            $this->identification->getFields(),
            parent::getFields()
        );

        $type = $this->identification->getType();
        $fields['eeaType' . $type] = $type;
        $fields['eeaType'] = $type;

        return $fields;
    }

    /**
     * @param Corporate $corp
     */
    public function setCorporate(Corporate $corp)
    {
        $this->corp = clone $corp;
    }

    /**
     * @return Corporate
     */
    public function getCorporate()
    {
        return clone $this->corp;
    }

    /**
     * @param int $type
     * @return Identification
     * @throws Exception
     */
    public function getIdentification($type = NULL)
    {
        // --- type is null - identification is not set ---
        if (!isset($this->identification) && is_null($type)) {
            throw new Exception('identification is not set, so you need to specify type');
        }

        // --- type is null - identification is set ---
        if (is_null($type)) {
            return $this->identification;
        }

        // --- identification doesn't exist, create new ---
        if (!isset($this->identification)) {
            return new Identification($type);
        } else {
            // --- identification is the same as the set one ---
            if ($this->identification->getType() == $type) {
                return clone $this->identification;
            } else {
                return new Identification($type);
            }
        }
    }

    /**
     * @param Identification $identification
     */
    public function setIdentification(Identification $identification)
    {
        $this->identification = clone $identification;
    }
}

