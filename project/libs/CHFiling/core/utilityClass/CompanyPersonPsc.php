<?php

final class CompanyPersonPsc extends PersonPsc
{
    /**
     * @var int
     */
    private $companyMemberId;

    /**
     * @var int
     */
    private $companyId;

    /**
     * @var string
     */
    private static $type = 'PSC';

    /**
     * @var bool
     */
    private static $corporate = 0;

    /**
     * @var string
     */
    private static $tableName = 'ch_company_member';

    /**
     * @param int $companyId
     * @param int $companyMemberId
     * @throws Exception
     */
    public function __construct($companyId, $companyMemberId = NULL)
    {
        if (!preg_match('/^\d+$/', $companyId)) {
            throw new Exception('form submission id has to be integer');
        }

        parent::__construct();

        if (!$companyMemberId) {
            $this->companyMemberId = NULL;
            $this->companyId = $companyId;

            return;
        }

        if (!preg_match('/^\d+$/', $companyMemberId)) {
            throw new Exception('Company member id has to be integer');
        }

        $sql = "SELECT * FROM `" . self::$tableName . "` WHERE `company_member_id` = $companyMemberId AND `company_id` = $companyId";

        if (!$result = CHFiling::getDb()->fetchRow($sql)) {
            throw new Exception("Record doesn't exist");
        }

        $this->companyMemberId = $result['company_member_id'];
        $this->companyId = $result['company_id'];
        $this->setFields($result);
    }

    /**
     * @return string
     */
    public function getType()
    {
        return self::$type;
    }

    public function remove()
    {
        CHFiling::getDb()->delete(self::$tableName, 'company_member_id = ' . $this->companyMemberId);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->companyMemberId;
    }

    /**
     * @return bool
     */
    public function isComplete()
    {
        $allowedFields = [
            'surname', 'premise', 'street', 'post_town', 'country',
            'dob', 'nationality', 'country_of_residence',
            'residential_premise', 'residential_street', 'residential_post_town',
            'residential_country',
        ];

        $fields = array_merge(
            $this->getPerson()->getFields(),
            $this->getAddress()->getFields(),
            $this->getResidentialAddress()->getFields('residential_'),
            $this->getNatureOfControl()->getFields()
        );

        foreach ($allowedFields as $v) {
            if (!isset($fields[$v]) || $fields[$v] === NULL) {
                return 0;
            }
        }

        return 1;
    }

    /**
     * @throws Exception
     */
    public function save()
    {
        if (!$this->isComplete()) {
            throw new Exception('You need to set all compulsory fields');
        }

        $address = $this->getAddress()->getFields();
        unset($address['secure_address_ind']);
        $data = array_merge(
            $this->getPerson()->getFields(),
            $address,
            $this->getResidentialAddress()->getFields('residential_'),
            $this->getNatureOfControl()->getFields()
        );

        // --- these two fields are not for residential address ---
        unset($data['residential_care_of_name']);
        unset($data['residential_po_box']);

        $data['company_id'] = $this->companyId;
        $data['corporate'] = self::$corporate;
        $data['type'] = self::$type;
        $data['residential_secure_address_ind'] = FALSE;

        if (isset($this->companyMemberId) && $this->companyMemberId !== NULL) {
            CHFiling::getDb()->update(self::$tableName, $data, 'company_member_id = ' . $this->companyMemberId);
        } else {
            CHFiling::getDb()->insert(self::$tableName, $data);
            $this->companyMemberId = CHFiling::getDb()->lastInsertId();
        }
    }
}

