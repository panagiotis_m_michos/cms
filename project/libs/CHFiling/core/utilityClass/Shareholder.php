<?php

class Shareholder {

    /**
     * fields
     *
     * array of fields and their values
     *
     * @var array
     * @access private
     */
    private $fields   = array();

    /**
     *
     * @var Address
     */
    private $address;

    /**
     * __construct
     *
     * @access protected
     * @return void
     */
    public function __construct() {

        // --- set null value for each field ---
        $fields = $this->getDBFields();
        foreach($fields as $key => $value) {
            $this->fields[$key] = null;
        }
        $this->address = new Address();
    }

    /**
     * getDBFields
     *
     * allowed fields - fields from database
     * true indicates that the field is compulsory
     *
     * @access public
     * @return array
     */
    public function getDBFields()
    {
        return array(
            'forename'      => 1,
            'middle_name'   => 1,
            'surname'       => 1,
            'isRemoved'       => 1
        );
    }

    /**
     * isEmpty
     *
     * checks if value is set, if is empty - if value is 0 or '0'
     * then it's not empty
     *
     * @param mixed $value
     * @access protected
     * @return boolean
     */
    protected function isEmpty($value)
    {
        if (isset($value) && (!empty($value) || $value == 0)) {
            return false;
        }
        return true;
    }

    /**
     * setDBField
     *
     * Calls appropriate method for database field if exists,
     * so if database field is share_class, setShareClass is called
     *
     * Pre-condition: field has to exist, otherwise exception is thrown
     *
     * @param string $field
     * @param mixed $value
     * @access protected
     * @return void
     */
    protected function setDBField($field, $value)
    {
        // --- separate field ---
        $words = explode('_', $field);

        // --- create method from field ---
        $method = 'set';
        foreach ($words as $word) {
            $method .= ucfirst($word);
        }

        // --- call appropriate method if exists ---
        if (method_exists($this, $method)) {
            $this->$method($value);
        } else {
            echo $method;
            throw new Exception('Specified field doesn\'t exist');
        }
    }

    /**
     * setFields
     *
     * key is column in db, value is value
     * prefix is column prefix - so for service street
     * you can use 'service_' = 'service_street'
     *
     * @param array $fields
     * @param string $prefix
     * @access public
     * @return void
     */
    public function setFields(array $fields, $prefix = null)
    {
        // --- get allowed fields ---
        $allowedFields = $this->getDBFields();

        foreach ($fields as $key => $value) {

            // --- check if there's a prefix, if there isn't then continue ---
            if (!is_null($prefix)) {
                if (!preg_match("/^$prefix/", $key)) {
                    continue;
                }
            }

            $key = (is_null($prefix)) ? $key : preg_replace('/^'.$prefix.'/', '', $key , 1) ;
            // --- check if field is not empty ---
            if (!$this->isEmpty($value)) {
                // --- check if the field is allowed ---
                if (isset($allowedFields[$key])) {
                    // --- call appropriate method ---
                    $this->setDBField($key, $value);
                }
            }
        }
        $this->address->setFields($fields);
    }

    /**
     * getFields
     *
     * @param string $prefix prepend prefix
     * @access public
     * @return array
     */
    public function getFields($prefix = null)
    {
        $fields = array();
        // --- add prefix that was used to set fields ---
        foreach ($this->fields as $key => $value) {
            $fields[$prefix . $key] = $value;
        }

        return array_merge($this->address->getFields(), $fields);
    }

    /**
     *
     * @param string $forename - separate multiple forenames with ";"
     */
    public function setForename($forename) {

        $forename = trim($forename);
        if (strlen($forename) > 50) {
            throw new Exception('Forename cannot be longer than 50 characters');
        }
        if (empty($forename)) { $forename = null; }
        $this->fields['forename'] = $forename;
    }

    /**
     * Use explode(";", $shareholder->getForename()), because method returns
     * forenames separated by ";"
     *
     * @return string - multiple forenames are separated by ";" 
     */
    public function getForename() {
        return $this->fields['forename'];
    }

    /**
     * setMiddleName
     *
     * Pre-condition: $middleName cannot be longer than 50 characters
     *
     * @param string $middleName
     * @access public
     * @return void
     */
    public function setMiddleName($middleName)
    {
        $middleName = trim($middleName);
        if (strlen($middleName) > 50) {
            throw new Exception('Forename cannot be longer than 50 characters');
        }
        if (empty($middleName)) { $middleName = null; }
        $this->fields['middle_name'] = $middleName;
    }

    /**
     * getMiddleName
     *
     * @access public
     * @return string
     */
    public function getMiddleName()
    {
        return $this->fields['middle_name'];
    }

    /**
     * setSurname
     *
     * Pre-condition: $surname cannot be empty and cannot exceed 160 characters
     *
     * @param mixed $surname
     * @access public
     * @return void
     */
    public function setSurname($surname)
    {
        $surname = trim($surname);
        if (strlen($surname) > 160) {
            throw new Exception('Surname cannot be longer than 160 characters');
        }
        if (empty($surname) && $surname != 0) {
            throw new Exception('Surname cannot be empty');
        }
        $this->fields['surname'] = $surname;
    }

    /**
     * getSurname
     *
     * @access public
     * @return string
     */
    public function getSurname()
    {
        return $this->fields['surname'];
    }

    /**
     * setAddress
     *
     * @param Address $address
     * @access public
     * @return void
     */
    public function setAddress(Address $address)
    {
        $this->address = clone $address;
    }

    /**
     * getAddress
     *
     * @access public
     * @return Address
     */
    public function getAddress()
    {
        return clone $this->address;
    }
    
    /**
     * setIsRemoved
     *
     * @param int $bool
     * @access public
     * @return void
     */
    public function setIsRemoved($bool)
    {
        if(!isset($bool) || $bool == NULL) {
            $this->fields['isRemoved'] = 0;
        } else {
            $this->fields['isRemoved'] = $bool;
        }
        
    }

    /**
     * getAddress
     *
     * @access public
     * @return Address
     */
    public function getIsRemoved()
    {
        return $this->fields['isRemoved'];
    }
    
}
