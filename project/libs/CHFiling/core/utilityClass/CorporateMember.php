<?php

abstract Class CorporateMember extends Member
{
    /**
     * 
     * @var Corporate
     */
    protected $corp;

    /**
     * identification
     * 
     * @var Identification
     * @access protected
     */
    protected $identification;

    /**
     * __construct 
     * 
     * @access protected
     * @return void
     */
    protected function __construct()
    {
        parent::__construct();
        $this->corp             = new Corporate();
        // --- set default to EEA ---
        // updated when set fields is called
        $this->identification   = new Identification(1);
        $this->setCorporateType(1);
    }

    /**
     * setFields 
     * 
     * @param array $data 
     * @access protected
     * @return void
     */
    public function setFields(array $data)
    {
        parent::setFields($data);
        $this->corp->setFields($data);
        // --- changed because on summary page for incorporated company ---
        // --- we don't get identification - after sync ---
        if (isset($data['identification_type'])) {
            $identificationType = array_search($data['identification_type'], Identification::$types);
        } else {
            $identificationType = 1;
        }
        $this->identification = new Identification($identificationType);
        $this->identification->setFields($data);
    }

    /**
     *
     * @return array
     */
    public function getFields()
    {
        $fields = array_merge(
                $this->corp->getFields(), $this->identification->getFields(), parent::getFields()
        );

        $type = $this->identification->getType();
        $fields['eeaType' . $type] = $type;
        $fields['eeaType'] = $type;
        return $fields;
    }

    /**
     * setCorporate 
     *
     * @param Corporate $corp
     * @access public
     * @return void
     */
    public function setCorporate(Corporate $corp)
    {
        $this->corp = clone $corp;
    }

    /**
     * getCorporate
     * 
     * @access public
     * @return Corporate
     */
    public function getCorporate()
    {
        return clone $this->corp;
    }

    /**
     * getIdentification 
     * 
     * @access public
     * @return Identification
     */
    public function getIdentification($type = null)
    {
        // --- type is null - identification is not set ---
        if (!isset($this->identification) && is_null($type)) {
            throw new Exception('identification is not set, so you need to specify type');
        }

        // --- type is null - identification is set ---
        if (is_null($type)) {
            return $this->identification;
        }

        // --- identification doesn't exist, create new ---
        if (!isset($this->identification)) {
            return new Identification($type);
        } else {
            // --- identification is the same as the set one ---
            if ($this->identification->getType() == $type) {
                return clone $this->identification;
            } else {
                return new Identification($type);
            }
        }
    }

    /**
     * setIdentification 
     * 
     * @param CompanyIdentification $identification 
     * @access public
     * @return void
     */
    public function setIdentification(Identification $identification)
    {
        $this->identification = clone $identification;
    }
}

