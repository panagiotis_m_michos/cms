<?php

class Identification extends Element
{
    const EEA = 1;
    const NonEEA = 2;
    const PSC = 3;

    /**
     * @var string[]
     */
    public static $types = [
        self::EEA => 'EEA',
        self::NonEEA => 'NonEEA',
        self::PSC => 'PSC'
    ];

    /**
     * @var int
     */
    private $type;

    /**
     * @var string
     */
    private $typeName;

    /**
     * @param int $type
     */
    public function __construct($type)
    {
        if (!isset(self::$types[$type])) {
            throw new Exception('specified type does not exist');
        }

        $this->type = $type;
        $this->typeName = self::$types[$type];
        parent::__construct($this->getDBFields());
    }

    /**
     * getType
     * 
     * @access public
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getTypeName()
    {
        return $this->typeName;
    }

    /**
     * getDBFields 
     *
     * allowed fields - fields from database
     * true indicates that the field is compulsory
     * 
     * @access public
     * @return array
     */
    public function getDBFields()
    {
        switch ($this->type) {
            case self::EEA:
                return [
                    'place_registered' => 1,
                    'registration_number' => 1,
                ];
                break;
            case self::NonEEA:
                return [
                    'place_registered' => 1,
                    'registration_number' => 1,
                    'law_governed' => 1,
                    'legal_form' => 1,
                ];
                break;
            case self::PSC:
                return [
                    'place_registered' => 1,
                    'registration_number' => 1,
                    'law_governed' => 1,
                    'legal_form' => 1,
                    'country_or_state' => 1,
                ];
                break;
        }
    }

    /**
     * setPlaceRegistered 
     *
     * Pre-condition: $placeRegistered cannot exceed 50 characters
     * 
     * @param mixed $placeRegistered 
     * @access public
     * @return void
     */
    public function setPlaceRegistered($placeRegistered)
    {
        $placeRegistered = trim($placeRegistered);
        if (strlen($placeRegistered) > 50) {
            throw new Exception('Place registered cannot be longer than 50 characters');
        }
        $this->fields['place_registered'] = $placeRegistered ?: NULL;
    }

    /**
     * getPlaceRegistered 
     * 
     * @access public
     * @return string
     */
    public function getPlaceRegistered()
    {
        return $this->fields['place_registered'];
    }

    /**
     * setRegistrationNumber 
     *
     * Pre-condition: $registrationNumber cannot exceed 20 characters
     * 
     * @param string $registrationNumber 
     * @access public
     * @return void
     */
    public function setRegistrationNumber($registrationNumber)
    {
        $registrationNumber = trim($registrationNumber);
        if (strlen($registrationNumber) > 20) {
            throw new Exception('Place registered cannot be longer than 20 characters');
        }
        $this->fields['registration_number'] = $registrationNumber ?: NULL;
    }

    /**
     * getRegistrationNumber 
     * 
     * @access public
     * @return string
     */
    public function getRegistrationNumber()
    {
        return $this->fields['registration_number'];
    }

    /**
     * setLawGoverned 
     *
     * Pre-condition: $lawGoverned cannot be empty 
     * and cannot exceed 50 characters
     * 
     * @param string $lawGoverned 
     * @access public
     * @return void
     */
    public function setLawGoverned($lawGoverned)
    {
        $lawGoverned = trim($lawGoverned);
        if (strlen($lawGoverned) > 50) {
            throw new Exception('Law governed cannot be longer than 50 characters');
        }
        if (empty($lawGoverned) && $lawGoverned != 0) {
            throw new Exception('Law Governed cannot be empty');
        }
        $this->fields['law_governed'] = $lawGoverned ?: NULL;
    }

    /**
     * getLawGoverned 
     * 
     * @access public
     * @return string
     */
    public function getLawGoverned()
    {
        return $this->fields['law_governed'];
    }

    /**
     * setLegalForm 
     *
     * Pre-condition: $legalForm cannot be empty 
     * and cannot exceed 50 characters
     * 
     * @param string $legalForm 
     * @access public
     * @return void
     */
    public function setLegalForm($legalForm)
    {
        $legalForm = trim($legalForm);
        if (strlen($legalForm) > 50) {
            throw new Exception('Legal form cannot be longer than 50 characters');
        }
        if (empty($legalForm) && $legalForm != 0) {
            throw new Exception('Legal form cannot be empty');
        }
        $this->fields['legal_form'] = $legalForm ?: NULL;
    }

    /**
     * getLegalForm 
     * 
     * @access public
     * @return string
     */
    public function getLegalForm()
    {
        return $this->fields['legal_form'];
    }

    /**
     * Pre-condition: $countryOrState cannot be empty
     * and cannot exceed 50 characters
     *
     * @param string $countryOrState
     * @throws Exception
     */
    public function setCountryOrState($countryOrState)
    {
        $countryOrState = trim($countryOrState);
        if (strlen($countryOrState) > 50) {
            throw new Exception('County or state cannot be longer than 50 characters');
        }
        $this->fields['country_or_state'] = $countryOrState ?: NULL;
    }

    /**
     * @return string
     */
    public function getCountryOrState()
    {
        return $this->fields['country_or_state'];
    }
}
