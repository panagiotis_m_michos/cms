<?php

use PeopleWithSignificantControl\Interfaces\ICorporatePsc;

class LegalPersonPsc extends LegalPersonMember implements ICorporatePsc
{
    /**
     * @var NatureOfControl
     */
    private $natureOfControl;

    /**
     * @var string
     */
    private $pscStatementNotification;

    public function __construct()
    {
        parent::__construct();
        $this->natureOfControl = new NatureOfControl;
    }

    /**
     * @return array
     */
    public function getFields()
    {
        return array_merge(
            ['psc_statement_notification' => $this->pscStatementNotification],
            $this->natureOfControl->getFields(),
            parent::getFields()
        );
    }

    /**
     * @param array $data
     */
    public function setFields(array $data)
    {
        parent::setFields($data);
        if (isset($data['psc_statement_notification'])) {
            $this->setPscStatementNotification($data['psc_statement_notification']);
        }
        $this->natureOfControl->setFields($data);
    }

    /**
     * @return NatureOfControl
     */
    public function getNatureOfControl()
    {
        return $this->natureOfControl;
    }

    /**
     * @param NatureOfControl $natureOfControl
     */
    public function setNatureOfControl(NatureOfControl $natureOfControl)
    {
        $this->natureOfControl = $natureOfControl;
    }

    /**
     * @return string
     */
    public function getPscStatementNotification()
    {
        return $this->pscStatementNotification;
    }

    /**
     * @param string $pscStatementNotification
     */
    public function setPscStatementNotification($pscStatementNotification)
    {
        $this->pscStatementNotification = $pscStatementNotification;
    }
}
