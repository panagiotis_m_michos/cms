<?php

final class CompanyCorporatePsc extends CorporatePsc
{
    /**
     * @var int
     */
    private $companyMemberId;

    /**
     * @var int
     */
    private $companyId;

    /**
     * @var string
     */
    private static $type = 'PSC';

    /**
     * @var bool
     */
    private static $corporate = 1;

    /**
     * @var string
     */
    private static $tableName = 'ch_company_member';

    /**
     * @param int $companyId
     * @param int $companyMemberId
     * @throws Exception
     */
    public function __construct($companyId, $companyMemberId = NULL)
    {
        if (!preg_match('/^\d+$/', $companyId)) {
            throw new Exception('companyId has to be integer');
        }

        parent::__construct();

        if (!$companyMemberId) {
            $this->companyMemberId = NULL;
            $this->companyId = $companyId;

            return;
        }

        if (!preg_match('/^\d+$/', $companyMemberId)) {
            throw new Exception('companyMemberId has to be integer');
        }

        //todo: psc - this is not needed as it's setting fields when creating these objects in Company object
        $sql = "SELECT * FROM `" . self::$tableName . "`
			    WHERE `company_member_id` = $companyMemberId AND `company_id` = $companyId";

        if (!$result = CHFiling::getDb()->fetchRow($sql)) {
            throw new Exception("Record doesn't exist");
        }

        $this->companyMemberId = $result['company_member_id'];
        $this->companyId = $result['company_id'];
        $this->setFields($result);
    }

    /**
     * @return string
     */
    public function getType()
    {
        return self::$type;
    }

    public function remove()
    {
        CHFiling::getDb()->delete(self::$tableName, 'company_member_id = ' . $this->companyMemberId);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->companyMemberId;
    }

    /**
     * @return bool
     */
    public function isComplete()
    {
        $allowedFields = ['corporate_name', 'premise', 'street', 'post_town', 'country'];

        $fields = array_merge(
            $this->getCorporate()->getFields(),
            $this->getAddress()->getFields()
        );

        foreach ($allowedFields as $v) {
            if (!isset($fields[$v]) || $fields[$v] === NULL) {
                return 0;
            }
        }

        return 1;
    }

    /**
     * @throws Exception
     */
    public function save()
    {
        if (!$this->isComplete()) {
            throw new Exception('You need to set all compulsory fielsd');
        }

        // --- prepare data ---
        $address = $this->getAddress()->getFields();
        unset($address['secure_address_ind']);
        $data = array_merge(
            $this->getCorporate()->getFields(),
            $address,
            $this->getNatureOfControl()->getFields()
        );

        $data['company_id'] = $this->companyId;
        $data['corporate'] = self::$corporate;
        $data['type'] = self::$type;

        // --- save data ---
        if (isset($this->companyMemberId) && !is_null($this->companyMemberId)) {
            CHFiling::getDb()->update(self::$tableName, $data, 'company_member_id = ' . $this->companyMemberId);
        } else {
            CHFiling::getDb()->insert(self::$tableName, $data);
            $this->companyMemberId = CHFiling::getDb()->lastInsertId();
        }
    }
}

