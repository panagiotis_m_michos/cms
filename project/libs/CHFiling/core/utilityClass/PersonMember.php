<?php

abstract class PersonMember extends Member
{
    /**
     * person
     * 
     * @var Person
     * @access protected
     */
    private $person;

    /**
     * __construct 
     * 
     * @access protected
     * @return void
     */
    protected function __construct()
    {
        parent::__construct();
        // --- set person ---
        $this->person = new Person();
        $this->setCorporateType(0);
    }

    /**
     * setFields 
     * 
     * @param array $data 
     * @access protected
     * @return void
     */
    public function setFields(array $data)
    {
        parent::setFields($data);
        $this->person->setFields($data);
    }

    /**
     * getFields 
     * 
     * @access protected
     * @return array
     */
    public function getFields() {
        return array_merge(
            $this->person->getFields(), 
            parent::getFields()
        );
    }

    /**
     * setPerson 
     * 
     * @access protected
     * @return void
     */
    public function setPerson(Person $person) {
        $this->person = clone $person;
    }

    /**
     * getPerson 
     * 
     * @access protected
     * @return Person
     */
    public function getPerson() {
        return clone $this->person;
    }

    /**
     * @param PersonMember $personMember
     * @return bool
     */
    public function isSamePerson(PersonMember $personMember)
    {
        return $this->person->getFullNameWithoutTitle() == $personMember->getPerson()->getFullNameWithoutTitle();
    }
}
