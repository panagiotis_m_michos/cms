<?php

/**
 * Address 
 * 
 * @final
 * @package chutil
 * @version $id$
 * @copyright 2009 Peter Reisinger
 * @author Peter Reisinger <p.reisinger@gmail.com> 
 * @license GNU General Public License
 */
final class Address extends Element
{
    /**
     * all countries
     *
     * used for country element
     */
    public static $countries = array(
        'GBR' => 'United Kingdom',
	    "Afghanistan" => "Afghanistan",
	    "Albania" => "Albania",
	    "Algeria" => "Algeria",
	    "American Samoa" => "American Samoa",
	    "Andorra" => "Andorra",
	    "Angola" => "Angola",
	    "Anguilla" => "Anguilla",
	    "Antarctica" => "Antarctica",
	    "Antigua And Barbuda" => "Antigua And Barbuda",
	    "Argentina" => "Argentina",
	    "Armenia" => "Armenia",
	    "Aruba" => "Aruba",
        'AUS' => 'Australia',
        'AUT' => 'Austria',
	    "Azerbaijan" => "Azerbaijan",
	    "Bahamas" => "Bahamas",
	    "Bahrain" => "Bahrain",
	    "Bangladesh" => "Bangladesh",
	    "Barbados" => "Barbados",
	    "Belarus" => "Belarus",
        'BEL' => 'Belgium',
	    "Belize" => "Belize",
	    "Benin" => "Benin",
	    "Bermuda" => "Bermuda",
	    "Bhutan" => "Bhutan",
	    "Bolivia" => "Bolivia",
	    "Bosnia And Herzegowina" => "Bosnia And Herzegowina",
	    "Botswana" => "Botswana",
	    "Bouvet Island" => "Bouvet Island",
	    "Brazil" => "Brazil",
	    "Brunei Darussalam" => "Brunei Darussalam",
	    "Bulgaria" => "Bulgaria",
	    "Burkina Faso" => "Burkina Faso",
	    "Burundi" => "Burundi",
	    "Cambodia" => "Cambodia",
	    "Cameroon" => "Cameroon",
        'CAN' => 'Canada',
	    "Cape Verde" => "Cape Verde",
	    "Cayman Islands" => "Cayman Islands",
	    "Central African Republic" => "Central African Republic",
	    "Chad" => "Chad",
	    "Chile" => "Chile",
	    "China" => "China",
	    "Christmas Island" => "Christmas Island",
	    "Cocos (Keeling) Islands" => "Cocos (Keeling) Islands",
	    "Colombia" => "Colombia",
	    "Comoros" => "Comoros",
	    "Congo" => "Congo",
	    "Cook Islands" => "Cook Islands",
	    "Costa Rica" => "Costa Rica",
	    "Cote D'Ivoire" => "Cote D'Ivoire",
        'HRV' => 'Croatia',         
	    "Cuba" => "Cuba",
        'CYP' => 'Cyprus',
        'CZE' => 'Czech Republic',  
        'DNK' => 'Denmark',         
	    "Djibouti" => "Djibouti",
	    "Dominica" => "Dominica",
	    "Dominican Republic" => "Dominican Republic",
	    "East Timor" => "East Timor",
	    "Ecuador" => "Ecuador",
	    "Egypt" => "Egypt",
	    "El Salvador" => "El Salvador",
        'GB-ENG' => 'England',      
	    "Equatorial Guinea" => "Equatorial Guinea",
	    "Eritrea" => "Eritrea",
        'EST' => 'Estonia',
	    "Ethiopia" => "Ethiopia",
	    "Falkland Islands" => "Falkland Islands",
	    "Faroe Islands" => "Faroe Islands",
	    "Fiji" => "Fiji",
	    "Finland" => "Finland",
        'FRA' => 'France',
	    "France Metropolitan" => "France Metropolitan",
	    "French Guiana" => "French Guiana",
	    "French Polynesia" => "French Polynesia",
	    "French Southern Territories" => "French Southern Territories",
	    "Gabon" => "Gabon",
	    "Gambia" => "Gambia",
	    "Georgia" => "Georgia",
        'DEU' => 'Germany',         
	    "Ghana" => "Ghana",
	    "Gibraltar" => "Gibraltar",
        'GRC' => 'Greece',
	    "Greenland" => "Greenland",
	    "Grenada" => "Grenada",
	    "Guadeloupe" => "Guadeloupe",
	    "Guam" => "Guam",
	    "Guatemala" => "Guatemala",
            "Guernsey" => "Guernsey",
	    "Guinea" => "Guinea",
	    "Guinea-Bissau" => "Guinea-Bissau",
	    "Guyana" => "Guyana",
	    "Haiti" => "Haiti",
	    "Honduras" => "Honduras",
	    "Hong Kong" => "Hong Kong",
        'HUN' => 'Hungary',         
	    "Iceland" => "Iceland",
	    "India" => "India",
	    "Indonesia" => "Indonesia",
	    "Iran (Islamic Republic Of)" => "Iran (Islamic Republic Of)",
	    "Iraq" => "Iraq",
        'IRL' => 'Ireland',
            "Isle of Man" => "Isle of Man",
            "Isle of Wight" => "Isle of Wight",
	    "Israel" => "Israel",        
        'ITA' => 'Italy',           
	    "Jamaica" => "Jamaica",
	    "Japan" => "Japan",
           "Jersey" => "Jersey",	
	    "Jordan" => "Jordan",
	    "Kazakhstan" => "Kazakhstan",
	    "Kenya" => "Kenya",
	    "Kiribati" => "Kiribati",
	    "Kuwait" => "Kuwait",
	    "Kyrgyzstan" => "Kyrgyzstan",
	    "Latvia" => "Latvia",
	    "Lebanon" => "Lebanon",
	    "Lesotho" => "Lesotho",
	    "Liberia" => "Liberia",
	    "Libyan Arab Jamahiriya" => "Libyan Arab Jamahiriya",
	    "Liechtenstein" => "Liechtenstein",
        'LTU' => 'Lithuania',       
	    "Luxembourg" => "Luxembourg",
	    "Macau" => "Macau",
	    "Macedonia" => "Macedonia",
	    "Madagascar" => "Madagascar",
	    "Malawi" => "Malawi",
	    "Malaysia" => "Malaysia",
	    "Maldives" => "Maldives",
	    "Mali" => "Mali",
	    "Malta" => "Malta",
	    "Marshall Islands" => "Marshall Islands",
	    "Martinique" => "Martinique",
	    "Mauritania" => "Mauritania",
	    "Mauritius" => "Mauritius",
	    "Mayotte" => "Mayotte",
	    "Mexico" => "Mexico",
	    "Micronesia" => "Micronesia",
	    "Moldova" => "Moldova",
	    "Monaco" => "Monaco",
	    "Mongolia" => "Mongolia",
	    "Montenegro" => "Montenegro",
	    "Montserrat" => "Montserrat",
	    "Morocco" => "Morocco",
	    "Mozambique" => "Mozambique",
	    "Myanmar" => "Myanmar",
	    "Namibia" => "Namibia",
	    "Nauru" => "Nauru",
	    "Nepal" => "Nepal",
        'NLD' => 'Netherlands',
	    "Netherlands Antilles" => "Netherlands Antilles",
	    "New Caledonia" => "New Caledonia",
        'NZL' => 'New Zealand',      
	    "Nicaragua" => "Nicaragua",
	    "Niger" => "Niger",
	    "Nigeria" => "Nigeria",
	    "Niue" => "Niue",
	    "Norfolk Island" => "Norfolk Island",
	    "North Korea" => "North Korea",
        'NOR' => 'Norway',          
        'GB-NIR' => 'Northern Ireland',
	    "Northern Mariana Islands" => "Northern Mariana Islands",
	    "Oman" => "Oman",
	    "Pakistan" => "Pakistan",
	    "Palau" => "Palau",
        "Palestinian Territory" => "Palestinian Territory",
	    "Panama" => "Panama",
	    "Papua New Guinea" => "Papua New Guinea",
	    "Paraguay" => "Paraguay",
	    "Peru" => "Peru",
	    "Philippines" => "Philippines",
	    "Pitcairn" => "Pitcairn",
        'POL' => 'Poland',          
        'PRT' => 'Portugal',        
	    "Puerto Rico" => "Puerto Rico",
	    "Qatar" => "Qatar",
	    "Reunion" => "Reunion",
	    "Romania" => "Romania",
	    "Russian Federation" => "Russian Federation",
	    "Rwanda" => "Rwanda",
	    "Saint Kitts And Nevis" => "Saint Kitts And Nevis",
	    "Saint Lucia" => "Saint Lucia",
            "Saint Vincent" => "Saint Vincent and the Grenadines",
	    "Samoa" => "Samoa",
	    "San Marino" => "San Marino",
	    "Sao Tome And Principe" => "Sao Tome And Principe",
	    "Saudi Arabia" => "Saudi Arabia",
        'GB-SCT' => 'Scotland',     
	    "Senegal" => "Senegal",
	    "Serbia" => "Serbia",
	    "Seychelles" => "Seychelles",
	    "Sierra Leone" => "Sierra Leone",
	    "Singapore" => "Singapore",
	    "Slovakia (Slovak Republic)" => "Slovakia (Slovak Republic)",
	    "Slovenia" => "Slovenia",
	    "Solomon Islands" => "Solomon Islands",
	    "Somalia" => "Somalia",
        'ZAF' => 'South Africa',    
	    "South Korea" => "South Korea",
        'ESP' => 'Spain',
	    "Sri Lanka" => "Sri Lanka",
	    "St. Helena" => "St. Helena",
	    "St. Pierre And Miquelon" => "St. Pierre And Miquelon",
	    "Sudan" => "Sudan",
	    "Suriname" => "Suriname",
	    "Swaziland" => "Swaziland",
        'SWE' => 'Sweden',
	    "Switzerland" => "Switzerland",
	    "Syrian Arab Republic" => "Syrian Arab Republic",
	    "Taiwan" => "Taiwan",
	    "Tajikistan" => "Tajikistan",
	    "Tanzania" => "Tanzania",
	    "Thailand" => "Thailand",
	    "Togo" => "Togo",
	    "Tokelau" => "Tokelau",
	    "Tonga" => "Tonga",
	    "Trinidad And Tobago" => "Trinidad And Tobago",
	    "Tunisia" => "Tunisia",
	    "Turkey" => "Turkey",
	    "Turkmenistan" => "Turkmenistan",
	    "Turks And Caicos Islands" => "Turks And Caicos Islands",
	    "Tuvalu" => "Tuvalu",
	    "Uganda" => "Uganda",
	    "Ukraine" => "Ukraine",
	    "United Arab Emirates" => "United Arab Emirates",
        'USA' => 'United States',
	    "Uruguay" => "Uruguay",
	    "Uzbekistan" => "Uzbekistan",
	    "Vanuatu" => "Vanuatu",
	    "Vatican City State" => "Vatican City State",
	    "Venezuela" => "Venezuela",
	    "Vietnam" => "Vietnam",
	    "Virgin Islands (British)" => "Virgin Islands (British)",
	    "Virgin Islands (U.S.)" => "Virgin Islands (U.S.)",
        'GB-WLS' => 'Wales',
	    "Wallis And Futuna Islands" => "Wallis And Futuna Islands",
	    "Western Sahara" => "Western Sahara",
	    "Yemen" => "Yemen",
	    "Yugoslavia" => "Yugoslavia",
	    "Zaire" => "Zaire",
	    "Zambia" => "Zambia",
	    "Zimbabwe" => "Zimbabwe");

    /**
     * these are countries that are going to be inside conutry tag
     * any other country has to be in OtherForeignCountry
     * countries - keys are iso 3 codes - value is name of the country 
     *
     * @var array
     */
    public static $nonForeignCountries = array(
        'USA' => 'United States',   'IRL' => 'Ireland',
        'DEU' => 'Germany',         'FRA' => 'France',
        'ITA' => 'Italy',           'ESP' => 'Spain',
        'PRT' => 'Portugal',        'NLD' => 'Netherlands',
        'POL' => 'Poland',          'BEL' => 'Belgium',
        'NOR' => 'Norway',          'SWE' => 'Sweden',
        'DNK' => 'Denmark',         'AUS' => 'Australia',
        'NZL' => 'New Zealand',      'CAN' => 'Canada',
        'ZAF' => 'South Africa',    'AUT' => 'Austria',
        'HRV' => 'Croatia',         'CYP' => 'Cyprus',
        'CZE' => 'Czech Republic',  'EST' => 'Estonia',
        'HUN' => 'Hungary',         'GRC' => 'Greece',
        'LTU' => 'Lithuania',       'GBR' => 'United Kingdom',
        'GB-ENG' => 'England',      'GB-WLS' => 'Wales',
        'GB-SCT' => 'Scotland',     'GB-NIR' => 'Northern Ireland'
    );

    public static $registeredOfficeCountries = array(
        //'GBR' => 'United Kingdom',  
        'GB-ENG' => 'England',      
        'GB-WLS' => 'Wales',        'GB-SCT' => 'Scotland',     
        'GB-NIR' => 'Northern Ireland'
    );

    /**
     * __construct 
     * 
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct($this->getDBFields());
    }

    /**
     * getDBFields 
     *
     * allowed fields - fields from database
     * true indicates that the field is compulsory
     * 
     * @access public
     * @return array
     */
    public function getDBFields()
    {
        return array(
            'premise'           => 1, 
            'street'            => 1, 
            'thoroughfare'      => 1,
            'post_town'         => 1,
            'county'            => 1, 
            'country'           => 1, 
            //'foreign_country'   => 1, 
            'postcode'          => 1, 
            'care_of_name'      => 1, 
            'po_box'            => 1,
            'secure_address_ind'=> 1
        );
    }

    /**
     * setPremise 
     * 
     * @param string $premise max 50 characters, not empty
     * @access public
     * @return void
     */
    public function setPremise($premise)
    {
        $premise = trim($premise);
        if (strlen($premise) > 50) {
            throw new Exception('Premise cannot be longer than 50 characters');
        }
        /* TODO - should be !== 0, but exception is not caught so it redirects to the list of companies */
        if (empty($premise) && $premise != 0) {
            throw new Exception('Premise cannot be empty');
        }
        $this->fields['premise'] = $premise;
    }

    /**
     * getPremise 
     * 
     * @access public
     * @return string
     */
    public function getPremise()
    {
        return $this->fields['premise'];
    }

    /**
     * setStreet 
     * 
     * @param string $street max 50 characters
     * @access public
     * @return void
     */
    public function setStreet($street)
    {
        $street = trim($street);
        if (strlen($street) > 50) {
            throw new Exception('Street cannot be longer than 50 characters');
        }
        if (empty($street)) { $street = null; }
        $this->fields['street'] = $street;
    }

    /**
     * getStreet 
     * 
     * @access public
     * @return string
     */
    public function getStreet()
    {
        return $this->fields['street'];
    }

    /**
     * setThoroughfare 
     * 
     * @param string $thoroughfare max 50 characters
     * @access public
     * @return void
     */
    public function setThoroughfare($thoroughfare)
    {
        $thoroughfare = trim($thoroughfare);
        if (strlen($thoroughfare) > 50) {
            throw new Exception('Thoroughfare cannot be longer than 50 characters');
        }
        if (empty($thoroughfare)) { $thoroughfare = null; }
        $this->fields['thoroughfare'] = $thoroughfare;
    }

    /**
     * getThoroughfare 
     * 
     * @access public
     * @return string
     */
    public function getThoroughfare()
    {
        return $this->fields['thoroughfare'];
    }

    /**
     * setPostTown 
     * 
     * @param string $postTown max 50 characters, not empty
     * @access public
     * @return void
     */
    public function setPostTown($postTown)
    {
        $postTown = trim($postTown);
        if (strlen($postTown) > 50) {
            throw new Exception('Post town cannot be longer than 50 characters');
        }
        if (empty($postTown) && $postTown != 0) {
            throw new Exception('Post town cannot be empty');
        }
        $this->fields['post_town'] = $postTown;
    }

    /**
     * getPostTown 
     * 
     * @access public
     * @return string
     */
    public function getPostTown()
    {
        return $this->fields['post_town'];
    }

    /**
     * setCounty 
     * 
     * @param string $county max 50 characters
     * @access public
     * @return void
     */
    public function setCounty($county)
    {
        $county = trim($county);
        if (strlen($county) > 50) {
            throw new Exception('County cannot be longer than 50 characters');
        }
        if (empty($county)) { $county = null; }
        $this->fields['county'] = $county;
    }

    /**
     * getCounty 
     * 
     * @access public
     * @return string
     */
    public function getCounty()
    {
        return $this->fields['county'];
    }

    /**
     * setCountry 
     * 
     * @param string $country key from $countries array
     * @access public
     * @return void
     */
    public function setCountry($country)
    {
        $country = trim($country);
        if (strlen($country) > 50) {
            throw new Exception('County cannot be longer than 50 characters');
        }

        /*
        $country = strtoupper($country);
        if (!isset(self::$countries[$country])) {
            throw new Exception('This country code is not allowed, use setForeignCountry instead');
        }
        if (isset($this->fields['foreign_country'])) {
            throw new Exception('You have already set foreign country, you cannot set both');
        }
        */
        if (empty($country) && $country != 0) {
            throw new Exception('Country cannot be empty');
        }
        $this->fields['country'] = $country;
    }
    
    /**
     * setRegisteredOfficeCountry
     *
     * @access public
     * @param string $country
     * @return void
     */
    public function setRegisteredOfficeCountry($country)
    {
        $country = strtoupper($country);
        if (!isset(self::$registeredOfficeCountries[$country])) {
            throw new Exception('This country code is not allowed');
        }
        if (empty($country) && $country != 0) {
            throw new Exception('Country cannot be empty');
        }
        $this->fields['country'] = $country;
    }

    /**
     * getCountry 
     * 
     * @access public
     * @return string
     */
    public function getCountry()
    {
        return $this->fields['country'];
    }

    /**
     * setForeignCountry 
     * 
     * @param string $county max 50 characters
     * @access public
     * @return void
     */
    public function setForeignCountry($country)
    {
        $country = trim($country);
        if (strlen($country) > 50) {
            throw new Exception('County town cannot be longer than 50 characters');
        }
        if (isset($this->fields['country'])) {
            throw new Exception('You have already set country, you cannot set both');
        }
        if (empty($country)) { $country = null; }
        $this->fields['foreign_county'] = $country;
    }

    /**
     * getForeignCountry 
     * 
     * @access public
     * @return string
     */
    public function getForeignCountry()
    {
        return $this->fields['foreign_country'];
    }

    /**
     * setPostcode 
     * 
     * @param string $postcode max 15 characters
     * @access public
     * @return void
     */
    public function setPostcode($postcode)
    {
        $postcode = trim($postcode);
        if (strlen($postcode) > 15) {
            throw new Exception('Postcode cannot be longer than 15 characters');
        }
        if (empty($postcode)) { $postcode = null; }
        $this->fields['postcode'] = $postcode;
    }

    /**
     * getPostcode 
     * 
     * @access public
     * @return string
     */
    public function getPostcode()
    {
        return $this->fields['postcode'];
    }

    /**
     * setCareOfName 
     * 
     * @param string $careOfName 
     * @access public
     * @return void
     */
    public function setCareOfName($careOfName)
    {
        $careOfName = trim($careOfName);
        if (strlen($careOfName) > 100) {
            throw new Exception('Care of name cannot be longer than 100 characters');
        }
        if (empty($careOfName)) { $careOfName = null; }
        $this->fields['care_of_name'] = $careOfName;
    }

    /**
     * getCareOfName 
     * 
     * @access public
     * @return string
     */
    public function getCareOfName()
    {
        return $this->fields['care_of_name'];
    }

    /**
     * setPoBox 
     * 
     * @param string $poBox 
     * @access public
     * @return void
     */
    public function setPoBox($poBox)
    {
        $poBox = trim($poBox);
        if (strlen($poBox) > 10) {
            throw new Exception('Po box cannot be longer than 10 characters');
        }
        if (empty($poBox)) { $poBox = null; }
        $this->fields['po_box'] = $poBox;
    }

    /**
     * getPoBox 
     * 
     * @access public
     * @return string
     */
    public function getPoBox()
    {
        return $this->fields['po_box'];
    }

    /**
     *
     * @param boolean $secure 
     */
    public function setSecureAddressInd($secure) {
        $this->fields['secure_address_ind'] = (boolean) $secure;
    }

    /**
     *
     * @return boolean
     */
    public function getSecureAddressInd() {
        return $this->fields['secure_address_ind'];
    }
    
        /**
     * Returns whole address
     *
     * @param string $newLine
     * @return string
     */
    public function getAddressAsString($newLine = "<br />")
    {
        $address = array();

        $premiseAndStreet = "";
        if ($this->getPremise())
            $premiseAndStreet = $this->getPremise() . " ";
        
        if ($this->getStreet())
            $premiseAndStreet .= $this->getStreet();
        
        $address[] = $premiseAndStreet;
            
        if ($this->getThoroughfare())
            $address[] = $this->getThoroughfare();
        if ($this->getPostTown())
            $address[] = $this->getPostTown();
        if ($this->getCounty())
            $address[] = $this->getCounty();
        if ($this->getPostCode())
            $address[] = $this->getPostCode();
        if ($this->getCountry())
            $address[] = $this->getCountry();

        $address = implode($newLine, $address);
        return $address;
    }
}
