<?php

use PeopleWithSignificantControl\Interfaces\IPersonPsc;

class PersonPsc extends PersonMember implements IPersonPsc
{
    /**
     * @var Address
     */
    private $residentialAddress;

    /**
     * @var NatureOfControl
     */
    private $natureOfControl;

    /**
     * @var string
     */
    private $pscStatementNotification;

    public function __construct()
    {
        parent::__construct();
        $this->residentialAddress = new Address;
        $this->natureOfControl = new NatureOfControl;
    }

    /**
     * @return array
     */
    public function getFields()
    {
        return array_merge(
            ['psc_statement_notification' => $this->pscStatementNotification],
            $this->residentialAddress->getFields('residential_'),
            $this->natureOfControl->getFields(),
            parent::getFields()
        );
    }

    /**
     * @param array $data
     */
    public function setFields(array $data)
    {
        parent::setFields($data);
        if (isset($data['psc_statement_notification'])) {
            $this->setPscStatementNotification($data['psc_statement_notification']);
        }
        $this->residentialAddress->setFields($data, 'residential_');
        $this->natureOfControl->setFields($data);
    }

    /**
     * @return Address
     */
    public function getResidentialAddress()
    {
        return clone $this->residentialAddress;
    }

    /**
     * @param Address $address
     */
    public function setResidentialAddress(Address $address)
    {
        $this->residentialAddress = clone $address;
    }

    /**
     * @return NatureOfControl
     */
    public function getNatureOfControl()
    {
        return $this->natureOfControl;
    }

    /**
     * @param NatureOfControl $natureOfControl
     */
    public function setNatureOfControl(NatureOfControl $natureOfControl)
    {
        $this->natureOfControl = $natureOfControl;
    }

    /**
     * @return string
     */
    public function getPscStatementNotification()
    {
        return $this->pscStatementNotification;
    }

    /**
     * @param string $pscStatementNotification
     */
    public function setPscStatementNotification($pscStatementNotification)
    {
        $this->pscStatementNotification = $pscStatementNotification;
    }
}
