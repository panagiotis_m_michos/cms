<?php

abstract class Member
{
    /**
     * @var Address
     */
    private $address;

    /**
     * @var Authentication
     */
    private $authentication;

    /**
     * @var bool
     */
    private $consentToAct = FALSE;

    /**
     * @var int
     */
    private $designated_ind;

    /**
     * @var int
     */
    private $corporate;

    /**
     * @var string
     */

    private $type;

    protected function __construct()
    {
        $this->address = new Address();
        $this->authentication = new Authentication();
    }

    /**
     * @param array $data
     */
    public function setFields(array $data)
    {
        $this->address->setFields($data);
        if (isset($data['authentication']) && !empty($data['authentication'])) {
            $this->authentication->setDBFields($data['authentication']);
        }
        if (isset($data['designated_ind'])) {
            $this->setDesignatedInd($data['designated_ind']);
        }
        if (isset($data['consentToAct'])) {
            $this->setConsentToAct($data['consentToAct']);
        }
    }

    /**
     * @return array
     */
    public function getFields()
    {
        return array_merge(
            ['consentToAct' => $this->consentToAct],
            $this->address->getFields(),
            $this->authentication->getFields()
        );
    }

    /**
     * Pre-condition: $address needs to be completed
     * Post-condition: exception is thrown if $address is not complete
     *
     * @param Address $address
     */
    public function setAddress(Address $address)
    {
        $this->address = clone $address;
    }

    /**
     * @return Address
     */
    public function getAddress()
    {
        return clone $this->address;
    }

    /**
     * @param Authentication $authentication
     */
    public function setAuthentication(Authentication $authentication)
    {
        $this->authentication = clone $authentication;
    }

    /**
     * @return Authentication
     */
    public function getAuthentication()
    {
        return clone $this->authentication;
    }

    /**
     * @param int $id
     */
    public function setDesignatedInd($id)
    {
        $this->designated_ind = $id;
    }

    /**
     * @return int
     */
    public function getDesignatedInd()
    {
        return $this->designated_ind;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $corporate
     */
    public function setCorporateType($corporate)
    {
        $this->corporate = $corporate;
    }

    /**
     * @return bool
     */
    public function isCorporate()
    {
        //todo: psc - legal entity = 2 (fix when psc are in different table)
        return $this->corporate == 1 || $this->corporate == 2;
    }

    /**
     * @return int
     */
    public function isLegalPerson()
    {
        return $this->corporate == 2;
    }

    /**
     * @return bool
     */
    public function hasConsentToAct()
    {
        return $this->consentToAct;
    }

    /**
     * @param bool $consentToAct
     */
    public function setConsentToAct($consentToAct)
    {
        $this->consentToAct = (bool) $consentToAct;
    }
}
