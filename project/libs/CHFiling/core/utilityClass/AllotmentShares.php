<?php

class AllotmentShares extends Element
{
    /**
     * __construct 
     * 
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct($this->getDBFields());
    }

    /**
     * getDBFields 
     *
     * allowed fields - fields from database
     * true indicates that the field is compulsory
     * 
     * @access public
     * @return array
     */
    public function getDBFields()
    {
        return array(
            'share_class'       => 1,
            'num_shares'        => 1,
            'amount_paid'       => 1,
            'amount_unpaid'     => 1,
            'currency'          => 1,
            'share_value'       => 1 
        );
    }

    /**
     * setShareClass 
     *
     * Pre-contition: $shareClass cannot be empty and 
     * can be maximum 50 characters long
     * 
     * @param string $shareClass 
     * @access public
     * @return void
     */
    public function setShareClass($shareClass)
    {
        $shareClass = trim($shareClass);
        if (strlen($shareClass) > 50) {
            throw new Exception('Share class cannot be longer than 50 characters');
        }
        if (empty($shareClass) && $shareClass != 0) {
            throw new Exception('Share class cannot be empty');
        }
        $this->fields['share_class'] = $shareClass;
    }

    /**
     * getShareClass 
     * 
     * @access public
     * @return string
     */
    public function getShareClass()
    {
        return $this->fields['share_class'];
    }

    /**
     * setNumShares 
     *
     * Pre-condition: $numShares has to be positive integer 
     * min 1 and mzx 11 digits
     * 
     * @param int $numShares 
     * @access public
     * @return void
     */
    public function setNumShares($numShares)
    {
        $numShares = trim($numShares);
        // --- number of shares ---
        if ($numShares <= 0) {
            throw new Exception('number of shares has to be more than 0');
        }
        if (!preg_match('/^\d{1,11}$/', $numShares)) {
            throw new Exception('The number of shares has to be a whole number between 1 and 11 digits.');
        }
        $this->fields['num_shares'] = $numShares;
    }

    /**
     * getNumShares 
     * 
     * @access public
     * @return int
     */
    public function getNumShares()
    {
        return $this->fields['num_shares'];
    }

    /**
     * setAmountPaid
     *
     * Pre-condition: $amountPerShare can be only positive integer or 
     * positive decimal with maximum 3 digits
     * 
     * @param int|decimal $amountPerShare 
     * @access public
     * @return void
     */
    public function setAmountPaid($amountPerShare)
    {
        $amountPerShare = trim($amountPerShare);
        // --- number of shares ---
        if ($amountPerShare <= 0) {
            throw new Exception('amount per share has to be more than 0');
        }
        if (!preg_match('/^\d+(.{1}(\d{1,6})){0,1}$/', $amountPerShare)) {
            throw new Exception('Amount per share can be int or decimal with maximum 6 fraction digits');
        }
        $this->fields['amount_paid'] = $amountPerShare;
    }

    /**
     * getAmountPaid
     * 
     * @access public
     * @return decimal
     */
    public function getAmountPaid()
    {
        return $this->fields['amount_paid'];
    }

    /**
     * setAmountUnpaid 
     *
     * Pre-condition: $amountPerShare can be only positive integer or 
     * positive decimal with maximum 3 digits
     * 
     * @param int|decimal $amountPerShare 
     * @access public
     * @return void
     */
    public function setAmountUnpaid($amountPerShare)
    {
        $amountPerShare = trim($amountPerShare);
        // --- number of shares ---
        if (!preg_match('/^\d+(.{1}(\d{1,6})){0,1}$/', $amountPerShare)) {
            throw new Exception('Amount per share can be int or decimal with maximum 6 fraction digits');
        }
        $this->fields['amount_unpaid'] = $amountPerShare;
    }

    /**
     * getAmountUnpaid 
     * 
     * @access public
     * @return int|decimal
     */
    public function getAmountUnpaid()
    {
        return $this->fields['amount_unpaid'];
    }

    /**
     * setCurrency 
     *
     * Pre-condition: $currency can be only ISO 4217 code
     * 
     * @param string $currency 
     * @access public
     * @return void
     */
    public function setCurrency($currency)
    {
        $currency = trim($currency);
        // --- currency ---
        $currency = strtoupper($currency);
        if (strlen($currency) > 3) {
            throw new Exception('Currency can be maximum 3 characters - use ISO 4217 codes');
        }
        $this->fields['currency'] = $currency;
    }

    /**
     * getShareCurrency 
     * 
     * @access public
     * @return string
     */
    public function getCurrency()
    {
        return $this->fields['currency'];
    }

    /**
     * setShareValue 
     *
     * Pre-condition: $shareValue can only be positive integer or 
     * positive decimal with max 3 fraction digits
     * 
     * @param decimal $shareValue int|decimal
     * @access public
     * @return void
     */
    public function setShareValue($shareValue)
    {
        /* trim spaces */
        $shareValue = trim($shareValue);

        /* trim 0 if it's decimal */
        if (preg_match("/\./", $shareValue)) {
            $shareValue = trim($shareValue, "0");
        } else {
            $shareValue = (int) $shareValue;
        }

        /* if it starts with . than prepend 0 */
        if (preg_match("/^\./", $shareValue)) {
            $shareValue = 0 . $shareValue;
        }

        /* if it finishes with . then remove it */
        if (preg_match("/\.$/", $shareValue)) {
            $shareValue = trim($shareValue, ".");
        }
        
        // --- share value ---
        if ($shareValue <= 0) {
            throw new Exception('share value has to be more than 0');
        }
        if (!preg_match('/^(\d+|\d+\.(\d){1,6})$/', $shareValue)) {
            throw new Exception('Share value has to be decimal max 6 fraction digits');
        }
        $this->fields['share_value'] = $shareValue;
    }

    /**
     * getShareValue 
     * 
     * @access public
     * @return float
     */
    public function getShareValue()
    {
        return $this->fields['share_value'];
    }

    /**
     * @return float
     */
    public function getAggregateValue()
    {
        return $this->getNumShares() * $this->getShareValue();
    }
}
