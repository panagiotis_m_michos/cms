<?php

class PersonSubscriber extends PersonMember
{
    /**
     * allotmentShares 
     * 
     * @var AllotmentShares
     * @access private
     */
    private $allotmentShares;

    /**
     * __construct 
     * 
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->allotmentShares = new AllotmentShares();
    }

    /**
     * setFields 
     * 
     * @param array $data 
     * @access public
     * @return void
     */
    public function setFields(array $data) {
        parent::setFields($data);
        $this->allotmentShares->setFields($data);
    }

    /**
     * setAllotmentShares 
     * 
     * @param AllotmentShares $allotmentShares 
     * @access public
     * @return void
     */
    public function setAllotmentShares(AllotmentShares $allotmentShares) {
        $this->allotmentShares = clone $allotmentShares;
    }

    /**
     * getAllotmentShares 
     * 
     * @access public
     * @return AllotmentShares
     */
    public function getAllotmentShares() {
        return clone $this->allotmentShares;
    }

    /**
     * getFields 
     * 
     * @access public
     * @return array
     */
    public function getFields() {
        return array_merge(
            $this->allotmentShares->getFields(),
            parent::getFields()
        );
    }
}
