<?php

/**
 * Authentication 
 * 
 * @final
 * @package chutil
 * @version $id$
 * @copyright 2009 Peter Reisinger
 * @author Peter Reisinger <p.reisinger@gmail.com> 
 * @license GNU General Public License
 */
final class Authentication
{
    /**
     * fields 
     *
     * holds authentication
     * 
     * @var array
     * @access private
     */
    private $fields = array();

    /**
     * getFields 
     *
     * return authentication as an array
     * 
     * @access public
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * getDbFields 
     *
     * return authentication as string - ready for database
     * 
     * @access public
     * @return string
     */
    public function getDbFields()
    {
        $dbAuth = array();
        foreach($this->fields as $key => $value) {
            $dbAuth[] = $key.'-'.$value;
        }
        return implode(';', $dbAuth);
    }

    /**
     * setDBFields 
     *
     * takes entry from database as an argument
     * 
     * @param string $authentication 
     * @access public
     * @return void
     */
    public function setDBFields($authentication)
    {
        // --- input string is: key-val;key-val;key-val ---
        $auth = array();

        // --- get authentication elements ---
        $authentication = explode(';', $authentication, 3);
        foreach($authentication as $value) {
            // --- get key value pairs ---
            $keyVal = explode('-', $value); 
            // --- set it ---
            $auth[$keyVal[0]] = $keyVal[1];
        }

        $this->setFields($auth);
    }

    /**
     * setFields 
     * 
     * array can contain only 3 unique elements
     * keys can be BIRTOWN, TEL, NATINS, PASSNO, MUM, DAD
     * value can contain only 3 characters
     * 
     * @param array $authentication key = attribute, value = data
     * @access public
     * @return void
     */
    public function setFields(array $authentication)
    {
        // --- allowed values ---
        $allowed = array(
            'BIRTOWN' => true, 'TEL' => true, 'NATINS' => true, 
            'PASSNO' => true, 'MUM' => true, 'DAD' => true, 'EYE' => true);

        //--- check if the authentication is allowed
        foreach($authentication as $key => $value) {
            $key = strtoupper($key);
            // --- check key ---
            if (!isset($allowed[$key])) {
                continue;
            }
            // --- check value ---
            if (strlen($value) != 3 || preg_match('/[-;]+/', $value)) {
                throw new Exception("$value can be only 3 characters long and cannot contain ; or -");
            }
            // key is personal attribute so we make sure that they are unique
            $this->fields[$key] = $value;
        }

        // --- check number of auhtentications
        if (count($this->fields) != 3) {
            throw new Exception('Authentication has to contain 3 unique elements');
        }
    }
}
