<?php

abstract class Element
{
    /**
     * fields 
     *
     * array of fields and their values
     * 
     * @var array
     * @access protected
     */
    protected $fields   = array();

    /**
     * __construct 
     * 
     * @access protected
     * @return void
     */
    protected function __construct(array $fields)
    {
        // --- set null value for each field ---
        foreach($fields as $key => $value) {
            $this->fields[$key] = null;
        }
    }

    /**
     * isEmpty 
     *
     * checks if value is set, if is empty - if value is 0 or '0' 
     * then it's not empty
     * 
     * @param mixed $value 
     * @access protected
     * @return boolean
     */
    protected function isEmpty($value)
    {
        if (isset($value) && (!empty($value) || $value == 0)) {
            return false;
        } 
        return true;
    }

    /**
     * setDBField 
     *
     * Calls appropriate method for database field if exists,
     * so if database field is share_class, setShareClass is called
     *
     * Pre-condition: field has to exist, otherwise exception is thrown
     * 
     * @param string $field 
     * @param mixed $value 
     * @access protected
     * @return void
     */
    protected function setDBField($field, $value)
    {
        // --- separate field ---
        $words = explode('_', $field);

        // --- create method from field ---
        $method = 'set';
        foreach ($words as $word) {
            $method .= ucfirst($word); 
        }

        // --- call appropriate method if exists ---
        if (method_exists($this, $method)) {
            $this->$method($value);
        } else {
            echo $method;
            throw new Exception('Specified field doesn\'t exist');
        }
    }

    /**
     * setFields 
     *
     * key is column in db, value is value
     * prefix is column prefix - so for service street
     * you can use 'service_' = 'service_street'
     * 
     * @param array $fields 
     * @param string $prefix
     * @access public
     * @return void
     */
    public function setFields(array $fields, $prefix = null)
    {
        // --- get allowed fields ---
        $allowedFields = $this->getDBFields();

        foreach ($fields as $key => $value) {

            // --- check if there's a prefix, if there isn't then continue ---
            if (!is_null($prefix)) {
                if (!preg_match("/^$prefix/", $key)) {
                    continue;
                }
            }

            $key = (is_null($prefix)) ? $key : preg_replace('/^'.$prefix.'/', '', $key , 1) ;
            // --- check if field is not empty ---
            if (!$this->isEmpty($value)) { 
                // --- check if the field is allowed ---
                if (isset($allowedFields[$key])) {
                    // --- call appropriate method ---
                    $this->setDBField($key, $value); 
                }
            } 
        }
    }

    /**
     * getFields 
     * 
     * @param string $prefix prepend prefix
     * @access public
     * @return array
     */
    public function getFields($prefix = null)
    {
        if (is_null($prefix)) {
            return $this->fields;
        } else {
            // --- add prefix that was used to set fields ---
            $fields = array();
            foreach ($this->fields as $key => $value) {
                $fields[$prefix . $key] = $value;
            }
            return $fields;
        }
    }

    /**
     * getDBFields 
     *
     * array of allowed fields - same as database fields
     * 
     * @abstract
     * @access public
     * @return array
     */
    abstract function getDBFields();
}
