<?php

class Transfer extends Element {

    /**
     * __construct
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct($this->getDBFields());
    }

    /**
     * getDBFields
     *
     * allowed fields - fields from database
     * true indicates that the field is compulsory
     *
     * @access public
     * @return array
     */
    public function getDBFields()
    {
        return array(
            'date_of_transfer'  => 1,
            'shares_transfered' => 1
        );
    }

    // TODO - setter and getter for date of transfer and shares transfered

    /**
     * setDateOfTransfer
     *
     * Pre-condition: $date has to be in date format YYYY-MM-DD
     * and has to be less or equal to today's date
     *
     * @param string $date
     * @access public
     * @return void
     */
    public function setDateOfTransfer($date)
    {
        // --- date ---
        // check format
        if (!preg_match('/^[\d]{4}-[\d]{1,2}-[\d]{1,2}$/', $date)) {
            throw new Exception('Date has to be in YYYY-MM-DD format');
        }

        $d      = explode('-', $date);
        $month  = $d[1];
        $day    = $d[2];
        $year   = $d[0];

        // check if it's correct
        if (!checkdate($month, $day, $year)) {
            throw new Exception('Date has to be correct date.');
        }

        // check if date is less than today's day
        if (strtotime($date) < time()) {
            throw new Exception('Date is more than today\'s date');
        }
        $this->fields['date_of_transfer'] = $date;
    }

    /**
     * getDateOfTransfer
     *
     * @access public
     * @return string - date format YYYY-MM-DD
     */
    public function getDateOfTransfer() {
        return $this->fields['date_of_transfer'];
    }

    /**
     *
     * @param int $shares
     */
    public function setSharesTransfered($shares) {

        $shares = trim($shares);
        if (!preg_match('/^\d$/', $shares)) {
            throw new Exception('Shares has to be integer');
        }

        $this->fields['shares_transfered'] = $shares;
    }

    /**
     *
     * @return int
     */
    public function getSharesTransfered() {
        return $this->fields['shares_transfered'];
    }
}
