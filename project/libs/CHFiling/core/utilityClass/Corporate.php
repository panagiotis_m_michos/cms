<?php

final class Corporate extends Element
{
    /**
     * __construct 
     * 
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct($this->getDBFields());
    }

    /**
     * getDBFields 
     *
     * allowed fields - fields from database
     * true indicates that the field is compulsory
     * 
     * @access public
     * @return array
     */
    public function getDBFields()
    {
        return array(
            'forename'      => 1, 
            'surname'       => 1,
            'corporate_name'=> 1//,
            //'appointment_date'=> 1
        );
    }

    /**
     * setForename 
     *
     * Pre-condition: $forename cannot be empty anc longer than 50 characters
     * 
     * @param string $forename 
     * @access public
     * @return void
     */
    public function setForename($forename)
    {
        $forename = trim($forename);
        if (strlen($forename) > 50) {
            throw new Exception('Forename cannot be longer than 50 characters');
        }
        if (empty($forename) && $forename != 0) {
            throw new Exception('Forename cannot be empty');
        }
        $this->fields['forename'] = $forename;
    }

    /**
     * getForename 
     * 
     * @access public
     * @return string
     */
    public function getForename()
    {
        return $this->fields['forename'];
    }

    /**
     * setSurname 
     *
     * Pre-condition: $surname cannot be empty and cannot exceed 160 characters
     * 
     * @param mixed $surname 
     * @access public
     * @return void
     */
    public function setSurname($surname)
    {
        $surname = trim($surname);
        if (strlen($surname) > 160) {
            throw new Exception('Surname cannot be longer than 50 characters');
        }
        if (empty($surname) && $surname != 0) {
            throw new Exception('Surname cannot be empty');
        }
        $this->fields['surname'] = $surname;
    }

    /**
     * getSurname 
     * 
     * @access public
     * @return string
     */
    public function getSurname()
    {
        return $this->fields['surname'];
    }

    /**
     * setCorporateName 
     *
     * Pre-condition: $corporateName cannot be empty and cannot be longer
     * than 160 characters
     * 
     * @param string $corporateName 
     * @access public
     * @return void
     */
    public function setCorporateName($corporateName)
    {
        $corporateName = trim($corporateName);
        if (strlen($corporateName) > 160) {
            throw new Exception('Corporate name cannot be longer than 160 characters');
        }
        if (empty($corporateName) && $corporateName != 0) {
            throw new Exception('Corporate name cannot be empty');
        }
        $this->fields['corporate_name'] = $corporateName;
    }

    /**
     * getCorporateName 
     * 
     * @access public
     * @return string
     */
    public function getCorporateName()
    {
        return $this->fields['corporate_name'];
    }
    
    /**
     * setAppointmentDate
     *
     * @access public
     * @return string
     */
    public function setAppointmentDate($appointmentDate)
    {
        $this->fields['appointment_date'] = $appointmentDate;
    }

     /**
     * getAppointmentDate
     *
     * @access public
     * @return string
     */
    public function getAppointmentDate()
    {
        return $this->fields['appointment_date'];
    }
}
