<?php

class PersonDirector extends PersonMember
{
    /**
     * residentialAddress 
     * 
     * @var Address
     * @access private
     */
    private $residentialAddress;

    /**
     * __construct 
     * 
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->residentialAddress = new Address();
    }

    /**
     * setFields 
     * 
     * @param array $data 
     * @access protected
     * @return void
     */
    public function setFields(array $data)
    {
        parent::setFields($data);
        $this->residentialAddress->setFields($data, 'residential_');
    }

    /**
     * setResidentialAddress 
     * 
     * @param Address $address 
     * @access public
     * @return void
     */
    public function setResidentialAddress(Address $address)
    {
        $this->residentialAddress = clone $address;
    }

    /**
     * getResidentialAddress 
     * 
     * @access public
     * @return Address
     */
    public function getResidentialAddress()
    {
        return clone $this->residentialAddress;
    }

    /**
     * getFields 
     * 
     * @access public
     * @return array
     */
    public function getFields() {
        return array_merge(
            $this->residentialAddress->getFields('residential_'), 
            parent::getFields()
        );
    }
}
