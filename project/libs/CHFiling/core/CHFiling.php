<?php

// --- this is for Zend Db ---
set_include_path(dirname(__FILE__));

require_once dirname(__FILE__) . '/Zend/Db/Adapter/Pdo/Mysql.php';

use CHFiling\Exceptions\EmptyResponseException;
use CompanyHouse\Filling\Responses\ResponseFactory;
use Entities\CompanyHouse\FormSubmission as FormSubmissionEntity;
use Services\SubmissionService;
use Utils\XmlElement;

final class CHFiling
{

    private static $packageReference;
    private static $senderId;
    private static $password;
    private static $emailAddress;
    private static $gatewayTest;

    public static function setPackageReference($packageReference)
    {
        self::$packageReference = $packageReference;
    }

    public static function setSenderId($senderId)
    {
        self::$senderId = $senderId;
    }

    public static function setPassword($password)
    {
        self::$password = $password;
    }

    public static function setEmailAddress($email)
    {
        self::$emailAddress = $email;
    }

    public static function setGatewayTest($test)
    {
        self::$gatewayTest = ($test) ? 1 : 0;
    }

    /**
     * config array for database
     * at index:
     * 0 - host
     * 1 - user name
     * 2 - password
     * 3 - database
     *
     * @var array
     */
    private static $dbConfig = [];

    /**
     *
     * @var ZendDb
     */
    private static $db;

    /**
     * Main folder for documents.
     *
     * @var string
     */
    private static $docPath;

    /**
     *
     * @return string
     */
    public static function getPackageReference()
    {
        return self::$packageReference;
    }

    public static function getSenderId()
    {
        return self::$senderId;
    }

    /**
     *
     * @return string
     */
    public static function getPassword()
    {
        return self::$password;
    }

    /**
     *
     * @return string
     */
    public static function getEmailAddress()
    {
        return self::$emailAddress;
    }

    /**
     *
     * @return boolean
     */
    public static function getGatewayTest()
    {
        return self::$gatewayTest;
    }

    /**
     * config array for database
     * at index:
     * 0 - host
     * 1 - user name
     * 2 - password
     * 3 - database
     *
     * @param array $config
     */
    public static function setDb(array $config)
    {
        // --- database already set ---
        if (isset(self::$db)) {
            return;
        }

        // --- set database ---
        self::$db = new CHAdapter(
            [
                'host' => $config[0],
                'username' => $config[1],
                'password' => $config[2],
                'dbname' => $config[3]
            ]
        );
    }

    /**
     * @return ZendDb
     */
    public static function getDb()
    {
        if (!isset(self::$db)) {
            throw new Exception('Set databse using CHFiling::setDb');
        }
        return self::$db;
    }

    /**
     * root folder for all documents
     *
     * @param string $docPath
     */
    public static function setDocPath($docPath)
    {
        self::$docPath = $docPath;
    }

    /**
     *
     * @return string
     */
    public static function getDocPath()
    {
        if (!isset(self::$docPath)) {
            throw new Exception('Set document path using CHFiling::setDocPath');
        }
        return self::$docPath;
    }

    /**
     *
     * @param int $id
     * @return string
     */
    public static function getDirName($id)
    {
        $dir = floor($id / 10000) + 1;
        $dir = (string) $dir . '0000';
        return $dir;
    }

    /**
     * retrieves specified submission's status and updates form submission
     * array:
     * 0 - customerId
     * 1 - companyId
     * 2 - companyName
     * 3 - status [accept|reject|error]
     * 4 - attachementPath
     *
     * @param int $formSubmissionId
     * @return array
     * @throws Exception
     */
    public static function getFormSubmissionStatus($formSubmissionId)
    {
        $submissionStatus = GetSubmissionStatus::getNewGetSubmissionStatus(self::$senderId);
        $submissionStatus->setSubmissionNumber($formSubmissionId);

        $response = $submissionStatus->sendRequest($formSubmissionId);

        if (empty($response)) {
            throw new EmptyResponseException;
        }

        $xmlResponse = new XmlElement($response);

        if (isset($xmlResponse->GovTalkDetails->GovTalkErrors->Error)) {
            return FALSE;
        }
        /** @var ResponseFactory $companyHouseFactory */
        $companyHouseFactory = Registry::$container->get(DiLocator::FACTORY_COMPANY_HOUSE_RESPONSE);

        /** @var SubmissionService $formSubmissionService */
        $formSubmissionService = Registry::$container->get(DiLocator::SERVICE_SUBMISSION);

        if (isset($xmlResponse->Body->SubmissionStatus->Status)) {
            $status = $xmlResponse->Body->SubmissionStatus->Status;
            $submissionStatus = $companyHouseFactory->createSubmissionStatus($status);
            $returnArray = $formSubmissionService->processSubmissionStatus($submissionStatus);
        }

        unset($reponse);

        return isset($returnArray) ? $returnArray : FALSE;
    }

    /**
     * returns array of companies
     *
     * @param int $limit
     * @return array
     */
    public static function getNonPrintedCompanies($limit = 10)
    {
        // --- check that limit is integer ---
        if (!preg_match('/^\d+$/', $limit)) {
            throw new Exception('limit has to be integer');
        }

        // --- select from db ---
        $result = self::getDb()->fetchAssoc(
            'SELECT ch_company.company_id, ch_company.company_name, ch_company.company_number, ch_company.incorporation_date,
            cms2_customers.firstName, cms2_customers.lastName
            FROM ch_company LEFT JOIN cms2_customers ON (ch_company.customer_id = cms2_customers.customerId)
            WHERE ch_company.company_number IS NOT NULL
            AND ch_company.is_certificate_printed = 0 AND locked = 0
            ORDER BY ch_company.incorporation_date ASC
            LIMIT 0, ' . $limit
        );

        // --- crate output ---
        $companies = [];
        foreach ($result as $company) {
            $companies[$company['company_id']] = $company;
        }
        return $companies;
    }

    public static function getNonPrintedBronzeCompanyCoverLetters($limit = 10)
    {
        // --- check that limit is integer ---
        if (!preg_match('/^\d+$/', $limit)) {
            throw new Exception('limit has to be integer');
        }

        // --- select from db ---
        $result = self::getDb()->fetchAssoc(
            'SELECT ch_company.company_id, ch_company.company_name, ch_company.company_number, ch_company.incorporation_date,
            cms2_customers.firstName, cms2_customers.lastName
            FROM ch_company LEFT JOIN cms2_customers ON (ch_company.customer_id = cms2_customers.customerId)
            WHERE ch_company.company_number IS NOT NULL
            AND ch_company.is_bronze_cover_letter_printed = 0
            ORDER BY ch_company.incorporation_date ASC
            LIMIT 0, ' . $limit
        );

        // --- crate output ---
        $companies = [];
        foreach ($result as $company) {
            $companies[$company['company_id']] = $company;
        }
        return $companies;
    }

    public static function getNonPrintedMA($limit = 10, $article = NULL)
    {
        // --- check that limit is integer ---
        if (!preg_match('/^\d+$/', $limit)) {
            throw new Exception('limit has to be integer');
        }
        $article ? $articleSQL = 'AND d.custom = 1 ' : $articleSQL = 'AND d.custom IS NULL ';

        // --- select from db ---
        $result = self::getDb()->fetchAssoc(
            'SELECT ch_company.company_id, ch_company.company_name, ch_company.company_number, ch_company.incorporation_date,
            cms2_customers.firstName, cms2_customers.lastName, d.filename
            FROM ch_company 
            LEFT JOIN cms2_customers ON (ch_company.customer_id = cms2_customers.customerId)
            LEFT JOIN ch_form_submission as f ON (ch_company.company_id = f.company_id)
            LEFT JOIN ch_document as d ON (f.form_submission_id = d.form_submission_id)
            
            WHERE ch_company.company_number IS NOT NULL
            AND ch_company.is_ma_printed = 0 ' . $articleSQL .
            'AND ch_company.company_category != "LLP"
            AND f.date_cancelled IS NULL
            GROUP BY ch_company.company_number
            ORDER BY ch_company.incorporation_date ASC
            LIMIT 0, ' . $limit
        );

        // --- crate output ---
        $companies = [];
        foreach ($result as $company) {

            $company['hasMA'] = (bool) Company::getCompany($company['company_id'])->hasMA();
            $company['hasMACoverLetter'] = (bool) Company::getCompany($company['company_id'])->hasMACoverLetter();
            $companies[$company['company_id']] = $company;
        }
        return $companies;
    }

    public static function getNonPrintedCertificates($limit = 10, $roleId = NULL, $tagId = NULL, $tab = NULL)
    {
        // --- check that limit is integer ---
        if (!preg_match('/^\d+$/', $limit)) {
            throw new Exception('limit has to be integer');
        }

        // --- select from db ---
        $result = self::getDb()->fetchAssoc(
            'SELECT ch_company.company_id, ch_company.company_name, ch_company.company_number, ch_company.incorporation_date,
            cms2_customers.firstName, cms2_customers.lastName, cms2_customers.roleId, cms2_customers.tagId
            FROM ch_company LEFT JOIN cms2_customers ON (ch_company.customer_id = cms2_customers.customerId)
            WHERE ch_company.company_number IS NOT NULL AND locked = 0
            AND ch_company.is_certificate_printed = 0
            ORDER BY ch_company.incorporation_date ASC
            LIMIT 0, ' . $limit
        );
        // --- crate output ---
        $companies = [];
        foreach ($result as $company) {
            //$company['CompanyCustomer'] = CompanyCustomer::getCompanyCustomerByCompanyId($company['company_id']);
            $company['hasCertificate'] = (bool) Company::getCompany($company['company_id'])->hasCertificate();
            $company['hasCoverLetter'] = (bool) Company::getCompany($company['company_id'])->hasCoverLetter();
            // roleid = Normal, wholesale
            // tagId = 'RETAIL','COSEC','AFFILIATE','ICAEW','ICAEWWHOLESALE'
            if ($roleId == $company['roleId'] || $roleId == NULL) {

                if ($tagId == $company['tagId'] || $tagId == NULL) {
                    //wholesale without affiliate
                    if ($tab == 'wholesale' && ($company['tagId'] == 'AFFILIATE' || self::checkProductbyCompany($company['company_id'], Package::PACKAGE_BRONZE))) {
                        //do nothing
                    } elseif ($tab == 'retail' && self::checkProductbyCompany($company['company_id'], Package::PACKAGE_BRONZE)) {
                        //do nothing
                    } else {
                        //bronze
                        //elseif($tab == 'bronze' &&  !self::checkProductbyCompany($company['company_id'], Package::PACKAGE_BRONZE)){
                        //do nothing
                        //}
                        $companies[$company['company_id']] = $company;
                    }
                }
            }
        }
        return $companies;
    }

    public static function checkProductbyCompany($companyId, $productId)
    {
        $comapany = Company::getCompany($companyId);
        $orderId = $comapany->getOrderId();
        foreach (OrderItem::getOrderItems($orderId) as $key => $item) {
            if ($item->productId == $productId) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public static function checkRemovedBarclaysByCompany($companyId, $productId)
    {
        $company = Company::getCompany($companyId);
        $customer = new Customer($company->getCustomerId());
        $orderId = $company->getOrderId();
        $i = 0;
        foreach (OrderItem::getOrderItems($orderId) as $key => $item) {
            if ($item->productId == $productId) {
                $i = 1;
            }
        }
        foreach (OrderItem::getOrderItems($orderId) as $key => $item) {
            if ($i != 1 && !$customer->isWholesale() && ($item->productId == 109 || $item->productId == 110 || $item->productId == 111 || $item->productId == 112 || $item->productId == 113 || $item->productId == 279)) {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * merge certificates for supplied company ids and prints them
     *
     * @param array $companyIds
     */
    public static function getPdfCertificates(array $companyIds)
    {
        require_once dirname(__FILE__) . '/request/document/memorandum/ConcatPdf.php';
        // --- all documets ---
        $filename = 'IncorporationCertificate.pdf';
        $pdfs = [];
        foreach ($companyIds as $id) {
            $path = self::$docPath . '/' . self::getDirName($id) . '/company-' . $id . '/documents/' . $filename;
            if (filesize($path) == 0) {
                throw new Exception('File is empty: ' . $path);
            }
            $pdfs[] = $path;
        }

        $pdf = new ConcatPdf();
        $pdf->setFiles($pdfs);
        $pdf->concat();

        // --- send to the browser ---
        $pdf->Output($filename, 'D');
        exit;
    }

    /**
     * merge certificates for supplied company ids and prints them
     *
     * @param array $companyIds
     */
    public static function getPdfMemorandumArticles(array $companyIds)
    {
        require_once dirname(__FILE__) . '/request/document/memorandum/ConcatPdf.php';
        // --- all documets ---
        $filename = 'article.pdf';
        $pdfs = [];
        foreach ($companyIds as $id) {
            $path = self::$docPath . '/' . self::getDirName($id) . '/company-' . $id . '/documents/' . $filename;
            if (filesize($path) == 0) {
                throw new Exception('File is empty: ' . $path);
            }
            $pdfs[] = $path;
        }

        $pdf = new ConcatPdf();
        $pdf->setFiles($pdfs);
        $pdf->concat();

        // --- send to the browser ---
        $pdf->Output($filename, 'D');
        exit;
    }

    public static function getPdfAnnualreturnSummary($return)
    {
        $anualReturn = new AnnualReturnSummaryPdf();
        $anualReturn->prepareData($return);
        $anualReturn->createPdf();
        $anualReturn->printPdf();
    }

    /**
     * return data about company from companies house
     *
     * @param int $customerId
     * @param string $companyNumber
     * @param string $authenticationCode
     * @return xml
     */
    public static function getCompanyData($customerId, $companyNumber, $authenticationCode)
    {
        // --- create new company data request ---
        require_once dirname(__FILE__) . '/request/CompanyDataRequest.php';
        $companyData = CompanyDataRequest::getNewCompanyDataRequest($customerId, $companyNumber, $authenticationCode);

        // --- send and return request ---
        return $companyData->sendRequest();
    }

    /**
     * Returns all user submission with status NULL or error
     *
     * @param int $userId
     */
    public static function getIncompleteFormSubmissions($userId)
    {

        // --- get data from db ---
        $sql = "
	   		SELECT c.*, f.form_submission_id, f.form_identifier,
            IF((f.response IS NULL), 'INCOMPLETE', f.response) response
            FROM `ch_company` as c
            LEFT JOIN ch_form_submission as f ON (c.company_id = f.company_id)
            WHERE c.customer_id = $userId
            AND (f.response IS NULL OR f.response = 'ERROR')
            AND (f.date_cancelled IS NULL)
            AND (f.form_identifier = 'CompanyIncorporation' OR f.form_identifier = 'AnnualReturn'  OR f.form_identifier = 'ChangeOfName')";
        $result = CHFiling::getDb()->fetchAll($sql);
        return $result;
    }

    /**
     * sets new company, plus creates new form submission - company incorporation
     * use for company incorporation packages
     *
     * @param int $customerId
     * @param string Company
     * @return Company
     */
    public function setCompany($customerId, $companyName)
    {
        $company = Company::getNewCompany($customerId, $companyName);
        FormSubmission::getNewFormSubmission($company, 'CompanyIncorporation');
        return $company;
    }

    /**
     *
     * @param int $customerId
     * @return array<Company>
     */
    public function getCustomerCompanies($customerId)
    {
        $all = dibi::select('c.*')
                ->from('ch_company', 'c')
                ->where('c.customer_id=%i', $customerId)
                ->orderBy('c.company_id')->desc()
                ->execute()->fetchAssoc('company_id');
        return $all;
    }

    /**
     *
     * @return array<Company>
     */
    public function getCompanies($where = [], $limit = NULL, $offset = NULL, $order = 'company_id DESC')
    {

        // --- get data from db ---
        $sql = "
	   		SELECT `company_id` FROM `ch_company`
	   		WHERE 1";

        // where
        if (!empty($where)) {
            foreach ($where as $key => $val) {
                //$val = '%'.self::getDb()->quote($val).'%';
                $val = self::getDb()->quote('%' . $val . '%');
                $sql .= ' AND `' . $key . '` LIKE ' . $val;
            }
        }

        //exclude deleted companies
        //Added by Razvan P 06/06/2011
        $sql .= ' AND `deleted`=0';

        // order
        $sql .= ' ORDER BY ' . $order;

        // limit
        if ($limit !== NULL) {
            $sql .= ' LIMIT ' . $offset;
        }

        // offset
        if ($offset !== NULL) {
            $sql .= ', ' . $limit;
        }

        $ids = CHFiling::getDb()->fetchCol($sql);

        // --- load companies into array ---
        $companies = [];
        foreach ($ids as $id) {
            $companies[$id] = Company::getCompany($id);
        }

        return $companies;
    }

    /**
     *
     * @return array<Company>
     */
    public function getCompaniesCount($where = [])
    {
        // --- get data from db ---
        $sql = "
	   		SELECT COUNT(*) FROM `ch_company`
	   		WHERE 1";
        // where
        if (!empty($where)) {
            foreach ($where as $key => $val) {
                $val = self::getDb()->quote('%' . $val . '%');
                $sql .= ' AND `' . $key . '` LIKE ' . $val;
            }
        }

        //exclude deleted companies from search
        //Added by Razvan P 06/06/2011
        $sql .= ' AND `deleted`=0';

        $count = CHFiling::getDb()->fetchOne($sql);
        return $count;
    }

    /**
     *
     * @param int $customerId
     * @return array<Company>
     */
    public function getCustomerIncorporatedCompanies($customerId)
    {
        $all = dibi::select('c.company_id, c.company_name, c.company_number, c.hidden, c.deleted, c.accounts_next_due_date, c.company_status')
            ->select('DATE_FORMAT(c.incorporation_date, "%d-%m-%Y")')->as('incorporation_date')
            ->select('MAX(IF(fs.response, fs.response, %s))', 'INCOMPLETE')->as('status')
            ->from('ch_company', 'c')
            ->leftJoin('ch_form_submission', 'fs')
            ->on('c.company_id=fs.company_id')
            ->leftJoin('ch_envelope', 'e')
            ->on('e.form_submission_id=fs.form_submission_id')
            ->where('c.customer_id=%i', $customerId)
            ->and('c.company_number IS NOT NULL')->and('c.deleted != 1')
            ->and('fs.date_cancelled IS NULL')
            ->groupBy('c.company_id')->orderBy('c.company_name')
            ->execute()->fetchAssoc('company_id');
        return $all;
    }

    /**
     *
     * @param int $customerId
     * @return array<Company>
     */
    public function getCustomerIncorporatedCompaniesGrid(array $where = [], $limit = NULL, $offset = NULL, $order = NULL)
    {

        $all = dibi::select('c.company_id, c.company_name, c.company_number, c.hidden, c.deleted, c.returns_next_due_date, c.accounts_next_due_date, c.company_status')
            ->select('DATE_FORMAT(c.incorporation_date, "%d-%m-%Y")')->as('incorporation_date')
            ->select('MAX(IF(fs.response, fs.response, %s))', 'INCOMPLETE')->as('status')
            ->from('ch_company', 'c')
            ->leftJoin('ch_form_submission', 'fs')
            ->on('c.company_id=fs.company_id')
            ->leftJoin('ch_envelope', 'e')
            ->on('e.form_submission_id=fs.form_submission_id');

        foreach ($where as $key => $val) {
            $all->where($key, $val);
        }
        $all->and('c.company_number IS NOT NULL')
            ->and('c.deleted != 1')
            ->and('fs.date_cancelled IS NULL')
            ->groupBy('c.company_id');

        if ($order !== NULL) {
            $all->orderBy($order);
        }
        if ($limit !== NULL) {
            $all->limit($limit);
        }
        if ($offset !== NULL) {
            $all->offset($offset);
        }

        $result = $all->execute()->fetchAssoc('company_id');
        return $result;
    }

    /**
     *
     * @param int $customerId
     * @return array
     */
    public function getCustomerNotIncorporatedCompanies($customerId)
    {
        $all = dibi::select('c.company_id, c.company_name, c.hidden')
            ->select('MAX(fs.form_submission_id)')->as('last_form_submission_id')
            ->select('DATE_FORMAT(MAX(e.date_sent), "%d-%m-%Y")')->as('last_submission_date')
            ->from('ch_company', 'c')
            ->leftJoin('ch_form_submission', 'fs')->on('c.company_id = fs.company_id')
            ->leftJoin('ch_envelope', 'e')->on('e.form_submission_id = fs.form_submission_id')
            ->where('c.customer_id = %i', $customerId)
            ->and('c.company_number IS NULL')
            ->and('fs.date_cancelled IS NULL')
            ->groupBy('c.company_id')
            ->orderBy('c.company_name')
            ->execute()
            ->fetchAssoc('company_id');

        foreach ($all as $companyId => $company) {
            $submission = dibi::select('response')
                ->from('ch_form_submission')
                ->where('company_id = %i', $company['company_id'])
                ->and("form_identifier LIKE 'CompanyIncorporation'")
                ->and('date_cancelled IS NULL')
                ->orderBy('form_submission_id DESC')
                ->execute()
                ->fetchAll();

            $lastSubmission = isset($submission[0]['response']) ? $submission[0]['response'] : NULL;
            if (empty($lastSubmission)) {
                $submission['response'] = 'INCOMPLETE';
                if (isset($submission[1]['response']) && $submission[1]['response'] == FormSubmissionEntity::RESPONSE_REJECT) {
                    $submission['response'] = FormSubmissionEntity::RESPONSE_REJECT;
                }
            } else {
                $submission['response'] = $lastSubmission;
            }
            $all[$companyId]['submission_status'] = $submission['response'];
        }
        return $all;
    }

    /**
     *
     * @param int $companyId
     * @param int $customerId
     * @return Company
     */
    public function getCustomerIncorporatedCompany($companyId, $customerId)
    {
        $company = Company::getCustomerCompany($companyId, $customerId);

        if ($company->getStatus() != 'complete') {
            throw new Exception('Company does not exist');
        }
        return $company;
    }

    /**
     *
     * @param int $companyId
     * @param int $customerId
     * @return array<Company>
     */
    /*
    public function getCustomerNotIncorporatedCompany($customerId)
    {
        $company = Company::getCustomerCompany($companyId, $customerId);

        if ($company->getStatus() != 'complete') {
            return $company;
        }
    }
    */

    /**
     *
     * @param int $companyId
     * @param int $customerId
     * @return Company
     */
    public function getCustomerCompany($companyId, $customerId)
    {
        return Company::getCustomerCompany($companyId, $customerId);
    }

    /**
     *
     * @param int $companyId
     * @return Company
     */
    public function getCompany($companyId)
    {
        return Company::getCompany($companyId);
    }

    public function importCompany($companyNumber, $authenticationCode, $customerId)
    {
        if ($companyNumber == NULL) {
            throw new Exception('company number needs to be set');
        }
        // --- check if company number exists in our db ---
        // --- sometimes companies house givers customer company number without leading zero ---
        $companyNumber = str_pad($companyNumber, 8, 0, STR_PAD_LEFT);
        $companyId = self::getDb()->fetchOne("SELECT company_id FROM ch_company WHERE company_number = '$companyNumber'");
        if (!empty($companyId)) {
            throw new Exception('Sorry, we cannot import this company as it is already in our database. <br/><br/>We can transfer this company to your account; however, we need written permission from the current account holder. Please ask them to email theteam@madesimplegroup.com specifying the name of the company and the email address for it to be transferred to.');
        }

        if ($authenticationCode == NULL) {
            throw new Exception('authentication needs to be set');
        }

        $companyInformations = dibi::select('company_id,customer_id, deleted ')
                ->from('ch_company', 'c')
                ->where('c.company_number=%s', 'DELETED_' . $companyNumber)
                ->execute()->fetch();

        // if (!empty($companyInformations) && $companyInformations->customer_id == $customerId){
        //     throw new Exception ("Sorry , you deleted this company. For more information contact Made Simple Group Team");
        // }

        $getData = CompanyDataRequest::getNewCompanyDataRequest($customerId, $companyNumber, $authenticationCode);
        $response = $getData->sendRequest();

        // --- check if there are any errors returned ---
        $xmlResponse = simplexml_load_string($response);

        if (isset($xmlResponse->GovTalkDetails->GovTalkErrors->Error)) {
            throw new Exception((string) $xmlResponse->GovTalkDetails->GovTalkErrors->Error->Text);
        }

        if (!empty($companyInformations) && $companyInformations->customer_id == $customerId) {
            DeleteCompany::restoreDeletedCompany($companyInformations['company_id'], $companyNumber);
            self::getDb()->update('ch_company', ['deleted' => FALSE, 'authentication_code' => $authenticationCode], 'company_id= ' . $companyInformations['company_id']);
            return TRUE;
        }

        if (!isset($xmlResponse->Body->CompanyData->CompanyName)) {
            throw new Exception('There was an error getting company name from Companies House for company: ' . $companyNumber . ' .');
        }

        // --- create new company ---
        $company = Company::getNewCompany($customerId, (string) $xmlResponse->Body->CompanyData->CompanyName);

        $company->setAuthenticationCode($authenticationCode);
        $company->setXmlCompanyData($response);
        unset($response);
        unset($xmlResponse);

        // --- second sync ---
        $chXmlGateway = new CHXmlGateway();
        $request = $chXmlGateway->getCompanyDetails($companyNumber);

        $result = $chXmlGateway->getResponse($request);
        // echo $result;exit;
        $result = simplexml_load_string($result);

        if (isset($result->Body->CompanyDetails)) {
            $result = $result->Body->CompanyDetails;
        } else {
            $company->delete();
            throw new Exception("There was an error getting data from Companies House. Please try again.");
        }

        $data = [];
        $data['company_number'] = isset($result->CompanyNumber) ? (string) $result->CompanyNumber : $companyNumber;
        $data['company_status'] = isset($result->CompanyStatus) ? (string) $result->CompanyStatus : '';
        $data['country_of_origin'] = isset($result->CountryOfOrigin) ? (string) $result->CountryOfOrigin : '';
        $data['incorporation_date'] = isset($result->IncorporationDate) ? (string) $result->IncorporationDate : '';
        $data['accounts_ref_date'] = isset($result->Accounts->AccountRefDate) ? (string) $result->Accounts->AccountRefDate : '';
        $data['accounts_next_due_date'] = isset($result->Accounts->NextDueDate) ? (string) $result->Accounts->NextDueDate : '';
        $data['accounts_last_made_up_date'] = isset($result->Accounts->LastMadeUpDate) ? (string) $result->Accounts->LastMadeUpDate : '';
        $data['returns_next_due_date'] = isset($result->Returns->NextDueDate) ? (string) $result->Returns->NextDueDate : '';
        $data['returns_last_made_up_date'] = isset($result->Returns->LastMadeUpDate) ? (string) $result->Returns->LastMadeUpDate : '';

        // --- remove empty values ---
        foreach ($data as $k => $v) {
            if (!isset($v) || empty($v)) {
                unset($data[$k]);
            }
        }
        /* mark certificate as printed */
        $data['is_certificate_printed'] = 1;
        $data['is_bronze_cover_letter_printed'] = 1;
        $data['is_ma_printed'] = 1;
        $data['is_ma_cover_letter_printed'] = 1;

        self::getDb()->update('ch_company', $data, 'company_id= ' . $company->getCompanyId());
    }

    /**
     * Returns if customer has incorporated company
     *
     * @param int $customerId
     * @return boolean
     */
    public function hasCustomerIncoporatedCompany($customerId)
    {
        $count = dibi::select('COUNT(*)')
                ->from('ch_company', 'c')
                ->where('c.customer_id=%i', $customerId)
                ->and('c.company_number IS NULL')
                ->execute()->fetchSingle();

        return ($count == 0) ? FALSE : TRUE;
    }

    /**
     * Returns TRUE if a company can be inported
     * Added by Razvan P 01/06/2011
     *
     * @param int $companyNumber
     * @param int $customerId
     * @return boolean
     *
     */
    public function isOk2Import($companyNumber, $customerId)
    {

        $companyInformations = dibi::select('company_id,customer_id, deleted ')
                ->from('ch_company', 'c')
                ->where('c.company_number=%i', $companyNumber)
                ->execute()->fetch();

        $companyId = $companyInformations->company_id;

        if (empty($companyId)) {
            return FALSE;
        }

        if ($companyInformations->customer_id == $customerId && $companyInformations->deleted == 0) {
            throw new Exception('Sorry the company exist in your account and you cand\'t import it again. If you want to update information pres sync ... bla bla');
        }

        if ($companyInformations->customer_id == $customerId && $companyInformations->deleted == 1) {
            throw new Exception('Sorry The company was deleted from our database.');
        }

        if ($companyInformations->customer_id != $customerId && $companyInformations->deleted == 0) {
            throw new Exception('Sorry , we cannot import this company as it is already in our database.<br/><br/>We can transfer this company to your account; however, we need written permission from the current account holder. Please ask them to email theteam@madesimplegroup.com specifying the name of the company and the email address for it to be transferred to.');
        }

        if ($companyInformations->customer_id != $customerId && $companyInformations->deleted == 1) {
            return $companyId;
        }
    }

}
