<?php

namespace CHFiling\Exceptions;

class CHExceptionFactory
{
    /**
     * @param object $error
     * @return CHException
     */
    public static function createChExceptionFromXmlGwError($error)
    {
        $errorCode = isset($error->Number) ? (int) $error->Number : 0;
        $errorText = isset($error->Text) ? (string) $error->Text : '';

        return self::getException($errorCode, $errorText);
    }

    /**
     * @param int $errorCode
     * @param string $errorText
     * @return CHException
     */
    private static function getException($errorCode, $errorText)
    {
        $exception = CHException::generic($errorText, $errorCode);

        switch ($errorText) {
            case 'Authentication code expired':
                $exception = AuthenticationCodeExpiredException::generic($errorText, $errorCode);
                break;
            case 'Invalid CompanyAuthenticationCode':
                $exception = InvalidCompanyAuthenticationCodeException::generic($errorText, $errorCode);
                break;
        }

        if (empty($errorText) && empty($errorCode)) {
            $exception = UnknownCHException::generic();
        }

        return $exception;
    }
}
