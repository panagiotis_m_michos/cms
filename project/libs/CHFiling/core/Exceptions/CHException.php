<?php

namespace CHFiling\Exceptions;

use Exception;

class CHException extends Exception
{
    /**
     * @param string $errorText
     * @param int $errorCode
     * @return CHException
     */
    public static function generic($errorText = "", $errorCode = 0)
    {
        return new static($errorText, $errorCode);
    }
}
