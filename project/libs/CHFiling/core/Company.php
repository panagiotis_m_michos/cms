<?php

use CHFiling\Exceptions\CHExceptionFactory;
use CompaniesHouse\Data\Entities\SicCodes;
use Entities\CompanyHouse\FormSubmission as FormSubmissionEntity;
use PeopleWithSignificantControl\Interfaces\IPscAware;

final class Company implements IPscAware
{

    const RANDOM_LTD_COMPANY_NAME = "CHANGE YOUR COMPANY NAME LTD";
    const RANDOM_LLP_COMPANY_NAME = "CHANGE YOUR COMPANY NAME LLP";

    /**
     * company_id field from database
     *
     * @var int
     */
    private $companyId;

    /**
     *
     * @var int
     */
    private $customerId;

    /**
     *
     * @var string
     */
    private $companyName;

    /**
     *
     * @var string
     */
    private $companyNumber;

    /**
     *
     * @var string
     */
    private $authenticationCode;

    /**
     *
     * @var string
     */
    private $companyCategory;

    /**
     *
     * @var int
     */
    private $dcaId;

    /**
     *
     * @var int
     */
    private $orderId;

    /**
     *
     * @var int
     */
    private $registeredOfficeId;

    /**
     *
     * @var int
     */
    private $nomineeDirectorId;

    /**
     *
     * @var int
     */
    private $nomineeSecretaryId;

    /**
     *
     * @var int
     */
    private $nomineeSubscriberId;

    /**
     *
     * @var int
     */
    private $annualReturnId;

    /**
     *
     * @var int
     */
    private $changeNameId;

    /**
     *
     * @var int
     */
    private $ereminderId;

    /**
     *
     * @var array<FormSubmission>
     */
    //private $formSubmissions;

    /**
     *
     * @var Address
     */
    private $registeredOffice;

    /**
     *
     * @var string
     */
    private $incorporationDate;

    /**
     *
     * @var Address
     */
    private $sailAddress;

    /**
     * added: Stan (30/04/2010)
     *
     * @var string
     */
    private static $tableName = 'ch_company';

    /**
     * @var int
     */
    private $locked;

    /**
     * @var int
     */
    private $hidden;

    /**
     *
     * @var string
     */
    private $ard;

    /**
     * added : Razvan Preda
     * @var int
     */
    private $deleted;

    /**
     *
     * @var int
     */
    private $serviceAddressId;

    /**
     * @var int
     */
    private $cashBackAmount;

    /**
     * @var string
     */
    private $noPscReason;

    /**
     *
     * @param int $companyId
     * @param int $customerId
     */
    private function __construct($companyId, $customerId = NULL)
    {
        // --- check that companyId is int ---
        if (!preg_match('/^\d+$/', $companyId)) {
            throw new Exception('companyId has to be integer');
        }

        // --- check that customerId is int ---
        if ($customerId !== NULL) {
            if (!preg_match('/^\d+$/', $customerId)) {
                throw new Exception('customerId has to be integer');
            }
            $customerSql = ' AND customer_id = ' . $customerId;
        } else {
            $customerSql = '';
        }

        // --- get data from db ---
        $sql = "
			SELECT * FROM `" . self::$tableName . "`
			WHERE `company_id` = $companyId" . $customerSql;
        if (!$result = CHFiling::getDb()->fetchRow($sql)) {
            throw new Exception("Company `$companyId` doesn't exist");
        }

        // --- set instance variables ---
        $this->companyId = $result['company_id'];
        $this->customerId = $result['customer_id'];
        $this->companyName = $result['company_name'];
        $this->companyNumber = $result['company_number'];
        $this->authenticationCode = $result['authentication_code'];
        $this->companyCategory = $result['company_category'];
        $this->dcaId = $result['dca_id'];
        $this->orderId = $result['order_id'];
        $this->registeredOfficeId = $result['registered_office_id'];
        $this->nomineeDirectorId = $result['nominee_director_id'];
        $this->nomineeSecretaryId = $result['nominee_secretary_id'];
        $this->nomineeSubscriberId = $result['nominee_subscriber_id'];
        $this->annualReturnId = $result['annual_return_id'];
        $this->changeNameId = $result['change_name_id'];
        $this->ereminderId = $result['ereminder_id'];
        $this->incorporationDate = $result['incorporation_date'];
        $this->locked = $result['locked'];
        $this->hidden = $result['hidden'];
        $this->cashBackAmount = $result['cash_back_amount'];
        $this->noPscReason = $result['no_psc_reason'];

        //adderd: Razvan P 25/05/2011
        $this->deleted = $result['deleted'];
        //*****

        $this->ard = $result['accounts_ref_date'];

        // --- registered office address ---
        $this->registeredOffice = new Address();
        $this->registeredOffice->setFields($result);

        // --- sail address ---
        $this->sailAddress = new Address();
        $this->sailAddress->setFields($result, 'sail_');

        $this->serviceAddressId = $result['service_address_id'];

        /* update company name - if needed */
        if ($this->getStatus() != 'complete') {
            $this->changeCompanyName($this->companyName);
        }
    }

    /**
     *
     * @param int $oldCompanyId
     * @param string $newCompanyName
     */
    public static function copy($customerId, $oldCompanyId, $newCompanyName)
    {

        // --- check if company has company incorporation submission ---
        $oldFormSubmissions = CHFiling::getDb()->fetchAll(
            'SELECT form_submission_id FROM ch_form_submission
            WHERE company_id = ' . $oldCompanyId .
            ' AND form_identifier = \'CompanyIncorporation\'
              AND date_cancelled IS NULL'
        );

        $size = count($oldFormSubmissions);
        if ($size < 1) {
            throw new Exception("Copied company doesn't have incorporation submission - nothing to be copied");
        }

        // --- get last incorporation submission ---
        $oldFormSubmissionId = $oldFormSubmissions[($size - 1)]['form_submission_id'];

        // --- insert new company data ---
        $data = [
            'customer_id' => $customerId,
            'company_name' => $newCompanyName];
        CHFiling::getDb()->insert(self::$tableName, $data);

        $newCompanyId = CHFiling::getDb()->lastInsertId();

        // --- copy the old form submission but with new company id ---
        $sql = "INSERT INTO ch_form_submission
            (`company_id`, `form_identifier`, `language`)
            SELECT $newCompanyId, `form_identifier`, `language`
            FROM ch_form_submission
            WHERE `form_submission_id` = $oldFormSubmissionId";

        // --- get id of the newly create form submission ---
        CHFiling::getDb()->query($sql);
        $newFormSubmissionId = CHFiling::getDb()->lastInsertId();

        // --- copy company incorporation form submission ---
        $oldIncorporation = CHFiling::getDb()->fetchRow(
            'SELECT * FROM ch_company_incorporation
            WHERE form_submission_id = ' . $oldFormSubmissionId
        );
        $oldIncorporation['form_submission_id'] = $newFormSubmissionId;
        $oldIncorporation['reject_reference'] = NULL; // make sure that we don't copy these
        $oldIncorporation['reject_description'] = NULL;

        CHFiling::getDb()->insert('ch_company_incorporation', $oldIncorporation);

        // --- copy all incorporation memebers ---
        $incMembers = CHFiling::getDb()->fetchAssoc(
            'SELECT * FROM `ch_incorporation_member`
            WHERE form_submission_id = ' . $oldFormSubmissionId
        );
        foreach ($incMembers as $value) {
            // --- insert new form submission id ---
            $value['form_submission_id'] = $newFormSubmissionId;
            unset($value['incorporation_member_id']);
            CHFiling::getDb()->insert('ch_incorporation_member', $value);
        }

        // --- copy authorised capital ---
        $incCapital = CHFiling::getDb()->fetchAssoc('SELECT * FROM `ch_incorporation_capital` WHERE form_submission_id = ' . $oldFormSubmissionId);
        foreach ($incCapital as $value) {
            // --- insert new form submission id ---
            $value['form_submission_id'] = $newFormSubmissionId;
            unset($value['incorporation_capital_id']);
            CHFiling::getDb()->insert('ch_incorporation_capital', $value);
        }
    }

    /**
     *
     * @param string $companyName
     */
    private function setCompanyName($companyName)
    {
        $companyName = trim($companyName);

        // --- check for special characters - copied from word etc. ---
        //if (preg_match('/[^\x20-\x7E]/', $companyName)) {
        //	throw new Exception('Company name can contain only UK (keyboard) letters');
        //}
        // --- check company name length ---
        if (strlen($companyName) > 160 || strlen($companyName) < 1) {
            throw new Exception('Company name has to be between 1 - 160 characters');
        }

        /* update db only if needed */
        if ($this->companyName != $companyName) {
            CHFiling::getDb()->update(self::$tableName, ['company_name' => $companyName], 'company_id = ' . $this->companyId);
            $this->companyName = $companyName;
        }
    }

    /**
     *
     * @param int $customerId
     * @return array
     */
    public static function getCustomerCompaniesIds($customerId)
    {
        // --- check that customerId is int ---
        if (!preg_match('/^\d+$/', $customerId)) {
            throw new Exception('customerId has to be integer');
        }

        // --- get data from db ---
        $sql = "
			SELECT `company_id` FROM `" . self::$tableName . "`
			WHERE `customer_id` = $customerId";

        return CHFiling::getDb()->fetchCol($sql);
    }

    /**
     *
     * @param boolean $bool
     */
    public function setCertificatePrinted($bool)
    {
        CHFiling::getDb()->update(self::$tableName, ['is_certificate_printed' => (boolean) $bool], 'company_id = ' . $this->companyId);
    }

    /**
     *
     * @param array $ids
     */
    public static function markCertificatesAsPrinted(array $ids)
    {
        foreach ($ids as $id) {
            if (!preg_match('/^\d+$/', $id)) {
                throw new Exception('id has to be integer');
            }
            CHFiling::getDb()->update(self::$tableName, ['is_certificate_printed' => 1], 'company_id = ' . $id);
        }
    }

    /**
     *
     * @param boolean $bool
     */
    public function setBronzeCoverLetterPrinted($bool)
    {
        CHFiling::getDb()->update(self::$tableName, ['is_bronze_cover_letter_printed' => (boolean) $bool], 'company_id = ' . $this->companyId);
    }

    /**
     *
     * @param array $ids
     */
    public static function markBronzeCoverLetterAsPrinted(array $ids)
    {
        foreach ($ids as $id) {
            if (!preg_match('/^\d+$/', $id)) {
                throw new Exception('id has to be integer');
            }
            CHFiling::getDb()->update(self::$tableName, ['is_bronze_cover_letter_printed' => 1], 'company_id = ' . $id);
        }
    }

    /**
     *
     * @param boolean $bool
     */
    public function setMACoverLetterPrinted($bool)
    {
        CHFiling::getDb()->update(self::$tableName, ['is_ma_cover_letter_printed' => (boolean) $bool], 'company_id = ' . $this->companyId);
    }

    /**
     *
     * @param array $ids
     */
    public static function markMACoverLetterAsPrinted(array $ids)
    {
        foreach ($ids as $id) {
            if (!preg_match('/^\d+$/', $id)) {
                throw new Exception('id has to be integer');
            }
            CHFiling::getDb()->update(self::$tableName, ['is_ma_cover_letter_printed' => 1], 'company_id = ' . $id);
        }
    }

    /**
     *
     * @param boolean $bool
     */
    public function setMAPrinted($bool)
    {
        CHFiling::getDb()->update(self::$tableName, ['is_ma_printed' => (boolean) $bool], 'company_id = ' . $this->companyId);
    }

    /**
     *
     * @param array $ids
     */
    public static function markMAAsPrinted(array $ids)
    {
        foreach ($ids as $id) {
            if (!preg_match('/^\d+$/', $id)) {
                throw new Exception('id has to be integer');
            }
            CHFiling::getDb()->update(self::$tableName, ['is_ma_printed' => 1], 'company_id = ' . $id);
        }
    }

    public function hasCertificate()
    {
        $result = dibi::select('*')->from('ch_company')->where('company_id=%i', $this->companyId)->and('is_certificate_printed=%i', 1)->execute()->fetch();
        return ($result === FALSE) ? FALSE : TRUE;
    }

    public function hasCoverLetter()
    {
        $result = dibi::select('*')->from('ch_company')->where('company_id=%i', $this->companyId)->and('is_bronze_cover_letter_printed=%i', 1)->execute()->fetch();
        return ($result === FALSE) ? FALSE : TRUE;
    }

    public function hasMA()
    {
        $result = dibi::select('*')->from('ch_company')->where('company_id=%i', $this->companyId)->and('is_ma_printed=%i', 1)->execute()->fetch();
        return ($result === FALSE) ? FALSE : TRUE;
    }

    public function hasMACoverLetter()
    {
        $result = dibi::select('*')->from('ch_company')->where('company_id=%i', $this->companyId)->and('is_ma_cover_letter_printed=%i', 1)->execute()->fetch();
        return ($result === FALSE) ? FALSE : TRUE;
    }

    /**
     * returns existing company
     *
     * @param int $companyId
     * @return Company
     */
    public static function getCompany($companyId)
    {
        return new Company($companyId);
    }

    /**
     * sets new customer id - used for moving companies
     *
     * @param int $id
     */
    public function setCustomerId($id)
    {
        CHFiling::getDb()->update(self::$tableName, ['customer_id' => $id], 'company_id = ' . $this->companyId);
        $this->customerId = $id;
    }

    /**
     *
     * @param string $authenticationCode 6 chars
     */
    public function setAuthenticationCode($authenticationCode)
    {
        if (strlen($authenticationCode) != 6) {
            throw new Exception('Authentication code has to consist of 6 characters');
        }
        CHFiling::getDb()->update(self::$tableName, ['authentication_code' => $authenticationCode], 'company_id = ' . $this->companyId);
        $this->authenticationCode = $authenticationCode;
    }

    /**
     *
     * @param string $companyNumber 8 chars
     */
    public function setCompanyNumber($companyNumber)
    {
        if (strlen($companyNumber) != 8) {
            throw new Exception('Company number has to consist of 8 characters');
        }
        CHFiling::getDb()->update(self::$tableName, ['company_number' => $companyNumber], 'company_id = ' . $this->companyId);
        $this->companyNumber = $companyNumber;
    }

    /**
     * returns list of all documents for this company
     *
     * @return array
     */
    public function getDocumentsList()
    {
        $filePath = CHFiling::getDocPath() . '/' . CHFiling::getDirName($this->companyId) . '/company-' . $this->companyId . '/documents/';
        if (!file_exists($filePath)) {
            return [];
        }
        $files = FTools::scanDir($filePath, FTools::RETURN_FILES);
        // --- get accepted form submission ---
        $formSubmission = FormSubmission::getCompanyIncorporationFormSubmission($this);
        if ($formSubmission != NULL) {
            if ($this->getType() != 'LLP') {
                $type = $formSubmission->getForm()->getArticles();
                // --- check if user uploaded their own articles, if not, we need to
                // --- provide model articles from companies house as well ---
                if ($type != 'BESPOKE') {
                    $files[] = 'ModelArticles';
                }
            }
        }
        return $files;
    }

    public function getDocument($name, $pdfOutput = TRUE)
    {
        $filePath = CHFiling::getDocPath() . '/' . CHFiling::getDirName($this->companyId) . '/company-' . $this->companyId . '/documents/' . $name;
        $formSubmission = FormSubmission::getCompanyIncorporationFormSubmission($this);
        // --- no form submission ---
        $type = NULL;
        if ($formSubmission != NULL) {
            $type = $formSubmission->getForm()->getArticles();
        }

        if ($type !== NULL) {
            // --- request for model articles ---
            if ($name == 'ModelArticles') {
                $name = 'Articles.pdf';
                $filePath = CHFiling::getDocPath() . '/_modelArticles/' . $type . '/Articles.pdf';
                // --- they used our articles - so we sent only memorandum ---
            } elseif ($type != 'BESPOKE' && $name == 'article.pdf') {
                $name = 'Memorandum.pdf';
            }
        }

        $out = @file_get_contents($filePath);
        if ($out === FALSE) {
            throw new Exception("Document `$name` doesn't exist!");
        }

        if (!$pdfOutput) {
            return $out;
        }

        // --- prepare headers ---

        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header('Content-disposition: attachment; filename=' . $name);
        header("Content-Type: application/pdf");
        header("Content-Transfer-Encoding: binary");
        header('Content-Length: ' . strlen($out));

        /*
          $agent = trim($_SERVER['HTTP_USER_AGENT']);
          if ((preg_match('|MSIE ([0-9.]+)|', $agent, $version)) ||
          (preg_match('|Internet Explorer/([0-9.]+)|', $agent, $version))) {
          header('Content-Type: application/x-msdownload');
          Header('Content-Length: ' . strlen($out));
          if ($version == '5.5') {
          header('Content-Disposition: filename="' . $name . '"');
          } else {
          header('Content-Disposition: attachment; filename="' . $name . '"');
          }
          } else {
          Header('Content-Type: application/pdf');
          Header('Content-Length: ' . strlen($out));
          Header('Content-disposition: attachment; filename=' . $name);
          }
         */

        // --- output ---
        echo $out;
        exit;
    }

    /**
     *
     * @param int $companyId
     * @param int $customerId
     * @return Company
     */
    public static function getCustomerCompany($companyId, $customerId)
    {
        return new Company($companyId, $customerId);
    }

    /**
     *
     * @return string
     */
    public function getIncorporationDate()
    {
        return $this->incorporationDate;
    }

    /**
     * creates and returns new company
     *
     * @param int $customerId
     * @param string $companyName
     * @return Company
     */
    public static function getNewCompany($customerId, $companyName)
    {
        // --- check that customerId is int ---
        if (!preg_match('/^\d+$/', $customerId)) {
            throw new Exception('customerId has to be integer');
        }
        // --- check company name length ---
        if (strlen($companyName) > 160 || strlen($companyName) < 1) {
            throw new Exception('Company name has to be between 1 - 160 characters');
        }

        // --- insert new data into db ---
        $data = [
            'customer_id' => $customerId,
            'company_name' => $companyName];
        CHFiling::getDb()->insert(self::$tableName, $data);

        // --- return newly created company ---
        return new Company(CHFiling::getDb()->lastInsertId());
    }

    /**
     *
     * @return int
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     *
     * @return int
     */
    public function getId()
    {
        return $this->companyId;
    }

    /**
     *
     * @return int
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     *
     * @return string
     */
    public function getCompanyNumber()
    {
        return $this->companyNumber;
    }

    /**
     *
     * @return string
     */
    public function getAuthenticationCode()
    {
        return $this->authenticationCode;
    }

    /**
     *
     * @return string
     */
    public function getCompanyCategory()
    {
        return $this->companyCategory;
    }

    /**
     * return array representation of the company
     *
     * @return array
     */
    public function getData()
    {
        $company = CHFiling::getDb()->fetchRow('SELECT * FROM `ch_company` WHERE `company_id` = ' . $this->companyId);
        $members = CHFiling::getDb()->fetchAssoc('SELECT * FROM `ch_company_member` WHERE `company_id` = ' . $this->companyId);
        $capital = CHFiling::getDb()->fetchAssoc('SELECT * FROM `ch_company_capital` WHERE company_id = ' . $this->companyId);

        $x = 0;
        $capitals = [];
        foreach ($capital as $value) {
            $capitals[$x] = $value;
            $capitals[$x]['shares'] = CHFiling::getDb()->fetchAssoc('SELECT * FROM ch_company_capital_shares WHERE company_capital_id = ' . $value['company_capital_id']);
            $x++;
        }

        // --- company details ---
        $companyDetails = [];
        $companyDetails['company_number'] = $company['company_number'];
        $companyDetails['company_name'] = $company['company_name'];
        $companyDetails['authentication_code'] = $company['authentication_code'];
        $companyDetails['company_category'] = $company['company_category'];
        $companyDetails['jurisdiction'] = $company['jurisdiction'];
        $companyDetails['made_up_date'] = $company['made_up_date'];
        $companyDetails['next_due_date'] = $company['next_due_date'];
        $companyDetails['sic_codes'] = $company['sic_code1'] . ' ' . $company['sic_code2'] . ' ' . $company['sic_code3'] . ' ' . $company['sic_code4'];

        $companyDetails['company_status'] = $company['company_status'];
        $companyDetails['country_of_origin'] = $company['country_of_origin'];
        $companyDetails['incorporation_date'] = $company['incorporation_date'];
        $companyDetails['accounts_ref_date'] = $company['accounts_ref_date'];
        $companyDetails['accounts_next_due_date'] = $company['accounts_next_due_date'];
        $companyDetails['accounts_last_made_up_date'] = $company['accounts_last_made_up_date'];
        $companyDetails['returns_next_due_date'] = $company['returns_next_due_date'];
        $companyDetails['returns_last_made_up_date'] = $company['returns_last_made_up_date'];

        // --- regisetered office ---
        $registeredOffice = [];
        $registeredOffice['premise'] = $company['premise'];
        $registeredOffice['street'] = $company['street'];
        $registeredOffice['thoroughfare'] = $company['thoroughfare'];
        $registeredOffice['post_town'] = $company['post_town'];
        $registeredOffice['county'] = $company['county'];
        $registeredOffice['country'] = (isset(Address::$registeredOfficeCountries[$company['country']])) ? Address::$registeredOfficeCountries[$company['country']] : $company['country'];
        $registeredOffice['postcode'] = $company['postcode'];
        $registeredOffice['care_of_name'] = $company['care_of_name'];
        $registeredOffice['po_box'] = $company['po_box'];

        // --- sail address ---
        $sailAddress = [];
        $sailAddress['premise'] = $company['sail_premise'];
        $sailAddress['street'] = $company['sail_street'];
        $sailAddress['thoroughfare'] = $company['sail_thoroughfare'];
        $sailAddress['post_town'] = $company['sail_post_town'];
        $sailAddress['county'] = $company['sail_county'];
        $sailAddress['country'] = $company['sail_country'];
        $sailAddress['postcode'] = $company['sail_postcode'];
        $sailAddress['care_of_name'] = $company['sail_care_of_name'];
        $sailAddress['po_box'] = $company['sail_po_box'];

        $DIR = [];
        $SEC = [];
        $SUB = [];
        $PSC = [];
        foreach ($members as $value) {
            $pom = [];

            $pom['id'] = $value['company_member_id'];
            $pom['corporate'] = $value['corporate'];
            if ($value['corporate']) {
                $pom['corporate_name'] = $value['corporate_name'];
            } else {
                $pom['title'] = $value['title'];
                if ($value['type'] == 'DIR') {
                    $pom['dob'] = $value['dob'];
                    $pom['nationality'] = $value['nationality'];
                    $pom['occupation'] = $value['occupation'];
                    $pom['country_of_residence'] = $value['country_of_residence'];
                }
            }
            $pom['surname'] = $value['surname'];
            $pom['forename'] = $value['forename'];
            $pom['middle_name'] = $value['middle_name'];
            $pom['premise'] = $value['premise'];
            $pom['street'] = $value['street'];
            $pom['thoroughfare'] = $value['thoroughfare'];
            $pom['share_class'] = $value['share_class'];
            $pom['num_shares'] = $value['num_shares'];
            $pom['post_town'] = $value['post_town'];
            $pom['county'] = $value['county'];
            $pom['country'] = $value['country'];
            $pom['postcode'] = $value['postcode'];
            $pom['care_of_name'] = $value['care_of_name'];

            ${$value['type']}[$value['company_member_id']] = $pom;
        }

        // --- subscribers - shareholders at incorporation ---
        if (FormSubmission::getCompanyIncorporationFormSubmission($this) !== NULL) {
            $companyIncorporation = FormSubmission::getCompanyIncorporationFormSubmission($this);
            $corpSubs = $companyIncorporation->getForm()->getIncCorporates(['SUB']);
            $persSubs = $companyIncorporation->getForm()->getIncPersons(['SUB']);

            $subscribers = [];
            $x = 0;
            foreach ($corpSubs as $sub) {
                $subscribers[$x]['id'] = $sub->getId();
                $subscribers[$x]['corporate_name'] = $sub->getCorporate()->getCorporateName();
                $subscribers[$x]['shares'] = $sub->getAllotmentShares()->getNumShares();
                $subscribers[$x]['currency'] = $sub->getAllotmentShares()->getCurrency();
                $subscribers[$x]['share_value'] = $sub->getAllotmentShares()->getShareValue();
                $x++;
            }
            foreach ($persSubs as $sub) {
                $subscribers[$x]['id'] = $sub->getId();
                $subscribers[$x]['surname'] = $sub->getPerson()->getSurname();
                $subscribers[$x]['forename'] = $sub->getPerson()->getForename();
                $subscribers[$x]['shares'] = $sub->getAllotmentShares()->getNumShares();
                $subscribers[$x]['currency'] = $sub->getAllotmentShares()->getCurrency();
                $subscribers[$x]['share_value'] = $sub->getAllotmentShares()->getShareValue();
                $x++;
            }
        } else {
            $subscribers = [];
        }



        // --- capital ---
        $capital = [];
        $x = 0;
        foreach ($capitals as $value) {
            $capital[$x]['total_issued'] = $value['total_issued'];
            $capital[$x]['currency'] = $value['currency'];
            $capital[$x]['total_aggregate_value'] = $value['total_aggregate_value'];
            if (isset($value['shares']) && !empty($value['shares'])) {
                $i = 0;
                foreach ($value['shares'] as $share) {
                    $capital[$x]['shares'][$i]['share_class'] = $share['share_class'];
                    $capital[$x]['shares'][$i]['prescribed_particulars'] = $share['prescribed_particulars'];
                    $capital[$x]['shares'][$i]['num_shares'] = $share['num_shares'];
                    $capital[$x]['shares'][$i]['amount_paid'] = $share['amount_paid'];
                    $capital[$x]['shares'][$i]['amount_unpaid'] = $share['amount_unpaid'];
                    $capital[$x]['shares'][$i]['nominal_value'] = $share['nominal_value'];
                    $i++;
                }
            }
            $x++;
        }

        $data = array_merge(
            ['company_details' => $companyDetails],
            ['registered_office' => $registeredOffice],
            ['sail_address' => $sailAddress],
            ['directors' => $DIR],
            ['secretaries' => $SEC],
            ['shareholders' => $SUB],
            //todo: psc - create view list in controller?
            //todo: psc - add getPscs method?
            //['pscs' => PscViewFactory::fromList(array_merge($this->getPersons(['PSC']), $this->getCorporates(['PSC'])))],
            ['pscs' => $PSC],
            ['capitals' => $capital],
            ['subscribers' => $subscribers]
        );

        return $data;
    }

    /**
     * @param int $id
     * @return Member
     * @throws Exception
     */
    public function getOfficer($id)
    {
        //todo: psc - add legal entity?
        $fields = CHFiling::getDb()->fetchRow(
            'SELECT * FROM `ch_company_member`
              WHERE `company_member_id` = ' . $id . '
              AND `company_id` = ' . $this->companyId
        );
        if (!empty($fields)) {
            switch ($fields['corporate']) {
                case 0:
                    $officer = $this->getPerson($id, $fields['type']);
                    $officer->setType($fields['type']);
                    $officer->setCorporateType(0);
                    break;
                case 1:
                    $officer = $this->getCorporate($id, $fields['type']);
                    $officer->setType($fields['type']);
                    $officer->setCorporateType(1);
                    break;
            }
        } else {
            throw new Exception('Officer record not exist');
        }

        return $officer;
    }

    public function sendOfficerResignationNew($officer, $resignDate)
    {
        $formSubmission = FormSubmission::getNewFormSubmission($this, 'OfficerResignation');
        /** @var OfficerResignation $form */
        $form = $formSubmission->getForm();
        $form->setCompanyType($this->getType());
        $form->setResignDate($resignDate);
        $form->setOfficer($officer);
        $formSubmission->sendRequest();
    }

    /**
     * @param int $id
     * @param string $type
     * @return Member
     * @throws Exception
     */
    public function getPerson($id, $type)
    {
        if (!is_numeric($id)) {
            throw new Exception('Sorry, you cannot access that URL.');
        }

        $type = strtoupper($type);
        $fields = CHFiling::getDb()->fetchRow(
            'SELECT * FROM `ch_company_member`
            WHERE `company_member_id` = ' . $id . '
            AND `type` = \'' . $type . '\' AND corporate = 0'
        );

        switch ($type) {
            case 'DIR':
                $person = new PersonDirector();
                break;
            case 'SEC':
                $person = new PersonSecretary();
                break;
            case 'SUB':
                $person = new PersonSubscriber();
                break;
            case 'PSC':
                $person = new PersonPsc();
                break;
        }

        $person->setFields($fields);
        if ($this->getType() == 'LLP') {
            $person->setDesignatedInd($fields['designated_ind']);
        }
        return $person;
    }

    public function getNewPerson($type)
    {
        $type = strtoupper($type);

        switch ($type) {
            case 'DIR':
                $person = new PersonDirector();
                break;
            case 'SEC':
                $person = new PersonSecretary();
                break;
            case 'SUB':
                $person = new PersonSubscriber();
                break;
            case 'PSC':
                $person = new PersonPsc();
                break;
        }

        return $person;
    }

    /**
     *
     * @param int $id
     * @param DIR|SUB|SEC $type
     * @return Member
     */
    public function getCorporate($id, $type)
    {
        if (!is_numeric($id)) {
            throw new Exception('Sorry, you cannot access that URL.');
        }
        $type = strtoupper($type);
        $fields = CHFiling::getDb()->fetchRow(
            'SELECT * FROM `ch_company_member`
            WHERE `company_member_id` = ' . $id . '
            AND `type` = \'' . $type . '\' AND corporate = 1'
        );

        switch ($type) {
            case 'DIR':
                $person = new CorporateDirector();
                break;
            case 'SEC':
                $person = new CorporateSecretary();
                break;
            case 'SUB':
                $person = new CorporateSubscriber();
                break;
            case 'PSC':
                $person = new CorporatePsc();
                break;
        }

        $person->setFields($fields);
        if ($this->getType() == 'LLP') {
            $person->setDesignatedInd($fields['designated_ind']);
        }

        return $person;
    }

    public function getNewCorporate($type)
    {
        $type = strtoupper($type);

        switch ($type) {
            case 'DIR':
                $person = new CorporateDirector();
                break;
            case 'SEC':
                $person = new CorporateSecretary();
                break;
            case 'SUB':
                $person = new CorporateSubscriber();
                break;
            case 'PSC':
                $person = new CorporatePsc();
                break;
        }

        return $person;
    }

    /**
     * same as getCompanyCategory
     *
     * @return string
     */
    public function getType()
    {
        return $this->companyCategory;
    }

    /**
     *
     * @param int $formSubmissionId
     * @return FormSubmission
     */
    public function getFormSubmission($formSubmissionId)
    {
        return FormSubmission::getFormSubmission($this, $formSubmissionId);
    }

    /**
     *
     * @return FormSubmission[]
     */
    public function getFormSubmissions()
    {
        $formSubmissions = [];
        $formSubmissionsIds = FormSubmission::getCompanyFormSubmissionsIds($this->companyId);
        foreach ($formSubmissionsIds as $v) {
            $formSubmissions[$v] = FormSubmission::getFormSubmission($this, $v);
        }

        return $formSubmissions;
    }

    /**
     *
     * @return FormSubmission[]
     */
    public function getIncorporationFormSubmissions()
    {
        $formSubmissions = [];
        $formSubmissionsIds = FormSubmission::getCompanyIncorporationFormSubmissionsIds($this->companyId);
        foreach ($formSubmissionsIds as $v) {
            $formSubmissions[$v] = FormSubmission::getFormSubmission($this, $v);
        }

        return $formSubmissions;
    }

    /**
     * change is not send to companies house - this is only for
     *
     * @param string $companyName
     */
    public function changeCompanyName($companyName)
    {

        /* check if company is incorporated */
        if ($this->getStatus() == 'complete') {
            $this->sendChangeCompanyName($companyName);
            throw new Exception('Company is incorporated, use paper form to change name.');
        }

        //fixing for getType formSubmision error
        $type = NULL;
        foreach ($this->getFormSubmissions() as $form) {
            if ($form->getClass() == FormSubmissionEntity::TYPE_COMPANY_INCORPORATION) {
                $type = $form->getForm()->getType();
            }
        };

        /* get type of the company incorproation submission */
        //$type = $this->getLastFormSubmission();
        /* no form submission yet, do nothing */
        if ($type === NULL) {
            return;
        }
        //$type = $type->getForm()->getType();

        /* company name and suffix */
        $companyName = strtoupper(trim($companyName));
        $suffix = '';

        /* if there's no prefix, then add defaul one */
        if (!preg_match('/( PLC| LTD| PUBLIC LIMITED COMPANY| LIMITED| PLC.| LTD.| LIMITED.| CYF| CYFYNGEDIG| LLP| LIMITED LIABILITY PARTNERSHIP| CIC| PAC| Partneriaeth Atebolrwydd Cyfyngedig)$/', $companyName)) {
            /* by shares and by guarantee */
            if ($type == 'BYSHR' || $type == 'BYGUAR') {
                $suffix = ' LTD';
                /* plc */
            } elseif ($type == 'PLC') {
                $suffix = ' PLC';
            } elseif ($type == 'LLP') {
                $suffix = ' LLP';
            }
        }

        /* set company name with the suffix */
        $this->setCompanyName($companyName . $suffix);
    }

    /**
     *
     * @return FormSubmission
     */
    public function getLastFormSubmission()
    {
        $formSubmissions = $this->getFormSubmissions();
        if (empty($formSubmissions)) {
            //throw new Exception("Form submission {$this->companyName} doesn't exist!");
            return NULL;
        }
        // get ids
        $ids = FormSubmission::getCompanyFormSubmissionsIds($this->companyId);
        // return id that's at index 0
        return $formSubmissions[$ids[0]];
    }

    /**
     *
     * @return FormSubmission
     */
    public function getLastIncorporationFormSubmission()
    {
        $incorporationFormSubmissions = $this->getIncorporationFormSubmissions();
        if (empty($incorporationFormSubmissions)) {
            return NULL;
        }

        $ids = FormSubmission::getCompanyIncorporationFormSubmissionsIds($this->companyId);

        return $incorporationFormSubmissions[$ids[0]];
    }

    /**
     * @param int $productId
     */
    public function setProductId($productId)
    {
        CHFiling::getDb()->update(self::$tableName, ['product_id' => $productId], 'company_id = ' . $this->companyId);
    }

    /**
     * @param int $cashBackAmount
     */
    public function setCashBackAmount($cashBackAmount)
    {
        CHFiling::getDb()->update(self::$tableName, ['cash_back_amount' => $cashBackAmount], 'company_id = ' . $this->companyId);
        $this->cashBackAmount = $cashBackAmount;
    }

    /**
     * @return string
     */
    public function getNoPscReason()
    {
        return $this->noPscReason;
    }

    /**
     * @param string $noPscReason
     */
    public function setNoPscReason($noPscReason)
    {
        CHFiling::getDb()->update(self::$tableName, ['no_psc_reason' => $noPscReason], 'company_id = ' . $this->companyId);
        $this->noPscReason = $noPscReason;
    }

    /**
     * @return CompanyPersonPsc[]
     */
    public function getPersonPscs()
    {
        return $this->getPersons(['PSC']);
    }

    /**
     * @return CompanyCorporatePsc[]
     */
    public function getCorporatePscs()
    {
        return $this->getCorporates(['PSC']);
    }

    /**
     * @return CompanyLegalPersonPsc[]
     */
    public function getLegalPersonPscs()
    {
        $pscs = [];
        $pscsData = CHFiling::getDb()->fetchAssoc(
            "SELECT * FROM ch_company_member WHERE company_id = {$this->companyId} AND `type` = 'PSC' AND corporate = 2"
        );

        foreach ($pscsData as $pscData) {
            $psc = new CompanyLegalPersonPsc($this->companyId, $pscData['company_member_id']);
            $psc->setFields($pscData);
            $pscs[] = $psc;
        }

        return $pscs;
    }

    /**
     * @return string enum(complete, incomplete, pending, rejected, error)
     */
    public function getStatus()
    {
        // --- company number is set ---
        if (isset($this->companyNumber)) {
            return 'complete';
        }
        // --- imported company ---
        if ($this->getLastFormSubmission() === NULL) {
            return NULL;
        }

        // --- check form submission status ---
        $lastFormSubmissionStatus = $this->getLastFormSubmission()->getSubmissionStatus();

        // --- get ids ---
        $ids = FormSubmission::getCompanyFormSubmissionsIds($this->companyId);
        if (count(FormSubmission::getCompanyFormSubmissionsIds($this->companyId)) > 1) {
            // --- get the last sent form submission ---
            $formSubmission = FormSubmission::getFormSubmission($this, $ids[1]);
            if ($formSubmission->isReject() && $lastFormSubmissionStatus == NULL) {
                return 'rejected';
            }
        }

        switch ($lastFormSubmissionStatus) {
            case FormSubmissionEntity::RESPONSE_PENDING:
            case FormSubmissionEntity::RESPONSE_INTERNAL_FAILURE:
                return 'pending';
                break;
            case FormSubmissionEntity::RESPONSE_ACCEPT:
                return 'complete';
                break;
            case FormSubmissionEntity::RESPONSE_REJECT:
                return 'rejected';
                break;
            case FormSubmissionEntity::RESPONSE_ERROR:
                return 'error';
                break;
            default: // NULL
                return 'incomplete';
        }
    }

    /**
     *
     * @return int
     */
    public function getDcaId()
    {
        return $this->dcaId;
    }

    /**
     *
     * @param int $dcaId
     */
    public function setDcaId($dcaId)
    {
        CHFiling::getDb()->update(self::$tableName, ['dca_id' => $dcaId], 'company_id = ' . $this->companyId);
        $this->dcaId = $dcaId;
    }

    /**
     *
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     *
     * @param int $orderId
     */
    public function setOrderId($orderId)
    {
        CHFiling::getDb()->update(self::$tableName, ['order_id' => $orderId], 'company_id = ' . $this->companyId);
        $this->orderId = $orderId;
    }

    /**
     *
     * @return int
     */
    public function getRegisteredOfficeId()
    {
        return $this->registeredOfficeId;
    }

    /**
     *
     * @param int $RegisteredOfficeId
     */
    public function setRegisteredOfficeId($RegisteredOfficeId)
    {
        CHFiling::getDb()->update(self::$tableName, ['registered_office_id' => $RegisteredOfficeId], 'company_id = ' . $this->companyId);
        $this->RegisteredOfficeId = $RegisteredOfficeId;
    }

    /**
     *
     * @return int
     */
    public function getNomineeDirectorId()
    {
        return $this->nomineeDirectorId;
    }

    /**
     *
     * @param int $nomineeDirectorId
     */
    public function setNomineeDirectorId($nomineeDirectorId)
    {
        CHFiling::getDb()->update(self::$tableName, ['nominee_director_id' => $nomineeDirectorId], 'company_id = ' . $this->companyId);
        $this->nomineeDirectorId = $nomineeDirectorId;
    }

    /**
     *
     * @return int
     */
    public function getNomineeSecretaryId()
    {
        return $this->nomineeSecretaryId;
    }

    /**
     *
     * @param int $nomineeSecretaryId
     */
    public function setNomineeSecretaryId($nomineeSecretaryId)
    {
        CHFiling::getDb()->update(self::$tableName, ['nominee_secretary_id' => $nomineeSecretaryId], 'company_id = ' . $this->companyId);
        $this->nomineeSecretaryId = $nomineeSecretaryId;
    }

    /**
     *
     * @return int
     */
    public function getNomineeSubscriberId()
    {
        return $this->nomineeSubscriberId;
    }

    /**
     *
     * @return int
     */
    public function getNomineeShareholderId()
    {
        return $this->nomineeSubscriberId;
    }

    /**
     *
     * @param int $nomineeSubscriberId
     */
    public function setNomineeShareholderId($nomineeSubscriberId)
    {
        CHFiling::getDb()->update(self::$tableName, ['nominee_subscriber_id' => $nomineeSubscriberId], 'company_id = ' . $this->companyId);
        $this->nomineeSubscriberId = $nomineeSubscriberId;
    }

    /**
     *
     * @return int
     */
    public function getAnnualReturnId()
    {
        return $this->annualReturnId;
    }

    /**
     * retunrs FALSE if annual return id is NULL
     *
     * @return boolean
     */
    public function hasAnnualReturn()
    {
        return ($this->annualReturnId == NULL) ? FALSE : TRUE;
    }

    /**
     * Removes annual return from the company
     *
     * @param int $id
     */
    public static function removeAnnualReturn($id)
    {
        CHFiling::getDb()->update(self::$tableName, ['annual_return_id' => NULL], 'company_id = ' . $id);
    }

    /**
     *
     * @param int $annualReturnId
     */
    public function setAnnualReturnId($annualReturnId)
    {
        CHFiling::getDb()->update(self::$tableName, ['annual_return_id' => $annualReturnId], 'company_id = ' . $this->companyId);
        $this->annualReturnId = $annualReturnId;
    }

    /**
     *
     * @return int
     */
    public function getChangeNameId()
    {
        return $this->changeNameId;
    }

    /**
     * Creates new annual return form submission
     *
     * @param int $changeNameId
     */
    public function setChangeName($changeNameId)
    {

        $this->setChangeNameId($changeNameId);

        foreach ($this->getFormSubmissions() as $k => $v) {
            if ($v->getFormIdentifier() == 'ChangeOfName' && $v->getSubmissionStatus() != 'ACCEPT') {
                /* ChangeOfName is already set */
                return;
            }
        }

        /* create new ChangeOfName */
        FormSubmission::getNewFormSubmission($this, 'ChangeOfName');
    }

    /**
     *
     * @param int $change_name_id
     */
    public function setChangeNameId($changeNameId)
    {
        CHFiling::getDb()->update(self::$tableName, ['change_name_id' => $changeNameId], 'company_id = ' . $this->companyId);
        $this->changeNameId = $changeNameId;
    }

    /**
     * retunrs FALSE if Change Name id is NULL
     *
     * @return boolean
     */
    public function hasChangeName()
    {
        return ($this->changeNameId == NULL) ? FALSE : TRUE;
    }

    /**
     * Removes annual return from the company
     *
     * @param int $id
     */
    public function removeChangeName($id)
    {
        CHFiling::getDb()->update(self::$tableName, ['change_name_id' => NULL], 'company_id = ' . $id);
    }

    /**
     *
     * @return int
     */
    public function getEreminderId()
    {
        return $this->ereminderId;
    }

    /**
     *
     * @return array
     */
    public static function getEreminderCompanyIds()
    {
        return CHFiling::getDb()->fetchCol('SELECT company_id FROM `ch_company` WHERE `ereminder_id` = 1 AND company_number IS NOT NULL');
    }

    /**
     * @return array
     */
    public static function getMissedDocumentsSubmisionIds()
    {
        return CHFiling::getDb()->fetchCol(
            "SELECT f.form_submission_id
            FROM ch_company
            LEFT JOIN ch_form_submission as f ON (ch_company.company_id = f.company_id)
            WHERE ch_company.document_date is NULL
            AND ch_company.company_number IS NOT NULL
            AND ch_company.product_id IS NOT NULL
            AND f.date_cancelled IS NULL
            AND f.form_identifier = 'CompanyIncorporation'
            AND f.response = 'ACCEPT' "
        );
    }

    /**
     *
     * @return int
     */
    public function setEreminderId($ereminderId)
    {
        CHFiling::getDb()->update(self::$tableName, ['ereminder_id' => $ereminderId], 'company_id = ' . $this->companyId);
        $this->ereminderId = $ereminderId;
    }

    /**
     *
     * @return Address
     */
    public function getRegisteredOffice()
    {
        return clone $this->registeredOffice;
    }

    /**
     *
     * @return Address
     */
    public function getSailAddress()
    {
        return clone $this->sailAddress;
    }

    /**
     * @param array $types
     * @return PersonMember[]
     * @throws Exception
     */
    public function getPersons(array $types = [])
    {
        foreach ($types as $value) {
            $value = strtoupper($value);
            if (!in_array($value, ['DIR', 'SEC', 'SUB', 'PSC'])) {
                throw new Exception("value $value is not allowed");
            }
        }

        $persons = [];
        // --- get data from db ---
        $pers = CHFiling::getDb()->fetchAssoc(
            'SELECT * FROM `ch_company_member`
              WHERE `company_id` = ' . $this->companyId . '
              AND corporate = 0 ' .
            (!empty($types) ? 'AND `type` IN ("' . implode('","', $types) . '")' : '')
        );

        // --- check type and populate appropriate object ---
        foreach ($pers as $v) {
            switch ($v['type']) {
                case 'DIR':
                    $person = new PersonDirector();
                    break;
                case 'SEC':
                    $person = new PersonSecretary();
                    break;
                case 'SUB':
                    $person = new PersonSubscriber();
                    break;
                case 'PSC':
                    $person = new CompanyPersonPsc($this->companyId, $v['company_member_id']);
                    break;
            }
            // fix becouse CH send back 0 for number of shares which is do not make any sence
            if (!$person instanceof PersonSubscriber || ($v['num_shares'] != 0 && $person instanceof PersonSubscriber)) {
                $person->setFields($v);
            }
            $persons[] = $person;
        }
        // --- return all objects ---
        return $persons;
    }

    /**
     * @param array $types
     * @return CorporateMember[]
     * @throws Exception
     */
    public function getCorporates(array $types = [])
    {
        foreach ($types as $value) {
            $value = strtoupper($value);
            if (!in_array($value, ['DIR', 'SEC', 'SUB', 'PSC'])) {
                throw new Exception("value $value is not allowed");
            }
        }

        $corporates = [];
        // --- get data from db ---
        $corps = CHFiling::getDb()->fetchAssoc(
            'SELECT * FROM `ch_company_member`
              WHERE `company_id` = ' . $this->companyId . '
              AND corporate = 1 ' .
            (!empty($types) ? 'AND `type` IN ("' . implode('","', $types) . '")' : '')
        );

        // --- check type and populate appropriate object ---
        foreach ($corps as $v) {
            switch ($v['type']) {
                case 'DIR':
                    $corporate = new CorporateDirector();
                    break;
                case 'SEC':
                    $corporate = new CorporateSecretary();
                    break;
                case 'SUB':
                    $corporate = new CorporateSubscriber();
                    break;
                case 'PSC':
                    $corporate = new CompanyCorporatePsc($this->companyId, $v['company_member_id']);
                    break;
            }
            // fix becouse CH send back 0 for number of shares which is do not make any sence
            if (!$corporate instanceof CorporateSubscriber || ($v['num_shares'] != 0 && $corporate instanceof CorporateSubscriber)) {
                $corporate->setFields($v);
            }
            $corporates[] = $corporate;
        }
        // --- return all objects ---
        return $corporates;
    }

    /**
     *
     * @return Member[]
     */
    public function getMembers()
    {
        $members = [];
        $mems = CHFiling::getDb()->fetchAssoc('SELECT * FROM `ch_company_member` WHERE `company_id` = ' . $this->companyId);

        // --- check type and populate appropriate object ---
        foreach ($mems as $v) {
            switch ($v['type']) {
                case 'DIR':
                    $member = $v['corporate'] ? new CorporateDirector() : new PersonDirector();
                    break;
                case 'SEC':
                    $member = $v['corporate'] ? new CorporateSecretary() : new PersonSecretary();
                    break;
                case 'SUB':
                    $member = $v['corporate'] ? new CorporateSubscriber() : new PersonSubscriber();
                    break;
                case 'PSC':
                    $member = $v['corporate'] ? new CorporatePsc() : new PersonPsc();
                    break;
            }
            $member->setFields($v);
            $members[] = $member;
        }
        // --- return all objects ---
        return $members;
    }

    /**
     * checks if second argument contains all fields in the first argument
     *
     * @param array $compulsoryFields
     * @param array $fields
     * @return boolean
     */
    private function isComplete(array $compulsoryFields, array $fields)
    {
        // --- check if all fields are set ---
        foreach ($compulsoryFields as $v) {
            if (!isset($fields[$v]) || $fields[$v] === NULL) {
                return 0;
            }
        }
        return 1;
    }

    /**
     * return all addresses
     *
     * @return array<Address>
     */
    public function getAddresses()
    {
        $addresses = [];

        $fields = ['premise', 'street', 'post_town', 'country', 'postcode'];

        if (FUser::getSignedIn()->roleId == 1) {
            $customer = new Customer($this->customerId);
            $customerAddress = new Address();
            $customerAddress->setPremise($customer->address1);
            $customerAddress->setStreet($customer->address2);
            $customerAddress->setPostTown($customer->city);
            $customerAddress->setCounty('');
            $customerAddress->setCountry('');
            $customerAddress->setPostcode($customer->postcode);

            if ($this->isComplete($fields, $customerAddress->getFields())) {
                $addresses[] = $customerAddress;
            }
        }

        // --- check if registered office is complete
        if ($this->isComplete($fields, $this->getRegisteredOffice()->getFields())) {
            $addresses[] = $this->getRegisteredOffice();
        }
        // --- check if sail address is complete
        if ($this->isComplete($fields, $this->getSailAddress()->getFields())) {
            $addresses[] = $this->getSailAddress();
        }

        // --- addresses of all members ---
        $members = $this->getMembers();
        foreach ($members as $v) {
            $address = $v->getAddress();
            if ($this->isComplete($fields, $address->getFields())) {
                $addresses[] = $address;
            }
        }

        return $addresses;
    }

    /*
     * +----------------+
     * |    requests    |
     * +----------------+
     */

    public function syncAllData()
    {
        $this->syncPublicData();
        $this->syncPrivateData();
    }

    /**
     * this method is used for sync and import
     * insert the whole xml response from companies house - envelope as well
     *
     * @param xml $response
     */
    public function setXmlCompanyData($response)
    {
        if (Feature::featurePsc()) {
            return $this->setNewXmlCompanyData($response);
        }

        if (empty($response)) {
            return;
        }
        $response = simplexml_load_string($response);
        if (isset($response->GovTalkDetails->GovTalkErrors->Error)) {
            throw CHExceptionFactory::createChExceptionFromXmlGwError($response->GovTalkDetails->GovTalkErrors->Error);
        }

        // --- check if we received the data ---
        // --- maybe return some error ---
        if (!isset($response->Body->CompanyData)) {
            throw new Exception('No data received');
        }

        $data = $response->Body->CompanyData;

        /*
          +-------------------------------------------------------+
          |   save data into an array - prepare for the query     |
          +-------------------------------------------------------+
         */
        // --- company ---
        $company = [];
        //$company['company_number']  = (string) $data->CompanyNumber;
        $company['company_name'] = (string) $data->CompanyName;
        $company['company_category'] = (string) $data->CompanyCategory;
        $company['jurisdiction'] = (string) $data->Jurisdiction;
        $company['made_up_date'] = (string) $data->MadeUpDate;
        $company['next_due_date'] = (string) $data->NextDueDate;

        $address = [
            'Premise' => 'premise',
            'Street' => 'street',
            'Thoroughfare' => 'thoroughfare',
            'PostTown' => 'post_town',
            'County' => 'county',
            'Country' => 'country',
            'Postcode' => 'postcode',
            'CareofName' => 'care_of_name',
            'PoBox' => 'po_box'
        ];

        /* registered office address */
        foreach ($address as $k => $v) {
            $company[$v] = (isset($data->RegisteredOfficeAddress->{$k}) &&
                !empty($data->RegisteredOfficeAddress->{$k})) ? (string) $data->RegisteredOfficeAddress->{$k} : NULL;
        }

        /* sail address */
        if (isset($data->SailAddress[0]) && !empty($data->SailAddress[0])) {
            foreach ($address as $k => $v) {
                $company['sail_' . $v] = (isset($data->SailAddress->{$k}) &&
                    !empty($data->SailAddress->{$k})) ? (string) $data->SailAddress->{$k} : NULL;
            }
            /* set to NULL */
        } else {
            foreach ($address as $k => $v) {
                $company['sail_' . $v] = NULL;
            }
        }

        // SailRecords
        /*
          $sailRecords = array();
          if (isset($data->SailRecords) && !empty($data->SailRecords)) {
          foreach($data->SailRecords as $value) {
          $sailRecords[] = $value;
          }
          $company['sail_records'] = implode(';', $sailRecords);
          }
         */

        // --- sic codes ---
        //        Company House stopped sending sic codes from 01-10-2011
        //        $x = 0;
        //        foreach ($data->SICCodes[0] as $value) {
        //            // there are only 4 siccodes in database
        //            if ($x < 4) {
        //                $x++;
        //                $company["sic_code$x"] = (string) $value;
        //            }
        //        }

        /*
         * +----------------------------+
         * |    insert company table    |
         * +----------------------------+
         */
        //$company['customer_id'] = $customerId;
        //$company['authentication_code'] = $authenticationCode;
        // --- !!! company has to exist, so if you do import you need to insert customer id, etc. first ---
        CHFiling::getDb()->update('ch_company', $company, 'company_id= ' . $this->companyId);

        // --- officers ---
        $officers = [];

        $officerVal = [
            'Title' => 'title',
            'Forename' => 'forename',
            'Surname' => 'surname',
            'CorporateName' => 'corporate_name',
            'DOB' => 'dob',
            'Nationality' => 'nationality',
            'Occupation' => 'occupation',
            'CountryOfResidence' => 'country_of_residence'
        ];

        $x = 0;
        foreach ($data->Officers[0] as $off) {

            $officer = [];
            if (!empty($off)) {
                $serviceAddress = [];

                // --- secretary || director ---
                switch (strtolower($off->getName())) {
                    case 'director':
                        $officer['type'] = 'DIR';
                        break;
                    case 'secretary':
                        $officer['type'] = 'SEC';
                        break;
                    //LLP companies
                    case 'member':
                        $officer['type'] = 'DIR';
                        break;
                }

                //LLP companies
                if (isset($off->DesignatedInd)) {
                    $officer['designated_ind'] = (string) $off->DesignatedInd;
                }

                if (isset($off->Corporate)) {
                    $el = 'Corporate';
                    $officer['corporate'] = 1;
                } else {
                    $el = 'Person';
                    $officer['corporate'] = 0;
                }
                //pr2($off);exit;

                foreach ($off->{$el}[0] as $key => $value) {
                    // --- address ---
                    if ((string) $key == 'ResidentialAddress') {
                        foreach ($value[0] as $k => $v) {
                            if (isset($address[$k])) {
                                $officer['residential_' . $address[$k]] = (string) $v;
                            }
                        }
                    }
                    // --- service address ---
                    if ((string) $key == 'ServiceAddress') {
                        // --- service address is the same as registered office address ---
                        // --- so just use data from registered office address ---
                        if (isset($value->SameAsRegisteredOffice) && $value->SameAsRegisteredOffice) {
                            foreach ($data->RegisteredOfficeAddress[0] as $k => $v) {
                                $officer[$address[$k]] = (string) $v;
                            }
                            // --- service address is different ---
                        } else {
                            if (!empty($value->Address[0])) {
                                foreach ($value->Address[0] as $k => $v) {
                                    if (isset($address[$k])) {
                                        $officer[$address[$k]] = (string) $v;
                                    }
                                }
                            }
                        }
                    }
                    // --- address - corporate ---
                    if ((string) $key == 'Address') {
                        foreach ($value[0] as $k => $v) {
                            if (isset($address[$k])) {
                                $officer[$address[$k]] = (string) $v;
                            }
                        }
                    }
                    // --- person || corporate ---
                    if (isset($officerVal[$key])) {
                        if ($key == 'Forename') {
                            if (isset($officer[$officerVal[$key]])) {
                                $officer['middle_name'] = (string) $value;
                            } else {
                                $officer[$officerVal[$key]] = (string) $value;
                            }
                        } else {
                            $officer[$officerVal[$key]] = (string) $value;
                        }
                    }
                }
            }
            $officers[$x]['officer'] = $officer;
            if (isset($serviceAddress) && !empty($serviceAddress)) {
                $officers[$x]['serviceAddress'] = $serviceAddress;
            }
            $x++;
        }

        /*
         * +--------------------------------+
         * |        update database         |
         * |    officer [service address]   |
         * +--------------------------------+
         */
        CHFiling::getDb()->delete('ch_company_member', "company_id = $this->companyId");

        foreach ($officers as $value) {
            $value['officer']['company_id'] = $this->companyId;
            CHFiling::getDb()->insert('ch_company_member', $value['officer']);
        }

        // --- statement of capital ---
        $companyCapitalId = CHFiling::getDb()->fetchCol('SELECT company_capital_id FROM ch_company_capital WHERE company_id = ' . $this->companyId);
        foreach ($companyCapitalId as $id) {
            CHFiling::getDb()->delete('ch_company_capital_shares', "company_capital_id = $id");
        }
        CHFiling::getDb()->delete('ch_company_capital', "company_id = $this->companyId");
        if (isset($data->StatementOfCapital->Capital) && !empty($data->StatementOfCapital->Capital)) {
            $x = 0;
            $y = 0;
            $capital = [];
            // --- capital ---
            foreach ($data->StatementOfCapital->Capital as $value) {
                $capital[$x]['capital']['total_issued'] = (string) $value->TotalNumberOfIssuedShares;
                $capital[$x]['capital']['currency'] = (string) $value->ShareCurrency;
                $capital[$x]['capital']['total_aggregate_value'] = (string) $value->TotalAggregateNominalValue;
                // --- shares ---
                foreach ($value->Shares as $k => $v) {
                    $capital[$x]['shares'][$y]['share_class'] = (string) $v->ShareClass;
                    $capital[$x]['shares'][$y]['prescribed_particulars'] = (string) $v->PrescribedParticulars;
                    $capital[$x]['shares'][$y]['num_shares'] = (string) $v->NumShares;
                    $capital[$x]['shares'][$y]['amount_paid'] = (string) $v->AmountPaidDuePerShare;
                    $capital[$x]['shares'][$y]['amount_unpaid'] = (string) $v->AmountUnpaidPerShare;
                    $capital[$x]['shares'][$y]['nominal_value'] = (string) $v->AggregateNominalValue;
                    $y++;
                }
                $x++;
            }

            /*
             * +----------------------------+
             * |        update databse      |
             * |    statement of capital    |
             * +----------------------------+
             */
            foreach ($capital as $value) {
                $value['capital']['company_id'] = $this->companyId;
                CHFiling::getDb()->insert('ch_company_capital', $value['capital']);
                $company_capital_id = CHFiling::getDb()->lastInsertId();
                // --- shares ---
                if (isset($value['shares'])) {
                    foreach ($value['shares'] as $shares) {
                        $shares['company_capital_id'] = $company_capital_id;
                        CHFiling::getDb()->insert('ch_company_capital_shares', $shares);
                    }
                }
            }
        }

        // --- shareholdings ---
        $shareholdings = [];
        if (isset($data->Shareholdings) && !empty($data->Shareholdings)) {
            $x = 0;
            // --- shareholdings ---
            foreach ($data->Shareholdings as $value) {
                $y = 0;
                //$shareholdings[$x]['share_class'] = (string) $value->ShareClass;
                //$shareholdings[$x]['number_held'] = (string) $value->NumberHeld;
                // --- shareholders ---
                foreach ($value->Shareholders as $v) {
                    $shareholdings[$x]['shareholders'][$y]['surname'] = (string) $v->Name->Surname;
                    if (isset($v->Name->Forename)) {
                        foreach ($v->Name->Forename as $forename) {
                            if (isset($shareholdings[$x]['shareholders'][$y]['forename'])) {
                                $shareholdings[$x]['shareholders'][$y]['forename'] .= (string) $forename . ' ';
                            } else {
                                $shareholdings[$x]['shareholders'][$y]['forename'] = (string) $forename . ' ';
                            }
                        }
                    }
                    //--- share class ---
                    if (isset($value->ShareClass)) {
                        foreach ($value->ShareClass as $share_class) {
                            $shareholdings[$x]['shareholders'][$y]['share_class'] = (string) $share_class;
                        }
                    }
                    //--- number of shares per shareholder  ---
                    if (isset($value->NumberHeld)) {
                        foreach ($value->NumberHeld as $num_shares) {
                            $shareholdings[$x]['shareholders'][$y]['num_shares'] = (string) $num_shares;
                        }
                    }
                    // --- address ---
                    foreach ($v->Address[0] as $k => $v) {
                        if (isset($address[$k])) {
                            $shareholdings[$x]['shareholders'][$y][$address[$k]] = (string) $v;
                        }
                    }
                    $y++;
                }
                $x++;
            }
        }
        /*
         * +----------------------------+
         * |        update databse      |
         * |        shareholdings       |
         * +----------------------------+
         */
        //$this->db->delete('shareholder', "company_id = $companyId");
        foreach ($shareholdings as $value) {
            // --- shares ---
            if (isset($value['shareholders'])) {
                //joint shareholders
                if (array_key_exists(1, $value['shareholders'])) {
                    $surname = '';
                    foreach ($value['shareholders'] as $shareholder) {
                        if (!empty($surname)) {
                            $and = ' and ';
                        } else {
                            $and = ' ';
                        }
                        if (!isset($shareholder['forename'])) {
                            $shareholder['forename'] = '';
                        }
                        $surname .= $and . $shareholder['forename'] . ' ' . $shareholder['surname'];
                        $shareholder['forename'] = '';
                        $shareholder['company_id'] = $this->companyId;
                        $shareholder['type'] = 'SUB';
                        $shareholder['share_class'] = $shareholder['share_class'];
                        $shareholder['num_shares'] = $shareholder['num_shares'];
                    }
                    $shareholder['surname'] = $surname;
                    CHFiling::getDb()->insert('ch_company_member', $shareholder);
                } else {
                    // simple shareholders
                    foreach ($value['shareholders'] as $shareholder) {
                        $shareholder['company_id'] = $this->companyId;
                        $shareholder['type'] = 'SUB';
                        $shareholder['share_class'] = $shareholder['share_class'];
                        $shareholder['num_shares'] = $shareholder['num_shares'];

                        //dont save shareholder if number shares 0
                        if ($shareholder['num_shares'] > 0) {
                            CHFiling::getDb()->insert('ch_company_member', $shareholder);
                        }
                    }
                }
            }
        }
    }

    /**
     * this method is used for sync and import
     * insert the whole xml response from companies house - envelope as well
     *
     * @param xml $response
     */
    public function setNewXmlCompanyData($response)
    {
        if (empty($response)) {
            return;
        }
        $response = simplexml_load_string($response);
        if (isset($response->GovTalkDetails->GovTalkErrors->Error)) {
            throw CHExceptionFactory::createChExceptionFromXmlGwError($response->GovTalkDetails->GovTalkErrors->Error);
        }

        // --- check if we received the data ---
        // --- maybe return some error ---
        if (!isset($response->Body->CompanyData)) {
            throw new Exception('No data received');
        }

        $data = $response->Body->CompanyData;

        /*
          +-------------------------------------------------------+
          |   save data into an array - prepare for the query     |
          +-------------------------------------------------------+
         */
        // --- company ---
        $company = [];
        //$company['company_number']  = (string) $data->CompanyNumber;
        $company['company_name'] = (string) $data->CompanyName;
        $company['company_category'] = (string) $data->CompanyCategory;
        $company['jurisdiction'] = (string) $data->Jurisdiction;
        $company['made_up_date'] = (string) $data->MadeUpDate;
        $company['next_due_date'] = (string) $data->NextDueDate;

        $address = [
            'Premise' => 'premise',
            'Street' => 'street',
            'Thoroughfare' => 'thoroughfare',
            'PostTown' => 'post_town',
            'County' => 'county',
            'Country' => 'country',
            'Postcode' => 'postcode',
            'CareofName' => 'care_of_name',
            'PoBox' => 'po_box'
        ];

        /* registered office address */
        foreach ($address as $k => $v) {
            $company[$v] = (isset($data->RegisteredOfficeAddress->{$k}) &&
                !empty($data->RegisteredOfficeAddress->{$k})) ? (string) $data->RegisteredOfficeAddress->{$k} : NULL;
        }

        /* sail address */
        if (isset($data->SailAddress[0]) && !empty($data->SailAddress[0])) {
            foreach ($address as $k => $v) {
                $company['sail_' . $v] = (isset($data->SailAddress->{$k}) &&
                    !empty($data->SailAddress->{$k})) ? (string) $data->SailAddress->{$k} : NULL;
            }
            /* set to NULL */
        } else {
            foreach ($address as $k => $v) {
                $company['sail_' . $v] = NULL;
            }
        }

        // SailRecords
        /*
          $sailRecords = array();
          if (isset($data->SailRecords) && !empty($data->SailRecords)) {
          foreach($data->SailRecords as $value) {
          $sailRecords[] = $value;
          }
          $company['sail_records'] = implode(';', $sailRecords);
          }
         */

        // --- sic codes ---
        $x = 0;
        foreach ($data->SICCodes[0] as $value) {
            // there are only 4 siccodes in database
            if ($x < 4) {
                $x++;
                $company["sic_code$x"] = (string) $value;
            }
        }

        /*
         * +----------------------------+
         * |    insert company table    |
         * +----------------------------+
         */
        //$company['customer_id'] = $customerId;
        //$company['authentication_code'] = $authenticationCode;
        // --- !!! company has to exist, so if you do import you need to insert customer id, etc. first ---
        CHFiling::getDb()->update('ch_company', $company, 'company_id= ' . $this->companyId);

        // --- officers ---
        $officers = [];

        $officerVal = [
            'Title' => 'title',
            'Forename' => 'forename',
            'Surname' => 'surname',
            'CorporateName' => 'corporate_name',
            'DOB' => 'dob',
            'Nationality' => 'nationality',
            'Occupation' => 'occupation',
            'CountryOfResidence' => 'country_of_residence'
        ];

        $x = 0;
        foreach ($data->Officers[0] as $off) {

            $officer = [];
            if (!empty($off)) {
                $serviceAddress = [];

                // --- secretary || director ---
                switch (strtolower($off->getName())) {
                    case 'director':
                        $officer['type'] = 'DIR';
                        break;
                    case 'secretary':
                        $officer['type'] = 'SEC';
                        break;
                    //LLP companies
                    case 'member':
                        $officer['type'] = 'DIR';
                        break;
                }

                //LLP companies
                if (isset($off->DesignatedInd)) {
                    $officer['designated_ind'] = (string) $off->DesignatedInd;
                }

                if (isset($off->Corporate)) {
                    $el = 'Corporate';
                    $officer['corporate'] = 1;
                } else {
                    $el = 'Person';
                    $officer['corporate'] = 0;
                }

                foreach ($off->{$el}[0] as $key => $value) {
                    // --- address ---
                    if ((string) $key == 'ResidentialAddress') {
                        foreach ($value[0] as $k => $v) {
                            if (isset($address[$k])) {
                                $officer['residential_' . $address[$k]] = (string) $v;
                            }
                        }
                    }
                    // --- service address ---
                    if ((string) $key == 'ServiceAddress') {
                        // --- service address is the same as registered office address ---
                        // --- so just use data from registered office address ---
                        if (isset($value->SameAsRegisteredOffice) && $value->SameAsRegisteredOffice) {
                            foreach ($data->RegisteredOfficeAddress[0] as $k => $v) {
                                $officer[$address[$k]] = (string) $v;
                            }
                            // --- service address is different ---
                        } else {
                            if (!empty($value->Address[0])) {
                                foreach ($value->Address[0] as $k => $v) {
                                    if (isset($address[$k])) {
                                        $officer[$address[$k]] = (string) $v;
                                    }
                                }
                            }
                        }
                    }
                    // --- address - corporate ---
                    if ((string) $key == 'Address') {
                        foreach ($value[0] as $k => $v) {
                            if (isset($address[$k])) {
                                $officer[$address[$k]] = (string) $v;
                            }
                        }
                    }
                    // --- person || corporate ---
                    if (isset($officerVal[$key])) {
                        if ($key == 'Forename') {
                            if (isset($officer[$officerVal[$key]])) {
                                $officer['middle_name'] = (string) $value;
                            } else {
                                $officer[$officerVal[$key]] = (string) $value;
                            }
                        } else {
                            $officer[$officerVal[$key]] = (string) $value;
                        }
                    }
                }
            }
            $officers[$x]['officer'] = $officer;
            if (isset($serviceAddress) && !empty($serviceAddress)) {
                $officers[$x]['serviceAddress'] = $serviceAddress;
            }
            $x++;
        }


        /*
         * +--------------------------------+
         * |        update PSCs             |
         * +--------------------------------+
         */

        $pscVal = [
            'Title' => 'title',
            'Forename' => 'forename',
            'Surname' => 'surname',
            'CorporateName' => 'corporate_name',
            //todo: psc - legal person name column
            'LegalPersonName' => 'corporate_name',
            'DOB' => 'dob',
            'Nationality' => 'nationality',
            'CountryOfResidence' => 'country_of_residence'
        ];


        //todo: psc - sync psc level statement (+ empty data)
        $pscCompanyStatement = NULL;
        $pscs = [];
        $x = 0;
        if (isset($data->PSCs)) {
            if (isset($data->PSCs->CompanyStatement)) {
                //todo: psc - company level psc statement - delete (skip) all pscs if present?
                $pscCompanyStatement = $data->PSCs->CompanyStatement;
            }

            if (isset($data->PSCs->PSC)) {
                foreach ($data->PSCs->PSC as $pscXml) {
                    $psc = [];

                    if (isset($pscXml->PSCStatementNotification)) {
                        $psc['type'] = 'PSC';
                        $psc['psc_statement_notification'] = $pscXml->PSCStatementNotification;
                    } elseif (isset($pscXml->PSCNotification)) {
                        $pscNotificationXml = $pscXml->PSCNotification;
                        if (!empty($pscNotificationXml)) {
                            $serviceAddress = [];

                            $psc['type'] = 'PSC';

                            if (isset($pscNotificationXml->Individual)) {
                                $el = 'Individual';
                                $psc['corporate'] = 0;
                            } elseif (isset($pscNotificationXml->Corporate)) {
                                $el = 'Corporate';
                                $psc['corporate'] = 1;
                            } elseif (isset($pscNotificationXml->LegalPerson)) {
                                $el = 'LegalPerson';
                                //todo: psc - isCorporate needs to check === 0/1, or create new column
                                $psc['corporate'] = 2;
                            }

                            foreach ($pscNotificationXml->{$el}[0] as $key => $value) {
                                // --- address ---
                                if ((string) $key == 'ResidentialAddress') {
                                    foreach ($value->Address[0] as $k => $v) {
                                        if (isset($address[$k])) {
                                            $psc['residential_' . $address[$k]] = (string) $v;
                                        }
                                    }
                                }
                                // --- service address ---
                                if ((string) $key == 'ServiceAddress') {
                                    // --- service address is the same as registered office address ---
                                    // --- so just use data from registered office address ---
                                    if (isset($value->SameAsRegisteredOffice) && $value->SameAsRegisteredOffice) {
                                        foreach ($data->RegisteredOfficeAddress[0] as $k => $v) {
                                            $psc[$address[$k]] = (string) $v;
                                        }
                                        // --- service address is different ---
                                    } else {
                                        if (!empty($value->Address[0])) {
                                            foreach ($value->Address[0] as $k => $v) {
                                                if (isset($address[$k])) {
                                                    $psc[$address[$k]] = (string) $v;
                                                }
                                            }
                                        }
                                    }
                                }
                                // --- address - corporate ---
                                if ((string) $key == 'Address') {
                                    foreach ($value[0] as $k => $v) {
                                        if (isset($address[$k])) {
                                            $psc[$address[$k]] = (string) $v;
                                        }
                                    }
                                }
                                // --- person || corporate ---
                                if (isset($pscVal[$key])) {
                                    if ($key == 'Forename') {
                                        if (isset($psc[$pscVal[$key]])) {
                                            $psc['middle_name'] = (string) $value;
                                        } else {
                                            $psc[$pscVal[$key]] = (string) $value;
                                        }
                                    } else {
                                        $psc[$pscVal[$key]] = (string) $value;
                                    }
                                }
                            }

                            $nocs = [];
                            if (isset($pscNotificationXml->NatureOfControls)) {
                                foreach ($pscNotificationXml->NatureOfControls[0] as $natureOfControl) {
                                    $nocs[] = (string) $natureOfControl;
                                }
                            }

                            //sort noc
                            foreach ($nocs as $noc) {
                                if (isset(NatureOfControl::$groups[$noc])) {
                                    $psc[NatureOfControl::$groups[$noc]] = $noc;
                                }
                            }

                            if (isset($pscNotificationXml->NotificationDate)) {
                                $psc['notification_date'] = $pscNotificationXml->NotificationDate;
                            }
                        }
                    }
                    
                    if (!empty($psc)) {
                        $pscs[$x]['psc'] = $psc;
                        if (isset($serviceAddress) && !empty($serviceAddress)) {
                            $pscs[$x]['serviceAddress'] = $serviceAddress;
                        }
                        $x++;
                    }
                }
            }
        }

        /*
         * +--------------------------------+
         * |        update database         |
         * |    officer [service address]   |
         * +--------------------------------+
         */
        CHFiling::getDb()->delete('ch_company_member', "company_id = $this->companyId");

        foreach ($officers as $officer) {
            $officer['officer']['company_id'] = $this->companyId;
            CHFiling::getDb()->insert('ch_company_member', $officer['officer']);
        }

        foreach ($pscs as $psc) {
            $psc['psc']['company_id'] = $this->companyId;
            CHFiling::getDb()->insert('ch_company_member', $psc['psc']);
        }

        CHFiling::getDb()->update('ch_company', ['no_psc_reason' => $pscCompanyStatement], 'company_id= ' . $this->companyId);

        // --- statement of capital ---
        $companyCapitalId = CHFiling::getDb()->fetchCol('SELECT company_capital_id FROM ch_company_capital WHERE company_id = ' . $this->companyId);
        foreach ($companyCapitalId as $id) {
            CHFiling::getDb()->delete('ch_company_capital_shares', "company_capital_id = $id");
        }
        CHFiling::getDb()->delete('ch_company_capital', "company_id = $this->companyId");
        if (isset($data->StatementOfCapital->Capital) && !empty($data->StatementOfCapital->Capital)) {
            $x = 0;
            $y = 0;
            $capital = [];
            // --- capital ---
            foreach ($data->StatementOfCapital->Capital as $value) {
                $capital[$x]['capital']['total_issued'] = (string) $value->TotalNumberOfIssuedShares;
                $capital[$x]['capital']['currency'] = (string) $value->ShareCurrency;
                $capital[$x]['capital']['total_aggregate_value'] = (string) $value->TotalAggregateNominalValue;
                // --- shares ---
                foreach ($value->Shares as $k => $v) {
                    $capital[$x]['shares'][$y]['share_class'] = (string) $v->ShareClass;
                    $capital[$x]['shares'][$y]['prescribed_particulars'] = (string) $v->PrescribedParticulars;
                    $capital[$x]['shares'][$y]['num_shares'] = (string) $v->NumShares;
                    $capital[$x]['shares'][$y]['amount_paid'] = (string) $v->AmountPaidDuePerShare;
                    $capital[$x]['shares'][$y]['amount_unpaid'] = (string) $v->AmountUnpaidPerShare;
                    $capital[$x]['shares'][$y]['nominal_value'] = (string) $v->AggregateNominalValue;
                    $y++;
                }
                $x++;
            }

            /*
             * +----------------------------+
             * |        update databse      |
             * |    statement of capital    |
             * +----------------------------+
             */
            foreach ($capital as $value) {
                $value['capital']['company_id'] = $this->companyId;
                CHFiling::getDb()->insert('ch_company_capital', $value['capital']);
                $company_capital_id = CHFiling::getDb()->lastInsertId();
                // --- shares ---
                if (isset($value['shares'])) {
                    foreach ($value['shares'] as $shares) {
                        $shares['company_capital_id'] = $company_capital_id;
                        CHFiling::getDb()->insert('ch_company_capital_shares', $shares);
                    }
                }
            }
        }

        // --- shareholdings ---
        $shareholdings = [];
        if (isset($data->Shareholdings) && !empty($data->Shareholdings)) {
            $x = 0;
            // --- shareholdings ---
            foreach ($data->Shareholdings as $value) {
                $y = 0;
                //$shareholdings[$x]['share_class'] = (string) $value->ShareClass;
                //$shareholdings[$x]['number_held'] = (string) $value->NumberHeld;
                // --- shareholders ---
                foreach ($value->Shareholders as $v) {
                    $shareholdings[$x]['shareholders'][$y]['surname'] = (string) $v->Name->Surname;
                    if (isset($v->Name->Forename)) {
                        foreach ($v->Name->Forename as $forename) {
                            if (isset($shareholdings[$x]['shareholders'][$y]['forename'])) {
                                $shareholdings[$x]['shareholders'][$y]['forename'] .= (string) $forename . ' ';
                            } else {
                                $shareholdings[$x]['shareholders'][$y]['forename'] = (string) $forename . ' ';
                            }
                        }
                    }
                    //--- share class ---
                    if (isset($value->ShareClass)) {
                        foreach ($value->ShareClass as $share_class) {
                            $shareholdings[$x]['shareholders'][$y]['share_class'] = (string) $share_class;
                        }
                    }
                    //--- number of shares per shareholder  ---
                    if (isset($value->NumberHeld)) {
                        foreach ($value->NumberHeld as $num_shares) {
                            $shareholdings[$x]['shareholders'][$y]['num_shares'] = (string) $num_shares;
                        }
                    }
                    // --- address ---
                    foreach ($v->Address[0] as $k => $vv) {
                        if (isset($address[$k])) {
                            $shareholdings[$x]['shareholders'][$y][$address[$k]] = (string) $vv;
                        }
                    }
                    $y++;
                }
                $x++;
            }
        }

        /*
         * +----------------------------+
         * |        update databse      |
         * |        shareholdings       |
         * +----------------------------+
         */
        //$this->db->delete('shareholder', "company_id = $companyId");
        foreach ($shareholdings as $value) {
            // --- shares ---
            if (isset($value['shareholders'])) {
                //joint shareholders
                if (array_key_exists(1, $value['shareholders'])) {
                    $surname = '';
                    foreach ($value['shareholders'] as $shareholder) {
                        if (!empty($surname)) {
                            $and = ' and ';
                        } else {
                            $and = ' ';
                        }
                        if (!isset($shareholder['forename'])) {
                            $shareholder['forename'] = '';
                        }
                        $surname .= $and . $shareholder['forename'] . ' ' . $shareholder['surname'];
                        $shareholder['forename'] = '';
                        $shareholder['company_id'] = $this->companyId;
                        $shareholder['type'] = 'SUB';
                        $shareholder['share_class'] = $shareholder['share_class'];
                        $shareholder['num_shares'] = $shareholder['num_shares'];
                    }
                    $shareholder['surname'] = $surname;
                    CHFiling::getDb()->insert('ch_company_member', $shareholder);
                } else {
                    // simple shareholders
                    foreach ($value['shareholders'] as $shareholder) {
                        $shareholder['company_id'] = $this->companyId;
                        $shareholder['type'] = 'SUB';
                        $shareholder['share_class'] = $shareholder['share_class'];
                        $shareholder['num_shares'] = $shareholder['num_shares'];

                        //dont save shareholder if number shares 0
                        if ($shareholder['num_shares'] > 0) {
                            CHFiling::getDb()->insert('ch_company_member', $shareholder);
                        }
                    }
                }
            }
        }
    }

    /**
     * return new or existiong (if there's unfinished one)
     * returnOfAllotmentShares object
     *
     * @return ReturnOfAllotmentShares
     */
    public function getReturnOfAllotmentShares()
    {
        /* check if there's existing request */
        foreach ($this->getFormSubmissions() as $formSubmission) {
            if ($formSubmission->getFormIdentifier() == 'ReturnOfAllotmentShares') {
                /* pending */
                if ($formSubmission->isPending() || $formSubmission->isInternalFailure()) {
                    throw new Exception("This request is still pending, please wait until it's accepted");
                }
                /* error */
                if ($formSubmission->isError()) {
                    throw new Exception("There's a problem with your submission.");
                }
                /* return unfinished form submission */
                if ($formSubmission->getSubmissionStatus() == NULL) {
                    return FormSubmission::getFormSubmission($this, $formSubmission->getFormSubmissionId())->getForm();
                }
            }
        }

        return FormSubmission::getNewFormSubmission($this, 'ReturnOfAllotmentShares')->getForm();
    }

    /**
     * Creates new annual return form submission
     *
     * @param int $annualReturnId
     */
    public function setAnnualReturn($annualReturnId)
    {

        $this->setAnnualReturnId($annualReturnId);

        //        foreach($this->getFormSubmissions() as $k => $v) {
        //            if ($v->getFormIdentifier() == 'AnnualReturn') {
        //                /* annual return is already set */
        //                return;
        //            }
        //        }

        /* create new annual return */
        FormSubmission::getNewFormSubmission($this, 'AnnualReturn');
    }

    /**
     * return new or existiong (if there's unfinished one)
     * annualReturn object
     *
     * @return AnnualReturn
     */
    public function getAnnualReturn($exception = TRUE)
    {
        /* check if there's existing request */
        foreach ($this->getFormSubmissions() as $formSubmission) {
            if ($formSubmission->isAnnualReturnType()) {
                if ($exception) {
                    if ($formSubmission->isPending() || $formSubmission->isInternalFailure()) {
                        throw new Exception("This request is pending, please wait till it's accepted");
                    } elseif ($formSubmission->isError()) {
                        throw new Exception('Remove error');
                    }
                } else {
                    if ($formSubmission->isPending() || $formSubmission->isInternalFailure() || $formSubmission->isError()) {
                        return FormSubmission::getFormSubmission($this, $formSubmission->getFormSubmissionId())->getForm();
                    }
                }

                if ($formSubmission->getSubmissionStatus() == NULL) {
                    return FormSubmission::getFormSubmission($this, $formSubmission->getFormSubmissionId())->getForm();
                }
            }
        }

        return FormSubmission::getNewFormSubmission($this, 'AnnualReturn')->getForm();
    }

    /**
     *
     * @return string
     */
    public function getAccountingReferenceDate()
    {
        $value = $this->getData();
        $ard = $this->ard;

        if (empty($this->ard) || !(isset($value['company_details']['accounts_next_due_date']))) {
            return NULL;
        }

        $nad = $value['company_details']['accounts_next_due_date'];
        $nad = explode('-', $nad);
        $ard = explode('-', $ard);
        //company house don't provide year for account reference day so we use 'next accounts due' year;
        //Leap year check
        if ($ard[1] == 02 && $ard[0] == 29) {
            $ard[0] = 28;
        }

        //check month to know if we need decrease year for 1 or not
        if ($nad[1] - 9 <= 0) {
            $ard[2] = $nad[0] - 1;
        } else {
            $ard[2] = $nad[0];
        }
        $ard = mktime(0, 0, 0, $ard[1], $ard[0], $ard[2]);
        $ard = date('d-m-Y', $ard);
        $this->ard = $ard;
        return $this->ard;
    }

    public function getChangeNameForm()
    {
        foreach ($this->getFormSubmissions() as $k => $v) {
            if ($v->getFormIdentifier() == 'ChangeOfName' && $v->getSubmissionStatus() == NULL) {
                $formSubmission = FormSubmission::getFormSubmission($this, $v->getFormSubmissionId());
                return $formSubmission;
            }
        }
        $formSubmission = FormSubmission::getNewFormSubmission($this, 'ChangeOfName');
        return $formSubmission;
    }

    /**
     *
     * @param array $fields
     */
    public function sendChangeCompanyName($fields)
    {
        //pr($this->getChangeNameForm());exit;
        $formSubmission = $this->getChangeNameForm();
        $form = $formSubmission->getForm();
        // --- set name ---
        $form->setChangeCompanyName($fields);
        // --- send it ---
        $formSubmission->sendRequest();
    }

    /**
     *
     * @param array $fields
     */
    public function sendChangeAccountingReferenceDate($fields)
    {
        // --- create form submission - it will create new form - Change Of Accounting Reference Date ---
        $formSubmission = FormSubmission::getNewFormSubmission($this, 'ChangeAccountingReferenceDate');
        // --- get form - Change Of Accounting Reference Date ---
        $form = $formSubmission->getForm();
        // --- set date ---
        $form->setArd($this->ard);

        $form->setChangeAccountingReferenceDate($fields);
        // --- send it ---
        $formSubmission->sendRequest();
    }

    /**
     *
     * @param Address $address
     */
    public function sendChangeRegisteredOfficeAddress(Address $address)
    {
        // --- create form submission - it will create new form - change address ---
        $formSubmission = FormSubmission::getNewFormSubmission($this, 'ChangeRegisteredOfficeAddress');
        // --- get form - change address ---
        $form = $formSubmission->getForm();
        // --- set address ---
        $form->setAddress($address);
        // --- send it ---
        $formSubmission->sendRequest();
    }

    /**
     * officer resignation
     * pass array with fields -
     * corporate
     * corporate_name
     * surname
     * forename
     * dob
     *
     * @param array $fields
     */
    private function sendOfficerResignation($fields)
    {
        // --- create form submission - it will create new form - change officer ---
        $formSubmission = FormSubmission::getNewFormSubmission($this, 'OfficerResignation');
        // --- get form - change officer ---
        $form = $formSubmission->getForm();

        // --- set officer details and company type---
        $fields['companyType'] = $this->getType();
        $form->setOfficer($fields);
        // --- send it ---
        $formSubmission->sendRequest();
    }

    /**
     *
     * @param int $id
     */
    public function sendTerminationOfDirector($id)
    {
        $sql = "SELECT `type`, `corporate`, `corporate_name`, `surname`, `forename`, `middle_name`, `dob`
                FROM ch_company_member
                WHERE company_member_id = $id AND type = 'DIR'";
        if (!$result = CHFiling::getDb()->fetchRow($sql)) {
            throw new Exception('director doesn\'t exist');
        }

        $this->sendOfficerResignation($result);
    }

    /**
     *
     * @param int $id
     */
    public function sendTerminationOfSecretary($id)
    {
        $sql = "SELECT `type`, `corporate`, `corporate_name`, `surname`, `forename`, `middle_name`, `dob`
                FROM ch_company_member WHERE company_member_id = $id AND type = 'SEC'";
        if (!$result = CHFiling::getDb()->fetchRow($sql)) {
            throw new Exception('secretary doesn\'t exist');
        }

        $this->sendOfficerResignation($result);
    }

    private function sendOfficerAppointment($fields)
    {
        // --- create form submission - it will create new form - change address ---
        $formSubmission = FormSubmission::getNewFormSubmission($this, 'OfficerAppointment');
        // --- get form - change officer ---
        $form = $formSubmission->getForm();
        // --- set fields ---
        $form->setFields($fields);
        // --- send it ---
        $formSubmission->sendRequest();
    }

    public function sendAppointmentOfNaturalDirector(PersonDirector $director)
    {
        $fields = [];
        $fields = array_merge($director->getAddress()->getFields(), $director->getResidentialAddress()->getFields('residential_'), $director->getPerson()->getFields());
        $fields['authentication'] = $director->getAuthentication()->getDbFields();
        $fields['consentToAct'] = $director->hasConsentToAct();
        if ($this->getType() == 'LLP') {
            $fields['designated_ind'] = $director->getDesignatedInd();
        }
        $fields['type'] = 'DIR';
        $fields['corporate'] = 0;
        $this->sendOfficerAppointment($fields);
    }

    /**
     *
     * @param CorporateDirector $director
     */
    public function sendAppointmentOfCorporateDirector(CorporateDirector $director)
    {
        $fields = [];
        $fields = array_merge($director->getAddress()->getFields(), $director->getIdentification()->getFields(), $director->getCorporate()->getFields());
        $fields['identification_type'] = $director->getIdentification()->getTypeName();
        $fields['authentication'] = $director->getAuthentication()->getDbFields();
        $fields['consentToAct'] = $director->hasConsentToAct();
        if ($this->getType() == 'LLP') {
            $fields['designated_ind'] = $director->getDesignatedInd();
        }
        $fields['type'] = 'DIR';
        $fields['corporate'] = 1;
        $this->sendOfficerAppointment($fields);
    }

    public function sendAppointmentOfNaturalSecretary(PersonSecretary $secretary)
    {
        $fields = [];
        $fields = array_merge($secretary->getAddress()->getFields(), $secretary->getPerson()->getFields());
        $fields['authentication'] = $secretary->getAuthentication()->getDbFields();
        $fields['consentToAct'] = $secretary->hasConsentToAct();
        $fields['type'] = 'SEC';
        $fields['corporate'] = 0;
        $this->sendOfficerAppointment($fields);
    }

    public function sendAppointmentOfCorporateSecretary(CorporateSecretary $secretary)
    {
        $fields = [];
        $fields = array_merge($secretary->getAddress()->getFields(), $secretary->getIdentification()->getFields(), $secretary->getCorporate()->getFields());
        $fields['identification_type'] = $secretary->getIdentification()->getTypeName();
        $fields['authentication'] = $secretary->getAuthentication()->getDbFields();
        $fields['consentToAct'] = $secretary->hasConsentToAct();
        $fields['type'] = 'SEC';
        $fields['corporate'] = 1;
        $this->sendOfficerAppointment($fields);
    }

    /**
     *
     * @param PersonDirector $director
     * @param array $fields
     * @param array $changes set(changeName|changeAddress|changeResidentialAddress|changeOther)
     */
    public function sendChangeOfNaturalDirector(PersonDirector $director, $fields, $changes)
    {
        $newFields = [];
        $person = $director->getPerson();

        $newFields['title'] = $person->getTitle();
        $newFields['forename'] = $person->getForename();
        $newFields['middle_name'] = $person->getMiddleName();
        $newFields['surname'] = $person->getSurname();
        $newFields['dob'] = $person->getDob();
        $newFields['type'] = 'DIR';
        $newFields['corporate'] = 0;
        $newFields['companyType'] = $this->getType();

        if (in_array('changeName', $changes)) {
            $newFields['new_title'] = $fields['title'];
            $newFields['new_forename'] = $fields['forename'];
            $newFields['new_middle_name'] = $fields['middle_name'];
            $newFields['new_surname'] = $fields['surname'];
        }

        if (in_array('changeAddress', $changes)) {
            $newFields['new_premise'] = $fields['premise'];
            $newFields['new_street'] = $fields['street'];
            $newFields['new_thoroughfare'] = $fields['thoroughfare'];
            $newFields['new_post_town'] = $fields['post_town'];
            $newFields['new_county'] = $fields['county'];
            $newFields['new_postcode'] = $fields['postcode'];
            $newFields['new_country'] = $fields['country'];
        }

        if (in_array('changeResidentialAddress', $changes)) {
            $newFields['new_residential_premise'] = $fields['residential_premise'];
            $newFields['new_residential_street'] = $fields['residential_street'];
            $newFields['new_residential_thoroughfare'] = $fields['residential_thoroughfare'];
            $newFields['new_residential_post_town'] = $fields['residential_post_town'];
            $newFields['new_residential_county'] = $fields['residential_county'];
            $newFields['new_residential_postcode'] = $fields['residential_postcode'];
            $newFields['new_residential_country'] = $fields['residential_country'];
            //$newFields['new_residential_secure_address_ind'] = $fields['residential_secure_address_ind'];
        }

        if (in_array('changeOther', $changes)) {
            if ($this->getType() != 'LLP') {
                if ($person->getNationality() != $fields['nationality']) {
                    $newFields['new_nationality'] = $fields['nationality'];
                }

                if ($person->getOccupation() != $fields['occupation']) {
                    $newFields['new_occupation'] = $fields['occupation'];
                }
            }
            if ($person->getCountryOfResidence() != $fields['country_of_residence']) {
                $newFields['new_country_of_residence'] = $fields['country_of_residence'];
            }
        }
        if (in_array('changeDesignation', $changes)) {
            $newFields['designated_ind'] = $fields['designated_ind'];
            $newFields['consentToAct'] = $fields['consentToAct'];
        }

        // --- create form submission - it will create new form - change address ---
        $formSubmission = FormSubmission::getNewFormSubmission($this, 'OfficerChangeDetails');
        // --- get form - change officer ---
        $form = $formSubmission->getForm();
        // --- set fields ---
        $form->setFields($newFields);
        // --- send it ---
        $formSubmission->sendRequest();
    }

    /**
     *
     * @param CorporateDirector $director
     * @param array $fields
     * @param array $changes set(changeName|changeAddress|changeEEA)
     */
    public function sendChangeOfCorporateDirector(CorporateDirector $director, $fields, $changes)
    {
        $newFields = [];
        $corporate = $director->getCorporate();

        $newFields['corporate_name'] = $corporate->getCorporateName();
        $newFields['type'] = 'DIR';
        $newFields['corporate'] = 1;
        $newFields['companyType'] = $this->getType();

        if (in_array('changeName', $fields)) {
            $newFields['new_corporate_name'] = $fields['corporate_name'];
        }

        if (in_array('changeAddress', $changes)) {
            $newFields['new_premise'] = $fields['premise'];
            $newFields['new_street'] = $fields['street'];
            $newFields['new_thoroughfare'] = $fields['thoroughfare'];
            $newFields['new_post_town'] = $fields['post_town'];
            $newFields['new_county'] = $fields['county'];
            $newFields['new_postcode'] = $fields['postcode'];
            $newFields['new_country'] = $fields['country'];
        }

        if (in_array('changeEEA', $changes)) {
            $newFields['new_identification_type'] = $fields['eeaType'];
            $newFields['new_place_registered'] = $fields['place_registered'];
            $newFields['new_registration_number'] = $fields['registration_number'];
            $newFields['new_law_governed'] = $fields['law_governed'];
            $newFields['new_legal_form'] = $fields['legal_form'];
        }

        if (in_array('changeDesignation', $changes)) {
            $newFields['designated_ind'] = $fields['designated_ind'];
            $newFields['consentToAct'] = $fields['consentToAct'];
        }
        // --- create form submission - it will create new form - change address ---
        $formSubmission = FormSubmission::getNewFormSubmission($this, 'OfficerChangeDetails');
        // --- get form - change officer ---
        $form = $formSubmission->getForm();
        // --- set fields ---
        $form->setFields($newFields);
        // --- send it ---
        $formSubmission->sendRequest();
    }

    /**
     *
     * @param CorporateSecretary $secretary
     * @param array $fields
     * @param array $changes set(changeName|changeAddress|changeEEA)
     */
    public function sendChangeOfCorporateSecretary(CorporateSecretary $secretary, $fields, $changes)
    {
        $newFields = [];
        $corporate = $secretary->getCorporate();

        $newFields['corporate_name'] = $corporate->getCorporateName();
        $newFields['type'] = 'SEC';
        $newFields['corporate'] = 1;

        if (in_array('changeName', $fields)) {
            $newFields['new_corporate_name'] = $fields['corporate_name'];
        }

        if (in_array('changeAddress', $changes)) {
            $newFields['new_premise'] = $fields['premise'];
            $newFields['new_street'] = $fields['street'];
            $newFields['new_thoroughfare'] = $fields['thoroughfare'];
            $newFields['new_post_town'] = $fields['post_town'];
            $newFields['new_county'] = $fields['county'];
            $newFields['new_postcode'] = $fields['postcode'];
            $newFields['new_country'] = $fields['country'];
        }

        if (in_array('changeEEA', $changes)) {
            $newFields['new_identification_type'] = $fields['eeaType'];
            $newFields['new_place_registered'] = $fields['place_registered'];
            $newFields['new_registration_number'] = $fields['registration_number'];
            $newFields['new_law_governed'] = $fields['law_governed'];
            $newFields['new_legal_form'] = $fields['legal_form'];
        }

        // --- create form submission - it will create new form - change address ---
        $formSubmission = FormSubmission::getNewFormSubmission($this, 'OfficerChangeDetails');
        // --- get form - change officer ---
        $form = $formSubmission->getForm();
        // --- set fields ---
        $form->setFields($newFields);
        // --- send it ---
        $formSubmission->sendRequest();
    }

    /**
     *
     * @param PersonSecretary $secretary
     * @param array $fields
     * @param array $changes set(changeName|changeAddress)
     */
    public function sendChangeOfNaturalSecretary(PersonSecretary $secretary, $fields, $changes)
    {
        $newFields = [];
        $person = $secretary->getPerson();

        $newFields['title'] = $person->getTitle();
        $newFields['forename'] = $person->getForename();
        $newFields['middle_name'] = $person->getMiddleName();
        $newFields['surname'] = $person->getSurname();
        $newFields['type'] = 'SEC';
        $newFields['corporate'] = 0;

        if (in_array('changeName', $changes)) {
            $newFields['new_title'] = $fields['title'];
            $newFields['new_forename'] = $fields['forename'];
            $newFields['new_middle_name'] = $fields['middle_name'];
            $newFields['new_surname'] = $fields['surname'];
        }

        if (in_array('changeAddress', $changes)) {
            $newFields['new_premise'] = $fields['premise'];
            $newFields['new_street'] = $fields['street'];
            $newFields['new_thoroughfare'] = $fields['thoroughfare'];
            $newFields['new_post_town'] = $fields['post_town'];
            $newFields['new_county'] = $fields['county'];
            $newFields['new_postcode'] = $fields['postcode'];
            $newFields['new_country'] = $fields['country'];
        }

        // --- create form submission - it will create new form - change address ---
        $formSubmission = FormSubmission::getNewFormSubmission($this, 'OfficerChangeDetails');
        // --- get form - change officer ---
        $form = $formSubmission->getForm();
        // --- set fields ---
        $form->setFields($newFields);
        // --- send it ---
        $formSubmission->sendRequest();
    }

    /**
     * alias for remove
     */
    public function delete()
    {
        $this->remove();
    }

    /**
     * Pre-condition: company cannot be incorporated, if it is, exception is
     * thrown
     * removes this company and all form submissions etc. associated with it
     */
    public function remove()
    {
        if ($this->getStatus() == 'complete') {
            throw new Exception('Company is already incorporated!');
        }
        /* remove all form submissions */
        $formSubmissions = $this->getFormSubmissions();
        foreach ($formSubmissions as $v) {
            $v->remove();
        }

        /* remove this company */
        CHFiling::getDb()->delete(self::$tableName, 'company_id = ' . $this->companyId);
        $this->companyId = NULL;
    }

    /**
     * Added: Stan (30/04/2010)
     * Provides lock company
     *
     * @return void
     */
    public function lockCompany()
    {
        CHFiling::getDb()->update(self::$tableName, ['locked' => TRUE], 'company_id = ' . $this->companyId);
    }

    /**
     * Added: Stan (30/04/2010)
     * Provides unlock lock company
     *
     * @return void
     */
    public function unlockCompany()
    {
        CHFiling::getDb()->update(self::$tableName, ['locked' => FALSE], 'company_id = ' . $this->companyId);
    }

    /**
     * Added: Stan (30/04/2010)
     *
     * Returns if company is locked
     *
     * @return boolean
     */
    public function isLocked()
    {
        return (bool) $this->locked;
    }

    /**
     * @return array
     */
    public function getBarclaysRequestData()
    {
        return BarclaysCompany::getBarclaysData($this->companyId);
    }

    /**
     * @return array
     */
    public function getCardOneRequestData()
    {
        return CardOne::getBankingData($this->companyId);
    }

    /**
     * @return array
     */
    public function getHSBCRequestData()
    {
        return HSBC::getHSBCData($this->companyId);
    }

    /**
     * Added: Stan (30/04/2010)
     * Provides hide company
     *
     * @return void
     */
    public function hideCompany()
    {
        CHFiling::getDb()->update(self::$tableName, ['hidden' => TRUE], 'company_id = ' . $this->companyId);
    }

    /**
     * Added: Stan (30/04/2010)
     * Provides unhide company
     *
     * @return void
     */
    public function unhideCompany()
    {
        CHFiling::getDb()->update(self::$tableName, ['hidden' => FALSE], 'company_id = ' . $this->companyId);
    }

    /**
     * Added: Stan (30/04/2010)
     *
     * Returns if company is hidden for customer
     *
     * @return boolean
     */
    public function isHidden()
    {
        return (bool) $this->hidden;
    }

    /**
     * Added: Razvan Preda (25/05/2011)
     *
     * Returns if company is deleted
     *
     * @return boolean
     */
    public function isDeleted()
    {
        return (bool) $this->deleted;
    }

    /**
     * Added: Razvan Preda (25/05/2011)
     *
     * Update ch_company table deleted field with TRUE (Mark a company as Deleted)
     *
     * @return boolean
     */
    public function markDeleted()
    {
        CHFiling::getDb()->update(self::$tableName, ['deleted' => 1], 'company_id = ' . $this->companyId);
    }

    /**
     * Added: Razvan Preda (25/05/2011)
     *
     * Update ch_company table deleted field with FALSE
     *
     * @return boolean
     */
    public function unMarkDeleted()
    {
        CHFiling::getDb()->update(self::$tableName, ['deleted' => FALSE], 'company_id = ' . $this->companyId);
    }

    /**
     * Added: Razvan Preda (25/05/2011)
     *
     * Update ch_company table company_number field with "DELETED_companyNumber"
     *
     * @return
     */
    public function setDeletedCompanyNumber($companyNumber)
    {

        CHFiling::getDb()->update(self::$tableName, ['company_number' => 'DELETED_' . $companyNumber], 'company_id = ' . $this->companyId);
        $this->companyNumber = $companyNumber;
    }

    /**
     *
     * @param int $serviceAddressId
     */
    public function setServiceAddress($serviceAddressId)
    {
        CHFiling::getDb()->update(self::$tableName, ['service_address_id' => $serviceAddressId], 'company_id = ' . $this->companyId);
        $this->serviceAddressId = $serviceAddressId;
    }

    /**
     * retunrs FALSE if Service Address  Name id is NULL
     *
     * @return boolean
     */
    public function getServiceAddress()
    {
        return ($this->serviceAddressId == NULL) ? FALSE : $this->serviceAddressId;
    }

    /**
     * Removes annual return from the company
     *
     * @param int $id
     */
    public function removeServiceAddress()
    {
        CHFiling::getDb()->update(self::$tableName, ['service_address_id' => NULL], 'company_id = ' . $this->companyId);
    }

    public static function getCompanyByOrderId($orderid)
    {
        $result = dibi::select('*')->from('ch_company')->where('order_id=%i', $orderid)->execute()->fetch();
        return ($result === FALSE) ? FALSE : new Company($result['company_id']);
    }

    private function syncPrivateData()
    {
        if (!$this->companyNumber) {
            throw new Exception('Company number needs to be set');
        }

        if (!$this->authenticationCode) {
            throw new Exception('Authentication needs to be set');
        }

        $getData = CompanyDataRequest::getNewCompanyDataRequest(
            $this->customerId,
            $this->companyNumber,
            $this->authenticationCode
        );

        $this->setXmlCompanyData($getData->sendRequest());
    }

    public function syncPublicData()
    {
        if (!$this->companyNumber) {
            throw new Exception('Company number needs to be set');
        }

        /** @var CompaniesHouse\Data\Data $chData */
        $chData = Registry::getService(DiLocator::CH_DATA);
        $result = $chData->getCompanyDetails($this->companyNumber);

        $data = [];
        $data['company_status'] = $result->getCompanyStatus();
        $data['country_of_origin'] = $result->getCountryOfOrigin();

        $incorporationDate = $result->getIncorporationDate();
        $registrationDate = $result->getRegistrationDate();
        if ($incorporationDate) {
            $data['incorporation_date'] = $incorporationDate->format('Y-m-d');
        } elseif ($registrationDate) {
            $data['incorporation_date'] = $registrationDate->format('Y-m-d');
        }

        $accounts = $result->getAccounts();
        if (!empty($accounts)) {
            $data['accounts_ref_date'] = $accounts->getAccountRefDay() . '-' . $accounts->getAccountRefMonth();

            if ($accounts->getNextDueDate()) {
                $data['accounts_next_due_date'] = $accounts->getNextDueDate()->format('Y-m-d');
            }

            if ($accounts->getLastMadeUpDate()) {
                $data['accounts_last_made_up_date'] = $accounts->getLastMadeUpDate()->format('Y-m-d');
            }
        }


        $returns = $result->getReturns();
        if (!empty($returns)) {
            if ($returns->getNextDueDate()) {
                $data['returns_next_due_date'] = $returns->getNextDueDate()->format('Y-m-d');
            }

            if ($returns->getLastMadeUpDate()) {
                $data['returns_last_made_up_date'] = $returns->getLastMadeUpDate()->format('Y-m-d');
            }
        }

        // We have 4 fields for Sic codes
        $sicTexts = $result->getSicCodes()->getSicText();
        for ($i = 1; $i <= 4 && $i <= count($sicTexts); $i++) {
            $sicText = $sicTexts[$i - 1];

            if ($sicText != SicCodes::NONE_SUPPLIED) {
                $explodedSicCode = explode('-', $sicText);
                $sicCode = reset($explodedSicCode);
                $data["sic_code{$i}"] = trim($sicCode);
            }
        }

        CHFiling::getDb()->update(self::$tableName, $data, 'company_id= ' . $this->companyId);
    }
}
