<?php

use BankingModule\Entities\CompanyCustomer as CompanyCustomerEntity;

class HSBC
{
    const HSBC_PRODUCT = 1024;
    /**
     * retrieves ids of companies that are supposed to be sent to barclays
     * 
     * @return array ids - returns ids of the companies that haven't been sent to barclays
     */
    public static function getCompaniesIds()
    {
        return CHFiling::getDb()->fetchCol(
            'SELECT com.company_id
                FROM cms2_company_customer AS bank
                LEFT JOIN ch_hsbc AS hsbc ON hsbc.company_id = bank.companyId
                LEFT JOIN ch_company AS com ON com.company_id = bank.companyId
                LEFT JOIN cms2_customers c ON com.customer_id = c.customerId
                WHERE
                  bank.bankTypeId = "' . CompanyCustomerEntity::BANK_TYPE_HSBC. '"
                  AND hsbc.company_id IS NULL
                  AND com.company_number IS NOT NULL
                  AND c.countryId = 223
                GROUP BY com.company_id'
        );
    }

    /**
     * sends company information to HSBC bank
     * @throws exception
     */
    public static function sendCompany($id)
    {

        $data = array('company_id' => $id);
        CHFiling::getDb()->insert('ch_hsbc', $data);
        $hsbcId = CHFiling::getDb()->lastInsertId();
        
        //just for wholesale
        $companyDetails = Company::getCompany($id);
        $customerDetails = CompanyCustomer::getCompanyCustomerByCompanyId($id, CompanyCustomerEntity::BANK_TYPE_HSBC);

        $businessBankingEmailer = Registry::$emailerFactory->get(EmailerFactory::BUSINESS_BANKING);
        $businessBankingEmailer->sendCompanyToHsbc($companyDetails, $customerDetails);
        
        $saveRes = self::saveResponse($hsbcId);

    }

    /**
     * Returns true if company has already been submitted to HSBC, false
     * otherwise
     *
     * @param int $id - company id
     * @return boolean 
     */
    public static function isCompanySubmitted($id)
    {

        $id = (int) $id;
        $result = CHFiling::getDb()->fetchCol(
            '
            SELECT hsbc_id 
            FROM ch_hsbc 
            WHERE company_id = ' . $id
        );

        if (empty($result)) {
            return FALSE;
        }
        return TRUE;
    }

    /**
     * Saves response to the db.
     *
     * @param int $hsbcId - primary key from db
     */
    private static function saveResponse($hsbcId)
    {
        /* update database */
        $data = array(
            'date_sent' => new Zend_Db_Expr('NOW()'),
            'success' => 1);
        CHFiling::getDb()->update(
            'ch_hsbc', $data, 'hsbc_id = ' . $hsbcId
        );

        return $data;
    }
    
    /**
     * Returns barclays data by company id
     *
     * @param int $companyId
     * @return array
     */
    public static function getHSBCData($companyId)
    {
        $data = CHFiling::getDb()->fetchAll("SELECT * FROM `ch_hsbc` WHERE `company_id`=$companyId ORDER BY `date_sent` DESC");
        return $data;
    }

}
