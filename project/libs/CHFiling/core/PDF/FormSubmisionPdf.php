<?php

use FormSubmissionModule\Views\FormSubmissionView;

class FormSubmisionPdf
{
    /**
     * @var array
     */
    private $general;

    /**
     * @var array
     */
    private $errors;

    /**
     * @var array
     */
    private $details;

    /**
     * @param FormSubmissionView $submission
     * @param array $submissionErrors
     */
    public function __construct(FormSubmissionView $submission, $submissionErrors)
    {
        $this->general = $this->getGeneralDetails($submission);
        $this->errors = $this->getErrors($submissionErrors);
    }

    /**
     * @param FormSubmissionView $submission
     * @return array
     */
    public function getGeneralDetails(FormSubmissionView $submission)
    {
        $general[] = ['Form Submission Id', $submission->getId()];
        $general[] = ['Company Id', $submission->getCompanyId()];
        $general[] = ['Form Identifier', $submission->getFormIdentifier()];
        $general[] = ['Language', $submission->getLanguage()];
        $general[] = ['Response', $submission->getResponseMessage()];
        $general[] = ['Reject Reference', $submission->getRejectReference()];
        $general[] = ['Examiner Telephone', $submission->getExaminerTelephone()];
        $general[] = ['Examiner Comment', $submission->getExaminerComment()];
        $general[] = ['Request Date', $submission->getFirstSubmissionDate()];
        $general[] = ['Response Date', $submission->getLastSubmissionDate()];

        return $general;
    }

    /**
     * @param array $submissionErrors
     * @return array
     */
    public function getErrors($submissionErrors)
    {
        if (!empty($submissionErrors)) {
            foreach ($submissionErrors as $submissionError) {
                $error[] = ['Id', $submissionError->form_submission_error_id];
                $error[] = ['Reject code', $submissionError->reject_code];
                $error[] = ['Description', $submissionError->description];
                $error[] = ['Instance number', $submissionError->instance_number];
            }
            $this->errors = $error;
        }

        return $this->errors;
    }

    public function renderFormSubmisionPdf()
    {
        $tpdf = new AnnualReturnSummary();
        $tpdf->AddPage();

        $width = 160;
        $height = 5;
        $fontSize = 8;

        $tpdf->SetTextColor(0, 0, 0);
        $tpdf->SetFont('Arial', 'B', 14);
        $tpdf->MultiCell($width, $height, 'Form Submission Summary ');
        $tpdf->Ln();

        if (!empty($this->general)) {
            $tpdf->SetFont('Arial', 'B', 12);
            $tpdf->MultiCell($width, $height, 'Submission');
            $tpdf->Ln();
            $tpdf->SetFont('Arial', '', $fontSize);
            $tpdf->basicTable($this->general);
            $tpdf->Ln();
        }
        if (!empty($this->errors)) {
            $tpdf->SetFont('Arial', 'B', 12);
            $tpdf->MultiCell($width, $height, 'Submission Errors');
            $tpdf->Ln();
            $tpdf->SetFont('Arial', '', $fontSize);
            $tpdf->basicTable($this->errors);
            $tpdf->Ln();
        }
        if (!empty($this->details)) {
            $tpdf->SetFont('Arial', 'B', 12);
            $tpdf->MultiCell($width, $height, 'Submission details');
            $tpdf->Ln();
            $tpdf->SetFont('Arial', '', $fontSize);
            $tpdf->basicTable($this->details);
            $tpdf->Ln();
        }

        // --- send to the browser ---
        $tpdf->Output('formSubmissionSummary.pdf', 'D');
        exit;
    }

    /**
     * @param HistoryViewerModel $data
     * @param $company
     */
    public function addIncorporationSubmisionData(HistoryViewerModel $data, $company)
    {
        
        $general[] = array('Company');
        $general[] = array('Name', $company->getCompanyName());
        $general[] = array('Type', $company->getType());
        
        $office = $data->getComponentOffice();
        $general[] = array();
        $general[] = array('Registered Office');
        $general[] = array('Address 1', $office->premise);
        $general[] = array('Address 2', $office->street);
        $general[] = array('Address 3', $office->thoroughfare);
        $general[] = array('Town', $office->post_town);
        $general[] = array('County', $office->county);
        $general[] = array('Postcode', $office->postcode);
        $general[] = array('Country', $office->country);

        $directors = $data->getComponentPersons('dir');
        foreach ($directors as $key => $director) {
            if ($company->getFormSubmission($data->getFormSubmision())->getForm()->getType() == 'LLP') {
                $general[] = array();
                $general[] = array('Member ' . ($key + 1));
            } else {
                $general[] = array();
                $general[] = array('Director ' . ($key + 1));
            }
            
            //{* CORPORATE DIRECTOR *}
            if (isset($director->corporate_name)) {
                $general[] = array('Company Name', $director->corporate_name);
                $general[] = array('Authorizing Person First Name', $director->forename);
                $general[] = array('Authorizing Person Last Name', $director->surname);
                //{* PERSON DIRECTOR *}
            } else {
                $general[] = array('Title', $director->title);
                $general[] = array('First Name', $director->forename);
                $general[] = array('Middle Name', $director->middle_name);
                $general[] = array('Last Name', $director->surname);
                $general[] = array('DOB', $director->dob);
                $general[] = array('Nationality', $director->nationality);
                $general[] = array('Occupation', $director->occupation);

                $general[] = array('Residential Address','');
                $general[] = array('Address 1', $director->residential_premise);
                $general[] = array('Address 2', $director->residential_street);
                $general[] = array('Address 3', $director->residential_thoroughfare);
                $general[] = array('Town', $director->residential_post_town);
                $general[] = array('County', $director->residential_county);
                $general[] = array('Postcode', $director->residential_postcode);
                $general[] = array('Country', $director->residential_country);
                $general[] = array('Service Address','');
            }
            $general[] = array('Address 1', $director->premise);
            $general[] = array('Address 2', $director->street);
            $general[] = array('Address 3', $director->thoroughfare);
            $general[] = array('Town', $director->post_town);
            $general[] = array('County', $director->county);
            $general[] = array('Postcode', $director->postcode);
            $general[] = array('Country', $director->country);
            //{* CORPORATE DIRECTOR *}
            if (isset($director->corporate_name)) {
                //{* EEA *}
                if ($director->eeaType) {
                    $general[] = array('Type', 'EEA');
                    $general[] = array('Country Registered', $director->place_registered);
                    $general[] = array('Registration number', $director->registration_number);
                    //{* NON EEA *}
                } else {
                    $general[] = array('Type', 'Non EEA');
                    $general[] = array('Country Registered', $director->place_registered);
                    $general[] = array('Registration numbers', $director->registration_number);
                    $general[] = array('Governing law', $director->law_governed);
                    $general[] = array('Legal Form', $director->legal_form);
                }
            }
            $general[] = array('First three letters of Town of birth', $director->BIRTOWN);
            $general[] = array('Last three digits of Telephone number', $director->TEL);
            $general[] = array('First three letters of Eye colour', $director->EYE);
        }

        ////{* SHAREHOLDERS *}
        //if ($company->getOldFormSubmission($data->getFormSubmision())->getForm()->getType() != 'BYGUAR') {
        //    $general[] = array();
        //    $general[] = array('Shareholders');
        //} else {
        //    $general[] = array();
        //    $general[] = array('Members');
        //}

        $shareholders = $data->getComponentPersons('sub');
        foreach ($shareholders as $key => $shareholder) {
            if ($company->getFormSubmission($data->getFormSubmision())->getForm()->getType() != 'BYGUAR') {
                $general[] = array();
                $general[] = array('Shareholder ' . ($key + 1));
            } else {
                $general[] = array();
                $general[] = array('Member ' . ($key + 1));
            }

            if (isset($shareholder->corporate_name)) {
                $general[] = array('Company Name', $shareholder->corporate_name);
                $general[] = array('Authorizing Person First Name', $shareholder->forename);
                $general[] = array('Authorizing Person Last Name', $shareholder->surname);
            } else {
                $general[] = array('Title', $shareholder->title);
                $general[] = array('First Name', $shareholder->forename);
                $general[] = array('Middle Name', $shareholder->middle_name);
                $general[] = array('Last Name', $shareholder->surname);
            }
            $general[] = array('Address 1', $shareholder->premise);
            $general[] = array('Address 2', $shareholder->street);
            $general[] = array('Address 3', $shareholder->thoroughfare);
            $general[] = array('Town', $shareholder->post_town);
            $general[] = array('County', $shareholder->county);
            $general[] = array('Postcode', $shareholder->postcode);
            $general[] = array('Country', $shareholder->country);
            $general[] = array('First three letters of Town of birth', $shareholder->BIRTOWN);
            $general[] = array('Last three digits of Telephone number', $shareholder->TEL);
            $general[] = array('First three letters of Eye colour', $shareholder->EYE);
            if ($company->getFormSubmission($data->getFormSubmision())->getForm()->getType() != 'BYGUAR') {
                $general[] = array('Share Class', $shareholder->share_class);
                $general[] = array('Shares', $shareholder->num_shares);
                $general[] = array('Share Currency', $shareholder->currency);
                $general[] = array('Share Value', $shareholder->share_value);
            }
        }

        //{* SECRETARIES *}
        $secretaries = $data->getComponentPersons('sec');
        if (!empty($secretaries)) {
            foreach ($secretaries as $key => $secretary) {
                $general[] = array();
                $general[] = array('Secretary ' . ($key + 1));

                if (isset($secretary->corporate_name)) {
                    $general[] = array('Company Name', $secretary->corporate_name);
                    $general[] = array('Authorizing Person First Name', $secretary->forename);
                    $general[] = array('Authorizing Person Last Name', $secretary->surname);
                } else {
                    $general[] = array('Title', $secretary->title);
                    $general[] = array('First Name', $secretary->forename);
                    $general[] = array('Middle Name', $secretary->middle_name);
                    $general[] = array('Last Name', $secretary->surname);
                }
                $general[] = array('Address 1', $secretary->premise);
                $general[] = array('Address 2', $secretary->street);
                $general[] = array('Address 3', $secretary->thoroughfare);
                $general[] = array('Town', $secretary->post_town);
                $general[] = array('County', $secretary->county);
                $general[] = array('Postcode', $secretary->postcode);
                $general[] = array('Country', $secretary->country);

                //{* CORPORATE SECRETARIES *}
                if (isset($secretary->corporate_name)) {
                    //{* EEA *}
                    if ($secretary->eeaType) {
                        $general[] = array('Type', 'EEA');
                        $general[] = array('Country Registered', $secretary->place_registered);
                        $general[] = array('Registration number', $secretary->registration_number);
                        //{* NON EEA *}
                    } else {
                        $general[] = array('Type', 'Non EEA');
                        $general[] = array('Country Registered', $secretary->place_registered);
                        $general[] = array('Registration numbers', $secretary->registration_number);
                        $general[] = array('Governing law', $secretary->law_governed);
                        $general[] = array('Legal Form', $secretary->legal_form);
                    }
                }
                $general[] = array('First three letters of Town of birth', $secretary->BIRTOWN);
                $general[] = array('Last three digits of Telephone number', $secretary->TEL);
                $general[] = array('First three letters of Eye colour', $secretary->EYE);
            }
        } 
        $articles = $data->getComponentArticle();
        if (!empty($articles['article'])) {
            $general[] = array();
            $general[] = array('Articles');
            $general[] = array('Article', $articles['article']['filename']);
        }
        if (!empty($articles['support'])) {
            $general[] = array();
            $general[] = array('Suport');
            $general[] = array('Article', $articles['support']['filename']);
        }
        $this->details = $general;
    }

    public function addOtherSubmisionData($data)
    {
        if (!empty($data)) {
            foreach ($data as $key => $item) {
                $details[] = array($key, $item);
            }
            $this->details = $details;
        }
    }

    public function addAnnualReturnSubmisionData($return)
    {
        if (!empty($return)) {
            $data[] = array();
            $data[] = array('Company Info');
            $data[] = array('Company Name:', $return['company_name']);
            $data[] = array('Registration Number: ', $return['company_number']);
            $data[] = array('Company Category: ', $return['company_category']);
            $data[] = array('Made Up Date: ', $return['made_up_date']);
            if (isset($return['sic_code'])) {
                foreach ($return['sic_code'] as $k => $code) {
                    $data[] = array('Sic Code:', $code);
                }
            } else {
                $data[] = array('No Sic Code', '');
            }
            $data[] = array();
            $data[] = array('Registered Office:');
            $data[] = array('Registered Office: ', $return['reg_office']['premise']);
            $data[] = array('Street', $return['reg_office']['street']);
            $data[] = array('Thoroughfare', $return['reg_office']['thoroughfare']);
            $data[] = array('Post Town', $return['reg_office']['post_town']);
            $data[] = array('County', $return['reg_office']['county']);
            $data[] = array('Country', $return['reg_office']['country']);
            $data[] = array('Postcode', $return['reg_office']['postcode']);
            $data[] = array('Care Of Name', $return['reg_office']['care_of_name']);
            $data[] = array('Po Box', $return['reg_office']['po_box']);
            $data[] = array('Secure Address Ind', $return['reg_office']['secure_address_ind']);

            if (isset($return['sail_address']['country']) && ($return['sail_address']['country'] != 'UNDEF' && $return['sail_address']['country'] != '')) {
                $data[] = array();
                $data[] = array('Sail Address:');
                $data[] = array('Sail Address: ', $return['sail_address']['country']);
                $data[] = array('Registered Office: ', $return['sail_address']['premise']);
                $data[] = array('Street', $return['sail_address']['street']);
                $data[] = array('Thoroughfare', $return['sail_address']['thoroughfare']);
                $data[] = array('Post Town', $return['sail_address']['post_town']);
                $data[] = array('County', $return['sail_address']['county']);
                $data[] = array('Country', $return['sail_address']['country']);
                $data[] = array('Postcode', $return['sail_address']['postcode']);
                $data[] = array('Care Of Name', $return['sail_address']['care_of_name']);
                $data[] = array('Po Box', $return['sail_address']['po_box']);
                $data[] = array('Secure Address Ind', $return['sail_address']['secure_address_ind']);
            }
            $data[] = array('Members List: ', $return['members_list']);
            if (isset($return['officers'])) {
                $data[] = array();
                $data[] = array('Officers');
                foreach ($return['officers'] as $k => $officer) {
                    $data[] = array('Officer Title:', $officer['title']);
                    $data[] = array('Officer Forename: ', $officer['forename']);
                    $data[] = array('Officer Middle Name: ', $officer['middle_name']);
                    $data[] = array('Officer Surname: ', $officer['surname']);
                    $data[] = array('Corporate Name: ', $officer['corporate_name']);
                    $data[] = array('Officer Type: ', $officer['type']);
                    $data[] = array('Address: ', $officer['premise']);
                    $data[] = array('Street', $officer['street']);
                    $data[] = array('Thoroughfare', $officer['thoroughfare']);
                    $data[] = array('Post Town', $officer['post_town']);
                    $data[] = array('County', $officer['county']);
                    $data[] = array('Country', $officer['country']);
                    $data[] = array('Postcode', $officer['postcode']);
                    $data[] = array('Care Of Name', $officer['care_of_name']);
                    $data[] = array('Po Box', $officer['po_box']);
                    $data[] = array('Previous Names: ', $officer['previous_names']);
                    $data[] = array('Date of Birth: ', $officer['dob']);
                    $data[] = array('Nationality: ', $officer['nationality']);
                    $data[] = array('Occupation: ', $officer['occupation']);
                    $data[] = array('Country of Residence: ', $officer['country_of_residence']);
                    $data[] = array('Identification Type: ', $officer['identification_type']);
                    $data[] = array('Place Registered: ', $officer['place_registered']);
                    $data[] = array('Registration Number: ', $officer['registration_number']);
                    $data[] = array('Law Governed: ', $officer['law_governed']);
                    $data[] = array('Legal Form: ', $officer['legal_form']);
                    $data[] = array();
                }
            } else {
                $data[] = array('No Officers', '');
            }
            if (isset($return['shares'])) {
                $data[] = array();
                $data[] = array('Shares');
                foreach ($return['shares'] as $k => $share) {
                    $data[] = array('Share Class:', $share['share_class']);
                    $data[] = array('Prescribed Particulars: ', $share['prescribed_particulars']);
                    $data[] = array('Number of Shares: ', $share['num_shares']);
                    $data[] = array('Paid Per Share: ', $share['amount_paid']);
                    $data[] = array('Unpaid Per Share: ', $share['amount_unpaid']);
                    $data[] = array('Currency: ', $share['currency']);
                    $data[] = array('Aggregate Nom Value: ', $share['aggregate_nom_value']);
                    $data[] = array();
                }
            } else {
                $data[] = array('No Shares', '');
            }
            if (isset($return['shareholdings'])) {
                $data[] = array();
                $data[] = array('Shareholdings');
                foreach ($return['shareholdings'] as $k => $shareholding) {
                    $data[] = array('Share Class:', $shareholding['share_class']);
                    $data[] = array('Number Held:', $shareholding['number_held']);
                    foreach ($shareholding['shareholder'] as $k => $shareholder) {
                        $data[] = array('Forename: ', $shareholder['forename']);
                        $data[] = array('Middle Name: ', $shareholder['middle_name']);
                        $data[] = array('Surname: ', $shareholder['surname']);
                        if (isset($shareholder['isRemoved']) && 0 == $shareholder['isRemoved']) {
                            $data[] = array('Premise: ', $shareholder['premise']);
                            $data[] = array('Street: ', $shareholder['street']);
                            $data[] = array('Thoroughfare: ', $shareholder['thoroughfare']);
                            $data[] = array('Post Town: ', $shareholder['post_town']);
                            $data[] = array('County: ', $shareholder['county']);
                            $data[] = array('Country: ', $shareholder['country']);
                            $data[] = array('Postcode: ', $shareholder['postcode']);
                        }
                    }
                    $data[] = array();
                }
            } else {
                $data[] = array('No Shareholdings', '');
            }
        }
        $this->details = $data;
    }

}
