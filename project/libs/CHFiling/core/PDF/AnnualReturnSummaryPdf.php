<?php
require_once dirname(__FILE__) . '../../request/document/memorandum/fpdf/fpdf.php';
require_once dirname(__FILE__) . '../../request/document/memorandum/AnnualReturnSummary.php';

/**
 * Description of AnnualReturnSummaryPdf
 *
 * @author Nikolai Senkevich
 */
final class AnnualReturnSummaryPdf {

    private $pdf;
    private $company;
    private $officers;
    private $shares;
    private $shareholdings;

    public function __construct() {
        $this->pdf = new AnnualReturnSummary();
    }

    public function prepareData($return) {
        $company[] = array('Company Name:', $return['company_name']);
        $company[] = array('Registration Number: ', $return['company_number']);
        $company[] = array('Company Category: ', $return['company_category']);
        $company[] = array('Made Up Date: ', $return['made_up_date']);
        if (isset($return['sic_code'])) {
            foreach ($return['sic_code'] as $k => $code) {
                $company[] = array('Sic Code:', $code);
            }
        } else {
            $company[] = array('No Sic Code', '');
        }
        $company[] = array('Registered Office: ', $return['reg_office']['premise']);
        $company[] = array('Street', $return['reg_office']['street']);
        $company[] = array('Thoroughfare', $return['reg_office']['thoroughfare']);
        $company[] = array('Post Town', $return['reg_office']['post_town']);
        $company[] = array('County', $return['reg_office']['county']);
        $company[] = array('Country', $return['reg_office']['country']);
        $company[] = array('Postcode', $return['reg_office']['postcode']);
        $company[] = array('Care Of Name', $return['reg_office']['care_of_name']);
        $company[] = array('Po Box', $return['reg_office']['po_box']);
        $company[] = array('Secure Address Ind', $return['reg_office']['secure_address_ind']);

        if (isset($return['sail_address']['country']) && ($return['sail_address']['country'] != 'UNDEF' && $return['sail_address']['country'] != '')) {
            $company[] = array('Sail Address: ', $return['sail_address']['country']);
            $company[] = array('Registered Office: ', $return['sail_address']['premise']);
            $company[] = array('Street', $return['sail_address']['street']);
            $company[] = array('Thoroughfare', $return['sail_address']['thoroughfare']);
            $company[] = array('Post Town', $return['sail_address']['post_town']);
            $company[] = array('County', $return['sail_address']['county']);
            $company[] = array('Country', $return['sail_address']['country']);
            $company[] = array('Postcode', $return['sail_address']['postcode']);
            $company[] = array('Care Of Name', $return['sail_address']['care_of_name']);
            $company[] = array('Po Box', $return['sail_address']['po_box']);
            $company[] = array('Secure Address Ind', $return['sail_address']['secure_address_ind']);
        }
        $company[] = array('Members List: ', $return['members_list']);
        if (isset($return['officers'])) {
            foreach ($return['officers'] as $k => $officer) {
                $officers[] = array('Officer Title:', $officer['title']);
                $officers[] = array('Officer Forename: ', $officer['forename']);
                $officers[] = array('Officer Middle Name: ', $officer['middle_name']);
                $officers[] = array('Officer Surname: ', $officer['surname']);
                $officers[] = array('Corporate Name: ', $officer['corporate_name']);
                $officers[] = array('Officer Type: ', $officer['type']);
                $officers[] = array('Address: ', $officer['premise']);
                $officers[] = array('Street', $officer['street']);
                $officers[] = array('Thoroughfare', $officer['thoroughfare']);
                $officers[] = array('Post Town', $officer['post_town']);
                $officers[] = array('County', $officer['county']);
                $officers[] = array('Country', $officer['country']);
                $officers[] = array('Postcode', $officer['postcode']);
                $officers[] = array('Care Of Name', $officer['care_of_name']);
                $officers[] = array('Po Box', $officer['po_box']);
                $officers[] = array('Previous Names: ', $officer['previous_names']);
                $officers[] = array('Date of Birth: ', $officer['dob']);
                $officers[] = array('Nationality: ', $officer['nationality']);
                $officers[] = array('Occupation: ', $officer['occupation']);
                $officers[] = array('Country of Residence: ', $officer['country_of_residence']);
                $officers[] = array('Identification Type: ', $officer['identification_type']);
                $officers[] = array('Place Registered: ', $officer['place_registered']);
                $officers[] = array('Registration Number: ', $officer['registration_number']);
                $officers[] = array('Law Governed: ', $officer['law_governed']);
                $officers[] = array('Legal Form: ', $officer['legal_form']);
                $officers[] = array();
            }
        } else {
            $officers[] = array('No Officers', '');
        }
        if (isset($return['shares'])) {
            foreach ($return['shares'] as $k => $share) {
                $shares[] = array('Share Class:', $share['share_class']);
                $shares[] = array('Prescribed Particulars: ', $share['prescribed_particulars']);
                $shares[] = array('Number of Shares: ', $share['num_shares']);
                $shares[] = array('Paid Per Share: ', $share['amount_paid']);
                $shares[] = array('Unpaid Per Share: ', $share['amount_unpaid']);
                $shares[] = array('Currency: ', $share['currency']);
                $shares[] = array('Aggregate Nom Value: ', $share['aggregate_nom_value']);
                $shares[] = array();
            }
        } else {
            $shares[] = array('No Shares', '');
        }
        if (isset($return['shareholdings'])) {
            foreach ($return['shareholdings'] as $k => $shareholding) {
                $shareholdings[] = array('Share Class:', $shareholding['share_class']);
                $shareholdings[] = array('Number Held:', $shareholding['number_held']);
                foreach ($shareholding['shareholder'] as $k => $shareholder) {
                    $shareholdings[] = array('Forename: ', $shareholder['forename']);
                    $shareholdings[] = array('Middle Name: ', $shareholder['middle_name']);
                    $shareholdings[] = array('Surname: ', $shareholder['surname']);
                    if (isset($shareholder['isRemoved']) && 0 == $shareholder['isRemoved']) {
                        $shareholdings[] = array('Premise: ', $shareholder['premise']);
                        $shareholdings[] = array('Street: ', $shareholder['street']);
                        $shareholdings[] = array('Thoroughfare: ', $shareholder['thoroughfare']);
                        $shareholdings[] = array('Post Town: ', $shareholder['post_town']);
                        $shareholdings[] = array('County: ', $shareholder['county']);
                        $shareholdings[] = array('Country: ', $shareholder['country']);
                        $shareholdings[] = array('Postcode: ', $shareholder['postcode']);
                    }
                }
                $shareholdings[] = array();
            }
        } else {
            $shareholdings[] = array('No Shareholdings', '');
        }
        $this->company = $company;
        $this->officers = $officers;
        $this->shares = $shares;
        $this->shareholdings = $shareholdings;
    }

    public function createPdf() {

        $this->pdf->AddPage();


        $width = 160;
        $height = 5;
        $fontSize = 8;

        $this->pdf->SetTextColor(0, 0, 0);
        $this->pdf->SetFont('Arial', 'B', 14);
        $this->pdf->MultiCell($width, $height, 'Annual Return (AR01) - Summary ');
        $this->pdf->Ln();


        $this->pdf->SetFont('Arial', 'B', 12);
        $this->pdf->MultiCell($width, $height, 'Company Details');
        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', '', $fontSize);
        $this->pdf->basicCompanyStatutoryTable($this->company);
        $this->pdf->Ln();

        $this->pdf->SetFont('Arial', 'B', 12);
        $this->pdf->MultiCell($width, $height, 'Officers');
        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', '', $fontSize);
        $this->pdf->basicCompanyStatutoryTable($this->officers);
        $this->pdf->Ln();

        $this->pdf->SetFont('Arial', 'B', 12);
        $this->pdf->MultiCell($width, $height, 'Shares');
        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', '', $fontSize);
        $this->pdf->basicCompanyStatutoryTable($this->shares);
        $this->pdf->Ln();

        $this->pdf->SetFont('Arial', 'B', 12);
        $this->pdf->MultiCell($width, $height, 'Shareholdings');
        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', '', $fontSize);
        $this->pdf->basicCompanyStatutoryTable($this->shareholdings);
        $this->pdf->Ln();
        
        $this->pdf->SetFont('Arial', '', $fontSize);
        $this->pdf->MultiCell($width, $height, 'Annual return summary produced on ' . date("d/m/Y"));
    }

    public function printPdf() {
        $this->pdf->Output('annualReturnSummary.pdf', 'D');
        exit;
    }

}