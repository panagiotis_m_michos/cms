<?php

require_once dirname(__FILE__) . '../../request/document/memorandum/fpdf/fpdf.php';

class SubscShareCert
{

    protected $pdf;

    public function __construct()
    {
        $this->pdf = new FPDF('L');
    }

    public function createCombinedPdf($ids)
    {

        foreach ($ids as $key => $companyId) {
            $company = Company::getCompany($companyId);
            $companyData = $company->getData();
            $companyName = $company->getCompanyName();
            $companyNumber = $company->getCompanyNumber();
            $registeredOffice = $company->getRegisteredOffice()->getFields();
            $subscribers = $companyData['subscribers'];
            $num = 0;

            foreach ($subscribers as $key => $subscriber) {
                $num += 1;
                if (!empty($subscriber) && isset($subscriber['corporate_name'])) {
                    $companyIncorporation = FormSubmission::getCompanyIncorporationFormSubmission($company);
                    $subscriberObject = $companyIncorporation->getForm()->getIncCorporate($subscriber['id'], 'SUB');
                    $corp = $this->createPage($subscriberObject, $companyName, $companyNumber, $registeredOffice, TRUE, $num);
                } else {
                    $companyIncorporation = FormSubmission::getCompanyIncorporationFormSubmission($company);
                    $subscriberObject = $companyIncorporation->getForm()->getIncPerson($subscriber['id'], 'SUB');
                    $person = $this->createPage($subscriberObject, $companyName, $companyNumber, $registeredOffice, FALSE, $num);
                }
            }
        }
    }

    public function createPage($subscriberObject, $companyName, $companyNumber, $registeredOffice, $corporate, $num)
    {
        /* values */
        $allotment = $subscriberObject->getAllotmentShares();

        switch ($allotment->getCurrency()) {
            case 'GBP':
                $currency = iconv("UTF-8", "ISO-8859-1", "£");
                break;
            case 'USD':
                $currency = iconv("UTF-8", "ISO-8859-1", "$");
                break;
            default:
                $currency = $allotment->getCurrency() . ' ';
        }

        $shareClass = $currency . $allotment->getShareValue() . ' Ordinary-Shares'; /* we do only ordinary shares */

        /* subscriber's name */
        $name = '';
        if ($corporate) {
            $corporate = $subscriberObject->getCorporate();
            $name = $corporate->getCorporateName();
        } else {
            $person = $subscriberObject->getPerson();
            $title = $person->getTitle();
            if (!is_null($title) && !empty($title)) {
                $name .= $title . ' ';
            }
            $name .= $person->getForename() . ' ';
            $middleName = $person->getMiddleName();
            if (!is_null($middleName) && !empty($middleName)) {
                $name .= $middleName . ' ';
            }
            $name .= $person->getSurname();
        }

        /* subscriber's address */
        $address = array();
        $add = $subscriberObject->getAddress();
        $address[] = $add->getPremise();
        $address[] = $add->getStreet();
        $address[] = $add->getThoroughfare();
        $address[] = $add->getPostTown();
        $address[] = $add->getPostcode();
        $address[] = (isset(Address::$countries[$add->getCountry()])) ?
                Address::$countries[$add->getCountry()] : $add->getCountry();

        /* remove empty fields */
        foreach ($address as $k => $v) {
            if (is_null($v) || empty($v)) {
                unset($address[$k]);
            }
        }

        /* create pdf */

        $this->pdf->AddPage();
        $this->pdf->SetAutoPageBreak(FALSE);

        /* black border - big black cell */
        $this->pdf->SetXY(16, 9);
        $this->pdf->SetMargins(16, 9, 16);
        $this->pdf->Cell(0, 190, '', 0, 0, 'L', TRUE);

        /* white inner cell */
        $this->pdf->SetXY(18, 11);
        $this->pdf->SetMargins(18, 11, 18);
        $this->pdf->SetFillColor(255, 255, 255);
        $this->pdf->Cell(0, 186, '', 0, 0, 'L', TRUE);

        /* top boxes */
        $this->pdf->SetFont('Arial', 'B', 11);
        $this->pdf->setY(18);

        /* first cell */
        $this->pdf->SetX(34);
        $this->pdf->Cell(38, 6, 'Certificate No', 1, 0, 'C');

        /* second cell */
        $this->pdf->SetX(85);
        $this->pdf->Cell(127, 6, 'Class', 1, 0, 'C');

        /* third cell */
        $this->pdf->SetX(225);
        $this->pdf->Cell(38, 6, 'No of Shares', 1, 0, 'C');

        /* bottom boxes - grey */
        $this->pdf->SetFont('Arial', '', 11);
        $this->pdf->SetFillColor(211, 211, 211);
        $this->pdf->setY(24);

        /* first cell */
        $this->pdf->SetX(34);
        $this->pdf->Cell(38, 6, $num, 1, 0, 'C', TRUE);

        /* second cell */
        $this->pdf->SetX(85);
        $this->pdf->Cell(127, 6, $shareClass, 1, 0, 'C', TRUE);

        /* third cell */
        $this->pdf->SetX(225);
        $this->pdf->Cell(38, 6, $allotment->getNumShares(), 1, 0, 'C', TRUE);

        /* text */
        $this->pdf->SetFont('Arial', 'B', 11);
        $this->pdf->SetY(42);
        $height = 4;
        $this->pdf->Cell(0, $height, strtoupper($companyName), 0, 1, 'C');
        $this->pdf->SetFont('Arial', '', 10);
        $this->pdf->Cell(0, $height, 'Registered in United Kingdom, Number: ' . $companyNumber, 0, 1, 'C');
        $this->pdf->Ln();
        $height = 8;
        $this->pdf->Cell(0, $height, 'This is to Certify that', 0, 1, 'C');
        $this->pdf->Cell(0, $height, iconv('UTF-8', 'ISO-8859-1', $name), 0, 1, 'C');
        $this->pdf->Cell(0, $height, 'of', 0, 1, 'C');
        $this->pdf->Cell(0, $height, implode(', ', $address), 0, 1, 'C');
        $height = 4;
        $this->pdf->Cell(0, $height, 'is the Registered Holder of ' . $allotment->getNumShares() . ' fully paid Ordinary-shares of ' . $currency . $allotment->getShareValue() . ' each in the above-named Company,', 0, 1, 'C');
        $this->pdf->Cell(0, $height, 'subject to the Memorandum and Articles of Association of the said Company', 0, 1, 'C');
        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', 'I', 10);
        $this->pdf->Cell(0, $height, 'This Certificate was Authorised by:', 0, 1, 'C');
        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', 'B', 10);
        $this->pdf->SetLeftMargin(53);
        $height = 8;

        /* director, secretary - first line */
        $this->pdf->Cell(25, $height, 'Director(s)', 0, 0, 'R');
        $this->pdf->Cell(65, $height, '', 'B', 0);
        $this->pdf->Cell(10, $height, '', 0, 0);
        $this->pdf->Cell(25, $height, 'Secretary', 0, 0, 'R');
        $this->pdf->Cell(65, $height, '', 'B', 1);

        /* date - second line */
        $this->pdf->Cell(25, $height, '', 0, 0, 'R');
        $this->pdf->Cell(65, $height, '', 'B', 0);
        $this->pdf->Cell(10, $height, '', 0, 0);
        $this->pdf->Cell(25, $height, 'Date', 0, 0, 'R');
        $this->pdf->Cell(65, $height, '', 'B', 1);

        /* bottom lines */
        $this->pdf->Cell(25, $height, 'Witness', 0, 0, 'R');
        $this->pdf->Cell(165, $height, '', 'B', 1);
        $this->pdf->Cell(25, $height, 'Name', 0, 0, 'R');
        $this->pdf->Cell(165, $height, '', 'B', 1);
        $this->pdf->Cell(25, $height, 'Address', 0, 0, 'R');
        $this->pdf->Cell(165, $height, '', 'B', 1);
        $this->pdf->Cell(25, $height, '', 0, 0, 'R');
        $this->pdf->Cell(165, $height, '', 'B', 0);
        $this->pdf->Ln();
        $this->pdf->Ln();

        /* registered office address */
        $this->pdf->SetFont('Arial', '', 8);
        $this->pdf->setX(18);
        $height = 4;
        $this->pdf->Cell(0, $height, 'Registered Office, ' . implode(', ', $registeredOffice), 0, 1, 'C');
        $this->pdf->Ln();
        $this->pdf->setX(18);
        $this->pdf->Cell(0, $height, 'NOTE: No transfer of any of the above mentioned shares can be registered until this Certificate', 0, 1, 'C');
        $this->pdf->setX(18);
        $this->pdf->Cell(0, $height, 'has been deposited at the Company\'s Registered Office', 0, 1, 'C');
    }

    public function printPdf($filename = FALSE)
    {
        if ($filename) {
            $this->pdf->Output($filename, 'D');
            exit;
        }
        $this->pdf->Output('ShareCert.pdf', 'D');
        exit;
    }

}