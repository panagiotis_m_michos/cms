<?php

require_once dirname(__FILE__) . '../../request/document/memorandum/fpdf/fpdf.php';

/**
 * Description of MACoverLetter
 *
 * @author Nikolai Senkevich
 */
final class MACoverLetter
{

    private $pdf;

    const width = '160';
    const height = '5';
    const fontSize = '8';

    public function __construct()
    {
        $this->pdf = new FPDF();
        $this->pdf->AliasNbPages();
        $this->pdf->SetAutoPageBreak(0);
    }

    /**
     * returns certificate as pdf
     */
    public function createPdf($id)
    {

        $company = Company::getCompany($id);
        $customer = new Customer($company->getCustomerId());
        if ($customer->affiliateId != NULL) {
            $affiliate = new Affiliate($customer->affiliateId);
            if ($affiliate) {
                $affiliate_text = 'In partnership with: ' . $affiliate->name;
            }
        } else {
            $affiliate_text = '';
        };

        $this->pdf->AddPage();
        $this->pdf->SetLeftMargin(25);

        $this->pdf->Image(dirname(__FILE__) . '../../request/document/msg_logo2.jpg', $x = null, $y = 5, $w = 78, $h = 25, $type = '', $link = '');

        $this->pdf->SetY(35);
        $this->pdf->SetFont('Arial', 'B', self::fontSize);
        $this->pdf->SetTextColor(0, 0, 0);
        $this->pdf->MultiCell(self::width, self::height, $affiliate_text);
        $this->pdf->Ln();

        $this->pdf->SetY(43);
        $this->pdf->SetFont('Arial', '', self::fontSize);
        $this->pdf->SetTextColor(28, 141, 190);
        $this->pdf->MultiCell(self::width, 4, "145/157 St John Street | London | EC1V 4PW \n tel 020 7608 5510 | fax 020 7160 5233 | web www.madesimplegroup.com");

        $this->pdf->SetFont('Arial', '', self::fontSize);
        $this->pdf->SetTextColor(0, 0, 0);

        $this->pdf->SetY(52);
        $this->pdf->SetX(135);
        $this->pdf->Cell(60, self::height, date('j F Y'));
        $this->pdf->SetY(52);

        $this->pdf->MultiCell(self::width, self::height, $customer->firstName . ' ' . $customer->lastName);
        $this->pdf->MultiCell(self::width, self::height, $company->getCompanyName());
        $this->pdf->MultiCell(self::width, self::height, $customer->address1 . ' ' . $customer->address2);
        if ($customer->address3 != NULL) {
            $this->pdf->MultiCell(self::width, self::height, $customer->address3);
        };
        $this->pdf->MultiCell(self::width, self::height, $customer->city . ' ' . $customer->county);
        $this->pdf->MultiCell(self::width, self::height, $customer->postcode);
        $this->pdf->MultiCell(self::width, self::height, $customer->country);

        $this->pdf->Ln();

        $this->pdf->MultiCell(self::width, self::height, 'Hello ' . $customer->firstName);
        $this->pdf->Ln();

        $this->pdf->SetFont('Arial', '', self::fontSize);
        $this->pdf->MultiCell(self::width, self::height, "Please find included your bound Memorandum & Articles of Association for your newly formed company.");
        $this->pdf->Ln();

        $this->pdf->SetFont('Arial', '', self::fontSize);
        $this->pdf->MultiCell(self::width, self::height, "This will need to be kept with your statutory books for the company.");
        $this->pdf->Ln();

        $this->pdf->SetFont('Arial', '', self::fontSize);
        $this->pdf->MultiCell(self::width, self::height, "We trust you will find everything in order but please contact us directly should you have any queries.");
        $this->pdf->Ln();

        $this->pdf->SetFont('Arial', 'B', self::fontSize);
        $this->pdf->MultiCell(self::width, self::height, "The team @ Made Simple");
        $this->pdf->Ln();

        $this->pdf->SetFont('Arial', 'B', self::fontSize);
        $this->pdf->SetTextColor(0, 0, 0);
        $this->pdf->MultiCell(self::width, self::height, "WARNING:");


        $this->pdf->SetFont('Arial', '', self::fontSize);
        $this->pdf->MultiCell(self::width, self::height, "You may be contacted by other company registration agents advising that you need to create Statutory Documents to ensure your company is legal and encouraging you to buy their solution. Please note - you do not need to do this, you already have everything you need for your new company.");
        $this->pdf->Ln();
        $this->pdf->Image(dirname(__FILE__) . '../../request/document/voucher.jpg', $x = 23, $y = 203, $w = 163, $h = 60, $type = '', $link = '');

        $this->pdf->SetY(-30);
        $this->pdf->SetFont('Arial', '', self::fontSize);
        $this->pdf->SetTextColor(28, 141, 190);
        $this->pdf->MultiCell(self::width, 4, "Made Simple Group Ltd \n Registered Office: 20-22 Wenlock Road London N1 7GU \n Registered in England No: 4214713. VAT Number: 820956327", 0, 'R');
        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', 'I', 11);
        $this->pdf->MultiCell(self::width, self::height, "Making business simple ...", 0, 'R');
    }

    /**
     * returns certificate as pdf
     */
    public function createPdfClean($id)
    {

        $company = Company::getCompany($id);
        $customer = new Customer($company->getCustomerId());
        if ($customer->affiliateId != NULL) {
            $affiliate = new Affiliate($customer->affiliateId);
            if ($affiliate) {
                $affiliate_text = 'In partnership with: ' . $affiliate->name;
            }
        } else {
            $affiliate_text = '';
        };

        $this->pdf->AddPage();
        $this->pdf->SetLeftMargin(25);

        $this->pdf->SetFont('Arial', '', self::fontSize);
        $this->pdf->SetTextColor(0, 0, 0);

        $this->pdf->SetY(52);
        $this->pdf->SetX(135);
        $this->pdf->Cell(60, self::height, date('j F Y'));
        $this->pdf->SetY(52);

        $this->pdf->MultiCell(self::width, self::height, $customer->firstName . ' ' . $customer->lastName);
        $this->pdf->MultiCell(self::width, self::height, $company->getCompanyName());
        $this->pdf->MultiCell(self::width, self::height, $customer->address1 . ' ' . $customer->address2);
        if ($customer->address3 != NULL) {
            $this->pdf->MultiCell(self::width, self::height, $customer->address3);
        };
        $this->pdf->MultiCell(self::width, self::height, $customer->city . ' ' . $customer->county);
        $this->pdf->MultiCell(self::width, self::height, $customer->postcode);
        $this->pdf->MultiCell(self::width, self::height, $customer->country);

        $this->pdf->Ln();

        $this->pdf->MultiCell(self::width, self::height, 'Hello ' . $customer->firstName);
        $this->pdf->Ln();

        $this->pdf->SetFont('Arial', '', self::fontSize);
        $this->pdf->MultiCell(self::width, self::height, "Please find included your bound Memorandum & Articles of Association for your newly formed company.");
        $this->pdf->Ln();

        $this->pdf->SetFont('Arial', '', self::fontSize);
        $this->pdf->MultiCell(self::width, self::height, "This will need to be kept with your statutory books for the company.");
        $this->pdf->Ln();

        $this->pdf->SetFont('Arial', '', self::fontSize);
        $this->pdf->MultiCell(self::width, self::height, "We trust you will find everything in order but please contact us directly should you have any queries.");
        $this->pdf->Ln();

        $this->pdf->SetFont('Arial', 'B', self::fontSize);
        $this->pdf->MultiCell(self::width, self::height, "The team @ Made Simple");
        $this->pdf->Ln();

        $this->pdf->SetFont('Arial', 'B', self::fontSize);
        $this->pdf->SetTextColor(0, 0, 0);
        $this->pdf->MultiCell(self::width, self::height, "WARNING:");


        $this->pdf->SetFont('Arial', '', self::fontSize);
        $this->pdf->MultiCell(self::width, self::height, "You may be contacted by other company registration agents advising that you need to create Statutory Documents to ensure your company is legal and encouraging you to buy their solution. Please note - you do not need to do this, you already have everything you need for your new company.");
        $this->pdf->Ln();
    }

    public function createCombinedPdf($companyIds)
    {
        foreach ($companyIds as $id) {
            $this->createPdf($id);
        }
    }

    public function createCombinedPdfClean($companyIds)
    {
        foreach ($companyIds as $id) {
            $this->createPdfClean($id);
        }
    }

    public function printPdf($filename = FALSE)
    {
        if($filename){
            $this->pdf->Output($filename, 'D');
            exit;
        }
        $this->pdf->Output('CoverLetter.pdf', 'D');
        exit;
    }

}
