<?php

final class CompanySummary
{

    const TYPE_DIR = 'DIR';
    const TYPE_SHAR = 'SHAR';
    const TYPE_SEC = 'SEC';

    /**
     * Comapny details. Array can be indexed or not - it has no
     * effect.
     *
     * @var array
     */
    private $companyDetails = array();

    /**
     * Registered office. Array can be indexed or not - it has no
     * effect.
     *
     * @var array
     */
    private $registeredOffice = array();

    /**
     * Sail address. Array can be indexed or not - it has no
     * effect.
     *
     * @var array
     */
    private $sailAddress = array();

    /**
     * Directors. Array can be indexed or not - it has no
     * effect.
     *
     * @var array
     */
    private $directors = array();

    /**
     * Secretaries. Array can be indexed or not - it has no
     * effect.
     *
     * @var array
     */
    private $secretaries = array();

    /**
     * Shareholders. Array can be indexed or not - it has no
     * effect.
     *
     * @var array
     */
    private $shareholders = array();

    /**
     * Capitals. Array can be indexed or not - it has no
     * effect.
     *
     * @var array
     */
    private $capitals = array();

    /**
     * @param array $companyDetails
     * @param array $registereOffice
     * @param array $sailAddress
     * @param array $directors
     * @param array $secretaries
     * @param array $shareholders
     * @param array $capitals
     */
    private function __construct(
        array $companyDetails,
        array $registeredOffice,
        array $sailAddress,
        array $directors,
        array $secretaries,
        array $shareholders,
        array $capitals
    )
    {

        $this->companyDetails = $companyDetails;
        $this->registeredOffice = $registeredOffice;
        $this->sailAddress = $sailAddress;
        $this->directors = $directors;
        $this->secretaries = $secretaries;
        $this->shareholders = $shareholders;
        $this->capitals = $capitals;
    }

    private function getCompanyDetails()
    {
        $data[] = array('Company Name:', $this->companyDetails['company_name']);
        $data[] = array('Registration Number:', $this->companyDetails['company_number']);
        switch ($this->companyDetails['jurisdiction']) {
            case 'EW':
                $data[] = array('Registered In:', 'England and Wales');
                break;
            case 'SC':
                $data[] = array('Registered In:', 'Scotland');
                break;
            case 'WA':
                $data[] = array('Registered In:', 'Wales');
                break;
            case 'NI':
                $data[] = array('Registered In:', 'Northern Ireland');
                break;
            case 'EU':
                $data[] = array('Registered In:', 'Europe');
                break;
            case 'UK':
                $data[] = array('Registered In:', 'United Kingdom');
                break;
            case 'EN':
                $data[] = array('Registered In:', 'England');
                break;
            case 'OTHER':
                $data[] = array('Registered In:', 'Other');
                break;
        }
        $data[] = array('Company Category: ', $this->companyDetails['company_category']);
        $data[] = array('Made Up Date: ', $this->companyDetails['made_up_date']);
        $data[] = array('Company Status', $this->companyDetails['company_status']);
        $data[] = array('Country of Origin: ', $this->companyDetails['country_of_origin']);
        $data[] = array('Incorporation Date:', $this->companyDetails['incorporation_date']);
        $data[] = array('Accounting Reference Date: ', $this->companyDetails['accounts_ref_date']);
        $data[] = array('Last Accounts Made Up Date:', $this->companyDetails['accounts_last_made_up_date']);
        $data[] = array('Next Accounts Due:', $this->companyDetails['accounts_next_due_date']);
        $data[] = array('Last Confirmation Statement Made Up To:', $this->companyDetails['returns_last_made_up_date']);
        $data[] = array('Next Confirmation Statement Due:', $this->companyDetails['returns_next_due_date']);
        if (isset($this->companyDetails['sic_code'])) {
            foreach ($this->companyDetails['sic_code'] as $k => $code) {
                $data[] = array('Sic Code:', $code);
            }
        } else {
            $data[] = array('No Sic Code', '');
        }
        $data[] = array('Registered Office: ', $this->registeredOffice['premise']);
        $data[] = array('Street', $this->registeredOffice['street']);
        $data[] = array('Thoroughfare', $this->registeredOffice['thoroughfare']);
        $data[] = array('Post Town', $this->registeredOffice['post_town']);
        $data[] = array('County', $this->registeredOffice['county']);
        $data[] = array('Country', $this->registeredOffice['country']);
        $data[] = array('Postcode', $this->registeredOffice['postcode']);
        $data[] = array('Care Of Name', $this->registeredOffice['care_of_name']);
        $data[] = array('Po Box', $this->registeredOffice['po_box']);

        if (isset($this->sailAddress['country']) && ($this->sailAddress['country'] != 'UNDEF' && $this->sailAddress['country'] != '')) {
            $data[] = array('Sail Address: ', $this->sailAddress['country']);
            $data[] = array('Registered Office: ', $this->sailAddress['premise']);
            $data[] = array('Street', $this->sailAddress['street']);
            $data[] = array('Thoroughfare', $this->sailAddress['thoroughfare']);
            $data[] = array('Post Town', $this->sailAddress['post_town']);
            $data[] = array('County', $this->sailAddress['county']);
            $data[] = array('Country', $this->sailAddress['country']);
            $data[] = array('Postcode', $this->sailAddress['postcode']);
            $data[] = array('Care Of Name', $this->sailAddress['care_of_name']);
            $data[] = array('Po Box', $this->sailAddress['po_box']);
        }
        foreach ($data as $k => $v) {
            if (empty($v[1]) || empty($v[1])) {
                unset($data[$k]);
            }
        }

        return $data;
    }

    /**
     * @param array $persons
     * returns director, secretory or shareholder
     */
    private function getPersonDetails($persons, $type)
    {

        if (isset($persons) && !empty($persons)) {
            foreach ($persons as $k => $person) {
                $dirAdditionalInfo = array();
                if (!empty($person['corporate_name'])) {
                    $data[] = array('Corporate Name: ', $person['corporate_name']);
                } else {
                    $data[] = array('Officer Title:', $person['title']);
                    if (!empty($type) && $type == self::TYPE_DIR) {
                        $dirAdditionalInfo[] = array('DOB', $person['dob']);
                        $dirAdditionalInfo[] = array('Nationality', $person['nationality']);
                        $dirAdditionalInfo[] = array('Occupation', $person['occupation']);
                        $dirAdditionalInfo[] = array('Country of residence', $person['country_of_residence']);
                    }
                }
                $data[] = array('Officer Forename: ', $person['forename']);
                $data[] = array('Officer Middle Name: ', $person['middle_name']);
                $data[] = array('Officer Surname: ', $person['surname']);
                $data = array_merge($data, $dirAdditionalInfo);
                $data[] = array('Address: ', $person['premise']);
                $data[] = array('Street', $person['street']);
                $data[] = array('Thoroughfare', $person['thoroughfare']);
                $data[] = array('Post Town', $person['post_town']);
                $data[] = array('County', $person['county']);
                $data[] = array('Country', $person['country']);
                $data[] = array('Postcode', $person['postcode']);
                $data[] = array('Care Of Name', $person['care_of_name']);
                if ($persons == $this->shareholders) {
                    $data[] = array('Share Class', $person['share_class']);
                    $data[] = array('Number Shares', $person['num_shares']);
                }
                foreach ($data as $k => $v) {
                    if (empty($v[1]) || empty($v[1])) {
                        unset($data[$k]);
                    }
                }
                $data[] = array(' ', ' ');
            }
            array_pop($data);
        } else {
            $data[] = array('No Officers', ' ');
        }

        return $data;
    }

    /**
     * returns capital and shares
     */
    private function getCapitalDetails()
    {
        if (isset($this->capitals) && !empty($this->capitals)) {
            foreach ($this->capitals as $k => $capitals) {
                $data[] = array('Total Issued:', $capitals['total_issued']);
                $data[] = array('Currency:', $capitals['currency']);
                $data[] = array('Total Aggregate Value:', $capitals['total_aggregate_value']);
                $shares = $this->getShares($capitals['shares']);
                $data = array_merge($data, $shares);

                unset($data['shares']);
            }
        } else {
            $data[] = array('No Capitals', ' ');
        }

        return $data;
    }

    /**
     * @param array $shares
     * returns certificate as pdf
     */
    private function getShares($shares)
    {
        if (isset($shares) && !empty($shares)) {
            foreach ($shares as $k => $share) {
                $data[] = array('  ', '  ');
                $data[] = array('Share Class:', $share['share_class']);
                $data[] = array('Prescribed Particulars: ', $share['prescribed_particulars']);
                $data[] = array('Number of Shares: ', $share['num_shares']);
                $data[] = array('Paid Per Share: ', $share['amount_paid']);
                $data[] = array('Unpaid Per Share: ', $share['amount_unpaid']);
            }
        } else {
            $data[] = array('No Shares', ' ');
        }

        return $data;
    }

    /**
     * @param array $companyDetails
     * @param array $registereOffice
     * @param array $sailAddress
     * @param array $directors
     * @param array $secretaries
     * @param array $shareholders
     * @param array $capitals
     */
    public static function getCompanySummary(
        array $companyDetails,
        array $registeredOffice,
        array $sailAddress,
        array $directors,
        array $secretaries,
        array $shareholders,
        array $capitals
    )
    {
        return new CompanySummary(
            $companyDetails,
            $registeredOffice,
            $sailAddress,
            $directors,
            $secretaries,
            $shareholders,
            $capitals
        );
    }

    /**
     * returns certificate as pdf
     */
    public function outputCompanySummaryPdf()
    {
        $pdf = $this->getCompanySummaryPdf();
        $pdf->Output('CompanySummary.pdf', 'D');
        exit;
    }

    /**
     * @return string
     */
    public function getCompanySummaryPdfString()
    {
        $pdf = $this->getCompanySummaryPdf();
        return $pdf->Output('', 'S');
    }

    /**
     * @return AnnualReturnSummary
     */
    private function getCompanySummaryPdf()
    {
        $tpdf = new AnnualReturnSummary();
        $tpdf->AddPage();

        $width = 160;
        $height = 5;
        $fontSize = 8;

        $tpdf->SetTextColor(0, 0, 0);
        $tpdf->SetFont('Arial', 'B', 14);
        $tpdf->MultiCell($width, $height, 'Company Summary ');
        $tpdf->Ln();

        $tpdf->SetFont('Arial', 'B', 12);
        $tpdf->MultiCell($width, $height, 'Company Details');
        $tpdf->Ln();
        $tpdf->SetFont('Arial', '', $fontSize);
        $tpdf->basicCompanyStatutoryTable($this->getCompanyDetails());
        $tpdf->Ln();

        $tpdf->SetFont('Arial', 'B', 12);
        $tpdf->MultiCell($width, $height, 'Directors');
        $tpdf->Ln();
        $tpdf->SetFont('Arial', '', $fontSize);
        $tpdf->basicCompanyStatutoryTable($this->getPersonDetails($this->directors, self::TYPE_DIR));
        $tpdf->Ln();

        $tpdf->SetFont('Arial', 'B', 12);
        $tpdf->MultiCell($width, $height, 'Secretaries');
        $tpdf->Ln();
        $tpdf->SetFont('Arial', '', $fontSize);
        $tpdf->basicCompanyStatutoryTable($this->getPersonDetails($this->secretaries, self::TYPE_SEC));
        $tpdf->Ln();

        $tpdf->SetFont('Arial', 'B', 12);
        $tpdf->MultiCell($width, $height, 'Shareholders');
        $tpdf->Ln();
        $tpdf->SetFont('Arial', '', $fontSize);
        $tpdf->basicCompanyStatutoryTable($this->getPersonDetails($this->shareholders, self::TYPE_SHAR));
        $tpdf->Ln();

        $tpdf->SetFont('Arial', 'B', 12);
        $tpdf->MultiCell($width, $height, 'Capitals');
        $tpdf->Ln();
        $tpdf->SetFont('Arial', '', $fontSize);
        //$tpdf->basicTable($this->getCapitalDetails());
        $tpdf->basicCompanyStatutoryTable($this->getCapitalDetails());
        $tpdf->Ln();

        $tpdf->SetFont('Arial', '', $fontSize);
        $tpdf->MultiCell($width, $height, 'Company summary produced on ' . date("d/m/Y"));

        return $tpdf;
    }
}
