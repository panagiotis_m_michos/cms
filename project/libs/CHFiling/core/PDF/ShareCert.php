<?php

require_once dirname(__FILE__).'../../request/document/memorandum/fpdf/fpdf.php';

/**
 * Description of ShareCert
 *
 * @author Nikolai Senkevich
 */
final class ShareCert {

    /**
     * Certificate number, for now it's number "1" all the time
     *
     * @var string
     */
    private $certNumber;

    /**
     *
     * @var PersonSubscriber|CorporateSubscriber
     */
    private $subscriber;

    /**
     *
     * @var string
     */
    private $companyName;

    /**
     *
     * @var string
     */
    private $companyNumber;

    /**
     * Registered office address. Array can be indexed or not - it has no
     * effect.
     *
     * @var array
     */
    private $registeredOffice = array();

    /**
     * Indicates if this cert is for corporate subscriber or person
     *
     * @var boolean
     */
    private $corporate;

    /**
     *
     * @param PersonSubscriber|CorporateSubscriber $subscriber
     * @param string $companyName
     * @param string $companyNumber
     * @param array $registereOffice
     * @param boolean corporate indicates if subscriber is person or corporate
     */
    private function  __construct($subscriber, $companyName, $companyNumber,
            array $registereOffice, $corporate, $certNumber = 1) {

        $this->certNumber       = $certNumber;
        $this->subscriber       = $subscriber;
        $this->companyName      = $companyName;
        $this->companyNumber    = $companyNumber;
        /* remove empty fields */
        foreach($registereOffice as $k => $v) {
            if (is_null($v) || empty($v)) {
                unset($registereOffice[$k]);
            }
        }
        $this->registeredOffice = $registereOffice;
        $this->corporate        = $corporate;

    }

    /**
     *
     * @param PersonSubscriber $subscriber
     * @param string $companyName
     * @param string $companyNumber
     * @param array $registereOffice
     * @return ShareCert
     */
    public static function getPersonCertificate(PersonSubscriber $subscriber,
            $companyName, $companyNumber, array $registereOffice, $certNumber = 1) {

        return new ShareCert($subscriber, $companyName, $companyNumber,
                $registereOffice, false, $certNumber);
    }

    /**
     *
     * @param CorporateSubscriber $subscriber
     * @param string $companyName
     * @param string $companyNumber
     * @param array $registereOffice
     * @return ShareCert
     */
    public static function getCorporateCertificate(CorporateSubscriber $subscriber,
            $companyName, $companyNumber, array $registereOffice, $certNumber = 1) {

        return new ShareCert($subscriber, $companyName, $companyNumber,
                $registereOffice, true, $certNumber);
    }

    /**
     * returns certificate as pdf
     */
    public function getPdfCertificate($pdfName = null, $dest = null) {

        /* values */
        $allotment = $this->subscriber->getAllotmentShares();

        switch ($allotment->getCurrency()) {
            case 'GBP':
                $currency = iconv("UTF-8", "ISO-8859-1", "£");
                break;
            case 'USD':
                $currency = iconv("UTF-8", "ISO-8859-1", "$");
                break;
            default:
                $currency = $allotment->getCurrency() . ' ';
        }

        $shareClass = $currency . $allotment->getShareValue() . ' Ordinary-Shares'; /* we do only ordinary shares */

        /* subscriber's name */
        $name = '';
        if ($this->corporate) {
            $corporate = $this->subscriber->getCorporate();
            $name = $corporate->getCorporateName();
        } else {
            $person = $this->subscriber->getPerson();
            $title = $person->getTitle();
            if (!is_null($title) && !empty($title)) {
                $name .= $title . ' ';
            }
            $name .= $person->getForename() . ' ';
            $middleName = $person->getMiddleName();
            if (!is_null($middleName) && !empty($middleName)) {
                $name .= $middleName . ' ';
            }
            $name .= $person->getSurname();
        }

        /* subscriber's address */
        $address = array();
        $add = $this->subscriber->getAddress();
        $address[] = $add->getPremise();
        $address[] = $add->getStreet();
        $address[] = $add->getThoroughfare();
        $address[] = $add->getPostTown();
        $address[] = $add->getPostcode();
        $address[] = (isset(Address::$countries[$add->getCountry()])) ?
                Address::$countries[$add->getCountry()] : $add->getCountry();

        /* remove empty fields */
        foreach($address as $k => $v) {
            if (is_null($v) || empty($v)) {
                unset($address[$k]);
            }
        }

        /* create pdf */
        $pdf = new FPDF('L');
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(false);

        /* black border - big black cell */
        $pdf->SetXY(16, 9);
        $pdf->SetMargins(16, 9, 16);
        $pdf->Cell(0, 190, '', 0, 0, 'L', true);

        /* white inner cell */
        $pdf->SetXY(18, 11);
        $pdf->SetMargins(18, 11, 18);
        $pdf->SetFillColor(255, 255, 255);
        $pdf->Cell(0, 186, '', 0, 0, 'L', true);

        /* top boxes */
        $pdf->SetFont('Arial', 'B', 11);
        $pdf->setY(18);

        /* first cell */
        $pdf->SetX(34);
        $pdf->Cell(38, 6, 'Certificate No', 1, 0, 'C');

        /* second cell */
        $pdf->SetX(85);
        $pdf->Cell(127, 6, 'Class', 1, 0, 'C');

        /* third cell */
        $pdf->SetX(225);
        $pdf->Cell(38, 6, 'No of Shares', 1, 0, 'C');

        /* bottom boxes - grey */
        $pdf->SetFont('Arial', '', 11);
        $pdf->SetFillColor(211, 211, 211);
        $pdf->setY(24);

        /* first cell */
        $pdf->SetX(34);
        $pdf->Cell(38, 6, $this->certNumber, 1, 0, 'C', true);

        /* second cell */
        $pdf->SetX(85);
        $pdf->Cell(127, 6, $shareClass, 1, 0, 'C', true);

        /* third cell */
        $pdf->SetX(225);
        $pdf->Cell(38, 6, $allotment->getNumShares(), 1, 0, 'C', true);

        /* text */
        $pdf->SetFont('Arial', 'B', 11);
        $pdf->SetY(42);
        $height = 4;

        $pdf->Cell(0, $height, strtoupper($this->companyName), 0, 1, 'C');

        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(0, $height, 'Registered in United Kingdom, Number: ' . $this->companyNumber, 0, 1, 'C');
        $pdf->Ln();

        $height = 8;

        $pdf->Cell(0, $height, 'This is to Certify that', 0, 1, 'C');
        $pdf->Cell(0, $height, iconv('UTF-8', 'ISO-8859-1',$name), 0, 1, 'C');
        $pdf->Cell(0, $height, 'of', 0, 1, 'C');
        $pdf->Cell(0, $height, implode(', ', $address), 0, 1, 'C');

        $height = 4;

        $pdf->Cell(0, $height, 'is the Registered Holder of ' . $allotment->getNumShares() . ' fully paid Ordinary-shares of ' . $currency . $allotment->getShareValue() . ' each in the above-named Company,', 0, 1, 'C');
        $pdf->Cell(0, $height, 'subject to the Memorandum and Articles of Association of the said Company', 0, 1, 'C');
        $pdf->Ln();

        $pdf->SetFont('Arial', 'I', 10);
        $pdf->Cell(0, $height, 'This Certificate was Authorised by:', 0, 1, 'C');
        $pdf->Ln();
        $pdf->Ln();

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetLeftMargin(53);
        $height = 8;

        /* director, secretary - first line */
        $pdf->Cell(25, $height, 'Director(s)', 0, 0, 'R');
        $pdf->Cell(65, $height, '', 'B', 0);

        $pdf->Cell(10, $height, '', 0, 0);

        $pdf->Cell(25, $height, 'Secretary', 0, 0, 'R');
        $pdf->Cell(65, $height, '', 'B', 1);

        /* date - second line */
        $pdf->Cell(25, $height, '', 0, 0, 'R');
        $pdf->Cell(65, $height, '', 'B', 0);

        $pdf->Cell(10, $height, '', 0, 0);

        $pdf->Cell(25, $height, 'Date', 0, 0, 'R');
        $pdf->Cell(65, $height, '', 'B', 1);

        /* bottom lines */
        $pdf->Cell(25, $height, 'Witness', 0, 0, 'R');
        $pdf->Cell(165, $height, '', 'B', 1);

        $pdf->Cell(25, $height, 'Name', 0, 0, 'R');
        $pdf->Cell(165, $height, '', 'B', 1);

        $pdf->Cell(25, $height, 'Address', 0, 0, 'R');
        $pdf->Cell(165, $height, '', 'B', 1);

        $pdf->Cell(25, $height, '', 0, 0, 'R');
        $pdf->Cell(165, $height, '', 'B', 0);

        $pdf->Ln();
        $pdf->Ln();

        /* registered office address */
        $pdf->SetFont('Arial', '', 8);
        $pdf->setX(18);
        $height = 4;

        $pdf->Cell(0, $height, 'Registered Office, ' . implode(', ', $this->registeredOffice), 0, 1, 'C');

        $pdf->Ln();

        $pdf->setX(18);
        $pdf->Cell(0, $height, 'NOTE: No transfer of any of the above mentioned shares can be registered until this Certificate', 0, 1, 'C');
        $pdf->setX(18);
        $pdf->Cell(0, $height, 'has been deposited at the Company\'s Registered Office', 0, 1, 'C');

        if ($pdfName == null) {
            $pdf->Output();
        } else {
            return $pdf->Output($pdfName, $dest);
        }
    }

}
