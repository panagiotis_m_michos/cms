<?php

class BarclaysSoletraderAll {

    /** @var int company id from database */
    private $id;

    // stan (12/01/2010): changed to public - needed in Barclays::sendCompany();
    
    /** @var array holds the data to be sent to barclays */
    public $fields;

    /** unique id for submission */
    private $partnerRef;

    /** @param int $id company id - from database */
    public function  __construct($id) {

        /* company, customer, form submission and company incorporation tables */
    	try {
	        $data = CHFiling::getDb()->fetchRow(
	            'SELECT *
                FROM cms2_barclays_soletrader
	            WHERE barclaysSoletraderId = ' . $id);

    	} catch (Zend_Db_Statement_Exception $e) {
            throw new Exception($e->getMessage());
    	}

        if (empty($data)) {
            throw new Exception("Nothing returned from db for id " . $id);
        }
    	
       
        $this->fields['soletrader']['CompanyName'] = $data['companyName'];
        $this->fields['soletrader']['EstablishedDate'] = $data['establishedDate'];
        $this->fields['soletrader']['PreferredContactDate'] = 0; //$data['preferredContactDate'];
        //$this->fields['soletrader']['wholesalerId'] = $wholesalerId;
        //$this->fields['soletrader']['companyId'] = $companyId;
        $this->fields['soletrader']['LeadContactPersonEmail'] = $data['email'];
        $this->fields['soletrader']['LeadContactPersonTitle'] = $data['titleId'];
        $this->fields['soletrader']['LeadContactPersonForename'] = $data['firstName'];
        $this->fields['soletrader']['LeadContactPersonSurname'] = $data['lastName'];
        $this->fields['soletrader']['Premise'] = $data['address1'];
        $this->fields['soletrader']['Street'] = $data['address2'];
        $this->fields['soletrader']['Thoroughfare'] = $data['address3'];
        $this->fields['soletrader']['PostTown'] = $data['city'];
        $this->fields['soletrader']['Country'] = $data['county'];
        $this->fields['soletrader']['Postcode'] = $data['postcode'];
        $this->fields['soletrader']['countryId'] = $data['countryId'];
        $this->fields['soletrader']['LeadContactPersonMainTelephone'] = $data['phone'];
        $this->fields['soletrader']['LeadContactPersonAdditionalTelephone'] = $data['additionalPhone'];
        
        
        /* set customer fields */
        if (!is_null($data['titleId'])) {
            if (isset(Customer::$titles[$data['titleId']])) {
                $this->fields['soletrader']['LeadContactPersonTitle'] = Customer::$titles[$data['titleId']];
            } else {
                $this->fields['soletrader']['LeadContactPersonTitle'] = null;
            }
        } else {
            $this->fields['soletrader']['LeadContactPersonTitle'] = null;
        }
        $this->fields['soletrader']['LeadContactPersonForename']  = $data['firstName'];
        $this->fields['soletrader']['LeadContactPersonSurname']   = $data['lastName'];
        $this->fields['soletrader']['PreferredContactDate']   =
                (isset($data['preferredContactDate']) && $data['preferredContactDate'] != "0000-00-00")
                ? $data['preferredContactDate']
                : date('Y-m-d');
        
        // validation: preg_replace("/[^0-9]/","", $data['phone']);  preg_replace('/\s*/m', '', $data['phone']);
        $phone = preg_replace("/[^0-9+,]/","", $data['phone']);
        $phone = explode(",", $phone);
        $phone = substr($phone[0], 0, 50);
        $this->fields['soletrader']['LeadContactPersonMainTelephone'] = $phone;

        if (isset($data['additionalPhone']) && !is_null($data['additionalPhone'])&& !empty($data['additionalPhone'])) {
            $phone2 = preg_replace("/[^0-9+,]/","", $data['additionalPhone']);
            $phone2 = explode(",", $phone2);
            $phone2 = substr($phone2[0], 0, 50);
            $this->fields['soletrader']['LeadContactPersonAdditionalTelephone'] = $phone2;
        } else {
            $this->fields['soletrader']['LeadContactPersonAdditionalTelephone'] = $phone;
        }
        
        $this->fields['soletrader']['LeadContactPersonEmail']     = $data['email'];
        /* address */
        $this->fields['soletrader']['address']['Premise']     = $data['address1'];
        $this->fields['soletrader']['address']['Street']      = $data['address2'];
        $this->fields['soletrader']['address']['Thoroughfare']= $data['address3'];
        $this->fields['soletrader']['address']['PostTown']    = $data['city'];
        $this->fields['soletrader']['address']['County']      = $data['county'];
        if (isset(Customer::$countries[$data['countryId']])) {
            $this->fields['soletrader']['address']['Country'] = Customer::$countries[$data['countryId']];
        } else {
            $this->fields['soletrader']['address']['Country'] = '';
        }
        //$this->fields['soletrader']['address']['CountryType'] = 'UK';
        $this->fields['soletrader']['address']['Postcode']    = $data['postcode'];
        

        /* compay id from db */
        $this->id = $id;
        //pr($this->fields);exit;
    }

   /**
     * Checks if company can be submitted to barclays.
     *
     * @param int $id
     * @return boolean
     */
//    public static function canBeSubmitted($id) {
//
//        $id = (int) $id;
//
//        /* check if company has CompanyIncorporation accepted form submission */
//    	try {
//	        $data = CHFiling::getDb()->fetchRow('
//                SELECT form_submission_id
//                FROM ch_form_submission
//	            WHERE company_id = ' . $id . '
//	            AND form_identifier = \'CompanyIncorporation\'
//	            AND response = \'ACCEPT\'');
//    	} catch (Zend_Db_Statement_Exception $e) {
//            throw new Exception($e->getMessage());
//    	}
//
//        return (empty($data)) ? false : true;
//    }

    /**
     *
     * @param int $partnerRef 
     */
    public function setPartnerRef($partnerRef) {
        $this->partnerRef = $partnerRef;
    }

    /**
     * return true if company has all complusory fields, so we ca generate xml 
     * otherwise false is returned
     *
     * @return boolean 
     */
    private function isComplete() {
        return (!isset($this->partnerRef) || empty($this->partnerRef)) ? 0 : 1;
    }

    /**
     * returns xml that can be sent to barclays, or throws exception if the 
     * xml is not complete
     * 
     * @return string xml representation of the company
     */
    public function getXml() {

        if (!$this->isComplete()) {
            throw new Exception('Company does not contain all compulsory fields');
        }

        $xml = simplexml_load_string("<MsgCompany></MsgCompany>");
        /* our parner id */
        $xml->addChild("PartnerID");
        /* string 4-5 chars */
        $xml->PartnerID = "cms1";

        /* for tracking purposes */
        $xml->addChild("Channel_ID");
        /* string 0-5 chars */
        $xml->Channel_ID = "cms";

        $xml->addChild("PartnerRef");
        /* partner ref 1-8 chars */
        $xml->PartnerRef = 'S'.$this->partnerRef;

        /* customer */
        if (!is_null($this->fields['soletrader']['LeadContactPersonTitle'])) {
            $xml->addChild("LeadContactPersonTitle");
            $xml->LeadContactPersonTitle = $this->fields['soletrader']['LeadContactPersonTitle'];
        }
        $xml->addChild("LeadContactPersonForename");
        $xml->LeadContactPersonForename = $this->fields['soletrader']['LeadContactPersonForename'];
        $xml->addChild("LeadContactPersonSurname");
        $xml->LeadContactPersonSurname = $this->fields['soletrader']['LeadContactPersonSurname'];

     
        $xml->addChild("PreferredContactDate");
        $xml->PreferredContactDate = $this->fields['soletrader']['PreferredContactDate'];
        $xml->addChild("LeadContactPersonMainTelephone");
        $xml->LeadContactPersonMainTelephone = $this->fields['soletrader']['LeadContactPersonMainTelephone'];

        
        if (!is_null($this->fields['soletrader']['LeadContactPersonAdditionalTelephone'])) {
            $xml->addChild("LeadContactPersonAdditionalTelephone");
            $xml->LeadContactPersonAdditionalTelephone = $this->fields['soletrader']['LeadContactPersonAdditionalTelephone'];
        }
        $xml->addChild("LeadContactPersonEmail");
        $xml->LeadContactPersonEmail = $this->fields['soletrader']['LeadContactPersonEmail'];

        /* customer's address */
        $xml->addChild("LeadContactPersonAddress");
        foreach($this->fields['soletrader']['address'] as $k => $v) {
            if (!is_null($v) && !empty($v)) {
                $xml->LeadContactPersonAddress->addChild($k);
                $xml->LeadContactPersonAddress->$k = $v;
            }
        }

        /* company details */
        $xml->addChild("CompanyName");
        $xml->CompanyName = $this->fields['soletrader']['CompanyName'];
        //$xml->addChild("EstablishedDate");
        //$xml->EstablishedDate = $this->fields['soletrader']['EstablishedDate'];
        return $xml->asXml();
    }

    
    /**
     * Returns barclays data by company id
     *
     * @param int $companyId
     * @return array
     */
    static public function getBarclaysData($barclaysSoletraderId)
    {
    	$data = CHFiling::getDb()->fetchAll("SELECT * FROM `ch_barclays_soletrader` WHERE `barclaysSoletraderId`=$barclaysSoletraderId ORDER BY `dateSent` DESC");
    	return $data;
    }
    

}
