<?php

class BarclaysTransport
{

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $code;

    /**
     * @param string $url where the request is going to be sent
     * @param string $id partner id
     * @param string $code access code
     */
    public function __construct($url, $id, $code)
    {
        $this->url = $url;
        $this->id = $id;
        $this->code = $code;
    }

    /**
     * @param string $msg xml request
     * @param boolean $test indicates if submission is test submission
     * @param string $type
     * @return string response
     */
    public function getResponse($msg, $test, $type = NULL)
    {
        /* set mode - test or submission */
        $mode = ($test) ? 't' : 's';

        /* create request to be sent */
        $request = array();
        $request['id'] = $this->id;
        $request['code'] = $this->code;
        $request['mode'] = $mode;
        $request['doc'] = $msg;
        if (isset($type)) {
            $request['type'] = $type;
        };
        //pr($request);exit;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);
        $response = curl_exec($ch);
        curl_close($ch);

        $response = mb_convert_encoding($response, 'UTF-8', 'UTF-8');
        return $response;
    }
}
