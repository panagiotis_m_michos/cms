<?php

class BarclaysCompany
{

    /** @var int company id from database */
    private $id;

    // stan (12/01/2010): changed to public - needed in Barclays::sendCompany();
    /** @var array holds the data to be sent to barclays */
    public $fields;

    /** unique id for submission */
    private $partnerRef;

    /** @param int $id company id - from database */
    public function  __construct($id, $wholesale = FALSE) 
    {

        if ($wholesale) {
                $sql = "LEFT JOIN (cms2_company_customer AS cu)
	            ON (co.company_id = cu.companyId AND cu.bankTypeId = 'BARCLAYS')";
        } else {
                $sql = "LEFT JOIN (cms2_customers AS cu)
	            ON (co.customer_id = cu.customerId)";
        }
       
        /* company, customer, form submission and company incorporation tables */
        try {
            $data = CHFiling::getDb()->fetchRow(
                'SELECT
	            co.company_id, co.company_number, co.company_name,
	            co.incorporation_date, '.($wholesale ? 'cu.preferredContactDate,' : '').' cu.titleId,
	            cu.email, cu.firstName, cu.lastName, cu.address1, cu.address2,
	            cu.address3, cu.city, cu.county, cu.postcode, cu.countryId, cu.phone, cu.additionalPhone,  cu.consent,
	            fs.form_submission_id, ci.type, ci.country_of_incorporation,
	            ci.premise, ci.street, ci.thoroughfare, ci.post_town, 
	            ci.county AS reg_county, ci.country, ci.postcode AS reg_postcode,
	            ci.articles
                FROM ch_company AS co ' . $sql . '
	            
	            LEFT JOIN (ch_form_submission AS fs)
	            ON fs.company_id = co.company_id
	            
	            LEFT JOIN (ch_company_incorporation AS ci)
	            ON ci.form_submission_id = fs.form_submission_id
	            
	            WHERE co.company_id = ' . $id . ' 
	            AND co.incorporation_date IS NOT NULL
	            AND fs.date_cancelled IS NULL
	            AND fs.form_identifier = \'CompanyIncorporation\'
	            AND fs.response = \'ACCEPT\''
            );
        } catch (Zend_Db_Statement_Exception $e) {
            throw new Exception($e->getMessage());
        }

        if (empty($data)) {
            throw new Exception("Nothing returned from db for id " . $id);
        }
  
        /* form submission id */
        $formSubmissionId = $data['form_submission_id'];

        /* set company fields */
        $this->fields['company']['CompanyNumber'] = $data['company_number'];
        $this->fields['company']['CompanyName']   = $data['company_name'];
        $this->fields['company']['IncorporationDate'] = $data['incorporation_date'];
        $this->fields['company']['CompanyType']   = $data['type'];
        $this->fields['company']['CountryOfIncorporation'] = $data['country_of_incorporation'];
        /* registered office address */
        $this->fields['company']['address']['Premise']      = $data['premise'];
        $this->fields['company']['address']['Street']       = $data['street'];
        $this->fields['company']['address']['Thoroughfare'] = $data['thoroughfare'];
        $this->fields['company']['address']['PostTown']     = $data['post_town'];
        $this->fields['company']['address']['County']       = $data['reg_county'];
        $this->fields['company']['address']['Country']      = $data['country'];
        $this->fields['company']['address']['Postcode']     = $data['reg_postcode'];
        $this->fields['company']['Articles'] = $data['articles'];
        
        
        /* set customer fields */
        if (!is_null($data['titleId'])) {
            if (isset(Customer::$titles[$data['titleId']])) {
                $this->fields['customer']['LeadContactPersonTitle'] = Customer::$titles[$data['titleId']];
            } else {
                $this->fields['customer']['LeadContactPersonTitle'] = NULL;
            }
        } else {
            $this->fields['customer']['LeadContactPersonTitle'] = NULL;
        }
        $this->fields['customer']['LeadContactPersonForename']  = $data['firstName'];
        $this->fields['customer']['LeadContactPersonSurname']   = $data['lastName'];
        $this->fields['customer']['PreferredContactDate']   =
                (isset($data['preferredContactDate']) && $data['preferredContactDate'] != "0000-00-00")
                ? $data['preferredContactDate']
                : date('Y-m-d');
        
        // validation: preg_replace("/[^0-9]/","", $data['phone']);  preg_replace('/\s*/m', '', $data['phone']);
        $phone = preg_replace("/[^0-9+,]/", "", $data['phone']);
        $phone = explode(",", $phone);
        $phone = substr($phone[0], 0, 50);
        $this->fields['customer']['LeadContactPersonMainTelephone'] = $phone;

        if (isset($data['additionalPhone']) && !is_null($data['additionalPhone'])&& !empty($data['additionalPhone'])) {
            $phone2 = preg_replace("/[^0-9+,]/", "", $data['additionalPhone']);
            $phone2 = explode(",", $phone2);
            $phone2 = substr($phone2[0], 0, 50);
            $this->fields['customer']['LeadContactPersonAdditionalTelephone'] = $phone2;
        } else {
            $this->fields['customer']['LeadContactPersonAdditionalTelephone'] = $phone;
        }
        
        /* SourceType: A for Affiliate, D for Direct, T for 3rd Party, and P for Accountant (A,D,T,P) */
        /* Have the person gives consent for a referral (Y,N) */

        if($data['consent'] == 1) {
            $this->fields['customer']['SourceType']       = 'D';
            $this->fields['customer']['ConsentGiven']     = 'Y';
        }else{
            $this->fields['customer']['SourceType']       = 'T';
            $this->fields['customer']['ConsentGiven']     = 'Y';
        }
   
        $this->fields['customer']['LeadContactPersonEmail']     = $data['email'];
        /* address */
        $this->fields['customer']['address']['Premise']     = $data['address1'];
        $this->fields['customer']['address']['Street']      = $data['address2'];
        $this->fields['customer']['address']['Thoroughfare']= $data['address3'];
        $this->fields['customer']['address']['PostTown']    = $data['city'];
        $this->fields['customer']['address']['County']      = $data['county'];
        if (isset(Customer::$countries[$data['countryId']])) {
            $this->fields['customer']['address']['Country'] = Customer::$countries[$data['countryId']];
        } else {
            $this->fields['customer']['address']['Country'] = '';
        }
        if ($this->fields['customer']['address']['Country'] == 'United Kingdom') {
            $this->fields['customer']['address']['CountryType'] = 'UK';
            $this->fields['customer']['address']['Postcode']    = $data['postcode'];
        } else {
            $this->fields['customer']['address']['CountryType'] = 'NON-UK';
            $this->fields['customer']['address']['InternationalPostcode'] = $data['postcode'];
        }

        /* appointments, subscribers or guarantors */
        $data = CHFiling::getDb()->fetchAll(
            '
            SELECT * FROM ch_incorporation_member
            WHERE form_submission_id = ' . $formSubmissionId
        );
        
        $x = 0; // appointments increment
        $i = 0; // subscribres/guarantors increment
        foreach($data as $v) {
            /* fields for an appointment */
            $appFields = array();

            /* set fields */
            if ($v['corporate']) {
                /* corporate - for all appointments the same fields */
                $appFields['corporate'] = TRUE;
                $appFields['Forename'] = $v['forename'];
                $appFields['Surname']  = $v['surname'];
                $appFields['CorporateName'] = $v['corporate_name'];
                /* address */
                $appFields['address']['Premise']     = $v['premise'];
                $appFields['address']['Street']      = $v['street'];
                $appFields['address']['Thoroughfare']= $v['thoroughfare'];
                $appFields['address']['PostTown']    = $v['post_town'];
                $appFields['address']['County']      = $v['county'];
                $appFields['address']['Country']     = $v['country'];
                $appFields['address']['Postcode']    = $v['postcode'];

                if ($v['type'] == 'DIR') {
                    $appFields['type'] = 'dir';
                    $this->fields['appointments'][$x] = $appFields;
                    $x++;
                } elseif($v['type'] == 'SEC') {
                    $appFields['type'] = 'sec';
                    $this->fields['appointments'][$x] = $appFields;
                    $x++;
                } elseif($v['type'] == 'SUB') {
                    if ($this->fields['company']['CompanyType'] == 'BYGUAR' 
                        || $this->fields['company']['CompanyType'] == 'BYGUAREXEMPT'
                    ) {

                        $appFields['AmountGuaranteed'] = 1;
                        $this->fields['guarantors'][$i] = $appFields;
                    } else {
                        $appFields['shares']['ShareClass'] = $v['share_class'];
                        $appFields['shares']['NumShares'] = $v['num_shares'];
                        $appFields['shares']['AmountPaidDuePerShare'] = 0.00;
                        $appFields['shares']['AmountUnpaidPerShare'] = $v['share_value'] * $v['num_shares'];
                        $appFields['shares']['ShareCurrency'] = $v['currency'];
                        $appFields['shares']['ShareValue'] = $v['share_value'];
                        $this->fields['subscribers'][$i] = $appFields;
                    }
                    $i++;
                }
            } else {
                /* person */
                $appFields['corporate'] = FALSE;
                $appFields['Title']   = $v['title'];
                $appFields['Forename']= $v['forename'];
                $appFields['Surname'] = $v['surname'];
                /* address */
                $appFields['address']['Premise']      = $v['premise'];
                $appFields['address']['Street']       = $v['street'];
                $appFields['address']['Thoroughfare'] = $v['thoroughfare'];
                $appFields['address']['PostTown']     = $v['post_town'];
                $appFields['address']['County']       = $v['county'];
                $appFields['address']['Country']      = $v['country'];
                $appFields['address']['Postcode']     = $v['postcode'];

                if ($v['type'] == 'DIR') {
                    /* director - additional fields in case of person */
                    $appFields['type'] = 'dir';
                    $appFields['DOB']         = $v['dob'];
                    $appFields['Nationality'] = $v['nationality'];
                    $appFields['Occupation']  = $v['occupation'];
                    $appFields['CountryOfResidence'] = $v['country_of_residence'];
                    /* residential address */
                    $appFields['res_address']['Premise']      = $v['residential_premise'];
                    $appFields['res_address']['Street']       = $v['residential_street'];
                    $appFields['res_address']['Thoroughfare'] = $v['residential_thoroughfare'];
                    $appFields['res_address']['PostTown']     = $v['residential_post_town'];
                    $appFields['res_address']['County']       = $v['residential_county'];
                    $appFields['res_address']['Country']      = $v['residential_country'];
                    $appFields['res_address']['Postcode']     = $v['residential_postcode'];
                    if (is_null($v['residential_secure_address_ind']) || $v['residential_secure_address_ind'] == 0) {
                        $appFields['res_address']['SecureAddressInd'] = 0;
                    } else {
                        $appFields['res_address']['SecureAddressInd'] = 1;
                    }

                    $this->fields['appointments'][$x] = $appFields;
                    $x++;
                } elseif($v['type'] == 'SEC') {
                    $appFields['type'] = 'sec';
                    $this->fields['appointments'][$x] = $appFields;
                    $x++;
                } elseif($v['type'] == 'SUB') {
                    if ($this->fields['company']['CompanyType'] == 'BYGUAR' 
                        || $this->fields['company']['CompanyType'] == 'BYGUAREXEMPT'
                    ) {

                        $appFields['AmountGuaranteed'] = 1;
                        $this->fields['guarantors'][$i] = $appFields;
                    } else {
                        $appFields['shares']['ShareClass'] = $v['share_class'];
                        $appFields['shares']['NumShares'] = $v['num_shares'];
                        $appFields['shares']['AmountPaidDuePerShare'] = 0.00;
                        $appFields['shares']['AmountUnpaidPerShare'] = $v['share_value'] * $v['num_shares'];
                        $appFields['shares']['ShareCurrency'] = $v['currency'];
                        $appFields['shares']['ShareValue'] = $v['share_value'];
                        $this->fields['subscribers'][$i] = $appFields;
                    }
                    $i++;
                }
            }
        }

        /* compay id from db */
        $this->id = $id;
        //pr($this->fields);exit;
    }

    /**
     * Checks if company can be submitted to barclays.
     *
     * @param int $id
     * @return boolean
     */
    public static function canBeSubmitted($id) 
    {

        $id = (int) $id;

        /* check if company has CompanyIncorporation accepted form submission */
        try {
            $data = CHFiling::getDb()->fetchRow(
                '
                SELECT form_submission_id
                FROM ch_form_submission
	            WHERE company_id = ' . $id . '
	            AND date_cancelled IS NULL
	            AND form_identifier = \'CompanyIncorporation\'
	            AND response = \'ACCEPT\''
            );
        } catch (Zend_Db_Statement_Exception $e) {
            throw new Exception($e->getMessage());
        }

        return (empty($data)) ? FALSE : TRUE;
    }

    /**
     *
     * @param int $partnerRef 
     */
    public function setPartnerRef($partnerRef) 
    {
        $this->partnerRef = $partnerRef;
    }

    /**
     * return true if company has all complusory fields, so we ca generate xml 
     * otherwise false is returned
     *
     * @return boolean 
     */
    private function isComplete() 
    {
        return (!isset($this->partnerRef) || empty($this->partnerRef)) ? 0 : 1;
    }

    /**
     * returns xml that can be sent to barclays, or throws exception if the 
     * xml is not complete
     * 
     * @return string xml representation of the company
     */
    public function getXml() 
    {

        if (!$this->isComplete()) {
            throw new Exception('Company does not contain all compulsory fields');
        }

        $xml = simplexml_load_string("<MsgCompany></MsgCompany>");
        /* our parner id */
        $xml->addChild("PartnerID");
        /* string 4-5 chars */
        $xml->PartnerID = "cms1";

        /* for tracking purposes */
        $xml->addChild("Channel_ID");
        /* string 0-5 chars */
        $xml->Channel_ID = "cms";

        $xml->addChild("PartnerRef");
        /* partner ref 1-8 chars */
        $xml->PartnerRef = $this->partnerRef;
        
        
        $xml->addChild("SourceType");
        $xml->SourceType = $this->fields['customer']['SourceType'];
        
        $xml->addChild("ConsentGiven");
        $xml->ConsentGiven = $this->fields['customer']['ConsentGiven'];

        
        /* customer */
        if (!is_null($this->fields['customer']['LeadContactPersonTitle'])) {
            $xml->addChild("LeadContactPersonTitle");
            $xml->LeadContactPersonTitle = $this->fields['customer']['LeadContactPersonTitle'];
        }
        $xml->addChild("LeadContactPersonForename");
        $xml->LeadContactPersonForename = $this->fields['customer']['LeadContactPersonForename'];
        $xml->addChild("LeadContactPersonSurname");
        $xml->LeadContactPersonSurname = $this->fields['customer']['LeadContactPersonSurname'];

     
        $xml->addChild("PreferredContactDate");
        $xml->PreferredContactDate = $this->fields['customer']['PreferredContactDate'];
        $xml->addChild("LeadContactPersonMainTelephone");
        $xml->LeadContactPersonMainTelephone = $this->fields['customer']['LeadContactPersonMainTelephone'];

        
        if (!is_null($this->fields['customer']['LeadContactPersonAdditionalTelephone'])) {
            $xml->addChild("LeadContactPersonAdditionalTelephone");
            $xml->LeadContactPersonAdditionalTelephone = $this->fields['customer']['LeadContactPersonAdditionalTelephone'];
        }
        $xml->addChild("LeadContactPersonEmail");
        $xml->LeadContactPersonEmail = $this->fields['customer']['LeadContactPersonEmail'];

        /* customer's address */
        $xml->addChild("LeadContactPersonAddress");
        foreach($this->fields['customer']['address'] as $k => $v) {
            if (!is_null($v) && !empty($v)) {
                $xml->LeadContactPersonAddress->addChild($k);
                $xml->LeadContactPersonAddress->$k = $v;
            }
        }

        /* company details */
        $xml->addChild("CompanyNumber");
        $xml->CompanyNumber = $this->fields['company']['CompanyNumber'];
        $xml->addChild("CompanyName");
        $xml->CompanyName = $this->fields['company']['CompanyName'];
        $xml->addChild("IncorporationDate");
        $xml->IncorporationDate = $this->fields['company']['IncorporationDate'];
        $xml->addChild("CompanyType");
        $xml->CompanyType = $this->fields['company']['CompanyType'];
        $xml->addChild("CountryOfIncorporation");
        $xml->CountryOfIncorporation = $this->fields['company']['CountryOfIncorporation'];

        /* company's address */
        $xml->addChild("RegisteredOfficeAddress");
        foreach($this->fields['company']['address'] as $k => $v) {
            if (!is_null($v) && !empty($v)) {
                $xml->RegisteredOfficeAddress->addChild($k);
                $xml->RegisteredOfficeAddress->$k = $v;
            }
        }

        /* articles */
        $xml->addChild("Articles");
        $xml->Articles = $this->fields['company']['Articles'];

        /* appointments */
        $x = 0;
        foreach($this->fields['appointments'] as $appointment) {
            $corporate = ($appointment['corporate']) ? 'Corporate' : 'Person';
            unset($appointment['corporate']);
            $type = ($appointment['type'] == 'dir') ? 'Director' : 'Secretary';
            unset($appointment['type']);

            $xml->addChild('Appointment');
            $xml->Appointment[$x]->addChild($type);
            $xml->Appointment[$x]->$type->addChild($corporate);
            /* all fields are already sorted and keys equals to the tag names */
            foreach($appointment as $k => $v) {
                if (!is_null($v) && !empty($v)) {
                    if ($k == 'address') {
                        /* address */
                        $xml->Appointment[$x]->$type->$corporate->addChild('Address');
                        foreach($v as $addressLine => $address) {
                            if (!is_null($address) && !empty($address)) {
                                $xml->Appointment[$x]->$type->$corporate->Address->addChild($addressLine);
                                $xml->Appointment[$x]->$type->$corporate->Address->$addressLine = $address;
                            }
                        }
                    } elseif($k == 'res_address') {
                        /* residential address */
                        $xml->Appointment[$x]->$type->$corporate->addChild('ResidentialAddress');
                        foreach($v as $addressLine => $address) {
                            if (!is_null($address)) {
                                $xml->Appointment[$x]->$type->$corporate->ResidentialAddress->addChild($addressLine);
                                $xml->Appointment[$x]->$type->$corporate->ResidentialAddress->$addressLine = $address;
                            }
                        }
                    } else {
                        $xml->Appointment[$x]->$type->$corporate->addChild($k);
                        $xml->Appointment[$x]->$type->$corporate->$k = $v;
                    }
                }
            }
            $x++;
        }

        /* subscribers | guarantors */
        $x = 0;
        if(!empty($this->fields['subscribers'])) {
            foreach($this->fields['subscribers'] as $subscriber) {
                $corporate = ($subscriber['corporate']) ? 'Corporate' : 'Person';
                unset($subscriber['corporate']);

                $xml->addChild('Subscribers');
                $xml->Subscribers[$x]->addChild($corporate);
                /* all fields are already sorted and keys equals to the tag names */
                foreach($subscriber as $k => $v) {
                    if (!is_null($v) && !empty($v)) {
                        if ($k == 'address') {
                            /* address */
                            $xml->Subscribers[$x]->$corporate->addChild('Address');
                            foreach($v as $addressLine => $address) {
                                if (!is_null($address) && !empty($address)) {
                                    $xml->Subscribers[$x]->$corporate->Address->addChild($addressLine);
                                    $xml->Subscribers[$x]->$corporate->Address->$addressLine = $address;
                                }
                            }
                        } elseif ($k == 'shares') {
                            /* shares */
                            $xml->Subscribers[$x]->addChild('Shares');
                            foreach($v as $shareLine => $shareValue) {
                                $xml->Subscribers[$x]->Shares->addChild($shareLine);
                                $xml->Subscribers[$x]->Shares->$shareLine = $shareValue;
                            }
                        } else {
                            $xml->Subscribers[$x]->$corporate->addChild($k);
                            $xml->Subscribers[$x]->$corporate->$k = $v;
                        }
                    }
                }
                $x++;
            }
        }
        
        return $xml->asXml();
    }

    
    /**
     * Returns barclays data by company id
     *
     * @param int $companyId
     * @return array
     */
    public static function getBarclaysData($companyId)
    {
        $data = CHFiling::getDb()->fetchAll("SELECT * FROM `ch_barclays` WHERE `company_id`=$companyId ORDER BY `date_sent` DESC");
        return $data;
    }
    

}
