<?php

use BankingModule\Entities\CompanyCustomer as CompanyCustomerEntity;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class Barclays
{
    /**
     * partner id
     *
     * @var string
     */
    private static $id = 'jqd9zKjuVCbr';

    /**
     * access code
     *
     * @var string
     */
    private static $code = '896698362948';

    /**
     * url where to send request
     *
     * @var string
     */
    private static $url = 'https://www.lead2value.com/gateway/';

    /**
     * url where to send request
     *
     * @var string
     */
    private static $type = 'soletrader';

    /**
     * retrieves ids of companies that are supposed to be sent to barclays
     *
     * @return array ids - returns ids of the companies that haven't been sent to barclays
     */
    public static function getCompaniesIds()
    {
        // companies with error as well (resubmittin error companies)
        /*
        return CHFiling::getDb()->fetchCol('
            select * from ch_barclays
            GROUP BY company_id
            having sum(success) = 0');
         */

        return CHFiling::getDb()->fetchCol(
            'SELECT com.company_id
                FROM cms2_company_customer AS bank
                LEFT JOIN ch_barclays AS barclays ON barclays.company_id = bank.companyId
                LEFT JOIN ch_company AS com ON com.company_id = bank.companyId
                LEFT JOIN cms2_customers c ON com.customer_id = c.customerId
                WHERE
                  bank.bankTypeId = "' . CompanyCustomerEntity::BANK_TYPE_BARCLAYS. '"
                  AND barclays.company_id IS NULL
                  AND com.company_number IS NOT NULL
                  AND c.countryId = 223
                GROUP BY com.company_id'
        );
    }

    /**
     * sends company information to barclays bank
     *
     * @param int $id company id - the one to be sent
     * @param boolean $test indicates if this submission is test submission
     * @param boolean $sync - indicates if sync is needed before sending company
     * @param bool $wholesale
     * @throws Exception
     */
    public static function sendCompany($id, $test, $sync = FALSE, $wholesale = FALSE)
    {
        /* sync company - load fresh data */
        if ($sync) {
            $company = Company::getCompany($id, $wholesale);
            $company->syncAllData();
        }

        /* data for barclays */
        $company = new BarclaysCompany($id, $wholesale);

        /* set partner ref - unique id of the transaction */
        $data = array('company_id' => $id, 'test' => $test);
        CHFiling::getDb()->insert('ch_barclays', $data);

        $partnerRef = CHFiling::getDb()->lastInsertId();
        $company->setPartnerRef($partnerRef);

        /* send */
        /** @var ParameterBagInterface $config */
        $config = FApplication::$container->get('config');
        /* send */
        $transport = new BarclaysTransport($config->get('barclays.url'), $config->get('barclays.id'), $config->get('barclays.code'));
        $response = $transport->getResponse($company->getXml(), $test);

        //return self::saveResponse($partnerRef, $response);
        /* returns the database data */
        $saveRes = self::saveResponse($partnerRef, $response);

        if (!$saveRes['success']) {
            throw new Exception('Company id: ' . $id . ', Error: ' . $saveRes['response_description']);

//        } else {
//
//            /* @var $companyService CompanyService */
//            $companyService = Registry::$container->get(DiLocator::SERVICE_COMPANY);
//            $companyEntity = $companyService->getCompanyById($id);
//            $package = FNode::getProductById($companyEntity->getProductId());
//            $packageType = ($package instanceof WholesalePackage ? Cashback::PACKAGE_TYPE_WHOLESALE : Cashback::PACKAGE_TYPE_RETAIL);
//
//            /* @var $eventDispatcher EventDispatcher */
//            $eventDispatcher = Registry::$container->get(DiLocator::DISPATCHER);
//            $eventDispatcher->dispatch(
//                EventLocator::BARCLAYS_SUCCESS,
//                new CashbackEvent($companyEntity, new Date(), Cashback::BANK_TYPE_BARCLAYS, $packageType)
//            );
        }
    }


    /**
     * sends soletrader information to barclays bank
     *
     * @param int $id company id - the one to be sent
     * @param boolean $test indicates if this submission is test submission
     * @throws exception
     */
    public static function sendCompanySoletrader($id, $test)
    {

        /* data for barclays */
        $company = new BarclaysSoletraderAll($id);

        /* set partner ref - unique id of the transaction */
        $data = array('barclaysSoletraderId' => $id, 'test' => (int) $test);
        CHFiling::getDb()->insert('ch_barclays_soletrader', $data);

        $partnerRef = CHFiling::getDb()->lastInsertId();
        $company->setPartnerRef($partnerRef);

        /** @var ParameterBagInterface $config */
        $config = FApplication::$container->get('config');
        /* send */
        $transport = new BarclaysTransport($config->get('barclays.url'), $config->get('barclays.id'), $config->get('barclays.code'));
        //pr('xxx');
        $response = $transport->getResponse($company->getXml(), $test, self::$type);

        /* returns the database data */
        $saveRes = self::saveResponseSoletrader($partnerRef, $response);

        if (!$saveRes['success']) {
            throw new Exception('Soletrader id: ' . $id . ', Error: ' . $saveRes['responseDescription']);
        }
    }

    /**
     * Saves response to the db.
     *
     * @param int $partnerRef - primary key from db
     * @param string $response - resonse header from barclays
     * @return boolean success - returns true if response is ok
     */
    private static function saveResponseSoletrader($partnerRef, $response)
    {

        /* response lines */
        $lines = preg_split('/\n/', $response);

        $res = $lines[2];
        $values = preg_split('/ /', $res);

        $success = ($values[1] == 200) ? TRUE : FALSE;

        /* update database */
        $data = array(
            'dateSent' => new Zend_Db_Expr('NOW()'),
            'success' => $success,
            'responseCode' => $values[1],
            'responseDescription' => $response
        );
        CHFiling::getDb()->update(
            'ch_barclays_soletrader',
            $data,
            'barclaysId = ' . $partnerRef
        );

        return $data;
    }


    /**
     * Returns true if company has already been submitted to barclays, false
     * otherwise
     *
     * @param int $id - company id
     * @return boolean
     */
    public static function isCompanySubmitted($id)
    {

        $id = (int) $id;
        $result = CHFiling::getDb()->fetchCol(
            '
                        SELECT barclays_id
                        FROM ch_barclays
                        WHERE company_id = ' . $id
        );

        if (empty($result)) {
            return FALSE;
        }
        return TRUE;
    }

    /**
     * Saves response to the db.
     *
     * @param int $partnerRef - primary key from db
     * @param string $response - resonse header from barclays
     * @return boolean success - returns true if response is ok
     */
    private static function saveResponse($partnerRef, $response)
    {

        /* response lines */
        $lines = preg_split('/\n/', $response);

        $res = $lines[2];
        $values = preg_split('/ /', $res);

        $success = ($values[1] == 200) ? TRUE : FALSE;

        /* update database */
        $data = array(
            'date_sent' => new Zend_Db_Expr('NOW()'),
            'success' => $success,
            'response_code' => $values[1],
            'response_description' => $response
        );
        CHFiling::getDb()->update(
            'ch_barclays',
            $data,
            'barclays_id = ' . $partnerRef
        );

        return $data;
    }
}
