<?php

require_once dirname(__FILE__).'/Request.php';

/**
 * this class is very simple so it doesn't have
 * any table behind. So you can get only new instance
 *
 * it is used only when company incorporation is accepted
 */
final class GetDocument implements Request
{
    /**
     *
     * @var string
     */
    private $docRequestKey;

    /**
     * tag element in the envelope
     *
     * @var string
     */
    private static $class		= 'GetDocument';

    /**
     *
     * @param string $docRequestKey
     */
    private function __construct($docRequestKey) {
        $this->docRequestKey = $docRequestKey;
    }

     /**
      *
      * @param string $docRequestKey
      * @return GetDocument
      */
    public static function getNewDocument($docRequestKey) {
        return new GetDocument($docRequestKey);
    }

    /**
     *
     * @return string
     */
    public function getClass() {
        return self::$class;
    }

    /**
     * getXml
     *
     * @access public
     * @return xml
     */
    public function getXml() {
        // --- test xml --
        $test = (CHFiling::getGatewayTest()) ? '' : '';

        // --- return xml ---
        return '<GetDocument
                    xmlns="http://xmlgw.companieshouse.gov.uk"
                    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                    xsi:schemaLocation="http://xmlgw.companieshouse.gov.uk
                    http://xml'.$test.'gw.companieshouse.gov.uk/v2-1/schema/forms/DocumentRequest-v1-1.xsd">
                    <DocRequestKey>'.$this->docRequestKey.'</DocRequestKey>
                </GetDocument>';
    }

    /**
     * sends this request to companies house and returns response
     *
     * @return xml
     */
    public function sendRequest($formSubmissionId = null) {
        require_once dirname(__FILE__).'/../Envelope.php';
        $envelope = Envelope::getNewEnvelope($formSubmissionId);
        return $envelope->sendRequest($this);
    }
}
