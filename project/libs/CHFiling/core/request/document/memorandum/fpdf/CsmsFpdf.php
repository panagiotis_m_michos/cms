<?php
/*
 * creates custom header and footer
 * for company searches made simple
 * pdf report
 */

require_once('fpdf.php');

/**
 * CsmsFpdf 
 * 
 * @uses FPDF
 * @package fpdf
 */
class CsmsFpdf extends FPDF
{
    /**
     * name 
     * 
     * @var string company name
     * @access private
     */
    private $name;

    /**
     * number 
     * 
     * @var string company number
     * @access private
     */
    private $number;

    /**
     * __construct 
     * 
     * @param string $name company name
     * @param string $number company number
     * @access public
     * @return void
     */
    public function __construct($name, $number)
    {
        parent::__construct();
        $this->name     = $name;
        $this->number   = $number;
    }

    /**
     * Header 
     *
     * page header
     * 
     * @access public
     * @return void
     */
    public function Header()
    {
        // --- Title ---
        $this->SetFont('Arial','B',14);
        $this->SetTextColor(255, 255, 255);
        $this->SetFillColor(0, 102, 153);
        $this->SetDrawColor(0, 0, 0);

        //top border
        $this->Cell(0,5,'',0,1,'L',1);  

        $x = $this->GetX();
        $y = $this->getY();
        $this->Cell(5,16.09,'',0,0,'L',1); //left spacer - left from image
        $this->Image(PROJECT_PATH .'custom/creditsafe/Convert/fpdf/CSMS.jpg', null, null, 50);   //image
        $this->SetXY($x+55, $y);
        $this->Cell(5,16.09,'',0,0,'L',1); //right spacer - right from image
        $this->Cell(0,8.045,'Online company credit reports.',0,2,'L',1);
        $this->Cell(0,8.045,'Speedy, reliable and competitively priced!',0,1,'L',1);

        //bottom border
        $this->Cell(0,5,'',0,1,'L',1);  

        // --- company name and company number ---
        $this->SetFillColor(255, 255, 255);
        $this->SetTextColor(0, 0, 0);
        $this->SetFont('Arial','B',10);
        $this->Cell(165,10,$this->name);
        $this->Cell(25,10,$this->number);

        $this->Ln(20);
    }

    /**
     * Footer 
     *
     * page footer
     * 
     * @access public
     * @return void
     */
    public function Footer()
    {
        $this->SetY(-20);
        //Arial italic 8
        $this->SetFont('Arial','I',8);
        $this->SetTextColor(0, 0, 0);
        $this->SetDrawColor(0, 0, 0);
        $this->Cell(0,10,'N/A* - no information available');

        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //Page number
        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }

    /**
     * getNbLines
     *
     * returns number of lines from passed mutlicell
     * code is taken from:
     * http://www.fpdf.de/downloads/addons/3/
     *
     * @param float $w width of multicell
     * @param string $txt text
     * @access public
     * @return integer
     */
    public function getNbLines($w, $txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r", '', $txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
