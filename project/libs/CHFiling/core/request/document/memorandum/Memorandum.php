<?php

require_once 'fpdf/fpdf.php';

class Memorandum {

    private $fpdf;

    private $companyName;

    private $subscribers;

    private $corpSubscribers;

    private $shareCapital;

    public function __construct($companyName, array $subscribers, array $corpSubscribers, $shareCapital = true) {
        $this->companyName = $companyName;
        $this->subscribers = $subscribers;
        $this->corpSubscribers = $corpSubscribers;
        $this->shareCapital = (bool) $shareCapital;

        $this->fpdf = new FPDF();
        $this->fpdf->AliasNbPages();
        $this->fpdf->AddPage();
    }

    private function createPdf() {
        $this->fpdf->SetFont('Arial', '', 16);
        $this->fpdf->MultiCell(190, 5, 'Companies Act 2006', 0, 'C');

        $this->fpdf->Ln();

        $this->fpdf->SetFont('Arial', '', 12);

        $shareCapital   = ($this->shareCapital) ? '' : ' NOT' ;
        $schedule       = ($this->shareCapital) ? '1' : '2';
        $share          = ($this->shareCapital) ? ' and to take at least one share each.' : '.';
        $heading = "SCHEDULE $schedule \n COMPANY$shareCapital HAVING A SHARE CAPITAL \n Memorandum of Association of \n ".htmlspecialchars_decode($this->companyName, ENT_NOQUOTES);

        $subHeading = 'Each subscriber to this memorandum of association wishes '.
            'to form a company under the Companies Act 2006 and agrees to become '.
            "a member of the company".$share;

        /*
            Name and address of each subscriber
            Authentication by each subscriber
            Authenticated Electronically
            Dated [insert date here]';
         */
        $this->fpdf->MultiCell(190, 5, $heading, 0, 'C');

        $this->fpdf->SetFont('Arial', '', 10);

        $this->fpdf->Ln();
        $this->fpdf->Ln();

        $this->fpdf->MultiCell(190, 5, $subHeading, 0, 'J');

        $this->fpdf->Ln();
        $this->fpdf->Ln();

        foreach ($this->subscribers as $subscriber) {
            $forename   = iconv("UTF-8", "ISO-8859-1//TRANSLIT",$subscriber->getPerson()->getForename());
            $surname    = iconv("UTF-8", "ISO-8859-1//TRANSLIT",$subscriber->getPerson()->getSurname());
            $middleName = iconv("UTF-8", "ISO-8859-1//TRANSLIT",$subscriber->getPerson()->getMiddleName()) . ' ';
            if (!isset($middleName) || is_null($middleName)) {
                $middleName = ' ';
            }
            $this->fpdf->MultiCell(190, 5, 'Subscriber:', 'B');
            $this->fpdf->MultiCell(190, 5, $forename .' ' . $middleName . $surname, 0, 'J');

            /* --- fucking companies house changed their mind
            $this->fpdf->Ln();
            $address = $subscriber->getAddress()->getFields();
            foreach($address as $value) {
                if ($value != null || !empty($value)) {
                    $this->fpdf->MultiCell(190, 5, $value, 0, 'J');
                }
            }
            */
            $this->fpdf->Ln();

            $this->fpdf->MultiCell(190, 5, 'Authentication: Authenticated Electronically');

            $this->fpdf->Ln();
        }

        foreach ($this->corpSubscribers as $subscriber) {
            $this->fpdf->MultiCell(190, 5, 'Subscriber:', 'B');
            $this->fpdf->MultiCell(190, 5, $subscriber->getCorporate()->getCorporateName());
            $this->fpdf->MultiCell(190, 5, 'Authorising Person: '.$subscriber->getCorporate()->getForename() .' '.$subscriber->getCorporate()->getSurname(), 0, 'J');

            /* --- fucking companies house changed their mind
            $this->fpdf->Ln();
            $address = $subscriber->getAddress()->getFields();
            foreach($address as $value) {
                if ($value != null || !empty($value)) {
                    $this->fpdf->MultiCell(190, 5, $value, 0, 'J');
                }
            }
            */
            $this->fpdf->Ln();

            $this->fpdf->MultiCell(190, 5, 'Authentication: Authenticated Electronically');

            $this->fpdf->Ln();
        }

        $this->fpdf->Ln();
        $this->fpdf->MultiCell(190, 5, 'Dated: '.date('j M Y'));

    }

    public function getMemorandumPdf() {
        $this->createPdf();

        $this->fpdf->Output('Memorandum.pdf','I');
        exit;
    }

    public function saveMemorandum($filePath) {
        $this->createPdf();

        $this->fpdf->Output($filePath,'F');
    }


}
