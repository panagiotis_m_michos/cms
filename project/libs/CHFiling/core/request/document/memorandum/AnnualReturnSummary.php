<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'fpdf/fpdf.php';

class AnnualReturnSummary extends FPDF
{
	
    public function basicTable($data)
	{
	foreach($data as $row)
	    {
		foreach($row as $col)
		    $this->Cell(80,6,$col,1);
                    $this->Ln();
	    }


	}
   public function basicCompanyStatutoryTable($data)
   {
        foreach($data as $row)
        {
            if (count($row) == 2) {
                $row0 = (string) $row[0];
                $row1 = (string) $row[1];
                if ( !strlen(trim($row0)) || !strlen(trim($row1))) {
                    continue;
                }
                $this->Cell(80,6,$row0,1);
                if (isset($row1) && strlen($row1 > 80)){
                       $this->Cell(110,6,$row1,1);
                       $this->Ln();
                } else {
                       $this->MultiCell(110,6,$row1,1);
                       
                }
               
            } else {
                foreach($row as $col)
		    $this->Cell(80,6,$col,1);
                    $this->Ln();
            }
             
        }
        
            
   }     
        
        
}