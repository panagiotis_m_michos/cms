<?php

final class Document
{

    /**
     *
     * @var int
     */
    private $documentId;

    /**
     *
     * @var int
     */
    private $formSubmissionId;

    /**
     *
     * @var enum(MEMARTS|SUPPNAMEAUTH|ACCOUNTS|SUPPEXISTNAME)
     */
    private $category;

    /**
     *
     * @var string
     */
    private $filename;

    /**
     *
     * @var string
     */
    private $filePath;

    /**
     *
     * @var int
     */
    private $companyId;

    /**
     *
     * @var string
     */
    private static $tableName = 'ch_document';

    /**
     *
     * @param int $documentId
     */
    private function __construct($documentId, $companyId)
    {
        // --- check that documentId is int ---
        if (!preg_match('/^\d+$/', $documentId)) {
            throw new Exception('documentId has to be integer');
        }

        $sql = "
			SELECT * FROM `" . self::$tableName . "`
			WHERE `document_id` = $documentId";
        if (!$result = CHFiling::getDb()->fetchRow($sql)) {
            throw new Exception('Record doesn\'t exist');
        }

        // --- set instance variables ---
        $this->documentId = $result['document_id'];
        $this->formSubmissionId = $result['form_submission_id'];
        $this->category = $result['category'];
        $this->filename = $result['filename'];
        $this->filePath = $result['file_path'];
        $this->companyId = $companyId;
    }

    /**
     *
     * @param int $documentId
     * @return Document
     */
    public static function getDocument($documentId, $companyId)
    {
        return new Document($documentId, $companyId);
    }

    /**
     *
     * @param int $formSubmissionId
     * @param enum('MEMARTS','SUPPNAMEAUTH','SUPPEXISTNAME','ACCOUNTS') $category
     * @param string $filename
     * @param string $filePath
     * @return Document
     */
    public static function getNewDocument($formSubmissionId, $category, $filename, $companyId, $custom = NULL)
    {
        // --- check that formSubmissionId is int ---
        if (!preg_match('/^\d+$/', $formSubmissionId)) {
            throw new Exception('formSubmissionId has to be integer');
        }

        // --- check category ---
        $category = strtoupper($category);
        if ($category != 'MEMARTS' && $category != 'SUPPNAMEAUTH' && $category != 'SUPPEXISTNAME' && $category != 'ACCOUNTS') {
            throw new Exception($category . 'category doesn\'t exist');
        }

        // --- check file extension ---
        $fileInf = pathinfo($filename);
        if ($category == 'ACCOUNTS') {
            if ($fileInf['extension'] != 'xml') { // xml
                throw new Exception('file extension is not correct for this category');
            }
        } else {
            if ($fileInf['extension'] != 'pdf') { // pdf
                throw new Exception('file extension is not correct for this category');
            }
        }

        // --- check if filename exists - filename has to be unique ---
        $sql = "
            SELECT `document_id` FROM " . self::$tableName . "
            WHERE form_submission_id = $formSubmissionId AND `category` = '$category' AND filename = '$filename'";
        $result = CHFiling::getDb()->fetchOne($sql);
        // --- document exists, so just return it ---
        if (!empty($result)) {
            return new Document($result);
        }

        // --- insert new document ---
        $data = array(
            'form_submission_id' => $formSubmissionId,
            'category' => $category,
            'file_path' => NULL,
            'filename' => $filename);
        if ($custom) {
            $data['custom'] = 1;
        }
        CHFiling::getDb()->insert(self::$tableName, $data);

        // --- return new instance of the doucment ---
        return new Document(CHFiling::getDb()->lastInsertId(), $companyId);
    }

    public static function getMemorandum($companyName, array $subscribers, array $corpSubscribers)
    {
        $memorandum = new Memorandum($companyName, $subscribers, $corpSubscribers);
        return $memorandum->getMemorandumPdf();
    }

    public static function saveMemorandum($companyName, array $subscribers, array $corpSubscribers, $filePath, $shareCapital = TRUE)
    {
        $memorandum = new Memorandum($companyName, $subscribers, $corpSubscribers, $shareCapital);
        return $memorandum->saveMemorandum($filePath);
    }

    /**
     *
     * @param int $formSubmissionId
     * @return array
     */
    public static function getFormSubmissionDocumentsIds($formSubmissionId)
    {
        // --- check that formSubmissionId is int ---
        if (!preg_match('/^\d+$/', $formSubmissionId)) {
            throw new Exception('formSubmissionId has to be integer');
        }

        // --- query ---
        $sql = "
            SELECT `document_id` FROM " . self::$tableName . "
            WHERE form_submission_id = $formSubmissionId";
        $result = CHFiling::getDb()->fetchCol($sql);

        // --- return result ---
        return $result;
    }

    /**
     *
     * @param int $formSubmissionId
     * @return array
     */
    public static function copyRejectedDocsWithNewFormSubmision($formSubmissionId, $newFormSubmissionId)
    {
        // --- check that formSubmissionId is int ---
        if (!preg_match('/^\d+$/', $formSubmissionId)) {
            throw new Exception('formSubmissionId has to be integer');
        }

        // --- query ---
        $sql = "INSERT INTO " . self::$tableName . "
            (`form_submission_id`, `category`, `filename`,`custom`)
            SELECT $newFormSubmissionId, `category`, `filename`,`custom`
            FROM `" . self::$tableName . "`
            WHERE `form_submission_id` = $formSubmissionId";

        CHFiling::getDb()->query($sql);
    }

    /**
     * removes this document
     */
    public function remove()
    {
        CHFiling::getDb()->delete(self::$tableName, 'document_id = ' . $this->documentId);

        // --- set instance variables ---
        $this->documentId = NULL;
        $this->formSubmissionId = NULL;
        $this->category = NULL;
        $this->filename = NULL;
        $this->filePath = NULL;
    }

    /**
     * output
     *
     * return pdf representation of the document
     *
     * @return pdf
     */
    public function output()
    {
        $out = file_get_contents($this->getFilePath() . $this->filename);
        // --- prepare headers ---
        $agent = trim($_SERVER['HTTP_USER_AGENT']);
        if ((preg_match('|MSIE ([0-9.]+)|', $agent, $version)) || (preg_match('|Internet Explorer/([0-9.]+)|', $agent, $version))) {
            header('Content-Type: application/x-msdownload');
            header('Content-Length: ' . strlen($out));
            if ($version == '5.5') {
                header('Content-Disposition: filename="' . $this->filename . '"');
            } else {
                header('Content-Disposition: attachment; filename="' . $this->filename . '"');
            }
        } else {
            header('Content-Type: application/pdf');
            header('Content-Length: ' . strlen($out));
            header('Content-disposition: attachment; filename=' . $this->filename);
        }

        // --- output ---
        echo $out;
        exit;
    }

    /**
     *
     * @param int $documentId
     */
    public static function removeDocument($documentId)
    {
        // --- check that documentId is int ---
        if (!preg_match('/^\d+$/', $documentId)) {
            throw new Exception('documentId has to be integer');
        }

        // --- remove document from db ---
        CHFiling::getDb()->delete(self::$tableName, 'document_id = ' . $documentId);
    }

    /**
     *
     * @return int
     */
    public function getDocumentId()
    {
        return $this->documentId;
    }

    /**
     *
     * @return enum(MEMARTS|SUPPNAMEAUTH|ACCOUNTS|SUPPEXISTNAME)
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    public function getFilePath()
    {
        return CHFiling::getDocPath() . '/' . CHFiling::getDirName($this->companyId) . '/company-' . $this->companyId . '/formSubmission-' . $this->formSubmissionId . '/';
    }

    /**
     * @return string
     */
    public function getFullPath()
    {
        return $this->getFilePath() . $this->getFilename();
    }

    /**
     * converts pdf to pcl and returns data
     *
     * @param string $filePath
     * @return string
     */
    public function pdf2pcl($filePath)
    {
        $pathParts = pathinfo($filePath);

        // store output in the file folder
        $fileName = dirname(__FILE__) . '/file/' . $this->documentId . $pathParts['filename'];

        //convert file.pdf to file.ps
        exec("pdf2ps $filePath $fileName.ps");

        // convert file.ps to file.pcl
        // --- -r200x200 - works fine
        // --- for plc articles (reduced to 16 pages) needs to set to -r160x160
        exec("gs -sDEVICE=ljet4 -sPAPERSIZE=a4 -r200x200 -sOutputFile=$fileName.pcl -dNOPAUSE -q $fileName.ps -c quit");
        //pr($fileName);exit;
        $data = base64_encode(file_get_contents($fileName . '.pcl'));

        // --- check size ---
        if (strlen($data) < 1) {
            throw new Exception('Invalid PDF for custom M&A or supporting documents.');
        }
        if (strlen($data) > 1200000) {
            throw new Exception('PDF is too large. Please try to decrease the number of pages in the file by reducing the margins and font size.');
        }
        return $data;
    }

    public function pdfEncode($filePath)
    {

        $data = base64_encode(file_get_contents($filePath));
        // --- check size ---
        if (strlen($data) < 1) {
            throw new Exception('Invalid PDF for custom M&A or supporting documents.');
        }
        if (strlen($data) > 1200000) {
            throw new Exception('PDF is too large. Please try to decrease the number of pages in the file by reducing the margins and font size.');
        }
        return $data;
    }

    /**
     *
     * @return xml
     */
    public function getXml()
    {
        $contentType = ($this->category == 'ACCOUNTS') ? 'application/xml' : 'application/pdf';
        return '<Document>
                <Data>' . $this->pdfEncode($this->getFilePath() . $this->filename) . '</Data>
                <ContentType>' . $contentType . '</ContentType>
                <Category>' . $this->category . '</Category>
            </Document>';
        //        } else {
        //            $contentType = ($this->category == 'ACCOUNTS') ? 'application/xml' : 'application/vnd.hp-pcl';
        //            return '<Document>
        //                <Data>' . $this->pdf2pcl($this->getFilePath() . $this->filename) . '</Data>
        //                <ContentType>' . $contentType . '</ContentType>
        //                <Category>' . $this->category . '</Category>
        //            </Document>';
        //        }
    }

    public static function removePCandPCLDocument($formsubmisionId, $companyId)
    {
        //get files
        $files = CHFiling::getDb()->fetchAll("SELECT document_id FROM `ch_document`	WHERE form_submission_id = $formsubmisionId");

        foreach ($files as $key => $value) {
            $document = Document::getDocument($value['document_id'], $companyId);
            $pathParts = pathinfo($document->getFilePath());
            $fileName = dirname(__FILE__) . '/file/' . $document->documentId . substr($document->filename, 0, -3);

            $filesToRemove[] = $fileName . 'ps';
            $filesToRemove[] = $fileName . 'pcl';
        }
        //remove files
        foreach ($filesToRemove as $fileToRemove) {
            if (file_exists($fileToRemove)) {
                if (@unlink($fileToRemove) === FALSE) {
                    throw new Exception("Something is wrong, we may not have enough permission to delete `$fileToRemove`");
                }
            } else {
                throw new Exception("the file `$fileToRemove` is not found");
            }
        }
    }

}
