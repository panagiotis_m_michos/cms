<?php

final class PaymentPeriodsRequest implements Request
{
	/**
	 * @var string
	 */
	private static $class = 'PaymentPeriodsRequest';

	/**
	 * @var int
	 */
	private $requestId;

	/**
	 * @var int
	 */
	private $customerId;

	/**
	 * @var int
	 */
	private $envelopeId;

	/**
	 * @var string
	 */
	private $companyNumber;

	/**
	 * @var string
	 */
	private $authenticationCode;

	/**
	 * @var string
	 */
	private static $tableName = 'ch_payment_periods_request';

	/**
	 * @param int $requestId
	 * @throws Exception
	 */
	private function __construct($requestId)
	{
		if (!preg_match('/^\d+$/', $requestId)) {
			throw new Exception('requestId has to be integer');
		}

		$sql = "
			SELECT * FROM `" . self::$tableName . "`
			WHERE `paymentPeriodsRequestId` = $requestId";
		if (!$result = CHFiling::getDb()->fetchRow($sql)) {
			throw new Exception("Record doesn't exist");
		}

		$this->requestId = $result['paymentPeriodsRequestId'];
		$this->customerId = $result['customerId'];
		$this->envelopeId = $result['envelopeId'];
		$this->companyNumber = $result['companyNumber'];
		$this->authenticationCode = $result['authenticationCode'];
	}

	/**
	 * @param int $requestId
	 * @return CompanyDataRequest
	 */
	public static function getPaymentPeriodsRequest($requestId)
	{
		return new self($requestId);
	}

	/**
	 * @param int $customerId
	 * @param string $companyNumber
	 * @param string $authenticationCode
	 * @return CompanyDataRequest
	 */
	public static function getNewPaymentPeriodsRequest($customerId, $companyNumber, $authenticationCode)
	{
		$data = [
			'customerId' => $customerId,
			'companyNumber' => $companyNumber,
			'authenticationCode' => $authenticationCode,
		];
		CHFiling::getDb()->insert(self::$tableName, $data);

		return new self(CHFiling::getDb()->lastInsertId());
	}

	/**
	 * set this after receiving response
	 * so in case of error this can be easily located
	 *
	 * @param int $envelopeId
	 * @throws Exception
	 */
	public function setEnvelopeId($envelopeId)
	{
		if (!preg_match('/^\d+$/', $envelopeId)) {
			throw new Exception('envelopeId has to be integer');
		}

		CHFiling::getDb()->update(
			self::$tableName,
			['envelopeId' => $envelopeId],
			'paymentPeriodsRequestId = ' . $this->requestId
		);
		$this->envelopeId = $envelopeId;
	}

	/**
	 * @return string
	 */
	public function getClass()
	{
		return self::$class;
	}

	/**
	 * @return string
	 */
	public function getXml()
	{
		$xsdSchema = 'http://xmlbeta.companieshouse.gov.uk/v1-0/schema/PaymentPeriods-v1-0-rc1.xsd';

		return '<PaymentPeriodsRequest
            xmlns="http://xmlgw.companieshouse.gov.uk"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://xmlgw.companieshouse.gov.uk ' . $xsdSchema . '">
			<CompanyNumber>' . $this->companyNumber . '</CompanyNumber>
			<CompanyAuthenticationCode>' . $this->authenticationCode . '</CompanyAuthenticationCode>
		</PaymentPeriodsRequest>';
	}

	/**
	 * @return string
	 */
	public function sendRequest()
	{
		$envelope = Envelope::getNewEnvelope();
		$response = $envelope->sendRequest($this);
		$this->setEnvelopeId($envelope->getEnvelopeId());

		return $response;
	}
}
