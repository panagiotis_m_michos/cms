<?php

/**
 * @package    CMS
 * @author     Nikolai Senkevich
 */

require_once dirname(__FILE__).'/Request.php';

final class SetEReminders implements Request
{
    /**
     * tag element in the envelope
     *
     * @var string
     */
    private static $class = 'SetERemindersRequest';

    /**
     *
     * @var array
     */
    public  $emails = array();

    /**
     *
     * @var int
     */
    public $companyNumber;
    
    /**
     *
     * @var string 
     */
    public $companyType;
    
    /**
     *
     * @var string 
     */
    public $authenticationCode;

    /**
     *
     * @param type $emails
     * @param Company $company 
     */
    private function __construct($emails, Company $company) {
        $this->emails = $emails;
        $this->companyNumber = $company->getCompanyNumber();
        $data = $company->getData();      
        $this->companyType = $data['company_details']['jurisdiction'];
        $this->authenticationCode = $company->getAuthenticationCode();
        
    }

    /**
     *
     * @param type $emails
     * @param Company $company
     * @return SetEReminders 
     */
    public static function getNewReminder($emails, $company) {
        return new SetEReminders($emails, $company);
    }

	/**
	 *
	 * @return string
	 */
	public function getClass() {
		return self::$class;
	}

	/**
	 *
	 * @return xml
	 */
    public function getXml() {
        // --- test xml --
        $test = (CHFiling::getGatewayTest()) ? '' : '';
        
        $emailXml = '';
        foreach ($this->emails as $email) {
            $emailXml .='<EmailAddress>'.$email.'</EmailAddress>';
        }
        return '<SetERemindersRequest 
                xmlns="http://xmlgw.companieshouse.gov.uk"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xsi:schemaLocation="http://xmlgw.companieshouse.gov.uk 
                http://xmlgw.companieshouse.gov.uk/v2-1/schema/EReminders-v1-0.xsd">

                <CompanyNumber>'.$this->companyNumber.'</CompanyNumber>
                <CompanyType>'.$this->companyType.'</CompanyType>
                <CompanyAuthenticationCode>'.$this->authenticationCode.'</CompanyAuthenticationCode>
                '.$emailXml.'
                </SetERemindersRequest>';
    }

    /**
     * sends this request to companies house
     * 
     * @return xml
     */
    public function sendRequest() {
        require_once dirname(__FILE__).'/../Envelope.php';
        $envelope = Envelope::getNewEnvelope();
        $response = $envelope->sendRequest($this);
        
		// --- return response ---
		return $response;

    }
}
