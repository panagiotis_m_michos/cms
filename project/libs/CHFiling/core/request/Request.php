<?php
/**
 * Description of Request
 *
 * @author pete
 */
interface Request {
    /**
     * class tag for envelope
     * 
     * @return string
     */
    public function getClass();

    /**
     * xml body for envelope
     * 
     * @return xml
     */
    public function getXml();

    /**
     * sends request to companies house and returns response
     * 
     * @return xml - response from companies house
     */
    public function sendRequest();
}