<?php

require_once dirname(__FILE__).'/Request.php';

/**
 * this class is very simple so it doesn't have 
 * any table behind. So you can get only new instance
 * 
 * it is used only by GetSubmissionStatus class
 */
final class StatusAck implements Request 
{
    /**
     * tag element in the envelope
     *
     * @var string
     */
    private static $class		= 'StatusAck';

    /**
     *
     */
    private function __construct() {

    }

    /**
     *
     * @return StatusAck
     */
    public static function getNewStatucAck() {
        return new StatusAck();
    }

    /**
     *
     * @return string
     */
    public function getClass() {
        return self::$class;
    }

    /**
     * getXml
     *
     * @access public
     * @return xml
     */
    public function getXml() {
        // --- test xml --
        $test = (CHFiling::getGatewayTest()) ? '09' : '';

        // --- return xml ---
        return '<StatusAck
                    xmlns="http://xmlgw.companieshouse.gov.uk"
                    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                    xsi:schemaLocation="http://xmlgw.companieshouse.gov.uk
                    http://xml'.$test.'gw.companieshouse.gov.uk/v1-0/schema/forms/GetStatusAck-v1-1.xsd"></StatusAck>';
    }

    /**
     * sends this request to companies house and returns response
     *
     * @return xml
     */
    public function sendRequest() {
        require_once dirname(__FILE__).'/../Envelope.php';
        $envelope = Envelope::getNewEnvelope();
        return $envelope->sendRequest($this);
    }
}