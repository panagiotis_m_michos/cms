<?php

require_once dirname(__FILE__).'/Request.php';

final class GetSubmissionStatus implements Request
{
    /**
     * tag element in the envelope
     *
     * @var string
     */
    private static $class		= 'GetSubmissionStatus';

	/**
	 * get_submission_status_id column
	 * 
	 * @var int
	 */
	private $statusId;

    /**
     * same as senderId
     * 
     * @var string
     */
    private $presenterId;

    /**
     * 
     * @var string
     */
    private $submissionNumber;

    /**
     *
     * @var string
     */
    private $companyNumber;

	/**
	 * envelopeId by which this request has been sent
	 * 
	 * @var int
	 */
	private $envelopeId;

	/**
	 *
	 * @var string
	 */
	private static $tableName	= 'ch_get_submission_status';

    /**
     *
     * @param string $presenterId 
     */
    private function __construct($statusId) {
        // --- check that statusId is int ---
        if (!preg_match('/^\d+$/', $statusId)) {
            throw new Exception('statusId has to be integer');
        }

        $sql = "
			SELECT * FROM `".self::$tableName."`
			WHERE `get_submission_status_id` = $statusId";
        if (!$result = CHFiling::getDb()->fetchRow($sql)) {
            throw new Exception('Record doesn\'t exist');
        }

        // --- set instance variables ---
		$this->statusId			= $result['get_submission_status_id'];
        $this->presenterId      = $result['presenter_id'];
        $this->submissionNumber = $result['submission_number'];
        $this->companyNumber    = $result['company_number'];
        $this->envelopeId		= $result['envelope_id'];
    }

	/**
	 * returns existion GetSubmissionStatusRequest
	 * 
	 * @param int $statusId
	 * @return GetSubmissionStatus 
	 */
	public static function getGetSubmissionStatus($statusId) {
		return new self($statusId);
	}
      
    /**
     *
     * @param string $presenterId 
     */
    public static function getNewGetSubmissionStatus($presenterId) {
        // --- insert data into database ---
        $data = array(
            'presenter_id' => $presenterId
        );
        CHFiling::getDb()->insert(self::$tableName, $data);

		// --- return new instance ---
        return new self(CHFiling::getDb()->lastInsertId());
    }

    /**
     *
     * @param string $companyNumber 
     */
    public function setCompanyNumber($companyNumber) {
        if (!preg_match('/^([a-zA-Z]|[\d]){2,2}\d{6,6}$/', $companyNumber)) {
            throw new Exception('company number is invalid');
        }
        if (isset($this->submissionNumber)) {
            throw new Exception('submission number is already set, you cannot set both');
        }

		// --- update database and instance variable ---
        CHFiling::getDb()->update(
			self::$tableName,
			array('company_number' => $companyNumber),
			'get_submission_status_id = ' . $this->statusId);
        $this->companyNumber = $companyNumber;
    }

    /**
     *
     * @param string $submissionNumber 
     */
    public function setSubmissionNumber($submissionNumber) {
        if (isset($this->companyNumber)) {
            throw new Exception('company number is already set, you cannot set both');
        }

		// --- update database and instance variable ---
        CHFiling::getDb()->update(
			self::$tableName,
			array('submission_number' => $submissionNumber),
			'get_submission_status_id = ' . $this->statusId);
        $this->submissionNumber = $submissionNumber;
    }

	/**
	 * set this after receiving response
	 * so in case of error this can be easily located
	 *
	 * @param int $envelopeId
	 */
    private function setEnvelopeId($envelopeId) {
        // --- check that statusId is int ---
        if (!preg_match('/^\d+$/', $envelopeId)) {
            throw new Exception('envelopeId has to be integer');
        }

		// --- update database and instance variable ---
        CHFiling::getDb()->update(
			self::$tableName,
			array('envelope_id' => $envelopeId),
			'get_submission_status_id = ' . $this->statusId);
        $this->envelopeId	= $envelopeId;
    }

    /**
     * 
     * @return string
     */
    public function getClass() {
        return self::$class;
    }

    /**
     * getXml 
     * 
     * @access public
     * @return xml
     */
    public function getXml() {
        // --- test xml --
        $test = (CHFiling::getGatewayTest()) ? '' : '';

        // --- preparet elements ---
        $submissionNumber   = (is_null($this->submissionNumber)) ? '' :
            '<SubmissionNumber>'.$this->submissionNumber.'</SubmissionNumber>';
        $companyNumber      = (is_null($this->companyNumber)) ? '' :
            '<CompanyNumber>'.$this->companyNumber.'</CompanyNumber>';

        // --- return xml ---
        return '<GetSubmissionStatus
            xmlns="http://xmlgw.companieshouse.gov.uk" 
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
            xsi:schemaLocation="http://xmlgw.companieshouse.gov.uk 
                http://xml'.$test.'gw.companieshouse.gov.uk/v2-1/schema/forms/GetSubmissionStatus-v2-5.xsd">'.
            $submissionNumber . $companyNumber
            .'<PresenterID>'.$this->presenterId.'</PresenterID>
            </GetSubmissionStatus>';
    }

    /**
     * sends this request to companies house and returns response
     * 
     * @@param $formSubmissionId - foreign key
     * @return xml
     */
    public function sendRequest($formSubmissionId = null) {
        require_once dirname(__FILE__).'/../Envelope.php';
        $envelope = Envelope::getNewEnvelope($formSubmissionId);
        $response = $envelope->sendRequest($this);
        // --- set foreign key ---
        $this->setEnvelopeId($envelope->getEnvelopeId());

        // --- check if we need to send status ack ---
        if (is_null($this->submissionNumber)) {
            // --- check if the previos reuquest was successful ---
            if ($envelope->getSuccess()) {
                // --- check if it's an error ---
                $xml = simplexml_load_string($response);
                if (isset($xml->GovTalkDetails->GovTalkErrors->Error)) {
                    // --- return response ---
                    return $response;
                }

                // --- create new envelope and send status ack ---
                require_once 'StatusAck.php';
                $statusAck  = StatusAck::getNewStatucAck();
                $envelope   = Envelope::getNewEnvelope();
                $envelope->sendRequest($statusAck);
            }
        }

        // --- return response ---
        return $response;
    }
}
