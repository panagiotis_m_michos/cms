<?php

class OfficerAppointment implements Form {

    /**
     *
     * @var int
     */
    private $formSubmissionId;

    /**
     *
     * @var string
     */
    private $rejectDescription;

    /**
     *
     * @var array
     */
    private $fields;

    /**
     *
     * @var string
     */
    private static $tableName = 'ch_officer_appointment';

    /**
     *
     * @param int $formSubmissionId
     */
    private function  __construct($formSubmissionId) {
        // --- check that formSubmissionId is int ---
        if (!preg_match('/^\d+$/', $formSubmissionId)) {
            throw new Exception('formSubmissionId has to be integer');
        }

        // --- load data from database ---
        $sql = "
            SELECT * FROM ".self::$tableName."
            WHERE `form_submission_id` = $formSubmissionId";
        if (!$result = CHFiling::getDb()->fetchRow($sql)) {
            throw new Exception('Officer apppointment doesn\'t exist');
        }

        // --- set instance variables ---
        $this->formSubmissionId = $result['form_submission_id'];
        $this->rejectDescription= $result['reject_description'];

        unset($result['form_submission_id']);
        unset($result['reject_description']);

        // --- set fields ---
        $this->fields = $result;
    }

    /**
     *
     * @param int $formSubmissionId
     * @return CompanyIncorporation
     */
    public static function getOfficerAppointment($formSubmissionId) {
        return new self($formSubmissionId);
    }

    /**
     *
     * @param int $formSubmissionId
     * @return CompanyIncorporation
     */
    public static function getNewOfficerAppointment($formSubmissionId) {
        // --- check that formSubmissionId is int ---
        if (!preg_match('/^\d+$/', $formSubmissionId)) {
            throw new Exception('formSubmissionId has to be integer');
        }

        // --- insert new company incorporation ---
        $data = array('form_submission_id'=> $formSubmissionId);
        CHFiling::getDb()->insert(self::$tableName, $data);

        // --- return new instance ---
        return new self($formSubmissionId);
    }

    /**
     * checks if array fields has filled in all compulsory fields - supplied
     * as first argument
     *
     * @param array $compulsoryFields
     * @param array $fields
     * @return boolean
     */
    private function isComplete($compulsoryFields, $fields) {
        // --- check if all fields are set ---
        foreach($compulsoryFields as $v) {
            if (!isset($fields[$v]) || is_null($fields[$v])) {
                return 0;
            }
        }
        return 1;
    }

    /**
     * sets new Registered office address that is going to be changed
     *
     * @param Address $address
     */
    public function setFields($fields) {
        // TODO - finish checking fields ---

        // --- check fields ---
        if (!$this->isComplete(array('type', 'corporate'), $fields)) {
            throw new Exception('you need to set type and corporate');
        }

        // --- if it's director we need dob  ---
        if ($fields['type'] == 'DIR' && $fields['corporate'] == 0) {
            if (!isset($fields['dob']) || is_null($fields['dob'])) {
                throw new Exception('you need to set dob for director');
            }
        }

        // --- if it's corporate we need corporate name ---
        if ($fields['corporate']) {
            if (!isset($fields['corporate_name']) || is_null($fields['corporate_name'])) {
                throw new Exception('you need to set corporate name for corporate officer');
            }
        }

        unset($fields['residential_care_of_name']);
        unset($fields['residential_po_box']);
        unset($fields['secure_address_ind']);

        CHFiling::getDb()->update(self::$tableName, $fields, 'form_submission_id = '.$this->formSubmissionId);
        $this->fields = $fields;
    }

    /**
     *
     * @return xml
     */
    public function getXml() {

        $test = (CHFiling::getGatewayTest()) ? '' : '';

        if ($this->fields['consentToAct']) {
            $authentication = '<ConsentToAct>true</ConsentToAct>';
        } else {
            $authentication = '<ConsentToAct>false</ConsentToAct>';
        }

        $appointment = '';

        // we get designatedId just in LLP case 
        $type = 'BYSHR';
        if(array_key_exists('designated_ind', $this->fields) && $this->fields['designated_ind'] !== NULL){
            $type = 'LLP';
        }
        
        // --- corporate ---
        if ($this->fields['corporate']) {
            // --- corporate ---
            $corporate = '';

            $corporate .= '<CorporateName>'.htmlspecialchars($this->fields['corporate_name'], ENT_NOQUOTES).'</CorporateName>';

            // --- address ---
            $serviceAddress = '';

            $serviceAddress .= '<Premise>'.htmlspecialchars($this->fields['premise'], ENT_NOQUOTES).'</Premise>';
            $serviceAddress .= '<Street>'.htmlspecialchars($this->fields['street'], ENT_NOQUOTES).'</Street>';
            $serviceAddress .= (is_null($this->fields['thoroughfare'])) ? '' :
                '<Thoroughfare>'.htmlspecialchars($this->fields['thoroughfare'], ENT_NOQUOTES).'</Thoroughfare>';
            $serviceAddress .= '<PostTown>'.htmlspecialchars($this->fields['post_town'], ENT_NOQUOTES).'</PostTown>';
            $serviceAddress .= (is_null($this->fields['county'])) ? '' :
                '<County>'.htmlspecialchars($this->fields['county'], ENT_NOQUOTES).'</County>';

            if (isset(Address::$nonForeignCountries[$this->fields['country']])) {
                $serviceAddress .= '<Country>'.htmlspecialchars($this->fields['country'], ENT_NOQUOTES).'</Country>';
            } else {
                $serviceAddress .= '<OtherForeignCountry>'.htmlspecialchars($this->fields['country'], ENT_NOQUOTES).'</OtherForeignCountry>';
            }

            $serviceAddress .= (is_null($this->fields['postcode'])) ? '' :
                '<Postcode>'.htmlspecialchars($this->fields['postcode'], ENT_NOQUOTES).'</Postcode>';
            $serviceAddress .= (is_null($this->fields['care_of_name'])) ? '' :
                '<CareOfName>'.htmlspecialchars($this->fields['care_of_name'], ENT_NOQUOTES).'</CareOfName>';
            $serviceAddress .= (is_null($this->fields['po_box'])) ? '' :
                '<PoBox>'.htmlspecialchars($this->fields['po_box'], ENT_NOQUOTES).'</PoBox>';

            // --- identification ---
            $identification = '';

            if ($this->fields['identification_type'] == 'EEA') {
                $identification .= '<PlaceRegistered>'.htmlspecialchars($this->fields['place_registered'], ENT_NOQUOTES).'</PlaceRegistered>';
                $identification .= '<RegistrationNumber>'.htmlspecialchars($this->fields['registration_number'], ENT_NOQUOTES).'</RegistrationNumber>';
                $identification = '<EEA>'.$identification.'</EEA>';
            } else {
                $identification .= (is_null($this->fields['place_registered'])) ? '' :
                    '<PlaceRegistered>'.htmlspecialchars($this->fields['place_registered'], ENT_NOQUOTES).'</PlaceRegistered>';
                $identification .= (is_null($this->fields['registration_number'])) ? '' :
                    '<RegistrationNumber>'.htmlspecialchars($this->fields['registration_number'], ENT_NOQUOTES).'</RegistrationNumber>';
                $identification .= '<LawGoverned>'.htmlspecialchars($this->fields['law_governed'], ENT_NOQUOTES).'</LawGoverned>';
                $identification .= '<LegalForm>'.htmlspecialchars($this->fields['legal_form'], ENT_NOQUOTES).'</LegalForm>';
                $identification = '<NonEEA>'.$identification.'</NonEEA>';
            }

            if ($this->fields['type'] == 'DIR') {
                if (isset($this->fields['designated_ind'])) {
                        $designatedIndex = $this->fields['designated_ind']== 1 ? 'true' : 'false';
                    }
               if($type != 'LLP'){
                    
                $appointment .= '<Director><Corporate>
                                    '.$corporate.'
                                    <Address>
                                        '.$serviceAddress.'
                                    </Address>
                                    <CompanyIdentification>
                                        '.$identification.'
                                    </CompanyIdentification>
                                </Corporate></Director>';
               }else{
                 $appointment .= '<Member><DesignatedInd>'.$designatedIndex.'</DesignatedInd>
                                    <Corporate>
                                    '.$corporate.'
                                    <Address>
                                        '.$serviceAddress.'
                                    </Address>
                                    <CompanyIdentification>
                                        '.$identification.'
                                    </CompanyIdentification>
                                </Corporate></Member>';
               }
            } else {
                $appointment .= '<Secretary><Corporate>
                                    '.$corporate.'
                                    <Address>
                                        '.$serviceAddress.'
                                    </Address>
                                    <CompanyIdentification>
                                        '.$identification.'
                                    </CompanyIdentification>
                                </Corporate></Secretary>';
            }

        // --- person ---
        } else {
            // --- person ---
            $person = '';

            $person .= (is_null($this->fields['title'])) ? '' :
                '<Title>'.htmlspecialchars($this->fields['title'], ENT_NOQUOTES).'</Title>';

            if (!is_null($this->fields['forename'])) {
                $person .= '<Forename>' . htmlspecialchars($this->fields['forename'], ENT_NOQUOTES);

                if (!is_null($this->fields['middle_name'])) {
                    $person .= ' ' . htmlspecialchars($this->fields['middle_name'], ENT_NOQUOTES);
                };
                $person .= '</Forename>';
            }
            
            $person .= '<Surname>'.htmlspecialchars($this->fields['surname'], ENT_NOQUOTES).'</Surname>';

            // --- service address ---
            $serviceAddress = '';

            $serviceAddress .= '<Premise>'.htmlspecialchars($this->fields['premise'], ENT_NOQUOTES).'</Premise>';
            $serviceAddress .= '<Street>'.htmlspecialchars($this->fields['street'], ENT_NOQUOTES).'</Street>';
            $serviceAddress .= (is_null($this->fields['thoroughfare'])) ? '' :
                '<Thoroughfare>'.htmlspecialchars($this->fields['thoroughfare'], ENT_NOQUOTES).'</Thoroughfare>';
            $serviceAddress .= '<PostTown>'.htmlspecialchars($this->fields['post_town'], ENT_NOQUOTES).'</PostTown>';
            $serviceAddress .= (is_null($this->fields['county'])) ? '' :
                '<County>'.htmlspecialchars($this->fields['county'], ENT_NOQUOTES).'</County>';

            if (isset(Address::$nonForeignCountries[$this->fields['country']])) {
                $serviceAddress .= '<Country>'.htmlspecialchars($this->fields['country'], ENT_NOQUOTES).'</Country>';
            } else {
                $serviceAddress .= '<OtherForeignCountry>'.htmlspecialchars($this->fields['country'], ENT_NOQUOTES).'</OtherForeignCountry>';
            }

            $serviceAddress .= (is_null($this->fields['postcode'])) ? '' :
                '<Postcode>'.htmlspecialchars($this->fields['postcode'], ENT_NOQUOTES).'</Postcode>';
            $serviceAddress .= (is_null($this->fields['care_of_name'])) ? '' :
                '<CareOfName>'.htmlspecialchars($this->fields['care_of_name'], ENT_NOQUOTES).'</CareOfName>';
            $serviceAddress .= (is_null($this->fields['po_box'])) ? '' :
                '<PoBox>'.htmlspecialchars($this->fields['po_box'], ENT_NOQUOTES).'</PoBox>';

            // --- director specific ---
            if ($this->fields['type'] == 'DIR') {
                $director = '';

                $director .= '<DOB>'.htmlspecialchars($this->fields['dob'], ENT_NOQUOTES).'</DOB>';
                if($type != 'LLP'){
                    $director .= '<Nationality>'.htmlspecialchars($this->fields['nationality'], ENT_NOQUOTES).'</Nationality>';
                    $director .= '<Occupation>'.htmlspecialchars($this->fields['occupation'], ENT_NOQUOTES).'</Occupation>';
                }
                $director .= '<CountryOfResidence>'.htmlspecialchars($this->fields['country_of_residence'], ENT_NOQUOTES).'</CountryOfResidence>';

                // --- residential address ---
                $residentialAddress = '';

                $residentialAddress .= '<Premise>'.htmlspecialchars($this->fields['residential_premise'], ENT_NOQUOTES).'</Premise>';
                $residentialAddress .= '<Street>'.htmlspecialchars($this->fields['residential_street'], ENT_NOQUOTES).'</Street>';
                $residentialAddress .= (is_null($this->fields['residential_thoroughfare'])) ? '' :
                    '<Thoroughfare>'.htmlspecialchars($this->fields['residential_thoroughfare'], ENT_NOQUOTES).'</Thoroughfare>';
                $residentialAddress .= '<PostTown>'.htmlspecialchars($this->fields['residential_post_town'], ENT_NOQUOTES).'</PostTown>';
                $residentialAddress .= (is_null($this->fields['residential_county'])) ? '' :
                    '<County>'.htmlspecialchars($this->fields['residential_county'], ENT_NOQUOTES).'</County>';

                if (isset(Address::$nonForeignCountries[$this->fields['residential_country']])) {
                    $residentialAddress .= '<Country>'.htmlspecialchars($this->fields['residential_country'], ENT_NOQUOTES).'</Country>';
                } else {
                    $residentialAddress .= '<OtherForeignCountry>'.htmlspecialchars($this->fields['residential_country'], ENT_NOQUOTES).'</OtherForeignCountry>';
                }

                $residentialAddress .= (is_null($this->fields['residential_postcode'])) ? '' :
                    '<Postcode>'.htmlspecialchars($this->fields['residential_postcode'], ENT_NOQUOTES).'</Postcode>';

                // --- secure address ind ---
                $secureAddress = (is_null($this->fields['residential_secure_address_ind'])) ? '' :
                    '<SecureAddressInd>'.(int)$this->fields['residential_secure_address_ind'].'</SecureAddressInd>';
            }

            if ($this->fields['type'] == 'DIR') {
                if (isset($this->fields['designated_ind'])) {
                        $designatedIndex = $this->fields['designated_ind'] == 1 ? 'true' : 'false';
                    }
                   
                 if($type != 'LLP'){
                    
                    $appointment .= '<Director><Person>'.
                                        $person
                                    .'<ServiceAddress>
                                        <Address>'.$serviceAddress.'</Address>
                                    </ServiceAddress>'.
                                        $director
                                    .'<ResidentialAddress>
                                        <Address>'.$residentialAddress.'</Address>'.
                                            $secureAddress
                                    .'</ResidentialAddress>
                                </Person></Director>';
                }else{   
                    $appointment .= '<Member><DesignatedInd>'.$designatedIndex.'</DesignatedInd><Person>'.
                                        $person
                                    .'<ServiceAddress>
                                        <Address>'.$serviceAddress.'</Address>
                                    </ServiceAddress>'.
                                        $director
                                    .'<ResidentialAddress>
                                        <Address>'.$residentialAddress.'</Address>'.
                                            $secureAddress
                                    .'</ResidentialAddress>
                                </Person></Member>';
                }
            } else {
                $appointment .= '<Secretary><Person>'.
                                        $person
                                    .'<ServiceAddress>
                                        <Address>'.$serviceAddress.'</Address>
                                    </ServiceAddress>
                                </Person></Secretary>';
            }
        }
        $appointment_date = (isset($this->fields['appointment_date'])) ? $this->fields['appointment_date'] : date('Y-m-d');

        $xsdSchema = 'http://xml'.$test.'gw.companieshouse.gov.uk/v2-1/schema/forms/OfficerAppointment-v2-7.xsd';

        return '<OfficerAppointment
            xmlns="http://xmlgw.companieshouse.gov.uk"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://xmlgw.companieshouse.gov.uk
            '.$xsdSchema.'">
                <AppointmentDate>'.$appointment_date.'</AppointmentDate>'.
                $authentication .
                $appointment
            .'</OfficerAppointment>';
    }
}


