<?php

class ChangeRegisteredOfficeAddress implements Form {

    /**
     *
     * @var int
     */
    private $formSubmissionId;

    /**
     *
     * @var string
     */
    private $rejectDescription;

    /**
     *
     * @var Address
     */
    private $address;

    /**
     *
     * @var string
     */
    private static $tableName = 'ch_address_change';

    /**
     *
     * @param int $formSubmissionId
     */
    private function  __construct($formSubmissionId) {
        // --- check that formSubmissionId is int ---
        if (!preg_match('/^\d+$/', $formSubmissionId)) {
            throw new Exception('formSubmissionId has to be integer');
        }

        // --- load data from database ---
        $sql = "
            SELECT * FROM ".self::$tableName."
            WHERE `form_submission_id` = $formSubmissionId";
        if (!$result = CHFiling::getDb()->fetchRow($sql)) {
            throw new Exception('Address change doesn\'t exist');
        }

        // --- set instance variables ---
        $this->formSubmissionId = $result['form_submission_id'];
        $this->rejectDescription= $result['reject_description'];

        $this->address          = new Address();
        $this->address->setFields($result);
    }

    /**
     *
     * @param int $formSubmissionId
     * @return CompanyIncorporation
     */
    public static function getChangeRegisteredOfficeAddress($formSubmissionId) {
        return new self($formSubmissionId);
    }

    /**
     *
     * @param int $formSubmissionId
     * @return CompanyIncorporation
     */
    public static function getNewChangeRegisteredOfficeAddress($formSubmissionId) {
        // --- check that formSubmissionId is int ---
        if (!preg_match('/^\d+$/', $formSubmissionId)) {
            throw new Exception('formSubmissionId has to be integer');
        }

        // --- insert new company incorporation ---
        $data = array('form_submission_id'=> $formSubmissionId);
        CHFiling::getDb()->insert(self::$tableName, $data);

        // --- return new instance ---
        return new self($formSubmissionId);
    }

    private function isComplete($compulsoryFields, $fields) {
        // --- check if all fields are set ---
        foreach($compulsoryFields as $v) {
            if (!isset($fields[$v]) || is_null($fields[$v])) {
                return 0;
            }
        }
        return 1;
    }

    public function setAddress(Address $address) {
        $fields = array('premise', 'street', 'post_town', 'country', 'postcode');

        $addressFields = $address->getFields();
        unset($addressFields['secure_address_ind']);
        if (!$this->isComplete($fields, $addressFields)) {
            throw new Exception("Address needs to have all compulsory fields");
        }

        if (!isset(Address::$registeredOfficeCountries[$addressFields['country']])) {
            throw new Exception("Country is not allowed for registered office");
        }

        CHFiling::getDb()->update(self::$tableName, $addressFields, 'form_submission_id = '.$this->formSubmissionId);
        $this->address = clone $address;
    }

    public function getXml() {
        $fields = array('premise', 'street', 'post_town', 'country', 'postcode');

        // --- check if we have all fields set ---
        if (!$this->isComplete($fields, $this->address->getFields())) {
            pr2($this->address->getFields());
            throw new Exception('Address is not set');
        }

        $test = (CHFiling::getGatewayTest()) ? '' : '';


        // --- registered office address ---
        $address = '';

        $address .= '<Premise>'.htmlspecialchars($this->address->getPremise(), ENT_NOQUOTES).'</Premise>';
        $address .= '<Street>'.htmlspecialchars($this->address->getStreet(), ENT_NOQUOTES).'</Street>';
        $address .= (is_null($this->address->getThoroughfare())) ? '' :
            '<Thoroughfare>'.htmlspecialchars($this->address->getThoroughfare(), ENT_NOQUOTES).'</Thoroughfare>';
        $address .= '<PostTown>'.htmlspecialchars($this->address->getPostTown(), ENT_NOQUOTES).'</PostTown>';
        $address .= (is_null($this->address->getCounty())) ? '' :
            '<County>'.htmlspecialchars($this->address->getCounty(), ENT_NOQUOTES).'</County>';
        $address .= '<Country>'.htmlspecialchars($this->address->getCountry(), ENT_NOQUOTES).'</Country>';
        $address .= (is_null($this->address->getPostcode())) ? '' :
            '<Postcode>'.htmlspecialchars($this->address->getPostcode(), ENT_NOQUOTES).'</Postcode>';
        $address .= (is_null($this->address->getCareOfName())) ? '' :
            '<CareOfName>'.htmlspecialchars($this->address->getCareOfName(), ENT_NOQUOTES).'</CareOfName>';
        $address .= (is_null($this->address->getPoBox())) ? '' :
            '<PoBox>'.htmlspecialchars($this->address->getPoBox(), ENT_NOQUOTES).'</PoBox>';

        // --- return xml ---
        return '<ChangeRegisteredOfficeAddress
            xmlns="http://xmlgw.companieshouse.gov.uk"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://xmlgw.companieshouse.gov.uk
            http://xml'.$test.'gw.companieshouse.gov.uk/v2-1/schema/forms/ChangeRegisteredOfficeAddress-v2-5.xsd">
                <Address>'.
                    $address
                .'</Address>
            </ChangeRegisteredOfficeAddress>';
    }
}
