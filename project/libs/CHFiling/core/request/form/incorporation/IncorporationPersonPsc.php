<?php

final class IncorporationPersonPsc extends PersonPsc
{
    /**
     * @var int
     */
    private $incorporationMemberId;

    /**
     *
     * @var int
     */
    private $formSubmissionId;

    /**
     *
     * @var bool
     */
    private $nominee;

    /**
     *
     * @var string
     */
    private static $type = 'PSC';

    /**
     *
     * @var bool
     */
    private static $corporate = 0;

    /**
     *
     * @var string
     */
    private static $tableName = 'ch_incorporation_member';

    /**
     * @param int $formSubmissionId
     * @param int $annualReturnOfficerId
     * @throws Exception
     */
    public function __construct($formSubmissionId, $annualReturnOfficerId = NULL)
    {
        if (!preg_match('/^\d+$/', $formSubmissionId)) {
            throw new Exception('form submissio id has to be integer');
        }

        parent::__construct();

        // --- do not save anything in db ---
        if (!$annualReturnOfficerId) {
            $this->incorporationMemberId = NULL;
            $this->formSubmissionId = $formSubmissionId;

            return;
        }

        if (!preg_match('/^\d+$/', $annualReturnOfficerId)) {
            throw new Exception('incorporation member id has to be integer');
        }

        $sql = "SELECT * FROM `" . self::$tableName . "`
			    WHERE `incorporation_member_id` = $annualReturnOfficerId AND `form_submission_id` = $formSubmissionId";

        if (!$result = CHFiling::getDb()->fetchRow($sql)) {
            throw new Exception("Record doesn't exist");
        }

        $this->incorporationMemberId = $result['incorporation_member_id'];
        $this->formSubmissionId = $result['form_submission_id'];
        $this->nominee = $result['nominee'];
        $this->setFields($result);
    }

    /**
     * @param int $formSubmissionId
     * @param int $incorporationMemberId
     * @return IncorporationPersonPsc
     */
    public static function getIncorporationPersonPsc($formSubmissionId, $incorporationMemberId)
    {
        return new IncorporationPersonPsc($formSubmissionId, $incorporationMemberId);
    }

    /**
     * @param int $formSubmissionId
     * @return IncorporationPersonPsc
     */
    public static function getNewIncorporationPersonPsc($formSubmissionId)
    {
        return new IncorporationPersonPsc($formSubmissionId);
    }

    /**
     * @param int $formSubmissionId
     * @param IncorporationPersonDirector $director
     * @return IncorporationPersonPsc
     */
    public static function getNewIncorporationPersonPscFromDirector($formSubmissionId, IncorporationPersonDirector $director)
    {
        $psc = new IncorporationPersonPsc($formSubmissionId);

        $person = $psc->getPerson();
        $directorPerson = $director->getPerson();
        $person->setTitle($directorPerson->getTitle());
        $person->setNationality($directorPerson->getNationality());
        $person->setForename($directorPerson->getForename());
        $person->setMiddleName($directorPerson->getMiddleName());
        $person->setSurname($directorPerson->getSurname());
        $person->setDob($directorPerson->getDob());
        $person->setCountryOfResidence($directorPerson->getCountryOfResidence());
        $psc->setPerson($person);

        $directorAddress = $director->getAddress();
        $address = $psc->getAddress();
        $address->setFields($directorAddress->getFields());
        $psc->setAddress($address);

        $directorResidentialAddress = $director->getResidentialAddress();
        $residentialAddress = $psc->getResidentialAddress();
        $residentialAddress->setFields($directorResidentialAddress->getFields());
        $psc->setResidentialAddress($residentialAddress);

        $psc->setConsentToAct(1);

        return $psc;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return self::$type;
    }

    public function remove()
    {
        CHFiling::getDb()->delete(self::$tableName, 'incorporation_member_id = ' . $this->incorporationMemberId);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->incorporationMemberId;
    }

    /**
     * @param bool $nominee
     */
    public function setNominee($nominee)
    {
        $this->nominee = ($nominee) ? 1 : 0;
    }

    /**
     * @return bool
     */
    public function isNominee()
    {
        return $this->nominee;
    }

    /**
     * @return bool
     */
    public function isComplete()
    {
        $allowedFields = [
            'authentication', 'surname', 'premise', 'street', 'post_town', 'country',
            'dob', 'nationality', 'country_of_residence',
            'residential_premise', 'residential_street', 'residential_post_town',
            'residential_country',
        ];

        $fields = array_merge(
            $this->getPerson()->getFields(),
            $this->getAddress()->getFields(),
            $this->getResidentialAddress()->getFields('residential_'),
            $this->getNatureOfControl()->getFields()
        );

        // --- set authentication in one field ---
        if (!is_null($this->getAuthentication()->getDbFields())) {
            $fields['authentication'] = $this->getAuthentication()->getDbFields();
        }

        // --- check if all fields are set ---
        foreach ($allowedFields as $v) {
            if (!isset($fields[$v]) || is_null($fields[$v])) {

                return 0;
            }
        }

        return 1;
    }

    /**
     * @throws Exception
     */
    public function save()
    {
        if (!$this->isComplete()) {
            throw new Exception('You need to set all compulsory fields');
        }

        // --- prepare data ---
        $address = $this->getAddress()->getFields();
        unset($address['secure_address_ind']);
        $data = array_merge(
            $this->getPerson()->getFields(),
            $address,
            $this->getResidentialAddress()->getFields('residential_'),
            $this->getNatureOfControl()->getFields()
        );

        // --- these two fields are not for residential address ---
        unset($data['residential_care_of_name']);
        unset($data['residential_po_box']);
        // --- set authentication in one field ---
        $data['authentication'] = $this->getAuthentication()->getDbFields();
        $data['form_submission_id'] = $this->formSubmissionId;
        $data['corporate'] = self::$corporate;
        $data['type'] = self::$type;
        $data['nominee'] = $this->nominee;
        $data['consentToAct'] = $this->hasConsentToAct();

        // --- save data ---
        if (isset($this->incorporationMemberId) && !is_null($this->incorporationMemberId)) {
            CHFiling::getDb()->update(
                self::$tableName,
                $data,
                'incorporation_member_id = ' . $this->incorporationMemberId
            );
        } else {
            CHFiling::getDb()->insert(self::$tableName, $data);
            $this->incorporationMemberId = CHFiling::getDb()->lastInsertId();
        }
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getXml()
    {
        // --- check if is complete ---
        if (!$this->isComplete()) {
            throw new Exception('This object is not complete');
        }

        // --- prepare xml ---
        $companyType = CompanyIncorporation::getCompanyIncorporation($this->formSubmissionId)->getCompanyType();

        // --- person ---
        $person = '';
        $personFields = $this->getPerson();

        $person .= (is_null($personFields->getTitle())) ? '' :
            '<Title>' . htmlspecialchars($personFields->getTitle(), ENT_NOQUOTES) . '</Title>';
        if (!is_null($personFields->getForename())) {
            $person .= '<Forename>' . htmlspecialchars($personFields->getForename(), ENT_NOQUOTES);

            if (!is_null($personFields->getMiddleName())) {
                $person .= ' ' . htmlspecialchars($personFields->getMiddleName(), ENT_NOQUOTES);
            };
            $person .= '</Forename>';
        }
        $person .= '<Surname>' . htmlspecialchars($personFields->getSurname(), ENT_NOQUOTES) . '</Surname>';

        $personAdditional = '<DOB>'.htmlspecialchars($personFields->getDob(), ENT_NOQUOTES).'</DOB>';
        //todo: ??
        //if($type != 'LLP') {
            $personAdditional .= '<Nationality>'.htmlspecialchars($personFields->getNationality(), ENT_NOQUOTES).'</Nationality>';
        //}
        $personAdditional .= '<CountryOfResidence>'.htmlspecialchars($personFields->getCountryOfResidence(), ENT_NOQUOTES).'</CountryOfResidence>';

        // --- service address ---
        $serviceAddress = '';
        $serviceAddressFields = $this->getAddress();

        $serviceAddress .= '<Premise>' . htmlspecialchars($serviceAddressFields->getPremise(), ENT_NOQUOTES) . '</Premise>';
        $serviceAddress .= '<Street>' . htmlspecialchars($serviceAddressFields->getStreet(), ENT_NOQUOTES) . '</Street>';
        $serviceAddress .= (is_null($serviceAddressFields->getThoroughfare())) ? '' : '<Thoroughfare>' . htmlspecialchars($serviceAddressFields->getThoroughfare(), ENT_NOQUOTES) . '</Thoroughfare>';
        $serviceAddress .= '<PostTown>' . htmlspecialchars($serviceAddressFields->getPostTown(), ENT_NOQUOTES) . '</PostTown>';

        //fix empty and -- Select -- in county problems
        $serviceAddress .= (is_null($serviceAddressFields->getCounty())) || strlen(stristr($serviceAddressFields->getCounty(), 'select')) > 0 ? '' : '<County>' . htmlspecialchars($serviceAddressFields->getCounty(), ENT_NOQUOTES) . '</County>';

        if (isset(Address::$nonForeignCountries[$serviceAddressFields->getCountry()])) {
            $serviceAddress .= '<Country>' . htmlspecialchars($serviceAddressFields->getCountry(), ENT_NOQUOTES) . '</Country>';
        } else {
            $serviceAddress .= '<OtherForeignCountry>' . htmlspecialchars($serviceAddressFields->getCountry(), ENT_NOQUOTES) . '</OtherForeignCountry>';
        }

        $serviceAddress .= (is_null($serviceAddressFields->getPostcode())) ? '' :
            '<Postcode>' . htmlspecialchars($serviceAddressFields->getPostcode(), ENT_NOQUOTES) . '</Postcode>';
        $serviceAddress .= (is_null($serviceAddressFields->getCareOfName())) ? '' :
            '<CareOfName>' . htmlspecialchars($serviceAddressFields->getCareOfName(), ENT_NOQUOTES) . '</CareOfName>';
        $serviceAddress .= (is_null($serviceAddressFields->getPoBox())) ? '' :
            '<PoBox>' . htmlspecialchars($serviceAddressFields->getPoBox(), ENT_NOQUOTES) . '</PoBox>';

        // --- residential address ---
        $residentialAddress = '';
        $residentialAddressFields = $this->getResidentialAddress();

        $residentialAddress .= '<Premise>'.htmlspecialchars($residentialAddressFields->getPremise(), ENT_NOQUOTES).'</Premise>';
        $residentialAddress .= '<Street>'.htmlspecialchars($residentialAddressFields->getStreet(), ENT_NOQUOTES).'</Street>';
        $residentialAddress .= (is_null($residentialAddressFields->getThoroughfare())) ? '' :
            '<Thoroughfare>'.htmlspecialchars($residentialAddressFields->getThoroughfare(), ENT_NOQUOTES).'</Thoroughfare>';
        $residentialAddress .= '<PostTown>'.htmlspecialchars($residentialAddressFields->getPostTown(), ENT_NOQUOTES).'</PostTown>';

        //fix empty and -- Select -- in county problems
        $residentialAddress .= (is_null($residentialAddressFields->getCounty()))  || strlen(stristr($residentialAddressFields->getCounty(), 'select'))>0  ? '' :
            '<County>'.htmlspecialchars($residentialAddressFields->getCounty(), ENT_NOQUOTES).'</County>';

        if (isset(Address::$nonForeignCountries[$residentialAddressFields->getCountry()])) {
            $residentialAddress .= '<Country>'.htmlspecialchars($residentialAddressFields->getCountry(), ENT_NOQUOTES).'</Country>';
        } else {
            $residentialAddress .= '<OtherForeignCountry>'.htmlspecialchars($residentialAddressFields->getCountry(), ENT_NOQUOTES).'</OtherForeignCountry>';
        }

        $residentialAddress .= (is_null($residentialAddressFields->getPostcode())) ? '' :
            '<Postcode>'.htmlspecialchars($residentialAddressFields->getPostcode(), ENT_NOQUOTES).'</Postcode>';
        $residentialAddress .= (is_null($residentialAddressFields->getCareOfName())) ? '' :
            '<CareOfName>'.htmlspecialchars($residentialAddressFields->getCareOfName(), ENT_NOQUOTES).'</CareOfName>';
        $residentialAddress .= (is_null($residentialAddressFields->getPoBox())) ? '' :
            '<PoBox>'.htmlspecialchars($residentialAddressFields->getPoBox(), ENT_NOQUOTES).'</PoBox>';

        $natureOfControlsFields = $this->getNatureOfControl();

        $natureOfControls = $companyType == CompanyIncorporation::LIMITED_LIABILITY_PARTHENERSHIP ? '<LLPNatureOfControls>' : '<NatureOfControls>';
        if ($natureOfControlsFields->getOwnershipOfShares()) {
            $natureOfControls .= '<NatureOfControl>'.htmlspecialchars($natureOfControlsFields->getOwnershipOfShares(), ENT_NOQUOTES).'</NatureOfControl>';
        }
        if ($natureOfControlsFields->getOwnershipOfVotingRights()) {
            $natureOfControls .= '<NatureOfControl>'.htmlspecialchars($natureOfControlsFields->getOwnershipOfVotingRights(), ENT_NOQUOTES).'</NatureOfControl>';
        }
        if ($natureOfControlsFields->getRightToAppointAndRemoveDirectors()) {
            $natureOfControls .= '<NatureOfControl>'.htmlspecialchars($natureOfControlsFields->getRightToAppointAndRemoveDirectors(), ENT_NOQUOTES).'</NatureOfControl>';
        }
        if ($natureOfControlsFields->getSignificantInfluenceOrControl()) {
            $natureOfControls .= '<NatureOfControl>'.htmlspecialchars($natureOfControlsFields->getSignificantInfluenceOrControl(), ENT_NOQUOTES).'</NatureOfControl>';
        }
        $natureOfControls .= $companyType == CompanyIncorporation::LIMITED_LIABILITY_PARTHENERSHIP ? '</LLPNatureOfControls>' : '</NatureOfControls>';

        // --- return xml ---
        return "
        <PSC>
            <PSCNotification>
                <Individual>
                    $person
                    <ServiceAddress>
                        <Address>
                            $serviceAddress
                        </Address>
                    </ServiceAddress>
                    $personAdditional
                    <ResidentialAddress>
                        <Address>
                            $residentialAddress
                        </Address>                    
                    </ResidentialAddress>
                    <ConsentStatement>true</ConsentStatement>
                </Individual>
                $natureOfControls
            </PSCNotification>
        </PSC>";
    }
}

