<?php

final class IncorporationCorporateSecretary extends CorporateSecretary{

    /**
     *
     * @var int
     */
    private $incorporationMemberId;

    /**
     *
     * @var int
     */
    private $formSubmissionId;

    /**
     *
     * @var boolean
     */
    private $nominee;

    /**
     *
     * @var string
     */
    private static $type = 'SEC';

    /**
     *
     * @var boolean
     */
    private static $corporate = 1;

    /**
     *
     * @var string
     */
    private static $tableName = 'ch_incorporation_member';

    /**
     *
     * @param int $formSubmissionId
     * @param int $annualReturnOfficerId
     * @param int $formSubmissionId
     */
    public function  __construct($formSubmissionId, $annualReturnOfficerId = null) {
        // --- check that formSubmissionId is int ---
        if (!preg_match('/^\d+$/', $formSubmissionId)) {
            throw new Exception('form submissio id has to be integer');
        }

        parent::__construct();

        // --- do not save anything in db ---
        if (is_null($annualReturnOfficerId)) {
            $this->incorporationMemberId    = null;
            $this->formSubmissionId         = $formSubmissionId;
            return;
        }

        // --- check that incorporationMemberId is int ---
        if (!preg_match('/^\d+$/', $annualReturnOfficerId)) {
            throw new Exception('incorporation member id has to be integer');
        }

        $sql = "
			SELECT * FROM `".self::$tableName."`
			WHERE `incorporation_member_id` = $annualReturnOfficerId AND `form_submission_id` = $formSubmissionId";
        if (!$result = CHFiling::getDb()->fetchRow($sql)) {
            throw new Exception('Record doesn\'t exist');
        }

        // --- set instance variables ---
		$this->incorporationMemberId = $result['incorporation_member_id'];
        $this->formSubmissionId = $result['form_submission_id'];
        $this->nominee          = $result['nominee'];
        $this->setFields($result);
    }

    /**
     *
     * @param int $formSubmissionId
     * @param int $incorporationMemberId
     * @return IncorporationCorporateSecretary
     */
    public static function getIncorporationCorporateSecretary($formSubmissionId, $incorporationMemberId) {
        return new IncorporationCorporateSecretary($formSubmissionId, $incorporationMemberId);
    }

    /**
     *
     * @param int $formSubmissionId
     * @return IncorporationCorporateSecretary
     */
    public static function getNewIncorporationCorporateSecretary($formSubmissionId) {
        return new IncorporationCorporateSecretary($formSubmissionId);
    }

    /**
     *
     * @return string
     */
    public function getType() {
        return self::$type;
    }

    /**
     *
     */
    public function remove() {
        CHFiling::getDb()->delete(self::$tableName, 'incorporation_member_id = '.$this->incorporationMemberId);
    }

    /**
     *
     * @return int
     */
    public function getId() {
        return $this->incorporationMemberId;
    }

    /**
     *
     * @param boolean $nominee
     */
    public function setNominee($nominee) {
        $this->nominee = ($nominee) ? 1 : 0;
    }

    /**
     *
     */
    public function isNominee() {
        return $this->nominee;
    }

    /**
     *
     * @return boolean
     */
    public function isComplete() {
        $allowedFields = array(
            'authentication', 'surname', 'corporate_name',
            'premise', 'street', 'post_town', 'country',
            'identification_type'
        );

        $fields = array_merge(
            $this->getCorporate()->getFields(),
            $this->getAddress()->getFields(),
            $this->getIdentification()->getFields()
        );
        $fields['identification_type'] = $this->getIdentification()->getTypeName();

        // --- set authentication in one field ---
        if (!is_null($this->getAuthentication()->getDbFields())) {
            $fields['authentication'] = $this->getAuthentication()->getDbFields();
        }

        // --- check if all fields are set ---
        foreach($allowedFields as $v) {
            if (!isset($fields[$v]) || is_null($fields[$v])) {
                return 0;
            }
        }

        // --- check identification ---
        if ($fields['identification_type'] == 'EEA') {
            if (!isset($fields['place_registered']) || is_null($fields['place_registered'])) {
                return 0;
            }
            if (!isset($fields['registration_number']) || is_null($fields['registration_number'])) {
                return 0;
            }
        } else {
            if (!isset($fields['law_governed']) || is_null($fields['law_governed'])) {
                return 0;
            }
            if (!isset($fields['legal_form']) || is_null($fields['legal_form'])) {
                return 0;
            }
        }

        return 1;
    }

    /**
     *
     */
    public function save() {
        if (!$this->isComplete()) {
            throw new Exception('You need to set all compulsory fielsd');
        }

        // --- prepare data ---
        $address = $this->getAddress()->getFields();
        unset($address['secure_address_ind']);
        $data = array_merge(
            $this->getCorporate()->getFields(),
            $address,
            $this->getIdentification()->getFields()
        );
        unset($data['eeaType']); // - this is for population form
        $data['identification_type'] = $this->getIdentification()->getTypeName();
        $data['authentication'] = $this->getAuthentication()->getDbFields();
        $data['form_submission_id'] = $this->formSubmissionId;
        $data['corporate']          = self::$corporate;
        $data['type']               = self::$type;
        $data['nominee']            = $this->nominee;
        $data['consentToAct'] = $this->hasConsentToAct();

        // --- save data ---
        if (isset($this->incorporationMemberId) && !is_null($this->incorporationMemberId)) {
            CHFiling::getDb()->update(self::$tableName, $data, 'incorporation_member_id = '.$this->incorporationMemberId);
        } else {
            CHFiling::getDb()->insert(self::$tableName, $data);
            $this->incorporationMemberId = CHFiling::getDb()->lastInsertId();
        }
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getXml() {
        // --- check if is complete ---
        if (!$this->isComplete()) {
            throw new Exception('This object is not complete');
        }

        // --- authentication ---
        if ($this->hasConsentToAct()) {
            $authentication = '<ConsentToAct>true</ConsentToAct>';
        } else {
            $authentication = '<ConsentToAct>false</ConsentToAct>';
        }

        // --- prepare xml ---

        // --- corporate ---
        $corporate = '';
        $corporateFields = $this->getCorporate();

        $corporate .= (is_null($corporateFields->getForename())) ? '' :
            '<Forename>'.htmlspecialchars($corporateFields->getForename(), ENT_NOQUOTES).'</Forename>';
        $corporate .= '<Surname>'.htmlspecialchars($corporateFields->getSurname(), ENT_NOQUOTES).'</Surname>';
        $corporate .= '<CorporateName>'.htmlspecialchars($corporateFields->getCorporateName(), ENT_NOQUOTES).'</CorporateName>';

        // --- address ---
        $address = '';
        $addressFields = $this->getAddress();

        $address .= '<Premise>'.htmlspecialchars($addressFields->getPremise(), ENT_NOQUOTES).'</Premise>';
        $address .= '<Street>'.htmlspecialchars($addressFields->getStreet(), ENT_NOQUOTES).'</Street>';
        $address .= (is_null($addressFields->getThoroughfare())) ? '' :
            '<Thoroughfare>'.htmlspecialchars($addressFields->getThoroughfare(), ENT_NOQUOTES).'</Thoroughfare>';
        $address .= '<PostTown>'.htmlspecialchars($addressFields->getPostTown(), ENT_NOQUOTES).'</PostTown>';
        
        //fix empty and -- Select -- in county problems
        $address .= (is_null($addressFields->getCounty())) || strlen(stristr($addressFields->getCounty(),'select'))>0 ? '' :
            '<County>'.htmlspecialchars($addressFields->getCounty(), ENT_NOQUOTES).'</County>';

        if (isset(Address::$nonForeignCountries[$addressFields->getCountry()])) {
            $address .= '<Country>'.htmlspecialchars($addressFields->getCountry(), ENT_NOQUOTES).'</Country>';
        } else {
            $address .= '<OtherForeignCountry>'.htmlspecialchars($addressFields->getCountry(), ENT_NOQUOTES).'</OtherForeignCountry>';
        }

        $address .= (is_null($addressFields->getPostcode())) ? '' :
            '<Postcode>'.htmlspecialchars($addressFields->getPostcode(), ENT_NOQUOTES).'</Postcode>';
        $address .= (is_null($addressFields->getCareOfName())) ? '' :
            '<CareOfName>'.htmlspecialchars($addressFields->getCareOfName(), ENT_NOQUOTES).'</CareOfName>';
        $address .= (is_null($addressFields->getPoBox())) ? '' :
            '<PoBox>'.htmlspecialchars($addressFields->getPoBox(), ENT_NOQUOTES).'</PoBox>';

        // --- identification ---
        $identification = '';
        $identificationFields = $this->getIdentification();

        if ($identificationFields->getTypeName() == 'EEA') {
            $identification .= '<PlaceRegistered>'.htmlspecialchars($identificationFields->getPlaceRegistered(), ENT_NOQUOTES).'</PlaceRegistered>';
            $identification .= '<RegistrationNumber>'.htmlspecialchars($identificationFields->getRegistrationNumber(), ENT_NOQUOTES).'</RegistrationNumber>';
            $identification = '<EEA>'.$identification.'</EEA>';
        } else {
            $identification .= (is_null($identificationFields->getPlaceRegistered())) ? '' :
                '<PlaceRegistered>'.htmlspecialchars($identificationFields->getPlaceRegistered(), ENT_NOQUOTES).'</PlaceRegistered>';
            $identification .= (is_null($identificationFields->getRegistrationNumber())) ? '' :
                '<RegistrationNumber>'.htmlspecialchars($identificationFields->getRegistrationNumber(), ENT_NOQUOTES).'</RegistrationNumber>';
            $identification .= '<LawGoverned>'.htmlspecialchars($identificationFields->getLawGoverned(), ENT_NOQUOTES).'</LawGoverned>';
            $identification .= '<LegalForm>'.htmlspecialchars($identificationFields->getLegalForm(), ENT_NOQUOTES).'</LegalForm>';
            $identification = '<NonEEA>'.$identification.'</NonEEA>';
        }

        // --- return xml ---
        return "<Appointment>
            $authentication
            <Secretary>
                <Corporate>
                    $corporate
                    <Address>
                        $address
                    </Address>
                    <CompanyIdentification>
                        $identification
                    </CompanyIdentification>
                </Corporate>
            </Secretary>
        </Appointment>";
    }
}
