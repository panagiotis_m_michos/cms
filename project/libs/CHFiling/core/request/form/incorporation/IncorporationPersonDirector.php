<?php

final class IncorporationPersonDirector extends PersonDirector
{

    /**
     *
     * @var int
     */
    private $incorporationMemberId;

    /**
     *
     * @var int
     */
    private $formSubmissionId;

    /**
     *
     * @var boolean
     */
    private $nominee;

    /**
     * @var string
     */
    private $nomineeType;

    /**
     *
     * @var string
     */
    private static $type = 'DIR';

    /**
     *
     * @var boolean
     */
    private static $corporate = 0;

    /**
     *
     * @var string
     */
    private static $tableName = 'ch_incorporation_member';

    /**
     *
     * @param int $formSubmissionId
     * @param int $annualReturnOfficerId
     * @param int $formSubmissionId
     */
    public function  __construct($formSubmissionId, $annualReturnOfficerId = NULL) 
    {
        // --- check that formSubmissionId is int ---
        if (!preg_match('/^\d+$/', $formSubmissionId)) {
            throw new Exception('form submissio id has to be integer');
        }

        parent::__construct();

        // --- do not save anything in db ---
        if (is_null($annualReturnOfficerId)) {
            $this->incorporationMemberId    = NULL;
            $this->formSubmissionId         = $formSubmissionId;
            return;
        }

        // --- check that incorporationMemberId is int ---
        if (!preg_match('/^\d+$/', $annualReturnOfficerId)) {
            throw new Exception('incorporation member id has to be integer');
        }

        $sql = "
			SELECT * FROM `".self::$tableName."`
			WHERE `incorporation_member_id` = $annualReturnOfficerId AND `form_submission_id` = $formSubmissionId";
        if (!$result = CHFiling::getDb()->fetchRow($sql)) {
            throw new Exception('Record doesn\'t exist');
        }

        // --- set instance variables ---
        $this->incorporationMemberId = $result['incorporation_member_id'];
        $this->formSubmissionId = $result['form_submission_id'];
        $this->nominee          = $result['nominee'];
        $this->nomineeType          = $result['nomineeType'];
        $this->setFields($result);
    }

    /**
     *
     * @param int $incorporationMemberId
     * @return IncorporationPersonDirector 
     */
    public static function getIncorporationPersonDirector($formSubmissionId, $incorporationMemberId) 
    {
        return new IncorporationPersonDirector($formSubmissionId, $incorporationMemberId);
    }

    /**
     *
     * @param int $formSubmissionId
     * @return IncorporationPersonDirector 
     */
    public static function getNewIncorporationPersonDirector($formSubmissionId) 
    {
        return new IncorporationPersonDirector($formSubmissionId);
    }

    /**
     *
     * @return string
     */
    public function getType() 
    {
        return self::$type;
    }

    /**
     * 
     */
    public function remove() 
    {
        CHFiling::getDb()->delete(self::$tableName, 'incorporation_member_id = '.$this->incorporationMemberId);
    }

    /**
     *
     * @return int
     */
    public function getId() 
    {
        return $this->incorporationMemberId;
    }

    /**
     *
     * @param boolean $nominee 
     */
    public function setNominee($nominee) 
    {
        $this->nominee = ($nominee) ? 1 : 0;
    }

    /**
     * 
     */
    public function isNominee() 
    {
        return $this->nominee;
    }

    /**
     * @return string
     */
    public function getnomineeType()
    {
        return $this->nomineeType;
    }

    /**
     * @param string $nomineeType
     */
    public function setNomineeType($nomineeType)
    {
        $this->nomineeType = $nomineeType;
    }

    /**
     *
     * @return boolean
     */ 
    public function isComplete() 
    {
        $allowedFields = array(
            'authentication', 'surname', 'premise', 'street', 'post_town', 'country',
            'dob', 'nationality', 'occupation', 'country_of_residence',
            'residential_premise', 'residential_street', 'residential_post_town', 
            'residential_country'
        );
        
        $compincorp = CompanyIncorporation::getCompanyIncorporation($this->formSubmissionId);
        $type = $compincorp->getType();

        if($type == 'LLP') {
            unset($allowedFields[7]);
            unset($allowedFields[8]);
        }

        $fields = array_merge(
            $this->getPerson()->getFields(),
            $this->getAddress()->getFields(),
            $this->getResidentialAddress()->getFields('residential_')
        );

        // --- set authentication in one field ---
        if (!is_null($this->getAuthentication()->getDbFields())) {
            $fields['authentication'] = $this->getAuthentication()->getDbFields();
        }

        // --- check if all fields are set ---
        foreach($allowedFields as $v) {
            if (!isset($fields[$v]) || is_null($fields[$v])) {
                return 0;
            }
        }

        return 1;
    }

    /**
     *
     */
    public function save() 
    {
        if (!$this->isComplete()) {
            throw new Exception('You need to set all compulsory fields');
        }

        // --- prepare data ---
        $address = $this->getAddress()->getFields();
        unset($address['secure_address_ind']);
        $data = array_merge(
            $this->getPerson()->getFields(),
            $address,
            $this->getResidentialAddress()->getFields('residential_')
        );
        // --- these two fields are not for residential address ---
        unset($data['residential_care_of_name']);
        unset($data['residential_po_box']);
        $data['authentication'] = $this->getAuthentication()->getDbFields();
        $data['form_submission_id'] = $this->formSubmissionId;
        $data['corporate']          = self::$corporate;
        $data['type']               = self::$type;
        $data['nominee']            = $this->nominee;
        $data['nomineeType']            = $this->nomineeType;
        $data['consentToAct'] = $this->hasConsentToAct();

        // --- save data ---
        if (isset($this->incorporationMemberId) && !is_null($this->incorporationMemberId)) {
            CHFiling::getDb()->update(self::$tableName, $data, 'incorporation_member_id = '.$this->incorporationMemberId);
        } else {
            CHFiling::getDb()->insert(self::$tableName, $data);
            $this->incorporationMemberId = CHFiling::getDb()->lastInsertId();
        }
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getXml()
    {
        
        //get company type
        $compincorp = CompanyIncorporation::getCompanyIncorporation($this->formSubmissionId);
        $type = $compincorp->getType();
        
        
        // --- check if is complete ---
        if (!$this->isComplete()) {
            throw new Exception('This object is not complete');
        }

        // --- authentication ---
        if ($this->hasConsentToAct()) {
            $authentication = '<ConsentToAct>true</ConsentToAct>';
        } else {
            $authentication = '<ConsentToAct>false</ConsentToAct>';
        }

        // --- prepare xml ---

        // --- person ---
        $person = '';
        $personFields = $this->getPerson();

        $person .= (is_null($personFields->getTitle())) ? '' :
            '<Title>'.htmlspecialchars($personFields->getTitle(), ENT_NOQUOTES).'</Title>';
        
        if (!is_null($personFields->getForename())) {
            $person .= '<Forename>'.htmlspecialchars($personFields->getForename(), ENT_NOQUOTES);
            
            if(!is_null($personFields->getMiddleName())) { 
                $person .=' '.htmlspecialchars($personFields->getMiddleName(), ENT_NOQUOTES);
            };
            $person .= '</Forename>';
        }
        
        $person .= '<Surname>'.htmlspecialchars($personFields->getSurname(), ENT_NOQUOTES).'</Surname>';

        // --- service address ---
        $serviceAddress = '';
        $serviceAddressFields = $this->getAddress();

        $serviceAddress .= '<Premise>'.htmlspecialchars($serviceAddressFields->getPremise(), ENT_NOQUOTES).'</Premise>';
        $serviceAddress .= '<Street>'.htmlspecialchars($serviceAddressFields->getStreet(), ENT_NOQUOTES).'</Street>';
        $serviceAddress .= (is_null($serviceAddressFields->getThoroughfare())) ? '' :
            '<Thoroughfare>'.htmlspecialchars($serviceAddressFields->getThoroughfare(), ENT_NOQUOTES).'</Thoroughfare>';
        $serviceAddress .= '<PostTown>'.htmlspecialchars($serviceAddressFields->getPostTown(), ENT_NOQUOTES).'</PostTown>';
        
        //fix empty and -- Select -- in county problems
        $serviceAddress .= (is_null($serviceAddressFields->getCounty())) || strlen(stristr($serviceAddressFields->getCounty(), 'select'))>0 ? '' :
            '<County>'.htmlspecialchars($serviceAddressFields->getCounty(), ENT_NOQUOTES).'</County>';

        if (isset(Address::$nonForeignCountries[$serviceAddressFields->getCountry()])) {
            $serviceAddress .= '<Country>'.htmlspecialchars($serviceAddressFields->getCountry(), ENT_NOQUOTES).'</Country>';
        } else {
            $serviceAddress .= '<OtherForeignCountry>'.htmlspecialchars($serviceAddressFields->getCountry(), ENT_NOQUOTES).'</OtherForeignCountry>';
        }

        $serviceAddress .= (is_null($serviceAddressFields->getPostcode())) ? '' :
            '<Postcode>'.htmlspecialchars($serviceAddressFields->getPostcode(), ENT_NOQUOTES).'</Postcode>';
        $serviceAddress .= (is_null($serviceAddressFields->getCareOfName())) ? '' :
            '<CareOfName>'.htmlspecialchars($serviceAddressFields->getCareOfName(), ENT_NOQUOTES).'</CareOfName>';
        $serviceAddress .= (is_null($serviceAddressFields->getPoBox())) ? '' :
            '<PoBox>'.htmlspecialchars($serviceAddressFields->getPoBox(), ENT_NOQUOTES).'</PoBox>';

        // --- director specific ---
        $director = '';

        $director .= '<DOB>'.htmlspecialchars($personFields->getDob(), ENT_NOQUOTES).'</DOB>';
        if($type != 'LLP') {
            $director .= '<Nationality>'.htmlspecialchars($personFields->getNationality(), ENT_NOQUOTES).'</Nationality>';
            $director .= '<Occupation>'.htmlspecialchars($personFields->getOccupation(), ENT_NOQUOTES).'</Occupation>';
        }
        $director .= '<CountryOfResidence>'.htmlspecialchars($personFields->getCountryOfResidence(), ENT_NOQUOTES).'</CountryOfResidence>';

        // --- residential address ---
        $residentialAddress = '';
        $residentialAddressFields = $this->getResidentialAddress();

        $residentialAddress .= '<Premise>'.htmlspecialchars($residentialAddressFields->getPremise(), ENT_NOQUOTES).'</Premise>';
        $residentialAddress .= '<Street>'.htmlspecialchars($residentialAddressFields->getStreet(), ENT_NOQUOTES).'</Street>';
        $residentialAddress .= (is_null($residentialAddressFields->getThoroughfare())) ? '' :
            '<Thoroughfare>'.htmlspecialchars($residentialAddressFields->getThoroughfare(), ENT_NOQUOTES).'</Thoroughfare>';
        $residentialAddress .= '<PostTown>'.htmlspecialchars($residentialAddressFields->getPostTown(), ENT_NOQUOTES).'</PostTown>';
        
        
        
        //fix empty and -- Select -- in county problems
        $residentialAddress .= (is_null($residentialAddressFields->getCounty()))  || strlen(stristr($residentialAddressFields->getCounty(), 'select'))>0  ? '' :
            '<County>'.htmlspecialchars($residentialAddressFields->getCounty(), ENT_NOQUOTES).'</County>';

        if (isset(Address::$nonForeignCountries[$residentialAddressFields->getCountry()])) {
            $residentialAddress .= '<Country>'.htmlspecialchars($residentialAddressFields->getCountry(), ENT_NOQUOTES).'</Country>';
        } else {
            $residentialAddress .= '<OtherForeignCountry>'.htmlspecialchars($residentialAddressFields->getCountry(), ENT_NOQUOTES).'</OtherForeignCountry>';
        }

        $residentialAddress .= (is_null($residentialAddressFields->getPostcode())) ? '' :
            '<Postcode>'.htmlspecialchars($residentialAddressFields->getPostcode(), ENT_NOQUOTES).'</Postcode>';
        $residentialAddress .= (is_null($residentialAddressFields->getCareOfName())) ? '' :
            '<CareOfName>'.htmlspecialchars($residentialAddressFields->getCareOfName(), ENT_NOQUOTES).'</CareOfName>';
        $residentialAddress .= (is_null($residentialAddressFields->getPoBox())) ? '' :
            '<PoBox>'.htmlspecialchars($residentialAddressFields->getPoBox(), ENT_NOQUOTES).'</PoBox>';

        $secureAddressInd = '';
        if (!is_null($residentialAddressFields->getSecureAddressInd())) {
            $secureAddressInd = '<SecureAddressInd>'.(int) $residentialAddressFields->getSecureAddressInd().'</SecureAddressInd>';
        }
        
        $directoro = ($type == 'LLP') ? '<Member><DesignatedInd>true</DesignatedInd>': '<Director>';
        $directorc = ($type == 'LLP') ? '</Member>': '</Director>';
       
        // --- return xml ---
        return "<Appointment>
            $authentication
            $directoro
        
                <Person>
                    $person
                    <ServiceAddress>
                        <Address>
                            $serviceAddress
                        </Address>
                    </ServiceAddress>
                    $director
                    <ResidentialAddress>
                        <Address>
                            $residentialAddress
                        </Address>
                            $secureAddressInd
                    </ResidentialAddress>
                </Person>
            $directorc
        </Appointment>";
    }
}
