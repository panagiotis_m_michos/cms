<?php

use CompanyFormationModule\ValueObjects\PossibleIncorporationPersonPsc;
use DusanKasan\Knapsack\Collection;
use PeopleWithSignificantControl\Interfaces\IPscAware;
use function DusanKasan\Knapsack\only;

class CompanyIncorporation implements Form, IPscAware
{
    const LIMITED_BY_SHARES = 'BYSHR';
    const PUBLIC_LIMITED = 'PLC';
    const BY_GUARANTEE = 'BYGUAR';
    const BY_GUARANTEE_EXEMPT = 'BYGUAREXEMPT';
    const LIMITED_LIABILITY_PARTHENERSHIP = 'LLP';

    /**
     * @var array
     */
    public static $types = [
        self::LIMITED_BY_SHARES => 'Private Company Limited by Shares',
        self::PUBLIC_LIMITED => 'Public Limited Company',
        self::BY_GUARANTEE => 'Private Limited Company by Guarantee',
        self::BY_GUARANTEE_EXEMPT => 'Private Company Limited by Guarantee exempt under section 30',
        self::LIMITED_LIABILITY_PARTHENERSHIP => 'Limited Liability Partnership'
    ];

    /**
     * @var int
     */
    private $formSubmissionId;

    /**
     *
     * @var string ('PLC', 'BYSHR', 'BYGUAR', 'BYGUAREXEMPT')
     */
    private $type;

    /**
     *
     * @var string
     */
    private $rejectReference;

    /**
     *
     * @var string
     */
    private $rejectDescription;

    /**
     *
     * @var boolean
     */
    private $sameDay;

    /**
     *
     * @var enum('EW,'SC','WA','NI')
     */
    private $countryOfIncorporation;

    /**
     *
     * @var enum('BYSHRMODEL', 'BYGUARMODEL', 'PLCMODEL', 'BYSHAREAMEND', 'BYGUARAMEND', 'PLCAMEND', 'BESPOKE')
     */
    private $articles;

    /**
     *
     * @var boolean
     */
    private $restrictedArticles;

    /**
     *
     * @var boolean
     */
    private $sameName;

    /**
     *
     * @var boolean
     */
    private $nameAuthorisation;

    /**
     * @var string
     */
    private $noPscReason;

    /**
     *
     * @var Address
     */
    private $address;

    /**
     * array of all incorporation capitals
     * that belong to this incorporation
     *
     * @var array<IncorporationCapital>
     */
    private $capital = [];

    /**
     * @var string
     */
    private $sicCode1 = '82990';

    /**
     * @var string
     */
    private $sicCode2;

    /**
     * @var string
     */
    private $sicCode3;

    /**
     * @var string
     */
    private $sicCode4;

    /**
     * @var bool
     */
    private $preAddedPscs;

    /**
     * @var string
     */
    private static $tableName = 'ch_company_incorporation';

    /**
     * @param int $formSubmissionId
     * @throws Exception
     */
    private function __construct($formSubmissionId)
    {
        if (!preg_match('/^\d+$/', $formSubmissionId)) {
            throw new Exception('formSubmissionId has to be integer');
        }

        $sql = "
            SELECT * FROM " . self::$tableName . "
            WHERE `form_submission_id` = $formSubmissionId";
        if (!$result = CHFiling::getDb()->fetchRow($sql)) {
            throw new Exception('Company incorporation doesn\'t exist');
        }

        $this->type = $result['type'];
        $this->formSubmissionId = $result['form_submission_id'];
        $this->rejectReference = $result['reject_reference'];
        $this->rejectDescription = $result['reject_description'];
        $this->sameDay = $result['same_day'];
        $this->countryOfIncorporation = $result['country_of_incorporation'];
        $this->articles = $result['articles'];
        if ($result['restricted_articles'] == NULL) {
            $this->restrictedArticles = 0;
        } else {
            $this->restrictedArticles = $result['restricted_articles'];
        }
        $this->sameName = $result['same_name'];
        $this->nameAuthorisation = $result['name_authorisation'];
        $this->noPscReason = $result['no_psc_reason'];
        $this->preAddedPscs = $result['preAddedPscs'];

        $this->address = new Address();
        $this->address->setFields($result);

        $this->sicCode1 = $result['sicCode1'] ?: $this->sicCode1;
        $this->sicCode2 = $result['sicCode2'];
        $this->sicCode3 = $result['sicCode3'];
        $this->sicCode4 = $result['sicCode4'];

        //todo: shares - not used anymore?
        $capitalIds = IncorporationCapital::getIncorporationCapitalsIds($this->formSubmissionId);
        foreach ($capitalIds as $id) {
            $this->capital[] = IncorporationCapital::getIncorporationCapital($id);
        }
    }

    /**
     * @param int $formSubmissionId
     * @return CompanyIncorporation
     */
    public static function getCompanyIncorporation($formSubmissionId)
    {
        return new self($formSubmissionId);
    }

    /**
     * @param int $formSubmissionId
     * @return CompanyIncorporation
     * @throws Exception
     */
    public static function getNewCompanyIncorporation($formSubmissionId)
    {
        if (!preg_match('/^\d+$/', $formSubmissionId)) {
            throw new Exception('formSubmissionId has to be integer');
        }

        $data = ['form_submission_id' => $formSubmissionId];
        CHFiling::getDb()->insert(self::$tableName, $data);

        return new self($formSubmissionId);
    }

    /**
     *
     * @param int $newFormSubmissionId
     * @param int $oldFormSubmissionId
     * @param string $rejectReference
     * @param string $rejectDescription
     */
    public static function createRejectCompanyIncorporation (
        $newFormSubmissionId,
        $oldFormSubmissionId,
        $rejectReference,
        $rejectDescription
    )
    {

        // --- copy company incorporation ---
        $rejectDescription = CHFiling::getDb()->quote($rejectDescription);
        $oldIncorporation = CHFiling::getDb()->fetchRow(
            'SELECT * FROM ' . self::$tableName . ' WHERE form_submission_id = ' . $oldFormSubmissionId
        );
        $oldIncorporation['form_submission_id'] = $newFormSubmissionId;
        $oldIncorporation['reject_reference'] = $rejectReference;
        $oldIncorporation['reject_description'] = $rejectDescription;

        CHFiling::getDb()->insert(self::$tableName, $oldIncorporation);

        // --- copy all incorporation memebers ---
        $incMembers = CHFiling::getDb()->fetchAssoc(
            'SELECT * FROM `ch_incorporation_member`
            WHERE form_submission_id = ' . $oldFormSubmissionId
        );
        foreach ($incMembers as $value) {
            $value['form_submission_id'] = $newFormSubmissionId;
            unset($value['incorporation_member_id']);
            CHFiling::getDb()->insert('ch_incorporation_member', $value);
        }

        // --- copy authorised capital ---
        $incCapital = CHFiling::getDb()->fetchAssoc(
            'SELECT * FROM `ch_incorporation_capital` WHERE form_submission_id = ' . $oldFormSubmissionId
        );
        foreach ($incCapital as $value) {
            $value['form_submission_id'] = $newFormSubmissionId;
            unset($value['incorporation_capital_id']);
            CHFiling::getDb()->insert('ch_incorporation_capital', $value);
        }
    }

    /**
     * @return int
     */
    public function getFormSubmissionId()
    {
        return $this->formSubmissionId;
    }

    /**
     * @return string ('PLC', 'BYSHR', 'BYGUAR', 'BYGUAREXEMPT')
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string ('PLC', 'BYSHR', 'BYGUAR', 'BYGUAREXEMPT')
     */
    public function getCompanyType()
    {
        return $this->type;
    }

    /**
     *
     * @return string
     */
    public function getRejectReference()
    {
        return $this->rejectReference;
    }

    /**
     * same as gerRejectDesc
     *
     * @return string
     */
    public function getRejectDescription()
    {
        return $this->rejectDescription;
    }

    /**
     * @return string
     */
    public function getRejectDesc()
    {
        return $this->rejectDescription;
    }

    /**
     *
     * @return boolean
     */
    public function getSameDay()
    {
        return $this->sameDay;
    }

    /**
     *
     * @return string ('BYSHRMODEL', 'BYGUARMODEL', 'PLCMODEL', 'BYSHAREAMEND', 'BYGUARAMEND', 'PLCAMEND', 'BESPOKE')
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * @param string $articles ('BYSHRMODEL', 'BYGUARMODEL', 'PLCMODEL', 'BYSHAREAMEND', 'BYGUARAMEND', 'PLCAMEND', 'BESPOKE')
     * @throws Exception
     */
    public function setArticles($articles)
    {
        $articles = strtoupper($articles);
        $allowed = [
            'BYSHRMODEL' => 1,
            'BYGUARMODEL' => 1,
            'PLCMODEL' => 1,
            'BYSHAREAMEND' => 1,
            'BYGUARAMEND' => 1,
            'PLCAMEND' => 1,
            'BESPOKE' => 1
        ];

        if (!isset($allowed[$articles])) {
            throw new Exception('Value ' . $articles . ' is not allowed');
        }
        CHFiling::getDb()->update(
            self::$tableName,
            ['articles' => $articles],
            "form_submission_id = $this->formSubmissionId"
        );
        $this->articles = $articles;
    }

    /**
     *
     * @return boolean
     */
    public function getRestrictedArticles()
    {
        return $this->restrictedArticles;
    }

    /**
     *
     * @return Address
     */
    public function getAddress()
    {
        return clone $this->address;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getAddresses()
    {
        $corporate = $this->getIncCorporates(['dir', 'sec', 'sub']);
        $person = $this->getIncPersons(['dir', 'sec', 'sub']);
        $address = [];

        foreach ($corporate as $value) {
            $address[] = $value->getAddress();
        }

        foreach ($person as $value) {
            $address[] = $value->getAddress();
        }

        $address[] = $this->getAddress();

        foreach ($address as $k => $v) {
            $empty = 1;
            $fields = $v->getFields();
            foreach ($fields as $field) {
                if ($field !== NULL) {
                    $empty = 0;
                }
            }
            if ($empty) {
                unset($address[$k]);
            }
        }

        return $address;
    }

    /**
     * @param Address $address
     * @throws Exception
     */
    public function setAddress(Address $address)
    {
        $fields = $address->getFields();
        unset($fields['secure_address_ind']);
        $allowed = ['premise', 'street', 'post_town', 'country'];

        foreach ($allowed as $v) {
            if (!isset($fields[$v])) {
                throw new Exception('Address has to have ' . $v . ' field');
            }
        }

        if (!isset(Address::$registeredOfficeCountries[$fields['country']])) {
            throw new Exception($fields['country'] . ' cannot be use as country for registered office');
        }

        switch ($address->getCountry()) {
            case 'GB-ENG':
                $countryOfIncorporation = 'EW';
                break;
            case 'GB-WLS':
                $countryOfIncorporation = 'WA';
                break;
            case 'GB-SCT':
                $countryOfIncorporation = 'SC';
                break;
            case 'GB-NIR':
                $countryOfIncorporation = 'NI';
                break;
        }

        $fields['country_of_incorporation'] = $countryOfIncorporation;

        CHFiling::getDb()->update(self::$tableName, $fields, "form_submission_id = $this->formSubmissionId");

        $this->countryOfIncorporation = $countryOfIncorporation;
        $this->address = clone $address;
    }

    /**
     * @return boolean
     */
    public function hasRegisteredOffice()
    {
        $fields = [
            'premise',
            'street',
            'post_town',
            'country',
            'postcode'
        ];

        $addressFields = $this->address->getFields();

        foreach ($fields as $v) {
            if (!isset($addressFields[$v]) || $addressFields[$v] === NULL) {
                return 0;
            }
        }

        return 1;
    }

    /**
     *
     * @param string $type (PLC, BYSHR, BYGUAR, BYGUAREXEMPT)
     * @throws Exception
     */
    public function setType($type)
    {
        $type = strtoupper($type);

        if (!isset(self::$types[$type])) {
            throw new Exception('Type is not allowed');
        }

        $articles = NULL;

        if (isset($this->articles) && !empty($this->articles)) {
            switch ($type) {
                case 'PLC':
                    $articles = 'PLCMODEL';
                    break;
                case 'BYSHR':
                    $articles = 'BYSHRMODEL';
                    break;
                case 'BYGUAR':
                    $articles = 'BYGUARMODEL';
                    break;
            }

            if (isset($articles)) {
                $this->setArticles($articles);
            }
        }


        CHFiling::getDb()->update(
            self::$tableName,
            ['type' => $type, 'articles' => $articles],
            "form_submission_id = $this->formSubmissionId"
        );

        $this->type = $type;
        $this->articles = $articles;
    }

    /**
     * @param boolean $sameDay
     * @access public
     * @return void
     */
    public function setSameDay($sameDay)
    {
        $this->sameDay = ($sameDay) ? TRUE : FALSE;
        CHFiling::getDb()->update(
            self::$tableName,
            ['same_day' => $this->sameDay],
            "form_submission_id = $this->formSubmissionId"
        );
    }

    /**
     *
     * @return string ('EW,'SC','WA','NI')
     */
    public function getCountryOfIncorporation()
    {
        return $this->countryOfIncorporation;
    }


    /**
     *
     * @param boolean $restricted
     */
    public function setRestrictedArticles($restricted)
    {
        $restricted = ($restricted) ? 1 : 0;

        CHFiling::getDb()->update(
            self::$tableName,
            ['restricted_articles' => $restricted],
            "form_submission_id = $this->formSubmissionId"
        );
        $this->restrictedArticles = $restricted;
    }

    /**
     *
     * @param boolean $sameName
     */
    public function setSameName($sameName)
    {
        $sameName = ($sameName) ? 1 : 0;

        CHFiling::getDb()->update(
            self::$tableName,
            ['same_name' => $sameName],
            "form_submission_id = $this->formSubmissionId"
        );
        $this->sameName = $sameName;
    }

    /**
     *
     * @return boolean
     */
    public function getSameName()
    {
        return $this->sameName;
    }

    /**
     *
     * @param boolean $nameAuthorisation
     */
    public function setNameAuthorisation($nameAuthorisation)
    {
        CHFiling::getDb()->update(
            self::$tableName,
            ['name_authorisation' => $nameAuthorisation],
            "form_submission_id = $this->formSubmissionId"
        );
        $this->nameAuthorisation = $nameAuthorisation;
    }

    /**
     *
     * @return boolean
     */
    public function getNameAuthorisation()
    {
        return $this->nameAuthorisation;
    }

    /**
     * @param string $noPscReason
     */
    public function setNoPscReason($noPscReason)
    {
        CHFiling::getDb()->update(self::$tableName, array('no_psc_reason' => $noPscReason), "form_submission_id = $this->formSubmissionId");
        $this->noPscReason = $noPscReason;
    }

    /**
     * @return string
     */
    public function getNoPscReason()
    {
        return $this->noPscReason;
    }

    /**
     * @return bool
     */
    public function hasNoPscReason()
    {
        return (bool) $this->noPscReason;
    }

    /**
     * @return array
     */
    public function getSicCodes()
    {
        return array_filter(
            [
                $this->sicCode1,
                $this->sicCode2,
                $this->sicCode3,
                $this->sicCode4
            ]
        );
    }

    /**
     * @param array $sicCodes
     */
    public function setSicCodes(array $sicCodes)
    {
        $sicCodes = array_pad(array_values(array_unique($sicCodes)), 4, NULL);

        $this->sicCode1 = $sicCodes[0];
        $this->sicCode2 = $sicCodes[1];
        $this->sicCode3 = $sicCodes[2];
        $this->sicCode4 = $sicCodes[3];

        CHFiling::getDb()->update(
            self::$tableName,
            [
                'sicCode1' => $this->sicCode1,
                'sicCode2' => $this->sicCode2,
                'sicCode3' => $this->sicCode3,
                'sicCode4' => $this->sicCode4,
            ],
            "form_submission_id = $this->formSubmissionId"
        );
    }

    /**
     * @return bool
     */
    public function isPreAddedPscs()
    {
        return $this->preAddedPscs;
    }

    /**
     * @param bool $preAddedPscs
     */
    public function setPreAddedPscs($preAddedPscs)
    {
        CHFiling::getDb()->update(
            self::$tableName,
            ['preAddedPscs' => (bool) $preAddedPscs],
            "form_submission_id = $this->formSubmissionId"
        );

        $this->preAddedPscs = (bool) $preAddedPscs;
    }

    //todo: shares - not used anymore?
    /**
     *
     * @param int $shareId
     * @return IncorporationCapital
     * @throws Exception
     */
    public function getIncShare($shareId)
    {
        $capitalIds = IncorporationCapital::getIncorporationCapitalsIds($this->formSubmissionId);
        if (!isset($capitalIds[$shareId])) {
            throw new Exception('record for this form submission cannot be found');
        }

        return IncorporationCapital::getIncorporationCapital($shareId);
    }

    /**
     *
     * @return array
     */
    public function getIncShares()
    {
        return $this->capital;
    }

    /**
     *
     * @return int
     */
    public function getIncSharesCount()
    {
        $numShares = 0;
        $shares = $this->getIncShares();

        foreach ($shares as $capital) {
            $numShares += $capital->getShares()->getNumShares();
        }

        return $numShares;
    }

    /**
     *
     * @return boolean
     */
    public function hasShareCapital()
    {
        if ($this->getIncSharesCount() > 0) {
            return 1;
        }

        return 0;
    }

    //todo: shares - not used anymore?
    /**
     *
     * @return IncorporationCapital
     */
    public function getNewIncShares()
    {
        return IncorporationCapital::getNewIncorporationCapital($this->formSubmissionId);
    }

    //todo: shares - not used anymore?
    /**
     *
     * @return int
     */
    public function getAvailableShares()
    {
        $total = 0;

        foreach ($this->capital as $value) {
            $total += $value->getShares()->getNumShares();
        }

        $taken = 0;
        $personSub = $this->getIncPersonSubscribers();
        $corporateSub = $this->getIncCorporateSubscribers();

        foreach ($personSub as $value) {
            $taken += $value->getAllotmentShares()->getNumShares();
        }

        foreach ($corporateSub as $value) {
            $taken += $value->getAllotmentShares()->getNumShares();
        }

        return $total - $taken;
    }

    /**
     * @param array $type
     * @return CorporateMember[]
     * @throws Exception
     */
    public function getIncCorporates(array $type)
    {
        // --- quote user input ---
        foreach ($type as $key => $value) {
            $value = strtoupper($value);
            if (!in_array($value, ['DIR', 'SEC', 'SUB', 'PSC'])) {
                throw new Exception("value $value is not allowed");
            }
        }

        // --- add OR clause ---
        $type = implode("' OR `type` = '", $type);
        $sql = "
            SELECT `incorporation_member_id`, `type` FROM `ch_incorporation_member`
            WHERE (`type` = '" . strtoupper($type) . "')
            AND `form_submission_id` = $this->formSubmissionId
            AND `corporate` = 1";
        $result = CHFiling::getDb()->fetchAssoc($sql);

        // --- create all specified inc corporates ---
        $persons = [];
        foreach ($result as $value) {
            $persons[] = $this->getIncCorporate($value['incorporation_member_id'], $value['type']);
        }

        return $persons;
    }

    /**
     * @return IncorporationCorporateDirector[]
     */
    public function getIncCorporateDirectors()
    {
        return $this->getIncCorporates(['DIR']);
    }

    /**
     * @return IncorporationCorporateSecretary[]
     */
    public function getIncCorporateSecretaries()
    {
        return $this->getIncCorporates(['SEC']);
    }

    /**
     * @return IncorporationCorporateSubscriber[]
     */
    public function getIncCorporateSubscribers()
    {
        return $this->getIncCorporates(['SUB']);
    }

    /**
     * @return IncorporationCorporatePsc[]
     */
    public function getCorporatePscs()
    {
        return $this->getIncCorporates(['PSC']);
    }

    /**
     * @param int $id
     * @param string $type
     * @return CorporateMember
     */
    public function getIncCorporate($id, $type)
    {
        $type = strtoupper($type);

        switch ($type) {
            case 'DIR':
                return IncorporationCorporateDirector::getIncorporationCorporateDirector($this->formSubmissionId, $id);
                break;
            case 'SEC':
                return IncorporationCorporateSecretary::getIncorporationCorporateSecretary($this->formSubmissionId, $id);
                break;
            case 'SUB':
                return IncorporationCorporateSubscriber::getIncorporationCorporateSubscriber($this->formSubmissionId, $id);
                break;
            case 'PSC':
                return IncorporationCorporatePsc::getIncorporationCorporatePsc($this->formSubmissionId, $id);
                break;
        }
    }

    /**
     * @param string $type
     * @return CorporateMember
     */
    public function getNewIncCorporate($type)
    {
        $type = strtoupper($type);

        switch ($type) {
            case 'DIR':
                return IncorporationCorporateDirector::getNewIncorporationCorporateDirector($this->formSubmissionId);
                break;
            case 'SEC':
                return IncorporationCorporateSecretary::getNewIncorporationCorporateSecretary($this->formSubmissionId);
                break;
            case 'SUB':
                return IncorporationCorporateSubscriber::getNewIncorporationCorporateSubscriber($this->formSubmissionId);
                break;
            case 'PSC':
                return IncorporationCorporatePsc::getNewIncorporationCorporatePsc($this->formSubmissionId);
                break;
        }
    }

    /**
     * @param array $type
     * @return PersonMember[]
     * @throws Exception
     */
    public function getIncPersons(array $type)
    {
        // --- quote user input ---
        foreach ($type as $key => $value) {
            $value = strtoupper($value);
            if (!in_array($value, ['DIR', 'SEC', 'SUB', 'PSC'])) {
                throw new Exception("value $value is not allowed");
            }
        }

        // --- add OR clause ---
        $type = implode("' OR `type` = '", $type);
        $sql = "
            SELECT `incorporation_member_id`, `type` FROM `ch_incorporation_member`
            WHERE (`type` = '" . strtoupper($type) . "')
            AND `form_submission_id` = $this->formSubmissionId
            AND `corporate` = 0";
        $result = CHFiling::getDb()->fetchAssoc($sql);

        // --- create all specified inc corporates ---
        $persons = [];
        foreach ($result as $value) {
            $persons[] = $this->getIncPerson($value['incorporation_member_id'], $value['type']);
        }

        return $persons;
    }

    /**
     * @return IncorporationPersonDirector[]
     */
    public function getIncPersonDirectors()
    {
        return $this->getIncPersons(['DIR']);
    }

    /**
     * @return IncorporationPersonSecretary[]
     */
    public function getIncPersonSecretaries()
    {
        return $this->getIncPersons(['SEC']);
    }

    /**
     * @return IncorporationPersonSubscriber[]
     */
    public function getIncPersonSubscribers()
    {
        return $this->getIncPersons(['SUB']);
    }

    /**
     * @return IncorporationPersonPsc[]
     */
    public function getPersonPscs()
    {
        return $this->getIncPersons(['PSC']);
    }
    
    /**
     *
     * @param int $id
     * @param string $type
     * @return PersonMember
     */
    public function getIncPerson($id, $type)
    {
        $type = strtoupper($type);

        switch ($type) {
            case 'DIR':
                return IncorporationPersonDirector::getIncorporationPersonDirector($this->formSubmissionId, $id);
                break;
            case 'SEC':
                return IncorporationPersonSecretary::getIncorporationPersonSecretary($this->formSubmissionId, $id);
                break;
            case 'SUB':
                return IncorporationPersonSubscriber::getIncorporationPersonSubscriber($this->formSubmissionId, $id);
                break;
            case 'PSC':
                return IncorporationPersonPsc::getIncorporationPersonPsc($this->formSubmissionId, $id);
                break;
        }
    }

    /**
     * @param string $type
     * @return PersonMember
     */
    public function getNewIncPerson($type)
    {
        $type = strtoupper($type);

        switch ($type) {
            case 'DIR':
                return IncorporationPersonDirector::getNewIncorporationPersonDirector($this->formSubmissionId);
                break;
            case 'SEC':
                return IncorporationPersonSecretary::getNewIncorporationPersonSecretary($this->formSubmissionId);
                break;
            case 'SUB':
                return IncorporationPersonSubscriber::getNewIncorporationPersonSubscriber($this->formSubmissionId);
                break;
            case 'PSC':
                return IncorporationPersonPsc::getNewIncorporationPersonPsc($this->formSubmissionId);
                break;
        }
    }

    /**
     * @return LegalPersonPsc[]
     */
    public function getLegalPersonPscs()
    {
        throw new NotImplementedException;
    }

    /**
     * @param string $type
     * @return int
     */
    public function hasNominee($type)
    {
        $type = strtoupper($type);

        $sql = "
            SELECT `incorporation_member_id`, `type` FROM `ch_incorporation_member`
            WHERE `type` = '$type'
            AND `form_submission_id` = $this->formSubmissionId
            AND `nominee` = 1";
        $result = CHFiling::getDb()->fetchAssoc($sql);

        if (!empty($result)) {
            return 1;
        }

        return 0;
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function hasMSGRegisteredOffice()
    {
        return CHFiling::getDb()->fetchOne(
            'SELECT msg_address FROM ' . self::$tableName . ' WHERE form_submission_id = ' . $this->formSubmissionId
        );
    }

    /**
     * @param bool $bool
     * @throws Exception
     */
    public function setMSGRegisteredOffice($bool)
    {
        CHFiling::getDb()->update(
            self::$tableName,
            ['msg_address' => (bool) $bool],
            'form_submission_id = ' . $this->formSubmissionId
        );
    }

    /**
     * @return bool
     */
    public function hasNomineeDirector()
    {
        return $this->hasNominee('DIR');
    }

    /**
     * @return bool
     */
    public function hasNomineeSecretary()
    {
        return $this->hasNominee('SEC');
    }

    /**
     * @return bool
     */
    public function hasNomineeSubscriber()
    {
        return $this->hasNominee('SUB');
    }

    /**
     *
     * @return int
     */
    public function getSubscribersSharesCount()
    {
        $numShares = 0;
        $persons = $this->getIncPersonSubscribers();
        $corporates = $this->getIncCorporateSubscribers();

        foreach ($persons as $value) {
            $numShares += $value->getAllotmentShares()->getNumShares();
        }

        foreach ($corporates as $value) {
            $numShares += $value->getAllotmentShares()->getNumShares();
        }

        return $numShares;
    }

    /**
     * return array representation of the company
     *
     * @return array
     */
    public function getData()
    {
        $company = CHFiling::getDb()->fetchRow('SELECT * FROM `ch_company_incorporation` WHERE `form_submission_id` = ' . $this->formSubmissionId);
        $members = CHFiling::getDb()->fetchAssoc('SELECT * FROM `ch_incorporation_member` WHERE `form_submission_id` = ' . $this->formSubmissionId);
        $capitals = CHFiling::getDb()->fetchAssoc('SELECT * FROM `ch_incorporation_capital` WHERE form_submission_id = ' . $this->formSubmissionId);

        // --- company details ---
        $companyDetails = [];
        $companyDetails['company_number'] = NULL;
        $companyDetails['company_name'] = NULL;
        $companyDetails['authentication_code'] = NULL;
        $companyDetails['company_category'] = NULL;
        $companyDetails['made_up_date'] = NULL;
        $companyDetails['next_due_date'] = NULL;
        $companyDetails['sic_codes'] = NULL;

        $registeredOffice = [];
        $registeredOffice['premise'] = $company['premise'];
        $registeredOffice['street'] = $company['street'];
        $registeredOffice['thoroughfare'] = $company['thoroughfare'];
        $registeredOffice['post_town'] = $company['post_town'];
        $registeredOffice['county'] = $company['county'];
        $registeredOffice['country'] = (isset(Address::$registeredOfficeCountries[$company['country']]))
            ? Address::$registeredOfficeCountries[$company['country']]
            : $company['country'];
        $registeredOffice['postcode'] = $company['postcode'];
        $registeredOffice['care_of_name'] = $company['care_of_name'];
        $registeredOffice['po_box'] = $company['po_box'];

        // --- sail address ---
        $sailAddress = [];
        $sailAddress['premise'] = NULL;
        $sailAddress['street'] = NULL;
        $sailAddress['thoroughfare'] = NULL;
        $sailAddress['post_town'] = NULL;
        $sailAddress['county'] = NULL;
        $sailAddress['country'] = NULL;
        $sailAddress['postcode'] = NULL;
        $sailAddress['care_of_name'] = NULL;
        $sailAddress['po_box'] = NULL;

        $DIR = [];
        $SEC = [];
        $SUB = [];
        $PSC = [];
        foreach ($members as $value) {
            $pom = [];
            $pom['id'] = $value['incorporation_member_id'];
            $pom['corporate'] = $value['corporate'];
            if ($value['corporate']) {
                $pom['corporate_name'] = $value['corporate_name'];
            } else {
                $pom['title'] = $value['title'];
                if ($value['type'] == 'director') {
                    $pom['dob'] = $value['dob'];
                    $pom['nationality'] = $value['nationality'];
                    $pom['occupation'] = $value['occupation'];
                    $pom['country_of_residence'] = $value['country_of_residence'];
                }
            }
            $pom['surname'] = $value['surname'];
            $pom['forename'] = $value['forename'];
            $pom['premise'] = $value['premise'];
            $pom['street'] = $value['street'];
            $pom['thoroughfare'] = $value['thoroughfare'];
            $pom['post_town'] = $value['post_town'];
            $pom['county'] = $value['county'];
            $pom['country'] = $value['country'];
            $pom['postcode'] = $value['postcode'];
            $pom['care_of_name'] = $value['care_of_name'];
            $pom['nominee'] = $value['nominee'];
            $pom['nomineeType'] = $value['nomineeType'];

            ${$value['type']}[$value['incorporation_member_id']] = $pom;
        }

        //todo: shares - not amount_paid/unpaid not needed?
        $capital = [];
        $x = 0;
        foreach ($capitals as $value) {
            $capital[$x]['total_issued'] = $value['num_shares'];
            $capital[$x]['currency'] = $value['currency'];
            $capital[$x]['total_aggregate_value'] = $value['aggregate_nom_value'];
            if (isset($value['shares']) && !empty($value['shares'])) {
                $i = 0;
                foreach ($value['shares'] as $share) {
                    $capital[$x]['shares'][$i]['share_class'] = $share['share_class'];
                    $capital[$x]['shares'][$i]['prescribed_particulars'] = $share['prescribed_particulars'];
                    $capital[$x]['shares'][$i]['num_shares'] = $share['num_shares'];
                    $capital[$x]['shares'][$i]['amount_paid'] = $share['amount_paid'];
                    $capital[$x]['shares'][$i]['amount_unpaid'] = $share['amount_unpaid'];
                    $capital[$x]['shares'][$i]['nominal_value'] = $share['nominal_value'];
                    $i++;
                }
            }
            $x++;
        }

        return array_merge(
            ['company_details' => $companyDetails],
            ['registered_office' => $registeredOffice],
            ['sail_address' => $sailAddress],
            ['directors' => $DIR],
            ['secretaries' => $SEC],
            ['shareholders' => $SUB],
            ['pscs' => $PSC],
            ['capitals' => $capital]
        );
    }

    /**
     *
     * @param string $type DIR|SEC|SUB
     */
    private function removeNominee($type)
    {
        CHFiling::getDb()->delete(
            'ch_incorporation_member',
            'form_submission_id = ' . $this->formSubmissionId . ' AND nominee = 1 AND type = \'' . $type . '\''
        );
    }

    public function removeNomineeDirector()
    {
        $this->removeNominee('DIR');
    }

    public function removeNomineeSecretary()
    {
        $this->removeNominee('SEC');
    }

    public function removeNomineeSubscriber()
    {
        $this->removeNominee('SUB');
    }

    /**
     * @param string $type
     * @param string $function
     * @return int
     * @throws Exception
     */
    private function appointmentsCount($type, $function)
    {
        if ($type !== NULL && $type != 'PERSON' && $type != 'CORPORATE') {
            throw new Exception('type can be only PERSON CORPORATE or NULL');
        }

        if ($type == 'PERSON') {
            return count($this->getIncPersons([$function]));
        }

        if ($type == 'CORPORATE') {
            return count($this->getIncCorporates([$function]));
        }

        $pers = count($this->getIncPersons([$function]));
        $corps = count($this->getIncCorporates([$function]));

        return $pers + $corps;
    }

    /**
     *
     * @return boolean
     */
    public function isSameOfficer()
    {
        if ($this->directorsCount() == 1) {
            $director = $this->getIncPersons(['dir']);

            // --- corporate director ---
            if (empty($director)) {
                $director = $this->getIncCorporates(['dir']);
                $secretaries = $this->getIncCorporates(['sec']);
                foreach ($secretaries as $value) {
                    if ($director[0]->getCorporate()->getCorporateName() == $value->getCorporate()->getCorporateName()) {
                        return TRUE;
                    }
                }
                // --- person director ---
            } else {
                $secretaries = $this->getIncPersons(['sec']);
                foreach ($secretaries as $value) {
                    if (($director[0]->getPerson()->getForename() == $value->getPerson()->getForename())
                        && ($director[0]->getPerson()->getSurname() == $value->getPerson()->getSurname())
                    ) {

                        return TRUE;
                    }
                }
            }
        }

        return FALSE;
    }

    /**
     * @param string $type
     * @return int
     */
    public function directorsCount($type = NULL)
    {
        return $this->appointmentsCount($type, 'DIR');
    }

    /**
     * @param string $type
     * @return int
     */
    public function secretariesCount($type = NULL)
    {
        return $this->appointmentsCount($type, 'SEC');
    }

    /**
     * @param string $type
     * @return int
     */
    public function shareholdersCount($type = NULL)
    {
        return $this->appointmentsCount($type, 'SUB');
    }

    /**
     * @param string $type
     * @return int
     */
    public function pscsCount($type = NULL)
    {
        return $this->appointmentsCount($type, 'PSC');
    }

    /**
     * @return array
     */
    public function getCapitalTypesData()
    {
        $subscribers = array_merge($this->getIncPersonSubscribers(), $this->getIncCorporateSubscribers());
        $currencyGroupIndex = 0;

        $currecyValueGroups = Collection::from($subscribers)
            ->map(
                function ($subscriber) {
                    /** @var IncorporationPersonSubscriber|IncorporationCorporateSubscriber $subscriber */
                    $shares = $subscriber->getAllotmentShares();

                    return [
                        'currency' => $shares->getCurrency(),
                        'value' => $shares->getShareValue(),
                        'numShares' => (int) ($shares->getNumShares()),
                        'nominalValue' => (int) ($shares->getNumShares()) * $shares->getShareValue(),
                    ];
                }
            )
            ->groupBy(
                function (array $subscriberShares) {
                    return "{$subscriberShares['currency']}-{$subscriberShares['value']}";
                }
            )
            ->map(
                function (Collection $currencyValueGroup) {
                    $types = $currencyValueGroup->map(
                        function (array $type) {
                            return only($type, ['numShares', 'nominalValue'])->toArray();
                        }
                    );

                    return [
                        'currency' => $currencyValueGroup->first()['currency'],
                        'value' => $currencyValueGroup->first()['value'],
                        'aggregateNominalValue' => $currencyValueGroup->extract('nominalValue')->sum(),
                        'aggregateNumShares' => $currencyValueGroup->extract('numShares')->sum(),
                        'types' => $types->toArray(),
                    ];
                }
            );

        $currecyValueGroupsSize = $currecyValueGroups->size();

        return $currecyValueGroups
            ->groupByKey('currency')
            ->map(
                function (Collection $currencyGroup) {
                    return [
                        'currency' => $currencyGroup->first()['currency'],
                        'totalAggregateNominalValue' => $currencyGroup->extract('aggregateNominalValue')->sum(),
                        'totalAggregateNumShares' => $currencyGroup->extract('aggregateNumShares')->sum(),
                        'currencyValues' => $currencyGroup
                            ->indexBy(
                                function (array $currencyValueGroup) {
                                    return "{$currencyValueGroup['currency']}-{$currencyValueGroup['value']}";
                                }
                            )
                            ->toArray(),
                    ];
                }
            )
            ->map(
                function (array $currencyGroup) use (&$currencyGroupIndex, $currecyValueGroupsSize) {
                    foreach ($currencyGroup['currencyValues'] as &$currencyValue) {
                        $shareClass = $currecyValueGroupsSize > 1 ? 'ORD ' . chr(++$currencyGroupIndex + 64) : 'ORD';
                        $currencyValue['shareClass'] = $shareClass;
                    }

                    return $currencyGroup;
                }
            )
            ->toArray();
    }

    /**
     * @return IncorporationPersonPsc[]
     */
    public function getSuggestedPscs()
    {
        if ($this->getCompanyType() == self::LIMITED_BY_SHARES) {
            return $this->getSuggestedBySharesPscs();
        } elseif ($this->getCompanyType() == self::BY_GUARANTEE) {
            return $this->getSuggestedByGuaranteePscs();
        }

        return [];
    }

    /**
     * @return PossibleIncorporationPersonPsc[]
     */
    public function getPossiblePscs()
    {
        if ($this->getCompanyType() == self::LIMITED_BY_SHARES) {
            return $this->getPossibleBySharesPscs();
        } elseif ($this->getCompanyType() == self::BY_GUARANTEE) {
            return $this->getPossibleByGuaranteePscs();
        }

        return [];
    }

    /**
     * @return IncorporationPersonPsc[]
     */
    private function getSuggestedBySharesPscs()
    {
        $currentPscs = $this->getPersonPscs();

        return Collection::from($this->getPossibleBySharesPscs())
            ->reject(
                function (PossibleIncorporationPersonPsc $possiblePsc) {
                    return empty($possiblePsc->getNatureOfControl()->getOwnershipOfShares());
                }
            )
            ->reject(
                function (PossibleIncorporationPersonPsc $possiblePsc) use ($currentPscs) {
                    foreach ($currentPscs as $currentPsc) {
                        if ($possiblePsc->isSamePerson($currentPsc)) {
                            return TRUE;
                        }
                    }

                    return FALSE;
                }
            )
            ->map(
                function (PossibleIncorporationPersonPsc $possiblePsc) {
                    $psc = IncorporationPersonPsc::getNewIncorporationPersonPscFromDirector(
                        $this->getFormSubmissionId(),
                        $possiblePsc->getDirector()
                    );

                    $psc->setNatureOfControl($possiblePsc->getNatureOfControl());

                    return $psc;
                }
            )
            ->toArray();
    }

    /**
     * @return PossibleIncorporationPersonPsc[]
     */
    private function getPossibleBySharesPscs()
    {
        $personSubscribers = $this->getIncPersonSubscribers();
        $personDirectors = $this->getIncPersonDirectors();
        $capitalTypes = $this->getCapitalTypesData();

        $possiblePscs = Collection::from($personSubscribers)
            ->map(
                function (IncorporationPersonSubscriber $personSubscriber) use ($personDirectors, $capitalTypes) {
                    foreach ($personDirectors as $personDirector) {
                        if ($personSubscriber->isSamePerson($personDirector)) {
                            $shares = $personSubscriber->getAllotmentShares();
                            $totalAggregatedNominalValue = $capitalTypes[$shares->getCurrency()]['totalAggregateNominalValue'];
                            $sharesAmount = $shares->getAggregateValue() / $totalAggregatedNominalValue * 100;

                            $natureOfControls = new NatureOfControl;
                            if ($sharesAmount > 75) {
                                $natureOfControls->setOwnershipOfShares(NatureOfControl::OWNERSHIPOFSHARES_75_TO_100_PERCENT);
                            } elseif ($sharesAmount > 50) {
                                $natureOfControls->setOwnershipOfShares(NatureOfControl::OWNERSHIPOFSHARES_50_TO_75_PERCENT);
                            } elseif ($sharesAmount > 25) {
                                $natureOfControls->setOwnershipOfShares(NatureOfControl::OWNERSHIPOFSHARES_25_TO_50_PERCENT);
                            }

                            return new PossibleIncorporationPersonPsc($personDirector, $natureOfControls);
                        }
                    }

                    return [];
                }
            )
            ->filter()
            ->toArray();

        return count($possiblePscs) == count($personSubscribers) ? $possiblePscs : [];
    }

    /**
     * @return IncorporationPersonPsc[]
     */
    private function getSuggestedByGuaranteePscs()
    {
        $currentPscs = $this->getPersonPscs();

        return Collection::from($this->getPossibleByGuaranteePscs())
            ->reject(
                function (PossibleIncorporationPersonPsc $possiblePsc) {
                    return empty($possiblePsc->getNatureOfControl()->getOwnershipOfVotingRights());
                }
            )
            ->reject(
                function (PossibleIncorporationPersonPsc $possiblePsc) use ($currentPscs) {
                    foreach ($currentPscs as $currentPsc) {
                        if ($possiblePsc->isSamePerson($currentPsc)) {
                            return TRUE;
                        }
                    }

                    return FALSE;
                }
            )
            ->map(
                function (PossibleIncorporationPersonPsc $possiblePsc) {
                    $psc = IncorporationPersonPsc::getNewIncorporationPersonPscFromDirector(
                        $this->getFormSubmissionId(),
                        $possiblePsc->getDirector()
                    );

                    $psc->setNatureOfControl($possiblePsc->getNatureOfControl());

                    return $psc;
                }
            )
            ->toArray();
    }

    /**
     * @return PossibleIncorporationPersonPsc[]
     */
    private function getPossibleByGuaranteePscs()
    {
        $personMembers = $this->getIncPersonSubscribers();
        $corporateMembers = $this->getIncCorporateSubscribers();
        $personDirectors = $this->getIncPersonDirectors();

        if (count($personMembers) == 1 && empty($corporateMembers)) {
            $member = reset($personMembers);
            foreach ($personDirectors as $personDirector) {
                if ($member->isSamePerson($personDirector)) {
                    $natureOfControls = new NatureOfControl;
                    $natureOfControls->setOwnershipOfVotingRights(NatureOfControl::VOTINGRIGHTS_75_TO_100_PERCENT);

                    return [new PossibleIncorporationPersonPsc($personDirector, $natureOfControls)];
                }
            }
        }

        return [];
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getXml()
    {
        $test = (CHFiling::getGatewayTest()) ? '' : '';


        /** @var IncorporationPersonDirector[] $personDirectors */
        $personDirectors = $this->getIncPersons(['DIR']);
        /** @var IncorporationPersonSecretary[] $personSecretaries */
        $personSecretaries = $this->getIncPersons(['SEC']);
        /** @var IncorporationPersonSubscriber[] $personSubscribers */
        $personSubscribers = $this->getIncPersons(['SUB']);
        /** @var IncorporationPersonPsc[] $personPscs */
        $personPscs = $this->getIncPersons(['PSC']);

        /** @var IncorporationCorporateDirector[] $corporateDirectors */
        $corporateDirectors = $this->getIncCorporates(['DIR']);
        /** @var IncorporationCorporateSecretary[] $corporateSecretaries */
        $corporateSecretaries = $this->getIncCorporates(['SEC']);
        /** @var IncorporationCorporateSubscriber[] $corporateSubscribers */
        $corporateSubscribers = $this->getIncCorporates(['SUB']);
        /** @var IncorporationCorporatePsc[] $corporatePscs */
        $corporatePscs = $this->getIncCorporates(['PSC']);

        if ($this->type != 'LLP') {
            // --- check subscriber - at least one subscriber ---
            if (empty($personSubscribers) && empty($corporateSubscribers)) {
                throw new Exception('You need to set at least one subscriber');
            }

            if (empty($personDirectors)) {
                throw new Exception('You need to set at least one PersonDirector');
            }
        }

        $address = '';
        $addressFields = $this->getAddress();

        $address .= '<Premise>' . htmlspecialchars($addressFields->getPremise(), ENT_NOQUOTES) . '</Premise>';
        $address .= '<Street>' . htmlspecialchars($addressFields->getStreet(), ENT_NOQUOTES) . '</Street>';
        $address .= ($addressFields->getThoroughfare() === NULL) ? '' :
            '<Thoroughfare>' . htmlspecialchars($addressFields->getThoroughfare(), ENT_NOQUOTES) . '</Thoroughfare>';
        $address .= '<PostTown>' . htmlspecialchars($addressFields->getPostTown(), ENT_NOQUOTES) . '</PostTown>';


        $address .= ($addressFields->getCounty() === NULL) || strlen(
            stristr($addressFields->getCounty(), 'select')
        ) > 0 ? '' :
            '<County>' . htmlspecialchars($addressFields->getCounty(), ENT_NOQUOTES) . '</County>';
        $address .= '<Country>' . htmlspecialchars($addressFields->getCountry(), ENT_NOQUOTES) . '</Country>';
        $address .= ($addressFields->getPostcode() === NULL) ? '' :
            '<Postcode>' . htmlspecialchars($addressFields->getPostcode(), ENT_NOQUOTES) . '</Postcode>';
        $address .= ($addressFields->getCareOfName() === NULL) ? '' :
            '<CareOfName>' . htmlspecialchars($addressFields->getCareOfName(), ENT_NOQUOTES) . '</CareOfName>';
        $address .= ($addressFields->getPoBox() === NULL) ? '' :
            '<PoBox>' . htmlspecialchars($addressFields->getPoBox(), ENT_NOQUOTES) . '</PoBox>';

        $appointments = '';
        $llpAuthorise = [];

        foreach ($personDirectors as $value) {
            /** @var = */
            $appointments .= $value->getXml();
            $llpAuthorise[] = [
                'xml' => $value->getXml(),
                'authenticationFields' => $value->getAuthentication()->getFields(),
                'forename' => $value->getPerson()->getForename(),
                'surname' => $value->getPerson()->getSurname(),
            ];
        }

        if (!empty($corporateDirectors)) {
            foreach ($corporateDirectors as $value) {
                $appointments .= $value->getXml();
                $llpAuthorise[] = [
                    'xml' => $value->getXml(),
                    'authenticationFields' => $value->getAuthentication()->getFields(),
                    'forename' => $value->getCorporate()->getForename(),
                    'surname' => $value->getCorporate()->getSurname(),
                ];
            }
        }

        if (!empty($personSecretaries)) {
            foreach ($personSecretaries as $value) {
                $appointments .= $value->getXml();
            }
        }
        if (!empty($corporateSecretaries)) {
            foreach ($corporateSecretaries as $value) {
                $appointments .= $value->getXml();
            }
        }

        $pscsXml = '';
        if (Feature::featurePsc()) {
            /** @var IncorporationPersonPsc[]|IncorporationCorporatePsc[] $pscs */
            $pscs = array_merge($personPscs, $corporatePscs);
            $pscsXml = '<PSCs>';
            if (empty($pscs)) {
                $pscsXml .= '<NoPSCStatement>' . $this->getNoPscReason() . '</NoPSCStatement>';
            } else {
                foreach ($pscs as $psc) {
                    $pscsXml .= $psc->getXml();
                }
            }
            $pscsXml .= '</PSCs>';
        }

        /** @var IncorporationPersonSubscriber[]|IncorporationCorporateSubscriber[] $subscribers */
        $subscribers = array_merge($personSubscribers, $corporateSubscribers);
        $subscribersXml = '';
        $statementOfCapitalXml = '';

        /* only PLC and BYSHR have capital */
        if ($this->type == 'PLC' || $this->type == 'BYSHR') {
            $capitalTypes = $this->getCapitalTypesData();

            foreach ($subscribers as $subscriber) {
                $shares = $subscriber->getAllotmentShares();
                $currency = $shares->getCurrency();
                $currencyValue = $currency . '-' . $shares->getShareValue();

                $shares->setShareClass($capitalTypes[$currency]['currencyValues'][$currencyValue]['shareClass']);
                $subscriber->setAllotmentShares($shares);
                $subscribersXml .= $subscriber->getXml();
            }

            $result = '';

            foreach ($capitalTypes as $capitalType) {
                $sharesXml = '';

                if (Feature::featurePsc()) {
                    // --- create shares for each share value (amount unpaid per share) ---
                    foreach ($capitalType['currencyValues'] as $currenctValueGroup) {
                        $sharesXml .= "
                            <Shares>
                                <ShareClass>{$currenctValueGroup['shareClass']}</ShareClass>
                                <PrescribedParticulars>Ordinary shares have full rights in the company with respect to voting, dividends and distributions.</PrescribedParticulars>
                                <NumShares>{$currenctValueGroup['aggregateNumShares']}</NumShares>
                                <AggregateNominalValue>{$currenctValueGroup['aggregateNominalValue']}</AggregateNominalValue>
                            </Shares>
                        ";
                    }

                    $capitalXml = "
                        <Capital>
                            <TotalAmountUnpaid>0</TotalAmountUnpaid>
                            <TotalNumberOfIssuedShares>{$capitalType['totalAggregateNumShares']}</TotalNumberOfIssuedShares>
                            <ShareCurrency>{$capitalType['currency']}</ShareCurrency>
                            <TotalAggregateNominalValue>{$capitalType['totalAggregateNominalValue']}</TotalAggregateNominalValue>
                            {$sharesXml}
                        </Capital>
                    ";
                } else {
                    // --- create shares for each share value (amount unpaid per share) ---
                    foreach ($capitalType['currencyValues'] as $currenctValueGroup) {
                        $sharesXml .= "
                            <Shares>
                                <ShareClass>{$currenctValueGroup['shareClass']}</ShareClass>
                                <PrescribedParticulars>Ordinary shares have full rights in the company with respect to voting, dividends and distributions.</PrescribedParticulars>
                                <NumShares>{$currenctValueGroup['aggregateNumShares']}</NumShares>
                                <AmountPaidDuePerShare>{$currenctValueGroup['value']}</AmountPaidDuePerShare>
                                <AmountUnpaidPerShare>0</AmountUnpaidPerShare>
                                <AggregateNominalValue>{$currenctValueGroup['aggregateNominalValue']}</AggregateNominalValue>
                            </Shares>
                        ";
                    }

                    $capitalXml = "
                        <Capital>
                            <TotalNumberOfIssuedShares>{$capitalType['totalAggregateNumShares']}</TotalNumberOfIssuedShares>
                            <ShareCurrency>{$capitalType['currency']}</ShareCurrency>
                            <TotalAggregateNominalValue>{$capitalType['totalAggregateNominalValue']}</TotalAggregateNominalValue>
                            {$sharesXml}
                        </Capital>
                    ";
                }

                $result .= $capitalXml;
            }

            $statementOfCapitalXml = '<StatementOfCapital>'.$result.'</StatementOfCapital>';
        } elseif ($this->type == 'BYGUAR') {
            foreach ($subscribers as $subscriber) {
                $subscribersXml .= $subscriber->getXml(TRUE); //guarantor
            }
        }

        $sameName = $this->getSameName() ? '<SameName>' . $this->getSameName() . '</SameName>' : '';
        $nameAuthorisation = $this->getNameAuthorisation()  ? '<NameAuthorisation>' . $this->getNameAuthorisation() . '</NameAuthorisation>' : '';
        $rejectReference = $this->getRejectReference() ? '<RejectReference>' . $this->getRejectReference() . '</RejectReference>' : '';

        $sicCodes = '';
        if (Feature::featurePsc() && $this->type != 'LLP') {
            foreach ($this->getSicCodes() as $sicCode) {
                $sicCodes .= "<SICCode>$sicCode</SICCode>";
            }
            $sicCodes = "<SICCodes>$sicCodes</SICCodes>";
        }

        if ($this->type != 'LLP') {
            $articles = '<Articles>' . $this->getArticles() . '</Articles><RestrictedArticles>' . $this->getRestrictedArticles() . '</RestrictedArticles>';
        } else {
            $articles = '';
        }

        if ($this->type != 'LLP') {
            // ltd hardcoded our agent detail
            $authoriser = '<Authoriser>
                    <Agent>
                        <Corporate>
                            <Forename>Howard</Forename>
                            <Surname>Graham</Surname>
                            <CorporateName>Companies Made Simple Ltd</CorporateName>
                        </Corporate>
                        <Authentication>
                            <PersonalAttribute>BIRTOWN</PersonalAttribute>
                            <PersonalData>LON</PersonalData>
                        </Authentication>
                        <Authentication>
                            <PersonalAttribute>TEL</PersonalAttribute>
                            <PersonalData>272</PersonalData>
                        </Authentication>
                        <Authentication>
                            <PersonalAttribute>EYE</PersonalAttribute>
                            <PersonalData>Blu</PersonalData>
                        </Authentication>
                        <Address>
                            <Premise>20-22</Premise>
                            <Street>Wenlock Road</Street>
                            <PostTown>London</PostTown>
                            <Country>GB-ENG</Country>
                            <Postcode>N1 7GU</Postcode>
                        </Address>
                    </Agent>
                </Authoriser>';
        } else {
            // for llp we can't use our agent, autoriser must be one of the llp members.
            $llpMember = $llpAuthorise[0];
            $auth = simplexml_load_string($llpMember['xml']);

            $authentication = '';
            foreach ($llpMember['authenticationFields'] as $shareCurrency => $shareValues) {
                $authentication .= '<Authentication>';
                $authentication .= '    <PersonalAttribute>' . $shareCurrency . '</PersonalAttribute>';
                $authentication .= '    <PersonalData>' . $shareValues . '</PersonalData>';
                $authentication .= '</Authentication>';
            }

            $authoriser = '';
            $authoriser .= '<Authoriser>
                <Member>';

            if (isset($auth->Member->Person)) {
                $authoriser .=
                    '<Person>
                        <Forename>' . htmlspecialchars($llpMember['forename'], ENT_NOQUOTES) . '</Forename>
                        <Surname>' . htmlspecialchars($llpMember['surname'], ENT_NOQUOTES) . '</Surname>
                    </Person>' . $authentication;
            } else {
                $authoriser .=
                    '<Corporate>
                        <Forename>' . htmlspecialchars($llpMember['forename'], ENT_NOQUOTES) . '</Forename>
                        <Surname>' . htmlspecialchars($llpMember['surname'], ENT_NOQUOTES) . '</Surname>
                        <CorporateName>' . htmlspecialchars($auth->Member->Corporate->CorporateName, ENT_NOQUOTES) . '</CorporateName>
                    </Corporate>' . $authentication;
            }
            $authoriser .= '</Member>

            </Authoriser>';
        }

        if (Feature::featurePsc()) {
            if (Feature::featurePscTesting()) {
                $xsdSchema = 'http://xmlbeta.companieshouse.gov.uk/v1-0/schema/forms/CompanyIncorporation-v3-0-rc2.xsd';
            } else {
                $xsdSchema = 'http://xmlgw.companieshouse.gov.uk/v1-0/schema/forms/CompanyIncorporation-v3-1.xsd';
            }
        } else {
            $xsdSchema = 'http://xml' . $test . 'gw.companieshouse.gov.uk/v2-1/schema/forms/CompanyIncorporation-v2-8.xsd';
        }

        // --- return xml ---
        return '<CompanyIncorporation
            xsi:schemaLocation="http://xmlgw.companieshouse.gov.uk
            ' . $xsdSchema . '"
            xmlns="http://xmlgw.companieshouse.gov.uk"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

			<CompanyType>' . $this->getCompanyType() . '</CompanyType>
			<CountryOfIncorporation>' . $this->getCountryOfIncorporation() . '</CountryOfIncorporation>
			<RegisteredOfficeAddress>' .
            $address .
            '</RegisteredOfficeAddress>' .
            $articles .
            $appointments .
            $pscsXml .
            $statementOfCapitalXml .
            $subscribersXml .
            $authoriser .
            '<SameDay>' . $this->getSameDay() . '</SameDay>' .
            $sameName .
            $nameAuthorisation .
            $rejectReference .
            $sicCodes .
            '</CompanyIncorporation>';
    }

    public function remove()
    {
        CHFiling::getDb()->delete(self::$tableName, 'form_submission_id = ' . $this->formSubmissionId);
        CHFiling::getDb()->delete('ch_incorporation_member', 'form_submission_id = ' . $this->formSubmissionId);
        CHFiling::getDb()->delete('ch_incorporation_capital', 'form_submission_id = ' . $this->formSubmissionId);
        $this->formSubmissionId = NULL;
    }
}
