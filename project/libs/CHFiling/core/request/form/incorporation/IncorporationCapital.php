<?php

//todo: shares - not used anymore?
final class IncorporationCapital {

    public static $currency = array('GBP' => 'GBP £', 'USD' => 'USD $', 'EUR' => 'EUR €');

    /**
     *
     * @var int
     */
    private $incorporationCapitalId;

    /**
     *
     * @var int
     */
    private $formSubmissionId;

    /**
     *
     * @var Shares
     */
    private $shares;

    /**
     *
     * @var string
     */
    private static $tableName = 'ch_incorporation_capital';

    /**
     * do not set both values, set either inc capital id or form submission id
     *
     * @param int $incorporationCapitalId
     * @param int $formSubmissionId
     */
    private function  __construct($incorporationCapitalId = null, $formSubmissionId = null) {
        // --- do not save anything in db ---
        if (is_null($incorporationCapitalId)) {
            if (is_null($formSubmissionId)) {
                throw new Exception('You need to set either incorporation capital id or form submission id');
            }
            $this->incorporationCapitalId   = null;
            $this->formSubmissionId         = $formSubmissionId;
            $this->shares                   = new Shares();
            return;
        }

        // --- check that incorporationCapitalId is int ---
        if (!preg_match('/^\d+$/', $incorporationCapitalId)) {
            throw new Exception('incorporationCapitalId has to be integer');
        }

        $sql = "
			SELECT * FROM `".self::$tableName."`
			WHERE `incorporation_capital_id` = $incorporationCapitalId";
        if (!$result = CHFiling::getDb()->fetchRow($sql)) {
            throw new Exception('Record doesn\'t exist');
        }

        // --- set instance variables ---
		$this->incorporationCapitalId = $result['incorporation_capital_id'];
        $this->formSubmissionId = $result['form_submission_id'];
        $this->shares           = new Shares();
        $this->shares->setFields($result);
    }

    /**
     *
     * @param int $incorporationCapitalId
     */
    public static function getIncorporationCapital($incorporationCapitalId) {
        return new IncorporationCapital($incorporationCapitalId);
    }

    /**
     *
     * @param int $formSubmissionId
     * @return IncorporationCapital
     */
    public static function getNewIncorporationCapital($formSubmissionId) {
        return new IncorporationCapital(null, $formSubmissionId);
    }

    /**
     *
     * @param int $formSubmissionId
     * @return array
     */
    public static function getIncorporationCapitalsIds($formSubmissionId) {
        // --- check that formSubmissionId is int ---
        if (!preg_match('/^\d+$/', $formSubmissionId)) {
            throw new Exception('formSubmissionId has to be integer');
        }

        // --- query ---
        $sql = "
            SELECT `incorporation_capital_id` FROM ".self::$tableName."
            WHERE form_submission_id = $formSubmissionId";
        $result = CHFiling::getDb()->fetchCol($sql);
        
        // --- return result ---
        return $result;
    }

    /**
     *
     * @return Shares
     */
    public function getShares() {
        return clone $this->shares;
    }

    /**
     *
     * @param Shares $shares
     */
    public function setShares(Shares $shares) {
        if (!$this->isComplete($shares)) {
            throw new Exception('Set all values for shares');
        }

        $this->shares = $shares;
    }

    /**
     * if the argument is null then method checks this object,
     * if argument is not null it will check supplied object
     *
     * @return boolean
     */
    private function isComplete(Shares $shares = null) {
        $fields = array(
            'currency', 'num_shares', 'share_class', 'prescribed_particulars',
            'amount_paid', 'amount_unpaid', 'aggregate_nom_value'
        );

        if ($shares == null) {
            $shareFields = $this->shares->getFields();
        } else {
            $shareFields = $shares->getFields();
        }

        // --- check if all fields are set ---
        foreach($fields as $v) {
            if (!isset($shareFields[$v]) || is_null($shareFields[$v])) {
                return 0;
            }
        }

        return 1;
    }

    /**
     *
     */
    public function save() {
        if (!$this->isComplete()) {
            throw new Exception('you need to set compulsory fiedls before saving');
        }

        // --- prepare data ---
        $data = $this->shares->getFields();
        $data['form_submission_id'] = $this->formSubmissionId;

        // --- save data ---
        if (isset($this->incorporationCapitalId) && !is_null($this->incorporationCapitalId)) {
            CHFiling::getDb()->update(self::$tableName, $data, 'incorporation_capital_id = '.$this->incorporationCapitalId);
        } else {
            CHFiling::getDb()->insert(self::$tableName, $data);
            $this->incorporationCapitalId = CHFiling::getDb()->lastInsertId();
        }
    }

    /**
     *
     * @return xml
     */
    public function getXml() {
        if(!$this->isComplete()) {
            throw new Exception('you need to fill compulsory fields');
        }

        // --- we implented model that company can have only one shares, so if it changes,
        // just create array of shares and database from one to one, to one to many  and change
        // this xml ---
        return '<Capital>
            <TotalNumberOfIssuedShares>'.$this->shares->getNumShares().'</TotalNumberOfIssuedShares>
            <ShareCurrency>'.$this->shares->getCurrency().'</ShareCurrency>
            <TotalAggregateNominalValue>'.$this->shares->getAggregateNomValue().'</TotalAggregateNominalValue>
            <Shares>
                <ShareClass>'.htmlspecialchars($this->shares->getShareClass(), ENT_NOQUOTES).'</ShareClass>
                <PrescribedParticulars>'.$this->shares->getPrescribedParticulars().'</PrescribedParticulars>
                <NumShares>'.$this->shares->getNumShares().'</NumShares>
                <AmountPaidDuePerShare>'.$this->shares->getAmountPaid().'</AmountPaidDuePerShare>
                <AmountUnpaidPerShare>'.$this->shares->getAmountUnpaid().'</AmountUnpaidPerShare>
                <AggregateNominalValue>'.$this->shares->getAggregateNomValue().'</AggregateNominalValue>
            </Shares>
        </Capital>';
    }
}
?>
