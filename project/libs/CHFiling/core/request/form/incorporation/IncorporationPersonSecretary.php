<?php

final class IncorporationPersonSecretary extends PersonSecretary {

    /**
     *
     * @var int
     */
    private $incorporationMemberId;

    /**
     *
     * @var int
     */
    private $formSubmissionId;

    /**
     *
     * @var boolean
     */
    private $nominee;

    /**
     *
     * @var string
     */
    private static $type = 'SEC';

    /**
     *
     * @var boolean
     */
    private static $corporate = 0;

    /**
     *
     * @var string
     */
    private static $tableName = 'ch_incorporation_member';

    /**
     *
     * @param int $annualReturnOfficerId
     * @param int $formSubmissionId
     */
    public function  __construct($formSubmissionId, $annualReturnOfficerId = null) {
        // --- check that formSubmissionId is int ---
        if (!preg_match('/^\d+$/', $formSubmissionId)) {
            throw new Exception('form submissio id has to be integer');
        }

        parent::__construct();

        // --- do not save anything in db ---
        if (is_null($annualReturnOfficerId)) {
            $this->incorporationMemberId    = null;
            $this->formSubmissionId         = $formSubmissionId;
            return;
        }

        // --- check that incorporationMemberId is int ---
        if (!preg_match('/^\d+$/', $annualReturnOfficerId)) {
            throw new Exception('incorporation member id has to be integer');
        }

        $sql = "
			SELECT * FROM `".self::$tableName."`
			WHERE `incorporation_member_id` = $annualReturnOfficerId AND `form_submission_id` = $formSubmissionId";
        if (!$result = CHFiling::getDb()->fetchRow($sql)) {
            throw new Exception('Record doesn\'t exist');
        }

        // --- set instance variables ---
		$this->incorporationMemberId = $result['incorporation_member_id'];
        $this->formSubmissionId = $result['form_submission_id'];
        $this->nominee          = $result['nominee'];
        $this->setFields($result);
    }

    /**
     *
     * @param int $formSubmissionId
     * @param int $incorporationMemberId
     * @return IncorporationPersonSubscriber
     */
    public static function getIncorporationPersonSecretary($formSubmissionId, $incorporationMemberId) {
        return new IncorporationPersonSecretary($formSubmissionId, $incorporationMemberId);
    }

    /**
     *
     * @param int $formSubmissionId
     * @return IncorporationPersonSubscriber
     */
    public static function getNewIncorporationPersonSecretary($formSubmissionId) {
        return new IncorporationPersonSecretary($formSubmissionId);
    }

    /**
     *
     * @return string
     */
    public function getType() {
        return self::$type;
    }

    /**
     *
     */
    public function remove() {
        CHFiling::getDb()->delete(self::$tableName, 'incorporation_member_id = '.$this->incorporationMemberId);
    }

    /**
     *
     * @return int
     */
    public function getId() {
        return $this->incorporationMemberId;
    }

    /**
     *
     * @param boolean $nominee
     */
    public function setNominee($nominee) {
        $this->nominee = ($nominee) ? 1 : 0;
    }

    /**
     *
     */
    public function isNominee() {
        return $this->nominee;
    }

    /**
     *
     * @return boolean
     */
    public function isComplete() {
        $allowedFields = array(
            'authentication', 'surname', 'premise', 'street', 'post_town', 'country'
        );

        $fields = array_merge(
            $this->getPerson()->getFields(),
            $this->getAddress()->getFields()
        );

        // --- set authentication in one field ---
        if (!is_null($this->getAuthentication()->getDbFields())) {
            $fields['authentication'] = $this->getAuthentication()->getDbFields();
        }

        // --- check if all fields are set ---
        foreach($allowedFields as $v) {
            if (!isset($fields[$v]) || is_null($fields[$v])) {
                return 0;
            }
        }
        return 1;
    }

    /**
     *
     */
    public function save() {
        if (!$this->isComplete()) {
            throw new Exception('You need to set all compulsory fielsd');
        }

        // --- prepare data ---
        $address = $this->getAddress()->getFields();
        unset($address['secure_address_ind']);
        $data = array_merge(
            $this->getPerson()->getFields(),
            $address
        );

        // --- set authentication in one field ---
        $data['authentication'] = $this->getAuthentication()->getDbFields();
        $data['form_submission_id'] = $this->formSubmissionId;
        $data['corporate']          = self::$corporate;
        $data['type']               = self::$type;
        $data['nominee']            = $this->nominee;
        $data['consentToAct'] = $this->hasConsentToAct();

        // --- save data ---
        if (isset($this->incorporationMemberId) && !is_null($this->incorporationMemberId)) {
            CHFiling::getDb()->update(self::$tableName, $data, 'incorporation_member_id = '.$this->incorporationMemberId);
        } else {
            CHFiling::getDb()->insert(self::$tableName, $data);
            $this->incorporationMemberId = CHFiling::getDb()->lastInsertId();
        }
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getXml() {
        // --- check if is complete ---
        if (!$this->isComplete()) {
            throw new Exception('This object is not complete');
        }

        // --- authentication ---
        if ($this->hasConsentToAct()) {
            $authentication = '<ConsentToAct>true</ConsentToAct>';
        } else {
            $authentication = '<ConsentToAct>false</ConsentToAct>';
        }

        // --- prepare xml ---

        // --- person ---
        $person = '';
        $personFields = $this->getPerson();

        $person .= (is_null($personFields->getTitle())) ? '' :
            '<Title>'.htmlspecialchars($personFields->getTitle(), ENT_NOQUOTES).'</Title>';
         if (!is_null($personFields->getForename())) {
            $person .= '<Forename>'.htmlspecialchars($personFields->getForename(), ENT_NOQUOTES);
            
            if(!is_null($personFields->getMiddleName())){ 
                $person .=' '.htmlspecialchars($personFields->getMiddleName(), ENT_NOQUOTES);
            };
            $person .= '</Forename>';
        }
        $person .= '<Surname>'.htmlspecialchars($personFields->getSurname(), ENT_NOQUOTES).'</Surname>';

        // --- service address ---
        $serviceAddress = '';
        $serviceAddressFields = $this->getAddress();

        $serviceAddress .= '<Premise>'.htmlspecialchars($serviceAddressFields->getPremise(), ENT_NOQUOTES).'</Premise>';
        $serviceAddress .= '<Street>'.htmlspecialchars($serviceAddressFields->getStreet(), ENT_NOQUOTES).'</Street>';
        $serviceAddress .= (is_null($serviceAddressFields->getThoroughfare())) ? '' :
            '<Thoroughfare>'.htmlspecialchars($serviceAddressFields->getThoroughfare(), ENT_NOQUOTES).'</Thoroughfare>';
        $serviceAddress .= '<PostTown>'.htmlspecialchars($serviceAddressFields->getPostTown(), ENT_NOQUOTES).'</PostTown>';
        
        //fix empty and -- Select -- in county problems
        $serviceAddress .= (is_null($serviceAddressFields->getCounty())) || strlen(stristr($serviceAddressFields->getCounty(),'select'))>0 ? ''  :
            '<County>'.htmlspecialchars($serviceAddressFields->getCounty(), ENT_NOQUOTES).'</County>';

        if (isset(Address::$nonForeignCountries[$serviceAddressFields->getCountry()])) {
            $serviceAddress .= '<Country>'.htmlspecialchars($serviceAddressFields->getCountry(), ENT_NOQUOTES).'</Country>';
        } else {
            $serviceAddress .= '<OtherForeignCountry>'.htmlspecialchars($serviceAddressFields->getCountry(), ENT_NOQUOTES).'</OtherForeignCountry>';
        }

        $serviceAddress .= (is_null($serviceAddressFields->getPostcode())) ? '' :
            '<Postcode>'.htmlspecialchars($serviceAddressFields->getPostcode(), ENT_NOQUOTES).'</Postcode>';
        $serviceAddress .= (is_null($serviceAddressFields->getCareOfName())) ? '' :
            '<CareOfName>'.htmlspecialchars($serviceAddressFields->getCareOfName(), ENT_NOQUOTES).'</CareOfName>';
        $serviceAddress .= (is_null($serviceAddressFields->getPoBox())) ? '' :
            '<PoBox>'.htmlspecialchars($serviceAddressFields->getPoBox(), ENT_NOQUOTES).'</PoBox>';

        // --- return xml ---
        return "<Appointment>
            $authentication
            <Secretary>
                <Person>
                    $person
                    <ServiceAddress>
                        <Address>
                            $serviceAddress
                        </Address>
                    </ServiceAddress>
                </Person>
            </Secretary>
        </Appointment>";
    }
}

