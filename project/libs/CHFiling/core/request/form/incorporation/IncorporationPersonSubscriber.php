<?php

final class IncorporationPersonSubscriber extends PersonSubscriber {

    /**
     *
     * @var int
     */
    private $incorporationMemberId;

    /**
     *
     * @var int
     */
    private $formSubmissionId;

    /**
     *
     * @var boolean
     */
    private $nominee;

    /**
     *
     * @var string
     */
    private static $type = 'SUB';

    /**
     *
     * @var boolean
     */
    private static $corporate = 0;

    /**
     *
     * @var string
     */
    private static $tableName = 'ch_incorporation_member';

    /**
     *
     * @param int $annualReturnOfficerId
     * @param int $formSubmissionId
     */
    public function  __construct($formSubmissionId, $annualReturnOfficerId = null) {
        // --- check that formSubmissionId is int ---
        if (!preg_match('/^\d+$/', $formSubmissionId)) {
            throw new Exception('form submissio id has to be integer');
        }

        parent::__construct();

        // --- do not save anything in db ---
        if (is_null($annualReturnOfficerId)) {
            $this->incorporationMemberId    = null;
            $this->formSubmissionId         = $formSubmissionId;
            return;
        }

        // --- check that incorporationMemberId is int ---
        if (!preg_match('/^\d+$/', $annualReturnOfficerId)) {
            throw new Exception('incorporation member id has to be integer');
        }

        $sql = "
			SELECT * FROM `".self::$tableName."`
			WHERE `incorporation_member_id` = $annualReturnOfficerId AND `form_submission_id` = $formSubmissionId";
        if (!$result = CHFiling::getDb()->fetchRow($sql)) {
            throw new Exception('Record doesn\'t exist');
        }

        // --- set instance variables ---
		$this->incorporationMemberId = $result['incorporation_member_id'];
        $this->formSubmissionId = $result['form_submission_id'];
        $this->nominee          = $result['nominee'];
        $this->setFields($result);
    }

    /**
     *
     * @param int $formSubmissionId
     * @param int $incorporationMemberId
     * @return IncorporationPersonSubscriber
     */
    public static function getIncorporationPersonSubscriber($formSubmissionId, $incorporationMemberId) {
        return new IncorporationPersonSubscriber($formSubmissionId, $incorporationMemberId);
    }

    /**
     *
     * @param int $formSubmissionId
     * @return IncorporationPersonSubscriber
     */
    public static function getNewIncorporationPersonSubscriber($formSubmissionId) {
        return new IncorporationPersonSubscriber($formSubmissionId);
    }
    
    /**
     *
     * @return string
     */
    public function getType() {
        return self::$type;
    }

    /**
     *
     */
    public function remove() {
        CHFiling::getDb()->delete(self::$tableName, 'incorporation_member_id = '.$this->incorporationMemberId);
    }

    /**
     *
     * @return int
     */
    public function getId() {
        return $this->incorporationMemberId;
    }

    /**
     *
     * @param boolean $nominee
     */
    public function setNominee($nominee) {
        $this->nominee = ($nominee) ? 1 : 0;
    }

    /**
     *
     */
    public function isNominee() {
        return $this->nominee;
    }

    /**
     *
     * @return boolean
     */
    public function isComplete() {
        /* cannot do this because members don't have shares ---
        $allowedFields = array(
            'authentication', 'surname', 'premise', 'street', 'post_town', 'country',
            'share_class', 'num_shares', 'amount_paid', 'amount_unpaid', 'currency',
            'share_value'
        );
        */
        $allowedFields = array(
            'authentication', 'surname', 'premise', 'street', 'post_town', 'country'
            );

        $fields = array_merge(
            $this->getPerson()->getFields(),
            $this->getAddress()->getFields(),
            $this->getAllotmentShares()->getFields()
        );

        // --- set authentication in one field ---
        if (!is_null($this->getAuthentication()->getDbFields())) {
            $fields['authentication'] = $this->getAuthentication()->getDbFields();
        }

        // --- check if all fields are set ---
        foreach($allowedFields as $v) {
            if (!isset($fields[$v]) || is_null($fields[$v])) {
                return 0;
            }
        }
        return 1;
    }

    /**
     *
     */
    public function save() {
        if (!$this->isComplete()) {
            throw new Exception('You need to set all compulsory fielsd');
        }

        // --- prepare data ---
        $address = $this->getAddress()->getFields();
        unset($address['secure_address_ind']);
        $data = array_merge(
            $this->getPerson()->getFields(),
            $address,
            $this->getAllotmentShares()->getFields()
        );

        // --- set authentication in one field ---
        $data['authentication'] = $this->getAuthentication()->getDbFields();
        $data['form_submission_id'] = $this->formSubmissionId;
        $data['corporate']          = self::$corporate;
        $data['type']               = self::$type;
        $data['nominee']            = $this->nominee;

        // --- save data ---
        if (isset($this->incorporationMemberId) && !is_null($this->incorporationMemberId)) {
            CHFiling::getDb()->update(self::$tableName, $data, 'incorporation_member_id = '.$this->incorporationMemberId);
        } else {
            CHFiling::getDb()->insert(self::$tableName, $data);
            $this->incorporationMemberId = CHFiling::getDb()->lastInsertId();
        }
    }

    /**
     * @param bool $guarantor
     * @return string
     * @throws Exception
     */
    public function getXml($guarantor = false) {
        // --- check if is complete ---
        if (!$this->isComplete()) {
            throw new Exception('This object is not complete');
        }

        // --- authentication ---
        $authentication = '';
        $autheticationFields = $this->getAuthentication()->getFields();
        foreach ($autheticationFields as $k => $v) {
            $authentication .= '<Authentication>';
            $authentication .= '<PersonalAttribute>'.$k.'</PersonalAttribute>';
            $authentication .= '<PersonalData>'.$v.'</PersonalData>';
            $authentication .= '</Authentication>';
        }

        // --- prepare xml ---

        // --- person ---
        $person = '';
        $personFields = $this->getPerson();

        if (!is_null($personFields->getForename())) {
            $person .= '<Forename>'.htmlspecialchars($personFields->getForename(), ENT_NOQUOTES);
            
            if(!is_null($personFields->getMiddleName())){ 
                $person .=' '.htmlspecialchars($personFields->getMiddleName(), ENT_NOQUOTES);
            };
            $person .= '</Forename>';
        }
        
        $person .= '<Surname>'.htmlspecialchars($personFields->getSurname(), ENT_NOQUOTES).'</Surname>';

        // --- address ---
        $address = '';
        $addressFields = $this->getAddress();

        $address .= '<Premise>'.htmlspecialchars($addressFields->getPremise(), ENT_NOQUOTES).'</Premise>';
        $address .= '<Street>'.htmlspecialchars($addressFields->getStreet(), ENT_NOQUOTES).'</Street>';
        $address .= (is_null($addressFields->getThoroughfare())) ? '' :
            '<Thoroughfare>'.htmlspecialchars($addressFields->getThoroughfare(), ENT_NOQUOTES).'</Thoroughfare>';
        $address .= '<PostTown>'.htmlspecialchars($addressFields->getPostTown(), ENT_NOQUOTES).'</PostTown>';
               
        //fix empty and -- Select -- in county problems
        $address .= (is_null($addressFields->getCounty()))  || strlen(stristr($addressFields->getCounty(),'select'))>0  ? '' :
            '<County>'.htmlspecialchars($addressFields->getCounty(), ENT_NOQUOTES).'</County>';

        if (isset(Address::$nonForeignCountries[$addressFields->getCountry()])) {
            $address .= '<Country>'.htmlspecialchars($addressFields->getCountry(), ENT_NOQUOTES).'</Country>';
        } else {
            $address .= '<OtherForeignCountry>'.htmlspecialchars($addressFields->getCountry(), ENT_NOQUOTES).'</OtherForeignCountry>';
        }

        $address .= (is_null($addressFields->getPostcode())) ? '' :
            '<Postcode>'.htmlspecialchars($addressFields->getPostcode(), ENT_NOQUOTES).'</Postcode>';
        $address .= (is_null($addressFields->getCareOfName())) ? '' :
            '<CareOfName>'.htmlspecialchars($addressFields->getCareOfName(), ENT_NOQUOTES).'</CareOfName>';
        $address .= (is_null($addressFields->getPoBox())) ? '' :
            '<PoBox>'.htmlspecialchars($addressFields->getPoBox(), ENT_NOQUOTES).'</PoBox>';

        // --- shares ---
        $shares = '';
        if (!$guarantor) {
            $shareFields = $this->getAllotmentShares();

            $shares .= '<ShareClass>'.htmlspecialchars($shareFields->getShareClass(), ENT_NOQUOTES).'</ShareClass>';
            $shares .= '<NumShares>'.htmlspecialchars($shareFields->getNumShares(), ENT_NOQUOTES).'</NumShares>';
            $shares .= '<AmountPaidDuePerShare>'.htmlspecialchars($shareFields->getShareValue(), ENT_NOQUOTES).'</AmountPaidDuePerShare>';
            $shares .= '<AmountUnpaidPerShare>0</AmountUnpaidPerShare>';
            $shares .= '<ShareCurrency>'.htmlspecialchars($shareFields->getCurrency(), ENT_NOQUOTES).'</ShareCurrency>';
            $shares .= '<ShareValue>'.htmlspecialchars($shareFields->getShareValue(), ENT_NOQUOTES).'</ShareValue>';

            $shares = '<Shares>' . $shares . '</Shares>';
        } else {
            $shares = '<AmountGuaranteed>1</AmountGuaranteed>';
        }

        // --- return xml ---
        $xml = "<Person>
                $person
            </Person>
            <Address>
                $address
            </Address>
            $authentication
            $shares";

        if (!$guarantor) {
            return "<Subscribers>$xml</Subscribers>";
        } else {
            return "<Guarantors>$xml</Guarantors>";
       }
    }
}
