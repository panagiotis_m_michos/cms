<?php

final class IncorporationCorporatePsc extends CorporatePsc
{
    /**
     * @var int
     */
    private $incorporationMemberId;

    /**
     * @var int
     */
    private $formSubmissionId;

    /**
     * @var bool
     */
    private $nominee;

    /**
     * @var string
     */
    private static $type = 'PSC';

    /**
     * @var bool
     */
    private static $corporate = 1;

    /**
     * @var string
     */
    private static $tableName = 'ch_incorporation_member';

    /**
     * @param int $formSubmissionId
     * @param int $annualReturnOfficerId
     * @throws Exception
     */
    public function __construct($formSubmissionId, $annualReturnOfficerId = NULL)
    {
        if (!preg_match('/^\d+$/', $formSubmissionId)) {
            throw new Exception('form submissio id has to be integer');
        }

        parent::__construct();

        // --- do not save anything in db ---
        if (!$annualReturnOfficerId) {
            $this->incorporationMemberId = NULL;
            $this->formSubmissionId = $formSubmissionId;

            return;
        }

        if (!preg_match('/^\d+$/', $annualReturnOfficerId)) {
            throw new Exception('incorporation member id has to be integer');
        }

        $sql = "SELECT * FROM `" . self::$tableName . "`
			    WHERE `incorporation_member_id` = $annualReturnOfficerId AND `form_submission_id` = $formSubmissionId";

        if (!$result = CHFiling::getDb()->fetchRow($sql)) {
            throw new Exception("Record doesn't exist");
        }

        $this->incorporationMemberId = $result['incorporation_member_id'];
        $this->formSubmissionId = $result['form_submission_id'];
        $this->nominee = $result['nominee'];
        $this->setFields($result);
    }

    /**
     * @param int $formSubmissionId
     * @param int $incorporationMemberId
     * @return IncorporationCorporatePsc
     */
    public static function getIncorporationCorporatePsc($formSubmissionId, $incorporationMemberId)
    {
        return new IncorporationCorporatePsc($formSubmissionId, $incorporationMemberId);
    }

    /**
     * @param int $formSubmissionId
     * @return IncorporationCorporatePsc
     */
    public static function getNewIncorporationCorporatePsc($formSubmissionId)
    {
        return new IncorporationCorporatePsc($formSubmissionId);
    }

    /**
     * @return string
     */
    public function getType()
    {
        return self::$type;
    }

    public function remove()
    {
        CHFiling::getDb()->delete(self::$tableName, 'incorporation_member_id = ' . $this->incorporationMemberId);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->incorporationMemberId;
    }

    /**
     * @param bool $nominee
     */
    public function setNominee($nominee)
    {
        $this->nominee = ($nominee) ? 1 : 0;
    }

    /**
     * @return bool
     */
    public function isNominee()
    {
        return $this->nominee;
    }

    /**
     * @return bool
     */
    public function isComplete()
    {
        $allowedFields = array(
            'authentication', 'corporate_name',
            'premise', 'street', 'post_town', 'country',
            'identification_type'
        );

        $fields = array_merge(
            $this->getCorporate()->getFields(),
            $this->getAddress()->getFields(),
            $this->getIdentification()->getFields()
        );
        $fields['identification_type'] = $this->getIdentification()->getTypeName();

        // --- set authentication in one field ---
        if (!is_null($this->getAuthentication()->getDbFields())) {
            $fields['authentication'] = $this->getAuthentication()->getDbFields();
        }

        foreach($allowedFields as $v) {
            if (!isset($fields[$v]) || is_null($fields[$v])) {
                return 0;
            }
        }

        return 1;
    }

    /**
     * @throws Exception
     */
    public function save()
    {
        if (!$this->isComplete()) {
            throw new Exception('You need to set all compulsory fielsd');
        }

        // --- prepare data ---
        $address = $this->getAddress()->getFields();
        unset($address['secure_address_ind']);
        $data = array_merge(
            $this->getCorporate()->getFields(),
            $address,
            $this->getIdentification()->getFields(),
            $this->getNatureOfControl()->getFields()
        );

        // --- set authentication in one field ---
        $data['identification_type'] = $this->getIdentification()->getTypeName();
        $data['authentication'] = $this->getAuthentication()->getDbFields();
        $data['form_submission_id'] = $this->formSubmissionId;
        $data['corporate'] = self::$corporate;
        $data['type'] = self::$type;
        $data['nominee'] = $this->nominee;
        $data['consentToAct'] = $this->hasConsentToAct();

        // --- save data ---
        if (isset($this->incorporationMemberId) && !is_null($this->incorporationMemberId)) {
            CHFiling::getDb()->update(
                self::$tableName,
                $data,
                'incorporation_member_id = ' . $this->incorporationMemberId
            );
        } else {
            CHFiling::getDb()->insert(self::$tableName, $data);
            $this->incorporationMemberId = CHFiling::getDb()->lastInsertId();
        }
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getXml()
    {
        // --- check if is complete ---
        if (!$this->isComplete()) {
            throw new Exception('This object is not complete');
        }

        $compincorp = CompanyIncorporation::getCompanyIncorporation($this->formSubmissionId);
        $type = $compincorp->getType();

        // --- prepare xml ---

        // --- corporate ---
        $corporate = '';
        $corporateFields = $this->getCorporate();

        $corporate .= '<CorporateName>'.htmlspecialchars($corporateFields->getCorporateName(), ENT_NOQUOTES).'</CorporateName>';

        // --- address ---
        $address = '';
        $addressFields = $this->getAddress();

        $address .= '<Premise>'.htmlspecialchars($addressFields->getPremise(), ENT_NOQUOTES).'</Premise>';
        $address .= '<Street>'.htmlspecialchars($addressFields->getStreet(), ENT_NOQUOTES).'</Street>';
        $address .= (is_null($addressFields->getThoroughfare())) ? '' :
            '<Thoroughfare>'.htmlspecialchars($addressFields->getThoroughfare(), ENT_NOQUOTES).'</Thoroughfare>';
        $address .= '<PostTown>'.htmlspecialchars($addressFields->getPostTown(), ENT_NOQUOTES).'</PostTown>';

        //fix empty and -- Select -- in county problems
        $address .= (is_null($addressFields->getCounty())) || strlen(stristr($addressFields->getCounty(), 'select'))>0 ? '' :
            '<County>'.htmlspecialchars($addressFields->getCounty(), ENT_NOQUOTES).'</County>';

        if (isset(Address::$nonForeignCountries[$addressFields->getCountry()])) {
            $address .= '<Country>'.htmlspecialchars($addressFields->getCountry(), ENT_NOQUOTES).'</Country>';
        } else {
            $address .= '<OtherForeignCountry>'.htmlspecialchars($addressFields->getCountry(), ENT_NOQUOTES).'</OtherForeignCountry>';
        }

        $address .= (is_null($addressFields->getPostcode())) ? '' :
            '<Postcode>'.htmlspecialchars($addressFields->getPostcode(), ENT_NOQUOTES).'</Postcode>';

        // --- identification ---
        $identification = '';
        $identificationFields = $this->getIdentification();

        $identification .= (is_null($identificationFields->getPlaceRegistered())) ? '' :
            '<PSCPlaceRegistered>'.htmlspecialchars($identificationFields->getPlaceRegistered(), ENT_NOQUOTES).'</PSCPlaceRegistered>';
        $identification .= (is_null($identificationFields->getRegistrationNumber())) ? '' :
            '<PSCRegistrationNumber>'.htmlspecialchars($identificationFields->getRegistrationNumber(), ENT_NOQUOTES).'</PSCRegistrationNumber>';
        $identification .= '<LawGoverned>'.htmlspecialchars($identificationFields->getLawGoverned(), ENT_NOQUOTES).'</LawGoverned>';
        $identification .= '<LegalForm>'.htmlspecialchars($identificationFields->getLegalForm(), ENT_NOQUOTES).'</LegalForm>';
        $identification .= (is_null($identificationFields->getCountryOrState())) ? '' :
            '<CountryOrState>'.htmlspecialchars($identificationFields->getCountryOrState(), ENT_NOQUOTES).'</CountryOrState>';

        //$directoro = ($type == 'LLP') ? '<Member><DesignatedInd>true</DesignatedInd>': '<Director>';
        //$directorc = ($type == 'LLP') ? '</Member>': '</Director>';

        $natureOfControlsFields = $this->getNatureOfControl();

        if ($type == 'LLP') {
            $natureOfControls = '<LLPNatureOfControls>';
        } else {
            $natureOfControls = '<NatureOfControls>';
        }
        if ($natureOfControlsFields->getOwnershipOfShares()) {
            $natureOfControls .= '<NatureOfControl>'.htmlspecialchars($natureOfControlsFields->getOwnershipOfShares(), ENT_NOQUOTES).'</NatureOfControl>';
        }
        if ($natureOfControlsFields->getOwnershipOfVotingRights()) {
            $natureOfControls .= '<NatureOfControl>'.htmlspecialchars($natureOfControlsFields->getOwnershipOfVotingRights(), ENT_NOQUOTES).'</NatureOfControl>';
        }
        if ($natureOfControlsFields->getRightToAppointAndRemoveDirectors()) {
            $natureOfControls .= '<NatureOfControl>'.htmlspecialchars($natureOfControlsFields->getRightToAppointAndRemoveDirectors(), ENT_NOQUOTES).'</NatureOfControl>';
        }
        if ($natureOfControlsFields->getSignificantInfluenceOrControl()) {
            $natureOfControls .= '<NatureOfControl>'.htmlspecialchars($natureOfControlsFields->getSignificantInfluenceOrControl(), ENT_NOQUOTES).'</NatureOfControl>';
        }
        if ($type == 'LLP') {
            $natureOfControls .= '</LLPNatureOfControls>';
        } else {
            $natureOfControls .= '</NatureOfControls>';
        }

        // --- return xml ---
        return "
        <PSC>
            <PSCNotification>
                <Corporate>
                    $corporate
                    <Address>
                        $address
                    </Address>
                    <PSCCompanyIdentification>
                        $identification
                    </PSCCompanyIdentification>
                </Corporate>
                $natureOfControls
            </PSCNotification>
        </PSC>";
    }
}

