<?php

class OfficerChangeDetails implements Form
{
    /**
     * @var int
     */
    private $formSubmissionId;

    /**
     * @var string
     */
    private $rejectDescription;

    /**
     * @var array
     */
    private $fields;

    /**
     * @var type
     */
    private $companyType;

    /**
     * @var string
     */
    private static $tableName = 'ch_officer_change';

    /**
     * @param int $formSubmissionId
     * @throws Exception
     */
    private function  __construct($formSubmissionId)
    {
        // --- check that formSubmissionId is int ---
        if (!preg_match('/^\d+$/', $formSubmissionId)) {
            throw new Exception('formSubmissionId has to be integer');
        }

        // --- load data from database ---
        $sql = "
            SELECT * FROM " . self::$tableName . "
            WHERE `form_submission_id` = $formSubmissionId";
        if (!$result = CHFiling::getDb()->fetchRow($sql)) {
            throw new Exception('Officer change details doesn\'t exist');
        }

        // --- set instance variables ---
        $this->formSubmissionId = $result['form_submission_id'];
        $this->rejectDescription = $result['reject_description'];

        unset($result['form_submission_id']);
        unset($result['reject_description']);

        // --- set fields ---
        $this->fields = $result;
    }

    /**
     * @param int $formSubmissionId
     *
     * @return OfficerChangeDetails
     */
    public static function getOfficerChangeDetails($formSubmissionId)
    {
        return new self($formSubmissionId);
    }

    /**
     * @param int $formSubmissionId
     * @throws Exception
     * @return OfficerChangeDetails
     */
    public static function getNewOfficerChangeDetails($formSubmissionId)
    {
        // --- check that formSubmissionId is int ---
        if (!preg_match('/^\d+$/', $formSubmissionId)) {
            throw new Exception('formSubmissionId has to be integer');
        }

        // --- insert new company incorporation ---
        $data = array('form_submission_id' => $formSubmissionId);
        CHFiling::getDb()->insert(self::$tableName, $data);

        // --- return new instance ---
        return new self($formSubmissionId);
    }

    /**
     * checks if array fields has filled in all compulsory fields - supplied
     * as first argument
     *
     * @param array $compulsoryFields
     * @param array $fields
     * @return int
     */
    private function isComplete($compulsoryFields, $fields)
    {
        // --- check if all fields are set ---
        foreach ($compulsoryFields as $v) {
            if (!isset($fields[$v])) {
                return 0;
            }
        }
        return 1;
    }

    /**
     * sets new Registered office address that is going to be changed
     *
     * @param array $fields
     * @throws Exception
     */
    public function setFields($fields)
    {
        // TODO - finish checking fields ---

        // --- check fields ---
        if (!$this->isComplete(array('type', 'corporate'), $fields)) {
            throw new Exception('you need to set type and corporate');
        }

        // --- if it's director we need dob  ---
        if ($fields['type'] == 'DIR' && $fields['corporate'] == 0) {
            if (!isset($fields['dob']) || $fields['dob'] == NULL) {
                throw new Exception('you need to set dob for director');
            }
        }

        // --- if it's corporate we need corporate name ---
        if ($fields['corporate']) {
            if (!isset($fields['corporate_name']) || $fields['corporate_name'] == NULL) {
                throw new Exception('you need to set corporate name for corporate officer');
            }
        }

        $this->companyType = isset($fields['companyType']) ? $fields['companyType'] : '';
        unset($fields['new_residential_care_of_name']);
        unset($fields['new_residential_po_box']);
        unset($fields['new_secure_address_ind']);
        unset($fields['companyType']);

        foreach ($fields as $k => $v) {
            if (empty($v)) {
                $fields[$k] = NULL;
            }
        }

        CHFiling::getDb()->update(self::$tableName, $fields, 'form_submission_id = ' . $this->formSubmissionId);
        $this->fields = CHFiling::getDb()->fetchRow(
            'SELECT * FROM ' . self::$tableName . ' WHERE form_submission_id = ' . $this->formSubmissionId
        );
    }

    /**
     * @return string
     */
    public function getXml()
    {
        $test = (CHFiling::getGatewayTest()) ? '' : '';

        // --- change type ---
        $changeType = '';
        // --- corporate ---
        if ($this->fields['corporate']) {
            $changeType .= '<CorporateName>'.htmlspecialchars($this->fields['corporate_name'], ENT_NOQUOTES).'</CorporateName>';
        } else { // --- person ---
            $changeType .= ($this->fields['title'] == NULL) ? '' :
                '<Title>'.htmlspecialchars($this->fields['title'], ENT_NOQUOTES).'</Title>';
            $changeType .= '<Surname>'.htmlspecialchars($this->fields['surname'], ENT_NOQUOTES).'</Surname>';

            if ($this->fields['forename'] != NULL) {
                $changeType .= '<Forename>' . htmlspecialchars($this->fields['forename'], ENT_NOQUOTES);

                if ($this->fields['middle_name'] != NULL) {
                    $changeType .= ' ' . htmlspecialchars($this->fields['middle_name'], ENT_NOQUOTES);
                };
                $changeType .= '</Forename>';
            }

            if ($this->fields['type'] == 'DIR') {
                $changeType .= '<DOB>'.htmlspecialchars($this->fields['dob'], ENT_NOQUOTES).'</DOB>';
            }
        }

            // --- change ---
        $change = '<Change>';
        // --- corporate ---
        if ($this->fields['corporate']) {
            // --- corporate name --
            $change .= ($this->fields['new_corporate_name'] == NULL) ? '' :
                '<CorporateName>'.htmlspecialchars($this->fields['new_corporate_name'], ENT_NOQUOTES).'</CorporateName>';

            // --- new address ---
            if ($this->fields['new_premise'] != NULL) {
                $change .= '<Address>';
                $change .= '<Premise>'.htmlspecialchars($this->fields['new_premise'], ENT_NOQUOTES).'</Premise>';
                $change .= '<Street>'.htmlspecialchars($this->fields['new_street'], ENT_NOQUOTES).'</Street>';
                $change .= ($this->fields['new_thoroughfare'] == NULL) ? '' :
                    '<Thoroughfare>'.htmlspecialchars($this->fields['new_thoroughfare'], ENT_NOQUOTES).'</Thoroughfare>';
                $change .= '<PostTown>'.htmlspecialchars($this->fields['new_post_town'], ENT_NOQUOTES).'</PostTown>';
                $change .= ($this->fields['new_county'] == NULL) ? '' :
                    '<County>'.htmlspecialchars($this->fields['new_county'], ENT_NOQUOTES).'</County>';

                if (isset(Address::$nonForeignCountries[$this->fields['new_country']])) {
                    $change .= '<Country>'.htmlspecialchars($this->fields['new_country'], ENT_NOQUOTES).'</Country>';
                } else {
                    $change .= '<OtherForeignCountry>'.htmlspecialchars($this->fields['new_country'], ENT_NOQUOTES).'</OtherForeignCountry>';
                }

                $change .= ($this->fields['new_postcode'] == NULL) ? '' :
                    '<Postcode>'.htmlspecialchars($this->fields['new_postcode'], ENT_NOQUOTES).'</Postcode>';
                $change .= ($this->fields['new_care_of_name'] == NULL) ? '' :
                    '<CareOfName>'.htmlspecialchars($this->fields['new_care_of_name'], ENT_NOQUOTES).'</CareOfName>';
                $change .= ($this->fields['new_po_box'] == NULL) ? '' :
                    '<PoBox>'.htmlspecialchars($this->fields['new_po_box'], ENT_NOQUOTES).'</PoBox>';
                $change .= '</Address>';
            }

            // --- identification - EEA/NonEEA
            if ($this->fields['new_identification_type'] != NULL) {
                $identification = '';
                if ($this->fields['new_identification_type'] == 'EEA') {
                    $identification .= '<PlaceRegistered>'.htmlspecialchars($this->fields['new_place_registered'], ENT_NOQUOTES).'</PlaceRegistered>';
                    $identification .= '<RegistrationNumber>'.htmlspecialchars($this->fields['new_registration_number'], ENT_NOQUOTES).'</RegistrationNumber>';
                    $change .= '<CompanyIdentification><EEA>'.$identification.'</EEA></CompanyIdentification>';
                } else {
                    $identification .= ($this->fields['new_place_registered'] == NULL) ? '' :
                        '<PlaceRegistered>'.htmlspecialchars($this->fields['new_place_registered'], ENT_NOQUOTES).'</PlaceRegistered>';
                    $identification .= ($this->fields['new_registration_number'] == NULL) ? '' :
                        '<RegistrationNumber>'.htmlspecialchars($this->fields['new_registration_number'], ENT_NOQUOTES).'</RegistrationNumber>';
                    $identification .= '<LawGoverned>'.htmlspecialchars($this->fields['new_law_governed'], ENT_NOQUOTES).'</LawGoverned>';
                    $identification .= '<LegalForm>'.htmlspecialchars($this->fields['new_legal_form'], ENT_NOQUOTES).'</LegalForm>';
                    $change .= '<CompanyIdentification><NonEEA>'.$identification.'</NonEEA></CompanyIdentification>';
                }
            }

        } else { // --- person ---
            // --- new name details ---
            if ($this->fields['new_surname'] != NULL) {
                $change .= '<Name>';
                $change .= ($this->fields['new_title'] == NULL) ? '' :
                    '<Title>'.htmlspecialchars($this->fields['new_title'], ENT_NOQUOTES).'</Title>';
                $change .= '<Surname>'.htmlspecialchars($this->fields['new_surname'], ENT_NOQUOTES).'</Surname>';

                if ($this->fields['new_forename'] != NULL) {
                    $change .= '<Forename>' . htmlspecialchars($this->fields['new_forename'], ENT_NOQUOTES);

                    if ($this->fields['new_middle_name'] != NULL) {
                        $change .= ' ' . htmlspecialchars($this->fields['new_middle_name'], ENT_NOQUOTES);
                    };
                    $change .= '</Forename>';
                }
                $change .= '</Name>';
            }
            // --- new service address ---
            if ($this->fields['new_premise'] != NULL) {
                $change .= '<ServiceAddress><Address>';
                $change .= '<Premise>'.htmlspecialchars($this->fields['new_premise'], ENT_NOQUOTES).'</Premise>';
                $change .= '<Street>'.htmlspecialchars($this->fields['new_street'], ENT_NOQUOTES).'</Street>';
                $change .= ($this->fields['new_thoroughfare'] == NULL) ? '' :
                    '<Thoroughfare>'.htmlspecialchars($this->fields['new_thoroughfare'], ENT_NOQUOTES).'</Thoroughfare>';
                $change .= '<PostTown>'.htmlspecialchars($this->fields['new_post_town'], ENT_NOQUOTES).'</PostTown>';
                $change .= ($this->fields['new_county'] == NULL) ? '' :
                    '<County>'.htmlspecialchars($this->fields['new_county'], ENT_NOQUOTES).'</County>';

                if (isset(Address::$nonForeignCountries[$this->fields['new_country']])) {
                    $change .= '<Country>'.htmlspecialchars($this->fields['new_country'], ENT_NOQUOTES).'</Country>';
                } else {
                    $change .= '<OtherForeignCountry>'.htmlspecialchars($this->fields['new_country'], ENT_NOQUOTES).'</OtherForeignCountry>';
                }

                $change .= ($this->fields['new_postcode'] == NULL) ? '' :
                    '<Postcode>'.htmlspecialchars($this->fields['new_postcode'], ENT_NOQUOTES).'</Postcode>';
                $change .= ($this->fields['new_care_of_name'] == NULL) ? '' :
                    '<CareOfName>'.htmlspecialchars($this->fields['new_care_of_name'], ENT_NOQUOTES).'</CareOfName>';
                $change .= ($this->fields['new_po_box'] == NULL) ? '' :
                    '<PoBox>'.htmlspecialchars($this->fields['new_po_box'], ENT_NOQUOTES).'</PoBox>';
                //nick changes  $change .= '</Address></ServiceAddress>';
                $change .= '</Address>';
                if ($this->fields['type'] == 'DIR') {
                    if ($this->fields['new_residential_premise'] == NULL) {
                        $change .= '<ResidentialAddressUnchangedInd>true</ResidentialAddressUnchangedInd>';
                    }
                }
                $change .= '</ServiceAddress>';
                //nick changes
            }
            // --- dierector person specific ---
            if ($this->fields['type'] == 'DIR') {
                // --- residential address ---
                if ($this->fields['new_residential_premise'] != NULL) {
                    $change .= '<ResidentialAddress><Address>';
                    $change .= '<Premise>'.htmlspecialchars($this->fields['new_residential_premise'], ENT_NOQUOTES).'</Premise>';
                    $change .= '<Street>'.htmlspecialchars($this->fields['new_residential_street'], ENT_NOQUOTES).'</Street>';
                    $change .= ($this->fields['new_residential_thoroughfare'] == NULL) ? '' :
                        '<Thoroughfare>'.htmlspecialchars($this->fields['new_residential_thoroughfare'], ENT_NOQUOTES).'</Thoroughfare>';
                    $change .= '<PostTown>'.htmlspecialchars($this->fields['new_residential_post_town'], ENT_NOQUOTES).'</PostTown>';
                    $change .= ($this->fields['new_residential_county'] == NULL) ? '' :
                        '<County>'.htmlspecialchars($this->fields['new_residential_county'], ENT_NOQUOTES).'</County>';

                    if (isset(Address::$nonForeignCountries[$this->fields['new_residential_country']])) {
                        $change .= '<Country>'.htmlspecialchars($this->fields['new_residential_country'], ENT_NOQUOTES).'</Country>';
                    } else {
                        $change .= '<OtherForeignCountry>'.htmlspecialchars($this->fields['new_residential_country'], ENT_NOQUOTES).'</OtherForeignCountry>';
                    }

                    $change .= ($this->fields['new_residential_postcode'] == NULL) ? '' :
                        '<Postcode>'.htmlspecialchars($this->fields['new_residential_postcode'], ENT_NOQUOTES).'</Postcode>';

                    $change .= '</Address>';
                    $change .= '</ResidentialAddress>';
                }
                // --- nationality ---
                if ($this->fields['new_nationality'] != NULL) {
                    $change .= '<Nationality>'.htmlspecialchars($this->fields['new_nationality'], ENT_NOQUOTES).'</Nationality>';
                }
                // --- country of residence ---
                if ($this->fields['new_country_of_residence'] != NULL) {
                    $change .= '<CountryOfResidence>'.htmlspecialchars($this->fields['new_country_of_residence'], ENT_NOQUOTES).'</CountryOfResidence>';
                }
                // --- occupation ---
                if ($this->fields['new_occupation'] != NULL) {
                    $change .= '<Occupation>'.htmlspecialchars($this->fields['new_occupation'], ENT_NOQUOTES).'</Occupation>';
                }
            } // --- end director specific ---
        }

        if ($this->companyType == 'LLP') {
            if ($this->fields['consentToAct']) {
                $authentication = '<ConsentToAct>true</ConsentToAct>';
            } else {
                $authentication = '<ConsentToAct>false</ConsentToAct>';
            }

            $designatedInd = $this->fields['designated_ind']== 1 ? 'true' : 'false';
            $change .= '<DesignatedInd>'.$designatedInd.'</DesignatedInd>';
            $change .= $authentication;
        }
        $change .= '</Change>';

        // --- create main change element ---

        if ($this->companyType != 'LLP') {
            $type = ($this->fields['type'] == 'DIR') ? 'Director' : 'Secretary';
        } else {
            $type = 'Member';
        }
        $corporate = $this->fields['corporate'] ? 'Corporate' : 'Person';

        $change = '<'.$type.'><'.$corporate.'>'.
                $changeType .
                $change
            .'</'.$corporate.'></'.$type.'>';

        $xsdSchema = 'http://xml'.$test.'gw.companieshouse.gov.uk/v2-1/schema/forms/OfficerChangeDetails-v2-8.xsd';

        return '<OfficerChangeDetails
            xmlns="http://xmlgw.companieshouse.gov.uk"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://xmlgw.companieshouse.gov.uk
            '.$xsdSchema.'">
                <DateOfChange>'.date('Y-m-d').'</DateOfChange>'.
                $change
            .'</OfficerChangeDetails>';
    }
}



