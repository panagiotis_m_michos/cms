<?php

use Utils\Date;

class ChangeAccountingReferenceDate implements Form
{

    /**
     *
     * @var int
     */
    private $formSubmissionId;

    /**
     *
     * @var string
     */
    private static $ard;

    /**
     *
     * @var string
     */
    private $rejectDescription;
    /**
     *
     * @var array
     */
    private $fields;

    /**
     *
     * @var string
     */
    private static $tableName = 'ch_change_accounting_reference_date';

    /**
     *
     * @param int $formSubmissionId
     */
    private function  __construct($formSubmissionId)
    {
        //   pr($formSubmissionId);exit;
        // --- check that formSubmissionId is int ---
        if (!preg_match('/^\d+$/', $formSubmissionId)) {
            throw new Exception('formSubmissionId has to be integer');
        }

        // --- load data from database ---
        $sql = "
            SELECT * FROM ".self::$tableName."
            WHERE `form_submission_id` = $formSubmissionId";

        if (!$result = CHFiling::getDb()->fetchRow($sql)) {
            throw new Exception('change accounting reference date doesn\'t exist');
        }
        // --- set instance variables ---
        $this->formSubmissionId = $result['form_submission_id'];
        $this->rejectDescription= $result['reject_description'];

        // unset($result['form_submission_id']);
        // unset($result['reject_description']);
        //
        // --- set fields for ---
        $accounting['date'] = $result['date'];
        $accounting['extensionReason'] = $result['extensionReason'];
        $accounting['extensionAuthorisedCode'] = $result['extensionAuthorisedCode'];
        // --- set fields ---
        $this->fields = $accounting;

        $sql = "
            SELECT ch_company.accounts_ref_date
            FROM ".self::$tableName."
            INNER JOIN ch_form_submission USING (`form_submission_id`)
            INNER JOIN ch_company USING (`company_id`)
            WHERE `form_submission_id` = $formSubmissionId";

        $result = CHFiling::getDb()->fetchRow($sql);
        self::$ard = $result['accounts_ref_date'];
    }
    /**
     *
     * @param int $formSubmissionId
     * @return CompanyIncorporation
     */
    public static function getChangeAccountingReferenceDate($formSubmissionId)
    {
        return new self($formSubmissionId);
    }

    /**
     * @param string $ard
     */
    public static function setArd($ard)
    {
        self::$ard = $ard;
    }

    /**
     * @param int $formSubmissionId
     * @return ChangeAccountingReferenceDate
     * @throws Exception
     */
    public static function getNewChangeAccountingReferenceDate($formSubmissionId)
    {
        // --- check that formSubmissionId is int ---
        if (!preg_match('/^\d+$/', $formSubmissionId)) {
            throw new Exception('formSubmissionId has to be integer');
        }

        // --- insert change_accounting_reference_date ---
        $data = array('form_submission_id'=> $formSubmissionId);
        CHFiling::getDb()->insert(self::$tableName, $data);
        //pr('get'.$formSubmissionId);exit;
        // --- return new instance ---
        return new self($formSubmissionId);
    }

    //    private function isComplete($compulsoryFields, $fields) {
    //        // --- check if all fields are set ---
    //    }

    public function setChangeAccountingReferenceDate($fields)
    {
        unset($fields['submited_changeDate']);
        unset($fields['send']);
        unset($fields['previosDate']);
        unset($fields['ARDRange']);
        foreach ($fields as $k => $v) {
            if (empty($v)) {
                $fields[$k] = NULL;
            }
        }
        CHFiling::getDb()->update(self::$tableName, $fields, 'form_submission_id = '.$this->formSubmissionId);
        $this->fields = $fields;

    }

    public function getXml()
    {
        $ard = !empty(self::$ard) ? Date::createFromFormat('d-m', self::$ard) : FALSE;
        if (!$ard) {
            throw new Exception('Please try to sync your company with Company House ,it looks that you don\'t have a valid Accounting Reference Date !');
        }
        $ardCheck = $ard->format('Y-m-d');
        //$ardShort = date('d-m',$ard);

        $change =  "\n".'<ChangeToPeriod>';
        if ($ardCheck > $this->fields['date']) {
            $change .= 'SHORT';
        } else {
            $change .= 'EXTEND';
        }

        $change .= '</ChangeToPeriod>'."\n";
        $change .= '<AmendedAccountRefDate>';
        $change .= $this->fields['date'];
        $change .= '</AmendedAccountRefDate>'. "\n";

        if ($this->fields['extensionReason'] and ($ardCheck < $this->fields['date'])) {
            $change .= '<FiveYearExtensionDetails>'. "\n";
            $change .= '<ExtensionReason>';
            $change .= $this->fields['extensionReason'];
            $change .= '</ExtensionReason>'. "\n";
            if ($this->fields['extensionReason'] == 'STATE') {
                $change .= '<ExtensionAuthorisedCode>';
                $change .= $this->fields['extensionAuthorisedCode'];
                $change .= '</ExtensionAuthorisedCode>'. "\n";
            }
            $change .= '</FiveYearExtensionDetails>'. "\n";
        }

        return '<ChangeAccountingReferenceDate
            xmlns="http://xmlgw.companieshouse.gov.uk"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://xmlgw.companieshouse.gov.uk
            http://xmlgw.companieshouse.gov.uk/v2-1/schema/forms/ChangeAccountingReferenceDate-v2-1.xsd">
            <AccountRefDate>'.$ardCheck.'</AccountRefDate>'.
            $change
            .'</ChangeAccountingReferenceDate>';
    }
}
