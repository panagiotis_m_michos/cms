<?php

class AnnualReturnTransfer extends Element {

    /**
     *
     * @var int
     */
    private $transferId;

    /**
     *
     * @var Shareholding
     */
    private $shareholding;

    /**
     *
     * @var string
     */
    private static $tableName = 'ch_annual_return_transfer';

    /**
     *
     * @param Shareholding $shareholding
     * @param int $transferId
     */
    protected function  __construct(Shareholding $shareholding, $transferId = null) {

        $this->shareholding = $shareholding;
        $shareholdingId     = $shareholding->getId();

        parent::__construct($this->getDBFields());

        // --- do not save anything in db ---
        if (is_null($transferId)) {
            $this->transferId    = null;
            return;
        }

        // --- check that transferId is int ---
        if (!preg_match('/^\d+$/', $transferId)) {
            throw new Exception('transfer id has to be integer');
        }

        $sql = "
			SELECT * FROM `".self::$tableName."`
			WHERE `annual_return_transfer_id` = $transferId AND `annual_return_shareholding_id` = $shareholdingId";
        if (!$result = CHFiling::getDb()->fetchRow($sql)) {
            throw new Exception('Record doesn\'t exist');
        }

        // --- set instance variables ---
		$this->transferId = $result['annual_return_transfer_id'];
        $this->setFields($result);
    }

    /**
     * getDBFields
     *
     * allowed fields - fields from database
     * true indicates that the field is compulsory
     *
     * @access public
     * @return array
     */
    public function getDBFields()
    {
        return array(
            'date_of_transfer'  => 1,
            'shares_transfered' => 1
        );
    }

    /**
     *
     * @param Shareholding $shareholding
     * @param int $transferId
     * @return AnnualReturnTransfer
     */
    public static function getAnnualReturnTransfer(Shareholding $shareholding, $transferId) {
        return new AnnualReturnTransfer($shareholding, $transferId);
    }

    /**
     *
     * @param Shareholding $shareholding
     * @return AnnualReturnTransfer
     */
    public static function getNewAnnualReturnTransfer(Shareholding $shareholding) {
        return new AnnualReturnTransfer($shareholding);
    }

    /**
     * Returns all Shareholders that belong to the specified shareholding, if
     * none found, empty array is returned.
     * Index is shareholder id.
     *
     * @param Shareholding $shareholding
     * @return array<AnnualReturnTransfer>
     */
    public static function getAnnualReturnTransfers(Shareholding $shareholding) {

        $shareholdingId = $shareholding->getId();

        $sql = "
			SELECT annual_return_transfer_id FROM `".self::$tableName."`
			WHERE `annual_return_shareholding_id` = $shareholdingId";
        $result = CHFiling::getDb()->fetchAll($sql);

        if (empty($result)) {
            return array();
        }

        $transfers = array();
        foreach($result as $id) {
            $transferId = $id['annual_return_transfer_id'];
            $transfers[$transferId] = new AnnualReturnTransfer($shareholding, $transferId);
        }

        return $transfers;
    }

    /**
     *
     */
    public function remove() {
        CHFiling::getDb()->delete(self::$tableName, 'annual_return_transfer_id = '.$this->transferId);
    }

    /**
     *
     * @return int
     */
    public function getId() {
        return $this->transferId;
    }

    /**
     * returns false if object is not ready for saving/sending xml, otherwise
     * returns true
     *
     * @return boolean
     */
    public function isComplete() {
        $complete = true;

        $fields = $this->getFields();

        if (!isset($fields['date_of_transfer']) || empty($fields['date_of_transfer'])) {
            $complete = false;
        }
        if (!isset($fields['shares_transfered']) || empty($fields['shares_transfered'])) {
            $complete = false;
        }

        return $complete;
    }

    /**
     * saves objecte into database
     *
     * @return void
     */
    public function save() {

        if (!$this->isComplete()) {
            throw new Exception('You need to set all compulsory fielsd');
        }

        // --- prepare data ---
        $data = $this->getFields();
        $data['annual_return_shareholding_id'] = $this->shareholding->getId();

        // --- save data ---
        if (isset($this->transferId) && !is_null($this->transferId)) {
            CHFiling::getDb()->update(self::$tableName, $data, 'annual_return_transfer_id = '.$this->transferId);
        } else {
            CHFiling::getDb()->insert(self::$tableName, $data);
            $this->transferId = CHFiling::getDb()->lastInsertId();
        }
    }

    /**
     * setDateOfTransfer
     *
     * Pre-condition: $date has to be in date format YYYY-MM-DD
     * and has to be less or equal to today's date
     *
     * @param string $date
     * @access public
     * @return void
     */
    public function setDateOfTransfer($date)
    {
        // --- date ---
        // check format
        if (!preg_match('/^[\d]{4}-[\d]{1,2}-[\d]{1,2}$/', $date)) {
            throw new Exception('Date has to be in YYYY-MM-DD format');
        }

        $d      = explode('-', $date);
        $month  = $d[1];
        $day    = $d[2];
        $year   = $d[0];

        // check if it's correct
        if (!checkdate($month, $day, $year)) {
            throw new Exception('Date has to be correct date.');
        }

        // check if date is less than today's day
        if (strtotime($date) > time()) {
            throw new Exception('Date is more than today\'s date');
        }
        $this->fields['date_of_transfer'] = $date;
    }

    /**
     * getDateOfTransfer
     *
     * @access public
     * @return string - date format YYYY-MM-DD
     */
    public function getDateOfTransfer() {
        return $this->fields['date_of_transfer'];
    }

    /**
     *
     * @param int $shares
     */
    public function setSharesTransfered($shares) {

        if (!preg_match('/^\d+$/', $shares)) {
            throw new Exception('Shares has to be integer');
        }

        // TODO - cannot be more than share capital

        $this->fields['shares_transfered'] = $shares;
    }

    /**
     *
     * @return int
     */
    public function getSharesTransfered() {
        return $this->fields['shares_transfered'];
    }

    /**
     *
     * @return xml - xml string, part of the shareholder
     */
    public function getXml() {
        $data = $this->getFields();

        return '<Transfers><DateOfTransfer>'.$data['date_of_transfer'].'</DateOfTransfer>'.
            '<NumberSharesTransferred>'.$data['shares_transfered'].'</NumberSharesTransferred></Transfers>';
    }

}
