<?php

class AnnualReturnShareholder extends Shareholder {

    /**
     *
     * @var int
     */
    private $shareholderId;

    /**
     *
     * @var Shareholding
     */
    private $shareholding;

    /**
     *
     * @var string
     */
    private static $tableName = 'ch_annual_return_shareholder';

    /**
     *
     * @param Shareholding $shareholding
     * @param int $shareholderId
     */
    public function  __construct(Shareholding $shareholding, $shareholderId = null) {

        $this->shareholding = $shareholding;
        $shareholdingId     = $shareholding->getId();

        parent::__construct();

        // --- do not save anything in db ---
        if (is_null($shareholderId)) {
            $this->shareholderId    = null;
            return;
        }

        // --- check that shareholderId is int ---
        if (!preg_match('/^\d+$/', $shareholderId)) {
            throw new Exception('shareholder id has to be integer');
        }

        $sql = "
			SELECT * FROM `".self::$tableName."`
			WHERE `annual_return_shareholder_id` = $shareholderId AND `annual_return_shareholding_id` = $shareholdingId";
        if (!$result = CHFiling::getDb()->fetchRow($sql)) {
            throw new Exception('Record doesn\'t exist');
        }

        // --- set instance variables ---
		$this->shareholderId = $result['annual_return_shareholder_id'];
        $this->setFields($result);
    }

    /**
     *
     * @param Shareholding $shareholding
     * @param int $shareholderId
     * @return AnnualReturnShareholder
     */
    public static function getAnnualReturnShareholder(Shareholding $shareholding, $shareholderId) {
        return new AnnualReturnShareholder($shareholding, $shareholderId);
    }

    /**
     *
     * @param Shareholding $shareholding
     * @return AnnualReturnShareholder
     */
    public static function getNewAnnualReturnShareholder(Shareholding $shareholding) {
        return new AnnualReturnShareholder($shareholding);
    }

    /**
     * Returns all Shareholders that belong to the specified shareholding, if 
     * none found, empty array is returned.
     * Index is shareholder id.
     *
     * @param Shareholding $shareholding 
     * @return array<AnnualReturnShareholder>
     */
    public static function getAnnualReturnShareholders(Shareholding $shareholding) {

        $shareholdingId = $shareholding->getId();

        $sql = "
			SELECT annual_return_shareholder_id FROM `".self::$tableName."`
			WHERE `annual_return_shareholding_id` = $shareholdingId";
        $result = CHFiling::getDb()->fetchAll($sql);

        if (empty($result)) {
            return array();
        }

        $shareholders = array();
        foreach($result as $id) {
            $shareholderId = $id['annual_return_shareholder_id'];
            $shareholders[$shareholderId] =
                new AnnualReturnShareholder($shareholding, $shareholderId);
        }

        return $shareholders;
    }

    /**
     *
     */
    public function remove() {
        CHFiling::getDb()->delete(self::$tableName, 'annual_return_shareholder_id = '.$this->shareholderId);
    }

    /**
     *
     * @return int
     */
    public function getId() {
        return $this->shareholderId;
    }

    /**
     *
     * @return boolean  returns true if this shareholder belongs to the 
     *                  shareholding synced from companies house
     */
    public function isSynced() {
        return $this->shareholding->isSynced();
    }

    /**
     * returns false if object is not ready for saving/sending xml, otherwise 
     * returns true
     *
     * @return boolean 
     */
    public function isComplete() {
        $complete = true;

        $fields = $this->getFields();

        if (!isset($fields['surname']) || empty($fields['surname'])) {
            $complete = false;
        }

        return $complete;
    }

    /**
     * saves objecte into database
     * 
     * @return void
     */
    public function save() {

        if (!$this->isComplete()) {
            throw new Exception('You need to set all compulsory fielsd');
        }

        // --- prepare data ---
        $data = $this->getFields();
        unset($data['care_of_name']);
        unset($data['po_box']);
        unset($data['secure_address_ind']);
        $data['annual_return_shareholding_id'] = $this->shareholding->getId();

        // --- save data ---
        if (isset($this->shareholderId) && !is_null($this->shareholderId)) {
            CHFiling::getDb()->update(self::$tableName, $data, 'annual_return_shareholder_id = '.$this->shareholderId);
        } else {
            CHFiling::getDb()->insert(self::$tableName, $data);
            $this->shareholderId = CHFiling::getDb()->lastInsertId();
        }
    }

    /**
     *
     * @return xml - xml string, part of the shareholder 
     */
    public function getXml() {
        $data = $this->getFields();

        /* encode values */
        foreach($data as $key => $val) {
            $data[$key] = htmlspecialchars($val, ENT_NOQUOTES);
        }

        /* the rest is other foreign country tag */
        $country = array("USA" => 1, "IRL" => 1, "DEU" => 1, "FRA" => 1,
            "ITA" => 1, "ESP" => 1, "PRT" => 1, "NLD" => 1, "POL" => 1,
            "BEL" => 1, "NOR" => 1, "SWE" => 1, "DNK" => 1, "AUS" => 1,
            "NZL" => 1, "CAN" => 1, "ZAF" => 1, "AUT" => 1, "HRV" => 1,
            "CYP" => 1, "CZE" => 1, "EST" => 1, "HUN" => 1, "GRC" => 1,
            "LTU" => 1, "GBR" => 1, "GB-ENG" => 1, "GB-WLS" => 1,
            "GB-SCT" => 1, "GB-NIR" => 1);

        $xml = '<Name>';
        $xml .= '<Surname>' . $data['surname'] . '</Surname>';
        if (isset($data['forename']) && !empty($data['forename'])) {
            $xml .= '<Forename>'. $data['forename'];
            
            if(isset($data['middle_name']) && !empty($data['middle_name'])){ 
                $xml .=' '.$data['middle_name'];
            };
            $xml .= '</Forename>';
        }
        $xml .= '</Name>';

        // MotHEr FucKERs Don'T wan'T ThE ADDreSs EvenTHough ScheMa SayS WE Can
        // PROvidE it - nutters
        $ar = CHAnnualReturn::getAnnualReturn($this->shareholding->getFormSubmissionId());
        if($ar->getTrading() == 1 &&  $ar->getDtr5() != 1){
            if (isset($data['isRemoved']) && $data['isRemoved'] == 0) {
                $xml .= '<Address>';
                $xml .= '<Premise>'.$data['premise'].'</Premise>';
                if (isset($data['street']) && !empty($data['street'])) {
                    $xml .= '<Street>'.$data['street'].'</Street>';
                }
                if (isset($data['thoroughfare']) && !empty($data['thoroughfare'])) {
                    $xml .= '<Thoroughfare>'.$data['thoroughfare'].'</Thoroughfare>';
                }
                $xml .= '<PostTown>'.$data['post_town'].'</PostTown>';
                if (isset($data['county']) && !empty($data['county'])) {
                    $xml .= '<County>'.$data['county'].'</County>';
                }

                if (isset($country[$data['country']])) {
                    $xml .= '<Country>'.$data['country'].'</Country>';
                } else {
                    $xml .= '<OtherForeignCountry>'.$data['country'].'</OtherForeignCountry>';
                }

                $xml .= '<Postcode>'.$data['postcode'].'</Postcode>';
                $xml .= '</Address>';
            }
        }

        return '<Shareholders>'.$xml.'</Shareholders>';
    }
}
