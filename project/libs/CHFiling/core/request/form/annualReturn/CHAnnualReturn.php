<?php

use CHFiling\Exceptions\CHExceptionFactory;
use DusanKasan\Knapsack\Collection;
use Entities\CompanyHouse\FormSubmission as FormSubmissionEntity;
use PeopleWithSignificantControl\Interfaces\IPscAware;
use Utils\Date;

/**
 * Description of CHAnnualReturn
 *
 * @author pete
 */
class CHAnnualReturn implements Form, IPscAware
{
    /**
     *
     * @var int
     */
    private $formSubmissionId;

    /**
     *
     * @var string
     */
    private $rejectDescription;

    /**
     *
     * @var Enum('PLC','BYSHR','BYGUAR','BYSHREXUNDSEC60','BYGUAREXUNDSEC60',
     *  'UNLCWSHRCAP','UNLCWOSHRCAP','LLP')
     */
    private $companyCategory;

    /**
     *
     * @var bool
     */
    private $trading;

    /**
     *
     * @var bool
     */
    private $dtr5;

    /**
     *
     * @var Date
     */
    private $madeUpDate;

    /**
     *
     * @var Enum ('code', 'text')
     */
    private $sicCodeType;

    /**
     *
     * @var String
     */
    private $sicCode;

    /**
     *
     * @var Address
     */
    private $regOffice;

    /**
     *
     * @var Address
     */
    private $sailAddress;

    /**
     * values are separated by ";"
     *
     * @var string
     */
    private $sailRecords;

    /**
     *
     * @var Enum ('NONE','FULL','NOCHANGE','FULLPLC')
     */
    private $memebersList;

    /**
     *
     * @var array
     */
    private $shares;

    /**
     *
     * @var array
     */
    private $officers;

    /**
     *
     * @var Shareholding[]
     */
    private $shareholdings;

    /**
     * @var string
     */
    private $noPscReason;

    /**
     *
     * @var string
     */
    private static $tableName = 'ch_annual_return';

    /**
     * @var bool
     */
    private $hasPreAddedPscs;

    /**
     *
     * @param int $formSubmissionId
     */
    private function  __construct($formSubmissionId)
    {
        // --- check that formSubmissionId is int ---
        if (!preg_match('/^\d+$/', $formSubmissionId)) {
            throw new Exception('formSubmissionId has to be integer');
        }

        // --- load data from database ---
        /* annual return */
        $sql = "
            SELECT * FROM ".self::$tableName."
            WHERE `form_submission_id` = $formSubmissionId";
        $result = CHFiling::getDb()->fetchRow($sql);
        if (!$result) {
            throw new Exception('annual return doesn\'t exist');
        }

        // TODO - move shares to CHAnnualReturnShares!!!

        /* annual return shares */
        $sql = "
            SELECT * FROM ch_annual_return_shares
            WHERE `form_submission_id` = $formSubmissionId";
        $shares = CHFiling::getDb()->fetchAll($sql);
        if (!$shares) {
            $result['shares'] = [];
        } else {
            $result['shares'] = $shares;
        }

        /* set instance variables */
        $this->formSubmissionId = $result['form_submission_id'];
        $this->rejectDescription= $result['reject_description'];

        /* set the rest */
        $this->setFields($result);
    }

    /**
     * set instance variables, used by constructor and sync
     * $result argument should be array in this format:
     *  <ul>
     *      <li>...
     *      <li>shares 0 - *
     *          <ul>
     *              <li>...</li>
     *          </ul>
     *      </li>
     *  </ul>
     *
     * @param array - data, db columns and their values
     * @return void
     */
    private function setFields($result)
    {
        $this->regOffice = new Address();
        $this->regOffice->setFields($result);

        $this->sailAddress = new Address();
        $this->sailAddress->setFields($result, 'sail_');

        $this->sailRecords = $result['sail_records'];

        $this->memebersList = isset($result['members_list'])
            ? $result['members_list'] : NULL;

        $this->companyCategory = $result['company_category'];
        $this->trading = isset($result['trading']) ? $result['trading'] : 0;
        $this->dtr5 = isset($result['dtr5']) ? $result['dtr5'] : 0;
        $this->madeUpDate = $result['made_up_date'];
        $this->sicCodeType = $result['sic_code_type'];
        $this->sicCode = $result['sic_code'];
        //todo: psc - should these two lines be here? they are called after sync, but it's not "syncable" properties
        $this->noPscReason = isset($result['no_psc_reason']) ? $result['no_psc_reason'] : NULL;
        $this->hasPreAddedPscs = isset($result['pscs_pre_added']) ? (bool) $result['pscs_pre_added'] : NULL;

        /* shares */ // TODO - same as shareholdings
            $this->shares = [];
        foreach ($result['shares'] as $i => $share) {
            if (isset($share['annual_return_shares_id'])) {
                $k = $share['annual_return_shares_id'];
                $this->shares[$k] = $share;
            } else {
                $this->shares[$i] = $share;
            }
        }

        /* shareholdings */
        $this->shareholdings = Shareholding::getShareholdings($this);

        // TODO - officers etc.
        $sql = "
            SELECT * FROM ch_annual_return_officer
            WHERE `form_submission_id` = $this->formSubmissionId";
        $officers = CHFiling::getDb()->fetchAll($sql);

        if (!$officers) {
            $this->officers = [];
        } else {
            $this->officers = $officers;
        }

    }

    /**
     *
     * @param int $formSubmissionId
     * @return CHAnnualReturn
     */
    public static function getAnnualReturn($formSubmissionId)
    {
        return new self($formSubmissionId);
    }

    /**
     *
     * @param int $formSubmissionId
     * @return AnnualReturn
     */
    public static function getNewAnnualReturn($formSubmissionId)
    {
        // --- check that formSubmissionId is int ---
        if (!preg_match('/^\d+$/', $formSubmissionId)) {
            throw new Exception('formSubmissionId has to be integer');
        }

        // --- new instance ---
        /* insert annual return */
        CHFiling::getDb()->insert(self::$tableName, ['form_submission_id' => $formSubmissionId]);

        $annualReturn = new self($formSubmissionId);
        //        $annualReturn->sync();
        return $annualReturn;

    }

    /**
     * Return submission status/response of this submission. If not finished yet
     * then null is returned
     *
     * @return string
     */
    public function getSubmissionStatus()
    {
        return CHFiling::getDb()->fetchOne('SELECT response FROM ch_form_submission WHERE form_submission_id = ' . $this->formSubmissionId);
    }
    /**
     *  check
     */
    public function checkAuthenticationCode()
    {

        /* get company */
        $sql = "
            SELECT `company_id` FROM `ch_form_submission`
            WHERE `form_submission_id` = $this->formSubmissionId";
        $companyId = CHFiling::getDb()->fetchOne($sql);
        $company = Company::getCompany($companyId);

        /* send request for company data - sync request */
        $getData = CompanyDataRequest::getNewCompanyDataRequest(
            $company->getCustomerId(),
            $company->getCompanyNumber(),
            $company->getAuthenticationCode()
        );
        $xml = $getData->sendRequest();
        $response = simplexml_load_string($xml);

        /* check if it's an error */
        if (isset($response->GovTalkDetails->GovTalkErrors)) {
            if (isset($response->GovTalkDetails->GovTalkErrors->Error->Text)
                && $response->GovTalkDetails->GovTalkErrors->Error->Text == 'Invalid CompanyAuthenticationCode'
            ) {
                throw new Exception(
                    "<p>Invalid CompanyAuthenticationCode</p><p>You need a valid authentication code to: file annual returns, update the
                        registered office and assign, resign &amp; edit officer details. To get a new code please:</p>
                        <ol>
                        <li>Register for <a href='https://ewf.companieshouse.gov.uk/seclogin'>Web Filing</a></li>
                        <li>Sign in to <a href='https://ewf.companieshouse.gov.uk/seclogin'>Web Filing</a></li>
                        <li>Click 'Forgotten your authentication code'</li>
                        <li>Request it to be resent.</li>
                        </ol>
                        <p>Once you've received it please email us at <a href='mailto:theteam@madesimplegroup.com'>theteam@madesimplegroup.com</a> and we'll update it for you!</p>"
                );
            }
        }
    }

    /**
     * Loads this annual return with data from comapnies house sync
     */
    public function sync()
    {
        if (Feature::featurePsc()) {
            return $this->syncPsc();
        }

        /* get company */
        $sql = "
            SELECT `company_id` FROM `ch_form_submission`
            WHERE `form_submission_id` = $this->formSubmissionId";
        $companyId = CHFiling::getDb()->fetchOne($sql);
        $company = Company::getCompany($companyId);

        /* send request for company data - sync request */
        $getData = CompanyDataRequest::getNewCompanyDataRequest(
            $company->getCustomerId(),
            $company->getCompanyNumber(),
            $company->getAuthenticationCode()
        );
        $xml = $getData->sendRequest();
        $response = simplexml_load_string($xml);

        /* check if it's an error */
        if (isset($response->GovTalkDetails->GovTalkErrors)) {
            throw CHExceptionFactory::createChExceptionFromXmlGwError($response->GovTalkDetails->GovTalkErrors->Error);
        }

        //if (isset($response->Body, $response->Body->CompanyData, $response->Body->CompanyData->CompanyCategory)) {
        //if (empty($response->Body->CompanyData->StatementOfCapital->Capital)) {
        //if ((string) $response->Body->CompanyData->CompanyCategory == 'BYSHR'|| (string) $response->Body->CompanyData->CompanyCategory == 'PLC') {
        //foreach ($response->Body->CompanyData->StatementOfCapital->Capital as $dataN)
        //{
        //$dataN->addChild('TotalNumberOfIssuedShares', '0');
        //$dataN->addChild('ShareCurrency', '');
        //$dataN->addChild('TotalAggregateNominalValue', '0');
        //$dataN->addChild('Shares');
        //$dataN->Shares->addChild('ShareClas', '');
        //$dataN->Shares->addChild('NumShares', '1');
        //$dataN->Shares->addChild('AmountPaidDuePerShare', '0');
        //$dataN->Shares->addChild('AmountUnpaidPerShare', '0');
        //$dataN->Shares->addChild('AggregateNominalValue', '0');
        //}
        //}
        //}
        //}

        /*
         * compnay data/sync DOES NOT return
         * <b>TradingOnRegulatedMarkets</b> (this is only for PLC),
         * <b>TypeOfMembersList</b> - this is set full or in case of PLC
         * fullplc and <b>SubsidaryAssociatedUndertakings</b>.
         */

        $data = [];
        if (!isset($response->Body->CompanyData)) {
            throw new Exception('Error when geting Company Data');
        }

        $xmlData = $response->Body->CompanyData;
        //echo $xmlData->asXml();exit;

        //echo $xmlData->asXml();exit;

        $data['company_category'] = (string) $xmlData->CompanyCategory;
        $data['members_list'] = 'FULL';

        $madeUpDate = (string) $xmlData->NextDueDate;
        $now = date("Y-m-d");
        $data['made_up_date'] = (strtotime($now) < strtotime($madeUpDate))
            ? $now
            : $madeUpDate;
        //$data['made_up_date'] = date("Y-m-d");

        $address = [
            'Premise'   => 'premise',
            'Street'    => 'street',
            'Thoroughfare'=> 'thoroughfare',
            'PostTown'  => 'post_town',
            'County'    => 'county',
            'Country'   => 'country',
            //'OtherForeignCountry' => 'country', // dickheads - that says it all. Companies house sends country sometimes in Country tag sometimes in OtherForeignCountry
            'Postcode'  => 'postcode',
            'CareOfName'=> 'care_of_name',
            'PoBox'     => 'po_box'
        ];

        /* registered office address */
        foreach ($address as $k => $v) {
            $data[$v] = (isset($xmlData->RegisteredOfficeAddress->{$k}) &&
                !empty($xmlData->RegisteredOfficeAddress->{$k}))
                ? (string) $xmlData->RegisteredOfficeAddress->{$k}
                : NULL;
            /* fuckers send UNDEF for country so need to change it to GBR */
            if ($k == 'Country' && (string) $xmlData->RegisteredOfficeAddress->{$k} == 'UNDEF') {
                $data[$v] = 'GBR';
            }
        }

        /* sail address */
        if (isset($xmlData->SailAddress[0]) && !empty($xmlData->SailAddress[0])) {
            foreach ($address as $k => $v) {
                $data['sail_'.$v] = (isset($xmlData->SailAddress->{$k}) &&
                    !empty($xmlData->SailAddress->{$k}))
                    ? (string) $xmlData->SailAddress->{$k}
                    : NULL;
            }
            /* set to null */
        } else {
            foreach ($address as $k => $v) {
                $data['sail_'.$v] = NULL;
            }
        }

        /* sic codes */
        if (isset($xmlData->SICCodes->SICCode)) {
            $data['sic_code_type'] = "code";
            /* get all siccodes */
            $codes = [];
            foreach ($xmlData->SICCodes->SICCode as $code) {
                $code = str_replace("None Supplied", "", $code);
                $codes[] = $code;
            }
            /* separate them using ";" */
            $data['sic_code'] = implode(";", $codes);
        } else {
            if (isset($xmlData->SICCodes->SICText)) {
                $data['sic_code_type'] = "text";
                $data['sic_code'] = (string) $xmlData->SICCodes->SICText;
            } else {
                /* nothing from ch */
                $data['sic_code_type'] = "code";
                $data['sic_code'] = NULL;
            }
        }

        /* sail records */
        if (isset($xmlData->SailRecords)) {
            $codes = [];
            foreach ($xmlData->SailRecords as $record) {
                $codes[] = (string) $record->RecordType;
            }
            $data['sail_records'] = implode(";", $codes);
        } else {
            $data['sail_records'] = NULL;
        }

        /* officers */
        $officers = [];
        /* members */
        if (isset($xmlData->Officers->Member) && !empty($xmlData->Officers->Member)) {
            $mems = $xmlData->Officers->Member;
            foreach ($mems as $v) {
                $x = count($officers);
                $officers[$x]['form_submission_id'] = $this->formSubmissionId;
                $officers[$x]['type'] = "MEM";
                $officers[$x]['designated_ind'] = isset($v->DesignatedInd) ? (string) $v->DesignatedInd : 0;
                /* person */
                if (isset($v->Person)) {

                    $officers[$x]['corporate']  = 0;
                    $officers[$x]['title'] = isset($v->Person->Title) ? (string) $v->Person->Title : NULL;
                    /* forename and middle name */
                    if (isset($v->Person->Forename)) {
                        $name = [];
                        foreach ($v->Person->Forename as $n) {
                            $name[] = (string) $n;
                        }
                        $officers[$x]['forename'] = $name[0];
                        unset($name[0]);
                        if (!empty($name)) {
                            $officers[$x]['middle_name'] = implode(" ", $name);
                        }
                    }
                    $officers[$x]['surname'] = (string) $v->Person->Surname;

                    /* same as registered office */
                    if (isset($v->Person->ServiceAddress->SameAsRegisteredOffice)) {
                        $officers[$x]['same_as_reg_office'] = 1;
                        /* address */
                    } else {
                        foreach ($address as $xmlField => $dbField) {
                            $officers[$x][$dbField] = (isset($v->Person->ServiceAddress->Address->{$xmlField}) &&
                                !empty($v->Person->ServiceAddress->Address->{$xmlField}))
                                ? (string) $v->Person->ServiceAddress->Address->{$xmlField}
                                : NULL;
                        }
                        /* other foreign country */
                        if (isset($v->Person->ServiceAddress->Address->OtherForeignCountry)
                            && !empty($v->Person->ServiceAddress->Address->OtherForeignCountry)
                        ) {
                            $officers[$x]['country'] = (string) $v->Person->ServiceAddress->Address->OtherForeignCountry;
                        }
                        /* fuckers send null for country so need to record it */
                        if ($officers[$x]['country'] === NULL) {
                            $officers[$x]['country_not_synced'] = 1;
                        }
                    }

                    $officers[$x]['dob']            = (string) $v->Person->DOB;
                    $officers[$x]['country_of_residence'] = (string) $v->Person->CountryOfResidence;
                    /* fuckers send null for country of residence as well, so need to record it */
                    if (!isset($officers[$x]['country_of_residence']) || empty($officers[$x]['country_of_residence'])) {
                        $officers[$x]['country_of_residence_not_synced'] = 1;
                    }
                    if (isset($v->Person->PreviousNames)) {
                        $previousNames = [];
                        foreach ($v->Person->PreviousNames as $n) {
                            $previousNames[] = (string) $n;
                        }
                        $officers[$x]['previous_names'] = implode(";", $previousNames);
                    }
                    /* corporate */
                } else {
                    $officers[$x]['corporate']  = 1;
                    $officers[$x]['corporate_name'] = (string) $v->Corporate->CorporateName;
                    /* Address */
                    foreach ($address as $xmlField => $dbField) {
                        $officers[$x][$dbField] = (isset($v->Corporate->Address->{$xmlField}) &&
                            !empty($v->Corporate->Address->{$xmlField}))
                            ? (string) $v->Corporate->Address->{$xmlField}
                            : NULL;
                    }
                    /* other foreign country */
                    if (isset($v->Corporate->Address->OtherForeignCountry)
                        && !empty($v->Corporate->Address->OtherForeignCountry)
                    ) {
                        $officers[$x]['country'] = (string) $v->Corporate->Address->OtherForeignCountry;
                    }
                    /* fuckers send null for country so need to record it */
                    if ($officers[$x]['country'] === NULL) {
                        $officers[$x]['country_not_synced'] = 1;
                    }

                    /* Identification */
                    /* NonEEA */
                    // MOthER FuCKers rETurN EmpTY FiELDs FOr IdeNTIFiCatioN
                    $identification = ['PlaceRegistered' => 'place_registered',
                        'RegistrationNumber' => 'registration_number',
                        'LawGoverned' => 'law_governed',
                        'LegalForm' => 'legal_form'];
                    if (isset($v->Corporate->CompanyIdentification->NonEEA)) {
                        $officers[$x]['identification_type']  = 'NonEEA';
                        foreach ($identification as $xmlField => $dbField) {
                            $officers[$x][$dbField]  = (isset($v->Corporate->CompanyIdentification->NonEEA->{$xmlField}) &&
                                !empty($v->Corporate->CompanyIdentification->NonEEA->{$xmlField}))
                                ? (string) $v->Corporate->CompanyIdentification->NonEEA->{$xmlField}
                                : NULL;
                        }
                        /* eea not received from sync */
                        if ((!isset($v->Corporate->CompanyIdentification->NonEEA->LawGoverned)
                                || empty($v->Corporate->CompanyIdentification->NonEEA->LawGoverned))
                            || (!isset($v->Corporate->CompanyIdentification->NonEEA->LegalForm)
                                || empty($v->Corporate->CompanyIdentification->NonEEA->LegalForm))
                        ) {
                            $officers[$x]['eea_not_synced'] = 1;
                            $officer['eea_complete'] = 0;
                        }
                    } else {
                        $officers[$x]['identification_type']  = 'EEA';
                        foreach ($identification as $xmlField => $dbField) {
                            $officers[$x][$dbField]  = (isset($v->Corporate->CompanyIdentification->EEA->{$xmlField}) &&
                                !empty($v->Corporate->CompanyIdentification->EEA->{$xmlField}))
                                ? (string) $v->Corporate->CompanyIdentification->EEA->{$xmlField}
                                : NULL;
                        }
                        /* eea not received from sync */
                        if ((!isset($v->Corporate->CompanyIdentification->EEA->PlaceRegistered)
                                || empty($v->Corporate->CompanyIdentification->EEA->PlaceRegistered))
                            || (!isset($v->Corporate->CompanyIdentification->EEA->RegistrationNumber)
                                || empty($v->Corporate->CompanyIdentification->EEA->RegistrationNumber))
                        ) {
                            $officers[$x]['eea_not_synced'] = 1;
                            $officer['eea_complete'] = 0;
                        }
                    } // end of identification
                } // end of corporate
            }
        }

        if (isset($xmlData->Officers->Director) && !empty($xmlData->Officers->Director)) {
            /* directors */
            $dirs = $xmlData->Officers->Director;

            foreach ($dirs as $v) {
                $x = count($officers);
                $officers[$x]['form_submission_id'] = $this->formSubmissionId;
                $officers[$x]['type'] = "DIR";
                /* person */
                if (isset($v->Person)) {

                    $officers[$x]['corporate']  = 0;
                    $officers[$x]['title'] = isset($v->Person->Title) ? (string) $v->Person->Title : NULL;
                    /* forename and middle name */
                    if (isset($v->Person->Forename)) {
                        $name = [];
                        foreach ($v->Person->Forename as $n) {
                            $name[] = (string) $n;
                        }
                        $officers[$x]['forename'] = $name[0];
                        unset($name[0]);
                        if (!empty($name)) {
                            $officers[$x]['middle_name'] = implode(" ", $name);
                        }
                    }
                    $officers[$x]['surname'] = (string) $v->Person->Surname;

                    /* same as registered office */
                    if (isset($v->Person->ServiceAddress->SameAsRegisteredOffice)) {
                        $officers[$x]['same_as_reg_office'] = 1;
                        /* address */
                    } else {
                        foreach ($address as $xmlField => $dbField) {
                            $officers[$x][$dbField] = (isset($v->Person->ServiceAddress->Address->{$xmlField}) &&
                                !empty($v->Person->ServiceAddress->Address->{$xmlField}))
                                ? (string) $v->Person->ServiceAddress->Address->{$xmlField}
                                : NULL;
                        }
                        /* other foreign country */
                        if (isset($v->Person->ServiceAddress->Address->OtherForeignCountry)
                            && !empty($v->Person->ServiceAddress->Address->OtherForeignCountry)
                        ) {
                            $officers[$x]['country'] = (string) $v->Person->ServiceAddress->Address->OtherForeignCountry;
                        }
                        /* fuckers send null for country so need to record it */
                        if ($officers[$x]['country'] === NULL) {
                            $officers[$x]['country_not_synced'] = 1;
                        }
                    }

                    $officers[$x]['dob']            = (string) $v->Person->DOB;
                    $officers[$x]['nationality']    = (string) $v->Person->Nationality;
                    $officers[$x]['occupation']     = (string) $v->Person->Occupation;
                    $officers[$x]['country_of_residence'] = (string) $v->Person->CountryOfResidence;
                    /* fuckers send null for country of residence as well, so need to record it */
                    if (!isset($officers[$x]['country_of_residence']) || empty($officers[$x]['country_of_residence'])) {
                        $officers[$x]['country_of_residence_not_synced'] = 1;
                    }
                    if (isset($v->Person->PreviousNames)) {
                        $previousNames = [];
                        foreach ($v->Person->PreviousNames as $n) {
                            $previousNames[] = (string) $n;
                        }
                        $officers[$x]['previous_names'] = implode(";", $previousNames);
                    }
                    /* corporate */
                } else {
                    $officers[$x]['corporate']  = 1;
                    $officers[$x]['corporate_name'] = (string) $v->Corporate->CorporateName;
                    /* Address */
                    foreach ($address as $xmlField => $dbField) {
                        $officers[$x][$dbField] = (isset($v->Corporate->Address->{$xmlField}) &&
                            !empty($v->Corporate->Address->{$xmlField}))
                            ? (string) $v->Corporate->Address->{$xmlField}
                            : NULL;
                    }
                    /* other foreign country */
                    if (isset($v->Corporate->Address->OtherForeignCountry)
                        && !empty($v->Corporate->Address->OtherForeignCountry)
                    ) {
                        $officers[$x]['country'] = (string) $v->Corporate->Address->OtherForeignCountry;
                    }
                    /* fuckers send null for country so need to record it */
                    if ($officers[$x]['country'] === NULL) {
                        $officers[$x]['country_not_synced'] = 1;
                    }

                    /* Identification */
                    /* NonEEA */
                    // MOthER FuCKers rETurN EmpTY FiELDs FOr IdeNTIFiCatioN
                    $identification = ['PlaceRegistered' => 'place_registered',
                        'RegistrationNumber' => 'registration_number',
                        'LawGoverned' => 'law_governed',
                        'LegalForm' => 'legal_form'];
                    if (isset($v->Corporate->CompanyIdentification->NonEEA)) {
                        $officers[$x]['identification_type']  = 'NonEEA';
                        foreach ($identification as $xmlField => $dbField) {
                            $officers[$x][$dbField]  = (isset($v->Corporate->CompanyIdentification->NonEEA->{$xmlField}) &&
                                !empty($v->Corporate->CompanyIdentification->NonEEA->{$xmlField}))
                                ? (string) $v->Corporate->CompanyIdentification->NonEEA->{$xmlField}
                                : NULL;
                        }
                        /* eea not received from sync */
                        if ((!isset($v->Corporate->CompanyIdentification->NonEEA->LawGoverned)
                                || empty($v->Corporate->CompanyIdentification->NonEEA->LawGoverned))
                            || (!isset($v->Corporate->CompanyIdentification->NonEEA->LegalForm)
                                || empty($v->Corporate->CompanyIdentification->NonEEA->LegalForm))
                        ) {
                            $officers[$x]['eea_not_synced'] = 1;
                            $officer['eea_complete'] = 0;
                        }
                    } else {
                        $officers[$x]['identification_type']  = 'EEA';
                        foreach ($identification as $xmlField => $dbField) {
                            $officers[$x][$dbField]  = (isset($v->Corporate->CompanyIdentification->EEA->{$xmlField}) &&
                                !empty($v->Corporate->CompanyIdentification->EEA->{$xmlField}))
                                ? (string) $v->Corporate->CompanyIdentification->EEA->{$xmlField}
                                : NULL;
                        }
                        /* eea not received from sync */
                        if ((!isset($v->Corporate->CompanyIdentification->EEA->PlaceRegistered)
                                || empty($v->Corporate->CompanyIdentification->EEA->PlaceRegistered))
                            || (!isset($v->Corporate->CompanyIdentification->EEA->RegistrationNumber)
                                || empty($v->Corporate->CompanyIdentification->EEA->RegistrationNumber))
                        ) {
                            $officers[$x]['eea_not_synced'] = 1;
                            $officer['eea_complete'] = 0;
                        }
                    } // end of identification
                } // end of corporate
            } // end of director foreach
        }
        /* secretaries */
        if (isset($xmlData->Officers->Secretary) && !empty($xmlData->Officers->Secretary)) {
            $secs = $xmlData->Officers->Secretary;

            foreach ($secs as $v) {
                $x = count($officers);
                $officers[$x]['form_submission_id'] = $this->formSubmissionId;
                $officers[$x]['type'] = "SEC";
                /* person */
                if (isset($v->Person)) {

                    $officers[$x]['corporate']  = 0;
                    $officers[$x]['title'] = isset($v->Person->Title) ? (string) $v->Person->Title : NULL;
                    /* forename and middle name */
                    if (isset($v->Person->Forename)) {
                        $name = [];
                        foreach ($v->Person->Forename as $n) {
                            $name[] = (string) $n;
                        }
                        $officers[$x]['forename'] = $name[0];
                        unset($name[0]);
                        if (!empty($name)) {
                            $officers[$x]['middle_name'] = implode(" ", $name);
                        }
                    }
                    $officers[$x]['surname'] = (string) $v->Person->Surname;

                    /* same as registered office */
                    if (isset($v->Person->ServiceAddress->SameAsRegisteredOffice)) {
                        $officers[$x]['same_as_reg_office'] = 1;
                        /* address */
                    } else {
                        foreach ($address as $xmlField => $dbField) {
                            $officers[$x][$dbField] = (isset($v->Person->ServiceAddress->Address->{$xmlField}) &&
                                !empty($v->Person->ServiceAddress->Address->{$xmlField}))
                                ? (string) $v->Person->ServiceAddress->Address->{$xmlField}
                                : NULL;
                        }
                        /* other foreign country */
                        if (isset($v->Person->ServiceAddress->Address->OtherForeignCountry)
                            && !empty($v->Person->ServiceAddress->Address->OtherForeignCountry)
                        ) {
                            $officers[$x]['country'] = (string) $v->Person->ServiceAddress->Address->OtherForeignCountry;
                        }
                        /* fuckers send null for country so need to record it */
                        if ($officers[$x]['country'] === NULL) {
                            $officers[$x]['country_not_synced'] = 1;
                        }
                    }

                    if (isset($v->Person->PreviousNames)) {
                        $previousNames = [];
                        foreach ($v->Person->PreviousNames as $n) {
                            $previousNames[] = (string) $n;
                        }
                        $officers[$x]['previous_names'] = implode(";", $previousNames);
                    }
                    /* corporate */
                } else {
                    $officers[$x]['corporate']  = 1;
                    $officers[$x]['corporate_name'] = (string) $v->Corporate->CorporateName;
                    /* Address */
                    foreach ($address as $xmlField => $dbField) {
                        $officers[$x][$dbField] = (isset($v->Corporate->Address->{$xmlField}) &&
                            !empty($v->Corporate->Address->{$xmlField}))
                            ? (string) $v->Corporate->Address->{$xmlField}
                            : NULL;
                    }
                    /* other foreign country */
                    if (isset($v->Corporate->Address->OtherForeignCountry)
                        && !empty($v->Corporate->Address->OtherForeignCountry)
                    ) {
                        $officers[$x]['country'] = (string) $v->Corporate->Address->OtherForeignCountry;
                    }
                    /* fuckers send null for country so need to record it */
                    if ($officers[$x]['country'] === NULL) {
                        $officers[$x]['country_not_synced'] = 1;
                    }
                    /* Identification */
                    /* NonEEA */
                    // MOthER FuCKers rETurN EmpTY FiELDs FOr IdeNTIFiCatioN
                    $identification = ['PlaceRegistered' => 'place_registered',
                        'RegistrationNumber' => 'registration_number',
                        'LawGoverned' => 'law_governed',
                        'LegalForm' => 'legal_form'];
                    if (isset($v->Corporate->CompanyIdentification->NonEEA)) {
                        $officers[$x]['identification_type']  = 'NonEEA';
                        foreach ($identification as $xmlField => $dbField) {
                            $officers[$x][$dbField]  = (isset($v->Corporate->CompanyIdentification->NonEEA->{$xmlField}) &&
                                !empty($v->Corporate->CompanyIdentification->NonEEA->{$xmlField}))
                                ? (string) $v->Corporate->CompanyIdentification->NonEEA->{$xmlField}
                                : NULL;
                        }
                        /* eea not received from sync */
                        if ((!isset($v->Corporate->CompanyIdentification->NonEEA->LawGoverned)
                                || empty($v->Corporate->CompanyIdentification->NonEEA->LawGoverned))
                            || (!isset($v->Corporate->CompanyIdentification->NonEEA->LegalForm)
                                || empty($v->Corporate->CompanyIdentification->NonEEA->LegalForm))
                        ) {
                            $officers[$x]['eea_not_synced'] = 1;
                            $officer['eea_complete'] = 0;
                        }
                    } else {
                        $officers[$x]['identification_type']  = 'EEA';
                        foreach ($identification as $xmlField => $dbField) {
                            $officers[$x][$dbField]  = (isset($v->Corporate->CompanyIdentification->EEA->{$xmlField}) &&
                                !empty($v->Corporate->CompanyIdentification->EEA->{$xmlField}))
                                ? (string) $v->Corporate->CompanyIdentification->EEA->{$xmlField}
                                : NULL;
                        }
                        /* eea not received from sync */
                        if ((!isset($v->Corporate->CompanyIdentification->EEA->PlaceRegistered)
                                || empty($v->Corporate->CompanyIdentification->EEA->PlaceRegistered))
                            || (!isset($v->Corporate->CompanyIdentification->EEA->RegistrationNumber)
                                || empty($v->Corporate->CompanyIdentification->EEA->RegistrationNumber))
                        ) {
                            $officers[$x]['eea_not_synced'] = 1;
                            $officer['eea_complete'] = 0;
                        }
                    } // end of identification
                } // end of corporate
            } // end of secretary foreach
        } else {
            $secs = [];
        }

        /* update officers db */
        CHFiling::getDb()->delete("ch_annual_return_officer", 'form_submission_id = '.$this->formSubmissionId);
        foreach ($officers as $officer) {
            CHFiling::getDb()->insert("ch_annual_return_officer", $officer);
        }

        // no need for members - won't use them for annual return

        /* these values are not in sync, so null them */
        $data['regulated_markets'] = NULL;
        if ($data['company_category'] == 'BYGUAR' || $data['company_category'] == 'BYGUAREXUNDSEC60') {
            $data['members_list']      = 'NONE';
        } else {
            $data['members_list']      = 'FULL';
        }

        /* update annual return db */
        CHFiling::getDb()->update(self::$tableName, $data, 'form_submission_id = '.$this->formSubmissionId);

        /* statement of capital */
        $shares = [];
        $x = 0;
        if (isset($xmlData->StatementOfCapital)) {
            foreach ($xmlData->StatementOfCapital->Capital as $capital) {
                $currency = (string) $capital->ShareCurrency;
                foreach ($capital->Shares as $share) {
                    $shares[$x]['share_class'] = (string) $share->ShareClass;
                    // fucking companies house does not return this, even thogh they have to
                    if (isset($share->PrescribedParticulars)) {
                        $shares[$x]['prescribed_particulars'] = (string) $share->PrescribedParticulars;
                    } else {
                        $shares[$x]['prescribed_particulars'] = 'Full voting rights.';
                    }

                    $shares[$x]['aggregate_nom_value']  = (string) $share->AggregateNominalValue;
                    $shares[$x]['num_shares']           = (string) $share->NumShares;
                    $shares[$x]['currency']             = $currency;
                    $shares[$x]['amount_paid']          = (double) $share->AmountPaidDuePerShare;
                    $shares[$x]['amount_unpaid']        = (double) $share->AmountUnpaidPerShare;

                    $shareValue = $shares[$x]['aggregate_nom_value'] / $shares[$x]['num_shares'];

                    /* partialy paid */
                    if ($shares[$x]['amount_paid'] != 0 && $shares[$x]['amount_unpaid'] != 0) {
                        $shares[$x]['sync_paid'] = $shares[$x]['amount_paid'];
                        $shares[$x]['paid']      = $shares[$x]['amount_paid'];
                        /* unpaid */
                    } elseif ($shares[$x]['amount_paid'] == 0 && $shares[$x]['amount_unpaid'] != 0) {
                        $shares[$x]['sync_paid'] = 'unpaid';
                        $shares[$x]['paid']      = 'unpaid';
                        /* paid */
                    } elseif ($shares[$x]['amount_paid'] != 0 && $shares[$x]['amount_unpaid'] == 0) {
                        $shares[$x]['sync_paid'] = 'paid';
                        $shares[$x]['paid']      = 'paid';
                        /* both values 0 */
                    } else {
                        $shares[$x]['sync_paid'] = 'unpaid';
                        $shares[$x]['paid']      = 'unpaid';
                        $shares[$x]['amount_unpaid'] = $shareValue;
                        $shares[$x]['amount_paid'] = 0;
                    }

                    /* we make it all paid, customer can change it later */
                    /*
                    $shareValue = (int) $share->AmountPaidDuePerShare + (int) $share->AmountUnpaidPerShare;
                    $shares[$x]['num_shares'] = (string) $share->NumShares;
                    $shares[$x]['currency'] = $currency;
                    $shares[$x]['aggregate_nom_value'] = (string) $share->AggregateNominalValue;
                     */
                    /* sometimes mf sends amount paid and unpaid 0 */
                    /*
                    if ($shareValue > 0) {
                        $shares[$x]['amount_paid'] = $shareValue;
                    } else {
                        $shares[$x]['amount_paid'] = $shares[$x]['aggregate_nom_value'] / $shares[$x]['num_shares'];
                    }
                    $shares[$x]['amount_unpaid'] = null;
                     */
                    $x++;
                }
            }
        }

        /* update statement of capital db */
        CHFiling::getDb()->delete("ch_annual_return_shares", 'form_submission_id = '.$this->formSubmissionId);
        foreach ($shares as $share) {
            $share['form_submission_id'] = $this->formSubmissionId;
            CHFiling::getDb()->insert("ch_annual_return_shares", $share);
        }

        /* shareholdings */
        $i = 0;
        $data['shareholding'] = [];
        if (isset($xmlData->Shareholdings)) {
            /* remove old records */
            foreach ($this->shareholdings as $k => $v) {
                /* shareholoders and transfers */
                CHFiling::getDb()->delete("ch_annual_return_shareholder", "annual_return_shareholding_id = ".$k);
                CHFiling::getDb()->delete("ch_annual_return_transfer", "annual_return_shareholding_id = ".$k);
            }
            /* shareholdings */
            CHFiling::getDb()->delete("ch_annual_return_shareholding", 'form_submission_id = '.$this->formSubmissionId);
            foreach ($xmlData->Shareholdings as $v) {
                $shareholding = [];
                $shareholding['share_class'] = (string) $v->ShareClass;
                $shareholding['number_held'] = (string) $v->NumberHeld;
                $shareholding['form_submission_id'] = $this->formSubmissionId;
                $shareholding['sync'] = 1; // indicate that this is from sync
                /* insert new record */
                CHFiling::getDb()->insert("ch_annual_return_shareholding", $shareholding);
                $shareholdingId = CHFiling::getDb()->lastInsertId();

                $shareholding['transfer'] = [];
                $shareholding['shareholder'] = [];

                /* shareholders */
                $x = 0;
                foreach ($v->Shareholders as $sh) {
                    $shareholder= [];
                    $forename   = [];
                    $shareholder['surname'] = (string) $sh->Name->Surname;
                    foreach ($sh->Name->Forename as $fn) {
                        $forename[] = $fn;
                    }
                    if (!empty($forename)) {
                        $shareholder['forename'] = $forename[0];
                        unset($forename[0]);
                        if (!empty($forename[0])) {
                            $shareholder['middle_name'] = implode(" ", $forename);
                        }
                    }

                    /* address */
                    if (isset($sh->Address)) {
                        foreach ($address as $k => $v) {
                            if ($v != 'care_of_name' && $v != 'po_box') {
                                $shareholder[$v] = (isset($sh->Address->{$k}) &&
                                    !empty($sh->Address->{$k}))
                                    ? (string) $sh->Address->{$k}
                                    : NULL;
                            }
                        }
                    }
                    $shareholder['annual_return_shareholding_id'] = $shareholdingId;

                    /* insert new shareholder from sync */
                    $shareholding['shareholder'][$x++] = $shareholder;
                    CHFiling::getDb()->insert("ch_annual_return_shareholder", $shareholder);
                }
            }
        }

        /* update instance variables */
        $data['shares'] = $shares; // TODO - delete this when shares are separate
        $this->setFields($data);

    }

    public function syncPsc()
    {
        /* get company */
        $sql = "
            SELECT `company_id` FROM `ch_form_submission`
            WHERE `form_submission_id` = $this->formSubmissionId";
        $companyId = CHFiling::getDb()->fetchOne($sql);
        $company = Company::getCompany($companyId);

        /* send request for company data - sync request */
        $getData = CompanyDataRequest::getNewCompanyDataRequest(
            $company->getCustomerId(),
            $company->getCompanyNumber(),
            $company->getAuthenticationCode()
        );
        $xml = $getData->sendRequest();

        $response = simplexml_load_string($xml);

        if (isset($response->GovTalkDetails->GovTalkErrors)) {
            throw CHExceptionFactory::createChExceptionFromXmlGwError($response->GovTalkDetails->GovTalkErrors->Error);
        }

        $data = [];
        if (!isset($response->Body->CompanyData)) {
            throw new Exception('Error when geting Company Data');
        }

        $xmlData = $response->Body->CompanyData;

        $data['company_category'] = (string) $xmlData->CompanyCategory;
        $data['members_list'] = 'FULL';

        $madeUpDate = (string) $xmlData->NextDueDate;
        $now = date("Y-m-d");
        $data['made_up_date'] = (strtotime($now) < strtotime($madeUpDate))
            ? $now
            : $madeUpDate;
        //$data['made_up_date'] = date("Y-m-d");

        $address = [
            'Premise'   => 'premise',
            'Street'    => 'street',
            'Thoroughfare'=> 'thoroughfare',
            'PostTown'  => 'post_town',
            'County'    => 'county',
            'Country'   => 'country',
            //'OtherForeignCountry' => 'country', // dickheads - that says it all. Companies house sends country sometimes in Country tag sometimes in OtherForeignCountry
            'Postcode'  => 'postcode',
            'CareOfName'=> 'care_of_name',
            'PoBox'     => 'po_box'
        ];

        /* registered office address */
        foreach ($address as $k => $v) {
            $data[$v] = (isset($xmlData->RegisteredOfficeAddress->{$k}) &&
                !empty($xmlData->RegisteredOfficeAddress->{$k}))
                ? (string) $xmlData->RegisteredOfficeAddress->{$k}
                : NULL;
            /* fuckers send UNDEF for country so need to change it to GBR */
            if ($k == 'Country' && (string) $xmlData->RegisteredOfficeAddress->{$k} == 'UNDEF') {
                $data[$v] = 'GBR';
            }
        }

        /* sail address */
        if (isset($xmlData->SailAddress[0]) && !empty($xmlData->SailAddress[0])) {
            foreach ($address as $k => $v) {
                $data['sail_'.$v] = (isset($xmlData->SailAddress->{$k}) &&
                    !empty($xmlData->SailAddress->{$k}))
                    ? (string) $xmlData->SailAddress->{$k}
                    : NULL;
            }
            /* set to null */
        } else {
            foreach ($address as $k => $v) {
                $data['sail_'.$v] = NULL;
            }
        }

        /* sic codes */
        if (isset($xmlData->SICCodes->SICCode)) {
            $data['sic_code_type'] = "code";
            /* get all siccodes */
            $codes = [];
            foreach ($xmlData->SICCodes->SICCode as $code) {
                $code = str_replace("None Supplied", "", $code);
                $codes[] = $code;
            }
            /* separate them using ";" */
            $data['sic_code'] = implode(";", $codes);
        } else {
            if (isset($xmlData->SICCodes->SICText)) {
                $data['sic_code_type'] = "text";
                $data['sic_code'] = (string) $xmlData->SICCodes->SICText;
            } else {
                /* nothing from ch */
                $data['sic_code_type'] = "code";
                $data['sic_code'] = NULL;
            }
        }

        /* sail records */
        if (isset($xmlData->SailRecords)) {
            $codes = [];
            foreach ($xmlData->SailRecords as $record) {
                $codes[] = (string) $record->RecordType;
            }
            $data['sail_records'] = implode(";", $codes);
        } else {
            $data['sail_records'] = NULL;
        }

        /* officers */
        $officers = [];
        /* members */
        if (isset($xmlData->Officers->Member) && !empty($xmlData->Officers->Member)) {
            $mems = $xmlData->Officers->Member;
            foreach ($mems as $v) {
                $x = count($officers);
                $officers[$x]['form_submission_id'] = $this->formSubmissionId;
                $officers[$x]['type'] = "MEM";
                $officers[$x]['designated_ind'] = isset($v->DesignatedInd) ? (string) $v->DesignatedInd : 0;
                /* person */
                if (isset($v->Person)) {

                    $officers[$x]['corporate']  = 0;
                    $officers[$x]['title'] = isset($v->Person->Title) ? (string) $v->Person->Title : NULL;
                    /* forename and middle name */
                    if (isset($v->Person->Forename)) {
                        $name = [];
                        foreach ($v->Person->Forename as $n) {
                            $name[] = (string) $n;
                        }
                        $officers[$x]['forename'] = $name[0];
                        unset($name[0]);
                        if (!empty($name)) {
                            $officers[$x]['middle_name'] = implode(" ", $name);
                        }
                    }
                    $officers[$x]['surname'] = (string) $v->Person->Surname;

                    /* same as registered office */
                    if (isset($v->Person->ServiceAddress->SameAsRegisteredOffice)) {
                        $officers[$x]['same_as_reg_office'] = 1;
                        /* address */
                    } else {
                        foreach ($address as $xmlField => $dbField) {
                            $officers[$x][$dbField] = (isset($v->Person->ServiceAddress->Address->{$xmlField}) &&
                                !empty($v->Person->ServiceAddress->Address->{$xmlField}))
                                ? (string) $v->Person->ServiceAddress->Address->{$xmlField}
                                : NULL;
                        }
                        /* other foreign country */
                        if (isset($v->Person->ServiceAddress->Address->OtherForeignCountry)
                            && !empty($v->Person->ServiceAddress->Address->OtherForeignCountry)
                        ) {
                            $officers[$x]['country'] = (string) $v->Person->ServiceAddress->Address->OtherForeignCountry;
                        }
                        /* fuckers send null for country so need to record it */
                        if ($officers[$x]['country'] === NULL) {
                            $officers[$x]['country_not_synced'] = 1;
                        }
                    }

                    $officers[$x]['dob']            = (string) $v->Person->DOB;
                    $officers[$x]['country_of_residence'] = (string) $v->Person->CountryOfResidence;
                    /* fuckers send null for country of residence as well, so need to record it */
                    if (!isset($officers[$x]['country_of_residence']) || empty($officers[$x]['country_of_residence'])) {
                        $officers[$x]['country_of_residence_not_synced'] = 1;
                    }
                    if (isset($v->Person->PreviousNames)) {
                        $previousNames = [];
                        foreach ($v->Person->PreviousNames as $n) {
                            $previousNames[] = (string) $n;
                        }
                        $officers[$x]['previous_names'] = implode(";", $previousNames);
                    }
                    /* corporate */
                } else {
                    $officers[$x]['corporate']  = 1;
                    $officers[$x]['corporate_name'] = (string) $v->Corporate->CorporateName;
                    /* Address */
                    foreach ($address as $xmlField => $dbField) {
                        $officers[$x][$dbField] = (isset($v->Corporate->Address->{$xmlField}) &&
                            !empty($v->Corporate->Address->{$xmlField}))
                            ? (string) $v->Corporate->Address->{$xmlField}
                            : NULL;
                    }
                    /* other foreign country */
                    if (isset($v->Corporate->Address->OtherForeignCountry)
                        && !empty($v->Corporate->Address->OtherForeignCountry)
                    ) {
                        $officers[$x]['country'] = (string) $v->Corporate->Address->OtherForeignCountry;
                    }
                    /* fuckers send null for country so need to record it */
                    if ($officers[$x]['country'] === NULL) {
                        $officers[$x]['country_not_synced'] = 1;
                    }

                    /* Identification */
                    /* NonEEA */
                    // MOthER FuCKers rETurN EmpTY FiELDs FOr IdeNTIFiCatioN
                    $identification = ['PlaceRegistered' => 'place_registered',
                        'RegistrationNumber' => 'registration_number',
                        'LawGoverned' => 'law_governed',
                        'LegalForm' => 'legal_form'];
                    if (isset($v->Corporate->CompanyIdentification->NonEEA)) {
                        $officers[$x]['identification_type']  = 'NonEEA';
                        foreach ($identification as $xmlField => $dbField) {
                            $officers[$x][$dbField]  = (isset($v->Corporate->CompanyIdentification->NonEEA->{$xmlField}) &&
                                !empty($v->Corporate->CompanyIdentification->NonEEA->{$xmlField}))
                                ? (string) $v->Corporate->CompanyIdentification->NonEEA->{$xmlField}
                                : NULL;
                        }
                        /* eea not received from sync */
                        if ((!isset($v->Corporate->CompanyIdentification->NonEEA->LawGoverned)
                                || empty($v->Corporate->CompanyIdentification->NonEEA->LawGoverned))
                            || (!isset($v->Corporate->CompanyIdentification->NonEEA->LegalForm)
                                || empty($v->Corporate->CompanyIdentification->NonEEA->LegalForm))
                        ) {
                            $officers[$x]['eea_not_synced'] = 1;
                            $officer['eea_complete'] = 0;
                        }
                    } else {
                        $officers[$x]['identification_type']  = 'EEA';
                        foreach ($identification as $xmlField => $dbField) {
                            $officers[$x][$dbField]  = (isset($v->Corporate->CompanyIdentification->EEA->{$xmlField}) &&
                                !empty($v->Corporate->CompanyIdentification->EEA->{$xmlField}))
                                ? (string) $v->Corporate->CompanyIdentification->EEA->{$xmlField}
                                : NULL;
                        }
                        /* eea not received from sync */
                        if ((!isset($v->Corporate->CompanyIdentification->EEA->PlaceRegistered)
                                || empty($v->Corporate->CompanyIdentification->EEA->PlaceRegistered))
                            || (!isset($v->Corporate->CompanyIdentification->EEA->RegistrationNumber)
                                || empty($v->Corporate->CompanyIdentification->EEA->RegistrationNumber))
                        ) {
                            $officers[$x]['eea_not_synced'] = 1;
                            $officer['eea_complete'] = 0;
                        }
                    } // end of identification
                } // end of corporate
            }
        }

        if (isset($xmlData->Officers->Director) && !empty($xmlData->Officers->Director)) {
            /* directors */
            $pscs = $xmlData->Officers->Director;

            foreach ($pscs as $v) {
                $x = count($officers);
                $officers[$x]['form_submission_id'] = $this->formSubmissionId;
                $officers[$x]['type'] = "DIR";
                /* person */
                if (isset($v->Person)) {

                    $officers[$x]['corporate']  = 0;
                    $officers[$x]['title'] = isset($v->Person->Title) ? (string) $v->Person->Title : NULL;
                    /* forename and middle name */
                    if (isset($v->Person->Forename)) {
                        $name = [];
                        foreach ($v->Person->Forename as $n) {
                            $name[] = (string) $n;
                        }
                        $officers[$x]['forename'] = $name[0];
                        unset($name[0]);
                        if (!empty($name)) {
                            $officers[$x]['middle_name'] = implode(" ", $name);
                        }
                    }
                    $officers[$x]['surname'] = (string) $v->Person->Surname;

                    /* same as registered office */
                    if (isset($v->Person->ServiceAddress->SameAsRegisteredOffice)) {
                        $officers[$x]['same_as_reg_office'] = 1;
                        /* address */
                    } else {
                        foreach ($address as $xmlField => $dbField) {
                            $officers[$x][$dbField] = (isset($v->Person->ServiceAddress->Address->{$xmlField}) &&
                                !empty($v->Person->ServiceAddress->Address->{$xmlField}))
                                ? (string) $v->Person->ServiceAddress->Address->{$xmlField}
                                : NULL;
                        }
                        /* other foreign country */
                        if (isset($v->Person->ServiceAddress->Address->OtherForeignCountry)
                            && !empty($v->Person->ServiceAddress->Address->OtherForeignCountry)
                        ) {
                            $officers[$x]['country'] = (string) $v->Person->ServiceAddress->Address->OtherForeignCountry;
                        }
                        /* fuckers send null for country so need to record it */
                        if ($officers[$x]['country'] === NULL) {
                            $officers[$x]['country_not_synced'] = 1;
                        }
                    }

                    $officers[$x]['dob']            = (string) $v->Person->DOB;
                    $officers[$x]['nationality']    = (string) $v->Person->Nationality;
                    $officers[$x]['occupation']     = (string) $v->Person->Occupation;
                    $officers[$x]['country_of_residence'] = (string) $v->Person->CountryOfResidence;
                    /* fuckers send null for country of residence as well, so need to record it */
                    if (!isset($officers[$x]['country_of_residence']) || empty($officers[$x]['country_of_residence'])) {
                        $officers[$x]['country_of_residence_not_synced'] = 1;
                    }
                    if (isset($v->Person->PreviousNames)) {
                        $previousNames = [];
                        foreach ($v->Person->PreviousNames as $n) {
                            $previousNames[] = (string) $n;
                        }
                        $officers[$x]['previous_names'] = implode(";", $previousNames);
                    }
                    /* corporate */
                } else {
                    $officers[$x]['corporate']  = 1;
                    $officers[$x]['corporate_name'] = (string) $v->Corporate->CorporateName;
                    /* Address */
                    foreach ($address as $xmlField => $dbField) {
                        $officers[$x][$dbField] = (isset($v->Corporate->Address->{$xmlField}) &&
                            !empty($v->Corporate->Address->{$xmlField}))
                            ? (string) $v->Corporate->Address->{$xmlField}
                            : NULL;
                    }
                    /* other foreign country */
                    if (isset($v->Corporate->Address->OtherForeignCountry)
                        && !empty($v->Corporate->Address->OtherForeignCountry)
                    ) {
                        $officers[$x]['country'] = (string) $v->Corporate->Address->OtherForeignCountry;
                    }
                    /* fuckers send null for country so need to record it */
                    if ($officers[$x]['country'] === NULL) {
                        $officers[$x]['country_not_synced'] = 1;
                    }

                    /* Identification */
                    /* NonEEA */
                    // MOthER FuCKers rETurN EmpTY FiELDs FOr IdeNTIFiCatioN
                    $identification = ['PlaceRegistered' => 'place_registered',
                        'RegistrationNumber' => 'registration_number',
                        'LawGoverned' => 'law_governed',
                        'LegalForm' => 'legal_form'];
                    if (isset($v->Corporate->CompanyIdentification->NonEEA)) {
                        $officers[$x]['identification_type']  = 'NonEEA';
                        foreach ($identification as $xmlField => $dbField) {
                            $officers[$x][$dbField]  = (isset($v->Corporate->CompanyIdentification->NonEEA->{$xmlField}) &&
                                !empty($v->Corporate->CompanyIdentification->NonEEA->{$xmlField}))
                                ? (string) $v->Corporate->CompanyIdentification->NonEEA->{$xmlField}
                                : NULL;
                        }
                        /* eea not received from sync */
                        if ((!isset($v->Corporate->CompanyIdentification->NonEEA->LawGoverned)
                                || empty($v->Corporate->CompanyIdentification->NonEEA->LawGoverned))
                            || (!isset($v->Corporate->CompanyIdentification->NonEEA->LegalForm)
                                || empty($v->Corporate->CompanyIdentification->NonEEA->LegalForm))
                        ) {
                            $officers[$x]['eea_not_synced'] = 1;
                            $officer['eea_complete'] = 0;
                        }
                    } else {
                        $officers[$x]['identification_type']  = 'EEA';
                        foreach ($identification as $xmlField => $dbField) {
                            $officers[$x][$dbField]  = (isset($v->Corporate->CompanyIdentification->EEA->{$xmlField}) &&
                                !empty($v->Corporate->CompanyIdentification->EEA->{$xmlField}))
                                ? (string) $v->Corporate->CompanyIdentification->EEA->{$xmlField}
                                : NULL;
                        }
                        /* eea not received from sync */
                        if ((!isset($v->Corporate->CompanyIdentification->EEA->PlaceRegistered)
                                || empty($v->Corporate->CompanyIdentification->EEA->PlaceRegistered))
                            || (!isset($v->Corporate->CompanyIdentification->EEA->RegistrationNumber)
                                || empty($v->Corporate->CompanyIdentification->EEA->RegistrationNumber))
                        ) {
                            $officers[$x]['eea_not_synced'] = 1;
                            $officer['eea_complete'] = 0;
                        }
                    } // end of identification
                } // end of corporate
            } // end of director foreach
        }
        /* secretaries */
        if (isset($xmlData->Officers->Secretary) && !empty($xmlData->Officers->Secretary)) {
            $secs = $xmlData->Officers->Secretary;

            foreach ($secs as $v) {
                $x = count($officers);
                $officers[$x]['form_submission_id'] = $this->formSubmissionId;
                $officers[$x]['type'] = "SEC";
                /* person */
                if (isset($v->Person)) {

                    $officers[$x]['corporate']  = 0;
                    $officers[$x]['title'] = isset($v->Person->Title) ? (string) $v->Person->Title : NULL;
                    /* forename and middle name */
                    if (isset($v->Person->Forename)) {
                        $name = [];
                        foreach ($v->Person->Forename as $n) {
                            $name[] = (string) $n;
                        }
                        $officers[$x]['forename'] = $name[0];
                        unset($name[0]);
                        if (!empty($name)) {
                            $officers[$x]['middle_name'] = implode(" ", $name);
                        }
                    }
                    $officers[$x]['surname'] = (string) $v->Person->Surname;

                    /* same as registered office */
                    if (isset($v->Person->ServiceAddress->SameAsRegisteredOffice)) {
                        $officers[$x]['same_as_reg_office'] = 1;
                        /* address */
                    } else {
                        foreach ($address as $xmlField => $dbField) {
                            $officers[$x][$dbField] = (isset($v->Person->ServiceAddress->Address->{$xmlField}) &&
                                !empty($v->Person->ServiceAddress->Address->{$xmlField}))
                                ? (string) $v->Person->ServiceAddress->Address->{$xmlField}
                                : NULL;
                        }
                        /* other foreign country */
                        if (isset($v->Person->ServiceAddress->Address->OtherForeignCountry)
                            && !empty($v->Person->ServiceAddress->Address->OtherForeignCountry)
                        ) {
                            $officers[$x]['country'] = (string) $v->Person->ServiceAddress->Address->OtherForeignCountry;
                        }
                        /* fuckers send null for country so need to record it */
                        if ($officers[$x]['country'] === NULL) {
                            $officers[$x]['country_not_synced'] = 1;
                        }
                    }

                    if (isset($v->Person->PreviousNames)) {
                        $previousNames = [];
                        foreach ($v->Person->PreviousNames as $n) {
                            $previousNames[] = (string) $n;
                        }
                        $officers[$x]['previous_names'] = implode(";", $previousNames);
                    }
                    /* corporate */
                } else {
                    $officers[$x]['corporate']  = 1;
                    $officers[$x]['corporate_name'] = (string) $v->Corporate->CorporateName;
                    /* Address */
                    foreach ($address as $xmlField => $dbField) {
                        $officers[$x][$dbField] = (isset($v->Corporate->Address->{$xmlField}) &&
                            !empty($v->Corporate->Address->{$xmlField}))
                            ? (string) $v->Corporate->Address->{$xmlField}
                            : NULL;
                    }
                    /* other foreign country */
                    if (isset($v->Corporate->Address->OtherForeignCountry)
                        && !empty($v->Corporate->Address->OtherForeignCountry)
                    ) {
                        $officers[$x]['country'] = (string) $v->Corporate->Address->OtherForeignCountry;
                    }
                    /* fuckers send null for country so need to record it */
                    if ($officers[$x]['country'] === NULL) {
                        $officers[$x]['country_not_synced'] = 1;
                    }
                    /* Identification */
                    /* NonEEA */
                    // MOthER FuCKers rETurN EmpTY FiELDs FOr IdeNTIFiCatioN
                    $identification = ['PlaceRegistered' => 'place_registered',
                        'RegistrationNumber' => 'registration_number',
                        'LawGoverned' => 'law_governed',
                        'LegalForm' => 'legal_form'];
                    if (isset($v->Corporate->CompanyIdentification->NonEEA)) {
                        $officers[$x]['identification_type']  = 'NonEEA';
                        foreach ($identification as $xmlField => $dbField) {
                            $officers[$x][$dbField]  = (isset($v->Corporate->CompanyIdentification->NonEEA->{$xmlField}) &&
                                !empty($v->Corporate->CompanyIdentification->NonEEA->{$xmlField}))
                                ? (string) $v->Corporate->CompanyIdentification->NonEEA->{$xmlField}
                                : NULL;
                        }
                        /* eea not received from sync */
                        if ((!isset($v->Corporate->CompanyIdentification->NonEEA->LawGoverned)
                                || empty($v->Corporate->CompanyIdentification->NonEEA->LawGoverned))
                            || (!isset($v->Corporate->CompanyIdentification->NonEEA->LegalForm)
                                || empty($v->Corporate->CompanyIdentification->NonEEA->LegalForm))
                        ) {
                            $officers[$x]['eea_not_synced'] = 1;
                            $officer['eea_complete'] = 0;
                        }
                    } else {
                        $officers[$x]['identification_type']  = 'EEA';
                        foreach ($identification as $xmlField => $dbField) {
                            $officers[$x][$dbField]  = (isset($v->Corporate->CompanyIdentification->EEA->{$xmlField}) &&
                                !empty($v->Corporate->CompanyIdentification->EEA->{$xmlField}))
                                ? (string) $v->Corporate->CompanyIdentification->EEA->{$xmlField}
                                : NULL;
                        }
                        /* eea not received from sync */
                        if ((!isset($v->Corporate->CompanyIdentification->EEA->PlaceRegistered)
                                || empty($v->Corporate->CompanyIdentification->EEA->PlaceRegistered))
                            || (!isset($v->Corporate->CompanyIdentification->EEA->RegistrationNumber)
                                || empty($v->Corporate->CompanyIdentification->EEA->RegistrationNumber))
                        ) {
                            $officers[$x]['eea_not_synced'] = 1;
                            $officer['eea_complete'] = 0;
                        }
                    } // end of identification
                } // end of corporate
            } // end of secretary foreach
        } else {
            $secs = [];
        }

        //todo: psc - sync psc level statement (+ empty data)
        $pscCompanyStatement = NULL;
        $pscs = [];
        $x = 0;

        if (isset($xmlData->PSCs) && !empty($xmlData->PSCs)) {
            if (isset($xmlData->PSCs->CompanyStatement)) {
                //todo: psc - company level psc statement - delete (skip) all pscs if present?
                $pscCompanyStatement = (string) $xmlData->PSCs->CompanyStatement;
            }

            if (isset($xmlData->PSCs->PSC)) {
                foreach ($xmlData->PSCs->PSC as $pscElement) {
                    $x++;

                    $pscs[$x]['form_submission_id'] = $this->formSubmissionId;
                    $pscs[$x]['type'] = "PSC";
                    $pscs[$x]['existing_officer'] = 1;

                    if (isset($pscElement->PSCLinkedStatementNotification)) {
                        //todo: psc - needs implementation
                        throw new Exception('Unable to process PSCLinkedStatementNotifications.');
                    } elseif (isset($pscElement->PSCStatementNotification)) {
                        $pscs[$x]['psc_statement_notification'] = (string) $pscElement->PSCStatementNotification;
                    } elseif (isset($pscElement->PSCNotification)) {
                        $v = $pscElement->PSCNotification;

                        if (isset($v->NatureOfControls)) {
                            foreach ($v->NatureOfControls[0] as $natureOfControl) {
                                $noc = (string) $natureOfControl;

                                if (isset(NatureOfControl::$groups[$noc])) {
                                    $pscs[$x][NatureOfControl::$groups[$noc]] = $noc;
                                }
                            }
                        }

                        /* person */
                        if (isset($v->Individual)) {

                            $pscs[$x]['corporate'] = 0;
                            $pscs[$x]['title'] = isset($v->Individual->Title) ? (string) $v->Individual->Title : NULL;
                            /* forename and middle name */
                            if (isset($v->Individual->Forename)) {
                                $name = [];
                                foreach ($v->Individual->Forename as $n) {
                                    $name[] = (string) $n;
                                }
                                $pscs[$x]['forename'] = $name[0];
                                unset($name[0]);
                                if (!empty($name)) {
                                    $pscs[$x]['middle_name'] = implode(" ", $name);
                                }
                            }
                            $pscs[$x]['surname'] = (string) $v->Individual->Surname;

                            /* same as registered office */
                            if (isset($v->Individual->ServiceAddress->SameAsRegisteredOffice)) {
                                $pscs[$x]['same_as_reg_office'] = 1;
                                /* address */
                            } else {
                                foreach ($address as $xmlField => $dbField) {
                                    $pscs[$x][$dbField] = (isset($v->Individual->ServiceAddress->Address->{$xmlField}) &&
                                        !empty($v->Individual->ServiceAddress->Address->{$xmlField}))
                                        ? (string) $v->Individual->ServiceAddress->Address->{$xmlField}
                                        : NULL;
                                }
                                /* other foreign country */
                                if (isset($v->Individual->ServiceAddress->Address->OtherForeignCountry)
                                    && !empty($v->Individual->ServiceAddress->Address->OtherForeignCountry)
                                ) {
                                    $pscs[$x]['country'] = (string) $v->Individual->ServiceAddress->Address->OtherForeignCountry;
                                }
                                /* fuckers send null for country so need to record it */
                                if ($pscs[$x]['country'] === NULL) {
                                    $pscs[$x]['country_not_synced'] = 1;
                                }
                            }

                            if (isset($v->Individual->ResidentialAddress)) {
                                foreach ($v->Individual->ResidentialAddress->Address[0] as $resAddrNodeName => $residentialAddressData) {
                                    if (isset($address[$resAddrNodeName])) {
                                        $pscs[$x]['residential_' . $address[$resAddrNodeName]] = (string) $residentialAddressData;
                                    }
                                }
//
//                                foreach ($address as $xmlField => $dbField) {
//                                    $pscs[$x]['residential_'.$dbField] = (isset($v->Individual->ResidentialAddress->Address->{$xmlField}) &&
//                                        !empty($v->Individual->ResidentialAddress->Address->{$xmlField}))
//                                        ? (string) $v->Individual->ResidentialAddress->Address->{$xmlField}
//                                        : NULL;
//                                }
                            }

                            $pscs[$x]['dob'] = (string) $v->Individual->DOB;
                            $pscs[$x]['nationality'] = (string) $v->Individual->Nationality;
                            $pscs[$x]['country_of_residence'] = (string) $v->Individual->CountryOfResidence;
                            /* fuckers send null for country of residence as well, so need to record it */
                            if (!isset($pscs[$x]['country_of_residence']) || empty($pscs[$x]['country_of_residence'])) {
                                $pscs[$x]['country_of_residence_not_synced'] = 1;
                            }
                            if (isset($v->Individual->PreviousNames)) {
                                $previousNames = [];
                                foreach ($v->Individual->PreviousNames as $n) {
                                    $previousNames[] = (string) $n;
                                }
                                $pscs[$x]['previous_names'] = implode(";", $previousNames);
                            }
                            /* corporate */
                        } elseif (isset($v->Corporate)) {
                            $pscs[$x]['corporate'] = 1;
                            $pscs[$x]['corporate_name'] = (string) $v->Corporate->CorporateName;
                            /* Address */
                            foreach ($address as $xmlField => $dbField) {
                                $pscs[$x][$dbField] = (isset($v->Corporate->Address->{$xmlField}) &&
                                    !empty($v->Corporate->Address->{$xmlField}))
                                    ? (string) $v->Corporate->Address->{$xmlField}
                                    : NULL;
                            }
                            /* other foreign country */
                            if (isset($v->Corporate->Address->OtherForeignCountry)
                                && !empty($v->Corporate->Address->OtherForeignCountry)
                            ) {
                                $pscs[$x]['country'] = (string) $v->Corporate->Address->OtherForeignCountry;
                            }
                            /* fuckers send null for country so need to record it */
                            if ($pscs[$x]['country'] === NULL) {
                                $pscs[$x]['country_not_synced'] = 1;
                            }

                            /* Identification */
                            /* NonEEA */
                            // MOthER FuCKers rETurN EmpTY FiELDs FOr IdeNTIFiCatioN
                            $identification = [
                                'PSCPlaceRegistered' => 'place_registered',
                                'PSCRegistrationNumber' => 'registration_number',
                                'LawGoverned' => 'law_governed',
                                'LegalForm' => 'legal_form',
                                'CountryOrState' => 'country_or_state',
                            ];
                            if (isset($v->Corporate->PSCCompanyIdentification)) {
                                $pscs[$x]['identification_type'] = 'PSC';
                                foreach ($identification as $xmlField => $dbField) {
                                    $pscs[$x][$dbField] = (isset($v->Corporate->PSCCompanyIdentification->{$xmlField}) &&
                                        !empty($v->Corporate->PSCCompanyIdentification->{$xmlField}))
                                        ? (string) $v->Corporate->PSCCompanyIdentification->{$xmlField}
                                        : NULL;
                                }
                                /* eea not received from sync */
                                if ((!isset($v->Corporate->PSCCompanyIdentification->LegalForm)
                                        || empty($v->Corporate->PSCCompanyIdentification->LegalForm))
                                    || (!isset($v->Corporate->PSCCompanyIdentification->LawGoverned)
                                        || empty($v->Corporate->PSCCompanyIdentification->LawGoverned))
                                ) {
                                    $pscs[$x]['eea_not_synced'] = 1;
                                    $psc['eea_complete'] = 0;
                                }
                            } // end of identification
                        } elseif (isset($v->LegalPerson)) {
                            $pscs[$x]['corporate'] = 2;
                            $pscs[$x]['corporate_name'] = (string) $v->LegalPerson->LegalPersonName;
                            /* Address */
                            foreach ($address as $xmlField => $dbField) {
                                $pscs[$x][$dbField] = (isset($v->LegalPerson->Address->{$xmlField}) &&
                                    !empty($v->LegalPerson->Address->{$xmlField}))
                                    ? (string) $v->LegalPerson->Address->{$xmlField}
                                    : NULL;
                            }
                            /* other foreign country */
                            if (isset($v->LegalPerson->Address->OtherForeignCountry)
                                && !empty($v->LegalPerson->Address->OtherForeignCountry)
                            ) {
                                $pscs[$x]['country'] = (string) $v->LegalPerson->Address->OtherForeignCountry;
                            }
                            /* fuckers send null for country so need to record it */
                            if ($pscs[$x]['country'] === NULL) {
                                $pscs[$x]['country_not_synced'] = 1;
                            }

                            /* Identification */
                            /* NonEEA */
                            // MOthER FuCKers rETurN EmpTY FiELDs FOr IdeNTIFiCatioN
                            $identification = [
                                'LawGoverned' => 'law_governed',
                                'LegalForm' => 'legal_form',
                            ];
                            if (isset($v->LegalPerson->LegalPersonIdentification)) {
                                $pscs[$x]['identification_type'] = 'PSC';
                                foreach ($identification as $xmlField => $dbField) {
                                    $pscs[$x][$dbField] = (isset($v->LegalPerson->LegalPersonIdentification->{$xmlField}) &&
                                        !empty($v->LegalPerson->LegalPersonIdentification->{$xmlField}))
                                        ? (string) $v->LegalPerson->LegalPersonIdentification->{$xmlField}
                                        : NULL;
                                }
                                /* eea not received from sync */
                                if ((!isset($v->LegalPerson->LegalPersonIdentification->LegalForm)
                                        || empty($v->LegalPerson->LegalPersonIdentification->LegalForm))
                                    || (!isset($v->LegalPerson->LegalPersonIdentification->LawGoverned)
                                        || empty($v->LegalPerson->LegalPersonIdentification->LawGoverned))
                                ) {
                                    $pscs[$x]['eea_not_synced'] = 1;
                                    $psc['eea_complete'] = 0;
                                }
                            } // end of identification
                        } // end of legal person
                    }

                } // end of director foreach
            }
        }


        /* update officers db */
        CHFiling::getDb()->delete("ch_annual_return_officer", 'form_submission_id = '.$this->formSubmissionId);
        foreach ($officers as $officer) {
            CHFiling::getDb()->insert("ch_annual_return_officer", $officer);
        }

        foreach ($pscs as $psc) {
            if (isset($psc['corporate'])) {
                $dbObject = new AnnualReturnPersonPsc($psc['form_submission_id']);

                if ($psc['corporate'] == 1) {
                    $dbObject = new AnnualReturnCorporatePsc($psc['form_submission_id']);
                } elseif ($psc['corporate'] == 2) {
                    $dbObject = new AnnualReturnLegalPersonPsc($psc['form_submission_id']);
                }

                $dbObject->setFields($psc);
                $psc['before_changes'] = json_encode($dbObject->getData());
            }

            CHFiling::getDb()->insert("ch_annual_return_officer", $psc);
        }

        CHFiling::getDb()->update('ch_annual_return', ['no_psc_reason' => $pscCompanyStatement], 'form_submission_id = '.$this->formSubmissionId);

        // no need for members - won't use them for annual return

        /* these values are not in sync, so null them */
        $data['regulated_markets'] = NULL;
        if ($data['company_category'] == 'BYGUAR' || $data['company_category'] == 'BYGUAREXUNDSEC60') {
            $data['members_list']      = 'NONE';
        } else {
            $data['members_list']      = 'FULL';
        }

        /* update annual return db */
        CHFiling::getDb()->update(self::$tableName, $data, 'form_submission_id = '.$this->formSubmissionId);

        /* statement of capital */
        $shares = [];
        $x = 0;
        if (isset($xmlData->StatementOfCapital)) {
            foreach ($xmlData->StatementOfCapital->Capital as $capital) {
                $currency = (string) $capital->ShareCurrency;
                foreach ($capital->Shares as $share) {
                    $shares[$x]['share_class'] = (string) $share->ShareClass;
                    // fucking companies house does not return this, even thogh they have to
                    if (isset($share->PrescribedParticulars)) {
                        $shares[$x]['prescribed_particulars'] = (string) $share->PrescribedParticulars;
                    } else {
                        $shares[$x]['prescribed_particulars'] = 'Full voting rights.';
                    }

                    $shares[$x]['aggregate_nom_value']  = (string) $share->AggregateNominalValue;
                    $shares[$x]['num_shares']           = (string) $share->NumShares;
                    $shares[$x]['currency']             = $currency;
                    $shares[$x]['amount_paid']          = (double) $share->AmountPaidDuePerShare;
                    $shares[$x]['amount_unpaid']        = (double) $share->AmountUnpaidPerShare;

                    $shareValue = $shares[$x]['aggregate_nom_value'] / $shares[$x]['num_shares'];

                    /* partialy paid */
                    if ($shares[$x]['amount_paid'] != 0 && $shares[$x]['amount_unpaid'] != 0) {
                        $shares[$x]['sync_paid'] = $shares[$x]['amount_paid'];
                        $shares[$x]['paid']      = $shares[$x]['amount_paid'];
                        /* unpaid */
                    } elseif ($shares[$x]['amount_paid'] == 0 && $shares[$x]['amount_unpaid'] != 0) {
                        $shares[$x]['sync_paid'] = 'unpaid';
                        $shares[$x]['paid']      = 'unpaid';
                        /* paid */
                    } elseif ($shares[$x]['amount_paid'] != 0 && $shares[$x]['amount_unpaid'] == 0) {
                        $shares[$x]['sync_paid'] = 'paid';
                        $shares[$x]['paid']      = 'paid';
                        /* both values 0 */
                    } else {
                        $shares[$x]['sync_paid'] = 'unpaid';
                        $shares[$x]['paid']      = 'unpaid';
                        $shares[$x]['amount_unpaid'] = $shareValue;
                        $shares[$x]['amount_paid'] = 0;
                    }

                    /* we make it all paid, customer can change it later */
                    /*
                    $shareValue = (int) $share->AmountPaidDuePerShare + (int) $share->AmountUnpaidPerShare;
                    $shares[$x]['num_shares'] = (string) $share->NumShares;
                    $shares[$x]['currency'] = $currency;
                    $shares[$x]['aggregate_nom_value'] = (string) $share->AggregateNominalValue;
                     */
                    /* sometimes mf sends amount paid and unpaid 0 */
                    /*
                    if ($shareValue > 0) {
                        $shares[$x]['amount_paid'] = $shareValue;
                    } else {
                        $shares[$x]['amount_paid'] = $shares[$x]['aggregate_nom_value'] / $shares[$x]['num_shares'];
                    }
                    $shares[$x]['amount_unpaid'] = null;
                     */
                    $x++;
                }
            }
        }

        /* update statement of capital db */
        CHFiling::getDb()->delete("ch_annual_return_shares", 'form_submission_id = '.$this->formSubmissionId);
        foreach ($shares as $share) {
            $share['form_submission_id'] = $this->formSubmissionId;
            CHFiling::getDb()->insert("ch_annual_return_shares", $share);
        }

        /* shareholdings */
        $i = 0;
        $data['shareholding'] = [];
        if (isset($xmlData->Shareholdings)) {
            /* remove old records */
            foreach ($this->shareholdings as $k => $v) {
                /* shareholoders and transfers */
                CHFiling::getDb()->delete("ch_annual_return_shareholder", "annual_return_shareholding_id = ".$k);
                CHFiling::getDb()->delete("ch_annual_return_transfer", "annual_return_shareholding_id = ".$k);
            }
            /* shareholdings */
            CHFiling::getDb()->delete("ch_annual_return_shareholding", 'form_submission_id = '.$this->formSubmissionId);
            foreach ($xmlData->Shareholdings as $v) {
                $shareholding = [];
                $shareholding['share_class'] = (string) $v->ShareClass;
                $shareholding['number_held'] = (string) $v->NumberHeld;
                $shareholding['form_submission_id'] = $this->formSubmissionId;
                $shareholding['sync'] = 1; // indicate that this is from sync
                /* insert new record */
                CHFiling::getDb()->insert("ch_annual_return_shareholding", $shareholding);
                $shareholdingId = CHFiling::getDb()->lastInsertId();

                $shareholding['transfer'] = [];
                $shareholding['shareholder'] = [];

                /* shareholders */
                $x = 0;
                foreach ($v->Shareholders as $sh) {
                    $shareholder= [];
                    $forename   = [];
                    $shareholder['surname'] = (string) $sh->Name->Surname;
                    foreach ($sh->Name->Forename as $fn) {
                        $forename[] = $fn;
                    }
                    if (!empty($forename)) {
                        $shareholder['forename'] = $forename[0];
                        unset($forename[0]);
                        if (!empty($forename[0])) {
                            $shareholder['middle_name'] = implode(" ", $forename);
                        }
                    }

                    /* address */
                    if (isset($sh->Address)) {
                        foreach ($address as $k => $v) {
                            if ($v != 'care_of_name' && $v != 'po_box') {
                                $shareholder[$v] = (isset($sh->Address->{$k}) &&
                                    !empty($sh->Address->{$k}))
                                    ? (string) $sh->Address->{$k}
                                    : NULL;
                            }
                        }
                    }
                    $shareholder['annual_return_shareholding_id'] = $shareholdingId;

                    /* insert new shareholder from sync */
                    $shareholding['shareholder'][$x++] = $shareholder;
                    CHFiling::getDb()->insert("ch_annual_return_shareholder", $shareholder);
                }
            }
        }

        /* update instance variables */
        $data['shares'] = $shares; // TODO - delete this when shares are separate
        $this->setFields($data);

    }

    /**
     *
     * @return int
     */
    public function getFormSubmissionId()
    {
        return $this->formSubmissionId;
    }

    /**
     * Returns Trading On Regulated Market bool
     *
     * @return bool
     */
    public function getTrading()
    {
        return $this->trading;
    }

    /**
     * sets 'Trading On Regulated Market
     *
     * @param bool $trading
     */
    public function setTrading($trading)
    {
        $this->trading = $trading;
    }

    /**
     * Returns Dtr5
     *
     * @return bool
     */
    public function getDtr5()
    {
        return $this->dtr5;
    }

    /**
     * sets Dtr5
     *
     * @param bool $Dtr5
     */
    public function setDtr5($dtr5)
    {
        $this->dtr5 = $dtr5;
    }

    /**
     * Returns type of sic codes
     *
     * @return Enum ('code', 'text')
     */
    public function getSicCodeType()
    {
        return $this->sicCodeType;
    }

    /**
     * returns sic codes as array. If SicCodeType is text, then array will
     * contain only one element, otherwise array size can be from 1 to 4
     * elements
     *
     * @return array
     */
    public function getSicCodes()
    {
        return explode(";", $this->sicCode);
    }

    /**
     * Sets numeric sic codes
     *
     * @param array $sicCodes array of sic codes
     */
    public function setSicCodes(array $sCodes)
    {
        $sicCodes = [];
        foreach ($sCodes as $sicCode) {
            if (!empty($sicCode)) {
                $sicCodes[] = $sicCode;
            }
        }
        if (count($sicCodes) > 4) {
            throw new Exception("Maximum 4 siccodes can be set.");
        }
        $this->sicCodeType = "code";
        $this->sicCode = implode(";", $sicCodes);
    }

    /**
     * sets text sic code
     *
     * @param string $sicCode
     */
    public function setSicCodeText($sicCode)
    {
        $this->sicCodeType = "text";
        $this->sicCode = $sicCode;
    }

    /**
     * returns fields as an array
     * fields don't include officers, shares and shareholdings, so you
     * need to call appropriate methods
     *
     * @return array
     */
    public function getFields()
    {
        $fields = [];
        $fields['company_category'] = $this->companyCategory;
        $fields['made_up_date'] = $this->madeUpDate;
        $fields['sic_code_type'] = $this->sicCodeType;
        $fields['sic_code'] = explode(";", $this->sicCode);
        $fields['trading'] = $this->trading;
        $fields['dtr5'] = $this->dtr5;
        $fields['reg_office'] = $this->regOffice->getFields();
        $fields['sail_address'] = $this->sailAddress->getFields();
        $fields['members_list'] = $this->memebersList;

        return $fields;
    }

    /**
     *
     * @return Address
     */
    public function getRegOffice()
    {
        return $this->regOffice;
    }

    /**
     *
     * @return Address
     */
    public function getSailAddress()
    {
        return $this->sailAddress;
    }

    public function getSailRecords()
    {
        return $this->sailRecords;
    }

    public function getCompanyCategory()
    {
        return $this->companyCategory;
    }

    /**
     * @return Date
     */
    public function getMadeUpDate()
    {
        return $this->madeUpDate;
    }

    /**
     * Pre-condition: $date has to be in date format YYYY-MM-DD
     * and has to be less or equal to today's date
     *
     * @param string $date
     */
    public function setMadeUpDate($date)
    {

        // --- date ---
        // check format
        if (!preg_match('/^[\d]{4}-[\d]{1,2}-[\d]{1,2}$/', $date)) {
            throw new Exception('Date has to be in YYYY-MM-DD format');
        }

        $d      = explode('-', $date);
        $month  = $d[1];
        $day    = $d[2];
        $year   = $d[0];

        // check if it's correct
        if (!checkdate($month, $day, $year)) {
            throw new Exception('Date has to be correct date.');
        }

        // check if date is less than today's day
        if (strtotime($date) > time()) {
            throw new Exception('Date is more than today\'s date');
        }
        $this->madeUpDate = $date;
    }

    public function getMembersList()
    {
        return $this->memebersList;
    }

    public function getShareholdings()
    {
        return $this->shareholdings;
    }

    public function getShareholding($id)
    {
        return isset($this->shareholdings[$id]) ? $this->shareholdings[$id] : NULL;
    }

    public function getNewShareholding()
    {
        return Shareholding::getNewShareholding($this);
    }

    /**
     * return array of data from db
     *
     * @return array
     */
    public function getOfficers()
    {
        return $this->officers;
    }

    public function getOfficer($id)
    {

        foreach ($this->officers as $officer) {
            if ($officer['annual_return_officer_id'] == $id) {
                return $officer;
            }
        }
        return NULL;
    }

    /**
     * Officers with missing eea or nonEea identification
     *
     * @param $type DIR|SEC|MEM|null
     * @return array
     */
    public function getNotSyncedOfficers($type = NULL)
    {

        if ($type !== NULL && $type != 'DIR' && $type != 'SEC' && $type != 'MEM') {
            throw new Exception("type can be only null, DIR or SEC.");
        }

        $type = strtoupper($type);

        $officers = [];
        if ($type == NULL) {
            foreach ($this->officers as $officer) {
                /* check if the type is set - null returns DIRs and SECs */
                if ($type != NULL) {
                    /* type can be DIR or SEC, this officer is not the type user want, then skip */
                    if ($officer[$type] != $type) {
                        continue;
                    }
                }
                if ($officer['eea_not_synced'] || $officer['country_not_synced'] || $officer['country_of_residence_not_synced'] || empty($officer['premise']) || empty($officer['street'])) {
                    /* save if officer is corporate - when setting the name in db, we need to know if it's corporate */

                    $officers[$officer['annual_return_officer_id']]['corporate']= $officer['corporate'];
                    $officers[$officer['annual_return_officer_id']]['type']     = $officer['type'];

                    //just for llp companies
                    if ($officer['type'] == 'MEM') {
                        $officers[$officer['annual_return_officer_id']]['designated_ind']     = $officer['designated_ind'];
                    }

                    /* corporate */
                    if ($officer['corporate']) {
                        /* corporate name */
                        $officers[$officer['annual_return_officer_id']]['name'] = $officer['corporate_name'];

                        //
                        $officers[$officer['annual_return_officer_id']]['premise'] = $officer['premise'];
                        $officers[$officer['annual_return_officer_id']]['street']  = $officer['street'];
                        $officers[$officer['annual_return_officer_id']]['post_town']  = $officer['post_town'];
                        $officers[$officer['annual_return_officer_id']]['thoroughfare']  = $officer['thoroughfare'];
                        //


                        /* identification - only corporate officers */
                        if ($officer['eea_not_synced']) {
                            $officers[$officer['annual_return_officer_id']]['identification_type']  = $officer['identification_type'];
                            $officers[$officer['annual_return_officer_id']]['place_registered']     = $officer['place_registered'];
                            $officers[$officer['annual_return_officer_id']]['registration_number']  = $officer['registration_number'];
                            $officers[$officer['annual_return_officer_id']]['law_governed']         = $officer['law_governed'];
                            $officers[$officer['annual_return_officer_id']]['legal_form']           = $officer['legal_form'];
                        }
                        /* person */
                    } else {
                        /* set name */
                        $officers[$officer['annual_return_officer_id']]['name'] = $officer['forename'];
                        if ($officer['middle_name'] !== NULL) {
                            $officers[$officer['annual_return_officer_id']]['name'] .= " " . $officer['middle_name'];
                        }
                        $officers[$officer['annual_return_officer_id']]['name'] .= " " . $officer['surname'];

                        //
                        $officers[$officer['annual_return_officer_id']]['premise'] = $officer['premise'];
                        $officers[$officer['annual_return_officer_id']]['street']  = $officer['street'];
                        $officers[$officer['annual_return_officer_id']]['post_town']  = $officer['post_town'];
                        $officers[$officer['annual_return_officer_id']]['thoroughfare']  = $officer['thoroughfare'];
                        //

                        /* country of residence */
                        if ($officer['country_of_residence_not_synced']) {
                            $officers[$officer['annual_return_officer_id']]['country_of_residence'] = $officer['country_of_residence'];
                        }
                    }

                    /* country - corporate and person */
                    if ($officer['country_not_synced']) {
                        $officers[$officer['annual_return_officer_id']]['country']  = $officer['country'];
                    }
                }
            }
        }
        //pr($officers);exit;
        return $officers;
    }

    /**
     * officer should have the same structure as array returned from
     * getNotSyncedOfficers or getOfficers. All fields are updated,
     * so do not set them to null.
     *
     * @param array $officer
     */
    public function updateOfficer($id, array $officer)
    {

        /* check if identification is complete */
        if (isset($officer['identification_type']) && !empty($officer['identification_type'])) {
            if ($officer['identification_type'] == 'EEA') {
                if ((isset($officer['place_registered']) && !empty($officer['place_registered']))
                    && (isset($officer['registration_number']) && !empty($officer['registration_number']))
                ) {

                    $officer['eea_complete'] = 1;
                }
            } else {
                if ((isset($officer['law_governed']) && !empty($officer['law_governed']))
                    && (isset($officer['legal_form']) && !empty($officer['legal_form']))
                ) {

                    $officer['eea_complete'] = 1;
                }
            }
        } else {
            $officer['eea_complete'] = 0;
        }

        //designation just for llp companies unset for all other types
        if (!isset($officer['designated_ind'])) {
            unset($officer['designated_ind']);
        }

        /* make sure there's no id */
        unset($officer['annual_return_officer_id']);
        /* name is used just for displaying the form */
        unset($officer['name']);

        /* all the fields are set as they come, so this can change country etc. as well */

        CHFiling::getDb()->update('ch_annual_return_officer', $officer, 'annual_return_officer_id = '.$id);
    }

    public function getShares()
    {
        return $this->shares;
    }

    /**
     * formats and returns shares as an array
     *
     * @return array
     */
    public function getStatementOfCapital()
    {

        /*
         * there are two columns in the db:
         * sync_paid and paid
         * both of them can have 'paid', 'unpaid' or numeric value
         * paid means that all shares have been paid for, unpaid is oposite and
         * numeric value represent combination - paid part and the rest is unpaid.
         * sync_paid is the one from companies house and paid is the one that user set
         */

        $shares = [];
        foreach ($this->shares as $share) {


            /* paid from sync */
            if ($share['sync_paid'] == "paid") {
                $share['paid'] = ["paid" => 1];
                /* unpaid from sync */
            } elseif ($share['sync_paid'] == "unpaid") {
                /* set numeric value for partially paid */
                if ($share['paid'] == 'paid' || $share['paid'] == 'unpaid') {
                    $share['partialy_paid'] = 0;
                } else {
                    $share['partialy_paid'] = $share['paid'];
                }
                if ($share['paid'] == 'paid') {
                    $share['paid'] = ["unpaid" => 0, "partialy_paid" => 0, "paid" => 1];
                } elseif ($share['paid'] == 'unpaid') {
                    $share['paid'] = ["unpaid" => 1, "partialy_paid" => 0, "paid" => 0];
                } else {
                    $share['paid'] = ["unpaid" => 0, "partialy_paid" => 1, "paid" => 0];
                }
                /* partialy paid from sync */
            } else {
                /* in this case 'paid' holds the number */
                if ($share['paid'] == 'paid') {
                    $share['partialy_paid'] = $share['sync_paid'];
                    $share['paid'] = ["partialy_paid" => 0, "paid" => 1];
                } else {
                    $share['partialy_paid'] = $share['paid'];
                    $share['paid'] = ["partialy_paid" => 1, "paid" => 1];
                }
            }
            unset($share['sync_paid']);
            $share['share_value'] = $share['amount_paid'] + $share['amount_unpaid'];

            $shares[$share['annual_return_shares_id']] = $share;
        }
        return $shares;
    }

    /**
     *
     * @param int $id - share id - the one you want to update
     * @param string $shareClass - share class
     * @param enum(paid, unpaid, partialy_paid) $paid
     * @param <type> $partPaid
     */
    public function setStatementOfCapital($id, $shareClass, $paid, $partPaid = NULL)
    {

        /*
        pr($id);
        pr($shareClass);
        pr($paid);
        pr($partPaid);
        pr($this->shares[$id]);
        exit;
         */

        if (!isset($this->shares[$id])) {
            throw new Exception("Specified share does not exist");
        }

        /* set share class */
        $this->shares[$id]['share_class'] = $shareClass;

        /* get specific share */
        $share = $this->shares[$id];

        /* holds data to be saved in db */
        $data = [];

        /* validation and setting data */
        if ($paid == "partialy_paid") {

            /* paid */
            if ($share['sync_paid'] == 'paid') {
                throw new Exception('Your shares need to be fully paid');
            }
            /* partialy paid */
            if ($share['sync_paid'] != 'unpaid' && $share['sync_paid'] != 'paid') {
                /* cannot have less paid than before */
                if ($partPaid < $share['sync_paid']) {
                    throw new Exception('The amount partially paid per share cannot be less than the original amount (' . $share['sync_paid'] . ' ' . $share['currency'] . ')');
                }
            }
            if ($partPaid > ($share['amount_paid'] + $share['amount_unpaid'])) {
                throw new Exception('Amount paid per share cannot be more than the share value.');
            }
            /* the rest */
            $data['amount_unpaid']  = ($share['amount_paid'] + $share['amount_unpaid']) - $partPaid;
            /* part paid */
            $data['amount_paid']    = $partPaid;
            $data['paid']           = $partPaid;
        } else {
            if ($paid == 'unpaid' && $share['sync_paid'] == 'paid') {
                throw new Exception('You cannot \'unpaid shares\'');
            }
            if ($paid == 'paid') {
                $data['amount_paid']    = $share['amount_paid'] + $share['amount_unpaid'];
                $data['amount_unpaid']  = 0;
                $data['paid']           = "paid";
            } else {
                $data['amount_unpaid']  = $share['amount_paid'] + $share['amount_unpaid'];
                $data['amount_paid']    = 0;
                $data['paid']           = "unpaid";
            }
        }
        $data['share_class'] = $shareClass;

        /* available share classes, share class is key so we don't have duplicates */
        $shareClasses = [];
        foreach ($this->shares as $sh) {
            $shareClasses[$sh['share_class']] = 1;
        }

        /* remove share classes (not in share caplital) from the sheroholdings */
        foreach ($this->shareholdings as $shareholding) {

            /* if share class is not in share capital - delete it */
            if (!isset($shareClasses[$shareholding->getShareClass()])) {
                $shareholding->setShareClass(NULL);
                $shareholding->save();
            }
        }

        CHFiling::getDb()->update('ch_annual_return_shares', $data, 'annual_return_shares_id = '.$id);
    }

    /**
     * Updates the whole annual return, updates all tables according to values
     * passed in $data array.
     *
     * @param array $data - columns and fields from db
     * @return CHAnnualReturn
     */
    public static function getUpdatedAnnualReturn(array $data)
    {

        /* officers */
        if (isset($data['officers'])) {
            $officers = $data['officers'];
            foreach ($officers as $officer) {
                $id = $officer['annual_return_officer_id'];
                unset($officer['annual_return_officer_id']);
                CHFiling::getDb()->update('ch_annual_return_officer', $officer, 'annual_return_officer_id = '.$id);
            }
            unset($data['officers']);
        }
        /* shares */
        if (isset($data['shares'])) {
            $shares = $data['shares'];
            foreach ($shares as $share) {
                $id = $share['annual_return_shares_id'];
                unset($share['annual_return_shares_id']);
                CHFiling::getDb()->update('ch_annual_return_shares', $share, 'annual_return_shares_id = '.$id);
            }
            unset($data['shares']);
        }
        /* shareholdings */

        if (isset($data['shareholdings'])) {
            $shareholdings = $data['shareholdings'];
            foreach ($shareholdings as $shareholding) {

                /* transfers */
                foreach ($shareholding['transfers'] as $transfer) {
                    $id = $transfer['annual_return_transfer_id'];
                    unset($transfer['annual_return_transfer_id']);
                    CHFiling::getDb()->update('ch_annual_return_transfer', $transfer, 'annual_return_transfer_id = '.$id);
                }
                unset($shareholding['transfers']);

                /* shareholders */
                foreach ($shareholding['shareholders'] as $shareholder) {
                    $id = $shareholder['annual_return_shareholder_id'];
                    unset($shareholder['annual_return_shareholder_id']);
                    CHFiling::getDb()->update('ch_annual_return_shareholder', $shareholder, 'annual_return_shareholder_id = '.$id);
                }
                unset($shareholding['shareholders']);

                /* shareholding */
                if (isset($shareholding['annual_return_shareholding_id'])) {
                    $id = $shareholding['annual_return_shareholding_id'];
                    unset($shareholding['annual_return_shareholding_id']);
                    CHFiling::getDb()->update('ch_annual_return_shareholding', $shareholding, 'annual_return_shareholding_id = '.$id);
                }
            }
            unset($data['shareholdings']);
        }

        /* annual return */
        $id = $data['form_submission_id'];
        unset($data['form_submission_id']);
        CHFiling::getDb()->update(self::$tableName, $data, 'form_submission_id = '.$id);

        /* tables are updated so return new instance */
        return new self($id);
    }

    /**
     * returns false, if annual return is not finished - is not ready for
     * submission
     *
     * @return boolean
     */
    public function isComplete()
    {

        if ($this->memebersList == NULL) {
            return FALSE;
        }

        //        foreach ($this->officers as $officer) {
        //            if (!$officer['eea_complete']) {
        //               	return false;
        //            }
        //        }

        return TRUE;
    }

    /**
     * Saves siccodes and made up date, nothing else.
     */
    public function save()
    {

        $data = [];
        $data['pscs_pre_added']  = $this->hasPreAddedPscs;
        $data['no_psc_reason']  = $this->noPscReason;
        $data['sic_code_type']  = $this->sicCodeType;
        $data['sic_code']       = $this->sicCode;
        $data['made_up_date']   = $this->madeUpDate;
        $data['trading']        = $this->trading;
        $data['dtr5']           = $this->dtr5;
        $registerOffice = $this->getRegOffice();
        $data['premise']        = $registerOffice->getPremise();
        $data['street']         = $registerOffice->getStreet();

        CHFiling::getDb()->update(self::$tableName, $data, 'form_submission_id = '.$this->formSubmissionId);
    }

    /**
     *
     * @return xml
     */
    public function getXml()
    {

        if (!$this->isComplete()) {
            throw new Exception("Annual return is missing some information");
        }

        /* sic codes */
        //$sicTag = ($this->getSicCodeType() == 'code') ? 'SICCode' : 'SICText';
        $sicCodes = '';
        foreach ($this->getSicCodes() as $code) {
            if (!empty($code)) {
                $sicCodes .= '<SICCode>'.htmlspecialchars($code, ENT_NOQUOTES).'</SICCode>';
            }
        }
        $sicCodes ='<SICCodes>'.$sicCodes.'</SICCodes>';

        /* address fields */
        $add= [
            'Premise'   => 'premise',
            'Street'    => 'street',
            'Thoroughfare'=> 'thoroughfare',
            'PostTown'  => 'post_town',
            'County'    => 'county',
            'Country'   => 'country',
            'Postcode'  => 'postcode',
            'CareOfName'=> 'care_of_name',
            'PoBox'     => 'po_box'
        ];

        /* this countries can be in country tag, the rest is other foreign country */
        /* don't ask me why - ask mother fuckers @ companies house */
        $country = ["USA" => 1, "IRL" => 1, "DEU" => 1, "FRA" => 1,
            "ITA" => 1, "ESP" => 1, "PRT" => 1, "NLD" => 1, "POL" => 1,
            "BEL" => 1, "NOR" => 1, "SWE" => 1, "DNK" => 1, "AUS" => 1,
            "NZL" => 1, "CAN" => 1, "ZAF" => 1, "AUT" => 1, "HRV" => 1,
            "CYP" => 1, "CZE" => 1, "EST" => 1, "HUN" => 1, "GRC" => 1,
            "LTU" => 1, "GBR" => 1, "GB-ENG" => 1, "GB-WLS" => 1,
            "GB-SCT" => 1, "GB-NIR" => 1];

        /* registered office address */
        $address = '';
        $addressFields = $this->regOffice->getFields();
        foreach ($add as $k => $v) {
            if (isset($addressFields[$v]) && !empty($addressFields[$v])) {
                $address .= '<'.$k.'>'.htmlspecialchars($addressFields[$v], ENT_NOQUOTES).'</'.$k.'>';
            }
        }

        /* sailAddress */
        $sailAddress = '';
        $addressFields = $this->sailAddress->getFields();

        /* check if they have sail address */
        if (isset($addressFields['premise']) && !empty($addressFields['premise'])) {
            $sailAddress .= '<SailAddress>';
            foreach ($add as $k => $v) {
                if (isset($addressFields[$v]) && !empty($addressFields[$v])) {
                    $sailAddress .= '<'.$k.'>'.htmlspecialchars($addressFields[$v], ENT_NOQUOTES).'</'.$k.'>';
                }
            }
            $sailAddress .= '</SailAddress>';
        }

        /* sail records */
        $sr = $this->sailRecords;
        $sailRecords = '';
        if (isset($sr) && !empty($sr)) {
            $sr = explode(";", $sr);
            foreach ($sr as $r) {
                $sailRecords .= '<SailRegisterList><RecordType>'.$r.'</RecordType></SailRegisterList>';
            }
        }

        /* officers */
        $officers = '';
        foreach ($this->officers as $officer) {
            if ($officer['type'] == 'PSC') {
                continue;
            }

            /* encode values */
            foreach ($officer as $key => $val) {
                $officer[$key] = htmlspecialchars($val, ENT_NOQUOTES);
            }

            if ($officer['type'] == 'DIR') {
                $type = 'Director';
            } elseif ($officer['type'] == 'MEM') {
                $type = 'Member';
            } else {
                $type = 'Secretary';
            }
            $corporate = ($officer['corporate']) ? 'Corporate' : 'Person';

            $officers .= '<Officer>';
            $officers .= '<'.$type.'>';

            if ($officer['type'] == 'MEM') {
                if ($officer['designated_ind'] == 1) {
                    $desigmation = '<DesignatedInd>true</DesignatedInd>';
                } else {
                    $desigmation = '<DesignatedInd>false</DesignatedInd>';
                }
                $officers .= $desigmation;
            }
            $officers .= '<'.$corporate.'>';

            if (!$officer['corporate']) {
                $officers .= (isset($officer['title']) && !empty($officer['title'])) ? '<Title>'.$officer['title'].'</Title>': '';
                $officers .= (isset($officer['forename']) && !empty($officer['forename'])) ? '<Forename>'.$officer['forename'].'</Forename>': '';
                $officers .= (isset($officer['middle_name']) && !empty($officer['middle_name'])) ? '<OtherForenames>'.$officer['middle_name'].'</OtherForenames>': '';
                $officers .= '<Surname>'.$officer['surname'].'</Surname>';
            } else {
                $officers .= '<CorporateName>'.$officer['corporate_name'].'</CorporateName>';
            }

            if (!$officer['corporate']) {
                $officers .= '<ServiceAddress>';
            }
            if (isset($officer['same_as_reg_office']) && !empty($officer['same_as_reg_office'])) {
                $officers .= '<SameAsRegisteredOffice>true</SameAsRegisteredOffice>';
            } else {
                $officers .= '<Address>';
                foreach ($add as $k => $v) {
                    if (isset($officer[$v]) && !empty($officer[$v])) {
                        if ($k == 'Country') {
                            if (!isset($country[$officer[$v]])) {
                                $k = 'OtherForeignCountry';
                            }
                            $officers .= '<'.$k.'>'.$officer[$v].'</'.$k.'>';
                        } else {
                            $officers .= '<'.$k.'>'.$officer[$v].'</'.$k.'>';
                        }
                    }
                }
                $officers .= '</Address>';
            }
            if (!$officer['corporate']) {
                $officers .= '</ServiceAddress>';
            }

            // TODO - previous names - it's forename and surname

            if ($officer['type'] == 'DIR' && !$officer['corporate']) {
                $officers .= '<DOB>'.$officer['dob'].'</DOB>';
                $officers .= '<Nationality>'.$officer['nationality'].'</Nationality>';
                $officers .= '<Occupation>'.$officer['occupation'].'</Occupation>';
                $officers .= '<CountryOfResidence>'.$officer['country_of_residence'].'</CountryOfResidence>';
            }
            if ($officer['type'] == 'MEM' && !$officer['corporate']) {
                $officers .= '<DOB>'.$officer['dob'].'</DOB>';
                $officers .= '<CountryOfResidence>'.$officer['country_of_residence'].'</CountryOfResidence>';
            }

            if ($officer['corporate']) {
                $officers .= '<CompanyIdentification><'.$officer['identification_type'].'>';
                $officers .= (isset($officer['place_registered']) && !empty($officer['place_registered']))
                    ? '<PlaceRegistered>'. $officer['place_registered'] .'</PlaceRegistered>'
                    : '';
                $officers .= (isset($officer['registration_number']) && !empty($officer['registration_number']))
                    ? '<RegistrationNumber>'. $officer['registration_number'] .'</RegistrationNumber>'
                    : '';
                $officers .= (isset($officer['law_governed']) && !empty($officer['law_governed']))
                    ? '<LawGoverned>'. $officer['law_governed'] .'</LawGoverned>'
                    : '';
                $officers .= (isset($officer['legal_form']) && !empty($officer['legal_form']))
                    ? '<LegalForm>'. $officer['legal_form'] .'</LegalForm>'
                    : '';
                $officers .= '</'.$officer['identification_type'].'></CompanyIdentification>';
            }

            $officers .= '</'.$corporate.'>';
            $officers .= '</'.$type.'>';
            $officers .= '</Officer>';
        }

        /* capital */
        $capitals = [];
        /* sort capitals by currency */
        foreach ($this->shares as $v) {
            $capitals[$v['currency']][] = $v;
        }

        //pr($capitals);exit;

        $capital = '';
        /* $k = currency, $v = Shares object */
        foreach ($capitals as $k => $v) {
            // create shares
            $shares = '';
            $totalNominalValue  = 0;
            $totalNumberOfShares= 0;
            $totalAmountUnpaid = 0;
            foreach ($v as $shareObject) {
                $share = $shareObject;
                $numberOfShares = $share['num_shares'];
                $amountPaid     = $share['amount_paid'];
                $amountUnpaid   = $share['amount_unpaid'];
                $shareValue     = $amountPaid + $amountUnpaid;
                $nominalValue   = $shareValue * $numberOfShares;

                $totalNominalValue += $nominalValue;
                $totalNumberOfShares += $numberOfShares;
                $totalAmountUnpaid += $amountUnpaid;

                $shares .= '<Shares>
                    <ShareClass>'. $share['share_class'] .'</ShareClass>
                    <PrescribedParticulars>'. $share['prescribed_particulars'] .'</PrescribedParticulars>
                    <NumShares>'. $numberOfShares.'</NumShares>
                    <AggregateNominalValue>'. sprintf('%f', $nominalValue) .'</AggregateNominalValue>
                    </Shares>';
            }

            // create share for specific statement of capital
            $capital .= '<Capital>';
            $capital .= '<TotalAmountUnpaid>'.$totalAmountUnpaid.'</TotalAmountUnpaid>';
            $capital .= '<TotalNumberOfIssuedShares>'.$totalNumberOfShares.'</TotalNumberOfIssuedShares>';
            $capital .= '<ShareCurrency>'.$k.'</ShareCurrency>';
            $capital .= '<TotalAggregateNominalValue>'.sprintf('%f', $totalNominalValue).'</TotalAggregateNominalValue>';
            $capital .= $shares;
            $capital .= '</Capital>';

        }
        // BYGUAR doesn't have capital
        if (!empty($capital)) {
            $capital = '<StatementOfCapital>' . $capital . '</StatementOfCapital>';
        }
        //echo $capital;exit;

        /* shareholdings */
        $shareholdings = '';
        foreach ($this->shareholdings as $v) {
            $shareholdings .= $v->getXml();
        }

        // $this->madeUpDate =     '2011-10-01';
        $memberlist = $this->memebersList;
        if (strtotime($this->madeUpDate) >= strtotime('2011-10-01')) {
            if ($this->trading == 1 && $this->dtr5 == 1) {
                //change member list to None
                $memberlist = 'NONE';
                // remove shareholding if trading and dtr5 true
                $shareholdings = '';
            }
        }
        // checking plc and date before or after 2011/10/01
        $trading = '';

        // just plc before 01/10/2011
        if ($this->companyCategory == 'PLC' && strtotime($this->madeUpDate) < strtotime('2011-10-01')) {
            $trading.=($this->trading) ? '<TradingOnRegulatedMarket>true</TradingOnRegulatedMarket>' : '<TradingOnRegulatedMarket>false</TradingOnRegulatedMarket>';
        } elseif (strtotime($this->madeUpDate) >= strtotime('2011-10-01')) {
            // all companies after 01/10/2011
            if ($this->companyCategory != 'BYGUAR') {
                $trading.=($this->trading) ? '<TradingOnMarket>true</TradingOnMarket>' : '<TradingOnMarket>false</TradingOnMarket>';
                $trading.=($this->dtr5) ? '<DTR5Applies>true</DTR5Applies>' : '<DTR5Applies>false</DTR5Applies>';
            }
        }// not plc before 01/10/2011
        //elseif ($this->companyCategory != 'PLC' && strtotime($this->madeUpDate) > strtotime('2011-10-01')) {
            ////nothing to do for now :)
        //}

        if ($this->companyCategory == 'LLP') {
            $trading = '';
            $capital = '';
            $shareholdings = '';
            $memberlist = 'NONE';
            $sicCodes = '';
        }

        if (Feature::featurePsc() && new DateTime($this->madeUpDate) >= new DateTime('2016-06-30')) {
            $today = date('Y-m-d');
            $notifications = '';
            $changes = '';
            $cessations = '';

            if ($this->getNoPscReason()) {
                $pscs = array_merge(
                    $this->getPersonPscs(),
                    $this->getLegalPersonPscs(),
                    $this->getCorporatePscs()
                );

                foreach ($pscs as $psc) {
                    /** @var AnnualReturnLegalPersonPsc|AnnualReturnPersonPsc|AnnualReturnCorporatePsc $psc */
                    $psc->setCessationDate(new DateTime);
                    $psc->save();
                }
            }

            foreach ($this->getCorporatePscs() as $corporatePsc) {
                $pscAddress = $corporatePsc->getAddress();
                $pscIdentification = $corporatePsc->getIdentification();
                $pscNatureOfControls = Collection::from($corporatePsc->getNatureOfControl()->getFields())
                    ->filter()
                    ->map(function ($natureOfControl) {
                        return "<NatureOfControl>{$natureOfControl}</NatureOfControl>";
                    })
                    ->toString();

                if ($corporatePsc->getCessationDate()) {
                    $cessations .= "
                        <Cessation>
                            <Corporate>
                                <CorporateName>{$corporatePsc->getCorporate()->getCorporateName()}</CorporateName>
                                <CessationDate>{$corporatePsc->getCessationDate()->format('Y-m-d')}</CessationDate>
                            </Corporate>
                        </Cessation>
                    ";
                } elseif ($corporatePsc->isChanged() && $corporatePsc->getDateOfChange()) {
                    $original = $corporatePsc->withOriginalData();
                    $change = '';

                    $originalPscAddress = $original->getAddress();
                    if ($originalPscAddress != $pscAddress) {
                        $countryElement = in_array($pscAddress->getCountry(), array_keys($country))
                            ? "<Country>{$pscAddress->getCountry()}</Country>"
                            : "<OtherForeignCountry>{$pscAddress->getCountry()}</OtherForeignCountry>";

                        $change .= "
                            <Address>
                                <Premise>{$pscAddress->getPremise()}</Premise>
                                <Street>{$pscAddress->getStreet()}</Street>
                                <PostTown>{$pscAddress->getPostTown()}</PostTown>
                                {$countryElement}
                            </Address>
                        ";
                    }

                    $originalPscIdentification = $original->getIdentification();
                    if ($pscIdentification != $originalPscIdentification) {
                        $change .= "
                            <PSCCompanyIdentification>
                                <LawGoverned>{$originalPscIdentification->getLawGoverned()}</LawGoverned>
                                <LegalForm>{$originalPscIdentification->getLegalForm()}</LegalForm>
                            </PSCCompanyIdentification>
                        ";
                    }

                    $originalPscNatureOfControl = $original->getNatureOfControl();
                    if ($corporatePsc->getNatureOfControl() != $originalPscNatureOfControl) {
                        $noc = Collection::from($originalPscNatureOfControl->getDBFields())
                            ->filter()
                            ->map(function ($natureOfControl) {
                                return "<NatureOfControl>{$natureOfControl}</NatureOfControl>";
                            })
                            ->toString();

                        $change .= "<NatureOfControls>{$noc}</NatureOfControls>";
                    }

                    $changes .= "
                        <Change>
                            <Corporate>
                                <CorporateName>{$original->getCorporate()->getCorporateName()}</CorporateName>
                                <Change>
                                     {$change}   
                                </Change>
                            </Corporate>
                            <DateOfChange>{$corporatePsc->getDateOfChange()->format('Y-m-d')}</DateOfChange>
                        </Change>
                    ";
                } else {
                    $countryElement = in_array($pscAddress->getCountry(), array_keys($country))
                        ? "<Country>{$pscAddress->getCountry()}</Country>"
                        : "<OtherForeignCountry>{$pscAddress->getCountry()}</OtherForeignCountry>";

                    $notifications .= "
                        <Notification>
                            <Corporate>
                                <CorporateName>{$corporatePsc->getCorporate()->getCorporateName()}</CorporateName>
                                <Address>
                                    <Premise>{$pscAddress->getPremise()}</Premise>
                                    <Street>{$pscAddress->getStreet()}</Street>
                                    <PostTown>{$pscAddress->getPostTown()}</PostTown>
                                    {$countryElement}
                                </Address>
                                <PSCCompanyIdentification>
                                    <LawGoverned>{$pscIdentification->getLawGoverned()}</LawGoverned>
                                    <LegalForm>{$pscIdentification->getLegalForm()}</LegalForm>
                                </PSCCompanyIdentification>
                            </Corporate>
                            <NatureOfControls>
                                {$pscNatureOfControls}
                            </NatureOfControls>
                            <NotificationDate>
                                " . ($corporatePsc->getNotificationDate() ? $corporatePsc->getNotificationDate()->format('Y-m-d') : $today) . "
                            </NotificationDate>
                        </Notification>
                    ";
                }
            }

            foreach ($this->getPersonPscs() as $personPsc) {
                $personResidentialAddress = $personPsc->getResidentialAddress();
                $personServiceAddress = $personPsc->getAddress();
                $pscNatureOfControls = Collection::from($personPsc->getNatureOfControl()->getFields())
                    ->filter()
                    ->map(function ($natureOfControl) {
                        return "<NatureOfControl>{$natureOfControl}</NatureOfControl>";
                    })
                    ->toString();

                $person = $personPsc->getPerson();
                $personDateOfBirth = new DateTime($person->getDob());

                if ($personPsc->getCessationDate()) {
                    $cessations .= "
                        <Cessation>
                            <Corporate>
                                <Individual>
                                    <Surname>{$person->getForename()}</Surname>
                                    <Forename>{$person->getSurname()}</Forename>
                                    <PartialDOB>
                                        <Month>{$personDateOfBirth->format('m')}</Month>
                                        <Year>{$personDateOfBirth->format('Y')}</Year>
                                    </PartialDOB>
                                </Individual>
                                <CessationDate>{$personPsc->getCessationDate()->format('Y-m-d')}</CessationDate>
                            </Corporate>
                        </Cessation>
                    ";
                } elseif (false && $personPsc->isChanged()) {
                    $original = $personPsc->withOriginalData();
                    $originalPerson = $original->getPerson();
                    $originalPersonDateOfBirth = new DateTime($originalPerson->getDob());
                    $change = '';

                    if ($originalPerson->getTitle() != $person->getTitle()) {
                        $change .= "<Title>{$person->getTitle()}</Title>";
                    }

                    if ($originalPerson->getForename() != $person->getForename()) {
                        $change .= "<Forename>{$person->getForename()}</Forename>";
                    }

                    if ($originalPerson->getSurname() != $person->getSurname()) {
                        $change .= "<Surname>{$person->getSurname()}</Surname>";
                    }

                    if ($originalPerson->getDob() != $person->getDob()) {
                        $change .= "<DOB>{$person->getDob()}</DOB>";
                    }

                    if ($originalPerson->getNationality() != $person->getNationality()) {
                        $change .= "<Nationality>{$person->getNationality()}</Nationality>";
                    }

                    if ($originalPerson->getCountryOfResidence() != $person->getCountryOfResidence()) {
                        $change .= "<CountryOfResidence>{$person->getCountryOfResidence()}</CountryOfResidence>";
                    }

                    $originalPscResidentialAddress = $original->getResidentialAddress();
                    if ($originalPscResidentialAddress != $personResidentialAddress) {
                        $countryElement = in_array($personResidentialAddress->getCountry(), array_keys($country))
                            ? "<Country>{$personResidentialAddress->getCountry()}</Country>"
                            : "<OtherForeignCountry>{$personResidentialAddress->getCountry()}</OtherForeignCountry>";

                        $change .= "
                            <ResidentialAddress>
                                <Address>
                                    <Premise>{$personResidentialAddress->getPremise()}</Premise>
                                    <Street>{$personResidentialAddress->getStreet()}</Street>
                                    <Thoroughfare>{$personResidentialAddress->getThoroughfare()}</Thoroughfare>
                                    <PostTown>{$personResidentialAddress->getPostTown()}</PostTown>
                                    {$countryElement}
                                </Address>
                            </ResidentialAddress>
                        ";
                    }

                    $originalPscServiceAddress = $original->getAddress();
                    if ($originalPscServiceAddress != $personServiceAddress) {
                        $newServiceAddress = '<SameAsRegisteredOffice>true</SameAsRegisteredOffice>';
                        if ($personServiceAddress && $personServiceAddress != $personResidentialAddress) {
                            $countryElement = in_array($personServiceAddress->getCountry(), array_keys($country))
                                ? "<Country>{$personServiceAddress->getCountry()}</Country>"
                                : "<OtherForeignCountry>{$personServiceAddress->getCountry()}</OtherForeignCountry>";

                            $newServiceAddress = "
                                <Address>
                                    <Premise>{$personServiceAddress->getPremise()}</Premise>
                                    <Street>{$personServiceAddress->getStreet()}</Street>
                                    <Thoroughfare>{$personServiceAddress->getThoroughfare()}</Thoroughfare>
                                    <PostTown>{$personServiceAddress->getPostTown()}</PostTown>
                                    {$countryElement}
                                </Address>
                            ";
                        }

                        $change .= "<ServiceAddress>{$newServiceAddress}</ServiceAddress>";
                    }

                    $originalPscNatureOfControl = $original->getNatureOfControl();
                    if ($personPsc->getNatureOfControl() != $originalPscNatureOfControl) {
                        $noc = Collection::from($originalPscNatureOfControl->getDBFields())
                            ->filter()
                            ->map(function ($natureOfControl) {
                                return "<NatureOfControl>{$natureOfControl}</NatureOfControl>";
                            })
                            ->toString();

                        $change .= "<NatureOfControls>{$noc}</NatureOfControls>";
                    }

                    $changes .= "
                        <Change>
                            <Individual>
                                <Title>{$originalPerson->getTitle()}</Title>
                                <Surname>{$originalPerson->getSurname()}</Surname>
                                <Forename>{$originalPerson->getForename()}</Forename>
                                <PartialDOB>
                                    <Month>{$originalPersonDateOfBirth->format('m')}</Month>
                                    <Year>{$originalPersonDateOfBirth->format('Y')}</Year>
                                </PartialDOB>
                                <Change>{$change}</Change>
                            </Individual>
                            <DateOfChange>{$personPsc->getDateOfChange()->format('Y-m-d')}</DateOfChange>
                        </Change>
                    ";
                } else {
                    $serviceAddress = "<SameAsRegisteredOffice>true</SameAsRegisteredOffice>";
                    if ($personPsc->getAddress() && $personPsc->getAddress() != $personResidentialAddress) {
                        $countryElement = in_array($personPsc->getAddress()->getCountry(), array_keys($country))
                            ? "<Country>{$personPsc->getAddress()->getCountry()}</Country>"
                            : "<OtherForeignCountry>{$personPsc->getAddress()->getCountry()}</OtherForeignCountry>";

                        $serviceAddress = "
                            <Address>
                                <Premise>{$personPsc->getAddress()->getPremise()}</Premise>
                                <Street>{$personPsc->getAddress()->getStreet()}</Street>
                                <Thoroughfare>{$personPsc->getAddress()->getThoroughfare()}</Thoroughfare>
                                <PostTown>{$personPsc->getAddress()->getPostTown()}</PostTown>
                                {$countryElement}
                            </Address>
                        ";
                    }

                    $countryElement = in_array($personResidentialAddress->getCountry(), array_keys($country))
                        ? "<Country>{$personResidentialAddress->getCountry()}</Country>"
                        : "<OtherForeignCountry>{$personResidentialAddress->getCountry()}</OtherForeignCountry>";

                    $notifications .= "
                        <Notification>
                            <Individual>
                                <Title>{$person->getTitle()}</Title>
                                <Forename>{$person->getForename()}</Forename>
                                <Surname>{$person->getSurname()}</Surname>
                                <ServiceAddress>{$serviceAddress}</ServiceAddress>
                                <DOB>{$person->getDob()}</DOB>
                                <Nationality>{$person->getNationality()}</Nationality>
                                <CountryOfResidence>{$person->getCountryOfResidence()}</CountryOfResidence>
                                <ResidentialAddress>
                                    <Address>
                                        <Premise>{$personResidentialAddress->getPremise()}</Premise>
                                        <Street>{$personResidentialAddress->getStreet()}</Street>
                                        <Thoroughfare>{$personResidentialAddress->getThoroughfare()}</Thoroughfare>
                                        <PostTown>{$personResidentialAddress->getPostTown()}</PostTown>
                                        {$countryElement}
                                    </Address>
                                </ResidentialAddress>
                            </Individual>
                            <NatureOfControls>{$pscNatureOfControls}</NatureOfControls>
                            <NotificationDate>
                                " . ($personPsc->getNotificationDate() ? $personPsc->getNotificationDate()->format('Y-m-d') : $today) . "
                            </NotificationDate>
                        </Notification>
                    ";
                }
            }

            foreach ($this->getLegalPersonPscs() as $legalPersonPsc) {
                $pscAddress = $legalPersonPsc->getAddress();
                $pscIdentification = $legalPersonPsc->getIdentification();
                $pscNatureOfControls = Collection::from($legalPersonPsc->getNatureOfControl()->getFields())
                    ->filter()
                    ->map(function ($natureOfControl) {
                        return "<NatureOfControl>{$natureOfControl}</NatureOfControl>";
                    })
                    ->toString();

                if ($legalPersonPsc->getCessationDate()) {
                    $cessations .= "
                        <Cessation>
                            <LegalPerson>
                                <LegalPersonName>{$legalPersonPsc->getCorporate()->getCorporateName()}</LegalPersonName>
                                <CessationDate>{$today}</CessationDate>
                            </LegalPerson>
                        </Cessation>
                    ";
                } elseif ($legalPersonPsc->isChanged() && $legalPersonPsc->getDateOfChange()) {
                    $original = $legalPersonPsc->withOriginalData();
                    $change = '';

                    $originalPscAddress = $original->getAddress();
                    if ($originalPscAddress != $pscAddress) {
                        $countryElement = in_array($pscAddress->getCountry(), array_keys($country))
                            ? "<Country>{$pscAddress->getCountry()}</Country>"
                            : "<OtherForeignCountry>{$pscAddress->getCountry()}</OtherForeignCountry>";

                        $change .= "
                            <Address>
                                <Premise>{$pscAddress->getPremise()}</Premise>
                                <Street>{$pscAddress->getStreet()}</Street>
                                <PostTown>{$pscAddress->getPostTown()}</PostTown>
                                {$countryElement}
                            </Address>
                        ";
                    }

                    $originalPscIdentification = $original->getIdentification();
                    if ($pscIdentification != $originalPscIdentification) {
                        $change .= "
                            <PSCCompanyIdentification>
                                <LawGoverned>{$originalPscIdentification->getLawGoverned()}</LawGoverned>
                                <LegalForm>{$originalPscIdentification->getLegalForm()}</LegalForm>
                            </PSCCompanyIdentification>
                        ";
                    }

                    $originalPscNatureOfControl = $original->getNatureOfControl();
                    if ($legalPersonPsc->getNatureOfControl() != $originalPscNatureOfControl) {
                        $noc = Collection::from($originalPscNatureOfControl->getDBFields())
                            ->filter()
                            ->map(function ($natureOfControl) {
                                return "<NatureOfControl>{$natureOfControl}</NatureOfControl>";
                            })
                            ->toString();

                        $change .= "<NatureOfControls>{$noc}</NatureOfControls>";
                    }

                    $changes .= "
                        <Change>
                            <LegalPerson>
                                <LegalPersonName>{$original->getCorporate()->getCorporateName()}</LegalPersonName>
                                <Change>
                                     {$change}   
                                </Change>
                            </LegalPerson>
                            <DateOfChange>{$today}</DateOfChange>
                        </Change>
                    ";
                } else {
                    $countryElement = in_array($pscAddress->getCountry(), array_keys($country))
                        ? "<Country>{$pscAddress->getCountry()}</Country>"
                        : "<OtherForeignCountry>{$pscAddress->getCountry()}</OtherForeignCountry>";

                    $notifications .= "
                        <Notification>
                            <LegalPerson>
                                <LegalPersonName>{$legalPersonPsc->getCorporate()->getCorporateName()}</LegalPersonName>
                                <Address>
                                    <Premise>{$pscAddress->getPremise()}</Premise>
                                    <Street>{$pscAddress->getStreet()}</Street>
                                    <PostTown>{$pscAddress->getPostTown()}</PostTown>
                                    {$countryElement}
                                </Address>
                                <PSCCompanyIdentification>
                                    <LawGoverned>{$pscIdentification->getLawGoverned()}</LawGoverned>
                                    <LegalForm>{$pscIdentification->getLegalForm()}</LegalForm>
                                </PSCCompanyIdentification>
                            </LegalPerson>
                            <NatureOfControls>
                                {$pscNatureOfControls}
                            </NatureOfControls>
                            <NotificationDate>
                                {$today}
                            </NotificationDate>
                        </Notification>
                    ";
                }
            }

            $notifications = $notifications ? "<Notifications>$notifications</Notifications>" : '';
            $changes = $changes ? "<Changes>$changes</Changes>" : '';
            $cessations = $cessations ? "<Cessations>$cessations</Cessations>" : '';
            $pscs = "<PSCs>{$notifications}{$changes}{$cessations}</PSCs>";

            //todo: psc-change xmlbeta to xmlgw
            return '<ConfirmationStatement 
                xmlns="http://xmlgw.companieshouse.gov.uk" 
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
                xsi:schemaLocation="http://xmlgw.companieshouse.gov.uk http://xmlgw.companieshouse.gov.uk/v1-0/schema/forms/ConfirmationStatement-v1-0.xsd">'.
            $trading .
            '<ReviewDate>'.$this->madeUpDate.'</ReviewDate>'.
            $sicCodes.
            $capital.
            $shareholdings.
            $pscs.
            '<StateConfirmation>true</StateConfirmation>'.
            '</ConfirmationStatement>';
        }

        return '<AnnualReturn
            xsi:schemaLocation="http://xmlgw.companieshouse.gov.uk
            http://xmlgw.companieshouse.gov.uk/v1-0/schema/forms/AnnualReturn-v3-0.xsd"
            xmlns="http://xmlgw.companieshouse.gov.uk"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'.
            '<CompanyCategory>'.$this->companyCategory.'</CompanyCategory>'.
            $trading.
            '<MadeUpDate>'.$this->madeUpDate.'</MadeUpDate>'.
            $sicCodes.
            '<RegisteredOfficeAddress>'. $address .'</RegisteredOfficeAddress>'.
            $sailAddress .
            $sailRecords .
            $officers .
            $capital .
            '<TypeOfMembersList>'.$memberlist. '</TypeOfMembersList>' .
            $shareholdings.
        '</AnnualReturn>';
    }

    /**
     * @return string
     */
    public function getAnnualReturnFormId()
    {
        if (Feature::featurePsc() && new DateTime($this->madeUpDate) >= new DateTime('2016-06-30')) {
            return 'ConfirmationStatement';
        }

        return 'AnnualReturn';
    }

    /**
     * @param Company $company
     * @return string
     */
    public function send(Company $company)
    {
        return FormSubmission::getFormSubmission($company, $this->formSubmissionId)->sendRequest();
    }

    /**
     * @return bool
     */
    public function isSubmissionPending()
    {
        $submissionStatus = $this->getSubmissionStatus();

        return $submissionStatus == FormSubmissionEntity::RESPONSE_PENDING
            || $submissionStatus == FormSubmissionEntity::RESPONSE_INTERNAL_FAILURE;
    }


    /**
     * @return string
     */
    public function getNoPscReason()
    {
        return $this->noPscReason;
    }

    /**
     * @return string
     */
    public function setNoPscReason($reason)
    {
        return $this->noPscReason = $reason;
    }

    /**
     * @return AnnualReturnPersonPsc[]
     */
    public function getPersonPscs()
    {
        $personPscs = [];
        foreach ($this->officers as $officer) {
            if ($officer['corporate'] == 0 && $officer['type'] == 'PSC') {
                $personPscs[] = new AnnualReturnPersonPsc($this->formSubmissionId, $officer['annual_return_officer_id']);
            }
        };

        return $personPscs;
    }

    /**
     * @return AnnualReturnCorporatePsc[]
     */
    public function getCorporatePscs()
    {
        $corporatePscs = [];
        foreach ($this->officers as $officer) {
            if ($officer['corporate'] == 1 && $officer['type'] == 'PSC') {
                $corporatePscs[] = new AnnualReturnCorporatePsc($this->formSubmissionId, $officer['annual_return_officer_id']);
            }
        };

        return $corporatePscs;
    }

    /**
     * @return AnnualReturnLegalPersonPsc[]
     */
    public function getLegalPersonPscs()
    {
        $legalPersonPscs = [];
        foreach ($this->officers as $officer) {
            if ($officer['corporate'] == 2 && $officer['type'] == 'PSC') {
                $legalPersonPscs[] = new AnnualReturnLegalPersonPsc($this->formSubmissionId, $officer['annual_return_officer_id']);
            }
        };

        return $legalPersonPscs;
    }

    /**
     * @return boolean
     */
    public function hasPreAddedPscs()
    {
        return $this->hasPreAddedPscs;
    }

    /**
     * @param boolean $importedPscs
     */
    public function setPreAddedPscs($importedPscs)
    {
        $this->hasPreAddedPscs = $importedPscs;
    }
}
