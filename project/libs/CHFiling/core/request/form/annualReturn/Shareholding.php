<?php

class Shareholding extends Element 
{

    /**
     *
     * @var int 
     */
    private $shareholdingId;

    /**
     *
     * @var AnnualReturn
     */
    private $annualReturn;

    /**
     *
     * @var array<AnnualReturnTransfer>
     */
    private $transfers;

    /**
     *
     * @var array<AnnualReturnShareholder>
     */
    private $shareholders;

    /**
     *
     * @var string
     */
    private static $tableName = 'ch_annual_return_shareholding';
    
    /**
     *
     * @var string
     */
    private static $AnualReturnShareholderTable = 'ch_annual_return_shareholder';
    
    /**
     *
     * @param int $formSubmission Id
     */
    protected function  __construct(CHAnnualReturn $annualReturn, $shareholdingId = NULL)
    {

        $this->annualReturn = $annualReturn;
        $formSubmissionId = $annualReturn->getFormSubmissionId();

        // --- do not save anything in db ---
        if ($shareholdingId === NULL) {
            $this->shareholdingId = NULL;
            $this->shareholders = array();
            $this->transfers = array();
            parent::__construct($this->getDBFields());
            return;
        }

        $sql = "
			SELECT * FROM `".self::$tableName."`
			WHERE `annual_return_shareholding_id` = $shareholdingId
            AND `form_submission_id` = $formSubmissionId";
        $result = CHFiling::getDb()->fetchRow($sql);

        if (!$result) {
            throw new Exception('Record doesn\'t exist');
        }

        $this->shareholdingId = $shareholdingId;

        parent::__construct($this->getDBFields());

        // --- set instance variables ---
        $this->setFields($result);

        $this->shareholders = AnnualReturnShareholder::getAnnualReturnShareholders($this);
        $this->transfers    = AnnualReturnTransfer::getAnnualReturnTransfers($this);
    }

    /**
     *
     * @param AnnualRetur $annualReturn
     * @param int $shareholdingId
     * @return Shareholding
     */
    public static function getShareholding(CHAnnualReturn $annualReturn, $shareholdingId)
    {
        return new Shareholding($annualReturn, $shareholdingId);
    }

    /**
     *
     * @param CHAnnualReturn $annualReturn
     * @return Shareholding
     */
    public static function getNewShareholding(CHAnnualReturn $annualReturn)
    {
        return new Shareholding($annualReturn);
    }

    /**
     * Returns all Shareholdings that belong to the specified form submission, if
     * none found, empty array is returned.
     * Index is shareholding id.
     *
     * @param CHAnnualReturn $annualReturn
     * @return array<Shareholdings>
     */
    public static function getShareholdings(CHAnnualReturn $annualReturn)
    {

        $formSubmissionId = $annualReturn->getFormSubmissionId();

        $sql = "
			SELECT annual_return_shareholding_id FROM `".self::$tableName."`
			WHERE `form_submission_id` = $formSubmissionId";
        $result = CHFiling::getDb()->fetchAll($sql);

        if (empty($result)) {
            return array();
        }

        $shareholdings = array();
        foreach ($result as $id) {
            $shareholdingId = $id['annual_return_shareholding_id'];

            $shareholdings[$shareholdingId] =
                new Shareholding($annualReturn, $shareholdingId);
        }

        return $shareholdings;
    }

    /**
     * setFields
     *
     * key is column in db, value is value
     * prefix is column prefix - so for service street
     * you can use 'service_' = 'service_street'
     *
     * @param array $data
     * @param string $prefix
     * @access public
     * @return void
     */
    public function setFields(array $data, $prefix = NULL)
    {
        parent::setFields($data, $prefix);
    }

    /**
     * getDBFields
     *
     * allowed fields - fields from database
     * true indicates that the field is compulsory
     *
     * @access public
     * @return array
     */
    public function getDBFields()
    {
        return array(
            'share_class'   => 1,
            'number_held'   => 1,
            'sync'          => 1
        );
    }

    /**
     * Return form shareholding id
     *
     * @return int primary key from db
     */
    public function getId()
    {
        return $this->shareholdingId;
    }

    /**
     * Return form submission id
     *
     * @return int 
     */
    public function getFormSubmissionId()
    {
        return $this->annualReturn->getFormSubmissionId();
    }


    /**
     * setShareClass
     *
     * Pre-contition: $shareClass cannot be empty and
     * can be maximum 50 characters long
     *
     * @param string $shareClass
     * @access public
     * @return void
     */
    public function setShareClass($shareClass)
    {
        if (strlen($shareClass) > 50) {
            throw new Exception('Share class cannot be longer than 50 characters');
        }
        if ($shareClass != NULL) {  // CHAnnual return has to be able to null share class (MFs don't send share classes for share capital)
            if ($this->isEmpty($shareClass)) {
                throw new Exception('Share class cannot be empty');
            }
        }
        $this->fields['share_class'] = $shareClass;
    }

    public function setSync($sync)
    {
        $this->fields['sync'] = ($sync) ? 1 : 0;
    }

    /**
     * getShareClass
     *
     * @access public
     * @return string
     */
    public function getShareClass()
    {
        return isset($this->fields['share_class']) ? $this->fields['share_class'] : NULL;
    }

    public function getNumberHeld() 
    {
        return isset($this->fields['number_held']) ? (int) $this->fields['number_held'] : 0;
    }

    /**
     * @return array
     */
    public function getAvailableShareClasses()
    {
        $shares = $this->annualReturn->getShares();
        $shareClass = array();
        foreach ($shares as $v) {
            if (!isset($v['share_class'])) {
                continue;
            }
            $currency = !empty($v['currency']) ? $v['currency'] : 'GBP'; 
            $shareClass[$currency][$v['share_class']] = $v['share_class'];
        }
        return $shareClass;
    }

    /**
     * setNumberHeld
     *
     * Pre-condition: $numberHeld has to be positive integer
     *
     * @param int $numberHeld
     * @access public
     * @return void
     */
    public function setNumberHeld($numberHeld)
    {
        if ($numberHeld < 0) {
            throw new Exception('number held has to be more/equal to 0');
        }
        if (!preg_match('/^\d+$/', $numberHeld)) {
            throw new Exception('The number held has to be a whole number between 1 and 11 digits long.');
        }
        $this->fields['number_held'] = $numberHeld;
    }

    /**
     * Transfer has to be saved using save() method, when finished editing
     * 
     * @return AnnualReturnTransfer
     */
    public function getNewTransfer()
    {
        return AnnualReturnTransfer::getNewAnnualReturnTransfer($this);
    }

    /**
     * 
     * @return array<AnnualReturnTransfer> - transfers that belong to this 
     *                                       shareholding
     */
    public function getTransfers()
    {
        return $this->transfers;
    }

    /**
     * Returns AnnualReturnTransfer specified by id and that belongs to this
     * shareholding
     *
     * @param int $id
     * @return AnnualReturnTransfer
     */
    public function getTransfer($id)
    {

        if (isset($this->transfers[$id])) {
            return $this->transfers;
        }
        return NULL;
    }

    /**
     * 
     * @return true if this shareholding is from companies house/sync
     */
    public function isSynced()
    {
        return $this->fields['sync'];
    }

    /**
     * returns shares from share capital (for the same share class)
     *
     * @return int 
     */
    public function getAvailableShares()
    {

        /* check if number of share is not more than in the share capital */
        /* minus shareholdings */

        /* all shares */
        $shares = $this->annualReturn->getShares();
        $numShares = array();
        foreach ($shares as $share) {
            if (!isset($numShares[$share['share_class']])) {
                $numShares[$share['share_class']] = 0;
            }
            $numShares[$share['share_class']] += (int) $share['num_shares'];
        }

        /* all shareholdings */
        $numHeld = array();
        $shareholdings = $this->annualReturn->getShareholdings();
        foreach ($shareholdings as $shareholding) {
            if (!isset($numHeld[$shareholding->getShareClass()])) {
                $numHeld[$shareholding->getShareClass()] = 0;
            }
            $numHeld[$shareholding->getShareClass()] += (int) $shareholding->getNumberHeld();
        }

        /* calculate available shares */
        $available = array();
        foreach ($numShares as $k => $v) {
            if (isset($numHeld[$k])) {
                $available[$k] = $v - $numHeld[$k];
            } else {
                $available[$k] = $v;
            }
        }
        /*
        pr($numShares);
        pr($numHeld);
        pr($available);
        exit;
         */

        /* if share class is set then return available only for that class */
        if ($this->getShareClass()) {
            /* user is editing so add shares for this object as well */
            return $available[$this->getShareClass()] = $this->getNumberHeld();
        }

        $x = '';
        foreach ($available as $k => $v) {
            $x .= $k . ' = ' .$v . ' '; 
        }

        return $x;
    }

    public function save()
    {

        $numShares = $this->getAvailableShares();

        if ($this->getNumberHeld() > $numShares) {
            throw new Exception('You cannot hold more shares than specified in share capital');
        }

        // --- prepare data ---
        $data = $this->getFields();
        $data['form_submission_id'] = $this->annualReturn->getFormSubmissionId();
        /* sync cannot be null */
        if ($data['sync'] == NULL) {
            $data['sync'] = 0;
        }

        // --- save data ---
        if (isset($this->shareholdingId)) {
            CHFiling::getDb()->update(self::$tableName, $data, 'annual_return_shareholding_id = '.$this->shareholdingId);
        } else {
            CHFiling::getDb()->insert(self::$tableName, $data);
            $this->shareholdingId = CHFiling::getDb()->lastInsertId();
        }
        /* save transfers */
        //foreach($this->transfers as $transfer) {
            //$transfer->save();
        //}
        //[> save shareholders <]
        //foreach($this->shareholders as $shareholder) {
            //$shareholder->save();
        //}
    }

    public function remove()
    {
        CHFiling::getDb()->delete(self::$tableName, 'annual_return_shareholding_id = '.$this->shareholdingId);
        CHFiling::getDb()->delete(self::$AnualReturnShareholderTable, 'annual_return_shareholding_id = '.$this->shareholdingId);
    }

    /**
     * Shareholder has to be saved using save() method, when finished editing
     * 
     * @return AnnualReturnShareholder
     */
    public function getNewShareholder()
    {
        return AnnualReturnShareholder::getNewAnnualReturnShareholder($this);
    }

    /**
     *
     * @return array<AnnualReturnShareholder> - shareholders that belong to
     *                                          this shareholding
     */
    public function getShareholders()
    {
        return $this->shareholders;
    }

    /**
     * Returns AnnualReturnShareholder specified by id and that belongs to this
     * shareholding, if no shareholder found, null is returned
     *
     * @param int $id
     * @return AnnualReturnShareholder
     */
    public function getShareholder($id)
    {

        if (isset($this->shareholders[$id])) {
            return $this->shareholders[$id];
        }
        return NULL;
    }

    /**
     *
     * @return boolean 
     */
    public function isComplete()
    {
        $shareClass = $this->getShareClass();
        if (!isset($shareClass) || empty($shareClass)) {
            return FALSE;
        }
        $numberHeld = $this->getNumberHeld();
        if (!isset($numberHeld) || (empty($numberHeld) && $numberHeld !== 0 && $numberHeld !== '0')) {
            return FALSE;
        }
        return TRUE;
    }

    /**
     *
     * @return String - xml string representation of this shareholding 
     */
    public function getXml()
    {

        $transfers = '';
        foreach ($this->transfers as $transfer) {
            $transfers .= $transfer->getXml();
        }

        $shareholders = '';
        foreach ($this->shareholders as $shareholder) {
            $shareholders .= $shareholder->getXml();
        }

        return '<Shareholdings>'. 
            '<ShareClass>'.$this->getShareClass().'</ShareClass>'.
            '<NumberHeld>'.$this->getNumberHeld().'</NumberHeld>'.
            $transfers .
            $shareholders .
            '</Shareholdings>';
    }
}
