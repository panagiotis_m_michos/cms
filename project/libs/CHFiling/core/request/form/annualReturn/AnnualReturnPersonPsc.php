<?php

final class AnnualReturnPersonPsc extends PersonPsc
{
    /**
     * @var int
     */
    private $annualReturnOfficerId;

    /**
     *
     * @var int
     */
    private $formSubmissionId;

    /**
     *
     * @var string
     */
    private static $type = 'PSC';

    /**
     *
     * @var bool
     */
    private static $corporate = 0;

    /**
     *
     * @var string
     */
    private static $tableName = 'ch_annual_return_officer';

    /**
     * @var bool
     */
    private $alreadyExistingPsc = FALSE;

    /**
     * @var DateTime
     */
    private $cessationDate;

    /**
     * @var DateTime
     */
    private $notificationDate;

    /**
     * @var DateTime
     */
    private $dateOfChange;

    /**
     * @var array|null
     */
    private $originalData;

    /**
     * @param int $formSubmissionId
     * @param int $annualReturnOfficerId
     * @throws Exception
     */
    public function __construct($formSubmissionId, $annualReturnOfficerId = NULL)
    {
        if (!preg_match('/^\d+$/', $formSubmissionId)) {
            throw new Exception('form submission id has to be integer');
        }

        parent::__construct();

        if (!$annualReturnOfficerId) {
            $this->annualReturnOfficerId = NULL;
            $this->formSubmissionId = $formSubmissionId;

            return;
        }

        if (!preg_match('/^\d+$/', $annualReturnOfficerId)) {
            throw new Exception('annual return officer id has to be integer');
        }

        $sql = "SELECT * FROM `" . self::$tableName . "` WHERE `annual_return_officer_id` = $annualReturnOfficerId AND `form_submission_id` = $formSubmissionId";

        if (!$result = CHFiling::getDb()->fetchRow($sql)) {
            throw new Exception("Record doesn't exist");
        }

        $this->annualReturnOfficerId = $result['annual_return_officer_id'];
        $this->formSubmissionId = $result['form_submission_id'];
        $this->setFields($result);
    }

    public function setFields(array $data)
    {
        parent::setFields($data);
        $this->alreadyExistingPsc = (bool) $data['existing_officer'];
        $this->cessationDate = isset($data['cessation_date']) ? new DateTime($data['cessation_date']) : NULL;
        $this->notificationDate = isset($data['notification_date']) ? new DateTime($data['notification_date']) : NULL;
        $this->dateOfChange = isset($data['date_of_change']) ? new DateTime($data['date_of_change']) : NULL;
        $this->originalData = isset($data['before_changes']) ? json_decode($data['before_changes'], TRUE) : NULL;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return self::$type;
    }

    public function remove()
    {
        CHFiling::getDb()->delete(self::$tableName, 'annual_return_officer_id = ' . $this->annualReturnOfficerId);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->annualReturnOfficerId;
    }

    /**
     * @return bool
     */
    public function isComplete()
    {
        $allowedFields = [
            'surname', 'premise', 'street', 'post_town', 'country',
            'dob', 'nationality', 'country_of_residence',
            'residential_premise', 'residential_street', 'residential_post_town',
            'residential_country',
        ];

        $fields = array_merge(
            $this->getPerson()->getFields(),
            $this->getAddress()->getFields(),
            $this->getResidentialAddress()->getFields('residential_'),
            $this->getNatureOfControl()->getFields()
        );

        foreach ($allowedFields as $v) {
            if (!isset($fields[$v]) || $fields[$v] === NULL) {
                return 0;
            }
        }

        return 1;
    }

    /**
     * @throws Exception
     */
    public function save()
    {
        if (!$this->isComplete()) {
            throw new Exception('You need to set all compulsory fields');
        }

        $data = $this->getData();

        if (isset($this->annualReturnOfficerId) && $this->annualReturnOfficerId !== NULL) {
            CHFiling::getDb()->update(
                self::$tableName,
                $data,
                'annual_return_officer_id = ' . $this->annualReturnOfficerId
            );
        } else {
            $data['before_changes'] = json_encode($data);
            CHFiling::getDb()->insert(self::$tableName, $data);
            $this->annualReturnOfficerId = CHFiling::getDb()->lastInsertId();
        }
    }

    /**
     * @return array
     */
    public function getData()
    {
        $address = $this->getAddress()->getFields();
        unset($address['secure_address_ind']);
        $data = array_merge(
            $this->getPerson()->getFields(),
            $address,
            $this->getResidentialAddress()->getFields('residential_'),
            $this->getNatureOfControl()->getFields()
        );

        // --- these two fields are not for residential address ---
        unset($data['residential_care_of_name']);
        unset($data['residential_po_box']);

        $data['form_submission_id'] = $this->formSubmissionId;
        $data['corporate'] = self::$corporate;
        $data['type'] = self::$type;
        $data['consentToAct'] = $this->hasConsentToAct();
        $data['residential_secure_address_ind'] = FALSE;
        $data['existing_officer'] = $this->isAlreadyExistingPsc();
        $data['notification_date'] = $this->getNotificationDate() ? $this->getNotificationDate()->format('Y-m-d') : NULL;
        $data['cessation_date'] = $this->getCessationDate() ? $this->getCessationDate()->format('Y-m-d') : NULL;
        $data['date_of_change'] = $this->getDateOfChange() ? $this->getDateOfChange()->format('Y-m-d') : NULL;

        return $data;
    }

    /**
     * @return bool
     */
    public function isChanged()
    {
        return $this->getData() != $this->originalData;
    }

    /**
     * @return AnnualReturnPersonPsc
     */
    public function withOriginalData()
    {
        $cloned = clone $this;
        $cloned->setFields(array_merge($this->getData(), (array) $this->originalData));

        return $cloned;
    }

    /**
     * @return boolean
     */
    public function isAlreadyExistingPsc()
    {
        return $this->alreadyExistingPsc;
    }

    /**
     * @param boolean $alreadyExistingPsc
     */
    public function setAlreadyExistingPsc($alreadyExistingPsc)
    {
        $this->alreadyExistingPsc = $alreadyExistingPsc;
    }


    /**
     * @return DateTime
     */
    public function getCessationDate()
    {
        return $this->cessationDate;
    }

    /**
     * @param DateTime $cessationDate
     */
    public function setCessationDate(DateTime $cessationDate)
    {
        $this->cessationDate = $cessationDate;
    }

    /**
     * @return DateTime
     */
    public function getNotificationDate()
    {
        return $this->notificationDate;
    }

    /**
     * @param DateTime $notificationDate
     */
    public function setNotificationDate(DateTime $notificationDate)
    {
        $this->notificationDate = $notificationDate;
    }

    /**
     * @return DateTime
     */
    public function getDateOfChange()
    {
        return $this->dateOfChange;
    }

    /**
     * @param DateTime $dateOfChange
     */
    public function setDateOfChange(DateTime $dateOfChange)
    {
        $this->dateOfChange = $dateOfChange;
    }
}

