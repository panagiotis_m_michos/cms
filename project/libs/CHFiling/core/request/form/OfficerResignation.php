<?php

class OfficerResignation implements Form
{

    /**
     * @var int
     */
    private $formSubmissionId;

    /**
     * @var string
     */
    private $rejectDescription;

    /**
     * @var array
     */
    private $officer;
    
    /**
     * @var string
     */
    private $companyType;
    
    /**
     * @var string
     */
    private $resignDate;

    /**
     * @var string
     */
    private static $tableName = 'ch_officer_resignation';

    /**
     * @param int $formSubmissionId
     * @throws Exception
     */
    private function  __construct($formSubmissionId)
    {
        // --- check that formSubmissionId is int ---
        if (!preg_match('/^\d+$/', $formSubmissionId)) {
            throw new Exception('formSubmissionId has to be integer');
        }

        // --- load data from database ---
        $sql = "
            SELECT * FROM ".self::$tableName."
            WHERE `form_submission_id` = $formSubmissionId";
        if (!$result = CHFiling::getDb()->fetchRow($sql)) {
            throw new Exception('Officer resignation doesn\'t exist');
        }

        // --- set instance variables ---
        $this->formSubmissionId = $result['form_submission_id'];
        $this->rejectDescription= $result['reject_description'];

        // --- set fields for officer ---
        $officer['corporate'] = $result['corporate'];
        $officer['corporate_name'] = $result['corporate_name'];
        $officer['surname'] = $result['surname'];
        $officer['forename'] = $result['forename'];
        $officer['middle_name'] = $result['middle_name'];
        $officer['dob'] = $result['dob'];
        $officer['type'] = $result['type'];

        $this->resignDate = $result['resign_date'];
        $this->companyType = $result['company_type'];
        $this->officer = $officer;
    }

    /**
     *
     * @param int $formSubmissionId
     * @return CompanyIncorporation
     */
    public static function getOfficerResignation($formSubmissionId)
    {
        return new self($formSubmissionId);
    }

    /**
     * @param int $formSubmissionId
     * @return CompanyIncorporation
     * @throws Exception
     */
    public static function getNewOfficerResignation($formSubmissionId)
    {
        // --- check that formSubmissionId is int ---
        if (!preg_match('/^\d+$/', $formSubmissionId)) {
            throw new Exception('formSubmissionId has to be integer');
        }

        // --- insert new company incorporation ---
        $data = array('form_submission_id'=> $formSubmissionId);
        CHFiling::getDb()->insert(self::$tableName, $data);

        // --- return new instance ---
        return new self($formSubmissionId);
    }

    /**
     * checks if array fields has filled in all compulsory fields - supplied
     * as first argument
     *
     * @param array $compulsoryFields
     * @param array $fields
     * @return boolean
     */
    private function isComplete($compulsoryFields, $fields) 
    {
        // --- check if all fields are set ---
        foreach($compulsoryFields as $v) {
            if (!isset($fields[$v]) || is_null($fields[$v])) {
                return 0;
            }
        }
        return 1;
    }

    /**
     * sets new Registered office address that is going to be changed
     *
     * @param $officer
     * @throws Exception
     */
    public function setOfficer($officer)
    {
        
        $officerArray  = $officer->getFields();
        $officerFields['type'] = $officer->getType();
        $officerFields['corporate'] = $officer->isCorporate();
        
        // --- check fields ---
        if (!$this->isComplete(array('type', 'corporate'), $officerFields)) {
            throw new Exception('you need to set type and corporate');
        }

        // --- if it's corporate we need surname, forename, middle_name ---    
        if($officerFields['corporate'] == 0) {
            // --- if it's director we need dob  ---
            if ($officerFields['type'] == 'DIR') {
                $officerFields['dob'] = $officerArray['dob'];
                if (!isset($officerFields['dob']) || is_null($officerFields['dob'])) {
                        throw new Exception('you need to set dob for director');
                }
            }
            $officerFields['surname'] = $officerArray['surname'];
            $officerFields['forename'] = $officerArray['forename'];
            $officerFields['middle_name'] = $officerArray['middle_name'];
            
        }
        
         // --- if it's corporate we need corporate name ---
        if ($officerFields['corporate'] == 1) {
            $officerFields['corporate_name'] = $officerArray['corporate_name'];
            if (!isset($officerFields['corporate_name']) || is_null($officerFields['corporate_name'])) {
                throw new Exception('you need to set corporate name for corporate officer');
            }
        }

        CHFiling::getDb()->update(self::$tableName, $officerFields, 'form_submission_id = '.$this->formSubmissionId);
        $this->officer = $officerFields;
    }

    /**
     * @param string $resignDate
     */
    public function setResignDate($resignDate)
    {
        $fields = array('resign_date' => $resignDate);
        CHFiling::getDb()->update(self::$tableName, $fields, 'form_submission_id = '.$this->formSubmissionId);

        $this->resignDate = $resignDate;
    }

    /**
     * @param string $companyType
     */
    public function setCompanyType($companyType)
    {
        $fields = array('company_type' => $companyType);
        CHFiling::getDb()->update(self::$tableName, $fields, 'form_submission_id = '.$this->formSubmissionId);

        $this->companyType = $companyType;
    }

    /**
     * @return string
     */
    public function getXml()
    {
        $test = (CHFiling::getGatewayTest()) ? '' : '';

        if ($this->officer['corporate']) {
            $resignation = '<CorporateName>'.htmlspecialchars($this->officer['corporate_name'], ENT_NOQUOTES).'</CorporateName>';
        } else {
            $resignation = '<Surname>'.htmlspecialchars($this->officer['surname'], ENT_NOQUOTES).'</Surname>';
            if (!is_null($this->officer['forename'])) {
                $resignation .= '<Forename>'.htmlspecialchars($this->officer['forename'], ENT_NOQUOTES);

                if(!is_null($this->officer['middle_name'])) {
                    $resignation .=' '.htmlspecialchars($this->officer['middle_name'], ENT_NOQUOTES);
                };
                $resignation .= '</Forename>';
            }

            if ($this->officer['type'] == 'DIR') {
                $resignation .= '<DOB>'.htmlspecialchars($this->officer['dob'], ENT_NOQUOTES).'</DOB>';
            }
            $resignation = '<Person>'.$resignation.'</Person>';
        }

        if ($this->officer['type'] == 'DIR') {
            if($this->companyType != 'LLP') {
                $resignation = '<Director>'.$resignation.'</Director>';
            }else{
                $resignation = '<Member>'.$resignation.'</Member>';
            }
        } else {
            $resignation = '<Secretary>'.$resignation.'</Secretary>';
        }

        return '<OfficerResignation
            xmlns="http://xmlgw.companieshouse.gov.uk"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://xmlgw.companieshouse.gov.uk
            http://xml'.$test.'gw.companieshouse.gov.uk/v2-1/schema/forms/OfficerResignation-v2-5.xsd">
                <ResignationDate>'.$this->resignDate.'</ResignationDate>'.
                $resignation
            .'</OfficerResignation>';
    }
}

