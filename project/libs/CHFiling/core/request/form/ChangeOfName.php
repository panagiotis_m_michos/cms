<?php

class ChangeOfName implements Form
{
    /**
     *
     * @var int
     */
    private $formSubmissionId;

    /**
     *
     * @var string
     */
    private $rejectDescription;

    /**
     *
     * @var array
     */
    private $fields;

     /**
     *
     * @var int
     */
    private $changeNameId;

    /**
     *
     * @var string
     */
    private static $tableName = 'ch_change_company_name';

    /**
     * @param int $formSubmissionId
     * @throws Exception
     */
    private function  __construct($formSubmissionId)
    {
        if (!preg_match('/^\d+$/', $formSubmissionId)) {
            throw new Exception('formSubmissionId has to be integer');
        }

        $sql = "
            SELECT * FROM ".self::$tableName."
            WHERE `form_submission_id` = $formSubmissionId";

        if (!$result = CHFiling::getDb()->fetchRow($sql)) {
            throw new Exception("Change company name doesn't exist");
        }

        $this->formSubmissionId = $result['form_submission_id'];
        $this->rejectDescription = $result['reject_description'];
        unset($result['form_submission_id']);
        unset($result['reject_description']);

        $this->fields = $result;
    }

    /**
     * @param int $formSubmissionId
     * @return ChangeOfName
     */
    public static function getChangeCompanyName($formSubmissionId)
    {
        return new self($formSubmissionId);
    }

    /**
     * @param int $formSubmissionId
     * @return ChangeOfName
     * @throws Exception
     */
    public static function getNewChangeCompanyName($formSubmissionId)
    {
	    if (!preg_match('/^\d+$/', $formSubmissionId)) {
            throw new Exception('formSubmissionId has to be integer');
        }

        $data = array('form_submission_id'=> $formSubmissionId);
        CHFiling::getDb()->insert(self::$tableName, $data);

        return new self($formSubmissionId);
    }

    /**
     * @param int $company_id
     * @return array
     */
    public static function getSubmissionStatus($company_id)
    {
	    return CHFiling::getDb()->fetchAll('SELECT response FROM ch_form_submission WHERE company_id = ' . $company_id .' AND form_identifier = "ChangeOfName" ORDER BY form_submission_id DESC');
    }

    /**
     * @param array $fields
     */
    public function setChangeCompanyName($fields)
    {
        $this->fields = $fields;
        CHFiling::getDb()->update(self::$tableName, $fields, 'form_submission_id = ' . $this->formSubmissionId);
    }

    /**
     * @return string
     */
    public function getXml()
    {
        $change = '<ChangeOfName
		    xmlns="http://xmlgw.companieshouse.gov.uk"
		    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		    xsi:schemaLocation="http://xmlgw.companieshouse.gov.uk
		    http://xmlgw.companieshouse.gov.uk/v2-1/schema/forms/ChangeOfName-v2-5-1.xsd">
		   ';

        $change .= '<MethodOfChange>' . $this->fields['methodOfChange'] . '</MethodOfChange>';
        $change .= '<ProposedCompanyName>' . $this->fields['newCompanyName'] . '</ProposedCompanyName>';
        $change .= '<MeetingDate>' . $this->fields['meetingDate'] . '</MeetingDate>';

        // isset doesn't work because it was set all time (from 21/10/2011 to 20/01/2012 we were setting same day for every namechange purchase)
        if ($this->fields['sameDay'] == 1) {
            $change .= '<SameDay>true</SameDay>';
        } else {
            $change .= '<SameDay/>';
        }

        $change .= '<NoticeGiven>true</NoticeGiven>';
        $change .= '</ChangeOfName>';

        return $change;
    }
}
