<?php

use Utils\Date;

class ReturnOfAllotmentShares implements Form
{

    /** currency - for now we use only 3 main currencies */
    public static $currency = array('GBP' => 'GBP �', 'USD' => 'USD $', 'EUR' => 'EUR �');

    /**
     *
     * @var int
     */
    private $formSubmissionId;

    /**
     *
     * @var string
     */
    private $rejectDescription;

    /**
     *
     * @var String - date in YYYY-MM-DD format 
     */
    private $startPeriod;

    /**
     *
     * @var String - date in YYYY-MM-DD format 
     */
    private $endPeriod;

    /**
     *
     * @var Array of shares (fields from db)
     */
    private $shares;

    /**
     *
     * @var Array of returnAllomentId's
     */
    //private $allotment;

    /**
     *
     * @var string
     */
    private static $tableName = 'ch_return_of_allotment_shares';

    /**
     *
     * @param int $formSubmissionId
     */
    private function  __construct($formSubmissionId) 
    {
        // --- check that formSubmissionId is int ---
        if (!preg_match('/^\d+$/', $formSubmissionId)) {
            throw new Exception('formSubmissionId has to be integer');
        }

        // --- load data from database ---
        $sql = "
            SELECT * FROM ".self::$tableName."
            WHERE `form_submission_id` = $formSubmissionId";
        if (!$result = CHFiling::getDb()->fetchRow($sql)) {
            throw new Exception('return of allotment shares doesn\'t exist'.$formSubmissionId);
        }

        // --- set instance variables ---
        $this->formSubmissionId = $result['form_submission_id'];
        $this->rejectDescription= $result['reject_description'];
        $this->startPeriod      = $result['start_period'];
        $this->endPeriod        = $result['end_period'];

        // --- set shares ---
        $sql = "SELECT * FROM ch_return_shares
            WHERE `form_submission_id` = $formSubmissionId";
        $result = CHFiling::getDb()->fetchAll($sql);

        $this->shares = array();
        foreach($result as $v) {
            $this->shares[$v['return_shares_id']] = $v;
        }

        // --- set fields allotment ---
        /*
        $sql = "SELECT return_allotment_id FROM ch_return_allotment
            WHERE `form_submission_id` = $formSubmissionId";
        $result = CHFiling::getDb()->fetchAll($sql);

        foreach($result as $v) {
        $this->allotment[] = $v['return_allotment_id'];
        }
        */
    }

    /**
     *
     * @param int $formSubmissionId
     * @return ReturnOfAllotmentShares
     */
    public static function getReturnOfAllotmentShares($formSubmissionId) 
    {
        return new self($formSubmissionId);
    }

    /**
     *
     * @param int $formSubmissionId
     * @return ReturnOfAllotmentShares
     */
    public static function getNewReturnOfAllotmentShares($formSubmissionId) 
    {
        // --- check that formSubmissionId is int ---
        if (!preg_match('/^\d+$/', $formSubmissionId)) {
            throw new Exception('formSubmissionId has to be integer');
        }

        /* get company */
        $sql = "
            SELECT `company_id` FROM `ch_form_submission`
            WHERE `form_submission_id` = $formSubmissionId";
        $companyId = CHFiling::getDb()->fetchOne($sql);
        $company = Company::getCompany($companyId);

        /* send request for company data - sync request */
        $getData = CompanyDataRequest::getNewCompanyDataRequest(
            $company->getCustomerId(), $company->getCompanyNumber(),
            $company->getAuthenticationCode()
        );
        $xml = $getData->sendRequest();
        $response = simplexml_load_string($xml);
  
        /* check if it's an error */
        if (isset($response->GovTalkDetails->GovTalkErrors)) {
            /* delete form submission */
            CHFiling::getDb()->delete('ch_form_submission', 'form_submission_id = ' . $formSubmissionId);
            if (isset($response->GovTalkDetails->GovTalkErrors->Error->Text)) {
                throw new Exception($response->GovTalkDetails->GovTalkErrors->Error->Text);
            } else {
                throw new Exception('Error when importing company');
            }
        }

        /* set statement of capital */
        if (isset($response->Body->CompanyData->StatementOfCapital)) {
            $statementOfCapital = $response->Body->CompanyData->StatementOfCapital;
            if (isset($statementOfCapital->Capital)) {
                foreach($statementOfCapital->Capital as $capital) {
                    if (isset($capital->Shares)) {
                        foreach($capital->Shares as $share) {
                            $data = array();
                            $data['form_submission_id'] = $formSubmissionId;
                            $data['share_class']        = (string) $share->ShareClass;
                            $data['prescribed_particulars'] = (string) $share->PrescribedParticulars;
                            $data['num_shares']         = (string) $share->NumShares;
                            $data['amount_paid']        = (string) $share->AmountPaidDuePerShare;
                            $data['amount_unpaid']      = (string) $share->AmountUnpaidPerShare;
                            $data['currency']           = (string) $capital->ShareCurrency;
                            $data['aggregate_nom_value']= (string) $share->AggregateNominalValue;
                            CHFiling::getDb()->insert('ch_return_shares', $data);
                        }
                    } else {
                        throw new Exception('Error no statement of capital');
                    }
                }
            } else {
                throw new Exception('Error no statement of capital');
            }
        } else {
            throw new Exception('Error no statement of capital');
        }
        // --- insert new company incorporation ---
        $data = array(
            'form_submission_id'=> $formSubmissionId,
            'start_period' => date('Y-m-d'),
        );
        CHFiling::getDb()->insert(self::$tableName, $data);

        // --- return new instance ---
        return new self($formSubmissionId);
    }

    /**
     *
     * @return int 
     */
    public function getFormSubmissionId() 
    {
        return $this->formSubmissionId;
    }

    /**
     * returns array of integers which represent ids of shares associated
     * with this return of allotment shares
     *
     * @return array
     */
    /*
    public function getSharesIds() {
        return $this->shares;
    }
    */

    /**
     *
     * @param array $sharesIds array of all ids belonging to this object 
     */
    /*
    public function setSharesIds(array $sharesIds) {
        $this->shares = $sharesIds;
    }
    */

    /**
     * returns all shares in an array
     *
     * @return array<ReturnShares>
     */
    public function getShares() 
    {
        return $this->shares;
    }

    /**
     * returns shares with specified id
     *
     * @param int $shareId
     * @return Shares
     */
     /*
    public function getShare($shareId) {
        return ReturnShares::getReturnShares($this, $shareId);
    }
     */

    /**
     * returns new shares
     *
     * @return Shares
     */
     /*
    public function getNewShare() {
        return ReturnShares::getNewReturnShares($this);
    }
     */

    /**
     * returns array of integers which represent ids of allotments associated
     * with this return of allotment shares
     *
     * @return array
     */
    /*
    public function getAllotmentIds() {
        return $this->allotment;
    }
    */

    /**
     *
     * @param array $ids - allotment ids belonging to this object
     */
    /*
    public function setAllotmentIds(array $ids) {
        $this->allotment = $ids;
    }
    */

    /**
     * returns all allotments in an array
     *
     * @return array<ReturnAllotment>
     */
    /*
    public function getAllotments() {
        $allotments = array();

    if (!empty($this->allotment)) {
	    foreach($this->allotment as $v) {
    $allotments[$v] = ReturnAllotment::getReturnAllotment($this, $v);
	    }
    }

        return $allotments;
    }
    */

    /**
     * returns allotment with specified id
     *
     * @param int $allotmentId
     * @return Allotment
     */
    /*
    public function getAllotment($allotmentId) {
        return ReturnAllotment::getReturnAllotment($this, $allotmentId);
    }
    */

    /**
     * returns new allotment
     *
     * @return Allotment
     */
    /*
    public function getNewAllotment() {
        return ReturnAllotment::getNewReturnAllotment($this);
    }
    */

    //public function removeAllotment($id) {

        /* check if exists */
        /*
        if (!in_array($id, $this->allotment)) {
            throw new Exception('This allotment does not exist');
        }
        */

        /* remove from db */
        //CHFiling::getDb()->delete('ch_return_allotment', 'return_allotment_id = '.$id);

        /* create new array of ids without removed one */
        /*
        $newAllotment = array();
        foreach ($this->allotment as $v) {
            if ($v == $id) {
                $newAllotment[] = $v;
            }
        }
        $this->allotment = $newAllotment;
        */
    //}

    /*
    public function removeShares($id) {

        // check if exists
        if (!in_array($id, $this->shares)) {
            throw new Exception('This sheare does not exist');
        }

        // remove from db
        CHFiling::getDb()->delete('ch_return_shares', 'return_shares_id = '.$id);

        // create new array of ids without removed one
        $newShares = array();
        foreach ($this->shares as $v) {
            if ($v == $id) {
                $newShares[] = $v;
            }
        }
        $this->shares = $newShares;
    }
    */

    /**
     * returns true if submission can be submitted to companies house
     * otherwise false
     * 
     * @return boolean
     */
    public function isComplete() 
    {

        foreach($this->getShares() as $share) {
            if ($share['allotment'] > 0) {
                return TRUE;
            }
        }

        return FALSE;
    }

    /**
     * Key is share id (to be alloted to) and num of shares to be alloted
     *
     * @param array $data 
     */
    public function setAllotment(array $data) 
    {

        /* update allotment */
        $dbData = array();
        foreach($data as $k => $v) {
            //    pr($data);exit;
                CHFiling::getDb()->update(
                    'ch_return_shares',
                    array('share_class' => $v['share_class'],
                            'prescribed_particulars' => $v['prescribed_particulars'],
                            'num_shares' => $v['num_shares'],
                            'amount_paid' => $v['amount_paid'],
                            'amount_unpaid' => $v['amount_unpaid'],
                            'currency' => $v['currency'],
                            'aggregate_nom_value' => ($v['amount_paid']-$v['amount_unpaid'])*$v['num_shares'],
                            'allotment' => $v['new_shares_add'],
                            ),
                    'return_shares_id = ' . $k
                );
        }
    }

    /**
     *
     * @return xml
     */
    public function getXml() 
    {

        if (!$this->isComplete()) {
            throw new Exception('You have not set any allotment');
        }
        
        // start perion - end period
        $startPeriod = (is_null($this->startPeriod)) ?
                '<StartPeriodSharesAllotted>' . date('Y-m-d') . '</StartPeriodSharesAllotted>' :
                '<StartPeriodSharesAllotted>' . $this->startPeriod . '</StartPeriodSharesAllotted>';

        $endPeriod = (is_null($this->endPeriod)) ? '' :
                '<EndPeriodSharesAllotted>' . $this->endPeriod . '</EndPeriodSharesAllotted>';

        // statement of capital
        $statementOfCapital = '';
        $capitals = array();
        // sort shares by currency
        foreach($this->getShares() as $v) {
            $capitals[$v['currency']][] = $v;
        }

        // capitals are sorted by currency
        $allotment = '';
        foreach($capitals as $k => $v) {
            // create shares
            $shares = '';
            $totalNominalValue  = 0;
            $totalNumberOfShares= 0;
            foreach ($v as $share) {
                $numberOfShares = $share['num_shares'] + $share['allotment'];
                $shareValue     = $share['amount_paid'] + $share['amount_unpaid'];
                $nominalValue   = $shareValue * $numberOfShares;

                $totalNominalValue += $nominalValue;
                $totalNumberOfShares += $numberOfShares;
                $amountPaid     = $share['amount_paid'] + $share['amount_unpaid'];

                $shares .= '<Shares>
                    <ShareClass>'. $share['share_class'] .'</ShareClass>
                    <PrescribedParticulars>'. $share['prescribed_particulars'] .'</PrescribedParticulars>
                    <NumShares>'. $numberOfShares.'</NumShares>
                    <AmountPaidDuePerShare>'. $amountPaid .'</AmountPaidDuePerShare>
                    <AmountUnpaidPerShare>0</AmountUnpaidPerShare>
                    <AggregateNominalValue>'. $nominalValue .'</AggregateNominalValue>
                    </Shares>';

                /* allotment */
                if ($share['allotment'] > 0) {
                    $allotment .= '<Allotment>
                            <ShareClass>'.$share['share_class'].'</ShareClass>
                            <NumShares>'.$share['allotment'].'</NumShares>
                            <AmountPaidDuePerShare>'.$amountPaid.'</AmountPaidDuePerShare>
                            <AmountUnpaidPerShare>0</AmountUnpaidPerShare>
                            <ShareCurrency>'.$share['currency'].'</ShareCurrency>
                            <ShareValue>'. $shareValue .'</ShareValue>
                            </Allotment>';
                }
            }

            // create share for specific statement of capital
            $statementOfCapital .= '<Capital>';
            $statementOfCapital .= '<TotalNumberOfIssuedShares>'.$totalNumberOfShares.'</TotalNumberOfIssuedShares>';
            $statementOfCapital .= '<ShareCurrency>'.$k.'</ShareCurrency>';
            $statementOfCapital .= '<TotalAggregateNominalValue>'.$totalNominalValue.'</TotalAggregateNominalValue>';
            $statementOfCapital .= $shares;
            $statementOfCapital .= '</Capital>';
            
        }

        return '<ReturnofAllotmentShares
            xmlns="http://xmlgw.companieshouse.gov.uk"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://xmlgw.companieshouse.gov.uk
            http://xmlgw.companieshouse.gov.uk/v2-1/schema/forms/ReturnofAllotmentShares-v2-1.xsd">' .
                $startPeriod .
                $endPeriod .
                '<StatementOfCapital>' .
                $statementOfCapital .
                '</StatementOfCapital>' .
                $allotment
            .'</ReturnofAllotmentShares>';
    }

    /**
     * Sends request to companies house
     *
     * @param Company $company 
     */
    public function send(Company $company) 
    {

        $formSubmission = FormSubmission::getFormSubmission($company, $this->formSubmissionId);
        $formSubmission->sendRequest();
    }
}
