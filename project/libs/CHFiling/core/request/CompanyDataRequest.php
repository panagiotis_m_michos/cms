<?php

final class CompanyDataRequest implements Request
{
	/**
	 * tag element in the envelope
	 *
	 * @var string
	 */
	private static $class = 'CompanyDataRequest';

	/**
	 * company_data_request_id column from database
	 *
	 * @var int
	 */
	private $requestId;

	/**
	 * id of the customer doing request
	 *
	 * @var int
	 */
	private $customerId;

	/**
	 * @var int
	 */
	private $envelopeId;

	/**
	 * @var string
	 */
	private $companyNumber;

	/**
	 * authentication code of the company
	 *
	 * @var string
	 */
	private $authenticationCode;

	/**
	 * @var string
	 */
	private static $tableName = 'ch_company_data_request';

	/**
	 * @param int $requestId
	 * @throws Exception
	 */
	private function __construct($requestId)
	{
		if (!preg_match('/^\d+$/', $requestId)) {
			throw new Exception('requestId has to be integer');
		}

		$sql = "
			SELECT * FROM `" . self::$tableName . "`
			WHERE `company_data_request_id` = $requestId";
		if (!$result = CHFiling::getDb()->fetchRow($sql)) {
			throw new Exception('Record doesn\'t exist');
		}

		$this->requestId = $result['company_data_request_id'];
		$this->customerId = $result['customer_id'];
		$this->envelopeId = $result['envelope_id'];
		$this->companyNumber = $result['company_number'];
		$this->authenticationCode = $result['authentication_code'];
	}

	/**
	 * company_data_request_id column from the table
	 * returns existing request
	 *
	 * @param int $requestId
	 * @return CompanyDataRequest
	 */
	public static function getCompanyDataRequest($requestId)
	{
		return new CompanyDataRequest($requestId);
	}

	/**
	 * creates new request - ready for submission
	 *
	 * @param int $customerId
	 * @param string $companyNumber
	 * @param string $authenticationCode
	 * @return CompanyDataRequest
	 */
	public static function getNewCompanyDataRequest($customerId, $companyNumber, $authenticationCode)
	{
		// --- insert data into database ---
		$data = [
			'customer_id' => $customerId,
			'company_number' => $companyNumber,
			'authentication_code' => $authenticationCode,
		];
		CHFiling::getDb()->insert(self::$tableName, $data);

		// --- return new instance ---
		return new CompanyDataRequest(CHFiling::getDb()->lastInsertId());
	}

	/**
	 * set this after receiving response
	 * so in case of error this can be easily located
	 *
	 * @param int $envelopeId
	 * @throws Exception
	 */
	public function setEnvelopeId($envelopeId)
	{
		// --- check that statusId is int ---
		if (!preg_match('/^\d+$/', $envelopeId)) {
			throw new Exception('envelopeId has to be integer');
		}

		// --- update database and instance variable ---
		CHFiling::getDb()->update(
			self::$tableName,
			['envelope_id' => $envelopeId],
			'company_data_request_id = ' . $this->requestId
		);
		$this->envelopeId = $envelopeId;
	}

	/**
	 * @return string
	 */
	public function getClass()
	{
		return self::$class;
	}

	/**
	 * @return string
	 */
	public function getXml()
	{
		// --- test xml --
		$test = (CHFiling::getGatewayTest()) ? '' : '';
		if (Feature::featurePsc()) {
			if (Feature::featurePscTesting()) {
				$xsdSchema = 'http://xmlbeta.companieshouse.gov.uk/v1-0/schema/CompanyData-v3-1-rc1.xsd';
			} else {
				$xsdSchema = 'http://xmlgw.companieshouse.gov.uk/v1-0/schema/CompanyData-v3-1.xsd';
			}
		} else {
			$xsdSchema = 'http://xml' . $test . 'gw.companieshouse.gov.uk/v2-1/schema/CompanyData-v1-1.xsd';
		}

		$companyNumber = $this->companyNumber;
		$companyType = '';
		if (preg_match('/^([a-zA-Z]{2})([0-9]+)/', $companyNumber, $matches)) {
			if (count($matches) == 3) {
				$companyType = "<CompanyType>{$matches[1]}</CompanyType>";
				$companyNumber = $matches[2];
			}
		}

		return '<CompanyDataRequest
            xmlns="http://xmlgw.companieshouse.gov.uk"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://xmlgw.companieshouse.gov.uk ' . $xsdSchema . '">
			<CompanyNumber>' . $companyNumber . '</CompanyNumber>' .
			$companyType .
			'<CompanyAuthenticationCode>' . $this->authenticationCode . '</CompanyAuthenticationCode>
			<MadeUpDate>' . date('Y-m-d') . '</MadeUpDate>
		</CompanyDataRequest>';
	}

	/**
	 * @return string
	 */
	public function sendRequest()
	{
		require_once dirname(__FILE__) . '/../Envelope.php';
		$envelope = Envelope::getNewEnvelope();
		$response = $envelope->sendRequest($this);
		$this->setEnvelopeId($envelope->getEnvelopeId());

		return $response;
	}
}
