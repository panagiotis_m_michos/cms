<?php

/**
 * @package    CMS
 * @author     Nikolai Senkevich
 */

require_once dirname(__FILE__).'/Request.php';

final class GetEReminders implements Request
{
    /**
     * tag element in the envelope
     *
     * @var string
     */
    private static $class = 'GetERemindersRequest';


    /**
     *
     * @var int
     */
    public $companyNumber;
    
    /**
     *
     * @var string 
     */
    public $companyType;
    
    /**
     *
     * @var string 
     */
    public $authenticationCode;

    /**
     *
     * @param type $emails
     * @param Company $company 
     */
    private function __construct(Company $company) {

        $this->companyNumber = $company->getCompanyNumber();
        $data = $company->getData();      
        $this->companyType = $data['company_details']['jurisdiction'];
        $this->authenticationCode = $company->getAuthenticationCode();
        
    }

    /**
     *
     * @param type $emails
     * @param Company $company
     * @return GetEReminders 
     */
    public static function getNewReminder($company) {
        return new GetEReminders($company);
    }

	/**
	 *
	 * @return string
	 */
	public function getClass() {
		return self::$class;
	}

	/**
	 *
	 * @return xml
	 */
    public function getXml() {
       
        return '<GetERemindersRequest 
                xmlns="http://xmlgw.companieshouse.gov.uk"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xsi:schemaLocation="http://xmlgw.companieshouse.gov.uk 
                http://xmlgw.companieshouse.gov.uk/v2-1/schema/EReminders-v1-0.xsd">

                <CompanyNumber>'.$this->companyNumber.'</CompanyNumber>
                <CompanyType>'.$this->companyType.'</CompanyType>
                <CompanyAuthenticationCode>'.$this->authenticationCode.'</CompanyAuthenticationCode>
                </GetERemindersRequest>';
    }

    /**
     * sends this request to companies house
     * 
     * @return xml
     */
    public function sendRequest() {
        require_once dirname(__FILE__).'/../Envelope.php';
        $envelope = Envelope::getNewEnvelope();
        $response = $envelope->sendRequest($this);
        
		// --- return response ---
		return $response;

    }
}
