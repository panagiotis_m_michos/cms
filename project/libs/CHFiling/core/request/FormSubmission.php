<?php

use Entities\CompanyHouse\FormSubmission as FormSubmissionEntity;
use Exceptions\Technical\RequestException;

class FormSubmission implements Request
{

    /**
     * form_submission_id column from database
     *
     * @var int
     */
    private $formSubmissionId;

    /**
     * company_id from database - foreign key
     *
     * @var int
     */
    private $companyId;

    /**
     * same as class tag in the envelope
     *
     * @var string
     */
    private $formIdentifier;

    /**
     * @var string
     */
    private $language;

    /**
     * @var string
     */
    private $response;

    /**
     * @var string
     */
    private $rejectReference;

    /**
     * @var string
     */
    private $examinerTelephone;

    /**
     * @var string
     */
    private $examinerComment;

    /**
     * @var DateTime
     */
    private $dateCancelled;

    /**
     * array holding ch_form_submission_error table
     *
     * @var array
     */
    private $errors;

    /**
     *
     * @var Company
     */
    private $company;

    /**
     * body of this form submission
     *
     * @var Form
     */
    private $form;

    /**
     * array of all documents that belongs to this form submission
     *
     * @var Document[]
     */
    private $documents;

    /**
     *
     * @var string
     */
    private static $tableName = 'ch_form_submission';

    /**
     * @param Company $company
     * @param int $formSubmissionId
     * @throws Exception
     */
    private function __construct(Company $company, $formSubmissionId)
    {
        // --- check that formSubmissionId is int ---
        if (!preg_match('/^\d+$/', $formSubmissionId)) {
            throw new Exception('formSubmissionId has to be integer');
        }

        // --- form submission ---
        $sql = "
			SELECT * FROM `" . self::$tableName . "`
			WHERE `form_submission_id` = $formSubmissionId";
        if (!$result = CHFiling::getDb()->fetchRow($sql)) {
            throw new Exception('Record doesn\'t exist');
        }

        if ($company->getCompanyId() != $result['company_id']) {
            throw new Exception('form submission is not for specified company');
        }

        // --- errors ---
        $sql = "
			SELECT * FROM `ch_form_submission_error`
			WHERE `form_submission_id` = $formSubmissionId";
        $errors = CHFiling::getDb()->fetchAssoc($sql);

        // --- set instance variables ---
        $this->errors = $errors;

        $this->company = $company;
        $this->formSubmissionId = $result['form_submission_id'];
        $this->companyId = $result['company_id'];
        $this->formIdentifier = $result['form_identifier'];
        $this->language = $result['language'];
        $this->response = $result['response'];
        $this->rejectReference = $result['reject_reference'];
        $this->examinerTelephone = $result['examiner_telephone'];
        $this->examinerComment = $result['examiner_comment'];
        $this->dateCancelled = $result['date_cancelled'] ? new DateTime($result['date_cancelled']) : NULL;

        // --- set form instance variable ---
        switch ($this->formIdentifier) {
            case 'CompanyIncorporation':
                require_once dirname(__FILE__) . '/form/incorporation/CompanyIncorporation.php';
                $this->form = CompanyIncorporation::getCompanyIncorporation($this->formSubmissionId);
                break;
            case 'ChangeRegisteredOfficeAddress':
                require_once dirname(__FILE__) . '/form/ChangeRegisteredOfficeAddress.php';
                $this->form = ChangeRegisteredOfficeAddress::getChangeRegisteredOfficeAddress($this->formSubmissionId);
                break;
            case 'OfficerResignation':
                require_once dirname(__FILE__) . '/form/OfficerResignation.php';
                $this->form = OfficerResignation::getOfficerResignation($this->formSubmissionId);
                break;
            case 'OfficerAppointment':
                require_once dirname(__FILE__) . '/form/OfficerAppointment.php';
                $this->form = OfficerAppointment::getOfficerAppointment($this->formSubmissionId);
                break;
            case 'OfficerChangeDetails':
                require_once dirname(__FILE__) . '/form/OfficerChangeDetails.php';
                $this->form = OfficerChangeDetails::getOfficerChangeDetails($this->formSubmissionId);
                break;
            case 'ReturnOfAllotmentShares':
                require_once dirname(__FILE__) . '/form/returnOfAllotmentShares/ReturnOfAllotmentShares.php';
                $this->form = ReturnOfAllotmentShares::getReturnOfAllotmentShares($formSubmissionId);
                break;
            case 'AnnualReturn':
                require_once dirname(__FILE__) . '/form/annualReturn/CHAnnualReturn.php';
                $this->form = CHAnnualReturn::getAnnualReturn($formSubmissionId);
                break;
            case 'ChangeOfName':
                require_once dirname(__FILE__) . '/form/ChangeOfName.php';
                $this->form = ChangeOfName::getChangeCompanyName($formSubmissionId);
                break;
            case 'ChangeAccountingReferenceDate':
                require_once dirname(__FILE__) . '/form/ChangeAccountingReferenceDate.php';
                $this->form = ChangeAccountingReferenceDate::getChangeAccountingReferenceDate($formSubmissionId);
                break;
            default:
                throw new Exception($this->formIndentifier . ' is not supported');
        }

        // --- set documents ---
        $documentsIds = Document::getFormSubmissionDocumentsIds($this->formSubmissionId);

        $this->documents = array();
        foreach ($documentsIds as $v) {
            $document = Document::getDocument($v, $this->companyId);
            $this->documents[$document->getDocumentId()] = $document;
        }
    }

    /**
     * form_submission_id column from the table
     *
     * @param Company $company
     * @param int $formSubmissionId
     * @return FormSubmission
     */
    public static function getFormSubmission(Company $company, $formSubmissionId)
    {
        return new FormSubmission($company, $formSubmissionId);
    }

    /**
     * creates and returns new form submission
     *
     * @param Company $company
     * @param string $formIdentifier
     * @throws Exception
     * @return FormSubmission
     */
    public static function getNewFormSubmission(Company $company, $formIdentifier)
    {
        $companyId = $company->getCompanyId();

        // --- insert data into database ---
        $data = array(
            'company_id' => $companyId,
            'form_identifier' => $formIdentifier
        );
        CHFiling::getDb()->insert(self::$tableName, $data);
        $formSubmissionId = CHFiling::getDb()->lastInsertId();

        // --- set form ---
        switch ($formIdentifier) {
            case 'CompanyIncorporation':
                require_once dirname(__FILE__) . '/form/incorporation/CompanyIncorporation.php';
                CompanyIncorporation::getNewCompanyIncorporation($formSubmissionId);
                break;
            case 'ChangeRegisteredOfficeAddress':
                require_once dirname(__FILE__) . '/form/ChangeRegisteredOfficeAddress.php';
                ChangeRegisteredOfficeAddress::getNewChangeRegisteredOfficeAddress($formSubmissionId);
                break;
            case 'OfficerResignation':
                require_once dirname(__FILE__) . '/form/OfficerResignation.php';
                OfficerResignation::getNewOfficerResignation($formSubmissionId);
                break;
            case 'OfficerAppointment':
                require_once dirname(__FILE__) . '/form/OfficerAppointment.php';
                OfficerAppointment::getNewOfficerAppointment($formSubmissionId);
                break;
            case 'OfficerChangeDetails':
                require_once dirname(__FILE__) . '/form/OfficerChangeDetails.php';
                OfficerChangeDetails::getNewOfficerChangeDetails($formSubmissionId);
                break;
            case 'ReturnOfAllotmentShares':
                // stan: 08/10/2010
                try {
                    require_once dirname(__FILE__) . '/form/returnOfAllotmentShares/ReturnOfAllotmentShares.php';
                    ReturnOfAllotmentShares::getNewReturnOfAllotmentShares($formSubmissionId);
                } catch (Exception $e) {
                    CHFiling::getDb()->delete(self::$tableName, 'form_submission_id = ' . $formSubmissionId);
                    throw $e;
                }
                break;
            case 'AnnualReturn':
                require_once dirname(__FILE__) . '/form/annualReturn/CHAnnualReturn.php';
                CHAnnualReturn::getNewAnnualReturn($formSubmissionId);
                break;
            case 'ChangeOfName':
                require_once dirname(__FILE__) . '/form/ChangeOfName.php';
                ChangeOfName::getNewChangeCompanyName($formSubmissionId);
                break;
            case 'ChangeAccountingReferenceDate':
                require_once dirname(__FILE__) . '/form/ChangeAccountingReferenceDate.php';
                ChangeAccountingReferenceDate::getNewChangeAccountingReferenceDate($formSubmissionId);
                break;
            default:
                throw new Exception($formIdentifier . ' is not supported');
        }

        // --- return new instance ---
        return new FormSubmission($company, $formSubmissionId);
    }

    /**
     * @param int $companyId
     * @throws Exception
     * @return array
     */
    public static function getCompanyFormSubmissionsIds($companyId)
    {
        // --- check that companyId is int ---
        if (!preg_match('/^\d+$/', $companyId)) {
            throw new Exception('companyId has to be integer');
        }

        // --- query ---
        $sql = "
			SELECT `form_submission_id` FROM `" . self::$tableName . "`
			WHERE `date_cancelled` IS NULL AND `company_id` = $companyId ORDER BY `form_submission_id` DESC";
        $result = CHFiling::getDb()->fetchCol($sql);

        return $result;
    }

    /**
     * @param int $companyId
     * @throws Exception
     * @return array
     */
    public static function getCompanyIncorporationFormSubmissionsIds($companyId)
    {
        // --- check that companyId is int ---
        if (!preg_match('/^\d+$/', $companyId)) {
            throw new Exception('companyId has to be integer');
        }

        // --- query ---
        $sql = "
			SELECT `form_submission_id` FROM `" . self::$tableName . "`
			WHERE `company_id` = $companyId AND form_identifier = 'companyIncorporation' ORDER BY `form_submission_id` DESC";
        $result = CHFiling::getDb()->fetchCol($sql);

        return $result;
    }

    /**
     * returns form submission object which is accepted CompanyIncorporation
     *
     * @param Company $company
     * @return FormSubmission
     */
    public static function getCompanyIncorporationFormSubmission(Company $company)
    {
        $companyId = $company->getCompanyId();

        // --- query ---
        $sql = "
			SELECT `form_submission_id` FROM `" . self::$tableName . "`
			WHERE `date_cancelled` IS NULL AND `company_id` = $companyId AND `form_identifier` = 'CompanyIncorporation' AND `response` = 'ACCEPT'";
        $result = CHFiling::getDb()->fetchOne($sql);

        if (!empty($result) && $result != NULL) {
            return self::getFormSubmission($company, $result);
        }
        return NULL;
    }

    /**
     *
     * @return string
     */
    public function getFormIdentifier()
    {
        return $this->formIdentifier;
    }

    /**
     *
     * @return Envelope[]
     */
    private function getEnvelopes()
    {
        return Envelope::getFormSubmissionEnvelopes($this->formSubmissionId);
    }

    /**
     * @return string
     */
    public function getFirstSubmissionDate()
    {
        $envelopes = $this->getEnvelopes();
        if (empty($envelopes)) {
            return NULL;
        }

        $envelope = end($envelopes);

        return $envelope->getDateSent();
    }

    /**
     * @return string
     */
    public function getLastSubmissionDate()
    {
        $envelopes = $this->getEnvelopes();
        if (empty($envelopes)) {
            return NULL;
        }

        return $envelopes[0]->getDateSent();
    }

    /**
     * return first submission date
     *
     * @return string
     */
    public function getSubmissionDate()
    {
        return $this->getLastSubmissionDate();
    }

    /**
     *
     * @return Form
     */
    public function getForm()
    {
        // --- do not use clone for now, because change address won't work ---
        return $this->form;
    }

    /**
     * @param string $category enum('MEMARTS','SUPPNAMEAUTH','SUPPEXISTNAME','ACCOUNTS')
     * @param string $filename
     * @param string $filePath
     * @param mixed $custom
     * @throws Exception
     * @return Document
     */
    public function setDocument($category, $filename, $filePath , $custom = NULL)
    {
        // --- check if we don't have 3 docs already ---
        if (count($this->documents) > 3) {
            throw new Exception('Form submission can have max 3 documents');
        }

        // --- create new document ---
        $document = Document::getNewDocument($this->formSubmissionId, $category, $filename, $this->companyId, $custom);
        $this->documents[$document->getDocumentId()] = $document;
    }

    /**
     * @param int $documentId
     * @throws Exception
     * @return Document
     */
    public function getDocument($documentId)
    {
        if (!isset($this->documents[$documentId])) {
            throw new Exception('Specified document does not exist');
        }
        // --- return document ---
        return $this->documents[$documentId];
    }

    /**
     * @return Document[]
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * @param string $type enum('MEMARTS','SUPPNAMEAUTH','SUPPEXISTNAME','ACCOUNTS')
     * @param string|array $file
     * @param mixed $custom
     * @throws Exception
     */
    public function uploadDocument($type, $file, $custom = NULL)
    {
        // --- array - custom/uploaded articles/documents ---
        if (is_array($file)) {
            if (!isset($file['error'], $file['tmp_name'], $file['name'])) {
                throw new Exception('Error uploading file');
            }
            if ($file['error'] != 0) {
                throw new Exception('Error uploading file');
            }
            if (!is_uploaded_file($file['tmp_name'])) {
                throw new Exception('Error uploading file');
            }

            switch ($type) {
                case 'MEMARTS':
                    $fileName = 'article.pdf';
                    $this->form->setArticles('BESPOKE');
                    break;
                case 'SUPPNAMEAUTH':
                    $fileName = 'suppnameauth.pdf';
                    break;
                case 'SUPPEXISTNAME':
                    $fileName = 'suppexistname.pdf';
                    break;
                case 'ACCOUNTS':
                    $fileName = 'accounts.xml';
                    break;
            }
        }

        // --- create file path for documents ---
        $filePath = CHFiling::getDocPath() . '/' . CHFiling::getDirName($this->company->getCompanyId());
        // --- create folder if needed ---
        if (!is_dir($filePath)) {
            mkdir($filePath, 0777, TRUE);
        }

        $filePath = $filePath . "/company-{$this->company->getCompanyId()}" . "/formSubmission-$this->formSubmissionId" . "/";
        if (!is_dir($filePath)) {
            mkdir($filePath, 0777, TRUE);
        }

        // --- string - model articles ---
        if (!is_array($file)) {
            copy($file, $filePath . 'article.pdf');
            $fileName = 'article.pdf';
            $this->form->setArticles($this->form->getType() . 'MODEL');
            // --- array - custom/uploaded articles/documents ---
        } else {
            move_uploaded_file($file['tmp_name'], $filePath . $fileName);
        }

        /* check if there are memarts already */
        $memarts = $this->getMemarts();
        /* if type is the same as existing document - MEMARTS */
        if ($memarts != NULL && $type == 'MEMARTS') {
            /* remove old MEMARTS */
            $this->documents[$memarts['id']]->remove();
            unset($this->documents[$memarts['id']]);
        }

        // --- set document ---
        $this->setDocument($type, $fileName, $filePath, $custom);
    }

    /**
     * @return array
     */
    public function getMemarts()
    {
        foreach ($this->documents as $document) {
            if ($document->getCategory() == 'MEMARTS') {
                $doc = array();
                $doc['filename'] = $document->getFilename();
                $doc['bespoke'] = ($this->form->getArticles() == 'BESPOKE') ? 1 : 0;
                $doc['id'] = $document->getDocumentId();
                return $doc;
            }
        }
        return NULL;
    }

    /**
     * @return array
     */
    public function getSuppnameauth()
    {
        foreach ($this->documents as $document) {
            if ($document->getCategory() == 'SUPPNAMEAUTH') {
                $doc = array();
                $doc['filename'] = $document->getFilename();
                $doc['id'] = $document->getDocumentId();
                return $doc;
            }
        }
        return NULL;
    }

    /**
     * @return int
     */
    public function getFormSubmissionId()
    {
        return $this->formSubmissionId;
    }

    /**
     * sets response column and instance variable
     *
     * @param string $response
     * @throws Exception
     */
    public function setResponse($response)
    {
        if (!isset(FormSubmissionEntity::$responses[$response])) {
            throw new Exception('Response value - ' . $response . ' is not allowed');
        }

        CHFiling::getDb()->update(
            self::$tableName,
            array('response' => $response),
            'form_submission_id = ' . $this->formSubmissionId
        );
        $this->response = $response;
    }

    /**
     * @return string
     */
    public function getClass()
    {
        if (method_exists($this->form, 'getAnnualReturnFormId')) {
            return $this->form->getAnnualReturnFormId();
        }

        return $this->formIdentifier;
    }

    /**
     * @return string
     */
    public function getXml()
    {
        // --- test xml --
        $test = (CHFiling::getGatewayTest()) ? '' : '';
        $companyNumber = $this->company->getCompanyNumber() === NULL ? '' :
                $this->company->getCompanyNumber();

        // --- create company number and type tag ---
        if (!empty($companyNumber)) {
            // --- if company number is not all digits
            if (!preg_match('/^\d+$/', $companyNumber)) {
                // --- take first two letters from company number ---
                $companyType = substr($companyNumber, 0, 2);
                // --- and remove them from the number ---
                $companyNumber = substr($companyNumber, 2);
            } else {
                $companyType = 'EW';
            }
            // --- create tag for company type ---
            $companyType = '<CompanyType>' . $companyType . '</CompanyType>';
            $companyNumber = '<CompanyNumber>' . $companyNumber . '</CompanyNumber>';
        } else {
            $companyType = '';
        }

        $pdfCompanyName = $this->company->getCompanyName();
        $companyName = '<CompanyName>' . htmlspecialchars($this->company->getCompanyName(), ENT_NOQUOTES) . '</CompanyName>';
        $cAuth = $this->company->getAuthenticationCode();
        $companyAuth = empty($cAuth) ? '' :
                '<CompanyAuthenticationCode>' . $this->company->getAuthenticationCode() . '</CompanyAuthenticationCode>';

        // --- documents ---
        $documents = '';
        foreach ($this->documents as $document) {
            if ($this->formIdentifier == 'CompanyIncorporation') {
                $shareCapital = ($this->form->getType() == 'BYGUAR') ? FALSE : TRUE;
                if ($this->form->getArticles() != 'BESPOKE' && $document->getCategory() == 'MEMARTS') {
                    if (!is_dir($document->getFilePath())) {
                        mkdir($document->getFilePath(), 0777, TRUE);
                    }
                    Document::saveMemorandum(
                        $pdfCompanyName,
                        $this->form->getIncPersons(array('SUB')),
                        $this->form->getIncCorporates(array('SUB')),
                        $document->getFilePath() . $document->getFilename(),
                        $shareCapital
                    );
                }
            }
            $documents .= $document->getXml();
        }

        // --- check if it is company incorporation ---
        if ($this->formIdentifier == 'CompanyIncorporation') {
            // --- check what documents we have ---
            $docCategory = array();
            foreach ($this->documents as $document) {
                $docCategory[$document->getCategory()] = 1;
            }
            // --- if there's support of name set it in the form as well ---
            if (isset($docCategory['SUPPNAMEAUTH']) || isset($docCategory['SUPPEXISTNAME'])) {
                $this->form->setNameAuthorisation(1);
            } else {
                $this->form->setNameAuthorisation(NULL);
            }
        }

        //todo: psc-getXml  sets annualReturnFormIdentifier
        $xml = $this->form->getXml();

        if ($this->formIdentifier == 'ReturnOfAllotmentShares') {
            $formIdentifier = 'ReturnofAllotmentShares';
        } elseif (method_exists($this->form, 'getAnnualReturnFormId')) {
            $formIdentifier = $this->form->getAnnualReturnFormId();
        } else {
            $formIdentifier = $this->formIdentifier;
        }

        return '<FormSubmission
                    xmlns="http://xmlgw.companieshouse.gov.uk/Header"
                    xmlns:bs="http://xmlgw.companieshouse.gov.uk"
                    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                    xsi:schemaLocation="http://xmlgw.companieshouse.gov.uk/Header
                    http://xml' . $test . 'gw.companieshouse.gov.uk/v2-1/schema/forms/FormSubmission-v2-6.xsd">
	            <FormHeader>' .
                $companyNumber .
                $companyType .
                $companyName .
                $companyAuth .
                '<PackageReference>' . CHFiling::getPackageReference() . '</PackageReference>
                    <Language>' . $this->language . '</Language>
                    <FormIdentifier>' . $formIdentifier . '</FormIdentifier>
                    <SubmissionNumber>' . $this->formSubmissionId . '</SubmissionNumber>
                </FormHeader>
                <DateSigned>' . date('Y-m-d') . '</DateSigned>
                <Form>' .
                $xml
                . '</Form>' .
                $documents
                . '</FormSubmission>';
    }

    /**
     * sends this request to companies house
     * sends only if the previous response is nothing or pending
     *
     * @return string
     */
    public function sendRequest()
    {
        // --- already have response, no need to send again ---
        if (isset($this->response) && !$this->isPending() && !$this->isInternalFailure()) {
            return;
        }

        if (Feature::featureFakeSubmissionSending()) {
            CHFiling::getDb()->update(
                self::$tableName,
                [
                    'response' => FormSubmissionEntity::RESPONSE_PENDING,
                    'toResend' => 1
                ],
                'form_submission_id = ' . $this->formSubmissionId
            );

            return;
        }

        // --- send request ---
        $envelope = Envelope::getNewEnvelope($this->formSubmissionId);
        $response = $envelope->sendRequest($this);

        $xml = simplexml_load_string($response);

        // --- check if there are any errors ---
        if (isset($xml->GovTalkDetails->GovTalkErrors->Error)) {
            $this->setResponse(FormSubmissionEntity::RESPONSE_ERROR);
            return $this->response;
        }
        unset($xml);

        // --- set status to pending, if envelope was successfuly sent ---
        if ($envelope->getSuccess()) {
            $this->setResponse(FormSubmissionEntity::RESPONSE_PENDING);
        }

        return $this->response;
    }

    /**
     * @TODO refactor calls to "sendRequest" method to use this method instead
     * @throws InvalidStateException
     * @throws RequestException
     * @return string
     */
    public function sendRequestImproved()
    {
        // already have response, no need to send again
        if (isset($this->response) && !$this->isPending() && !$this->isInternalFailure()) {
            throw new InvalidStateException('Request has already been sent');
        }

        if (Feature::featureFakeSubmissionSending()) {
            CHFiling::getDb()->update(
                self::$tableName,
                [
                    'response' => FormSubmissionEntity::RESPONSE_PENDING,
                    'toResend' => 1
                ],
                'form_submission_id = ' . $this->formSubmissionId
            );

            return;
        }

        $envelope = Envelope::getNewEnvelope($this->formSubmissionId);
        $response = $envelope->sendRequest($this);
        $xml = new SimpleXMLElement($response);

        if ($envelope->getSuccess() && !isset($xml->GovTalkDetails->GovTalkErrors->Error)) {
            $this->setResponse(FormSubmissionEntity::RESPONSE_PENDING);
            return $this->response;
        } else {
            $this->setResponse(FormSubmissionEntity::RESPONSE_ERROR);
            throw new RequestException('There has been an error in request');
        }
    }

    /**
     * @return string
     */
    public function getSubmissionStatus()
    {
        return $this->response;
    }

    /**
     * @return int
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @return string
     */
    public function getRejectReference()
    {
        return $this->rejectReference;
    }

    /**
     * @return string
     */
    public function getExaminerTelephone()
    {
        return $this->examinerTelephone;
    }

    /**
     * @return string
     */
    public function getExaminerComment()
    {
        return $this->examinerComment;
    }

    /**
     * @return DateTime
     */
    public function getDateCancelled()
    {
        return $this->dateCancelled;
    }

    /**
     * @return bool
     */
    public function isAccept()
    {
        return $this->response == FormSubmissionEntity::RESPONSE_ACCEPT;
    }

    /**
     * @return bool
     */
    public function isReject()
    {
        return $this->response == FormSubmissionEntity::RESPONSE_REJECT;
    }

    /**
     * @return bool
     */
    public function isError()
    {
        return $this->response == FormSubmissionEntity::RESPONSE_ERROR;
    }

    /**
     * @return bool
     */
    public function isPending()
    {
        return $this->response == FormSubmissionEntity::RESPONSE_PENDING;
    }

    /**
     * @return bool
     */
    public function isInternalFailure()
    {
        return $this->response == FormSubmissionEntity::RESPONSE_INTERNAL_FAILURE;
    }

    /**
     * @return bool
     */
    public function isCompanyIncorporationType()
    {
        return $this->formIdentifier == FormSubmissionEntity::TYPE_COMPANY_INCORPORATION;
    }

    /**
     * @return bool
     */
    public function isAnnualReturnType()
    {
        return $this->formIdentifier == FormSubmissionEntity::TYPE_ANNUAL_RETURN;
    }

    /**
     * @return bool
     */
    public function isChangeAccountingReferenceDateType()
    {
        return $this->formIdentifier == FormSubmissionEntity::TYPE_CHANGE_ACCOUNTING_REFERENCE_DATE;
    }

    /**
     * @param int $formSubmissionId
     * @param int $companyId
     * @return int new form submission id
     */
    public static function setRejectCompanyIncorporation($formSubmissionId, $companyId)
    {

        // --- copy the old form submission and create new one ---
        $sql = "INSERT INTO " . self::$tableName . "
            (`company_id`, `form_identifier`, `language`, `dtc`, `dtm`)
            SELECT `company_id`, `form_identifier`, `language`, NOW(), NOW()
            FROM `" . self::$tableName . "`
            WHERE `form_submission_id` = $formSubmissionId";

        // --- get id of the newly create form submission ---
        CHFiling::getDb()->query($sql);
        $newFormSubmissionId = CHFiling::getDb()->lastInsertId();

        // -- copy the old documents records and create new records
        Document::copyRejectedDocsWithNewFormSubmision($formSubmissionId, $newFormSubmissionId);

        // --- save incorporation certificate ---
        // --- create file path for documents ---
        $fromPath = CHFiling::getDocPath() . '/' . CHFiling::getDirName($companyId) . "/company-$companyId/formSubmission-$formSubmissionId/";
        $toPath = CHFiling::getDocPath() . '/' . CHFiling::getDirName($companyId) . "/company-$companyId/formSubmission-$newFormSubmissionId/";

        // --- create folder if needed ---
        if (!is_dir($toPath)) {
            mkdir($toPath, 0777, TRUE);
        }

        // --- copy all the files into new form submission folder ---
        if (file_exists($fromPath)) {
            $handle = opendir($fromPath);
            while (FALSE !== ($file = readdir($handle))) {
                if ($file != '.' && $file != '..') {
                    copy($fromPath . $file, $toPath . $file);
                }
            }
        }

        return $newFormSubmissionId;
    }

    /**
     * @param array $data (formSubmissionId, requestKey, companyNumber)
     * @throws Exception
     * @return array array with one element ['attachementPath'] or empty array when request for document failed
     */
    public static function setAcceptChangeOfName(array $data)
    {
        /* required data */
        $reqData = array(
            'formSubmissionId' => TRUE,
            'requestKey' => TRUE,
            'companyNumber' => TRUE
        );

        /* check required data */
        foreach ($reqData as $k => $v) {
            if (!isset($data[$k]) || $data[$k] == NULL) {
                Throw new Exception($k . ' is required.');
            }
        }

        /* create document request */
        $document = GetDocument::getNewDocument($data['requestKey']);
        $xml = simplexml_load_string($document->sendRequest($data['formSubmissionId']));

        /* if there's an error, return empty array */
        if (isset($xml->GovTalkDetails->GovTalkErrors->Error)) {
            return array();
        }

        /* response from get docuement request */
        $documentDate = (string) $xml->Body->Document->DocumentDate;
        $documentId = (string) $xml->Body->Document->DocumentID;
        $incCert = base64_decode((string) $xml->Body->Document->DocumentData);
        $companyId = CHFiling::getDb()->fetchOne(
            'SELECT `company_id`
            FROM ' . self::$tableName . '
            WHERE form_submission_id = ' . $data['formSubmissionId']
        );

        /* plus get data from this form submission */
        $companyNaneChange = CHFiling::getDb()->fetchRow(
            'SELECT `newCompanyName`
            FROM `ch_change_company_name`
            WHERE `form_submission_id` = ' . $data['formSubmissionId']
        );

        $companyNaneChange['change_name_id'] = NULL;
        $companyNaneChange['company_name'] = $companyNaneChange['newCompanyName'];
        unset($companyNaneChange['newCompanyName']);

        // --- update company table ---
        CHFiling::getDb()->update('ch_company', $companyNaneChange, 'company_id = ' . $companyId);

        $docPath = CHFiling::getDocPath() . '/' . CHFiling::getDirName($companyId) . "/company-$companyId/documents/";
        $docName = str_replace('/', '_', $companyNaneChange['company_name']) . 'NameChangeCertificate.pdf';

        /* prepare data for insertion */
        //$companyData = array(
            //'form_submission_id'	=> $data['formSubmissionId'],
            //'filename'			=> $docName,
            //'category'			=> 'NAMECHANGE',
              //'file_path'			=> $docPath);

        // --- update document table ---
        //CHFiling::getDb()->insert('ch_document', $companyData);
        // --- create folder if needed ---
        if (!is_dir($docPath)) {
            mkdir($docPath, 0777, TRUE);
        }

        // --- Company Name Change Sertificate ---
        if (isset($incCert)) {
            file_put_contents($docPath . $docName, $incCert);
        }
        /* return array */
        return array('attachmentPath' => $docPath . $docName);
    }

    /**
     * @param array $data (formSubmissionId, requestKey, incorporationDate, authenticationCode, companyNumber)
     * @throws Exception
     * @return array array with one element ['attachementPath'] or empty array when request for document failed
     */
    public static function setAcceptCompanyIncorporation(array $data)
    {

        /* required data */
        $reqData = array(
            'formSubmissionId' => TRUE,
            'requestKey' => TRUE,
            'incorporationDate' => TRUE,
            'authenticationCode' => TRUE,
            'companyNumber' => TRUE
        );

        /* check required data */
        foreach ($reqData as $k => $v) {
            if (!isset($data[$k]) || $data[$k] == NULL) {
                Throw new Exception($k . ' is required.');
            }
        }

        /* create document request */
        $document = GetDocument::getNewDocument($data['requestKey']);
        $xml = simplexml_load_string($document->sendRequest($data['formSubmissionId']));

        /* if there's an error, return empty array */
        if (empty($xml) || isset($xml->GovTalkDetails->GovTalkErrors->Error)) {
            return array();
        }

        /* plus get data from this form submission */
        $companyInc = CHFiling::getDb()->fetchRow(
            'SELECT `type`, `premise`, `street`, `thoroughfare`, `post_town`,
            `county`, `country`, `postcode`, `care_of_name`, `po_box`
            FROM `ch_company_incorporation` 
            WHERE `form_submission_id` = ' . $data['formSubmissionId']
        );

        /* prepare data for insertion */
        $companyData = array(
            'incorporation_date' => $data['incorporationDate'],
            'authentication_code' => $data['authenticationCode'],
            'company_number' => $data['companyNumber'],
            'accepted_date' => date('Y-m-d'),
            'company_category' => $companyInc['type']);
            unset($companyInc['type']);

        /* response from get docuement request */
        if (isset($xml->Body->Document)) {
            $documentDate = (string) $xml->Body->Document->DocumentDate;
            $documentId = (string) $xml->Body->Document->DocumentId;
            $incCert = base64_decode((string) $xml->Body->Document->DocumentData);
            $companyData['document_date'] = $documentDate;
            $companyData['document_id'] = $documentId;
        }

        /* merge last form submission data and received data */
        $companyFullData = array_merge($companyData, $companyInc);

        // --- update company table ---
        $companyId = CHFiling::getDb()->fetchOne(
            'SELECT `company_id`
            FROM ' . self::$tableName . ' 
            WHERE form_submission_id = ' . $data['formSubmissionId']
        );
        CHFiling::getDb()->update('ch_company', $companyFullData, 'company_id = ' . $companyId);

        if (empty($incCert)) {
            FApplication::$container->get('error_testing_emailer')->sendEmptyIncorporationCertificateError($companyId, $data['formSubmissionId']);
        }

        // --- update members and shares ---
        $members = CHFiling::getDb()->fetchAssoc(
            'SELECT `type`, `corporate`, `title`, `forename`, `middle_name`, `surname`, `corporate_name`,
            `premise`, `street`, `thoroughfare`, `post_town`, `county`, `country`, 
            `postcode`, `care_of_name`, `po_box`,
            `dob`, `nationality`, `occupation`, `country_of_residence`,
            `residential_premise`, `residential_street`, `residential_thoroughfare`,
            `residential_post_town`, `residential_county`, `residential_country`,
            `residential_postcode`, `residential_secure_address_ind`
            FROM `ch_incorporation_member` 
            WHERE `form_submission_id` = ' . $data['formSubmissionId']
        );

        // --- members ---
        foreach ($members as $member) {
            $member['company_id'] = $companyId;
            CHFiling::getDb()->insert('ch_company_member', $member);
        }

        // --- shares ---
        $capitals = CHFiling::getDb()->fetchAssoc(
            'SELECT * FROM `ch_incorporation_capital`
            WHERE `form_submission_id` = ' . $data['formSubmissionId']
        );

        foreach ($capitals as $capital) {
            unset($capital['incorporation_capital_id']);
            unset($capital['form_submission_id']);

            $cCapital = array();
            $cCapital['total_issued'] = $capital['num_shares'];
            $cCapital['currency'] = $capital['currency'];
            $cCapital['total_aggregate_value'] = $capital['aggregate_nom_value'];
            $cCapital['company_id'] = $companyId;

            $shares = array();
            $shares['share_class'] = $capital['share_class'];
            $shares['prescribed_particulars'] = $capital['prescribed_particulars'];
            $shares['num_shares'] = $capital['num_shares'];
            $shares['amount_paid'] = $capital['amount_paid'];
            $shares['amount_unpaid'] = $capital['amount_unpaid'];
            $shares['nominal_value'] = $capital['aggregate_nom_value'];

            CHFiling::getDb()->insert('ch_company_capital', $cCapital);
            $shares['company_capital_id'] = CHFiling::getDb()->lastInsertId();

            CHFiling::getDb()->insert('ch_company_capital_shares', $shares);
        }

        $formSubmissionId = $data['formSubmissionId'];
        // --- save incorporation certificate ---
        // --- create file path for documents ---
        $formPath = CHFiling::getDocPath() . '/' . CHFiling::getDirName($companyId) . "/company-$companyId/formSubmission-$formSubmissionId/";
        $docPath = CHFiling::getDocPath() . '/' . CHFiling::getDirName($companyId) . "/company-$companyId/documents/";

        // --- create folder if needed ---
        if (!is_dir($docPath)) {
            mkdir($docPath, 0777, TRUE);
        }
        // --- incorporation certificate ---
        if (isset($incCert)) {
            file_put_contents($docPath . 'IncorporationCertificate.pdf', $incCert);
        }
        // --- articels ---
        if (file_exists($formPath . 'article.pdf')) {
            if (copy($formPath . 'article.pdf', $docPath . 'article.pdf')) {
                //unlink($formPath . 'article.pdf');
            }
        }

        /* return array */
        return array('attachmentPath' => $docPath . 'IncorporationCertificate.pdf');
    }

    /**
     * Pre-condition: form submission must be of CompanyIncorporation type,
     * otherwise form submission is not removed
     *
     * Removes form submission and everything associated with it
     */
    public function remove()
    {
        /* check if it is incorporation */
        if ($this->getClass() != 'CompanyIncorporation') {
            return;
        }

        /* remove company incorporation */
        $this->getForm()->remove();

        /* remove documents */
        $documents = $this->getDocuments();
        foreach ($documents as $v) {
            $v->remove();
        }

        /* remove this form submission */
        CHFiling::getDb()->delete(self::$tableName, 'form_submission_id = ' . $this->formSubmissionId);
        $this->formSubmissionId = NULL;
    }
}
