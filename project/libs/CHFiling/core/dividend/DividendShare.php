<?php

/**
 * DividendShare represents share from which the dividends are going to be paid.
 *
 * @author pete
 */
class DividendShare {

    /**
     * table holding the data for this class
     *
     * @var string
     */
    private static $tableName = "ch_dividend_share";

    /**
     * Primary key
     *
     * @var int 
     */
    private $dividendId;

    /**
     *
     * @var string 
     */
    private $shareClass;

    /**
     *
     * @var string currency code 
     */
    private $currency;

    /**
     * positive integer or positive float with max 3 fraction digits
     *
     * @var float
     */
    private $shareValue;

    /**
     *
     * @var array<DividendShareholder> 
     */
    private $shareholders;

    /**
     *
     * @param string $shareClass
     */
    private function  __construct($dividendId, $shareClass, $currency,
            $shareValue) {

        /* prepare fields */
        $currency   = strtoupper(trim($currency));
        $shareValue = trim($shareValue);

        /* trim 0 if it's decimal */
        if (preg_match("/\./", $shareValue)) {
            $shareValue = trim($shareValue, "0");
        } else {
            $shareValue = (int) $shareValue;
        }

        /* if it starts with . than prepend 0 */
        if (preg_match("/^\./", $shareValue)) {
            $shareValue = 0 . $shareValue;
        }

        /* if it finishes with . then remove it */
        if (preg_match("/\.$/", $shareValue)) {
            $shareValue = trim($shareValue, ".");
        }

        /* validation */
        if (is_null($shareClass)) {
            throw new Exception("Share class cannot be null");
        }
        if (strlen($shareClass) > 50 || strlen($shareClass) < 1) {
            throw new Exception("Share class has to be between 1 - 50 "
                    . "characters long");
        }
        if (strlen($currency) > 3) {
            throw new Exception('Currency can be maximum 3 characters - use ISO 4217 codes');
        }
        if ($shareValue <= 0) {
            throw new Exception('share value has to be more than 0');
        }
        if (!preg_match('/^(\d+|\d+\.(\d){1,6})$/', $shareValue)) {
            throw new Exception('Share value has to be decimal max 6 fraction digits');
        }

        /* instance variables */
        $this->shareholders = array();
        $this->shareClass   = $shareClass;
        $this->dividendId   = $dividendId;
        $this->currency     = $currency;
        $this->shareValue   = (float) $shareValue;
    }

    /**
     * Returns new DividendShare, you need to call save() to save this object 
     * in db.
     *
     * @param string $shareClass
     * @return DividendShare 
     */
    public static function getNewDividendShare($shareClass, $currency, 
            $shareValue) {

        return new DividendShare(null, $shareClass, $currency, $shareValue);
    }

    /**
     * Returns existing DividendShare, values are loaded from db
     *
     * @param int $dividendId
     * @return DividendShare 
     */
    public static function getDividendShare($dividendId) {

        /* get data from db */
        $data   = dibi::select('*')
                ->from(self::$tableName)
                ->where('dividend_id = %i', $dividendId)
                ->execute()
                ->fetch();

        $share = new DividendShare($dividendId, $data['share_class'], 
                $data['currency'], $data['share_value']);

        /* add shareholders */
        $share->shareholders = DividendShareholder::getShareholders($dividendId);

        return $share;
    }

    /**
     * Returns new DividendShare from values that are supplied as an argument.
     * Data needs to represent ch_dividend_share table, so keys are column
     * names. The compulsory fields (keys) are share_class, currency and
     * share_value - they cannot be null and have to be set.
     *
     * to save this dividend, call save() method.
     *
     * @param array $data   data representing row to be inserted in the
     *                      ch_dividend_share table
     */
    public static function getNewDbDividendShare(array $data) {

        return self::getNewDividendShare($data['share_class'], $data['currency'],
                $data['share_value']);
    }

    /**
     * Adds shareholder to the share class - so he/she will receive dividend.
     * If DividendShare is existing (already saved) this method will
     * automatically save shareholder in db. to remove it you need to call
     * remove method on the shareholder.
     *
     * @param DividendShareholder $shareholder
     */
    public function addShareholder(DividendShareholder $shareholder) {

        if (!is_null($this->dividendId)) {
            $shareholder->save($this->dividendId);
        }
        $this->shareholders[$shareholder->getShareholderId()] = $shareholder;
    }

    /**
     * Returns all sharholders that belong to this share
     *
     * @return array<DividendShareholder> 
     */
    public function getShareholders() {
        return $this->shareholders;
    }


    /**
     * returns specified shareholder, if shareholder does not exist, exception
     * is thrown
     *
     * @param int $shareholderId
     * @return DividendShareholder
     */
    public function getShareholder($shareholderId) {

        if (!isset($this->shareholders[$shareholderId])) {
            throw new Exception("Shareholder with id " . $shareholderId . " does not exist");
        }
        return $this->shareholders[$shareholderId];
    }

    /**
     * returns share class of this share
     *
     * @return string
     */
    public function getShareClass() {
        return $this->shareClass;
    }

    /**
     * returns currency of this share
     *
     * @return string
     */
    public function getCurrency() {
        return $this->currency;
    }

    /**
     * returns share value - value per share
     *
     * @return float
     */
    public function getShareValue() {
        return $this->shareValue;
    }

    /**
     * Saves data in db, if DividendShare is new, if DividendShare is existing,
     * then this method updates the data in db.
     *
     * @param int $dividendId primary key
     */
    public function save($dividendId) {

        /* data to be inserted/updated */
        $data = array(
            'share_class'   => $this->shareClass,
            'currency'      => $this->currency, 
            'share_value'   => $this->shareValue);

        /* insert */
        if (is_null($this->dividendId)) {
            $data['dividend_id'] = $dividendId;
            $this->dividendId    = $dividendId;
            dibi::insert(self::$tableName, $data)->execute();
            /* save shareholders */
            foreach($this->shareholders as $shareholder) {
                $shareholder->save($this->dividendId);
            }
        /* update */
        } else {
            dibi::update(self::$tableName, $data)->where('dividend_id=%i', $this->dividendId)->execute();
            /* no need to update shareholders, they are added automatically if there's id */
        }
    }

    /**
     * Removes DividendShare and associated shareholders from db.
     *
     */
    public function remove() {
        
        /* there's nothing to remove */
        if (is_null($this->dividendId)) {
            return;
        }

        /* remove from database */
        dibi::delete(self::$tableName)
            ->where('dividend_id=%i', $this->dividendId)
            ->execute();

        /* remove shareholders */
        /* don't have to iterate over, just call remove all on one of them */
        $shareholder = array_pop($this->shareholders);
        if (!is_null($shareholder)) {
            $shareholder->removeAll();
            $this->shareholders = array();
        }

        /* set dividedId to null - it was removed */
        $this->dividendId = null;
    }
}
