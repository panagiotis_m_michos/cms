<?php

require_once dirname(__FILE__) . '/../request/document/memorandum/fpdf/fpdf.php';

class Pdf extends FPDF {

    /* date at the bottom of the page */
    private $date;

    public function __construct($date) {
        parent::__construct('L');   // landscape
        $this->date = $date;
    }

    public function Footer() {

        $this->SetY(-15);
        $this->SetFont('Arial','',8);
        $this->Cell(100,10,'Date '.$this->date,'T',0,'L');
        $this->Cell(0,10,'Page '.$this->PageNo(),'T',0,'R');
    }
}

class PdfReport {

    /**
     * fpdf class used to create pdf
     *
     * @var fpdf
     */
    private $fpdf;

    /**
     * Dividend object that holds all the information necessary for creating
     * the report
     *
     * @var Dividend
     */
    private $dividend;

    /**
     * Company to which the shares/dividend belongs
     *
     * @var Company
     */
    private $company;

    /**
     *
     * @param Dividend $dividend    dividend from which the report should
     *                              be created
     * @return PdfReport
     */
    public function __construct(Dividend $dividend) {

        $this->dividend = $dividend;
        $this->company  = Company::getCompany($dividend->getCompanyId());

        $this->fpdf = new Pdf($dividend->getRecordDate());
    }

    private function createReport() {

        $this->fpdf->AddPage();

        $this->fpdf->SetMargins(12, 12, 12);
        $this->fpdf->SetXY(12,12);
        $this->fpdf->SetFillColor(204, 204, 204);

        $this->fpdf->SetFont('Arial', '', 10);
        $this->fpdf->Cell(0, 6, 'Dividend Report', 0, 1, 'C');

        $this->fpdf->SetFont('Arial', 'B', 16);
        $this->fpdf->Cell(0, 14, $this->company->getCompanyName(), 'B', 1, 'C');

        $this->fpdf->Ln(8);

        $this->fpdf->SetFont('Arial', 'B', 12);
        $this->fpdf->Cell(0, 8, 'Dividend on ' . $this->dividend->getShareValue() . ' ' . $this->dividend->getCurrency() . ' ' . $this->dividend->getShareClass() . ' Shares', 0, 1, 'C');

        $this->fpdf->SetFont('Arial', 'B', 8);
        $this->fpdf->SetLeftMargin(20);
        $this->fpdf->Cell(25, 5, "Accounting", "TRL", 0, 'C', true);
        $this->fpdf->Cell(25, 5, "Record", "TRL", 0, 'C', true);
        $this->fpdf->Cell(25, 5, "Payment", "TRL", 0, 'C', true);
        $this->fpdf->Cell(25, 5, "Tax", "TRL", 0, 'C', true);
        $this->fpdf->Cell(25, 5, "Net " . $this->dividend->getCurrency(), "TRL", 0, 'C', true);
        $this->fpdf->Cell(25, 5, "Period", "TRL", 0, 'C', true);
        $this->fpdf->Cell(25, 5, "Total", "TRL", 0, 'R', true);
        $this->fpdf->Cell(25, 5, "Total", "TRL", 0, 'R', true);
        $this->fpdf->Cell(25, 5, "Total", "TRL", 0, 'R', true);
        $this->fpdf->Cell(25, 5, "Total", "TRL", 1, 'R', true);

        $this->fpdf->Cell(25, 5, "Period", "RL", 0, 'C', true);
        $this->fpdf->Cell(25, 5, "Date", "RL", 0, 'C', true);
        $this->fpdf->Cell(25, 5, "Date", "RL", 0, 'C', true);
        $this->fpdf->Cell(25, 5, "Rate", "RL", 0, 'C', true);
        $this->fpdf->Cell(25, 5, "Per Share", "RL", 0, 'C', true);
        $this->fpdf->Cell(25, 5, "Type", "RL", 0, 'C', true);
        $this->fpdf->Cell(25, 5, "Shares", "RL", 0, 'R', true);
        $this->fpdf->Cell(25, 5, "Net", "RL", 0, 'R', true);
        $this->fpdf->Cell(25, 5, "Tax", "RL", 0, 'R', true);
        $this->fpdf->Cell(25, 5, "Gross", "RL", 1, 'R', true);

        $this->fpdf->SetFont('Arial', '', 8);
        $this->fpdf->Cell(25, 5, date('d/m/Y', strtotime($this->dividend->getAccountingPeriod())), "TRBL", 0, 'C');
        $this->fpdf->Cell(25, 5, date('d/m/Y', strtotime($this->dividend->getRecordDate())), "TRBL", 0, 'C');
        $this->fpdf->Cell(25, 5, date('d/m/Y', strtotime($this->dividend->getPaidDate())), "TRBL", 0, 'C');
        $this->fpdf->Cell(25, 5, $this->dividend->getTaxRate() . '%', "TRBL", 0, 'C');
        $this->fpdf->Cell(25, 5, $this->dividend->getNetDividend(), "TRBL", 0, 'C');
        $this->fpdf->Cell(25, 5, $this->dividend->getPeriodType(), "TRBL", 0, 'C');
        $this->fpdf->Cell(25, 5, $this->dividend->getNumberOfShares(), "TRBL", 0, 'R');
        $this->fpdf->Cell(25, 5, $this->dividend->getNetDividend($this->dividend->getNumberOfShares()), "TRBL", 0, 'R');
        $this->fpdf->Cell(25, 5, $this->dividend->getTax($this->dividend->getNumberOfShares()), "TRBL", 0, 'R');
        $this->fpdf->Cell(25, 5, $this->dividend->getGrossDividend($this->dividend->getNumberOfShares()), "TRBL", 1, 'R');

        $this->fpdf->Ln();

        $this->fpdf->SetFont('Arial', 'B', 8);
        $this->fpdf->Cell(25, 5, '', "RTLB", 0, 'C', true);
        $this->fpdf->Cell(125, 5, "Name and Address of Recipient of Dividend", "RTLB", 0, 'L', true);
        $this->fpdf->Cell(25, 5, "Shares", "RTLB", 0, 'R', true);
        $this->fpdf->Cell(25, 5, "Net", "RTLB", 0, 'R', true);
        $this->fpdf->Cell(25, 5, "Tax", "RTLB", 0, 'R', true);
        $this->fpdf->Cell(25, 5, "Gross", "RTLB", 1, 'R', true);

        $this->fpdf->SetFont('Arial', '', 8);
        $x = 0;
        foreach($this->dividend->getShareholders() as $shareholder) {
            $address = array();
            foreach($shareholder->getAddress()->getFields() as $field) {
                if (!is_null($field)) {
                    $address[] = $field;
                }
            }
            
            $this->fpdf->Cell(25, 5, ++$x, "RL", 0, 'C');
            $this->fpdf->Cell(125, 5, $shareholder->getName(), "RL", 0, 'L');
            $this->fpdf->Cell(25, 5, $shareholder->getNumberOfShares(), "RL", 0, 'R');
            $this->fpdf->Cell(25, 5, $this->dividend->getNetDividend($shareholder->getNumberOfShares()), "RL", 0, 'R');
            $this->fpdf->Cell(25, 5, $this->dividend->getTax($shareholder->getNumberOfShares()), "RL", 0, 'R');
            $this->fpdf->Cell(25, 5, $this->dividend->getGrossDividend($shareholder->getNumberOfShares()), "RL", 1, 'R');

            $this->fpdf->Cell(25, 5, '', "RLB", 0, 'C');
            $this->fpdf->Cell(125, 5, implode(', ', $address), "RLB", 0, 'L');
            $this->fpdf->Cell(25, 5, '', "RLB", 0, 'R');
            $this->fpdf->Cell(25, 5, '', "RLB", 0, 'R');
            $this->fpdf->Cell(25, 5, '', "RLB", 0, 'R');
            $this->fpdf->Cell(25, 5, '', "RLB", 1, 'R');
        }

        $this->fpdf->Cell(150, 5, 'Totals', '', 0, 'R');
        $this->fpdf->Cell(25, 5, $this->dividend->getNumberOfShares(), "RTBL", 0, 'R', true);
        $this->fpdf->Cell(25, 5, $this->dividend->getNetDividend($this->dividend->getNumberOfShares()), "RTBL", 0, 'R', true);
        $this->fpdf->Cell(25, 5, $this->dividend->getTax($this->dividend->getNumberOfShares()), "RTBL", 0, 'R', true);
        $this->fpdf->Cell(25, 5, $this->dividend->getGrossDividend($this->dividend->getNumberOfShares()), "RTBL", 1, 'R', true);
    }

    private function createPdf() {

        /* add page for each shareholder */
        /*
        foreach($this->dividend->getShareholders() as $shareholder) {
            $this->addVoucher($shareholder);
        }
        */
        $this->createReport();
    }

    /**
     * returns voucher as pdf.
     * If specified shareholder's id does not exist exception is thrown
     *
     * @param int $shareholderId
     */
    /*
    public function getVoucher($shareholderId) {

        $this->addVoucher($this->dividend->getShareholder($shareholderId));
        $this->fpdf->Output('Voucher.pdf','I');
        exit;
    }
    */

    /**
     * return report as pdf.
     */
    public function getReport() {

        $this->createPdf();
        $this->fpdf->Output('Voucher.pdf','I');
        exit;
    }

    /**
     * Saves report under specified file path.
     * If specified shareholder's id does not exist exception is thrown
     *
     * @param int $shareholderId
     * @param string $filePath
     */
    /*
    public function saveVoucher($shareholderId, $filePath) {

        $this->createPdf();
        $this->fpdf->Output($filePath,'F');
    }
    */

    /**
     * Save report under specified file path.
     *
     * @param string $filePath
     */
    public function saveReport($filePath) {

        $this->createPdf();
        $this->fpdf->Output($filePath,'F');
    }
}
