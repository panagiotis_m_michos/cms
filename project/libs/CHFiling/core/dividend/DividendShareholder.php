<?php

/**
 * Shareholders who are going to get dividends, they have to belong to the
 * particular share.
 *
 * @author pete
 */
class DividendShareholder {

    /**
     * table holding the data for this class
     *
     * @var string
     */
    private static $tableName = "ch_dividend_shareholder";

    /**
     * Primary key
     *
     * @var int
     */
    private $shareholderId;

    /**
     * Foreign key
     *
     * @var int
     */
    private $dividendId;

    /**
     *
     * @var string name of the shareholder or company 
     */
    private $name;

    /**
     *
     * @var Address shareholder's address 
     */
    private $address;

    /**
     *
     * @var int number of shares that shareholder holds 
     */
    private $shares;


    /**
     *
     * @param int $dividendId       primary key
     * @param int $shareholderId    foreign key
     * @param string $name          the name of the shareholder or company
     * @param Address $address      shareholder's address
     * @param int $shares           number of shares that shareholder holds
     */
    private function  __construct($dividendId, $shareholderId, $name,
            Address $address, $shares) {
        
        $this->dividendId       = $dividendId;
        $this->shareholderId    = $shareholderId;
        $this->name             = $name;
        $this->address          = $address;
        $this->shares           = $shares;
    }

    /**
     * Returns new Shareholder. After setting all the fields call save() method
     * to save in db. After that you can call
     * DividendShareholder::getShareholder($id) with the id to load shareholder
     * from db.
     *
     * @param string $name
     * @param Address $address
     * @param int $shares
     * @return DividendShareholder
     */
    public static function getNewShareholder($name, Address $address, $shares) {
        return new DividendShareholder(null, null, $name, $address, $shares);
    }

    /**
     * Returns new Shareholder, with the values supplied in the array. $data
     * argument represents table ch_dividend_shareholder, where keys are column
     * names. Call save() method to save the data in db.
     *
     * @param array $data
     * @return DividendShareholder
     */
    public static function getNewDbShareholder(array $data) {

        $data['dividend_id']    = null;
        $data['shareholder_id'] = null;
        return self::getDbShareholder($data);
    }

    /**
     * Helper method that crates shareholder from the array that represents 
     * table columns and data
     *
     * @param array $data 
     * @return DividendShareholder 
     */
    private static function getDbShareholder(array $data) {

        /* set and return Dividend */
        $address = new Address();
        $address->setFields($data);
        return new DividendShareholder($data['dividend_id'], 
                $data['shareholder_id'], $data['name'], $address, $data['shares']);
    }

    /**
     * Returns existing shareholder, loads data from db.
     *
     * @param int $shareholderId
     * @return DividendShareholder
     */
    public static function getShareholder($shareholderId) {

        /* get data from db */
        $data   = dibi::select('*')
                ->from(self::$tableName)
                ->where('shareholder_id = %i', $shareholderId)
                ->execute()
                ->fetch();

        return self::getDbShareholder((array) $data);
    }

    /**
     * Return an array of all existing shareholders, for specific dividend/share.
     *
     * @param int $dividendId
     * @return array<DividendShareholder>
     */
    public static function getShareholders($dividendId) {

        /* get data from db */
        $data   = dibi::select('*')
                ->from(self::$tableName)
                ->where('dividend_id = %i', $dividendId)
                ->execute()
                ->fetchAll();

        $shareholders = array();
        foreach($data as $d) {
            $shareholders[$d['shareholder_id']] = self::getDbShareholder((array) $d);
        }

        return $shareholders;
    }

    /**
     *
     * @return int shareholder's primary key from db 
     */
    public function getShareholderId() {
        return $this->shareholderId;
    }

    /**
     * returns name of the shareholder or company (if the shares are owned by a
     * company)
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Returns number of shares this shareholder holds
     *
     * @return int
     */
    public function getNumberOfShares() {
        return $this->shares;
    }

    /**
     * returns shareholder's address
     *
     * @return Address
     */
    public function getAddress() {
        return $this->address;
    }

    /**
     * returns shareholder's premise (address line)
     *
     * @return string
     */
    public function getPremise() {
        return $this->address->getPremise();
    }

    /**
     * returns shareholder's street (address line)
     *
     * @return string
     */
    public function getStreet() {
        return $this->address->getStreet();
    }

    /**
     * returns shareholder's thoroughfare (address line)
     *
     * @return string
     */
    public function getThoroughfare() {
        return $this->address->getThoroughfare();
    }

    /**
     * returns shareholder's post town (address line)
     *
     * @return string
     */
    public function getPostTown() {
        return $this->address->getPostTown();
    }

    /**
     * returns shareholder's county (address line)
     *
     * @return string
     */
    public function getCounty() {
        return $this->address->getCounty();
    }

    /**
     * returns shareholder's country (address line)
     *
     * @return string
     */
    public function getCountry() {
        return $this->address->getCountry();
    }

    /**
     * returns shareholder's postcode (address line)
     *
     * @return string
     */
    public function getPostcode() {
        return $this->address->getPostcode();
    }

    /**
     * Saves this shareholder, or update the data if it's existing shareholder. 
     * If this is existing shareholder, then dividendId is not checked, so it 
     * can be set to anything.
     *
     * @param int $dividendId id of the dividend to which shareholder belongs
     */
    public function save($dividendId) {

        if (is_null($this->dividendId)) {
            $this->dividendId = $dividendId;
        }

        /* data to be inserted/updated */
        $address = $this->address->getFields();
        unset($address['care_of_name']);        // these two values are not used
        unset($address['po_box']);
        unset($address['secure_address_ind']);

        $data = array(
            'dividend_id'   => $this->dividendId,
            'name'          => $this->name,
            'shares'        => $this->shares);

        $data = array_merge($address, $data);

        /* insert */
        if (is_null($this->shareholderId)) {
            $this->shareholderId = dibi::insert(self::$tableName, $data)->execute(dibi::IDENTIFIER);
        /* update */
        } else {
            dibi::update(self::$tableName, $data)->where('shareholder_id=%i', $this->shareholderId)->execute();
        }
    }

    /**
     * Removes this shareholder from db
     */
    public function remove() {

        /* there's nothing to remove */
        if (is_null($this->shareholderId)) {
            return;
        }

        /* remove from database */
        dibi::delete(self::$tableName)
            ->where('shareholder_id=%i', $this->shareholderId)
            ->execute();

        /* set companyId and shareholderId to null - it was removed */
        $this->shareholderId = null;
        $this->dividendId    = null;
    }

    /**
     * Removes all shareholders that have the same companyId, 
     */
    public function removeAll() {

        /* there's nothing to remove */
        if (is_null($this->dividendId)) {
            return;
        }

        /* remove from database */
        dibi::delete(self::$tableName)
            ->where('dividend_id=%i', $this->dividendId)
            ->execute();

        /* set companyId and shareholderId to null - it was removed */
        $this->shareholderId = null;
        $this->dividendId    = null;
    }
}
