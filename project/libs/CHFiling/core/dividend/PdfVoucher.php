<?php

use Utils\Date;

require_once dirname(__FILE__) . '/../request/document/memorandum/fpdf/fpdf.php';

class PdfVoucher {

    /**
     * fpdf class used to create pdf
     *
     * @var fpdf
     */
    private $fpdf;

    /**
     * Dividend object that holds all the information necessary for creating
     * the voucher(s)
     *
     * @var Dividend
     */
    private $dividend;

    /**
     * Company to which the shares/dividend belongs
     *
     * @var Company
     */
    private $company;

    /**
     *
     * @param Dividend $dividend    dividend from which the voucher(s) should
     *                              be created
     * @return PdfVoucher
     */
    public function __construct(Dividend $dividend) {

        $this->dividend = $dividend;
        $this->company  = Company::getCompany($dividend->getCompanyId());

        $this->fpdf = new FPDF();
    }

    /**
     * adds new page with black border
     */
    private function addPage() {

        $this->fpdf->AddPage();
        $this->fpdf->SetAutoPageBreak(false);

        /* black border - big black cell */
        $this->fpdf->SetXY(13, 13);
        $this->fpdf->SetMargins(13, 13, 13);
        $this->fpdf->SetFillColor(0, 0, 0);
        $this->fpdf->Cell(0, 271, '', 0, 0, 'L', true);

        /* white inner cell */
        $this->fpdf->SetXY(14, 14);
        $this->fpdf->SetMargins(14, 14, 14);
        $this->fpdf->SetFillColor(255, 255, 255);
        $this->fpdf->Cell(0, 269, '', 0, 0, 'L', true);
        $this->fpdf->SetFillColor(204, 204, 204);

        $this->fpdf->SetMargins(22, 22, 22);
        $this->fpdf->SetXY(22,22);
    }

    /**
     * Adds one page (shareholder)
     */
    private function addVoucher(DividendShareholder $shareholder)
    {
        $this->addPage();

        $this->fpdf->Ln(10);

        $this->fpdf->SetFont('Arial', 'B', 16);
        $this->fpdf->Cell(0, 16, $this->company->getCompanyName(), 0, 1, 'C');

        $this->fpdf->SetFont('Arial', '', 10);
        $this->fpdf->Cell(0, 6, 'Dividend on', 0, 1, 'C');
        $this->fpdf->SetFont('Arial', 'B', 10);
        $this->fpdf->Cell(0, 6, $this->dividend->getShareValue() . ' ' . $this->dividend->getCurrency() . ' ' . $this->dividend->getShareClass() . ' Shares', 0, 1, 'C');
        if ($this->dividend->hasTaxRate()) {
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Cell(0, 6, 'Tax Voucher', 0, 1, 'C');
        } else {
            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(0, 6, 'Dividend Statement', 0, 1, 'C');
        }

        $this->fpdf->SetFont('Arial', '', 10);
        $this->fpdf->Ln();

        $this->fpdf->Cell(4, 5);
        $this->fpdf->Cell(30, 5, "Interim /", "TLR", 0, 'C');
        $this->fpdf->Cell(4, 5);

        $this->fpdf->Cell(30, 5, "Period", "TLR", 0, 'C');
        $this->fpdf->Cell(30, 5, "Record", "TLR", 0, 'C');
        $this->fpdf->Cell(30, 5, "Payment", "TLR", 0, 'C');

        $this->fpdf->Cell(4, 5);
        $this->fpdf->Cell(30, 5, "Net " . $this->dividend->getCurrency(), "TLR", 0, 'C');
        $this->fpdf->Cell(4, 5, "", 0, 1);

        $this->fpdf->Cell(4, 5);
        $this->fpdf->Cell(30, 5, "Final", "LBR", 0, 'C');
        $this->fpdf->Cell(4, 5);

        $this->fpdf->Cell(30, 5, "Ending", "LBR", 0, 'C');
        $this->fpdf->Cell(30, 5, "Date", "LBR", 0, 'C');
        $this->fpdf->Cell(30, 5, "Date", "LBR", 0, 'C');

        $this->fpdf->Cell(4, 5);
        $this->fpdf->Cell(30, 5, "per share", "LBR", 0, 'C');
        $this->fpdf->Cell(4, 5, "", 0, 1);

        $this->fpdf->Cell(4, 6);
        $this->fpdf->Cell(30, 6, $this->dividend->getType(), "LBR", 0, 'C');
        $this->fpdf->Cell(4, 6);

        $this->fpdf->Cell(30, 6, date('d/m/Y', strtotime($this->dividend->getAccountingPeriod())), "LBR", 0, 'C');
        $this->fpdf->Cell(30, 6, date('d/m/Y', strtotime($this->dividend->getRecordDate())), "LBR", 0, 'C');
        $this->fpdf->Cell(30, 6, date('d/m/Y', strtotime($this->dividend->getPaidDate())), "LBR", 0, 'C');

        $this->fpdf->Cell(4, 6);
        $this->fpdf->Cell(30, 6, $this->dividend->getNetDividend(), "LBR", 0, 'C');
        $this->fpdf->Cell(4, 6, "", 0, 1);

        $this->fpdf->Ln();

        $this->fpdf->Cell(0, 8, $shareholder->getName(), 1, 1, 'C');

        $this->fpdf->Ln();

        if ($this->dividend->hasTaxRate()) {
            $this->fpdf->Cell(13, 10);
            $this->fpdf->Cell(44, 10, "Holding", "TLR", 0, 'C');
            $this->fpdf->Cell(4, 10);

            $this->fpdf->Cell(44, 10, "Tax Credit", "TLR", 0, 'C');

            $this->fpdf->Cell(4, 10);
            $this->fpdf->Cell(44, 10, "Dividend", "TLR", 0, 'C');
            $this->fpdf->Cell(4, 10, "", 0, 1);

            $this->fpdf->Cell(13, 6);
            $this->fpdf->Cell(44, 6, $shareholder->getNumberOfShares(), 1, 0, 'C', true);
            $this->fpdf->Cell(4, 6);

            $this->fpdf->Cell(44, 6,
                    $this->dividend->getTax($shareholder->getNumberOfShares()) . ' ' . $this->dividend->getCurrency(),
                    1, 0, 'C', true);

            $this->fpdf->Cell(4, 6);
            $this->fpdf->Cell(44, 6,
                    $this->dividend->getNetDividend($shareholder->getNumberOfShares()) . ' ' . $this->dividend->getCurrency(),
                    1, 0, 'C', true);
            $this->fpdf->Cell(4, 6, "", 0, 1);

            $this->fpdf->Ln();

            $this->fpdf->MultiCell(0, 5,
                    "This voucher should be retained, as it will be accepted by HM Revenue and Customs as evidence of tax credit at " .
                    $this->dividend->getTaxRate() .
                    "% in respect of which you may be entitled to claim relief.");

        } else {
            $this->fpdf->Cell(37, 10);
            $this->fpdf->Cell(44, 10, "Holding", "TLR", 0, 'C');
            $this->fpdf->Cell(4, 10);

            $this->fpdf->Cell(44, 10, "Dividend", "TLR", 0, 'C');
            $this->fpdf->Cell(4, 10, "", 0, 1);

            $this->fpdf->Cell(37, 6);
            $this->fpdf->Cell(44, 6, $shareholder->getNumberOfShares(), 1, 0, 'C', true);
            $this->fpdf->Cell(4, 6);

            $this->fpdf->Cell(44, 6,
                    $this->dividend->getNetDividend($shareholder->getNumberOfShares()) . ' ' . $this->dividend->getCurrency(),
                    1, 0, 'C', true);
            $this->fpdf->Cell(4, 6, "", 0, 1);

            $this->fpdf->Ln();
            $this->fpdf->Ln();
        }

        $this->fpdf->Ln();
        $this->fpdf->Ln();
        $this->fpdf->Ln();
        $this->fpdf->Ln();
        $this->fpdf->Cell(4, 6, $this->dividend->getOfficerName(), 0, 1);
        $this->fpdf->Cell(4, 6, $this->dividend->getOfficerType(), 0, 1);

        $this->fpdf->Cell(0, 60, "", 0, 1);

        /* shareholder's address */
        $address        = $shareholder->getName();
        $addressLines   = 8;                        // total number of lines for address
        foreach($shareholder->getAddress()->getFields() as $field) {
            if (!is_null($field)) {
                $address .= "\n" . $field;
                $addressLines--;                    // decrement lines, so formating will be the same
            }
        }
        for ($x = 0; $x < $addressLines; $x++) {
            $address .= "\n";                       // append missing/null lines (formatting)
        }
        $yCoordinate = $this->fpdf->GetY();         // get y coordinate for registered office
        $this->fpdf->MultiCell(80, 5, $address, 1);

        /* registered office */
        $this->fpdf->setXY(106, $yCoordinate);      // set coordinates
        $address = array();
        foreach($this->company->getRegisteredOffice()->getFields() as $field) {
            if (!is_null($field)) {
                $address[] = $field;
            }
        }
        $address = "Registered Office: \n" . implode(", ", $address) . "\n\n";
        $address .= "Registered in United Kingdom\n";
        $address .= "Registered on " . date('d/m/Y', strtotime($this->company->getIncorporationDate())) . "\n";
        $address .= "Company Number " . $this->company->getCompanyNumber() . "\n";
        $this->fpdf->MultiCell(80, 5, $address);
    }

    private function createPdf() {

        /* add page for each shareholder */
        foreach($this->dividend->getShareholders() as $shareholder) {
            $this->addVoucher($shareholder);
        }
    }

    /**
     * returns voucher as pdf.
     * If specified shareholder's id does not exist exception is thrown
     *
     * @param int $shareholderId
     */
    public function getVoucher($shareholderId) {

        $this->addVoucher($this->dividend->getShareholder($shareholderId));
        $this->fpdf->Output('Voucher.pdf','I');
        exit;
    }

    /**
     * returns all vouchers as pdf.
     */
    public function getVouchers() {

        $this->createPdf();
        $this->fpdf->Output('Voucher.pdf','I');
        exit;
    }

    /**
     * Saves voucher under specified file path.
     * If specified shareholder's id does not exist exception is thrown
     *
     * @param int $shareholderId
     * @param string $filePath
     */
    public function saveVoucher($shareholderId, $filePath) {

        $this->createPdf();
        $this->fpdf->Output($filePath,'F');
    }

    /**
     * Save vouchers under specified file path.
     *
     * @param string $filePath
     */
    public function saveVouchers($filePath) {

        $this->createPdf();
        $this->fpdf->Output($filePath,'F');
    }
}
