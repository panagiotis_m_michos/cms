<?php

/**
 * Dividend class represents dividends for one share
 * (but can have more shareholders).
 *
 * @author pete
 */
final class Dividend
{

    /**
     * table holding the data for this class
     *
     * @var string
     */
    private static $tableName = "ch_dividend";

    /**
     * Primary key
     *
     * @var int
     */
    private $dividendId;

    /**
     * Id of the company to which this dividend belongs
     *
     * @var int
     */
    private $companyId;

    /**
     * Shares type dividend.
     * Type is default ORDINARY (in our case it will stay ORDINARY).
     *
     * @var enum (ORDINARY|PREFERENCE)
     */
    private $type;

    /**
     * Directors Meeting to approve dividend was held on specified date.
     *
     * @var string YYYY-MM-DD
     */
    private $directorsMeeting;

    /**
     * End of accounting period divident applies to specified date.
     *
     * @var string YYYY-MM-DD
     */
    private $accountingPeriod;

    /**
     * Record date (in our case it is the same as directors meeting)
     *
     * @var string YYYY-MM-DD
     */
    private $recordDate;

    /**
     * Basic rate of tax. In our case it is default 10 (10%) and cannot be
     * changed
     *
     * @var float
     */
    private $taxRate;

    /**
     * Dividend - amount of dividend.
     *
     * @var number
     */
    private $dividend;

    /**
     * In our case this is NET all the time.
     *
     * @var enum (NET|GROSS)
     */
    private $declaredAs;

    /**
     * We use only INTERIM, this is the default value.
     *
     * @var enum (FINAL|INTERIM)
     */
    private $period;

    /**
     * Date on which the dividend will be paid
     *
     * @var string YYYY-MM-DD
     */
    private $paidDate;

    /**
     * Dividend belongs to this share (it is payed from this share)
     *
     * @var DividendShare
     */
    private $share;

    /**
     * This is the officer that is going to sign dividend voucher.
     * His/her name will appear on the voucher
     *
     * @var string
     */
    private $officerName;

    /**
     * This is the type of the officer that is going to sign dividend voucher.
     * It can be Director, or Secreatry.
     *
     * @var string
     */
    private $officerType;

    /**
     *
     * @param int $companyId        id of the company to which this dividend
     *                              belongs
     * @param int $dividendId
     * @param DividendShare $share  this is the share that dividends are going
     *                              to be paid from.
     * @param number $dividend      amount to be paid (as dividend) per share
     */
    private function __construct($companyId, $dividendId, DividendShare $share, $dividend, $officerName, $officerType, $taxRate = 10)
    {
        /* validate */
        if (!is_numeric($dividend)) {
            throw new Exception("Dividend " . $dividend . " has to be number");
        }

        /* default values */
        $this->type = "ORDINARY";
        $this->directorsMeeting = date("Y-m-d");
        $this->accountingPeriod = date("Y-m-d");
        $this->recordDate = date("Y-m-d");
        $this->taxRate = $taxRate;
        $this->declaredAs = "NET";
        $this->period = "INTERIM";
        $this->paidDate = date("Y-m-d");

        $this->share = $share;
        $this->dividend = $dividend;
        $this->dividendId = $dividendId;
        $this->companyId = $companyId;
        $this->officerName = $officerName;
        $this->officerType = $officerType;
    }

    /**
     * Return new dividend, you need to call save() method to save dividend in
     * db.
     *
     * @param int $companyId
     * @param DividendShare $share
     * @param number $dividend      net dividend to be paid
     * @return Dividend
     */
    public static function getNewDividend($companyId, DividendShare $share, $dividend, $taxRate = 10)
    {
        $officerName = 'Wetco Nominees Limited';
        $officerType = 'Secretary';
        return new Dividend($companyId, NULL, $share, $dividend, '', '', $taxRate);
    }

    /**
     * Helper method that crates dividend from the array that represents
     * table columns and data
     *
     * @param array $data
     * @return Dividend
     */
    private static function getDbDividend(array $data)
    {
        if (!isset($data) || empty($data)) {
            throw new Exception("Specified dividend does not exist");
        }
        if (!isset($data['dividend_id'])) {
            throw new Exception("Specified dividend does not exist");
        }

        /* set and return Dividend */
        $share = DividendShare::getDividendShare($data['dividend_id']);
        $dividend = new Dividend($data['company_id'], $data['dividend_id'], $share, $data['dividend'], $data['officer_name'], $data['officer_type'], $data['tax_rate']);
        $dividend->setDirectorsMeeting($data['directors_meeting']);
        $dividend->setAccountingPeriod($data['end_of_acc_period']);
        $dividend->setPaidDate($data['paid_date']);
        return $dividend;
    }

    /**
     * Returns existing dividend, loads data from db.
     *
     * @param int $dividendId
     * @return Dividend
     */
    public static function getDividend($companyId, $dividendId)
    {
        /* get data from db */
        $data = dibi::select('*')
            ->from(self::$tableName)
            ->where('dividend_id = %i', $dividendId)    // TODO - add AND company_id = ...
            ->and('company_id= %i', $companyId)
            ->execute()
            ->fetch();

        return self::getDbDividend((array) $data);
    }

    /**
     * Return an array of all existing dividends, for specific company.
     *
     * @param int $companyId
     * @return array<Dividend>
     */
    public static function getDividends($companyId)
    {
        /* get data from db */
        $data = dibi::select('*')
            ->from(self::$tableName)
            ->where('company_id = %i', $companyId)
            ->execute()
            ->fetchAll();

        $dividends = array();
        foreach ($data as $d) {
            $dividends[$d['dividend_id']] = self::getDbDividend((array) $d);
        }

        return $dividends;
    }

    /**
     * @param string $date DibiDateTime or 'Y-m-d'
     * @return boolean
     */
    private function isValidDate($date)
    {
        if ($date instanceof DibiDateTime) {
            $date = $date->format('Y-m-d');
        }

        /* check format */
        if (!preg_match('/^[\d]{4}-[\d]{1,2}-[\d]{1,2}$/', $date)) {
            return FALSE;
        }

        /* check if it's correct */
        $d = explode('-', $date);
        $month = $d[1];
        $day = $d[2];
        $year = $d[0];
        if (!checkdate($month, $day, $year)) {
            return FALSE;
        }

        return TRUE;
    }

    /**
     * @var string $date YYYY-MM-DD format
     */
    public function setDirectorsMeeting($date)
    {
        if (!$this->isValidDate($date)) {
            throw new Exception("Directors meeting date is not valid.");
        }
        $this->directorsMeeting = $date;
        /* in our case, record date is the same as directors meeting */
        $this->recordDate = $date;
    }

    /**
     * @param string $date YYYY-MM-DD format
     */
    public function setAccountingPeriod($date)
    {
        if (!$this->isValidDate($date)) {
            throw new Exception("End of accounting period date is not valid.");
        }
        $this->accountingPeriod = $date;
    }

    /**
     * @param string $date YYYY-MM-DD format
     */
    public function setPaidDate($date)
    {
        if (!$this->isValidDate($date)) {
            throw new Exception("Paid date is not valid.");
        }
        $this->paidDate = $date;
    }

    /**
     * This is the officer that is going to sign dividend voucher.
     * His/her name will appear on the voucher.
     *
     * @param string $name officer's name
     * @param string $type officer type - Director or Secretary
     */
    public function setOfficer($name, $type)
    {
        $this->officerName = $name;
        $this->officerType = $type;
    }

    /**
     * Returns primary key, id of this dividend
     *
     * @return int
     */
    public function getDividendId()
    {
        return $this->dividendId;
    }

    /**
     * Returns company id, foreign key
     *
     * @return int
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * returns end of accounting period date
     *
     * @return string YYYY-MM-DD format
     */
    public function getAccountingPeriod()
    {
        return $this->accountingPeriod;
    }

    /**
     * returns end of accounting period date
     *
     * @return string YYYY-MM-DD format
     */
    public function getDirectorsMeeting()
    {
        return $this->directorsMeeting;
    }

    /**
     *
     * @return enum(ORDINARY|PREFERENCE)
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * returns share class of the share from which this dividend is going to
     * be paid
     *
     * @return string
     */
    public function getShareClass()
    {
        return $this->share->getShareClass();
    }

    /**
     * returns currency of the share from which this dividend is going to
     * be paid
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->share->getCurrency();
    }

    /**
     * returns share value of the share from which this dividend is going to
     * be paid
     *
     * @return float
     */
    public function getShareValue()
    {
        return $this->share->getShareValue();
    }

    /**
     * returns number of shares (not from share!) shareholders are holding. So
     * we know how much the dividends will be in total. This value is
     * calculated from shareholders, because not all shares can be taken,
     * so from those that are not allocated dividends won't be paid
     *
     * @return int
     */
    public function getNumberOfShares()
    {
        $total = 0;
        $shareholders = $this->share->getShareholders();
        foreach ($shareholders as $shareholder) {
            $total += $shareholder->getNumberOfShares();
        }

        return $total;
    }

    /**
     * Returns array of shareholders that get this dividend
     *
     * @return array<DividendShareholder>
     */
    public function getShareholders()
    {
        return $this->share->getShareholders();
    }

    /**
     * Returns specified shareholder, if shareholder does not exist, exception
     * is thrown
     *
     * @param int $shareholderId
     * @return DividendShareholder
     */
    public function getShareholder($shareholderId)
    {
        return $this->share->getShareholder($shareholderId);
    }

    /**
     * returns net value of the dividend rounded to two decimal places. This
     * value is per share, so if you want total you need to supply the number
     * of shares as an argument.
     *
     * @param int $numberOfShares positive integer
     * @return number
     */
    public function getNetDividend($numberOfShares = NULL)
    {
        if ($numberOfShares == NULL) {
            return $this->decimalSharesConverter($this->dividend);
        }

        $netValue = $this->dividend * $numberOfShares;
        return number_format(round($netValue, 2), 2, '.', '');
    }

    /**
     * @param float $number
     * @return float
     */
    public function decimalSharesConverter($number)
    {
        $number_formated = number_format($number, 2, '.', '');
        $round_number = round($number, 5);
        if ($number_formated == $round_number) {
            return $number_formated . ' ';
        } else {
            return $round_number;
        }
    }

    /**
     * returns tax rate, in our case it will be all the time integer 10 which
     * represents 10%
     *
     * @return float
     */
    public function getTaxRate()
    {
        return $this->taxRate;
    }

    /**
     * @return bool
     */
    public function hasTaxRate()
    {
        return $this->taxRate > 0;
    }

    /**
     * returns amount of tax rounded to two decimals. This value is per share.
     * if you want total you need to supply number of shares as an argument.
     *
     * @param int $numberOfShares positive integer
     * @return number tax amount
     */
    public function getTax($numberOfShares = NULL)
    {
        return number_format(round($this->getGrossDividend($numberOfShares) - $this->getNetDividend($numberOfShares), 2), 2, '.', '');
    }

    /**
     * Returns gross dividend value rounded to two decimals. This value is
     * per share, so if you want total you need to supply number of
     * shares as an argument.
     *
     * @param int $numberOfShares positive integer
     * @return number
     */
    public function getGrossDividend($numberOfShares = NULL)
    {
        /* percentage representation of the net dividend */
        $x = 1 - ($this->taxRate / 100);

        /* get net dividend - don't use getNetDvidend because it would round it to two decimals */
        if ($numberOfShares == NULL) {
            $netDividend = $this->dividend;
        } else {
            $netDividend = $this->dividend * $numberOfShares;
        }

        /* get the gross */
        $gross = $netDividend / $x;

        /* format to two decimals */
        return number_format(round($gross, 2), 2, '.', '');
    }

    /**
     * returns record date
     *
     * @return string YYYY-MM-DD format
     */
    public function getRecordDate()
    {
        return $this->recordDate;
    }

    /**
     * returns paid date
     *
     * @return string YYYY-MM-DD format
     */
    public function getPaidDate()
    {
        return $this->paidDate;
    }

    /**
     * @return enum (FINAL|INTERIM)
     */
    public function getPeriodType()
    {
        return $this->period;
    }

    /**
     * This is the officer that is going to sign dividend voucher.
     * His/her name will appear on the voucher.
     *
     * @return string
     */
    public function getOfficerName()
    {
        return $this->officerName;
    }

    /**
     * This is the type of the officer that is going to sign dividend voucher.
     * It can be Director or Secretary
     *
     * @return string
     */
    public function getOfficerType()
    {
        return $this->officerType;
    }

    /**
     * returns all vouchers as pdf
     */
    public function getVouchers()
    {
        $vouchers = new PdfVoucher($this);
        $vouchers->getVouchers();
    }

    public function getVoucher($shareholderId)
    {
        $vouchers = new PdfVoucher($this);
        $vouchers->getVoucher($shareholderId);
    }

    public function getPdfReport()
    {
        $report = new PdfReport($this);
        $report->getReport();
    }

    /**
     * Saves data in db, when Dividend is new, if Dividend is existing, then
     * this method updates the data in db.
     */
    public function save()
    {
        /* data to be inserted/updated */
        $data = array(
            'company_id' => $this->companyId,
            'type' => $this->type,
            'directors_meeting' => $this->directorsMeeting,
            'end_of_acc_period' => $this->accountingPeriod,
            'record_date' => $this->recordDate,
            'tax_rate' => $this->taxRate,
            'dividend' => $this->dividend,
            'declared_as' => $this->declaredAs,
            'period' => $this->period,
            'officer_name' => $this->officerName,
            'officer_type' => $this->officerType,
            'paid_date' => $this->paidDate);

        /* insert */
        if ($this->dividendId == NULL) {
            $this->dividendId = dibi::insert(self::$tableName, $data)->execute(dibi::IDENTIFIER);
            /* update */
        } else {
            dibi::update(self::$tableName, $data)->where('dividend_id=%i', $this->dividendId)->execute();
        }

        /* update/save shares */
        $this->share->save($this->dividendId);
    }

    /**
     * Removes this Dividend from db and all associated Shareholders and Share.
     */
    public function remove()
    {
        /* there's nothing to remove */
        if ($this->dividendId == NULL) {
            return;
        }

        /* remove from database */
        $success = dibi::delete(self::$tableName)
            ->where('dividend_id=%i', $this->dividendId)->and('company_id=%i', $this->companyId)
            ->execute();
        if (!$success) {
            return;
        }

        /* associated share */
        $this->share->remove();

        /* set to NULL - it was removed */
        $this->dividendId = NULL;
        $this->companyId = NULL;
    }

}
