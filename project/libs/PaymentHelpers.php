<?php

/**
 * CMS
 *
 * @license    GPL
 * @category   Project
 * @package    SagePay
 * @author     Razvan Preda <razvanp@madesimplegroup.com
 * @version    2011-01-04
 */

class PaymentHelpers {
	
	public static function getPaymentResponse()
    {
        $paymentResponse = new PaymentResponse();
        $paymentResponse->setTypeId(Transaction::TYPE_SAGEPAY);
        
        
        $paymentResponse->setPaymentTransactionTime(date('Y m d H:i:s'));
        $paymentResponse->setPaymentHolderEmail('razvanp@madesimplegroup.com');
        $paymentResponse->setPaymentHolderFirstName('Razvan');
        $paymentResponse->setPaymentHolderLastName('Preda');
        $paymentResponse->setPaymentHolderAddressStreet('88 road');
        $paymentResponse->setPaymentHolderAddressCity('London');
        $paymentResponse->setPaymentHolderAddressState(NULL);
        $paymentResponse->setPaymentHolderAddressPostCode('412');
        $paymentResponse->setPaymentHolderAddressCountry('GB');
        
        $paymentResponse->setPaymentTransactionDetails(serialize(self::createSeccondSageResponse()));
        $paymentResponse->setPaymentCardNumber('0099');
        $paymentResponse->setPaymentOrderCode('{C484E9BA-3531-45E7-511B-71EEDDB2C75C}');
        $paymentResponse->setPaymentVpsAuthCode('1930151');
        $paymentResponse->setPaymentVendorTXCode('REF19bf88c54c27047bcff36ef204aa15ec13607');
        
        return $paymentResponse;
        
    }
    
    public static function createSeccondSageResponse()
    {
        $response = array(
                'Status' => 'OK',
                'StatusDetail' => "0000 : The Authorisation was Successful.",
                'VPSTxId' => "{C484E9BA-3531-45E7-511B-71EEDDB2C75C}", 
                'SecurityKey' => 'L00EVZXDY5',
                'TxAuthNo' => '1930151',
                'AVSCV2' => 'ALL MATCH',
                'AddressResult' => 'MATCHED',
                'PostCodeResult' => 'MATCHED',
                'CV2Result' => 'MATCHED',
                'CAVV' => 'AAABARR5kwAAAAAAAAAAAAAAAAA=',
                '3DSecureStatus' => 'OK'
            );
        
        return $response;
    }
}