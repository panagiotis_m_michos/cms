<?php

class CHEnvelope
{
    const ENVELOPE_FILE = '/xml/envelope.xml';

    /**
     * domain for sending xml requests (envelopes)
     * @var string
     */
    private $domain         = 'xmlgw.companieshouse.gov.uk';

    /**
     * path for sending xml requests (envelopes)
     * @var string
     */
    private $path           = '/v1-0/xmlgw/Gateway';

    /**
     * @var string
     */
    private $envelopeVersion= '1.0';

    /**
     * @var string
     */
    private $qualifier      = 'request';

    /**
     * @var string
     */
    private $method         = 'CHMD5';

    /**
     * obtained from companies house
     * @var string
     */
    private $senderID;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $emailAddress;

    /**
     * @param string $senderID
     * @param string $password
     * @param string $emailAddress
     */
    public function __construct($senderID, $password, $emailAddress = NULL)
    {
        $this->senderID         = $senderID;
        $this->password         = $password;
        $this->emailAddress     = $emailAddress;
    }

    /**
     * @param string $xml
     * @return string
     */
    private function sendHttp($xml, $timeout = 60)
    {
        $server = $this->domain . $this->path;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://' . $server);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        // how long to wait to receive a completely buffered output from the server
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        //how long to wait to make a successful connection to the server before starting to buffer the output.
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        $response = curl_exec($ch);
        $curlinfo = curl_getinfo($ch);
        curl_close($ch);

        if ($response) {
            Notifier::triggerSuccess('cms-chsearch-connectioncheck', time(), $curlinfo['connect_time']);
        } else {
            Notifier::triggerFailure('cms-chsearch-connectioncheck', time(), $curlinfo['connect_time']);
        }

        StatCollector::timing('CHSearchConnectionTime', $curlinfo['connect_time']);
        StatCollector::timing('CHSearchTotalTime', $curlinfo['total_time']);

        $response = mb_convert_encoding($response, 'UTF-8', 'UTF-8');
        return $response;
    }

    /**
     * @param CHRequest $request
     * @param string $transactionID
     * @return string
     */
    public function getResponse(CHRequest $request, $transactionID, $timeout = 60)
    {
        // --- prepare xml request for submission ---
        $xml = simplexml_load_file(dirname(__FILE__) . self::ENVELOPE_FILE);

        $xml->EnvelopeVersion = $this->envelopeVersion;
        $xml->Header->MessageDetails->Class = $request->getClass();
        $xml->Header->MessageDetails->Qualifier = $this->qualifier;
        $xml->Header->MessageDetails->TransactionID = $transactionID;
        $xml->Header->SenderDetails->IDAuthentication->SenderID = $this->senderID;
        $xml->Header->SenderDetails->IDAuthentication->Authentication->Method = $this->method;
        $xml->Header->SenderDetails->IDAuthentication->Authentication->Value = md5($this->senderID.$this->password.$transactionID);
        if ($this->emailAddress != NULL) {
            $xml->Header->SenderDetails->EmailAddress = $this->emailAddress;
        }

        // --- convert from simplexml to xml ---
        $xml = $xml->asXml();

        // --- replace 'Body' placeholder with body
        // reomove xml declaration from body, so it can be inserted
        $body = trim(preg_replace('/<\?xml.*\?>/', '', $request->getRequest(), 1));

        // insert body in the xml request - envelope
        $xml = trim(str_replace('<?Body ?>', $body, $xml));

        //echo $xml;exit;

        return $this->sendHttp($xml, $timeout);
    }
}
