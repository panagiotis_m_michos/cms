<?php
/*
+-------------------------------------------------------------------------------+
|   Copyright 2009 Peter Reisinger - p.reisinger@gmail.com                      |
|                                                                               |
|   This program is free software: you can redistribute it and/or modify        |
|   it under the terms of the GNU General Public License as published by        |
|   the Free Software Foundation, either version 3 of the License, or           |
|   (at your option) any later version.                                         |
|                                                                               |
|   This program is distributed in the hope that it will be useful,             |
|   but WITHOUT ANY WARRANTY; without even the implied warranty of              |
|   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               |
|   GNU General Public License for more details.                                |
|                                                                               |
|   You should have received a copy of the GNU General Public License           |
|   along with this program.  If not, see <http://www.gnu.org/licenses/>.       |
+-------------------------------------------------------------------------------+ 
 */

/**
 * interface is included in CHXmlGateway
 */

/**
 * NameSearch 
 * 
 * @uses CHRequest
 * @package chxmlgateway
 * @version $id$
 * @copyright 2009 Peter Reisinger
 * @author Peter Reisinger <p.reisinger@gmail.com> 
 * @license GNU General Public License
 */
class FilingHistory implements CHRequest
{
    /**
     * xml file 
     */
    const NAME_SEARCH_FILE  = "/xml/filing-history.xml";

    /**
     * class 
     *
     * class tag in the envelope
     * 
     * @var string
     * @access private
     */
    private $class          = 'FilingHistory';

    /**
     * data 
     *
     * data to be submitted
     * like company name, data set etc.
     * 
     * @var array
     * @access private
     */
    private $data           = array();

    /**
     * __construct 
     * 
     * @param string $companyNumber 
     * @access public
     * @return void
     */
    public function __construct($companyNumber)
    {
        $companyNumber = trim($companyNumber);

        $pattern = '/^[A-Z0-9]{8}$/';

        if (!preg_match($pattern,$companyNumber)) {
            trigger_error('Company number has to be in this pattern: '.$pattern, E_USER_ERROR);
        }
        $this->data['companyNumber'] = $companyNumber;
    }

    /**
     * setContinuationKey 
     * 
     * @param string $continuationKey 
     * @access public
     * @return void
     */
    public function setContinuationKey($continuationKey)
    {
        $this->data['continuationKey'] = $continuationKey;
    }

    /**
     * setCapitalDocInd 
     *
     * indicates whether capital documents are required in result
     * 
     * @param boolean $capitalDocInd 
     * @access public
     * @return void
     */
    public function setCapitalDocInd($capitalDocInd)
    {
        if ($capitalDocInd) {
            $this->data['capitalDocInd'] = true;
        } else {
            $this->data['capitalDocInd'] = false;
        }
    }


    /**
     * getClass 
     *
     * return class - this is used in the envelope
     * 
     * @access public
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * getData 
     *
     * contains all data set by user
     * 
     * @access public
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * getRequest 
     * 
     * @access public
     * @return xml
     */
    public function getRequest()
    {
        // load xml file
        $body = simplexml_load_file(dirname(__FILE__).self::NAME_SEARCH_FILE);

        // fill in compulsory fields
        $body->CompanyNumber = $this->data['companyNumber'];

        // fill in optional fields
        if (isset($this->data['capitalDocInd'])) {
            if ($this->data['capitalDocInd']) {
                $body->addChild('SearchRows', 1);
            } else {
                $body->addChild('SearchRows', 0);
            }
        }

        if (isset($this->data['continuationKey'])) {
            $body->addChild('ContinuationKey', $this->data['continuationKey']);
        }

        // return xml
        return $body->asXML();
    }
}
