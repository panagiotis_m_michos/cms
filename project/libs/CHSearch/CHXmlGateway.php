<?php
/*
+-------------------------------------------------------------------------------+
|   Copyright 2009 Peter Reisinger - p.reisinger@gmail.com                      |
|                                                                               |
|   This program is free software: you can redistribute it and/or modify        |
|   it under the terms of the GNU General Public License as published by        |
|   the Free Software Foundation, either version 3 of the License, or           |
|   (at your option) any later version.                                         |
|                                                                               |
|   This program is distributed in the hope that it will be useful,             |
|   but WITHOUT ANY WARRANTY; without even the implied warranty of              |
|   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               |
|   GNU General Public License for more details.                                |
|                                                                               |
|   You should have received a copy of the GNU General Public License           |
|   along with this program.  If not, see <http://www.gnu.org/licenses/>.       |
+-------------------------------------------------------------------------------+ 
 */

/**
 * include parent interface used by all requests
 */
require_once('core/CHRequest.php');

/**
 * CHXmlGateway 
 * 
 * @package chxmlgateway
 * @version $id$
 * @copyright 2009 Peter Reisinger
 * @author Peter Reisinger <p.reisinger@gmail.com> 
 * @license GNU General Public License
 */
class CHXmlGateway
{
    /**
     * file used to generate unique transaction id
     */
    const TRANS_ID_FILE = '/trans/request.txt';

    /**
     * envelope 
     * 
     * @var Envelope
     * @access private
     */
    private $envelope;

    /**
     * __construct 
     * 
     * @access public
     * @return void
     */
    public function __construct()
    {
        // --- values from companies house  ---
        $senderID       = '84252070909205726479271288903567';   // change
        $password       = 'me62za2he8svls957rqjjvihtwft9dmj';   // change
        $emailAddress   = NULL;                                 // set to your email or leave null
        // ---  --- --- --- --- --- --- --- ---

        // --- include Envelope class ---
        require_once('core/CHEnvelope.php');
        // --- create instance of envelope ---
        $this->envelope = new CHEnvelope($senderID, $password, $emailAddress);
    }

    /**
     * getTransactionID 
     * 
     * @access private
     * @return int
     */
    private function getTransactionID()
    {
        //$time = $_SERVER["REQUEST_TIME"];
        //try {
            //$fh = fopen(dirname(__FILE__).self::TRANS_ID_FILE, "r+");
            //$size = filesize(dirname(__FILE__).self::TRANS_ID_FILE);
            //flock($fh,LOCK_EX);
            //if ($size == 0) {
                //fwrite($fh,'1,'.$time);
                //$number = 1;
            //} else {
                //$data = explode(",",fread($fh,$size));
                //$number = (int) $data[0];
                //$number++;
                //rewind($fh);
                //fwrite($fh,$number.','.$time);
            //}
            //fflush($fh);
            //flock($fh,LOCK_UN);
            //fclose($fh);
        //} catch (Exception $e) {
            //die($e->__toString());
        //}
        //return $number;
        return uniqid('', TRUE) . uniqid();
    }

    /**
     * getNameSearch 
     * 
     * @param string $companyName 
     * @param string $dataSet LIVE, DISSOLVED, FORMER, PROPOSEDLIVE, DISSOLVED, FORMER, PROPOSED
     * @access public
     * @return NameSearch
     */
    public function getNameSearch($companyName, $dataSet)
    {
        require_once('core/NameSearch.php');
        return new NameSearch($companyName, $dataSet);
    }

    /**
     * getNumberSearch 
     * 
     * @param string $partialCompanyNumber 
     * @param array $dataSet LIVE, DISSOLVED, FORMER, PROPOSED
     * @access public
     * @return NumberSearch
     */
    public function getNumberSearch($partialCompanyNumber, array $dataSet)
    {
        require_once('core/NumberSearch.php');
        return new NumberSearch($partialCompanyNumber, $dataSet);
    }

    /**
     * getCompanyDetails 
     * 
     * @param string $companyNumber 
     * @access public
     * @return CompanyDetails
     */
    public function getCompanyDetails($companyNumber)
    {
        require_once('core/CompanyDetails.php');
        return new CompanyDetails($companyNumber);
    }

    /**
     * getMortgages 
     * 
     * @param string $companyNumber 
     * @param string $companyName 
     * @access public
     * @return Mortgages
     */
    public function getMortgages($companyNumber, $companyName)
    {
        require_once('core/Mortgages.php');
        return new Mortgages($companyNumber, $companyName);
    }

    /**
     * getOfficerSearch 
     * 
     * @param string $surname 
     * @param string $officerType CUR | LLP | DIS | EUR  
     * @access public
     * @return OfficerSearch
     */
    public function getOfficerSearch($surname, $officerType)
    {
        require_once('core/OfficerSearch.php');
        return new OfficerSearch($surname, $officerType);
    }

    /**
     * getResponse 
     *
     * returns xml response from companies house
     * 
     * @param CHRequest $request 
     * @access public
     * @return xml
     */
    public function getResponse(CHRequest $request, $timeout = 60)
    {
        // --- response xml from companies house ---
        return $this->envelope->getResponse($request, $this->getTransactionID(), $timeout);
    }
    
    /**
     * getFilingHistory 
     * 
     * @param string $companyNumber 
     * @access public
     * @return FilingHistory
     */
    public function getFilingHistory($companyNumber)
    {
        require_once('core/FilingHistory.php');
        return new FilingHistory($companyNumber);
    }

}
