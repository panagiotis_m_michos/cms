<?php

namespace Helpers\Admin\Template;

use DateTime;
use Entities\Cashback;
use Entities\Customer;
use Nette\Object;
use Nette\Utils\Arrays;

class CashbackHelper extends Object
{
    /**
     * @var Customer
     */
    private $customer;
    /**
     * @var Cashback[] array
     */
    private $cashbacks;

    /**
     * @param Customer $customer
     * @param array $cashbacks
     */
    public function __construct(Customer $customer, array $cashbacks)
    {
        $this->customer = $customer;
        $this->cashbacks = $cashbacks;
    }

    /**
     * @return array
     */
    public function getCashbacks()
    {
        return $this->cashbacks;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @return int
     */
    public function getTotalAmount()
    {
        $total = 0;
        foreach ($this->cashbacks as $cashback) {
            $total += $cashback->getAmount();
        }
        return $total;
    }

    /**
     * @return DateTime
     */
    public function getDatePaid()
    {
        foreach ($this->cashbacks as $cashback) {
            return $cashback->getDatePaid();
        }
    }

    /**
     * @return mixed
     */
    public function getStatusId()
    {
        foreach ($this->cashbacks as $cashback) {
            return $cashback->getStatusId();
        }
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return Arrays::get(Cashback::$statuses, $this->getStatusId(), NULL);
    }

    /**
     * @return bool
     */
    public function isPaid()
    {
        return ($this->getStatusId() === Cashback::STATUS_PAID);
    }

    /**
     * @return bool
     */
    public function isCashbackTypeBank()
    {
        return $this->customer->getCashbackType() == Cashback::CASHBACK_TYPE_BANK;
    }

    /**
     * @return bool
     */
    public function isCashbackTypeCredits()
    {
        return $this->customer->getCashbackType() == Cashback::CASHBACK_TYPE_CREDITS;
    }
}
