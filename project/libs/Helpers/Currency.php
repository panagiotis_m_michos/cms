<?php

namespace Helpers;

use Nette\Utils\Arrays;

class Currency
{
    const ISO_GBP = 'GBP';

    /**
     * @var array
     */
    public static $currencySymbols = array(
        self::ISO_GBP => '£',
    );

    /**
     * @param mixed $value
     * @param int $decimals
     * @param string $currencyIso
     * @return string
     */
    public static function format($value, $decimals = 2, $currencyIso = self::ISO_GBP)
    {
        return Arrays::get(self::$currencySymbols, $currencyIso, '') . number_format($value, $decimals);
    }
}
