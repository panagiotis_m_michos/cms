<?php

// include ch xml gateway class		
require_once('CHSearch/CHXmlGateway.php');
		
class SearchByUrl
{	
	private $xmlGateway;
	
	public function __constructor(){}
	
	public function getXmlGateway()
	{
		return $this->xmlGateway;
	}
	
	public function setXmlGateway()
	{
		// create instance of xml gateway class
		$this->xmlGateway = new CHXmlGateway();
	}
	
	public function doSearch($companyName)
	{
		$company = self::clearCompanyName($companyName);		
		$response = $this->searchByName($company);		
		if(self::checkErrors($response))
		{
			throw new Exception($response); 
		}
		else
		{
			if(self::getElementValue($response, 'SearchMatch') == 'NEAR')
			{				
				return false;				
			}
			elseif(self::getElementValue($response, 'SearchMatch') == 'EXACT')
			{	
				$company2 = str_replace('&', '&amp;', $company);			
				$responsedName = strtolower(self::getElementValue($response, 'CompanyName'));		
				//get company number and do search by company number 				
				if($responsedName == ($company || $company2))
				{
					$response = $this->searchCompanyDetailsByNumber(self::getElementValue($response, 'CompanyNumber'));					
					if(self::checkErrors($response))
					{
						  throw new Exception($response);
					}
					else
					{
						//do everything here with company details										
						return $response;
					}
				}
				else
				{	
									
					return false;					
				}
			}
		}
	}	
	
	public function searchList($company, $rows)
	{
		$company = self::clearCompanyName($company);
		$response = $this->searchByName($company, $rows);
		if(self::checkErrors($response))
		{
			throw new Exception($response);
		}
		else
		{
			return $response;
		}
	}
	
	public function searchFiles($companyNumber)
	{
		try{
			$files = $this->searchFilesByNumber($companyNumber);
		}
		catch(exception $e)
		{
			print_r($e->getMessage());
			exit;
		}
		if(String::checkEncoding($files, 'UTF-8'))
		{
			$xml = simplexml_load_string($files);
			$files = (array) $xml->Body->FilingHistory;
		}
		else
		{
			$files = array();
		}
		return $files;
	}
	
	public static function list2Array($xml)
	{
		$list = array();
		$xml = @simplexml_load_string($xml);
		if ($xml !== FALSE && isset($xml->Body->NameSearch)) {
			$array = (array) $xml->Body->NameSearch;
			if(!isset($array['CoSearchItem'])) return array();		
			foreach($array['CoSearchItem'] as $key => $value)
			{
				$list[$key] = (array) $value;
				$list[$key]['urlName'] = (string) str_replace(' ', '_', strtolower($value->CompanyName));
			}	
			return $list;
		}
		return $list;
	}
	
	public static function getContinuationKey($xml)
	{
		$xml = @simplexml_load_string($xml);
		if ($xml !== FALSE && isset($xml->Body->NameSearch->ContinuationKey)) {
			$key = (string) $xml->Body->NameSearch->ContinuationKey;
			return $key;
		}
		return FALSE;
	}
	
	public static function getRegressionKey($xml)
	{
		$xml = @simplexml_load_string($xml);
		if ($xml !== FALSE && isset($xml->Body->NameSearch->RegressionKey)) {
			$key = (string) $xml->Body->NameSearch->RegressionKey;
			return $key;
		}
		return FALSE;
	}
	
	public static function companyDetails2Array($xml)
	{
		$details = array();
		$xml = @simplexml_load_string($xml);
		if ($xml !== FALSE && isset($xml->Body->CompanyDetails)) {
			$details = (array) $xml->Body->CompanyDetails;
		}
		return $details;
	}
	
	private static function checkErrors($response)
	{
		if(self::getElementValue($response, 'Qualifier') == 'error')
		{
			return true;
		}
		return false;		
	}
	
	private static function clearCompanyName($name)
	{		
		return strtolower(str_replace('_', ' ', $name));
	}
	
	private function searchByName($companyName, $rows = 1)
	{		
		// get object for appropriate search
		// in this case, first argument is company name and second is data set
		$nameSearch = $this->xmlGateway->getNameSearch($companyName, 'LIVE');
		
		// now you can set optional paramenters if needed
		$nameSearch->setSearchRows($rows);
		
		// call getResponse method, that takes one argument and that is request 
		// we want to make. In this case it is nameSearch object
		return $this->xmlGateway->getResponse($nameSearch);
	}
	
	public function searchNextByName($companyName, $rows, $continuationKey)
	{
		$nameSearch = $this->xmlGateway->getNameSearch($companyName, 'LIVE');
		$nameSearch->setSearchRows($rows);
		$nameSearch->setContinuationKey($continuationKey);
		//pr($this->xmlGateway->getResponse($nameSearch));exit;
		return $this->xmlGateway->getResponse($nameSearch);
	}
	
	public function searchPreviousByName($companyName, $rows, $regressionKey)
	{
		$nameSearch = $this->xmlGateway->getNameSearch($companyName, 'LIVE');
		$nameSearch->setSearchRows($rows);
		$nameSearch->setRegressionKey($regressionKey);
		//pr($this->xmlGateway->getResponse($nameSearch));exit;
		return $this->xmlGateway->getResponse($nameSearch);
	} 
	
	private function searchCompanyDetailsByNumber($companyNumber)
	{		
		// get object for appropriate search
		// in this case, argument is company number
		$companyDetails = $this->xmlGateway->getCompanyDetails($companyNumber);

		// call getResponse method, that takes one argument and that is request 
		// we want to make. In this case it is companyDetails object
		return $this->xmlGateway->getResponse($companyDetails);		
	}
	
	
	private function searchFilesByNumber($companyNumber)
	{		
		// get object for appropriate search
		// in this case, argument is company number
		try{
			$files = $this->xmlGateway->getFilingHistory($companyNumber);
		}
		catch(exception $e)
		{
			throw new exception("The phrase is illegal.");
		}		

		// call getResponse method, that takes one argument and that is request 
		// we want to make. In this case it is filingHistory object
		return $this->xmlGateway->getResponse($files);		
	}
	
	
   	/**
	 * getElementValue
     *
     * gets element's value from xml
     *
     * @param string $xml 
     * @param string $element_name
     * @static
     * @access private
     * @return string element value
     *
	 */	
    private static function getElementValue($xml, $element_name)
    {
    	$pattern = '<'.$element_name.'>';    	
    	$result = split($pattern, $xml);
    	$pattern = '</'.$element_name.'>';
    	if(isset($result[1]))
    	{
    		$result = split($pattern, $result[1]);    	  
    	}
    	if(isset($result[0]))
    	{
    		return $result[0];
    	}
    	return;
    }
	
	public static function cutString($str, $limit)
	{
		$length = strlen($str);
		if($length <= $limit)
		{
			$result = $str;
		}
		else
		{
			$result = substr($str, 0, $limit);
		}
		return $result;
	}
	
	
}
