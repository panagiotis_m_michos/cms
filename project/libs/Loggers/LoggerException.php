<?php

namespace Loggers;

use Exception;

class LoggerException extends Exception
{
    /**
     * @param $level
     * @return self
     */
    public static function wrongLogLevel($level)
    {
        return new self("Wrong log level received: {$level}");
    }
}
