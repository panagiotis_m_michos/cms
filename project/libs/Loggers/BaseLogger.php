<?php

namespace Loggers;

use ILogger;
use Debug;

abstract class BaseLogger implements ILogger
{
    /**
     * @var string
     */
    private $logPath;

    /**
     * @var int
     */
    private $level;

    /**
     * @param string $logPath
     * @param int $level
     * @throws LoggerException
     */
    public function __construct($logPath, $level = self::DEBUG)
    {
        if (empty($logPath)) {
            throw new LoggerException('Log file has to be specified');
        }

        if (!file_exists($logPath)) {
            if (fopen($logPath, 'w') === FALSE) {
                throw new LoggerException("Can't create file `{$logPath}`");
            }
        }

        $this->logPath = $logPath;
        $this->level = $level;
    }

    /**
     * @param int $level
     * @param string $message
     * @throws LoggerException
     * @return int
     */
    public function logMessage($level, $message = NULL)
    {
        //there is no need to output messages greater than the default level
        if ($level > $this->level) {
            return;
        }

        $messageType = $this->getMessageType($level);
        $text = date('Y-m-d H:i:s') . ': ' . $messageType . ': ' . $message . PHP_EOL;
        $appendStatus = file_put_contents($this->logPath, $text, FILE_APPEND);
        if ($appendStatus === FALSE) {
            throw new LoggerException("File `{$this->logPath}` is not writeable");
        }

        if ($level === self::ERROR && class_exists('Debug')) {
            Debug::log('Error! Message: ' . $message, Debug::ERROR);
        }
        return $appendStatus;
    }

    /**
     * @param int $level
     */
    public function setMinimumLogLevel($level)
    {
        $this->level = $level;
    }

    /**
     * @return string
     */
    public function getLogPath()
    {
        return $this->logPath;
    }

    /**
     * @param string $level
     * @return string
     */
    private function getMessageType($level)
    {
        switch ($level) {
            case self::ERROR:
                return 'Error';
            case self::WARNING:
                return 'Warning';
            case self::DEBUG:
                return 'Debug';
            default:
                return 'Info';
        }
    }
}
