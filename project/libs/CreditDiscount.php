<?php

class CreditDiscount
{

    const CREDIT_ADD_STARTED = '2011-10-31';
    const COMPANIES_BOUGHT_LIMIT = 10;
    const AMOUNT = 20;

    /**
     * @var array
     */
    public static $productIds = [
        Package::PACKAGE_WHOLESALE_EXECUTIVE,
        Package::PACKAGE_WHOLESALE_PROFESSIONAL
    ];

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var PaymentEmailer
     */
    private $paymentEmailer;

    /**
     * @param Customer $customer
     * @param PaymentEmailer $paymentEmailer
     */
    public function __construct(Customer $customer, PaymentEmailer $paymentEmailer)
    {
        $this->customer = $customer;
        $this->paymentEmailer = $paymentEmailer;
    }

    public function applyCreditBonus()
    {
        if ($this->customer->isWholesale()) {
            $timesToAdd = $this->getTimesToAddCredit();
            while ($timesToAdd-- > 0) {
                $this->addCredit(self::AMOUNT);
            }
        }
    }

    /**
     * @return int
     */
    protected function getTimesToAddCredit()
    {
        $fromDate = $this->getLastCreditAddedDate();
        $fromDate = $fromDate ? $fromDate : self::CREDIT_ADD_STARTED;
        $timesBought = Order::getTotalProductsByDate($this->customer, self::$productIds, $fromDate);
        if ($timesBought >= self::COMPANIES_BOUGHT_LIMIT) {
            return (int) ($timesBought / self::COMPANIES_BOUGHT_LIMIT);
        }

        return 0;
    }

    /**
     * @param float $amount
     */
    protected function addCredit($amount)
    {
        $this->customer->credit += $amount;
        $this->customer->save();
        $this->logCreditAdded($amount);
        $this->paymentEmailer->sendEmailCreditAdded($amount, $this->customer);
    }

    /**
     * @param string $amount
     */
    protected function logCreditAdded($amount)
    {
        $arr['dtc'] = new DibiDateTime();
        $arr['customerId'] = $this->customer->getId();
        $arr['amount'] = $amount;
        $arr['dtm'] = new DibiDateTime();
        FApplication::$db->insert(TBL_CREDITS_ADDED, $arr)->execute();
    }

    /**
     * @return string
     */
    protected function getLastCreditAddedDate()
    {
        $result = dibi::select('dtc')
            ->from(TBL_CREDITS_ADDED)
            ->where('customerId=%i', $this->customer->getId())
            ->orderBy('dtc', dibi::DESC)
            ->limit(1)
            ->execute()->fetchSingle();

        return $result;
    }

}
