<?php
use CommonModule\Mappers\IMapper;
use Doctrine\ORM\Internal\Hydration\IterableResult;

/**
 * @package     Company Searches Made Simple
 * @subpackage  CMS
 * @author      Stan (diviak@gmail.com)
 * @internal    project/custom/Array2Csv.php
 * @created     26/03/2009
 */


class Array2Csv
{
    const COLUMN_SEPARATOR = ',';
    const ROW_SEPARATOR = "\n";

    /**
     * Returns CSV content
     * 
     * @param array $data
     * @return string
     */
    public static function getContent(array $data)
    {
        $output = '';
        foreach ($data as $key => $row) {
            foreach ($row as $key2 => $column) {
                $column = str_replace(self::COLUMN_SEPARATOR, '', $column);
                $column .= self::COLUMN_SEPARATOR;
                 $column = str_replace(self::COLUMN_SEPARATOR . '$', '', $column);
                 $column = preg_replace('/\r\n|\n\r|\n|\r/', ' ', $column);
                 $output .= $column;
            }
            $output = substr($output, 0, -1);
            $output .= self::ROW_SEPARATOR;
        }
        $output = trim($output);
        return $output;
    }

    /**
     * Provides output CSV file
     * 
     * @param array $data
     * @param string $filename
     * @return void
     */
    public static function output(array $data, $filename = 'file.csv')
    {
        header('Content-type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . $filename);
        header('Pragma: no-cache');
        header('Expires: 0');
        echo self::getContent($data);
        exit;
    }

    /**
     * @param IterableResult $iterator
     * @param IMapper $mapper
     * @return string
     */
    public static function getContentFromDoctrineIterator(IterableResult $iterator, IMapper $mapper)
    {
        $data = [];
        foreach ($iterator as $row) {
            $data[] = $mapper->toArray($row[0]);
        }
        if (!empty($data[0])) {
            $keys = array_keys($data[0]);
            array_unshift($data, array_combine($keys, $keys));
        }
        return self::getContent($data);
    }
}