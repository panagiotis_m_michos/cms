<?php

namespace Console\Commands\Tokens;

use Doctrine\ORM\EntityManager;
use Entities\Payment\Token;
use Entities\Payment\TokenHistory;
use Entities\Transaction;
use Loggers\NikolaiLogger;
use SagePay\Reporting\Request\TransactionDetails as TransactionDetailsRequest;
use SagePay\Reporting\Response\TransactionDetails as TransactionDetailsResponse;
use SagePay\Reporting\ResponseException;
use SagePay\Reporting\SagePay;
use SagePay\Reporting\SagePayFactory;
use SagePay\Token\Exception\FailedResponse;
use SagePay\Token\Request\Remove as RemoveRequest;
use SagePay\Token\SagePay as SageToken;
use SagePay\Token\SageFactory as SageTokenFactory;
use Services\Payment\TokenService;
use Services\TransactionService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UsCardsBillingStateUpdateCommand extends Command
{
    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * @var NikolaiLogger
     */
    private $logger;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var TokenService
     */
    private $tokenService;

    /**
     * @var SagePayFactory
     */
    private $sagePayFactory;

    /**
     * @var TransactionService
     */
    private $transactionService;

    /**
     * @param EntityManager $entityManager
     * @param NikolaiLogger $logger
     * @param TokenService $tokenService
     * @param SagePayFactory $sagePayFactory
     * @param TransactionService $transactionService
     * @param null $name
     */
    public function __construct(
        EntityManager $entityManager,
        NikolaiLogger $logger,
        TokenService $tokenService,
        SagePayFactory $sagePayFactory,
        TransactionService $transactionService,
        $name = NULL
    )
    {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
        $this->tokenService = $tokenService;
        $this->sagePayFactory = $sagePayFactory;
        $this->transactionService = $transactionService;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('tokens:usCardsBillingStateUpdate')
            ->setDescription('Updates Billing Address for US Cards if empty.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var SagePay $sagePay */
        $sagePay = $this->sagePayFactory->createGateway(VENDOR_DIR);
        $rows = $this->tokenService->getUsCardsWithNoBillingState();
        $processed = $updated = 0;
        foreach ($rows as $row) {
            /** @var Token $token */
            $token = $row[0];
            $output->writeln("<info>Processing token id: {$token->getId()}</info>");
            $customer = $token->getCustomer();
            $transactions = $this->transactionService->getTransactions($customer);
            $count = count($transactions);
            if ($count == 1) {
                /** @var Transaction $transaction */
                $transaction = $transactions[0];
                try {
                    /** @var TransactionDetailsResponse $response */
                    $response = $sagePay->getTransactionDetails(new TransactionDetailsRequest($transaction->getVendorTXCode()));
                    $token->setBillingState($response->getBillingState());
                    $this->tokenService->saveTokenWithHistory($token, TokenHistory::STATUS_COMPLETE, $this->getName());
                    $updated++;
                } catch (ResponseException $e) {
                    $output->writeln("<warning>Couldn't retrieve details for Transaction: {$transaction->getId()} ({$transaction->getVendorTXCode()}). ERROR: {$e->getMessage()}</warning>");
                    $output->writeln("<warning>Token: {$token->getIdentifier()} ({$token->getId()}) has not been processed.</warning>");
                }
            } elseif ($count > 1) {
                $matchFound = FALSE;
                foreach ($transactions as $transaction) {
                    /** @var Transaction $transaction */
                    if ($transaction->getDtc()->format('d-m-y') == $token->getDtc()->format('d-m-y') && ($transaction->getVendorTXCode() !== NULL) ) {
                        try {
                            /** @var TransactionDetailsResponse $response */
                            $response = $sagePay->getTransactionDetails(new TransactionDetailsRequest($transaction->getVendorTXCode()));
                            $token->setBillingState($response->getBillingState());
                            $this->tokenService->saveTokenWithHistory($token, TokenHistory::STATUS_COMPLETE, 'tokens:usCardsBillingStateUpdate');
                            $matchFound = TRUE;
                            $updated++;
                        } catch (ResponseException $e) {
                            $output->writeln("<warning>Couldn't retrieve details for Transaction: {$transaction->getId()} ({$transaction->getVendorTXCode()}). ERROR: {$e->getMessage()}</warning>");
                            $output->writeln("<warning>Token: {$token->getIdentifier()} ({$token->getId()}) has not been processed.</warning>");
                        }
                    }
                }
                if(!$matchFound){
                    $this->removeToken($token, $output);
                }
            }
            $processed++;
        }
        $output->writeln("<info>{$processed} tokens processed and {$updated} updated.</info>");
    }

    /**
     * @param Token $token
     * @param OutputInterface $output
     */
    protected function removeToken(Token $token, OutputInterface $output)
    {
        $token->setSageStatus(Token::SAGE_STATUS_DELETED);
        $storage = SageTokenFactory::createStorage();
        /** @var SageToken $sageToken */
        $sageToken = SageTokenFactory::createGateway($storage);
        try {
            $request = new RemoveRequest();
            $request->setToken($token->getIdentifier());
            $response = $sageToken->removeToken($request);
        } catch(FailedResponse $e) {
            $output->writeln("<warning>Couldn't remove Token: {$token->getIdentifier()}. ERROR: {$e->getMessage()}</warning>");
        }
    }
} 