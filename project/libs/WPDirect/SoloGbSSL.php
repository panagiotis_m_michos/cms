<?php
/*
+-------------------------------------------------------------------------------+
|   Copyright 2009 Peter Reisinger - p.reisinger@gmail.com                      |
|                                                                               |
|   This program is free software: you can redistribute it and/or modify        |
|   it under the terms of the GNU General Public License as published by        |
|   the Free Software Foundation, either version 3 of the License, or           |
|   (at your option) any later version.                                         |
|                                                                               |
|   This program is distributed in the hope that it will be useful,             |
|   but WITHOUT ANY WARRANTY; without even the implied warranty of              |
|   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               |
|   GNU General Public License for more details.                                |
|                                                                               |
|   You should have received a copy of the GNU General Public License           |
|   along with this program.  If not, see <http://www.gnu.org/licenses/>.       |
+-------------------------------------------------------------------------------+ 
 */

/**
 * include parent class
 */
require_once('WPPaymentDetails.php');

/**
 * SoloGbSSL 
 * 
 * @uses WPPaymentDetails
 * @package wpdirect
 * @version $id$
 * @copyright 2009 Peter Reisinger
 * @author Peter Reisinger <p.reisinger@gmail.com> 
 * @license GNU General Public License
 */
class SoloGbSSL extends WPPaymentDetails
{
    /**
     * __construct 
     *
     * You have to set issueNumber or startDate + optionaly issue Number
     * 
     * @param string $cardNumber 
     * @param array $expiryDate array('month' => MM, 'year' => YYYY)
     * @param string $cardHolderName 
     * @param mixed $issueNumber optional
     * @param array $startDate optional array('month' => MM, 'year' => YYYY)
     * @access public
     * @return void
     */
    public function __construct($cardNumber, $expiryDate, $cardHolderName, $issueNumber = NULL, $startDate = NULL)
    {
        $cardNumber = str_replace(array('-', ' '), '', $cardNumber);


        if (is_null($startDate) && is_null($issueNumber)) {
            trigger_error('You have to set issueNumber or startDate + optionaly issue Number', E_USER_NOTICE);
        }

        parent::__construct('SOLO_GB-SSL');

        $this->setCardNumber($cardNumber);
        $this->setExpiryDate($expiryDate);
        $this->setCardHolderName($cardHolderName);

        if (!is_null($startDate)) {
            $this->setStartDate($startDate);
        }
        if (!is_null($issueNumber)) {
            $this->setIssueNumber($issueNumber);
        }
    }

    /**
     * setCvc 
     * 
     * @param mixed $cvc 
     * @access public
     * @return void
     */
    public function setCvc($cvc)
    {
        parent::setCvc($cvc); 
    }

    /**
     * setCardAddress 
     *
     * allowed values:
     *  --- compulsory ---
     *   - street
     *   - postalCode
     *   - countryCode
     *  --- optional ---
     *   - firstName
     *   - lastName
     *   - houseName
     *   - houseNumber
     *   - houseNumberExtension
     *   - city
     *   - state
     *   - telephoneNumber
     * 
     * @param array $cardAddress 
     * @access public
     * @return void
     */
    public function setCardAddress($cardAddress)
    {
        parent::setCardAddress($cardAddress); 
    }

    /**
     * setCardSwipe 
     * 
     * @param mixed $cardSwipe 
     * @access public
     * @return void
     */
    public function setCardSwipe($cardSwipe)
    {
        parent::setCardSwipe($cardSwipe); 
    }
}
