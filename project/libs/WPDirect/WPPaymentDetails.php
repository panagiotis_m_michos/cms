<?php
/*
+-------------------------------------------------------------------------------+
|   Copyright 2009 Peter Reisinger - p.reisinger@gmail.com                      |
|                                                                               |
|   This program is free software: you can redistribute it and/or modify        |
|   it under the terms of the GNU General Public License as published by        |
|   the Free Software Foundation, either version 3 of the License, or           |
|   (at your option) any later version.                                         |
|                                                                               |
|   This program is distributed in the hope that it will be useful,             |
|   but WITHOUT ANY WARRANTY; without even the implied warranty of              |
|   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               |
|   GNU General Public License for more details.                                |
|                                                                               |
|   You should have received a copy of the GNU General Public License           |
|   along with this program.  If not, see <http://www.gnu.org/licenses/>.       |
+-------------------------------------------------------------------------------+ 
 */

/**
 * WPPaymentDetails 
 * 
 * @abstract
 * @package wpdirect
 * @version $id$
 * @copyright 2009 Peter Reisinger
 * @author Peter Reisinger <p.reisinger@gmail.com> 
 * @license GNU General Public License
 */
abstract class WPPaymentDetails
{
    /**
     * type 
     *
     * direct child of paymentDetails
     * 
     * @var string
     * @access private
     */
    private $type;

    /**
     * paymentDetails 
     *
     * array with all set payment details
     * 
     * @var array
     * @access private
     */
    private $paymentDetails     = array();

    /**
     * __construct 
     *
     * direct child of paymentDetails tag
     * 
     * @param string $type type of payment, ex: VISA-SSL 
     * @access protected
     * @return void
     */
    protected function __construct($type)
    {
        // --- allowed types ---
        $types = array(
            'AMEX-SSL'      => true,    'DINERS-SSL'=> true,    'ELV-SSL'       => true, 
            'JCB-SSL'       => true,    'ECMC-SSL'  => true,    'SOLO_GB-SSL'   => true, 
            'MAESTRO-SSL'   => true,    'VISA-SSL'  => true
        );

        // --- check if this value is allowed ---
        if (!$types[$type]) {
            trigger_error('This type of payment is not supported', E_USER_NOTICE);
        }

        $this->type = $type;
    }

    /**
     * setCardNumber 
     * 
     * @param string $cardNumber 
     * @access protected
     * @return void
     */
    protected function setCardNumber($cardNumber)
    {
        $this->paymentDetails['cardNumber'] = $cardNumber;
    }

    /**
     * setExpiryDate 
     *
     * expiry date has to be bigger than today's day
     * 
     * @param array $expiryDate array('month' => MM, 'year' => YYYY)
     * @access protected
     * @return void
     */
    protected function setExpiryDate(array $expiryDate)
    {
        if (!isset($expiryDate['month'], $expiryDate['year'])) {
            trigger_error('Expiry date has to be array with keys: month and year', E_USER_NOTICE);
        }

        // pad month, in case we received one digit number
        $expiryDate['month'] = str_pad($expiryDate['month'], 2, 0, STR_PAD_LEFT);
        
        // check if expiry date is more than today's date
        if (strtotime($expiryDate['year'].'-'.$expiryDate['month']) < strtotime(date("Y-m"))) {
            trigger_error('Expiry date has to be more than or equal to today\'s date', E_USER_NOTICE);
        }

        $this->paymentDetails['expiryDate'] = $expiryDate;
    }

    /**
     * setStartDate 
     *
     * start date has to be less than today's day
     * 
     * @param array $startDate array('month' => MM, 'year' => YYYY)
     * @access protected
     * @return void
     */
    protected function setStartDate(array $startDate)
    {
        if (!isset($startDate['month'], $startDate['year'])) {
            trigger_error('Start date has to be array with keys: month and year', E_USER_NOTICE);
        }

        // pad month, in case we received one digit number
        $startDate['month'] = str_pad($startDate['month'], 2, 0, STR_PAD_LEFT);

        // check if expiry date is less than today's date
        if (strtotime($startDate['year'].'-'.$startDate['month']) >= strtotime(date("Y-m"))) {
            trigger_error('Start date has to be less than or equal to today\'s date', E_USER_NOTICE);
        }

        $this->paymentDetails['startDate'] = $startDate;
    }

    /**
     * setCardHolderName 
     * 
     * @param string $cardHolderName 
     * @access protected
     * @return void
     */
    protected function setCardHolderName($cardHolderName)
    {
        $this->paymentDetails['cardHolderName'] = $cardHolderName;
    }

    /**
     * setCvc 
     * 
     * @param string $cvc 
     * @access protected
     * @return void
     */
    protected function setCvc($cvc)
    {
        $this->paymentDetails['cvc'] = $cvc;
    }

    /**
     * isValidAddress 
     *
     * helper method - checks if address fields are valid
     *
     * allowed values:
     *  --- compulsory ---
     *   - street
     *   - postalCode
     *   - countryCode
     *  --- optional ---
     *   - firstName
     *   - lastName
     *   - houseName
     *   - houseNumber
     *   - houseNumberExtension
     *   - city
     *   - state
     *   - telephoneNumber
     * 
     * @param array $address 
     * @access private
     * @return boolean
     */
    private function isValidAddress(array $address)
    {
        $valid = true;

        // --- allowed fields, true is compulsory, false is optional ---
        $addressFields = array(
            'firstName'     => false,   'lastName'              => false,
            'street'        => true,    'houseName'             => false,
            'houseNumber'   => false,   'houseNumberExtension'  => false,
            'postalCode'    => true,    'city'                  => false,
            'state'         => false,   'countryCode'           => true,
            'telephoneNumber' => false
        );

        // --- check compulsory fields ---
        foreach ($addressFields as $key => $value) {
            if ($value) {
                if (!isset($address[$key])) {
                    $valid = false;
                    trigger_error($key . ' address field is compulsory', E_USER_ERROR);
                }
            }
        }

        // --- check fields ---
        foreach ($address as $key => $value) {
            if (!isset($addressFields[$key])) {
                $valid = false;
                trigger_error($key . ' address field is not allowed', E_USER_ERROR);
            } 
        }

        return $valid;
    }

    /**
     * setCardAddress 
     *
     * allowed values:
     *  --- compulsory ---
     *   - street
     *   - postalCode
     *   - countryCode
     *  --- optional ---
     *   - firstName
     *   - lastName
     *   - houseName
     *   - houseNumber
     *   - houseNumberExtension
     *   - city
     *   - state
     *   - telephoneNumber
     *
     * @param array $cardAddress 
     * @access protected
     * @return void
     */
    public function setCardAddress(array $cardAddress)
    {
        if ($this->isValidAddress($cardAddress)) {
            $this->paymentDetails['cardAddress'] = $cardAddress;
        }
    }

    /**
     * setCardSwipe 
     *
     * TODO - 
     * 
     * @param mixed $cardSwipe 
     * @access protected
     * @return void
     */
    protected function setCardSwipe($cardSwipe)
    {
        $this->paymentDetails['cardSwipe'] = $cardSwipe;
    }

    /**
     * setIssueNumber 
     *
     * TODO -
     * 
     * @param mixed $issueNumber 
     * @access protected
     * @return void
     */
    protected function setIssueNumber($issueNumber)
    {
        $this->paymentDetails['issueNumber'] = $issueNumber;
    }

    /**
     * setAccountHolderName 
     * 
     * @param string $accountHolderName 
     * @access protected
     * @return void
     */
    protected function setAccountHolderName($accountHolderName)
    {
        $this->paymentDetails['accountHolderName'] = $accountHolderName;
    }

    /**
     * setBankAccountNr 
     * 
     * @param string $bankAccountNr 
     * @access protected
     * @return void
     */
    protected function setBankAccountNr($bankAccountNr)
    {
        $this->paymentDetails['bankAccountNr'] = $bankAccountNr;
    }

    /**
     * setBankName 
     * 
     * @param string $bankName 
     * @access protected
     * @return void
     */
    protected function setBankName($bankName)
    {
        $this->paymentDetails['bankName'] = $bankName;
    }

    /**
     * setBankLocation 
     * 
     * @param string $bankLocation 
     * @access protected
     * @return void
     */
    protected function setBankLocation($bankLocation)
    {
        $this->paymentDetails['bankLocation'] = $bankLocation;
    }

    /**
     * setBankLocationId 
     * 
     * @param string $bankLocationId 
     * @access protected
     * @return void
     */
    protected function setBankLocationId($bankLocationId)
    {
        $this->paymentDetails['bankLocationId'] = $bankLocationId;
    }

    /**
     * setCreditScoring 
     * 
     * @param array $creditScoring two values 
     * - address (see setCardAddress for details) 
     * - birthDate array('day' => DD, 'month' => MM, 'year' => YYYY)
     *
     * @access protected
     * @return void
     */
    protected function setCreditScoring(array $creditScoring)
    {
        if (!isset($creditScoring['address'], $creditScoring['birthDate'])) {
            trigger_error('Credit scoring has to be array with keys: address and birthDate', E_USER_NOTICE);
        }

        // check address
        if (!$this->isValidAddress($creditScoring['address'])) {
            trigger_error('Address is not valid', E_USER_NOTICE);
        }

        // check birth date
        if (!isset($creditScoring['birthDate']['day'], $creditScoring['birthDate']['month'], $creditScoring['birthDate']['year'])) {
            trigger_error('Birth date has to be array with keys: day, month and year', E_USER_NOTICE);
        }
        
        // pad month and day, in case we received one digit number
        $creditScoring['birthDate']['day']  = str_pad($creditScoring['birthDate']['day'], 2, 0, STR_PAD_LEFT);
        $creditScoring['birthDate']['month']= str_pad($creditScoring['birthDate']['month'], 2, 0, STR_PAD_LEFT);

        // check if birth date is more than today's date - check if card holder has to have certain age
        // and rewrite checking mechanism
        if (strtotime($creditScoring['birthDate']['year'].'-'.$creditScoring['birthDate']['month'].'-'.$creditScoring['birthDate']['day']) 
            <= strtotime(date("Y-m-d"))) 
        {
            trigger_error('Start date has to be less than or equal to today\'s date', E_USER_NOTICE);
        }

        $this->paymentDetails['creditScoring'] = $creditScoring;
    }

    /**
     * getXml 
     *
     * creates xml from set fields.
     * 
     * @access public
     * @return xml
     */
    public function getXml()
    {
        // load root xml
        $xml = '<paymentDetails></paymentDetails>';
        $xml = simplexml_load_string($xml);

        // add type
        $xml->addChild($this->type);

        // create the rest
        foreach($this->paymentDetails as $key => $value) {
            // --- expiry/start date
            if($key === 'expiryDate' || $key === 'startDate') { 
                $xml->{$this->type}->addChild($key);
                $xml->{$this->type}->{$key}->addChild('date');
                $xml->{$this->type}->{$key}->date->addAttribute('month', $value['month']);
                $xml->{$this->type}->{$key}->date->addAttribute('year', $value['year']);
            // --- card address
            } elseif($key === 'cardAddress') {
                $xml->{$this->type}->addChild($key);
                $xml->{$this->type}->{$key}->addChild('address');

                foreach ($value as $field => $content) {
                    $xml->{$this->type}->{$key}->address->addChild($field, $content);
                }
            // --- credit scoring
            } elseif($key === 'creditScoring') {
                $xml->{$this->type}->addChild($key);

                // --- birthDate ---
                $xml->{$this->type}->{$key}->addChild('birthDate');
                $xml->{$this->type}->{$key}->birthDate->addChild('date');

                foreach ($value['birthDate'] as $field => $content) {
                    $xml->{$this->type}->{$key}->birthDate->date->addAttribute($field, $content);
                }

                // --- address ---
                $xml->{$this->type}->addChild($key);
                $xml->{$this->type}->{$key}->addChild('address');

                foreach ($value['address'] as $field => $content) {
                    $xml->{$this->type}->{$key}->address->addChild($field, $content);
                }
            } else {
                $xml->{$this->type}->addChild($key, $value);
            }
        }

        // --- session - session is started inside WPDirect class ---
        $xml->addChild('session');
        $xml->session->addAttribute('shopperIPAddress', $_SERVER['REMOTE_ADDR']);
        $xml->session->addAttribute('id', session_id());

        return $xml->asXml();
    }
}
