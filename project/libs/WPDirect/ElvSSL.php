<?php
/*
+-------------------------------------------------------------------------------+
|   Copyright 2009 Peter Reisinger - p.reisinger@gmail.com                      |
|                                                                               |
|   This program is free software: you can redistribute it and/or modify        |
|   it under the terms of the GNU General Public License as published by        |
|   the Free Software Foundation, either version 3 of the License, or           |
|   (at your option) any later version.                                         |
|                                                                               |
|   This program is distributed in the hope that it will be useful,             |
|   but WITHOUT ANY WARRANTY; without even the implied warranty of              |
|   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               |
|   GNU General Public License for more details.                                |
|                                                                               |
|   You should have received a copy of the GNU General Public License           |
|   along with this program.  If not, see <http://www.gnu.org/licenses/>.       |
+-------------------------------------------------------------------------------+ 
 */

/**
 * include parent class
 */
require_once('WPPaymentDetails.php');

/**
 * ElvSSL 
 * 
 * @uses WPPaymentDetails
 * @package wpdirect
 * @version $id$
 * @copyright 2009 Peter Reisinger
 * @author Peter Reisinger <p.reisinger@gmail.com> 
 * @license GNU General Public License
 */
class ElvSSL extends WPPaymentDetails
{
    /**
     * __construct 
     * 
     * @param string $accountHolderName 
     * @param string $bankAccountNr 
     * @param string $bankName 
     * @param string $bankLocation 
     * @param string $bankLocationId 
     * @access public
     * @return void
     */
    public function __construct($accountHolderName, $bankAccountNr, $bankName, $bankLocation, $bankLocationId)
    {
        $cardNumber = str_replace(array('-', ' '), '', $cardNumber);

        parent::__construct('ELV-SSL');

        $this->setAccountHolderName($accountHolderName);
        $this->bankAccountNr($bankAccountNr);
        $this->bankName($bankName);
        $this->bankLocation($bankLocation);
        $this->bankLocationId($bankLocationId);
    }

    /**
     * setCreditScoring 
     *
     * @param array $creditScoring two values 
     *
     * * address: 
     *  --- compulsory ---
     *   - street
     *   - postalCode
     *   - countryCode
     *  --- optional ---
     *   - firstName
     *   - lastName
     *   - houseName
     *   - houseNumber
     *   - houseNumberExtension
     *   - city
     *   - state
     *   - telephoneNumber
     *
     * * birthDate: 
     * array('day' => DD, 'month' => MM, 'year' => YYYY)
     * 
     * @param mixed $creditScoring 
     * @access public
     * @return void
     */
    public function setCreditScoring($creditScoring)
    {
        parent::setCreditScoring($creditScoring);
    }
}
