<?php

class IssuerForm
{
    private $paRequest;

    private $issuerURL;

    private $termURL;

    private $md;

    public function __construct($paRequest, $issuerURL)
    {
        // parameters to create termURL
        $isHTTPS    = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on");
        $port       = (isset($_SERVER["SERVER_PORT"]) && ((!$isHTTPS && $_SERVER["SERVER_PORT"] != "80") || ($isHTTPS && $_SERVER["SERVER_PORT"] != "443")));
        $port       = ($port) ? ':'.$_SERVER["SERVER_PORT"] : '';
        // set return url, can be changed using setTermURL
        $this->termURL      = ($isHTTPS ? 'https://' : 'http://').$_SERVER["SERVER_NAME"].$port.$_SERVER["REQUEST_URI"];
        $this->paRequest    = $paRequest;
        $this->issuerURL    = $issuerURL;
    }

    public function setTermURL($termURL)
    {
        $this->termURL  = $termURL;
    }

    public function setMd($md)
    {
        $this->md       = $md;
    }


    /**
     * getIssuerForm
     *
     * returns html form for 3-d secure authentication 
     *
     * @param xml $response response
     * @param string $termURL url of your page, where user will be redirected
     * after submiting the form
     * @param string $md these parameters will be returned
     * @access public
     * @return html 
     */
    public function getIssuerForm()
    {
        // create hidden field for md if it's set
        if (isset($this->md)) {
            $md = '<input type="hidden" name="MD" value="'.$this->md.'" />';
        } else {
            $md = '';
        }

        // form
        $form = <<<EOF
<html>
<head>
    <title>3-D Secure helper page</title>
</head>
<body OnLoad="OnLoadEvent();">
This page should forward you to your own card issuer for identification.
If your browser does not start loading the page, press the button you see.
<br/>
After you successfully identify yourself you will be sent back to this site
where the payment process will continue as if nothing had happened.
<br/>
implemented...
<form name="issuerForm" method="POST" action="{$this->issuerURL}" >
<input type="hidden" name="PaReq" value="{$this->paRequest}" />
<input type="hidden" name="TermUrl" value="{$this->termURL}" />
{$md}
<input type="submit" name="Identify yourself" />
</form>
<script language="Javascript">
<!--
function OnLoadEvent() {
document.issuerForm.submit(); }
// -->
</script>
</body>
</html>
EOF;

        return $form;
    }
}
