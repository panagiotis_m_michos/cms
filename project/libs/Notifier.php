<?php

class Notifier {

    public static function triggerSuccess($eventKey, $objectId, $message){
        self::send($eventKey, $objectId, $message, 'SUCCESS');
    }
    
    public static function triggerWarning($eventKey, $objectId, $message){
        self::send($eventKey, $objectId, $message, 'WARNING');
    }
    
    public static function triggerFailure($eventKey, $objectId, $message){
        self::send($eventKey, $objectId, $message, 'FAILURE');
    }
    
    public static function send($eventKey, $objectId, $message, $statusId)
    {
        $cache = FApplication::getCache();
        
        //already seted
        if (isset($cache[$eventKey])) {
            //same message
            $cacheObject = json_decode($cache[$eventKey]);
            if($cacheObject->statusId == $statusId) {
                return true;
            }
        }
        
        $data = new stdClass();
        $data->eventKey = $eventKey;
        $data->statusId = $statusId;
        $data->objectId = $objectId;
        $data->message = $message;
        $data->dtc = time();
        
        $cache->save($eventKey, json_encode($data), array('expire' => time() + 60 * 10));
    }
    
    public static function getStatusObject($eventKey){
        
        $cache = FApplication::getCache();
        
         //not seted 
        if (!isset($cache[$eventKey])) {
            return false;
        }
        
        return  json_decode($cache[$eventKey]);
        
    }

}
