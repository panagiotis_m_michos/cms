<?php

namespace Console\Commands\Cashbacks;

use DateTime;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityManager;
use Entities\Cashback;
use Exception;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Services\CashbackService;
use Services\OrderItemService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Utils\Date;

class OutdateOldCashbacksCommand extends Command
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param LoggerInterface $logger
     * @param EntityManager $em
     * @param string $name
     */
    public function __construct(LoggerInterface $logger, EntityManager $em, $name = NULL)
    {
        $this->logger = $logger;
        $this->em = $em;

        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('cashbacks:outdateOldCashbacks')
            ->setDescription('Set cashbacks for companies incorporated before 28/07/2014 as OUTDATED')
            ->addOption('dry-run', NULL, InputOption::VALUE_NONE, 'If set, updates will not be executed, only logged');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $isDryRun = $input->getOption('dry-run');
        if ($isDryRun) {
            $this->logMessage($output, "<info>Dry-run mode enabled</info>", LogLevel::INFO);
        }

        $this->processEligibleCashbacks($output, $isDryRun);
        $this->processImportedCashbacks($output, $isDryRun);
    }

    /**
     * @param OutputInterface $output
     * @param bool $isDryRun
     */
    private function processEligibleCashbacks(OutputInterface $output, $isDryRun)
    {
        $eligibleCashbackIds = [];
        $eligibleCashbacks = $this->em->createQueryBuilder()
            ->select('cb')
            ->from('Entities\Cashback', 'cb')
            ->innerJoin('cb.company', 'c')
            ->where('c.incorporationDate < :oldDate')->setParameter('oldDate', new Date('2014-07-28'), Type::DATE)
            ->andWhere('cb.statusId = :eligibleStatus')->setParameter('eligibleStatus', Cashback::STATUS_ELIGIBLE)
            ->orderBy('c.incorporationDate', 'desc')
            ->getQuery()
            ->iterate();

        foreach ($eligibleCashbacks as $cashbackData) {
            /** @var Cashback $cashback */
            $cashback = $cashbackData[0];
            $cashbackId = $cashback->getCashbackId();
            $eligibleCashbackIds[] = $cashbackId;

            $this->logMessage(
                $output,
                sprintf(
                    '<info>Changing old ELIGIBLE cashback to OUTDATED for id <comment>%d</comment> (company incorporated on <comment>%s</comment>)</info>',
                    $cashbackId,
                    $cashback->getCompany()->getIncorporationDate()
                )
            );
        }

        $this->logMessage(
            $output,
            sprintf(
                '<info>Revert query: <comment>UPDATE cms2_cashback SET statusId = "ELIGIBLE" WHERE cashbackId IN (%s)</comment></info>',
                implode(',', $eligibleCashbackIds)
            )
        );

        if (!$isDryRun) {
            $this->em->createQuery('UPDATE Entities\Cashback cb SET cb.statusId = :outdatedStatus WHERE cb.cashbackId IN (:eligibleIds)')
                ->setParameter('outdatedStatus', Cashback::STATUS_OUTDATED)
                ->setParameter('eligibleIds', $eligibleCashbackIds)
                ->execute();
        }
    }

    /**
     * @param OutputInterface $output
     * @param bool $isDryRun
     */
    private function processImportedCashbacks(OutputInterface $output, $isDryRun)
    {
        $importedCashbackIds = [];
        $importedCashbacks = $this->em->createQueryBuilder()
            ->select('cb')
            ->from('Entities\Cashback', 'cb')
            ->innerJoin('Entities\Company', 'c', 'WITH', 'c = cb.company')
            ->where('c.incorporationDate < :oldDate')->setParameter('oldDate', new Date('2014-07-28'), Type::DATE)
            ->andWhere('cb.statusId = :importedStatus')->setParameter('importedStatus', Cashback::STATUS_IMPORTED)
            ->orderBy('c.incorporationDate', 'desc')
            ->getQuery()
            ->iterate();

        foreach ($importedCashbacks as $cashbackData) {
            /** @var Cashback $cashback */
            $cashback = $cashbackData[0];
            $cashbackId = $cashback->getCashbackId();
            $importedCashbackIds[] = $cashbackId;

            $this->logMessage(
                $output,
                sprintf(
                    '<info>Changing old IMPORTED cashback to OUTDATED for id <comment>%d</comment> (company incorporated on <comment>%s</comment>)</info>',
                    $cashbackId,
                    $cashback->getCompany()->getIncorporationDate()
                )
            );
        }

        $this->logMessage(
            $output,
            sprintf(
                '<info>Revert query: <comment>UPDATE cms2_cashback SET statusId = "IMPORTED" WHERE cashbackId IN (%s)</comment></info>',
                implode(',', $importedCashbackIds)
            )
        );

        if (!$isDryRun) {
            $this->em->createQuery('UPDATE Entities\Cashback cb SET cb.statusId = :outdatedStatus WHERE cb.cashbackId IN (:importedIds)')
                ->setParameter('outdatedStatus', Cashback::STATUS_OUTDATED)
                ->setParameter('importedIds', $importedCashbackIds)
                ->execute();
        }
    }

    /**
     * @param OutputInterface $output
     * @param string $message
     * @param string $level
     */
    private function logMessage(OutputInterface $output, $message, $level = LogLevel::DEBUG)
    {
        $output->writeln($message);
        $this->logger->log($level, strip_tags($message));
    }
}
