<?php

namespace Console\Commands\Services;

use DibiConnection;
use Doctrine\ORM\EntityManager;
use Entities\Service;
use Exception;
use ILogger;
use Loggers\NikolaiLogger;
use Package;
use Product;
use Services\ServiceService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

class UpdateMissingChildrenCommand extends Command
{
    /**
     * @var DibiConnection
     */
    private $em;

    /**
     * @var NikolaiLogger
     */
    private $logger;

    /**
     * @var ServiceService
     */
    private $serviceService;

    /**
     * @var array
     */
    private static $packageIds = array(1353, 342);

    /**
     * @param EntityManager $em
     * @param NikolaiLogger $logger
     * @param ServiceService $serviceService
     * @param string $name
     */
    public function __construct(EntityManager $em, NikolaiLogger $logger, ServiceService $serviceService, $name = NULL)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->serviceService = $serviceService;

        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('services:updateMissingChildren')
            ->setDescription('Update missing package children')
            ->addOption('count', 'c', InputOption::VALUE_REQUIRED, 'If set just provided number of services will be executed')
            ->addOption('dry-run', 'd', InputOption::VALUE_NONE, 'If set, changes are not flushed to database');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $batchSize = 50;
        $batchCount = 0;
        $processed = 0;

        if ($input->getOption('dry-run')) {
            $this->logMessage($output, ILogger::DEBUG, "<info>Dry-run flag set</info>");
        }

        $countOption = (int) $input->getOption('count');

        foreach (self::$packageIds as $packageId) {

            $package = Package::getProductById($packageId);
            $included = $package->getPackageProducts('includedProducts');

            $services = $this->serviceService->getServicesByProductId($packageId);
            foreach ($services as $row) {

                /** @var Service $parentService */
                $parentService = $row[0];

                if (!$parentService->hasChildren()) {
                    /** @var Product $product */
                    foreach ($included as $product) {
                        if ($product->hasServiceType()) {
                            $childService = new Service($product->serviceTypeId, $product, $parentService->getCompany(), $parentService->getOrder());
                            $parentService->addChild($childService);
                        }
                    }

                    $this->logMessage($output, ILogger::DEBUG, sprintf("- <info>service <comment>%d</comment> has been updated</info>", $parentService->getId()));

                    $this->em->persist($parentService);
                    $processed++;
                    $batchCount++;

                    if ($countOption == $processed) {
                        break(2);
                    }

                    if ($batchCount % $batchSize === 0) {
                        $this->flushUpdates($output, $input);
                    }
                }
            }
        }

        $this->flushUpdates($output, $input);
        $this->logMessage($output, ILogger::DEBUG, "<info>Done with <comment>$processed</comment> <info>services proceeded.</info>");
    }

    /**
     * @param OutputInterface $output
     * @param int $level
     * @param string $message
     */
    private function logMessage(OutputInterface $output, $level, $message)
    {
        $logMessage = sprintf('[%s] %s', $this->getName(), $message);
        $output->writeln($logMessage);
        $this->logger->logMessage($level, strip_tags($logMessage));
    }

    /**
     * @param OutputInterface $output
     * @param InputInterface $input
     */
    private function flushUpdates(OutputInterface $output, InputInterface $input)
    {
        if (!$input->getOption('dry-run')) {
            $this->em->flush();
        }
        $this->em->clear();
        $this->logMessage($output, ILogger::DEBUG, 'Updates have been executed');
    }
}
