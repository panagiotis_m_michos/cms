<?php

namespace Console\Commands\Services;

use DibiConnection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Internal\Hydration\IterableResult;
use Entities\OrderItem;
use Entities\Service;
use Exception;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Services\OrderItemService;
use Services\OrderService;
use Services\ServiceService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

class AssignOrderItemsCommand extends Command
{
    /**
     * @var DibiConnection
     */
    private $em;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var DibiConnection
     */
    private $dibi;

    /**
     * @var ServiceService
     */
    private $serviceService;

    /**
     * @var OrderService
     */
    private $orderService;

    /**
     * @var OrderItemService
     */
    private $orderItemService;

    /**
     * @param EntityManager $em
     * @param LoggerInterface $logger
     * @param DibiConnection $dibi
     * @param ServiceService $serviceService
     * @param OrderService $orderService
     * @param OrderItemService $orderItemService
     * @param string $name
     */
    public function __construct(
        EntityManager $em,
        LoggerInterface $logger,
        DibiConnection $dibi,
        ServiceService $serviceService,
        OrderService $orderService,
        OrderItemService $orderItemService,
        $name = NULL
    )
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->dibi = $dibi;
        $this->serviceService = $serviceService;
        $this->orderService = $orderService;
        $this->orderItemService = $orderItemService;

        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('services:assignOrderItem')
            ->setDescription('Assign order items references')
            ->addOption('dry-run', 'd', InputOption::VALUE_NONE, 'If set, changes are not flushed to database');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $batchSize = 100;
        $processed = 0;
        $assignedIds = [];

        if ($input->getOption('dry-run')) {
            $this->logger->debug("Dry-run flag set");
        }

        $orderItems = $this->getOrderItemsToMatch();

        foreach ($orderItems as $orderItemData) {
            /** @var OrderItem $orderItem */
            $orderItem = $orderItemData[0];
            $orderId = $orderItem->getOrder()->getId();
            $services = $this->getServiceForOrderItem($orderItem);

            if ($services) {
                if (count($services) == 1) {
                    $service = $services[0];
                    $service->setOrderItem($orderItem);
                    $this->logger->debug("Setting order item {$orderItem->getId()} (order {$orderId}) for service {$service->getId()}");
                } else {
                    foreach ($services as $service) {
                        // if multiple possible services are found, set first of them and add its ID to array
                        // so it's not assigned to all order items (when customer bought multiple service of same type
                        // at once
                        if (!in_array($service->getId(), $assignedIds)) {
                            $service->setOrderItem($orderItem);
                            $this->logger->debug("Setting order iteorderItem->getId()} (order {$orderId}) for service {$service->getId()}");
                            $assignedIds[] = $service->getId();
                            break;
                        }
                    }
                }
            } else {
                $this->logger->debug("Service not found for order item {$orderItem->getId()} (order {$orderId})");
            }

            $processed++;
            if ($processed % $batchSize == 0) {
                $this->flushUpdates($input);
            }
            if ($processed % 500 == 0) {
                $this->logger->debug("{$processed} order items processed");
            }
        }

        $this->flushUpdates($input);
        $this->logger->debug("Done with $processed order items processed.");
    }

    /**
     * @param InputInterface $input
     */
    private function flushUpdates(InputInterface $input)
    {
        if (!$input->getOption('dry-run')) {
            $this->em->flush();
        }
        $this->em->clear();
        $this->logger->debug('Updates have been executed');
    }

    /**
     * @return IterableResult
     */
    private function getOrderItemsToMatch()
    {
        $serviceProductIds = $this->getServiceProductIds();

        $qb = $this->em->createQueryBuilder()
            ->select('oi')
            ->from('Entities\OrderItem', 'oi')
            ->join('Entities\Order', 'o', 'WITH', 'oi.order = o')
            ->leftJoin('Entities\Service', 's', 'WITH', 's.orderItem = oi')
            ->where('o.services IS NOT EMPTY')
            ->andWhere('s.orderItem IS NULL')
            ->andWhere('oi.productId IN (:serviceProducts)')->setParameter('serviceProducts', $serviceProductIds)
            ->andWhere('oi.isRefunded IS NULL')
            ->getQuery()
            ->iterate();

        return $qb;
    }

    /**
     * @param OrderItem $orderItem
     * @return Service[]
     */
    private function getServiceForOrderItem(OrderItem $orderItem)
    {
        $company = $orderItem->getCompany();
        $companyNumber = $orderItem->getCompanyNumber();

        $qb = $this->em->createQueryBuilder()
            ->select('s')->from('Entities\Service', 's')
            ->join('Entities\Company', 'c', 'WITH', 's.company = c')
            ->where('s.order = :order')
            ->andWhere('s.productId = :productId')
            ->andWhere('s.parent IS NULL')
            ->andWhere('s.orderItem IS NULL')
            ->orderBy('s.dtExpires', 'DESC')
            ->setParameters(
                [
                    'productId' => $orderItem->getProductId(),
                    'order' => $orderItem->getOrder(),
                ]
            );

        // if company or company number are set for order item, add condition for service as well
        if ($company) {
            $qb = $qb->andWhere('c = :company')->setParameter('company', $company);
        } elseif ($companyNumber) {
            $qb = $qb->andWhere('c.companyNumber = :companyNumber')->setParameter('companyNumber', $companyNumber);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @return array
     */
    private function getServiceProductIds()
    {
        return $this->dibi->select('nodeId')
            ->from(TBL_PROPERTIES)
            ->where('name = %s', 'serviceTypeId')
            ->where('CHAR_LENGTH(value) > 0')
            ->fetchPairs();
    }
}
