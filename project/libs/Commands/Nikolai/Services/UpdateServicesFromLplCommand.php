<?php

namespace Console\Commands\Services;

use Basket;
use DateTime;
use DibiConnection;
use Entities\CompanyHouse\FormSubmission;
use Exception;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateServicesFromLplCommand extends Command
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var DibiConnection
     */
    private $dibiCms;

    /**
     * @var DibiConnection
     */
    private $dibiLpl;

    /**
     * @param LoggerInterface $logger
     * @param DibiConnection $dibiCms
     * @param DibiConnection $dibiLpl
     */
    public function __construct(
        LoggerInterface $logger,
        DibiConnection $dibiCms,
        DibiConnection $dibiLpl
    )
    {
        parent::__construct();

        $this->logger = $logger;
        $this->dibiCms = $dibiCms;
        $this->dibiLpl = $dibiLpl;
    }

    protected function configure()
    {
        $this->setName('services:updateServicesFromLpl')
            ->setDescription('Update services from LPL')
            ->addOption('truncate', NULL, InputOption::VALUE_NONE, 'If set, temp table with LPL services will be re-filled again from LPL')
            ->addOption('update', NULL, InputOption::VALUE_NONE, 'If set, updates will be executed')
            ->addOption('wrong-connect', NULL, InputOption::VALUE_NONE, 'If set, only show wrong connected services')
            ->addOption('threshold', NULL, InputOption::VALUE_REQUIRED, 'Threshold of day difference which will be updated by command');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $daysThreshold = $input->getOption('threshold');
        if (!$daysThreshold || !is_numeric($daysThreshold)) {
            throw new InvalidArgumentException('Threshold has to be numeric value');
        }

        $date = (new DateTime('-1 year -1 month 00:00:00'))->format('Y-m-d');

        $lookup = $this->dibiCms
            ->select('s.productId, s.renewalProductId')
            ->from('cms2_services s')
            ->where('s.renewalProductId IS NOT NULL')
            ->fetchPairs('productId', 'renewalProductId');

        $lookup += [
            111 => [165, 334, 1257],
            341 => [165, 334, 1257],
        ];

        $reverseLookup = $this->dibiCms
            ->select('s.productId, s.renewalProductId')
            ->from('cms2_services s')
            ->where('s.renewalProductId IS NOT NULL')
            ->fetchPairs('renewalProductId', 'productId');

        if ($input->getOption('wrong-connect')) {
            $highDiffLplServices = $this->dibiLpl->query(
                'SELECT
                    dbo.tblCoService.CoServId AS CoServId,
                    dbo.tblCoClient.CustomerId AS CustomerId,
                    dbo.tblCoClient.CoClientCoId AS CompanyId,
                    dbo.tblCoClient.CoClientCompany AS CompanyName,
                    dbo.tblCoService.CoServOrderNo AS OrderId,
                    dbo.tblCoService.CoServItemId AS OrderItemId,
                    dbo.tblCategory.CategoryZone AS ProductId,
                    dbo.tblCategory.CategoryDescr AS ProductName,
                    CONVERT(VARCHAR(40), dbo.tblCoService.CoServPaidDate, 120) AS PaidDate,
                    CONVERT(VARCHAR(40), dbo.tblCoService.CoServDate, 120) AS StartDate,
                    CONVERT(VARCHAR(40), dbo.tblCoService.CoServExpiry, 120) AS ExpiryDate,
                    DATEDIFF(d, dbo.tblCoService.CoServPaidDate, dbo.tblCoService.CoServDate) AS PaidDiffDate
                FROM
                    dbo.tblCoService
                    INNER JOIN dbo.tblCoClient ON dbo.tblCoService.CoClientId = dbo.tblCoClient.CoClientId
                    INNER JOIN dbo.tblCategory ON dbo.tblCoService.CoServType = dbo.tblCategory.id
                WHERE
                    1 = 1
                    AND dbo.tblCoClient.CustomerId != 253949
                    AND dbo.tblCategory.CategoryZone IN (334,379,1317,1315,1316,165,475,1430,342,899,806,1257,1493,1353,1598,1599,1643,1644,1647,1648)
                    AND (dbo.tblCoService.CoServPaidDate >= DATEADD(yyyy, - 1, GETDATE()))
                    AND (dbo.tblCoService.Del = 0)
                    AND DATEDIFF(d, dbo.tblCoService.CoServPaidDate, dbo.tblCoService.CoServDate) < -28
                ORDER BY
                    dbo.tblCoService.CoServItemId ASC,
                    DATEDIFF(d, dbo.tblCoService.CoServPaidDate, dbo.tblCoService.CoServDate) ASC'
            )->fetchAll();

            $tableRows = [];
            foreach ($highDiffLplServices as &$highDiffLplService) {
                $previous = $this->dibiLpl->query(
                    'SELECT
                        dbo.tblCoService.CoServId AS CoServId,
                        dbo.tblCoClient.CustomerId AS CustomerId,
                        dbo.tblCoClient.CoClientCoId AS CompanyId,
                        dbo.tblCoClient.CoClientCompany AS CompanyName,
                        dbo.tblCoService.CoServOrderNo AS OrderId,
                        dbo.tblCoService.CoServItemId AS OrderItemId,
                        dbo.tblCategory.CategoryZone AS ProductId,
                        dbo.tblCategory.CategoryDescr AS ProductName,
                        CONVERT(VARCHAR(40), dbo.tblCoService.CoServPaidDate, 120) AS PaidDate,
                        CONVERT(VARCHAR(40), dbo.tblCoService.CoServDate, 120) AS StartDate,
                        CONVERT(VARCHAR(40), dbo.tblCoService.CoServExpiry, 120) AS ExpiryDate,
                        DATEDIFF(d, CONVERT(VARCHAR(40), dbo.tblCoService.CoServExpiry, 120), %d) AS DiffDays
                    FROM
                        dbo.tblCoService
                        INNER JOIN dbo.tblCoClient ON dbo.tblCoService.CoClientId = dbo.tblCoClient.CoClientId
                        INNER JOIN dbo.tblCategory ON dbo.tblCoService.CoServType = dbo.tblCategory.id
                    WHERE
                        1 = 1
                        AND dbo.tblCoClient.CoClientCoId = %i
                        AND dbo.tblCategory.CategoryZone IN (%iN)
                        AND CONVERT(VARCHAR(40), dbo.tblCoService.CoServDate, 120) < %d
                        AND CONVERT(VARCHAR(40), dbo.tblCoService.CoServExpiry, 120) < %d
                    ORDER BY
                        dbo.tblCoService.CoServDate DESC',
                    $highDiffLplService->StartDate,
                    $highDiffLplService->CompanyId,
                    array_filter([$highDiffLplService->ProductId, $lookup[$highDiffLplService->ProductId], $this->lookupSearch($highDiffLplService->ProductId, $lookup)]),
                    $highDiffLplService->StartDate,
                    $highDiffLplService->ExpiryDate
                )->fetch();

                if ($previous) {
                    $highDiffLplService->PreviousService = str_replace(' 00:00:00', '', $previous->StartDate) . ' - ' . str_replace(' 00:00:00', '', $previous->ExpiryDate) . " ({$previous->ProductName})";
                    $highDiffLplService->DiffDays = $previous->DiffDays;
                } else {
                    $highDiffLplService->PreviousService = '- no previous service -';
                    $highDiffLplService->DiffDays = '';
                }

                $tableRows[] = (array) $highDiffLplService;
            }

            $wrongConnectTable = new Table($output);
            $wrongConnectTable
                ->setHeaders(array_keys(reset($tableRows)))
                ->setRows($tableRows);
            $wrongConnectTable->render();

            die;
        }

        // re-create temp LPL table
        if ($input->getOption('truncate')) {
            echo 'Getting all services from LPL' . "\n";

            $this->dibiCms->query('DROP TABLE IF EXISTS [temp_lpl_update_dates]');
            $this->dibiCms->query(
                'CREATE TABLE [temp_lpl_update_dates] (
                  `CoServId` int(11) DEFAULT NULL,
                  `CustomerId` int(11) DEFAULT NULL,
                  `CompanyId` int(11) DEFAULT NULL,
                  `CompanyName` varchar(255) DEFAULT NULL,
                  `CompanyNumber` varchar(255) DEFAULT NULL,
                  `OrderId` int(11) DEFAULT NULL,
                  `OrderItemId` int(11) DEFAULT NULL,
                  `ProductId` int(11) DEFAULT NULL,
                  `PaidDate` datetime NOT NULL,
                  `StartDate` datetime NOT NULL,
                  `ExpiryDate` datetime NOT NULL,
                  KEY `OrderItemId` (`OrderItemId`),
                  KEY `ProductId` (`ProductId`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8'
            );

            $lplItems = $this->dibiLpl->query(
                'SELECT
                    dbo.tblCoService.CoServId AS CoServId,
                    dbo.tblCoClient.CustomerId AS CustomerId,
                	dbo.tblCoClient.CoClientCoId AS CompanyId,
                	dbo.tblCoClient.CoClientCompany AS CompanyName,
                	dbo.tblCoClient.CoClientCoNumber AS CompanyNumber,
                    dbo.tblCoService.CoServOrderNo AS OrderId,
                	dbo.tblCoService.CoServItemId AS OrderItemId,
                	dbo.tblCategory.CategoryZone AS ProductId,
                    CONVERT(VARCHAR(40), dbo.tblCoService.CoServPaidDate, 120) AS PaidDate,
                    CONVERT(VARCHAR(40), dbo.tblCoService.CoServDate, 120) AS StartDate,
                    CONVERT(VARCHAR(40), dbo.tblCoService.CoServExpiry, 120) AS ExpiryDate
                FROM
                	dbo.tblCoService
                    INNER JOIN dbo.tblCoClient ON dbo.tblCoService.CoClientId = dbo.tblCoClient.CoClientId
                	INNER JOIN dbo.tblCategory ON dbo.tblCoService.CoServType = dbo.tblCategory.id
                WHERE
                	((dbo.tblCoService.CoServDate >= DATEADD(yyyy, - 2, GETDATE())) OR (dbo.tblCoService.CoServPaidDate >= DATEADD(yyyy, - 2, GETDATE())))
                	AND (dbo.tblCoService.Del = 0)
                ORDER BY
                	StartDate'
            )->fetchAll();

            echo 'Inserting all services from LPL' . "\n";

            $this->dibiCms->query('INSERT INTO [temp_lpl_update_dates] %ex', $lplItems);
        }

        // select all CMS services with dates different than LPL dates
        $cmsServiceWithDifferentDates = $this->dibiCms
            ->select('s.serviceId, s.orderItemId, c.company_name AS companyName, s.serviceName, s.companyId')
            ->select('s.serviceTypeId')
            ->select('s.productId')
            ->select('s.renewalProductId')
            ->select('DATE(s.dtStart) AS dtStart, DATE(s.dtExpires) AS dtExpires')
            ->select('DATE(lpl.StartDate) AS lplDtStart, DATE(lpl.ExpiryDate) AS lplDtExpires')
            ->select('DATEDIFF(lpl.StartDate, s.dtStart) AS sd')
            ->select('DATEDIFF(lpl.ExpiryDate, s.dtExpires) AS ed')
            ->select('DATEDIFF(lpl.PaidDate, lpl.StartDate) AS lplStartPaidDateDiff')
            ->select('IF(s.stateId = "DISABLED", s.stateId, "") AS X')
            ->from('cms2_services s')
            ->join('ch_company c')->on('c.company_id = s.companyId')
            ->join('temp_lpl_update_dates lpl')->on('lpl.OrderItemId = s.orderItemId')
            ->where('s.dtc >= "' . $date . ' 00:00:00"')
            ->where('s.parentId IS NULL')
            ->where('c.company_number IS NOT NULL')
            ->where('(s.dtStart <> DATE(lpl.StartDate) OR s.dtExpires <> DATE(lpl.ExpiryDate))')
            ->where('lpl.CustomerId != 253949') // toddington
            ->where('ABS(DATEDIFF(lpl.StartDate, s.dtStart)) <= %i', $daysThreshold)
            ->where('ABS(DATEDIFF(lpl.ExpiryDate, s.dtExpires)) <= %i', $daysThreshold)
            ->where('s.dtStart <= CURRENT_DATE()')
            //->where('s.companyId = 139735')
            ->orderBy('sd DESC')
            //->limit(10)
            ->orderBy('s.dtStart DESC')
            ->fetchAll();

        $wrongCmsServices = [];
        // add additional info to services (order of all other services for that company)
        foreach ($cmsServiceWithDifferentDates as &$wrongCmsService) {
            $previousCmsService = $this->dibiCms
                ->select('s.dtStart AS previousStart, s.dtExpires AS previousExpires')
                ->from('cms2_services s')
                ->where('s.orderItemId != %i', $wrongCmsService->orderItemId)
                ->where('s.companyId = %i', $wrongCmsService->companyId)
                ->where('s.serviceTypeId = %s', $wrongCmsService->serviceTypeId)
                ->where('(s.productId = %i OR s.renewalProductId = %i)', $wrongCmsService->productId, $wrongCmsService->productId)
                ->where('s.parentId IS NULL')
                ->where('s.dtStart <= %d', $wrongCmsService->dtStart)
                ->where('s.dtExpires <= %d', $wrongCmsService->dtExpires)
                ->orderBy('s.dtExpires DESC')
                ->fetch();

            $nextCmsService = $this->dibiCms
                ->select('s.dtStart AS nextStart, s.dtExpires AS nextExpires')
                ->from('cms2_services s')
                ->where('s.orderItemId != %i', $wrongCmsService->orderItemId)
                ->where('s.companyId = %i', $wrongCmsService->companyId)
                ->where('s.serviceTypeId = %s', $wrongCmsService->serviceTypeId)
                ->where('s.productId IN (%iN)', array_filter([$wrongCmsService->productId, $wrongCmsService->renewalProductId, $this->lookupSearch($wrongCmsService->productId, $lookup)]))
                ->where('s.parentId IS NULL')
                ->where('s.dtStart >= %d', $wrongCmsService->dtStart)
                ->where('s.dtExpires >= %d', $wrongCmsService->dtExpires)
                ->orderBy('s.dtStart DESC')
                ->fetch();

            $previousLplService = $this->dibiCms
                ->select('DATE(lpl.StartDate) AS lplPreviousStart, DATE(lpl.ExpiryDate) AS lplPreviousExpires')
                ->select('DATEDIFF(%d, lpl.ExpiryDate) AS daysSincePrevious', $wrongCmsService->lplDtStart)
                ->from('temp_lpl_update_dates lpl FORCE INDEX (OrderItemId, ProductId)')
                ->where('lpl.OrderItemId != %i', $wrongCmsService->orderItemId)
                ->where('lpl.CompanyId = %i', $wrongCmsService->companyId)
                ->where('lpl.ProductId IN (%iN)', array_filter([$wrongCmsService->productId, $lookup[$wrongCmsService->productId], $this->lookupSearch($wrongCmsService->productId, $lookup)]))
                ->where('lpl.StartDate <= %d', $wrongCmsService->lplDtStart)
                ->where('lpl.ExpiryDate <= %d', $wrongCmsService->lplDtExpires)
                ->orderBy('lpl.ExpiryDate DESC')
                ->fetch();

            $nextLplService = $this->dibiCms
                ->select('DATE(lpl.StartDate) AS lplNextStart, DATE(lpl.ExpiryDate) AS lplNextExpires')
                ->from('temp_lpl_update_dates lpl FORCE INDEX (OrderItemId, ProductId)')
                ->where('lpl.OrderItemId != %i', $wrongCmsService->orderItemId)
                ->where('lpl.CompanyId = %i', $wrongCmsService->companyId)
                ->where('lpl.ProductId IN (%iN)', array_filter([$wrongCmsService->productId, $lookup[$wrongCmsService->productId], $this->lookupSearch($wrongCmsService->productId, $lookup)]))
                ->where('lpl.StartDate >= %d', $wrongCmsService->lplDtStart)
                ->where('lpl.ExpiryDate >= %d', $wrongCmsService->lplDtExpires)
                ->orderBy('lpl.StartDate DESC')
                ->fetch();

            $wrongCmsService->daysSincePrevious = ($previousLplService ? $previousLplService->daysSincePrevious : NULL);

            $action = '';
            if ($wrongCmsService->dtExpires > $wrongCmsService->lplDtExpires) {
                $action = 'Update dtm';
            } elseif ($wrongCmsService->lplDtExpires > $wrongCmsService->dtExpires) {
                if ($wrongCmsService->daysSincePrevious > 1 && $wrongCmsService->daysSincePrevious <= 28) {
                    $action = 'Update dtm';
                } else {
                    $action = 'Set dates from LPL';
                }
            }

            $wrongCmsServices[] =
                (array) $wrongCmsService
                + ['previousCmsService' => ($previousCmsService ? implode(' - ', [$previousCmsService->previousStart->format('d/m/Y'), $previousCmsService->previousExpires->format('d/m/Y')]) : '')]
                + ['nextCmsService' => ($nextCmsService ? implode(' - ', [$nextCmsService->nextStart->format('d/m/Y'), $nextCmsService->nextExpires->format('d/m/Y')]) : '')]
                + ['previousLplService' => ($previousLplService ? implode(' - ', [$previousLplService->lplPreviousStart->format('d/m/Y'), $previousLplService->lplPreviousExpires->format('d/m/Y')]) : '')]
                + ['nextLplService' => ($nextLplService ? implode(' - ', [$nextLplService->lplNextStart->format('d/m/Y'), $nextLplService->lplNextExpires->format('d/m/Y')]) : '')]
                + ['daysSincePrevious' => ($previousLplService ? $previousLplService->daysSincePrevious : '')]
                + ['action' => $action]
            ;
        }

        if (!empty($wrongCmsServices)) {
            $tableServices = [];
            foreach ($wrongCmsServices as $s) {
                $tableServices[] = [
                    'orderItemId' => $s['orderItemId'],
                    'companyId' => $s['companyId'],
                    'companyName' => $s['companyName'],
                    'productId' => $s['productId'],
                    'serviceName' => $s['serviceName'],
                    'dtStart' => $s['dtStart']->format('d/m/Y'),
                    'dtExpires' => $s['dtExpires']->format('d/m/Y'),
                    'lplDtStart' => $s['lplDtStart']->format('d/m/Y'),
                    'lplDtExpires' => $s['lplDtExpires']->format('d/m/Y'),
                    'sd' => $s['sd'],
                    'ed' => $s['ed'],
                    'lplStartPaidDateDiff' => $s['lplStartPaidDateDiff'],
                    'X' => $s['X'],
                    'previousCmsService' => $s['previousCmsService'],
                    'nextCmsService' => $s['nextCmsService'],
                    'previousLplService' => $s['previousLplService'],
                    'nextLplService' => $s['nextLplService'],
                    'daysSincePrevious' => $s['daysSincePrevious'],
                    'action' => $s['action'],
                ];
            }

            $servicesTable = new Table($output);
            $servicesTable
                ->setHeaders(array_keys(reset($tableServices)))
                ->setRows($tableServices);
            $servicesTable->render();
        }

        $now = new DateTime;
        foreach ($cmsServiceWithDifferentDates as $cmsService) {
            if ($cmsService->dtExpires > $cmsService->lplDtExpires) {
                $this->logger->debug(
                    'Updating dtm to current date',
                    [
                        'serviceId' => $cmsService->serviceId,
                        'serviceName' => $cmsService->serviceName,
                        'company' => $cmsService->companyName,
                        'orderId' => $cmsService->orderItemId,
                        'cmsStart' => $cmsService->dtStart->format('d/m/Y'),
                        'cmsExpires' => $cmsService->dtExpires->format('d/m/Y'),
                        'lplStart' => $cmsService->lplDtStart->format('d/m/Y'),
                        'lplExpires' => $cmsService->lplDtExpires->format('d/m/Y'),
                    ]
                );

                if ($input->getOption('update')) {
                    $this->dibiCms
                        ->update('cms2_services', ['dtm' => $now])
                        ->where('serviceId = %i', $cmsService->serviceId)
                        ->execute();
                }
            } elseif ($cmsService->lplDtExpires > $cmsService->dtExpires) {
                if ($cmsService->daysSincePrevious > 1 && $cmsService->daysSincePrevious <= 28) {
                    $this->logger->debug(
                        'Updating dtm to current date',
                        [
                            'serviceId' => $cmsService->serviceId,
                            'serviceName' => $cmsService->serviceName,
                            'company' => $cmsService->companyName,
                            'orderId' => $cmsService->orderItemId,
                            'cmsStart' => $cmsService->dtStart->format('d/m/Y'),
                            'cmsExpires' => $cmsService->dtExpires->format('d/m/Y'),
                            'lplStart' => $cmsService->lplDtStart->format('d/m/Y'),
                            'lplExpires' => $cmsService->lplDtExpires->format('d/m/Y'),
                        ]
                    );

                    if ($input->getOption('update')) {
                        $this->dibiCms
                            ->update('cms2_services', ['dtm' => $now])
                            ->where('serviceId = %i', $cmsService->serviceId)
                            ->execute();
                    }
                } else {
                    $this->logger->debug(
                        'Updating dates according to LPL',
                        [
                            'serviceId' => $cmsService->serviceId,
                            'serviceName' => $cmsService->serviceName,
                            'company' => $cmsService->companyName,
                            'orderId' => $cmsService->orderItemId,
                            'cmsStart' => $cmsService->dtStart->format('d/m/Y'),
                            'cmsExpires' => $cmsService->dtExpires->format('d/m/Y'),
                            'lplStart' => $cmsService->lplDtStart->format('d/m/Y'),
                            'lplExpires' => $cmsService->lplDtExpires->format('d/m/Y'),
                        ]
                    );

                    if ($input->getOption('update')) {
                        $this->dibiCms
                            ->update('cms2_services', ['dtStart' => $cmsService->lplDtStart, 'dtExpires' => $cmsService->lplDtExpires])
                            ->where('serviceId = %i', $cmsService->serviceId)
                            ->execute();
                    }
                }
            }
        }
    }

    private function lookupSearch($needle, $haystack)
    {
        $keys = [];
        foreach ($haystack as $key => $value) {
            if ($needle === $value OR (is_array($value) && $this->lookupSearch($needle, $value) !== FALSE)) {
                $keys[] = $key;
            }
        }

        if (!empty($keys)) {
            return $keys;
        }

        return FALSE;
    }
}
