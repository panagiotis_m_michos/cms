<?php

namespace Console\Commands\Services;

use Doctrine\ORM\EntityManager;
use Entities\Service;
use Exception;
use Exceptions\Technical\RemoveRefundedServicesException;
use ILogger;
use Services\ServiceService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RemoveRefundedCommand extends Command
{
    /**
     * @var ILogger
     */
    private $logger;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var ServiceService
     */
    private $serviceService;

    /**
     * @param ILogger $logger
     * @param ServiceService $serviceService
     * @param EntityManager $em
     * @param string $name
     */
    public function __construct(ILogger $logger, ServiceService $serviceService, EntityManager $em, $name = NULL)
    {
        $this->logger = $logger;
        $this->em = $em;
        $this->serviceService = $serviceService;

        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('services:removeRefunded')
            ->setDescription('Remove refunded services')
            ->addOption('dry-run', NULL, InputOption::VALUE_NONE, 'If set, updates will not be executed, only logged');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $batchSize = 50;
        $batchCount = $processed = 0;

        $isDryRun = $input->getOption('dry-run');
        if ($isDryRun) {
            $this->logMessage($output, ILogger::INFO, "<info># dry-run mode enabled</info>");
        }

        foreach ($this->serviceService->getRefundedServices() as $row) {
            try {
                /** @var Service $service */
                $service = $row[0];

                if (count($row) > 1) {
                    throw RemoveRefundedServicesException::multipleServicesDetected($service->getOrder());
                }

                $this->em->remove($service);

                $processed++;
                $batchCount++;

                $this->logMessage(
                    $output,
                    ILogger::DEBUG,
                    sprintf("-> <info>Refunded service <comment>`%d`</comment> has been deleted</info>", $service->getId())
                );

                if ($batchCount % $batchSize === 0) {
                    $this->flushUpdates($output, $isDryRun);
                }

            } catch (Exception $e) {
                $this->logMessage($output, ILogger::ERROR, $e->getMessage());
            }

        }

        $this->flushUpdates($output, $isDryRun);
        $this->logMessage($output, ILogger::DEBUG, "<info># done with <comment>`$processed`</comment> <info>services proceeded</info>");
    }

    /**
     * @param OutputInterface $output
     * @param int $level
     * @param string $message
     */
    private function logMessage(OutputInterface $output, $level, $message)
    {
        $output->writeln($message);
        $this->logger->logMessage($level, sprintf("[%s] %s", $this->getName(), strip_tags($message)));
    }

    /**
     * @param OutputInterface $output
     * @param bool $isDryRun
     */
    private function flushUpdates(OutputInterface $output, $isDryRun)
    {
        if (!$isDryRun) {
            $this->em->flush();
            $this->em->clear();
        }
        $this->logMessage($output, ILogger::DEBUG, '<info># updates have been flushed</info>');
    }
}
