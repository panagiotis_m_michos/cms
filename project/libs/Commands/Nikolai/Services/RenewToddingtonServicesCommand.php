<?php

namespace Console\Commands\Services;

use Basket;
use DateTime;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityManager;
use Entities\Company;
use Entities\Customer;
use Entities\Order;
use Entities\OrderItem;
use Entities\Service;
use Entities\Transaction;
use Exception;
use Product;
use Psr\Log\LoggerInterface;
use Services\CustomerService;
use Services\ProductService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RenewToddingtonServicesCommand extends Command
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var CustomerService
     */
    private $customerService;

    /**
     * @var ProductService
     */
    private $productService;

    /**
     * @param LoggerInterface $logger
     * @param EntityManager $em
     * @param CustomerService $customerService
     * @param ProductService $productService
     * @param string $name
     */
    public function __construct(
        LoggerInterface $logger,
        EntityManager $em,
        CustomerService $customerService,
        ProductService $productService,
        $name = NULL
    )
    {
        $this->logger = $logger;
        $this->em = $em;
        $this->customerService = $customerService;
        $this->productService = $productService;

        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('services:renewToddington')
            ->setDescription('Renew Toddington\'s RO services (2015/2016)')
            ->addOption('dry-run', NULL, InputOption::VALUE_NONE, 'If set, updates will not be executed, only logged');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $isDryRun = $input->getOption('dry-run');
        if ($isDryRun) {
            $this->logger->info('Dry-run mode enabled');
        }

        $customer = $this->customerService->getCustomerByEmail('toddington.harper@belectric.co.uk');
        $services = $this->getServices($customer);
        $this->logger->debug(sprintf('%d services found to renew', count($services)));

        $order = $this->createOrder($customer);

        $basket = new Basket('autoRenewal');
        $basket->clear();

        $newServices = [];
        foreach ($services as $service) {
            $renewalProduct = $service->getRenewalProduct();
            $orderItem = $this->createOrderItem($order, $service->getCompany(), $renewalProduct);
            $order->addItem($orderItem);
            $basket->add($renewalProduct);

            $renewalProduct->setOrderItem($orderItem);
            $newServices[] = $renewalProduct;
        }

        if (!empty($newServices)) {
            $transaction = new Transaction($customer, Transaction::TYPE_FREE);
            $transaction->setOrder($order);
            $transaction->setStatusId(Transaction::STATUS_SUCCEEDED);
            $this->em->persist($transaction);

            $price = $basket->getPrice();

            $order->setSubTotal($price->subTotal2);
            $order->setRealSubtotal($price->subTotal);
            $order->setDiscount($price->discount);
            $order->setVat($price->vat);
            $order->setTotal($price->total);

            if (!$isDryRun) {
                $this->productService->saveServices($newServices);
            }
            $this->logger->debug(sprintf('%d services renewed with total price of £%.2f including VAT', count($newServices), $price->total));

            if (!$isDryRun) {
                $this->updateDates($order);
            }
            $this->logger->debug('Dates changed - dtStart 04/04/2015 and dtExpires 03/04/2016');
        }

        $this->flushUpdates($isDryRun);
    }

    /**
     * @param Customer $customer
     * @return Service[]
     */
    private function getServices(Customer $customer)
    {
        return $this->em->createQueryBuilder()
            ->select('s')
            ->from('Entities\Service', 's')
            ->innerJoin('s.company', 'co')
            ->andWhere('s.serviceTypeId = :serviceTypeId')
            ->andWhere('s.parent IS NULL')
            ->andWhere('s.stateId = :enabledState')
            ->andWhere('co.customer = :toddington')
            ->andWhere('co.incorporationDate IS NOT NULL')
            ->orderBy('s.dtExpires', 'DESC')
            ->setParameters(
                [
                    'serviceTypeId' => Service::TYPE_REGISTERED_OFFICE,
                    'enabledState' => Service::STATE_ENABLED,
                    'toddington' => $customer,
                ]
            )
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Order $order
     */
    private function updateDates(Order $order)
    {
        $this->em->createQueryBuilder()
            ->update('Entities\Service', 's')
            ->set('s.dtStart', ':dtStart')->setParameter('dtStart', new DateTime('2015-04-04'), Type::DATE)
            ->set('s.dtExpires', ':dtExpires')->setParameter('dtExpires', new DateTime('2016-04-03'), Type::DATE)
            ->where('s.order = :order')->setParameter('order', $order)
            ->getQuery()
            ->execute();
    }

    /**
     * @param Customer $customer
     * @return Order
     */
    private function createOrder(Customer $customer)
    {
        $order = new Order($customer);
        $order->setDescription('Batch RO renewal');

        return $order;
    }

    /**
     * @param Order $order
     * @param Company $company
     * @param Product $product
     * @return OrderItem
     */
    private function createOrderItem(Order $order, Company $company, Product $product)
    {
        $orderItem = new OrderItem($order);
        $orderItem->setCompany($company);
        $orderItem->setProductId($product->getId());
        $orderItem->setProductTitle("{$product->getLongTitle()} for company {$company->getCompanyName()}");
        $orderItem->setPrice($product->getPrice());
        $orderItem->setQty(1);
        $orderItem->setSubTotal($product->getPrice());
        $orderItem->setVat($product->getVat());
        $orderItem->setTotalPrice($product->getTotalVatPrice());
        $orderItem->setIsFee(FALSE);
        $orderItem->setNotApplyVat(FALSE);
        $orderItem->setNonVatableValue(0);

        return $orderItem;
    }

    /**
     * @param bool $isDryRun
     */
    private function flushUpdates($isDryRun)
    {
        if (!$isDryRun) {
            $this->em->flush();
            $this->em->clear();
        }
        $this->logger->debug('Updates have been flushed');
    }
}
