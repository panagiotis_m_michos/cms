<?php

namespace Console\Commands\Services;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Internal\Hydration\IterableResult;
use Entities\Service;
use Exception;
use ILogger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RemoveOrphansCommand extends Command
{
    /**
     * @var ILogger
     */
    private $logger;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @param ILogger $logger
     * @param EntityManager $em
     * @param string $name
     */
    public function __construct(ILogger $logger, EntityManager $em, $name = NULL)
    {
        $this->logger = $logger;
        $this->em = $em;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('services:removeOrphans')
            ->setDescription('Remove services without companies')
            ->addOption('dry-run', NULL, InputOption::VALUE_NONE, 'If set, updates will not be executed, only logged');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $batchSize = 50;
        $batchCount = $processed = 0;

        $isDryRun = $input->getOption('dry-run');
        if ($isDryRun) {
            $this->logMessage($output, ILogger::INFO, "<info># dry-run mode enabled</info>");
        }

        foreach ($this->getServices() as $row) {
            /** @var Service $service */
            $service = $row[0];
            $this->em->remove($service);

            $processed++;
            $batchCount++;

            $this->logMessage(
                $output,
                ILogger::DEBUG,
                sprintf("-> <info>orphan service <comment>`%d`</comment> has been deleted</info>", $service->getId())
            );

            if ($batchCount % $batchSize === 0) {
                $this->flushUpdates($output, $isDryRun);
            }
        }

        $this->flushUpdates($output, $isDryRun);
        $this->logMessage($output, ILogger::DEBUG, "<info># done with <comment>`$processed`</comment> <info>services proceeded</info>");
    }

    /**
     * @param OutputInterface $output
     * @param int $level
     * @param string $message
     */
    private function logMessage(OutputInterface $output, $level, $message)
    {
        $output->writeln($message);
        $this->logger->logMessage($level, sprintf("[%s] %s", $this->getName(), strip_tags($message)));
    }

    /**
     * @param OutputInterface $output
     * @param bool $isDryRun
     */
    private function flushUpdates(OutputInterface $output, $isDryRun)
    {
        if (!$isDryRun) {
            $this->em->flush();
            $this->em->clear();
        }
        $this->logMessage($output, ILogger::DEBUG, '<info># updates have been flushed</info>');
    }

    /**
     * @return IterableResult
     */
    private function getServices()
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('s')
            ->from('Entities\Service', 's')
            ->leftJoin('Entities\Company', 'c', 'WITH', 'c = s.company')
            ->where('c IS NULL');
        return $qb->getQuery()->iterate();
    }

}
