<?php

namespace Console\Commands\Services;

use BasketProduct;
use DibiConnection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Internal\Hydration\IterableResult;
use Entities\OrderItem;
use Entities\Service;
use Entities\ServiceSettings;
use Exception;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Services\CompanyService;
use Services\NodeService;
use Services\OrderItemService;
use Services\OrderService;
use Services\ProductService;
use Services\ServiceService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

class CreateInternationalServicesCommand extends Command
{
    /**
     * @var DibiConnection
     */
    private $em;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ServiceService
     */
    private $serviceService;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
    /**
     * @var NodeService
     */
    private $nodeService;

    /**
     * @param EntityManager $em
     * @param LoggerInterface $logger
     * @param ServiceService $serviceService
     * @param CompanyService $companyService
     * @param NodeService $nodeService
     * @param string $name
     */
    public function __construct(
        EntityManager $em,
        LoggerInterface $logger,
        ServiceService $serviceService,
        CompanyService $companyService,
        NodeService $nodeService,
        $name = NULL
    )
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->serviceService = $serviceService;
        $this->companyService = $companyService;
        $this->nodeService = $nodeService;

        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('services:createInternationalServices')
            ->setDescription('Create services from International packages')
            ->addOption('dry-run', 'd', InputOption::VALUE_NONE, 'If set, changes are not flushed to database');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $processed = 0;

        if ($input->getOption('dry-run')) {
            $this->logger->debug("Dry-run flag set");
        }

        $orderItems = $this->getInternationalOrderItem();
        $renewalProduct = $this->nodeService->getProductById(1493);

        foreach ($orderItems as $orderItem) {
            $company = $this->companyService->getCompanyByOrder($orderItem->getOrder());
            if (!$company) {
                $this->logger->warning("Company for order item {$orderItem->getId()} does not exist!");
                continue;
            }

            if ($this->serviceService->getOrderItemService($orderItem)) {
                $this->logger->warning("International service for company {$company->getId()} already exists!");
                continue;
            }

            $product = $this->nodeService->getProductById($orderItem->getProductId());
            $service = new Service(Service::TYPE_PACKAGE_COMPREHENSIVE_ULTIMATE, $product, $company, $orderItem);
            $service->setRenewalProduct($renewalProduct);

            if ($company->isIncorporated()) {
                $incorporationDate = clone $company->getIncorporationDate();
                $service->setDtStart($company->getIncorporationDate());
                $service->setDtExpires($incorporationDate->modify($product->getDuration())->modify('-1 day'));
            }

            $included = $product->getPackageProducts('includedProducts');
            /** @var BasketProduct $product */
            foreach ($included as $product) {
                if ($product->hasServiceType()) {
                    $childService = new Service($product->serviceTypeId, $product, $company, $orderItem);
                    $service->addChild($childService);
                }
            }

            $company->addService($service);

            $settings = new ServiceSettings($company, Service::TYPE_PACKAGE_COMPREHENSIVE_ULTIMATE);

            $this->logger->debug("Creating service for company {$company->getId()}, order item {$orderItem->getId()}");
            $this->em->persist($service);
            $this->em->persist($settings);

            $processed++;
        }

        $this->flushUpdates($input);
        $this->logger->debug("Done with $processed order items processed.");
    }

    /**
     * @param InputInterface $input
     */
    private function flushUpdates(InputInterface $input)
    {
        if (!$input->getOption('dry-run')) {
            $this->em->flush();
        }
        $this->em->clear();
        $this->logger->debug('Updates have been executed');
    }

    /**
     * @return OrderItem[]
     */
    private function getInternationalOrderItem()
    {
        return $this->em->createQueryBuilder()
            ->select('oi')->from('Entities\OrderItem', 'oi')
            ->where('oi.productId IN (:internationalProducts)')
            ->andWhere('(oi.isRefund IS NULL OR oi.isRefund = FALSE)')
            ->andWhere('(oi.isRefunded IS NULL OR oi.isRefunded = FALSE)')
            ->setParameter('internationalProducts', [1430, 1598, 1599, 1493])
            ->getQuery()
            ->getResult();
    }
}
