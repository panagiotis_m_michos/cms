<?php

namespace Console\Commands;

use DibiConnection;
use Exception;
use ILogger;
use Loggers\NikolaiLogger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateCashbacksCommand extends Command
{
    /**
     * @var DibiConnection
     */
    private $db;

    /**
     * @var NikolaiLogger
     */
    private $logger;

    /**
     * @param DibiConnection $db
     * @param NikolaiLogger $logger
     * @param string $name
     */
    public function __construct(DibiConnection $db, NikolaiLogger $logger, $name = NULL)
    {
        $this->db = $db;
        $this->logger = $logger;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('updateCashbacks')
            ->setDescription('Update cashbacks entries to new table structure')
            ->addOption(
                'limit', 'l', InputOption::VALUE_OPTIONAL, 'Limit number of cashbacks to update'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $limit = (int) $input->getOption('limit');
        $wholesalePackagesIds = array(
            1022 => TRUE,
            898 => TRUE,
            899 => TRUE
        );

        $this->logger->logMessage(ILogger::DEBUG, "UpdateCashbacksCommand executed");
        $this->logger->logMessage(ILogger::DEBUG, "Updating cashbacks");
        $output->writeln("<info>Updating cashbacks</info>");

        $this->db->query(
            "ALTER TABLE [cms2_cashback]
            DROP [claimDate],
            CHANGE [bankType] [bankTypeId] enum('BARCLAYS','HSBC') COLLATE 'latin1_swedish_ci' NOT NULL AFTER [companyNumber],
            DROP [email],
            ADD [dateAccountOpened] datetime NULL AFTER [amount],
            ADD [datePaid] datetime NULL AFTER [dateAccountOpened],
            CHANGE [leadDate] [dateLeadSent] DATETIME NULL DEFAULT NULL AFTER [amount],
            ADD [companyId] int(11) NOT NULL AFTER [cashbackId],
            ADD [customerId] int(11) NOT NULL AFTER [companyId],
            ADD [packageTypeId] enum('RETAIL', 'WHOLESALE') COLLATE 'latin1_swedish_ci' NULL AFTER [customerId],
            ADD [cashbackTypeId] enum('BANK', 'CREDITS') COLLATE 'latin1_swedish_ci' NOT NULL AFTER [packageTypeId],
            ADD [groupId] VARCHAR(255) COLLATE 'latin1_swedish_ci' NULL AFTER [cashbackId],
            ADD [statusId] enum('IMPORTED','ELIGIBLE','PAID') NOT NULL AFTER [cashbackId],
            COMMENT='' COLLATE 'utf8_general_ci';"
        );

        $cashbacks = $this->db->select('*')
            ->from(TBL_CASHBACK)
            ->where('customerId = 0')
            ->orderBy('cashbackId ASC');

        if ($limit) {
            $cashbacks = $cashbacks->limit($limit);
        }

        $cashbacks = $cashbacks->iterator;

        $processed = 0;
        $errors = 0;
        try {
            foreach ($cashbacks as $cashback) {
                $company = $this->db->select('company_id, customer_id, product_id')->from(TBL_COMPANY)->where('company_number = %s', $cashback->companyNumber)->fetch();
                if (!$company) {
                    $this->logger->logMessage(ILogger::DEBUG, "Company with number {$cashback->companyNumber} not found");
                    $output->writeln("<error>Company with number {$cashback->companyNumber} not found</error>");
                    $errors++;

                    $updateData = array(
                        'groupId' => NULL,
                        'packageTypeId' => NULL,
                        'dateAccountOpened' => $cashback->openDate,
                        'datePaid' => $cashback->cashbackDate,
                    );
                } else {
                    $packageTypeId = (isset($wholesalePackagesIds[$company->product_id]) ? 'WHOLESALE' : 'RETAIL');
                    $updateData = array(
                        'groupId' => ($cashback->status == 'PAID' ? uniqid('', TRUE) : NULL),
                        'companyId' => $company->company_id,
                        'customerId' => $company->customer_id,
                        'packageTypeId' => $packageTypeId,
                        'dateAccountOpened' => $cashback->openDate,
                        'datePaid' => $cashback->cashbackDate,
                        'amount' => ($cashback->status == 'PAID') ? $cashback->amount : 50,
                    );
                    $logMessage = sprintf(
                        'cashbackId %d (UPDATE) - companyId: %s, customerId: %s, packageTypeId: %s',
                        $cashback->cashbackId,
                        $company->company_id,
                        $company->customer_id,
                        $packageTypeId
                    );
                    $this->logger->logMessage(ILogger::DEBUG, $logMessage);
                }

                if ($cashback->accountNumber && $cashback->sortCode && $company) {
                    $customerData = array(
                        'cashbackType' => 'BANK',
                        'accountNumber' => $cashback->accountNumber,
                        'sortCode' => $cashback->sortCode,
                    );
                    $this->db->update(TBL_CUSTOMERS, $customerData)->where('customerId = %i', $company->customer_id)->execute();
                }

                $this->db->update(TBL_CASHBACK, $updateData)->where('cashbackId = %i', $cashback->cashbackId)->execute();

                $processed++;
                if ($processed % 1000 == 0) {
                    $this->logger->logMessage(ILogger::DEBUG, $processed . ' cashbacks updated');
                    $output->writeln("<comment>{$processed} cashbacks updated</comment>");
                }
            }

            // delete old INELIGIBLE cashbacks
            $this->db->query(
                "DELETE FROM [cms2_cashback]
                WHERE [status] = 'INELIGIBLE';"
            );
            $this->db->query(
                "ALTER TABLE [cms2_cashback]
                DROP [status],
                DROP [openDate],
                DROP [cashbackDate],
                DROP [companyNumber];"
            );
            $this->db->query("ALTER TABLE [cms2_cashback] ADD INDEX ([customerId]);");
            $this->db->query("ALTER TABLE [cms2_cashback] ADD INDEX ([companyId]);");
            // set PAID
            $this->db->query(
                "UPDATE [cms2_cashback]
                SET [statusId] = 'PAID'
                WHERE [datePaid] IS NOT NULL;
                "
            );
            // set ELIGIBLE
            $this->db->query(
                "UPDATE [cms2_cashback]
                SET [statusId] = 'ELIGIBLE'
                WHERE [accountNumber] IS NOT NULL
                AND [dateAccountOpened] IS NOT NULL
                AND [datePaid] IS NULL;"
            );
            // delete imported
            $this->db->query(
                "DELETE FROM [cms2_cashback]
                WHERE [accountNumber] IS NOT NULL
                AND [dateAccountOpened] IS NULL
                AND [datePaid] IS NULL;"
            );
            // clear bank account details
            $this->db->query(
                "UPDATE [cms2_cashback]
                SET [accountNumber] = NULL,
                [sortCode] = NULL
                WHERE [statusId] != 'PAID';"
            );
            // clear customer cashbackType
            $this->db->query(
                "UPDATE [cms2_customers]
                SET [cashbackType] = NULL
                WHERE [accountNumber] IS NULL;"
            );
            // clear cashback cashbackType
            $this->db->query(
                "UPDATE [cms2_cashback]
                SET [cashbackTypeId] = NULL
                WHERE [accountNumber] IS NULL;"
            );

        } catch (Exception $e) {
            $this->logger->logMessage(ILogger::ERROR, $e->getMessage());
            $output->writeln("<error>{$e->getMessage()}</error>");
        }

        $output->writeln("<info>Done.</info> <comment>{$processed}</comment> <info>cashbacks processed in total (<comment>{$errors}</comment> errors)</info>");
        $this->logger->logMessage(ILogger::DEBUG, "Done. {$processed} cashbacks processed in total ({$errors} errors)");
    }
}
