<?php

namespace Console\Commands\OrderItems;

use Basket;
use DateTime;
use DibiConnection;
use Entities\CompanyHouse\FormSubmission;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class DiffLplCommand extends Command
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var DibiConnection
     */
    private $dibiCms;

    /**
     * @var DibiConnection
     */
    private $dibiLpl;

    /**
     * @param LoggerInterface $logger
     * @param DibiConnection $dibiCms
     * @param DibiConnection $dibiLpl
     */
    public function __construct(
        LoggerInterface $logger,
        DibiConnection $dibiCms,
        DibiConnection $dibiLpl
    )
    {
        parent::__construct();

        $this->logger = $logger;
        $this->dibiCms = $dibiCms;
        $this->dibiLpl = $dibiLpl;
    }

    protected function configure()
    {
        $this->setName('orderItems:diffLpl')
            ->setDescription('Diff LPL')
            ->addOption('dry-run', NULL, InputOption::VALUE_NONE, 'If set, updates will not be executed, only logged')
            ->addOption('truncate', NULL, InputOption::VALUE_NONE, 'If set, temp table with LPL services will be re-filled again from LPL');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $date = (new DateTime('-1 year 00:00:00'))->format('Y-m-d');
        $dateLpl = $date;
        $dateLpl = (new DateTime('2014-01-01 00:00:00'))->format('Y-m-d');

        if ($input->getOption('truncate')) {
            $this->dibiCms->query('DROP TABLE IF EXISTS [temp_lpl_sync]');
            $this->dibiCms->query(
                'CREATE TABLE [temp_lpl_sync] (
                  `CoServId` int(11) NOT NULL,
                  `CustomerId` int(11) DEFAULT NULL,
                  `CompanyId` int(11) DEFAULT NULL,
                  `OrderId` int(11) DEFAULT NULL,
                  `OrderItemId` int(11) DEFAULT NULL,
                  `ProductId` int(11) DEFAULT NULL,
                  `PaidDate` datetime NOT NULL,
                  `StartDate` datetime NOT NULL,
                  `ExpiryDate` datetime NOT NULL,
                  `Del` tinyint(4) NOT NULL,
                  KEY `OrderItemId` (`OrderItemId`),
                  KEY `CustomerId` (`CustomerId`),
                  KEY `ProductId` (`ProductId`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8'
            );

            echo 'Getting all services from LPL' . "\n";

            $lplItems = $this->dibiLpl->query(
                'SELECT
                    dbo.tblCoService.CoServId AS CoServId,
                    dbo.tblCoClient.CustomerId AS CustomerId,
                    dbo.tblCoClient.CoClientCoId AS CompanyId,
                    dbo.tblCoService.CoServOrderNo AS OrderId,
                    dbo.tblCoService.CoServItemId AS OrderItemId,
                    dbo.tblCategory.CategoryZone AS ProductId,
                    dbo.tblCoService.Del AS Del,
                    CONVERT(VARCHAR(40), dbo.tblCoService.CoServPaidDate, 120) AS PaidDate,
                    CONVERT(VARCHAR(40), dbo.tblCoService.CoServDate, 120) AS StartDate,
                    CONVERT(VARCHAR(40), dbo.tblCoService.CoServExpiry, 120) AS ExpiryDate
                FROM
                    dbo.tblCoService
                INNER JOIN MsgData.dbo.tblCoClient ON dbo.tblCoService.CoClientId = dbo.tblCoClient.CoClientId
                INNER JOIN MsgData.dbo.tblCategory ON dbo.tblCoService.CoServType = dbo.tblCategory.id
                WHERE
                    1 = 1
                    -- dbo.tblCoService.Del = 0
                    AND dbo.tblCoService.CoServPaidDate >= "' . $dateLpl . ' 00:00:00"'
            )->fetchAll();

            echo 'Inserting all services from LPL' . "\n";

            $this->dibiCms->query('INSERT INTO [temp_lpl_sync] %ex', $lplItems);
        }

        $exportedUntil = (new DateTime('-1 day'))->format('Y-m-d H:i:s');
        $ignoreProducts = [
            1350, // business startup toolkit
            201, // fraud protection
            0, // custom credit
            609, // credit product
            782, // specil offer - guides pack
            321, // Tax and Finance Guide
            322, // Business Start Up Guide
            324, // How to Make a Profit Guide
            325, // 100 ways to save time with Microsoft Office
            569, // Business Forms Pack
            565, // Guides Pack
            615, // Admin Fee
            619, // Barclays Bank Account
            1302, // Shelf Company 2013
            1428, // Shelf company 2014
            1605, // Shelf company 2015
            340, // Custom Annual Return Service
            628, // Non Standard Payment
            1270, // Mail Forwarding for company "DIALECTICA LTD"
            1271, // Mail Forwarding & Telephone Answering for company "PARK AVENUE RARE COINS LIMITED"
            1240, // Test Product
            1024, // HSBC Business Banking
            1625, // Mail Forwarding - 3 Months for company "INTELLISE LTD"
            1405, // Card One Business Account
            1022, // Starter - Ltd Company Formation Package for company "Hesoba Limited"
            898, // Executive - Ltd Company Formation Package for company "The Jacksons Boat Sale Ltd"
            843, // Custom Company Name Change
            666, // Custom Annual Return (DIY)
            733, // Belize Annual Renewal Fee
            735, // British Virgin Islands Annual Renewal Fee
            443, // Mail Forwarding Address (N1) - 3 Month Service for company "VPC I LTD"
            411, // Seychelles Formation (with Bank Account Assistance)
            413, // Offshore Bank Account Assistance
            844, // Custom Company Name Change (Guaranteed Same Day)
        ];

        // get all order items from last year
        $allExportedCmsOrderItems = $this->dibiCms
            ->select('orderItemId')
            ->from('cms2_orders_items')
            ->where('dtc >= "' . $date . ' 00:00:00"')
            ->where('dtExported IS NOT NULL')
            //->where('dtExported < "' . $exportedUntil . '"')
            //->where('productId NOT IN %in', [1350, 340, 201, 0, 609]) //340 - custom AR, 201 - fraud protection, 0/609 - credit
            ->where('productId NOT IN %in', $ignoreProducts) //1350 - business startup toolkit
            ->where('(isRefund = 0 OR isRefund IS NULL)')
            ->where('(isRefunded = 0 OR isRefunded IS NULL)')
            ->where('productId IN (334,379,1317,1315,1316,165,475,1430,342,899,806,1257,1493,1353,1598,1599,1643,1644,1647,1648)')
            //->where('orderItemId IN (1256649,1152258,1204837,1107131,1195988,1142965,1147986,1231282,1252627,1194794,1198024,1195989,1080395,1255171,1093507,1195991,1292234,1210670,1209065,1173332,1195990,1221079,1151043,1256747,1087007,1102545,1256581,1083506,1295797,1149329,1118922,1132302,1109892,1146455,1106527,1082993,1211051,1257913,1256571,1119620,1300699,1194308,1256274,1194793,1256166,1194795,1163373,1141324,1130655,1145428,1156010,1104051,1178317,1146454,1183258,1191058,1118921,1256829,1194705,1194337,1249901,1250444,1252376,1194419,1194642,1084212,1084211,1084213,1084232,1194140,1084210,1281858,1191103,1190848,1256170,1193612,1255994,1193356,1183517,1193138,1256308,1193889,1193125,1194082,1194179,1194804,1194349,1194356,1194375,1194763,1194818,1194833,1194290,1194857,1194843,1194321,1194739,1194719,1194602,1194481,1194485,1194516,1194612,1194468,1194712,1194663,1194458,1194619,1194269,1194367,1255853,1255326,1255841,1255764,1194154,1255835,1194143,1255861,1194176,1194260,1255882,1194132,1194191,1256029,1255690,1255341,1255847,1194551,1255857,1255926,1255879,1194464,1194104,1194406,1226890,1282284,1197434,1282470,1078605,1110842,1105502,1083238,1126262,1206920,1231028,1158832,1252631,1093242,1218271,1295016,1256626,1256675,1159119,1303938,1194775,1182017,1164909,1169551,1220616,1168628,1163043,1288275,1290757,1240448,1275010,1275011,1313066,1313067,1268530,1112157,1290167,1256145,1157308,1166936,1133954,1264386,1114047,1114046,1303223,1306951,1131387,1215734,1230692)')
            //->where('orderItemId IN (1078388)')
            ->fetchPairs();

        echo 'All exported from ' . $date . ': ' . count($allExportedCmsOrderItems) . "\n";

        $chunks = array_chunk($allExportedCmsOrderItems, 5000);

        $c = 5000;
        $missingServicesOnLpl = [];
        foreach ($chunks as $chunk) {
            echo "- Chunk - for total of {$c}";
            $c += 5000;

            $importedlplServices = $this->dibiCms->select('OrderItemId')
                ->from('temp_lpl_sync')
                ->where('OrderItemId IN %in', $chunk)
                ->fetchPairs();

            $diff = array_diff($chunk, $importedlplServices);
            echo " - missing " . count($diff) . "\n";
            $missingServicesOnLpl = array_merge($missingServicesOnLpl, $diff);
        }

        echo 'Missing services on LPL: ' . count($missingServicesOnLpl) . "\n";

        $cmsOrderItemsMissingOnLpl = $this->dibiCms
            ->select('oi.orderItemId')
            ->select('DATE_FORMAT(oi.dtc, "%Y-%m-%d 00:00:00")')->as('datePaid')
            ->select('oi.productId')->as('productId')
            ->select('oi.productTitle')->as('productTitle')
            ->select('co.company_id')->as('companyId')
            ->select('o.customerId')->as('customerId')
            ->select('o.orderId')->as('orderId')
            ->from('cms2_orders_items oi')
            ->innerJoin('cms2_orders o')->on('o.orderId = oi.orderId')
            ->leftJoin('ch_company co')->on('co.company_id = oi.companyId')
            ->where('orderItemId IN %in', $missingServicesOnLpl)
            ->getIterator();

        $noMatches = [];
        $yesMatches = [];
        $multipleMatches = [];

        $i = 100;
        foreach ($cmsOrderItemsMissingOnLpl as $cmsOrderItemMissingOnLpl) {
            if ($i % 100 == 0) {
                echo "- Matching next order items - for total of {$i}\n";
            }
            $i++;

            $lplMatches = $this->dibiCms
                ->select('*')
                ->from('temp_lpl_sync')
                ->where('OrderItemId != %i', $cmsOrderItemMissingOnLpl->orderItemId)
                ->where('(CustomerId IS NULL OR CustomerId = %i)', $cmsOrderItemMissingOnLpl->customerId)
                ->where('(CompanyId IS NULL OR CompanyId = %i)', $cmsOrderItemMissingOnLpl->companyId)
                ->where('(OrderId IS NULL OR OrderId = %i)', $cmsOrderItemMissingOnLpl->orderId)
                ->where('(ProductId IS NULL OR ProductId = %i)', $cmsOrderItemMissingOnLpl->productId)
                ->where('(PaidDate IS NULL OR PaidDate = %s)', $cmsOrderItemMissingOnLpl->datePaid)
                ->where('Del = 0')
                ->fetchAll();

            $matchCount = count($lplMatches);
            if ($matchCount == 0) {
                $noMatches[] = [
                    'cmsOrderItemId' => $cmsOrderItemMissingOnLpl->orderItemId,
                    'customerId' => $cmsOrderItemMissingOnLpl->customerId,
                    'companyId' => $cmsOrderItemMissingOnLpl->companyId,
                    'productId' => $cmsOrderItemMissingOnLpl->productId,
                    'productTitle' => $cmsOrderItemMissingOnLpl->productTitle,
                ];
            } elseif ($matchCount > 1) {
                $lplIds = [];
                foreach ($lplMatches as $lplMatch) {
                    $lplIds[] = $lplMatch->CoServId;
                }
                $multipleMatches[] = [
                    'cmsOrderItemId' => $cmsOrderItemMissingOnLpl->orderItemId,
                    'customerId' => $cmsOrderItemMissingOnLpl->customerId,
                    'companyId' => $cmsOrderItemMissingOnLpl->companyId,
                    'matches (CoServId)' => $matchCount . ' (' . implode(', ', $lplIds) . ')',
                    'productId' => $cmsOrderItemMissingOnLpl->productId,
                    'productTitle' => $cmsOrderItemMissingOnLpl->productTitle,
                ];
            } elseif ($matchCount == 1) {
                $yesMatches[] = [
                    'cmsOrderItemId' => $cmsOrderItemMissingOnLpl->orderItemId,
                    'customerId' => $cmsOrderItemMissingOnLpl->customerId,
                    'companyId' => $cmsOrderItemMissingOnLpl->companyId,
                    'matched (CoServId)' => $lplMatches[0]->CoServId,
                    'productId' => $cmsOrderItemMissingOnLpl->productId,
                    'productTitle' => $cmsOrderItemMissingOnLpl->productTitle,
                ];
            }
        }

        usort($noMatches, [$this, 'sortByProductId']);
        usort($multipleMatches, [$this, 'sortByProductId']);
        usort($yesMatches, [$this, 'sortByProductId']);

        $noMatchesFile = fopen('no_matches2.csv', 'w');
        $multipleMatchesFile = fopen('multiple_matches2.csv', 'w');
        $yesMatchesFile = fopen('yes_matches2.csv', 'w');

        //foreach ($noMatches as $noMatch) {
        //    fputcsv($noMatchesFile, $noMatch);
        //}
        //foreach ($multipleMatches as $multipleMatch) {
        //    fputcsv($multipleMatchesFile, $multipleMatch);
        //}
        //foreach ($yesMatches as $yesMatch) {
        //    fputcsv($yesMatchesFile, $yesMatch);
        //}

        if (!empty($noMatches)) {
            echo "\nNO MATCHES";
            echo "\n----------";
            $noMatchesTable = new Table($output);
            $noMatchesTable
                ->setHeaders(array_keys(reset($noMatches)))
                ->setRows($noMatches);
            $noMatchesTable->render();
        }

        if (!empty($multipleMatches)) {
            echo "\nMULTIPLE MATCHES";
            echo "\n----------------";
            $multipleMatchesTable = new Table($output);
            $multipleMatchesTable
                ->setHeaders(array_keys(reset($multipleMatches)))
                ->setRows($multipleMatches);
            $multipleMatchesTable->render();
        }

        if (!empty($yesMatches)) {
            echo "\nYES MATCHES";
            echo "\n-----------";
            $yesMatchesTable = new Table($output);
            $yesMatchesTable
                ->setHeaders(array_keys(reset($yesMatches)))
                ->setRows($yesMatches);
            $yesMatchesTable->render();
        }
    }

    /**
     * @param array $match1
     * @param array $match2
     * @return bool
     */
    public function sortByProductId($match1, $match2)
    {
        return $match1['productId'] > $match2['productId'];
    }
}
