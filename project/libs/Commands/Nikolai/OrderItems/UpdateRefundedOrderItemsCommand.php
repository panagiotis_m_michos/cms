<?php

namespace Console\Commands\OrderItems;

use Basket;
use Doctrine\ORM\EntityManager;
use Entities\CompanyHouse\FormSubmission;
use Entities\OrderItem;
use Exception;
use Psr\Log\LoggerInterface;
use Repositories\OrderItemRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateRefundedOrderItemsCommand extends Command
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var OrderItemRepository
     */
    private $orderItemRepository;

    /**
     * @param LoggerInterface $logger
     * @param EntityManager $em
     * @param OrderItemRepository $orderItemRepository
     */
    public function __construct(LoggerInterface $logger, EntityManager $em, OrderItemRepository $orderItemRepository)
    {
        parent::__construct();

        $this->logger = $logger;
        $this->em = $em;
        $this->orderItemRepository = $orderItemRepository;
    }

    protected function configure()
    {
        $this->setName('orderItems:updateRefundedOrderItems')
            ->setDescription('Update refunded order items references')
            ->addOption('update', NULL, InputOption::VALUE_NONE, 'If set, updates will be flushed to database')
            ->addOption('only-output', NULL, InputOption::VALUE_NONE, 'Output table with matched order items');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($input->getOption('update')) {
            $this->logger->info('Update mode set, changes will be flushed to database');
        }

        $clonedRefunedItems = $this->em->createQueryBuilder()
            ->select('oi')
            ->from('Entities\OrderItem', 'oi')
            ->where('oi.isRefund = TRUE')
            ->andWhere('oi.refundedOrderItem IS NULL')
            ->getQuery()
            ->iterate();

        $multiple = [];
        $no = [];
        $yes = [];
        foreach ($clonedRefunedItems as $clonedRefunedItemData) {
            /** @var OrderItem $refundItem */
            $refundItem = $clonedRefunedItemData[0];

            $originalItemsQuery = $this->em->createQueryBuilder()
                ->select('oi')
                ->from('Entities\OrderItem', 'oi')
                ->where('oi.order = :order')
                ->andWhere('oi.isRefunded = TRUE')
                ->andWhere('oi.productId = :productId')
                ->setParameter('order', $refundItem->getOrder())
                ->setParameter('productId', $refundItem->getProductId());

            if ($refundItem->getCompany()) {
                $originalItemsQuery
                    ->andWhere('oi.company = :company')
                    ->setParameter('company', $refundItem->getCompany());
            } else {
                $originalItemsQuery->andWhere('oi.company IS NULL');
            }

            if ($refundItem->getCompanyNumber()) {
                $originalItemsQuery
                    ->andWhere('oi.companyNumber = :companyNumber')
                    ->setParameter('companyNumber', $refundItem->getCompanyNumber());
            } else {
                $originalItemsQuery->andWhere('oi.companyNumber IS NULL');
            }

            $originalItems = $originalItemsQuery
                ->getQuery()
                ->getResult();

            $count = count($originalItems);
            if ($count == 1) {
                $yes[$refundItem->getId()] = $originalItems[0]->getId();
            } elseif ($count > 1) {
                $multiple[] = $refundItem->getId();
            } elseif ($count == 0) {
                $no[] = $refundItem->getId();
            }
        }

        if ($input->getOption('only-output')) {
            (new Table($output))
                ->setHeaders(['NO - orderItemId'])
                ->setRows(
                    array_map(
                        function ($item) {
                            return [$item];
                        },
                        $no
                    )
                )
                ->render();

            (new Table($output))
                ->setHeaders(['MULTIPLE - orderItemId'])
                ->setRows(
                    array_map(
                        function ($item) {
                            return [$item];
                        },
                        $multiple
                    )
                )
                ->render();

            (new Table($output))
                ->setHeaders(['YES (count)'])
                ->setRows([[count($yes)]])
                ->render();

            die;
        }

        foreach ($yes as $refundItemId => $originalItemId) {
            $this->logger->debug(
                sprintf(
                    'Updating refund order item %d reference to original order item %d',
                    $refundItemId,
                    $originalItemId
                )
            );

            if ($input->getOption('update')) {
                $this->em
                    ->createQuery(
                        'UPDATE Entities\OrderItem oi
                        SET oi.refundedOrderItem = :refundedOrderItemId
                        WHERE oi = :refundOrderItemId'
                    )
                    ->setParameter('refundedOrderItemId', $originalItemId)
                    ->setParameter('refundOrderItemId', $refundItemId)
                    ->execute();
            }
        }
    }
}
