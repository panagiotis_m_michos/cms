<?php

namespace Commands\Nikolai\OrderItems;

use DateTime;
use Exception;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Services\OrderItemService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MarkItemsAsExportedCommand extends Command
{
    /**
     * @var OrderItemService
     */
    private $orderItemService;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param OrderItemService $orderItemService
     * @param LoggerInterface $logger
     * @param string $name
     */
    public function __construct(OrderItemService $orderItemService, LoggerInterface $logger, $name = NULL)
    {
        $this->orderItemService = $orderItemService;
        $this->logger = $logger;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('orderItems:markItemsAsExported')
            ->setDescription('Set dtExported for order items')
            ->addArgument('dateTo', InputArgument::REQUIRED, 'Date to order items will be marked as exported (Y-m-d)');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->logMessage($output, LogLevel::INFO, "<info>MarkItemsAsExportedCommand</info> <comment>started</comment>");

            $dateTo = DateTime::createFromFormat('Y-m-d', $input->getArgument('dateTo'));
            if ($dateTo === FALSE) {
                throw new InvalidArgumentException("Invalid date format");
            }

            $result = $this->orderItemService->markItemsAsExported($dateTo);
            $this->logMessage($output, LogLevel::INFO, "Affected rows: $result");

            $this->logMessage($output, LogLevel::INFO, "<info>MarkItemsAsExportedCommand</info> <comment>finished</comment>");
        } catch (Exception $e) {
            $this->logMessage($output, LogLevel::ERROR, sprintf("<error>%s</error>", $e->getMessage()));
        }
    }

    /**
     * @param OutputInterface $output
     * @param int $level
     * @param string $message
     */
    private function logMessage(OutputInterface $output, $level, $message)
    {
        $output->writeln($message);
        $this->logger->log($level, sprintf("[%s] %s", $this->getName(), strip_tags($message)));
    }
}
