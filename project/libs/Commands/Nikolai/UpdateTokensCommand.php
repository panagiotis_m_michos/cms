<?php

namespace Console\Commands;

use Doctrine\ORM\EntityManager;
use Entities\Payment\Token;
use Exception;
use ILogger;
use Loggers\NikolaiLogger;
use SagePay\Reporting\Request\TokenDetails;
use SagePay\Reporting\SagePayFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use UnexpectedValueException;
use Utils\Date;

class UpdateTokensCommand extends Command
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var NikolaiLogger
     */
    private $logger;

    /**
     * @var string
     */
    private $packagesDir;

    /**
     * @param EntityManager $em
     * @param string $packagesDir
     * @param NikolaiLogger $logger
     * @param string $name
     */
    public function __construct(EntityManager $em, NikolaiLogger $logger, $packagesDir, $name = NULL)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->packagesDir = $packagesDir;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('updateTokens')
            ->setDescription('Update token details from SagePay')
            ->addOption(
                'limit', 'l', InputOption::VALUE_OPTIONAL, 'Limit number of tokens to update', 1000
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws UnexpectedValueException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $limit = (int) $input->getOption('limit');

        $qb = $this->em->createQueryBuilder();
        $tokensCount = $qb->select('COUNT(t.tokenId)')
            ->from('Entities\Payment\Token', 't')
            ->where('t.cardExpiryDate IS NULL')
            ->getQuery()
            ->getSingleScalarResult();

        $batchSize = 100;

        $this->logger->logMessage(ILogger::DEBUG, "UpdateTokensCommand executed");
        $output->writeln("<info>Tokens without details: <comment>{$tokensCount}</comment></info>");
        $this->logger->logMessage(ILogger::DEBUG, "Updating {$limit} from {$tokensCount} tokens");
        $output->writeln("<info>Updating <comment>{$limit}</comment> from <comment>{$tokensCount}</comment> tokens</info>");

        $qb = $this->em->createQueryBuilder();
        $tokens = $qb->select('t')
            ->from('Entities\Payment\Token', 't')
            ->where('t.cardExpiryDate IS NULL')
            ->orderBy('t.tokenId', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->iterate();

        $sagepay = SagePayFactory::createGateway($this->packagesDir);

        $batchCount = 0;
        $processed = 0;
        $errors = 0;
        foreach ($tokens as $token) {
            /** @var $tokenEntity Token */
            $tokenEntity = $token[0];
            try {
                $response = $sagepay->getTokenDetails(new TokenDetails($tokenEntity->getIdentifier()));

                $cardExpiryDate = Date::createFromFormat('my', $response->getExpiryDate());
                $cardExpiryDate->modify('last day of this month 00:00:00');

                $logMessage = sprintf(
                    'tokenId %d (PREPARE TO UPDATE) - cardHolder: "%s", cardType: "%s", cardExpiryDate: "%s" (%s)',
                    $tokenEntity->getTokenId(),
                    $response->getCardHolder(),
                    $response->getPaymentSystem(),
                    $response->getExpiryDate(),
                    $cardExpiryDate->format('Y-m-d')
                );
                $this->logger->logMessage(ILogger::DEBUG, $logMessage);

                $tokenEntity->setCardHolder($response->getCardHolder());
                $tokenEntity->setCardType($response->getPaymentSystem());
                $tokenEntity->setCardExpiryDate($cardExpiryDate);
                $this->em->persist($tokenEntity);
            } catch (Exception $e) {
                $errors++;

                $logMessage = sprintf(
                    'tokenId %d - %s',
                    $tokenEntity->getTokenId(),
                    $e->getMessage()
                );
                $this->logger->logMessage(ILogger::WARNING, $logMessage);
                $output->writeln("<error>tokenId: {$tokenEntity->getTokenId()} - {$e->getMessage()}</error>");
            }

            $processed++;
            $batchCount++;
            if ($batchCount % $batchSize === 0) {
                $batchCount = 0;
                $this->flushUpdates();
                $output->writeln("<comment>{$processed}</comment> <info>tokens processed</info>");
            }
        }

        if ($batchCount > 0) {
            $this->flushUpdates();
        }

        $output->writeln("<info>Done.</info> <comment>{$processed}</comment> <info>tokens processed in total (<comment>{$errors}</comment> errors)</info>");
        $this->logger->logMessage(ILogger::DEBUG, "Done. {$processed} tokens processed in total ({$errors} errors)");
    }

    private function flushUpdates()
    {
        $this->em->flush();
        $this->em->clear();
        $this->logger->logMessage(ILogger::DEBUG, 'Updates have been executed');
    }
}
