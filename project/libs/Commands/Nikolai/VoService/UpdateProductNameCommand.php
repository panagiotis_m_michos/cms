<?php

namespace Console\Commands\VoService;

use Doctrine\ORM\EntityManager;
use VoServiceModule\Entities\VoServiceQueue;
use Exception;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Services\NodeService;
use VoServiceModule\Services\VoServiceQueueService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateProductNameCommand extends Command
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var VoServiceQueueService
     */
    private $voServiceQueueService;

    /**
     * @var NodeService
     */
    private $nodeService;

    /**
     * @param LoggerInterface $logger
     * @param EntityManager $em
     * @param VoServiceQueueService $voServiceQueueService
     * @param NodeService $nodeService
     * @param string $name
     */
    public function __construct(LoggerInterface $logger, EntityManager $em, VoServiceQueueService $voServiceQueueService, NodeService $nodeService, $name = NULL)
    {
        $this->logger = $logger;
        $this->em = $em;
        $this->nodeService = $nodeService;
        $this->voServiceQueueService = $voServiceQueueService;

        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('voService:updateProductName')
            ->setDescription('Updates missing product names')
            ->addOption('dry-run', NULL, InputOption::VALUE_NONE, 'If set, updates will not be executed, only logged');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|NULL|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $batchSize = 50;
        $batchCount = $processed = 0;

        $isDryRun = $input->getOption('dry-run');
        if ($isDryRun) {
            $this->logMessage($output, LogLevel::INFO, "<info># dry-run mode enabled</info>");
        }

        foreach ($this->voServiceQueueService->getServicesWithEmptyProductName() as $row) {
            try {
                /** @var VoServiceQueue $item */
                $item = $row[0];

                $product = $this->nodeService->getProductById($item->getProductId(), TRUE);
                $item->setProductName($product->getLngTitle());

                $processed++;
                $batchCount++;

                $this->logItemUpdate($output, $item);

                if ($batchCount % $batchSize === 0) {
                    $this->flushUpdates($output, $isDryRun);
                }

            } catch (Exception $e) {
                $this->logMessage($output, LogLevel::ERROR, $e->getMessage());
            }
        }

        $this->flushUpdates($output, $isDryRun);
        $this->logMessage(
            $output,
            LogLevel::INFO,
            "<info># done with <comment>`$processed`</comment> <info>items proceeded</info>"
        );
    }


    /**
     * @param OutputInterface $output
     * @param VoServiceQueue $item
     */
    private function logItemUpdate(OutputInterface $output, VoServiceQueue $item)
    {
        $this->logMessage(
            $output,
            LogLevel::INFO,
            sprintf(
                "-> <info>Product name <comment>%s</comment> for product id <comment>%d</comment> has been updated for item <comment>%d</comment></info>",
                $item->getProductName(),
                $item->getProductId(),
                $item->getId()
            )
        );
    }

    /**
     * @param OutputInterface $output
     * @param int $level
     * @param string $message
     */
    private function logMessage(OutputInterface $output, $level, $message)
    {
        $output->writeln($message);
        $this->logger->log($level, sprintf("[%s] %s", $this->getName(), strip_tags($message)));
    }

    /**
     * @param OutputInterface $output
     * @param bool $isDryRun
     */
    private function flushUpdates(OutputInterface $output, $isDryRun)
    {
        if (!$isDryRun) {
            $this->em->flush();
            $this->em->clear();
        }
        $this->logMessage($output, LogLevel::INFO, '<info># updates have been flushed</info>');
    }

}
