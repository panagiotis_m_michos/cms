<?php

namespace Console\Commands\Submissions;

use Basket;
use DibiConnection;
use Entities\CompanyHouse\FormSubmission;
use Exception;
use Psr\Log\LoggerInterface;
use Services\SubmissionService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ResendFakedCommand extends Command
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var DibiConnection
     */
    private $dibi;

    /**
     * @var SubmissionService
     */
    private $submissionService;

    /**
     * @param LoggerInterface $logger
     * @param DibiConnection $dibi
     * @param SubmissionService $submissionService
     * @param string $name
     */
    public function __construct(
        LoggerInterface $logger,
        DibiConnection $dibi,
        SubmissionService $submissionService,
        $name = NULL
    )
    {
        $this->logger = $logger;
        $this->dibi = $dibi;
        $this->submissionService = $submissionService;

        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('submissions:resendFaked')
            ->setDescription('Send faked pending form submission from 9/10/2015')
            ->addOption('dry-run', NULL, InputOption::VALUE_NONE, 'If set, updates will not be executed, only logged');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $isDryRun = $input->getOption('dry-run');
        if ($isDryRun) {
            $this->logger->info('Dry-run mode enabled');
        }

        $submissionIds = $this->dibi->select('form_submission_id')
            ->from(TBL_FORM_SUBMISSIONS)
            ->where('response = %s', FormSubmission::RESPONSE_PENDING)
            ->where('toResend = 1')
            ->fetchPairs();

        foreach ($submissionIds as $submissionId) {
            $submissionEntity = $this->submissionService->getFormSubmissionById($submissionId);
            $oldSubmission = $this->submissionService->getOldFormSubmission(
                $submissionEntity->getCompany(),
                $submissionEntity->getId()
            );

            if ($isDryRun) {
                $response = 'DRY-RUN';
            } else {
                $this->dibi->update(TBL_FORM_SUBMISSIONS, ['response' => NULL])
                    ->where('form_submission_id = %i', $submissionId)
                    ->execute();

                $response = $oldSubmission->sendRequest();

                $this->dibi->update(TBL_FORM_SUBMISSIONS, ['toResend' => 0])
                    ->where('form_submission_id = %i', $submissionId)
                    ->execute();
            }

            $this->logger->debug(
                'Form submission resent',
                [
                    'formSubmissionId' => $submissionId,
                    'type' => $oldSubmission->getFormIdentifier(),
                    'response' => $response,
                    'dtm' => $submissionEntity->getDtm()->format('d/m/Y H:i:s'),
                ]
            );
        }
    }
}
