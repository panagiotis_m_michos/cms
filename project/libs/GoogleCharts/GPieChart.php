<?php

class GPieChart extends GChart{
	function __construct($width = 350, $height = 200) {
		$this -> setProperty('cht', 'p');
		$this -> setDimensions($width, $height);
	}
	public function getApplicableLabels($labels) {
		return array_splice($labels, 0, count($this->values[0]));
	}
	public function set3D($is3d = true, $resize = true){
		if($is3d){
			$this -> setProperty('cht', 'p3');
			if ($resize)
				$this -> setDimensions($this->getWidth() * 1.5, $this->getHeight());
		}
		else {
			$this -> setProperty('cht', 'p');
			if ($resize)
				$this -> setDimensions($this->getWidth() / 1.5, $this->getHeight());
		}
	}
	/**
	 * Sets the labels for Pie Charts
	 *
	 * @param $labels Array
	 */
	public function setLabels($labels) {
		$this -> setProperty('chl', urlencode($this->encodeData($this->getApplicableLabels($labels),"|")));
	}
	/**
	 * Rotates the chart.
	 *
	 * By default, the first series is drawn starting at 3:00, continuing clockwise around the chart, but 
	 * you can specify a custom rotation using this function.
	 *
	 * @param $angle Integer A floating point value describing how many radians to rotate the chart clockwise. 
	 * One complete turn is 2� (2 pi�about 6.28) radians.
	 * @param $degree Bool Specifies if $angle is in degrees and not in radians. The function will to the conversion.
	 */
	public function setRotation($angle, $degree = false) {
		if ($degree)
			$angle = ($angle / 360) * 6.2831;
		$this -> setProperty('chp', $angle);
	}
}