<?php

class GOverlappedBarChart extends GBarChart{
	function __construct($width = 200, $height = 200){
		$this -> setChartType('o', 'h');
		$this->setDimensions($width, $height);			
	}
	public function setHorizontal($isHorizontal = true){
		if($isHorizontal){
			$this -> setChartType('o', 'v');
		}
		else{
			$this -> setChartType('o', 'h');
		}
	}
}