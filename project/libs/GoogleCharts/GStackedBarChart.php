<?php

class GStackedBarChart extends GBarChart{
	function __construct($width = 200, $height = 200){
		$this -> setChartType('s', 'h');
		$this -> setDimensions($width, $height);			
	}
	public function setHorizontal($isHorizontal = true){
		if($isHorizontal){
			$this -> setChartType('s', 'v');
		}
		else{
			$this -> setChartType('s', 'h');
		}
	}
}