<?php

class GMeterChart extends GChart{
	/**
	 * Google-o-Meter Chart constructor.
	 *
	 * Please see documentation for specia usage of functions setVisibleAxes(), addAxisLabel(), and setColors().
	 */
	function __construct($width = 200, $height = 200){
		$this -> setDimensions($width, $height);
		$this -> setProperty('cht','gom');
	}
	public function getApplicableLabels($labels) {
		return array_splice($labels, 0, count($this->values[0]));
	}
}
