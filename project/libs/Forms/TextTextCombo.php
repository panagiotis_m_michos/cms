<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    TextTextCombo.php 2010-08-19 divak@gmail.com
 */

class TextTextCombo extends FControl
{
	/**
	 * @var int
	 */
	private $counter = 1;
	
	/**
	 * @var int
	 */
	private $maximumRows = 0;
	
	/**
	 * @var string
	 */
	private $text1Label;
	
	/**
	 * @var string
	 */
	private $areaLabel;
	
	/**
	 * @var string
	 */
	private $text2Label;
	
	/**
	 * @param array $value
	 * @return TextTextCombo
	 */
	public function setValue($value)
	{
		if (isset($value, $value['counter'])) {
			$this->value = array();
			$this->counter = $this->value['counter'] = $value['counter'];
			if ($this->counter < 1) $this->counter = 1;
            unset($value['counter']);
            foreach ($value as $key => $value) {
                $data['rows'][substr($key,4,1)][substr($key,6)] =  $value;
            }
            
			for ($i = 1; $i <= $this->counter; $i++) {
				// check for maximum rows
				if ($this->maximumRows != 0 && $i > $this->maximumRows) {
					$this->counter = $this->maximumRows;
					break;
				}
				if (isset($data['rows'][$i]['text1'], $data['rows'][$i]['text2'], $data['rows'][$i]['itemId'])) {
					$this->value['rows'][$i] = array(
						'text1' => $data['rows'][$i]['text1'],
						'text2' => $data['rows'][$i]['text2'],
						'itemId' => $data['rows'][$i]['itemId']
					);
				}
			}
		}
		return $this;
	}
		/**
	 * @param array $value
	 * @return TextTextCombo
	 */
	public function setValue2($value)
	{//pr($value);exit;
		if (isset($value->rows, $value->counter)) {
			$this->value = array();
			$this->counter = $this->value['counter'] = $value->counter;
			if ($this->counter < 1) $this->counter = 1;
			for ($i = 1; $i <= $this->counter; $i++) {
				// check for maximum rows
				if ($this->maximumRows != 0 && $i > $this->maximumRows) {
					$this->counter = $this->maximumRows;
					break;
				}
				if (isset($value->rows->$i->text1, $value->rows->$i->text2, $value->rows->$i->itemId)) {
					$this->value['rows'][$i] = array(
						'text1' => $value->rows->$i->text1,
						'text2' => $value->rows->$i->text2,
						'itemId' => $value->rows->$i->itemId
					);
				}
			}
		}
		return $this;
	}
	/**
	 * @param int $maximumRows
	 * @return TextTextCombo
	 */
	public function setMaximumRows($maximumRows)
	{
		$this->maximumRows = (int) $maximumRows;
		return $this;
	}
	
	
	/**
	 * @param string $text1Label
	 * @return TextTextCombo
	 */
	public function setText1Label($text1Label)
	{
		$this->text1Label = $text1Label;
		return $this;
	}
	
	/**
	 * @param string $text2Label
	 * @return TextTextCombo
	 */
	public function setText2Label($text2Label)
	{
		$this->text2Label = $text2Label;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getValue()
	{
		return $this->value;
	}

	/**
	 * @return Html
	 */
	public function getControl()
	{
		$el = Html::el();
		$table = $el->create('table')->border(0);
		$this->addControlLabels($table);
		$this->addControlControls($table);
		$this->addControlLinks($table);
		$this->addControlCounter($el);
		$this->addControlHandlingJavascript($el);
		return $el;
	}
	
	/**
	 * Provides adding labels
	 *
	 * @param Html $table
	 */
	private function addControlLabels(Html $table)
	{
		// labels
		$tr = $table->create('tr');
		$tr->create('td');
		$tr->create('td')->setHtml($this->text1Label);
		$tr->create('td')->setHtml($this->text2Label);
	}
	
	/**
	 * Provides adding controls
	 *
	 * @param Html $table
	 */
	private function addControlControls(Html $table)
	{
		$form = $this->owner;
		for ($i=1; $i<=$this->counter; $i++) {
			
			// row
			$className = sprintf("%s_row_%s", $this->getName(), $i);
			$tr = $table->create('tr')->class($className);
			$tr->create('td')->setText("$i.")->class('row_num')->valign('top');
			
			// text
			$td = $tr->create('td')->valign('top');

            $name = sprintf("%s[row_%s_text1]", $this->getName(), $i);
			$text1 = $form->getElement('Text', $name)->class('text1');
			if (isset($this->value['rows'][$i]['text1'])) $text1->setValue($this->value['rows'][$i]['text1']);
			$td->add($text1->getControl());
			
			// text
			$td = $tr->create('td')->valign('top');
            $name = sprintf("%s[row_%s_text2]", $this->getName(), $i);
			$text2 = $form->getElement('Text', $name)->class('text2');
			if (isset($this->value['rows'][$i]['text2'])) $text2->setValue($this->value['rows'][$i]['text2']);
			$td->add($text2->getControl());
			
			// itemId
			$td = $tr->create('td');
			$name = sprintf("%s[row_%s_itemId]", $this->getName(), $i);
			$text = $form->getElement('Hidden', $name);
			if (isset($this->value['rows'][$i]['itemId'])) $text->setValue($this->value['rows'][$i]['itemId']);
			$td->add($text->getControl());
		}
	}
	
	/**
	 * Provides adding counter
	 *
	 * @param Html $table
	 */
	private function addControlCounter(Html $el)
	{
		// counter
		$name = sprintf("%s[counter]", $this->getName());
		$counter = $this->owner->getElement('Hidden', $name);
		$counter->setValue($this->counter);
		$el->add($counter->getControl());
	}
	
	/**
	 * Provides adding links add/remove
	 *
	 * @param Html $table
	 */
	private function addControlLinks(Html $table)
	{
		$tr = $table->create('tr');
		$tr->create('td');
		
		// link to add
		$id = sprintf("%s_add", $this->getName());
		$add = Html::el('a')
			->setText('Add Another')
			->href('#')
			->id($id)
			->class('ui-widget-content ui-corner-all');
		
		// link to remove
		$id = sprintf("%s_remove", $this->getName());
		$remove = Html::el('a')
			->setText('Remove')
			->href('#')
			->id($id)
			->class('ui-widget-content ui-corner-all');
			
		$tr->create('td')->add($add)->add($remove)->style('padding: 5px 0 0 0;');
	}
	
	/**
	 * Provides adding handling javascript
	 *
	 * @param Html $el
	 */
	private function addControlHandlingJavascript(Html $el)
	{
		$name = $this->getName();
		$nameSelector = str_replace(array('[', ']'), array('\\\[', '\\\]'), $name);
		
		$maximumRows = $this->maximumRows;
		
		$el->create('script')
			->type('text/javascript')
			->setHtml("
			
				/**
				 * add link
				 */
				$('#".$nameSelector."_add').click(function() {
					
					// get counter
					counter = $('#".$nameSelector."\\\[counter\\\]');
					var counterVal = parseInt(counter.val());
					var nextCounterVal = counterVal + 1;
					
					// check for maximum rows
					if ($maximumRows !=0 && nextCounterVal > $maximumRows) {
						alert('Maximum rows is $maximumRows');
						return false;
					}
			
					// get clone of last row		
					clone = $('.".$nameSelector."_row_' + counterVal)
						.clone()
						.attr('class', '".$name."_row_' + nextCounterVal);
						
					// change counter for row
					$('td.row_num', clone).html(nextCounterVal + '.');
					
					// change id for select
					$('input.text1', clone)
						.attr('id', '".$name."[row_' + nextCounterVal + '_text1]')
						.attr('name', '".$name."[row_' + nextCounterVal + '_text1]')
						.val('');
						
					// change id for text
					$('input.text2', clone)
						.attr('id', '".$name."[row_' + nextCounterVal + '_text2]')
						.attr('name', '".$name."[row_' + nextCounterVal + '_text2]')
						.val('');
						
					// change id for itemId
					$('input[name*=itemId]', clone)
						.attr('id', '".$name."[row_' + nextCounterVal + '_itemId]')
						.attr('name', '".$name."[row_' + nextCounterVal + '_itemId]')
						.val(0);
						
					
					// insert after last row
					clone.insertAfter($('.".$nameSelector."_row_' + counterVal));
					
					// increment counter
					counter.val(nextCounterVal);
					return false;
				});
				
				/**
				 * remove link
				 */
				$('#".$nameSelector."_remove').click(function() {
				
					// get counter
					counter = $('#".$nameSelector."\\\[counter\\\]');
					var counterVal = parseInt(counter.val());
					var prevCounterVal = counterVal - 1;
					
					// at least one row has to remain
					if (prevCounterVal < 1) {
						alert('At least one row is required');
						return false;
					}
					
					// remove row
					$('.".$nameSelector."_row_' + counterVal).remove();
					
					// decrement counter
					counter.val(prevCounterVal);
					return false;
				});
			");
	}
	
	
	/**
	 * Validator for this control - check if all rows are filled
	 *
	 * @param TextTextCombo $control
	 * @param string $error
	 * @param array $params
	 */
	static public function Validator_textTextRequired(TextTextCombo $control, array $error, array $params)
	{
		$checkbox = $control->owner[$params['checkbox']]->getValue();
		$values = $control->getValue();
		if ($checkbox == 1) {
		foreach ($values['rows'] as $value) {
			if (empty($value['text1']) || empty($value['text2'])) {
				return $error[0];
				}
			if (!is_numeric($value['text2'])){
					return $error[1];
				}
			}
		}
		return TRUE;
	}
}