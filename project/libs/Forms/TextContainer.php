<?php

class TextContainer extends FControl
{
	const DIVIDER = '#===#';
	
	public $value = array('aa', 'bb', 'ccc'); 
	
	/**
	 * @param mixed $value
	 * @return provides fluent interfaces
	 */
	public function setValue($value)
	{
		if (is_array($value)) {
			$this->value = $value;
		} else {
			$this->value = explode(self::DIVIDER, $value);
		}
	}
	
	/**
	 * @return string
	 */
	public function getValue()
	{
		return $this->value;
	}

	
	
	/**
	 * @return string $html
	 */
	public function getControl()
	{
		$el = Html::el();
		$table = $el->create('table')->border(0);
		
		// text area
		$tr = $table->create('tr');
		$area = FForm::getElement('area', $this->getName() . '_area')->cols(40)->rows(7);
		$tr->create('td')->add($area->getControl());
		
		// button
		$tr = $table->create('tr');
		$submit = FForm::getElement('submit', $this->getName() . '_button')->class('btn')->setValue('Add');
		$tr->create('td')->add($submit->getControl())->align('right');
		
		// container
		$tr = $table->create('tr');
		$ul = Html::el('ul')->id($this->getName() . '_ul');
		foreach ($this->value as $value) {
			$ul->create('li')->create('span')->class('text')->setHtml($value);
		}
		$tr->create('td')->add($ul);
		
		// hidden
		$tr = $table->create('tr');
		$submit = FForm::getElement('area', $this->getName())->setValue(implode(self::DIVIDER, $this->value))->cols(40)->rows(10);
		$tr->create('td')->add($submit->getControl());
		
		$el->add(self::getCssStyle($this->getName()));
		$el->add(self::getJavascript($this->getName()));
		
		return $el;
	}
	
	static public function getCssStyle($name)
	{
		$style = Html::el('style');
		$style->setText("
			#{$name}_ul {
				padding; 0;
				margin: 10px 0 0 0;
				list-style-type: none;
			}
				#{$name}_ul li {
					border: 1px solid #d2d2d2;
					margin: 0 0 10px 0;
					padding: 10px 10px 5px 3px;
					position: relative;
					width: 330px;
					overflow: hidden;			
				}
				#{$name}_ul li span.close {
					position: absolute;
					right: 3px;
					top: -3px;
					cursor: pointer;
				}
		");
		return $style;
	}
	
	static public function getJavascript($name)
	{
		$divider = self::DIVIDER;
		$script = Html::el('script')->type('text/javascript');
		$script->setHtml("
			$(document).ready(function() {
			
				$('#{$name}_ul').sortable({
					placeholder: 'ui-state-highlight',
					stop: function(event, ui) {
						fillHiddenField_{$name}();
					}
				});
				$('#{$name}_ul').disableSelection();
			
				$('#{$name}_ul li').each(function(i) {
					span = $('<span>x</span>').attr('class', 'close').click(function() {
						$(this).parent().remove();
						fillHiddenField_{$name}();
					});
					$(this).append(span);
				});
			
				$('#{$name}_button').click(function() {
					var text = $('#{$name}_area').val();
					if (text != '') {
						text = $('<span>').attr('class', 'text').html(text);
						var li = $('<li/>').html(text);
						$('#{$name}_ul').append(li);
						$('#{$name}_area').val('');
						fillHiddenField_{$name}();
					} else {
						alert('Please provide value');
					}
					return false;
				});
				
			});
			
			function fillHiddenField_{$name}() {
				var fields = new Array;
				$('#{$name}_ul li span.text').each(function(i) {
					fields[i] = $(this).html();
				});
				if (fields.length != 0) {
					var html = fields.join('$divider');
				} else {
					var html = '';
				}
				$('#{$name}').val(html);
			}
			
		");
		
		return $script;
	}
	
	
	
	
	
}