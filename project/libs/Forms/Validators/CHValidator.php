<?php

use Nette\Forms\IControl;
use Nette\Object;

class CHValidator extends Object
{
    public static function Validator_requiredResidentialAddress($control, $error, $params)
    {
        $value = $control->getValue();
        if ($params == 'add') {
            if ($control->owner['residentialAddress']->getValue() == 1 && empty($value)) {
                return $error;
            }
        } else {
            if (empty($value)) {
                return $error;
            }
        }
        return TRUE;
    }

    public static function Validator_directorEditEEARequired($control, $error, $param)
    {
        if ($control->owner['changeEEA']->getValue() == 1) {
            if ($control->owner['eeaType']->getValue() !== NULL) {
                // EEA
                if ($control->owner['eeaType']->getValue() == 1) {
                    if (($control->getName() == 'place_registered' || $control->getName() == 'registration_number') && $control->getValue() == '') {
                        return $error;
                    }
                    // non EEA
                } else {
                    if (($control->getName() == 'place_registered' || $control->getName() == 'law_governed' || $control->getName() == 'legal_form') && $control->getValue() == '') {
                        return $error;
                    }
                }
            }
        }
        return TRUE;
    }

    public static function Validator_directorEditChangeRequired($control, $error, $params)
    {
        $value = $control->getValue();
        if (empty($value) && $control->owner[$params]->getValue() == 1) {
            return $error;
        }
        return TRUE;
    }

    public static function Validator_directorLLPEditChangeRequired($control, $error, $params)
    {
        $value = $control->getValue();
        if (!isset($value) && $control->owner[$params]->getValue() == 1) {
            return $error;
        }
        return TRUE;
    }

    public static function Validator_changeChecboxRequired($control, $error, $params)
    {
        foreach ($params as $key => $val) {
            if ($control->owner[$val]->getValue() == 1) {
                return TRUE;
            }
        }
        return $error;
    }

    public static function Validator_eeaRequired(Text $control, $error, $params)
    {
        if ($control->owner['eeaType']->getValue() !== NULL) {

            // EEA
            if ($control->owner['eeaType']->getValue() == 1) {
                if (($control->getName() == 'place_registered' || $control->getName() == 'registration_number') && $control->getValue() == '') {
                    return $error;
                }
                // non EEA
            } else {
                if (($control->getName() == 'place_registered' || $control->getName() == 'law_governed' || $control->getName() == 'legal_form') && $control->getValue() == '') {
                    return $error;
                }
            }
        }
        return TRUE;
    }

    public static function Validator_personDOB(DateSelect $control, $error, $minYears)
    {
        $value = $control->getValue();

        // check if dob is valid day
        $temp = explode('-', $value);
        if (checkdate($temp[1], $temp[2], $temp[0]) === FALSE) {
            return $error[0];
        }

        // check if director is older than 16 years
        $dob = date('Ymd', strtotime($value));
        $years = floor((date("Ymd") - $dob) / 10000);
        if ($years < $minYears) {
            return sprintf($error[1], $minYears);
        }
        return TRUE;
    }

    public static function Validator_securityEyeColour($control, $error, $params)
    {
        $value = $control->getValue();
        if (String::upper($value) === 'BLA') {
            return $error;
        }
        return TRUE;
    }

    public static function Validator_secretaryEditChangeRequired($control, $error, $params)
    {
        $value = $control->getValue();
        if (empty($value) && $control->owner[$params]->getValue() == 1) {
            return $error;
        }
        return TRUE;
    }

    public static function Validator_secretaryEditChangeEeaRequired($control, $error, $params)
    {
        $value = $control->getValue();
        if (empty($value) && $control->owner[$params]->getValue() == 1 && $control->owner['eeaType']->getValue() !== NULL) {
            // EEA
            if ($control->owner['eeaType']->getValue() == 1) {
                if (($control->getName() == 'place_registered' || $control->getName() == 'registration_number') && $control->getValue() == '') {
                    return $error;
                }
                // non EEA
            } else {
                if (($control->getName() == 'place_registered' || $control->getName() == 'law_governed' || $control->getName() == 'legal_form') && $control->getValue() == '') {
                    return $error;
                }
            }
        }
        return TRUE;
    }

    public static function Validator_serviceAddress($control, $error, Company $company)
    {

        $postcode = strtoupper($control->owner['postcode']->getValue());
        $postcode = str_replace(' ', '', $postcode);

        if (strstr($postcode, 'EC1V4PW')) {
            $registeredOfficeId = $company->getRegisteredOfficeId();
            if (empty($registeredOfficeId)) {
                return $error;
            }
        }
        return TRUE;
    }

    public static function Validator_requiredServiceAddress($control, $error, $params)
    {
        $value = $control->getValue();
        if ($params == 'add') {
            if ($control->owner['residentialAddress']->getValue() == 1 && empty($value)) {
                return $error;
            }
        } else {
            if (empty($value)) {
                return $error;
            }
        }
        return TRUE;
    }

    public static function Validator_officeRequired($control, $error, $param)
    {
        $value = $control->getValue();
        if (isset($control->owner['ourRegisteredOffice']) && $control->owner['ourRegisteredOffice']->getValue() == 1) {
            return TRUE;
        } else {
            if (empty($value)) {
                return $error;
            }
        }
        return TRUE;
    }

    public static function Validator_futureDateDatePicker(DatePicker $control, $error, $params)
    {
        $value = $control->getValue();
        $userDate = Date::createFromFormat('d-m-Y', $value);
        if ($userDate > new Date('now')) {
            return $error;
        }
        return TRUE;
    }

    public static function Validator_futureDate(DatePicker $control, $error, $params)
    {
        $value = $control->getValue();
        if (strtotime($value) > strtotime(date('d-m-Y'))) {
            return $error;
        }
        return TRUE;
    }

    public static function Validator_futureDateDateSelect(DateSelect $control, $error, $params)
    {
        $value = $control->getValue();
        if (strtotime($value) <= strtotime(date('Y-m-d'))) {
            return $error;
        }
        return TRUE;
    }

    /**
     * @param IControl $control
     * @param string|NULL $error
     * @return bool|string
     */
    public static function Validator_isValidChString($control, $error = NULL)
    {
        $name = $control->getValue();
        if (empty($name)) {
            return TRUE;
        } else {
            return self::isChValidString($name, $error);
        }
    }

    /**
     * Allow only CH accepted strings
     *
     * @param string $name
     * @param string|NULL $error
     * @return bool|string
     */
    private static function isChValidString($name, $error = NULL)
    {
        if (!isset(FApplication::$config['companies_house']['character_blacklist'])) {
            throw ConfigurationException::missingConfigParameter('companies_house.character_blacklist');
        }

        $blacklist = FApplication::$config['companies_house']['character_blacklist'];

        if (!preg_match($blacklist, $name)) {
            return TRUE;
        }
        if (empty($error)) {
            preg_match_all($blacklist, $name, $invalidCharacters);
            $invalidCharacters = implode('', array_unique($invalidCharacters[0]));

            return 'Invalid characters (' . $invalidCharacters . '). Companies House does not allow these characters.';
        } else {
            return $error;
        }
    }
}
