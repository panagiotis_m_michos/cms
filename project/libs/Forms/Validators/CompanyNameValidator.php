<?php

class CompanyNameValidator
{

    public static function isValid($control, $error = NULL)
    {
        $name = $control->getValue();
        if (empty($name)) {
            return TRUE;
        } else {
            return self::checkName($name, $error);
        }
    }

    /**
     * @param string $name
     * @param string $error
     * @return bool|string
     */
    public static function checkName($name, $error = NULL)
    {
        // company name regular expresion
        // letters, digits, space, -,.:;&@$'"?!/\()[]{}<> -first word up to 3 characters
        // letters, digits, space, -,.:;&@$'"?!/\()[]{}<>*=#%+ -next words up to 157
        if (preg_match("/^[-,.:;a-zA-Z0-9\/&$\\\@£*?'\"******?!{}<>()\[\] ]{3}[-,.:;a-zA-Z0-9\/&$\\\@£*?'\"******?!{}<>()\[\]*=#%+ ]{0,157}+$/", $name)) {
            return TRUE;
        }

        if ($error) {
            return $error;
        } elseif ((empty($name) || mb_strlen($name) > 160)) {
            return 'Company name must be between 1 and 160 characters long.';
        } else {
            return 'Company name is using invalid characters (' . preg_replace("/[\x20-\x7F£]+/u", '  ', $name) . ') Please only use characters found on an English keyboard';
        }
    }
}
