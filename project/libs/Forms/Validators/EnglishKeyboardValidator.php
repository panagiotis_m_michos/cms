<?php

class EnglishKeyboardValidator
{

    public static function isValid($control, $error = NULL)
    {
        $name = $control->getValue();
        if (empty($name)) {
            return TRUE;
        } else {
            return self::check($name, $error);
        }
    }

    public static function check($name, $error = NULL)
    {
        // all symbols on english keyboard
        // http://www.codetable.net/unicodecharacters?page=1

        if (preg_match('/^[\x20-\x7F£]*$/u', $name)) {
            return TRUE;
        }
        if (empty($error)) {
            return 'Invalid characters (' . preg_replace("/[\x20-\x7F£]+/u", '  ', $name) . ') Please only use characters found on an English keyboard';
        } else {
            return $error;
        }
    }

}