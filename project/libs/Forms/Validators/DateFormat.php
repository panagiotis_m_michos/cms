<?php

namespace Forms\Validators;

use Utils\Date;
use Validator;

class DateFormat extends Validator
{
    /**
     * @var string
     */
    private $format = 'd-m-Y';

    /**
     * @param string $value
     * @return boolean
     */
    public function isValid($value) 
    {
        $format = !empty($this->params[0]) ? $this->params[0] : $this->format;
        $date = is_string($value) ? Date::createFromFormat($format, $value) : NULL;
        return $date && $date->format($format) === $value ? TRUE : FALSE;
    }
}
