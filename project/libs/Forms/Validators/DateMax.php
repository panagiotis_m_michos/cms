<?php

namespace Forms\Validators;

use Utils\Date;

class DateMax extends Date_Abstract
{
    /**
     * @param string $value
     * @return boolean
     */
    public function isValid($value) 
    {
        if (!$this->date) {
            return TRUE;
        }
        $providedDate = is_string($value) ? Date::createFromFormat($this->format, $value) : NULL;
        return $providedDate && $providedDate <= $this->date ? TRUE : FALSE;
    }
}
