<?php

namespace Forms\Validators;

use Utils\Date;
use Validator;
use InvalidArgumentException;
use Utils\Exceptions\InvalidDateException;

abstract class Date_Abstract extends Validator
{
    /**
     * @var string
     */
    protected $format = 'd-m-Y';

    /**
     * @var Date
     */
    protected $date;

    /**
     * @param string $err_msg
     * @param array $param ( 0 => Date|NULL|string, 1 => string)
     * @param mixed $owner
     * @param mixed $control
     * @throws InvalidArgumentException
     * @throws InvalidDateException
     */
    public function __construct($err_msg, $params = array(), $owner, $control)
    {
        parent::__construct($err_msg, $params, $owner, $control);
        if (!array_key_exists(0, $this->params)) {
            throw new InvalidArgumentException('Please provide Date object or string date format');
        }

        //since we can not change constructor make sure its valid
        if (!empty($this->params[1])) {
            $this->format = $params[1];
        }
        if (!empty($this->params[0])) {
            if (is_string($this->params[0])) {
                $this->date = new Date($this->params[0]);
            } elseif ($this->params[0] instanceof Date) {
                $this->date = $this->params[0];
            } else {
                throw new InvalidArgumentException('Invalid date format provided');
            }
            $this->err_msg = sprintf($this->err_msg, $this->date->format($this->format));
        }
    }
}
