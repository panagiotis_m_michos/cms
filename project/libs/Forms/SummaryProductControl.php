<?php

use BootstrapModule\Ext\Ui\UiExt_Abstract;
use Exceptions\Business\CompanyImportException;
use Nette\Utils\Strings;
use Services\CompanyService;
use Services\CustomerService;
use UiHelper\ViewHelper;

class SummaryProductControl extends FControl
{

    const EXISTING = 'existing';
    const IMPORT = 'import';

    /**
     * @var array
     */
    protected $options = array();

    /**
     * @var array
     */
    protected $value = array();

    /**
     * @var array
     */
    protected $rows = array();

    /**
     * @var BasketProduct
     */
    private $product;

    /**
     * @param BasketProduct $product
     * @return $this
     */
    public function setProduct(BasketProduct $product)
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @param array $options
     * @return $this
     */
    public function setOptions($options)
    {
        $this->options = (array) $options;
        return $this;
    }

    /**
     * @param array $rows
     * @return $this
     */
    public function setRows($rows)
    {
        $this->rows = (array) $rows;
        return $this;
    }

    /**
     * @param mixed $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return array
     */
    public function getValue()
    {
        $return = array(
            'radio' => isset($this->value['radio']) ? $this->value['radio'] : NULL,
            'select' => isset($this->value['select']) ? $this->value['select'] : NULL,
            'text' => isset($this->value['text']) ? $this->value['text'] : NULL,
        );
        return $return;
    }

    /**
     * @return Html
     */
    public function getControl()
    {
        $serviceContainer = Html::el('div');
        if ($this->product->requiredIncorporatedCompanyNumber) {
            $serviceContainer->class('summary-service-incorporated');
        } else {
            $serviceContainer->class('summary-service-all');
        }

        $radioDiv = Html::el('div', array('class' => 'company-radio-container'));

        $radioOptions = array(
            self::EXISTING => 'Select an existing company <span class="company-radio-description">(select company from list below)</span>',
        );

        if (!$this->product->requiredIncorporatedCompanyNumber && !$this->product->onlyOurCompanies) {
            $radioOptions += array(
                self::IMPORT => 'Add new company <span class="company-radio-description">(if the company is not on the list)</span>',
            );
        }

        // radio button group
        $actionRadio = FForm::getElement('radio', $this->getGroupName() . '[radio]');
        $actionRadio->setOptions($radioOptions)
            ->class('company-radio')
            ->setValue(isset($this->value['radio']) ? $this->value['radio'] : self::EXISTING);

        $radioDiv->add($actionRadio->getControl());
        $serviceContainer->add($radioDiv);

        // company select dropdown
        $existingDiv = Html::el('div', array('class' => 'company-existing-container'));

        $existingDiv->add(
            Html::el('div')->class('company-existing-header')
                ->setText('Select an existing company:')
        );
        $companySelect = FForm::getElement('select', $this->getGroupName() . '[select]', 'Select an existing company:');
        $companySelect->id('company-number-select_' . $this->getName())
            ->class('company-number-select')
            ->setOptions($this->options)
            ->setFirstOption('--- Select a company ---')
            ->style('width:200px;')
            ->setValue(isset($this->value['select']) ? $this->value['select'] : NULL);

        $existingDiv->add($companySelect->getControl());
        $existingDiv->add(Html::el('span')->class('company-number-select-status'));

        $serviceContainer->add($existingDiv);

        // company import container
        $importDiv = Html::el('div')->class('company-import-container');
        $importDiv->add(
            Html::el('div')->class('company-import-header')
                ->setText('Company number:')
        );

        $importTextDiv = Html::el('div')->class('company-import-text');
        $importEditDiv = Html::el('div')->class('company-import-edit');
        $importErrorDiv = Html::el('div')->class('company-import-error');

        // company import text field
        $companyText = FForm::getElement('text', $this->getGroupName() . '[text]');
        $companyText->id('company-number-text_' . $this->getName())
            ->class('company-number-text')
            ->setValue(isset($this->value['text']) ? $this->value['text'] : NULL);
        $importTextDiv->add($companyText->getControl());

        // @todo: inject
        /** @var ViewHelper $viewHelper */
        $viewHelper = Registry::getService(UiExt_Abstract::UI_VIEW_HELPER);
        $button = $viewHelper->render(
            array(
                'name' => 'button',
                'loading' => 'expand-right',
                'text' => 'Import company',
                'idAttr' => 'import_' . $this->getName(),
                'nameAttr' => 'import_' . $this->getName(),
                'classAttr' => 'import-company',
            )
        );
        $importTextDiv->add($button);

        // forgotten number link
        $importTextDiv->add(
            Html::el('a')
                ->setText('Forgot the company number?')
                ->href('http://wck2.companieshouse.gov.uk/')
                ->target('_blank')
                ->class('forgot-link')
        );

        // company import info box
        $info = Html::el('div')->class('company-import-info');
        $companyName = Html::el('span')->class('company-name');
        $companyEdit = Html::el('span')->class('company-edit');

        $editSubmit = FForm::getElement('submit', 'edit_' . $this->getName());
        $editSubmit->setValue('Edit')
            ->class('edit-import linkButton')
            ->addAtrib('data-action-text', 'Processing...');

        $companyEdit->add($editSubmit->getControl());

        // if Import company button has been pressed and company imported
        if ($this->product->isLinkedToNewImportedCompany()) {
            $companyName->setText($this->product->companyName);
            $serviceContainer->class .= ' summary-imported';
        } elseif ($this->product->isLinkedToExistingImportedCompany()) {
            /** @var CompanyService $companyService */
            $companyService = Registry::$container->get(DiLocator::SERVICE_COMPANY);
            /** @var CustomerService $customerService */
            $customerService = Registry::$container->get(DiLocator::SERVICE_CUSTOMER);
            $signedCustomer = Customer::getSignedIn();
            $customer = $customerService->getCustomerById($signedCustomer->getId());

            $importedCompany = $companyService->getCompanyByCompanyNumber($this->product->companyNumber);
            if ($importedCompany && $companyService->doesCompanyBelongToCustomer($importedCompany, $customer)) {
                $companyName->setText($importedCompany->getCompanyName());
            }
            $serviceContainer->class .= ' summary-imported';
        }

        $info->add($companyName);
        $info->add($companyEdit);

        $importEditDiv->add($info);

        $importDiv->add($importTextDiv);
        $importDiv->add($importEditDiv);
        $importDiv->add($importErrorDiv);
        $serviceContainer->add($importDiv);

        return $serviceContainer;
    }

    /**
     * @param SummaryProductControl $control
     * @param array $error
     * @param array $params
     * @return bool
     */
    public static function Validator_summaryReq(SummaryProductControl $control, array $error, array $params)
    {
        if ($control->owner->isSubmitedBy('addVoucher') || $control->owner->isSubmitedBy('removeVoucher')) {
            return TRUE;
        }

        /** @var CompanyService $companyService */
        $companyService = Registry::$container->get(DiLocator::SERVICE_COMPANY);
        /** @var CustomerService $customerService */
        $customerService = Registry::$container->get(DiLocator::SERVICE_CUSTOMER);
        $signedCustomer = Customer::getSignedIn();
        $customer = $customerService->getCustomerById($signedCustomer->getId());

        /** @var BasketProduct[] $basket */
        $basket = $params[0];
        $val = $control->getValue();
        $companyNumber = $val['text'];

        // validate only product which was imported
        foreach ($basket as $key => $item) {
            if ($control->owner->isSubmitedBy('import_' . $key) && $control->getName() == $key) {
                if (empty($val['text'])) {
                    return $error[1];
                } else {
                    try {
                        if (!preg_match('/^([a-z]{2}|\d{1,2})\d{6}$/i', $companyNumber)) {
                            throw CompanyImportException::invalidCompanyNumber();
                        }

                        $companyNumber = Strings::padLeft($companyNumber, 8, '0');
                        $company = $companyService->getCompanyByCompanyNumber($companyNumber);

                        if ($company && !$company->getCustomer()->isEqual($customer)) {
                            // company belongs to different customer
                            throw CompanyImportException::companyDoesNotBelongToCustomer($company);
                        }

                    } catch (CompanyImportException $e) {
                        return $e->getMessage();
                    } catch (Exception $e) {
                        return $e->getMessage();
                    }

                }
            }
        }

        foreach ($basket as $key => $item) {
            if ($control->owner->isSubmitedBy('payment') && $control->getName() == $key) {
                if ($val['radio'] == self::IMPORT && !$item->isLinkedToImportedCompany()) {
                    return $error[1];
                }
            }
        }

        if ($control->owner->isSubmitedBy('payment')) {
            if ($val['radio'] == self::EXISTING) {
                if ($val['select'] == '?') {
                    return $error[0];
                } elseif (!preg_match('/^imported_/', $val['select'])) {
                    $company = $companyService->getCompanyById($val['select']);
                    if (!$company || !$companyService->doesCompanyBelongToCustomer($company, $customer)) {
                        return $error[0];
                    }
                }
            }
        }

        return TRUE;
    }

    /**
     * @return string
     */
    private function getGroupName()
    {
        return $this->getGroup() . '[' . $this->getName() . ']';
    }
}
