<?php

class SplitTestRadio extends Radio
{
	protected $options = Array();
	
	protected $disabled_options = Array();
	
	/**
	 * @param misc $$options
	 */
	public function setDisabledOptions($options)
	{
		$this->disabled_options = (array)$options;
		return $this;
	}
	
	/**
	 * @param int $key
	 * @return string $html
	 */
	public function getControl($key=Null)
	{
            $RadioDivs = '';
		$html = '';
		if (!is_Null($key)) {
			$options = Array($key => $this->options[$key]);
		} else {
			$options = $this->options;
		}
		
		foreach ($options as $key=>$val) {
			
			// controls
			$control = Html::el('input')
				->type('radio')
				->name($this->getName())
				->value($key)
				->id($this->getName() . $key);
				
			// this control should be set if	
			if ((string)$this->value == $key) {
				$control->checked('checked');
			}
			
			// disabled options
			if (in_Array($key,$this->disabled_options)) {
				$control->disabled('disabled');
			}
				
			self::applyAttribs($control);
		    
		    // labels
		    $label = Html::el('label')
		   		->setHtml($val)
		    	->for($this->getName() . $key);
		    
		    $html .= $control->render() . $label->render() . HTML::el('br')->render();
                    $RadioDivs .= Html::el('div')->class('divtest h45')->setHtml(
                            Html::el('span')->class('spaninput')->setHtml($control->render()) . 
                            Html::el('span')->class('spanlabel')->setHtml($label->render()));
		}
                
                $mainDiv = Html::el('div')->class('mnaindiv')->setHtml($RadioDivs);
		return $mainDiv;
	}
	
	/**
	 * @param 
	 */
	public function setOptions($options)
	{
		$this->options = (array)$options;
		return $this;
	}
	
	/**
	 * @return string $html
	 */
	public function getLabel()
	{
		if (is_Null($this->label)) {
			return NULL;
		}
		return $this->label;
	}
}
?>
