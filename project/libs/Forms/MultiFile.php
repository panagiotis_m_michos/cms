<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @version    MultiFile.php 2010-09-20 divak@gmail.com
 */

class MultiFile extends FControl
{
	/**
	 * @var int
	 */
	private $counter = 1;
	
	/**
	 * @var int
	 */
	private $maximumRows = 10;
	
	/**
	 * @param string $name
	 * @param string $label
	 * @return void
	 */
	public function __construct($name, $label, &$owner)
	{
		parent::__construct($name, $label, $owner);
		$this->owner->hasEnctype = TRUE;
        //pr($this);exit;
	}
	
	/**
	 * @param array $value
	 * @return array<HttpFileUpload>
	 */
	public function setValue($value)
	{//pr($value);exit;
		if (isset($value['counter'])) {
			$this->value = array();
			$this->counter = $this->value['counter'] = $value['counter'];
			if ($this->counter < 1) $this->counter = 1;
			for ($i = 1; $i <= $this->counter; $i++) {
				// check for maximum rows
				if ($this->maximumRows != 0 && $i > $this->maximumRows) {
					$this->counter = $this->maximumRows;
					break;
				}
			}
		}
		return $this;
	}
	
	/**
	 * @param int $maximumRows
	 * @return TextTextCombo
	 */
	public function setMaximumRows($maximumRows)
	{
		$this->maximumRows = (int) $maximumRows;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getValue()
	{
		$files2 = $_FILES;
		$values = array();
		if (!empty($files2)) {
            $i = 1;
            foreach ($files2 as $key => $value2) {
                foreach ($value2 as $key3 => $value3) {
                    foreach ($value3 as $key4 => $value4) {
                        $files[$this->getName()][$key3]['rows'][$i++] =  array('file'=>$value4);
                    }$i = 1;
                }
             }
            //pr($files);
			for ($i = 1; $i <= $this->counter; $i++) {
				if (isset($files[$this->getName()]['name']['rows'][$i]['file'])) {

                $value = array(
						'name' => $files[$this->getName()]['name']['rows'][$i]['file'],
						'type' => $files[$this->getName()]['type']['rows'][$i]['file'],
						'tmp_name' => $files[$this->getName()]['tmp_name']['rows'][$i]['file'],
						'error' => $files[$this->getName()]['error']['rows'][$i]['file'],
						'size' => $files[$this->getName()]['size']['rows'][$i]['file']
					);
					if ($value['error'] === UPLOAD_ERR_OK) {
						$values[$i] = new HttpUploadedFile($value);
					} else {
						$values[$i] = NULL;
//						$values[$i] = new HttpUploadedFile();
					}
				}
			}
        }
        
		return $values;
	}

	/**
	 * @return Html
	 */
	public function getControl()
	{
		$el = Html::el();
		$table = $el->create('table')->border(0);
		$this->addControlLabels($table);
		$this->addControlControls($table);
		$this->addControlLinks($table);
		$this->addControlCounter($el);
		$this->addControlHandlingJavascript($el);
		return $el;
	}
	
	/**
	 * Provides adding labels
	 *
	 * @param Html $table
	 */
	private function addControlLabels(Html $table)
	{
		// labels
		$tr = $table->create('tr');
		$tr->create('td');
//		$tr->create('td')->setText($this->label);
	}
	
	/**
	 * Provides adding controls
	 *
	 * @param Html $table
	 */
	private function addControlControls(Html $table)
	{
		$form = $this->owner;
		for ($i=1; $i<=$this->counter; $i++) {

			// row
			$className = sprintf("%s_row_%s", $this->getName(), $i);
			$tr = $table->create('tr')->class($className);
			$tr->create('td')->setText("$i.")->class('row_num')->valign('top');

			// text
			$td = $tr->create('td')->valign('top');
			$name = sprintf("%s[rows_%s_file]", $this->getName(), $i);
			$text1 = $form->add('InputFile', $name, NULL)->class('file');
			if (isset($this->value['rows'][$i]['file'])) $text1->setValue($this->value['rows'][$i]['file']);
			$td->add($text1->getControl());
		}
	}
	
	/**
	 * Provides adding counter
	 *
	 * @param Html $table
	 */
	private function addControlCounter(Html $el)
	{
		// counter
		$name = sprintf("%s[counter]", $this->getName());
		$counter = $this->owner->getElement('Hidden', $name);
		$counter->setValue($this->counter);
		$el->add($counter->getControl());
	}
	
	/**
	 * Provides adding links add/remove
	 *
	 * @param Html $table
	 */
	private function addControlLinks(Html $table)
	{
		$tr = $table->create('tr');
		$tr->create('td');
		
		// link to add
		$id = sprintf("%s_add", $this->getName());
		$add = Html::el('a')
			->setText('Add Another')
			->href('#')
			->id($id)
			->class('ui-widget-content ui-corner-all');
		
		// link to remove
		$id = sprintf("%s_remove", $this->getName());
		$remove = Html::el('a')
			->setText('Remove')
			->href('#')
			->id($id)
			->class('ui-widget-content ui-corner-all');
			
		$tr->create('td')->add($add)->add($remove)->style('padding: 5px 0 0 0;');
	}
	
	/**
	 * Provides adding handling javascript
	 *
	 * @param Html $el
	 */
	private function addControlHandlingJavascript(Html $el)
	{
		$name = $this->getName();
		$nameSelector = str_replace(array('[', ']'), array('\\\[', '\\\]'), $name);
		
		$maximumRows = $this->maximumRows;
		
		$el->create('script')
			->type('text/javascript')
			->setHtml("
			
				/**
				 * add link
				 */
				$('#".$nameSelector."_add').click(function() {
					
					// get counter
					counter = $('#".$nameSelector."\\\[counter\\\]');
					var counterVal = parseInt(counter.val());
					var nextCounterVal = counterVal + 1;
					
					// check for maximum rows
					if ($maximumRows !=0 && nextCounterVal > $maximumRows) {
						alert('Maximum rows is $maximumRows');
						return false;
					}
                    
					// get clone of last row		
					clone = $('.".$nameSelector."_row_' + counterVal)
						.clone()
						.attr('class', '".$name."_row_' + nextCounterVal);
						
					// change counter for row
					$('td.row_num', clone).html(nextCounterVal + '.');
					
					// change id for select
					$('input.file', clone)
						.attr('id', '".$name."[row_' + nextCounterVal + '_file]')
						.attr('name', '".$name."[row_' + nextCounterVal + '_file]')
						.val('');
						
					// insert after last row
					clone.insertAfter($('.".$nameSelector."_row_' + counterVal));
					
					// increment counter
					counter.val(nextCounterVal);
					return false;
				});
				
				/**
				 * remove link
				 */
				$('#".$nameSelector."_remove').click(function() {
				
					// get counter
					counter = $('#".$nameSelector."\\\[counter\\\]');
					var counterVal = parseInt(counter.val());
					var prevCounterVal = counterVal - 1;
					
					// at least one row has to remain
					if (prevCounterVal < 1) {
						alert('At least one row is required');
						return false;
					}
					
					// remove row
					$('.".$nameSelector."_row_' + counterVal).remove();
					
					// decrement counter
					counter.val(prevCounterVal);
					return false;
				});
			");
	}
}