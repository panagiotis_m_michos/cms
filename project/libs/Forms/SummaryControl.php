<?php

class SummaryControl extends FControl
{
    /** @var array */
    protected $options = array();

    /** @var array */
    protected $value = array();

    /** @var array */
    protected $rows = array();

    /** @var boolean */
    protected $onlySelect = FALSE;

    /**
     * @param array $options
     * @return $this
     */
    public function setOptions($options)
    {
        $this->options = (array) $options;
        return $this;
    }

    /**
     * @param bool $onlySelect
     * @return $this
     */
    public function setOnlySelect($onlySelect)
    {
        $this->onlySelect = (bool) $onlySelect;
        return $this;
    }

    /**
     * @param array $rows
     * @return $this
     */
    public function setRows($rows)
    {
        $this->rows = (array) $rows;
        return $this;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return array
     */
    public function getValue()
    {
        $return = array();
        foreach ($this->value as $key => $val) {
            $return[$key] = array(
                'select' => isset($val['select']) ? $val['select'] : NULL,
                'text' => isset($val['text']) ? $val['text'] : NULL
            );
        }
        return $return;
    }

    public function getControl()
    {
        $table = Html::el('table')->border(0);
        $table->create('col')->width(190);
        $table->create('col')->width(260);
        $table->create('col')->width(60);

        $tr = $table->create('tr');
        $tr->create('th');
        $tr->create('th')->setText('Company Name');

        if ($this->onlySelect == FALSE) {
            $tr->create('th')->setText('Company No');
        }

        foreach ($this->rows as $key => $row) {
            $tr = $table->create('tr');
            $tr->create('td')->setText($row);

            // select
            $control = FForm::getElement('select', $this->getName() . '[' . $key . '][select]')
                ->setOptions($this->options)
                ->setFirstOption('--- other ---')->style('width:200px;');
            if ($this->onlySelect == TRUE) {
                $control->style('width:260px;');
            }
            if (isSet($this->value[$key]['select'])) {
                $control->setValue($this->value[$key]['select']);
            }
            self::applyAttribs($control);
            $tr->create('td')->add($control->getControl());

            // text
            if ($this->onlySelect == FALSE) {
                $control = FForm::getElement('text', $this->getName() . '[' . $key . '][text]');
                if (isSet($this->value[$key]['text'])) {
                    $control->setValue($this->value[$key]['text']);
                }
                self::applyAttribs($control);
                $tr->create('td')->add($control->getControl());
            }
        }
        return $table;
    }

    /**
     * @param SummaryControl $control
     * @param array $error
     * @return bool
     */
    public static function Validator_summaryReq(SummaryControl $control, array $error)
    {
        if ($control->owner->isSubmitedBy('addVoucher') || $control->owner->isSubmitedBy('removeVoucher')) {
            return TRUE;
        }

        $data = $control->getValue();
        foreach ($data as $key => $val) {
            if ($val['select'] == '?' && $val['text'] == NULL) {
                return $error[0];
            } elseif ($val['select'] == '?' && preg_match('/^([a-z]{2}|\d{1,2})\d{6}$/i', $val['text']) == FALSE) {
                return $error[1];
            }
        }
        return TRUE;
    }
}
