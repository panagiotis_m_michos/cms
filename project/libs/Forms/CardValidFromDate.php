<?php

class CardValidFromDate extends FControl
{
    /** @var int */
    protected $endYear = 15;

    /**
     * @param mixed $value
     * @return provides fluent interfaces
     */
    public function setValue($value)
    {
        if (empty($value)) {
            $this->value = array('y' => date('Y'), 'm' => date('m')); 
        } elseif (is_array($value)) {
            $this->value = $value;
        } else {
            // check date format
            if (!preg_match('/^[0-9]{4}-[0-9]{2}$/', $value)) {
                throw new Exception('CardDate::setValue(): Wrong date format `'.$value.'` for '.$this->name.', should be in form `YYYY-MM`');
            }
            $time = strtotime($value);
            $this->value = array('y' => date('Y', $time), 'm' => date('m', $time));
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        $value = NULL;
        if (isset($this->value['y'], $this->value['m']) && $this->value['y'] != '?' && $this->value['m'] != '?') {
            $value = $this->value['y'] . '-' . $this->value['m'];
        }
        return $value;
    }

    /**
     * @return string $html
     */
    public function getControl()
    {
        // options
        $options = $this->getOptions();

        // month
        $control = FForm::getElement('select', $this->getName() . '[m]')->setOptions($options['m'])->setFirstOption('---');
        if (isSet($this->value['m'])) $control->setValue($this->value['m']);
        self::applyAttribs($control);
        $html = $control->getControl();

        // year
        $control = FForm::getElement('select', $this->getName() . '[y]')->setOptions($options['y'])->setFirstOption('---');
        if (isSet($this->value['y'])) $control->setValue($this->value['y']);
        self::applyAttribs($control);
        $html .= $control->getControl();

        return $html;
    }


    /**
     * @return array
     */
    protected function getOptions()
    {
        //		@todo find out why this code is necessary
        //		// months
        //		for ($i=1; $i<=12; $i++) {
        //			
        //			$options['m'][$i] = date('m', mktime(0, 0, 0, date($i)  , date("d"), date("Y")));
        //			if($i==2)
        //			{
        //				$options['m'][$i] = '02';			
        //			}
        //		}
        $options['m'] = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
        // years
        for ($i = 2000; $i <= 2000 + $this->endYear; $i++) {
            $options['y'][$i] = $i;
        }
        return $options;
    }
}
