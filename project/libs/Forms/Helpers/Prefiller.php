<?php

class Prefiller
{
    const TYPE_PERSON = 'PERSON';
    const TYPE_CORPORATE = 'CORPORATE';
    
    /**
     * @var Company 
     */
    private $company;
    
    /**
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        $this->company = $company;
    }
    
    /**
     * Returns addreses for select and javascript used for prefill address
     *
     * @return array
     */
    public function getPrefillAdresses()
    {
        try {
            $addresses = $this->company->getAddresses();
        } catch (Exception $e) {
            $addresses = [];
        }

        $addressesFields = [];
        $selectValues = [];
        foreach ($addresses as $key => $address) {
            $fields = $address->getFields();
            $addressesFields[$key] = $fields;
            $selectValues[$key] = $fields['postcode'] . ', ' . $fields['premise'] . ' ' . $fields['street'];
        }

        return [
            'select' => $selectValues,
            'js' => json_encode($addressesFields, JSON_FORCE_OBJECT),
        ];
    }

    /**
     * Returns person details for select and javascript used for prefill person data
     *
     * @param string $type
     * @return array
     */
    public function getPrefillOfficers($type = self::TYPE_PERSON)
    {
        $officersFields = [];
        $selectValues = [];

        if ($type == self::TYPE_PERSON) {
            $persons = $this->company->getPersons();
            foreach ($persons as $key => $person) {
                $fields = $person->getFields();
                $officersFields[$key] = $fields;
                $selectValues[$key] = $fields['surname'] . ' ' . $fields['forename'];
            }
        } elseif ($type == self::TYPE_CORPORATE) {
            $persons = $this->company->getCorporates();
            foreach ($persons as $key => $person) {
                $fields = $person->getFields();
                $officersFields[$key] = $fields;
                $selectValues[$key] = $fields['corporate_name'];
            }
        }

        return [
            'select' => $selectValues,
            'js' => json_encode($officersFields, JSON_FORCE_OBJECT),
        ];
    }

}
