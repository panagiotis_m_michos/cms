<?php

class AnnualReturnOfficerForm extends FControl
{
    const TYPE_EEA = 'EEA';
    const TYPE_NON_EEA = 'NonEEA';

    /** @var array */
    static public $types = array(
        self::TYPE_EEA => 'EEA',
        self::TYPE_NON_EEA => 'Non EEA'
    );
    /** @var array */
    public $validators = array(
        array(
            array('AnnualReturnOfficerForm', 'Validator'),
            // errors
            array(
                'Please provide EEA type!',
                'Please provide Country registered!',
                'Please provide Registration Number!',
                'Please provide Governing Law!',
                'Please provide Legal Form!',
                'Please provide Contry',
                'Please provide Country of Resindence. Max 50 characters.',
                'Please provide Premise.',
                'All fields are required.'
            ),
            // params
            array()
        )
    );
    /** @var string */
    public $group = 'officers';
    /** @var array */
    public $officer = array();

    /**
     * @param array $officer
     */
    public function setOfficer(array $officer)
    {
        $this->officer = $officer;
        $this->validators[0][2]['officer'] = $officer;
        return $this;
    }

    /**
     * Returns html form control
     *
     * @return string
     */
    public function getControl()
    {
        $el = Html::el();
        $table = $el->create('table');
        $table->create('col')->width("170px");

        // eea
        if (array_key_exists('identification_type', $this->officer)) {

            // type
            $tr = $table->create('tr');
            $type = $this->getControlComponent('identification_type');
            $tr->create('th')->add($type->getLabel());
            $tr->create('td')->add($type->getControl());


            // country registered
            $tr = $table->create('tr');
            $country = $this->getControlComponent('place_registered');
            $tr->create('th')->add($country->getLabel());
            $tr->create('td')->add($country->getControl());

            // registration number
            $tr = $table->create('tr');
            $number = $this->getControlComponent('registration_number');
            $tr->create('th')->add($number->getLabel());
            $tr->create('td')->add($number->getControl());

            // governing law
            $tr = $table->create('tr');
            $law = $this->getControlComponent('law_governed');
            $tr->create('th')->add($law->getLabel());
            $tr->create('td')->add($law->getControl());

            // legal form
            $tr = $table->create('tr');
            $legal = $this->getControlComponent('legal_form');
            $tr->create('th')->add($legal->getLabel());
            $tr->create('td')->add($legal->getControl());
        }

        // premise
        if (array_key_exists('premise', $this->officer)) {
            $tr = $table->create('tr');
            $legal = $this->getControlComponent('premise');
            $tr->create('th')->add($legal->getLabel());
            $tr->create('td')->add($legal->getControl());
        }

        // street
        if (array_key_exists('street', $this->officer)) {
            $tr = $table->create('tr');
            $legal = $this->getControlComponent('street');
            $tr->create('th')->add($legal->getLabel());
            $tr->create('td')->add($legal->getControl());
        }

        // thoroghfare
        if (array_key_exists('thoroughfare', $this->officer)) {
            $tr = $table->create('tr');
            $legal = $this->getControlComponent('thoroughfare');
            $tr->create('th')->add($legal->getLabel());
            $tr->create('td')->add($legal->getControl());
        }

        // post town
        if (array_key_exists('post_town', $this->officer)) {
            $tr = $table->create('tr');
            $legal = $this->getControlComponent('post_town');//->readonly();
            $tr->create('th')->add($legal->getLabel());
            $tr->create('td')->add($legal->getControl());
        }

        // country
        if (array_key_exists('country', $this->officer)) {
            $tr = $table->create('tr');
            $legal = $this->getControlComponent('country');
            $tr->create('th')->add($legal->getLabel());
            $tr->create('td')->add($legal->getControl());
        }

        // country of residence
        if (array_key_exists('country_of_residence', $this->officer)) {
            $tr = $table->create('tr');
            $legal = $this->getControlComponent('country_of_residence');
            $tr->create('th')->add($legal->getLabel());
            $tr->create('td')->add($legal->getControl());
        }

        // DesignatedInd
        if (array_key_exists('designated_ind', $this->officer)) {
            $tr = $table->create('tr');
            $legal = $this->getControlComponent('designated_ind');
            $tr->create('th')->add($legal->getLabel());
            $tr->create('td')->add($legal->getControl());
        }

        // toggle javascript
        $el->add($this->getToggleJavascript());
        return $el;
    }

    /**
     * Returns control component
     *
     * @param string $type
     * @return mixed
     */
    private function getControlComponent($type)
    {
        switch ($type) {
            case 'identification_type':
                $component = FForm::getElement('Radio', $this->getGroupName($type), 'Type')->setOptions(self::$types);
                break;
            case 'place_registered':
                $component = FForm::getElement('Text', $this->getGroupName($type), 'Country Registered *');
                break;
            case 'registration_number':
                $component = FForm::getElement('Text', $this->getGroupName($type), 'Registration Number *');
                break;
            case 'law_governed':
                $component = FForm::getElement('Text', $this->getGroupName($type), 'Governing Law *');
                break;
            case 'legal_form':
                $component = FForm::getElement('Text', $this->getGroupName($type), 'Legal Form *');
                break;
            case 'post_town':
                $component = FForm::getElement('Text', $this->getGroupName($type), 'Post town');
                break;
            case 'street':
                $component = FForm::getElement('Text', $this->getGroupName($type), 'Street *');
                break;
            case 'premise':
                $component = FForm::getElement('Text', $this->getGroupName($type), 'Premise *');
                break;
            case 'thoroughfare':
                $component = FForm::getElement('Text', $this->getGroupName($type), 'Thoroughfare');
                break;
            case 'country':
                $component = FForm::getElement('Select', $this->getGroupName($type), 'Country (Service Address) *')->setOptions(Address::$countries)->setFirstOption('--- Select ---');
                break;
            case 'country_of_residence':
                $component = FForm::getElement('Text', $this->getGroupName($type), 'Country of residence *');
                break;
            case 'designated_ind':
                $component = FForm::getElement('Radio', $this->getGroupName($type), 'Designated Member *')->setOptions(array(0=>'no',1=>'yes'));
                break;
            default:
                throw new Exception("There is no $type component for EEAForm");
                break;
        }

        $component->setValue($this->getGroupValue($type));
        self::applyAttribs($component);
        return $component;
    }

    /**
     * Returns toggle javascript
     *
     * @return string
     */
    private function getToggleJavascript()
    {
        $html = "
		<script type='text/javascript'>
		<!--
		function toggleOfficer{$this->getName()} () {


			if ($('#{$this->getGroupName('identification_type', TRUE)}EEA').is(':checked')) {

				$('#{$this->getGroupName('place_registered', TRUE)}').parents('tr:first').show();
				$('#{$this->getGroupName('registration_number', TRUE)}').parents('tr:first').show();
				$('#{$this->getGroupName('law_governed', TRUE)}').parents('tr:first').hide();
				$('#{$this->getGroupName('legal_form', TRUE)}').parents('tr:first').hide();

				$('label[for=\"{$this->getGroupName('registration_number', TRUE)}\"]').text('Registration Number *');

			} else if($('#{$this->getGroupName('identification_type', TRUE)}NonEEA').is(':checked')) {
				$('#{$this->getGroupName('place_registered', TRUE)}').parents('tr:first').show();
				$('#{$this->getGroupName('registration_number', TRUE)}').parents('tr:first').show();
				$('#{$this->getGroupName('law_governed', TRUE)}').parents('tr:first').show();
				$('#{$this->getGroupName('legal_form', TRUE)}').parents('tr:first').show();

				$('label[for=\"{$this->getGroupName('registration_number', TRUE)}\"]').text('Registration Number');

			} else {
				$('#{$this->getGroupName('place_registered', TRUE)}').parents('tr:first').hide();
				$('#{$this->getGroupName('registration_number', TRUE)}').parents('tr:first').hide();
				$('#{$this->getGroupName('law_governed', TRUE)}').parents('tr:first').hide();
				$('#{$this->getGroupName('legal_form', TRUE)}').parents('tr:first').hide();
			}
		}

		toggleOfficer{$this->getName()}();

		$(document).ready(function() {

			$(\"input[name='{$this->getGroupName('identification_type', TRUE)}']\").click(function() {
				toggleOfficer{$this->getName()}();
			});


		});
		//-->
		</script>";
        return $html;
    }

    /**
     * Returns value if any
     *
     * @param string $key
     * @return mixed
     */
    private function getGroupValue($key)
    {
        $values = $this->getValue();
        if (isset($values[$key])) {
            return $values[$key];
        }
        return NULL;
    }

    /**
     * Returns group name for each element
     *
     * @param string $key
     * @return string
     */
    private function getGroupName($key, $escape = FALSE)
    {
        $return = sprintf('%s[%s][%s]', $this->group, $this->getName(), $key);
        if ($escape === TRUE) {
            $return = str_replace(array('[', ']'), array('\\\[', '\\\]'), $return);
        }
        return $return;
    }

    /**
     * Returns label text
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Provides validate control
     *
     * @param EEAForm $control
     * @param array $error
     * @param array $params
     * @return mixed
     */
    static public function Validator(AnnualReturnOfficerForm $control, array $error, array $params)
    {
        //todo: psc - hackity hack to skip the officers validation when necessary
        if ($_POST['returnDate']) {
            $madeUpTo = DateTime::createFromFormat('d-m-Y', $_POST['returnDate']);
            $madeUpTo->setTime(0, 0, 1);

            if ($madeUpTo && $madeUpTo > new DateTime('2016-06-29 23:59:59')) {
                return TRUE;
            }
        }

        $values = $control->getValue();

        // eea
        if (array_key_exists('identification_type', $params['officer'])) {
            if (!isset($values['identification_type'])) {
                return $error[0];
            } elseif (($values['identification_type'] == self::TYPE_EEA || $values['identification_type'] == self::TYPE_NON_EEA) && empty($values['place_registered'])) {
                return $error[1];
            } elseif ($values['identification_type'] == self::TYPE_EEA && empty($values['registration_number'])) {
                return $error[2];
            } elseif ($values['identification_type'] == self::TYPE_NON_EEA && empty($values['law_governed'])) {
                return $error[3];
            } elseif ($values['identification_type'] == self::TYPE_NON_EEA && empty($values['legal_form'])) {
                return $error[4];
            }
        }



        // country
        if (array_key_exists('country', $params['officer']) && $values['country'] == '?') {
            return $error[8];
        }

        // country of residence
        if (array_key_exists('country_of_residence', $params['officer']) && !preg_match('#^[^\d]{1,50}$#i', $values['country_of_residence'])) {
            return $error[8];
        }

        // premise
        if (array_key_exists('premise', $params['officer']) && $values['premise'] == '') {
            return $error[8];
        }

        // street
        if (array_key_exists('street', $params['officer']) && $values['street'] == '') {
            return $error[8];
        }

        // designated_ind
        if (array_key_exists('designated_ind', $params['officer']) && $values['designated_ind'] == '') {
            return $error[8];
        }

        return TRUE;
    }

}
