<?php

use Utils\Date;

class CardValidFromDateNew extends CardValidFromDate
{
    /**
     * @param mixed $value
     * @return provides fluent interfaces
     */
    public function setValue($value)
    {
        if (empty($value)) {
            $this->value = array('y' => date('y'), 'm' => date('m')); 
        } else {
            $this->value = $value;
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        $value = NULL;
        if (isset($this->value['y'], $this->value['m']) && $this->value['y'] != '?' && $this->value['m'] != '?') {
            $value = $this->value['m'] . $this->value['y'];
        }
        return $value;
    }

    /**
     * @return array
     */
    protected function getOptions()
    {
        $options = array();
        for ($i = 1; $i <= 12; $i++) {
            $option = sprintf("%02s", $i);
            $options['m'][$option] = $option;
        }
        $end = new Date();
        $start = new Date('-' . $this->endYear . ' years');
        while ($start <= $end) {
            $options['y'][$start->format('y')] = $start->format('Y');
            $start->modify('+1 year');
        }
        return $options;
    }
}
