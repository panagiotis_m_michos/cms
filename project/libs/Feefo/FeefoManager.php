<?php

namespace Feefo;

use Entities\Feefo;
use Entities\Order;
use Exception;
use Exceptions\Business\CurlException;
use Exceptions\Business\FeefoException;
use Nette\Object;
use Services\FeefoService;

class FeefoManager extends Object
{
    /**
     * @var array
     */
    private $config;

    /**
     * @var FeefoService
     */
    private $feefoService;

    /**
     * @param array $config
     * @param FeefoService $feefoService
     */
    public function __construct($config, FeefoService $feefoService)
    {
        $this->config = $config;
        $this->feefoService = $feefoService;
    }

    /**
     * @param Feefo $feefo
     * @throws Exception
     * @return bool
     */
    public function orderNotify(Feefo $feefo)
    {
        $data = $this->buildParameters($feefo->getOrder());

        if ($data) {
            $response = $this->makeRequest($data);
            if ($response != 'Sale processed successfully.') {
                throw FeefoException::wrongResponseReceived($response);
            }
            $feefo->markAsSent();
            $this->feefoService->saveFeefo($feefo);
            return TRUE;
        }
        
        return FALSE;
    }

    /**
     * @param Order $order
     * @throws Exception
     * @return string|bool
     */
    private function buildParameters(Order $order)
    {
        $data = $order->getFeefoData();
        
        // if customer has no name entered, wait till next execution
        if (empty($data['name'])) {
            return FALSE;
        }

        if (empty($this->config['param'])) {
            throw FeefoException::configParameterMissing('param');
        }
        
        $output = array(
            'website=' . urlencode($this->config['param']['website']),
            'password=' . urlencode($this->config['param']['password']),
        );

        foreach ($data as $key => $value) {
            $output[] = urlencode($key) . '=' . urlencode($value);
        }

        return implode('&', $output);
    }

    /**
     * @param string $sendData
     * @return string
     * @throws Exception
     */
    private function makeRequest($sendData)
    {
        if (!function_exists('curl_setopt')) {
            throw CurlException::curlNotFound();
        }

        if (empty($this->config['url'])) {
            throw FeefoException::configParameterMissing('url');
        }

        $curlSession = curl_init();
        curl_setopt($curlSession, CURLOPT_URL, $this->config['url']);
        curl_setopt($curlSession, CURLOPT_HEADER, 0);
        curl_setopt($curlSession, CURLOPT_POST, 1);
        curl_setopt($curlSession, CURLOPT_POSTFIELDS, $sendData);
        curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlSession, CURLOPT_TIMEOUT, 30);
        $response = curl_exec($curlSession);

        $errno = curl_errno($curlSession);
        if ($errno) {
            throw CurlException::requestError($errno, curl_error($curlSession));
        } else {
            curl_close($curlSession);
        }

        return trim($response);
    }

}
