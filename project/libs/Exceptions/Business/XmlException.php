<?php

namespace Exceptions\Business;

class XmlException extends Business_Abstract
{
    /**
     * @return XmlException
     */
    public static function invalidFormat()
    {
        return new self("Unable to parse xml string");
    }

} 