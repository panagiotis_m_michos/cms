<?php

namespace Exceptions\Business;

class AutoRenewalException extends Business_Abstract
{
    /**
     * @return AutoRenewalException
     */
    public static function noActiveToken()
    {
        return new self("There is no active token set");
    }
}
