<?php

namespace Exceptions\Business;

use BasketProduct;
use Exception;

class BasketException extends Business_Abstract
{
    const ITEM_COULD_NOT_BE_ADDED = 1;

    /**
     * @param BasketProduct $item
     * @param Exception $previous
     * @return $this
     */
    public static function itemCouldNotBeAdded(BasketProduct $item, Exception $previous = NULL)
    {
        return new self(
            sprintf("Product '%s (#%s)' can't be added to basket!", $item->getLngTitle(), $item->getId()),
            self::ITEM_COULD_NOT_BE_ADDED,
            $previous
        );
    }
}
