<?php

namespace Exceptions\Business;

use Entities\Company;

class CompanyImportException extends Business_Abstract
{
    /**
     * @return CompanyImportException
     */
    public static function invalidCompanyNumber()
    {
        return new self(
            'Invalid company number. Please check the number and try again.
            If you are not sure about the number you can <a href="http://wck2.companieshouse.gov.uk/" target="_blank">check it on Companies House WebCHeck</a>.'
        );
    }

    /**
     * @param Company $company
     * @return CompanyImportException
     */
    public static function companyDoesNotBelongToCustomer(Company $company)
    {
        return new self(
            "The company {$company->getCompanyName()} is already on our systems and it is linked to a different account.
            If you want to transfer this company to your account please <a href='mailto:theteam@madesimplegroup.com'>contact us</a>."
        );
    }

    /**
     * @return CompanyImportException
     */
    public static function companyNotFound()
    {
        return new self(
            'No companies were found with this number. Please check the inserted number and try again.
            If you are not sure about the number you can <a href="http://wck2.companieshouse.gov.uk/" target="_blank">check it on Companies House WebCHeck</a>.'
        );
    }

    /**
     * @return CompanyImportException
     */
    public static function companiesHouseNotResponding()
    {
        return new self(
            "Sorry, we are having trouble importing your company from Companies House (they're not responding). Please try again in a minute."
        );
    }

    /**
     * @param Company[] $companies
     * @return CompanyImportException
     */
    public static function companiesAlreadyExist(array $companies)
    {
        $companyNames = array();
        foreach ($companies as $company) {
            $companyNames[] = $company->getCompanyName();
        }

        return new self('These companies already exist in our system: ' . implode(', ', array_unique($companyNames)));
    }

    /**
     * @param Company[] $companies
     * @return CompanyImportException
     */
    public static function companiesAlreadyExistContact(array $companies)
    {
        $companyNames = array();
        foreach ($companies as $company) {
            $companyNames[] = $company->getCompanyName();
        }

        return new self(
            sprintf(
                'These companies already exist in our system: %s. You will be contacted by a member of our team.',
                implode(', ', array_unique($companyNames))
            )
        );
    }
}
