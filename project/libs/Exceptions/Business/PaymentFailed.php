<?php

/**
 * CMS
 *
 * @license    GPL
 * @category   Project
 * @package    SagePay
 * @author     Tomas Jakstas tomasj@madesimplegroup.com
 * @version    02-Nov-2012
 */

namespace Exception\Business;

use Exception;

class PaymentFailed extends BusinessAbstract
{
    /**
     * @param Exception $e
     * @return PaymentFailed
     */
    public static function invalidData(Exception $e)
    {
        return new self($e->getMessage(), 0, $e);
    }
}
