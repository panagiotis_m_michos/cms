<?php

namespace Exceptions\Business;

use Exception;

class CompaniesHouseException extends Business_Abstract
{
    /**
     * @param string $companyName
     * @return CompaniesHouseException
     */
    public static function companyNameUnavailable($companyName)
    {
        return new self("Company name `$companyName` has been taken.");
    }

    /**
     * @return CompaniesHouseException
     */
    public static function companiesHouseServiceUnreachable()
    {
        return new self('We are unable to check the availability of this company name at the moment because the Companies House name checking service is unavailable. You may still make this purchase, however you may need to supply an alternative name when the Companies House name checking service becomes available again.');
    }

    /**
     * @param string $message
     * @param integer|NULL $code
     * @param Exception $previous
     * @return CompaniesHouseException
     */
    public static function genericException($message, $code = NULL, Exception $previous = NULL)
    {
        return new self($message, $code, $previous);
    }
}
