<?php

namespace Exceptions\Business;

use Entities\Company;
use Entities\Customer;

class CompanyException extends Business_Abstract
{
    /**
     * @param Company $company
     * @param Customer $customer
     * @return CompanyException
     */
    public static function companyDoesNotBelongToCustomer(Company $company, Customer $customer)
    {
        return new self("Company {$company->getCompanyName()} doesn't belong to customer {$customer->getFullName()}");
    }
}
