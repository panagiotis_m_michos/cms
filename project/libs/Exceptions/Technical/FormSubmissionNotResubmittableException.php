<?php

namespace Exceptions\Technical;

use Entities\CompanyHouse\FormSubmission;
use Exception;

class FormSubmissionNotResubmittableException extends TechnicalAbstract
{
    /**
     * @param FormSubmission $formSubmission
     * @param Exception $previous
     * @return FormSubmissionNotResubmittableException
     */
    public static function generic(FormSubmission $formSubmission, Exception $previous = NULL)
    {
        return new self("Form submission with id {$formSubmission->getFormSubmissionId()} can not be resubmitted.", 0, $previous);
    }
}
