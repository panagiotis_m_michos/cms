<?php

namespace Exceptions\Technical;

use Entities\Customer;
use Entities\Payment\Token;
use Entities\Service;
use Exception;

class AutoRenewalException extends TechnicalAbstract
{
    const AUTO_RENEWAL_FAILED = 1;

    /**
     * @return $this
     */
    public static function customersDataMissing()
    {
        return new self('Customer not found in renewal data');
    }

    /**
     * @param Customer $customer
     * @return $this
     */
    public static function noActiveTokenForCustomer(Customer $customer)
    {
        return new self("Customer id {$customer->getCustomerId()} has no active token");
    }

    /**
     * @param Customer $customer
     * @param Exception $previous
     * @return $this
     */
    public static function autoRenewalFailed(Customer $customer, Exception $previous = NULL)
    {
        return new self(
            sprintf(
                "Auto renewal for customer %d{$customer->getCustomerId()} failed with error: %s",
                $previous->getMessage()
            ),
            self::AUTO_RENEWAL_FAILED,
            $previous
        );
    }

    /**
     * @param Customer $customer
     * @param Token $customerToken
     * @param Service $service
     * @param Token $serviceToken
     * @return AutoRenewalException
     */
    public static function differentTokens(
        Customer $customer,
        Token $customerToken,
        Service $service,
        Token $serviceToken
    )
    {
        return new self(
            sprintf(
                "Renewal token for service %d (tokenId %d) is different from customer's token (customerId %d, tokenId %d)",
                $service->getServiceId(),
                $serviceToken->getTokenId(),
                $customer->getCustomerId(),
                $customerToken->getTokenId()
            )
        );
    }
}
