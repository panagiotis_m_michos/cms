<?php

namespace Exceptions\Technical;

use Entities\Order;

class RemoveRefundedServicesException extends TechnicalAbstract
{
    /**
     * @param Order $order
     * @return RemoveRefundedServicesException
     */
    public static function multipleServicesDetected(Order $order)
    {
        return new self(sprintf("Multiple services for order `%d` detected", $order->getId()));
    }
}