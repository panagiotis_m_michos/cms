<?php

namespace Exceptions\Technical;

use Entities\Service;

class InvalidArgumentException extends TechnicalAbstract
{
    /**
     * @param Service $service
     * @return InvalidArgumentException
     */
    public static function companyForServiceDoesNotExist(Service $service)
    {
        return new self("Service {$service->getServiceId()} has no company associated");
    }
}
