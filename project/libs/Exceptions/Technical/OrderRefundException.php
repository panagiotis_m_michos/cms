<?php

namespace Exceptions\Technical;

use Entities\OrderItem;

class OrderRefundException extends TechnicalAbstract
{
    /**
     * @param OrderItem $orderItem
     * @return OrderRefundException
     */
    public static function multipleServicesDetected(OrderItem $orderItem)
    {
        return new self(sprintf("Multiple services for order item `%d` refund detected", $orderItem->getId()));
    }
}