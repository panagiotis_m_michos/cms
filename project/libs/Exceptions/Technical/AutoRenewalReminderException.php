<?php

namespace Exceptions\Technical;

use Entities\Company;
use Entities\Customer;
use Exception;

class AutoRenewalReminderException extends TechnicalAbstract
{
    const COMMON_EXCEPTION = 0;

    /**
     * @param Exception $previous
     * @return $this
     */
    public static function common(Exception $previous = NULL)
    {
        return new self('Auto renewal reminder error', self::COMMON_EXCEPTION, $previous);
    }
}
