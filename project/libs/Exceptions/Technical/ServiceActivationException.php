<?php

namespace Exceptions\Technical;

use Entities\Company;
use Entities\Service;

class ServiceActivationException extends TechnicalAbstract
{
    /**
     * @param Company $company
     * @param Service $packageService
     * @param Service $productService
     * @return ServiceActivationException
     */
    public static function packageToProductDowngrade(Company $company, Service $packageService, Service $productService)
    {
        return new self(
            sprintf(
                'Service "%s" (serviceId: %d) for company "%s" (companyId: %d) can not be activated, because there is already "%s" (serviceId: %d) on hold',
                $productService->getServiceName(),
                $productService->getServiceId(),
                $company->getCompanyName(),
                $company->getCompanyId(),
                ($packageService->hasParent() ? $packageService->getParent()->getServiceName() : $packageService->getServiceName()),
                ($packageService->hasParent() ? $packageService->getParent()->getServiceId() : $packageService->getServiceId())
            )
        );
    }
}
