<?php

namespace Exceptions\Technical;

use Exception;

class FormSubmissionNotFoundException extends TechnicalAbstract
{
    /**
     * @param int $formSubmissionId
     * @param Exception $previous
     * @return FormSubmissionNotFoundException
     */
    public static function generic($formSubmissionId, Exception $previous = NULL)
    {
        return new self("Form submission with id {$formSubmissionId} not found.", 0, $previous);
    }
}
