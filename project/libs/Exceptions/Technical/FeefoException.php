<?php

namespace Exceptions\Business;

class FeefoException extends Business_Abstract
{

    /**
     * @param string $parameter
     * @return $this
     */
    public static function configParameterMissing($parameter)
    {
        return new self("Feefo is not configured properly. Parameter '{$parameter}' is missing in conifg.");
    }

    /**
     * @param string $response
     * @return FeefoException
     */
    public static function wrongResponseReceived($response)
    {
        return new self("Wrong response received: {$response}");
    }
}
