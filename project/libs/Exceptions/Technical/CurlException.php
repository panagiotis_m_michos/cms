<?php

namespace Exceptions\Business;

class CurlException extends Business_Abstract
{
    /**
     * @return CurlException
     */
    public static function curlNotFound()
    {
        return new self('cURL is not installed in your server.');
    }

    /**
     * @param int $number
     * @param string $message
     * @return CurlException
     */
    public static function requestError($number, $message)
    {
        return new self("Error #{$number}: A problem occured when posting data to Feefo. Message: {$message}", $number);
    }
}
