<?php

namespace Exceptions\Technical;

class CompanyException extends TechnicalAbstract
{
    /**
     * @param $companyNumber
     * @return CompanyException
     */
    public static function importedCompanyDetailsMissing($companyNumber)
    {
        return new self("Details for imported company with company number {$companyNumber} are missing in session");
    }
}
