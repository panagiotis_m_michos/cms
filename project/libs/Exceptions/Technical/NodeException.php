<?php

namespace Exceptions\Business;

use Exceptions\Technical\TechnicalAbstract;

class NodeException extends TechnicalAbstract
{
    /**
     * @param int $nodeId
     * @return NodeException
     */
    public static function nodeDoesNotExist($nodeId)
    {
        return new self("Node `$nodeId` does not exist");
    }

    /**
     * @param int $nodeId
     * @return NodeException
     */
    public static function nodeIsNotAProduct($nodeId)
    {
        return new self("Node `$nodeId` is not a product");
    }
}