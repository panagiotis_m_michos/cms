<?php

/**
 * CMS
 *
 * @license    GPL
 * @category   Project
 * @package    Exception
 * @author     Tomas Jakstas tomasj@madesimplegroup.com
 * @version    23-Aug-2012
 */
class ServiceUnreachable extends Exception
{

}
