<?php

use Exceptions\Business\CompaniesHouseException;

class CompanySearch extends Object
{
    /**
     * @param string $companyName
     * @return bool
     * @throws CompaniesHouseException
     */
    public static function isAvailable($companyName)
    {
        /* remove suffix from the name */
        //$companyName = strtoupper(self::getCleanCompanyName($companyName));

        $xmlGateway = new CHXmlGateway();
        $nameSearch = $xmlGateway->getNameSearch($companyName, 'LIVE');
        $nameSearch->setSameAs(1);
        $response = $xmlGateway->getResponse($nameSearch, 8);

        /* check if we received response */
        if (!$response) {
            throw CompaniesHouseException::companiesHouseServiceUnreachable();
        }

        $xml = simplexml_load_string($response);

        /* no xml returned */
        if (!$xml) {
            throw CompaniesHouseException::companiesHouseServiceUnreachable();
        }

        if (!empty($xml->GovTalkDetails->GovTalkErrors)) {
            $errors = $xml->GovTalkDetails->GovTalkErrors->children();
            $error = reset($errors);

            if (empty($error->Text)) {
                throw CompaniesHouseException::companiesHouseServiceUnreachable();
            } else {
                throw new Exception($error->Text);
            }
        }

        //does company exists in companies house
        $companyExistInCompaniesHouse = !empty($xml->Body->NameSearch->CoSearchItem);
        return !$companyExistInCompaniesHouse;
    }

    private static function getCleanCompanyName($companyName)
    {

        /*
         * patterns has to be in order - more specific first
         * because only first matched pattern is used - after that, empty space
         * after the company name is left, so it allows us not to use next match
         */

        /* positive look behind */
        $pattern    = array(
            /* LIMITED/LTD/LIMITED PARTNERSHIP/LLP */
            '/(?<= )\+ CO LIMITED$/i',
            '/(?<= )& CO LIMITED$/i',
            '/(?<= )AND CO LIMITED$/i',
            '/(?<= )CO LIMITED$/i',
            '/(?<= )\+ COMPANY LIMITED$/i',
            '/(?<= )& COMPANY LIMITED$/i',
            '/(?<= )AND COMPANY LIMITED$/i',
            '/(?<= )COMPANY LIMITED$/i',
            '/(?<= )UN LIMITED$/i',
            '/(?<= )LIMITED$/i',
            '/(?<= )LIMITED PARTNERSHIP$/i',
            '/(?<= )PARTNERSHIP$/i',
            '/(?<= )LIMITED PARTNERSHIPS$/i',
            '/(?<= )PARTNERSHIPS$/i',
            '/(?<= )\+ CO LTD$/i',
            '/(?<= )& CO LTD$/i',
            '/(?<= )AND CO LTD$/i',
            '/(?<= )CO LTD$/i',
            '/(?<= )& COMPANY LTD$/i',
            '/(?<= )AND COMPANY LTD$/i',
            '/(?<= )COMPANY LTD$/i',
            '/(?<= )LTD$/i',
            '/(?<= )LLP$/i',
            /* COMPANY/CO */
            '/(?<= )\+ COMPANY PUBLIC LIMITED COMPANY$/i',
            '/(?<= )& COMPANY PUBLIC LIMITED COMPANY$/i',
            '/(?<= )AND COMPANY PUBLIC LIMITED COMPANY$/i',
            '/(?<= )COMPANY PUBLIC LIMITED COMPANY$/i',
            '/(?<= )\+ CO PUBLIC LIMITED COMPANY$/i',
            '/(?<= )& CO PUBLIC LIMITED COMPANY$/i',
            '/(?<= )AND CO PUBLIC LIMITED COMPANY$/i',
            '/(?<= )CO PUBLIC LIMITED COMPANY$/i',
            '/(?<= )PUBLIC LIMITED COMPANY$/i', /* no +, & or AND for this one */
            '/(?<= )LIMITED COMPANY$/i',
            '/(?<= )\+ COMPANY$/i',
            '/(?<= )& COMPANY$/i',
            '/(?<= )AND COMPANY$/i',
            '/(?<= )COMPANY$/i',
            '/(?<= )\+ CO$/i',
            '/(?<= )& CO$/i',
            '/(?<= )AND CO$/i',
            '/(?<= )CO$/i',
            /* PLC */
            '/(?<= )\+ COMPANY PLC$/i',
            '/(?<= )& COMPANY PLC$/i',
            '/(?<= )AND COMPANY PLC$/i',
            '/(?<= )COMPANY PLC$/i',
            '/(?<= )\+ CO PLC$/i',
            '/(?<= )& CO PLC$/i',
            '/(?<= )AND CO PLC$/i',
            '/(?<= )CO PLC$/i',
            '/(?<= )PLC$/i',
            /* language of wales - didn't know that wales can speak */
            '/(?<= )AR CWMNI CYF$/i',
            '/(?<= )CWMNI CYF$/i',
            '/(?<= )CYF$/i',
            '/(?<= )AR CWMNI CWMNI CYFYNGEDIG CYHOEDDUS$/i',
            '/(?<= )AR CWMNI CYFYNGEDIG CYHOEDDUS$/i',
            '/(?<= )CWMNI CWMNI CYFYNGEDIG CYHOEDDUS$/i',
            '/(?<= )CWMNI CYFYNGEDIG CYHOEDDUS$/i',
            '/(?<= )CYFYNGEDIG CYHOEDDUS$/i',
            '/(?<= )CYHOEDDUS$/i',
            '/(?<= )AR CWMNI CYFYNGEDIG$/i',
            '/(?<= )CWMNI CYFYNGEDIG$/i',
            '/(?<= )CYFYNGEDIG$/i',
            '/(?<= )AR CWMNI CCC$/i',
            '/(?<= )CWMNI CCC$/i',
            '/(?<= )CCC$/i',
            '/(?<= )AR CWMNI$/i',
            '/(?<= )CWMNI$/i',
            '/(?<= )ANGHYFYNGEDIG$/i'
        );

        /* remove suffix from the name and convert &amp;, &lt; and &gt; */
        $cleanCompanyName = preg_replace($pattern, '', trim(htmlspecialchars_decode($companyName, ENT_NOQUOTES)));

        /* there can be empty space at the end after removal - that helps us not to use next regex pattern */
        return trim($cleanCompanyName);
    }
}
