<?php 

class testClass
{		
	
	public static function printTestForm()
	{
		print <<<EOT
		<html>
		<head>
		<title>Sage Pay VSP Direct Test</title>
		<link rel="stylesheet" type="text/css" href="test/css/style.css">		
		<script type="text/javascript" language="javascript" src="test/scripts/main.js"></script>
		<script type="text/javascript" language="javascript" src="test/scripts/jquery.js"></script>
		</head>
		<body>
		<table>
		<tbody>
		<form action="#" method="post">
		<tr><td>Cardholder Name: </td>
		<td><input type="text" name="Cardholder" /></td></tr>
		<tr><td>Card Type: </td>
		<td>		
		<select name="CardType">
			<option value="VISA">Visa</option>
			<option value="MC">MasterCard</option>
			<option value="DELTA">Visa Debit/Delta</option>
			<option value="SOLO">Solo</option>
			<option value="MAESTRO">UK Maestro</option>
			<option value="MAESTRO">International Maestro</option>
			<option value="AMEX">American Express</option>
			<option value="UKE">Visa Electron</option>
			<option value="JCB">JCB</option>
			<option value="DINERS">Diner's Club</option>
			<option value="LASER">Laser</option>
		</select>
		</td></tr>
		<tr><td>Card Number: </td>
		<td>
		<select name="CardNumber">
			<option value="4929000000006">Visa</option>
			<option value="5404000000000001">MasterCard</option>
			<option value="4462000000000003">Visa Debit/Delta</option>
			<option value="6334900000000005">Solo</option>
			<option value="5641820000000005">UK Maestro</option>
			<option value="300000000000000004">International Maestro</option>
			<option value="374200000000004">American Express</option>
			<option value="4917300000000008">Visa Electron</option>
			<option value="3569990000000009">JCB</option>
			<option value="36000000000008">Diner's Club</option>
			<option value="6304990000000000044">Laser</option>
		</select>
		</td></tr>
		<tr><td>Start Date: </td>
		<td><input type="text" name="StartDate" /></td></tr>
		<tr><td>Expiry Date: </td>
		<td><input type="text" name="ExpiryDate" /></td></tr>
		<tr><td>Issue Number: </td>
		<td><input type="text" name="IssueNumber" />just for solo = 1 and uk maestro = 01</td></tr>
		<tr><td>CV2: </td>
		<td><input type="text" name="CV2" value="123" /></td></tr>
		<tr><td>Billing Surname: </td>
		<td><input type="text" name="BillingSurname" /></td></tr>
		<tr><td>Billing Firstnames: </td>
		<td><input type="text" name="BillingFirstnames" /></td></tr>
		<tr><td>Billing Address1: </td>
		<td><input type="text" name="BillingAddress1" value="88" /></td></tr>
		<tr><td>Billing Address2: </td>
		<td><input type="text" name="BillingAddress2" /></td></tr>
		<tr><td>Billing City: </td>
		<td><input type="text" name="BillingCity" /></td></tr>
		<tr><td>Billing Post Code: </td>
		<td><input type="text" name="BillingPostCode" value="412" /></td></tr>
		<tr><td>Billing Country: </td>
		<td><input type="text" name="BillingCountry" value="GB" /></td></tr>
		<tr><td>Billing State: </td>
		<td><input type="text" name="BillingState" /></td></tr>
		<tr><td>Billing Phone: </td>
		<td><input type="text" name="BillingPhone" /></td></tr>
		<tr><td></td><td><input type="submit" name="submit" value="Submit" /></td></tr>
		</form>	
		</tbody>
		</table>
		</body>	
		</html>
EOT;
	}
}

?>