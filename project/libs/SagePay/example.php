<?php
ini_set('display_errors', E_ALL);
include_once("lib/SagePayDirect.php");

$sage = new SagePayDirect();
#$sage->setToTest();

if(isset($_REQUEST["PaRes"]))
{	
	$sage = unserialize($_SESSION['SagePayDirect']);
	$response = $sage->finish3DSecure($_REQUEST);	
	
	print_r($response);			
	exit;
}
elseif(!empty($_POST))
{		
	$payment = $sage->getPayment();	
	$payment->setCard($_POST['Cardholder'], $_POST['CardType'], $_POST['CardNumber'], $_POST['ExpiryDate']);
	$payment->setBillingAddress($_POST['BillingFirstnames'], $_POST['BillingSurname'], $_POST['BillingAddress1'], $_POST['BillingCity'], $_POST['BillingPostCode'], $_POST['BillingCountry']);	
	$payment->setDeliveryAddress($_POST['BillingFirstnames'], $_POST['BillingSurname'], $_POST['BillingAddress1'], $_POST['BillingCity'], $_POST['BillingPostCode'], $_POST['BillingCountry']);
	$payment->setPaymentDetails("description goes here..", 2.00);	
	
	$sage->setAction($payment);
	
	$response = $sage->doAction();
	
	print_r($response);
	exit;
	
	//ABORT ACTION
	/*
	$abort = $sage->getAbort();
	$abort->setAbortDetails($vpsTxId, $securityKey, $txAuthNo);
	$sage->setAction($abort);
	$response = $sage->doAction();
	*/
	
	//AUTHORISE ACTION
	/*
	$authorise = $sage->getAuthorise();
	$authorise->setAuthoriseDetails($description, $amount, $relatedVPSTxId, $relatedVendorTxCode, $relatedSecurityKey);
	$sage->setAction($authorise);
	$response = $sage->doAction();
	*/

	//CANCEL ACTION
	/*
	$cancel = $sage->getCancel();
	$cancel->setCancelDetails($vpsTxId, $securityKey);
	$sage->setAction($cancel);
	$response = $sage->doAction();
	*/
	
	//DIRECT REFUND ACTION
	/*
	$directRefund = $sage->getDirectRefund();
	$directRefund->setDirectRefundDetails($amount, $description);
	$directRefund->setCardDetails($cardholderName, $cardType, $cardNumber, $expiryDate);
	$sage->setAction($directRefund);
	$response = $sage->doAction();
	*/
	
	//MANUAL PAYMENT ACTION
	/*
	$manual = $sage->getManualPayment();
	$manual->setManualPaymentDetails();
	$sage->setAction($manual);
	$response = $sage->doAction();
	*/
	
	//PAYPAL PAYMENT ACTION
	/*
	$paypal = $sage->getPaypal();
	$paypal->setPaypalDetails();
	$sage->setAction($paypal);
	$response = $sage->doAction();
	*/	
	
	//REFUND ACTION
	/*
	$refund = $sage->getRefund();
	$refund->setRepeatDetails($description, $amount, $relatedVPSTxId, $relatedVendorTxCode, $relatedSecurityKey, $relatedTxAuthNo);
	$sage->setAction($refund);
	$response = $sage->doAction();
	*/
	
	//RELEASE ACTION
	/*
	$release = $sage->getRelease();
	$release->setRepeatDetails($vpsTxId, $securityKey, $txAuthNo, $releaseAmount);
	$sage->setAction($release);
	$response = $sage->doAction();
	*/
	
	//REPEAT ACTION
	/*
	$repeat = $sage->getRepeat();
	$repeat->setRepeatDetails($description, $amount, $relatedVPSTxId, $relatedVendorTxCode, $relatedSecurityKey, $relatedTxAuthNo);
	$sage->setAction($repeat);
	$response = $sage->doAction();
	*/
	
	//VOID ACTION
	/*
	$void = $sage->getVoid();
	$void->setVoidDetails($vpsTxId, $securityKey);
	$sage->setAction($void);
	$response = $sage->doAction();
	*/

}	
else
{
	require_once("test/testClass.php");
	testClass::printTestForm();
}

?>