<?php

use Admin\Cashback\PaymentForm;
use Services\Payment\TokenService;

class SageService extends Object
{

    const NO_SAGE_PAY_RESPONSE = 'ERROR: MSG01. Please contact us on +44 (0) 207 608 5500 or info@madesimplegroup.com - No SagePay response';

    /**
     * @var Basket 
     */
    private $basket;

    /**
     * @var PaymentForm
     */
    private $form;

    /**
     * @var array
     */
    private $query;

    /**
     * @var array
     */
    private $post;

    /**
     * @var array
     */
    private $credentials = [];

    /**
     * @var string 
     */
    private $token;

    /**
     * @var SagePayDirect 
     */
    private $sage;

    /**
     * @var TokenLogService
     */
    private $tokenLogService;

    /**
     *
     * @var DirectTokenPayment
     */
    private $service;

    /**
     * @var int 
     */
    private $tokenLogId;

    /**
     * @var TokenService
     */
    private $tokenService;

    /**
     * @var SessionNamespace
     */
    private $reenableAutoRenewalSession;

    /**
     * @param Basket $basket
     * @param PaymentForm $form
     * @param array $query
     * @param array $post
     */
    public function __construct(Basket $basket, $form, array $query, array $post)
    {
        $this->sage = new SagePayDirect();
        $this->basket = $basket;
        $this->form = $form;
        $this->query = $query;
        $this->post = $post;
        $this->credentials = FApplication::$config['sage'];
        $this->sage->setVendor($this->credentials['vendor']);
        $this->sage->setSystem($this->credentials['server']);
        $this->tokenLogService = new TokenLogService($this->post);
        $this->service = new DirectTokenPayment($this->sage, $this->post);
        $this->tokenService = Registry::$container->get(DiLocator::SERVICE_TOKEN);
        $this->reenableAutoRenewalSession = Registry::$container->get(DiLocator::SESSION_REENABLE_AUTO_RENEWAL);
    }

    /**
     * @param PaymentService $paymentService
     * @param bool $disable3dSecure
     * @return array
     * @throws Exception
     * @throws SageAuthenticationException
     * @throws SageCurlException
     */
    public function processTokenPayments(PaymentService $paymentService, $disable3dSecure = FALSE)
    {
        try {
            $this->isReturningTransaction();
            $this->token = $this->getTokenFromSession();

            if ($this->form->getValue('cardNumber') && !$this->getTokenFromSession()) {
                !isset($this->post['cardShortNumber']) ? $this->post['cardShortNumber'] = substr($this->form->getValue('cardNumber'), -4, 4) : NULL;
                $this->token = $this->service->getDirectPaymentToken();
                $this->saveTokenToSession($this->token);
                $this->tokenLogId = $this->tokenLogService->processTokenLog($this->token, $this->post);
                $this->tokenLogService->saveTokenLogId($this->tokenLogId);
            }

            $this->tokenLogId = $this->tokenLogService->getTokenLogId();
            //sage response
            $response = $this->service->doDirectTokenAction($this->token, $this->basket, 1, $disable3dSecure);
            $this->handlePaymentResponse($response);
            $paymentResponse = $this->getPaymentResponse($response, TRUE);
            $customer = $paymentService->processCustomerInformations($paymentResponse);
            $this->service->saveDirectPaymentToken($customer, $this->post, $this->token);
            $tokenEntity = $this->tokenService->getTokenByIdentifier($this->token);
            $paymentService->completeTransaction($paymentResponse, $tokenEntity);
            $this->reenableAutoRenewalSession->shouldReenable = TRUE;
        } catch (SageAuthenticationException $e) {
            throw $e;
        } catch (SageCurlException $e) {
            PaymentModel::savePaymentFailedTransatcion(serialize($this->post), $this->basket, isset($_SESSION['VendorTxCode']) ? $_SESSION['VendorTxCode'] : NULL, 'NEW', $e->getMessage(), PaymentErrorsLog::SAGE, PaymentErrorsLog::STATUS_CURL_ERROR);
            $this->removeTokens($this->token);
            SagePayDirect::cleanAuthForm();
            throw $e;
        } catch (Exception $e) {
            $this->removeTokens($this->token);
            $this->tokenLogService->processTokenLog($this->token, $this->post, TokenLogs::TOKEN_STATUS_ERROR, $this->tokenLogId);
            SagePayDirect::cleanAuthForm();
            throw $e;
        }

        $this->tokenLogService->processTokenLog($this->token, $this->post, TokenLogs::TOKEN_STATUS_COMPLETE, $this->tokenLogId);
        SagePayDirect::cleanAuthForm();
    }

    /**
     * @param PaymentService $paymentService
     * @throws SageAuthenticationException
     */
    public function processCardPayments(PaymentService $paymentService)
    {
        try {
            $this->isReturningTransaction();

            if ($this->form->getValue('cardNumber') && !$this->getTokenFromSession()) {
                !isset($this->post['cardShortNumber']) ? $this->post['cardShortNumber'] = substr($this->form->getValue('cardNumber'), -4, 4) : NULL;
                $this->token = $this->service->getDirectPaymentToken();
                $this->saveTokenToSession($this->token);
                $this->tokenLogId = $this->tokenLogService->processTokenLog($this->token, $this->post);
                $this->tokenLogService->saveTokenLogId($this->tokenLogId);
            }

            //sage response
            $response = $this->service->doDirectTokenAction($this->token, $this->basket, 0);
            $this->handlePaymentResponse($response);
            $paymentResponse = $this->getPaymentResponse($response, TRUE);
            $customer = $paymentService->processCustomerInformations($paymentResponse);
            $paymentService->completeTransaction($paymentResponse);

            $this->tokenLogService->processTokenLog($this->token, $this->post, TokenLogs::TOKEN_STATUS_COMPLETE, $this->tokenLogId);
            //$this->service->saveDirectPaymentToken($customer, $this->post, $this->token);
            SagePayDirect::cleanAuthForm();
        } catch (SageAuthenticationException $e) {
            throw $e;
        } catch (SageCurlException $e) {
            PaymentModel::savePaymentFailedTransatcion(serialize($this->post), $this->basket, isset($_SESSION['VendorTxCode']) ? $_SESSION['VendorTxCode'] : NULL, 'NEW', $e->getMessage(), PaymentErrorsLog::SAGE, PaymentErrorsLog::STATUS_CURL_ERROR);
            $this->service->removeTransactionToken($this->token);
            SagePayDirect::cleanAuthForm();
            throw $e;
        } catch (Exception $e) {
            $this->service->removeTransactionToken($this->token);
            $this->tokenLogService->processTokenLog($this->token, $this->post, TokenLogs::TOKEN_STATUS_ERROR, $this->tokenLogId);
            SagePayDirect::cleanAuthForm();
            throw $e;
        }
    }

    /**
     * @param PaymentService $paymentService
     * @param TokensModel $tokenModel
     * @param Customer $customer
     * @param Entities\Customer $customerEntity
     * @param Services\OrderService $orderEntityService
     * @param Services\ProductService $productService
     */
    public function processReusedTokenPayments(PaymentService $paymentService, TokensModel $tokenModel, Customer $customer, Entities\Customer $customerEntity, Services\OrderService $orderEntityService, Services\ProductService $productService)
    {
        //'sending data to sage'
        $response = $tokenModel->useToken($customer, $this->basket);
        $tokenModel->getTokenAnswer($customer, $this->basket, $response, $customerEntity, $orderEntityService, $productService);
    }

    /**
     * @param PaymentService $paymentService
     * @return array
     */
    public function process(PaymentService $paymentService)
    {
        try {
            ($this->form->getValue('cardNumber')) ? $this->mapCardInformations($this->form->getValues()) : NULL;
            $backUrl = FApplication::$router->secureLink(PaymentNewCustomerControler::SAGE_AUTH_VALIDATION, ['sagepay-3d' => 1]);
            $this->isReturningTransaction();

            $response = $this->sage->doAction($backUrl);
            $this->handlePaymentResponse($response);
            $paymentResponse = $this->getPaymentResponse($response);
            $paymentService->processCustomerInformations($paymentResponse);
            $paymentService->completeTransaction($paymentResponse);
            SagePayDirect::cleanAuthForm();
        } catch (SageAuthenticationException $e) {
            throw $e;
        } catch (SageCurlException $e) {
            PaymentModel::savePaymentFailedTransatcion(serialize($this->post), $this->basket, isset($_SESSION['VendorTxCode']) ? $_SESSION['VendorTxCode'] : NULL, 'NEW', $e->getMessage(), PaymentErrorsLog::SAGE, PaymentErrorsLog::STATUS_CURL_ERROR);
            SagePayDirect::cleanAuthForm();
            throw $e;
        } catch (Exception $e) {
            SagePayDirect::cleanAuthForm();
            throw $e;
        }
    }

    /**
     * 
     * @param PaymentService $paymentService
     * @throws SageAuthenticationException
     */
    public function processPaymentViaPhone(PaymentService $paymentService)
    {
        try {
            ($this->form->getValue('cardNumber')) ? $this->mapCardInformations($this->form->getValues(), TRUE, $this->paymentViaPhoneSagePayeDescription()) : NULL;
            $this->isReturningTransaction();
            $response = $this->sage->doAction();
            $this->handlePaymentResponse($response);
            $paymentResponse = $this->getPaymentResponse($response);
            $paymentService->processCustomerInformations($paymentResponse, !PaymentService::CUSTOMER_SIGN_IN);
            $paymentService->completeTransaction($paymentResponse);
        } catch (SageAuthenticationException $e) {
            throw $e;
        } catch (SageCurlException $e) {
            PaymentModel::savePaymentFailedTransatcion(serialize($this->post), $this->basket, isset($_SESSION['VendorTxCode']) ? $_SESSION['VendorTxCode'] : NULL, 'PHONE', $e->getMessage(), PaymentErrorsLog::SAGE, PaymentErrorsLog::STATUS_CURL_ERROR);
            SagePayDirect::cleanAuthForm();
            throw $e;
        } catch (Exception $e) {
            SagePayDirect::cleanAuthForm();
            throw $e;
        }
    }

    /**
     * @throws \Exception 
     */
    private function isReturningTransaction()
    {
        if (!isset($this->post['MD'])) {
            if (isset($_SESSION['VendorTxCode'])) {
                PaymentModel::savePaymentFailedTransatcion(serialize($this->post), $this->basket, isset($_SESSION['VendorTxCode']) ? $_SESSION['VendorTxCode'] : NULL, 'NEW', 'Transaction was cancelled due to form re-submission', PaymentErrorsLog::SAGE, PaymentErrorsLog::STATUS_RESUBMIT);
                throw new Exception(
                    'Transaction was cancelled due to form re-submission. No payment has been taken.
                     Please submit the form again.'
                );
            }
            SagePayDirect::cleanAuthForm();
        }
    }

    /**
     * @param Sage $response
     * @param bool $tokenPayment
     * @return PaymentResponse
     */
    private function getPaymentResponse($response, $tokenPayment = FALSE)
    {
        $paymentResponse = new PaymentResponse();
        $paymentResponse->setTypeId(Transaction::TYPE_SAGEPAY);
        $sage = unserialize($_SESSION['SagePayDirect']);

        /** @var SagePayTokenPayment $paymentDetails */
        $paymentDetails = $sage->getAction();
        $newCustomerAddress = $paymentDetails->getDeliveryAddress();

        $paymentResponse->setPaymentTransactionTime(date('Y m d H:i:s'));
        $paymentResponse->setPaymentHolderEmail($paymentDetails->getCustomerEMail());
        $paymentResponse->setPaymentHolderFirstName($newCustomerAddress->getFirstnames());
        $paymentResponse->setPaymentHolderLastName($newCustomerAddress->getSurname());
        $paymentResponse->setPaymentHolderAddressStreet($newCustomerAddress->getLine1() . ' ' . $newCustomerAddress->getLine2());
        $paymentResponse->setPaymentHolderAddressCity($newCustomerAddress->getCity());
        $paymentResponse->setPaymentHolderAddressState($newCustomerAddress->getState() ? $newCustomerAddress->getState() : NULL);
        $paymentResponse->setPaymentHolderAddressPostCode($newCustomerAddress->getPostcode());
        $paymentResponse->setPaymentHolderAddressCountry($newCustomerAddress->getCountry());

        $paymentResponse->setPaymentTransactionDetails(serialize($response));
        $paymentResponse->setPaymentCardNumber($tokenPayment ? $this->post['cardShortNumber'] : substr($sage->getAction()->getCard()->getCardNumber(), -4));
        $paymentResponse->setPaymentOrderCode(trim($response['VPSTxId']));
        $paymentResponse->setPaymentVpsAuthCode(trim($response['TxAuthNo']));
        $paymentResponse->setPaymentVendorTXCode($_SESSION['VendorTxCode']);

        unset($_SESSION['SagePayDirect']);
        unset($_SESSION['SagePaymentForm']);
        SagePayDirect::cleanAuthForm();
        return $paymentResponse;
    }

    /**
     *
     * @param SageDirect $response
     * @throws Exception 
     */
    private function handlePaymentResponse($response)
    {
        if (!empty($response)) {
            if (trim($response['Status']) != 'OK') {
                $msg = trim($response['Status']) . '. ' . trim($response['StatusDetail']);
                PaymentModel::savePaymentFailedTransatcion($_SESSION['SagePayDirect'], $this->basket, isset($_SESSION['VendorTxCode']) ? $_SESSION['VendorTxCode'] : NULL, 'NEW', $msg, PaymentErrorsLog::SAGE, PaymentErrorsLog::STATUS_FAILED);

                if (preg_match('/2000/', $msg)) {
                    throw new Exception($msg . " This could be due to funds not being available, spending limit reached or bank account closure. We advise that you contact your card issuer for more information. Alternatively you can attempt the transaction again using another card.");
                } elseif (preg_match('/2001/', $msg)) {
                    throw new Exception($msg . "This is often due to the billing address, postcode or security code not matching the details held by the card issuer. Please double check all the details you have entered and try again. If you have received this error multiple times we advise that you contact your card issuer for more information.");
                } else {
                    throw new Exception($msg . " If you have any questions regarding this message please contact us +44 (0) 207 608 5500.");
                }
            }
        } else {
            PaymentModel::savePaymentFailedTransatcion($_SESSION['SagePayDirect'], $this->basket, isset($_SESSION['VendorTxCode']) ? $_SESSION['VendorTxCode'] : NULL, 'NEW', $msg, PaymentErrorsLog::SAGE, PaymentErrorsLog::STATUS_EMPTY_RESPONSE);
            throw new SageCurlException('ERROR: MSG01. Please contact us on +44 (0) 207 608 5500 or info@madesimplegroup.com - No SagePay response');
        }
    }

    private function mapCardInformations($data, $secure3d = NULL, $description = NULL)
    {

        // expiry date
        $explode = explode('-', $data['expiryDate']);
        $explode[0] = substr($explode[0], -2);
        if (strlen($explode[1]) < 2) {
            $explode[1] = str_replace($explode[1], '0' . $explode[1], $explode[1]);
        }
        $expiryDate = $explode[1] . $explode[0];
        if (isset($data['cardType']) && $data['cardType'] == SagePayDirect::CARD_MAESTRO) {
            if (isset($data['validFrom'])) {
                $explode = explode('-', $data['validFrom']);
                $explode[0] = substr($explode[0], -2);
                if (strlen($explode[1]) < 2) {
                    $explode[1] = str_replace($explode[1], '0' . $explode[1], $explode[1]);
                }
                $validFrom = $explode[1] . $explode[0];
            }
        }

        // Names
        $names = explode(' ', trim($data['cardholder']));
        $firstnames = '';
        foreach ($names as $value) {
            $firstnames .= ' ' . $value;
        }
        $firstnames = str_replace($value, '', $firstnames);
        $temp = str_replace(' ', '', $firstnames);
        if (trim($firstnames) == '' or trim($temp) == '') {
            $firstnames = 'M';
        }
        $surname = $value;
        $firstnames = SagePayDirect::cutString($firstnames, 20);
        $surname = SagePayDirect::cutString($surname, 20);

        if (isset($data['address2']) || isset($data['address3'])) {
            $address2 = '';
            $address2 .= $data['address2'] ? $data['address2'] : '';
            $address2 .= $data['address3'] ? $data['address3'] : '';
        }

        $payment = $this->sage->getPayment();
        $payment->setCard(trim($data['cardholder']), trim($data['cardType']), trim($data['cardNumber']), trim($expiryDate));
        $payment->getCard()->setCVV(trim($data['CV2']));
        $payment->setBillingAddress(trim($firstnames), trim($surname), trim($data['address1']), trim($data['town']), trim($data['postcode']), trim($data['country']));
        $payment->setDeliveryAddressObject();
        if (isset($address2)) {
            $payment->getCard()->getBillingAddress()->setLine2(trim($address2));
            $payment->getDeliveryAddress()->setLine2(trim($address2));
        }
        if (isset($data['issueNumber'])) {
            $payment->getCard()->setIssueNumber(trim($data['issueNumber']));
        }
        if (isset($validFrom)) {
            $payment->getCard()->setStartDate(trim($validFrom));
        }
        if (isset($data['billingState']) && trim($data['country']) == 'US') {
            $payment->getCard()->getBillingAddress()->setState(trim($data['billingState']));
            $payment->getDeliveryAddress()->setState(trim($data['billingState']));
        }

        $payment->setPaymentDetails("Companies Made Simple", $this->basket->getPrice('total'));
        if (isset($data['emails']) && !empty($data['emails'])) {
            $payment->setCustomerEMail($data['emails']);
        }

        if ($secure3d) {
            $payment->setApply3DSecure(2);
        }

        if ($description) {
            $payment->setDescription($description);
        }

        $this->sage->setAction($payment);
    }

    /**
     *
     * @param type $country
     * @return boolean 
     */
    public function getTranslateSageCountry($country)
    {
        if (isset(WPDirect::$countries[$country])) {
            $country = array_search(WPDirect::$countries[$country], Customer::$countries);
            if ($country) {
                return $country;
            }
        }
        return FALSE;
    }

    public function saveTokenToSession($token)
    {
        $_SESSION['token'] = $token;
    }

    public function getTokenFromSession()
    {
        if (isset($_SESSION['token'])) {
            return $_SESSION['token'];
        }
        return NULL;
    }

    /**
     * @return string
     */
    public function paymentViaPhoneSagePayeDescription()
    {
        $user = FUser::getSignedIn();
        $name = $user->firstName . ' ' . substr(ucfirst($user->lastName), 0, 1) . " ({$user->login})";
        return sprintf("Phone Payment: %s", $name);
    }

    /**
     * @param string $tokenIdentifier
     */
    private function removeTokens($tokenIdentifier)
    {
        $token = $this->tokenService->getTokenByIdentifier($tokenIdentifier);
        if ($token) {
            $this->tokenService->removeEntity($token);
        }

        $this->service->removeTransactionToken($this->token);
    }

}
