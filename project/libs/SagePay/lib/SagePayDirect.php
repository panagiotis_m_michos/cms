<?php

/*
+-------------------------------------------------------------------------------+
|   Copyright 2009 Zygimantas Daraska - sidabrinis@gmail.com                    |
|                                                                               |
|   This program is free software: you can redistribute it and/or modify        |
|   it under the terms of the GNU General Public License as published by        |
|   the Free Software Foundation, either version 3 of the License, or           |
|   (at your option) any later version.                                         |
|                                                                               |
|   This program is distributed in the hope that it will be useful,             |
|   but WITHOUT ANY WARRANTY; without even the implied warranty of              |
|   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               |
|   GNU General Public License for more details.                                |
|                                                                               |
|   You should have received a copy of the GNU General Public License           |
|   along with this program.  If not, see <http://www.gnu.org/licenses/>.       |
+-------------------------------------------------------------------------------+ 
 */

/**
 * SagePayDirect
 * 
 * @package SagePayDirect
 * @version 1.0
 * @copyright 2009 Zygimantas Daraska
 * @author Zygimantas Daraska <sidabrinis@gmail.com> 
 * @license GNU General Public License
 */

class SagePayDirect
{
	/**
	 * Some constants which are used in this library
	 */
	const SAGEPAY_VENDOR = "madesimplegroup"; //change it!
	const SAGEPAY_VPSPROTOCOL = "2.23";
	const SAGEPAY_CURRENCY = "GBP";
	const SAGEPAY_SIMULATOR_URL = "https://test.sagepay.com/Simulator/";
	const SAGEPAY_TEST_URL = "https://test.sagepay.com/gateway/service/";
	const SAGEPAY_LIVE_URL = "https://live.sagepay.com/gateway/service/";
	
	const CARD_MAESTRO = 'MAESTRO';
	const CARD_AMEX = 'AMEX';

	const SECURITY_CODE_AMEX_LENGTH = 4;
	const SECURITY_CODE_DEFAULT_LENGTH = 3;

	/**
	 * _vpsProtocol
	 * 
	 * Version of protocol
	 * 
	 * @var string; fixed 4 characters; Default = "2.23"
	 * @access private
	 */	
	private $_vpsProtocol;
	
	/**
	 * _vendor
	 * 
	 * Vendor Login Name.
	 * 
	 * @var string; max 15 characters;
	 * @access private
	 */
	private $_vendor;

	/**
	 * _action
	 * 
	 * object of appropriate action details
	 * 
	 * @var object;
	 * @access private
	 */
	private $_action;
	
	/**
	 * _system
	 * 
	 * Defines which environment should be used: SIMULATOR | TEST | LIVE
	 * 
	 * @var string; Default = SIMULATOR;
	 * @access private
	 */		
	private $_system = "LIVE";
    
	/**
     * __construct 
     * 
     * @access public
     * @return void
     */
	public function __construct()
	{	
		require_once("core/SagePayTransaction.php");	
		$this->_vpsProtocol = self::SAGEPAY_VPSPROTOCOL;
	}
	
	/**
     * getVPSProtocol 
     * 
     * @access public
     * @return string
     */
	public function getVPSProtocol()
	{
		return $this->_vpsProtocol;
	}
	
	/**
     * getVendor 
     * 
     * @access public
     * @return string
     */
	public function getVendor()
	{
		return $this->_vendor;
	}
	
	/**
     * getAction 
     * 
     * @access public
     * @return object
     */
	public function getAction()
	{
		return $this->_action;
	}
	
	/**
     * getSystem 
     * 
     * @access public
     * @return string
     */
	public function getSystem()
	{
		return $this->_system;
	}
	
	/**
     * setVPSProtocol 
     * 
     * @param string $protocol
     * @access public
     * @return void
     */
	public function setVPSProtocol($protocol)
	{
		$this->_vpsProtocol = $protocol;
	}
	
	/**
     * setVendor 
     * 
     * @param string $vendor
     * @access public
     * @return void
     */
	public function setVendor($vendor)
	{
		$this->_vendor = $vendor;
	}	
	
	/**
     * setAction
     * 
     * @param object $action
     * @access public
     * @return void
     */
	public function setAction($action)
	{
		$this->_action = $action;
	}
	
	/**
     * setToLive 
     * 
     * sets system environment to live
     * 
     * @access public
     * @return void
     */
	public function setToLive()
	{
		$this->_system = 'LIVE';
	}
	
	/**
     * setToTest 
     *      
     * sets system environment to test
     * 
     * @access public
     * @return void
     */
	public function setToTest()
	{
		$this->_system = 'TEST';
	}
	
	/**
     * setToSimulator 
     *      
     * sets system environment to simulator
     * 
     * @access public
     * @return void
     */
	public function setToSimulator()
	{
		$this->_system = 'SIMULATOR';
	}

	/**
     * setSystem 
     *      
     * sets system environment
     * 
     * @param string $system
     * @access public
     * @return void
     */
	public function setSystem($system)
	{
		switch (strtoupper($system))
		{
			case 'SIMULATOR':
				$this->_system = 'SIMULATOR';
				break;
			case 'TEST':
				$this->_system = 'TEST';
				break;
			case 'LIVE':
				$this->_system = 'LIVE';
				break;			
		}				
	}
	
	/**
	 * getAbort
	 * 
	 * gets an instance of abort action
	 * 
	 * @access public
	 * @return object;
	 */
	public function getAbort()
	{
		require_once("core/SagePayAbort.php");
		return new SagePayAbort(); 
	}
	
	/**
	 * getAuthorise
	 * 
	 * gets an instance of authorise action
	 *
	 * @access public
	 * @return object;
	 */
	public function getAuthorise()
	{
		require_once("core/SagePayAuthorise.php");
		return new SagePayAuthorise();
	}
	
	/**
	 * getCancel
	 * 
	 * gets an instance of cancel action
	 *
	 * @access public
	 * @return object;
	 */
	public function getCancel()
	{
		require_once("core/SagePayCancel.php");
		return new SagePayCancel();
	}
	
	/**
	 * getDirectRefund
	 * 
	 * gets an instance of direct refund action
	 *
	 * @access public
	 * @return object;
	 */
	public function getDirectRefund()
	{
		require_once("core/SagePayDirectRefund.php");
		return new SagePayDirectRefund();
	}
	
	/**
	 * getManualPayment
	 * 
	 * gets an instance of manual payment action
	 *
	 * @access public
	 * @return object;
	 */
	public function getManualPayment()
	{
		require_once("core/SagePayManualPayment.php");
		return new SagePayManualPayment();
	}
	
	/**
	 * getPayment
	 * 
	 * gets an instance of payment action
	 *
	 * @access public
	 * @return object;
	 */
	public function getPayment()
	{
		require_once("core/SagePayPayment.php");
		return new SagePayPayment();
	}
	
	/**
	 * getPaypal
	 * 
	 * gets an instance of paypal action
	 *
	 * @access public
	 * @return object;
	 */
	public function getPaypal()
	{
		require_once("core/SagePayPaypal.php");
		return new SagePayPaypal();
	}
	
	/**
	 * getRefund
	 * 
	 * gets an instance of refund action
	 *
	 * @access public
	 * @return object;
	 */
	public function getRefund()
	{
		require_once("core/SagePayRefund.php");
		return new SagePayRefund();
	}
	
	/**
	 * getRelease
	 * 
	 * gets an instance of release action
	 *
	 * @access public
	 * @return object;
	 */
	public function getRelease()
	{
		require_once("core/SagePayRelease.php");
		return new SagePayRelease();
	}
	
	/**
	 * getRepeat
	 * 
	 * gets an instance of repeat action
	 *
	 * @access public
	 * @return object;
	 */
	public function getRepeat()
	{
		require_once("core/SagePayRepeat.php");
		return new SagePayRepeat();
	}
	
	/**
	 * getVoid
	 * 
	 * gets an instance of void action
	 *
	 * @access public
	 * @return object;
	 */
	public function getVoid()
	{
		require_once("core/SagePayVoid.php");
		return new SagePayVoid();
	}
    
    	/**
	 * doAction
	 * 
	 * executes action and gets response from SagePay.
	 *
	 * @access public
	 * @return string;
	 */
	public function doAction($backUrl = null)
	{	
        if(isset($_POST["PaRes"]))
		{		
			$sage = unserialize($_SESSION['SagePayDirect']);
			$response = $sage->finish3DSecure($_POST);
			return $response;
		}
		elseif(!isset($this->_action))
		{
			trigger_error("You should set action before call doAction() method", E_USER_ERROR);
		}
		else
		{ 
			$response = $this->doMakeRequest();
		}

		if(isset($response['ACSURL']) && isset($response['PAReq']) && isset($response['MD']))
		{
            $_SESSION['SagePayDirect'] = serialize($this);
			$backUrl = $backUrl ? $backUrl : self::getBackURL();
			$_SESSION['sageAuthForm'] = self::get3DForm($response['ACSURL'], $response['PAReq'], $response['MD'], $backUrl);	
			throw new SageAuthenticationException('3D authentication required');
		} else {
            $_SESSION['SagePayDirect'] = serialize($this);
        }
//		elseif($this->_action->getTxType()=='PAYMENT')
//		{
//			$number = $this->_action->getCard()->getCardNumber();
//			$num = substr($number, -4);
//			$string = str_replace($number, $num, $string);			
//			$_SESSION['SagePayDirectRequest'] = $string;
//		}
		
		if(isset($_POST["PaRes"]))
		{	
			$sage = unserialize($_SESSION['SagePayDirect']);
			$response = $sage->finish3DSecure($_POST);		
		}
		
		return $response;
	}
	
    /**
     * @return type
     */
    public function doMakeRequest() {
        $string = $this->getPreparedData().$this->_action->getPreparedData();
        parse_str($string);
        $_SESSION['VendorTxCode'] = $VendorTxCode;
        $url = $this->getActionURL();
        $response = $this->getCurlResponse($url, $string);				
        return self::recogniseResponseFields(self::explodeResponse($response));
    }
    
    
    /**
	 * finish3DSecure
	 * 
	 * This method is for finishing 3D Secure Payment.
	 *
	 * @param array
	 * @access public
	 * @return string;
	 */
	public function finish3DSecure($data)
	{
		$string = "MD=".$data['MD']."&PaRes=".$data['PaRes'];
		$url = $this->get3DCallbackURL();
		$response = $this->getCurlResponse($url, $string);
		$response = self::recogniseResponseFields(self::explodeResponse($response));
		return $response;
	}
	
	/**
	 * getCurlResponse
	 * 
	 * @param string $url;
	 * @param string $data;
	 * @access public;
	 * @return string;
	 */
	public function getCurlResponse($url, $data)
	{
		//check if Curl is installed
		try
		{
			if(!function_exists('curl_setopt'))			
				throw new exception ("Curl is not installed in your server.");
		}
		catch (exception $exception)
		{
			trigger_error($exception->getMessage(), E_USER_ERROR);
		}
		
		$curlSession = curl_init();

		// Set curl options
		curl_setopt ($curlSession, CURLOPT_URL, $url);
		curl_setopt ($curlSession, CURLOPT_HEADER, 0);
		curl_setopt ($curlSession, CURLOPT_POST, 1);
		curl_setopt ($curlSession, CURLOPT_POSTFIELDS, $data);
		curl_setopt ($curlSession, CURLOPT_RETURNTRANSFER, 1);		 
		curl_setopt ($curlSession, CURLOPT_TIMEOUT, 30);
		//curl_setopt ($curlSession, CURLOPT_TIMEOUT_MS, 30);
		curl_setopt ($curlSession, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt ($curlSession, CURLOPT_SSL_VERIFYHOST, 2);
    	
    	// Execute request
		
        $response = curl_exec($curlSession);
		
		// Verify that the request executed successfully.
      
        	if (curl_errno($curlSession))
        	{
        		throw new SageCurlException ("A problem occured when posting data to Sage Pay (Timeout). ");
        	}
        	else
        	{
        		curl_close($curlSession);
        	}
      
        return $response;
	}
	
	/**
	 * validateLength
	 * 
	 * returns true if string is longer than limit.
	 *  
	 * @param string $str;
	 * @param int $limit;
	 * @static
	 * @access public;
	 * @return boolean;
	 */
	public static function validateLength($str, $limit)
	{
		$length = strlen((string)$str);
		if($length <= $limit) return false;
		else return true;
	}
	
	/**
	 * cutString
	 * 
	 * cuts string if it is longer than limit.
	 *  
	 * @param string $str;
	 * @param int $limit;
	 * @static
	 * @access public;
	 * @return string;
	 */
	public static function cutString($str, $limit)
	{
		$length = strlen($str);
		if($length <= $limit)
		{
			$result = $str;
		}
		else
		{
			$result = substr($str, 0, $limit);
		}
		return $result;
	}
	
	/**
     * getPreparedData
     * 
     * gets data for sending to SagePay
     * 
     * @access public
     * @return string
     */
	public function getPreparedData()
	{
		$string  = "VPSProtocol=".urlencode($this->_vpsProtocol);
		$string .= "&Vendor=".urlencode($this->_vendor);		
		return $string;
	}
	
	
	/**
     * get3DForm
     * 
     * gets form for 3D Secure payment
     * 
     * @param string $ascURL;
     * @param string $pareq;
     * @param string $md;
     * @param string $yourURL;
     * @static 
     * @access private
     * @return string
     */
	private static function get3DForm($ascURL, $pareq, $md, $yourURL)
	{
		$form = '<html><head><title>3D Secure Verification</title></head>
			<body OnLoad="OnLoadEvent();">			
			<form name="issuerForm" method="POST" action="'.trim($ascURL).'" >
			<input type="hidden" name="PaReq" value="'.trim($pareq).'" />
			<input type="hidden" name="MD" value="'.trim($md).'" />
			<input type="hidden" name="TermUrl" value="'.trim($yourURL).'" />			
			<noscript>
			This page should forward you to your own card issuer for identification.
			If your browser does not start loading the page, press the button you see.
			<br/>
			After you successfully identify yourself you will be sent back to the site
			where the payment process will continue as if nothing had happened.
			<br/>
			<input type="submit" name="Authenticate your card" />
			</noscript>
			</form>
			<script language="Javascript">
			<!--
			function OnLoadEvent() {
				document.issuerForm.submit(); }
			//-->
			</script>
            <script type="text/javascript">
            var _gaq = _gaq || [];
                _gaq.push([\'_setAccount\', \'UA-543198-9\']);
                _gaq.push([\'_trackPageview\']);    

                (function() {
                    var ga = document.createElement(\'script\'); ga.type = \'text/javascript\'; ga.async = true;
                    ga.src = (\'https:\' == document.location.protocol ? \'https://\' : \'http://\') + \'stats.g.doubleclick.net/dc.js\';
                    var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(ga, s);
                })();
            </script>
			</body>
			</html>';
		return $form;
	}

	/**
     * explodeResponse
     *  
     * @param string $response;     
     * @static 
     * @access private
     * @return array
     */
	private static function explodeResponse($response)
	{		
		$result = explode("\n", $response);
		return $result;
	}
	
	/**
     * recogniseResponseFields
     *  
     * @param string $response;     
     * @static 
     * @access private
     * @return array
     */
	private static function recogniseResponseFields($response)
	{
		$result = array();		
		foreach ($response as $key => $value)
		{			
			switch (true)
			{
				case (strstr($value, "3DSecureStatus=")):
					$result['3DSecureStatus'] = str_replace("3DSecureStatus=", '', $value);
					break;				
				case (strstr($value, "Status=")):
					$result['Status'] = str_replace("Status=", '', $value);
					break;
				case (strstr($value, "StatusDetail=")):
					$result['StatusDetail'] = str_replace("StatusDetail=", '', $value);
					break;
				case (strstr($value, "VPSTxId=")):
					$result['VPSTxId'] = str_replace("VPSTxId=", '', $value);
					break;
				case (strstr($value, "SecurityKey=")):
					$result['SecurityKey'] = str_replace("SecurityKey=", '', $value);
					break;				
				case (strstr($value, "TxAuthNo=")):
					$result['TxAuthNo'] = str_replace("TxAuthNo=", '', $value);
					break;
				case (strstr($value, "AVSCV2=")):
					$result['AVSCV2'] = str_replace("AVSCV2=", '', $value);
				case (strstr($value, "AddressResult=")):
					$result['AddressResult'] = str_replace("AddressResult=", '', $value);
					break;
				case (strstr($value, "PostCodeResult=")):
					$result['PostCodeResult'] = str_replace("PostCodeResult=", '', $value);
					break;
				case (strstr($value, "CV2Result=")):
					$result['CV2Result'] = str_replace("CV2Result=", '', $value);
					break;				
				case (strstr($value, "CAVV=")):
					$result['CAVV'] = str_replace("CAVV=", '', $value);
					break;
				case (strstr($value, "MD=")):
					$result['MD'] = str_replace("MD=", '', $value);
					break;
				case (strstr($value, "ACSURL=")):
					$result['ACSURL'] = str_replace("ACSURL=", '', $value);
					break;
				case (strstr($value, "PAReq=")):
					$result['PAReq'] = str_replace("PAReq=", '', $value);
					break;
				case (strstr($value, "PayPalRedirectURL=")):
					$result['PaypalRedirectURL'] = str_replace("PayPalRedirectURL=", '', $value);
					break;
                case (strstr($value, "Token=")):
					$result['Token'] = str_replace("Token=", '', $value);
					break;
			}			
		}
		return $result;
	}
	
	/**
     * getActionURL
     * 
     * @access private
     * @return string
     */
	private function getActionURL()
	{
		switch($this->_action->getTxType())
		{
			case "PAYMENT":
			case "DEFERRED":
			case "AUTHENTICATE":
				$simulator = self::SAGEPAY_SIMULATOR_URL."VSPDirectGateway.asp";
				$test = self::SAGEPAY_TEST_URL."vspdirect-register.vsp";
				$live = self::SAGEPAY_LIVE_URL."vspdirect-register.vsp";
				break;
			case "RELEASE":
				$simulator = self::SAGEPAY_SIMULATOR_URL."VSPServerGateway.asp?Service=VendorReleaseTx";
				$test = self::SAGEPAY_TEST_URL."release.vsp";
				$live = self::SAGEPAY_LIVE_URL."release.vsp";
				break;
			case "ABORT":
				$simulator = self::SAGEPAY_SIMULATOR_URL."VSPServerGateway.asp?Service=VendorAbortTx";
				$test = self::SAGEPAY_TEST_URL."abort.vsp";
				$live = self::SAGEPAY_LIVE_URL."abort.vsp";
				break;
			case "REFUND":
				$simulator = self::SAGEPAY_SIMULATOR_URL."VSPServerGateway.asp?Service=VendorRefundTx";
				$test = self::SAGEPAY_TEST_URL."refund.vsp";
				$live = self::SAGEPAY_LIVE_URL."refund.vsp";
				break;
			case "REPEAT":
			case "REPEATDEFERRED":
				$simulator = self::SAGEPAY_SIMULATOR_URL."VSPServerGateway.asp?Service=VendorRepeatTx";
				$test = self::SAGEPAY_TEST_URL."repeat.vsp";
				$live = self::SAGEPAY_LIVE_URL."repeat.vsp";
				break;
			case "VOID":
				$simulator = self::SAGEPAY_SIMULATOR_URL."VSPServerGateway.asp?Service=VendorVoidTx";
				$test = self::SAGEPAY_TEST_URL."void.vsp";
				$live = self::SAGEPAY_LIVE_URL."void.vsp";
				break;
			case "MANUAL":				
				$simulator = null; //Not supported in VSP Simulator at present
				$test = self::SAGEPAY_TEST_URL."manualpayment.vsp";
				$live = self::SAGEPAY_LIVE_URL."manualpayment.vsp";
				break;
			case "DIRECTREFUND":
				$simulator = null; //Not supported in VSP Simulator at present
				$test = self::SAGEPAY_TEST_URL."directrefund.vsp";
				$live = self::SAGEPAY_LIVE_URL."directrefund.vsp";
				break;
			case "AUTHORISE":
				$simulator = self::SAGEPAY_SIMULATOR_URL."VSPServerGateway.asp?Service=VendorAuthoriseTx";
				$test = self::SAGEPAY_TEST_URL."authorise.vsp";
				$live = self::SAGEPAY_LIVE_URL."authorise.vsp";
				break;
			case "CANCEL":
				$simulator = self::SAGEPAY_SIMULATOR_URL."VSPServerGateway.asp?Service=VendorCancelTx";
				$test = self::SAGEPAY_TEST_URL."cancel.vsp";
				$live = self::SAGEPAY_LIVE_URL."cancel.vsp";
				break;		
            case "TOKEN":
                $simulator = null;
                $test = self::SAGEPAY_TEST_URL."directtoken.vsp";
                $live = self::SAGEPAY_LIVE_URL."directtoken.vsp";
                break;
            case "REMOVETOKEN":
                $simulator = null;
                $test = self::SAGEPAY_TEST_URL."removetoken.vsp";
                $live = self::SAGEPAY_LIVE_URL."removetoken.vsp";
                break;
		}
		if($this->_system == "SIMULATOR")
		{
			if(!is_null($simulator))
			{
				return $simulator;
			}
			else
			{
				trigger_error("This action not supported in VSP Simulator at the present", E_USER_ERROR);
			}
		}
		elseif($this->_system == "TEST")
		{	
			return $test;
		}
		elseif($this->_system == "LIVE")
		{		
			return $live;
		}
	}

	/**
     * get3DCallbackURL
     * 
     * @access private
     * @return string
     */
	private function get3DCallbackURL()
	{
		if($this->_system == "SIMULATOR")
		{
			return self::SAGEPAY_SIMULATOR_URL."VSPDirectCallback.asp";
		}
		elseif($this->_system == "TEST")
		{
			return self::SAGEPAY_TEST_URL."direct3dcallback.vsp";
		}
		elseif($this->_system == "LIVE")
		{
			return self::SAGEPAY_LIVE_URL."direct3dcallback.vsp";
		}
	}

	/**
     * getBackURL
     * 
     * @static 
     * @access private
     * @return string
     */
	private static function getBackURL()
	{
        $isHTTPS = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on");
        $port = (isset($_SERVER["SERVER_PORT"]) && ((!$isHTTPS && $_SERVER["SERVER_PORT"] != "80") || ($isHTTPS && $_SERVER["SERVER_PORT"] != "443")));
        $port = ($port) ? ':'.$_SERVER["SERVER_PORT"] : '';
        $backURL = ($isHTTPS ? 'https://' : 'http://').$_SERVER["SERVER_NAME"].$port.$_SERVER["REQUEST_URI"];
		return $backURL;
	}
	
	
	public static function logErrors($msg)
	{
		$log_file = dirname(__FILE__).'/logs/errors.log';
		if(!$file = @fopen($log_file, "a"))
		{			
			$log = "Cannot open " . $log_file . " file.\n" .
					"Logs are not writable, set them to 777";			
			return false;
		}
		else
		{
			fwrite($file, sprintf("\n%s:- %s\n",date("D M j G:i:s T Y"), $msg));
			return true;			
		}
	}
	
	/**
     * getStates
     * 
     * Returns all USA states array
     * 
     * @static 
     * @access public
     * @return array
     */
	public static function getStates()
	{
		return array('AL'=>"Alabama",  
			'AK'=>"Alaska",  
			'AZ'=>"Arizona",  
			'AR'=>"Arkansas",  
			'CA'=>"California",  
			'CO'=>"Colorado",  
			'CT'=>"Connecticut",  
			'DE'=>"Delaware",  
			'DC'=>"District Of Columbia",  
			'FL'=>"Florida",  
			'GA'=>"Georgia",  
			'HI'=>"Hawaii",  
			'ID'=>"Idaho",  
			'IL'=>"Illinois",  
			'IN'=>"Indiana",  
			'IA'=>"Iowa",  
			'KS'=>"Kansas",  
			'KY'=>"Kentucky",  
			'LA'=>"Louisiana",  
			'ME'=>"Maine",  
			'MD'=>"Maryland",  
			'MA'=>"Massachusetts",  
			'MI'=>"Michigan",  
			'MN'=>"Minnesota",  
			'MS'=>"Mississippi",  
			'MO'=>"Missouri",  
			'MT'=>"Montana",
			'NE'=>"Nebraska",
			'NV'=>"Nevada",
			'NH'=>"New Hampshire",
			'NJ'=>"New Jersey",
			'NM'=>"New Mexico",
			'NY'=>"New York",
			'NC'=>"North Carolina",
			'ND'=>"North Dakota",
			'OH'=>"Ohio",  
			'OK'=>"Oklahoma",  
			'OR'=>"Oregon",  
			'PA'=>"Pennsylvania",  
			'RI'=>"Rhode Island",  
			'SC'=>"South Carolina",  
			'SD'=>"South Dakota",
			'TN'=>"Tennessee",  
			'TX'=>"Texas",  
			'UT'=>"Utah",  
			'VT'=>"Vermont",  
			'VA'=>"Virginia",  
			'WA'=>"Washington",  
			'WV'=>"West Virginia",  
			'WI'=>"Wisconsin",  
			'WY'=>"Wyoming");
	}

	/**
     * getCardTypes
     * 
     * Returns all card types array
     * 
     * @static 
     * @access public
     * @return array
     */
	public static function getCardTypes()
	{
		return array(
			self::CARD_AMEX => "American Express",
			#"DINERS" => "Diner's Club",
			#"JCB" => "JCB",
			#"LASER" => "Laser",
			self::CARD_MAESTRO => "Maestro",
			"MC" => "MasterCard",
			#"SOLO" => "Solo",
			"VISA" => "Visa",			
			"DELTA" => "Visa Debit/Delta",
			"UKE" => "Visa Electron"
		);
	}
	
	public static function cleanAuthForm() {
		if (isset($_SESSION['sageAuthForm'])) {
			unset($_SESSION['sageAuthForm']);
		}
		if (isset($_SESSION['VendorTxCode'])) {
			unset($_SESSION['VendorTxCode']);
		}
		if (isset($_SESSION['SagePayDirect'])) {
			unset($_SESSION['SagePayDirect']);
		}
        
        if (isset($_SESSION['token'])) {
			unset($_SESSION['token']);
		}
        
		}
    
}
?>
